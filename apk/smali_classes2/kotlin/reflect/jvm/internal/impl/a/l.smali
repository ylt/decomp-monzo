.class public abstract Lkotlin/reflect/jvm/internal/impl/a/l;
.super Ljava/lang/Object;
.source "KotlinBuiltIns.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lkotlin/reflect/jvm/internal/impl/a/l$a;,
        Lkotlin/reflect/jvm/internal/impl/a/l$b;,
        Lkotlin/reflect/jvm/internal/impl/a/l$c;
    }
.end annotation


# static fields
.field private static final a:Lkotlin/reflect/jvm/internal/impl/e/b;

.field public static final b:Lkotlin/reflect/jvm/internal/impl/e/f;

.field public static final c:Lkotlin/reflect/jvm/internal/impl/e/b;

.field public static final d:Lkotlin/reflect/jvm/internal/impl/e/b;

.field public static final e:Lkotlin/reflect/jvm/internal/impl/e/b;

.field public static final f:Lkotlin/reflect/jvm/internal/impl/e/b;

.field public static final g:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/e/b;",
            ">;"
        }
    .end annotation
.end field

.field public static final h:Lkotlin/reflect/jvm/internal/impl/a/l$a;

.field public static final i:Lkotlin/reflect/jvm/internal/impl/e/f;

.field static final synthetic j:Z


# instance fields
.field private k:Lkotlin/reflect/jvm/internal/impl/b/b/u;

.field private final l:Lkotlin/reflect/jvm/internal/impl/j/f;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/reflect/jvm/internal/impl/j/f",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/a/l$c;",
            ">;"
        }
    .end annotation
.end field

.field private final m:Lkotlin/reflect/jvm/internal/impl/j/f;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/reflect/jvm/internal/impl/j/f",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/a/l$b;",
            ">;"
        }
    .end annotation
.end field

.field private final n:Lkotlin/reflect/jvm/internal/impl/j/c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/reflect/jvm/internal/impl/j/c",
            "<",
            "Ljava/lang/Integer;",
            "Lkotlin/reflect/jvm/internal/impl/b/e;",
            ">;"
        }
    .end annotation
.end field

.field private final o:Lkotlin/reflect/jvm/internal/impl/j/i;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 55
    const-class v0, Lkotlin/reflect/jvm/internal/impl/a/l;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    sput-boolean v0, Lkotlin/reflect/jvm/internal/impl/a/l;->j:Z

    .line 56
    const-string v0, "kotlin"

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/e/f;->a(Ljava/lang/String;)Lkotlin/reflect/jvm/internal/impl/e/f;

    move-result-object v0

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/a/l;->b:Lkotlin/reflect/jvm/internal/impl/e/f;

    .line 57
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/a/l;->b:Lkotlin/reflect/jvm/internal/impl/e/f;

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/e/b;->b(Lkotlin/reflect/jvm/internal/impl/e/f;)Lkotlin/reflect/jvm/internal/impl/e/b;

    move-result-object v0

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/a/l;->c:Lkotlin/reflect/jvm/internal/impl/e/b;

    .line 58
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/a/l;->c:Lkotlin/reflect/jvm/internal/impl/e/b;

    const-string v3, "annotation"

    invoke-static {v3}, Lkotlin/reflect/jvm/internal/impl/e/f;->a(Ljava/lang/String;)Lkotlin/reflect/jvm/internal/impl/e/f;

    move-result-object v3

    invoke-virtual {v0, v3}, Lkotlin/reflect/jvm/internal/impl/e/b;->a(Lkotlin/reflect/jvm/internal/impl/e/f;)Lkotlin/reflect/jvm/internal/impl/e/b;

    move-result-object v0

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/a/l;->a:Lkotlin/reflect/jvm/internal/impl/e/b;

    .line 59
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/a/l;->c:Lkotlin/reflect/jvm/internal/impl/e/b;

    const-string v3, "collections"

    invoke-static {v3}, Lkotlin/reflect/jvm/internal/impl/e/f;->a(Ljava/lang/String;)Lkotlin/reflect/jvm/internal/impl/e/f;

    move-result-object v3

    invoke-virtual {v0, v3}, Lkotlin/reflect/jvm/internal/impl/e/b;->a(Lkotlin/reflect/jvm/internal/impl/e/f;)Lkotlin/reflect/jvm/internal/impl/e/b;

    move-result-object v0

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/a/l;->d:Lkotlin/reflect/jvm/internal/impl/e/b;

    .line 60
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/a/l;->c:Lkotlin/reflect/jvm/internal/impl/e/b;

    const-string v3, "ranges"

    invoke-static {v3}, Lkotlin/reflect/jvm/internal/impl/e/f;->a(Ljava/lang/String;)Lkotlin/reflect/jvm/internal/impl/e/f;

    move-result-object v3

    invoke-virtual {v0, v3}, Lkotlin/reflect/jvm/internal/impl/e/b;->a(Lkotlin/reflect/jvm/internal/impl/e/f;)Lkotlin/reflect/jvm/internal/impl/e/b;

    move-result-object v0

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/a/l;->e:Lkotlin/reflect/jvm/internal/impl/e/b;

    .line 61
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/a/l;->c:Lkotlin/reflect/jvm/internal/impl/e/b;

    const-string v3, "text"

    invoke-static {v3}, Lkotlin/reflect/jvm/internal/impl/e/f;->a(Ljava/lang/String;)Lkotlin/reflect/jvm/internal/impl/e/f;

    move-result-object v3

    invoke-virtual {v0, v3}, Lkotlin/reflect/jvm/internal/impl/e/b;->a(Lkotlin/reflect/jvm/internal/impl/e/f;)Lkotlin/reflect/jvm/internal/impl/e/b;

    move-result-object v0

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/a/l;->f:Lkotlin/reflect/jvm/internal/impl/e/b;

    .line 63
    const/4 v0, 0x6

    new-array v0, v0, [Lkotlin/reflect/jvm/internal/impl/e/b;

    sget-object v3, Lkotlin/reflect/jvm/internal/impl/a/l;->c:Lkotlin/reflect/jvm/internal/impl/e/b;

    aput-object v3, v0, v2

    sget-object v2, Lkotlin/reflect/jvm/internal/impl/a/l;->d:Lkotlin/reflect/jvm/internal/impl/e/b;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    sget-object v2, Lkotlin/reflect/jvm/internal/impl/a/l;->e:Lkotlin/reflect/jvm/internal/impl/e/b;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    sget-object v2, Lkotlin/reflect/jvm/internal/impl/a/l;->a:Lkotlin/reflect/jvm/internal/impl/e/b;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/a/o;->a()Lkotlin/reflect/jvm/internal/impl/e/b;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x5

    sget-object v2, Lkotlin/reflect/jvm/internal/impl/a/l;->c:Lkotlin/reflect/jvm/internal/impl/e/b;

    const-string v3, "internal"

    invoke-static {v3}, Lkotlin/reflect/jvm/internal/impl/e/f;->a(Ljava/lang/String;)Lkotlin/reflect/jvm/internal/impl/e/f;

    move-result-object v3

    invoke-virtual {v2, v3}, Lkotlin/reflect/jvm/internal/impl/e/b;->a(Lkotlin/reflect/jvm/internal/impl/e/f;)Lkotlin/reflect/jvm/internal/impl/e/b;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, Lkotlin/a/ah;->a([Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v0

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/a/l;->g:Ljava/util/Set;

    .line 81
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/a/l$a;

    invoke-direct {v0}, Lkotlin/reflect/jvm/internal/impl/a/l$a;-><init>()V

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/a/l;->h:Lkotlin/reflect/jvm/internal/impl/a/l$a;

    .line 82
    const-string v0, "<built-ins module>"

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/e/f;->c(Ljava/lang/String;)Lkotlin/reflect/jvm/internal/impl/e/f;

    move-result-object v0

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/a/l;->i:Lkotlin/reflect/jvm/internal/impl/e/f;

    return-void

    :cond_0
    move v0, v2

    .line 55
    goto/16 :goto_0
.end method

.method protected constructor <init>(Lkotlin/reflect/jvm/internal/impl/j/i;)V
    .locals 5

    .prologue
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "storageManager"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "kotlin/reflect/jvm/internal/impl/builtins/KotlinBuiltIns"

    aput-object v4, v2, v3

    const/4 v3, 0x2

    const-string v4, "<init>"

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 84
    :cond_0
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 85
    iput-object p1, p0, Lkotlin/reflect/jvm/internal/impl/a/l;->o:Lkotlin/reflect/jvm/internal/impl/j/i;

    .line 87
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/a/l$1;

    invoke-direct {v0, p0}, Lkotlin/reflect/jvm/internal/impl/a/l$1;-><init>(Lkotlin/reflect/jvm/internal/impl/a/l;)V

    invoke-interface {p1, v0}, Lkotlin/reflect/jvm/internal/impl/j/i;->a(Lkotlin/d/a/a;)Lkotlin/reflect/jvm/internal/impl/j/f;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/a/l;->m:Lkotlin/reflect/jvm/internal/impl/j/f;

    .line 103
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/a/l$2;

    invoke-direct {v0, p0}, Lkotlin/reflect/jvm/internal/impl/a/l$2;-><init>(Lkotlin/reflect/jvm/internal/impl/a/l;)V

    invoke-interface {p1, v0}, Lkotlin/reflect/jvm/internal/impl/j/i;->a(Lkotlin/d/a/a;)Lkotlin/reflect/jvm/internal/impl/j/f;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/a/l;->l:Lkotlin/reflect/jvm/internal/impl/j/f;

    .line 123
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/a/l$3;

    invoke-direct {v0, p0}, Lkotlin/reflect/jvm/internal/impl/a/l$3;-><init>(Lkotlin/reflect/jvm/internal/impl/a/l;)V

    invoke-interface {p1, v0}, Lkotlin/reflect/jvm/internal/impl/j/i;->a(Lkotlin/d/a/b;)Lkotlin/reflect/jvm/internal/impl/j/c;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/a/l;->n:Lkotlin/reflect/jvm/internal/impl/j/c;

    .line 134
    return-void
.end method

.method static synthetic K()Lkotlin/reflect/jvm/internal/impl/e/b;
    .locals 1

    .prologue
    .line 55
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/a/l;->a:Lkotlin/reflect/jvm/internal/impl/e/b;

    return-object v0
.end method

.method public static a(I)Ljava/lang/String;
    .locals 5

    .prologue
    .line 535
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Function"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "@NotNull method %s.%s must not return null"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "kotlin/reflect/jvm/internal/impl/builtins/KotlinBuiltIns"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "getFunctionName"

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    return-object v0
.end method

.method static synthetic a(Lkotlin/reflect/jvm/internal/impl/a/l;)Lkotlin/reflect/jvm/internal/impl/b/b/u;
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/a/l;->k:Lkotlin/reflect/jvm/internal/impl/b/b/u;

    return-object v0
.end method

.method private static a(Ljava/lang/String;Lkotlin/reflect/jvm/internal/impl/b/y;)Lkotlin/reflect/jvm/internal/impl/b/e;
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    if-nez p0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "simpleName"

    aput-object v3, v2, v4

    const-string v3, "kotlin/reflect/jvm/internal/impl/builtins/KotlinBuiltIns"

    aput-object v3, v2, v5

    const-string v3, "getBuiltInClassByName"

    aput-object v3, v2, v6

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 455
    :cond_0
    invoke-static {p0}, Lkotlin/reflect/jvm/internal/impl/e/f;->a(Ljava/lang/String;)Lkotlin/reflect/jvm/internal/impl/e/f;

    move-result-object v0

    invoke-static {v0, p1}, Lkotlin/reflect/jvm/internal/impl/a/l;->a(Lkotlin/reflect/jvm/internal/impl/e/f;Lkotlin/reflect/jvm/internal/impl/b/y;)Lkotlin/reflect/jvm/internal/impl/b/e;

    move-result-object v0

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "@NotNull method %s.%s must not return null"

    new-array v2, v6, [Ljava/lang/Object;

    const-string v3, "kotlin/reflect/jvm/internal/impl/builtins/KotlinBuiltIns"

    aput-object v3, v2, v4

    const-string v3, "getBuiltInClassByName"

    aput-object v3, v2, v5

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    return-object v0
.end method

.method private static a(Lkotlin/reflect/jvm/internal/impl/b/e;Ljava/lang/String;)Lkotlin/reflect/jvm/internal/impl/b/e;
    .locals 7

    .prologue
    const/4 v2, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    if-nez p0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "enumDescriptor"

    aput-object v3, v2, v4

    const-string v3, "kotlin/reflect/jvm/internal/impl/builtins/KotlinBuiltIns"

    aput-object v3, v2, v5

    const-string v3, "getEnumEntry"

    aput-object v3, v2, v6

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    if-nez p1, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "entryName"

    aput-object v3, v2, v4

    const-string v3, "kotlin/reflect/jvm/internal/impl/builtins/KotlinBuiltIns"

    aput-object v3, v2, v5

    const-string v3, "getEnumEntry"

    aput-object v3, v2, v6

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 566
    :cond_1
    invoke-interface {p0}, Lkotlin/reflect/jvm/internal/impl/b/e;->B()Lkotlin/reflect/jvm/internal/impl/h/e/h;

    move-result-object v0

    invoke-static {p1}, Lkotlin/reflect/jvm/internal/impl/e/f;->a(Ljava/lang/String;)Lkotlin/reflect/jvm/internal/impl/e/f;

    move-result-object v1

    sget-object v2, Lkotlin/reflect/jvm/internal/impl/c/a/d;->d:Lkotlin/reflect/jvm/internal/impl/c/a/d;

    invoke-interface {v0, v1, v2}, Lkotlin/reflect/jvm/internal/impl/h/e/h;->c(Lkotlin/reflect/jvm/internal/impl/e/f;Lkotlin/reflect/jvm/internal/impl/c/a/b;)Lkotlin/reflect/jvm/internal/impl/b/h;

    move-result-object v0

    .line 569
    instance-of v1, v0, Lkotlin/reflect/jvm/internal/impl/b/e;

    if-eqz v1, :cond_2

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/e;

    :goto_0
    return-object v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static a(Lkotlin/reflect/jvm/internal/impl/e/f;Lkotlin/reflect/jvm/internal/impl/b/y;)Lkotlin/reflect/jvm/internal/impl/b/e;
    .locals 7

    .prologue
    const/4 v2, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    if-nez p0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "simpleName"

    aput-object v3, v2, v4

    const-string v3, "kotlin/reflect/jvm/internal/impl/builtins/KotlinBuiltIns"

    aput-object v3, v2, v5

    const-string v3, "getBuiltInClassByName"

    aput-object v3, v2, v6

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    if-nez p1, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "packageFragment"

    aput-object v3, v2, v4

    const-string v3, "kotlin/reflect/jvm/internal/impl/builtins/KotlinBuiltIns"

    aput-object v3, v2, v5

    const-string v3, "getBuiltInClassByName"

    aput-object v3, v2, v6

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 413
    :cond_1
    invoke-static {p0, p1}, Lkotlin/reflect/jvm/internal/impl/a/l;->b(Lkotlin/reflect/jvm/internal/impl/e/f;Lkotlin/reflect/jvm/internal/impl/b/y;)Lkotlin/reflect/jvm/internal/impl/b/e;

    move-result-object v0

    .line 414
    if-nez v0, :cond_2

    .line 415
    new-instance v0, Ljava/lang/AssertionError;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Built-in class "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/b/y;->f()Lkotlin/reflect/jvm/internal/impl/e/b;

    move-result-object v2

    invoke-virtual {v2, p0}, Lkotlin/reflect/jvm/internal/impl/e/b;->a(Lkotlin/reflect/jvm/internal/impl/e/f;)Lkotlin/reflect/jvm/internal/impl/e/b;

    move-result-object v2

    invoke-virtual {v2}, Lkotlin/reflect/jvm/internal/impl/e/b;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is not found"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 417
    :cond_2
    if-nez v0, :cond_3

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "@NotNull method %s.%s must not return null"

    new-array v2, v6, [Ljava/lang/Object;

    const-string v3, "kotlin/reflect/jvm/internal/impl/builtins/KotlinBuiltIns"

    aput-object v3, v2, v4

    const-string v3, "getBuiltInClassByName"

    aput-object v3, v2, v5

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_3
    return-object v0
.end method

.method static synthetic a(Lkotlin/reflect/jvm/internal/impl/a/l;Lkotlin/reflect/jvm/internal/impl/b/z;Ljava/util/Map;Lkotlin/reflect/jvm/internal/impl/e/b;)Lkotlin/reflect/jvm/internal/impl/b/y;
    .locals 1

    .prologue
    .line 55
    invoke-direct {p0, p1, p2, p3}, Lkotlin/reflect/jvm/internal/impl/a/l;->a(Lkotlin/reflect/jvm/internal/impl/b/z;Ljava/util/Map;Lkotlin/reflect/jvm/internal/impl/e/b;)Lkotlin/reflect/jvm/internal/impl/b/y;

    move-result-object v0

    return-object v0
.end method

.method private a(Lkotlin/reflect/jvm/internal/impl/b/z;Ljava/util/Map;Lkotlin/reflect/jvm/internal/impl/e/b;)Lkotlin/reflect/jvm/internal/impl/b/y;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/reflect/jvm/internal/impl/b/z;",
            "Ljava/util/Map",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/e/b;",
            "Lkotlin/reflect/jvm/internal/impl/b/y;",
            ">;",
            "Lkotlin/reflect/jvm/internal/impl/e/b;",
            ")",
            "Lkotlin/reflect/jvm/internal/impl/b/y;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x3

    const/4 v8, 0x2

    const/4 v7, 0x0

    const/4 v6, 0x1

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "fragmentProvider"

    aput-object v3, v2, v7

    const-string v3, "kotlin/reflect/jvm/internal/impl/builtins/KotlinBuiltIns"

    aput-object v3, v2, v6

    const-string v3, "createPackage"

    aput-object v3, v2, v8

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    if-nez p3, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "packageFqName"

    aput-object v3, v2, v7

    const-string v3, "kotlin/reflect/jvm/internal/impl/builtins/KotlinBuiltIns"

    aput-object v3, v2, v6

    const-string v3, "createPackage"

    aput-object v3, v2, v8

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 192
    :cond_1
    invoke-interface {p1, p3}, Lkotlin/reflect/jvm/internal/impl/b/z;->a(Lkotlin/reflect/jvm/internal/impl/e/b;)Ljava/util/List;

    move-result-object v5

    .line 194
    invoke-interface {v5}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_3

    new-instance v0, Lkotlin/reflect/jvm/internal/impl/b/b/m;

    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/a/l;->k:Lkotlin/reflect/jvm/internal/impl/b/b/u;

    invoke-direct {v0, v1, p3}, Lkotlin/reflect/jvm/internal/impl/b/b/m;-><init>(Lkotlin/reflect/jvm/internal/impl/b/w;Lkotlin/reflect/jvm/internal/impl/e/b;)V

    .line 218
    :goto_0
    if-eqz p2, :cond_2

    invoke-interface {p2, p3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 219
    :cond_2
    if-nez v0, :cond_5

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "@NotNull method %s.%s must not return null"

    new-array v2, v8, [Ljava/lang/Object;

    const-string v3, "kotlin/reflect/jvm/internal/impl/builtins/KotlinBuiltIns"

    aput-object v3, v2, v7

    const-string v3, "createPackage"

    aput-object v3, v2, v6

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 194
    :cond_3
    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v0

    if-ne v0, v6, :cond_4

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/y;

    goto :goto_0

    :cond_4
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/a/l$5;

    iget-object v2, p0, Lkotlin/reflect/jvm/internal/impl/a/l;->k:Lkotlin/reflect/jvm/internal/impl/b/b/u;

    move-object v1, p0

    move-object v3, p3

    move-object v4, p3

    invoke-direct/range {v0 .. v5}, Lkotlin/reflect/jvm/internal/impl/a/l$5;-><init>(Lkotlin/reflect/jvm/internal/impl/a/l;Lkotlin/reflect/jvm/internal/impl/b/w;Lkotlin/reflect/jvm/internal/impl/e/b;Lkotlin/reflect/jvm/internal/impl/e/b;Ljava/util/List;)V

    goto :goto_0

    .line 219
    :cond_5
    return-object v0
.end method

.method static synthetic a(Lkotlin/reflect/jvm/internal/impl/a/l;Ljava/lang/String;)Lkotlin/reflect/jvm/internal/impl/k/v;
    .locals 1

    .prologue
    .line 55
    invoke-direct {p0, p1}, Lkotlin/reflect/jvm/internal/impl/a/l;->d(Ljava/lang/String;)Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lkotlin/reflect/jvm/internal/impl/b/e;)Z
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    if-nez p0, :cond_0

    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/Object;

    const-string v5, "descriptor"

    aput-object v5, v4, v0

    const-string v0, "kotlin/reflect/jvm/internal/impl/builtins/KotlinBuiltIns"

    aput-object v0, v4, v1

    const/4 v0, 0x2

    const-string v1, "isArrayOrPrimitiveArray"

    aput-object v1, v4, v0

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 872
    :cond_0
    sget-object v2, Lkotlin/reflect/jvm/internal/impl/a/l;->h:Lkotlin/reflect/jvm/internal/impl/a/l$a;

    iget-object v2, v2, Lkotlin/reflect/jvm/internal/impl/a/l$a;->h:Lkotlin/reflect/jvm/internal/impl/e/c;

    invoke-static {p0, v2}, Lkotlin/reflect/jvm/internal/impl/a/l;->a(Lkotlin/reflect/jvm/internal/impl/b/h;Lkotlin/reflect/jvm/internal/impl/e/c;)Z

    move-result v2

    if-nez v2, :cond_1

    invoke-static {p0}, Lkotlin/reflect/jvm/internal/impl/h/c;->d(Lkotlin/reflect/jvm/internal/impl/b/m;)Lkotlin/reflect/jvm/internal/impl/e/c;

    move-result-object v2

    invoke-static {v2}, Lkotlin/reflect/jvm/internal/impl/a/l;->c(Lkotlin/reflect/jvm/internal/impl/e/c;)Lkotlin/reflect/jvm/internal/impl/a/m;

    move-result-object v2

    if-eqz v2, :cond_2

    :cond_1
    move v0, v1

    :cond_2
    return v0
.end method

.method private static a(Lkotlin/reflect/jvm/internal/impl/b/h;Lkotlin/reflect/jvm/internal/impl/e/c;)Z
    .locals 7

    .prologue
    const/4 v4, 0x3

    const/4 v6, 0x2

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-nez p0, :cond_0

    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    new-array v4, v4, [Ljava/lang/Object;

    const-string v5, "descriptor"

    aput-object v5, v4, v1

    const-string v1, "kotlin/reflect/jvm/internal/impl/builtins/KotlinBuiltIns"

    aput-object v1, v4, v0

    const-string v0, "classFqNameEquals"

    aput-object v0, v4, v6

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_0
    if-nez p1, :cond_1

    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    new-array v4, v4, [Ljava/lang/Object;

    const-string v5, "fqName"

    aput-object v5, v4, v1

    const-string v1, "kotlin/reflect/jvm/internal/impl/builtins/KotlinBuiltIns"

    aput-object v1, v4, v0

    const-string v0, "classFqNameEquals"

    aput-object v0, v4, v6

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 900
    :cond_1
    invoke-interface {p0}, Lkotlin/reflect/jvm/internal/impl/b/h;->D_()Lkotlin/reflect/jvm/internal/impl/e/f;

    move-result-object v2

    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/e/c;->f()Lkotlin/reflect/jvm/internal/impl/e/f;

    move-result-object v3

    invoke-virtual {v2, v3}, Lkotlin/reflect/jvm/internal/impl/e/f;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-static {p0}, Lkotlin/reflect/jvm/internal/impl/h/c;->d(Lkotlin/reflect/jvm/internal/impl/b/m;)Lkotlin/reflect/jvm/internal/impl/e/c;

    move-result-object v2

    invoke-virtual {p1, v2}, Lkotlin/reflect/jvm/internal/impl/e/c;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    :goto_0
    return v0

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public static a(Lkotlin/reflect/jvm/internal/impl/b/m;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    if-nez p0, :cond_0

    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/Object;

    const-string v5, "descriptor"

    aput-object v5, v4, v1

    const-string v1, "kotlin/reflect/jvm/internal/impl/builtins/KotlinBuiltIns"

    aput-object v1, v4, v0

    const/4 v0, 0x2

    const-string v1, "isBuiltIn"

    aput-object v1, v4, v0

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 393
    :cond_0
    const-class v2, Lkotlin/reflect/jvm/internal/impl/a/e;

    invoke-static {p0, v2, v1}, Lkotlin/reflect/jvm/internal/impl/h/c;->a(Lkotlin/reflect/jvm/internal/impl/b/m;Ljava/lang/Class;Z)Lkotlin/reflect/jvm/internal/impl/b/m;

    move-result-object v2

    if-eqz v2, :cond_1

    :goto_0
    return v0

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method private static a(Lkotlin/reflect/jvm/internal/impl/b/m;Lkotlin/reflect/jvm/internal/impl/e/b;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 1084
    invoke-interface {p0}, Lkotlin/reflect/jvm/internal/impl/b/m;->z_()Lkotlin/reflect/jvm/internal/impl/b/m;

    move-result-object v1

    .line 1085
    invoke-interface {v1}, Lkotlin/reflect/jvm/internal/impl/b/m;->w()Lkotlin/reflect/jvm/internal/impl/b/a/h;

    move-result-object v1

    .line 1087
    invoke-interface {v1, p1}, Lkotlin/reflect/jvm/internal/impl/b/a/h;->a(Lkotlin/reflect/jvm/internal/impl/e/b;)Lkotlin/reflect/jvm/internal/impl/b/a/c;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 1096
    :cond_0
    :goto_0
    return v0

    .line 1089
    :cond_1
    sget-object v2, Lkotlin/reflect/jvm/internal/impl/b/a/e;->j:Lkotlin/reflect/jvm/internal/impl/b/a/e$a;

    invoke-virtual {v2, p0}, Lkotlin/reflect/jvm/internal/impl/b/a/e$a;->a(Lkotlin/reflect/jvm/internal/impl/b/m;)Lkotlin/reflect/jvm/internal/impl/b/a/e;

    move-result-object v2

    .line 1090
    if-eqz v2, :cond_2

    .line 1091
    sget-object v3, Lkotlin/reflect/jvm/internal/impl/b/a/h;->a:Lkotlin/reflect/jvm/internal/impl/b/a/h$a;

    invoke-virtual {v3, v1, v2, p1}, Lkotlin/reflect/jvm/internal/impl/b/a/h$a;->a(Lkotlin/reflect/jvm/internal/impl/b/a/h;Lkotlin/reflect/jvm/internal/impl/b/a/e;Lkotlin/reflect/jvm/internal/impl/e/b;)Lkotlin/reflect/jvm/internal/impl/b/a/c;

    move-result-object v1

    if-nez v1, :cond_0

    .line 1096
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Lkotlin/reflect/jvm/internal/impl/e/c;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    if-nez p0, :cond_0

    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/Object;

    const-string v5, "arrayFqName"

    aput-object v5, v4, v1

    const-string v1, "kotlin/reflect/jvm/internal/impl/builtins/KotlinBuiltIns"

    aput-object v1, v4, v0

    const/4 v0, 0x2

    const-string v1, "isPrimitiveArray"

    aput-object v1, v4, v0

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 836
    :cond_0
    invoke-static {p0}, Lkotlin/reflect/jvm/internal/impl/a/l;->c(Lkotlin/reflect/jvm/internal/impl/e/c;)Lkotlin/reflect/jvm/internal/impl/a/m;

    move-result-object v2

    if-eqz v2, :cond_1

    :goto_0
    return v0

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public static a(Lkotlin/reflect/jvm/internal/impl/k/r;Lkotlin/reflect/jvm/internal/impl/e/c;)Z
    .locals 7

    .prologue
    const/4 v4, 0x3

    const/4 v6, 0x2

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-nez p0, :cond_0

    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    new-array v4, v4, [Ljava/lang/Object;

    const-string v5, "type"

    aput-object v5, v4, v1

    const-string v1, "kotlin/reflect/jvm/internal/impl/builtins/KotlinBuiltIns"

    aput-object v1, v4, v0

    const-string v0, "isConstructedFromGivenClass"

    aput-object v0, v4, v6

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_0
    if-nez p1, :cond_1

    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    new-array v4, v4, [Ljava/lang/Object;

    const-string v5, "fqName"

    aput-object v5, v4, v1

    const-string v1, "kotlin/reflect/jvm/internal/impl/builtins/KotlinBuiltIns"

    aput-object v1, v4, v0

    const-string v0, "isConstructedFromGivenClass"

    aput-object v0, v4, v6

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 890
    :cond_1
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/k/r;->i()Lkotlin/reflect/jvm/internal/impl/k/ad;

    move-result-object v2

    invoke-interface {v2}, Lkotlin/reflect/jvm/internal/impl/k/ad;->d()Lkotlin/reflect/jvm/internal/impl/b/h;

    move-result-object v2

    .line 891
    instance-of v3, v2, Lkotlin/reflect/jvm/internal/impl/b/e;

    if-eqz v3, :cond_2

    invoke-static {v2, p1}, Lkotlin/reflect/jvm/internal/impl/a/l;->a(Lkotlin/reflect/jvm/internal/impl/b/h;Lkotlin/reflect/jvm/internal/impl/e/c;)Z

    move-result v2

    if-eqz v2, :cond_2

    :goto_0
    return v0

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public static b(Lkotlin/reflect/jvm/internal/impl/e/c;)Lkotlin/reflect/jvm/internal/impl/a/m;
    .locals 5

    .prologue
    if-nez p0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "primitiveClassFqName"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "kotlin/reflect/jvm/internal/impl/builtins/KotlinBuiltIns"

    aput-object v4, v2, v3

    const/4 v3, 0x2

    const-string v4, "getPrimitiveTypeByFqName"

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 841
    :cond_0
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/a/l;->h:Lkotlin/reflect/jvm/internal/impl/a/l$a;

    iget-object v0, v0, Lkotlin/reflect/jvm/internal/impl/a/l$a;->aj:Ljava/util/Map;

    invoke-interface {v0, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/a/m;

    return-object v0
.end method

.method private b(Ljava/lang/String;)Lkotlin/reflect/jvm/internal/impl/b/e;
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "simpleName"

    aput-object v3, v2, v4

    const-string v3, "kotlin/reflect/jvm/internal/impl/builtins/KotlinBuiltIns"

    aput-object v3, v2, v5

    const-string v3, "getBuiltInClassByName"

    aput-object v3, v2, v6

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 450
    :cond_0
    invoke-static {p1}, Lkotlin/reflect/jvm/internal/impl/e/f;->a(Ljava/lang/String;)Lkotlin/reflect/jvm/internal/impl/e/f;

    move-result-object v0

    invoke-virtual {p0, v0}, Lkotlin/reflect/jvm/internal/impl/a/l;->a(Lkotlin/reflect/jvm/internal/impl/e/f;)Lkotlin/reflect/jvm/internal/impl/b/e;

    move-result-object v0

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "@NotNull method %s.%s must not return null"

    new-array v2, v6, [Ljava/lang/Object;

    const-string v3, "kotlin/reflect/jvm/internal/impl/builtins/KotlinBuiltIns"

    aput-object v3, v2, v4

    const-string v3, "getBuiltInClassByName"

    aput-object v3, v2, v5

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    return-object v0
.end method

.method private b(Lkotlin/reflect/jvm/internal/impl/e/f;)Lkotlin/reflect/jvm/internal/impl/b/e;
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "simpleName"

    aput-object v3, v2, v4

    const-string v3, "kotlin/reflect/jvm/internal/impl/builtins/KotlinBuiltIns"

    aput-object v3, v2, v5

    const-string v3, "getAnnotationClassByName"

    aput-object v3, v2, v6

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 403
    :cond_0
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/a/l;->m:Lkotlin/reflect/jvm/internal/impl/j/f;

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/j/f;->v_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/a/l$b;

    iget-object v0, v0, Lkotlin/reflect/jvm/internal/impl/a/l$b;->c:Lkotlin/reflect/jvm/internal/impl/b/y;

    invoke-static {p1, v0}, Lkotlin/reflect/jvm/internal/impl/a/l;->a(Lkotlin/reflect/jvm/internal/impl/e/f;Lkotlin/reflect/jvm/internal/impl/b/y;)Lkotlin/reflect/jvm/internal/impl/b/e;

    move-result-object v0

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "@NotNull method %s.%s must not return null"

    new-array v2, v6, [Ljava/lang/Object;

    const-string v3, "kotlin/reflect/jvm/internal/impl/builtins/KotlinBuiltIns"

    aput-object v3, v2, v4

    const-string v3, "getAnnotationClassByName"

    aput-object v3, v2, v5

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    return-object v0
.end method

.method private static b(Lkotlin/reflect/jvm/internal/impl/e/f;Lkotlin/reflect/jvm/internal/impl/b/y;)Lkotlin/reflect/jvm/internal/impl/b/e;
    .locals 7

    .prologue
    const/4 v2, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    if-nez p0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "simpleName"

    aput-object v3, v2, v4

    const-string v3, "kotlin/reflect/jvm/internal/impl/builtins/KotlinBuiltIns"

    aput-object v3, v2, v5

    const-string v3, "getBuiltInClassByNameNullable"

    aput-object v3, v2, v6

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    if-nez p1, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "packageFragment"

    aput-object v3, v2, v4

    const-string v3, "kotlin/reflect/jvm/internal/impl/builtins/KotlinBuiltIns"

    aput-object v3, v2, v5

    const-string v3, "getBuiltInClassByNameNullable"

    aput-object v3, v2, v6

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 439
    :cond_1
    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/b/y;->x_()Lkotlin/reflect/jvm/internal/impl/h/e/h;

    move-result-object v0

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/c/a/d;->d:Lkotlin/reflect/jvm/internal/impl/c/a/d;

    invoke-interface {v0, p0, v1}, Lkotlin/reflect/jvm/internal/impl/h/e/h;->c(Lkotlin/reflect/jvm/internal/impl/e/f;Lkotlin/reflect/jvm/internal/impl/c/a/b;)Lkotlin/reflect/jvm/internal/impl/b/h;

    move-result-object v0

    .line 444
    sget-boolean v1, Lkotlin/reflect/jvm/internal/impl/a/l;->j:Z

    if-nez v1, :cond_2

    if-eqz v0, :cond_2

    instance-of v1, v0, Lkotlin/reflect/jvm/internal/impl/b/e;

    if-nez v1, :cond_2

    new-instance v1, Ljava/lang/AssertionError;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Must be a class descriptor "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", but was "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v1

    .line 445
    :cond_2
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/e;

    return-object v0
.end method

.method public static b(I)Lkotlin/reflect/jvm/internal/impl/e/a;
    .locals 5

    .prologue
    .line 540
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/e/a;

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/a/l;->c:Lkotlin/reflect/jvm/internal/impl/e/b;

    invoke-static {p0}, Lkotlin/reflect/jvm/internal/impl/a/l;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lkotlin/reflect/jvm/internal/impl/e/f;->a(Ljava/lang/String;)Lkotlin/reflect/jvm/internal/impl/e/f;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lkotlin/reflect/jvm/internal/impl/e/a;-><init>(Lkotlin/reflect/jvm/internal/impl/e/b;Lkotlin/reflect/jvm/internal/impl/e/f;)V

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "@NotNull method %s.%s must not return null"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "kotlin/reflect/jvm/internal/impl/builtins/KotlinBuiltIns"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "getFunctionClassId"

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    return-object v0
.end method

.method static synthetic b(Lkotlin/reflect/jvm/internal/impl/a/l;)Lkotlin/reflect/jvm/internal/impl/j/f;
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/a/l;->m:Lkotlin/reflect/jvm/internal/impl/j/f;

    return-object v0
.end method

.method public static b(Lkotlin/reflect/jvm/internal/impl/b/e;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    if-nez p0, :cond_0

    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/Object;

    const-string v5, "descriptor"

    aput-object v5, v4, v1

    const-string v1, "kotlin/reflect/jvm/internal/impl/builtins/KotlinBuiltIns"

    aput-object v1, v4, v0

    const/4 v0, 0x2

    const-string v1, "isPrimitiveClass"

    aput-object v1, v4, v0

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 886
    :cond_0
    invoke-static {p0}, Lkotlin/reflect/jvm/internal/impl/h/c;->d(Lkotlin/reflect/jvm/internal/impl/b/m;)Lkotlin/reflect/jvm/internal/impl/e/c;

    move-result-object v2

    invoke-static {v2}, Lkotlin/reflect/jvm/internal/impl/a/l;->b(Lkotlin/reflect/jvm/internal/impl/e/c;)Lkotlin/reflect/jvm/internal/impl/a/m;

    move-result-object v2

    if-eqz v2, :cond_1

    :goto_0
    return v0

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public static b(Lkotlin/reflect/jvm/internal/impl/b/m;)Z
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    if-nez p0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v3, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/Object;

    const-string v5, "declarationDescriptor"

    aput-object v5, v4, v2

    const-string v2, "kotlin/reflect/jvm/internal/impl/builtins/KotlinBuiltIns"

    aput-object v2, v4, v1

    const/4 v1, 0x2

    const-string v2, "isDeprecated"

    aput-object v2, v4, v1

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1063
    :cond_0
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/a/l;->h:Lkotlin/reflect/jvm/internal/impl/a/l$a;

    iget-object v0, v0, Lkotlin/reflect/jvm/internal/impl/a/l$a;->x:Lkotlin/reflect/jvm/internal/impl/e/b;

    invoke-static {p0, v0}, Lkotlin/reflect/jvm/internal/impl/a/l;->a(Lkotlin/reflect/jvm/internal/impl/b/m;Lkotlin/reflect/jvm/internal/impl/e/b;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1072
    :goto_0
    return v1

    .line 1065
    :cond_1
    instance-of v0, p0, Lkotlin/reflect/jvm/internal/impl/b/ag;

    if-eqz v0, :cond_4

    move-object v0, p0

    .line 1066
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/ag;

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/b/ag;->y()Z

    move-result v3

    move-object v0, p0

    .line 1067
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/ag;

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/b/ag;->a()Lkotlin/reflect/jvm/internal/impl/b/ah;

    move-result-object v0

    .line 1068
    check-cast p0, Lkotlin/reflect/jvm/internal/impl/b/ag;

    invoke-interface {p0}, Lkotlin/reflect/jvm/internal/impl/b/ag;->c()Lkotlin/reflect/jvm/internal/impl/b/ai;

    move-result-object v4

    .line 1069
    if-eqz v0, :cond_3

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/a/l;->b(Lkotlin/reflect/jvm/internal/impl/b/m;)Z

    move-result v0

    if-eqz v0, :cond_3

    if-eqz v3, :cond_2

    if-eqz v4, :cond_3

    invoke-static {v4}, Lkotlin/reflect/jvm/internal/impl/a/l;->b(Lkotlin/reflect/jvm/internal/impl/b/m;)Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_2
    move v0, v1

    :goto_1
    move v1, v0

    goto :goto_0

    :cond_3
    move v0, v2

    goto :goto_1

    :cond_4
    move v1, v2

    .line 1072
    goto :goto_0
.end method

.method private static b(Lkotlin/reflect/jvm/internal/impl/k/r;Lkotlin/reflect/jvm/internal/impl/e/c;)Z
    .locals 7

    .prologue
    const/4 v4, 0x3

    const/4 v6, 0x2

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-nez p0, :cond_0

    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    new-array v4, v4, [Ljava/lang/Object;

    const-string v5, "type"

    aput-object v5, v4, v1

    const-string v1, "kotlin/reflect/jvm/internal/impl/builtins/KotlinBuiltIns"

    aput-object v1, v4, v0

    const-string v0, "isNotNullConstructedFromGivenClass"

    aput-object v0, v4, v6

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_0
    if-nez p1, :cond_1

    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    new-array v4, v4, [Ljava/lang/Object;

    const-string v5, "fqName"

    aput-object v5, v4, v1

    const-string v1, "kotlin/reflect/jvm/internal/impl/builtins/KotlinBuiltIns"

    aput-object v1, v4, v0

    const-string v0, "isNotNullConstructedFromGivenClass"

    aput-object v0, v4, v6

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 905
    :cond_1
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/k/r;->c()Z

    move-result v2

    if-nez v2, :cond_2

    invoke-static {p0, p1}, Lkotlin/reflect/jvm/internal/impl/a/l;->a(Lkotlin/reflect/jvm/internal/impl/k/r;Lkotlin/reflect/jvm/internal/impl/e/c;)Z

    move-result v2

    if-eqz v2, :cond_2

    :goto_0
    return v0

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public static c(Lkotlin/reflect/jvm/internal/impl/e/c;)Lkotlin/reflect/jvm/internal/impl/a/m;
    .locals 5

    .prologue
    if-nez p0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "primitiveArrayClassFqName"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "kotlin/reflect/jvm/internal/impl/builtins/KotlinBuiltIns"

    aput-object v4, v2, v3

    const/4 v3, 0x2

    const-string v4, "getPrimitiveTypeByArrayClassFqName"

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 846
    :cond_0
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/a/l;->h:Lkotlin/reflect/jvm/internal/impl/a/l$a;

    iget-object v0, v0, Lkotlin/reflect/jvm/internal/impl/a/l$a;->ak:Ljava/util/Map;

    invoke-interface {v0, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/a/m;

    return-object v0
.end method

.method private c(Ljava/lang/String;)Lkotlin/reflect/jvm/internal/impl/b/e;
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "simpleName"

    aput-object v3, v2, v4

    const-string v3, "kotlin/reflect/jvm/internal/impl/builtins/KotlinBuiltIns"

    aput-object v3, v2, v5

    const-string v3, "getCollectionClassByName"

    aput-object v3, v2, v6

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 629
    :cond_0
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/a/l;->m:Lkotlin/reflect/jvm/internal/impl/j/f;

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/j/f;->v_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/a/l$b;

    iget-object v0, v0, Lkotlin/reflect/jvm/internal/impl/a/l$b;->b:Lkotlin/reflect/jvm/internal/impl/b/y;

    invoke-static {p1, v0}, Lkotlin/reflect/jvm/internal/impl/a/l;->a(Ljava/lang/String;Lkotlin/reflect/jvm/internal/impl/b/y;)Lkotlin/reflect/jvm/internal/impl/b/e;

    move-result-object v0

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "@NotNull method %s.%s must not return null"

    new-array v2, v6, [Ljava/lang/Object;

    const-string v3, "kotlin/reflect/jvm/internal/impl/builtins/KotlinBuiltIns"

    aput-object v3, v2, v4

    const-string v3, "getCollectionClassByName"

    aput-object v3, v2, v5

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    return-object v0
.end method

.method public static c(Lkotlin/reflect/jvm/internal/impl/a/m;)Lkotlin/reflect/jvm/internal/impl/e/b;
    .locals 5

    .prologue
    if-nez p0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "primitiveType"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "kotlin/reflect/jvm/internal/impl/builtins/KotlinBuiltIns"

    aput-object v4, v2, v3

    const/4 v3, 0x2

    const-string v4, "getPrimitiveFqName"

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1076
    :cond_0
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/a/l;->c:Lkotlin/reflect/jvm/internal/impl/e/b;

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/a/m;->a()Lkotlin/reflect/jvm/internal/impl/e/f;

    move-result-object v1

    invoke-virtual {v0, v1}, Lkotlin/reflect/jvm/internal/impl/e/b;->a(Lkotlin/reflect/jvm/internal/impl/e/f;)Lkotlin/reflect/jvm/internal/impl/e/b;

    move-result-object v0

    return-object v0
.end method

.method public static c(Lkotlin/reflect/jvm/internal/impl/b/e;)Z
    .locals 5

    .prologue
    if-nez p0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "descriptor"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "kotlin/reflect/jvm/internal/impl/builtins/KotlinBuiltIns"

    aput-object v4, v2, v3

    const/4 v3, 0x2

    const-string v4, "isAny"

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 913
    :cond_0
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/a/l;->h:Lkotlin/reflect/jvm/internal/impl/a/l$a;

    iget-object v0, v0, Lkotlin/reflect/jvm/internal/impl/a/l$a;->a:Lkotlin/reflect/jvm/internal/impl/e/c;

    invoke-static {p0, v0}, Lkotlin/reflect/jvm/internal/impl/a/l;->a(Lkotlin/reflect/jvm/internal/impl/b/h;Lkotlin/reflect/jvm/internal/impl/e/c;)Z

    move-result v0

    return v0
.end method

.method public static c(Lkotlin/reflect/jvm/internal/impl/k/r;)Z
    .locals 5

    .prologue
    if-nez p0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "type"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "kotlin/reflect/jvm/internal/impl/builtins/KotlinBuiltIns"

    aput-object v4, v2, v3

    const/4 v3, 0x2

    const-string v4, "isArray"

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 868
    :cond_0
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/a/l;->h:Lkotlin/reflect/jvm/internal/impl/a/l$a;

    iget-object v0, v0, Lkotlin/reflect/jvm/internal/impl/a/l$a;->h:Lkotlin/reflect/jvm/internal/impl/e/c;

    invoke-static {p0, v0}, Lkotlin/reflect/jvm/internal/impl/a/l;->a(Lkotlin/reflect/jvm/internal/impl/k/r;Lkotlin/reflect/jvm/internal/impl/e/c;)Z

    move-result v0

    return v0
.end method

.method private d(Lkotlin/reflect/jvm/internal/impl/a/m;)Lkotlin/reflect/jvm/internal/impl/b/e;
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "type"

    aput-object v3, v2, v4

    const-string v3, "kotlin/reflect/jvm/internal/impl/builtins/KotlinBuiltIns"

    aput-object v3, v2, v5

    const-string v3, "getPrimitiveClassDescriptor"

    aput-object v3, v2, v6

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 470
    :cond_0
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/a/m;->a()Lkotlin/reflect/jvm/internal/impl/e/f;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/e/f;->a()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lkotlin/reflect/jvm/internal/impl/a/l;->b(Ljava/lang/String;)Lkotlin/reflect/jvm/internal/impl/b/e;

    move-result-object v0

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "@NotNull method %s.%s must not return null"

    new-array v2, v6, [Ljava/lang/Object;

    const-string v3, "kotlin/reflect/jvm/internal/impl/builtins/KotlinBuiltIns"

    aput-object v3, v2, v4

    const-string v3, "getPrimitiveClassDescriptor"

    aput-object v3, v2, v5

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    return-object v0
.end method

.method private d(Ljava/lang/String;)Lkotlin/reflect/jvm/internal/impl/k/v;
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "classSimpleName"

    aput-object v3, v2, v4

    const-string v3, "kotlin/reflect/jvm/internal/impl/builtins/KotlinBuiltIns"

    aput-object v3, v2, v5

    const-string v3, "getBuiltInTypeByClassName"

    aput-object v3, v2, v6

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 718
    :cond_0
    invoke-direct {p0, p1}, Lkotlin/reflect/jvm/internal/impl/a/l;->b(Ljava/lang/String;)Lkotlin/reflect/jvm/internal/impl/b/e;

    move-result-object v0

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/b/e;->E_()Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v0

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "@NotNull method %s.%s must not return null"

    new-array v2, v6, [Ljava/lang/Object;

    const-string v3, "kotlin/reflect/jvm/internal/impl/builtins/KotlinBuiltIns"

    aput-object v3, v2, v4

    const-string v3, "getBuiltInTypeByClassName"

    aput-object v3, v2, v5

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    return-object v0
.end method

.method public static d(Lkotlin/reflect/jvm/internal/impl/b/e;)Z
    .locals 5

    .prologue
    if-nez p0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "descriptor"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "kotlin/reflect/jvm/internal/impl/builtins/KotlinBuiltIns"

    aput-object v4, v2, v3

    const/4 v3, 0x2

    const-string v4, "isKClass"

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1051
    :cond_0
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/a/l;->h:Lkotlin/reflect/jvm/internal/impl/a/l$a;

    iget-object v0, v0, Lkotlin/reflect/jvm/internal/impl/a/l$a;->aa:Lkotlin/reflect/jvm/internal/impl/e/c;

    invoke-static {p0, v0}, Lkotlin/reflect/jvm/internal/impl/a/l;->a(Lkotlin/reflect/jvm/internal/impl/b/h;Lkotlin/reflect/jvm/internal/impl/e/c;)Z

    move-result v0

    return v0
.end method

.method public static d(Lkotlin/reflect/jvm/internal/impl/k/r;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    if-nez p0, :cond_0

    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/Object;

    const-string v5, "type"

    aput-object v5, v4, v1

    const-string v1, "kotlin/reflect/jvm/internal/impl/builtins/KotlinBuiltIns"

    aput-object v1, v4, v0

    const/4 v0, 0x2

    const-string v1, "isPrimitiveArray"

    aput-object v1, v4, v0

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 876
    :cond_0
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/k/r;->i()Lkotlin/reflect/jvm/internal/impl/k/ad;

    move-result-object v2

    invoke-interface {v2}, Lkotlin/reflect/jvm/internal/impl/k/ad;->d()Lkotlin/reflect/jvm/internal/impl/b/h;

    move-result-object v2

    .line 877
    if-eqz v2, :cond_1

    invoke-static {v2}, Lkotlin/reflect/jvm/internal/impl/h/c;->d(Lkotlin/reflect/jvm/internal/impl/b/m;)Lkotlin/reflect/jvm/internal/impl/e/c;

    move-result-object v2

    invoke-static {v2}, Lkotlin/reflect/jvm/internal/impl/a/l;->c(Lkotlin/reflect/jvm/internal/impl/e/c;)Lkotlin/reflect/jvm/internal/impl/a/m;

    move-result-object v2

    if-eqz v2, :cond_1

    :goto_0
    return v0

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public static e(Lkotlin/reflect/jvm/internal/impl/k/r;)Z
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    if-nez p0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v3, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/Object;

    const-string v5, "type"

    aput-object v5, v4, v2

    const-string v2, "kotlin/reflect/jvm/internal/impl/builtins/KotlinBuiltIns"

    aput-object v2, v4, v1

    const/4 v1, 0x2

    const-string v2, "isPrimitiveType"

    aput-object v2, v4, v1

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 881
    :cond_0
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/k/r;->i()Lkotlin/reflect/jvm/internal/impl/k/ad;

    move-result-object v0

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/k/ad;->d()Lkotlin/reflect/jvm/internal/impl/b/h;

    move-result-object v0

    .line 882
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/k/r;->c()Z

    move-result v3

    if-nez v3, :cond_1

    instance-of v3, v0, Lkotlin/reflect/jvm/internal/impl/b/e;

    if-eqz v3, :cond_1

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/e;

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/a/l;->b(Lkotlin/reflect/jvm/internal/impl/b/e;)Z

    move-result v0

    if-eqz v0, :cond_1

    move v0, v1

    :goto_0
    return v0

    :cond_1
    move v0, v2

    goto :goto_0
.end method

.method public static f(Lkotlin/reflect/jvm/internal/impl/k/r;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    if-nez p0, :cond_0

    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/Object;

    const-string v5, "type"

    aput-object v5, v4, v1

    const-string v1, "kotlin/reflect/jvm/internal/impl/builtins/KotlinBuiltIns"

    aput-object v1, v4, v0

    const/4 v0, 0x2

    const-string v1, "isNothing"

    aput-object v1, v4, v0

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 977
    :cond_0
    invoke-static {p0}, Lkotlin/reflect/jvm/internal/impl/a/l;->g(Lkotlin/reflect/jvm/internal/impl/k/r;)Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/k/r;->c()Z

    move-result v2

    if-nez v2, :cond_1

    :goto_0
    return v0

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public static g(Lkotlin/reflect/jvm/internal/impl/k/r;)Z
    .locals 5

    .prologue
    if-nez p0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "type"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "kotlin/reflect/jvm/internal/impl/builtins/KotlinBuiltIns"

    aput-object v4, v2, v3

    const/4 v3, 0x2

    const-string v4, "isNothingOrNullableNothing"

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 987
    :cond_0
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/a/l;->h:Lkotlin/reflect/jvm/internal/impl/a/l$a;

    iget-object v0, v0, Lkotlin/reflect/jvm/internal/impl/a/l$a;->b:Lkotlin/reflect/jvm/internal/impl/e/c;

    invoke-static {p0, v0}, Lkotlin/reflect/jvm/internal/impl/a/l;->a(Lkotlin/reflect/jvm/internal/impl/k/r;Lkotlin/reflect/jvm/internal/impl/e/c;)Z

    move-result v0

    return v0
.end method

.method public static h(Lkotlin/reflect/jvm/internal/impl/k/r;)Z
    .locals 5

    .prologue
    if-nez p0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "type"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "kotlin/reflect/jvm/internal/impl/builtins/KotlinBuiltIns"

    aput-object v4, v2, v3

    const/4 v3, 0x2

    const-string v4, "isAnyOrNullableAny"

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 991
    :cond_0
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/a/l;->h:Lkotlin/reflect/jvm/internal/impl/a/l$a;

    iget-object v0, v0, Lkotlin/reflect/jvm/internal/impl/a/l$a;->a:Lkotlin/reflect/jvm/internal/impl/e/c;

    invoke-static {p0, v0}, Lkotlin/reflect/jvm/internal/impl/a/l;->a(Lkotlin/reflect/jvm/internal/impl/k/r;Lkotlin/reflect/jvm/internal/impl/e/c;)Z

    move-result v0

    return v0
.end method

.method public static i(Lkotlin/reflect/jvm/internal/impl/k/r;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    if-nez p0, :cond_0

    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/Object;

    const-string v5, "type"

    aput-object v5, v4, v1

    const-string v1, "kotlin/reflect/jvm/internal/impl/builtins/KotlinBuiltIns"

    aput-object v1, v4, v0

    const/4 v0, 0x2

    const-string v1, "isNullableAny"

    aput-object v1, v4, v0

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 995
    :cond_0
    invoke-static {p0}, Lkotlin/reflect/jvm/internal/impl/a/l;->h(Lkotlin/reflect/jvm/internal/impl/k/r;)Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/k/r;->c()Z

    move-result v2

    if-eqz v2, :cond_1

    :goto_0
    return v0

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public static j(Lkotlin/reflect/jvm/internal/impl/k/r;)Z
    .locals 5

    .prologue
    if-nez p0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "type"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "kotlin/reflect/jvm/internal/impl/builtins/KotlinBuiltIns"

    aput-object v4, v2, v3

    const/4 v3, 0x2

    const-string v4, "isDefaultBound"

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 999
    :cond_0
    invoke-static {p0}, Lkotlin/reflect/jvm/internal/impl/a/l;->i(Lkotlin/reflect/jvm/internal/impl/k/r;)Z

    move-result v0

    return v0
.end method

.method public static k(Lkotlin/reflect/jvm/internal/impl/k/r;)Z
    .locals 5

    .prologue
    if-nez p0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "type"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "kotlin/reflect/jvm/internal/impl/builtins/KotlinBuiltIns"

    aput-object v4, v2, v3

    const/4 v3, 0x2

    const-string v4, "isUnit"

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1003
    :cond_0
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/a/l;->h:Lkotlin/reflect/jvm/internal/impl/a/l$a;

    iget-object v0, v0, Lkotlin/reflect/jvm/internal/impl/a/l$a;->e:Lkotlin/reflect/jvm/internal/impl/e/c;

    invoke-static {p0, v0}, Lkotlin/reflect/jvm/internal/impl/a/l;->b(Lkotlin/reflect/jvm/internal/impl/k/r;Lkotlin/reflect/jvm/internal/impl/e/c;)Z

    move-result v0

    return v0
.end method

.method public static l(Lkotlin/reflect/jvm/internal/impl/k/r;)Z
    .locals 1

    .prologue
    .line 1019
    if-eqz p0, :cond_0

    sget-object v0, Lkotlin/reflect/jvm/internal/impl/a/l;->h:Lkotlin/reflect/jvm/internal/impl/a/l$a;

    iget-object v0, v0, Lkotlin/reflect/jvm/internal/impl/a/l$a;->g:Lkotlin/reflect/jvm/internal/impl/e/c;

    invoke-static {p0, v0}, Lkotlin/reflect/jvm/internal/impl/a/l;->b(Lkotlin/reflect/jvm/internal/impl/k/r;Lkotlin/reflect/jvm/internal/impl/e/c;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public A()Lkotlin/reflect/jvm/internal/impl/k/v;
    .locals 5

    .prologue
    .line 753
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/a/m;->c:Lkotlin/reflect/jvm/internal/impl/a/m;

    invoke-virtual {p0, v0}, Lkotlin/reflect/jvm/internal/impl/a/l;->a(Lkotlin/reflect/jvm/internal/impl/a/m;)Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "@NotNull method %s.%s must not return null"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "kotlin/reflect/jvm/internal/impl/builtins/KotlinBuiltIns"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "getByteType"

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    return-object v0
.end method

.method public B()Lkotlin/reflect/jvm/internal/impl/k/v;
    .locals 5

    .prologue
    .line 758
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/a/m;->d:Lkotlin/reflect/jvm/internal/impl/a/m;

    invoke-virtual {p0, v0}, Lkotlin/reflect/jvm/internal/impl/a/l;->a(Lkotlin/reflect/jvm/internal/impl/a/m;)Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "@NotNull method %s.%s must not return null"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "kotlin/reflect/jvm/internal/impl/builtins/KotlinBuiltIns"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "getShortType"

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    return-object v0
.end method

.method public C()Lkotlin/reflect/jvm/internal/impl/k/v;
    .locals 5

    .prologue
    .line 763
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/a/m;->e:Lkotlin/reflect/jvm/internal/impl/a/m;

    invoke-virtual {p0, v0}, Lkotlin/reflect/jvm/internal/impl/a/l;->a(Lkotlin/reflect/jvm/internal/impl/a/m;)Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "@NotNull method %s.%s must not return null"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "kotlin/reflect/jvm/internal/impl/builtins/KotlinBuiltIns"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "getIntType"

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    return-object v0
.end method

.method public D()Lkotlin/reflect/jvm/internal/impl/k/v;
    .locals 5

    .prologue
    .line 768
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/a/m;->g:Lkotlin/reflect/jvm/internal/impl/a/m;

    invoke-virtual {p0, v0}, Lkotlin/reflect/jvm/internal/impl/a/l;->a(Lkotlin/reflect/jvm/internal/impl/a/m;)Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "@NotNull method %s.%s must not return null"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "kotlin/reflect/jvm/internal/impl/builtins/KotlinBuiltIns"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "getLongType"

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    return-object v0
.end method

.method public E()Lkotlin/reflect/jvm/internal/impl/k/v;
    .locals 5

    .prologue
    .line 773
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/a/m;->f:Lkotlin/reflect/jvm/internal/impl/a/m;

    invoke-virtual {p0, v0}, Lkotlin/reflect/jvm/internal/impl/a/l;->a(Lkotlin/reflect/jvm/internal/impl/a/m;)Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "@NotNull method %s.%s must not return null"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "kotlin/reflect/jvm/internal/impl/builtins/KotlinBuiltIns"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "getFloatType"

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    return-object v0
.end method

.method public F()Lkotlin/reflect/jvm/internal/impl/k/v;
    .locals 5

    .prologue
    .line 778
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/a/m;->h:Lkotlin/reflect/jvm/internal/impl/a/m;

    invoke-virtual {p0, v0}, Lkotlin/reflect/jvm/internal/impl/a/l;->a(Lkotlin/reflect/jvm/internal/impl/a/m;)Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "@NotNull method %s.%s must not return null"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "kotlin/reflect/jvm/internal/impl/builtins/KotlinBuiltIns"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "getDoubleType"

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    return-object v0
.end method

.method public G()Lkotlin/reflect/jvm/internal/impl/k/v;
    .locals 5

    .prologue
    .line 783
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/a/m;->b:Lkotlin/reflect/jvm/internal/impl/a/m;

    invoke-virtual {p0, v0}, Lkotlin/reflect/jvm/internal/impl/a/l;->a(Lkotlin/reflect/jvm/internal/impl/a/m;)Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "@NotNull method %s.%s must not return null"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "kotlin/reflect/jvm/internal/impl/builtins/KotlinBuiltIns"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "getCharType"

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    return-object v0
.end method

.method public H()Lkotlin/reflect/jvm/internal/impl/k/v;
    .locals 5

    .prologue
    .line 788
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/a/m;->a:Lkotlin/reflect/jvm/internal/impl/a/m;

    invoke-virtual {p0, v0}, Lkotlin/reflect/jvm/internal/impl/a/l;->a(Lkotlin/reflect/jvm/internal/impl/a/m;)Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "@NotNull method %s.%s must not return null"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "kotlin/reflect/jvm/internal/impl/builtins/KotlinBuiltIns"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "getBooleanType"

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    return-object v0
.end method

.method public I()Lkotlin/reflect/jvm/internal/impl/k/v;
    .locals 5

    .prologue
    .line 793
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/a/l;->n()Lkotlin/reflect/jvm/internal/impl/b/e;

    move-result-object v0

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/b/e;->E_()Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "@NotNull method %s.%s must not return null"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "kotlin/reflect/jvm/internal/impl/builtins/KotlinBuiltIns"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "getUnitType"

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    return-object v0
.end method

.method public J()Lkotlin/reflect/jvm/internal/impl/k/v;
    .locals 5

    .prologue
    .line 798
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/a/l;->t()Lkotlin/reflect/jvm/internal/impl/b/e;

    move-result-object v0

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/b/e;->E_()Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "@NotNull method %s.%s must not return null"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "kotlin/reflect/jvm/internal/impl/builtins/KotlinBuiltIns"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "getStringType"

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    return-object v0
.end method

.method public a(Ljava/lang/String;)Lkotlin/reflect/jvm/internal/impl/b/e;
    .locals 5

    .prologue
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "level"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "kotlin/reflect/jvm/internal/impl/builtins/KotlinBuiltIns"

    aput-object v4, v2, v3

    const/4 v3, 0x2

    const-string v4, "getDeprecationLevelEnumEntry"

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 574
    :cond_0
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/a/l;->h:Lkotlin/reflect/jvm/internal/impl/a/l$a;

    iget-object v0, v0, Lkotlin/reflect/jvm/internal/impl/a/l$a;->y:Lkotlin/reflect/jvm/internal/impl/e/b;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/e/b;->e()Lkotlin/reflect/jvm/internal/impl/e/f;

    move-result-object v0

    invoke-virtual {p0, v0}, Lkotlin/reflect/jvm/internal/impl/a/l;->a(Lkotlin/reflect/jvm/internal/impl/e/f;)Lkotlin/reflect/jvm/internal/impl/b/e;

    move-result-object v0

    invoke-static {v0, p1}, Lkotlin/reflect/jvm/internal/impl/a/l;->a(Lkotlin/reflect/jvm/internal/impl/b/e;Ljava/lang/String;)Lkotlin/reflect/jvm/internal/impl/b/e;

    move-result-object v0

    return-object v0
.end method

.method public a(Lkotlin/reflect/jvm/internal/impl/b/a/m;)Lkotlin/reflect/jvm/internal/impl/b/e;
    .locals 5

    .prologue
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "retention"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "kotlin/reflect/jvm/internal/impl/builtins/KotlinBuiltIns"

    aput-object v4, v2, v3

    const/4 v3, 0x2

    const-string v4, "getAnnotationRetentionEnumEntry"

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 604
    :cond_0
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/a/l;->h:Lkotlin/reflect/jvm/internal/impl/a/l$a;

    iget-object v0, v0, Lkotlin/reflect/jvm/internal/impl/a/l$a;->E:Lkotlin/reflect/jvm/internal/impl/e/b;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/e/b;->e()Lkotlin/reflect/jvm/internal/impl/e/f;

    move-result-object v0

    invoke-direct {p0, v0}, Lkotlin/reflect/jvm/internal/impl/a/l;->b(Lkotlin/reflect/jvm/internal/impl/e/f;)Lkotlin/reflect/jvm/internal/impl/b/e;

    move-result-object v0

    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/b/a/m;->name()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/reflect/jvm/internal/impl/a/l;->a(Lkotlin/reflect/jvm/internal/impl/b/e;Ljava/lang/String;)Lkotlin/reflect/jvm/internal/impl/b/e;

    move-result-object v0

    return-object v0
.end method

.method public a(Lkotlin/reflect/jvm/internal/impl/b/a/n;)Lkotlin/reflect/jvm/internal/impl/b/e;
    .locals 5

    .prologue
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "target"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "kotlin/reflect/jvm/internal/impl/builtins/KotlinBuiltIns"

    aput-object v4, v2, v3

    const/4 v3, 0x2

    const-string v4, "getAnnotationTargetEnumEntry"

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 599
    :cond_0
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/a/l;->h:Lkotlin/reflect/jvm/internal/impl/a/l$a;

    iget-object v0, v0, Lkotlin/reflect/jvm/internal/impl/a/l$a;->D:Lkotlin/reflect/jvm/internal/impl/e/b;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/e/b;->e()Lkotlin/reflect/jvm/internal/impl/e/f;

    move-result-object v0

    invoke-direct {p0, v0}, Lkotlin/reflect/jvm/internal/impl/a/l;->b(Lkotlin/reflect/jvm/internal/impl/e/f;)Lkotlin/reflect/jvm/internal/impl/b/e;

    move-result-object v0

    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/b/a/n;->name()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/reflect/jvm/internal/impl/a/l;->a(Lkotlin/reflect/jvm/internal/impl/b/e;Ljava/lang/String;)Lkotlin/reflect/jvm/internal/impl/b/e;

    move-result-object v0

    return-object v0
.end method

.method public a(Lkotlin/reflect/jvm/internal/impl/e/b;)Lkotlin/reflect/jvm/internal/impl/b/e;
    .locals 5

    .prologue
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "fqName"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "kotlin/reflect/jvm/internal/impl/builtins/KotlinBuiltIns"

    aput-object v4, v2, v3

    const/4 v3, 0x2

    const-string v4, "getBuiltInClassByFqNameNullable"

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 427
    :cond_0
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/a/l;->k:Lkotlin/reflect/jvm/internal/impl/b/b/u;

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/c/a/d;->d:Lkotlin/reflect/jvm/internal/impl/c/a/d;

    invoke-static {v0, p1, v1}, Lkotlin/reflect/jvm/internal/impl/b/r;->a(Lkotlin/reflect/jvm/internal/impl/b/w;Lkotlin/reflect/jvm/internal/impl/e/b;Lkotlin/reflect/jvm/internal/impl/c/a/b;)Lkotlin/reflect/jvm/internal/impl/b/e;

    move-result-object v0

    return-object v0
.end method

.method public a(Lkotlin/reflect/jvm/internal/impl/e/f;)Lkotlin/reflect/jvm/internal/impl/b/e;
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "simpleName"

    aput-object v3, v2, v4

    const-string v3, "kotlin/reflect/jvm/internal/impl/builtins/KotlinBuiltIns"

    aput-object v3, v2, v5

    const-string v3, "getBuiltInClassByName"

    aput-object v3, v2, v6

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 408
    :cond_0
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/a/l;->i()Lkotlin/reflect/jvm/internal/impl/b/y;

    move-result-object v0

    invoke-static {p1, v0}, Lkotlin/reflect/jvm/internal/impl/a/l;->a(Lkotlin/reflect/jvm/internal/impl/e/f;Lkotlin/reflect/jvm/internal/impl/b/y;)Lkotlin/reflect/jvm/internal/impl/b/e;

    move-result-object v0

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "@NotNull method %s.%s must not return null"

    new-array v2, v6, [Ljava/lang/Object;

    const-string v3, "kotlin/reflect/jvm/internal/impl/builtins/KotlinBuiltIns"

    aput-object v3, v2, v4

    const-string v3, "getBuiltInClassByName"

    aput-object v3, v2, v5

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    return-object v0
.end method

.method public a(Lkotlin/reflect/jvm/internal/impl/k/r;)Lkotlin/reflect/jvm/internal/impl/k/r;
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "arrayType"

    aput-object v3, v2, v4

    const-string v3, "kotlin/reflect/jvm/internal/impl/builtins/KotlinBuiltIns"

    aput-object v3, v2, v5

    const-string v3, "getArrayElementType"

    aput-object v3, v2, v6

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 808
    :cond_0
    invoke-static {p1}, Lkotlin/reflect/jvm/internal/impl/a/l;->c(Lkotlin/reflect/jvm/internal/impl/k/r;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 809
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/k/r;->a()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-eq v0, v5, :cond_1

    .line 810
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    .line 812
    :cond_1
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/k/r;->a()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/ah;

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/k/ah;->c()Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v0

    if-nez v0, :cond_4

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "@NotNull method %s.%s must not return null"

    new-array v2, v6, [Ljava/lang/Object;

    const-string v3, "kotlin/reflect/jvm/internal/impl/builtins/KotlinBuiltIns"

    aput-object v3, v2, v4

    const-string v3, "getArrayElementType"

    aput-object v3, v2, v5

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 815
    :cond_2
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/a/l;->l:Lkotlin/reflect/jvm/internal/impl/j/f;

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/j/f;->v_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/a/l$c;

    iget-object v0, v0, Lkotlin/reflect/jvm/internal/impl/a/l$c;->c:Ljava/util/Map;

    invoke-static {p1}, Lkotlin/reflect/jvm/internal/impl/k/an;->c(Lkotlin/reflect/jvm/internal/impl/k/r;)Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/r;

    .line 816
    if-nez v0, :cond_3

    .line 817
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "not array: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 819
    :cond_3
    if-nez v0, :cond_4

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "@NotNull method %s.%s must not return null"

    new-array v2, v6, [Ljava/lang/Object;

    const-string v3, "kotlin/reflect/jvm/internal/impl/builtins/KotlinBuiltIns"

    aput-object v3, v2, v4

    const-string v3, "getArrayElementType"

    aput-object v3, v2, v5

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_4
    return-object v0
.end method

.method public a(Lkotlin/reflect/jvm/internal/impl/a/m;)Lkotlin/reflect/jvm/internal/impl/k/v;
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "type"

    aput-object v3, v2, v4

    const-string v3, "kotlin/reflect/jvm/internal/impl/builtins/KotlinBuiltIns"

    aput-object v3, v2, v5

    const-string v3, "getPrimitiveKotlinType"

    aput-object v3, v2, v6

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 748
    :cond_0
    invoke-direct {p0, p1}, Lkotlin/reflect/jvm/internal/impl/a/l;->d(Lkotlin/reflect/jvm/internal/impl/a/m;)Lkotlin/reflect/jvm/internal/impl/b/e;

    move-result-object v0

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/b/e;->E_()Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v0

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "@NotNull method %s.%s must not return null"

    new-array v2, v6, [Ljava/lang/Object;

    const-string v3, "kotlin/reflect/jvm/internal/impl/builtins/KotlinBuiltIns"

    aput-object v3, v2, v4

    const-string v3, "getPrimitiveKotlinType"

    aput-object v3, v2, v5

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    return-object v0
.end method

.method public a(Lkotlin/reflect/jvm/internal/impl/k/ap;Lkotlin/reflect/jvm/internal/impl/k/r;)Lkotlin/reflect/jvm/internal/impl/k/v;
    .locals 7

    .prologue
    const/4 v2, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "projectionType"

    aput-object v3, v2, v4

    const-string v3, "kotlin/reflect/jvm/internal/impl/builtins/KotlinBuiltIns"

    aput-object v3, v2, v5

    const-string v3, "getArrayType"

    aput-object v3, v2, v6

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    if-nez p2, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "argument"

    aput-object v3, v2, v4

    const-string v3, "kotlin/reflect/jvm/internal/impl/builtins/KotlinBuiltIns"

    aput-object v3, v2, v5

    const-string v3, "getArrayType"

    aput-object v3, v2, v6

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 851
    :cond_1
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/k/aj;

    invoke-direct {v0, p1, p2}, Lkotlin/reflect/jvm/internal/impl/k/aj;-><init>(Lkotlin/reflect/jvm/internal/impl/k/ap;Lkotlin/reflect/jvm/internal/impl/k/r;)V

    invoke-static {v0}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    .line 852
    sget-object v1, Lkotlin/reflect/jvm/internal/impl/b/a/h;->a:Lkotlin/reflect/jvm/internal/impl/b/a/h$a;

    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/impl/b/a/h$a;->a()Lkotlin/reflect/jvm/internal/impl/b/a/h;

    move-result-object v1

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/a/l;->l()Lkotlin/reflect/jvm/internal/impl/b/e;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lkotlin/reflect/jvm/internal/impl/k/s;->a(Lkotlin/reflect/jvm/internal/impl/b/a/h;Lkotlin/reflect/jvm/internal/impl/b/e;Ljava/util/List;)Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v0

    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "@NotNull method %s.%s must not return null"

    new-array v2, v6, [Ljava/lang/Object;

    const-string v3, "kotlin/reflect/jvm/internal/impl/builtins/KotlinBuiltIns"

    aput-object v3, v2, v4

    const-string v3, "getArrayType"

    aput-object v3, v2, v5

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    return-object v0
.end method

.method public b(Lkotlin/reflect/jvm/internal/impl/e/b;)Lkotlin/reflect/jvm/internal/impl/b/e;
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "fqName"

    aput-object v3, v2, v4

    const-string v3, "kotlin/reflect/jvm/internal/impl/builtins/KotlinBuiltIns"

    aput-object v3, v2, v5

    const-string v3, "getBuiltInClassByFqName"

    aput-object v3, v2, v6

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 432
    :cond_0
    invoke-virtual {p0, p1}, Lkotlin/reflect/jvm/internal/impl/a/l;->a(Lkotlin/reflect/jvm/internal/impl/e/b;)Lkotlin/reflect/jvm/internal/impl/b/e;

    move-result-object v0

    .line 433
    sget-boolean v1, Lkotlin/reflect/jvm/internal/impl/a/l;->j:Z

    if-nez v1, :cond_1

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Can\'t find built-in class "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 434
    :cond_1
    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "@NotNull method %s.%s must not return null"

    new-array v2, v6, [Ljava/lang/Object;

    const-string v3, "kotlin/reflect/jvm/internal/impl/builtins/KotlinBuiltIns"

    aput-object v3, v2, v4

    const-string v3, "getBuiltInClassByFqName"

    aput-object v3, v2, v5

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    return-object v0
.end method

.method public b(Lkotlin/reflect/jvm/internal/impl/a/m;)Lkotlin/reflect/jvm/internal/impl/k/v;
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "primitiveType"

    aput-object v3, v2, v4

    const-string v3, "kotlin/reflect/jvm/internal/impl/builtins/KotlinBuiltIns"

    aput-object v3, v2, v5

    const-string v3, "getPrimitiveArrayKotlinType"

    aput-object v3, v2, v6

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 824
    :cond_0
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/a/l;->l:Lkotlin/reflect/jvm/internal/impl/j/f;

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/j/f;->v_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/a/l$c;

    iget-object v0, v0, Lkotlin/reflect/jvm/internal/impl/a/l$c;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/v;

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "@NotNull method %s.%s must not return null"

    new-array v2, v6, [Ljava/lang/Object;

    const-string v3, "kotlin/reflect/jvm/internal/impl/builtins/KotlinBuiltIns"

    aput-object v3, v2, v4

    const-string v3, "getPrimitiveArrayKotlinType"

    aput-object v3, v2, v5

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    return-object v0
.end method

.method public b(Lkotlin/reflect/jvm/internal/impl/k/r;)Lkotlin/reflect/jvm/internal/impl/k/v;
    .locals 5

    .prologue
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "kotlinType"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "kotlin/reflect/jvm/internal/impl/builtins/KotlinBuiltIns"

    aput-object v4, v2, v3

    const/4 v3, 0x2

    const-string v4, "getPrimitiveArrayKotlinTypeByPrimitiveKotlinType"

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 832
    :cond_0
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/a/l;->l:Lkotlin/reflect/jvm/internal/impl/j/f;

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/j/f;->v_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/a/l$c;

    iget-object v0, v0, Lkotlin/reflect/jvm/internal/impl/a/l$c;->b:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/v;

    return-object v0
.end method

.method public c(I)Lkotlin/reflect/jvm/internal/impl/b/e;
    .locals 5

    .prologue
    .line 545
    invoke-static {p1}, Lkotlin/reflect/jvm/internal/impl/a/l;->a(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lkotlin/reflect/jvm/internal/impl/a/l;->b(Ljava/lang/String;)Lkotlin/reflect/jvm/internal/impl/b/e;

    move-result-object v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "@NotNull method %s.%s must not return null"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "kotlin/reflect/jvm/internal/impl/builtins/KotlinBuiltIns"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "getFunction"

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    return-object v0
.end method

.method protected c()V
    .locals 7

    .prologue
    .line 137
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/b/b/u;

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/a/l;->i:Lkotlin/reflect/jvm/internal/impl/e/f;

    iget-object v2, p0, Lkotlin/reflect/jvm/internal/impl/a/l;->o:Lkotlin/reflect/jvm/internal/impl/j/i;

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, p0, v3}, Lkotlin/reflect/jvm/internal/impl/b/b/u;-><init>(Lkotlin/reflect/jvm/internal/impl/e/f;Lkotlin/reflect/jvm/internal/impl/j/i;Lkotlin/reflect/jvm/internal/impl/a/l;Lkotlin/reflect/jvm/internal/impl/h/f;)V

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/a/l;->k:Lkotlin/reflect/jvm/internal/impl/b/b/u;

    .line 138
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/a/l;->o:Lkotlin/reflect/jvm/internal/impl/j/i;

    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/a/l;->k:Lkotlin/reflect/jvm/internal/impl/b/b/u;

    sget-object v2, Lkotlin/reflect/jvm/internal/impl/a/l;->g:Ljava/util/Set;

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/a/l;->f()Ljava/lang/Iterable;

    move-result-object v3

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/a/l;->e()Lkotlin/reflect/jvm/internal/impl/i/b/ac;

    move-result-object v4

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/a/l;->d()Lkotlin/reflect/jvm/internal/impl/i/b/a;

    move-result-object v5

    new-instance v6, Lkotlin/reflect/jvm/internal/impl/a/l$4;

    invoke-direct {v6, p0}, Lkotlin/reflect/jvm/internal/impl/a/l$4;-><init>(Lkotlin/reflect/jvm/internal/impl/a/l;)V

    invoke-static/range {v0 .. v6}, Lkotlin/reflect/jvm/internal/impl/a/f;->a(Lkotlin/reflect/jvm/internal/impl/j/i;Lkotlin/reflect/jvm/internal/impl/b/w;Ljava/util/Set;Ljava/lang/Iterable;Lkotlin/reflect/jvm/internal/impl/i/b/ac;Lkotlin/reflect/jvm/internal/impl/i/b/a;Lkotlin/d/a/b;)Lkotlin/reflect/jvm/internal/impl/b/z;

    move-result-object v0

    .line 152
    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/a/l;->k:Lkotlin/reflect/jvm/internal/impl/b/b/u;

    invoke-virtual {v1, v0}, Lkotlin/reflect/jvm/internal/impl/b/b/u;->a(Lkotlin/reflect/jvm/internal/impl/b/z;)V

    .line 153
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/a/l;->k:Lkotlin/reflect/jvm/internal/impl/b/b/u;

    const/4 v1, 0x1

    new-array v1, v1, [Lkotlin/reflect/jvm/internal/impl/b/b/u;

    const/4 v2, 0x0

    iget-object v3, p0, Lkotlin/reflect/jvm/internal/impl/a/l;->k:Lkotlin/reflect/jvm/internal/impl/b/b/u;

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lkotlin/reflect/jvm/internal/impl/b/b/u;->a([Lkotlin/reflect/jvm/internal/impl/b/b/u;)V

    .line 154
    return-void
.end method

.method public d(I)Lkotlin/reflect/jvm/internal/impl/b/e;
    .locals 5

    .prologue
    .line 551
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/a/l;->n:Lkotlin/reflect/jvm/internal/impl/j/c;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Lkotlin/reflect/jvm/internal/impl/j/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/e;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "@NotNull method %s.%s must not return null"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "kotlin/reflect/jvm/internal/impl/builtins/KotlinBuiltIns"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "getSuspendFunction"

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    return-object v0
.end method

.method protected d()Lkotlin/reflect/jvm/internal/impl/i/b/a;
    .locals 5

    .prologue
    .line 173
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/i/b/a$a;->a:Lkotlin/reflect/jvm/internal/impl/i/b/a$a;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "@NotNull method %s.%s must not return null"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "kotlin/reflect/jvm/internal/impl/builtins/KotlinBuiltIns"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "getAdditionalClassPartsProvider"

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    return-object v0
.end method

.method protected e()Lkotlin/reflect/jvm/internal/impl/i/b/ac;
    .locals 5

    .prologue
    .line 178
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/i/b/ac$b;->a:Lkotlin/reflect/jvm/internal/impl/i/b/ac$b;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "@NotNull method %s.%s must not return null"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "kotlin/reflect/jvm/internal/impl/builtins/KotlinBuiltIns"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "getPlatformDependentDeclarationFilter"

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    return-object v0
.end method

.method protected f()Ljava/lang/Iterable;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Iterable",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/i/b/i;",
            ">;"
        }
    .end annotation

    .prologue
    .line 183
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/a/a/a;

    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/a/l;->o:Lkotlin/reflect/jvm/internal/impl/j/i;

    iget-object v2, p0, Lkotlin/reflect/jvm/internal/impl/a/l;->k:Lkotlin/reflect/jvm/internal/impl/b/b/u;

    invoke-direct {v0, v1, v2}, Lkotlin/reflect/jvm/internal/impl/a/a/a;-><init>(Lkotlin/reflect/jvm/internal/impl/j/i;Lkotlin/reflect/jvm/internal/impl/b/w;)V

    invoke-static {v0}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "@NotNull method %s.%s must not return null"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "kotlin/reflect/jvm/internal/impl/builtins/KotlinBuiltIns"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "getClassDescriptorFactories"

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    return-object v0
.end method

.method protected g()Lkotlin/reflect/jvm/internal/impl/j/i;
    .locals 5

    .prologue
    .line 224
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/a/l;->o:Lkotlin/reflect/jvm/internal/impl/j/i;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "@NotNull method %s.%s must not return null"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "kotlin/reflect/jvm/internal/impl/builtins/KotlinBuiltIns"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "getStorageManager"

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    return-object v0
.end method

.method public h()Lkotlin/reflect/jvm/internal/impl/b/b/u;
    .locals 5

    .prologue
    .line 379
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/a/l;->k:Lkotlin/reflect/jvm/internal/impl/b/b/u;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "@NotNull method %s.%s must not return null"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "kotlin/reflect/jvm/internal/impl/builtins/KotlinBuiltIns"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "getBuiltInsModule"

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    return-object v0
.end method

.method public i()Lkotlin/reflect/jvm/internal/impl/b/y;
    .locals 5

    .prologue
    .line 389
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/a/l;->m:Lkotlin/reflect/jvm/internal/impl/j/f;

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/j/f;->v_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/a/l$b;

    iget-object v0, v0, Lkotlin/reflect/jvm/internal/impl/a/l$b;->a:Lkotlin/reflect/jvm/internal/impl/b/y;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "@NotNull method %s.%s must not return null"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "kotlin/reflect/jvm/internal/impl/builtins/KotlinBuiltIns"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "getBuiltInsPackageFragment"

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    return-object v0
.end method

.method public j()Lkotlin/reflect/jvm/internal/impl/b/e;
    .locals 5

    .prologue
    .line 460
    const-string v0, "Any"

    invoke-direct {p0, v0}, Lkotlin/reflect/jvm/internal/impl/a/l;->b(Ljava/lang/String;)Lkotlin/reflect/jvm/internal/impl/b/e;

    move-result-object v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "@NotNull method %s.%s must not return null"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "kotlin/reflect/jvm/internal/impl/builtins/KotlinBuiltIns"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "getAny"

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    return-object v0
.end method

.method public k()Lkotlin/reflect/jvm/internal/impl/b/e;
    .locals 5

    .prologue
    .line 465
    const-string v0, "Nothing"

    invoke-direct {p0, v0}, Lkotlin/reflect/jvm/internal/impl/a/l;->b(Ljava/lang/String;)Lkotlin/reflect/jvm/internal/impl/b/e;

    move-result-object v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "@NotNull method %s.%s must not return null"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "kotlin/reflect/jvm/internal/impl/builtins/KotlinBuiltIns"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "getNothing"

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    return-object v0
.end method

.method public l()Lkotlin/reflect/jvm/internal/impl/b/e;
    .locals 5

    .prologue
    .line 515
    const-string v0, "Array"

    invoke-direct {p0, v0}, Lkotlin/reflect/jvm/internal/impl/a/l;->b(Ljava/lang/String;)Lkotlin/reflect/jvm/internal/impl/b/e;

    move-result-object v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "@NotNull method %s.%s must not return null"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "kotlin/reflect/jvm/internal/impl/builtins/KotlinBuiltIns"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "getArray"

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    return-object v0
.end method

.method public m()Lkotlin/reflect/jvm/internal/impl/b/e;
    .locals 5

    .prologue
    .line 525
    const-string v0, "Number"

    invoke-direct {p0, v0}, Lkotlin/reflect/jvm/internal/impl/a/l;->b(Ljava/lang/String;)Lkotlin/reflect/jvm/internal/impl/b/e;

    move-result-object v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "@NotNull method %s.%s must not return null"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "kotlin/reflect/jvm/internal/impl/builtins/KotlinBuiltIns"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "getNumber"

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    return-object v0
.end method

.method public n()Lkotlin/reflect/jvm/internal/impl/b/e;
    .locals 5

    .prologue
    .line 530
    const-string v0, "Unit"

    invoke-direct {p0, v0}, Lkotlin/reflect/jvm/internal/impl/a/l;->b(Ljava/lang/String;)Lkotlin/reflect/jvm/internal/impl/b/e;

    move-result-object v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "@NotNull method %s.%s must not return null"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "kotlin/reflect/jvm/internal/impl/builtins/KotlinBuiltIns"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "getUnit"

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    return-object v0
.end method

.method public o()Lkotlin/reflect/jvm/internal/impl/b/e;
    .locals 5

    .prologue
    .line 561
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/a/l;->h:Lkotlin/reflect/jvm/internal/impl/a/l$a;

    iget-object v0, v0, Lkotlin/reflect/jvm/internal/impl/a/l$a;->x:Lkotlin/reflect/jvm/internal/impl/e/b;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/e/b;->e()Lkotlin/reflect/jvm/internal/impl/e/f;

    move-result-object v0

    invoke-virtual {p0, v0}, Lkotlin/reflect/jvm/internal/impl/a/l;->a(Lkotlin/reflect/jvm/internal/impl/e/f;)Lkotlin/reflect/jvm/internal/impl/b/e;

    move-result-object v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "@NotNull method %s.%s must not return null"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "kotlin/reflect/jvm/internal/impl/builtins/KotlinBuiltIns"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "getDeprecatedAnnotation"

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    return-object v0
.end method

.method public p()Lkotlin/reflect/jvm/internal/impl/b/e;
    .locals 5

    .prologue
    .line 579
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/a/l;->h:Lkotlin/reflect/jvm/internal/impl/a/l$a;

    iget-object v0, v0, Lkotlin/reflect/jvm/internal/impl/a/l$a;->C:Lkotlin/reflect/jvm/internal/impl/e/b;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/e/b;->e()Lkotlin/reflect/jvm/internal/impl/e/f;

    move-result-object v0

    invoke-direct {p0, v0}, Lkotlin/reflect/jvm/internal/impl/a/l;->b(Lkotlin/reflect/jvm/internal/impl/e/f;)Lkotlin/reflect/jvm/internal/impl/b/e;

    move-result-object v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "@NotNull method %s.%s must not return null"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "kotlin/reflect/jvm/internal/impl/builtins/KotlinBuiltIns"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "getTargetAnnotation"

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    return-object v0
.end method

.method public q()Lkotlin/reflect/jvm/internal/impl/b/e;
    .locals 5

    .prologue
    .line 584
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/a/l;->h:Lkotlin/reflect/jvm/internal/impl/a/l$a;

    iget-object v0, v0, Lkotlin/reflect/jvm/internal/impl/a/l$a;->F:Lkotlin/reflect/jvm/internal/impl/e/b;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/e/b;->e()Lkotlin/reflect/jvm/internal/impl/e/f;

    move-result-object v0

    invoke-direct {p0, v0}, Lkotlin/reflect/jvm/internal/impl/a/l;->b(Lkotlin/reflect/jvm/internal/impl/e/f;)Lkotlin/reflect/jvm/internal/impl/b/e;

    move-result-object v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "@NotNull method %s.%s must not return null"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "kotlin/reflect/jvm/internal/impl/builtins/KotlinBuiltIns"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "getRetentionAnnotation"

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    return-object v0
.end method

.method public r()Lkotlin/reflect/jvm/internal/impl/b/e;
    .locals 5

    .prologue
    .line 589
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/a/l;->h:Lkotlin/reflect/jvm/internal/impl/a/l$a;

    iget-object v0, v0, Lkotlin/reflect/jvm/internal/impl/a/l$a;->G:Lkotlin/reflect/jvm/internal/impl/e/b;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/e/b;->e()Lkotlin/reflect/jvm/internal/impl/e/f;

    move-result-object v0

    invoke-direct {p0, v0}, Lkotlin/reflect/jvm/internal/impl/a/l;->b(Lkotlin/reflect/jvm/internal/impl/e/f;)Lkotlin/reflect/jvm/internal/impl/b/e;

    move-result-object v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "@NotNull method %s.%s must not return null"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "kotlin/reflect/jvm/internal/impl/builtins/KotlinBuiltIns"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "getRepeatableAnnotation"

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    return-object v0
.end method

.method public s()Lkotlin/reflect/jvm/internal/impl/b/e;
    .locals 5

    .prologue
    .line 594
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/a/l;->h:Lkotlin/reflect/jvm/internal/impl/a/l$a;

    iget-object v0, v0, Lkotlin/reflect/jvm/internal/impl/a/l$a;->H:Lkotlin/reflect/jvm/internal/impl/e/b;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/e/b;->e()Lkotlin/reflect/jvm/internal/impl/e/f;

    move-result-object v0

    invoke-direct {p0, v0}, Lkotlin/reflect/jvm/internal/impl/a/l;->b(Lkotlin/reflect/jvm/internal/impl/e/f;)Lkotlin/reflect/jvm/internal/impl/b/e;

    move-result-object v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "@NotNull method %s.%s must not return null"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "kotlin/reflect/jvm/internal/impl/builtins/KotlinBuiltIns"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "getMustBeDocumentedAnnotation"

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    return-object v0
.end method

.method public t()Lkotlin/reflect/jvm/internal/impl/b/e;
    .locals 5

    .prologue
    .line 609
    const-string v0, "String"

    invoke-direct {p0, v0}, Lkotlin/reflect/jvm/internal/impl/a/l;->b(Ljava/lang/String;)Lkotlin/reflect/jvm/internal/impl/b/e;

    move-result-object v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "@NotNull method %s.%s must not return null"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "kotlin/reflect/jvm/internal/impl/builtins/KotlinBuiltIns"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "getString"

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    return-object v0
.end method

.method public u()Lkotlin/reflect/jvm/internal/impl/b/e;
    .locals 5

    .prologue
    .line 654
    const-string v0, "Collection"

    invoke-direct {p0, v0}, Lkotlin/reflect/jvm/internal/impl/a/l;->c(Ljava/lang/String;)Lkotlin/reflect/jvm/internal/impl/b/e;

    move-result-object v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "@NotNull method %s.%s must not return null"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "kotlin/reflect/jvm/internal/impl/builtins/KotlinBuiltIns"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "getCollection"

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    return-object v0
.end method

.method public v()Lkotlin/reflect/jvm/internal/impl/k/v;
    .locals 5

    .prologue
    .line 723
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/a/l;->k()Lkotlin/reflect/jvm/internal/impl/b/e;

    move-result-object v0

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/b/e;->E_()Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "@NotNull method %s.%s must not return null"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "kotlin/reflect/jvm/internal/impl/builtins/KotlinBuiltIns"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "getNothingType"

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    return-object v0
.end method

.method public w()Lkotlin/reflect/jvm/internal/impl/k/v;
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 728
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/a/l;->v()Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v0

    invoke-virtual {v0, v5}, Lkotlin/reflect/jvm/internal/impl/k/v;->a(Z)Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "@NotNull method %s.%s must not return null"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "kotlin/reflect/jvm/internal/impl/builtins/KotlinBuiltIns"

    aput-object v4, v2, v3

    const-string v3, "getNullableNothingType"

    aput-object v3, v2, v5

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    return-object v0
.end method

.method public x()Lkotlin/reflect/jvm/internal/impl/k/v;
    .locals 5

    .prologue
    .line 733
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/a/l;->j()Lkotlin/reflect/jvm/internal/impl/b/e;

    move-result-object v0

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/b/e;->E_()Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "@NotNull method %s.%s must not return null"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "kotlin/reflect/jvm/internal/impl/builtins/KotlinBuiltIns"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "getAnyType"

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    return-object v0
.end method

.method public y()Lkotlin/reflect/jvm/internal/impl/k/v;
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 738
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/a/l;->x()Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v0

    invoke-virtual {v0, v5}, Lkotlin/reflect/jvm/internal/impl/k/v;->a(Z)Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "@NotNull method %s.%s must not return null"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "kotlin/reflect/jvm/internal/impl/builtins/KotlinBuiltIns"

    aput-object v4, v2, v3

    const-string v3, "getNullableAnyType"

    aput-object v3, v2, v5

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    return-object v0
.end method

.method public z()Lkotlin/reflect/jvm/internal/impl/k/v;
    .locals 5

    .prologue
    .line 743
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/a/l;->y()Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "@NotNull method %s.%s must not return null"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "kotlin/reflect/jvm/internal/impl/builtins/KotlinBuiltIns"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "getDefaultBound"

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    return-object v0
.end method
