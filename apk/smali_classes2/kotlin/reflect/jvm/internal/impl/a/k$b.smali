.class final Lkotlin/reflect/jvm/internal/impl/a/k$b;
.super Lkotlin/d/b/m;
.source "JvmBuiltInClassDescriptorFactory.kt"

# interfaces
.implements Lkotlin/d/a/a;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lkotlin/reflect/jvm/internal/impl/a/k;-><init>(Lkotlin/reflect/jvm/internal/impl/j/i;Lkotlin/reflect/jvm/internal/impl/b/w;Lkotlin/d/a/b;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/d/b/m;",
        "Lkotlin/d/a/a",
        "<",
        "Lkotlin/reflect/jvm/internal/impl/b/b/h;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lkotlin/reflect/jvm/internal/impl/a/k;

.field final synthetic b:Lkotlin/reflect/jvm/internal/impl/j/i;


# direct methods
.method constructor <init>(Lkotlin/reflect/jvm/internal/impl/a/k;Lkotlin/reflect/jvm/internal/impl/j/i;)V
    .locals 1

    iput-object p1, p0, Lkotlin/reflect/jvm/internal/impl/a/k$b;->a:Lkotlin/reflect/jvm/internal/impl/a/k;

    iput-object p2, p0, Lkotlin/reflect/jvm/internal/impl/a/k$b;->b:Lkotlin/reflect/jvm/internal/impl/j/i;

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lkotlin/d/b/m;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final b()Lkotlin/reflect/jvm/internal/impl/b/b/h;
    .locals 8

    .prologue
    .line 36
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/b/b/h;

    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/a/k$b;->a:Lkotlin/reflect/jvm/internal/impl/a/k;

    invoke-static {v1}, Lkotlin/reflect/jvm/internal/impl/a/k;->a(Lkotlin/reflect/jvm/internal/impl/a/k;)Lkotlin/d/a/b;

    move-result-object v1

    iget-object v2, p0, Lkotlin/reflect/jvm/internal/impl/a/k$b;->a:Lkotlin/reflect/jvm/internal/impl/a/k;

    invoke-static {v2}, Lkotlin/reflect/jvm/internal/impl/a/k;->b(Lkotlin/reflect/jvm/internal/impl/a/k;)Lkotlin/reflect/jvm/internal/impl/b/w;

    move-result-object v2

    invoke-interface {v1, v2}, Lkotlin/d/a/b;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lkotlin/reflect/jvm/internal/impl/b/m;

    sget-object v2, Lkotlin/reflect/jvm/internal/impl/a/k;->b:Lkotlin/reflect/jvm/internal/impl/a/k$a;

    invoke-static {v2}, Lkotlin/reflect/jvm/internal/impl/a/k$a;->a(Lkotlin/reflect/jvm/internal/impl/a/k$a;)Lkotlin/reflect/jvm/internal/impl/e/f;

    move-result-object v2

    sget-object v3, Lkotlin/reflect/jvm/internal/impl/b/u;->d:Lkotlin/reflect/jvm/internal/impl/b/u;

    sget-object v4, Lkotlin/reflect/jvm/internal/impl/b/f;->b:Lkotlin/reflect/jvm/internal/impl/b/f;

    iget-object v5, p0, Lkotlin/reflect/jvm/internal/impl/a/k$b;->a:Lkotlin/reflect/jvm/internal/impl/a/k;

    invoke-static {v5}, Lkotlin/reflect/jvm/internal/impl/a/k;->b(Lkotlin/reflect/jvm/internal/impl/a/k;)Lkotlin/reflect/jvm/internal/impl/b/w;

    move-result-object v5

    invoke-interface {v5}, Lkotlin/reflect/jvm/internal/impl/b/w;->a()Lkotlin/reflect/jvm/internal/impl/a/l;

    move-result-object v5

    invoke-virtual {v5}, Lkotlin/reflect/jvm/internal/impl/a/l;->x()Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v5

    invoke-static {v5}, Lkotlin/a/m;->a(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v5

    check-cast v5, Ljava/util/Collection;

    sget-object v6, Lkotlin/reflect/jvm/internal/impl/b/al;->a:Lkotlin/reflect/jvm/internal/impl/b/al;

    const/4 v7, 0x0

    invoke-direct/range {v0 .. v7}, Lkotlin/reflect/jvm/internal/impl/b/b/h;-><init>(Lkotlin/reflect/jvm/internal/impl/b/m;Lkotlin/reflect/jvm/internal/impl/e/f;Lkotlin/reflect/jvm/internal/impl/b/u;Lkotlin/reflect/jvm/internal/impl/b/f;Ljava/util/Collection;Lkotlin/reflect/jvm/internal/impl/b/al;Z)V

    move-object v1, v0

    .line 40
    check-cast v1, Lkotlin/reflect/jvm/internal/impl/b/b/h;

    .line 41
    new-instance v3, Lkotlin/reflect/jvm/internal/impl/a/g;

    iget-object v4, p0, Lkotlin/reflect/jvm/internal/impl/a/k$b;->b:Lkotlin/reflect/jvm/internal/impl/j/i;

    move-object v2, v1

    check-cast v2, Lkotlin/reflect/jvm/internal/impl/b/e;

    invoke-direct {v3, v4, v2}, Lkotlin/reflect/jvm/internal/impl/a/g;-><init>(Lkotlin/reflect/jvm/internal/impl/j/i;Lkotlin/reflect/jvm/internal/impl/b/e;)V

    move-object v2, v3

    check-cast v2, Lkotlin/reflect/jvm/internal/impl/h/e/h;

    invoke-static {}, Lkotlin/a/ah;->a()Ljava/util/Set;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v3, v4}, Lkotlin/reflect/jvm/internal/impl/b/b/h;->a(Lkotlin/reflect/jvm/internal/impl/h/e/h;Ljava/util/Set;Lkotlin/reflect/jvm/internal/impl/b/d;)V

    .line 42
    nop

    .line 40
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/b/h;

    .line 42
    return-object v0
.end method

.method public synthetic v_()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 28
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/a/k$b;->b()Lkotlin/reflect/jvm/internal/impl/b/b/h;

    move-result-object v0

    return-object v0
.end method
