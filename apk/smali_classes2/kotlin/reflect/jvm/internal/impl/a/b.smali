.class public final Lkotlin/reflect/jvm/internal/impl/a/b;
.super Lkotlin/reflect/jvm/internal/impl/i/b/g;
.source "BuiltInsBinaryVersion.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lkotlin/reflect/jvm/internal/impl/a/b$a;
    }
.end annotation


# static fields
.field public static final a:Lkotlin/reflect/jvm/internal/impl/a/b;

.field public static final b:Lkotlin/reflect/jvm/internal/impl/a/b$a;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/a/b$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lkotlin/reflect/jvm/internal/impl/a/b$a;-><init>(Lkotlin/d/b/i;)V

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/a/b;->b:Lkotlin/reflect/jvm/internal/impl/a/b$a;

    .line 32
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/a/b;

    const/4 v1, 0x3

    new-array v1, v1, [I

    fill-array-data v1, :array_0

    invoke-direct {v0, v1}, Lkotlin/reflect/jvm/internal/impl/a/b;-><init>([I)V

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/a/b;->a:Lkotlin/reflect/jvm/internal/impl/a/b;

    return-void

    :array_0
    .array-data 4
        0x1
        0x0
        0x0
    .end array-data
.end method

.method public varargs constructor <init>([I)V
    .locals 1

    .prologue
    const-string v0, "numbers"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 27
    array-length v0, p1

    invoke-static {p1, v0}, Ljava/util/Arrays;->copyOf([II)[I

    move-result-object v0

    invoke-direct {p0, v0}, Lkotlin/reflect/jvm/internal/impl/i/b/g;-><init>([I)V

    return-void
.end method


# virtual methods
.method public a()Z
    .locals 1

    .prologue
    .line 28
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/a/b;->a:Lkotlin/reflect/jvm/internal/impl/a/b;

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/i/b/g;

    invoke-virtual {p0, v0}, Lkotlin/reflect/jvm/internal/impl/a/b;->a(Lkotlin/reflect/jvm/internal/impl/i/b/g;)Z

    move-result v0

    return v0
.end method
