.class public final Lkotlin/reflect/jvm/internal/impl/a/a/e$a;
.super Ljava/lang/Object;
.source "FunctionInvokeDescriptor.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lkotlin/reflect/jvm/internal/impl/a/a/e;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 98
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/d/b/i;)V
    .locals 0

    .prologue
    .line 98
    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/a/a/e$a;-><init>()V

    return-void
.end method

.method private final a(Lkotlin/reflect/jvm/internal/impl/a/a/e;ILkotlin/reflect/jvm/internal/impl/b/aq;)Lkotlin/reflect/jvm/internal/impl/b/at;
    .locals 12

    .prologue
    const/4 v2, 0x0

    const/4 v7, 0x0

    .line 123
    invoke-interface {p3}, Lkotlin/reflect/jvm/internal/impl/b/aq;->D_()Lkotlin/reflect/jvm/internal/impl/e/f;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/e/f;->a()Ljava/lang/String;

    move-result-object v0

    .line 124
    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    .line 129
    :cond_0
    if-nez v0, :cond_1

    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type java.lang.String"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 124
    :sswitch_0
    const-string v1, "E"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 126
    const-string v0, "receiver"

    move-object v3, v0

    .line 133
    :goto_0
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/b/b/ag;

    move-object v1, p1

    check-cast v1, Lkotlin/reflect/jvm/internal/impl/b/a;

    sget-object v4, Lkotlin/reflect/jvm/internal/impl/b/a/h;->a:Lkotlin/reflect/jvm/internal/impl/b/a/h$a;

    invoke-virtual {v4}, Lkotlin/reflect/jvm/internal/impl/b/a/h$a;->a()Lkotlin/reflect/jvm/internal/impl/b/a/h;

    move-result-object v4

    invoke-static {v3}, Lkotlin/reflect/jvm/internal/impl/e/f;->a(Ljava/lang/String;)Lkotlin/reflect/jvm/internal/impl/e/f;

    move-result-object v5

    const-string v3, "Name.identifier(name)"

    invoke-static {v5, v3}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {p3}, Lkotlin/reflect/jvm/internal/impl/b/aq;->E_()Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v6

    const-string v3, "typeParameter.defaultType"

    invoke-static {v6, v3}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v6, Lkotlin/reflect/jvm/internal/impl/k/r;

    sget-object v11, Lkotlin/reflect/jvm/internal/impl/b/al;->a:Lkotlin/reflect/jvm/internal/impl/b/al;

    const-string v3, "SourceElement.NO_SOURCE"

    invoke-static {v11, v3}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    move v3, p2

    move v8, v7

    move v9, v7

    move-object v10, v2

    invoke-direct/range {v0 .. v11}, Lkotlin/reflect/jvm/internal/impl/b/b/ag;-><init>(Lkotlin/reflect/jvm/internal/impl/b/a;Lkotlin/reflect/jvm/internal/impl/b/at;ILkotlin/reflect/jvm/internal/impl/b/a/h;Lkotlin/reflect/jvm/internal/impl/e/f;Lkotlin/reflect/jvm/internal/impl/k/r;ZZZLkotlin/reflect/jvm/internal/impl/k/r;Lkotlin/reflect/jvm/internal/impl/b/al;)V

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/at;

    return-object v0

    .line 124
    :sswitch_1
    const-string v1, "T"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 125
    const-string v0, "instance"

    move-object v3, v0

    goto :goto_0

    .line 129
    :cond_1
    check-cast v0, Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    const-string v1, "(this as java.lang.String).toLowerCase()"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v3, v0

    goto :goto_0

    .line 124
    nop

    :sswitch_data_0
    .sparse-switch
        0x45 -> :sswitch_0
        0x54 -> :sswitch_1
    .end sparse-switch
.end method


# virtual methods
.method public final a(Lkotlin/reflect/jvm/internal/impl/a/a/b;Z)Lkotlin/reflect/jvm/internal/impl/a/a/e;
    .locals 10

    .prologue
    const/4 v2, 0x0

    const-string v0, "functionClass"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 100
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/a/a/b;->y()Ljava/util/List;

    move-result-object v6

    .line 102
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/a/a/e;

    move-object v1, p1

    check-cast v1, Lkotlin/reflect/jvm/internal/impl/b/m;

    sget-object v3, Lkotlin/reflect/jvm/internal/impl/b/b$a;->a:Lkotlin/reflect/jvm/internal/impl/b/b$a;

    move v4, p2

    move-object v5, v2

    invoke-direct/range {v0 .. v5}, Lkotlin/reflect/jvm/internal/impl/a/a/e;-><init>(Lkotlin/reflect/jvm/internal/impl/b/m;Lkotlin/reflect/jvm/internal/impl/a/a/e;Lkotlin/reflect/jvm/internal/impl/b/b$a;ZLkotlin/d/b/i;)V

    .line 103
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/a/a/b;->C()Lkotlin/reflect/jvm/internal/impl/b/aj;

    move-result-object v3

    invoke-static {}, Lkotlin/a/m;->a()Ljava/util/List;

    move-result-object v4

    move-object v1, v6

    check-cast v1, Ljava/lang/Iterable;

    .line 148
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 149
    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_0
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    move-object v1, v5

    .line 150
    check-cast v1, Lkotlin/reflect/jvm/internal/impl/b/aq;

    .line 107
    invoke-interface {v1}, Lkotlin/reflect/jvm/internal/impl/b/aq;->k()Lkotlin/reflect/jvm/internal/impl/k/ap;

    move-result-object v1

    sget-object v9, Lkotlin/reflect/jvm/internal/impl/k/ap;->b:Lkotlin/reflect/jvm/internal/impl/k/ap;

    invoke-static {v1, v9}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    move-object v1, v7

    .line 154
    check-cast v1, Ljava/util/List;

    .line 103
    check-cast v1, Ljava/lang/Iterable;

    invoke-static {v1}, Lkotlin/a/m;->n(Ljava/lang/Iterable;)Ljava/lang/Iterable;

    move-result-object v5

    .line 155
    new-instance v1, Ljava/util/ArrayList;

    const/16 v7, 0xa

    invoke-static {v5, v7}, Lkotlin/a/m;->a(Ljava/lang/Iterable;I)I

    move-result v7

    invoke-direct {v1, v7}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v1, Ljava/util/Collection;

    .line 156
    invoke-interface {v5}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_1
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    .line 157
    check-cast v5, Lkotlin/a/x;

    .line 109
    sget-object v8, Lkotlin/reflect/jvm/internal/impl/a/a/e;->a:Lkotlin/reflect/jvm/internal/impl/a/a/e$a;

    invoke-virtual {v5}, Lkotlin/a/x;->a()I

    move-result v9

    invoke-virtual {v5}, Lkotlin/a/x;->b()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lkotlin/reflect/jvm/internal/impl/b/aq;

    invoke-direct {v8, v0, v9, v5}, Lkotlin/reflect/jvm/internal/impl/a/a/e$a;->a(Lkotlin/reflect/jvm/internal/impl/a/a/e;ILkotlin/reflect/jvm/internal/impl/b/aq;)Lkotlin/reflect/jvm/internal/impl/b/at;

    move-result-object v5

    invoke-interface {v1, v5}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 152
    :cond_1
    invoke-virtual {v7, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_2
    move-object v5, v1

    .line 158
    check-cast v5, Ljava/util/List;

    .line 103
    invoke-static {v6}, Lkotlin/a/m;->g(Ljava/util/List;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lkotlin/reflect/jvm/internal/impl/b/aq;

    invoke-interface {v1}, Lkotlin/reflect/jvm/internal/impl/b/aq;->E_()Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v6

    check-cast v6, Lkotlin/reflect/jvm/internal/impl/k/r;

    sget-object v7, Lkotlin/reflect/jvm/internal/impl/b/u;->d:Lkotlin/reflect/jvm/internal/impl/b/u;

    sget-object v8, Lkotlin/reflect/jvm/internal/impl/b/ax;->e:Lkotlin/reflect/jvm/internal/impl/b/ay;

    move-object v1, v0

    invoke-virtual/range {v1 .. v8}, Lkotlin/reflect/jvm/internal/impl/a/a/e;->b(Lkotlin/reflect/jvm/internal/impl/k/r;Lkotlin/reflect/jvm/internal/impl/b/aj;Ljava/util/List;Ljava/util/List;Lkotlin/reflect/jvm/internal/impl/k/r;Lkotlin/reflect/jvm/internal/impl/b/u;Lkotlin/reflect/jvm/internal/impl/b/ay;)Lkotlin/reflect/jvm/internal/impl/b/b/ac;

    .line 114
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lkotlin/reflect/jvm/internal/impl/a/a/e;->k(Z)V

    .line 115
    return-object v0
.end method
