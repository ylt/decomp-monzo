.class public final Lkotlin/reflect/jvm/internal/impl/a/f;
.super Ljava/lang/Object;
.source "builtInsPackageFragmentProvider.kt"


# direct methods
.method public static final a(Lkotlin/reflect/jvm/internal/impl/j/i;Lkotlin/reflect/jvm/internal/impl/b/w;Ljava/util/Set;Ljava/lang/Iterable;Lkotlin/reflect/jvm/internal/impl/i/b/ac;Lkotlin/reflect/jvm/internal/impl/i/b/a;Lkotlin/d/a/b;)Lkotlin/reflect/jvm/internal/impl/b/z;
    .locals 19
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/reflect/jvm/internal/impl/j/i;",
            "Lkotlin/reflect/jvm/internal/impl/b/w;",
            "Ljava/util/Set",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/e/b;",
            ">;",
            "Ljava/lang/Iterable",
            "<+",
            "Lkotlin/reflect/jvm/internal/impl/i/b/i;",
            ">;",
            "Lkotlin/reflect/jvm/internal/impl/i/b/ac;",
            "Lkotlin/reflect/jvm/internal/impl/i/b/a;",
            "Lkotlin/d/a/b",
            "<-",
            "Ljava/lang/String;",
            "+",
            "Ljava/io/InputStream;",
            ">;)",
            "Lkotlin/reflect/jvm/internal/impl/b/z;"
        }
    .end annotation

    .prologue
    const-string v2, "storageManager"

    move-object/from16 v0, p0

    invoke-static {v0, v2}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v2, "module"

    move-object/from16 v0, p1

    invoke-static {v0, v2}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v2, "packageFqNames"

    move-object/from16 v0, p2

    invoke-static {v0, v2}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v2, "classDescriptorFactories"

    move-object/from16 v0, p3

    invoke-static {v0, v2}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v2, "platformDependentDeclarationFilter"

    move-object/from16 v0, p4

    invoke-static {v0, v2}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v2, "additionalClassPartsProvider"

    move-object/from16 v0, p5

    invoke-static {v0, v2}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v2, "loadResource"

    move-object/from16 v0, p6

    invoke-static {v0, v2}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 37
    check-cast p2, Ljava/lang/Iterable;

    .line 70
    new-instance v2, Ljava/util/ArrayList;

    const/16 v3, 0xa

    move-object/from16 v0, p2

    invoke-static {v0, v3}, Lkotlin/a/m;->a(Ljava/lang/Iterable;I)I

    move-result v3

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v2, Ljava/util/Collection;

    .line 71
    invoke-interface/range {p2 .. p2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    .line 72
    check-cast v3, Lkotlin/reflect/jvm/internal/impl/e/b;

    .line 38
    sget-object v4, Lkotlin/reflect/jvm/internal/impl/a/a;->a:Lkotlin/reflect/jvm/internal/impl/a/a;

    invoke-virtual {v4, v3}, Lkotlin/reflect/jvm/internal/impl/a/a;->a(Lkotlin/reflect/jvm/internal/impl/e/b;)Ljava/lang/String;

    move-result-object v6

    .line 39
    move-object/from16 v0, p6

    invoke-interface {v0, v6}, Lkotlin/d/a/b;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/io/InputStream;

    if-eqz v4, :cond_0

    .line 40
    new-instance v6, Lkotlin/reflect/jvm/internal/impl/a/e;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v6, v3, v0, v1, v4}, Lkotlin/reflect/jvm/internal/impl/a/e;-><init>(Lkotlin/reflect/jvm/internal/impl/e/b;Lkotlin/reflect/jvm/internal/impl/j/i;Lkotlin/reflect/jvm/internal/impl/b/w;Ljava/io/InputStream;)V

    invoke-interface {v2, v6}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 39
    :cond_0
    new-instance v2, Ljava/lang/IllegalStateException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Resource not found in classpath: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v2, Ljava/lang/Throwable;

    throw v2

    :cond_1
    move-object/from16 v17, v2

    .line 73
    check-cast v17, Ljava/util/List;

    .line 42
    new-instance v18, Lkotlin/reflect/jvm/internal/impl/b/aa;

    move-object/from16 v2, v17

    check-cast v2, Ljava/util/Collection;

    move-object/from16 v0, v18

    invoke-direct {v0, v2}, Lkotlin/reflect/jvm/internal/impl/b/aa;-><init>(Ljava/util/Collection;)V

    .line 44
    new-instance v14, Lkotlin/reflect/jvm/internal/impl/i/b/aa;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v14, v0, v1}, Lkotlin/reflect/jvm/internal/impl/i/b/aa;-><init>(Lkotlin/reflect/jvm/internal/impl/j/i;Lkotlin/reflect/jvm/internal/impl/b/w;)V

    .line 46
    new-instance v2, Lkotlin/reflect/jvm/internal/impl/i/b/m;

    sget-object v5, Lkotlin/reflect/jvm/internal/impl/i/b/n$a;->a:Lkotlin/reflect/jvm/internal/impl/i/b/n$a;

    check-cast v5, Lkotlin/reflect/jvm/internal/impl/i/b/n;

    new-instance v6, Lkotlin/reflect/jvm/internal/impl/i/b/p;

    move-object/from16 v3, v18

    check-cast v3, Lkotlin/reflect/jvm/internal/impl/b/z;

    invoke-direct {v6, v3}, Lkotlin/reflect/jvm/internal/impl/i/b/p;-><init>(Lkotlin/reflect/jvm/internal/impl/b/z;)V

    check-cast v6, Lkotlin/reflect/jvm/internal/impl/i/b/h;

    new-instance v7, Lkotlin/reflect/jvm/internal/impl/i/b/d;

    sget-object v3, Lkotlin/reflect/jvm/internal/impl/a/a;->a:Lkotlin/reflect/jvm/internal/impl/a/a;

    check-cast v3, Lkotlin/reflect/jvm/internal/impl/i/f;

    move-object/from16 v0, p1

    invoke-direct {v7, v0, v14, v3}, Lkotlin/reflect/jvm/internal/impl/i/b/d;-><init>(Lkotlin/reflect/jvm/internal/impl/b/w;Lkotlin/reflect/jvm/internal/impl/i/b/aa;Lkotlin/reflect/jvm/internal/impl/i/f;)V

    check-cast v7, Lkotlin/reflect/jvm/internal/impl/i/b/c;

    move-object/from16 v8, v18

    check-cast v8, Lkotlin/reflect/jvm/internal/impl/b/z;

    sget-object v9, Lkotlin/reflect/jvm/internal/impl/i/b/v$a;->a:Lkotlin/reflect/jvm/internal/impl/i/b/v$a;

    check-cast v9, Lkotlin/reflect/jvm/internal/impl/i/b/v;

    sget-object v10, Lkotlin/reflect/jvm/internal/impl/i/b/r;->b:Lkotlin/reflect/jvm/internal/impl/i/b/r;

    const-string v3, "ErrorReporter.DO_NOTHING"

    invoke-static {v10, v3}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v3, Lkotlin/reflect/jvm/internal/impl/c/a/c;->a:Lkotlin/reflect/jvm/internal/impl/c/a/c$a;

    invoke-virtual {v3}, Lkotlin/reflect/jvm/internal/impl/c/a/c$a;->a()Lkotlin/reflect/jvm/internal/impl/c/a/c;

    move-result-object v11

    sget-object v12, Lkotlin/reflect/jvm/internal/impl/i/b/t$a;->a:Lkotlin/reflect/jvm/internal/impl/i/b/t$a;

    check-cast v12, Lkotlin/reflect/jvm/internal/impl/i/b/t;

    move-object/from16 v3, p0

    move-object/from16 v4, p1

    move-object/from16 v13, p3

    move-object/from16 v15, p5

    move-object/from16 v16, p4

    invoke-direct/range {v2 .. v16}, Lkotlin/reflect/jvm/internal/impl/i/b/m;-><init>(Lkotlin/reflect/jvm/internal/impl/j/i;Lkotlin/reflect/jvm/internal/impl/b/w;Lkotlin/reflect/jvm/internal/impl/i/b/n;Lkotlin/reflect/jvm/internal/impl/i/b/h;Lkotlin/reflect/jvm/internal/impl/i/b/c;Lkotlin/reflect/jvm/internal/impl/b/z;Lkotlin/reflect/jvm/internal/impl/i/b/v;Lkotlin/reflect/jvm/internal/impl/i/b/r;Lkotlin/reflect/jvm/internal/impl/c/a/c;Lkotlin/reflect/jvm/internal/impl/i/b/t;Ljava/lang/Iterable;Lkotlin/reflect/jvm/internal/impl/i/b/aa;Lkotlin/reflect/jvm/internal/impl/i/b/a;Lkotlin/reflect/jvm/internal/impl/i/b/ac;)V

    .line 63
    invoke-interface/range {v17 .. v17}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lkotlin/reflect/jvm/internal/impl/a/e;

    .line 64
    invoke-virtual {v3, v2}, Lkotlin/reflect/jvm/internal/impl/a/e;->a(Lkotlin/reflect/jvm/internal/impl/i/b/m;)V

    goto :goto_1

    .line 67
    :cond_2
    check-cast v18, Lkotlin/reflect/jvm/internal/impl/b/z;

    return-object v18
.end method
