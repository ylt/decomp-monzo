.class public final Lkotlin/reflect/jvm/internal/impl/a/c;
.super Ljava/lang/Object;
.source "BuiltInsClassDataFinder.kt"

# interfaces
.implements Lkotlin/reflect/jvm/internal/impl/i/b/h;


# instance fields
.field private final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/e/a;",
            "Lkotlin/reflect/jvm/internal/impl/i/e$c;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Lkotlin/reflect/jvm/internal/impl/i/b/x;


# direct methods
.method public constructor <init>(Lkotlin/reflect/jvm/internal/impl/i/a/a$a;Lkotlin/reflect/jvm/internal/impl/i/b/x;)V
    .locals 5

    .prologue
    const-string v0, "proto"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "nameResolver"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lkotlin/reflect/jvm/internal/impl/a/c;->b:Lkotlin/reflect/jvm/internal/impl/i/b/x;

    .line 32
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/a/a$a;->r()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 44
    const/16 v1, 0xa

    invoke-static {v0, v1}, Lkotlin/a/m;->a(Ljava/lang/Iterable;I)I

    move-result v1

    invoke-static {v1}, Lkotlin/a/ab;->a(I)I

    move-result v1

    const/16 v2, 0x10

    invoke-static {v1, v2}, Lkotlin/f/d;->c(II)I

    move-result v2

    .line 45
    new-instance v1, Ljava/util/LinkedHashMap;

    invoke-direct {v1, v2}, Ljava/util/LinkedHashMap;-><init>(I)V

    check-cast v1, Ljava/util/Map;

    .line 46
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    move-object v0, v2

    .line 47
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/i/e$c;

    .line 33
    iget-object v4, p0, Lkotlin/reflect/jvm/internal/impl/a/c;->b:Lkotlin/reflect/jvm/internal/impl/i/b/x;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/i/e$c;->r()I

    move-result v0

    invoke-interface {v4, v0}, Lkotlin/reflect/jvm/internal/impl/i/b/x;->c(I)Lkotlin/reflect/jvm/internal/impl/e/a;

    move-result-object v0

    invoke-interface {v1, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 49
    :cond_0
    iput-object v1, p0, Lkotlin/reflect/jvm/internal/impl/a/c;->a:Ljava/util/Map;

    return-void
.end method


# virtual methods
.method public final a()Ljava/util/Collection;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/e/a;",
            ">;"
        }
    .end annotation

    .prologue
    .line 36
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/a/c;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    return-object v0
.end method

.method public a(Lkotlin/reflect/jvm/internal/impl/e/a;)Lkotlin/reflect/jvm/internal/impl/i/b;
    .locals 4

    .prologue
    const-string v0, "classId"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 39
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/a/c;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/i/e$c;

    if-eqz v0, :cond_0

    .line 40
    new-instance v1, Lkotlin/reflect/jvm/internal/impl/i/b;

    new-instance v2, Lkotlin/reflect/jvm/internal/impl/i/a;

    iget-object v3, p0, Lkotlin/reflect/jvm/internal/impl/a/c;->b:Lkotlin/reflect/jvm/internal/impl/i/b/x;

    invoke-direct {v2, v3, v0}, Lkotlin/reflect/jvm/internal/impl/i/a;-><init>(Lkotlin/reflect/jvm/internal/impl/i/b/x;Lkotlin/reflect/jvm/internal/impl/i/e$c;)V

    sget-object v0, Lkotlin/reflect/jvm/internal/impl/b/al;->a:Lkotlin/reflect/jvm/internal/impl/b/al;

    const-string v3, "SourceElement.NO_SOURCE"

    invoke-static {v0, v3}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v1, v2, v0}, Lkotlin/reflect/jvm/internal/impl/i/b;-><init>(Lkotlin/reflect/jvm/internal/impl/i/a;Lkotlin/reflect/jvm/internal/impl/b/al;)V

    move-object v0, v1

    :goto_0
    return-object v0

    .line 39
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
