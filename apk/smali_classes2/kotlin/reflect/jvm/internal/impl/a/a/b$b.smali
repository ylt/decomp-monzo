.class public final enum Lkotlin/reflect/jvm/internal/impl/a/a/b$b;
.super Ljava/lang/Enum;
.source "FunctionClassDescriptor.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lkotlin/reflect/jvm/internal/impl/a/a/b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lkotlin/reflect/jvm/internal/impl/a/a/b$b$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lkotlin/reflect/jvm/internal/impl/a/a/b$b;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lkotlin/reflect/jvm/internal/impl/a/a/b$b;

.field public static final enum b:Lkotlin/reflect/jvm/internal/impl/a/a/b$b;

.field public static final enum c:Lkotlin/reflect/jvm/internal/impl/a/a/b$b;

.field public static final d:Lkotlin/reflect/jvm/internal/impl/a/a/b$b$a;

.field private static final synthetic e:[Lkotlin/reflect/jvm/internal/impl/a/a/b$b;


# instance fields
.field private final f:Lkotlin/reflect/jvm/internal/impl/e/b;

.field private final g:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    const/4 v0, 0x3

    new-array v0, v0, [Lkotlin/reflect/jvm/internal/impl/a/a/b$b;

    new-instance v1, Lkotlin/reflect/jvm/internal/impl/a/a/b$b;

    const-string v2, "Function"

    sget-object v3, Lkotlin/reflect/jvm/internal/impl/a/l;->c:Lkotlin/reflect/jvm/internal/impl/e/b;

    const-string v4, "BUILT_INS_PACKAGE_FQ_NAME"

    invoke-static {v3, v4}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v4, "Function"

    .line 52
    invoke-direct {v1, v2, v5, v3, v4}, Lkotlin/reflect/jvm/internal/impl/a/a/b$b;-><init>(Ljava/lang/String;ILkotlin/reflect/jvm/internal/impl/e/b;Ljava/lang/String;)V

    sput-object v1, Lkotlin/reflect/jvm/internal/impl/a/a/b$b;->a:Lkotlin/reflect/jvm/internal/impl/a/a/b$b;

    aput-object v1, v0, v5

    new-instance v1, Lkotlin/reflect/jvm/internal/impl/a/a/b$b;

    const-string v2, "SuspendFunction"

    sget-object v3, Lkotlin/reflect/jvm/internal/impl/a/l;->c:Lkotlin/reflect/jvm/internal/impl/e/b;

    const-string v4, "BUILT_INS_PACKAGE_FQ_NAME"

    invoke-static {v3, v4}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v4, "SuspendFunction"

    .line 53
    invoke-direct {v1, v2, v6, v3, v4}, Lkotlin/reflect/jvm/internal/impl/a/a/b$b;-><init>(Ljava/lang/String;ILkotlin/reflect/jvm/internal/impl/e/b;Ljava/lang/String;)V

    sput-object v1, Lkotlin/reflect/jvm/internal/impl/a/a/b$b;->b:Lkotlin/reflect/jvm/internal/impl/a/a/b$b;

    aput-object v1, v0, v6

    new-instance v1, Lkotlin/reflect/jvm/internal/impl/a/a/b$b;

    const-string v2, "KFunction"

    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/a/o;->a()Lkotlin/reflect/jvm/internal/impl/e/b;

    move-result-object v3

    const-string v4, "KFunction"

    .line 54
    invoke-direct {v1, v2, v7, v3, v4}, Lkotlin/reflect/jvm/internal/impl/a/a/b$b;-><init>(Ljava/lang/String;ILkotlin/reflect/jvm/internal/impl/e/b;Ljava/lang/String;)V

    sput-object v1, Lkotlin/reflect/jvm/internal/impl/a/a/b$b;->c:Lkotlin/reflect/jvm/internal/impl/a/a/b$b;

    aput-object v1, v0, v7

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/a/a/b$b;->e:[Lkotlin/reflect/jvm/internal/impl/a/a/b$b;

    new-instance v0, Lkotlin/reflect/jvm/internal/impl/a/a/b$b$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lkotlin/reflect/jvm/internal/impl/a/a/b$b$a;-><init>(Lkotlin/d/b/i;)V

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/a/a/b$b;->d:Lkotlin/reflect/jvm/internal/impl/a/a/b$b$a;

    return-void
.end method

.method protected constructor <init>(Ljava/lang/String;ILkotlin/reflect/jvm/internal/impl/e/b;Ljava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/reflect/jvm/internal/impl/e/b;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    const-string v0, "packageFqName"

    invoke-static {p3, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "classNamePrefix"

    invoke-static {p4, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 51
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput-object p3, p0, Lkotlin/reflect/jvm/internal/impl/a/a/b$b;->f:Lkotlin/reflect/jvm/internal/impl/e/b;

    iput-object p4, p0, Lkotlin/reflect/jvm/internal/impl/a/a/b$b;->g:Ljava/lang/String;

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lkotlin/reflect/jvm/internal/impl/a/a/b$b;
    .locals 1

    const-class v0, Lkotlin/reflect/jvm/internal/impl/a/a/b$b;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/a/a/b$b;

    return-object v0
.end method

.method public static values()[Lkotlin/reflect/jvm/internal/impl/a/a/b$b;
    .locals 1

    sget-object v0, Lkotlin/reflect/jvm/internal/impl/a/a/b$b;->e:[Lkotlin/reflect/jvm/internal/impl/a/a/b$b;

    invoke-virtual {v0}, [Lkotlin/reflect/jvm/internal/impl/a/a/b$b;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lkotlin/reflect/jvm/internal/impl/a/a/b$b;

    return-object v0
.end method


# virtual methods
.method public final a()Lkotlin/reflect/jvm/internal/impl/e/b;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/a/a/b$b;->f:Lkotlin/reflect/jvm/internal/impl/e/b;

    return-object v0
.end method

.method public final a(I)Lkotlin/reflect/jvm/internal/impl/e/f;
    .locals 2

    .prologue
    .line 56
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/a/a/b$b;->g:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/e/f;->a(Ljava/lang/String;)Lkotlin/reflect/jvm/internal/impl/e/f;

    move-result-object v0

    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/a/a/b$b;->g:Ljava/lang/String;

    return-object v0
.end method
