.class public final Lkotlin/reflect/jvm/internal/impl/a/e;
.super Lkotlin/reflect/jvm/internal/impl/i/b/q;
.source "BuiltInsPackageFragment.kt"


# instance fields
.field private final b:Lkotlin/reflect/jvm/internal/impl/i/a/a$a;

.field private final c:Lkotlin/reflect/jvm/internal/impl/i/b/y;

.field private final d:Lkotlin/reflect/jvm/internal/impl/a/c;


# direct methods
.method public constructor <init>(Lkotlin/reflect/jvm/internal/impl/e/b;Lkotlin/reflect/jvm/internal/impl/j/i;Lkotlin/reflect/jvm/internal/impl/b/w;Ljava/io/InputStream;)V
    .locals 7

    .prologue
    const-string v1, "fqName"

    invoke-static {p1, v1}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "storageManager"

    invoke-static {p2, v1}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "module"

    invoke-static {p3, v1}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "inputStream"

    invoke-static {p4, v1}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 34
    invoke-direct {p0, p1, p2, p3}, Lkotlin/reflect/jvm/internal/impl/i/b/q;-><init>(Lkotlin/reflect/jvm/internal/impl/e/b;Lkotlin/reflect/jvm/internal/impl/j/i;Lkotlin/reflect/jvm/internal/impl/b/w;)V

    .line 35
    check-cast p4, Ljava/io/Closeable;

    const/4 v2, 0x0

    nop

    :try_start_0
    move-object v0, p4

    check-cast v0, Ljava/io/InputStream;

    move-object v1, v0

    .line 36
    sget-object v3, Lkotlin/reflect/jvm/internal/impl/a/b;->b:Lkotlin/reflect/jvm/internal/impl/a/b$a;

    invoke-virtual {v3, v1}, Lkotlin/reflect/jvm/internal/impl/a/b$a;->a(Ljava/io/InputStream;)Lkotlin/reflect/jvm/internal/impl/a/b;

    move-result-object v3

    .line 38
    invoke-virtual {v3}, Lkotlin/reflect/jvm/internal/impl/a/b;->a()Z

    move-result v4

    if-nez v4, :cond_2

    .line 40
    new-instance v1, Ljava/lang/UnsupportedOperationException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Kotlin built-in definition format version is not supported: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "expected "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    sget-object v6, Lkotlin/reflect/jvm/internal/impl/a/b;->a:Lkotlin/reflect/jvm/internal/impl/a/b;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", actual "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, ". "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "Please update Kotlin"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v3}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    check-cast v1, Ljava/lang/Throwable;

    throw v1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 35
    :catch_0
    move-exception v1

    const/4 v2, 0x1

    nop

    if-eqz p4, :cond_0

    :try_start_1
    invoke-interface {p4}, Ljava/io/Closeable;->close()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_0
    :goto_0
    :try_start_2
    check-cast v1, Ljava/lang/Throwable;

    throw v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :catchall_0
    move-exception v1

    if-nez v2, :cond_1

    if-eqz p4, :cond_1

    invoke-interface {p4}, Ljava/io/Closeable;->close()V

    :cond_1
    throw v1

    .line 47
    :cond_2
    :try_start_3
    sget-object v3, Lkotlin/reflect/jvm/internal/impl/a/a;->a:Lkotlin/reflect/jvm/internal/impl/a/a;

    invoke-virtual {v3}, Lkotlin/reflect/jvm/internal/impl/a/a;->a()Lkotlin/reflect/jvm/internal/impl/protobuf/f;

    move-result-object v3

    invoke-static {v1, v3}, Lkotlin/reflect/jvm/internal/impl/i/a/a$a;->a(Ljava/io/InputStream;Lkotlin/reflect/jvm/internal/impl/protobuf/f;)Lkotlin/reflect/jvm/internal/impl/i/a/a$a;
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result-object v1

    .line 35
    if-eqz p4, :cond_3

    invoke-interface {p4}, Ljava/io/Closeable;->close()V

    :cond_3
    check-cast v1, Lkotlin/reflect/jvm/internal/impl/i/a/a$a;

    iput-object v1, p0, Lkotlin/reflect/jvm/internal/impl/a/e;->b:Lkotlin/reflect/jvm/internal/impl/i/a/a$a;

    .line 50
    new-instance v1, Lkotlin/reflect/jvm/internal/impl/i/b/y;

    iget-object v2, p0, Lkotlin/reflect/jvm/internal/impl/a/e;->b:Lkotlin/reflect/jvm/internal/impl/i/a/a$a;

    invoke-virtual {v2}, Lkotlin/reflect/jvm/internal/impl/i/a/a$a;->m()Lkotlin/reflect/jvm/internal/impl/i/e$w;

    move-result-object v2

    const-string v3, "proto.strings"

    invoke-static {v2, v3}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v3, p0, Lkotlin/reflect/jvm/internal/impl/a/e;->b:Lkotlin/reflect/jvm/internal/impl/i/a/a$a;

    invoke-virtual {v3}, Lkotlin/reflect/jvm/internal/impl/i/a/a$a;->o()Lkotlin/reflect/jvm/internal/impl/i/e$q;

    move-result-object v3

    const-string v4, "proto.qualifiedNames"

    invoke-static {v3, v4}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v1, v2, v3}, Lkotlin/reflect/jvm/internal/impl/i/b/y;-><init>(Lkotlin/reflect/jvm/internal/impl/i/e$w;Lkotlin/reflect/jvm/internal/impl/i/e$q;)V

    iput-object v1, p0, Lkotlin/reflect/jvm/internal/impl/a/e;->c:Lkotlin/reflect/jvm/internal/impl/i/b/y;

    .line 52
    new-instance v2, Lkotlin/reflect/jvm/internal/impl/a/c;

    iget-object v3, p0, Lkotlin/reflect/jvm/internal/impl/a/e;->b:Lkotlin/reflect/jvm/internal/impl/i/a/a$a;

    const-string v1, "proto"

    invoke-static {v3, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/a/e;->c:Lkotlin/reflect/jvm/internal/impl/i/b/y;

    check-cast v1, Lkotlin/reflect/jvm/internal/impl/i/b/x;

    invoke-direct {v2, v3, v1}, Lkotlin/reflect/jvm/internal/impl/a/c;-><init>(Lkotlin/reflect/jvm/internal/impl/i/a/a$a;Lkotlin/reflect/jvm/internal/impl/i/b/x;)V

    iput-object v2, p0, Lkotlin/reflect/jvm/internal/impl/a/e;->d:Lkotlin/reflect/jvm/internal/impl/a/c;

    return-void

    .line 35
    :catch_1
    move-exception v3

    goto :goto_0
.end method


# virtual methods
.method public a()Lkotlin/reflect/jvm/internal/impl/a/c;
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/a/e;->d:Lkotlin/reflect/jvm/internal/impl/a/c;

    return-object v0
.end method

.method public synthetic b()Lkotlin/reflect/jvm/internal/impl/i/b/h;
    .locals 1

    .prologue
    .line 29
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/a/e;->a()Lkotlin/reflect/jvm/internal/impl/a/c;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/i/b/h;

    return-object v0
.end method

.method protected c()Lkotlin/reflect/jvm/internal/impl/i/b/a/i;
    .locals 7

    .prologue
    .line 55
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/i/b/a/i;

    move-object v1, p0

    check-cast v1, Lkotlin/reflect/jvm/internal/impl/b/y;

    iget-object v2, p0, Lkotlin/reflect/jvm/internal/impl/a/e;->b:Lkotlin/reflect/jvm/internal/impl/i/a/a$a;

    invoke-virtual {v2}, Lkotlin/reflect/jvm/internal/impl/i/a/a$a;->q()Lkotlin/reflect/jvm/internal/impl/i/e$m;

    move-result-object v2

    const-string v3, "proto.`package`"

    invoke-static {v2, v3}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v3, p0, Lkotlin/reflect/jvm/internal/impl/a/e;->c:Lkotlin/reflect/jvm/internal/impl/i/b/y;

    check-cast v3, Lkotlin/reflect/jvm/internal/impl/i/b/x;

    const/4 v4, 0x0

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/a/e;->g()Lkotlin/reflect/jvm/internal/impl/i/b/m;

    move-result-object v5

    new-instance v6, Lkotlin/reflect/jvm/internal/impl/a/e$a;

    invoke-direct {v6, p0}, Lkotlin/reflect/jvm/internal/impl/a/e$a;-><init>(Lkotlin/reflect/jvm/internal/impl/a/e;)V

    check-cast v6, Lkotlin/d/a/a;

    invoke-direct/range {v0 .. v6}, Lkotlin/reflect/jvm/internal/impl/i/b/a/i;-><init>(Lkotlin/reflect/jvm/internal/impl/b/y;Lkotlin/reflect/jvm/internal/impl/i/e$m;Lkotlin/reflect/jvm/internal/impl/i/b/x;Lkotlin/reflect/jvm/internal/impl/i/b/a/f;Lkotlin/reflect/jvm/internal/impl/i/b/m;Lkotlin/d/a/a;)V

    .line 60
    return-object v0
.end method

.method public synthetic d()Lkotlin/reflect/jvm/internal/impl/h/e/h;
    .locals 1

    .prologue
    .line 29
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/a/e;->c()Lkotlin/reflect/jvm/internal/impl/i/b/a/i;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/h/e/h;

    return-object v0
.end method
