.class public final Lkotlin/reflect/jvm/internal/impl/a/g;
.super Lkotlin/reflect/jvm/internal/impl/h/e/e;
.source "CloneableClassScope.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lkotlin/reflect/jvm/internal/impl/a/g$a;
    }
.end annotation


# static fields
.field public static final a:Lkotlin/reflect/jvm/internal/impl/a/g$a;

.field private static final d:Lkotlin/reflect/jvm/internal/impl/e/f;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/a/g$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lkotlin/reflect/jvm/internal/impl/a/g$a;-><init>(Lkotlin/d/b/i;)V

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/a/g;->a:Lkotlin/reflect/jvm/internal/impl/a/g$a;

    .line 42
    const-string v0, "clone"

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/e/f;->a(Ljava/lang/String;)Lkotlin/reflect/jvm/internal/impl/e/f;

    move-result-object v0

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/a/g;->d:Lkotlin/reflect/jvm/internal/impl/e/f;

    return-void
.end method

.method public constructor <init>(Lkotlin/reflect/jvm/internal/impl/j/i;Lkotlin/reflect/jvm/internal/impl/b/e;)V
    .locals 1

    .prologue
    const-string v0, "storageManager"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "containingClass"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 31
    invoke-direct {p0, p1, p2}, Lkotlin/reflect/jvm/internal/impl/h/e/e;-><init>(Lkotlin/reflect/jvm/internal/impl/j/i;Lkotlin/reflect/jvm/internal/impl/b/e;)V

    return-void
.end method

.method public static final synthetic b()Lkotlin/reflect/jvm/internal/impl/e/f;
    .locals 1

    .prologue
    .line 28
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/a/g;->d:Lkotlin/reflect/jvm/internal/impl/e/f;

    return-object v0
.end method


# virtual methods
.method protected a()Ljava/util/List;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/b/s;",
            ">;"
        }
    .end annotation

    .prologue
    .line 32
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/a/g;->c()Lkotlin/reflect/jvm/internal/impl/b/e;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/m;

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/b/a/h;->a:Lkotlin/reflect/jvm/internal/impl/b/a/h$a;

    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/impl/b/a/h$a;->a()Lkotlin/reflect/jvm/internal/impl/b/a/h;

    move-result-object v1

    sget-object v2, Lkotlin/reflect/jvm/internal/impl/a/g;->a:Lkotlin/reflect/jvm/internal/impl/a/g$a;

    invoke-virtual {v2}, Lkotlin/reflect/jvm/internal/impl/a/g$a;->a()Lkotlin/reflect/jvm/internal/impl/e/f;

    move-result-object v2

    sget-object v3, Lkotlin/reflect/jvm/internal/impl/b/b$a;->a:Lkotlin/reflect/jvm/internal/impl/b/b$a;

    sget-object v4, Lkotlin/reflect/jvm/internal/impl/b/al;->a:Lkotlin/reflect/jvm/internal/impl/b/al;

    invoke-static {v0, v1, v2, v3, v4}, Lkotlin/reflect/jvm/internal/impl/b/b/ac;->a(Lkotlin/reflect/jvm/internal/impl/b/m;Lkotlin/reflect/jvm/internal/impl/b/a/h;Lkotlin/reflect/jvm/internal/impl/e/f;Lkotlin/reflect/jvm/internal/impl/b/b$a;Lkotlin/reflect/jvm/internal/impl/b/al;)Lkotlin/reflect/jvm/internal/impl/b/b/ac;

    move-result-object v8

    move-object v0, v8

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/b/ac;

    .line 34
    const/4 v1, 0x0

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/a/g;->c()Lkotlin/reflect/jvm/internal/impl/b/e;

    move-result-object v2

    invoke-interface {v2}, Lkotlin/reflect/jvm/internal/impl/b/e;->C()Lkotlin/reflect/jvm/internal/impl/b/aj;

    move-result-object v2

    invoke-static {}, Lkotlin/a/m;->a()Ljava/util/List;

    move-result-object v3

    invoke-static {}, Lkotlin/a/m;->a()Ljava/util/List;

    move-result-object v4

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/a/g;->c()Lkotlin/reflect/jvm/internal/impl/b/e;

    move-result-object v5

    check-cast v5, Lkotlin/reflect/jvm/internal/impl/b/m;

    invoke-static {v5}, Lkotlin/reflect/jvm/internal/impl/h/c/a;->d(Lkotlin/reflect/jvm/internal/impl/b/m;)Lkotlin/reflect/jvm/internal/impl/a/l;

    move-result-object v5

    invoke-virtual {v5}, Lkotlin/reflect/jvm/internal/impl/a/l;->x()Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v5

    check-cast v5, Lkotlin/reflect/jvm/internal/impl/k/r;

    sget-object v6, Lkotlin/reflect/jvm/internal/impl/b/u;->c:Lkotlin/reflect/jvm/internal/impl/b/u;

    sget-object v7, Lkotlin/reflect/jvm/internal/impl/b/ax;->c:Lkotlin/reflect/jvm/internal/impl/b/ay;

    invoke-virtual/range {v0 .. v7}, Lkotlin/reflect/jvm/internal/impl/b/b/ac;->b(Lkotlin/reflect/jvm/internal/impl/k/r;Lkotlin/reflect/jvm/internal/impl/b/aj;Ljava/util/List;Ljava/util/List;Lkotlin/reflect/jvm/internal/impl/k/r;Lkotlin/reflect/jvm/internal/impl/b/u;Lkotlin/reflect/jvm/internal/impl/b/ay;)Lkotlin/reflect/jvm/internal/impl/b/b/ac;

    .line 38
    nop

    .line 32
    invoke-static {v8}, Lkotlin/a/m;->a(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    .line 39
    return-object v0
.end method
