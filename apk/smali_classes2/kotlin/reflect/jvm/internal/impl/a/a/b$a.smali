.class final Lkotlin/reflect/jvm/internal/impl/a/a/b$a;
.super Lkotlin/reflect/jvm/internal/impl/k/b;
.source "FunctionClassDescriptor.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lkotlin/reflect/jvm/internal/impl/a/a/b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "a"
.end annotation


# instance fields
.field final synthetic a:Lkotlin/reflect/jvm/internal/impl/a/a/b;


# direct methods
.method public constructor <init>(Lkotlin/reflect/jvm/internal/impl/a/a/b;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 113
    iput-object p1, p0, Lkotlin/reflect/jvm/internal/impl/a/a/b$a;->a:Lkotlin/reflect/jvm/internal/impl/a/a/b;

    invoke-static {p1}, Lkotlin/reflect/jvm/internal/impl/a/a/b;->c(Lkotlin/reflect/jvm/internal/impl/a/a/b;)Lkotlin/reflect/jvm/internal/impl/j/i;

    move-result-object v0

    invoke-direct {p0, v0}, Lkotlin/reflect/jvm/internal/impl/k/b;-><init>(Lkotlin/reflect/jvm/internal/impl/j/i;)V

    return-void
.end method


# virtual methods
.method protected F_()Lkotlin/reflect/jvm/internal/impl/b/ao;
    .locals 1

    .prologue
    .line 161
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/b/ao$a;->a:Lkotlin/reflect/jvm/internal/impl/b/ao$a;

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/ao;

    return-object v0
.end method

.method protected a()Ljava/util/Collection;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/k/r;",
            ">;"
        }
    .end annotation

    .prologue
    .line 115
    new-instance v3, Ljava/util/ArrayList;

    const/4 v0, 0x2

    invoke-direct {v3, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 117
    new-instance v1, Lkotlin/reflect/jvm/internal/impl/a/a/b$a$a;

    invoke-direct {v1, p0, v3}, Lkotlin/reflect/jvm/internal/impl/a/a/b$a$a;-><init>(Lkotlin/reflect/jvm/internal/impl/a/a/b$a;Ljava/util/ArrayList;)V

    .line 132
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/a/a/b$a;->a:Lkotlin/reflect/jvm/internal/impl/a/a/b;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/a/a/b;->z()Lkotlin/reflect/jvm/internal/impl/a/a/b$b;

    move-result-object v0

    sget-object v2, Lkotlin/reflect/jvm/internal/impl/a/a/b$b;->b:Lkotlin/reflect/jvm/internal/impl/a/a/b$b;

    invoke-static {v0, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 134
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/a/a/b$a;->a:Lkotlin/reflect/jvm/internal/impl/a/a/b;

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/a/a/b;->a(Lkotlin/reflect/jvm/internal/impl/a/a/b;)Lkotlin/reflect/jvm/internal/impl/b/y;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/m;

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/h/c/a;->d(Lkotlin/reflect/jvm/internal/impl/b/m;)Lkotlin/reflect/jvm/internal/impl/a/l;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/a/l;->x()Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 142
    :goto_0
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/a/a/b$a;->a:Lkotlin/reflect/jvm/internal/impl/a/a/b;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/a/a/b;->z()Lkotlin/reflect/jvm/internal/impl/a/a/b$b;

    move-result-object v0

    sget-object v2, Lkotlin/reflect/jvm/internal/impl/a/a/b$b;->c:Lkotlin/reflect/jvm/internal/impl/a/a/b$b;

    invoke-static {v0, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 143
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/a/a/b$a;->a:Lkotlin/reflect/jvm/internal/impl/a/a/b;

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/a/a/b;->a(Lkotlin/reflect/jvm/internal/impl/a/a/b;)Lkotlin/reflect/jvm/internal/impl/b/y;

    move-result-object v0

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/b/y;->e()Lkotlin/reflect/jvm/internal/impl/b/w;

    move-result-object v0

    sget-object v2, Lkotlin/reflect/jvm/internal/impl/a/l;->c:Lkotlin/reflect/jvm/internal/impl/e/b;

    const-string v4, "BUILT_INS_PACKAGE_FQ_NAME"

    invoke-static {v2, v4}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v0, v2}, Lkotlin/reflect/jvm/internal/impl/b/w;->a(Lkotlin/reflect/jvm/internal/impl/e/b;)Lkotlin/reflect/jvm/internal/impl/b/ac;

    move-result-object v0

    .line 144
    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/b/ac;->f()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 167
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    check-cast v2, Ljava/util/Collection;

    .line 174
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    instance-of v5, v4, Lkotlin/reflect/jvm/internal/impl/a/e;

    if-eqz v5, :cond_0

    invoke-interface {v2, v4}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_1
    move-object v0, v1

    .line 138
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/a/a/b$a$a;

    iget-object v2, p0, Lkotlin/reflect/jvm/internal/impl/a/a/b$a;->a:Lkotlin/reflect/jvm/internal/impl/a/a/b;

    invoke-static {v2}, Lkotlin/reflect/jvm/internal/impl/a/a/b;->a(Lkotlin/reflect/jvm/internal/impl/a/a/b;)Lkotlin/reflect/jvm/internal/impl/b/y;

    move-result-object v2

    iget-object v4, p0, Lkotlin/reflect/jvm/internal/impl/a/a/b$a;->a:Lkotlin/reflect/jvm/internal/impl/a/a/b;

    invoke-virtual {v4}, Lkotlin/reflect/jvm/internal/impl/a/a/b;->z()Lkotlin/reflect/jvm/internal/impl/a/a/b$b;

    move-result-object v4

    invoke-virtual {v4}, Lkotlin/reflect/jvm/internal/impl/a/a/b$b;->b()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lkotlin/reflect/jvm/internal/impl/e/f;->a(Ljava/lang/String;)Lkotlin/reflect/jvm/internal/impl/e/f;

    move-result-object v4

    const-string v5, "Name.identifier(functionKind.classNamePrefix)"

    invoke-static {v4, v5}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0, v2, v4}, Lkotlin/reflect/jvm/internal/impl/a/a/b$a$a;->a(Lkotlin/reflect/jvm/internal/impl/b/y;Lkotlin/reflect/jvm/internal/impl/e/f;)V

    goto :goto_0

    .line 175
    :cond_2
    check-cast v2, Ljava/util/List;

    .line 144
    invoke-static {v2}, Lkotlin/a/m;->e(Ljava/util/List;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/a/e;

    .line 146
    check-cast v1, Lkotlin/reflect/jvm/internal/impl/a/a/b$a$a;

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/y;

    sget-object v2, Lkotlin/reflect/jvm/internal/impl/a/a/b$b;->a:Lkotlin/reflect/jvm/internal/impl/a/a/b$b;

    iget-object v4, p0, Lkotlin/reflect/jvm/internal/impl/a/a/b$a;->a:Lkotlin/reflect/jvm/internal/impl/a/a/b;

    invoke-virtual {v4}, Lkotlin/reflect/jvm/internal/impl/a/a/b;->A()I

    move-result v4

    invoke-virtual {v2, v4}, Lkotlin/reflect/jvm/internal/impl/a/a/b$b;->a(I)Lkotlin/reflect/jvm/internal/impl/e/f;

    move-result-object v2

    const-string v4, "Kind.Function.numberedClassName(arity)"

    invoke-static {v2, v4}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v1, v0, v2}, Lkotlin/reflect/jvm/internal/impl/a/a/b$a$a;->a(Lkotlin/reflect/jvm/internal/impl/b/y;Lkotlin/reflect/jvm/internal/impl/e/f;)V

    :cond_3
    move-object v0, v3

    .line 149
    check-cast v0, Ljava/util/Collection;

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/utils/a;->a(Ljava/util/Collection;)Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    return-object v0
.end method

.method public b()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/b/aq;",
            ">;"
        }
    .end annotation

    .prologue
    .line 152
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/a/a/b$a;->a:Lkotlin/reflect/jvm/internal/impl/a/a/b;

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/a/a/b;->b(Lkotlin/reflect/jvm/internal/impl/a/a/b;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public c()Lkotlin/reflect/jvm/internal/impl/a/a/b;
    .locals 1

    .prologue
    .line 154
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/a/a/b$a;->a:Lkotlin/reflect/jvm/internal/impl/a/a/b;

    return-object v0
.end method

.method public synthetic d()Lkotlin/reflect/jvm/internal/impl/b/h;
    .locals 1

    .prologue
    .line 113
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/a/a/b$a;->c()Lkotlin/reflect/jvm/internal/impl/a/a/b;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/h;

    return-object v0
.end method

.method public e()Z
    .locals 1

    .prologue
    .line 156
    const/4 v0, 0x0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 158
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/a/a/b$a;->c()Lkotlin/reflect/jvm/internal/impl/a/a/b;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/a/a/b;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
