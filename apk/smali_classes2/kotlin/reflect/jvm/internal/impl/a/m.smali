.class public final enum Lkotlin/reflect/jvm/internal/impl/a/m;
.super Ljava/lang/Enum;
.source "PrimitiveType.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lkotlin/reflect/jvm/internal/impl/a/m;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lkotlin/reflect/jvm/internal/impl/a/m;

.field public static final enum b:Lkotlin/reflect/jvm/internal/impl/a/m;

.field public static final enum c:Lkotlin/reflect/jvm/internal/impl/a/m;

.field public static final enum d:Lkotlin/reflect/jvm/internal/impl/a/m;

.field public static final enum e:Lkotlin/reflect/jvm/internal/impl/a/m;

.field public static final enum f:Lkotlin/reflect/jvm/internal/impl/a/m;

.field public static final enum g:Lkotlin/reflect/jvm/internal/impl/a/m;

.field public static final enum h:Lkotlin/reflect/jvm/internal/impl/a/m;

.field public static final i:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/a/m;",
            ">;"
        }
    .end annotation
.end field

.field private static final synthetic n:[Lkotlin/reflect/jvm/internal/impl/a/m;


# instance fields
.field private final j:Lkotlin/reflect/jvm/internal/impl/e/f;

.field private final k:Lkotlin/reflect/jvm/internal/impl/e/f;

.field private l:Lkotlin/reflect/jvm/internal/impl/e/b;

.field private m:Lkotlin/reflect/jvm/internal/impl/e/b;


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 28
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/a/m;

    const-string v1, "BOOLEAN"

    const-string v2, "Boolean"

    invoke-direct {v0, v1, v4, v2}, Lkotlin/reflect/jvm/internal/impl/a/m;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/a/m;->a:Lkotlin/reflect/jvm/internal/impl/a/m;

    .line 29
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/a/m;

    const-string v1, "CHAR"

    const-string v2, "Char"

    invoke-direct {v0, v1, v5, v2}, Lkotlin/reflect/jvm/internal/impl/a/m;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/a/m;->b:Lkotlin/reflect/jvm/internal/impl/a/m;

    .line 30
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/a/m;

    const-string v1, "BYTE"

    const-string v2, "Byte"

    invoke-direct {v0, v1, v6, v2}, Lkotlin/reflect/jvm/internal/impl/a/m;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/a/m;->c:Lkotlin/reflect/jvm/internal/impl/a/m;

    .line 31
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/a/m;

    const-string v1, "SHORT"

    const-string v2, "Short"

    invoke-direct {v0, v1, v7, v2}, Lkotlin/reflect/jvm/internal/impl/a/m;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/a/m;->d:Lkotlin/reflect/jvm/internal/impl/a/m;

    .line 32
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/a/m;

    const-string v1, "INT"

    const-string v2, "Int"

    invoke-direct {v0, v1, v8, v2}, Lkotlin/reflect/jvm/internal/impl/a/m;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/a/m;->e:Lkotlin/reflect/jvm/internal/impl/a/m;

    .line 33
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/a/m;

    const-string v1, "FLOAT"

    const/4 v2, 0x5

    const-string v3, "Float"

    invoke-direct {v0, v1, v2, v3}, Lkotlin/reflect/jvm/internal/impl/a/m;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/a/m;->f:Lkotlin/reflect/jvm/internal/impl/a/m;

    .line 34
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/a/m;

    const-string v1, "LONG"

    const/4 v2, 0x6

    const-string v3, "Long"

    invoke-direct {v0, v1, v2, v3}, Lkotlin/reflect/jvm/internal/impl/a/m;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/a/m;->g:Lkotlin/reflect/jvm/internal/impl/a/m;

    .line 35
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/a/m;

    const-string v1, "DOUBLE"

    const/4 v2, 0x7

    const-string v3, "Double"

    invoke-direct {v0, v1, v2, v3}, Lkotlin/reflect/jvm/internal/impl/a/m;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/a/m;->h:Lkotlin/reflect/jvm/internal/impl/a/m;

    .line 27
    const/16 v0, 0x8

    new-array v0, v0, [Lkotlin/reflect/jvm/internal/impl/a/m;

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/a/m;->a:Lkotlin/reflect/jvm/internal/impl/a/m;

    aput-object v1, v0, v4

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/a/m;->b:Lkotlin/reflect/jvm/internal/impl/a/m;

    aput-object v1, v0, v5

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/a/m;->c:Lkotlin/reflect/jvm/internal/impl/a/m;

    aput-object v1, v0, v6

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/a/m;->d:Lkotlin/reflect/jvm/internal/impl/a/m;

    aput-object v1, v0, v7

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/a/m;->e:Lkotlin/reflect/jvm/internal/impl/a/m;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lkotlin/reflect/jvm/internal/impl/a/m;->f:Lkotlin/reflect/jvm/internal/impl/a/m;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lkotlin/reflect/jvm/internal/impl/a/m;->g:Lkotlin/reflect/jvm/internal/impl/a/m;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lkotlin/reflect/jvm/internal/impl/a/m;->h:Lkotlin/reflect/jvm/internal/impl/a/m;

    aput-object v2, v0, v1

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/a/m;->n:[Lkotlin/reflect/jvm/internal/impl/a/m;

    .line 38
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/a/m;->b:Lkotlin/reflect/jvm/internal/impl/a/m;

    const/4 v1, 0x6

    new-array v1, v1, [Lkotlin/reflect/jvm/internal/impl/a/m;

    sget-object v2, Lkotlin/reflect/jvm/internal/impl/a/m;->c:Lkotlin/reflect/jvm/internal/impl/a/m;

    aput-object v2, v1, v4

    sget-object v2, Lkotlin/reflect/jvm/internal/impl/a/m;->d:Lkotlin/reflect/jvm/internal/impl/a/m;

    aput-object v2, v1, v5

    sget-object v2, Lkotlin/reflect/jvm/internal/impl/a/m;->e:Lkotlin/reflect/jvm/internal/impl/a/m;

    aput-object v2, v1, v6

    sget-object v2, Lkotlin/reflect/jvm/internal/impl/a/m;->f:Lkotlin/reflect/jvm/internal/impl/a/m;

    aput-object v2, v1, v7

    sget-object v2, Lkotlin/reflect/jvm/internal/impl/a/m;->g:Lkotlin/reflect/jvm/internal/impl/a/m;

    aput-object v2, v1, v8

    const/4 v2, 0x5

    sget-object v3, Lkotlin/reflect/jvm/internal/impl/a/m;->h:Lkotlin/reflect/jvm/internal/impl/a/m;

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/util/EnumSet;->of(Ljava/lang/Enum;[Ljava/lang/Enum;)Ljava/util/EnumSet;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/a/m;->i:Ljava/util/Set;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 46
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 43
    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/a/m;->l:Lkotlin/reflect/jvm/internal/impl/e/b;

    .line 44
    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/a/m;->m:Lkotlin/reflect/jvm/internal/impl/e/b;

    .line 47
    invoke-static {p3}, Lkotlin/reflect/jvm/internal/impl/e/f;->a(Ljava/lang/String;)Lkotlin/reflect/jvm/internal/impl/e/f;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/a/m;->j:Lkotlin/reflect/jvm/internal/impl/e/f;

    .line 48
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "Array"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/e/f;->a(Ljava/lang/String;)Lkotlin/reflect/jvm/internal/impl/e/f;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/a/m;->k:Lkotlin/reflect/jvm/internal/impl/e/f;

    .line 49
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lkotlin/reflect/jvm/internal/impl/a/m;
    .locals 1

    .prologue
    .line 27
    const-class v0, Lkotlin/reflect/jvm/internal/impl/a/m;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/a/m;

    return-object v0
.end method

.method public static values()[Lkotlin/reflect/jvm/internal/impl/a/m;
    .locals 1

    .prologue
    .line 27
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/a/m;->n:[Lkotlin/reflect/jvm/internal/impl/a/m;

    invoke-virtual {v0}, [Lkotlin/reflect/jvm/internal/impl/a/m;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lkotlin/reflect/jvm/internal/impl/a/m;

    return-object v0
.end method


# virtual methods
.method public a()Lkotlin/reflect/jvm/internal/impl/e/f;
    .locals 5

    .prologue
    .line 53
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/a/m;->j:Lkotlin/reflect/jvm/internal/impl/e/f;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "@NotNull method %s.%s must not return null"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "kotlin/reflect/jvm/internal/impl/builtins/PrimitiveType"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "getTypeName"

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    return-object v0
.end method

.method public b()Lkotlin/reflect/jvm/internal/impl/e/f;
    .locals 5

    .prologue
    .line 67
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/a/m;->k:Lkotlin/reflect/jvm/internal/impl/e/f;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "@NotNull method %s.%s must not return null"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "kotlin/reflect/jvm/internal/impl/builtins/PrimitiveType"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "getArrayTypeName"

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    return-object v0
.end method
