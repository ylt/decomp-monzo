.class public final Lkotlin/reflect/jvm/internal/impl/k/d/b;
.super Ljava/lang/Object;
.source "CapturedTypeApproximation.kt"


# direct methods
.method private static final a(Lkotlin/reflect/jvm/internal/impl/k/ah;)Lkotlin/reflect/jvm/internal/impl/k/ah;
    .locals 1

    .prologue
    .line 96
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/k/d/b$c;

    invoke-direct {v0}, Lkotlin/reflect/jvm/internal/impl/k/d/b$c;-><init>()V

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/ak;

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/k/am;->a(Lkotlin/reflect/jvm/internal/impl/k/ak;)Lkotlin/reflect/jvm/internal/impl/k/am;

    move-result-object v0

    .line 105
    invoke-virtual {v0, p0}, Lkotlin/reflect/jvm/internal/impl/k/am;->b(Lkotlin/reflect/jvm/internal/impl/k/ah;)Lkotlin/reflect/jvm/internal/impl/k/ah;

    move-result-object v0

    return-object v0
.end method

.method public static final a(Lkotlin/reflect/jvm/internal/impl/k/ah;Z)Lkotlin/reflect/jvm/internal/impl/k/ah;
    .locals 3

    .prologue
    .line 72
    if-nez p0, :cond_1

    const/4 p0, 0x0

    .line 92
    :cond_0
    :goto_0
    return-object p0

    .line 73
    :cond_1
    invoke-interface {p0}, Lkotlin/reflect/jvm/internal/impl/k/ah;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 75
    invoke-interface {p0}, Lkotlin/reflect/jvm/internal/impl/k/ah;->c()Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v1

    .line 76
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/k/d/b$b;->a:Lkotlin/reflect/jvm/internal/impl/k/d/b$b;

    check-cast v0, Lkotlin/d/a/b;

    invoke-static {v1, v0}, Lkotlin/reflect/jvm/internal/impl/k/an;->a(Lkotlin/reflect/jvm/internal/impl/k/r;Lkotlin/d/a/b;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 79
    invoke-interface {p0}, Lkotlin/reflect/jvm/internal/impl/k/ah;->b()Lkotlin/reflect/jvm/internal/impl/k/ap;

    move-result-object v2

    .line 80
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/k/ap;->c:Lkotlin/reflect/jvm/internal/impl/k/ap;

    invoke-static {v2, v0}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 82
    const-string v0, "type"

    invoke-static {v1, v0}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/reflect/jvm/internal/impl/k/d/b;->a(Lkotlin/reflect/jvm/internal/impl/k/r;)Lkotlin/reflect/jvm/internal/impl/k/d/a;

    move-result-object v0

    .line 83
    new-instance v1, Lkotlin/reflect/jvm/internal/impl/k/aj;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/k/d/a;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/r;

    invoke-direct {v1, v2, v0}, Lkotlin/reflect/jvm/internal/impl/k/aj;-><init>(Lkotlin/reflect/jvm/internal/impl/k/ap;Lkotlin/reflect/jvm/internal/impl/k/r;)V

    move-object v0, v1

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/ah;

    move-object p0, v0

    goto :goto_0

    .line 86
    :cond_2
    if-eqz p1, :cond_3

    .line 88
    const-string v0, "type"

    invoke-static {v1, v0}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/reflect/jvm/internal/impl/k/d/b;->a(Lkotlin/reflect/jvm/internal/impl/k/r;)Lkotlin/reflect/jvm/internal/impl/k/d/a;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/k/d/a;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/r;

    .line 89
    new-instance v1, Lkotlin/reflect/jvm/internal/impl/k/aj;

    invoke-direct {v1, v2, v0}, Lkotlin/reflect/jvm/internal/impl/k/aj;-><init>(Lkotlin/reflect/jvm/internal/impl/k/ap;Lkotlin/reflect/jvm/internal/impl/k/r;)V

    move-object v0, v1

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/ah;

    move-object p0, v0

    goto :goto_0

    .line 92
    :cond_3
    invoke-static {p0}, Lkotlin/reflect/jvm/internal/impl/k/d/b;->a(Lkotlin/reflect/jvm/internal/impl/k/ah;)Lkotlin/reflect/jvm/internal/impl/k/ah;

    move-result-object p0

    goto :goto_0
.end method

.method private static final a(Lkotlin/reflect/jvm/internal/impl/k/d/d;)Lkotlin/reflect/jvm/internal/impl/k/ah;
    .locals 4

    .prologue
    .line 46
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/k/d/d;->a()Z

    move-result v0

    sget-boolean v1, Lkotlin/p;->a:Z

    if-eqz v1, :cond_0

    if-nez v0, :cond_0

    .line 47
    sget-object v1, Lkotlin/reflect/jvm/internal/impl/g/c;->i:Lkotlin/reflect/jvm/internal/impl/g/c$a;

    sget-object v0, Lkotlin/reflect/jvm/internal/impl/k/d/b$d;->a:Lkotlin/reflect/jvm/internal/impl/k/d/b$d;

    check-cast v0, Lkotlin/d/a/b;

    invoke-virtual {v1, v0}, Lkotlin/reflect/jvm/internal/impl/g/c$a;->a(Lkotlin/d/a/b;)Lkotlin/reflect/jvm/internal/impl/g/c;

    move-result-object v1

    .line 50
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Only consistent enhanced type projection can be converted to type projection, but "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 51
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "["

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/k/d/d;->b()Lkotlin/reflect/jvm/internal/impl/b/aq;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/m;

    invoke-virtual {v1, v0}, Lkotlin/reflect/jvm/internal/impl/g/c;->a(Lkotlin/reflect/jvm/internal/impl/b/m;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, ": <"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/k/d/d;->c()Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v3

    invoke-virtual {v1, v3}, Lkotlin/reflect/jvm/internal/impl/g/c;->a(Lkotlin/reflect/jvm/internal/impl/k/r;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, ", "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/k/d/d;->d()Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v3

    invoke-virtual {v1, v3}, Lkotlin/reflect/jvm/internal/impl/g/c;->a(Lkotlin/reflect/jvm/internal/impl/k/r;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ">]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " was found"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 46
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    .line 54
    :cond_0
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/k/d/b$e;

    invoke-direct {v0, p0}, Lkotlin/reflect/jvm/internal/impl/k/d/b$e;-><init>(Lkotlin/reflect/jvm/internal/impl/k/d/d;)V

    .line 56
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/k/d/d;->c()Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v1

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/k/d/d;->d()Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v2

    invoke-static {v1, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    new-instance v0, Lkotlin/reflect/jvm/internal/impl/k/aj;

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/k/d/d;->c()Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v1

    invoke-direct {v0, v1}, Lkotlin/reflect/jvm/internal/impl/k/aj;-><init>(Lkotlin/reflect/jvm/internal/impl/k/r;)V

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/ah;

    .line 55
    :goto_0
    return-object v0

    .line 57
    :cond_1
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/k/d/d;->c()Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v1

    invoke-static {v1}, Lkotlin/reflect/jvm/internal/impl/a/l;->f(Lkotlin/reflect/jvm/internal/impl/k/r;)Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/k/d/d;->b()Lkotlin/reflect/jvm/internal/impl/b/aq;

    move-result-object v1

    invoke-interface {v1}, Lkotlin/reflect/jvm/internal/impl/b/aq;->k()Lkotlin/reflect/jvm/internal/impl/k/ap;

    move-result-object v1

    sget-object v2, Lkotlin/reflect/jvm/internal/impl/k/ap;->b:Lkotlin/reflect/jvm/internal/impl/k/ap;

    invoke-static {v1, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    xor-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_2

    .line 58
    new-instance v1, Lkotlin/reflect/jvm/internal/impl/k/aj;

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/d/b$e;

    sget-object v2, Lkotlin/reflect/jvm/internal/impl/k/ap;->c:Lkotlin/reflect/jvm/internal/impl/k/ap;

    invoke-virtual {v0, v2}, Lkotlin/reflect/jvm/internal/impl/k/d/b$e;->a(Lkotlin/reflect/jvm/internal/impl/k/ap;)Lkotlin/reflect/jvm/internal/impl/k/ap;

    move-result-object v0

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/k/d/d;->d()Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v2

    invoke-direct {v1, v0, v2}, Lkotlin/reflect/jvm/internal/impl/k/aj;-><init>(Lkotlin/reflect/jvm/internal/impl/k/ap;Lkotlin/reflect/jvm/internal/impl/k/r;)V

    move-object v0, v1

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/ah;

    goto :goto_0

    .line 59
    :cond_2
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/k/d/d;->d()Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v1

    invoke-static {v1}, Lkotlin/reflect/jvm/internal/impl/a/l;->i(Lkotlin/reflect/jvm/internal/impl/k/r;)Z

    move-result v1

    if-eqz v1, :cond_3

    new-instance v1, Lkotlin/reflect/jvm/internal/impl/k/aj;

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/d/b$e;

    sget-object v2, Lkotlin/reflect/jvm/internal/impl/k/ap;->b:Lkotlin/reflect/jvm/internal/impl/k/ap;

    invoke-virtual {v0, v2}, Lkotlin/reflect/jvm/internal/impl/k/d/b$e;->a(Lkotlin/reflect/jvm/internal/impl/k/ap;)Lkotlin/reflect/jvm/internal/impl/k/ap;

    move-result-object v0

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/k/d/d;->c()Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v2

    invoke-direct {v1, v0, v2}, Lkotlin/reflect/jvm/internal/impl/k/aj;-><init>(Lkotlin/reflect/jvm/internal/impl/k/ap;Lkotlin/reflect/jvm/internal/impl/k/r;)V

    move-object v0, v1

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/ah;

    goto :goto_0

    .line 60
    :cond_3
    new-instance v1, Lkotlin/reflect/jvm/internal/impl/k/aj;

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/d/b$e;

    sget-object v2, Lkotlin/reflect/jvm/internal/impl/k/ap;->c:Lkotlin/reflect/jvm/internal/impl/k/ap;

    invoke-virtual {v0, v2}, Lkotlin/reflect/jvm/internal/impl/k/d/b$e;->a(Lkotlin/reflect/jvm/internal/impl/k/ap;)Lkotlin/reflect/jvm/internal/impl/k/ap;

    move-result-object v0

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/k/d/d;->d()Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v2

    invoke-direct {v1, v0, v2}, Lkotlin/reflect/jvm/internal/impl/k/aj;-><init>(Lkotlin/reflect/jvm/internal/impl/k/ap;Lkotlin/reflect/jvm/internal/impl/k/r;)V

    move-object v0, v1

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/ah;

    goto :goto_0
.end method

.method public static final a(Lkotlin/reflect/jvm/internal/impl/k/r;)Lkotlin/reflect/jvm/internal/impl/k/d/a;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/reflect/jvm/internal/impl/k/r;",
            ")",
            "Lkotlin/reflect/jvm/internal/impl/k/d/a",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/k/r;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    const-string v0, "type"

    invoke-static {p0, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 110
    invoke-static {p0}, Lkotlin/reflect/jvm/internal/impl/k/o;->a(Lkotlin/reflect/jvm/internal/impl/k/r;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 111
    invoke-static {p0}, Lkotlin/reflect/jvm/internal/impl/k/o;->c(Lkotlin/reflect/jvm/internal/impl/k/r;)Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/r;

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/k/d/b;->a(Lkotlin/reflect/jvm/internal/impl/k/r;)Lkotlin/reflect/jvm/internal/impl/k/d/a;

    move-result-object v2

    .line 112
    invoke-static {p0}, Lkotlin/reflect/jvm/internal/impl/k/o;->d(Lkotlin/reflect/jvm/internal/impl/k/r;)Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/r;

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/k/d/b;->a(Lkotlin/reflect/jvm/internal/impl/k/r;)Lkotlin/reflect/jvm/internal/impl/k/d/a;

    move-result-object v3

    .line 114
    new-instance v1, Lkotlin/reflect/jvm/internal/impl/k/d/a;

    invoke-virtual {v2}, Lkotlin/reflect/jvm/internal/impl/k/d/a;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/r;

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/k/o;->c(Lkotlin/reflect/jvm/internal/impl/k/r;)Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v4

    invoke-virtual {v3}, Lkotlin/reflect/jvm/internal/impl/k/d/a;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/r;

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/k/o;->d(Lkotlin/reflect/jvm/internal/impl/k/r;)Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v0

    invoke-static {v4, v0}, Lkotlin/reflect/jvm/internal/impl/k/s;->a(Lkotlin/reflect/jvm/internal/impl/k/v;Lkotlin/reflect/jvm/internal/impl/k/v;)Lkotlin/reflect/jvm/internal/impl/k/ao;

    move-result-object v4

    invoke-virtual {v2}, Lkotlin/reflect/jvm/internal/impl/k/d/a;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/r;

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/k/o;->c(Lkotlin/reflect/jvm/internal/impl/k/r;)Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v2

    invoke-virtual {v3}, Lkotlin/reflect/jvm/internal/impl/k/d/a;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/r;

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/k/o;->d(Lkotlin/reflect/jvm/internal/impl/k/r;)Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v0

    invoke-static {v2, v0}, Lkotlin/reflect/jvm/internal/impl/k/s;->a(Lkotlin/reflect/jvm/internal/impl/k/v;Lkotlin/reflect/jvm/internal/impl/k/v;)Lkotlin/reflect/jvm/internal/impl/k/ao;

    move-result-object v0

    invoke-direct {v1, v4, v0}, Lkotlin/reflect/jvm/internal/impl/k/d/a;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    move-object v0, v1

    .line 151
    :goto_0
    return-object v0

    .line 119
    :cond_0
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/k/r;->i()Lkotlin/reflect/jvm/internal/impl/k/ad;

    move-result-object v0

    .line 120
    invoke-static {p0}, Lkotlin/reflect/jvm/internal/impl/h/a/a/c;->a(Lkotlin/reflect/jvm/internal/impl/k/r;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 121
    if-nez v0, :cond_1

    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type org.jetbrains.kotlin.resolve.calls.inference.CapturedTypeConstructor"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/h/a/a/b;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/h/a/a/b;->h()Lkotlin/reflect/jvm/internal/impl/k/ah;

    move-result-object v1

    .line 122
    new-instance v2, Lkotlin/reflect/jvm/internal/impl/k/d/b$a;

    invoke-direct {v2, p0}, Lkotlin/reflect/jvm/internal/impl/k/d/b$a;-><init>(Lkotlin/reflect/jvm/internal/impl/k/r;)V

    .line 123
    invoke-interface {v1}, Lkotlin/reflect/jvm/internal/impl/k/ah;->c()Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v0

    invoke-virtual {v2, v0}, Lkotlin/reflect/jvm/internal/impl/k/d/b$a;->a(Lkotlin/reflect/jvm/internal/impl/k/r;)Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v3

    .line 125
    invoke-interface {v1}, Lkotlin/reflect/jvm/internal/impl/k/ah;->b()Lkotlin/reflect/jvm/internal/impl/k/ap;

    move-result-object v0

    sget-object v4, Lkotlin/reflect/jvm/internal/impl/k/d/c;->b:[I

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/k/ap;->ordinal()I

    move-result v0

    aget v0, v4, v0

    packed-switch v0, :pswitch_data_0

    .line 128
    new-instance v0, Ljava/lang/AssertionError;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Only nontrivial projections should have been captured, not: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    .line 126
    :pswitch_0
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/k/d/a;

    const-string v1, "bound"

    invoke-static {v3, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p0}, Lkotlin/reflect/jvm/internal/impl/k/c/a;->a(Lkotlin/reflect/jvm/internal/impl/k/r;)Lkotlin/reflect/jvm/internal/impl/a/l;

    move-result-object v1

    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/impl/a/l;->y()Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v1

    const-string v2, "type.builtIns.nullableAnyType"

    invoke-static {v1, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v0, v3, v1}, Lkotlin/reflect/jvm/internal/impl/k/d/a;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_0

    .line 127
    :pswitch_1
    new-instance v1, Lkotlin/reflect/jvm/internal/impl/k/d/a;

    invoke-static {p0}, Lkotlin/reflect/jvm/internal/impl/k/c/a;->a(Lkotlin/reflect/jvm/internal/impl/k/r;)Lkotlin/reflect/jvm/internal/impl/a/l;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/a/l;->v()Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/r;

    invoke-virtual {v2, v0}, Lkotlin/reflect/jvm/internal/impl/k/d/b$a;->a(Lkotlin/reflect/jvm/internal/impl/k/r;)Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v0

    invoke-direct {v1, v0, v3}, Lkotlin/reflect/jvm/internal/impl/k/d/a;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    move-object v0, v1

    goto :goto_0

    .line 131
    :cond_2
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/k/r;->a()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_3

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/k/r;->a()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/k/ad;->b()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-eq v1, v2, :cond_4

    .line 132
    :cond_3
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/k/d/a;

    invoke-direct {v0, p0, p0}, Lkotlin/reflect/jvm/internal/impl/k/d/a;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 134
    :cond_4
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 135
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 136
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/k/r;->a()Ljava/util/List;

    move-result-object v1

    check-cast v1, Ljava/lang/Iterable;

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/k/ad;->b()Ljava/util/List;

    move-result-object v0

    const-string v6, "typeConstructor.parameters"

    invoke-static {v0, v6}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Iterable;

    invoke-static {v1, v0}, Lkotlin/a/m;->a(Ljava/lang/Iterable;Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/h;

    invoke-virtual {v0}, Lkotlin/h;->c()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lkotlin/reflect/jvm/internal/impl/k/ah;

    invoke-virtual {v0}, Lkotlin/h;->d()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/aq;

    .line 137
    const-string v7, "typeParameter"

    invoke-static {v0, v7}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v1, v0}, Lkotlin/reflect/jvm/internal/impl/k/d/b;->a(Lkotlin/reflect/jvm/internal/impl/k/ah;Lkotlin/reflect/jvm/internal/impl/b/aq;)Lkotlin/reflect/jvm/internal/impl/k/d/d;

    move-result-object v0

    .line 140
    invoke-interface {v1}, Lkotlin/reflect/jvm/internal/impl/k/ah;->a()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 141
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 142
    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 145
    :cond_5
    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/k/d/b;->b(Lkotlin/reflect/jvm/internal/impl/k/d/d;)Lkotlin/reflect/jvm/internal/impl/k/d/a;

    move-result-object v1

    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/impl/k/d/a;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/d/d;

    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/impl/k/d/a;->d()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lkotlin/reflect/jvm/internal/impl/k/d/d;

    .line 146
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 147
    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_6
    move-object v0, v2

    .line 150
    check-cast v0, Ljava/lang/Iterable;

    .line 169
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_7
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_9

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/d/d;

    .line 150
    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/k/d/d;->a()Z

    move-result v0

    if-nez v0, :cond_8

    move v0, v4

    :goto_2
    if-eqz v0, :cond_7

    move v0, v4

    .line 151
    :goto_3
    new-instance v4, Lkotlin/reflect/jvm/internal/impl/k/d/a;

    if-eqz v0, :cond_a

    invoke-static {p0}, Lkotlin/reflect/jvm/internal/impl/k/c/a;->a(Lkotlin/reflect/jvm/internal/impl/k/r;)Lkotlin/reflect/jvm/internal/impl/a/l;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/a/l;->v()Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v0

    const-string v1, "type.builtIns.nothingType"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/r;

    move-object v1, v0

    :goto_4
    move-object v0, v3

    check-cast v0, Ljava/util/List;

    invoke-static {p0, v0}, Lkotlin/reflect/jvm/internal/impl/k/d/b;->a(Lkotlin/reflect/jvm/internal/impl/k/r;Ljava/util/List;)Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v0

    invoke-direct {v4, v1, v0}, Lkotlin/reflect/jvm/internal/impl/k/d/a;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    move-object v0, v4

    goto/16 :goto_0

    :cond_8
    move v0, v5

    .line 150
    goto :goto_2

    :cond_9
    move v0, v5

    .line 170
    goto :goto_3

    .line 151
    :cond_a
    check-cast v2, Ljava/util/List;

    invoke-static {p0, v2}, Lkotlin/reflect/jvm/internal/impl/k/d/b;->a(Lkotlin/reflect/jvm/internal/impl/k/r;Ljava/util/List;)Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v0

    move-object v1, v0

    goto :goto_4

    .line 125
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private static final a(Lkotlin/reflect/jvm/internal/impl/k/ah;Lkotlin/reflect/jvm/internal/impl/b/aq;)Lkotlin/reflect/jvm/internal/impl/k/d/d;
    .locals 4

    .prologue
    .line 65
    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/b/aq;->k()Lkotlin/reflect/jvm/internal/impl/k/ap;

    move-result-object v0

    invoke-static {v0, p0}, Lkotlin/reflect/jvm/internal/impl/k/am;->a(Lkotlin/reflect/jvm/internal/impl/k/ap;Lkotlin/reflect/jvm/internal/impl/k/ah;)Lkotlin/reflect/jvm/internal/impl/k/ap;

    move-result-object v0

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/k/d/c;->a:[I

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/k/ap;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 68
    new-instance v0, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {v0}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw v0

    .line 66
    :pswitch_0
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/k/d/d;

    invoke-interface {p0}, Lkotlin/reflect/jvm/internal/impl/k/ah;->c()Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v1

    const-string v2, "type"

    invoke-static {v1, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {p0}, Lkotlin/reflect/jvm/internal/impl/k/ah;->c()Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v2

    const-string v3, "type"

    invoke-static {v2, v3}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v0, p1, v1, v2}, Lkotlin/reflect/jvm/internal/impl/k/d/d;-><init>(Lkotlin/reflect/jvm/internal/impl/b/aq;Lkotlin/reflect/jvm/internal/impl/k/r;Lkotlin/reflect/jvm/internal/impl/k/r;)V

    .line 69
    :goto_0
    return-object v0

    .line 67
    :pswitch_1
    new-instance v1, Lkotlin/reflect/jvm/internal/impl/k/d/d;

    invoke-interface {p0}, Lkotlin/reflect/jvm/internal/impl/k/ah;->c()Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v2

    const-string v0, "type"

    invoke-static {v2, v0}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v0, p1

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/m;

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/h/c/a;->d(Lkotlin/reflect/jvm/internal/impl/b/m;)Lkotlin/reflect/jvm/internal/impl/a/l;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/a/l;->y()Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v0

    const-string v3, "typeParameter.builtIns.nullableAnyType"

    invoke-static {v0, v3}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/r;

    invoke-direct {v1, p1, v2, v0}, Lkotlin/reflect/jvm/internal/impl/k/d/d;-><init>(Lkotlin/reflect/jvm/internal/impl/b/aq;Lkotlin/reflect/jvm/internal/impl/k/r;Lkotlin/reflect/jvm/internal/impl/k/r;)V

    move-object v0, v1

    goto :goto_0

    .line 68
    :pswitch_2
    new-instance v1, Lkotlin/reflect/jvm/internal/impl/k/d/d;

    move-object v0, p1

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/m;

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/h/c/a;->d(Lkotlin/reflect/jvm/internal/impl/b/m;)Lkotlin/reflect/jvm/internal/impl/a/l;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/a/l;->v()Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v0

    const-string v2, "typeParameter.builtIns.nothingType"

    invoke-static {v0, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/r;

    invoke-interface {p0}, Lkotlin/reflect/jvm/internal/impl/k/ah;->c()Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v2

    const-string v3, "type"

    invoke-static {v2, v3}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v1, p1, v0, v2}, Lkotlin/reflect/jvm/internal/impl/k/d/d;-><init>(Lkotlin/reflect/jvm/internal/impl/b/aq;Lkotlin/reflect/jvm/internal/impl/k/r;Lkotlin/reflect/jvm/internal/impl/k/r;)V

    move-object v0, v1

    goto :goto_0

    .line 65
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private static final a(Lkotlin/reflect/jvm/internal/impl/k/r;Ljava/util/List;)Lkotlin/reflect/jvm/internal/impl/k/r;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/reflect/jvm/internal/impl/k/r;",
            "Ljava/util/List",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/k/d/d;",
            ">;)",
            "Lkotlin/reflect/jvm/internal/impl/k/r;"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 157
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/k/r;->a()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    sget-boolean v1, Lkotlin/p;->a:Z

    if-eqz v1, :cond_1

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Incorrect type arguments "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 158
    :cond_1
    check-cast p1, Ljava/lang/Iterable;

    .line 171
    new-instance v0, Ljava/util/ArrayList;

    const/16 v1, 0xa

    invoke-static {p1, v1}, Lkotlin/a/m;->a(Ljava/lang/Iterable;I)I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v0, Ljava/util/Collection;

    .line 172
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 173
    check-cast v1, Lkotlin/reflect/jvm/internal/impl/k/d/d;

    .line 158
    invoke-static {v1}, Lkotlin/reflect/jvm/internal/impl/k/d/b;->a(Lkotlin/reflect/jvm/internal/impl/k/d/d;)Lkotlin/reflect/jvm/internal/impl/k/ah;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 174
    :cond_2
    check-cast v0, Ljava/util/List;

    .line 158
    const/4 v1, 0x2

    invoke-static {p0, v0, v3, v1, v3}, Lkotlin/reflect/jvm/internal/impl/k/al;->a(Lkotlin/reflect/jvm/internal/impl/k/r;Ljava/util/List;Lkotlin/reflect/jvm/internal/impl/b/a/h;ILjava/lang/Object;)Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v0

    return-object v0
.end method

.method private static final b(Lkotlin/reflect/jvm/internal/impl/k/d/d;)Lkotlin/reflect/jvm/internal/impl/k/d/a;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/reflect/jvm/internal/impl/k/d/d;",
            ")",
            "Lkotlin/reflect/jvm/internal/impl/k/d/a",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/k/d/d;",
            ">;"
        }
    .end annotation

    .prologue
    .line 162
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/k/d/d;->c()Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v0

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/k/d/b;->a(Lkotlin/reflect/jvm/internal/impl/k/r;)Lkotlin/reflect/jvm/internal/impl/k/d/a;

    move-result-object v1

    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/impl/k/d/a;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/r;

    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/impl/k/d/a;->d()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lkotlin/reflect/jvm/internal/impl/k/r;

    .line 163
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/k/d/d;->d()Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v2

    invoke-static {v2}, Lkotlin/reflect/jvm/internal/impl/k/d/b;->a(Lkotlin/reflect/jvm/internal/impl/k/r;)Lkotlin/reflect/jvm/internal/impl/k/d/a;

    move-result-object v3

    invoke-virtual {v3}, Lkotlin/reflect/jvm/internal/impl/k/d/a;->c()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lkotlin/reflect/jvm/internal/impl/k/r;

    invoke-virtual {v3}, Lkotlin/reflect/jvm/internal/impl/k/d/a;->d()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lkotlin/reflect/jvm/internal/impl/k/r;

    .line 164
    new-instance v4, Lkotlin/reflect/jvm/internal/impl/k/d/a;

    new-instance v5, Lkotlin/reflect/jvm/internal/impl/k/d/d;

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/k/d/d;->b()Lkotlin/reflect/jvm/internal/impl/b/aq;

    move-result-object v6

    invoke-direct {v5, v6, v1, v2}, Lkotlin/reflect/jvm/internal/impl/k/d/d;-><init>(Lkotlin/reflect/jvm/internal/impl/b/aq;Lkotlin/reflect/jvm/internal/impl/k/r;Lkotlin/reflect/jvm/internal/impl/k/r;)V

    new-instance v1, Lkotlin/reflect/jvm/internal/impl/k/d/d;

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/k/d/d;->b()Lkotlin/reflect/jvm/internal/impl/b/aq;

    move-result-object v2

    invoke-direct {v1, v2, v0, v3}, Lkotlin/reflect/jvm/internal/impl/k/d/d;-><init>(Lkotlin/reflect/jvm/internal/impl/b/aq;Lkotlin/reflect/jvm/internal/impl/k/r;Lkotlin/reflect/jvm/internal/impl/k/r;)V

    invoke-direct {v4, v5, v1}, Lkotlin/reflect/jvm/internal/impl/k/d/a;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    return-object v4
.end method
