.class public final Lkotlin/reflect/jvm/internal/impl/k/z;
.super Ljava/lang/Object;
.source "StarProjectionImpl.kt"


# direct methods
.method public static final a(Lkotlin/reflect/jvm/internal/impl/b/aq;)Lkotlin/reflect/jvm/internal/impl/k/r;
    .locals 3

    .prologue
    const-string v0, "$receiver"

    invoke-static {p0, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 40
    invoke-interface {p0}, Lkotlin/reflect/jvm/internal/impl/b/aq;->y_()Lkotlin/reflect/jvm/internal/impl/b/m;

    move-result-object v0

    if-nez v0, :cond_0

    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type org.jetbrains.kotlin.descriptors.ClassifierDescriptorWithTypeParameters"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/i;

    .line 41
    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/b/i;->e()Lkotlin/reflect/jvm/internal/impl/k/ad;

    move-result-object v0

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/k/ad;->b()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 63
    new-instance v1, Ljava/util/ArrayList;

    const/16 v2, 0xa

    invoke-static {v0, v2}, Lkotlin/a/m;->a(Ljava/lang/Iterable;I)I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v1, Ljava/util/Collection;

    .line 64
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .line 65
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/aq;

    .line 41
    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/b/aq;->e()Lkotlin/reflect/jvm/internal/impl/k/ad;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 66
    :cond_1
    check-cast v1, Ljava/util/List;

    .line 42
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/k/z$a;

    invoke-direct {v0, v1}, Lkotlin/reflect/jvm/internal/impl/k/z$a;-><init>(Ljava/util/List;)V

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/ak;

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/k/am;->a(Lkotlin/reflect/jvm/internal/impl/k/ak;)Lkotlin/reflect/jvm/internal/impl/k/am;

    move-result-object v1

    invoke-interface {p0}, Lkotlin/reflect/jvm/internal/impl/b/aq;->I_()Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lkotlin/a/m;->e(Ljava/util/List;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/r;

    sget-object v2, Lkotlin/reflect/jvm/internal/impl/k/ap;->c:Lkotlin/reflect/jvm/internal/impl/k/ap;

    .line 50
    invoke-virtual {v1, v0, v2}, Lkotlin/reflect/jvm/internal/impl/k/am;->b(Lkotlin/reflect/jvm/internal/impl/k/r;Lkotlin/reflect/jvm/internal/impl/k/ap;)Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v0

    if-eqz v0, :cond_2

    :goto_1
    return-object v0

    :cond_2
    check-cast p0, Lkotlin/reflect/jvm/internal/impl/b/m;

    invoke-static {p0}, Lkotlin/reflect/jvm/internal/impl/h/c/a;->d(Lkotlin/reflect/jvm/internal/impl/b/m;)Lkotlin/reflect/jvm/internal/impl/a/l;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/a/l;->z()Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v0

    const-string v1, "builtIns.defaultBound"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/r;

    goto :goto_1
.end method
