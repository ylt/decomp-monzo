.class public final Lkotlin/reflect/jvm/internal/impl/k/x;
.super Ljava/lang/Object;
.source "SpecialTypes.kt"


# direct methods
.method public static final a(Lkotlin/reflect/jvm/internal/impl/k/r;)Lkotlin/reflect/jvm/internal/impl/k/a;
    .locals 2

    .prologue
    const-string v0, "$receiver"

    invoke-static {p0, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 45
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/k/r;->j()Lkotlin/reflect/jvm/internal/impl/k/ao;

    move-result-object v0

    instance-of v1, v0, Lkotlin/reflect/jvm/internal/impl/k/a;

    if-nez v1, :cond_0

    const/4 v0, 0x0

    :cond_0
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/a;

    return-object v0
.end method

.method public static final a(Lkotlin/reflect/jvm/internal/impl/k/v;Lkotlin/reflect/jvm/internal/impl/k/v;)Lkotlin/reflect/jvm/internal/impl/k/v;
    .locals 1

    .prologue
    const-string v0, "$receiver"

    invoke-static {p0, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "abbreviatedType"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 49
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/k/v;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 50
    :goto_0
    return-object p0

    :cond_0
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/k/a;

    invoke-direct {v0, p0, p1}, Lkotlin/reflect/jvm/internal/impl/k/a;-><init>(Lkotlin/reflect/jvm/internal/impl/k/v;Lkotlin/reflect/jvm/internal/impl/k/v;)V

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/v;

    move-object p0, v0

    goto :goto_0
.end method

.method public static final b(Lkotlin/reflect/jvm/internal/impl/k/r;)Lkotlin/reflect/jvm/internal/impl/k/v;
    .locals 1

    .prologue
    const-string v0, "$receiver"

    invoke-static {p0, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 46
    invoke-static {p0}, Lkotlin/reflect/jvm/internal/impl/k/x;->a(Lkotlin/reflect/jvm/internal/impl/k/r;)Lkotlin/reflect/jvm/internal/impl/k/a;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/k/a;->g()Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
