.class public final Lkotlin/reflect/jvm/internal/impl/k/a/e;
.super Lkotlin/reflect/jvm/internal/impl/k/v;
.source "NewCapturedType.kt"


# instance fields
.field private final a:Lkotlin/reflect/jvm/internal/impl/k/a/a;

.field private final b:Lkotlin/reflect/jvm/internal/impl/k/a/f;

.field private final c:Lkotlin/reflect/jvm/internal/impl/k/ao;

.field private final d:Lkotlin/reflect/jvm/internal/impl/b/a/h;

.field private final e:Z


# direct methods
.method public constructor <init>(Lkotlin/reflect/jvm/internal/impl/k/a/a;Lkotlin/reflect/jvm/internal/impl/k/a/f;Lkotlin/reflect/jvm/internal/impl/k/ao;Lkotlin/reflect/jvm/internal/impl/b/a/h;Z)V
    .locals 1

    .prologue
    const-string v0, "captureStatus"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "constructor"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "annotations"

    invoke-static {p4, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 88
    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/k/v;-><init>()V

    iput-object p1, p0, Lkotlin/reflect/jvm/internal/impl/k/a/e;->a:Lkotlin/reflect/jvm/internal/impl/k/a/a;

    iput-object p2, p0, Lkotlin/reflect/jvm/internal/impl/k/a/e;->b:Lkotlin/reflect/jvm/internal/impl/k/a/f;

    iput-object p3, p0, Lkotlin/reflect/jvm/internal/impl/k/a/e;->c:Lkotlin/reflect/jvm/internal/impl/k/ao;

    iput-object p4, p0, Lkotlin/reflect/jvm/internal/impl/k/a/e;->d:Lkotlin/reflect/jvm/internal/impl/b/a/h;

    iput-boolean p5, p0, Lkotlin/reflect/jvm/internal/impl/k/a/e;->e:Z

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/reflect/jvm/internal/impl/k/a/a;Lkotlin/reflect/jvm/internal/impl/k/a/f;Lkotlin/reflect/jvm/internal/impl/k/ao;Lkotlin/reflect/jvm/internal/impl/b/a/h;ZILkotlin/d/b/i;)V
    .locals 6

    .prologue
    and-int/lit8 v0, p6, 0x8

    if-eqz v0, :cond_1

    .line 86
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/b/a/h;->a:Lkotlin/reflect/jvm/internal/impl/b/a/h$a;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/b/a/h$a;->a()Lkotlin/reflect/jvm/internal/impl/b/a/h;

    move-result-object v4

    :goto_0
    and-int/lit8 v0, p6, 0x10

    if-eqz v0, :cond_0

    .line 87
    const/4 v5, 0x0

    :goto_1
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    invoke-direct/range {v0 .. v5}, Lkotlin/reflect/jvm/internal/impl/k/a/e;-><init>(Lkotlin/reflect/jvm/internal/impl/k/a/a;Lkotlin/reflect/jvm/internal/impl/k/a/f;Lkotlin/reflect/jvm/internal/impl/k/ao;Lkotlin/reflect/jvm/internal/impl/b/a/h;Z)V

    return-void

    :cond_0
    move v5, p5

    goto :goto_1

    :cond_1
    move-object v4, p4

    goto :goto_0
.end method

.method public constructor <init>(Lkotlin/reflect/jvm/internal/impl/k/a/a;Lkotlin/reflect/jvm/internal/impl/k/ao;Lkotlin/reflect/jvm/internal/impl/k/ah;)V
    .locals 8

    .prologue
    const/4 v4, 0x0

    const-string v0, "captureStatus"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "projection"

    invoke-static {p3, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 90
    new-instance v2, Lkotlin/reflect/jvm/internal/impl/k/a/f;

    const/4 v0, 0x2

    invoke-direct {v2, p3, v4, v0, v4}, Lkotlin/reflect/jvm/internal/impl/k/a/f;-><init>(Lkotlin/reflect/jvm/internal/impl/k/ah;Ljava/util/List;ILkotlin/d/b/i;)V

    const/4 v5, 0x0

    const/16 v6, 0x18

    move-object v0, p0

    move-object v1, p1

    move-object v3, p2

    move-object v7, v4

    invoke-direct/range {v0 .. v7}, Lkotlin/reflect/jvm/internal/impl/k/a/e;-><init>(Lkotlin/reflect/jvm/internal/impl/k/a/a;Lkotlin/reflect/jvm/internal/impl/k/a/f;Lkotlin/reflect/jvm/internal/impl/k/ao;Lkotlin/reflect/jvm/internal/impl/b/a/h;ZILkotlin/d/b/i;)V

    return-void
.end method


# virtual methods
.method public a()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/k/ah;",
            ">;"
        }
    .end annotation

    .prologue
    .line 92
    invoke-static {}, Lkotlin/a/m;->a()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public a(Lkotlin/reflect/jvm/internal/impl/b/a/h;)Lkotlin/reflect/jvm/internal/impl/k/a/e;
    .locals 6

    .prologue
    const-string v0, "newAnnotations"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 98
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/k/a/e;

    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/k/a/e;->a:Lkotlin/reflect/jvm/internal/impl/k/a/a;

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/k/a/e;->e()Lkotlin/reflect/jvm/internal/impl/k/a/f;

    move-result-object v2

    iget-object v3, p0, Lkotlin/reflect/jvm/internal/impl/k/a/e;->c:Lkotlin/reflect/jvm/internal/impl/k/ao;

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/k/a/e;->c()Z

    move-result v5

    move-object v4, p1

    invoke-direct/range {v0 .. v5}, Lkotlin/reflect/jvm/internal/impl/k/a/e;-><init>(Lkotlin/reflect/jvm/internal/impl/k/a/a;Lkotlin/reflect/jvm/internal/impl/k/a/f;Lkotlin/reflect/jvm/internal/impl/k/ao;Lkotlin/reflect/jvm/internal/impl/b/a/h;Z)V

    return-object v0
.end method

.method public synthetic a(Z)Lkotlin/reflect/jvm/internal/impl/k/v;
    .locals 1

    .prologue
    .line 82
    invoke-virtual {p0, p1}, Lkotlin/reflect/jvm/internal/impl/k/a/e;->c(Z)Lkotlin/reflect/jvm/internal/impl/k/a/e;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/v;

    return-object v0
.end method

.method public b()Lkotlin/reflect/jvm/internal/impl/h/e/h;
    .locals 2

    .prologue
    .line 94
    const-string v0, "No member resolution should be done on captured type!"

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lkotlin/reflect/jvm/internal/impl/k/l;->a(Ljava/lang/String;Z)Lkotlin/reflect/jvm/internal/impl/h/e/h;

    move-result-object v0

    const-string v1, "ErrorUtils.createErrorSc\u2026on captured type!\", true)"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public synthetic b(Lkotlin/reflect/jvm/internal/impl/b/a/h;)Lkotlin/reflect/jvm/internal/impl/k/ao;
    .locals 1

    .prologue
    .line 82
    invoke-virtual {p0, p1}, Lkotlin/reflect/jvm/internal/impl/k/a/e;->a(Lkotlin/reflect/jvm/internal/impl/b/a/h;)Lkotlin/reflect/jvm/internal/impl/k/a/e;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/ao;

    return-object v0
.end method

.method public synthetic b(Z)Lkotlin/reflect/jvm/internal/impl/k/ao;
    .locals 1

    .prologue
    .line 82
    invoke-virtual {p0, p1}, Lkotlin/reflect/jvm/internal/impl/k/a/e;->c(Z)Lkotlin/reflect/jvm/internal/impl/k/a/e;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/ao;

    return-object v0
.end method

.method public c(Z)Lkotlin/reflect/jvm/internal/impl/k/a/e;
    .locals 6

    .prologue
    .line 101
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/k/a/e;

    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/k/a/e;->a:Lkotlin/reflect/jvm/internal/impl/k/a/a;

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/k/a/e;->e()Lkotlin/reflect/jvm/internal/impl/k/a/f;

    move-result-object v2

    iget-object v3, p0, Lkotlin/reflect/jvm/internal/impl/k/a/e;->c:Lkotlin/reflect/jvm/internal/impl/k/ao;

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/k/a/e;->w()Lkotlin/reflect/jvm/internal/impl/b/a/h;

    move-result-object v4

    move v5, p1

    invoke-direct/range {v0 .. v5}, Lkotlin/reflect/jvm/internal/impl/k/a/e;-><init>(Lkotlin/reflect/jvm/internal/impl/k/a/a;Lkotlin/reflect/jvm/internal/impl/k/a/f;Lkotlin/reflect/jvm/internal/impl/k/ao;Lkotlin/reflect/jvm/internal/impl/b/a/h;Z)V

    return-object v0
.end method

.method public synthetic c(Lkotlin/reflect/jvm/internal/impl/b/a/h;)Lkotlin/reflect/jvm/internal/impl/k/v;
    .locals 1

    .prologue
    .line 82
    invoke-virtual {p0, p1}, Lkotlin/reflect/jvm/internal/impl/k/a/e;->a(Lkotlin/reflect/jvm/internal/impl/b/a/h;)Lkotlin/reflect/jvm/internal/impl/k/a/e;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/v;

    return-object v0
.end method

.method public c()Z
    .locals 1

    .prologue
    .line 87
    iget-boolean v0, p0, Lkotlin/reflect/jvm/internal/impl/k/a/e;->e:Z

    return v0
.end method

.method public d()Z
    .locals 1

    .prologue
    .line 95
    const/4 v0, 0x0

    return v0
.end method

.method public e()Lkotlin/reflect/jvm/internal/impl/k/a/f;
    .locals 1

    .prologue
    .line 84
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/k/a/e;->b:Lkotlin/reflect/jvm/internal/impl/k/a/f;

    return-object v0
.end method

.method public final f()Lkotlin/reflect/jvm/internal/impl/k/ao;
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/k/a/e;->c:Lkotlin/reflect/jvm/internal/impl/k/ao;

    return-object v0
.end method

.method public synthetic i()Lkotlin/reflect/jvm/internal/impl/k/ad;
    .locals 1

    .prologue
    .line 82
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/k/a/e;->e()Lkotlin/reflect/jvm/internal/impl/k/a/f;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/ad;

    return-object v0
.end method

.method public w()Lkotlin/reflect/jvm/internal/impl/b/a/h;
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/k/a/e;->d:Lkotlin/reflect/jvm/internal/impl/b/a/h;

    return-object v0
.end method
