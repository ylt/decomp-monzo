.class public abstract Lkotlin/reflect/jvm/internal/impl/k/r;
.super Ljava/lang/Object;
.source "KotlinType.kt"

# interfaces
.implements Lkotlin/reflect/jvm/internal/impl/b/a/a;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/d/b/i;)V
    .locals 0

    .prologue
    .line 43
    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/k/r;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract a()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/k/ah;",
            ">;"
        }
    .end annotation
.end method

.method public abstract b()Lkotlin/reflect/jvm/internal/impl/h/e/h;
.end method

.method public abstract c()Z
.end method

.method public abstract d()Z
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 63
    move-object v0, p0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/r;

    if-ne v0, p1, :cond_0

    .line 66
    :goto_0
    return v1

    .line 64
    :cond_0
    instance-of v0, p1, Lkotlin/reflect/jvm/internal/impl/k/r;

    if-nez v0, :cond_1

    move v1, v2

    goto :goto_0

    .line 66
    :cond_1
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/k/r;->c()Z

    move-result v3

    move-object v0, p1

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/r;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/k/r;->c()Z

    move-result v0

    if-ne v3, v0, :cond_2

    sget-object v0, Lkotlin/reflect/jvm/internal/impl/k/a/l;->a:Lkotlin/reflect/jvm/internal/impl/k/a/l;

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/k/r;->j()Lkotlin/reflect/jvm/internal/impl/k/ao;

    move-result-object v3

    check-cast p1, Lkotlin/reflect/jvm/internal/impl/k/r;

    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/k/r;->j()Lkotlin/reflect/jvm/internal/impl/k/ao;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Lkotlin/reflect/jvm/internal/impl/k/a/l;->a(Lkotlin/reflect/jvm/internal/impl/k/ao;Lkotlin/reflect/jvm/internal/impl/k/ao;)Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v1

    :goto_1
    move v1, v0

    goto :goto_0

    :cond_2
    move v0, v2

    goto :goto_1
.end method

.method public final hashCode()I
    .locals 2

    .prologue
    .line 54
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/k/r;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-super {p0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    .line 59
    :goto_0
    return v0

    .line 56
    :cond_0
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/k/r;->i()Lkotlin/reflect/jvm/internal/impl/k/ad;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    .line 57
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/k/r;->a()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 58
    mul-int/lit8 v1, v0, 0x1f

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/k/r;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_1
    add-int/2addr v0, v1

    .line 59
    goto :goto_0

    .line 58
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public abstract i()Lkotlin/reflect/jvm/internal/impl/k/ad;
.end method

.method public abstract j()Lkotlin/reflect/jvm/internal/impl/k/ao;
.end method
