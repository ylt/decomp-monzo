.class public final Lkotlin/reflect/jvm/internal/impl/k/s;
.super Ljava/lang/Object;
.source "KotlinTypeFactory.kt"


# static fields
.field public static final a:Lkotlin/reflect/jvm/internal/impl/k/s;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 27
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/k/s;

    invoke-direct {v0}, Lkotlin/reflect/jvm/internal/impl/k/s;-><init>()V

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    check-cast p0, Lkotlin/reflect/jvm/internal/impl/k/s;

    sput-object p0, Lkotlin/reflect/jvm/internal/impl/k/s;->a:Lkotlin/reflect/jvm/internal/impl/k/s;

    return-void
.end method

.method private final a(Lkotlin/reflect/jvm/internal/impl/k/ad;Ljava/util/List;)Lkotlin/reflect/jvm/internal/impl/h/e/h;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/reflect/jvm/internal/impl/k/ad;",
            "Ljava/util/List",
            "<+",
            "Lkotlin/reflect/jvm/internal/impl/k/ah;",
            ">;)",
            "Lkotlin/reflect/jvm/internal/impl/h/e/h;"
        }
    .end annotation

    .prologue
    .line 29
    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/k/ad;->d()Lkotlin/reflect/jvm/internal/impl/b/h;

    move-result-object v0

    .line 31
    instance-of v1, v0, Lkotlin/reflect/jvm/internal/impl/b/aq;

    if-eqz v1, :cond_0

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/b/h;->E_()Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/k/v;->b()Lkotlin/reflect/jvm/internal/impl/h/e/h;

    move-result-object v0

    .line 30
    :goto_0
    return-object v0

    .line 32
    :cond_0
    instance-of v1, v0, Lkotlin/reflect/jvm/internal/impl/b/e;

    if-eqz v1, :cond_1

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/e;

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/k/ae;->b:Lkotlin/reflect/jvm/internal/impl/k/ae$a;

    invoke-virtual {v1, p1, p2}, Lkotlin/reflect/jvm/internal/impl/k/ae$a;->a(Lkotlin/reflect/jvm/internal/impl/k/ad;Ljava/util/List;)Lkotlin/reflect/jvm/internal/impl/k/ak;

    move-result-object v1

    invoke-interface {v0, v1}, Lkotlin/reflect/jvm/internal/impl/b/e;->a(Lkotlin/reflect/jvm/internal/impl/k/ak;)Lkotlin/reflect/jvm/internal/impl/h/e/h;

    move-result-object v0

    const-string v1, "descriptor.getMemberScop\u2026(constructor, arguments))"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    .line 33
    :cond_1
    instance-of v1, v0, Lkotlin/reflect/jvm/internal/impl/b/ap;

    if-eqz v1, :cond_2

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Scope for abbreviation: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/ap;

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/b/ap;->D_()Lkotlin/reflect/jvm/internal/impl/e/f;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lkotlin/reflect/jvm/internal/impl/k/l;->a(Ljava/lang/String;Z)Lkotlin/reflect/jvm/internal/impl/h/e/h;

    move-result-object v0

    const-string v1, "ErrorUtils.createErrorSc\u2026{descriptor.name}\", true)"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    .line 34
    :cond_2
    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unsupported classifier: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " for constructor: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    move-object v0, v1

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method

.method public static final a(Lkotlin/reflect/jvm/internal/impl/k/v;Lkotlin/reflect/jvm/internal/impl/k/v;)Lkotlin/reflect/jvm/internal/impl/k/ao;
    .locals 1

    .prologue
    const-string v0, "lowerBound"

    invoke-static {p0, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "upperBound"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 66
    invoke-static {p0, p1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    check-cast p0, Lkotlin/reflect/jvm/internal/impl/k/ao;

    .line 67
    :goto_0
    return-object p0

    :cond_0
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/k/n;

    invoke-direct {v0, p0, p1}, Lkotlin/reflect/jvm/internal/impl/k/n;-><init>(Lkotlin/reflect/jvm/internal/impl/k/v;Lkotlin/reflect/jvm/internal/impl/k/v;)V

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/ao;

    move-object p0, v0

    goto :goto_0
.end method

.method public static final a(Lkotlin/reflect/jvm/internal/impl/b/a/h;Lkotlin/reflect/jvm/internal/impl/b/e;Ljava/util/List;)Lkotlin/reflect/jvm/internal/impl/k/v;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/reflect/jvm/internal/impl/b/a/h;",
            "Lkotlin/reflect/jvm/internal/impl/b/e;",
            "Ljava/util/List",
            "<+",
            "Lkotlin/reflect/jvm/internal/impl/k/ah;",
            ">;)",
            "Lkotlin/reflect/jvm/internal/impl/k/v;"
        }
    .end annotation

    .prologue
    const-string v0, "annotations"

    invoke-static {p0, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "descriptor"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "arguments"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 52
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/k/w;

    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/b/e;->e()Lkotlin/reflect/jvm/internal/impl/k/ad;

    move-result-object v2

    const-string v1, "descriptor.typeConstructor"

    invoke-static {v2, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v4, 0x0

    invoke-interface {p1, p2}, Lkotlin/reflect/jvm/internal/impl/b/e;->a(Ljava/util/List;)Lkotlin/reflect/jvm/internal/impl/h/e/h;

    move-result-object v5

    const-string v1, "descriptor.getMemberScope(arguments)"

    invoke-static {v5, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v1, p0

    move-object v3, p2

    invoke-direct/range {v0 .. v5}, Lkotlin/reflect/jvm/internal/impl/k/w;-><init>(Lkotlin/reflect/jvm/internal/impl/b/a/h;Lkotlin/reflect/jvm/internal/impl/k/ad;Ljava/util/List;ZLkotlin/reflect/jvm/internal/impl/h/e/h;)V

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/v;

    return-object v0
.end method

.method public static final a(Lkotlin/reflect/jvm/internal/impl/b/a/h;Lkotlin/reflect/jvm/internal/impl/k/ad;Ljava/util/List;ZLkotlin/reflect/jvm/internal/impl/h/e/h;)Lkotlin/reflect/jvm/internal/impl/k/v;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/reflect/jvm/internal/impl/b/a/h;",
            "Lkotlin/reflect/jvm/internal/impl/k/ad;",
            "Ljava/util/List",
            "<+",
            "Lkotlin/reflect/jvm/internal/impl/k/ah;",
            ">;Z",
            "Lkotlin/reflect/jvm/internal/impl/h/e/h;",
            ")",
            "Lkotlin/reflect/jvm/internal/impl/k/v;"
        }
    .end annotation

    .prologue
    const-string v0, "annotations"

    invoke-static {p0, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "constructor"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "arguments"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "memberScope"

    invoke-static {p4, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 45
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/k/w;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lkotlin/reflect/jvm/internal/impl/k/w;-><init>(Lkotlin/reflect/jvm/internal/impl/b/a/h;Lkotlin/reflect/jvm/internal/impl/k/ad;Ljava/util/List;ZLkotlin/reflect/jvm/internal/impl/h/e/h;)V

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/v;

    return-object v0
.end method

.method public static bridge synthetic a(Lkotlin/reflect/jvm/internal/impl/b/a/h;Lkotlin/reflect/jvm/internal/impl/k/ad;Ljava/util/List;ZLkotlin/reflect/jvm/internal/impl/h/e/h;ILjava/lang/Object;)Lkotlin/reflect/jvm/internal/impl/k/v;
    .locals 1

    .prologue
    and-int/lit8 v0, p5, 0x10

    if-eqz v0, :cond_0

    .line 44
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/k/s;->a:Lkotlin/reflect/jvm/internal/impl/k/s;

    invoke-direct {v0, p1, p2}, Lkotlin/reflect/jvm/internal/impl/k/s;->a(Lkotlin/reflect/jvm/internal/impl/k/ad;Ljava/util/List;)Lkotlin/reflect/jvm/internal/impl/h/e/h;

    move-result-object p4

    :cond_0
    invoke-static {p0, p1, p2, p3, p4}, Lkotlin/reflect/jvm/internal/impl/k/s;->a(Lkotlin/reflect/jvm/internal/impl/b/a/h;Lkotlin/reflect/jvm/internal/impl/k/ad;Ljava/util/List;ZLkotlin/reflect/jvm/internal/impl/h/e/h;)Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v0

    return-object v0
.end method
