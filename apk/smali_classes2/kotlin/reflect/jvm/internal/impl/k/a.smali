.class public final Lkotlin/reflect/jvm/internal/impl/k/a;
.super Lkotlin/reflect/jvm/internal/impl/k/g;
.source "SpecialTypes.kt"


# instance fields
.field private final a:Lkotlin/reflect/jvm/internal/impl/k/v;

.field private final b:Lkotlin/reflect/jvm/internal/impl/k/v;


# direct methods
.method public constructor <init>(Lkotlin/reflect/jvm/internal/impl/k/v;Lkotlin/reflect/jvm/internal/impl/k/v;)V
    .locals 1

    .prologue
    const-string v0, "delegate"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "abbreviation"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 33
    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/k/g;-><init>()V

    iput-object p1, p0, Lkotlin/reflect/jvm/internal/impl/k/a;->a:Lkotlin/reflect/jvm/internal/impl/k/v;

    iput-object p2, p0, Lkotlin/reflect/jvm/internal/impl/k/a;->b:Lkotlin/reflect/jvm/internal/impl/k/v;

    return-void
.end method


# virtual methods
.method public a(Lkotlin/reflect/jvm/internal/impl/b/a/h;)Lkotlin/reflect/jvm/internal/impl/k/a;
    .locals 3

    .prologue
    const-string v0, "newAnnotations"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 37
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/k/a;

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/k/a;->e()Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v1

    invoke-virtual {v1, p1}, Lkotlin/reflect/jvm/internal/impl/k/v;->c(Lkotlin/reflect/jvm/internal/impl/b/a/h;)Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v1

    iget-object v2, p0, Lkotlin/reflect/jvm/internal/impl/k/a;->b:Lkotlin/reflect/jvm/internal/impl/k/v;

    invoke-direct {v0, v1, v2}, Lkotlin/reflect/jvm/internal/impl/k/a;-><init>(Lkotlin/reflect/jvm/internal/impl/k/v;Lkotlin/reflect/jvm/internal/impl/k/v;)V

    return-object v0
.end method

.method public synthetic a(Z)Lkotlin/reflect/jvm/internal/impl/k/v;
    .locals 1

    .prologue
    .line 33
    invoke-virtual {p0, p1}, Lkotlin/reflect/jvm/internal/impl/k/a;->c(Z)Lkotlin/reflect/jvm/internal/impl/k/a;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/v;

    return-object v0
.end method

.method public synthetic b(Lkotlin/reflect/jvm/internal/impl/b/a/h;)Lkotlin/reflect/jvm/internal/impl/k/ao;
    .locals 1

    .prologue
    .line 33
    invoke-virtual {p0, p1}, Lkotlin/reflect/jvm/internal/impl/k/a;->a(Lkotlin/reflect/jvm/internal/impl/b/a/h;)Lkotlin/reflect/jvm/internal/impl/k/a;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/ao;

    return-object v0
.end method

.method public synthetic b(Z)Lkotlin/reflect/jvm/internal/impl/k/ao;
    .locals 1

    .prologue
    .line 33
    invoke-virtual {p0, p1}, Lkotlin/reflect/jvm/internal/impl/k/a;->c(Z)Lkotlin/reflect/jvm/internal/impl/k/a;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/ao;

    return-object v0
.end method

.method public c(Z)Lkotlin/reflect/jvm/internal/impl/k/a;
    .locals 3

    .prologue
    .line 40
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/k/a;

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/k/a;->e()Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v1

    invoke-virtual {v1, p1}, Lkotlin/reflect/jvm/internal/impl/k/v;->a(Z)Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v1

    iget-object v2, p0, Lkotlin/reflect/jvm/internal/impl/k/a;->b:Lkotlin/reflect/jvm/internal/impl/k/v;

    invoke-virtual {v2, p1}, Lkotlin/reflect/jvm/internal/impl/k/v;->a(Z)Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lkotlin/reflect/jvm/internal/impl/k/a;-><init>(Lkotlin/reflect/jvm/internal/impl/k/v;Lkotlin/reflect/jvm/internal/impl/k/v;)V

    return-object v0
.end method

.method public synthetic c(Lkotlin/reflect/jvm/internal/impl/b/a/h;)Lkotlin/reflect/jvm/internal/impl/k/v;
    .locals 1

    .prologue
    .line 33
    invoke-virtual {p0, p1}, Lkotlin/reflect/jvm/internal/impl/k/a;->a(Lkotlin/reflect/jvm/internal/impl/b/a/h;)Lkotlin/reflect/jvm/internal/impl/k/a;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/v;

    return-object v0
.end method

.method public d()Z
    .locals 1

    .prologue
    .line 42
    const/4 v0, 0x0

    return v0
.end method

.method protected e()Lkotlin/reflect/jvm/internal/impl/k/v;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/k/a;->a:Lkotlin/reflect/jvm/internal/impl/k/v;

    return-object v0
.end method

.method public final f()Lkotlin/reflect/jvm/internal/impl/k/v;
    .locals 1

    .prologue
    .line 34
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/k/a;->e()Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v0

    return-object v0
.end method

.method public final g()Lkotlin/reflect/jvm/internal/impl/k/v;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/k/a;->b:Lkotlin/reflect/jvm/internal/impl/k/v;

    return-object v0
.end method
