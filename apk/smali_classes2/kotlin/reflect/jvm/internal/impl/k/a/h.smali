.class public final Lkotlin/reflect/jvm/internal/impl/k/a/h;
.super Ljava/lang/Object;
.source "NewKotlinTypeChecker.kt"

# interfaces
.implements Lkotlin/reflect/jvm/internal/impl/k/a/c;


# static fields
.field public static final b:Lkotlin/reflect/jvm/internal/impl/k/a/h;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 79
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/k/a/h;

    invoke-direct {v0}, Lkotlin/reflect/jvm/internal/impl/k/a/h;-><init>()V

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 79
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    check-cast p0, Lkotlin/reflect/jvm/internal/impl/k/a/h;

    sput-object p0, Lkotlin/reflect/jvm/internal/impl/k/a/h;->b:Lkotlin/reflect/jvm/internal/impl/k/a/h;

    return-void
.end method

.method private final a(Lkotlin/reflect/jvm/internal/impl/k/a/n;Lkotlin/reflect/jvm/internal/impl/k/v;Lkotlin/reflect/jvm/internal/impl/k/v;)Ljava/lang/Boolean;
    .locals 6

    .prologue
    const/4 v2, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 146
    invoke-virtual {p2}, Lkotlin/reflect/jvm/internal/impl/k/v;->d()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p3}, Lkotlin/reflect/jvm/internal/impl/k/v;->d()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 147
    :cond_0
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/k/a/n;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    .line 414
    :cond_1
    :goto_0
    return-object v2

    .line 149
    :cond_2
    invoke-virtual {p2}, Lkotlin/reflect/jvm/internal/impl/k/v;->c()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p3}, Lkotlin/reflect/jvm/internal/impl/k/v;->c()Z

    move-result v0

    if-nez v0, :cond_3

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    goto :goto_0

    .line 151
    :cond_3
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/k/a/l;->a:Lkotlin/reflect/jvm/internal/impl/k/a/l;

    invoke-virtual {p2, v4}, Lkotlin/reflect/jvm/internal/impl/k/v;->a(Z)Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v1

    invoke-virtual {p3, v4}, Lkotlin/reflect/jvm/internal/impl/k/v;->a(Z)Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lkotlin/reflect/jvm/internal/impl/k/a/l;->a(Lkotlin/reflect/jvm/internal/impl/k/v;Lkotlin/reflect/jvm/internal/impl/k/v;)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    goto :goto_0

    .line 154
    :cond_4
    instance-of v0, p3, Lkotlin/reflect/jvm/internal/impl/k/a/e;

    if-eqz v0, :cond_5

    move-object v0, p3

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/a/e;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/k/a/e;->f()Lkotlin/reflect/jvm/internal/impl/k/ao;

    move-result-object v0

    if-eqz v0, :cond_5

    move-object v0, p2

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/ao;

    move-object v1, p3

    check-cast v1, Lkotlin/reflect/jvm/internal/impl/k/a/e;

    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/impl/k/a/e;->f()Lkotlin/reflect/jvm/internal/impl/k/ao;

    move-result-object v1

    invoke-virtual {p0, p1, v0, v1}, Lkotlin/reflect/jvm/internal/impl/k/a/h;->b(Lkotlin/reflect/jvm/internal/impl/k/a/n;Lkotlin/reflect/jvm/internal/impl/k/ao;Lkotlin/reflect/jvm/internal/impl/k/ao;)Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    goto :goto_0

    .line 156
    :cond_5
    invoke-virtual {p3}, Lkotlin/reflect/jvm/internal/impl/k/v;->i()Lkotlin/reflect/jvm/internal/impl/k/ad;

    move-result-object v0

    instance-of v1, v0, Lkotlin/reflect/jvm/internal/impl/k/q;

    if-nez v1, :cond_6

    move-object v0, v2

    :cond_6
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/q;

    if-eqz v0, :cond_1

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/q;

    .line 157
    invoke-virtual {p3}, Lkotlin/reflect/jvm/internal/impl/k/v;->c()Z

    move-result v1

    if-nez v1, :cond_7

    move v1, v3

    :goto_1
    sget-boolean v2, Lkotlin/p;->a:Z

    if-eqz v2, :cond_8

    if-nez v1, :cond_8

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Intersection type should not be marked nullable!: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    :cond_7
    move v1, v4

    goto :goto_1

    .line 158
    :cond_8
    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/k/q;->G_()Ljava/util/Collection;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 413
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_9
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_a

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/r;

    .line 158
    sget-object v5, Lkotlin/reflect/jvm/internal/impl/k/a/h;->b:Lkotlin/reflect/jvm/internal/impl/k/a/h;

    move-object v1, p2

    check-cast v1, Lkotlin/reflect/jvm/internal/impl/k/ao;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/k/r;->j()Lkotlin/reflect/jvm/internal/impl/k/ao;

    move-result-object v0

    invoke-virtual {v5, p1, v1, v0}, Lkotlin/reflect/jvm/internal/impl/k/a/h;->b(Lkotlin/reflect/jvm/internal/impl/k/a/n;Lkotlin/reflect/jvm/internal/impl/k/ao;Lkotlin/reflect/jvm/internal/impl/k/ao;)Z

    move-result v0

    if-nez v0, :cond_9

    .line 414
    :goto_2
    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    goto/16 :goto_0

    :cond_a
    move v4, v3

    goto :goto_2
.end method

.method private final a(Ljava/util/List;)Ljava/util/List;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<+",
            "Lkotlin/reflect/jvm/internal/impl/k/v;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/k/v;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 276
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x2

    if-ge v0, v1, :cond_1

    .line 283
    :cond_0
    :goto_0
    return-object p1

    :cond_1
    move-object v0, p1

    .line 278
    check-cast v0, Ljava/lang/Iterable;

    .line 438
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    check-cast v1, Ljava/util/Collection;

    .line 439
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_2
    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    move-object v0, v2

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/v;

    .line 278
    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/k/v;->a()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 440
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_3
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/ah;

    .line 278
    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/k/ah;->c()Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v0

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/k/o;->a(Lkotlin/reflect/jvm/internal/impl/k/r;)Z

    move-result v0

    if-nez v0, :cond_4

    move v0, v3

    :goto_2
    if-nez v0, :cond_3

    move v0, v4

    :goto_3
    if-eqz v0, :cond_2

    invoke-interface {v1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_4
    move v0, v4

    goto :goto_2

    :cond_5
    move v0, v3

    .line 441
    goto :goto_3

    .line 442
    :cond_6
    check-cast v1, Ljava/util/List;

    move-object v0, v1

    .line 279
    check-cast v0, Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_7

    move v0, v3

    :goto_4
    if-eqz v0, :cond_0

    move-object p1, v1

    .line 280
    goto :goto_0

    :cond_7
    move v0, v4

    .line 279
    goto :goto_4
.end method

.method public static final synthetic a(Lkotlin/reflect/jvm/internal/impl/k/a/h;Ljava/util/List;)Ljava/util/List;
    .locals 1

    .prologue
    .line 79
    invoke-direct {p0, p1}, Lkotlin/reflect/jvm/internal/impl/k/a/h;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public static final synthetic a(Lkotlin/reflect/jvm/internal/impl/k/a/h;Lkotlin/reflect/jvm/internal/impl/k/a/n;Lkotlin/reflect/jvm/internal/impl/k/v;Lkotlin/reflect/jvm/internal/impl/k/ad;)Ljava/util/List;
    .locals 1

    .prologue
    .line 79
    invoke-direct {p0, p1, p2, p3}, Lkotlin/reflect/jvm/internal/impl/k/a/h;->b(Lkotlin/reflect/jvm/internal/impl/k/a/n;Lkotlin/reflect/jvm/internal/impl/k/v;Lkotlin/reflect/jvm/internal/impl/k/ad;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method private final a(Lkotlin/reflect/jvm/internal/impl/k/a/n;Lkotlin/reflect/jvm/internal/impl/k/v;Lkotlin/reflect/jvm/internal/impl/k/ad;)Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/reflect/jvm/internal/impl/k/a/n;",
            "Lkotlin/reflect/jvm/internal/impl/k/v;",
            "Lkotlin/reflect/jvm/internal/impl/k/ad;",
            ")",
            "Ljava/util/List",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/k/v;",
            ">;"
        }
    .end annotation

    .prologue
    .line 209
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/k/a/h$c;->a:Lkotlin/reflect/jvm/internal/impl/k/a/h$c;

    .line 212
    invoke-static {p2}, Lkotlin/reflect/jvm/internal/impl/k/a/j;->a(Lkotlin/reflect/jvm/internal/impl/k/v;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 213
    invoke-virtual {v0, p1, p2, p3}, Lkotlin/reflect/jvm/internal/impl/k/a/h$c;->a(Lkotlin/reflect/jvm/internal/impl/k/a/n;Lkotlin/reflect/jvm/internal/impl/k/v;Lkotlin/reflect/jvm/internal/impl/k/ad;)Ljava/util/List;

    move-result-object v1

    .line 437
    :goto_0
    return-object v1

    .line 217
    :cond_0
    instance-of v0, p3, Lkotlin/reflect/jvm/internal/impl/b/e;

    if-nez v0, :cond_1

    .line 218
    invoke-direct {p0, p1, p2, p3}, Lkotlin/reflect/jvm/internal/impl/k/a/h;->b(Lkotlin/reflect/jvm/internal/impl/k/a/n;Lkotlin/reflect/jvm/internal/impl/k/v;Lkotlin/reflect/jvm/internal/impl/k/ad;)Ljava/util/List;

    move-result-object v1

    goto :goto_0

    .line 222
    :cond_1
    new-instance v2, Lkotlin/reflect/jvm/internal/impl/utils/e;

    invoke-direct {v2}, Lkotlin/reflect/jvm/internal/impl/utils/e;-><init>()V

    .line 223
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/k/a/h$d;->a:Lkotlin/reflect/jvm/internal/impl/k/a/h$d;

    check-cast v0, Lkotlin/d/a/b;

    new-instance v1, Lkotlin/reflect/jvm/internal/impl/k/a/h$e;

    invoke-direct {v1, v2}, Lkotlin/reflect/jvm/internal/impl/k/a/h$e;-><init>(Lkotlin/reflect/jvm/internal/impl/utils/e;)V

    check-cast v1, Lkotlin/d/a/b;

    invoke-virtual {p1, p2, v0, v1}, Lkotlin/reflect/jvm/internal/impl/k/a/n;->a(Lkotlin/reflect/jvm/internal/impl/k/v;Lkotlin/d/a/b;Lkotlin/d/a/b;)Z

    move-object v0, v2

    .line 233
    check-cast v0, Ljava/lang/Iterable;

    .line 426
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    check-cast v1, Ljava/util/Collection;

    .line 433
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .line 434
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/v;

    .line 233
    sget-object v3, Lkotlin/reflect/jvm/internal/impl/k/a/h$c;->a:Lkotlin/reflect/jvm/internal/impl/k/a/h$c;

    const-string v4, "it"

    invoke-static {v0, v4}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v3, p1, v0, p3}, Lkotlin/reflect/jvm/internal/impl/k/a/h$c;->a(Lkotlin/reflect/jvm/internal/impl/k/a/n;Lkotlin/reflect/jvm/internal/impl/k/v;Lkotlin/reflect/jvm/internal/impl/k/ad;)Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 435
    invoke-static {v1, v0}, Lkotlin/a/m;->a(Ljava/util/Collection;Ljava/lang/Iterable;)Z

    goto :goto_1

    .line 437
    :cond_2
    check-cast v1, Ljava/util/List;

    goto :goto_0
.end method

.method private final a(Lkotlin/reflect/jvm/internal/impl/k/ap;Lkotlin/reflect/jvm/internal/impl/k/ap;)Lkotlin/reflect/jvm/internal/impl/k/ap;
    .locals 1

    .prologue
    .line 288
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/k/ap;->a:Lkotlin/reflect/jvm/internal/impl/k/ap;

    invoke-static {p1, v0}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    move-object p1, p2

    .line 295
    :cond_0
    :goto_0
    return-object p1

    .line 289
    :cond_1
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/k/ap;->a:Lkotlin/reflect/jvm/internal/impl/k/ap;

    invoke-static {p2, v0}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 292
    invoke-static {p1, p2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 295
    const/4 p1, 0x0

    goto :goto_0
.end method

.method private final a(Lkotlin/reflect/jvm/internal/impl/k/a/n;Ljava/util/List;Lkotlin/reflect/jvm/internal/impl/k/v;)Z
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/reflect/jvm/internal/impl/k/a/n;",
            "Ljava/util/List",
            "<+",
            "Lkotlin/reflect/jvm/internal/impl/k/ah;",
            ">;",
            "Lkotlin/reflect/jvm/internal/impl/k/v;",
            ")Z"
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    .line 302
    invoke-virtual {p3}, Lkotlin/reflect/jvm/internal/impl/k/v;->i()Lkotlin/reflect/jvm/internal/impl/k/ad;

    move-result-object v0

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/k/ad;->b()Ljava/util/List;

    move-result-object v1

    move-object v0, v1

    .line 304
    check-cast v0, Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->size()I

    move-result v0

    add-int/lit8 v6, v0, -0x1

    if-gt v5, v6, :cond_5

    move v4, v5

    .line 305
    :goto_0
    invoke-virtual {p3}, Lkotlin/reflect/jvm/internal/impl/k/v;->a()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/ah;

    .line 306
    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/k/ah;->a()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 304
    :cond_0
    if-eq v4, v6, :cond_5

    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_0

    .line 308
    :cond_1
    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/k/ah;->c()Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v2

    invoke-virtual {v2}, Lkotlin/reflect/jvm/internal/impl/k/r;->j()Lkotlin/reflect/jvm/internal/impl/k/ao;

    move-result-object v7

    .line 309
    invoke-interface {p2, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lkotlin/reflect/jvm/internal/impl/k/ah;

    .line 310
    invoke-interface {v2}, Lkotlin/reflect/jvm/internal/impl/k/ah;->b()Lkotlin/reflect/jvm/internal/impl/k/ap;

    move-result-object v3

    sget-object v8, Lkotlin/reflect/jvm/internal/impl/k/ap;->a:Lkotlin/reflect/jvm/internal/impl/k/ap;

    invoke-static {v3, v8}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    sget-boolean v8, Lkotlin/p;->a:Z

    if-eqz v8, :cond_2

    if-nez v3, :cond_2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Incorrect sub argument: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    .line 311
    :cond_2
    invoke-interface {v2}, Lkotlin/reflect/jvm/internal/impl/k/ah;->c()Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v2

    invoke-virtual {v2}, Lkotlin/reflect/jvm/internal/impl/k/r;->j()Lkotlin/reflect/jvm/internal/impl/k/ao;

    move-result-object v2

    .line 309
    check-cast v2, Lkotlin/reflect/jvm/internal/impl/k/ao;

    .line 314
    invoke-interface {v1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lkotlin/reflect/jvm/internal/impl/b/aq;

    invoke-interface {v3}, Lkotlin/reflect/jvm/internal/impl/b/aq;->k()Lkotlin/reflect/jvm/internal/impl/k/ap;

    move-result-object v3

    const-string v8, "parameters[index].variance"

    invoke-static {v3, v8}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/k/ah;->b()Lkotlin/reflect/jvm/internal/impl/k/ap;

    move-result-object v0

    const-string v8, "superProjection.projectionKind"

    invoke-static {v0, v8}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, v3, v0}, Lkotlin/reflect/jvm/internal/impl/k/a/h;->a(Lkotlin/reflect/jvm/internal/impl/k/ap;Lkotlin/reflect/jvm/internal/impl/k/ap;)Lkotlin/reflect/jvm/internal/impl/k/ap;

    move-result-object v3

    if-eqz v3, :cond_3

    .line 443
    invoke-static {p1}, Lkotlin/reflect/jvm/internal/impl/k/a/n;->a(Lkotlin/reflect/jvm/internal/impl/k/a/n;)I

    move-result v0

    const/16 v8, 0x64

    if-le v0, v8, :cond_4

    .line 444
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Arguments depth is too high. Some related argument: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    .line 315
    :cond_3
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/k/a/n;->a()Z

    move-result v5

    .line 326
    :goto_1
    return v5

    .line 447
    :cond_4
    invoke-static {p1}, Lkotlin/reflect/jvm/internal/impl/k/a/n;->a(Lkotlin/reflect/jvm/internal/impl/k/a/n;)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    invoke-static {p1, v0}, Lkotlin/reflect/jvm/internal/impl/k/a/n;->a(Lkotlin/reflect/jvm/internal/impl/k/a/n;I)V

    move-object v0, p1

    .line 448
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/a/n;

    .line 318
    sget-object v8, Lkotlin/reflect/jvm/internal/impl/k/a/i;->a:[I

    invoke-virtual {v3}, Lkotlin/reflect/jvm/internal/impl/k/ap;->ordinal()I

    move-result v3

    aget v3, v8, v3

    packed-switch v3, :pswitch_data_0

    .line 321
    new-instance v0, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {v0}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw v0

    .line 319
    :pswitch_0
    sget-object v3, Lkotlin/reflect/jvm/internal/impl/k/a/h;->b:Lkotlin/reflect/jvm/internal/impl/k/a/h;

    invoke-virtual {v3, v0, v2, v7}, Lkotlin/reflect/jvm/internal/impl/k/a/h;->a(Lkotlin/reflect/jvm/internal/impl/k/a/n;Lkotlin/reflect/jvm/internal/impl/k/ao;Lkotlin/reflect/jvm/internal/impl/k/ao;)Z

    move-result v0

    .line 449
    :goto_2
    invoke-static {p1}, Lkotlin/reflect/jvm/internal/impl/k/a/n;->a(Lkotlin/reflect/jvm/internal/impl/k/a/n;)I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-static {p1, v2}, Lkotlin/reflect/jvm/internal/impl/k/a/n;->a(Lkotlin/reflect/jvm/internal/impl/k/a/n;I)V

    .line 324
    if-nez v0, :cond_0

    goto :goto_1

    .line 320
    :pswitch_1
    sget-object v3, Lkotlin/reflect/jvm/internal/impl/k/a/h;->b:Lkotlin/reflect/jvm/internal/impl/k/a/h;

    invoke-virtual {v3, v0, v2, v7}, Lkotlin/reflect/jvm/internal/impl/k/a/h;->b(Lkotlin/reflect/jvm/internal/impl/k/a/n;Lkotlin/reflect/jvm/internal/impl/k/ao;Lkotlin/reflect/jvm/internal/impl/k/ao;)Z

    move-result v0

    goto :goto_2

    .line 321
    :pswitch_2
    sget-object v3, Lkotlin/reflect/jvm/internal/impl/k/a/h;->b:Lkotlin/reflect/jvm/internal/impl/k/a/h;

    invoke-virtual {v3, v0, v7, v2}, Lkotlin/reflect/jvm/internal/impl/k/a/h;->b(Lkotlin/reflect/jvm/internal/impl/k/a/n;Lkotlin/reflect/jvm/internal/impl/k/ao;Lkotlin/reflect/jvm/internal/impl/k/ao;)Z

    move-result v0

    goto :goto_2

    .line 326
    :cond_5
    const/4 v5, 0x1

    goto :goto_1

    .line 318
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private final a(Lkotlin/reflect/jvm/internal/impl/k/a/n;Lkotlin/reflect/jvm/internal/impl/k/v;)Z
    .locals 2

    .prologue
    .line 165
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/k/a/h$f;->a:Lkotlin/reflect/jvm/internal/impl/k/a/h$f;

    check-cast v0, Lkotlin/d/a/b;

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/k/a/h$g;->a:Lkotlin/reflect/jvm/internal/impl/k/a/h$g;

    check-cast v1, Lkotlin/d/a/b;

    invoke-virtual {p1, p2, v0, v1}, Lkotlin/reflect/jvm/internal/impl/k/a/n;->a(Lkotlin/reflect/jvm/internal/impl/k/v;Lkotlin/d/a/b;Lkotlin/d/a/b;)Z

    move-result v0

    .line 172
    return v0
.end method

.method private final b(Lkotlin/reflect/jvm/internal/impl/k/a/n;Lkotlin/reflect/jvm/internal/impl/k/v;Lkotlin/reflect/jvm/internal/impl/k/ad;)Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/reflect/jvm/internal/impl/k/a/n;",
            "Lkotlin/reflect/jvm/internal/impl/k/v;",
            "Lkotlin/reflect/jvm/internal/impl/k/ad;",
            ")",
            "Ljava/util/List",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/k/v;",
            ">;"
        }
    .end annotation

    .prologue
    .line 241
    new-instance v2, Lkotlin/d/b/x$c;

    invoke-direct {v2}, Lkotlin/d/b/x$c;-><init>()V

    const/4 v0, 0x0

    check-cast v0, Ljava/util/List;

    iput-object v0, v2, Lkotlin/d/b/x$c;->a:Ljava/lang/Object;

    .line 243
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/k/a/h$a;->a:Lkotlin/reflect/jvm/internal/impl/k/a/h$a;

    check-cast v0, Lkotlin/d/a/b;

    new-instance v1, Lkotlin/reflect/jvm/internal/impl/k/a/h$b;

    invoke-direct {v1, p3, v2}, Lkotlin/reflect/jvm/internal/impl/k/a/h$b;-><init>(Lkotlin/reflect/jvm/internal/impl/k/ad;Lkotlin/d/b/x$c;)V

    check-cast v1, Lkotlin/d/a/b;

    invoke-virtual {p1, p2, v0, v1}, Lkotlin/reflect/jvm/internal/impl/k/a/n;->a(Lkotlin/reflect/jvm/internal/impl/k/v;Lkotlin/d/a/b;Lkotlin/d/a/b;)Z

    .line 262
    iget-object v0, v2, Lkotlin/d/b/x$c;->a:Ljava/lang/Object;

    check-cast v0, Ljava/util/List;

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lkotlin/a/m;->a()Ljava/util/List;

    move-result-object v0

    goto :goto_0
.end method

.method private final b(Lkotlin/reflect/jvm/internal/impl/k/a/n;Lkotlin/reflect/jvm/internal/impl/k/v;Lkotlin/reflect/jvm/internal/impl/k/v;)Z
    .locals 11

    .prologue
    const/16 v10, 0xa

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 175
    invoke-static {p2}, Lkotlin/reflect/jvm/internal/impl/k/a/j;->b(Lkotlin/reflect/jvm/internal/impl/k/v;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p2}, Lkotlin/reflect/jvm/internal/impl/k/a/j;->c(Lkotlin/reflect/jvm/internal/impl/k/v;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    move v0, v2

    :goto_0
    sget-boolean v1, Lkotlin/p;->a:Z

    if-eqz v1, :cond_2

    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Not singleClassifierType and not intersection subType: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    :cond_1
    move v0, v3

    goto :goto_0

    .line 176
    :cond_2
    invoke-static {p3}, Lkotlin/reflect/jvm/internal/impl/k/a/j;->b(Lkotlin/reflect/jvm/internal/impl/k/v;)Z

    move-result v0

    sget-boolean v1, Lkotlin/p;->a:Z

    if-eqz v1, :cond_3

    if-nez v0, :cond_3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Not singleClassifierType superType: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    .line 178
    :cond_3
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/k/a/k;->a:Lkotlin/reflect/jvm/internal/impl/k/a/k;

    invoke-virtual {v0, p1, p2, p3}, Lkotlin/reflect/jvm/internal/impl/k/a/k;->a(Lkotlin/reflect/jvm/internal/impl/k/a/n;Lkotlin/reflect/jvm/internal/impl/k/v;Lkotlin/reflect/jvm/internal/impl/k/v;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 199
    :goto_1
    return v3

    .line 180
    :cond_4
    invoke-virtual {p3}, Lkotlin/reflect/jvm/internal/impl/k/v;->i()Lkotlin/reflect/jvm/internal/impl/k/ad;

    move-result-object v4

    .line 181
    invoke-direct {p0, p1, p2, v4}, Lkotlin/reflect/jvm/internal/impl/k/a/h;->a(Lkotlin/reflect/jvm/internal/impl/k/a/n;Lkotlin/reflect/jvm/internal/impl/k/v;Lkotlin/reflect/jvm/internal/impl/k/ad;)Ljava/util/List;

    move-result-object v1

    .line 182
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    move-object v0, v1

    .line 187
    check-cast v0, Ljava/lang/Iterable;

    .line 415
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_5
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/v;

    .line 187
    sget-object v6, Lkotlin/reflect/jvm/internal/impl/k/a/h;->b:Lkotlin/reflect/jvm/internal/impl/k/a/h;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/k/v;->a()Ljava/util/List;

    move-result-object v0

    invoke-direct {v6, p1, v0, p3}, Lkotlin/reflect/jvm/internal/impl/k/a/h;->a(Lkotlin/reflect/jvm/internal/impl/k/a/n;Ljava/util/List;Lkotlin/reflect/jvm/internal/impl/k/v;)Z

    move-result v0

    if-eqz v0, :cond_5

    move v0, v2

    .line 416
    :goto_2
    if-eqz v0, :cond_7

    move v3, v2

    .line 187
    goto :goto_1

    .line 183
    :pswitch_0
    invoke-direct {p0, p1, p2}, Lkotlin/reflect/jvm/internal/impl/k/a/h;->a(Lkotlin/reflect/jvm/internal/impl/k/a/n;Lkotlin/reflect/jvm/internal/impl/k/v;)Z

    move-result v3

    goto :goto_1

    .line 184
    :pswitch_1
    invoke-static {v1}, Lkotlin/a/m;->e(Ljava/util/List;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/v;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/k/v;->a()Ljava/util/List;

    move-result-object v0

    invoke-direct {p0, p1, v0, p3}, Lkotlin/reflect/jvm/internal/impl/k/a/h;->a(Lkotlin/reflect/jvm/internal/impl/k/a/n;Ljava/util/List;Lkotlin/reflect/jvm/internal/impl/k/v;)Z

    move-result v3

    goto :goto_1

    :cond_6
    move v0, v3

    .line 416
    goto :goto_2

    .line 189
    :cond_7
    invoke-interface {v4}, Lkotlin/reflect/jvm/internal/impl/k/ad;->b()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 417
    new-instance v2, Ljava/util/ArrayList;

    invoke-static {v0, v10}, Lkotlin/a/m;->a(Ljava/lang/Iterable;I)I

    move-result v4

    invoke-direct {v2, v4}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v2, Ljava/util/Collection;

    .line 419
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v8

    move v6, v3

    :goto_3
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_a

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .line 420
    add-int/lit8 v7, v6, 0x1

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/aq;

    move-object v0, v1

    .line 190
    check-cast v0, Ljava/lang/Iterable;

    .line 421
    new-instance v3, Ljava/util/ArrayList;

    invoke-static {v0, v10}, Lkotlin/a/m;->a(Ljava/lang/Iterable;I)I

    move-result v4

    invoke-direct {v3, v4}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v3, Ljava/util/Collection;

    .line 422
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :goto_4
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_9

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .line 423
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/v;

    .line 191
    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/k/v;->a()Ljava/util/List;

    move-result-object v4

    invoke-static {v4, v6}, Lkotlin/a/m;->c(Ljava/util/List;I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lkotlin/reflect/jvm/internal/impl/k/ah;

    if-eqz v4, :cond_8

    sget-object v5, Lkotlin/reflect/jvm/internal/impl/k/a/h$h;->a:Lkotlin/reflect/jvm/internal/impl/k/a/h$h;

    check-cast v5, Lkotlin/d/a/b;

    invoke-static {v4, v5}, Lkotlin/reflect/jvm/internal/impl/utils/a/a;->a(Ljava/lang/Object;Lkotlin/d/a/b;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lkotlin/reflect/jvm/internal/impl/k/ah;

    if-eqz v4, :cond_8

    invoke-interface {v4}, Lkotlin/reflect/jvm/internal/impl/k/ah;->c()Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v4

    if-eqz v4, :cond_8

    invoke-virtual {v4}, Lkotlin/reflect/jvm/internal/impl/k/r;->j()Lkotlin/reflect/jvm/internal/impl/k/ao;

    move-result-object v4

    if-eqz v4, :cond_8

    .line 192
    invoke-interface {v3, v4}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_4

    :cond_8
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Incorrect type: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", subType: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", superType: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    .line 424
    :cond_9
    check-cast v3, Ljava/util/List;

    .line 196
    invoke-static {v3}, Lkotlin/reflect/jvm/internal/impl/k/a/b;->a(Ljava/util/List;)Lkotlin/reflect/jvm/internal/impl/k/ao;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/r;

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/k/c/a;->e(Lkotlin/reflect/jvm/internal/impl/k/r;)Lkotlin/reflect/jvm/internal/impl/k/ah;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    move v6, v7

    .line 419
    goto/16 :goto_3

    .line 425
    :cond_a
    check-cast v2, Ljava/util/List;

    .line 199
    invoke-direct {p0, p1, v2, p3}, Lkotlin/reflect/jvm/internal/impl/k/a/h;->a(Lkotlin/reflect/jvm/internal/impl/k/a/n;Ljava/util/List;Lkotlin/reflect/jvm/internal/impl/k/v;)Z

    move-result v3

    goto/16 :goto_1

    .line 182
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public final a(Lkotlin/reflect/jvm/internal/impl/k/ao;)Lkotlin/reflect/jvm/internal/impl/k/ao;
    .locals 3

    .prologue
    const-string v0, "type"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 132
    instance-of v0, p1, Lkotlin/reflect/jvm/internal/impl/k/v;

    if-eqz v0, :cond_0

    check-cast p1, Lkotlin/reflect/jvm/internal/impl/k/v;

    invoke-virtual {p0, p1}, Lkotlin/reflect/jvm/internal/impl/k/a/h;->a(Lkotlin/reflect/jvm/internal/impl/k/v;)Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/ao;

    .line 143
    :goto_0
    return-object v0

    .line 133
    :cond_0
    instance-of v0, p1, Lkotlin/reflect/jvm/internal/impl/k/m;

    if-eqz v0, :cond_3

    move-object v0, p1

    .line 134
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/m;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/k/m;->g()Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v0

    invoke-virtual {p0, v0}, Lkotlin/reflect/jvm/internal/impl/k/a/h;->a(Lkotlin/reflect/jvm/internal/impl/k/v;)Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v1

    move-object v0, p1

    .line 135
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/m;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/k/m;->h()Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v0

    invoke-virtual {p0, v0}, Lkotlin/reflect/jvm/internal/impl/k/a/h;->a(Lkotlin/reflect/jvm/internal/impl/k/v;)Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v2

    move-object v0, p1

    .line 136
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/m;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/k/m;->g()Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v0

    if-ne v1, v0, :cond_1

    move-object v0, p1

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/m;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/k/m;->h()Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v0

    if-eq v2, v0, :cond_2

    .line 137
    :cond_1
    invoke-static {v1, v2}, Lkotlin/reflect/jvm/internal/impl/k/s;->a(Lkotlin/reflect/jvm/internal/impl/k/v;Lkotlin/reflect/jvm/internal/impl/k/v;)Lkotlin/reflect/jvm/internal/impl/k/ao;

    move-result-object p1

    :cond_2
    move-object v0, p1

    .line 136
    goto :goto_0

    :cond_3
    new-instance v0, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {v0}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw v0
.end method

.method public final a(Lkotlin/reflect/jvm/internal/impl/k/v;)Lkotlin/reflect/jvm/internal/impl/k/v;
    .locals 7

    .prologue
    const/4 v5, 0x0

    const/16 v6, 0xa

    const-string v0, "type"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 104
    instance-of v0, p1, Lkotlin/reflect/jvm/internal/impl/h/a/a/a;

    if-eqz v0, :cond_4

    move-object v0, p1

    .line 105
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/h/a/a/a;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/h/a/a/a;->g()Lkotlin/reflect/jvm/internal/impl/k/ah;

    move-result-object v1

    sget-object v0, Lkotlin/reflect/jvm/internal/impl/k/a/h$i;->a:Lkotlin/reflect/jvm/internal/impl/k/a/h$i;

    check-cast v0, Lkotlin/d/a/b;

    invoke-static {v1, v0}, Lkotlin/reflect/jvm/internal/impl/utils/a/a;->a(Ljava/lang/Object;Lkotlin/d/a/b;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/ah;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/k/ah;->c()Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/k/r;->j()Lkotlin/reflect/jvm/internal/impl/k/ao;

    move-result-object v3

    :goto_0
    move-object v0, p1

    .line 108
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/h/a/a/a;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/h/a/a/a;->h()Lkotlin/reflect/jvm/internal/impl/h/a/a/b;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/h/a/a/b;->a()Lkotlin/reflect/jvm/internal/impl/k/a/f;

    move-result-object v0

    if-nez v0, :cond_2

    move-object v0, p1

    .line 109
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/h/a/a/a;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/h/a/a/a;->h()Lkotlin/reflect/jvm/internal/impl/h/a/a/b;

    move-result-object v2

    new-instance v4, Lkotlin/reflect/jvm/internal/impl/k/a/f;

    move-object v0, p1

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/h/a/a/a;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/h/a/a/a;->g()Lkotlin/reflect/jvm/internal/impl/k/ah;

    move-result-object v5

    move-object v0, p1

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/h/a/a/a;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/h/a/a/a;->h()Lkotlin/reflect/jvm/internal/impl/h/a/a/b;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/h/a/a/b;->G_()Ljava/util/Collection;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 401
    new-instance v1, Ljava/util/ArrayList;

    invoke-static {v0, v6}, Lkotlin/a/m;->a(Ljava/lang/Iterable;I)I

    move-result v6

    invoke-direct {v1, v6}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v1, Ljava/util/Collection;

    .line 402
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .line 403
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/r;

    .line 109
    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/k/r;->j()Lkotlin/reflect/jvm/internal/impl/k/ao;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 105
    :cond_0
    const/4 v3, 0x0

    goto :goto_0

    .line 404
    :cond_1
    check-cast v1, Ljava/util/List;

    .line 109
    invoke-direct {v4, v5, v1}, Lkotlin/reflect/jvm/internal/impl/k/a/f;-><init>(Lkotlin/reflect/jvm/internal/impl/k/ah;Ljava/util/List;)V

    invoke-virtual {v2, v4}, Lkotlin/reflect/jvm/internal/impl/h/a/a/b;->a(Lkotlin/reflect/jvm/internal/impl/k/a/f;)V

    .line 111
    :cond_2
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/k/a/e;

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/k/a/a;->a:Lkotlin/reflect/jvm/internal/impl/k/a/a;

    move-object v2, p1

    check-cast v2, Lkotlin/reflect/jvm/internal/impl/h/a/a/a;

    invoke-virtual {v2}, Lkotlin/reflect/jvm/internal/impl/h/a/a/a;->h()Lkotlin/reflect/jvm/internal/impl/h/a/a/b;

    move-result-object v2

    invoke-virtual {v2}, Lkotlin/reflect/jvm/internal/impl/h/a/a/b;->a()Lkotlin/reflect/jvm/internal/impl/k/a/f;

    move-result-object v2

    if-nez v2, :cond_3

    invoke-static {}, Lkotlin/d/b/l;->a()V

    :cond_3
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/k/v;->w()Lkotlin/reflect/jvm/internal/impl/b/a/h;

    move-result-object v4

    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/k/v;->c()Z

    move-result v5

    invoke-direct/range {v0 .. v5}, Lkotlin/reflect/jvm/internal/impl/k/a/e;-><init>(Lkotlin/reflect/jvm/internal/impl/k/a/a;Lkotlin/reflect/jvm/internal/impl/k/a/f;Lkotlin/reflect/jvm/internal/impl/k/ao;Lkotlin/reflect/jvm/internal/impl/b/a/h;Z)V

    .line 113
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/v;

    .line 127
    :goto_2
    return-object v0

    .line 116
    :cond_4
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/k/v;->i()Lkotlin/reflect/jvm/internal/impl/k/ad;

    move-result-object v0

    instance-of v0, v0, Lkotlin/reflect/jvm/internal/impl/k/q;

    if-eqz v0, :cond_6

    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/k/v;->c()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 117
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/k/v;->i()Lkotlin/reflect/jvm/internal/impl/k/ad;

    move-result-object v0

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/k/ad;->G_()Ljava/util/Collection;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 405
    new-instance v1, Ljava/util/ArrayList;

    invoke-static {v0, v6}, Lkotlin/a/m;->a(Ljava/lang/Iterable;I)I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v1, Ljava/util/Collection;

    .line 406
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_3
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .line 407
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/r;

    .line 117
    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/k/c/a;->b(Lkotlin/reflect/jvm/internal/impl/k/r;)Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 408
    :cond_5
    check-cast v1, Ljava/util/List;

    .line 118
    new-instance v2, Lkotlin/reflect/jvm/internal/impl/k/q;

    check-cast v1, Ljava/util/Collection;

    invoke-direct {v2, v1}, Lkotlin/reflect/jvm/internal/impl/k/q;-><init>(Ljava/util/Collection;)V

    .line 119
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/k/v;->w()Lkotlin/reflect/jvm/internal/impl/b/a/h;

    move-result-object v1

    move-object v0, v2

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/ad;

    invoke-static {}, Lkotlin/a/m;->a()Ljava/util/List;

    move-result-object v3

    invoke-virtual {v2}, Lkotlin/reflect/jvm/internal/impl/k/q;->a()Lkotlin/reflect/jvm/internal/impl/h/e/h;

    move-result-object v2

    const-string v4, "newConstructor.createScopeForKotlinType()"

    invoke-static {v2, v4}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v1, v0, v3, v5, v2}, Lkotlin/reflect/jvm/internal/impl/k/s;->a(Lkotlin/reflect/jvm/internal/impl/b/a/h;Lkotlin/reflect/jvm/internal/impl/k/ad;Ljava/util/List;ZLkotlin/reflect/jvm/internal/impl/h/e/h;)Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v0

    goto :goto_2

    .line 122
    :cond_6
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/k/v;->i()Lkotlin/reflect/jvm/internal/impl/k/ad;

    move-result-object v0

    instance-of v0, v0, Lkotlin/reflect/jvm/internal/impl/h/b/n;

    if-eqz v0, :cond_8

    .line 123
    new-instance v2, Lkotlin/reflect/jvm/internal/impl/k/q;

    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/k/v;->i()Lkotlin/reflect/jvm/internal/impl/k/ad;

    move-result-object v0

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/k/ad;->G_()Ljava/util/Collection;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 409
    new-instance v1, Ljava/util/ArrayList;

    invoke-static {v0, v6}, Lkotlin/a/m;->a(Ljava/lang/Iterable;I)I

    move-result v3

    invoke-direct {v1, v3}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v1, Ljava/util/Collection;

    .line 410
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_4
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .line 411
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/r;

    .line 123
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/k/v;->c()Z

    move-result v4

    invoke-static {v0, v4}, Lkotlin/reflect/jvm/internal/impl/k/an;->a(Lkotlin/reflect/jvm/internal/impl/k/r;Z)Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_4

    .line 412
    :cond_7
    check-cast v1, Ljava/util/List;

    .line 123
    check-cast v1, Ljava/util/Collection;

    invoke-direct {v2, v1}, Lkotlin/reflect/jvm/internal/impl/k/q;-><init>(Ljava/util/Collection;)V

    .line 124
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/k/v;->w()Lkotlin/reflect/jvm/internal/impl/b/a/h;

    move-result-object v1

    move-object v0, v2

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/ad;

    invoke-static {}, Lkotlin/a/m;->a()Ljava/util/List;

    move-result-object v2

    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/k/v;->b()Lkotlin/reflect/jvm/internal/impl/h/e/h;

    move-result-object v3

    invoke-static {v1, v0, v2, v5, v3}, Lkotlin/reflect/jvm/internal/impl/k/s;->a(Lkotlin/reflect/jvm/internal/impl/b/a/h;Lkotlin/reflect/jvm/internal/impl/k/ad;Ljava/util/List;ZLkotlin/reflect/jvm/internal/impl/h/e/h;)Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v0

    goto/16 :goto_2

    :cond_8
    move-object v0, p1

    .line 127
    goto/16 :goto_2
.end method

.method public final a(Lkotlin/reflect/jvm/internal/impl/k/a/n;Lkotlin/reflect/jvm/internal/impl/k/ao;Lkotlin/reflect/jvm/internal/impl/k/ao;)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    const-string v1, "$receiver"

    invoke-static {p1, v1}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "a"

    invoke-static {p2, v1}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "b"

    invoke-static {p3, v1}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 86
    if-ne p2, p3, :cond_1

    .line 88
    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-virtual {p0, p1, p2, p3}, Lkotlin/reflect/jvm/internal/impl/k/a/h;->b(Lkotlin/reflect/jvm/internal/impl/k/a/n;Lkotlin/reflect/jvm/internal/impl/k/ao;Lkotlin/reflect/jvm/internal/impl/k/ao;)Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {p0, p1, p3, p2}, Lkotlin/reflect/jvm/internal/impl/k/a/h;->b(Lkotlin/reflect/jvm/internal/impl/k/a/n;Lkotlin/reflect/jvm/internal/impl/k/ao;Lkotlin/reflect/jvm/internal/impl/k/ao;)Z

    move-result v1

    if-nez v1, :cond_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Lkotlin/reflect/jvm/internal/impl/k/r;Lkotlin/reflect/jvm/internal/impl/k/r;)Z
    .locals 4

    .prologue
    const-string v0, "subtype"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "supertype"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 81
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/k/a/n;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lkotlin/reflect/jvm/internal/impl/k/a/n;-><init>(Z)V

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/a/n;

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/k/a/h;->b:Lkotlin/reflect/jvm/internal/impl/k/a/h;

    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/k/r;->j()Lkotlin/reflect/jvm/internal/impl/k/ao;

    move-result-object v2

    invoke-virtual {p2}, Lkotlin/reflect/jvm/internal/impl/k/r;->j()Lkotlin/reflect/jvm/internal/impl/k/ao;

    move-result-object v3

    invoke-virtual {v1, v0, v2, v3}, Lkotlin/reflect/jvm/internal/impl/k/a/h;->b(Lkotlin/reflect/jvm/internal/impl/k/a/n;Lkotlin/reflect/jvm/internal/impl/k/ao;Lkotlin/reflect/jvm/internal/impl/k/ao;)Z

    move-result v0

    return v0
.end method

.method public final b(Lkotlin/reflect/jvm/internal/impl/k/a/n;Lkotlin/reflect/jvm/internal/impl/k/ao;Lkotlin/reflect/jvm/internal/impl/k/ao;)Z
    .locals 4

    .prologue
    const-string v0, "$receiver"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "subType"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "superType"

    invoke-static {p3, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 92
    invoke-virtual {p0, p2}, Lkotlin/reflect/jvm/internal/impl/k/a/h;->a(Lkotlin/reflect/jvm/internal/impl/k/ao;)Lkotlin/reflect/jvm/internal/impl/k/ao;

    move-result-object v1

    .line 93
    invoke-virtual {p0, p3}, Lkotlin/reflect/jvm/internal/impl/k/a/h;->a(Lkotlin/reflect/jvm/internal/impl/k/ao;)Lkotlin/reflect/jvm/internal/impl/k/ao;

    move-result-object v2

    move-object v0, v1

    .line 95
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/r;

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/k/o;->c(Lkotlin/reflect/jvm/internal/impl/k/r;)Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v3

    move-object v0, v2

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/r;

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/k/o;->d(Lkotlin/reflect/jvm/internal/impl/k/r;)Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v0

    invoke-direct {p0, p1, v3, v0}, Lkotlin/reflect/jvm/internal/impl/k/a/h;->a(Lkotlin/reflect/jvm/internal/impl/k/a/n;Lkotlin/reflect/jvm/internal/impl/k/v;Lkotlin/reflect/jvm/internal/impl/k/v;)Ljava/lang/Boolean;

    move-result-object v0

    if-eqz v0, :cond_0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    .line 100
    :goto_0
    return v0

    .line 98
    :cond_0
    invoke-virtual {p1, v1, v2}, Lkotlin/reflect/jvm/internal/impl/k/a/n;->a(Lkotlin/reflect/jvm/internal/impl/k/ao;Lkotlin/reflect/jvm/internal/impl/k/ao;)Ljava/lang/Boolean;

    move-result-object v0

    if-eqz v0, :cond_1

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    goto :goto_0

    .line 100
    :cond_1
    check-cast v1, Lkotlin/reflect/jvm/internal/impl/k/r;

    invoke-static {v1}, Lkotlin/reflect/jvm/internal/impl/k/o;->c(Lkotlin/reflect/jvm/internal/impl/k/r;)Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v0

    check-cast v2, Lkotlin/reflect/jvm/internal/impl/k/r;

    invoke-static {v2}, Lkotlin/reflect/jvm/internal/impl/k/o;->d(Lkotlin/reflect/jvm/internal/impl/k/r;)Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v1

    invoke-direct {p0, p1, v0, v1}, Lkotlin/reflect/jvm/internal/impl/k/a/h;->b(Lkotlin/reflect/jvm/internal/impl/k/a/n;Lkotlin/reflect/jvm/internal/impl/k/v;Lkotlin/reflect/jvm/internal/impl/k/v;)Z

    move-result v0

    goto :goto_0
.end method

.method public b(Lkotlin/reflect/jvm/internal/impl/k/r;Lkotlin/reflect/jvm/internal/impl/k/r;)Z
    .locals 4

    .prologue
    const-string v0, "a"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "b"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 83
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/k/a/n;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lkotlin/reflect/jvm/internal/impl/k/a/n;-><init>(Z)V

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/a/n;

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/k/a/h;->b:Lkotlin/reflect/jvm/internal/impl/k/a/h;

    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/k/r;->j()Lkotlin/reflect/jvm/internal/impl/k/ao;

    move-result-object v2

    invoke-virtual {p2}, Lkotlin/reflect/jvm/internal/impl/k/r;->j()Lkotlin/reflect/jvm/internal/impl/k/ao;

    move-result-object v3

    invoke-virtual {v1, v0, v2, v3}, Lkotlin/reflect/jvm/internal/impl/k/a/h;->a(Lkotlin/reflect/jvm/internal/impl/k/a/n;Lkotlin/reflect/jvm/internal/impl/k/ao;Lkotlin/reflect/jvm/internal/impl/k/ao;)Z

    move-result v0

    return v0
.end method
