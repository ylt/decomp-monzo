.class public Lkotlin/reflect/jvm/internal/impl/k/am;
.super Ljava/lang/Object;
.source "TypeSubstitutor.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lkotlin/reflect/jvm/internal/impl/k/am$2;,
        Lkotlin/reflect/jvm/internal/impl/k/am$b;,
        Lkotlin/reflect/jvm/internal/impl/k/am$a;
    }
.end annotation


# static fields
.field public static final a:Lkotlin/reflect/jvm/internal/impl/k/am;

.field static final synthetic b:Z


# instance fields
.field private final c:Lkotlin/reflect/jvm/internal/impl/k/ak;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 36
    const-class v0, Lkotlin/reflect/jvm/internal/impl/k/am;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lkotlin/reflect/jvm/internal/impl/k/am;->b:Z

    .line 40
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/k/ak;->d:Lkotlin/reflect/jvm/internal/impl/k/ak;

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/k/am;->a(Lkotlin/reflect/jvm/internal/impl/k/ak;)Lkotlin/reflect/jvm/internal/impl/k/am;

    move-result-object v0

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/k/am;->a:Lkotlin/reflect/jvm/internal/impl/k/am;

    return-void

    .line 36
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected constructor <init>(Lkotlin/reflect/jvm/internal/impl/k/ak;)V
    .locals 5

    .prologue
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "substitution"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "kotlin/reflect/jvm/internal/impl/types/TypeSubstitutor"

    aput-object v4, v2, v3

    const/4 v3, 0x2

    const-string v4, "<init>"

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 72
    :cond_0
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 73
    iput-object p1, p0, Lkotlin/reflect/jvm/internal/impl/k/am;->c:Lkotlin/reflect/jvm/internal/impl/k/ak;

    .line 74
    return-void
.end method

.method private static a(Ljava/lang/Object;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 318
    :try_start_0
    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 325
    :goto_0
    return-object v0

    .line 320
    :catch_0
    move-exception v0

    .line 321
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "com.intellij.openapi.progress.ProcessCanceledException"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 323
    check-cast v0, Ljava/lang/RuntimeException;

    throw v0

    .line 325
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[Exception while computing toString(): "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private a(Ljava/util/List;Ljava/util/List;I)Ljava/util/List;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/b/aq;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/k/ah;",
            ">;I)",
            "Ljava/util/List",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/k/ah;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lkotlin/reflect/jvm/internal/impl/k/am$a;
        }
    .end annotation

    .prologue
    .line 253
    new-instance v3, Ljava/util/ArrayList;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    invoke-direct {v3, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 254
    const/4 v0, 0x0

    move v2, v0

    :goto_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_1

    .line 255
    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/aq;

    .line 256
    invoke-interface {p2, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lkotlin/reflect/jvm/internal/impl/k/ah;

    .line 258
    add-int/lit8 v4, p3, 0x1

    invoke-direct {p0, v1, v4}, Lkotlin/reflect/jvm/internal/impl/k/am;->a(Lkotlin/reflect/jvm/internal/impl/k/ah;I)Lkotlin/reflect/jvm/internal/impl/k/ah;

    move-result-object v1

    .line 260
    sget-object v4, Lkotlin/reflect/jvm/internal/impl/k/am$2;->a:[I

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/b/aq;->k()Lkotlin/reflect/jvm/internal/impl/k/ap;

    move-result-object v5

    invoke-interface {v1}, Lkotlin/reflect/jvm/internal/impl/k/ah;->b()Lkotlin/reflect/jvm/internal/impl/k/ap;

    move-result-object v6

    invoke-static {v5, v6}, Lkotlin/reflect/jvm/internal/impl/k/am;->b(Lkotlin/reflect/jvm/internal/impl/k/ap;Lkotlin/reflect/jvm/internal/impl/k/ap;)Lkotlin/reflect/jvm/internal/impl/k/am$b;

    move-result-object v5

    invoke-virtual {v5}, Lkotlin/reflect/jvm/internal/impl/k/am$b;->ordinal()I

    move-result v5

    aget v4, v4, v5

    packed-switch v4, :pswitch_data_0

    :cond_0
    move-object v0, v1

    .line 273
    :goto_1
    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 254
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 263
    :pswitch_0
    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/b/aq;->k()Lkotlin/reflect/jvm/internal/impl/k/ap;

    move-result-object v0

    sget-object v4, Lkotlin/reflect/jvm/internal/impl/k/ap;->a:Lkotlin/reflect/jvm/internal/impl/k/ap;

    if-eq v0, v4, :cond_0

    invoke-interface {v1}, Lkotlin/reflect/jvm/internal/impl/k/ah;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 264
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/k/aj;

    sget-object v4, Lkotlin/reflect/jvm/internal/impl/k/ap;->a:Lkotlin/reflect/jvm/internal/impl/k/ap;

    invoke-interface {v1}, Lkotlin/reflect/jvm/internal/impl/k/ah;->c()Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v1

    invoke-direct {v0, v4, v1}, Lkotlin/reflect/jvm/internal/impl/k/aj;-><init>(Lkotlin/reflect/jvm/internal/impl/k/ap;Lkotlin/reflect/jvm/internal/impl/k/r;)V

    goto :goto_1

    .line 269
    :pswitch_1
    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/k/an;->a(Lkotlin/reflect/jvm/internal/impl/b/aq;)Lkotlin/reflect/jvm/internal/impl/k/ah;

    move-result-object v0

    goto :goto_1

    .line 275
    :cond_1
    return-object v3

    .line 260
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private static a(Lkotlin/reflect/jvm/internal/impl/b/a/h;)Lkotlin/reflect/jvm/internal/impl/b/a/h;
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    if-nez p0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "annotations"

    aput-object v3, v2, v4

    const-string v3, "kotlin/reflect/jvm/internal/impl/types/TypeSubstitutor"

    aput-object v3, v2, v5

    const-string v3, "filterOutUnsafeVariance"

    aput-object v3, v2, v6

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 211
    :cond_0
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/a/l;->h:Lkotlin/reflect/jvm/internal/impl/a/l$a;

    iget-object v0, v0, Lkotlin/reflect/jvm/internal/impl/a/l$a;->I:Lkotlin/reflect/jvm/internal/impl/e/b;

    invoke-interface {p0, v0}, Lkotlin/reflect/jvm/internal/impl/b/a/h;->b(Lkotlin/reflect/jvm/internal/impl/e/b;)Z

    move-result v0

    if-nez v0, :cond_1

    if-nez p0, :cond_3

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "@NotNull method %s.%s must not return null"

    new-array v2, v6, [Ljava/lang/Object;

    const-string v3, "kotlin/reflect/jvm/internal/impl/types/TypeSubstitutor"

    aput-object v3, v2, v4

    const-string v3, "filterOutUnsafeVariance"

    aput-object v3, v2, v5

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 212
    :cond_1
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/b/a/l;

    new-instance v1, Lkotlin/reflect/jvm/internal/impl/k/am$1;

    invoke-direct {v1}, Lkotlin/reflect/jvm/internal/impl/k/am$1;-><init>()V

    invoke-direct {v0, p0, v1}, Lkotlin/reflect/jvm/internal/impl/b/a/l;-><init>(Lkotlin/reflect/jvm/internal/impl/b/a/h;Lkotlin/d/a/b;)V

    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "@NotNull method %s.%s must not return null"

    new-array v2, v6, [Ljava/lang/Object;

    const-string v3, "kotlin/reflect/jvm/internal/impl/types/TypeSubstitutor"

    aput-object v3, v2, v4

    const-string v3, "filterOutUnsafeVariance"

    aput-object v3, v2, v5

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    move-object p0, v0

    :cond_3
    return-object p0
.end method

.method private a(Lkotlin/reflect/jvm/internal/impl/k/ah;I)Lkotlin/reflect/jvm/internal/impl/k/ah;
    .locals 11
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lkotlin/reflect/jvm/internal/impl/k/am$a;
        }
    .end annotation

    .prologue
    const/4 v10, 0x2

    const/4 v9, 0x1

    const/4 v8, 0x0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "originalProjection"

    aput-object v3, v2, v8

    const-string v3, "kotlin/reflect/jvm/internal/impl/types/TypeSubstitutor"

    aput-object v3, v2, v9

    const-string v3, "unsafeSubstitute"

    aput-object v3, v2, v10

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 130
    :cond_0
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/k/am;->c:Lkotlin/reflect/jvm/internal/impl/k/ak;

    invoke-static {p2, p1, v0}, Lkotlin/reflect/jvm/internal/impl/k/am;->a(ILkotlin/reflect/jvm/internal/impl/k/ah;Lkotlin/reflect/jvm/internal/impl/k/ak;)V

    .line 132
    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/k/ah;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    if-nez p1, :cond_b

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "@NotNull method %s.%s must not return null"

    new-array v2, v10, [Ljava/lang/Object;

    const-string v3, "kotlin/reflect/jvm/internal/impl/types/TypeSubstitutor"

    aput-object v3, v2, v8

    const-string v3, "unsafeSubstitute"

    aput-object v3, v2, v9

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 135
    :cond_1
    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/k/ah;->c()Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v3

    .line 136
    invoke-static {v3}, Lkotlin/reflect/jvm/internal/impl/k/k;->a(Lkotlin/reflect/jvm/internal/impl/k/r;)Z

    move-result v0

    if-nez v0, :cond_2

    invoke-virtual {v3}, Lkotlin/reflect/jvm/internal/impl/k/r;->j()Lkotlin/reflect/jvm/internal/impl/k/ao;

    move-result-object v0

    instance-of v0, v0, Lkotlin/reflect/jvm/internal/impl/k/u;

    if-eqz v0, :cond_3

    .line 137
    :cond_2
    if-nez p1, :cond_b

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "@NotNull method %s.%s must not return null"

    new-array v2, v10, [Ljava/lang/Object;

    const-string v3, "kotlin/reflect/jvm/internal/impl/types/TypeSubstitutor"

    aput-object v3, v2, v8

    const-string v3, "unsafeSubstitute"

    aput-object v3, v2, v9

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 140
    :cond_3
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/k/am;->c:Lkotlin/reflect/jvm/internal/impl/k/ak;

    invoke-virtual {v0, v3}, Lkotlin/reflect/jvm/internal/impl/k/ak;->b(Lkotlin/reflect/jvm/internal/impl/k/r;)Lkotlin/reflect/jvm/internal/impl/k/ah;

    move-result-object v2

    .line 141
    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/k/ah;->b()Lkotlin/reflect/jvm/internal/impl/k/ap;

    move-result-object v1

    .line 142
    if-nez v2, :cond_6

    invoke-static {v3}, Lkotlin/reflect/jvm/internal/impl/k/o;->a(Lkotlin/reflect/jvm/internal/impl/k/r;)Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-static {v3}, Lkotlin/reflect/jvm/internal/impl/k/ac;->a(Lkotlin/reflect/jvm/internal/impl/k/r;)Z

    move-result v0

    if-nez v0, :cond_6

    .line 143
    invoke-static {v3}, Lkotlin/reflect/jvm/internal/impl/k/o;->b(Lkotlin/reflect/jvm/internal/impl/k/r;)Lkotlin/reflect/jvm/internal/impl/k/m;

    move-result-object v0

    .line 144
    new-instance v2, Lkotlin/reflect/jvm/internal/impl/k/aj;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/k/m;->g()Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v3

    invoke-direct {v2, v1, v3}, Lkotlin/reflect/jvm/internal/impl/k/aj;-><init>(Lkotlin/reflect/jvm/internal/impl/k/ap;Lkotlin/reflect/jvm/internal/impl/k/r;)V

    add-int/lit8 v3, p2, 0x1

    invoke-direct {p0, v2, v3}, Lkotlin/reflect/jvm/internal/impl/k/am;->a(Lkotlin/reflect/jvm/internal/impl/k/ah;I)Lkotlin/reflect/jvm/internal/impl/k/ah;

    move-result-object v2

    .line 146
    new-instance v3, Lkotlin/reflect/jvm/internal/impl/k/aj;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/k/m;->h()Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v0

    invoke-direct {v3, v1, v0}, Lkotlin/reflect/jvm/internal/impl/k/aj;-><init>(Lkotlin/reflect/jvm/internal/impl/k/ap;Lkotlin/reflect/jvm/internal/impl/k/r;)V

    add-int/lit8 v0, p2, 0x1

    invoke-direct {p0, v3, v0}, Lkotlin/reflect/jvm/internal/impl/k/am;->a(Lkotlin/reflect/jvm/internal/impl/k/ah;I)Lkotlin/reflect/jvm/internal/impl/k/ah;

    move-result-object v0

    .line 149
    invoke-interface {v2}, Lkotlin/reflect/jvm/internal/impl/k/ah;->b()Lkotlin/reflect/jvm/internal/impl/k/ap;

    move-result-object v3

    .line 152
    sget-boolean v4, Lkotlin/reflect/jvm/internal/impl/k/am;->b:Z

    if-nez v4, :cond_5

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/k/ah;->b()Lkotlin/reflect/jvm/internal/impl/k/ap;

    move-result-object v4

    if-ne v3, v4, :cond_4

    sget-object v4, Lkotlin/reflect/jvm/internal/impl/k/ap;->a:Lkotlin/reflect/jvm/internal/impl/k/ap;

    if-eq v1, v4, :cond_5

    :cond_4
    if-eq v1, v3, :cond_5

    new-instance v0, Ljava/lang/AssertionError;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Unexpected substituted projection kind: "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "; original: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 154
    :cond_5
    invoke-interface {v2}, Lkotlin/reflect/jvm/internal/impl/k/ah;->c()Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v1

    invoke-static {v1}, Lkotlin/reflect/jvm/internal/impl/k/al;->a(Lkotlin/reflect/jvm/internal/impl/k/r;)Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v1

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/k/ah;->c()Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v0

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/k/al;->a(Lkotlin/reflect/jvm/internal/impl/k/r;)Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v0

    invoke-static {v1, v0}, Lkotlin/reflect/jvm/internal/impl/k/s;->a(Lkotlin/reflect/jvm/internal/impl/k/v;Lkotlin/reflect/jvm/internal/impl/k/v;)Lkotlin/reflect/jvm/internal/impl/k/ao;

    move-result-object v0

    .line 156
    new-instance p1, Lkotlin/reflect/jvm/internal/impl/k/aj;

    invoke-direct {p1, v3, v0}, Lkotlin/reflect/jvm/internal/impl/k/aj;-><init>(Lkotlin/reflect/jvm/internal/impl/k/ap;Lkotlin/reflect/jvm/internal/impl/k/r;)V

    if-nez p1, :cond_b

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "@NotNull method %s.%s must not return null"

    new-array v2, v10, [Ljava/lang/Object;

    const-string v3, "kotlin/reflect/jvm/internal/impl/types/TypeSubstitutor"

    aput-object v3, v2, v8

    const-string v3, "unsafeSubstitute"

    aput-object v3, v2, v9

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 159
    :cond_6
    invoke-static {v3}, Lkotlin/reflect/jvm/internal/impl/a/l;->f(Lkotlin/reflect/jvm/internal/impl/k/r;)Z

    move-result v0

    if-nez v0, :cond_7

    invoke-virtual {v3}, Lkotlin/reflect/jvm/internal/impl/k/r;->d()Z

    move-result v0

    if-eqz v0, :cond_8

    :cond_7
    if-nez p1, :cond_b

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "@NotNull method %s.%s must not return null"

    new-array v2, v10, [Ljava/lang/Object;

    const-string v3, "kotlin/reflect/jvm/internal/impl/types/TypeSubstitutor"

    aput-object v3, v2, v8

    const-string v3, "unsafeSubstitute"

    aput-object v3, v2, v9

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 161
    :cond_8
    if-eqz v2, :cond_10

    .line 162
    invoke-interface {v2}, Lkotlin/reflect/jvm/internal/impl/k/ah;->b()Lkotlin/reflect/jvm/internal/impl/k/ap;

    move-result-object v0

    invoke-static {v1, v0}, Lkotlin/reflect/jvm/internal/impl/k/am;->b(Lkotlin/reflect/jvm/internal/impl/k/ap;Lkotlin/reflect/jvm/internal/impl/k/ap;)Lkotlin/reflect/jvm/internal/impl/k/am$b;

    move-result-object v4

    .line 167
    invoke-static {v3}, Lkotlin/reflect/jvm/internal/impl/h/a/a/c;->a(Lkotlin/reflect/jvm/internal/impl/k/r;)Z

    move-result v0

    .line 168
    if-nez v0, :cond_9

    .line 170
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/k/am$2;->a:[I

    invoke-virtual {v4}, Lkotlin/reflect/jvm/internal/impl/k/am$b;->ordinal()I

    move-result v5

    aget v0, v0, v5

    packed-switch v0, :pswitch_data_0

    .line 179
    :cond_9
    invoke-static {v3}, Lkotlin/reflect/jvm/internal/impl/k/ac;->b(Lkotlin/reflect/jvm/internal/impl/k/r;)Lkotlin/reflect/jvm/internal/impl/k/e;

    move-result-object v0

    .line 180
    invoke-interface {v2}, Lkotlin/reflect/jvm/internal/impl/k/ah;->a()Z

    move-result v5

    if-eqz v5, :cond_c

    .line 181
    if-nez v2, :cond_a

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "@NotNull method %s.%s must not return null"

    new-array v2, v10, [Ljava/lang/Object;

    const-string v3, "kotlin/reflect/jvm/internal/impl/types/TypeSubstitutor"

    aput-object v3, v2, v8

    const-string v3, "unsafeSubstitute"

    aput-object v3, v2, v9

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 172
    :pswitch_0
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/k/am$a;

    const-string v1, "Out-projection in in-position"

    invoke-direct {v0, v1}, Lkotlin/reflect/jvm/internal/impl/k/am$a;-><init>(Ljava/lang/String;)V

    throw v0

    .line 175
    :pswitch_1
    new-instance p1, Lkotlin/reflect/jvm/internal/impl/k/aj;

    sget-object v0, Lkotlin/reflect/jvm/internal/impl/k/ap;->c:Lkotlin/reflect/jvm/internal/impl/k/ap;

    invoke-virtual {v3}, Lkotlin/reflect/jvm/internal/impl/k/r;->i()Lkotlin/reflect/jvm/internal/impl/k/ad;

    move-result-object v1

    invoke-interface {v1}, Lkotlin/reflect/jvm/internal/impl/k/ad;->f()Lkotlin/reflect/jvm/internal/impl/a/l;

    move-result-object v1

    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/impl/a/l;->y()Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v1

    invoke-direct {p1, v0, v1}, Lkotlin/reflect/jvm/internal/impl/k/aj;-><init>(Lkotlin/reflect/jvm/internal/impl/k/ap;Lkotlin/reflect/jvm/internal/impl/k/r;)V

    if-nez p1, :cond_b

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "@NotNull method %s.%s must not return null"

    new-array v2, v10, [Ljava/lang/Object;

    const-string v3, "kotlin/reflect/jvm/internal/impl/types/TypeSubstitutor"

    aput-object v3, v2, v8

    const-string v3, "unsafeSubstitute"

    aput-object v3, v2, v9

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_a
    move-object p1, v2

    .line 206
    :cond_b
    return-object p1

    .line 183
    :cond_c
    if-eqz v0, :cond_f

    .line 184
    invoke-interface {v2}, Lkotlin/reflect/jvm/internal/impl/k/ah;->c()Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v5

    invoke-interface {v0, v5}, Lkotlin/reflect/jvm/internal/impl/k/e;->a_(Lkotlin/reflect/jvm/internal/impl/k/r;)Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v0

    .line 192
    :goto_0
    invoke-virtual {v3}, Lkotlin/reflect/jvm/internal/impl/k/r;->w()Lkotlin/reflect/jvm/internal/impl/b/a/h;

    move-result-object v5

    invoke-interface {v5}, Lkotlin/reflect/jvm/internal/impl/b/a/h;->a()Z

    move-result v5

    if-nez v5, :cond_d

    .line 193
    iget-object v5, p0, Lkotlin/reflect/jvm/internal/impl/k/am;->c:Lkotlin/reflect/jvm/internal/impl/k/ak;

    invoke-virtual {v3}, Lkotlin/reflect/jvm/internal/impl/k/r;->w()Lkotlin/reflect/jvm/internal/impl/b/a/h;

    move-result-object v3

    invoke-virtual {v5, v3}, Lkotlin/reflect/jvm/internal/impl/k/ak;->a(Lkotlin/reflect/jvm/internal/impl/b/a/h;)Lkotlin/reflect/jvm/internal/impl/b/a/h;

    move-result-object v3

    invoke-static {v3}, Lkotlin/reflect/jvm/internal/impl/k/am;->a(Lkotlin/reflect/jvm/internal/impl/b/a/h;)Lkotlin/reflect/jvm/internal/impl/b/a/h;

    move-result-object v3

    .line 194
    new-instance v5, Lkotlin/reflect/jvm/internal/impl/b/a/k;

    new-array v6, v10, [Lkotlin/reflect/jvm/internal/impl/b/a/h;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/k/r;->w()Lkotlin/reflect/jvm/internal/impl/b/a/h;

    move-result-object v7

    aput-object v7, v6, v8

    aput-object v3, v6, v9

    invoke-direct {v5, v6}, Lkotlin/reflect/jvm/internal/impl/b/a/k;-><init>([Lkotlin/reflect/jvm/internal/impl/b/a/h;)V

    invoke-static {v0, v5}, Lkotlin/reflect/jvm/internal/impl/k/c/a;->a(Lkotlin/reflect/jvm/internal/impl/k/r;Lkotlin/reflect/jvm/internal/impl/b/a/h;)Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v0

    .line 200
    :cond_d
    sget-object v3, Lkotlin/reflect/jvm/internal/impl/k/am$b;->a:Lkotlin/reflect/jvm/internal/impl/k/am$b;

    if-ne v4, v3, :cond_e

    invoke-interface {v2}, Lkotlin/reflect/jvm/internal/impl/k/ah;->b()Lkotlin/reflect/jvm/internal/impl/k/ap;

    move-result-object v2

    invoke-static {v1, v2}, Lkotlin/reflect/jvm/internal/impl/k/am;->a(Lkotlin/reflect/jvm/internal/impl/k/ap;Lkotlin/reflect/jvm/internal/impl/k/ap;)Lkotlin/reflect/jvm/internal/impl/k/ap;

    move-result-object v1

    .line 203
    :cond_e
    new-instance p1, Lkotlin/reflect/jvm/internal/impl/k/aj;

    invoke-direct {p1, v1, v0}, Lkotlin/reflect/jvm/internal/impl/k/aj;-><init>(Lkotlin/reflect/jvm/internal/impl/k/ap;Lkotlin/reflect/jvm/internal/impl/k/r;)V

    if-nez p1, :cond_b

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "@NotNull method %s.%s must not return null"

    new-array v2, v10, [Ljava/lang/Object;

    const-string v3, "kotlin/reflect/jvm/internal/impl/types/TypeSubstitutor"

    aput-object v3, v2, v8

    const-string v3, "unsafeSubstitute"

    aput-object v3, v2, v9

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 188
    :cond_f
    invoke-interface {v2}, Lkotlin/reflect/jvm/internal/impl/k/ah;->c()Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v0

    invoke-virtual {v3}, Lkotlin/reflect/jvm/internal/impl/k/r;->c()Z

    move-result v5

    invoke-static {v0, v5}, Lkotlin/reflect/jvm/internal/impl/k/an;->b(Lkotlin/reflect/jvm/internal/impl/k/r;Z)Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v0

    goto :goto_0

    .line 206
    :cond_10
    invoke-direct {p0, p1, p2}, Lkotlin/reflect/jvm/internal/impl/k/am;->b(Lkotlin/reflect/jvm/internal/impl/k/ah;I)Lkotlin/reflect/jvm/internal/impl/k/ah;

    move-result-object p1

    if-nez p1, :cond_b

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "@NotNull method %s.%s must not return null"

    new-array v2, v10, [Ljava/lang/Object;

    const-string v3, "kotlin/reflect/jvm/internal/impl/types/TypeSubstitutor"

    aput-object v3, v2, v8

    const-string v3, "unsafeSubstitute"

    aput-object v3, v2, v9

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 170
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static a(Lkotlin/reflect/jvm/internal/impl/k/ak;)Lkotlin/reflect/jvm/internal/impl/k/am;
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    if-nez p0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "substitution"

    aput-object v3, v2, v4

    const-string v3, "kotlin/reflect/jvm/internal/impl/types/TypeSubstitutor"

    aput-object v3, v2, v5

    const-string v3, "create"

    aput-object v3, v2, v6

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 50
    :cond_0
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/k/am;

    invoke-direct {v0, p0}, Lkotlin/reflect/jvm/internal/impl/k/am;-><init>(Lkotlin/reflect/jvm/internal/impl/k/ak;)V

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "@NotNull method %s.%s must not return null"

    new-array v2, v6, [Ljava/lang/Object;

    const-string v3, "kotlin/reflect/jvm/internal/impl/types/TypeSubstitutor"

    aput-object v3, v2, v4

    const-string v3, "create"

    aput-object v3, v2, v5

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    return-object v0
.end method

.method public static a(Lkotlin/reflect/jvm/internal/impl/k/ak;Lkotlin/reflect/jvm/internal/impl/k/ak;)Lkotlin/reflect/jvm/internal/impl/k/am;
    .locals 7

    .prologue
    const/4 v2, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    if-nez p0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "first"

    aput-object v3, v2, v4

    const-string v3, "kotlin/reflect/jvm/internal/impl/types/TypeSubstitutor"

    aput-object v3, v2, v5

    const-string v3, "createChainedSubstitutor"

    aput-object v3, v2, v6

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    if-nez p1, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "second"

    aput-object v3, v2, v4

    const-string v3, "kotlin/reflect/jvm/internal/impl/types/TypeSubstitutor"

    aput-object v3, v2, v5

    const-string v3, "createChainedSubstitutor"

    aput-object v3, v2, v6

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 55
    :cond_1
    invoke-static {p0, p1}, Lkotlin/reflect/jvm/internal/impl/k/i;->a(Lkotlin/reflect/jvm/internal/impl/k/ak;Lkotlin/reflect/jvm/internal/impl/k/ak;)Lkotlin/reflect/jvm/internal/impl/k/ak;

    move-result-object v0

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/k/am;->a(Lkotlin/reflect/jvm/internal/impl/k/ak;)Lkotlin/reflect/jvm/internal/impl/k/am;

    move-result-object v0

    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "@NotNull method %s.%s must not return null"

    new-array v2, v6, [Ljava/lang/Object;

    const-string v3, "kotlin/reflect/jvm/internal/impl/types/TypeSubstitutor"

    aput-object v3, v2, v4

    const-string v3, "createChainedSubstitutor"

    aput-object v3, v2, v5

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    return-object v0
.end method

.method public static a(Lkotlin/reflect/jvm/internal/impl/k/r;)Lkotlin/reflect/jvm/internal/impl/k/am;
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    if-nez p0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "context"

    aput-object v3, v2, v4

    const-string v3, "kotlin/reflect/jvm/internal/impl/types/TypeSubstitutor"

    aput-object v3, v2, v5

    const-string v3, "create"

    aput-object v3, v2, v6

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 65
    :cond_0
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/k/r;->i()Lkotlin/reflect/jvm/internal/impl/k/ad;

    move-result-object v0

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/k/r;->a()Ljava/util/List;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/reflect/jvm/internal/impl/k/ae;->a(Lkotlin/reflect/jvm/internal/impl/k/ad;Ljava/util/List;)Lkotlin/reflect/jvm/internal/impl/k/ak;

    move-result-object v0

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/k/am;->a(Lkotlin/reflect/jvm/internal/impl/k/ak;)Lkotlin/reflect/jvm/internal/impl/k/am;

    move-result-object v0

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "@NotNull method %s.%s must not return null"

    new-array v2, v6, [Ljava/lang/Object;

    const-string v3, "kotlin/reflect/jvm/internal/impl/types/TypeSubstitutor"

    aput-object v3, v2, v4

    const-string v3, "create"

    aput-object v3, v2, v5

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    return-object v0
.end method

.method public static a(Lkotlin/reflect/jvm/internal/impl/k/ap;Lkotlin/reflect/jvm/internal/impl/k/ah;)Lkotlin/reflect/jvm/internal/impl/k/ap;
    .locals 7

    .prologue
    const/4 v2, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    if-nez p0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "typeParameterVariance"

    aput-object v3, v2, v4

    const-string v3, "kotlin/reflect/jvm/internal/impl/types/TypeSubstitutor"

    aput-object v3, v2, v5

    const-string v3, "combine"

    aput-object v3, v2, v6

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    if-nez p1, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "typeProjection"

    aput-object v3, v2, v4

    const-string v3, "kotlin/reflect/jvm/internal/impl/types/TypeSubstitutor"

    aput-object v3, v2, v5

    const-string v3, "combine"

    aput-object v3, v2, v6

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 280
    :cond_1
    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/k/ah;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    sget-object v0, Lkotlin/reflect/jvm/internal/impl/k/ap;->c:Lkotlin/reflect/jvm/internal/impl/k/ap;

    if-nez v0, :cond_3

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "@NotNull method %s.%s must not return null"

    new-array v2, v6, [Ljava/lang/Object;

    const-string v3, "kotlin/reflect/jvm/internal/impl/types/TypeSubstitutor"

    aput-object v3, v2, v4

    const-string v3, "combine"

    aput-object v3, v2, v5

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 282
    :cond_2
    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/k/ah;->b()Lkotlin/reflect/jvm/internal/impl/k/ap;

    move-result-object v0

    invoke-static {p0, v0}, Lkotlin/reflect/jvm/internal/impl/k/am;->a(Lkotlin/reflect/jvm/internal/impl/k/ap;Lkotlin/reflect/jvm/internal/impl/k/ap;)Lkotlin/reflect/jvm/internal/impl/k/ap;

    move-result-object v0

    if-nez v0, :cond_3

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "@NotNull method %s.%s must not return null"

    new-array v2, v6, [Ljava/lang/Object;

    const-string v3, "kotlin/reflect/jvm/internal/impl/types/TypeSubstitutor"

    aput-object v3, v2, v4

    const-string v3, "combine"

    aput-object v3, v2, v5

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_3
    return-object v0
.end method

.method public static a(Lkotlin/reflect/jvm/internal/impl/k/ap;Lkotlin/reflect/jvm/internal/impl/k/ap;)Lkotlin/reflect/jvm/internal/impl/k/ap;
    .locals 7

    .prologue
    const/4 v2, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    if-nez p0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "typeParameterVariance"

    aput-object v3, v2, v4

    const-string v3, "kotlin/reflect/jvm/internal/impl/types/TypeSubstitutor"

    aput-object v3, v2, v5

    const-string v3, "combine"

    aput-object v3, v2, v6

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    if-nez p1, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "projectionKind"

    aput-object v3, v2, v4

    const-string v3, "kotlin/reflect/jvm/internal/impl/types/TypeSubstitutor"

    aput-object v3, v2, v5

    const-string v3, "combine"

    aput-object v3, v2, v6

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 287
    :cond_1
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/k/ap;->a:Lkotlin/reflect/jvm/internal/impl/k/ap;

    if-ne p0, v0, :cond_2

    if-nez p1, :cond_4

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "@NotNull method %s.%s must not return null"

    new-array v2, v6, [Ljava/lang/Object;

    const-string v3, "kotlin/reflect/jvm/internal/impl/types/TypeSubstitutor"

    aput-object v3, v2, v4

    const-string v3, "combine"

    aput-object v3, v2, v5

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 288
    :cond_2
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/k/ap;->a:Lkotlin/reflect/jvm/internal/impl/k/ap;

    if-ne p1, v0, :cond_5

    if-nez p0, :cond_3

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "@NotNull method %s.%s must not return null"

    new-array v2, v6, [Ljava/lang/Object;

    const-string v3, "kotlin/reflect/jvm/internal/impl/types/TypeSubstitutor"

    aput-object v3, v2, v4

    const-string v3, "combine"

    aput-object v3, v2, v5

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_3
    move-object p1, p0

    .line 289
    :cond_4
    return-object p1

    :cond_5
    if-ne p0, p1, :cond_6

    if-nez p1, :cond_4

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "@NotNull method %s.%s must not return null"

    new-array v2, v6, [Ljava/lang/Object;

    const-string v3, "kotlin/reflect/jvm/internal/impl/types/TypeSubstitutor"

    aput-object v3, v2, v4

    const-string v3, "combine"

    aput-object v3, v2, v5

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 290
    :cond_6
    new-instance v0, Ljava/lang/AssertionError;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Variance conflict: type parameter variance \'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\' and "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "projection kind \'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\' cannot be combined"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0
.end method

.method private static a(ILkotlin/reflect/jvm/internal/impl/k/ah;Lkotlin/reflect/jvm/internal/impl/k/ak;)V
    .locals 3

    .prologue
    .line 311
    const/16 v0, 0x64

    if-le p0, v0, :cond_0

    .line 312
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Recursion too deep. Most likely infinite loop while substituting "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {p1}, Lkotlin/reflect/jvm/internal/impl/k/am;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "; substitution: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {p2}, Lkotlin/reflect/jvm/internal/impl/k/am;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 314
    :cond_0
    return-void
.end method

.method private b(Lkotlin/reflect/jvm/internal/impl/k/ah;I)Lkotlin/reflect/jvm/internal/impl/k/ah;
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lkotlin/reflect/jvm/internal/impl/k/am$a;
        }
    .end annotation

    .prologue
    .line 224
    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/k/ah;->c()Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v0

    .line 225
    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/k/ah;->b()Lkotlin/reflect/jvm/internal/impl/k/ap;

    move-result-object v2

    .line 226
    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/k/r;->i()Lkotlin/reflect/jvm/internal/impl/k/ad;

    move-result-object v1

    invoke-interface {v1}, Lkotlin/reflect/jvm/internal/impl/k/ad;->d()Lkotlin/reflect/jvm/internal/impl/b/h;

    move-result-object v1

    instance-of v1, v1, Lkotlin/reflect/jvm/internal/impl/b/aq;

    if-eqz v1, :cond_0

    .line 247
    :goto_0
    return-object p1

    .line 232
    :cond_0
    const/4 v1, 0x0

    .line 233
    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/k/x;->b(Lkotlin/reflect/jvm/internal/impl/k/r;)Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v3

    .line 234
    if-eqz v3, :cond_1

    .line 235
    sget-object v1, Lkotlin/reflect/jvm/internal/impl/k/ap;->a:Lkotlin/reflect/jvm/internal/impl/k/ap;

    invoke-virtual {p0, v3, v1}, Lkotlin/reflect/jvm/internal/impl/k/am;->b(Lkotlin/reflect/jvm/internal/impl/k/r;Lkotlin/reflect/jvm/internal/impl/k/ap;)Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v1

    .line 238
    :cond_1
    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/k/r;->i()Lkotlin/reflect/jvm/internal/impl/k/ad;

    move-result-object v3

    invoke-interface {v3}, Lkotlin/reflect/jvm/internal/impl/k/ad;->b()Ljava/util/List;

    move-result-object v3

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/k/r;->a()Ljava/util/List;

    move-result-object v4

    invoke-direct {p0, v3, v4, p2}, Lkotlin/reflect/jvm/internal/impl/k/am;->a(Ljava/util/List;Ljava/util/List;I)Ljava/util/List;

    move-result-object v3

    .line 241
    iget-object v4, p0, Lkotlin/reflect/jvm/internal/impl/k/am;->c:Lkotlin/reflect/jvm/internal/impl/k/ak;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/k/r;->w()Lkotlin/reflect/jvm/internal/impl/b/a/h;

    move-result-object v5

    invoke-virtual {v4, v5}, Lkotlin/reflect/jvm/internal/impl/k/ak;->a(Lkotlin/reflect/jvm/internal/impl/b/a/h;)Lkotlin/reflect/jvm/internal/impl/b/a/h;

    move-result-object v4

    invoke-static {v0, v3, v4}, Lkotlin/reflect/jvm/internal/impl/k/al;->a(Lkotlin/reflect/jvm/internal/impl/k/r;Ljava/util/List;Lkotlin/reflect/jvm/internal/impl/b/a/h;)Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v0

    .line 243
    instance-of v3, v0, Lkotlin/reflect/jvm/internal/impl/k/v;

    if-eqz v3, :cond_2

    instance-of v3, v1, Lkotlin/reflect/jvm/internal/impl/k/v;

    if-eqz v3, :cond_2

    .line 244
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/v;

    check-cast v1, Lkotlin/reflect/jvm/internal/impl/k/v;

    invoke-static {v0, v1}, Lkotlin/reflect/jvm/internal/impl/k/x;->a(Lkotlin/reflect/jvm/internal/impl/k/v;Lkotlin/reflect/jvm/internal/impl/k/v;)Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v0

    .line 247
    :cond_2
    new-instance p1, Lkotlin/reflect/jvm/internal/impl/k/aj;

    invoke-direct {p1, v2, v0}, Lkotlin/reflect/jvm/internal/impl/k/aj;-><init>(Lkotlin/reflect/jvm/internal/impl/k/ap;Lkotlin/reflect/jvm/internal/impl/k/r;)V

    goto :goto_0
.end method

.method private static b(Lkotlin/reflect/jvm/internal/impl/k/ap;Lkotlin/reflect/jvm/internal/impl/k/ap;)Lkotlin/reflect/jvm/internal/impl/k/am$b;
    .locals 1

    .prologue
    .line 301
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/k/ap;->b:Lkotlin/reflect/jvm/internal/impl/k/ap;

    if-ne p0, v0, :cond_0

    sget-object v0, Lkotlin/reflect/jvm/internal/impl/k/ap;->c:Lkotlin/reflect/jvm/internal/impl/k/ap;

    if-ne p1, v0, :cond_0

    .line 302
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/k/am$b;->c:Lkotlin/reflect/jvm/internal/impl/k/am$b;

    .line 307
    :goto_0
    return-object v0

    .line 304
    :cond_0
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/k/ap;->c:Lkotlin/reflect/jvm/internal/impl/k/ap;

    if-ne p0, v0, :cond_1

    sget-object v0, Lkotlin/reflect/jvm/internal/impl/k/ap;->b:Lkotlin/reflect/jvm/internal/impl/k/ap;

    if-ne p1, v0, :cond_1

    .line 305
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/k/am$b;->b:Lkotlin/reflect/jvm/internal/impl/k/am$b;

    goto :goto_0

    .line 307
    :cond_1
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/k/am$b;->a:Lkotlin/reflect/jvm/internal/impl/k/am$b;

    goto :goto_0
.end method


# virtual methods
.method public a(Lkotlin/reflect/jvm/internal/impl/k/ah;)Lkotlin/reflect/jvm/internal/impl/k/ah;
    .locals 5

    .prologue
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "typeProjection"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "kotlin/reflect/jvm/internal/impl/types/TypeSubstitutor"

    aput-object v4, v2, v3

    const/4 v3, 0x2

    const-string v4, "substitute"

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 107
    :cond_0
    invoke-virtual {p0, p1}, Lkotlin/reflect/jvm/internal/impl/k/am;->b(Lkotlin/reflect/jvm/internal/impl/k/ah;)Lkotlin/reflect/jvm/internal/impl/k/ah;

    move-result-object v0

    .line 108
    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/k/am;->c:Lkotlin/reflect/jvm/internal/impl/k/ak;

    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/impl/k/ak;->c()Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/k/am;->c:Lkotlin/reflect/jvm/internal/impl/k/ak;

    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/impl/k/ak;->b()Z

    move-result v1

    if-nez v1, :cond_1

    .line 111
    :goto_0
    return-object v0

    :cond_1
    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/k/am;->c:Lkotlin/reflect/jvm/internal/impl/k/ak;

    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/impl/k/ak;->b()Z

    move-result v1

    invoke-static {v0, v1}, Lkotlin/reflect/jvm/internal/impl/k/d/b;->a(Lkotlin/reflect/jvm/internal/impl/k/ah;Z)Lkotlin/reflect/jvm/internal/impl/k/ah;

    move-result-object v0

    goto :goto_0
.end method

.method public a(Lkotlin/reflect/jvm/internal/impl/k/r;Lkotlin/reflect/jvm/internal/impl/k/ap;)Lkotlin/reflect/jvm/internal/impl/k/r;
    .locals 7

    .prologue
    const/4 v2, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "type"

    aput-object v3, v2, v4

    const-string v3, "kotlin/reflect/jvm/internal/impl/types/TypeSubstitutor"

    aput-object v3, v2, v5

    const-string v3, "safeSubstitute"

    aput-object v3, v2, v6

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    if-nez p2, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "howThisTypeIsUsed"

    aput-object v3, v2, v4

    const-string v3, "kotlin/reflect/jvm/internal/impl/types/TypeSubstitutor"

    aput-object v3, v2, v5

    const-string v3, "safeSubstitute"

    aput-object v3, v2, v6

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 87
    :cond_1
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/k/am;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 88
    if-nez p1, :cond_3

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "@NotNull method %s.%s must not return null"

    new-array v2, v6, [Ljava/lang/Object;

    const-string v3, "kotlin/reflect/jvm/internal/impl/types/TypeSubstitutor"

    aput-object v3, v2, v4

    const-string v3, "safeSubstitute"

    aput-object v3, v2, v5

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 92
    :cond_2
    :try_start_0
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/k/aj;

    invoke-direct {v0, p2, p1}, Lkotlin/reflect/jvm/internal/impl/k/aj;-><init>(Lkotlin/reflect/jvm/internal/impl/k/ap;Lkotlin/reflect/jvm/internal/impl/k/r;)V

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lkotlin/reflect/jvm/internal/impl/k/am;->a(Lkotlin/reflect/jvm/internal/impl/k/ah;I)Lkotlin/reflect/jvm/internal/impl/k/ah;

    move-result-object v0

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/k/ah;->c()Lkotlin/reflect/jvm/internal/impl/k/r;
    :try_end_0
    .catch Lkotlin/reflect/jvm/internal/impl/k/am$a; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object p1

    if-nez p1, :cond_3

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "@NotNull method %s.%s must not return null"

    new-array v2, v6, [Ljava/lang/Object;

    const-string v3, "kotlin/reflect/jvm/internal/impl/types/TypeSubstitutor"

    aput-object v3, v2, v4

    const-string v3, "safeSubstitute"

    aput-object v3, v2, v5

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 93
    :catch_0
    move-exception v0

    .line 94
    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/k/am$a;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/k/l;->c(Ljava/lang/String;)Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object p1

    if-nez p1, :cond_3

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "@NotNull method %s.%s must not return null"

    new-array v2, v6, [Ljava/lang/Object;

    const-string v3, "kotlin/reflect/jvm/internal/impl/types/TypeSubstitutor"

    aput-object v3, v2, v4

    const-string v3, "safeSubstitute"

    aput-object v3, v2, v5

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_3
    return-object p1
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 77
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/k/am;->c:Lkotlin/reflect/jvm/internal/impl/k/ak;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/k/ak;->a()Z

    move-result v0

    return v0
.end method

.method public b(Lkotlin/reflect/jvm/internal/impl/k/ah;)Lkotlin/reflect/jvm/internal/impl/k/ah;
    .locals 5

    .prologue
    const/4 v4, 0x0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "typeProjection"

    aput-object v3, v2, v4

    const/4 v3, 0x1

    const-string v4, "kotlin/reflect/jvm/internal/impl/types/TypeSubstitutor"

    aput-object v4, v2, v3

    const/4 v3, 0x2

    const-string v4, "substituteWithoutApproximation"

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 117
    :cond_0
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/k/am;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 124
    :goto_0
    return-object p1

    .line 122
    :cond_1
    const/4 v0, 0x0

    :try_start_0
    invoke-direct {p0, p1, v0}, Lkotlin/reflect/jvm/internal/impl/k/am;->a(Lkotlin/reflect/jvm/internal/impl/k/ah;I)Lkotlin/reflect/jvm/internal/impl/k/ah;
    :try_end_0
    .catch Lkotlin/reflect/jvm/internal/impl/k/am$a; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object p1

    goto :goto_0

    .line 123
    :catch_0
    move-exception v0

    .line 124
    const/4 p1, 0x0

    goto :goto_0
.end method

.method public b()Lkotlin/reflect/jvm/internal/impl/k/ak;
    .locals 5

    .prologue
    .line 82
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/k/am;->c:Lkotlin/reflect/jvm/internal/impl/k/ak;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "@NotNull method %s.%s must not return null"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "kotlin/reflect/jvm/internal/impl/types/TypeSubstitutor"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "getSubstitution"

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    return-object v0
.end method

.method public b(Lkotlin/reflect/jvm/internal/impl/k/r;Lkotlin/reflect/jvm/internal/impl/k/ap;)Lkotlin/reflect/jvm/internal/impl/k/r;
    .locals 7

    .prologue
    const/4 v2, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "type"

    aput-object v3, v2, v4

    const-string v3, "kotlin/reflect/jvm/internal/impl/types/TypeSubstitutor"

    aput-object v3, v2, v5

    const-string v3, "substitute"

    aput-object v3, v2, v6

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    if-nez p2, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "howThisTypeIsUsed"

    aput-object v3, v2, v4

    const-string v3, "kotlin/reflect/jvm/internal/impl/types/TypeSubstitutor"

    aput-object v3, v2, v5

    const-string v3, "substitute"

    aput-object v3, v2, v6

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 100
    :cond_1
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/k/aj;

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/k/am;->b()Lkotlin/reflect/jvm/internal/impl/k/ak;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Lkotlin/reflect/jvm/internal/impl/k/ak;->a(Lkotlin/reflect/jvm/internal/impl/k/r;Lkotlin/reflect/jvm/internal/impl/k/ap;)Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v1

    invoke-direct {v0, p2, v1}, Lkotlin/reflect/jvm/internal/impl/k/aj;-><init>(Lkotlin/reflect/jvm/internal/impl/k/ap;Lkotlin/reflect/jvm/internal/impl/k/r;)V

    invoke-virtual {p0, v0}, Lkotlin/reflect/jvm/internal/impl/k/am;->a(Lkotlin/reflect/jvm/internal/impl/k/ah;)Lkotlin/reflect/jvm/internal/impl/k/ah;

    move-result-object v0

    .line 102
    if-nez v0, :cond_2

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_2
    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/k/ah;->c()Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v0

    goto :goto_0
.end method
