.class public final Lkotlin/reflect/jvm/internal/impl/k/i;
.super Lkotlin/reflect/jvm/internal/impl/k/ak;
.source "DisjointKeysUnionTypeSubstitution.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lkotlin/reflect/jvm/internal/impl/k/i$a;
    }
.end annotation


# static fields
.field public static final a:Lkotlin/reflect/jvm/internal/impl/k/i$a;


# instance fields
.field private final b:Lkotlin/reflect/jvm/internal/impl/k/ak;

.field private final c:Lkotlin/reflect/jvm/internal/impl/k/ak;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lkotlin/reflect/jvm/internal/impl/k/i$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lkotlin/reflect/jvm/internal/impl/k/i$a;-><init>(Lkotlin/d/b/i;)V

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/k/i;->a:Lkotlin/reflect/jvm/internal/impl/k/i$a;

    return-void
.end method

.method private constructor <init>(Lkotlin/reflect/jvm/internal/impl/k/ak;Lkotlin/reflect/jvm/internal/impl/k/ak;)V
    .locals 0

    .prologue
    .line 21
    .line 24
    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/k/ak;-><init>()V

    iput-object p1, p0, Lkotlin/reflect/jvm/internal/impl/k/i;->b:Lkotlin/reflect/jvm/internal/impl/k/ak;

    iput-object p2, p0, Lkotlin/reflect/jvm/internal/impl/k/i;->c:Lkotlin/reflect/jvm/internal/impl/k/ak;

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/reflect/jvm/internal/impl/k/ak;Lkotlin/reflect/jvm/internal/impl/k/ak;Lkotlin/d/b/i;)V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0, p1, p2}, Lkotlin/reflect/jvm/internal/impl/k/i;-><init>(Lkotlin/reflect/jvm/internal/impl/k/ak;Lkotlin/reflect/jvm/internal/impl/k/ak;)V

    return-void
.end method

.method public static final a(Lkotlin/reflect/jvm/internal/impl/k/ak;Lkotlin/reflect/jvm/internal/impl/k/ak;)Lkotlin/reflect/jvm/internal/impl/k/ak;
    .locals 1

    const-string v0, "first"

    invoke-static {p0, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "second"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v0, Lkotlin/reflect/jvm/internal/impl/k/i;->a:Lkotlin/reflect/jvm/internal/impl/k/i$a;

    invoke-virtual {v0, p0, p1}, Lkotlin/reflect/jvm/internal/impl/k/i$a;->a(Lkotlin/reflect/jvm/internal/impl/k/ak;Lkotlin/reflect/jvm/internal/impl/k/ak;)Lkotlin/reflect/jvm/internal/impl/k/ak;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public a(Lkotlin/reflect/jvm/internal/impl/b/a/h;)Lkotlin/reflect/jvm/internal/impl/b/a/h;
    .locals 2

    .prologue
    const-string v0, "annotations"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 43
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/k/i;->c:Lkotlin/reflect/jvm/internal/impl/k/ak;

    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/k/i;->b:Lkotlin/reflect/jvm/internal/impl/k/ak;

    invoke-virtual {v1, p1}, Lkotlin/reflect/jvm/internal/impl/k/ak;->a(Lkotlin/reflect/jvm/internal/impl/b/a/h;)Lkotlin/reflect/jvm/internal/impl/b/a/h;

    move-result-object v1

    invoke-virtual {v0, v1}, Lkotlin/reflect/jvm/internal/impl/k/ak;->a(Lkotlin/reflect/jvm/internal/impl/b/a/h;)Lkotlin/reflect/jvm/internal/impl/b/a/h;

    move-result-object v0

    return-object v0
.end method

.method public a(Lkotlin/reflect/jvm/internal/impl/k/r;Lkotlin/reflect/jvm/internal/impl/k/ap;)Lkotlin/reflect/jvm/internal/impl/k/r;
    .locals 2

    .prologue
    const-string v0, "topLevelType"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "position"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 36
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/k/i;->c:Lkotlin/reflect/jvm/internal/impl/k/ak;

    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/k/i;->b:Lkotlin/reflect/jvm/internal/impl/k/ak;

    invoke-virtual {v1, p1, p2}, Lkotlin/reflect/jvm/internal/impl/k/ak;->a(Lkotlin/reflect/jvm/internal/impl/k/r;Lkotlin/reflect/jvm/internal/impl/k/ap;)Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v1

    invoke-virtual {v0, v1, p2}, Lkotlin/reflect/jvm/internal/impl/k/ak;->a(Lkotlin/reflect/jvm/internal/impl/k/r;Lkotlin/reflect/jvm/internal/impl/k/ap;)Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v0

    return-object v0
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 38
    const/4 v0, 0x0

    return v0
.end method

.method public b(Lkotlin/reflect/jvm/internal/impl/k/r;)Lkotlin/reflect/jvm/internal/impl/k/ah;
    .locals 1

    .prologue
    const-string v0, "key"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 34
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/k/i;->b:Lkotlin/reflect/jvm/internal/impl/k/ak;

    invoke-virtual {v0, p1}, Lkotlin/reflect/jvm/internal/impl/k/ak;->b(Lkotlin/reflect/jvm/internal/impl/k/r;)Lkotlin/reflect/jvm/internal/impl/k/ah;

    move-result-object v0

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/k/i;->c:Lkotlin/reflect/jvm/internal/impl/k/ak;

    invoke-virtual {v0, p1}, Lkotlin/reflect/jvm/internal/impl/k/ak;->b(Lkotlin/reflect/jvm/internal/impl/k/r;)Lkotlin/reflect/jvm/internal/impl/k/ah;

    move-result-object v0

    goto :goto_0
.end method

.method public b()Z
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/k/i;->b:Lkotlin/reflect/jvm/internal/impl/k/ak;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/k/ak;->b()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/k/i;->c:Lkotlin/reflect/jvm/internal/impl/k/ak;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/k/ak;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public c()Z
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/k/i;->b:Lkotlin/reflect/jvm/internal/impl/k/ak;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/k/ak;->c()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/k/i;->c:Lkotlin/reflect/jvm/internal/impl/k/ak;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/k/ak;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
