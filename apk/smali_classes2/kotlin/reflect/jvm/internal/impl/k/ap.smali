.class public final enum Lkotlin/reflect/jvm/internal/impl/k/ap;
.super Ljava/lang/Enum;
.source "Variance.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lkotlin/reflect/jvm/internal/impl/k/ap;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lkotlin/reflect/jvm/internal/impl/k/ap;

.field public static final enum b:Lkotlin/reflect/jvm/internal/impl/k/ap;

.field public static final enum c:Lkotlin/reflect/jvm/internal/impl/k/ap;

.field private static final synthetic d:[Lkotlin/reflect/jvm/internal/impl/k/ap;


# instance fields
.field private final e:Ljava/lang/String;

.field private final f:Z

.field private final g:Z

.field private final h:I


# direct methods
.method static constructor <clinit>()V
    .locals 14

    .prologue
    const/4 v13, 0x2

    const/4 v2, 0x0

    const/4 v4, 0x1

    const/4 v0, 0x3

    new-array v12, v0, [Lkotlin/reflect/jvm/internal/impl/k/ap;

    new-instance v0, Lkotlin/reflect/jvm/internal/impl/k/ap;

    const-string v1, "INVARIANT"

    const-string v3, ""

    move v5, v4

    move v6, v2

    .line 25
    invoke-direct/range {v0 .. v6}, Lkotlin/reflect/jvm/internal/impl/k/ap;-><init>(Ljava/lang/String;ILjava/lang/String;ZZI)V

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/k/ap;->a:Lkotlin/reflect/jvm/internal/impl/k/ap;

    aput-object v0, v12, v2

    new-instance v5, Lkotlin/reflect/jvm/internal/impl/k/ap;

    const-string v6, "IN_VARIANCE"

    const-string v8, "in"

    const/4 v11, -0x1

    move v7, v4

    move v9, v4

    move v10, v2

    .line 26
    invoke-direct/range {v5 .. v11}, Lkotlin/reflect/jvm/internal/impl/k/ap;-><init>(Ljava/lang/String;ILjava/lang/String;ZZI)V

    sput-object v5, Lkotlin/reflect/jvm/internal/impl/k/ap;->b:Lkotlin/reflect/jvm/internal/impl/k/ap;

    aput-object v5, v12, v4

    new-instance v5, Lkotlin/reflect/jvm/internal/impl/k/ap;

    const-string v6, "OUT_VARIANCE"

    const-string v8, "out"

    move v7, v13

    move v9, v2

    move v10, v4

    move v11, v4

    .line 27
    invoke-direct/range {v5 .. v11}, Lkotlin/reflect/jvm/internal/impl/k/ap;-><init>(Ljava/lang/String;ILjava/lang/String;ZZI)V

    sput-object v5, Lkotlin/reflect/jvm/internal/impl/k/ap;->c:Lkotlin/reflect/jvm/internal/impl/k/ap;

    aput-object v5, v12, v13

    sput-object v12, Lkotlin/reflect/jvm/internal/impl/k/ap;->d:[Lkotlin/reflect/jvm/internal/impl/k/ap;

    return-void
.end method

.method protected constructor <init>(Ljava/lang/String;ILjava/lang/String;ZZI)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "ZZI)V"
        }
    .end annotation

    .prologue
    const-string v0, "label"

    invoke-static {p3, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 19
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput-object p3, p0, Lkotlin/reflect/jvm/internal/impl/k/ap;->e:Ljava/lang/String;

    iput-boolean p4, p0, Lkotlin/reflect/jvm/internal/impl/k/ap;->f:Z

    iput-boolean p5, p0, Lkotlin/reflect/jvm/internal/impl/k/ap;->g:Z

    iput p6, p0, Lkotlin/reflect/jvm/internal/impl/k/ap;->h:I

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lkotlin/reflect/jvm/internal/impl/k/ap;
    .locals 1

    const-class v0, Lkotlin/reflect/jvm/internal/impl/k/ap;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/ap;

    return-object v0
.end method

.method public static values()[Lkotlin/reflect/jvm/internal/impl/k/ap;
    .locals 1

    sget-object v0, Lkotlin/reflect/jvm/internal/impl/k/ap;->d:[Lkotlin/reflect/jvm/internal/impl/k/ap;

    invoke-virtual {v0}, [Lkotlin/reflect/jvm/internal/impl/k/ap;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lkotlin/reflect/jvm/internal/impl/k/ap;

    return-object v0
.end method


# virtual methods
.method public final a()Lkotlin/reflect/jvm/internal/impl/k/ap;
    .locals 2

    .prologue
    .line 47
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/k/aq;->b:[I

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/k/ap;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 50
    new-instance v0, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {v0}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw v0

    .line 48
    :pswitch_0
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/k/ap;->a:Lkotlin/reflect/jvm/internal/impl/k/ap;

    .line 47
    :goto_0
    return-object v0

    .line 49
    :pswitch_1
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/k/ap;->c:Lkotlin/reflect/jvm/internal/impl/k/ap;

    goto :goto_0

    .line 50
    :pswitch_2
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/k/ap;->b:Lkotlin/reflect/jvm/internal/impl/k/ap;

    goto :goto_0

    .line 47
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final a(Lkotlin/reflect/jvm/internal/impl/k/ap;)Z
    .locals 2

    .prologue
    const-string v0, "position"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 30
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/k/aq;->a:[I

    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/k/ap;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 33
    new-instance v0, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {v0}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw v0

    .line 31
    :pswitch_0
    iget-boolean v0, p0, Lkotlin/reflect/jvm/internal/impl/k/ap;->f:Z

    .line 34
    :goto_0
    return v0

    .line 32
    :pswitch_1
    iget-boolean v0, p0, Lkotlin/reflect/jvm/internal/impl/k/ap;->g:Z

    goto :goto_0

    .line 33
    :pswitch_2
    iget-boolean v0, p0, Lkotlin/reflect/jvm/internal/impl/k/ap;->f:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lkotlin/reflect/jvm/internal/impl/k/ap;->g:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 30
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 20
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/k/ap;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 22
    iget-boolean v0, p0, Lkotlin/reflect/jvm/internal/impl/k/ap;->g:Z

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/k/ap;->e:Ljava/lang/String;

    return-object v0
.end method
