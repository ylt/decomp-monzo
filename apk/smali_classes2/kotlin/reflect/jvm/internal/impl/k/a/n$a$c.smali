.class public final Lkotlin/reflect/jvm/internal/impl/k/a/n$a$c;
.super Lkotlin/reflect/jvm/internal/impl/k/a/n$a;
.source "TypeCheckerContext.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lkotlin/reflect/jvm/internal/impl/k/a/n$a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "c"
.end annotation


# static fields
.field public static final a:Lkotlin/reflect/jvm/internal/impl/k/a/n$a$c;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 99
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/k/a/n$a$c;

    invoke-direct {v0}, Lkotlin/reflect/jvm/internal/impl/k/a/n$a$c;-><init>()V

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 99
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lkotlin/reflect/jvm/internal/impl/k/a/n$a;-><init>(Lkotlin/d/b/i;)V

    check-cast p0, Lkotlin/reflect/jvm/internal/impl/k/a/n$a$c;

    sput-object p0, Lkotlin/reflect/jvm/internal/impl/k/a/n$a$c;->a:Lkotlin/reflect/jvm/internal/impl/k/a/n$a$c;

    return-void
.end method


# virtual methods
.method public synthetic a(Lkotlin/reflect/jvm/internal/impl/k/r;)Lkotlin/reflect/jvm/internal/impl/k/v;
    .locals 1

    .prologue
    .line 99
    invoke-virtual {p0, p1}, Lkotlin/reflect/jvm/internal/impl/k/a/n$a$c;->b(Lkotlin/reflect/jvm/internal/impl/k/r;)Ljava/lang/Void;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/v;

    return-object v0
.end method

.method public b(Lkotlin/reflect/jvm/internal/impl/k/r;)Ljava/lang/Void;
    .locals 2

    .prologue
    const-string v0, "type"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 100
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Should not be called"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method
