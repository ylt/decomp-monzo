.class public final Lkotlin/reflect/jvm/internal/impl/k/a/j;
.super Ljava/lang/Object;
.source "NewKotlinTypeChecker.kt"


# direct methods
.method public static final synthetic a(Lkotlin/reflect/jvm/internal/impl/k/v;)Z
    .locals 1

    .prologue
    .line 1
    invoke-static {p0}, Lkotlin/reflect/jvm/internal/impl/k/a/j;->d(Lkotlin/reflect/jvm/internal/impl/k/v;)Z

    move-result v0

    return v0
.end method

.method public static final synthetic b(Lkotlin/reflect/jvm/internal/impl/k/v;)Z
    .locals 1

    .prologue
    .line 1
    invoke-static {p0}, Lkotlin/reflect/jvm/internal/impl/k/a/j;->e(Lkotlin/reflect/jvm/internal/impl/k/v;)Z

    move-result v0

    return v0
.end method

.method public static final synthetic c(Lkotlin/reflect/jvm/internal/impl/k/v;)Z
    .locals 1

    .prologue
    .line 1
    invoke-static {p0}, Lkotlin/reflect/jvm/internal/impl/k/a/j;->f(Lkotlin/reflect/jvm/internal/impl/k/v;)Z

    move-result v0

    return v0
.end method

.method private static final d(Lkotlin/reflect/jvm/internal/impl/k/v;)Z
    .locals 1

    .prologue
    .line 384
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/k/v;->i()Lkotlin/reflect/jvm/internal/impl/k/ad;

    move-result-object v0

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/k/ad;->d()Lkotlin/reflect/jvm/internal/impl/b/h;

    move-result-object v0

    instance-of v0, v0, Lkotlin/reflect/jvm/internal/impl/b/e;

    return v0
.end method

.method private static final e(Lkotlin/reflect/jvm/internal/impl/k/v;)Z
    .locals 1

    .prologue
    .line 397
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/k/v;->d()Z

    move-result v0

    if-nez v0, :cond_1

    .line 396
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/k/v;->i()Lkotlin/reflect/jvm/internal/impl/k/ad;

    move-result-object v0

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/k/ad;->d()Lkotlin/reflect/jvm/internal/impl/b/h;

    move-result-object v0

    instance-of v0, v0, Lkotlin/reflect/jvm/internal/impl/b/ap;

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/k/v;->i()Lkotlin/reflect/jvm/internal/impl/k/ad;

    move-result-object v0

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/k/ad;->d()Lkotlin/reflect/jvm/internal/impl/b/h;

    move-result-object v0

    if-nez v0, :cond_0

    .line 397
    instance-of v0, p0, Lkotlin/reflect/jvm/internal/impl/h/a/a/a;

    if-nez v0, :cond_0

    instance-of v0, p0, Lkotlin/reflect/jvm/internal/impl/k/a/e;

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static final f(Lkotlin/reflect/jvm/internal/impl/k/v;)Z
    .locals 1

    .prologue
    .line 400
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/k/v;->i()Lkotlin/reflect/jvm/internal/impl/k/ad;

    move-result-object v0

    instance-of v0, v0, Lkotlin/reflect/jvm/internal/impl/k/q;

    return v0
.end method
