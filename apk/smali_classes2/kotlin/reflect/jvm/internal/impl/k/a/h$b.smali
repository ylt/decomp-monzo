.class final Lkotlin/reflect/jvm/internal/impl/k/a/h$b;
.super Lkotlin/d/b/m;
.source "NewKotlinTypeChecker.kt"

# interfaces
.implements Lkotlin/d/a/b;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lkotlin/reflect/jvm/internal/impl/k/a/h;->b(Lkotlin/reflect/jvm/internal/impl/k/a/n;Lkotlin/reflect/jvm/internal/impl/k/v;Lkotlin/reflect/jvm/internal/impl/k/ad;)Ljava/util/List;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/d/b/m;",
        "Lkotlin/d/a/b",
        "<",
        "Lkotlin/reflect/jvm/internal/impl/k/v;",
        "Lkotlin/reflect/jvm/internal/impl/k/a/n$a;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lkotlin/reflect/jvm/internal/impl/k/ad;

.field final synthetic b:Lkotlin/d/b/x$c;


# direct methods
.method constructor <init>(Lkotlin/reflect/jvm/internal/impl/k/ad;Lkotlin/d/b/x$c;)V
    .locals 1

    iput-object p1, p0, Lkotlin/reflect/jvm/internal/impl/k/a/h$b;->a:Lkotlin/reflect/jvm/internal/impl/k/ad;

    iput-object p2, p0, Lkotlin/reflect/jvm/internal/impl/k/a/h$b;->b:Lkotlin/d/b/x$c;

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lkotlin/d/b/m;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 79
    check-cast p1, Lkotlin/reflect/jvm/internal/impl/k/v;

    invoke-virtual {p0, p1}, Lkotlin/reflect/jvm/internal/impl/k/a/h$b;->a(Lkotlin/reflect/jvm/internal/impl/k/v;)Lkotlin/reflect/jvm/internal/impl/k/a/n$a;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lkotlin/reflect/jvm/internal/impl/k/v;)Lkotlin/reflect/jvm/internal/impl/k/a/n$a;
    .locals 4

    .prologue
    const/4 v2, 0x0

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 244
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/k/a/a;->a:Lkotlin/reflect/jvm/internal/impl/k/a/a;

    const/4 v1, 0x4

    invoke-static {p1, v0, v2, v1, v2}, Lkotlin/reflect/jvm/internal/impl/k/a/g;->a(Lkotlin/reflect/jvm/internal/impl/k/v;Lkotlin/reflect/jvm/internal/impl/k/a/a;Lkotlin/d/a/m;ILjava/lang/Object;)Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v1

    .line 246
    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/impl/k/v;->i()Lkotlin/reflect/jvm/internal/impl/k/ad;

    move-result-object v0

    iget-object v2, p0, Lkotlin/reflect/jvm/internal/impl/k/a/h$b;->a:Lkotlin/reflect/jvm/internal/impl/k/ad;

    invoke-static {v0, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 247
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/k/a/h$b;->b:Lkotlin/d/b/x$c;

    iget-object v0, v0, Lkotlin/d/b/x$c;->a:Ljava/lang/Object;

    check-cast v0, Ljava/util/List;

    if-nez v0, :cond_0

    .line 248
    iget-object v2, p0, Lkotlin/reflect/jvm/internal/impl/k/a/h$b;->b:Lkotlin/d/b/x$c;

    new-instance v0, Lkotlin/reflect/jvm/internal/impl/utils/e;

    invoke-direct {v0}, Lkotlin/reflect/jvm/internal/impl/utils/e;-><init>()V

    check-cast v0, Ljava/util/List;

    iput-object v0, v2, Lkotlin/d/b/x$c;->a:Ljava/lang/Object;

    .line 250
    :cond_0
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/k/a/h$b;->b:Lkotlin/d/b/x$c;

    iget-object v0, v0, Lkotlin/d/b/x$c;->a:Ljava/lang/Object;

    check-cast v0, Ljava/util/List;

    if-nez v0, :cond_1

    invoke-static {}, Lkotlin/d/b/l;->a()V

    :cond_1
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 252
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/k/a/n$a$c;->a:Lkotlin/reflect/jvm/internal/impl/k/a/n$a$c;

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/a/n$a;

    .line 259
    :goto_0
    return-object v0

    .line 254
    :cond_2
    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/impl/k/v;->a()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 255
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/k/a/n$a$a;->a:Lkotlin/reflect/jvm/internal/impl/k/a/n$a$a;

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/a/n$a;

    goto :goto_0

    .line 258
    :cond_3
    new-instance v2, Lkotlin/reflect/jvm/internal/impl/k/a/n$a$b;

    sget-object v3, Lkotlin/reflect/jvm/internal/impl/k/ae;->b:Lkotlin/reflect/jvm/internal/impl/k/ae$a;

    move-object v0, v1

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/r;

    invoke-virtual {v3, v0}, Lkotlin/reflect/jvm/internal/impl/k/ae$a;->a(Lkotlin/reflect/jvm/internal/impl/k/r;)Lkotlin/reflect/jvm/internal/impl/k/ak;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/k/ak;->f()Lkotlin/reflect/jvm/internal/impl/k/am;

    move-result-object v0

    invoke-direct {v2, v0}, Lkotlin/reflect/jvm/internal/impl/k/a/n$a$b;-><init>(Lkotlin/reflect/jvm/internal/impl/k/am;)V

    move-object v0, v2

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/a/n$a;

    goto :goto_0
.end method
