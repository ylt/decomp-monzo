.class public final Lkotlin/reflect/jvm/internal/impl/k/ar;
.super Ljava/lang/Object;
.source "VarianceChecker.kt"


# direct methods
.method public static final a(Lkotlin/reflect/jvm/internal/impl/k/af;Lkotlin/reflect/jvm/internal/impl/k/ap;Lkotlin/d/a/q;Lkotlin/d/a/b;)Z
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<D::",
            "Lkotlin/reflect/jvm/internal/impl/k/af",
            "<+TD;>;>(TD;",
            "Lkotlin/reflect/jvm/internal/impl/k/ap;",
            "Lkotlin/d/a/q",
            "<-",
            "Lkotlin/reflect/jvm/internal/impl/b/aq;",
            "-TD;-",
            "Lkotlin/reflect/jvm/internal/impl/k/ap;",
            "Lkotlin/n;",
            ">;",
            "Lkotlin/d/a/b",
            "<-",
            "Lkotlin/reflect/jvm/internal/impl/b/aq;",
            "+",
            "Lkotlin/reflect/jvm/internal/impl/k/ap;",
            ">;)Z"
        }
    .end annotation

    .prologue
    const-string v0, "$receiver"

    invoke-static {p0, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "position"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "reportError"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "customVariance"

    invoke-static {p3, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 41
    invoke-interface {p0}, Lkotlin/reflect/jvm/internal/impl/k/af;->a()Lkotlin/h;

    move-result-object v0

    if-eqz v0, :cond_1

    check-cast v0, Lkotlin/h;

    .line 42
    invoke-virtual {v0}, Lkotlin/h;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lkotlin/reflect/jvm/internal/impl/k/af;

    invoke-static {v1, p1, p2, p3}, Lkotlin/reflect/jvm/internal/impl/k/ar;->a(Lkotlin/reflect/jvm/internal/impl/k/af;Lkotlin/reflect/jvm/internal/impl/k/ap;Lkotlin/d/a/q;Lkotlin/d/a/b;)Z

    move-result v1

    invoke-virtual {v0}, Lkotlin/h;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/af;

    invoke-static {v0, p1, p2, p3}, Lkotlin/reflect/jvm/internal/impl/k/ar;->a(Lkotlin/reflect/jvm/internal/impl/k/af;Lkotlin/reflect/jvm/internal/impl/k/ap;Lkotlin/d/a/q;Lkotlin/d/a/b;)Z

    move-result v0

    and-int/2addr v1, v0

    .line 71
    :cond_0
    :goto_0
    return v1

    .line 46
    :cond_1
    invoke-interface {p0}, Lkotlin/reflect/jvm/internal/impl/k/af;->c()Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/k/r;->i()Lkotlin/reflect/jvm/internal/impl/k/ad;

    move-result-object v0

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/k/ad;->d()Lkotlin/reflect/jvm/internal/impl/b/h;

    move-result-object v1

    .line 47
    instance-of v0, v1, Lkotlin/reflect/jvm/internal/impl/b/aq;

    if-eqz v0, :cond_4

    .line 48
    invoke-interface {p3, v1}, Lkotlin/d/a/b;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/ap;

    if-eqz v0, :cond_3

    .line 49
    :goto_1
    invoke-virtual {v0, p1}, Lkotlin/reflect/jvm/internal/impl/k/ap;->a(Lkotlin/reflect/jvm/internal/impl/k/ap;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 50
    invoke-interface {p0}, Lkotlin/reflect/jvm/internal/impl/k/af;->c()Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v2

    invoke-virtual {v2}, Lkotlin/reflect/jvm/internal/impl/k/r;->w()Lkotlin/reflect/jvm/internal/impl/b/a/h;

    move-result-object v2

    sget-object v3, Lkotlin/reflect/jvm/internal/impl/a/l;->h:Lkotlin/reflect/jvm/internal/impl/a/l$a;

    iget-object v3, v3, Lkotlin/reflect/jvm/internal/impl/a/l$a;->I:Lkotlin/reflect/jvm/internal/impl/e/b;

    const-string v4, "org.jetbrains.kotlin.bui\u2026s.FQ_NAMES.unsafeVariance"

    invoke-static {v3, v4}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v2, v3}, Lkotlin/reflect/jvm/internal/impl/b/a/h;->b(Lkotlin/reflect/jvm/internal/impl/e/b;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 51
    invoke-interface {p2, v1, p0, p1}, Lkotlin/d/a/q;->a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 53
    :cond_2
    invoke-virtual {v0, p1}, Lkotlin/reflect/jvm/internal/impl/k/ap;->a(Lkotlin/reflect/jvm/internal/impl/k/ap;)Z

    move-result v1

    goto :goto_0

    :cond_3
    move-object v0, v1

    .line 48
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/aq;

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/b/aq;->k()Lkotlin/reflect/jvm/internal/impl/k/ap;

    move-result-object v0

    const-string v2, "classifierDescriptor.variance"

    invoke-static {v0, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_1

    .line 56
    :cond_4
    const/4 v0, 0x1

    .line 57
    invoke-interface {p0}, Lkotlin/reflect/jvm/internal/impl/k/af;->b()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v1, v0

    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/ag;

    .line 58
    if-eqz v0, :cond_5

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/k/ag;->b()Lkotlin/reflect/jvm/internal/impl/b/aq;

    move-result-object v2

    if-eqz v2, :cond_5

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/k/ag;->a()Lkotlin/reflect/jvm/internal/impl/k/ah;

    move-result-object v2

    invoke-interface {v2}, Lkotlin/reflect/jvm/internal/impl/k/ah;->a()Z

    move-result v2

    if-eqz v2, :cond_6

    :cond_5
    move v0, v1

    :goto_3
    move v1, v0

    .line 57
    goto :goto_2

    .line 60
    :cond_6
    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/k/ag;->b()Lkotlin/reflect/jvm/internal/impl/b/aq;

    move-result-object v2

    if-nez v2, :cond_7

    invoke-static {}, Lkotlin/d/b/l;->a()V

    :cond_7
    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/k/ag;->a()Lkotlin/reflect/jvm/internal/impl/k/ah;

    move-result-object v4

    invoke-static {v2, v4}, Lkotlin/reflect/jvm/internal/impl/k/a/p;->a(Lkotlin/reflect/jvm/internal/impl/b/aq;Lkotlin/reflect/jvm/internal/impl/k/ah;)Lkotlin/reflect/jvm/internal/impl/k/a/p$a;

    move-result-object v2

    if-nez v2, :cond_8

    invoke-static {}, Lkotlin/d/b/l;->a()V

    .line 61
    :cond_8
    sget-object v4, Lkotlin/reflect/jvm/internal/impl/k/as;->a:[I

    invoke-virtual {v2}, Lkotlin/reflect/jvm/internal/impl/k/a/p$a;->ordinal()I

    move-result v2

    aget v2, v4, v2

    packed-switch v2, :pswitch_data_0

    .line 65
    new-instance v0, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {v0}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw v0

    :pswitch_0
    move-object v2, p1

    .line 67
    :goto_4
    if-eqz v2, :cond_9

    .line 68
    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/k/ag;->d()Lkotlin/reflect/jvm/internal/impl/k/af;

    move-result-object v0

    invoke-static {v0, v2, p2, p3}, Lkotlin/reflect/jvm/internal/impl/k/ar;->a(Lkotlin/reflect/jvm/internal/impl/k/af;Lkotlin/reflect/jvm/internal/impl/k/ap;Lkotlin/d/a/q;Lkotlin/d/a/b;)Z

    move-result v0

    and-int/2addr v1, v0

    move v0, v1

    goto :goto_3

    .line 63
    :pswitch_1
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/k/ap;->a()Lkotlin/reflect/jvm/internal/impl/k/ap;

    move-result-object v2

    goto :goto_4

    .line 64
    :pswitch_2
    sget-object v2, Lkotlin/reflect/jvm/internal/impl/k/ap;->a:Lkotlin/reflect/jvm/internal/impl/k/ap;

    goto :goto_4

    .line 65
    :pswitch_3
    const/4 v2, 0x0

    goto :goto_4

    :cond_9
    move v0, v1

    goto :goto_3

    .line 61
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
