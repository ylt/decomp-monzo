.class public abstract Lkotlin/reflect/jvm/internal/impl/k/b;
.super Lkotlin/reflect/jvm/internal/impl/k/c;
.source "AbstractClassTypeConstructor.java"

# interfaces
.implements Lkotlin/reflect/jvm/internal/impl/k/ad;


# instance fields
.field private a:I


# direct methods
.method public constructor <init>(Lkotlin/reflect/jvm/internal/impl/j/i;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "storageManager"

    aput-object v3, v2, v4

    const/4 v3, 0x1

    const-string v4, "kotlin/reflect/jvm/internal/impl/types/AbstractClassTypeConstructor"

    aput-object v4, v2, v3

    const/4 v3, 0x2

    const-string v4, "<init>"

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 36
    :cond_0
    invoke-direct {p0, p1}, Lkotlin/reflect/jvm/internal/impl/k/c;-><init>(Lkotlin/reflect/jvm/internal/impl/j/i;)V

    .line 33
    iput v4, p0, Lkotlin/reflect/jvm/internal/impl/k/b;->a:I

    .line 37
    return-void
.end method

.method private static a(Lkotlin/reflect/jvm/internal/impl/b/h;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    if-nez p0, :cond_0

    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/Object;

    const-string v5, "descriptor"

    aput-object v5, v4, v1

    const-string v1, "kotlin/reflect/jvm/internal/impl/types/AbstractClassTypeConstructor"

    aput-object v1, v4, v0

    const/4 v0, 0x2

    const-string v1, "hasMeaningfulFqName"

    aput-object v1, v4, v0

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 98
    :cond_0
    invoke-static {p0}, Lkotlin/reflect/jvm/internal/impl/k/l;->a(Lkotlin/reflect/jvm/internal/impl/b/m;)Z

    move-result v2

    if-nez v2, :cond_1

    invoke-static {p0}, Lkotlin/reflect/jvm/internal/impl/h/c;->b(Lkotlin/reflect/jvm/internal/impl/b/m;)Z

    move-result v2

    if-nez v2, :cond_1

    :goto_0
    return v0

    :cond_1
    move v0, v1

    goto :goto_0
.end method


# virtual methods
.method public abstract d()Lkotlin/reflect/jvm/internal/impl/b/h;
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 67
    instance-of v0, p1, Lkotlin/reflect/jvm/internal/impl/k/ad;

    if-nez v0, :cond_1

    .line 94
    :cond_0
    :goto_0
    return v2

    .line 70
    :cond_1
    invoke-virtual {p1}, Ljava/lang/Object;->hashCode()I

    move-result v0

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/k/b;->hashCode()I

    move-result v3

    if-ne v0, v3, :cond_0

    move-object v0, p1

    .line 74
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/ad;

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/k/ad;->b()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/k/b;->b()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-ne v0, v3, :cond_0

    .line 76
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/k/b;->d()Lkotlin/reflect/jvm/internal/impl/b/h;

    move-result-object v3

    move-object v0, p1

    .line 77
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/ad;

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/k/ad;->d()Lkotlin/reflect/jvm/internal/impl/b/h;

    move-result-object v0

    .line 80
    if-ne v3, v0, :cond_2

    move v2, v1

    goto :goto_0

    .line 83
    :cond_2
    invoke-static {v3}, Lkotlin/reflect/jvm/internal/impl/k/b;->a(Lkotlin/reflect/jvm/internal/impl/b/h;)Z

    move-result v4

    if-eqz v4, :cond_3

    if-eqz v0, :cond_5

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/k/b;->a(Lkotlin/reflect/jvm/internal/impl/b/h;)Z

    move-result v4

    if-nez v4, :cond_5

    .line 85
    :cond_3
    if-ne p0, p1, :cond_4

    move v0, v1

    :goto_1
    move v2, v0

    goto :goto_0

    :cond_4
    move v0, v2

    goto :goto_1

    .line 88
    :cond_5
    instance-of v1, v3, Lkotlin/reflect/jvm/internal/impl/b/e;

    if-eqz v1, :cond_0

    instance-of v1, v0, Lkotlin/reflect/jvm/internal/impl/b/e;

    if-eqz v1, :cond_0

    .line 89
    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/h/c;->d(Lkotlin/reflect/jvm/internal/impl/b/m;)Lkotlin/reflect/jvm/internal/impl/e/c;

    move-result-object v0

    .line 90
    invoke-static {v3}, Lkotlin/reflect/jvm/internal/impl/h/c;->d(Lkotlin/reflect/jvm/internal/impl/b/m;)Lkotlin/reflect/jvm/internal/impl/e/c;

    move-result-object v1

    .line 91
    invoke-virtual {v1, v0}, Lkotlin/reflect/jvm/internal/impl/e/c;->equals(Ljava/lang/Object;)Z

    move-result v2

    goto :goto_0
.end method

.method public f()Lkotlin/reflect/jvm/internal/impl/a/l;
    .locals 5

    .prologue
    .line 62
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/k/b;->d()Lkotlin/reflect/jvm/internal/impl/b/h;

    move-result-object v0

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/h/c/a;->d(Lkotlin/reflect/jvm/internal/impl/b/m;)Lkotlin/reflect/jvm/internal/impl/a/l;

    move-result-object v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "@NotNull method %s.%s must not return null"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "kotlin/reflect/jvm/internal/impl/types/AbstractClassTypeConstructor"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "getBuiltIns"

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    return-object v0
.end method

.method public final hashCode()I
    .locals 2

    .prologue
    .line 41
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/k/b;->a:I

    .line 42
    if-eqz v0, :cond_0

    .line 52
    :goto_0
    return v0

    .line 44
    :cond_0
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/k/b;->d()Lkotlin/reflect/jvm/internal/impl/b/h;

    move-result-object v0

    .line 45
    instance-of v1, v0, Lkotlin/reflect/jvm/internal/impl/b/e;

    if-eqz v1, :cond_1

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/k/b;->a(Lkotlin/reflect/jvm/internal/impl/b/h;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 46
    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/h/c;->d(Lkotlin/reflect/jvm/internal/impl/b/m;)Lkotlin/reflect/jvm/internal/impl/e/c;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/e/c;->hashCode()I

    move-result v0

    .line 51
    :goto_1
    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/k/b;->a:I

    goto :goto_0

    .line 49
    :cond_1
    invoke-static {p0}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    move-result v0

    goto :goto_1
.end method

.method protected i()Ljava/util/Collection;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/k/r;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 111
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/k/b;->d()Lkotlin/reflect/jvm/internal/impl/b/h;

    move-result-object v0

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/b/h;->y_()Lkotlin/reflect/jvm/internal/impl/b/m;

    move-result-object v0

    .line 112
    instance-of v1, v0, Lkotlin/reflect/jvm/internal/impl/b/e;

    if-eqz v1, :cond_0

    .line 113
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/e;

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/b/e;->E_()Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Collections;->singleton(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v0

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "@NotNull method %s.%s must not return null"

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "kotlin/reflect/jvm/internal/impl/types/AbstractClassTypeConstructor"

    aput-object v3, v2, v4

    const-string v3, "getAdditionalNeighboursInSupertypeGraph"

    aput-object v3, v2, v5

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 115
    :cond_0
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "@NotNull method %s.%s must not return null"

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "kotlin/reflect/jvm/internal/impl/types/AbstractClassTypeConstructor"

    aput-object v3, v2, v4

    const-string v3, "getAdditionalNeighboursInSupertypeGraph"

    aput-object v3, v2, v5

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    return-object v0
.end method
