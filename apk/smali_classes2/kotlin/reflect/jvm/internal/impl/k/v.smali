.class public abstract Lkotlin/reflect/jvm/internal/impl/k/v;
.super Lkotlin/reflect/jvm/internal/impl/k/ao;
.source "KotlinType.kt"


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 122
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lkotlin/reflect/jvm/internal/impl/k/ao;-><init>(Lkotlin/d/b/i;)V

    return-void
.end method


# virtual methods
.method public abstract a(Z)Lkotlin/reflect/jvm/internal/impl/k/v;
.end method

.method public abstract c(Lkotlin/reflect/jvm/internal/impl/b/a/h;)Lkotlin/reflect/jvm/internal/impl/k/v;
.end method

.method public toString()Ljava/lang/String;
    .locals 12

    .prologue
    const/4 v5, 0x0

    const/4 v6, 0x0

    .line 127
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    move-object v10, v11

    check-cast v10, Ljava/lang/StringBuilder;

    .line 128
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/k/v;->w()Lkotlin/reflect/jvm/internal/impl/b/a/h;

    move-result-object v0

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/b/a/h;->c()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/a/g;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/b/a/g;->c()Lkotlin/reflect/jvm/internal/impl/b/a/c;

    move-result-object v2

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/b/a/g;->d()Lkotlin/reflect/jvm/internal/impl/b/a/e;

    move-result-object v0

    .line 129
    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/String;

    const-string v4, "["

    aput-object v4, v3, v5

    const/4 v4, 0x1

    sget-object v7, Lkotlin/reflect/jvm/internal/impl/g/c;->g:Lkotlin/reflect/jvm/internal/impl/g/c;

    invoke-virtual {v7, v2, v0}, Lkotlin/reflect/jvm/internal/impl/g/c;->a(Lkotlin/reflect/jvm/internal/impl/b/a/c;Lkotlin/reflect/jvm/internal/impl/b/a/e;)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v3, v4

    const/4 v0, 0x2

    const-string v2, "] "

    aput-object v2, v3, v0

    invoke-static {v10, v3}, Lkotlin/h/j;->a(Ljava/lang/StringBuilder;[Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 132
    :cond_0
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/k/v;->i()Lkotlin/reflect/jvm/internal/impl/k/ad;

    move-result-object v0

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 133
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/k/v;->a()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/k/v;->a()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    move-object v1, v10

    check-cast v1, Ljava/lang/Appendable;

    const-string v2, ", "

    check-cast v2, Ljava/lang/CharSequence;

    const-string v3, "<"

    check-cast v3, Ljava/lang/CharSequence;

    const-string v4, ">"

    check-cast v4, Ljava/lang/CharSequence;

    const/16 v8, 0x70

    move-object v7, v6

    move-object v9, v6

    invoke-static/range {v0 .. v9}, Lkotlin/a/m;->a(Ljava/lang/Iterable;Ljava/lang/Appendable;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;ILjava/lang/CharSequence;Lkotlin/d/a/b;ILjava/lang/Object;)Ljava/lang/Appendable;

    .line 134
    :cond_1
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/k/v;->c()Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "?"

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 135
    :cond_2
    nop

    .line 127
    check-cast v11, Ljava/lang/StringBuilder;

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "StringBuilder().apply(builderAction).toString()"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method
