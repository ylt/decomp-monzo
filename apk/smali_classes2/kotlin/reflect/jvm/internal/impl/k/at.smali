.class public abstract Lkotlin/reflect/jvm/internal/impl/k/at;
.super Lkotlin/reflect/jvm/internal/impl/k/r;
.source "KotlinType.kt"


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 70
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lkotlin/reflect/jvm/internal/impl/k/r;-><init>(Lkotlin/d/b/i;)V

    return-void
.end method


# virtual methods
.method public a()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/k/ah;",
            ">;"
        }
    .end annotation

    .prologue
    .line 76
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/k/at;->e()Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/k/r;->a()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public b()Lkotlin/reflect/jvm/internal/impl/h/e/h;
    .locals 1

    .prologue
    .line 78
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/k/at;->e()Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/k/r;->b()Lkotlin/reflect/jvm/internal/impl/h/e/h;

    move-result-object v0

    return-object v0
.end method

.method public c()Z
    .locals 1

    .prologue
    .line 77
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/k/at;->e()Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/k/r;->c()Z

    move-result v0

    return v0
.end method

.method public d()Z
    .locals 1

    .prologue
    .line 79
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/k/at;->e()Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/k/r;->d()Z

    move-result v0

    return v0
.end method

.method protected abstract e()Lkotlin/reflect/jvm/internal/impl/k/r;
.end method

.method public f()Z
    .locals 1

    .prologue
    .line 71
    const/4 v0, 0x1

    return v0
.end method

.method public i()Lkotlin/reflect/jvm/internal/impl/k/ad;
    .locals 1

    .prologue
    .line 75
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/k/at;->e()Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/k/r;->i()Lkotlin/reflect/jvm/internal/impl/k/ad;

    move-result-object v0

    return-object v0
.end method

.method public final j()Lkotlin/reflect/jvm/internal/impl/k/ao;
    .locals 2

    .prologue
    .line 82
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/k/at;->e()Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v0

    .line 83
    :goto_0
    instance-of v1, v0, Lkotlin/reflect/jvm/internal/impl/k/at;

    if-eqz v1, :cond_0

    .line 84
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/at;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/k/at;->e()Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v0

    goto :goto_0

    .line 86
    :cond_0
    if-nez v0, :cond_1

    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type org.jetbrains.kotlin.types.UnwrappedType"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/ao;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 90
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/k/at;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 91
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/k/at;->e()Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/k/r;->toString()Ljava/lang/String;

    move-result-object v0

    .line 94
    :goto_0
    return-object v0

    :cond_0
    const-string v0, "<Not computed yet>"

    goto :goto_0
.end method

.method public w()Lkotlin/reflect/jvm/internal/impl/b/a/h;
    .locals 1

    .prologue
    .line 74
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/k/at;->e()Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/k/r;->w()Lkotlin/reflect/jvm/internal/impl/b/a/h;

    move-result-object v0

    return-object v0
.end method
