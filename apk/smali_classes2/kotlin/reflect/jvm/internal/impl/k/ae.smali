.class public abstract Lkotlin/reflect/jvm/internal/impl/k/ae;
.super Lkotlin/reflect/jvm/internal/impl/k/ak;
.source "TypeSubstitution.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lkotlin/reflect/jvm/internal/impl/k/ae$a;
    }
.end annotation


# static fields
.field public static final b:Lkotlin/reflect/jvm/internal/impl/k/ae$a;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lkotlin/reflect/jvm/internal/impl/k/ae$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lkotlin/reflect/jvm/internal/impl/k/ae$a;-><init>(Lkotlin/d/b/i;)V

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/k/ae;->b:Lkotlin/reflect/jvm/internal/impl/k/ae$a;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 46
    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/k/ak;-><init>()V

    return-void
.end method

.method public static final a(Ljava/util/Map;)Lkotlin/reflect/jvm/internal/impl/k/ae;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/k/ad;",
            "+",
            "Lkotlin/reflect/jvm/internal/impl/k/ah;",
            ">;)",
            "Lkotlin/reflect/jvm/internal/impl/k/ae;"
        }
    .end annotation

    sget-object v0, Lkotlin/reflect/jvm/internal/impl/k/ae;->b:Lkotlin/reflect/jvm/internal/impl/k/ae$a;

    const/4 v1, 0x0

    const/4 v2, 0x2

    const/4 v3, 0x0

    invoke-static {v0, p0, v1, v2, v3}, Lkotlin/reflect/jvm/internal/impl/k/ae$a;->a(Lkotlin/reflect/jvm/internal/impl/k/ae$a;Ljava/util/Map;ZILjava/lang/Object;)Lkotlin/reflect/jvm/internal/impl/k/ae;

    move-result-object v0

    return-object v0
.end method

.method public static final a(Lkotlin/reflect/jvm/internal/impl/k/ad;Ljava/util/List;)Lkotlin/reflect/jvm/internal/impl/k/ak;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/reflect/jvm/internal/impl/k/ad;",
            "Ljava/util/List",
            "<+",
            "Lkotlin/reflect/jvm/internal/impl/k/ah;",
            ">;)",
            "Lkotlin/reflect/jvm/internal/impl/k/ak;"
        }
    .end annotation

    const-string v0, "typeConstructor"

    invoke-static {p0, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "arguments"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v0, Lkotlin/reflect/jvm/internal/impl/k/ae;->b:Lkotlin/reflect/jvm/internal/impl/k/ae$a;

    invoke-virtual {v0, p0, p1}, Lkotlin/reflect/jvm/internal/impl/k/ae$a;->a(Lkotlin/reflect/jvm/internal/impl/k/ad;Ljava/util/List;)Lkotlin/reflect/jvm/internal/impl/k/ak;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public abstract a(Lkotlin/reflect/jvm/internal/impl/k/ad;)Lkotlin/reflect/jvm/internal/impl/k/ah;
.end method

.method public b(Lkotlin/reflect/jvm/internal/impl/k/r;)Lkotlin/reflect/jvm/internal/impl/k/ah;
    .locals 1

    .prologue
    const-string v0, "key"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 47
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/k/r;->i()Lkotlin/reflect/jvm/internal/impl/k/ad;

    move-result-object v0

    invoke-virtual {p0, v0}, Lkotlin/reflect/jvm/internal/impl/k/ae;->a(Lkotlin/reflect/jvm/internal/impl/k/ad;)Lkotlin/reflect/jvm/internal/impl/k/ah;

    move-result-object v0

    return-object v0
.end method
