.class final Lkotlin/reflect/jvm/internal/impl/k/a/d$1;
.super Lkotlin/reflect/jvm/internal/impl/k/a/o;
.source "KotlinTypeCheckerImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lkotlin/reflect/jvm/internal/impl/k/a/d;->a(Lkotlin/reflect/jvm/internal/impl/k/a/c$a;)Lkotlin/reflect/jvm/internal/impl/k/a/c;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic a:Lkotlin/reflect/jvm/internal/impl/k/a/c$a;


# direct methods
.method constructor <init>(Lkotlin/reflect/jvm/internal/impl/k/a/c$a;)V
    .locals 0

    .prologue
    .line 28
    iput-object p1, p0, Lkotlin/reflect/jvm/internal/impl/k/a/d$1;->a:Lkotlin/reflect/jvm/internal/impl/k/a/c$a;

    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/k/a/o;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lkotlin/reflect/jvm/internal/impl/k/ad;Lkotlin/reflect/jvm/internal/impl/k/ad;)Z
    .locals 7

    .prologue
    const/4 v4, 0x3

    const/4 v6, 0x2

    const/4 v1, 0x1

    const/4 v0, 0x0

    if-nez p1, :cond_0

    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    new-array v4, v4, [Ljava/lang/Object;

    const-string v5, "constructor1"

    aput-object v5, v4, v0

    const-string v0, "kotlin/reflect/jvm/internal/impl/types/checker/KotlinTypeCheckerImpl$1"

    aput-object v0, v4, v1

    const-string v0, "assertEqualTypeConstructors"

    aput-object v0, v4, v6

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_0
    if-nez p2, :cond_1

    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    new-array v4, v4, [Ljava/lang/Object;

    const-string v5, "constructor2"

    aput-object v5, v4, v0

    const-string v0, "kotlin/reflect/jvm/internal/impl/types/checker/KotlinTypeCheckerImpl$1"

    aput-object v0, v4, v1

    const-string v0, "assertEqualTypeConstructors"

    aput-object v0, v4, v6

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 31
    :cond_1
    invoke-virtual {p1, p2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    iget-object v2, p0, Lkotlin/reflect/jvm/internal/impl/k/a/d$1;->a:Lkotlin/reflect/jvm/internal/impl/k/a/c$a;

    invoke-interface {v2, p1, p2}, Lkotlin/reflect/jvm/internal/impl/k/a/c$a;->a(Lkotlin/reflect/jvm/internal/impl/k/ad;Lkotlin/reflect/jvm/internal/impl/k/ad;)Z

    move-result v2

    if-eqz v2, :cond_3

    :cond_2
    move v0, v1

    :cond_3
    return v0
.end method
