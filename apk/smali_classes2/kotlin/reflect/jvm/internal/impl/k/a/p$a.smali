.class public final enum Lkotlin/reflect/jvm/internal/impl/k/a/p$a;
.super Ljava/lang/Enum;
.source "TypeCheckingProcedure.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lkotlin/reflect/jvm/internal/impl/k/a/p;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lkotlin/reflect/jvm/internal/impl/k/a/p$a;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lkotlin/reflect/jvm/internal/impl/k/a/p$a;

.field public static final enum b:Lkotlin/reflect/jvm/internal/impl/k/a/p$a;

.field public static final enum c:Lkotlin/reflect/jvm/internal/impl/k/a/p$a;

.field public static final enum d:Lkotlin/reflect/jvm/internal/impl/k/a/p$a;

.field private static final synthetic e:[Lkotlin/reflect/jvm/internal/impl/k/a/p$a;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 129
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/k/a/p$a;

    const-string v1, "IN"

    invoke-direct {v0, v1, v2}, Lkotlin/reflect/jvm/internal/impl/k/a/p$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/k/a/p$a;->a:Lkotlin/reflect/jvm/internal/impl/k/a/p$a;

    new-instance v0, Lkotlin/reflect/jvm/internal/impl/k/a/p$a;

    const-string v1, "OUT"

    invoke-direct {v0, v1, v3}, Lkotlin/reflect/jvm/internal/impl/k/a/p$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/k/a/p$a;->b:Lkotlin/reflect/jvm/internal/impl/k/a/p$a;

    new-instance v0, Lkotlin/reflect/jvm/internal/impl/k/a/p$a;

    const-string v1, "INV"

    invoke-direct {v0, v1, v4}, Lkotlin/reflect/jvm/internal/impl/k/a/p$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/k/a/p$a;->c:Lkotlin/reflect/jvm/internal/impl/k/a/p$a;

    new-instance v0, Lkotlin/reflect/jvm/internal/impl/k/a/p$a;

    const-string v1, "STAR"

    invoke-direct {v0, v1, v5}, Lkotlin/reflect/jvm/internal/impl/k/a/p$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/k/a/p$a;->d:Lkotlin/reflect/jvm/internal/impl/k/a/p$a;

    .line 128
    const/4 v0, 0x4

    new-array v0, v0, [Lkotlin/reflect/jvm/internal/impl/k/a/p$a;

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/k/a/p$a;->a:Lkotlin/reflect/jvm/internal/impl/k/a/p$a;

    aput-object v1, v0, v2

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/k/a/p$a;->b:Lkotlin/reflect/jvm/internal/impl/k/a/p$a;

    aput-object v1, v0, v3

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/k/a/p$a;->c:Lkotlin/reflect/jvm/internal/impl/k/a/p$a;

    aput-object v1, v0, v4

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/k/a/p$a;->d:Lkotlin/reflect/jvm/internal/impl/k/a/p$a;

    aput-object v1, v0, v5

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/k/a/p$a;->e:[Lkotlin/reflect/jvm/internal/impl/k/a/p$a;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 128
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static a(Lkotlin/reflect/jvm/internal/impl/k/ap;)Lkotlin/reflect/jvm/internal/impl/k/a/p$a;
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    if-nez p0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "variance"

    aput-object v3, v2, v4

    const-string v3, "kotlin/reflect/jvm/internal/impl/types/checker/TypeCheckingProcedure$EnrichedProjectionKind"

    aput-object v3, v2, v5

    const-string v3, "fromVariance"

    aput-object v3, v2, v6

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 133
    :cond_0
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/k/a/p$1;->a:[I

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/k/ap;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 141
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Unknown variance"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 135
    :pswitch_0
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/k/a/p$a;->c:Lkotlin/reflect/jvm/internal/impl/k/a/p$a;

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "@NotNull method %s.%s must not return null"

    new-array v2, v6, [Ljava/lang/Object;

    const-string v3, "kotlin/reflect/jvm/internal/impl/types/checker/TypeCheckingProcedure$EnrichedProjectionKind"

    aput-object v3, v2, v4

    const-string v3, "fromVariance"

    aput-object v3, v2, v5

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 137
    :pswitch_1
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/k/a/p$a;->a:Lkotlin/reflect/jvm/internal/impl/k/a/p$a;

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "@NotNull method %s.%s must not return null"

    new-array v2, v6, [Ljava/lang/Object;

    const-string v3, "kotlin/reflect/jvm/internal/impl/types/checker/TypeCheckingProcedure$EnrichedProjectionKind"

    aput-object v3, v2, v4

    const-string v3, "fromVariance"

    aput-object v3, v2, v5

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 139
    :pswitch_2
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/k/a/p$a;->b:Lkotlin/reflect/jvm/internal/impl/k/a/p$a;

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "@NotNull method %s.%s must not return null"

    new-array v2, v6, [Ljava/lang/Object;

    const-string v3, "kotlin/reflect/jvm/internal/impl/types/checker/TypeCheckingProcedure$EnrichedProjectionKind"

    aput-object v3, v2, v4

    const-string v3, "fromVariance"

    aput-object v3, v2, v5

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    return-object v0

    .line 133
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lkotlin/reflect/jvm/internal/impl/k/a/p$a;
    .locals 1

    .prologue
    .line 128
    const-class v0, Lkotlin/reflect/jvm/internal/impl/k/a/p$a;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/a/p$a;

    return-object v0
.end method

.method public static values()[Lkotlin/reflect/jvm/internal/impl/k/a/p$a;
    .locals 1

    .prologue
    .line 128
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/k/a/p$a;->e:[Lkotlin/reflect/jvm/internal/impl/k/a/p$a;

    invoke-virtual {v0}, [Lkotlin/reflect/jvm/internal/impl/k/a/p$a;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lkotlin/reflect/jvm/internal/impl/k/a/p$a;

    return-object v0
.end method
