.class final Lkotlin/reflect/jvm/internal/impl/k/am$1;
.super Ljava/lang/Object;
.source "TypeSubstitutor.java"

# interfaces
.implements Lkotlin/d/a/b;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lkotlin/reflect/jvm/internal/impl/k/am;->a(Lkotlin/reflect/jvm/internal/impl/b/a/h;)Lkotlin/reflect/jvm/internal/impl/b/a/h;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lkotlin/d/a/b",
        "<",
        "Lkotlin/reflect/jvm/internal/impl/e/b;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 212
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lkotlin/reflect/jvm/internal/impl/e/b;)Ljava/lang/Boolean;
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    if-nez p1, :cond_0

    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/Object;

    const-string v5, "name"

    aput-object v5, v4, v1

    const-string v1, "kotlin/reflect/jvm/internal/impl/types/TypeSubstitutor$1"

    aput-object v1, v4, v0

    const/4 v0, 0x2

    const-string v1, "invoke"

    aput-object v1, v4, v0

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 215
    :cond_0
    sget-object v2, Lkotlin/reflect/jvm/internal/impl/a/l;->h:Lkotlin/reflect/jvm/internal/impl/a/l$a;

    iget-object v2, v2, Lkotlin/reflect/jvm/internal/impl/a/l$a;->I:Lkotlin/reflect/jvm/internal/impl/e/b;

    invoke-virtual {p1, v2}, Lkotlin/reflect/jvm/internal/impl/e/b;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 212
    check-cast p1, Lkotlin/reflect/jvm/internal/impl/e/b;

    invoke-virtual {p0, p1}, Lkotlin/reflect/jvm/internal/impl/k/am$1;->a(Lkotlin/reflect/jvm/internal/impl/e/b;)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method
