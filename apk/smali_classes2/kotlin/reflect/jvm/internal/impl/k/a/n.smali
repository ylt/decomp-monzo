.class public Lkotlin/reflect/jvm/internal/impl/k/a/n;
.super Ljava/lang/Object;
.source "TypeCheckerContext.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lkotlin/reflect/jvm/internal/impl/k/a/n$a;
    }
.end annotation


# instance fields
.field private a:I

.field private b:Z

.field private c:Ljava/util/ArrayDeque;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayDeque",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/k/v;",
            ">;"
        }
    .end annotation
.end field

.field private d:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/k/v;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Z


# direct methods
.method public constructor <init>(Z)V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean p1, p0, Lkotlin/reflect/jvm/internal/impl/k/a/n;->e:Z

    return-void
.end method

.method public static final synthetic a(Lkotlin/reflect/jvm/internal/impl/k/a/n;)I
    .locals 1

    .prologue
    .line 24
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/k/a/n;->a:I

    return v0
.end method

.method public static final synthetic a(Lkotlin/reflect/jvm/internal/impl/k/a/n;I)V
    .locals 0

    .prologue
    .line 24
    iput p1, p0, Lkotlin/reflect/jvm/internal/impl/k/a/n;->a:I

    return-void
.end method

.method private final b()V
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 45
    iget-boolean v0, p0, Lkotlin/reflect/jvm/internal/impl/k/a/n;->b:Z

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    sget-boolean v2, Lkotlin/p;->a:Z

    if-eqz v2, :cond_1

    if-nez v0, :cond_1

    const-string v1, "Assertion failed"

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 46
    :cond_1
    iput-boolean v1, p0, Lkotlin/reflect/jvm/internal/impl/k/a/n;->b:Z

    .line 48
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/k/a/n;->c:Ljava/util/ArrayDeque;

    if-nez v0, :cond_2

    .line 49
    new-instance v0, Ljava/util/ArrayDeque;

    invoke-direct {v0}, Ljava/util/ArrayDeque;-><init>()V

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/k/a/n;->c:Ljava/util/ArrayDeque;

    .line 51
    :cond_2
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/k/a/n;->d:Ljava/util/Set;

    if-nez v0, :cond_3

    .line 52
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/utils/f;->a:Lkotlin/reflect/jvm/internal/impl/utils/f$b;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/utils/f$b;->a()Lkotlin/reflect/jvm/internal/impl/utils/f;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/k/a/n;->d:Ljava/util/Set;

    .line 54
    :cond_3
    return-void
.end method

.method private final c()V
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/k/a/n;->c:Ljava/util/ArrayDeque;

    if-nez v0, :cond_0

    invoke-static {}, Lkotlin/d/b/l;->a()V

    :cond_0
    invoke-virtual {v0}, Ljava/util/ArrayDeque;->clear()V

    .line 58
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/k/a/n;->d:Ljava/util/Set;

    if-nez v0, :cond_1

    invoke-static {}, Lkotlin/d/b/l;->a()V

    :cond_1
    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 59
    const/4 v0, 0x0

    iput-boolean v0, p0, Lkotlin/reflect/jvm/internal/impl/k/a/n;->b:Z

    .line 60
    return-void
.end method


# virtual methods
.method public a(Lkotlin/reflect/jvm/internal/impl/k/ao;Lkotlin/reflect/jvm/internal/impl/k/ao;)Ljava/lang/Boolean;
    .locals 1

    .prologue
    const-string v0, "subType"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "superType"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 31
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 24
    iget-boolean v0, p0, Lkotlin/reflect/jvm/internal/impl/k/a/n;->e:Z

    return v0
.end method

.method public final a(Lkotlin/reflect/jvm/internal/impl/k/v;Lkotlin/d/a/b;Lkotlin/d/a/b;)Z
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/reflect/jvm/internal/impl/k/v;",
            "Lkotlin/d/a/b",
            "<-",
            "Lkotlin/reflect/jvm/internal/impl/k/v;",
            "Ljava/lang/Boolean;",
            ">;",
            "Lkotlin/d/a/b",
            "<-",
            "Lkotlin/reflect/jvm/internal/impl/k/v;",
            "+",
            "Lkotlin/reflect/jvm/internal/impl/k/a/n$a;",
            ">;)Z"
        }
    .end annotation

    .prologue
    const/4 v6, 0x1

    const/4 v4, 0x0

    const/4 v1, 0x0

    const-string v0, "start"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "predicate"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "supertypesPolicy"

    invoke-static {p3, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 67
    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/k/a/n;->b()V

    .line 69
    iget-object v2, p0, Lkotlin/reflect/jvm/internal/impl/k/a/n;->c:Ljava/util/ArrayDeque;

    if-nez v2, :cond_0

    invoke-static {}, Lkotlin/d/b/l;->a()V

    .line 70
    :cond_0
    iget-object v3, p0, Lkotlin/reflect/jvm/internal/impl/k/a/n;->d:Ljava/util/Set;

    if-nez v3, :cond_1

    invoke-static {}, Lkotlin/d/b/l;->a()V

    .line 72
    :cond_1
    invoke-virtual {v2, p1}, Ljava/util/ArrayDeque;->push(Ljava/lang/Object;)V

    :cond_2
    move-object v0, v2

    .line 73
    check-cast v0, Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    move v0, v6

    :goto_0
    if-eqz v0, :cond_6

    .line 74
    invoke-interface {v3}, Ljava/util/Set;->size()I

    move-result v0

    const/16 v5, 0x3e8

    if-le v0, v5, :cond_4

    .line 75
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Too many supertypes for type: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ". Supertypes = "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    move-object v0, v3

    check-cast v0, Ljava/lang/Iterable;

    const/16 v7, 0x3f

    move-object v2, v1

    move-object v3, v1

    move-object v5, v1

    move-object v6, v1

    move-object v8, v1

    invoke-static/range {v0 .. v8}, Lkotlin/a/m;->a(Ljava/lang/Iterable;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;ILjava/lang/CharSequence;Lkotlin/d/a/b;ILjava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    :cond_3
    move v0, v4

    .line 73
    goto :goto_0

    .line 77
    :cond_4
    invoke-virtual {v2}, Ljava/util/ArrayDeque;->pop()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/v;

    .line 79
    const-string v5, "current"

    invoke-static {v0, v5}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v3, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 83
    const-string v5, "current"

    invoke-static {v0, v5}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {p2, v0}, Lkotlin/d/a/b;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Boolean;

    invoke-virtual {v5}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v5

    if-eqz v5, :cond_5

    .line 84
    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/k/a/n;->c()V

    move v4, v6

    .line 93
    :goto_1
    return v4

    .line 88
    :cond_5
    const-string v5, "current"

    invoke-static {v0, v5}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {p3, v0}, Lkotlin/d/a/b;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    sget-object v5, Lkotlin/reflect/jvm/internal/impl/k/a/n$b;->a:Lkotlin/reflect/jvm/internal/impl/k/a/n$b;

    check-cast v5, Lkotlin/d/a/b;

    invoke-static {v7, v5}, Lkotlin/reflect/jvm/internal/impl/utils/a/a;->a(Ljava/lang/Object;Lkotlin/d/a/b;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lkotlin/reflect/jvm/internal/impl/k/a/n$a;

    if-eqz v5, :cond_2

    .line 89
    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/k/v;->i()Lkotlin/reflect/jvm/internal/impl/k/ad;

    move-result-object v0

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/k/ad;->G_()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_2
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/r;

    const-string v8, "supertype"

    invoke-static {v0, v8}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v5, v0}, Lkotlin/reflect/jvm/internal/impl/k/a/n$a;->a(Lkotlin/reflect/jvm/internal/impl/k/r;)Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/util/ArrayDeque;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 92
    :cond_6
    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/k/a/n;->c()V

    goto :goto_1
.end method
