.class final Lkotlin/reflect/jvm/internal/impl/k/a/h$c;
.super Lkotlin/d/b/m;
.source "NewKotlinTypeChecker.kt"

# interfaces
.implements Lkotlin/d/a/q;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lkotlin/reflect/jvm/internal/impl/k/a/h;->a(Lkotlin/reflect/jvm/internal/impl/k/a/n;Lkotlin/reflect/jvm/internal/impl/k/v;Lkotlin/reflect/jvm/internal/impl/k/ad;)Ljava/util/List;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/d/b/m;",
        "Lkotlin/d/a/q",
        "<",
        "Lkotlin/reflect/jvm/internal/impl/k/a/n;",
        "Lkotlin/reflect/jvm/internal/impl/k/v;",
        "Lkotlin/reflect/jvm/internal/impl/k/ad;",
        "Ljava/util/List",
        "<+",
        "Lkotlin/reflect/jvm/internal/impl/k/v;",
        ">;>;"
    }
.end annotation


# static fields
.field public static final a:Lkotlin/reflect/jvm/internal/impl/k/a/h$c;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lkotlin/reflect/jvm/internal/impl/k/a/h$c;

    invoke-direct {v0}, Lkotlin/reflect/jvm/internal/impl/k/a/h$c;-><init>()V

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/k/a/h$c;->a:Lkotlin/reflect/jvm/internal/impl/k/a/h$c;

    return-void
.end method

.method constructor <init>()V
    .locals 1

    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lkotlin/d/b/m;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 79
    check-cast p1, Lkotlin/reflect/jvm/internal/impl/k/a/n;

    check-cast p2, Lkotlin/reflect/jvm/internal/impl/k/v;

    check-cast p3, Lkotlin/reflect/jvm/internal/impl/k/ad;

    invoke-virtual {p0, p1, p2, p3}, Lkotlin/reflect/jvm/internal/impl/k/a/h$c;->a(Lkotlin/reflect/jvm/internal/impl/k/a/n;Lkotlin/reflect/jvm/internal/impl/k/v;Lkotlin/reflect/jvm/internal/impl/k/ad;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lkotlin/reflect/jvm/internal/impl/k/a/n;Lkotlin/reflect/jvm/internal/impl/k/v;Lkotlin/reflect/jvm/internal/impl/k/ad;)Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/reflect/jvm/internal/impl/k/a/n;",
            "Lkotlin/reflect/jvm/internal/impl/k/v;",
            "Lkotlin/reflect/jvm/internal/impl/k/ad;",
            ")",
            "Ljava/util/List",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/k/v;",
            ">;"
        }
    .end annotation

    .prologue
    const-string v0, "$receiver"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "classType"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "constructor"

    invoke-static {p3, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 210
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/k/a/h;->b:Lkotlin/reflect/jvm/internal/impl/k/a/h;

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/k/a/h;->b:Lkotlin/reflect/jvm/internal/impl/k/a/h;

    invoke-static {v1, p1, p2, p3}, Lkotlin/reflect/jvm/internal/impl/k/a/h;->a(Lkotlin/reflect/jvm/internal/impl/k/a/h;Lkotlin/reflect/jvm/internal/impl/k/a/n;Lkotlin/reflect/jvm/internal/impl/k/v;Lkotlin/reflect/jvm/internal/impl/k/ad;)Ljava/util/List;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/reflect/jvm/internal/impl/k/a/h;->a(Lkotlin/reflect/jvm/internal/impl/k/a/h;Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method
