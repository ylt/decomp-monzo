.class public final Lkotlin/reflect/jvm/internal/impl/k/a/b;
.super Ljava/lang/Object;
.source "IntersectionType.kt"


# direct methods
.method public static final a(Ljava/util/List;)Lkotlin/reflect/jvm/internal/impl/k/ao;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<+",
            "Lkotlin/reflect/jvm/internal/impl/k/ao;",
            ">;)",
            "Lkotlin/reflect/jvm/internal/impl/k/ao;"
        }
    .end annotation

    .prologue
    const/16 v8, 0xa

    const/4 v4, 0x1

    const/4 v3, 0x0

    const-string v0, "types"

    invoke-static {p0, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 23
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 27
    new-instance v5, Lkotlin/d/b/x$a;

    invoke-direct {v5}, Lkotlin/d/b/x$a;-><init>()V

    iput-boolean v3, v5, Lkotlin/d/b/x$a;->a:Z

    .line 28
    new-instance v6, Lkotlin/d/b/x$a;

    invoke-direct {v6}, Lkotlin/d/b/x$a;-><init>()V

    iput-boolean v3, v6, Lkotlin/d/b/x$a;->a:Z

    move-object v0, p0

    .line 29
    check-cast v0, Ljava/lang/Iterable;

    .line 66
    new-instance v1, Ljava/util/ArrayList;

    invoke-static {v0, v8}, Lkotlin/a/m;->a(Ljava/lang/Iterable;I)I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v1, Ljava/util/Collection;

    .line 67
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .line 68
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/ao;

    .line 30
    iget-boolean v2, v6, Lkotlin/d/b/x$a;->a:Z

    if-nez v2, :cond_0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/k/ao;->d()Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    move v2, v4

    :goto_1
    iput-boolean v2, v6, Lkotlin/d/b/x$a;->a:Z

    .line 32
    instance-of v2, v0, Lkotlin/reflect/jvm/internal/impl/k/v;

    if-eqz v2, :cond_2

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/v;

    .line 37
    :goto_2
    invoke-interface {v1, v0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 24
    :pswitch_0
    const-string v1, "Expected some types"

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    .line 25
    :pswitch_1
    invoke-static {p0}, Lkotlin/a/m;->i(Ljava/util/List;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/ao;

    .line 55
    :goto_3
    return-object v0

    :cond_1
    move v2, v3

    .line 30
    goto :goto_1

    .line 33
    :cond_2
    instance-of v2, v0, Lkotlin/reflect/jvm/internal/impl/k/m;

    if-eqz v2, :cond_3

    .line 34
    iput-boolean v4, v5, Lkotlin/d/b/x$a;->a:Z

    .line 35
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/m;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/k/m;->g()Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v0

    goto :goto_2

    :cond_3
    new-instance v0, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {v0}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw v0

    .line 69
    :cond_4
    check-cast v1, Ljava/util/List;

    .line 39
    iget-boolean v0, v6, Lkotlin/d/b/x$a;->a:Z

    if-eqz v0, :cond_5

    .line 40
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Intersection of error types: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/k/l;->c(Ljava/lang/String;)Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v0

    const-string v1, "ErrorUtils.createErrorTy\u2026 of error types: $types\")"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/ao;

    goto :goto_3

    .line 43
    :cond_5
    iget-boolean v0, v5, Lkotlin/d/b/x$a;->a:Z

    if-nez v0, :cond_6

    .line 44
    invoke-static {v1}, Lkotlin/reflect/jvm/internal/impl/k/a/b;->b(Ljava/util/List;)Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/ao;

    goto :goto_3

    .line 47
    :cond_6
    check-cast p0, Ljava/lang/Iterable;

    .line 70
    new-instance v0, Ljava/util/ArrayList;

    invoke-static {p0, v8}, Lkotlin/a/m;->a(Ljava/lang/Iterable;I)I

    move-result v2

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v0, Ljava/util/Collection;

    .line 71
    invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_4
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_7

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 72
    check-cast v2, Lkotlin/reflect/jvm/internal/impl/k/ao;

    .line 47
    check-cast v2, Lkotlin/reflect/jvm/internal/impl/k/r;

    invoke-static {v2}, Lkotlin/reflect/jvm/internal/impl/k/o;->d(Lkotlin/reflect/jvm/internal/impl/k/r;)Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_4

    .line 73
    :cond_7
    check-cast v0, Ljava/util/List;

    .line 55
    invoke-static {v1}, Lkotlin/reflect/jvm/internal/impl/k/a/b;->b(Ljava/util/List;)Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v1

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/k/a/b;->b(Ljava/util/List;)Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v0

    invoke-static {v1, v0}, Lkotlin/reflect/jvm/internal/impl/k/s;->a(Lkotlin/reflect/jvm/internal/impl/k/v;Lkotlin/reflect/jvm/internal/impl/k/v;)Lkotlin/reflect/jvm/internal/impl/k/ao;

    move-result-object v0

    goto :goto_3

    .line 23
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private static final b(Ljava/util/List;)Lkotlin/reflect/jvm/internal/impl/k/v;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<+",
            "Lkotlin/reflect/jvm/internal/impl/k/v;",
            ">;)",
            "Lkotlin/reflect/jvm/internal/impl/k/v;"
        }
    .end annotation

    .prologue
    .line 61
    new-instance v1, Lkotlin/reflect/jvm/internal/impl/k/q;

    check-cast p0, Ljava/util/Collection;

    invoke-direct {v1, p0}, Lkotlin/reflect/jvm/internal/impl/k/q;-><init>(Ljava/util/Collection;)V

    .line 62
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/b/a/h;->a:Lkotlin/reflect/jvm/internal/impl/b/a/h$a;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/b/a/h$a;->a()Lkotlin/reflect/jvm/internal/impl/b/a/h;

    move-result-object v2

    move-object v0, v1

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/ad;

    invoke-static {}, Lkotlin/a/m;->a()Ljava/util/List;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/impl/k/q;->a()Lkotlin/reflect/jvm/internal/impl/h/e/h;

    move-result-object v1

    const-string v5, "constructor.createScopeForKotlinType()"

    invoke-static {v1, v5}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v2, v0, v3, v4, v1}, Lkotlin/reflect/jvm/internal/impl/k/s;->a(Lkotlin/reflect/jvm/internal/impl/b/a/h;Lkotlin/reflect/jvm/internal/impl/k/ad;Ljava/util/List;ZLkotlin/reflect/jvm/internal/impl/h/e/h;)Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v0

    return-object v0
.end method
