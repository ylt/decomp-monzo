.class public final Lkotlin/reflect/jvm/internal/impl/k/o;
.super Ljava/lang/Object;
.source "flexibleTypes.kt"


# direct methods
.method public static final a(Lkotlin/reflect/jvm/internal/impl/k/r;)Z
    .locals 1

    .prologue
    const-string v0, "$receiver"

    invoke-static {p0, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 28
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/k/r;->j()Lkotlin/reflect/jvm/internal/impl/k/ao;

    move-result-object v0

    instance-of v0, v0, Lkotlin/reflect/jvm/internal/impl/k/m;

    return v0
.end method

.method public static final b(Lkotlin/reflect/jvm/internal/impl/k/r;)Lkotlin/reflect/jvm/internal/impl/k/m;
    .locals 2

    .prologue
    const-string v0, "$receiver"

    invoke-static {p0, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 29
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/k/r;->j()Lkotlin/reflect/jvm/internal/impl/k/ao;

    move-result-object v0

    if-nez v0, :cond_0

    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type org.jetbrains.kotlin.types.FlexibleType"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/m;

    return-object v0
.end method

.method public static final c(Lkotlin/reflect/jvm/internal/impl/k/r;)Lkotlin/reflect/jvm/internal/impl/k/v;
    .locals 2

    .prologue
    const-string v0, "$receiver"

    invoke-static {p0, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 69
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/k/r;->j()Lkotlin/reflect/jvm/internal/impl/k/ao;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/ao;

    .line 71
    instance-of v1, v0, Lkotlin/reflect/jvm/internal/impl/k/m;

    if-eqz v1, :cond_0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/m;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/k/m;->g()Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v0

    .line 69
    :goto_0
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/v;

    .line 74
    return-object v0

    .line 72
    :cond_0
    instance-of v1, v0, Lkotlin/reflect/jvm/internal/impl/k/v;

    if-eqz v1, :cond_1

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/v;

    goto :goto_0

    :cond_1
    new-instance v0, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {v0}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw v0
.end method

.method public static final d(Lkotlin/reflect/jvm/internal/impl/k/r;)Lkotlin/reflect/jvm/internal/impl/k/v;
    .locals 2

    .prologue
    const-string v0, "$receiver"

    invoke-static {p0, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 75
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/k/r;->j()Lkotlin/reflect/jvm/internal/impl/k/ao;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/ao;

    .line 77
    instance-of v1, v0, Lkotlin/reflect/jvm/internal/impl/k/m;

    if-eqz v1, :cond_0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/m;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/k/m;->h()Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v0

    .line 75
    :goto_0
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/v;

    .line 80
    return-object v0

    .line 78
    :cond_0
    instance-of v1, v0, Lkotlin/reflect/jvm/internal/impl/k/v;

    if-eqz v1, :cond_1

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/v;

    goto :goto_0

    :cond_1
    new-instance v0, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {v0}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw v0
.end method
