.class public final Lkotlin/reflect/jvm/internal/impl/k/t;
.super Lkotlin/reflect/jvm/internal/impl/k/at;
.source "SpecialTypes.kt"


# instance fields
.field private final a:Lkotlin/reflect/jvm/internal/impl/j/f;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/reflect/jvm/internal/impl/j/f",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/k/r;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lkotlin/reflect/jvm/internal/impl/j/i;Lkotlin/d/a/a;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/reflect/jvm/internal/impl/j/i;",
            "Lkotlin/d/a/a",
            "<+",
            "Lkotlin/reflect/jvm/internal/impl/k/r;",
            ">;)V"
        }
    .end annotation

    .prologue
    const-string v0, "storageManager"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "computation"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 53
    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/k/at;-><init>()V

    .line 54
    invoke-interface {p1, p2}, Lkotlin/reflect/jvm/internal/impl/j/i;->a(Lkotlin/d/a/a;)Lkotlin/reflect/jvm/internal/impl/j/f;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/k/t;->a:Lkotlin/reflect/jvm/internal/impl/j/f;

    return-void
.end method


# virtual methods
.method protected e()Lkotlin/reflect/jvm/internal/impl/k/r;
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/k/t;->a:Lkotlin/reflect/jvm/internal/impl/j/f;

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/j/f;->v_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/r;

    return-object v0
.end method

.method public f()Z
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/k/t;->a:Lkotlin/reflect/jvm/internal/impl/j/f;

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/j/f;->b()Z

    move-result v0

    return v0
.end method
