.class final Lkotlin/reflect/jvm/internal/impl/k/d/d;
.super Ljava/lang/Object;
.source "CapturedTypeApproximation.kt"


# instance fields
.field private final a:Lkotlin/reflect/jvm/internal/impl/b/aq;

.field private final b:Lkotlin/reflect/jvm/internal/impl/k/r;

.field private final c:Lkotlin/reflect/jvm/internal/impl/k/r;


# direct methods
.method public constructor <init>(Lkotlin/reflect/jvm/internal/impl/b/aq;Lkotlin/reflect/jvm/internal/impl/k/r;Lkotlin/reflect/jvm/internal/impl/k/r;)V
    .locals 1

    .prologue
    const-string v0, "typeParameter"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "inProjection"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "outProjection"

    invoke-static {p3, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lkotlin/reflect/jvm/internal/impl/k/d/d;->a:Lkotlin/reflect/jvm/internal/impl/b/aq;

    iput-object p2, p0, Lkotlin/reflect/jvm/internal/impl/k/d/d;->b:Lkotlin/reflect/jvm/internal/impl/k/r;

    iput-object p3, p0, Lkotlin/reflect/jvm/internal/impl/k/d/d;->c:Lkotlin/reflect/jvm/internal/impl/k/r;

    return-void
.end method


# virtual methods
.method public final a()Z
    .locals 3

    .prologue
    .line 42
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/k/a/c;->a:Lkotlin/reflect/jvm/internal/impl/k/a/c;

    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/k/d/d;->b:Lkotlin/reflect/jvm/internal/impl/k/r;

    iget-object v2, p0, Lkotlin/reflect/jvm/internal/impl/k/d/d;->c:Lkotlin/reflect/jvm/internal/impl/k/r;

    invoke-interface {v0, v1, v2}, Lkotlin/reflect/jvm/internal/impl/k/a/c;->a(Lkotlin/reflect/jvm/internal/impl/k/r;Lkotlin/reflect/jvm/internal/impl/k/r;)Z

    move-result v0

    return v0
.end method

.method public final b()Lkotlin/reflect/jvm/internal/impl/b/aq;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/k/d/d;->a:Lkotlin/reflect/jvm/internal/impl/b/aq;

    return-object v0
.end method

.method public final c()Lkotlin/reflect/jvm/internal/impl/k/r;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/k/d/d;->b:Lkotlin/reflect/jvm/internal/impl/k/r;

    return-object v0
.end method

.method public final d()Lkotlin/reflect/jvm/internal/impl/k/r;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/k/d/d;->c:Lkotlin/reflect/jvm/internal/impl/k/r;

    return-object v0
.end method
