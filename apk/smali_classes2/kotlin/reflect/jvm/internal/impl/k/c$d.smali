.class final Lkotlin/reflect/jvm/internal/impl/k/c$d;
.super Lkotlin/d/b/m;
.source "AbstractTypeConstructor.kt"

# interfaces
.implements Lkotlin/d/a/b;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lkotlin/reflect/jvm/internal/impl/k/c;-><init>(Lkotlin/reflect/jvm/internal/impl/j/i;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/d/b/m;",
        "Lkotlin/d/a/b",
        "<",
        "Lkotlin/reflect/jvm/internal/impl/k/c$a;",
        "Lkotlin/n;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lkotlin/reflect/jvm/internal/impl/k/c;


# direct methods
.method constructor <init>(Lkotlin/reflect/jvm/internal/impl/k/c;)V
    .locals 1

    iput-object p1, p0, Lkotlin/reflect/jvm/internal/impl/k/c$d;->a:Lkotlin/reflect/jvm/internal/impl/k/c;

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lkotlin/d/b/m;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 22
    check-cast p1, Lkotlin/reflect/jvm/internal/impl/k/c$a;

    invoke-virtual {p0, p1}, Lkotlin/reflect/jvm/internal/impl/k/c$d;->a(Lkotlin/reflect/jvm/internal/impl/k/c$a;)V

    sget-object v0, Lkotlin/n;->a:Lkotlin/n;

    return-object v0
.end method

.method public final a(Lkotlin/reflect/jvm/internal/impl/k/c$a;)V
    .locals 6

    .prologue
    const/4 v3, 0x0

    const-string v0, "supertypes"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 42
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/k/c$d;->a:Lkotlin/reflect/jvm/internal/impl/k/c;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/k/c;->F_()Lkotlin/reflect/jvm/internal/impl/b/ao;

    move-result-object v4

    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/k/c$d;->a:Lkotlin/reflect/jvm/internal/impl/k/c;

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/ad;

    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/k/c$a;->b()Ljava/util/Collection;

    move-result-object v5

    new-instance v1, Lkotlin/reflect/jvm/internal/impl/k/c$d$a;

    invoke-direct {v1, p0}, Lkotlin/reflect/jvm/internal/impl/k/c$d$a;-><init>(Lkotlin/reflect/jvm/internal/impl/k/c$d;)V

    check-cast v1, Lkotlin/d/a/b;

    new-instance v2, Lkotlin/reflect/jvm/internal/impl/k/c$d$b;

    invoke-direct {v2, p0}, Lkotlin/reflect/jvm/internal/impl/k/c$d$b;-><init>(Lkotlin/reflect/jvm/internal/impl/k/c$d;)V

    check-cast v2, Lkotlin/d/a/b;

    invoke-interface {v4, v0, v5, v1, v2}, Lkotlin/reflect/jvm/internal/impl/b/ao;->a(Lkotlin/reflect/jvm/internal/impl/k/ad;Ljava/util/Collection;Lkotlin/d/a/b;Lkotlin/d/a/b;)Ljava/util/Collection;

    move-result-object v1

    .line 47
    invoke-interface {v1}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 48
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/k/c$d;->a:Lkotlin/reflect/jvm/internal/impl/k/c;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/k/c;->g()Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v0

    if-eqz v0, :cond_1

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/r;

    invoke-static {v0}, Lkotlin/a/m;->a(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    :goto_0
    if-eqz v0, :cond_2

    :goto_1
    check-cast v0, Ljava/util/Collection;

    move-object v1, v0

    .line 51
    :cond_0
    instance-of v0, v1, Ljava/util/List;

    if-nez v0, :cond_4

    move-object v0, v3

    :goto_2
    check-cast v0, Ljava/util/List;

    if-eqz v0, :cond_3

    :goto_3
    invoke-virtual {p1, v0}, Lkotlin/reflect/jvm/internal/impl/k/c$a;->a(Ljava/util/List;)V

    .line 52
    return-void

    :cond_1
    move-object v0, v3

    .line 48
    goto :goto_0

    :cond_2
    invoke-static {}, Lkotlin/a/m;->a()Ljava/util/List;

    move-result-object v0

    goto :goto_1

    :cond_3
    move-object v0, v1

    .line 51
    check-cast v0, Ljava/lang/Iterable;

    invoke-static {v0}, Lkotlin/a/m;->k(Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object v0

    goto :goto_3

    :cond_4
    move-object v0, v1

    goto :goto_2
.end method
