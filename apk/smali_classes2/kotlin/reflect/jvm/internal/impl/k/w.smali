.class final Lkotlin/reflect/jvm/internal/impl/k/w;
.super Lkotlin/reflect/jvm/internal/impl/k/v;
.source "KotlinTypeFactory.kt"


# instance fields
.field private final a:Lkotlin/reflect/jvm/internal/impl/b/a/h;

.field private final b:Lkotlin/reflect/jvm/internal/impl/k/ad;

.field private final c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/k/ah;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Z

.field private final e:Lkotlin/reflect/jvm/internal/impl/h/e/h;


# direct methods
.method public constructor <init>(Lkotlin/reflect/jvm/internal/impl/b/a/h;Lkotlin/reflect/jvm/internal/impl/k/ad;Ljava/util/List;ZLkotlin/reflect/jvm/internal/impl/h/e/h;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/reflect/jvm/internal/impl/b/a/h;",
            "Lkotlin/reflect/jvm/internal/impl/k/ad;",
            "Ljava/util/List",
            "<+",
            "Lkotlin/reflect/jvm/internal/impl/k/ah;",
            ">;Z",
            "Lkotlin/reflect/jvm/internal/impl/h/e/h;",
            ")V"
        }
    .end annotation

    .prologue
    const-string v0, "annotations"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "constructor"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "arguments"

    invoke-static {p3, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "memberScope"

    invoke-static {p5, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 77
    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/k/v;-><init>()V

    iput-object p1, p0, Lkotlin/reflect/jvm/internal/impl/k/w;->a:Lkotlin/reflect/jvm/internal/impl/b/a/h;

    iput-object p2, p0, Lkotlin/reflect/jvm/internal/impl/k/w;->b:Lkotlin/reflect/jvm/internal/impl/k/ad;

    iput-object p3, p0, Lkotlin/reflect/jvm/internal/impl/k/w;->c:Ljava/util/List;

    iput-boolean p4, p0, Lkotlin/reflect/jvm/internal/impl/k/w;->d:Z

    iput-object p5, p0, Lkotlin/reflect/jvm/internal/impl/k/w;->e:Lkotlin/reflect/jvm/internal/impl/h/e/h;

    .line 85
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/k/w;->b()Lkotlin/reflect/jvm/internal/impl/h/e/h;

    move-result-object v0

    instance-of v0, v0, Lkotlin/reflect/jvm/internal/impl/k/l$b;

    if-eqz v0, :cond_0

    .line 86
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "SimpleTypeImpl should not be created for error type: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/k/w;->b()Lkotlin/reflect/jvm/internal/impl/h/e/h;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/k/w;->i()Lkotlin/reflect/jvm/internal/impl/k/ad;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    :cond_0
    return-void
.end method


# virtual methods
.method public a()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/k/ah;",
            ">;"
        }
    .end annotation

    .prologue
    .line 74
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/k/w;->c:Ljava/util/List;

    return-object v0
.end method

.method public synthetic a(Z)Lkotlin/reflect/jvm/internal/impl/k/v;
    .locals 1

    .prologue
    .line 71
    invoke-virtual {p0, p1}, Lkotlin/reflect/jvm/internal/impl/k/w;->c(Z)Lkotlin/reflect/jvm/internal/impl/k/w;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/v;

    return-object v0
.end method

.method public a(Lkotlin/reflect/jvm/internal/impl/b/a/h;)Lkotlin/reflect/jvm/internal/impl/k/w;
    .locals 6

    .prologue
    const-string v0, "newAnnotations"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 78
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/k/w;

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/k/w;->i()Lkotlin/reflect/jvm/internal/impl/k/ad;

    move-result-object v2

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/k/w;->a()Ljava/util/List;

    move-result-object v3

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/k/w;->c()Z

    move-result v4

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/k/w;->b()Lkotlin/reflect/jvm/internal/impl/h/e/h;

    move-result-object v5

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lkotlin/reflect/jvm/internal/impl/k/w;-><init>(Lkotlin/reflect/jvm/internal/impl/b/a/h;Lkotlin/reflect/jvm/internal/impl/k/ad;Ljava/util/List;ZLkotlin/reflect/jvm/internal/impl/h/e/h;)V

    return-object v0
.end method

.method public b()Lkotlin/reflect/jvm/internal/impl/h/e/h;
    .locals 1

    .prologue
    .line 76
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/k/w;->e:Lkotlin/reflect/jvm/internal/impl/h/e/h;

    return-object v0
.end method

.method public synthetic b(Lkotlin/reflect/jvm/internal/impl/b/a/h;)Lkotlin/reflect/jvm/internal/impl/k/ao;
    .locals 1

    .prologue
    .line 71
    invoke-virtual {p0, p1}, Lkotlin/reflect/jvm/internal/impl/k/w;->a(Lkotlin/reflect/jvm/internal/impl/b/a/h;)Lkotlin/reflect/jvm/internal/impl/k/w;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/ao;

    return-object v0
.end method

.method public synthetic b(Z)Lkotlin/reflect/jvm/internal/impl/k/ao;
    .locals 1

    .prologue
    .line 71
    invoke-virtual {p0, p1}, Lkotlin/reflect/jvm/internal/impl/k/w;->c(Z)Lkotlin/reflect/jvm/internal/impl/k/w;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/ao;

    return-object v0
.end method

.method public synthetic c(Lkotlin/reflect/jvm/internal/impl/b/a/h;)Lkotlin/reflect/jvm/internal/impl/k/v;
    .locals 1

    .prologue
    .line 71
    invoke-virtual {p0, p1}, Lkotlin/reflect/jvm/internal/impl/k/w;->a(Lkotlin/reflect/jvm/internal/impl/b/a/h;)Lkotlin/reflect/jvm/internal/impl/k/w;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/v;

    return-object v0
.end method

.method public c(Z)Lkotlin/reflect/jvm/internal/impl/k/w;
    .locals 6

    .prologue
    .line 79
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/k/w;

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/k/w;->w()Lkotlin/reflect/jvm/internal/impl/b/a/h;

    move-result-object v1

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/k/w;->i()Lkotlin/reflect/jvm/internal/impl/k/ad;

    move-result-object v2

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/k/w;->a()Ljava/util/List;

    move-result-object v3

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/k/w;->b()Lkotlin/reflect/jvm/internal/impl/h/e/h;

    move-result-object v5

    move v4, p1

    invoke-direct/range {v0 .. v5}, Lkotlin/reflect/jvm/internal/impl/k/w;-><init>(Lkotlin/reflect/jvm/internal/impl/b/a/h;Lkotlin/reflect/jvm/internal/impl/k/ad;Ljava/util/List;ZLkotlin/reflect/jvm/internal/impl/h/e/h;)V

    return-object v0
.end method

.method public c()Z
    .locals 1

    .prologue
    .line 75
    iget-boolean v0, p0, Lkotlin/reflect/jvm/internal/impl/k/w;->d:Z

    return v0
.end method

.method public d()Z
    .locals 1

    .prologue
    .line 82
    const/4 v0, 0x0

    return v0
.end method

.method public i()Lkotlin/reflect/jvm/internal/impl/k/ad;
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/k/w;->b:Lkotlin/reflect/jvm/internal/impl/k/ad;

    return-object v0
.end method

.method public w()Lkotlin/reflect/jvm/internal/impl/b/a/h;
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/k/w;->a:Lkotlin/reflect/jvm/internal/impl/b/a/h;

    return-object v0
.end method
