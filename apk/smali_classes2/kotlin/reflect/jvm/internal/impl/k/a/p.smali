.class public Lkotlin/reflect/jvm/internal/impl/k/a/p;
.super Ljava/lang/Object;
.source "TypeCheckingProcedure.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lkotlin/reflect/jvm/internal/impl/k/a/p$1;,
        Lkotlin/reflect/jvm/internal/impl/k/a/p$a;
    }
.end annotation


# static fields
.field static final synthetic a:Z


# instance fields
.field private final b:Lkotlin/reflect/jvm/internal/impl/k/a/q;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 30
    const-class v0, Lkotlin/reflect/jvm/internal/impl/k/a/p;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lkotlin/reflect/jvm/internal/impl/k/a/p;->a:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lkotlin/reflect/jvm/internal/impl/k/a/q;)V
    .locals 0

    .prologue
    .line 60
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 61
    iput-object p1, p0, Lkotlin/reflect/jvm/internal/impl/k/a/p;->b:Lkotlin/reflect/jvm/internal/impl/k/a/q;

    .line 62
    return-void
.end method

.method public static a(Lkotlin/reflect/jvm/internal/impl/b/aq;Lkotlin/reflect/jvm/internal/impl/k/ah;)Lkotlin/reflect/jvm/internal/impl/k/a/p$a;
    .locals 8

    .prologue
    const/4 v2, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    if-nez p0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "typeParameter"

    aput-object v3, v2, v4

    const-string v3, "kotlin/reflect/jvm/internal/impl/types/checker/TypeCheckingProcedure"

    aput-object v3, v2, v5

    const-string v3, "getEffectiveProjectionKind"

    aput-object v3, v2, v6

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    if-nez p1, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "typeArgument"

    aput-object v3, v2, v4

    const-string v3, "kotlin/reflect/jvm/internal/impl/types/checker/TypeCheckingProcedure"

    aput-object v3, v2, v5

    const-string v3, "getEffectiveProjectionKind"

    aput-object v3, v2, v6

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 161
    :cond_1
    invoke-interface {p0}, Lkotlin/reflect/jvm/internal/impl/b/aq;->k()Lkotlin/reflect/jvm/internal/impl/k/ap;

    move-result-object v0

    .line 162
    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/k/ah;->b()Lkotlin/reflect/jvm/internal/impl/k/ap;

    move-result-object v1

    .line 165
    sget-object v2, Lkotlin/reflect/jvm/internal/impl/k/ap;->a:Lkotlin/reflect/jvm/internal/impl/k/ap;

    if-ne v1, v2, :cond_4

    .line 172
    :goto_0
    sget-object v2, Lkotlin/reflect/jvm/internal/impl/k/ap;->b:Lkotlin/reflect/jvm/internal/impl/k/ap;

    if-ne v1, v2, :cond_2

    sget-object v2, Lkotlin/reflect/jvm/internal/impl/k/ap;->c:Lkotlin/reflect/jvm/internal/impl/k/ap;

    if-ne v0, v2, :cond_2

    .line 173
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/k/a/p$a;->d:Lkotlin/reflect/jvm/internal/impl/k/a/p$a;

    .line 180
    :goto_1
    return-object v0

    .line 175
    :cond_2
    sget-object v2, Lkotlin/reflect/jvm/internal/impl/k/ap;->c:Lkotlin/reflect/jvm/internal/impl/k/ap;

    if-ne v1, v2, :cond_3

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/k/ap;->b:Lkotlin/reflect/jvm/internal/impl/k/ap;

    if-ne v0, v1, :cond_3

    .line 176
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/k/a/p$a;->d:Lkotlin/reflect/jvm/internal/impl/k/a/p$a;

    goto :goto_1

    .line 180
    :cond_3
    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/k/a/p$a;->a(Lkotlin/reflect/jvm/internal/impl/k/ap;)Lkotlin/reflect/jvm/internal/impl/k/a/p$a;

    move-result-object v0

    goto :goto_1

    :cond_4
    move-object v7, v1

    move-object v1, v0

    move-object v0, v7

    goto :goto_0
.end method

.method public static a(Lkotlin/reflect/jvm/internal/impl/k/r;Lkotlin/reflect/jvm/internal/impl/k/r;)Lkotlin/reflect/jvm/internal/impl/k/r;
    .locals 7

    .prologue
    const/4 v2, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    if-nez p0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "subtype"

    aput-object v3, v2, v4

    const-string v3, "kotlin/reflect/jvm/internal/impl/types/checker/TypeCheckingProcedure"

    aput-object v3, v2, v5

    const-string v3, "findCorrespondingSupertype"

    aput-object v3, v2, v6

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    if-nez p1, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "supertype"

    aput-object v3, v2, v4

    const-string v3, "kotlin/reflect/jvm/internal/impl/types/checker/TypeCheckingProcedure"

    aput-object v3, v2, v5

    const-string v3, "findCorrespondingSupertype"

    aput-object v3, v2, v6

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 36
    :cond_1
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/k/a/o;

    invoke-direct {v0}, Lkotlin/reflect/jvm/internal/impl/k/a/o;-><init>()V

    invoke-static {p0, p1, v0}, Lkotlin/reflect/jvm/internal/impl/k/a/p;->a(Lkotlin/reflect/jvm/internal/impl/k/r;Lkotlin/reflect/jvm/internal/impl/k/r;Lkotlin/reflect/jvm/internal/impl/k/a/q;)Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lkotlin/reflect/jvm/internal/impl/k/r;Lkotlin/reflect/jvm/internal/impl/k/r;Lkotlin/reflect/jvm/internal/impl/k/a/q;)Lkotlin/reflect/jvm/internal/impl/k/r;
    .locals 7

    .prologue
    const/4 v2, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    if-nez p0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "subtype"

    aput-object v3, v2, v4

    const-string v3, "kotlin/reflect/jvm/internal/impl/types/checker/TypeCheckingProcedure"

    aput-object v3, v2, v5

    const-string v3, "findCorrespondingSupertype"

    aput-object v3, v2, v6

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    if-nez p1, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "supertype"

    aput-object v3, v2, v4

    const-string v3, "kotlin/reflect/jvm/internal/impl/types/checker/TypeCheckingProcedure"

    aput-object v3, v2, v5

    const-string v3, "findCorrespondingSupertype"

    aput-object v3, v2, v6

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    if-nez p2, :cond_2

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "typeCheckingProcedureCallbacks"

    aput-object v3, v2, v4

    const-string v3, "kotlin/reflect/jvm/internal/impl/types/checker/TypeCheckingProcedure"

    aput-object v3, v2, v5

    const-string v3, "findCorrespondingSupertype"

    aput-object v3, v2, v6

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 43
    :cond_2
    invoke-static {p0, p1, p2}, Lkotlin/reflect/jvm/internal/impl/k/a/r;->a(Lkotlin/reflect/jvm/internal/impl/k/r;Lkotlin/reflect/jvm/internal/impl/k/r;Lkotlin/reflect/jvm/internal/impl/k/a/q;)Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v0

    return-object v0
.end method

.method private a(Lkotlin/reflect/jvm/internal/impl/k/ah;Lkotlin/reflect/jvm/internal/impl/k/ah;Lkotlin/reflect/jvm/internal/impl/b/aq;)Z
    .locals 7

    .prologue
    const/4 v3, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v0, 0x0

    if-nez p1, :cond_0

    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    new-array v3, v3, [Ljava/lang/Object;

    const-string v4, "subtypeArgumentProjection"

    aput-object v4, v3, v0

    const-string v0, "kotlin/reflect/jvm/internal/impl/types/checker/TypeCheckingProcedure"

    aput-object v0, v3, v5

    const-string v0, "capture"

    aput-object v0, v3, v6

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_0
    if-nez p2, :cond_1

    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    new-array v3, v3, [Ljava/lang/Object;

    const-string v4, "supertypeArgumentProjection"

    aput-object v4, v3, v0

    const-string v0, "kotlin/reflect/jvm/internal/impl/types/checker/TypeCheckingProcedure"

    aput-object v0, v3, v5

    const-string v0, "capture"

    aput-object v0, v3, v6

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_1
    if-nez p3, :cond_2

    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    new-array v3, v3, [Ljava/lang/Object;

    const-string v4, "parameter"

    aput-object v4, v3, v0

    const-string v0, "kotlin/reflect/jvm/internal/impl/types/checker/TypeCheckingProcedure"

    aput-object v0, v3, v5

    const-string v0, "capture"

    aput-object v0, v3, v6

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 272
    :cond_2
    invoke-interface {p3}, Lkotlin/reflect/jvm/internal/impl/b/aq;->k()Lkotlin/reflect/jvm/internal/impl/k/ap;

    move-result-object v1

    sget-object v2, Lkotlin/reflect/jvm/internal/impl/k/ap;->a:Lkotlin/reflect/jvm/internal/impl/k/ap;

    if-eq v1, v2, :cond_4

    .line 281
    :cond_3
    :goto_0
    return v0

    .line 278
    :cond_4
    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/k/ah;->b()Lkotlin/reflect/jvm/internal/impl/k/ap;

    move-result-object v1

    sget-object v2, Lkotlin/reflect/jvm/internal/impl/k/ap;->a:Lkotlin/reflect/jvm/internal/impl/k/ap;

    if-eq v1, v2, :cond_3

    invoke-interface {p2}, Lkotlin/reflect/jvm/internal/impl/k/ah;->b()Lkotlin/reflect/jvm/internal/impl/k/ap;

    move-result-object v1

    sget-object v2, Lkotlin/reflect/jvm/internal/impl/k/ap;->a:Lkotlin/reflect/jvm/internal/impl/k/ap;

    if-ne v1, v2, :cond_3

    .line 279
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/k/a/p;->b:Lkotlin/reflect/jvm/internal/impl/k/a/q;

    invoke-interface {p2}, Lkotlin/reflect/jvm/internal/impl/k/ah;->c()Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v1

    invoke-interface {v0, v1, p1}, Lkotlin/reflect/jvm/internal/impl/k/a/q;->a(Lkotlin/reflect/jvm/internal/impl/k/r;Lkotlin/reflect/jvm/internal/impl/k/ah;)Z

    move-result v0

    goto :goto_0
.end method

.method private static b(Lkotlin/reflect/jvm/internal/impl/b/aq;Lkotlin/reflect/jvm/internal/impl/k/ah;)Lkotlin/reflect/jvm/internal/impl/k/r;
    .locals 7

    .prologue
    const/4 v4, 0x3

    const/4 v6, 0x2

    const/4 v2, 0x1

    const/4 v1, 0x0

    if-nez p0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v3, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    new-array v4, v4, [Ljava/lang/Object;

    const-string v5, "parameter"

    aput-object v5, v4, v1

    const-string v1, "kotlin/reflect/jvm/internal/impl/types/checker/TypeCheckingProcedure"

    aput-object v1, v4, v2

    const-string v1, "getOutType"

    aput-object v1, v4, v6

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    if-nez p1, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v3, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    new-array v4, v4, [Ljava/lang/Object;

    const-string v5, "argument"

    aput-object v5, v4, v1

    const-string v1, "kotlin/reflect/jvm/internal/impl/types/checker/TypeCheckingProcedure"

    aput-object v1, v4, v2

    const-string v1, "getOutType"

    aput-object v1, v4, v6

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 48
    :cond_1
    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/k/ah;->b()Lkotlin/reflect/jvm/internal/impl/k/ap;

    move-result-object v0

    sget-object v3, Lkotlin/reflect/jvm/internal/impl/k/ap;->b:Lkotlin/reflect/jvm/internal/impl/k/ap;

    if-eq v0, v3, :cond_2

    invoke-interface {p0}, Lkotlin/reflect/jvm/internal/impl/b/aq;->k()Lkotlin/reflect/jvm/internal/impl/k/ap;

    move-result-object v0

    sget-object v3, Lkotlin/reflect/jvm/internal/impl/k/ap;->b:Lkotlin/reflect/jvm/internal/impl/k/ap;

    if-ne v0, v3, :cond_3

    :cond_2
    move v0, v2

    .line 49
    :goto_0
    if-eqz v0, :cond_4

    invoke-static {p0}, Lkotlin/reflect/jvm/internal/impl/h/c/a;->d(Lkotlin/reflect/jvm/internal/impl/b/m;)Lkotlin/reflect/jvm/internal/impl/a/l;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/a/l;->y()Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v0

    :goto_1
    if-nez v0, :cond_5

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v3, "@NotNull method %s.%s must not return null"

    new-array v4, v6, [Ljava/lang/Object;

    const-string v5, "kotlin/reflect/jvm/internal/impl/types/checker/TypeCheckingProcedure"

    aput-object v5, v4, v1

    const-string v1, "getOutType"

    aput-object v1, v4, v2

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_3
    move v0, v1

    .line 48
    goto :goto_0

    .line 49
    :cond_4
    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/k/ah;->c()Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v0

    goto :goto_1

    :cond_5
    return-object v0
.end method

.method private static c(Lkotlin/reflect/jvm/internal/impl/b/aq;Lkotlin/reflect/jvm/internal/impl/k/ah;)Lkotlin/reflect/jvm/internal/impl/k/r;
    .locals 7

    .prologue
    const/4 v4, 0x3

    const/4 v6, 0x2

    const/4 v2, 0x1

    const/4 v1, 0x0

    if-nez p0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v3, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    new-array v4, v4, [Ljava/lang/Object;

    const-string v5, "parameter"

    aput-object v5, v4, v1

    const-string v1, "kotlin/reflect/jvm/internal/impl/types/checker/TypeCheckingProcedure"

    aput-object v1, v4, v2

    const-string v1, "getInType"

    aput-object v1, v4, v6

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    if-nez p1, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v3, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    new-array v4, v4, [Ljava/lang/Object;

    const-string v5, "argument"

    aput-object v5, v4, v1

    const-string v1, "kotlin/reflect/jvm/internal/impl/types/checker/TypeCheckingProcedure"

    aput-object v1, v4, v2

    const-string v1, "getInType"

    aput-object v1, v4, v6

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 54
    :cond_1
    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/k/ah;->b()Lkotlin/reflect/jvm/internal/impl/k/ap;

    move-result-object v0

    sget-object v3, Lkotlin/reflect/jvm/internal/impl/k/ap;->c:Lkotlin/reflect/jvm/internal/impl/k/ap;

    if-eq v0, v3, :cond_2

    invoke-interface {p0}, Lkotlin/reflect/jvm/internal/impl/b/aq;->k()Lkotlin/reflect/jvm/internal/impl/k/ap;

    move-result-object v0

    sget-object v3, Lkotlin/reflect/jvm/internal/impl/k/ap;->c:Lkotlin/reflect/jvm/internal/impl/k/ap;

    if-ne v0, v3, :cond_3

    :cond_2
    move v0, v2

    .line 55
    :goto_0
    if-eqz v0, :cond_4

    invoke-static {p0}, Lkotlin/reflect/jvm/internal/impl/h/c/a;->d(Lkotlin/reflect/jvm/internal/impl/b/m;)Lkotlin/reflect/jvm/internal/impl/a/l;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/a/l;->v()Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v0

    :goto_1
    if-nez v0, :cond_5

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v3, "@NotNull method %s.%s must not return null"

    new-array v4, v6, [Ljava/lang/Object;

    const-string v5, "kotlin/reflect/jvm/internal/impl/types/checker/TypeCheckingProcedure"

    aput-object v5, v4, v1

    const-string v1, "getInType"

    aput-object v1, v4, v2

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_3
    move v0, v1

    .line 54
    goto :goto_0

    .line 55
    :cond_4
    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/k/ah;->c()Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v0

    goto :goto_1

    :cond_5
    return-object v0
.end method

.method private e(Lkotlin/reflect/jvm/internal/impl/k/r;Lkotlin/reflect/jvm/internal/impl/k/r;)Z
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 197
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/k/r;->d()Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {p2}, Lkotlin/reflect/jvm/internal/impl/k/r;->d()Z

    move-result v2

    if-eqz v2, :cond_2

    :cond_0
    move v0, v1

    .line 218
    :cond_1
    :goto_0
    return v0

    .line 201
    :cond_2
    invoke-virtual {p2}, Lkotlin/reflect/jvm/internal/impl/k/r;->c()Z

    move-result v2

    if-nez v2, :cond_3

    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/k/r;->c()Z

    move-result v2

    if-nez v2, :cond_1

    .line 205
    :cond_3
    invoke-static {p1}, Lkotlin/reflect/jvm/internal/impl/a/l;->g(Lkotlin/reflect/jvm/internal/impl/k/r;)Z

    move-result v2

    if-eqz v2, :cond_4

    move v0, v1

    .line 206
    goto :goto_0

    .line 209
    :cond_4
    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/k/a/p;->b:Lkotlin/reflect/jvm/internal/impl/k/a/q;

    invoke-static {p1, p2, v1}, Lkotlin/reflect/jvm/internal/impl/k/a/p;->a(Lkotlin/reflect/jvm/internal/impl/k/r;Lkotlin/reflect/jvm/internal/impl/k/r;Lkotlin/reflect/jvm/internal/impl/k/a/q;)Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v1

    .line 210
    if-nez v1, :cond_5

    .line 211
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/k/a/p;->b:Lkotlin/reflect/jvm/internal/impl/k/a/q;

    invoke-interface {v0, p1, p2}, Lkotlin/reflect/jvm/internal/impl/k/a/q;->a(Lkotlin/reflect/jvm/internal/impl/k/r;Lkotlin/reflect/jvm/internal/impl/k/r;)Z

    move-result v0

    goto :goto_0

    .line 214
    :cond_5
    invoke-virtual {p2}, Lkotlin/reflect/jvm/internal/impl/k/r;->c()Z

    move-result v2

    if-nez v2, :cond_6

    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/impl/k/r;->c()Z

    move-result v2

    if-nez v2, :cond_1

    .line 218
    :cond_6
    invoke-direct {p0, v1, p2}, Lkotlin/reflect/jvm/internal/impl/k/a/p;->f(Lkotlin/reflect/jvm/internal/impl/k/r;Lkotlin/reflect/jvm/internal/impl/k/r;)Z

    move-result v0

    goto :goto_0
.end method

.method private f(Lkotlin/reflect/jvm/internal/impl/k/r;Lkotlin/reflect/jvm/internal/impl/k/r;)Z
    .locals 12

    .prologue
    const/4 v2, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "subtype"

    aput-object v3, v2, v4

    const-string v3, "kotlin/reflect/jvm/internal/impl/types/checker/TypeCheckingProcedure"

    aput-object v3, v2, v5

    const-string v3, "checkSubtypeForTheSameConstructor"

    aput-object v3, v2, v6

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    if-nez p2, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "supertype"

    aput-object v3, v2, v4

    const-string v3, "kotlin/reflect/jvm/internal/impl/types/checker/TypeCheckingProcedure"

    aput-object v3, v2, v5

    const-string v3, "checkSubtypeForTheSameConstructor"

    aput-object v3, v2, v6

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 222
    :cond_1
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/k/r;->i()Lkotlin/reflect/jvm/internal/impl/k/ad;

    move-result-object v0

    .line 227
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/k/r;->a()Ljava/util/List;

    move-result-object v7

    .line 228
    invoke-virtual {p2}, Lkotlin/reflect/jvm/internal/impl/k/r;->a()Ljava/util/List;

    move-result-object v8

    .line 229
    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v1

    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v2

    if-eq v1, v2, :cond_3

    .line 263
    :cond_2
    :goto_0
    return v4

    .line 231
    :cond_3
    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/k/ad;->b()Ljava/util/List;

    move-result-object v9

    move v3, v4

    .line 232
    :goto_1
    invoke-interface {v9}, Ljava/util/List;->size()I

    move-result v0

    if-ge v3, v0, :cond_a

    .line 233
    invoke-interface {v9, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/aq;

    .line 235
    invoke-interface {v8, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lkotlin/reflect/jvm/internal/impl/k/ah;

    .line 236
    invoke-interface {v7, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lkotlin/reflect/jvm/internal/impl/k/ah;

    .line 238
    invoke-interface {v1}, Lkotlin/reflect/jvm/internal/impl/k/ah;->a()Z

    move-result v6

    if-eqz v6, :cond_5

    .line 232
    :cond_4
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_1

    .line 240
    :cond_5
    invoke-direct {p0, v2, v1, v0}, Lkotlin/reflect/jvm/internal/impl/k/a/p;->a(Lkotlin/reflect/jvm/internal/impl/k/ah;Lkotlin/reflect/jvm/internal/impl/k/ah;Lkotlin/reflect/jvm/internal/impl/b/aq;)Z

    move-result v6

    if-nez v6, :cond_4

    .line 242
    invoke-interface {v2}, Lkotlin/reflect/jvm/internal/impl/k/ah;->c()Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v6

    invoke-virtual {v6}, Lkotlin/reflect/jvm/internal/impl/k/r;->d()Z

    move-result v6

    if-nez v6, :cond_6

    invoke-interface {v1}, Lkotlin/reflect/jvm/internal/impl/k/ah;->c()Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v6

    invoke-virtual {v6}, Lkotlin/reflect/jvm/internal/impl/k/r;->d()Z

    move-result v6

    if-eqz v6, :cond_7

    :cond_6
    move v6, v5

    .line 243
    :goto_2
    if-nez v6, :cond_8

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/b/aq;->k()Lkotlin/reflect/jvm/internal/impl/k/ap;

    move-result-object v6

    sget-object v10, Lkotlin/reflect/jvm/internal/impl/k/ap;->a:Lkotlin/reflect/jvm/internal/impl/k/ap;

    if-ne v6, v10, :cond_8

    invoke-interface {v2}, Lkotlin/reflect/jvm/internal/impl/k/ah;->b()Lkotlin/reflect/jvm/internal/impl/k/ap;

    move-result-object v6

    sget-object v10, Lkotlin/reflect/jvm/internal/impl/k/ap;->a:Lkotlin/reflect/jvm/internal/impl/k/ap;

    if-ne v6, v10, :cond_8

    invoke-interface {v1}, Lkotlin/reflect/jvm/internal/impl/k/ah;->b()Lkotlin/reflect/jvm/internal/impl/k/ap;

    move-result-object v6

    sget-object v10, Lkotlin/reflect/jvm/internal/impl/k/ap;->a:Lkotlin/reflect/jvm/internal/impl/k/ap;

    if-ne v6, v10, :cond_8

    .line 245
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/k/a/p;->b:Lkotlin/reflect/jvm/internal/impl/k/a/q;

    invoke-interface {v2}, Lkotlin/reflect/jvm/internal/impl/k/ah;->c()Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v2

    invoke-interface {v1}, Lkotlin/reflect/jvm/internal/impl/k/ah;->c()Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v1

    invoke-interface {v0, v2, v1, p0}, Lkotlin/reflect/jvm/internal/impl/k/a/q;->a(Lkotlin/reflect/jvm/internal/impl/k/r;Lkotlin/reflect/jvm/internal/impl/k/r;Lkotlin/reflect/jvm/internal/impl/k/a/p;)Z

    move-result v0

    if-nez v0, :cond_4

    goto :goto_0

    :cond_7
    move v6, v4

    .line 242
    goto :goto_2

    .line 249
    :cond_8
    invoke-static {v0, v1}, Lkotlin/reflect/jvm/internal/impl/k/a/p;->b(Lkotlin/reflect/jvm/internal/impl/b/aq;Lkotlin/reflect/jvm/internal/impl/k/ah;)Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v6

    .line 250
    invoke-static {v0, v2}, Lkotlin/reflect/jvm/internal/impl/k/a/p;->b(Lkotlin/reflect/jvm/internal/impl/b/aq;Lkotlin/reflect/jvm/internal/impl/k/ah;)Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v10

    .line 251
    iget-object v11, p0, Lkotlin/reflect/jvm/internal/impl/k/a/p;->b:Lkotlin/reflect/jvm/internal/impl/k/a/q;

    invoke-interface {v11, v10, v6, p0}, Lkotlin/reflect/jvm/internal/impl/k/a/q;->b(Lkotlin/reflect/jvm/internal/impl/k/r;Lkotlin/reflect/jvm/internal/impl/k/r;Lkotlin/reflect/jvm/internal/impl/k/a/p;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 253
    invoke-static {v0, v1}, Lkotlin/reflect/jvm/internal/impl/k/a/p;->c(Lkotlin/reflect/jvm/internal/impl/b/aq;Lkotlin/reflect/jvm/internal/impl/k/ah;)Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v6

    .line 254
    invoke-static {v0, v2}, Lkotlin/reflect/jvm/internal/impl/k/a/p;->c(Lkotlin/reflect/jvm/internal/impl/b/aq;Lkotlin/reflect/jvm/internal/impl/k/ah;)Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v0

    .line 256
    invoke-interface {v1}, Lkotlin/reflect/jvm/internal/impl/k/ah;->b()Lkotlin/reflect/jvm/internal/impl/k/ap;

    move-result-object v1

    sget-object v2, Lkotlin/reflect/jvm/internal/impl/k/ap;->c:Lkotlin/reflect/jvm/internal/impl/k/ap;

    if-eq v1, v2, :cond_9

    .line 257
    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/k/a/p;->b:Lkotlin/reflect/jvm/internal/impl/k/a/q;

    invoke-interface {v1, v6, v0, p0}, Lkotlin/reflect/jvm/internal/impl/k/a/q;->b(Lkotlin/reflect/jvm/internal/impl/k/r;Lkotlin/reflect/jvm/internal/impl/k/r;Lkotlin/reflect/jvm/internal/impl/k/a/p;)Z

    move-result v0

    if-nez v0, :cond_4

    goto/16 :goto_0

    .line 260
    :cond_9
    sget-boolean v0, Lkotlin/reflect/jvm/internal/impl/k/a/p;->a:Z

    if-nez v0, :cond_4

    invoke-static {v6}, Lkotlin/reflect/jvm/internal/impl/a/l;->f(Lkotlin/reflect/jvm/internal/impl/k/r;)Z

    move-result v0

    if-nez v0, :cond_4

    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "In component must be Nothing for out-projection"

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    :cond_a
    move v4, v5

    .line 263
    goto/16 :goto_0
.end method


# virtual methods
.method public b(Lkotlin/reflect/jvm/internal/impl/k/r;Lkotlin/reflect/jvm/internal/impl/k/r;)Z
    .locals 12

    .prologue
    const/4 v2, 0x3

    const/4 v6, 0x2

    const/4 v4, 0x1

    const/4 v5, 0x0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "type1"

    aput-object v3, v2, v5

    const-string v3, "kotlin/reflect/jvm/internal/impl/types/checker/TypeCheckingProcedure"

    aput-object v3, v2, v4

    const-string v3, "equalTypes"

    aput-object v3, v2, v6

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    if-nez p2, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "type2"

    aput-object v3, v2, v5

    const-string v3, "kotlin/reflect/jvm/internal/impl/types/checker/TypeCheckingProcedure"

    aput-object v3, v2, v4

    const-string v3, "equalTypes"

    aput-object v3, v2, v6

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 65
    :cond_1
    if-ne p1, p2, :cond_3

    move v5, v4

    .line 118
    :cond_2
    :goto_0
    return v5

    .line 66
    :cond_3
    invoke-static {p1}, Lkotlin/reflect/jvm/internal/impl/k/o;->a(Lkotlin/reflect/jvm/internal/impl/k/r;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 67
    invoke-static {p2}, Lkotlin/reflect/jvm/internal/impl/k/o;->a(Lkotlin/reflect/jvm/internal/impl/k/r;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 68
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/k/r;->d()Z

    move-result v0

    if-nez v0, :cond_4

    invoke-virtual {p2}, Lkotlin/reflect/jvm/internal/impl/k/r;->d()Z

    move-result v0

    if-nez v0, :cond_4

    invoke-virtual {p0, p1, p2}, Lkotlin/reflect/jvm/internal/impl/k/a/p;->d(Lkotlin/reflect/jvm/internal/impl/k/r;Lkotlin/reflect/jvm/internal/impl/k/r;)Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-virtual {p0, p2, p1}, Lkotlin/reflect/jvm/internal/impl/k/a/p;->d(Lkotlin/reflect/jvm/internal/impl/k/r;Lkotlin/reflect/jvm/internal/impl/k/r;)Z

    move-result v0

    if-eqz v0, :cond_4

    move v0, v4

    :goto_1
    move v5, v0

    goto :goto_0

    :cond_4
    move v0, v5

    goto :goto_1

    .line 70
    :cond_5
    invoke-virtual {p0, p2, p1}, Lkotlin/reflect/jvm/internal/impl/k/a/p;->c(Lkotlin/reflect/jvm/internal/impl/k/r;Lkotlin/reflect/jvm/internal/impl/k/r;)Z

    move-result v5

    goto :goto_0

    .line 72
    :cond_6
    invoke-static {p2}, Lkotlin/reflect/jvm/internal/impl/k/o;->a(Lkotlin/reflect/jvm/internal/impl/k/r;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 73
    invoke-virtual {p0, p1, p2}, Lkotlin/reflect/jvm/internal/impl/k/a/p;->c(Lkotlin/reflect/jvm/internal/impl/k/r;Lkotlin/reflect/jvm/internal/impl/k/r;)Z

    move-result v5

    goto :goto_0

    .line 76
    :cond_7
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/k/r;->c()Z

    move-result v0

    invoke-virtual {p2}, Lkotlin/reflect/jvm/internal/impl/k/r;->c()Z

    move-result v1

    if-ne v0, v1, :cond_2

    .line 80
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/k/r;->c()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 82
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/k/a/p;->b:Lkotlin/reflect/jvm/internal/impl/k/a/q;

    invoke-static {p1}, Lkotlin/reflect/jvm/internal/impl/k/an;->c(Lkotlin/reflect/jvm/internal/impl/k/r;)Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v1

    invoke-static {p2}, Lkotlin/reflect/jvm/internal/impl/k/an;->c(Lkotlin/reflect/jvm/internal/impl/k/r;)Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v2

    invoke-interface {v0, v1, v2, p0}, Lkotlin/reflect/jvm/internal/impl/k/a/q;->a(Lkotlin/reflect/jvm/internal/impl/k/r;Lkotlin/reflect/jvm/internal/impl/k/r;Lkotlin/reflect/jvm/internal/impl/k/a/p;)Z

    move-result v5

    goto :goto_0

    .line 85
    :cond_8
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/k/r;->i()Lkotlin/reflect/jvm/internal/impl/k/ad;

    move-result-object v7

    .line 86
    invoke-virtual {p2}, Lkotlin/reflect/jvm/internal/impl/k/r;->i()Lkotlin/reflect/jvm/internal/impl/k/ad;

    move-result-object v8

    .line 88
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/k/a/p;->b:Lkotlin/reflect/jvm/internal/impl/k/a/q;

    invoke-interface {v0, v7, v8}, Lkotlin/reflect/jvm/internal/impl/k/a/q;->a(Lkotlin/reflect/jvm/internal/impl/k/ad;Lkotlin/reflect/jvm/internal/impl/k/ad;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 92
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/k/r;->a()Ljava/util/List;

    move-result-object v9

    .line 93
    invoke-virtual {p2}, Lkotlin/reflect/jvm/internal/impl/k/r;->a()Ljava/util/List;

    move-result-object v10

    .line 94
    invoke-interface {v9}, Ljava/util/List;->size()I

    move-result v0

    invoke-interface {v10}, Ljava/util/List;->size()I

    move-result v1

    if-ne v0, v1, :cond_2

    move v6, v5

    .line 98
    :goto_2
    invoke-interface {v9}, Ljava/util/List;->size()I

    move-result v0

    if-ge v6, v0, :cond_b

    .line 99
    invoke-interface {v9, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/ah;

    .line 100
    invoke-interface {v10, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lkotlin/reflect/jvm/internal/impl/k/ah;

    .line 101
    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/k/ah;->a()Z

    move-result v2

    if-eqz v2, :cond_a

    invoke-interface {v1}, Lkotlin/reflect/jvm/internal/impl/k/ah;->a()Z

    move-result v2

    if-eqz v2, :cond_a

    .line 98
    :cond_9
    add-int/lit8 v0, v6, 0x1

    move v6, v0

    goto :goto_2

    .line 104
    :cond_a
    invoke-interface {v7}, Lkotlin/reflect/jvm/internal/impl/k/ad;->b()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lkotlin/reflect/jvm/internal/impl/b/aq;

    .line 105
    invoke-interface {v8}, Lkotlin/reflect/jvm/internal/impl/k/ad;->b()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lkotlin/reflect/jvm/internal/impl/b/aq;

    .line 107
    invoke-direct {p0, v0, v1, v2}, Lkotlin/reflect/jvm/internal/impl/k/a/p;->a(Lkotlin/reflect/jvm/internal/impl/k/ah;Lkotlin/reflect/jvm/internal/impl/k/ah;Lkotlin/reflect/jvm/internal/impl/b/aq;)Z

    move-result v11

    if-nez v11, :cond_9

    .line 110
    invoke-static {v2, v0}, Lkotlin/reflect/jvm/internal/impl/k/a/p;->a(Lkotlin/reflect/jvm/internal/impl/b/aq;Lkotlin/reflect/jvm/internal/impl/k/ah;)Lkotlin/reflect/jvm/internal/impl/k/a/p$a;

    move-result-object v2

    invoke-static {v3, v1}, Lkotlin/reflect/jvm/internal/impl/k/a/p;->a(Lkotlin/reflect/jvm/internal/impl/b/aq;Lkotlin/reflect/jvm/internal/impl/k/ah;)Lkotlin/reflect/jvm/internal/impl/k/a/p$a;

    move-result-object v3

    if-ne v2, v3, :cond_2

    .line 114
    iget-object v2, p0, Lkotlin/reflect/jvm/internal/impl/k/a/p;->b:Lkotlin/reflect/jvm/internal/impl/k/a/q;

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/k/ah;->c()Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v0

    invoke-interface {v1}, Lkotlin/reflect/jvm/internal/impl/k/ah;->c()Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v1

    invoke-interface {v2, v0, v1, p0}, Lkotlin/reflect/jvm/internal/impl/k/a/q;->a(Lkotlin/reflect/jvm/internal/impl/k/r;Lkotlin/reflect/jvm/internal/impl/k/r;Lkotlin/reflect/jvm/internal/impl/k/a/p;)Z

    move-result v0

    if-nez v0, :cond_9

    goto/16 :goto_0

    :cond_b
    move v5, v4

    .line 118
    goto/16 :goto_0
.end method

.method protected c(Lkotlin/reflect/jvm/internal/impl/k/r;Lkotlin/reflect/jvm/internal/impl/k/r;)Z
    .locals 3

    .prologue
    .line 123
    sget-boolean v0, Lkotlin/reflect/jvm/internal/impl/k/a/p;->a:Z

    if-nez v0, :cond_0

    invoke-static {p1}, Lkotlin/reflect/jvm/internal/impl/k/o;->a(Lkotlin/reflect/jvm/internal/impl/k/r;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Only inflexible types are allowed here: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 124
    :cond_0
    invoke-static {p2}, Lkotlin/reflect/jvm/internal/impl/k/o;->b(Lkotlin/reflect/jvm/internal/impl/k/r;)Lkotlin/reflect/jvm/internal/impl/k/m;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/k/m;->g()Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v0

    invoke-virtual {p0, v0, p1}, Lkotlin/reflect/jvm/internal/impl/k/a/p;->d(Lkotlin/reflect/jvm/internal/impl/k/r;Lkotlin/reflect/jvm/internal/impl/k/r;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-static {p2}, Lkotlin/reflect/jvm/internal/impl/k/o;->b(Lkotlin/reflect/jvm/internal/impl/k/r;)Lkotlin/reflect/jvm/internal/impl/k/m;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/k/m;->h()Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lkotlin/reflect/jvm/internal/impl/k/a/p;->d(Lkotlin/reflect/jvm/internal/impl/k/r;Lkotlin/reflect/jvm/internal/impl/k/r;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public d(Lkotlin/reflect/jvm/internal/impl/k/r;Lkotlin/reflect/jvm/internal/impl/k/r;)Z
    .locals 7

    .prologue
    const/4 v4, 0x3

    const/4 v6, 0x2

    const/4 v1, 0x1

    const/4 v0, 0x0

    if-nez p1, :cond_0

    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    new-array v4, v4, [Ljava/lang/Object;

    const-string v5, "subtype"

    aput-object v5, v4, v0

    const-string v0, "kotlin/reflect/jvm/internal/impl/types/checker/TypeCheckingProcedure"

    aput-object v0, v4, v1

    const-string v0, "isSubtypeOf"

    aput-object v0, v4, v6

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_0
    if-nez p2, :cond_1

    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    new-array v4, v4, [Ljava/lang/Object;

    const-string v5, "supertype"

    aput-object v5, v4, v0

    const-string v0, "kotlin/reflect/jvm/internal/impl/types/checker/TypeCheckingProcedure"

    aput-object v0, v4, v1

    const-string v0, "isSubtypeOf"

    aput-object v0, v4, v6

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 184
    :cond_1
    invoke-static {p1, p2}, Lkotlin/reflect/jvm/internal/impl/k/ac;->a(Lkotlin/reflect/jvm/internal/impl/k/r;Lkotlin/reflect/jvm/internal/impl/k/r;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 185
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/k/r;->c()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {p2}, Lkotlin/reflect/jvm/internal/impl/k/r;->c()Z

    move-result v2

    if-eqz v2, :cond_3

    :cond_2
    move v0, v1

    .line 193
    :cond_3
    :goto_0
    return v0

    .line 187
    :cond_4
    invoke-static {p1}, Lkotlin/reflect/jvm/internal/impl/k/ac;->c(Lkotlin/reflect/jvm/internal/impl/k/r;)Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v0

    .line 188
    invoke-static {p2}, Lkotlin/reflect/jvm/internal/impl/k/ac;->d(Lkotlin/reflect/jvm/internal/impl/k/r;)Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v1

    .line 189
    if-ne v0, p1, :cond_5

    if-eq v1, p2, :cond_6

    .line 191
    :cond_5
    invoke-virtual {p0, v0, v1}, Lkotlin/reflect/jvm/internal/impl/k/a/p;->d(Lkotlin/reflect/jvm/internal/impl/k/r;Lkotlin/reflect/jvm/internal/impl/k/r;)Z

    move-result v0

    goto :goto_0

    .line 193
    :cond_6
    invoke-direct {p0, p1, p2}, Lkotlin/reflect/jvm/internal/impl/k/a/p;->e(Lkotlin/reflect/jvm/internal/impl/k/r;Lkotlin/reflect/jvm/internal/impl/k/r;)Z

    move-result v0

    goto :goto_0
.end method
