.class Lkotlin/reflect/jvm/internal/impl/k/l$c;
.super Lkotlin/reflect/jvm/internal/impl/k/v;
.source "ErrorUtils.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lkotlin/reflect/jvm/internal/impl/k/l;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "c"
.end annotation


# instance fields
.field private final a:Lkotlin/reflect/jvm/internal/impl/k/ad;

.field private final b:Lkotlin/reflect/jvm/internal/impl/h/e/h;

.field private final c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/k/ah;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Z


# direct methods
.method private constructor <init>(Lkotlin/reflect/jvm/internal/impl/k/ad;Lkotlin/reflect/jvm/internal/impl/h/e/h;)V
    .locals 7

    .prologue
    const/4 v2, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "constructor"

    aput-object v3, v2, v4

    const-string v3, "kotlin/reflect/jvm/internal/impl/types/ErrorUtils$ErrorTypeImpl"

    aput-object v3, v2, v5

    const-string v3, "<init>"

    aput-object v3, v2, v6

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    if-nez p2, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "memberScope"

    aput-object v3, v2, v4

    const-string v3, "kotlin/reflect/jvm/internal/impl/types/ErrorUtils$ErrorTypeImpl"

    aput-object v3, v2, v5

    const-string v3, "<init>"

    aput-object v3, v2, v6

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 521
    :cond_1
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    invoke-direct {p0, p1, p2, v0, v4}, Lkotlin/reflect/jvm/internal/impl/k/l$c;-><init>(Lkotlin/reflect/jvm/internal/impl/k/ad;Lkotlin/reflect/jvm/internal/impl/h/e/h;Ljava/util/List;Z)V

    .line 522
    return-void
.end method

.method private constructor <init>(Lkotlin/reflect/jvm/internal/impl/k/ad;Lkotlin/reflect/jvm/internal/impl/h/e/h;Ljava/util/List;Z)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/reflect/jvm/internal/impl/k/ad;",
            "Lkotlin/reflect/jvm/internal/impl/h/e/h;",
            "Ljava/util/List",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/k/ah;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "constructor"

    aput-object v3, v2, v4

    const-string v3, "kotlin/reflect/jvm/internal/impl/types/ErrorUtils$ErrorTypeImpl"

    aput-object v3, v2, v5

    const-string v3, "<init>"

    aput-object v3, v2, v6

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    if-nez p2, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "memberScope"

    aput-object v3, v2, v4

    const-string v3, "kotlin/reflect/jvm/internal/impl/types/ErrorUtils$ErrorTypeImpl"

    aput-object v3, v2, v5

    const-string v3, "<init>"

    aput-object v3, v2, v6

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    if-nez p3, :cond_2

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "arguments"

    aput-object v3, v2, v4

    const-string v3, "kotlin/reflect/jvm/internal/impl/types/ErrorUtils$ErrorTypeImpl"

    aput-object v3, v2, v5

    const-string v3, "<init>"

    aput-object v3, v2, v6

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 513
    :cond_2
    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/k/v;-><init>()V

    .line 514
    iput-object p1, p0, Lkotlin/reflect/jvm/internal/impl/k/l$c;->a:Lkotlin/reflect/jvm/internal/impl/k/ad;

    .line 515
    iput-object p2, p0, Lkotlin/reflect/jvm/internal/impl/k/l$c;->b:Lkotlin/reflect/jvm/internal/impl/h/e/h;

    .line 516
    iput-object p3, p0, Lkotlin/reflect/jvm/internal/impl/k/l$c;->c:Ljava/util/List;

    .line 517
    iput-boolean p4, p0, Lkotlin/reflect/jvm/internal/impl/k/l$c;->d:Z

    .line 518
    return-void
.end method

.method synthetic constructor <init>(Lkotlin/reflect/jvm/internal/impl/k/ad;Lkotlin/reflect/jvm/internal/impl/h/e/h;Ljava/util/List;ZLkotlin/reflect/jvm/internal/impl/k/l$1;)V
    .locals 0

    .prologue
    .line 502
    invoke-direct {p0, p1, p2, p3, p4}, Lkotlin/reflect/jvm/internal/impl/k/l$c;-><init>(Lkotlin/reflect/jvm/internal/impl/k/ad;Lkotlin/reflect/jvm/internal/impl/h/e/h;Ljava/util/List;Z)V

    return-void
.end method

.method synthetic constructor <init>(Lkotlin/reflect/jvm/internal/impl/k/ad;Lkotlin/reflect/jvm/internal/impl/h/e/h;Lkotlin/reflect/jvm/internal/impl/k/l$1;)V
    .locals 0

    .prologue
    .line 502
    invoke-direct {p0, p1, p2}, Lkotlin/reflect/jvm/internal/impl/k/l$c;-><init>(Lkotlin/reflect/jvm/internal/impl/k/ad;Lkotlin/reflect/jvm/internal/impl/h/e/h;)V

    return-void
.end method


# virtual methods
.method public a()Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/k/ah;",
            ">;"
        }
    .end annotation

    .prologue
    .line 533
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/k/l$c;->c:Ljava/util/List;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "@NotNull method %s.%s must not return null"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "kotlin/reflect/jvm/internal/impl/types/ErrorUtils$ErrorTypeImpl"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "getArguments"

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    return-object v0
.end method

.method public a(Z)Lkotlin/reflect/jvm/internal/impl/k/v;
    .locals 5

    .prologue
    .line 572
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/k/l$c;

    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/k/l$c;->a:Lkotlin/reflect/jvm/internal/impl/k/ad;

    iget-object v2, p0, Lkotlin/reflect/jvm/internal/impl/k/l$c;->b:Lkotlin/reflect/jvm/internal/impl/h/e/h;

    iget-object v3, p0, Lkotlin/reflect/jvm/internal/impl/k/l$c;->c:Ljava/util/List;

    invoke-direct {v0, v1, v2, v3, p1}, Lkotlin/reflect/jvm/internal/impl/k/l$c;-><init>(Lkotlin/reflect/jvm/internal/impl/k/ad;Lkotlin/reflect/jvm/internal/impl/h/e/h;Ljava/util/List;Z)V

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "@NotNull method %s.%s must not return null"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "kotlin/reflect/jvm/internal/impl/types/ErrorUtils$ErrorTypeImpl"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "makeNullableAsSpecified"

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    return-object v0
.end method

.method public b()Lkotlin/reflect/jvm/internal/impl/h/e/h;
    .locals 5

    .prologue
    .line 544
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/k/l$c;->b:Lkotlin/reflect/jvm/internal/impl/h/e/h;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "@NotNull method %s.%s must not return null"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "kotlin/reflect/jvm/internal/impl/types/ErrorUtils$ErrorTypeImpl"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "getMemberScope"

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    return-object v0
.end method

.method public synthetic b(Lkotlin/reflect/jvm/internal/impl/b/a/h;)Lkotlin/reflect/jvm/internal/impl/k/ao;
    .locals 1

    .prologue
    .line 502
    invoke-virtual {p0, p1}, Lkotlin/reflect/jvm/internal/impl/k/l$c;->c(Lkotlin/reflect/jvm/internal/impl/b/a/h;)Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v0

    return-object v0
.end method

.method public synthetic b(Z)Lkotlin/reflect/jvm/internal/impl/k/ao;
    .locals 1

    .prologue
    .line 502
    invoke-virtual {p0, p1}, Lkotlin/reflect/jvm/internal/impl/k/l$c;->a(Z)Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v0

    return-object v0
.end method

.method public c(Lkotlin/reflect/jvm/internal/impl/b/a/h;)Lkotlin/reflect/jvm/internal/impl/k/v;
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "newAnnotations"

    aput-object v3, v2, v4

    const-string v3, "kotlin/reflect/jvm/internal/impl/types/ErrorUtils$ErrorTypeImpl"

    aput-object v3, v2, v5

    const-string v3, "replaceAnnotations"

    aput-object v3, v2, v6

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 566
    :cond_0
    if-nez p0, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "@NotNull method %s.%s must not return null"

    new-array v2, v6, [Ljava/lang/Object;

    const-string v3, "kotlin/reflect/jvm/internal/impl/types/ErrorUtils$ErrorTypeImpl"

    aput-object v3, v2, v4

    const-string v3, "replaceAnnotations"

    aput-object v3, v2, v5

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    return-object p0
.end method

.method public c()Z
    .locals 1

    .prologue
    .line 538
    iget-boolean v0, p0, Lkotlin/reflect/jvm/internal/impl/k/l$c;->d:Z

    return v0
.end method

.method public d()Z
    .locals 1

    .prologue
    .line 549
    const/4 v0, 0x1

    return v0
.end method

.method public i()Lkotlin/reflect/jvm/internal/impl/k/ad;
    .locals 5

    .prologue
    .line 527
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/k/l$c;->a:Lkotlin/reflect/jvm/internal/impl/k/ad;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "@NotNull method %s.%s must not return null"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "kotlin/reflect/jvm/internal/impl/types/ErrorUtils$ErrorTypeImpl"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "getConstructor"

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 8

    .prologue
    .line 560
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/k/l$c;->a:Lkotlin/reflect/jvm/internal/impl/k/ad;

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/k/l$c;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, ""

    :goto_0
    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/k/l$c;->c:Ljava/util/List;

    const-string v1, ", "

    const-string v2, "<"

    const-string v3, ">"

    const/4 v4, -0x1

    const-string v5, "..."

    const/4 v6, 0x0

    invoke-static/range {v0 .. v6}, Lkotlin/a/m;->a(Ljava/lang/Iterable;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;ILjava/lang/CharSequence;Lkotlin/d/a/b;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public w()Lkotlin/reflect/jvm/internal/impl/b/a/h;
    .locals 5

    .prologue
    .line 555
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/b/a/h;->a:Lkotlin/reflect/jvm/internal/impl/b/a/h$a;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/b/a/h$a;->a()Lkotlin/reflect/jvm/internal/impl/b/a/h;

    move-result-object v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "@NotNull method %s.%s must not return null"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "kotlin/reflect/jvm/internal/impl/types/ErrorUtils$ErrorTypeImpl"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "getAnnotations"

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    return-object v0
.end method
