.class public final Lkotlin/reflect/jvm/internal/impl/k/a/k;
.super Ljava/lang/Object;
.source "NewKotlinTypeChecker.kt"


# static fields
.field public static final a:Lkotlin/reflect/jvm/internal/impl/k/a/k;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 332
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/k/a/k;

    invoke-direct {v0}, Lkotlin/reflect/jvm/internal/impl/k/a/k;-><init>()V

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 332
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    check-cast p0, Lkotlin/reflect/jvm/internal/impl/k/a/k;

    sput-object p0, Lkotlin/reflect/jvm/internal/impl/k/a/k;->a:Lkotlin/reflect/jvm/internal/impl/k/a/k;

    return-void
.end method

.method private final a(Lkotlin/reflect/jvm/internal/impl/k/a/n;Lkotlin/reflect/jvm/internal/impl/k/v;Lkotlin/reflect/jvm/internal/impl/k/a/n$a;)Z
    .locals 2

    .prologue
    .line 370
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/k/a/k$a;->a:Lkotlin/reflect/jvm/internal/impl/k/a/k$a;

    check-cast v0, Lkotlin/d/a/b;

    new-instance v1, Lkotlin/reflect/jvm/internal/impl/k/a/k$b;

    invoke-direct {v1, p3}, Lkotlin/reflect/jvm/internal/impl/k/a/k$b;-><init>(Lkotlin/reflect/jvm/internal/impl/k/a/n$a;)V

    check-cast v1, Lkotlin/d/a/b;

    invoke-virtual {p1, p2, v0, v1}, Lkotlin/reflect/jvm/internal/impl/k/a/n;->a(Lkotlin/reflect/jvm/internal/impl/k/v;Lkotlin/d/a/b;Lkotlin/d/a/b;)Z

    move-result v0

    .line 372
    return v0
.end method

.method private final a(Lkotlin/reflect/jvm/internal/impl/k/a/n;Lkotlin/reflect/jvm/internal/impl/k/v;Lkotlin/reflect/jvm/internal/impl/k/ad;)Z
    .locals 2

    .prologue
    .line 375
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/k/a/k$c;

    invoke-direct {v0, p3}, Lkotlin/reflect/jvm/internal/impl/k/a/k$c;-><init>(Lkotlin/reflect/jvm/internal/impl/k/ad;)V

    check-cast v0, Lkotlin/d/a/b;

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/k/a/k$d;->a:Lkotlin/reflect/jvm/internal/impl/k/a/k$d;

    check-cast v1, Lkotlin/d/a/b;

    invoke-virtual {p1, p2, v0, v1}, Lkotlin/reflect/jvm/internal/impl/k/a/n;->a(Lkotlin/reflect/jvm/internal/impl/k/v;Lkotlin/d/a/b;Lkotlin/d/a/b;)Z

    move-result v0

    .line 377
    return v0
.end method

.method private final b(Lkotlin/reflect/jvm/internal/impl/k/a/n;Lkotlin/reflect/jvm/internal/impl/k/v;Lkotlin/reflect/jvm/internal/impl/k/v;)Z
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 340
    invoke-static {p2}, Lkotlin/reflect/jvm/internal/impl/k/a/j;->c(Lkotlin/reflect/jvm/internal/impl/k/v;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p2}, Lkotlin/reflect/jvm/internal/impl/k/a/j;->b(Lkotlin/reflect/jvm/internal/impl/k/v;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    move v0, v2

    :goto_0
    sget-boolean v3, Lkotlin/p;->a:Z

    if-eqz v3, :cond_2

    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Not singleClassifierType superType: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    :cond_1
    move v0, v1

    goto :goto_0

    .line 341
    :cond_2
    invoke-static {p3}, Lkotlin/reflect/jvm/internal/impl/k/a/j;->b(Lkotlin/reflect/jvm/internal/impl/k/v;)Z

    move-result v0

    sget-boolean v3, Lkotlin/p;->a:Z

    if-eqz v3, :cond_3

    if-nez v0, :cond_3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Not singleClassifierType superType: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    .line 344
    :cond_3
    invoke-virtual {p3}, Lkotlin/reflect/jvm/internal/impl/k/v;->c()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 366
    :cond_4
    :goto_1
    return v2

    .line 347
    :cond_5
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/k/a/n$a$a;->a:Lkotlin/reflect/jvm/internal/impl/k/a/n$a$a;

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/a/n$a;

    invoke-direct {p0, p1, p2, v0}, Lkotlin/reflect/jvm/internal/impl/k/a/k;->a(Lkotlin/reflect/jvm/internal/impl/k/a/n;Lkotlin/reflect/jvm/internal/impl/k/v;Lkotlin/reflect/jvm/internal/impl/k/a/n$a;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 350
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/k/a/n$a$d;->a:Lkotlin/reflect/jvm/internal/impl/k/a/n$a$d;

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/a/n$a;

    invoke-direct {p0, p1, p3, v0}, Lkotlin/reflect/jvm/internal/impl/k/a/k;->a(Lkotlin/reflect/jvm/internal/impl/k/a/n;Lkotlin/reflect/jvm/internal/impl/k/v;Lkotlin/reflect/jvm/internal/impl/k/a/n$a;)Z

    move-result v0

    if-eqz v0, :cond_6

    move v2, v1

    goto :goto_1

    .line 364
    :cond_6
    invoke-static {p2}, Lkotlin/reflect/jvm/internal/impl/k/a/j;->a(Lkotlin/reflect/jvm/internal/impl/k/v;)Z

    move-result v0

    if-eqz v0, :cond_7

    move v2, v1

    goto :goto_1

    .line 366
    :cond_7
    invoke-virtual {p3}, Lkotlin/reflect/jvm/internal/impl/k/v;->i()Lkotlin/reflect/jvm/internal/impl/k/ad;

    move-result-object v0

    invoke-direct {p0, p1, p2, v0}, Lkotlin/reflect/jvm/internal/impl/k/a/k;->a(Lkotlin/reflect/jvm/internal/impl/k/a/n;Lkotlin/reflect/jvm/internal/impl/k/v;Lkotlin/reflect/jvm/internal/impl/k/ad;)Z

    move-result v2

    goto :goto_1
.end method


# virtual methods
.method public final a(Lkotlin/reflect/jvm/internal/impl/k/a/n;Lkotlin/reflect/jvm/internal/impl/k/v;Lkotlin/reflect/jvm/internal/impl/k/v;)Z
    .locals 1

    .prologue
    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "subType"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "superType"

    invoke-static {p3, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 336
    invoke-direct {p0, p1, p2, p3}, Lkotlin/reflect/jvm/internal/impl/k/a/k;->b(Lkotlin/reflect/jvm/internal/impl/k/a/n;Lkotlin/reflect/jvm/internal/impl/k/v;Lkotlin/reflect/jvm/internal/impl/k/v;)Z

    move-result v0

    return v0
.end method
