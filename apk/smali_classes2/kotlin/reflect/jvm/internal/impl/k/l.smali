.class public Lkotlin/reflect/jvm/internal/impl/k/l;
.super Ljava/lang/Object;
.source "ErrorUtils.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lkotlin/reflect/jvm/internal/impl/k/l$e;,
        Lkotlin/reflect/jvm/internal/impl/k/l$c;,
        Lkotlin/reflect/jvm/internal/impl/k/l$a;,
        Lkotlin/reflect/jvm/internal/impl/k/l$d;,
        Lkotlin/reflect/jvm/internal/impl/k/l$b;
    }
.end annotation


# static fields
.field public static final a:Lkotlin/reflect/jvm/internal/impl/k/v;

.field private static final b:Lkotlin/reflect/jvm/internal/impl/b/w;

.field private static final c:Lkotlin/reflect/jvm/internal/impl/k/l$a;

.field private static final d:Lkotlin/reflect/jvm/internal/impl/k/r;

.field private static final e:Lkotlin/reflect/jvm/internal/impl/b/ag;

.field private static final f:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/b/ag;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 52
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/k/l$1;

    invoke-direct {v0}, Lkotlin/reflect/jvm/internal/impl/k/l$1;-><init>()V

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/k/l;->b:Lkotlin/reflect/jvm/internal/impl/b/w;

    .line 297
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/k/l$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lkotlin/reflect/jvm/internal/impl/k/l$a;-><init>(Ljava/lang/String;)V

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/k/l;->c:Lkotlin/reflect/jvm/internal/impl/k/l$a;

    .line 364
    const-string v0, "<LOOP IN SUPERTYPES>"

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/k/l;->c(Ljava/lang/String;)Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v0

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/k/l;->a:Lkotlin/reflect/jvm/internal/impl/k/v;

    .line 366
    const-string v0, "<ERROR PROPERTY TYPE>"

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/k/l;->c(Ljava/lang/String;)Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v0

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/k/l;->d:Lkotlin/reflect/jvm/internal/impl/k/r;

    .line 367
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/k/l;->c()Lkotlin/reflect/jvm/internal/impl/b/b/y;

    move-result-object v0

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/k/l;->e:Lkotlin/reflect/jvm/internal/impl/b/ag;

    .line 369
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/k/l;->e:Lkotlin/reflect/jvm/internal/impl/b/ag;

    invoke-static {v0}, Ljava/util/Collections;->singleton(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v0

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/k/l;->f:Ljava/util/Set;

    return-void
.end method

.method static synthetic a(Lkotlin/reflect/jvm/internal/impl/k/l$b;)Lkotlin/reflect/jvm/internal/impl/b/ak;
    .locals 1

    .prologue
    .line 48
    invoke-static {p0}, Lkotlin/reflect/jvm/internal/impl/k/l;->b(Lkotlin/reflect/jvm/internal/impl/k/l$b;)Lkotlin/reflect/jvm/internal/impl/b/ak;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/lang/String;)Lkotlin/reflect/jvm/internal/impl/b/e;
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    if-nez p0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "debugMessage"

    aput-object v3, v2, v4

    const-string v3, "kotlin/reflect/jvm/internal/impl/types/ErrorUtils"

    aput-object v3, v2, v5

    const-string v3, "createErrorClass"

    aput-object v3, v2, v6

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 347
    :cond_0
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/k/l$a;

    invoke-direct {v0, p0}, Lkotlin/reflect/jvm/internal/impl/k/l$a;-><init>(Ljava/lang/String;)V

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "@NotNull method %s.%s must not return null"

    new-array v2, v6, [Ljava/lang/Object;

    const-string v3, "kotlin/reflect/jvm/internal/impl/types/ErrorUtils"

    aput-object v3, v2, v4

    const-string v3, "createErrorClass"

    aput-object v3, v2, v5

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    return-object v0
.end method

.method public static a()Lkotlin/reflect/jvm/internal/impl/b/w;
    .locals 5

    .prologue
    .line 578
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/k/l;->b:Lkotlin/reflect/jvm/internal/impl/b/w;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "@NotNull method %s.%s must not return null"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "kotlin/reflect/jvm/internal/impl/types/ErrorUtils"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "getErrorModule"

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    return-object v0
.end method

.method public static a(Ljava/lang/String;Z)Lkotlin/reflect/jvm/internal/impl/h/e/h;
    .locals 7

    .prologue
    const/4 v1, 0x0

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    if-nez p0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "debugMessage"

    aput-object v3, v2, v4

    const-string v3, "kotlin/reflect/jvm/internal/impl/types/ErrorUtils"

    aput-object v3, v2, v5

    const-string v3, "createErrorScope"

    aput-object v3, v2, v6

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 357
    :cond_0
    if-eqz p1, :cond_1

    .line 358
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/k/l$d;

    invoke-direct {v0, p0, v1}, Lkotlin/reflect/jvm/internal/impl/k/l$d;-><init>(Ljava/lang/String;Lkotlin/reflect/jvm/internal/impl/k/l$1;)V

    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "@NotNull method %s.%s must not return null"

    new-array v2, v6, [Ljava/lang/Object;

    const-string v3, "kotlin/reflect/jvm/internal/impl/types/ErrorUtils"

    aput-object v3, v2, v4

    const-string v3, "createErrorScope"

    aput-object v3, v2, v5

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 360
    :cond_1
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/k/l$b;

    invoke-direct {v0, p0, v1}, Lkotlin/reflect/jvm/internal/impl/k/l$b;-><init>(Ljava/lang/String;Lkotlin/reflect/jvm/internal/impl/k/l$1;)V

    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "@NotNull method %s.%s must not return null"

    new-array v2, v6, [Ljava/lang/Object;

    const-string v3, "kotlin/reflect/jvm/internal/impl/types/ErrorUtils"

    aput-object v3, v2, v4

    const-string v3, "createErrorScope"

    aput-object v3, v2, v5

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    return-object v0
.end method

.method static synthetic a(Ljava/lang/String;Lkotlin/reflect/jvm/internal/impl/k/l$a;)Lkotlin/reflect/jvm/internal/impl/k/ad;
    .locals 1

    .prologue
    .line 48
    invoke-static {p0, p1}, Lkotlin/reflect/jvm/internal/impl/k/l;->b(Ljava/lang/String;Lkotlin/reflect/jvm/internal/impl/k/l$a;)Lkotlin/reflect/jvm/internal/impl/k/ad;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/lang/String;Ljava/util/List;)Lkotlin/reflect/jvm/internal/impl/k/v;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/k/ah;",
            ">;)",
            "Lkotlin/reflect/jvm/internal/impl/k/v;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v4, 0x0

    if-nez p0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "debugMessage"

    aput-object v3, v2, v4

    const-string v3, "kotlin/reflect/jvm/internal/impl/types/ErrorUtils"

    aput-object v3, v2, v6

    const-string v3, "createErrorTypeWithArguments"

    aput-object v3, v2, v7

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    if-nez p1, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "arguments"

    aput-object v3, v2, v4

    const-string v3, "kotlin/reflect/jvm/internal/impl/types/ErrorUtils"

    aput-object v3, v2, v6

    const-string v3, "createErrorTypeWithArguments"

    aput-object v3, v2, v7

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 425
    :cond_1
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/k/l$c;

    invoke-static {p0}, Lkotlin/reflect/jvm/internal/impl/k/l;->e(Ljava/lang/String;)Lkotlin/reflect/jvm/internal/impl/k/ad;

    move-result-object v1

    invoke-static {p0}, Lkotlin/reflect/jvm/internal/impl/k/l;->b(Ljava/lang/String;)Lkotlin/reflect/jvm/internal/impl/h/e/h;

    move-result-object v2

    const/4 v5, 0x0

    move-object v3, p1

    invoke-direct/range {v0 .. v5}, Lkotlin/reflect/jvm/internal/impl/k/l$c;-><init>(Lkotlin/reflect/jvm/internal/impl/k/ad;Lkotlin/reflect/jvm/internal/impl/h/e/h;Ljava/util/List;ZLkotlin/reflect/jvm/internal/impl/k/l$1;)V

    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "@NotNull method %s.%s must not return null"

    new-array v2, v7, [Ljava/lang/Object;

    const-string v3, "kotlin/reflect/jvm/internal/impl/types/ErrorUtils"

    aput-object v3, v2, v4

    const-string v3, "createErrorTypeWithArguments"

    aput-object v3, v2, v6

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    return-object v0
.end method

.method public static a(Ljava/lang/String;Lkotlin/reflect/jvm/internal/impl/k/ad;)Lkotlin/reflect/jvm/internal/impl/k/v;
    .locals 7

    .prologue
    const/4 v2, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    if-nez p0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "debugName"

    aput-object v3, v2, v4

    const-string v3, "kotlin/reflect/jvm/internal/impl/types/ErrorUtils"

    aput-object v3, v2, v5

    const-string v3, "createErrorTypeWithCustomConstructor"

    aput-object v3, v2, v6

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    if-nez p1, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "typeConstructor"

    aput-object v3, v2, v4

    const-string v3, "kotlin/reflect/jvm/internal/impl/types/ErrorUtils"

    aput-object v3, v2, v5

    const-string v3, "createErrorTypeWithCustomConstructor"

    aput-object v3, v2, v6

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 420
    :cond_1
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/k/l$c;

    invoke-static {p0}, Lkotlin/reflect/jvm/internal/impl/k/l;->b(Ljava/lang/String;)Lkotlin/reflect/jvm/internal/impl/h/e/h;

    move-result-object v1

    const/4 v2, 0x0

    invoke-direct {v0, p1, v1, v2}, Lkotlin/reflect/jvm/internal/impl/k/l$c;-><init>(Lkotlin/reflect/jvm/internal/impl/k/ad;Lkotlin/reflect/jvm/internal/impl/h/e/h;Lkotlin/reflect/jvm/internal/impl/k/l$1;)V

    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "@NotNull method %s.%s must not return null"

    new-array v2, v6, [Ljava/lang/Object;

    const-string v3, "kotlin/reflect/jvm/internal/impl/types/ErrorUtils"

    aput-object v3, v2, v4

    const-string v3, "createErrorTypeWithCustomConstructor"

    aput-object v3, v2, v5

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    return-object v0
.end method

.method public static a(Lkotlin/reflect/jvm/internal/impl/b/m;)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 494
    if-nez p0, :cond_1

    .line 495
    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-static {p0}, Lkotlin/reflect/jvm/internal/impl/k/l;->b(Lkotlin/reflect/jvm/internal/impl/b/m;)Z

    move-result v1

    if-nez v1, :cond_2

    invoke-interface {p0}, Lkotlin/reflect/jvm/internal/impl/b/m;->y_()Lkotlin/reflect/jvm/internal/impl/b/m;

    move-result-object v1

    invoke-static {v1}, Lkotlin/reflect/jvm/internal/impl/k/l;->b(Lkotlin/reflect/jvm/internal/impl/b/m;)Z

    move-result v1

    if-nez v1, :cond_2

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/k/l;->b:Lkotlin/reflect/jvm/internal/impl/b/w;

    if-ne p0, v1, :cond_0

    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static a(Lkotlin/reflect/jvm/internal/impl/k/r;)Z
    .locals 1

    .prologue
    .line 582
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/k/r;->i()Lkotlin/reflect/jvm/internal/impl/k/ad;

    move-result-object v0

    instance-of v0, v0, Lkotlin/reflect/jvm/internal/impl/k/l$e;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic b()Ljava/util/Set;
    .locals 1

    .prologue
    .line 48
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/k/l;->f:Ljava/util/Set;

    return-object v0
.end method

.method private static b(Lkotlin/reflect/jvm/internal/impl/k/l$b;)Lkotlin/reflect/jvm/internal/impl/b/ak;
    .locals 11

    .prologue
    const/4 v1, 0x0

    const/4 v10, 0x2

    const/4 v9, 0x1

    const/4 v8, 0x0

    if-nez p0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "ownerScope"

    aput-object v3, v2, v8

    const-string v3, "kotlin/reflect/jvm/internal/impl/types/ErrorUtils"

    aput-object v3, v2, v9

    const-string v3, "createErrorFunction"

    aput-object v3, v2, v10

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 395
    :cond_0
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/k/b/a;

    sget-object v2, Lkotlin/reflect/jvm/internal/impl/k/l;->c:Lkotlin/reflect/jvm/internal/impl/k/l$a;

    invoke-direct {v0, v2, p0}, Lkotlin/reflect/jvm/internal/impl/k/b/a;-><init>(Lkotlin/reflect/jvm/internal/impl/b/e;Lkotlin/reflect/jvm/internal/impl/k/l$b;)V

    .line 396
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v3

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v4

    const-string v2, "<ERROR FUNCTION RETURN TYPE>"

    invoke-static {v2}, Lkotlin/reflect/jvm/internal/impl/k/l;->c(Ljava/lang/String;)Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v5

    sget-object v6, Lkotlin/reflect/jvm/internal/impl/b/u;->c:Lkotlin/reflect/jvm/internal/impl/b/u;

    sget-object v7, Lkotlin/reflect/jvm/internal/impl/b/ax;->e:Lkotlin/reflect/jvm/internal/impl/b/ay;

    move-object v2, v1

    invoke-virtual/range {v0 .. v7}, Lkotlin/reflect/jvm/internal/impl/k/b/a;->b(Lkotlin/reflect/jvm/internal/impl/k/r;Lkotlin/reflect/jvm/internal/impl/b/aj;Ljava/util/List;Ljava/util/List;Lkotlin/reflect/jvm/internal/impl/k/r;Lkotlin/reflect/jvm/internal/impl/b/u;Lkotlin/reflect/jvm/internal/impl/b/ay;)Lkotlin/reflect/jvm/internal/impl/b/b/ac;

    .line 405
    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "@NotNull method %s.%s must not return null"

    new-array v2, v10, [Ljava/lang/Object;

    const-string v3, "kotlin/reflect/jvm/internal/impl/types/ErrorUtils"

    aput-object v3, v2, v8

    const-string v3, "createErrorFunction"

    aput-object v3, v2, v9

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    return-object v0
.end method

.method public static b(Ljava/lang/String;)Lkotlin/reflect/jvm/internal/impl/h/e/h;
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    if-nez p0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "debugMessage"

    aput-object v3, v2, v4

    const-string v3, "kotlin/reflect/jvm/internal/impl/types/ErrorUtils"

    aput-object v3, v2, v5

    const-string v3, "createErrorScope"

    aput-object v3, v2, v6

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 352
    :cond_0
    invoke-static {p0, v4}, Lkotlin/reflect/jvm/internal/impl/k/l;->a(Ljava/lang/String;Z)Lkotlin/reflect/jvm/internal/impl/h/e/h;

    move-result-object v0

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "@NotNull method %s.%s must not return null"

    new-array v2, v6, [Ljava/lang/Object;

    const-string v3, "kotlin/reflect/jvm/internal/impl/types/ErrorUtils"

    aput-object v3, v2, v4

    const-string v3, "createErrorScope"

    aput-object v3, v2, v5

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    return-object v0
.end method

.method private static b(Ljava/lang/String;Lkotlin/reflect/jvm/internal/impl/k/l$a;)Lkotlin/reflect/jvm/internal/impl/k/ad;
    .locals 7

    .prologue
    const/4 v2, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    if-nez p0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "debugName"

    aput-object v3, v2, v4

    const-string v3, "kotlin/reflect/jvm/internal/impl/types/ErrorUtils"

    aput-object v3, v2, v5

    const-string v3, "createErrorTypeConstructorWithCustomDebugName"

    aput-object v3, v2, v6

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    if-nez p1, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "errorClass"

    aput-object v3, v2, v4

    const-string v3, "kotlin/reflect/jvm/internal/impl/types/ErrorUtils"

    aput-object v3, v2, v5

    const-string v3, "createErrorTypeConstructorWithCustomDebugName"

    aput-object v3, v2, v6

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 442
    :cond_1
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/k/l$2;

    invoke-direct {v0, p1, p0}, Lkotlin/reflect/jvm/internal/impl/k/l$2;-><init>(Lkotlin/reflect/jvm/internal/impl/k/l$a;Ljava/lang/String;)V

    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "@NotNull method %s.%s must not return null"

    new-array v2, v6, [Ljava/lang/Object;

    const-string v3, "kotlin/reflect/jvm/internal/impl/types/ErrorUtils"

    aput-object v3, v2, v4

    const-string v3, "createErrorTypeConstructorWithCustomDebugName"

    aput-object v3, v2, v5

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    return-object v0
.end method

.method private static b(Lkotlin/reflect/jvm/internal/impl/b/m;)Z
    .locals 1

    .prologue
    .line 499
    instance-of v0, p0, Lkotlin/reflect/jvm/internal/impl/k/l$a;

    return v0
.end method

.method private static c()Lkotlin/reflect/jvm/internal/impl/b/b/y;
    .locals 15

    .prologue
    const/4 v14, 0x0

    const/4 v4, 0x1

    const/4 v8, 0x0

    .line 373
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/k/l;->c:Lkotlin/reflect/jvm/internal/impl/k/l$a;

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/b/a/h;->a:Lkotlin/reflect/jvm/internal/impl/b/a/h$a;

    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/impl/b/a/h$a;->a()Lkotlin/reflect/jvm/internal/impl/b/a/h;

    move-result-object v1

    sget-object v2, Lkotlin/reflect/jvm/internal/impl/b/u;->c:Lkotlin/reflect/jvm/internal/impl/b/u;

    sget-object v3, Lkotlin/reflect/jvm/internal/impl/b/ax;->e:Lkotlin/reflect/jvm/internal/impl/b/ay;

    const-string v5, "<ERROR PROPERTY>"

    invoke-static {v5}, Lkotlin/reflect/jvm/internal/impl/e/f;->c(Ljava/lang/String;)Lkotlin/reflect/jvm/internal/impl/e/f;

    move-result-object v5

    sget-object v6, Lkotlin/reflect/jvm/internal/impl/b/b$a;->a:Lkotlin/reflect/jvm/internal/impl/b/b$a;

    sget-object v7, Lkotlin/reflect/jvm/internal/impl/b/al;->a:Lkotlin/reflect/jvm/internal/impl/b/al;

    move v9, v8

    move v10, v8

    move v11, v8

    move v12, v8

    move v13, v8

    invoke-static/range {v0 .. v13}, Lkotlin/reflect/jvm/internal/impl/b/b/y;->a(Lkotlin/reflect/jvm/internal/impl/b/m;Lkotlin/reflect/jvm/internal/impl/b/a/h;Lkotlin/reflect/jvm/internal/impl/b/u;Lkotlin/reflect/jvm/internal/impl/b/ay;ZLkotlin/reflect/jvm/internal/impl/e/f;Lkotlin/reflect/jvm/internal/impl/b/b$a;Lkotlin/reflect/jvm/internal/impl/b/al;ZZZZZZ)Lkotlin/reflect/jvm/internal/impl/b/b/y;

    move-result-object v1

    .line 384
    sget-object v2, Lkotlin/reflect/jvm/internal/impl/k/l;->d:Lkotlin/reflect/jvm/internal/impl/k/r;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v3

    move-object v0, v14

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/r;

    invoke-virtual {v1, v2, v3, v14, v0}, Lkotlin/reflect/jvm/internal/impl/b/b/y;->a(Lkotlin/reflect/jvm/internal/impl/k/r;Ljava/util/List;Lkotlin/reflect/jvm/internal/impl/b/aj;Lkotlin/reflect/jvm/internal/impl/k/r;)V

    .line 390
    if-nez v1, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "@NotNull method %s.%s must not return null"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "kotlin/reflect/jvm/internal/impl/types/ErrorUtils"

    aput-object v3, v2, v8

    const-string v3, "createErrorProperty"

    aput-object v3, v2, v4

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    return-object v1
.end method

.method public static c(Ljava/lang/String;)Lkotlin/reflect/jvm/internal/impl/k/v;
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    if-nez p0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "debugMessage"

    aput-object v3, v2, v4

    const-string v3, "kotlin/reflect/jvm/internal/impl/types/ErrorUtils"

    aput-object v3, v2, v5

    const-string v3, "createErrorType"

    aput-object v3, v2, v6

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 410
    :cond_0
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    invoke-static {p0, v0}, Lkotlin/reflect/jvm/internal/impl/k/l;->a(Ljava/lang/String;Ljava/util/List;)Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v0

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "@NotNull method %s.%s must not return null"

    new-array v2, v6, [Ljava/lang/Object;

    const-string v3, "kotlin/reflect/jvm/internal/impl/types/ErrorUtils"

    aput-object v3, v2, v4

    const-string v3, "createErrorType"

    aput-object v3, v2, v5

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    return-object v0
.end method

.method public static d(Ljava/lang/String;)Lkotlin/reflect/jvm/internal/impl/k/v;
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    if-nez p0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "debugName"

    aput-object v3, v2, v4

    const-string v3, "kotlin/reflect/jvm/internal/impl/types/ErrorUtils"

    aput-object v3, v2, v5

    const-string v3, "createErrorTypeWithCustomDebugName"

    aput-object v3, v2, v6

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 415
    :cond_0
    invoke-static {p0}, Lkotlin/reflect/jvm/internal/impl/k/l;->f(Ljava/lang/String;)Lkotlin/reflect/jvm/internal/impl/k/ad;

    move-result-object v0

    invoke-static {p0, v0}, Lkotlin/reflect/jvm/internal/impl/k/l;->a(Ljava/lang/String;Lkotlin/reflect/jvm/internal/impl/k/ad;)Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v0

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "@NotNull method %s.%s must not return null"

    new-array v2, v6, [Ljava/lang/Object;

    const-string v3, "kotlin/reflect/jvm/internal/impl/types/ErrorUtils"

    aput-object v3, v2, v4

    const-string v3, "createErrorTypeWithCustomDebugName"

    aput-object v3, v2, v5

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    return-object v0
.end method

.method public static e(Ljava/lang/String;)Lkotlin/reflect/jvm/internal/impl/k/ad;
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    if-nez p0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "debugMessage"

    aput-object v3, v2, v4

    const-string v3, "kotlin/reflect/jvm/internal/impl/types/ErrorUtils"

    aput-object v3, v2, v5

    const-string v3, "createErrorTypeConstructor"

    aput-object v3, v2, v6

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 430
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[ERROR : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/k/l;->c:Lkotlin/reflect/jvm/internal/impl/k/l$a;

    invoke-static {v0, v1}, Lkotlin/reflect/jvm/internal/impl/k/l;->b(Ljava/lang/String;Lkotlin/reflect/jvm/internal/impl/k/l$a;)Lkotlin/reflect/jvm/internal/impl/k/ad;

    move-result-object v0

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "@NotNull method %s.%s must not return null"

    new-array v2, v6, [Ljava/lang/Object;

    const-string v3, "kotlin/reflect/jvm/internal/impl/types/ErrorUtils"

    aput-object v3, v2, v4

    const-string v3, "createErrorTypeConstructor"

    aput-object v3, v2, v5

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    return-object v0
.end method

.method public static f(Ljava/lang/String;)Lkotlin/reflect/jvm/internal/impl/k/ad;
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    if-nez p0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "debugName"

    aput-object v3, v2, v4

    const-string v3, "kotlin/reflect/jvm/internal/impl/types/ErrorUtils"

    aput-object v3, v2, v5

    const-string v3, "createErrorTypeConstructorWithCustomDebugName"

    aput-object v3, v2, v6

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 435
    :cond_0
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/k/l;->c:Lkotlin/reflect/jvm/internal/impl/k/l$a;

    invoke-static {p0, v0}, Lkotlin/reflect/jvm/internal/impl/k/l;->b(Ljava/lang/String;Lkotlin/reflect/jvm/internal/impl/k/l$a;)Lkotlin/reflect/jvm/internal/impl/k/ad;

    move-result-object v0

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "@NotNull method %s.%s must not return null"

    new-array v2, v6, [Ljava/lang/Object;

    const-string v3, "kotlin/reflect/jvm/internal/impl/types/ErrorUtils"

    aput-object v3, v2, v4

    const-string v3, "createErrorTypeConstructorWithCustomDebugName"

    aput-object v3, v2, v5

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    return-object v0
.end method
