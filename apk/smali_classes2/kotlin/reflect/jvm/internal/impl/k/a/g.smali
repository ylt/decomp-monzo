.class public final Lkotlin/reflect/jvm/internal/impl/k/a/g;
.super Ljava/lang/Object;
.source "NewCapturedType.kt"


# direct methods
.method public static final a(Lkotlin/reflect/jvm/internal/impl/k/v;Lkotlin/reflect/jvm/internal/impl/k/a/a;Lkotlin/d/a/m;)Lkotlin/reflect/jvm/internal/impl/k/v;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/reflect/jvm/internal/impl/k/v;",
            "Lkotlin/reflect/jvm/internal/impl/k/a/a;",
            "Lkotlin/d/a/m",
            "<-",
            "Ljava/lang/Integer;",
            "-",
            "Lkotlin/reflect/jvm/internal/impl/k/a/e;",
            "Lkotlin/n;",
            ">;)",
            "Lkotlin/reflect/jvm/internal/impl/k/v;"
        }
    .end annotation

    .prologue
    const-string v0, "type"

    invoke-static {p0, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "status"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "acceptNewCapturedType"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 34
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/k/v;->a()Ljava/util/List;

    move-result-object v1

    move-object v0, v1

    .line 35
    check-cast v0, Ljava/lang/Iterable;

    .line 123
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/ah;

    .line 35
    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/k/ah;->b()Lkotlin/reflect/jvm/internal/impl/k/ap;

    move-result-object v0

    sget-object v3, Lkotlin/reflect/jvm/internal/impl/k/ap;->a:Lkotlin/reflect/jvm/internal/impl/k/ap;

    invoke-static {v0, v3}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    .line 124
    :goto_0
    if-eqz v0, :cond_2

    .line 66
    :goto_1
    return-object p0

    .line 124
    :cond_1
    const/4 v0, 0x1

    goto :goto_0

    :cond_2
    move-object v0, v1

    .line 37
    check-cast v0, Ljava/lang/Iterable;

    .line 125
    new-instance v2, Ljava/util/ArrayList;

    const/16 v3, 0xa

    invoke-static {v0, v3}, Lkotlin/a/m;->a(Ljava/lang/Iterable;I)I

    move-result v3

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v2, Ljava/util/Collection;

    .line 126
    const/4 v3, 0x0

    .line 127
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_2
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .line 128
    add-int/lit8 v4, v3, 0x1

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/ah;

    .line 39
    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/k/ah;->b()Lkotlin/reflect/jvm/internal/impl/k/ap;

    move-result-object v3

    sget-object v5, Lkotlin/reflect/jvm/internal/impl/k/ap;->a:Lkotlin/reflect/jvm/internal/impl/k/ap;

    invoke-static {v3, v5}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 45
    :goto_3
    invoke-interface {v2, v0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    move v3, v4

    .line 127
    goto :goto_2

    .line 41
    :cond_3
    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/k/ah;->a()Z

    move-result v3

    if-nez v3, :cond_4

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/k/ah;->b()Lkotlin/reflect/jvm/internal/impl/k/ap;

    move-result-object v3

    sget-object v5, Lkotlin/reflect/jvm/internal/impl/k/ap;->b:Lkotlin/reflect/jvm/internal/impl/k/ap;

    invoke-static {v3, v5}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 42
    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/k/ah;->c()Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v3

    invoke-virtual {v3}, Lkotlin/reflect/jvm/internal/impl/k/r;->j()Lkotlin/reflect/jvm/internal/impl/k/ao;

    move-result-object v3

    move-object v5, v3

    .line 45
    :goto_4
    new-instance v3, Lkotlin/reflect/jvm/internal/impl/k/a/e;

    invoke-direct {v3, p1, v5, v0}, Lkotlin/reflect/jvm/internal/impl/k/a/e;-><init>(Lkotlin/reflect/jvm/internal/impl/k/a/a;Lkotlin/reflect/jvm/internal/impl/k/ao;Lkotlin/reflect/jvm/internal/impl/k/ah;)V

    move-object v0, v3

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/r;

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/k/c/a;->e(Lkotlin/reflect/jvm/internal/impl/k/r;)Lkotlin/reflect/jvm/internal/impl/k/ah;

    move-result-object v0

    goto :goto_3

    .line 43
    :cond_4
    const/4 v3, 0x0

    move-object v5, v3

    goto :goto_4

    .line 129
    :cond_5
    check-cast v2, Ljava/util/List;

    .line 48
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/k/ae;->b:Lkotlin/reflect/jvm/internal/impl/k/ae$a;

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/k/v;->i()Lkotlin/reflect/jvm/internal/impl/k/ad;

    move-result-object v3

    invoke-virtual {v0, v3, v2}, Lkotlin/reflect/jvm/internal/impl/k/ae$a;->a(Lkotlin/reflect/jvm/internal/impl/k/ad;Ljava/util/List;)Lkotlin/reflect/jvm/internal/impl/k/ak;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/k/ak;->f()Lkotlin/reflect/jvm/internal/impl/k/am;

    move-result-object v7

    .line 49
    const/4 v3, 0x0

    move-object v0, v1

    check-cast v0, Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->size()I

    move-result v0

    add-int/lit8 v8, v0, -0x1

    if-gt v3, v8, :cond_a

    move v6, v3

    .line 50
    :goto_5
    invoke-interface {v1, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lkotlin/reflect/jvm/internal/impl/k/ah;

    .line 51
    invoke-interface {v2, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lkotlin/reflect/jvm/internal/impl/k/ah;

    .line 53
    invoke-interface {v4}, Lkotlin/reflect/jvm/internal/impl/k/ah;->b()Lkotlin/reflect/jvm/internal/impl/k/ap;

    move-result-object v0

    sget-object v3, Lkotlin/reflect/jvm/internal/impl/k/ap;->a:Lkotlin/reflect/jvm/internal/impl/k/ap;

    invoke-static {v0, v3}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 49
    :goto_6
    if-eq v6, v8, :cond_a

    add-int/lit8 v0, v6, 0x1

    move v6, v0

    goto :goto_5

    .line 54
    :cond_6
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/k/v;->i()Lkotlin/reflect/jvm/internal/impl/k/ad;

    move-result-object v0

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/k/ad;->b()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/aq;

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/b/aq;->I_()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 130
    new-instance v3, Ljava/util/ArrayList;

    const/16 v9, 0xa

    invoke-static {v0, v9}, Lkotlin/a/m;->a(Ljava/lang/Iterable;I)I

    move-result v9

    invoke-direct {v3, v9}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v3, Ljava/util/Collection;

    .line 131
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :goto_7
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .line 132
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/r;

    .line 55
    sget-object v10, Lkotlin/reflect/jvm/internal/impl/k/a/h;->b:Lkotlin/reflect/jvm/internal/impl/k/a/h;

    sget-object v11, Lkotlin/reflect/jvm/internal/impl/k/ap;->a:Lkotlin/reflect/jvm/internal/impl/k/ap;

    invoke-virtual {v7, v0, v11}, Lkotlin/reflect/jvm/internal/impl/k/am;->a(Lkotlin/reflect/jvm/internal/impl/k/r;Lkotlin/reflect/jvm/internal/impl/k/ap;)Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/k/r;->j()Lkotlin/reflect/jvm/internal/impl/k/ao;

    move-result-object v0

    invoke-virtual {v10, v0}, Lkotlin/reflect/jvm/internal/impl/k/a/h;->a(Lkotlin/reflect/jvm/internal/impl/k/ao;)Lkotlin/reflect/jvm/internal/impl/k/ao;

    move-result-object v0

    invoke-interface {v3, v0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_7

    .line 133
    :cond_7
    check-cast v3, Ljava/util/List;

    .line 57
    invoke-interface {v4}, Lkotlin/reflect/jvm/internal/impl/k/ah;->a()Z

    move-result v0

    if-nez v0, :cond_8

    invoke-interface {v4}, Lkotlin/reflect/jvm/internal/impl/k/ah;->b()Lkotlin/reflect/jvm/internal/impl/k/ap;

    move-result-object v0

    sget-object v9, Lkotlin/reflect/jvm/internal/impl/k/ap;->c:Lkotlin/reflect/jvm/internal/impl/k/ap;

    invoke-static {v0, v9}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 58
    check-cast v3, Ljava/util/Collection;

    sget-object v0, Lkotlin/reflect/jvm/internal/impl/k/a/h;->b:Lkotlin/reflect/jvm/internal/impl/k/a/h;

    invoke-interface {v4}, Lkotlin/reflect/jvm/internal/impl/k/ah;->c()Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v4

    invoke-virtual {v4}, Lkotlin/reflect/jvm/internal/impl/k/r;->j()Lkotlin/reflect/jvm/internal/impl/k/ao;

    move-result-object v4

    invoke-virtual {v0, v4}, Lkotlin/reflect/jvm/internal/impl/k/a/h;->a(Lkotlin/reflect/jvm/internal/impl/k/ao;)Lkotlin/reflect/jvm/internal/impl/k/ao;

    move-result-object v0

    invoke-static {v3, v0}, Lkotlin/a/m;->a(Ljava/util/Collection;Ljava/lang/Object;)Ljava/util/List;

    move-result-object v3

    .line 61
    :cond_8
    invoke-interface {v5}, Lkotlin/reflect/jvm/internal/impl/k/ah;->c()Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v0

    if-nez v0, :cond_9

    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type org.jetbrains.kotlin.types.checker.NewCapturedType"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_9
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/a/e;

    .line 62
    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/k/a/e;->e()Lkotlin/reflect/jvm/internal/impl/k/a/f;

    move-result-object v4

    invoke-virtual {v4, v3}, Lkotlin/reflect/jvm/internal/impl/k/a/f;->a(Ljava/util/List;)V

    .line 63
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {p2, v3, v0}, Lkotlin/d/a/m;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_6

    .line 66
    :cond_a
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/k/v;->w()Lkotlin/reflect/jvm/internal/impl/b/a/h;

    move-result-object v0

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/k/v;->i()Lkotlin/reflect/jvm/internal/impl/k/ad;

    move-result-object v1

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/k/v;->c()Z

    move-result v3

    const/4 v4, 0x0

    const/16 v5, 0x10

    const/4 v6, 0x0

    invoke-static/range {v0 .. v6}, Lkotlin/reflect/jvm/internal/impl/k/s;->a(Lkotlin/reflect/jvm/internal/impl/b/a/h;Lkotlin/reflect/jvm/internal/impl/k/ad;Ljava/util/List;ZLkotlin/reflect/jvm/internal/impl/h/e/h;ILjava/lang/Object;)Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object p0

    goto/16 :goto_1
.end method

.method public static synthetic a(Lkotlin/reflect/jvm/internal/impl/k/v;Lkotlin/reflect/jvm/internal/impl/k/a/a;Lkotlin/d/a/m;ILjava/lang/Object;)Lkotlin/reflect/jvm/internal/impl/k/v;
    .locals 1

    .prologue
    and-int/lit8 v0, p3, 0x4

    if-eqz v0, :cond_0

    .line 32
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/utils/d;->c()Lkotlin/d/a/m;

    move-result-object p2

    :cond_0
    invoke-static {p0, p1, p2}, Lkotlin/reflect/jvm/internal/impl/k/a/g;->a(Lkotlin/reflect/jvm/internal/impl/k/v;Lkotlin/reflect/jvm/internal/impl/k/a/a;Lkotlin/d/a/m;)Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v0

    return-object v0
.end method
