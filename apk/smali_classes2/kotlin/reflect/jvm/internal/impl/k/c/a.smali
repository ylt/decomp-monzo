.class public final Lkotlin/reflect/jvm/internal/impl/k/c/a;
.super Ljava/lang/Object;
.source "TypeUtils.kt"


# direct methods
.method public static final a(Lkotlin/reflect/jvm/internal/impl/k/r;)Lkotlin/reflect/jvm/internal/impl/a/l;
    .locals 2

    .prologue
    const-string v0, "$receiver"

    invoke-static {p0, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 46
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/k/r;->i()Lkotlin/reflect/jvm/internal/impl/k/ad;

    move-result-object v0

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/k/ad;->f()Lkotlin/reflect/jvm/internal/impl/a/l;

    move-result-object v0

    const-string v1, "constructor.builtIns"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public static final a(Lkotlin/reflect/jvm/internal/impl/k/r;Lkotlin/reflect/jvm/internal/impl/k/ap;Lkotlin/reflect/jvm/internal/impl/b/aq;)Lkotlin/reflect/jvm/internal/impl/k/ah;
    .locals 2

    .prologue
    const-string v0, "type"

    invoke-static {p0, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "projectionKind"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 112
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/k/aj;

    if-eqz p2, :cond_1

    invoke-interface {p2}, Lkotlin/reflect/jvm/internal/impl/b/aq;->k()Lkotlin/reflect/jvm/internal/impl/k/ap;

    move-result-object v1

    :goto_0
    invoke-static {v1, p1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    sget-object p1, Lkotlin/reflect/jvm/internal/impl/k/ap;->a:Lkotlin/reflect/jvm/internal/impl/k/ap;

    :cond_0
    invoke-direct {v0, p1, p0}, Lkotlin/reflect/jvm/internal/impl/k/aj;-><init>(Lkotlin/reflect/jvm/internal/impl/k/ap;Lkotlin/reflect/jvm/internal/impl/k/r;)V

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/ah;

    return-object v0

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static final a(Lkotlin/reflect/jvm/internal/impl/k/r;Lkotlin/reflect/jvm/internal/impl/b/a/h;)Lkotlin/reflect/jvm/internal/impl/k/r;
    .locals 1

    .prologue
    const-string v0, "$receiver"

    invoke-static {p0, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "newAnnotations"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 95
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/k/r;->w()Lkotlin/reflect/jvm/internal/impl/b/a/h;

    move-result-object v0

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/b/a/h;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/b/a/h;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 96
    :goto_0
    return-object p0

    :cond_0
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/k/r;->j()Lkotlin/reflect/jvm/internal/impl/k/ao;

    move-result-object v0

    invoke-virtual {v0, p1}, Lkotlin/reflect/jvm/internal/impl/k/ao;->b(Lkotlin/reflect/jvm/internal/impl/b/a/h;)Lkotlin/reflect/jvm/internal/impl/k/ao;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/r;

    move-object p0, v0

    goto :goto_0
.end method

.method private static final a(Lkotlin/reflect/jvm/internal/impl/k/v;)Lkotlin/reflect/jvm/internal/impl/k/v;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 179
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/k/v;->i()Lkotlin/reflect/jvm/internal/impl/k/ad;

    move-result-object v0

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/k/ad;->b()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/k/v;->i()Lkotlin/reflect/jvm/internal/impl/k/ad;

    move-result-object v0

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/k/ad;->d()Lkotlin/reflect/jvm/internal/impl/b/h;

    move-result-object v0

    if-nez v0, :cond_1

    .line 183
    :cond_0
    :goto_0
    return-object p0

    .line 181
    :cond_1
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/k/v;->i()Lkotlin/reflect/jvm/internal/impl/k/ad;

    move-result-object v0

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/k/ad;->b()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 232
    new-instance v1, Ljava/util/ArrayList;

    const/16 v2, 0xa

    invoke-static {v0, v2}, Lkotlin/a/m;->a(Ljava/lang/Iterable;I)I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v1, Ljava/util/Collection;

    .line 233
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .line 234
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/aq;

    new-instance v3, Lkotlin/reflect/jvm/internal/impl/k/y;

    .line 181
    invoke-direct {v3, v0}, Lkotlin/reflect/jvm/internal/impl/k/y;-><init>(Lkotlin/reflect/jvm/internal/impl/b/aq;)V

    invoke-interface {v1, v3}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 235
    :cond_2
    check-cast v1, Ljava/util/List;

    .line 183
    const/4 v0, 0x2

    invoke-static {p0, v1, v4, v0, v4}, Lkotlin/reflect/jvm/internal/impl/k/al;->a(Lkotlin/reflect/jvm/internal/impl/k/v;Ljava/util/List;Lkotlin/reflect/jvm/internal/impl/b/a/h;ILjava/lang/Object;)Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object p0

    goto :goto_0
.end method

.method public static final a(Lkotlin/reflect/jvm/internal/impl/k/r;Lkotlin/reflect/jvm/internal/impl/k/r;)Z
    .locals 1

    .prologue
    const-string v0, "$receiver"

    invoke-static {p0, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "superType"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 80
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/k/a/c;->a:Lkotlin/reflect/jvm/internal/impl/k/a/c;

    invoke-interface {v0, p0, p1}, Lkotlin/reflect/jvm/internal/impl/k/a/c;->a(Lkotlin/reflect/jvm/internal/impl/k/r;Lkotlin/reflect/jvm/internal/impl/k/r;)Z

    move-result v0

    return v0
.end method

.method public static final b(Lkotlin/reflect/jvm/internal/impl/k/r;)Lkotlin/reflect/jvm/internal/impl/k/r;
    .locals 1

    .prologue
    const-string v0, "$receiver"

    invoke-static {p0, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 48
    invoke-static {p0}, Lkotlin/reflect/jvm/internal/impl/k/an;->b(Lkotlin/reflect/jvm/internal/impl/k/r;)Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v0

    return-object v0
.end method

.method public static final c(Lkotlin/reflect/jvm/internal/impl/k/r;)Lkotlin/reflect/jvm/internal/impl/k/r;
    .locals 1

    .prologue
    const-string v0, "$receiver"

    invoke-static {p0, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 49
    invoke-static {p0}, Lkotlin/reflect/jvm/internal/impl/k/an;->c(Lkotlin/reflect/jvm/internal/impl/k/r;)Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v0

    return-object v0
.end method

.method public static final d(Lkotlin/reflect/jvm/internal/impl/k/r;)Z
    .locals 1

    .prologue
    const-string v0, "$receiver"

    invoke-static {p0, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 68
    invoke-static {p0}, Lkotlin/reflect/jvm/internal/impl/k/an;->i(Lkotlin/reflect/jvm/internal/impl/k/r;)Z

    move-result v0

    return v0
.end method

.method public static final e(Lkotlin/reflect/jvm/internal/impl/k/r;)Lkotlin/reflect/jvm/internal/impl/k/ah;
    .locals 1

    .prologue
    const-string v0, "$receiver"

    invoke-static {p0, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 164
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/k/aj;

    invoke-direct {v0, p0}, Lkotlin/reflect/jvm/internal/impl/k/aj;-><init>(Lkotlin/reflect/jvm/internal/impl/k/r;)V

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/ah;

    return-object v0
.end method

.method public static final f(Lkotlin/reflect/jvm/internal/impl/k/r;)Lkotlin/reflect/jvm/internal/impl/k/r;
    .locals 2

    .prologue
    const-string v0, "$receiver"

    invoke-static {p0, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 168
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/k/r;->j()Lkotlin/reflect/jvm/internal/impl/k/ao;

    move-result-object v1

    .line 170
    instance-of v0, v1, Lkotlin/reflect/jvm/internal/impl/k/m;

    if-eqz v0, :cond_0

    move-object v0, v1

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/m;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/k/m;->g()Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v0

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/k/c/a;->a(Lkotlin/reflect/jvm/internal/impl/k/v;)Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v0

    check-cast v1, Lkotlin/reflect/jvm/internal/impl/k/m;

    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/impl/k/m;->h()Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v1

    invoke-static {v1}, Lkotlin/reflect/jvm/internal/impl/k/c/a;->a(Lkotlin/reflect/jvm/internal/impl/k/v;)Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/reflect/jvm/internal/impl/k/s;->a(Lkotlin/reflect/jvm/internal/impl/k/v;Lkotlin/reflect/jvm/internal/impl/k/v;)Lkotlin/reflect/jvm/internal/impl/k/ao;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/r;

    .line 169
    :goto_0
    return-object v0

    .line 174
    :cond_0
    instance-of v0, v1, Lkotlin/reflect/jvm/internal/impl/k/v;

    if-eqz v0, :cond_1

    check-cast v1, Lkotlin/reflect/jvm/internal/impl/k/v;

    invoke-static {v1}, Lkotlin/reflect/jvm/internal/impl/k/c/a;->a(Lkotlin/reflect/jvm/internal/impl/k/v;)Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/r;

    goto :goto_0

    :cond_1
    new-instance v0, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {v0}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw v0
.end method
