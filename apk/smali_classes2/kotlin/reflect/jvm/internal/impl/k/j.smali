.class public final Lkotlin/reflect/jvm/internal/impl/k/j;
.super Lkotlin/reflect/jvm/internal/impl/k/m;
.source "dynamicTypes.kt"


# instance fields
.field private final a:Lkotlin/reflect/jvm/internal/impl/b/a/h;


# direct methods
.method public constructor <init>(Lkotlin/reflect/jvm/internal/impl/a/l;Lkotlin/reflect/jvm/internal/impl/b/a/h;)V
    .locals 3

    .prologue
    const-string v0, "builtIns"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "annotations"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 39
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/a/l;->v()Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v0

    const-string v1, "builtIns.nothingType"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/a/l;->y()Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v1

    const-string v2, "builtIns.nullableAnyType"

    invoke-static {v1, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, v0, v1}, Lkotlin/reflect/jvm/internal/impl/k/m;-><init>(Lkotlin/reflect/jvm/internal/impl/k/v;Lkotlin/reflect/jvm/internal/impl/k/v;)V

    iput-object p2, p0, Lkotlin/reflect/jvm/internal/impl/k/j;->a:Lkotlin/reflect/jvm/internal/impl/b/a/h;

    return-void
.end method


# virtual methods
.method public J_()Lkotlin/reflect/jvm/internal/impl/k/v;
    .locals 1

    .prologue
    .line 40
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/k/j;->h()Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v0

    return-object v0
.end method

.method public a(Lkotlin/reflect/jvm/internal/impl/g/c;Lkotlin/reflect/jvm/internal/impl/g/h;)Ljava/lang/String;
    .locals 1

    .prologue
    const-string v0, "renderer"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "options"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 49
    const-string v0, "dynamic"

    return-object v0
.end method

.method public a(Lkotlin/reflect/jvm/internal/impl/b/a/h;)Lkotlin/reflect/jvm/internal/impl/k/j;
    .locals 3

    .prologue
    const-string v0, "newAnnotations"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 47
    new-instance v1, Lkotlin/reflect/jvm/internal/impl/k/j;

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/k/j;->J_()Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/r;

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/k/c/a;->a(Lkotlin/reflect/jvm/internal/impl/k/r;)Lkotlin/reflect/jvm/internal/impl/a/l;

    move-result-object v0

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/k/j;->w()Lkotlin/reflect/jvm/internal/impl/b/a/h;

    move-result-object v2

    invoke-direct {v1, v0, v2}, Lkotlin/reflect/jvm/internal/impl/k/j;-><init>(Lkotlin/reflect/jvm/internal/impl/a/l;Lkotlin/reflect/jvm/internal/impl/b/a/h;)V

    return-object v1
.end method

.method public a(Z)Lkotlin/reflect/jvm/internal/impl/k/j;
    .locals 0

    .prologue
    .line 43
    return-object p0
.end method

.method public synthetic b(Lkotlin/reflect/jvm/internal/impl/b/a/h;)Lkotlin/reflect/jvm/internal/impl/k/ao;
    .locals 1

    .prologue
    .line 39
    invoke-virtual {p0, p1}, Lkotlin/reflect/jvm/internal/impl/k/j;->a(Lkotlin/reflect/jvm/internal/impl/b/a/h;)Lkotlin/reflect/jvm/internal/impl/k/j;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/ao;

    return-object v0
.end method

.method public synthetic b(Z)Lkotlin/reflect/jvm/internal/impl/k/ao;
    .locals 1

    .prologue
    .line 39
    invoke-virtual {p0, p1}, Lkotlin/reflect/jvm/internal/impl/k/j;->a(Z)Lkotlin/reflect/jvm/internal/impl/k/j;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/ao;

    return-object v0
.end method

.method public c()Z
    .locals 1

    .prologue
    .line 45
    const/4 v0, 0x0

    return v0
.end method

.method public w()Lkotlin/reflect/jvm/internal/impl/b/a/h;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/k/j;->a:Lkotlin/reflect/jvm/internal/impl/b/a/h;

    return-object v0
.end method
