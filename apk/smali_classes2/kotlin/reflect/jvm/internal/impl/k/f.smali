.class public Lkotlin/reflect/jvm/internal/impl/k/f;
.super Lkotlin/reflect/jvm/internal/impl/k/ak;
.source "TypeSubstitution.kt"


# instance fields
.field private final a:Lkotlin/reflect/jvm/internal/impl/k/ak;


# direct methods
.method public constructor <init>(Lkotlin/reflect/jvm/internal/impl/k/ak;)V
    .locals 1

    .prologue
    const-string v0, "substitution"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 155
    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/k/ak;-><init>()V

    iput-object p1, p0, Lkotlin/reflect/jvm/internal/impl/k/f;->a:Lkotlin/reflect/jvm/internal/impl/k/ak;

    return-void
.end method


# virtual methods
.method public a(Lkotlin/reflect/jvm/internal/impl/b/a/h;)Lkotlin/reflect/jvm/internal/impl/b/a/h;
    .locals 1

    .prologue
    const-string v0, "annotations"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 164
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/k/f;->a:Lkotlin/reflect/jvm/internal/impl/k/ak;

    invoke-virtual {v0, p1}, Lkotlin/reflect/jvm/internal/impl/k/ak;->a(Lkotlin/reflect/jvm/internal/impl/b/a/h;)Lkotlin/reflect/jvm/internal/impl/b/a/h;

    move-result-object v0

    return-object v0
.end method

.method public a(Lkotlin/reflect/jvm/internal/impl/k/r;Lkotlin/reflect/jvm/internal/impl/k/ap;)Lkotlin/reflect/jvm/internal/impl/k/r;
    .locals 1

    .prologue
    const-string v0, "topLevelType"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "position"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 157
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/k/f;->a:Lkotlin/reflect/jvm/internal/impl/k/ak;

    invoke-virtual {v0, p1, p2}, Lkotlin/reflect/jvm/internal/impl/k/ak;->a(Lkotlin/reflect/jvm/internal/impl/k/r;Lkotlin/reflect/jvm/internal/impl/k/ap;)Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v0

    return-object v0
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 159
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/k/f;->a:Lkotlin/reflect/jvm/internal/impl/k/ak;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/k/ak;->a()Z

    move-result v0

    return v0
.end method

.method public b(Lkotlin/reflect/jvm/internal/impl/k/r;)Lkotlin/reflect/jvm/internal/impl/k/ah;
    .locals 1

    .prologue
    const-string v0, "key"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 156
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/k/f;->a:Lkotlin/reflect/jvm/internal/impl/k/ak;

    invoke-virtual {v0, p1}, Lkotlin/reflect/jvm/internal/impl/k/ak;->b(Lkotlin/reflect/jvm/internal/impl/k/r;)Lkotlin/reflect/jvm/internal/impl/k/ah;

    move-result-object v0

    return-object v0
.end method

.method public b()Z
    .locals 1

    .prologue
    .line 162
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/k/f;->a:Lkotlin/reflect/jvm/internal/impl/k/ak;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/k/ak;->b()Z

    move-result v0

    return v0
.end method

.method public c()Z
    .locals 1

    .prologue
    .line 161
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/k/f;->a:Lkotlin/reflect/jvm/internal/impl/k/ak;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/k/ak;->c()Z

    move-result v0

    return v0
.end method
