.class final Lkotlin/reflect/jvm/internal/impl/k/a/h$f;
.super Lkotlin/d/b/k;
.source "NewKotlinTypeChecker.kt"

# interfaces
.implements Lkotlin/d/a/b;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lkotlin/reflect/jvm/internal/impl/k/a/h;->a(Lkotlin/reflect/jvm/internal/impl/k/a/n;Lkotlin/reflect/jvm/internal/impl/k/v;)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/d/b/k;",
        "Lkotlin/d/a/b",
        "<",
        "Lkotlin/reflect/jvm/internal/impl/k/r;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:Lkotlin/reflect/jvm/internal/impl/k/a/h$f;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lkotlin/reflect/jvm/internal/impl/k/a/h$f;

    invoke-direct {v0}, Lkotlin/reflect/jvm/internal/impl/k/a/h$f;-><init>()V

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/k/a/h$f;->a:Lkotlin/reflect/jvm/internal/impl/k/a/h$f;

    return-void
.end method

.method constructor <init>()V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lkotlin/d/b/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 79
    check-cast p1, Lkotlin/reflect/jvm/internal/impl/k/r;

    invoke-virtual {p0, p1}, Lkotlin/reflect/jvm/internal/impl/k/a/h$f;->a(Lkotlin/reflect/jvm/internal/impl/k/r;)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public final a()Lkotlin/reflect/e;
    .locals 1

    const-class v0, Lkotlin/reflect/jvm/internal/impl/a/l;

    invoke-static {v0}, Lkotlin/d/b/y;->a(Ljava/lang/Class;)Lkotlin/reflect/c;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lkotlin/reflect/jvm/internal/impl/k/r;)Z
    .locals 1

    .prologue
    const-string v0, "p1"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 165
    invoke-static {p1}, Lkotlin/reflect/jvm/internal/impl/a/l;->g(Lkotlin/reflect/jvm/internal/impl/k/r;)Z

    move-result v0

    return v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    const-string v0, "isNothingOrNullableNothing"

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    const-string v0, "isNothingOrNullableNothing(Lorg/jetbrains/kotlin/types/KotlinType;)Z"

    return-object v0
.end method
