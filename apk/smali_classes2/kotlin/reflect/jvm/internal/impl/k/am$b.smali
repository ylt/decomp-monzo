.class final enum Lkotlin/reflect/jvm/internal/impl/k/am$b;
.super Ljava/lang/Enum;
.source "TypeSubstitutor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lkotlin/reflect/jvm/internal/impl/k/am;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lkotlin/reflect/jvm/internal/impl/k/am$b;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lkotlin/reflect/jvm/internal/impl/k/am$b;

.field public static final enum b:Lkotlin/reflect/jvm/internal/impl/k/am$b;

.field public static final enum c:Lkotlin/reflect/jvm/internal/impl/k/am$b;

.field private static final synthetic d:[Lkotlin/reflect/jvm/internal/impl/k/am$b;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 295
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/k/am$b;

    const-string v1, "NO_CONFLICT"

    invoke-direct {v0, v1, v2}, Lkotlin/reflect/jvm/internal/impl/k/am$b;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/k/am$b;->a:Lkotlin/reflect/jvm/internal/impl/k/am$b;

    .line 296
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/k/am$b;

    const-string v1, "IN_IN_OUT_POSITION"

    invoke-direct {v0, v1, v3}, Lkotlin/reflect/jvm/internal/impl/k/am$b;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/k/am$b;->b:Lkotlin/reflect/jvm/internal/impl/k/am$b;

    .line 297
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/k/am$b;

    const-string v1, "OUT_IN_IN_POSITION"

    invoke-direct {v0, v1, v4}, Lkotlin/reflect/jvm/internal/impl/k/am$b;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/k/am$b;->c:Lkotlin/reflect/jvm/internal/impl/k/am$b;

    .line 294
    const/4 v0, 0x3

    new-array v0, v0, [Lkotlin/reflect/jvm/internal/impl/k/am$b;

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/k/am$b;->a:Lkotlin/reflect/jvm/internal/impl/k/am$b;

    aput-object v1, v0, v2

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/k/am$b;->b:Lkotlin/reflect/jvm/internal/impl/k/am$b;

    aput-object v1, v0, v3

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/k/am$b;->c:Lkotlin/reflect/jvm/internal/impl/k/am$b;

    aput-object v1, v0, v4

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/k/am$b;->d:[Lkotlin/reflect/jvm/internal/impl/k/am$b;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 294
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lkotlin/reflect/jvm/internal/impl/k/am$b;
    .locals 1

    .prologue
    .line 294
    const-class v0, Lkotlin/reflect/jvm/internal/impl/k/am$b;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/am$b;

    return-object v0
.end method

.method public static values()[Lkotlin/reflect/jvm/internal/impl/k/am$b;
    .locals 1

    .prologue
    .line 294
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/k/am$b;->d:[Lkotlin/reflect/jvm/internal/impl/k/am$b;

    invoke-virtual {v0}, [Lkotlin/reflect/jvm/internal/impl/k/am$b;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lkotlin/reflect/jvm/internal/impl/k/am$b;

    return-object v0
.end method
