.class public abstract Lkotlin/reflect/jvm/internal/impl/k/m;
.super Lkotlin/reflect/jvm/internal/impl/k/ao;
.source "KotlinType.kt"

# interfaces
.implements Lkotlin/reflect/jvm/internal/impl/k/aa;


# instance fields
.field private final a:Lkotlin/reflect/jvm/internal/impl/k/v;

.field private final b:Lkotlin/reflect/jvm/internal/impl/k/v;


# direct methods
.method public constructor <init>(Lkotlin/reflect/jvm/internal/impl/k/v;Lkotlin/reflect/jvm/internal/impl/k/v;)V
    .locals 1

    .prologue
    const-string v0, "lowerBound"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "upperBound"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 140
    const/4 v0, 0x0

    .line 141
    invoke-direct {p0, v0}, Lkotlin/reflect/jvm/internal/impl/k/ao;-><init>(Lkotlin/d/b/i;)V

    iput-object p1, p0, Lkotlin/reflect/jvm/internal/impl/k/m;->a:Lkotlin/reflect/jvm/internal/impl/k/v;

    iput-object p2, p0, Lkotlin/reflect/jvm/internal/impl/k/m;->b:Lkotlin/reflect/jvm/internal/impl/k/v;

    return-void
.end method


# virtual methods
.method public abstract J_()Lkotlin/reflect/jvm/internal/impl/k/v;
.end method

.method public abstract a(Lkotlin/reflect/jvm/internal/impl/g/c;Lkotlin/reflect/jvm/internal/impl/g/h;)Ljava/lang/String;
.end method

.method public a()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/k/ah;",
            ">;"
        }
    .end annotation

    .prologue
    .line 156
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/k/m;->J_()Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/k/v;->a()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public a(Lkotlin/reflect/jvm/internal/impl/k/r;)Z
    .locals 1

    .prologue
    const-string v0, "type"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 150
    const/4 v0, 0x0

    return v0
.end method

.method public b()Lkotlin/reflect/jvm/internal/impl/h/e/h;
    .locals 1

    .prologue
    .line 158
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/k/m;->J_()Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/k/v;->b()Lkotlin/reflect/jvm/internal/impl/h/e/h;

    move-result-object v0

    return-object v0
.end method

.method public c()Z
    .locals 1

    .prologue
    .line 157
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/k/m;->J_()Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/k/v;->c()Z

    move-result v0

    return v0
.end method

.method public d()Z
    .locals 1

    .prologue
    .line 159
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/k/m;->J_()Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/k/v;->d()Z

    move-result v0

    return v0
.end method

.method public e()Lkotlin/reflect/jvm/internal/impl/k/r;
    .locals 1

    .prologue
    .line 146
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/k/m;->a:Lkotlin/reflect/jvm/internal/impl/k/v;

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/r;

    return-object v0
.end method

.method public f()Lkotlin/reflect/jvm/internal/impl/k/r;
    .locals 1

    .prologue
    .line 148
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/k/m;->b:Lkotlin/reflect/jvm/internal/impl/k/v;

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/r;

    return-object v0
.end method

.method public final g()Lkotlin/reflect/jvm/internal/impl/k/v;
    .locals 1

    .prologue
    .line 140
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/k/m;->a:Lkotlin/reflect/jvm/internal/impl/k/v;

    return-object v0
.end method

.method public final h()Lkotlin/reflect/jvm/internal/impl/k/v;
    .locals 1

    .prologue
    .line 140
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/k/m;->b:Lkotlin/reflect/jvm/internal/impl/k/v;

    return-object v0
.end method

.method public i()Lkotlin/reflect/jvm/internal/impl/k/ad;
    .locals 1

    .prologue
    .line 155
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/k/m;->J_()Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/k/v;->i()Lkotlin/reflect/jvm/internal/impl/k/ad;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 161
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/g/c;->g:Lkotlin/reflect/jvm/internal/impl/g/c;

    check-cast p0, Lkotlin/reflect/jvm/internal/impl/k/r;

    invoke-virtual {v0, p0}, Lkotlin/reflect/jvm/internal/impl/g/c;->a(Lkotlin/reflect/jvm/internal/impl/k/r;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public w()Lkotlin/reflect/jvm/internal/impl/b/a/h;
    .locals 1

    .prologue
    .line 154
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/k/m;->J_()Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/k/v;->w()Lkotlin/reflect/jvm/internal/impl/b/a/h;

    move-result-object v0

    return-object v0
.end method
