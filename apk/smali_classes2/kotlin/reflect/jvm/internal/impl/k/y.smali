.class public final Lkotlin/reflect/jvm/internal/impl/k/y;
.super Lkotlin/reflect/jvm/internal/impl/k/ai;
.source "StarProjectionImpl.kt"


# static fields
.field static final synthetic a:[Lkotlin/reflect/l;


# instance fields
.field private final b:Lkotlin/c;

.field private final c:Lkotlin/reflect/jvm/internal/impl/b/aq;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const/4 v0, 0x1

    new-array v1, v0, [Lkotlin/reflect/l;

    const/4 v2, 0x0

    new-instance v0, Lkotlin/d/b/w;

    const-class v3, Lkotlin/reflect/jvm/internal/impl/k/y;

    invoke-static {v3}, Lkotlin/d/b/y;->a(Ljava/lang/Class;)Lkotlin/reflect/c;

    move-result-object v3

    const-string v4, "_type"

    const-string v5, "get_type()Lorg/jetbrains/kotlin/types/KotlinType;"

    invoke-direct {v0, v3, v4, v5}, Lkotlin/d/b/w;-><init>(Lkotlin/reflect/e;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v0}, Lkotlin/d/b/y;->a(Lkotlin/d/b/v;)Lkotlin/reflect/n;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/l;

    aput-object v0, v1, v2

    sput-object v1, Lkotlin/reflect/jvm/internal/impl/k/y;->a:[Lkotlin/reflect/l;

    return-void
.end method

.method public constructor <init>(Lkotlin/reflect/jvm/internal/impl/b/aq;)V
    .locals 2

    .prologue
    const-string v0, "typeParameter"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 26
    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/k/ai;-><init>()V

    iput-object p1, p0, Lkotlin/reflect/jvm/internal/impl/k/y;->c:Lkotlin/reflect/jvm/internal/impl/b/aq;

    .line 32
    sget-object v1, Lkotlin/f;->b:Lkotlin/f;

    new-instance v0, Lkotlin/reflect/jvm/internal/impl/k/y$a;

    invoke-direct {v0, p0}, Lkotlin/reflect/jvm/internal/impl/k/y$a;-><init>(Lkotlin/reflect/jvm/internal/impl/k/y;)V

    check-cast v0, Lkotlin/d/a/a;

    invoke-static {v1, v0}, Lkotlin/d;->a(Lkotlin/f;Lkotlin/d/a/a;)Lkotlin/c;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/k/y;->b:Lkotlin/c;

    return-void
.end method

.method public static final synthetic a(Lkotlin/reflect/jvm/internal/impl/k/y;)Lkotlin/reflect/jvm/internal/impl/b/aq;
    .locals 1

    .prologue
    .line 24
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/k/y;->c:Lkotlin/reflect/jvm/internal/impl/b/aq;

    return-object v0
.end method

.method private final d()Lkotlin/reflect/jvm/internal/impl/k/r;
    .locals 3

    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/k/y;->b:Lkotlin/c;

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/k/y;->a:[Lkotlin/reflect/l;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-interface {v0}, Lkotlin/c;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/r;

    return-object v0
.end method


# virtual methods
.method public a()Z
    .locals 1

    .prologue
    .line 27
    const/4 v0, 0x1

    return v0
.end method

.method public b()Lkotlin/reflect/jvm/internal/impl/k/ap;
    .locals 1

    .prologue
    .line 29
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/k/ap;->c:Lkotlin/reflect/jvm/internal/impl/k/ap;

    return-object v0
.end method

.method public c()Lkotlin/reflect/jvm/internal/impl/k/r;
    .locals 1

    .prologue
    .line 36
    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/k/y;->d()Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v0

    return-object v0
.end method
