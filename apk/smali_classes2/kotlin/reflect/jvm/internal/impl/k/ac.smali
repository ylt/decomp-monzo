.class public final Lkotlin/reflect/jvm/internal/impl/k/ac;
.super Ljava/lang/Object;
.source "TypeCapabilities.kt"


# direct methods
.method public static final a(Lkotlin/reflect/jvm/internal/impl/k/r;)Z
    .locals 2

    .prologue
    const-string v0, "$receiver"

    invoke-static {p0, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 30
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/k/r;->j()Lkotlin/reflect/jvm/internal/impl/k/ao;

    move-result-object v0

    instance-of v1, v0, Lkotlin/reflect/jvm/internal/impl/k/e;

    if-nez v1, :cond_0

    const/4 v0, 0x0

    :cond_0
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/e;

    if-eqz v0, :cond_1

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/k/e;->M_()Z

    move-result v0

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static final a(Lkotlin/reflect/jvm/internal/impl/k/r;Lkotlin/reflect/jvm/internal/impl/k/r;)Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x0

    const-string v0, "first"

    invoke-static {p0, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "second"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 50
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/k/r;->j()Lkotlin/reflect/jvm/internal/impl/k/ao;

    move-result-object v0

    instance-of v3, v0, Lkotlin/reflect/jvm/internal/impl/k/aa;

    if-nez v3, :cond_0

    move-object v0, v1

    :cond_0
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/aa;

    if-eqz v0, :cond_3

    invoke-interface {v0, p1}, Lkotlin/reflect/jvm/internal/impl/k/aa;->a(Lkotlin/reflect/jvm/internal/impl/k/r;)Z

    move-result v0

    :goto_0
    if-nez v0, :cond_2

    .line 51
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/k/r;->j()Lkotlin/reflect/jvm/internal/impl/k/ao;

    move-result-object v0

    instance-of v3, v0, Lkotlin/reflect/jvm/internal/impl/k/aa;

    if-nez v3, :cond_1

    move-object v0, v1

    :cond_1
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/aa;

    if-eqz v0, :cond_4

    invoke-interface {v0, p0}, Lkotlin/reflect/jvm/internal/impl/k/aa;->a(Lkotlin/reflect/jvm/internal/impl/k/r;)Z

    move-result v0

    :goto_1
    if-eqz v0, :cond_5

    :cond_2
    const/4 v0, 0x1

    :goto_2
    return v0

    :cond_3
    move v0, v2

    .line 50
    goto :goto_0

    :cond_4
    move v0, v2

    .line 51
    goto :goto_1

    :cond_5
    move v0, v2

    goto :goto_2
.end method

.method public static final b(Lkotlin/reflect/jvm/internal/impl/k/r;)Lkotlin/reflect/jvm/internal/impl/k/e;
    .locals 3

    .prologue
    const/4 v1, 0x0

    const-string v0, "$receiver"

    invoke-static {p0, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 32
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/k/r;->j()Lkotlin/reflect/jvm/internal/impl/k/ao;

    move-result-object v0

    instance-of v2, v0, Lkotlin/reflect/jvm/internal/impl/k/e;

    if-nez v2, :cond_0

    move-object v0, v1

    :cond_0
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/e;

    if-eqz v0, :cond_2

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/e;

    .line 33
    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/k/e;->M_()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 32
    :goto_0
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/e;

    .line 34
    :goto_1
    return-object v0

    :cond_1
    move-object v0, v1

    .line 33
    goto :goto_0

    :cond_2
    move-object v0, v1

    .line 32
    goto :goto_1
.end method

.method public static final c(Lkotlin/reflect/jvm/internal/impl/k/r;)Lkotlin/reflect/jvm/internal/impl/k/r;
    .locals 2

    .prologue
    const-string v0, "$receiver"

    invoke-static {p0, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 44
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/k/r;->j()Lkotlin/reflect/jvm/internal/impl/k/ao;

    move-result-object v0

    instance-of v1, v0, Lkotlin/reflect/jvm/internal/impl/k/aa;

    if-nez v1, :cond_0

    const/4 v0, 0x0

    :cond_0
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/aa;

    if-eqz v0, :cond_1

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/k/aa;->e()Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v0

    if-eqz v0, :cond_1

    move-object p0, v0

    :cond_1
    return-object p0
.end method

.method public static final d(Lkotlin/reflect/jvm/internal/impl/k/r;)Lkotlin/reflect/jvm/internal/impl/k/r;
    .locals 2

    .prologue
    const-string v0, "$receiver"

    invoke-static {p0, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 47
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/k/r;->j()Lkotlin/reflect/jvm/internal/impl/k/ao;

    move-result-object v0

    instance-of v1, v0, Lkotlin/reflect/jvm/internal/impl/k/aa;

    if-nez v1, :cond_0

    const/4 v0, 0x0

    :cond_0
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/aa;

    if-eqz v0, :cond_1

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/k/aa;->f()Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v0

    if-eqz v0, :cond_1

    move-object p0, v0

    :cond_1
    return-object p0
.end method
