.class public abstract Lkotlin/reflect/jvm/internal/impl/k/ai;
.super Ljava/lang/Object;
.source "TypeProjectionBase.java"

# interfaces
.implements Lkotlin/reflect/jvm/internal/impl/k/ah;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 34
    if-ne p0, p1, :cond_1

    .line 43
    :cond_0
    :goto_0
    return v0

    .line 35
    :cond_1
    instance-of v2, p1, Lkotlin/reflect/jvm/internal/impl/k/ah;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    .line 37
    :cond_2
    check-cast p1, Lkotlin/reflect/jvm/internal/impl/k/ah;

    .line 39
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/k/ai;->a()Z

    move-result v2

    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/k/ah;->a()Z

    move-result v3

    if-eq v2, v3, :cond_3

    move v0, v1

    goto :goto_0

    .line 40
    :cond_3
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/k/ai;->b()Lkotlin/reflect/jvm/internal/impl/k/ap;

    move-result-object v2

    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/k/ah;->b()Lkotlin/reflect/jvm/internal/impl/k/ap;

    move-result-object v3

    if-eq v2, v3, :cond_4

    move v0, v1

    goto :goto_0

    .line 41
    :cond_4
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/k/ai;->c()Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v2

    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/k/ah;->c()Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v3

    invoke-virtual {v2, v3}, Lkotlin/reflect/jvm/internal/impl/k/r;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 48
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/k/ai;->b()Lkotlin/reflect/jvm/internal/impl/k/ap;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/k/ap;->hashCode()I

    move-result v0

    .line 49
    mul-int/lit8 v1, v0, 0x1f

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/k/ai;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x11

    :goto_0
    add-int/2addr v0, v1

    .line 50
    return v0

    .line 49
    :cond_0
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/k/ai;->c()Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/k/r;->hashCode()I

    move-result v0

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 23
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/k/ai;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 24
    const-string v0, "*"

    .line 29
    :goto_0
    return-object v0

    .line 26
    :cond_0
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/k/ai;->b()Lkotlin/reflect/jvm/internal/impl/k/ap;

    move-result-object v0

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/k/ap;->a:Lkotlin/reflect/jvm/internal/impl/k/ap;

    if-ne v0, v1, :cond_1

    .line 27
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/k/ai;->c()Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 29
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/k/ai;->b()Lkotlin/reflect/jvm/internal/impl/k/ap;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/k/ai;->c()Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
