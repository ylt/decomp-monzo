.class public final Lkotlin/reflect/jvm/internal/impl/k/ae$a;
.super Ljava/lang/Object;
.source "TypeSubstitution.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lkotlin/reflect/jvm/internal/impl/k/ae;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 51
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/d/b/i;)V
    .locals 0

    .prologue
    .line 51
    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/k/ae$a;-><init>()V

    return-void
.end method

.method public static bridge synthetic a(Lkotlin/reflect/jvm/internal/impl/k/ae$a;Ljava/util/Map;ZILjava/lang/Object;)Lkotlin/reflect/jvm/internal/impl/k/ae;
    .locals 1

    .prologue
    and-int/lit8 v0, p3, 0x2

    if-eqz v0, :cond_0

    .line 56
    const/4 p2, 0x0

    :cond_0
    invoke-virtual {p0, p1, p2}, Lkotlin/reflect/jvm/internal/impl/k/ae$a;->a(Ljava/util/Map;Z)Lkotlin/reflect/jvm/internal/impl/k/ae;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(Ljava/util/Map;Z)Lkotlin/reflect/jvm/internal/impl/k/ae;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/k/ad;",
            "+",
            "Lkotlin/reflect/jvm/internal/impl/k/ah;",
            ">;Z)",
            "Lkotlin/reflect/jvm/internal/impl/k/ae;"
        }
    .end annotation

    .prologue
    const-string v0, "map"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 58
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/k/ae$a$a;

    invoke-direct {v0, p1, p2}, Lkotlin/reflect/jvm/internal/impl/k/ae$a$a;-><init>(Ljava/util/Map;Z)V

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/ae;

    .line 62
    return-object v0
.end method

.method public final a(Lkotlin/reflect/jvm/internal/impl/k/ad;Ljava/util/List;)Lkotlin/reflect/jvm/internal/impl/k/ak;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/reflect/jvm/internal/impl/k/ad;",
            "Ljava/util/List",
            "<+",
            "Lkotlin/reflect/jvm/internal/impl/k/ah;",
            ">;)",
            "Lkotlin/reflect/jvm/internal/impl/k/ak;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    const-string v0, "typeConstructor"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "arguments"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 73
    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/k/ad;->b()Ljava/util/List;

    move-result-object v1

    .line 75
    invoke-static {v1}, Lkotlin/a/m;->h(Ljava/util/List;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/aq;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/b/aq;->a()Z

    move-result v0

    :goto_0
    if-eqz v0, :cond_2

    .line 76
    check-cast p0, Lkotlin/reflect/jvm/internal/impl/k/ae$a;

    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/k/ad;->b()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 172
    new-instance v1, Ljava/util/ArrayList;

    const/16 v3, 0xa

    invoke-static {v0, v3}, Lkotlin/a/m;->a(Ljava/lang/Iterable;I)I

    move-result v3

    invoke-direct {v1, v3}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v1, Ljava/util/Collection;

    .line 173
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .line 174
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/aq;

    .line 76
    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/b/aq;->e()Lkotlin/reflect/jvm/internal/impl/k/ad;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_0
    move v0, v2

    .line 75
    goto :goto_0

    .line 175
    :cond_1
    check-cast v1, Ljava/util/List;

    .line 76
    check-cast v1, Ljava/lang/Iterable;

    check-cast p2, Ljava/lang/Iterable;

    invoke-static {v1, p2}, Lkotlin/a/m;->a(Ljava/lang/Iterable;Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    invoke-static {v0}, Lkotlin/a/ab;->a(Ljava/lang/Iterable;)Ljava/util/Map;

    move-result-object v0

    const/4 v1, 0x2

    const/4 v3, 0x0

    invoke-static {p0, v0, v2, v1, v3}, Lkotlin/reflect/jvm/internal/impl/k/ae$a;->a(Lkotlin/reflect/jvm/internal/impl/k/ae$a;Ljava/util/Map;ZILjava/lang/Object;)Lkotlin/reflect/jvm/internal/impl/k/ae;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/ak;

    .line 79
    :goto_2
    return-object v0

    :cond_2
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/k/p;

    const-string v2, "parameters"

    invoke-static {v1, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v0, v1, p2}, Lkotlin/reflect/jvm/internal/impl/k/p;-><init>(Ljava/util/List;Ljava/util/List;)V

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/ak;

    goto :goto_2
.end method

.method public final a(Lkotlin/reflect/jvm/internal/impl/k/r;)Lkotlin/reflect/jvm/internal/impl/k/ak;
    .locals 2

    .prologue
    const-string v0, "kotlinType"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 70
    check-cast p0, Lkotlin/reflect/jvm/internal/impl/k/ae$a;

    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/k/r;->i()Lkotlin/reflect/jvm/internal/impl/k/ad;

    move-result-object v0

    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/k/r;->a()Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lkotlin/reflect/jvm/internal/impl/k/ae$a;->a(Lkotlin/reflect/jvm/internal/impl/k/ad;Ljava/util/List;)Lkotlin/reflect/jvm/internal/impl/k/ak;

    move-result-object v0

    return-object v0
.end method
