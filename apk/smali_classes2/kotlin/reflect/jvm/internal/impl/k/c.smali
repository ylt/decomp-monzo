.class public abstract Lkotlin/reflect/jvm/internal/impl/k/c;
.super Ljava/lang/Object;
.source "AbstractTypeConstructor.kt"

# interfaces
.implements Lkotlin/reflect/jvm/internal/impl/k/ad;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lkotlin/reflect/jvm/internal/impl/k/c$a;
    }
.end annotation


# instance fields
.field private final a:Lkotlin/reflect/jvm/internal/impl/j/f;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/reflect/jvm/internal/impl/j/f",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/k/c$a;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lkotlin/reflect/jvm/internal/impl/j/i;)V
    .locals 3

    .prologue
    const-string v0, "storageManager"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/k/c$b;

    invoke-direct {v0, p0}, Lkotlin/reflect/jvm/internal/impl/k/c$b;-><init>(Lkotlin/reflect/jvm/internal/impl/k/c;)V

    check-cast v0, Lkotlin/d/a/a;

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/k/c$c;->a:Lkotlin/reflect/jvm/internal/impl/k/c$c;

    check-cast v1, Lkotlin/d/a/b;

    new-instance v2, Lkotlin/reflect/jvm/internal/impl/k/c$d;

    invoke-direct {v2, p0}, Lkotlin/reflect/jvm/internal/impl/k/c$d;-><init>(Lkotlin/reflect/jvm/internal/impl/k/c;)V

    check-cast v2, Lkotlin/d/a/b;

    invoke-interface {p1, v0, v1, v2}, Lkotlin/reflect/jvm/internal/impl/j/i;->a(Lkotlin/d/a/a;Lkotlin/d/a/b;Lkotlin/d/a/b;)Lkotlin/reflect/jvm/internal/impl/j/f;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/k/c;->a:Lkotlin/reflect/jvm/internal/impl/j/f;

    return-void
.end method

.method private final a(Lkotlin/reflect/jvm/internal/impl/k/ad;)Ljava/util/Collection;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/reflect/jvm/internal/impl/k/ad;",
            ")",
            "Ljava/util/Collection",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/k/r;",
            ">;"
        }
    .end annotation

    .prologue
    .line 55
    instance-of v0, p1, Lkotlin/reflect/jvm/internal/impl/k/c;

    if-nez v0, :cond_1

    const/4 v0, 0x0

    :goto_0
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/c;

    if-eqz v0, :cond_0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/c;

    .line 57
    iget-object v1, v0, Lkotlin/reflect/jvm/internal/impl/k/c;->a:Lkotlin/reflect/jvm/internal/impl/j/f;

    invoke-interface {v1}, Lkotlin/reflect/jvm/internal/impl/j/f;->v_()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lkotlin/reflect/jvm/internal/impl/k/c$a;

    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/impl/k/c$a;->b()Ljava/util/Collection;

    move-result-object v1

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/k/c;->i()Ljava/util/Collection;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    invoke-static {v1, v0}, Lkotlin/a/m;->b(Ljava/util/Collection;Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object v0

    .line 55
    check-cast v0, Ljava/util/List;

    if-eqz v0, :cond_0

    check-cast v0, Ljava/util/Collection;

    .line 59
    :goto_1
    return-object v0

    :cond_0
    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/k/ad;->G_()Ljava/util/Collection;

    move-result-object v0

    const-string v1, "supertypes"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_1

    :cond_1
    move-object v0, p1

    goto :goto_0
.end method

.method public static final synthetic a(Lkotlin/reflect/jvm/internal/impl/k/c;Lkotlin/reflect/jvm/internal/impl/k/ad;)Ljava/util/Collection;
    .locals 1

    .prologue
    .line 22
    invoke-direct {p0, p1}, Lkotlin/reflect/jvm/internal/impl/k/c;->a(Lkotlin/reflect/jvm/internal/impl/k/ad;)Ljava/util/Collection;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method protected abstract F_()Lkotlin/reflect/jvm/internal/impl/b/ao;
.end method

.method public synthetic G_()Ljava/util/Collection;
    .locals 1

    .prologue
    .line 22
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/k/c;->j()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    return-object v0
.end method

.method protected abstract a()Ljava/util/Collection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/k/r;",
            ">;"
        }
    .end annotation
.end method

.method protected a(Lkotlin/reflect/jvm/internal/impl/k/r;)V
    .locals 1

    .prologue
    const-string v0, "type"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 63
    return-void
.end method

.method protected g()Lkotlin/reflect/jvm/internal/impl/k/r;
    .locals 1

    .prologue
    .line 65
    const/4 v0, 0x0

    return-object v0
.end method

.method protected i()Ljava/util/Collection;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/k/r;",
            ">;"
        }
    .end annotation

    .prologue
    .line 64
    invoke-static {}, Lkotlin/a/m;->a()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    return-object v0
.end method

.method public j()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/k/r;",
            ">;"
        }
    .end annotation

    .prologue
    .line 23
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/k/c;->a:Lkotlin/reflect/jvm/internal/impl/j/f;

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/j/f;->v_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/c$a;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/k/c$a;->a()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method
