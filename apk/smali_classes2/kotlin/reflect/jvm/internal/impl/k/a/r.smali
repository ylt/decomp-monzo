.class public final Lkotlin/reflect/jvm/internal/impl/k/a/r;
.super Ljava/lang/Object;
.source "utils.kt"


# direct methods
.method private static final a(Lkotlin/reflect/jvm/internal/impl/k/ad;)Ljava/lang/String;
    .locals 5

    .prologue
    .line 88
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    move-object v0, v1

    check-cast v0, Ljava/lang/StringBuilder;

    .line 89
    new-instance v2, Lkotlin/reflect/jvm/internal/impl/k/a/r$a;

    invoke-direct {v2, v0}, Lkotlin/reflect/jvm/internal/impl/k/a/r$a;-><init>(Ljava/lang/StringBuilder;)V

    .line 91
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "type: "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lkotlin/reflect/jvm/internal/impl/k/a/r$a;->a(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 92
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "hashCode: "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->hashCode()I

    move-result v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lkotlin/reflect/jvm/internal/impl/k/a/r$a;->a(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 93
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "javaClass: "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lkotlin/reflect/jvm/internal/impl/k/a/r$a;->a(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 94
    invoke-interface {p0}, Lkotlin/reflect/jvm/internal/impl/k/ad;->d()Lkotlin/reflect/jvm/internal/impl/b/h;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/m;

    .line 95
    :goto_0
    if-eqz v0, :cond_0

    .line 97
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "fqName: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v4, Lkotlin/reflect/jvm/internal/impl/g/c;->e:Lkotlin/reflect/jvm/internal/impl/g/c;

    invoke-virtual {v4, v0}, Lkotlin/reflect/jvm/internal/impl/g/c;->a(Lkotlin/reflect/jvm/internal/impl/b/m;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lkotlin/reflect/jvm/internal/impl/k/a/r$a;->a(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 98
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "javaClass: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lkotlin/reflect/jvm/internal/impl/k/a/r$a;->a(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 100
    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/b/m;->y_()Lkotlin/reflect/jvm/internal/impl/b/m;

    move-result-object v0

    goto :goto_0

    .line 102
    :cond_0
    nop

    .line 88
    check-cast v1, Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "StringBuilder().apply(builderAction).toString()"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 102
    return-object v0
.end method

.method private static final a(Lkotlin/reflect/jvm/internal/impl/k/r;)Lkotlin/reflect/jvm/internal/impl/k/r;
    .locals 1

    .prologue
    .line 86
    invoke-static {p0}, Lkotlin/reflect/jvm/internal/impl/k/d/b;->a(Lkotlin/reflect/jvm/internal/impl/k/r;)Lkotlin/reflect/jvm/internal/impl/k/d/a;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/k/d/a;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/r;

    return-object v0
.end method

.method public static final a(Lkotlin/reflect/jvm/internal/impl/k/r;Lkotlin/reflect/jvm/internal/impl/k/r;Lkotlin/reflect/jvm/internal/impl/k/a/q;)Lkotlin/reflect/jvm/internal/impl/k/r;
    .locals 11

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    const/4 v2, 0x0

    const-string v0, "subtype"

    invoke-static {p0, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "supertype"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "typeCheckingProcedureCallbacks"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 32
    new-instance v6, Ljava/util/ArrayDeque;

    invoke-direct {v6}, Ljava/util/ArrayDeque;-><init>()V

    .line 33
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/k/a/m;

    invoke-direct {v0, p0, v4}, Lkotlin/reflect/jvm/internal/impl/k/a/m;-><init>(Lkotlin/reflect/jvm/internal/impl/k/r;Lkotlin/reflect/jvm/internal/impl/k/a/m;)V

    invoke-virtual {v6, v0}, Ljava/util/ArrayDeque;->add(Ljava/lang/Object;)Z

    .line 35
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/k/r;->i()Lkotlin/reflect/jvm/internal/impl/k/ad;

    move-result-object v7

    .line 37
    :cond_0
    invoke-virtual {v6}, Ljava/util/ArrayDeque;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_9

    .line 38
    invoke-virtual {v6}, Ljava/util/ArrayDeque;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/a/m;

    .line 39
    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/k/a/m;->a()Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v5

    .line 40
    invoke-virtual {v5}, Lkotlin/reflect/jvm/internal/impl/k/r;->i()Lkotlin/reflect/jvm/internal/impl/k/ad;

    move-result-object v1

    .line 42
    invoke-interface {p2, v1, v7}, Lkotlin/reflect/jvm/internal/impl/k/a/q;->a(Lkotlin/reflect/jvm/internal/impl/k/ad;Lkotlin/reflect/jvm/internal/impl/k/ad;)Z

    move-result v8

    if-eqz v8, :cond_8

    .line 44
    invoke-virtual {v5}, Lkotlin/reflect/jvm/internal/impl/k/r;->c()Z

    move-result v1

    .line 46
    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/k/a/m;->b()Lkotlin/reflect/jvm/internal/impl/k/a/m;

    move-result-object v0

    move v6, v1

    move-object v1, v5

    move-object v5, v0

    .line 48
    :goto_0
    if-eqz v5, :cond_6

    .line 49
    invoke-virtual {v5}, Lkotlin/reflect/jvm/internal/impl/k/a/m;->a()Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v8

    .line 50
    invoke-virtual {v8}, Lkotlin/reflect/jvm/internal/impl/k/r;->a()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 104
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :cond_1
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/ah;

    .line 50
    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/k/ah;->b()Lkotlin/reflect/jvm/internal/impl/k/ap;

    move-result-object v0

    sget-object v10, Lkotlin/reflect/jvm/internal/impl/k/ap;->a:Lkotlin/reflect/jvm/internal/impl/k/ap;

    invoke-static {v0, v10}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_1

    move v0, v3

    .line 105
    :goto_1
    if-eqz v0, :cond_4

    .line 51
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/k/ae;->b:Lkotlin/reflect/jvm/internal/impl/k/ae$a;

    invoke-virtual {v0, v8}, Lkotlin/reflect/jvm/internal/impl/k/ae$a;->a(Lkotlin/reflect/jvm/internal/impl/k/r;)Lkotlin/reflect/jvm/internal/impl/k/ak;

    move-result-object v0

    .line 52
    invoke-static {v0, v2, v3, v4}, Lkotlin/reflect/jvm/internal/impl/h/a/a/c;->a(Lkotlin/reflect/jvm/internal/impl/k/ak;ZILjava/lang/Object;)Lkotlin/reflect/jvm/internal/impl/k/ak;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/k/ak;->f()Lkotlin/reflect/jvm/internal/impl/k/am;

    move-result-object v0

    sget-object v9, Lkotlin/reflect/jvm/internal/impl/k/ap;->a:Lkotlin/reflect/jvm/internal/impl/k/ap;

    .line 53
    invoke-virtual {v0, v1, v9}, Lkotlin/reflect/jvm/internal/impl/k/am;->a(Lkotlin/reflect/jvm/internal/impl/k/r;Lkotlin/reflect/jvm/internal/impl/k/ap;)Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v0

    .line 54
    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/k/a/r;->a(Lkotlin/reflect/jvm/internal/impl/k/r;)Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v0

    move-object v1, v0

    .line 62
    :goto_2
    if-nez v6, :cond_2

    invoke-virtual {v8}, Lkotlin/reflect/jvm/internal/impl/k/r;->c()Z

    move-result v0

    if-eqz v0, :cond_5

    :cond_2
    move v0, v3

    .line 64
    :goto_3
    invoke-virtual {v5}, Lkotlin/reflect/jvm/internal/impl/k/a/m;->b()Lkotlin/reflect/jvm/internal/impl/k/a/m;

    move-result-object v5

    move v6, v0

    .line 48
    goto :goto_0

    :cond_3
    move v0, v2

    .line 105
    goto :goto_1

    .line 57
    :cond_4
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/k/ae;->b:Lkotlin/reflect/jvm/internal/impl/k/ae$a;

    invoke-virtual {v0, v8}, Lkotlin/reflect/jvm/internal/impl/k/ae$a;->a(Lkotlin/reflect/jvm/internal/impl/k/r;)Lkotlin/reflect/jvm/internal/impl/k/ak;

    move-result-object v0

    .line 58
    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/k/ak;->f()Lkotlin/reflect/jvm/internal/impl/k/am;

    move-result-object v0

    sget-object v9, Lkotlin/reflect/jvm/internal/impl/k/ap;->a:Lkotlin/reflect/jvm/internal/impl/k/ap;

    .line 59
    invoke-virtual {v0, v1, v9}, Lkotlin/reflect/jvm/internal/impl/k/am;->a(Lkotlin/reflect/jvm/internal/impl/k/r;Lkotlin/reflect/jvm/internal/impl/k/ap;)Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v0

    const-string v1, "TypeConstructorSubstitut\u2026uted, Variance.INVARIANT)"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v1, v0

    goto :goto_2

    :cond_5
    move v0, v2

    .line 62
    goto :goto_3

    .line 67
    :cond_6
    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/impl/k/r;->i()Lkotlin/reflect/jvm/internal/impl/k/ad;

    move-result-object v2

    .line 68
    invoke-interface {p2, v2, v7}, Lkotlin/reflect/jvm/internal/impl/k/a/q;->a(Lkotlin/reflect/jvm/internal/impl/k/ad;Lkotlin/reflect/jvm/internal/impl/k/ad;)Z

    move-result v0

    if-nez v0, :cond_7

    .line 69
    new-instance v0, Ljava/lang/AssertionError;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Type constructors should be equals!\n"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "substitutedSuperType: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {v2}, Lkotlin/reflect/jvm/internal/impl/k/a/r;->a(Lkotlin/reflect/jvm/internal/impl/k/ad;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "supertype: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {v7}, Lkotlin/reflect/jvm/internal/impl/k/a/r;->a(Lkotlin/reflect/jvm/internal/impl/k/ad;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-interface {p2, v2, v7}, Lkotlin/reflect/jvm/internal/impl/k/a/q;->a(Lkotlin/reflect/jvm/internal/impl/k/ad;Lkotlin/reflect/jvm/internal/impl/k/ad;)Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    .line 75
    :cond_7
    invoke-static {v1, v6}, Lkotlin/reflect/jvm/internal/impl/k/an;->a(Lkotlin/reflect/jvm/internal/impl/k/r;Z)Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v0

    .line 83
    :goto_4
    return-object v0

    .line 78
    :cond_8
    invoke-interface {v1}, Lkotlin/reflect/jvm/internal/impl/k/ad;->G_()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_5
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lkotlin/reflect/jvm/internal/impl/k/r;

    .line 79
    new-instance v8, Lkotlin/reflect/jvm/internal/impl/k/a/m;

    const-string v9, "immediateSupertype"

    invoke-static {v1, v9}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v8, v1, v0}, Lkotlin/reflect/jvm/internal/impl/k/a/m;-><init>(Lkotlin/reflect/jvm/internal/impl/k/r;Lkotlin/reflect/jvm/internal/impl/k/a/m;)V

    invoke-virtual {v6, v8}, Ljava/util/ArrayDeque;->add(Ljava/lang/Object;)Z

    goto :goto_5

    :cond_9
    move-object v0, v4

    .line 83
    goto :goto_4
.end method
