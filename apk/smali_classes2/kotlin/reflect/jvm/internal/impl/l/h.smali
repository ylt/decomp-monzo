.class final Lkotlin/reflect/jvm/internal/impl/l/h;
.super Ljava/lang/Object;
.source "modifierChecks.kt"

# interfaces
.implements Lkotlin/reflect/jvm/internal/impl/l/b;


# static fields
.field public static final a:Lkotlin/reflect/jvm/internal/impl/l/h; = null

.field private static final b:Ljava/lang/String; = "should not have varargs or parameters with default values"


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 94
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/l/h;

    invoke-direct {v0}, Lkotlin/reflect/jvm/internal/impl/l/h;-><init>()V

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 94
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    check-cast p0, Lkotlin/reflect/jvm/internal/impl/l/h;

    sput-object p0, Lkotlin/reflect/jvm/internal/impl/l/h;->a:Lkotlin/reflect/jvm/internal/impl/l/h;

    .line 95
    const-string v0, "should not have varargs or parameters with default values"

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/l/h;->b:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 95
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/l/h;->b:Ljava/lang/String;

    return-object v0
.end method

.method public a(Lkotlin/reflect/jvm/internal/impl/b/s;)Z
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    const-string v0, "functionDescriptor"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 97
    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/b/s;->i()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 217
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/at;

    .line 97
    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/h/c/a;->a(Lkotlin/reflect/jvm/internal/impl/b/at;)Z

    move-result v4

    if-nez v4, :cond_1

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/b/at;->m()Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v0

    if-nez v0, :cond_1

    move v0, v2

    :goto_0
    if-nez v0, :cond_0

    move v0, v1

    :goto_1
    return v0

    :cond_1
    move v0, v1

    goto :goto_0

    :cond_2
    move v0, v2

    .line 218
    goto :goto_1
.end method

.method public b(Lkotlin/reflect/jvm/internal/impl/b/s;)Ljava/lang/String;
    .locals 1

    .prologue
    const-string v0, "functionDescriptor"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 94
    invoke-static {p0, p1}, Lkotlin/reflect/jvm/internal/impl/l/b$a;->a(Lkotlin/reflect/jvm/internal/impl/l/b;Lkotlin/reflect/jvm/internal/impl/b/s;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
