.class public abstract Lkotlin/reflect/jvm/internal/impl/l/k;
.super Ljava/lang/Object;
.source "modifierChecks.kt"

# interfaces
.implements Lkotlin/reflect/jvm/internal/impl/l/b;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lkotlin/reflect/jvm/internal/impl/l/k$a;,
        Lkotlin/reflect/jvm/internal/impl/l/k$b;,
        Lkotlin/reflect/jvm/internal/impl/l/k$c;
    }
.end annotation


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Ljava/lang/String;

.field private final c:Lkotlin/d/a/b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/d/a/b",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/a/l;",
            "Lkotlin/reflect/jvm/internal/impl/k/r;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Ljava/lang/String;Lkotlin/d/a/b;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lkotlin/d/a/b",
            "<-",
            "Lkotlin/reflect/jvm/internal/impl/a/l;",
            "+",
            "Lkotlin/reflect/jvm/internal/impl/k/r;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 113
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lkotlin/reflect/jvm/internal/impl/l/k;->b:Ljava/lang/String;

    iput-object p2, p0, Lkotlin/reflect/jvm/internal/impl/l/k;->c:Lkotlin/d/a/b;

    .line 114
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "must return "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/l/k;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/l/k;->a:Ljava/lang/String;

    return-void
.end method

.method public synthetic constructor <init>(Ljava/lang/String;Lkotlin/d/a/b;Lkotlin/d/b/i;)V
    .locals 0

    .prologue
    .line 113
    invoke-direct {p0, p1, p2}, Lkotlin/reflect/jvm/internal/impl/l/k;-><init>(Ljava/lang/String;Lkotlin/d/a/b;)V

    return-void
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 114
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/l/k;->a:Ljava/lang/String;

    return-object v0
.end method

.method public a(Lkotlin/reflect/jvm/internal/impl/b/s;)Z
    .locals 3

    .prologue
    const-string v0, "functionDescriptor"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 115
    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/b/s;->g()Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v1

    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/l/k;->c:Lkotlin/d/a/b;

    check-cast p1, Lkotlin/reflect/jvm/internal/impl/b/m;

    invoke-static {p1}, Lkotlin/reflect/jvm/internal/impl/h/c/a;->d(Lkotlin/reflect/jvm/internal/impl/b/m;)Lkotlin/reflect/jvm/internal/impl/a/l;

    move-result-object v2

    invoke-interface {v0, v2}, Lkotlin/d/a/b;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/r;

    invoke-static {v1, v0}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public b(Lkotlin/reflect/jvm/internal/impl/b/s;)Ljava/lang/String;
    .locals 1

    .prologue
    const-string v0, "functionDescriptor"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 113
    invoke-static {p0, p1}, Lkotlin/reflect/jvm/internal/impl/l/b$a;->a(Lkotlin/reflect/jvm/internal/impl/l/b;Lkotlin/reflect/jvm/internal/impl/b/s;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
