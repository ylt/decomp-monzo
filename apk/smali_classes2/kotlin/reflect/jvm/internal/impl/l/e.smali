.class final Lkotlin/reflect/jvm/internal/impl/l/e;
.super Ljava/lang/Object;
.source "modifierChecks.kt"

# interfaces
.implements Lkotlin/reflect/jvm/internal/impl/l/b;


# static fields
.field public static final a:Lkotlin/reflect/jvm/internal/impl/l/e; = null

.field private static final b:Ljava/lang/String; = "second parameter must have a KProperty type or its supertype"


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 105
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/l/e;

    invoke-direct {v0}, Lkotlin/reflect/jvm/internal/impl/l/e;-><init>()V

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 105
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    check-cast p0, Lkotlin/reflect/jvm/internal/impl/l/e;

    sput-object p0, Lkotlin/reflect/jvm/internal/impl/l/e;->a:Lkotlin/reflect/jvm/internal/impl/l/e;

    .line 106
    const-string v0, "second parameter must have a KProperty type or its supertype"

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/l/e;->b:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 106
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/l/e;->b:Ljava/lang/String;

    return-object v0
.end method

.method public a(Lkotlin/reflect/jvm/internal/impl/b/s;)Z
    .locals 3

    .prologue
    const-string v0, "functionDescriptor"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 108
    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/b/s;->i()Ljava/util/List;

    move-result-object v0

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/at;

    .line 109
    sget-object v2, Lkotlin/reflect/jvm/internal/impl/a/n;->b:Lkotlin/reflect/jvm/internal/impl/a/n$b;

    move-object v1, v0

    check-cast v1, Lkotlin/reflect/jvm/internal/impl/b/m;

    invoke-static {v1}, Lkotlin/reflect/jvm/internal/impl/h/c/a;->c(Lkotlin/reflect/jvm/internal/impl/b/m;)Lkotlin/reflect/jvm/internal/impl/b/w;

    move-result-object v1

    invoke-virtual {v2, v1}, Lkotlin/reflect/jvm/internal/impl/a/n$b;->a(Lkotlin/reflect/jvm/internal/impl/b/w;)Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/b/at;->s()Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v0

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/k/c/a;->c(Lkotlin/reflect/jvm/internal/impl/k/r;)Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v0

    const-string v2, "secondParameter.type.makeNotNullable()"

    invoke-static {v0, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v1, v0}, Lkotlin/reflect/jvm/internal/impl/k/c/a;->a(Lkotlin/reflect/jvm/internal/impl/k/r;Lkotlin/reflect/jvm/internal/impl/k/r;)Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b(Lkotlin/reflect/jvm/internal/impl/b/s;)Ljava/lang/String;
    .locals 1

    .prologue
    const-string v0, "functionDescriptor"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 105
    invoke-static {p0, p1}, Lkotlin/reflect/jvm/internal/impl/l/b$a;->a(Lkotlin/reflect/jvm/internal/impl/l/b;Lkotlin/reflect/jvm/internal/impl/b/s;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
