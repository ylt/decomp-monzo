.class final Lkotlin/reflect/jvm/internal/impl/l/i$c;
.super Lkotlin/d/b/m;
.source "modifierChecks.kt"

# interfaces
.implements Lkotlin/d/a/b;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lkotlin/reflect/jvm/internal/impl/l/i;-><init>()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/d/b/m;",
        "Lkotlin/d/a/b",
        "<",
        "Lkotlin/reflect/jvm/internal/impl/b/s;",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:Lkotlin/reflect/jvm/internal/impl/l/i$c;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lkotlin/reflect/jvm/internal/impl/l/i$c;

    invoke-direct {v0}, Lkotlin/reflect/jvm/internal/impl/l/i$c;-><init>()V

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/l/i$c;->a:Lkotlin/reflect/jvm/internal/impl/l/i$c;

    return-void
.end method

.method constructor <init>()V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lkotlin/d/b/m;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 177
    check-cast p1, Lkotlin/reflect/jvm/internal/impl/b/s;

    invoke-virtual {p0, p1}, Lkotlin/reflect/jvm/internal/impl/l/i$c;->a(Lkotlin/reflect/jvm/internal/impl/b/s;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lkotlin/reflect/jvm/internal/impl/b/s;)Ljava/lang/String;
    .locals 4

    .prologue
    const/4 v1, 0x0

    const-string v0, "$receiver"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 201
    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/b/s;->e()Lkotlin/reflect/jvm/internal/impl/b/aj;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 202
    :goto_0
    sget-object v2, Lkotlin/reflect/jvm/internal/impl/l/i;->a:Lkotlin/reflect/jvm/internal/impl/l/i;

    if-eqz v0, :cond_2

    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/b/s;->g()Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/b/aj;->s()Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v0

    const-string v3, "receiver.type"

    invoke-static {v0, v3}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v2, v0}, Lkotlin/reflect/jvm/internal/impl/k/c/a;->a(Lkotlin/reflect/jvm/internal/impl/k/r;Lkotlin/reflect/jvm/internal/impl/k/r;)Z

    move-result v0

    :goto_1
    if-eqz v0, :cond_2

    const/4 v0, 0x1

    .line 217
    :goto_2
    if-nez v0, :cond_3

    .line 203
    const-string v0, "receiver must be a supertype of the return type"

    check-cast v0, Ljava/lang/String;

    .line 204
    :goto_3
    return-object v0

    .line 201
    :cond_0
    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/b/s;->d()Lkotlin/reflect/jvm/internal/impl/b/aj;

    move-result-object v0

    goto :goto_0

    :cond_1
    move v0, v1

    .line 202
    goto :goto_1

    :cond_2
    move v0, v1

    goto :goto_2

    .line 203
    :cond_3
    const/4 v0, 0x0

    goto :goto_3
.end method
