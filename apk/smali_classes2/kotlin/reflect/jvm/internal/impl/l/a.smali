.class public abstract Lkotlin/reflect/jvm/internal/impl/l/a;
.super Ljava/lang/Object;
.source "modifierChecks.kt"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 162
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract a()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/l/d;",
            ">;"
        }
    .end annotation
.end method

.method public final a(Lkotlin/reflect/jvm/internal/impl/b/s;)Lkotlin/reflect/jvm/internal/impl/l/c;
    .locals 3

    .prologue
    const-string v0, "functionDescriptor"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 168
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/l/a;->a()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/l/d;

    .line 169
    invoke-virtual {v0, p1}, Lkotlin/reflect/jvm/internal/impl/l/d;->a(Lkotlin/reflect/jvm/internal/impl/b/s;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 170
    invoke-virtual {v0, p1}, Lkotlin/reflect/jvm/internal/impl/l/d;->b(Lkotlin/reflect/jvm/internal/impl/b/s;)Lkotlin/reflect/jvm/internal/impl/l/c;

    move-result-object v0

    .line 173
    :goto_0
    return-object v0

    :cond_1
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/l/c$a;->a:Lkotlin/reflect/jvm/internal/impl/l/c$a;

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/l/c;

    goto :goto_0
.end method
