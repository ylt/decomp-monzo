.class public final Lkotlin/reflect/jvm/internal/impl/l/d;
.super Ljava/lang/Object;
.source "modifierChecks.kt"


# instance fields
.field private final a:Lkotlin/reflect/jvm/internal/impl/e/f;

.field private final b:Lkotlin/h/g;

.field private final c:Ljava/util/Collection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Collection",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/e/f;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Lkotlin/d/a/b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/d/a/b",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/b/s;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final e:[Lkotlin/reflect/jvm/internal/impl/l/b;


# direct methods
.method public constructor <init>(Ljava/util/Collection;[Lkotlin/reflect/jvm/internal/impl/l/b;Lkotlin/d/a/b;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/e/f;",
            ">;[",
            "Lkotlin/reflect/jvm/internal/impl/l/b;",
            "Lkotlin/d/a/b",
            "<-",
            "Lkotlin/reflect/jvm/internal/impl/b/s;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    const-string v0, "nameList"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "checks"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "additionalChecks"

    invoke-static {p3, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 158
    array-length v0, p2

    invoke-static {p2, v0}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v5

    check-cast v5, [Lkotlin/reflect/jvm/internal/impl/l/b;

    move-object v0, p0

    move-object v2, v1

    move-object v3, p1

    move-object v4, p3

    .line 159
    invoke-direct/range {v0 .. v5}, Lkotlin/reflect/jvm/internal/impl/l/d;-><init>(Lkotlin/reflect/jvm/internal/impl/e/f;Lkotlin/h/g;Ljava/util/Collection;Lkotlin/d/a/b;[Lkotlin/reflect/jvm/internal/impl/l/b;)V

    return-void
.end method

.method public synthetic constructor <init>(Ljava/util/Collection;[Lkotlin/reflect/jvm/internal/impl/l/b;Lkotlin/d/a/b;ILkotlin/d/b/i;)V
    .locals 1

    .prologue
    and-int/lit8 v0, p4, 0x4

    if-eqz v0, :cond_0

    .line 158
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/l/d$3;->a:Lkotlin/reflect/jvm/internal/impl/l/d$3;

    check-cast v0, Lkotlin/d/a/b;

    :goto_0
    invoke-direct {p0, p1, p2, v0}, Lkotlin/reflect/jvm/internal/impl/l/d;-><init>(Ljava/util/Collection;[Lkotlin/reflect/jvm/internal/impl/l/b;Lkotlin/d/a/b;)V

    return-void

    :cond_0
    move-object v0, p3

    goto :goto_0
.end method

.method public constructor <init>(Lkotlin/h/g;[Lkotlin/reflect/jvm/internal/impl/l/b;Lkotlin/d/a/b;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/h/g;",
            "[",
            "Lkotlin/reflect/jvm/internal/impl/l/b;",
            "Lkotlin/d/a/b",
            "<-",
            "Lkotlin/reflect/jvm/internal/impl/b/s;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    const-string v0, "regex"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "checks"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "additionalChecks"

    invoke-static {p3, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 156
    array-length v0, p2

    invoke-static {p2, v0}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v5

    check-cast v5, [Lkotlin/reflect/jvm/internal/impl/l/b;

    move-object v0, p0

    move-object v2, p1

    move-object v3, v1

    move-object v4, p3

    .line 157
    invoke-direct/range {v0 .. v5}, Lkotlin/reflect/jvm/internal/impl/l/d;-><init>(Lkotlin/reflect/jvm/internal/impl/e/f;Lkotlin/h/g;Ljava/util/Collection;Lkotlin/d/a/b;[Lkotlin/reflect/jvm/internal/impl/l/b;)V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/h/g;[Lkotlin/reflect/jvm/internal/impl/l/b;Lkotlin/d/a/b;ILkotlin/d/b/i;)V
    .locals 1

    .prologue
    and-int/lit8 v0, p4, 0x4

    if-eqz v0, :cond_0

    .line 156
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/l/d$2;->a:Lkotlin/reflect/jvm/internal/impl/l/d$2;

    check-cast v0, Lkotlin/d/a/b;

    :goto_0
    invoke-direct {p0, p1, p2, v0}, Lkotlin/reflect/jvm/internal/impl/l/d;-><init>(Lkotlin/h/g;[Lkotlin/reflect/jvm/internal/impl/l/b;Lkotlin/d/a/b;)V

    return-void

    :cond_0
    move-object v0, p3

    goto :goto_0
.end method

.method private varargs constructor <init>(Lkotlin/reflect/jvm/internal/impl/e/f;Lkotlin/h/g;Ljava/util/Collection;Lkotlin/d/a/b;[Lkotlin/reflect/jvm/internal/impl/l/b;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/reflect/jvm/internal/impl/e/f;",
            "Lkotlin/h/g;",
            "Ljava/util/Collection",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/e/f;",
            ">;",
            "Lkotlin/d/a/b",
            "<-",
            "Lkotlin/reflect/jvm/internal/impl/b/s;",
            "Ljava/lang/String;",
            ">;[",
            "Lkotlin/reflect/jvm/internal/impl/l/b;",
            ")V"
        }
    .end annotation

    .prologue
    .line 122
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lkotlin/reflect/jvm/internal/impl/l/d;->a:Lkotlin/reflect/jvm/internal/impl/e/f;

    iput-object p2, p0, Lkotlin/reflect/jvm/internal/impl/l/d;->b:Lkotlin/h/g;

    iput-object p3, p0, Lkotlin/reflect/jvm/internal/impl/l/d;->c:Ljava/util/Collection;

    iput-object p4, p0, Lkotlin/reflect/jvm/internal/impl/l/d;->d:Lkotlin/d/a/b;

    iput-object p5, p0, Lkotlin/reflect/jvm/internal/impl/l/d;->e:[Lkotlin/reflect/jvm/internal/impl/l/b;

    return-void
.end method

.method public constructor <init>(Lkotlin/reflect/jvm/internal/impl/e/f;[Lkotlin/reflect/jvm/internal/impl/l/b;Lkotlin/d/a/b;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/reflect/jvm/internal/impl/e/f;",
            "[",
            "Lkotlin/reflect/jvm/internal/impl/l/b;",
            "Lkotlin/d/a/b",
            "<-",
            "Lkotlin/reflect/jvm/internal/impl/b/s;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    const-string v0, "name"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "checks"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "additionalChecks"

    invoke-static {p3, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 154
    array-length v0, p2

    invoke-static {p2, v0}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v5

    check-cast v5, [Lkotlin/reflect/jvm/internal/impl/l/b;

    move-object v0, p0

    move-object v1, p1

    move-object v3, v2

    move-object v4, p3

    .line 155
    invoke-direct/range {v0 .. v5}, Lkotlin/reflect/jvm/internal/impl/l/d;-><init>(Lkotlin/reflect/jvm/internal/impl/e/f;Lkotlin/h/g;Ljava/util/Collection;Lkotlin/d/a/b;[Lkotlin/reflect/jvm/internal/impl/l/b;)V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/reflect/jvm/internal/impl/e/f;[Lkotlin/reflect/jvm/internal/impl/l/b;Lkotlin/d/a/b;ILkotlin/d/b/i;)V
    .locals 1

    .prologue
    and-int/lit8 v0, p4, 0x4

    if-eqz v0, :cond_0

    .line 154
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/l/d$1;->a:Lkotlin/reflect/jvm/internal/impl/l/d$1;

    check-cast v0, Lkotlin/d/a/b;

    :goto_0
    invoke-direct {p0, p1, p2, v0}, Lkotlin/reflect/jvm/internal/impl/l/d;-><init>(Lkotlin/reflect/jvm/internal/impl/e/f;[Lkotlin/reflect/jvm/internal/impl/l/b;Lkotlin/d/a/b;)V

    return-void

    :cond_0
    move-object v0, p3

    goto :goto_0
.end method


# virtual methods
.method public final a(Lkotlin/reflect/jvm/internal/impl/b/s;)Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    const-string v0, "functionDescriptor"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 130
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/l/d;->a:Lkotlin/reflect/jvm/internal/impl/e/f;

    if-eqz v0, :cond_0

    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/b/s;->D_()Lkotlin/reflect/jvm/internal/impl/e/f;

    move-result-object v0

    iget-object v2, p0, Lkotlin/reflect/jvm/internal/impl/l/d;->a:Lkotlin/reflect/jvm/internal/impl/e/f;

    invoke-static {v0, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    move v0, v1

    .line 133
    :goto_0
    return v0

    .line 131
    :cond_0
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/l/d;->b:Lkotlin/h/g;

    if-eqz v0, :cond_1

    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/b/s;->D_()Lkotlin/reflect/jvm/internal/impl/e/f;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/e/f;->a()Ljava/lang/String;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    iget-object v2, p0, Lkotlin/reflect/jvm/internal/impl/l/d;->b:Lkotlin/h/g;

    invoke-virtual {v2, v0}, Lkotlin/h/g;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    goto :goto_0

    .line 132
    :cond_1
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/l/d;->c:Ljava/util/Collection;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/l/d;->c:Ljava/util/Collection;

    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/b/s;->D_()Lkotlin/reflect/jvm/internal/impl/e/f;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Collection;->contains(Ljava/lang/Object;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_2

    move v0, v1

    goto :goto_0

    .line 133
    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final b(Lkotlin/reflect/jvm/internal/impl/b/s;)Lkotlin/reflect/jvm/internal/impl/l/c;
    .locals 3

    .prologue
    const-string v0, "functionDescriptor"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 137
    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/l/d;->e:[Lkotlin/reflect/jvm/internal/impl/l/b;

    const/4 v0, 0x0

    :goto_0
    array-length v2, v1

    if-ge v0, v2, :cond_1

    aget-object v2, v1, v0

    .line 138
    invoke-interface {v2, p1}, Lkotlin/reflect/jvm/internal/impl/l/b;->b(Lkotlin/reflect/jvm/internal/impl/b/s;)Ljava/lang/String;

    move-result-object v2

    .line 139
    if-eqz v2, :cond_0

    .line 140
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/l/c$b;

    invoke-direct {v0, v2}, Lkotlin/reflect/jvm/internal/impl/l/c$b;-><init>(Ljava/lang/String;)V

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/l/c;

    .line 149
    :goto_1
    return-object v0

    .line 137
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 144
    :cond_1
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/l/d;->d:Lkotlin/d/a/b;

    invoke-interface {v0, p1}, Lkotlin/d/a/b;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 145
    if-eqz v0, :cond_2

    .line 146
    new-instance v1, Lkotlin/reflect/jvm/internal/impl/l/c$b;

    invoke-direct {v1, v0}, Lkotlin/reflect/jvm/internal/impl/l/c$b;-><init>(Ljava/lang/String;)V

    move-object v0, v1

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/l/c;

    goto :goto_1

    .line 149
    :cond_2
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/l/c$c;->a:Lkotlin/reflect/jvm/internal/impl/l/c$c;

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/l/c;

    goto :goto_1
.end method
