.class final Lkotlin/reflect/jvm/internal/impl/l/i$a;
.super Lkotlin/d/b/m;
.source "modifierChecks.kt"

# interfaces
.implements Lkotlin/d/a/b;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lkotlin/reflect/jvm/internal/impl/l/i;-><init>()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/d/b/m;",
        "Lkotlin/d/a/b",
        "<",
        "Lkotlin/reflect/jvm/internal/impl/b/s;",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:Lkotlin/reflect/jvm/internal/impl/l/i$a;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lkotlin/reflect/jvm/internal/impl/l/i$a;

    invoke-direct {v0}, Lkotlin/reflect/jvm/internal/impl/l/i$a;-><init>()V

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/l/i$a;->a:Lkotlin/reflect/jvm/internal/impl/l/i$a;

    return-void
.end method

.method constructor <init>()V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lkotlin/d/b/m;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 177
    check-cast p1, Lkotlin/reflect/jvm/internal/impl/b/s;

    invoke-virtual {p0, p1}, Lkotlin/reflect/jvm/internal/impl/l/i$a;->a(Lkotlin/reflect/jvm/internal/impl/b/s;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lkotlin/reflect/jvm/internal/impl/b/s;)Ljava/lang/String;
    .locals 3

    .prologue
    const/4 v1, 0x0

    const-string v0, "$receiver"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 181
    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/b/s;->i()Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lkotlin/a/m;->h(Ljava/util/List;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/at;

    if-eqz v0, :cond_1

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/at;

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/h/c/a;->a(Lkotlin/reflect/jvm/internal/impl/b/at;)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/b/at;->m()Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    .line 182
    :goto_0
    sget-object v1, Lkotlin/reflect/jvm/internal/impl/l/i;->a:Lkotlin/reflect/jvm/internal/impl/l/i;

    .line 217
    if-nez v0, :cond_2

    .line 182
    const-string v0, "last parameter should not have a default value or be a vararg"

    check-cast v0, Ljava/lang/String;

    :goto_1
    return-object v0

    :cond_0
    move v0, v1

    .line 181
    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_0

    .line 182
    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method
