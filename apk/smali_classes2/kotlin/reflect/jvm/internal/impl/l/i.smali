.class public final Lkotlin/reflect/jvm/internal/impl/l/i;
.super Lkotlin/reflect/jvm/internal/impl/l/a;
.source "modifierChecks.kt"


# static fields
.field public static final a:Lkotlin/reflect/jvm/internal/impl/l/i;

.field private static final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/l/d;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 177
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/l/i;

    invoke-direct {v0}, Lkotlin/reflect/jvm/internal/impl/l/i;-><init>()V

    return-void
.end method

.method private constructor <init>()V
    .locals 12

    .prologue
    const/4 v11, 0x2

    const/4 v10, 0x0

    const/4 v9, 0x1

    const/4 v4, 0x4

    const/4 v3, 0x0

    .line 177
    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/l/a;-><init>()V

    check-cast p0, Lkotlin/reflect/jvm/internal/impl/l/i;

    sput-object p0, Lkotlin/reflect/jvm/internal/impl/l/i;->a:Lkotlin/reflect/jvm/internal/impl/l/i;

    .line 178
    const/16 v0, 0x12

    new-array v6, v0, [Lkotlin/reflect/jvm/internal/impl/l/d;

    new-instance v0, Lkotlin/reflect/jvm/internal/impl/l/d;

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/l/j;->i:Lkotlin/reflect/jvm/internal/impl/e/f;

    const-string v2, "GET"

    invoke-static {v1, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    new-array v2, v11, [Lkotlin/reflect/jvm/internal/impl/l/b;

    sget-object v5, Lkotlin/reflect/jvm/internal/impl/l/f$b;->a:Lkotlin/reflect/jvm/internal/impl/l/f$b;

    check-cast v5, Lkotlin/reflect/jvm/internal/impl/l/b;

    aput-object v5, v2, v10

    new-instance v5, Lkotlin/reflect/jvm/internal/impl/l/l$a;

    invoke-direct {v5, v9}, Lkotlin/reflect/jvm/internal/impl/l/l$a;-><init>(I)V

    check-cast v5, Lkotlin/reflect/jvm/internal/impl/l/b;

    aput-object v5, v2, v9

    move-object v5, v3

    invoke-direct/range {v0 .. v5}, Lkotlin/reflect/jvm/internal/impl/l/d;-><init>(Lkotlin/reflect/jvm/internal/impl/e/f;[Lkotlin/reflect/jvm/internal/impl/l/b;Lkotlin/d/a/b;ILkotlin/d/b/i;)V

    aput-object v0, v6, v10

    new-instance v1, Lkotlin/reflect/jvm/internal/impl/l/d;

    sget-object v2, Lkotlin/reflect/jvm/internal/impl/l/j;->j:Lkotlin/reflect/jvm/internal/impl/e/f;

    const-string v0, "SET"

    invoke-static {v2, v0}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    new-array v5, v11, [Lkotlin/reflect/jvm/internal/impl/l/b;

    sget-object v0, Lkotlin/reflect/jvm/internal/impl/l/f$b;->a:Lkotlin/reflect/jvm/internal/impl/l/f$b;

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/l/b;

    aput-object v0, v5, v10

    new-instance v0, Lkotlin/reflect/jvm/internal/impl/l/l$a;

    invoke-direct {v0, v11}, Lkotlin/reflect/jvm/internal/impl/l/l$a;-><init>(I)V

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/l/b;

    aput-object v0, v5, v9

    sget-object v0, Lkotlin/reflect/jvm/internal/impl/l/i$a;->a:Lkotlin/reflect/jvm/internal/impl/l/i$a;

    check-cast v0, Lkotlin/d/a/b;

    invoke-direct {v1, v2, v5, v0}, Lkotlin/reflect/jvm/internal/impl/l/d;-><init>(Lkotlin/reflect/jvm/internal/impl/e/f;[Lkotlin/reflect/jvm/internal/impl/l/b;Lkotlin/d/a/b;)V

    aput-object v1, v6, v9

    new-instance v0, Lkotlin/reflect/jvm/internal/impl/l/d;

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/l/j;->a:Lkotlin/reflect/jvm/internal/impl/e/f;

    const-string v2, "GET_VALUE"

    invoke-static {v1, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    new-array v2, v4, [Lkotlin/reflect/jvm/internal/impl/l/b;

    sget-object v5, Lkotlin/reflect/jvm/internal/impl/l/f$b;->a:Lkotlin/reflect/jvm/internal/impl/l/f$b;

    check-cast v5, Lkotlin/reflect/jvm/internal/impl/l/b;

    aput-object v5, v2, v10

    sget-object v5, Lkotlin/reflect/jvm/internal/impl/l/h;->a:Lkotlin/reflect/jvm/internal/impl/l/h;

    check-cast v5, Lkotlin/reflect/jvm/internal/impl/l/b;

    aput-object v5, v2, v9

    new-instance v5, Lkotlin/reflect/jvm/internal/impl/l/l$a;

    invoke-direct {v5, v11}, Lkotlin/reflect/jvm/internal/impl/l/l$a;-><init>(I)V

    check-cast v5, Lkotlin/reflect/jvm/internal/impl/l/b;

    aput-object v5, v2, v11

    const/4 v7, 0x3

    sget-object v5, Lkotlin/reflect/jvm/internal/impl/l/e;->a:Lkotlin/reflect/jvm/internal/impl/l/e;

    check-cast v5, Lkotlin/reflect/jvm/internal/impl/l/b;

    aput-object v5, v2, v7

    move-object v5, v3

    invoke-direct/range {v0 .. v5}, Lkotlin/reflect/jvm/internal/impl/l/d;-><init>(Lkotlin/reflect/jvm/internal/impl/e/f;[Lkotlin/reflect/jvm/internal/impl/l/b;Lkotlin/d/a/b;ILkotlin/d/b/i;)V

    aput-object v0, v6, v11

    const/4 v7, 0x3

    new-instance v0, Lkotlin/reflect/jvm/internal/impl/l/d;

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/l/j;->b:Lkotlin/reflect/jvm/internal/impl/e/f;

    const-string v2, "SET_VALUE"

    invoke-static {v1, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    new-array v2, v4, [Lkotlin/reflect/jvm/internal/impl/l/b;

    sget-object v5, Lkotlin/reflect/jvm/internal/impl/l/f$b;->a:Lkotlin/reflect/jvm/internal/impl/l/f$b;

    check-cast v5, Lkotlin/reflect/jvm/internal/impl/l/b;

    aput-object v5, v2, v10

    sget-object v5, Lkotlin/reflect/jvm/internal/impl/l/h;->a:Lkotlin/reflect/jvm/internal/impl/l/h;

    check-cast v5, Lkotlin/reflect/jvm/internal/impl/l/b;

    aput-object v5, v2, v9

    new-instance v5, Lkotlin/reflect/jvm/internal/impl/l/l$a;

    const/4 v8, 0x3

    invoke-direct {v5, v8}, Lkotlin/reflect/jvm/internal/impl/l/l$a;-><init>(I)V

    check-cast v5, Lkotlin/reflect/jvm/internal/impl/l/b;

    aput-object v5, v2, v11

    const/4 v8, 0x3

    sget-object v5, Lkotlin/reflect/jvm/internal/impl/l/e;->a:Lkotlin/reflect/jvm/internal/impl/l/e;

    check-cast v5, Lkotlin/reflect/jvm/internal/impl/l/b;

    aput-object v5, v2, v8

    move-object v5, v3

    invoke-direct/range {v0 .. v5}, Lkotlin/reflect/jvm/internal/impl/l/d;-><init>(Lkotlin/reflect/jvm/internal/impl/e/f;[Lkotlin/reflect/jvm/internal/impl/l/b;Lkotlin/d/a/b;ILkotlin/d/b/i;)V

    aput-object v0, v6, v7

    new-instance v0, Lkotlin/reflect/jvm/internal/impl/l/d;

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/l/j;->c:Lkotlin/reflect/jvm/internal/impl/e/f;

    const-string v2, "PROVIDE_DELEGATE"

    invoke-static {v1, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    new-array v2, v4, [Lkotlin/reflect/jvm/internal/impl/l/b;

    sget-object v5, Lkotlin/reflect/jvm/internal/impl/l/f$b;->a:Lkotlin/reflect/jvm/internal/impl/l/f$b;

    check-cast v5, Lkotlin/reflect/jvm/internal/impl/l/b;

    aput-object v5, v2, v10

    sget-object v5, Lkotlin/reflect/jvm/internal/impl/l/h;->a:Lkotlin/reflect/jvm/internal/impl/l/h;

    check-cast v5, Lkotlin/reflect/jvm/internal/impl/l/b;

    aput-object v5, v2, v9

    new-instance v5, Lkotlin/reflect/jvm/internal/impl/l/l$b;

    invoke-direct {v5, v11}, Lkotlin/reflect/jvm/internal/impl/l/l$b;-><init>(I)V

    check-cast v5, Lkotlin/reflect/jvm/internal/impl/l/b;

    aput-object v5, v2, v11

    const/4 v7, 0x3

    sget-object v5, Lkotlin/reflect/jvm/internal/impl/l/e;->a:Lkotlin/reflect/jvm/internal/impl/l/e;

    check-cast v5, Lkotlin/reflect/jvm/internal/impl/l/b;

    aput-object v5, v2, v7

    move-object v5, v3

    invoke-direct/range {v0 .. v5}, Lkotlin/reflect/jvm/internal/impl/l/d;-><init>(Lkotlin/reflect/jvm/internal/impl/e/f;[Lkotlin/reflect/jvm/internal/impl/l/b;Lkotlin/d/a/b;ILkotlin/d/b/i;)V

    aput-object v0, v6, v4

    const/4 v7, 0x5

    new-instance v0, Lkotlin/reflect/jvm/internal/impl/l/d;

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/l/j;->g:Lkotlin/reflect/jvm/internal/impl/e/f;

    const-string v2, "INVOKE"

    invoke-static {v1, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    new-array v2, v9, [Lkotlin/reflect/jvm/internal/impl/l/b;

    sget-object v5, Lkotlin/reflect/jvm/internal/impl/l/f$b;->a:Lkotlin/reflect/jvm/internal/impl/l/f$b;

    check-cast v5, Lkotlin/reflect/jvm/internal/impl/l/b;

    aput-object v5, v2, v10

    move-object v5, v3

    invoke-direct/range {v0 .. v5}, Lkotlin/reflect/jvm/internal/impl/l/d;-><init>(Lkotlin/reflect/jvm/internal/impl/e/f;[Lkotlin/reflect/jvm/internal/impl/l/b;Lkotlin/d/a/b;ILkotlin/d/b/i;)V

    aput-object v0, v6, v7

    const/4 v7, 0x6

    new-instance v0, Lkotlin/reflect/jvm/internal/impl/l/d;

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/l/j;->f:Lkotlin/reflect/jvm/internal/impl/e/f;

    const-string v2, "CONTAINS"

    invoke-static {v1, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    new-array v2, v4, [Lkotlin/reflect/jvm/internal/impl/l/b;

    sget-object v5, Lkotlin/reflect/jvm/internal/impl/l/f$b;->a:Lkotlin/reflect/jvm/internal/impl/l/f$b;

    check-cast v5, Lkotlin/reflect/jvm/internal/impl/l/b;

    aput-object v5, v2, v10

    sget-object v5, Lkotlin/reflect/jvm/internal/impl/l/l$d;->a:Lkotlin/reflect/jvm/internal/impl/l/l$d;

    check-cast v5, Lkotlin/reflect/jvm/internal/impl/l/b;

    aput-object v5, v2, v9

    sget-object v5, Lkotlin/reflect/jvm/internal/impl/l/h;->a:Lkotlin/reflect/jvm/internal/impl/l/h;

    check-cast v5, Lkotlin/reflect/jvm/internal/impl/l/b;

    aput-object v5, v2, v11

    const/4 v8, 0x3

    sget-object v5, Lkotlin/reflect/jvm/internal/impl/l/k$a;->a:Lkotlin/reflect/jvm/internal/impl/l/k$a;

    check-cast v5, Lkotlin/reflect/jvm/internal/impl/l/b;

    aput-object v5, v2, v8

    move-object v5, v3

    invoke-direct/range {v0 .. v5}, Lkotlin/reflect/jvm/internal/impl/l/d;-><init>(Lkotlin/reflect/jvm/internal/impl/e/f;[Lkotlin/reflect/jvm/internal/impl/l/b;Lkotlin/d/a/b;ILkotlin/d/b/i;)V

    aput-object v0, v6, v7

    const/4 v7, 0x7

    new-instance v0, Lkotlin/reflect/jvm/internal/impl/l/d;

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/l/j;->h:Lkotlin/reflect/jvm/internal/impl/e/f;

    const-string v2, "ITERATOR"

    invoke-static {v1, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    new-array v2, v11, [Lkotlin/reflect/jvm/internal/impl/l/b;

    sget-object v5, Lkotlin/reflect/jvm/internal/impl/l/f$b;->a:Lkotlin/reflect/jvm/internal/impl/l/f$b;

    check-cast v5, Lkotlin/reflect/jvm/internal/impl/l/b;

    aput-object v5, v2, v10

    sget-object v5, Lkotlin/reflect/jvm/internal/impl/l/l$c;->a:Lkotlin/reflect/jvm/internal/impl/l/l$c;

    check-cast v5, Lkotlin/reflect/jvm/internal/impl/l/b;

    aput-object v5, v2, v9

    move-object v5, v3

    invoke-direct/range {v0 .. v5}, Lkotlin/reflect/jvm/internal/impl/l/d;-><init>(Lkotlin/reflect/jvm/internal/impl/e/f;[Lkotlin/reflect/jvm/internal/impl/l/b;Lkotlin/d/a/b;ILkotlin/d/b/i;)V

    aput-object v0, v6, v7

    const/16 v7, 0x8

    new-instance v0, Lkotlin/reflect/jvm/internal/impl/l/d;

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/l/j;->k:Lkotlin/reflect/jvm/internal/impl/e/f;

    const-string v2, "NEXT"

    invoke-static {v1, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    new-array v2, v11, [Lkotlin/reflect/jvm/internal/impl/l/b;

    sget-object v5, Lkotlin/reflect/jvm/internal/impl/l/f$b;->a:Lkotlin/reflect/jvm/internal/impl/l/f$b;

    check-cast v5, Lkotlin/reflect/jvm/internal/impl/l/b;

    aput-object v5, v2, v10

    sget-object v5, Lkotlin/reflect/jvm/internal/impl/l/l$c;->a:Lkotlin/reflect/jvm/internal/impl/l/l$c;

    check-cast v5, Lkotlin/reflect/jvm/internal/impl/l/b;

    aput-object v5, v2, v9

    move-object v5, v3

    invoke-direct/range {v0 .. v5}, Lkotlin/reflect/jvm/internal/impl/l/d;-><init>(Lkotlin/reflect/jvm/internal/impl/e/f;[Lkotlin/reflect/jvm/internal/impl/l/b;Lkotlin/d/a/b;ILkotlin/d/b/i;)V

    aput-object v0, v6, v7

    const/16 v7, 0x9

    new-instance v0, Lkotlin/reflect/jvm/internal/impl/l/d;

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/l/j;->l:Lkotlin/reflect/jvm/internal/impl/e/f;

    const-string v2, "HAS_NEXT"

    invoke-static {v1, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v2, 0x3

    new-array v2, v2, [Lkotlin/reflect/jvm/internal/impl/l/b;

    sget-object v5, Lkotlin/reflect/jvm/internal/impl/l/f$b;->a:Lkotlin/reflect/jvm/internal/impl/l/f$b;

    check-cast v5, Lkotlin/reflect/jvm/internal/impl/l/b;

    aput-object v5, v2, v10

    sget-object v5, Lkotlin/reflect/jvm/internal/impl/l/l$c;->a:Lkotlin/reflect/jvm/internal/impl/l/l$c;

    check-cast v5, Lkotlin/reflect/jvm/internal/impl/l/b;

    aput-object v5, v2, v9

    sget-object v5, Lkotlin/reflect/jvm/internal/impl/l/k$a;->a:Lkotlin/reflect/jvm/internal/impl/l/k$a;

    check-cast v5, Lkotlin/reflect/jvm/internal/impl/l/b;

    aput-object v5, v2, v11

    move-object v5, v3

    invoke-direct/range {v0 .. v5}, Lkotlin/reflect/jvm/internal/impl/l/d;-><init>(Lkotlin/reflect/jvm/internal/impl/e/f;[Lkotlin/reflect/jvm/internal/impl/l/b;Lkotlin/d/a/b;ILkotlin/d/b/i;)V

    aput-object v0, v6, v7

    const/16 v7, 0xa

    new-instance v0, Lkotlin/reflect/jvm/internal/impl/l/d;

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/l/j;->A:Lkotlin/reflect/jvm/internal/impl/e/f;

    const-string v2, "RANGE_TO"

    invoke-static {v1, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v2, 0x3

    new-array v2, v2, [Lkotlin/reflect/jvm/internal/impl/l/b;

    sget-object v5, Lkotlin/reflect/jvm/internal/impl/l/f$b;->a:Lkotlin/reflect/jvm/internal/impl/l/f$b;

    check-cast v5, Lkotlin/reflect/jvm/internal/impl/l/b;

    aput-object v5, v2, v10

    sget-object v5, Lkotlin/reflect/jvm/internal/impl/l/l$d;->a:Lkotlin/reflect/jvm/internal/impl/l/l$d;

    check-cast v5, Lkotlin/reflect/jvm/internal/impl/l/b;

    aput-object v5, v2, v9

    sget-object v5, Lkotlin/reflect/jvm/internal/impl/l/h;->a:Lkotlin/reflect/jvm/internal/impl/l/h;

    check-cast v5, Lkotlin/reflect/jvm/internal/impl/l/b;

    aput-object v5, v2, v11

    move-object v5, v3

    invoke-direct/range {v0 .. v5}, Lkotlin/reflect/jvm/internal/impl/l/d;-><init>(Lkotlin/reflect/jvm/internal/impl/e/f;[Lkotlin/reflect/jvm/internal/impl/l/b;Lkotlin/d/a/b;ILkotlin/d/b/i;)V

    aput-object v0, v6, v7

    const/16 v1, 0xb

    new-instance v2, Lkotlin/reflect/jvm/internal/impl/l/d;

    sget-object v5, Lkotlin/reflect/jvm/internal/impl/l/j;->d:Lkotlin/reflect/jvm/internal/impl/e/f;

    const-string v0, "EQUALS"

    invoke-static {v5, v0}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    new-array v7, v9, [Lkotlin/reflect/jvm/internal/impl/l/b;

    sget-object v0, Lkotlin/reflect/jvm/internal/impl/l/f$a;->a:Lkotlin/reflect/jvm/internal/impl/l/f$a;

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/l/b;

    aput-object v0, v7, v10

    sget-object v0, Lkotlin/reflect/jvm/internal/impl/l/i$b;->a:Lkotlin/reflect/jvm/internal/impl/l/i$b;

    check-cast v0, Lkotlin/d/a/b;

    invoke-direct {v2, v5, v7, v0}, Lkotlin/reflect/jvm/internal/impl/l/d;-><init>(Lkotlin/reflect/jvm/internal/impl/e/f;[Lkotlin/reflect/jvm/internal/impl/l/b;Lkotlin/d/a/b;)V

    aput-object v2, v6, v1

    const/16 v7, 0xc

    new-instance v0, Lkotlin/reflect/jvm/internal/impl/l/d;

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/l/j;->e:Lkotlin/reflect/jvm/internal/impl/e/f;

    const-string v2, "COMPARE_TO"

    invoke-static {v1, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    new-array v2, v4, [Lkotlin/reflect/jvm/internal/impl/l/b;

    sget-object v5, Lkotlin/reflect/jvm/internal/impl/l/f$b;->a:Lkotlin/reflect/jvm/internal/impl/l/f$b;

    check-cast v5, Lkotlin/reflect/jvm/internal/impl/l/b;

    aput-object v5, v2, v10

    sget-object v5, Lkotlin/reflect/jvm/internal/impl/l/k$b;->a:Lkotlin/reflect/jvm/internal/impl/l/k$b;

    check-cast v5, Lkotlin/reflect/jvm/internal/impl/l/b;

    aput-object v5, v2, v9

    sget-object v5, Lkotlin/reflect/jvm/internal/impl/l/l$d;->a:Lkotlin/reflect/jvm/internal/impl/l/l$d;

    check-cast v5, Lkotlin/reflect/jvm/internal/impl/l/b;

    aput-object v5, v2, v11

    const/4 v8, 0x3

    sget-object v5, Lkotlin/reflect/jvm/internal/impl/l/h;->a:Lkotlin/reflect/jvm/internal/impl/l/h;

    check-cast v5, Lkotlin/reflect/jvm/internal/impl/l/b;

    aput-object v5, v2, v8

    move-object v5, v3

    invoke-direct/range {v0 .. v5}, Lkotlin/reflect/jvm/internal/impl/l/d;-><init>(Lkotlin/reflect/jvm/internal/impl/e/f;[Lkotlin/reflect/jvm/internal/impl/l/b;Lkotlin/d/a/b;ILkotlin/d/b/i;)V

    aput-object v0, v6, v7

    const/16 v7, 0xd

    new-instance v0, Lkotlin/reflect/jvm/internal/impl/l/d;

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/l/j;->J:Ljava/util/Set;

    check-cast v1, Ljava/util/Collection;

    const/4 v2, 0x3

    new-array v2, v2, [Lkotlin/reflect/jvm/internal/impl/l/b;

    sget-object v5, Lkotlin/reflect/jvm/internal/impl/l/f$b;->a:Lkotlin/reflect/jvm/internal/impl/l/f$b;

    check-cast v5, Lkotlin/reflect/jvm/internal/impl/l/b;

    aput-object v5, v2, v10

    sget-object v5, Lkotlin/reflect/jvm/internal/impl/l/l$d;->a:Lkotlin/reflect/jvm/internal/impl/l/l$d;

    check-cast v5, Lkotlin/reflect/jvm/internal/impl/l/b;

    aput-object v5, v2, v9

    sget-object v5, Lkotlin/reflect/jvm/internal/impl/l/h;->a:Lkotlin/reflect/jvm/internal/impl/l/h;

    check-cast v5, Lkotlin/reflect/jvm/internal/impl/l/b;

    aput-object v5, v2, v11

    move-object v5, v3

    invoke-direct/range {v0 .. v5}, Lkotlin/reflect/jvm/internal/impl/l/d;-><init>(Ljava/util/Collection;[Lkotlin/reflect/jvm/internal/impl/l/b;Lkotlin/d/a/b;ILkotlin/d/b/i;)V

    aput-object v0, v6, v7

    const/16 v7, 0xe

    new-instance v0, Lkotlin/reflect/jvm/internal/impl/l/d;

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/l/j;->I:Ljava/util/Set;

    check-cast v1, Ljava/util/Collection;

    new-array v2, v11, [Lkotlin/reflect/jvm/internal/impl/l/b;

    sget-object v5, Lkotlin/reflect/jvm/internal/impl/l/f$b;->a:Lkotlin/reflect/jvm/internal/impl/l/f$b;

    check-cast v5, Lkotlin/reflect/jvm/internal/impl/l/b;

    aput-object v5, v2, v10

    sget-object v5, Lkotlin/reflect/jvm/internal/impl/l/l$c;->a:Lkotlin/reflect/jvm/internal/impl/l/l$c;

    check-cast v5, Lkotlin/reflect/jvm/internal/impl/l/b;

    aput-object v5, v2, v9

    move-object v5, v3

    invoke-direct/range {v0 .. v5}, Lkotlin/reflect/jvm/internal/impl/l/d;-><init>(Ljava/util/Collection;[Lkotlin/reflect/jvm/internal/impl/l/b;Lkotlin/d/a/b;ILkotlin/d/b/i;)V

    aput-object v0, v6, v7

    const/16 v2, 0xf

    new-instance v5, Lkotlin/reflect/jvm/internal/impl/l/d;

    new-array v0, v11, [Lkotlin/reflect/jvm/internal/impl/e/f;

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/l/j;->p:Lkotlin/reflect/jvm/internal/impl/e/f;

    aput-object v1, v0, v10

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/l/j;->q:Lkotlin/reflect/jvm/internal/impl/e/f;

    aput-object v1, v0, v9

    invoke-static {v0}, Lkotlin/a/m;->b([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    new-array v7, v9, [Lkotlin/reflect/jvm/internal/impl/l/b;

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/l/f$b;->a:Lkotlin/reflect/jvm/internal/impl/l/f$b;

    check-cast v1, Lkotlin/reflect/jvm/internal/impl/l/b;

    aput-object v1, v7, v10

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/l/i$c;->a:Lkotlin/reflect/jvm/internal/impl/l/i$c;

    check-cast v1, Lkotlin/d/a/b;

    invoke-direct {v5, v0, v7, v1}, Lkotlin/reflect/jvm/internal/impl/l/d;-><init>(Ljava/util/Collection;[Lkotlin/reflect/jvm/internal/impl/l/b;Lkotlin/d/a/b;)V

    aput-object v5, v6, v2

    const/16 v7, 0x10

    new-instance v0, Lkotlin/reflect/jvm/internal/impl/l/d;

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/l/j;->K:Ljava/util/Set;

    check-cast v1, Ljava/util/Collection;

    new-array v2, v4, [Lkotlin/reflect/jvm/internal/impl/l/b;

    sget-object v5, Lkotlin/reflect/jvm/internal/impl/l/f$b;->a:Lkotlin/reflect/jvm/internal/impl/l/f$b;

    check-cast v5, Lkotlin/reflect/jvm/internal/impl/l/b;

    aput-object v5, v2, v10

    sget-object v5, Lkotlin/reflect/jvm/internal/impl/l/k$c;->a:Lkotlin/reflect/jvm/internal/impl/l/k$c;

    check-cast v5, Lkotlin/reflect/jvm/internal/impl/l/b;

    aput-object v5, v2, v9

    sget-object v5, Lkotlin/reflect/jvm/internal/impl/l/l$d;->a:Lkotlin/reflect/jvm/internal/impl/l/l$d;

    check-cast v5, Lkotlin/reflect/jvm/internal/impl/l/b;

    aput-object v5, v2, v11

    const/4 v8, 0x3

    sget-object v5, Lkotlin/reflect/jvm/internal/impl/l/h;->a:Lkotlin/reflect/jvm/internal/impl/l/h;

    check-cast v5, Lkotlin/reflect/jvm/internal/impl/l/b;

    aput-object v5, v2, v8

    move-object v5, v3

    invoke-direct/range {v0 .. v5}, Lkotlin/reflect/jvm/internal/impl/l/d;-><init>(Ljava/util/Collection;[Lkotlin/reflect/jvm/internal/impl/l/b;Lkotlin/d/a/b;ILkotlin/d/b/i;)V

    aput-object v0, v6, v7

    const/16 v7, 0x11

    new-instance v0, Lkotlin/reflect/jvm/internal/impl/l/d;

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/l/j;->m:Lkotlin/h/g;

    new-array v2, v11, [Lkotlin/reflect/jvm/internal/impl/l/b;

    sget-object v5, Lkotlin/reflect/jvm/internal/impl/l/f$b;->a:Lkotlin/reflect/jvm/internal/impl/l/f$b;

    check-cast v5, Lkotlin/reflect/jvm/internal/impl/l/b;

    aput-object v5, v2, v10

    sget-object v5, Lkotlin/reflect/jvm/internal/impl/l/l$c;->a:Lkotlin/reflect/jvm/internal/impl/l/l$c;

    check-cast v5, Lkotlin/reflect/jvm/internal/impl/l/b;

    aput-object v5, v2, v9

    move-object v5, v3

    invoke-direct/range {v0 .. v5}, Lkotlin/reflect/jvm/internal/impl/l/d;-><init>(Lkotlin/h/g;[Lkotlin/reflect/jvm/internal/impl/l/b;Lkotlin/d/a/b;ILkotlin/d/b/i;)V

    aput-object v0, v6, v7

    invoke-static {v6}, Lkotlin/a/m;->b([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/l/i;->b:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public a()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/l/d;",
            ">;"
        }
    .end annotation

    .prologue
    .line 178
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/l/i;->b:Ljava/util/List;

    return-object v0
.end method
