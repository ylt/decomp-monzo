.class public final Lkotlin/reflect/jvm/internal/impl/l/a/a;
.super Ljava/lang/Object;
.source "capitalizeDecapitalize.kt"


# direct methods
.method public static final a(Ljava/lang/String;)Ljava/lang/String;
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    const-string v0, "$receiver"

    invoke-static {p0, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v0, p0

    .line 62
    check-cast v0, Ljava/lang/CharSequence;

    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    :goto_0
    if-eqz v0, :cond_2

    .line 64
    :cond_0
    :goto_1
    return-object p0

    :cond_1
    move v0, v2

    .line 62
    goto :goto_0

    .line 63
    :cond_2
    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v3

    .line 64
    const/16 v0, 0x61

    if-gt v0, v3, :cond_3

    const/16 v0, 0x7a

    if-gt v3, v0, :cond_3

    move v0, v1

    :goto_2
    if-eqz v0, :cond_0

    .line 65
    invoke-static {v3}, Ljava/lang/Character;->toUpperCase(C)C

    move-result v0

    if-nez p0, :cond_4

    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type java.lang.String"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_3
    move v0, v2

    .line 64
    goto :goto_2

    .line 65
    :cond_4
    check-cast p0, Ljava/lang/String;

    invoke-virtual {p0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, "(this as java.lang.String).substring(startIndex)"

    invoke-static {v1, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {v0}, Ljava/lang/String;->valueOf(C)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    goto :goto_1
.end method

.method public static final a(Ljava/lang/String;Z)Ljava/lang/String;
    .locals 7

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    const-string v0, "$receiver"

    invoke-static {p0, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 25
    new-instance v1, Lkotlin/reflect/jvm/internal/impl/l/a/a$a;

    invoke-direct {v1, p0, p1}, Lkotlin/reflect/jvm/internal/impl/l/a/a$a;-><init>(Ljava/lang/String;Z)V

    move-object v0, p0

    .line 30
    check-cast v0, Ljava/lang/CharSequence;

    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-nez v0, :cond_1

    move v0, v4

    :goto_0
    if-nez v0, :cond_0

    move-object v0, v1

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/l/a/a$a;

    invoke-virtual {v0, v5}, Lkotlin/reflect/jvm/internal/impl/l/a/a$a;->a(I)Z

    move-result v0

    if-nez v0, :cond_2

    :cond_0
    move-object v0, p0

    .line 40
    :goto_1
    return-object v0

    :cond_1
    move v0, v5

    .line 30
    goto :goto_0

    .line 32
    :cond_2
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    if-eq v0, v4, :cond_3

    move-object v0, v1

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/l/a/a$a;

    invoke-virtual {v0, v4}, Lkotlin/reflect/jvm/internal/impl/l/a/a$a;->a(I)Z

    move-result v0

    if-nez v0, :cond_5

    .line 33
    :cond_3
    if-eqz p1, :cond_4

    invoke-static {p0}, Lkotlin/reflect/jvm/internal/impl/l/a/a;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_4
    invoke-static {p0}, Lkotlin/h/j;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 36
    :cond_5
    new-instance v3, Lkotlin/reflect/jvm/internal/impl/l/a/a$b;

    invoke-direct {v3, p1}, Lkotlin/reflect/jvm/internal/impl/l/a/a$b;-><init>(Z)V

    move-object v0, p0

    .line 38
    check-cast v0, Ljava/lang/CharSequence;

    invoke-static {v0}, Lkotlin/h/j;->d(Ljava/lang/CharSequence;)Lkotlin/f/c;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 98
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_6
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    move-object v0, v2

    check-cast v0, Ljava/lang/Number;

    invoke-virtual {v0}, Ljava/lang/Number;->intValue()I

    move-result v0

    .line 38
    invoke-virtual {v1, v0}, Lkotlin/reflect/jvm/internal/impl/l/a/a$a;->a(I)Z

    move-result v0

    if-nez v0, :cond_7

    move v0, v4

    :goto_2
    if-eqz v0, :cond_6

    move-object v0, v2

    .line 99
    :goto_3
    check-cast v0, Ljava/lang/Integer;

    if-eqz v0, :cond_9

    invoke-virtual {v0}, Ljava/lang/Number;->intValue()I

    move-result v0

    .line 39
    add-int/lit8 v1, v0, -0x1

    .line 40
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    check-cast v3, Lkotlin/reflect/jvm/internal/impl/l/a/a$b;

    if-nez p0, :cond_a

    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type java.lang.String"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_7
    move v0, v5

    .line 38
    goto :goto_2

    .line 99
    :cond_8
    const/4 v0, 0x0

    goto :goto_3

    :cond_9
    move-object v0, v3

    .line 39
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/l/a/a$b;

    invoke-virtual {v0, p0}, Lkotlin/reflect/jvm/internal/impl/l/a/a$b;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_a
    move-object v0, p0

    .line 40
    check-cast v0, Ljava/lang/String;

    invoke-virtual {v0, v5, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    const-string v4, "(this as java.lang.Strin\u2026ing(startIndex, endIndex)"

    invoke-static {v0, v4}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Lkotlin/reflect/jvm/internal/impl/l/a/a$b;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    if-nez p0, :cond_b

    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type java.lang.String"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_b
    check-cast p0, Ljava/lang/String;

    invoke-virtual {p0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, "(this as java.lang.String).substring(startIndex)"

    invoke-static {v1, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_1
.end method

.method public static final b(Ljava/lang/String;)Ljava/lang/String;
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    const-string v0, "$receiver"

    invoke-static {p0, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v0, p0

    .line 71
    check-cast v0, Ljava/lang/CharSequence;

    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    :goto_0
    if-eqz v0, :cond_2

    .line 73
    :cond_0
    :goto_1
    return-object p0

    :cond_1
    move v0, v2

    .line 71
    goto :goto_0

    .line 72
    :cond_2
    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v3

    .line 73
    const/16 v0, 0x41

    if-gt v0, v3, :cond_3

    const/16 v0, 0x5a

    if-gt v3, v0, :cond_3

    move v0, v1

    :goto_2
    if-eqz v0, :cond_0

    .line 74
    invoke-static {v3}, Ljava/lang/Character;->toLowerCase(C)C

    move-result v0

    if-nez p0, :cond_4

    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type java.lang.String"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_3
    move v0, v2

    .line 73
    goto :goto_2

    .line 74
    :cond_4
    check-cast p0, Ljava/lang/String;

    invoke-virtual {p0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, "(this as java.lang.String).substring(startIndex)"

    invoke-static {v1, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {v0}, Ljava/lang/String;->valueOf(C)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    goto :goto_1
.end method

.method public static final c(Ljava/lang/String;)Ljava/lang/String;
    .locals 4

    .prologue
    const-string v0, "$receiver"

    invoke-static {p0, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 80
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    invoke-direct {v2, v0}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 81
    check-cast p0, Ljava/lang/CharSequence;

    invoke-static {p0}, Lkotlin/h/j;->c(Ljava/lang/CharSequence;)Lkotlin/a/l;

    move-result-object v3

    :goto_0
    invoke-virtual {v3}, Lkotlin/a/l;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {v3}, Lkotlin/a/l;->b()C

    move-result v0

    .line 82
    const/16 v1, 0x41

    if-gt v1, v0, :cond_0

    const/16 v1, 0x5a

    if-gt v0, v1, :cond_0

    const/4 v1, 0x1

    :goto_1
    if-eqz v1, :cond_1

    invoke-static {v0}, Ljava/lang/Character;->toLowerCase(C)C

    move-result v0

    move-object v1, v2

    :goto_2
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    goto :goto_1

    :cond_1
    move-object v1, v2

    goto :goto_2

    .line 84
    :cond_2
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "builder.toString()"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method
