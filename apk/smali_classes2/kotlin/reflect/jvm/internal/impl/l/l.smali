.class public abstract Lkotlin/reflect/jvm/internal/impl/l/l;
.super Ljava/lang/Object;
.source "modifierChecks.kt"

# interfaces
.implements Lkotlin/reflect/jvm/internal/impl/l/b;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lkotlin/reflect/jvm/internal/impl/l/l$c;,
        Lkotlin/reflect/jvm/internal/impl/l/l$d;,
        Lkotlin/reflect/jvm/internal/impl/l/l$a;,
        Lkotlin/reflect/jvm/internal/impl/l/l$b;
    }
.end annotation


# instance fields
.field private final a:Ljava/lang/String;


# direct methods
.method private constructor <init>(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 79
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lkotlin/reflect/jvm/internal/impl/l/l;->a:Ljava/lang/String;

    return-void
.end method

.method public synthetic constructor <init>(Ljava/lang/String;Lkotlin/d/b/i;)V
    .locals 0

    .prologue
    .line 79
    invoke-direct {p0, p1}, Lkotlin/reflect/jvm/internal/impl/l/l;-><init>(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/l/l;->a:Ljava/lang/String;

    return-object v0
.end method

.method public b(Lkotlin/reflect/jvm/internal/impl/b/s;)Ljava/lang/String;
    .locals 1

    .prologue
    const-string v0, "functionDescriptor"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 79
    invoke-static {p0, p1}, Lkotlin/reflect/jvm/internal/impl/l/b$a;->a(Lkotlin/reflect/jvm/internal/impl/l/b;Lkotlin/reflect/jvm/internal/impl/b/s;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
