.class final Lkotlin/reflect/jvm/internal/impl/l/i$b;
.super Lkotlin/d/b/m;
.source "modifierChecks.kt"

# interfaces
.implements Lkotlin/d/a/b;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lkotlin/reflect/jvm/internal/impl/l/i;-><init>()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/d/b/m;",
        "Lkotlin/d/a/b",
        "<",
        "Lkotlin/reflect/jvm/internal/impl/b/s;",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:Lkotlin/reflect/jvm/internal/impl/l/i$b;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lkotlin/reflect/jvm/internal/impl/l/i$b;

    invoke-direct {v0}, Lkotlin/reflect/jvm/internal/impl/l/i$b;-><init>()V

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/l/i$b;->a:Lkotlin/reflect/jvm/internal/impl/l/i$b;

    return-void
.end method

.method constructor <init>()V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lkotlin/d/b/m;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 177
    check-cast p1, Lkotlin/reflect/jvm/internal/impl/b/s;

    invoke-virtual {p0, p1}, Lkotlin/reflect/jvm/internal/impl/l/i$b;->a(Lkotlin/reflect/jvm/internal/impl/b/s;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lkotlin/reflect/jvm/internal/impl/b/s;)Ljava/lang/String;
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    const-string v0, "$receiver"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 194
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/l/i$b$1;->a:Lkotlin/reflect/jvm/internal/impl/l/i$b$1;

    .line 195
    sget-object v3, Lkotlin/reflect/jvm/internal/impl/l/i;->a:Lkotlin/reflect/jvm/internal/impl/l/i;

    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/b/s;->y_()Lkotlin/reflect/jvm/internal/impl/b/m;

    move-result-object v3

    invoke-virtual {v0, v3}, Lkotlin/reflect/jvm/internal/impl/l/i$b$1;->a(Lkotlin/reflect/jvm/internal/impl/b/m;)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/b/s;->k()Ljava/util/Collection;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 217
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/s;

    .line 195
    sget-object v4, Lkotlin/reflect/jvm/internal/impl/l/i$b$1;->a:Lkotlin/reflect/jvm/internal/impl/l/i$b$1;

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/b/s;->y_()Lkotlin/reflect/jvm/internal/impl/b/m;

    move-result-object v0

    invoke-virtual {v4, v0}, Lkotlin/reflect/jvm/internal/impl/l/i$b$1;->a(Lkotlin/reflect/jvm/internal/impl/b/m;)Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v2

    :goto_0
    if-eqz v0, :cond_3

    :cond_1
    move v0, v2

    .line 219
    :goto_1
    if-nez v0, :cond_4

    .line 195
    const-string v0, "must override \'\'equals()\'\' in Any"

    check-cast v0, Ljava/lang/String;

    :goto_2
    return-object v0

    :cond_2
    move v0, v1

    .line 218
    goto :goto_0

    :cond_3
    move v0, v1

    .line 195
    goto :goto_1

    :cond_4
    const/4 v0, 0x0

    goto :goto_2
.end method
