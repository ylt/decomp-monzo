.class public final Lkotlin/reflect/jvm/internal/impl/l/f$b;
.super Lkotlin/reflect/jvm/internal/impl/l/f;
.source "modifierChecks.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lkotlin/reflect/jvm/internal/impl/l/f;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "b"
.end annotation


# static fields
.field public static final a:Lkotlin/reflect/jvm/internal/impl/l/f$b;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 69
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/l/f$b;

    invoke-direct {v0}, Lkotlin/reflect/jvm/internal/impl/l/f$b;-><init>()V

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    .line 69
    const-string v0, "must be a member or an extension function"

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lkotlin/reflect/jvm/internal/impl/l/f;-><init>(Ljava/lang/String;Lkotlin/d/b/i;)V

    check-cast p0, Lkotlin/reflect/jvm/internal/impl/l/f$b;

    sput-object p0, Lkotlin/reflect/jvm/internal/impl/l/f$b;->a:Lkotlin/reflect/jvm/internal/impl/l/f$b;

    return-void
.end method


# virtual methods
.method public a(Lkotlin/reflect/jvm/internal/impl/b/s;)Z
    .locals 1

    .prologue
    const-string v0, "functionDescriptor"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 71
    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/b/s;->e()Lkotlin/reflect/jvm/internal/impl/b/aj;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/b/s;->d()Lkotlin/reflect/jvm/internal/impl/b/aj;

    move-result-object v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
