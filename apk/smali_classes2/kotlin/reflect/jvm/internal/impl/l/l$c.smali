.class public final Lkotlin/reflect/jvm/internal/impl/l/l$c;
.super Lkotlin/reflect/jvm/internal/impl/l/l;
.source "modifierChecks.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lkotlin/reflect/jvm/internal/impl/l/l;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "c"
.end annotation


# static fields
.field public static final a:Lkotlin/reflect/jvm/internal/impl/l/l$c;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 80
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/l/l$c;

    invoke-direct {v0}, Lkotlin/reflect/jvm/internal/impl/l/l$c;-><init>()V

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    .line 80
    const-string v0, "must have no value parameters"

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lkotlin/reflect/jvm/internal/impl/l/l;-><init>(Ljava/lang/String;Lkotlin/d/b/i;)V

    check-cast p0, Lkotlin/reflect/jvm/internal/impl/l/l$c;

    sput-object p0, Lkotlin/reflect/jvm/internal/impl/l/l$c;->a:Lkotlin/reflect/jvm/internal/impl/l/l$c;

    return-void
.end method


# virtual methods
.method public a(Lkotlin/reflect/jvm/internal/impl/b/s;)Z
    .locals 1

    .prologue
    const-string v0, "functionDescriptor"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 81
    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/b/s;->i()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    return v0
.end method
