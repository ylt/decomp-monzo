.class public final Lkotlin/reflect/jvm/internal/impl/e/b;
.super Ljava/lang/Object;
.source "FqName.java"


# static fields
.field public static final a:Lkotlin/reflect/jvm/internal/impl/e/b;


# instance fields
.field private final b:Lkotlin/reflect/jvm/internal/impl/e/c;

.field private transient c:Lkotlin/reflect/jvm/internal/impl/e/b;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 31
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/e/b;

    const-string v1, ""

    invoke-direct {v0, v1}, Lkotlin/reflect/jvm/internal/impl/e/b;-><init>(Ljava/lang/String;)V

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/e/b;->a:Lkotlin/reflect/jvm/internal/impl/e/b;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 5

    .prologue
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "fqName"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "kotlin/reflect/jvm/internal/impl/name/FqName"

    aput-object v4, v2, v3

    const/4 v3, 0x2

    const-string v4, "<init>"

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 39
    :cond_0
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/e/c;

    invoke-direct {v0, p1, p0}, Lkotlin/reflect/jvm/internal/impl/e/c;-><init>(Ljava/lang/String;Lkotlin/reflect/jvm/internal/impl/e/b;)V

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/e/b;->b:Lkotlin/reflect/jvm/internal/impl/e/c;

    .line 41
    return-void
.end method

.method public constructor <init>(Lkotlin/reflect/jvm/internal/impl/e/c;)V
    .locals 5

    .prologue
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "fqName"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "kotlin/reflect/jvm/internal/impl/name/FqName"

    aput-object v4, v2, v3

    const/4 v3, 0x2

    const-string v4, "<init>"

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 43
    :cond_0
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    iput-object p1, p0, Lkotlin/reflect/jvm/internal/impl/e/b;->b:Lkotlin/reflect/jvm/internal/impl/e/c;

    .line 45
    return-void
.end method

.method private constructor <init>(Lkotlin/reflect/jvm/internal/impl/e/c;Lkotlin/reflect/jvm/internal/impl/e/b;)V
    .locals 5

    .prologue
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "fqName"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "kotlin/reflect/jvm/internal/impl/name/FqName"

    aput-object v4, v2, v3

    const/4 v3, 0x2

    const-string v4, "<init>"

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 47
    :cond_0
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    iput-object p1, p0, Lkotlin/reflect/jvm/internal/impl/e/b;->b:Lkotlin/reflect/jvm/internal/impl/e/c;

    .line 49
    iput-object p2, p0, Lkotlin/reflect/jvm/internal/impl/e/b;->c:Lkotlin/reflect/jvm/internal/impl/e/b;

    .line 50
    return-void
.end method

.method public static a(Ljava/util/List;)Lkotlin/reflect/jvm/internal/impl/e/b;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lkotlin/reflect/jvm/internal/impl/e/b;"
        }
    .end annotation

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    if-nez p0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "names"

    aput-object v3, v2, v4

    const-string v3, "kotlin/reflect/jvm/internal/impl/name/FqName"

    aput-object v3, v2, v5

    const-string v3, "fromSegments"

    aput-object v3, v2, v6

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 28
    :cond_0
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/e/b;

    const-string v1, "."

    invoke-static {p0, v1}, Lkotlin/reflect/jvm/internal/impl/utils/g;->a(Ljava/lang/Iterable;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lkotlin/reflect/jvm/internal/impl/e/b;-><init>(Ljava/lang/String;)V

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "@NotNull method %s.%s must not return null"

    new-array v2, v6, [Ljava/lang/Object;

    const-string v3, "kotlin/reflect/jvm/internal/impl/name/FqName"

    aput-object v3, v2, v4

    const-string v3, "fromSegments"

    aput-object v3, v2, v5

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    return-object v0
.end method

.method public static b(Lkotlin/reflect/jvm/internal/impl/e/f;)Lkotlin/reflect/jvm/internal/impl/e/b;
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    if-nez p0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "shortName"

    aput-object v3, v2, v4

    const-string v3, "kotlin/reflect/jvm/internal/impl/name/FqName"

    aput-object v3, v2, v5

    const-string v3, "topLevel"

    aput-object v3, v2, v6

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 103
    :cond_0
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/e/b;

    invoke-static {p0}, Lkotlin/reflect/jvm/internal/impl/e/c;->c(Lkotlin/reflect/jvm/internal/impl/e/f;)Lkotlin/reflect/jvm/internal/impl/e/c;

    move-result-object v1

    invoke-direct {v0, v1}, Lkotlin/reflect/jvm/internal/impl/e/b;-><init>(Lkotlin/reflect/jvm/internal/impl/e/c;)V

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "@NotNull method %s.%s must not return null"

    new-array v2, v6, [Ljava/lang/Object;

    const-string v3, "kotlin/reflect/jvm/internal/impl/name/FqName"

    aput-object v3, v2, v4

    const-string v3, "topLevel"

    aput-object v3, v2, v5

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    return-object v0
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 5

    .prologue
    .line 54
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/e/b;->b:Lkotlin/reflect/jvm/internal/impl/e/c;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/e/c;->a()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "@NotNull method %s.%s must not return null"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "kotlin/reflect/jvm/internal/impl/name/FqName"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "asString"

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    return-object v0
.end method

.method public a(Lkotlin/reflect/jvm/internal/impl/e/f;)Lkotlin/reflect/jvm/internal/impl/e/b;
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "name"

    aput-object v3, v2, v4

    const-string v3, "kotlin/reflect/jvm/internal/impl/name/FqName"

    aput-object v3, v2, v5

    const-string v3, "child"

    aput-object v3, v2, v6

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 83
    :cond_0
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/e/b;

    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/e/b;->b:Lkotlin/reflect/jvm/internal/impl/e/c;

    invoke-virtual {v1, p1}, Lkotlin/reflect/jvm/internal/impl/e/c;->a(Lkotlin/reflect/jvm/internal/impl/e/f;)Lkotlin/reflect/jvm/internal/impl/e/c;

    move-result-object v1

    invoke-direct {v0, v1, p0}, Lkotlin/reflect/jvm/internal/impl/e/b;-><init>(Lkotlin/reflect/jvm/internal/impl/e/c;Lkotlin/reflect/jvm/internal/impl/e/b;)V

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "@NotNull method %s.%s must not return null"

    new-array v2, v6, [Ljava/lang/Object;

    const-string v3, "kotlin/reflect/jvm/internal/impl/name/FqName"

    aput-object v3, v2, v4

    const-string v3, "child"

    aput-object v3, v2, v5

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    return-object v0
.end method

.method public b()Lkotlin/reflect/jvm/internal/impl/e/c;
    .locals 5

    .prologue
    .line 59
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/e/b;->b:Lkotlin/reflect/jvm/internal/impl/e/c;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "@NotNull method %s.%s must not return null"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "kotlin/reflect/jvm/internal/impl/name/FqName"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "toUnsafe"

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    return-object v0
.end method

.method public c()Z
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/e/b;->b:Lkotlin/reflect/jvm/internal/impl/e/c;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/e/c;->d()Z

    move-result v0

    return v0
.end method

.method public d()Lkotlin/reflect/jvm/internal/impl/e/b;
    .locals 6

    .prologue
    const/4 v2, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 68
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/e/b;->c:Lkotlin/reflect/jvm/internal/impl/e/b;

    if-eqz v0, :cond_0

    .line 69
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/e/b;->c:Lkotlin/reflect/jvm/internal/impl/e/b;

    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "@NotNull method %s.%s must not return null"

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "kotlin/reflect/jvm/internal/impl/name/FqName"

    aput-object v3, v2, v4

    const-string v3, "parent"

    aput-object v3, v2, v5

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 72
    :cond_0
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/e/b;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 73
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "root"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 76
    :cond_1
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/e/b;

    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/e/b;->b:Lkotlin/reflect/jvm/internal/impl/e/c;

    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/impl/e/c;->e()Lkotlin/reflect/jvm/internal/impl/e/c;

    move-result-object v1

    invoke-direct {v0, v1}, Lkotlin/reflect/jvm/internal/impl/e/b;-><init>(Lkotlin/reflect/jvm/internal/impl/e/c;)V

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/e/b;->c:Lkotlin/reflect/jvm/internal/impl/e/b;

    .line 78
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/e/b;->c:Lkotlin/reflect/jvm/internal/impl/e/b;

    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "@NotNull method %s.%s must not return null"

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "kotlin/reflect/jvm/internal/impl/name/FqName"

    aput-object v3, v2, v4

    const-string v3, "parent"

    aput-object v3, v2, v5

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    return-object v0
.end method

.method public e()Lkotlin/reflect/jvm/internal/impl/e/f;
    .locals 5

    .prologue
    .line 88
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/e/b;->b:Lkotlin/reflect/jvm/internal/impl/e/c;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/e/c;->f()Lkotlin/reflect/jvm/internal/impl/e/f;

    move-result-object v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "@NotNull method %s.%s must not return null"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "kotlin/reflect/jvm/internal/impl/name/FqName"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "shortName"

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 113
    if-ne p0, p1, :cond_1

    .line 120
    :cond_0
    :goto_0
    return v0

    .line 114
    :cond_1
    instance-of v2, p1, Lkotlin/reflect/jvm/internal/impl/e/b;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    .line 116
    :cond_2
    check-cast p1, Lkotlin/reflect/jvm/internal/impl/e/b;

    .line 118
    iget-object v2, p0, Lkotlin/reflect/jvm/internal/impl/e/b;->b:Lkotlin/reflect/jvm/internal/impl/e/c;

    iget-object v3, p1, Lkotlin/reflect/jvm/internal/impl/e/b;->b:Lkotlin/reflect/jvm/internal/impl/e/c;

    invoke-virtual {v2, v3}, Lkotlin/reflect/jvm/internal/impl/e/c;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method public f()Lkotlin/reflect/jvm/internal/impl/e/f;
    .locals 5

    .prologue
    .line 93
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/e/b;->b:Lkotlin/reflect/jvm/internal/impl/e/c;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/e/c;->g()Lkotlin/reflect/jvm/internal/impl/e/f;

    move-result-object v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "@NotNull method %s.%s must not return null"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "kotlin/reflect/jvm/internal/impl/name/FqName"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "shortNameOrSpecial"

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    return-object v0
.end method

.method public g()Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/e/f;",
            ">;"
        }
    .end annotation

    .prologue
    .line 98
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/e/b;->b:Lkotlin/reflect/jvm/internal/impl/e/c;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/e/c;->h()Ljava/util/List;

    move-result-object v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "@NotNull method %s.%s must not return null"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "kotlin/reflect/jvm/internal/impl/name/FqName"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "pathSegments"

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    return-object v0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 125
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/e/b;->b:Lkotlin/reflect/jvm/internal/impl/e/c;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/e/c;->hashCode()I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 108
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/e/b;->b:Lkotlin/reflect/jvm/internal/impl/e/c;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/e/c;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
