.class public final Lkotlin/reflect/jvm/internal/impl/e/g;
.super Ljava/lang/Object;
.source "NameUtils.kt"


# static fields
.field public static final a:Lkotlin/reflect/jvm/internal/impl/e/g;

.field private static final b:Lkotlin/h/g;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 21
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/e/g;

    invoke-direct {v0}, Lkotlin/reflect/jvm/internal/impl/e/g;-><init>()V

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    check-cast p0, Lkotlin/reflect/jvm/internal/impl/e/g;

    sput-object p0, Lkotlin/reflect/jvm/internal/impl/e/g;->a:Lkotlin/reflect/jvm/internal/impl/e/g;

    .line 22
    const-string v0, "[^\\p{L}\\p{Digit}]"

    new-instance v1, Lkotlin/h/g;

    invoke-direct {v1, v0}, Lkotlin/h/g;-><init>(Ljava/lang/String;)V

    sput-object v1, Lkotlin/reflect/jvm/internal/impl/e/g;->b:Lkotlin/h/g;

    return-void
.end method

.method public static final a(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    const-string v0, "name"

    invoke-static {p0, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 26
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/e/g;->b:Lkotlin/h/g;

    check-cast p0, Ljava/lang/CharSequence;

    const-string v1, "_"

    invoke-virtual {v0, p0, v1}, Lkotlin/h/g;->a(Ljava/lang/CharSequence;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
