.class public Lkotlin/reflect/jvm/internal/impl/e/h;
.super Ljava/lang/Object;
.source "SpecialNames.java"


# static fields
.field public static final a:Lkotlin/reflect/jvm/internal/impl/e/f;

.field public static final b:Lkotlin/reflect/jvm/internal/impl/e/f;

.field public static final c:Lkotlin/reflect/jvm/internal/impl/e/f;

.field public static final d:Lkotlin/reflect/jvm/internal/impl/e/f;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 23
    const-string v0, "<no name provided>"

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/e/f;->c(Ljava/lang/String;)Lkotlin/reflect/jvm/internal/impl/e/f;

    move-result-object v0

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/e/h;->a:Lkotlin/reflect/jvm/internal/impl/e/f;

    .line 24
    const-string v0, "<root package>"

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/e/f;->c(Ljava/lang/String;)Lkotlin/reflect/jvm/internal/impl/e/f;

    move-result-object v0

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/e/h;->b:Lkotlin/reflect/jvm/internal/impl/e/f;

    .line 26
    const-string v0, "Companion"

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/e/f;->a(Ljava/lang/String;)Lkotlin/reflect/jvm/internal/impl/e/f;

    move-result-object v0

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/e/h;->c:Lkotlin/reflect/jvm/internal/impl/e/f;

    .line 32
    const-string v0, "no_name_in_PSI_3d19d79d_1ba9_4cd0_b7f5_b46aa3cd5d40"

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/e/f;->a(Ljava/lang/String;)Lkotlin/reflect/jvm/internal/impl/e/f;

    move-result-object v0

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/e/h;->d:Lkotlin/reflect/jvm/internal/impl/e/f;

    return-void
.end method

.method public static a(Lkotlin/reflect/jvm/internal/impl/e/f;)Lkotlin/reflect/jvm/internal/impl/e/f;
    .locals 5

    .prologue
    .line 36
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/e/f;->c()Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    if-nez p0, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "@NotNull method %s.%s must not return null"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "kotlin/reflect/jvm/internal/impl/name/SpecialNames"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "safeIdentifier"

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    sget-object p0, Lkotlin/reflect/jvm/internal/impl/e/h;->d:Lkotlin/reflect/jvm/internal/impl/e/f;

    goto :goto_0

    :cond_1
    return-object p0
.end method

.method public static b(Lkotlin/reflect/jvm/internal/impl/e/f;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    if-nez p0, :cond_0

    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/Object;

    const-string v5, "name"

    aput-object v5, v4, v1

    const-string v1, "kotlin/reflect/jvm/internal/impl/name/SpecialNames"

    aput-object v1, v4, v0

    const/4 v0, 0x2

    const-string v1, "isSafeIdentifier"

    aput-object v1, v4, v0

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 45
    :cond_0
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/e/f;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_1

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/e/f;->c()Z

    move-result v2

    if-nez v2, :cond_1

    :goto_0
    return v0

    :cond_1
    move v0, v1

    goto :goto_0
.end method
