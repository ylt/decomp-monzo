.class public final Lkotlin/reflect/jvm/internal/impl/e/d;
.super Ljava/lang/Object;
.source "FqNamesUtil.kt"


# direct methods
.method public static final a(Ljava/lang/String;)Z
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 62
    if-nez p0, :cond_0

    move v0, v1

    .line 81
    :goto_0
    return v0

    .line 64
    :cond_0
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/e/i;->a:Lkotlin/reflect/jvm/internal/impl/e/i;

    .line 66
    check-cast p0, Ljava/lang/CharSequence;

    invoke-static {p0}, Lkotlin/h/j;->c(Ljava/lang/CharSequence;)Lkotlin/a/l;

    move-result-object v2

    :cond_1
    :goto_1
    invoke-virtual {v2}, Lkotlin/a/l;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-virtual {v2}, Lkotlin/a/l;->b()C

    move-result v3

    .line 67
    sget-object v4, Lkotlin/reflect/jvm/internal/impl/e/e;->a:[I

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/e/i;->ordinal()I

    move-result v5

    aget v4, v4, v5

    packed-switch v4, :pswitch_data_0

    goto :goto_1

    .line 69
    :pswitch_0
    invoke-static {v3}, Ljava/lang/Character;->isJavaIdentifierPart(C)Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    goto :goto_0

    .line 70
    :cond_2
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/e/i;->b:Lkotlin/reflect/jvm/internal/impl/e/i;

    goto :goto_1

    .line 73
    :pswitch_1
    const/16 v4, 0x2e

    if-ne v3, v4, :cond_3

    .line 74
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/e/i;->c:Lkotlin/reflect/jvm/internal/impl/e/i;

    goto :goto_1

    .line 76
    :cond_3
    invoke-static {v3}, Ljava/lang/Character;->isJavaIdentifierPart(C)Z

    move-result v3

    if-nez v3, :cond_1

    move v0, v1

    goto :goto_0

    .line 81
    :cond_4
    sget-object v1, Lkotlin/reflect/jvm/internal/impl/e/i;->c:Lkotlin/reflect/jvm/internal/impl/e/i;

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 67
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
