.class public final Lkotlin/reflect/jvm/internal/impl/f/b;
.super Lkotlin/reflect/jvm/internal/impl/a/l;
.source "JvmBuiltIns.kt"


# static fields
.field static final synthetic a:[Lkotlin/reflect/l;


# instance fields
.field private k:Lkotlin/reflect/jvm/internal/impl/b/w;

.field private l:Z

.field private final m:Lkotlin/reflect/jvm/internal/impl/j/f;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const/4 v0, 0x1

    new-array v1, v0, [Lkotlin/reflect/l;

    const/4 v2, 0x0

    new-instance v0, Lkotlin/d/b/w;

    const-class v3, Lkotlin/reflect/jvm/internal/impl/f/b;

    invoke-static {v3}, Lkotlin/d/b/y;->a(Ljava/lang/Class;)Lkotlin/reflect/c;

    move-result-object v3

    const-string v4, "settings"

    const-string v5, "getSettings()Lorg/jetbrains/kotlin/load/kotlin/JvmBuiltInsSettings;"

    invoke-direct {v0, v3, v4, v5}, Lkotlin/d/b/w;-><init>(Lkotlin/reflect/e;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v0}, Lkotlin/d/b/y;->a(Lkotlin/d/b/v;)Lkotlin/reflect/n;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/l;

    aput-object v0, v1, v2

    sput-object v1, Lkotlin/reflect/jvm/internal/impl/f/b;->a:[Lkotlin/reflect/l;

    return-void
.end method

.method public constructor <init>(Lkotlin/reflect/jvm/internal/impl/j/i;Z)V
    .locals 1

    .prologue
    const-string v0, "storageManager"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 32
    invoke-direct {p0, p1}, Lkotlin/reflect/jvm/internal/impl/a/l;-><init>(Lkotlin/reflect/jvm/internal/impl/j/i;)V

    .line 35
    const/4 v0, 0x1

    iput-boolean v0, p0, Lkotlin/reflect/jvm/internal/impl/f/b;->l:Z

    .line 43
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/f/b$a;

    invoke-direct {v0, p0, p1}, Lkotlin/reflect/jvm/internal/impl/f/b$a;-><init>(Lkotlin/reflect/jvm/internal/impl/f/b;Lkotlin/reflect/jvm/internal/impl/j/i;)V

    check-cast v0, Lkotlin/d/a/a;

    invoke-interface {p1, v0}, Lkotlin/reflect/jvm/internal/impl/j/i;->a(Lkotlin/d/a/a;)Lkotlin/reflect/jvm/internal/impl/j/f;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/f/b;->m:Lkotlin/reflect/jvm/internal/impl/j/f;

    .line 55
    if-eqz p2, :cond_0

    .line 56
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/f/b;->c()V

    :cond_0
    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/reflect/jvm/internal/impl/j/i;ZILkotlin/d/b/i;)V
    .locals 1

    .prologue
    and-int/lit8 v0, p3, 0x2

    if-eqz v0, :cond_0

    .line 31
    const/4 p2, 0x1

    :cond_0
    invoke-direct {p0, p1, p2}, Lkotlin/reflect/jvm/internal/impl/f/b;-><init>(Lkotlin/reflect/jvm/internal/impl/j/i;Z)V

    return-void
.end method

.method public static final synthetic a(Lkotlin/reflect/jvm/internal/impl/f/b;)Lkotlin/reflect/jvm/internal/impl/b/w;
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/f/b;->k:Lkotlin/reflect/jvm/internal/impl/b/w;

    return-object v0
.end method

.method public static final synthetic b(Lkotlin/reflect/jvm/internal/impl/f/b;)Z
    .locals 1

    .prologue
    .line 29
    iget-boolean v0, p0, Lkotlin/reflect/jvm/internal/impl/f/b;->l:Z

    return v0
.end method


# virtual methods
.method public final a()Lkotlin/reflect/jvm/internal/impl/d/b/i;
    .locals 3

    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/f/b;->m:Lkotlin/reflect/jvm/internal/impl/j/f;

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/f/b;->a:[Lkotlin/reflect/l;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-static {v0, p0, v1}, Lkotlin/reflect/jvm/internal/impl/j/h;->a(Lkotlin/reflect/jvm/internal/impl/j/f;Ljava/lang/Object;Lkotlin/reflect/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/d/b/i;

    return-object v0
.end method

.method public final a(Lkotlin/reflect/jvm/internal/impl/b/w;Z)V
    .locals 2

    .prologue
    const-string v0, "moduleDescriptor"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 38
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/f/b;->k:Lkotlin/reflect/jvm/internal/impl/b/w;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sget-boolean v1, Lkotlin/p;->a:Z

    if-eqz v1, :cond_1

    if-nez v0, :cond_1

    const-string v1, "JvmBuiltins repeated initialization"

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 39
    :cond_1
    iput-object p1, p0, Lkotlin/reflect/jvm/internal/impl/f/b;->k:Lkotlin/reflect/jvm/internal/impl/b/w;

    .line 40
    iput-boolean p2, p0, Lkotlin/reflect/jvm/internal/impl/f/b;->l:Z

    .line 41
    return-void
.end method

.method protected b()Ljava/util/List;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/i/b/i;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 65
    invoke-super {p0}, Lkotlin/reflect/jvm/internal/impl/a/l;->f()Ljava/lang/Iterable;

    move-result-object v6

    new-instance v0, Lkotlin/reflect/jvm/internal/impl/a/k;

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/f/b;->g()Lkotlin/reflect/jvm/internal/impl/j/i;

    move-result-object v1

    const-string v2, "storageManager"

    invoke-static {v1, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/f/b;->h()Lkotlin/reflect/jvm/internal/impl/b/b/u;

    move-result-object v2

    const-string v4, "builtInsModule"

    invoke-static {v2, v4}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v2, Lkotlin/reflect/jvm/internal/impl/b/w;

    const/4 v4, 0x4

    move-object v5, v3

    invoke-direct/range {v0 .. v5}, Lkotlin/reflect/jvm/internal/impl/a/k;-><init>(Lkotlin/reflect/jvm/internal/impl/j/i;Lkotlin/reflect/jvm/internal/impl/b/w;Lkotlin/d/a/b;ILkotlin/d/b/i;)V

    invoke-static {v6, v0}, Lkotlin/a/m;->d(Ljava/lang/Iterable;Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method protected d()Lkotlin/reflect/jvm/internal/impl/i/b/a;
    .locals 1

    .prologue
    .line 62
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/f/b;->a()Lkotlin/reflect/jvm/internal/impl/d/b/i;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/i/b/a;

    return-object v0
.end method

.method protected e()Lkotlin/reflect/jvm/internal/impl/i/b/ac;
    .locals 1

    .prologue
    .line 60
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/f/b;->a()Lkotlin/reflect/jvm/internal/impl/d/b/i;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/i/b/ac;

    return-object v0
.end method

.method public synthetic f()Ljava/lang/Iterable;
    .locals 1

    .prologue
    .line 29
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/f/b;->b()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    return-object v0
.end method
