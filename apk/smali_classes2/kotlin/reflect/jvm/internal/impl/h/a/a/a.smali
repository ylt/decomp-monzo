.class public final Lkotlin/reflect/jvm/internal/impl/h/a/a/a;
.super Lkotlin/reflect/jvm/internal/impl/k/v;
.source "CapturedTypeConstructor.kt"

# interfaces
.implements Lkotlin/reflect/jvm/internal/impl/k/aa;


# instance fields
.field private final a:Lkotlin/reflect/jvm/internal/impl/k/ah;

.field private final b:Lkotlin/reflect/jvm/internal/impl/h/a/a/b;

.field private final c:Z

.field private final d:Lkotlin/reflect/jvm/internal/impl/b/a/h;


# direct methods
.method public constructor <init>(Lkotlin/reflect/jvm/internal/impl/k/ah;Lkotlin/reflect/jvm/internal/impl/h/a/a/b;ZLkotlin/reflect/jvm/internal/impl/b/a/h;)V
    .locals 1

    .prologue
    const-string v0, "typeProjection"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "constructor"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "annotations"

    invoke-static {p4, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 67
    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/k/v;-><init>()V

    iput-object p1, p0, Lkotlin/reflect/jvm/internal/impl/h/a/a/a;->a:Lkotlin/reflect/jvm/internal/impl/k/ah;

    iput-object p2, p0, Lkotlin/reflect/jvm/internal/impl/h/a/a/a;->b:Lkotlin/reflect/jvm/internal/impl/h/a/a/b;

    iput-boolean p3, p0, Lkotlin/reflect/jvm/internal/impl/h/a/a/a;->c:Z

    iput-object p4, p0, Lkotlin/reflect/jvm/internal/impl/h/a/a/a;->d:Lkotlin/reflect/jvm/internal/impl/b/a/h;

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/reflect/jvm/internal/impl/k/ah;Lkotlin/reflect/jvm/internal/impl/h/a/a/b;ZLkotlin/reflect/jvm/internal/impl/b/a/h;ILkotlin/d/b/i;)V
    .locals 1

    .prologue
    and-int/lit8 v0, p5, 0x2

    if-eqz v0, :cond_0

    .line 64
    new-instance p2, Lkotlin/reflect/jvm/internal/impl/h/a/a/b;

    invoke-direct {p2, p1}, Lkotlin/reflect/jvm/internal/impl/h/a/a/b;-><init>(Lkotlin/reflect/jvm/internal/impl/k/ah;)V

    :cond_0
    and-int/lit8 v0, p5, 0x4

    if-eqz v0, :cond_1

    .line 65
    const/4 p3, 0x0

    :cond_1
    and-int/lit8 v0, p5, 0x8

    if-eqz v0, :cond_2

    .line 66
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/b/a/h;->a:Lkotlin/reflect/jvm/internal/impl/b/a/h$a;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/b/a/h$a;->a()Lkotlin/reflect/jvm/internal/impl/b/a/h;

    move-result-object p4

    :cond_2
    invoke-direct {p0, p1, p2, p3, p4}, Lkotlin/reflect/jvm/internal/impl/h/a/a/a;-><init>(Lkotlin/reflect/jvm/internal/impl/k/ah;Lkotlin/reflect/jvm/internal/impl/h/a/a/b;ZLkotlin/reflect/jvm/internal/impl/b/a/h;)V

    return-void
.end method

.method private final a(Lkotlin/reflect/jvm/internal/impl/k/ap;Lkotlin/reflect/jvm/internal/impl/k/r;)Lkotlin/reflect/jvm/internal/impl/k/r;
    .locals 1

    .prologue
    .line 84
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/h/a/a/a;->a:Lkotlin/reflect/jvm/internal/impl/k/ah;

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/k/ah;->b()Lkotlin/reflect/jvm/internal/impl/k/ap;

    move-result-object v0

    invoke-static {v0, p1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/h/a/a/a;->a:Lkotlin/reflect/jvm/internal/impl/k/ah;

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/k/ah;->c()Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object p2

    :cond_0
    return-object p2
.end method


# virtual methods
.method public a()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/k/ah;",
            ">;"
        }
    .end annotation

    .prologue
    .line 70
    invoke-static {}, Lkotlin/a/m;->a()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public a(Lkotlin/reflect/jvm/internal/impl/b/a/h;)Lkotlin/reflect/jvm/internal/impl/h/a/a/a;
    .locals 4

    .prologue
    const-string v0, "newAnnotations"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 95
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/h/a/a/a;

    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/h/a/a/a;->a:Lkotlin/reflect/jvm/internal/impl/k/ah;

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/h/a/a/a;->h()Lkotlin/reflect/jvm/internal/impl/h/a/a/b;

    move-result-object v2

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/h/a/a/a;->c()Z

    move-result v3

    invoke-direct {v0, v1, v2, v3, p1}, Lkotlin/reflect/jvm/internal/impl/h/a/a/a;-><init>(Lkotlin/reflect/jvm/internal/impl/k/ah;Lkotlin/reflect/jvm/internal/impl/h/a/a/b;ZLkotlin/reflect/jvm/internal/impl/b/a/h;)V

    return-object v0
.end method

.method public synthetic a(Z)Lkotlin/reflect/jvm/internal/impl/k/v;
    .locals 1

    .prologue
    .line 62
    invoke-virtual {p0, p1}, Lkotlin/reflect/jvm/internal/impl/h/a/a/a;->c(Z)Lkotlin/reflect/jvm/internal/impl/h/a/a/a;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/v;

    return-object v0
.end method

.method public a(Lkotlin/reflect/jvm/internal/impl/k/r;)Z
    .locals 2

    .prologue
    const-string v0, "type"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 86
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/h/a/a/a;->h()Lkotlin/reflect/jvm/internal/impl/h/a/a/b;

    move-result-object v0

    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/k/r;->i()Lkotlin/reflect/jvm/internal/impl/k/ad;

    move-result-object v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()Lkotlin/reflect/jvm/internal/impl/h/e/h;
    .locals 2

    .prologue
    .line 72
    const-string v0, "No member resolution should be done on captured type, it used only during constraint system resolution"

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lkotlin/reflect/jvm/internal/impl/k/l;->a(Ljava/lang/String;Z)Lkotlin/reflect/jvm/internal/impl/h/e/h;

    move-result-object v0

    const-string v1, "ErrorUtils.createErrorSc\u2026system resolution\", true)"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 73
    return-object v0
.end method

.method public synthetic b(Lkotlin/reflect/jvm/internal/impl/b/a/h;)Lkotlin/reflect/jvm/internal/impl/k/ao;
    .locals 1

    .prologue
    .line 62
    invoke-virtual {p0, p1}, Lkotlin/reflect/jvm/internal/impl/h/a/a/a;->a(Lkotlin/reflect/jvm/internal/impl/b/a/h;)Lkotlin/reflect/jvm/internal/impl/h/a/a/a;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/ao;

    return-object v0
.end method

.method public synthetic b(Z)Lkotlin/reflect/jvm/internal/impl/k/ao;
    .locals 1

    .prologue
    .line 62
    invoke-virtual {p0, p1}, Lkotlin/reflect/jvm/internal/impl/h/a/a/a;->c(Z)Lkotlin/reflect/jvm/internal/impl/h/a/a/a;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/ao;

    return-object v0
.end method

.method public c(Z)Lkotlin/reflect/jvm/internal/impl/h/a/a/a;
    .locals 4

    .prologue
    .line 91
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/h/a/a/a;->c()Z

    move-result v0

    if-ne p1, v0, :cond_0

    .line 92
    :goto_0
    return-object p0

    :cond_0
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/h/a/a/a;

    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/h/a/a/a;->a:Lkotlin/reflect/jvm/internal/impl/k/ah;

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/h/a/a/a;->h()Lkotlin/reflect/jvm/internal/impl/h/a/a/b;

    move-result-object v2

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/h/a/a/a;->w()Lkotlin/reflect/jvm/internal/impl/b/a/h;

    move-result-object v3

    invoke-direct {v0, v1, v2, p1, v3}, Lkotlin/reflect/jvm/internal/impl/h/a/a/a;-><init>(Lkotlin/reflect/jvm/internal/impl/k/ah;Lkotlin/reflect/jvm/internal/impl/h/a/a/b;ZLkotlin/reflect/jvm/internal/impl/b/a/h;)V

    move-object p0, v0

    goto :goto_0
.end method

.method public synthetic c(Lkotlin/reflect/jvm/internal/impl/b/a/h;)Lkotlin/reflect/jvm/internal/impl/k/v;
    .locals 1

    .prologue
    .line 62
    invoke-virtual {p0, p1}, Lkotlin/reflect/jvm/internal/impl/h/a/a/a;->a(Lkotlin/reflect/jvm/internal/impl/b/a/h;)Lkotlin/reflect/jvm/internal/impl/h/a/a/a;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/v;

    return-object v0
.end method

.method public c()Z
    .locals 1

    .prologue
    .line 65
    iget-boolean v0, p0, Lkotlin/reflect/jvm/internal/impl/h/a/a/a;->c:Z

    return v0
.end method

.method public d()Z
    .locals 1

    .prologue
    .line 75
    const/4 v0, 0x0

    return v0
.end method

.method public e()Lkotlin/reflect/jvm/internal/impl/k/r;
    .locals 3

    .prologue
    .line 78
    sget-object v1, Lkotlin/reflect/jvm/internal/impl/k/ap;->c:Lkotlin/reflect/jvm/internal/impl/k/ap;

    invoke-static {p0}, Lkotlin/reflect/jvm/internal/impl/k/c/a;->a(Lkotlin/reflect/jvm/internal/impl/k/r;)Lkotlin/reflect/jvm/internal/impl/a/l;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/a/l;->y()Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v0

    const-string v2, "builtIns.nullableAnyType"

    invoke-static {v0, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/r;

    invoke-direct {p0, v1, v0}, Lkotlin/reflect/jvm/internal/impl/h/a/a/a;->a(Lkotlin/reflect/jvm/internal/impl/k/ap;Lkotlin/reflect/jvm/internal/impl/k/r;)Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v0

    const-string v1, "representative(OUT_VARIA\u2026builtIns.nullableAnyType)"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public f()Lkotlin/reflect/jvm/internal/impl/k/r;
    .locals 3

    .prologue
    .line 81
    sget-object v1, Lkotlin/reflect/jvm/internal/impl/k/ap;->b:Lkotlin/reflect/jvm/internal/impl/k/ap;

    invoke-static {p0}, Lkotlin/reflect/jvm/internal/impl/k/c/a;->a(Lkotlin/reflect/jvm/internal/impl/k/r;)Lkotlin/reflect/jvm/internal/impl/a/l;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/a/l;->v()Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v0

    const-string v2, "builtIns.nothingType"

    invoke-static {v0, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/r;

    invoke-direct {p0, v1, v0}, Lkotlin/reflect/jvm/internal/impl/h/a/a/a;->a(Lkotlin/reflect/jvm/internal/impl/k/ap;Lkotlin/reflect/jvm/internal/impl/k/r;)Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v0

    const-string v1, "representative(IN_VARIANCE, builtIns.nothingType)"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public final g()Lkotlin/reflect/jvm/internal/impl/k/ah;
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/h/a/a/a;->a:Lkotlin/reflect/jvm/internal/impl/k/ah;

    return-object v0
.end method

.method public h()Lkotlin/reflect/jvm/internal/impl/h/a/a/b;
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/h/a/a/a;->b:Lkotlin/reflect/jvm/internal/impl/h/a/a/b;

    return-object v0
.end method

.method public synthetic i()Lkotlin/reflect/jvm/internal/impl/k/ad;
    .locals 1

    .prologue
    .line 62
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/h/a/a/a;->h()Lkotlin/reflect/jvm/internal/impl/h/a/a/b;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/ad;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 88
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Captured("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lkotlin/reflect/jvm/internal/impl/h/a/a/a;->a:Lkotlin/reflect/jvm/internal/impl/k/ah;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/h/a/a/a;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "?"

    :goto_0
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    const-string v0, ""

    goto :goto_0
.end method

.method public w()Lkotlin/reflect/jvm/internal/impl/b/a/h;
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/h/a/a/a;->d:Lkotlin/reflect/jvm/internal/impl/b/a/h;

    return-object v0
.end method
