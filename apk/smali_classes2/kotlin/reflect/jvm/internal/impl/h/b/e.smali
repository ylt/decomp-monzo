.class public final Lkotlin/reflect/jvm/internal/impl/h/b/e;
.super Lkotlin/reflect/jvm/internal/impl/h/b/m;
.source "constantValues.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/reflect/jvm/internal/impl/h/b/m",
        "<",
        "Ljava/lang/Character;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Lkotlin/reflect/jvm/internal/impl/k/v;


# direct methods
.method public constructor <init>(CLkotlin/reflect/jvm/internal/impl/a/l;)V
    .locals 1

    .prologue
    const-string v0, "builtIns"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 93
    invoke-static {p1}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v0

    .line 96
    invoke-direct {p0, v0}, Lkotlin/reflect/jvm/internal/impl/h/b/m;-><init>(Ljava/lang/Object;)V

    .line 98
    invoke-virtual {p2}, Lkotlin/reflect/jvm/internal/impl/a/l;->G()Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/h/b/e;->a:Lkotlin/reflect/jvm/internal/impl/k/v;

    return-void
.end method

.method private final a(C)Ljava/lang/String;
    .locals 2

    .prologue
    .line 105
    packed-switch p1, :pswitch_data_0

    .line 112
    :pswitch_0
    invoke-direct {p0, p1}, Lkotlin/reflect/jvm/internal/impl/h/b/e;->b(C)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p1}, Ljava/lang/Character;->toString(C)Ljava/lang/String;

    move-result-object v0

    const-string v1, "Character.toString(c)"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    :goto_0
    return-object v0

    .line 106
    :pswitch_1
    const-string v0, "\\b"

    goto :goto_0

    .line 107
    :pswitch_2
    const-string v0, "\\t"

    goto :goto_0

    .line 108
    :pswitch_3
    const-string v0, "\\n"

    goto :goto_0

    .line 110
    :pswitch_4
    const-string v0, "\\f"

    goto :goto_0

    .line 111
    :pswitch_5
    const-string v0, "\\r"

    goto :goto_0

    .line 112
    :cond_0
    const-string v0, "?"

    goto :goto_0

    .line 105
    nop

    :pswitch_data_0
    .packed-switch 0x8
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method private final b(C)Z
    .locals 2

    .prologue
    .line 117
    invoke-static {p1}, Ljava/lang/Character;->getType(C)I

    move-result v0

    int-to-byte v0, v0

    .line 124
    if-eqz v0, :cond_0

    const/16 v1, 0xd

    if-eq v0, v1, :cond_0

    const/16 v1, 0xe

    if-eq v0, v1, :cond_0

    const/16 v1, 0xf

    if-eq v0, v1, :cond_0

    const/16 v1, 0x10

    if-eq v0, v1, :cond_0

    const/16 v1, 0x12

    if-eq v0, v1, :cond_0

    const/16 v1, 0x13

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public synthetic a()Lkotlin/reflect/jvm/internal/impl/k/r;
    .locals 1

    .prologue
    .line 93
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/h/b/e;->b()Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/r;

    return-object v0
.end method

.method public b()Lkotlin/reflect/jvm/internal/impl/k/v;
    .locals 1

    .prologue
    .line 98
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/h/b/e;->a:Lkotlin/reflect/jvm/internal/impl/k/v;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 102
    const-string v1, "\\u%04X (\'%s\')"

    const/4 v0, 0x2

    new-array v2, v0, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/h/b/e;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Character;

    invoke-virtual {v0}, Ljava/lang/Character;->charValue()C

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v2, v3

    const/4 v3, 0x1

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/h/b/e;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Character;

    invoke-virtual {v0}, Ljava/lang/Character;->charValue()C

    move-result v0

    invoke-direct {p0, v0}, Lkotlin/reflect/jvm/internal/impl/h/b/e;->a(C)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v3

    array-length v0, v2

    invoke-static {v2, v0}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/Object;

    invoke-static {v1, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "java.lang.String.format(this, *args)"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method
