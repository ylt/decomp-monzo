.class public final enum Lkotlin/reflect/jvm/internal/impl/h/d$b;
.super Ljava/lang/Enum;
.source "ExternalOverridabilityCondition.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lkotlin/reflect/jvm/internal/impl/h/d;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lkotlin/reflect/jvm/internal/impl/h/d$b;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lkotlin/reflect/jvm/internal/impl/h/d$b;

.field public static final enum b:Lkotlin/reflect/jvm/internal/impl/h/d$b;

.field public static final enum c:Lkotlin/reflect/jvm/internal/impl/h/d$b;

.field public static final enum d:Lkotlin/reflect/jvm/internal/impl/h/d$b;

.field private static final synthetic e:[Lkotlin/reflect/jvm/internal/impl/h/d$b;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 26
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/h/d$b;

    const-string v1, "OVERRIDABLE"

    invoke-direct {v0, v1, v2}, Lkotlin/reflect/jvm/internal/impl/h/d$b;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/h/d$b;->a:Lkotlin/reflect/jvm/internal/impl/h/d$b;

    new-instance v0, Lkotlin/reflect/jvm/internal/impl/h/d$b;

    const-string v1, "CONFLICT"

    invoke-direct {v0, v1, v3}, Lkotlin/reflect/jvm/internal/impl/h/d$b;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/h/d$b;->b:Lkotlin/reflect/jvm/internal/impl/h/d$b;

    new-instance v0, Lkotlin/reflect/jvm/internal/impl/h/d$b;

    const-string v1, "INCOMPATIBLE"

    invoke-direct {v0, v1, v4}, Lkotlin/reflect/jvm/internal/impl/h/d$b;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/h/d$b;->c:Lkotlin/reflect/jvm/internal/impl/h/d$b;

    new-instance v0, Lkotlin/reflect/jvm/internal/impl/h/d$b;

    const-string v1, "UNKNOWN"

    invoke-direct {v0, v1, v5}, Lkotlin/reflect/jvm/internal/impl/h/d$b;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/h/d$b;->d:Lkotlin/reflect/jvm/internal/impl/h/d$b;

    .line 25
    const/4 v0, 0x4

    new-array v0, v0, [Lkotlin/reflect/jvm/internal/impl/h/d$b;

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/h/d$b;->a:Lkotlin/reflect/jvm/internal/impl/h/d$b;

    aput-object v1, v0, v2

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/h/d$b;->b:Lkotlin/reflect/jvm/internal/impl/h/d$b;

    aput-object v1, v0, v3

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/h/d$b;->c:Lkotlin/reflect/jvm/internal/impl/h/d$b;

    aput-object v1, v0, v4

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/h/d$b;->d:Lkotlin/reflect/jvm/internal/impl/h/d$b;

    aput-object v1, v0, v5

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/h/d$b;->e:[Lkotlin/reflect/jvm/internal/impl/h/d$b;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 25
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lkotlin/reflect/jvm/internal/impl/h/d$b;
    .locals 1

    .prologue
    .line 25
    const-class v0, Lkotlin/reflect/jvm/internal/impl/h/d$b;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/h/d$b;

    return-object v0
.end method

.method public static values()[Lkotlin/reflect/jvm/internal/impl/h/d$b;
    .locals 1

    .prologue
    .line 25
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/h/d$b;->e:[Lkotlin/reflect/jvm/internal/impl/h/d$b;

    invoke-virtual {v0}, [Lkotlin/reflect/jvm/internal/impl/h/d$b;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lkotlin/reflect/jvm/internal/impl/h/d$b;

    return-object v0
.end method
