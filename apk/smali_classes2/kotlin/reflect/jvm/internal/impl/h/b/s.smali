.class public final Lkotlin/reflect/jvm/internal/impl/h/b/s;
.super Lkotlin/reflect/jvm/internal/impl/h/b/f;
.source "constantValues.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/reflect/jvm/internal/impl/h/b/f",
        "<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Lkotlin/reflect/jvm/internal/impl/k/v;


# direct methods
.method public constructor <init>(Ljava/lang/String;Lkotlin/reflect/jvm/internal/impl/a/l;)V
    .locals 1

    .prologue
    const-string v0, "value"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "builtIns"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 260
    invoke-direct {p0, p1}, Lkotlin/reflect/jvm/internal/impl/h/b/f;-><init>(Ljava/lang/Object;)V

    .line 261
    invoke-virtual {p2}, Lkotlin/reflect/jvm/internal/impl/a/l;->J()Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/h/b/s;->a:Lkotlin/reflect/jvm/internal/impl/k/v;

    return-void
.end method


# virtual methods
.method public synthetic a()Lkotlin/reflect/jvm/internal/impl/k/r;
    .locals 1

    .prologue
    .line 257
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/h/b/s;->b()Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/r;

    return-object v0
.end method

.method public b()Lkotlin/reflect/jvm/internal/impl/k/v;
    .locals 1

    .prologue
    .line 261
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/h/b/s;->a:Lkotlin/reflect/jvm/internal/impl/k/v;

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 268
    move-object v0, p0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/h/b/s;

    if-ne v0, p1, :cond_0

    const/4 v0, 0x1

    .line 271
    :goto_0
    return v0

    .line 269
    :cond_0
    if-eqz p1, :cond_1

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_2

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 271
    :cond_2
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/h/b/s;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    if-nez p1, :cond_3

    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type org.jetbrains.kotlin.resolve.constants.StringValue"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_3
    check-cast p1, Lkotlin/reflect/jvm/internal/impl/h/b/s;

    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/h/b/s;->c()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 274
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/h/b/s;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 265
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "\""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/h/b/s;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
