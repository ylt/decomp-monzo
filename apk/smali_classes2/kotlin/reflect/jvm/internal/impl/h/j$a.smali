.class public Lkotlin/reflect/jvm/internal/impl/h/j$a;
.super Ljava/lang/Object;
.source "OverridingUtil.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lkotlin/reflect/jvm/internal/impl/h/j;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lkotlin/reflect/jvm/internal/impl/h/j$a$a;
    }
.end annotation


# static fields
.field private static final a:Lkotlin/reflect/jvm/internal/impl/h/j$a;


# instance fields
.field private final b:Lkotlin/reflect/jvm/internal/impl/h/j$a$a;

.field private final c:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 876
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/h/j$a;

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/h/j$a$a;->a:Lkotlin/reflect/jvm/internal/impl/h/j$a$a;

    const-string v2, "SUCCESS"

    invoke-direct {v0, v1, v2}, Lkotlin/reflect/jvm/internal/impl/h/j$a;-><init>(Lkotlin/reflect/jvm/internal/impl/h/j$a$a;Ljava/lang/String;)V

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/h/j$a;->a:Lkotlin/reflect/jvm/internal/impl/h/j$a;

    return-void
.end method

.method public constructor <init>(Lkotlin/reflect/jvm/internal/impl/h/j$a$a;Ljava/lang/String;)V
    .locals 7

    .prologue
    const/4 v2, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "success"

    aput-object v3, v2, v4

    const-string v3, "kotlin/reflect/jvm/internal/impl/resolve/OverridingUtil$OverrideCompatibilityInfo"

    aput-object v3, v2, v5

    const-string v3, "<init>"

    aput-object v3, v2, v6

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    if-nez p2, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "debugMessage"

    aput-object v3, v2, v4

    const-string v3, "kotlin/reflect/jvm/internal/impl/resolve/OverridingUtil$OverrideCompatibilityInfo"

    aput-object v3, v2, v5

    const-string v3, "<init>"

    aput-object v3, v2, v6

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 896
    :cond_1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 897
    iput-object p1, p0, Lkotlin/reflect/jvm/internal/impl/h/j$a;->b:Lkotlin/reflect/jvm/internal/impl/h/j$a$a;

    .line 898
    iput-object p2, p0, Lkotlin/reflect/jvm/internal/impl/h/j$a;->c:Ljava/lang/String;

    .line 899
    return-void
.end method

.method public static a()Lkotlin/reflect/jvm/internal/impl/h/j$a;
    .locals 5

    .prologue
    .line 880
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/h/j$a;->a:Lkotlin/reflect/jvm/internal/impl/h/j$a;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "@NotNull method %s.%s must not return null"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "kotlin/reflect/jvm/internal/impl/resolve/OverridingUtil$OverrideCompatibilityInfo"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "success"

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    return-object v0
.end method

.method public static a(Ljava/lang/String;)Lkotlin/reflect/jvm/internal/impl/h/j$a;
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    if-nez p0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "debugMessage"

    aput-object v3, v2, v4

    const-string v3, "kotlin/reflect/jvm/internal/impl/resolve/OverridingUtil$OverrideCompatibilityInfo"

    aput-object v3, v2, v5

    const-string v3, "incompatible"

    aput-object v3, v2, v6

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 885
    :cond_0
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/h/j$a;

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/h/j$a$a;->b:Lkotlin/reflect/jvm/internal/impl/h/j$a$a;

    invoke-direct {v0, v1, p0}, Lkotlin/reflect/jvm/internal/impl/h/j$a;-><init>(Lkotlin/reflect/jvm/internal/impl/h/j$a$a;Ljava/lang/String;)V

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "@NotNull method %s.%s must not return null"

    new-array v2, v6, [Ljava/lang/Object;

    const-string v3, "kotlin/reflect/jvm/internal/impl/resolve/OverridingUtil$OverrideCompatibilityInfo"

    aput-object v3, v2, v4

    const-string v3, "incompatible"

    aput-object v3, v2, v5

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    return-object v0
.end method

.method public static b(Ljava/lang/String;)Lkotlin/reflect/jvm/internal/impl/h/j$a;
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    if-nez p0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "debugMessage"

    aput-object v3, v2, v4

    const-string v3, "kotlin/reflect/jvm/internal/impl/resolve/OverridingUtil$OverrideCompatibilityInfo"

    aput-object v3, v2, v5

    const-string v3, "conflict"

    aput-object v3, v2, v6

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 890
    :cond_0
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/h/j$a;

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/h/j$a$a;->c:Lkotlin/reflect/jvm/internal/impl/h/j$a$a;

    invoke-direct {v0, v1, p0}, Lkotlin/reflect/jvm/internal/impl/h/j$a;-><init>(Lkotlin/reflect/jvm/internal/impl/h/j$a$a;Ljava/lang/String;)V

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "@NotNull method %s.%s must not return null"

    new-array v2, v6, [Ljava/lang/Object;

    const-string v3, "kotlin/reflect/jvm/internal/impl/resolve/OverridingUtil$OverrideCompatibilityInfo"

    aput-object v3, v2, v4

    const-string v3, "conflict"

    aput-object v3, v2, v5

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    return-object v0
.end method


# virtual methods
.method public b()Lkotlin/reflect/jvm/internal/impl/h/j$a$a;
    .locals 5

    .prologue
    .line 903
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/h/j$a;->b:Lkotlin/reflect/jvm/internal/impl/h/j$a$a;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "@NotNull method %s.%s must not return null"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "kotlin/reflect/jvm/internal/impl/resolve/OverridingUtil$OverrideCompatibilityInfo"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "getResult"

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    return-object v0
.end method
