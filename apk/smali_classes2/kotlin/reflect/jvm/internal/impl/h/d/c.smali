.class public final enum Lkotlin/reflect/jvm/internal/impl/h/d/c;
.super Ljava/lang/Enum;
.source "JvmPrimitiveType.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lkotlin/reflect/jvm/internal/impl/h/d/c;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lkotlin/reflect/jvm/internal/impl/h/d/c;

.field public static final enum b:Lkotlin/reflect/jvm/internal/impl/h/d/c;

.field public static final enum c:Lkotlin/reflect/jvm/internal/impl/h/d/c;

.field public static final enum d:Lkotlin/reflect/jvm/internal/impl/h/d/c;

.field public static final enum e:Lkotlin/reflect/jvm/internal/impl/h/d/c;

.field public static final enum f:Lkotlin/reflect/jvm/internal/impl/h/d/c;

.field public static final enum g:Lkotlin/reflect/jvm/internal/impl/h/d/c;

.field public static final enum h:Lkotlin/reflect/jvm/internal/impl/h/d/c;

.field private static final i:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/e/b;",
            ">;"
        }
    .end annotation
.end field

.field private static final j:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lkotlin/reflect/jvm/internal/impl/h/d/c;",
            ">;"
        }
    .end annotation
.end field

.field private static final k:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/a/m;",
            "Lkotlin/reflect/jvm/internal/impl/h/d/c;",
            ">;"
        }
    .end annotation
.end field

.field private static final synthetic p:[Lkotlin/reflect/jvm/internal/impl/h/d/c;


# instance fields
.field private final l:Lkotlin/reflect/jvm/internal/impl/a/m;

.field private final m:Ljava/lang/String;

.field private final n:Ljava/lang/String;

.field private final o:Lkotlin/reflect/jvm/internal/impl/e/b;


# direct methods
.method static constructor <clinit>()V
    .locals 14

    .prologue
    const/4 v13, 0x4

    const/4 v12, 0x3

    const/4 v11, 0x2

    const/4 v10, 0x1

    const/4 v2, 0x0

    .line 26
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/h/d/c;

    const-string v1, "BOOLEAN"

    sget-object v3, Lkotlin/reflect/jvm/internal/impl/a/m;->a:Lkotlin/reflect/jvm/internal/impl/a/m;

    const-string v4, "boolean"

    const-string v5, "Z"

    const-string v6, "java.lang.Boolean"

    invoke-direct/range {v0 .. v6}, Lkotlin/reflect/jvm/internal/impl/h/d/c;-><init>(Ljava/lang/String;ILkotlin/reflect/jvm/internal/impl/a/m;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/h/d/c;->a:Lkotlin/reflect/jvm/internal/impl/h/d/c;

    .line 27
    new-instance v3, Lkotlin/reflect/jvm/internal/impl/h/d/c;

    const-string v4, "CHAR"

    sget-object v6, Lkotlin/reflect/jvm/internal/impl/a/m;->b:Lkotlin/reflect/jvm/internal/impl/a/m;

    const-string v7, "char"

    const-string v8, "C"

    const-string v9, "java.lang.Character"

    move v5, v10

    invoke-direct/range {v3 .. v9}, Lkotlin/reflect/jvm/internal/impl/h/d/c;-><init>(Ljava/lang/String;ILkotlin/reflect/jvm/internal/impl/a/m;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v3, Lkotlin/reflect/jvm/internal/impl/h/d/c;->b:Lkotlin/reflect/jvm/internal/impl/h/d/c;

    .line 28
    new-instance v3, Lkotlin/reflect/jvm/internal/impl/h/d/c;

    const-string v4, "BYTE"

    sget-object v6, Lkotlin/reflect/jvm/internal/impl/a/m;->c:Lkotlin/reflect/jvm/internal/impl/a/m;

    const-string v7, "byte"

    const-string v8, "B"

    const-string v9, "java.lang.Byte"

    move v5, v11

    invoke-direct/range {v3 .. v9}, Lkotlin/reflect/jvm/internal/impl/h/d/c;-><init>(Ljava/lang/String;ILkotlin/reflect/jvm/internal/impl/a/m;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v3, Lkotlin/reflect/jvm/internal/impl/h/d/c;->c:Lkotlin/reflect/jvm/internal/impl/h/d/c;

    .line 29
    new-instance v3, Lkotlin/reflect/jvm/internal/impl/h/d/c;

    const-string v4, "SHORT"

    sget-object v6, Lkotlin/reflect/jvm/internal/impl/a/m;->d:Lkotlin/reflect/jvm/internal/impl/a/m;

    const-string v7, "short"

    const-string v8, "S"

    const-string v9, "java.lang.Short"

    move v5, v12

    invoke-direct/range {v3 .. v9}, Lkotlin/reflect/jvm/internal/impl/h/d/c;-><init>(Ljava/lang/String;ILkotlin/reflect/jvm/internal/impl/a/m;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v3, Lkotlin/reflect/jvm/internal/impl/h/d/c;->d:Lkotlin/reflect/jvm/internal/impl/h/d/c;

    .line 30
    new-instance v3, Lkotlin/reflect/jvm/internal/impl/h/d/c;

    const-string v4, "INT"

    sget-object v6, Lkotlin/reflect/jvm/internal/impl/a/m;->e:Lkotlin/reflect/jvm/internal/impl/a/m;

    const-string v7, "int"

    const-string v8, "I"

    const-string v9, "java.lang.Integer"

    move v5, v13

    invoke-direct/range {v3 .. v9}, Lkotlin/reflect/jvm/internal/impl/h/d/c;-><init>(Ljava/lang/String;ILkotlin/reflect/jvm/internal/impl/a/m;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v3, Lkotlin/reflect/jvm/internal/impl/h/d/c;->e:Lkotlin/reflect/jvm/internal/impl/h/d/c;

    .line 31
    new-instance v3, Lkotlin/reflect/jvm/internal/impl/h/d/c;

    const-string v4, "FLOAT"

    const/4 v5, 0x5

    sget-object v6, Lkotlin/reflect/jvm/internal/impl/a/m;->f:Lkotlin/reflect/jvm/internal/impl/a/m;

    const-string v7, "float"

    const-string v8, "F"

    const-string v9, "java.lang.Float"

    invoke-direct/range {v3 .. v9}, Lkotlin/reflect/jvm/internal/impl/h/d/c;-><init>(Ljava/lang/String;ILkotlin/reflect/jvm/internal/impl/a/m;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v3, Lkotlin/reflect/jvm/internal/impl/h/d/c;->f:Lkotlin/reflect/jvm/internal/impl/h/d/c;

    .line 32
    new-instance v3, Lkotlin/reflect/jvm/internal/impl/h/d/c;

    const-string v4, "LONG"

    const/4 v5, 0x6

    sget-object v6, Lkotlin/reflect/jvm/internal/impl/a/m;->g:Lkotlin/reflect/jvm/internal/impl/a/m;

    const-string v7, "long"

    const-string v8, "J"

    const-string v9, "java.lang.Long"

    invoke-direct/range {v3 .. v9}, Lkotlin/reflect/jvm/internal/impl/h/d/c;-><init>(Ljava/lang/String;ILkotlin/reflect/jvm/internal/impl/a/m;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v3, Lkotlin/reflect/jvm/internal/impl/h/d/c;->g:Lkotlin/reflect/jvm/internal/impl/h/d/c;

    .line 33
    new-instance v3, Lkotlin/reflect/jvm/internal/impl/h/d/c;

    const-string v4, "DOUBLE"

    const/4 v5, 0x7

    sget-object v6, Lkotlin/reflect/jvm/internal/impl/a/m;->h:Lkotlin/reflect/jvm/internal/impl/a/m;

    const-string v7, "double"

    const-string v8, "D"

    const-string v9, "java.lang.Double"

    invoke-direct/range {v3 .. v9}, Lkotlin/reflect/jvm/internal/impl/h/d/c;-><init>(Ljava/lang/String;ILkotlin/reflect/jvm/internal/impl/a/m;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v3, Lkotlin/reflect/jvm/internal/impl/h/d/c;->h:Lkotlin/reflect/jvm/internal/impl/h/d/c;

    .line 25
    const/16 v0, 0x8

    new-array v0, v0, [Lkotlin/reflect/jvm/internal/impl/h/d/c;

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/h/d/c;->a:Lkotlin/reflect/jvm/internal/impl/h/d/c;

    aput-object v1, v0, v2

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/h/d/c;->b:Lkotlin/reflect/jvm/internal/impl/h/d/c;

    aput-object v1, v0, v10

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/h/d/c;->c:Lkotlin/reflect/jvm/internal/impl/h/d/c;

    aput-object v1, v0, v11

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/h/d/c;->d:Lkotlin/reflect/jvm/internal/impl/h/d/c;

    aput-object v1, v0, v12

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/h/d/c;->e:Lkotlin/reflect/jvm/internal/impl/h/d/c;

    aput-object v1, v0, v13

    const/4 v1, 0x5

    sget-object v3, Lkotlin/reflect/jvm/internal/impl/h/d/c;->f:Lkotlin/reflect/jvm/internal/impl/h/d/c;

    aput-object v3, v0, v1

    const/4 v1, 0x6

    sget-object v3, Lkotlin/reflect/jvm/internal/impl/h/d/c;->g:Lkotlin/reflect/jvm/internal/impl/h/d/c;

    aput-object v3, v0, v1

    const/4 v1, 0x7

    sget-object v3, Lkotlin/reflect/jvm/internal/impl/h/d/c;->h:Lkotlin/reflect/jvm/internal/impl/h/d/c;

    aput-object v3, v0, v1

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/h/d/c;->p:[Lkotlin/reflect/jvm/internal/impl/h/d/c;

    .line 41
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/h/d/c;->i:Ljava/util/Set;

    .line 42
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/h/d/c;->j:Ljava/util/Map;

    .line 43
    new-instance v0, Ljava/util/EnumMap;

    const-class v1, Lkotlin/reflect/jvm/internal/impl/a/m;

    invoke-direct {v0, v1}, Ljava/util/EnumMap;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/h/d/c;->k:Ljava/util/Map;

    .line 45
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/h/d/c;->values()[Lkotlin/reflect/jvm/internal/impl/h/d/c;

    move-result-object v0

    array-length v1, v0

    :goto_0
    if-ge v2, v1, :cond_0

    aget-object v3, v0, v2

    .line 46
    sget-object v4, Lkotlin/reflect/jvm/internal/impl/h/d/c;->i:Ljava/util/Set;

    invoke-virtual {v3}, Lkotlin/reflect/jvm/internal/impl/h/d/c;->d()Lkotlin/reflect/jvm/internal/impl/e/b;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 47
    sget-object v4, Lkotlin/reflect/jvm/internal/impl/h/d/c;->j:Ljava/util/Map;

    invoke-virtual {v3}, Lkotlin/reflect/jvm/internal/impl/h/d/c;->b()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, v5, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 48
    sget-object v4, Lkotlin/reflect/jvm/internal/impl/h/d/c;->k:Ljava/util/Map;

    invoke-virtual {v3}, Lkotlin/reflect/jvm/internal/impl/h/d/c;->a()Lkotlin/reflect/jvm/internal/impl/a/m;

    move-result-object v5

    invoke-interface {v4, v5, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 45
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 50
    :cond_0
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILkotlin/reflect/jvm/internal/impl/a/m;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/reflect/jvm/internal/impl/a/m;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    if-nez p3, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "primitiveType"

    aput-object v3, v2, v4

    const-string v3, "kotlin/reflect/jvm/internal/impl/resolve/jvm/JvmPrimitiveType"

    aput-object v3, v2, v5

    const-string v3, "<init>"

    aput-object v3, v2, v6

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    if-nez p4, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "name"

    aput-object v3, v2, v4

    const-string v3, "kotlin/reflect/jvm/internal/impl/resolve/jvm/JvmPrimitiveType"

    aput-object v3, v2, v5

    const-string v3, "<init>"

    aput-object v3, v2, v6

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    if-nez p5, :cond_2

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "desc"

    aput-object v3, v2, v4

    const-string v3, "kotlin/reflect/jvm/internal/impl/resolve/jvm/JvmPrimitiveType"

    aput-object v3, v2, v5

    const-string v3, "<init>"

    aput-object v3, v2, v6

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    if-nez p6, :cond_3

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "wrapperClassName"

    aput-object v3, v2, v4

    const-string v3, "kotlin/reflect/jvm/internal/impl/resolve/jvm/JvmPrimitiveType"

    aput-object v3, v2, v5

    const-string v3, "<init>"

    aput-object v3, v2, v6

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 75
    :cond_3
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 76
    iput-object p3, p0, Lkotlin/reflect/jvm/internal/impl/h/d/c;->l:Lkotlin/reflect/jvm/internal/impl/a/m;

    .line 77
    iput-object p4, p0, Lkotlin/reflect/jvm/internal/impl/h/d/c;->m:Ljava/lang/String;

    .line 78
    iput-object p5, p0, Lkotlin/reflect/jvm/internal/impl/h/d/c;->n:Ljava/lang/String;

    .line 79
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/e/b;

    invoke-direct {v0, p6}, Lkotlin/reflect/jvm/internal/impl/e/b;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/h/d/c;->o:Lkotlin/reflect/jvm/internal/impl/e/b;

    .line 80
    return-void
.end method

.method public static a(Ljava/lang/String;)Lkotlin/reflect/jvm/internal/impl/h/d/c;
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    if-nez p0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "name"

    aput-object v3, v2, v4

    const-string v3, "kotlin/reflect/jvm/internal/impl/resolve/jvm/JvmPrimitiveType"

    aput-object v3, v2, v5

    const-string v3, "get"

    aput-object v3, v2, v6

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 58
    :cond_0
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/h/d/c;->j:Ljava/util/Map;

    invoke-interface {v0, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/h/d/c;

    .line 59
    if-nez v0, :cond_1

    .line 60
    new-instance v0, Ljava/lang/AssertionError;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Non-primitive type name passed: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 62
    :cond_1
    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "@NotNull method %s.%s must not return null"

    new-array v2, v6, [Ljava/lang/Object;

    const-string v3, "kotlin/reflect/jvm/internal/impl/resolve/jvm/JvmPrimitiveType"

    aput-object v3, v2, v4

    const-string v3, "get"

    aput-object v3, v2, v5

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    return-object v0
.end method

.method public static a(Lkotlin/reflect/jvm/internal/impl/a/m;)Lkotlin/reflect/jvm/internal/impl/h/d/c;
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    if-nez p0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "type"

    aput-object v3, v2, v4

    const-string v3, "kotlin/reflect/jvm/internal/impl/resolve/jvm/JvmPrimitiveType"

    aput-object v3, v2, v5

    const-string v3, "get"

    aput-object v3, v2, v6

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 67
    :cond_0
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/h/d/c;->k:Ljava/util/Map;

    invoke-interface {v0, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/h/d/c;

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "@NotNull method %s.%s must not return null"

    new-array v2, v6, [Ljava/lang/Object;

    const-string v3, "kotlin/reflect/jvm/internal/impl/resolve/jvm/JvmPrimitiveType"

    aput-object v3, v2, v4

    const-string v3, "get"

    aput-object v3, v2, v5

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)Lkotlin/reflect/jvm/internal/impl/h/d/c;
    .locals 1

    .prologue
    .line 25
    const-class v0, Lkotlin/reflect/jvm/internal/impl/h/d/c;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/h/d/c;

    return-object v0
.end method

.method public static values()[Lkotlin/reflect/jvm/internal/impl/h/d/c;
    .locals 1

    .prologue
    .line 25
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/h/d/c;->p:[Lkotlin/reflect/jvm/internal/impl/h/d/c;

    invoke-virtual {v0}, [Lkotlin/reflect/jvm/internal/impl/h/d/c;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lkotlin/reflect/jvm/internal/impl/h/d/c;

    return-object v0
.end method


# virtual methods
.method public a()Lkotlin/reflect/jvm/internal/impl/a/m;
    .locals 5

    .prologue
    .line 84
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/h/d/c;->l:Lkotlin/reflect/jvm/internal/impl/a/m;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "@NotNull method %s.%s must not return null"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "kotlin/reflect/jvm/internal/impl/resolve/jvm/JvmPrimitiveType"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "getPrimitiveType"

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    return-object v0
.end method

.method public b()Ljava/lang/String;
    .locals 5

    .prologue
    .line 89
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/h/d/c;->m:Ljava/lang/String;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "@NotNull method %s.%s must not return null"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "kotlin/reflect/jvm/internal/impl/resolve/jvm/JvmPrimitiveType"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "getJavaKeywordName"

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    return-object v0
.end method

.method public c()Ljava/lang/String;
    .locals 5

    .prologue
    .line 94
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/h/d/c;->n:Ljava/lang/String;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "@NotNull method %s.%s must not return null"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "kotlin/reflect/jvm/internal/impl/resolve/jvm/JvmPrimitiveType"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "getDesc"

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    return-object v0
.end method

.method public d()Lkotlin/reflect/jvm/internal/impl/e/b;
    .locals 5

    .prologue
    .line 99
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/h/d/c;->o:Lkotlin/reflect/jvm/internal/impl/e/b;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "@NotNull method %s.%s must not return null"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "kotlin/reflect/jvm/internal/impl/resolve/jvm/JvmPrimitiveType"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "getWrapperFqName"

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    return-object v0
.end method
