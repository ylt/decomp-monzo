.class public abstract Lkotlin/reflect/jvm/internal/impl/h/h;
.super Lkotlin/reflect/jvm/internal/impl/h/i;
.source "OverridingStrategy.kt"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/h/i;-><init>()V

    return-void
.end method


# virtual methods
.method protected abstract a(Lkotlin/reflect/jvm/internal/impl/b/b;Lkotlin/reflect/jvm/internal/impl/b/b;)V
.end method

.method public b(Lkotlin/reflect/jvm/internal/impl/b/b;Lkotlin/reflect/jvm/internal/impl/b/b;)V
    .locals 1

    .prologue
    const-string v0, "fromSuper"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "fromCurrent"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 35
    invoke-virtual {p0, p1, p2}, Lkotlin/reflect/jvm/internal/impl/h/h;->a(Lkotlin/reflect/jvm/internal/impl/b/b;Lkotlin/reflect/jvm/internal/impl/b/b;)V

    .line 36
    return-void
.end method

.method public c(Lkotlin/reflect/jvm/internal/impl/b/b;Lkotlin/reflect/jvm/internal/impl/b/b;)V
    .locals 1

    .prologue
    const-string v0, "first"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "second"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 39
    invoke-virtual {p0, p1, p2}, Lkotlin/reflect/jvm/internal/impl/h/h;->a(Lkotlin/reflect/jvm/internal/impl/b/b;Lkotlin/reflect/jvm/internal/impl/b/b;)V

    .line 40
    return-void
.end method
