.class public final Lkotlin/reflect/jvm/internal/impl/h/b/p;
.super Lkotlin/reflect/jvm/internal/impl/h/b/m;
.source "constantValues.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/reflect/jvm/internal/impl/h/b/m",
        "<",
        "Ljava/lang/Long;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Lkotlin/reflect/jvm/internal/impl/k/v;


# direct methods
.method public constructor <init>(JLkotlin/reflect/jvm/internal/impl/a/l;)V
    .locals 1

    .prologue
    const-string v0, "builtIns"

    invoke-static {p3, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 222
    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    .line 225
    invoke-direct {p0, v0}, Lkotlin/reflect/jvm/internal/impl/h/b/m;-><init>(Ljava/lang/Object;)V

    .line 227
    invoke-virtual {p3}, Lkotlin/reflect/jvm/internal/impl/a/l;->D()Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/h/b/p;->a:Lkotlin/reflect/jvm/internal/impl/k/v;

    return-void
.end method


# virtual methods
.method public synthetic a()Lkotlin/reflect/jvm/internal/impl/k/r;
    .locals 1

    .prologue
    .line 222
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/h/b/p;->b()Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/r;

    return-object v0
.end method

.method public b()Lkotlin/reflect/jvm/internal/impl/k/v;
    .locals 1

    .prologue
    .line 227
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/h/b/p;->a:Lkotlin/reflect/jvm/internal/impl/k/v;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 231
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/h/b/p;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Number;

    invoke-virtual {v0}, Ljava/lang/Number;->longValue()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".toLong()"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
