.class public final Lkotlin/reflect/jvm/internal/impl/h/e/j$a;
.super Ljava/lang/Object;
.source "ResolutionScope.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lkotlin/reflect/jvm/internal/impl/h/e/j;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation


# direct methods
.method public static synthetic a(Lkotlin/reflect/jvm/internal/impl/h/e/j;Lkotlin/reflect/jvm/internal/impl/h/e/d;Lkotlin/d/a/b;ILjava/lang/Object;)Ljava/util/Collection;
    .locals 2

    .prologue
    if-eqz p4, :cond_0

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Super calls with default arguments not supported in this target, function: getContributedDescriptors"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    and-int/lit8 v0, p3, 0x1

    if-eqz v0, :cond_1

    .line 36
    sget-object p1, Lkotlin/reflect/jvm/internal/impl/h/e/d;->a:Lkotlin/reflect/jvm/internal/impl/h/e/d;

    :cond_1
    and-int/lit8 v0, p3, 0x2

    if-eqz v0, :cond_2

    .line 37
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/h/e/h;->c:Lkotlin/reflect/jvm/internal/impl/h/e/h$a;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/h/e/h$a;->a()Lkotlin/d/a/b;

    move-result-object p2

    :cond_2
    invoke-interface {p0, p1, p2}, Lkotlin/reflect/jvm/internal/impl/h/e/j;->a(Lkotlin/reflect/jvm/internal/impl/h/e/d;Lkotlin/d/a/b;)Ljava/util/Collection;

    move-result-object v0

    return-object v0
.end method
