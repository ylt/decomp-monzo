.class public final Lkotlin/reflect/jvm/internal/impl/h/b/g;
.super Ljava/lang/Object;
.source "ConstantValueFactory.kt"


# instance fields
.field private final a:Lkotlin/reflect/jvm/internal/impl/a/l;


# direct methods
.method public constructor <init>(Lkotlin/reflect/jvm/internal/impl/a/l;)V
    .locals 1

    .prologue
    const-string v0, "builtins"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lkotlin/reflect/jvm/internal/impl/h/b/g;->a:Lkotlin/reflect/jvm/internal/impl/a/l;

    return-void
.end method


# virtual methods
.method public final a(Lkotlin/reflect/jvm/internal/impl/b/a/c;)Lkotlin/reflect/jvm/internal/impl/h/b/a;
    .locals 1

    .prologue
    const-string v0, "value"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 57
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/h/b/a;

    invoke-direct {v0, p1}, Lkotlin/reflect/jvm/internal/impl/h/b/a;-><init>(Lkotlin/reflect/jvm/internal/impl/b/a/c;)V

    return-object v0
.end method

.method public final a(Ljava/util/List;Lkotlin/reflect/jvm/internal/impl/k/r;)Lkotlin/reflect/jvm/internal/impl/h/b/b;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<+",
            "Lkotlin/reflect/jvm/internal/impl/h/b/f",
            "<*>;>;",
            "Lkotlin/reflect/jvm/internal/impl/k/r;",
            ")",
            "Lkotlin/reflect/jvm/internal/impl/h/b/b;"
        }
    .end annotation

    .prologue
    const-string v0, "value"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "type"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 55
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/h/b/b;

    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/h/b/g;->a:Lkotlin/reflect/jvm/internal/impl/a/l;

    invoke-direct {v0, p1, p2, v1}, Lkotlin/reflect/jvm/internal/impl/h/b/b;-><init>(Ljava/util/List;Lkotlin/reflect/jvm/internal/impl/k/r;Lkotlin/reflect/jvm/internal/impl/a/l;)V

    return-object v0
.end method

.method public final a(Z)Lkotlin/reflect/jvm/internal/impl/h/b/c;
    .locals 2

    .prologue
    .line 42
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/h/b/c;

    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/h/b/g;->a:Lkotlin/reflect/jvm/internal/impl/a/l;

    invoke-direct {v0, p1, v1}, Lkotlin/reflect/jvm/internal/impl/h/b/c;-><init>(ZLkotlin/reflect/jvm/internal/impl/a/l;)V

    return-object v0
.end method

.method public final a(B)Lkotlin/reflect/jvm/internal/impl/h/b/d;
    .locals 2

    .prologue
    .line 36
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/h/b/d;

    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/h/b/g;->a:Lkotlin/reflect/jvm/internal/impl/a/l;

    invoke-direct {v0, p1, v1}, Lkotlin/reflect/jvm/internal/impl/h/b/d;-><init>(BLkotlin/reflect/jvm/internal/impl/a/l;)V

    return-object v0
.end method

.method public final a(C)Lkotlin/reflect/jvm/internal/impl/h/b/e;
    .locals 2

    .prologue
    .line 44
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/h/b/e;

    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/h/b/g;->a:Lkotlin/reflect/jvm/internal/impl/a/l;

    invoke-direct {v0, p1, v1}, Lkotlin/reflect/jvm/internal/impl/h/b/e;-><init>(CLkotlin/reflect/jvm/internal/impl/a/l;)V

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Lkotlin/reflect/jvm/internal/impl/h/b/f;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            ")",
            "Lkotlin/reflect/jvm/internal/impl/h/b/f",
            "<*>;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 65
    .line 66
    instance-of v1, p1, Ljava/lang/Byte;

    if-eqz v1, :cond_1

    check-cast p1, Ljava/lang/Number;

    invoke-virtual {p1}, Ljava/lang/Number;->byteValue()B

    move-result v0

    invoke-virtual {p0, v0}, Lkotlin/reflect/jvm/internal/impl/h/b/g;->a(B)Lkotlin/reflect/jvm/internal/impl/h/b/d;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/h/b/f;

    .line 65
    :cond_0
    :goto_0
    return-object v0

    .line 67
    :cond_1
    instance-of v1, p1, Ljava/lang/Short;

    if-eqz v1, :cond_2

    check-cast p1, Ljava/lang/Number;

    invoke-virtual {p1}, Ljava/lang/Number;->shortValue()S

    move-result v0

    invoke-virtual {p0, v0}, Lkotlin/reflect/jvm/internal/impl/h/b/g;->a(S)Lkotlin/reflect/jvm/internal/impl/h/b/r;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/h/b/f;

    goto :goto_0

    .line 68
    :cond_2
    instance-of v1, p1, Ljava/lang/Integer;

    if-eqz v1, :cond_3

    check-cast p1, Ljava/lang/Number;

    invoke-virtual {p1}, Ljava/lang/Number;->intValue()I

    move-result v0

    invoke-virtual {p0, v0}, Lkotlin/reflect/jvm/internal/impl/h/b/g;->a(I)Lkotlin/reflect/jvm/internal/impl/h/b/l;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/h/b/f;

    goto :goto_0

    .line 69
    :cond_3
    instance-of v1, p1, Ljava/lang/Long;

    if-eqz v1, :cond_4

    check-cast p1, Ljava/lang/Number;

    invoke-virtual {p1}, Ljava/lang/Number;->longValue()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lkotlin/reflect/jvm/internal/impl/h/b/g;->a(J)Lkotlin/reflect/jvm/internal/impl/h/b/p;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/h/b/f;

    goto :goto_0

    .line 70
    :cond_4
    instance-of v1, p1, Ljava/lang/Character;

    if-eqz v1, :cond_5

    check-cast p1, Ljava/lang/Character;

    invoke-virtual {p1}, Ljava/lang/Character;->charValue()C

    move-result v0

    invoke-virtual {p0, v0}, Lkotlin/reflect/jvm/internal/impl/h/b/g;->a(C)Lkotlin/reflect/jvm/internal/impl/h/b/e;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/h/b/f;

    goto :goto_0

    .line 71
    :cond_5
    instance-of v1, p1, Ljava/lang/Float;

    if-eqz v1, :cond_6

    check-cast p1, Ljava/lang/Number;

    invoke-virtual {p1}, Ljava/lang/Number;->floatValue()F

    move-result v0

    invoke-virtual {p0, v0}, Lkotlin/reflect/jvm/internal/impl/h/b/g;->a(F)Lkotlin/reflect/jvm/internal/impl/h/b/k;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/h/b/f;

    goto :goto_0

    .line 72
    :cond_6
    instance-of v1, p1, Ljava/lang/Double;

    if-eqz v1, :cond_7

    check-cast p1, Ljava/lang/Number;

    invoke-virtual {p1}, Ljava/lang/Number;->doubleValue()D

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lkotlin/reflect/jvm/internal/impl/h/b/g;->a(D)Lkotlin/reflect/jvm/internal/impl/h/b/h;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/h/b/f;

    goto :goto_0

    .line 73
    :cond_7
    instance-of v1, p1, Ljava/lang/Boolean;

    if-eqz v1, :cond_8

    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-virtual {p0, v0}, Lkotlin/reflect/jvm/internal/impl/h/b/g;->a(Z)Lkotlin/reflect/jvm/internal/impl/h/b/c;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/h/b/f;

    goto :goto_0

    .line 74
    :cond_8
    instance-of v1, p1, Ljava/lang/String;

    if-eqz v1, :cond_9

    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1}, Lkotlin/reflect/jvm/internal/impl/h/b/g;->b(Ljava/lang/String;)Lkotlin/reflect/jvm/internal/impl/h/b/s;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/h/b/f;

    goto/16 :goto_0

    .line 75
    :cond_9
    invoke-static {p1, v0}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/h/b/g;->a()Lkotlin/reflect/jvm/internal/impl/h/b/q;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/h/b/f;

    goto/16 :goto_0
.end method

.method public final a(D)Lkotlin/reflect/jvm/internal/impl/h/b/h;
    .locals 3

    .prologue
    .line 38
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/h/b/h;

    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/h/b/g;->a:Lkotlin/reflect/jvm/internal/impl/a/l;

    invoke-direct {v0, p1, p2, v1}, Lkotlin/reflect/jvm/internal/impl/h/b/h;-><init>(DLkotlin/reflect/jvm/internal/impl/a/l;)V

    return-object v0
.end method

.method public final a(Lkotlin/reflect/jvm/internal/impl/b/e;)Lkotlin/reflect/jvm/internal/impl/h/b/i;
    .locals 1

    .prologue
    const-string v0, "enumEntryClass"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 50
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/h/b/i;

    invoke-direct {v0, p1}, Lkotlin/reflect/jvm/internal/impl/h/b/i;-><init>(Lkotlin/reflect/jvm/internal/impl/b/e;)V

    return-object v0
.end method

.method public final a(Ljava/lang/String;)Lkotlin/reflect/jvm/internal/impl/h/b/j;
    .locals 1

    .prologue
    const-string v0, "message"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 32
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/h/b/j;->a:Lkotlin/reflect/jvm/internal/impl/h/b/j$a;

    invoke-virtual {v0, p1}, Lkotlin/reflect/jvm/internal/impl/h/b/j$a;->a(Ljava/lang/String;)Lkotlin/reflect/jvm/internal/impl/h/b/j;

    move-result-object v0

    return-object v0
.end method

.method public final a(F)Lkotlin/reflect/jvm/internal/impl/h/b/k;
    .locals 2

    .prologue
    .line 40
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/h/b/k;

    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/h/b/g;->a:Lkotlin/reflect/jvm/internal/impl/a/l;

    invoke-direct {v0, p1, v1}, Lkotlin/reflect/jvm/internal/impl/h/b/k;-><init>(FLkotlin/reflect/jvm/internal/impl/a/l;)V

    return-object v0
.end method

.method public final a(I)Lkotlin/reflect/jvm/internal/impl/h/b/l;
    .locals 2

    .prologue
    .line 30
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/h/b/l;

    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/h/b/g;->a:Lkotlin/reflect/jvm/internal/impl/a/l;

    invoke-direct {v0, p1, v1}, Lkotlin/reflect/jvm/internal/impl/h/b/l;-><init>(ILkotlin/reflect/jvm/internal/impl/a/l;)V

    return-object v0
.end method

.method public final a(Lkotlin/reflect/jvm/internal/impl/k/r;)Lkotlin/reflect/jvm/internal/impl/h/b/o;
    .locals 1

    .prologue
    const-string v0, "type"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 59
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/h/b/o;

    invoke-direct {v0, p1}, Lkotlin/reflect/jvm/internal/impl/h/b/o;-><init>(Lkotlin/reflect/jvm/internal/impl/k/r;)V

    return-object v0
.end method

.method public final a(J)Lkotlin/reflect/jvm/internal/impl/h/b/p;
    .locals 3

    .prologue
    .line 28
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/h/b/p;

    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/h/b/g;->a:Lkotlin/reflect/jvm/internal/impl/a/l;

    invoke-direct {v0, p1, p2, v1}, Lkotlin/reflect/jvm/internal/impl/h/b/p;-><init>(JLkotlin/reflect/jvm/internal/impl/a/l;)V

    return-object v0
.end method

.method public final a()Lkotlin/reflect/jvm/internal/impl/h/b/q;
    .locals 2

    .prologue
    .line 48
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/h/b/q;

    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/h/b/g;->a:Lkotlin/reflect/jvm/internal/impl/a/l;

    invoke-direct {v0, v1}, Lkotlin/reflect/jvm/internal/impl/h/b/q;-><init>(Lkotlin/reflect/jvm/internal/impl/a/l;)V

    return-object v0
.end method

.method public final a(S)Lkotlin/reflect/jvm/internal/impl/h/b/r;
    .locals 2

    .prologue
    .line 34
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/h/b/r;

    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/h/b/g;->a:Lkotlin/reflect/jvm/internal/impl/a/l;

    invoke-direct {v0, p1, v1}, Lkotlin/reflect/jvm/internal/impl/h/b/r;-><init>(SLkotlin/reflect/jvm/internal/impl/a/l;)V

    return-object v0
.end method

.method public final b(Ljava/lang/String;)Lkotlin/reflect/jvm/internal/impl/h/b/s;
    .locals 2

    .prologue
    const-string v0, "value"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 46
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/h/b/s;

    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/h/b/g;->a:Lkotlin/reflect/jvm/internal/impl/a/l;

    invoke-direct {v0, p1, v1}, Lkotlin/reflect/jvm/internal/impl/h/b/s;-><init>(Ljava/lang/String;Lkotlin/reflect/jvm/internal/impl/a/l;)V

    return-object v0
.end method
