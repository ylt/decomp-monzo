.class public final Lkotlin/reflect/jvm/internal/impl/h/e/f;
.super Lkotlin/reflect/jvm/internal/impl/h/e/i;
.source "InnerClassesScopeWrapper.kt"


# instance fields
.field private final a:Lkotlin/reflect/jvm/internal/impl/h/e/h;


# direct methods
.method public constructor <init>(Lkotlin/reflect/jvm/internal/impl/h/e/h;)V
    .locals 1

    .prologue
    const-string v0, "workerScope"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 27
    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/h/e/i;-><init>()V

    iput-object p1, p0, Lkotlin/reflect/jvm/internal/impl/h/e/f;->a:Lkotlin/reflect/jvm/internal/impl/h/e/h;

    return-void
.end method


# virtual methods
.method public synthetic a(Lkotlin/reflect/jvm/internal/impl/h/e/d;Lkotlin/d/a/b;)Ljava/util/Collection;
    .locals 1

    .prologue
    .line 27
    invoke-virtual {p0, p1, p2}, Lkotlin/reflect/jvm/internal/impl/h/e/f;->b(Lkotlin/reflect/jvm/internal/impl/h/e/d;Lkotlin/d/a/b;)Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    return-object v0
.end method

.method public b(Lkotlin/reflect/jvm/internal/impl/h/e/d;Lkotlin/d/a/b;)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/reflect/jvm/internal/impl/h/e/d;",
            "Lkotlin/d/a/b",
            "<-",
            "Lkotlin/reflect/jvm/internal/impl/e/f;",
            "Ljava/lang/Boolean;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/b/h;",
            ">;"
        }
    .end annotation

    .prologue
    const-string v0, "kindFilter"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "nameFilter"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 34
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/h/e/d;->k:Lkotlin/reflect/jvm/internal/impl/h/e/d$a;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/h/e/d$a;->h()I

    move-result v0

    invoke-virtual {p1, v0}, Lkotlin/reflect/jvm/internal/impl/h/e/d;->b(I)Lkotlin/reflect/jvm/internal/impl/h/e/d;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 35
    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/h/e/f;->a:Lkotlin/reflect/jvm/internal/impl/h/e/h;

    invoke-interface {v1, v0, p2}, Lkotlin/reflect/jvm/internal/impl/h/e/h;->a(Lkotlin/reflect/jvm/internal/impl/h/e/d;Lkotlin/d/a/b;)Ljava/util/Collection;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 46
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    check-cast v1, Ljava/util/Collection;

    .line 53
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    instance-of v3, v2, Lkotlin/reflect/jvm/internal/impl/b/i;

    if-eqz v3, :cond_0

    invoke-interface {v1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 34
    :cond_1
    invoke-static {}, Lkotlin/a/m;->a()Ljava/util/List;

    move-result-object v1

    .line 54
    :goto_1
    return-object v1

    :cond_2
    check-cast v1, Ljava/util/List;

    goto :goto_1
.end method

.method public c(Lkotlin/reflect/jvm/internal/impl/e/f;Lkotlin/reflect/jvm/internal/impl/c/a/b;)Lkotlin/reflect/jvm/internal/impl/b/h;
    .locals 3

    .prologue
    const/4 v2, 0x0

    const-string v0, "name"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "location"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 29
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/h/e/f;->a:Lkotlin/reflect/jvm/internal/impl/h/e/h;

    invoke-interface {v0, p1, p2}, Lkotlin/reflect/jvm/internal/impl/h/e/h;->c(Lkotlin/reflect/jvm/internal/impl/e/f;Lkotlin/reflect/jvm/internal/impl/c/a/b;)Lkotlin/reflect/jvm/internal/impl/b/h;

    move-result-object v0

    if-eqz v0, :cond_2

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/h;

    .line 30
    instance-of v1, v0, Lkotlin/reflect/jvm/internal/impl/b/e;

    if-nez v1, :cond_3

    move-object v1, v2

    :goto_0
    check-cast v1, Lkotlin/reflect/jvm/internal/impl/b/e;

    if-eqz v1, :cond_0

    check-cast v1, Lkotlin/reflect/jvm/internal/impl/b/h;

    move-object v0, v1

    .line 29
    :goto_1
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/h;

    .line 31
    :goto_2
    return-object v0

    .line 30
    :cond_0
    instance-of v1, v0, Lkotlin/reflect/jvm/internal/impl/b/ap;

    if-nez v1, :cond_1

    move-object v0, v2

    :cond_1
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/ap;

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/h;

    goto :goto_1

    :cond_2
    move-object v0, v2

    .line 29
    goto :goto_2

    :cond_3
    move-object v1, v0

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 43
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Classes from "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/h/e/f;->a:Lkotlin/reflect/jvm/internal/impl/h/e/h;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
