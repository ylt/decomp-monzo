.class synthetic Lkotlin/reflect/jvm/internal/impl/h/j$8;
.super Ljava/lang/Object;
.source "OverridingUtil.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lkotlin/reflect/jvm/internal/impl/h/j;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1008
    name = null
.end annotation


# static fields
.field static final synthetic a:[I

.field static final synthetic b:[I

.field static final synthetic c:[I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 652
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/b/u;->values()[Lkotlin/reflect/jvm/internal/impl/b/u;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/h/j$8;->c:[I

    :try_start_0
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/h/j$8;->c:[I

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/b/u;->a:Lkotlin/reflect/jvm/internal/impl/b/u;

    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/impl/b/u;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_a

    :goto_0
    :try_start_1
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/h/j$8;->c:[I

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/b/u;->b:Lkotlin/reflect/jvm/internal/impl/b/u;

    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/impl/b/u;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_9

    :goto_1
    :try_start_2
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/h/j$8;->c:[I

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/b/u;->c:Lkotlin/reflect/jvm/internal/impl/b/u;

    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/impl/b/u;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_8

    :goto_2
    :try_start_3
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/h/j$8;->c:[I

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/b/u;->d:Lkotlin/reflect/jvm/internal/impl/b/u;

    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/impl/b/u;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_7

    .line 436
    :goto_3
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/h/j$a$a;->values()[Lkotlin/reflect/jvm/internal/impl/h/j$a$a;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/h/j$8;->b:[I

    :try_start_4
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/h/j$8;->b:[I

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/h/j$a$a;->a:Lkotlin/reflect/jvm/internal/impl/h/j$a$a;

    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/impl/h/j$a$a;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_6

    :goto_4
    :try_start_5
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/h/j$8;->b:[I

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/h/j$a$a;->c:Lkotlin/reflect/jvm/internal/impl/h/j$a$a;

    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/impl/h/j$a$a;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_5
    .catch Ljava/lang/NoSuchFieldError; {:try_start_5 .. :try_end_5} :catch_5

    :goto_5
    :try_start_6
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/h/j$8;->b:[I

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/h/j$a$a;->b:Lkotlin/reflect/jvm/internal/impl/h/j$a$a;

    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/impl/h/j$a$a;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_6
    .catch Ljava/lang/NoSuchFieldError; {:try_start_6 .. :try_end_6} :catch_4

    .line 182
    :goto_6
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/h/d$b;->values()[Lkotlin/reflect/jvm/internal/impl/h/d$b;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/h/j$8;->a:[I

    :try_start_7
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/h/j$8;->a:[I

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/h/d$b;->a:Lkotlin/reflect/jvm/internal/impl/h/d$b;

    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/impl/h/d$b;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_7
    .catch Ljava/lang/NoSuchFieldError; {:try_start_7 .. :try_end_7} :catch_3

    :goto_7
    :try_start_8
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/h/j$8;->a:[I

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/h/d$b;->b:Lkotlin/reflect/jvm/internal/impl/h/d$b;

    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/impl/h/d$b;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_8
    .catch Ljava/lang/NoSuchFieldError; {:try_start_8 .. :try_end_8} :catch_2

    :goto_8
    :try_start_9
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/h/j$8;->a:[I

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/h/d$b;->c:Lkotlin/reflect/jvm/internal/impl/h/d$b;

    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/impl/h/d$b;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_9
    .catch Ljava/lang/NoSuchFieldError; {:try_start_9 .. :try_end_9} :catch_1

    :goto_9
    :try_start_a
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/h/j$8;->a:[I

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/h/d$b;->d:Lkotlin/reflect/jvm/internal/impl/h/d$b;

    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/impl/h/d$b;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_a
    .catch Ljava/lang/NoSuchFieldError; {:try_start_a .. :try_end_a} :catch_0

    :goto_a
    return-void

    :catch_0
    move-exception v0

    goto :goto_a

    :catch_1
    move-exception v0

    goto :goto_9

    :catch_2
    move-exception v0

    goto :goto_8

    :catch_3
    move-exception v0

    goto :goto_7

    .line 436
    :catch_4
    move-exception v0

    goto :goto_6

    :catch_5
    move-exception v0

    goto :goto_5

    :catch_6
    move-exception v0

    goto :goto_4

    .line 652
    :catch_7
    move-exception v0

    goto :goto_3

    :catch_8
    move-exception v0

    goto :goto_2

    :catch_9
    move-exception v0

    goto/16 :goto_1

    :catch_a
    move-exception v0

    goto/16 :goto_0
.end method
