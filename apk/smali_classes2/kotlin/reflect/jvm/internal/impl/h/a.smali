.class public final Lkotlin/reflect/jvm/internal/impl/h/a;
.super Ljava/lang/Object;
.source "DescriptorEquivalenceForOverrides.kt"


# static fields
.field public static final a:Lkotlin/reflect/jvm/internal/impl/h/a;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 22
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/h/a;

    invoke-direct {v0}, Lkotlin/reflect/jvm/internal/impl/h/a;-><init>()V

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    check-cast p0, Lkotlin/reflect/jvm/internal/impl/h/a;

    sput-object p0, Lkotlin/reflect/jvm/internal/impl/h/a;->a:Lkotlin/reflect/jvm/internal/impl/h/a;

    return-void
.end method

.method private final a(Lkotlin/reflect/jvm/internal/impl/b/aq;Lkotlin/reflect/jvm/internal/impl/b/aq;Lkotlin/d/a/m;)Z
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/reflect/jvm/internal/impl/b/aq;",
            "Lkotlin/reflect/jvm/internal/impl/b/aq;",
            "Lkotlin/d/a/m",
            "<-",
            "Lkotlin/reflect/jvm/internal/impl/b/m;",
            "-",
            "Lkotlin/reflect/jvm/internal/impl/b/m;",
            "Ljava/lang/Boolean;",
            ">;)Z"
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 52
    invoke-static {p1, p2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    move v3, v2

    .line 57
    :cond_0
    :goto_0
    return v3

    .line 53
    :cond_1
    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/b/aq;->y_()Lkotlin/reflect/jvm/internal/impl/b/m;

    move-result-object v0

    invoke-interface {p2}, Lkotlin/reflect/jvm/internal/impl/b/aq;->y_()Lkotlin/reflect/jvm/internal/impl/b/m;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    move-object v0, p1

    .line 55
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/m;

    move-object v1, p2

    check-cast v1, Lkotlin/reflect/jvm/internal/impl/b/m;

    invoke-direct {p0, v0, v1, p3}, Lkotlin/reflect/jvm/internal/impl/h/a;->a(Lkotlin/reflect/jvm/internal/impl/b/m;Lkotlin/reflect/jvm/internal/impl/b/m;Lkotlin/d/a/m;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 57
    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/b/aq;->g()I

    move-result v0

    invoke-interface {p2}, Lkotlin/reflect/jvm/internal/impl/b/aq;->g()I

    move-result v1

    if-ne v0, v1, :cond_2

    move v0, v2

    :goto_1
    move v3, v0

    goto :goto_0

    :cond_2
    move v0, v3

    goto :goto_1
.end method

.method private final a(Lkotlin/reflect/jvm/internal/impl/b/e;Lkotlin/reflect/jvm/internal/impl/b/e;)Z
    .locals 2

    .prologue
    .line 44
    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/b/e;->e()Lkotlin/reflect/jvm/internal/impl/k/ad;

    move-result-object v0

    invoke-interface {p2}, Lkotlin/reflect/jvm/internal/impl/b/e;->e()Lkotlin/reflect/jvm/internal/impl/k/ad;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method private final a(Lkotlin/reflect/jvm/internal/impl/b/m;Lkotlin/reflect/jvm/internal/impl/b/m;Lkotlin/d/a/m;)Z
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/reflect/jvm/internal/impl/b/m;",
            "Lkotlin/reflect/jvm/internal/impl/b/m;",
            "Lkotlin/d/a/m",
            "<-",
            "Lkotlin/reflect/jvm/internal/impl/b/m;",
            "-",
            "Lkotlin/reflect/jvm/internal/impl/b/m;",
            "Ljava/lang/Boolean;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 96
    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/b/m;->y_()Lkotlin/reflect/jvm/internal/impl/b/m;

    move-result-object v0

    .line 97
    invoke-interface {p2}, Lkotlin/reflect/jvm/internal/impl/b/m;->y_()Lkotlin/reflect/jvm/internal/impl/b/m;

    move-result-object v1

    .line 101
    instance-of v2, v0, Lkotlin/reflect/jvm/internal/impl/b/b;

    if-nez v2, :cond_0

    instance-of v2, v1, Lkotlin/reflect/jvm/internal/impl/b/b;

    if-eqz v2, :cond_1

    .line 102
    :cond_0
    invoke-interface {p3, v0, v1}, Lkotlin/d/a/m;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    .line 105
    :goto_0
    return v0

    :cond_1
    invoke-virtual {p0, v0, v1}, Lkotlin/reflect/jvm/internal/impl/h/a;->a(Lkotlin/reflect/jvm/internal/impl/b/m;Lkotlin/reflect/jvm/internal/impl/b/m;)Z

    move-result v0

    goto :goto_0
.end method

.method public static bridge synthetic a(Lkotlin/reflect/jvm/internal/impl/h/a;Lkotlin/reflect/jvm/internal/impl/b/a;Lkotlin/reflect/jvm/internal/impl/b/a;ZILjava/lang/Object;)Z
    .locals 1

    .prologue
    and-int/lit8 v0, p4, 0x4

    if-eqz v0, :cond_0

    .line 63
    const/4 p3, 0x0

    :cond_0
    invoke-virtual {p0, p1, p2, p3}, Lkotlin/reflect/jvm/internal/impl/h/a;->a(Lkotlin/reflect/jvm/internal/impl/b/a;Lkotlin/reflect/jvm/internal/impl/b/a;Z)Z

    move-result v0

    return v0
.end method

.method public static final synthetic a(Lkotlin/reflect/jvm/internal/impl/h/a;Lkotlin/reflect/jvm/internal/impl/b/aq;Lkotlin/reflect/jvm/internal/impl/b/aq;Lkotlin/d/a/m;)Z
    .locals 1

    .prologue
    .line 22
    invoke-direct {p0, p1, p2, p3}, Lkotlin/reflect/jvm/internal/impl/h/a;->a(Lkotlin/reflect/jvm/internal/impl/b/aq;Lkotlin/reflect/jvm/internal/impl/b/aq;Lkotlin/d/a/m;)Z

    move-result v0

    return v0
.end method

.method static bridge synthetic a(Lkotlin/reflect/jvm/internal/impl/h/a;Lkotlin/reflect/jvm/internal/impl/b/aq;Lkotlin/reflect/jvm/internal/impl/b/aq;Lkotlin/d/a/m;ILjava/lang/Object;)Z
    .locals 1

    .prologue
    and-int/lit8 v0, p4, 0x4

    if-eqz v0, :cond_0

    .line 50
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/h/a$c;->a:Lkotlin/reflect/jvm/internal/impl/h/a$c;

    check-cast v0, Lkotlin/d/a/m;

    :goto_0
    invoke-direct {p0, p1, p2, v0}, Lkotlin/reflect/jvm/internal/impl/h/a;->a(Lkotlin/reflect/jvm/internal/impl/b/aq;Lkotlin/reflect/jvm/internal/impl/b/aq;Lkotlin/d/a/m;)Z

    move-result v0

    return v0

    :cond_0
    move-object v0, p3

    goto :goto_0
.end method


# virtual methods
.method public final a(Lkotlin/reflect/jvm/internal/impl/b/a;Lkotlin/reflect/jvm/internal/impl/b/a;Z)Z
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x0

    const-string v0, "a"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "b"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 65
    invoke-static {p1, p2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    move v4, v3

    .line 87
    :cond_0
    :goto_0
    return v4

    .line 66
    :cond_1
    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/b/a;->D_()Lkotlin/reflect/jvm/internal/impl/e/f;

    move-result-object v0

    invoke-interface {p2}, Lkotlin/reflect/jvm/internal/impl/b/a;->D_()Lkotlin/reflect/jvm/internal/impl/e/f;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-nez v0, :cond_0

    .line 67
    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/b/a;->y_()Lkotlin/reflect/jvm/internal/impl/b/m;

    move-result-object v0

    invoke-interface {p2}, Lkotlin/reflect/jvm/internal/impl/b/a;->y_()Lkotlin/reflect/jvm/internal/impl/b/m;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    move-object v0, p1

    .line 70
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/m;

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/h/c;->b(Lkotlin/reflect/jvm/internal/impl/b/m;)Z

    move-result v0

    if-nez v0, :cond_0

    move-object v0, p2

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/m;

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/h/c;->b(Lkotlin/reflect/jvm/internal/impl/b/m;)Z

    move-result v0

    if-nez v0, :cond_0

    move-object v0, p1

    .line 72
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/m;

    move-object v1, p2

    check-cast v1, Lkotlin/reflect/jvm/internal/impl/b/m;

    sget-object v2, Lkotlin/reflect/jvm/internal/impl/h/a$a;->a:Lkotlin/reflect/jvm/internal/impl/h/a$a;

    check-cast v2, Lkotlin/d/a/m;

    invoke-direct {p0, v0, v1, v2}, Lkotlin/reflect/jvm/internal/impl/h/a;->a(Lkotlin/reflect/jvm/internal/impl/b/m;Lkotlin/reflect/jvm/internal/impl/b/m;Lkotlin/d/a/m;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 74
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/h/a$b;

    invoke-direct {v0, p1, p2}, Lkotlin/reflect/jvm/internal/impl/h/a$b;-><init>(Lkotlin/reflect/jvm/internal/impl/b/a;Lkotlin/reflect/jvm/internal/impl/b/a;)V

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/a/c$a;

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/h/j;->a(Lkotlin/reflect/jvm/internal/impl/k/a/c$a;)Lkotlin/reflect/jvm/internal/impl/h/j;

    move-result-object v1

    .line 86
    if-nez p3, :cond_2

    move v0, v3

    :goto_1
    invoke-virtual {v1, p1, p2, v5, v0}, Lkotlin/reflect/jvm/internal/impl/h/j;->a(Lkotlin/reflect/jvm/internal/impl/b/a;Lkotlin/reflect/jvm/internal/impl/b/a;Lkotlin/reflect/jvm/internal/impl/b/e;Z)Lkotlin/reflect/jvm/internal/impl/h/j$a;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/h/j$a;->b()Lkotlin/reflect/jvm/internal/impl/h/j$a$a;

    move-result-object v0

    sget-object v2, Lkotlin/reflect/jvm/internal/impl/h/j$a$a;->a:Lkotlin/reflect/jvm/internal/impl/h/j$a$a;

    invoke-static {v0, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 87
    if-nez p3, :cond_3

    move v0, v3

    :goto_2
    invoke-virtual {v1, p2, p1, v5, v0}, Lkotlin/reflect/jvm/internal/impl/h/j;->a(Lkotlin/reflect/jvm/internal/impl/b/a;Lkotlin/reflect/jvm/internal/impl/b/a;Lkotlin/reflect/jvm/internal/impl/b/e;Z)Lkotlin/reflect/jvm/internal/impl/h/j$a;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/h/j$a;->b()Lkotlin/reflect/jvm/internal/impl/h/j$a$a;

    move-result-object v0

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/h/j$a$a;->a:Lkotlin/reflect/jvm/internal/impl/h/j$a$a;

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    :goto_3
    move v4, v3

    goto :goto_0

    :cond_2
    move v0, v4

    .line 86
    goto :goto_1

    :cond_3
    move v0, v4

    .line 87
    goto :goto_2

    :cond_4
    move v3, v4

    goto :goto_3
.end method

.method public final a(Lkotlin/reflect/jvm/internal/impl/b/m;Lkotlin/reflect/jvm/internal/impl/b/m;)Z
    .locals 11

    .prologue
    const/4 v4, 0x4

    const/4 v3, 0x0

    .line 26
    instance-of v0, p1, Lkotlin/reflect/jvm/internal/impl/b/e;

    if-eqz v0, :cond_0

    .line 27
    instance-of v0, p2, Lkotlin/reflect/jvm/internal/impl/b/e;

    if-eqz v0, :cond_0

    check-cast p1, Lkotlin/reflect/jvm/internal/impl/b/e;

    check-cast p2, Lkotlin/reflect/jvm/internal/impl/b/e;

    invoke-direct {p0, p1, p2}, Lkotlin/reflect/jvm/internal/impl/h/a;->a(Lkotlin/reflect/jvm/internal/impl/b/e;Lkotlin/reflect/jvm/internal/impl/b/e;)Z

    move-result v0

    .line 25
    :goto_0
    return v0

    .line 29
    :cond_0
    instance-of v0, p1, Lkotlin/reflect/jvm/internal/impl/b/aq;

    if-eqz v0, :cond_1

    .line 30
    instance-of v0, p2, Lkotlin/reflect/jvm/internal/impl/b/aq;

    if-eqz v0, :cond_1

    move-object v1, p1

    check-cast v1, Lkotlin/reflect/jvm/internal/impl/b/aq;

    move-object v2, p2

    check-cast v2, Lkotlin/reflect/jvm/internal/impl/b/aq;

    move-object v0, p0

    move-object v5, v3

    invoke-static/range {v0 .. v5}, Lkotlin/reflect/jvm/internal/impl/h/a;->a(Lkotlin/reflect/jvm/internal/impl/h/a;Lkotlin/reflect/jvm/internal/impl/b/aq;Lkotlin/reflect/jvm/internal/impl/b/aq;Lkotlin/d/a/m;ILjava/lang/Object;)Z

    move-result v0

    goto :goto_0

    .line 32
    :cond_1
    instance-of v0, p1, Lkotlin/reflect/jvm/internal/impl/b/a;

    if-eqz v0, :cond_2

    .line 33
    instance-of v0, p2, Lkotlin/reflect/jvm/internal/impl/b/a;

    if-eqz v0, :cond_2

    move-object v6, p1

    check-cast v6, Lkotlin/reflect/jvm/internal/impl/b/a;

    move-object v7, p2

    check-cast v7, Lkotlin/reflect/jvm/internal/impl/b/a;

    const/4 v8, 0x0

    move-object v5, p0

    move v9, v4

    move-object v10, v3

    invoke-static/range {v5 .. v10}, Lkotlin/reflect/jvm/internal/impl/h/a;->a(Lkotlin/reflect/jvm/internal/impl/h/a;Lkotlin/reflect/jvm/internal/impl/b/a;Lkotlin/reflect/jvm/internal/impl/b/a;ZILjava/lang/Object;)Z

    move-result v0

    goto :goto_0

    .line 35
    :cond_2
    instance-of v0, p1, Lkotlin/reflect/jvm/internal/impl/b/y;

    if-eqz v0, :cond_3

    .line 36
    instance-of v0, p2, Lkotlin/reflect/jvm/internal/impl/b/y;

    if-eqz v0, :cond_3

    check-cast p1, Lkotlin/reflect/jvm/internal/impl/b/y;

    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/b/y;->f()Lkotlin/reflect/jvm/internal/impl/e/b;

    move-result-object v0

    check-cast p2, Lkotlin/reflect/jvm/internal/impl/b/y;

    invoke-interface {p2}, Lkotlin/reflect/jvm/internal/impl/b/y;->f()Lkotlin/reflect/jvm/internal/impl/e/b;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0

    .line 38
    :cond_3
    invoke-static {p1, p2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method
