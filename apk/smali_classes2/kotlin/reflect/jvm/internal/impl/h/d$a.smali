.class public final enum Lkotlin/reflect/jvm/internal/impl/h/d$a;
.super Ljava/lang/Enum;
.source "ExternalOverridabilityCondition.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lkotlin/reflect/jvm/internal/impl/h/d;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lkotlin/reflect/jvm/internal/impl/h/d$a;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lkotlin/reflect/jvm/internal/impl/h/d$a;

.field public static final enum b:Lkotlin/reflect/jvm/internal/impl/h/d$a;

.field public static final enum c:Lkotlin/reflect/jvm/internal/impl/h/d$a;

.field private static final synthetic d:[Lkotlin/reflect/jvm/internal/impl/h/d$a;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 30
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/h/d$a;

    const-string v1, "CONFLICTS_ONLY"

    invoke-direct {v0, v1, v2}, Lkotlin/reflect/jvm/internal/impl/h/d$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/h/d$a;->a:Lkotlin/reflect/jvm/internal/impl/h/d$a;

    new-instance v0, Lkotlin/reflect/jvm/internal/impl/h/d$a;

    const-string v1, "SUCCESS_ONLY"

    invoke-direct {v0, v1, v3}, Lkotlin/reflect/jvm/internal/impl/h/d$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/h/d$a;->b:Lkotlin/reflect/jvm/internal/impl/h/d$a;

    new-instance v0, Lkotlin/reflect/jvm/internal/impl/h/d$a;

    const-string v1, "BOTH"

    invoke-direct {v0, v1, v4}, Lkotlin/reflect/jvm/internal/impl/h/d$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/h/d$a;->c:Lkotlin/reflect/jvm/internal/impl/h/d$a;

    .line 29
    const/4 v0, 0x3

    new-array v0, v0, [Lkotlin/reflect/jvm/internal/impl/h/d$a;

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/h/d$a;->a:Lkotlin/reflect/jvm/internal/impl/h/d$a;

    aput-object v1, v0, v2

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/h/d$a;->b:Lkotlin/reflect/jvm/internal/impl/h/d$a;

    aput-object v1, v0, v3

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/h/d$a;->c:Lkotlin/reflect/jvm/internal/impl/h/d$a;

    aput-object v1, v0, v4

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/h/d$a;->d:[Lkotlin/reflect/jvm/internal/impl/h/d$a;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 29
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lkotlin/reflect/jvm/internal/impl/h/d$a;
    .locals 1

    .prologue
    .line 29
    const-class v0, Lkotlin/reflect/jvm/internal/impl/h/d$a;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/h/d$a;

    return-object v0
.end method

.method public static values()[Lkotlin/reflect/jvm/internal/impl/h/d$a;
    .locals 1

    .prologue
    .line 29
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/h/d$a;->d:[Lkotlin/reflect/jvm/internal/impl/h/d$a;

    invoke-virtual {v0}, [Lkotlin/reflect/jvm/internal/impl/h/d$a;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lkotlin/reflect/jvm/internal/impl/h/d$a;

    return-object v0
.end method
