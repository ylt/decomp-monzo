.class public Lkotlin/reflect/jvm/internal/impl/h/e;
.super Ljava/lang/Object;
.source "MemberComparator.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Lkotlin/reflect/jvm/internal/impl/b/m;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:Lkotlin/reflect/jvm/internal/impl/h/e;

.field static final synthetic b:Z

.field private static final c:Lkotlin/reflect/jvm/internal/impl/g/c;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 34
    const-class v0, Lkotlin/reflect/jvm/internal/impl/h/e;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lkotlin/reflect/jvm/internal/impl/h/e;->b:Z

    .line 35
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/h/e;

    invoke-direct {v0}, Lkotlin/reflect/jvm/internal/impl/h/e;-><init>()V

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/h/e;->a:Lkotlin/reflect/jvm/internal/impl/h/e;

    .line 37
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/g/c;->i:Lkotlin/reflect/jvm/internal/impl/g/c$a;

    new-instance v1, Lkotlin/reflect/jvm/internal/impl/h/e$1;

    invoke-direct {v1}, Lkotlin/reflect/jvm/internal/impl/h/e$1;-><init>()V

    invoke-virtual {v0, v1}, Lkotlin/reflect/jvm/internal/impl/g/c$a;->a(Lkotlin/d/a/b;)Lkotlin/reflect/jvm/internal/impl/g/c;

    move-result-object v0

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/h/e;->c:Lkotlin/reflect/jvm/internal/impl/g/c;

    return-void

    .line 34
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 50
    return-void
.end method

.method private static a(Lkotlin/reflect/jvm/internal/impl/b/m;)I
    .locals 1

    .prologue
    .line 53
    invoke-static {p0}, Lkotlin/reflect/jvm/internal/impl/h/c;->l(Lkotlin/reflect/jvm/internal/impl/b/m;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 54
    const/16 v0, 0x8

    .line 81
    :goto_0
    return v0

    .line 56
    :cond_0
    instance-of v0, p0, Lkotlin/reflect/jvm/internal/impl/b/l;

    if-eqz v0, :cond_1

    .line 57
    const/4 v0, 0x7

    goto :goto_0

    .line 59
    :cond_1
    instance-of v0, p0, Lkotlin/reflect/jvm/internal/impl/b/ag;

    if-eqz v0, :cond_3

    .line 60
    check-cast p0, Lkotlin/reflect/jvm/internal/impl/b/ag;

    invoke-interface {p0}, Lkotlin/reflect/jvm/internal/impl/b/ag;->d()Lkotlin/reflect/jvm/internal/impl/b/aj;

    move-result-object v0

    if-nez v0, :cond_2

    .line 61
    const/4 v0, 0x6

    goto :goto_0

    .line 64
    :cond_2
    const/4 v0, 0x5

    goto :goto_0

    .line 67
    :cond_3
    instance-of v0, p0, Lkotlin/reflect/jvm/internal/impl/b/s;

    if-eqz v0, :cond_5

    .line 68
    check-cast p0, Lkotlin/reflect/jvm/internal/impl/b/s;

    invoke-interface {p0}, Lkotlin/reflect/jvm/internal/impl/b/s;->d()Lkotlin/reflect/jvm/internal/impl/b/aj;

    move-result-object v0

    if-nez v0, :cond_4

    .line 69
    const/4 v0, 0x4

    goto :goto_0

    .line 72
    :cond_4
    const/4 v0, 0x3

    goto :goto_0

    .line 75
    :cond_5
    instance-of v0, p0, Lkotlin/reflect/jvm/internal/impl/b/e;

    if-eqz v0, :cond_6

    .line 76
    const/4 v0, 0x2

    goto :goto_0

    .line 78
    :cond_6
    instance-of v0, p0, Lkotlin/reflect/jvm/internal/impl/b/ap;

    if-eqz v0, :cond_7

    .line 79
    const/4 v0, 0x1

    goto :goto_0

    .line 81
    :cond_7
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a(Lkotlin/reflect/jvm/internal/impl/b/m;Lkotlin/reflect/jvm/internal/impl/b/m;)I
    .locals 12

    .prologue
    const/4 v2, 0x1

    const/4 v4, 0x0

    .line 86
    invoke-static {p2}, Lkotlin/reflect/jvm/internal/impl/h/e;->a(Lkotlin/reflect/jvm/internal/impl/b/m;)I

    move-result v0

    invoke-static {p1}, Lkotlin/reflect/jvm/internal/impl/h/e;->a(Lkotlin/reflect/jvm/internal/impl/b/m;)I

    move-result v1

    sub-int/2addr v0, v1

    .line 87
    if-eqz v0, :cond_1

    move v4, v0

    .line 201
    :cond_0
    :goto_0
    return v4

    .line 90
    :cond_1
    invoke-static {p1}, Lkotlin/reflect/jvm/internal/impl/h/c;->l(Lkotlin/reflect/jvm/internal/impl/b/m;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-static {p2}, Lkotlin/reflect/jvm/internal/impl/h/c;->l(Lkotlin/reflect/jvm/internal/impl/b/m;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 95
    :cond_2
    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/b/m;->D_()Lkotlin/reflect/jvm/internal/impl/e/f;

    move-result-object v0

    invoke-interface {p2}, Lkotlin/reflect/jvm/internal/impl/b/m;->D_()Lkotlin/reflect/jvm/internal/impl/e/f;

    move-result-object v1

    invoke-virtual {v0, v1}, Lkotlin/reflect/jvm/internal/impl/e/f;->a(Lkotlin/reflect/jvm/internal/impl/e/f;)I

    move-result v0

    .line 96
    if-eqz v0, :cond_3

    move v4, v0

    .line 97
    goto :goto_0

    .line 100
    :cond_3
    instance-of v0, p1, Lkotlin/reflect/jvm/internal/impl/b/ap;

    if-eqz v0, :cond_5

    instance-of v0, p2, Lkotlin/reflect/jvm/internal/impl/b/ap;

    if-eqz v0, :cond_5

    move-object v0, p1

    .line 101
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/ap;

    move-object v1, p2

    .line 102
    check-cast v1, Lkotlin/reflect/jvm/internal/impl/b/ap;

    .line 103
    sget-object v2, Lkotlin/reflect/jvm/internal/impl/h/e;->c:Lkotlin/reflect/jvm/internal/impl/g/c;

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/b/ap;->a()Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v0

    invoke-virtual {v2, v0}, Lkotlin/reflect/jvm/internal/impl/g/c;->a(Lkotlin/reflect/jvm/internal/impl/k/r;)Ljava/lang/String;

    move-result-object v0

    .line 104
    sget-object v2, Lkotlin/reflect/jvm/internal/impl/h/e;->c:Lkotlin/reflect/jvm/internal/impl/g/c;

    invoke-interface {v1}, Lkotlin/reflect/jvm/internal/impl/b/ap;->a()Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v1

    invoke-virtual {v2, v1}, Lkotlin/reflect/jvm/internal/impl/g/c;->a(Lkotlin/reflect/jvm/internal/impl/k/r;)Ljava/lang/String;

    move-result-object v1

    .line 105
    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v4

    .line 106
    if-nez v4, :cond_0

    .line 195
    :cond_4
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/h/e;->c:Lkotlin/reflect/jvm/internal/impl/g/c;

    invoke-virtual {v0, p1}, Lkotlin/reflect/jvm/internal/impl/g/c;->a(Lkotlin/reflect/jvm/internal/impl/b/m;)Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/h/e;->c:Lkotlin/reflect/jvm/internal/impl/g/c;

    invoke-virtual {v1, p2}, Lkotlin/reflect/jvm/internal/impl/g/c;->a(Lkotlin/reflect/jvm/internal/impl/b/m;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v4

    .line 196
    if-nez v4, :cond_0

    .line 198
    invoke-static {p1}, Lkotlin/reflect/jvm/internal/impl/h/c;->g(Lkotlin/reflect/jvm/internal/impl/b/m;)Lkotlin/reflect/jvm/internal/impl/b/w;

    move-result-object v0

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/b/w;->D_()Lkotlin/reflect/jvm/internal/impl/e/f;

    move-result-object v0

    .line 199
    invoke-static {p2}, Lkotlin/reflect/jvm/internal/impl/h/c;->g(Lkotlin/reflect/jvm/internal/impl/b/m;)Lkotlin/reflect/jvm/internal/impl/b/w;

    move-result-object v1

    invoke-interface {v1}, Lkotlin/reflect/jvm/internal/impl/b/w;->D_()Lkotlin/reflect/jvm/internal/impl/e/f;

    move-result-object v1

    .line 201
    invoke-virtual {v0, v1}, Lkotlin/reflect/jvm/internal/impl/e/f;->a(Lkotlin/reflect/jvm/internal/impl/e/f;)I

    move-result v4

    goto :goto_0

    .line 110
    :cond_5
    instance-of v0, p1, Lkotlin/reflect/jvm/internal/impl/b/a;

    if-eqz v0, :cond_11

    instance-of v0, p2, Lkotlin/reflect/jvm/internal/impl/b/a;

    if-eqz v0, :cond_11

    move-object v0, p1

    .line 111
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/a;

    move-object v1, p2

    .line 112
    check-cast v1, Lkotlin/reflect/jvm/internal/impl/b/a;

    .line 114
    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/b/a;->d()Lkotlin/reflect/jvm/internal/impl/b/aj;

    move-result-object v5

    .line 115
    invoke-interface {v1}, Lkotlin/reflect/jvm/internal/impl/b/a;->d()Lkotlin/reflect/jvm/internal/impl/b/aj;

    move-result-object v6

    .line 116
    sget-boolean v3, Lkotlin/reflect/jvm/internal/impl/h/e;->b:Z

    if-nez v3, :cond_8

    if-eqz v5, :cond_6

    move v3, v2

    :goto_1
    if-eqz v6, :cond_7

    :goto_2
    if-eq v3, v2, :cond_8

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_6
    move v3, v4

    goto :goto_1

    :cond_7
    move v2, v4

    goto :goto_2

    .line 117
    :cond_8
    if-eqz v5, :cond_9

    .line 118
    sget-object v2, Lkotlin/reflect/jvm/internal/impl/h/e;->c:Lkotlin/reflect/jvm/internal/impl/g/c;

    invoke-interface {v5}, Lkotlin/reflect/jvm/internal/impl/b/aj;->s()Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v3

    invoke-virtual {v2, v3}, Lkotlin/reflect/jvm/internal/impl/g/c;->a(Lkotlin/reflect/jvm/internal/impl/k/r;)Ljava/lang/String;

    move-result-object v2

    .line 119
    sget-object v3, Lkotlin/reflect/jvm/internal/impl/h/e;->c:Lkotlin/reflect/jvm/internal/impl/g/c;

    invoke-interface {v6}, Lkotlin/reflect/jvm/internal/impl/b/aj;->s()Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v5

    invoke-virtual {v3, v5}, Lkotlin/reflect/jvm/internal/impl/g/c;->a(Lkotlin/reflect/jvm/internal/impl/k/r;)Ljava/lang/String;

    move-result-object v3

    .line 120
    invoke-virtual {v2, v3}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v2

    .line 121
    if-eqz v2, :cond_9

    move v4, v2

    .line 122
    goto/16 :goto_0

    .line 126
    :cond_9
    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/b/a;->i()Ljava/util/List;

    move-result-object v5

    .line 127
    invoke-interface {v1}, Lkotlin/reflect/jvm/internal/impl/b/a;->i()Ljava/util/List;

    move-result-object v6

    move v3, v4

    .line 128
    :goto_3
    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v2

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v7

    invoke-static {v2, v7}, Ljava/lang/Math;->min(II)I

    move-result v2

    if-ge v3, v2, :cond_b

    .line 129
    sget-object v7, Lkotlin/reflect/jvm/internal/impl/h/e;->c:Lkotlin/reflect/jvm/internal/impl/g/c;

    invoke-interface {v5, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lkotlin/reflect/jvm/internal/impl/b/at;

    invoke-interface {v2}, Lkotlin/reflect/jvm/internal/impl/b/at;->s()Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v2

    invoke-virtual {v7, v2}, Lkotlin/reflect/jvm/internal/impl/g/c;->a(Lkotlin/reflect/jvm/internal/impl/k/r;)Ljava/lang/String;

    move-result-object v7

    .line 130
    sget-object v8, Lkotlin/reflect/jvm/internal/impl/h/e;->c:Lkotlin/reflect/jvm/internal/impl/g/c;

    invoke-interface {v6, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lkotlin/reflect/jvm/internal/impl/b/at;

    invoke-interface {v2}, Lkotlin/reflect/jvm/internal/impl/b/at;->s()Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v2

    invoke-virtual {v8, v2}, Lkotlin/reflect/jvm/internal/impl/g/c;->a(Lkotlin/reflect/jvm/internal/impl/k/r;)Ljava/lang/String;

    move-result-object v2

    .line 131
    invoke-virtual {v7, v2}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v2

    .line 132
    if-eqz v2, :cond_a

    move v4, v2

    .line 133
    goto/16 :goto_0

    .line 128
    :cond_a
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_3

    .line 137
    :cond_b
    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v2

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v3

    sub-int/2addr v2, v3

    .line 138
    if-eqz v2, :cond_c

    move v4, v2

    .line 139
    goto/16 :goto_0

    .line 142
    :cond_c
    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/b/a;->f()Ljava/util/List;

    move-result-object v6

    .line 143
    invoke-interface {v1}, Lkotlin/reflect/jvm/internal/impl/b/a;->f()Ljava/util/List;

    move-result-object v7

    move v3, v4

    .line 144
    :goto_4
    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v2

    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v5

    invoke-static {v2, v5}, Ljava/lang/Math;->min(II)I

    move-result v2

    if-ge v3, v2, :cond_10

    .line 145
    invoke-interface {v6, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lkotlin/reflect/jvm/internal/impl/b/aq;

    invoke-interface {v2}, Lkotlin/reflect/jvm/internal/impl/b/aq;->I_()Ljava/util/List;

    move-result-object v8

    .line 146
    invoke-interface {v7, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lkotlin/reflect/jvm/internal/impl/b/aq;

    invoke-interface {v2}, Lkotlin/reflect/jvm/internal/impl/b/aq;->I_()Ljava/util/List;

    move-result-object v9

    .line 147
    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v2

    invoke-interface {v9}, Ljava/util/List;->size()I

    move-result v5

    sub-int/2addr v2, v5

    .line 148
    if-eqz v2, :cond_d

    move v4, v2

    .line 149
    goto/16 :goto_0

    :cond_d
    move v5, v4

    .line 151
    :goto_5
    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v2

    if-ge v5, v2, :cond_f

    .line 152
    sget-object v10, Lkotlin/reflect/jvm/internal/impl/h/e;->c:Lkotlin/reflect/jvm/internal/impl/g/c;

    invoke-interface {v8, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lkotlin/reflect/jvm/internal/impl/k/r;

    invoke-virtual {v10, v2}, Lkotlin/reflect/jvm/internal/impl/g/c;->a(Lkotlin/reflect/jvm/internal/impl/k/r;)Ljava/lang/String;

    move-result-object v10

    .line 153
    sget-object v11, Lkotlin/reflect/jvm/internal/impl/h/e;->c:Lkotlin/reflect/jvm/internal/impl/g/c;

    invoke-interface {v9, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lkotlin/reflect/jvm/internal/impl/k/r;

    invoke-virtual {v11, v2}, Lkotlin/reflect/jvm/internal/impl/g/c;->a(Lkotlin/reflect/jvm/internal/impl/k/r;)Ljava/lang/String;

    move-result-object v2

    .line 154
    invoke-virtual {v10, v2}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v2

    .line 155
    if-eqz v2, :cond_e

    move v4, v2

    .line 156
    goto/16 :goto_0

    .line 151
    :cond_e
    add-int/lit8 v2, v5, 0x1

    move v5, v2

    goto :goto_5

    .line 144
    :cond_f
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_4

    .line 161
    :cond_10
    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v2

    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v3

    sub-int v4, v2, v3

    .line 162
    if-nez v4, :cond_0

    .line 166
    instance-of v2, v0, Lkotlin/reflect/jvm/internal/impl/b/b;

    if-eqz v2, :cond_4

    instance-of v2, v1, Lkotlin/reflect/jvm/internal/impl/b/b;

    if-eqz v2, :cond_4

    .line 167
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/b;

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/b/b;->n()Lkotlin/reflect/jvm/internal/impl/b/b$a;

    move-result-object v0

    .line 168
    check-cast v1, Lkotlin/reflect/jvm/internal/impl/b/b;

    invoke-interface {v1}, Lkotlin/reflect/jvm/internal/impl/b/b;->n()Lkotlin/reflect/jvm/internal/impl/b/b$a;

    move-result-object v1

    .line 169
    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/b/b$a;->ordinal()I

    move-result v0

    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/impl/b/b$a;->ordinal()I

    move-result v1

    sub-int v4, v0, v1

    .line 170
    if-eqz v4, :cond_4

    goto/16 :goto_0

    .line 175
    :cond_11
    instance-of v0, p1, Lkotlin/reflect/jvm/internal/impl/b/e;

    if-eqz v0, :cond_14

    instance-of v0, p2, Lkotlin/reflect/jvm/internal/impl/b/e;

    if-eqz v0, :cond_14

    move-object v0, p1

    .line 176
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/e;

    move-object v1, p2

    .line 177
    check-cast v1, Lkotlin/reflect/jvm/internal/impl/b/e;

    .line 179
    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/b/e;->l()Lkotlin/reflect/jvm/internal/impl/b/f;

    move-result-object v3

    invoke-virtual {v3}, Lkotlin/reflect/jvm/internal/impl/b/f;->ordinal()I

    move-result v3

    invoke-interface {v1}, Lkotlin/reflect/jvm/internal/impl/b/e;->l()Lkotlin/reflect/jvm/internal/impl/b/f;

    move-result-object v4

    invoke-virtual {v4}, Lkotlin/reflect/jvm/internal/impl/b/f;->ordinal()I

    move-result v4

    if-eq v3, v4, :cond_12

    .line 180
    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/b/e;->l()Lkotlin/reflect/jvm/internal/impl/b/f;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/b/f;->ordinal()I

    move-result v0

    invoke-interface {v1}, Lkotlin/reflect/jvm/internal/impl/b/e;->l()Lkotlin/reflect/jvm/internal/impl/b/f;

    move-result-object v1

    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/impl/b/f;->ordinal()I

    move-result v1

    sub-int v4, v0, v1

    goto/16 :goto_0

    .line 183
    :cond_12
    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/b/e;->q()Z

    move-result v3

    invoke-interface {v1}, Lkotlin/reflect/jvm/internal/impl/b/e;->q()Z

    move-result v1

    if-eq v3, v1, :cond_4

    .line 184
    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/b/e;->q()Z

    move-result v0

    if-eqz v0, :cond_13

    move v0, v2

    :goto_6
    move v4, v0

    goto/16 :goto_0

    :cond_13
    const/4 v0, -0x1

    goto :goto_6

    .line 188
    :cond_14
    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "Unsupported pair of descriptors:\n\'%s\' Class: %s\n%s\' Class: %s"

    const/4 v3, 0x4

    new-array v3, v3, [Ljava/lang/Object;

    aput-object p1, v3, v4

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    aput-object v4, v3, v2

    const/4 v2, 0x2

    aput-object p2, v3, v2

    const/4 v2, 0x3

    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    aput-object v4, v3, v2

    invoke-static {v1, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0
.end method

.method public synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 34
    check-cast p1, Lkotlin/reflect/jvm/internal/impl/b/m;

    check-cast p2, Lkotlin/reflect/jvm/internal/impl/b/m;

    invoke-virtual {p0, p1, p2}, Lkotlin/reflect/jvm/internal/impl/h/e;->a(Lkotlin/reflect/jvm/internal/impl/b/m;Lkotlin/reflect/jvm/internal/impl/b/m;)I

    move-result v0

    return v0
.end method
