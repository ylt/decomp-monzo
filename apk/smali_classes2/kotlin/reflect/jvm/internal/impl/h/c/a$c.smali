.class public final Lkotlin/reflect/jvm/internal/impl/h/c/a$c;
.super Lkotlin/reflect/jvm/internal/impl/utils/b$a;
.source "DescriptorUtils.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lkotlin/reflect/jvm/internal/impl/h/c/a;->a(Lkotlin/reflect/jvm/internal/impl/b/b;ZLkotlin/d/a/b;)Lkotlin/reflect/jvm/internal/impl/b/b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/reflect/jvm/internal/impl/utils/b$a",
        "<",
        "Lkotlin/reflect/jvm/internal/impl/b/b;",
        "Lkotlin/reflect/jvm/internal/impl/b/b;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lkotlin/d/b/x$c;

.field final synthetic b:Lkotlin/d/a/b;


# direct methods
.method constructor <init>(Lkotlin/d/b/x$c;Lkotlin/d/a/b;)V
    .locals 0

    .prologue
    .line 243
    iput-object p1, p0, Lkotlin/reflect/jvm/internal/impl/h/c/a$c;->a:Lkotlin/d/b/x$c;

    iput-object p2, p0, Lkotlin/reflect/jvm/internal/impl/h/c/a$c;->b:Lkotlin/d/a/b;

    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/utils/b$a;-><init>()V

    return-void
.end method


# virtual methods
.method public a()Lkotlin/reflect/jvm/internal/impl/b/b;
    .locals 1

    .prologue
    .line 250
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/h/c/a$c;->a:Lkotlin/d/b/x$c;

    iget-object v0, v0, Lkotlin/d/b/x$c;->a:Ljava/lang/Object;

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/b;

    return-object v0
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 243
    check-cast p1, Lkotlin/reflect/jvm/internal/impl/b/b;

    invoke-virtual {p0, p1}, Lkotlin/reflect/jvm/internal/impl/h/c/a$c;->a(Lkotlin/reflect/jvm/internal/impl/b/b;)Z

    move-result v0

    return v0
.end method

.method public a(Lkotlin/reflect/jvm/internal/impl/b/b;)Z
    .locals 1

    .prologue
    const-string v0, "current"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 244
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/h/c/a$c;->a:Lkotlin/d/b/x$c;

    iget-object v0, v0, Lkotlin/d/b/x$c;->a:Ljava/lang/Object;

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/b;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public synthetic b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 243
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/h/c/a$c;->a()Lkotlin/reflect/jvm/internal/impl/b/b;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic b(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 243
    check-cast p1, Lkotlin/reflect/jvm/internal/impl/b/b;

    invoke-virtual {p0, p1}, Lkotlin/reflect/jvm/internal/impl/h/c/a$c;->b(Lkotlin/reflect/jvm/internal/impl/b/b;)V

    return-void
.end method

.method public b(Lkotlin/reflect/jvm/internal/impl/b/b;)V
    .locals 1

    .prologue
    const-string v0, "current"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 246
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/h/c/a$c;->a:Lkotlin/d/b/x$c;

    iget-object v0, v0, Lkotlin/d/b/x$c;->a:Ljava/lang/Object;

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/b;

    if-nez v0, :cond_0

    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/h/c/a$c;->b:Lkotlin/d/a/b;

    invoke-interface {v0, p1}, Lkotlin/d/a/b;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 247
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/h/c/a$c;->a:Lkotlin/d/b/x$c;

    iput-object p1, v0, Lkotlin/d/b/x$c;->a:Ljava/lang/Object;

    .line 249
    :cond_0
    return-void
.end method
