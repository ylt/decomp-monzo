.class final Lkotlin/reflect/jvm/internal/impl/h/c/a$a;
.super Lkotlin/d/b/m;
.source "DescriptorUtils.kt"

# interfaces
.implements Lkotlin/d/a/m;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lkotlin/reflect/jvm/internal/impl/h/c/a;->c(Lkotlin/reflect/jvm/internal/impl/b/e;)Ljava/util/Collection;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/d/b/m;",
        "Lkotlin/d/a/m",
        "<",
        "Lkotlin/reflect/jvm/internal/impl/h/e/h;",
        "Ljava/lang/Boolean;",
        "Lkotlin/n;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lkotlin/reflect/jvm/internal/impl/b/e;

.field final synthetic b:Ljava/util/LinkedHashSet;


# direct methods
.method constructor <init>(Lkotlin/reflect/jvm/internal/impl/b/e;Ljava/util/LinkedHashSet;)V
    .locals 1

    iput-object p1, p0, Lkotlin/reflect/jvm/internal/impl/h/c/a$a;->a:Lkotlin/reflect/jvm/internal/impl/b/e;

    iput-object p2, p0, Lkotlin/reflect/jvm/internal/impl/h/c/a$a;->b:Ljava/util/LinkedHashSet;

    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lkotlin/d/b/m;-><init>(I)V

    return-void
.end method


# virtual methods
.method public synthetic a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    check-cast p1, Lkotlin/reflect/jvm/internal/impl/h/e/h;

    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-virtual {p0, p1, v0}, Lkotlin/reflect/jvm/internal/impl/h/c/a$a;->a(Lkotlin/reflect/jvm/internal/impl/h/e/h;Z)V

    sget-object v0, Lkotlin/n;->a:Lkotlin/n;

    return-object v0
.end method

.method public final a(Lkotlin/reflect/jvm/internal/impl/h/e/h;Z)V
    .locals 4

    .prologue
    const/4 v2, 0x0

    const-string v0, "scope"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 374
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/h/e/d;->f:Lkotlin/reflect/jvm/internal/impl/h/e/d;

    const/4 v1, 0x2

    invoke-static {p1, v0, v2, v1, v2}, Lkotlin/reflect/jvm/internal/impl/h/e/j$a;->a(Lkotlin/reflect/jvm/internal/impl/h/e/j;Lkotlin/reflect/jvm/internal/impl/h/e/d;Lkotlin/d/a/b;ILjava/lang/Object;)Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/m;

    .line 375
    instance-of v1, v0, Lkotlin/reflect/jvm/internal/impl/b/e;

    if-eqz v1, :cond_0

    move-object v1, v0

    .line 377
    check-cast v1, Lkotlin/reflect/jvm/internal/impl/b/e;

    iget-object v3, p0, Lkotlin/reflect/jvm/internal/impl/h/c/a$a;->a:Lkotlin/reflect/jvm/internal/impl/b/e;

    invoke-static {v1, v3}, Lkotlin/reflect/jvm/internal/impl/h/c;->a(Lkotlin/reflect/jvm/internal/impl/b/e;Lkotlin/reflect/jvm/internal/impl/b/e;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 378
    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/h/c/a$a;->b:Ljava/util/LinkedHashSet;

    invoke-virtual {v1, v0}, Ljava/util/LinkedHashSet;->add(Ljava/lang/Object;)Z

    .line 381
    :cond_1
    if-eqz p2, :cond_0

    move-object v1, p0

    .line 382
    check-cast v1, Lkotlin/reflect/jvm/internal/impl/h/c/a$a;

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/e;

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/b/e;->B()Lkotlin/reflect/jvm/internal/impl/h/e/h;

    move-result-object v0

    const-string v3, "descriptor.unsubstitutedInnerClassesScope"

    invoke-static {v0, v3}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v1, v0, p2}, Lkotlin/reflect/jvm/internal/impl/h/c/a$a;->a(Lkotlin/reflect/jvm/internal/impl/h/e/h;Z)V

    goto :goto_0

    .line 385
    :cond_2
    return-void
.end method
