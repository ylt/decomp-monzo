.class public final Lkotlin/reflect/jvm/internal/impl/h/e/d;
.super Ljava/lang/Object;
.source "MemberScope.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lkotlin/reflect/jvm/internal/impl/h/e/d$a;
    }
.end annotation


# static fields
.field public static final a:Lkotlin/reflect/jvm/internal/impl/h/e/d;

.field public static final b:Lkotlin/reflect/jvm/internal/impl/h/e/d;

.field public static final c:Lkotlin/reflect/jvm/internal/impl/h/e/d;

.field public static final d:Lkotlin/reflect/jvm/internal/impl/h/e/d;

.field public static final e:Lkotlin/reflect/jvm/internal/impl/h/e/d;

.field public static final f:Lkotlin/reflect/jvm/internal/impl/h/e/d;

.field public static final g:Lkotlin/reflect/jvm/internal/impl/h/e/d;

.field public static final h:Lkotlin/reflect/jvm/internal/impl/h/e/d;

.field public static final i:Lkotlin/reflect/jvm/internal/impl/h/e/d;

.field public static final j:Lkotlin/reflect/jvm/internal/impl/h/e/d;

.field public static final k:Lkotlin/reflect/jvm/internal/impl/h/e/d$a;

.field private static n:I

.field private static final o:I

.field private static final p:I

.field private static final q:I

.field private static final r:I

.field private static final s:I

.field private static final t:I

.field private static final u:I

.field private static final v:I

.field private static final w:I

.field private static final x:I

.field private static final y:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/h/e/d$a$a;",
            ">;"
        }
    .end annotation
.end field

.field private static final z:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/h/e/d$a$a;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final l:I

.field private final m:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/h/e/c;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v7, 0x1

    const/4 v5, 0x0

    const/4 v2, 0x2

    const/4 v6, 0x0

    new-instance v0, Lkotlin/reflect/jvm/internal/impl/h/e/d$a;

    invoke-direct {v0, v6}, Lkotlin/reflect/jvm/internal/impl/h/e/d$a;-><init>(Lkotlin/d/b/i;)V

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/h/e/d;->k:Lkotlin/reflect/jvm/internal/impl/h/e/d$a;

    .line 127
    sput v7, Lkotlin/reflect/jvm/internal/impl/h/e/d;->n:I

    .line 130
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/h/e/d;->k:Lkotlin/reflect/jvm/internal/impl/h/e/d$a;

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/h/e/d$a;->c(Lkotlin/reflect/jvm/internal/impl/h/e/d$a;)I

    move-result v0

    sput v0, Lkotlin/reflect/jvm/internal/impl/h/e/d;->o:I

    .line 131
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/h/e/d;->k:Lkotlin/reflect/jvm/internal/impl/h/e/d$a;

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/h/e/d$a;->c(Lkotlin/reflect/jvm/internal/impl/h/e/d$a;)I

    move-result v0

    sput v0, Lkotlin/reflect/jvm/internal/impl/h/e/d;->p:I

    .line 132
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/h/e/d;->k:Lkotlin/reflect/jvm/internal/impl/h/e/d$a;

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/h/e/d$a;->c(Lkotlin/reflect/jvm/internal/impl/h/e/d$a;)I

    move-result v0

    sput v0, Lkotlin/reflect/jvm/internal/impl/h/e/d;->q:I

    .line 133
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/h/e/d;->k:Lkotlin/reflect/jvm/internal/impl/h/e/d$a;

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/h/e/d$a;->c(Lkotlin/reflect/jvm/internal/impl/h/e/d$a;)I

    move-result v0

    sput v0, Lkotlin/reflect/jvm/internal/impl/h/e/d;->r:I

    .line 134
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/h/e/d;->k:Lkotlin/reflect/jvm/internal/impl/h/e/d$a;

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/h/e/d$a;->c(Lkotlin/reflect/jvm/internal/impl/h/e/d$a;)I

    move-result v0

    sput v0, Lkotlin/reflect/jvm/internal/impl/h/e/d;->s:I

    .line 135
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/h/e/d;->k:Lkotlin/reflect/jvm/internal/impl/h/e/d$a;

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/h/e/d$a;->c(Lkotlin/reflect/jvm/internal/impl/h/e/d$a;)I

    move-result v0

    sput v0, Lkotlin/reflect/jvm/internal/impl/h/e/d;->t:I

    .line 137
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/h/e/d;->k:Lkotlin/reflect/jvm/internal/impl/h/e/d$a;

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/h/e/d$a;->c(Lkotlin/reflect/jvm/internal/impl/h/e/d$a;)I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    sput v0, Lkotlin/reflect/jvm/internal/impl/h/e/d;->u:I

    .line 138
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/h/e/d;->k:Lkotlin/reflect/jvm/internal/impl/h/e/d$a;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/h/e/d$a;->a()I

    move-result v0

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/h/e/d;->k:Lkotlin/reflect/jvm/internal/impl/h/e/d$a;

    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/impl/h/e/d$a;->b()I

    move-result v1

    or-int/2addr v0, v1

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/h/e/d;->k:Lkotlin/reflect/jvm/internal/impl/h/e/d$a;

    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/impl/h/e/d$a;->c()I

    move-result v1

    or-int/2addr v0, v1

    sput v0, Lkotlin/reflect/jvm/internal/impl/h/e/d;->v:I

    .line 139
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/h/e/d;->k:Lkotlin/reflect/jvm/internal/impl/h/e/d$a;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/h/e/d$a;->b()I

    move-result v0

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/h/e/d;->k:Lkotlin/reflect/jvm/internal/impl/h/e/d$a;

    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/impl/h/e/d$a;->e()I

    move-result v1

    or-int/2addr v0, v1

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/h/e/d;->k:Lkotlin/reflect/jvm/internal/impl/h/e/d$a;

    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/impl/h/e/d$a;->f()I

    move-result v1

    or-int/2addr v0, v1

    sput v0, Lkotlin/reflect/jvm/internal/impl/h/e/d;->w:I

    .line 140
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/h/e/d;->k:Lkotlin/reflect/jvm/internal/impl/h/e/d$a;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/h/e/d$a;->e()I

    move-result v0

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/h/e/d;->k:Lkotlin/reflect/jvm/internal/impl/h/e/d$a;

    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/impl/h/e/d$a;->f()I

    move-result v1

    or-int/2addr v0, v1

    sput v0, Lkotlin/reflect/jvm/internal/impl/h/e/d;->x:I

    .line 142
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/h/e/d;

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/h/e/d;->k:Lkotlin/reflect/jvm/internal/impl/h/e/d$a;

    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/impl/h/e/d$a;->g()I

    move-result v1

    invoke-direct {v0, v1, v6, v2, v6}, Lkotlin/reflect/jvm/internal/impl/h/e/d;-><init>(ILjava/util/List;ILkotlin/d/b/i;)V

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/h/e/d;->a:Lkotlin/reflect/jvm/internal/impl/h/e/d;

    .line 143
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/h/e/d;

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/h/e/d;->k:Lkotlin/reflect/jvm/internal/impl/h/e/d$a;

    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/impl/h/e/d$a;->j()I

    move-result v1

    invoke-direct {v0, v1, v6, v2, v6}, Lkotlin/reflect/jvm/internal/impl/h/e/d;-><init>(ILjava/util/List;ILkotlin/d/b/i;)V

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/h/e/d;->b:Lkotlin/reflect/jvm/internal/impl/h/e/d;

    .line 144
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/h/e/d;

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/h/e/d;->k:Lkotlin/reflect/jvm/internal/impl/h/e/d$a;

    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/impl/h/e/d$a;->a()I

    move-result v1

    invoke-direct {v0, v1, v6, v2, v6}, Lkotlin/reflect/jvm/internal/impl/h/e/d;-><init>(ILjava/util/List;ILkotlin/d/b/i;)V

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/h/e/d;->c:Lkotlin/reflect/jvm/internal/impl/h/e/d;

    .line 145
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/h/e/d;

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/h/e/d;->k:Lkotlin/reflect/jvm/internal/impl/h/e/d$a;

    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/impl/h/e/d$a;->b()I

    move-result v1

    invoke-direct {v0, v1, v6, v2, v6}, Lkotlin/reflect/jvm/internal/impl/h/e/d;-><init>(ILjava/util/List;ILkotlin/d/b/i;)V

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/h/e/d;->d:Lkotlin/reflect/jvm/internal/impl/h/e/d;

    .line 146
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/h/e/d;

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/h/e/d;->k:Lkotlin/reflect/jvm/internal/impl/h/e/d$a;

    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/impl/h/e/d$a;->c()I

    move-result v1

    invoke-direct {v0, v1, v6, v2, v6}, Lkotlin/reflect/jvm/internal/impl/h/e/d;-><init>(ILjava/util/List;ILkotlin/d/b/i;)V

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/h/e/d;->e:Lkotlin/reflect/jvm/internal/impl/h/e/d;

    .line 147
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/h/e/d;

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/h/e/d;->k:Lkotlin/reflect/jvm/internal/impl/h/e/d$a;

    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/impl/h/e/d$a;->h()I

    move-result v1

    invoke-direct {v0, v1, v6, v2, v6}, Lkotlin/reflect/jvm/internal/impl/h/e/d;-><init>(ILjava/util/List;ILkotlin/d/b/i;)V

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/h/e/d;->f:Lkotlin/reflect/jvm/internal/impl/h/e/d;

    .line 148
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/h/e/d;

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/h/e/d;->k:Lkotlin/reflect/jvm/internal/impl/h/e/d$a;

    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/impl/h/e/d$a;->d()I

    move-result v1

    invoke-direct {v0, v1, v6, v2, v6}, Lkotlin/reflect/jvm/internal/impl/h/e/d;-><init>(ILjava/util/List;ILkotlin/d/b/i;)V

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/h/e/d;->g:Lkotlin/reflect/jvm/internal/impl/h/e/d;

    .line 149
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/h/e/d;

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/h/e/d;->k:Lkotlin/reflect/jvm/internal/impl/h/e/d$a;

    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/impl/h/e/d$a;->e()I

    move-result v1

    invoke-direct {v0, v1, v6, v2, v6}, Lkotlin/reflect/jvm/internal/impl/h/e/d;-><init>(ILjava/util/List;ILkotlin/d/b/i;)V

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/h/e/d;->h:Lkotlin/reflect/jvm/internal/impl/h/e/d;

    .line 150
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/h/e/d;

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/h/e/d;->k:Lkotlin/reflect/jvm/internal/impl/h/e/d$a;

    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/impl/h/e/d$a;->f()I

    move-result v1

    invoke-direct {v0, v1, v6, v2, v6}, Lkotlin/reflect/jvm/internal/impl/h/e/d;-><init>(ILjava/util/List;ILkotlin/d/b/i;)V

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/h/e/d;->i:Lkotlin/reflect/jvm/internal/impl/h/e/d;

    .line 151
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/h/e/d;

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/h/e/d;->k:Lkotlin/reflect/jvm/internal/impl/h/e/d$a;

    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/impl/h/e/d$a;->i()I

    move-result v1

    invoke-direct {v0, v1, v6, v2, v6}, Lkotlin/reflect/jvm/internal/impl/h/e/d;-><init>(ILjava/util/List;ILkotlin/d/b/i;)V

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/h/e/d;->j:Lkotlin/reflect/jvm/internal/impl/h/e/d;

    .line 155
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/h/e/d;->k:Lkotlin/reflect/jvm/internal/impl/h/e/d$a;

    .line 241
    const-class v0, Lkotlin/reflect/jvm/internal/impl/h/e/d;

    invoke-virtual {v0}, Ljava/lang/Class;->getFields()[Ljava/lang/reflect/Field;

    move-result-object v0

    check-cast v0, [Ljava/lang/Object;

    .line 242
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    check-cast v1, Ljava/util/Collection;

    move v4, v5

    .line 243
    :goto_0
    array-length v2, v0

    if-ge v4, v2, :cond_1

    aget-object v3, v0, v4

    move-object v2, v3

    check-cast v2, Ljava/lang/reflect/Field;

    .line 241
    invoke-virtual {v2}, Ljava/lang/reflect/Field;->getModifiers()I

    move-result v2

    invoke-static {v2}, Ljava/lang/reflect/Modifier;->isStatic(I)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1, v3}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    :cond_0
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    goto :goto_0

    .line 244
    :cond_1
    check-cast v1, Ljava/util/List;

    .line 241
    check-cast v1, Ljava/lang/Iterable;

    .line 156
    nop

    .line 245
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    check-cast v0, Ljava/util/Collection;

    .line 254
    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_2
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_5

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 253
    check-cast v1, Ljava/lang/reflect/Field;

    .line 157
    invoke-virtual {v1, v6}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    instance-of v3, v2, Lkotlin/reflect/jvm/internal/impl/h/e/d;

    if-nez v3, :cond_3

    move-object v2, v6

    :cond_3
    check-cast v2, Lkotlin/reflect/jvm/internal/impl/h/e/d;

    .line 158
    if-eqz v2, :cond_4

    new-instance v3, Lkotlin/reflect/jvm/internal/impl/h/e/d$a$a;

    iget v2, v2, Lkotlin/reflect/jvm/internal/impl/h/e/d;->l:I

    invoke-virtual {v1}, Ljava/lang/reflect/Field;->getName()Ljava/lang/String;

    move-result-object v1

    const-string v8, "field.name"

    invoke-static {v1, v8}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v3, v2, v1}, Lkotlin/reflect/jvm/internal/impl/h/e/d$a$a;-><init>(ILjava/lang/String;)V

    move-object v1, v3

    :goto_2
    if-eqz v1, :cond_2

    .line 253
    invoke-interface {v0, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_4
    move-object v1, v6

    .line 158
    goto :goto_2

    .line 256
    :cond_5
    check-cast v0, Ljava/util/List;

    check-cast v0, Ljava/util/Collection;

    .line 160
    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/utils/a;->a(Ljava/util/Collection;)Ljava/util/List;

    move-result-object v0

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/h/e/d;->y:Ljava/util/List;

    .line 162
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/h/e/d;->k:Lkotlin/reflect/jvm/internal/impl/h/e/d$a;

    .line 257
    const-class v0, Lkotlin/reflect/jvm/internal/impl/h/e/d;

    invoke-virtual {v0}, Ljava/lang/Class;->getFields()[Ljava/lang/reflect/Field;

    move-result-object v0

    check-cast v0, [Ljava/lang/Object;

    .line 258
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    check-cast v1, Ljava/util/Collection;

    move v4, v5

    .line 259
    :goto_3
    array-length v2, v0

    if-ge v4, v2, :cond_7

    aget-object v3, v0, v4

    move-object v2, v3

    check-cast v2, Ljava/lang/reflect/Field;

    .line 257
    invoke-virtual {v2}, Ljava/lang/reflect/Field;->getModifiers()I

    move-result v2

    invoke-static {v2}, Ljava/lang/reflect/Modifier;->isStatic(I)Z

    move-result v2

    if-eqz v2, :cond_6

    invoke-interface {v1, v3}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    :cond_6
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    goto :goto_3

    .line 260
    :cond_7
    check-cast v1, Ljava/util/List;

    .line 257
    check-cast v1, Ljava/lang/Iterable;

    .line 163
    nop

    .line 261
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    check-cast v0, Ljava/util/Collection;

    .line 262
    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_8
    :goto_4
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_9

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    move-object v1, v2

    check-cast v1, Ljava/lang/reflect/Field;

    .line 163
    invoke-virtual {v1}, Ljava/lang/reflect/Field;->getType()Ljava/lang/Class;

    move-result-object v1

    sget-object v4, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    invoke-static {v1, v4}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_8

    invoke-interface {v0, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_4

    .line 263
    :cond_9
    check-cast v0, Ljava/util/List;

    check-cast v0, Ljava/lang/Iterable;

    .line 164
    nop

    .line 264
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    check-cast v1, Ljava/util/Collection;

    .line 273
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_a
    :goto_5
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_e

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .line 272
    check-cast v0, Ljava/lang/reflect/Field;

    .line 165
    invoke-virtual {v0, v6}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    if-nez v2, :cond_b

    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type kotlin.Int"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_b
    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Number;->intValue()I

    move-result v4

    .line 166
    neg-int v2, v4

    and-int/2addr v2, v4

    if-ne v4, v2, :cond_c

    move v2, v7

    .line 167
    :goto_6
    if-eqz v2, :cond_d

    new-instance v2, Lkotlin/reflect/jvm/internal/impl/h/e/d$a$a;

    invoke-virtual {v0}, Ljava/lang/reflect/Field;->getName()Ljava/lang/String;

    move-result-object v0

    const-string v8, "field.name"

    invoke-static {v0, v8}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v2, v4, v0}, Lkotlin/reflect/jvm/internal/impl/h/e/d$a$a;-><init>(ILjava/lang/String;)V

    move-object v0, v2

    :goto_7
    if-eqz v0, :cond_a

    .line 272
    invoke-interface {v1, v0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_5

    :cond_c
    move v2, v5

    .line 166
    goto :goto_6

    :cond_d
    move-object v0, v6

    .line 167
    goto :goto_7

    .line 275
    :cond_e
    check-cast v1, Ljava/util/List;

    check-cast v1, Ljava/util/Collection;

    .line 169
    invoke-static {v1}, Lkotlin/reflect/jvm/internal/impl/utils/a;->a(Ljava/util/Collection;)Ljava/util/List;

    move-result-object v0

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/h/e/d;->z:Ljava/util/List;

    return-void
.end method

.method public constructor <init>(ILjava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/List",
            "<+",
            "Lkotlin/reflect/jvm/internal/impl/h/e/c;",
            ">;)V"
        }
    .end annotation

    .prologue
    const-string v0, "excludes"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 67
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lkotlin/reflect/jvm/internal/impl/h/e/d;->m:Ljava/util/List;

    .line 74
    new-instance v1, Lkotlin/d/b/x$b;

    invoke-direct {v1}, Lkotlin/d/b/x$b;-><init>()V

    iput p1, v1, Lkotlin/d/b/x$b;->a:I

    .line 75
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/h/e/d;->m:Ljava/util/List;

    check-cast v0, Ljava/lang/Iterable;

    .line 239
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/h/e/c;

    .line 75
    iget v3, v1, Lkotlin/d/b/x$b;->a:I

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/h/e/c;->a()I

    move-result v0

    xor-int/lit8 v0, v0, -0x1

    and-int/2addr v0, v3

    iput v0, v1, Lkotlin/d/b/x$b;->a:I

    goto :goto_0

    .line 76
    :cond_0
    iget v0, v1, Lkotlin/d/b/x$b;->a:I

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/h/e/d;->l:I

    return-void
.end method

.method public synthetic constructor <init>(ILjava/util/List;ILkotlin/d/b/i;)V
    .locals 1

    .prologue
    and-int/lit8 v0, p3, 0x2

    if-eqz v0, :cond_0

    .line 69
    invoke-static {}, Lkotlin/a/m;->a()Ljava/util/List;

    move-result-object p2

    :cond_0
    invoke-direct {p0, p1, p2}, Lkotlin/reflect/jvm/internal/impl/h/e/d;-><init>(ILjava/util/List;)V

    return-void
.end method

.method public static final synthetic c()I
    .locals 1

    .prologue
    .line 67
    sget v0, Lkotlin/reflect/jvm/internal/impl/h/e/d;->n:I

    return v0
.end method

.method public static final synthetic c(I)V
    .locals 0

    .prologue
    .line 67
    sput p0, Lkotlin/reflect/jvm/internal/impl/h/e/d;->n:I

    return-void
.end method

.method public static final synthetic d()I
    .locals 1

    .prologue
    .line 67
    sget v0, Lkotlin/reflect/jvm/internal/impl/h/e/d;->o:I

    return v0
.end method

.method public static final synthetic e()I
    .locals 1

    .prologue
    .line 67
    sget v0, Lkotlin/reflect/jvm/internal/impl/h/e/d;->p:I

    return v0
.end method

.method public static final synthetic f()I
    .locals 1

    .prologue
    .line 67
    sget v0, Lkotlin/reflect/jvm/internal/impl/h/e/d;->q:I

    return v0
.end method

.method public static final synthetic g()I
    .locals 1

    .prologue
    .line 67
    sget v0, Lkotlin/reflect/jvm/internal/impl/h/e/d;->r:I

    return v0
.end method

.method public static final synthetic h()I
    .locals 1

    .prologue
    .line 67
    sget v0, Lkotlin/reflect/jvm/internal/impl/h/e/d;->s:I

    return v0
.end method

.method public static final synthetic i()I
    .locals 1

    .prologue
    .line 67
    sget v0, Lkotlin/reflect/jvm/internal/impl/h/e/d;->t:I

    return v0
.end method

.method public static final synthetic j()I
    .locals 1

    .prologue
    .line 67
    sget v0, Lkotlin/reflect/jvm/internal/impl/h/e/d;->u:I

    return v0
.end method

.method public static final synthetic k()I
    .locals 1

    .prologue
    .line 67
    sget v0, Lkotlin/reflect/jvm/internal/impl/h/e/d;->v:I

    return v0
.end method

.method public static final synthetic l()I
    .locals 1

    .prologue
    .line 67
    sget v0, Lkotlin/reflect/jvm/internal/impl/h/e/d;->w:I

    return v0
.end method

.method public static final synthetic m()I
    .locals 1

    .prologue
    .line 67
    sget v0, Lkotlin/reflect/jvm/internal/impl/h/e/d;->x:I

    return v0
.end method

.method public static final synthetic n()Ljava/util/List;
    .locals 1

    .prologue
    .line 67
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/h/e/d;->y:Ljava/util/List;

    return-object v0
.end method

.method public static final synthetic o()Ljava/util/List;
    .locals 1

    .prologue
    .line 67
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/h/e/d;->z:Ljava/util/List;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 71
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/h/e/d;->l:I

    return v0
.end method

.method public final a(I)Z
    .locals 1

    .prologue
    .line 83
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/h/e/d;->l:I

    and-int/2addr v0, p1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/h/e/c;",
            ">;"
        }
    .end annotation

    .prologue
    .line 69
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/h/e/d;->m:Ljava/util/List;

    return-object v0
.end method

.method public final b(I)Lkotlin/reflect/jvm/internal/impl/h/e/d;
    .locals 3

    .prologue
    .line 98
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/h/e/d;->l:I

    and-int v1, v0, p1

    .line 99
    if-nez v1, :cond_0

    const/4 v0, 0x0

    .line 100
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/h/e/d;

    iget-object v2, p0, Lkotlin/reflect/jvm/internal/impl/h/e/d;->m:Ljava/util/List;

    invoke-direct {v0, v1, v2}, Lkotlin/reflect/jvm/internal/impl/h/e/d;-><init>(ILjava/util/List;)V

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 9

    .prologue
    const/4 v4, 0x0

    const/4 v2, 0x0

    .line 106
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/h/e/d;->k:Lkotlin/reflect/jvm/internal/impl/h/e/d$a;

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/h/e/d$a;->a(Lkotlin/reflect/jvm/internal/impl/h/e/d$a;)Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 225
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    move-object v0, v1

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/h/e/d$a$a;

    .line 106
    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/h/e/d$a$a;->a()I

    move-result v0

    iget v5, p0, Lkotlin/reflect/jvm/internal/impl/h/e/d;->l:I

    if-ne v0, v5, :cond_1

    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_0

    move-object v0, v1

    .line 226
    :goto_1
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/h/e/d$a$a;

    if-eqz v0, :cond_3

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/h/e/d$a$a;->b()Ljava/lang/String;

    move-result-object v0

    .line 107
    :goto_2
    if-eqz v0, :cond_4

    .line 111
    :goto_3
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "DescriptorKindFilter("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/h/e/d;->m:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_1
    move v0, v4

    .line 106
    goto :goto_0

    :cond_2
    move-object v0, v2

    .line 226
    goto :goto_1

    :cond_3
    move-object v0, v2

    goto :goto_2

    .line 107
    :cond_4
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/h/e/d;->k:Lkotlin/reflect/jvm/internal/impl/h/e/d$a;

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/h/e/d$a;->b(Lkotlin/reflect/jvm/internal/impl/h/e/d$a;)Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 108
    nop

    .line 227
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    check-cast v1, Ljava/util/Collection;

    .line 236
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_5
    :goto_4
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .line 235
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/h/e/d$a$a;

    .line 108
    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/h/e/d$a$a;->a()I

    move-result v5

    invoke-virtual {p0, v5}, Lkotlin/reflect/jvm/internal/impl/h/e/d;->a(I)Z

    move-result v5

    if-eqz v5, :cond_6

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/h/e/d$a$a;->b()Ljava/lang/String;

    move-result-object v0

    :goto_5
    if-eqz v0, :cond_5

    .line 235
    invoke-interface {v1, v0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_4

    :cond_6
    move-object v0, v2

    .line 108
    goto :goto_5

    .line 238
    :cond_7
    check-cast v1, Ljava/util/List;

    move-object v0, v1

    check-cast v0, Ljava/lang/Iterable;

    const-string v1, " | "

    check-cast v1, Ljava/lang/CharSequence;

    const/16 v7, 0x3e

    move-object v3, v2

    move-object v5, v2

    move-object v6, v2

    move-object v8, v2

    .line 109
    invoke-static/range {v0 .. v8}, Lkotlin/a/m;->a(Ljava/lang/Iterable;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;ILjava/lang/CharSequence;Lkotlin/d/a/b;ILjava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_3
.end method
