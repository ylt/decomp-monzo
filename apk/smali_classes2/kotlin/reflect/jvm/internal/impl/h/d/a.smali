.class public final Lkotlin/reflect/jvm/internal/impl/h/d/a;
.super Ljava/lang/Object;
.source "JavaDescriptorResolver.kt"


# instance fields
.field private final a:Lkotlin/reflect/jvm/internal/impl/d/a/c/e;

.field private final b:Lkotlin/reflect/jvm/internal/impl/d/a/a/h;


# direct methods
.method public constructor <init>(Lkotlin/reflect/jvm/internal/impl/d/a/c/e;Lkotlin/reflect/jvm/internal/impl/d/a/a/h;)V
    .locals 1

    .prologue
    const-string v0, "packageFragmentProvider"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "javaResolverCache"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lkotlin/reflect/jvm/internal/impl/h/d/a;->a:Lkotlin/reflect/jvm/internal/impl/d/a/c/e;

    iput-object p2, p0, Lkotlin/reflect/jvm/internal/impl/h/d/a;->b:Lkotlin/reflect/jvm/internal/impl/d/a/a/h;

    return-void
.end method


# virtual methods
.method public final a(Lkotlin/reflect/jvm/internal/impl/d/a/f/g;)Lkotlin/reflect/jvm/internal/impl/b/e;
    .locals 4

    .prologue
    const/4 v1, 0x0

    const-string v0, "javaClass"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 31
    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/d/a/f/g;->c()Lkotlin/reflect/jvm/internal/impl/e/b;

    move-result-object v2

    .line 32
    if-eqz v2, :cond_1

    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/d/a/f/g;->j()Lkotlin/reflect/jvm/internal/impl/d/a/f/aa;

    move-result-object v0

    sget-object v3, Lkotlin/reflect/jvm/internal/impl/d/a/f/aa;->a:Lkotlin/reflect/jvm/internal/impl/d/a/f/aa;

    invoke-static {v0, v3}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 33
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/h/d/a;->b:Lkotlin/reflect/jvm/internal/impl/d/a/a/h;

    invoke-interface {v0, v2}, Lkotlin/reflect/jvm/internal/impl/d/a/a/h;->a(Lkotlin/reflect/jvm/internal/impl/e/b;)Lkotlin/reflect/jvm/internal/impl/b/e;

    move-result-object v1

    .line 43
    :cond_0
    :goto_0
    return-object v1

    .line 36
    :cond_1
    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/d/a/f/g;->f()Lkotlin/reflect/jvm/internal/impl/d/a/f/g;

    move-result-object v0

    if-eqz v0, :cond_5

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/d/a/f/g;

    .line 37
    invoke-virtual {p0, v0}, Lkotlin/reflect/jvm/internal/impl/h/d/a;->a(Lkotlin/reflect/jvm/internal/impl/d/a/f/g;)Lkotlin/reflect/jvm/internal/impl/b/e;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/b/e;->B()Lkotlin/reflect/jvm/internal/impl/h/e/h;

    move-result-object v0

    move-object v2, v0

    .line 38
    :goto_1
    if-eqz v2, :cond_4

    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/d/a/f/g;->r()Lkotlin/reflect/jvm/internal/impl/e/f;

    move-result-object v3

    sget-object v0, Lkotlin/reflect/jvm/internal/impl/c/a/d;->s:Lkotlin/reflect/jvm/internal/impl/c/a/d;

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/c/a/b;

    invoke-interface {v2, v3, v0}, Lkotlin/reflect/jvm/internal/impl/h/e/h;->c(Lkotlin/reflect/jvm/internal/impl/e/f;Lkotlin/reflect/jvm/internal/impl/c/a/b;)Lkotlin/reflect/jvm/internal/impl/b/h;

    move-result-object v0

    :goto_2
    instance-of v2, v0, Lkotlin/reflect/jvm/internal/impl/b/e;

    if-nez v2, :cond_2

    move-object v0, v1

    :cond_2
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/e;

    move-object v1, v0

    goto :goto_0

    :cond_3
    move-object v2, v1

    .line 37
    goto :goto_1

    :cond_4
    move-object v0, v1

    .line 38
    goto :goto_2

    .line 41
    :cond_5
    if-eqz v2, :cond_0

    .line 43
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/h/d/a;->a:Lkotlin/reflect/jvm/internal/impl/d/a/c/e;

    invoke-virtual {v2}, Lkotlin/reflect/jvm/internal/impl/e/b;->d()Lkotlin/reflect/jvm/internal/impl/e/b;

    move-result-object v2

    const-string v3, "fqName.parent()"

    invoke-static {v2, v3}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Lkotlin/reflect/jvm/internal/impl/d/a/c/e;->a(Lkotlin/reflect/jvm/internal/impl/e/b;)Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lkotlin/a/m;->f(Ljava/util/List;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/h;

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/h;->a(Lkotlin/reflect/jvm/internal/impl/d/a/f/g;)Lkotlin/reflect/jvm/internal/impl/b/e;

    move-result-object v1

    goto :goto_0
.end method

.method public final a()Lkotlin/reflect/jvm/internal/impl/d/a/c/e;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/h/d/a;->a:Lkotlin/reflect/jvm/internal/impl/d/a/c/e;

    return-object v0
.end method
