.class public final Lkotlin/reflect/jvm/internal/impl/h/b/l;
.super Lkotlin/reflect/jvm/internal/impl/h/b/m;
.source "constantValues.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/reflect/jvm/internal/impl/h/b/m",
        "<",
        "Ljava/lang/Integer;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Lkotlin/reflect/jvm/internal/impl/k/v;


# direct methods
.method public constructor <init>(ILkotlin/reflect/jvm/internal/impl/a/l;)V
    .locals 1

    .prologue
    const-string v0, "builtIns"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 193
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 196
    invoke-direct {p0, v0}, Lkotlin/reflect/jvm/internal/impl/h/b/m;-><init>(Ljava/lang/Object;)V

    .line 198
    invoke-virtual {p2}, Lkotlin/reflect/jvm/internal/impl/a/l;->C()Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/h/b/l;->a:Lkotlin/reflect/jvm/internal/impl/k/v;

    return-void
.end method


# virtual methods
.method public synthetic a()Lkotlin/reflect/jvm/internal/impl/k/r;
    .locals 1

    .prologue
    .line 193
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/h/b/l;->b()Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/r;

    return-object v0
.end method

.method public b()Lkotlin/reflect/jvm/internal/impl/k/v;
    .locals 1

    .prologue
    .line 198
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/h/b/l;->a:Lkotlin/reflect/jvm/internal/impl/k/v;

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 203
    move-object v0, p0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/h/b/l;

    if-ne v0, p1, :cond_0

    .line 208
    :goto_0
    return v1

    .line 204
    :cond_0
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    :goto_1
    invoke-static {v3, v0}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_2

    move v1, v2

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    .line 206
    :cond_2
    if-nez p1, :cond_3

    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type org.jetbrains.kotlin.resolve.constants.IntValue"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_3
    check-cast p1, Lkotlin/reflect/jvm/internal/impl/h/b/l;

    .line 208
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/h/b/l;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Number;

    invoke-virtual {v0}, Ljava/lang/Number;->intValue()I

    move-result v3

    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/h/b/l;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Number;

    invoke-virtual {v0}, Ljava/lang/Number;->intValue()I

    move-result v0

    if-ne v3, v0, :cond_4

    move v0, v1

    :goto_2
    move v1, v0

    goto :goto_0

    :cond_4
    move v0, v2

    goto :goto_2
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 211
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/h/b/l;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Number;

    invoke-virtual {v0}, Ljava/lang/Number;->intValue()I

    move-result v0

    return v0
.end method
