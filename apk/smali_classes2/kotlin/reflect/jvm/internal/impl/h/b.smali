.class public Lkotlin/reflect/jvm/internal/impl/h/b;
.super Ljava/lang/Object;
.source "DescriptorFactory.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lkotlin/reflect/jvm/internal/impl/h/b$a;
    }
.end annotation


# direct methods
.method public static a(Lkotlin/reflect/jvm/internal/impl/b/a;Lkotlin/reflect/jvm/internal/impl/k/r;)Lkotlin/reflect/jvm/internal/impl/b/aj;
    .locals 5

    .prologue
    if-nez p0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "owner"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "kotlin/reflect/jvm/internal/impl/resolve/DescriptorFactory"

    aput-object v4, v2, v3

    const/4 v3, 0x2

    const-string v4, "createExtensionReceiverParameterForCallable"

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 160
    :cond_0
    if-nez p1, :cond_1

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_1
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/b/b/ab;

    new-instance v1, Lkotlin/reflect/jvm/internal/impl/h/e/a/b;

    invoke-direct {v1, p0, p1}, Lkotlin/reflect/jvm/internal/impl/h/e/a/b;-><init>(Lkotlin/reflect/jvm/internal/impl/b/a;Lkotlin/reflect/jvm/internal/impl/k/r;)V

    invoke-direct {v0, p0, v1}, Lkotlin/reflect/jvm/internal/impl/b/b/ab;-><init>(Lkotlin/reflect/jvm/internal/impl/b/m;Lkotlin/reflect/jvm/internal/impl/h/e/a/e;)V

    goto :goto_0
.end method

.method public static a(Lkotlin/reflect/jvm/internal/impl/b/e;)Lkotlin/reflect/jvm/internal/impl/b/ak;
    .locals 11

    .prologue
    const/4 v1, 0x0

    const/4 v10, 0x2

    const/4 v9, 0x1

    const/4 v8, 0x0

    if-nez p0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "enumClass"

    aput-object v3, v2, v8

    const-string v3, "kotlin/reflect/jvm/internal/impl/resolve/DescriptorFactory"

    aput-object v3, v2, v9

    const-string v3, "createEnumValuesMethod"

    aput-object v3, v2, v10

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 128
    :cond_0
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/b/a/h;->a:Lkotlin/reflect/jvm/internal/impl/b/a/h$a;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/b/a/h$a;->a()Lkotlin/reflect/jvm/internal/impl/b/a/h;

    move-result-object v0

    sget-object v2, Lkotlin/reflect/jvm/internal/impl/h/c;->a:Lkotlin/reflect/jvm/internal/impl/e/f;

    sget-object v3, Lkotlin/reflect/jvm/internal/impl/b/b$a;->d:Lkotlin/reflect/jvm/internal/impl/b/b$a;

    invoke-interface {p0}, Lkotlin/reflect/jvm/internal/impl/b/e;->x()Lkotlin/reflect/jvm/internal/impl/b/al;

    move-result-object v4

    invoke-static {p0, v0, v2, v3, v4}, Lkotlin/reflect/jvm/internal/impl/b/b/ac;->a(Lkotlin/reflect/jvm/internal/impl/b/m;Lkotlin/reflect/jvm/internal/impl/b/a/h;Lkotlin/reflect/jvm/internal/impl/e/f;Lkotlin/reflect/jvm/internal/impl/b/b$a;Lkotlin/reflect/jvm/internal/impl/b/al;)Lkotlin/reflect/jvm/internal/impl/b/b/ac;

    move-result-object v0

    .line 131
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v3

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v4

    invoke-static {p0}, Lkotlin/reflect/jvm/internal/impl/h/c/a;->d(Lkotlin/reflect/jvm/internal/impl/b/m;)Lkotlin/reflect/jvm/internal/impl/a/l;

    move-result-object v2

    sget-object v5, Lkotlin/reflect/jvm/internal/impl/k/ap;->a:Lkotlin/reflect/jvm/internal/impl/k/ap;

    invoke-interface {p0}, Lkotlin/reflect/jvm/internal/impl/b/e;->E_()Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v6

    invoke-virtual {v2, v5, v6}, Lkotlin/reflect/jvm/internal/impl/a/l;->a(Lkotlin/reflect/jvm/internal/impl/k/ap;Lkotlin/reflect/jvm/internal/impl/k/r;)Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v5

    sget-object v6, Lkotlin/reflect/jvm/internal/impl/b/u;->a:Lkotlin/reflect/jvm/internal/impl/b/u;

    sget-object v7, Lkotlin/reflect/jvm/internal/impl/b/ax;->e:Lkotlin/reflect/jvm/internal/impl/b/ay;

    move-object v2, v1

    invoke-virtual/range {v0 .. v7}, Lkotlin/reflect/jvm/internal/impl/b/b/ac;->b(Lkotlin/reflect/jvm/internal/impl/k/r;Lkotlin/reflect/jvm/internal/impl/b/aj;Ljava/util/List;Ljava/util/List;Lkotlin/reflect/jvm/internal/impl/k/r;Lkotlin/reflect/jvm/internal/impl/b/u;Lkotlin/reflect/jvm/internal/impl/b/ay;)Lkotlin/reflect/jvm/internal/impl/b/b/ac;

    move-result-object v0

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "@NotNull method %s.%s must not return null"

    new-array v2, v10, [Ljava/lang/Object;

    const-string v3, "kotlin/reflect/jvm/internal/impl/resolve/DescriptorFactory"

    aput-object v3, v2, v8

    const-string v3, "createEnumValuesMethod"

    aput-object v3, v2, v9

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    return-object v0
.end method

.method public static a(Lkotlin/reflect/jvm/internal/impl/b/ag;Lkotlin/reflect/jvm/internal/impl/b/a/h;)Lkotlin/reflect/jvm/internal/impl/b/b/aa;
    .locals 7

    .prologue
    const/4 v4, 0x3

    const/4 v6, 0x2

    const/4 v2, 0x1

    const/4 v3, 0x0

    if-nez p0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    new-array v4, v4, [Ljava/lang/Object;

    const-string v5, "propertyDescriptor"

    aput-object v5, v4, v3

    const-string v3, "kotlin/reflect/jvm/internal/impl/resolve/DescriptorFactory"

    aput-object v3, v4, v2

    const-string v2, "createDefaultSetter"

    aput-object v2, v4, v6

    invoke-static {v1, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    if-nez p1, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    new-array v4, v4, [Ljava/lang/Object;

    const-string v5, "annotations"

    aput-object v5, v4, v3

    const-string v3, "kotlin/reflect/jvm/internal/impl/resolve/DescriptorFactory"

    aput-object v3, v4, v2

    const-string v2, "createDefaultSetter"

    aput-object v2, v4, v6

    invoke-static {v1, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 51
    :cond_1
    invoke-interface {p0}, Lkotlin/reflect/jvm/internal/impl/b/ag;->x()Lkotlin/reflect/jvm/internal/impl/b/al;

    move-result-object v5

    move-object v0, p0

    move-object v1, p1

    move v4, v3

    invoke-static/range {v0 .. v5}, Lkotlin/reflect/jvm/internal/impl/h/b;->a(Lkotlin/reflect/jvm/internal/impl/b/ag;Lkotlin/reflect/jvm/internal/impl/b/a/h;ZZZLkotlin/reflect/jvm/internal/impl/b/al;)Lkotlin/reflect/jvm/internal/impl/b/b/aa;

    move-result-object v0

    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "@NotNull method %s.%s must not return null"

    new-array v4, v6, [Ljava/lang/Object;

    const-string v5, "kotlin/reflect/jvm/internal/impl/resolve/DescriptorFactory"

    aput-object v5, v4, v3

    const-string v3, "createDefaultSetter"

    aput-object v3, v4, v2

    invoke-static {v1, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    return-object v0
.end method

.method public static a(Lkotlin/reflect/jvm/internal/impl/b/ag;Lkotlin/reflect/jvm/internal/impl/b/a/h;ZZZLkotlin/reflect/jvm/internal/impl/b/al;)Lkotlin/reflect/jvm/internal/impl/b/b/aa;
    .locals 10

    .prologue
    const/4 v2, 0x3

    const/4 v9, 0x2

    const/4 v8, 0x1

    const/4 v7, 0x0

    if-nez p0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "propertyDescriptor"

    aput-object v3, v2, v7

    const-string v3, "kotlin/reflect/jvm/internal/impl/resolve/DescriptorFactory"

    aput-object v3, v2, v8

    const-string v3, "createSetter"

    aput-object v3, v2, v9

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    if-nez p1, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "annotations"

    aput-object v3, v2, v7

    const-string v3, "kotlin/reflect/jvm/internal/impl/resolve/DescriptorFactory"

    aput-object v3, v2, v8

    const-string v3, "createSetter"

    aput-object v3, v2, v9

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    if-nez p5, :cond_2

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "sourceElement"

    aput-object v3, v2, v7

    const-string v3, "kotlin/reflect/jvm/internal/impl/resolve/DescriptorFactory"

    aput-object v3, v2, v8

    const-string v3, "createSetter"

    aput-object v3, v2, v9

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 63
    :cond_2
    invoke-interface {p0}, Lkotlin/reflect/jvm/internal/impl/b/ag;->p()Lkotlin/reflect/jvm/internal/impl/b/ay;

    move-result-object v5

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move-object v6, p5

    invoke-static/range {v0 .. v6}, Lkotlin/reflect/jvm/internal/impl/h/b;->a(Lkotlin/reflect/jvm/internal/impl/b/ag;Lkotlin/reflect/jvm/internal/impl/b/a/h;ZZZLkotlin/reflect/jvm/internal/impl/b/ay;Lkotlin/reflect/jvm/internal/impl/b/al;)Lkotlin/reflect/jvm/internal/impl/b/b/aa;

    move-result-object v0

    if-nez v0, :cond_3

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "@NotNull method %s.%s must not return null"

    new-array v2, v9, [Ljava/lang/Object;

    const-string v3, "kotlin/reflect/jvm/internal/impl/resolve/DescriptorFactory"

    aput-object v3, v2, v7

    const-string v3, "createSetter"

    aput-object v3, v2, v8

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_3
    return-object v0
.end method

.method public static a(Lkotlin/reflect/jvm/internal/impl/b/ag;Lkotlin/reflect/jvm/internal/impl/b/a/h;ZZZLkotlin/reflect/jvm/internal/impl/b/ay;Lkotlin/reflect/jvm/internal/impl/b/al;)Lkotlin/reflect/jvm/internal/impl/b/b/aa;
    .locals 11

    .prologue
    if-nez p0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "propertyDescriptor"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "kotlin/reflect/jvm/internal/impl/resolve/DescriptorFactory"

    aput-object v4, v2, v3

    const/4 v3, 0x2

    const-string v4, "createSetter"

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    if-nez p1, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "annotations"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "kotlin/reflect/jvm/internal/impl/resolve/DescriptorFactory"

    aput-object v4, v2, v3

    const/4 v3, 0x2

    const-string v4, "createSetter"

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    if-nez p5, :cond_2

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "visibility"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "kotlin/reflect/jvm/internal/impl/resolve/DescriptorFactory"

    aput-object v4, v2, v3

    const/4 v3, 0x2

    const-string v4, "createSetter"

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    if-nez p6, :cond_3

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "sourceElement"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "kotlin/reflect/jvm/internal/impl/resolve/DescriptorFactory"

    aput-object v4, v2, v3

    const/4 v3, 0x2

    const-string v4, "createSetter"

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 76
    :cond_3
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/b/b/aa;

    invoke-interface {p0}, Lkotlin/reflect/jvm/internal/impl/b/ag;->m()Lkotlin/reflect/jvm/internal/impl/b/u;

    move-result-object v3

    sget-object v8, Lkotlin/reflect/jvm/internal/impl/b/b$a;->a:Lkotlin/reflect/jvm/internal/impl/b/b$a;

    const/4 v9, 0x0

    move-object v1, p0

    move-object v2, p1

    move-object/from16 v4, p5

    move v5, p2

    move v6, p3

    move v7, p4

    move-object/from16 v10, p6

    invoke-direct/range {v0 .. v10}, Lkotlin/reflect/jvm/internal/impl/b/b/aa;-><init>(Lkotlin/reflect/jvm/internal/impl/b/ag;Lkotlin/reflect/jvm/internal/impl/b/a/h;Lkotlin/reflect/jvm/internal/impl/b/u;Lkotlin/reflect/jvm/internal/impl/b/ay;ZZZLkotlin/reflect/jvm/internal/impl/b/b$a;Lkotlin/reflect/jvm/internal/impl/b/ai;Lkotlin/reflect/jvm/internal/impl/b/al;)V

    .line 80
    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/b/b/aa;->F()V

    .line 81
    if-nez v0, :cond_4

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "@NotNull method %s.%s must not return null"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "kotlin/reflect/jvm/internal/impl/resolve/DescriptorFactory"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "createSetter"

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_4
    return-object v0
.end method

.method public static a(Lkotlin/reflect/jvm/internal/impl/b/e;Lkotlin/reflect/jvm/internal/impl/b/al;)Lkotlin/reflect/jvm/internal/impl/b/b/f;
    .locals 7

    .prologue
    const/4 v2, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    if-nez p0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "containingClass"

    aput-object v3, v2, v4

    const-string v3, "kotlin/reflect/jvm/internal/impl/resolve/DescriptorFactory"

    aput-object v3, v2, v5

    const-string v3, "createPrimaryConstructorForObject"

    aput-object v3, v2, v6

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    if-nez p1, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "source"

    aput-object v3, v2, v4

    const-string v3, "kotlin/reflect/jvm/internal/impl/resolve/DescriptorFactory"

    aput-object v3, v2, v5

    const-string v3, "createPrimaryConstructorForObject"

    aput-object v3, v2, v6

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 123
    :cond_1
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/h/b$a;

    invoke-direct {v0, p0, p1}, Lkotlin/reflect/jvm/internal/impl/h/b$a;-><init>(Lkotlin/reflect/jvm/internal/impl/b/e;Lkotlin/reflect/jvm/internal/impl/b/al;)V

    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "@NotNull method %s.%s must not return null"

    new-array v2, v6, [Ljava/lang/Object;

    const-string v3, "kotlin/reflect/jvm/internal/impl/resolve/DescriptorFactory"

    aput-object v3, v2, v4

    const-string v3, "createPrimaryConstructorForObject"

    aput-object v3, v2, v5

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    return-object v0
.end method

.method public static a(Lkotlin/reflect/jvm/internal/impl/b/ag;Lkotlin/reflect/jvm/internal/impl/b/a/h;ZZZ)Lkotlin/reflect/jvm/internal/impl/b/b/z;
    .locals 9

    .prologue
    const/4 v2, 0x3

    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    if-nez p0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "propertyDescriptor"

    aput-object v3, v2, v6

    const-string v3, "kotlin/reflect/jvm/internal/impl/resolve/DescriptorFactory"

    aput-object v3, v2, v7

    const-string v3, "createGetter"

    aput-object v3, v2, v8

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    if-nez p1, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "annotations"

    aput-object v3, v2, v6

    const-string v3, "kotlin/reflect/jvm/internal/impl/resolve/DescriptorFactory"

    aput-object v3, v2, v7

    const-string v3, "createGetter"

    aput-object v3, v2, v8

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 100
    :cond_1
    invoke-interface {p0}, Lkotlin/reflect/jvm/internal/impl/b/ag;->x()Lkotlin/reflect/jvm/internal/impl/b/al;

    move-result-object v5

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    invoke-static/range {v0 .. v5}, Lkotlin/reflect/jvm/internal/impl/h/b;->b(Lkotlin/reflect/jvm/internal/impl/b/ag;Lkotlin/reflect/jvm/internal/impl/b/a/h;ZZZLkotlin/reflect/jvm/internal/impl/b/al;)Lkotlin/reflect/jvm/internal/impl/b/b/z;

    move-result-object v0

    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "@NotNull method %s.%s must not return null"

    new-array v2, v8, [Ljava/lang/Object;

    const-string v3, "kotlin/reflect/jvm/internal/impl/resolve/DescriptorFactory"

    aput-object v3, v2, v6

    const-string v3, "createGetter"

    aput-object v3, v2, v7

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    return-object v0
.end method

.method public static b(Lkotlin/reflect/jvm/internal/impl/b/e;)Lkotlin/reflect/jvm/internal/impl/b/ak;
    .locals 14

    .prologue
    const/4 v13, 0x2

    const/4 v12, 0x1

    const/4 v2, 0x0

    const/4 v3, 0x0

    if-nez p0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const-string v4, "enumClass"

    aput-object v4, v2, v3

    const-string v3, "kotlin/reflect/jvm/internal/impl/resolve/DescriptorFactory"

    aput-object v3, v2, v12

    const-string v3, "createEnumValueOfMethod"

    aput-object v3, v2, v13

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 139
    :cond_0
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/b/a/h;->a:Lkotlin/reflect/jvm/internal/impl/b/a/h$a;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/b/a/h$a;->a()Lkotlin/reflect/jvm/internal/impl/b/a/h;

    move-result-object v0

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/h/c;->b:Lkotlin/reflect/jvm/internal/impl/e/f;

    sget-object v4, Lkotlin/reflect/jvm/internal/impl/b/b$a;->d:Lkotlin/reflect/jvm/internal/impl/b/b$a;

    invoke-interface {p0}, Lkotlin/reflect/jvm/internal/impl/b/e;->x()Lkotlin/reflect/jvm/internal/impl/b/al;

    move-result-object v5

    invoke-static {p0, v0, v1, v4, v5}, Lkotlin/reflect/jvm/internal/impl/b/b/ac;->a(Lkotlin/reflect/jvm/internal/impl/b/m;Lkotlin/reflect/jvm/internal/impl/b/a/h;Lkotlin/reflect/jvm/internal/impl/e/f;Lkotlin/reflect/jvm/internal/impl/b/b$a;Lkotlin/reflect/jvm/internal/impl/b/al;)Lkotlin/reflect/jvm/internal/impl/b/b/ac;

    move-result-object v1

    .line 142
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/b/b/ag;

    sget-object v4, Lkotlin/reflect/jvm/internal/impl/b/a/h;->a:Lkotlin/reflect/jvm/internal/impl/b/a/h$a;

    invoke-virtual {v4}, Lkotlin/reflect/jvm/internal/impl/b/a/h$a;->a()Lkotlin/reflect/jvm/internal/impl/b/a/h;

    move-result-object v4

    const-string v5, "value"

    invoke-static {v5}, Lkotlin/reflect/jvm/internal/impl/e/f;->a(Ljava/lang/String;)Lkotlin/reflect/jvm/internal/impl/e/f;

    move-result-object v5

    invoke-static {p0}, Lkotlin/reflect/jvm/internal/impl/h/c/a;->d(Lkotlin/reflect/jvm/internal/impl/b/m;)Lkotlin/reflect/jvm/internal/impl/a/l;

    move-result-object v6

    invoke-virtual {v6}, Lkotlin/reflect/jvm/internal/impl/a/l;->J()Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v6

    invoke-interface {p0}, Lkotlin/reflect/jvm/internal/impl/b/e;->x()Lkotlin/reflect/jvm/internal/impl/b/al;

    move-result-object v11

    move v7, v3

    move v8, v3

    move v9, v3

    move-object v10, v2

    invoke-direct/range {v0 .. v11}, Lkotlin/reflect/jvm/internal/impl/b/b/ag;-><init>(Lkotlin/reflect/jvm/internal/impl/b/a;Lkotlin/reflect/jvm/internal/impl/b/at;ILkotlin/reflect/jvm/internal/impl/b/a/h;Lkotlin/reflect/jvm/internal/impl/e/f;Lkotlin/reflect/jvm/internal/impl/k/r;ZZZLkotlin/reflect/jvm/internal/impl/k/r;Lkotlin/reflect/jvm/internal/impl/b/al;)V

    .line 150
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v7

    invoke-static {v0}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v8

    invoke-interface {p0}, Lkotlin/reflect/jvm/internal/impl/b/e;->E_()Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v9

    sget-object v10, Lkotlin/reflect/jvm/internal/impl/b/u;->a:Lkotlin/reflect/jvm/internal/impl/b/u;

    sget-object v11, Lkotlin/reflect/jvm/internal/impl/b/ax;->e:Lkotlin/reflect/jvm/internal/impl/b/ay;

    move-object v4, v1

    move-object v5, v2

    move-object v6, v2

    invoke-virtual/range {v4 .. v11}, Lkotlin/reflect/jvm/internal/impl/b/b/ac;->b(Lkotlin/reflect/jvm/internal/impl/k/r;Lkotlin/reflect/jvm/internal/impl/b/aj;Ljava/util/List;Ljava/util/List;Lkotlin/reflect/jvm/internal/impl/k/r;Lkotlin/reflect/jvm/internal/impl/b/u;Lkotlin/reflect/jvm/internal/impl/b/ay;)Lkotlin/reflect/jvm/internal/impl/b/b/ac;

    move-result-object v0

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "@NotNull method %s.%s must not return null"

    new-array v2, v13, [Ljava/lang/Object;

    const-string v4, "kotlin/reflect/jvm/internal/impl/resolve/DescriptorFactory"

    aput-object v4, v2, v3

    const-string v3, "createEnumValueOfMethod"

    aput-object v3, v2, v12

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    return-object v0
.end method

.method public static b(Lkotlin/reflect/jvm/internal/impl/b/ag;Lkotlin/reflect/jvm/internal/impl/b/a/h;)Lkotlin/reflect/jvm/internal/impl/b/b/z;
    .locals 7

    .prologue
    const/4 v2, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    if-nez p0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "propertyDescriptor"

    aput-object v3, v2, v4

    const-string v3, "kotlin/reflect/jvm/internal/impl/resolve/DescriptorFactory"

    aput-object v3, v2, v5

    const-string v3, "createDefaultGetter"

    aput-object v3, v2, v6

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    if-nez p1, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "annotations"

    aput-object v3, v2, v4

    const-string v3, "kotlin/reflect/jvm/internal/impl/resolve/DescriptorFactory"

    aput-object v3, v2, v5

    const-string v3, "createDefaultGetter"

    aput-object v3, v2, v6

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 89
    :cond_1
    invoke-static {p0, p1, v5, v4, v4}, Lkotlin/reflect/jvm/internal/impl/h/b;->a(Lkotlin/reflect/jvm/internal/impl/b/ag;Lkotlin/reflect/jvm/internal/impl/b/a/h;ZZZ)Lkotlin/reflect/jvm/internal/impl/b/b/z;

    move-result-object v0

    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "@NotNull method %s.%s must not return null"

    new-array v2, v6, [Ljava/lang/Object;

    const-string v3, "kotlin/reflect/jvm/internal/impl/resolve/DescriptorFactory"

    aput-object v3, v2, v4

    const-string v3, "createDefaultGetter"

    aput-object v3, v2, v5

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    return-object v0
.end method

.method public static b(Lkotlin/reflect/jvm/internal/impl/b/ag;Lkotlin/reflect/jvm/internal/impl/b/a/h;ZZZLkotlin/reflect/jvm/internal/impl/b/al;)Lkotlin/reflect/jvm/internal/impl/b/b/z;
    .locals 11

    .prologue
    if-nez p0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "propertyDescriptor"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "kotlin/reflect/jvm/internal/impl/resolve/DescriptorFactory"

    aput-object v4, v2, v3

    const/4 v3, 0x2

    const-string v4, "createGetter"

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    if-nez p1, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "annotations"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "kotlin/reflect/jvm/internal/impl/resolve/DescriptorFactory"

    aput-object v4, v2, v3

    const/4 v3, 0x2

    const-string v4, "createGetter"

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    if-nez p5, :cond_2

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "sourceElement"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "kotlin/reflect/jvm/internal/impl/resolve/DescriptorFactory"

    aput-object v4, v2, v3

    const/4 v3, 0x2

    const-string v4, "createGetter"

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 112
    :cond_2
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/b/b/z;

    invoke-interface {p0}, Lkotlin/reflect/jvm/internal/impl/b/ag;->m()Lkotlin/reflect/jvm/internal/impl/b/u;

    move-result-object v3

    invoke-interface {p0}, Lkotlin/reflect/jvm/internal/impl/b/ag;->p()Lkotlin/reflect/jvm/internal/impl/b/ay;

    move-result-object v4

    sget-object v8, Lkotlin/reflect/jvm/internal/impl/b/b$a;->a:Lkotlin/reflect/jvm/internal/impl/b/b$a;

    const/4 v9, 0x0

    move-object v1, p0

    move-object v2, p1

    move v5, p2

    move v6, p3

    move v7, p4

    move-object/from16 v10, p5

    invoke-direct/range {v0 .. v10}, Lkotlin/reflect/jvm/internal/impl/b/b/z;-><init>(Lkotlin/reflect/jvm/internal/impl/b/ag;Lkotlin/reflect/jvm/internal/impl/b/a/h;Lkotlin/reflect/jvm/internal/impl/b/u;Lkotlin/reflect/jvm/internal/impl/b/ay;ZZZLkotlin/reflect/jvm/internal/impl/b/b$a;Lkotlin/reflect/jvm/internal/impl/b/ah;Lkotlin/reflect/jvm/internal/impl/b/al;)V

    if-nez v0, :cond_3

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "@NotNull method %s.%s must not return null"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "kotlin/reflect/jvm/internal/impl/resolve/DescriptorFactory"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "createGetter"

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_3
    return-object v0
.end method
