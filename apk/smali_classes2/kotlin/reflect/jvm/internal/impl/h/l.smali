.class public final Lkotlin/reflect/jvm/internal/impl/h/l;
.super Ljava/lang/Object;
.source "VisibilityUtil.kt"


# direct methods
.method public static final a(Ljava/util/Collection;)Lkotlin/reflect/jvm/internal/impl/b/b;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<+",
            "Lkotlin/reflect/jvm/internal/impl/b/b;",
            ">;)",
            "Lkotlin/reflect/jvm/internal/impl/b/b;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    const-string v0, "descriptors"

    invoke-static {p0, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 23
    invoke-interface {p0}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sget-boolean v2, Lkotlin/p;->a:Z

    if-eqz v2, :cond_1

    if-nez v0, :cond_1

    const-string v1, "Assertion failed"

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    :cond_0
    move v0, v1

    goto :goto_0

    .line 25
    :cond_1
    const/4 v0, 0x0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/b;

    .line 26
    invoke-interface {p0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move-object v2, v0

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/b;

    .line 27
    if-nez v2, :cond_3

    :cond_2
    :goto_2
    move-object v2, v0

    .line 26
    goto :goto_1

    .line 32
    :cond_3
    invoke-interface {v2}, Lkotlin/reflect/jvm/internal/impl/b/b;->p()Lkotlin/reflect/jvm/internal/impl/b/ay;

    move-result-object v4

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/b/b;->p()Lkotlin/reflect/jvm/internal/impl/b/ay;

    move-result-object v5

    invoke-static {v4, v5}, Lkotlin/reflect/jvm/internal/impl/b/ax;->b(Lkotlin/reflect/jvm/internal/impl/b/ay;Lkotlin/reflect/jvm/internal/impl/b/ay;)Ljava/lang/Integer;

    move-result-object v4

    .line 33
    if-eqz v4, :cond_4

    invoke-virtual {v4}, Ljava/lang/Number;->intValue()I

    move-result v4

    invoke-static {v4, v1}, Lkotlin/d/b/l;->a(II)I

    move-result v4

    if-ltz v4, :cond_2

    :cond_4
    move-object v0, v2

    goto :goto_2

    .line 37
    :cond_5
    if-nez v2, :cond_6

    invoke-static {}, Lkotlin/d/b/l;->a()V

    :cond_6
    return-object v2
.end method
