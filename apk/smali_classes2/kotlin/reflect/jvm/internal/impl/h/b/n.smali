.class public final Lkotlin/reflect/jvm/internal/impl/h/b/n;
.super Ljava/lang/Object;
.source "IntegerValueTypeConstructor.kt"

# interfaces
.implements Lkotlin/reflect/jvm/internal/impl/k/ad;


# instance fields
.field private final a:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/k/r;",
            ">;"
        }
    .end annotation
.end field

.field private final b:J

.field private final c:Lkotlin/reflect/jvm/internal/impl/a/l;


# virtual methods
.method public G_()Ljava/util/Collection;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/k/r;",
            ">;"
        }
    .end annotation

    .prologue
    .line 47
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/h/b/n;->a:Ljava/util/ArrayList;

    check-cast v0, Ljava/util/Collection;

    return-object v0
.end method

.method public a()Ljava/lang/Void;
    .locals 1

    .prologue
    .line 55
    const/4 v0, 0x0

    return-object v0
.end method

.method public b()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/b/aq;",
            ">;"
        }
    .end annotation

    .prologue
    .line 49
    invoke-static {}, Lkotlin/a/m;->a()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public synthetic d()Lkotlin/reflect/jvm/internal/impl/b/h;
    .locals 1

    .prologue
    .line 25
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/h/b/n;->a()Ljava/lang/Void;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/h;

    return-object v0
.end method

.method public e()Z
    .locals 1

    .prologue
    .line 51
    const/4 v0, 0x0

    return v0
.end method

.method public f()Lkotlin/reflect/jvm/internal/impl/a/l;
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/h/b/n;->c:Lkotlin/reflect/jvm/internal/impl/a/l;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 63
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "IntegerValueType("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lkotlin/reflect/jvm/internal/impl/h/b/n;->b:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
