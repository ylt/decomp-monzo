.class public final Lkotlin/reflect/jvm/internal/impl/h/e/m;
.super Lkotlin/reflect/jvm/internal/impl/h/e/a;
.source "TypeIntersectionScope.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lkotlin/reflect/jvm/internal/impl/h/e/m$a;
    }
.end annotation


# static fields
.field public static final a:Lkotlin/reflect/jvm/internal/impl/h/e/m$a;


# instance fields
.field private final b:Lkotlin/reflect/jvm/internal/impl/h/e/b;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lkotlin/reflect/jvm/internal/impl/h/e/m$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lkotlin/reflect/jvm/internal/impl/h/e/m$a;-><init>(Lkotlin/d/b/i;)V

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/h/e/m;->a:Lkotlin/reflect/jvm/internal/impl/h/e/m$a;

    return-void
.end method

.method private constructor <init>(Lkotlin/reflect/jvm/internal/impl/h/e/b;)V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/h/e/a;-><init>()V

    iput-object p1, p0, Lkotlin/reflect/jvm/internal/impl/h/e/m;->b:Lkotlin/reflect/jvm/internal/impl/h/e/b;

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/reflect/jvm/internal/impl/h/e/b;Lkotlin/d/b/i;)V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0, p1}, Lkotlin/reflect/jvm/internal/impl/h/e/m;-><init>(Lkotlin/reflect/jvm/internal/impl/h/e/b;)V

    return-void
.end method

.method public static final a(Ljava/lang/String;Ljava/util/Collection;)Lkotlin/reflect/jvm/internal/impl/h/e/h;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Collection",
            "<+",
            "Lkotlin/reflect/jvm/internal/impl/k/r;",
            ">;)",
            "Lkotlin/reflect/jvm/internal/impl/h/e/h;"
        }
    .end annotation

    const-string v0, "message"

    invoke-static {p0, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "types"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v0, Lkotlin/reflect/jvm/internal/impl/h/e/m;->a:Lkotlin/reflect/jvm/internal/impl/h/e/m$a;

    invoke-virtual {v0, p0, p1}, Lkotlin/reflect/jvm/internal/impl/h/e/m$a;->a(Ljava/lang/String;Ljava/util/Collection;)Lkotlin/reflect/jvm/internal/impl/h/e/h;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public a(Lkotlin/reflect/jvm/internal/impl/e/f;Lkotlin/reflect/jvm/internal/impl/c/a/b;)Ljava/util/Collection;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/reflect/jvm/internal/impl/e/f;",
            "Lkotlin/reflect/jvm/internal/impl/c/a/b;",
            ")",
            "Ljava/util/Collection",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/b/ag;",
            ">;"
        }
    .end annotation

    .prologue
    const-string v0, "name"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "location"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 32
    invoke-super {p0, p1, p2}, Lkotlin/reflect/jvm/internal/impl/h/e/a;->a(Lkotlin/reflect/jvm/internal/impl/e/f;Lkotlin/reflect/jvm/internal/impl/c/a/b;)Ljava/util/Collection;

    move-result-object v1

    sget-object v0, Lkotlin/reflect/jvm/internal/impl/h/e/m$d;->a:Lkotlin/reflect/jvm/internal/impl/h/e/m$d;

    check-cast v0, Lkotlin/d/a/b;

    invoke-static {v1, v0}, Lkotlin/reflect/jvm/internal/impl/h/k;->a(Ljava/util/Collection;Lkotlin/d/a/b;)Ljava/util/Collection;

    move-result-object v0

    return-object v0
.end method

.method public a(Lkotlin/reflect/jvm/internal/impl/h/e/d;Lkotlin/d/a/b;)Ljava/util/Collection;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/reflect/jvm/internal/impl/h/e/d;",
            "Lkotlin/d/a/b",
            "<-",
            "Lkotlin/reflect/jvm/internal/impl/e/f;",
            "Ljava/lang/Boolean;",
            ">;)",
            "Ljava/util/Collection",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/b/m;",
            ">;"
        }
    .end annotation

    .prologue
    const-string v0, "kindFilter"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "nameFilter"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 35
    invoke-super {p0, p1, p2}, Lkotlin/reflect/jvm/internal/impl/h/e/a;->a(Lkotlin/reflect/jvm/internal/impl/h/e/d;Lkotlin/d/a/b;)Ljava/util/Collection;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 57
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 58
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 59
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    move-object v0, v1

    .line 60
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/m;

    .line 35
    instance-of v0, v0, Lkotlin/reflect/jvm/internal/impl/b/a;

    if-eqz v0, :cond_0

    .line 61
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 63
    :cond_0
    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 66
    :cond_1
    new-instance v1, Lkotlin/h;

    invoke-direct {v1, v2, v3}, Lkotlin/h;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-virtual {v1}, Lkotlin/h;->c()Ljava/lang/Object;

    move-result-object v0

    .line 35
    check-cast v0, Ljava/util/List;

    invoke-virtual {v1}, Lkotlin/h;->d()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    .line 38
    if-nez v0, :cond_2

    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type kotlin.collections.Collection<org.jetbrains.kotlin.descriptors.CallableDescriptor>"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    check-cast v0, Ljava/util/Collection;

    sget-object v2, Lkotlin/reflect/jvm/internal/impl/h/e/m$b;->a:Lkotlin/reflect/jvm/internal/impl/h/e/m$b;

    check-cast v2, Lkotlin/d/a/b;

    invoke-static {v0, v2}, Lkotlin/reflect/jvm/internal/impl/h/k;->a(Ljava/util/Collection;Lkotlin/d/a/b;)Ljava/util/Collection;

    move-result-object v0

    check-cast v1, Ljava/lang/Iterable;

    invoke-static {v0, v1}, Lkotlin/a/m;->b(Ljava/util/Collection;Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    return-object v0
.end method

.method public b(Lkotlin/reflect/jvm/internal/impl/e/f;Lkotlin/reflect/jvm/internal/impl/c/a/b;)Ljava/util/Collection;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/reflect/jvm/internal/impl/e/f;",
            "Lkotlin/reflect/jvm/internal/impl/c/a/b;",
            ")",
            "Ljava/util/Collection",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/b/ak;",
            ">;"
        }
    .end annotation

    .prologue
    const-string v0, "name"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "location"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 29
    invoke-super {p0, p1, p2}, Lkotlin/reflect/jvm/internal/impl/h/e/a;->b(Lkotlin/reflect/jvm/internal/impl/e/f;Lkotlin/reflect/jvm/internal/impl/c/a/b;)Ljava/util/Collection;

    move-result-object v1

    sget-object v0, Lkotlin/reflect/jvm/internal/impl/h/e/m$c;->a:Lkotlin/reflect/jvm/internal/impl/h/e/m$c;

    check-cast v0, Lkotlin/d/a/b;

    invoke-static {v1, v0}, Lkotlin/reflect/jvm/internal/impl/h/k;->a(Ljava/util/Collection;Lkotlin/d/a/b;)Ljava/util/Collection;

    move-result-object v0

    return-object v0
.end method

.method public synthetic c()Lkotlin/reflect/jvm/internal/impl/h/e/h;
    .locals 1

    .prologue
    .line 27
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/h/e/m;->d()Lkotlin/reflect/jvm/internal/impl/h/e/b;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/h/e/h;

    return-object v0
.end method

.method protected d()Lkotlin/reflect/jvm/internal/impl/h/e/b;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/h/e/m;->b:Lkotlin/reflect/jvm/internal/impl/h/e/b;

    return-object v0
.end method
