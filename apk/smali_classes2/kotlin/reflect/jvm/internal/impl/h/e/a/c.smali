.class public Lkotlin/reflect/jvm/internal/impl/h/e/a/c;
.super Ljava/lang/Object;
.source "ImplicitClassReceiver.kt"

# interfaces
.implements Lkotlin/reflect/jvm/internal/impl/h/e/a/d;
.implements Lkotlin/reflect/jvm/internal/impl/h/e/a/g;


# instance fields
.field private final a:Lkotlin/reflect/jvm/internal/impl/b/e;

.field private final b:Lkotlin/reflect/jvm/internal/impl/b/e;


# direct methods
.method public constructor <init>(Lkotlin/reflect/jvm/internal/impl/b/e;)V
    .locals 1

    .prologue
    const-string v0, "classDescriptor"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lkotlin/reflect/jvm/internal/impl/h/e/a/c;->b:Lkotlin/reflect/jvm/internal/impl/b/e;

    .line 35
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/h/e/a/c;->b:Lkotlin/reflect/jvm/internal/impl/b/e;

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/h/e/a/c;->a:Lkotlin/reflect/jvm/internal/impl/b/e;

    return-void
.end method


# virtual methods
.method public synthetic a()Lkotlin/reflect/jvm/internal/impl/k/r;
    .locals 1

    .prologue
    .line 31
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/h/e/a/c;->b()Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/r;

    return-object v0
.end method

.method public b()Lkotlin/reflect/jvm/internal/impl/k/v;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/h/e/a/c;->b:Lkotlin/reflect/jvm/internal/impl/b/e;

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/b/e;->E_()Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v0

    return-object v0
.end method

.method public final c()Lkotlin/reflect/jvm/internal/impl/b/e;
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/h/e/a/c;->b:Lkotlin/reflect/jvm/internal/impl/b/e;

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 37
    iget-object v2, p0, Lkotlin/reflect/jvm/internal/impl/h/e/a/c;->b:Lkotlin/reflect/jvm/internal/impl/b/e;

    instance-of v0, p1, Lkotlin/reflect/jvm/internal/impl/h/e/a/c;

    if-nez v0, :cond_1

    move-object v0, v1

    :goto_0
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/h/e/a/c;

    if-eqz v0, :cond_0

    iget-object v1, v0, Lkotlin/reflect/jvm/internal/impl/h/e/a/c;->b:Lkotlin/reflect/jvm/internal/impl/b/e;

    :cond_0
    invoke-static {v2, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    return v0

    :cond_1
    move-object v0, p1

    goto :goto_0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/h/e/a/c;->b:Lkotlin/reflect/jvm/internal/impl/b/e;

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 41
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Class{"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/h/e/a/c;->b()Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
