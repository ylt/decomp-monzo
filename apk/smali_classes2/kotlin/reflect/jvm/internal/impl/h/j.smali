.class public Lkotlin/reflect/jvm/internal/impl/h/j;
.super Ljava/lang/Object;
.source "OverridingUtil.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lkotlin/reflect/jvm/internal/impl/h/j$8;,
        Lkotlin/reflect/jvm/internal/impl/h/j$a;
    }
.end annotation


# static fields
.field public static final a:Lkotlin/reflect/jvm/internal/impl/h/j;

.field static final synthetic b:Z

.field private static final c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/h/d;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final d:Lkotlin/reflect/jvm/internal/impl/k/a/c$a;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 42
    const-class v0, Lkotlin/reflect/jvm/internal/impl/h/j;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lkotlin/reflect/jvm/internal/impl/h/j;->b:Z

    .line 44
    const-class v0, Lkotlin/reflect/jvm/internal/impl/h/d;

    const-class v1, Lkotlin/reflect/jvm/internal/impl/h/d;

    invoke-virtual {v1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    invoke-static {v0, v1}, Ljava/util/ServiceLoader;->load(Ljava/lang/Class;Ljava/lang/ClassLoader;)Ljava/util/ServiceLoader;

    move-result-object v0

    invoke-static {v0}, Lkotlin/a/m;->k(Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object v0

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/h/j;->c:Ljava/util/List;

    .line 50
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/h/j;

    new-instance v1, Lkotlin/reflect/jvm/internal/impl/h/j$1;

    invoke-direct {v1}, Lkotlin/reflect/jvm/internal/impl/h/j$1;-><init>()V

    invoke-direct {v0, v1}, Lkotlin/reflect/jvm/internal/impl/h/j;-><init>(Lkotlin/reflect/jvm/internal/impl/k/a/c$a;)V

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/h/j;->a:Lkotlin/reflect/jvm/internal/impl/h/j;

    return-void

    .line 42
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private constructor <init>(Lkotlin/reflect/jvm/internal/impl/k/a/c$a;)V
    .locals 0

    .prologue
    .line 64
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 65
    iput-object p1, p0, Lkotlin/reflect/jvm/internal/impl/h/j;->d:Lkotlin/reflect/jvm/internal/impl/k/a/c$a;

    .line 66
    return-void
.end method

.method public static a(Ljava/util/Collection;Lkotlin/d/a/b;)Ljava/lang/Object;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<H:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/Collection",
            "<TH;>;",
            "Lkotlin/d/a/b",
            "<TH;",
            "Lkotlin/reflect/jvm/internal/impl/b/a;",
            ">;)TH;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x3

    const/4 v10, 0x0

    const/4 v9, 0x2

    const/4 v8, 0x1

    if-nez p0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "overridables"

    aput-object v3, v2, v10

    const-string v3, "kotlin/reflect/jvm/internal/impl/resolve/OverridingUtil"

    aput-object v3, v2, v8

    const-string v3, "selectMostSpecificMember"

    aput-object v3, v2, v9

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    if-nez p1, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "descriptorByHandle"

    aput-object v3, v2, v10

    const-string v3, "kotlin/reflect/jvm/internal/impl/resolve/OverridingUtil"

    aput-object v3, v2, v8

    const-string v3, "selectMostSpecificMember"

    aput-object v3, v2, v9

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 566
    :cond_1
    sget-boolean v0, Lkotlin/reflect/jvm/internal/impl/h/j;->b:Z

    if-nez v0, :cond_2

    invoke-interface {p0}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "Should have at least one overridable descriptor"

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 568
    :cond_2
    invoke-interface {p0}, Ljava/util/Collection;->size()I

    move-result v0

    if-ne v0, v8, :cond_3

    .line 569
    invoke-static {p0}, Lkotlin/a/m;->b(Ljava/lang/Iterable;)Ljava/lang/Object;

    move-result-object v2

    if-nez v2, :cond_a

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "@NotNull method %s.%s must not return null"

    new-array v2, v9, [Ljava/lang/Object;

    const-string v3, "kotlin/reflect/jvm/internal/impl/resolve/OverridingUtil"

    aput-object v3, v2, v10

    const-string v3, "selectMostSpecificMember"

    aput-object v3, v2, v8

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 572
    :cond_3
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4, v9}, Ljava/util/ArrayList;-><init>(I)V

    .line 573
    invoke-static {p0, p1}, Lkotlin/a/m;->c(Ljava/lang/Iterable;Lkotlin/d/a/b;)Ljava/util/List;

    move-result-object v5

    .line 575
    invoke-static {p0}, Lkotlin/a/m;->b(Ljava/lang/Iterable;)Ljava/lang/Object;

    move-result-object v1

    .line 576
    invoke-interface {p1, v1}, Lkotlin/d/a/b;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/a;

    .line 578
    invoke-interface {p0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v6

    move-object v2, v1

    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_5

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    .line 579
    invoke-interface {p1, v3}, Lkotlin/d/a/b;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lkotlin/reflect/jvm/internal/impl/b/a;

    .line 580
    invoke-static {v1, v5}, Lkotlin/reflect/jvm/internal/impl/h/j;->a(Lkotlin/reflect/jvm/internal/impl/b/a;Ljava/util/Collection;)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 581
    invoke-interface {v4, v3}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 583
    :cond_4
    invoke-static {v1, v0}, Lkotlin/reflect/jvm/internal/impl/h/j;->c(Lkotlin/reflect/jvm/internal/impl/b/a;Lkotlin/reflect/jvm/internal/impl/b/a;)Z

    move-result v7

    if-eqz v7, :cond_c

    invoke-static {v0, v1}, Lkotlin/reflect/jvm/internal/impl/h/j;->c(Lkotlin/reflect/jvm/internal/impl/b/a;Lkotlin/reflect/jvm/internal/impl/b/a;)Z

    move-result v1

    if-nez v1, :cond_c

    move-object v1, v3

    :goto_1
    move-object v2, v1

    .line 587
    goto :goto_0

    .line 589
    :cond_5
    invoke-interface {v4}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 590
    if-nez v2, :cond_a

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "@NotNull method %s.%s must not return null"

    new-array v2, v9, [Ljava/lang/Object;

    const-string v3, "kotlin/reflect/jvm/internal/impl/resolve/OverridingUtil"

    aput-object v3, v2, v10

    const-string v3, "selectMostSpecificMember"

    aput-object v3, v2, v8

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 592
    :cond_6
    invoke-interface {v4}, Ljava/util/Collection;->size()I

    move-result v0

    if-ne v0, v8, :cond_7

    .line 593
    invoke-static {v4}, Lkotlin/a/m;->b(Ljava/lang/Iterable;)Ljava/lang/Object;

    move-result-object v2

    if-nez v2, :cond_a

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "@NotNull method %s.%s must not return null"

    new-array v2, v9, [Ljava/lang/Object;

    const-string v3, "kotlin/reflect/jvm/internal/impl/resolve/OverridingUtil"

    aput-object v3, v2, v10

    const-string v3, "selectMostSpecificMember"

    aput-object v3, v2, v8

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 596
    :cond_7
    const/4 v1, 0x0

    .line 597
    invoke-interface {v4}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_8
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_b

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 599
    invoke-interface {p1, v2}, Lkotlin/d/a/b;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/a;

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/b/a;->g()Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v0

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/k/o;->a(Lkotlin/reflect/jvm/internal/impl/k/r;)Z

    move-result v0

    if-nez v0, :cond_8

    .line 604
    :goto_2
    if-eqz v2, :cond_9

    .line 605
    if-nez v2, :cond_a

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "@NotNull method %s.%s must not return null"

    new-array v2, v9, [Ljava/lang/Object;

    const-string v3, "kotlin/reflect/jvm/internal/impl/resolve/OverridingUtil"

    aput-object v3, v2, v10

    const-string v3, "selectMostSpecificMember"

    aput-object v3, v2, v8

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 608
    :cond_9
    invoke-static {v4}, Lkotlin/a/m;->b(Ljava/lang/Iterable;)Ljava/lang/Object;

    move-result-object v2

    if-nez v2, :cond_a

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "@NotNull method %s.%s must not return null"

    new-array v2, v9, [Ljava/lang/Object;

    const-string v3, "kotlin/reflect/jvm/internal/impl/resolve/OverridingUtil"

    aput-object v3, v2, v10

    const-string v3, "selectMostSpecificMember"

    aput-object v3, v2, v8

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_a
    return-object v2

    :cond_b
    move-object v2, v1

    goto :goto_2

    :cond_c
    move-object v1, v2

    goto/16 :goto_1
.end method

.method public static a(Ljava/lang/Object;Ljava/util/Collection;Lkotlin/d/a/b;Lkotlin/d/a/b;)Ljava/util/Collection;
    .locals 9
    .param p1    # Ljava/util/Collection;
        .annotation build Lorg/jetbrains/annotations/Mutable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<H:",
            "Ljava/lang/Object;",
            ">(TH;",
            "Ljava/util/Collection",
            "<TH;>;",
            "Lkotlin/d/a/b",
            "<TH;",
            "Lkotlin/reflect/jvm/internal/impl/b/a;",
            ">;",
            "Lkotlin/d/a/b",
            "<TH;",
            "Lkotlin/n;",
            ">;)",
            "Ljava/util/Collection",
            "<TH;>;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x3

    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    if-nez p0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "overrider"

    aput-object v3, v2, v6

    const-string v3, "kotlin/reflect/jvm/internal/impl/resolve/OverridingUtil"

    aput-object v3, v2, v7

    const-string v3, "extractMembersOverridableInBothWays"

    aput-object v3, v2, v8

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    if-nez p1, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "extractFrom"

    aput-object v3, v2, v6

    const-string v3, "kotlin/reflect/jvm/internal/impl/resolve/OverridingUtil"

    aput-object v3, v2, v7

    const-string v3, "extractMembersOverridableInBothWays"

    aput-object v3, v2, v8

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    if-nez p2, :cond_2

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "descriptorByHandle"

    aput-object v3, v2, v6

    const-string v3, "kotlin/reflect/jvm/internal/impl/resolve/OverridingUtil"

    aput-object v3, v2, v7

    const-string v3, "extractMembersOverridableInBothWays"

    aput-object v3, v2, v8

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    if-nez p3, :cond_3

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "onConflict"

    aput-object v3, v2, v6

    const-string v3, "kotlin/reflect/jvm/internal/impl/resolve/OverridingUtil"

    aput-object v3, v2, v7

    const-string v3, "extractMembersOverridableInBothWays"

    aput-object v3, v2, v8

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 713
    :cond_3
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 714
    invoke-interface {v2, p0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 715
    invoke-interface {p2, p0}, Lkotlin/d/a/b;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/a;

    .line 716
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_4
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_7

    .line 717
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    .line 718
    invoke-interface {p2, v4}, Lkotlin/d/a/b;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lkotlin/reflect/jvm/internal/impl/b/a;

    .line 719
    if-ne p0, v4, :cond_5

    .line 720
    invoke-interface {v3}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    .line 724
    :cond_5
    invoke-static {v0, v1}, Lkotlin/reflect/jvm/internal/impl/h/j;->d(Lkotlin/reflect/jvm/internal/impl/b/a;Lkotlin/reflect/jvm/internal/impl/b/a;)Lkotlin/reflect/jvm/internal/impl/h/j$a$a;

    move-result-object v1

    .line 726
    sget-object v5, Lkotlin/reflect/jvm/internal/impl/h/j$a$a;->a:Lkotlin/reflect/jvm/internal/impl/h/j$a$a;

    if-ne v1, v5, :cond_6

    .line 727
    invoke-interface {v2, v4}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 728
    invoke-interface {v3}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    .line 730
    :cond_6
    sget-object v5, Lkotlin/reflect/jvm/internal/impl/h/j$a$a;->c:Lkotlin/reflect/jvm/internal/impl/h/j$a$a;

    if-ne v1, v5, :cond_4

    .line 731
    invoke-interface {p3, v4}, Lkotlin/d/a/b;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 732
    invoke-interface {v3}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    .line 735
    :cond_7
    if-nez v2, :cond_8

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "@NotNull method %s.%s must not return null"

    new-array v2, v8, [Ljava/lang/Object;

    const-string v3, "kotlin/reflect/jvm/internal/impl/resolve/OverridingUtil"

    aput-object v3, v2, v6

    const-string v3, "extractMembersOverridableInBothWays"

    aput-object v3, v2, v7

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_8
    return-object v2
.end method

.method private static a(Lkotlin/reflect/jvm/internal/impl/b/b;Ljava/util/Collection;Lkotlin/reflect/jvm/internal/impl/b/e;Lkotlin/reflect/jvm/internal/impl/h/i;)Ljava/util/Collection;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/reflect/jvm/internal/impl/b/b;",
            "Ljava/util/Collection",
            "<+",
            "Lkotlin/reflect/jvm/internal/impl/b/b;",
            ">;",
            "Lkotlin/reflect/jvm/internal/impl/b/e;",
            "Lkotlin/reflect/jvm/internal/impl/h/i;",
            ")",
            "Ljava/util/Collection",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/b/b;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    if-nez p0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "fromCurrent"

    aput-object v3, v2, v4

    const-string v3, "kotlin/reflect/jvm/internal/impl/resolve/OverridingUtil"

    aput-object v3, v2, v5

    const-string v3, "extractAndBindOverridesForMember"

    aput-object v3, v2, v6

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    if-nez p1, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "descriptorsFromSuper"

    aput-object v3, v2, v4

    const-string v3, "kotlin/reflect/jvm/internal/impl/resolve/OverridingUtil"

    aput-object v3, v2, v5

    const-string v3, "extractAndBindOverridesForMember"

    aput-object v3, v2, v6

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    if-nez p2, :cond_2

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "current"

    aput-object v3, v2, v4

    const-string v3, "kotlin/reflect/jvm/internal/impl/resolve/OverridingUtil"

    aput-object v3, v2, v5

    const-string v3, "extractAndBindOverridesForMember"

    aput-object v3, v2, v6

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    if-nez p3, :cond_3

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "strategy"

    aput-object v3, v2, v4

    const-string v3, "kotlin/reflect/jvm/internal/impl/resolve/OverridingUtil"

    aput-object v3, v2, v5

    const-string v3, "extractAndBindOverridesForMember"

    aput-object v3, v2, v6

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 429
    :cond_3
    new-instance v1, Ljava/util/ArrayList;

    invoke-interface {p1}, Ljava/util/Collection;->size()I

    move-result v0

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 430
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/utils/f;->c()Lkotlin/reflect/jvm/internal/impl/utils/f;

    move-result-object v2

    .line 431
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/b;

    .line 432
    sget-object v4, Lkotlin/reflect/jvm/internal/impl/h/j;->a:Lkotlin/reflect/jvm/internal/impl/h/j;

    invoke-virtual {v4, v0, p0, p2}, Lkotlin/reflect/jvm/internal/impl/h/j;->a(Lkotlin/reflect/jvm/internal/impl/b/a;Lkotlin/reflect/jvm/internal/impl/b/a;Lkotlin/reflect/jvm/internal/impl/b/e;)Lkotlin/reflect/jvm/internal/impl/h/j$a;

    move-result-object v4

    invoke-virtual {v4}, Lkotlin/reflect/jvm/internal/impl/h/j$a;->b()Lkotlin/reflect/jvm/internal/impl/h/j$a$a;

    move-result-object v4

    .line 434
    invoke-static {p0, v0}, Lkotlin/reflect/jvm/internal/impl/h/j;->a(Lkotlin/reflect/jvm/internal/impl/b/t;Lkotlin/reflect/jvm/internal/impl/b/t;)Z

    move-result v5

    .line 436
    sget-object v6, Lkotlin/reflect/jvm/internal/impl/h/j$8;->b:[I

    invoke-virtual {v4}, Lkotlin/reflect/jvm/internal/impl/h/j$a$a;->ordinal()I

    move-result v4

    aget v4, v6, v4

    packed-switch v4, :pswitch_data_0

    goto :goto_0

    .line 438
    :pswitch_0
    if-eqz v5, :cond_4

    .line 439
    invoke-interface {v2, v0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 441
    :cond_4
    invoke-interface {v1, v0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 444
    :pswitch_1
    if-eqz v5, :cond_5

    .line 445
    invoke-virtual {p3, v0, p0}, Lkotlin/reflect/jvm/internal/impl/h/i;->b(Lkotlin/reflect/jvm/internal/impl/b/b;Lkotlin/reflect/jvm/internal/impl/b/b;)V

    .line 447
    :cond_5
    invoke-interface {v1, v0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 454
    :cond_6
    invoke-virtual {p3, p0, v2}, Lkotlin/reflect/jvm/internal/impl/h/i;->a(Lkotlin/reflect/jvm/internal/impl/b/b;Ljava/util/Collection;)V

    .line 456
    return-object v1

    .line 436
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private static a(Lkotlin/reflect/jvm/internal/impl/b/b;Ljava/util/Queue;Lkotlin/reflect/jvm/internal/impl/h/i;)Ljava/util/Collection;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/reflect/jvm/internal/impl/b/b;",
            "Ljava/util/Queue",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/b/b;",
            ">;",
            "Lkotlin/reflect/jvm/internal/impl/h/i;",
            ")",
            "Ljava/util/Collection",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/b/b;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    if-nez p0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "overrider"

    aput-object v3, v2, v4

    const-string v3, "kotlin/reflect/jvm/internal/impl/resolve/OverridingUtil"

    aput-object v3, v2, v5

    const-string v3, "extractMembersOverridableInBothWays"

    aput-object v3, v2, v6

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    if-nez p1, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "extractFrom"

    aput-object v3, v2, v4

    const-string v3, "kotlin/reflect/jvm/internal/impl/resolve/OverridingUtil"

    aput-object v3, v2, v5

    const-string v3, "extractMembersOverridableInBothWays"

    aput-object v3, v2, v6

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    if-nez p2, :cond_2

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "strategy"

    aput-object v3, v2, v4

    const-string v3, "kotlin/reflect/jvm/internal/impl/resolve/OverridingUtil"

    aput-object v3, v2, v5

    const-string v3, "extractMembersOverridableInBothWays"

    aput-object v3, v2, v6

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 757
    :cond_2
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/h/j$6;

    invoke-direct {v0}, Lkotlin/reflect/jvm/internal/impl/h/j$6;-><init>()V

    new-instance v1, Lkotlin/reflect/jvm/internal/impl/h/j$7;

    invoke-direct {v1, p2, p0}, Lkotlin/reflect/jvm/internal/impl/h/j$7;-><init>(Lkotlin/reflect/jvm/internal/impl/h/i;Lkotlin/reflect/jvm/internal/impl/b/b;)V

    invoke-static {p0, p1, v0, v1}, Lkotlin/reflect/jvm/internal/impl/h/j;->a(Ljava/lang/Object;Ljava/util/Collection;Lkotlin/d/a/b;Lkotlin/d/a/b;)Ljava/util/Collection;

    move-result-object v0

    if-nez v0, :cond_3

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "@NotNull method %s.%s must not return null"

    new-array v2, v6, [Ljava/lang/Object;

    const-string v3, "kotlin/reflect/jvm/internal/impl/resolve/OverridingUtil"

    aput-object v3, v2, v4

    const-string v3, "extractMembersOverridableInBothWays"

    aput-object v3, v2, v5

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_3
    return-object v0
.end method

.method private static a(Lkotlin/reflect/jvm/internal/impl/b/e;Ljava/util/Collection;)Ljava/util/Collection;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/reflect/jvm/internal/impl/b/e;",
            "Ljava/util/Collection",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/b/b;",
            ">;)",
            "Ljava/util/Collection",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/b/b;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    if-nez p0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "current"

    aput-object v3, v2, v4

    const-string v3, "kotlin/reflect/jvm/internal/impl/resolve/OverridingUtil"

    aput-object v3, v2, v5

    const-string v3, "filterVisibleFakeOverrides"

    aput-object v3, v2, v6

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    if-nez p1, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "toFilter"

    aput-object v3, v2, v4

    const-string v3, "kotlin/reflect/jvm/internal/impl/resolve/OverridingUtil"

    aput-object v3, v2, v5

    const-string v3, "filterVisibleFakeOverrides"

    aput-object v3, v2, v6

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 692
    :cond_1
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/h/j$5;

    invoke-direct {v0, p0}, Lkotlin/reflect/jvm/internal/impl/h/j$5;-><init>(Lkotlin/reflect/jvm/internal/impl/b/e;)V

    invoke-static {p1, v0}, Lkotlin/a/m;->b(Ljava/lang/Iterable;Lkotlin/d/a/b;)Ljava/util/List;

    move-result-object v0

    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "@NotNull method %s.%s must not return null"

    new-array v2, v6, [Ljava/lang/Object;

    const-string v3, "kotlin/reflect/jvm/internal/impl/resolve/OverridingUtil"

    aput-object v3, v2, v4

    const-string v3, "filterVisibleFakeOverrides"

    aput-object v3, v2, v5

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    return-object v0
.end method

.method private static a(Lkotlin/reflect/jvm/internal/impl/b/a;)Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/reflect/jvm/internal/impl/b/a;",
            ")",
            "Ljava/util/List",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/k/r;",
            ">;"
        }
    .end annotation

    .prologue
    .line 388
    invoke-interface {p0}, Lkotlin/reflect/jvm/internal/impl/b/a;->d()Lkotlin/reflect/jvm/internal/impl/b/aj;

    move-result-object v0

    .line 389
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 390
    if-eqz v0, :cond_0

    .line 391
    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/b/aj;->s()Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 393
    :cond_0
    invoke-interface {p0}, Lkotlin/reflect/jvm/internal/impl/b/a;->i()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/at;

    .line 394
    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/b/at;->s()Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 396
    :cond_1
    return-object v1
.end method

.method public static a(Ljava/util/Set;)Ljava/util/Set;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<D::",
            "Lkotlin/reflect/jvm/internal/impl/b/a;",
            ">(",
            "Ljava/util/Set",
            "<TD;>;)",
            "Ljava/util/Set",
            "<TD;>;"
        }
    .end annotation

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    if-nez p0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "candidateSet"

    aput-object v3, v2, v4

    const-string v3, "kotlin/reflect/jvm/internal/impl/resolve/OverridingUtil"

    aput-object v3, v2, v5

    const-string v3, "filterOutOverridden"

    aput-object v3, v2, v6

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 74
    :cond_0
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/utils/d;->a()Lkotlin/d/a/b;

    move-result-object v0

    invoke-static {p0, v0}, Lkotlin/reflect/jvm/internal/impl/h/j;->a(Ljava/util/Set;Lkotlin/d/a/b;)Ljava/util/Set;

    move-result-object v0

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "@NotNull method %s.%s must not return null"

    new-array v2, v6, [Ljava/lang/Object;

    const-string v3, "kotlin/reflect/jvm/internal/impl/resolve/OverridingUtil"

    aput-object v3, v2, v4

    const-string v3, "filterOutOverridden"

    aput-object v3, v2, v5

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    return-object v0
.end method

.method public static a(Ljava/util/Set;Lkotlin/d/a/b;)Ljava/util/Set;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<D:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/Set",
            "<TD;>;",
            "Lkotlin/d/a/b",
            "<-TD;+",
            "Lkotlin/reflect/jvm/internal/impl/b/a;",
            ">;)",
            "Ljava/util/Set",
            "<TD;>;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x3

    const/4 v9, 0x2

    const/4 v8, 0x0

    const/4 v7, 0x1

    if-nez p0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "candidateSet"

    aput-object v3, v2, v8

    const-string v3, "kotlin/reflect/jvm/internal/impl/resolve/OverridingUtil"

    aput-object v3, v2, v7

    const-string v3, "filterOverrides"

    aput-object v3, v2, v9

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    if-nez p1, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "transform"

    aput-object v3, v2, v8

    const-string v3, "kotlin/reflect/jvm/internal/impl/resolve/OverridingUtil"

    aput-object v3, v2, v7

    const-string v3, "filterOverrides"

    aput-object v3, v2, v9

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 82
    :cond_1
    invoke-interface {p0}, Ljava/util/Set;->size()I

    move-result v0

    if-gt v0, v7, :cond_2

    if-nez p0, :cond_9

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "@NotNull method %s.%s must not return null"

    new-array v2, v9, [Ljava/lang/Object;

    const-string v3, "kotlin/reflect/jvm/internal/impl/resolve/OverridingUtil"

    aput-object v3, v2, v8

    const-string v3, "filterOverrides"

    aput-object v3, v2, v7

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 84
    :cond_2
    new-instance v2, Ljava/util/LinkedHashSet;

    invoke-direct {v2}, Ljava/util/LinkedHashSet;-><init>()V

    .line 86
    invoke-interface {p0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    .line 87
    invoke-interface {p1, v4}, Lkotlin/d/a/b;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/a;

    .line 88
    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_3
    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 89
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 90
    invoke-interface {p1, v1}, Lkotlin/d/a/b;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lkotlin/reflect/jvm/internal/impl/b/a;

    .line 91
    invoke-static {v0, v1}, Lkotlin/reflect/jvm/internal/impl/h/j;->a(Lkotlin/reflect/jvm/internal/impl/b/a;Lkotlin/reflect/jvm/internal/impl/b/a;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 92
    invoke-interface {v5}, Ljava/util/Iterator;->remove()V

    goto :goto_1

    .line 94
    :cond_4
    invoke-static {v1, v0}, Lkotlin/reflect/jvm/internal/impl/h/j;->a(Lkotlin/reflect/jvm/internal/impl/b/a;Lkotlin/reflect/jvm/internal/impl/b/a;)Z

    move-result v1

    if-eqz v1, :cond_3

    goto :goto_0

    .line 98
    :cond_5
    invoke-interface {v2, v4}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 101
    :cond_6
    sget-boolean v0, Lkotlin/reflect/jvm/internal/impl/h/j;->b:Z

    if-nez v0, :cond_7

    invoke-interface {v2}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_7

    new-instance v0, Ljava/lang/AssertionError;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "All candidates filtered out from "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 103
    :cond_7
    if-nez v2, :cond_8

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "@NotNull method %s.%s must not return null"

    new-array v2, v9, [Ljava/lang/Object;

    const-string v3, "kotlin/reflect/jvm/internal/impl/resolve/OverridingUtil"

    aput-object v3, v2, v8

    const-string v3, "filterOverrides"

    aput-object v3, v2, v7

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_8
    move-object p0, v2

    :cond_9
    return-object p0
.end method

.method public static a(Lkotlin/reflect/jvm/internal/impl/b/b;)Ljava/util/Set;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/reflect/jvm/internal/impl/b/b;",
            ")",
            "Ljava/util/Set",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/b/b;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    if-nez p0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "descriptor"

    aput-object v3, v2, v4

    const-string v3, "kotlin/reflect/jvm/internal/impl/resolve/OverridingUtil"

    aput-object v3, v2, v5

    const-string v3, "getOverriddenDeclarations"

    aput-object v3, v2, v6

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 133
    :cond_0
    new-instance v0, Ljava/util/LinkedHashSet;

    invoke-direct {v0}, Ljava/util/LinkedHashSet;-><init>()V

    .line 134
    invoke-static {p0, v0}, Lkotlin/reflect/jvm/internal/impl/h/j;->a(Lkotlin/reflect/jvm/internal/impl/b/b;Ljava/util/Set;)V

    .line 135
    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "@NotNull method %s.%s must not return null"

    new-array v2, v6, [Ljava/lang/Object;

    const-string v3, "kotlin/reflect/jvm/internal/impl/resolve/OverridingUtil"

    aput-object v3, v2, v4

    const-string v3, "getOverriddenDeclarations"

    aput-object v3, v2, v5

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    return-object v0
.end method

.method public static a(Ljava/util/Collection;)Lkotlin/reflect/jvm/internal/impl/b/ay;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<+",
            "Lkotlin/reflect/jvm/internal/impl/b/b;",
            ">;)",
            "Lkotlin/reflect/jvm/internal/impl/b/ay;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    if-nez p0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "descriptors"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "kotlin/reflect/jvm/internal/impl/resolve/OverridingUtil"

    aput-object v4, v2, v3

    const/4 v3, 0x2

    const-string v4, "findMaxVisibility"

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 838
    :cond_0
    invoke-interface {p0}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 839
    sget-object v2, Lkotlin/reflect/jvm/internal/impl/b/ax;->k:Lkotlin/reflect/jvm/internal/impl/b/ay;

    .line 866
    :cond_1
    :goto_0
    return-object v2

    .line 842
    :cond_2
    invoke-interface {p0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move-object v1, v2

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/b;

    .line 843
    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/b/b;->p()Lkotlin/reflect/jvm/internal/impl/b/ay;

    move-result-object v3

    .line 844
    sget-boolean v5, Lkotlin/reflect/jvm/internal/impl/h/j;->b:Z

    if-nez v5, :cond_3

    sget-object v5, Lkotlin/reflect/jvm/internal/impl/b/ax;->g:Lkotlin/reflect/jvm/internal/impl/b/ay;

    if-ne v3, v5, :cond_3

    new-instance v1, Ljava/lang/AssertionError;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Visibility should have been computed for "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v1

    .line 845
    :cond_3
    if-nez v1, :cond_4

    move-object v1, v3

    .line 847
    goto :goto_1

    .line 849
    :cond_4
    invoke-static {v3, v1}, Lkotlin/reflect/jvm/internal/impl/b/ax;->b(Lkotlin/reflect/jvm/internal/impl/b/ay;Lkotlin/reflect/jvm/internal/impl/b/ay;)Ljava/lang/Integer;

    move-result-object v0

    .line 850
    if-nez v0, :cond_5

    move-object v0, v2

    :goto_2
    move-object v1, v0

    .line 856
    goto :goto_1

    .line 853
    :cond_5
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-lez v0, :cond_9

    move-object v0, v3

    .line 854
    goto :goto_2

    .line 857
    :cond_6
    if-eqz v1, :cond_1

    .line 860
    invoke-interface {p0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_7
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/b;

    .line 861
    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/b/b;->p()Lkotlin/reflect/jvm/internal/impl/b/ay;

    move-result-object v0

    invoke-static {v1, v0}, Lkotlin/reflect/jvm/internal/impl/b/ax;->b(Lkotlin/reflect/jvm/internal/impl/b/ay;Lkotlin/reflect/jvm/internal/impl/b/ay;)Ljava/lang/Integer;

    move-result-object v0

    .line 862
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-gez v0, :cond_7

    goto :goto_0

    :cond_8
    move-object v2, v1

    .line 866
    goto :goto_0

    :cond_9
    move-object v0, v1

    goto :goto_2
.end method

.method public static a(Lkotlin/reflect/jvm/internal/impl/k/a/c$a;)Lkotlin/reflect/jvm/internal/impl/h/j;
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    if-nez p0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "equalityAxioms"

    aput-object v3, v2, v4

    const-string v3, "kotlin/reflect/jvm/internal/impl/resolve/OverridingUtil"

    aput-object v3, v2, v5

    const-string v3, "createWithEqualityAxioms"

    aput-object v3, v2, v6

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 59
    :cond_0
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/h/j;

    invoke-direct {v0, p0}, Lkotlin/reflect/jvm/internal/impl/h/j;-><init>(Lkotlin/reflect/jvm/internal/impl/k/a/c$a;)V

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "@NotNull method %s.%s must not return null"

    new-array v2, v6, [Ljava/lang/Object;

    const-string v3, "kotlin/reflect/jvm/internal/impl/resolve/OverridingUtil"

    aput-object v3, v2, v4

    const-string v3, "createWithEqualityAxioms"

    aput-object v3, v2, v5

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    return-object v0
.end method

.method static synthetic a(Lkotlin/reflect/jvm/internal/impl/h/j;)Lkotlin/reflect/jvm/internal/impl/k/a/c$a;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/h/j;->d:Lkotlin/reflect/jvm/internal/impl/k/a/c$a;

    return-object v0
.end method

.method private a(Ljava/util/List;Ljava/util/List;)Lkotlin/reflect/jvm/internal/impl/k/a/c;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/b/aq;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/b/aq;",
            ">;)",
            "Lkotlin/reflect/jvm/internal/impl/k/a/c;"
        }
    .end annotation

    .prologue
    const/4 v3, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v2, 0x0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    new-array v3, v3, [Ljava/lang/Object;

    const-string v4, "firstParameters"

    aput-object v4, v3, v2

    const-string v2, "kotlin/reflect/jvm/internal/impl/resolve/OverridingUtil"

    aput-object v2, v3, v5

    const-string v2, "createTypeChecker"

    aput-object v2, v3, v6

    invoke-static {v1, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    if-nez p2, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    new-array v3, v3, [Ljava/lang/Object;

    const-string v4, "secondParameters"

    aput-object v4, v3, v2

    const-string v2, "kotlin/reflect/jvm/internal/impl/resolve/OverridingUtil"

    aput-object v2, v3, v5

    const-string v2, "createTypeChecker"

    aput-object v2, v3, v6

    invoke-static {v1, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 316
    :cond_1
    sget-boolean v0, Lkotlin/reflect/jvm/internal/impl/h/j;->b:Z

    if-nez v0, :cond_2

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v1

    if-eq v0, v1, :cond_2

    new-instance v0, Ljava/lang/AssertionError;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Should be the same number of type parameters: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " vs "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 317
    :cond_2
    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/h/j;->d:Lkotlin/reflect/jvm/internal/impl/k/a/c$a;

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/k/a/d;->a(Lkotlin/reflect/jvm/internal/impl/k/a/c$a;)Lkotlin/reflect/jvm/internal/impl/k/a/c;

    move-result-object v0

    if-nez v0, :cond_5

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "@NotNull method %s.%s must not return null"

    new-array v3, v6, [Ljava/lang/Object;

    const-string v4, "kotlin/reflect/jvm/internal/impl/resolve/OverridingUtil"

    aput-object v4, v3, v2

    const-string v2, "createTypeChecker"

    aput-object v2, v3, v5

    invoke-static {v1, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 319
    :cond_3
    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    move v1, v2

    .line 320
    :goto_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_4

    .line 321
    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/aq;

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/b/aq;->e()Lkotlin/reflect/jvm/internal/impl/k/ad;

    move-result-object v4

    invoke-interface {p2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/aq;

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/b/aq;->e()Lkotlin/reflect/jvm/internal/impl/k/ad;

    move-result-object v0

    invoke-interface {v3, v4, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 320
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 324
    :cond_4
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/h/j$2;

    invoke-direct {v0, p0, v3}, Lkotlin/reflect/jvm/internal/impl/h/j$2;-><init>(Lkotlin/reflect/jvm/internal/impl/h/j;Ljava/util/Map;)V

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/k/a/d;->a(Lkotlin/reflect/jvm/internal/impl/k/a/c$a;)Lkotlin/reflect/jvm/internal/impl/k/a/c;

    move-result-object v0

    if-nez v0, :cond_5

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "@NotNull method %s.%s must not return null"

    new-array v3, v6, [Ljava/lang/Object;

    const-string v4, "kotlin/reflect/jvm/internal/impl/resolve/OverridingUtil"

    aput-object v4, v3, v2

    const-string v2, "createTypeChecker"

    aput-object v2, v3, v5

    invoke-static {v1, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_5
    return-object v0
.end method

.method private static a(Ljava/util/Collection;Lkotlin/reflect/jvm/internal/impl/b/e;Lkotlin/reflect/jvm/internal/impl/h/i;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/b/b;",
            ">;",
            "Lkotlin/reflect/jvm/internal/impl/b/e;",
            "Lkotlin/reflect/jvm/internal/impl/h/i;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x3

    const/4 v6, 0x2

    const/4 v4, 0x1

    const/4 v5, 0x0

    if-nez p0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "overridables"

    aput-object v3, v2, v5

    const-string v3, "kotlin/reflect/jvm/internal/impl/resolve/OverridingUtil"

    aput-object v3, v2, v4

    const-string v3, "createAndBindFakeOverride"

    aput-object v3, v2, v6

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    if-nez p1, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "current"

    aput-object v3, v2, v5

    const-string v3, "kotlin/reflect/jvm/internal/impl/resolve/OverridingUtil"

    aput-object v3, v2, v4

    const-string v3, "createAndBindFakeOverride"

    aput-object v3, v2, v6

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    if-nez p2, :cond_2

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "strategy"

    aput-object v3, v2, v5

    const-string v3, "kotlin/reflect/jvm/internal/impl/resolve/OverridingUtil"

    aput-object v3, v2, v4

    const-string v3, "createAndBindFakeOverride"

    aput-object v3, v2, v6

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 616
    :cond_2
    invoke-static {p1, p0}, Lkotlin/reflect/jvm/internal/impl/h/j;->a(Lkotlin/reflect/jvm/internal/impl/b/e;Ljava/util/Collection;)Ljava/util/Collection;

    move-result-object v0

    .line 617
    invoke-interface {v0}, Ljava/util/Collection;->isEmpty()Z

    move-result v1

    .line 618
    if-eqz v1, :cond_3

    .line 620
    :goto_0
    invoke-static {p0}, Lkotlin/reflect/jvm/internal/impl/h/j;->c(Ljava/util/Collection;)Lkotlin/reflect/jvm/internal/impl/b/u;

    move-result-object v2

    .line 621
    if-eqz v1, :cond_4

    sget-object v3, Lkotlin/reflect/jvm/internal/impl/b/ax;->h:Lkotlin/reflect/jvm/internal/impl/b/ay;

    .line 630
    :goto_1
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/h/j$4;

    invoke-direct {v0}, Lkotlin/reflect/jvm/internal/impl/h/j$4;-><init>()V

    invoke-static {p0, v0}, Lkotlin/reflect/jvm/internal/impl/h/j;->a(Ljava/util/Collection;Lkotlin/d/a/b;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/b;

    .line 638
    sget-object v4, Lkotlin/reflect/jvm/internal/impl/b/b$a;->b:Lkotlin/reflect/jvm/internal/impl/b/b$a;

    move-object v1, p1

    invoke-interface/range {v0 .. v5}, Lkotlin/reflect/jvm/internal/impl/b/b;->a(Lkotlin/reflect/jvm/internal/impl/b/m;Lkotlin/reflect/jvm/internal/impl/b/u;Lkotlin/reflect/jvm/internal/impl/b/ay;Lkotlin/reflect/jvm/internal/impl/b/b$a;Z)Lkotlin/reflect/jvm/internal/impl/b/b;

    move-result-object v0

    .line 640
    invoke-virtual {p2, v0, p0}, Lkotlin/reflect/jvm/internal/impl/h/i;->a(Lkotlin/reflect/jvm/internal/impl/b/b;Ljava/util/Collection;)V

    .line 642
    sget-boolean v1, Lkotlin/reflect/jvm/internal/impl/h/j;->b:Z

    if-nez v1, :cond_5

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/b/b;->k()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_5

    new-instance v0, Ljava/lang/AssertionError;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Overridden descriptors should be set for "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lkotlin/reflect/jvm/internal/impl/b/b$a;->b:Lkotlin/reflect/jvm/internal/impl/b/b$a;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    :cond_3
    move-object p0, v0

    .line 618
    goto :goto_0

    .line 621
    :cond_4
    sget-object v3, Lkotlin/reflect/jvm/internal/impl/b/ax;->g:Lkotlin/reflect/jvm/internal/impl/b/ay;

    goto :goto_1

    .line 643
    :cond_5
    invoke-virtual {p2, v0}, Lkotlin/reflect/jvm/internal/impl/h/i;->a(Lkotlin/reflect/jvm/internal/impl/b/b;)V

    .line 644
    return-void
.end method

.method private static a(Lkotlin/reflect/jvm/internal/impl/b/b;Ljava/util/Set;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/reflect/jvm/internal/impl/b/b;",
            "Ljava/util/Set",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/b/b;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    if-nez p0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "descriptor"

    aput-object v3, v2, v4

    const-string v3, "kotlin/reflect/jvm/internal/impl/resolve/OverridingUtil"

    aput-object v3, v2, v5

    const-string v3, "collectOverriddenDeclarations"

    aput-object v3, v2, v6

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    if-nez p1, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "result"

    aput-object v3, v2, v4

    const-string v3, "kotlin/reflect/jvm/internal/impl/resolve/OverridingUtil"

    aput-object v3, v2, v5

    const-string v3, "collectOverriddenDeclarations"

    aput-object v3, v2, v6

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 142
    :cond_1
    invoke-interface {p0}, Lkotlin/reflect/jvm/internal/impl/b/b;->n()Lkotlin/reflect/jvm/internal/impl/b/b$a;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/b/b$a;->a()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 143
    invoke-interface {p1, p0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 153
    :cond_2
    return-void

    .line 146
    :cond_3
    invoke-interface {p0}, Lkotlin/reflect/jvm/internal/impl/b/b;->k()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 147
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "No overridden descriptors found for (fake override) "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 149
    :cond_4
    invoke-interface {p0}, Lkotlin/reflect/jvm/internal/impl/b/b;->k()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/b;

    .line 150
    invoke-static {v0, p1}, Lkotlin/reflect/jvm/internal/impl/h/j;->a(Lkotlin/reflect/jvm/internal/impl/b/b;Ljava/util/Set;)V

    goto :goto_0
.end method

.method public static a(Lkotlin/reflect/jvm/internal/impl/b/b;Lkotlin/d/a/b;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/reflect/jvm/internal/impl/b/b;",
            "Lkotlin/d/a/b",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/b/b;",
            "Lkotlin/n;",
            ">;)V"
        }
    .end annotation

    .prologue
    if-nez p0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "memberDescriptor"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "kotlin/reflect/jvm/internal/impl/resolve/OverridingUtil"

    aput-object v4, v2, v3

    const/4 v3, 0x2

    const-string v4, "resolveUnknownVisibilityForMember"

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 779
    :cond_0
    invoke-interface {p0}, Lkotlin/reflect/jvm/internal/impl/b/b;->k()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/b;

    .line 780
    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/b/b;->p()Lkotlin/reflect/jvm/internal/impl/b/ay;

    move-result-object v2

    sget-object v3, Lkotlin/reflect/jvm/internal/impl/b/ax;->g:Lkotlin/reflect/jvm/internal/impl/b/ay;

    if-ne v2, v3, :cond_1

    .line 781
    invoke-static {v0, p1}, Lkotlin/reflect/jvm/internal/impl/h/j;->a(Lkotlin/reflect/jvm/internal/impl/b/b;Lkotlin/d/a/b;)V

    goto :goto_0

    .line 785
    :cond_2
    invoke-interface {p0}, Lkotlin/reflect/jvm/internal/impl/b/b;->p()Lkotlin/reflect/jvm/internal/impl/b/ay;

    move-result-object v0

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/b/ax;->g:Lkotlin/reflect/jvm/internal/impl/b/ay;

    if-eq v0, v1, :cond_4

    .line 815
    :cond_3
    :goto_1
    return-void

    .line 789
    :cond_4
    invoke-static {p0}, Lkotlin/reflect/jvm/internal/impl/h/j;->b(Lkotlin/reflect/jvm/internal/impl/b/b;)Lkotlin/reflect/jvm/internal/impl/b/ay;

    move-result-object v2

    .line 791
    if-nez v2, :cond_6

    .line 792
    if-eqz p1, :cond_5

    .line 793
    invoke-interface {p1, p0}, Lkotlin/d/a/b;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 795
    :cond_5
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/b/ax;->e:Lkotlin/reflect/jvm/internal/impl/b/ay;

    move-object v1, v0

    .line 801
    :goto_2
    instance-of v0, p0, Lkotlin/reflect/jvm/internal/impl/b/b/y;

    if-eqz v0, :cond_8

    move-object v0, p0

    .line 802
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/b/y;

    invoke-virtual {v0, v1}, Lkotlin/reflect/jvm/internal/impl/b/b/y;->a(Lkotlin/reflect/jvm/internal/impl/b/ay;)V

    .line 803
    check-cast p0, Lkotlin/reflect/jvm/internal/impl/b/ag;

    invoke-interface {p0}, Lkotlin/reflect/jvm/internal/impl/b/ag;->o()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_3
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/af;

    .line 805
    if-nez v2, :cond_7

    const/4 v1, 0x0

    :goto_4
    invoke-static {v0, v1}, Lkotlin/reflect/jvm/internal/impl/h/j;->a(Lkotlin/reflect/jvm/internal/impl/b/b;Lkotlin/d/a/b;)V

    goto :goto_3

    :cond_6
    move-object v1, v2

    .line 798
    goto :goto_2

    :cond_7
    move-object v1, p1

    .line 805
    goto :goto_4

    .line 808
    :cond_8
    instance-of v0, p0, Lkotlin/reflect/jvm/internal/impl/b/b/o;

    if-eqz v0, :cond_9

    .line 809
    check-cast p0, Lkotlin/reflect/jvm/internal/impl/b/b/o;

    invoke-virtual {p0, v1}, Lkotlin/reflect/jvm/internal/impl/b/b/o;->a(Lkotlin/reflect/jvm/internal/impl/b/ay;)V

    goto :goto_1

    .line 812
    :cond_9
    sget-boolean v0, Lkotlin/reflect/jvm/internal/impl/h/j;->b:Z

    if-nez v0, :cond_a

    instance-of v0, p0, Lkotlin/reflect/jvm/internal/impl/b/b/x;

    if-nez v0, :cond_a

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 813
    :cond_a
    check-cast p0, Lkotlin/reflect/jvm/internal/impl/b/b/x;

    invoke-virtual {p0, v1}, Lkotlin/reflect/jvm/internal/impl/b/b/x;->a(Lkotlin/reflect/jvm/internal/impl/b/ay;)V

    goto :goto_1
.end method

.method private static a(Lkotlin/reflect/jvm/internal/impl/b/e;Ljava/util/Collection;Lkotlin/reflect/jvm/internal/impl/h/i;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/reflect/jvm/internal/impl/b/e;",
            "Ljava/util/Collection",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/b/b;",
            ">;",
            "Lkotlin/reflect/jvm/internal/impl/h/i;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    if-nez p0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "current"

    aput-object v3, v2, v4

    const-string v3, "kotlin/reflect/jvm/internal/impl/resolve/OverridingUtil"

    aput-object v3, v2, v5

    const-string v3, "createAndBindFakeOverrides"

    aput-object v3, v2, v6

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    if-nez p1, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "notOverridden"

    aput-object v3, v2, v4

    const-string v3, "kotlin/reflect/jvm/internal/impl/resolve/OverridingUtil"

    aput-object v3, v2, v5

    const-string v3, "createAndBindFakeOverrides"

    aput-object v3, v2, v6

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    if-nez p2, :cond_2

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "strategy"

    aput-object v3, v2, v4

    const-string v3, "kotlin/reflect/jvm/internal/impl/resolve/OverridingUtil"

    aput-object v3, v2, v5

    const-string v3, "createAndBindFakeOverrides"

    aput-object v3, v2, v6

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 478
    :cond_2
    invoke-static {p1}, Lkotlin/reflect/jvm/internal/impl/h/j;->b(Ljava/util/Collection;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 479
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/b;

    .line 480
    invoke-static {v0}, Ljava/util/Collections;->singleton(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v0

    invoke-static {v0, p0, p2}, Lkotlin/reflect/jvm/internal/impl/h/j;->a(Ljava/util/Collection;Lkotlin/reflect/jvm/internal/impl/b/e;Lkotlin/reflect/jvm/internal/impl/h/i;)V

    goto :goto_0

    .line 485
    :cond_3
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0, p1}, Ljava/util/LinkedList;-><init>(Ljava/util/Collection;)V

    .line 486
    :goto_1
    invoke-interface {v0}, Ljava/util/Queue;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_4

    .line 487
    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/h/l;->a(Ljava/util/Collection;)Lkotlin/reflect/jvm/internal/impl/b/b;

    move-result-object v1

    .line 488
    invoke-static {v1, v0, p2}, Lkotlin/reflect/jvm/internal/impl/h/j;->a(Lkotlin/reflect/jvm/internal/impl/b/b;Ljava/util/Queue;Lkotlin/reflect/jvm/internal/impl/h/i;)Ljava/util/Collection;

    move-result-object v1

    .line 490
    invoke-static {v1, p0, p2}, Lkotlin/reflect/jvm/internal/impl/h/j;->a(Ljava/util/Collection;Lkotlin/reflect/jvm/internal/impl/b/e;Lkotlin/reflect/jvm/internal/impl/h/i;)V

    goto :goto_1

    .line 492
    :cond_4
    return-void
.end method

.method public static a(Lkotlin/reflect/jvm/internal/impl/e/f;Ljava/util/Collection;Ljava/util/Collection;Lkotlin/reflect/jvm/internal/impl/b/e;Lkotlin/reflect/jvm/internal/impl/h/i;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/reflect/jvm/internal/impl/e/f;",
            "Ljava/util/Collection",
            "<+",
            "Lkotlin/reflect/jvm/internal/impl/b/b;",
            ">;",
            "Ljava/util/Collection",
            "<+",
            "Lkotlin/reflect/jvm/internal/impl/b/b;",
            ">;",
            "Lkotlin/reflect/jvm/internal/impl/b/e;",
            "Lkotlin/reflect/jvm/internal/impl/h/i;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    if-nez p0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "name"

    aput-object v3, v2, v4

    const-string v3, "kotlin/reflect/jvm/internal/impl/resolve/OverridingUtil"

    aput-object v3, v2, v5

    const-string v3, "generateOverridesInFunctionGroup"

    aput-object v3, v2, v6

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    if-nez p1, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "membersFromSupertypes"

    aput-object v3, v2, v4

    const-string v3, "kotlin/reflect/jvm/internal/impl/resolve/OverridingUtil"

    aput-object v3, v2, v5

    const-string v3, "generateOverridesInFunctionGroup"

    aput-object v3, v2, v6

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    if-nez p2, :cond_2

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "membersFromCurrent"

    aput-object v3, v2, v4

    const-string v3, "kotlin/reflect/jvm/internal/impl/resolve/OverridingUtil"

    aput-object v3, v2, v5

    const-string v3, "generateOverridesInFunctionGroup"

    aput-object v3, v2, v6

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    if-nez p3, :cond_3

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "current"

    aput-object v3, v2, v4

    const-string v3, "kotlin/reflect/jvm/internal/impl/resolve/OverridingUtil"

    aput-object v3, v2, v5

    const-string v3, "generateOverridesInFunctionGroup"

    aput-object v3, v2, v6

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_3
    if-nez p4, :cond_4

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "strategy"

    aput-object v3, v2, v4

    const-string v3, "kotlin/reflect/jvm/internal/impl/resolve/OverridingUtil"

    aput-object v3, v2, v5

    const-string v3, "generateOverridesInFunctionGroup"

    aput-object v3, v2, v6

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 407
    :cond_4
    new-instance v1, Ljava/util/LinkedHashSet;

    invoke-direct {v1, p1}, Ljava/util/LinkedHashSet;-><init>(Ljava/util/Collection;)V

    .line 409
    invoke-interface {p2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/b;

    .line 410
    invoke-static {v0, p1, p3, p4}, Lkotlin/reflect/jvm/internal/impl/h/j;->a(Lkotlin/reflect/jvm/internal/impl/b/b;Ljava/util/Collection;Lkotlin/reflect/jvm/internal/impl/b/e;Lkotlin/reflect/jvm/internal/impl/h/i;)Ljava/util/Collection;

    move-result-object v0

    .line 412
    invoke-interface {v1, v0}, Ljava/util/Collection;->removeAll(Ljava/util/Collection;)Z

    goto :goto_0

    .line 415
    :cond_5
    invoke-static {p3, v1, p4}, Lkotlin/reflect/jvm/internal/impl/h/j;->a(Lkotlin/reflect/jvm/internal/impl/b/e;Ljava/util/Collection;Lkotlin/reflect/jvm/internal/impl/h/i;)V

    .line 416
    return-void
.end method

.method private static a(Lkotlin/reflect/jvm/internal/impl/b/a;Ljava/util/Collection;)Z
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/reflect/jvm/internal/impl/b/a;",
            "Ljava/util/Collection",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/b/a;",
            ">;)Z"
        }
    .end annotation

    .prologue
    const/4 v4, 0x3

    const/4 v6, 0x2

    const/4 v2, 0x1

    const/4 v1, 0x0

    if-nez p0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v3, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    new-array v4, v4, [Ljava/lang/Object;

    const-string v5, "candidate"

    aput-object v5, v4, v1

    const-string v1, "kotlin/reflect/jvm/internal/impl/resolve/OverridingUtil"

    aput-object v1, v4, v2

    const-string v1, "isMoreSpecificThenAllOf"

    aput-object v1, v4, v6

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    if-nez p1, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v3, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    new-array v4, v4, [Ljava/lang/Object;

    const-string v5, "descriptors"

    aput-object v5, v4, v1

    const-string v1, "kotlin/reflect/jvm/internal/impl/resolve/OverridingUtil"

    aput-object v1, v4, v2

    const-string v1, "isMoreSpecificThenAllOf"

    aput-object v1, v4, v6

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 543
    :cond_1
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/a;

    .line 544
    invoke-static {p0, v0}, Lkotlin/reflect/jvm/internal/impl/h/j;->c(Lkotlin/reflect/jvm/internal/impl/b/a;Lkotlin/reflect/jvm/internal/impl/b/a;)Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    .line 548
    :goto_0
    return v0

    :cond_3
    move v0, v2

    goto :goto_0
.end method

.method public static a(Lkotlin/reflect/jvm/internal/impl/b/a;Lkotlin/reflect/jvm/internal/impl/b/a;)Z
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<D::",
            "Lkotlin/reflect/jvm/internal/impl/b/a;",
            ">(TD;TD;)Z"
        }
    .end annotation

    .prologue
    const/4 v4, 0x3

    const/4 v6, 0x2

    const/4 v2, 0x0

    const/4 v1, 0x1

    if-nez p0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v3, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    new-array v4, v4, [Ljava/lang/Object;

    const-string v5, "f"

    aput-object v5, v4, v2

    const-string v2, "kotlin/reflect/jvm/internal/impl/resolve/OverridingUtil"

    aput-object v2, v4, v1

    const-string v1, "overrides"

    aput-object v1, v4, v6

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    if-nez p1, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v3, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    new-array v4, v4, [Ljava/lang/Object;

    const-string v5, "g"

    aput-object v5, v4, v2

    const-string v2, "kotlin/reflect/jvm/internal/impl/resolve/OverridingUtil"

    aput-object v2, v4, v1

    const-string v1, "overrides"

    aput-object v1, v4, v6

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 118
    :cond_1
    invoke-virtual {p0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    sget-object v0, Lkotlin/reflect/jvm/internal/impl/h/a;->a:Lkotlin/reflect/jvm/internal/impl/h/a;

    invoke-interface {p0}, Lkotlin/reflect/jvm/internal/impl/b/a;->h()Lkotlin/reflect/jvm/internal/impl/b/a;

    move-result-object v3

    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/b/a;->h()Lkotlin/reflect/jvm/internal/impl/b/a;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Lkotlin/reflect/jvm/internal/impl/h/a;->a(Lkotlin/reflect/jvm/internal/impl/b/m;Lkotlin/reflect/jvm/internal/impl/b/m;)Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v1

    .line 124
    :goto_0
    return v0

    .line 120
    :cond_2
    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/b/a;->h()Lkotlin/reflect/jvm/internal/impl/b/a;

    move-result-object v3

    .line 121
    invoke-static {p0}, Lkotlin/reflect/jvm/internal/impl/h/c;->a(Lkotlin/reflect/jvm/internal/impl/b/a;)Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_3
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/a;

    .line 122
    sget-object v5, Lkotlin/reflect/jvm/internal/impl/h/a;->a:Lkotlin/reflect/jvm/internal/impl/h/a;

    invoke-virtual {v5, v3, v0}, Lkotlin/reflect/jvm/internal/impl/h/a;->a(Lkotlin/reflect/jvm/internal/impl/b/m;Lkotlin/reflect/jvm/internal/impl/b/m;)Z

    move-result v0

    if-eqz v0, :cond_3

    move v0, v1

    goto :goto_0

    :cond_4
    move v0, v2

    .line 124
    goto :goto_0
.end method

.method private static a(Lkotlin/reflect/jvm/internal/impl/b/a;Lkotlin/reflect/jvm/internal/impl/k/r;Lkotlin/reflect/jvm/internal/impl/b/a;Lkotlin/reflect/jvm/internal/impl/k/r;)Z
    .locals 7

    .prologue
    const/4 v2, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    if-nez p0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "a"

    aput-object v3, v2, v4

    const-string v3, "kotlin/reflect/jvm/internal/impl/resolve/OverridingUtil"

    aput-object v3, v2, v5

    const-string v3, "isReturnTypeMoreSpecific"

    aput-object v3, v2, v6

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    if-nez p1, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "aReturnType"

    aput-object v3, v2, v4

    const-string v3, "kotlin/reflect/jvm/internal/impl/resolve/OverridingUtil"

    aput-object v3, v2, v5

    const-string v3, "isReturnTypeMoreSpecific"

    aput-object v3, v2, v6

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    if-nez p2, :cond_2

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "b"

    aput-object v3, v2, v4

    const-string v3, "kotlin/reflect/jvm/internal/impl/resolve/OverridingUtil"

    aput-object v3, v2, v5

    const-string v3, "isReturnTypeMoreSpecific"

    aput-object v3, v2, v6

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    if-nez p3, :cond_3

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "bReturnType"

    aput-object v3, v2, v4

    const-string v3, "kotlin/reflect/jvm/internal/impl/resolve/OverridingUtil"

    aput-object v3, v2, v5

    const-string v3, "isReturnTypeMoreSpecific"

    aput-object v3, v2, v6

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 557
    :cond_3
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/h/j;->a:Lkotlin/reflect/jvm/internal/impl/h/j;

    invoke-interface {p0}, Lkotlin/reflect/jvm/internal/impl/b/a;->f()Ljava/util/List;

    move-result-object v1

    invoke-interface {p2}, Lkotlin/reflect/jvm/internal/impl/b/a;->f()Ljava/util/List;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lkotlin/reflect/jvm/internal/impl/h/j;->a(Ljava/util/List;Ljava/util/List;)Lkotlin/reflect/jvm/internal/impl/k/a/c;

    move-result-object v0

    .line 558
    invoke-interface {v0, p1, p3}, Lkotlin/reflect/jvm/internal/impl/k/a/c;->a(Lkotlin/reflect/jvm/internal/impl/k/r;Lkotlin/reflect/jvm/internal/impl/k/r;)Z

    move-result v0

    return v0
.end method

.method private static a(Lkotlin/reflect/jvm/internal/impl/b/af;Lkotlin/reflect/jvm/internal/impl/b/af;)Z
    .locals 1

    .prologue
    .line 536
    if-eqz p0, :cond_0

    if-nez p1, :cond_1

    :cond_0
    const/4 v0, 0x1

    .line 537
    :goto_0
    return v0

    :cond_1
    invoke-static {p0, p1}, Lkotlin/reflect/jvm/internal/impl/h/j;->a(Lkotlin/reflect/jvm/internal/impl/b/q;Lkotlin/reflect/jvm/internal/impl/b/q;)Z

    move-result v0

    goto :goto_0
.end method

.method private static a(Lkotlin/reflect/jvm/internal/impl/b/aq;Lkotlin/reflect/jvm/internal/impl/b/aq;Lkotlin/reflect/jvm/internal/impl/k/a/c;)Z
    .locals 7

    .prologue
    const/4 v4, 0x3

    const/4 v6, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    if-nez p0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    new-array v4, v4, [Ljava/lang/Object;

    const-string v5, "superTypeParameter"

    aput-object v5, v4, v2

    const-string v2, "kotlin/reflect/jvm/internal/impl/resolve/OverridingUtil"

    aput-object v2, v4, v3

    const-string v2, "areTypeParametersEquivalent"

    aput-object v2, v4, v6

    invoke-static {v1, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    if-nez p1, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    new-array v4, v4, [Ljava/lang/Object;

    const-string v5, "subTypeParameter"

    aput-object v5, v4, v2

    const-string v2, "kotlin/reflect/jvm/internal/impl/resolve/OverridingUtil"

    aput-object v2, v4, v3

    const-string v2, "areTypeParametersEquivalent"

    aput-object v2, v4, v6

    invoke-static {v1, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    if-nez p2, :cond_2

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    new-array v4, v4, [Ljava/lang/Object;

    const-string v5, "typeChecker"

    aput-object v5, v4, v2

    const-string v2, "kotlin/reflect/jvm/internal/impl/resolve/OverridingUtil"

    aput-object v2, v4, v3

    const-string v2, "areTypeParametersEquivalent"

    aput-object v2, v4, v6

    invoke-static {v1, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 367
    :cond_2
    invoke-interface {p0}, Lkotlin/reflect/jvm/internal/impl/b/aq;->I_()Ljava/util/List;

    move-result-object v0

    .line 368
    new-instance v4, Ljava/util/ArrayList;

    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/b/aq;->I_()Ljava/util/List;

    move-result-object v1

    invoke-direct {v4, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 369
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v5

    if-eq v1, v5, :cond_3

    move v0, v2

    .line 384
    :goto_0
    return v0

    .line 372
    :cond_3
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/r;

    .line 373
    invoke-interface {v4}, Ljava/util/List;->listIterator()Ljava/util/ListIterator;

    move-result-object v6

    .line 374
    :cond_4
    invoke-interface {v6}, Ljava/util/ListIterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 375
    invoke-interface {v6}, Ljava/util/ListIterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lkotlin/reflect/jvm/internal/impl/k/r;

    .line 376
    invoke-static {v0, v1, p2}, Lkotlin/reflect/jvm/internal/impl/h/j;->a(Lkotlin/reflect/jvm/internal/impl/k/r;Lkotlin/reflect/jvm/internal/impl/k/r;Lkotlin/reflect/jvm/internal/impl/k/a/c;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 377
    invoke-interface {v6}, Ljava/util/ListIterator;->remove()V

    goto :goto_1

    :cond_5
    move v0, v2

    .line 381
    goto :goto_0

    :cond_6
    move v0, v3

    .line 384
    goto :goto_0
.end method

.method private static a(Lkotlin/reflect/jvm/internal/impl/b/q;Lkotlin/reflect/jvm/internal/impl/b/q;)Z
    .locals 7

    .prologue
    const/4 v4, 0x3

    const/4 v6, 0x2

    const/4 v1, 0x1

    const/4 v0, 0x0

    if-nez p0, :cond_0

    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    new-array v4, v4, [Ljava/lang/Object;

    const-string v5, "a"

    aput-object v5, v4, v0

    const-string v0, "kotlin/reflect/jvm/internal/impl/resolve/OverridingUtil"

    aput-object v0, v4, v1

    const-string v0, "isVisibilityMoreSpecific"

    aput-object v0, v4, v6

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_0
    if-nez p1, :cond_1

    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    new-array v4, v4, [Ljava/lang/Object;

    const-string v5, "b"

    aput-object v5, v4, v0

    const-string v0, "kotlin/reflect/jvm/internal/impl/resolve/OverridingUtil"

    aput-object v0, v4, v1

    const-string v0, "isVisibilityMoreSpecific"

    aput-object v0, v4, v6

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 531
    :cond_1
    invoke-interface {p0}, Lkotlin/reflect/jvm/internal/impl/b/q;->p()Lkotlin/reflect/jvm/internal/impl/b/ay;

    move-result-object v2

    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/b/q;->p()Lkotlin/reflect/jvm/internal/impl/b/ay;

    move-result-object v3

    invoke-static {v2, v3}, Lkotlin/reflect/jvm/internal/impl/b/ax;->b(Lkotlin/reflect/jvm/internal/impl/b/ay;Lkotlin/reflect/jvm/internal/impl/b/ay;)Ljava/lang/Integer;

    move-result-object v2

    .line 532
    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    if-ltz v2, :cond_3

    :cond_2
    move v0, v1

    :cond_3
    return v0
.end method

.method public static a(Lkotlin/reflect/jvm/internal/impl/b/t;Lkotlin/reflect/jvm/internal/impl/b/t;)Z
    .locals 7

    .prologue
    const/4 v4, 0x3

    const/4 v6, 0x2

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-nez p0, :cond_0

    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    new-array v4, v4, [Ljava/lang/Object;

    const-string v5, "overriding"

    aput-object v5, v4, v1

    const-string v1, "kotlin/reflect/jvm/internal/impl/resolve/OverridingUtil"

    aput-object v1, v4, v0

    const-string v0, "isVisibleForOverride"

    aput-object v0, v4, v6

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_0
    if-nez p1, :cond_1

    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    new-array v4, v4, [Ljava/lang/Object;

    const-string v5, "fromSuper"

    aput-object v5, v4, v1

    const-string v1, "kotlin/reflect/jvm/internal/impl/resolve/OverridingUtil"

    aput-object v1, v4, v0

    const-string v0, "isVisibleForOverride"

    aput-object v0, v4, v6

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 419
    :cond_1
    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/b/t;->p()Lkotlin/reflect/jvm/internal/impl/b/ay;

    move-result-object v2

    invoke-static {v2}, Lkotlin/reflect/jvm/internal/impl/b/ax;->a(Lkotlin/reflect/jvm/internal/impl/b/ay;)Z

    move-result v2

    if-nez v2, :cond_2

    invoke-static {p1, p0}, Lkotlin/reflect/jvm/internal/impl/b/ax;->a(Lkotlin/reflect/jvm/internal/impl/b/q;Lkotlin/reflect/jvm/internal/impl/b/m;)Z

    move-result v2

    if-eqz v2, :cond_2

    :goto_0
    return v0

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method private static a(Lkotlin/reflect/jvm/internal/impl/k/r;Lkotlin/reflect/jvm/internal/impl/k/r;Lkotlin/reflect/jvm/internal/impl/k/a/c;)Z
    .locals 7

    .prologue
    const/4 v4, 0x3

    const/4 v6, 0x2

    const/4 v1, 0x1

    const/4 v0, 0x0

    if-nez p0, :cond_0

    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    new-array v4, v4, [Ljava/lang/Object;

    const-string v5, "typeInSuper"

    aput-object v5, v4, v0

    const-string v0, "kotlin/reflect/jvm/internal/impl/resolve/OverridingUtil"

    aput-object v0, v4, v1

    const-string v0, "areTypesEquivalent"

    aput-object v0, v4, v6

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_0
    if-nez p1, :cond_1

    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    new-array v4, v4, [Ljava/lang/Object;

    const-string v5, "typeInSub"

    aput-object v5, v4, v0

    const-string v0, "kotlin/reflect/jvm/internal/impl/resolve/OverridingUtil"

    aput-object v0, v4, v1

    const-string v0, "areTypesEquivalent"

    aput-object v0, v4, v6

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_1
    if-nez p2, :cond_2

    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    new-array v4, v4, [Ljava/lang/Object;

    const-string v5, "typeChecker"

    aput-object v5, v4, v0

    const-string v0, "kotlin/reflect/jvm/internal/impl/resolve/OverridingUtil"

    aput-object v0, v4, v1

    const-string v0, "areTypesEquivalent"

    aput-object v0, v4, v6

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 356
    :cond_2
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/k/r;->d()Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/k/r;->d()Z

    move-result v2

    if-eqz v2, :cond_5

    move v2, v1

    .line 357
    :goto_0
    if-nez v2, :cond_3

    invoke-interface {p2, p0, p1}, Lkotlin/reflect/jvm/internal/impl/k/a/c;->b(Lkotlin/reflect/jvm/internal/impl/k/r;Lkotlin/reflect/jvm/internal/impl/k/r;)Z

    move-result v2

    if-eqz v2, :cond_4

    :cond_3
    move v0, v1

    :cond_4
    return v0

    :cond_5
    move v2, v0

    .line 356
    goto :goto_0
.end method

.method private static b(Lkotlin/reflect/jvm/internal/impl/b/b;)Lkotlin/reflect/jvm/internal/impl/b/ay;
    .locals 6

    .prologue
    const/4 v1, 0x0

    if-nez p0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "memberDescriptor"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "kotlin/reflect/jvm/internal/impl/resolve/OverridingUtil"

    aput-object v4, v2, v3

    const/4 v3, 0x2

    const-string v4, "computeVisibilityToInherit"

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 819
    :cond_0
    invoke-interface {p0}, Lkotlin/reflect/jvm/internal/impl/b/b;->k()Ljava/util/Collection;

    move-result-object v0

    .line 820
    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/h/j;->a(Ljava/util/Collection;)Lkotlin/reflect/jvm/internal/impl/b/ay;

    move-result-object v2

    .line 821
    if-nez v2, :cond_1

    move-object v0, v1

    .line 833
    :goto_0
    return-object v0

    .line 824
    :cond_1
    invoke-interface {p0}, Lkotlin/reflect/jvm/internal/impl/b/b;->n()Lkotlin/reflect/jvm/internal/impl/b/b$a;

    move-result-object v3

    sget-object v4, Lkotlin/reflect/jvm/internal/impl/b/b$a;->b:Lkotlin/reflect/jvm/internal/impl/b/b$a;

    if-ne v3, v4, :cond_4

    .line 825
    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/b;

    .line 827
    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/b/b;->m()Lkotlin/reflect/jvm/internal/impl/b/u;

    move-result-object v4

    sget-object v5, Lkotlin/reflect/jvm/internal/impl/b/u;->d:Lkotlin/reflect/jvm/internal/impl/b/u;

    if-eq v4, v5, :cond_2

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/b/b;->p()Lkotlin/reflect/jvm/internal/impl/b/ay;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    move-object v0, v1

    .line 828
    goto :goto_0

    :cond_3
    move-object v0, v2

    .line 831
    goto :goto_0

    .line 833
    :cond_4
    invoke-virtual {v2}, Lkotlin/reflect/jvm/internal/impl/b/ay;->b()Lkotlin/reflect/jvm/internal/impl/b/ay;

    move-result-object v0

    goto :goto_0
.end method

.method public static b(Lkotlin/reflect/jvm/internal/impl/b/a;Lkotlin/reflect/jvm/internal/impl/b/a;)Lkotlin/reflect/jvm/internal/impl/h/j$a;
    .locals 7

    .prologue
    const/4 v2, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    if-nez p0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "superDescriptor"

    aput-object v3, v2, v4

    const-string v3, "kotlin/reflect/jvm/internal/impl/resolve/OverridingUtil"

    aput-object v3, v2, v5

    const-string v3, "getBasicOverridabilityProblem"

    aput-object v3, v2, v6

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    if-nez p1, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "subDescriptor"

    aput-object v3, v2, v4

    const-string v3, "kotlin/reflect/jvm/internal/impl/resolve/OverridingUtil"

    aput-object v3, v2, v5

    const-string v3, "getBasicOverridabilityProblem"

    aput-object v3, v2, v6

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 288
    :cond_1
    instance-of v0, p0, Lkotlin/reflect/jvm/internal/impl/b/s;

    if-eqz v0, :cond_2

    instance-of v0, p1, Lkotlin/reflect/jvm/internal/impl/b/s;

    if-eqz v0, :cond_3

    :cond_2
    instance-of v0, p0, Lkotlin/reflect/jvm/internal/impl/b/ag;

    if-eqz v0, :cond_5

    instance-of v0, p1, Lkotlin/reflect/jvm/internal/impl/b/ag;

    if-nez v0, :cond_5

    .line 290
    :cond_3
    const-string v0, "Member kind mismatch"

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/h/j$a;->a(Ljava/lang/String;)Lkotlin/reflect/jvm/internal/impl/h/j$a;

    move-result-object v0

    .line 307
    :cond_4
    :goto_0
    return-object v0

    .line 293
    :cond_5
    instance-of v0, p0, Lkotlin/reflect/jvm/internal/impl/b/s;

    if-nez v0, :cond_6

    instance-of v0, p0, Lkotlin/reflect/jvm/internal/impl/b/ag;

    if-nez v0, :cond_6

    .line 294
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "This type of CallableDescriptor cannot be checked for overridability: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 298
    :cond_6
    invoke-interface {p0}, Lkotlin/reflect/jvm/internal/impl/b/a;->D_()Lkotlin/reflect/jvm/internal/impl/e/f;

    move-result-object v0

    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/b/a;->D_()Lkotlin/reflect/jvm/internal/impl/e/f;

    move-result-object v1

    invoke-virtual {v0, v1}, Lkotlin/reflect/jvm/internal/impl/e/f;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_7

    .line 299
    const-string v0, "Name mismatch"

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/h/j$a;->a(Ljava/lang/String;)Lkotlin/reflect/jvm/internal/impl/h/j$a;

    move-result-object v0

    goto :goto_0

    .line 302
    :cond_7
    invoke-static {p0, p1}, Lkotlin/reflect/jvm/internal/impl/h/j;->e(Lkotlin/reflect/jvm/internal/impl/b/a;Lkotlin/reflect/jvm/internal/impl/b/a;)Lkotlin/reflect/jvm/internal/impl/h/j$a;

    move-result-object v0

    .line 303
    if-nez v0, :cond_4

    .line 307
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static b(Ljava/util/Collection;)Z
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/b/b;",
            ">;)Z"
        }
    .end annotation

    .prologue
    const/4 v6, 0x2

    const/4 v0, 0x1

    if-nez p0, :cond_0

    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    const-string v5, "notOverridden"

    aput-object v5, v3, v4

    const-string v4, "kotlin/reflect/jvm/internal/impl/resolve/OverridingUtil"

    aput-object v4, v3, v0

    const-string v0, "allHasSameContainingDeclaration"

    aput-object v0, v3, v6

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 460
    :cond_0
    invoke-interface {p0}, Ljava/util/Collection;->size()I

    move-result v1

    if-ge v1, v6, :cond_1

    .line 463
    :goto_0
    return v0

    .line 462
    :cond_1
    invoke-interface {p0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/b;

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/b/b;->y_()Lkotlin/reflect/jvm/internal/impl/b/m;

    move-result-object v0

    .line 463
    new-instance v1, Lkotlin/reflect/jvm/internal/impl/h/j$3;

    invoke-direct {v1, v0}, Lkotlin/reflect/jvm/internal/impl/h/j$3;-><init>(Lkotlin/reflect/jvm/internal/impl/b/m;)V

    invoke-static {p0, v1}, Lkotlin/a/m;->d(Ljava/lang/Iterable;Lkotlin/d/a/b;)Z

    move-result v0

    goto :goto_0
.end method

.method private static c(Ljava/util/Collection;)Lkotlin/reflect/jvm/internal/impl/b/u;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/b/b;",
            ">;)",
            "Lkotlin/reflect/jvm/internal/impl/b/u;"
        }
    .end annotation

    .prologue
    const/4 v8, 0x2

    const/4 v4, 0x1

    const/4 v2, 0x0

    if-nez p0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Object;

    const-string v5, "descriptors"

    aput-object v5, v3, v2

    const-string v2, "kotlin/reflect/jvm/internal/impl/resolve/OverridingUtil"

    aput-object v2, v3, v4

    const-string v2, "determineModality"

    aput-object v2, v3, v8

    invoke-static {v1, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 651
    :cond_0
    invoke-interface {p0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v5

    move v1, v2

    move v3, v2

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/b;

    .line 652
    sget-object v6, Lkotlin/reflect/jvm/internal/impl/h/j$8;->c:[I

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/b/b;->m()Lkotlin/reflect/jvm/internal/impl/b/u;

    move-result-object v7

    invoke-virtual {v7}, Lkotlin/reflect/jvm/internal/impl/b/u;->ordinal()I

    move-result v7

    aget v6, v6, v7

    packed-switch v6, :pswitch_data_0

    move v0, v1

    move v1, v3

    :goto_1
    move v3, v1

    move v1, v0

    .line 662
    goto :goto_0

    .line 654
    :pswitch_0
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/b/u;->a:Lkotlin/reflect/jvm/internal/impl/b/u;

    if-nez v0, :cond_5

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "@NotNull method %s.%s must not return null"

    new-array v3, v8, [Ljava/lang/Object;

    const-string v5, "kotlin/reflect/jvm/internal/impl/resolve/OverridingUtil"

    aput-object v5, v3, v2

    const-string v2, "determineModality"

    aput-object v2, v3, v4

    invoke-static {v1, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 656
    :pswitch_1
    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Member cannot have SEALED modality: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    :pswitch_2
    move v0, v1

    move v1, v4

    .line 659
    goto :goto_1

    :pswitch_3
    move v0, v4

    move v1, v3

    .line 661
    goto :goto_1

    .line 666
    :cond_1
    if-eqz v3, :cond_2

    if-nez v1, :cond_2

    sget-object v0, Lkotlin/reflect/jvm/internal/impl/b/u;->c:Lkotlin/reflect/jvm/internal/impl/b/u;

    if-nez v0, :cond_5

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "@NotNull method %s.%s must not return null"

    new-array v3, v8, [Ljava/lang/Object;

    const-string v5, "kotlin/reflect/jvm/internal/impl/resolve/OverridingUtil"

    aput-object v5, v3, v2

    const-string v2, "determineModality"

    aput-object v2, v3, v4

    invoke-static {v1, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 667
    :cond_2
    if-nez v3, :cond_3

    if-eqz v1, :cond_3

    sget-object v0, Lkotlin/reflect/jvm/internal/impl/b/u;->d:Lkotlin/reflect/jvm/internal/impl/b/u;

    if-nez v0, :cond_5

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "@NotNull method %s.%s must not return null"

    new-array v3, v8, [Ljava/lang/Object;

    const-string v5, "kotlin/reflect/jvm/internal/impl/resolve/OverridingUtil"

    aput-object v5, v3, v2

    const-string v2, "determineModality"

    aput-object v2, v3, v4

    invoke-static {v1, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 669
    :cond_3
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    .line 670
    invoke-interface {p0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/b;

    .line 671
    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/h/j;->a(Lkotlin/reflect/jvm/internal/impl/b/b;)Ljava/util/Set;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    goto :goto_2

    .line 673
    :cond_4
    invoke-static {v1}, Lkotlin/reflect/jvm/internal/impl/h/j;->a(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/h/j;->d(Ljava/util/Collection;)Lkotlin/reflect/jvm/internal/impl/b/u;

    move-result-object v0

    if-nez v0, :cond_5

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "@NotNull method %s.%s must not return null"

    new-array v3, v8, [Ljava/lang/Object;

    const-string v5, "kotlin/reflect/jvm/internal/impl/resolve/OverridingUtil"

    aput-object v5, v3, v2

    const-string v2, "determineModality"

    aput-object v2, v3, v4

    invoke-static {v1, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_5
    return-object v0

    .line 652
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static c(Lkotlin/reflect/jvm/internal/impl/b/a;Lkotlin/reflect/jvm/internal/impl/b/a;)Z
    .locals 8

    .prologue
    const/4 v4, 0x3

    const/4 v6, 0x2

    const/4 v2, 0x1

    const/4 v3, 0x0

    if-nez p0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    new-array v4, v4, [Ljava/lang/Object;

    const-string v5, "a"

    aput-object v5, v4, v3

    const-string v3, "kotlin/reflect/jvm/internal/impl/resolve/OverridingUtil"

    aput-object v3, v4, v2

    const-string v2, "isMoreSpecific"

    aput-object v2, v4, v6

    invoke-static {v1, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    if-nez p1, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    new-array v4, v4, [Ljava/lang/Object;

    const-string v5, "b"

    aput-object v5, v4, v3

    const-string v3, "kotlin/reflect/jvm/internal/impl/resolve/OverridingUtil"

    aput-object v3, v4, v2

    const-string v2, "isMoreSpecific"

    aput-object v2, v4, v6

    invoke-static {v1, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 495
    :cond_1
    invoke-interface {p0}, Lkotlin/reflect/jvm/internal/impl/b/a;->g()Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v4

    .line 496
    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/b/a;->g()Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v5

    .line 498
    sget-boolean v0, Lkotlin/reflect/jvm/internal/impl/h/j;->b:Z

    if-nez v0, :cond_2

    if-nez v4, :cond_2

    new-instance v0, Ljava/lang/AssertionError;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Return type of "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is null"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 499
    :cond_2
    sget-boolean v0, Lkotlin/reflect/jvm/internal/impl/h/j;->b:Z

    if-nez v0, :cond_3

    if-nez v5, :cond_3

    new-instance v0, Ljava/lang/AssertionError;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Return type of "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is null"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 501
    :cond_3
    invoke-static {p0, p1}, Lkotlin/reflect/jvm/internal/impl/h/j;->a(Lkotlin/reflect/jvm/internal/impl/b/q;Lkotlin/reflect/jvm/internal/impl/b/q;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 521
    :cond_4
    :goto_0
    return v3

    .line 503
    :cond_5
    instance-of v0, p0, Lkotlin/reflect/jvm/internal/impl/b/ak;

    if-eqz v0, :cond_7

    .line 504
    sget-boolean v0, Lkotlin/reflect/jvm/internal/impl/h/j;->b:Z

    if-nez v0, :cond_6

    instance-of v0, p1, Lkotlin/reflect/jvm/internal/impl/b/ak;

    if-nez v0, :cond_6

    new-instance v0, Ljava/lang/AssertionError;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "b is "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 506
    :cond_6
    invoke-static {p0, v4, p1, v5}, Lkotlin/reflect/jvm/internal/impl/h/j;->a(Lkotlin/reflect/jvm/internal/impl/b/a;Lkotlin/reflect/jvm/internal/impl/k/r;Lkotlin/reflect/jvm/internal/impl/b/a;Lkotlin/reflect/jvm/internal/impl/k/r;)Z

    move-result v3

    goto :goto_0

    .line 508
    :cond_7
    instance-of v0, p0, Lkotlin/reflect/jvm/internal/impl/b/ag;

    if-eqz v0, :cond_c

    .line 509
    sget-boolean v0, Lkotlin/reflect/jvm/internal/impl/h/j;->b:Z

    if-nez v0, :cond_8

    instance-of v0, p1, Lkotlin/reflect/jvm/internal/impl/b/ag;

    if-nez v0, :cond_8

    new-instance v0, Ljava/lang/AssertionError;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "b is "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    :cond_8
    move-object v0, p0

    .line 511
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/ag;

    move-object v1, p1

    .line 512
    check-cast v1, Lkotlin/reflect/jvm/internal/impl/b/ag;

    .line 514
    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/b/ag;->c()Lkotlin/reflect/jvm/internal/impl/b/ai;

    move-result-object v6

    invoke-interface {v1}, Lkotlin/reflect/jvm/internal/impl/b/ag;->c()Lkotlin/reflect/jvm/internal/impl/b/ai;

    move-result-object v7

    invoke-static {v6, v7}, Lkotlin/reflect/jvm/internal/impl/h/j;->a(Lkotlin/reflect/jvm/internal/impl/b/af;Lkotlin/reflect/jvm/internal/impl/b/af;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 516
    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/b/ag;->y()Z

    move-result v6

    if-eqz v6, :cond_9

    invoke-interface {v1}, Lkotlin/reflect/jvm/internal/impl/b/ag;->y()Z

    move-result v6

    if-eqz v6, :cond_9

    .line 517
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/h/j;->a:Lkotlin/reflect/jvm/internal/impl/h/j;

    invoke-interface {p0}, Lkotlin/reflect/jvm/internal/impl/b/a;->f()Ljava/util/List;

    move-result-object v1

    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/b/a;->f()Ljava/util/List;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lkotlin/reflect/jvm/internal/impl/h/j;->a(Ljava/util/List;Ljava/util/List;)Lkotlin/reflect/jvm/internal/impl/k/a/c;

    move-result-object v0

    invoke-interface {v0, v4, v5}, Lkotlin/reflect/jvm/internal/impl/k/a/c;->b(Lkotlin/reflect/jvm/internal/impl/k/r;Lkotlin/reflect/jvm/internal/impl/k/r;)Z

    move-result v3

    goto/16 :goto_0

    .line 521
    :cond_9
    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/b/ag;->y()Z

    move-result v0

    if-nez v0, :cond_a

    invoke-interface {v1}, Lkotlin/reflect/jvm/internal/impl/b/ag;->y()Z

    move-result v0

    if-nez v0, :cond_b

    :cond_a
    invoke-static {p0, v4, p1, v5}, Lkotlin/reflect/jvm/internal/impl/h/j;->a(Lkotlin/reflect/jvm/internal/impl/b/a;Lkotlin/reflect/jvm/internal/impl/k/r;Lkotlin/reflect/jvm/internal/impl/b/a;Lkotlin/reflect/jvm/internal/impl/k/r;)Z

    move-result v0

    if-eqz v0, :cond_b

    move v0, v2

    :goto_1
    move v3, v0

    goto/16 :goto_0

    :cond_b
    move v0, v3

    goto :goto_1

    .line 524
    :cond_c
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unexpected callable: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private static d(Ljava/util/Collection;)Lkotlin/reflect/jvm/internal/impl/b/u;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/b/b;",
            ">;)",
            "Lkotlin/reflect/jvm/internal/impl/b/u;"
        }
    .end annotation

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    if-nez p0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "descriptors"

    aput-object v3, v2, v4

    const-string v3, "kotlin/reflect/jvm/internal/impl/resolve/OverridingUtil"

    aput-object v3, v2, v5

    const-string v3, "getMinimalModality"

    aput-object v3, v2, v6

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 678
    :cond_0
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/b/u;->d:Lkotlin/reflect/jvm/internal/impl/b/u;

    .line 679
    invoke-interface {p0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move-object v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/b;

    .line 680
    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/b/b;->m()Lkotlin/reflect/jvm/internal/impl/b/u;

    move-result-object v3

    invoke-virtual {v3, v1}, Lkotlin/reflect/jvm/internal/impl/b/u;->compareTo(Ljava/lang/Enum;)I

    move-result v3

    if-gez v3, :cond_3

    .line 681
    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/b/b;->m()Lkotlin/reflect/jvm/internal/impl/b/u;

    move-result-object v0

    :goto_1
    move-object v1, v0

    goto :goto_0

    .line 684
    :cond_1
    if-nez v1, :cond_2

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "@NotNull method %s.%s must not return null"

    new-array v2, v6, [Ljava/lang/Object;

    const-string v3, "kotlin/reflect/jvm/internal/impl/resolve/OverridingUtil"

    aput-object v3, v2, v4

    const-string v3, "getMinimalModality"

    aput-object v3, v2, v5

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    return-object v1

    :cond_3
    move-object v0, v1

    goto :goto_1
.end method

.method public static d(Lkotlin/reflect/jvm/internal/impl/b/a;Lkotlin/reflect/jvm/internal/impl/b/a;)Lkotlin/reflect/jvm/internal/impl/h/j$a$a;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 743
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/h/j;->a:Lkotlin/reflect/jvm/internal/impl/h/j;

    invoke-virtual {v0, p1, p0, v2}, Lkotlin/reflect/jvm/internal/impl/h/j;->a(Lkotlin/reflect/jvm/internal/impl/b/a;Lkotlin/reflect/jvm/internal/impl/b/a;Lkotlin/reflect/jvm/internal/impl/b/e;)Lkotlin/reflect/jvm/internal/impl/h/j$a;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/h/j$a;->b()Lkotlin/reflect/jvm/internal/impl/h/j$a$a;

    move-result-object v0

    .line 744
    sget-object v1, Lkotlin/reflect/jvm/internal/impl/h/j;->a:Lkotlin/reflect/jvm/internal/impl/h/j;

    invoke-virtual {v1, p0, p1, v2}, Lkotlin/reflect/jvm/internal/impl/h/j;->a(Lkotlin/reflect/jvm/internal/impl/b/a;Lkotlin/reflect/jvm/internal/impl/b/a;Lkotlin/reflect/jvm/internal/impl/b/e;)Lkotlin/reflect/jvm/internal/impl/h/j$a;

    move-result-object v1

    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/impl/h/j$a;->b()Lkotlin/reflect/jvm/internal/impl/h/j$a$a;

    move-result-object v1

    .line 746
    sget-object v2, Lkotlin/reflect/jvm/internal/impl/h/j$a$a;->a:Lkotlin/reflect/jvm/internal/impl/h/j$a$a;

    if-ne v0, v2, :cond_0

    sget-object v2, Lkotlin/reflect/jvm/internal/impl/h/j$a$a;->a:Lkotlin/reflect/jvm/internal/impl/h/j$a$a;

    if-ne v1, v2, :cond_0

    sget-object v0, Lkotlin/reflect/jvm/internal/impl/h/j$a$a;->a:Lkotlin/reflect/jvm/internal/impl/h/j$a$a;

    :goto_0
    return-object v0

    :cond_0
    sget-object v2, Lkotlin/reflect/jvm/internal/impl/h/j$a$a;->c:Lkotlin/reflect/jvm/internal/impl/h/j$a$a;

    if-eq v0, v2, :cond_1

    sget-object v0, Lkotlin/reflect/jvm/internal/impl/h/j$a$a;->c:Lkotlin/reflect/jvm/internal/impl/h/j$a$a;

    if-ne v1, v0, :cond_2

    :cond_1
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/h/j$a$a;->c:Lkotlin/reflect/jvm/internal/impl/h/j$a$a;

    goto :goto_0

    :cond_2
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/h/j$a$a;->b:Lkotlin/reflect/jvm/internal/impl/h/j$a$a;

    goto :goto_0
.end method

.method private static e(Lkotlin/reflect/jvm/internal/impl/b/a;Lkotlin/reflect/jvm/internal/impl/b/a;)Lkotlin/reflect/jvm/internal/impl/h/j$a;
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 340
    invoke-interface {p0}, Lkotlin/reflect/jvm/internal/impl/b/a;->d()Lkotlin/reflect/jvm/internal/impl/b/aj;

    move-result-object v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/b/a;->d()Lkotlin/reflect/jvm/internal/impl/b/aj;

    move-result-object v3

    if-nez v3, :cond_1

    :goto_1
    if-eq v0, v1, :cond_2

    .line 341
    const-string v0, "Receiver presence mismatch"

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/h/j$a;->a(Ljava/lang/String;)Lkotlin/reflect/jvm/internal/impl/h/j$a;

    move-result-object v0

    .line 348
    :goto_2
    return-object v0

    :cond_0
    move v0, v2

    .line 340
    goto :goto_0

    :cond_1
    move v1, v2

    goto :goto_1

    .line 344
    :cond_2
    invoke-interface {p0}, Lkotlin/reflect/jvm/internal/impl/b/a;->i()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/b/a;->i()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-eq v0, v1, :cond_3

    .line 345
    const-string v0, "Value parameter number mismatch"

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/h/j$a;->a(Ljava/lang/String;)Lkotlin/reflect/jvm/internal/impl/h/j$a;

    move-result-object v0

    goto :goto_2

    .line 348
    :cond_3
    const/4 v0, 0x0

    goto :goto_2
.end method


# virtual methods
.method public a(Lkotlin/reflect/jvm/internal/impl/b/a;Lkotlin/reflect/jvm/internal/impl/b/a;Lkotlin/reflect/jvm/internal/impl/b/e;)Lkotlin/reflect/jvm/internal/impl/h/j$a;
    .locals 7

    .prologue
    const/4 v2, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "superDescriptor"

    aput-object v3, v2, v4

    const-string v3, "kotlin/reflect/jvm/internal/impl/resolve/OverridingUtil"

    aput-object v3, v2, v5

    const-string v3, "isOverridableBy"

    aput-object v3, v2, v6

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    if-nez p2, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "subDescriptor"

    aput-object v3, v2, v4

    const-string v3, "kotlin/reflect/jvm/internal/impl/resolve/OverridingUtil"

    aput-object v3, v2, v5

    const-string v3, "isOverridableBy"

    aput-object v3, v2, v6

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 161
    :cond_1
    invoke-virtual {p0, p1, p2, p3, v4}, Lkotlin/reflect/jvm/internal/impl/h/j;->a(Lkotlin/reflect/jvm/internal/impl/b/a;Lkotlin/reflect/jvm/internal/impl/b/a;Lkotlin/reflect/jvm/internal/impl/b/e;Z)Lkotlin/reflect/jvm/internal/impl/h/j$a;

    move-result-object v0

    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "@NotNull method %s.%s must not return null"

    new-array v2, v6, [Ljava/lang/Object;

    const-string v3, "kotlin/reflect/jvm/internal/impl/resolve/OverridingUtil"

    aput-object v3, v2, v4

    const-string v3, "isOverridableBy"

    aput-object v3, v2, v5

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    return-object v0
.end method

.method public a(Lkotlin/reflect/jvm/internal/impl/b/a;Lkotlin/reflect/jvm/internal/impl/b/a;Lkotlin/reflect/jvm/internal/impl/b/e;Z)Lkotlin/reflect/jvm/internal/impl/h/j$a;
    .locals 9

    .prologue
    const/4 v4, 0x3

    const/4 v8, 0x2

    const/4 v2, 0x0

    const/4 v1, 0x1

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v3, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    new-array v4, v4, [Ljava/lang/Object;

    const-string v5, "superDescriptor"

    aput-object v5, v4, v2

    const-string v2, "kotlin/reflect/jvm/internal/impl/resolve/OverridingUtil"

    aput-object v2, v4, v1

    const-string v1, "isOverridableBy"

    aput-object v1, v4, v8

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    if-nez p2, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v3, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    new-array v4, v4, [Ljava/lang/Object;

    const-string v5, "subDescriptor"

    aput-object v5, v4, v2

    const-string v2, "kotlin/reflect/jvm/internal/impl/resolve/OverridingUtil"

    aput-object v2, v4, v1

    const-string v1, "isOverridableBy"

    aput-object v1, v4, v8

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 171
    :cond_1
    invoke-virtual {p0, p1, p2, p4}, Lkotlin/reflect/jvm/internal/impl/h/j;->a(Lkotlin/reflect/jvm/internal/impl/b/a;Lkotlin/reflect/jvm/internal/impl/b/a;Z)Lkotlin/reflect/jvm/internal/impl/h/j$a;

    move-result-object v4

    .line 172
    invoke-virtual {v4}, Lkotlin/reflect/jvm/internal/impl/h/j$a;->b()Lkotlin/reflect/jvm/internal/impl/h/j$a$a;

    move-result-object v0

    sget-object v3, Lkotlin/reflect/jvm/internal/impl/h/j$a$a;->a:Lkotlin/reflect/jvm/internal/impl/h/j$a$a;

    if-ne v0, v3, :cond_4

    move v0, v1

    .line 174
    :goto_0
    sget-object v3, Lkotlin/reflect/jvm/internal/impl/h/j;->c:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    move v3, v0

    :cond_2
    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/h/d;

    .line 176
    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/h/d;->a()Lkotlin/reflect/jvm/internal/impl/h/d$a;

    move-result-object v6

    sget-object v7, Lkotlin/reflect/jvm/internal/impl/h/d$a;->a:Lkotlin/reflect/jvm/internal/impl/h/d$a;

    if-eq v6, v7, :cond_2

    .line 177
    if-eqz v3, :cond_3

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/h/d;->a()Lkotlin/reflect/jvm/internal/impl/h/d$a;

    move-result-object v6

    sget-object v7, Lkotlin/reflect/jvm/internal/impl/h/d$a;->b:Lkotlin/reflect/jvm/internal/impl/h/d$a;

    if-eq v6, v7, :cond_2

    .line 179
    :cond_3
    invoke-interface {v0, p1, p2, p3}, Lkotlin/reflect/jvm/internal/impl/h/d;->a(Lkotlin/reflect/jvm/internal/impl/b/a;Lkotlin/reflect/jvm/internal/impl/b/a;Lkotlin/reflect/jvm/internal/impl/b/e;)Lkotlin/reflect/jvm/internal/impl/h/d$b;

    move-result-object v0

    .line 182
    sget-object v6, Lkotlin/reflect/jvm/internal/impl/h/j$8;->a:[I

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/h/d$b;->ordinal()I

    move-result v0

    aget v0, v6, v0

    packed-switch v0, :pswitch_data_0

    move v0, v3

    :goto_2
    move v3, v0

    .line 194
    goto :goto_1

    :cond_4
    move v0, v2

    .line 172
    goto :goto_0

    :pswitch_0
    move v0, v1

    .line 185
    goto :goto_2

    .line 187
    :pswitch_1
    const-string v0, "External condition failed"

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/h/j$a;->b(Ljava/lang/String;)Lkotlin/reflect/jvm/internal/impl/h/j$a;

    move-result-object v0

    if-nez v0, :cond_7

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v3, "@NotNull method %s.%s must not return null"

    new-array v4, v8, [Ljava/lang/Object;

    const-string v5, "kotlin/reflect/jvm/internal/impl/resolve/OverridingUtil"

    aput-object v5, v4, v2

    const-string v2, "isOverridableBy"

    aput-object v2, v4, v1

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 189
    :pswitch_2
    const-string v0, "External condition"

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/h/j$a;->a(Ljava/lang/String;)Lkotlin/reflect/jvm/internal/impl/h/j$a;

    move-result-object v0

    if-nez v0, :cond_7

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v3, "@NotNull method %s.%s must not return null"

    new-array v4, v8, [Ljava/lang/Object;

    const-string v5, "kotlin/reflect/jvm/internal/impl/resolve/OverridingUtil"

    aput-object v5, v4, v2

    const-string v2, "isOverridableBy"

    aput-object v2, v4, v1

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 196
    :cond_5
    if-nez v3, :cond_8

    .line 197
    if-nez v4, :cond_6

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v3, "@NotNull method %s.%s must not return null"

    new-array v4, v8, [Ljava/lang/Object;

    const-string v5, "kotlin/reflect/jvm/internal/impl/resolve/OverridingUtil"

    aput-object v5, v4, v2

    const-string v2, "isOverridableBy"

    aput-object v2, v4, v1

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_6
    move-object v0, v4

    .line 221
    :cond_7
    return-object v0

    .line 201
    :cond_8
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/h/j;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_9
    :goto_3
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_a

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/h/d;

    .line 203
    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/h/d;->a()Lkotlin/reflect/jvm/internal/impl/h/d$a;

    move-result-object v4

    sget-object v5, Lkotlin/reflect/jvm/internal/impl/h/d$a;->a:Lkotlin/reflect/jvm/internal/impl/h/d$a;

    if-ne v4, v5, :cond_9

    .line 205
    invoke-interface {v0, p1, p2, p3}, Lkotlin/reflect/jvm/internal/impl/h/d;->a(Lkotlin/reflect/jvm/internal/impl/b/a;Lkotlin/reflect/jvm/internal/impl/b/a;Lkotlin/reflect/jvm/internal/impl/b/e;)Lkotlin/reflect/jvm/internal/impl/h/d$b;

    move-result-object v4

    .line 207
    sget-object v5, Lkotlin/reflect/jvm/internal/impl/h/j$8;->a:[I

    invoke-virtual {v4}, Lkotlin/reflect/jvm/internal/impl/h/d$b;->ordinal()I

    move-result v4

    aget v4, v5, v4

    packed-switch v4, :pswitch_data_1

    goto :goto_3

    .line 213
    :pswitch_3
    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Contract violation in "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " condition. It\'s not supposed to end with success"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 209
    :pswitch_4
    const-string v0, "External condition failed"

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/h/j$a;->b(Ljava/lang/String;)Lkotlin/reflect/jvm/internal/impl/h/j$a;

    move-result-object v0

    if-nez v0, :cond_7

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v3, "@NotNull method %s.%s must not return null"

    new-array v4, v8, [Ljava/lang/Object;

    const-string v5, "kotlin/reflect/jvm/internal/impl/resolve/OverridingUtil"

    aput-object v5, v4, v2

    const-string v2, "isOverridableBy"

    aput-object v2, v4, v1

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 211
    :pswitch_5
    const-string v0, "External condition"

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/h/j$a;->a(Ljava/lang/String;)Lkotlin/reflect/jvm/internal/impl/h/j$a;

    move-result-object v0

    if-nez v0, :cond_7

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v3, "@NotNull method %s.%s must not return null"

    new-array v4, v8, [Ljava/lang/Object;

    const-string v5, "kotlin/reflect/jvm/internal/impl/resolve/OverridingUtil"

    aput-object v5, v4, v2

    const-string v2, "isOverridableBy"

    aput-object v2, v4, v1

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 221
    :cond_a
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/h/j$a;->a()Lkotlin/reflect/jvm/internal/impl/h/j$a;

    move-result-object v0

    if-nez v0, :cond_7

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v3, "@NotNull method %s.%s must not return null"

    new-array v4, v8, [Ljava/lang/Object;

    const-string v5, "kotlin/reflect/jvm/internal/impl/resolve/OverridingUtil"

    aput-object v5, v4, v2

    const-string v2, "isOverridableBy"

    aput-object v2, v4, v1

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 182
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch

    .line 207
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public a(Lkotlin/reflect/jvm/internal/impl/b/a;Lkotlin/reflect/jvm/internal/impl/b/a;Z)Lkotlin/reflect/jvm/internal/impl/h/j$a;
    .locals 11

    .prologue
    const/4 v2, 0x3

    const/4 v10, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    new-array v2, v2, [Ljava/lang/Object;

    const-string v5, "superDescriptor"

    aput-object v5, v2, v3

    const-string v3, "kotlin/reflect/jvm/internal/impl/resolve/OverridingUtil"

    aput-object v3, v2, v4

    const-string v3, "isOverridableByWithoutExternalConditions"

    aput-object v3, v2, v10

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    if-nez p2, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    new-array v2, v2, [Ljava/lang/Object;

    const-string v5, "subDescriptor"

    aput-object v5, v2, v3

    const-string v3, "kotlin/reflect/jvm/internal/impl/resolve/OverridingUtil"

    aput-object v3, v2, v4

    const-string v3, "isOverridableByWithoutExternalConditions"

    aput-object v3, v2, v10

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 230
    :cond_1
    invoke-static {p1, p2}, Lkotlin/reflect/jvm/internal/impl/h/j;->b(Lkotlin/reflect/jvm/internal/impl/b/a;Lkotlin/reflect/jvm/internal/impl/b/a;)Lkotlin/reflect/jvm/internal/impl/h/j$a;

    move-result-object v0

    .line 231
    if-eqz v0, :cond_2

    if-nez v0, :cond_d

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "@NotNull method %s.%s must not return null"

    new-array v2, v10, [Ljava/lang/Object;

    const-string v5, "kotlin/reflect/jvm/internal/impl/resolve/OverridingUtil"

    aput-object v5, v2, v3

    const-string v3, "isOverridableByWithoutExternalConditions"

    aput-object v3, v2, v4

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 233
    :cond_2
    invoke-static {p1}, Lkotlin/reflect/jvm/internal/impl/h/j;->a(Lkotlin/reflect/jvm/internal/impl/b/a;)Ljava/util/List;

    move-result-object v5

    .line 234
    invoke-static {p2}, Lkotlin/reflect/jvm/internal/impl/h/j;->a(Lkotlin/reflect/jvm/internal/impl/b/a;)Ljava/util/List;

    move-result-object v6

    .line 236
    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/b/a;->f()Ljava/util/List;

    move-result-object v7

    .line 237
    invoke-interface {p2}, Lkotlin/reflect/jvm/internal/impl/b/a;->f()Ljava/util/List;

    move-result-object v8

    .line 239
    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v0

    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v1

    if-eq v0, v1, :cond_5

    move v2, v3

    .line 240
    :goto_0
    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_4

    .line 242
    sget-object v7, Lkotlin/reflect/jvm/internal/impl/k/a/c;->a:Lkotlin/reflect/jvm/internal/impl/k/a/c;

    invoke-interface {v5, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/r;

    invoke-interface {v6, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lkotlin/reflect/jvm/internal/impl/k/r;

    invoke-interface {v7, v0, v1}, Lkotlin/reflect/jvm/internal/impl/k/a/c;->b(Lkotlin/reflect/jvm/internal/impl/k/r;Lkotlin/reflect/jvm/internal/impl/k/r;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 243
    const-string v0, "Type parameter number mismatch"

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/h/j$a;->a(Ljava/lang/String;)Lkotlin/reflect/jvm/internal/impl/h/j$a;

    move-result-object v0

    if-nez v0, :cond_d

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "@NotNull method %s.%s must not return null"

    new-array v2, v10, [Ljava/lang/Object;

    const-string v5, "kotlin/reflect/jvm/internal/impl/resolve/OverridingUtil"

    aput-object v5, v2, v3

    const-string v3, "isOverridableByWithoutExternalConditions"

    aput-object v3, v2, v4

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 240
    :cond_3
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 246
    :cond_4
    const-string v0, "Type parameter number mismatch"

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/h/j$a;->b(Ljava/lang/String;)Lkotlin/reflect/jvm/internal/impl/h/j$a;

    move-result-object v0

    if-nez v0, :cond_d

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "@NotNull method %s.%s must not return null"

    new-array v2, v10, [Ljava/lang/Object;

    const-string v5, "kotlin/reflect/jvm/internal/impl/resolve/OverridingUtil"

    aput-object v5, v2, v3

    const-string v3, "isOverridableByWithoutExternalConditions"

    aput-object v3, v2, v4

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 249
    :cond_5
    invoke-direct {p0, v7, v8}, Lkotlin/reflect/jvm/internal/impl/h/j;->a(Ljava/util/List;Ljava/util/List;)Lkotlin/reflect/jvm/internal/impl/k/a/c;

    move-result-object v9

    move v2, v3

    .line 251
    :goto_1
    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_7

    .line 252
    invoke-interface {v7, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/aq;

    invoke-interface {v8, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lkotlin/reflect/jvm/internal/impl/b/aq;

    invoke-static {v0, v1, v9}, Lkotlin/reflect/jvm/internal/impl/h/j;->a(Lkotlin/reflect/jvm/internal/impl/b/aq;Lkotlin/reflect/jvm/internal/impl/b/aq;Lkotlin/reflect/jvm/internal/impl/k/a/c;)Z

    move-result v0

    if-nez v0, :cond_6

    .line 253
    const-string v0, "Type parameter bounds mismatch"

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/h/j$a;->a(Ljava/lang/String;)Lkotlin/reflect/jvm/internal/impl/h/j$a;

    move-result-object v0

    if-nez v0, :cond_d

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "@NotNull method %s.%s must not return null"

    new-array v2, v10, [Ljava/lang/Object;

    const-string v5, "kotlin/reflect/jvm/internal/impl/resolve/OverridingUtil"

    aput-object v5, v2, v3

    const-string v3, "isOverridableByWithoutExternalConditions"

    aput-object v3, v2, v4

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 251
    :cond_6
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    :cond_7
    move v2, v3

    .line 257
    :goto_2
    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_9

    .line 258
    invoke-interface {v5, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/r;

    invoke-interface {v6, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lkotlin/reflect/jvm/internal/impl/k/r;

    invoke-static {v0, v1, v9}, Lkotlin/reflect/jvm/internal/impl/h/j;->a(Lkotlin/reflect/jvm/internal/impl/k/r;Lkotlin/reflect/jvm/internal/impl/k/r;Lkotlin/reflect/jvm/internal/impl/k/a/c;)Z

    move-result v0

    if-nez v0, :cond_8

    .line 259
    const-string v0, "Value parameter type mismatch"

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/h/j$a;->a(Ljava/lang/String;)Lkotlin/reflect/jvm/internal/impl/h/j$a;

    move-result-object v0

    if-nez v0, :cond_d

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "@NotNull method %s.%s must not return null"

    new-array v2, v10, [Ljava/lang/Object;

    const-string v5, "kotlin/reflect/jvm/internal/impl/resolve/OverridingUtil"

    aput-object v5, v2, v3

    const-string v3, "isOverridableByWithoutExternalConditions"

    aput-object v3, v2, v4

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 257
    :cond_8
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_2

    .line 263
    :cond_9
    instance-of v0, p1, Lkotlin/reflect/jvm/internal/impl/b/s;

    if-eqz v0, :cond_a

    instance-of v0, p2, Lkotlin/reflect/jvm/internal/impl/b/s;

    if-eqz v0, :cond_a

    move-object v0, p1

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/s;

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/b/s;->C()Z

    move-result v1

    move-object v0, p2

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/s;

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/b/s;->C()Z

    move-result v0

    if-eq v1, v0, :cond_a

    .line 265
    const-string v0, "Incompatible suspendability"

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/h/j$a;->b(Ljava/lang/String;)Lkotlin/reflect/jvm/internal/impl/h/j$a;

    move-result-object v0

    if-nez v0, :cond_d

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "@NotNull method %s.%s must not return null"

    new-array v2, v10, [Ljava/lang/Object;

    const-string v5, "kotlin/reflect/jvm/internal/impl/resolve/OverridingUtil"

    aput-object v5, v2, v3

    const-string v3, "isOverridableByWithoutExternalConditions"

    aput-object v3, v2, v4

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 268
    :cond_a
    if-eqz p3, :cond_c

    .line 269
    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/b/a;->g()Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v1

    .line 270
    invoke-interface {p2}, Lkotlin/reflect/jvm/internal/impl/b/a;->g()Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v2

    .line 272
    if-eqz v1, :cond_c

    if-eqz v2, :cond_c

    .line 273
    invoke-virtual {v2}, Lkotlin/reflect/jvm/internal/impl/k/r;->d()Z

    move-result v0

    if-eqz v0, :cond_b

    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/impl/k/r;->d()Z

    move-result v0

    if-eqz v0, :cond_b

    move v0, v4

    .line 274
    :goto_3
    if-nez v0, :cond_c

    invoke-interface {v9, v2, v1}, Lkotlin/reflect/jvm/internal/impl/k/a/c;->a(Lkotlin/reflect/jvm/internal/impl/k/r;Lkotlin/reflect/jvm/internal/impl/k/r;)Z

    move-result v0

    if-nez v0, :cond_c

    .line 275
    const-string v0, "Return type mismatch"

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/h/j$a;->b(Ljava/lang/String;)Lkotlin/reflect/jvm/internal/impl/h/j$a;

    move-result-object v0

    if-nez v0, :cond_d

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "@NotNull method %s.%s must not return null"

    new-array v2, v10, [Ljava/lang/Object;

    const-string v5, "kotlin/reflect/jvm/internal/impl/resolve/OverridingUtil"

    aput-object v5, v2, v3

    const-string v3, "isOverridableByWithoutExternalConditions"

    aput-object v3, v2, v4

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_b
    move v0, v3

    .line 273
    goto :goto_3

    .line 280
    :cond_c
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/h/j$a;->a()Lkotlin/reflect/jvm/internal/impl/h/j$a;

    move-result-object v0

    if-nez v0, :cond_d

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "@NotNull method %s.%s must not return null"

    new-array v2, v10, [Ljava/lang/Object;

    const-string v5, "kotlin/reflect/jvm/internal/impl/resolve/OverridingUtil"

    aput-object v5, v2, v3

    const-string v3, "isOverridableByWithoutExternalConditions"

    aput-object v3, v2, v4

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_d
    return-object v0
.end method
