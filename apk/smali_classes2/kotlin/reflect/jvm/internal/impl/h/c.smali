.class public Lkotlin/reflect/jvm/internal/impl/h/c;
.super Ljava/lang/Object;
.source "DescriptorUtils.java"


# static fields
.field public static final a:Lkotlin/reflect/jvm/internal/impl/e/f;

.field public static final b:Lkotlin/reflect/jvm/internal/impl/e/f;

.field public static final c:Lkotlin/reflect/jvm/internal/impl/e/b;

.field public static final d:Lkotlin/reflect/jvm/internal/impl/e/b;

.field public static final e:Lkotlin/reflect/jvm/internal/impl/e/b;

.field static final synthetic f:Z

.field private static final g:Lkotlin/reflect/jvm/internal/impl/e/b;

.field private static final h:Lkotlin/reflect/jvm/internal/impl/e/b;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 48
    const-class v0, Lkotlin/reflect/jvm/internal/impl/h/c;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lkotlin/reflect/jvm/internal/impl/h/c;->f:Z

    .line 49
    const-string v0, "values"

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/e/f;->a(Ljava/lang/String;)Lkotlin/reflect/jvm/internal/impl/e/f;

    move-result-object v0

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/h/c;->a:Lkotlin/reflect/jvm/internal/impl/e/f;

    .line 50
    const-string v0, "valueOf"

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/e/f;->a(Ljava/lang/String;)Lkotlin/reflect/jvm/internal/impl/e/f;

    move-result-object v0

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/h/c;->b:Lkotlin/reflect/jvm/internal/impl/e/f;

    .line 51
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/e/b;

    const-string v1, "kotlin.jvm.JvmName"

    invoke-direct {v0, v1}, Lkotlin/reflect/jvm/internal/impl/e/b;-><init>(Ljava/lang/String;)V

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/h/c;->c:Lkotlin/reflect/jvm/internal/impl/e/b;

    .line 52
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/e/b;

    const-string v1, "kotlin.jvm.Volatile"

    invoke-direct {v0, v1}, Lkotlin/reflect/jvm/internal/impl/e/b;-><init>(Ljava/lang/String;)V

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/h/c;->g:Lkotlin/reflect/jvm/internal/impl/e/b;

    .line 53
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/e/b;

    const-string v1, "kotlin.jvm.Synchronized"

    invoke-direct {v0, v1}, Lkotlin/reflect/jvm/internal/impl/e/b;-><init>(Ljava/lang/String;)V

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/h/c;->h:Lkotlin/reflect/jvm/internal/impl/e/b;

    .line 54
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/e/b;

    const-string v1, "kotlin.coroutines.experimental"

    invoke-direct {v0, v1}, Lkotlin/reflect/jvm/internal/impl/e/b;-><init>(Ljava/lang/String;)V

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/h/c;->d:Lkotlin/reflect/jvm/internal/impl/e/b;

    .line 55
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/h/c;->d:Lkotlin/reflect/jvm/internal/impl/e/b;

    const-string v1, "Continuation"

    invoke-static {v1}, Lkotlin/reflect/jvm/internal/impl/e/f;->a(Ljava/lang/String;)Lkotlin/reflect/jvm/internal/impl/e/f;

    move-result-object v1

    invoke-virtual {v0, v1}, Lkotlin/reflect/jvm/internal/impl/e/b;->a(Lkotlin/reflect/jvm/internal/impl/e/f;)Lkotlin/reflect/jvm/internal/impl/e/b;

    move-result-object v0

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/h/c;->e:Lkotlin/reflect/jvm/internal/impl/e/b;

    return-void

    .line 48
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 57
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 58
    return-void
.end method

.method public static a(Lkotlin/reflect/jvm/internal/impl/b/a;)Ljava/util/Set;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<D::",
            "Lkotlin/reflect/jvm/internal/impl/b/a;",
            ">(TD;)",
            "Ljava/util/Set",
            "<TD;>;"
        }
    .end annotation

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    if-nez p0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "f"

    aput-object v3, v2, v4

    const-string v3, "kotlin/reflect/jvm/internal/impl/resolve/DescriptorUtils"

    aput-object v3, v2, v5

    const-string v3, "getAllOverriddenDescriptors"

    aput-object v3, v2, v6

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 479
    :cond_0
    new-instance v0, Ljava/util/LinkedHashSet;

    invoke-direct {v0}, Ljava/util/LinkedHashSet;-><init>()V

    .line 480
    invoke-interface {p0}, Lkotlin/reflect/jvm/internal/impl/b/a;->h()Lkotlin/reflect/jvm/internal/impl/b/a;

    move-result-object v1

    invoke-static {v1, v0}, Lkotlin/reflect/jvm/internal/impl/h/c;->a(Lkotlin/reflect/jvm/internal/impl/b/a;Ljava/util/Set;)V

    .line 481
    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "@NotNull method %s.%s must not return null"

    new-array v2, v6, [Ljava/lang/Object;

    const-string v3, "kotlin/reflect/jvm/internal/impl/resolve/DescriptorUtils"

    aput-object v3, v2, v4

    const-string v3, "getAllOverriddenDescriptors"

    aput-object v3, v2, v5

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    return-object v0
.end method

.method public static a(Lkotlin/reflect/jvm/internal/impl/b/m;)Lkotlin/reflect/jvm/internal/impl/b/aj;
    .locals 5

    .prologue
    if-nez p0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "containingDeclaration"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "kotlin/reflect/jvm/internal/impl/resolve/DescriptorUtils"

    aput-object v4, v2, v3

    const/4 v3, 0x2

    const-string v4, "getDispatchReceiverParameterIfNeeded"

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 62
    :cond_0
    instance-of v0, p0, Lkotlin/reflect/jvm/internal/impl/b/e;

    if-eqz v0, :cond_1

    .line 63
    check-cast p0, Lkotlin/reflect/jvm/internal/impl/b/e;

    .line 64
    invoke-interface {p0}, Lkotlin/reflect/jvm/internal/impl/b/e;->C()Lkotlin/reflect/jvm/internal/impl/b/aj;

    move-result-object v0

    .line 66
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Lkotlin/reflect/jvm/internal/impl/b/b;)Lkotlin/reflect/jvm/internal/impl/b/b;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<D::",
            "Lkotlin/reflect/jvm/internal/impl/b/b;",
            ">(TD;)TD;"
        }
    .end annotation

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    if-nez p0, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "descriptor"

    aput-object v3, v2, v4

    const-string v3, "kotlin/reflect/jvm/internal/impl/resolve/DescriptorUtils"

    aput-object v3, v2, v5

    const-string v3, "unwrapFakeOverride"

    aput-object v3, v2, v6

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 436
    :cond_0
    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/b;

    move-object p0, v0

    .line 431
    :cond_1
    invoke-interface {p0}, Lkotlin/reflect/jvm/internal/impl/b/b;->n()Lkotlin/reflect/jvm/internal/impl/b/b$a;

    move-result-object v0

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/b/b$a;->b:Lkotlin/reflect/jvm/internal/impl/b/b$a;

    if-ne v0, v1, :cond_2

    .line 432
    invoke-interface {p0}, Lkotlin/reflect/jvm/internal/impl/b/b;->k()Ljava/util/Collection;

    move-result-object v0

    .line 433
    invoke-interface {v0}, Ljava/util/Collection;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 434
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Fake override should have at least one overridden descriptor: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 438
    :cond_2
    if-nez p0, :cond_3

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "@NotNull method %s.%s must not return null"

    new-array v2, v6, [Ljava/lang/Object;

    const-string v3, "kotlin/reflect/jvm/internal/impl/resolve/DescriptorUtils"

    aput-object v3, v2, v4

    const-string v3, "unwrapFakeOverride"

    aput-object v3, v2, v5

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_3
    return-object p0
.end method

.method public static a(Lkotlin/reflect/jvm/internal/impl/b/e;)Lkotlin/reflect/jvm/internal/impl/b/e;
    .locals 5

    .prologue
    if-nez p0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "classDescriptor"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "kotlin/reflect/jvm/internal/impl/resolve/DescriptorUtils"

    aput-object v4, v2, v3

    const/4 v3, 0x2

    const-string v4, "getSuperClassDescriptor"

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 352
    :cond_0
    invoke-interface {p0}, Lkotlin/reflect/jvm/internal/impl/b/e;->e()Lkotlin/reflect/jvm/internal/impl/k/ad;

    move-result-object v0

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/k/ad;->G_()Ljava/util/Collection;

    move-result-object v0

    .line 353
    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/r;

    .line 354
    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/h/c;->a(Lkotlin/reflect/jvm/internal/impl/k/r;)Lkotlin/reflect/jvm/internal/impl/b/e;

    move-result-object v0

    .line 355
    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/b/e;->l()Lkotlin/reflect/jvm/internal/impl/b/f;

    move-result-object v2

    sget-object v3, Lkotlin/reflect/jvm/internal/impl/b/f;->b:Lkotlin/reflect/jvm/internal/impl/b/f;

    if-eq v2, v3, :cond_1

    .line 359
    :goto_0
    return-object v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Lkotlin/reflect/jvm/internal/impl/k/ad;)Lkotlin/reflect/jvm/internal/impl/b/e;
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    if-nez p0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "typeConstructor"

    aput-object v3, v2, v4

    const-string v3, "kotlin/reflect/jvm/internal/impl/resolve/DescriptorUtils"

    aput-object v3, v2, v5

    const-string v3, "getClassDescriptorForTypeConstructor"

    aput-object v3, v2, v6

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 369
    :cond_0
    invoke-interface {p0}, Lkotlin/reflect/jvm/internal/impl/k/ad;->d()Lkotlin/reflect/jvm/internal/impl/b/h;

    move-result-object v0

    .line 371
    sget-boolean v1, Lkotlin/reflect/jvm/internal/impl/h/c;->f:Z

    if-nez v1, :cond_1

    instance-of v1, v0, Lkotlin/reflect/jvm/internal/impl/b/e;

    if-nez v1, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Classifier descriptor of a type should be of type ClassDescriptor: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 372
    :cond_1
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/e;

    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "@NotNull method %s.%s must not return null"

    new-array v2, v6, [Ljava/lang/Object;

    const-string v3, "kotlin/reflect/jvm/internal/impl/resolve/DescriptorUtils"

    aput-object v3, v2, v4

    const-string v3, "getClassDescriptorForTypeConstructor"

    aput-object v3, v2, v5

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    return-object v0
.end method

.method public static a(Lkotlin/reflect/jvm/internal/impl/k/r;)Lkotlin/reflect/jvm/internal/impl/b/e;
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    if-nez p0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "type"

    aput-object v3, v2, v4

    const-string v3, "kotlin/reflect/jvm/internal/impl/resolve/DescriptorUtils"

    aput-object v3, v2, v5

    const-string v3, "getClassDescriptorForType"

    aput-object v3, v2, v6

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 364
    :cond_0
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/k/r;->i()Lkotlin/reflect/jvm/internal/impl/k/ad;

    move-result-object v0

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/h/c;->a(Lkotlin/reflect/jvm/internal/impl/k/ad;)Lkotlin/reflect/jvm/internal/impl/b/e;

    move-result-object v0

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "@NotNull method %s.%s must not return null"

    new-array v2, v6, [Ljava/lang/Object;

    const-string v3, "kotlin/reflect/jvm/internal/impl/resolve/DescriptorUtils"

    aput-object v3, v2, v4

    const-string v3, "getClassDescriptorForType"

    aput-object v3, v2, v5

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    return-object v0
.end method

.method public static a(Lkotlin/reflect/jvm/internal/impl/b/m;Ljava/lang/Class;)Lkotlin/reflect/jvm/internal/impl/b/m;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<D::",
            "Lkotlin/reflect/jvm/internal/impl/b/m;",
            ">(",
            "Lkotlin/reflect/jvm/internal/impl/b/m;",
            "Ljava/lang/Class",
            "<TD;>;)TD;"
        }
    .end annotation

    .prologue
    const/4 v5, 0x1

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "aClass"

    aput-object v4, v2, v3

    const-string v3, "kotlin/reflect/jvm/internal/impl/resolve/DescriptorUtils"

    aput-object v3, v2, v5

    const/4 v3, 0x2

    const-string v4, "getParentOfType"

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 169
    :cond_0
    invoke-static {p0, p1, v5}, Lkotlin/reflect/jvm/internal/impl/h/c;->a(Lkotlin/reflect/jvm/internal/impl/b/m;Ljava/lang/Class;Z)Lkotlin/reflect/jvm/internal/impl/b/m;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lkotlin/reflect/jvm/internal/impl/b/m;Ljava/lang/Class;Z)Lkotlin/reflect/jvm/internal/impl/b/m;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<D::",
            "Lkotlin/reflect/jvm/internal/impl/b/m;",
            ">(",
            "Lkotlin/reflect/jvm/internal/impl/b/m;",
            "Ljava/lang/Class",
            "<TD;>;Z)TD;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "aClass"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "kotlin/reflect/jvm/internal/impl/resolve/DescriptorUtils"

    aput-object v4, v2, v3

    const/4 v3, 0x2

    const-string v4, "getParentOfType"

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 179
    :cond_0
    if-nez p0, :cond_2

    .line 189
    :cond_1
    :goto_0
    return-object v0

    .line 180
    :cond_2
    if-eqz p2, :cond_4

    .line 181
    invoke-interface {p0}, Lkotlin/reflect/jvm/internal/impl/b/m;->y_()Lkotlin/reflect/jvm/internal/impl/b/m;

    move-result-object v1

    .line 183
    :goto_1
    if-eqz v1, :cond_1

    .line 184
    invoke-virtual {p1, v1}, Ljava/lang/Class;->isInstance(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    move-object v0, v1

    .line 185
    goto :goto_0

    .line 187
    :cond_3
    invoke-interface {v1}, Lkotlin/reflect/jvm/internal/impl/b/m;->y_()Lkotlin/reflect/jvm/internal/impl/b/m;

    move-result-object v1

    goto :goto_1

    :cond_4
    move-object v1, p0

    goto :goto_1
.end method

.method public static a(Lkotlin/reflect/jvm/internal/impl/b/q;)Lkotlin/reflect/jvm/internal/impl/b/q;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<D::",
            "Lkotlin/reflect/jvm/internal/impl/b/q;",
            ">(TD;)TD;"
        }
    .end annotation

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    if-nez p0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "descriptor"

    aput-object v3, v2, v4

    const-string v3, "kotlin/reflect/jvm/internal/impl/resolve/DescriptorUtils"

    aput-object v3, v2, v5

    const-string v3, "unwrapFakeOverrideToAnyDeclaration"

    aput-object v3, v2, v6

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 444
    :cond_0
    instance-of v0, p0, Lkotlin/reflect/jvm/internal/impl/b/b;

    if-eqz v0, :cond_1

    .line 445
    check-cast p0, Lkotlin/reflect/jvm/internal/impl/b/b;

    invoke-static {p0}, Lkotlin/reflect/jvm/internal/impl/h/c;->a(Lkotlin/reflect/jvm/internal/impl/b/b;)Lkotlin/reflect/jvm/internal/impl/b/b;

    move-result-object p0

    if-nez p0, :cond_2

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "@NotNull method %s.%s must not return null"

    new-array v2, v6, [Ljava/lang/Object;

    const-string v3, "kotlin/reflect/jvm/internal/impl/resolve/DescriptorUtils"

    aput-object v3, v2, v4

    const-string v3, "unwrapFakeOverrideToAnyDeclaration"

    aput-object v3, v2, v5

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 448
    :cond_1
    if-nez p0, :cond_2

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "@NotNull method %s.%s must not return null"

    new-array v2, v6, [Ljava/lang/Object;

    const-string v3, "kotlin/reflect/jvm/internal/impl/resolve/DescriptorUtils"

    aput-object v3, v2, v4

    const-string v3, "unwrapFakeOverrideToAnyDeclaration"

    aput-object v3, v2, v5

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    return-object p0
.end method

.method private static a(Lkotlin/reflect/jvm/internal/impl/b/a;Ljava/util/Set;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<D::",
            "Lkotlin/reflect/jvm/internal/impl/b/a;",
            ">(TD;",
            "Ljava/util/Set",
            "<TD;>;)V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    if-nez p0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "current"

    aput-object v3, v2, v4

    const-string v3, "kotlin/reflect/jvm/internal/impl/resolve/DescriptorUtils"

    aput-object v3, v2, v5

    const-string v3, "collectAllOverriddenDescriptors"

    aput-object v3, v2, v6

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    if-nez p1, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "result"

    aput-object v3, v2, v4

    const-string v3, "kotlin/reflect/jvm/internal/impl/resolve/DescriptorUtils"

    aput-object v3, v2, v5

    const-string v3, "collectAllOverriddenDescriptors"

    aput-object v3, v2, v6

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 485
    :cond_1
    invoke-interface {p1, p0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 492
    :cond_2
    return-void

    .line 486
    :cond_3
    invoke-interface {p0}, Lkotlin/reflect/jvm/internal/impl/b/a;->h()Lkotlin/reflect/jvm/internal/impl/b/a;

    move-result-object v0

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/b/a;->k()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/a;

    .line 488
    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/b/a;->h()Lkotlin/reflect/jvm/internal/impl/b/a;

    move-result-object v0

    .line 489
    invoke-static {v0, p1}, Lkotlin/reflect/jvm/internal/impl/h/c;->a(Lkotlin/reflect/jvm/internal/impl/b/a;Ljava/util/Set;)V

    .line 490
    invoke-interface {p1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public static a(Lkotlin/reflect/jvm/internal/impl/b/av;Lkotlin/reflect/jvm/internal/impl/k/r;)Z
    .locals 7

    .prologue
    const/4 v4, 0x3

    const/4 v6, 0x2

    const/4 v1, 0x1

    const/4 v0, 0x0

    if-nez p0, :cond_0

    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    new-array v4, v4, [Ljava/lang/Object;

    const-string v5, "variable"

    aput-object v5, v4, v0

    const-string v0, "kotlin/reflect/jvm/internal/impl/resolve/DescriptorUtils"

    aput-object v0, v4, v1

    const-string v0, "shouldRecordInitializerForProperty"

    aput-object v0, v4, v6

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_0
    if-nez p1, :cond_1

    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    new-array v4, v4, [Ljava/lang/Object;

    const-string v5, "type"

    aput-object v5, v4, v0

    const-string v0, "kotlin/reflect/jvm/internal/impl/resolve/DescriptorUtils"

    aput-object v0, v4, v1

    const-string v0, "shouldRecordInitializerForProperty"

    aput-object v0, v4, v6

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 452
    :cond_1
    invoke-interface {p0}, Lkotlin/reflect/jvm/internal/impl/b/av;->y()Z

    move-result v2

    if-nez v2, :cond_2

    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/k/r;->d()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 457
    :cond_2
    :goto_0
    return v0

    .line 454
    :cond_3
    invoke-static {p1}, Lkotlin/reflect/jvm/internal/impl/k/an;->f(Lkotlin/reflect/jvm/internal/impl/k/r;)Z

    move-result v2

    if-eqz v2, :cond_4

    move v0, v1

    goto :goto_0

    .line 456
    :cond_4
    invoke-static {p0}, Lkotlin/reflect/jvm/internal/impl/h/c/a;->d(Lkotlin/reflect/jvm/internal/impl/b/m;)Lkotlin/reflect/jvm/internal/impl/a/l;

    move-result-object v2

    .line 457
    invoke-static {p1}, Lkotlin/reflect/jvm/internal/impl/a/l;->e(Lkotlin/reflect/jvm/internal/impl/k/r;)Z

    move-result v3

    if-nez v3, :cond_5

    sget-object v3, Lkotlin/reflect/jvm/internal/impl/k/a/c;->a:Lkotlin/reflect/jvm/internal/impl/k/a/c;

    invoke-virtual {v2}, Lkotlin/reflect/jvm/internal/impl/a/l;->J()Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v4

    invoke-interface {v3, v4, p1}, Lkotlin/reflect/jvm/internal/impl/k/a/c;->b(Lkotlin/reflect/jvm/internal/impl/k/r;Lkotlin/reflect/jvm/internal/impl/k/r;)Z

    move-result v3

    if-nez v3, :cond_5

    sget-object v3, Lkotlin/reflect/jvm/internal/impl/k/a/c;->a:Lkotlin/reflect/jvm/internal/impl/k/a/c;

    invoke-virtual {v2}, Lkotlin/reflect/jvm/internal/impl/a/l;->m()Lkotlin/reflect/jvm/internal/impl/b/e;

    move-result-object v4

    invoke-interface {v4}, Lkotlin/reflect/jvm/internal/impl/b/e;->E_()Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v4

    invoke-interface {v3, v4, p1}, Lkotlin/reflect/jvm/internal/impl/k/a/c;->b(Lkotlin/reflect/jvm/internal/impl/k/r;Lkotlin/reflect/jvm/internal/impl/k/r;)Z

    move-result v3

    if-nez v3, :cond_5

    sget-object v3, Lkotlin/reflect/jvm/internal/impl/k/a/c;->a:Lkotlin/reflect/jvm/internal/impl/k/a/c;

    invoke-virtual {v2}, Lkotlin/reflect/jvm/internal/impl/a/l;->x()Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v2

    invoke-interface {v3, v2, p1}, Lkotlin/reflect/jvm/internal/impl/k/a/c;->b(Lkotlin/reflect/jvm/internal/impl/k/r;Lkotlin/reflect/jvm/internal/impl/k/r;)Z

    move-result v2

    if-eqz v2, :cond_2

    :cond_5
    move v0, v1

    goto :goto_0
.end method

.method public static a(Lkotlin/reflect/jvm/internal/impl/b/e;Lkotlin/reflect/jvm/internal/impl/b/e;)Z
    .locals 7

    .prologue
    const/4 v4, 0x3

    const/4 v6, 0x2

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-nez p0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v3, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    new-array v4, v4, [Ljava/lang/Object;

    const-string v5, "subClass"

    aput-object v5, v4, v2

    const-string v2, "kotlin/reflect/jvm/internal/impl/resolve/DescriptorUtils"

    aput-object v2, v4, v1

    const-string v1, "isDirectSubclass"

    aput-object v1, v4, v6

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    if-nez p1, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v3, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    new-array v4, v4, [Ljava/lang/Object;

    const-string v5, "superClass"

    aput-object v5, v4, v2

    const-string v2, "kotlin/reflect/jvm/internal/impl/resolve/DescriptorUtils"

    aput-object v2, v4, v1

    const-string v1, "isDirectSubclass"

    aput-object v1, v4, v6

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 241
    :cond_1
    invoke-interface {p0}, Lkotlin/reflect/jvm/internal/impl/b/e;->e()Lkotlin/reflect/jvm/internal/impl/k/ad;

    move-result-object v0

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/k/ad;->G_()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/r;

    .line 242
    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/b/e;->D()Lkotlin/reflect/jvm/internal/impl/b/e;

    move-result-object v4

    invoke-static {v0, v4}, Lkotlin/reflect/jvm/internal/impl/h/c;->b(Lkotlin/reflect/jvm/internal/impl/k/r;Lkotlin/reflect/jvm/internal/impl/b/m;)Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v1

    .line 246
    :goto_0
    return v0

    :cond_3
    move v0, v2

    goto :goto_0
.end method

.method private static a(Lkotlin/reflect/jvm/internal/impl/b/m;Lkotlin/reflect/jvm/internal/impl/b/f;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    if-nez p1, :cond_0

    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/Object;

    const-string v5, "classKind"

    aput-object v5, v4, v1

    const-string v1, "kotlin/reflect/jvm/internal/impl/resolve/DescriptorUtils"

    aput-object v1, v4, v0

    const/4 v0, 0x2

    const-string v1, "isKindOf"

    aput-object v1, v4, v0

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 322
    :cond_0
    instance-of v2, p0, Lkotlin/reflect/jvm/internal/impl/b/e;

    if-eqz v2, :cond_1

    check-cast p0, Lkotlin/reflect/jvm/internal/impl/b/e;

    invoke-interface {p0}, Lkotlin/reflect/jvm/internal/impl/b/e;->l()Lkotlin/reflect/jvm/internal/impl/b/f;

    move-result-object v2

    if-ne v2, p1, :cond_1

    :goto_0
    return v0

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public static a(Lkotlin/reflect/jvm/internal/impl/b/m;Lkotlin/reflect/jvm/internal/impl/b/m;)Z
    .locals 7

    .prologue
    const/4 v2, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    if-nez p0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "first"

    aput-object v3, v2, v4

    const-string v3, "kotlin/reflect/jvm/internal/impl/resolve/DescriptorUtils"

    aput-object v3, v2, v5

    const-string v3, "areInSameModule"

    aput-object v3, v2, v6

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    if-nez p1, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "second"

    aput-object v3, v2, v4

    const-string v3, "kotlin/reflect/jvm/internal/impl/resolve/DescriptorUtils"

    aput-object v3, v2, v5

    const-string v3, "areInSameModule"

    aput-object v3, v2, v6

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 161
    :cond_1
    invoke-static {p0}, Lkotlin/reflect/jvm/internal/impl/h/c;->g(Lkotlin/reflect/jvm/internal/impl/b/m;)Lkotlin/reflect/jvm/internal/impl/b/w;

    move-result-object v0

    invoke-static {p1}, Lkotlin/reflect/jvm/internal/impl/h/c;->g(Lkotlin/reflect/jvm/internal/impl/b/m;)Lkotlin/reflect/jvm/internal/impl/b/w;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public static a(Lkotlin/reflect/jvm/internal/impl/k/r;Lkotlin/reflect/jvm/internal/impl/b/m;)Z
    .locals 7

    .prologue
    const/4 v4, 0x3

    const/4 v6, 0x2

    const/4 v2, 0x0

    const/4 v1, 0x1

    if-nez p0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v3, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    new-array v4, v4, [Ljava/lang/Object;

    const-string v5, "type"

    aput-object v5, v4, v2

    const-string v2, "kotlin/reflect/jvm/internal/impl/resolve/DescriptorUtils"

    aput-object v2, v4, v1

    const-string v1, "isSubtypeOfClass"

    aput-object v1, v4, v6

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    if-nez p1, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v3, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    new-array v4, v4, [Ljava/lang/Object;

    const-string v5, "superClass"

    aput-object v5, v4, v2

    const-string v2, "kotlin/reflect/jvm/internal/impl/resolve/DescriptorUtils"

    aput-object v2, v4, v1

    const-string v1, "isSubtypeOfClass"

    aput-object v1, v4, v6

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 268
    :cond_1
    invoke-static {p0, p1}, Lkotlin/reflect/jvm/internal/impl/h/c;->b(Lkotlin/reflect/jvm/internal/impl/k/r;Lkotlin/reflect/jvm/internal/impl/b/m;)Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v1

    .line 274
    :goto_0
    return v0

    .line 269
    :cond_2
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/k/r;->i()Lkotlin/reflect/jvm/internal/impl/k/ad;

    move-result-object v0

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/k/ad;->G_()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_3
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/r;

    .line 270
    invoke-static {v0, p1}, Lkotlin/reflect/jvm/internal/impl/h/c;->a(Lkotlin/reflect/jvm/internal/impl/k/r;Lkotlin/reflect/jvm/internal/impl/b/m;)Z

    move-result v0

    if-eqz v0, :cond_3

    move v0, v1

    .line 271
    goto :goto_0

    :cond_4
    move v0, v2

    .line 274
    goto :goto_0
.end method

.method public static b(Lkotlin/reflect/jvm/internal/impl/b/e;)Lkotlin/reflect/jvm/internal/impl/b/ay;
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    if-nez p0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "classDescriptor"

    aput-object v3, v2, v4

    const-string v3, "kotlin/reflect/jvm/internal/impl/resolve/DescriptorUtils"

    aput-object v3, v2, v5

    const-string v3, "getDefaultConstructorVisibility"

    aput-object v3, v2, v6

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 377
    :cond_0
    invoke-interface {p0}, Lkotlin/reflect/jvm/internal/impl/b/e;->l()Lkotlin/reflect/jvm/internal/impl/b/f;

    move-result-object v0

    .line 378
    sget-object v1, Lkotlin/reflect/jvm/internal/impl/b/f;->c:Lkotlin/reflect/jvm/internal/impl/b/f;

    if-eq v0, v1, :cond_1

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/b/f;->a()Z

    move-result v1

    if-nez v1, :cond_1

    invoke-static {p0}, Lkotlin/reflect/jvm/internal/impl/h/c;->j(Lkotlin/reflect/jvm/internal/impl/b/m;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 379
    :cond_1
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/b/ax;->a:Lkotlin/reflect/jvm/internal/impl/b/ay;

    if-nez v0, :cond_5

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "@NotNull method %s.%s must not return null"

    new-array v2, v6, [Ljava/lang/Object;

    const-string v3, "kotlin/reflect/jvm/internal/impl/resolve/DescriptorUtils"

    aput-object v3, v2, v4

    const-string v3, "getDefaultConstructorVisibility"

    aput-object v3, v2, v5

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 381
    :cond_2
    invoke-static {p0}, Lkotlin/reflect/jvm/internal/impl/h/c;->k(Lkotlin/reflect/jvm/internal/impl/b/m;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 382
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/b/ax;->k:Lkotlin/reflect/jvm/internal/impl/b/ay;

    if-nez v0, :cond_5

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "@NotNull method %s.%s must not return null"

    new-array v2, v6, [Ljava/lang/Object;

    const-string v3, "kotlin/reflect/jvm/internal/impl/resolve/DescriptorUtils"

    aput-object v3, v2, v4

    const-string v3, "getDefaultConstructorVisibility"

    aput-object v3, v2, v5

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 384
    :cond_3
    sget-boolean v1, Lkotlin/reflect/jvm/internal/impl/h/c;->f:Z

    if-nez v1, :cond_4

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/b/f;->a:Lkotlin/reflect/jvm/internal/impl/b/f;

    if-eq v0, v1, :cond_4

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/b/f;->b:Lkotlin/reflect/jvm/internal/impl/b/f;

    if-eq v0, v1, :cond_4

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/b/f;->e:Lkotlin/reflect/jvm/internal/impl/b/f;

    if-eq v0, v1, :cond_4

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 385
    :cond_4
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/b/ax;->e:Lkotlin/reflect/jvm/internal/impl/b/ay;

    if-nez v0, :cond_5

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "@NotNull method %s.%s must not return null"

    new-array v2, v6, [Ljava/lang/Object;

    const-string v3, "kotlin/reflect/jvm/internal/impl/resolve/DescriptorUtils"

    aput-object v3, v2, v4

    const-string v3, "getDefaultConstructorVisibility"

    aput-object v3, v2, v5

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_5
    return-object v0
.end method

.method public static b(Lkotlin/reflect/jvm/internal/impl/b/b;)Lkotlin/reflect/jvm/internal/impl/b/b;
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    if-nez p0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "descriptor"

    aput-object v3, v2, v4

    const-string v3, "kotlin/reflect/jvm/internal/impl/resolve/DescriptorUtils"

    aput-object v3, v2, v5

    const-string v3, "getDirectMember"

    aput-object v3, v2, v6

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 619
    :cond_0
    instance-of v0, p0, Lkotlin/reflect/jvm/internal/impl/b/af;

    if-eqz v0, :cond_1

    check-cast p0, Lkotlin/reflect/jvm/internal/impl/b/af;

    invoke-interface {p0}, Lkotlin/reflect/jvm/internal/impl/b/af;->q()Lkotlin/reflect/jvm/internal/impl/b/ag;

    move-result-object p0

    :cond_1
    if-nez p0, :cond_2

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "@NotNull method %s.%s must not return null"

    new-array v2, v6, [Ljava/lang/Object;

    const-string v3, "kotlin/reflect/jvm/internal/impl/resolve/DescriptorUtils"

    aput-object v3, v2, v4

    const-string v3, "getDirectMember"

    aput-object v3, v2, v5

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    return-object p0
.end method

.method public static b(Lkotlin/reflect/jvm/internal/impl/b/e;Lkotlin/reflect/jvm/internal/impl/b/e;)Z
    .locals 7

    .prologue
    const/4 v2, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    if-nez p0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "subClass"

    aput-object v3, v2, v4

    const-string v3, "kotlin/reflect/jvm/internal/impl/resolve/DescriptorUtils"

    aput-object v3, v2, v5

    const-string v3, "isSubclass"

    aput-object v3, v2, v6

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    if-nez p1, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "superClass"

    aput-object v3, v2, v4

    const-string v3, "kotlin/reflect/jvm/internal/impl/resolve/DescriptorUtils"

    aput-object v3, v2, v5

    const-string v3, "isSubclass"

    aput-object v3, v2, v6

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 250
    :cond_1
    invoke-interface {p0}, Lkotlin/reflect/jvm/internal/impl/b/e;->E_()Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v0

    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/b/e;->D()Lkotlin/reflect/jvm/internal/impl/b/e;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/reflect/jvm/internal/impl/h/c;->a(Lkotlin/reflect/jvm/internal/impl/k/r;Lkotlin/reflect/jvm/internal/impl/b/m;)Z

    move-result v0

    return v0
.end method

.method public static b(Lkotlin/reflect/jvm/internal/impl/b/m;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    if-nez p0, :cond_1

    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/Object;

    const-string v5, "descriptor"

    aput-object v5, v4, v1

    const-string v1, "kotlin/reflect/jvm/internal/impl/resolve/DescriptorUtils"

    aput-object v1, v4, v0

    const/4 v0, 0x2

    const-string v1, "isLocal"

    aput-object v1, v4, v0

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 78
    :cond_0
    invoke-interface {p0}, Lkotlin/reflect/jvm/internal/impl/b/m;->y_()Lkotlin/reflect/jvm/internal/impl/b/m;

    move-result-object p0

    .line 74
    :cond_1
    if-eqz p0, :cond_3

    .line 75
    invoke-static {p0}, Lkotlin/reflect/jvm/internal/impl/h/c;->k(Lkotlin/reflect/jvm/internal/impl/b/m;)Z

    move-result v2

    if-nez v2, :cond_2

    invoke-static {p0}, Lkotlin/reflect/jvm/internal/impl/h/c;->c(Lkotlin/reflect/jvm/internal/impl/b/m;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 80
    :cond_2
    :goto_0
    return v0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method private static b(Lkotlin/reflect/jvm/internal/impl/k/r;Lkotlin/reflect/jvm/internal/impl/b/m;)Z
    .locals 7

    .prologue
    const/4 v4, 0x3

    const/4 v6, 0x2

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-nez p0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v3, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    new-array v4, v4, [Ljava/lang/Object;

    const-string v5, "type"

    aput-object v5, v4, v2

    const-string v2, "kotlin/reflect/jvm/internal/impl/resolve/DescriptorUtils"

    aput-object v2, v4, v1

    const-string v1, "isSameClass"

    aput-object v1, v4, v6

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    if-nez p1, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v3, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    new-array v4, v4, [Ljava/lang/Object;

    const-string v5, "other"

    aput-object v5, v4, v2

    const-string v2, "kotlin/reflect/jvm/internal/impl/resolve/DescriptorUtils"

    aput-object v2, v4, v1

    const-string v1, "isSameClass"

    aput-object v1, v4, v6

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 254
    :cond_1
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/k/r;->i()Lkotlin/reflect/jvm/internal/impl/k/ad;

    move-result-object v0

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/k/ad;->d()Lkotlin/reflect/jvm/internal/impl/b/h;

    move-result-object v0

    .line 255
    if-eqz v0, :cond_2

    .line 256
    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/b/m;->z_()Lkotlin/reflect/jvm/internal/impl/b/m;

    move-result-object v0

    .line 257
    instance-of v3, v0, Lkotlin/reflect/jvm/internal/impl/b/h;

    if-eqz v3, :cond_2

    instance-of v3, p1, Lkotlin/reflect/jvm/internal/impl/b/h;

    if-eqz v3, :cond_2

    check-cast p1, Lkotlin/reflect/jvm/internal/impl/b/h;

    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/b/h;->e()Lkotlin/reflect/jvm/internal/impl/k/ad;

    move-result-object v3

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/h;

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/b/h;->e()Lkotlin/reflect/jvm/internal/impl/k/ad;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v1

    .line 264
    :goto_0
    return v0

    :cond_2
    move v0, v2

    goto :goto_0
.end method

.method public static c(Lkotlin/reflect/jvm/internal/impl/b/m;)Z
    .locals 2

    .prologue
    .line 84
    instance-of v0, p0, Lkotlin/reflect/jvm/internal/impl/b/q;

    if-eqz v0, :cond_0

    check-cast p0, Lkotlin/reflect/jvm/internal/impl/b/q;

    invoke-interface {p0}, Lkotlin/reflect/jvm/internal/impl/b/q;->p()Lkotlin/reflect/jvm/internal/impl/b/ay;

    move-result-object v0

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/b/ax;->f:Lkotlin/reflect/jvm/internal/impl/b/ay;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static d(Lkotlin/reflect/jvm/internal/impl/b/m;)Lkotlin/reflect/jvm/internal/impl/e/c;
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    if-nez p0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "descriptor"

    aput-object v3, v2, v4

    const-string v3, "kotlin/reflect/jvm/internal/impl/resolve/DescriptorUtils"

    aput-object v3, v2, v5

    const-string v3, "getFqName"

    aput-object v3, v2, v6

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 90
    :cond_0
    invoke-static {p0}, Lkotlin/reflect/jvm/internal/impl/h/c;->s(Lkotlin/reflect/jvm/internal/impl/b/m;)Lkotlin/reflect/jvm/internal/impl/e/b;

    move-result-object v0

    .line 91
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/e/b;->b()Lkotlin/reflect/jvm/internal/impl/e/c;

    move-result-object v0

    :goto_0
    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "@NotNull method %s.%s must not return null"

    new-array v2, v6, [Ljava/lang/Object;

    const-string v3, "kotlin/reflect/jvm/internal/impl/resolve/DescriptorUtils"

    aput-object v3, v2, v4

    const-string v3, "getFqName"

    aput-object v3, v2, v5

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    invoke-static {p0}, Lkotlin/reflect/jvm/internal/impl/h/c;->t(Lkotlin/reflect/jvm/internal/impl/b/m;)Lkotlin/reflect/jvm/internal/impl/e/c;

    move-result-object v0

    goto :goto_0

    :cond_2
    return-object v0
.end method

.method public static e(Lkotlin/reflect/jvm/internal/impl/b/m;)Lkotlin/reflect/jvm/internal/impl/e/b;
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    if-nez p0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "descriptor"

    aput-object v3, v2, v4

    const-string v3, "kotlin/reflect/jvm/internal/impl/resolve/DescriptorUtils"

    aput-object v3, v2, v5

    const-string v3, "getFqNameSafe"

    aput-object v3, v2, v6

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 96
    :cond_0
    invoke-static {p0}, Lkotlin/reflect/jvm/internal/impl/h/c;->s(Lkotlin/reflect/jvm/internal/impl/b/m;)Lkotlin/reflect/jvm/internal/impl/e/b;

    move-result-object v0

    .line 97
    if-eqz v0, :cond_1

    :goto_0
    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "@NotNull method %s.%s must not return null"

    new-array v2, v6, [Ljava/lang/Object;

    const-string v3, "kotlin/reflect/jvm/internal/impl/resolve/DescriptorUtils"

    aput-object v3, v2, v4

    const-string v3, "getFqNameSafe"

    aput-object v3, v2, v5

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    invoke-static {p0}, Lkotlin/reflect/jvm/internal/impl/h/c;->t(Lkotlin/reflect/jvm/internal/impl/b/m;)Lkotlin/reflect/jvm/internal/impl/e/c;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/e/c;->c()Lkotlin/reflect/jvm/internal/impl/e/b;

    move-result-object v0

    goto :goto_0

    :cond_2
    return-object v0
.end method

.method public static f(Lkotlin/reflect/jvm/internal/impl/b/m;)Z
    .locals 1

    .prologue
    .line 135
    if-eqz p0, :cond_0

    invoke-interface {p0}, Lkotlin/reflect/jvm/internal/impl/b/m;->y_()Lkotlin/reflect/jvm/internal/impl/b/m;

    move-result-object v0

    instance-of v0, v0, Lkotlin/reflect/jvm/internal/impl/b/y;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static g(Lkotlin/reflect/jvm/internal/impl/b/m;)Lkotlin/reflect/jvm/internal/impl/b/w;
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    if-nez p0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "descriptor"

    aput-object v3, v2, v4

    const-string v3, "kotlin/reflect/jvm/internal/impl/resolve/DescriptorUtils"

    aput-object v3, v2, v5

    const-string v3, "getContainingModule"

    aput-object v3, v2, v6

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 194
    :cond_0
    invoke-static {p0}, Lkotlin/reflect/jvm/internal/impl/h/c;->h(Lkotlin/reflect/jvm/internal/impl/b/m;)Lkotlin/reflect/jvm/internal/impl/b/w;

    move-result-object v0

    .line 195
    sget-boolean v1, Lkotlin/reflect/jvm/internal/impl/h/c;->f:Z

    if-nez v1, :cond_1

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Descriptor without a containing module: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 196
    :cond_1
    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "@NotNull method %s.%s must not return null"

    new-array v2, v6, [Ljava/lang/Object;

    const-string v3, "kotlin/reflect/jvm/internal/impl/resolve/DescriptorUtils"

    aput-object v3, v2, v4

    const-string v3, "getContainingModule"

    aput-object v3, v2, v5

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    return-object v0
.end method

.method public static h(Lkotlin/reflect/jvm/internal/impl/b/m;)Lkotlin/reflect/jvm/internal/impl/b/w;
    .locals 5

    .prologue
    if-nez p0, :cond_3

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "descriptor"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "kotlin/reflect/jvm/internal/impl/resolve/DescriptorUtils"

    aput-object v4, v2, v3

    const/4 v3, 0x2

    const-string v4, "getContainingModuleOrNull"

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 209
    :cond_0
    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/b/m;->y_()Lkotlin/reflect/jvm/internal/impl/b/m;

    move-result-object v0

    .line 201
    :goto_0
    if-eqz v0, :cond_2

    .line 202
    instance-of v1, v0, Lkotlin/reflect/jvm/internal/impl/b/w;

    if-eqz v1, :cond_1

    .line 203
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/w;

    .line 211
    :goto_1
    return-object v0

    .line 205
    :cond_1
    instance-of v1, v0, Lkotlin/reflect/jvm/internal/impl/b/ac;

    if-eqz v1, :cond_0

    .line 206
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/ac;

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/b/ac;->e()Lkotlin/reflect/jvm/internal/impl/b/w;

    move-result-object v0

    goto :goto_1

    .line 211
    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    :cond_3
    move-object v0, p0

    goto :goto_0
.end method

.method public static i(Lkotlin/reflect/jvm/internal/impl/b/m;)Z
    .locals 1

    .prologue
    .line 278
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/b/f;->f:Lkotlin/reflect/jvm/internal/impl/b/f;

    invoke-static {p0, v0}, Lkotlin/reflect/jvm/internal/impl/h/c;->a(Lkotlin/reflect/jvm/internal/impl/b/m;Lkotlin/reflect/jvm/internal/impl/b/f;)Z

    move-result v0

    if-eqz v0, :cond_0

    check-cast p0, Lkotlin/reflect/jvm/internal/impl/b/e;

    invoke-interface {p0}, Lkotlin/reflect/jvm/internal/impl/b/e;->q()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static j(Lkotlin/reflect/jvm/internal/impl/b/m;)Z
    .locals 2

    .prologue
    .line 282
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/b/f;->a:Lkotlin/reflect/jvm/internal/impl/b/f;

    invoke-static {p0, v0}, Lkotlin/reflect/jvm/internal/impl/h/c;->a(Lkotlin/reflect/jvm/internal/impl/b/m;Lkotlin/reflect/jvm/internal/impl/b/f;)Z

    move-result v0

    if-eqz v0, :cond_0

    check-cast p0, Lkotlin/reflect/jvm/internal/impl/b/e;

    invoke-interface {p0}, Lkotlin/reflect/jvm/internal/impl/b/e;->m()Lkotlin/reflect/jvm/internal/impl/b/u;

    move-result-object v0

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/b/u;->b:Lkotlin/reflect/jvm/internal/impl/b/u;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static k(Lkotlin/reflect/jvm/internal/impl/b/m;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    if-nez p0, :cond_0

    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/Object;

    const-string v5, "descriptor"

    aput-object v5, v4, v1

    const-string v1, "kotlin/reflect/jvm/internal/impl/resolve/DescriptorUtils"

    aput-object v1, v4, v0

    const/4 v0, 0x2

    const-string v1, "isAnonymousObject"

    aput-object v1, v4, v0

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 286
    :cond_0
    invoke-static {p0}, Lkotlin/reflect/jvm/internal/impl/h/c;->p(Lkotlin/reflect/jvm/internal/impl/b/m;)Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {p0}, Lkotlin/reflect/jvm/internal/impl/b/m;->D_()Lkotlin/reflect/jvm/internal/impl/e/f;

    move-result-object v2

    sget-object v3, Lkotlin/reflect/jvm/internal/impl/e/h;->a:Lkotlin/reflect/jvm/internal/impl/e/f;

    invoke-virtual {v2, v3}, Lkotlin/reflect/jvm/internal/impl/e/f;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    :goto_0
    return v0

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public static l(Lkotlin/reflect/jvm/internal/impl/b/m;)Z
    .locals 5

    .prologue
    if-nez p0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "descriptor"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "kotlin/reflect/jvm/internal/impl/resolve/DescriptorUtils"

    aput-object v4, v2, v3

    const/4 v3, 0x2

    const-string v4, "isEnumEntry"

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 298
    :cond_0
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/b/f;->d:Lkotlin/reflect/jvm/internal/impl/b/f;

    invoke-static {p0, v0}, Lkotlin/reflect/jvm/internal/impl/h/c;->a(Lkotlin/reflect/jvm/internal/impl/b/m;Lkotlin/reflect/jvm/internal/impl/b/f;)Z

    move-result v0

    return v0
.end method

.method public static m(Lkotlin/reflect/jvm/internal/impl/b/m;)Z
    .locals 1

    .prologue
    .line 302
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/b/f;->c:Lkotlin/reflect/jvm/internal/impl/b/f;

    invoke-static {p0, v0}, Lkotlin/reflect/jvm/internal/impl/h/c;->a(Lkotlin/reflect/jvm/internal/impl/b/m;Lkotlin/reflect/jvm/internal/impl/b/f;)Z

    move-result v0

    return v0
.end method

.method public static n(Lkotlin/reflect/jvm/internal/impl/b/m;)Z
    .locals 1

    .prologue
    .line 306
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/b/f;->e:Lkotlin/reflect/jvm/internal/impl/b/f;

    invoke-static {p0, v0}, Lkotlin/reflect/jvm/internal/impl/h/c;->a(Lkotlin/reflect/jvm/internal/impl/b/m;Lkotlin/reflect/jvm/internal/impl/b/f;)Z

    move-result v0

    return v0
.end method

.method public static o(Lkotlin/reflect/jvm/internal/impl/b/m;)Z
    .locals 1

    .prologue
    .line 310
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/b/f;->b:Lkotlin/reflect/jvm/internal/impl/b/f;

    invoke-static {p0, v0}, Lkotlin/reflect/jvm/internal/impl/h/c;->a(Lkotlin/reflect/jvm/internal/impl/b/m;Lkotlin/reflect/jvm/internal/impl/b/f;)Z

    move-result v0

    return v0
.end method

.method public static p(Lkotlin/reflect/jvm/internal/impl/b/m;)Z
    .locals 1

    .prologue
    .line 314
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/b/f;->a:Lkotlin/reflect/jvm/internal/impl/b/f;

    invoke-static {p0, v0}, Lkotlin/reflect/jvm/internal/impl/h/c;->a(Lkotlin/reflect/jvm/internal/impl/b/m;Lkotlin/reflect/jvm/internal/impl/b/f;)Z

    move-result v0

    return v0
.end method

.method public static q(Lkotlin/reflect/jvm/internal/impl/b/m;)Z
    .locals 1

    .prologue
    .line 318
    invoke-static {p0}, Lkotlin/reflect/jvm/internal/impl/h/c;->p(Lkotlin/reflect/jvm/internal/impl/b/m;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p0}, Lkotlin/reflect/jvm/internal/impl/h/c;->m(Lkotlin/reflect/jvm/internal/impl/b/m;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static r(Lkotlin/reflect/jvm/internal/impl/b/m;)Lkotlin/reflect/jvm/internal/impl/b/am;
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    if-nez p0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "descriptor"

    aput-object v3, v2, v4

    const-string v3, "kotlin/reflect/jvm/internal/impl/resolve/DescriptorUtils"

    aput-object v3, v2, v5

    const-string v3, "getContainingSourceFile"

    aput-object v3, v2, v6

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 563
    :cond_0
    instance-of v0, p0, Lkotlin/reflect/jvm/internal/impl/b/ai;

    if-eqz v0, :cond_3

    .line 564
    check-cast p0, Lkotlin/reflect/jvm/internal/impl/b/ai;

    invoke-interface {p0}, Lkotlin/reflect/jvm/internal/impl/b/ai;->q()Lkotlin/reflect/jvm/internal/impl/b/ag;

    move-result-object v0

    .line 567
    :goto_0
    instance-of v1, v0, Lkotlin/reflect/jvm/internal/impl/b/p;

    if-eqz v1, :cond_1

    .line 568
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/p;

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/b/p;->x()Lkotlin/reflect/jvm/internal/impl/b/al;

    move-result-object v0

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/b/al;->a()Lkotlin/reflect/jvm/internal/impl/b/am;

    move-result-object v0

    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "@NotNull method %s.%s must not return null"

    new-array v2, v6, [Ljava/lang/Object;

    const-string v3, "kotlin/reflect/jvm/internal/impl/resolve/DescriptorUtils"

    aput-object v3, v2, v4

    const-string v3, "getContainingSourceFile"

    aput-object v3, v2, v5

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 571
    :cond_1
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/b/am;->a:Lkotlin/reflect/jvm/internal/impl/b/am;

    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "@NotNull method %s.%s must not return null"

    new-array v2, v6, [Ljava/lang/Object;

    const-string v3, "kotlin/reflect/jvm/internal/impl/resolve/DescriptorUtils"

    aput-object v3, v2, v4

    const-string v3, "getContainingSourceFile"

    aput-object v3, v2, v5

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    return-object v0

    :cond_3
    move-object v0, p0

    goto :goto_0
.end method

.method private static s(Lkotlin/reflect/jvm/internal/impl/b/m;)Lkotlin/reflect/jvm/internal/impl/e/b;
    .locals 5

    .prologue
    if-nez p0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "descriptor"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "kotlin/reflect/jvm/internal/impl/resolve/DescriptorUtils"

    aput-object v4, v2, v3

    const/4 v3, 0x2

    const-string v4, "getFqNameSafeIfPossible"

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 103
    :cond_0
    instance-of v0, p0, Lkotlin/reflect/jvm/internal/impl/b/w;

    if-nez v0, :cond_1

    invoke-static {p0}, Lkotlin/reflect/jvm/internal/impl/k/l;->a(Lkotlin/reflect/jvm/internal/impl/b/m;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 104
    :cond_1
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/e/b;->a:Lkotlin/reflect/jvm/internal/impl/e/b;

    .line 114
    :goto_0
    return-object v0

    .line 107
    :cond_2
    instance-of v0, p0, Lkotlin/reflect/jvm/internal/impl/b/ac;

    if-eqz v0, :cond_3

    .line 108
    check-cast p0, Lkotlin/reflect/jvm/internal/impl/b/ac;

    invoke-interface {p0}, Lkotlin/reflect/jvm/internal/impl/b/ac;->a()Lkotlin/reflect/jvm/internal/impl/e/b;

    move-result-object v0

    goto :goto_0

    .line 110
    :cond_3
    instance-of v0, p0, Lkotlin/reflect/jvm/internal/impl/b/y;

    if-eqz v0, :cond_4

    .line 111
    check-cast p0, Lkotlin/reflect/jvm/internal/impl/b/y;

    invoke-interface {p0}, Lkotlin/reflect/jvm/internal/impl/b/y;->f()Lkotlin/reflect/jvm/internal/impl/e/b;

    move-result-object v0

    goto :goto_0

    .line 114
    :cond_4
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static t(Lkotlin/reflect/jvm/internal/impl/b/m;)Lkotlin/reflect/jvm/internal/impl/e/c;
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    if-nez p0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "descriptor"

    aput-object v3, v2, v4

    const-string v3, "kotlin/reflect/jvm/internal/impl/resolve/DescriptorUtils"

    aput-object v3, v2, v5

    const-string v3, "getFqNameUnsafe"

    aput-object v3, v2, v6

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 119
    :cond_0
    invoke-interface {p0}, Lkotlin/reflect/jvm/internal/impl/b/m;->y_()Lkotlin/reflect/jvm/internal/impl/b/m;

    move-result-object v0

    .line 120
    sget-boolean v1, Lkotlin/reflect/jvm/internal/impl/h/c;->f:Z

    if-nez v1, :cond_1

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Not package/module descriptor doesn\'t have containing declaration: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 121
    :cond_1
    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/h/c;->d(Lkotlin/reflect/jvm/internal/impl/b/m;)Lkotlin/reflect/jvm/internal/impl/e/c;

    move-result-object v0

    invoke-interface {p0}, Lkotlin/reflect/jvm/internal/impl/b/m;->D_()Lkotlin/reflect/jvm/internal/impl/e/f;

    move-result-object v1

    invoke-virtual {v0, v1}, Lkotlin/reflect/jvm/internal/impl/e/c;->a(Lkotlin/reflect/jvm/internal/impl/e/f;)Lkotlin/reflect/jvm/internal/impl/e/c;

    move-result-object v0

    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "@NotNull method %s.%s must not return null"

    new-array v2, v6, [Ljava/lang/Object;

    const-string v3, "kotlin/reflect/jvm/internal/impl/resolve/DescriptorUtils"

    aput-object v3, v2, v4

    const-string v3, "getFqNameUnsafe"

    aput-object v3, v2, v5

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    return-object v0
.end method
