.class public final Lkotlin/reflect/jvm/internal/impl/h/e/l;
.super Ljava/lang/Object;
.source "SubstitutingScope.kt"

# interfaces
.implements Lkotlin/reflect/jvm/internal/impl/h/e/h;


# static fields
.field static final synthetic a:[Lkotlin/reflect/l;


# instance fields
.field private final b:Lkotlin/reflect/jvm/internal/impl/k/am;

.field private d:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/b/m;",
            "Lkotlin/reflect/jvm/internal/impl/b/m;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Lkotlin/c;

.field private final f:Lkotlin/reflect/jvm/internal/impl/h/e/h;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const/4 v0, 0x1

    new-array v1, v0, [Lkotlin/reflect/l;

    const/4 v2, 0x0

    new-instance v0, Lkotlin/d/b/w;

    const-class v3, Lkotlin/reflect/jvm/internal/impl/h/e/l;

    invoke-static {v3}, Lkotlin/d/b/y;->a(Ljava/lang/Class;)Lkotlin/reflect/c;

    move-result-object v3

    const-string v4, "_allDescriptors"

    const-string v5, "get_allDescriptors()Ljava/util/Collection;"

    invoke-direct {v0, v3, v4, v5}, Lkotlin/d/b/w;-><init>(Lkotlin/reflect/e;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v0}, Lkotlin/d/b/y;->a(Lkotlin/d/b/v;)Lkotlin/reflect/n;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/l;

    aput-object v0, v1, v2

    sput-object v1, Lkotlin/reflect/jvm/internal/impl/h/e/l;->a:[Lkotlin/reflect/l;

    return-void
.end method

.method public constructor <init>(Lkotlin/reflect/jvm/internal/impl/h/e/h;Lkotlin/reflect/jvm/internal/impl/k/am;)V
    .locals 4

    .prologue
    const-string v0, "workerScope"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "givenSubstitutor"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lkotlin/reflect/jvm/internal/impl/h/e/l;->f:Lkotlin/reflect/jvm/internal/impl/h/e/h;

    .line 31
    invoke-virtual {p2}, Lkotlin/reflect/jvm/internal/impl/k/am;->b()Lkotlin/reflect/jvm/internal/impl/k/ak;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-static {v0, v1, v2, v3}, Lkotlin/reflect/jvm/internal/impl/h/a/a/c;->a(Lkotlin/reflect/jvm/internal/impl/k/ak;ZILjava/lang/Object;)Lkotlin/reflect/jvm/internal/impl/k/ak;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/k/ak;->f()Lkotlin/reflect/jvm/internal/impl/k/am;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/h/e/l;->b:Lkotlin/reflect/jvm/internal/impl/k/am;

    .line 35
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/h/e/l$a;

    invoke-direct {v0, p0}, Lkotlin/reflect/jvm/internal/impl/h/e/l$a;-><init>(Lkotlin/reflect/jvm/internal/impl/h/e/l;)V

    check-cast v0, Lkotlin/d/a/a;

    invoke-static {v0}, Lkotlin/d;->a(Lkotlin/d/a/a;)Lkotlin/c;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/h/e/l;->e:Lkotlin/c;

    return-void
.end method

.method private final a(Ljava/util/Collection;)Ljava/util/Collection;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<D::",
            "Lkotlin/reflect/jvm/internal/impl/b/m;",
            ">(",
            "Ljava/util/Collection",
            "<+TD;>;)",
            "Ljava/util/Collection",
            "<TD;>;"
        }
    .end annotation

    .prologue
    .line 56
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/h/e/l;->b:Lkotlin/reflect/jvm/internal/impl/k/am;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/k/am;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 65
    :cond_0
    :goto_0
    return-object p1

    .line 57
    :cond_1
    invoke-interface {p1}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 59
    invoke-interface {p1}, Ljava/util/Collection;->size()I

    move-result v0

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/utils/a;->b(I)Ljava/util/HashSet;

    move-result-object v1

    .line 60
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/m;

    .line 61
    invoke-direct {p0, v0}, Lkotlin/reflect/jvm/internal/impl/h/e/l;->a(Lkotlin/reflect/jvm/internal/impl/b/m;)Lkotlin/reflect/jvm/internal/impl/b/m;

    move-result-object v0

    .line 62
    invoke-virtual {v1, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_2
    move-object v0, v1

    .line 65
    check-cast v0, Ljava/util/Collection;

    move-object p1, v0

    goto :goto_0
.end method

.method public static final synthetic a(Lkotlin/reflect/jvm/internal/impl/h/e/l;Ljava/util/Collection;)Ljava/util/Collection;
    .locals 1

    .prologue
    .line 29
    invoke-direct {p0, p1}, Lkotlin/reflect/jvm/internal/impl/h/e/l;->a(Ljava/util/Collection;)Ljava/util/Collection;

    move-result-object v0

    return-object v0
.end method

.method private final a(Lkotlin/reflect/jvm/internal/impl/b/m;)Lkotlin/reflect/jvm/internal/impl/b/m;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<D::",
            "Lkotlin/reflect/jvm/internal/impl/b/m;",
            ">(TD;)TD;"
        }
    .end annotation

    .prologue
    .line 38
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/h/e/l;->b:Lkotlin/reflect/jvm/internal/impl/k/am;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/k/am;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 52
    :goto_0
    return-object p1

    .line 40
    :cond_0
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/h/e/l;->d:Ljava/util/Map;

    if-nez v0, :cond_1

    .line 41
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    check-cast v0, Ljava/util/Map;

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/h/e/l;->d:Ljava/util/Map;

    .line 44
    :cond_1
    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/h/e/l;->d:Ljava/util/Map;

    if-nez v1, :cond_2

    invoke-static {}, Lkotlin/d/b/l;->a()V

    .line 98
    :cond_2
    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 99
    if-nez v0, :cond_3

    .line 100
    nop

    .line 45
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/h/e/l;->b:Lkotlin/reflect/jvm/internal/impl/k/am;

    invoke-interface {p1, v0}, Lkotlin/reflect/jvm/internal/impl/b/m;->b(Lkotlin/reflect/jvm/internal/impl/k/am;)Lkotlin/reflect/jvm/internal/impl/b/m;

    move-result-object v0

    .line 101
    if-eqz v0, :cond_4

    .line 47
    const-string v2, "descriptor.substitute(su\u2026tion fails\"\n            }"

    invoke-static {v0, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/m;

    .line 102
    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 44
    :cond_3
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/m;

    .line 52
    if-nez v0, :cond_5

    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type D"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 101
    :cond_4
    new-instance v0, Ljava/lang/AssertionError;

    .line 46
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "We expect that no conflict should happen while substitution is guaranteed to generate invariant projection, "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 47
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "but "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " substitution fails"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    .line 52
    :cond_5
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/m;

    move-object p1, v0

    goto :goto_0
.end method

.method public static final synthetic a(Lkotlin/reflect/jvm/internal/impl/h/e/l;)Lkotlin/reflect/jvm/internal/impl/h/e/h;
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/h/e/l;->f:Lkotlin/reflect/jvm/internal/impl/h/e/h;

    return-object v0
.end method

.method private final c()Ljava/util/Collection;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/b/m;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/h/e/l;->e:Lkotlin/c;

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/h/e/l;->a:[Lkotlin/reflect/l;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-interface {v0}, Lkotlin/c;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    return-object v0
.end method


# virtual methods
.method public A_()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/e/f;",
            ">;"
        }
    .end annotation

    .prologue
    .line 78
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/h/e/l;->f:Lkotlin/reflect/jvm/internal/impl/h/e/h;

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/h/e/h;->A_()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public B_()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/e/f;",
            ">;"
        }
    .end annotation

    .prologue
    .line 79
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/h/e/l;->f:Lkotlin/reflect/jvm/internal/impl/h/e/h;

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/h/e/h;->B_()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public a(Lkotlin/reflect/jvm/internal/impl/e/f;Lkotlin/reflect/jvm/internal/impl/c/a/b;)Ljava/util/Collection;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/reflect/jvm/internal/impl/e/f;",
            "Lkotlin/reflect/jvm/internal/impl/c/a/b;",
            ")",
            "Ljava/util/Collection",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/b/ag;",
            ">;"
        }
    .end annotation

    .prologue
    const-string v0, "name"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "location"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 68
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/h/e/l;->f:Lkotlin/reflect/jvm/internal/impl/h/e/h;

    invoke-interface {v0, p1, p2}, Lkotlin/reflect/jvm/internal/impl/h/e/h;->a(Lkotlin/reflect/jvm/internal/impl/e/f;Lkotlin/reflect/jvm/internal/impl/c/a/b;)Ljava/util/Collection;

    move-result-object v0

    invoke-direct {p0, v0}, Lkotlin/reflect/jvm/internal/impl/h/e/l;->a(Ljava/util/Collection;)Ljava/util/Collection;

    move-result-object v0

    return-object v0
.end method

.method public a(Lkotlin/reflect/jvm/internal/impl/h/e/d;Lkotlin/d/a/b;)Ljava/util/Collection;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/reflect/jvm/internal/impl/h/e/d;",
            "Lkotlin/d/a/b",
            "<-",
            "Lkotlin/reflect/jvm/internal/impl/e/f;",
            "Ljava/lang/Boolean;",
            ">;)",
            "Ljava/util/Collection",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/b/m;",
            ">;"
        }
    .end annotation

    .prologue
    const-string v0, "kindFilter"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "nameFilter"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 76
    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/h/e/l;->c()Ljava/util/Collection;

    move-result-object v0

    return-object v0
.end method

.method public b(Lkotlin/reflect/jvm/internal/impl/e/f;Lkotlin/reflect/jvm/internal/impl/c/a/b;)Ljava/util/Collection;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/reflect/jvm/internal/impl/e/f;",
            "Lkotlin/reflect/jvm/internal/impl/c/a/b;",
            ")",
            "Ljava/util/Collection",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/b/ak;",
            ">;"
        }
    .end annotation

    .prologue
    const-string v0, "name"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "location"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 73
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/h/e/l;->f:Lkotlin/reflect/jvm/internal/impl/h/e/h;

    invoke-interface {v0, p1, p2}, Lkotlin/reflect/jvm/internal/impl/h/e/h;->b(Lkotlin/reflect/jvm/internal/impl/e/f;Lkotlin/reflect/jvm/internal/impl/c/a/b;)Ljava/util/Collection;

    move-result-object v0

    invoke-direct {p0, v0}, Lkotlin/reflect/jvm/internal/impl/h/e/l;->a(Ljava/util/Collection;)Ljava/util/Collection;

    move-result-object v0

    return-object v0
.end method

.method public c(Lkotlin/reflect/jvm/internal/impl/e/f;Lkotlin/reflect/jvm/internal/impl/c/a/b;)Lkotlin/reflect/jvm/internal/impl/b/h;
    .locals 1

    .prologue
    const-string v0, "name"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "location"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 71
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/h/e/l;->f:Lkotlin/reflect/jvm/internal/impl/h/e/h;

    invoke-interface {v0, p1, p2}, Lkotlin/reflect/jvm/internal/impl/h/e/h;->c(Lkotlin/reflect/jvm/internal/impl/e/f;Lkotlin/reflect/jvm/internal/impl/c/a/b;)Lkotlin/reflect/jvm/internal/impl/b/h;

    move-result-object v0

    if-eqz v0, :cond_0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/h;

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/m;

    invoke-direct {p0, v0}, Lkotlin/reflect/jvm/internal/impl/h/e/l;->a(Lkotlin/reflect/jvm/internal/impl/b/m;)Lkotlin/reflect/jvm/internal/impl/b/m;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/h;

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/h;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
