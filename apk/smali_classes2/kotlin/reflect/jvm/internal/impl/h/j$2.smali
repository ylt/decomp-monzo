.class Lkotlin/reflect/jvm/internal/impl/h/j$2;
.super Ljava/lang/Object;
.source "OverridingUtil.java"

# interfaces
.implements Lkotlin/reflect/jvm/internal/impl/k/a/c$a;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lkotlin/reflect/jvm/internal/impl/h/j;->a(Ljava/util/List;Ljava/util/List;)Lkotlin/reflect/jvm/internal/impl/k/a/c;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Ljava/util/Map;

.field final synthetic b:Lkotlin/reflect/jvm/internal/impl/h/j;


# direct methods
.method constructor <init>(Lkotlin/reflect/jvm/internal/impl/h/j;Ljava/util/Map;)V
    .locals 0

    .prologue
    .line 324
    iput-object p1, p0, Lkotlin/reflect/jvm/internal/impl/h/j$2;->b:Lkotlin/reflect/jvm/internal/impl/h/j;

    iput-object p2, p0, Lkotlin/reflect/jvm/internal/impl/h/j$2;->a:Ljava/util/Map;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lkotlin/reflect/jvm/internal/impl/k/ad;Lkotlin/reflect/jvm/internal/impl/k/ad;)Z
    .locals 7

    .prologue
    const/4 v4, 0x3

    const/4 v6, 0x2

    const/4 v3, 0x0

    const/4 v2, 0x1

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    new-array v4, v4, [Ljava/lang/Object;

    const-string v5, "a"

    aput-object v5, v4, v3

    const-string v3, "kotlin/reflect/jvm/internal/impl/resolve/OverridingUtil$2"

    aput-object v3, v4, v2

    const-string v2, "equals"

    aput-object v2, v4, v6

    invoke-static {v1, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    if-nez p2, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    new-array v4, v4, [Ljava/lang/Object;

    const-string v5, "b"

    aput-object v5, v4, v3

    const-string v3, "kotlin/reflect/jvm/internal/impl/resolve/OverridingUtil$2"

    aput-object v3, v4, v2

    const-string v2, "equals"

    aput-object v2, v4, v6

    invoke-static {v1, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 327
    :cond_1
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/h/j$2;->b:Lkotlin/reflect/jvm/internal/impl/h/j;

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/h/j;->a(Lkotlin/reflect/jvm/internal/impl/h/j;)Lkotlin/reflect/jvm/internal/impl/k/a/c$a;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Lkotlin/reflect/jvm/internal/impl/k/a/c$a;->a(Lkotlin/reflect/jvm/internal/impl/k/ad;Lkotlin/reflect/jvm/internal/impl/k/ad;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 330
    :goto_0
    return v2

    .line 328
    :cond_2
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/h/j$2;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/ad;

    .line 329
    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/h/j$2;->a:Ljava/util/Map;

    invoke-interface {v1, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lkotlin/reflect/jvm/internal/impl/k/ad;

    .line 330
    if-eqz v0, :cond_3

    invoke-virtual {v0, p2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    :cond_3
    if-eqz v1, :cond_5

    invoke-virtual {v1, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    :cond_4
    move v0, v2

    :goto_1
    move v2, v0

    goto :goto_0

    :cond_5
    move v0, v3

    goto :goto_1
.end method
