.class public final Lkotlin/reflect/jvm/internal/impl/h/a/a/c;
.super Ljava/lang/Object;
.source "CapturedTypeConstructor.kt"


# direct methods
.method public static final synthetic a(Lkotlin/reflect/jvm/internal/impl/k/ah;Lkotlin/reflect/jvm/internal/impl/b/aq;)Lkotlin/reflect/jvm/internal/impl/k/ah;
    .locals 1

    .prologue
    .line 1
    invoke-static {p0, p1}, Lkotlin/reflect/jvm/internal/impl/h/a/a/c;->b(Lkotlin/reflect/jvm/internal/impl/k/ah;Lkotlin/reflect/jvm/internal/impl/b/aq;)Lkotlin/reflect/jvm/internal/impl/k/ah;

    move-result-object v0

    return-object v0
.end method

.method public static final a(Lkotlin/reflect/jvm/internal/impl/k/ak;Z)Lkotlin/reflect/jvm/internal/impl/k/ak;
    .locals 6

    .prologue
    const-string v0, "$receiver"

    invoke-static {p0, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 104
    instance-of v0, p0, Lkotlin/reflect/jvm/internal/impl/k/p;

    if-eqz v0, :cond_2

    .line 105
    new-instance v3, Lkotlin/reflect/jvm/internal/impl/k/p;

    move-object v0, p0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/p;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/k/p;->d()[Lkotlin/reflect/jvm/internal/impl/b/aq;

    move-result-object v4

    move-object v0, p0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/p;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/k/p;->e()[Lkotlin/reflect/jvm/internal/impl/k/ah;

    move-result-object v0

    check-cast v0, [Ljava/lang/Object;

    check-cast p0, Lkotlin/reflect/jvm/internal/impl/k/p;

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/k/p;->d()[Lkotlin/reflect/jvm/internal/impl/b/aq;

    move-result-object v1

    check-cast v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lkotlin/a/g;->a([Ljava/lang/Object;[Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 134
    new-instance v1, Ljava/util/ArrayList;

    const/16 v2, 0xa

    invoke-static {v0, v2}, Lkotlin/a/m;->a(Ljava/lang/Iterable;I)I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v1, Ljava/util/Collection;

    .line 135
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .line 136
    check-cast v0, Lkotlin/h;

    .line 108
    invoke-virtual {v0}, Lkotlin/h;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lkotlin/reflect/jvm/internal/impl/k/ah;

    invoke-virtual {v0}, Lkotlin/h;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/aq;

    invoke-static {v2, v0}, Lkotlin/reflect/jvm/internal/impl/h/a/a/c;->b(Lkotlin/reflect/jvm/internal/impl/k/ah;Lkotlin/reflect/jvm/internal/impl/b/aq;)Lkotlin/reflect/jvm/internal/impl/k/ah;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 137
    :cond_0
    check-cast v1, Ljava/util/List;

    .line 105
    check-cast v1, Ljava/util/Collection;

    .line 138
    check-cast v1, Ljava/util/Collection;

    .line 139
    invoke-interface {v1}, Ljava/util/Collection;->size()I

    move-result v0

    new-array v0, v0, [Lkotlin/reflect/jvm/internal/impl/k/ah;

    invoke-interface {v1, v0}, Ljava/util/Collection;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_1

    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type kotlin.Array<T>"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    check-cast v0, [Ljava/lang/Object;

    .line 105
    check-cast v0, [Lkotlin/reflect/jvm/internal/impl/k/ah;

    invoke-direct {v3, v4, v0, p1}, Lkotlin/reflect/jvm/internal/impl/k/p;-><init>([Lkotlin/reflect/jvm/internal/impl/b/aq;[Lkotlin/reflect/jvm/internal/impl/k/ah;Z)V

    move-object v0, v3

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/ak;

    .line 115
    :goto_1
    return-object v0

    .line 112
    :cond_2
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/h/a/a/c$b;

    invoke-direct {v0, p0, p1, p0}, Lkotlin/reflect/jvm/internal/impl/h/a/a/c$b;-><init>(Lkotlin/reflect/jvm/internal/impl/k/ak;ZLkotlin/reflect/jvm/internal/impl/k/ak;)V

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/ak;

    goto :goto_1
.end method

.method public static bridge synthetic a(Lkotlin/reflect/jvm/internal/impl/k/ak;ZILjava/lang/Object;)Lkotlin/reflect/jvm/internal/impl/k/ak;
    .locals 1

    .prologue
    and-int/lit8 v0, p2, 0x1

    if-eqz v0, :cond_0

    .line 103
    const/4 p1, 0x1

    :cond_0
    invoke-static {p0, p1}, Lkotlin/reflect/jvm/internal/impl/h/a/a/c;->a(Lkotlin/reflect/jvm/internal/impl/k/ak;Z)Lkotlin/reflect/jvm/internal/impl/k/ak;

    move-result-object v0

    return-object v0
.end method

.method public static final a(Lkotlin/reflect/jvm/internal/impl/k/ah;)Lkotlin/reflect/jvm/internal/impl/k/r;
    .locals 7

    .prologue
    const/4 v2, 0x0

    const-string v0, "typeProjection"

    invoke-static {p0, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 99
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/h/a/a/a;

    const/4 v3, 0x0

    const/16 v5, 0xe

    move-object v1, p0

    move-object v4, v2

    move-object v6, v2

    invoke-direct/range {v0 .. v6}, Lkotlin/reflect/jvm/internal/impl/h/a/a/a;-><init>(Lkotlin/reflect/jvm/internal/impl/k/ah;Lkotlin/reflect/jvm/internal/impl/h/a/a/b;ZLkotlin/reflect/jvm/internal/impl/b/a/h;ILkotlin/d/b/i;)V

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/r;

    return-object v0
.end method

.method public static final a(Lkotlin/reflect/jvm/internal/impl/k/r;)Z
    .locals 1

    .prologue
    const-string v0, "$receiver"

    invoke-static {p0, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 101
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/k/r;->i()Lkotlin/reflect/jvm/internal/impl/k/ad;

    move-result-object v0

    instance-of v0, v0, Lkotlin/reflect/jvm/internal/impl/h/a/a/b;

    return v0
.end method

.method private static final b(Lkotlin/reflect/jvm/internal/impl/k/ah;Lkotlin/reflect/jvm/internal/impl/b/aq;)Lkotlin/reflect/jvm/internal/impl/k/ah;
    .locals 4

    .prologue
    .line 118
    if-eqz p1, :cond_0

    invoke-interface {p0}, Lkotlin/reflect/jvm/internal/impl/k/ah;->b()Lkotlin/reflect/jvm/internal/impl/k/ap;

    move-result-object v0

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/k/ap;->a:Lkotlin/reflect/jvm/internal/impl/k/ap;

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    move-object v0, p0

    .line 131
    :goto_0
    return-object v0

    .line 121
    :cond_1
    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/b/aq;->k()Lkotlin/reflect/jvm/internal/impl/k/ap;

    move-result-object v0

    invoke-interface {p0}, Lkotlin/reflect/jvm/internal/impl/k/ah;->b()Lkotlin/reflect/jvm/internal/impl/k/ap;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 123
    invoke-interface {p0}, Lkotlin/reflect/jvm/internal/impl/k/ah;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 124
    new-instance v2, Lkotlin/reflect/jvm/internal/impl/k/aj;

    new-instance v1, Lkotlin/reflect/jvm/internal/impl/k/t;

    sget-object v3, Lkotlin/reflect/jvm/internal/impl/j/b;->a:Lkotlin/reflect/jvm/internal/impl/j/i;

    const-string v0, "LockBasedStorageManager.NO_LOCKS"

    invoke-static {v3, v0}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lkotlin/reflect/jvm/internal/impl/h/a/a/c$a;

    invoke-direct {v0, p0}, Lkotlin/reflect/jvm/internal/impl/h/a/a/c$a;-><init>(Lkotlin/reflect/jvm/internal/impl/k/ah;)V

    check-cast v0, Lkotlin/d/a/a;

    invoke-direct {v1, v3, v0}, Lkotlin/reflect/jvm/internal/impl/k/t;-><init>(Lkotlin/reflect/jvm/internal/impl/j/i;Lkotlin/d/a/a;)V

    move-object v0, v1

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/r;

    invoke-direct {v2, v0}, Lkotlin/reflect/jvm/internal/impl/k/aj;-><init>(Lkotlin/reflect/jvm/internal/impl/k/r;)V

    move-object v0, v2

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/ah;

    goto :goto_0

    .line 128
    :cond_2
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/k/aj;

    invoke-interface {p0}, Lkotlin/reflect/jvm/internal/impl/k/ah;->c()Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v1

    invoke-direct {v0, v1}, Lkotlin/reflect/jvm/internal/impl/k/aj;-><init>(Lkotlin/reflect/jvm/internal/impl/k/r;)V

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/ah;

    goto :goto_0

    .line 131
    :cond_3
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/k/aj;

    invoke-static {p0}, Lkotlin/reflect/jvm/internal/impl/h/a/a/c;->a(Lkotlin/reflect/jvm/internal/impl/k/ah;)Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v1

    invoke-direct {v0, v1}, Lkotlin/reflect/jvm/internal/impl/k/aj;-><init>(Lkotlin/reflect/jvm/internal/impl/k/r;)V

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/ah;

    goto :goto_0
.end method
