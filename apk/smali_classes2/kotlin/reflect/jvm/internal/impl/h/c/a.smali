.class public final Lkotlin/reflect/jvm/internal/impl/h/c/a;
.super Ljava/lang/Object;
.source "DescriptorUtils.kt"


# direct methods
.method public static final a(Lkotlin/reflect/jvm/internal/impl/b/b;)Lkotlin/reflect/jvm/internal/impl/b/b;
    .locals 2

    .prologue
    const-string v0, "$receiver"

    invoke-static {p0, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 229
    instance-of v0, p0, Lkotlin/reflect/jvm/internal/impl/b/af;

    if-eqz v0, :cond_0

    check-cast p0, Lkotlin/reflect/jvm/internal/impl/b/af;

    invoke-interface {p0}, Lkotlin/reflect/jvm/internal/impl/b/af;->q()Lkotlin/reflect/jvm/internal/impl/b/ag;

    move-result-object v0

    const-string v1, "correspondingProperty"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/b;

    :goto_0
    return-object v0

    :cond_0
    move-object v0, p0

    goto :goto_0
.end method

.method public static final a(Lkotlin/reflect/jvm/internal/impl/b/b;ZLkotlin/d/a/b;)Lkotlin/reflect/jvm/internal/impl/b/b;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/reflect/jvm/internal/impl/b/b;",
            "Z",
            "Lkotlin/d/a/b",
            "<-",
            "Lkotlin/reflect/jvm/internal/impl/b/b;",
            "Ljava/lang/Boolean;",
            ">;)",
            "Lkotlin/reflect/jvm/internal/impl/b/b;"
        }
    .end annotation

    .prologue
    const-string v0, "$receiver"

    invoke-static {p0, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "predicate"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 237
    new-instance v3, Lkotlin/d/b/x$c;

    invoke-direct {v3}, Lkotlin/d/b/x$c;-><init>()V

    const/4 v0, 0x0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/b;

    iput-object v0, v3, Lkotlin/d/b/x$c;->a:Ljava/lang/Object;

    .line 238
    invoke-static {p0}, Lkotlin/a/m;->a(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    new-instance v1, Lkotlin/reflect/jvm/internal/impl/h/c/a$b;

    invoke-direct {v1, p1}, Lkotlin/reflect/jvm/internal/impl/h/c/a$b;-><init>(Z)V

    check-cast v1, Lkotlin/reflect/jvm/internal/impl/utils/b$b;

    new-instance v2, Lkotlin/reflect/jvm/internal/impl/h/c/a$c;

    invoke-direct {v2, v3, p2}, Lkotlin/reflect/jvm/internal/impl/h/c/a$c;-><init>(Lkotlin/d/b/x$c;Lkotlin/d/a/b;)V

    check-cast v2, Lkotlin/reflect/jvm/internal/impl/utils/b$c;

    invoke-static {v0, v1, v2}, Lkotlin/reflect/jvm/internal/impl/utils/b;->a(Ljava/util/Collection;Lkotlin/reflect/jvm/internal/impl/utils/b$b;Lkotlin/reflect/jvm/internal/impl/utils/b$c;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/b;

    return-object v0
.end method

.method public static bridge synthetic a(Lkotlin/reflect/jvm/internal/impl/b/b;ZLkotlin/d/a/b;ILjava/lang/Object;)Lkotlin/reflect/jvm/internal/impl/b/b;
    .locals 1

    .prologue
    and-int/lit8 v0, p3, 0x1

    if-eqz v0, :cond_0

    .line 234
    const/4 p1, 0x0

    :cond_0
    invoke-static {p0, p1, p2}, Lkotlin/reflect/jvm/internal/impl/h/c/a;->a(Lkotlin/reflect/jvm/internal/impl/b/b;ZLkotlin/d/a/b;)Lkotlin/reflect/jvm/internal/impl/b/b;

    move-result-object v0

    return-object v0
.end method

.method public static final a(Lkotlin/reflect/jvm/internal/impl/b/i;)Lkotlin/reflect/jvm/internal/impl/b/e;
    .locals 3

    .prologue
    const-string v0, "$receiver"

    invoke-static {p0, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 74
    instance-of v0, p0, Lkotlin/reflect/jvm/internal/impl/b/e;

    if-eqz v0, :cond_0

    check-cast p0, Lkotlin/reflect/jvm/internal/impl/b/e;

    .line 77
    :goto_0
    return-object p0

    .line 75
    :cond_0
    instance-of v0, p0, Lkotlin/reflect/jvm/internal/impl/b/ap;

    if-eqz v0, :cond_1

    check-cast p0, Lkotlin/reflect/jvm/internal/impl/b/ap;

    invoke-interface {p0}, Lkotlin/reflect/jvm/internal/impl/b/ap;->c()Lkotlin/reflect/jvm/internal/impl/b/e;

    move-result-object p0

    goto :goto_0

    .line 76
    :cond_1
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unexpected descriptor kind: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method

.method public static final a(Lkotlin/reflect/jvm/internal/impl/b/w;Lkotlin/reflect/jvm/internal/impl/e/b;Lkotlin/reflect/jvm/internal/impl/c/a/b;)Lkotlin/reflect/jvm/internal/impl/b/e;
    .locals 3

    .prologue
    const-string v0, "$receiver"

    invoke-static {p0, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "topLevelClassFqName"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "location"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 68
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/e/b;->c()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sget-boolean v1, Lkotlin/p;->a:Z

    if-eqz v1, :cond_1

    if-nez v0, :cond_1

    const-string v1, "Assertion failed"

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 69
    :cond_1
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/e/b;->d()Lkotlin/reflect/jvm/internal/impl/e/b;

    move-result-object v0

    const-string v1, "topLevelClassFqName.parent()"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {p0, v0}, Lkotlin/reflect/jvm/internal/impl/b/w;->a(Lkotlin/reflect/jvm/internal/impl/e/b;)Lkotlin/reflect/jvm/internal/impl/b/ac;

    move-result-object v0

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/b/ac;->c()Lkotlin/reflect/jvm/internal/impl/h/e/h;

    move-result-object v0

    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/e/b;->e()Lkotlin/reflect/jvm/internal/impl/e/f;

    move-result-object v1

    const-string v2, "topLevelClassFqName.shortName()"

    invoke-static {v1, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v0, v1, p2}, Lkotlin/reflect/jvm/internal/impl/h/e/h;->c(Lkotlin/reflect/jvm/internal/impl/e/f;Lkotlin/reflect/jvm/internal/impl/c/a/b;)Lkotlin/reflect/jvm/internal/impl/b/h;

    move-result-object v0

    instance-of v1, v0, Lkotlin/reflect/jvm/internal/impl/b/e;

    if-nez v1, :cond_2

    const/4 v0, 0x0

    :cond_2
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/e;

    return-object v0
.end method

.method public static final a(Lkotlin/reflect/jvm/internal/impl/b/a;)Lkotlin/reflect/jvm/internal/impl/e/b;
    .locals 2

    .prologue
    const-string v0, "$receiver"

    invoke-static {p0, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 231
    check-cast p0, Lkotlin/reflect/jvm/internal/impl/b/m;

    invoke-static {p0}, Lkotlin/reflect/jvm/internal/impl/h/c/a;->a(Lkotlin/reflect/jvm/internal/impl/b/m;)Lkotlin/reflect/jvm/internal/impl/e/c;

    move-result-object v1

    sget-object v0, Lkotlin/reflect/jvm/internal/impl/h/c/a$d;->a:Lkotlin/reflect/jvm/internal/impl/h/c/a$d;

    check-cast v0, Lkotlin/d/a/b;

    invoke-static {v1, v0}, Lkotlin/reflect/jvm/internal/impl/utils/a/a;->a(Ljava/lang/Object;Lkotlin/d/a/b;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/e/c;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/e/c;->c()Lkotlin/reflect/jvm/internal/impl/e/b;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static final a(Lkotlin/reflect/jvm/internal/impl/b/m;)Lkotlin/reflect/jvm/internal/impl/e/c;
    .locals 2

    .prologue
    const-string v0, "$receiver"

    invoke-static {p0, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 56
    invoke-static {p0}, Lkotlin/reflect/jvm/internal/impl/h/c;->d(Lkotlin/reflect/jvm/internal/impl/b/m;)Lkotlin/reflect/jvm/internal/impl/e/c;

    move-result-object v0

    const-string v1, "DescriptorUtils.getFqName(this)"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public static final a(Lkotlin/reflect/jvm/internal/impl/b/e;)Lkotlin/reflect/jvm/internal/impl/k/r;
    .locals 1

    .prologue
    const-string v0, "$receiver"

    invoke-static {p0, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 117
    check-cast p0, Lkotlin/reflect/jvm/internal/impl/b/i;

    invoke-static {p0}, Lkotlin/reflect/jvm/internal/impl/h/c/a;->c(Lkotlin/reflect/jvm/internal/impl/b/i;)Lkotlin/reflect/jvm/internal/impl/b/e;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/b/e;->E_()Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v0

    :goto_0
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/r;

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static final a(Lkotlin/reflect/jvm/internal/impl/b/at;)Z
    .locals 3

    .prologue
    const-string v0, "$receiver"

    invoke-static {p0, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 191
    invoke-static {p0}, Lkotlin/a/m;->a(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/h/c/a$e;->a:Lkotlin/reflect/jvm/internal/impl/h/c/a$e;

    check-cast v1, Lkotlin/reflect/jvm/internal/impl/utils/b$b;

    sget-object v2, Lkotlin/reflect/jvm/internal/impl/h/c/a$f;->a:Lkotlin/reflect/jvm/internal/impl/h/c/a$f;

    check-cast v2, Lkotlin/d/a/b;

    invoke-static {v0, v1, v2}, Lkotlin/reflect/jvm/internal/impl/utils/b;->a(Ljava/util/Collection;Lkotlin/reflect/jvm/internal/impl/utils/b$b;Lkotlin/d/a/b;)Ljava/lang/Boolean;

    move-result-object v0

    const-string v1, "DFS.ifAny(\n            l\u2026eclaresDefaultValue\n    )"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method

.method public static final b(Lkotlin/reflect/jvm/internal/impl/b/e;)Lkotlin/reflect/jvm/internal/impl/b/e;
    .locals 3

    .prologue
    const-string v0, "$receiver"

    invoke-static {p0, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 134
    invoke-interface {p0}, Lkotlin/reflect/jvm/internal/impl/b/e;->E_()Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/k/v;->i()Lkotlin/reflect/jvm/internal/impl/k/ad;

    move-result-object v0

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/k/ad;->G_()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/r;

    .line 135
    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/a/l;->h(Lkotlin/reflect/jvm/internal/impl/k/r;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 136
    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/k/r;->i()Lkotlin/reflect/jvm/internal/impl/k/ad;

    move-result-object v0

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/k/ad;->d()Lkotlin/reflect/jvm/internal/impl/b/h;

    move-result-object v1

    move-object v0, v1

    .line 137
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/m;

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/h/c;->q(Lkotlin/reflect/jvm/internal/impl/b/m;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 138
    if-nez v1, :cond_1

    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type org.jetbrains.kotlin.descriptors.ClassDescriptor"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    check-cast v1, Lkotlin/reflect/jvm/internal/impl/b/e;

    .line 142
    :goto_0
    return-object v1

    :cond_2
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static final b(Lkotlin/reflect/jvm/internal/impl/b/i;)Lkotlin/reflect/jvm/internal/impl/e/a;
    .locals 3

    .prologue
    const/4 v1, 0x0

    const-string v0, "$receiver"

    invoke-static {p0, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 80
    invoke-interface {p0}, Lkotlin/reflect/jvm/internal/impl/b/i;->y_()Lkotlin/reflect/jvm/internal/impl/b/m;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/m;

    .line 82
    instance-of v2, v0, Lkotlin/reflect/jvm/internal/impl/b/y;

    if-eqz v2, :cond_0

    new-instance v1, Lkotlin/reflect/jvm/internal/impl/e/a;

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/y;

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/b/y;->f()Lkotlin/reflect/jvm/internal/impl/e/b;

    move-result-object v0

    invoke-interface {p0}, Lkotlin/reflect/jvm/internal/impl/b/i;->D_()Lkotlin/reflect/jvm/internal/impl/e/f;

    move-result-object v2

    invoke-direct {v1, v0, v2}, Lkotlin/reflect/jvm/internal/impl/e/a;-><init>(Lkotlin/reflect/jvm/internal/impl/e/b;Lkotlin/reflect/jvm/internal/impl/e/f;)V

    move-object v0, v1

    .line 80
    :goto_0
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/e/a;

    .line 86
    return-object v0

    .line 83
    :cond_0
    instance-of v2, v0, Lkotlin/reflect/jvm/internal/impl/b/i;

    if-eqz v2, :cond_2

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/i;

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/h/c/a;->b(Lkotlin/reflect/jvm/internal/impl/b/i;)Lkotlin/reflect/jvm/internal/impl/e/a;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-interface {p0}, Lkotlin/reflect/jvm/internal/impl/b/i;->D_()Lkotlin/reflect/jvm/internal/impl/e/f;

    move-result-object v1

    invoke-virtual {v0, v1}, Lkotlin/reflect/jvm/internal/impl/e/a;->a(Lkotlin/reflect/jvm/internal/impl/e/f;)Lkotlin/reflect/jvm/internal/impl/e/a;

    move-result-object v0

    goto :goto_0

    :cond_1
    move-object v0, v1

    goto :goto_0

    :cond_2
    move-object v0, v1

    .line 84
    goto :goto_0
.end method

.method public static final b(Lkotlin/reflect/jvm/internal/impl/b/m;)Lkotlin/reflect/jvm/internal/impl/e/b;
    .locals 2

    .prologue
    const-string v0, "$receiver"

    invoke-static {p0, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 59
    invoke-static {p0}, Lkotlin/reflect/jvm/internal/impl/h/c;->e(Lkotlin/reflect/jvm/internal/impl/b/m;)Lkotlin/reflect/jvm/internal/impl/e/b;

    move-result-object v0

    const-string v1, "DescriptorUtils.getFqNameSafe(this)"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public static final c(Lkotlin/reflect/jvm/internal/impl/b/e;)Ljava/util/Collection;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/reflect/jvm/internal/impl/b/e;",
            ")",
            "Ljava/util/Collection",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/b/e;",
            ">;"
        }
    .end annotation

    .prologue
    const-string v0, "sealedClass"

    invoke-static {p0, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 369
    invoke-interface {p0}, Lkotlin/reflect/jvm/internal/impl/b/e;->m()Lkotlin/reflect/jvm/internal/impl/b/u;

    move-result-object v0

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/b/u;->b:Lkotlin/reflect/jvm/internal/impl/b/u;

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    invoke-static {}, Lkotlin/a/m;->a()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    .line 392
    :goto_0
    return-object v0

    .line 371
    :cond_0
    new-instance v3, Ljava/util/LinkedHashSet;

    invoke-direct {v3}, Ljava/util/LinkedHashSet;-><init>()V

    .line 373
    new-instance v1, Lkotlin/reflect/jvm/internal/impl/h/c/a$a;

    invoke-direct {v1, p0, v3}, Lkotlin/reflect/jvm/internal/impl/h/c/a$a;-><init>(Lkotlin/reflect/jvm/internal/impl/b/e;Ljava/util/LinkedHashSet;)V

    .line 387
    invoke-interface {p0}, Lkotlin/reflect/jvm/internal/impl/b/e;->y_()Lkotlin/reflect/jvm/internal/impl/b/m;

    move-result-object v2

    .line 388
    instance-of v0, v2, Lkotlin/reflect/jvm/internal/impl/b/y;

    if-eqz v0, :cond_1

    move-object v0, v1

    .line 389
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/h/c/a$a;

    check-cast v2, Lkotlin/reflect/jvm/internal/impl/b/y;

    invoke-interface {v2}, Lkotlin/reflect/jvm/internal/impl/b/y;->x_()Lkotlin/reflect/jvm/internal/impl/h/e/h;

    move-result-object v2

    const/4 v4, 0x0

    invoke-virtual {v0, v2, v4}, Lkotlin/reflect/jvm/internal/impl/h/c/a$a;->a(Lkotlin/reflect/jvm/internal/impl/h/e/h;Z)V

    .line 391
    :cond_1
    check-cast v1, Lkotlin/reflect/jvm/internal/impl/h/c/a$a;

    invoke-interface {p0}, Lkotlin/reflect/jvm/internal/impl/b/e;->B()Lkotlin/reflect/jvm/internal/impl/h/e/h;

    move-result-object v0

    const-string v2, "sealedClass.unsubstitutedInnerClassesScope"

    invoke-static {v0, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, Lkotlin/reflect/jvm/internal/impl/h/c/a$a;->a(Lkotlin/reflect/jvm/internal/impl/h/e/h;Z)V

    move-object v0, v3

    .line 392
    check-cast v0, Ljava/util/Collection;

    goto :goto_0
.end method

.method public static final c(Lkotlin/reflect/jvm/internal/impl/b/i;)Lkotlin/reflect/jvm/internal/impl/b/e;
    .locals 3

    .prologue
    const-string v0, "$receiver"

    invoke-static {p0, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 102
    invoke-static {p0}, Lkotlin/reflect/jvm/internal/impl/h/c/a;->a(Lkotlin/reflect/jvm/internal/impl/b/i;)Lkotlin/reflect/jvm/internal/impl/b/e;

    move-result-object v0

    if-eqz v0, :cond_3

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/e;

    .line 103
    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/b/e;->l()Lkotlin/reflect/jvm/internal/impl/b/f;

    move-result-object v1

    sget-object v2, Lkotlin/reflect/jvm/internal/impl/h/c/b;->a:[I

    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/impl/b/f;->ordinal()I

    move-result v1

    aget v1, v2, v1

    packed-switch v1, :pswitch_data_0

    .line 111
    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/b/e;->i()Lkotlin/reflect/jvm/internal/impl/b/e;

    move-result-object v0

    .line 102
    :goto_0
    :pswitch_0
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/e;

    .line 113
    :goto_1
    return-object v0

    .line 107
    :pswitch_1
    invoke-interface {p0}, Lkotlin/reflect/jvm/internal/impl/b/i;->y_()Lkotlin/reflect/jvm/internal/impl/b/m;

    move-result-object v1

    .line 108
    instance-of v0, v1, Lkotlin/reflect/jvm/internal/impl/b/e;

    if-eqz v0, :cond_0

    move-object v0, v1

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/e;

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/b/e;->l()Lkotlin/reflect/jvm/internal/impl/b/f;

    move-result-object v0

    sget-object v2, Lkotlin/reflect/jvm/internal/impl/b/f;->c:Lkotlin/reflect/jvm/internal/impl/b/f;

    invoke-static {v0, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_2
    sget-boolean v2, Lkotlin/p;->a:Z

    if-eqz v2, :cond_1

    if-nez v0, :cond_1

    const-string v1, "Assertion failed"

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_2

    .line 109
    :cond_1
    if-nez v1, :cond_2

    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type org.jetbrains.kotlin.descriptors.ClassDescriptor"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    check-cast v1, Lkotlin/reflect/jvm/internal/impl/b/e;

    move-object v0, v1

    goto :goto_0

    .line 102
    :cond_3
    const/4 v0, 0x0

    goto :goto_1

    .line 103
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static final c(Lkotlin/reflect/jvm/internal/impl/b/m;)Lkotlin/reflect/jvm/internal/impl/b/w;
    .locals 2

    .prologue
    const-string v0, "$receiver"

    invoke-static {p0, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 65
    invoke-static {p0}, Lkotlin/reflect/jvm/internal/impl/h/c;->g(Lkotlin/reflect/jvm/internal/impl/b/m;)Lkotlin/reflect/jvm/internal/impl/b/w;

    move-result-object v0

    const-string v1, "DescriptorUtils.getContainingModule(this)"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public static final d(Lkotlin/reflect/jvm/internal/impl/b/m;)Lkotlin/reflect/jvm/internal/impl/a/l;
    .locals 1

    .prologue
    const-string v0, "$receiver"

    invoke-static {p0, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 160
    invoke-static {p0}, Lkotlin/reflect/jvm/internal/impl/h/c/a;->c(Lkotlin/reflect/jvm/internal/impl/b/m;)Lkotlin/reflect/jvm/internal/impl/b/w;

    move-result-object v0

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/b/w;->a()Lkotlin/reflect/jvm/internal/impl/a/l;

    move-result-object v0

    return-object v0
.end method

.method public static final e(Lkotlin/reflect/jvm/internal/impl/b/m;)Lkotlin/g/g;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/reflect/jvm/internal/impl/b/m;",
            ")",
            "Lkotlin/g/g",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/b/m;",
            ">;"
        }
    .end annotation

    .prologue
    const-string v0, "$receiver"

    invoke-static {p0, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 223
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/h/c/a$g;->a:Lkotlin/reflect/jvm/internal/impl/h/c/a$g;

    check-cast v0, Lkotlin/d/a/b;

    invoke-static {p0, v0}, Lkotlin/g/h;->a(Ljava/lang/Object;Lkotlin/d/a/b;)Lkotlin/g/g;

    move-result-object v0

    return-object v0
.end method

.method public static final f(Lkotlin/reflect/jvm/internal/impl/b/m;)Lkotlin/g/g;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/reflect/jvm/internal/impl/b/m;",
            ")",
            "Lkotlin/g/g",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/b/m;",
            ">;"
        }
    .end annotation

    .prologue
    const-string v0, "$receiver"

    invoke-static {p0, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 226
    invoke-static {p0}, Lkotlin/reflect/jvm/internal/impl/h/c/a;->e(Lkotlin/reflect/jvm/internal/impl/b/m;)Lkotlin/g/g;

    move-result-object v0

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lkotlin/g/h;->a(Lkotlin/g/g;I)Lkotlin/g/g;

    move-result-object v0

    return-object v0
.end method
