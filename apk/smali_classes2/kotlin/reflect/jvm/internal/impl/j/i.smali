.class public interface abstract Lkotlin/reflect/jvm/internal/impl/j/i;
.super Ljava/lang/Object;
.source "StorageManager.kt"


# virtual methods
.method public abstract a(Lkotlin/d/a/b;)Lkotlin/reflect/jvm/internal/impl/j/c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            "V:",
            "Ljava/lang/Object;",
            ">(",
            "Lkotlin/d/a/b",
            "<-TK;+TV;>;)",
            "Lkotlin/reflect/jvm/internal/impl/j/c",
            "<TK;TV;>;"
        }
    .end annotation
.end method

.method public abstract a(Lkotlin/d/a/a;)Lkotlin/reflect/jvm/internal/impl/j/f;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lkotlin/d/a/a",
            "<+TT;>;)",
            "Lkotlin/reflect/jvm/internal/impl/j/f",
            "<TT;>;"
        }
    .end annotation
.end method

.method public abstract a(Lkotlin/d/a/a;Ljava/lang/Object;)Lkotlin/reflect/jvm/internal/impl/j/f;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lkotlin/d/a/a",
            "<+TT;>;TT;)",
            "Lkotlin/reflect/jvm/internal/impl/j/f",
            "<TT;>;"
        }
    .end annotation
.end method

.method public abstract a(Lkotlin/d/a/a;Lkotlin/d/a/b;Lkotlin/d/a/b;)Lkotlin/reflect/jvm/internal/impl/j/f;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lkotlin/d/a/a",
            "<+TT;>;",
            "Lkotlin/d/a/b",
            "<-",
            "Ljava/lang/Boolean;",
            "+TT;>;",
            "Lkotlin/d/a/b",
            "<-TT;",
            "Lkotlin/n;",
            ">;)",
            "Lkotlin/reflect/jvm/internal/impl/j/f",
            "<TT;>;"
        }
    .end annotation
.end method

.method public abstract b()Lkotlin/reflect/jvm/internal/impl/j/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            "V:",
            "Ljava/lang/Object;",
            ">()",
            "Lkotlin/reflect/jvm/internal/impl/j/a",
            "<TK;TV;>;"
        }
    .end annotation
.end method

.method public abstract b(Lkotlin/d/a/b;)Lkotlin/reflect/jvm/internal/impl/j/d;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            "V:",
            "Ljava/lang/Object;",
            ">(",
            "Lkotlin/d/a/b",
            "<-TK;+TV;>;)",
            "Lkotlin/reflect/jvm/internal/impl/j/d",
            "<TK;TV;>;"
        }
    .end annotation
.end method

.method public abstract b(Lkotlin/d/a/a;)Lkotlin/reflect/jvm/internal/impl/j/g;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lkotlin/d/a/a",
            "<+TT;>;)",
            "Lkotlin/reflect/jvm/internal/impl/j/g",
            "<TT;>;"
        }
    .end annotation
.end method
