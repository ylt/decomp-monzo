.class final Lkotlin/reflect/jvm/internal/impl/j/b$1;
.super Lkotlin/reflect/jvm/internal/impl/j/b;
.source "LockBasedStorageManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lkotlin/reflect/jvm/internal/impl/j/b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# direct methods
.method constructor <init>(Ljava/lang/String;Lkotlin/reflect/jvm/internal/impl/j/b$c;Ljava/util/concurrent/locks/Lock;)V
    .locals 1

    .prologue
    .line 58
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, Lkotlin/reflect/jvm/internal/impl/j/b;-><init>(Ljava/lang/String;Lkotlin/reflect/jvm/internal/impl/j/b$c;Ljava/util/concurrent/locks/Lock;Lkotlin/reflect/jvm/internal/impl/j/b$1;)V

    return-void
.end method


# virtual methods
.method protected a()Lkotlin/reflect/jvm/internal/impl/j/b$j;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">()",
            "Lkotlin/reflect/jvm/internal/impl/j/b$j",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 62
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/j/b$j;->a()Lkotlin/reflect/jvm/internal/impl/j/b$j;

    move-result-object v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "@NotNull method %s.%s must not return null"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "kotlin/reflect/jvm/internal/impl/storage/LockBasedStorageManager$1"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "recursionDetectedDefault"

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    return-object v0
.end method
