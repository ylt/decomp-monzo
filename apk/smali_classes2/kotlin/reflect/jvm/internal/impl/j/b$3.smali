.class Lkotlin/reflect/jvm/internal/impl/j/b$3;
.super Lkotlin/reflect/jvm/internal/impl/j/b$f;
.source "LockBasedStorageManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lkotlin/reflect/jvm/internal/impl/j/b;->a(Lkotlin/d/a/a;Lkotlin/d/a/b;Lkotlin/d/a/b;)Lkotlin/reflect/jvm/internal/impl/j/f;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/reflect/jvm/internal/impl/j/b$f",
        "<TT;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lkotlin/d/a/b;

.field final synthetic b:Lkotlin/d/a/b;

.field final synthetic c:Lkotlin/reflect/jvm/internal/impl/j/b;


# direct methods
.method constructor <init>(Lkotlin/reflect/jvm/internal/impl/j/b;Lkotlin/reflect/jvm/internal/impl/j/b;Lkotlin/d/a/a;Lkotlin/d/a/b;Lkotlin/d/a/b;)V
    .locals 0

    .prologue
    .line 159
    iput-object p1, p0, Lkotlin/reflect/jvm/internal/impl/j/b$3;->c:Lkotlin/reflect/jvm/internal/impl/j/b;

    iput-object p4, p0, Lkotlin/reflect/jvm/internal/impl/j/b$3;->a:Lkotlin/d/a/b;

    iput-object p5, p0, Lkotlin/reflect/jvm/internal/impl/j/b$3;->b:Lkotlin/d/a/b;

    invoke-direct {p0, p2, p3}, Lkotlin/reflect/jvm/internal/impl/j/b$f;-><init>(Lkotlin/reflect/jvm/internal/impl/j/b;Lkotlin/d/a/a;)V

    return-void
.end method


# virtual methods
.method protected a(Z)Lkotlin/reflect/jvm/internal/impl/j/b$j;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)",
            "Lkotlin/reflect/jvm/internal/impl/j/b$j",
            "<TT;>;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 163
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/j/b$3;->a:Lkotlin/d/a/b;

    if-nez v0, :cond_0

    .line 164
    invoke-super {p0, p1}, Lkotlin/reflect/jvm/internal/impl/j/b$f;->a(Z)Lkotlin/reflect/jvm/internal/impl/j/b$j;

    move-result-object v0

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "@NotNull method %s.%s must not return null"

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "kotlin/reflect/jvm/internal/impl/storage/LockBasedStorageManager$3"

    aput-object v3, v2, v4

    const-string v3, "recursionDetected"

    aput-object v3, v2, v5

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 166
    :cond_0
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/j/b$3;->a:Lkotlin/d/a/b;

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-interface {v0, v1}, Lkotlin/d/a/b;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/j/b$j;->a(Ljava/lang/Object;)Lkotlin/reflect/jvm/internal/impl/j/b$j;

    move-result-object v0

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "@NotNull method %s.%s must not return null"

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "kotlin/reflect/jvm/internal/impl/storage/LockBasedStorageManager$3"

    aput-object v3, v2, v4

    const-string v3, "recursionDetected"

    aput-object v3, v2, v5

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    return-object v0
.end method

.method protected a(Ljava/lang/Object;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .prologue
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "value"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "kotlin/reflect/jvm/internal/impl/storage/LockBasedStorageManager$3"

    aput-object v4, v2, v3

    const/4 v3, 0x2

    const-string v4, "postCompute"

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 171
    :cond_0
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/j/b$3;->b:Lkotlin/d/a/b;

    invoke-interface {v0, p1}, Lkotlin/d/a/b;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 172
    return-void
.end method
