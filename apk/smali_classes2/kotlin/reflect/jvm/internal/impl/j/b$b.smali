.class Lkotlin/reflect/jvm/internal/impl/j/b$b;
.super Lkotlin/reflect/jvm/internal/impl/j/b$g;
.source "LockBasedStorageManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lkotlin/reflect/jvm/internal/impl/j/b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "Lkotlin/reflect/jvm/internal/impl/j/b$g",
        "<",
        "Lkotlin/reflect/jvm/internal/impl/j/b$d",
        "<TK;TV;>;TV;>;"
    }
.end annotation


# direct methods
.method private constructor <init>(Lkotlin/reflect/jvm/internal/impl/j/b;Ljava/util/concurrent/ConcurrentMap;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/reflect/jvm/internal/impl/j/b;",
            "Ljava/util/concurrent/ConcurrentMap",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/j/b$d",
            "<TK;TV;>;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "storageManager"

    aput-object v3, v2, v4

    const-string v3, "kotlin/reflect/jvm/internal/impl/storage/LockBasedStorageManager$CacheWithNullableValuesBasedOnMemoizedFunction"

    aput-object v3, v2, v5

    const-string v3, "<init>"

    aput-object v3, v2, v6

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    if-nez p2, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "map"

    aput-object v3, v2, v4

    const-string v3, "kotlin/reflect/jvm/internal/impl/storage/LockBasedStorageManager$CacheWithNullableValuesBasedOnMemoizedFunction"

    aput-object v3, v2, v5

    const-string v3, "<init>"

    aput-object v3, v2, v6

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 517
    :cond_1
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/j/b$b$1;

    invoke-direct {v0}, Lkotlin/reflect/jvm/internal/impl/j/b$b$1;-><init>()V

    invoke-direct {p0, p1, p2, v0}, Lkotlin/reflect/jvm/internal/impl/j/b$g;-><init>(Lkotlin/reflect/jvm/internal/impl/j/b;Ljava/util/concurrent/ConcurrentMap;Lkotlin/d/a/b;)V

    .line 523
    return-void
.end method

.method synthetic constructor <init>(Lkotlin/reflect/jvm/internal/impl/j/b;Ljava/util/concurrent/ConcurrentMap;Lkotlin/reflect/jvm/internal/impl/j/b$1;)V
    .locals 0

    .prologue
    .line 511
    invoke-direct {p0, p1, p2}, Lkotlin/reflect/jvm/internal/impl/j/b$b;-><init>(Lkotlin/reflect/jvm/internal/impl/j/b;Ljava/util/concurrent/ConcurrentMap;)V

    return-void
.end method


# virtual methods
.method public a(Ljava/lang/Object;Lkotlin/d/a/a;)Ljava/lang/Object;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;",
            "Lkotlin/d/a/a",
            "<+TV;>;)TV;"
        }
    .end annotation

    .prologue
    if-nez p2, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "computation"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "kotlin/reflect/jvm/internal/impl/storage/LockBasedStorageManager$CacheWithNullableValuesBasedOnMemoizedFunction"

    aput-object v4, v2, v3

    const/4 v3, 0x2

    const-string v4, "computeIfAbsent"

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 528
    :cond_0
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/j/b$d;

    invoke-direct {v0, p1, p2}, Lkotlin/reflect/jvm/internal/impl/j/b$d;-><init>(Ljava/lang/Object;Lkotlin/d/a/a;)V

    invoke-virtual {p0, v0}, Lkotlin/reflect/jvm/internal/impl/j/b$b;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method
