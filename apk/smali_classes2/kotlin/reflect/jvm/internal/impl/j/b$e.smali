.class Lkotlin/reflect/jvm/internal/impl/j/b$e;
.super Ljava/lang/Object;
.source "LockBasedStorageManager.java"

# interfaces
.implements Lkotlin/reflect/jvm/internal/impl/j/g;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lkotlin/reflect/jvm/internal/impl/j/b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "e"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lkotlin/reflect/jvm/internal/impl/j/g",
        "<TT;>;"
    }
.end annotation


# instance fields
.field private final a:Lkotlin/reflect/jvm/internal/impl/j/b;

.field private final b:Lkotlin/d/a/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/d/a/a",
            "<+TT;>;"
        }
    .end annotation
.end field

.field private volatile c:Ljava/lang/Object;


# direct methods
.method public constructor <init>(Lkotlin/reflect/jvm/internal/impl/j/b;Lkotlin/d/a/a;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/reflect/jvm/internal/impl/j/b;",
            "Lkotlin/d/a/a",
            "<+TT;>;)V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "storageManager"

    aput-object v3, v2, v4

    const-string v3, "kotlin/reflect/jvm/internal/impl/storage/LockBasedStorageManager$LockBasedLazyValue"

    aput-object v3, v2, v5

    const-string v3, "<init>"

    aput-object v3, v2, v6

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    if-nez p2, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "computable"

    aput-object v3, v2, v4

    const-string v3, "kotlin/reflect/jvm/internal/impl/storage/LockBasedStorageManager$LockBasedLazyValue"

    aput-object v3, v2, v5

    const-string v3, "<init>"

    aput-object v3, v2, v6

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 281
    :cond_1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 278
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/j/b$i;->a:Lkotlin/reflect/jvm/internal/impl/j/b$i;

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/j/b$e;->c:Ljava/lang/Object;

    .line 282
    iput-object p1, p0, Lkotlin/reflect/jvm/internal/impl/j/b$e;->a:Lkotlin/reflect/jvm/internal/impl/j/b;

    .line 283
    iput-object p2, p0, Lkotlin/reflect/jvm/internal/impl/j/b$e;->b:Lkotlin/d/a/a;

    .line 284
    return-void
.end method


# virtual methods
.method protected a(Z)Lkotlin/reflect/jvm/internal/impl/j/b$j;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)",
            "Lkotlin/reflect/jvm/internal/impl/j/b$j",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 347
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/j/b$e;->a:Lkotlin/reflect/jvm/internal/impl/j/b;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/j/b;->a()Lkotlin/reflect/jvm/internal/impl/j/b$j;

    move-result-object v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "@NotNull method %s.%s must not return null"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "kotlin/reflect/jvm/internal/impl/storage/LockBasedStorageManager$LockBasedLazyValue"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "recursionDetected"

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    return-object v0
.end method

.method protected a(Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .prologue
    .line 352
    return-void
.end method

.method public b()Z
    .locals 2

    .prologue
    .line 288
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/j/b$e;->c:Ljava/lang/Object;

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/j/b$i;->a:Lkotlin/reflect/jvm/internal/impl/j/b$i;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/j/b$e;->c:Ljava/lang/Object;

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/j/b$i;->b:Lkotlin/reflect/jvm/internal/impl/j/b$i;

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public v_()Ljava/lang/Object;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .prologue
    .line 298
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/j/b$e;->c:Ljava/lang/Object;

    .line 299
    instance-of v1, v0, Lkotlin/reflect/jvm/internal/impl/j/b$i;

    if-nez v1, :cond_0

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/utils/WrappedValues;->d(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 337
    :goto_0
    return-object v0

    .line 301
    :cond_0
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/j/b$e;->a:Lkotlin/reflect/jvm/internal/impl/j/b;

    iget-object v0, v0, Lkotlin/reflect/jvm/internal/impl/j/b;->b:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    .line 303
    :try_start_0
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/j/b$e;->c:Ljava/lang/Object;

    .line 304
    instance-of v1, v0, Lkotlin/reflect/jvm/internal/impl/j/b$i;

    if-nez v1, :cond_1

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/utils/WrappedValues;->d(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 337
    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/j/b$e;->a:Lkotlin/reflect/jvm/internal/impl/j/b;

    iget-object v1, v1, Lkotlin/reflect/jvm/internal/impl/j/b;->b:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    goto :goto_0

    .line 306
    :cond_1
    :try_start_1
    sget-object v1, Lkotlin/reflect/jvm/internal/impl/j/b$i;->b:Lkotlin/reflect/jvm/internal/impl/j/b$i;

    if-ne v0, v1, :cond_2

    .line 307
    sget-object v1, Lkotlin/reflect/jvm/internal/impl/j/b$i;->c:Lkotlin/reflect/jvm/internal/impl/j/b$i;

    iput-object v1, p0, Lkotlin/reflect/jvm/internal/impl/j/b$e;->c:Ljava/lang/Object;

    .line 308
    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lkotlin/reflect/jvm/internal/impl/j/b$e;->a(Z)Lkotlin/reflect/jvm/internal/impl/j/b$j;

    move-result-object v1

    .line 309
    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/impl/j/b$j;->c()Z

    move-result v2

    if-nez v2, :cond_2

    .line 310
    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/impl/j/b$j;->b()Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    .line 337
    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/j/b$e;->a:Lkotlin/reflect/jvm/internal/impl/j/b;

    iget-object v1, v1, Lkotlin/reflect/jvm/internal/impl/j/b;->b:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    goto :goto_0

    .line 314
    :cond_2
    :try_start_2
    sget-object v1, Lkotlin/reflect/jvm/internal/impl/j/b$i;->c:Lkotlin/reflect/jvm/internal/impl/j/b$i;

    if-ne v0, v1, :cond_3

    .line 315
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lkotlin/reflect/jvm/internal/impl/j/b$e;->a(Z)Lkotlin/reflect/jvm/internal/impl/j/b$j;

    move-result-object v0

    .line 316
    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/j/b$j;->c()Z

    move-result v1

    if-nez v1, :cond_3

    .line 317
    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/j/b$j;->b()Ljava/lang/Object;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v0

    .line 337
    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/j/b$e;->a:Lkotlin/reflect/jvm/internal/impl/j/b;

    iget-object v1, v1, Lkotlin/reflect/jvm/internal/impl/j/b;->b:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    goto :goto_0

    .line 321
    :cond_3
    :try_start_3
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/j/b$i;->b:Lkotlin/reflect/jvm/internal/impl/j/b$i;

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/j/b$e;->c:Ljava/lang/Object;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 323
    :try_start_4
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/j/b$e;->b:Lkotlin/d/a/a;

    invoke-interface {v0}, Lkotlin/d/a/a;->v_()Ljava/lang/Object;

    move-result-object v0

    .line 324
    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/j/b$e;->c:Ljava/lang/Object;

    .line 325
    invoke-virtual {p0, v0}, Lkotlin/reflect/jvm/internal/impl/j/b$e;->a(Ljava/lang/Object;)V
    :try_end_4
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 337
    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/j/b$e;->a:Lkotlin/reflect/jvm/internal/impl/j/b;

    iget-object v1, v1, Lkotlin/reflect/jvm/internal/impl/j/b;->b:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    goto :goto_0

    .line 328
    :catch_0
    move-exception v0

    .line 329
    :try_start_5
    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/j/b$e;->c:Ljava/lang/Object;

    sget-object v2, Lkotlin/reflect/jvm/internal/impl/j/b$i;->b:Lkotlin/reflect/jvm/internal/impl/j/b$i;

    if-ne v1, v2, :cond_4

    .line 331
    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/utils/WrappedValues;->a(Ljava/lang/Throwable;)Ljava/lang/Object;

    move-result-object v1

    iput-object v1, p0, Lkotlin/reflect/jvm/internal/impl/j/b$e;->c:Ljava/lang/Object;

    .line 333
    :cond_4
    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/j/b$e;->a:Lkotlin/reflect/jvm/internal/impl/j/b;

    invoke-static {v1}, Lkotlin/reflect/jvm/internal/impl/j/b;->a(Lkotlin/reflect/jvm/internal/impl/j/b;)Lkotlin/reflect/jvm/internal/impl/j/b$c;

    move-result-object v1

    invoke-interface {v1, v0}, Lkotlin/reflect/jvm/internal/impl/j/b$c;->a(Ljava/lang/Throwable;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 337
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/j/b$e;->a:Lkotlin/reflect/jvm/internal/impl/j/b;

    iget-object v1, v1, Lkotlin/reflect/jvm/internal/impl/j/b;->b:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0
.end method
