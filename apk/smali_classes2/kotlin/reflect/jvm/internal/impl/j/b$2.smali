.class Lkotlin/reflect/jvm/internal/impl/j/b$2;
.super Lkotlin/reflect/jvm/internal/impl/j/b$f;
.source "LockBasedStorageManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lkotlin/reflect/jvm/internal/impl/j/b;->a(Lkotlin/d/a/a;Ljava/lang/Object;)Lkotlin/reflect/jvm/internal/impl/j/f;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/reflect/jvm/internal/impl/j/b$f",
        "<TT;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Ljava/lang/Object;

.field final synthetic b:Lkotlin/reflect/jvm/internal/impl/j/b;


# direct methods
.method constructor <init>(Lkotlin/reflect/jvm/internal/impl/j/b;Lkotlin/reflect/jvm/internal/impl/j/b;Lkotlin/d/a/a;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 143
    iput-object p1, p0, Lkotlin/reflect/jvm/internal/impl/j/b$2;->b:Lkotlin/reflect/jvm/internal/impl/j/b;

    iput-object p4, p0, Lkotlin/reflect/jvm/internal/impl/j/b$2;->a:Ljava/lang/Object;

    invoke-direct {p0, p2, p3}, Lkotlin/reflect/jvm/internal/impl/j/b$f;-><init>(Lkotlin/reflect/jvm/internal/impl/j/b;Lkotlin/d/a/a;)V

    return-void
.end method


# virtual methods
.method protected a(Z)Lkotlin/reflect/jvm/internal/impl/j/b$j;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)",
            "Lkotlin/reflect/jvm/internal/impl/j/b$j",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 147
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/j/b$2;->a:Ljava/lang/Object;

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/j/b$j;->a(Ljava/lang/Object;)Lkotlin/reflect/jvm/internal/impl/j/b$j;

    move-result-object v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "@NotNull method %s.%s must not return null"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "kotlin/reflect/jvm/internal/impl/storage/LockBasedStorageManager$2"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "recursionDetected"

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    return-object v0
.end method
