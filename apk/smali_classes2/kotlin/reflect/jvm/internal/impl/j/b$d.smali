.class Lkotlin/reflect/jvm/internal/impl/j/b$d;
.super Ljava/lang/Object;
.source "LockBasedStorageManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lkotlin/reflect/jvm/internal/impl/j/b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "d"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field private final a:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TK;"
        }
    .end annotation
.end field

.field private final b:Lkotlin/d/a/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/d/a/a",
            "<+TV;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/Object;Lkotlin/d/a/a;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;",
            "Lkotlin/d/a/a",
            "<+TV;>;)V"
        }
    .end annotation

    .prologue
    .line 561
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 562
    iput-object p1, p0, Lkotlin/reflect/jvm/internal/impl/j/b$d;->a:Ljava/lang/Object;

    .line 563
    iput-object p2, p0, Lkotlin/reflect/jvm/internal/impl/j/b$d;->b:Lkotlin/d/a/a;

    .line 564
    return-void
.end method

.method static synthetic a(Lkotlin/reflect/jvm/internal/impl/j/b$d;)Lkotlin/d/a/a;
    .locals 1

    .prologue
    .line 557
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/j/b$d;->b:Lkotlin/d/a/a;

    return-object v0
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 568
    if-ne p0, p1, :cond_1

    .line 575
    :cond_0
    :goto_0
    return v0

    .line 569
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    goto :goto_0

    .line 571
    :cond_3
    check-cast p1, Lkotlin/reflect/jvm/internal/impl/j/b$d;

    .line 573
    iget-object v2, p0, Lkotlin/reflect/jvm/internal/impl/j/b$d;->a:Ljava/lang/Object;

    iget-object v3, p1, Lkotlin/reflect/jvm/internal/impl/j/b$d;->a:Ljava/lang/Object;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 580
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/j/b$d;->a:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    return v0
.end method
