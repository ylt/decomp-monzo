.class public Lkotlin/reflect/jvm/internal/impl/utils/b;
.super Ljava/lang/Object;
.source "DFS.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lkotlin/reflect/jvm/internal/impl/utils/b$e;,
        Lkotlin/reflect/jvm/internal/impl/utils/b$a;,
        Lkotlin/reflect/jvm/internal/impl/utils/b$d;,
        Lkotlin/reflect/jvm/internal/impl/utils/b$b;,
        Lkotlin/reflect/jvm/internal/impl/utils/b$c;
    }
.end annotation


# direct methods
.method public static a(Ljava/util/Collection;Lkotlin/reflect/jvm/internal/impl/utils/b$b;Lkotlin/d/a/b;)Ljava/lang/Boolean;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<N:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/Collection",
            "<TN;>;",
            "Lkotlin/reflect/jvm/internal/impl/utils/b$b",
            "<TN;>;",
            "Lkotlin/d/a/b",
            "<TN;",
            "Ljava/lang/Boolean;",
            ">;)",
            "Ljava/lang/Boolean;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x0

    const/4 v4, 0x1

    if-nez p0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "nodes"

    aput-object v3, v2, v5

    const-string v3, "kotlin/reflect/jvm/internal/impl/utils/DFS"

    aput-object v3, v2, v4

    const-string v3, "ifAny"

    aput-object v3, v2, v6

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    if-nez p1, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "neighbors"

    aput-object v3, v2, v5

    const-string v3, "kotlin/reflect/jvm/internal/impl/utils/DFS"

    aput-object v3, v2, v4

    const-string v3, "ifAny"

    aput-object v3, v2, v6

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    if-nez p2, :cond_2

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "predicate"

    aput-object v3, v2, v5

    const-string v3, "kotlin/reflect/jvm/internal/impl/utils/DFS"

    aput-object v3, v2, v4

    const-string v3, "ifAny"

    aput-object v3, v2, v6

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 45
    :cond_2
    new-array v0, v4, [Z

    .line 47
    new-instance v1, Lkotlin/reflect/jvm/internal/impl/utils/b$1;

    invoke-direct {v1, p2, v0}, Lkotlin/reflect/jvm/internal/impl/utils/b$1;-><init>(Lkotlin/d/a/b;[Z)V

    invoke-static {p0, p1, v1}, Lkotlin/reflect/jvm/internal/impl/utils/b;->a(Ljava/util/Collection;Lkotlin/reflect/jvm/internal/impl/utils/b$b;Lkotlin/reflect/jvm/internal/impl/utils/b$c;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    return-object v0
.end method

.method public static a(Ljava/util/Collection;Lkotlin/reflect/jvm/internal/impl/utils/b$b;Lkotlin/reflect/jvm/internal/impl/utils/b$c;)Ljava/lang/Object;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<N:",
            "Ljava/lang/Object;",
            "R:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/Collection",
            "<TN;>;",
            "Lkotlin/reflect/jvm/internal/impl/utils/b$b",
            "<TN;>;",
            "Lkotlin/reflect/jvm/internal/impl/utils/b$c",
            "<TN;TR;>;)TR;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    if-nez p0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "nodes"

    aput-object v3, v2, v4

    const-string v3, "kotlin/reflect/jvm/internal/impl/utils/DFS"

    aput-object v3, v2, v5

    const-string v3, "dfs"

    aput-object v3, v2, v6

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    if-nez p1, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "neighbors"

    aput-object v3, v2, v4

    const-string v3, "kotlin/reflect/jvm/internal/impl/utils/DFS"

    aput-object v3, v2, v5

    const-string v3, "dfs"

    aput-object v3, v2, v6

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    if-nez p2, :cond_2

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "handler"

    aput-object v3, v2, v4

    const-string v3, "kotlin/reflect/jvm/internal/impl/utils/DFS"

    aput-object v3, v2, v5

    const-string v3, "dfs"

    aput-object v3, v2, v6

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 37
    :cond_2
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/utils/b$e;

    invoke-direct {v0}, Lkotlin/reflect/jvm/internal/impl/utils/b$e;-><init>()V

    invoke-static {p0, p1, v0, p2}, Lkotlin/reflect/jvm/internal/impl/utils/b;->a(Ljava/util/Collection;Lkotlin/reflect/jvm/internal/impl/utils/b$b;Lkotlin/reflect/jvm/internal/impl/utils/b$d;Lkotlin/reflect/jvm/internal/impl/utils/b$c;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/util/Collection;Lkotlin/reflect/jvm/internal/impl/utils/b$b;Lkotlin/reflect/jvm/internal/impl/utils/b$d;Lkotlin/reflect/jvm/internal/impl/utils/b$c;)Ljava/lang/Object;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<N:",
            "Ljava/lang/Object;",
            "R:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/Collection",
            "<TN;>;",
            "Lkotlin/reflect/jvm/internal/impl/utils/b$b",
            "<TN;>;",
            "Lkotlin/reflect/jvm/internal/impl/utils/b$d",
            "<TN;>;",
            "Lkotlin/reflect/jvm/internal/impl/utils/b$c",
            "<TN;TR;>;)TR;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    if-nez p0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "nodes"

    aput-object v3, v2, v4

    const-string v3, "kotlin/reflect/jvm/internal/impl/utils/DFS"

    aput-object v3, v2, v5

    const-string v3, "dfs"

    aput-object v3, v2, v6

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    if-nez p1, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "neighbors"

    aput-object v3, v2, v4

    const-string v3, "kotlin/reflect/jvm/internal/impl/utils/DFS"

    aput-object v3, v2, v5

    const-string v3, "dfs"

    aput-object v3, v2, v6

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    if-nez p2, :cond_2

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "visited"

    aput-object v3, v2, v4

    const-string v3, "kotlin/reflect/jvm/internal/impl/utils/DFS"

    aput-object v3, v2, v5

    const-string v3, "dfs"

    aput-object v3, v2, v6

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    if-nez p3, :cond_3

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "handler"

    aput-object v3, v2, v4

    const-string v3, "kotlin/reflect/jvm/internal/impl/utils/DFS"

    aput-object v3, v2, v5

    const-string v3, "dfs"

    aput-object v3, v2, v6

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 26
    :cond_3
    invoke-interface {p0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 27
    invoke-static {v1, p1, p2, p3}, Lkotlin/reflect/jvm/internal/impl/utils/b;->a(Ljava/lang/Object;Lkotlin/reflect/jvm/internal/impl/utils/b$b;Lkotlin/reflect/jvm/internal/impl/utils/b$d;Lkotlin/reflect/jvm/internal/impl/utils/b$c;)V

    goto :goto_0

    .line 29
    :cond_4
    invoke-interface {p3}, Lkotlin/reflect/jvm/internal/impl/utils/b$c;->b()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method private static a(Ljava/lang/Object;Lkotlin/reflect/jvm/internal/impl/utils/b$b;Lkotlin/reflect/jvm/internal/impl/utils/b$d;Lkotlin/reflect/jvm/internal/impl/utils/b$c;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<N:",
            "Ljava/lang/Object;",
            ">(TN;",
            "Lkotlin/reflect/jvm/internal/impl/utils/b$b",
            "<TN;>;",
            "Lkotlin/reflect/jvm/internal/impl/utils/b$d",
            "<TN;>;",
            "Lkotlin/reflect/jvm/internal/impl/utils/b$c",
            "<TN;*>;)V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    if-nez p0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "current"

    aput-object v3, v2, v4

    const-string v3, "kotlin/reflect/jvm/internal/impl/utils/DFS"

    aput-object v3, v2, v5

    const-string v3, "doDfs"

    aput-object v3, v2, v6

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    if-nez p1, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "neighbors"

    aput-object v3, v2, v4

    const-string v3, "kotlin/reflect/jvm/internal/impl/utils/DFS"

    aput-object v3, v2, v5

    const-string v3, "doDfs"

    aput-object v3, v2, v6

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    if-nez p2, :cond_2

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "visited"

    aput-object v3, v2, v4

    const-string v3, "kotlin/reflect/jvm/internal/impl/utils/DFS"

    aput-object v3, v2, v5

    const-string v3, "doDfs"

    aput-object v3, v2, v6

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    if-nez p3, :cond_3

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "handler"

    aput-object v3, v2, v4

    const-string v3, "kotlin/reflect/jvm/internal/impl/utils/DFS"

    aput-object v3, v2, v5

    const-string v3, "doDfs"

    aput-object v3, v2, v6

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 95
    :cond_3
    invoke-interface {p2, p0}, Lkotlin/reflect/jvm/internal/impl/utils/b$d;->a(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 102
    :cond_4
    :goto_0
    return-void

    .line 96
    :cond_5
    invoke-interface {p3, p0}, Lkotlin/reflect/jvm/internal/impl/utils/b$c;->a(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 98
    invoke-interface {p1, p0}, Lkotlin/reflect/jvm/internal/impl/utils/b$b;->a(Ljava/lang/Object;)Ljava/lang/Iterable;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_6

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 99
    invoke-static {v1, p1, p2, p3}, Lkotlin/reflect/jvm/internal/impl/utils/b;->a(Ljava/lang/Object;Lkotlin/reflect/jvm/internal/impl/utils/b$b;Lkotlin/reflect/jvm/internal/impl/utils/b$d;Lkotlin/reflect/jvm/internal/impl/utils/b$c;)V

    goto :goto_1

    .line 101
    :cond_6
    invoke-interface {p3, p0}, Lkotlin/reflect/jvm/internal/impl/utils/b$c;->b(Ljava/lang/Object;)V

    goto :goto_0
.end method
