.class Lkotlin/reflect/jvm/internal/impl/utils/e$b;
.super Lkotlin/reflect/jvm/internal/impl/utils/e$c;
.source "SmartList.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lkotlin/reflect/jvm/internal/impl/utils/e;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/reflect/jvm/internal/impl/utils/e$c",
        "<TE;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lkotlin/reflect/jvm/internal/impl/utils/e;

.field private final b:I


# direct methods
.method public constructor <init>(Lkotlin/reflect/jvm/internal/impl/utils/e;)V
    .locals 1

    .prologue
    .line 264
    iput-object p1, p0, Lkotlin/reflect/jvm/internal/impl/utils/e$b;->a:Lkotlin/reflect/jvm/internal/impl/utils/e;

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lkotlin/reflect/jvm/internal/impl/utils/e$c;-><init>(Lkotlin/reflect/jvm/internal/impl/utils/e$1;)V

    .line 265
    invoke-static {p1}, Lkotlin/reflect/jvm/internal/impl/utils/e;->a(Lkotlin/reflect/jvm/internal/impl/utils/e;)I

    move-result v0

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/utils/e$b;->b:I

    .line 266
    return-void
.end method


# virtual methods
.method protected a()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TE;"
        }
    .end annotation

    .prologue
    .line 270
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/utils/e$b;->a:Lkotlin/reflect/jvm/internal/impl/utils/e;

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/utils/e;->b(Lkotlin/reflect/jvm/internal/impl/utils/e;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method protected b()V
    .locals 3

    .prologue
    .line 275
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/utils/e$b;->a:Lkotlin/reflect/jvm/internal/impl/utils/e;

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/utils/e;->c(Lkotlin/reflect/jvm/internal/impl/utils/e;)I

    move-result v0

    iget v1, p0, Lkotlin/reflect/jvm/internal/impl/utils/e$b;->b:I

    if-eq v0, v1, :cond_0

    .line 276
    new-instance v0, Ljava/util/ConcurrentModificationException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "ModCount: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lkotlin/reflect/jvm/internal/impl/utils/e$b;->a:Lkotlin/reflect/jvm/internal/impl/utils/e;

    invoke-static {v2}, Lkotlin/reflect/jvm/internal/impl/utils/e;->d(Lkotlin/reflect/jvm/internal/impl/utils/e;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "; expected: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lkotlin/reflect/jvm/internal/impl/utils/e$b;->b:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/ConcurrentModificationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 278
    :cond_0
    return-void
.end method

.method public remove()V
    .locals 1

    .prologue
    .line 282
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/utils/e$b;->b()V

    .line 283
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/utils/e$b;->a:Lkotlin/reflect/jvm/internal/impl/utils/e;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/utils/e;->clear()V

    .line 284
    return-void
.end method
