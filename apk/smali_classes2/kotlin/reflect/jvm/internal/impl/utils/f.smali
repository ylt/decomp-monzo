.class public final Lkotlin/reflect/jvm/internal/impl/utils/f;
.super Ljava/util/AbstractSet;
.source "SmartSet.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lkotlin/reflect/jvm/internal/impl/utils/f$c;,
        Lkotlin/reflect/jvm/internal/impl/utils/f$a;,
        Lkotlin/reflect/jvm/internal/impl/utils/f$b;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/util/AbstractSet",
        "<TT;>;"
    }
.end annotation


# static fields
.field public static final a:Lkotlin/reflect/jvm/internal/impl/utils/f$b;

# The value of this static final field might be set in the static constructor
.field private static final d:I = 0x5


# instance fields
.field private b:Ljava/lang/Object;

.field private c:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/utils/f$b;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lkotlin/reflect/jvm/internal/impl/utils/f$b;-><init>(Lkotlin/d/b/i;)V

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/utils/f;->a:Lkotlin/reflect/jvm/internal/impl/utils/f$b;

    .line 30
    const/4 v0, 0x5

    sput v0, Lkotlin/reflect/jvm/internal/impl/utils/f;->d:I

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/util/AbstractSet;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/d/b/i;)V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/utils/f;-><init>()V

    return-void
.end method

.method public static final synthetic b()I
    .locals 1

    .prologue
    .line 28
    sget v0, Lkotlin/reflect/jvm/internal/impl/utils/f;->d:I

    return v0
.end method

.method public static final c()Lkotlin/reflect/jvm/internal/impl/utils/f;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">()",
            "Lkotlin/reflect/jvm/internal/impl/utils/f",
            "<TT;>;"
        }
    .end annotation

    sget-object v0, Lkotlin/reflect/jvm/internal/impl/utils/f;->a:Lkotlin/reflect/jvm/internal/impl/utils/f$b;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/utils/f$b;->a()Lkotlin/reflect/jvm/internal/impl/utils/f;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 42
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/utils/f;->c:I

    return v0
.end method

.method public a(I)V
    .locals 0

    .prologue
    .line 42
    iput p1, p0, Lkotlin/reflect/jvm/internal/impl/utils/f;->c:I

    return-void
.end method

.method public add(Ljava/lang/Object;)Z
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)Z"
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 53
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/utils/f;->size()I

    move-result v0

    if-nez v0, :cond_1

    .line 54
    iput-object p1, p0, Lkotlin/reflect/jvm/internal/impl/utils/f;->b:Ljava/lang/Object;

    .line 72
    :cond_0
    :goto_0
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/utils/f;->size()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    invoke-virtual {p0, v0}, Lkotlin/reflect/jvm/internal/impl/utils/f;->a(I)V

    move v0, v2

    .line 73
    :goto_1
    return v0

    .line 56
    :cond_1
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/utils/f;->size()I

    move-result v0

    if-ne v0, v2, :cond_3

    .line 57
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/utils/f;->b:Ljava/lang/Object;

    invoke-static {v0, p1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v1

    goto :goto_1

    .line 58
    :cond_2
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    iget-object v3, p0, Lkotlin/reflect/jvm/internal/impl/utils/f;->b:Ljava/lang/Object;

    aput-object v3, v0, v1

    aput-object p1, v0, v2

    .line 112
    check-cast v0, [Ljava/lang/Object;

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/utils/f;->b:Ljava/lang/Object;

    goto :goto_0

    .line 60
    :cond_3
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/utils/f;->size()I

    move-result v0

    sget-object v3, Lkotlin/reflect/jvm/internal/impl/utils/f;->a:Lkotlin/reflect/jvm/internal/impl/utils/f$b;

    invoke-static {v3}, Lkotlin/reflect/jvm/internal/impl/utils/f$b;->a(Lkotlin/reflect/jvm/internal/impl/utils/f$b;)I

    move-result v3

    if-ge v0, v3, :cond_7

    .line 61
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/utils/f;->b:Ljava/lang/Object;

    if-nez v0, :cond_4

    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type kotlin.Array<T>"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_4
    check-cast v0, [Ljava/lang/Object;

    .line 62
    invoke-static {v0, p1}, Lkotlin/a/g;->a([Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_5

    move v0, v1

    goto :goto_1

    .line 63
    :cond_5
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/utils/f;->size()I

    move-result v1

    sget-object v3, Lkotlin/reflect/jvm/internal/impl/utils/f;->a:Lkotlin/reflect/jvm/internal/impl/utils/f$b;

    invoke-static {v3}, Lkotlin/reflect/jvm/internal/impl/utils/f$b;->a(Lkotlin/reflect/jvm/internal/impl/utils/f$b;)I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    if-ne v1, v3, :cond_6

    array-length v1, v0

    invoke-static {v0, v1}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/Object;

    invoke-static {v0}, Lkotlin/a/ah;->b([Ljava/lang/Object;)Ljava/util/LinkedHashSet;

    move-result-object v1

    move-object v0, v1

    check-cast v0, Ljava/util/LinkedHashSet;

    invoke-virtual {v0, p1}, Ljava/util/LinkedHashSet;->add(Ljava/lang/Object;)Z

    move-object v0, p0

    :goto_2
    iput-object v1, v0, Lkotlin/reflect/jvm/internal/impl/utils/f;->b:Ljava/lang/Object;

    goto :goto_0

    .line 64
    :cond_6
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/utils/f;->size()I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    invoke-static {v0, v1}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v1

    move-object v0, v1

    check-cast v0, [Ljava/lang/Object;

    array-length v3, v0

    add-int/lit8 v3, v3, -0x1

    aput-object p1, v0, v3

    move-object v0, p0

    goto :goto_2

    .line 67
    :cond_7
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/utils/f;->b:Ljava/lang/Object;

    if-nez v0, :cond_8

    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type kotlin.collections.MutableSet<T>"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_8
    invoke-static {v0}, Lkotlin/d/b/ac;->a(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v0

    .line 68
    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    goto/16 :goto_1
.end method

.method public clear()V
    .locals 1

    .prologue
    .line 77
    const/4 v0, 0x0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/utils/f;->b:Ljava/lang/Object;

    .line 78
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lkotlin/reflect/jvm/internal/impl/utils/f;->a(I)V

    .line 79
    return-void
.end method

.method public contains(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 82
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/utils/f;->size()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    .line 86
    :goto_0
    return v0

    .line 83
    :cond_0
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/utils/f;->size()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/utils/f;->b:Ljava/lang/Object;

    invoke-static {v0, p1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0

    .line 84
    :cond_1
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/utils/f;->size()I

    move-result v0

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/utils/f;->a:Lkotlin/reflect/jvm/internal/impl/utils/f$b;

    invoke-static {v1}, Lkotlin/reflect/jvm/internal/impl/utils/f$b;->a(Lkotlin/reflect/jvm/internal/impl/utils/f$b;)I

    move-result v1

    if-ge v0, v1, :cond_3

    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/utils/f;->b:Ljava/lang/Object;

    if-nez v0, :cond_2

    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type kotlin.Array<T>"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    check-cast v0, [Ljava/lang/Object;

    invoke-static {v0, p1}, Lkotlin/a/g;->a([Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0

    .line 85
    :cond_3
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/utils/f;->b:Ljava/lang/Object;

    if-nez v0, :cond_4

    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type kotlin.collections.Set<T>"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_4
    check-cast v0, Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public iterator()Ljava/util/Iterator;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 45
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/utils/f;->size()I

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Ljava/util/Collections;->emptySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .line 49
    :goto_0
    return-object v0

    .line 46
    :cond_0
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/utils/f;->size()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    new-instance v1, Lkotlin/reflect/jvm/internal/impl/utils/f$c;

    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/utils/f;->b:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Object;

    invoke-direct {v1, v0}, Lkotlin/reflect/jvm/internal/impl/utils/f$c;-><init>(Ljava/lang/Object;)V

    move-object v0, v1

    check-cast v0, Ljava/util/Iterator;

    goto :goto_0

    .line 47
    :cond_1
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/utils/f;->size()I

    move-result v0

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/utils/f;->a:Lkotlin/reflect/jvm/internal/impl/utils/f$b;

    invoke-static {v1}, Lkotlin/reflect/jvm/internal/impl/utils/f$b;->a(Lkotlin/reflect/jvm/internal/impl/utils/f$b;)I

    move-result v1

    if-ge v0, v1, :cond_3

    new-instance v1, Lkotlin/reflect/jvm/internal/impl/utils/f$a;

    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/utils/f;->b:Ljava/lang/Object;

    if-nez v0, :cond_2

    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type kotlin.Array<T>"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    check-cast v0, [Ljava/lang/Object;

    invoke-direct {v1, v0}, Lkotlin/reflect/jvm/internal/impl/utils/f$a;-><init>([Ljava/lang/Object;)V

    move-object v0, v1

    check-cast v0, Ljava/util/Iterator;

    goto :goto_0

    .line 48
    :cond_3
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/utils/f;->b:Ljava/lang/Object;

    if-nez v0, :cond_4

    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type kotlin.collections.MutableSet<T>"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_4
    invoke-static {v0}, Lkotlin/d/b/ac;->a(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    goto :goto_0
.end method

.method public final size()I
    .locals 1

    .prologue
    .line 28
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/utils/f;->a()I

    move-result v0

    return v0
.end method
