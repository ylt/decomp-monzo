.class public final Lkotlin/reflect/jvm/internal/impl/utils/d;
.super Ljava/lang/Object;
.source "functions.kt"


# static fields
.field private static final a:Lkotlin/d/a/b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/d/a/b",
            "<",
            "Ljava/lang/Object;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private static final b:Lkotlin/d/a/b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/d/a/b",
            "<",
            "Ljava/lang/Object;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private static final c:Lkotlin/d/a/b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/d/a/b",
            "<",
            "Ljava/lang/Object;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private static final d:Lkotlin/d/a/b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/d/a/b",
            "<",
            "Ljava/lang/Object;",
            "Lkotlin/n;",
            ">;"
        }
    .end annotation
.end field

.field private static final e:Lkotlin/d/a/m;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/d/a/m",
            "<",
            "Ljava/lang/Object;",
            "Ljava/lang/Object;",
            "Lkotlin/n;",
            ">;"
        }
    .end annotation
.end field

.field private static final f:Lkotlin/d/a/q;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/d/a/q",
            "<",
            "Ljava/lang/Object;",
            "Ljava/lang/Object;",
            "Ljava/lang/Object;",
            "Lkotlin/n;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 19
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/utils/d$f;->a:Lkotlin/reflect/jvm/internal/impl/utils/d$f;

    check-cast v0, Lkotlin/d/a/b;

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/utils/d;->a:Lkotlin/d/a/b;

    .line 24
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/utils/d$b;->a:Lkotlin/reflect/jvm/internal/impl/utils/d$b;

    check-cast v0, Lkotlin/d/a/b;

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/utils/d;->b:Lkotlin/d/a/b;

    .line 28
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/utils/d$a;->a:Lkotlin/reflect/jvm/internal/impl/utils/d$a;

    check-cast v0, Lkotlin/d/a/b;

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/utils/d;->c:Lkotlin/d/a/b;

    .line 33
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/utils/d$c;->a:Lkotlin/reflect/jvm/internal/impl/utils/d$c;

    check-cast v0, Lkotlin/d/a/b;

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/utils/d;->d:Lkotlin/d/a/b;

    .line 34
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/utils/d$d;->a:Lkotlin/reflect/jvm/internal/impl/utils/d$d;

    check-cast v0, Lkotlin/d/a/m;

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/utils/d;->e:Lkotlin/d/a/m;

    .line 35
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/utils/d$e;->a:Lkotlin/reflect/jvm/internal/impl/utils/d$e;

    check-cast v0, Lkotlin/d/a/q;

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/utils/d;->f:Lkotlin/d/a/q;

    return-void
.end method

.method public static final a()Lkotlin/d/a/b;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">()",
            "Lkotlin/d/a/b",
            "<TT;TT;>;"
        }
    .end annotation

    .prologue
    .line 21
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/utils/d;->a:Lkotlin/d/a/b;

    if-nez v0, :cond_0

    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type (T) -> T"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    const/4 v1, 0x1

    invoke-static {v0, v1}, Lkotlin/d/b/ac;->b(Ljava/lang/Object;I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/d/a/b;

    return-object v0
.end method

.method public static final b()Lkotlin/d/a/b;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">()",
            "Lkotlin/d/a/b",
            "<TT;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .prologue
    .line 26
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/utils/d;->b:Lkotlin/d/a/b;

    return-object v0
.end method

.method public static final c()Lkotlin/d/a/m;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/d/a/m",
            "<",
            "Ljava/lang/Object;",
            "Ljava/lang/Object;",
            "Lkotlin/n;",
            ">;"
        }
    .end annotation

    .prologue
    .line 34
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/utils/d;->e:Lkotlin/d/a/m;

    return-object v0
.end method

.method public static final d()Lkotlin/d/a/q;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/d/a/q",
            "<",
            "Ljava/lang/Object;",
            "Ljava/lang/Object;",
            "Ljava/lang/Object;",
            "Lkotlin/n;",
            ">;"
        }
    .end annotation

    .prologue
    .line 35
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/utils/d;->f:Lkotlin/d/a/q;

    return-object v0
.end method
