.class public final Lkotlin/reflect/jvm/internal/impl/utils/f$b;
.super Ljava/lang/Object;
.source "SmartSet.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lkotlin/reflect/jvm/internal/impl/utils/f;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "b"
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/d/b/i;)V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/utils/f$b;-><init>()V

    return-void
.end method

.method public static final synthetic a(Lkotlin/reflect/jvm/internal/impl/utils/f$b;)I
    .locals 1

    .prologue
    .line 29
    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/utils/f$b;->b()I

    move-result v0

    return v0
.end method

.method private final b()I
    .locals 1

    .prologue
    .line 30
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/utils/f;->b()I

    move-result v0

    return v0
.end method


# virtual methods
.method public final a()Lkotlin/reflect/jvm/internal/impl/utils/f;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">()",
            "Lkotlin/reflect/jvm/internal/impl/utils/f",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 33
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/utils/f;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lkotlin/reflect/jvm/internal/impl/utils/f;-><init>(Lkotlin/d/b/i;)V

    return-object v0
.end method

.method public final a(Ljava/util/Collection;)Lkotlin/reflect/jvm/internal/impl/utils/f;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/Collection",
            "<+TT;>;)",
            "Lkotlin/reflect/jvm/internal/impl/utils/f",
            "<TT;>;"
        }
    .end annotation

    .prologue
    const-string v0, "set"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 36
    new-instance v1, Lkotlin/reflect/jvm/internal/impl/utils/f;

    const/4 v0, 0x0

    invoke-direct {v1, v0}, Lkotlin/reflect/jvm/internal/impl/utils/f;-><init>(Lkotlin/d/b/i;)V

    move-object v0, v1

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/utils/f;

    invoke-virtual {v0, p1}, Lkotlin/reflect/jvm/internal/impl/utils/f;->addAll(Ljava/util/Collection;)Z

    check-cast v1, Lkotlin/reflect/jvm/internal/impl/utils/f;

    return-object v1
.end method
