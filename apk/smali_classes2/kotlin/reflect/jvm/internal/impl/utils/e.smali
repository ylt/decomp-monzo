.class public Lkotlin/reflect/jvm/internal/impl/utils/e;
.super Ljava/util/AbstractList;
.source "SmartList.java"

# interfaces
.implements Ljava/util/RandomAccess;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lkotlin/reflect/jvm/internal/impl/utils/e$1;,
        Lkotlin/reflect/jvm/internal/impl/utils/e$b;,
        Lkotlin/reflect/jvm/internal/impl/utils/e$c;,
        Lkotlin/reflect/jvm/internal/impl/utils/e$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/util/AbstractList",
        "<TE;>;",
        "Ljava/util/RandomAccess;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Ljava/lang/Object;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/util/AbstractList;-><init>()V

    return-void
.end method

.method static synthetic a(Lkotlin/reflect/jvm/internal/impl/utils/e;)I
    .locals 1

    .prologue
    .line 31
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/utils/e;->modCount:I

    return v0
.end method

.method static synthetic b(Lkotlin/reflect/jvm/internal/impl/utils/e;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/utils/e;->b:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic c(Lkotlin/reflect/jvm/internal/impl/utils/e;)I
    .locals 1

    .prologue
    .line 31
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/utils/e;->modCount:I

    return v0
.end method

.method static synthetic d(Lkotlin/reflect/jvm/internal/impl/utils/e;)I
    .locals 1

    .prologue
    .line 31
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/utils/e;->modCount:I

    return v0
.end method


# virtual methods
.method public add(ILjava/lang/Object;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ITE;)V"
        }
    .end annotation

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 109
    if-ltz p1, :cond_0

    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/utils/e;->a:I

    if-le p1, v0, :cond_1

    .line 110
    :cond_0
    new-instance v0, Ljava/lang/IndexOutOfBoundsException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Index: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", Size: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lkotlin/reflect/jvm/internal/impl/utils/e;->a:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 113
    :cond_1
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/utils/e;->a:I

    if-nez v0, :cond_2

    .line 114
    iput-object p2, p0, Lkotlin/reflect/jvm/internal/impl/utils/e;->b:Ljava/lang/Object;

    .line 136
    :goto_0
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/utils/e;->a:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/utils/e;->a:I

    .line 137
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/utils/e;->modCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/utils/e;->modCount:I

    .line 138
    return-void

    .line 116
    :cond_2
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/utils/e;->a:I

    if-ne v0, v3, :cond_3

    if-nez p1, :cond_3

    .line 117
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    .line 118
    aput-object p2, v0, v2

    .line 119
    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/utils/e;->b:Ljava/lang/Object;

    aput-object v1, v0, v3

    .line 120
    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/utils/e;->b:Ljava/lang/Object;

    goto :goto_0

    .line 123
    :cond_3
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/utils/e;->a:I

    add-int/lit8 v0, v0, 0x1

    new-array v1, v0, [Ljava/lang/Object;

    .line 124
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/utils/e;->a:I

    if-ne v0, v3, :cond_4

    .line 125
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/utils/e;->b:Ljava/lang/Object;

    aput-object v0, v1, v2

    .line 132
    :goto_1
    aput-object p2, v1, p1

    .line 133
    iput-object v1, p0, Lkotlin/reflect/jvm/internal/impl/utils/e;->b:Ljava/lang/Object;

    goto :goto_0

    .line 128
    :cond_4
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/utils/e;->b:Ljava/lang/Object;

    check-cast v0, [Ljava/lang/Object;

    check-cast v0, [Ljava/lang/Object;

    .line 129
    invoke-static {v0, v2, v1, v2, p1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 130
    add-int/lit8 v2, p1, 0x1

    iget v3, p0, Lkotlin/reflect/jvm/internal/impl/utils/e;->a:I

    sub-int/2addr v3, p1

    invoke-static {v0, p1, v1, v2, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    goto :goto_1
.end method

.method public add(Ljava/lang/Object;)Z
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;)Z"
        }
    .end annotation

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 76
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/utils/e;->a:I

    if-nez v0, :cond_0

    .line 77
    iput-object p1, p0, Lkotlin/reflect/jvm/internal/impl/utils/e;->b:Ljava/lang/Object;

    .line 102
    :goto_0
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/utils/e;->a:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/utils/e;->a:I

    .line 103
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/utils/e;->modCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/utils/e;->modCount:I

    .line 104
    return v5

    .line 79
    :cond_0
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/utils/e;->a:I

    if-ne v0, v5, :cond_1

    .line 80
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    .line 81
    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/utils/e;->b:Ljava/lang/Object;

    aput-object v1, v0, v4

    .line 82
    aput-object p1, v0, v5

    .line 83
    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/utils/e;->b:Ljava/lang/Object;

    goto :goto_0

    .line 86
    :cond_1
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/utils/e;->b:Ljava/lang/Object;

    check-cast v0, [Ljava/lang/Object;

    check-cast v0, [Ljava/lang/Object;

    .line 87
    array-length v3, v0

    .line 88
    iget v1, p0, Lkotlin/reflect/jvm/internal/impl/utils/e;->a:I

    if-lt v1, v3, :cond_2

    .line 90
    mul-int/lit8 v1, v3, 0x3

    div-int/lit8 v1, v1, 0x2

    add-int/lit8 v2, v1, 0x1

    .line 91
    iget v1, p0, Lkotlin/reflect/jvm/internal/impl/utils/e;->a:I

    add-int/lit8 v1, v1, 0x1

    .line 92
    if-ge v2, v1, :cond_3

    .line 96
    :goto_1
    new-array v1, v1, [Ljava/lang/Object;

    iput-object v1, p0, Lkotlin/reflect/jvm/internal/impl/utils/e;->b:Ljava/lang/Object;

    .line 97
    invoke-static {v0, v4, v1, v4, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    move-object v0, v1

    .line 99
    :cond_2
    iget v1, p0, Lkotlin/reflect/jvm/internal/impl/utils/e;->a:I

    aput-object p1, v0, v1

    goto :goto_0

    :cond_3
    move v1, v2

    goto :goto_1
.end method

.method public clear()V
    .locals 1

    .prologue
    .line 147
    const/4 v0, 0x0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/utils/e;->b:Ljava/lang/Object;

    .line 148
    const/4 v0, 0x0

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/utils/e;->a:I

    .line 149
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/utils/e;->modCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/utils/e;->modCount:I

    .line 150
    return-void
.end method

.method public get(I)Ljava/lang/Object;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)TE;"
        }
    .end annotation

    .prologue
    .line 65
    if-ltz p1, :cond_0

    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/utils/e;->a:I

    if-lt p1, v0, :cond_1

    .line 66
    :cond_0
    new-instance v0, Ljava/lang/IndexOutOfBoundsException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Index: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", Size: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lkotlin/reflect/jvm/internal/impl/utils/e;->a:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 68
    :cond_1
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/utils/e;->a:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_2

    .line 69
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/utils/e;->b:Ljava/lang/Object;

    .line 71
    :goto_0
    return-object v0

    :cond_2
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/utils/e;->b:Ljava/lang/Object;

    check-cast v0, [Ljava/lang/Object;

    check-cast v0, [Ljava/lang/Object;

    aget-object v0, v0, p1

    goto :goto_0
.end method

.method public iterator()Ljava/util/Iterator;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator",
            "<TE;>;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x2

    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 228
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/utils/e;->a:I

    if-nez v0, :cond_0

    .line 229
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/utils/e$a;->a()Lkotlin/reflect/jvm/internal/impl/utils/e$a;

    move-result-object v0

    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "@NotNull method %s.%s must not return null"

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "kotlin/reflect/jvm/internal/impl/utils/SmartList"

    aput-object v3, v2, v5

    const-string v3, "iterator"

    aput-object v3, v2, v4

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 231
    :cond_0
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/utils/e;->a:I

    if-ne v0, v4, :cond_1

    .line 232
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/utils/e$b;

    invoke-direct {v0, p0}, Lkotlin/reflect/jvm/internal/impl/utils/e$b;-><init>(Lkotlin/reflect/jvm/internal/impl/utils/e;)V

    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "@NotNull method %s.%s must not return null"

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "kotlin/reflect/jvm/internal/impl/utils/SmartList"

    aput-object v3, v2, v5

    const-string v3, "iterator"

    aput-object v3, v2, v4

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 234
    :cond_1
    invoke-super {p0}, Ljava/util/AbstractList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "@NotNull method %s.%s must not return null"

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "kotlin/reflect/jvm/internal/impl/utils/SmartList"

    aput-object v3, v2, v5

    const-string v3, "iterator"

    aput-object v3, v2, v4

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    return-object v0
.end method

.method public remove(I)Ljava/lang/Object;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)TE;"
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 173
    if-ltz p1, :cond_0

    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/utils/e;->a:I

    if-lt p1, v0, :cond_1

    .line 174
    :cond_0
    new-instance v0, Ljava/lang/IndexOutOfBoundsException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Index: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", Size: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lkotlin/reflect/jvm/internal/impl/utils/e;->a:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 178
    :cond_1
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/utils/e;->a:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_2

    .line 179
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/utils/e;->b:Ljava/lang/Object;

    .line 180
    iput-object v4, p0, Lkotlin/reflect/jvm/internal/impl/utils/e;->b:Ljava/lang/Object;

    .line 197
    :goto_0
    iget v1, p0, Lkotlin/reflect/jvm/internal/impl/utils/e;->a:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lkotlin/reflect/jvm/internal/impl/utils/e;->a:I

    .line 198
    iget v1, p0, Lkotlin/reflect/jvm/internal/impl/utils/e;->modCount:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lkotlin/reflect/jvm/internal/impl/utils/e;->modCount:I

    .line 199
    return-object v0

    .line 183
    :cond_2
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/utils/e;->b:Ljava/lang/Object;

    check-cast v0, [Ljava/lang/Object;

    check-cast v0, [Ljava/lang/Object;

    .line 184
    aget-object v1, v0, p1

    .line 186
    iget v2, p0, Lkotlin/reflect/jvm/internal/impl/utils/e;->a:I

    const/4 v3, 0x2

    if-ne v2, v3, :cond_3

    .line 187
    rsub-int/lit8 v2, p1, 0x1

    aget-object v0, v0, v2

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/utils/e;->b:Ljava/lang/Object;

    move-object v0, v1

    goto :goto_0

    .line 190
    :cond_3
    iget v2, p0, Lkotlin/reflect/jvm/internal/impl/utils/e;->a:I

    sub-int/2addr v2, p1

    add-int/lit8 v2, v2, -0x1

    .line 191
    if-lez v2, :cond_4

    .line 192
    add-int/lit8 v3, p1, 0x1

    invoke-static {v0, v3, v0, p1, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 194
    :cond_4
    iget v2, p0, Lkotlin/reflect/jvm/internal/impl/utils/e;->a:I

    add-int/lit8 v2, v2, -0x1

    aput-object v4, v0, v2

    move-object v0, v1

    goto :goto_0
.end method

.method public set(ILjava/lang/Object;)Ljava/lang/Object;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ITE;)TE;"
        }
    .end annotation

    .prologue
    .line 154
    if-ltz p1, :cond_0

    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/utils/e;->a:I

    if-lt p1, v0, :cond_1

    .line 155
    :cond_0
    new-instance v0, Ljava/lang/IndexOutOfBoundsException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Index: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", Size: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lkotlin/reflect/jvm/internal/impl/utils/e;->a:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 159
    :cond_1
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/utils/e;->a:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_2

    .line 160
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/utils/e;->b:Ljava/lang/Object;

    .line 161
    iput-object p2, p0, Lkotlin/reflect/jvm/internal/impl/utils/e;->b:Ljava/lang/Object;

    .line 168
    :goto_0
    return-object v0

    .line 164
    :cond_2
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/utils/e;->b:Ljava/lang/Object;

    check-cast v0, [Ljava/lang/Object;

    check-cast v0, [Ljava/lang/Object;

    .line 165
    aget-object v1, v0, p1

    .line 166
    aput-object p2, v0, p1

    move-object v0, v1

    goto :goto_0
.end method

.method public size()I
    .locals 1

    .prologue
    .line 142
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/utils/e;->a:I

    return v0
.end method

.method public sort(Ljava/util/Comparator;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Comparator",
            "<-TE;>;)V"
        }
    .end annotation

    .prologue
    .line 288
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/utils/e;->a:I

    const/4 v1, 0x2

    if-lt v0, v1, :cond_0

    .line 289
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/utils/e;->b:Ljava/lang/Object;

    check-cast v0, [Ljava/lang/Object;

    check-cast v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget v2, p0, Lkotlin/reflect/jvm/internal/impl/utils/e;->a:I

    invoke-static {v0, v1, v2, p1}, Ljava/util/Arrays;->sort([Ljava/lang/Object;IILjava/util/Comparator;)V

    .line 291
    :cond_0
    return-void
.end method

.method public toArray([Ljava/lang/Object;)[Ljava/lang/Object;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">([TT;)[TT;"
        }
    .end annotation

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "a"

    aput-object v3, v2, v4

    const-string v3, "kotlin/reflect/jvm/internal/impl/utils/SmartList"

    aput-object v3, v2, v5

    const-string v3, "toArray"

    aput-object v3, v2, v6

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 300
    :cond_0
    array-length v0, p1

    .line 301
    iget v1, p0, Lkotlin/reflect/jvm/internal/impl/utils/e;->a:I

    if-ne v1, v5, :cond_4

    .line 302
    if-eqz v0, :cond_3

    .line 303
    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/utils/e;->b:Ljava/lang/Object;

    aput-object v1, p1, v4

    .line 319
    :cond_1
    :goto_0
    iget v1, p0, Lkotlin/reflect/jvm/internal/impl/utils/e;->a:I

    if-le v0, v1, :cond_2

    .line 320
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/utils/e;->a:I

    const/4 v1, 0x0

    aput-object v1, p1, v0

    .line 322
    :cond_2
    if-nez p1, :cond_6

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "@NotNull method %s.%s must not return null"

    new-array v2, v6, [Ljava/lang/Object;

    const-string v3, "kotlin/reflect/jvm/internal/impl/utils/SmartList"

    aput-object v3, v2, v4

    const-string v3, "toArray"

    aput-object v3, v2, v5

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 306
    :cond_3
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getComponentType()Ljava/lang/Class;

    move-result-object v0

    invoke-static {v0, v5}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/Object;

    check-cast v0, [Ljava/lang/Object;

    .line 307
    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/utils/e;->b:Ljava/lang/Object;

    aput-object v1, v0, v4

    .line 308
    if-nez v0, :cond_7

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "@NotNull method %s.%s must not return null"

    new-array v2, v6, [Ljava/lang/Object;

    const-string v3, "kotlin/reflect/jvm/internal/impl/utils/SmartList"

    aput-object v3, v2, v4

    const-string v3, "toArray"

    aput-object v3, v2, v5

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 311
    :cond_4
    iget v1, p0, Lkotlin/reflect/jvm/internal/impl/utils/e;->a:I

    if-ge v0, v1, :cond_5

    .line 312
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/utils/e;->b:Ljava/lang/Object;

    check-cast v0, [Ljava/lang/Object;

    check-cast v0, [Ljava/lang/Object;

    iget v1, p0, Lkotlin/reflect/jvm/internal/impl/utils/e;->a:I

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-static {v0, v1, v2}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;ILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/Object;

    if-nez v0, :cond_7

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "@NotNull method %s.%s must not return null"

    new-array v2, v6, [Ljava/lang/Object;

    const-string v3, "kotlin/reflect/jvm/internal/impl/utils/SmartList"

    aput-object v3, v2, v4

    const-string v3, "toArray"

    aput-object v3, v2, v5

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 314
    :cond_5
    iget v1, p0, Lkotlin/reflect/jvm/internal/impl/utils/e;->a:I

    if-eqz v1, :cond_1

    .line 316
    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/utils/e;->b:Ljava/lang/Object;

    iget v2, p0, Lkotlin/reflect/jvm/internal/impl/utils/e;->a:I

    invoke-static {v1, v4, p1, v4, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    goto/16 :goto_0

    :cond_6
    move-object v0, p1

    .line 322
    :cond_7
    return-object v0
.end method
