.class final Lkotlin/reflect/jvm/internal/ag$a;
.super Lkotlin/d/b/m;
.source "ReflectionObjectRenderer.kt"

# interfaces
.implements Lkotlin/d/a/b;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lkotlin/reflect/jvm/internal/ag;->a(Lkotlin/reflect/jvm/internal/impl/b/s;)Ljava/lang/String;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/d/b/m;",
        "Lkotlin/d/a/b",
        "<",
        "Lkotlin/reflect/jvm/internal/impl/b/at;",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x1
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "",
        "it",
        "Lorg/jetbrains/kotlin/descriptors/ValueParameterDescriptor;",
        "kotlin.jvm.PlatformType",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x5
    }
.end annotation


# static fields
.field public static final a:Lkotlin/reflect/jvm/internal/ag$a;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lkotlin/reflect/jvm/internal/ag$a;

    invoke-direct {v0}, Lkotlin/reflect/jvm/internal/ag$a;-><init>()V

    sput-object v0, Lkotlin/reflect/jvm/internal/ag$a;->a:Lkotlin/reflect/jvm/internal/ag$a;

    return-void
.end method

.method constructor <init>()V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lkotlin/d/b/m;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 25
    check-cast p1, Lkotlin/reflect/jvm/internal/impl/b/at;

    invoke-virtual {p0, p1}, Lkotlin/reflect/jvm/internal/ag$a;->a(Lkotlin/reflect/jvm/internal/impl/b/at;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lkotlin/reflect/jvm/internal/impl/b/at;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 74
    sget-object v0, Lkotlin/reflect/jvm/internal/ag;->a:Lkotlin/reflect/jvm/internal/ag;

    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/b/at;->s()Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v1

    const-string v2, "it.type"

    invoke-static {v1, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lkotlin/reflect/jvm/internal/ag;->a(Lkotlin/reflect/jvm/internal/impl/k/r;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
