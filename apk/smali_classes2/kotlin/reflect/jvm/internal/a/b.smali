.class public final Lkotlin/reflect/jvm/internal/a/b;
.super Ljava/lang/Object;
.source "HashPMap.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static final a:Lkotlin/reflect/jvm/internal/a/b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/reflect/jvm/internal/a/b",
            "<",
            "Ljava/lang/Object;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final b:Lkotlin/reflect/jvm/internal/a/d;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/reflect/jvm/internal/a/d",
            "<",
            "Lkotlin/reflect/jvm/internal/a/a",
            "<",
            "Lkotlin/reflect/jvm/internal/a/e",
            "<TK;TV;>;>;>;"
        }
    .end annotation
.end field

.field private final c:I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 26
    new-instance v0, Lkotlin/reflect/jvm/internal/a/b;

    invoke-static {}, Lkotlin/reflect/jvm/internal/a/d;->a()Lkotlin/reflect/jvm/internal/a/d;

    move-result-object v1

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lkotlin/reflect/jvm/internal/a/b;-><init>(Lkotlin/reflect/jvm/internal/a/d;I)V

    sput-object v0, Lkotlin/reflect/jvm/internal/a/b;->a:Lkotlin/reflect/jvm/internal/a/b;

    return-void
.end method

.method private constructor <init>(Lkotlin/reflect/jvm/internal/a/d;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/reflect/jvm/internal/a/d",
            "<",
            "Lkotlin/reflect/jvm/internal/a/a",
            "<",
            "Lkotlin/reflect/jvm/internal/a/e",
            "<TK;TV;>;>;>;I)V"
        }
    .end annotation

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    iput-object p1, p0, Lkotlin/reflect/jvm/internal/a/b;->b:Lkotlin/reflect/jvm/internal/a/d;

    .line 39
    iput p2, p0, Lkotlin/reflect/jvm/internal/a/b;->c:I

    .line 40
    return-void
.end method

.method private static a(Lkotlin/reflect/jvm/internal/a/a;Ljava/lang/Object;)I
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            "V:",
            "Ljava/lang/Object;",
            ">(",
            "Lkotlin/reflect/jvm/internal/a/a",
            "<",
            "Lkotlin/reflect/jvm/internal/a/e",
            "<TK;TV;>;>;",
            "Ljava/lang/Object;",
            ")I"
        }
    .end annotation

    .prologue
    .line 91
    const/4 v1, 0x0

    .line 92
    :goto_0
    if-eqz p0, :cond_1

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/a/a;->b()I

    move-result v0

    if-lez v0, :cond_1

    .line 93
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/a/a;->a:Ljava/lang/Object;

    check-cast v0, Lkotlin/reflect/jvm/internal/a/e;

    .line 94
    iget-object v0, v0, Lkotlin/reflect/jvm/internal/a/e;->a:Ljava/lang/Object;

    invoke-virtual {v0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    .line 99
    :goto_1
    return v0

    .line 96
    :cond_0
    iget-object p0, p0, Lkotlin/reflect/jvm/internal/a/a;->b:Lkotlin/reflect/jvm/internal/a/a;

    .line 97
    add-int/lit8 v1, v1, 0x1

    .line 98
    goto :goto_0

    .line 99
    :cond_1
    const/4 v0, -0x1

    goto :goto_1
.end method

.method private a(I)Lkotlin/reflect/jvm/internal/a/a;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Lkotlin/reflect/jvm/internal/a/a",
            "<",
            "Lkotlin/reflect/jvm/internal/a/e",
            "<TK;TV;>;>;"
        }
    .end annotation

    .prologue
    .line 85
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/a/b;->b:Lkotlin/reflect/jvm/internal/a/d;

    invoke-virtual {v0, p1}, Lkotlin/reflect/jvm/internal/a/d;->a(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/a/a;

    .line 86
    if-nez v0, :cond_0

    invoke-static {}, Lkotlin/reflect/jvm/internal/a/a;->a()Lkotlin/reflect/jvm/internal/a/a;

    move-result-object v0

    .line 87
    :cond_0
    return-object v0
.end method

.method public static a()Lkotlin/reflect/jvm/internal/a/b;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            "V:",
            "Ljava/lang/Object;",
            ">()",
            "Lkotlin/reflect/jvm/internal/a/b",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 31
    sget-object v0, Lkotlin/reflect/jvm/internal/a/b;->a:Lkotlin/reflect/jvm/internal/a/b;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "@NotNull method %s.%s must not return null"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "kotlin/reflect/jvm/internal/pcollections/HashPMap"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "empty"

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    return-object v0
.end method


# virtual methods
.method public a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            ")TV;"
        }
    .end annotation

    .prologue
    .line 51
    invoke-virtual {p1}, Ljava/lang/Object;->hashCode()I

    move-result v0

    invoke-direct {p0, v0}, Lkotlin/reflect/jvm/internal/a/b;->a(I)Lkotlin/reflect/jvm/internal/a/a;

    move-result-object v0

    move-object v1, v0

    .line 52
    :goto_0
    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/a/a;->b()I

    move-result v0

    if-lez v0, :cond_1

    .line 53
    iget-object v0, v1, Lkotlin/reflect/jvm/internal/a/a;->a:Ljava/lang/Object;

    check-cast v0, Lkotlin/reflect/jvm/internal/a/e;

    .line 54
    iget-object v2, v0, Lkotlin/reflect/jvm/internal/a/e;->a:Ljava/lang/Object;

    invoke-virtual {v2, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 55
    iget-object v0, v0, Lkotlin/reflect/jvm/internal/a/e;->b:Ljava/lang/Object;

    .line 58
    :goto_1
    return-object v0

    .line 56
    :cond_0
    iget-object v0, v1, Lkotlin/reflect/jvm/internal/a/a;->b:Lkotlin/reflect/jvm/internal/a/a;

    move-object v1, v0

    .line 57
    goto :goto_0

    .line 58
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public a(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/reflect/jvm/internal/a/b;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;TV;)",
            "Lkotlin/reflect/jvm/internal/a/b",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 63
    invoke-virtual {p1}, Ljava/lang/Object;->hashCode()I

    move-result v0

    invoke-direct {p0, v0}, Lkotlin/reflect/jvm/internal/a/b;->a(I)Lkotlin/reflect/jvm/internal/a/a;

    move-result-object v0

    .line 64
    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/a/a;->b()I

    move-result v1

    .line 65
    invoke-static {v0, p1}, Lkotlin/reflect/jvm/internal/a/b;->a(Lkotlin/reflect/jvm/internal/a/a;Ljava/lang/Object;)I

    move-result v2

    .line 66
    const/4 v3, -0x1

    if-eq v2, v3, :cond_0

    invoke-virtual {v0, v2}, Lkotlin/reflect/jvm/internal/a/a;->b(I)Lkotlin/reflect/jvm/internal/a/a;

    move-result-object v0

    .line 67
    :cond_0
    new-instance v2, Lkotlin/reflect/jvm/internal/a/e;

    invoke-direct {v2, p1, p2}, Lkotlin/reflect/jvm/internal/a/e;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-virtual {v0, v2}, Lkotlin/reflect/jvm/internal/a/a;->a(Ljava/lang/Object;)Lkotlin/reflect/jvm/internal/a/a;

    move-result-object v0

    .line 68
    new-instance v2, Lkotlin/reflect/jvm/internal/a/b;

    iget-object v3, p0, Lkotlin/reflect/jvm/internal/a/b;->b:Lkotlin/reflect/jvm/internal/a/d;

    invoke-virtual {p1}, Ljava/lang/Object;->hashCode()I

    move-result v4

    invoke-virtual {v3, v4, v0}, Lkotlin/reflect/jvm/internal/a/d;->a(ILjava/lang/Object;)Lkotlin/reflect/jvm/internal/a/d;

    move-result-object v3

    iget v4, p0, Lkotlin/reflect/jvm/internal/a/b;->c:I

    sub-int v1, v4, v1

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/a/a;->b()I

    move-result v0

    add-int/2addr v0, v1

    invoke-direct {v2, v3, v0}, Lkotlin/reflect/jvm/internal/a/b;-><init>(Lkotlin/reflect/jvm/internal/a/d;I)V

    if-nez v2, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "@NotNull method %s.%s must not return null"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "kotlin/reflect/jvm/internal/pcollections/HashPMap"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "plus"

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    return-object v2
.end method
