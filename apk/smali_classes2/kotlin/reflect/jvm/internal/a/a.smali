.class final Lkotlin/reflect/jvm/internal/a/a;
.super Ljava/lang/Object;
.source "ConsPStack.java"

# interfaces
.implements Ljava/lang/Iterable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lkotlin/reflect/jvm/internal/a/a$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Ljava/lang/Iterable",
        "<TE;>;"
    }
.end annotation


# static fields
.field private static final c:Lkotlin/reflect/jvm/internal/a/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/reflect/jvm/internal/a/a",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field final a:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TE;"
        }
    .end annotation
.end field

.field final b:Lkotlin/reflect/jvm/internal/a/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/reflect/jvm/internal/a/a",
            "<TE;>;"
        }
    .end annotation
.end field

.field private final d:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 28
    new-instance v0, Lkotlin/reflect/jvm/internal/a/a;

    invoke-direct {v0}, Lkotlin/reflect/jvm/internal/a/a;-><init>()V

    sput-object v0, Lkotlin/reflect/jvm/internal/a/a;->c:Lkotlin/reflect/jvm/internal/a/a;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    const/4 v0, 0x0

    iput v0, p0, Lkotlin/reflect/jvm/internal/a/a;->d:I

    .line 41
    iput-object v1, p0, Lkotlin/reflect/jvm/internal/a/a;->a:Ljava/lang/Object;

    .line 42
    iput-object v1, p0, Lkotlin/reflect/jvm/internal/a/a;->b:Lkotlin/reflect/jvm/internal/a/a;

    .line 43
    return-void
.end method

.method private constructor <init>(Ljava/lang/Object;Lkotlin/reflect/jvm/internal/a/a;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;",
            "Lkotlin/reflect/jvm/internal/a/a",
            "<TE;>;)V"
        }
    .end annotation

    .prologue
    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    iput-object p1, p0, Lkotlin/reflect/jvm/internal/a/a;->a:Ljava/lang/Object;

    .line 47
    iput-object p2, p0, Lkotlin/reflect/jvm/internal/a/a;->b:Lkotlin/reflect/jvm/internal/a/a;

    .line 48
    iget v0, p2, Lkotlin/reflect/jvm/internal/a/a;->d:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lkotlin/reflect/jvm/internal/a/a;->d:I

    .line 49
    return-void
.end method

.method static synthetic a(Lkotlin/reflect/jvm/internal/a/a;)I
    .locals 1

    .prologue
    .line 27
    iget v0, p0, Lkotlin/reflect/jvm/internal/a/a;->d:I

    return v0
.end method

.method public static a()Lkotlin/reflect/jvm/internal/a/a;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E:",
            "Ljava/lang/Object;",
            ">()",
            "Lkotlin/reflect/jvm/internal/a/a",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 32
    sget-object v0, Lkotlin/reflect/jvm/internal/a/a;->c:Lkotlin/reflect/jvm/internal/a/a;

    return-object v0
.end method

.method private b(Ljava/lang/Object;)Lkotlin/reflect/jvm/internal/a/a;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            ")",
            "Lkotlin/reflect/jvm/internal/a/a",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 104
    iget v0, p0, Lkotlin/reflect/jvm/internal/a/a;->d:I

    if-nez v0, :cond_1

    .line 110
    :cond_0
    :goto_0
    return-object p0

    .line 105
    :cond_1
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/a/a;->a:Ljava/lang/Object;

    invoke-virtual {v0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 106
    iget-object p0, p0, Lkotlin/reflect/jvm/internal/a/a;->b:Lkotlin/reflect/jvm/internal/a/a;

    goto :goto_0

    .line 108
    :cond_2
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/a/a;->b:Lkotlin/reflect/jvm/internal/a/a;

    invoke-direct {v0, p1}, Lkotlin/reflect/jvm/internal/a/a;->b(Ljava/lang/Object;)Lkotlin/reflect/jvm/internal/a/a;

    move-result-object v1

    .line 109
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/a/a;->b:Lkotlin/reflect/jvm/internal/a/a;

    if-eq v1, v0, :cond_0

    .line 110
    new-instance v0, Lkotlin/reflect/jvm/internal/a/a;

    iget-object v2, p0, Lkotlin/reflect/jvm/internal/a/a;->a:Ljava/lang/Object;

    invoke-direct {v0, v2, v1}, Lkotlin/reflect/jvm/internal/a/a;-><init>(Ljava/lang/Object;Lkotlin/reflect/jvm/internal/a/a;)V

    move-object p0, v0

    goto :goto_0
.end method

.method private c(I)Ljava/util/Iterator;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/Iterator",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 71
    new-instance v0, Lkotlin/reflect/jvm/internal/a/a$a;

    invoke-direct {p0, p1}, Lkotlin/reflect/jvm/internal/a/a;->d(I)Lkotlin/reflect/jvm/internal/a/a;

    move-result-object v1

    invoke-direct {v0, v1}, Lkotlin/reflect/jvm/internal/a/a$a;-><init>(Lkotlin/reflect/jvm/internal/a/a;)V

    return-object v0
.end method

.method private d(I)Lkotlin/reflect/jvm/internal/a/a;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Lkotlin/reflect/jvm/internal/a/a",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 118
    if-ltz p1, :cond_0

    iget v0, p0, Lkotlin/reflect/jvm/internal/a/a;->d:I

    if-le p1, v0, :cond_1

    .line 119
    :cond_0
    new-instance v0, Ljava/lang/IndexOutOfBoundsException;

    invoke-direct {v0}, Ljava/lang/IndexOutOfBoundsException;-><init>()V

    throw v0

    .line 120
    :cond_1
    if-nez p1, :cond_2

    .line 122
    :goto_0
    return-object p0

    :cond_2
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/a/a;->b:Lkotlin/reflect/jvm/internal/a/a;

    add-int/lit8 v1, p1, -0x1

    invoke-direct {v0, v1}, Lkotlin/reflect/jvm/internal/a/a;->d(I)Lkotlin/reflect/jvm/internal/a/a;

    move-result-object p0

    goto :goto_0
.end method


# virtual methods
.method public a(I)Ljava/lang/Object;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)TE;"
        }
    .end annotation

    .prologue
    .line 52
    if-ltz p1, :cond_0

    iget v0, p0, Lkotlin/reflect/jvm/internal/a/a;->d:I

    if-le p1, v0, :cond_1

    :cond_0
    new-instance v0, Ljava/lang/IndexOutOfBoundsException;

    invoke-direct {v0}, Ljava/lang/IndexOutOfBoundsException;-><init>()V

    throw v0

    .line 55
    :cond_1
    :try_start_0
    invoke-direct {p0, p1}, Lkotlin/reflect/jvm/internal/a/a;->c(I)Ljava/util/Iterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;
    :try_end_0
    .catch Ljava/util/NoSuchElementException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    .line 56
    :catch_0
    move-exception v0

    .line 57
    new-instance v0, Ljava/lang/IndexOutOfBoundsException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Index: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public a(Ljava/lang/Object;)Lkotlin/reflect/jvm/internal/a/a;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;)",
            "Lkotlin/reflect/jvm/internal/a/a",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 100
    new-instance v0, Lkotlin/reflect/jvm/internal/a/a;

    invoke-direct {v0, p1, p0}, Lkotlin/reflect/jvm/internal/a/a;-><init>(Ljava/lang/Object;Lkotlin/reflect/jvm/internal/a/a;)V

    return-object v0
.end method

.method public b()I
    .locals 1

    .prologue
    .line 67
    iget v0, p0, Lkotlin/reflect/jvm/internal/a/a;->d:I

    return v0
.end method

.method public b(I)Lkotlin/reflect/jvm/internal/a/a;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Lkotlin/reflect/jvm/internal/a/a",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 114
    invoke-virtual {p0, p1}, Lkotlin/reflect/jvm/internal/a/a;->a(I)Ljava/lang/Object;

    move-result-object v0

    invoke-direct {p0, v0}, Lkotlin/reflect/jvm/internal/a/a;->b(Ljava/lang/Object;)Lkotlin/reflect/jvm/internal/a/a;

    move-result-object v0

    return-object v0
.end method

.method public iterator()Ljava/util/Iterator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 63
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lkotlin/reflect/jvm/internal/a/a;->c(I)Ljava/util/Iterator;

    move-result-object v0

    return-object v0
.end method
