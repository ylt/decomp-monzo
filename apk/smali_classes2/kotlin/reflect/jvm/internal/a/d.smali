.class final Lkotlin/reflect/jvm/internal/a/d;
.super Ljava/lang/Object;
.source "IntTreePMap.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static final a:Lkotlin/reflect/jvm/internal/a/d;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/reflect/jvm/internal/a/d",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final b:Lkotlin/reflect/jvm/internal/a/c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/reflect/jvm/internal/a/c",
            "<TV;>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 23
    new-instance v0, Lkotlin/reflect/jvm/internal/a/d;

    sget-object v1, Lkotlin/reflect/jvm/internal/a/c;->a:Lkotlin/reflect/jvm/internal/a/c;

    invoke-direct {v0, v1}, Lkotlin/reflect/jvm/internal/a/d;-><init>(Lkotlin/reflect/jvm/internal/a/c;)V

    sput-object v0, Lkotlin/reflect/jvm/internal/a/d;->a:Lkotlin/reflect/jvm/internal/a/d;

    return-void
.end method

.method private constructor <init>(Lkotlin/reflect/jvm/internal/a/c;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/reflect/jvm/internal/a/c",
            "<TV;>;)V"
        }
    .end annotation

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    iput-object p1, p0, Lkotlin/reflect/jvm/internal/a/d;->b:Lkotlin/reflect/jvm/internal/a/c;

    .line 34
    return-void
.end method

.method public static a()Lkotlin/reflect/jvm/internal/a/d;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<V:",
            "Ljava/lang/Object;",
            ">()",
            "Lkotlin/reflect/jvm/internal/a/d",
            "<TV;>;"
        }
    .end annotation

    .prologue
    .line 27
    sget-object v0, Lkotlin/reflect/jvm/internal/a/d;->a:Lkotlin/reflect/jvm/internal/a/d;

    return-object v0
.end method

.method private a(Lkotlin/reflect/jvm/internal/a/c;)Lkotlin/reflect/jvm/internal/a/d;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/reflect/jvm/internal/a/c",
            "<TV;>;)",
            "Lkotlin/reflect/jvm/internal/a/d",
            "<TV;>;"
        }
    .end annotation

    .prologue
    .line 37
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/a/d;->b:Lkotlin/reflect/jvm/internal/a/c;

    if-ne p1, v0, :cond_0

    .line 38
    :goto_0
    return-object p0

    :cond_0
    new-instance p0, Lkotlin/reflect/jvm/internal/a/d;

    invoke-direct {p0, p1}, Lkotlin/reflect/jvm/internal/a/d;-><init>(Lkotlin/reflect/jvm/internal/a/c;)V

    goto :goto_0
.end method


# virtual methods
.method public a(I)Ljava/lang/Object;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)TV;"
        }
    .end annotation

    .prologue
    .line 42
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/a/d;->b:Lkotlin/reflect/jvm/internal/a/c;

    int-to-long v2, p1

    invoke-virtual {v0, v2, v3}, Lkotlin/reflect/jvm/internal/a/c;->a(J)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public a(ILjava/lang/Object;)Lkotlin/reflect/jvm/internal/a/d;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ITV;)",
            "Lkotlin/reflect/jvm/internal/a/d",
            "<TV;>;"
        }
    .end annotation

    .prologue
    .line 46
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/a/d;->b:Lkotlin/reflect/jvm/internal/a/c;

    int-to-long v2, p1

    invoke-virtual {v0, v2, v3, p2}, Lkotlin/reflect/jvm/internal/a/c;->a(JLjava/lang/Object;)Lkotlin/reflect/jvm/internal/a/c;

    move-result-object v0

    invoke-direct {p0, v0}, Lkotlin/reflect/jvm/internal/a/d;->a(Lkotlin/reflect/jvm/internal/a/c;)Lkotlin/reflect/jvm/internal/a/d;

    move-result-object v0

    return-object v0
.end method
