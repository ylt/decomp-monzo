.class public abstract Lkotlin/reflect/jvm/internal/f;
.super Ljava/lang/Object;
.source "FunctionCaller.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lkotlin/reflect/jvm/internal/f$n;,
        Lkotlin/reflect/jvm/internal/f$c;,
        Lkotlin/reflect/jvm/internal/f$w;,
        Lkotlin/reflect/jvm/internal/f$z;,
        Lkotlin/reflect/jvm/internal/f$s;,
        Lkotlin/reflect/jvm/internal/f$t;,
        Lkotlin/reflect/jvm/internal/f$j;,
        Lkotlin/reflect/jvm/internal/f$f;,
        Lkotlin/reflect/jvm/internal/f$g;,
        Lkotlin/reflect/jvm/internal/f$o;,
        Lkotlin/reflect/jvm/internal/f$p;,
        Lkotlin/reflect/jvm/internal/f$x;,
        Lkotlin/reflect/jvm/internal/f$q;,
        Lkotlin/reflect/jvm/internal/f$u;,
        Lkotlin/reflect/jvm/internal/f$k;,
        Lkotlin/reflect/jvm/internal/f$d;,
        Lkotlin/reflect/jvm/internal/f$h;,
        Lkotlin/reflect/jvm/internal/f$a;,
        Lkotlin/reflect/jvm/internal/f$y;,
        Lkotlin/reflect/jvm/internal/f$r;,
        Lkotlin/reflect/jvm/internal/f$v;,
        Lkotlin/reflect/jvm/internal/f$l;,
        Lkotlin/reflect/jvm/internal/f$e;,
        Lkotlin/reflect/jvm/internal/f$i;,
        Lkotlin/reflect/jvm/internal/f$b;,
        Lkotlin/reflect/jvm/internal/f$m;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<M::",
        "Ljava/lang/reflect/Member;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x1
    }
    d1 = {
        "\u0000>\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0011\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0002\u0008\u0008\n\u0002\u0010 \n\u0002\u0008\u0008\n\u0002\u0010\u0002\n\u0002\u0008\u001e\u0008 \u0018\u0000 /*\u000c\u0008\u0000\u0010\u0001 \u0001*\u0004\u0018\u00010\u00022\u00020\u0003:\u001a#$%&\'()*+,-./0123456789:;<B1\u0012\u0006\u0010\u0004\u001a\u00028\u0000\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u000c\u0010\u0007\u001a\u0008\u0012\u0002\u0008\u0003\u0018\u00010\u0008\u0012\u000c\u0010\t\u001a\u0008\u0012\u0004\u0012\u00020\u00060\n\u00a2\u0006\u0002\u0010\u000bJ\u001b\u0010\u001b\u001a\u0004\u0018\u00010\u00032\n\u0010\u001c\u001a\u0006\u0012\u0002\u0008\u00030\nH&\u00a2\u0006\u0002\u0010\u001dJ\u0019\u0010\u001e\u001a\u00020\u001f2\n\u0010\u001c\u001a\u0006\u0012\u0002\u0008\u00030\nH\u0014\u00a2\u0006\u0002\u0010 J\u0012\u0010!\u001a\u00020\u001f2\u0008\u0010\"\u001a\u0004\u0018\u00010\u0003H\u0004R\u0011\u0010\u000c\u001a\u00020\r8F\u00a2\u0006\u0006\u001a\u0004\u0008\u000e\u0010\u000fR\u001a\u0010\u0007\u001a\u0008\u0012\u0002\u0008\u0003\u0018\u00010\u0008X\u0080\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0010\u0010\u0011R\u0016\u0010\u0004\u001a\u00028\u0000X\u0080\u0004\u00a2\u0006\n\n\u0002\u0010\u0014\u001a\u0004\u0008\u0012\u0010\u0013R\u0017\u0010\u0015\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0016\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0017\u0010\u0018R\u0014\u0010\u0005\u001a\u00020\u0006X\u0080\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0019\u0010\u001a\u00a8\u0006="
    }
    d2 = {
        "Lkotlin/reflect/jvm/internal/FunctionCaller;",
        "M",
        "Ljava/lang/reflect/Member;",
        "",
        "member",
        "returnType",
        "Ljava/lang/reflect/Type;",
        "instanceClass",
        "Ljava/lang/Class;",
        "valueParameterTypes",
        "",
        "(Ljava/lang/reflect/Member;Ljava/lang/reflect/Type;Ljava/lang/Class;[Ljava/lang/reflect/Type;)V",
        "arity",
        "",
        "getArity",
        "()I",
        "getInstanceClass$kotlin_reflection",
        "()Ljava/lang/Class;",
        "getMember$kotlin_reflection",
        "()Ljava/lang/reflect/Member;",
        "Ljava/lang/reflect/Member;",
        "parameterTypes",
        "",
        "getParameterTypes",
        "()Ljava/util/List;",
        "getReturnType$kotlin_reflection",
        "()Ljava/lang/reflect/Type;",
        "call",
        "args",
        "([Ljava/lang/Object;)Ljava/lang/Object;",
        "checkArguments",
        "",
        "([Ljava/lang/Object;)V",
        "checkObjectInstance",
        "obj",
        "BoundClassCompanionFieldGetter",
        "BoundClassCompanionFieldSetter",
        "BoundConstructor",
        "BoundInstanceFieldGetter",
        "BoundInstanceFieldSetter",
        "BoundInstanceMethod",
        "BoundJvmStaticInObject",
        "BoundJvmStaticInObjectFieldGetter",
        "BoundJvmStaticInObjectFieldSetter",
        "BoundStaticMethod",
        "ClassCompanionFieldGetter",
        "ClassCompanionFieldSetter",
        "Companion",
        "Constructor",
        "FieldGetter",
        "FieldSetter",
        "InstanceFieldGetter",
        "InstanceFieldSetter",
        "InstanceMethod",
        "JvmStaticInObject",
        "JvmStaticInObjectFieldGetter",
        "JvmStaticInObjectFieldSetter",
        "Method",
        "StaticFieldGetter",
        "StaticFieldSetter",
        "StaticMethod",
        "kotlin-reflection"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x5
    }
.end annotation


# static fields
.field public static final a:Lkotlin/reflect/jvm/internal/f$m;


# instance fields
.field private final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/reflect/Type;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Ljava/lang/reflect/Member;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TM;"
        }
    .end annotation
.end field

.field private final d:Ljava/lang/reflect/Type;

.field private final e:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lkotlin/reflect/jvm/internal/f$m;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lkotlin/reflect/jvm/internal/f$m;-><init>(Lkotlin/d/b/i;)V

    sput-object v0, Lkotlin/reflect/jvm/internal/f;->a:Lkotlin/reflect/jvm/internal/f$m;

    return-void
.end method

.method public constructor <init>(Ljava/lang/reflect/Member;Ljava/lang/reflect/Type;Ljava/lang/Class;[Ljava/lang/reflect/Type;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TM;",
            "Ljava/lang/reflect/Type;",
            "Ljava/lang/Class",
            "<*>;[",
            "Ljava/lang/reflect/Type;",
            ")V"
        }
    .end annotation

    .prologue
    const-string v0, "returnType"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "valueParameterTypes"

    invoke-static {p4, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lkotlin/reflect/jvm/internal/f;->c:Ljava/lang/reflect/Member;

    iput-object p2, p0, Lkotlin/reflect/jvm/internal/f;->d:Ljava/lang/reflect/Type;

    iput-object p3, p0, Lkotlin/reflect/jvm/internal/f;->e:Ljava/lang/Class;

    .line 33
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/f;->e:Ljava/lang/Class;

    if-eqz v0, :cond_0

    check-cast v0, Ljava/lang/Class;

    new-instance v1, Lkotlin/d/b/aa;

    const/4 v2, 0x2

    invoke-direct {v1, v2}, Lkotlin/d/b/aa;-><init>(I)V

    check-cast v0, Ljava/lang/reflect/Type;

    invoke-virtual {v1, v0}, Lkotlin/d/b/aa;->b(Ljava/lang/Object;)V

    invoke-virtual {v1, p4}, Lkotlin/d/b/aa;->a(Ljava/lang/Object;)V

    invoke-virtual {v1}, Lkotlin/d/b/aa;->a()I

    move-result v0

    new-array v0, v0, [Ljava/lang/reflect/Type;

    invoke-virtual {v1, v0}, Lkotlin/d/b/aa;->a([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/reflect/Type;

    invoke-static {v0}, Lkotlin/a/m;->b([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    if-eqz v0, :cond_0

    .line 34
    :goto_0
    iput-object v0, p0, Lkotlin/reflect/jvm/internal/f;->b:Ljava/util/List;

    return-void

    :cond_0
    check-cast p4, [Ljava/lang/Object;

    invoke-static {p4}, Lkotlin/a/g;->h([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public abstract a([Ljava/lang/Object;)Ljava/lang/Object;
.end method

.method public final a()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/reflect/Type;",
            ">;"
        }
    .end annotation

    .prologue
    .line 32
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/f;->b:Ljava/util/List;

    return-object v0
.end method

.method protected final a(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 48
    if-eqz p1, :cond_1

    iget-object v0, p0, Lkotlin/reflect/jvm/internal/f;->c:Ljava/lang/reflect/Member;

    if-nez v0, :cond_0

    invoke-static {}, Lkotlin/d/b/l;->a()V

    :cond_0
    invoke-interface {v0}, Ljava/lang/reflect/Member;->getDeclaringClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/Class;->isInstance(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 49
    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "An object member requires the object instance passed as the first argument."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    .line 51
    :cond_2
    return-void
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/f;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method protected b([Ljava/lang/Object;)V
    .locals 3

    .prologue
    const-string v0, "args"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 42
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/f;->b()I

    move-result v0

    array-length v1, p1

    if-eq v0, v1, :cond_0

    .line 43
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Callable expects "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/f;->b()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " arguments, but "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    array-length v2, p1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " were provided."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    .line 45
    :cond_0
    return-void
.end method

.method public final c()Ljava/lang/reflect/Member;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TM;"
        }
    .end annotation

    .prologue
    .line 27
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/f;->c:Ljava/lang/reflect/Member;

    return-object v0
.end method

.method public final d()Ljava/lang/reflect/Type;
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/f;->d:Ljava/lang/reflect/Type;

    return-object v0
.end method

.method public final e()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 29
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/f;->e:Ljava/lang/Class;

    return-object v0
.end method
