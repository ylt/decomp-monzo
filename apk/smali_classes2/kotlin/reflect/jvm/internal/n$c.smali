.class public final enum Lkotlin/reflect/jvm/internal/n$c;
.super Ljava/lang/Enum;
.source "KDeclarationContainerImpl.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lkotlin/reflect/jvm/internal/n;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401c
    name = "c"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lkotlin/reflect/jvm/internal/n$c;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x1
    }
    d1 = {
        "\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0008\u0084\u0001\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00000\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u000e\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006j\u0002\u0008\u0007j\u0002\u0008\u0008\u00a8\u0006\t"
    }
    d2 = {
        "Lkotlin/reflect/jvm/internal/KDeclarationContainerImpl$MemberBelonginess;",
        "",
        "(Ljava/lang/String;I)V",
        "accept",
        "",
        "member",
        "Lorg/jetbrains/kotlin/descriptors/CallableMemberDescriptor;",
        "DECLARED",
        "INHERITED",
        "kotlin-reflection"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x5
    }
.end annotation


# static fields
.field public static final enum a:Lkotlin/reflect/jvm/internal/n$c;

.field public static final enum b:Lkotlin/reflect/jvm/internal/n$c;

.field private static final synthetic c:[Lkotlin/reflect/jvm/internal/n$c;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/4 v4, 0x1

    const/4 v3, 0x0

    const/4 v0, 0x2

    new-array v0, v0, [Lkotlin/reflect/jvm/internal/n$c;

    new-instance v1, Lkotlin/reflect/jvm/internal/n$c;

    const-string v2, "DECLARED"

    invoke-direct {v1, v2, v3}, Lkotlin/reflect/jvm/internal/n$c;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lkotlin/reflect/jvm/internal/n$c;->a:Lkotlin/reflect/jvm/internal/n$c;

    aput-object v1, v0, v3

    new-instance v1, Lkotlin/reflect/jvm/internal/n$c;

    const-string v2, "INHERITED"

    invoke-direct {v1, v2, v4}, Lkotlin/reflect/jvm/internal/n$c;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lkotlin/reflect/jvm/internal/n$c;->b:Lkotlin/reflect/jvm/internal/n$c;

    aput-object v1, v0, v4

    sput-object v0, Lkotlin/reflect/jvm/internal/n$c;->c:[Lkotlin/reflect/jvm/internal/n$c;

    return-void
.end method

.method protected constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 71
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lkotlin/reflect/jvm/internal/n$c;
    .locals 1

    const-class v0, Lkotlin/reflect/jvm/internal/n$c;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/n$c;

    return-object v0
.end method

.method public static values()[Lkotlin/reflect/jvm/internal/n$c;
    .locals 1

    sget-object v0, Lkotlin/reflect/jvm/internal/n$c;->c:[Lkotlin/reflect/jvm/internal/n$c;

    invoke-virtual {v0}, [Lkotlin/reflect/jvm/internal/n$c;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lkotlin/reflect/jvm/internal/n$c;

    return-object v0
.end method


# virtual methods
.method public final a(Lkotlin/reflect/jvm/internal/impl/b/b;)Z
    .locals 2

    .prologue
    const-string v0, "member"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 76
    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/b/b;->n()Lkotlin/reflect/jvm/internal/impl/b/b$a;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/b/b$a;->a()Z

    move-result v0

    check-cast p0, Lkotlin/reflect/jvm/internal/n$c;

    sget-object v1, Lkotlin/reflect/jvm/internal/n$c;->a:Lkotlin/reflect/jvm/internal/n$c;

    invoke-static {p0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
