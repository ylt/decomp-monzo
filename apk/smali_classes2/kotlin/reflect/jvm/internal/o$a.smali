.class final Lkotlin/reflect/jvm/internal/o$a;
.super Lkotlin/d/b/m;
.source "KFunctionImpl.kt"

# interfaces
.implements Lkotlin/d/a/a;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lkotlin/reflect/jvm/internal/o;-><init>(Lkotlin/reflect/jvm/internal/n;Ljava/lang/String;Ljava/lang/String;Lkotlin/reflect/jvm/internal/impl/b/s;Ljava/lang/Object;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/d/b/m;",
        "Lkotlin/d/a/a",
        "<",
        "Lkotlin/reflect/jvm/internal/f",
        "<+",
        "Ljava/lang/reflect/Member;",
        ">;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x1
    }
    d1 = {
        "\u0000\u000c\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\u00020\u0001H\n\u00a2\u0006\u0002\u0008\u0003"
    }
    d2 = {
        "<anonymous>",
        "Lkotlin/reflect/jvm/internal/FunctionCaller;",
        "Ljava/lang/reflect/Member;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x5
    }
.end annotation


# instance fields
.field final synthetic a:Lkotlin/reflect/jvm/internal/o;


# direct methods
.method constructor <init>(Lkotlin/reflect/jvm/internal/o;)V
    .locals 1

    iput-object p1, p0, Lkotlin/reflect/jvm/internal/o$a;->a:Lkotlin/reflect/jvm/internal/o;

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lkotlin/d/b/m;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final b()Lkotlin/reflect/jvm/internal/f;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/reflect/jvm/internal/f",
            "<",
            "Ljava/lang/reflect/Member;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    const/16 v4, 0xa

    .line 68
    sget-object v0, Lkotlin/reflect/jvm/internal/ai;->a:Lkotlin/reflect/jvm/internal/ai;

    iget-object v1, p0, Lkotlin/reflect/jvm/internal/o$a;->a:Lkotlin/reflect/jvm/internal/o;

    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/o;->c()Lkotlin/reflect/jvm/internal/impl/b/s;

    move-result-object v1

    invoke-virtual {v0, v1}, Lkotlin/reflect/jvm/internal/ai;->a(Lkotlin/reflect/jvm/internal/impl/b/s;)Lkotlin/reflect/jvm/internal/h;

    move-result-object v0

    .line 70
    instance-of v1, v0, Lkotlin/reflect/jvm/internal/h$e;

    if-eqz v1, :cond_3

    .line 71
    iget-object v1, p0, Lkotlin/reflect/jvm/internal/o$a;->a:Lkotlin/reflect/jvm/internal/o;

    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/o;->q()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 72
    new-instance v0, Lkotlin/reflect/jvm/internal/a;

    iget-object v1, p0, Lkotlin/reflect/jvm/internal/o$a;->a:Lkotlin/reflect/jvm/internal/o;

    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/o;->o()Lkotlin/reflect/jvm/internal/n;

    move-result-object v1

    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/n;->a()Ljava/lang/Class;

    move-result-object v1

    iget-object v2, p0, Lkotlin/reflect/jvm/internal/o$a;->a:Lkotlin/reflect/jvm/internal/o;

    invoke-virtual {v2}, Lkotlin/reflect/jvm/internal/o;->h()Ljava/util/List;

    move-result-object v2

    check-cast v2, Ljava/lang/Iterable;

    .line 181
    new-instance v3, Ljava/util/ArrayList;

    invoke-static {v2, v4}, Lkotlin/a/m;->a(Ljava/lang/Iterable;I)I

    move-result v4

    invoke-direct {v3, v4}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v3, Ljava/util/Collection;

    .line 182
    invoke-interface {v2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 183
    check-cast v2, Lkotlin/reflect/k;

    .line 72
    invoke-interface {v2}, Lkotlin/reflect/k;->b()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_0

    invoke-static {}, Lkotlin/d/b/l;->a()V

    :cond_0
    invoke-interface {v3, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    move-object v2, v3

    .line 184
    check-cast v2, Ljava/util/List;

    .line 72
    sget-object v3, Lkotlin/reflect/jvm/internal/a$a;->b:Lkotlin/reflect/jvm/internal/a$a;

    sget-object v4, Lkotlin/reflect/jvm/internal/a$b;->b:Lkotlin/reflect/jvm/internal/a$b;

    const/16 v6, 0x10

    move-object v7, v5

    invoke-direct/range {v0 .. v7}, Lkotlin/reflect/jvm/internal/a;-><init>(Ljava/lang/Class;Ljava/util/List;Lkotlin/reflect/jvm/internal/a$a;Lkotlin/reflect/jvm/internal/a$b;Ljava/util/List;ILkotlin/d/b/i;)V

    check-cast v0, Lkotlin/reflect/jvm/internal/f;

    .line 97
    :goto_1
    return-object v0

    .line 73
    :cond_2
    iget-object v1, p0, Lkotlin/reflect/jvm/internal/o$a;->a:Lkotlin/reflect/jvm/internal/o;

    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/o;->o()Lkotlin/reflect/jvm/internal/n;

    move-result-object v1

    check-cast v0, Lkotlin/reflect/jvm/internal/h$e;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/h$e;->b()Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lkotlin/reflect/jvm/internal/o$a;->a:Lkotlin/reflect/jvm/internal/o;

    invoke-static {v2}, Lkotlin/reflect/jvm/internal/o;->b(Lkotlin/reflect/jvm/internal/o;)Z

    move-result v2

    invoke-virtual {v1, v0, v2}, Lkotlin/reflect/jvm/internal/n;->a(Ljava/lang/String;Z)Ljava/lang/reflect/Constructor;

    move-result-object v0

    check-cast v0, Ljava/lang/reflect/Member;

    .line 86
    :goto_2
    instance-of v1, v0, Ljava/lang/reflect/Constructor;

    if-eqz v1, :cond_a

    .line 87
    iget-object v1, p0, Lkotlin/reflect/jvm/internal/o$a;->a:Lkotlin/reflect/jvm/internal/o;

    check-cast v0, Ljava/lang/reflect/Constructor;

    invoke-static {v1, v0}, Lkotlin/reflect/jvm/internal/o;->a(Lkotlin/reflect/jvm/internal/o;Ljava/lang/reflect/Constructor;)Lkotlin/reflect/jvm/internal/f;

    move-result-object v0

    goto :goto_1

    .line 75
    :cond_3
    instance-of v1, v0, Lkotlin/reflect/jvm/internal/h$f;

    if-eqz v1, :cond_4

    iget-object v1, p0, Lkotlin/reflect/jvm/internal/o$a;->a:Lkotlin/reflect/jvm/internal/o;

    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/o;->o()Lkotlin/reflect/jvm/internal/n;

    move-result-object v2

    move-object v1, v0

    check-cast v1, Lkotlin/reflect/jvm/internal/h$f;

    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/h$f;->b()Ljava/lang/String;

    move-result-object v1

    check-cast v0, Lkotlin/reflect/jvm/internal/h$f;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/h$f;->c()Ljava/lang/String;

    move-result-object v0

    iget-object v3, p0, Lkotlin/reflect/jvm/internal/o$a;->a:Lkotlin/reflect/jvm/internal/o;

    invoke-static {v3}, Lkotlin/reflect/jvm/internal/o;->b(Lkotlin/reflect/jvm/internal/o;)Z

    move-result v3

    invoke-virtual {v2, v1, v0, v3}, Lkotlin/reflect/jvm/internal/n;->a(Ljava/lang/String;Ljava/lang/String;Z)Ljava/lang/reflect/Method;

    move-result-object v0

    check-cast v0, Ljava/lang/reflect/Member;

    goto :goto_2

    .line 76
    :cond_4
    instance-of v1, v0, Lkotlin/reflect/jvm/internal/h$d;

    if-eqz v1, :cond_5

    check-cast v0, Lkotlin/reflect/jvm/internal/h$d;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/h$d;->b()Ljava/lang/reflect/Method;

    move-result-object v0

    check-cast v0, Ljava/lang/reflect/Member;

    goto :goto_2

    .line 77
    :cond_5
    instance-of v1, v0, Lkotlin/reflect/jvm/internal/h$c;

    if-eqz v1, :cond_6

    check-cast v0, Lkotlin/reflect/jvm/internal/h$c;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/h$c;->b()Ljava/lang/reflect/Constructor;

    move-result-object v0

    check-cast v0, Ljava/lang/reflect/Member;

    goto :goto_2

    .line 78
    :cond_6
    instance-of v1, v0, Lkotlin/reflect/jvm/internal/h$b;

    if-eqz v1, :cond_8

    .line 79
    check-cast v0, Lkotlin/reflect/jvm/internal/h$b;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/h$b;->b()Ljava/util/List;

    move-result-object v5

    .line 80
    new-instance v0, Lkotlin/reflect/jvm/internal/a;

    iget-object v1, p0, Lkotlin/reflect/jvm/internal/o$a;->a:Lkotlin/reflect/jvm/internal/o;

    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/o;->o()Lkotlin/reflect/jvm/internal/n;

    move-result-object v1

    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/n;->a()Ljava/lang/Class;

    move-result-object v1

    move-object v2, v5

    check-cast v2, Ljava/lang/Iterable;

    .line 185
    new-instance v3, Ljava/util/ArrayList;

    invoke-static {v2, v4}, Lkotlin/a/m;->a(Ljava/lang/Iterable;I)I

    move-result v4

    invoke-direct {v3, v4}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v3, Ljava/util/Collection;

    .line 186
    invoke-interface {v2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_3
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_7

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 187
    check-cast v2, Ljava/lang/reflect/Method;

    .line 80
    invoke-virtual {v2}, Ljava/lang/reflect/Method;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v3, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_3

    :cond_7
    move-object v2, v3

    .line 188
    check-cast v2, Ljava/util/List;

    .line 80
    sget-object v3, Lkotlin/reflect/jvm/internal/a$a;->b:Lkotlin/reflect/jvm/internal/a$a;

    sget-object v4, Lkotlin/reflect/jvm/internal/a$b;->a:Lkotlin/reflect/jvm/internal/a$b;

    invoke-direct/range {v0 .. v5}, Lkotlin/reflect/jvm/internal/a;-><init>(Ljava/lang/Class;Ljava/util/List;Lkotlin/reflect/jvm/internal/a$a;Lkotlin/reflect/jvm/internal/a$b;Ljava/util/List;)V

    check-cast v0, Lkotlin/reflect/jvm/internal/f;

    goto/16 :goto_1

    .line 82
    :cond_8
    instance-of v1, v0, Lkotlin/reflect/jvm/internal/h$a;

    if-eqz v1, :cond_9

    check-cast v0, Lkotlin/reflect/jvm/internal/h$a;

    iget-object v1, p0, Lkotlin/reflect/jvm/internal/o$a;->a:Lkotlin/reflect/jvm/internal/o;

    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/o;->o()Lkotlin/reflect/jvm/internal/n;

    move-result-object v1

    invoke-virtual {v0, v1}, Lkotlin/reflect/jvm/internal/h$a;->a(Lkotlin/reflect/jvm/internal/n;)Ljava/lang/reflect/Member;

    move-result-object v0

    goto/16 :goto_2

    :cond_9
    new-instance v0, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {v0}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw v0

    .line 88
    :cond_a
    instance-of v1, v0, Ljava/lang/reflect/Method;

    if-eqz v1, :cond_d

    move-object v1, v0

    .line 89
    check-cast v1, Ljava/lang/reflect/Method;

    invoke-virtual {v1}, Ljava/lang/reflect/Method;->getModifiers()I

    move-result v1

    invoke-static {v1}, Ljava/lang/reflect/Modifier;->isStatic(I)Z

    move-result v1

    if-nez v1, :cond_b

    .line 90
    iget-object v1, p0, Lkotlin/reflect/jvm/internal/o$a;->a:Lkotlin/reflect/jvm/internal/o;

    check-cast v0, Ljava/lang/reflect/Method;

    invoke-static {v1, v0}, Lkotlin/reflect/jvm/internal/o;->a(Lkotlin/reflect/jvm/internal/o;Ljava/lang/reflect/Method;)Lkotlin/reflect/jvm/internal/f$w;

    move-result-object v0

    .line 88
    :goto_4
    check-cast v0, Lkotlin/reflect/jvm/internal/f;

    goto/16 :goto_1

    .line 91
    :cond_b
    iget-object v1, p0, Lkotlin/reflect/jvm/internal/o$a;->a:Lkotlin/reflect/jvm/internal/o;

    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/o;->c()Lkotlin/reflect/jvm/internal/impl/b/s;

    move-result-object v1

    invoke-interface {v1}, Lkotlin/reflect/jvm/internal/impl/b/s;->w()Lkotlin/reflect/jvm/internal/impl/b/a/h;

    move-result-object v1

    invoke-static {}, Lkotlin/reflect/jvm/internal/ak;->a()Lkotlin/reflect/jvm/internal/impl/e/b;

    move-result-object v2

    invoke-interface {v1, v2}, Lkotlin/reflect/jvm/internal/impl/b/a/h;->a(Lkotlin/reflect/jvm/internal/impl/e/b;)Lkotlin/reflect/jvm/internal/impl/b/a/c;

    move-result-object v1

    if-eqz v1, :cond_c

    .line 92
    iget-object v1, p0, Lkotlin/reflect/jvm/internal/o$a;->a:Lkotlin/reflect/jvm/internal/o;

    check-cast v0, Ljava/lang/reflect/Method;

    invoke-static {v1, v0}, Lkotlin/reflect/jvm/internal/o;->b(Lkotlin/reflect/jvm/internal/o;Ljava/lang/reflect/Method;)Lkotlin/reflect/jvm/internal/f$w;

    move-result-object v0

    goto :goto_4

    .line 94
    :cond_c
    iget-object v1, p0, Lkotlin/reflect/jvm/internal/o$a;->a:Lkotlin/reflect/jvm/internal/o;

    check-cast v0, Ljava/lang/reflect/Method;

    invoke-static {v1, v0}, Lkotlin/reflect/jvm/internal/o;->c(Lkotlin/reflect/jvm/internal/o;Ljava/lang/reflect/Method;)Lkotlin/reflect/jvm/internal/f$w;

    move-result-object v0

    goto :goto_4

    .line 96
    :cond_d
    new-instance v1, Lkotlin/reflect/jvm/internal/ac;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Call is not yet supported for this function: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lkotlin/reflect/jvm/internal/o$a;->a:Lkotlin/reflect/jvm/internal/o;

    invoke-virtual {v3}, Lkotlin/reflect/jvm/internal/o;->c()Lkotlin/reflect/jvm/internal/impl/b/s;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " (member = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ")"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lkotlin/reflect/jvm/internal/ac;-><init>(Ljava/lang/String;)V

    move-object v0, v1

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method

.method public synthetic v_()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 36
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/o$a;->b()Lkotlin/reflect/jvm/internal/f;

    move-result-object v0

    return-object v0
.end method
