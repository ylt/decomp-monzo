.class public final Lkotlin/reflect/jvm/internal/n$e;
.super Lkotlin/reflect/jvm/internal/impl/b/b/l;
.source "KDeclarationContainerImpl.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lkotlin/reflect/jvm/internal/n;->a(Lkotlin/reflect/jvm/internal/impl/h/e/h;Lkotlin/reflect/jvm/internal/n$c;)Ljava/util/Collection;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/reflect/jvm/internal/impl/b/b/l",
        "<",
        "Lkotlin/reflect/jvm/internal/j",
        "<*>;",
        "Lkotlin/n;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x1
    }
    d1 = {
        "\u0000-\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002*\u0001\u0000\u0008\n\u0018\u00002\u0012\u0012\u0008\u0012\u0006\u0012\u0002\u0008\u00030\u0002\u0012\u0004\u0012\u00020\u00030\u0001B\u0005\u00a2\u0006\u0002\u0010\u0004J!\u0010\u0005\u001a\u0006\u0012\u0002\u0008\u00030\u00022\u0006\u0010\u0006\u001a\u00020\u00072\u0006\u0010\u0008\u001a\u00020\u0003H\u0016\u00a2\u0006\u0002\u0010\tJ!\u0010\n\u001a\u0006\u0012\u0002\u0008\u00030\u00022\u0006\u0010\u0006\u001a\u00020\u000b2\u0006\u0010\u0008\u001a\u00020\u0003H\u0016\u00a2\u0006\u0002\u0010\u000cJ!\u0010\r\u001a\u0006\u0012\u0002\u0008\u00030\u00022\u0006\u0010\u0006\u001a\u00020\u000e2\u0006\u0010\u0008\u001a\u00020\u0003H\u0016\u00a2\u0006\u0002\u0010\u000f\u00a8\u0006\u0010"
    }
    d2 = {
        "kotlin/reflect/jvm/internal/KDeclarationContainerImpl$getMembers$visitor$1",
        "Lorg/jetbrains/kotlin/descriptors/impl/DeclarationDescriptorVisitorEmptyBodies;",
        "Lkotlin/reflect/jvm/internal/KCallableImpl;",
        "",
        "(Lkotlin/reflect/jvm/internal/KDeclarationContainerImpl;)V",
        "visitConstructorDescriptor",
        "descriptor",
        "Lorg/jetbrains/kotlin/descriptors/ConstructorDescriptor;",
        "data",
        "(Lorg/jetbrains/kotlin/descriptors/ConstructorDescriptor;Lkotlin/Unit;)Lkotlin/reflect/jvm/internal/KCallableImpl;",
        "visitFunctionDescriptor",
        "Lorg/jetbrains/kotlin/descriptors/FunctionDescriptor;",
        "(Lorg/jetbrains/kotlin/descriptors/FunctionDescriptor;Lkotlin/Unit;)Lkotlin/reflect/jvm/internal/KCallableImpl;",
        "visitPropertyDescriptor",
        "Lorg/jetbrains/kotlin/descriptors/PropertyDescriptor;",
        "(Lorg/jetbrains/kotlin/descriptors/PropertyDescriptor;Lkotlin/Unit;)Lkotlin/reflect/jvm/internal/KCallableImpl;",
        "kotlin-reflection"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x5
    }
.end annotation


# instance fields
.field final synthetic a:Lkotlin/reflect/jvm/internal/n;


# direct methods
.method constructor <init>(Lkotlin/reflect/jvm/internal/n;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 51
    iput-object p1, p0, Lkotlin/reflect/jvm/internal/n$e;->a:Lkotlin/reflect/jvm/internal/n;

    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/b/b/l;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic a(Lkotlin/reflect/jvm/internal/impl/b/ag;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 51
    check-cast p2, Lkotlin/n;

    invoke-virtual {p0, p1, p2}, Lkotlin/reflect/jvm/internal/n$e;->a(Lkotlin/reflect/jvm/internal/impl/b/ag;Lkotlin/n;)Lkotlin/reflect/jvm/internal/j;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic a(Lkotlin/reflect/jvm/internal/impl/b/l;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 51
    check-cast p2, Lkotlin/n;

    invoke-virtual {p0, p1, p2}, Lkotlin/reflect/jvm/internal/n$e;->a(Lkotlin/reflect/jvm/internal/impl/b/l;Lkotlin/n;)Lkotlin/reflect/jvm/internal/j;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic a(Lkotlin/reflect/jvm/internal/impl/b/s;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 51
    check-cast p2, Lkotlin/n;

    invoke-virtual {p0, p1, p2}, Lkotlin/reflect/jvm/internal/n$e;->a(Lkotlin/reflect/jvm/internal/impl/b/s;Lkotlin/n;)Lkotlin/reflect/jvm/internal/j;

    move-result-object v0

    return-object v0
.end method

.method public a(Lkotlin/reflect/jvm/internal/impl/b/ag;Lkotlin/n;)Lkotlin/reflect/jvm/internal/j;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/reflect/jvm/internal/impl/b/ag;",
            "Lkotlin/n;",
            ")",
            "Lkotlin/reflect/jvm/internal/j",
            "<*>;"
        }
    .end annotation

    .prologue
    const-string v0, "descriptor"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "data"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 53
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/n$e;->a:Lkotlin/reflect/jvm/internal/n;

    invoke-static {v0, p1}, Lkotlin/reflect/jvm/internal/n;->a(Lkotlin/reflect/jvm/internal/n;Lkotlin/reflect/jvm/internal/impl/b/ag;)Lkotlin/reflect/jvm/internal/w;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/j;

    return-object v0
.end method

.method public a(Lkotlin/reflect/jvm/internal/impl/b/l;Lkotlin/n;)Lkotlin/reflect/jvm/internal/j;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/reflect/jvm/internal/impl/b/l;",
            "Lkotlin/n;",
            ")",
            "Lkotlin/reflect/jvm/internal/j",
            "<*>;"
        }
    .end annotation

    .prologue
    const-string v0, "descriptor"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "data"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 59
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "No constructors should appear in this scope: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method

.method public a(Lkotlin/reflect/jvm/internal/impl/b/s;Lkotlin/n;)Lkotlin/reflect/jvm/internal/j;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/reflect/jvm/internal/impl/b/s;",
            "Lkotlin/n;",
            ")",
            "Lkotlin/reflect/jvm/internal/j",
            "<*>;"
        }
    .end annotation

    .prologue
    const-string v0, "descriptor"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "data"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 56
    new-instance v0, Lkotlin/reflect/jvm/internal/o;

    iget-object v1, p0, Lkotlin/reflect/jvm/internal/n$e;->a:Lkotlin/reflect/jvm/internal/n;

    invoke-direct {v0, v1, p1}, Lkotlin/reflect/jvm/internal/o;-><init>(Lkotlin/reflect/jvm/internal/n;Lkotlin/reflect/jvm/internal/impl/b/s;)V

    check-cast v0, Lkotlin/reflect/jvm/internal/j;

    return-object v0
.end method
