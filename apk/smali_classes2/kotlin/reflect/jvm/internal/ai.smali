.class public final Lkotlin/reflect/jvm/internal/ai;
.super Ljava/lang/Object;
.source "RuntimeTypeMapper.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x1
    }
    d1 = {
        "\u0000:\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0008\u00c0\u0002\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u0012\u0010\u0008\u001a\u0004\u0018\u00010\t2\u0006\u0010\n\u001a\u00020\u000bH\u0002J\u0012\u0010\u000c\u001a\u00020\r2\n\u0010\u000e\u001a\u0006\u0012\u0002\u0008\u00030\u0005J\u000e\u0010\u000f\u001a\u00020\u00102\u0006\u0010\u0011\u001a\u00020\u0012J\u000e\u0010\u0013\u001a\u00020\t2\u0006\u0010\u0014\u001a\u00020\u000bR\u001e\u0010\u0003\u001a\u0004\u0018\u00010\u0004*\u0006\u0012\u0002\u0008\u00030\u00058BX\u0082\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0006\u0010\u0007\u00a8\u0006\u0015"
    }
    d2 = {
        "Lkotlin/reflect/jvm/internal/RuntimeTypeMapper;",
        "",
        "()V",
        "primitiveType",
        "Lorg/jetbrains/kotlin/builtins/PrimitiveType;",
        "Ljava/lang/Class;",
        "getPrimitiveType",
        "(Ljava/lang/Class;)Lorg/jetbrains/kotlin/builtins/PrimitiveType;",
        "mapIntrinsicFunctionSignature",
        "Lkotlin/reflect/jvm/internal/JvmFunctionSignature;",
        "function",
        "Lorg/jetbrains/kotlin/descriptors/FunctionDescriptor;",
        "mapJvmClassToKotlinClassId",
        "Lorg/jetbrains/kotlin/name/ClassId;",
        "klass",
        "mapPropertySignature",
        "Lkotlin/reflect/jvm/internal/JvmPropertySignature;",
        "possiblyOverriddenProperty",
        "Lorg/jetbrains/kotlin/descriptors/PropertyDescriptor;",
        "mapSignature",
        "possiblySubstitutedFunction",
        "kotlin-reflection"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x5
    }
.end annotation


# static fields
.field public static final a:Lkotlin/reflect/jvm/internal/ai;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 162
    new-instance v0, Lkotlin/reflect/jvm/internal/ai;

    invoke-direct {v0}, Lkotlin/reflect/jvm/internal/ai;-><init>()V

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 162
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    check-cast p0, Lkotlin/reflect/jvm/internal/ai;

    sput-object p0, Lkotlin/reflect/jvm/internal/ai;->a:Lkotlin/reflect/jvm/internal/ai;

    return-void
.end method

.method private final b(Lkotlin/reflect/jvm/internal/impl/b/s;)Lkotlin/reflect/jvm/internal/h;
    .locals 7

    .prologue
    const/4 v4, 0x1

    const/4 v6, 0x0

    .line 237
    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/b/s;->i()Ljava/util/List;

    move-result-object v0

    .line 239
    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/b/s;->D_()Lkotlin/reflect/jvm/internal/impl/e/f;

    move-result-object v1

    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/impl/e/f;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v2

    sparse-switch v2, :sswitch_data_0

    .line 255
    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 239
    :sswitch_0
    const-string v2, "equals"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 240
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-ne v1, v4, :cond_0

    invoke-static {v0}, Lkotlin/a/m;->i(Ljava/util/List;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/at;

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/b/at;->s()Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v0

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/a/l;->i(Lkotlin/reflect/jvm/internal/impl/k/r;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 241
    new-instance v1, Lkotlin/reflect/jvm/internal/h$a$a;

    const-string v2, "equals(Ljava/lang/Object;)Z"

    const-class v0, Ljava/lang/Object;

    const-string v3, "equals"

    new-array v4, v4, [Ljava/lang/Class;

    const-class v5, Ljava/lang/Object;

    aput-object v5, v4, v6

    invoke-virtual {v0, v3, v4}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    const-string v3, "Any::class.java.getDecla\u2026equals\", Any::class.java)"

    invoke-static {v0, v3}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Ljava/lang/reflect/Member;

    invoke-direct {v1, v2, v0}, Lkotlin/reflect/jvm/internal/h$a$a;-><init>(Ljava/lang/String;Ljava/lang/reflect/Member;)V

    move-object v0, v1

    check-cast v0, Lkotlin/reflect/jvm/internal/h;

    goto :goto_0

    .line 239
    :sswitch_1
    const-string v2, "hashCode"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 244
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 245
    new-instance v1, Lkotlin/reflect/jvm/internal/h$a$a;

    const-string v2, "hashCode()I"

    const-class v0, Ljava/lang/Object;

    const-string v3, "hashCode"

    new-array v4, v6, [Ljava/lang/Class;

    invoke-virtual {v0, v3, v4}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    const-string v3, "Any::class.java.getDeclaredMethod(\"hashCode\")"

    invoke-static {v0, v3}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Ljava/lang/reflect/Member;

    invoke-direct {v1, v2, v0}, Lkotlin/reflect/jvm/internal/h$a$a;-><init>(Ljava/lang/String;Ljava/lang/reflect/Member;)V

    move-object v0, v1

    check-cast v0, Lkotlin/reflect/jvm/internal/h;

    goto :goto_0

    .line 239
    :sswitch_2
    const-string v2, "toString"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 248
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 249
    new-instance v1, Lkotlin/reflect/jvm/internal/h$a$a;

    const-string v2, "toString()Ljava/lang/String;"

    const-class v0, Ljava/lang/Object;

    const-string v3, "toString"

    new-array v4, v6, [Ljava/lang/Class;

    invoke-virtual {v0, v3, v4}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    const-string v3, "Any::class.java.getDeclaredMethod(\"toString\")"

    invoke-static {v0, v3}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Ljava/lang/reflect/Member;

    invoke-direct {v1, v2, v0}, Lkotlin/reflect/jvm/internal/h$a$a;-><init>(Ljava/lang/String;Ljava/lang/reflect/Member;)V

    move-object v0, v1

    check-cast v0, Lkotlin/reflect/jvm/internal/h;

    goto/16 :goto_0

    .line 239
    :sswitch_data_0
    .sparse-switch
        -0x69e9ad94 -> :sswitch_2
        -0x4d378041 -> :sswitch_0
        0x8cdac1b -> :sswitch_1
    .end sparse-switch
.end method

.method private final b(Ljava/lang/Class;)Lkotlin/reflect/jvm/internal/impl/a/m;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;)",
            "Lkotlin/reflect/jvm/internal/impl/a/m;"
        }
    .end annotation

    .prologue
    .line 279
    invoke-virtual {p1}, Ljava/lang/Class;->isPrimitive()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/h/d/c;->a(Ljava/lang/String;)Lkotlin/reflect/jvm/internal/impl/h/d/c;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/h/d/c;->a()Lkotlin/reflect/jvm/internal/impl/a/m;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Lkotlin/reflect/jvm/internal/impl/b/s;)Lkotlin/reflect/jvm/internal/h;
    .locals 6

    .prologue
    const/4 v2, 0x0

    const-string v0, "possiblySubstitutedFunction"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 166
    check-cast p1, Lkotlin/reflect/jvm/internal/impl/b/b;

    invoke-static {p1}, Lkotlin/reflect/jvm/internal/impl/h/c;->a(Lkotlin/reflect/jvm/internal/impl/b/b;)Lkotlin/reflect/jvm/internal/impl/b/b;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/s;

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/b/s;->r()Lkotlin/reflect/jvm/internal/impl/b/s;

    move-result-object v1

    .line 169
    instance-of v0, v1, Lkotlin/reflect/jvm/internal/impl/i/b/a/c;

    if-eqz v0, :cond_3

    .line 170
    const-string v0, "function"

    invoke-static {v1, v0}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, v1}, Lkotlin/reflect/jvm/internal/ai;->b(Lkotlin/reflect/jvm/internal/impl/b/s;)Lkotlin/reflect/jvm/internal/h;

    move-result-object v0

    if-eqz v0, :cond_0

    check-cast v0, Lkotlin/reflect/jvm/internal/h;

    .line 201
    :goto_0
    return-object v0

    :cond_0
    move-object v0, v1

    .line 174
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/i/b/a/c;

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/i/b/a/c;->J()Lkotlin/reflect/jvm/internal/impl/protobuf/o;

    move-result-object v2

    .line 175
    instance-of v0, v2, Lkotlin/reflect/jvm/internal/impl/i/e$i;

    if-eqz v0, :cond_1

    .line 176
    sget-object v4, Lkotlin/reflect/jvm/internal/impl/i/c/d;->a:Lkotlin/reflect/jvm/internal/impl/i/c/d;

    move-object v0, v2

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/i/e$i;

    move-object v3, v1

    check-cast v3, Lkotlin/reflect/jvm/internal/impl/i/b/a/c;

    invoke-interface {v3}, Lkotlin/reflect/jvm/internal/impl/i/b/a/c;->K()Lkotlin/reflect/jvm/internal/impl/i/b/x;

    move-result-object v5

    move-object v3, v1

    check-cast v3, Lkotlin/reflect/jvm/internal/impl/i/b/a/c;

    invoke-interface {v3}, Lkotlin/reflect/jvm/internal/impl/i/b/a/c;->L()Lkotlin/reflect/jvm/internal/impl/i/b/ah;

    move-result-object v3

    invoke-virtual {v4, v0, v5, v3}, Lkotlin/reflect/jvm/internal/impl/i/c/d;->a(Lkotlin/reflect/jvm/internal/impl/i/e$i;Lkotlin/reflect/jvm/internal/impl/i/b/x;Lkotlin/reflect/jvm/internal/impl/i/b/ah;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    check-cast v0, Ljava/lang/String;

    .line 177
    new-instance v1, Lkotlin/reflect/jvm/internal/h$f;

    invoke-direct {v1, v0}, Lkotlin/reflect/jvm/internal/h$f;-><init>(Ljava/lang/String;)V

    move-object v0, v1

    check-cast v0, Lkotlin/reflect/jvm/internal/h;

    goto :goto_0

    .line 180
    :cond_1
    instance-of v0, v2, Lkotlin/reflect/jvm/internal/impl/i/e$e;

    if-eqz v0, :cond_2

    .line 181
    sget-object v3, Lkotlin/reflect/jvm/internal/impl/i/c/d;->a:Lkotlin/reflect/jvm/internal/impl/i/c/d;

    check-cast v2, Lkotlin/reflect/jvm/internal/impl/i/e$e;

    move-object v0, v1

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/i/b/a/c;

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/i/b/a/c;->K()Lkotlin/reflect/jvm/internal/impl/i/b/x;

    move-result-object v4

    move-object v0, v1

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/i/b/a/c;

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/i/b/a/c;->L()Lkotlin/reflect/jvm/internal/impl/i/b/ah;

    move-result-object v0

    invoke-virtual {v3, v2, v4, v0}, Lkotlin/reflect/jvm/internal/impl/i/c/d;->a(Lkotlin/reflect/jvm/internal/impl/i/e$e;Lkotlin/reflect/jvm/internal/impl/i/b/x;Lkotlin/reflect/jvm/internal/impl/i/b/ah;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    check-cast v0, Ljava/lang/String;

    .line 182
    new-instance v1, Lkotlin/reflect/jvm/internal/h$e;

    invoke-direct {v1, v0}, Lkotlin/reflect/jvm/internal/h$e;-><init>(Ljava/lang/String;)V

    move-object v0, v1

    check-cast v0, Lkotlin/reflect/jvm/internal/h;

    goto :goto_0

    .line 186
    :cond_2
    new-instance v0, Lkotlin/reflect/jvm/internal/ac;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Reflection on built-in Kotlin types is not yet fully supported. "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "No metadata found for "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lkotlin/reflect/jvm/internal/ac;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    .line 189
    :cond_3
    instance-of v0, v1, Lkotlin/reflect/jvm/internal/impl/d/a/b/d;

    if-eqz v0, :cond_8

    move-object v0, v1

    .line 190
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/d/a/b/d;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/d/a/b/d;->x()Lkotlin/reflect/jvm/internal/impl/b/al;

    move-result-object v0

    instance-of v3, v0, Lkotlin/reflect/jvm/internal/impl/d/a/e/a;

    if-nez v3, :cond_4

    move-object v0, v2

    :cond_4
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/d/a/e/a;

    if-eqz v0, :cond_6

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/d/a/e/a;->c()Lkotlin/reflect/jvm/internal/impl/d/a/f/l;

    move-result-object v0

    :goto_1
    instance-of v3, v0, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/s;

    if-nez v3, :cond_5

    move-object v0, v2

    :cond_5
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/s;

    if-eqz v0, :cond_7

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/s;->h()Ljava/lang/reflect/Method;

    move-result-object v2

    if-eqz v2, :cond_7

    .line 193
    new-instance v0, Lkotlin/reflect/jvm/internal/h$d;

    invoke-direct {v0, v2}, Lkotlin/reflect/jvm/internal/h$d;-><init>(Ljava/lang/reflect/Method;)V

    check-cast v0, Lkotlin/reflect/jvm/internal/h;

    goto/16 :goto_0

    :cond_6
    move-object v0, v2

    .line 190
    goto :goto_1

    .line 191
    :cond_7
    new-instance v0, Lkotlin/reflect/jvm/internal/ac;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Incorrect resolution sequence for Java method "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lkotlin/reflect/jvm/internal/ac;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    .line 195
    :cond_8
    instance-of v0, v1, Lkotlin/reflect/jvm/internal/impl/d/a/b/b;

    if-eqz v0, :cond_d

    move-object v0, v1

    .line 196
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/d/a/b/b;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/d/a/b/b;->x()Lkotlin/reflect/jvm/internal/impl/b/al;

    move-result-object v0

    instance-of v3, v0, Lkotlin/reflect/jvm/internal/impl/d/a/e/a;

    if-nez v3, :cond_9

    move-object v0, v2

    :cond_9
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/d/a/e/a;

    if-eqz v0, :cond_a

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/d/a/e/a;->c()Lkotlin/reflect/jvm/internal/impl/d/a/f/l;

    move-result-object v0

    .line 198
    :goto_2
    instance-of v2, v0, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/m;

    if-eqz v2, :cond_b

    .line 199
    new-instance v1, Lkotlin/reflect/jvm/internal/h$c;

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/m;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/m;->d()Ljava/lang/reflect/Constructor;

    move-result-object v0

    invoke-direct {v1, v0}, Lkotlin/reflect/jvm/internal/h$c;-><init>(Ljava/lang/reflect/Constructor;)V

    move-object v0, v1

    check-cast v0, Lkotlin/reflect/jvm/internal/h;

    goto/16 :goto_0

    :cond_a
    move-object v0, v2

    .line 196
    goto :goto_2

    .line 200
    :cond_b
    instance-of v2, v0, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/j;

    if-eqz v2, :cond_c

    move-object v2, v0

    check-cast v2, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/j;

    invoke-virtual {v2}, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/j;->h()Z

    move-result v2

    if-eqz v2, :cond_c

    .line 201
    new-instance v1, Lkotlin/reflect/jvm/internal/h$b;

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/j;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/j;->u()Ljava/lang/Class;

    move-result-object v0

    invoke-direct {v1, v0}, Lkotlin/reflect/jvm/internal/h$b;-><init>(Ljava/lang/Class;)V

    move-object v0, v1

    check-cast v0, Lkotlin/reflect/jvm/internal/h;

    goto/16 :goto_0

    .line 202
    :cond_c
    new-instance v2, Lkotlin/reflect/jvm/internal/ac;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Incorrect resolution sequence for Java constructor "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " ("

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lkotlin/reflect/jvm/internal/ac;-><init>(Ljava/lang/String;)V

    move-object v0, v2

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    .line 205
    :cond_d
    new-instance v0, Lkotlin/reflect/jvm/internal/ac;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unknown origin of "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " ("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lkotlin/reflect/jvm/internal/ac;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method

.method public final a(Lkotlin/reflect/jvm/internal/impl/b/ag;)Lkotlin/reflect/jvm/internal/i;
    .locals 6

    .prologue
    const/4 v3, 0x0

    const-string v0, "possiblyOverriddenProperty"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 210
    check-cast p1, Lkotlin/reflect/jvm/internal/impl/b/b;

    invoke-static {p1}, Lkotlin/reflect/jvm/internal/impl/h/c;->a(Lkotlin/reflect/jvm/internal/impl/b/b;)Lkotlin/reflect/jvm/internal/impl/b/b;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/ag;

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/b/ag;->q()Lkotlin/reflect/jvm/internal/impl/b/ag;

    move-result-object v1

    .line 211
    instance-of v0, v1, Lkotlin/reflect/jvm/internal/impl/i/b/a/j;

    if-eqz v0, :cond_1

    move-object v0, v1

    .line 212
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/i/b/a/j;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/i/b/a/j;->G()Lkotlin/reflect/jvm/internal/impl/i/e$o;

    move-result-object v2

    .line 213
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/i/c/c;->c:Lkotlin/reflect/jvm/internal/impl/protobuf/h$f;

    invoke-virtual {v2, v0}, Lkotlin/reflect/jvm/internal/impl/i/e$o;->a(Lkotlin/reflect/jvm/internal/impl/protobuf/h$f;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 215
    new-instance v0, Lkotlin/reflect/jvm/internal/ac;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Reflection on built-in Kotlin types is not yet fully supported. "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "No metadata found for "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lkotlin/reflect/jvm/internal/ac;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    .line 218
    :cond_0
    new-instance v0, Lkotlin/reflect/jvm/internal/i$c;

    const-string v3, "property"

    invoke-static {v1, v3}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v3, Lkotlin/reflect/jvm/internal/impl/i/c/c;->c:Lkotlin/reflect/jvm/internal/impl/protobuf/h$f;

    invoke-virtual {v2, v3}, Lkotlin/reflect/jvm/internal/impl/i/e$o;->b(Lkotlin/reflect/jvm/internal/impl/protobuf/h$f;)Ljava/lang/Object;

    move-result-object v3

    const-string v4, "proto.getExtension(JvmProtoBuf.propertySignature)"

    invoke-static {v3, v4}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v3, Lkotlin/reflect/jvm/internal/impl/i/c/c$e;

    move-object v4, v1

    check-cast v4, Lkotlin/reflect/jvm/internal/impl/i/b/a/j;

    invoke-virtual {v4}, Lkotlin/reflect/jvm/internal/impl/i/b/a/j;->K()Lkotlin/reflect/jvm/internal/impl/i/b/x;

    move-result-object v4

    move-object v5, v1

    check-cast v5, Lkotlin/reflect/jvm/internal/impl/i/b/a/j;

    invoke-virtual {v5}, Lkotlin/reflect/jvm/internal/impl/i/b/a/j;->L()Lkotlin/reflect/jvm/internal/impl/i/b/ah;

    move-result-object v5

    invoke-direct/range {v0 .. v5}, Lkotlin/reflect/jvm/internal/i$c;-><init>(Lkotlin/reflect/jvm/internal/impl/b/ag;Lkotlin/reflect/jvm/internal/impl/i/e$o;Lkotlin/reflect/jvm/internal/impl/i/c/c$e;Lkotlin/reflect/jvm/internal/impl/i/b/x;Lkotlin/reflect/jvm/internal/impl/i/b/ah;)V

    check-cast v0, Lkotlin/reflect/jvm/internal/i;

    .line 226
    :goto_0
    return-object v0

    .line 222
    :cond_1
    instance-of v0, v1, Lkotlin/reflect/jvm/internal/impl/d/a/b/e;

    if-eqz v0, :cond_b

    move-object v0, v1

    .line 223
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/d/a/b/e;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/d/a/b/e;->x()Lkotlin/reflect/jvm/internal/impl/b/al;

    move-result-object v0

    instance-of v2, v0, Lkotlin/reflect/jvm/internal/impl/d/a/e/a;

    if-nez v2, :cond_2

    move-object v0, v3

    :cond_2
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/d/a/e/a;

    if-eqz v0, :cond_3

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/d/a/e/a;->c()Lkotlin/reflect/jvm/internal/impl/d/a/f/l;

    move-result-object v0

    .line 225
    :goto_1
    instance-of v2, v0, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/p;

    if-eqz v2, :cond_4

    new-instance v1, Lkotlin/reflect/jvm/internal/i$a;

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/p;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/p;->h()Ljava/lang/reflect/Field;

    move-result-object v0

    invoke-direct {v1, v0}, Lkotlin/reflect/jvm/internal/i$a;-><init>(Ljava/lang/reflect/Field;)V

    move-object v0, v1

    check-cast v0, Lkotlin/reflect/jvm/internal/i;

    goto :goto_0

    :cond_3
    move-object v0, v3

    .line 223
    goto :goto_1

    .line 226
    :cond_4
    instance-of v2, v0, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/s;

    if-eqz v2, :cond_a

    new-instance v2, Lkotlin/reflect/jvm/internal/i$b;

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/s;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/s;->h()Ljava/lang/reflect/Method;

    move-result-object v4

    invoke-interface {v1}, Lkotlin/reflect/jvm/internal/impl/b/ag;->c()Lkotlin/reflect/jvm/internal/impl/b/ai;

    move-result-object v0

    if-eqz v0, :cond_8

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/b/ai;->x()Lkotlin/reflect/jvm/internal/impl/b/al;

    move-result-object v0

    :goto_2
    instance-of v1, v0, Lkotlin/reflect/jvm/internal/impl/d/a/e/a;

    if-nez v1, :cond_5

    move-object v0, v3

    :cond_5
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/d/a/e/a;

    if-eqz v0, :cond_9

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/d/a/e/a;->c()Lkotlin/reflect/jvm/internal/impl/d/a/f/l;

    move-result-object v0

    :goto_3
    instance-of v1, v0, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/s;

    if-nez v1, :cond_6

    move-object v0, v3

    :cond_6
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/s;

    if-eqz v0, :cond_7

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/s;->h()Ljava/lang/reflect/Method;

    move-result-object v3

    :cond_7
    invoke-direct {v2, v4, v3}, Lkotlin/reflect/jvm/internal/i$b;-><init>(Ljava/lang/reflect/Method;Ljava/lang/reflect/Method;)V

    move-object v0, v2

    check-cast v0, Lkotlin/reflect/jvm/internal/i;

    goto :goto_0

    :cond_8
    move-object v0, v3

    goto :goto_2

    :cond_9
    move-object v0, v3

    goto :goto_3

    .line 230
    :cond_a
    new-instance v2, Lkotlin/reflect/jvm/internal/ac;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Incorrect resolution sequence for Java field "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " (source = "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lkotlin/reflect/jvm/internal/ac;-><init>(Ljava/lang/String;)V

    move-object v0, v2

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    .line 233
    :cond_b
    new-instance v0, Lkotlin/reflect/jvm/internal/ac;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unknown origin of "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " ("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lkotlin/reflect/jvm/internal/ac;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method

.method public final a(Ljava/lang/Class;)Lkotlin/reflect/jvm/internal/impl/e/a;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;)",
            "Lkotlin/reflect/jvm/internal/impl/e/a;"
        }
    .end annotation

    .prologue
    const-string v0, "klass"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 259
    invoke-virtual {p1}, Ljava/lang/Class;->isArray()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 260
    invoke-virtual {p1}, Ljava/lang/Class;->getComponentType()Ljava/lang/Class;

    move-result-object v0

    invoke-direct {p0, v0}, Lkotlin/reflect/jvm/internal/ai;->b(Ljava/lang/Class;)Lkotlin/reflect/jvm/internal/impl/a/m;

    move-result-object v0

    if-eqz v0, :cond_0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/a/m;

    .line 261
    new-instance v1, Lkotlin/reflect/jvm/internal/impl/e/a;

    sget-object v2, Lkotlin/reflect/jvm/internal/impl/a/l;->c:Lkotlin/reflect/jvm/internal/impl/e/b;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/a/m;->b()Lkotlin/reflect/jvm/internal/impl/e/f;

    move-result-object v0

    invoke-direct {v1, v2, v0}, Lkotlin/reflect/jvm/internal/impl/e/a;-><init>(Lkotlin/reflect/jvm/internal/impl/e/b;Lkotlin/reflect/jvm/internal/impl/e/f;)V

    move-object v0, v1

    .line 275
    :goto_0
    return-object v0

    .line 263
    :cond_0
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/a/l;->h:Lkotlin/reflect/jvm/internal/impl/a/l$a;

    iget-object v0, v0, Lkotlin/reflect/jvm/internal/impl/a/l$a;->h:Lkotlin/reflect/jvm/internal/impl/e/c;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/e/c;->c()Lkotlin/reflect/jvm/internal/impl/e/b;

    move-result-object v0

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/e/a;->a(Lkotlin/reflect/jvm/internal/impl/e/b;)Lkotlin/reflect/jvm/internal/impl/e/a;

    move-result-object v0

    const-string v1, "ClassId.topLevel(KotlinB\u2026.FQ_NAMES.array.toSafe())"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    .line 266
    :cond_1
    invoke-direct {p0, p1}, Lkotlin/reflect/jvm/internal/ai;->b(Ljava/lang/Class;)Lkotlin/reflect/jvm/internal/impl/a/m;

    move-result-object v0

    if-eqz v0, :cond_2

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/a/m;

    .line 267
    new-instance v1, Lkotlin/reflect/jvm/internal/impl/e/a;

    sget-object v2, Lkotlin/reflect/jvm/internal/impl/a/l;->c:Lkotlin/reflect/jvm/internal/impl/e/b;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/a/m;->a()Lkotlin/reflect/jvm/internal/impl/e/f;

    move-result-object v0

    invoke-direct {v1, v2, v0}, Lkotlin/reflect/jvm/internal/impl/e/a;-><init>(Lkotlin/reflect/jvm/internal/impl/e/b;Lkotlin/reflect/jvm/internal/impl/e/f;)V

    move-object v0, v1

    goto :goto_0

    .line 270
    :cond_2
    invoke-static {p1}, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/b;->e(Ljava/lang/Class;)Lkotlin/reflect/jvm/internal/impl/e/a;

    move-result-object v1

    .line 271
    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/impl/e/a;->d()Z

    move-result v0

    if-nez v0, :cond_3

    .line 272
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/f/a;->a:Lkotlin/reflect/jvm/internal/impl/f/a;

    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/impl/e/a;->g()Lkotlin/reflect/jvm/internal/impl/e/b;

    move-result-object v2

    invoke-virtual {v0, v2}, Lkotlin/reflect/jvm/internal/impl/f/a;->a(Lkotlin/reflect/jvm/internal/impl/e/b;)Lkotlin/reflect/jvm/internal/impl/e/a;

    move-result-object v0

    if-eqz v0, :cond_3

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/e/a;

    const-string v1, "it"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    :cond_3
    move-object v0, v1

    .line 275
    goto :goto_0
.end method
