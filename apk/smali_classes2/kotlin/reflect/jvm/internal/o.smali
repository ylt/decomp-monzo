.class public final Lkotlin/reflect/jvm/internal/o;
.super Lkotlin/reflect/jvm/internal/j;
.source "KFunctionImpl.kt"

# interfaces
.implements Lkotlin/d/b/j;
.implements Lkotlin/reflect/f;
.implements Lkotlin/reflect/jvm/internal/g;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/reflect/jvm/internal/j",
        "<",
        "Ljava/lang/Object;",
        ">;",
        "Lkotlin/d/b/j;",
        "Lkotlin/reflect/f",
        "<",
        "Ljava/lang/Object;",
        ">;",
        "Lkotlin/reflect/jvm/internal/g;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x1
    }
    d1 = {
        "\u0000\\\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\r\n\u0002\u0010\u000b\n\u0002\u0008\t\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0010\u0008\n\u0002\u0008\u0005\u0008\u0000\u0018\u00002\n\u0012\u0006\u0012\u0004\u0018\u00010\u00020\u00012\n\u0012\u0006\u0012\u0004\u0018\u00010\u00020\u00032\u00020\u00042\u00020\u0005B)\u0008\u0016\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\t\u0012\u0008\u0010\u000b\u001a\u0004\u0018\u00010\u0002\u00a2\u0006\u0002\u0010\u000cB\u0017\u0008\u0016\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\r\u001a\u00020\u000e\u00a2\u0006\u0002\u0010\u000fB5\u0008\u0002\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\t\u0012\u0008\u0010\u0010\u001a\u0004\u0018\u00010\u000e\u0012\n\u0008\u0002\u0010\u000b\u001a\u0004\u0018\u00010\u0002\u00a2\u0006\u0002\u0010\u0011J\u001e\u0010*\u001a\u000c\u0012\u0008\u0012\u0006\u0012\u0002\u0008\u00030+0\u00132\n\u0010,\u001a\u0006\u0012\u0002\u0008\u00030+H\u0002J\u0010\u0010-\u001a\u00020.2\u0006\u0010,\u001a\u00020/H\u0002J\u0010\u00100\u001a\u00020.2\u0006\u0010,\u001a\u00020/H\u0002J\u0010\u00101\u001a\u00020.2\u0006\u0010,\u001a\u00020/H\u0002J\u0013\u00102\u001a\u00020!2\u0008\u00103\u001a\u0004\u0018\u00010\u0002H\u0096\u0002J\u0008\u00104\u001a\u000205H\u0016J\u0008\u00106\u001a\u000205H\u0016J\u0008\u00107\u001a\u00020!H\u0002J\u0008\u00108\u001a\u00020!H\u0002J\u0008\u00109\u001a\u00020\tH\u0016R\u0010\u0010\u000b\u001a\u0004\u0018\u00010\u0002X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001f\u0010\u0012\u001a\u0006\u0012\u0002\u0008\u00030\u00138VX\u0096\u0084\u0002\u00a2\u0006\u000c\n\u0004\u0008\u0016\u0010\u0017\u001a\u0004\u0008\u0014\u0010\u0015R\u0014\u0010\u0006\u001a\u00020\u0007X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0018\u0010\u0019R!\u0010\u001a\u001a\u0008\u0012\u0002\u0008\u0003\u0018\u00010\u00138VX\u0096\u0084\u0002\u00a2\u0006\u000c\n\u0004\u0008\u001c\u0010\u0017\u001a\u0004\u0008\u001b\u0010\u0015R\u001b\u0010\r\u001a\u00020\u000e8VX\u0096\u0084\u0002\u00a2\u0006\u000c\n\u0004\u0008\u001f\u0010\u0017\u001a\u0004\u0008\u001d\u0010\u001eR\u0014\u0010 \u001a\u00020!8VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008 \u0010\"R\u0014\u0010#\u001a\u00020!8VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008#\u0010\"R\u0014\u0010$\u001a\u00020!8VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008$\u0010\"R\u0014\u0010%\u001a\u00020!8VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008%\u0010\"R\u0014\u0010&\u001a\u00020!8VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008&\u0010\"R\u0014\u0010\'\u001a\u00020!8VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\'\u0010\"R\u0014\u0010\u0008\u001a\u00020\t8VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008(\u0010)R\u000e\u0010\n\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006:"
    }
    d2 = {
        "Lkotlin/reflect/jvm/internal/KFunctionImpl;",
        "Lkotlin/reflect/jvm/internal/KCallableImpl;",
        "",
        "Lkotlin/reflect/KFunction;",
        "Lkotlin/jvm/internal/FunctionBase;",
        "Lkotlin/reflect/jvm/internal/FunctionWithAllInvokes;",
        "container",
        "Lkotlin/reflect/jvm/internal/KDeclarationContainerImpl;",
        "name",
        "",
        "signature",
        "boundReceiver",
        "(Lkotlin/reflect/jvm/internal/KDeclarationContainerImpl;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;)V",
        "descriptor",
        "Lorg/jetbrains/kotlin/descriptors/FunctionDescriptor;",
        "(Lkotlin/reflect/jvm/internal/KDeclarationContainerImpl;Lorg/jetbrains/kotlin/descriptors/FunctionDescriptor;)V",
        "descriptorInitialValue",
        "(Lkotlin/reflect/jvm/internal/KDeclarationContainerImpl;Ljava/lang/String;Ljava/lang/String;Lorg/jetbrains/kotlin/descriptors/FunctionDescriptor;Ljava/lang/Object;)V",
        "caller",
        "Lkotlin/reflect/jvm/internal/FunctionCaller;",
        "getCaller",
        "()Lkotlin/reflect/jvm/internal/FunctionCaller;",
        "caller$delegate",
        "Lkotlin/reflect/jvm/internal/ReflectProperties$LazySoftVal;",
        "getContainer",
        "()Lkotlin/reflect/jvm/internal/KDeclarationContainerImpl;",
        "defaultCaller",
        "getDefaultCaller",
        "defaultCaller$delegate",
        "getDescriptor",
        "()Lorg/jetbrains/kotlin/descriptors/FunctionDescriptor;",
        "descriptor$delegate",
        "isBound",
        "",
        "()Z",
        "isExternal",
        "isInfix",
        "isInline",
        "isOperator",
        "isSuspend",
        "getName",
        "()Ljava/lang/String;",
        "createConstructorCaller",
        "Ljava/lang/reflect/Constructor;",
        "member",
        "createInstanceMethodCaller",
        "Lkotlin/reflect/jvm/internal/FunctionCaller$Method;",
        "Ljava/lang/reflect/Method;",
        "createJvmStaticInObjectCaller",
        "createStaticMethodCaller",
        "equals",
        "other",
        "getArity",
        "",
        "hashCode",
        "isDeclared",
        "isPrivateInBytecode",
        "toString",
        "kotlin-reflection"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x5
    }
.end annotation


# static fields
.field static final synthetic a:[Lkotlin/reflect/l;


# instance fields
.field private final b:Lkotlin/reflect/jvm/internal/ae$a;

.field private final c:Lkotlin/reflect/jvm/internal/ae$a;

.field private final d:Lkotlin/reflect/jvm/internal/ae$a;

.field private final e:Lkotlin/reflect/jvm/internal/n;

.field private final f:Ljava/lang/String;

.field private final g:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const/4 v0, 0x3

    new-array v1, v0, [Lkotlin/reflect/l;

    const/4 v2, 0x0

    new-instance v0, Lkotlin/d/b/w;

    const-class v3, Lkotlin/reflect/jvm/internal/o;

    invoke-static {v3}, Lkotlin/d/b/y;->a(Ljava/lang/Class;)Lkotlin/reflect/c;

    move-result-object v3

    const-string v4, "descriptor"

    const-string v5, "getDescriptor()Lorg/jetbrains/kotlin/descriptors/FunctionDescriptor;"

    invoke-direct {v0, v3, v4, v5}, Lkotlin/d/b/w;-><init>(Lkotlin/reflect/e;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v0}, Lkotlin/d/b/y;->a(Lkotlin/d/b/v;)Lkotlin/reflect/n;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/l;

    aput-object v0, v1, v2

    const/4 v2, 0x1

    new-instance v0, Lkotlin/d/b/w;

    const-class v3, Lkotlin/reflect/jvm/internal/o;

    invoke-static {v3}, Lkotlin/d/b/y;->a(Ljava/lang/Class;)Lkotlin/reflect/c;

    move-result-object v3

    const-string v4, "caller"

    const-string v5, "getCaller()Lkotlin/reflect/jvm/internal/FunctionCaller;"

    invoke-direct {v0, v3, v4, v5}, Lkotlin/d/b/w;-><init>(Lkotlin/reflect/e;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v0}, Lkotlin/d/b/y;->a(Lkotlin/d/b/v;)Lkotlin/reflect/n;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/l;

    aput-object v0, v1, v2

    const/4 v2, 0x2

    new-instance v0, Lkotlin/d/b/w;

    const-class v3, Lkotlin/reflect/jvm/internal/o;

    invoke-static {v3}, Lkotlin/d/b/y;->a(Ljava/lang/Class;)Lkotlin/reflect/c;

    move-result-object v3

    const-string v4, "defaultCaller"

    const-string v5, "getDefaultCaller()Lkotlin/reflect/jvm/internal/FunctionCaller;"

    invoke-direct {v0, v3, v4, v5}, Lkotlin/d/b/w;-><init>(Lkotlin/reflect/e;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v0}, Lkotlin/d/b/y;->a(Lkotlin/d/b/v;)Lkotlin/reflect/n;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/l;

    aput-object v0, v1, v2

    sput-object v1, Lkotlin/reflect/jvm/internal/o;->a:[Lkotlin/reflect/l;

    return-void
.end method

.method public constructor <init>(Lkotlin/reflect/jvm/internal/n;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;)V
    .locals 6

    .prologue
    const-string v0, "container"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "name"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "signature"

    invoke-static {p3, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 43
    const/4 v4, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v5, p4

    .line 44
    invoke-direct/range {v0 .. v5}, Lkotlin/reflect/jvm/internal/o;-><init>(Lkotlin/reflect/jvm/internal/n;Ljava/lang/String;Ljava/lang/String;Lkotlin/reflect/jvm/internal/impl/b/s;Ljava/lang/Object;)V

    return-void
.end method

.method private constructor <init>(Lkotlin/reflect/jvm/internal/n;Ljava/lang/String;Ljava/lang/String;Lkotlin/reflect/jvm/internal/impl/b/s;Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 36
    .line 42
    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/j;-><init>()V

    iput-object p1, p0, Lkotlin/reflect/jvm/internal/o;->e:Lkotlin/reflect/jvm/internal/n;

    iput-object p3, p0, Lkotlin/reflect/jvm/internal/o;->f:Ljava/lang/String;

    iput-object p5, p0, Lkotlin/reflect/jvm/internal/o;->g:Ljava/lang/Object;

    .line 55
    new-instance v0, Lkotlin/reflect/jvm/internal/o$c;

    invoke-direct {v0, p0, p2}, Lkotlin/reflect/jvm/internal/o$c;-><init>(Lkotlin/reflect/jvm/internal/o;Ljava/lang/String;)V

    check-cast v0, Lkotlin/d/a/a;

    invoke-static {p4, v0}, Lkotlin/reflect/jvm/internal/ae;->a(Ljava/lang/Object;Lkotlin/d/a/a;)Lkotlin/reflect/jvm/internal/ae$a;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/o;->b:Lkotlin/reflect/jvm/internal/ae$a;

    .line 67
    new-instance v0, Lkotlin/reflect/jvm/internal/o$a;

    invoke-direct {v0, p0}, Lkotlin/reflect/jvm/internal/o$a;-><init>(Lkotlin/reflect/jvm/internal/o;)V

    check-cast v0, Lkotlin/d/a/a;

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/ae;->b(Lkotlin/d/a/a;)Lkotlin/reflect/jvm/internal/ae$a;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/o;->c:Lkotlin/reflect/jvm/internal/ae$a;

    .line 100
    new-instance v0, Lkotlin/reflect/jvm/internal/o$b;

    invoke-direct {v0, p0}, Lkotlin/reflect/jvm/internal/o$b;-><init>(Lkotlin/reflect/jvm/internal/o;)V

    check-cast v0, Lkotlin/d/a/a;

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/ae;->b(Lkotlin/d/a/a;)Lkotlin/reflect/jvm/internal/ae$a;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/o;->d:Lkotlin/reflect/jvm/internal/ae$a;

    return-void
.end method

.method synthetic constructor <init>(Lkotlin/reflect/jvm/internal/n;Ljava/lang/String;Ljava/lang/String;Lkotlin/reflect/jvm/internal/impl/b/s;Ljava/lang/Object;ILkotlin/d/b/i;)V
    .locals 6

    .prologue
    and-int/lit8 v0, p6, 0x10

    if-eqz v0, :cond_0

    .line 41
    sget-object v5, Lkotlin/d/b/e;->c:Ljava/lang/Object;

    :goto_0
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, Lkotlin/reflect/jvm/internal/o;-><init>(Lkotlin/reflect/jvm/internal/n;Ljava/lang/String;Ljava/lang/String;Lkotlin/reflect/jvm/internal/impl/b/s;Ljava/lang/Object;)V

    return-void

    :cond_0
    move-object v5, p5

    goto :goto_0
.end method

.method public constructor <init>(Lkotlin/reflect/jvm/internal/n;Lkotlin/reflect/jvm/internal/impl/b/s;)V
    .locals 8

    .prologue
    const/4 v5, 0x0

    const-string v0, "container"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "descriptor"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 46
    invoke-interface {p2}, Lkotlin/reflect/jvm/internal/impl/b/s;->D_()Lkotlin/reflect/jvm/internal/impl/e/f;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/e/f;->a()Ljava/lang/String;

    move-result-object v2

    const-string v0, "descriptor.name.asString()"

    invoke-static {v2, v0}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v0, Lkotlin/reflect/jvm/internal/ai;->a:Lkotlin/reflect/jvm/internal/ai;

    invoke-virtual {v0, p2}, Lkotlin/reflect/jvm/internal/ai;->a(Lkotlin/reflect/jvm/internal/impl/b/s;)Lkotlin/reflect/jvm/internal/h;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/h;->a()Ljava/lang/String;

    move-result-object v3

    const/16 v6, 0x10

    move-object v0, p0

    move-object v1, p1

    move-object v4, p2

    move-object v7, v5

    invoke-direct/range {v0 .. v7}, Lkotlin/reflect/jvm/internal/o;-><init>(Lkotlin/reflect/jvm/internal/n;Ljava/lang/String;Ljava/lang/String;Lkotlin/reflect/jvm/internal/impl/b/s;Ljava/lang/Object;ILkotlin/d/b/i;)V

    return-void
.end method

.method public static final synthetic a(Lkotlin/reflect/jvm/internal/o;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/o;->f:Ljava/lang/String;

    return-object v0
.end method

.method private final a(Ljava/lang/reflect/Method;)Lkotlin/reflect/jvm/internal/f$w;
    .locals 2

    .prologue
    .line 141
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/o;->p()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Lkotlin/reflect/jvm/internal/f$j;

    iget-object v1, p0, Lkotlin/reflect/jvm/internal/o;->g:Ljava/lang/Object;

    invoke-direct {v0, p1, v1}, Lkotlin/reflect/jvm/internal/f$j;-><init>(Ljava/lang/reflect/Method;Ljava/lang/Object;)V

    check-cast v0, Lkotlin/reflect/jvm/internal/f$w;

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lkotlin/reflect/jvm/internal/f$z;

    invoke-direct {v0, p1}, Lkotlin/reflect/jvm/internal/f$z;-><init>(Ljava/lang/reflect/Method;)V

    check-cast v0, Lkotlin/reflect/jvm/internal/f$w;

    goto :goto_0
.end method

.method public static final synthetic a(Lkotlin/reflect/jvm/internal/o;Ljava/lang/reflect/Method;)Lkotlin/reflect/jvm/internal/f$w;
    .locals 1

    .prologue
    .line 36
    invoke-direct {p0, p1}, Lkotlin/reflect/jvm/internal/o;->c(Ljava/lang/reflect/Method;)Lkotlin/reflect/jvm/internal/f$w;

    move-result-object v0

    return-object v0
.end method

.method private final a(Ljava/lang/reflect/Constructor;)Lkotlin/reflect/jvm/internal/f;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/reflect/Constructor",
            "<*>;)",
            "Lkotlin/reflect/jvm/internal/f",
            "<",
            "Ljava/lang/reflect/Constructor",
            "<*>;>;"
        }
    .end annotation

    .prologue
    .line 150
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/o;->p()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Lkotlin/reflect/jvm/internal/f$c;

    iget-object v1, p0, Lkotlin/reflect/jvm/internal/o;->g:Ljava/lang/Object;

    invoke-direct {v0, p1, v1}, Lkotlin/reflect/jvm/internal/f$c;-><init>(Ljava/lang/reflect/Constructor;Ljava/lang/Object;)V

    check-cast v0, Lkotlin/reflect/jvm/internal/f;

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lkotlin/reflect/jvm/internal/f$n;

    invoke-direct {v0, p1}, Lkotlin/reflect/jvm/internal/f$n;-><init>(Ljava/lang/reflect/Constructor;)V

    check-cast v0, Lkotlin/reflect/jvm/internal/f;

    goto :goto_0
.end method

.method public static final synthetic a(Lkotlin/reflect/jvm/internal/o;Ljava/lang/reflect/Constructor;)Lkotlin/reflect/jvm/internal/f;
    .locals 1

    .prologue
    .line 36
    invoke-direct {p0, p1}, Lkotlin/reflect/jvm/internal/o;->a(Ljava/lang/reflect/Constructor;)Lkotlin/reflect/jvm/internal/f;

    move-result-object v0

    return-object v0
.end method

.method private final b(Ljava/lang/reflect/Method;)Lkotlin/reflect/jvm/internal/f$w;
    .locals 1

    .prologue
    .line 144
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/o;->p()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Lkotlin/reflect/jvm/internal/f$g;

    invoke-direct {v0, p1}, Lkotlin/reflect/jvm/internal/f$g;-><init>(Ljava/lang/reflect/Method;)V

    check-cast v0, Lkotlin/reflect/jvm/internal/f$w;

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lkotlin/reflect/jvm/internal/f$t;

    invoke-direct {v0, p1}, Lkotlin/reflect/jvm/internal/f$t;-><init>(Ljava/lang/reflect/Method;)V

    check-cast v0, Lkotlin/reflect/jvm/internal/f$w;

    goto :goto_0
.end method

.method public static final synthetic b(Lkotlin/reflect/jvm/internal/o;Ljava/lang/reflect/Method;)Lkotlin/reflect/jvm/internal/f$w;
    .locals 1

    .prologue
    .line 36
    invoke-direct {p0, p1}, Lkotlin/reflect/jvm/internal/o;->b(Ljava/lang/reflect/Method;)Lkotlin/reflect/jvm/internal/f$w;

    move-result-object v0

    return-object v0
.end method

.method public static final synthetic b(Lkotlin/reflect/jvm/internal/o;)Z
    .locals 1

    .prologue
    .line 36
    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/o;->l()Z

    move-result v0

    return v0
.end method

.method private final c(Ljava/lang/reflect/Method;)Lkotlin/reflect/jvm/internal/f$w;
    .locals 2

    .prologue
    .line 147
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/o;->p()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Lkotlin/reflect/jvm/internal/f$f;

    iget-object v1, p0, Lkotlin/reflect/jvm/internal/o;->g:Ljava/lang/Object;

    invoke-direct {v0, p1, v1}, Lkotlin/reflect/jvm/internal/f$f;-><init>(Ljava/lang/reflect/Method;Ljava/lang/Object;)V

    check-cast v0, Lkotlin/reflect/jvm/internal/f$w;

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lkotlin/reflect/jvm/internal/f$s;

    invoke-direct {v0, p1}, Lkotlin/reflect/jvm/internal/f$s;-><init>(Ljava/lang/reflect/Method;)V

    check-cast v0, Lkotlin/reflect/jvm/internal/f$w;

    goto :goto_0
.end method

.method public static final synthetic c(Lkotlin/reflect/jvm/internal/o;Ljava/lang/reflect/Method;)Lkotlin/reflect/jvm/internal/f$w;
    .locals 1

    .prologue
    .line 36
    invoke-direct {p0, p1}, Lkotlin/reflect/jvm/internal/o;->a(Ljava/lang/reflect/Method;)Lkotlin/reflect/jvm/internal/f$w;

    move-result-object v0

    return-object v0
.end method

.method private final k()Z
    .locals 1

    .prologue
    .line 63
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/o;->c()Lkotlin/reflect/jvm/internal/impl/b/s;

    move-result-object v0

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/b/s;->p()Lkotlin/reflect/jvm/internal/impl/b/ay;

    move-result-object v0

    .line 62
    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/b/ax;->a(Lkotlin/reflect/jvm/internal/impl/b/ay;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 63
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/o;->c()Lkotlin/reflect/jvm/internal/impl/b/s;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/t;

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/b/a/f;->a(Lkotlin/reflect/jvm/internal/impl/b/t;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private final l()Z
    .locals 1

    .prologue
    .line 65
    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/o;->k()Z

    move-result v0

    return v0
.end method


# virtual methods
.method public a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 36
    invoke-static {p0, p1}, Lkotlin/reflect/jvm/internal/g$a;->a(Lkotlin/reflect/jvm/internal/g;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 36
    invoke-static {p0, p1, p2}, Lkotlin/reflect/jvm/internal/g$a;->a(Lkotlin/reflect/jvm/internal/g;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 36
    invoke-static {p0, p1, p2, p3}, Lkotlin/reflect/jvm/internal/g$a;->a(Lkotlin/reflect/jvm/internal/g;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 36
    invoke-static {p0, p1, p2, p3, p4}, Lkotlin/reflect/jvm/internal/g$a;->a(Lkotlin/reflect/jvm/internal/g;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public b()Ljava/lang/String;
    .locals 2

    .prologue
    .line 59
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/o;->c()Lkotlin/reflect/jvm/internal/impl/b/s;

    move-result-object v0

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/b/s;->D_()Lkotlin/reflect/jvm/internal/impl/e/f;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/e/f;->a()Ljava/lang/String;

    move-result-object v0

    const-string v1, "descriptor.name.asString()"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public c()Lkotlin/reflect/jvm/internal/impl/b/s;
    .locals 3

    iget-object v0, p0, Lkotlin/reflect/jvm/internal/o;->b:Lkotlin/reflect/jvm/internal/ae$a;

    sget-object v1, Lkotlin/reflect/jvm/internal/o;->a:[Lkotlin/reflect/l;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-virtual {v0, p0, v1}, Lkotlin/reflect/jvm/internal/ae$a;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/s;

    return-object v0
.end method

.method public synthetic d()Lkotlin/reflect/jvm/internal/impl/b/b;
    .locals 1

    .prologue
    .line 36
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/o;->c()Lkotlin/reflect/jvm/internal/impl/b/s;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/b;

    return-object v0
.end method

.method public e()I
    .locals 1

    .prologue
    .line 152
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/o;->f()Lkotlin/reflect/jvm/internal/f;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/f;->b()I

    move-result v0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 170
    invoke-static {p1}, Lkotlin/reflect/jvm/internal/ak;->a(Ljava/lang/Object;)Lkotlin/reflect/jvm/internal/o;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 171
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/o;->o()Lkotlin/reflect/jvm/internal/n;

    move-result-object v2

    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/o;->o()Lkotlin/reflect/jvm/internal/n;

    move-result-object v3

    invoke-static {v2, v3}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/o;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/o;->b()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lkotlin/reflect/jvm/internal/o;->f:Ljava/lang/String;

    iget-object v3, v1, Lkotlin/reflect/jvm/internal/o;->f:Ljava/lang/String;

    invoke-static {v2, v3}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lkotlin/reflect/jvm/internal/o;->g:Ljava/lang/Object;

    iget-object v1, v1, Lkotlin/reflect/jvm/internal/o;->g:Ljava/lang/Object;

    invoke-static {v2, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method public f()Lkotlin/reflect/jvm/internal/f;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/reflect/jvm/internal/f",
            "<*>;"
        }
    .end annotation

    iget-object v0, p0, Lkotlin/reflect/jvm/internal/o;->c:Lkotlin/reflect/jvm/internal/ae$a;

    sget-object v1, Lkotlin/reflect/jvm/internal/o;->a:[Lkotlin/reflect/l;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-virtual {v0, p0, v1}, Lkotlin/reflect/jvm/internal/ae$a;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/f;

    return-object v0
.end method

.method public g()Lkotlin/reflect/jvm/internal/f;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/reflect/jvm/internal/f",
            "<*>;"
        }
    .end annotation

    iget-object v0, p0, Lkotlin/reflect/jvm/internal/o;->d:Lkotlin/reflect/jvm/internal/ae$a;

    sget-object v1, Lkotlin/reflect/jvm/internal/o;->a:[Lkotlin/reflect/l;

    const/4 v2, 0x2

    aget-object v1, v1, v2

    invoke-virtual {v0, p0, v1}, Lkotlin/reflect/jvm/internal/ae$a;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/f;

    return-object v0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 175
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/o;->o()Lkotlin/reflect/jvm/internal/n;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/n;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/o;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lkotlin/reflect/jvm/internal/o;->f:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public o()Lkotlin/reflect/jvm/internal/n;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/o;->e:Lkotlin/reflect/jvm/internal/n;

    return-object v0
.end method

.method public p()Z
    .locals 2

    .prologue
    .line 53
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/o;->g:Ljava/lang/Object;

    sget-object v1, Lkotlin/d/b/e;->c:Ljava/lang/Object;

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 178
    sget-object v0, Lkotlin/reflect/jvm/internal/ag;->a:Lkotlin/reflect/jvm/internal/ag;

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/o;->c()Lkotlin/reflect/jvm/internal/impl/b/s;

    move-result-object v1

    invoke-virtual {v0, v1}, Lkotlin/reflect/jvm/internal/ag;->a(Lkotlin/reflect/jvm/internal/impl/b/s;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public v_()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 36
    invoke-static {p0}, Lkotlin/reflect/jvm/internal/g$a;->a(Lkotlin/reflect/jvm/internal/g;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method
