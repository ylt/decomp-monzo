.class public final Lkotlin/reflect/jvm/internal/l$a;
.super Lkotlin/reflect/jvm/internal/n$b;
.source "KClassImpl.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lkotlin/reflect/jvm/internal/l;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x11
    name = "a"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x1
    }
    d1 = {
        "\u0000^\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u001e\n\u0002\u0018\u0002\n\u0002\u0008\u000b\n\u0002\u0010 \n\u0002\u0010\u001b\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u000e\n\u0002\u0018\u0002\n\u0002\u0008\n\n\u0002\u0018\u0002\n\u0002\u0008\t\n\u0002\u0010\u000e\n\u0002\u0008\u0007\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0000\u0008\u0086\u0004\u0018\u00002\u00060\u0001R\u00020\u0002B\u0005\u00a2\u0006\u0002\u0010\u0003J\u0014\u0010K\u001a\u00020<2\n\u0010L\u001a\u0006\u0012\u0002\u0008\u00030MH\u0002R%\u0010\u0004\u001a\u000c\u0012\u0008\u0012\u0006\u0012\u0002\u0008\u00030\u00060\u00058FX\u0086\u0084\u0002\u00a2\u0006\u000c\n\u0004\u0008\t\u0010\n\u001a\u0004\u0008\u0007\u0010\u0008R%\u0010\u000b\u001a\u000c\u0012\u0008\u0012\u0006\u0012\u0002\u0008\u00030\u00060\u00058FX\u0086\u0084\u0002\u00a2\u0006\u000c\n\u0004\u0008\r\u0010\n\u001a\u0004\u0008\u000c\u0010\u0008R%\u0010\u000e\u001a\u000c\u0012\u0008\u0012\u0006\u0012\u0002\u0008\u00030\u00060\u00058FX\u0086\u0084\u0002\u00a2\u0006\u000c\n\u0004\u0008\u0010\u0010\n\u001a\u0004\u0008\u000f\u0010\u0008R!\u0010\u0011\u001a\u0008\u0012\u0004\u0012\u00020\u00130\u00128FX\u0086\u0084\u0002\u00a2\u0006\u000c\n\u0004\u0008\u0016\u0010\n\u001a\u0004\u0008\u0014\u0010\u0015R-\u0010\u0017\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00028\u00000\u00180\u00058FX\u0087\u0084\u0002\u00a2\u0006\u0012\n\u0004\u0008\u001c\u0010\n\u0012\u0004\u0008\u0019\u0010\u001a\u001a\u0004\u0008\u001b\u0010\u0008R%\u0010\u001d\u001a\u000c\u0012\u0008\u0012\u0006\u0012\u0002\u0008\u00030\u00060\u00058FX\u0086\u0084\u0002\u00a2\u0006\u000c\n\u0004\u0008\u001f\u0010\n\u001a\u0004\u0008\u001e\u0010\u0008R%\u0010 \u001a\u000c\u0012\u0008\u0012\u0006\u0012\u0002\u0008\u00030\u00060\u00058FX\u0086\u0084\u0002\u00a2\u0006\u000c\n\u0004\u0008\"\u0010\n\u001a\u0004\u0008!\u0010\u0008R%\u0010#\u001a\u000c\u0012\u0008\u0012\u0006\u0012\u0002\u0008\u00030\u00060\u00058FX\u0086\u0084\u0002\u00a2\u0006\u000c\n\u0004\u0008%\u0010\n\u001a\u0004\u0008$\u0010\u0008R\u001b\u0010&\u001a\u00020\'8FX\u0086\u0084\u0002\u00a2\u0006\u000c\n\u0004\u0008*\u0010\n\u001a\u0004\u0008(\u0010)R%\u0010+\u001a\u000c\u0012\u0008\u0012\u0006\u0012\u0002\u0008\u00030\u00060\u00058FX\u0086\u0084\u0002\u00a2\u0006\u000c\n\u0004\u0008-\u0010\n\u001a\u0004\u0008,\u0010\u0008R%\u0010.\u001a\u000c\u0012\u0008\u0012\u0006\u0012\u0002\u0008\u00030\u00060\u00058FX\u0086\u0084\u0002\u00a2\u0006\u000c\n\u0004\u00080\u0010\n\u001a\u0004\u0008/\u0010\u0008R%\u00101\u001a\u000c\u0012\u0008\u0012\u0006\u0012\u0002\u0008\u0003020\u00058FX\u0086\u0084\u0002\u00a2\u0006\u000c\n\u0004\u00084\u0010\n\u001a\u0004\u00083\u0010\u0008R#\u00105\u001a\u0004\u0018\u00018\u00008FX\u0087\u0084\u0002\u00a2\u0006\u0012\n\u0004\u00089\u0010:\u0012\u0004\u00086\u0010\u001a\u001a\u0004\u00087\u00108R\u001d\u0010;\u001a\u0004\u0018\u00010<8FX\u0086\u0084\u0002\u00a2\u0006\u000c\n\u0004\u0008?\u0010\n\u001a\u0004\u0008=\u0010>R\u001d\u0010@\u001a\u0004\u0018\u00010<8FX\u0086\u0084\u0002\u00a2\u0006\u000c\n\u0004\u0008B\u0010\n\u001a\u0004\u0008A\u0010>R!\u0010C\u001a\u0008\u0012\u0004\u0012\u00020D0\u00128FX\u0086\u0084\u0002\u00a2\u0006\u000c\n\u0004\u0008F\u0010\n\u001a\u0004\u0008E\u0010\u0015R!\u0010G\u001a\u0008\u0012\u0004\u0012\u00020H0\u00128FX\u0086\u0084\u0002\u00a2\u0006\u000c\n\u0004\u0008J\u0010\n\u001a\u0004\u0008I\u0010\u0015\u00a8\u0006N"
    }
    d2 = {
        "Lkotlin/reflect/jvm/internal/KClassImpl$Data;",
        "Lkotlin/reflect/jvm/internal/KDeclarationContainerImpl$Data;",
        "Lkotlin/reflect/jvm/internal/KDeclarationContainerImpl;",
        "(Lkotlin/reflect/jvm/internal/KClassImpl;)V",
        "allMembers",
        "",
        "Lkotlin/reflect/jvm/internal/KCallableImpl;",
        "getAllMembers",
        "()Ljava/util/Collection;",
        "allMembers$delegate",
        "Lkotlin/reflect/jvm/internal/ReflectProperties$LazySoftVal;",
        "allNonStaticMembers",
        "getAllNonStaticMembers",
        "allNonStaticMembers$delegate",
        "allStaticMembers",
        "getAllStaticMembers",
        "allStaticMembers$delegate",
        "annotations",
        "",
        "",
        "getAnnotations",
        "()Ljava/util/List;",
        "annotations$delegate",
        "constructors",
        "Lkotlin/reflect/KFunction;",
        "constructors$annotations",
        "()V",
        "getConstructors",
        "constructors$delegate",
        "declaredMembers",
        "getDeclaredMembers",
        "declaredMembers$delegate",
        "declaredNonStaticMembers",
        "getDeclaredNonStaticMembers",
        "declaredNonStaticMembers$delegate",
        "declaredStaticMembers",
        "getDeclaredStaticMembers",
        "declaredStaticMembers$delegate",
        "descriptor",
        "Lorg/jetbrains/kotlin/descriptors/ClassDescriptor;",
        "getDescriptor",
        "()Lorg/jetbrains/kotlin/descriptors/ClassDescriptor;",
        "descriptor$delegate",
        "inheritedNonStaticMembers",
        "getInheritedNonStaticMembers",
        "inheritedNonStaticMembers$delegate",
        "inheritedStaticMembers",
        "getInheritedStaticMembers",
        "inheritedStaticMembers$delegate",
        "nestedClasses",
        "Lkotlin/reflect/KClass;",
        "getNestedClasses",
        "nestedClasses$delegate",
        "objectInstance",
        "objectInstance$annotations",
        "getObjectInstance",
        "()Ljava/lang/Object;",
        "objectInstance$delegate",
        "Lkotlin/reflect/jvm/internal/ReflectProperties$LazyVal;",
        "qualifiedName",
        "",
        "getQualifiedName",
        "()Ljava/lang/String;",
        "qualifiedName$delegate",
        "simpleName",
        "getSimpleName",
        "simpleName$delegate",
        "supertypes",
        "Lkotlin/reflect/KType;",
        "getSupertypes",
        "supertypes$delegate",
        "typeParameters",
        "Lkotlin/reflect/KTypeParameter;",
        "getTypeParameters",
        "typeParameters$delegate",
        "calculateLocalClassName",
        "jClass",
        "Ljava/lang/Class;",
        "kotlin-reflection"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x5
    }
.end annotation


# static fields
.field static final synthetic a:[Lkotlin/reflect/l;


# instance fields
.field final synthetic b:Lkotlin/reflect/jvm/internal/l;

.field private final e:Lkotlin/reflect/jvm/internal/ae$a;

.field private final f:Lkotlin/reflect/jvm/internal/ae$a;

.field private final g:Lkotlin/reflect/jvm/internal/ae$a;

.field private final h:Lkotlin/reflect/jvm/internal/ae$a;

.field private final i:Lkotlin/reflect/jvm/internal/ae$a;

.field private final j:Lkotlin/reflect/jvm/internal/ae$a;

.field private final k:Lkotlin/reflect/jvm/internal/ae$b;

.field private final l:Lkotlin/reflect/jvm/internal/ae$a;

.field private final m:Lkotlin/reflect/jvm/internal/ae$a;

.field private final n:Lkotlin/reflect/jvm/internal/ae$a;

.field private final o:Lkotlin/reflect/jvm/internal/ae$a;

.field private final p:Lkotlin/reflect/jvm/internal/ae$a;

.field private final q:Lkotlin/reflect/jvm/internal/ae$a;

.field private final r:Lkotlin/reflect/jvm/internal/ae$a;

.field private final s:Lkotlin/reflect/jvm/internal/ae$a;

.field private final t:Lkotlin/reflect/jvm/internal/ae$a;

.field private final u:Lkotlin/reflect/jvm/internal/ae$a;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const/16 v0, 0x11

    new-array v1, v0, [Lkotlin/reflect/l;

    const/4 v2, 0x0

    new-instance v0, Lkotlin/d/b/w;

    const-class v3, Lkotlin/reflect/jvm/internal/l$a;

    invoke-static {v3}, Lkotlin/d/b/y;->a(Ljava/lang/Class;)Lkotlin/reflect/c;

    move-result-object v3

    const-string v4, "descriptor"

    const-string v5, "getDescriptor()Lorg/jetbrains/kotlin/descriptors/ClassDescriptor;"

    invoke-direct {v0, v3, v4, v5}, Lkotlin/d/b/w;-><init>(Lkotlin/reflect/e;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v0}, Lkotlin/d/b/y;->a(Lkotlin/d/b/v;)Lkotlin/reflect/n;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/l;

    aput-object v0, v1, v2

    const/4 v2, 0x1

    new-instance v0, Lkotlin/d/b/w;

    const-class v3, Lkotlin/reflect/jvm/internal/l$a;

    invoke-static {v3}, Lkotlin/d/b/y;->a(Ljava/lang/Class;)Lkotlin/reflect/c;

    move-result-object v3

    const-string v4, "annotations"

    const-string v5, "getAnnotations()Ljava/util/List;"

    invoke-direct {v0, v3, v4, v5}, Lkotlin/d/b/w;-><init>(Lkotlin/reflect/e;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v0}, Lkotlin/d/b/y;->a(Lkotlin/d/b/v;)Lkotlin/reflect/n;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/l;

    aput-object v0, v1, v2

    const/4 v2, 0x2

    new-instance v0, Lkotlin/d/b/w;

    const-class v3, Lkotlin/reflect/jvm/internal/l$a;

    invoke-static {v3}, Lkotlin/d/b/y;->a(Ljava/lang/Class;)Lkotlin/reflect/c;

    move-result-object v3

    const-string v4, "simpleName"

    const-string v5, "getSimpleName()Ljava/lang/String;"

    invoke-direct {v0, v3, v4, v5}, Lkotlin/d/b/w;-><init>(Lkotlin/reflect/e;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v0}, Lkotlin/d/b/y;->a(Lkotlin/d/b/v;)Lkotlin/reflect/n;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/l;

    aput-object v0, v1, v2

    const/4 v2, 0x3

    new-instance v0, Lkotlin/d/b/w;

    const-class v3, Lkotlin/reflect/jvm/internal/l$a;

    invoke-static {v3}, Lkotlin/d/b/y;->a(Ljava/lang/Class;)Lkotlin/reflect/c;

    move-result-object v3

    const-string v4, "qualifiedName"

    const-string v5, "getQualifiedName()Ljava/lang/String;"

    invoke-direct {v0, v3, v4, v5}, Lkotlin/d/b/w;-><init>(Lkotlin/reflect/e;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v0}, Lkotlin/d/b/y;->a(Lkotlin/d/b/v;)Lkotlin/reflect/n;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/l;

    aput-object v0, v1, v2

    const/4 v2, 0x4

    new-instance v0, Lkotlin/d/b/w;

    const-class v3, Lkotlin/reflect/jvm/internal/l$a;

    invoke-static {v3}, Lkotlin/d/b/y;->a(Ljava/lang/Class;)Lkotlin/reflect/c;

    move-result-object v3

    const-string v4, "constructors"

    const-string v5, "getConstructors()Ljava/util/Collection;"

    invoke-direct {v0, v3, v4, v5}, Lkotlin/d/b/w;-><init>(Lkotlin/reflect/e;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v0}, Lkotlin/d/b/y;->a(Lkotlin/d/b/v;)Lkotlin/reflect/n;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/l;

    aput-object v0, v1, v2

    const/4 v2, 0x5

    new-instance v0, Lkotlin/d/b/w;

    const-class v3, Lkotlin/reflect/jvm/internal/l$a;

    invoke-static {v3}, Lkotlin/d/b/y;->a(Ljava/lang/Class;)Lkotlin/reflect/c;

    move-result-object v3

    const-string v4, "nestedClasses"

    const-string v5, "getNestedClasses()Ljava/util/Collection;"

    invoke-direct {v0, v3, v4, v5}, Lkotlin/d/b/w;-><init>(Lkotlin/reflect/e;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v0}, Lkotlin/d/b/y;->a(Lkotlin/d/b/v;)Lkotlin/reflect/n;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/l;

    aput-object v0, v1, v2

    const/4 v2, 0x6

    new-instance v0, Lkotlin/d/b/w;

    const-class v3, Lkotlin/reflect/jvm/internal/l$a;

    invoke-static {v3}, Lkotlin/d/b/y;->a(Ljava/lang/Class;)Lkotlin/reflect/c;

    move-result-object v3

    const-string v4, "objectInstance"

    const-string v5, "getObjectInstance()Ljava/lang/Object;"

    invoke-direct {v0, v3, v4, v5}, Lkotlin/d/b/w;-><init>(Lkotlin/reflect/e;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v0}, Lkotlin/d/b/y;->a(Lkotlin/d/b/v;)Lkotlin/reflect/n;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/l;

    aput-object v0, v1, v2

    const/4 v2, 0x7

    new-instance v0, Lkotlin/d/b/w;

    const-class v3, Lkotlin/reflect/jvm/internal/l$a;

    invoke-static {v3}, Lkotlin/d/b/y;->a(Ljava/lang/Class;)Lkotlin/reflect/c;

    move-result-object v3

    const-string v4, "typeParameters"

    const-string v5, "getTypeParameters()Ljava/util/List;"

    invoke-direct {v0, v3, v4, v5}, Lkotlin/d/b/w;-><init>(Lkotlin/reflect/e;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v0}, Lkotlin/d/b/y;->a(Lkotlin/d/b/v;)Lkotlin/reflect/n;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/l;

    aput-object v0, v1, v2

    const/16 v2, 0x8

    new-instance v0, Lkotlin/d/b/w;

    const-class v3, Lkotlin/reflect/jvm/internal/l$a;

    invoke-static {v3}, Lkotlin/d/b/y;->a(Ljava/lang/Class;)Lkotlin/reflect/c;

    move-result-object v3

    const-string v4, "supertypes"

    const-string v5, "getSupertypes()Ljava/util/List;"

    invoke-direct {v0, v3, v4, v5}, Lkotlin/d/b/w;-><init>(Lkotlin/reflect/e;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v0}, Lkotlin/d/b/y;->a(Lkotlin/d/b/v;)Lkotlin/reflect/n;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/l;

    aput-object v0, v1, v2

    const/16 v2, 0x9

    new-instance v0, Lkotlin/d/b/w;

    const-class v3, Lkotlin/reflect/jvm/internal/l$a;

    invoke-static {v3}, Lkotlin/d/b/y;->a(Ljava/lang/Class;)Lkotlin/reflect/c;

    move-result-object v3

    const-string v4, "declaredNonStaticMembers"

    const-string v5, "getDeclaredNonStaticMembers()Ljava/util/Collection;"

    invoke-direct {v0, v3, v4, v5}, Lkotlin/d/b/w;-><init>(Lkotlin/reflect/e;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v0}, Lkotlin/d/b/y;->a(Lkotlin/d/b/v;)Lkotlin/reflect/n;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/l;

    aput-object v0, v1, v2

    const/16 v2, 0xa

    new-instance v0, Lkotlin/d/b/w;

    const-class v3, Lkotlin/reflect/jvm/internal/l$a;

    invoke-static {v3}, Lkotlin/d/b/y;->a(Ljava/lang/Class;)Lkotlin/reflect/c;

    move-result-object v3

    const-string v4, "declaredStaticMembers"

    const-string v5, "getDeclaredStaticMembers()Ljava/util/Collection;"

    invoke-direct {v0, v3, v4, v5}, Lkotlin/d/b/w;-><init>(Lkotlin/reflect/e;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v0}, Lkotlin/d/b/y;->a(Lkotlin/d/b/v;)Lkotlin/reflect/n;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/l;

    aput-object v0, v1, v2

    const/16 v2, 0xb

    new-instance v0, Lkotlin/d/b/w;

    const-class v3, Lkotlin/reflect/jvm/internal/l$a;

    invoke-static {v3}, Lkotlin/d/b/y;->a(Ljava/lang/Class;)Lkotlin/reflect/c;

    move-result-object v3

    const-string v4, "inheritedNonStaticMembers"

    const-string v5, "getInheritedNonStaticMembers()Ljava/util/Collection;"

    invoke-direct {v0, v3, v4, v5}, Lkotlin/d/b/w;-><init>(Lkotlin/reflect/e;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v0}, Lkotlin/d/b/y;->a(Lkotlin/d/b/v;)Lkotlin/reflect/n;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/l;

    aput-object v0, v1, v2

    const/16 v2, 0xc

    new-instance v0, Lkotlin/d/b/w;

    const-class v3, Lkotlin/reflect/jvm/internal/l$a;

    invoke-static {v3}, Lkotlin/d/b/y;->a(Ljava/lang/Class;)Lkotlin/reflect/c;

    move-result-object v3

    const-string v4, "inheritedStaticMembers"

    const-string v5, "getInheritedStaticMembers()Ljava/util/Collection;"

    invoke-direct {v0, v3, v4, v5}, Lkotlin/d/b/w;-><init>(Lkotlin/reflect/e;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v0}, Lkotlin/d/b/y;->a(Lkotlin/d/b/v;)Lkotlin/reflect/n;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/l;

    aput-object v0, v1, v2

    const/16 v2, 0xd

    new-instance v0, Lkotlin/d/b/w;

    const-class v3, Lkotlin/reflect/jvm/internal/l$a;

    invoke-static {v3}, Lkotlin/d/b/y;->a(Ljava/lang/Class;)Lkotlin/reflect/c;

    move-result-object v3

    const-string v4, "allNonStaticMembers"

    const-string v5, "getAllNonStaticMembers()Ljava/util/Collection;"

    invoke-direct {v0, v3, v4, v5}, Lkotlin/d/b/w;-><init>(Lkotlin/reflect/e;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v0}, Lkotlin/d/b/y;->a(Lkotlin/d/b/v;)Lkotlin/reflect/n;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/l;

    aput-object v0, v1, v2

    const/16 v2, 0xe

    new-instance v0, Lkotlin/d/b/w;

    const-class v3, Lkotlin/reflect/jvm/internal/l$a;

    invoke-static {v3}, Lkotlin/d/b/y;->a(Ljava/lang/Class;)Lkotlin/reflect/c;

    move-result-object v3

    const-string v4, "allStaticMembers"

    const-string v5, "getAllStaticMembers()Ljava/util/Collection;"

    invoke-direct {v0, v3, v4, v5}, Lkotlin/d/b/w;-><init>(Lkotlin/reflect/e;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v0}, Lkotlin/d/b/y;->a(Lkotlin/d/b/v;)Lkotlin/reflect/n;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/l;

    aput-object v0, v1, v2

    const/16 v2, 0xf

    new-instance v0, Lkotlin/d/b/w;

    const-class v3, Lkotlin/reflect/jvm/internal/l$a;

    invoke-static {v3}, Lkotlin/d/b/y;->a(Ljava/lang/Class;)Lkotlin/reflect/c;

    move-result-object v3

    const-string v4, "declaredMembers"

    const-string v5, "getDeclaredMembers()Ljava/util/Collection;"

    invoke-direct {v0, v3, v4, v5}, Lkotlin/d/b/w;-><init>(Lkotlin/reflect/e;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v0}, Lkotlin/d/b/y;->a(Lkotlin/d/b/v;)Lkotlin/reflect/n;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/l;

    aput-object v0, v1, v2

    const/16 v2, 0x10

    new-instance v0, Lkotlin/d/b/w;

    const-class v3, Lkotlin/reflect/jvm/internal/l$a;

    invoke-static {v3}, Lkotlin/d/b/y;->a(Ljava/lang/Class;)Lkotlin/reflect/c;

    move-result-object v3

    const-string v4, "allMembers"

    const-string v5, "getAllMembers()Ljava/util/Collection;"

    invoke-direct {v0, v3, v4, v5}, Lkotlin/d/b/w;-><init>(Lkotlin/reflect/e;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v0}, Lkotlin/d/b/y;->a(Lkotlin/d/b/v;)Lkotlin/reflect/n;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/l;

    aput-object v0, v1, v2

    sput-object v1, Lkotlin/reflect/jvm/internal/l$a;->a:[Lkotlin/reflect/l;

    return-void
.end method

.method public constructor <init>(Lkotlin/reflect/jvm/internal/l;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 39
    iput-object p1, p0, Lkotlin/reflect/jvm/internal/l$a;->b:Lkotlin/reflect/jvm/internal/l;

    invoke-direct {p0, p1}, Lkotlin/reflect/jvm/internal/n$b;-><init>(Lkotlin/reflect/jvm/internal/n;)V

    .line 40
    new-instance v0, Lkotlin/reflect/jvm/internal/l$a$i;

    invoke-direct {v0, p0}, Lkotlin/reflect/jvm/internal/l$a$i;-><init>(Lkotlin/reflect/jvm/internal/l$a;)V

    check-cast v0, Lkotlin/d/a/a;

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/ae;->b(Lkotlin/d/a/a;)Lkotlin/reflect/jvm/internal/ae$a;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/l$a;->e:Lkotlin/reflect/jvm/internal/ae$a;

    .line 51
    new-instance v0, Lkotlin/reflect/jvm/internal/l$a$d;

    invoke-direct {v0, p0}, Lkotlin/reflect/jvm/internal/l$a$d;-><init>(Lkotlin/reflect/jvm/internal/l$a;)V

    check-cast v0, Lkotlin/d/a/a;

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/ae;->b(Lkotlin/d/a/a;)Lkotlin/reflect/jvm/internal/ae$a;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/l$a;->f:Lkotlin/reflect/jvm/internal/ae$a;

    .line 53
    new-instance v0, Lkotlin/reflect/jvm/internal/l$a$o;

    invoke-direct {v0, p0}, Lkotlin/reflect/jvm/internal/l$a$o;-><init>(Lkotlin/reflect/jvm/internal/l$a;)V

    check-cast v0, Lkotlin/d/a/a;

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/ae;->b(Lkotlin/d/a/a;)Lkotlin/reflect/jvm/internal/ae$a;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/l$a;->g:Lkotlin/reflect/jvm/internal/ae$a;

    .line 63
    new-instance v0, Lkotlin/reflect/jvm/internal/l$a$n;

    invoke-direct {v0, p0}, Lkotlin/reflect/jvm/internal/l$a$n;-><init>(Lkotlin/reflect/jvm/internal/l$a;)V

    check-cast v0, Lkotlin/d/a/a;

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/ae;->b(Lkotlin/d/a/a;)Lkotlin/reflect/jvm/internal/ae$a;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/l$a;->h:Lkotlin/reflect/jvm/internal/ae$a;

    .line 85
    new-instance v0, Lkotlin/reflect/jvm/internal/l$a$e;

    invoke-direct {v0, p0}, Lkotlin/reflect/jvm/internal/l$a$e;-><init>(Lkotlin/reflect/jvm/internal/l$a;)V

    check-cast v0, Lkotlin/d/a/a;

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/ae;->b(Lkotlin/d/a/a;)Lkotlin/reflect/jvm/internal/ae$a;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/l$a;->i:Lkotlin/reflect/jvm/internal/ae$a;

    .line 91
    new-instance v0, Lkotlin/reflect/jvm/internal/l$a$l;

    invoke-direct {v0, p0}, Lkotlin/reflect/jvm/internal/l$a$l;-><init>(Lkotlin/reflect/jvm/internal/l$a;)V

    check-cast v0, Lkotlin/d/a/a;

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/ae;->b(Lkotlin/d/a/a;)Lkotlin/reflect/jvm/internal/ae$a;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/l$a;->j:Lkotlin/reflect/jvm/internal/ae$a;

    .line 100
    new-instance v0, Lkotlin/reflect/jvm/internal/l$a$m;

    invoke-direct {v0, p0}, Lkotlin/reflect/jvm/internal/l$a$m;-><init>(Lkotlin/reflect/jvm/internal/l$a;)V

    check-cast v0, Lkotlin/d/a/a;

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/ae;->a(Lkotlin/d/a/a;)Lkotlin/reflect/jvm/internal/ae$b;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/l$a;->k:Lkotlin/reflect/jvm/internal/ae$b;

    .line 113
    new-instance v0, Lkotlin/reflect/jvm/internal/l$a$q;

    invoke-direct {v0, p0}, Lkotlin/reflect/jvm/internal/l$a$q;-><init>(Lkotlin/reflect/jvm/internal/l$a;)V

    check-cast v0, Lkotlin/d/a/a;

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/ae;->b(Lkotlin/d/a/a;)Lkotlin/reflect/jvm/internal/ae$a;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/l$a;->l:Lkotlin/reflect/jvm/internal/ae$a;

    .line 117
    new-instance v0, Lkotlin/reflect/jvm/internal/l$a$p;

    invoke-direct {v0, p0}, Lkotlin/reflect/jvm/internal/l$a$p;-><init>(Lkotlin/reflect/jvm/internal/l$a;)V

    check-cast v0, Lkotlin/d/a/a;

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/ae;->b(Lkotlin/d/a/a;)Lkotlin/reflect/jvm/internal/ae$a;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/l$a;->m:Lkotlin/reflect/jvm/internal/ae$a;

    .line 139
    new-instance v0, Lkotlin/reflect/jvm/internal/l$a$g;

    invoke-direct {v0, p0}, Lkotlin/reflect/jvm/internal/l$a$g;-><init>(Lkotlin/reflect/jvm/internal/l$a;)V

    check-cast v0, Lkotlin/d/a/a;

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/ae;->b(Lkotlin/d/a/a;)Lkotlin/reflect/jvm/internal/ae$a;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/l$a;->n:Lkotlin/reflect/jvm/internal/ae$a;

    .line 141
    new-instance v0, Lkotlin/reflect/jvm/internal/l$a$h;

    invoke-direct {v0, p0}, Lkotlin/reflect/jvm/internal/l$a$h;-><init>(Lkotlin/reflect/jvm/internal/l$a;)V

    check-cast v0, Lkotlin/d/a/a;

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/ae;->b(Lkotlin/d/a/a;)Lkotlin/reflect/jvm/internal/ae$a;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/l$a;->o:Lkotlin/reflect/jvm/internal/ae$a;

    .line 143
    new-instance v0, Lkotlin/reflect/jvm/internal/l$a$j;

    invoke-direct {v0, p0}, Lkotlin/reflect/jvm/internal/l$a$j;-><init>(Lkotlin/reflect/jvm/internal/l$a;)V

    check-cast v0, Lkotlin/d/a/a;

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/ae;->b(Lkotlin/d/a/a;)Lkotlin/reflect/jvm/internal/ae$a;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/l$a;->p:Lkotlin/reflect/jvm/internal/ae$a;

    .line 145
    new-instance v0, Lkotlin/reflect/jvm/internal/l$a$k;

    invoke-direct {v0, p0}, Lkotlin/reflect/jvm/internal/l$a$k;-><init>(Lkotlin/reflect/jvm/internal/l$a;)V

    check-cast v0, Lkotlin/d/a/a;

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/ae;->b(Lkotlin/d/a/a;)Lkotlin/reflect/jvm/internal/ae$a;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/l$a;->q:Lkotlin/reflect/jvm/internal/ae$a;

    .line 148
    new-instance v0, Lkotlin/reflect/jvm/internal/l$a$b;

    invoke-direct {v0, p0}, Lkotlin/reflect/jvm/internal/l$a$b;-><init>(Lkotlin/reflect/jvm/internal/l$a;)V

    check-cast v0, Lkotlin/d/a/a;

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/ae;->b(Lkotlin/d/a/a;)Lkotlin/reflect/jvm/internal/ae$a;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/l$a;->r:Lkotlin/reflect/jvm/internal/ae$a;

    .line 150
    new-instance v0, Lkotlin/reflect/jvm/internal/l$a$c;

    invoke-direct {v0, p0}, Lkotlin/reflect/jvm/internal/l$a$c;-><init>(Lkotlin/reflect/jvm/internal/l$a;)V

    check-cast v0, Lkotlin/d/a/a;

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/ae;->b(Lkotlin/d/a/a;)Lkotlin/reflect/jvm/internal/ae$a;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/l$a;->s:Lkotlin/reflect/jvm/internal/ae$a;

    .line 152
    new-instance v0, Lkotlin/reflect/jvm/internal/l$a$f;

    invoke-direct {v0, p0}, Lkotlin/reflect/jvm/internal/l$a$f;-><init>(Lkotlin/reflect/jvm/internal/l$a;)V

    check-cast v0, Lkotlin/d/a/a;

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/ae;->b(Lkotlin/d/a/a;)Lkotlin/reflect/jvm/internal/ae$a;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/l$a;->t:Lkotlin/reflect/jvm/internal/ae$a;

    .line 154
    new-instance v0, Lkotlin/reflect/jvm/internal/l$a$a;

    invoke-direct {v0, p0}, Lkotlin/reflect/jvm/internal/l$a$a;-><init>(Lkotlin/reflect/jvm/internal/l$a;)V

    check-cast v0, Lkotlin/d/a/a;

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/ae;->b(Lkotlin/d/a/a;)Lkotlin/reflect/jvm/internal/ae$a;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/l$a;->u:Lkotlin/reflect/jvm/internal/ae$a;

    return-void
.end method

.method private final a(Ljava/lang/Class;)Ljava/lang/String;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x0

    .line 74
    invoke-virtual {p1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    .line 75
    invoke-virtual {p1}, Ljava/lang/Class;->getEnclosingMethod()Ljava/lang/reflect/Method;

    move-result-object v0

    if-eqz v0, :cond_0

    check-cast v0, Ljava/lang/reflect/Method;

    .line 76
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Ljava/lang/reflect/Method;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "$"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0, v3, v4, v3}, Lkotlin/h/j;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 81
    :goto_0
    return-object v0

    .line 78
    :cond_0
    invoke-virtual {p1}, Ljava/lang/Class;->getEnclosingConstructor()Ljava/lang/reflect/Constructor;

    move-result-object v0

    if-eqz v0, :cond_1

    check-cast v0, Ljava/lang/reflect/Constructor;

    .line 79
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Ljava/lang/reflect/Constructor;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "$"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0, v3, v4, v3}, Lkotlin/h/j;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 81
    :cond_1
    const/16 v0, 0x24

    invoke-static {v1, v0, v3, v4, v3}, Lkotlin/h/j;->b(Ljava/lang/String;CLjava/lang/String;ILjava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static final synthetic a(Lkotlin/reflect/jvm/internal/l$a;Ljava/lang/Class;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 39
    invoke-direct {p0, p1}, Lkotlin/reflect/jvm/internal/l$a;->a(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a()Lkotlin/reflect/jvm/internal/impl/b/e;
    .locals 3

    iget-object v0, p0, Lkotlin/reflect/jvm/internal/l$a;->e:Lkotlin/reflect/jvm/internal/ae$a;

    sget-object v1, Lkotlin/reflect/jvm/internal/l$a;->a:[Lkotlin/reflect/l;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-virtual {v0, p0, v1}, Lkotlin/reflect/jvm/internal/ae$a;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/e;

    return-object v0
.end method

.method public final b()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/annotation/Annotation;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lkotlin/reflect/jvm/internal/l$a;->f:Lkotlin/reflect/jvm/internal/ae$a;

    sget-object v1, Lkotlin/reflect/jvm/internal/l$a;->a:[Lkotlin/reflect/l;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-virtual {v0, p0, v1}, Lkotlin/reflect/jvm/internal/ae$a;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 3

    iget-object v0, p0, Lkotlin/reflect/jvm/internal/l$a;->h:Lkotlin/reflect/jvm/internal/ae$a;

    sget-object v1, Lkotlin/reflect/jvm/internal/l$a;->a:[Lkotlin/reflect/l;

    const/4 v2, 0x3

    aget-object v1, v1, v2

    invoke-virtual {v0, p0, v1}, Lkotlin/reflect/jvm/internal/ae$a;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public final d()Ljava/util/Collection;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<",
            "Lkotlin/reflect/f",
            "<TT;>;>;"
        }
    .end annotation

    iget-object v0, p0, Lkotlin/reflect/jvm/internal/l$a;->i:Lkotlin/reflect/jvm/internal/ae$a;

    sget-object v1, Lkotlin/reflect/jvm/internal/l$a;->a:[Lkotlin/reflect/l;

    const/4 v2, 0x4

    aget-object v1, v1, v2

    invoke-virtual {v0, p0, v1}, Lkotlin/reflect/jvm/internal/ae$a;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    return-object v0
.end method

.method public final e()Ljava/util/Collection;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<",
            "Lkotlin/reflect/jvm/internal/j",
            "<*>;>;"
        }
    .end annotation

    iget-object v0, p0, Lkotlin/reflect/jvm/internal/l$a;->n:Lkotlin/reflect/jvm/internal/ae$a;

    sget-object v1, Lkotlin/reflect/jvm/internal/l$a;->a:[Lkotlin/reflect/l;

    const/16 v2, 0x9

    aget-object v1, v1, v2

    invoke-virtual {v0, p0, v1}, Lkotlin/reflect/jvm/internal/ae$a;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    return-object v0
.end method

.method public final f()Ljava/util/Collection;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<",
            "Lkotlin/reflect/jvm/internal/j",
            "<*>;>;"
        }
    .end annotation

    iget-object v0, p0, Lkotlin/reflect/jvm/internal/l$a;->o:Lkotlin/reflect/jvm/internal/ae$a;

    sget-object v1, Lkotlin/reflect/jvm/internal/l$a;->a:[Lkotlin/reflect/l;

    const/16 v2, 0xa

    aget-object v1, v1, v2

    invoke-virtual {v0, p0, v1}, Lkotlin/reflect/jvm/internal/ae$a;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    return-object v0
.end method

.method public final g()Ljava/util/Collection;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<",
            "Lkotlin/reflect/jvm/internal/j",
            "<*>;>;"
        }
    .end annotation

    iget-object v0, p0, Lkotlin/reflect/jvm/internal/l$a;->p:Lkotlin/reflect/jvm/internal/ae$a;

    sget-object v1, Lkotlin/reflect/jvm/internal/l$a;->a:[Lkotlin/reflect/l;

    const/16 v2, 0xb

    aget-object v1, v1, v2

    invoke-virtual {v0, p0, v1}, Lkotlin/reflect/jvm/internal/ae$a;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    return-object v0
.end method

.method public final h()Ljava/util/Collection;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<",
            "Lkotlin/reflect/jvm/internal/j",
            "<*>;>;"
        }
    .end annotation

    iget-object v0, p0, Lkotlin/reflect/jvm/internal/l$a;->q:Lkotlin/reflect/jvm/internal/ae$a;

    sget-object v1, Lkotlin/reflect/jvm/internal/l$a;->a:[Lkotlin/reflect/l;

    const/16 v2, 0xc

    aget-object v1, v1, v2

    invoke-virtual {v0, p0, v1}, Lkotlin/reflect/jvm/internal/ae$a;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    return-object v0
.end method

.method public final i()Ljava/util/Collection;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<",
            "Lkotlin/reflect/jvm/internal/j",
            "<*>;>;"
        }
    .end annotation

    iget-object v0, p0, Lkotlin/reflect/jvm/internal/l$a;->r:Lkotlin/reflect/jvm/internal/ae$a;

    sget-object v1, Lkotlin/reflect/jvm/internal/l$a;->a:[Lkotlin/reflect/l;

    const/16 v2, 0xd

    aget-object v1, v1, v2

    invoke-virtual {v0, p0, v1}, Lkotlin/reflect/jvm/internal/ae$a;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    return-object v0
.end method

.method public final j()Ljava/util/Collection;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<",
            "Lkotlin/reflect/jvm/internal/j",
            "<*>;>;"
        }
    .end annotation

    iget-object v0, p0, Lkotlin/reflect/jvm/internal/l$a;->s:Lkotlin/reflect/jvm/internal/ae$a;

    sget-object v1, Lkotlin/reflect/jvm/internal/l$a;->a:[Lkotlin/reflect/l;

    const/16 v2, 0xe

    aget-object v1, v1, v2

    invoke-virtual {v0, p0, v1}, Lkotlin/reflect/jvm/internal/ae$a;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    return-object v0
.end method
