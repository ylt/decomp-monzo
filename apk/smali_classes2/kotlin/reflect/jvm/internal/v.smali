.class public Lkotlin/reflect/jvm/internal/v;
.super Lkotlin/reflect/jvm/internal/w;
.source "KProperty2Impl.kt"

# interfaces
.implements Lkotlin/reflect/o;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lkotlin/reflect/jvm/internal/v$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<D:",
        "Ljava/lang/Object;",
        "E:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Lkotlin/reflect/jvm/internal/w",
        "<TR;>;",
        "Lkotlin/reflect/o",
        "<TD;TE;TR;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x1
    }
    d1 = {
        "\u0000P\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0010\u0000\n\u0002\u0008\u0003\u0008\u0010\u0018\u0000*\u0004\u0008\u0000\u0010\u0001*\u0004\u0008\u0001\u0010\u0002*\u0006\u0008\u0002\u0010\u0003 \u00012\u0014\u0012\u0004\u0012\u0002H\u0001\u0012\u0004\u0012\u0002H\u0002\u0012\u0004\u0012\u0002H\u00030\u00042\u0008\u0012\u0004\u0012\u0002H\u00030\u0005:\u0001!B\u001f\u0008\u0016\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\t\u00a2\u0006\u0002\u0010\u000bB\u0017\u0008\u0016\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u000c\u001a\u00020\r\u00a2\u0006\u0002\u0010\u000eJ\u001d\u0010\u001a\u001a\u00028\u00022\u0006\u0010\u001b\u001a\u00028\u00002\u0006\u0010\u001c\u001a\u00028\u0001H\u0016\u00a2\u0006\u0002\u0010\u001dJ\u001f\u0010\u001e\u001a\u0004\u0018\u00010\u001f2\u0006\u0010\u001b\u001a\u00028\u00002\u0006\u0010\u001c\u001a\u00028\u0001H\u0016\u00a2\u0006\u0002\u0010\u001dJ\u001e\u0010 \u001a\u00028\u00022\u0006\u0010\u001b\u001a\u00028\u00002\u0006\u0010\u001c\u001a\u00028\u0001H\u0096\u0002\u00a2\u0006\u0002\u0010\u001dR\u0016\u0010\u000f\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\u00110\u0010X\u0082\u0004\u00a2\u0006\u0002\n\u0000R&\u0010\u0012\u001a\u0014\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u0001\u0012\u0004\u0012\u00028\u00020\u00138VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0014\u0010\u0015RE\u0010\u0016\u001a9\u00120\u0012.\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u0001\u0012\u0004\u0012\u00028\u0002 \u0018*\u0016\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u0001\u0012\u0004\u0012\u00028\u0002\u0018\u00010\u00130\u00130\u0017\u00a2\u0006\u0002\u0008\u0019X\u0088\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\""
    }
    d2 = {
        "Lkotlin/reflect/jvm/internal/KProperty2Impl;",
        "D",
        "E",
        "R",
        "Lkotlin/reflect/KProperty2;",
        "Lkotlin/reflect/jvm/internal/KPropertyImpl;",
        "container",
        "Lkotlin/reflect/jvm/internal/KDeclarationContainerImpl;",
        "name",
        "",
        "signature",
        "(Lkotlin/reflect/jvm/internal/KDeclarationContainerImpl;Ljava/lang/String;Ljava/lang/String;)V",
        "descriptor",
        "Lorg/jetbrains/kotlin/descriptors/PropertyDescriptor;",
        "(Lkotlin/reflect/jvm/internal/KDeclarationContainerImpl;Lorg/jetbrains/kotlin/descriptors/PropertyDescriptor;)V",
        "delegateField",
        "Lkotlin/Lazy;",
        "Ljava/lang/reflect/Field;",
        "getter",
        "Lkotlin/reflect/jvm/internal/KProperty2Impl$Getter;",
        "getGetter",
        "()Lkotlin/reflect/jvm/internal/KProperty2Impl$Getter;",
        "getter_",
        "Lkotlin/reflect/jvm/internal/ReflectProperties$LazyVal;",
        "kotlin.jvm.PlatformType",
        "Lorg/jetbrains/annotations/NotNull;",
        "get",
        "receiver1",
        "receiver2",
        "(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;",
        "getDelegate",
        "",
        "invoke",
        "Getter",
        "kotlin-reflection"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x5
    }
.end annotation


# instance fields
.field private final b:Lkotlin/reflect/jvm/internal/ae$b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/reflect/jvm/internal/ae$b",
            "<",
            "Lkotlin/reflect/jvm/internal/v$a",
            "<TD;TE;TR;>;>;"
        }
    .end annotation
.end field

.field private final c:Lkotlin/c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/c",
            "<",
            "Ljava/lang/reflect/Field;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lkotlin/reflect/jvm/internal/n;Lkotlin/reflect/jvm/internal/impl/b/ag;)V
    .locals 2

    .prologue
    const-string v0, "container"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "descriptor"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 28
    invoke-direct {p0, p1, p2}, Lkotlin/reflect/jvm/internal/w;-><init>(Lkotlin/reflect/jvm/internal/n;Lkotlin/reflect/jvm/internal/impl/b/ag;)V

    .line 30
    new-instance v0, Lkotlin/reflect/jvm/internal/v$c;

    invoke-direct {v0, p0}, Lkotlin/reflect/jvm/internal/v$c;-><init>(Lkotlin/reflect/jvm/internal/v;)V

    check-cast v0, Lkotlin/d/a/a;

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/ae;->a(Lkotlin/d/a/a;)Lkotlin/reflect/jvm/internal/ae$b;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/v;->b:Lkotlin/reflect/jvm/internal/ae$b;

    .line 36
    sget-object v1, Lkotlin/f;->b:Lkotlin/f;

    new-instance v0, Lkotlin/reflect/jvm/internal/v$b;

    invoke-direct {v0, p0}, Lkotlin/reflect/jvm/internal/v$b;-><init>(Lkotlin/reflect/jvm/internal/v;)V

    check-cast v0, Lkotlin/d/a/a;

    invoke-static {v1, v0}, Lkotlin/d;->a(Lkotlin/f;Lkotlin/d/a/a;)Lkotlin/c;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/v;->c:Lkotlin/c;

    return-void
.end method


# virtual methods
.method public a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TD;TE;)TR;"
        }
    .end annotation

    .prologue
    .line 40
    invoke-virtual {p0, p1, p2}, Lkotlin/reflect/jvm/internal/v;->b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TD;TE;)TR;"
        }
    .end annotation

    .prologue
    .line 34
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/v;->c()Lkotlin/reflect/jvm/internal/v$a;

    move-result-object v0

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    const/4 v2, 0x1

    aput-object p2, v1, v2

    invoke-virtual {v0, v1}, Lkotlin/reflect/jvm/internal/v$a;->a([Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public c()Lkotlin/reflect/jvm/internal/v$a;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/reflect/jvm/internal/v$a",
            "<TD;TE;TR;>;"
        }
    .end annotation

    .prologue
    .line 32
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/v;->b:Lkotlin/reflect/jvm/internal/ae$b;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/ae$b;->a()Ljava/lang/Object;

    move-result-object v0

    const-string v1, "getter_()"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lkotlin/reflect/jvm/internal/v$a;

    return-object v0
.end method

.method public synthetic n()Lkotlin/reflect/l$b;
    .locals 1

    .prologue
    .line 25
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/v;->c()Lkotlin/reflect/jvm/internal/v$a;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/l$b;

    return-object v0
.end method

.method public synthetic r()Lkotlin/reflect/jvm/internal/w$c;
    .locals 1

    .prologue
    .line 25
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/v;->c()Lkotlin/reflect/jvm/internal/v$a;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/w$c;

    return-object v0
.end method
