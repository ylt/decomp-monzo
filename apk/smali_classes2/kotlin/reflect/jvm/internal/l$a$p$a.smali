.class final Lkotlin/reflect/jvm/internal/l$a$p$a;
.super Lkotlin/d/b/m;
.source "KClassImpl.kt"

# interfaces
.implements Lkotlin/d/a/a;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lkotlin/reflect/jvm/internal/l$a$p;->b()Ljava/util/List;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/d/b/m;",
        "Lkotlin/d/a/a",
        "<",
        "Ljava/lang/reflect/Type;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x1
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0000\u0010\u0000\u001a\u00020\u0001\"\u0008\u0008\u0000\u0010\u0002*\u00020\u0003H\n\u00a2\u0006\u0002\u0008\u0004"
    }
    d2 = {
        "<anonymous>",
        "Ljava/lang/reflect/Type;",
        "T",
        "",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x5
    }
.end annotation


# instance fields
.field final synthetic a:Lkotlin/reflect/jvm/internal/impl/k/r;

.field final synthetic b:Lkotlin/reflect/jvm/internal/l$a$p;


# direct methods
.method constructor <init>(Lkotlin/reflect/jvm/internal/impl/k/r;Lkotlin/reflect/jvm/internal/l$a$p;)V
    .locals 1

    iput-object p1, p0, Lkotlin/reflect/jvm/internal/l$a$p$a;->a:Lkotlin/reflect/jvm/internal/impl/k/r;

    iput-object p2, p0, Lkotlin/reflect/jvm/internal/l$a$p$a;->b:Lkotlin/reflect/jvm/internal/l$a$p;

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lkotlin/d/b/m;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final b()Ljava/lang/reflect/Type;
    .locals 4

    .prologue
    .line 120
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/l$a$p$a;->a:Lkotlin/reflect/jvm/internal/impl/k/r;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/k/r;->i()Lkotlin/reflect/jvm/internal/impl/k/ad;

    move-result-object v0

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/k/ad;->d()Lkotlin/reflect/jvm/internal/impl/b/h;

    move-result-object v1

    .line 121
    instance-of v0, v1, Lkotlin/reflect/jvm/internal/impl/b/e;

    if-nez v0, :cond_0

    new-instance v0, Lkotlin/reflect/jvm/internal/ac;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Supertype not a class: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lkotlin/reflect/jvm/internal/ac;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    :cond_0
    move-object v0, v1

    .line 123
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/e;

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/ak;->a(Lkotlin/reflect/jvm/internal/impl/b/e;)Ljava/lang/Class;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 126
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/l$a$p$a;->b:Lkotlin/reflect/jvm/internal/l$a$p;

    iget-object v0, v0, Lkotlin/reflect/jvm/internal/l$a$p;->a:Lkotlin/reflect/jvm/internal/l$a;

    iget-object v0, v0, Lkotlin/reflect/jvm/internal/l$a;->b:Lkotlin/reflect/jvm/internal/l;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/l;->a()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSuperclass()Ljava/lang/Class;

    move-result-object v0

    invoke-static {v0, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 127
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/l$a$p$a;->b:Lkotlin/reflect/jvm/internal/l$a$p;

    iget-object v0, v0, Lkotlin/reflect/jvm/internal/l$a$p;->a:Lkotlin/reflect/jvm/internal/l$a;

    iget-object v0, v0, Lkotlin/reflect/jvm/internal/l$a;->b:Lkotlin/reflect/jvm/internal/l;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/l;->a()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getGenericSuperclass()Ljava/lang/reflect/Type;

    move-result-object v0

    const-string v1, "jClass.genericSuperclass"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 133
    :goto_0
    return-object v0

    .line 124
    :cond_1
    new-instance v0, Lkotlin/reflect/jvm/internal/ac;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unsupported superclass of "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lkotlin/reflect/jvm/internal/l$a$p$a;->b:Lkotlin/reflect/jvm/internal/l$a$p;

    iget-object v3, v3, Lkotlin/reflect/jvm/internal/l$a$p;->a:Lkotlin/reflect/jvm/internal/l$a;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ": "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lkotlin/reflect/jvm/internal/ac;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    .line 130
    :cond_2
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/l$a$p$a;->b:Lkotlin/reflect/jvm/internal/l$a$p;

    iget-object v0, v0, Lkotlin/reflect/jvm/internal/l$a$p;->a:Lkotlin/reflect/jvm/internal/l$a;

    iget-object v0, v0, Lkotlin/reflect/jvm/internal/l$a;->b:Lkotlin/reflect/jvm/internal/l;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/l;->a()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getInterfaces()[Ljava/lang/Class;

    move-result-object v0

    check-cast v0, [Ljava/lang/Object;

    invoke-static {v0, v2}, Lkotlin/a/g;->b([Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v0

    .line 131
    if-gez v0, :cond_3

    new-instance v0, Lkotlin/reflect/jvm/internal/ac;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "No superclass of "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lkotlin/reflect/jvm/internal/l$a$p$a;->b:Lkotlin/reflect/jvm/internal/l$a$p;

    iget-object v3, v3, Lkotlin/reflect/jvm/internal/l$a$p;->a:Lkotlin/reflect/jvm/internal/l$a;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " in Java reflection for "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lkotlin/reflect/jvm/internal/ac;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    .line 132
    :cond_3
    iget-object v1, p0, Lkotlin/reflect/jvm/internal/l$a$p$a;->b:Lkotlin/reflect/jvm/internal/l$a$p;

    iget-object v1, v1, Lkotlin/reflect/jvm/internal/l$a$p;->a:Lkotlin/reflect/jvm/internal/l$a;

    iget-object v1, v1, Lkotlin/reflect/jvm/internal/l$a;->b:Lkotlin/reflect/jvm/internal/l;

    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/l;->a()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getGenericInterfaces()[Ljava/lang/reflect/Type;

    move-result-object v1

    aget-object v0, v1, v0

    const-string v1, "jClass.genericInterfaces[index]"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public synthetic v_()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 39
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/l$a$p$a;->b()Ljava/lang/reflect/Type;

    move-result-object v0

    return-object v0
.end method
