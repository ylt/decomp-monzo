.class public final Lkotlin/reflect/jvm/internal/s;
.super Ljava/lang/Object;
.source "KParameterImpl.kt"

# interfaces
.implements Lkotlin/reflect/k;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x1
    }
    d1 = {
        "\u0000T\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010 \n\u0002\u0010\u001b\n\u0002\u0008\r\n\u0002\u0010\u000b\n\u0002\u0008\u0005\n\u0002\u0010\u000e\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0010\u0000\n\u0002\u0008\u0003\u0008\u0000\u0018\u00002\u00020\u0001B/\u0012\n\u0010\u0002\u001a\u0006\u0012\u0002\u0008\u00030\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u000c\u0010\u0008\u001a\u0008\u0012\u0004\u0012\u00020\n0\t\u00a2\u0006\u0002\u0010\u000bJ\u0013\u0010)\u001a\u00020\u001c2\u0008\u0010*\u001a\u0004\u0018\u00010+H\u0096\u0002J\u0008\u0010,\u001a\u00020\u0005H\u0016J\u0008\u0010-\u001a\u00020\"H\u0016R!\u0010\u000c\u001a\u0008\u0012\u0004\u0012\u00020\u000e0\r8VX\u0096\u0084\u0002\u00a2\u0006\u000c\n\u0004\u0008\u0011\u0010\u0012\u001a\u0004\u0008\u000f\u0010\u0010R\u0015\u0010\u0002\u001a\u0006\u0012\u0002\u0008\u00030\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0013\u0010\u0014R\u001b\u0010\u0015\u001a\u00020\n8BX\u0082\u0084\u0002\u00a2\u0006\u000c\n\u0004\u0008\u0018\u0010\u0012\u001a\u0004\u0008\u0016\u0010\u0017R\u0014\u0010\u0004\u001a\u00020\u0005X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0019\u0010\u001aR\u0014\u0010\u001b\u001a\u00020\u001c8VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u001b\u0010\u001dR\u0014\u0010\u001e\u001a\u00020\u001c8VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u001e\u0010\u001dR\u0014\u0010\u0006\u001a\u00020\u0007X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001f\u0010 R\u0016\u0010!\u001a\u0004\u0018\u00010\"8VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008#\u0010$R\u0014\u0010%\u001a\u00020&8VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\'\u0010(\u00a8\u0006."
    }
    d2 = {
        "Lkotlin/reflect/jvm/internal/KParameterImpl;",
        "Lkotlin/reflect/KParameter;",
        "callable",
        "Lkotlin/reflect/jvm/internal/KCallableImpl;",
        "index",
        "",
        "kind",
        "Lkotlin/reflect/KParameter$Kind;",
        "computeDescriptor",
        "Lkotlin/Function0;",
        "Lorg/jetbrains/kotlin/descriptors/ParameterDescriptor;",
        "(Lkotlin/reflect/jvm/internal/KCallableImpl;ILkotlin/reflect/KParameter$Kind;Lkotlin/jvm/functions/Function0;)V",
        "annotations",
        "",
        "",
        "getAnnotations",
        "()Ljava/util/List;",
        "annotations$delegate",
        "Lkotlin/reflect/jvm/internal/ReflectProperties$LazySoftVal;",
        "getCallable",
        "()Lkotlin/reflect/jvm/internal/KCallableImpl;",
        "descriptor",
        "getDescriptor",
        "()Lorg/jetbrains/kotlin/descriptors/ParameterDescriptor;",
        "descriptor$delegate",
        "getIndex",
        "()I",
        "isOptional",
        "",
        "()Z",
        "isVararg",
        "getKind",
        "()Lkotlin/reflect/KParameter$Kind;",
        "name",
        "",
        "getName",
        "()Ljava/lang/String;",
        "type",
        "Lkotlin/reflect/KType;",
        "getType",
        "()Lkotlin/reflect/KType;",
        "equals",
        "other",
        "",
        "hashCode",
        "toString",
        "kotlin-reflection"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x5
    }
.end annotation


# static fields
.field static final synthetic a:[Lkotlin/reflect/l;


# instance fields
.field private final b:Lkotlin/reflect/jvm/internal/ae$a;

.field private final c:Lkotlin/reflect/jvm/internal/ae$a;

.field private final d:Lkotlin/reflect/jvm/internal/j;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/reflect/jvm/internal/j",
            "<*>;"
        }
    .end annotation
.end field

.field private final e:I

.field private final f:Lkotlin/reflect/k$a;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const/4 v0, 0x2

    new-array v1, v0, [Lkotlin/reflect/l;

    const/4 v2, 0x0

    new-instance v0, Lkotlin/d/b/w;

    const-class v3, Lkotlin/reflect/jvm/internal/s;

    invoke-static {v3}, Lkotlin/d/b/y;->a(Ljava/lang/Class;)Lkotlin/reflect/c;

    move-result-object v3

    const-string v4, "descriptor"

    const-string v5, "getDescriptor()Lorg/jetbrains/kotlin/descriptors/ParameterDescriptor;"

    invoke-direct {v0, v3, v4, v5}, Lkotlin/d/b/w;-><init>(Lkotlin/reflect/e;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v0}, Lkotlin/d/b/y;->a(Lkotlin/d/b/v;)Lkotlin/reflect/n;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/l;

    aput-object v0, v1, v2

    const/4 v2, 0x1

    new-instance v0, Lkotlin/d/b/w;

    const-class v3, Lkotlin/reflect/jvm/internal/s;

    invoke-static {v3}, Lkotlin/d/b/y;->a(Ljava/lang/Class;)Lkotlin/reflect/c;

    move-result-object v3

    const-string v4, "annotations"

    const-string v5, "getAnnotations()Ljava/util/List;"

    invoke-direct {v0, v3, v4, v5}, Lkotlin/d/b/w;-><init>(Lkotlin/reflect/e;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v0}, Lkotlin/d/b/y;->a(Lkotlin/d/b/v;)Lkotlin/reflect/n;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/l;

    aput-object v0, v1, v2

    sput-object v1, Lkotlin/reflect/jvm/internal/s;->a:[Lkotlin/reflect/l;

    return-void
.end method

.method public constructor <init>(Lkotlin/reflect/jvm/internal/j;ILkotlin/reflect/k$a;Lkotlin/d/a/a;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/reflect/jvm/internal/j",
            "<*>;I",
            "Lkotlin/reflect/k$a;",
            "Lkotlin/d/a/a",
            "<+",
            "Lkotlin/reflect/jvm/internal/impl/b/ad;",
            ">;)V"
        }
    .end annotation

    .prologue
    const-string v0, "callable"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "kind"

    invoke-static {p3, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "computeDescriptor"

    invoke-static {p4, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lkotlin/reflect/jvm/internal/s;->d:Lkotlin/reflect/jvm/internal/j;

    iput p2, p0, Lkotlin/reflect/jvm/internal/s;->e:I

    iput-object p3, p0, Lkotlin/reflect/jvm/internal/s;->f:Lkotlin/reflect/k$a;

    .line 31
    invoke-static {p4}, Lkotlin/reflect/jvm/internal/ae;->b(Lkotlin/d/a/a;)Lkotlin/reflect/jvm/internal/ae$a;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/s;->b:Lkotlin/reflect/jvm/internal/ae$a;

    .line 33
    new-instance v0, Lkotlin/reflect/jvm/internal/s$a;

    invoke-direct {v0, p0}, Lkotlin/reflect/jvm/internal/s$a;-><init>(Lkotlin/reflect/jvm/internal/s;)V

    check-cast v0, Lkotlin/d/a/a;

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/ae;->b(Lkotlin/d/a/a;)Lkotlin/reflect/jvm/internal/ae$a;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/s;->c:Lkotlin/reflect/jvm/internal/ae$a;

    return-void
.end method

.method public static final synthetic a(Lkotlin/reflect/jvm/internal/s;)Lkotlin/reflect/jvm/internal/impl/b/ad;
    .locals 1

    .prologue
    .line 25
    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/s;->g()Lkotlin/reflect/jvm/internal/impl/b/ad;

    move-result-object v0

    return-object v0
.end method

.method private final g()Lkotlin/reflect/jvm/internal/impl/b/ad;
    .locals 3

    iget-object v0, p0, Lkotlin/reflect/jvm/internal/s;->b:Lkotlin/reflect/jvm/internal/ae$a;

    sget-object v1, Lkotlin/reflect/jvm/internal/s;->a:[Lkotlin/reflect/l;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-virtual {v0, p0, v1}, Lkotlin/reflect/jvm/internal/ae$a;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/ad;

    return-object v0
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 27
    iget v0, p0, Lkotlin/reflect/jvm/internal/s;->e:I

    return v0
.end method

.method public b()Ljava/lang/String;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 36
    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/s;->g()Lkotlin/reflect/jvm/internal/impl/b/ad;

    move-result-object v0

    instance-of v2, v0, Lkotlin/reflect/jvm/internal/impl/b/at;

    if-nez v2, :cond_0

    move-object v0, v1

    :cond_0
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/at;

    if-eqz v0, :cond_1

    .line 37
    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/b/at;->a()Lkotlin/reflect/jvm/internal/impl/b/a;

    move-result-object v2

    invoke-interface {v2}, Lkotlin/reflect/jvm/internal/impl/b/a;->H_()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 39
    :cond_1
    :goto_0
    return-object v1

    .line 38
    :cond_2
    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/b/at;->D_()Lkotlin/reflect/jvm/internal/impl/e/f;

    move-result-object v0

    .line 39
    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/e/f;->c()Z

    move-result v2

    if-nez v2, :cond_1

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/e/f;->a()Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method public c()Lkotlin/reflect/p;
    .locals 3

    .prologue
    .line 43
    new-instance v1, Lkotlin/reflect/jvm/internal/y;

    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/s;->g()Lkotlin/reflect/jvm/internal/impl/b/ad;

    move-result-object v0

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/b/ad;->s()Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v2

    const-string v0, "descriptor.type"

    invoke-static {v2, v0}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lkotlin/reflect/jvm/internal/s$b;

    invoke-direct {v0, p0}, Lkotlin/reflect/jvm/internal/s$b;-><init>(Lkotlin/reflect/jvm/internal/s;)V

    check-cast v0, Lkotlin/d/a/a;

    invoke-direct {v1, v2, v0}, Lkotlin/reflect/jvm/internal/y;-><init>(Lkotlin/reflect/jvm/internal/impl/k/r;Lkotlin/d/a/a;)V

    move-object v0, v1

    check-cast v0, Lkotlin/reflect/p;

    return-object v0
.end method

.method public d()Lkotlin/reflect/k$a;
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/s;->f:Lkotlin/reflect/k$a;

    return-object v0
.end method

.method public e()Z
    .locals 2

    .prologue
    .line 46
    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/s;->g()Lkotlin/reflect/jvm/internal/impl/b/ad;

    move-result-object v0

    instance-of v1, v0, Lkotlin/reflect/jvm/internal/impl/b/at;

    if-nez v1, :cond_0

    const/4 v0, 0x0

    :cond_0
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/at;

    if-eqz v0, :cond_1

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/h/c/a;->a(Lkotlin/reflect/jvm/internal/impl/b/at;)Z

    move-result v0

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 52
    instance-of v0, p1, Lkotlin/reflect/jvm/internal/s;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lkotlin/reflect/jvm/internal/s;->d:Lkotlin/reflect/jvm/internal/j;

    move-object v0, p1

    check-cast v0, Lkotlin/reflect/jvm/internal/s;

    iget-object v0, v0, Lkotlin/reflect/jvm/internal/s;->d:Lkotlin/reflect/jvm/internal/j;

    invoke-static {v1, v0}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/s;->g()Lkotlin/reflect/jvm/internal/impl/b/ad;

    move-result-object v0

    check-cast p1, Lkotlin/reflect/jvm/internal/s;

    invoke-direct {p1}, Lkotlin/reflect/jvm/internal/s;->g()Lkotlin/reflect/jvm/internal/impl/b/ad;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final f()Lkotlin/reflect/jvm/internal/j;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/reflect/jvm/internal/j",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 26
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/s;->d:Lkotlin/reflect/jvm/internal/j;

    return-object v0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 55
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/s;->d:Lkotlin/reflect/jvm/internal/j;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/j;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x1f

    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/s;->g()Lkotlin/reflect/jvm/internal/impl/b/ad;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public j()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/annotation/Annotation;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lkotlin/reflect/jvm/internal/s;->c:Lkotlin/reflect/jvm/internal/ae$a;

    sget-object v1, Lkotlin/reflect/jvm/internal/s;->a:[Lkotlin/reflect/l;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-virtual {v0, p0, v1}, Lkotlin/reflect/jvm/internal/ae$a;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 58
    sget-object v0, Lkotlin/reflect/jvm/internal/ag;->a:Lkotlin/reflect/jvm/internal/ag;

    invoke-virtual {v0, p0}, Lkotlin/reflect/jvm/internal/ag;->a(Lkotlin/reflect/jvm/internal/s;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
