.class public final Lkotlin/reflect/jvm/internal/y;
.super Ljava/lang/Object;
.source "KTypeImpl.kt"

# interfaces
.implements Lkotlin/reflect/p;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x1
    }
    d1 = {
        "\u0000J\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0010\u000b\n\u0002\u0008\n\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u000e\n\u0000\u0008\u0000\u0018\u00002\u00020\u0001B\u001b\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u000c\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0005\u00a2\u0006\u0002\u0010\u0007J\u0012\u0010\u001d\u001a\u0004\u0018\u00010\u00102\u0006\u0010\u0002\u001a\u00020\u0003H\u0002J\u0013\u0010\u001e\u001a\u00020\u00152\u0008\u0010\u001f\u001a\u0004\u0018\u00010 H\u0096\u0002J\u0008\u0010!\u001a\u00020\"H\u0016J\u0008\u0010#\u001a\u00020$H\u0016R!\u0010\u0008\u001a\u0008\u0012\u0004\u0012\u00020\n0\t8VX\u0096\u0084\u0002\u00a2\u0006\u000c\n\u0004\u0008\r\u0010\u000e\u001a\u0004\u0008\u000b\u0010\u000cR\u001d\u0010\u000f\u001a\u0004\u0018\u00010\u00108VX\u0096\u0084\u0002\u00a2\u0006\u000c\n\u0004\u0008\u0013\u0010\u000e\u001a\u0004\u0008\u0011\u0010\u0012R\u0014\u0010\u0014\u001a\u00020\u00158VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0014\u0010\u0016R\u001b\u0010\u0017\u001a\u00020\u00068@X\u0080\u0084\u0002\u00a2\u0006\u000c\n\u0004\u0008\u001a\u0010\u000e\u001a\u0004\u0008\u0018\u0010\u0019R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001b\u0010\u001c\u00a8\u0006%"
    }
    d2 = {
        "Lkotlin/reflect/jvm/internal/KTypeImpl;",
        "Lkotlin/reflect/KType;",
        "type",
        "Lorg/jetbrains/kotlin/types/KotlinType;",
        "computeJavaType",
        "Lkotlin/Function0;",
        "Ljava/lang/reflect/Type;",
        "(Lorg/jetbrains/kotlin/types/KotlinType;Lkotlin/jvm/functions/Function0;)V",
        "arguments",
        "",
        "Lkotlin/reflect/KTypeProjection;",
        "getArguments",
        "()Ljava/util/List;",
        "arguments$delegate",
        "Lkotlin/reflect/jvm/internal/ReflectProperties$LazySoftVal;",
        "classifier",
        "Lkotlin/reflect/KClassifier;",
        "getClassifier",
        "()Lkotlin/reflect/KClassifier;",
        "classifier$delegate",
        "isMarkedNullable",
        "",
        "()Z",
        "javaType",
        "getJavaType$kotlin_reflection",
        "()Ljava/lang/reflect/Type;",
        "javaType$delegate",
        "getType",
        "()Lorg/jetbrains/kotlin/types/KotlinType;",
        "convert",
        "equals",
        "other",
        "",
        "hashCode",
        "",
        "toString",
        "",
        "kotlin-reflection"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x5
    }
.end annotation


# static fields
.field static final synthetic a:[Lkotlin/reflect/l;


# instance fields
.field private final b:Lkotlin/reflect/jvm/internal/ae$a;

.field private final c:Lkotlin/reflect/jvm/internal/ae$a;

.field private final d:Lkotlin/reflect/jvm/internal/ae$a;

.field private final e:Lkotlin/reflect/jvm/internal/impl/k/r;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const/4 v0, 0x3

    new-array v1, v0, [Lkotlin/reflect/l;

    const/4 v2, 0x0

    new-instance v0, Lkotlin/d/b/w;

    const-class v3, Lkotlin/reflect/jvm/internal/y;

    invoke-static {v3}, Lkotlin/d/b/y;->a(Ljava/lang/Class;)Lkotlin/reflect/c;

    move-result-object v3

    const-string v4, "javaType"

    const-string v5, "getJavaType$kotlin_reflection()Ljava/lang/reflect/Type;"

    invoke-direct {v0, v3, v4, v5}, Lkotlin/d/b/w;-><init>(Lkotlin/reflect/e;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v0}, Lkotlin/d/b/y;->a(Lkotlin/d/b/v;)Lkotlin/reflect/n;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/l;

    aput-object v0, v1, v2

    const/4 v2, 0x1

    new-instance v0, Lkotlin/d/b/w;

    const-class v3, Lkotlin/reflect/jvm/internal/y;

    invoke-static {v3}, Lkotlin/d/b/y;->a(Ljava/lang/Class;)Lkotlin/reflect/c;

    move-result-object v3

    const-string v4, "classifier"

    const-string v5, "getClassifier()Lkotlin/reflect/KClassifier;"

    invoke-direct {v0, v3, v4, v5}, Lkotlin/d/b/w;-><init>(Lkotlin/reflect/e;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v0}, Lkotlin/d/b/y;->a(Lkotlin/d/b/v;)Lkotlin/reflect/n;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/l;

    aput-object v0, v1, v2

    const/4 v2, 0x2

    new-instance v0, Lkotlin/d/b/w;

    const-class v3, Lkotlin/reflect/jvm/internal/y;

    invoke-static {v3}, Lkotlin/d/b/y;->a(Ljava/lang/Class;)Lkotlin/reflect/c;

    move-result-object v3

    const-string v4, "arguments"

    const-string v5, "getArguments()Ljava/util/List;"

    invoke-direct {v0, v3, v4, v5}, Lkotlin/d/b/w;-><init>(Lkotlin/reflect/e;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v0}, Lkotlin/d/b/y;->a(Lkotlin/d/b/v;)Lkotlin/reflect/n;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/l;

    aput-object v0, v1, v2

    sput-object v1, Lkotlin/reflect/jvm/internal/y;->a:[Lkotlin/reflect/l;

    return-void
.end method

.method public constructor <init>(Lkotlin/reflect/jvm/internal/impl/k/r;Lkotlin/d/a/a;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/reflect/jvm/internal/impl/k/r;",
            "Lkotlin/d/a/a",
            "<+",
            "Ljava/lang/reflect/Type;",
            ">;)V"
        }
    .end annotation

    .prologue
    const-string v0, "type"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "computeJavaType"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lkotlin/reflect/jvm/internal/y;->e:Lkotlin/reflect/jvm/internal/impl/k/r;

    .line 42
    invoke-static {p2}, Lkotlin/reflect/jvm/internal/ae;->b(Lkotlin/d/a/a;)Lkotlin/reflect/jvm/internal/ae$a;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/y;->b:Lkotlin/reflect/jvm/internal/ae$a;

    .line 44
    new-instance v0, Lkotlin/reflect/jvm/internal/y$b;

    invoke-direct {v0, p0}, Lkotlin/reflect/jvm/internal/y$b;-><init>(Lkotlin/reflect/jvm/internal/y;)V

    check-cast v0, Lkotlin/d/a/a;

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/ae;->b(Lkotlin/d/a/a;)Lkotlin/reflect/jvm/internal/ae$a;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/y;->c:Lkotlin/reflect/jvm/internal/ae$a;

    .line 72
    new-instance v0, Lkotlin/reflect/jvm/internal/y$a;

    invoke-direct {v0, p0}, Lkotlin/reflect/jvm/internal/y$a;-><init>(Lkotlin/reflect/jvm/internal/y;)V

    check-cast v0, Lkotlin/d/a/a;

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/ae;->b(Lkotlin/d/a/a;)Lkotlin/reflect/jvm/internal/ae$a;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/y;->d:Lkotlin/reflect/jvm/internal/ae$a;

    return-void
.end method

.method private final a(Lkotlin/reflect/jvm/internal/impl/k/r;)Lkotlin/reflect/d;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 47
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/k/r;->i()Lkotlin/reflect/jvm/internal/impl/k/ad;

    move-result-object v0

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/k/ad;->d()Lkotlin/reflect/jvm/internal/impl/b/h;

    move-result-object v0

    .line 49
    instance-of v2, v0, Lkotlin/reflect/jvm/internal/impl/b/e;

    if-eqz v2, :cond_6

    .line 50
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/e;

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/ak;->a(Lkotlin/reflect/jvm/internal/impl/b/e;)Ljava/lang/Class;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 51
    invoke-virtual {v2}, Ljava/lang/Class;->isArray()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 53
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/k/r;->a()Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lkotlin/a/m;->j(Ljava/util/List;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/ah;

    if-eqz v0, :cond_1

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/k/ah;->c()Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 55
    const-string v1, "argument"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lkotlin/reflect/jvm/internal/y;->a(Lkotlin/reflect/jvm/internal/impl/k/r;)Lkotlin/reflect/d;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 57
    new-instance v0, Lkotlin/reflect/jvm/internal/l;

    invoke-static {v1}, Lkotlin/reflect/jvm/b;->a(Lkotlin/reflect/d;)Lkotlin/reflect/c;

    move-result-object v1

    invoke-static {v1}, Lkotlin/d/a;->a(Lkotlin/reflect/c;)Ljava/lang/Class;

    move-result-object v1

    invoke-static {v1}, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/b;->g(Ljava/lang/Class;)Ljava/lang/Class;

    move-result-object v1

    invoke-direct {v0, v1}, Lkotlin/reflect/jvm/internal/l;-><init>(Ljava/lang/Class;)V

    check-cast v0, Lkotlin/reflect/d;

    .line 68
    :goto_0
    return-object v0

    :cond_0
    move-object v0, v1

    .line 50
    goto :goto_0

    .line 53
    :cond_1
    new-instance v0, Lkotlin/reflect/jvm/internal/l;

    invoke-direct {v0, v2}, Lkotlin/reflect/jvm/internal/l;-><init>(Ljava/lang/Class;)V

    check-cast v0, Lkotlin/reflect/d;

    goto :goto_0

    .line 56
    :cond_2
    new-instance v0, Lkotlin/reflect/jvm/internal/ac;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Cannot determine classifier for array element type: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lkotlin/reflect/jvm/internal/ac;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    .line 60
    :cond_3
    invoke-static {p1}, Lkotlin/reflect/jvm/internal/impl/k/an;->e(Lkotlin/reflect/jvm/internal/impl/k/r;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 61
    new-instance v0, Lkotlin/reflect/jvm/internal/l;

    invoke-static {v2}, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/b;->c(Ljava/lang/Class;)Ljava/lang/Class;

    move-result-object v1

    if-eqz v1, :cond_4

    :goto_1
    invoke-direct {v0, v1}, Lkotlin/reflect/jvm/internal/l;-><init>(Ljava/lang/Class;)V

    check-cast v0, Lkotlin/reflect/d;

    goto :goto_0

    :cond_4
    move-object v1, v2

    goto :goto_1

    .line 64
    :cond_5
    new-instance v0, Lkotlin/reflect/jvm/internal/l;

    invoke-direct {v0, v2}, Lkotlin/reflect/jvm/internal/l;-><init>(Ljava/lang/Class;)V

    check-cast v0, Lkotlin/reflect/d;

    goto :goto_0

    .line 66
    :cond_6
    instance-of v2, v0, Lkotlin/reflect/jvm/internal/impl/b/aq;

    if-eqz v2, :cond_7

    new-instance v1, Lkotlin/reflect/jvm/internal/ab;

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/aq;

    invoke-direct {v1, v0}, Lkotlin/reflect/jvm/internal/ab;-><init>(Lkotlin/reflect/jvm/internal/impl/b/aq;)V

    move-object v0, v1

    check-cast v0, Lkotlin/reflect/d;

    goto :goto_0

    .line 67
    :cond_7
    instance-of v0, v0, Lkotlin/reflect/jvm/internal/impl/b/ap;

    if-eqz v0, :cond_8

    const-string v1, "Type alias classifiers are not yet supported"

    new-instance v0, Lkotlin/g;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "An operation is not implemented: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lkotlin/g;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    :cond_8
    move-object v0, v1

    .line 68
    goto :goto_0
.end method

.method public static final synthetic a(Lkotlin/reflect/jvm/internal/y;Lkotlin/reflect/jvm/internal/impl/k/r;)Lkotlin/reflect/d;
    .locals 1

    .prologue
    .line 38
    invoke-direct {p0, p1}, Lkotlin/reflect/jvm/internal/y;->a(Lkotlin/reflect/jvm/internal/impl/k/r;)Lkotlin/reflect/d;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public a()Lkotlin/reflect/d;
    .locals 3

    iget-object v0, p0, Lkotlin/reflect/jvm/internal/y;->c:Lkotlin/reflect/jvm/internal/ae$a;

    sget-object v1, Lkotlin/reflect/jvm/internal/y;->a:[Lkotlin/reflect/l;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-virtual {v0, p0, v1}, Lkotlin/reflect/jvm/internal/ae$a;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/d;

    return-object v0
.end method

.method public b()Z
    .locals 1

    .prologue
    .line 114
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/y;->e:Lkotlin/reflect/jvm/internal/impl/k/r;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/k/r;->c()Z

    move-result v0

    return v0
.end method

.method public final c()Ljava/lang/reflect/Type;
    .locals 3

    iget-object v0, p0, Lkotlin/reflect/jvm/internal/y;->b:Lkotlin/reflect/jvm/internal/ae$a;

    sget-object v1, Lkotlin/reflect/jvm/internal/y;->a:[Lkotlin/reflect/l;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-virtual {v0, p0, v1}, Lkotlin/reflect/jvm/internal/ae$a;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/reflect/Type;

    return-object v0
.end method

.method public final d()Lkotlin/reflect/jvm/internal/impl/k/r;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/y;->e:Lkotlin/reflect/jvm/internal/impl/k/r;

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 117
    instance-of v0, p1, Lkotlin/reflect/jvm/internal/y;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lkotlin/reflect/jvm/internal/y;->e:Lkotlin/reflect/jvm/internal/impl/k/r;

    check-cast p1, Lkotlin/reflect/jvm/internal/y;

    iget-object v1, p1, Lkotlin/reflect/jvm/internal/y;->e:Lkotlin/reflect/jvm/internal/impl/k/r;

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 120
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/y;->e:Lkotlin/reflect/jvm/internal/impl/k/r;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/k/r;->hashCode()I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 123
    sget-object v0, Lkotlin/reflect/jvm/internal/ag;->a:Lkotlin/reflect/jvm/internal/ag;

    iget-object v1, p0, Lkotlin/reflect/jvm/internal/y;->e:Lkotlin/reflect/jvm/internal/impl/k/r;

    invoke-virtual {v0, v1}, Lkotlin/reflect/jvm/internal/ag;->a(Lkotlin/reflect/jvm/internal/impl/k/r;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
