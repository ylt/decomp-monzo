.class public final Lkotlin/reflect/jvm/internal/h$a$a;
.super Lkotlin/reflect/jvm/internal/h$a;
.source "RuntimeTypeMapper.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lkotlin/reflect/jvm/internal/h$a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x1
    }
    d1 = {
        "\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\u0010\u0010\u0007\u001a\u00020\u00052\u0006\u0010\u0008\u001a\u00020\tH\u0016R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\n"
    }
    d2 = {
        "Lkotlin/reflect/jvm/internal/JvmFunctionSignature$BuiltInFunction$Predefined;",
        "Lkotlin/reflect/jvm/internal/JvmFunctionSignature$BuiltInFunction;",
        "signature",
        "",
        "member",
        "Ljava/lang/reflect/Member;",
        "(Ljava/lang/String;Ljava/lang/reflect/Member;)V",
        "getMember",
        "container",
        "Lkotlin/reflect/jvm/internal/KDeclarationContainerImpl;",
        "kotlin-reflection"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x5
    }
.end annotation


# instance fields
.field private final a:Ljava/lang/reflect/Member;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/reflect/Member;)V
    .locals 1

    .prologue
    const-string v0, "signature"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "member"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 88
    invoke-direct {p0, p1}, Lkotlin/reflect/jvm/internal/h$a;-><init>(Ljava/lang/String;)V

    iput-object p2, p0, Lkotlin/reflect/jvm/internal/h$a$a;->a:Ljava/lang/reflect/Member;

    return-void
.end method


# virtual methods
.method public a(Lkotlin/reflect/jvm/internal/n;)Ljava/lang/reflect/Member;
    .locals 1

    .prologue
    const-string v0, "container"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 89
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/h$a$a;->a:Ljava/lang/reflect/Member;

    return-object v0
.end method
