.class final Lkotlin/reflect/jvm/internal/w$f;
.super Lkotlin/d/b/m;
.source "KPropertyImpl.kt"

# interfaces
.implements Lkotlin/d/a/a;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lkotlin/reflect/jvm/internal/w;-><init>(Lkotlin/reflect/jvm/internal/n;Ljava/lang/String;Ljava/lang/String;Lkotlin/reflect/jvm/internal/impl/b/ag;Ljava/lang/Object;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/d/b/m;",
        "Lkotlin/d/a/a",
        "<",
        "Ljava/lang/reflect/Field;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x1
    }
    d1 = {
        "\u0000\n\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u0004\u0018\u00010\u0001\"\u0006\u0008\u0000\u0010\u0002 \u0001H\n\u00a2\u0006\u0002\u0008\u0003"
    }
    d2 = {
        "<anonymous>",
        "Ljava/lang/reflect/Field;",
        "R",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x5
    }
.end annotation


# instance fields
.field final synthetic a:Lkotlin/reflect/jvm/internal/w;


# direct methods
.method constructor <init>(Lkotlin/reflect/jvm/internal/w;)V
    .locals 1

    iput-object p1, p0, Lkotlin/reflect/jvm/internal/w$f;->a:Lkotlin/reflect/jvm/internal/w;

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lkotlin/d/b/m;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final b()Ljava/lang/reflect/Field;
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 57
    sget-object v0, Lkotlin/reflect/jvm/internal/ai;->a:Lkotlin/reflect/jvm/internal/ai;

    iget-object v1, p0, Lkotlin/reflect/jvm/internal/w$f;->a:Lkotlin/reflect/jvm/internal/w;

    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/w;->u()Lkotlin/reflect/jvm/internal/impl/b/ag;

    move-result-object v1

    invoke-virtual {v0, v1}, Lkotlin/reflect/jvm/internal/ai;->a(Lkotlin/reflect/jvm/internal/impl/b/ag;)Lkotlin/reflect/jvm/internal/i;

    move-result-object v1

    .line 59
    instance-of v0, v1, Lkotlin/reflect/jvm/internal/i$c;

    if-eqz v0, :cond_4

    move-object v0, v1

    .line 60
    check-cast v0, Lkotlin/reflect/jvm/internal/i$c;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/i$c;->b()Lkotlin/reflect/jvm/internal/impl/b/ag;

    move-result-object v3

    .line 61
    sget-object v4, Lkotlin/reflect/jvm/internal/impl/i/c/d;->a:Lkotlin/reflect/jvm/internal/impl/i/c/d;

    move-object v0, v1

    check-cast v0, Lkotlin/reflect/jvm/internal/i$c;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/i$c;->c()Lkotlin/reflect/jvm/internal/impl/i/e$o;

    move-result-object v5

    move-object v0, v1

    check-cast v0, Lkotlin/reflect/jvm/internal/i$c;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/i$c;->e()Lkotlin/reflect/jvm/internal/impl/i/b/x;

    move-result-object v0

    check-cast v1, Lkotlin/reflect/jvm/internal/i$c;

    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/i$c;->f()Lkotlin/reflect/jvm/internal/impl/i/b/ah;

    move-result-object v1

    invoke-virtual {v4, v5, v0, v1}, Lkotlin/reflect/jvm/internal/impl/i/c/d;->a(Lkotlin/reflect/jvm/internal/impl/i/e$o;Lkotlin/reflect/jvm/internal/impl/i/b/x;Lkotlin/reflect/jvm/internal/impl/i/b/ah;)Lkotlin/reflect/jvm/internal/impl/i/c/d$a;

    move-result-object v0

    if-eqz v0, :cond_3

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/i/c/d$a;

    .line 62
    invoke-interface {v3}, Lkotlin/reflect/jvm/internal/impl/b/ag;->y_()Lkotlin/reflect/jvm/internal/impl/b/m;

    move-result-object v1

    invoke-static {v1}, Lkotlin/reflect/jvm/internal/impl/d/a/h;->a(Lkotlin/reflect/jvm/internal/impl/b/m;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 63
    iget-object v1, p0, Lkotlin/reflect/jvm/internal/w$f;->a:Lkotlin/reflect/jvm/internal/w;

    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/w;->o()Lkotlin/reflect/jvm/internal/n;

    move-result-object v1

    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/n;->a()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getEnclosingClass()Ljava/lang/Class;

    move-result-object v1

    .line 70
    :goto_0
    nop

    .line 71
    if-eqz v1, :cond_2

    :try_start_0
    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/i/c/d$a;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;
    :try_end_0
    .catch Ljava/lang/NoSuchFieldException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 61
    :goto_1
    check-cast v0, Ljava/lang/reflect/Field;

    .line 80
    :goto_2
    return-object v0

    .line 65
    :cond_0
    invoke-interface {v3}, Lkotlin/reflect/jvm/internal/impl/b/ag;->y_()Lkotlin/reflect/jvm/internal/impl/b/m;

    move-result-object v1

    check-cast v1, Lkotlin/reflect/jvm/internal/impl/b/m;

    .line 66
    instance-of v3, v1, Lkotlin/reflect/jvm/internal/impl/b/e;

    if-eqz v3, :cond_1

    check-cast v1, Lkotlin/reflect/jvm/internal/impl/b/e;

    invoke-static {v1}, Lkotlin/reflect/jvm/internal/ak;->a(Lkotlin/reflect/jvm/internal/impl/b/e;)Ljava/lang/Class;

    move-result-object v1

    .line 65
    :goto_3
    check-cast v1, Ljava/lang/Class;

    goto :goto_0

    .line 67
    :cond_1
    iget-object v1, p0, Lkotlin/reflect/jvm/internal/w$f;->a:Lkotlin/reflect/jvm/internal/w;

    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/w;->o()Lkotlin/reflect/jvm/internal/n;

    move-result-object v1

    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/n;->a()Ljava/lang/Class;

    move-result-object v1

    goto :goto_3

    :cond_2
    move-object v0, v2

    .line 71
    goto :goto_1

    .line 73
    :catch_0
    move-exception v0

    move-object v0, v2

    .line 74
    goto :goto_1

    :cond_3
    move-object v0, v2

    .line 61
    goto :goto_2

    .line 78
    :cond_4
    instance-of v0, v1, Lkotlin/reflect/jvm/internal/i$a;

    if-eqz v0, :cond_5

    check-cast v1, Lkotlin/reflect/jvm/internal/i$a;

    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/i$a;->b()Ljava/lang/reflect/Field;

    move-result-object v0

    goto :goto_2

    .line 79
    :cond_5
    instance-of v0, v1, Lkotlin/reflect/jvm/internal/i$b;

    if-eqz v0, :cond_6

    move-object v0, v2

    goto :goto_2

    :cond_6
    new-instance v0, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {v0}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw v0
.end method

.method public synthetic v_()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 35
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/w$f;->b()Ljava/lang/reflect/Field;

    move-result-object v0

    return-object v0
.end method
