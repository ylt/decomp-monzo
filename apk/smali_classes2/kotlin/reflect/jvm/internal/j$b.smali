.class final Lkotlin/reflect/jvm/internal/j$b;
.super Lkotlin/d/b/m;
.source "KCallableImpl.kt"

# interfaces
.implements Lkotlin/d/a/a;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lkotlin/reflect/jvm/internal/j;-><init>()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/d/b/m;",
        "Lkotlin/d/a/a",
        "<",
        "Ljava/util/ArrayList",
        "<",
        "Lkotlin/reflect/k;",
        ">;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x1
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u0008\u0012\u0004\u0012\u00020\u00020\u0001\"\u0006\u0008\u0000\u0010\u0003 \u0001H\n\u00a2\u0006\u0002\u0008\u0004"
    }
    d2 = {
        "<anonymous>",
        "Ljava/util/ArrayList;",
        "Lkotlin/reflect/KParameter;",
        "R",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x5
    }
.end annotation


# instance fields
.field final synthetic a:Lkotlin/reflect/jvm/internal/j;


# direct methods
.method constructor <init>(Lkotlin/reflect/jvm/internal/j;)V
    .locals 1

    iput-object p1, p0, Lkotlin/reflect/jvm/internal/j$b;->a:Lkotlin/reflect/jvm/internal/j;

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lkotlin/d/b/m;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final b()Ljava/util/ArrayList;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lkotlin/reflect/k;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 46
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/j$b;->a:Lkotlin/reflect/jvm/internal/j;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/j;->d()Lkotlin/reflect/jvm/internal/impl/b/b;

    move-result-object v6

    .line 47
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 50
    invoke-interface {v6}, Lkotlin/reflect/jvm/internal/impl/b/b;->e()Lkotlin/reflect/jvm/internal/impl/b/aj;

    move-result-object v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lkotlin/reflect/jvm/internal/j$b;->a:Lkotlin/reflect/jvm/internal/j;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/j;->p()Z

    move-result v0

    if-nez v0, :cond_3

    .line 51
    new-instance v2, Lkotlin/reflect/jvm/internal/s;

    iget-object v5, p0, Lkotlin/reflect/jvm/internal/j$b;->a:Lkotlin/reflect/jvm/internal/j;

    sget-object v7, Lkotlin/reflect/k$a;->a:Lkotlin/reflect/k$a;

    new-instance v0, Lkotlin/reflect/jvm/internal/j$b$1;

    invoke-direct {v0, v6}, Lkotlin/reflect/jvm/internal/j$b$1;-><init>(Lkotlin/reflect/jvm/internal/impl/b/b;)V

    check-cast v0, Lkotlin/d/a/a;

    invoke-direct {v2, v5, v4, v7, v0}, Lkotlin/reflect/jvm/internal/s;-><init>(Lkotlin/reflect/jvm/internal/j;ILkotlin/reflect/k$a;Lkotlin/d/a/a;)V

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move v2, v3

    .line 54
    :goto_0
    invoke-interface {v6}, Lkotlin/reflect/jvm/internal/impl/b/b;->d()Lkotlin/reflect/jvm/internal/impl/b/aj;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lkotlin/reflect/jvm/internal/j$b;->a:Lkotlin/reflect/jvm/internal/j;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/j;->p()Z

    move-result v0

    if-nez v0, :cond_0

    .line 55
    new-instance v7, Lkotlin/reflect/jvm/internal/s;

    iget-object v8, p0, Lkotlin/reflect/jvm/internal/j$b;->a:Lkotlin/reflect/jvm/internal/j;

    add-int/lit8 v5, v2, 0x1

    sget-object v9, Lkotlin/reflect/k$a;->b:Lkotlin/reflect/k$a;

    new-instance v0, Lkotlin/reflect/jvm/internal/j$b$2;

    invoke-direct {v0, v6}, Lkotlin/reflect/jvm/internal/j$b$2;-><init>(Lkotlin/reflect/jvm/internal/impl/b/b;)V

    check-cast v0, Lkotlin/d/a/a;

    invoke-direct {v7, v8, v2, v9, v0}, Lkotlin/reflect/jvm/internal/s;-><init>(Lkotlin/reflect/jvm/internal/j;ILkotlin/reflect/k$a;Lkotlin/d/a/a;)V

    invoke-virtual {v1, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move v2, v5

    .line 58
    :cond_0
    invoke-interface {v6}, Lkotlin/reflect/jvm/internal/impl/b/b;->i()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->size()I

    move-result v0

    add-int/lit8 v7, v0, -0x1

    if-gt v4, v7, :cond_1

    .line 59
    :goto_1
    new-instance v8, Lkotlin/reflect/jvm/internal/s;

    iget-object v9, p0, Lkotlin/reflect/jvm/internal/j$b;->a:Lkotlin/reflect/jvm/internal/j;

    add-int/lit8 v5, v2, 0x1

    sget-object v10, Lkotlin/reflect/k$a;->c:Lkotlin/reflect/k$a;

    new-instance v0, Lkotlin/reflect/jvm/internal/j$b$3;

    invoke-direct {v0, v6, v4}, Lkotlin/reflect/jvm/internal/j$b$3;-><init>(Lkotlin/reflect/jvm/internal/impl/b/b;I)V

    check-cast v0, Lkotlin/d/a/a;

    invoke-direct {v8, v9, v2, v10, v0}, Lkotlin/reflect/jvm/internal/s;-><init>(Lkotlin/reflect/jvm/internal/j;ILkotlin/reflect/k$a;Lkotlin/d/a/a;)V

    invoke-virtual {v1, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 58
    if-eq v4, v7, :cond_1

    add-int/lit8 v4, v4, 0x1

    move v2, v5

    goto :goto_1

    .line 65
    :cond_1
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/j$b;->a:Lkotlin/reflect/jvm/internal/j;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/j;->q()Z

    move-result v0

    if-eqz v0, :cond_2

    instance-of v0, v6, Lkotlin/reflect/jvm/internal/impl/d/a/b/a;

    if-eqz v0, :cond_2

    move-object v0, v1

    .line 66
    check-cast v0, Ljava/util/List;

    .line 202
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    if-le v2, v3, :cond_2

    new-instance v2, Lkotlin/reflect/jvm/internal/j$b$a;

    invoke-direct {v2}, Lkotlin/reflect/jvm/internal/j$b$a;-><init>()V

    check-cast v2, Ljava/util/Comparator;

    invoke-static {v0, v2}, Lkotlin/a/m;->a(Ljava/util/List;Ljava/util/Comparator;)V

    .line 69
    :cond_2
    invoke-virtual {v1}, Ljava/util/ArrayList;->trimToSize()V

    .line 70
    return-object v1

    :cond_3
    move v2, v4

    goto :goto_0
.end method

.method public synthetic v_()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 28
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/j$b;->b()Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method
