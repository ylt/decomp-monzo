.class public final Lkotlin/reflect/jvm/internal/x;
.super Ljava/lang/Object;
.source "KPropertyImpl.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x1
    }
    d1 = {
        "\u0000\u0012\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\u001a \u0010\u0000\u001a\u0006\u0012\u0002\u0008\u00030\u0001*\n\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u00022\u0006\u0010\u0003\u001a\u00020\u0004H\u0002\u00a8\u0006\u0005"
    }
    d2 = {
        "computeCallerForAccessor",
        "Lkotlin/reflect/jvm/internal/FunctionCaller;",
        "Lkotlin/reflect/jvm/internal/KPropertyImpl$Accessor;",
        "isGetter",
        "",
        "kotlin-reflection"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x5
    }
.end annotation


# direct methods
.method public static final synthetic a(Lkotlin/reflect/jvm/internal/w$a;Z)Lkotlin/reflect/jvm/internal/f;
    .locals 1

    .prologue
    .line 1
    invoke-static {p0, p1}, Lkotlin/reflect/jvm/internal/x;->b(Lkotlin/reflect/jvm/internal/w$a;Z)Lkotlin/reflect/jvm/internal/f;

    move-result-object v0

    return-object v0
.end method

.method private static final b(Lkotlin/reflect/jvm/internal/w$a;Z)Lkotlin/reflect/jvm/internal/f;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/reflect/jvm/internal/w$a",
            "<**>;Z)",
            "Lkotlin/reflect/jvm/internal/f",
            "<*>;"
        }
    .end annotation

    .prologue
    const/4 v6, 0x0

    .line 181
    new-instance v2, Lkotlin/reflect/jvm/internal/x$a;

    invoke-direct {v2, p0}, Lkotlin/reflect/jvm/internal/x$a;-><init>(Lkotlin/reflect/jvm/internal/w$a;)V

    .line 189
    new-instance v5, Lkotlin/reflect/jvm/internal/x$b;

    invoke-direct {v5, p0}, Lkotlin/reflect/jvm/internal/x$b;-><init>(Lkotlin/reflect/jvm/internal/w$a;)V

    .line 192
    new-instance v4, Lkotlin/reflect/jvm/internal/x$c;

    invoke-direct {v4, p0}, Lkotlin/reflect/jvm/internal/x$c;-><init>(Lkotlin/reflect/jvm/internal/w$a;)V

    .line 195
    new-instance v0, Lkotlin/reflect/jvm/internal/x$d;

    move-object v1, p0

    move v3, p1

    invoke-direct/range {v0 .. v5}, Lkotlin/reflect/jvm/internal/x$d;-><init>(Lkotlin/reflect/jvm/internal/w$a;Lkotlin/reflect/jvm/internal/x$a;ZLkotlin/reflect/jvm/internal/x$c;Lkotlin/reflect/jvm/internal/x$b;)V

    .line 224
    sget-object v1, Lkotlin/reflect/jvm/internal/ai;->a:Lkotlin/reflect/jvm/internal/ai;

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/w$a;->e()Lkotlin/reflect/jvm/internal/w;

    move-result-object v2

    invoke-virtual {v2}, Lkotlin/reflect/jvm/internal/w;->u()Lkotlin/reflect/jvm/internal/impl/b/ag;

    move-result-object v2

    invoke-virtual {v1, v2}, Lkotlin/reflect/jvm/internal/ai;->a(Lkotlin/reflect/jvm/internal/impl/b/ag;)Lkotlin/reflect/jvm/internal/i;

    move-result-object v2

    .line 226
    instance-of v1, v2, Lkotlin/reflect/jvm/internal/i$c;

    if-eqz v1, :cond_b

    move-object v1, v2

    .line 227
    check-cast v1, Lkotlin/reflect/jvm/internal/i$c;

    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/i$c;->d()Lkotlin/reflect/jvm/internal/impl/i/c/c$e;

    move-result-object v1

    check-cast v1, Lkotlin/reflect/jvm/internal/impl/i/c/c$e;

    .line 229
    if-eqz p1, :cond_2

    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/impl/i/c/c$e;->p()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/impl/i/c/c$e;->q()Lkotlin/reflect/jvm/internal/impl/i/c/c$c;

    move-result-object v1

    .line 227
    :goto_0
    check-cast v1, Lkotlin/reflect/jvm/internal/impl/i/c/c$c;

    .line 234
    if-eqz v1, :cond_4

    check-cast v1, Lkotlin/reflect/jvm/internal/impl/i/c/c$c;

    .line 235
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/w$a;->e()Lkotlin/reflect/jvm/internal/w;

    move-result-object v3

    invoke-virtual {v3}, Lkotlin/reflect/jvm/internal/w;->o()Lkotlin/reflect/jvm/internal/n;

    move-result-object v4

    move-object v3, v2

    check-cast v3, Lkotlin/reflect/jvm/internal/i$c;

    invoke-virtual {v3}, Lkotlin/reflect/jvm/internal/i$c;->e()Lkotlin/reflect/jvm/internal/impl/i/b/x;

    move-result-object v3

    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/impl/i/c/c$c;->m()I

    move-result v6

    invoke-interface {v3, v6}, Lkotlin/reflect/jvm/internal/impl/i/b/x;->a(I)Ljava/lang/String;

    move-result-object v3

    const-string v6, "jvmSignature.nameResolve\u2026getString(signature.name)"

    invoke-static {v3, v6}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v2, Lkotlin/reflect/jvm/internal/i$c;

    invoke-virtual {v2}, Lkotlin/reflect/jvm/internal/i$c;->e()Lkotlin/reflect/jvm/internal/impl/i/b/x;

    move-result-object v2

    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/impl/i/c/c$c;->o()I

    move-result v1

    invoke-interface {v2, v1}, Lkotlin/reflect/jvm/internal/impl/i/b/x;->a(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, "jvmSignature.nameResolve\u2026getString(signature.desc)"

    invoke-static {v1, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/w$a;->l()Lkotlin/reflect/jvm/internal/impl/b/af;

    move-result-object v2

    invoke-interface {v2}, Lkotlin/reflect/jvm/internal/impl/b/af;->p()Lkotlin/reflect/jvm/internal/impl/b/ay;

    move-result-object v2

    invoke-static {v2}, Lkotlin/reflect/jvm/internal/impl/b/ax;->a(Lkotlin/reflect/jvm/internal/impl/b/ay;)Z

    move-result v2

    invoke-virtual {v4, v3, v1, v2}, Lkotlin/reflect/jvm/internal/n;->a(Ljava/lang/String;Ljava/lang/String;Z)Ljava/lang/reflect/Method;

    move-result-object v1

    .line 234
    check-cast v1, Ljava/lang/reflect/Method;

    .line 243
    :goto_1
    if-nez v1, :cond_5

    check-cast v0, Lkotlin/reflect/jvm/internal/x$d;

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/w$a;->e()Lkotlin/reflect/jvm/internal/w;

    move-result-object v1

    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/w;->s()Ljava/lang/reflect/Field;

    move-result-object v1

    if-nez v1, :cond_0

    invoke-static {}, Lkotlin/d/b/l;->a()V

    :cond_0
    invoke-virtual {v0, v1}, Lkotlin/reflect/jvm/internal/x$d;->a(Ljava/lang/reflect/Field;)Lkotlin/reflect/jvm/internal/f;

    move-result-object v0

    .line 225
    :goto_2
    return-object v0

    :cond_1
    move-object v1, v6

    .line 229
    goto :goto_0

    .line 230
    :cond_2
    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/impl/i/c/c$e;->r()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/impl/i/c/c$e;->s()Lkotlin/reflect/jvm/internal/impl/i/c/c$c;

    move-result-object v1

    goto :goto_0

    :cond_3
    move-object v1, v6

    goto :goto_0

    :cond_4
    move-object v1, v6

    .line 234
    goto :goto_1

    .line 244
    :cond_5
    invoke-virtual {v1}, Ljava/lang/reflect/Method;->getModifiers()I

    move-result v0

    invoke-static {v0}, Ljava/lang/reflect/Modifier;->isStatic(I)Z

    move-result v0

    if-nez v0, :cond_7

    .line 245
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/w$a;->p()Z

    move-result v0

    if-eqz v0, :cond_6

    new-instance v0, Lkotlin/reflect/jvm/internal/f$f;

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/w$a;->e()Lkotlin/reflect/jvm/internal/w;

    move-result-object v2

    invoke-virtual {v2}, Lkotlin/reflect/jvm/internal/w;->w()Ljava/lang/Object;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lkotlin/reflect/jvm/internal/f$f;-><init>(Ljava/lang/reflect/Method;Ljava/lang/Object;)V

    check-cast v0, Lkotlin/reflect/jvm/internal/f$w;

    :goto_3
    check-cast v0, Lkotlin/reflect/jvm/internal/f;

    goto :goto_2

    .line 246
    :cond_6
    new-instance v0, Lkotlin/reflect/jvm/internal/f$s;

    invoke-direct {v0, v1}, Lkotlin/reflect/jvm/internal/f$s;-><init>(Ljava/lang/reflect/Method;)V

    check-cast v0, Lkotlin/reflect/jvm/internal/f$w;

    goto :goto_3

    .line 247
    :cond_7
    check-cast v5, Lkotlin/reflect/jvm/internal/x$b;

    invoke-virtual {v5}, Lkotlin/reflect/jvm/internal/x$b;->b()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 248
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/w$a;->p()Z

    move-result v0

    if-eqz v0, :cond_8

    new-instance v0, Lkotlin/reflect/jvm/internal/f$g;

    invoke-direct {v0, v1}, Lkotlin/reflect/jvm/internal/f$g;-><init>(Ljava/lang/reflect/Method;)V

    check-cast v0, Lkotlin/reflect/jvm/internal/f$w;

    :goto_4
    check-cast v0, Lkotlin/reflect/jvm/internal/f;

    goto :goto_2

    .line 249
    :cond_8
    new-instance v0, Lkotlin/reflect/jvm/internal/f$t;

    invoke-direct {v0, v1}, Lkotlin/reflect/jvm/internal/f$t;-><init>(Ljava/lang/reflect/Method;)V

    check-cast v0, Lkotlin/reflect/jvm/internal/f$w;

    goto :goto_4

    .line 251
    :cond_9
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/w$a;->p()Z

    move-result v0

    if-eqz v0, :cond_a

    new-instance v0, Lkotlin/reflect/jvm/internal/f$j;

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/w$a;->e()Lkotlin/reflect/jvm/internal/w;

    move-result-object v2

    invoke-virtual {v2}, Lkotlin/reflect/jvm/internal/w;->w()Ljava/lang/Object;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lkotlin/reflect/jvm/internal/f$j;-><init>(Ljava/lang/reflect/Method;Ljava/lang/Object;)V

    check-cast v0, Lkotlin/reflect/jvm/internal/f$w;

    :goto_5
    check-cast v0, Lkotlin/reflect/jvm/internal/f;

    goto :goto_2

    .line 252
    :cond_a
    new-instance v0, Lkotlin/reflect/jvm/internal/f$z;

    invoke-direct {v0, v1}, Lkotlin/reflect/jvm/internal/f$z;-><init>(Ljava/lang/reflect/Method;)V

    check-cast v0, Lkotlin/reflect/jvm/internal/f$w;

    goto :goto_5

    .line 255
    :cond_b
    instance-of v1, v2, Lkotlin/reflect/jvm/internal/i$a;

    if-eqz v1, :cond_c

    .line 256
    check-cast v0, Lkotlin/reflect/jvm/internal/x$d;

    check-cast v2, Lkotlin/reflect/jvm/internal/i$a;

    invoke-virtual {v2}, Lkotlin/reflect/jvm/internal/i$a;->b()Ljava/lang/reflect/Field;

    move-result-object v1

    invoke-virtual {v0, v1}, Lkotlin/reflect/jvm/internal/x$d;->a(Ljava/lang/reflect/Field;)Lkotlin/reflect/jvm/internal/f;

    move-result-object v0

    goto/16 :goto_2

    .line 258
    :cond_c
    instance-of v0, v2, Lkotlin/reflect/jvm/internal/i$b;

    if-eqz v0, :cond_10

    .line 260
    if-eqz p1, :cond_d

    check-cast v2, Lkotlin/reflect/jvm/internal/i$b;

    invoke-virtual {v2}, Lkotlin/reflect/jvm/internal/i$b;->b()Ljava/lang/reflect/Method;

    move-result-object v0

    move-object v1, v0

    .line 264
    :goto_6
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/w$a;->p()Z

    move-result v0

    if-eqz v0, :cond_f

    new-instance v0, Lkotlin/reflect/jvm/internal/f$f;

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/w$a;->e()Lkotlin/reflect/jvm/internal/w;

    move-result-object v2

    invoke-virtual {v2}, Lkotlin/reflect/jvm/internal/w;->w()Ljava/lang/Object;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lkotlin/reflect/jvm/internal/f$f;-><init>(Ljava/lang/reflect/Method;Ljava/lang/Object;)V

    check-cast v0, Lkotlin/reflect/jvm/internal/f$w;

    :goto_7
    check-cast v0, Lkotlin/reflect/jvm/internal/f;

    goto/16 :goto_2

    :cond_d
    move-object v0, v2

    .line 261
    check-cast v0, Lkotlin/reflect/jvm/internal/i$b;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/i$b;->c()Ljava/lang/reflect/Method;

    move-result-object v0

    if-eqz v0, :cond_e

    move-object v1, v0

    goto :goto_6

    :cond_e
    new-instance v0, Lkotlin/reflect/jvm/internal/ac;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "No source found for setter of Java method property: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    check-cast v2, Lkotlin/reflect/jvm/internal/i$b;

    invoke-virtual {v2}, Lkotlin/reflect/jvm/internal/i$b;->b()Ljava/lang/reflect/Method;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lkotlin/reflect/jvm/internal/ac;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    .line 265
    :cond_f
    new-instance v0, Lkotlin/reflect/jvm/internal/f$s;

    invoke-direct {v0, v1}, Lkotlin/reflect/jvm/internal/f$s;-><init>(Ljava/lang/reflect/Method;)V

    check-cast v0, Lkotlin/reflect/jvm/internal/f$w;

    goto :goto_7

    .line 264
    :cond_10
    new-instance v0, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {v0}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw v0
.end method
