.class public final Lkotlin/reflect/jvm/internal/r;
.super Lkotlin/reflect/jvm/internal/v;
.source "KProperty2Impl.kt"

# interfaces
.implements Lkotlin/reflect/j;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lkotlin/reflect/jvm/internal/r$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<D:",
        "Ljava/lang/Object;",
        "E:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Lkotlin/reflect/jvm/internal/v",
        "<TD;TE;TR;>;",
        "Lkotlin/reflect/j",
        "<TD;TE;TR;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x1
    }
    d1 = {
        "\u0000D\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0006\u0008\u0000\u0018\u0000*\u0004\u0008\u0000\u0010\u0001*\u0004\u0008\u0001\u0010\u0002*\u0004\u0008\u0002\u0010\u00032\u0014\u0012\u0004\u0012\u0002H\u0001\u0012\u0004\u0012\u0002H\u0002\u0012\u0004\u0012\u0002H\u00030\u00042\u0014\u0012\u0004\u0012\u0002H\u0001\u0012\u0004\u0012\u0002H\u0002\u0012\u0004\u0012\u0002H\u00030\u0005:\u0001\u001dB\u001f\u0008\u0016\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\t\u00a2\u0006\u0002\u0010\u000bB\u0017\u0008\u0016\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u000c\u001a\u00020\r\u00a2\u0006\u0002\u0010\u000eJ%\u0010\u0017\u001a\u00020\u00182\u0006\u0010\u0019\u001a\u00028\u00002\u0006\u0010\u001a\u001a\u00028\u00012\u0006\u0010\u001b\u001a\u00028\u0002H\u0016\u00a2\u0006\u0002\u0010\u001cR&\u0010\u000f\u001a\u0014\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u0001\u0012\u0004\u0012\u00028\u00020\u00108VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0011\u0010\u0012RE\u0010\u0013\u001a9\u00120\u0012.\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u0001\u0012\u0004\u0012\u00028\u0002 \u0015*\u0016\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u0001\u0012\u0004\u0012\u00028\u0002\u0018\u00010\u00100\u00100\u0014\u00a2\u0006\u0002\u0008\u0016X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001e"
    }
    d2 = {
        "Lkotlin/reflect/jvm/internal/KMutableProperty2Impl;",
        "D",
        "E",
        "R",
        "Lkotlin/reflect/jvm/internal/KProperty2Impl;",
        "Lkotlin/reflect/KMutableProperty2;",
        "container",
        "Lkotlin/reflect/jvm/internal/KDeclarationContainerImpl;",
        "name",
        "",
        "signature",
        "(Lkotlin/reflect/jvm/internal/KDeclarationContainerImpl;Ljava/lang/String;Ljava/lang/String;)V",
        "descriptor",
        "Lorg/jetbrains/kotlin/descriptors/PropertyDescriptor;",
        "(Lkotlin/reflect/jvm/internal/KDeclarationContainerImpl;Lorg/jetbrains/kotlin/descriptors/PropertyDescriptor;)V",
        "setter",
        "Lkotlin/reflect/jvm/internal/KMutableProperty2Impl$Setter;",
        "getSetter",
        "()Lkotlin/reflect/jvm/internal/KMutableProperty2Impl$Setter;",
        "setter_",
        "Lkotlin/reflect/jvm/internal/ReflectProperties$LazyVal;",
        "kotlin.jvm.PlatformType",
        "Lorg/jetbrains/annotations/NotNull;",
        "set",
        "",
        "receiver1",
        "receiver2",
        "value",
        "(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V",
        "Setter",
        "kotlin-reflection"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x5
    }
.end annotation


# instance fields
.field private final b:Lkotlin/reflect/jvm/internal/ae$b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/reflect/jvm/internal/ae$b",
            "<",
            "Lkotlin/reflect/jvm/internal/r$a",
            "<TD;TE;TR;>;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lkotlin/reflect/jvm/internal/n;Lkotlin/reflect/jvm/internal/impl/b/ag;)V
    .locals 1

    .prologue
    const-string v0, "container"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "descriptor"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 50
    invoke-direct {p0, p1, p2}, Lkotlin/reflect/jvm/internal/v;-><init>(Lkotlin/reflect/jvm/internal/n;Lkotlin/reflect/jvm/internal/impl/b/ag;)V

    .line 52
    new-instance v0, Lkotlin/reflect/jvm/internal/r$b;

    invoke-direct {v0, p0}, Lkotlin/reflect/jvm/internal/r$b;-><init>(Lkotlin/reflect/jvm/internal/r;)V

    check-cast v0, Lkotlin/d/a/a;

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/ae;->a(Lkotlin/d/a/a;)Lkotlin/reflect/jvm/internal/ae$b;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/r;->b:Lkotlin/reflect/jvm/internal/ae$b;

    return-void
.end method


# virtual methods
.method public a()Lkotlin/reflect/jvm/internal/r$a;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/reflect/jvm/internal/r$a",
            "<TD;TE;TR;>;"
        }
    .end annotation

    .prologue
    .line 54
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/r;->b:Lkotlin/reflect/jvm/internal/ae$b;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/ae$b;->a()Ljava/lang/Object;

    move-result-object v0

    const-string v1, "setter_()"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lkotlin/reflect/jvm/internal/r$a;

    return-object v0
.end method

.method public a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TD;TE;TR;)V"
        }
    .end annotation

    .prologue
    .line 56
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/r;->a()Lkotlin/reflect/jvm/internal/r$a;

    move-result-object v0

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    const/4 v2, 0x1

    aput-object p2, v1, v2

    const/4 v2, 0x2

    aput-object p3, v1, v2

    invoke-virtual {v0, v1}, Lkotlin/reflect/jvm/internal/r$a;->a([Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public synthetic m()Lkotlin/reflect/g$a;
    .locals 1

    .prologue
    .line 47
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/r;->a()Lkotlin/reflect/jvm/internal/r$a;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/g$a;

    return-object v0
.end method
