.class public final Lkotlin/reflect/jvm/d;
.super Ljava/lang/Object;
.source "reflectLambda.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x1
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u001a\u001e\u0010\u0000\u001a\n\u0012\u0004\u0012\u0002H\u0002\u0018\u00010\u0001\"\u0004\u0008\u0000\u0010\u0002*\u0008\u0012\u0004\u0012\u0002H\u00020\u0003\u00a8\u0006\u0004"
    }
    d2 = {
        "reflect",
        "Lkotlin/reflect/KFunction;",
        "R",
        "Lkotlin/Function;",
        "kotlin-reflection"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x5
    }
.end annotation


# direct methods
.method public static final a(Lkotlin/a;)Lkotlin/reflect/f;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<R:",
            "Ljava/lang/Object;",
            ">(",
            "Lkotlin/a",
            "<+TR;>;)",
            "Lkotlin/reflect/f",
            "<TR;>;"
        }
    .end annotation

    .prologue
    const/4 v6, 0x0

    const-string v0, "$receiver"

    invoke-static {p0, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 41
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-class v1, Lkotlin/Metadata;

    invoke-virtual {v0, v1}, Ljava/lang/Class;->getAnnotation(Ljava/lang/Class;)Ljava/lang/annotation/Annotation;

    move-result-object v0

    check-cast v0, Lkotlin/Metadata;

    if-eqz v0, :cond_0

    .line 42
    invoke-interface {v0}, Lkotlin/Metadata;->d1()[Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lkotlin/reflect/jvm/internal/impl/i/c/a;->a([Ljava/lang/String;)[B

    move-result-object v1

    new-instance v2, Ljava/io/ByteArrayInputStream;

    invoke-direct {v2, v1}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    .line 43
    new-instance v3, Lkotlin/reflect/jvm/internal/impl/d/b/n;

    move-object v1, v2

    check-cast v1, Ljava/io/InputStream;

    sget-object v4, Lkotlin/reflect/jvm/internal/impl/i/c/d;->a:Lkotlin/reflect/jvm/internal/impl/i/c/d;

    invoke-virtual {v4}, Lkotlin/reflect/jvm/internal/impl/i/c/d;->a()Lkotlin/reflect/jvm/internal/impl/protobuf/f;

    move-result-object v4

    invoke-static {v1, v4}, Lkotlin/reflect/jvm/internal/impl/i/c/c$g;->a(Ljava/io/InputStream;Lkotlin/reflect/jvm/internal/impl/protobuf/f;)Lkotlin/reflect/jvm/internal/impl/i/c/c$g;

    move-result-object v1

    const-string v4, "JvmProtoBuf.StringTableT\u2026fUtil.EXTENSION_REGISTRY)"

    invoke-static {v1, v4}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v0}, Lkotlin/Metadata;->d2()[Ljava/lang/String;

    move-result-object v0

    invoke-direct {v3, v1, v0}, Lkotlin/reflect/jvm/internal/impl/d/b/n;-><init>(Lkotlin/reflect/jvm/internal/impl/i/c/c$g;[Ljava/lang/String;)V

    .line 46
    check-cast v2, Ljava/io/InputStream;

    sget-object v0, Lkotlin/reflect/jvm/internal/impl/i/c/d;->a:Lkotlin/reflect/jvm/internal/impl/i/c/d;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/i/c/d;->a()Lkotlin/reflect/jvm/internal/impl/protobuf/f;

    move-result-object v0

    invoke-static {v2, v0}, Lkotlin/reflect/jvm/internal/impl/i/e$i;->a(Ljava/io/InputStream;Lkotlin/reflect/jvm/internal/impl/protobuf/f;)Lkotlin/reflect/jvm/internal/impl/i/e$i;

    move-result-object v9

    .line 47
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/ad;->a(Ljava/lang/Class;)Lkotlin/reflect/jvm/internal/impl/d/b/b/g;

    move-result-object v4

    .line 48
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/i/b/o;

    invoke-virtual {v4}, Lkotlin/reflect/jvm/internal/impl/d/b/b/g;->b()Lkotlin/reflect/jvm/internal/impl/i/b/m;

    move-result-object v1

    move-object v2, v3

    check-cast v2, Lkotlin/reflect/jvm/internal/impl/i/b/x;

    invoke-virtual {v4}, Lkotlin/reflect/jvm/internal/impl/d/b/b/g;->a()Lkotlin/reflect/jvm/internal/impl/b/w;

    move-result-object v3

    check-cast v3, Lkotlin/reflect/jvm/internal/impl/b/m;

    new-instance v4, Lkotlin/reflect/jvm/internal/impl/i/b/ah;

    invoke-virtual {v9}, Lkotlin/reflect/jvm/internal/impl/i/e$i;->H()Lkotlin/reflect/jvm/internal/impl/i/e$ae;

    move-result-object v5

    const-string v7, "proto.typeTable"

    invoke-static {v5, v7}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v4, v5}, Lkotlin/reflect/jvm/internal/impl/i/b/ah;-><init>(Lkotlin/reflect/jvm/internal/impl/i/e$ae;)V

    sget-object v5, Lkotlin/reflect/jvm/internal/impl/i/b/a/n;->a:Lkotlin/reflect/jvm/internal/impl/i/b/a/n$a;

    invoke-virtual {v5}, Lkotlin/reflect/jvm/internal/impl/i/b/a/n$a;->a()Lkotlin/reflect/jvm/internal/impl/i/b/a/n;

    move-result-object v5

    invoke-virtual {v9}, Lkotlin/reflect/jvm/internal/impl/i/e$i;->y()Ljava/util/List;

    move-result-object v8

    const-string v7, "proto.typeParameterList"

    invoke-static {v8, v7}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v7, v6

    invoke-direct/range {v0 .. v8}, Lkotlin/reflect/jvm/internal/impl/i/b/o;-><init>(Lkotlin/reflect/jvm/internal/impl/i/b/m;Lkotlin/reflect/jvm/internal/impl/i/b/x;Lkotlin/reflect/jvm/internal/impl/b/m;Lkotlin/reflect/jvm/internal/impl/i/b/ah;Lkotlin/reflect/jvm/internal/impl/i/b/a/n;Lkotlin/reflect/jvm/internal/impl/i/b/a/f;Lkotlin/reflect/jvm/internal/impl/i/b/ag;Ljava/util/List;)V

    .line 52
    new-instance v1, Lkotlin/reflect/jvm/internal/impl/i/b/w;

    invoke-direct {v1, v0}, Lkotlin/reflect/jvm/internal/impl/i/b/w;-><init>(Lkotlin/reflect/jvm/internal/impl/i/b/o;)V

    const-string v0, "proto"

    invoke-static {v9, v0}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v1, v9}, Lkotlin/reflect/jvm/internal/impl/i/b/w;->a(Lkotlin/reflect/jvm/internal/impl/i/e$i;)Lkotlin/reflect/jvm/internal/impl/b/ak;

    move-result-object v1

    .line 54
    new-instance v2, Lkotlin/reflect/jvm/internal/o;

    sget-object v0, Lkotlin/reflect/jvm/internal/e;->a:Lkotlin/reflect/jvm/internal/e;

    check-cast v0, Lkotlin/reflect/jvm/internal/n;

    check-cast v1, Lkotlin/reflect/jvm/internal/impl/b/s;

    invoke-direct {v2, v0, v1}, Lkotlin/reflect/jvm/internal/o;-><init>(Lkotlin/reflect/jvm/internal/n;Lkotlin/reflect/jvm/internal/impl/b/s;)V

    move-object v0, v2

    check-cast v0, Lkotlin/reflect/f;

    :goto_0
    return-object v0

    :cond_0
    move-object v0, v6

    .line 41
    goto :goto_0
.end method
