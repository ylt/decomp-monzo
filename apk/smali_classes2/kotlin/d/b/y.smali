.class public Lkotlin/d/b/y;
.super Ljava/lang/Object;
.source "Reflection.java"


# static fields
.field private static final a:Lkotlin/d/b/z;

.field private static final b:[Lkotlin/reflect/c;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 33
    :try_start_0
    const-string v0, "kotlin.reflect.jvm.internal.af"

    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    .line 34
    invoke-virtual {v0}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/d/b/z;
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/InstantiationException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_3

    .line 41
    :goto_0
    if-eqz v0, :cond_0

    :goto_1
    sput-object v0, Lkotlin/d/b/y;->a:Lkotlin/d/b/z;

    .line 46
    const/4 v0, 0x0

    new-array v0, v0, [Lkotlin/reflect/c;

    sput-object v0, Lkotlin/d/b/y;->b:[Lkotlin/reflect/c;

    return-void

    .line 36
    :catch_0
    move-exception v0

    move-object v0, v1

    .line 39
    goto :goto_0

    .line 37
    :catch_1
    move-exception v0

    move-object v0, v1

    .line 39
    goto :goto_0

    .line 38
    :catch_2
    move-exception v0

    move-object v0, v1

    .line 39
    goto :goto_0

    :catch_3
    move-exception v0

    move-object v0, v1

    goto :goto_0

    .line 41
    :cond_0
    new-instance v0, Lkotlin/d/b/z;

    invoke-direct {v0}, Lkotlin/d/b/z;-><init>()V

    goto :goto_1
.end method

.method public static a(Lkotlin/d/b/m;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 80
    sget-object v0, Lkotlin/d/b/y;->a:Lkotlin/d/b/z;

    invoke-virtual {v0, p0}, Lkotlin/d/b/z;->a(Lkotlin/d/b/m;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/lang/Class;)Lkotlin/reflect/c;
    .locals 1

    .prologue
    .line 61
    sget-object v0, Lkotlin/d/b/y;->a:Lkotlin/d/b/z;

    invoke-virtual {v0, p0}, Lkotlin/d/b/z;->a(Ljava/lang/Class;)Lkotlin/reflect/c;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lkotlin/d/b/k;)Lkotlin/reflect/f;
    .locals 1

    .prologue
    .line 86
    sget-object v0, Lkotlin/d/b/y;->a:Lkotlin/d/b/z;

    invoke-virtual {v0, p0}, Lkotlin/d/b/z;->a(Lkotlin/d/b/k;)Lkotlin/reflect/f;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lkotlin/d/b/q;)Lkotlin/reflect/i;
    .locals 1

    .prologue
    .line 104
    sget-object v0, Lkotlin/d/b/y;->a:Lkotlin/d/b/z;

    invoke-virtual {v0, p0}, Lkotlin/d/b/z;->a(Lkotlin/d/b/q;)Lkotlin/reflect/i;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lkotlin/d/b/t;)Lkotlin/reflect/m;
    .locals 1

    .prologue
    .line 92
    sget-object v0, Lkotlin/d/b/y;->a:Lkotlin/d/b/z;

    invoke-virtual {v0, p0}, Lkotlin/d/b/z;->a(Lkotlin/d/b/t;)Lkotlin/reflect/m;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lkotlin/d/b/v;)Lkotlin/reflect/n;
    .locals 1

    .prologue
    .line 100
    sget-object v0, Lkotlin/d/b/y;->a:Lkotlin/d/b/z;

    invoke-virtual {v0, p0}, Lkotlin/d/b/z;->a(Lkotlin/d/b/v;)Lkotlin/reflect/n;

    move-result-object v0

    return-object v0
.end method
