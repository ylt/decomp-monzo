.class public abstract Lkotlin/d/b/e;
.super Ljava/lang/Object;
.source "CallableReference.java"

# interfaces
.implements Ljava/io/Serializable;
.implements Lkotlin/reflect/b;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lkotlin/d/b/e$a;
    }
.end annotation


# static fields
.field public static final c:Ljava/lang/Object;


# instance fields
.field private transient a:Lkotlin/reflect/b;

.field protected final b:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 47
    invoke-static {}, Lkotlin/d/b/e$a;->a()Lkotlin/d/b/e$a;

    move-result-object v0

    sput-object v0, Lkotlin/d/b/e;->c:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 59
    sget-object v0, Lkotlin/d/b/e;->c:Ljava/lang/Object;

    invoke-direct {p0, v0}, Lkotlin/d/b/e;-><init>(Ljava/lang/Object;)V

    .line 60
    return-void
.end method

.method protected constructor <init>(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 63
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 64
    iput-object p1, p0, Lkotlin/d/b/e;->b:Ljava/lang/Object;

    .line 65
    return-void
.end method


# virtual methods
.method public a(Ljava/util/Map;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 154
    invoke-virtual {p0}, Lkotlin/d/b/e;->g()Lkotlin/reflect/b;

    move-result-object v0

    invoke-interface {v0, p1}, Lkotlin/reflect/b;->a(Ljava/util/Map;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public varargs a([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 149
    invoke-virtual {p0}, Lkotlin/d/b/e;->g()Lkotlin/reflect/b;

    move-result-object v0

    invoke-interface {v0, p1}, Lkotlin/reflect/b;->a([Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public a()Lkotlin/reflect/e;
    .locals 1

    .prologue
    .line 100
    new-instance v0, Ljava/lang/AbstractMethodError;

    invoke-direct {v0}, Ljava/lang/AbstractMethodError;-><init>()V

    throw v0
.end method

.method public b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 108
    new-instance v0, Ljava/lang/AbstractMethodError;

    invoke-direct {v0}, Ljava/lang/AbstractMethodError;-><init>()V

    throw v0
.end method

.method public c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 120
    new-instance v0, Ljava/lang/AbstractMethodError;

    invoke-direct {v0}, Ljava/lang/AbstractMethodError;-><init>()V

    throw v0
.end method

.method public c_()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lkotlin/d/b/e;->b:Ljava/lang/Object;

    return-object v0
.end method

.method protected abstract d()Lkotlin/reflect/b;
.end method

.method public f()Lkotlin/reflect/b;
    .locals 1

    .prologue
    .line 76
    iget-object v0, p0, Lkotlin/d/b/e;->a:Lkotlin/reflect/b;

    .line 77
    if-nez v0, :cond_0

    .line 78
    invoke-virtual {p0}, Lkotlin/d/b/e;->d()Lkotlin/reflect/b;

    move-result-object v0

    .line 79
    iput-object v0, p0, Lkotlin/d/b/e;->a:Lkotlin/reflect/b;

    .line 81
    :cond_0
    return-object v0
.end method

.method protected g()Lkotlin/reflect/b;
    .locals 1

    .prologue
    .line 86
    invoke-virtual {p0}, Lkotlin/d/b/e;->f()Lkotlin/reflect/b;

    move-result-object v0

    .line 87
    if-ne v0, p0, :cond_0

    .line 88
    new-instance v0, Lkotlin/d/b;

    invoke-direct {v0}, Lkotlin/d/b;-><init>()V

    throw v0

    .line 90
    :cond_0
    return-object v0
.end method

.method public h()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lkotlin/reflect/k;",
            ">;"
        }
    .end annotation

    .prologue
    .line 128
    invoke-virtual {p0}, Lkotlin/d/b/e;->g()Lkotlin/reflect/b;

    move-result-object v0

    invoke-interface {v0}, Lkotlin/reflect/b;->h()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public i()Lkotlin/reflect/p;
    .locals 1

    .prologue
    .line 133
    invoke-virtual {p0}, Lkotlin/d/b/e;->g()Lkotlin/reflect/b;

    move-result-object v0

    invoke-interface {v0}, Lkotlin/reflect/b;->i()Lkotlin/reflect/p;

    move-result-object v0

    return-object v0
.end method

.method public j()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/annotation/Annotation;",
            ">;"
        }
    .end annotation

    .prologue
    .line 138
    invoke-virtual {p0}, Lkotlin/d/b/e;->g()Lkotlin/reflect/b;

    move-result-object v0

    invoke-interface {v0}, Lkotlin/reflect/b;->j()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method
