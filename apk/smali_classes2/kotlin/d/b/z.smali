.class public Lkotlin/d/b/z;
.super Ljava/lang/Object;
.source "ReflectionFactory.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lkotlin/d/b/m;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 47
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getGenericInterfaces()[Ljava/lang/reflect/Type;

    move-result-object v0

    const/4 v1, 0x0

    aget-object v0, v0, v1

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 48
    const-string v1, "kotlin.jvm.functions."

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "kotlin.jvm.functions."

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    :cond_0
    return-object v0
.end method

.method public a(Ljava/lang/Class;)Lkotlin/reflect/c;
    .locals 1

    .prologue
    .line 38
    new-instance v0, Lkotlin/d/b/g;

    invoke-direct {v0, p1}, Lkotlin/d/b/g;-><init>(Ljava/lang/Class;)V

    return-object v0
.end method

.method public a(Lkotlin/d/b/k;)Lkotlin/reflect/f;
    .locals 0

    .prologue
    .line 54
    return-object p1
.end method

.method public a(Lkotlin/d/b/q;)Lkotlin/reflect/i;
    .locals 0

    .prologue
    .line 72
    return-object p1
.end method

.method public a(Lkotlin/d/b/t;)Lkotlin/reflect/m;
    .locals 0

    .prologue
    .line 60
    return-object p1
.end method

.method public a(Lkotlin/d/b/v;)Lkotlin/reflect/n;
    .locals 0

    .prologue
    .line 68
    return-object p1
.end method
