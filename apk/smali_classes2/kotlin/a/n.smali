.class Lkotlin/a/n;
.super Ljava/lang/Object;
.source "Collections.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000v\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u001e\n\u0002\u0008\u0003\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010 \n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010!\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0011\n\u0002\u0008\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u000c\n\u0002\u0010\u0002\n\u0002\u0008\u0007\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000f\n\u0002\u0008\u0007\n\u0002\u0010\u000b\n\u0002\u0018\u0002\n\u0002\u0008\u0008\n\u0002\u0018\u0002\n\u0000\u001a@\u0010\u000b\u001a\u0008\u0012\u0004\u0012\u0002H\u00070\u0008\"\u0004\u0008\u0000\u0010\u00072\u0006\u0010\u000c\u001a\u00020\u00062!\u0010\r\u001a\u001d\u0012\u0013\u0012\u00110\u0006\u00a2\u0006\u000c\u0008\u000f\u0012\u0008\u0008\u0010\u0012\u0004\u0008\u0008(\u0011\u0012\u0004\u0012\u0002H\u00070\u000eH\u0087\u0008\u001a@\u0010\u0012\u001a\u0008\u0012\u0004\u0012\u0002H\u00070\u0013\"\u0004\u0008\u0000\u0010\u00072\u0006\u0010\u000c\u001a\u00020\u00062!\u0010\r\u001a\u001d\u0012\u0013\u0012\u00110\u0006\u00a2\u0006\u000c\u0008\u000f\u0012\u0008\u0008\u0010\u0012\u0004\u0008\u0008(\u0011\u0012\u0004\u0012\u0002H\u00070\u000eH\u0087\u0008\u001a\u001f\u0010\u0014\u001a\u0012\u0012\u0004\u0012\u0002H\u00070\u0015j\u0008\u0012\u0004\u0012\u0002H\u0007`\u0016\"\u0004\u0008\u0000\u0010\u0007H\u0087\u0008\u001a5\u0010\u0014\u001a\u0012\u0012\u0004\u0012\u0002H\u00070\u0015j\u0008\u0012\u0004\u0012\u0002H\u0007`\u0016\"\u0004\u0008\u0000\u0010\u00072\u0012\u0010\u0017\u001a\n\u0012\u0006\u0008\u0001\u0012\u0002H\u00070\u0018\"\u0002H\u0007\u00a2\u0006\u0002\u0010\u0019\u001a\"\u0010\u001a\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\u001b0\u00182\n\u0010\u001c\u001a\u0006\u0012\u0002\u0008\u00030\u0002H\u0081\u0008\u00a2\u0006\u0002\u0010\u001d\u001a4\u0010\u001a\u001a\u0008\u0012\u0004\u0012\u0002H\u00070\u0018\"\u0004\u0008\u0000\u0010\u00072\n\u0010\u001c\u001a\u0006\u0012\u0002\u0008\u00030\u00022\u000c\u0010\u001e\u001a\u0008\u0012\u0004\u0012\u0002H\u00070\u0018H\u0081\u0008\u00a2\u0006\u0002\u0010\u001f\u001a\u0012\u0010 \u001a\u0008\u0012\u0004\u0012\u0002H\u00070\u0008\"\u0004\u0008\u0000\u0010\u0007\u001a\u0015\u0010!\u001a\u0008\u0012\u0004\u0012\u0002H\u00070\u0008\"\u0004\u0008\u0000\u0010\u0007H\u0087\u0008\u001a!\u0010!\u001a\u0008\u0012\u0004\u0012\u0002H\u00070\u0008\"\u0004\u0008\u0000\u0010\u00072\u0006\u0010\"\u001a\u0002H\u0007H\u0007\u00a2\u0006\u0002\u0010#\u001a+\u0010!\u001a\u0008\u0012\u0004\u0012\u0002H\u00070\u0008\"\u0004\u0008\u0000\u0010\u00072\u0012\u0010\u0017\u001a\n\u0012\u0006\u0008\u0001\u0012\u0002H\u00070\u0018\"\u0002H\u0007\u00a2\u0006\u0002\u0010$\u001a%\u0010%\u001a\u0008\u0012\u0004\u0012\u0002H\u00070\u0008\"\u0008\u0008\u0000\u0010\u0007*\u00020\u001b2\u0008\u0010\"\u001a\u0004\u0018\u0001H\u0007\u00a2\u0006\u0002\u0010#\u001a3\u0010%\u001a\u0008\u0012\u0004\u0012\u0002H\u00070\u0008\"\u0008\u0008\u0000\u0010\u0007*\u00020\u001b2\u0016\u0010\u0017\u001a\u000c\u0012\u0008\u0008\u0001\u0012\u0004\u0018\u0001H\u00070\u0018\"\u0004\u0018\u0001H\u0007\u00a2\u0006\u0002\u0010$\u001a\u0015\u0010&\u001a\u0008\u0012\u0004\u0012\u0002H\u00070\u0013\"\u0004\u0008\u0000\u0010\u0007H\u0087\u0008\u001a+\u0010&\u001a\u0008\u0012\u0004\u0012\u0002H\u00070\u0013\"\u0004\u0008\u0000\u0010\u00072\u0012\u0010\u0017\u001a\n\u0012\u0006\u0008\u0001\u0012\u0002H\u00070\u0018\"\u0002H\u0007\u00a2\u0006\u0002\u0010$\u001a%\u0010\'\u001a\u00020(2\u0006\u0010\u000c\u001a\u00020\u00062\u0006\u0010)\u001a\u00020\u00062\u0006\u0010*\u001a\u00020\u0006H\u0002\u00a2\u0006\u0002\u0008+\u001a%\u0010,\u001a\u0008\u0012\u0004\u0012\u0002H\u00070\u0002\"\u0004\u0008\u0000\u0010\u0007*\n\u0012\u0006\u0008\u0001\u0012\u0002H\u00070\u0018H\u0000\u00a2\u0006\u0002\u0010-\u001aS\u0010.\u001a\u00020\u0006\"\u0004\u0008\u0000\u0010\u0007*\u0008\u0012\u0004\u0012\u0002H\u00070\u00082\u0006\u0010\"\u001a\u0002H\u00072\u001a\u0010/\u001a\u0016\u0012\u0006\u0008\u0000\u0012\u0002H\u000700j\n\u0012\u0006\u0008\u0000\u0012\u0002H\u0007`12\u0008\u0008\u0002\u0010)\u001a\u00020\u00062\u0008\u0008\u0002\u0010*\u001a\u00020\u0006\u00a2\u0006\u0002\u00102\u001a>\u0010.\u001a\u00020\u0006\"\u0004\u0008\u0000\u0010\u0007*\u0008\u0012\u0004\u0012\u0002H\u00070\u00082\u0008\u0008\u0002\u0010)\u001a\u00020\u00062\u0008\u0008\u0002\u0010*\u001a\u00020\u00062\u0012\u00103\u001a\u000e\u0012\u0004\u0012\u0002H\u0007\u0012\u0004\u0012\u00020\u00060\u000e\u001aE\u0010.\u001a\u00020\u0006\"\u000e\u0008\u0000\u0010\u0007*\u0008\u0012\u0004\u0012\u0002H\u000704*\n\u0012\u0006\u0012\u0004\u0018\u0001H\u00070\u00082\u0008\u0010\"\u001a\u0004\u0018\u0001H\u00072\u0008\u0008\u0002\u0010)\u001a\u00020\u00062\u0008\u0008\u0002\u0010*\u001a\u00020\u0006\u00a2\u0006\u0002\u00105\u001ad\u00106\u001a\u00020\u0006\"\u0004\u0008\u0000\u0010\u0007\"\u000e\u0008\u0001\u00107*\u0008\u0012\u0004\u0012\u0002H704*\u0008\u0012\u0004\u0012\u0002H\u00070\u00082\u0008\u00108\u001a\u0004\u0018\u0001H72\u0008\u0008\u0002\u0010)\u001a\u00020\u00062\u0008\u0008\u0002\u0010*\u001a\u00020\u00062\u0016\u0008\u0004\u00109\u001a\u0010\u0012\u0004\u0012\u0002H\u0007\u0012\u0006\u0012\u0004\u0018\u0001H70\u000eH\u0086\u0008\u00a2\u0006\u0002\u0010:\u001a,\u0010;\u001a\u00020<\"\t\u0008\u0000\u0010\u0007\u00a2\u0006\u0002\u0008=*\u0008\u0012\u0004\u0012\u0002H\u00070\u00022\u000c\u0010\u0017\u001a\u0008\u0012\u0004\u0012\u0002H\u00070\u0002H\u0087\u0008\u001a1\u0010>\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\u001b0\u0018\"\u0004\u0008\u0000\u0010\u0007*\n\u0012\u0006\u0008\u0001\u0012\u0002H\u00070\u00182\u0006\u0010?\u001a\u00020<H\u0003\u00a2\u0006\u0004\u0008@\u0010A\u001a\u0019\u0010B\u001a\u00020<\"\u0004\u0008\u0000\u0010\u0007*\u0008\u0012\u0004\u0012\u0002H\u00070\u0002H\u0087\u0008\u001a\u001e\u0010C\u001a\u0008\u0012\u0004\u0012\u0002H\u00070\u0008\"\u0004\u0008\u0000\u0010\u0007*\u0008\u0012\u0004\u0012\u0002H\u00070\u0008H\u0000\u001a!\u0010D\u001a\u0008\u0012\u0004\u0012\u0002H\u00070\u0002\"\u0004\u0008\u0000\u0010\u0007*\n\u0012\u0004\u0012\u0002H\u0007\u0018\u00010\u0002H\u0087\u0008\u001a!\u0010D\u001a\u0008\u0012\u0004\u0012\u0002H\u00070\u0008\"\u0004\u0008\u0000\u0010\u0007*\n\u0012\u0004\u0012\u0002H\u0007\u0018\u00010\u0008H\u0087\u0008\u001a\u001f\u0010E\u001a\u0008\u0012\u0004\u0012\u0002H\u00070\u0008\"\u0004\u0008\u0000\u0010\u0007*\u0008\u0012\u0004\u0012\u0002H\u00070FH\u0087\u0008\"\u0019\u0010\u0000\u001a\u00020\u0001*\u0006\u0012\u0002\u0008\u00030\u00028F\u00a2\u0006\u0006\u001a\u0004\u0008\u0003\u0010\u0004\"!\u0010\u0005\u001a\u00020\u0006\"\u0004\u0008\u0000\u0010\u0007*\u0008\u0012\u0004\u0012\u0002H\u00070\u00088F\u00a2\u0006\u0006\u001a\u0004\u0008\t\u0010\n\u00a8\u0006G"
    }
    d2 = {
        "indices",
        "Lkotlin/ranges/IntRange;",
        "",
        "getIndices",
        "(Ljava/util/Collection;)Lkotlin/ranges/IntRange;",
        "lastIndex",
        "",
        "T",
        "",
        "getLastIndex",
        "(Ljava/util/List;)I",
        "List",
        "size",
        "init",
        "Lkotlin/Function1;",
        "Lkotlin/ParameterName;",
        "name",
        "index",
        "MutableList",
        "",
        "arrayListOf",
        "Ljava/util/ArrayList;",
        "Lkotlin/collections/ArrayList;",
        "elements",
        "",
        "([Ljava/lang/Object;)Ljava/util/ArrayList;",
        "copyToArrayImpl",
        "",
        "collection",
        "(Ljava/util/Collection;)[Ljava/lang/Object;",
        "array",
        "(Ljava/util/Collection;[Ljava/lang/Object;)[Ljava/lang/Object;",
        "emptyList",
        "listOf",
        "element",
        "(Ljava/lang/Object;)Ljava/util/List;",
        "([Ljava/lang/Object;)Ljava/util/List;",
        "listOfNotNull",
        "mutableListOf",
        "rangeCheck",
        "",
        "fromIndex",
        "toIndex",
        "rangeCheck$CollectionsKt__CollectionsKt",
        "asCollection",
        "([Ljava/lang/Object;)Ljava/util/Collection;",
        "binarySearch",
        "comparator",
        "Ljava/util/Comparator;",
        "Lkotlin/Comparator;",
        "(Ljava/util/List;Ljava/lang/Object;Ljava/util/Comparator;II)I",
        "comparison",
        "",
        "(Ljava/util/List;Ljava/lang/Comparable;II)I",
        "binarySearchBy",
        "K",
        "key",
        "selector",
        "(Ljava/util/List;Ljava/lang/Comparable;IILkotlin/jvm/functions/Function1;)I",
        "containsAll",
        "",
        "Lkotlin/internal/OnlyInputTypes;",
        "copyToArrayOfAny",
        "isVarargs",
        "copyToArrayOfAny$CollectionsKt__CollectionsKt",
        "([Ljava/lang/Object;Z)[Ljava/lang/Object;",
        "isNotEmpty",
        "optimizeReadOnlyList",
        "orEmpty",
        "toList",
        "Ljava/util/Enumeration;",
        "kotlin-stdlib"
    }
    k = 0x5
    mv = {
        0x1,
        0x1,
        0x7
    }
    xi = 0x1
    xs = "kotlin/collections/CollectionsKt"
.end annotation


# direct methods
.method public static final a(Ljava/util/List;)I
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/List",
            "<+TT;>;)I"
        }
    .end annotation

    .prologue
    const-string v0, "$receiver"

    invoke-static {p0, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 149
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    return v0
.end method

.method public static final a([Ljava/lang/Object;)Ljava/util/Collection;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">([TT;)",
            "Ljava/util/Collection",
            "<TT;>;"
        }
    .end annotation

    .prologue
    const-string v0, "$receiver"

    invoke-static {p0, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 64
    new-instance v0, Lkotlin/a/f;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lkotlin/a/f;-><init>([Ljava/lang/Object;Z)V

    check-cast v0, Ljava/util/Collection;

    return-object v0
.end method

.method public static final a()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">()",
            "Ljava/util/List",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 77
    sget-object v0, Lkotlin/a/u;->a:Lkotlin/a/u;

    check-cast v0, Ljava/util/List;

    return-object v0
.end method

.method public static final a(Ljava/lang/Object;)Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(TT;)",
            "Ljava/util/List",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 91
    invoke-static {p0}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    const-string v1, "java.util.Collections.singletonList(element)"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public static final a(Ljava/util/Collection;)Lkotlin/f/c;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<*>;)",
            "Lkotlin/f/c;"
        }
    .end annotation

    .prologue
    const-string v0, "$receiver"

    invoke-static {p0, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 141
    const/4 v0, 0x0

    new-instance v1, Lkotlin/f/c;

    invoke-interface {p0}, Ljava/util/Collection;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-direct {v1, v0, v2}, Lkotlin/f/c;-><init>(II)V

    return-object v1
.end method

.method public static final synthetic a([Ljava/lang/Object;Z)[Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1
    invoke-static {p0, p1}, Lkotlin/a/n;->b([Ljava/lang/Object;Z)[Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public static final b(Ljava/lang/Object;)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(TT;)",
            "Ljava/util/List",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 112
    if-eqz p0, :cond_0

    invoke-static {p0}, Lkotlin/a/m;->a(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lkotlin/a/m;->a()Ljava/util/List;

    move-result-object v0

    goto :goto_0
.end method

.method public static final b(Ljava/util/List;)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/List",
            "<+TT;>;)",
            "Ljava/util/List",
            "<TT;>;"
        }
    .end annotation

    .prologue
    const-string v0, "$receiver"

    invoke-static {p0, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 180
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :goto_0
    return-object p0

    .line 181
    :pswitch_0
    invoke-static {}, Lkotlin/a/m;->a()Ljava/util/List;

    move-result-object p0

    goto :goto_0

    .line 182
    :pswitch_1
    const/4 v0, 0x0

    invoke-interface {p0, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lkotlin/a/m;->a(Ljava/lang/Object;)Ljava/util/List;

    move-result-object p0

    goto :goto_0

    .line 180
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static final varargs b([Ljava/lang/Object;)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">([TT;)",
            "Ljava/util/List",
            "<TT;>;"
        }
    .end annotation

    .prologue
    const-string v0, "elements"

    invoke-static {p0, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 80
    array-length v0, p0

    if-lez v0, :cond_0

    invoke-static {p0}, Lkotlin/a/g;->m([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lkotlin/a/m;->a()Ljava/util/List;

    move-result-object v0

    goto :goto_0
.end method

.method private static final b([Ljava/lang/Object;Z)[Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">([TT;Z)[",
            "Ljava/lang/Object;"
        }
    .end annotation

    .prologue
    .line 199
    if-eqz p1, :cond_0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-class v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 201
    if-nez p0, :cond_1

    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type kotlin.Array<kotlin.Any?>"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 203
    :cond_0
    array-length v0, p0

    const-class v1, [Ljava/lang/Object;

    invoke-static {p0, v0, v1}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;ILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object p0

    const-string v0, "java.util.Arrays.copyOf(\u2026 Array<Any?>::class.java)"

    invoke-static {p0, v0}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 199
    :cond_1
    return-object p0
.end method

.method public static final varargs c([Ljava/lang/Object;)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">([TT;)",
            "Ljava/util/List",
            "<TT;>;"
        }
    .end annotation

    .prologue
    const-string v0, "elements"

    invoke-static {p0, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 115
    invoke-static {p0}, Lkotlin/a/g;->e([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method
