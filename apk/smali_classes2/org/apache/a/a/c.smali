.class public final Lorg/apache/a/a/c;
.super Ljava/lang/Object;
.source "CSVPrinter.java"

# interfaces
.implements Ljava/io/Closeable;
.implements Ljava/io/Flushable;


# instance fields
.field private final a:Ljava/lang/Appendable;

.field private final b:Lorg/apache/a/a/b;

.field private c:Z


# direct methods
.method public constructor <init>(Ljava/lang/Appendable;Lorg/apache/a/a/b;)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 58
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/apache/a/a/c;->c:Z

    .line 59
    const-string v0, "out"

    invoke-static {p1, v0}, Lorg/apache/a/a/a;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 60
    const-string v0, "format"

    invoke-static {p2, v0}, Lorg/apache/a/a/a;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 62
    iput-object p1, p0, Lorg/apache/a/a/c;->a:Ljava/lang/Appendable;

    .line 63
    iput-object p2, p0, Lorg/apache/a/a/c;->b:Lorg/apache/a/a/b;

    .line 66
    invoke-virtual {p2}, Lorg/apache/a/a/b;->e()[Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 67
    invoke-virtual {p2}, Lorg/apache/a/a/b;->e()[Ljava/lang/String;

    move-result-object v1

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 68
    if-eqz v3, :cond_0

    .line 69
    invoke-virtual {p0, v3}, Lorg/apache/a/a/c;->a(Ljava/lang/String;)V

    .line 67
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 73
    :cond_1
    invoke-virtual {p2}, Lorg/apache/a/a/b;->d()[Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {p2}, Lorg/apache/a/a/b;->k()Z

    move-result v0

    if-nez v0, :cond_2

    .line 74
    invoke-virtual {p2}, Lorg/apache/a/a/b;->d()[Ljava/lang/String;

    move-result-object v0

    check-cast v0, [Ljava/lang/Object;

    invoke-virtual {p0, v0}, Lorg/apache/a/a/c;->a([Ljava/lang/Object;)V

    .line 76
    :cond_2
    return-void
.end method


# virtual methods
.method public a()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 176
    iget-object v0, p0, Lorg/apache/a/a/c;->b:Lorg/apache/a/a/b;

    iget-object v1, p0, Lorg/apache/a/a/c;->a:Ljava/lang/Appendable;

    invoke-virtual {v0, v1}, Lorg/apache/a/a/b;->a(Ljava/lang/Appendable;)V

    .line 177
    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/apache/a/a/c;->c:Z

    .line 178
    return-void
.end method

.method public a(Ljava/lang/Iterable;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<*>;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 194
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 195
    invoke-virtual {p0, v1}, Lorg/apache/a/a/c;->a(Ljava/lang/Object;)V

    goto :goto_0

    .line 197
    :cond_0
    invoke-virtual {p0}, Lorg/apache/a/a/c;->a()V

    .line 198
    return-void
.end method

.method public a(Ljava/lang/Object;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 120
    iget-object v0, p0, Lorg/apache/a/a/c;->b:Lorg/apache/a/a/b;

    iget-object v1, p0, Lorg/apache/a/a/c;->a:Ljava/lang/Appendable;

    iget-boolean v2, p0, Lorg/apache/a/a/c;->c:Z

    invoke-virtual {v0, p1, v1, v2}, Lorg/apache/a/a/b;->a(Ljava/lang/Object;Ljava/lang/Appendable;Z)V

    .line 121
    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/apache/a/a/c;->c:Z

    .line 122
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/16 v3, 0x20

    .line 140
    iget-object v0, p0, Lorg/apache/a/a/c;->b:Lorg/apache/a/a/b;

    invoke-virtual {v0}, Lorg/apache/a/a/b;->n()Z

    move-result v0

    if-nez v0, :cond_0

    .line 167
    :goto_0
    return-void

    .line 143
    :cond_0
    iget-boolean v0, p0, Lorg/apache/a/a/c;->c:Z

    if-nez v0, :cond_1

    .line 144
    invoke-virtual {p0}, Lorg/apache/a/a/c;->a()V

    .line 146
    :cond_1
    iget-object v0, p0, Lorg/apache/a/a/c;->a:Ljava/lang/Appendable;

    iget-object v1, p0, Lorg/apache/a/a/c;->b:Lorg/apache/a/a/b;

    invoke-virtual {v1}, Lorg/apache/a/a/b;->a()Ljava/lang/Character;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Character;->charValue()C

    move-result v1

    invoke-interface {v0, v1}, Ljava/lang/Appendable;->append(C)Ljava/lang/Appendable;

    .line 147
    iget-object v0, p0, Lorg/apache/a/a/c;->a:Ljava/lang/Appendable;

    invoke-interface {v0, v3}, Ljava/lang/Appendable;->append(C)Ljava/lang/Appendable;

    .line 148
    const/4 v0, 0x0

    :goto_1
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    if-ge v0, v1, :cond_3

    .line 149
    invoke-virtual {p1, v0}, Ljava/lang/String;->charAt(I)C

    move-result v1

    .line 150
    packed-switch v1, :pswitch_data_0

    .line 162
    :pswitch_0
    iget-object v2, p0, Lorg/apache/a/a/c;->a:Ljava/lang/Appendable;

    invoke-interface {v2, v1}, Ljava/lang/Appendable;->append(C)Ljava/lang/Appendable;

    .line 148
    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 152
    :pswitch_1
    add-int/lit8 v1, v0, 0x1

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    if-ge v1, v2, :cond_2

    add-int/lit8 v1, v0, 0x1

    invoke-virtual {p1, v1}, Ljava/lang/String;->charAt(I)C

    move-result v1

    const/16 v2, 0xa

    if-ne v1, v2, :cond_2

    .line 153
    add-int/lit8 v0, v0, 0x1

    .line 157
    :cond_2
    :pswitch_2
    invoke-virtual {p0}, Lorg/apache/a/a/c;->a()V

    .line 158
    iget-object v1, p0, Lorg/apache/a/a/c;->a:Ljava/lang/Appendable;

    iget-object v2, p0, Lorg/apache/a/a/c;->b:Lorg/apache/a/a/b;

    invoke-virtual {v2}, Lorg/apache/a/a/b;->a()Ljava/lang/Character;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Character;->charValue()C

    move-result v2

    invoke-interface {v1, v2}, Ljava/lang/Appendable;->append(C)Ljava/lang/Appendable;

    .line 159
    iget-object v1, p0, Lorg/apache/a/a/c;->a:Ljava/lang/Appendable;

    invoke-interface {v1, v3}, Ljava/lang/Appendable;->append(C)Ljava/lang/Appendable;

    goto :goto_2

    .line 166
    :cond_3
    invoke-virtual {p0}, Lorg/apache/a/a/c;->a()V

    goto :goto_0

    .line 150
    :pswitch_data_0
    .packed-switch 0xa
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public varargs a([Ljava/lang/Object;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 214
    iget-object v0, p0, Lorg/apache/a/a/c;->b:Lorg/apache/a/a/b;

    iget-object v1, p0, Lorg/apache/a/a/c;->a:Ljava/lang/Appendable;

    invoke-virtual {v0, v1, p1}, Lorg/apache/a/a/b;->a(Ljava/lang/Appendable;[Ljava/lang/Object;)V

    .line 215
    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/apache/a/a/c;->c:Z

    .line 216
    return-void
.end method

.method public b(Ljava/lang/Iterable;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<*>;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 258
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .line 259
    instance-of v2, v0, [Ljava/lang/Object;

    if-eqz v2, :cond_0

    .line 260
    check-cast v0, [Ljava/lang/Object;

    check-cast v0, [Ljava/lang/Object;

    invoke-virtual {p0, v0}, Lorg/apache/a/a/c;->a([Ljava/lang/Object;)V

    goto :goto_0

    .line 261
    :cond_0
    instance-of v2, v0, Ljava/lang/Iterable;

    if-eqz v2, :cond_1

    .line 262
    check-cast v0, Ljava/lang/Iterable;

    invoke-virtual {p0, v0}, Lorg/apache/a/a/c;->a(Ljava/lang/Iterable;)V

    goto :goto_0

    .line 264
    :cond_1
    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object v0, v2, v3

    invoke-virtual {p0, v2}, Lorg/apache/a/a/c;->a([Ljava/lang/Object;)V

    goto :goto_0

    .line 267
    :cond_2
    return-void
.end method

.method public close()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 84
    iget-object v0, p0, Lorg/apache/a/a/c;->a:Ljava/lang/Appendable;

    instance-of v0, v0, Ljava/io/Closeable;

    if-eqz v0, :cond_0

    .line 85
    iget-object v0, p0, Lorg/apache/a/a/c;->a:Ljava/lang/Appendable;

    check-cast v0, Ljava/io/Closeable;

    invoke-interface {v0}, Ljava/io/Closeable;->close()V

    .line 87
    :cond_0
    return-void
.end method

.method public flush()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 97
    iget-object v0, p0, Lorg/apache/a/a/c;->a:Ljava/lang/Appendable;

    instance-of v0, v0, Ljava/io/Flushable;

    if-eqz v0, :cond_0

    .line 98
    iget-object v0, p0, Lorg/apache/a/a/c;->a:Ljava/lang/Appendable;

    check-cast v0, Ljava/io/Flushable;

    invoke-interface {v0}, Ljava/io/Flushable;->flush()V

    .line 100
    :cond_0
    return-void
.end method
