.class public final Lorg/apache/a/a/b;
.super Ljava/lang/Object;
.source "CSVFormat.java"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field public static final a:Lorg/apache/a/a/b;

.field public static final b:Lorg/apache/a/a/b;

.field public static final c:Lorg/apache/a/a/b;

.field public static final d:Lorg/apache/a/a/b;

.field public static final e:Lorg/apache/a/a/b;

.field public static final f:Lorg/apache/a/a/b;

.field public static final g:Lorg/apache/a/a/b;

.field public static final h:Lorg/apache/a/a/b;

.field public static final i:Lorg/apache/a/a/b;


# instance fields
.field private final j:Z

.field private final k:Ljava/lang/Character;

.field private final l:C

.field private final m:Ljava/lang/Character;

.field private final n:[Ljava/lang/String;

.field private final o:[Ljava/lang/String;

.field private final p:Z

.field private final q:Z

.field private final r:Z

.field private final s:Ljava/lang/String;

.field private final t:Ljava/lang/Character;

.field private final u:Lorg/apache/a/a/e;

.field private final v:Ljava/lang/String;

.field private final w:Z

.field private final x:Z

.field private final y:Z


# direct methods
.method static constructor <clinit>()V
    .locals 17

    .prologue
    .line 244
    new-instance v0, Lorg/apache/a/a/b;

    const/16 v1, 0x2c

    sget-object v2, Lorg/apache/a/a/d;->a:Ljava/lang/Character;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x1

    const-string v8, "\r\n"

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    invoke-direct/range {v0 .. v16}, Lorg/apache/a/a/b;-><init>(CLjava/lang/Character;Lorg/apache/a/a/e;Ljava/lang/Character;Ljava/lang/Character;ZZLjava/lang/String;Ljava/lang/String;[Ljava/lang/Object;[Ljava/lang/String;ZZZZZ)V

    sput-object v0, Lorg/apache/a/a/b;->a:Lorg/apache/a/a/b;

    .line 277
    sget-object v0, Lorg/apache/a/a/b;->a:Lorg/apache/a/a/b;

    const/4 v1, 0x0

    .line 278
    invoke-virtual {v0, v1}, Lorg/apache/a/a/b;->b(Z)Lorg/apache/a/a/b;

    move-result-object v0

    .line 279
    invoke-virtual {v0}, Lorg/apache/a/a/b;->r()Lorg/apache/a/a/b;

    move-result-object v0

    sput-object v0, Lorg/apache/a/a/b;->b:Lorg/apache/a/a/b;

    .line 307
    sget-object v0, Lorg/apache/a/a/b;->a:Lorg/apache/a/a/b;

    const/16 v1, 0x7c

    .line 308
    invoke-virtual {v0, v1}, Lorg/apache/a/a/b;->a(C)Lorg/apache/a/a/b;

    move-result-object v0

    const/16 v1, 0x5c

    .line 309
    invoke-virtual {v0, v1}, Lorg/apache/a/a/b;->b(C)Lorg/apache/a/a/b;

    move-result-object v0

    sget-object v1, Lorg/apache/a/a/d;->a:Ljava/lang/Character;

    .line 310
    invoke-virtual {v0, v1}, Lorg/apache/a/a/b;->b(Ljava/lang/Character;)Lorg/apache/a/a/b;

    move-result-object v0

    const/16 v1, 0xa

    .line 311
    invoke-virtual {v0, v1}, Lorg/apache/a/a/b;->c(C)Lorg/apache/a/a/b;

    move-result-object v0

    sput-object v0, Lorg/apache/a/a/b;->c:Lorg/apache/a/a/b;

    .line 338
    sget-object v0, Lorg/apache/a/a/b;->a:Lorg/apache/a/a/b;

    const/16 v1, 0x2c

    .line 339
    invoke-virtual {v0, v1}, Lorg/apache/a/a/b;->a(C)Lorg/apache/a/a/b;

    move-result-object v0

    sget-object v1, Lorg/apache/a/a/d;->a:Ljava/lang/Character;

    .line 340
    invoke-virtual {v0, v1}, Lorg/apache/a/a/b;->b(Ljava/lang/Character;)Lorg/apache/a/a/b;

    move-result-object v0

    const/16 v1, 0xa

    .line 341
    invoke-virtual {v0, v1}, Lorg/apache/a/a/b;->c(C)Lorg/apache/a/a/b;

    move-result-object v0

    sput-object v0, Lorg/apache/a/a/b;->d:Lorg/apache/a/a/b;

    .line 370
    sget-object v0, Lorg/apache/a/a/b;->a:Lorg/apache/a/a/b;

    const/16 v1, 0x9

    .line 371
    invoke-virtual {v0, v1}, Lorg/apache/a/a/b;->a(C)Lorg/apache/a/a/b;

    move-result-object v0

    const/16 v1, 0x5c

    .line 372
    invoke-virtual {v0, v1}, Lorg/apache/a/a/b;->b(C)Lorg/apache/a/a/b;

    move-result-object v0

    const/4 v1, 0x0

    .line 373
    invoke-virtual {v0, v1}, Lorg/apache/a/a/b;->b(Z)Lorg/apache/a/a/b;

    move-result-object v0

    const/4 v1, 0x0

    .line 374
    invoke-virtual {v0, v1}, Lorg/apache/a/a/b;->b(Ljava/lang/Character;)Lorg/apache/a/a/b;

    move-result-object v0

    const/16 v1, 0xa

    .line 375
    invoke-virtual {v0, v1}, Lorg/apache/a/a/b;->c(C)Lorg/apache/a/a/b;

    move-result-object v0

    const-string v1, "\\N"

    .line 376
    invoke-virtual {v0, v1}, Lorg/apache/a/a/b;->a(Ljava/lang/String;)Lorg/apache/a/a/b;

    move-result-object v0

    sget-object v1, Lorg/apache/a/a/e;->b:Lorg/apache/a/a/e;

    .line 377
    invoke-virtual {v0, v1}, Lorg/apache/a/a/b;->a(Lorg/apache/a/a/e;)Lorg/apache/a/a/b;

    move-result-object v0

    sput-object v0, Lorg/apache/a/a/b;->e:Lorg/apache/a/a/b;

    .line 407
    sget-object v0, Lorg/apache/a/a/b;->a:Lorg/apache/a/a/b;

    const/16 v1, 0x2c

    .line 408
    invoke-virtual {v0, v1}, Lorg/apache/a/a/b;->a(C)Lorg/apache/a/a/b;

    move-result-object v0

    sget-object v1, Lorg/apache/a/a/d;->a:Ljava/lang/Character;

    .line 409
    invoke-virtual {v0, v1}, Lorg/apache/a/a/b;->a(Ljava/lang/Character;)Lorg/apache/a/a/b;

    move-result-object v0

    const/4 v1, 0x0

    .line 410
    invoke-virtual {v0, v1}, Lorg/apache/a/a/b;->b(Z)Lorg/apache/a/a/b;

    move-result-object v0

    sget-object v1, Lorg/apache/a/a/d;->a:Ljava/lang/Character;

    .line 411
    invoke-virtual {v0, v1}, Lorg/apache/a/a/b;->b(Ljava/lang/Character;)Lorg/apache/a/a/b;

    move-result-object v0

    const/16 v1, 0xa

    .line 412
    invoke-virtual {v0, v1}, Lorg/apache/a/a/b;->c(C)Lorg/apache/a/a/b;

    move-result-object v0

    const-string v1, ""

    .line 413
    invoke-virtual {v0, v1}, Lorg/apache/a/a/b;->a(Ljava/lang/String;)Lorg/apache/a/a/b;

    move-result-object v0

    sget-object v1, Lorg/apache/a/a/e;->b:Lorg/apache/a/a/e;

    .line 414
    invoke-virtual {v0, v1}, Lorg/apache/a/a/b;->a(Lorg/apache/a/a/e;)Lorg/apache/a/a/b;

    move-result-object v0

    sput-object v0, Lorg/apache/a/a/b;->f:Lorg/apache/a/a/b;

    .line 444
    sget-object v0, Lorg/apache/a/a/b;->a:Lorg/apache/a/a/b;

    const/16 v1, 0x9

    .line 445
    invoke-virtual {v0, v1}, Lorg/apache/a/a/b;->a(C)Lorg/apache/a/a/b;

    move-result-object v0

    sget-object v1, Lorg/apache/a/a/d;->a:Ljava/lang/Character;

    .line 446
    invoke-virtual {v0, v1}, Lorg/apache/a/a/b;->a(Ljava/lang/Character;)Lorg/apache/a/a/b;

    move-result-object v0

    const/4 v1, 0x0

    .line 447
    invoke-virtual {v0, v1}, Lorg/apache/a/a/b;->b(Z)Lorg/apache/a/a/b;

    move-result-object v0

    sget-object v1, Lorg/apache/a/a/d;->a:Ljava/lang/Character;

    .line 448
    invoke-virtual {v0, v1}, Lorg/apache/a/a/b;->b(Ljava/lang/Character;)Lorg/apache/a/a/b;

    move-result-object v0

    const/16 v1, 0xa

    .line 449
    invoke-virtual {v0, v1}, Lorg/apache/a/a/b;->c(C)Lorg/apache/a/a/b;

    move-result-object v0

    const-string v1, "\\N"

    .line 450
    invoke-virtual {v0, v1}, Lorg/apache/a/a/b;->a(Ljava/lang/String;)Lorg/apache/a/a/b;

    move-result-object v0

    sget-object v1, Lorg/apache/a/a/e;->b:Lorg/apache/a/a/e;

    .line 451
    invoke-virtual {v0, v1}, Lorg/apache/a/a/b;->a(Lorg/apache/a/a/e;)Lorg/apache/a/a/b;

    move-result-object v0

    sput-object v0, Lorg/apache/a/a/b;->g:Lorg/apache/a/a/b;

    .line 469
    sget-object v0, Lorg/apache/a/a/b;->a:Lorg/apache/a/a/b;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lorg/apache/a/a/b;->b(Z)Lorg/apache/a/a/b;

    move-result-object v0

    sput-object v0, Lorg/apache/a/a/b;->h:Lorg/apache/a/a/b;

    .line 489
    sget-object v0, Lorg/apache/a/a/b;->a:Lorg/apache/a/a/b;

    const/16 v1, 0x9

    .line 490
    invoke-virtual {v0, v1}, Lorg/apache/a/a/b;->a(C)Lorg/apache/a/a/b;

    move-result-object v0

    .line 491
    invoke-virtual {v0}, Lorg/apache/a/a/b;->s()Lorg/apache/a/a/b;

    move-result-object v0

    sput-object v0, Lorg/apache/a/a/b;->i:Lorg/apache/a/a/b;

    .line 489
    return-void
.end method

.method private constructor <init>(CLjava/lang/Character;Lorg/apache/a/a/e;Ljava/lang/Character;Ljava/lang/Character;ZZLjava/lang/String;Ljava/lang/String;[Ljava/lang/Object;[Ljava/lang/String;ZZZZZ)V
    .locals 2

    .prologue
    .line 630
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 631
    iput-char p1, p0, Lorg/apache/a/a/b;->l:C

    .line 632
    iput-object p2, p0, Lorg/apache/a/a/b;->t:Ljava/lang/Character;

    .line 633
    iput-object p3, p0, Lorg/apache/a/a/b;->u:Lorg/apache/a/a/e;

    .line 634
    iput-object p4, p0, Lorg/apache/a/a/b;->k:Ljava/lang/Character;

    .line 635
    iput-object p5, p0, Lorg/apache/a/a/b;->m:Ljava/lang/Character;

    .line 636
    iput-boolean p6, p0, Lorg/apache/a/a/b;->r:Z

    .line 637
    iput-boolean p13, p0, Lorg/apache/a/a/b;->j:Z

    .line 638
    iput-boolean p7, p0, Lorg/apache/a/a/b;->p:Z

    .line 639
    iput-object p8, p0, Lorg/apache/a/a/b;->v:Ljava/lang/String;

    .line 640
    iput-object p9, p0, Lorg/apache/a/a/b;->s:Ljava/lang/String;

    .line 641
    invoke-direct {p0, p10}, Lorg/apache/a/a/b;->a([Ljava/lang/Object;)[Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lorg/apache/a/a/b;->o:[Ljava/lang/String;

    .line 642
    if-nez p11, :cond_0

    const/4 v1, 0x0

    :goto_0
    iput-object v1, p0, Lorg/apache/a/a/b;->n:[Ljava/lang/String;

    .line 643
    iput-boolean p12, p0, Lorg/apache/a/a/b;->w:Z

    .line 644
    move/from16 v0, p14

    iput-boolean v0, p0, Lorg/apache/a/a/b;->q:Z

    .line 645
    move/from16 v0, p16

    iput-boolean v0, p0, Lorg/apache/a/a/b;->x:Z

    .line 646
    move/from16 v0, p15

    iput-boolean v0, p0, Lorg/apache/a/a/b;->y:Z

    .line 647
    invoke-direct {p0}, Lorg/apache/a/a/b;->t()V

    .line 648
    return-void

    .line 642
    :cond_0
    invoke-virtual {p11}, [Ljava/lang/String;->clone()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Ljava/lang/String;

    goto :goto_0
.end method

.method private a(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;
    .locals 5

    .prologue
    const/16 v4, 0x20

    .line 1345
    instance-of v0, p1, Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 1346
    check-cast p1, Ljava/lang/String;

    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object p1

    .line 1358
    :cond_0
    :goto_0
    return-object p1

    .line 1348
    :cond_1
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v1

    .line 1350
    const/4 v0, 0x0

    move v2, v0

    .line 1352
    :goto_1
    if-ge v2, v1, :cond_4

    invoke-interface {p1, v2}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v0

    if-gt v0, v4, :cond_4

    .line 1353
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    .line 1355
    :goto_2
    if-ge v2, v0, :cond_2

    add-int/lit8 v3, v0, -0x1

    invoke-interface {p1, v3}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v3

    if-gt v3, v4, :cond_2

    .line 1356
    add-int/lit8 v0, v0, -0x1

    goto :goto_2

    .line 1358
    :cond_2
    if-gtz v2, :cond_3

    if-ge v0, v1, :cond_0

    :cond_3
    invoke-interface {p1, v2, v0}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object p1

    goto :goto_0

    :cond_4
    move v0, v1

    goto :goto_2
.end method

.method private a(Ljava/lang/CharSequence;IILjava/lang/Appendable;)V
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/16 v6, 0xd

    const/16 v5, 0xa

    .line 1098
    .line 1100
    add-int v2, p2, p3

    .line 1102
    invoke-virtual {p0}, Lorg/apache/a/a/b;->b()C

    move-result v3

    .line 1103
    invoke-virtual {p0}, Lorg/apache/a/a/b;->c()Ljava/lang/Character;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Character;->charValue()C

    move-result v4

    move v1, p2

    .line 1105
    :goto_0
    if-ge p2, v2, :cond_4

    .line 1106
    invoke-interface {p1, p2}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v0

    .line 1107
    if-eq v0, v6, :cond_0

    if-eq v0, v5, :cond_0

    if-eq v0, v3, :cond_0

    if-ne v0, v4, :cond_6

    .line 1109
    :cond_0
    if-le p2, v1, :cond_1

    .line 1110
    invoke-interface {p4, p1, v1, p2}, Ljava/lang/Appendable;->append(Ljava/lang/CharSequence;II)Ljava/lang/Appendable;

    .line 1112
    :cond_1
    if-ne v0, v5, :cond_3

    .line 1113
    const/16 v0, 0x6e

    .line 1118
    :cond_2
    :goto_1
    invoke-interface {p4, v4}, Ljava/lang/Appendable;->append(C)Ljava/lang/Appendable;

    .line 1119
    invoke-interface {p4, v0}, Ljava/lang/Appendable;->append(C)Ljava/lang/Appendable;

    .line 1121
    add-int/lit8 v0, p2, 0x1

    .line 1124
    :goto_2
    add-int/lit8 p2, p2, 0x1

    move v1, v0

    .line 1125
    goto :goto_0

    .line 1114
    :cond_3
    if-ne v0, v6, :cond_2

    .line 1115
    const/16 v0, 0x72

    goto :goto_1

    .line 1128
    :cond_4
    if-le p2, v1, :cond_5

    .line 1129
    invoke-interface {p4, p1, v1, p2}, Ljava/lang/Appendable;->append(Ljava/lang/CharSequence;II)Ljava/lang/Appendable;

    .line 1131
    :cond_5
    return-void

    :cond_6
    move v0, v1

    goto :goto_2
.end method

.method private a(Ljava/lang/Object;Ljava/lang/CharSequence;IILjava/lang/Appendable;Z)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1078
    if-nez p6, :cond_0

    .line 1079
    invoke-virtual {p0}, Lorg/apache/a/a/b;->b()C

    move-result v0

    invoke-interface {p5, v0}, Ljava/lang/Appendable;->append(C)Ljava/lang/Appendable;

    .line 1081
    :cond_0
    if-nez p1, :cond_1

    .line 1082
    invoke-interface {p5, p2}, Ljava/lang/Appendable;->append(Ljava/lang/CharSequence;)Ljava/lang/Appendable;

    .line 1091
    :goto_0
    return-void

    .line 1083
    :cond_1
    invoke-virtual {p0}, Lorg/apache/a/a/b;->q()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1085
    invoke-direct/range {p0 .. p6}, Lorg/apache/a/a/b;->b(Ljava/lang/Object;Ljava/lang/CharSequence;IILjava/lang/Appendable;Z)V

    goto :goto_0

    .line 1086
    :cond_2
    invoke-virtual {p0}, Lorg/apache/a/a/b;->o()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1087
    invoke-direct {p0, p2, p3, p4, p5}, Lorg/apache/a/a/b;->a(Ljava/lang/CharSequence;IILjava/lang/Appendable;)V

    goto :goto_0

    .line 1089
    :cond_3
    add-int v0, p3, p4

    invoke-interface {p5, p2, p3, v0}, Ljava/lang/Appendable;->append(Ljava/lang/CharSequence;II)Ljava/lang/Appendable;

    goto :goto_0
.end method

.method private a([Ljava/lang/Object;)[Ljava/lang/String;
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 1333
    if-nez p1, :cond_0

    .line 1341
    :goto_0
    return-object v2

    .line 1336
    :cond_0
    array-length v0, p1

    new-array v3, v0, [Ljava/lang/String;

    .line 1337
    const/4 v0, 0x0

    :goto_1
    array-length v1, p1

    if-ge v0, v1, :cond_2

    .line 1338
    aget-object v1, p1, v0

    .line 1339
    if-nez v1, :cond_1

    move-object v1, v2

    :goto_2
    aput-object v1, v3, v0

    .line 1337
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 1339
    :cond_1
    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_2

    :cond_2
    move-object v2, v3

    .line 1341
    goto :goto_0
.end method

.method private b(Ljava/lang/Object;Ljava/lang/CharSequence;IILjava/lang/Appendable;Z)V
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1139
    const/4 v2, 0x0

    .line 1142
    add-int v3, p3, p4

    .line 1144
    invoke-virtual {p0}, Lorg/apache/a/a/b;->b()C

    move-result v4

    .line 1145
    invoke-virtual {p0}, Lorg/apache/a/a/b;->i()Ljava/lang/Character;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Character;->charValue()C

    move-result v5

    .line 1147
    invoke-virtual {p0}, Lorg/apache/a/a/b;->j()Lorg/apache/a/a/e;

    move-result-object v0

    .line 1148
    if-nez v0, :cond_0

    .line 1149
    sget-object v0, Lorg/apache/a/a/e;->c:Lorg/apache/a/a/e;

    .line 1151
    :cond_0
    sget-object v1, Lorg/apache/a/a/b$1;->a:[I

    invoke-virtual {v0}, Lorg/apache/a/a/e;->ordinal()I

    move-result v6

    aget v1, v1, v6

    packed-switch v1, :pswitch_data_0

    .line 1212
    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unexpected Quote value: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 1154
    :pswitch_0
    const/4 v0, 0x1

    move v1, p3

    .line 1215
    :cond_1
    :goto_0
    if-nez v0, :cond_b

    .line 1217
    invoke-interface {p5, p2, p3, v3}, Ljava/lang/Appendable;->append(Ljava/lang/CharSequence;II)Ljava/lang/Appendable;

    .line 1243
    :goto_1
    return-void

    .line 1157
    :pswitch_1
    instance-of v0, p1, Ljava/lang/Number;

    if-nez v0, :cond_2

    const/4 v0, 0x1

    :goto_2
    move v1, p3

    .line 1158
    goto :goto_0

    .line 1157
    :cond_2
    const/4 v0, 0x0

    goto :goto_2

    .line 1161
    :pswitch_2
    invoke-direct {p0, p2, p3, p4, p5}, Lorg/apache/a/a/b;->a(Ljava/lang/CharSequence;IILjava/lang/Appendable;)V

    goto :goto_1

    .line 1164
    :pswitch_3
    if-gtz p4, :cond_4

    .line 1169
    if-eqz p6, :cond_10

    .line 1170
    const/4 v0, 0x1

    move v1, p3

    .line 1205
    :cond_3
    :goto_3
    if-nez v0, :cond_1

    .line 1207
    invoke-interface {p5, p2, p3, v3}, Ljava/lang/Appendable;->append(Ljava/lang/CharSequence;II)Ljava/lang/Appendable;

    goto :goto_1

    .line 1173
    :cond_4
    invoke-interface {p2, p3}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v0

    .line 1176
    if-eqz p6, :cond_8

    const/16 v1, 0x20

    if-lt v0, v1, :cond_7

    const/16 v1, 0x21

    if-le v0, v1, :cond_5

    const/16 v1, 0x23

    if-lt v0, v1, :cond_7

    :cond_5
    const/16 v1, 0x2b

    if-le v0, v1, :cond_6

    const/16 v1, 0x2d

    if-lt v0, v1, :cond_7

    :cond_6
    const/16 v1, 0x7e

    if-le v0, v1, :cond_8

    .line 1177
    :cond_7
    const/4 v0, 0x1

    move v1, p3

    goto :goto_3

    .line 1178
    :cond_8
    const/16 v1, 0x23

    if-gt v0, v1, :cond_f

    .line 1182
    const/4 v0, 0x1

    move v1, p3

    goto :goto_3

    .line 1190
    :cond_9
    add-int/lit8 v1, v1, 0x1

    .line 1184
    :goto_4
    if-ge v1, v3, :cond_e

    .line 1185
    invoke-interface {p2, v1}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v0

    .line 1186
    const/16 v6, 0xa

    if-eq v0, v6, :cond_a

    const/16 v6, 0xd

    if-eq v0, v6, :cond_a

    if-eq v0, v5, :cond_a

    if-ne v0, v4, :cond_9

    .line 1187
    :cond_a
    const/4 v0, 0x1

    .line 1193
    :goto_5
    if-nez v0, :cond_3

    .line 1194
    add-int/lit8 v1, v3, -0x1

    .line 1195
    invoke-interface {p2, v1}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v2

    .line 1198
    const/16 v4, 0x20

    if-gt v2, v4, :cond_3

    .line 1199
    const/4 v0, 0x1

    goto :goto_3

    .line 1222
    :cond_b
    invoke-interface {p5, v5}, Ljava/lang/Appendable;->append(C)Ljava/lang/Appendable;

    move v0, p3

    .line 1226
    :goto_6
    if-ge v1, v3, :cond_d

    .line 1227
    invoke-interface {p2, v1}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v2

    .line 1228
    if-ne v2, v5, :cond_c

    .line 1232
    add-int/lit8 v2, v1, 0x1

    invoke-interface {p5, p2, v0, v2}, Ljava/lang/Appendable;->append(Ljava/lang/CharSequence;II)Ljava/lang/Appendable;

    move v0, v1

    .line 1237
    :cond_c
    add-int/lit8 v1, v1, 0x1

    .line 1238
    goto :goto_6

    .line 1241
    :cond_d
    invoke-interface {p5, p2, v0, v1}, Ljava/lang/Appendable;->append(Ljava/lang/CharSequence;II)Ljava/lang/Appendable;

    .line 1242
    invoke-interface {p5, v5}, Ljava/lang/Appendable;->append(C)Ljava/lang/Appendable;

    goto :goto_1

    :cond_e
    move v0, v2

    goto :goto_5

    :cond_f
    move v1, p3

    goto :goto_4

    :cond_10
    move v1, p3

    move v0, v2

    goto :goto_3

    .line 1151
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private static c(Ljava/lang/Character;)Z
    .locals 1

    .prologue
    .line 515
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/Character;->charValue()C

    move-result v0

    invoke-static {v0}, Lorg/apache/a/a/b;->d(C)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static d(C)Z
    .locals 1

    .prologue
    .line 503
    const/16 v0, 0xa

    if-eq p0, v0, :cond_0

    const/16 v0, 0xd

    if-ne p0, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private t()V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    .prologue
    .line 1367
    iget-char v0, p0, Lorg/apache/a/a/b;->l:C

    invoke-static {v0}, Lorg/apache/a/a/b;->d(C)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1368
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "The delimiter cannot be a line break"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1371
    :cond_0
    iget-object v0, p0, Lorg/apache/a/a/b;->t:Ljava/lang/Character;

    if-eqz v0, :cond_1

    iget-char v0, p0, Lorg/apache/a/a/b;->l:C

    iget-object v1, p0, Lorg/apache/a/a/b;->t:Ljava/lang/Character;

    invoke-virtual {v1}, Ljava/lang/Character;->charValue()C

    move-result v1

    if-ne v0, v1, :cond_1

    .line 1372
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "The quoteChar character and the delimiter cannot be the same (\'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lorg/apache/a/a/b;->t:Ljava/lang/Character;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\')"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1376
    :cond_1
    iget-object v0, p0, Lorg/apache/a/a/b;->m:Ljava/lang/Character;

    if-eqz v0, :cond_2

    iget-char v0, p0, Lorg/apache/a/a/b;->l:C

    iget-object v1, p0, Lorg/apache/a/a/b;->m:Ljava/lang/Character;

    invoke-virtual {v1}, Ljava/lang/Character;->charValue()C

    move-result v1

    if-ne v0, v1, :cond_2

    .line 1377
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "The escape character and the delimiter cannot be the same (\'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lorg/apache/a/a/b;->m:Ljava/lang/Character;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\')"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1381
    :cond_2
    iget-object v0, p0, Lorg/apache/a/a/b;->k:Ljava/lang/Character;

    if-eqz v0, :cond_3

    iget-char v0, p0, Lorg/apache/a/a/b;->l:C

    iget-object v1, p0, Lorg/apache/a/a/b;->k:Ljava/lang/Character;

    invoke-virtual {v1}, Ljava/lang/Character;->charValue()C

    move-result v1

    if-ne v0, v1, :cond_3

    .line 1382
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "The comment start character and the delimiter cannot be the same (\'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lorg/apache/a/a/b;->k:Ljava/lang/Character;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\')"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1386
    :cond_3
    iget-object v0, p0, Lorg/apache/a/a/b;->t:Ljava/lang/Character;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lorg/apache/a/a/b;->t:Ljava/lang/Character;

    iget-object v1, p0, Lorg/apache/a/a/b;->k:Ljava/lang/Character;

    invoke-virtual {v0, v1}, Ljava/lang/Character;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1387
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "The comment start character and the quoteChar cannot be the same (\'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lorg/apache/a/a/b;->k:Ljava/lang/Character;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\')"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1391
    :cond_4
    iget-object v0, p0, Lorg/apache/a/a/b;->m:Ljava/lang/Character;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lorg/apache/a/a/b;->m:Ljava/lang/Character;

    iget-object v1, p0, Lorg/apache/a/a/b;->k:Ljava/lang/Character;

    invoke-virtual {v0, v1}, Ljava/lang/Character;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 1392
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "The comment start and the escape character cannot be the same (\'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lorg/apache/a/a/b;->k:Ljava/lang/Character;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\')"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1396
    :cond_5
    iget-object v0, p0, Lorg/apache/a/a/b;->m:Ljava/lang/Character;

    if-nez v0, :cond_6

    iget-object v0, p0, Lorg/apache/a/a/b;->u:Lorg/apache/a/a/e;

    sget-object v1, Lorg/apache/a/a/e;->e:Lorg/apache/a/a/e;

    if-ne v0, v1, :cond_6

    .line 1397
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "No quotes mode set but no escape character is set"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1401
    :cond_6
    iget-object v0, p0, Lorg/apache/a/a/b;->n:[Ljava/lang/String;

    if-eqz v0, :cond_8

    .line 1402
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    .line 1403
    iget-object v2, p0, Lorg/apache/a/a/b;->n:[Ljava/lang/String;

    array-length v3, v2

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_8

    aget-object v4, v2, v0

    .line 1404
    invoke-interface {v1, v4}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_7

    .line 1405
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "The header contains a duplicate entry: \'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\' in "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lorg/apache/a/a/b;->n:[Ljava/lang/String;

    .line 1406
    invoke-static {v2}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1403
    :cond_7
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1410
    :cond_8
    return-void
.end method


# virtual methods
.method public a()Ljava/lang/Character;
    .locals 1

    .prologue
    .line 753
    iget-object v0, p0, Lorg/apache/a/a/b;->k:Ljava/lang/Character;

    return-object v0
.end method

.method public a(C)Lorg/apache/a/a/b;
    .locals 18

    .prologue
    .line 1482
    invoke-static/range {p1 .. p1}, Lorg/apache/a/a/b;->d(C)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1483
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "The delimiter cannot be a line break"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 1485
    :cond_0
    new-instance v1, Lorg/apache/a/a/b;

    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/apache/a/a/b;->t:Ljava/lang/Character;

    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/a/a/b;->u:Lorg/apache/a/a/e;

    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/apache/a/a/b;->k:Ljava/lang/Character;

    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/a/a/b;->m:Ljava/lang/Character;

    move-object/from16 v0, p0

    iget-boolean v7, v0, Lorg/apache/a/a/b;->r:Z

    move-object/from16 v0, p0

    iget-boolean v8, v0, Lorg/apache/a/a/b;->p:Z

    move-object/from16 v0, p0

    iget-object v9, v0, Lorg/apache/a/a/b;->v:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v10, v0, Lorg/apache/a/a/b;->s:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v11, v0, Lorg/apache/a/a/b;->o:[Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v12, v0, Lorg/apache/a/a/b;->n:[Ljava/lang/String;

    move-object/from16 v0, p0

    iget-boolean v13, v0, Lorg/apache/a/a/b;->w:Z

    move-object/from16 v0, p0

    iget-boolean v14, v0, Lorg/apache/a/a/b;->j:Z

    move-object/from16 v0, p0

    iget-boolean v15, v0, Lorg/apache/a/a/b;->q:Z

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lorg/apache/a/a/b;->y:Z

    move/from16 v16, v0

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lorg/apache/a/a/b;->x:Z

    move/from16 v17, v0

    move/from16 v2, p1

    invoke-direct/range {v1 .. v17}, Lorg/apache/a/a/b;-><init>(CLjava/lang/Character;Lorg/apache/a/a/e;Ljava/lang/Character;Ljava/lang/Character;ZZLjava/lang/String;Ljava/lang/String;[Ljava/lang/Object;[Ljava/lang/String;ZZZZZ)V

    return-object v1
.end method

.method public a(Ljava/lang/Character;)Lorg/apache/a/a/b;
    .locals 18

    .prologue
    .line 1513
    invoke-static/range {p1 .. p1}, Lorg/apache/a/a/b;->c(Ljava/lang/Character;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1514
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "The escape character cannot be a line break"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 1516
    :cond_0
    new-instance v1, Lorg/apache/a/a/b;

    move-object/from16 v0, p0

    iget-char v2, v0, Lorg/apache/a/a/b;->l:C

    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/apache/a/a/b;->t:Ljava/lang/Character;

    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/a/a/b;->u:Lorg/apache/a/a/e;

    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/apache/a/a/b;->k:Ljava/lang/Character;

    move-object/from16 v0, p0

    iget-boolean v7, v0, Lorg/apache/a/a/b;->r:Z

    move-object/from16 v0, p0

    iget-boolean v8, v0, Lorg/apache/a/a/b;->p:Z

    move-object/from16 v0, p0

    iget-object v9, v0, Lorg/apache/a/a/b;->v:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v10, v0, Lorg/apache/a/a/b;->s:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v11, v0, Lorg/apache/a/a/b;->o:[Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v12, v0, Lorg/apache/a/a/b;->n:[Ljava/lang/String;

    move-object/from16 v0, p0

    iget-boolean v13, v0, Lorg/apache/a/a/b;->w:Z

    move-object/from16 v0, p0

    iget-boolean v14, v0, Lorg/apache/a/a/b;->j:Z

    move-object/from16 v0, p0

    iget-boolean v15, v0, Lorg/apache/a/a/b;->q:Z

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lorg/apache/a/a/b;->y:Z

    move/from16 v16, v0

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lorg/apache/a/a/b;->x:Z

    move/from16 v17, v0

    move-object/from16 v6, p1

    invoke-direct/range {v1 .. v17}, Lorg/apache/a/a/b;-><init>(CLjava/lang/Character;Lorg/apache/a/a/e;Ljava/lang/Character;Ljava/lang/Character;ZZLjava/lang/String;Ljava/lang/String;[Ljava/lang/Object;[Ljava/lang/String;ZZZZZ)V

    return-object v1
.end method

.method public a(Ljava/lang/String;)Lorg/apache/a/a/b;
    .locals 18

    .prologue
    .line 1787
    new-instance v1, Lorg/apache/a/a/b;

    move-object/from16 v0, p0

    iget-char v2, v0, Lorg/apache/a/a/b;->l:C

    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/apache/a/a/b;->t:Ljava/lang/Character;

    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/a/a/b;->u:Lorg/apache/a/a/e;

    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/apache/a/a/b;->k:Ljava/lang/Character;

    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/a/a/b;->m:Ljava/lang/Character;

    move-object/from16 v0, p0

    iget-boolean v7, v0, Lorg/apache/a/a/b;->r:Z

    move-object/from16 v0, p0

    iget-boolean v8, v0, Lorg/apache/a/a/b;->p:Z

    move-object/from16 v0, p0

    iget-object v9, v0, Lorg/apache/a/a/b;->v:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v11, v0, Lorg/apache/a/a/b;->o:[Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v12, v0, Lorg/apache/a/a/b;->n:[Ljava/lang/String;

    move-object/from16 v0, p0

    iget-boolean v13, v0, Lorg/apache/a/a/b;->w:Z

    move-object/from16 v0, p0

    iget-boolean v14, v0, Lorg/apache/a/a/b;->j:Z

    move-object/from16 v0, p0

    iget-boolean v15, v0, Lorg/apache/a/a/b;->q:Z

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lorg/apache/a/a/b;->y:Z

    move/from16 v16, v0

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lorg/apache/a/a/b;->x:Z

    move/from16 v17, v0

    move-object/from16 v10, p1

    invoke-direct/range {v1 .. v17}, Lorg/apache/a/a/b;-><init>(CLjava/lang/Character;Lorg/apache/a/a/e;Ljava/lang/Character;Ljava/lang/Character;ZZLjava/lang/String;Ljava/lang/String;[Ljava/lang/Object;[Ljava/lang/String;ZZZZZ)V

    return-object v1
.end method

.method public a(Lorg/apache/a/a/e;)Lorg/apache/a/a/b;
    .locals 18

    .prologue
    .line 1832
    new-instance v1, Lorg/apache/a/a/b;

    move-object/from16 v0, p0

    iget-char v2, v0, Lorg/apache/a/a/b;->l:C

    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/apache/a/a/b;->t:Ljava/lang/Character;

    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/apache/a/a/b;->k:Ljava/lang/Character;

    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/a/a/b;->m:Ljava/lang/Character;

    move-object/from16 v0, p0

    iget-boolean v7, v0, Lorg/apache/a/a/b;->r:Z

    move-object/from16 v0, p0

    iget-boolean v8, v0, Lorg/apache/a/a/b;->p:Z

    move-object/from16 v0, p0

    iget-object v9, v0, Lorg/apache/a/a/b;->v:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v10, v0, Lorg/apache/a/a/b;->s:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v11, v0, Lorg/apache/a/a/b;->o:[Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v12, v0, Lorg/apache/a/a/b;->n:[Ljava/lang/String;

    move-object/from16 v0, p0

    iget-boolean v13, v0, Lorg/apache/a/a/b;->w:Z

    move-object/from16 v0, p0

    iget-boolean v14, v0, Lorg/apache/a/a/b;->j:Z

    move-object/from16 v0, p0

    iget-boolean v15, v0, Lorg/apache/a/a/b;->q:Z

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lorg/apache/a/a/b;->y:Z

    move/from16 v16, v0

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lorg/apache/a/a/b;->x:Z

    move/from16 v17, v0

    move-object/from16 v4, p1

    invoke-direct/range {v1 .. v17}, Lorg/apache/a/a/b;-><init>(CLjava/lang/Character;Lorg/apache/a/a/e;Ljava/lang/Character;Ljava/lang/Character;ZZLjava/lang/String;Ljava/lang/String;[Ljava/lang/Object;[Ljava/lang/String;ZZZZZ)V

    return-object v1
.end method

.method public a(Z)Lorg/apache/a/a/b;
    .locals 18

    .prologue
    .line 1432
    new-instance v1, Lorg/apache/a/a/b;

    move-object/from16 v0, p0

    iget-char v2, v0, Lorg/apache/a/a/b;->l:C

    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/apache/a/a/b;->t:Ljava/lang/Character;

    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/a/a/b;->u:Lorg/apache/a/a/e;

    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/apache/a/a/b;->k:Ljava/lang/Character;

    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/a/a/b;->m:Ljava/lang/Character;

    move-object/from16 v0, p0

    iget-boolean v7, v0, Lorg/apache/a/a/b;->r:Z

    move-object/from16 v0, p0

    iget-boolean v8, v0, Lorg/apache/a/a/b;->p:Z

    move-object/from16 v0, p0

    iget-object v9, v0, Lorg/apache/a/a/b;->v:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v10, v0, Lorg/apache/a/a/b;->s:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v11, v0, Lorg/apache/a/a/b;->o:[Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v12, v0, Lorg/apache/a/a/b;->n:[Ljava/lang/String;

    move-object/from16 v0, p0

    iget-boolean v13, v0, Lorg/apache/a/a/b;->w:Z

    move-object/from16 v0, p0

    iget-boolean v15, v0, Lorg/apache/a/a/b;->q:Z

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lorg/apache/a/a/b;->y:Z

    move/from16 v16, v0

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lorg/apache/a/a/b;->x:Z

    move/from16 v17, v0

    move/from16 v14, p1

    invoke-direct/range {v1 .. v17}, Lorg/apache/a/a/b;-><init>(CLjava/lang/Character;Lorg/apache/a/a/e;Ljava/lang/Character;Ljava/lang/Character;ZZLjava/lang/String;Ljava/lang/String;[Ljava/lang/Object;[Ljava/lang/String;ZZZZZ)V

    return-object v1
.end method

.method public varargs a([Ljava/lang/String;)Lorg/apache/a/a/b;
    .locals 18

    .prologue
    .line 1671
    new-instance v1, Lorg/apache/a/a/b;

    move-object/from16 v0, p0

    iget-char v2, v0, Lorg/apache/a/a/b;->l:C

    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/apache/a/a/b;->t:Ljava/lang/Character;

    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/a/a/b;->u:Lorg/apache/a/a/e;

    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/apache/a/a/b;->k:Ljava/lang/Character;

    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/a/a/b;->m:Ljava/lang/Character;

    move-object/from16 v0, p0

    iget-boolean v7, v0, Lorg/apache/a/a/b;->r:Z

    move-object/from16 v0, p0

    iget-boolean v8, v0, Lorg/apache/a/a/b;->p:Z

    move-object/from16 v0, p0

    iget-object v9, v0, Lorg/apache/a/a/b;->v:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v10, v0, Lorg/apache/a/a/b;->s:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v11, v0, Lorg/apache/a/a/b;->o:[Ljava/lang/String;

    move-object/from16 v0, p0

    iget-boolean v13, v0, Lorg/apache/a/a/b;->w:Z

    move-object/from16 v0, p0

    iget-boolean v14, v0, Lorg/apache/a/a/b;->j:Z

    move-object/from16 v0, p0

    iget-boolean v15, v0, Lorg/apache/a/a/b;->q:Z

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lorg/apache/a/a/b;->y:Z

    move/from16 v16, v0

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lorg/apache/a/a/b;->x:Z

    move/from16 v17, v0

    move-object/from16 v12, p1

    invoke-direct/range {v1 .. v17}, Lorg/apache/a/a/b;-><init>(CLjava/lang/Character;Lorg/apache/a/a/e;Ljava/lang/Character;Ljava/lang/Character;ZZLjava/lang/String;Ljava/lang/String;[Ljava/lang/Object;[Ljava/lang/String;ZZZZZ)V

    return-object v1
.end method

.method public a(Ljava/lang/Appendable;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1255
    invoke-virtual {p0}, Lorg/apache/a/a/b;->l()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1256
    invoke-virtual {p0}, Lorg/apache/a/a/b;->b()C

    move-result v0

    invoke-interface {p1, v0}, Ljava/lang/Appendable;->append(C)Ljava/lang/Appendable;

    .line 1258
    :cond_0
    iget-object v0, p0, Lorg/apache/a/a/b;->v:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 1259
    iget-object v0, p0, Lorg/apache/a/a/b;->v:Ljava/lang/String;

    invoke-interface {p1, v0}, Ljava/lang/Appendable;->append(Ljava/lang/CharSequence;)Ljava/lang/Appendable;

    .line 1261
    :cond_1
    return-void
.end method

.method public varargs a(Ljava/lang/Appendable;[Ljava/lang/Object;)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 1281
    move v0, v1

    :goto_0
    array-length v2, p2

    if-ge v0, v2, :cond_1

    .line 1282
    aget-object v3, p2, v0

    if-nez v0, :cond_0

    const/4 v2, 0x1

    :goto_1
    invoke-virtual {p0, v3, p1, v2}, Lorg/apache/a/a/b;->a(Ljava/lang/Object;Ljava/lang/Appendable;Z)V

    .line 1281
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    move v2, v1

    .line 1282
    goto :goto_1

    .line 1284
    :cond_1
    invoke-virtual {p0, p1}, Lorg/apache/a/a/b;->a(Ljava/lang/Appendable;)V

    .line 1285
    return-void
.end method

.method public a(Ljava/lang/Object;Ljava/lang/Appendable;Z)V
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1058
    if-nez p1, :cond_2

    .line 1060
    iget-object v0, p0, Lorg/apache/a/a/b;->s:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 1061
    const-string v0, ""

    .line 1072
    :goto_0
    invoke-virtual {p0}, Lorg/apache/a/a/b;->m()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-direct {p0, v0}, Lorg/apache/a/a/b;->a(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v2

    .line 1073
    :goto_1
    const/4 v3, 0x0

    invoke-interface {v2}, Ljava/lang/CharSequence;->length()I

    move-result v4

    move-object v0, p0

    move-object v1, p1

    move-object v5, p2

    move v6, p3

    invoke-direct/range {v0 .. v6}, Lorg/apache/a/a/b;->a(Ljava/lang/Object;Ljava/lang/CharSequence;IILjava/lang/Appendable;Z)V

    .line 1074
    return-void

    .line 1063
    :cond_0
    sget-object v0, Lorg/apache/a/a/e;->a:Lorg/apache/a/a/e;

    iget-object v1, p0, Lorg/apache/a/a/b;->u:Lorg/apache/a/a/e;

    if-ne v0, v1, :cond_1

    .line 1064
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lorg/apache/a/a/b;->t:Ljava/lang/Character;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lorg/apache/a/a/b;->s:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lorg/apache/a/a/b;->t:Ljava/lang/Character;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1066
    :cond_1
    iget-object v0, p0, Lorg/apache/a/a/b;->s:Ljava/lang/String;

    goto :goto_0

    .line 1070
    :cond_2
    instance-of v0, p1, Ljava/lang/CharSequence;

    if-eqz v0, :cond_3

    move-object v0, p1

    check-cast v0, Ljava/lang/CharSequence;

    goto :goto_0

    :cond_3
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_4
    move-object v2, v0

    .line 1072
    goto :goto_1
.end method

.method public b()C
    .locals 1

    .prologue
    .line 762
    iget-char v0, p0, Lorg/apache/a/a/b;->l:C

    return v0
.end method

.method public b(C)Lorg/apache/a/a/b;
    .locals 1

    .prologue
    .line 1500
    invoke-static {p1}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v0

    invoke-virtual {p0, v0}, Lorg/apache/a/a/b;->a(Ljava/lang/Character;)Lorg/apache/a/a/b;

    move-result-object v0

    return-object v0
.end method

.method public b(Ljava/lang/Character;)Lorg/apache/a/a/b;
    .locals 18

    .prologue
    .line 1815
    invoke-static/range {p1 .. p1}, Lorg/apache/a/a/b;->c(Ljava/lang/Character;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1816
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "The quoteChar cannot be a line break"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 1818
    :cond_0
    new-instance v1, Lorg/apache/a/a/b;

    move-object/from16 v0, p0

    iget-char v2, v0, Lorg/apache/a/a/b;->l:C

    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/a/a/b;->u:Lorg/apache/a/a/e;

    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/apache/a/a/b;->k:Ljava/lang/Character;

    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/a/a/b;->m:Ljava/lang/Character;

    move-object/from16 v0, p0

    iget-boolean v7, v0, Lorg/apache/a/a/b;->r:Z

    move-object/from16 v0, p0

    iget-boolean v8, v0, Lorg/apache/a/a/b;->p:Z

    move-object/from16 v0, p0

    iget-object v9, v0, Lorg/apache/a/a/b;->v:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v10, v0, Lorg/apache/a/a/b;->s:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v11, v0, Lorg/apache/a/a/b;->o:[Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v12, v0, Lorg/apache/a/a/b;->n:[Ljava/lang/String;

    move-object/from16 v0, p0

    iget-boolean v13, v0, Lorg/apache/a/a/b;->w:Z

    move-object/from16 v0, p0

    iget-boolean v14, v0, Lorg/apache/a/a/b;->j:Z

    move-object/from16 v0, p0

    iget-boolean v15, v0, Lorg/apache/a/a/b;->q:Z

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lorg/apache/a/a/b;->y:Z

    move/from16 v16, v0

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lorg/apache/a/a/b;->x:Z

    move/from16 v17, v0

    move-object/from16 v3, p1

    invoke-direct/range {v1 .. v17}, Lorg/apache/a/a/b;-><init>(CLjava/lang/Character;Lorg/apache/a/a/e;Ljava/lang/Character;Ljava/lang/Character;ZZLjava/lang/String;Ljava/lang/String;[Ljava/lang/Object;[Ljava/lang/String;ZZZZZ)V

    return-object v1
.end method

.method public b(Ljava/lang/String;)Lorg/apache/a/a/b;
    .locals 18

    .prologue
    .line 1870
    new-instance v1, Lorg/apache/a/a/b;

    move-object/from16 v0, p0

    iget-char v2, v0, Lorg/apache/a/a/b;->l:C

    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/apache/a/a/b;->t:Ljava/lang/Character;

    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/a/a/b;->u:Lorg/apache/a/a/e;

    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/apache/a/a/b;->k:Ljava/lang/Character;

    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/a/a/b;->m:Ljava/lang/Character;

    move-object/from16 v0, p0

    iget-boolean v7, v0, Lorg/apache/a/a/b;->r:Z

    move-object/from16 v0, p0

    iget-boolean v8, v0, Lorg/apache/a/a/b;->p:Z

    move-object/from16 v0, p0

    iget-object v10, v0, Lorg/apache/a/a/b;->s:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v11, v0, Lorg/apache/a/a/b;->o:[Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v12, v0, Lorg/apache/a/a/b;->n:[Ljava/lang/String;

    move-object/from16 v0, p0

    iget-boolean v13, v0, Lorg/apache/a/a/b;->w:Z

    move-object/from16 v0, p0

    iget-boolean v14, v0, Lorg/apache/a/a/b;->j:Z

    move-object/from16 v0, p0

    iget-boolean v15, v0, Lorg/apache/a/a/b;->q:Z

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lorg/apache/a/a/b;->y:Z

    move/from16 v16, v0

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lorg/apache/a/a/b;->x:Z

    move/from16 v17, v0

    move-object/from16 v9, p1

    invoke-direct/range {v1 .. v17}, Lorg/apache/a/a/b;-><init>(CLjava/lang/Character;Lorg/apache/a/a/e;Ljava/lang/Character;Ljava/lang/Character;ZZLjava/lang/String;Ljava/lang/String;[Ljava/lang/Object;[Ljava/lang/String;ZZZZZ)V

    return-object v1
.end method

.method public b(Z)Lorg/apache/a/a/b;
    .locals 18

    .prologue
    .line 1717
    new-instance v1, Lorg/apache/a/a/b;

    move-object/from16 v0, p0

    iget-char v2, v0, Lorg/apache/a/a/b;->l:C

    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/apache/a/a/b;->t:Ljava/lang/Character;

    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/a/a/b;->u:Lorg/apache/a/a/e;

    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/apache/a/a/b;->k:Ljava/lang/Character;

    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/a/a/b;->m:Ljava/lang/Character;

    move-object/from16 v0, p0

    iget-boolean v7, v0, Lorg/apache/a/a/b;->r:Z

    move-object/from16 v0, p0

    iget-object v9, v0, Lorg/apache/a/a/b;->v:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v10, v0, Lorg/apache/a/a/b;->s:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v11, v0, Lorg/apache/a/a/b;->o:[Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v12, v0, Lorg/apache/a/a/b;->n:[Ljava/lang/String;

    move-object/from16 v0, p0

    iget-boolean v13, v0, Lorg/apache/a/a/b;->w:Z

    move-object/from16 v0, p0

    iget-boolean v14, v0, Lorg/apache/a/a/b;->j:Z

    move-object/from16 v0, p0

    iget-boolean v15, v0, Lorg/apache/a/a/b;->q:Z

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lorg/apache/a/a/b;->y:Z

    move/from16 v16, v0

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lorg/apache/a/a/b;->x:Z

    move/from16 v17, v0

    move/from16 v8, p1

    invoke-direct/range {v1 .. v17}, Lorg/apache/a/a/b;-><init>(CLjava/lang/Character;Lorg/apache/a/a/e;Ljava/lang/Character;Ljava/lang/Character;ZZLjava/lang/String;Ljava/lang/String;[Ljava/lang/Object;[Ljava/lang/String;ZZZZZ)V

    return-object v1
.end method

.method public c()Ljava/lang/Character;
    .locals 1

    .prologue
    .line 771
    iget-object v0, p0, Lorg/apache/a/a/b;->m:Ljava/lang/Character;

    return-object v0
.end method

.method public c(C)Lorg/apache/a/a/b;
    .locals 1

    .prologue
    .line 1851
    invoke-static {p1}, Ljava/lang/String;->valueOf(C)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lorg/apache/a/a/b;->b(Ljava/lang/String;)Lorg/apache/a/a/b;

    move-result-object v0

    return-object v0
.end method

.method public c(Z)Lorg/apache/a/a/b;
    .locals 18

    .prologue
    .line 1768
    new-instance v1, Lorg/apache/a/a/b;

    move-object/from16 v0, p0

    iget-char v2, v0, Lorg/apache/a/a/b;->l:C

    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/apache/a/a/b;->t:Ljava/lang/Character;

    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/a/a/b;->u:Lorg/apache/a/a/e;

    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/apache/a/a/b;->k:Ljava/lang/Character;

    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/a/a/b;->m:Ljava/lang/Character;

    move-object/from16 v0, p0

    iget-boolean v8, v0, Lorg/apache/a/a/b;->p:Z

    move-object/from16 v0, p0

    iget-object v9, v0, Lorg/apache/a/a/b;->v:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v10, v0, Lorg/apache/a/a/b;->s:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v11, v0, Lorg/apache/a/a/b;->o:[Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v12, v0, Lorg/apache/a/a/b;->n:[Ljava/lang/String;

    move-object/from16 v0, p0

    iget-boolean v13, v0, Lorg/apache/a/a/b;->w:Z

    move-object/from16 v0, p0

    iget-boolean v14, v0, Lorg/apache/a/a/b;->j:Z

    move-object/from16 v0, p0

    iget-boolean v15, v0, Lorg/apache/a/a/b;->q:Z

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lorg/apache/a/a/b;->y:Z

    move/from16 v16, v0

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lorg/apache/a/a/b;->x:Z

    move/from16 v17, v0

    move/from16 v7, p1

    invoke-direct/range {v1 .. v17}, Lorg/apache/a/a/b;-><init>(CLjava/lang/Character;Lorg/apache/a/a/e;Ljava/lang/Character;Ljava/lang/Character;ZZLjava/lang/String;Ljava/lang/String;[Ljava/lang/Object;[Ljava/lang/String;ZZZZZ)V

    return-object v1
.end method

.method public d()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 780
    iget-object v0, p0, Lorg/apache/a/a/b;->n:[Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/apache/a/a/b;->n:[Ljava/lang/String;

    invoke-virtual {v0}, [Ljava/lang/String;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public e()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 789
    iget-object v0, p0, Lorg/apache/a/a/b;->o:[Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/apache/a/a/b;->o:[Ljava/lang/String;

    invoke-virtual {v0}, [Ljava/lang/String;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 652
    if-ne p0, p1, :cond_1

    .line 716
    :cond_0
    :goto_0
    return v0

    .line 655
    :cond_1
    if-nez p1, :cond_2

    move v0, v1

    .line 656
    goto :goto_0

    .line 658
    :cond_2
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    move v0, v1

    .line 659
    goto :goto_0

    .line 662
    :cond_3
    check-cast p1, Lorg/apache/a/a/b;

    .line 663
    iget-char v2, p0, Lorg/apache/a/a/b;->l:C

    iget-char v3, p1, Lorg/apache/a/a/b;->l:C

    if-eq v2, v3, :cond_4

    move v0, v1

    .line 664
    goto :goto_0

    .line 666
    :cond_4
    iget-object v2, p0, Lorg/apache/a/a/b;->u:Lorg/apache/a/a/e;

    iget-object v3, p1, Lorg/apache/a/a/b;->u:Lorg/apache/a/a/e;

    if-eq v2, v3, :cond_5

    move v0, v1

    .line 667
    goto :goto_0

    .line 669
    :cond_5
    iget-object v2, p0, Lorg/apache/a/a/b;->t:Ljava/lang/Character;

    if-nez v2, :cond_6

    .line 670
    iget-object v2, p1, Lorg/apache/a/a/b;->t:Ljava/lang/Character;

    if-eqz v2, :cond_7

    move v0, v1

    .line 671
    goto :goto_0

    .line 673
    :cond_6
    iget-object v2, p0, Lorg/apache/a/a/b;->t:Ljava/lang/Character;

    iget-object v3, p1, Lorg/apache/a/a/b;->t:Ljava/lang/Character;

    invoke-virtual {v2, v3}, Ljava/lang/Character;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_7

    move v0, v1

    .line 674
    goto :goto_0

    .line 676
    :cond_7
    iget-object v2, p0, Lorg/apache/a/a/b;->k:Ljava/lang/Character;

    if-nez v2, :cond_8

    .line 677
    iget-object v2, p1, Lorg/apache/a/a/b;->k:Ljava/lang/Character;

    if-eqz v2, :cond_9

    move v0, v1

    .line 678
    goto :goto_0

    .line 680
    :cond_8
    iget-object v2, p0, Lorg/apache/a/a/b;->k:Ljava/lang/Character;

    iget-object v3, p1, Lorg/apache/a/a/b;->k:Ljava/lang/Character;

    invoke-virtual {v2, v3}, Ljava/lang/Character;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    move v0, v1

    .line 681
    goto :goto_0

    .line 683
    :cond_9
    iget-object v2, p0, Lorg/apache/a/a/b;->m:Ljava/lang/Character;

    if-nez v2, :cond_a

    .line 684
    iget-object v2, p1, Lorg/apache/a/a/b;->m:Ljava/lang/Character;

    if-eqz v2, :cond_b

    move v0, v1

    .line 685
    goto :goto_0

    .line 687
    :cond_a
    iget-object v2, p0, Lorg/apache/a/a/b;->m:Ljava/lang/Character;

    iget-object v3, p1, Lorg/apache/a/a/b;->m:Ljava/lang/Character;

    invoke-virtual {v2, v3}, Ljava/lang/Character;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_b

    move v0, v1

    .line 688
    goto :goto_0

    .line 690
    :cond_b
    iget-object v2, p0, Lorg/apache/a/a/b;->s:Ljava/lang/String;

    if-nez v2, :cond_c

    .line 691
    iget-object v2, p1, Lorg/apache/a/a/b;->s:Ljava/lang/String;

    if-eqz v2, :cond_d

    move v0, v1

    .line 692
    goto :goto_0

    .line 694
    :cond_c
    iget-object v2, p0, Lorg/apache/a/a/b;->s:Ljava/lang/String;

    iget-object v3, p1, Lorg/apache/a/a/b;->s:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_d

    move v0, v1

    .line 695
    goto :goto_0

    .line 697
    :cond_d
    iget-object v2, p0, Lorg/apache/a/a/b;->n:[Ljava/lang/String;

    iget-object v3, p1, Lorg/apache/a/a/b;->n:[Ljava/lang/String;

    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_e

    move v0, v1

    .line 698
    goto/16 :goto_0

    .line 700
    :cond_e
    iget-boolean v2, p0, Lorg/apache/a/a/b;->r:Z

    iget-boolean v3, p1, Lorg/apache/a/a/b;->r:Z

    if-eq v2, v3, :cond_f

    move v0, v1

    .line 701
    goto/16 :goto_0

    .line 703
    :cond_f
    iget-boolean v2, p0, Lorg/apache/a/a/b;->p:Z

    iget-boolean v3, p1, Lorg/apache/a/a/b;->p:Z

    if-eq v2, v3, :cond_10

    move v0, v1

    .line 704
    goto/16 :goto_0

    .line 706
    :cond_10
    iget-boolean v2, p0, Lorg/apache/a/a/b;->w:Z

    iget-boolean v3, p1, Lorg/apache/a/a/b;->w:Z

    if-eq v2, v3, :cond_11

    move v0, v1

    .line 707
    goto/16 :goto_0

    .line 709
    :cond_11
    iget-object v2, p0, Lorg/apache/a/a/b;->v:Ljava/lang/String;

    if-nez v2, :cond_12

    .line 710
    iget-object v2, p1, Lorg/apache/a/a/b;->v:Ljava/lang/String;

    if-eqz v2, :cond_0

    move v0, v1

    .line 711
    goto/16 :goto_0

    .line 713
    :cond_12
    iget-object v2, p0, Lorg/apache/a/a/b;->v:Ljava/lang/String;

    iget-object v3, p1, Lorg/apache/a/a/b;->v:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 714
    goto/16 :goto_0
.end method

.method public f()Z
    .locals 1

    .prologue
    .line 799
    iget-boolean v0, p0, Lorg/apache/a/a/b;->p:Z

    return v0
.end method

.method public g()Z
    .locals 1

    .prologue
    .line 809
    iget-boolean v0, p0, Lorg/apache/a/a/b;->q:Z

    return v0
.end method

.method public h()Z
    .locals 1

    .prologue
    .line 818
    iget-boolean v0, p0, Lorg/apache/a/a/b;->r:Z

    return v0
.end method

.method public hashCode()I
    .locals 5

    .prologue
    const/16 v3, 0x4d5

    const/16 v2, 0x4cf

    const/4 v1, 0x0

    .line 892
    .line 895
    iget-char v0, p0, Lorg/apache/a/a/b;->l:C

    add-int/lit8 v0, v0, 0x1f

    .line 896
    mul-int/lit8 v4, v0, 0x1f

    iget-object v0, p0, Lorg/apache/a/a/b;->u:Lorg/apache/a/a/e;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/2addr v0, v4

    .line 897
    mul-int/lit8 v4, v0, 0x1f

    iget-object v0, p0, Lorg/apache/a/a/b;->t:Ljava/lang/Character;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v4

    .line 898
    mul-int/lit8 v4, v0, 0x1f

    iget-object v0, p0, Lorg/apache/a/a/b;->k:Ljava/lang/Character;

    if-nez v0, :cond_2

    move v0, v1

    :goto_2
    add-int/2addr v0, v4

    .line 899
    mul-int/lit8 v4, v0, 0x1f

    iget-object v0, p0, Lorg/apache/a/a/b;->m:Ljava/lang/Character;

    if-nez v0, :cond_3

    move v0, v1

    :goto_3
    add-int/2addr v0, v4

    .line 900
    mul-int/lit8 v4, v0, 0x1f

    iget-object v0, p0, Lorg/apache/a/a/b;->s:Ljava/lang/String;

    if-nez v0, :cond_4

    move v0, v1

    :goto_4
    add-int/2addr v0, v4

    .line 901
    mul-int/lit8 v4, v0, 0x1f

    iget-boolean v0, p0, Lorg/apache/a/a/b;->r:Z

    if-eqz v0, :cond_5

    move v0, v2

    :goto_5
    add-int/2addr v0, v4

    .line 902
    mul-int/lit8 v4, v0, 0x1f

    iget-boolean v0, p0, Lorg/apache/a/a/b;->q:Z

    if-eqz v0, :cond_6

    move v0, v2

    :goto_6
    add-int/2addr v0, v4

    .line 903
    mul-int/lit8 v4, v0, 0x1f

    iget-boolean v0, p0, Lorg/apache/a/a/b;->p:Z

    if-eqz v0, :cond_7

    move v0, v2

    :goto_7
    add-int/2addr v0, v4

    .line 904
    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v4, p0, Lorg/apache/a/a/b;->w:Z

    if-eqz v4, :cond_8

    :goto_8
    add-int/2addr v0, v2

    .line 905
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lorg/apache/a/a/b;->v:Ljava/lang/String;

    if-nez v2, :cond_9

    :goto_9
    add-int/2addr v0, v1

    .line 906
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lorg/apache/a/a/b;->n:[Ljava/lang/String;

    invoke-static {v1}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 907
    return v0

    .line 896
    :cond_0
    iget-object v0, p0, Lorg/apache/a/a/b;->u:Lorg/apache/a/a/e;

    invoke-virtual {v0}, Lorg/apache/a/a/e;->hashCode()I

    move-result v0

    goto :goto_0

    .line 897
    :cond_1
    iget-object v0, p0, Lorg/apache/a/a/b;->t:Ljava/lang/Character;

    invoke-virtual {v0}, Ljava/lang/Character;->hashCode()I

    move-result v0

    goto :goto_1

    .line 898
    :cond_2
    iget-object v0, p0, Lorg/apache/a/a/b;->k:Ljava/lang/Character;

    invoke-virtual {v0}, Ljava/lang/Character;->hashCode()I

    move-result v0

    goto :goto_2

    .line 899
    :cond_3
    iget-object v0, p0, Lorg/apache/a/a/b;->m:Ljava/lang/Character;

    invoke-virtual {v0}, Ljava/lang/Character;->hashCode()I

    move-result v0

    goto :goto_3

    .line 900
    :cond_4
    iget-object v0, p0, Lorg/apache/a/a/b;->s:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_4

    :cond_5
    move v0, v3

    .line 901
    goto :goto_5

    :cond_6
    move v0, v3

    .line 902
    goto :goto_6

    :cond_7
    move v0, v3

    .line 903
    goto :goto_7

    :cond_8
    move v2, v3

    .line 904
    goto :goto_8

    .line 905
    :cond_9
    iget-object v1, p0, Lorg/apache/a/a/b;->v:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_9
.end method

.method public i()Ljava/lang/Character;
    .locals 1

    .prologue
    .line 841
    iget-object v0, p0, Lorg/apache/a/a/b;->t:Ljava/lang/Character;

    return-object v0
.end method

.method public j()Lorg/apache/a/a/e;
    .locals 1

    .prologue
    .line 850
    iget-object v0, p0, Lorg/apache/a/a/b;->u:Lorg/apache/a/a/e;

    return-object v0
.end method

.method public k()Z
    .locals 1

    .prologue
    .line 868
    iget-boolean v0, p0, Lorg/apache/a/a/b;->w:Z

    return v0
.end method

.method public l()Z
    .locals 1

    .prologue
    .line 878
    iget-boolean v0, p0, Lorg/apache/a/a/b;->x:Z

    return v0
.end method

.method public m()Z
    .locals 1

    .prologue
    .line 887
    iget-boolean v0, p0, Lorg/apache/a/a/b;->y:Z

    return v0
.end method

.method public n()Z
    .locals 1

    .prologue
    .line 918
    iget-object v0, p0, Lorg/apache/a/a/b;->k:Ljava/lang/Character;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public o()Z
    .locals 1

    .prologue
    .line 927
    iget-object v0, p0, Lorg/apache/a/a/b;->m:Ljava/lang/Character;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public p()Z
    .locals 1

    .prologue
    .line 936
    iget-object v0, p0, Lorg/apache/a/a/b;->s:Ljava/lang/String;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public q()Z
    .locals 1

    .prologue
    .line 945
    iget-object v0, p0, Lorg/apache/a/a/b;->t:Ljava/lang/Character;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public r()Lorg/apache/a/a/b;
    .locals 1

    .prologue
    .line 1420
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lorg/apache/a/a/b;->a(Z)Lorg/apache/a/a/b;

    move-result-object v0

    return-object v0
.end method

.method public s()Lorg/apache/a/a/b;
    .locals 1

    .prologue
    .line 1756
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lorg/apache/a/a/b;->c(Z)Lorg/apache/a/a/b;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 5

    .prologue
    const/16 v4, 0x3e

    const/16 v3, 0x20

    .line 1289
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 1290
    const-string v1, "Delimiter=<"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-char v2, p0, Lorg/apache/a/a/b;->l:C

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 1291
    invoke-virtual {p0}, Lorg/apache/a/a/b;->o()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1292
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 1293
    const-string v1, "Escape=<"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lorg/apache/a/a/b;->m:Ljava/lang/Character;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 1295
    :cond_0
    invoke-virtual {p0}, Lorg/apache/a/a/b;->q()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1296
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 1297
    const-string v1, "QuoteChar=<"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lorg/apache/a/a/b;->t:Ljava/lang/Character;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 1299
    :cond_1
    invoke-virtual {p0}, Lorg/apache/a/a/b;->n()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1300
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 1301
    const-string v1, "CommentStart=<"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lorg/apache/a/a/b;->k:Ljava/lang/Character;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 1303
    :cond_2
    invoke-virtual {p0}, Lorg/apache/a/a/b;->p()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1304
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 1305
    const-string v1, "NullString=<"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lorg/apache/a/a/b;->s:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 1307
    :cond_3
    iget-object v1, p0, Lorg/apache/a/a/b;->v:Ljava/lang/String;

    if-eqz v1, :cond_4

    .line 1308
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 1309
    const-string v1, "RecordSeparator=<"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lorg/apache/a/a/b;->v:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 1311
    :cond_4
    invoke-virtual {p0}, Lorg/apache/a/a/b;->f()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 1312
    const-string v1, " EmptyLines:ignored"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1314
    :cond_5
    invoke-virtual {p0}, Lorg/apache/a/a/b;->h()Z

    move-result v1

    if-eqz v1, :cond_6

    .line 1315
    const-string v1, " SurroundingSpaces:ignored"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1317
    :cond_6
    invoke-virtual {p0}, Lorg/apache/a/a/b;->g()Z

    move-result v1

    if-eqz v1, :cond_7

    .line 1318
    const-string v1, " IgnoreHeaderCase:ignored"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1320
    :cond_7
    const-string v1, " SkipHeaderRecord:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lorg/apache/a/a/b;->w:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 1321
    iget-object v1, p0, Lorg/apache/a/a/b;->o:[Ljava/lang/String;

    if-eqz v1, :cond_8

    .line 1322
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 1323
    const-string v1, "HeaderComments:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lorg/apache/a/a/b;->o:[Ljava/lang/String;

    invoke-static {v2}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1325
    :cond_8
    iget-object v1, p0, Lorg/apache/a/a/b;->n:[Ljava/lang/String;

    if-eqz v1, :cond_9

    .line 1326
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 1327
    const-string v1, "Header:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lorg/apache/a/a/b;->n:[Ljava/lang/String;

    invoke-static {v2}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1329
    :cond_9
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
