.class public final enum Lorg/apache/a/a/e;
.super Ljava/lang/Enum;
.source "QuoteMode.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lorg/apache/a/a/e;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lorg/apache/a/a/e;

.field public static final enum b:Lorg/apache/a/a/e;

.field public static final enum c:Lorg/apache/a/a/e;

.field public static final enum d:Lorg/apache/a/a/e;

.field public static final enum e:Lorg/apache/a/a/e;

.field private static final synthetic f:[Lorg/apache/a/a/e;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 28
    new-instance v0, Lorg/apache/a/a/e;

    const-string v1, "ALL"

    invoke-direct {v0, v1, v2}, Lorg/apache/a/a/e;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/apache/a/a/e;->a:Lorg/apache/a/a/e;

    .line 33
    new-instance v0, Lorg/apache/a/a/e;

    const-string v1, "ALL_NON_NULL"

    invoke-direct {v0, v1, v3}, Lorg/apache/a/a/e;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/apache/a/a/e;->b:Lorg/apache/a/a/e;

    .line 39
    new-instance v0, Lorg/apache/a/a/e;

    const-string v1, "MINIMAL"

    invoke-direct {v0, v1, v4}, Lorg/apache/a/a/e;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/apache/a/a/e;->c:Lorg/apache/a/a/e;

    .line 44
    new-instance v0, Lorg/apache/a/a/e;

    const-string v1, "NON_NUMERIC"

    invoke-direct {v0, v1, v5}, Lorg/apache/a/a/e;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/apache/a/a/e;->d:Lorg/apache/a/a/e;

    .line 50
    new-instance v0, Lorg/apache/a/a/e;

    const-string v1, "NONE"

    invoke-direct {v0, v1, v6}, Lorg/apache/a/a/e;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/apache/a/a/e;->e:Lorg/apache/a/a/e;

    .line 23
    const/4 v0, 0x5

    new-array v0, v0, [Lorg/apache/a/a/e;

    sget-object v1, Lorg/apache/a/a/e;->a:Lorg/apache/a/a/e;

    aput-object v1, v0, v2

    sget-object v1, Lorg/apache/a/a/e;->b:Lorg/apache/a/a/e;

    aput-object v1, v0, v3

    sget-object v1, Lorg/apache/a/a/e;->c:Lorg/apache/a/a/e;

    aput-object v1, v0, v4

    sget-object v1, Lorg/apache/a/a/e;->d:Lorg/apache/a/a/e;

    aput-object v1, v0, v5

    sget-object v1, Lorg/apache/a/a/e;->e:Lorg/apache/a/a/e;

    aput-object v1, v0, v6

    sput-object v0, Lorg/apache/a/a/e;->f:[Lorg/apache/a/a/e;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 23
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lorg/apache/a/a/e;
    .locals 1

    .prologue
    .line 23
    const-class v0, Lorg/apache/a/a/e;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lorg/apache/a/a/e;

    return-object v0
.end method

.method public static values()[Lorg/apache/a/a/e;
    .locals 1

    .prologue
    .line 23
    sget-object v0, Lorg/apache/a/a/e;->f:[Lorg/apache/a/a/e;

    invoke-virtual {v0}, [Lorg/apache/a/a/e;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lorg/apache/a/a/e;

    return-object v0
.end method
