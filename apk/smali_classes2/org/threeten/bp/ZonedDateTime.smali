.class public final Lorg/threeten/bp/ZonedDateTime;
.super Lorg/threeten/bp/chrono/ChronoZonedDateTime;
.source "ZonedDateTime.java"

# interfaces
.implements Ljava/io/Serializable;
.implements Lorg/threeten/bp/temporal/Temporal;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/threeten/bp/ZonedDateTime$2;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lorg/threeten/bp/chrono/ChronoZonedDateTime",
        "<",
        "Lorg/threeten/bp/LocalDate;",
        ">;",
        "Ljava/io/Serializable;",
        "Lorg/threeten/bp/temporal/Temporal;"
    }
.end annotation


# static fields
.field public static final a:Lorg/threeten/bp/temporal/TemporalQuery;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lorg/threeten/bp/temporal/TemporalQuery",
            "<",
            "Lorg/threeten/bp/ZonedDateTime;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final b:Lorg/threeten/bp/LocalDateTime;

.field private final c:Lorg/threeten/bp/ZoneOffset;

.field private final d:Lorg/threeten/bp/ZoneId;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 129
    new-instance v0, Lorg/threeten/bp/ZonedDateTime$1;

    invoke-direct {v0}, Lorg/threeten/bp/ZonedDateTime$1;-><init>()V

    sput-object v0, Lorg/threeten/bp/ZonedDateTime;->a:Lorg/threeten/bp/temporal/TemporalQuery;

    return-void
.end method

.method private constructor <init>(Lorg/threeten/bp/LocalDateTime;Lorg/threeten/bp/ZoneOffset;Lorg/threeten/bp/ZoneId;)V
    .locals 0

    .prologue
    .line 573
    invoke-direct {p0}, Lorg/threeten/bp/chrono/ChronoZonedDateTime;-><init>()V

    .line 574
    iput-object p1, p0, Lorg/threeten/bp/ZonedDateTime;->b:Lorg/threeten/bp/LocalDateTime;

    .line 575
    iput-object p2, p0, Lorg/threeten/bp/ZonedDateTime;->c:Lorg/threeten/bp/ZoneOffset;

    .line 576
    iput-object p3, p0, Lorg/threeten/bp/ZonedDateTime;->d:Lorg/threeten/bp/ZoneId;

    .line 577
    return-void
.end method

.method private static a(JILorg/threeten/bp/ZoneId;)Lorg/threeten/bp/ZonedDateTime;
    .locals 4

    .prologue
    .line 419
    invoke-virtual {p3}, Lorg/threeten/bp/ZoneId;->d()Lorg/threeten/bp/zone/ZoneRules;

    move-result-object v0

    .line 420
    int-to-long v2, p2

    invoke-static {p0, p1, v2, v3}, Lorg/threeten/bp/Instant;->a(JJ)Lorg/threeten/bp/Instant;

    move-result-object v1

    .line 421
    invoke-virtual {v0, v1}, Lorg/threeten/bp/zone/ZoneRules;->a(Lorg/threeten/bp/Instant;)Lorg/threeten/bp/ZoneOffset;

    move-result-object v0

    .line 422
    invoke-static {p0, p1, p2, v0}, Lorg/threeten/bp/LocalDateTime;->a(JILorg/threeten/bp/ZoneOffset;)Lorg/threeten/bp/LocalDateTime;

    move-result-object v1

    .line 423
    new-instance v2, Lorg/threeten/bp/ZonedDateTime;

    invoke-direct {v2, v1, v0, p3}, Lorg/threeten/bp/ZonedDateTime;-><init>(Lorg/threeten/bp/LocalDateTime;Lorg/threeten/bp/ZoneOffset;Lorg/threeten/bp/ZoneId;)V

    return-object v2
.end method

.method static a(Ljava/io/DataInput;)Lorg/threeten/bp/ZonedDateTime;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2120
    invoke-static {p0}, Lorg/threeten/bp/LocalDateTime;->a(Ljava/io/DataInput;)Lorg/threeten/bp/LocalDateTime;

    move-result-object v1

    .line 2121
    invoke-static {p0}, Lorg/threeten/bp/ZoneOffset;->a(Ljava/io/DataInput;)Lorg/threeten/bp/ZoneOffset;

    move-result-object v2

    .line 2122
    invoke-static {p0}, Lorg/threeten/bp/Ser;->a(Ljava/io/DataInput;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/threeten/bp/ZoneId;

    .line 2123
    invoke-static {v1, v2, v0}, Lorg/threeten/bp/ZonedDateTime;->b(Lorg/threeten/bp/LocalDateTime;Lorg/threeten/bp/ZoneOffset;Lorg/threeten/bp/ZoneId;)Lorg/threeten/bp/ZonedDateTime;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/lang/CharSequence;)Lorg/threeten/bp/ZonedDateTime;
    .locals 1

    .prologue
    .line 547
    sget-object v0, Lorg/threeten/bp/format/DateTimeFormatter;->i:Lorg/threeten/bp/format/DateTimeFormatter;

    invoke-static {p0, v0}, Lorg/threeten/bp/ZonedDateTime;->a(Ljava/lang/CharSequence;Lorg/threeten/bp/format/DateTimeFormatter;)Lorg/threeten/bp/ZonedDateTime;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/lang/CharSequence;Lorg/threeten/bp/format/DateTimeFormatter;)Lorg/threeten/bp/ZonedDateTime;
    .locals 1

    .prologue
    .line 561
    const-string v0, "formatter"

    invoke-static {p1, v0}, Lorg/threeten/bp/jdk8/Jdk8Methods;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 562
    sget-object v0, Lorg/threeten/bp/ZonedDateTime;->a:Lorg/threeten/bp/temporal/TemporalQuery;

    invoke-virtual {p1, p0, v0}, Lorg/threeten/bp/format/DateTimeFormatter;->a(Ljava/lang/CharSequence;Lorg/threeten/bp/temporal/TemporalQuery;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/threeten/bp/ZonedDateTime;

    return-object v0
.end method

.method public static a(Lorg/threeten/bp/Instant;Lorg/threeten/bp/ZoneId;)Lorg/threeten/bp/ZonedDateTime;
    .locals 3

    .prologue
    .line 376
    const-string v0, "instant"

    invoke-static {p0, v0}, Lorg/threeten/bp/jdk8/Jdk8Methods;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 377
    const-string v0, "zone"

    invoke-static {p1, v0}, Lorg/threeten/bp/jdk8/Jdk8Methods;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 378
    invoke-virtual {p0}, Lorg/threeten/bp/Instant;->a()J

    move-result-wide v0

    invoke-virtual {p0}, Lorg/threeten/bp/Instant;->b()I

    move-result v2

    invoke-static {v0, v1, v2, p1}, Lorg/threeten/bp/ZonedDateTime;->a(JILorg/threeten/bp/ZoneId;)Lorg/threeten/bp/ZonedDateTime;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lorg/threeten/bp/LocalDate;Lorg/threeten/bp/LocalTime;Lorg/threeten/bp/ZoneId;)Lorg/threeten/bp/ZonedDateTime;
    .locals 1

    .prologue
    .line 234
    invoke-static {p0, p1}, Lorg/threeten/bp/LocalDateTime;->a(Lorg/threeten/bp/LocalDate;Lorg/threeten/bp/LocalTime;)Lorg/threeten/bp/LocalDateTime;

    move-result-object v0

    invoke-static {v0, p2}, Lorg/threeten/bp/ZonedDateTime;->a(Lorg/threeten/bp/LocalDateTime;Lorg/threeten/bp/ZoneId;)Lorg/threeten/bp/ZonedDateTime;

    move-result-object v0

    return-object v0
.end method

.method private a(Lorg/threeten/bp/LocalDateTime;)Lorg/threeten/bp/ZonedDateTime;
    .locals 2

    .prologue
    .line 586
    iget-object v0, p0, Lorg/threeten/bp/ZonedDateTime;->d:Lorg/threeten/bp/ZoneId;

    iget-object v1, p0, Lorg/threeten/bp/ZonedDateTime;->c:Lorg/threeten/bp/ZoneOffset;

    invoke-static {p1, v0, v1}, Lorg/threeten/bp/ZonedDateTime;->a(Lorg/threeten/bp/LocalDateTime;Lorg/threeten/bp/ZoneId;Lorg/threeten/bp/ZoneOffset;)Lorg/threeten/bp/ZonedDateTime;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lorg/threeten/bp/LocalDateTime;Lorg/threeten/bp/ZoneId;)Lorg/threeten/bp/ZonedDateTime;
    .locals 1

    .prologue
    .line 262
    const/4 v0, 0x0

    invoke-static {p0, p1, v0}, Lorg/threeten/bp/ZonedDateTime;->a(Lorg/threeten/bp/LocalDateTime;Lorg/threeten/bp/ZoneId;Lorg/threeten/bp/ZoneOffset;)Lorg/threeten/bp/ZonedDateTime;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lorg/threeten/bp/LocalDateTime;Lorg/threeten/bp/ZoneId;Lorg/threeten/bp/ZoneOffset;)Lorg/threeten/bp/ZonedDateTime;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 336
    const-string v0, "localDateTime"

    invoke-static {p0, v0}, Lorg/threeten/bp/jdk8/Jdk8Methods;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 337
    const-string v0, "zone"

    invoke-static {p1, v0}, Lorg/threeten/bp/jdk8/Jdk8Methods;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 338
    instance-of v0, p1, Lorg/threeten/bp/ZoneOffset;

    if-eqz v0, :cond_0

    .line 339
    new-instance v1, Lorg/threeten/bp/ZonedDateTime;

    move-object v0, p1

    check-cast v0, Lorg/threeten/bp/ZoneOffset;

    invoke-direct {v1, p0, v0, p1}, Lorg/threeten/bp/ZonedDateTime;-><init>(Lorg/threeten/bp/LocalDateTime;Lorg/threeten/bp/ZoneOffset;Lorg/threeten/bp/ZoneId;)V

    move-object v0, v1

    .line 357
    :goto_0
    return-object v0

    .line 341
    :cond_0
    invoke-virtual {p1}, Lorg/threeten/bp/ZoneId;->d()Lorg/threeten/bp/zone/ZoneRules;

    move-result-object v0

    .line 342
    invoke-virtual {v0, p0}, Lorg/threeten/bp/zone/ZoneRules;->a(Lorg/threeten/bp/LocalDateTime;)Ljava/util/List;

    move-result-object v1

    .line 344
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_1

    .line 345
    invoke-interface {v1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/threeten/bp/ZoneOffset;

    .line 357
    :goto_1
    new-instance v1, Lorg/threeten/bp/ZonedDateTime;

    invoke-direct {v1, p0, v0, p1}, Lorg/threeten/bp/ZonedDateTime;-><init>(Lorg/threeten/bp/LocalDateTime;Lorg/threeten/bp/ZoneOffset;Lorg/threeten/bp/ZoneId;)V

    move-object v0, v1

    goto :goto_0

    .line 346
    :cond_1
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v2

    if-nez v2, :cond_2

    .line 347
    invoke-virtual {v0, p0}, Lorg/threeten/bp/zone/ZoneRules;->b(Lorg/threeten/bp/LocalDateTime;)Lorg/threeten/bp/zone/ZoneOffsetTransition;

    move-result-object v0

    .line 348
    invoke-virtual {v0}, Lorg/threeten/bp/zone/ZoneOffsetTransition;->g()Lorg/threeten/bp/Duration;

    move-result-object v1

    invoke-virtual {v1}, Lorg/threeten/bp/Duration;->a()J

    move-result-wide v2

    invoke-virtual {p0, v2, v3}, Lorg/threeten/bp/LocalDateTime;->d(J)Lorg/threeten/bp/LocalDateTime;

    move-result-object p0

    .line 349
    invoke-virtual {v0}, Lorg/threeten/bp/zone/ZoneOffsetTransition;->f()Lorg/threeten/bp/ZoneOffset;

    move-result-object v0

    goto :goto_1

    .line 351
    :cond_2
    if-eqz p2, :cond_3

    invoke-interface {v1, p2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    move-object v0, p2

    .line 352
    goto :goto_1

    .line 354
    :cond_3
    invoke-interface {v1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    const-string v1, "offset"

    invoke-static {v0, v1}, Lorg/threeten/bp/jdk8/Jdk8Methods;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/threeten/bp/ZoneOffset;

    goto :goto_1
.end method

.method public static a(Lorg/threeten/bp/LocalDateTime;Lorg/threeten/bp/ZoneOffset;Lorg/threeten/bp/ZoneId;)Lorg/threeten/bp/ZonedDateTime;
    .locals 3

    .prologue
    .line 402
    const-string v0, "localDateTime"

    invoke-static {p0, v0}, Lorg/threeten/bp/jdk8/Jdk8Methods;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 403
    const-string v0, "offset"

    invoke-static {p1, v0}, Lorg/threeten/bp/jdk8/Jdk8Methods;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 404
    const-string v0, "zone"

    invoke-static {p2, v0}, Lorg/threeten/bp/jdk8/Jdk8Methods;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 405
    invoke-virtual {p0, p1}, Lorg/threeten/bp/LocalDateTime;->c(Lorg/threeten/bp/ZoneOffset;)J

    move-result-wide v0

    invoke-virtual {p0}, Lorg/threeten/bp/LocalDateTime;->i()I

    move-result v2

    invoke-static {v0, v1, v2, p2}, Lorg/threeten/bp/ZonedDateTime;->a(JILorg/threeten/bp/ZoneId;)Lorg/threeten/bp/ZonedDateTime;

    move-result-object v0

    return-object v0
.end method

.method private a(Lorg/threeten/bp/ZoneOffset;)Lorg/threeten/bp/ZonedDateTime;
    .locals 3

    .prologue
    .line 608
    iget-object v0, p0, Lorg/threeten/bp/ZonedDateTime;->c:Lorg/threeten/bp/ZoneOffset;

    invoke-virtual {p1, v0}, Lorg/threeten/bp/ZoneOffset;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lorg/threeten/bp/ZonedDateTime;->d:Lorg/threeten/bp/ZoneId;

    invoke-virtual {v0}, Lorg/threeten/bp/ZoneId;->d()Lorg/threeten/bp/zone/ZoneRules;

    move-result-object v0

    iget-object v1, p0, Lorg/threeten/bp/ZonedDateTime;->b:Lorg/threeten/bp/LocalDateTime;

    invoke-virtual {v0, v1, p1}, Lorg/threeten/bp/zone/ZoneRules;->a(Lorg/threeten/bp/LocalDateTime;Lorg/threeten/bp/ZoneOffset;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 609
    new-instance v0, Lorg/threeten/bp/ZonedDateTime;

    iget-object v1, p0, Lorg/threeten/bp/ZonedDateTime;->b:Lorg/threeten/bp/LocalDateTime;

    iget-object v2, p0, Lorg/threeten/bp/ZonedDateTime;->d:Lorg/threeten/bp/ZoneId;

    invoke-direct {v0, v1, p1, v2}, Lorg/threeten/bp/ZonedDateTime;-><init>(Lorg/threeten/bp/LocalDateTime;Lorg/threeten/bp/ZoneOffset;Lorg/threeten/bp/ZoneId;)V

    move-object p0, v0

    .line 611
    :cond_0
    return-object p0
.end method

.method public static a(Lorg/threeten/bp/temporal/TemporalAccessor;)Lorg/threeten/bp/ZonedDateTime;
    .locals 4

    .prologue
    .line 511
    instance-of v0, p0, Lorg/threeten/bp/ZonedDateTime;

    if-eqz v0, :cond_0

    .line 512
    check-cast p0, Lorg/threeten/bp/ZonedDateTime;

    .line 527
    :goto_0
    return-object p0

    .line 515
    :cond_0
    :try_start_0
    invoke-static {p0}, Lorg/threeten/bp/ZoneId;->a(Lorg/threeten/bp/temporal/TemporalAccessor;)Lorg/threeten/bp/ZoneId;

    move-result-object v0

    .line 516
    sget-object v1, Lorg/threeten/bp/temporal/ChronoField;->C:Lorg/threeten/bp/temporal/ChronoField;

    invoke-interface {p0, v1}, Lorg/threeten/bp/temporal/TemporalAccessor;->a(Lorg/threeten/bp/temporal/TemporalField;)Z
    :try_end_0
    .catch Lorg/threeten/bp/DateTimeException; {:try_start_0 .. :try_end_0} :catch_1

    move-result v1

    if-eqz v1, :cond_1

    .line 518
    :try_start_1
    sget-object v1, Lorg/threeten/bp/temporal/ChronoField;->C:Lorg/threeten/bp/temporal/ChronoField;

    invoke-interface {p0, v1}, Lorg/threeten/bp/temporal/TemporalAccessor;->d(Lorg/threeten/bp/temporal/TemporalField;)J

    move-result-wide v2

    .line 519
    sget-object v1, Lorg/threeten/bp/temporal/ChronoField;->a:Lorg/threeten/bp/temporal/ChronoField;

    invoke-interface {p0, v1}, Lorg/threeten/bp/temporal/TemporalAccessor;->c(Lorg/threeten/bp/temporal/TemporalField;)I

    move-result v1

    .line 520
    invoke-static {v2, v3, v1, v0}, Lorg/threeten/bp/ZonedDateTime;->a(JILorg/threeten/bp/ZoneId;)Lorg/threeten/bp/ZonedDateTime;
    :try_end_1
    .catch Lorg/threeten/bp/DateTimeException; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object p0

    goto :goto_0

    .line 522
    :catch_0
    move-exception v1

    .line 526
    :cond_1
    :try_start_2
    invoke-static {p0}, Lorg/threeten/bp/LocalDateTime;->a(Lorg/threeten/bp/temporal/TemporalAccessor;)Lorg/threeten/bp/LocalDateTime;

    move-result-object v1

    .line 527
    invoke-static {v1, v0}, Lorg/threeten/bp/ZonedDateTime;->a(Lorg/threeten/bp/LocalDateTime;Lorg/threeten/bp/ZoneId;)Lorg/threeten/bp/ZonedDateTime;
    :try_end_2
    .catch Lorg/threeten/bp/DateTimeException; {:try_start_2 .. :try_end_2} :catch_1

    move-result-object p0

    goto :goto_0

    .line 528
    :catch_1
    move-exception v0

    .line 529
    new-instance v0, Lorg/threeten/bp/DateTimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unable to obtain ZonedDateTime from TemporalAccessor: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", type "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/threeten/bp/DateTimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private b(Lorg/threeten/bp/LocalDateTime;)Lorg/threeten/bp/ZonedDateTime;
    .locals 2

    .prologue
    .line 596
    iget-object v0, p0, Lorg/threeten/bp/ZonedDateTime;->c:Lorg/threeten/bp/ZoneOffset;

    iget-object v1, p0, Lorg/threeten/bp/ZonedDateTime;->d:Lorg/threeten/bp/ZoneId;

    invoke-static {p1, v0, v1}, Lorg/threeten/bp/ZonedDateTime;->a(Lorg/threeten/bp/LocalDateTime;Lorg/threeten/bp/ZoneOffset;Lorg/threeten/bp/ZoneId;)Lorg/threeten/bp/ZonedDateTime;

    move-result-object v0

    return-object v0
.end method

.method private static b(Lorg/threeten/bp/LocalDateTime;Lorg/threeten/bp/ZoneOffset;Lorg/threeten/bp/ZoneId;)Lorg/threeten/bp/ZonedDateTime;
    .locals 2

    .prologue
    .line 482
    const-string v0, "localDateTime"

    invoke-static {p0, v0}, Lorg/threeten/bp/jdk8/Jdk8Methods;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 483
    const-string v0, "offset"

    invoke-static {p1, v0}, Lorg/threeten/bp/jdk8/Jdk8Methods;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 484
    const-string v0, "zone"

    invoke-static {p2, v0}, Lorg/threeten/bp/jdk8/Jdk8Methods;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 485
    instance-of v0, p2, Lorg/threeten/bp/ZoneOffset;

    if-eqz v0, :cond_0

    invoke-virtual {p1, p2}, Lorg/threeten/bp/ZoneOffset;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 486
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "ZoneId must match ZoneOffset"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 488
    :cond_0
    new-instance v0, Lorg/threeten/bp/ZonedDateTime;

    invoke-direct {v0, p0, p1, p2}, Lorg/threeten/bp/ZonedDateTime;-><init>(Lorg/threeten/bp/LocalDateTime;Lorg/threeten/bp/ZoneOffset;Lorg/threeten/bp/ZoneId;)V

    return-object v0
.end method

.method private readResolve()Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/ObjectStreamException;
        }
    .end annotation

    .prologue
    .line 2110
    new-instance v0, Ljava/io/InvalidObjectException;

    const-string v1, "Deserialization via serialization delegate"

    invoke-direct {v0, v1}, Ljava/io/InvalidObjectException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private writeReplace()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 2101
    new-instance v0, Lorg/threeten/bp/Ser;

    const/4 v1, 0x6

    invoke-direct {v0, v1, p0}, Lorg/threeten/bp/Ser;-><init>(BLjava/lang/Object;)V

    return-object v0
.end method


# virtual methods
.method public a(Lorg/threeten/bp/temporal/Temporal;Lorg/threeten/bp/temporal/TemporalUnit;)J
    .locals 2

    .prologue
    .line 1966
    invoke-static {p1}, Lorg/threeten/bp/ZonedDateTime;->a(Lorg/threeten/bp/temporal/TemporalAccessor;)Lorg/threeten/bp/ZonedDateTime;

    move-result-object v0

    .line 1967
    instance-of v1, p2, Lorg/threeten/bp/temporal/ChronoUnit;

    if-eqz v1, :cond_1

    .line 1968
    iget-object v1, p0, Lorg/threeten/bp/ZonedDateTime;->d:Lorg/threeten/bp/ZoneId;

    invoke-virtual {v0, v1}, Lorg/threeten/bp/ZonedDateTime;->b(Lorg/threeten/bp/ZoneId;)Lorg/threeten/bp/ZonedDateTime;

    move-result-object v0

    .line 1969
    invoke-interface {p2}, Lorg/threeten/bp/temporal/TemporalUnit;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1970
    iget-object v1, p0, Lorg/threeten/bp/ZonedDateTime;->b:Lorg/threeten/bp/LocalDateTime;

    iget-object v0, v0, Lorg/threeten/bp/ZonedDateTime;->b:Lorg/threeten/bp/LocalDateTime;

    invoke-virtual {v1, v0, p2}, Lorg/threeten/bp/LocalDateTime;->a(Lorg/threeten/bp/temporal/Temporal;Lorg/threeten/bp/temporal/TemporalUnit;)J

    move-result-wide v0

    .line 1975
    :goto_0
    return-wide v0

    .line 1972
    :cond_0
    invoke-virtual {p0}, Lorg/threeten/bp/ZonedDateTime;->g()Lorg/threeten/bp/OffsetDateTime;

    move-result-object v1

    invoke-virtual {v0}, Lorg/threeten/bp/ZonedDateTime;->g()Lorg/threeten/bp/OffsetDateTime;

    move-result-object v0

    invoke-virtual {v1, v0, p2}, Lorg/threeten/bp/OffsetDateTime;->a(Lorg/threeten/bp/temporal/Temporal;Lorg/threeten/bp/temporal/TemporalUnit;)J

    move-result-wide v0

    goto :goto_0

    .line 1975
    :cond_1
    invoke-interface {p2, p0, v0}, Lorg/threeten/bp/temporal/TemporalUnit;->a(Lorg/threeten/bp/temporal/Temporal;Lorg/threeten/bp/temporal/Temporal;)J

    move-result-wide v0

    goto :goto_0
.end method

.method public a(Lorg/threeten/bp/temporal/TemporalQuery;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<R:",
            "Ljava/lang/Object;",
            ">(",
            "Lorg/threeten/bp/temporal/TemporalQuery",
            "<TR;>;)TR;"
        }
    .end annotation

    .prologue
    .line 1896
    invoke-static {}, Lorg/threeten/bp/temporal/TemporalQueries;->f()Lorg/threeten/bp/temporal/TemporalQuery;

    move-result-object v0

    if-ne p1, v0, :cond_0

    .line 1897
    invoke-virtual {p0}, Lorg/threeten/bp/ZonedDateTime;->e()Lorg/threeten/bp/LocalDate;

    move-result-object v0

    .line 1899
    :goto_0
    return-object v0

    :cond_0
    invoke-super {p0, p1}, Lorg/threeten/bp/chrono/ChronoZonedDateTime;->a(Lorg/threeten/bp/temporal/TemporalQuery;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method public a(Lorg/threeten/bp/format/DateTimeFormatter;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 2096
    invoke-super {p0, p1}, Lorg/threeten/bp/chrono/ChronoZonedDateTime;->a(Lorg/threeten/bp/format/DateTimeFormatter;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public a()Lorg/threeten/bp/ZoneOffset;
    .locals 1

    .prologue
    .line 793
    iget-object v0, p0, Lorg/threeten/bp/ZonedDateTime;->c:Lorg/threeten/bp/ZoneOffset;

    return-object v0
.end method

.method public a(JLorg/threeten/bp/temporal/TemporalUnit;)Lorg/threeten/bp/ZonedDateTime;
    .locals 1

    .prologue
    .line 1464
    instance-of v0, p3, Lorg/threeten/bp/temporal/ChronoUnit;

    if-eqz v0, :cond_1

    .line 1465
    invoke-interface {p3}, Lorg/threeten/bp/temporal/TemporalUnit;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1466
    iget-object v0, p0, Lorg/threeten/bp/ZonedDateTime;->b:Lorg/threeten/bp/LocalDateTime;

    invoke-virtual {v0, p1, p2, p3}, Lorg/threeten/bp/LocalDateTime;->a(JLorg/threeten/bp/temporal/TemporalUnit;)Lorg/threeten/bp/LocalDateTime;

    move-result-object v0

    invoke-direct {p0, v0}, Lorg/threeten/bp/ZonedDateTime;->a(Lorg/threeten/bp/LocalDateTime;)Lorg/threeten/bp/ZonedDateTime;

    move-result-object v0

    .line 1471
    :goto_0
    return-object v0

    .line 1468
    :cond_0
    iget-object v0, p0, Lorg/threeten/bp/ZonedDateTime;->b:Lorg/threeten/bp/LocalDateTime;

    invoke-virtual {v0, p1, p2, p3}, Lorg/threeten/bp/LocalDateTime;->a(JLorg/threeten/bp/temporal/TemporalUnit;)Lorg/threeten/bp/LocalDateTime;

    move-result-object v0

    invoke-direct {p0, v0}, Lorg/threeten/bp/ZonedDateTime;->b(Lorg/threeten/bp/LocalDateTime;)Lorg/threeten/bp/ZonedDateTime;

    move-result-object v0

    goto :goto_0

    .line 1471
    :cond_1
    invoke-interface {p3, p0, p1, p2}, Lorg/threeten/bp/temporal/TemporalUnit;->a(Lorg/threeten/bp/temporal/Temporal;J)Lorg/threeten/bp/temporal/Temporal;

    move-result-object v0

    check-cast v0, Lorg/threeten/bp/ZonedDateTime;

    goto :goto_0
.end method

.method public a(Lorg/threeten/bp/ZoneId;)Lorg/threeten/bp/ZonedDateTime;
    .locals 2

    .prologue
    .line 890
    const-string v0, "zone"

    invoke-static {p1, v0}, Lorg/threeten/bp/jdk8/Jdk8Methods;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 891
    iget-object v0, p0, Lorg/threeten/bp/ZonedDateTime;->d:Lorg/threeten/bp/ZoneId;

    invoke-virtual {v0, p1}, Lorg/threeten/bp/ZoneId;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return-object p0

    :cond_0
    iget-object v0, p0, Lorg/threeten/bp/ZonedDateTime;->b:Lorg/threeten/bp/LocalDateTime;

    iget-object v1, p0, Lorg/threeten/bp/ZonedDateTime;->c:Lorg/threeten/bp/ZoneOffset;

    invoke-static {v0, p1, v1}, Lorg/threeten/bp/ZonedDateTime;->a(Lorg/threeten/bp/LocalDateTime;Lorg/threeten/bp/ZoneId;Lorg/threeten/bp/ZoneOffset;)Lorg/threeten/bp/ZonedDateTime;

    move-result-object p0

    goto :goto_0
.end method

.method public a(Lorg/threeten/bp/temporal/TemporalAdjuster;)Lorg/threeten/bp/ZonedDateTime;
    .locals 4

    .prologue
    .line 1115
    instance-of v0, p1, Lorg/threeten/bp/LocalDate;

    if-eqz v0, :cond_0

    .line 1116
    check-cast p1, Lorg/threeten/bp/LocalDate;

    iget-object v0, p0, Lorg/threeten/bp/ZonedDateTime;->b:Lorg/threeten/bp/LocalDateTime;

    invoke-virtual {v0}, Lorg/threeten/bp/LocalDateTime;->k()Lorg/threeten/bp/LocalTime;

    move-result-object v0

    invoke-static {p1, v0}, Lorg/threeten/bp/LocalDateTime;->a(Lorg/threeten/bp/LocalDate;Lorg/threeten/bp/LocalTime;)Lorg/threeten/bp/LocalDateTime;

    move-result-object v0

    invoke-direct {p0, v0}, Lorg/threeten/bp/ZonedDateTime;->a(Lorg/threeten/bp/LocalDateTime;)Lorg/threeten/bp/ZonedDateTime;

    move-result-object v0

    .line 1127
    :goto_0
    return-object v0

    .line 1117
    :cond_0
    instance-of v0, p1, Lorg/threeten/bp/LocalTime;

    if-eqz v0, :cond_1

    .line 1118
    iget-object v0, p0, Lorg/threeten/bp/ZonedDateTime;->b:Lorg/threeten/bp/LocalDateTime;

    invoke-virtual {v0}, Lorg/threeten/bp/LocalDateTime;->j()Lorg/threeten/bp/LocalDate;

    move-result-object v0

    check-cast p1, Lorg/threeten/bp/LocalTime;

    invoke-static {v0, p1}, Lorg/threeten/bp/LocalDateTime;->a(Lorg/threeten/bp/LocalDate;Lorg/threeten/bp/LocalTime;)Lorg/threeten/bp/LocalDateTime;

    move-result-object v0

    invoke-direct {p0, v0}, Lorg/threeten/bp/ZonedDateTime;->a(Lorg/threeten/bp/LocalDateTime;)Lorg/threeten/bp/ZonedDateTime;

    move-result-object v0

    goto :goto_0

    .line 1119
    :cond_1
    instance-of v0, p1, Lorg/threeten/bp/LocalDateTime;

    if-eqz v0, :cond_2

    .line 1120
    check-cast p1, Lorg/threeten/bp/LocalDateTime;

    invoke-direct {p0, p1}, Lorg/threeten/bp/ZonedDateTime;->a(Lorg/threeten/bp/LocalDateTime;)Lorg/threeten/bp/ZonedDateTime;

    move-result-object v0

    goto :goto_0

    .line 1121
    :cond_2
    instance-of v0, p1, Lorg/threeten/bp/Instant;

    if-eqz v0, :cond_3

    .line 1122
    check-cast p1, Lorg/threeten/bp/Instant;

    .line 1123
    invoke-virtual {p1}, Lorg/threeten/bp/Instant;->a()J

    move-result-wide v0

    invoke-virtual {p1}, Lorg/threeten/bp/Instant;->b()I

    move-result v2

    iget-object v3, p0, Lorg/threeten/bp/ZonedDateTime;->d:Lorg/threeten/bp/ZoneId;

    invoke-static {v0, v1, v2, v3}, Lorg/threeten/bp/ZonedDateTime;->a(JILorg/threeten/bp/ZoneId;)Lorg/threeten/bp/ZonedDateTime;

    move-result-object v0

    goto :goto_0

    .line 1124
    :cond_3
    instance-of v0, p1, Lorg/threeten/bp/ZoneOffset;

    if-eqz v0, :cond_4

    .line 1125
    check-cast p1, Lorg/threeten/bp/ZoneOffset;

    invoke-direct {p0, p1}, Lorg/threeten/bp/ZonedDateTime;->a(Lorg/threeten/bp/ZoneOffset;)Lorg/threeten/bp/ZonedDateTime;

    move-result-object v0

    goto :goto_0

    .line 1127
    :cond_4
    invoke-interface {p1, p0}, Lorg/threeten/bp/temporal/TemporalAdjuster;->a(Lorg/threeten/bp/temporal/Temporal;)Lorg/threeten/bp/temporal/Temporal;

    move-result-object v0

    check-cast v0, Lorg/threeten/bp/ZonedDateTime;

    goto :goto_0
.end method

.method public a(Lorg/threeten/bp/temporal/TemporalAmount;)Lorg/threeten/bp/ZonedDateTime;
    .locals 1

    .prologue
    .line 1430
    invoke-interface {p1, p0}, Lorg/threeten/bp/temporal/TemporalAmount;->a(Lorg/threeten/bp/temporal/Temporal;)Lorg/threeten/bp/temporal/Temporal;

    move-result-object v0

    check-cast v0, Lorg/threeten/bp/ZonedDateTime;

    return-object v0
.end method

.method public a(Lorg/threeten/bp/temporal/TemporalField;J)Lorg/threeten/bp/ZonedDateTime;
    .locals 4

    .prologue
    .line 1184
    instance-of v0, p1, Lorg/threeten/bp/temporal/ChronoField;

    if-eqz v0, :cond_0

    move-object v0, p1

    .line 1185
    check-cast v0, Lorg/threeten/bp/temporal/ChronoField;

    .line 1186
    sget-object v1, Lorg/threeten/bp/ZonedDateTime$2;->a:[I

    invoke-virtual {v0}, Lorg/threeten/bp/temporal/ChronoField;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 1193
    iget-object v0, p0, Lorg/threeten/bp/ZonedDateTime;->b:Lorg/threeten/bp/LocalDateTime;

    invoke-virtual {v0, p1, p2, p3}, Lorg/threeten/bp/LocalDateTime;->a(Lorg/threeten/bp/temporal/TemporalField;J)Lorg/threeten/bp/LocalDateTime;

    move-result-object v0

    invoke-direct {p0, v0}, Lorg/threeten/bp/ZonedDateTime;->a(Lorg/threeten/bp/LocalDateTime;)Lorg/threeten/bp/ZonedDateTime;

    move-result-object v0

    .line 1195
    :goto_0
    return-object v0

    .line 1187
    :pswitch_0
    invoke-virtual {p0}, Lorg/threeten/bp/ZonedDateTime;->c()I

    move-result v0

    iget-object v1, p0, Lorg/threeten/bp/ZonedDateTime;->d:Lorg/threeten/bp/ZoneId;

    invoke-static {p2, p3, v0, v1}, Lorg/threeten/bp/ZonedDateTime;->a(JILorg/threeten/bp/ZoneId;)Lorg/threeten/bp/ZonedDateTime;

    move-result-object v0

    goto :goto_0

    .line 1189
    :pswitch_1
    invoke-virtual {v0, p2, p3}, Lorg/threeten/bp/temporal/ChronoField;->b(J)I

    move-result v0

    invoke-static {v0}, Lorg/threeten/bp/ZoneOffset;->a(I)Lorg/threeten/bp/ZoneOffset;

    move-result-object v0

    .line 1190
    invoke-direct {p0, v0}, Lorg/threeten/bp/ZonedDateTime;->a(Lorg/threeten/bp/ZoneOffset;)Lorg/threeten/bp/ZonedDateTime;

    move-result-object v0

    goto :goto_0

    .line 1195
    :cond_0
    invoke-interface {p1, p0, p2, p3}, Lorg/threeten/bp/temporal/TemporalField;->a(Lorg/threeten/bp/temporal/Temporal;J)Lorg/threeten/bp/temporal/Temporal;

    move-result-object v0

    check-cast v0, Lorg/threeten/bp/ZonedDateTime;

    goto :goto_0

    .line 1186
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method a(Ljava/io/DataOutput;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2114
    iget-object v0, p0, Lorg/threeten/bp/ZonedDateTime;->b:Lorg/threeten/bp/LocalDateTime;

    invoke-virtual {v0, p1}, Lorg/threeten/bp/LocalDateTime;->a(Ljava/io/DataOutput;)V

    .line 2115
    iget-object v0, p0, Lorg/threeten/bp/ZonedDateTime;->c:Lorg/threeten/bp/ZoneOffset;

    invoke-virtual {v0, p1}, Lorg/threeten/bp/ZoneOffset;->b(Ljava/io/DataOutput;)V

    .line 2116
    iget-object v0, p0, Lorg/threeten/bp/ZonedDateTime;->d:Lorg/threeten/bp/ZoneId;

    invoke-virtual {v0, p1}, Lorg/threeten/bp/ZoneId;->a(Ljava/io/DataOutput;)V

    .line 2117
    return-void
.end method

.method public a(Lorg/threeten/bp/temporal/TemporalField;)Z
    .locals 1

    .prologue
    .line 668
    instance-of v0, p1, Lorg/threeten/bp/temporal/ChronoField;

    if-nez v0, :cond_0

    if-eqz p1, :cond_1

    invoke-interface {p1, p0}, Lorg/threeten/bp/temporal/TemporalField;->a(Lorg/threeten/bp/temporal/TemporalAccessor;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()Lorg/threeten/bp/ZoneId;
    .locals 1

    .prologue
    .line 868
    iget-object v0, p0, Lorg/threeten/bp/ZonedDateTime;->d:Lorg/threeten/bp/ZoneId;

    return-object v0
.end method

.method public b(JLorg/threeten/bp/temporal/TemporalUnit;)Lorg/threeten/bp/ZonedDateTime;
    .locals 5

    .prologue
    .line 1699
    const-wide/high16 v0, -0x8000000000000000L

    cmp-long v0, p1, v0

    if-nez v0, :cond_0

    const-wide v0, 0x7fffffffffffffffL

    invoke-virtual {p0, v0, v1, p3}, Lorg/threeten/bp/ZonedDateTime;->a(JLorg/threeten/bp/temporal/TemporalUnit;)Lorg/threeten/bp/ZonedDateTime;

    move-result-object v0

    const-wide/16 v2, 0x1

    invoke-virtual {v0, v2, v3, p3}, Lorg/threeten/bp/ZonedDateTime;->a(JLorg/threeten/bp/temporal/TemporalUnit;)Lorg/threeten/bp/ZonedDateTime;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    neg-long v0, p1

    invoke-virtual {p0, v0, v1, p3}, Lorg/threeten/bp/ZonedDateTime;->a(JLorg/threeten/bp/temporal/TemporalUnit;)Lorg/threeten/bp/ZonedDateTime;

    move-result-object v0

    goto :goto_0
.end method

.method public b(Lorg/threeten/bp/ZoneId;)Lorg/threeten/bp/ZonedDateTime;
    .locals 3

    .prologue
    .line 913
    const-string v0, "zone"

    invoke-static {p1, v0}, Lorg/threeten/bp/jdk8/Jdk8Methods;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 914
    iget-object v0, p0, Lorg/threeten/bp/ZonedDateTime;->d:Lorg/threeten/bp/ZoneId;

    invoke-virtual {v0, p1}, Lorg/threeten/bp/ZoneId;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return-object p0

    :cond_0
    iget-object v0, p0, Lorg/threeten/bp/ZonedDateTime;->b:Lorg/threeten/bp/LocalDateTime;

    iget-object v1, p0, Lorg/threeten/bp/ZonedDateTime;->c:Lorg/threeten/bp/ZoneOffset;

    invoke-virtual {v0, v1}, Lorg/threeten/bp/LocalDateTime;->c(Lorg/threeten/bp/ZoneOffset;)J

    move-result-wide v0

    iget-object v2, p0, Lorg/threeten/bp/ZonedDateTime;->b:Lorg/threeten/bp/LocalDateTime;

    invoke-virtual {v2}, Lorg/threeten/bp/LocalDateTime;->i()I

    move-result v2

    invoke-static {v0, v1, v2, p1}, Lorg/threeten/bp/ZonedDateTime;->a(JILorg/threeten/bp/ZoneId;)Lorg/threeten/bp/ZonedDateTime;

    move-result-object p0

    goto :goto_0
.end method

.method public synthetic b(Lorg/threeten/bp/temporal/TemporalAmount;)Lorg/threeten/bp/chrono/ChronoZonedDateTime;
    .locals 1

    .prologue
    .line 122
    invoke-virtual {p0, p1}, Lorg/threeten/bp/ZonedDateTime;->a(Lorg/threeten/bp/temporal/TemporalAmount;)Lorg/threeten/bp/ZonedDateTime;

    move-result-object v0

    return-object v0
.end method

.method public synthetic b(Lorg/threeten/bp/temporal/TemporalAdjuster;)Lorg/threeten/bp/temporal/Temporal;
    .locals 1

    .prologue
    .line 122
    invoke-virtual {p0, p1}, Lorg/threeten/bp/ZonedDateTime;->a(Lorg/threeten/bp/temporal/TemporalAdjuster;)Lorg/threeten/bp/ZonedDateTime;

    move-result-object v0

    return-object v0
.end method

.method public synthetic b(Lorg/threeten/bp/temporal/TemporalField;J)Lorg/threeten/bp/temporal/Temporal;
    .locals 2

    .prologue
    .line 122
    invoke-virtual {p0, p1, p2, p3}, Lorg/threeten/bp/ZonedDateTime;->a(Lorg/threeten/bp/temporal/TemporalField;J)Lorg/threeten/bp/ZonedDateTime;

    move-result-object v0

    return-object v0
.end method

.method public b(Lorg/threeten/bp/temporal/TemporalField;)Lorg/threeten/bp/temporal/ValueRange;
    .locals 1

    .prologue
    .line 703
    instance-of v0, p1, Lorg/threeten/bp/temporal/ChronoField;

    if-eqz v0, :cond_2

    .line 704
    sget-object v0, Lorg/threeten/bp/temporal/ChronoField;->C:Lorg/threeten/bp/temporal/ChronoField;

    if-eq p1, v0, :cond_0

    sget-object v0, Lorg/threeten/bp/temporal/ChronoField;->D:Lorg/threeten/bp/temporal/ChronoField;

    if-ne p1, v0, :cond_1

    .line 705
    :cond_0
    invoke-interface {p1}, Lorg/threeten/bp/temporal/TemporalField;->a()Lorg/threeten/bp/temporal/ValueRange;

    move-result-object v0

    .line 709
    :goto_0
    return-object v0

    .line 707
    :cond_1
    iget-object v0, p0, Lorg/threeten/bp/ZonedDateTime;->b:Lorg/threeten/bp/LocalDateTime;

    invoke-virtual {v0, p1}, Lorg/threeten/bp/LocalDateTime;->b(Lorg/threeten/bp/temporal/TemporalField;)Lorg/threeten/bp/temporal/ValueRange;

    move-result-object v0

    goto :goto_0

    .line 709
    :cond_2
    invoke-interface {p1, p0}, Lorg/threeten/bp/temporal/TemporalField;->b(Lorg/threeten/bp/temporal/TemporalAccessor;)Lorg/threeten/bp/temporal/ValueRange;

    move-result-object v0

    goto :goto_0
.end method

.method public c()I
    .locals 1

    .prologue
    .line 1056
    iget-object v0, p0, Lorg/threeten/bp/ZonedDateTime;->b:Lorg/threeten/bp/LocalDateTime;

    invoke-virtual {v0}, Lorg/threeten/bp/LocalDateTime;->i()I

    move-result v0

    return v0
.end method

.method public c(Lorg/threeten/bp/temporal/TemporalField;)I
    .locals 3

    .prologue
    .line 739
    instance-of v0, p1, Lorg/threeten/bp/temporal/ChronoField;

    if-eqz v0, :cond_0

    .line 740
    sget-object v1, Lorg/threeten/bp/ZonedDateTime$2;->a:[I

    move-object v0, p1

    check-cast v0, Lorg/threeten/bp/temporal/ChronoField;

    invoke-virtual {v0}, Lorg/threeten/bp/temporal/ChronoField;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 744
    iget-object v0, p0, Lorg/threeten/bp/ZonedDateTime;->b:Lorg/threeten/bp/LocalDateTime;

    invoke-virtual {v0, p1}, Lorg/threeten/bp/LocalDateTime;->c(Lorg/threeten/bp/temporal/TemporalField;)I

    move-result v0

    .line 746
    :goto_0
    return v0

    .line 741
    :pswitch_0
    new-instance v0, Lorg/threeten/bp/DateTimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Field too large for an int: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/threeten/bp/DateTimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 742
    :pswitch_1
    invoke-virtual {p0}, Lorg/threeten/bp/ZonedDateTime;->a()Lorg/threeten/bp/ZoneOffset;

    move-result-object v0

    invoke-virtual {v0}, Lorg/threeten/bp/ZoneOffset;->f()I

    move-result v0

    goto :goto_0

    .line 746
    :cond_0
    invoke-super {p0, p1}, Lorg/threeten/bp/chrono/ChronoZonedDateTime;->c(Lorg/threeten/bp/temporal/TemporalField;)I

    move-result v0

    goto :goto_0

    .line 740
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public synthetic c(Lorg/threeten/bp/ZoneId;)Lorg/threeten/bp/chrono/ChronoZonedDateTime;
    .locals 1

    .prologue
    .line 122
    invoke-virtual {p0, p1}, Lorg/threeten/bp/ZonedDateTime;->b(Lorg/threeten/bp/ZoneId;)Lorg/threeten/bp/ZonedDateTime;

    move-result-object v0

    return-object v0
.end method

.method public synthetic c(Lorg/threeten/bp/temporal/TemporalAdjuster;)Lorg/threeten/bp/chrono/ChronoZonedDateTime;
    .locals 1

    .prologue
    .line 122
    invoke-virtual {p0, p1}, Lorg/threeten/bp/ZonedDateTime;->a(Lorg/threeten/bp/temporal/TemporalAdjuster;)Lorg/threeten/bp/ZonedDateTime;

    move-result-object v0

    return-object v0
.end method

.method public synthetic c(Lorg/threeten/bp/temporal/TemporalField;J)Lorg/threeten/bp/chrono/ChronoZonedDateTime;
    .locals 2

    .prologue
    .line 122
    invoke-virtual {p0, p1, p2, p3}, Lorg/threeten/bp/ZonedDateTime;->a(Lorg/threeten/bp/temporal/TemporalField;J)Lorg/threeten/bp/ZonedDateTime;

    move-result-object v0

    return-object v0
.end method

.method public synthetic c(JLorg/threeten/bp/temporal/TemporalUnit;)Lorg/threeten/bp/temporal/Temporal;
    .locals 1

    .prologue
    .line 122
    invoke-virtual {p0, p1, p2, p3}, Lorg/threeten/bp/ZonedDateTime;->b(JLorg/threeten/bp/temporal/TemporalUnit;)Lorg/threeten/bp/ZonedDateTime;

    move-result-object v0

    return-object v0
.end method

.method public synthetic c(Lorg/threeten/bp/temporal/TemporalAmount;)Lorg/threeten/bp/temporal/Temporal;
    .locals 1

    .prologue
    .line 122
    invoke-virtual {p0, p1}, Lorg/threeten/bp/ZonedDateTime;->a(Lorg/threeten/bp/temporal/TemporalAmount;)Lorg/threeten/bp/ZonedDateTime;

    move-result-object v0

    return-object v0
.end method

.method public d(Lorg/threeten/bp/temporal/TemporalField;)J
    .locals 2

    .prologue
    .line 773
    instance-of v0, p1, Lorg/threeten/bp/temporal/ChronoField;

    if-eqz v0, :cond_0

    .line 774
    sget-object v1, Lorg/threeten/bp/ZonedDateTime$2;->a:[I

    move-object v0, p1

    check-cast v0, Lorg/threeten/bp/temporal/ChronoField;

    invoke-virtual {v0}, Lorg/threeten/bp/temporal/ChronoField;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 778
    iget-object v0, p0, Lorg/threeten/bp/ZonedDateTime;->b:Lorg/threeten/bp/LocalDateTime;

    invoke-virtual {v0, p1}, Lorg/threeten/bp/LocalDateTime;->d(Lorg/threeten/bp/temporal/TemporalField;)J

    move-result-wide v0

    .line 780
    :goto_0
    return-wide v0

    .line 775
    :pswitch_0
    invoke-virtual {p0}, Lorg/threeten/bp/ZonedDateTime;->k()J

    move-result-wide v0

    goto :goto_0

    .line 776
    :pswitch_1
    invoke-virtual {p0}, Lorg/threeten/bp/ZonedDateTime;->a()Lorg/threeten/bp/ZoneOffset;

    move-result-object v0

    invoke-virtual {v0}, Lorg/threeten/bp/ZoneOffset;->f()I

    move-result v0

    int-to-long v0, v0

    goto :goto_0

    .line 780
    :cond_0
    invoke-interface {p1, p0}, Lorg/threeten/bp/temporal/TemporalField;->c(Lorg/threeten/bp/temporal/TemporalAccessor;)J

    move-result-wide v0

    goto :goto_0

    .line 774
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public d()Lorg/threeten/bp/LocalDateTime;
    .locals 1

    .prologue
    .line 1989
    iget-object v0, p0, Lorg/threeten/bp/ZonedDateTime;->b:Lorg/threeten/bp/LocalDateTime;

    return-object v0
.end method

.method public synthetic d(Lorg/threeten/bp/ZoneId;)Lorg/threeten/bp/chrono/ChronoZonedDateTime;
    .locals 1

    .prologue
    .line 122
    invoke-virtual {p0, p1}, Lorg/threeten/bp/ZonedDateTime;->a(Lorg/threeten/bp/ZoneId;)Lorg/threeten/bp/ZonedDateTime;

    move-result-object v0

    return-object v0
.end method

.method public synthetic d(JLorg/threeten/bp/temporal/TemporalUnit;)Lorg/threeten/bp/temporal/Temporal;
    .locals 1

    .prologue
    .line 122
    invoke-virtual {p0, p1, p2, p3}, Lorg/threeten/bp/ZonedDateTime;->a(JLorg/threeten/bp/temporal/TemporalUnit;)Lorg/threeten/bp/ZonedDateTime;

    move-result-object v0

    return-object v0
.end method

.method public e()Lorg/threeten/bp/LocalDate;
    .locals 1

    .prologue
    .line 2002
    iget-object v0, p0, Lorg/threeten/bp/ZonedDateTime;->b:Lorg/threeten/bp/LocalDateTime;

    invoke-virtual {v0}, Lorg/threeten/bp/LocalDateTime;->j()Lorg/threeten/bp/LocalDate;

    move-result-object v0

    return-object v0
.end method

.method public synthetic e(JLorg/threeten/bp/temporal/TemporalUnit;)Lorg/threeten/bp/chrono/ChronoZonedDateTime;
    .locals 1

    .prologue
    .line 122
    invoke-virtual {p0, p1, p2, p3}, Lorg/threeten/bp/ZonedDateTime;->b(JLorg/threeten/bp/temporal/TemporalUnit;)Lorg/threeten/bp/ZonedDateTime;

    move-result-object v0

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2042
    if-ne p0, p1, :cond_1

    .line 2051
    :cond_0
    :goto_0
    return v0

    .line 2045
    :cond_1
    instance-of v2, p1, Lorg/threeten/bp/ZonedDateTime;

    if-eqz v2, :cond_3

    .line 2046
    check-cast p1, Lorg/threeten/bp/ZonedDateTime;

    .line 2047
    iget-object v2, p0, Lorg/threeten/bp/ZonedDateTime;->b:Lorg/threeten/bp/LocalDateTime;

    iget-object v3, p1, Lorg/threeten/bp/ZonedDateTime;->b:Lorg/threeten/bp/LocalDateTime;

    invoke-virtual {v2, v3}, Lorg/threeten/bp/LocalDateTime;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lorg/threeten/bp/ZonedDateTime;->c:Lorg/threeten/bp/ZoneOffset;

    iget-object v3, p1, Lorg/threeten/bp/ZonedDateTime;->c:Lorg/threeten/bp/ZoneOffset;

    invoke-virtual {v2, v3}, Lorg/threeten/bp/ZoneOffset;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lorg/threeten/bp/ZonedDateTime;->d:Lorg/threeten/bp/ZoneId;

    iget-object v3, p1, Lorg/threeten/bp/ZonedDateTime;->d:Lorg/threeten/bp/ZoneId;

    invoke-virtual {v2, v3}, Lorg/threeten/bp/ZoneId;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_2
    move v0, v1

    goto :goto_0

    :cond_3
    move v0, v1

    .line 2051
    goto :goto_0
.end method

.method public f()Lorg/threeten/bp/LocalTime;
    .locals 1

    .prologue
    .line 2015
    iget-object v0, p0, Lorg/threeten/bp/ZonedDateTime;->b:Lorg/threeten/bp/LocalDateTime;

    invoke-virtual {v0}, Lorg/threeten/bp/LocalDateTime;->k()Lorg/threeten/bp/LocalTime;

    move-result-object v0

    return-object v0
.end method

.method public synthetic f(JLorg/threeten/bp/temporal/TemporalUnit;)Lorg/threeten/bp/chrono/ChronoZonedDateTime;
    .locals 1

    .prologue
    .line 122
    invoke-virtual {p0, p1, p2, p3}, Lorg/threeten/bp/ZonedDateTime;->a(JLorg/threeten/bp/temporal/TemporalUnit;)Lorg/threeten/bp/ZonedDateTime;

    move-result-object v0

    return-object v0
.end method

.method public g()Lorg/threeten/bp/OffsetDateTime;
    .locals 2

    .prologue
    .line 2027
    iget-object v0, p0, Lorg/threeten/bp/ZonedDateTime;->b:Lorg/threeten/bp/LocalDateTime;

    iget-object v1, p0, Lorg/threeten/bp/ZonedDateTime;->c:Lorg/threeten/bp/ZoneOffset;

    invoke-static {v0, v1}, Lorg/threeten/bp/OffsetDateTime;->a(Lorg/threeten/bp/LocalDateTime;Lorg/threeten/bp/ZoneOffset;)Lorg/threeten/bp/OffsetDateTime;

    move-result-object v0

    return-object v0
.end method

.method public synthetic h()Lorg/threeten/bp/chrono/ChronoLocalDateTime;
    .locals 1

    .prologue
    .line 122
    invoke-virtual {p0}, Lorg/threeten/bp/ZonedDateTime;->d()Lorg/threeten/bp/LocalDateTime;

    move-result-object v0

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 2061
    iget-object v0, p0, Lorg/threeten/bp/ZonedDateTime;->b:Lorg/threeten/bp/LocalDateTime;

    invoke-virtual {v0}, Lorg/threeten/bp/LocalDateTime;->hashCode()I

    move-result v0

    iget-object v1, p0, Lorg/threeten/bp/ZonedDateTime;->c:Lorg/threeten/bp/ZoneOffset;

    invoke-virtual {v1}, Lorg/threeten/bp/ZoneOffset;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    iget-object v1, p0, Lorg/threeten/bp/ZonedDateTime;->d:Lorg/threeten/bp/ZoneId;

    invoke-virtual {v1}, Lorg/threeten/bp/ZoneId;->hashCode()I

    move-result v1

    const/4 v2, 0x3

    invoke-static {v1, v2}, Ljava/lang/Integer;->rotateLeft(II)I

    move-result v1

    xor-int/2addr v0, v1

    return v0
.end method

.method public synthetic i()Lorg/threeten/bp/chrono/ChronoLocalDate;
    .locals 1

    .prologue
    .line 122
    invoke-virtual {p0}, Lorg/threeten/bp/ZonedDateTime;->e()Lorg/threeten/bp/LocalDate;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 2077
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lorg/threeten/bp/ZonedDateTime;->b:Lorg/threeten/bp/LocalDateTime;

    invoke-virtual {v1}, Lorg/threeten/bp/LocalDateTime;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lorg/threeten/bp/ZonedDateTime;->c:Lorg/threeten/bp/ZoneOffset;

    invoke-virtual {v1}, Lorg/threeten/bp/ZoneOffset;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 2078
    iget-object v1, p0, Lorg/threeten/bp/ZonedDateTime;->c:Lorg/threeten/bp/ZoneOffset;

    iget-object v2, p0, Lorg/threeten/bp/ZonedDateTime;->d:Lorg/threeten/bp/ZoneId;

    if-eq v1, v2, :cond_0

    .line 2079
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x5b

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lorg/threeten/bp/ZonedDateTime;->d:Lorg/threeten/bp/ZoneId;

    invoke-virtual {v1}, Lorg/threeten/bp/ZoneId;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x5d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 2081
    :cond_0
    return-object v0
.end method
