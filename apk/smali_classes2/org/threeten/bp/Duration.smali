.class public final Lorg/threeten/bp/Duration;
.super Ljava/lang/Object;
.source "Duration.java"

# interfaces
.implements Ljava/io/Serializable;
.implements Ljava/lang/Comparable;
.implements Lorg/threeten/bp/temporal/TemporalAmount;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/io/Serializable;",
        "Ljava/lang/Comparable",
        "<",
        "Lorg/threeten/bp/Duration;",
        ">;",
        "Lorg/threeten/bp/temporal/TemporalAmount;"
    }
.end annotation


# static fields
.field public static final a:Lorg/threeten/bp/Duration;

.field private static final b:Ljava/math/BigInteger;

.field private static final c:Ljava/util/regex/Pattern;


# instance fields
.field private final d:J

.field private final e:I


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 99
    new-instance v0, Lorg/threeten/bp/Duration;

    const-wide/16 v2, 0x0

    const/4 v1, 0x0

    invoke-direct {v0, v2, v3, v1}, Lorg/threeten/bp/Duration;-><init>(JI)V

    sput-object v0, Lorg/threeten/bp/Duration;->a:Lorg/threeten/bp/Duration;

    .line 115
    const-wide/32 v0, 0x3b9aca00

    invoke-static {v0, v1}, Ljava/math/BigInteger;->valueOf(J)Ljava/math/BigInteger;

    move-result-object v0

    sput-object v0, Lorg/threeten/bp/Duration;->b:Ljava/math/BigInteger;

    .line 119
    const-string v0, "([-+]?)P(?:([-+]?[0-9]+)D)?(T(?:([-+]?[0-9]+)H)?(?:([-+]?[0-9]+)M)?(?:([-+]?[0-9]+)(?:[.,]([0-9]{0,9}))?S)?)?"

    const/4 v1, 0x2

    invoke-static {v0, v1}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;I)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lorg/threeten/bp/Duration;->c:Ljava/util/regex/Pattern;

    return-void
.end method

.method private constructor <init>(JI)V
    .locals 1

    .prologue
    .line 487
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 488
    iput-wide p1, p0, Lorg/threeten/bp/Duration;->d:J

    .line 489
    iput p3, p0, Lorg/threeten/bp/Duration;->e:I

    .line 490
    return-void
.end method

.method public static a(J)Lorg/threeten/bp/Duration;
    .locals 2

    .prologue
    .line 190
    const/4 v0, 0x0

    invoke-static {p0, p1, v0}, Lorg/threeten/bp/Duration;->a(JI)Lorg/threeten/bp/Duration;

    move-result-object v0

    return-object v0
.end method

.method private static a(JI)Lorg/threeten/bp/Duration;
    .locals 4

    .prologue
    .line 474
    int-to-long v0, p2

    or-long/2addr v0, p0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 475
    sget-object v0, Lorg/threeten/bp/Duration;->a:Lorg/threeten/bp/Duration;

    .line 477
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lorg/threeten/bp/Duration;

    invoke-direct {v0, p0, p1, p2}, Lorg/threeten/bp/Duration;-><init>(JI)V

    goto :goto_0
.end method

.method public static a(JJ)Lorg/threeten/bp/Duration;
    .locals 4

    .prologue
    .line 213
    const-wide/32 v0, 0x3b9aca00

    invoke-static {p2, p3, v0, v1}, Lorg/threeten/bp/jdk8/Jdk8Methods;->e(JJ)J

    move-result-wide v0

    invoke-static {p0, p1, v0, v1}, Lorg/threeten/bp/jdk8/Jdk8Methods;->b(JJ)J

    move-result-wide v0

    .line 214
    const v2, 0x3b9aca00

    invoke-static {p2, p3, v2}, Lorg/threeten/bp/jdk8/Jdk8Methods;->b(JI)I

    move-result v2

    .line 215
    invoke-static {v0, v1, v2}, Lorg/threeten/bp/Duration;->a(JI)Lorg/threeten/bp/Duration;

    move-result-object v0

    return-object v0
.end method

.method static a(Ljava/io/DataInput;)Lorg/threeten/bp/Duration;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1260
    invoke-interface {p0}, Ljava/io/DataInput;->readLong()J

    move-result-wide v0

    .line 1261
    invoke-interface {p0}, Ljava/io/DataInput;->readInt()I

    move-result v2

    .line 1262
    int-to-long v2, v2

    invoke-static {v0, v1, v2, v3}, Lorg/threeten/bp/Duration;->a(JJ)Lorg/threeten/bp/Duration;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lorg/threeten/bp/temporal/Temporal;Lorg/threeten/bp/temporal/Temporal;)Lorg/threeten/bp/Duration;
    .locals 12

    .prologue
    const-wide/32 v10, 0x3b9aca00

    const-wide/16 v4, 0x0

    .line 326
    sget-object v0, Lorg/threeten/bp/temporal/ChronoUnit;->d:Lorg/threeten/bp/temporal/ChronoUnit;

    invoke-interface {p0, p1, v0}, Lorg/threeten/bp/temporal/Temporal;->a(Lorg/threeten/bp/temporal/Temporal;Lorg/threeten/bp/temporal/TemporalUnit;)J

    move-result-wide v2

    .line 328
    sget-object v0, Lorg/threeten/bp/temporal/ChronoField;->a:Lorg/threeten/bp/temporal/ChronoField;

    invoke-interface {p0, v0}, Lorg/threeten/bp/temporal/Temporal;->a(Lorg/threeten/bp/temporal/TemporalField;)Z

    move-result v0

    if-eqz v0, :cond_3

    sget-object v0, Lorg/threeten/bp/temporal/ChronoField;->a:Lorg/threeten/bp/temporal/ChronoField;

    invoke-interface {p1, v0}, Lorg/threeten/bp/temporal/Temporal;->a(Lorg/threeten/bp/temporal/TemporalField;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 330
    :try_start_0
    sget-object v0, Lorg/threeten/bp/temporal/ChronoField;->a:Lorg/threeten/bp/temporal/ChronoField;

    invoke-interface {p0, v0}, Lorg/threeten/bp/temporal/Temporal;->d(Lorg/threeten/bp/temporal/TemporalField;)J

    move-result-wide v6

    .line 331
    sget-object v0, Lorg/threeten/bp/temporal/ChronoField;->a:Lorg/threeten/bp/temporal/ChronoField;

    invoke-interface {p1, v0}, Lorg/threeten/bp/temporal/Temporal;->d(Lorg/threeten/bp/temporal/TemporalField;)J
    :try_end_0
    .catch Lorg/threeten/bp/DateTimeException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/ArithmeticException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v0

    sub-long/2addr v0, v6

    .line 332
    cmp-long v8, v2, v4

    if-lez v8, :cond_1

    cmp-long v8, v0, v4

    if-gez v8, :cond_1

    .line 333
    add-long/2addr v0, v10

    .line 347
    :cond_0
    :goto_0
    invoke-static {v2, v3, v0, v1}, Lorg/threeten/bp/Duration;->a(JJ)Lorg/threeten/bp/Duration;

    move-result-object v0

    return-object v0

    .line 334
    :cond_1
    cmp-long v8, v2, v4

    if-gez v8, :cond_2

    cmp-long v8, v0, v4

    if-lez v8, :cond_2

    .line 335
    sub-long/2addr v0, v10

    goto :goto_0

    .line 336
    :cond_2
    cmp-long v8, v2, v4

    if-nez v8, :cond_0

    cmp-long v4, v0, v4

    if-eqz v4, :cond_0

    .line 338
    :try_start_1
    sget-object v4, Lorg/threeten/bp/temporal/ChronoField;->a:Lorg/threeten/bp/temporal/ChronoField;

    invoke-interface {p1, v4, v6, v7}, Lorg/threeten/bp/temporal/Temporal;->b(Lorg/threeten/bp/temporal/TemporalField;J)Lorg/threeten/bp/temporal/Temporal;

    move-result-object v4

    .line 339
    sget-object v5, Lorg/threeten/bp/temporal/ChronoUnit;->d:Lorg/threeten/bp/temporal/ChronoUnit;

    invoke-interface {p0, v4, v5}, Lorg/threeten/bp/temporal/Temporal;->a(Lorg/threeten/bp/temporal/Temporal;Lorg/threeten/bp/temporal/TemporalUnit;)J
    :try_end_1
    .catch Lorg/threeten/bp/DateTimeException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/lang/ArithmeticException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-wide v2

    goto :goto_0

    .line 343
    :catch_0
    move-exception v0

    move-wide v0, v4

    goto :goto_0

    :catch_1
    move-exception v4

    goto :goto_0

    .line 341
    :catch_2
    move-exception v0

    move-wide v0, v4

    goto :goto_0

    :catch_3
    move-exception v4

    goto :goto_0

    :cond_3
    move-wide v0, v4

    goto :goto_0
.end method

.method public static b(J)Lorg/threeten/bp/Duration;
    .locals 6

    .prologue
    const-wide/32 v0, 0x3b9aca00

    .line 246
    div-long v2, p0, v0

    .line 247
    rem-long v0, p0, v0

    long-to-int v0, v0

    .line 248
    if-gez v0, :cond_0

    .line 249
    const v1, 0x3b9aca00

    add-int/2addr v0, v1

    .line 250
    const-wide/16 v4, 0x1

    sub-long/2addr v2, v4

    .line 252
    :cond_0
    invoke-static {v2, v3, v0}, Lorg/threeten/bp/Duration;->a(JI)Lorg/threeten/bp/Duration;

    move-result-object v0

    return-object v0
.end method

.method private readResolve()Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/ObjectStreamException;
        }
    .end annotation

    .prologue
    .line 1251
    new-instance v0, Ljava/io/InvalidObjectException;

    const-string v1, "Deserialization via serialization delegate"

    invoke-direct {v0, v1}, Ljava/io/InvalidObjectException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private writeReplace()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 1242
    new-instance v0, Lorg/threeten/bp/Ser;

    const/4 v1, 0x1

    invoke-direct {v0, v1, p0}, Lorg/threeten/bp/Ser;-><init>(BLjava/lang/Object;)V

    return-object v0
.end method


# virtual methods
.method public a(Lorg/threeten/bp/Duration;)I
    .locals 4

    .prologue
    .line 1134
    iget-wide v0, p0, Lorg/threeten/bp/Duration;->d:J

    iget-wide v2, p1, Lorg/threeten/bp/Duration;->d:J

    invoke-static {v0, v1, v2, v3}, Lorg/threeten/bp/jdk8/Jdk8Methods;->a(JJ)I

    move-result v0

    .line 1135
    if-eqz v0, :cond_0

    .line 1138
    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lorg/threeten/bp/Duration;->e:I

    iget v1, p1, Lorg/threeten/bp/Duration;->e:I

    sub-int/2addr v0, v1

    goto :goto_0
.end method

.method public a()J
    .locals 2

    .prologue
    .line 552
    iget-wide v0, p0, Lorg/threeten/bp/Duration;->d:J

    return-wide v0
.end method

.method public a(Lorg/threeten/bp/temporal/Temporal;)Lorg/threeten/bp/temporal/Temporal;
    .locals 4

    .prologue
    .line 1000
    iget-wide v0, p0, Lorg/threeten/bp/Duration;->d:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    .line 1001
    iget-wide v0, p0, Lorg/threeten/bp/Duration;->d:J

    sget-object v2, Lorg/threeten/bp/temporal/ChronoUnit;->d:Lorg/threeten/bp/temporal/ChronoUnit;

    invoke-interface {p1, v0, v1, v2}, Lorg/threeten/bp/temporal/Temporal;->d(JLorg/threeten/bp/temporal/TemporalUnit;)Lorg/threeten/bp/temporal/Temporal;

    move-result-object p1

    .line 1003
    :cond_0
    iget v0, p0, Lorg/threeten/bp/Duration;->e:I

    if-eqz v0, :cond_1

    .line 1004
    iget v0, p0, Lorg/threeten/bp/Duration;->e:I

    int-to-long v0, v0

    sget-object v2, Lorg/threeten/bp/temporal/ChronoUnit;->a:Lorg/threeten/bp/temporal/ChronoUnit;

    invoke-interface {p1, v0, v1, v2}, Lorg/threeten/bp/temporal/Temporal;->d(JLorg/threeten/bp/temporal/TemporalUnit;)Lorg/threeten/bp/temporal/Temporal;

    move-result-object p1

    .line 1006
    :cond_1
    return-object p1
.end method

.method a(Ljava/io/DataOutput;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1255
    iget-wide v0, p0, Lorg/threeten/bp/Duration;->d:J

    invoke-interface {p1, v0, v1}, Ljava/io/DataOutput;->writeLong(J)V

    .line 1256
    iget v0, p0, Lorg/threeten/bp/Duration;->e:I

    invoke-interface {p1, v0}, Ljava/io/DataOutput;->writeInt(I)V

    .line 1257
    return-void
.end method

.method public b()J
    .locals 4

    .prologue
    .line 1085
    iget-wide v0, p0, Lorg/threeten/bp/Duration;->d:J

    const-wide/16 v2, 0x3c

    div-long/2addr v0, v2

    return-wide v0
.end method

.method public synthetic compareTo(Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 93
    check-cast p1, Lorg/threeten/bp/Duration;

    invoke-virtual {p0, p1}, Lorg/threeten/bp/Duration;->a(Lorg/threeten/bp/Duration;)I

    move-result v0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1152
    if-ne p0, p1, :cond_1

    .line 1160
    :cond_0
    :goto_0
    return v0

    .line 1155
    :cond_1
    instance-of v2, p1, Lorg/threeten/bp/Duration;

    if-eqz v2, :cond_3

    .line 1156
    check-cast p1, Lorg/threeten/bp/Duration;

    .line 1157
    iget-wide v2, p0, Lorg/threeten/bp/Duration;->d:J

    iget-wide v4, p1, Lorg/threeten/bp/Duration;->d:J

    cmp-long v2, v2, v4

    if-nez v2, :cond_2

    iget v2, p0, Lorg/threeten/bp/Duration;->e:I

    iget v3, p1, Lorg/threeten/bp/Duration;->e:I

    if-eq v2, v3, :cond_0

    :cond_2
    move v0, v1

    goto :goto_0

    :cond_3
    move v0, v1

    .line 1160
    goto :goto_0
.end method

.method public hashCode()I
    .locals 5

    .prologue
    .line 1170
    iget-wide v0, p0, Lorg/threeten/bp/Duration;->d:J

    iget-wide v2, p0, Lorg/threeten/bp/Duration;->d:J

    const/16 v4, 0x20

    ushr-long/2addr v2, v4

    xor-long/2addr v0, v2

    long-to-int v0, v0

    iget v1, p0, Lorg/threeten/bp/Duration;->e:I

    mul-int/lit8 v1, v1, 0x33

    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 8

    .prologue
    const-wide/16 v4, 0xe10

    const-wide/16 v6, 0x3c

    .line 1198
    sget-object v0, Lorg/threeten/bp/Duration;->a:Lorg/threeten/bp/Duration;

    if-ne p0, v0, :cond_0

    .line 1199
    const-string v0, "PT0S"

    .line 1237
    :goto_0
    return-object v0

    .line 1201
    :cond_0
    iget-wide v0, p0, Lorg/threeten/bp/Duration;->d:J

    div-long/2addr v0, v4

    .line 1202
    iget-wide v2, p0, Lorg/threeten/bp/Duration;->d:J

    rem-long/2addr v2, v4

    div-long/2addr v2, v6

    long-to-int v2, v2

    .line 1203
    iget-wide v4, p0, Lorg/threeten/bp/Duration;->d:J

    rem-long/2addr v4, v6

    long-to-int v3, v4

    .line 1204
    new-instance v4, Ljava/lang/StringBuilder;

    const/16 v5, 0x18

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 1205
    const-string v5, "PT"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1206
    const-wide/16 v6, 0x0

    cmp-long v5, v0, v6

    if-eqz v5, :cond_1

    .line 1207
    invoke-virtual {v4, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x48

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 1209
    :cond_1
    if-eqz v2, :cond_2

    .line 1210
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x4d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 1212
    :cond_2
    if-nez v3, :cond_3

    iget v0, p0, Lorg/threeten/bp/Duration;->e:I

    if-nez v0, :cond_3

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    const/4 v1, 0x2

    if-le v0, v1, :cond_3

    .line 1213
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1215
    :cond_3
    if-gez v3, :cond_5

    iget v0, p0, Lorg/threeten/bp/Duration;->e:I

    if-lez v0, :cond_5

    .line 1216
    const/4 v0, -0x1

    if-ne v3, v0, :cond_4

    .line 1217
    const-string v0, "-0"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1224
    :goto_1
    iget v0, p0, Lorg/threeten/bp/Duration;->e:I

    if-lez v0, :cond_8

    .line 1225
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    .line 1226
    if-gez v3, :cond_6

    .line 1227
    const v1, 0x77359400

    iget v2, p0, Lorg/threeten/bp/Duration;->e:I

    sub-int/2addr v1, v2

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 1231
    :goto_2
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->charAt(I)C

    move-result v1

    const/16 v2, 0x30

    if-ne v1, v2, :cond_7

    .line 1232
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->setLength(I)V

    goto :goto_2

    .line 1219
    :cond_4
    add-int/lit8 v0, v3, 0x1

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 1222
    :cond_5
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 1229
    :cond_6
    iget v1, p0, Lorg/threeten/bp/Duration;->e:I

    const v2, 0x3b9aca00

    add-int/2addr v1, v2

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    goto :goto_2

    .line 1234
    :cond_7
    const/16 v1, 0x2e

    invoke-virtual {v4, v0, v1}, Ljava/lang/StringBuilder;->setCharAt(IC)V

    .line 1236
    :cond_8
    const/16 v0, 0x53

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 1237
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0
.end method
