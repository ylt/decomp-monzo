.class public final Lorg/threeten/bp/chrono/HijrahDate;
.super Lorg/threeten/bp/chrono/ChronoDateImpl;
.source "HijrahDate.java"

# interfaces
.implements Ljava/io/Serializable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/threeten/bp/chrono/HijrahDate$1;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lorg/threeten/bp/chrono/ChronoDateImpl",
        "<",
        "Lorg/threeten/bp/chrono/HijrahDate;",
        ">;",
        "Ljava/io/Serializable;"
    }
.end annotation


# static fields
.field private static final a:[I

.field private static final b:[I

.field private static final c:[I

.field private static final d:[I

.field private static final e:[I

.field private static final f:[I

.field private static final g:[I

.field private static final h:[I

.field private static final i:C

.field private static final j:Ljava/lang/String;

.field private static final k:Ljava/lang/String;

.field private static final l:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "[",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private static final m:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "[",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private static final n:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "[",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private static final o:[Ljava/lang/Long;

.field private static final p:[Ljava/lang/Integer;

.field private static final q:[Ljava/lang/Integer;

.field private static final r:[Ljava/lang/Integer;

.field private static final s:[Ljava/lang/Integer;

.field private static final t:[Ljava/lang/Integer;

.field private static final u:[Ljava/lang/Integer;

.field private static final v:[Ljava/lang/Integer;

.field private static final w:[Ljava/lang/Integer;


# instance fields
.field private final transient A:I

.field private final transient B:I

.field private final transient C:Lorg/threeten/bp/DayOfWeek;

.field private final D:J

.field private final transient E:Z

.field private final transient x:Lorg/threeten/bp/chrono/HijrahEra;

.field private final transient y:I

.field private final transient z:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v3, 0x7

    const/16 v2, 0xc

    const/4 v1, 0x0

    .line 136
    new-array v0, v2, [I

    fill-array-data v0, :array_0

    sput-object v0, Lorg/threeten/bp/chrono/HijrahDate;->a:[I

    .line 141
    new-array v0, v2, [I

    fill-array-data v0, :array_1

    sput-object v0, Lorg/threeten/bp/chrono/HijrahDate;->b:[I

    .line 146
    new-array v0, v2, [I

    fill-array-data v0, :array_2

    sput-object v0, Lorg/threeten/bp/chrono/HijrahDate;->c:[I

    .line 151
    new-array v0, v2, [I

    fill-array-data v0, :array_3

    sput-object v0, Lorg/threeten/bp/chrono/HijrahDate;->d:[I

    .line 168
    new-array v0, v3, [I

    fill-array-data v0, :array_4

    sput-object v0, Lorg/threeten/bp/chrono/HijrahDate;->e:[I

    .line 182
    new-array v0, v3, [I

    fill-array-data v0, :array_5

    sput-object v0, Lorg/threeten/bp/chrono/HijrahDate;->f:[I

    .line 196
    new-array v0, v3, [I

    fill-array-data v0, :array_6

    sput-object v0, Lorg/threeten/bp/chrono/HijrahDate;->g:[I

    .line 220
    const/16 v0, 0x1e

    new-array v0, v0, [I

    fill-array-data v0, :array_7

    sput-object v0, Lorg/threeten/bp/chrono/HijrahDate;->h:[I

    .line 257
    sget-char v0, Ljava/io/File;->separatorChar:C

    sput-char v0, Lorg/threeten/bp/chrono/HijrahDate;->i:C

    .line 261
    sget-object v0, Ljava/io/File;->pathSeparator:Ljava/lang/String;

    sput-object v0, Lorg/threeten/bp/chrono/HijrahDate;->j:Ljava/lang/String;

    .line 269
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "org"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-char v2, Lorg/threeten/bp/chrono/HijrahDate;->i:C

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "threeten"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-char v2, Lorg/threeten/bp/chrono/HijrahDate;->i:C

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "bp"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-char v2, Lorg/threeten/bp/chrono/HijrahDate;->i:C

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "chrono"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lorg/threeten/bp/chrono/HijrahDate;->k:Ljava/lang/String;

    .line 274
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lorg/threeten/bp/chrono/HijrahDate;->l:Ljava/util/HashMap;

    .line 279
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lorg/threeten/bp/chrono/HijrahDate;->m:Ljava/util/HashMap;

    .line 285
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lorg/threeten/bp/chrono/HijrahDate;->n:Ljava/util/HashMap;

    .line 331
    sget-object v0, Lorg/threeten/bp/chrono/HijrahDate;->a:[I

    array-length v0, v0

    new-array v0, v0, [Ljava/lang/Integer;

    sput-object v0, Lorg/threeten/bp/chrono/HijrahDate;->s:[Ljava/lang/Integer;

    move v0, v1

    .line 332
    :goto_0
    sget-object v2, Lorg/threeten/bp/chrono/HijrahDate;->a:[I

    array-length v2, v2

    if-ge v0, v2, :cond_0

    .line 333
    sget-object v2, Lorg/threeten/bp/chrono/HijrahDate;->s:[Ljava/lang/Integer;

    new-instance v3, Ljava/lang/Integer;

    sget-object v4, Lorg/threeten/bp/chrono/HijrahDate;->a:[I

    aget v4, v4, v0

    invoke-direct {v3, v4}, Ljava/lang/Integer;-><init>(I)V

    aput-object v3, v2, v0

    .line 332
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 336
    :cond_0
    sget-object v0, Lorg/threeten/bp/chrono/HijrahDate;->b:[I

    array-length v0, v0

    new-array v0, v0, [Ljava/lang/Integer;

    sput-object v0, Lorg/threeten/bp/chrono/HijrahDate;->t:[Ljava/lang/Integer;

    move v0, v1

    .line 337
    :goto_1
    sget-object v2, Lorg/threeten/bp/chrono/HijrahDate;->b:[I

    array-length v2, v2

    if-ge v0, v2, :cond_1

    .line 338
    sget-object v2, Lorg/threeten/bp/chrono/HijrahDate;->t:[Ljava/lang/Integer;

    new-instance v3, Ljava/lang/Integer;

    sget-object v4, Lorg/threeten/bp/chrono/HijrahDate;->b:[I

    aget v4, v4, v0

    invoke-direct {v3, v4}, Ljava/lang/Integer;-><init>(I)V

    aput-object v3, v2, v0

    .line 337
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 341
    :cond_1
    sget-object v0, Lorg/threeten/bp/chrono/HijrahDate;->c:[I

    array-length v0, v0

    new-array v0, v0, [Ljava/lang/Integer;

    sput-object v0, Lorg/threeten/bp/chrono/HijrahDate;->u:[Ljava/lang/Integer;

    move v0, v1

    .line 342
    :goto_2
    sget-object v2, Lorg/threeten/bp/chrono/HijrahDate;->c:[I

    array-length v2, v2

    if-ge v0, v2, :cond_2

    .line 343
    sget-object v2, Lorg/threeten/bp/chrono/HijrahDate;->u:[Ljava/lang/Integer;

    new-instance v3, Ljava/lang/Integer;

    sget-object v4, Lorg/threeten/bp/chrono/HijrahDate;->c:[I

    aget v4, v4, v0

    invoke-direct {v3, v4}, Ljava/lang/Integer;-><init>(I)V

    aput-object v3, v2, v0

    .line 342
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 346
    :cond_2
    sget-object v0, Lorg/threeten/bp/chrono/HijrahDate;->d:[I

    array-length v0, v0

    new-array v0, v0, [Ljava/lang/Integer;

    sput-object v0, Lorg/threeten/bp/chrono/HijrahDate;->v:[Ljava/lang/Integer;

    move v0, v1

    .line 347
    :goto_3
    sget-object v2, Lorg/threeten/bp/chrono/HijrahDate;->d:[I

    array-length v2, v2

    if-ge v0, v2, :cond_3

    .line 348
    sget-object v2, Lorg/threeten/bp/chrono/HijrahDate;->v:[Ljava/lang/Integer;

    new-instance v3, Ljava/lang/Integer;

    sget-object v4, Lorg/threeten/bp/chrono/HijrahDate;->d:[I

    aget v4, v4, v0

    invoke-direct {v3, v4}, Ljava/lang/Integer;-><init>(I)V

    aput-object v3, v2, v0

    .line 347
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 351
    :cond_3
    sget-object v0, Lorg/threeten/bp/chrono/HijrahDate;->h:[I

    array-length v0, v0

    new-array v0, v0, [Ljava/lang/Integer;

    sput-object v0, Lorg/threeten/bp/chrono/HijrahDate;->w:[Ljava/lang/Integer;

    move v0, v1

    .line 352
    :goto_4
    sget-object v2, Lorg/threeten/bp/chrono/HijrahDate;->h:[I

    array-length v2, v2

    if-ge v0, v2, :cond_4

    .line 353
    sget-object v2, Lorg/threeten/bp/chrono/HijrahDate;->w:[Ljava/lang/Integer;

    new-instance v3, Ljava/lang/Integer;

    sget-object v4, Lorg/threeten/bp/chrono/HijrahDate;->h:[I

    aget v4, v4, v0

    invoke-direct {v3, v4}, Ljava/lang/Integer;-><init>(I)V

    aput-object v3, v2, v0

    .line 352
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 356
    :cond_4
    const/16 v0, 0x14e

    new-array v0, v0, [Ljava/lang/Long;

    sput-object v0, Lorg/threeten/bp/chrono/HijrahDate;->o:[Ljava/lang/Long;

    move v0, v1

    .line 357
    :goto_5
    sget-object v2, Lorg/threeten/bp/chrono/HijrahDate;->o:[Ljava/lang/Long;

    array-length v2, v2

    if-ge v0, v2, :cond_5

    .line 358
    sget-object v2, Lorg/threeten/bp/chrono/HijrahDate;->o:[Ljava/lang/Long;

    new-instance v3, Ljava/lang/Long;

    mul-int/lit16 v4, v0, 0x2987

    int-to-long v4, v4

    invoke-direct {v3, v4, v5}, Ljava/lang/Long;-><init>(J)V

    aput-object v3, v2, v0

    .line 357
    add-int/lit8 v0, v0, 0x1

    goto :goto_5

    .line 361
    :cond_5
    sget-object v0, Lorg/threeten/bp/chrono/HijrahDate;->e:[I

    array-length v0, v0

    new-array v0, v0, [Ljava/lang/Integer;

    sput-object v0, Lorg/threeten/bp/chrono/HijrahDate;->p:[Ljava/lang/Integer;

    move v0, v1

    .line 362
    :goto_6
    sget-object v2, Lorg/threeten/bp/chrono/HijrahDate;->e:[I

    array-length v2, v2

    if-ge v0, v2, :cond_6

    .line 363
    sget-object v2, Lorg/threeten/bp/chrono/HijrahDate;->p:[Ljava/lang/Integer;

    new-instance v3, Ljava/lang/Integer;

    sget-object v4, Lorg/threeten/bp/chrono/HijrahDate;->e:[I

    aget v4, v4, v0

    invoke-direct {v3, v4}, Ljava/lang/Integer;-><init>(I)V

    aput-object v3, v2, v0

    .line 362
    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    .line 365
    :cond_6
    sget-object v0, Lorg/threeten/bp/chrono/HijrahDate;->f:[I

    array-length v0, v0

    new-array v0, v0, [Ljava/lang/Integer;

    sput-object v0, Lorg/threeten/bp/chrono/HijrahDate;->q:[Ljava/lang/Integer;

    move v0, v1

    .line 366
    :goto_7
    sget-object v2, Lorg/threeten/bp/chrono/HijrahDate;->f:[I

    array-length v2, v2

    if-ge v0, v2, :cond_7

    .line 367
    sget-object v2, Lorg/threeten/bp/chrono/HijrahDate;->q:[Ljava/lang/Integer;

    new-instance v3, Ljava/lang/Integer;

    sget-object v4, Lorg/threeten/bp/chrono/HijrahDate;->f:[I

    aget v4, v4, v0

    invoke-direct {v3, v4}, Ljava/lang/Integer;-><init>(I)V

    aput-object v3, v2, v0

    .line 366
    add-int/lit8 v0, v0, 0x1

    goto :goto_7

    .line 369
    :cond_7
    sget-object v0, Lorg/threeten/bp/chrono/HijrahDate;->g:[I

    array-length v0, v0

    new-array v0, v0, [Ljava/lang/Integer;

    sput-object v0, Lorg/threeten/bp/chrono/HijrahDate;->r:[Ljava/lang/Integer;

    .line 370
    :goto_8
    sget-object v0, Lorg/threeten/bp/chrono/HijrahDate;->g:[I

    array-length v0, v0

    if-ge v1, v0, :cond_8

    .line 371
    sget-object v0, Lorg/threeten/bp/chrono/HijrahDate;->r:[Ljava/lang/Integer;

    new-instance v2, Ljava/lang/Integer;

    sget-object v3, Lorg/threeten/bp/chrono/HijrahDate;->g:[I

    aget v3, v3, v1

    invoke-direct {v2, v3}, Ljava/lang/Integer;-><init>(I)V

    aput-object v2, v0, v1

    .line 370
    add-int/lit8 v1, v1, 0x1

    goto :goto_8

    .line 374
    :cond_8
    :try_start_0
    invoke-static {}, Lorg/threeten/bp/chrono/HijrahDate;->g()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_0} :catch_1

    .line 382
    :goto_9
    return-void

    .line 375
    :catch_0
    move-exception v0

    goto :goto_9

    .line 378
    :catch_1
    move-exception v0

    goto :goto_9

    .line 136
    :array_0
    .array-data 4
        0x0
        0x1e
        0x3b
        0x59
        0x76
        0x94
        0xb1
        0xcf
        0xec
        0x10a
        0x127
        0x145
    .end array-data

    .line 141
    :array_1
    .array-data 4
        0x0
        0x1e
        0x3b
        0x59
        0x76
        0x94
        0xb1
        0xcf
        0xec
        0x10a
        0x127
        0x145
    .end array-data

    .line 146
    :array_2
    .array-data 4
        0x1e
        0x1d
        0x1e
        0x1d
        0x1e
        0x1d
        0x1e
        0x1d
        0x1e
        0x1d
        0x1e
        0x1d
    .end array-data

    .line 151
    :array_3
    .array-data 4
        0x1e
        0x1d
        0x1e
        0x1d
        0x1e
        0x1d
        0x1e
        0x1d
        0x1e
        0x1d
        0x1e
        0x1e
    .end array-data

    .line 168
    :array_4
    .array-data 4
        0x0
        0x1
        0x0
        0x1
        0x0
        0x1
        0x1
    .end array-data

    .line 182
    :array_5
    .array-data 4
        0x1
        0x270f
        0xb
        0x33
        0x5
        0x1d
        0x162
    .end array-data

    .line 196
    :array_6
    .array-data 4
        0x1
        0x270f
        0xb
        0x34
        0x6
        0x1e
        0x163
    .end array-data

    .line 220
    :array_7
    .array-data 4
        0x0
        0x162
        0x2c5
        0x427
        0x589
        0x6ec
        0x84e
        0x9b1
        0xb13
        0xc75
        0xdd8
        0xf3a
        0x109c
        0x11ff
        0x1361
        0x14c3
        0x1626
        0x1788
        0x18eb
        0x1a4d
        0x1baf
        0x1d12
        0x1e74
        0x1fd6
        0x2139
        0x229b
        0x23fe
        0x2560
        0x26c2
        0x2825
    .end array-data
.end method

.method private constructor <init>(J)V
    .locals 7

    .prologue
    const/4 v5, 0x4

    const/4 v4, 0x3

    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 588
    invoke-direct {p0}, Lorg/threeten/bp/chrono/ChronoDateImpl;-><init>()V

    .line 589
    invoke-static {p1, p2}, Lorg/threeten/bp/chrono/HijrahDate;->i(J)[I

    move-result-object v0

    .line 591
    aget v1, v0, v2

    invoke-static {v1}, Lorg/threeten/bp/chrono/HijrahDate;->b(I)V

    .line 592
    aget v1, v0, v3

    invoke-static {v1}, Lorg/threeten/bp/chrono/HijrahDate;->d(I)V

    .line 593
    aget v1, v0, v4

    invoke-static {v1}, Lorg/threeten/bp/chrono/HijrahDate;->e(I)V

    .line 594
    aget v1, v0, v5

    invoke-static {v1}, Lorg/threeten/bp/chrono/HijrahDate;->c(I)V

    .line 596
    const/4 v1, 0x0

    aget v1, v0, v1

    invoke-static {v1}, Lorg/threeten/bp/chrono/HijrahEra;->a(I)Lorg/threeten/bp/chrono/HijrahEra;

    move-result-object v1

    iput-object v1, p0, Lorg/threeten/bp/chrono/HijrahDate;->x:Lorg/threeten/bp/chrono/HijrahEra;

    .line 597
    aget v1, v0, v2

    iput v1, p0, Lorg/threeten/bp/chrono/HijrahDate;->y:I

    .line 598
    aget v1, v0, v3

    iput v1, p0, Lorg/threeten/bp/chrono/HijrahDate;->z:I

    .line 599
    aget v1, v0, v4

    iput v1, p0, Lorg/threeten/bp/chrono/HijrahDate;->A:I

    .line 600
    aget v1, v0, v5

    iput v1, p0, Lorg/threeten/bp/chrono/HijrahDate;->B:I

    .line 601
    const/4 v1, 0x5

    aget v0, v0, v1

    invoke-static {v0}, Lorg/threeten/bp/DayOfWeek;->a(I)Lorg/threeten/bp/DayOfWeek;

    move-result-object v0

    iput-object v0, p0, Lorg/threeten/bp/chrono/HijrahDate;->C:Lorg/threeten/bp/DayOfWeek;

    .line 602
    iput-wide p1, p0, Lorg/threeten/bp/chrono/HijrahDate;->D:J

    .line 603
    iget v0, p0, Lorg/threeten/bp/chrono/HijrahDate;->y:I

    int-to-long v0, v0

    invoke-static {v0, v1}, Lorg/threeten/bp/chrono/HijrahDate;->h(J)Z

    move-result v0

    iput-boolean v0, p0, Lorg/threeten/bp/chrono/HijrahDate;->E:Z

    .line 604
    return-void
.end method

.method static a(I)I
    .locals 5

    .prologue
    .line 1154
    add-int/lit8 v0, p0, -0x1

    div-int/lit8 v1, v0, 0x1e

    .line 1157
    :try_start_0
    sget-object v0, Lorg/threeten/bp/chrono/HijrahDate;->n:Ljava/util/HashMap;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/Integer;
    :try_end_0
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1161
    :goto_0
    if-eqz v0, :cond_1

    .line 1162
    add-int/lit8 v2, p0, -0x1

    rem-int/lit8 v2, v2, 0x1e

    .line 1163
    const/16 v3, 0x1d

    if-ne v2, v3, :cond_0

    .line 1164
    sget-object v3, Lorg/threeten/bp/chrono/HijrahDate;->o:[Ljava/lang/Long;

    add-int/lit8 v4, v1, 0x1

    aget-object v3, v3, v4

    invoke-virtual {v3}, Ljava/lang/Long;->intValue()I

    move-result v3

    sget-object v4, Lorg/threeten/bp/chrono/HijrahDate;->o:[Ljava/lang/Long;

    aget-object v1, v4, v1

    invoke-virtual {v1}, Ljava/lang/Long;->intValue()I

    move-result v1

    sub-int v1, v3, v1

    aget-object v0, v0, v2

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    sub-int v0, v1, v0

    .line 1171
    :goto_1
    return v0

    .line 1158
    :catch_0
    move-exception v0

    .line 1159
    const/4 v0, 0x0

    goto :goto_0

    .line 1168
    :cond_0
    add-int/lit8 v1, v2, 0x1

    aget-object v1, v0, v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    aget-object v0, v0, v2

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    sub-int v0, v1, v0

    goto :goto_1

    .line 1171
    :cond_1
    int-to-long v0, p0

    invoke-static {v0, v1}, Lorg/threeten/bp/chrono/HijrahDate;->h(J)Z

    move-result v0

    if-eqz v0, :cond_2

    const/16 v0, 0x163

    goto :goto_1

    :cond_2
    const/16 v0, 0x162

    goto :goto_1
.end method

.method static a(II)I
    .locals 1

    .prologue
    .line 1137
    invoke-static {p1}, Lorg/threeten/bp/chrono/HijrahDate;->i(I)[Ljava/lang/Integer;

    move-result-object v0

    .line 1138
    aget-object v0, v0, p0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0
.end method

.method private static a(IJ)I
    .locals 9

    .prologue
    const-wide/16 v4, 0x0

    const/16 v1, 0x1d

    const/4 v0, 0x0

    .line 941
    invoke-static {p0}, Lorg/threeten/bp/chrono/HijrahDate;->g(I)[Ljava/lang/Integer;

    move-result-object v2

    .line 942
    cmp-long v3, p1, v4

    if-nez v3, :cond_0

    .line 960
    :goto_0
    return v0

    .line 946
    :cond_0
    cmp-long v3, p1, v4

    if-lez v3, :cond_3

    .line 947
    :goto_1
    array-length v3, v2

    if-ge v0, v3, :cond_2

    .line 948
    aget-object v3, v2, v0

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    int-to-long v4, v3

    cmp-long v3, p1, v4

    if-gez v3, :cond_1

    .line 949
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 947
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    move v0, v1

    .line 952
    goto :goto_0

    .line 954
    :cond_3
    neg-long v4, p1

    .line 955
    :goto_2
    array-length v3, v2

    if-ge v0, v3, :cond_5

    .line 956
    aget-object v3, v2, v0

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    int-to-long v6, v3

    cmp-long v3, v4, v6

    if-gtz v3, :cond_4

    .line 957
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 955
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_5
    move v0, v1

    .line 960
    goto :goto_0
.end method

.method private static a(JI)I
    .locals 4

    .prologue
    .line 923
    :try_start_0
    sget-object v0, Lorg/threeten/bp/chrono/HijrahDate;->o:[Ljava/lang/Long;

    aget-object v0, v0, p2
    :try_end_0
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    .line 927
    :goto_0
    if-nez v0, :cond_0

    .line 928
    new-instance v0, Ljava/lang/Long;

    mul-int/lit16 v1, p2, 0x2987

    int-to-long v2, v1

    invoke-direct {v0, v2, v3}, Ljava/lang/Long;-><init>(J)V

    .line 930
    :cond_0
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    sub-long v0, p0, v0

    long-to-int v0, v0

    return v0

    .line 924
    :catch_0
    move-exception v0

    .line 925
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static a(Ljava/io/DataInput;)Lorg/threeten/bp/chrono/ChronoLocalDate;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1770
    invoke-interface {p0}, Ljava/io/DataInput;->readInt()I

    move-result v0

    .line 1771
    invoke-interface {p0}, Ljava/io/DataInput;->readByte()B

    move-result v1

    .line 1772
    invoke-interface {p0}, Ljava/io/DataInput;->readByte()B

    move-result v2

    .line 1773
    sget-object v3, Lorg/threeten/bp/chrono/HijrahChronology;->b:Lorg/threeten/bp/chrono/HijrahChronology;

    invoke-virtual {v3, v0, v1, v2}, Lorg/threeten/bp/chrono/HijrahChronology;->b(III)Lorg/threeten/bp/chrono/HijrahDate;

    move-result-object v0

    return-object v0
.end method

.method public static a(III)Lorg/threeten/bp/chrono/HijrahDate;
    .locals 2

    .prologue
    .line 487
    const/4 v0, 0x1

    if-lt p0, v0, :cond_0

    sget-object v0, Lorg/threeten/bp/chrono/HijrahEra;->b:Lorg/threeten/bp/chrono/HijrahEra;

    invoke-static {v0, p0, p1, p2}, Lorg/threeten/bp/chrono/HijrahDate;->a(Lorg/threeten/bp/chrono/HijrahEra;III)Lorg/threeten/bp/chrono/HijrahDate;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lorg/threeten/bp/chrono/HijrahEra;->a:Lorg/threeten/bp/chrono/HijrahEra;

    rsub-int/lit8 v1, p0, 0x1

    invoke-static {v0, v1, p1, p2}, Lorg/threeten/bp/chrono/HijrahDate;->a(Lorg/threeten/bp/chrono/HijrahEra;III)Lorg/threeten/bp/chrono/HijrahDate;

    move-result-object v0

    goto :goto_0
.end method

.method static a(Lorg/threeten/bp/chrono/HijrahEra;III)Lorg/threeten/bp/chrono/HijrahDate;
    .locals 3

    .prologue
    .line 505
    const-string v0, "era"

    invoke-static {p0, v0}, Lorg/threeten/bp/jdk8/Jdk8Methods;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 506
    invoke-static {p1}, Lorg/threeten/bp/chrono/HijrahDate;->b(I)V

    .line 507
    invoke-static {p2}, Lorg/threeten/bp/chrono/HijrahDate;->d(I)V

    .line 508
    invoke-static {p3}, Lorg/threeten/bp/chrono/HijrahDate;->e(I)V

    .line 509
    invoke-virtual {p0, p1}, Lorg/threeten/bp/chrono/HijrahEra;->b(I)I

    move-result v0

    invoke-static {v0, p2, p3}, Lorg/threeten/bp/chrono/HijrahDate;->c(III)J

    move-result-wide v0

    .line 510
    new-instance v2, Lorg/threeten/bp/chrono/HijrahDate;

    invoke-direct {v2, v0, v1}, Lorg/threeten/bp/chrono/HijrahDate;-><init>(J)V

    return-object v2
.end method

.method private static a(IIIII)V
    .locals 9

    .prologue
    .line 1236
    const/4 v0, 0x1

    if-ge p0, v0, :cond_0

    .line 1237
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "startYear < 1"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1239
    :cond_0
    const/4 v0, 0x1

    if-ge p2, v0, :cond_1

    .line 1240
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "endYear < 1"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1242
    :cond_1
    if-ltz p1, :cond_2

    const/16 v0, 0xb

    if-le p1, v0, :cond_3

    .line 1243
    :cond_2
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "startMonth < 0 || startMonth > 11"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1246
    :cond_3
    if-ltz p3, :cond_4

    const/16 v0, 0xb

    if-le p3, v0, :cond_5

    .line 1247
    :cond_4
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "endMonth < 0 || endMonth > 11"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1249
    :cond_5
    const/16 v0, 0x270f

    if-le p2, v0, :cond_6

    .line 1250
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "endYear > 9999"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1252
    :cond_6
    if-ge p2, p0, :cond_7

    .line 1253
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "startYear > endYear"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1255
    :cond_7
    if-ne p2, p0, :cond_8

    if-ge p3, p1, :cond_8

    .line 1256
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "startYear == endYear && endMonth < startMonth"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1261
    :cond_8
    int-to-long v0, p0

    invoke-static {v0, v1}, Lorg/threeten/bp/chrono/HijrahDate;->h(J)Z

    move-result v2

    .line 1264
    sget-object v0, Lorg/threeten/bp/chrono/HijrahDate;->l:Ljava/util/HashMap;

    new-instance v1, Ljava/lang/Integer;

    invoke-direct {v1, p0}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/Integer;

    .line 1266
    if-nez v0, :cond_a

    .line 1267
    if-eqz v2, :cond_b

    .line 1268
    sget-object v0, Lorg/threeten/bp/chrono/HijrahDate;->b:[I

    array-length v0, v0

    new-array v1, v0, [Ljava/lang/Integer;

    .line 1269
    const/4 v0, 0x0

    :goto_0
    sget-object v3, Lorg/threeten/bp/chrono/HijrahDate;->b:[I

    array-length v3, v3

    if-ge v0, v3, :cond_9

    .line 1270
    new-instance v3, Ljava/lang/Integer;

    sget-object v4, Lorg/threeten/bp/chrono/HijrahDate;->b:[I

    aget v4, v4, v0

    invoke-direct {v3, v4}, Ljava/lang/Integer;-><init>(I)V

    aput-object v3, v1, v0

    .line 1269
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_9
    move-object v0, v1

    .line 1280
    :cond_a
    :goto_1
    array-length v1, v0

    new-array v3, v1, [Ljava/lang/Integer;

    .line 1282
    const/4 v1, 0x0

    :goto_2
    const/16 v4, 0xc

    if-ge v1, v4, :cond_d

    .line 1283
    if-le v1, p1, :cond_c

    .line 1284
    new-instance v4, Ljava/lang/Integer;

    aget-object v5, v0, v1

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    sub-int/2addr v5, p4

    invoke-direct {v4, v5}, Ljava/lang/Integer;-><init>(I)V

    aput-object v4, v3, v1

    .line 1282
    :goto_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 1273
    :cond_b
    sget-object v0, Lorg/threeten/bp/chrono/HijrahDate;->a:[I

    array-length v0, v0

    new-array v1, v0, [Ljava/lang/Integer;

    .line 1274
    const/4 v0, 0x0

    :goto_4
    sget-object v3, Lorg/threeten/bp/chrono/HijrahDate;->a:[I

    array-length v3, v3

    if-ge v0, v3, :cond_31

    .line 1275
    new-instance v3, Ljava/lang/Integer;

    sget-object v4, Lorg/threeten/bp/chrono/HijrahDate;->a:[I

    aget v4, v4, v0

    invoke-direct {v3, v4}, Ljava/lang/Integer;-><init>(I)V

    aput-object v3, v1, v0

    .line 1274
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 1288
    :cond_c
    new-instance v4, Ljava/lang/Integer;

    aget-object v5, v0, v1

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    invoke-direct {v4, v5}, Ljava/lang/Integer;-><init>(I)V

    aput-object v4, v3, v1

    goto :goto_3

    .line 1293
    :cond_d
    sget-object v0, Lorg/threeten/bp/chrono/HijrahDate;->l:Ljava/util/HashMap;

    new-instance v1, Ljava/lang/Integer;

    invoke-direct {v1, p0}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v0, v1, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1297
    sget-object v0, Lorg/threeten/bp/chrono/HijrahDate;->m:Ljava/util/HashMap;

    new-instance v1, Ljava/lang/Integer;

    invoke-direct {v1, p0}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/Integer;

    .line 1299
    if-nez v0, :cond_f

    .line 1300
    if-eqz v2, :cond_10

    .line 1301
    sget-object v0, Lorg/threeten/bp/chrono/HijrahDate;->d:[I

    array-length v0, v0

    new-array v1, v0, [Ljava/lang/Integer;

    .line 1302
    const/4 v0, 0x0

    :goto_5
    sget-object v2, Lorg/threeten/bp/chrono/HijrahDate;->d:[I

    array-length v2, v2

    if-ge v0, v2, :cond_e

    .line 1303
    new-instance v2, Ljava/lang/Integer;

    sget-object v3, Lorg/threeten/bp/chrono/HijrahDate;->d:[I

    aget v3, v3, v0

    invoke-direct {v2, v3}, Ljava/lang/Integer;-><init>(I)V

    aput-object v2, v1, v0

    .line 1302
    add-int/lit8 v0, v0, 0x1

    goto :goto_5

    :cond_e
    move-object v0, v1

    .line 1313
    :cond_f
    :goto_6
    array-length v1, v0

    new-array v2, v1, [Ljava/lang/Integer;

    .line 1315
    const/4 v1, 0x0

    :goto_7
    const/16 v3, 0xc

    if-ge v1, v3, :cond_12

    .line 1316
    if-ne v1, p1, :cond_11

    .line 1317
    new-instance v3, Ljava/lang/Integer;

    aget-object v4, v0, v1

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    sub-int/2addr v4, p4

    invoke-direct {v3, v4}, Ljava/lang/Integer;-><init>(I)V

    aput-object v3, v2, v1

    .line 1315
    :goto_8
    add-int/lit8 v1, v1, 0x1

    goto :goto_7

    .line 1306
    :cond_10
    sget-object v0, Lorg/threeten/bp/chrono/HijrahDate;->c:[I

    array-length v0, v0

    new-array v1, v0, [Ljava/lang/Integer;

    .line 1307
    const/4 v0, 0x0

    :goto_9
    sget-object v2, Lorg/threeten/bp/chrono/HijrahDate;->c:[I

    array-length v2, v2

    if-ge v0, v2, :cond_30

    .line 1308
    new-instance v2, Ljava/lang/Integer;

    sget-object v3, Lorg/threeten/bp/chrono/HijrahDate;->c:[I

    aget v3, v3, v0

    invoke-direct {v2, v3}, Ljava/lang/Integer;-><init>(I)V

    aput-object v2, v1, v0

    .line 1307
    add-int/lit8 v0, v0, 0x1

    goto :goto_9

    .line 1320
    :cond_11
    new-instance v3, Ljava/lang/Integer;

    aget-object v4, v0, v1

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-direct {v3, v4}, Ljava/lang/Integer;-><init>(I)V

    aput-object v3, v2, v1

    goto :goto_8

    .line 1325
    :cond_12
    sget-object v0, Lorg/threeten/bp/chrono/HijrahDate;->m:Ljava/util/HashMap;

    new-instance v1, Ljava/lang/Integer;

    invoke-direct {v1, p0}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1327
    if-eq p0, p2, :cond_1b

    .line 1330
    add-int/lit8 v0, p0, -0x1

    div-int/lit8 v2, v0, 0x1e

    .line 1331
    add-int/lit8 v0, p0, -0x1

    rem-int/lit8 v3, v0, 0x1e

    .line 1332
    sget-object v0, Lorg/threeten/bp/chrono/HijrahDate;->n:Ljava/util/HashMap;

    new-instance v1, Ljava/lang/Integer;

    invoke-direct {v1, v2}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/Integer;

    .line 1334
    if-nez v0, :cond_14

    .line 1335
    sget-object v0, Lorg/threeten/bp/chrono/HijrahDate;->h:[I

    array-length v0, v0

    new-array v1, v0, [Ljava/lang/Integer;

    .line 1336
    const/4 v0, 0x0

    :goto_a
    array-length v4, v1

    if-ge v0, v4, :cond_13

    .line 1337
    new-instance v4, Ljava/lang/Integer;

    sget-object v5, Lorg/threeten/bp/chrono/HijrahDate;->h:[I

    aget v5, v5, v0

    invoke-direct {v4, v5}, Ljava/lang/Integer;-><init>(I)V

    aput-object v4, v1, v0

    .line 1336
    add-int/lit8 v0, v0, 0x1

    goto :goto_a

    :cond_13
    move-object v0, v1

    .line 1341
    :cond_14
    add-int/lit8 v1, v3, 0x1

    :goto_b
    sget-object v3, Lorg/threeten/bp/chrono/HijrahDate;->h:[I

    array-length v3, v3

    if-ge v1, v3, :cond_15

    .line 1342
    new-instance v3, Ljava/lang/Integer;

    aget-object v4, v0, v1

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    sub-int/2addr v4, p4

    invoke-direct {v3, v4}, Ljava/lang/Integer;-><init>(I)V

    aput-object v3, v0, v1

    .line 1341
    add-int/lit8 v1, v1, 0x1

    goto :goto_b

    .line 1346
    :cond_15
    sget-object v1, Lorg/threeten/bp/chrono/HijrahDate;->n:Ljava/util/HashMap;

    new-instance v3, Ljava/lang/Integer;

    invoke-direct {v3, v2}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v1, v3, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1348
    add-int/lit8 v0, p0, -0x1

    div-int/lit8 v0, v0, 0x1e

    .line 1349
    add-int/lit8 v1, p2, -0x1

    div-int/lit8 v1, v1, 0x1e

    .line 1351
    if-eq v0, v1, :cond_17

    .line 1356
    add-int/lit8 v0, v0, 0x1

    :goto_c
    sget-object v2, Lorg/threeten/bp/chrono/HijrahDate;->o:[Ljava/lang/Long;

    array-length v2, v2

    if-ge v0, v2, :cond_16

    .line 1357
    sget-object v2, Lorg/threeten/bp/chrono/HijrahDate;->o:[Ljava/lang/Long;

    new-instance v3, Ljava/lang/Long;

    sget-object v4, Lorg/threeten/bp/chrono/HijrahDate;->o:[Ljava/lang/Long;

    aget-object v4, v4, v0

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    int-to-long v6, p4

    sub-long/2addr v4, v6

    invoke-direct {v3, v4, v5}, Ljava/lang/Long;-><init>(J)V

    aput-object v3, v2, v0

    .line 1356
    add-int/lit8 v0, v0, 0x1

    goto :goto_c

    .line 1362
    :cond_16
    add-int/lit8 v0, v1, 0x1

    :goto_d
    sget-object v1, Lorg/threeten/bp/chrono/HijrahDate;->o:[Ljava/lang/Long;

    array-length v1, v1

    if-ge v0, v1, :cond_17

    .line 1363
    sget-object v1, Lorg/threeten/bp/chrono/HijrahDate;->o:[Ljava/lang/Long;

    new-instance v2, Ljava/lang/Long;

    sget-object v3, Lorg/threeten/bp/chrono/HijrahDate;->o:[Ljava/lang/Long;

    aget-object v3, v3, v0

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    int-to-long v6, p4

    add-long/2addr v4, v6

    invoke-direct {v2, v4, v5}, Ljava/lang/Long;-><init>(J)V

    aput-object v2, v1, v0

    .line 1362
    add-int/lit8 v0, v0, 0x1

    goto :goto_d

    .line 1369
    :cond_17
    add-int/lit8 v0, p2, -0x1

    div-int/lit8 v2, v0, 0x1e

    .line 1370
    add-int/lit8 v0, p2, -0x1

    rem-int/lit8 v3, v0, 0x1e

    .line 1371
    sget-object v0, Lorg/threeten/bp/chrono/HijrahDate;->n:Ljava/util/HashMap;

    new-instance v1, Ljava/lang/Integer;

    invoke-direct {v1, v2}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/Integer;

    .line 1373
    if-nez v0, :cond_19

    .line 1374
    sget-object v0, Lorg/threeten/bp/chrono/HijrahDate;->h:[I

    array-length v0, v0

    new-array v1, v0, [Ljava/lang/Integer;

    .line 1375
    const/4 v0, 0x0

    :goto_e
    array-length v4, v1

    if-ge v0, v4, :cond_18

    .line 1376
    new-instance v4, Ljava/lang/Integer;

    sget-object v5, Lorg/threeten/bp/chrono/HijrahDate;->h:[I

    aget v5, v5, v0

    invoke-direct {v4, v5}, Ljava/lang/Integer;-><init>(I)V

    aput-object v4, v1, v0

    .line 1375
    add-int/lit8 v0, v0, 0x1

    goto :goto_e

    :cond_18
    move-object v0, v1

    .line 1379
    :cond_19
    add-int/lit8 v1, v3, 0x1

    :goto_f
    sget-object v3, Lorg/threeten/bp/chrono/HijrahDate;->h:[I

    array-length v3, v3

    if-ge v1, v3, :cond_1a

    .line 1380
    new-instance v3, Ljava/lang/Integer;

    aget-object v4, v0, v1

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    add-int/2addr v4, p4

    invoke-direct {v3, v4}, Ljava/lang/Integer;-><init>(I)V

    aput-object v3, v0, v1

    .line 1379
    add-int/lit8 v1, v1, 0x1

    goto :goto_f

    .line 1382
    :cond_1a
    sget-object v1, Lorg/threeten/bp/chrono/HijrahDate;->n:Ljava/util/HashMap;

    new-instance v3, Ljava/lang/Integer;

    invoke-direct {v3, v2}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v1, v3, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1386
    :cond_1b
    int-to-long v0, p2

    invoke-static {v0, v1}, Lorg/threeten/bp/chrono/HijrahDate;->h(J)Z

    move-result v2

    .line 1388
    sget-object v0, Lorg/threeten/bp/chrono/HijrahDate;->l:Ljava/util/HashMap;

    new-instance v1, Ljava/lang/Integer;

    invoke-direct {v1, p2}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/Integer;

    .line 1390
    if-nez v0, :cond_1d

    .line 1391
    if-eqz v2, :cond_1e

    .line 1392
    sget-object v0, Lorg/threeten/bp/chrono/HijrahDate;->b:[I

    array-length v0, v0

    new-array v1, v0, [Ljava/lang/Integer;

    .line 1393
    const/4 v0, 0x0

    :goto_10
    sget-object v3, Lorg/threeten/bp/chrono/HijrahDate;->b:[I

    array-length v3, v3

    if-ge v0, v3, :cond_1c

    .line 1394
    new-instance v3, Ljava/lang/Integer;

    sget-object v4, Lorg/threeten/bp/chrono/HijrahDate;->b:[I

    aget v4, v4, v0

    invoke-direct {v3, v4}, Ljava/lang/Integer;-><init>(I)V

    aput-object v3, v1, v0

    .line 1393
    add-int/lit8 v0, v0, 0x1

    goto :goto_10

    :cond_1c
    move-object v0, v1

    .line 1404
    :cond_1d
    :goto_11
    array-length v1, v0

    new-array v3, v1, [Ljava/lang/Integer;

    .line 1406
    const/4 v1, 0x0

    :goto_12
    const/16 v4, 0xc

    if-ge v1, v4, :cond_20

    .line 1407
    if-le v1, p3, :cond_1f

    .line 1408
    new-instance v4, Ljava/lang/Integer;

    aget-object v5, v0, v1

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    add-int/2addr v5, p4

    invoke-direct {v4, v5}, Ljava/lang/Integer;-><init>(I)V

    aput-object v4, v3, v1

    .line 1406
    :goto_13
    add-int/lit8 v1, v1, 0x1

    goto :goto_12

    .line 1397
    :cond_1e
    sget-object v0, Lorg/threeten/bp/chrono/HijrahDate;->a:[I

    array-length v0, v0

    new-array v1, v0, [Ljava/lang/Integer;

    .line 1398
    const/4 v0, 0x0

    :goto_14
    sget-object v3, Lorg/threeten/bp/chrono/HijrahDate;->a:[I

    array-length v3, v3

    if-ge v0, v3, :cond_2f

    .line 1399
    new-instance v3, Ljava/lang/Integer;

    sget-object v4, Lorg/threeten/bp/chrono/HijrahDate;->a:[I

    aget v4, v4, v0

    invoke-direct {v3, v4}, Ljava/lang/Integer;-><init>(I)V

    aput-object v3, v1, v0

    .line 1398
    add-int/lit8 v0, v0, 0x1

    goto :goto_14

    .line 1412
    :cond_1f
    new-instance v4, Ljava/lang/Integer;

    aget-object v5, v0, v1

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    invoke-direct {v4, v5}, Ljava/lang/Integer;-><init>(I)V

    aput-object v4, v3, v1

    goto :goto_13

    .line 1417
    :cond_20
    sget-object v0, Lorg/threeten/bp/chrono/HijrahDate;->l:Ljava/util/HashMap;

    new-instance v1, Ljava/lang/Integer;

    invoke-direct {v1, p2}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v0, v1, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1420
    sget-object v0, Lorg/threeten/bp/chrono/HijrahDate;->m:Ljava/util/HashMap;

    new-instance v1, Ljava/lang/Integer;

    invoke-direct {v1, p2}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/Integer;

    .line 1423
    if-nez v0, :cond_22

    .line 1424
    if-eqz v2, :cond_23

    .line 1425
    sget-object v0, Lorg/threeten/bp/chrono/HijrahDate;->d:[I

    array-length v0, v0

    new-array v1, v0, [Ljava/lang/Integer;

    .line 1426
    const/4 v0, 0x0

    :goto_15
    sget-object v2, Lorg/threeten/bp/chrono/HijrahDate;->d:[I

    array-length v2, v2

    if-ge v0, v2, :cond_21

    .line 1427
    new-instance v2, Ljava/lang/Integer;

    sget-object v3, Lorg/threeten/bp/chrono/HijrahDate;->d:[I

    aget v3, v3, v0

    invoke-direct {v2, v3}, Ljava/lang/Integer;-><init>(I)V

    aput-object v2, v1, v0

    .line 1426
    add-int/lit8 v0, v0, 0x1

    goto :goto_15

    :cond_21
    move-object v0, v1

    .line 1437
    :cond_22
    :goto_16
    array-length v1, v0

    new-array v2, v1, [Ljava/lang/Integer;

    .line 1439
    const/4 v1, 0x0

    :goto_17
    const/16 v3, 0xc

    if-ge v1, v3, :cond_25

    .line 1440
    if-ne v1, p3, :cond_24

    .line 1441
    new-instance v3, Ljava/lang/Integer;

    aget-object v4, v0, v1

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    add-int/2addr v4, p4

    invoke-direct {v3, v4}, Ljava/lang/Integer;-><init>(I)V

    aput-object v3, v2, v1

    .line 1439
    :goto_18
    add-int/lit8 v1, v1, 0x1

    goto :goto_17

    .line 1430
    :cond_23
    sget-object v0, Lorg/threeten/bp/chrono/HijrahDate;->c:[I

    array-length v0, v0

    new-array v1, v0, [Ljava/lang/Integer;

    .line 1431
    const/4 v0, 0x0

    :goto_19
    sget-object v2, Lorg/threeten/bp/chrono/HijrahDate;->c:[I

    array-length v2, v2

    if-ge v0, v2, :cond_2e

    .line 1432
    new-instance v2, Ljava/lang/Integer;

    sget-object v3, Lorg/threeten/bp/chrono/HijrahDate;->c:[I

    aget v3, v3, v0

    invoke-direct {v2, v3}, Ljava/lang/Integer;-><init>(I)V

    aput-object v2, v1, v0

    .line 1431
    add-int/lit8 v0, v0, 0x1

    goto :goto_19

    .line 1444
    :cond_24
    new-instance v3, Ljava/lang/Integer;

    aget-object v4, v0, v1

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-direct {v3, v4}, Ljava/lang/Integer;-><init>(I)V

    aput-object v3, v2, v1

    goto :goto_18

    .line 1449
    :cond_25
    sget-object v0, Lorg/threeten/bp/chrono/HijrahDate;->m:Ljava/util/HashMap;

    new-instance v1, Ljava/lang/Integer;

    invoke-direct {v1, p2}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1451
    sget-object v0, Lorg/threeten/bp/chrono/HijrahDate;->m:Ljava/util/HashMap;

    new-instance v1, Ljava/lang/Integer;

    invoke-direct {v1, p0}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/Integer;

    .line 1453
    sget-object v1, Lorg/threeten/bp/chrono/HijrahDate;->m:Ljava/util/HashMap;

    new-instance v2, Ljava/lang/Integer;

    invoke-direct {v2, p2}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Ljava/lang/Integer;

    .line 1455
    sget-object v2, Lorg/threeten/bp/chrono/HijrahDate;->l:Ljava/util/HashMap;

    new-instance v3, Ljava/lang/Integer;

    invoke-direct {v3, p0}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v2, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Ljava/lang/Integer;

    .line 1457
    sget-object v3, Lorg/threeten/bp/chrono/HijrahDate;->l:Ljava/util/HashMap;

    new-instance v4, Ljava/lang/Integer;

    invoke-direct {v4, p2}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v3, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [Ljava/lang/Integer;

    .line 1459
    aget-object v4, v0, p1

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v5

    .line 1460
    aget-object v4, v1, p3

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    .line 1461
    const/16 v6, 0xb

    aget-object v2, v2, v6

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    const/16 v6, 0xb

    aget-object v0, v0, v6

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    add-int/2addr v2, v0

    .line 1463
    const/16 v0, 0xb

    aget-object v0, v3, v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/16 v3, 0xb

    aget-object v1, v1, v3

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    add-int/2addr v1, v0

    .line 1466
    sget-object v0, Lorg/threeten/bp/chrono/HijrahDate;->r:[Ljava/lang/Integer;

    const/4 v3, 0x5

    aget-object v0, v0, v3

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 1468
    sget-object v3, Lorg/threeten/bp/chrono/HijrahDate;->q:[Ljava/lang/Integer;

    const/4 v6, 0x5

    aget-object v3, v3, v6

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    .line 1471
    if-ge v0, v5, :cond_26

    move v0, v5

    .line 1474
    :cond_26
    if-ge v0, v4, :cond_27

    move v0, v4

    .line 1477
    :cond_27
    sget-object v6, Lorg/threeten/bp/chrono/HijrahDate;->r:[Ljava/lang/Integer;

    const/4 v7, 0x5

    new-instance v8, Ljava/lang/Integer;

    invoke-direct {v8, v0}, Ljava/lang/Integer;-><init>(I)V

    aput-object v8, v6, v7

    .line 1479
    if-le v3, v5, :cond_2d

    move v0, v5

    .line 1482
    :goto_1a
    if-le v0, v4, :cond_2c

    .line 1485
    :goto_1b
    sget-object v0, Lorg/threeten/bp/chrono/HijrahDate;->q:[Ljava/lang/Integer;

    const/4 v3, 0x5

    new-instance v5, Ljava/lang/Integer;

    invoke-direct {v5, v4}, Ljava/lang/Integer;-><init>(I)V

    aput-object v5, v0, v3

    .line 1488
    sget-object v0, Lorg/threeten/bp/chrono/HijrahDate;->r:[Ljava/lang/Integer;

    const/4 v3, 0x6

    aget-object v0, v0, v3

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 1489
    sget-object v3, Lorg/threeten/bp/chrono/HijrahDate;->q:[Ljava/lang/Integer;

    const/4 v4, 0x6

    aget-object v3, v3, v4

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    .line 1492
    if-ge v0, v2, :cond_28

    move v0, v2

    .line 1495
    :cond_28
    if-ge v0, v1, :cond_29

    move v0, v1

    .line 1499
    :cond_29
    sget-object v4, Lorg/threeten/bp/chrono/HijrahDate;->r:[Ljava/lang/Integer;

    const/4 v5, 0x6

    new-instance v6, Ljava/lang/Integer;

    invoke-direct {v6, v0}, Ljava/lang/Integer;-><init>(I)V

    aput-object v6, v4, v5

    .line 1501
    if-le v3, v2, :cond_2b

    move v0, v2

    .line 1504
    :goto_1c
    if-le v0, v1, :cond_2a

    .line 1507
    :goto_1d
    sget-object v0, Lorg/threeten/bp/chrono/HijrahDate;->q:[Ljava/lang/Integer;

    const/4 v2, 0x6

    new-instance v3, Ljava/lang/Integer;

    invoke-direct {v3, v1}, Ljava/lang/Integer;-><init>(I)V

    aput-object v3, v0, v2

    .line 1509
    return-void

    :cond_2a
    move v1, v0

    goto :goto_1d

    :cond_2b
    move v0, v3

    goto :goto_1c

    :cond_2c
    move v4, v0

    goto :goto_1b

    :cond_2d
    move v0, v3

    goto :goto_1a

    :cond_2e
    move-object v0, v1

    goto/16 :goto_16

    :cond_2f
    move-object v0, v1

    goto/16 :goto_11

    :cond_30
    move-object v0, v1

    goto/16 :goto_6

    :cond_31
    move-object v0, v1

    goto/16 :goto_1
.end method

.method private static a(Ljava/lang/String;I)V
    .locals 11
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/text/ParseException;
        }
    .end annotation

    .prologue
    const/16 v10, 0x2f

    const/4 v9, 0x0

    const/4 v8, -0x1

    .line 1558
    new-instance v0, Ljava/util/StringTokenizer;

    const-string v1, ";"

    invoke-direct {v0, p0, v1}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 1559
    :goto_0
    invoke-virtual {v0}, Ljava/util/StringTokenizer;->hasMoreTokens()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 1560
    invoke-virtual {v0}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v1

    .line 1561
    const/16 v2, 0x3a

    invoke-virtual {v1, v2}, Ljava/lang/String;->indexOf(I)I

    move-result v2

    .line 1562
    if-eq v2, v8, :cond_4

    .line 1563
    add-int/lit8 v3, v2, 0x1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v4

    invoke-virtual {v1, v3, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    .line 1567
    :try_start_0
    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v3

    .line 1573
    const/16 v4, 0x2d

    invoke-virtual {v1, v4}, Ljava/lang/String;->indexOf(I)I

    move-result v4

    .line 1574
    if-eq v4, v8, :cond_3

    .line 1575
    invoke-virtual {v1, v9, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    .line 1577
    add-int/lit8 v4, v4, 0x1

    invoke-virtual {v1, v4, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    .line 1579
    invoke-virtual {v5, v10}, Ljava/lang/String;->indexOf(I)I

    move-result v2

    .line 1580
    invoke-virtual {v1, v10}, Ljava/lang/String;->indexOf(I)I

    move-result v4

    .line 1585
    if-eq v2, v8, :cond_0

    .line 1586
    invoke-virtual {v5, v9, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v6

    .line 1588
    add-int/lit8 v2, v2, 0x1

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v7

    invoke-virtual {v5, v2, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    .line 1592
    :try_start_1
    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_1

    move-result v5

    .line 1599
    :try_start_2
    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_2
    .catch Ljava/lang/NumberFormatException; {:try_start_2 .. :try_end_2} :catch_2

    move-result v2

    .line 1610
    if-eq v4, v8, :cond_1

    .line 1611
    invoke-virtual {v1, v9, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v6

    .line 1613
    add-int/lit8 v4, v4, 0x1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v7

    invoke-virtual {v1, v4, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    .line 1616
    :try_start_3
    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_3
    .catch Ljava/lang/NumberFormatException; {:try_start_3 .. :try_end_3} :catch_3

    move-result v4

    .line 1623
    :try_start_4
    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_4
    .catch Ljava/lang/NumberFormatException; {:try_start_4 .. :try_end_4} :catch_4

    move-result v1

    .line 1634
    if-eq v5, v8, :cond_2

    if-eq v2, v8, :cond_2

    if-eq v4, v8, :cond_2

    if-eq v1, v8, :cond_2

    .line 1636
    invoke-static {v5, v2, v4, v1, v3}, Lorg/threeten/bp/chrono/HijrahDate;->a(IIIII)V

    goto :goto_0

    .line 1568
    :catch_0
    move-exception v0

    .line 1569
    new-instance v0, Ljava/text/ParseException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Offset is not properly set at line "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Ljava/text/ParseException;-><init>(Ljava/lang/String;I)V

    throw v0

    .line 1593
    :catch_1
    move-exception v0

    .line 1594
    new-instance v0, Ljava/text/ParseException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Start year is not properly set at line "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Ljava/text/ParseException;-><init>(Ljava/lang/String;I)V

    throw v0

    .line 1600
    :catch_2
    move-exception v0

    .line 1601
    new-instance v0, Ljava/text/ParseException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Start month is not properly set at line "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Ljava/text/ParseException;-><init>(Ljava/lang/String;I)V

    throw v0

    .line 1606
    :cond_0
    new-instance v0, Ljava/text/ParseException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Start year/month has incorrect format at line "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Ljava/text/ParseException;-><init>(Ljava/lang/String;I)V

    throw v0

    .line 1617
    :catch_3
    move-exception v0

    .line 1618
    new-instance v0, Ljava/text/ParseException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "End year is not properly set at line "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Ljava/text/ParseException;-><init>(Ljava/lang/String;I)V

    throw v0

    .line 1624
    :catch_4
    move-exception v0

    .line 1625
    new-instance v0, Ljava/text/ParseException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "End month is not properly set at line "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Ljava/text/ParseException;-><init>(Ljava/lang/String;I)V

    throw v0

    .line 1630
    :cond_1
    new-instance v0, Ljava/text/ParseException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "End year/month has incorrect format at line "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Ljava/text/ParseException;-><init>(Ljava/lang/String;I)V

    throw v0

    .line 1639
    :cond_2
    new-instance v0, Ljava/text/ParseException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown error at line "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Ljava/text/ParseException;-><init>(Ljava/lang/String;I)V

    throw v0

    .line 1643
    :cond_3
    new-instance v0, Ljava/text/ParseException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Start and end year/month has incorrect format at line "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Ljava/text/ParseException;-><init>(Ljava/lang/String;I)V

    throw v0

    .line 1648
    :cond_4
    new-instance v0, Ljava/text/ParseException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Offset has incorrect format at line "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Ljava/text/ParseException;-><init>(Ljava/lang/String;I)V

    throw v0

    .line 1652
    :cond_5
    return-void
.end method

.method private static b(II)I
    .locals 6

    .prologue
    const/16 v2, 0xb

    const/4 v0, 0x0

    .line 1057
    invoke-static {p1}, Lorg/threeten/bp/chrono/HijrahDate;->h(I)[Ljava/lang/Integer;

    move-result-object v3

    .line 1059
    if-ltz p0, :cond_2

    .line 1060
    :goto_0
    array-length v1, v3

    if-ge v0, v1, :cond_1

    .line 1061
    aget-object v1, v3, v0

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-ge p0, v1, :cond_0

    .line 1062
    add-int/lit8 v0, v0, -0x1

    .line 1074
    :goto_1
    return v0

    .line 1060
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    move v0, v2

    .line 1065
    goto :goto_1

    .line 1067
    :cond_2
    int-to-long v4, p1

    invoke-static {v4, v5}, Lorg/threeten/bp/chrono/HijrahDate;->h(J)Z

    move-result v1

    if-eqz v1, :cond_3

    add-int/lit16 v1, p0, 0x163

    .line 1069
    :goto_2
    array-length v4, v3

    if-ge v0, v4, :cond_5

    .line 1070
    aget-object v4, v3, v0

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    if-ge v1, v4, :cond_4

    .line 1071
    add-int/lit8 v0, v0, -0x1

    goto :goto_1

    .line 1067
    :cond_3
    add-int/lit16 v1, p0, 0x162

    goto :goto_2

    .line 1069
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_5
    move v0, v2

    .line 1074
    goto :goto_1
.end method

.method private static b(III)Lorg/threeten/bp/chrono/HijrahDate;
    .locals 1

    .prologue
    .line 698
    add-int/lit8 v0, p1, -0x1

    invoke-static {v0, p0}, Lorg/threeten/bp/chrono/HijrahDate;->c(II)I

    move-result v0

    .line 699
    if-le p2, v0, :cond_0

    move p2, v0

    .line 702
    :cond_0
    invoke-static {p0, p1, p2}, Lorg/threeten/bp/chrono/HijrahDate;->a(III)Lorg/threeten/bp/chrono/HijrahDate;

    move-result-object v0

    return-object v0
.end method

.method private static b(I)V
    .locals 2

    .prologue
    .line 518
    const/4 v0, 0x1

    if-lt p0, v0, :cond_0

    const/16 v0, 0x270f

    if-le p0, v0, :cond_1

    .line 520
    :cond_0
    new-instance v0, Lorg/threeten/bp/DateTimeException;

    const-string v1, "Invalid year of Hijrah Era"

    invoke-direct {v0, v1}, Lorg/threeten/bp/DateTimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 522
    :cond_1
    return-void
.end method

.method private static c(II)I
    .locals 1

    .prologue
    .line 1125
    invoke-static {p1}, Lorg/threeten/bp/chrono/HijrahDate;->h(I)[Ljava/lang/Integer;

    move-result-object v0

    .line 1126
    aget-object v0, v0, p0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0
.end method

.method private static c(III)J
    .locals 4

    .prologue
    .line 852
    invoke-static {p0}, Lorg/threeten/bp/chrono/HijrahDate;->f(I)J

    move-result-wide v0

    .line 853
    add-int/lit8 v2, p1, -0x1

    invoke-static {v2, p0}, Lorg/threeten/bp/chrono/HijrahDate;->c(II)I

    move-result v2

    int-to-long v2, v2

    add-long/2addr v0, v2

    .line 854
    int-to-long v2, p2

    add-long/2addr v0, v2

    .line 855
    return-wide v0
.end method

.method private static c(I)V
    .locals 2

    .prologue
    .line 525
    const/4 v0, 0x1

    if-lt p0, v0, :cond_0

    invoke-static {}, Lorg/threeten/bp/chrono/HijrahDate;->f()I

    move-result v0

    if-le p0, v0, :cond_1

    .line 527
    :cond_0
    new-instance v0, Lorg/threeten/bp/DateTimeException;

    const-string v1, "Invalid day of year of Hijrah date"

    invoke-direct {v0, v1}, Lorg/threeten/bp/DateTimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 529
    :cond_1
    return-void
.end method

.method private static d(III)I
    .locals 1

    .prologue
    .line 1039
    invoke-static {p0}, Lorg/threeten/bp/chrono/HijrahDate;->g(I)[Ljava/lang/Integer;

    move-result-object v0

    .line 1041
    if-lez p1, :cond_0

    .line 1042
    aget-object v0, v0, p2

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    sub-int v0, p1, v0

    .line 1044
    :goto_0
    return v0

    :cond_0
    aget-object v0, v0, p2

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    add-int/2addr v0, p1

    goto :goto_0
.end method

.method static d(J)Lorg/threeten/bp/chrono/HijrahDate;
    .locals 2

    .prologue
    .line 558
    new-instance v0, Lorg/threeten/bp/chrono/HijrahDate;

    invoke-direct {v0, p0, p1}, Lorg/threeten/bp/chrono/HijrahDate;-><init>(J)V

    return-object v0
.end method

.method private static d(I)V
    .locals 2

    .prologue
    .line 532
    const/4 v0, 0x1

    if-lt p0, v0, :cond_0

    const/16 v0, 0xc

    if-le p0, v0, :cond_1

    .line 533
    :cond_0
    new-instance v0, Lorg/threeten/bp/DateTimeException;

    const-string v1, "Invalid month of Hijrah date"

    invoke-direct {v0, v1}, Lorg/threeten/bp/DateTimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 535
    :cond_1
    return-void
.end method

.method static e()I
    .locals 2

    .prologue
    .line 1186
    sget-object v0, Lorg/threeten/bp/chrono/HijrahDate;->r:[Ljava/lang/Integer;

    const/4 v1, 0x5

    aget-object v0, v0, v1

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0
.end method

.method private static e(III)I
    .locals 4

    .prologue
    .line 1088
    invoke-static {p2}, Lorg/threeten/bp/chrono/HijrahDate;->h(I)[Ljava/lang/Integer;

    move-result-object v0

    .line 1090
    if-ltz p0, :cond_1

    .line 1091
    if-lez p1, :cond_0

    .line 1092
    aget-object v0, v0, p1

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    sub-int/2addr p0, v0

    .line 1102
    :cond_0
    :goto_0
    return p0

    .line 1097
    :cond_1
    int-to-long v2, p2

    invoke-static {v2, v3}, Lorg/threeten/bp/chrono/HijrahDate;->h(J)Z

    move-result v1

    if-eqz v1, :cond_2

    add-int/lit16 p0, p0, 0x163

    .line 1099
    :goto_1
    if-lez p1, :cond_0

    .line 1100
    aget-object v0, v0, p1

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    sub-int/2addr p0, v0

    goto :goto_0

    .line 1097
    :cond_2
    add-int/lit16 p0, p0, 0x162

    goto :goto_1
.end method

.method private static e(I)V
    .locals 3

    .prologue
    .line 538
    const/4 v0, 0x1

    if-lt p0, v0, :cond_0

    invoke-static {}, Lorg/threeten/bp/chrono/HijrahDate;->e()I

    move-result v0

    if-le p0, v0, :cond_1

    .line 540
    :cond_0
    new-instance v0, Lorg/threeten/bp/DateTimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Invalid day of month of Hijrah date, day "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " greater than "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Lorg/threeten/bp/chrono/HijrahDate;->e()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " or less than 1"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/threeten/bp/DateTimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 543
    :cond_1
    return-void
.end method

.method static f()I
    .locals 2

    .prologue
    .line 1204
    sget-object v0, Lorg/threeten/bp/chrono/HijrahDate;->r:[Ljava/lang/Integer;

    const/4 v1, 0x6

    aget-object v0, v0, v1

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0
.end method

.method private static f(I)J
    .locals 4

    .prologue
    .line 865
    add-int/lit8 v0, p0, -0x1

    div-int/lit8 v2, v0, 0x1e

    .line 866
    add-int/lit8 v0, p0, -0x1

    rem-int/lit8 v1, v0, 0x1e

    .line 868
    invoke-static {v2}, Lorg/threeten/bp/chrono/HijrahDate;->g(I)[Ljava/lang/Integer;

    move-result-object v0

    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    move-result v3

    aget-object v0, v0, v3

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 871
    if-gez v1, :cond_1

    .line 872
    neg-int v0, v0

    move v1, v0

    .line 878
    :goto_0
    :try_start_0
    sget-object v0, Lorg/threeten/bp/chrono/HijrahDate;->o:[Ljava/lang/Long;

    aget-object v0, v0, v2
    :try_end_0
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    .line 883
    :goto_1
    if-nez v0, :cond_0

    .line 884
    new-instance v0, Ljava/lang/Long;

    mul-int/lit16 v2, v2, 0x2987

    int-to-long v2, v2

    invoke-direct {v0, v2, v3}, Ljava/lang/Long;-><init>(J)V

    .line 887
    :cond_0
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    int-to-long v0, v1

    add-long/2addr v0, v2

    const-wide/32 v2, -0x78274

    add-long/2addr v0, v2

    const-wide/16 v2, 0x1

    sub-long/2addr v0, v2

    return-wide v0

    .line 879
    :catch_0
    move-exception v0

    .line 880
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    move v1, v0

    goto :goto_0
.end method

.method private static g()V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/text/ParseException;
        }
    .end annotation

    .prologue
    .line 1530
    invoke-static {}, Lorg/threeten/bp/chrono/HijrahDate;->h()Ljava/io/InputStream;

    move-result-object v0

    .line 1531
    if-eqz v0, :cond_2

    .line 1532
    const/4 v2, 0x0

    .line 1534
    :try_start_0
    new-instance v1, Ljava/io/BufferedReader;

    new-instance v3, Ljava/io/InputStreamReader;

    invoke-direct {v3, v0}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    invoke-direct {v1, v3}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1535
    :try_start_1
    const-string v0, ""

    .line 1536
    const/4 v0, 0x0

    .line 1537
    :goto_0
    invoke-virtual {v1}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 1538
    add-int/lit8 v0, v0, 0x1

    .line 1539
    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    .line 1540
    invoke-static {v2, v0}, Lorg/threeten/bp/chrono/HijrahDate;->a(Ljava/lang/String;I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1543
    :catchall_0
    move-exception v0

    :goto_1
    if-eqz v1, :cond_0

    .line 1544
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V

    :cond_0
    throw v0

    .line 1543
    :cond_1
    if-eqz v1, :cond_2

    .line 1544
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V

    .line 1548
    :cond_2
    return-void

    .line 1543
    :catchall_1
    move-exception v0

    move-object v1, v2

    goto :goto_1
.end method

.method private static g(I)[Ljava/lang/Integer;
    .locals 2

    .prologue
    .line 974
    :try_start_0
    sget-object v0, Lorg/threeten/bp/chrono/HijrahDate;->n:Ljava/util/HashMap;

    new-instance v1, Ljava/lang/Integer;

    invoke-direct {v1, p0}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/Integer;
    :try_end_0
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    .line 978
    :goto_0
    if-nez v0, :cond_0

    .line 979
    sget-object v0, Lorg/threeten/bp/chrono/HijrahDate;->w:[Ljava/lang/Integer;

    .line 981
    :cond_0
    return-object v0

    .line 975
    :catch_0
    move-exception v0

    .line 976
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static h()Ljava/io/InputStream;
    .locals 9
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    const/16 v8, 0x5c

    const/16 v7, 0x2f

    .line 1677
    const-string v0, "org.threeten.bp.i18n.HijrahDate.deviationConfigFile"

    invoke-static {v0}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1680
    if-nez v0, :cond_0

    .line 1681
    const-string v0, "hijrah_deviation.cfg"

    .line 1684
    :cond_0
    const-string v1, "org.threeten.bp.i18n.HijrahDate.deviationConfigDir"

    invoke-static {v1}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1687
    if-eqz v1, :cond_4

    .line 1688
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v3

    if-nez v3, :cond_1

    const-string v3, "file.separator"

    invoke-static {v3}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 1690
    :cond_1
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "file.separator"

    invoke-static {v3}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1692
    :cond_2
    new-instance v3, Ljava/io/File;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-char v4, Lorg/threeten/bp/chrono/HijrahDate;->i:C

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v3, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1693
    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1695
    :try_start_0
    new-instance v0, Ljava/io/FileInputStream;

    invoke-direct {v0, v3}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1754
    :goto_0
    return-object v0

    .line 1696
    :catch_0
    move-exception v0

    .line 1697
    throw v0

    :cond_3
    move-object v0, v2

    .line 1700
    goto :goto_0

    .line 1703
    :cond_4
    const-string v1, "java.class.path"

    invoke-static {v1}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1704
    new-instance v5, Ljava/util/StringTokenizer;

    sget-object v3, Lorg/threeten/bp/chrono/HijrahDate;->j:Ljava/lang/String;

    invoke-direct {v5, v1, v3}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 1705
    :cond_5
    invoke-virtual {v5}, Ljava/util/StringTokenizer;->hasMoreTokens()Z

    move-result v1

    if-eqz v1, :cond_9

    .line 1706
    invoke-virtual {v5}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v3

    .line 1707
    new-instance v4, Ljava/io/File;

    invoke-direct {v4, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1708
    invoke-virtual {v4}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 1709
    invoke-virtual {v4}, Ljava/io/File;->isDirectory()Z

    move-result v1

    if-eqz v1, :cond_6

    .line 1710
    new-instance v1, Ljava/io/File;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    sget-char v6, Lorg/threeten/bp/chrono/HijrahDate;->i:C

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v4

    sget-object v6, Lorg/threeten/bp/chrono/HijrahDate;->k:Ljava/lang/String;

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v1, v4, v0}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 1712
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 1714
    :try_start_1
    new-instance v1, Ljava/io/FileInputStream;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-char v3, Lorg/threeten/bp/chrono/HijrahDate;->i:C

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Lorg/threeten/bp/chrono/HijrahDate;->k:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-char v3, Lorg/threeten/bp/chrono/HijrahDate;->i:C

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    move-object v0, v1

    goto :goto_0

    .line 1717
    :catch_1
    move-exception v0

    .line 1718
    throw v0

    .line 1724
    :cond_6
    :try_start_2
    new-instance v1, Ljava/util/zip/ZipFile;

    invoke-direct {v1, v4}, Ljava/util/zip/ZipFile;-><init>(Ljava/io/File;)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2

    move-object v4, v1

    .line 1729
    :goto_1
    if-eqz v4, :cond_5

    .line 1730
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v3, Lorg/threeten/bp/chrono/HijrahDate;->k:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-char v3, Lorg/threeten/bp/chrono/HijrahDate;->i:C

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1732
    invoke-virtual {v4, v1}, Ljava/util/zip/ZipFile;->getEntry(Ljava/lang/String;)Ljava/util/zip/ZipEntry;

    move-result-object v3

    .line 1734
    if-nez v3, :cond_a

    .line 1735
    sget-char v3, Lorg/threeten/bp/chrono/HijrahDate;->i:C

    if-ne v3, v7, :cond_8

    .line 1736
    invoke-virtual {v1, v7, v8}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object v1

    .line 1740
    :cond_7
    :goto_2
    invoke-virtual {v4, v1}, Ljava/util/zip/ZipFile;->getEntry(Ljava/lang/String;)Ljava/util/zip/ZipEntry;

    move-result-object v1

    .line 1743
    :goto_3
    if-eqz v1, :cond_5

    .line 1745
    :try_start_3
    invoke-virtual {v4, v1}, Ljava/util/zip/ZipFile;->getInputStream(Ljava/util/zip/ZipEntry;)Ljava/io/InputStream;
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_3

    move-result-object v0

    goto/16 :goto_0

    .line 1725
    :catch_2
    move-exception v1

    move-object v4, v2

    .line 1726
    goto :goto_1

    .line 1737
    :cond_8
    sget-char v3, Lorg/threeten/bp/chrono/HijrahDate;->i:C

    if-ne v3, v8, :cond_7

    .line 1738
    invoke-virtual {v1, v8, v7}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object v1

    goto :goto_2

    .line 1746
    :catch_3
    move-exception v0

    .line 1747
    throw v0

    :cond_9
    move-object v0, v2

    .line 1754
    goto/16 :goto_0

    :cond_a
    move-object v1, v3

    goto :goto_3
.end method

.method static h(J)Z
    .locals 6

    .prologue
    const-wide/16 v4, 0xb

    .line 1114
    const-wide/16 v0, 0xe

    const-wide/16 v2, 0x0

    cmp-long v2, p0, v2

    if-lez v2, :cond_0

    :goto_0
    mul-long v2, v4, p0

    add-long/2addr v0, v2

    const-wide/16 v2, 0x1e

    rem-long/2addr v0, v2

    cmp-long v0, v0, v4

    if-gez v0, :cond_1

    const/4 v0, 0x1

    :goto_1
    return v0

    :cond_0
    neg-long p0, p0

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private static h(I)[Ljava/lang/Integer;
    .locals 2

    .prologue
    .line 993
    :try_start_0
    sget-object v0, Lorg/threeten/bp/chrono/HijrahDate;->l:Ljava/util/HashMap;

    new-instance v1, Ljava/lang/Integer;

    invoke-direct {v1, p0}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/Integer;
    :try_end_0
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    .line 997
    :goto_0
    if-nez v0, :cond_0

    .line 998
    int-to-long v0, p0

    invoke-static {v0, v1}, Lorg/threeten/bp/chrono/HijrahDate;->h(J)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 999
    sget-object v0, Lorg/threeten/bp/chrono/HijrahDate;->t:[Ljava/lang/Integer;

    .line 1004
    :cond_0
    :goto_1
    return-object v0

    .line 994
    :catch_0
    move-exception v0

    .line 995
    const/4 v0, 0x0

    goto :goto_0

    .line 1001
    :cond_1
    sget-object v0, Lorg/threeten/bp/chrono/HijrahDate;->s:[Ljava/lang/Integer;

    goto :goto_1
.end method

.method private static i(J)[I
    .locals 12

    .prologue
    const/4 v6, 0x0

    .line 798
    const-wide/32 v0, -0x78274

    sub-long v8, p0, v0

    .line 800
    const-wide/16 v0, 0x0

    cmp-long v0, v8, v0

    if-ltz v0, :cond_0

    .line 801
    invoke-static {v8, v9}, Lorg/threeten/bp/chrono/HijrahDate;->j(J)I

    move-result v1

    .line 802
    invoke-static {v8, v9, v1}, Lorg/threeten/bp/chrono/HijrahDate;->a(JI)I

    move-result v0

    .line 803
    int-to-long v2, v0

    invoke-static {v1, v2, v3}, Lorg/threeten/bp/chrono/HijrahDate;->a(IJ)I

    move-result v2

    .line 804
    invoke-static {v1, v0, v2}, Lorg/threeten/bp/chrono/HijrahDate;->d(III)I

    move-result v0

    .line 806
    mul-int/lit8 v1, v1, 0x1e

    add-int/2addr v1, v2

    add-int/lit8 v3, v1, 0x1

    .line 807
    invoke-static {v0, v3}, Lorg/threeten/bp/chrono/HijrahDate;->b(II)I

    move-result v2

    .line 808
    invoke-static {v0, v2, v3}, Lorg/threeten/bp/chrono/HijrahDate;->e(III)I

    move-result v1

    .line 809
    add-int/lit8 v1, v1, 0x1

    .line 810
    sget-object v4, Lorg/threeten/bp/chrono/HijrahEra;->b:Lorg/threeten/bp/chrono/HijrahEra;

    invoke-virtual {v4}, Lorg/threeten/bp/chrono/HijrahEra;->a()I

    move-result v4

    .line 830
    :goto_0
    const-wide/16 v10, 0x5

    add-long/2addr v8, v10

    const-wide/16 v10, 0x7

    rem-long/2addr v8, v10

    long-to-int v7, v8

    .line 831
    if-gtz v7, :cond_3

    const/4 v5, 0x7

    :goto_1
    add-int/2addr v5, v7

    .line 833
    const/4 v7, 0x6

    new-array v7, v7, [I

    .line 834
    aput v4, v7, v6

    .line 835
    const/4 v4, 0x1

    aput v3, v7, v4

    .line 836
    const/4 v3, 0x2

    add-int/lit8 v2, v2, 0x1

    aput v2, v7, v3

    .line 837
    const/4 v2, 0x3

    aput v1, v7, v2

    .line 838
    const/4 v1, 0x4

    add-int/lit8 v0, v0, 0x1

    aput v0, v7, v1

    .line 839
    const/4 v0, 0x5

    aput v5, v7, v0

    .line 840
    return-object v7

    .line 812
    :cond_0
    long-to-int v0, v8

    div-int/lit16 v1, v0, 0x2987

    .line 813
    long-to-int v0, v8

    rem-int/lit16 v0, v0, 0x2987

    .line 814
    if-nez v0, :cond_1

    .line 815
    const/16 v0, -0x2987

    .line 816
    add-int/lit8 v1, v1, 0x1

    .line 818
    :cond_1
    int-to-long v2, v0

    invoke-static {v1, v2, v3}, Lorg/threeten/bp/chrono/HijrahDate;->a(IJ)I

    move-result v2

    .line 819
    invoke-static {v1, v0, v2}, Lorg/threeten/bp/chrono/HijrahDate;->d(III)I

    move-result v0

    .line 820
    mul-int/lit8 v1, v1, 0x1e

    sub-int/2addr v1, v2

    .line 821
    rsub-int/lit8 v3, v1, 0x1

    .line 822
    int-to-long v4, v3

    invoke-static {v4, v5}, Lorg/threeten/bp/chrono/HijrahDate;->h(J)Z

    move-result v1

    if-eqz v1, :cond_2

    add-int/lit16 v0, v0, 0x163

    .line 824
    :goto_2
    invoke-static {v0, v3}, Lorg/threeten/bp/chrono/HijrahDate;->b(II)I

    move-result v2

    .line 825
    invoke-static {v0, v2, v3}, Lorg/threeten/bp/chrono/HijrahDate;->e(III)I

    move-result v1

    .line 826
    add-int/lit8 v1, v1, 0x1

    .line 827
    sget-object v4, Lorg/threeten/bp/chrono/HijrahEra;->a:Lorg/threeten/bp/chrono/HijrahEra;

    invoke-virtual {v4}, Lorg/threeten/bp/chrono/HijrahEra;->a()I

    move-result v4

    goto :goto_0

    .line 822
    :cond_2
    add-int/lit16 v0, v0, 0x162

    goto :goto_2

    :cond_3
    move v5, v6

    .line 831
    goto :goto_1
.end method

.method private static i(I)[Ljava/lang/Integer;
    .locals 2

    .prologue
    .line 1016
    :try_start_0
    sget-object v0, Lorg/threeten/bp/chrono/HijrahDate;->m:Ljava/util/HashMap;

    new-instance v1, Ljava/lang/Integer;

    invoke-direct {v1, p0}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/Integer;
    :try_end_0
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1020
    :goto_0
    if-nez v0, :cond_0

    .line 1021
    int-to-long v0, p0

    invoke-static {v0, v1}, Lorg/threeten/bp/chrono/HijrahDate;->h(J)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1022
    sget-object v0, Lorg/threeten/bp/chrono/HijrahDate;->v:[Ljava/lang/Integer;

    .line 1027
    :cond_0
    :goto_1
    return-object v0

    .line 1017
    :catch_0
    move-exception v0

    .line 1018
    const/4 v0, 0x0

    goto :goto_0

    .line 1024
    :cond_1
    sget-object v0, Lorg/threeten/bp/chrono/HijrahDate;->u:[Ljava/lang/Integer;

    goto :goto_1
.end method

.method private static j(J)I
    .locals 4

    .prologue
    .line 897
    sget-object v1, Lorg/threeten/bp/chrono/HijrahDate;->o:[Ljava/lang/Long;

    .line 900
    const/4 v0, 0x0

    :goto_0
    :try_start_0
    array-length v2, v1

    if-ge v0, v2, :cond_1

    .line 901
    aget-object v2, v1, v0

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    cmp-long v2, p0, v2

    if-gez v2, :cond_0

    .line 902
    add-int/lit8 v0, v0, -0x1

    .line 909
    :goto_1
    return v0

    .line 900
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 905
    :cond_1
    long-to-int v0, p0

    div-int/lit16 v0, v0, 0x2987
    :try_end_0
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 906
    :catch_0
    move-exception v0

    .line 907
    long-to-int v0, p0

    div-int/lit16 v0, v0, 0x2987

    goto :goto_1
.end method

.method private readResolve()Ljava/lang/Object;
    .locals 4

    .prologue
    .line 612
    new-instance v0, Lorg/threeten/bp/chrono/HijrahDate;

    iget-wide v2, p0, Lorg/threeten/bp/chrono/HijrahDate;->D:J

    invoke-direct {v0, v2, v3}, Lorg/threeten/bp/chrono/HijrahDate;-><init>(J)V

    return-object v0
.end method

.method private writeReplace()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 1759
    new-instance v0, Lorg/threeten/bp/chrono/Ser;

    const/4 v1, 0x3

    invoke-direct {v0, v1, p0}, Lorg/threeten/bp/chrono/Ser;-><init>(BLjava/lang/Object;)V

    return-object v0
.end method


# virtual methods
.method public bridge synthetic a(Lorg/threeten/bp/temporal/Temporal;Lorg/threeten/bp/temporal/TemporalUnit;)J
    .locals 2

    .prologue
    .line 111
    invoke-super {p0, p1, p2}, Lorg/threeten/bp/chrono/ChronoDateImpl;->a(Lorg/threeten/bp/temporal/Temporal;Lorg/threeten/bp/temporal/TemporalUnit;)J

    move-result-wide v0

    return-wide v0
.end method

.method synthetic a(J)Lorg/threeten/bp/chrono/ChronoDateImpl;
    .locals 1

    .prologue
    .line 111
    invoke-virtual {p0, p1, p2}, Lorg/threeten/bp/chrono/HijrahDate;->e(J)Lorg/threeten/bp/chrono/HijrahDate;

    move-result-object v0

    return-object v0
.end method

.method public synthetic a(JLorg/threeten/bp/temporal/TemporalUnit;)Lorg/threeten/bp/chrono/ChronoDateImpl;
    .locals 1

    .prologue
    .line 111
    invoke-virtual {p0, p1, p2, p3}, Lorg/threeten/bp/chrono/HijrahDate;->b(JLorg/threeten/bp/temporal/TemporalUnit;)Lorg/threeten/bp/chrono/HijrahDate;

    move-result-object v0

    return-object v0
.end method

.method public a()Lorg/threeten/bp/chrono/HijrahChronology;
    .locals 1

    .prologue
    .line 618
    sget-object v0, Lorg/threeten/bp/chrono/HijrahChronology;->b:Lorg/threeten/bp/chrono/HijrahChronology;

    return-object v0
.end method

.method public a(Lorg/threeten/bp/temporal/TemporalAdjuster;)Lorg/threeten/bp/chrono/HijrahDate;
    .locals 1

    .prologue
    .line 669
    invoke-super {p0, p1}, Lorg/threeten/bp/chrono/ChronoDateImpl;->c(Lorg/threeten/bp/temporal/TemporalAdjuster;)Lorg/threeten/bp/chrono/ChronoLocalDate;

    move-result-object v0

    check-cast v0, Lorg/threeten/bp/chrono/HijrahDate;

    return-object v0
.end method

.method public a(Lorg/threeten/bp/temporal/TemporalAmount;)Lorg/threeten/bp/chrono/HijrahDate;
    .locals 1

    .prologue
    .line 707
    invoke-super {p0, p1}, Lorg/threeten/bp/chrono/ChronoDateImpl;->b(Lorg/threeten/bp/temporal/TemporalAmount;)Lorg/threeten/bp/chrono/ChronoLocalDate;

    move-result-object v0

    check-cast v0, Lorg/threeten/bp/chrono/HijrahDate;

    return-object v0
.end method

.method public a(Lorg/threeten/bp/temporal/TemporalField;J)Lorg/threeten/bp/chrono/HijrahDate;
    .locals 6

    .prologue
    const-wide/16 v4, 0x7

    .line 674
    instance-of v0, p1, Lorg/threeten/bp/temporal/ChronoField;

    if-eqz v0, :cond_1

    move-object v0, p1

    .line 675
    check-cast v0, Lorg/threeten/bp/temporal/ChronoField;

    .line 676
    invoke-virtual {v0, p2, p3}, Lorg/threeten/bp/temporal/ChronoField;->a(J)J

    .line 677
    long-to-int v1, p2

    .line 678
    sget-object v2, Lorg/threeten/bp/chrono/HijrahDate$1;->a:[I

    invoke-virtual {v0}, Lorg/threeten/bp/temporal/ChronoField;->ordinal()I

    move-result v0

    aget v0, v2, v0

    packed-switch v0, :pswitch_data_0

    .line 692
    new-instance v0, Lorg/threeten/bp/temporal/UnsupportedTemporalTypeException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unsupported field: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/threeten/bp/temporal/UnsupportedTemporalTypeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 679
    :pswitch_0
    iget-object v0, p0, Lorg/threeten/bp/chrono/HijrahDate;->C:Lorg/threeten/bp/DayOfWeek;

    invoke-virtual {v0}, Lorg/threeten/bp/DayOfWeek;->a()I

    move-result v0

    int-to-long v0, v0

    sub-long v0, p2, v0

    invoke-virtual {p0, v0, v1}, Lorg/threeten/bp/chrono/HijrahDate;->g(J)Lorg/threeten/bp/chrono/HijrahDate;

    move-result-object v0

    .line 694
    :goto_0
    return-object v0

    .line 680
    :pswitch_1
    sget-object v0, Lorg/threeten/bp/temporal/ChronoField;->q:Lorg/threeten/bp/temporal/ChronoField;

    invoke-virtual {p0, v0}, Lorg/threeten/bp/chrono/HijrahDate;->d(Lorg/threeten/bp/temporal/TemporalField;)J

    move-result-wide v0

    sub-long v0, p2, v0

    invoke-virtual {p0, v0, v1}, Lorg/threeten/bp/chrono/HijrahDate;->g(J)Lorg/threeten/bp/chrono/HijrahDate;

    move-result-object v0

    goto :goto_0

    .line 681
    :pswitch_2
    sget-object v0, Lorg/threeten/bp/temporal/ChronoField;->r:Lorg/threeten/bp/temporal/ChronoField;

    invoke-virtual {p0, v0}, Lorg/threeten/bp/chrono/HijrahDate;->d(Lorg/threeten/bp/temporal/TemporalField;)J

    move-result-wide v0

    sub-long v0, p2, v0

    invoke-virtual {p0, v0, v1}, Lorg/threeten/bp/chrono/HijrahDate;->g(J)Lorg/threeten/bp/chrono/HijrahDate;

    move-result-object v0

    goto :goto_0

    .line 682
    :pswitch_3
    iget v0, p0, Lorg/threeten/bp/chrono/HijrahDate;->y:I

    iget v2, p0, Lorg/threeten/bp/chrono/HijrahDate;->z:I

    invoke-static {v0, v2, v1}, Lorg/threeten/bp/chrono/HijrahDate;->b(III)Lorg/threeten/bp/chrono/HijrahDate;

    move-result-object v0

    goto :goto_0

    .line 683
    :pswitch_4
    iget v0, p0, Lorg/threeten/bp/chrono/HijrahDate;->y:I

    add-int/lit8 v2, v1, -0x1

    div-int/lit8 v2, v2, 0x1e

    add-int/lit8 v2, v2, 0x1

    add-int/lit8 v1, v1, -0x1

    rem-int/lit8 v1, v1, 0x1e

    add-int/lit8 v1, v1, 0x1

    invoke-static {v0, v2, v1}, Lorg/threeten/bp/chrono/HijrahDate;->b(III)Lorg/threeten/bp/chrono/HijrahDate;

    move-result-object v0

    goto :goto_0

    .line 684
    :pswitch_5
    new-instance v0, Lorg/threeten/bp/chrono/HijrahDate;

    int-to-long v2, v1

    invoke-direct {v0, v2, v3}, Lorg/threeten/bp/chrono/HijrahDate;-><init>(J)V

    goto :goto_0

    .line 685
    :pswitch_6
    sget-object v0, Lorg/threeten/bp/temporal/ChronoField;->v:Lorg/threeten/bp/temporal/ChronoField;

    invoke-virtual {p0, v0}, Lorg/threeten/bp/chrono/HijrahDate;->d(Lorg/threeten/bp/temporal/TemporalField;)J

    move-result-wide v0

    sub-long v0, p2, v0

    mul-long/2addr v0, v4

    invoke-virtual {p0, v0, v1}, Lorg/threeten/bp/chrono/HijrahDate;->g(J)Lorg/threeten/bp/chrono/HijrahDate;

    move-result-object v0

    goto :goto_0

    .line 686
    :pswitch_7
    sget-object v0, Lorg/threeten/bp/temporal/ChronoField;->w:Lorg/threeten/bp/temporal/ChronoField;

    invoke-virtual {p0, v0}, Lorg/threeten/bp/chrono/HijrahDate;->d(Lorg/threeten/bp/temporal/TemporalField;)J

    move-result-wide v0

    sub-long v0, p2, v0

    mul-long/2addr v0, v4

    invoke-virtual {p0, v0, v1}, Lorg/threeten/bp/chrono/HijrahDate;->g(J)Lorg/threeten/bp/chrono/HijrahDate;

    move-result-object v0

    goto :goto_0

    .line 687
    :pswitch_8
    iget v0, p0, Lorg/threeten/bp/chrono/HijrahDate;->y:I

    iget v2, p0, Lorg/threeten/bp/chrono/HijrahDate;->A:I

    invoke-static {v0, v1, v2}, Lorg/threeten/bp/chrono/HijrahDate;->b(III)Lorg/threeten/bp/chrono/HijrahDate;

    move-result-object v0

    goto :goto_0

    .line 688
    :pswitch_9
    iget v0, p0, Lorg/threeten/bp/chrono/HijrahDate;->y:I

    const/4 v2, 0x1

    if-lt v0, v2, :cond_0

    move v0, v1

    :goto_1
    iget v1, p0, Lorg/threeten/bp/chrono/HijrahDate;->z:I

    iget v2, p0, Lorg/threeten/bp/chrono/HijrahDate;->A:I

    invoke-static {v0, v1, v2}, Lorg/threeten/bp/chrono/HijrahDate;->b(III)Lorg/threeten/bp/chrono/HijrahDate;

    move-result-object v0

    goto :goto_0

    :cond_0
    rsub-int/lit8 v0, v1, 0x1

    goto :goto_1

    .line 689
    :pswitch_a
    iget v0, p0, Lorg/threeten/bp/chrono/HijrahDate;->z:I

    iget v2, p0, Lorg/threeten/bp/chrono/HijrahDate;->A:I

    invoke-static {v1, v0, v2}, Lorg/threeten/bp/chrono/HijrahDate;->b(III)Lorg/threeten/bp/chrono/HijrahDate;

    move-result-object v0

    goto :goto_0

    .line 690
    :pswitch_b
    iget v0, p0, Lorg/threeten/bp/chrono/HijrahDate;->y:I

    rsub-int/lit8 v0, v0, 0x1

    iget v1, p0, Lorg/threeten/bp/chrono/HijrahDate;->z:I

    iget v2, p0, Lorg/threeten/bp/chrono/HijrahDate;->A:I

    invoke-static {v0, v1, v2}, Lorg/threeten/bp/chrono/HijrahDate;->b(III)Lorg/threeten/bp/chrono/HijrahDate;

    move-result-object v0

    goto/16 :goto_0

    .line 694
    :cond_1
    invoke-interface {p1, p0, p2, p3}, Lorg/threeten/bp/temporal/TemporalField;->a(Lorg/threeten/bp/temporal/Temporal;J)Lorg/threeten/bp/temporal/Temporal;

    move-result-object v0

    check-cast v0, Lorg/threeten/bp/chrono/HijrahDate;

    goto/16 :goto_0

    .line 678
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_3
        :pswitch_4
        :pswitch_6
        :pswitch_9
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_5
        :pswitch_7
        :pswitch_8
        :pswitch_a
        :pswitch_b
    .end packed-switch
.end method

.method a(Ljava/io/DataOutput;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1764
    sget-object v0, Lorg/threeten/bp/temporal/ChronoField;->A:Lorg/threeten/bp/temporal/ChronoField;

    invoke-virtual {p0, v0}, Lorg/threeten/bp/chrono/HijrahDate;->c(Lorg/threeten/bp/temporal/TemporalField;)I

    move-result v0

    invoke-interface {p1, v0}, Ljava/io/DataOutput;->writeInt(I)V

    .line 1765
    sget-object v0, Lorg/threeten/bp/temporal/ChronoField;->x:Lorg/threeten/bp/temporal/ChronoField;

    invoke-virtual {p0, v0}, Lorg/threeten/bp/chrono/HijrahDate;->c(Lorg/threeten/bp/temporal/TemporalField;)I

    move-result v0

    invoke-interface {p1, v0}, Ljava/io/DataOutput;->writeByte(I)V

    .line 1766
    sget-object v0, Lorg/threeten/bp/temporal/ChronoField;->s:Lorg/threeten/bp/temporal/ChronoField;

    invoke-virtual {p0, v0}, Lorg/threeten/bp/chrono/HijrahDate;->c(Lorg/threeten/bp/temporal/TemporalField;)I

    move-result v0

    invoke-interface {p1, v0}, Ljava/io/DataOutput;->writeByte(I)V

    .line 1767
    return-void
.end method

.method synthetic b(J)Lorg/threeten/bp/chrono/ChronoDateImpl;
    .locals 1

    .prologue
    .line 111
    invoke-virtual {p0, p1, p2}, Lorg/threeten/bp/chrono/HijrahDate;->f(J)Lorg/threeten/bp/chrono/HijrahDate;

    move-result-object v0

    return-object v0
.end method

.method public synthetic b(Lorg/threeten/bp/temporal/TemporalAmount;)Lorg/threeten/bp/chrono/ChronoLocalDate;
    .locals 1

    .prologue
    .line 111
    invoke-virtual {p0, p1}, Lorg/threeten/bp/chrono/HijrahDate;->a(Lorg/threeten/bp/temporal/TemporalAmount;)Lorg/threeten/bp/chrono/HijrahDate;

    move-result-object v0

    return-object v0
.end method

.method public final b(Lorg/threeten/bp/LocalTime;)Lorg/threeten/bp/chrono/ChronoLocalDateTime;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/threeten/bp/LocalTime;",
            ")",
            "Lorg/threeten/bp/chrono/ChronoLocalDateTime",
            "<",
            "Lorg/threeten/bp/chrono/HijrahDate;",
            ">;"
        }
    .end annotation

    .prologue
    .line 729
    invoke-super {p0, p1}, Lorg/threeten/bp/chrono/ChronoDateImpl;->b(Lorg/threeten/bp/LocalTime;)Lorg/threeten/bp/chrono/ChronoLocalDateTime;

    move-result-object v0

    return-object v0
.end method

.method public b(JLorg/threeten/bp/temporal/TemporalUnit;)Lorg/threeten/bp/chrono/HijrahDate;
    .locals 1

    .prologue
    .line 712
    invoke-super {p0, p1, p2, p3}, Lorg/threeten/bp/chrono/ChronoDateImpl;->a(JLorg/threeten/bp/temporal/TemporalUnit;)Lorg/threeten/bp/chrono/ChronoDateImpl;

    move-result-object v0

    check-cast v0, Lorg/threeten/bp/chrono/HijrahDate;

    return-object v0
.end method

.method public b()Lorg/threeten/bp/chrono/HijrahEra;
    .locals 1

    .prologue
    .line 623
    iget-object v0, p0, Lorg/threeten/bp/chrono/HijrahDate;->x:Lorg/threeten/bp/chrono/HijrahEra;

    return-object v0
.end method

.method public synthetic b(Lorg/threeten/bp/temporal/TemporalAdjuster;)Lorg/threeten/bp/temporal/Temporal;
    .locals 1

    .prologue
    .line 111
    invoke-virtual {p0, p1}, Lorg/threeten/bp/chrono/HijrahDate;->a(Lorg/threeten/bp/temporal/TemporalAdjuster;)Lorg/threeten/bp/chrono/HijrahDate;

    move-result-object v0

    return-object v0
.end method

.method public synthetic b(Lorg/threeten/bp/temporal/TemporalField;J)Lorg/threeten/bp/temporal/Temporal;
    .locals 2

    .prologue
    .line 111
    invoke-virtual {p0, p1, p2, p3}, Lorg/threeten/bp/chrono/HijrahDate;->a(Lorg/threeten/bp/temporal/TemporalField;J)Lorg/threeten/bp/chrono/HijrahDate;

    move-result-object v0

    return-object v0
.end method

.method public b(Lorg/threeten/bp/temporal/TemporalField;)Lorg/threeten/bp/temporal/ValueRange;
    .locals 4

    .prologue
    const-wide/16 v2, 0x1

    .line 628
    instance-of v0, p1, Lorg/threeten/bp/temporal/ChronoField;

    if-eqz v0, :cond_1

    .line 629
    invoke-virtual {p0, p1}, Lorg/threeten/bp/chrono/HijrahDate;->a(Lorg/threeten/bp/temporal/TemporalField;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 630
    check-cast p1, Lorg/threeten/bp/temporal/ChronoField;

    .line 631
    sget-object v0, Lorg/threeten/bp/chrono/HijrahDate$1;->a:[I

    invoke-virtual {p1}, Lorg/threeten/bp/temporal/ChronoField;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 637
    invoke-virtual {p0}, Lorg/threeten/bp/chrono/HijrahDate;->a()Lorg/threeten/bp/chrono/HijrahChronology;

    move-result-object v0

    invoke-virtual {v0, p1}, Lorg/threeten/bp/chrono/HijrahChronology;->a(Lorg/threeten/bp/temporal/ChronoField;)Lorg/threeten/bp/temporal/ValueRange;

    move-result-object v0

    .line 641
    :goto_0
    return-object v0

    .line 632
    :pswitch_0
    invoke-virtual {p0}, Lorg/threeten/bp/chrono/HijrahDate;->d()I

    move-result v0

    int-to-long v0, v0

    invoke-static {v2, v3, v0, v1}, Lorg/threeten/bp/temporal/ValueRange;->a(JJ)Lorg/threeten/bp/temporal/ValueRange;

    move-result-object v0

    goto :goto_0

    .line 633
    :pswitch_1
    invoke-virtual {p0}, Lorg/threeten/bp/chrono/HijrahDate;->l()I

    move-result v0

    int-to-long v0, v0

    invoke-static {v2, v3, v0, v1}, Lorg/threeten/bp/temporal/ValueRange;->a(JJ)Lorg/threeten/bp/temporal/ValueRange;

    move-result-object v0

    goto :goto_0

    .line 634
    :pswitch_2
    const-wide/16 v0, 0x5

    invoke-static {v2, v3, v0, v1}, Lorg/threeten/bp/temporal/ValueRange;->a(JJ)Lorg/threeten/bp/temporal/ValueRange;

    move-result-object v0

    goto :goto_0

    .line 635
    :pswitch_3
    const-wide/16 v0, 0x3e8

    invoke-static {v2, v3, v0, v1}, Lorg/threeten/bp/temporal/ValueRange;->a(JJ)Lorg/threeten/bp/temporal/ValueRange;

    move-result-object v0

    goto :goto_0

    .line 639
    :cond_0
    new-instance v0, Lorg/threeten/bp/temporal/UnsupportedTemporalTypeException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unsupported field: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/threeten/bp/temporal/UnsupportedTemporalTypeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 641
    :cond_1
    invoke-interface {p1, p0}, Lorg/threeten/bp/temporal/TemporalField;->b(Lorg/threeten/bp/temporal/TemporalAccessor;)Lorg/threeten/bp/temporal/ValueRange;

    move-result-object v0

    goto :goto_0

    .line 631
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method synthetic c(J)Lorg/threeten/bp/chrono/ChronoDateImpl;
    .locals 1

    .prologue
    .line 111
    invoke-virtual {p0, p1, p2}, Lorg/threeten/bp/chrono/HijrahDate;->g(J)Lorg/threeten/bp/chrono/HijrahDate;

    move-result-object v0

    return-object v0
.end method

.method public synthetic c(Lorg/threeten/bp/temporal/TemporalAdjuster;)Lorg/threeten/bp/chrono/ChronoLocalDate;
    .locals 1

    .prologue
    .line 111
    invoke-virtual {p0, p1}, Lorg/threeten/bp/chrono/HijrahDate;->a(Lorg/threeten/bp/temporal/TemporalAdjuster;)Lorg/threeten/bp/chrono/HijrahDate;

    move-result-object v0

    return-object v0
.end method

.method public synthetic c(Lorg/threeten/bp/temporal/TemporalField;J)Lorg/threeten/bp/chrono/ChronoLocalDate;
    .locals 2

    .prologue
    .line 111
    invoke-virtual {p0, p1, p2, p3}, Lorg/threeten/bp/chrono/HijrahDate;->a(Lorg/threeten/bp/temporal/TemporalField;J)Lorg/threeten/bp/chrono/HijrahDate;

    move-result-object v0

    return-object v0
.end method

.method public synthetic c()Lorg/threeten/bp/chrono/Era;
    .locals 1

    .prologue
    .line 111
    invoke-virtual {p0}, Lorg/threeten/bp/chrono/HijrahDate;->b()Lorg/threeten/bp/chrono/HijrahEra;

    move-result-object v0

    return-object v0
.end method

.method public synthetic c(JLorg/threeten/bp/temporal/TemporalUnit;)Lorg/threeten/bp/temporal/Temporal;
    .locals 1

    .prologue
    .line 111
    invoke-virtual {p0, p1, p2, p3}, Lorg/threeten/bp/chrono/HijrahDate;->g(JLorg/threeten/bp/temporal/TemporalUnit;)Lorg/threeten/bp/chrono/HijrahDate;

    move-result-object v0

    return-object v0
.end method

.method public synthetic c(Lorg/threeten/bp/temporal/TemporalAmount;)Lorg/threeten/bp/temporal/Temporal;
    .locals 1

    .prologue
    .line 111
    invoke-virtual {p0, p1}, Lorg/threeten/bp/chrono/HijrahDate;->a(Lorg/threeten/bp/temporal/TemporalAmount;)Lorg/threeten/bp/chrono/HijrahDate;

    move-result-object v0

    return-object v0
.end method

.method public d()I
    .locals 2

    .prologue
    .line 1143
    iget v0, p0, Lorg/threeten/bp/chrono/HijrahDate;->z:I

    add-int/lit8 v0, v0, -0x1

    iget v1, p0, Lorg/threeten/bp/chrono/HijrahDate;->y:I

    invoke-static {v0, v1}, Lorg/threeten/bp/chrono/HijrahDate;->a(II)I

    move-result v0

    return v0
.end method

.method public d(Lorg/threeten/bp/temporal/TemporalField;)J
    .locals 3

    .prologue
    .line 646
    instance-of v0, p1, Lorg/threeten/bp/temporal/ChronoField;

    if-eqz v0, :cond_0

    .line 647
    sget-object v1, Lorg/threeten/bp/chrono/HijrahDate$1;->a:[I

    move-object v0, p1

    check-cast v0, Lorg/threeten/bp/temporal/ChronoField;

    invoke-virtual {v0}, Lorg/threeten/bp/temporal/ChronoField;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 661
    new-instance v0, Lorg/threeten/bp/temporal/UnsupportedTemporalTypeException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unsupported field: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/threeten/bp/temporal/UnsupportedTemporalTypeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 648
    :pswitch_0
    iget-object v0, p0, Lorg/threeten/bp/chrono/HijrahDate;->C:Lorg/threeten/bp/DayOfWeek;

    invoke-virtual {v0}, Lorg/threeten/bp/DayOfWeek;->a()I

    move-result v0

    int-to-long v0, v0

    .line 663
    :goto_0
    return-wide v0

    .line 649
    :pswitch_1
    iget v0, p0, Lorg/threeten/bp/chrono/HijrahDate;->A:I

    add-int/lit8 v0, v0, -0x1

    rem-int/lit8 v0, v0, 0x7

    add-int/lit8 v0, v0, 0x1

    int-to-long v0, v0

    goto :goto_0

    .line 650
    :pswitch_2
    iget v0, p0, Lorg/threeten/bp/chrono/HijrahDate;->B:I

    add-int/lit8 v0, v0, -0x1

    rem-int/lit8 v0, v0, 0x7

    add-int/lit8 v0, v0, 0x1

    int-to-long v0, v0

    goto :goto_0

    .line 651
    :pswitch_3
    iget v0, p0, Lorg/threeten/bp/chrono/HijrahDate;->A:I

    int-to-long v0, v0

    goto :goto_0

    .line 652
    :pswitch_4
    iget v0, p0, Lorg/threeten/bp/chrono/HijrahDate;->B:I

    int-to-long v0, v0

    goto :goto_0

    .line 653
    :pswitch_5
    invoke-virtual {p0}, Lorg/threeten/bp/chrono/HijrahDate;->n()J

    move-result-wide v0

    goto :goto_0

    .line 654
    :pswitch_6
    iget v0, p0, Lorg/threeten/bp/chrono/HijrahDate;->A:I

    add-int/lit8 v0, v0, -0x1

    div-int/lit8 v0, v0, 0x7

    add-int/lit8 v0, v0, 0x1

    int-to-long v0, v0

    goto :goto_0

    .line 655
    :pswitch_7
    iget v0, p0, Lorg/threeten/bp/chrono/HijrahDate;->B:I

    add-int/lit8 v0, v0, -0x1

    div-int/lit8 v0, v0, 0x7

    add-int/lit8 v0, v0, 0x1

    int-to-long v0, v0

    goto :goto_0

    .line 656
    :pswitch_8
    iget v0, p0, Lorg/threeten/bp/chrono/HijrahDate;->z:I

    int-to-long v0, v0

    goto :goto_0

    .line 657
    :pswitch_9
    iget v0, p0, Lorg/threeten/bp/chrono/HijrahDate;->y:I

    int-to-long v0, v0

    goto :goto_0

    .line 658
    :pswitch_a
    iget v0, p0, Lorg/threeten/bp/chrono/HijrahDate;->y:I

    int-to-long v0, v0

    goto :goto_0

    .line 659
    :pswitch_b
    iget-object v0, p0, Lorg/threeten/bp/chrono/HijrahDate;->x:Lorg/threeten/bp/chrono/HijrahEra;

    invoke-virtual {v0}, Lorg/threeten/bp/chrono/HijrahEra;->a()I

    move-result v0

    int-to-long v0, v0

    goto :goto_0

    .line 663
    :cond_0
    invoke-interface {p1, p0}, Lorg/threeten/bp/temporal/TemporalField;->c(Lorg/threeten/bp/temporal/TemporalAccessor;)J

    move-result-wide v0

    goto :goto_0

    .line 647
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_3
        :pswitch_4
        :pswitch_6
        :pswitch_9
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_5
        :pswitch_7
        :pswitch_8
        :pswitch_a
        :pswitch_b
    .end packed-switch
.end method

.method public synthetic d(JLorg/threeten/bp/temporal/TemporalUnit;)Lorg/threeten/bp/temporal/Temporal;
    .locals 1

    .prologue
    .line 111
    invoke-virtual {p0, p1, p2, p3}, Lorg/threeten/bp/chrono/HijrahDate;->b(JLorg/threeten/bp/temporal/TemporalUnit;)Lorg/threeten/bp/chrono/HijrahDate;

    move-result-object v0

    return-object v0
.end method

.method public synthetic e(JLorg/threeten/bp/temporal/TemporalUnit;)Lorg/threeten/bp/chrono/ChronoLocalDate;
    .locals 1

    .prologue
    .line 111
    invoke-virtual {p0, p1, p2, p3}, Lorg/threeten/bp/chrono/HijrahDate;->g(JLorg/threeten/bp/temporal/TemporalUnit;)Lorg/threeten/bp/chrono/HijrahDate;

    move-result-object v0

    return-object v0
.end method

.method e(J)Lorg/threeten/bp/chrono/HijrahDate;
    .locals 5

    .prologue
    .line 751
    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-nez v0, :cond_0

    .line 755
    :goto_0
    return-object p0

    .line 754
    :cond_0
    iget v0, p0, Lorg/threeten/bp/chrono/HijrahDate;->y:I

    long-to-int v1, p1

    invoke-static {v0, v1}, Lorg/threeten/bp/jdk8/Jdk8Methods;->b(II)I

    move-result v0

    .line 755
    iget-object v1, p0, Lorg/threeten/bp/chrono/HijrahDate;->x:Lorg/threeten/bp/chrono/HijrahEra;

    iget v2, p0, Lorg/threeten/bp/chrono/HijrahDate;->z:I

    iget v3, p0, Lorg/threeten/bp/chrono/HijrahDate;->A:I

    invoke-static {v1, v0, v2, v3}, Lorg/threeten/bp/chrono/HijrahDate;->a(Lorg/threeten/bp/chrono/HijrahEra;III)Lorg/threeten/bp/chrono/HijrahDate;

    move-result-object p0

    goto :goto_0
.end method

.method public synthetic f(JLorg/threeten/bp/temporal/TemporalUnit;)Lorg/threeten/bp/chrono/ChronoLocalDate;
    .locals 1

    .prologue
    .line 111
    invoke-virtual {p0, p1, p2, p3}, Lorg/threeten/bp/chrono/HijrahDate;->b(JLorg/threeten/bp/temporal/TemporalUnit;)Lorg/threeten/bp/chrono/HijrahDate;

    move-result-object v0

    return-object v0
.end method

.method f(J)Lorg/threeten/bp/chrono/HijrahDate;
    .locals 5

    .prologue
    .line 760
    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-nez v0, :cond_0

    .line 772
    :goto_0
    return-object p0

    .line 763
    :cond_0
    iget v0, p0, Lorg/threeten/bp/chrono/HijrahDate;->z:I

    add-int/lit8 v0, v0, -0x1

    .line 764
    long-to-int v1, p1

    add-int/2addr v1, v0

    .line 765
    div-int/lit8 v0, v1, 0xc

    .line 766
    rem-int/lit8 v1, v1, 0xc

    .line 767
    :goto_1
    if-gez v1, :cond_1

    .line 768
    add-int/lit8 v1, v1, 0xc

    .line 769
    const/4 v2, 0x1

    invoke-static {v0, v2}, Lorg/threeten/bp/jdk8/Jdk8Methods;->c(II)I

    move-result v0

    goto :goto_1

    .line 771
    :cond_1
    iget v2, p0, Lorg/threeten/bp/chrono/HijrahDate;->y:I

    invoke-static {v2, v0}, Lorg/threeten/bp/jdk8/Jdk8Methods;->b(II)I

    move-result v0

    .line 772
    iget-object v2, p0, Lorg/threeten/bp/chrono/HijrahDate;->x:Lorg/threeten/bp/chrono/HijrahEra;

    add-int/lit8 v1, v1, 0x1

    iget v3, p0, Lorg/threeten/bp/chrono/HijrahDate;->A:I

    invoke-static {v2, v0, v1, v3}, Lorg/threeten/bp/chrono/HijrahDate;->a(Lorg/threeten/bp/chrono/HijrahEra;III)Lorg/threeten/bp/chrono/HijrahDate;

    move-result-object p0

    goto :goto_0
.end method

.method g(J)Lorg/threeten/bp/chrono/HijrahDate;
    .locals 5

    .prologue
    .line 777
    new-instance v0, Lorg/threeten/bp/chrono/HijrahDate;

    iget-wide v2, p0, Lorg/threeten/bp/chrono/HijrahDate;->D:J

    add-long/2addr v2, p1

    invoke-direct {v0, v2, v3}, Lorg/threeten/bp/chrono/HijrahDate;-><init>(J)V

    return-object v0
.end method

.method public g(JLorg/threeten/bp/temporal/TemporalUnit;)Lorg/threeten/bp/chrono/HijrahDate;
    .locals 1

    .prologue
    .line 722
    invoke-super {p0, p1, p2, p3}, Lorg/threeten/bp/chrono/ChronoDateImpl;->e(JLorg/threeten/bp/temporal/TemporalUnit;)Lorg/threeten/bp/chrono/ChronoLocalDate;

    move-result-object v0

    check-cast v0, Lorg/threeten/bp/chrono/HijrahDate;

    return-object v0
.end method

.method public j()Z
    .locals 1

    .prologue
    .line 745
    iget-boolean v0, p0, Lorg/threeten/bp/chrono/HijrahDate;->E:Z

    return v0
.end method

.method public l()I
    .locals 1

    .prologue
    .line 1177
    iget v0, p0, Lorg/threeten/bp/chrono/HijrahDate;->y:I

    invoke-static {v0}, Lorg/threeten/bp/chrono/HijrahDate;->a(I)I

    move-result v0

    return v0
.end method

.method public n()J
    .locals 3

    .prologue
    .line 734
    iget v0, p0, Lorg/threeten/bp/chrono/HijrahDate;->y:I

    iget v1, p0, Lorg/threeten/bp/chrono/HijrahDate;->z:I

    iget v2, p0, Lorg/threeten/bp/chrono/HijrahDate;->A:I

    invoke-static {v0, v1, v2}, Lorg/threeten/bp/chrono/HijrahDate;->c(III)J

    move-result-wide v0

    return-wide v0
.end method

.method public synthetic o()Lorg/threeten/bp/chrono/Chronology;
    .locals 1

    .prologue
    .line 111
    invoke-virtual {p0}, Lorg/threeten/bp/chrono/HijrahDate;->a()Lorg/threeten/bp/chrono/HijrahChronology;

    move-result-object v0

    return-object v0
.end method
