.class final Lorg/threeten/bp/chrono/Ser;
.super Ljava/lang/Object;
.source "Ser.java"

# interfaces
.implements Ljava/io/Externalizable;


# instance fields
.field private a:B

.field private b:Ljava/lang/Object;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 93
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 94
    return-void
.end method

.method constructor <init>(BLjava/lang/Object;)V
    .locals 0

    .prologue
    .line 102
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 103
    iput-byte p1, p0, Lorg/threeten/bp/chrono/Ser;->a:B

    .line 104
    iput-object p2, p0, Lorg/threeten/bp/chrono/Ser;->b:Ljava/lang/Object;

    .line 105
    return-void
.end method

.method private static a(BLjava/io/ObjectInput;)Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 177
    packed-switch p0, :pswitch_data_0

    .line 190
    :pswitch_0
    new-instance v0, Ljava/io/StreamCorruptedException;

    const-string v1, "Unknown serialized type"

    invoke-direct {v0, v1}, Ljava/io/StreamCorruptedException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 178
    :pswitch_1
    invoke-static {p1}, Lorg/threeten/bp/chrono/JapaneseDate;->a(Ljava/io/DataInput;)Lorg/threeten/bp/chrono/ChronoLocalDate;

    move-result-object v0

    .line 188
    :goto_0
    return-object v0

    .line 179
    :pswitch_2
    invoke-static {p1}, Lorg/threeten/bp/chrono/JapaneseEra;->a(Ljava/io/DataInput;)Lorg/threeten/bp/chrono/JapaneseEra;

    move-result-object v0

    goto :goto_0

    .line 180
    :pswitch_3
    invoke-static {p1}, Lorg/threeten/bp/chrono/HijrahDate;->a(Ljava/io/DataInput;)Lorg/threeten/bp/chrono/ChronoLocalDate;

    move-result-object v0

    goto :goto_0

    .line 181
    :pswitch_4
    invoke-static {p1}, Lorg/threeten/bp/chrono/HijrahEra;->a(Ljava/io/DataInput;)Lorg/threeten/bp/chrono/HijrahEra;

    move-result-object v0

    goto :goto_0

    .line 182
    :pswitch_5
    invoke-static {p1}, Lorg/threeten/bp/chrono/MinguoDate;->a(Ljava/io/DataInput;)Lorg/threeten/bp/chrono/ChronoLocalDate;

    move-result-object v0

    goto :goto_0

    .line 183
    :pswitch_6
    invoke-static {p1}, Lorg/threeten/bp/chrono/MinguoEra;->a(Ljava/io/DataInput;)Lorg/threeten/bp/chrono/MinguoEra;

    move-result-object v0

    goto :goto_0

    .line 184
    :pswitch_7
    invoke-static {p1}, Lorg/threeten/bp/chrono/ThaiBuddhistDate;->a(Ljava/io/DataInput;)Lorg/threeten/bp/chrono/ChronoLocalDate;

    move-result-object v0

    goto :goto_0

    .line 185
    :pswitch_8
    invoke-static {p1}, Lorg/threeten/bp/chrono/ThaiBuddhistEra;->a(Ljava/io/DataInput;)Lorg/threeten/bp/chrono/ThaiBuddhistEra;

    move-result-object v0

    goto :goto_0

    .line 186
    :pswitch_9
    invoke-static {p1}, Lorg/threeten/bp/chrono/Chronology;->a(Ljava/io/DataInput;)Lorg/threeten/bp/chrono/Chronology;

    move-result-object v0

    goto :goto_0

    .line 187
    :pswitch_a
    invoke-static {p1}, Lorg/threeten/bp/chrono/ChronoLocalDateTimeImpl;->a(Ljava/io/ObjectInput;)Lorg/threeten/bp/chrono/ChronoLocalDateTime;

    move-result-object v0

    goto :goto_0

    .line 188
    :pswitch_b
    invoke-static {p1}, Lorg/threeten/bp/chrono/ChronoZonedDateTimeImpl;->a(Ljava/io/ObjectInput;)Lorg/threeten/bp/chrono/ChronoZonedDateTime;

    move-result-object v0

    goto :goto_0

    .line 177
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_0
        :pswitch_0
        :pswitch_9
        :pswitch_a
        :pswitch_b
    .end packed-switch
.end method

.method private static a(BLjava/lang/Object;Ljava/io/ObjectOutput;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 119
    invoke-interface {p2, p0}, Ljava/io/ObjectOutput;->writeByte(I)V

    .line 120
    packed-switch p0, :pswitch_data_0

    .line 155
    :pswitch_0
    new-instance v0, Ljava/io/InvalidClassException;

    const-string v1, "Unknown serialized type"

    invoke-direct {v0, v1}, Ljava/io/InvalidClassException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 122
    :pswitch_1
    check-cast p1, Lorg/threeten/bp/chrono/JapaneseDate;

    invoke-virtual {p1, p2}, Lorg/threeten/bp/chrono/JapaneseDate;->a(Ljava/io/DataOutput;)V

    .line 157
    :goto_0
    return-void

    .line 125
    :pswitch_2
    check-cast p1, Lorg/threeten/bp/chrono/JapaneseEra;

    invoke-virtual {p1, p2}, Lorg/threeten/bp/chrono/JapaneseEra;->a(Ljava/io/DataOutput;)V

    goto :goto_0

    .line 128
    :pswitch_3
    check-cast p1, Lorg/threeten/bp/chrono/HijrahDate;

    invoke-virtual {p1, p2}, Lorg/threeten/bp/chrono/HijrahDate;->a(Ljava/io/DataOutput;)V

    goto :goto_0

    .line 131
    :pswitch_4
    check-cast p1, Lorg/threeten/bp/chrono/HijrahEra;

    invoke-virtual {p1, p2}, Lorg/threeten/bp/chrono/HijrahEra;->a(Ljava/io/DataOutput;)V

    goto :goto_0

    .line 134
    :pswitch_5
    check-cast p1, Lorg/threeten/bp/chrono/MinguoDate;

    invoke-virtual {p1, p2}, Lorg/threeten/bp/chrono/MinguoDate;->a(Ljava/io/DataOutput;)V

    goto :goto_0

    .line 137
    :pswitch_6
    check-cast p1, Lorg/threeten/bp/chrono/MinguoEra;

    invoke-virtual {p1, p2}, Lorg/threeten/bp/chrono/MinguoEra;->a(Ljava/io/DataOutput;)V

    goto :goto_0

    .line 140
    :pswitch_7
    check-cast p1, Lorg/threeten/bp/chrono/ThaiBuddhistDate;

    invoke-virtual {p1, p2}, Lorg/threeten/bp/chrono/ThaiBuddhistDate;->a(Ljava/io/DataOutput;)V

    goto :goto_0

    .line 143
    :pswitch_8
    check-cast p1, Lorg/threeten/bp/chrono/ThaiBuddhistEra;

    invoke-virtual {p1, p2}, Lorg/threeten/bp/chrono/ThaiBuddhistEra;->a(Ljava/io/DataOutput;)V

    goto :goto_0

    .line 146
    :pswitch_9
    check-cast p1, Lorg/threeten/bp/chrono/Chronology;

    invoke-virtual {p1, p2}, Lorg/threeten/bp/chrono/Chronology;->a(Ljava/io/DataOutput;)V

    goto :goto_0

    .line 149
    :pswitch_a
    check-cast p1, Lorg/threeten/bp/chrono/ChronoLocalDateTimeImpl;

    invoke-virtual {p1, p2}, Lorg/threeten/bp/chrono/ChronoLocalDateTimeImpl;->a(Ljava/io/ObjectOutput;)V

    goto :goto_0

    .line 152
    :pswitch_b
    check-cast p1, Lorg/threeten/bp/chrono/ChronoZonedDateTimeImpl;

    invoke-virtual {p1, p2}, Lorg/threeten/bp/chrono/ChronoZonedDateTimeImpl;->a(Ljava/io/ObjectOutput;)V

    goto :goto_0

    .line 120
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_0
        :pswitch_0
        :pswitch_9
        :pswitch_a
        :pswitch_b
    .end packed-switch
.end method

.method private readResolve()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 200
    iget-object v0, p0, Lorg/threeten/bp/chrono/Ser;->b:Ljava/lang/Object;

    return-object v0
.end method


# virtual methods
.method public readExternal(Ljava/io/ObjectInput;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 167
    invoke-interface {p1}, Ljava/io/ObjectInput;->readByte()B

    move-result v0

    iput-byte v0, p0, Lorg/threeten/bp/chrono/Ser;->a:B

    .line 168
    iget-byte v0, p0, Lorg/threeten/bp/chrono/Ser;->a:B

    invoke-static {v0, p1}, Lorg/threeten/bp/chrono/Ser;->a(BLjava/io/ObjectInput;)Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Lorg/threeten/bp/chrono/Ser;->b:Ljava/lang/Object;

    .line 169
    return-void
.end method

.method public writeExternal(Ljava/io/ObjectOutput;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 115
    iget-byte v0, p0, Lorg/threeten/bp/chrono/Ser;->a:B

    iget-object v1, p0, Lorg/threeten/bp/chrono/Ser;->b:Ljava/lang/Object;

    invoke-static {v0, v1, p1}, Lorg/threeten/bp/chrono/Ser;->a(BLjava/lang/Object;Ljava/io/ObjectOutput;)V

    .line 116
    return-void
.end method
