.class public final Lorg/threeten/bp/chrono/JapaneseDate;
.super Lorg/threeten/bp/chrono/ChronoDateImpl;
.source "JapaneseDate.java"

# interfaces
.implements Ljava/io/Serializable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/threeten/bp/chrono/JapaneseDate$1;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lorg/threeten/bp/chrono/ChronoDateImpl",
        "<",
        "Lorg/threeten/bp/chrono/JapaneseDate;",
        ">;",
        "Ljava/io/Serializable;"
    }
.end annotation


# static fields
.field static final a:Lorg/threeten/bp/LocalDate;


# instance fields
.field private final b:Lorg/threeten/bp/LocalDate;

.field private transient c:Lorg/threeten/bp/chrono/JapaneseEra;

.field private transient d:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 95
    const/16 v0, 0x751

    invoke-static {v0, v1, v1}, Lorg/threeten/bp/LocalDate;->a(III)Lorg/threeten/bp/LocalDate;

    move-result-object v0

    sput-object v0, Lorg/threeten/bp/chrono/JapaneseDate;->a:Lorg/threeten/bp/LocalDate;

    return-void
.end method

.method constructor <init>(Lorg/threeten/bp/LocalDate;)V
    .locals 2

    .prologue
    .line 282
    invoke-direct {p0}, Lorg/threeten/bp/chrono/ChronoDateImpl;-><init>()V

    .line 283
    sget-object v0, Lorg/threeten/bp/chrono/JapaneseDate;->a:Lorg/threeten/bp/LocalDate;

    invoke-virtual {p1, v0}, Lorg/threeten/bp/LocalDate;->c(Lorg/threeten/bp/chrono/ChronoLocalDate;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 284
    new-instance v0, Lorg/threeten/bp/DateTimeException;

    const-string v1, "Minimum supported date is January 1st Meiji 6"

    invoke-direct {v0, v1}, Lorg/threeten/bp/DateTimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 286
    :cond_0
    invoke-static {p1}, Lorg/threeten/bp/chrono/JapaneseEra;->a(Lorg/threeten/bp/LocalDate;)Lorg/threeten/bp/chrono/JapaneseEra;

    move-result-object v0

    iput-object v0, p0, Lorg/threeten/bp/chrono/JapaneseDate;->c:Lorg/threeten/bp/chrono/JapaneseEra;

    .line 287
    iget-object v0, p0, Lorg/threeten/bp/chrono/JapaneseDate;->c:Lorg/threeten/bp/chrono/JapaneseEra;

    invoke-virtual {v0}, Lorg/threeten/bp/chrono/JapaneseEra;->c()Lorg/threeten/bp/LocalDate;

    move-result-object v0

    invoke-virtual {v0}, Lorg/threeten/bp/LocalDate;->d()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    .line 288
    invoke-virtual {p1}, Lorg/threeten/bp/LocalDate;->d()I

    move-result v1

    sub-int v0, v1, v0

    iput v0, p0, Lorg/threeten/bp/chrono/JapaneseDate;->d:I

    .line 289
    iput-object p1, p0, Lorg/threeten/bp/chrono/JapaneseDate;->b:Lorg/threeten/bp/LocalDate;

    .line 290
    return-void
.end method

.method static a(Ljava/io/DataInput;)Lorg/threeten/bp/chrono/ChronoLocalDate;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 600
    invoke-interface {p0}, Ljava/io/DataInput;->readInt()I

    move-result v0

    .line 601
    invoke-interface {p0}, Ljava/io/DataInput;->readByte()B

    move-result v1

    .line 602
    invoke-interface {p0}, Ljava/io/DataInput;->readByte()B

    move-result v2

    .line 603
    sget-object v3, Lorg/threeten/bp/chrono/JapaneseChronology;->c:Lorg/threeten/bp/chrono/JapaneseChronology;

    invoke-virtual {v3, v0, v1, v2}, Lorg/threeten/bp/chrono/JapaneseChronology;->b(III)Lorg/threeten/bp/chrono/JapaneseDate;

    move-result-object v0

    return-object v0
.end method

.method private a(Lorg/threeten/bp/LocalDate;)Lorg/threeten/bp/chrono/JapaneseDate;
    .locals 1

    .prologue
    .line 549
    iget-object v0, p0, Lorg/threeten/bp/chrono/JapaneseDate;->b:Lorg/threeten/bp/LocalDate;

    invoke-virtual {p1, v0}, Lorg/threeten/bp/LocalDate;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return-object p0

    :cond_0
    new-instance p0, Lorg/threeten/bp/chrono/JapaneseDate;

    invoke-direct {p0, p1}, Lorg/threeten/bp/chrono/JapaneseDate;-><init>(Lorg/threeten/bp/LocalDate;)V

    goto :goto_0
.end method

.method private a(Lorg/threeten/bp/chrono/JapaneseEra;I)Lorg/threeten/bp/chrono/JapaneseDate;
    .locals 2

    .prologue
    .line 511
    sget-object v0, Lorg/threeten/bp/chrono/JapaneseChronology;->c:Lorg/threeten/bp/chrono/JapaneseChronology;

    invoke-virtual {v0, p1, p2}, Lorg/threeten/bp/chrono/JapaneseChronology;->a(Lorg/threeten/bp/chrono/Era;I)I

    move-result v0

    .line 512
    iget-object v1, p0, Lorg/threeten/bp/chrono/JapaneseDate;->b:Lorg/threeten/bp/LocalDate;

    invoke-virtual {v1, v0}, Lorg/threeten/bp/LocalDate;->a(I)Lorg/threeten/bp/LocalDate;

    move-result-object v0

    invoke-direct {p0, v0}, Lorg/threeten/bp/chrono/JapaneseDate;->a(Lorg/threeten/bp/LocalDate;)Lorg/threeten/bp/chrono/JapaneseDate;

    move-result-object v0

    return-object v0
.end method

.method private a(I)Lorg/threeten/bp/temporal/ValueRange;
    .locals 4

    .prologue
    .line 406
    sget-object v0, Lorg/threeten/bp/chrono/JapaneseChronology;->b:Ljava/util/Locale;

    invoke-static {v0}, Ljava/util/Calendar;->getInstance(Ljava/util/Locale;)Ljava/util/Calendar;

    move-result-object v0

    .line 407
    const/4 v1, 0x0

    iget-object v2, p0, Lorg/threeten/bp/chrono/JapaneseDate;->c:Lorg/threeten/bp/chrono/JapaneseEra;

    invoke-virtual {v2}, Lorg/threeten/bp/chrono/JapaneseEra;->a()I

    move-result v2

    add-int/lit8 v2, v2, 0x2

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->set(II)V

    .line 408
    iget v1, p0, Lorg/threeten/bp/chrono/JapaneseDate;->d:I

    iget-object v2, p0, Lorg/threeten/bp/chrono/JapaneseDate;->b:Lorg/threeten/bp/LocalDate;

    invoke-virtual {v2}, Lorg/threeten/bp/LocalDate;->e()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    iget-object v3, p0, Lorg/threeten/bp/chrono/JapaneseDate;->b:Lorg/threeten/bp/LocalDate;

    invoke-virtual {v3}, Lorg/threeten/bp/LocalDate;->g()I

    move-result v3

    invoke-virtual {v0, v1, v2, v3}, Ljava/util/Calendar;->set(III)V

    .line 409
    invoke-virtual {v0, p1}, Ljava/util/Calendar;->getActualMinimum(I)I

    move-result v1

    int-to-long v2, v1

    invoke-virtual {v0, p1}, Ljava/util/Calendar;->getActualMaximum(I)I

    move-result v0

    int-to-long v0, v0

    invoke-static {v2, v3, v0, v1}, Lorg/threeten/bp/temporal/ValueRange;->a(JJ)Lorg/threeten/bp/temporal/ValueRange;

    move-result-object v0

    return-object v0
.end method

.method private b(I)Lorg/threeten/bp/chrono/JapaneseDate;
    .locals 1

    .prologue
    .line 529
    invoke-virtual {p0}, Lorg/threeten/bp/chrono/JapaneseDate;->b()Lorg/threeten/bp/chrono/JapaneseEra;

    move-result-object v0

    invoke-direct {p0, v0, p1}, Lorg/threeten/bp/chrono/JapaneseDate;->a(Lorg/threeten/bp/chrono/JapaneseEra;I)Lorg/threeten/bp/chrono/JapaneseDate;

    move-result-object v0

    return-object v0
.end method

.method private d()J
    .locals 2

    .prologue
    .line 435
    iget v0, p0, Lorg/threeten/bp/chrono/JapaneseDate;->d:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 436
    iget-object v0, p0, Lorg/threeten/bp/chrono/JapaneseDate;->b:Lorg/threeten/bp/LocalDate;

    invoke-virtual {v0}, Lorg/threeten/bp/LocalDate;->h()I

    move-result v0

    iget-object v1, p0, Lorg/threeten/bp/chrono/JapaneseDate;->c:Lorg/threeten/bp/chrono/JapaneseEra;

    invoke-virtual {v1}, Lorg/threeten/bp/chrono/JapaneseEra;->c()Lorg/threeten/bp/LocalDate;

    move-result-object v1

    invoke-virtual {v1}, Lorg/threeten/bp/LocalDate;->h()I

    move-result v1

    sub-int/2addr v0, v1

    add-int/lit8 v0, v0, 0x1

    int-to-long v0, v0

    .line 438
    :goto_0
    return-wide v0

    :cond_0
    iget-object v0, p0, Lorg/threeten/bp/chrono/JapaneseDate;->b:Lorg/threeten/bp/LocalDate;

    invoke-virtual {v0}, Lorg/threeten/bp/LocalDate;->h()I

    move-result v0

    int-to-long v0, v0

    goto :goto_0
.end method

.method private readObject(Ljava/io/ObjectInputStream;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 315
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->defaultReadObject()V

    .line 316
    iget-object v0, p0, Lorg/threeten/bp/chrono/JapaneseDate;->b:Lorg/threeten/bp/LocalDate;

    invoke-static {v0}, Lorg/threeten/bp/chrono/JapaneseEra;->a(Lorg/threeten/bp/LocalDate;)Lorg/threeten/bp/chrono/JapaneseEra;

    move-result-object v0

    iput-object v0, p0, Lorg/threeten/bp/chrono/JapaneseDate;->c:Lorg/threeten/bp/chrono/JapaneseEra;

    .line 317
    iget-object v0, p0, Lorg/threeten/bp/chrono/JapaneseDate;->c:Lorg/threeten/bp/chrono/JapaneseEra;

    invoke-virtual {v0}, Lorg/threeten/bp/chrono/JapaneseEra;->c()Lorg/threeten/bp/LocalDate;

    move-result-object v0

    invoke-virtual {v0}, Lorg/threeten/bp/LocalDate;->d()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    .line 318
    iget-object v1, p0, Lorg/threeten/bp/chrono/JapaneseDate;->b:Lorg/threeten/bp/LocalDate;

    invoke-virtual {v1}, Lorg/threeten/bp/LocalDate;->d()I

    move-result v1

    sub-int v0, v1, v0

    iput v0, p0, Lorg/threeten/bp/chrono/JapaneseDate;->d:I

    .line 319
    return-void
.end method

.method private writeReplace()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 589
    new-instance v0, Lorg/threeten/bp/chrono/Ser;

    const/4 v1, 0x1

    invoke-direct {v0, v1, p0}, Lorg/threeten/bp/chrono/Ser;-><init>(BLjava/lang/Object;)V

    return-object v0
.end method


# virtual methods
.method public bridge synthetic a(Lorg/threeten/bp/temporal/Temporal;Lorg/threeten/bp/temporal/TemporalUnit;)J
    .locals 2

    .prologue
    .line 84
    invoke-super {p0, p1, p2}, Lorg/threeten/bp/chrono/ChronoDateImpl;->a(Lorg/threeten/bp/temporal/Temporal;Lorg/threeten/bp/temporal/TemporalUnit;)J

    move-result-wide v0

    return-wide v0
.end method

.method synthetic a(J)Lorg/threeten/bp/chrono/ChronoDateImpl;
    .locals 1

    .prologue
    .line 84
    invoke-virtual {p0, p1, p2}, Lorg/threeten/bp/chrono/JapaneseDate;->d(J)Lorg/threeten/bp/chrono/JapaneseDate;

    move-result-object v0

    return-object v0
.end method

.method public synthetic a(JLorg/threeten/bp/temporal/TemporalUnit;)Lorg/threeten/bp/chrono/ChronoDateImpl;
    .locals 1

    .prologue
    .line 84
    invoke-virtual {p0, p1, p2, p3}, Lorg/threeten/bp/chrono/JapaneseDate;->b(JLorg/threeten/bp/temporal/TemporalUnit;)Lorg/threeten/bp/chrono/JapaneseDate;

    move-result-object v0

    return-object v0
.end method

.method public a()Lorg/threeten/bp/chrono/JapaneseChronology;
    .locals 1

    .prologue
    .line 324
    sget-object v0, Lorg/threeten/bp/chrono/JapaneseChronology;->c:Lorg/threeten/bp/chrono/JapaneseChronology;

    return-object v0
.end method

.method public a(Lorg/threeten/bp/temporal/TemporalAdjuster;)Lorg/threeten/bp/chrono/JapaneseDate;
    .locals 1

    .prologue
    .line 444
    invoke-super {p0, p1}, Lorg/threeten/bp/chrono/ChronoDateImpl;->c(Lorg/threeten/bp/temporal/TemporalAdjuster;)Lorg/threeten/bp/chrono/ChronoLocalDate;

    move-result-object v0

    check-cast v0, Lorg/threeten/bp/chrono/JapaneseDate;

    return-object v0
.end method

.method public a(Lorg/threeten/bp/temporal/TemporalAmount;)Lorg/threeten/bp/chrono/JapaneseDate;
    .locals 1

    .prologue
    .line 477
    invoke-super {p0, p1}, Lorg/threeten/bp/chrono/ChronoDateImpl;->b(Lorg/threeten/bp/temporal/TemporalAmount;)Lorg/threeten/bp/chrono/ChronoLocalDate;

    move-result-object v0

    check-cast v0, Lorg/threeten/bp/chrono/JapaneseDate;

    return-object v0
.end method

.method public a(Lorg/threeten/bp/temporal/TemporalField;J)Lorg/threeten/bp/chrono/JapaneseDate;
    .locals 6

    .prologue
    .line 449
    instance-of v0, p1, Lorg/threeten/bp/temporal/ChronoField;

    if-eqz v0, :cond_1

    move-object v0, p1

    .line 450
    check-cast v0, Lorg/threeten/bp/temporal/ChronoField;

    .line 451
    invoke-virtual {p0, v0}, Lorg/threeten/bp/chrono/JapaneseDate;->d(Lorg/threeten/bp/temporal/TemporalField;)J

    move-result-wide v2

    cmp-long v1, v2, p2

    if-nez v1, :cond_0

    .line 472
    :goto_0
    return-object p0

    .line 454
    :cond_0
    sget-object v1, Lorg/threeten/bp/chrono/JapaneseDate$1;->a:[I

    invoke-virtual {v0}, Lorg/threeten/bp/temporal/ChronoField;->ordinal()I

    move-result v2

    aget v1, v1, v2

    sparse-switch v1, :sswitch_data_0

    .line 470
    :goto_1
    iget-object v0, p0, Lorg/threeten/bp/chrono/JapaneseDate;->b:Lorg/threeten/bp/LocalDate;

    invoke-virtual {v0, p1, p2, p3}, Lorg/threeten/bp/LocalDate;->a(Lorg/threeten/bp/temporal/TemporalField;J)Lorg/threeten/bp/LocalDate;

    move-result-object v0

    invoke-direct {p0, v0}, Lorg/threeten/bp/chrono/JapaneseDate;->a(Lorg/threeten/bp/LocalDate;)Lorg/threeten/bp/chrono/JapaneseDate;

    move-result-object p0

    goto :goto_0

    .line 458
    :sswitch_0
    invoke-virtual {p0}, Lorg/threeten/bp/chrono/JapaneseDate;->a()Lorg/threeten/bp/chrono/JapaneseChronology;

    move-result-object v1

    invoke-virtual {v1, v0}, Lorg/threeten/bp/chrono/JapaneseChronology;->a(Lorg/threeten/bp/temporal/ChronoField;)Lorg/threeten/bp/temporal/ValueRange;

    move-result-object v1

    invoke-virtual {v1, p2, p3, v0}, Lorg/threeten/bp/temporal/ValueRange;->b(JLorg/threeten/bp/temporal/TemporalField;)I

    move-result v1

    .line 459
    sget-object v2, Lorg/threeten/bp/chrono/JapaneseDate$1;->a:[I

    invoke-virtual {v0}, Lorg/threeten/bp/temporal/ChronoField;->ordinal()I

    move-result v0

    aget v0, v2, v0

    sparse-switch v0, :sswitch_data_1

    goto :goto_1

    .line 461
    :sswitch_1
    iget-object v0, p0, Lorg/threeten/bp/chrono/JapaneseDate;->b:Lorg/threeten/bp/LocalDate;

    int-to-long v2, v1

    invoke-direct {p0}, Lorg/threeten/bp/chrono/JapaneseDate;->d()J

    move-result-wide v4

    sub-long/2addr v2, v4

    invoke-virtual {v0, v2, v3}, Lorg/threeten/bp/LocalDate;->e(J)Lorg/threeten/bp/LocalDate;

    move-result-object v0

    invoke-direct {p0, v0}, Lorg/threeten/bp/chrono/JapaneseDate;->a(Lorg/threeten/bp/LocalDate;)Lorg/threeten/bp/chrono/JapaneseDate;

    move-result-object p0

    goto :goto_0

    .line 463
    :sswitch_2
    invoke-direct {p0, v1}, Lorg/threeten/bp/chrono/JapaneseDate;->b(I)Lorg/threeten/bp/chrono/JapaneseDate;

    move-result-object p0

    goto :goto_0

    .line 465
    :sswitch_3
    invoke-static {v1}, Lorg/threeten/bp/chrono/JapaneseEra;->a(I)Lorg/threeten/bp/chrono/JapaneseEra;

    move-result-object v0

    iget v1, p0, Lorg/threeten/bp/chrono/JapaneseDate;->d:I

    invoke-direct {p0, v0, v1}, Lorg/threeten/bp/chrono/JapaneseDate;->a(Lorg/threeten/bp/chrono/JapaneseEra;I)Lorg/threeten/bp/chrono/JapaneseDate;

    move-result-object p0

    goto :goto_0

    .line 472
    :cond_1
    invoke-interface {p1, p0, p2, p3}, Lorg/threeten/bp/temporal/TemporalField;->a(Lorg/threeten/bp/temporal/Temporal;J)Lorg/threeten/bp/temporal/Temporal;

    move-result-object v0

    check-cast v0, Lorg/threeten/bp/chrono/JapaneseDate;

    move-object p0, v0

    goto :goto_0

    .line 454
    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x2 -> :sswitch_0
        0x7 -> :sswitch_0
    .end sparse-switch

    .line 459
    :sswitch_data_1
    .sparse-switch
        0x1 -> :sswitch_1
        0x2 -> :sswitch_2
        0x7 -> :sswitch_3
    .end sparse-switch
.end method

.method a(Ljava/io/DataOutput;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 594
    sget-object v0, Lorg/threeten/bp/temporal/ChronoField;->A:Lorg/threeten/bp/temporal/ChronoField;

    invoke-virtual {p0, v0}, Lorg/threeten/bp/chrono/JapaneseDate;->c(Lorg/threeten/bp/temporal/TemporalField;)I

    move-result v0

    invoke-interface {p1, v0}, Ljava/io/DataOutput;->writeInt(I)V

    .line 595
    sget-object v0, Lorg/threeten/bp/temporal/ChronoField;->x:Lorg/threeten/bp/temporal/ChronoField;

    invoke-virtual {p0, v0}, Lorg/threeten/bp/chrono/JapaneseDate;->c(Lorg/threeten/bp/temporal/TemporalField;)I

    move-result v0

    invoke-interface {p1, v0}, Ljava/io/DataOutput;->writeByte(I)V

    .line 596
    sget-object v0, Lorg/threeten/bp/temporal/ChronoField;->s:Lorg/threeten/bp/temporal/ChronoField;

    invoke-virtual {p0, v0}, Lorg/threeten/bp/chrono/JapaneseDate;->c(Lorg/threeten/bp/temporal/TemporalField;)I

    move-result v0

    invoke-interface {p1, v0}, Ljava/io/DataOutput;->writeByte(I)V

    .line 597
    return-void
.end method

.method public a(Lorg/threeten/bp/temporal/TemporalField;)Z
    .locals 1

    .prologue
    .line 378
    sget-object v0, Lorg/threeten/bp/temporal/ChronoField;->q:Lorg/threeten/bp/temporal/ChronoField;

    if-eq p1, v0, :cond_0

    sget-object v0, Lorg/threeten/bp/temporal/ChronoField;->r:Lorg/threeten/bp/temporal/ChronoField;

    if-eq p1, v0, :cond_0

    sget-object v0, Lorg/threeten/bp/temporal/ChronoField;->v:Lorg/threeten/bp/temporal/ChronoField;

    if-eq p1, v0, :cond_0

    sget-object v0, Lorg/threeten/bp/temporal/ChronoField;->w:Lorg/threeten/bp/temporal/ChronoField;

    if-ne p1, v0, :cond_1

    .line 382
    :cond_0
    const/4 v0, 0x0

    .line 384
    :goto_0
    return v0

    :cond_1
    invoke-super {p0, p1}, Lorg/threeten/bp/chrono/ChronoDateImpl;->a(Lorg/threeten/bp/temporal/TemporalField;)Z

    move-result v0

    goto :goto_0
.end method

.method synthetic b(J)Lorg/threeten/bp/chrono/ChronoDateImpl;
    .locals 1

    .prologue
    .line 84
    invoke-virtual {p0, p1, p2}, Lorg/threeten/bp/chrono/JapaneseDate;->e(J)Lorg/threeten/bp/chrono/JapaneseDate;

    move-result-object v0

    return-object v0
.end method

.method public synthetic b(Lorg/threeten/bp/temporal/TemporalAmount;)Lorg/threeten/bp/chrono/ChronoLocalDate;
    .locals 1

    .prologue
    .line 84
    invoke-virtual {p0, p1}, Lorg/threeten/bp/chrono/JapaneseDate;->a(Lorg/threeten/bp/temporal/TemporalAmount;)Lorg/threeten/bp/chrono/JapaneseDate;

    move-result-object v0

    return-object v0
.end method

.method public final b(Lorg/threeten/bp/LocalTime;)Lorg/threeten/bp/chrono/ChronoLocalDateTime;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/threeten/bp/LocalTime;",
            ")",
            "Lorg/threeten/bp/chrono/ChronoLocalDateTime",
            "<",
            "Lorg/threeten/bp/chrono/JapaneseDate;",
            ">;"
        }
    .end annotation

    .prologue
    .line 555
    invoke-super {p0, p1}, Lorg/threeten/bp/chrono/ChronoDateImpl;->b(Lorg/threeten/bp/LocalTime;)Lorg/threeten/bp/chrono/ChronoLocalDateTime;

    move-result-object v0

    return-object v0
.end method

.method public b(JLorg/threeten/bp/temporal/TemporalUnit;)Lorg/threeten/bp/chrono/JapaneseDate;
    .locals 1

    .prologue
    .line 482
    invoke-super {p0, p1, p2, p3}, Lorg/threeten/bp/chrono/ChronoDateImpl;->a(JLorg/threeten/bp/temporal/TemporalUnit;)Lorg/threeten/bp/chrono/ChronoDateImpl;

    move-result-object v0

    check-cast v0, Lorg/threeten/bp/chrono/JapaneseDate;

    return-object v0
.end method

.method public b()Lorg/threeten/bp/chrono/JapaneseEra;
    .locals 1

    .prologue
    .line 329
    iget-object v0, p0, Lorg/threeten/bp/chrono/JapaneseDate;->c:Lorg/threeten/bp/chrono/JapaneseEra;

    return-object v0
.end method

.method public synthetic b(Lorg/threeten/bp/temporal/TemporalAdjuster;)Lorg/threeten/bp/temporal/Temporal;
    .locals 1

    .prologue
    .line 84
    invoke-virtual {p0, p1}, Lorg/threeten/bp/chrono/JapaneseDate;->a(Lorg/threeten/bp/temporal/TemporalAdjuster;)Lorg/threeten/bp/chrono/JapaneseDate;

    move-result-object v0

    return-object v0
.end method

.method public synthetic b(Lorg/threeten/bp/temporal/TemporalField;J)Lorg/threeten/bp/temporal/Temporal;
    .locals 2

    .prologue
    .line 84
    invoke-virtual {p0, p1, p2, p3}, Lorg/threeten/bp/chrono/JapaneseDate;->a(Lorg/threeten/bp/temporal/TemporalField;J)Lorg/threeten/bp/chrono/JapaneseDate;

    move-result-object v0

    return-object v0
.end method

.method public b(Lorg/threeten/bp/temporal/TemporalField;)Lorg/threeten/bp/temporal/ValueRange;
    .locals 3

    .prologue
    .line 389
    instance-of v0, p1, Lorg/threeten/bp/temporal/ChronoField;

    if-eqz v0, :cond_1

    .line 390
    invoke-virtual {p0, p1}, Lorg/threeten/bp/chrono/JapaneseDate;->a(Lorg/threeten/bp/temporal/TemporalField;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 391
    check-cast p1, Lorg/threeten/bp/temporal/ChronoField;

    .line 392
    sget-object v0, Lorg/threeten/bp/chrono/JapaneseDate$1;->a:[I

    invoke-virtual {p1}, Lorg/threeten/bp/temporal/ChronoField;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 398
    invoke-virtual {p0}, Lorg/threeten/bp/chrono/JapaneseDate;->a()Lorg/threeten/bp/chrono/JapaneseChronology;

    move-result-object v0

    invoke-virtual {v0, p1}, Lorg/threeten/bp/chrono/JapaneseChronology;->a(Lorg/threeten/bp/temporal/ChronoField;)Lorg/threeten/bp/temporal/ValueRange;

    move-result-object v0

    .line 402
    :goto_0
    return-object v0

    .line 394
    :pswitch_0
    const/4 v0, 0x6

    invoke-direct {p0, v0}, Lorg/threeten/bp/chrono/JapaneseDate;->a(I)Lorg/threeten/bp/temporal/ValueRange;

    move-result-object v0

    goto :goto_0

    .line 396
    :pswitch_1
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lorg/threeten/bp/chrono/JapaneseDate;->a(I)Lorg/threeten/bp/temporal/ValueRange;

    move-result-object v0

    goto :goto_0

    .line 400
    :cond_0
    new-instance v0, Lorg/threeten/bp/temporal/UnsupportedTemporalTypeException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unsupported field: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/threeten/bp/temporal/UnsupportedTemporalTypeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 402
    :cond_1
    invoke-interface {p1, p0}, Lorg/threeten/bp/temporal/TemporalField;->b(Lorg/threeten/bp/temporal/TemporalAccessor;)Lorg/threeten/bp/temporal/ValueRange;

    move-result-object v0

    goto :goto_0

    .line 392
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method synthetic c(J)Lorg/threeten/bp/chrono/ChronoDateImpl;
    .locals 1

    .prologue
    .line 84
    invoke-virtual {p0, p1, p2}, Lorg/threeten/bp/chrono/JapaneseDate;->f(J)Lorg/threeten/bp/chrono/JapaneseDate;

    move-result-object v0

    return-object v0
.end method

.method public synthetic c(Lorg/threeten/bp/temporal/TemporalAdjuster;)Lorg/threeten/bp/chrono/ChronoLocalDate;
    .locals 1

    .prologue
    .line 84
    invoke-virtual {p0, p1}, Lorg/threeten/bp/chrono/JapaneseDate;->a(Lorg/threeten/bp/temporal/TemporalAdjuster;)Lorg/threeten/bp/chrono/JapaneseDate;

    move-result-object v0

    return-object v0
.end method

.method public synthetic c(Lorg/threeten/bp/temporal/TemporalField;J)Lorg/threeten/bp/chrono/ChronoLocalDate;
    .locals 2

    .prologue
    .line 84
    invoke-virtual {p0, p1, p2, p3}, Lorg/threeten/bp/chrono/JapaneseDate;->a(Lorg/threeten/bp/temporal/TemporalField;J)Lorg/threeten/bp/chrono/JapaneseDate;

    move-result-object v0

    return-object v0
.end method

.method public synthetic c()Lorg/threeten/bp/chrono/Era;
    .locals 1

    .prologue
    .line 84
    invoke-virtual {p0}, Lorg/threeten/bp/chrono/JapaneseDate;->b()Lorg/threeten/bp/chrono/JapaneseEra;

    move-result-object v0

    return-object v0
.end method

.method public synthetic c(JLorg/threeten/bp/temporal/TemporalUnit;)Lorg/threeten/bp/temporal/Temporal;
    .locals 1

    .prologue
    .line 84
    invoke-virtual {p0, p1, p2, p3}, Lorg/threeten/bp/chrono/JapaneseDate;->g(JLorg/threeten/bp/temporal/TemporalUnit;)Lorg/threeten/bp/chrono/JapaneseDate;

    move-result-object v0

    return-object v0
.end method

.method public synthetic c(Lorg/threeten/bp/temporal/TemporalAmount;)Lorg/threeten/bp/temporal/Temporal;
    .locals 1

    .prologue
    .line 84
    invoke-virtual {p0, p1}, Lorg/threeten/bp/chrono/JapaneseDate;->a(Lorg/threeten/bp/temporal/TemporalAmount;)Lorg/threeten/bp/chrono/JapaneseDate;

    move-result-object v0

    return-object v0
.end method

.method public d(Lorg/threeten/bp/temporal/TemporalField;)J
    .locals 3

    .prologue
    .line 415
    instance-of v0, p1, Lorg/threeten/bp/temporal/ChronoField;

    if-eqz v0, :cond_0

    .line 416
    sget-object v1, Lorg/threeten/bp/chrono/JapaneseDate$1;->a:[I

    move-object v0, p1

    check-cast v0, Lorg/threeten/bp/temporal/ChronoField;

    invoke-virtual {v0}, Lorg/threeten/bp/temporal/ChronoField;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 429
    iget-object v0, p0, Lorg/threeten/bp/chrono/JapaneseDate;->b:Lorg/threeten/bp/LocalDate;

    invoke-virtual {v0, p1}, Lorg/threeten/bp/LocalDate;->d(Lorg/threeten/bp/temporal/TemporalField;)J

    move-result-wide v0

    .line 431
    :goto_0
    return-wide v0

    .line 421
    :pswitch_0
    new-instance v0, Lorg/threeten/bp/temporal/UnsupportedTemporalTypeException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unsupported field: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/threeten/bp/temporal/UnsupportedTemporalTypeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 423
    :pswitch_1
    iget v0, p0, Lorg/threeten/bp/chrono/JapaneseDate;->d:I

    int-to-long v0, v0

    goto :goto_0

    .line 425
    :pswitch_2
    iget-object v0, p0, Lorg/threeten/bp/chrono/JapaneseDate;->c:Lorg/threeten/bp/chrono/JapaneseEra;

    invoke-virtual {v0}, Lorg/threeten/bp/chrono/JapaneseEra;->a()I

    move-result v0

    int-to-long v0, v0

    goto :goto_0

    .line 427
    :pswitch_3
    invoke-direct {p0}, Lorg/threeten/bp/chrono/JapaneseDate;->d()J

    move-result-wide v0

    goto :goto_0

    .line 431
    :cond_0
    invoke-interface {p1, p0}, Lorg/threeten/bp/temporal/TemporalField;->c(Lorg/threeten/bp/temporal/TemporalAccessor;)J

    move-result-wide v0

    goto :goto_0

    .line 416
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_3
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method d(J)Lorg/threeten/bp/chrono/JapaneseDate;
    .locals 1

    .prologue
    .line 535
    iget-object v0, p0, Lorg/threeten/bp/chrono/JapaneseDate;->b:Lorg/threeten/bp/LocalDate;

    invoke-virtual {v0, p1, p2}, Lorg/threeten/bp/LocalDate;->b(J)Lorg/threeten/bp/LocalDate;

    move-result-object v0

    invoke-direct {p0, v0}, Lorg/threeten/bp/chrono/JapaneseDate;->a(Lorg/threeten/bp/LocalDate;)Lorg/threeten/bp/chrono/JapaneseDate;

    move-result-object v0

    return-object v0
.end method

.method public synthetic d(JLorg/threeten/bp/temporal/TemporalUnit;)Lorg/threeten/bp/temporal/Temporal;
    .locals 1

    .prologue
    .line 84
    invoke-virtual {p0, p1, p2, p3}, Lorg/threeten/bp/chrono/JapaneseDate;->b(JLorg/threeten/bp/temporal/TemporalUnit;)Lorg/threeten/bp/chrono/JapaneseDate;

    move-result-object v0

    return-object v0
.end method

.method public synthetic e(JLorg/threeten/bp/temporal/TemporalUnit;)Lorg/threeten/bp/chrono/ChronoLocalDate;
    .locals 1

    .prologue
    .line 84
    invoke-virtual {p0, p1, p2, p3}, Lorg/threeten/bp/chrono/JapaneseDate;->g(JLorg/threeten/bp/temporal/TemporalUnit;)Lorg/threeten/bp/chrono/JapaneseDate;

    move-result-object v0

    return-object v0
.end method

.method e(J)Lorg/threeten/bp/chrono/JapaneseDate;
    .locals 1

    .prologue
    .line 540
    iget-object v0, p0, Lorg/threeten/bp/chrono/JapaneseDate;->b:Lorg/threeten/bp/LocalDate;

    invoke-virtual {v0, p1, p2}, Lorg/threeten/bp/LocalDate;->c(J)Lorg/threeten/bp/LocalDate;

    move-result-object v0

    invoke-direct {p0, v0}, Lorg/threeten/bp/chrono/JapaneseDate;->a(Lorg/threeten/bp/LocalDate;)Lorg/threeten/bp/chrono/JapaneseDate;

    move-result-object v0

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 572
    if-ne p0, p1, :cond_0

    .line 573
    const/4 v0, 0x1

    .line 579
    :goto_0
    return v0

    .line 575
    :cond_0
    instance-of v0, p1, Lorg/threeten/bp/chrono/JapaneseDate;

    if-eqz v0, :cond_1

    .line 576
    check-cast p1, Lorg/threeten/bp/chrono/JapaneseDate;

    .line 577
    iget-object v0, p0, Lorg/threeten/bp/chrono/JapaneseDate;->b:Lorg/threeten/bp/LocalDate;

    iget-object v1, p1, Lorg/threeten/bp/chrono/JapaneseDate;->b:Lorg/threeten/bp/LocalDate;

    invoke-virtual {v0, v1}, Lorg/threeten/bp/LocalDate;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0

    .line 579
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public synthetic f(JLorg/threeten/bp/temporal/TemporalUnit;)Lorg/threeten/bp/chrono/ChronoLocalDate;
    .locals 1

    .prologue
    .line 84
    invoke-virtual {p0, p1, p2, p3}, Lorg/threeten/bp/chrono/JapaneseDate;->b(JLorg/threeten/bp/temporal/TemporalUnit;)Lorg/threeten/bp/chrono/JapaneseDate;

    move-result-object v0

    return-object v0
.end method

.method f(J)Lorg/threeten/bp/chrono/JapaneseDate;
    .locals 1

    .prologue
    .line 545
    iget-object v0, p0, Lorg/threeten/bp/chrono/JapaneseDate;->b:Lorg/threeten/bp/LocalDate;

    invoke-virtual {v0, p1, p2}, Lorg/threeten/bp/LocalDate;->e(J)Lorg/threeten/bp/LocalDate;

    move-result-object v0

    invoke-direct {p0, v0}, Lorg/threeten/bp/chrono/JapaneseDate;->a(Lorg/threeten/bp/LocalDate;)Lorg/threeten/bp/chrono/JapaneseDate;

    move-result-object v0

    return-object v0
.end method

.method public g(JLorg/threeten/bp/temporal/TemporalUnit;)Lorg/threeten/bp/chrono/JapaneseDate;
    .locals 1

    .prologue
    .line 492
    invoke-super {p0, p1, p2, p3}, Lorg/threeten/bp/chrono/ChronoDateImpl;->e(JLorg/threeten/bp/temporal/TemporalUnit;)Lorg/threeten/bp/chrono/ChronoLocalDate;

    move-result-object v0

    check-cast v0, Lorg/threeten/bp/chrono/JapaneseDate;

    return-object v0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 584
    invoke-virtual {p0}, Lorg/threeten/bp/chrono/JapaneseDate;->a()Lorg/threeten/bp/chrono/JapaneseChronology;

    move-result-object v0

    invoke-virtual {v0}, Lorg/threeten/bp/chrono/JapaneseChronology;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    iget-object v1, p0, Lorg/threeten/bp/chrono/JapaneseDate;->b:Lorg/threeten/bp/LocalDate;

    invoke-virtual {v1}, Lorg/threeten/bp/LocalDate;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    return v0
.end method

.method public l()I
    .locals 4

    .prologue
    .line 339
    sget-object v0, Lorg/threeten/bp/chrono/JapaneseChronology;->b:Ljava/util/Locale;

    invoke-static {v0}, Ljava/util/Calendar;->getInstance(Ljava/util/Locale;)Ljava/util/Calendar;

    move-result-object v0

    .line 340
    const/4 v1, 0x0

    iget-object v2, p0, Lorg/threeten/bp/chrono/JapaneseDate;->c:Lorg/threeten/bp/chrono/JapaneseEra;

    invoke-virtual {v2}, Lorg/threeten/bp/chrono/JapaneseEra;->a()I

    move-result v2

    add-int/lit8 v2, v2, 0x2

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->set(II)V

    .line 341
    iget v1, p0, Lorg/threeten/bp/chrono/JapaneseDate;->d:I

    iget-object v2, p0, Lorg/threeten/bp/chrono/JapaneseDate;->b:Lorg/threeten/bp/LocalDate;

    invoke-virtual {v2}, Lorg/threeten/bp/LocalDate;->e()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    iget-object v3, p0, Lorg/threeten/bp/chrono/JapaneseDate;->b:Lorg/threeten/bp/LocalDate;

    invoke-virtual {v3}, Lorg/threeten/bp/LocalDate;->g()I

    move-result v3

    invoke-virtual {v0, v1, v2, v3}, Ljava/util/Calendar;->set(III)V

    .line 342
    const/4 v1, 0x6

    invoke-virtual {v0, v1}, Ljava/util/Calendar;->getActualMaximum(I)I

    move-result v0

    return v0
.end method

.method public n()J
    .locals 2

    .prologue
    .line 566
    iget-object v0, p0, Lorg/threeten/bp/chrono/JapaneseDate;->b:Lorg/threeten/bp/LocalDate;

    invoke-virtual {v0}, Lorg/threeten/bp/LocalDate;->n()J

    move-result-wide v0

    return-wide v0
.end method

.method public synthetic o()Lorg/threeten/bp/chrono/Chronology;
    .locals 1

    .prologue
    .line 84
    invoke-virtual {p0}, Lorg/threeten/bp/chrono/JapaneseDate;->a()Lorg/threeten/bp/chrono/JapaneseChronology;

    move-result-object v0

    return-object v0
.end method
