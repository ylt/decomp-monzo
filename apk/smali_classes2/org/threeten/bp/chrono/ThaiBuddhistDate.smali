.class public final Lorg/threeten/bp/chrono/ThaiBuddhistDate;
.super Lorg/threeten/bp/chrono/ChronoDateImpl;
.source "ThaiBuddhistDate.java"

# interfaces
.implements Ljava/io/Serializable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/threeten/bp/chrono/ThaiBuddhistDate$1;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lorg/threeten/bp/chrono/ChronoDateImpl",
        "<",
        "Lorg/threeten/bp/chrono/ThaiBuddhistDate;",
        ">;",
        "Ljava/io/Serializable;"
    }
.end annotation


# instance fields
.field private final a:Lorg/threeten/bp/LocalDate;


# direct methods
.method constructor <init>(Lorg/threeten/bp/LocalDate;)V
    .locals 1

    .prologue
    .line 177
    invoke-direct {p0}, Lorg/threeten/bp/chrono/ChronoDateImpl;-><init>()V

    .line 178
    const-string v0, "date"

    invoke-static {p1, v0}, Lorg/threeten/bp/jdk8/Jdk8Methods;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 179
    iput-object p1, p0, Lorg/threeten/bp/chrono/ThaiBuddhistDate;->a:Lorg/threeten/bp/LocalDate;

    .line 180
    return-void
.end method

.method static a(Ljava/io/DataInput;)Lorg/threeten/bp/chrono/ChronoLocalDate;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 373
    invoke-interface {p0}, Ljava/io/DataInput;->readInt()I

    move-result v0

    .line 374
    invoke-interface {p0}, Ljava/io/DataInput;->readByte()B

    move-result v1

    .line 375
    invoke-interface {p0}, Ljava/io/DataInput;->readByte()B

    move-result v2

    .line 376
    sget-object v3, Lorg/threeten/bp/chrono/ThaiBuddhistChronology;->b:Lorg/threeten/bp/chrono/ThaiBuddhistChronology;

    invoke-virtual {v3, v0, v1, v2}, Lorg/threeten/bp/chrono/ThaiBuddhistChronology;->b(III)Lorg/threeten/bp/chrono/ThaiBuddhistDate;

    move-result-object v0

    return-object v0
.end method

.method private a(Lorg/threeten/bp/LocalDate;)Lorg/threeten/bp/chrono/ThaiBuddhistDate;
    .locals 1

    .prologue
    .line 322
    iget-object v0, p0, Lorg/threeten/bp/chrono/ThaiBuddhistDate;->a:Lorg/threeten/bp/LocalDate;

    invoke-virtual {p1, v0}, Lorg/threeten/bp/LocalDate;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return-object p0

    :cond_0
    new-instance p0, Lorg/threeten/bp/chrono/ThaiBuddhistDate;

    invoke-direct {p0, p1}, Lorg/threeten/bp/chrono/ThaiBuddhistDate;-><init>(Lorg/threeten/bp/LocalDate;)V

    goto :goto_0
.end method

.method private d()J
    .locals 4

    .prologue
    .line 242
    invoke-direct {p0}, Lorg/threeten/bp/chrono/ThaiBuddhistDate;->e()I

    move-result v0

    int-to-long v0, v0

    const-wide/16 v2, 0xc

    mul-long/2addr v0, v2

    iget-object v2, p0, Lorg/threeten/bp/chrono/ThaiBuddhistDate;->a:Lorg/threeten/bp/LocalDate;

    invoke-virtual {v2}, Lorg/threeten/bp/LocalDate;->e()I

    move-result v2

    int-to-long v2, v2

    add-long/2addr v0, v2

    const-wide/16 v2, 0x1

    sub-long/2addr v0, v2

    return-wide v0
.end method

.method private e()I
    .locals 1

    .prologue
    .line 246
    iget-object v0, p0, Lorg/threeten/bp/chrono/ThaiBuddhistDate;->a:Lorg/threeten/bp/LocalDate;

    invoke-virtual {v0}, Lorg/threeten/bp/LocalDate;->d()I

    move-result v0

    add-int/lit16 v0, v0, 0x21f

    return v0
.end method

.method private writeReplace()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 362
    new-instance v0, Lorg/threeten/bp/chrono/Ser;

    const/4 v1, 0x7

    invoke-direct {v0, v1, p0}, Lorg/threeten/bp/chrono/Ser;-><init>(BLjava/lang/Object;)V

    return-object v0
.end method


# virtual methods
.method public bridge synthetic a(Lorg/threeten/bp/temporal/Temporal;Lorg/threeten/bp/temporal/TemporalUnit;)J
    .locals 2

    .prologue
    .line 71
    invoke-super {p0, p1, p2}, Lorg/threeten/bp/chrono/ChronoDateImpl;->a(Lorg/threeten/bp/temporal/Temporal;Lorg/threeten/bp/temporal/TemporalUnit;)J

    move-result-wide v0

    return-wide v0
.end method

.method synthetic a(J)Lorg/threeten/bp/chrono/ChronoDateImpl;
    .locals 1

    .prologue
    .line 71
    invoke-virtual {p0, p1, p2}, Lorg/threeten/bp/chrono/ThaiBuddhistDate;->d(J)Lorg/threeten/bp/chrono/ThaiBuddhistDate;

    move-result-object v0

    return-object v0
.end method

.method public synthetic a(JLorg/threeten/bp/temporal/TemporalUnit;)Lorg/threeten/bp/chrono/ChronoDateImpl;
    .locals 1

    .prologue
    .line 71
    invoke-virtual {p0, p1, p2, p3}, Lorg/threeten/bp/chrono/ThaiBuddhistDate;->b(JLorg/threeten/bp/temporal/TemporalUnit;)Lorg/threeten/bp/chrono/ThaiBuddhistDate;

    move-result-object v0

    return-object v0
.end method

.method public a()Lorg/threeten/bp/chrono/ThaiBuddhistChronology;
    .locals 1

    .prologue
    .line 185
    sget-object v0, Lorg/threeten/bp/chrono/ThaiBuddhistChronology;->b:Lorg/threeten/bp/chrono/ThaiBuddhistChronology;

    return-object v0
.end method

.method public a(Lorg/threeten/bp/temporal/TemporalAdjuster;)Lorg/threeten/bp/chrono/ThaiBuddhistDate;
    .locals 1

    .prologue
    .line 252
    invoke-super {p0, p1}, Lorg/threeten/bp/chrono/ChronoDateImpl;->c(Lorg/threeten/bp/temporal/TemporalAdjuster;)Lorg/threeten/bp/chrono/ChronoLocalDate;

    move-result-object v0

    check-cast v0, Lorg/threeten/bp/chrono/ThaiBuddhistDate;

    return-object v0
.end method

.method public a(Lorg/threeten/bp/temporal/TemporalAmount;)Lorg/threeten/bp/chrono/ThaiBuddhistDate;
    .locals 1

    .prologue
    .line 287
    invoke-super {p0, p1}, Lorg/threeten/bp/chrono/ChronoDateImpl;->b(Lorg/threeten/bp/temporal/TemporalAmount;)Lorg/threeten/bp/chrono/ChronoLocalDate;

    move-result-object v0

    check-cast v0, Lorg/threeten/bp/chrono/ThaiBuddhistDate;

    return-object v0
.end method

.method public a(Lorg/threeten/bp/temporal/TemporalField;J)Lorg/threeten/bp/chrono/ThaiBuddhistDate;
    .locals 4

    .prologue
    .line 257
    instance-of v0, p1, Lorg/threeten/bp/temporal/ChronoField;

    if-eqz v0, :cond_2

    move-object v0, p1

    .line 258
    check-cast v0, Lorg/threeten/bp/temporal/ChronoField;

    .line 259
    invoke-virtual {p0, v0}, Lorg/threeten/bp/chrono/ThaiBuddhistDate;->d(Lorg/threeten/bp/temporal/TemporalField;)J

    move-result-wide v2

    cmp-long v1, v2, p2

    if-nez v1, :cond_0

    .line 282
    :goto_0
    return-object p0

    .line 262
    :cond_0
    sget-object v1, Lorg/threeten/bp/chrono/ThaiBuddhistDate$1;->a:[I

    invoke-virtual {v0}, Lorg/threeten/bp/temporal/ChronoField;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 280
    :goto_1
    iget-object v0, p0, Lorg/threeten/bp/chrono/ThaiBuddhistDate;->a:Lorg/threeten/bp/LocalDate;

    invoke-virtual {v0, p1, p2, p3}, Lorg/threeten/bp/LocalDate;->a(Lorg/threeten/bp/temporal/TemporalField;J)Lorg/threeten/bp/LocalDate;

    move-result-object v0

    invoke-direct {p0, v0}, Lorg/threeten/bp/chrono/ThaiBuddhistDate;->a(Lorg/threeten/bp/LocalDate;)Lorg/threeten/bp/chrono/ThaiBuddhistDate;

    move-result-object p0

    goto :goto_0

    .line 264
    :pswitch_0
    invoke-virtual {p0}, Lorg/threeten/bp/chrono/ThaiBuddhistDate;->a()Lorg/threeten/bp/chrono/ThaiBuddhistChronology;

    move-result-object v1

    invoke-virtual {v1, v0}, Lorg/threeten/bp/chrono/ThaiBuddhistChronology;->a(Lorg/threeten/bp/temporal/ChronoField;)Lorg/threeten/bp/temporal/ValueRange;

    move-result-object v1

    invoke-virtual {v1, p2, p3, v0}, Lorg/threeten/bp/temporal/ValueRange;->a(JLorg/threeten/bp/temporal/TemporalField;)J

    .line 265
    invoke-direct {p0}, Lorg/threeten/bp/chrono/ThaiBuddhistDate;->d()J

    move-result-wide v0

    sub-long v0, p2, v0

    invoke-virtual {p0, v0, v1}, Lorg/threeten/bp/chrono/ThaiBuddhistDate;->e(J)Lorg/threeten/bp/chrono/ThaiBuddhistDate;

    move-result-object p0

    goto :goto_0

    .line 269
    :pswitch_1
    invoke-virtual {p0}, Lorg/threeten/bp/chrono/ThaiBuddhistDate;->a()Lorg/threeten/bp/chrono/ThaiBuddhistChronology;

    move-result-object v1

    invoke-virtual {v1, v0}, Lorg/threeten/bp/chrono/ThaiBuddhistChronology;->a(Lorg/threeten/bp/temporal/ChronoField;)Lorg/threeten/bp/temporal/ValueRange;

    move-result-object v1

    invoke-virtual {v1, p2, p3, v0}, Lorg/threeten/bp/temporal/ValueRange;->b(JLorg/threeten/bp/temporal/TemporalField;)I

    move-result v1

    .line 270
    sget-object v2, Lorg/threeten/bp/chrono/ThaiBuddhistDate$1;->a:[I

    invoke-virtual {v0}, Lorg/threeten/bp/temporal/ChronoField;->ordinal()I

    move-result v0

    aget v0, v2, v0

    packed-switch v0, :pswitch_data_1

    :pswitch_2
    goto :goto_1

    .line 272
    :pswitch_3
    iget-object v2, p0, Lorg/threeten/bp/chrono/ThaiBuddhistDate;->a:Lorg/threeten/bp/LocalDate;

    invoke-direct {p0}, Lorg/threeten/bp/chrono/ThaiBuddhistDate;->e()I

    move-result v0

    const/4 v3, 0x1

    if-lt v0, v3, :cond_1

    move v0, v1

    :goto_2
    add-int/lit16 v0, v0, -0x21f

    invoke-virtual {v2, v0}, Lorg/threeten/bp/LocalDate;->a(I)Lorg/threeten/bp/LocalDate;

    move-result-object v0

    invoke-direct {p0, v0}, Lorg/threeten/bp/chrono/ThaiBuddhistDate;->a(Lorg/threeten/bp/LocalDate;)Lorg/threeten/bp/chrono/ThaiBuddhistDate;

    move-result-object p0

    goto :goto_0

    :cond_1
    rsub-int/lit8 v0, v1, 0x1

    goto :goto_2

    .line 274
    :pswitch_4
    iget-object v0, p0, Lorg/threeten/bp/chrono/ThaiBuddhistDate;->a:Lorg/threeten/bp/LocalDate;

    add-int/lit16 v1, v1, -0x21f

    invoke-virtual {v0, v1}, Lorg/threeten/bp/LocalDate;->a(I)Lorg/threeten/bp/LocalDate;

    move-result-object v0

    invoke-direct {p0, v0}, Lorg/threeten/bp/chrono/ThaiBuddhistDate;->a(Lorg/threeten/bp/LocalDate;)Lorg/threeten/bp/chrono/ThaiBuddhistDate;

    move-result-object p0

    goto :goto_0

    .line 276
    :pswitch_5
    iget-object v0, p0, Lorg/threeten/bp/chrono/ThaiBuddhistDate;->a:Lorg/threeten/bp/LocalDate;

    invoke-direct {p0}, Lorg/threeten/bp/chrono/ThaiBuddhistDate;->e()I

    move-result v1

    rsub-int/lit8 v1, v1, 0x1

    add-int/lit16 v1, v1, -0x21f

    invoke-virtual {v0, v1}, Lorg/threeten/bp/LocalDate;->a(I)Lorg/threeten/bp/LocalDate;

    move-result-object v0

    invoke-direct {p0, v0}, Lorg/threeten/bp/chrono/ThaiBuddhistDate;->a(Lorg/threeten/bp/LocalDate;)Lorg/threeten/bp/chrono/ThaiBuddhistDate;

    move-result-object p0

    goto :goto_0

    .line 282
    :cond_2
    invoke-interface {p1, p0, p2, p3}, Lorg/threeten/bp/temporal/TemporalField;->a(Lorg/threeten/bp/temporal/Temporal;J)Lorg/threeten/bp/temporal/Temporal;

    move-result-object v0

    check-cast v0, Lorg/threeten/bp/chrono/ThaiBuddhistDate;

    move-object p0, v0

    goto/16 :goto_0

    .line 262
    nop

    :pswitch_data_0
    .packed-switch 0x4
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch

    .line 270
    :pswitch_data_1
    .packed-switch 0x4
        :pswitch_3
        :pswitch_2
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method a(Ljava/io/DataOutput;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 367
    sget-object v0, Lorg/threeten/bp/temporal/ChronoField;->A:Lorg/threeten/bp/temporal/ChronoField;

    invoke-virtual {p0, v0}, Lorg/threeten/bp/chrono/ThaiBuddhistDate;->c(Lorg/threeten/bp/temporal/TemporalField;)I

    move-result v0

    invoke-interface {p1, v0}, Ljava/io/DataOutput;->writeInt(I)V

    .line 368
    sget-object v0, Lorg/threeten/bp/temporal/ChronoField;->x:Lorg/threeten/bp/temporal/ChronoField;

    invoke-virtual {p0, v0}, Lorg/threeten/bp/chrono/ThaiBuddhistDate;->c(Lorg/threeten/bp/temporal/TemporalField;)I

    move-result v0

    invoke-interface {p1, v0}, Ljava/io/DataOutput;->writeByte(I)V

    .line 369
    sget-object v0, Lorg/threeten/bp/temporal/ChronoField;->s:Lorg/threeten/bp/temporal/ChronoField;

    invoke-virtual {p0, v0}, Lorg/threeten/bp/chrono/ThaiBuddhistDate;->c(Lorg/threeten/bp/temporal/TemporalField;)I

    move-result v0

    invoke-interface {p1, v0}, Ljava/io/DataOutput;->writeByte(I)V

    .line 370
    return-void
.end method

.method synthetic b(J)Lorg/threeten/bp/chrono/ChronoDateImpl;
    .locals 1

    .prologue
    .line 71
    invoke-virtual {p0, p1, p2}, Lorg/threeten/bp/chrono/ThaiBuddhistDate;->e(J)Lorg/threeten/bp/chrono/ThaiBuddhistDate;

    move-result-object v0

    return-object v0
.end method

.method public synthetic b(Lorg/threeten/bp/temporal/TemporalAmount;)Lorg/threeten/bp/chrono/ChronoLocalDate;
    .locals 1

    .prologue
    .line 71
    invoke-virtual {p0, p1}, Lorg/threeten/bp/chrono/ThaiBuddhistDate;->a(Lorg/threeten/bp/temporal/TemporalAmount;)Lorg/threeten/bp/chrono/ThaiBuddhistDate;

    move-result-object v0

    return-object v0
.end method

.method public final b(Lorg/threeten/bp/LocalTime;)Lorg/threeten/bp/chrono/ChronoLocalDateTime;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/threeten/bp/LocalTime;",
            ")",
            "Lorg/threeten/bp/chrono/ChronoLocalDateTime",
            "<",
            "Lorg/threeten/bp/chrono/ThaiBuddhistDate;",
            ">;"
        }
    .end annotation

    .prologue
    .line 328
    invoke-super {p0, p1}, Lorg/threeten/bp/chrono/ChronoDateImpl;->b(Lorg/threeten/bp/LocalTime;)Lorg/threeten/bp/chrono/ChronoLocalDateTime;

    move-result-object v0

    return-object v0
.end method

.method public b(JLorg/threeten/bp/temporal/TemporalUnit;)Lorg/threeten/bp/chrono/ThaiBuddhistDate;
    .locals 1

    .prologue
    .line 292
    invoke-super {p0, p1, p2, p3}, Lorg/threeten/bp/chrono/ChronoDateImpl;->a(JLorg/threeten/bp/temporal/TemporalUnit;)Lorg/threeten/bp/chrono/ChronoDateImpl;

    move-result-object v0

    check-cast v0, Lorg/threeten/bp/chrono/ThaiBuddhistDate;

    return-object v0
.end method

.method public b()Lorg/threeten/bp/chrono/ThaiBuddhistEra;
    .locals 1

    .prologue
    .line 190
    invoke-super {p0}, Lorg/threeten/bp/chrono/ChronoDateImpl;->c()Lorg/threeten/bp/chrono/Era;

    move-result-object v0

    check-cast v0, Lorg/threeten/bp/chrono/ThaiBuddhistEra;

    return-object v0
.end method

.method public synthetic b(Lorg/threeten/bp/temporal/TemporalAdjuster;)Lorg/threeten/bp/temporal/Temporal;
    .locals 1

    .prologue
    .line 71
    invoke-virtual {p0, p1}, Lorg/threeten/bp/chrono/ThaiBuddhistDate;->a(Lorg/threeten/bp/temporal/TemporalAdjuster;)Lorg/threeten/bp/chrono/ThaiBuddhistDate;

    move-result-object v0

    return-object v0
.end method

.method public synthetic b(Lorg/threeten/bp/temporal/TemporalField;J)Lorg/threeten/bp/temporal/Temporal;
    .locals 2

    .prologue
    .line 71
    invoke-virtual {p0, p1, p2, p3}, Lorg/threeten/bp/chrono/ThaiBuddhistDate;->a(Lorg/threeten/bp/temporal/TemporalField;J)Lorg/threeten/bp/chrono/ThaiBuddhistDate;

    move-result-object v0

    return-object v0
.end method

.method public b(Lorg/threeten/bp/temporal/TemporalField;)Lorg/threeten/bp/temporal/ValueRange;
    .locals 8

    .prologue
    const-wide/16 v6, 0x21f

    const-wide/16 v4, 0x1

    .line 200
    instance-of v0, p1, Lorg/threeten/bp/temporal/ChronoField;

    if-eqz v0, :cond_2

    .line 201
    invoke-virtual {p0, p1}, Lorg/threeten/bp/chrono/ThaiBuddhistDate;->a(Lorg/threeten/bp/temporal/TemporalField;)Z

    move-result v0

    if-eqz v0, :cond_1

    move-object v0, p1

    .line 202
    check-cast v0, Lorg/threeten/bp/temporal/ChronoField;

    .line 203
    sget-object v1, Lorg/threeten/bp/chrono/ThaiBuddhistDate$1;->a:[I

    invoke-virtual {v0}, Lorg/threeten/bp/temporal/ChronoField;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 214
    invoke-virtual {p0}, Lorg/threeten/bp/chrono/ThaiBuddhistDate;->a()Lorg/threeten/bp/chrono/ThaiBuddhistChronology;

    move-result-object v1

    invoke-virtual {v1, v0}, Lorg/threeten/bp/chrono/ThaiBuddhistChronology;->a(Lorg/threeten/bp/temporal/ChronoField;)Lorg/threeten/bp/temporal/ValueRange;

    move-result-object v0

    .line 218
    :goto_0
    return-object v0

    .line 207
    :pswitch_0
    iget-object v0, p0, Lorg/threeten/bp/chrono/ThaiBuddhistDate;->a:Lorg/threeten/bp/LocalDate;

    invoke-virtual {v0, p1}, Lorg/threeten/bp/LocalDate;->b(Lorg/threeten/bp/temporal/TemporalField;)Lorg/threeten/bp/temporal/ValueRange;

    move-result-object v0

    goto :goto_0

    .line 209
    :pswitch_1
    sget-object v0, Lorg/threeten/bp/temporal/ChronoField;->A:Lorg/threeten/bp/temporal/ChronoField;

    invoke-virtual {v0}, Lorg/threeten/bp/temporal/ChronoField;->a()Lorg/threeten/bp/temporal/ValueRange;

    move-result-object v0

    .line 210
    invoke-direct {p0}, Lorg/threeten/bp/chrono/ThaiBuddhistDate;->e()I

    move-result v1

    if-gtz v1, :cond_0

    invoke-virtual {v0}, Lorg/threeten/bp/temporal/ValueRange;->b()J

    move-result-wide v0

    add-long/2addr v0, v6

    neg-long v0, v0

    add-long/2addr v0, v4

    .line 211
    :goto_1
    invoke-static {v4, v5, v0, v1}, Lorg/threeten/bp/temporal/ValueRange;->a(JJ)Lorg/threeten/bp/temporal/ValueRange;

    move-result-object v0

    goto :goto_0

    .line 210
    :cond_0
    invoke-virtual {v0}, Lorg/threeten/bp/temporal/ValueRange;->c()J

    move-result-wide v0

    add-long/2addr v0, v6

    goto :goto_1

    .line 216
    :cond_1
    new-instance v0, Lorg/threeten/bp/temporal/UnsupportedTemporalTypeException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unsupported field: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/threeten/bp/temporal/UnsupportedTemporalTypeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 218
    :cond_2
    invoke-interface {p1, p0}, Lorg/threeten/bp/temporal/TemporalField;->b(Lorg/threeten/bp/temporal/TemporalAccessor;)Lorg/threeten/bp/temporal/ValueRange;

    move-result-object v0

    goto :goto_0

    .line 203
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method synthetic c(J)Lorg/threeten/bp/chrono/ChronoDateImpl;
    .locals 1

    .prologue
    .line 71
    invoke-virtual {p0, p1, p2}, Lorg/threeten/bp/chrono/ThaiBuddhistDate;->f(J)Lorg/threeten/bp/chrono/ThaiBuddhistDate;

    move-result-object v0

    return-object v0
.end method

.method public synthetic c(Lorg/threeten/bp/temporal/TemporalAdjuster;)Lorg/threeten/bp/chrono/ChronoLocalDate;
    .locals 1

    .prologue
    .line 71
    invoke-virtual {p0, p1}, Lorg/threeten/bp/chrono/ThaiBuddhistDate;->a(Lorg/threeten/bp/temporal/TemporalAdjuster;)Lorg/threeten/bp/chrono/ThaiBuddhistDate;

    move-result-object v0

    return-object v0
.end method

.method public synthetic c(Lorg/threeten/bp/temporal/TemporalField;J)Lorg/threeten/bp/chrono/ChronoLocalDate;
    .locals 2

    .prologue
    .line 71
    invoke-virtual {p0, p1, p2, p3}, Lorg/threeten/bp/chrono/ThaiBuddhistDate;->a(Lorg/threeten/bp/temporal/TemporalField;J)Lorg/threeten/bp/chrono/ThaiBuddhistDate;

    move-result-object v0

    return-object v0
.end method

.method public synthetic c()Lorg/threeten/bp/chrono/Era;
    .locals 1

    .prologue
    .line 71
    invoke-virtual {p0}, Lorg/threeten/bp/chrono/ThaiBuddhistDate;->b()Lorg/threeten/bp/chrono/ThaiBuddhistEra;

    move-result-object v0

    return-object v0
.end method

.method public synthetic c(JLorg/threeten/bp/temporal/TemporalUnit;)Lorg/threeten/bp/temporal/Temporal;
    .locals 1

    .prologue
    .line 71
    invoke-virtual {p0, p1, p2, p3}, Lorg/threeten/bp/chrono/ThaiBuddhistDate;->g(JLorg/threeten/bp/temporal/TemporalUnit;)Lorg/threeten/bp/chrono/ThaiBuddhistDate;

    move-result-object v0

    return-object v0
.end method

.method public synthetic c(Lorg/threeten/bp/temporal/TemporalAmount;)Lorg/threeten/bp/temporal/Temporal;
    .locals 1

    .prologue
    .line 71
    invoke-virtual {p0, p1}, Lorg/threeten/bp/chrono/ThaiBuddhistDate;->a(Lorg/threeten/bp/temporal/TemporalAmount;)Lorg/threeten/bp/chrono/ThaiBuddhistDate;

    move-result-object v0

    return-object v0
.end method

.method public d(Lorg/threeten/bp/temporal/TemporalField;)J
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 223
    instance-of v0, p1, Lorg/threeten/bp/temporal/ChronoField;

    if-eqz v0, :cond_2

    .line 224
    sget-object v2, Lorg/threeten/bp/chrono/ThaiBuddhistDate$1;->a:[I

    move-object v0, p1

    check-cast v0, Lorg/threeten/bp/temporal/ChronoField;

    invoke-virtual {v0}, Lorg/threeten/bp/temporal/ChronoField;->ordinal()I

    move-result v0

    aget v0, v2, v0

    packed-switch v0, :pswitch_data_0

    .line 236
    iget-object v0, p0, Lorg/threeten/bp/chrono/ThaiBuddhistDate;->a:Lorg/threeten/bp/LocalDate;

    invoke-virtual {v0, p1}, Lorg/threeten/bp/LocalDate;->d(Lorg/threeten/bp/temporal/TemporalField;)J

    move-result-wide v0

    .line 238
    :goto_0
    return-wide v0

    .line 226
    :pswitch_0
    invoke-direct {p0}, Lorg/threeten/bp/chrono/ThaiBuddhistDate;->d()J

    move-result-wide v0

    goto :goto_0

    .line 228
    :pswitch_1
    invoke-direct {p0}, Lorg/threeten/bp/chrono/ThaiBuddhistDate;->e()I

    move-result v0

    .line 229
    if-lt v0, v1, :cond_0

    :goto_1
    int-to-long v0, v0

    goto :goto_0

    :cond_0
    rsub-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 232
    :pswitch_2
    invoke-direct {p0}, Lorg/threeten/bp/chrono/ThaiBuddhistDate;->e()I

    move-result v0

    int-to-long v0, v0

    goto :goto_0

    .line 234
    :pswitch_3
    invoke-direct {p0}, Lorg/threeten/bp/chrono/ThaiBuddhistDate;->e()I

    move-result v0

    if-lt v0, v1, :cond_1

    move v0, v1

    :goto_2
    int-to-long v0, v0

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_2

    .line 238
    :cond_2
    invoke-interface {p1, p0}, Lorg/threeten/bp/temporal/TemporalField;->c(Lorg/threeten/bp/temporal/TemporalAccessor;)J

    move-result-wide v0

    goto :goto_0

    .line 224
    :pswitch_data_0
    .packed-switch 0x4
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method d(J)Lorg/threeten/bp/chrono/ThaiBuddhistDate;
    .locals 1

    .prologue
    .line 308
    iget-object v0, p0, Lorg/threeten/bp/chrono/ThaiBuddhistDate;->a:Lorg/threeten/bp/LocalDate;

    invoke-virtual {v0, p1, p2}, Lorg/threeten/bp/LocalDate;->b(J)Lorg/threeten/bp/LocalDate;

    move-result-object v0

    invoke-direct {p0, v0}, Lorg/threeten/bp/chrono/ThaiBuddhistDate;->a(Lorg/threeten/bp/LocalDate;)Lorg/threeten/bp/chrono/ThaiBuddhistDate;

    move-result-object v0

    return-object v0
.end method

.method public synthetic d(JLorg/threeten/bp/temporal/TemporalUnit;)Lorg/threeten/bp/temporal/Temporal;
    .locals 1

    .prologue
    .line 71
    invoke-virtual {p0, p1, p2, p3}, Lorg/threeten/bp/chrono/ThaiBuddhistDate;->b(JLorg/threeten/bp/temporal/TemporalUnit;)Lorg/threeten/bp/chrono/ThaiBuddhistDate;

    move-result-object v0

    return-object v0
.end method

.method public synthetic e(JLorg/threeten/bp/temporal/TemporalUnit;)Lorg/threeten/bp/chrono/ChronoLocalDate;
    .locals 1

    .prologue
    .line 71
    invoke-virtual {p0, p1, p2, p3}, Lorg/threeten/bp/chrono/ThaiBuddhistDate;->g(JLorg/threeten/bp/temporal/TemporalUnit;)Lorg/threeten/bp/chrono/ThaiBuddhistDate;

    move-result-object v0

    return-object v0
.end method

.method e(J)Lorg/threeten/bp/chrono/ThaiBuddhistDate;
    .locals 1

    .prologue
    .line 313
    iget-object v0, p0, Lorg/threeten/bp/chrono/ThaiBuddhistDate;->a:Lorg/threeten/bp/LocalDate;

    invoke-virtual {v0, p1, p2}, Lorg/threeten/bp/LocalDate;->c(J)Lorg/threeten/bp/LocalDate;

    move-result-object v0

    invoke-direct {p0, v0}, Lorg/threeten/bp/chrono/ThaiBuddhistDate;->a(Lorg/threeten/bp/LocalDate;)Lorg/threeten/bp/chrono/ThaiBuddhistDate;

    move-result-object v0

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 345
    if-ne p0, p1, :cond_0

    .line 346
    const/4 v0, 0x1

    .line 352
    :goto_0
    return v0

    .line 348
    :cond_0
    instance-of v0, p1, Lorg/threeten/bp/chrono/ThaiBuddhistDate;

    if-eqz v0, :cond_1

    .line 349
    check-cast p1, Lorg/threeten/bp/chrono/ThaiBuddhistDate;

    .line 350
    iget-object v0, p0, Lorg/threeten/bp/chrono/ThaiBuddhistDate;->a:Lorg/threeten/bp/LocalDate;

    iget-object v1, p1, Lorg/threeten/bp/chrono/ThaiBuddhistDate;->a:Lorg/threeten/bp/LocalDate;

    invoke-virtual {v0, v1}, Lorg/threeten/bp/LocalDate;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0

    .line 352
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public synthetic f(JLorg/threeten/bp/temporal/TemporalUnit;)Lorg/threeten/bp/chrono/ChronoLocalDate;
    .locals 1

    .prologue
    .line 71
    invoke-virtual {p0, p1, p2, p3}, Lorg/threeten/bp/chrono/ThaiBuddhistDate;->b(JLorg/threeten/bp/temporal/TemporalUnit;)Lorg/threeten/bp/chrono/ThaiBuddhistDate;

    move-result-object v0

    return-object v0
.end method

.method f(J)Lorg/threeten/bp/chrono/ThaiBuddhistDate;
    .locals 1

    .prologue
    .line 318
    iget-object v0, p0, Lorg/threeten/bp/chrono/ThaiBuddhistDate;->a:Lorg/threeten/bp/LocalDate;

    invoke-virtual {v0, p1, p2}, Lorg/threeten/bp/LocalDate;->e(J)Lorg/threeten/bp/LocalDate;

    move-result-object v0

    invoke-direct {p0, v0}, Lorg/threeten/bp/chrono/ThaiBuddhistDate;->a(Lorg/threeten/bp/LocalDate;)Lorg/threeten/bp/chrono/ThaiBuddhistDate;

    move-result-object v0

    return-object v0
.end method

.method public g(JLorg/threeten/bp/temporal/TemporalUnit;)Lorg/threeten/bp/chrono/ThaiBuddhistDate;
    .locals 1

    .prologue
    .line 302
    invoke-super {p0, p1, p2, p3}, Lorg/threeten/bp/chrono/ChronoDateImpl;->e(JLorg/threeten/bp/temporal/TemporalUnit;)Lorg/threeten/bp/chrono/ChronoLocalDate;

    move-result-object v0

    check-cast v0, Lorg/threeten/bp/chrono/ThaiBuddhistDate;

    return-object v0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 357
    invoke-virtual {p0}, Lorg/threeten/bp/chrono/ThaiBuddhistDate;->a()Lorg/threeten/bp/chrono/ThaiBuddhistChronology;

    move-result-object v0

    invoke-virtual {v0}, Lorg/threeten/bp/chrono/ThaiBuddhistChronology;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    iget-object v1, p0, Lorg/threeten/bp/chrono/ThaiBuddhistDate;->a:Lorg/threeten/bp/LocalDate;

    invoke-virtual {v1}, Lorg/threeten/bp/LocalDate;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    return v0
.end method

.method public n()J
    .locals 2

    .prologue
    .line 339
    iget-object v0, p0, Lorg/threeten/bp/chrono/ThaiBuddhistDate;->a:Lorg/threeten/bp/LocalDate;

    invoke-virtual {v0}, Lorg/threeten/bp/LocalDate;->n()J

    move-result-wide v0

    return-wide v0
.end method

.method public synthetic o()Lorg/threeten/bp/chrono/Chronology;
    .locals 1

    .prologue
    .line 71
    invoke-virtual {p0}, Lorg/threeten/bp/chrono/ThaiBuddhistDate;->a()Lorg/threeten/bp/chrono/ThaiBuddhistChronology;

    move-result-object v0

    return-object v0
.end method
