.class public final Lorg/threeten/bp/chrono/ThaiBuddhistChronology;
.super Lorg/threeten/bp/chrono/Chronology;
.source "ThaiBuddhistChronology.java"

# interfaces
.implements Ljava/io/Serializable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/threeten/bp/chrono/ThaiBuddhistChronology$1;
    }
.end annotation


# static fields
.field public static final b:Lorg/threeten/bp/chrono/ThaiBuddhistChronology;

.field private static final c:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "[",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final d:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "[",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final e:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "[",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 103
    new-instance v0, Lorg/threeten/bp/chrono/ThaiBuddhistChronology;

    invoke-direct {v0}, Lorg/threeten/bp/chrono/ThaiBuddhistChronology;-><init>()V

    sput-object v0, Lorg/threeten/bp/chrono/ThaiBuddhistChronology;->b:Lorg/threeten/bp/chrono/ThaiBuddhistChronology;

    .line 116
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lorg/threeten/bp/chrono/ThaiBuddhistChronology;->c:Ljava/util/HashMap;

    .line 120
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lorg/threeten/bp/chrono/ThaiBuddhistChronology;->d:Ljava/util/HashMap;

    .line 124
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lorg/threeten/bp/chrono/ThaiBuddhistChronology;->e:Ljava/util/HashMap;

    .line 137
    sget-object v0, Lorg/threeten/bp/chrono/ThaiBuddhistChronology;->c:Ljava/util/HashMap;

    const-string v1, "en"

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "BB"

    aput-object v3, v2, v4

    const-string v3, "BE"

    aput-object v3, v2, v5

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 138
    sget-object v0, Lorg/threeten/bp/chrono/ThaiBuddhistChronology;->c:Ljava/util/HashMap;

    const-string v1, "th"

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "BB"

    aput-object v3, v2, v4

    const-string v3, "BE"

    aput-object v3, v2, v5

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 139
    sget-object v0, Lorg/threeten/bp/chrono/ThaiBuddhistChronology;->d:Ljava/util/HashMap;

    const-string v1, "en"

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "B.B."

    aput-object v3, v2, v4

    const-string v3, "B.E."

    aput-object v3, v2, v5

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 140
    sget-object v0, Lorg/threeten/bp/chrono/ThaiBuddhistChronology;->d:Ljava/util/HashMap;

    const-string v1, "th"

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "\u0e1e.\u0e28."

    aput-object v3, v2, v4

    const-string v3, "\u0e1b\u0e35\u0e01\u0e48\u0e2d\u0e19\u0e04\u0e23\u0e34\u0e2a\u0e15\u0e4c\u0e01\u0e32\u0e25\u0e17\u0e35\u0e48"

    aput-object v3, v2, v5

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 143
    sget-object v0, Lorg/threeten/bp/chrono/ThaiBuddhistChronology;->e:Ljava/util/HashMap;

    const-string v1, "en"

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "Before Buddhist"

    aput-object v3, v2, v4

    const-string v3, "Budhhist Era"

    aput-object v3, v2, v5

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 144
    sget-object v0, Lorg/threeten/bp/chrono/ThaiBuddhistChronology;->e:Ljava/util/HashMap;

    const-string v1, "th"

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "\u0e1e\u0e38\u0e17\u0e18\u0e28\u0e31\u0e01\u0e23\u0e32\u0e0a"

    aput-object v3, v2, v4

    const-string v3, "\u0e1b\u0e35\u0e01\u0e48\u0e2d\u0e19\u0e04\u0e23\u0e34\u0e2a\u0e15\u0e4c\u0e01\u0e32\u0e25\u0e17\u0e35\u0e48"

    aput-object v3, v2, v5

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 147
    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 152
    invoke-direct {p0}, Lorg/threeten/bp/chrono/Chronology;-><init>()V

    .line 153
    return-void
.end method

.method private readResolve()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 161
    sget-object v0, Lorg/threeten/bp/chrono/ThaiBuddhistChronology;->b:Lorg/threeten/bp/chrono/ThaiBuddhistChronology;

    return-object v0
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 176
    const-string v0, "ThaiBuddhist"

    return-object v0
.end method

.method public synthetic a(III)Lorg/threeten/bp/chrono/ChronoLocalDate;
    .locals 1

    .prologue
    .line 98
    invoke-virtual {p0, p1, p2, p3}, Lorg/threeten/bp/chrono/ThaiBuddhistChronology;->b(III)Lorg/threeten/bp/chrono/ThaiBuddhistDate;

    move-result-object v0

    return-object v0
.end method

.method public a(Lorg/threeten/bp/Instant;Lorg/threeten/bp/ZoneId;)Lorg/threeten/bp/chrono/ChronoZonedDateTime;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/threeten/bp/Instant;",
            "Lorg/threeten/bp/ZoneId;",
            ")",
            "Lorg/threeten/bp/chrono/ChronoZonedDateTime",
            "<",
            "Lorg/threeten/bp/chrono/ThaiBuddhistDate;",
            ">;"
        }
    .end annotation

    .prologue
    .line 246
    invoke-super {p0, p1, p2}, Lorg/threeten/bp/chrono/Chronology;->a(Lorg/threeten/bp/Instant;Lorg/threeten/bp/ZoneId;)Lorg/threeten/bp/chrono/ChronoZonedDateTime;

    move-result-object v0

    return-object v0
.end method

.method public synthetic a(I)Lorg/threeten/bp/chrono/Era;
    .locals 1

    .prologue
    .line 98
    invoke-virtual {p0, p1}, Lorg/threeten/bp/chrono/ThaiBuddhistChronology;->b(I)Lorg/threeten/bp/chrono/ThaiBuddhistEra;

    move-result-object v0

    return-object v0
.end method

.method public a(Lorg/threeten/bp/temporal/ChronoField;)Lorg/threeten/bp/temporal/ValueRange;
    .locals 8

    .prologue
    const-wide/16 v4, 0x1974

    const-wide/16 v0, 0x1

    const-wide/16 v6, 0x21f

    .line 303
    sget-object v2, Lorg/threeten/bp/chrono/ThaiBuddhistChronology$1;->a:[I

    invoke-virtual {p1}, Lorg/threeten/bp/temporal/ChronoField;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 317
    invoke-virtual {p1}, Lorg/threeten/bp/temporal/ChronoField;->a()Lorg/threeten/bp/temporal/ValueRange;

    move-result-object v0

    :goto_0
    return-object v0

    .line 305
    :pswitch_0
    sget-object v0, Lorg/threeten/bp/temporal/ChronoField;->y:Lorg/threeten/bp/temporal/ChronoField;

    invoke-virtual {v0}, Lorg/threeten/bp/temporal/ChronoField;->a()Lorg/threeten/bp/temporal/ValueRange;

    move-result-object v0

    .line 306
    invoke-virtual {v0}, Lorg/threeten/bp/temporal/ValueRange;->b()J

    move-result-wide v2

    add-long/2addr v2, v4

    invoke-virtual {v0}, Lorg/threeten/bp/temporal/ValueRange;->c()J

    move-result-wide v0

    add-long/2addr v0, v4

    invoke-static {v2, v3, v0, v1}, Lorg/threeten/bp/temporal/ValueRange;->a(JJ)Lorg/threeten/bp/temporal/ValueRange;

    move-result-object v0

    goto :goto_0

    .line 309
    :pswitch_1
    sget-object v2, Lorg/threeten/bp/temporal/ChronoField;->A:Lorg/threeten/bp/temporal/ChronoField;

    invoke-virtual {v2}, Lorg/threeten/bp/temporal/ChronoField;->a()Lorg/threeten/bp/temporal/ValueRange;

    move-result-object v4

    .line 310
    invoke-virtual {v4}, Lorg/threeten/bp/temporal/ValueRange;->b()J

    move-result-wide v2

    add-long/2addr v2, v6

    neg-long v2, v2

    add-long/2addr v2, v0

    invoke-virtual {v4}, Lorg/threeten/bp/temporal/ValueRange;->c()J

    move-result-wide v4

    add-long/2addr v4, v6

    invoke-static/range {v0 .. v5}, Lorg/threeten/bp/temporal/ValueRange;->a(JJJ)Lorg/threeten/bp/temporal/ValueRange;

    move-result-object v0

    goto :goto_0

    .line 313
    :pswitch_2
    sget-object v0, Lorg/threeten/bp/temporal/ChronoField;->A:Lorg/threeten/bp/temporal/ChronoField;

    invoke-virtual {v0}, Lorg/threeten/bp/temporal/ChronoField;->a()Lorg/threeten/bp/temporal/ValueRange;

    move-result-object v0

    .line 314
    invoke-virtual {v0}, Lorg/threeten/bp/temporal/ValueRange;->b()J

    move-result-wide v2

    add-long/2addr v2, v6

    invoke-virtual {v0}, Lorg/threeten/bp/temporal/ValueRange;->c()J

    move-result-wide v0

    add-long/2addr v0, v6

    invoke-static {v2, v3, v0, v1}, Lorg/threeten/bp/temporal/ValueRange;->a(JJ)Lorg/threeten/bp/temporal/ValueRange;

    move-result-object v0

    goto :goto_0

    .line 303
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public a(J)Z
    .locals 5

    .prologue
    .line 279
    sget-object v0, Lorg/threeten/bp/chrono/IsoChronology;->b:Lorg/threeten/bp/chrono/IsoChronology;

    const-wide/16 v2, 0x21f

    sub-long v2, p1, v2

    invoke-virtual {v0, v2, v3}, Lorg/threeten/bp/chrono/IsoChronology;->a(J)Z

    move-result v0

    return v0
.end method

.method public b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 193
    const-string v0, "buddhist"

    return-object v0
.end method

.method public synthetic b(Lorg/threeten/bp/temporal/TemporalAccessor;)Lorg/threeten/bp/chrono/ChronoLocalDate;
    .locals 1

    .prologue
    .line 98
    invoke-virtual {p0, p1}, Lorg/threeten/bp/chrono/ThaiBuddhistChronology;->e(Lorg/threeten/bp/temporal/TemporalAccessor;)Lorg/threeten/bp/chrono/ThaiBuddhistDate;

    move-result-object v0

    return-object v0
.end method

.method public b(III)Lorg/threeten/bp/chrono/ThaiBuddhistDate;
    .locals 2

    .prologue
    .line 204
    new-instance v0, Lorg/threeten/bp/chrono/ThaiBuddhistDate;

    add-int/lit16 v1, p1, -0x21f

    invoke-static {v1, p2, p3}, Lorg/threeten/bp/LocalDate;->a(III)Lorg/threeten/bp/LocalDate;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/threeten/bp/chrono/ThaiBuddhistDate;-><init>(Lorg/threeten/bp/LocalDate;)V

    return-object v0
.end method

.method public b(I)Lorg/threeten/bp/chrono/ThaiBuddhistEra;
    .locals 1

    .prologue
    .line 292
    invoke-static {p1}, Lorg/threeten/bp/chrono/ThaiBuddhistEra;->a(I)Lorg/threeten/bp/chrono/ThaiBuddhistEra;

    move-result-object v0

    return-object v0
.end method

.method public c(Lorg/threeten/bp/temporal/TemporalAccessor;)Lorg/threeten/bp/chrono/ChronoLocalDateTime;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/threeten/bp/temporal/TemporalAccessor;",
            ")",
            "Lorg/threeten/bp/chrono/ChronoLocalDateTime",
            "<",
            "Lorg/threeten/bp/chrono/ThaiBuddhistDate;",
            ">;"
        }
    .end annotation

    .prologue
    .line 234
    invoke-super {p0, p1}, Lorg/threeten/bp/chrono/Chronology;->c(Lorg/threeten/bp/temporal/TemporalAccessor;)Lorg/threeten/bp/chrono/ChronoLocalDateTime;

    move-result-object v0

    return-object v0
.end method

.method public d(Lorg/threeten/bp/temporal/TemporalAccessor;)Lorg/threeten/bp/chrono/ChronoZonedDateTime;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/threeten/bp/temporal/TemporalAccessor;",
            ")",
            "Lorg/threeten/bp/chrono/ChronoZonedDateTime",
            "<",
            "Lorg/threeten/bp/chrono/ThaiBuddhistDate;",
            ">;"
        }
    .end annotation

    .prologue
    .line 240
    invoke-super {p0, p1}, Lorg/threeten/bp/chrono/Chronology;->d(Lorg/threeten/bp/temporal/TemporalAccessor;)Lorg/threeten/bp/chrono/ChronoZonedDateTime;

    move-result-object v0

    return-object v0
.end method

.method public e(Lorg/threeten/bp/temporal/TemporalAccessor;)Lorg/threeten/bp/chrono/ThaiBuddhistDate;
    .locals 2

    .prologue
    .line 225
    instance-of v0, p1, Lorg/threeten/bp/chrono/ThaiBuddhistDate;

    if-eqz v0, :cond_0

    .line 226
    check-cast p1, Lorg/threeten/bp/chrono/ThaiBuddhistDate;

    .line 228
    :goto_0
    return-object p1

    :cond_0
    new-instance v0, Lorg/threeten/bp/chrono/ThaiBuddhistDate;

    invoke-static {p1}, Lorg/threeten/bp/LocalDate;->a(Lorg/threeten/bp/temporal/TemporalAccessor;)Lorg/threeten/bp/LocalDate;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/threeten/bp/chrono/ThaiBuddhistDate;-><init>(Lorg/threeten/bp/LocalDate;)V

    move-object p1, v0

    goto :goto_0
.end method
