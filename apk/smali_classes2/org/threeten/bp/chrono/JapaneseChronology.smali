.class public final Lorg/threeten/bp/chrono/JapaneseChronology;
.super Lorg/threeten/bp/chrono/Chronology;
.source "JapaneseChronology.java"

# interfaces
.implements Ljava/io/Serializable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/threeten/bp/chrono/JapaneseChronology$1;
    }
.end annotation


# static fields
.field static final b:Ljava/util/Locale;

.field public static final c:Lorg/threeten/bp/chrono/JapaneseChronology;

.field private static final d:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "[",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final e:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "[",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final f:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "[",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 10

    .prologue
    const/4 v9, 0x4

    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 104
    new-instance v0, Ljava/util/Locale;

    const-string v1, "ja"

    const-string v2, "JP"

    const-string v3, "JP"

    invoke-direct {v0, v1, v2, v3}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lorg/threeten/bp/chrono/JapaneseChronology;->b:Ljava/util/Locale;

    .line 109
    new-instance v0, Lorg/threeten/bp/chrono/JapaneseChronology;

    invoke-direct {v0}, Lorg/threeten/bp/chrono/JapaneseChronology;-><init>()V

    sput-object v0, Lorg/threeten/bp/chrono/JapaneseChronology;->c:Lorg/threeten/bp/chrono/JapaneseChronology;

    .line 119
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lorg/threeten/bp/chrono/JapaneseChronology;->d:Ljava/util/Map;

    .line 123
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lorg/threeten/bp/chrono/JapaneseChronology;->e:Ljava/util/Map;

    .line 127
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lorg/threeten/bp/chrono/JapaneseChronology;->f:Ljava/util/Map;

    .line 142
    sget-object v0, Lorg/threeten/bp/chrono/JapaneseChronology;->d:Ljava/util/Map;

    const-string v1, "en"

    const/4 v2, 0x6

    new-array v2, v2, [Ljava/lang/String;

    const-string v3, "Unknown"

    aput-object v3, v2, v5

    const-string v3, "K"

    aput-object v3, v2, v6

    const-string v3, "M"

    aput-object v3, v2, v7

    const-string v3, "T"

    aput-object v3, v2, v8

    const-string v3, "S"

    aput-object v3, v2, v9

    const/4 v3, 0x5

    const-string v4, "H"

    aput-object v4, v2, v3

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 143
    sget-object v0, Lorg/threeten/bp/chrono/JapaneseChronology;->d:Ljava/util/Map;

    const-string v1, "ja"

    const/4 v2, 0x6

    new-array v2, v2, [Ljava/lang/String;

    const-string v3, "Unknown"

    aput-object v3, v2, v5

    const-string v3, "K"

    aput-object v3, v2, v6

    const-string v3, "M"

    aput-object v3, v2, v7

    const-string v3, "T"

    aput-object v3, v2, v8

    const-string v3, "S"

    aput-object v3, v2, v9

    const/4 v3, 0x5

    const-string v4, "H"

    aput-object v4, v2, v3

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 144
    sget-object v0, Lorg/threeten/bp/chrono/JapaneseChronology;->e:Ljava/util/Map;

    const-string v1, "en"

    const/4 v2, 0x6

    new-array v2, v2, [Ljava/lang/String;

    const-string v3, "Unknown"

    aput-object v3, v2, v5

    const-string v3, "K"

    aput-object v3, v2, v6

    const-string v3, "M"

    aput-object v3, v2, v7

    const-string v3, "T"

    aput-object v3, v2, v8

    const-string v3, "S"

    aput-object v3, v2, v9

    const/4 v3, 0x5

    const-string v4, "H"

    aput-object v4, v2, v3

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 145
    sget-object v0, Lorg/threeten/bp/chrono/JapaneseChronology;->e:Ljava/util/Map;

    const-string v1, "ja"

    const/4 v2, 0x6

    new-array v2, v2, [Ljava/lang/String;

    const-string v3, "Unknown"

    aput-object v3, v2, v5

    const-string v3, "\u6176"

    aput-object v3, v2, v6

    const-string v3, "\u660e"

    aput-object v3, v2, v7

    const-string v3, "\u5927"

    aput-object v3, v2, v8

    const-string v3, "\u662d"

    aput-object v3, v2, v9

    const/4 v3, 0x5

    const-string v4, "\u5e73"

    aput-object v4, v2, v3

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 146
    sget-object v0, Lorg/threeten/bp/chrono/JapaneseChronology;->f:Ljava/util/Map;

    const-string v1, "en"

    const/4 v2, 0x6

    new-array v2, v2, [Ljava/lang/String;

    const-string v3, "Unknown"

    aput-object v3, v2, v5

    const-string v3, "Keio"

    aput-object v3, v2, v6

    const-string v3, "Meiji"

    aput-object v3, v2, v7

    const-string v3, "Taisho"

    aput-object v3, v2, v8

    const-string v3, "Showa"

    aput-object v3, v2, v9

    const/4 v3, 0x5

    const-string v4, "Heisei"

    aput-object v4, v2, v3

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 147
    sget-object v0, Lorg/threeten/bp/chrono/JapaneseChronology;->f:Ljava/util/Map;

    const-string v1, "ja"

    const/4 v2, 0x6

    new-array v2, v2, [Ljava/lang/String;

    const-string v3, "Unknown"

    aput-object v3, v2, v5

    const-string v3, "\u6176\u5fdc"

    aput-object v3, v2, v6

    const-string v3, "\u660e\u6cbb"

    aput-object v3, v2, v7

    const-string v3, "\u5927\u6b63"

    aput-object v3, v2, v8

    const-string v3, "\u662d\u548c"

    aput-object v3, v2, v9

    const/4 v3, 0x5

    const-string v4, "\u5e73\u6210"

    aput-object v4, v2, v3

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 149
    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 155
    invoke-direct {p0}, Lorg/threeten/bp/chrono/Chronology;-><init>()V

    .line 156
    return-void
.end method

.method private readResolve()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 164
    sget-object v0, Lorg/threeten/bp/chrono/JapaneseChronology;->c:Lorg/threeten/bp/chrono/JapaneseChronology;

    return-object v0
.end method


# virtual methods
.method public a(Lorg/threeten/bp/chrono/Era;I)I
    .locals 6

    .prologue
    .line 329
    instance-of v0, p1, Lorg/threeten/bp/chrono/JapaneseEra;

    if-nez v0, :cond_0

    .line 330
    new-instance v0, Ljava/lang/ClassCastException;

    const-string v1, "Era must be JapaneseEra"

    invoke-direct {v0, v1}, Ljava/lang/ClassCastException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 332
    :cond_0
    check-cast p1, Lorg/threeten/bp/chrono/JapaneseEra;

    .line 333
    invoke-virtual {p1}, Lorg/threeten/bp/chrono/JapaneseEra;->c()Lorg/threeten/bp/LocalDate;

    move-result-object v0

    invoke-virtual {v0}, Lorg/threeten/bp/LocalDate;->d()I

    move-result v0

    add-int/2addr v0, p2

    add-int/lit8 v0, v0, -0x1

    .line 334
    const-wide/16 v2, 0x1

    invoke-virtual {p1}, Lorg/threeten/bp/chrono/JapaneseEra;->d()Lorg/threeten/bp/LocalDate;

    move-result-object v1

    invoke-virtual {v1}, Lorg/threeten/bp/LocalDate;->d()I

    move-result v1

    invoke-virtual {p1}, Lorg/threeten/bp/chrono/JapaneseEra;->c()Lorg/threeten/bp/LocalDate;

    move-result-object v4

    invoke-virtual {v4}, Lorg/threeten/bp/LocalDate;->d()I

    move-result v4

    sub-int/2addr v1, v4

    add-int/lit8 v1, v1, 0x1

    int-to-long v4, v1

    invoke-static {v2, v3, v4, v5}, Lorg/threeten/bp/temporal/ValueRange;->a(JJ)Lorg/threeten/bp/temporal/ValueRange;

    move-result-object v1

    .line 335
    int-to-long v2, p2

    sget-object v4, Lorg/threeten/bp/temporal/ChronoField;->z:Lorg/threeten/bp/temporal/ChronoField;

    invoke-virtual {v1, v2, v3, v4}, Lorg/threeten/bp/temporal/ValueRange;->a(JLorg/threeten/bp/temporal/TemporalField;)J

    .line 336
    return v0
.end method

.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 179
    const-string v0, "Japanese"

    return-object v0
.end method

.method public synthetic a(III)Lorg/threeten/bp/chrono/ChronoLocalDate;
    .locals 1

    .prologue
    .line 101
    invoke-virtual {p0, p1, p2, p3}, Lorg/threeten/bp/chrono/JapaneseChronology;->b(III)Lorg/threeten/bp/chrono/JapaneseDate;

    move-result-object v0

    return-object v0
.end method

.method public a(Lorg/threeten/bp/Instant;Lorg/threeten/bp/ZoneId;)Lorg/threeten/bp/chrono/ChronoZonedDateTime;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/threeten/bp/Instant;",
            "Lorg/threeten/bp/ZoneId;",
            ")",
            "Lorg/threeten/bp/chrono/ChronoZonedDateTime",
            "<",
            "Lorg/threeten/bp/chrono/JapaneseDate;",
            ">;"
        }
    .end annotation

    .prologue
    .line 291
    invoke-super {p0, p1, p2}, Lorg/threeten/bp/chrono/Chronology;->a(Lorg/threeten/bp/Instant;Lorg/threeten/bp/ZoneId;)Lorg/threeten/bp/chrono/ChronoZonedDateTime;

    move-result-object v0

    return-object v0
.end method

.method public synthetic a(I)Lorg/threeten/bp/chrono/Era;
    .locals 1

    .prologue
    .line 101
    invoke-virtual {p0, p1}, Lorg/threeten/bp/chrono/JapaneseChronology;->b(I)Lorg/threeten/bp/chrono/JapaneseEra;

    move-result-object v0

    return-object v0
.end method

.method public a(Lorg/threeten/bp/temporal/ChronoField;)Lorg/threeten/bp/temporal/ValueRange;
    .locals 8

    .prologue
    const-wide/16 v0, 0x1

    const/4 v2, 0x0

    const/4 v7, 0x2

    .line 363
    sget-object v3, Lorg/threeten/bp/chrono/JapaneseChronology$1;->a:[I

    invoke-virtual {p1}, Lorg/threeten/bp/temporal/ChronoField;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    .line 384
    sget-object v3, Lorg/threeten/bp/chrono/JapaneseChronology;->b:Ljava/util/Locale;

    invoke-static {v3}, Ljava/util/Calendar;->getInstance(Ljava/util/Locale;)Ljava/util/Calendar;

    move-result-object v6

    .line 385
    sget-object v3, Lorg/threeten/bp/chrono/JapaneseChronology$1;->a:[I

    invoke-virtual {p1}, Lorg/threeten/bp/temporal/ChronoField;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_1

    .line 417
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unimplementable field: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 382
    :pswitch_0
    invoke-virtual {p1}, Lorg/threeten/bp/temporal/ChronoField;->a()Lorg/threeten/bp/temporal/ValueRange;

    move-result-object v0

    .line 413
    :goto_0
    return-object v0

    .line 387
    :pswitch_1
    invoke-static {}, Lorg/threeten/bp/chrono/JapaneseEra;->b()[Lorg/threeten/bp/chrono/JapaneseEra;

    move-result-object v0

    .line 388
    aget-object v1, v0, v2

    invoke-virtual {v1}, Lorg/threeten/bp/chrono/JapaneseEra;->a()I

    move-result v1

    int-to-long v2, v1

    array-length v1, v0

    add-int/lit8 v1, v1, -0x1

    aget-object v0, v0, v1

    invoke-virtual {v0}, Lorg/threeten/bp/chrono/JapaneseEra;->a()I

    move-result v0

    int-to-long v0, v0

    invoke-static {v2, v3, v0, v1}, Lorg/threeten/bp/temporal/ValueRange;->a(JJ)Lorg/threeten/bp/temporal/ValueRange;

    move-result-object v0

    goto :goto_0

    .line 391
    :pswitch_2
    invoke-static {}, Lorg/threeten/bp/chrono/JapaneseEra;->b()[Lorg/threeten/bp/chrono/JapaneseEra;

    move-result-object v0

    .line 392
    sget-object v1, Lorg/threeten/bp/chrono/JapaneseDate;->a:Lorg/threeten/bp/LocalDate;

    invoke-virtual {v1}, Lorg/threeten/bp/LocalDate;->d()I

    move-result v1

    int-to-long v2, v1

    array-length v1, v0

    add-int/lit8 v1, v1, -0x1

    aget-object v0, v0, v1

    invoke-virtual {v0}, Lorg/threeten/bp/chrono/JapaneseEra;->d()Lorg/threeten/bp/LocalDate;

    move-result-object v0

    invoke-virtual {v0}, Lorg/threeten/bp/LocalDate;->d()I

    move-result v0

    int-to-long v0, v0

    invoke-static {v2, v3, v0, v1}, Lorg/threeten/bp/temporal/ValueRange;->a(JJ)Lorg/threeten/bp/temporal/ValueRange;

    move-result-object v0

    goto :goto_0

    .line 395
    :pswitch_3
    invoke-static {}, Lorg/threeten/bp/chrono/JapaneseEra;->b()[Lorg/threeten/bp/chrono/JapaneseEra;

    move-result-object v5

    .line 396
    array-length v3, v5

    add-int/lit8 v3, v3, -0x1

    aget-object v3, v5, v3

    invoke-virtual {v3}, Lorg/threeten/bp/chrono/JapaneseEra;->d()Lorg/threeten/bp/LocalDate;

    move-result-object v3

    invoke-virtual {v3}, Lorg/threeten/bp/LocalDate;->d()I

    move-result v3

    .line 397
    array-length v4, v5

    add-int/lit8 v4, v4, -0x1

    aget-object v4, v5, v4

    invoke-virtual {v4}, Lorg/threeten/bp/chrono/JapaneseEra;->c()Lorg/threeten/bp/LocalDate;

    move-result-object v4

    invoke-virtual {v4}, Lorg/threeten/bp/LocalDate;->d()I

    move-result v4

    sub-int/2addr v3, v4

    add-int/lit8 v6, v3, 0x1

    .line 398
    const v3, 0x7fffffff

    move v4, v3

    .line 399
    :goto_1
    array-length v3, v5

    if-ge v2, v3, :cond_0

    .line 400
    aget-object v3, v5, v2

    invoke-virtual {v3}, Lorg/threeten/bp/chrono/JapaneseEra;->d()Lorg/threeten/bp/LocalDate;

    move-result-object v3

    invoke-virtual {v3}, Lorg/threeten/bp/LocalDate;->d()I

    move-result v3

    aget-object v7, v5, v2

    invoke-virtual {v7}, Lorg/threeten/bp/chrono/JapaneseEra;->c()Lorg/threeten/bp/LocalDate;

    move-result-object v7

    invoke-virtual {v7}, Lorg/threeten/bp/LocalDate;->d()I

    move-result v7

    sub-int/2addr v3, v7

    add-int/lit8 v3, v3, 0x1

    invoke-static {v4, v3}, Ljava/lang/Math;->min(II)I

    move-result v3

    .line 399
    add-int/lit8 v2, v2, 0x1

    move v4, v3

    goto :goto_1

    .line 402
    :cond_0
    const-wide/16 v2, 0x6

    int-to-long v4, v4

    int-to-long v6, v6

    invoke-static/range {v0 .. v7}, Lorg/threeten/bp/temporal/ValueRange;->a(JJJJ)Lorg/threeten/bp/temporal/ValueRange;

    move-result-object v0

    goto/16 :goto_0

    .line 405
    :pswitch_4
    invoke-virtual {v6, v7}, Ljava/util/Calendar;->getMinimum(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    int-to-long v0, v0

    invoke-virtual {v6, v7}, Ljava/util/Calendar;->getGreatestMinimum(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    int-to-long v2, v2

    invoke-virtual {v6, v7}, Ljava/util/Calendar;->getLeastMaximum(I)I

    move-result v4

    add-int/lit8 v4, v4, 0x1

    int-to-long v4, v4

    invoke-virtual {v6, v7}, Ljava/util/Calendar;->getMaximum(I)I

    move-result v6

    add-int/lit8 v6, v6, 0x1

    int-to-long v6, v6

    invoke-static/range {v0 .. v7}, Lorg/threeten/bp/temporal/ValueRange;->a(JJJJ)Lorg/threeten/bp/temporal/ValueRange;

    move-result-object v0

    goto/16 :goto_0

    .line 408
    :pswitch_5
    invoke-static {}, Lorg/threeten/bp/chrono/JapaneseEra;->b()[Lorg/threeten/bp/chrono/JapaneseEra;

    move-result-object v4

    .line 409
    const/16 v3, 0x16e

    .line 410
    :goto_2
    array-length v5, v4

    if-ge v2, v5, :cond_1

    .line 411
    aget-object v5, v4, v2

    invoke-virtual {v5}, Lorg/threeten/bp/chrono/JapaneseEra;->c()Lorg/threeten/bp/LocalDate;

    move-result-object v5

    invoke-virtual {v5}, Lorg/threeten/bp/LocalDate;->l()I

    move-result v5

    aget-object v6, v4, v2

    invoke-virtual {v6}, Lorg/threeten/bp/chrono/JapaneseEra;->c()Lorg/threeten/bp/LocalDate;

    move-result-object v6

    invoke-virtual {v6}, Lorg/threeten/bp/LocalDate;->h()I

    move-result v6

    sub-int/2addr v5, v6

    add-int/lit8 v5, v5, 0x1

    invoke-static {v3, v5}, Ljava/lang/Math;->min(II)I

    move-result v3

    .line 410
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 413
    :cond_1
    int-to-long v2, v3

    const-wide/16 v4, 0x16e

    invoke-static/range {v0 .. v5}, Lorg/threeten/bp/temporal/ValueRange;->a(JJJ)Lorg/threeten/bp/temporal/ValueRange;

    move-result-object v0

    goto/16 :goto_0

    .line 363
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch

    .line 385
    :pswitch_data_1
    .packed-switch 0x13
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public a(J)Z
    .locals 1

    .prologue
    .line 324
    sget-object v0, Lorg/threeten/bp/chrono/IsoChronology;->b:Lorg/threeten/bp/chrono/IsoChronology;

    invoke-virtual {v0, p1, p2}, Lorg/threeten/bp/chrono/IsoChronology;->a(J)Z

    move-result v0

    return v0
.end method

.method public b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 196
    const-string v0, "japanese"

    return-object v0
.end method

.method public synthetic b(Lorg/threeten/bp/temporal/TemporalAccessor;)Lorg/threeten/bp/chrono/ChronoLocalDate;
    .locals 1

    .prologue
    .line 101
    invoke-virtual {p0, p1}, Lorg/threeten/bp/chrono/JapaneseChronology;->e(Lorg/threeten/bp/temporal/TemporalAccessor;)Lorg/threeten/bp/chrono/JapaneseDate;

    move-result-object v0

    return-object v0
.end method

.method public b(III)Lorg/threeten/bp/chrono/JapaneseDate;
    .locals 2

    .prologue
    .line 210
    new-instance v0, Lorg/threeten/bp/chrono/JapaneseDate;

    invoke-static {p1, p2, p3}, Lorg/threeten/bp/LocalDate;->a(III)Lorg/threeten/bp/LocalDate;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/threeten/bp/chrono/JapaneseDate;-><init>(Lorg/threeten/bp/LocalDate;)V

    return-object v0
.end method

.method public b(I)Lorg/threeten/bp/chrono/JapaneseEra;
    .locals 1

    .prologue
    .line 352
    invoke-static {p1}, Lorg/threeten/bp/chrono/JapaneseEra;->a(I)Lorg/threeten/bp/chrono/JapaneseEra;

    move-result-object v0

    return-object v0
.end method

.method public c(Lorg/threeten/bp/temporal/TemporalAccessor;)Lorg/threeten/bp/chrono/ChronoLocalDateTime;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/threeten/bp/temporal/TemporalAccessor;",
            ")",
            "Lorg/threeten/bp/chrono/ChronoLocalDateTime",
            "<",
            "Lorg/threeten/bp/chrono/JapaneseDate;",
            ">;"
        }
    .end annotation

    .prologue
    .line 279
    invoke-super {p0, p1}, Lorg/threeten/bp/chrono/Chronology;->c(Lorg/threeten/bp/temporal/TemporalAccessor;)Lorg/threeten/bp/chrono/ChronoLocalDateTime;

    move-result-object v0

    return-object v0
.end method

.method public d(Lorg/threeten/bp/temporal/TemporalAccessor;)Lorg/threeten/bp/chrono/ChronoZonedDateTime;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/threeten/bp/temporal/TemporalAccessor;",
            ")",
            "Lorg/threeten/bp/chrono/ChronoZonedDateTime",
            "<",
            "Lorg/threeten/bp/chrono/JapaneseDate;",
            ">;"
        }
    .end annotation

    .prologue
    .line 285
    invoke-super {p0, p1}, Lorg/threeten/bp/chrono/Chronology;->d(Lorg/threeten/bp/temporal/TemporalAccessor;)Lorg/threeten/bp/chrono/ChronoZonedDateTime;

    move-result-object v0

    return-object v0
.end method

.method public e(Lorg/threeten/bp/temporal/TemporalAccessor;)Lorg/threeten/bp/chrono/JapaneseDate;
    .locals 2

    .prologue
    .line 270
    instance-of v0, p1, Lorg/threeten/bp/chrono/JapaneseDate;

    if-eqz v0, :cond_0

    .line 271
    check-cast p1, Lorg/threeten/bp/chrono/JapaneseDate;

    .line 273
    :goto_0
    return-object p1

    :cond_0
    new-instance v0, Lorg/threeten/bp/chrono/JapaneseDate;

    invoke-static {p1}, Lorg/threeten/bp/LocalDate;->a(Lorg/threeten/bp/temporal/TemporalAccessor;)Lorg/threeten/bp/LocalDate;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/threeten/bp/chrono/JapaneseDate;-><init>(Lorg/threeten/bp/LocalDate;)V

    move-object p1, v0

    goto :goto_0
.end method
