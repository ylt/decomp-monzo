.class public final Lorg/threeten/bp/chrono/JapaneseEra;
.super Lorg/threeten/bp/jdk8/DefaultInterfaceEra;
.source "JapaneseEra.java"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field public static final a:Lorg/threeten/bp/chrono/JapaneseEra;

.field public static final b:Lorg/threeten/bp/chrono/JapaneseEra;

.field public static final c:Lorg/threeten/bp/chrono/JapaneseEra;

.field public static final d:Lorg/threeten/bp/chrono/JapaneseEra;

.field private static final e:Ljava/util/concurrent/atomic/AtomicReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/atomic/AtomicReference",
            "<[",
            "Lorg/threeten/bp/chrono/JapaneseEra;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final f:I

.field private final transient g:Lorg/threeten/bp/LocalDate;

.field private final transient h:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/16 v7, 0x8

    const/4 v6, 0x2

    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 77
    new-instance v0, Lorg/threeten/bp/chrono/JapaneseEra;

    const/4 v1, -0x1

    const/16 v2, 0x74c

    const/16 v3, 0x9

    invoke-static {v2, v3, v7}, Lorg/threeten/bp/LocalDate;->a(III)Lorg/threeten/bp/LocalDate;

    move-result-object v2

    const-string v3, "Meiji"

    invoke-direct {v0, v1, v2, v3}, Lorg/threeten/bp/chrono/JapaneseEra;-><init>(ILorg/threeten/bp/LocalDate;Ljava/lang/String;)V

    sput-object v0, Lorg/threeten/bp/chrono/JapaneseEra;->a:Lorg/threeten/bp/chrono/JapaneseEra;

    .line 82
    new-instance v0, Lorg/threeten/bp/chrono/JapaneseEra;

    const/16 v1, 0x778

    const/4 v2, 0x7

    const/16 v3, 0x1e

    invoke-static {v1, v2, v3}, Lorg/threeten/bp/LocalDate;->a(III)Lorg/threeten/bp/LocalDate;

    move-result-object v1

    const-string v2, "Taisho"

    invoke-direct {v0, v5, v1, v2}, Lorg/threeten/bp/chrono/JapaneseEra;-><init>(ILorg/threeten/bp/LocalDate;Ljava/lang/String;)V

    sput-object v0, Lorg/threeten/bp/chrono/JapaneseEra;->b:Lorg/threeten/bp/chrono/JapaneseEra;

    .line 87
    new-instance v0, Lorg/threeten/bp/chrono/JapaneseEra;

    const/16 v1, 0x786

    const/16 v2, 0xc

    const/16 v3, 0x19

    invoke-static {v1, v2, v3}, Lorg/threeten/bp/LocalDate;->a(III)Lorg/threeten/bp/LocalDate;

    move-result-object v1

    const-string v2, "Showa"

    invoke-direct {v0, v4, v1, v2}, Lorg/threeten/bp/chrono/JapaneseEra;-><init>(ILorg/threeten/bp/LocalDate;Ljava/lang/String;)V

    sput-object v0, Lorg/threeten/bp/chrono/JapaneseEra;->c:Lorg/threeten/bp/chrono/JapaneseEra;

    .line 92
    new-instance v0, Lorg/threeten/bp/chrono/JapaneseEra;

    const/16 v1, 0x7c5

    invoke-static {v1, v4, v7}, Lorg/threeten/bp/LocalDate;->a(III)Lorg/threeten/bp/LocalDate;

    move-result-object v1

    const-string v2, "Heisei"

    invoke-direct {v0, v6, v1, v2}, Lorg/threeten/bp/chrono/JapaneseEra;-><init>(ILorg/threeten/bp/LocalDate;Ljava/lang/String;)V

    sput-object v0, Lorg/threeten/bp/chrono/JapaneseEra;->d:Lorg/threeten/bp/chrono/JapaneseEra;

    .line 107
    const/4 v0, 0x4

    new-array v0, v0, [Lorg/threeten/bp/chrono/JapaneseEra;

    .line 108
    sget-object v1, Lorg/threeten/bp/chrono/JapaneseEra;->a:Lorg/threeten/bp/chrono/JapaneseEra;

    aput-object v1, v0, v5

    .line 109
    sget-object v1, Lorg/threeten/bp/chrono/JapaneseEra;->b:Lorg/threeten/bp/chrono/JapaneseEra;

    aput-object v1, v0, v4

    .line 110
    sget-object v1, Lorg/threeten/bp/chrono/JapaneseEra;->c:Lorg/threeten/bp/chrono/JapaneseEra;

    aput-object v1, v0, v6

    .line 111
    const/4 v1, 0x3

    sget-object v2, Lorg/threeten/bp/chrono/JapaneseEra;->d:Lorg/threeten/bp/chrono/JapaneseEra;

    aput-object v2, v0, v1

    .line 112
    new-instance v1, Ljava/util/concurrent/atomic/AtomicReference;

    invoke-direct {v1, v0}, Ljava/util/concurrent/atomic/AtomicReference;-><init>(Ljava/lang/Object;)V

    sput-object v1, Lorg/threeten/bp/chrono/JapaneseEra;->e:Ljava/util/concurrent/atomic/AtomicReference;

    .line 113
    return-void
.end method

.method private constructor <init>(ILorg/threeten/bp/LocalDate;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 133
    invoke-direct {p0}, Lorg/threeten/bp/jdk8/DefaultInterfaceEra;-><init>()V

    .line 134
    iput p1, p0, Lorg/threeten/bp/chrono/JapaneseEra;->f:I

    .line 135
    iput-object p2, p0, Lorg/threeten/bp/chrono/JapaneseEra;->g:Lorg/threeten/bp/LocalDate;

    .line 136
    iput-object p3, p0, Lorg/threeten/bp/chrono/JapaneseEra;->h:Ljava/lang/String;

    .line 137
    return-void
.end method

.method public static a(I)Lorg/threeten/bp/chrono/JapaneseEra;
    .locals 2

    .prologue
    .line 204
    sget-object v0, Lorg/threeten/bp/chrono/JapaneseEra;->e:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lorg/threeten/bp/chrono/JapaneseEra;

    .line 205
    sget-object v1, Lorg/threeten/bp/chrono/JapaneseEra;->a:Lorg/threeten/bp/chrono/JapaneseEra;

    iget v1, v1, Lorg/threeten/bp/chrono/JapaneseEra;->f:I

    if-lt p0, v1, :cond_0

    array-length v1, v0

    add-int/lit8 v1, v1, -0x1

    aget-object v1, v0, v1

    iget v1, v1, Lorg/threeten/bp/chrono/JapaneseEra;->f:I

    if-le p0, v1, :cond_1

    .line 206
    :cond_0
    new-instance v0, Lorg/threeten/bp/DateTimeException;

    const-string v1, "japaneseEra is invalid"

    invoke-direct {v0, v1}, Lorg/threeten/bp/DateTimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 208
    :cond_1
    invoke-static {p0}, Lorg/threeten/bp/chrono/JapaneseEra;->b(I)I

    move-result v1

    aget-object v0, v0, v1

    return-object v0
.end method

.method static a(Ljava/io/DataInput;)Lorg/threeten/bp/chrono/JapaneseEra;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 339
    invoke-interface {p0}, Ljava/io/DataInput;->readByte()B

    move-result v0

    .line 340
    invoke-static {v0}, Lorg/threeten/bp/chrono/JapaneseEra;->a(I)Lorg/threeten/bp/chrono/JapaneseEra;

    move-result-object v0

    return-object v0
.end method

.method static a(Lorg/threeten/bp/LocalDate;)Lorg/threeten/bp/chrono/JapaneseEra;
    .locals 4

    .prologue
    .line 256
    sget-object v0, Lorg/threeten/bp/chrono/JapaneseEra;->a:Lorg/threeten/bp/chrono/JapaneseEra;

    iget-object v0, v0, Lorg/threeten/bp/chrono/JapaneseEra;->g:Lorg/threeten/bp/LocalDate;

    invoke-virtual {p0, v0}, Lorg/threeten/bp/LocalDate;->c(Lorg/threeten/bp/chrono/ChronoLocalDate;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 257
    new-instance v0, Lorg/threeten/bp/DateTimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Date too early: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/threeten/bp/DateTimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 259
    :cond_0
    sget-object v0, Lorg/threeten/bp/chrono/JapaneseEra;->e:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lorg/threeten/bp/chrono/JapaneseEra;

    .line 260
    array-length v1, v0

    add-int/lit8 v1, v1, -0x1

    move v2, v1

    :goto_0
    if-ltz v2, :cond_2

    .line 261
    aget-object v1, v0, v2

    .line 262
    iget-object v3, v1, Lorg/threeten/bp/chrono/JapaneseEra;->g:Lorg/threeten/bp/LocalDate;

    invoke-virtual {p0, v3}, Lorg/threeten/bp/LocalDate;->a(Lorg/threeten/bp/chrono/ChronoLocalDate;)I

    move-result v3

    if-ltz v3, :cond_1

    move-object v0, v1

    .line 266
    :goto_1
    return-object v0

    .line 260
    :cond_1
    add-int/lit8 v1, v2, -0x1

    move v2, v1

    goto :goto_0

    .line 266
    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private static b(I)I
    .locals 1

    .prologue
    .line 276
    add-int/lit8 v0, p0, 0x1

    return v0
.end method

.method public static b()[Lorg/threeten/bp/chrono/JapaneseEra;
    .locals 2

    .prologue
    .line 244
    sget-object v0, Lorg/threeten/bp/chrono/JapaneseEra;->e:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lorg/threeten/bp/chrono/JapaneseEra;

    .line 245
    array-length v1, v0

    invoke-static {v0, v1}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lorg/threeten/bp/chrono/JapaneseEra;

    return-object v0
.end method

.method private readResolve()Ljava/lang/Object;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/ObjectStreamException;
        }
    .end annotation

    .prologue
    .line 149
    :try_start_0
    iget v0, p0, Lorg/threeten/bp/chrono/JapaneseEra;->f:I

    invoke-static {v0}, Lorg/threeten/bp/chrono/JapaneseEra;->a(I)Lorg/threeten/bp/chrono/JapaneseEra;
    :try_end_0
    .catch Lorg/threeten/bp/DateTimeException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    .line 150
    :catch_0
    move-exception v0

    .line 151
    new-instance v1, Ljava/io/InvalidObjectException;

    const-string v2, "Invalid era"

    invoke-direct {v1, v2}, Ljava/io/InvalidObjectException;-><init>(Ljava/lang/String;)V

    .line 152
    invoke-virtual {v1, v0}, Ljava/io/InvalidObjectException;->initCause(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    .line 153
    throw v1
.end method

.method private writeReplace()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 331
    new-instance v0, Lorg/threeten/bp/chrono/Ser;

    const/4 v1, 0x2

    invoke-direct {v0, v1, p0}, Lorg/threeten/bp/chrono/Ser;-><init>(BLjava/lang/Object;)V

    return-object v0
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 312
    iget v0, p0, Lorg/threeten/bp/chrono/JapaneseEra;->f:I

    return v0
.end method

.method a(Ljava/io/DataOutput;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 335
    invoke-virtual {p0}, Lorg/threeten/bp/chrono/JapaneseEra;->a()I

    move-result v0

    invoke-interface {p1, v0}, Ljava/io/DataOutput;->writeByte(I)V

    .line 336
    return-void
.end method

.method public b(Lorg/threeten/bp/temporal/TemporalField;)Lorg/threeten/bp/temporal/ValueRange;
    .locals 2

    .prologue
    .line 317
    sget-object v0, Lorg/threeten/bp/temporal/ChronoField;->B:Lorg/threeten/bp/temporal/ChronoField;

    if-ne p1, v0, :cond_0

    .line 318
    sget-object v0, Lorg/threeten/bp/chrono/JapaneseChronology;->c:Lorg/threeten/bp/chrono/JapaneseChronology;

    sget-object v1, Lorg/threeten/bp/temporal/ChronoField;->B:Lorg/threeten/bp/temporal/ChronoField;

    invoke-virtual {v0, v1}, Lorg/threeten/bp/chrono/JapaneseChronology;->a(Lorg/threeten/bp/temporal/ChronoField;)Lorg/threeten/bp/temporal/ValueRange;

    move-result-object v0

    .line 320
    :goto_0
    return-object v0

    :cond_0
    invoke-super {p0, p1}, Lorg/threeten/bp/jdk8/DefaultInterfaceEra;->b(Lorg/threeten/bp/temporal/TemporalField;)Lorg/threeten/bp/temporal/ValueRange;

    move-result-object v0

    goto :goto_0
.end method

.method c()Lorg/threeten/bp/LocalDate;
    .locals 1

    .prologue
    .line 284
    iget-object v0, p0, Lorg/threeten/bp/chrono/JapaneseEra;->g:Lorg/threeten/bp/LocalDate;

    return-object v0
.end method

.method d()Lorg/threeten/bp/LocalDate;
    .locals 4

    .prologue
    .line 292
    iget v0, p0, Lorg/threeten/bp/chrono/JapaneseEra;->f:I

    invoke-static {v0}, Lorg/threeten/bp/chrono/JapaneseEra;->b(I)I

    move-result v0

    .line 293
    invoke-static {}, Lorg/threeten/bp/chrono/JapaneseEra;->b()[Lorg/threeten/bp/chrono/JapaneseEra;

    move-result-object v1

    .line 294
    array-length v2, v1

    add-int/lit8 v2, v2, -0x1

    if-lt v0, v2, :cond_0

    .line 295
    sget-object v0, Lorg/threeten/bp/LocalDate;->b:Lorg/threeten/bp/LocalDate;

    .line 297
    :goto_0
    return-object v0

    :cond_0
    add-int/lit8 v0, v0, 0x1

    aget-object v0, v1, v0

    invoke-virtual {v0}, Lorg/threeten/bp/chrono/JapaneseEra;->c()Lorg/threeten/bp/LocalDate;

    move-result-object v0

    const-wide/16 v2, 0x1

    invoke-virtual {v0, v2, v3}, Lorg/threeten/bp/LocalDate;->g(J)Lorg/threeten/bp/LocalDate;

    move-result-object v0

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 326
    iget-object v0, p0, Lorg/threeten/bp/chrono/JapaneseEra;->h:Ljava/lang/String;

    return-object v0
.end method
