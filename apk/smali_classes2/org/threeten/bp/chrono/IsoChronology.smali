.class public final Lorg/threeten/bp/chrono/IsoChronology;
.super Lorg/threeten/bp/chrono/Chronology;
.source "IsoChronology.java"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field public static final b:Lorg/threeten/bp/chrono/IsoChronology;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 103
    new-instance v0, Lorg/threeten/bp/chrono/IsoChronology;

    invoke-direct {v0}, Lorg/threeten/bp/chrono/IsoChronology;-><init>()V

    sput-object v0, Lorg/threeten/bp/chrono/IsoChronology;->b:Lorg/threeten/bp/chrono/IsoChronology;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 113
    invoke-direct {p0}, Lorg/threeten/bp/chrono/Chronology;-><init>()V

    .line 114
    return-void
.end method

.method private readResolve()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 122
    sget-object v0, Lorg/threeten/bp/chrono/IsoChronology;->b:Lorg/threeten/bp/chrono/IsoChronology;

    return-object v0
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 137
    const-string v0, "ISO"

    return-object v0
.end method

.method public a(Ljava/util/Map;Lorg/threeten/bp/format/ResolverStyle;)Lorg/threeten/bp/LocalDate;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Lorg/threeten/bp/temporal/TemporalField;",
            "Ljava/lang/Long;",
            ">;",
            "Lorg/threeten/bp/format/ResolverStyle;",
            ")",
            "Lorg/threeten/bp/LocalDate;"
        }
    .end annotation

    .prologue
    const-wide/16 v6, 0x0

    const-wide/16 v10, 0x1

    const/4 v8, 0x1

    .line 386
    sget-object v0, Lorg/threeten/bp/temporal/ChronoField;->u:Lorg/threeten/bp/temporal/ChronoField;

    invoke-interface {p1, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 387
    sget-object v0, Lorg/threeten/bp/temporal/ChronoField;->u:Lorg/threeten/bp/temporal/ChronoField;

    invoke-interface {p1, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-static {v0, v1}, Lorg/threeten/bp/LocalDate;->a(J)Lorg/threeten/bp/LocalDate;

    move-result-object v0

    .line 534
    :cond_0
    :goto_0
    return-object v0

    .line 391
    :cond_1
    sget-object v0, Lorg/threeten/bp/temporal/ChronoField;->y:Lorg/threeten/bp/temporal/ChronoField;

    invoke-interface {p1, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    .line 392
    if-eqz v0, :cond_3

    .line 393
    sget-object v1, Lorg/threeten/bp/format/ResolverStyle;->c:Lorg/threeten/bp/format/ResolverStyle;

    if-eq p2, v1, :cond_2

    .line 394
    sget-object v1, Lorg/threeten/bp/temporal/ChronoField;->y:Lorg/threeten/bp/temporal/ChronoField;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lorg/threeten/bp/temporal/ChronoField;->a(J)J

    .line 396
    :cond_2
    sget-object v1, Lorg/threeten/bp/temporal/ChronoField;->x:Lorg/threeten/bp/temporal/ChronoField;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    const/16 v4, 0xc

    invoke-static {v2, v3, v4}, Lorg/threeten/bp/jdk8/Jdk8Methods;->b(JI)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    int-to-long v2, v2

    invoke-virtual {p0, p1, v1, v2, v3}, Lorg/threeten/bp/chrono/IsoChronology;->a(Ljava/util/Map;Lorg/threeten/bp/temporal/ChronoField;J)V

    .line 397
    sget-object v1, Lorg/threeten/bp/temporal/ChronoField;->A:Lorg/threeten/bp/temporal/ChronoField;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    const-wide/16 v4, 0xc

    invoke-static {v2, v3, v4, v5}, Lorg/threeten/bp/jdk8/Jdk8Methods;->e(JJ)J

    move-result-wide v2

    invoke-virtual {p0, p1, v1, v2, v3}, Lorg/threeten/bp/chrono/IsoChronology;->a(Ljava/util/Map;Lorg/threeten/bp/temporal/ChronoField;J)V

    .line 401
    :cond_3
    sget-object v0, Lorg/threeten/bp/temporal/ChronoField;->z:Lorg/threeten/bp/temporal/ChronoField;

    invoke-interface {p1, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    .line 402
    if-eqz v0, :cond_e

    .line 403
    sget-object v1, Lorg/threeten/bp/format/ResolverStyle;->c:Lorg/threeten/bp/format/ResolverStyle;

    if-eq p2, v1, :cond_4

    .line 404
    sget-object v1, Lorg/threeten/bp/temporal/ChronoField;->z:Lorg/threeten/bp/temporal/ChronoField;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lorg/threeten/bp/temporal/ChronoField;->a(J)J

    .line 406
    :cond_4
    sget-object v1, Lorg/threeten/bp/temporal/ChronoField;->B:Lorg/threeten/bp/temporal/ChronoField;

    invoke-interface {p1, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    .line 407
    if-nez v1, :cond_b

    .line 408
    sget-object v1, Lorg/threeten/bp/temporal/ChronoField;->A:Lorg/threeten/bp/temporal/ChronoField;

    invoke-interface {p1, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    .line 409
    sget-object v2, Lorg/threeten/bp/format/ResolverStyle;->a:Lorg/threeten/bp/format/ResolverStyle;

    if-ne p2, v2, :cond_8

    .line 411
    if-eqz v1, :cond_7

    .line 412
    sget-object v2, Lorg/threeten/bp/temporal/ChronoField;->A:Lorg/threeten/bp/temporal/ChronoField;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    cmp-long v1, v4, v6

    if-lez v1, :cond_6

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    :goto_1
    invoke-virtual {p0, p1, v2, v0, v1}, Lorg/threeten/bp/chrono/IsoChronology;->a(Ljava/util/Map;Lorg/threeten/bp/temporal/ChronoField;J)V

    .line 433
    :cond_5
    :goto_2
    sget-object v0, Lorg/threeten/bp/temporal/ChronoField;->A:Lorg/threeten/bp/temporal/ChronoField;

    invoke-interface {p1, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1e

    .line 434
    sget-object v0, Lorg/threeten/bp/temporal/ChronoField;->x:Lorg/threeten/bp/temporal/ChronoField;

    invoke-interface {p1, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_18

    .line 435
    sget-object v0, Lorg/threeten/bp/temporal/ChronoField;->s:Lorg/threeten/bp/temporal/ChronoField;

    invoke-interface {p1, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_14

    .line 436
    sget-object v1, Lorg/threeten/bp/temporal/ChronoField;->A:Lorg/threeten/bp/temporal/ChronoField;

    sget-object v0, Lorg/threeten/bp/temporal/ChronoField;->A:Lorg/threeten/bp/temporal/ChronoField;

    invoke-interface {p1, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lorg/threeten/bp/temporal/ChronoField;->b(J)I

    move-result v1

    .line 437
    sget-object v0, Lorg/threeten/bp/temporal/ChronoField;->x:Lorg/threeten/bp/temporal/ChronoField;

    invoke-interface {p1, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v2, v3}, Lorg/threeten/bp/jdk8/Jdk8Methods;->a(J)I

    move-result v2

    .line 438
    sget-object v0, Lorg/threeten/bp/temporal/ChronoField;->s:Lorg/threeten/bp/temporal/ChronoField;

    invoke-interface {p1, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-static {v4, v5}, Lorg/threeten/bp/jdk8/Jdk8Methods;->a(J)I

    move-result v0

    .line 439
    sget-object v3, Lorg/threeten/bp/format/ResolverStyle;->c:Lorg/threeten/bp/format/ResolverStyle;

    if-ne p2, v3, :cond_f

    .line 440
    invoke-static {v2, v8}, Lorg/threeten/bp/jdk8/Jdk8Methods;->c(II)I

    move-result v2

    int-to-long v2, v2

    .line 441
    invoke-static {v0, v8}, Lorg/threeten/bp/jdk8/Jdk8Methods;->c(II)I

    move-result v0

    int-to-long v4, v0

    .line 442
    invoke-static {v1, v8, v8}, Lorg/threeten/bp/LocalDate;->a(III)Lorg/threeten/bp/LocalDate;

    move-result-object v0

    invoke-virtual {v0, v2, v3}, Lorg/threeten/bp/LocalDate;->c(J)Lorg/threeten/bp/LocalDate;

    move-result-object v0

    invoke-virtual {v0, v4, v5}, Lorg/threeten/bp/LocalDate;->e(J)Lorg/threeten/bp/LocalDate;

    move-result-object v0

    goto/16 :goto_0

    .line 412
    :cond_6
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-static {v10, v11, v0, v1}, Lorg/threeten/bp/jdk8/Jdk8Methods;->c(JJ)J

    move-result-wide v0

    goto :goto_1

    .line 415
    :cond_7
    sget-object v1, Lorg/threeten/bp/temporal/ChronoField;->z:Lorg/threeten/bp/temporal/ChronoField;

    invoke-interface {p1, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    .line 419
    :cond_8
    sget-object v2, Lorg/threeten/bp/temporal/ChronoField;->A:Lorg/threeten/bp/temporal/ChronoField;

    if-eqz v1, :cond_9

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    cmp-long v1, v4, v6

    if-lez v1, :cond_a

    :cond_9
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    :goto_3
    invoke-virtual {p0, p1, v2, v0, v1}, Lorg/threeten/bp/chrono/IsoChronology;->a(Ljava/util/Map;Lorg/threeten/bp/temporal/ChronoField;J)V

    goto/16 :goto_2

    :cond_a
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-static {v10, v11, v0, v1}, Lorg/threeten/bp/jdk8/Jdk8Methods;->c(JJ)J

    move-result-wide v0

    goto :goto_3

    .line 421
    :cond_b
    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    cmp-long v2, v2, v10

    if-nez v2, :cond_c

    .line 422
    sget-object v1, Lorg/threeten/bp/temporal/ChronoField;->A:Lorg/threeten/bp/temporal/ChronoField;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p0, p1, v1, v2, v3}, Lorg/threeten/bp/chrono/IsoChronology;->a(Ljava/util/Map;Lorg/threeten/bp/temporal/ChronoField;J)V

    goto/16 :goto_2

    .line 423
    :cond_c
    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    cmp-long v2, v2, v6

    if-nez v2, :cond_d

    .line 424
    sget-object v1, Lorg/threeten/bp/temporal/ChronoField;->A:Lorg/threeten/bp/temporal/ChronoField;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v10, v11, v2, v3}, Lorg/threeten/bp/jdk8/Jdk8Methods;->c(JJ)J

    move-result-wide v2

    invoke-virtual {p0, p1, v1, v2, v3}, Lorg/threeten/bp/chrono/IsoChronology;->a(Ljava/util/Map;Lorg/threeten/bp/temporal/ChronoField;J)V

    goto/16 :goto_2

    .line 426
    :cond_d
    new-instance v0, Lorg/threeten/bp/DateTimeException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Invalid value for era: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/threeten/bp/DateTimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 428
    :cond_e
    sget-object v0, Lorg/threeten/bp/temporal/ChronoField;->B:Lorg/threeten/bp/temporal/ChronoField;

    invoke-interface {p1, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 429
    sget-object v1, Lorg/threeten/bp/temporal/ChronoField;->B:Lorg/threeten/bp/temporal/ChronoField;

    sget-object v0, Lorg/threeten/bp/temporal/ChronoField;->B:Lorg/threeten/bp/temporal/ChronoField;

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lorg/threeten/bp/temporal/ChronoField;->a(J)J

    goto/16 :goto_2

    .line 443
    :cond_f
    sget-object v3, Lorg/threeten/bp/format/ResolverStyle;->b:Lorg/threeten/bp/format/ResolverStyle;

    if-ne p2, v3, :cond_13

    .line 444
    sget-object v3, Lorg/threeten/bp/temporal/ChronoField;->s:Lorg/threeten/bp/temporal/ChronoField;

    int-to-long v4, v0

    invoke-virtual {v3, v4, v5}, Lorg/threeten/bp/temporal/ChronoField;->a(J)J

    .line 445
    const/4 v3, 0x4

    if-eq v2, v3, :cond_10

    const/4 v3, 0x6

    if-eq v2, v3, :cond_10

    const/16 v3, 0x9

    if-eq v2, v3, :cond_10

    const/16 v3, 0xb

    if-ne v2, v3, :cond_12

    .line 446
    :cond_10
    const/16 v3, 0x1e

    invoke-static {v0, v3}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 450
    :cond_11
    :goto_4
    invoke-static {v1, v2, v0}, Lorg/threeten/bp/LocalDate;->a(III)Lorg/threeten/bp/LocalDate;

    move-result-object v0

    goto/16 :goto_0

    .line 447
    :cond_12
    const/4 v3, 0x2

    if-ne v2, v3, :cond_11

    .line 448
    sget-object v3, Lorg/threeten/bp/Month;->b:Lorg/threeten/bp/Month;

    int-to-long v4, v1

    invoke-static {v4, v5}, Lorg/threeten/bp/Year;->a(J)Z

    move-result v4

    invoke-virtual {v3, v4}, Lorg/threeten/bp/Month;->a(Z)I

    move-result v3

    invoke-static {v0, v3}, Ljava/lang/Math;->min(II)I

    move-result v0

    goto :goto_4

    .line 452
    :cond_13
    invoke-static {v1, v2, v0}, Lorg/threeten/bp/LocalDate;->a(III)Lorg/threeten/bp/LocalDate;

    move-result-object v0

    goto/16 :goto_0

    .line 455
    :cond_14
    sget-object v0, Lorg/threeten/bp/temporal/ChronoField;->v:Lorg/threeten/bp/temporal/ChronoField;

    invoke-interface {p1, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_18

    .line 456
    sget-object v0, Lorg/threeten/bp/temporal/ChronoField;->q:Lorg/threeten/bp/temporal/ChronoField;

    invoke-interface {p1, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_16

    .line 457
    sget-object v1, Lorg/threeten/bp/temporal/ChronoField;->A:Lorg/threeten/bp/temporal/ChronoField;

    sget-object v0, Lorg/threeten/bp/temporal/ChronoField;->A:Lorg/threeten/bp/temporal/ChronoField;

    invoke-interface {p1, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lorg/threeten/bp/temporal/ChronoField;->b(J)I

    move-result v1

    .line 458
    sget-object v0, Lorg/threeten/bp/format/ResolverStyle;->c:Lorg/threeten/bp/format/ResolverStyle;

    if-ne p2, v0, :cond_15

    .line 459
    sget-object v0, Lorg/threeten/bp/temporal/ChronoField;->x:Lorg/threeten/bp/temporal/ChronoField;

    invoke-interface {p1, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v2, v3, v10, v11}, Lorg/threeten/bp/jdk8/Jdk8Methods;->c(JJ)J

    move-result-wide v2

    .line 460
    sget-object v0, Lorg/threeten/bp/temporal/ChronoField;->v:Lorg/threeten/bp/temporal/ChronoField;

    invoke-interface {p1, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-static {v4, v5, v10, v11}, Lorg/threeten/bp/jdk8/Jdk8Methods;->c(JJ)J

    move-result-wide v4

    .line 461
    sget-object v0, Lorg/threeten/bp/temporal/ChronoField;->q:Lorg/threeten/bp/temporal/ChronoField;

    invoke-interface {p1, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    invoke-static {v6, v7, v10, v11}, Lorg/threeten/bp/jdk8/Jdk8Methods;->c(JJ)J

    move-result-wide v6

    .line 462
    invoke-static {v1, v8, v8}, Lorg/threeten/bp/LocalDate;->a(III)Lorg/threeten/bp/LocalDate;

    move-result-object v0

    invoke-virtual {v0, v2, v3}, Lorg/threeten/bp/LocalDate;->c(J)Lorg/threeten/bp/LocalDate;

    move-result-object v0

    invoke-virtual {v0, v4, v5}, Lorg/threeten/bp/LocalDate;->d(J)Lorg/threeten/bp/LocalDate;

    move-result-object v0

    invoke-virtual {v0, v6, v7}, Lorg/threeten/bp/LocalDate;->e(J)Lorg/threeten/bp/LocalDate;

    move-result-object v0

    goto/16 :goto_0

    .line 464
    :cond_15
    sget-object v2, Lorg/threeten/bp/temporal/ChronoField;->x:Lorg/threeten/bp/temporal/ChronoField;

    sget-object v0, Lorg/threeten/bp/temporal/ChronoField;->x:Lorg/threeten/bp/temporal/ChronoField;

    invoke-interface {p1, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Lorg/threeten/bp/temporal/ChronoField;->b(J)I

    move-result v2

    .line 465
    sget-object v3, Lorg/threeten/bp/temporal/ChronoField;->v:Lorg/threeten/bp/temporal/ChronoField;

    sget-object v0, Lorg/threeten/bp/temporal/ChronoField;->v:Lorg/threeten/bp/temporal/ChronoField;

    invoke-interface {p1, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Lorg/threeten/bp/temporal/ChronoField;->b(J)I

    move-result v3

    .line 466
    sget-object v4, Lorg/threeten/bp/temporal/ChronoField;->q:Lorg/threeten/bp/temporal/ChronoField;

    sget-object v0, Lorg/threeten/bp/temporal/ChronoField;->q:Lorg/threeten/bp/temporal/ChronoField;

    invoke-interface {p1, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    invoke-virtual {v4, v6, v7}, Lorg/threeten/bp/temporal/ChronoField;->b(J)I

    move-result v0

    .line 467
    invoke-static {v1, v2, v8}, Lorg/threeten/bp/LocalDate;->a(III)Lorg/threeten/bp/LocalDate;

    move-result-object v1

    add-int/lit8 v3, v3, -0x1

    mul-int/lit8 v3, v3, 0x7

    add-int/lit8 v0, v0, -0x1

    add-int/2addr v0, v3

    int-to-long v4, v0

    invoke-virtual {v1, v4, v5}, Lorg/threeten/bp/LocalDate;->e(J)Lorg/threeten/bp/LocalDate;

    move-result-object v0

    .line 468
    sget-object v1, Lorg/threeten/bp/format/ResolverStyle;->a:Lorg/threeten/bp/format/ResolverStyle;

    if-ne p2, v1, :cond_0

    sget-object v1, Lorg/threeten/bp/temporal/ChronoField;->x:Lorg/threeten/bp/temporal/ChronoField;

    invoke-virtual {v0, v1}, Lorg/threeten/bp/LocalDate;->c(Lorg/threeten/bp/temporal/TemporalField;)I

    move-result v1

    if-eq v1, v2, :cond_0

    .line 469
    new-instance v0, Lorg/threeten/bp/DateTimeException;

    const-string v1, "Strict mode rejected date parsed to a different month"

    invoke-direct {v0, v1}, Lorg/threeten/bp/DateTimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 473
    :cond_16
    sget-object v0, Lorg/threeten/bp/temporal/ChronoField;->p:Lorg/threeten/bp/temporal/ChronoField;

    invoke-interface {p1, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_18

    .line 474
    sget-object v1, Lorg/threeten/bp/temporal/ChronoField;->A:Lorg/threeten/bp/temporal/ChronoField;

    sget-object v0, Lorg/threeten/bp/temporal/ChronoField;->A:Lorg/threeten/bp/temporal/ChronoField;

    invoke-interface {p1, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lorg/threeten/bp/temporal/ChronoField;->b(J)I

    move-result v1

    .line 475
    sget-object v0, Lorg/threeten/bp/format/ResolverStyle;->c:Lorg/threeten/bp/format/ResolverStyle;

    if-ne p2, v0, :cond_17

    .line 476
    sget-object v0, Lorg/threeten/bp/temporal/ChronoField;->x:Lorg/threeten/bp/temporal/ChronoField;

    invoke-interface {p1, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v2, v3, v10, v11}, Lorg/threeten/bp/jdk8/Jdk8Methods;->c(JJ)J

    move-result-wide v2

    .line 477
    sget-object v0, Lorg/threeten/bp/temporal/ChronoField;->v:Lorg/threeten/bp/temporal/ChronoField;

    invoke-interface {p1, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-static {v4, v5, v10, v11}, Lorg/threeten/bp/jdk8/Jdk8Methods;->c(JJ)J

    move-result-wide v4

    .line 478
    sget-object v0, Lorg/threeten/bp/temporal/ChronoField;->p:Lorg/threeten/bp/temporal/ChronoField;

    invoke-interface {p1, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    invoke-static {v6, v7, v10, v11}, Lorg/threeten/bp/jdk8/Jdk8Methods;->c(JJ)J

    move-result-wide v6

    .line 479
    invoke-static {v1, v8, v8}, Lorg/threeten/bp/LocalDate;->a(III)Lorg/threeten/bp/LocalDate;

    move-result-object v0

    invoke-virtual {v0, v2, v3}, Lorg/threeten/bp/LocalDate;->c(J)Lorg/threeten/bp/LocalDate;

    move-result-object v0

    invoke-virtual {v0, v4, v5}, Lorg/threeten/bp/LocalDate;->d(J)Lorg/threeten/bp/LocalDate;

    move-result-object v0

    invoke-virtual {v0, v6, v7}, Lorg/threeten/bp/LocalDate;->e(J)Lorg/threeten/bp/LocalDate;

    move-result-object v0

    goto/16 :goto_0

    .line 481
    :cond_17
    sget-object v2, Lorg/threeten/bp/temporal/ChronoField;->x:Lorg/threeten/bp/temporal/ChronoField;

    sget-object v0, Lorg/threeten/bp/temporal/ChronoField;->x:Lorg/threeten/bp/temporal/ChronoField;

    invoke-interface {p1, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Lorg/threeten/bp/temporal/ChronoField;->b(J)I

    move-result v2

    .line 482
    sget-object v3, Lorg/threeten/bp/temporal/ChronoField;->v:Lorg/threeten/bp/temporal/ChronoField;

    sget-object v0, Lorg/threeten/bp/temporal/ChronoField;->v:Lorg/threeten/bp/temporal/ChronoField;

    invoke-interface {p1, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Lorg/threeten/bp/temporal/ChronoField;->b(J)I

    move-result v3

    .line 483
    sget-object v4, Lorg/threeten/bp/temporal/ChronoField;->p:Lorg/threeten/bp/temporal/ChronoField;

    sget-object v0, Lorg/threeten/bp/temporal/ChronoField;->p:Lorg/threeten/bp/temporal/ChronoField;

    invoke-interface {p1, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    invoke-virtual {v4, v6, v7}, Lorg/threeten/bp/temporal/ChronoField;->b(J)I

    move-result v0

    .line 484
    invoke-static {v1, v2, v8}, Lorg/threeten/bp/LocalDate;->a(III)Lorg/threeten/bp/LocalDate;

    move-result-object v1

    add-int/lit8 v3, v3, -0x1

    int-to-long v4, v3

    invoke-virtual {v1, v4, v5}, Lorg/threeten/bp/LocalDate;->d(J)Lorg/threeten/bp/LocalDate;

    move-result-object v1

    invoke-static {v0}, Lorg/threeten/bp/DayOfWeek;->a(I)Lorg/threeten/bp/DayOfWeek;

    move-result-object v0

    invoke-static {v0}, Lorg/threeten/bp/temporal/TemporalAdjusters;->a(Lorg/threeten/bp/DayOfWeek;)Lorg/threeten/bp/temporal/TemporalAdjuster;

    move-result-object v0

    invoke-virtual {v1, v0}, Lorg/threeten/bp/LocalDate;->a(Lorg/threeten/bp/temporal/TemporalAdjuster;)Lorg/threeten/bp/LocalDate;

    move-result-object v0

    .line 485
    sget-object v1, Lorg/threeten/bp/format/ResolverStyle;->a:Lorg/threeten/bp/format/ResolverStyle;

    if-ne p2, v1, :cond_0

    sget-object v1, Lorg/threeten/bp/temporal/ChronoField;->x:Lorg/threeten/bp/temporal/ChronoField;

    invoke-virtual {v0, v1}, Lorg/threeten/bp/LocalDate;->c(Lorg/threeten/bp/temporal/TemporalField;)I

    move-result v1

    if-eq v1, v2, :cond_0

    .line 486
    new-instance v0, Lorg/threeten/bp/DateTimeException;

    const-string v1, "Strict mode rejected date parsed to a different month"

    invoke-direct {v0, v1}, Lorg/threeten/bp/DateTimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 492
    :cond_18
    sget-object v0, Lorg/threeten/bp/temporal/ChronoField;->t:Lorg/threeten/bp/temporal/ChronoField;

    invoke-interface {p1, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1a

    .line 493
    sget-object v1, Lorg/threeten/bp/temporal/ChronoField;->A:Lorg/threeten/bp/temporal/ChronoField;

    sget-object v0, Lorg/threeten/bp/temporal/ChronoField;->A:Lorg/threeten/bp/temporal/ChronoField;

    invoke-interface {p1, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lorg/threeten/bp/temporal/ChronoField;->b(J)I

    move-result v1

    .line 494
    sget-object v0, Lorg/threeten/bp/format/ResolverStyle;->c:Lorg/threeten/bp/format/ResolverStyle;

    if-ne p2, v0, :cond_19

    .line 495
    sget-object v0, Lorg/threeten/bp/temporal/ChronoField;->t:Lorg/threeten/bp/temporal/ChronoField;

    invoke-interface {p1, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v2, v3, v10, v11}, Lorg/threeten/bp/jdk8/Jdk8Methods;->c(JJ)J

    move-result-wide v2

    .line 496
    invoke-static {v1, v8}, Lorg/threeten/bp/LocalDate;->a(II)Lorg/threeten/bp/LocalDate;

    move-result-object v0

    invoke-virtual {v0, v2, v3}, Lorg/threeten/bp/LocalDate;->e(J)Lorg/threeten/bp/LocalDate;

    move-result-object v0

    goto/16 :goto_0

    .line 498
    :cond_19
    sget-object v2, Lorg/threeten/bp/temporal/ChronoField;->t:Lorg/threeten/bp/temporal/ChronoField;

    sget-object v0, Lorg/threeten/bp/temporal/ChronoField;->t:Lorg/threeten/bp/temporal/ChronoField;

    invoke-interface {p1, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Lorg/threeten/bp/temporal/ChronoField;->b(J)I

    move-result v0

    .line 499
    invoke-static {v1, v0}, Lorg/threeten/bp/LocalDate;->a(II)Lorg/threeten/bp/LocalDate;

    move-result-object v0

    goto/16 :goto_0

    .line 501
    :cond_1a
    sget-object v0, Lorg/threeten/bp/temporal/ChronoField;->w:Lorg/threeten/bp/temporal/ChronoField;

    invoke-interface {p1, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1e

    .line 502
    sget-object v0, Lorg/threeten/bp/temporal/ChronoField;->r:Lorg/threeten/bp/temporal/ChronoField;

    invoke-interface {p1, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1c

    .line 503
    sget-object v1, Lorg/threeten/bp/temporal/ChronoField;->A:Lorg/threeten/bp/temporal/ChronoField;

    sget-object v0, Lorg/threeten/bp/temporal/ChronoField;->A:Lorg/threeten/bp/temporal/ChronoField;

    invoke-interface {p1, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lorg/threeten/bp/temporal/ChronoField;->b(J)I

    move-result v1

    .line 504
    sget-object v0, Lorg/threeten/bp/format/ResolverStyle;->c:Lorg/threeten/bp/format/ResolverStyle;

    if-ne p2, v0, :cond_1b

    .line 505
    sget-object v0, Lorg/threeten/bp/temporal/ChronoField;->w:Lorg/threeten/bp/temporal/ChronoField;

    invoke-interface {p1, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v2, v3, v10, v11}, Lorg/threeten/bp/jdk8/Jdk8Methods;->c(JJ)J

    move-result-wide v2

    .line 506
    sget-object v0, Lorg/threeten/bp/temporal/ChronoField;->r:Lorg/threeten/bp/temporal/ChronoField;

    invoke-interface {p1, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-static {v4, v5, v10, v11}, Lorg/threeten/bp/jdk8/Jdk8Methods;->c(JJ)J

    move-result-wide v4

    .line 507
    invoke-static {v1, v8, v8}, Lorg/threeten/bp/LocalDate;->a(III)Lorg/threeten/bp/LocalDate;

    move-result-object v0

    invoke-virtual {v0, v2, v3}, Lorg/threeten/bp/LocalDate;->d(J)Lorg/threeten/bp/LocalDate;

    move-result-object v0

    invoke-virtual {v0, v4, v5}, Lorg/threeten/bp/LocalDate;->e(J)Lorg/threeten/bp/LocalDate;

    move-result-object v0

    goto/16 :goto_0

    .line 509
    :cond_1b
    sget-object v2, Lorg/threeten/bp/temporal/ChronoField;->w:Lorg/threeten/bp/temporal/ChronoField;

    sget-object v0, Lorg/threeten/bp/temporal/ChronoField;->w:Lorg/threeten/bp/temporal/ChronoField;

    invoke-interface {p1, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Lorg/threeten/bp/temporal/ChronoField;->b(J)I

    move-result v2

    .line 510
    sget-object v3, Lorg/threeten/bp/temporal/ChronoField;->r:Lorg/threeten/bp/temporal/ChronoField;

    sget-object v0, Lorg/threeten/bp/temporal/ChronoField;->r:Lorg/threeten/bp/temporal/ChronoField;

    invoke-interface {p1, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Lorg/threeten/bp/temporal/ChronoField;->b(J)I

    move-result v0

    .line 511
    invoke-static {v1, v8, v8}, Lorg/threeten/bp/LocalDate;->a(III)Lorg/threeten/bp/LocalDate;

    move-result-object v3

    add-int/lit8 v2, v2, -0x1

    mul-int/lit8 v2, v2, 0x7

    add-int/lit8 v0, v0, -0x1

    add-int/2addr v0, v2

    int-to-long v4, v0

    invoke-virtual {v3, v4, v5}, Lorg/threeten/bp/LocalDate;->e(J)Lorg/threeten/bp/LocalDate;

    move-result-object v0

    .line 512
    sget-object v2, Lorg/threeten/bp/format/ResolverStyle;->a:Lorg/threeten/bp/format/ResolverStyle;

    if-ne p2, v2, :cond_0

    sget-object v2, Lorg/threeten/bp/temporal/ChronoField;->A:Lorg/threeten/bp/temporal/ChronoField;

    invoke-virtual {v0, v2}, Lorg/threeten/bp/LocalDate;->c(Lorg/threeten/bp/temporal/TemporalField;)I

    move-result v2

    if-eq v2, v1, :cond_0

    .line 513
    new-instance v0, Lorg/threeten/bp/DateTimeException;

    const-string v1, "Strict mode rejected date parsed to a different year"

    invoke-direct {v0, v1}, Lorg/threeten/bp/DateTimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 517
    :cond_1c
    sget-object v0, Lorg/threeten/bp/temporal/ChronoField;->p:Lorg/threeten/bp/temporal/ChronoField;

    invoke-interface {p1, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1e

    .line 518
    sget-object v1, Lorg/threeten/bp/temporal/ChronoField;->A:Lorg/threeten/bp/temporal/ChronoField;

    sget-object v0, Lorg/threeten/bp/temporal/ChronoField;->A:Lorg/threeten/bp/temporal/ChronoField;

    invoke-interface {p1, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lorg/threeten/bp/temporal/ChronoField;->b(J)I

    move-result v1

    .line 519
    sget-object v0, Lorg/threeten/bp/format/ResolverStyle;->c:Lorg/threeten/bp/format/ResolverStyle;

    if-ne p2, v0, :cond_1d

    .line 520
    sget-object v0, Lorg/threeten/bp/temporal/ChronoField;->w:Lorg/threeten/bp/temporal/ChronoField;

    invoke-interface {p1, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v2, v3, v10, v11}, Lorg/threeten/bp/jdk8/Jdk8Methods;->c(JJ)J

    move-result-wide v2

    .line 521
    sget-object v0, Lorg/threeten/bp/temporal/ChronoField;->p:Lorg/threeten/bp/temporal/ChronoField;

    invoke-interface {p1, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-static {v4, v5, v10, v11}, Lorg/threeten/bp/jdk8/Jdk8Methods;->c(JJ)J

    move-result-wide v4

    .line 522
    invoke-static {v1, v8, v8}, Lorg/threeten/bp/LocalDate;->a(III)Lorg/threeten/bp/LocalDate;

    move-result-object v0

    invoke-virtual {v0, v2, v3}, Lorg/threeten/bp/LocalDate;->d(J)Lorg/threeten/bp/LocalDate;

    move-result-object v0

    invoke-virtual {v0, v4, v5}, Lorg/threeten/bp/LocalDate;->e(J)Lorg/threeten/bp/LocalDate;

    move-result-object v0

    goto/16 :goto_0

    .line 524
    :cond_1d
    sget-object v2, Lorg/threeten/bp/temporal/ChronoField;->w:Lorg/threeten/bp/temporal/ChronoField;

    sget-object v0, Lorg/threeten/bp/temporal/ChronoField;->w:Lorg/threeten/bp/temporal/ChronoField;

    invoke-interface {p1, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Lorg/threeten/bp/temporal/ChronoField;->b(J)I

    move-result v2

    .line 525
    sget-object v3, Lorg/threeten/bp/temporal/ChronoField;->p:Lorg/threeten/bp/temporal/ChronoField;

    sget-object v0, Lorg/threeten/bp/temporal/ChronoField;->p:Lorg/threeten/bp/temporal/ChronoField;

    invoke-interface {p1, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Lorg/threeten/bp/temporal/ChronoField;->b(J)I

    move-result v0

    .line 526
    invoke-static {v1, v8, v8}, Lorg/threeten/bp/LocalDate;->a(III)Lorg/threeten/bp/LocalDate;

    move-result-object v3

    add-int/lit8 v2, v2, -0x1

    int-to-long v4, v2

    invoke-virtual {v3, v4, v5}, Lorg/threeten/bp/LocalDate;->d(J)Lorg/threeten/bp/LocalDate;

    move-result-object v2

    invoke-static {v0}, Lorg/threeten/bp/DayOfWeek;->a(I)Lorg/threeten/bp/DayOfWeek;

    move-result-object v0

    invoke-static {v0}, Lorg/threeten/bp/temporal/TemporalAdjusters;->a(Lorg/threeten/bp/DayOfWeek;)Lorg/threeten/bp/temporal/TemporalAdjuster;

    move-result-object v0

    invoke-virtual {v2, v0}, Lorg/threeten/bp/LocalDate;->a(Lorg/threeten/bp/temporal/TemporalAdjuster;)Lorg/threeten/bp/LocalDate;

    move-result-object v0

    .line 527
    sget-object v2, Lorg/threeten/bp/format/ResolverStyle;->a:Lorg/threeten/bp/format/ResolverStyle;

    if-ne p2, v2, :cond_0

    sget-object v2, Lorg/threeten/bp/temporal/ChronoField;->A:Lorg/threeten/bp/temporal/ChronoField;

    invoke-virtual {v0, v2}, Lorg/threeten/bp/LocalDate;->c(Lorg/threeten/bp/temporal/TemporalField;)I

    move-result v2

    if-eq v2, v1, :cond_0

    .line 528
    new-instance v0, Lorg/threeten/bp/DateTimeException;

    const-string v1, "Strict mode rejected date parsed to a different month"

    invoke-direct {v0, v1}, Lorg/threeten/bp/DateTimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 534
    :cond_1e
    const/4 v0, 0x0

    goto/16 :goto_0
.end method

.method public synthetic a(III)Lorg/threeten/bp/chrono/ChronoLocalDate;
    .locals 1

    .prologue
    .line 98
    invoke-virtual {p0, p1, p2, p3}, Lorg/threeten/bp/chrono/IsoChronology;->b(III)Lorg/threeten/bp/LocalDate;

    move-result-object v0

    return-object v0
.end method

.method public synthetic a(Lorg/threeten/bp/Instant;Lorg/threeten/bp/ZoneId;)Lorg/threeten/bp/chrono/ChronoZonedDateTime;
    .locals 1

    .prologue
    .line 98
    invoke-virtual {p0, p1, p2}, Lorg/threeten/bp/chrono/IsoChronology;->b(Lorg/threeten/bp/Instant;Lorg/threeten/bp/ZoneId;)Lorg/threeten/bp/ZonedDateTime;

    move-result-object v0

    return-object v0
.end method

.method public synthetic a(I)Lorg/threeten/bp/chrono/Era;
    .locals 1

    .prologue
    .line 98
    invoke-virtual {p0, p1}, Lorg/threeten/bp/chrono/IsoChronology;->b(I)Lorg/threeten/bp/chrono/IsoEra;

    move-result-object v0

    return-object v0
.end method

.method public a(J)Z
    .locals 5

    .prologue
    const-wide/16 v2, 0x0

    .line 357
    const-wide/16 v0, 0x3

    and-long/2addr v0, p1

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    const-wide/16 v0, 0x64

    rem-long v0, p1, v0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    const-wide/16 v0, 0x190

    rem-long v0, p1, v0

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 154
    const-string v0, "iso8601"

    return-object v0
.end method

.method public b(III)Lorg/threeten/bp/LocalDate;
    .locals 1

    .prologue
    .line 188
    invoke-static {p1, p2, p3}, Lorg/threeten/bp/LocalDate;->a(III)Lorg/threeten/bp/LocalDate;

    move-result-object v0

    return-object v0
.end method

.method public b(Lorg/threeten/bp/Instant;Lorg/threeten/bp/ZoneId;)Lorg/threeten/bp/ZonedDateTime;
    .locals 1

    .prologue
    .line 280
    invoke-static {p1, p2}, Lorg/threeten/bp/ZonedDateTime;->a(Lorg/threeten/bp/Instant;Lorg/threeten/bp/ZoneId;)Lorg/threeten/bp/ZonedDateTime;

    move-result-object v0

    return-object v0
.end method

.method public synthetic b(Lorg/threeten/bp/temporal/TemporalAccessor;)Lorg/threeten/bp/chrono/ChronoLocalDate;
    .locals 1

    .prologue
    .line 98
    invoke-virtual {p0, p1}, Lorg/threeten/bp/chrono/IsoChronology;->e(Lorg/threeten/bp/temporal/TemporalAccessor;)Lorg/threeten/bp/LocalDate;

    move-result-object v0

    return-object v0
.end method

.method public b(I)Lorg/threeten/bp/chrono/IsoEra;
    .locals 1

    .prologue
    .line 370
    invoke-static {p1}, Lorg/threeten/bp/chrono/IsoEra;->a(I)Lorg/threeten/bp/chrono/IsoEra;

    move-result-object v0

    return-object v0
.end method

.method public synthetic c(Lorg/threeten/bp/temporal/TemporalAccessor;)Lorg/threeten/bp/chrono/ChronoLocalDateTime;
    .locals 1

    .prologue
    .line 98
    invoke-virtual {p0, p1}, Lorg/threeten/bp/chrono/IsoChronology;->f(Lorg/threeten/bp/temporal/TemporalAccessor;)Lorg/threeten/bp/LocalDateTime;

    move-result-object v0

    return-object v0
.end method

.method public synthetic d(Lorg/threeten/bp/temporal/TemporalAccessor;)Lorg/threeten/bp/chrono/ChronoZonedDateTime;
    .locals 1

    .prologue
    .line 98
    invoke-virtual {p0, p1}, Lorg/threeten/bp/chrono/IsoChronology;->g(Lorg/threeten/bp/temporal/TemporalAccessor;)Lorg/threeten/bp/ZonedDateTime;

    move-result-object v0

    return-object v0
.end method

.method public e(Lorg/threeten/bp/temporal/TemporalAccessor;)Lorg/threeten/bp/LocalDate;
    .locals 1

    .prologue
    .line 237
    invoke-static {p1}, Lorg/threeten/bp/LocalDate;->a(Lorg/threeten/bp/temporal/TemporalAccessor;)Lorg/threeten/bp/LocalDate;

    move-result-object v0

    return-object v0
.end method

.method public f(Lorg/threeten/bp/temporal/TemporalAccessor;)Lorg/threeten/bp/LocalDateTime;
    .locals 1

    .prologue
    .line 251
    invoke-static {p1}, Lorg/threeten/bp/LocalDateTime;->a(Lorg/threeten/bp/temporal/TemporalAccessor;)Lorg/threeten/bp/LocalDateTime;

    move-result-object v0

    return-object v0
.end method

.method public g(Lorg/threeten/bp/temporal/TemporalAccessor;)Lorg/threeten/bp/ZonedDateTime;
    .locals 1

    .prologue
    .line 265
    invoke-static {p1}, Lorg/threeten/bp/ZonedDateTime;->a(Lorg/threeten/bp/temporal/TemporalAccessor;)Lorg/threeten/bp/ZonedDateTime;

    move-result-object v0

    return-object v0
.end method
