.class final Lorg/threeten/bp/zone/Ser;
.super Ljava/lang/Object;
.source "Ser.java"

# interfaces
.implements Ljava/io/Externalizable;


# instance fields
.field private a:B

.field private b:Ljava/lang/Object;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 75
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 76
    return-void
.end method

.method constructor <init>(BLjava/lang/Object;)V
    .locals 0

    .prologue
    .line 84
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 85
    iput-byte p1, p0, Lorg/threeten/bp/zone/Ser;->a:B

    .line 86
    iput-object p2, p0, Lorg/threeten/bp/zone/Ser;->b:Ljava/lang/Object;

    .line 87
    return-void
.end method

.method private static a(BLjava/io/DataInput;)Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 137
    packed-switch p0, :pswitch_data_0

    .line 145
    new-instance v0, Ljava/io/StreamCorruptedException;

    const-string v1, "Unknown serialized type"

    invoke-direct {v0, v1}, Ljava/io/StreamCorruptedException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 139
    :pswitch_0
    invoke-static {p1}, Lorg/threeten/bp/zone/StandardZoneRules;->a(Ljava/io/DataInput;)Lorg/threeten/bp/zone/StandardZoneRules;

    move-result-object v0

    .line 143
    :goto_0
    return-object v0

    .line 141
    :pswitch_1
    invoke-static {p1}, Lorg/threeten/bp/zone/ZoneOffsetTransition;->a(Ljava/io/DataInput;)Lorg/threeten/bp/zone/ZoneOffsetTransition;

    move-result-object v0

    goto :goto_0

    .line 143
    :pswitch_2
    invoke-static {p1}, Lorg/threeten/bp/zone/ZoneOffsetTransitionRule;->a(Ljava/io/DataInput;)Lorg/threeten/bp/zone/ZoneOffsetTransitionRule;

    move-result-object v0

    goto :goto_0

    .line 137
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method static a(Ljava/io/DataInput;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 132
    invoke-interface {p0}, Ljava/io/DataInput;->readByte()B

    move-result v0

    .line 133
    invoke-static {v0, p0}, Lorg/threeten/bp/zone/Ser;->a(BLjava/io/DataInput;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method private static a(BLjava/lang/Object;Ljava/io/DataOutput;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 104
    invoke-interface {p2, p0}, Ljava/io/DataOutput;->writeByte(I)V

    .line 105
    packed-switch p0, :pswitch_data_0

    .line 116
    new-instance v0, Ljava/io/InvalidClassException;

    const-string v1, "Unknown serialized type"

    invoke-direct {v0, v1}, Ljava/io/InvalidClassException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 107
    :pswitch_0
    check-cast p1, Lorg/threeten/bp/zone/StandardZoneRules;

    invoke-virtual {p1, p2}, Lorg/threeten/bp/zone/StandardZoneRules;->a(Ljava/io/DataOutput;)V

    .line 118
    :goto_0
    return-void

    .line 110
    :pswitch_1
    check-cast p1, Lorg/threeten/bp/zone/ZoneOffsetTransition;

    invoke-virtual {p1, p2}, Lorg/threeten/bp/zone/ZoneOffsetTransition;->a(Ljava/io/DataOutput;)V

    goto :goto_0

    .line 113
    :pswitch_2
    check-cast p1, Lorg/threeten/bp/zone/ZoneOffsetTransitionRule;

    invoke-virtual {p1, p2}, Lorg/threeten/bp/zone/ZoneOffsetTransitionRule;->a(Ljava/io/DataOutput;)V

    goto :goto_0

    .line 105
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method static a(JLjava/io/DataOutput;)V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const-wide/16 v4, 0x384

    .line 196
    const-wide v0, -0x110bc5000L

    cmp-long v0, p0, v0

    if-ltz v0, :cond_0

    const-wide v0, 0x26cb5db00L

    cmp-long v0, p0, v0

    if-gez v0, :cond_0

    rem-long v0, p0, v4

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 197
    const-wide v0, 0x110bc5000L

    add-long/2addr v0, p0

    div-long/2addr v0, v4

    long-to-int v0, v0

    .line 198
    ushr-int/lit8 v1, v0, 0x10

    and-int/lit16 v1, v1, 0xff

    invoke-interface {p2, v1}, Ljava/io/DataOutput;->writeByte(I)V

    .line 199
    ushr-int/lit8 v1, v0, 0x8

    and-int/lit16 v1, v1, 0xff

    invoke-interface {p2, v1}, Ljava/io/DataOutput;->writeByte(I)V

    .line 200
    and-int/lit16 v0, v0, 0xff

    invoke-interface {p2, v0}, Ljava/io/DataOutput;->writeByte(I)V

    .line 205
    :goto_0
    return-void

    .line 202
    :cond_0
    const/16 v0, 0xff

    invoke-interface {p2, v0}, Ljava/io/DataOutput;->writeByte(I)V

    .line 203
    invoke-interface {p2, p0, p1}, Ljava/io/DataOutput;->writeLong(J)V

    goto :goto_0
.end method

.method static a(Lorg/threeten/bp/ZoneOffset;Ljava/io/DataOutput;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/16 v1, 0x7f

    .line 167
    invoke-virtual {p0}, Lorg/threeten/bp/ZoneOffset;->f()I

    move-result v2

    .line 168
    rem-int/lit16 v0, v2, 0x384

    if-nez v0, :cond_1

    div-int/lit16 v0, v2, 0x384

    .line 169
    :goto_0
    invoke-interface {p1, v0}, Ljava/io/DataOutput;->writeByte(I)V

    .line 170
    if-ne v0, v1, :cond_0

    .line 171
    invoke-interface {p1, v2}, Ljava/io/DataOutput;->writeInt(I)V

    .line 173
    :cond_0
    return-void

    :cond_1
    move v0, v1

    .line 168
    goto :goto_0
.end method

.method static b(Ljava/io/DataInput;)Lorg/threeten/bp/ZoneOffset;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 183
    invoke-interface {p0}, Ljava/io/DataInput;->readByte()B

    move-result v0

    .line 184
    const/16 v1, 0x7f

    if-ne v0, v1, :cond_0

    invoke-interface {p0}, Ljava/io/DataInput;->readInt()I

    move-result v0

    invoke-static {v0}, Lorg/threeten/bp/ZoneOffset;->a(I)Lorg/threeten/bp/ZoneOffset;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    mul-int/lit16 v0, v0, 0x384

    invoke-static {v0}, Lorg/threeten/bp/ZoneOffset;->a(I)Lorg/threeten/bp/ZoneOffset;

    move-result-object v0

    goto :goto_0
.end method

.method static c(Ljava/io/DataInput;)J
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 215
    invoke-interface {p0}, Ljava/io/DataInput;->readByte()B

    move-result v0

    and-int/lit16 v0, v0, 0xff

    .line 216
    const/16 v1, 0xff

    if-ne v0, v1, :cond_0

    .line 217
    invoke-interface {p0}, Ljava/io/DataInput;->readLong()J

    move-result-wide v0

    .line 222
    :goto_0
    return-wide v0

    .line 219
    :cond_0
    invoke-interface {p0}, Ljava/io/DataInput;->readByte()B

    move-result v1

    and-int/lit16 v1, v1, 0xff

    .line 220
    invoke-interface {p0}, Ljava/io/DataInput;->readByte()B

    move-result v2

    and-int/lit16 v2, v2, 0xff

    .line 221
    shl-int/lit8 v0, v0, 0x10

    shl-int/lit8 v1, v1, 0x8

    add-int/2addr v0, v1

    add-int/2addr v0, v2

    int-to-long v0, v0

    .line 222
    const-wide/16 v2, 0x384

    mul-long/2addr v0, v2

    const-wide v2, 0x110bc5000L

    sub-long/2addr v0, v2

    goto :goto_0
.end method

.method private readResolve()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 155
    iget-object v0, p0, Lorg/threeten/bp/zone/Ser;->b:Ljava/lang/Object;

    return-object v0
.end method


# virtual methods
.method public readExternal(Ljava/io/ObjectInput;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 127
    invoke-interface {p1}, Ljava/io/ObjectInput;->readByte()B

    move-result v0

    iput-byte v0, p0, Lorg/threeten/bp/zone/Ser;->a:B

    .line 128
    iget-byte v0, p0, Lorg/threeten/bp/zone/Ser;->a:B

    invoke-static {v0, p1}, Lorg/threeten/bp/zone/Ser;->a(BLjava/io/DataInput;)Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Lorg/threeten/bp/zone/Ser;->b:Ljava/lang/Object;

    .line 129
    return-void
.end method

.method public writeExternal(Ljava/io/ObjectOutput;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 96
    iget-byte v0, p0, Lorg/threeten/bp/zone/Ser;->a:B

    iget-object v1, p0, Lorg/threeten/bp/zone/Ser;->b:Ljava/lang/Object;

    invoke-static {v0, v1, p1}, Lorg/threeten/bp/zone/Ser;->a(BLjava/lang/Object;Ljava/io/DataOutput;)V

    .line 97
    return-void
.end method
