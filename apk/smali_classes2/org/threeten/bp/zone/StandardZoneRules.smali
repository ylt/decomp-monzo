.class final Lorg/threeten/bp/zone/StandardZoneRules;
.super Lorg/threeten/bp/zone/ZoneRules;
.source "StandardZoneRules.java"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field private final a:[J

.field private final b:[Lorg/threeten/bp/ZoneOffset;

.field private final c:[J

.field private final d:[Lorg/threeten/bp/LocalDateTime;

.field private final e:[Lorg/threeten/bp/ZoneOffset;

.field private final f:[Lorg/threeten/bp/zone/ZoneOffsetTransitionRule;

.field private final g:Ljava/util/concurrent/ConcurrentMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentMap",
            "<",
            "Ljava/lang/Integer;",
            "[",
            "Lorg/threeten/bp/zone/ZoneOffsetTransition;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>([J[Lorg/threeten/bp/ZoneOffset;[J[Lorg/threeten/bp/ZoneOffset;[Lorg/threeten/bp/zone/ZoneOffsetTransitionRule;)V
    .locals 8

    .prologue
    .line 175
    invoke-direct {p0}, Lorg/threeten/bp/zone/ZoneRules;-><init>()V

    .line 101
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lorg/threeten/bp/zone/StandardZoneRules;->g:Ljava/util/concurrent/ConcurrentMap;

    .line 177
    iput-object p1, p0, Lorg/threeten/bp/zone/StandardZoneRules;->a:[J

    .line 178
    iput-object p2, p0, Lorg/threeten/bp/zone/StandardZoneRules;->b:[Lorg/threeten/bp/ZoneOffset;

    .line 179
    iput-object p3, p0, Lorg/threeten/bp/zone/StandardZoneRules;->c:[J

    .line 180
    iput-object p4, p0, Lorg/threeten/bp/zone/StandardZoneRules;->e:[Lorg/threeten/bp/ZoneOffset;

    .line 181
    iput-object p5, p0, Lorg/threeten/bp/zone/StandardZoneRules;->f:[Lorg/threeten/bp/zone/ZoneOffsetTransitionRule;

    .line 184
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 185
    const/4 v0, 0x0

    :goto_0
    array-length v2, p3

    if-ge v0, v2, :cond_1

    .line 186
    aget-object v2, p4, v0

    .line 187
    add-int/lit8 v3, v0, 0x1

    aget-object v3, p4, v3

    .line 188
    new-instance v4, Lorg/threeten/bp/zone/ZoneOffsetTransition;

    aget-wide v6, p3, v0

    invoke-direct {v4, v6, v7, v2, v3}, Lorg/threeten/bp/zone/ZoneOffsetTransition;-><init>(JLorg/threeten/bp/ZoneOffset;Lorg/threeten/bp/ZoneOffset;)V

    .line 189
    invoke-virtual {v4}, Lorg/threeten/bp/zone/ZoneOffsetTransition;->h()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 190
    invoke-virtual {v4}, Lorg/threeten/bp/zone/ZoneOffsetTransition;->c()Lorg/threeten/bp/LocalDateTime;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 191
    invoke-virtual {v4}, Lorg/threeten/bp/zone/ZoneOffsetTransition;->d()Lorg/threeten/bp/LocalDateTime;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 185
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 193
    :cond_0
    invoke-virtual {v4}, Lorg/threeten/bp/zone/ZoneOffsetTransition;->d()Lorg/threeten/bp/LocalDateTime;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 194
    invoke-virtual {v4}, Lorg/threeten/bp/zone/ZoneOffsetTransition;->c()Lorg/threeten/bp/LocalDateTime;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 197
    :cond_1
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [Lorg/threeten/bp/LocalDateTime;

    invoke-interface {v1, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lorg/threeten/bp/LocalDateTime;

    iput-object v0, p0, Lorg/threeten/bp/zone/StandardZoneRules;->d:[Lorg/threeten/bp/LocalDateTime;

    .line 198
    return-void
.end method

.method private a(JLorg/threeten/bp/ZoneOffset;)I
    .locals 5

    .prologue
    .line 548
    invoke-virtual {p3}, Lorg/threeten/bp/ZoneOffset;->f()I

    move-result v0

    int-to-long v0, v0

    add-long/2addr v0, p1

    .line 549
    const-wide/32 v2, 0x15180

    invoke-static {v0, v1, v2, v3}, Lorg/threeten/bp/jdk8/Jdk8Methods;->e(JJ)J

    move-result-wide v0

    .line 550
    invoke-static {v0, v1}, Lorg/threeten/bp/LocalDate;->a(J)Lorg/threeten/bp/LocalDate;

    move-result-object v0

    invoke-virtual {v0}, Lorg/threeten/bp/LocalDate;->d()I

    move-result v0

    return v0
.end method

.method private a(Lorg/threeten/bp/LocalDateTime;Lorg/threeten/bp/zone/ZoneOffsetTransition;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 388
    invoke-virtual {p2}, Lorg/threeten/bp/zone/ZoneOffsetTransition;->c()Lorg/threeten/bp/LocalDateTime;

    move-result-object v0

    .line 389
    invoke-virtual {p2}, Lorg/threeten/bp/zone/ZoneOffsetTransition;->h()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 390
    invoke-virtual {p1, v0}, Lorg/threeten/bp/LocalDateTime;->c(Lorg/threeten/bp/chrono/ChronoLocalDateTime;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 391
    invoke-virtual {p2}, Lorg/threeten/bp/zone/ZoneOffsetTransition;->e()Lorg/threeten/bp/ZoneOffset;

    move-result-object p2

    .line 405
    :cond_0
    :goto_0
    return-object p2

    .line 393
    :cond_1
    invoke-virtual {p2}, Lorg/threeten/bp/zone/ZoneOffsetTransition;->d()Lorg/threeten/bp/LocalDateTime;

    move-result-object v0

    invoke-virtual {p1, v0}, Lorg/threeten/bp/LocalDateTime;->c(Lorg/threeten/bp/chrono/ChronoLocalDateTime;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 396
    invoke-virtual {p2}, Lorg/threeten/bp/zone/ZoneOffsetTransition;->f()Lorg/threeten/bp/ZoneOffset;

    move-result-object p2

    goto :goto_0

    .line 399
    :cond_2
    invoke-virtual {p1, v0}, Lorg/threeten/bp/LocalDateTime;->c(Lorg/threeten/bp/chrono/ChronoLocalDateTime;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 400
    invoke-virtual {p2}, Lorg/threeten/bp/zone/ZoneOffsetTransition;->f()Lorg/threeten/bp/ZoneOffset;

    move-result-object p2

    goto :goto_0

    .line 402
    :cond_3
    invoke-virtual {p2}, Lorg/threeten/bp/zone/ZoneOffsetTransition;->d()Lorg/threeten/bp/LocalDateTime;

    move-result-object v0

    invoke-virtual {p1, v0}, Lorg/threeten/bp/LocalDateTime;->c(Lorg/threeten/bp/chrono/ChronoLocalDateTime;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 403
    invoke-virtual {p2}, Lorg/threeten/bp/zone/ZoneOffsetTransition;->e()Lorg/threeten/bp/ZoneOffset;

    move-result-object p2

    goto :goto_0
.end method

.method static a(Ljava/io/DataInput;)Lorg/threeten/bp/zone/StandardZoneRules;
    .locals 10
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    const/4 v6, 0x0

    .line 245
    invoke-interface {p0}, Ljava/io/DataInput;->readInt()I

    move-result v2

    .line 246
    new-array v1, v2, [J

    move v0, v6

    .line 247
    :goto_0
    if-ge v0, v2, :cond_0

    .line 248
    invoke-static {p0}, Lorg/threeten/bp/zone/Ser;->c(Ljava/io/DataInput;)J

    move-result-wide v4

    aput-wide v4, v1, v0

    .line 247
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 250
    :cond_0
    add-int/lit8 v0, v2, 0x1

    new-array v2, v0, [Lorg/threeten/bp/ZoneOffset;

    move v0, v6

    .line 251
    :goto_1
    array-length v3, v2

    if-ge v0, v3, :cond_1

    .line 252
    invoke-static {p0}, Lorg/threeten/bp/zone/Ser;->b(Ljava/io/DataInput;)Lorg/threeten/bp/ZoneOffset;

    move-result-object v3

    aput-object v3, v2, v0

    .line 251
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 254
    :cond_1
    invoke-interface {p0}, Ljava/io/DataInput;->readInt()I

    move-result v4

    .line 255
    new-array v3, v4, [J

    move v0, v6

    .line 256
    :goto_2
    if-ge v0, v4, :cond_2

    .line 257
    invoke-static {p0}, Lorg/threeten/bp/zone/Ser;->c(Ljava/io/DataInput;)J

    move-result-wide v8

    aput-wide v8, v3, v0

    .line 256
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 259
    :cond_2
    add-int/lit8 v0, v4, 0x1

    new-array v4, v0, [Lorg/threeten/bp/ZoneOffset;

    move v0, v6

    .line 260
    :goto_3
    array-length v5, v4

    if-ge v0, v5, :cond_3

    .line 261
    invoke-static {p0}, Lorg/threeten/bp/zone/Ser;->b(Ljava/io/DataInput;)Lorg/threeten/bp/ZoneOffset;

    move-result-object v5

    aput-object v5, v4, v0

    .line 260
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 263
    :cond_3
    invoke-interface {p0}, Ljava/io/DataInput;->readByte()B

    move-result v0

    .line 264
    new-array v5, v0, [Lorg/threeten/bp/zone/ZoneOffsetTransitionRule;

    .line 265
    :goto_4
    if-ge v6, v0, :cond_4

    .line 266
    invoke-static {p0}, Lorg/threeten/bp/zone/ZoneOffsetTransitionRule;->a(Ljava/io/DataInput;)Lorg/threeten/bp/zone/ZoneOffsetTransitionRule;

    move-result-object v7

    aput-object v7, v5, v6

    .line 265
    add-int/lit8 v6, v6, 0x1

    goto :goto_4

    .line 268
    :cond_4
    new-instance v0, Lorg/threeten/bp/zone/StandardZoneRules;

    invoke-direct/range {v0 .. v5}, Lorg/threeten/bp/zone/StandardZoneRules;-><init>([J[Lorg/threeten/bp/ZoneOffset;[J[Lorg/threeten/bp/ZoneOffset;[Lorg/threeten/bp/zone/ZoneOffsetTransitionRule;)V

    return-object v0
.end method

.method private a(I)[Lorg/threeten/bp/zone/ZoneOffsetTransition;
    .locals 5

    .prologue
    .line 423
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    .line 424
    iget-object v0, p0, Lorg/threeten/bp/zone/StandardZoneRules;->g:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v0, v2}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lorg/threeten/bp/zone/ZoneOffsetTransition;

    .line 425
    if-eqz v0, :cond_0

    .line 436
    :goto_0
    return-object v0

    .line 428
    :cond_0
    iget-object v3, p0, Lorg/threeten/bp/zone/StandardZoneRules;->f:[Lorg/threeten/bp/zone/ZoneOffsetTransitionRule;

    .line 429
    array-length v0, v3

    new-array v1, v0, [Lorg/threeten/bp/zone/ZoneOffsetTransition;

    .line 430
    const/4 v0, 0x0

    :goto_1
    array-length v4, v3

    if-ge v0, v4, :cond_1

    .line 431
    aget-object v4, v3, v0

    invoke-virtual {v4, p1}, Lorg/threeten/bp/zone/ZoneOffsetTransitionRule;->a(I)Lorg/threeten/bp/zone/ZoneOffsetTransition;

    move-result-object v4

    aput-object v4, v1, v0

    .line 430
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 433
    :cond_1
    const/16 v0, 0x834

    if-ge p1, v0, :cond_2

    .line 434
    iget-object v0, p0, Lorg/threeten/bp/zone/StandardZoneRules;->g:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v0, v2, v1}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_2
    move-object v0, v1

    .line 436
    goto :goto_0
.end method

.method private c(Lorg/threeten/bp/LocalDateTime;)Ljava/lang/Object;
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 334
    iget-object v0, p0, Lorg/threeten/bp/zone/StandardZoneRules;->f:[Lorg/threeten/bp/zone/ZoneOffsetTransitionRule;

    array-length v0, v0

    if-lez v0, :cond_2

    iget-object v0, p0, Lorg/threeten/bp/zone/StandardZoneRules;->d:[Lorg/threeten/bp/LocalDateTime;

    iget-object v2, p0, Lorg/threeten/bp/zone/StandardZoneRules;->d:[Lorg/threeten/bp/LocalDateTime;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    aget-object v0, v0, v2

    invoke-virtual {p1, v0}, Lorg/threeten/bp/LocalDateTime;->b(Lorg/threeten/bp/chrono/ChronoLocalDateTime;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 336
    invoke-virtual {p1}, Lorg/threeten/bp/LocalDateTime;->b()I

    move-result v0

    invoke-direct {p0, v0}, Lorg/threeten/bp/zone/StandardZoneRules;->a(I)[Lorg/threeten/bp/zone/ZoneOffsetTransition;

    move-result-object v2

    .line 337
    const/4 v0, 0x0

    .line 338
    array-length v3, v2

    :goto_0
    if-ge v1, v3, :cond_0

    aget-object v4, v2, v1

    .line 339
    invoke-direct {p0, p1, v4}, Lorg/threeten/bp/zone/StandardZoneRules;->a(Lorg/threeten/bp/LocalDateTime;Lorg/threeten/bp/zone/ZoneOffsetTransition;)Ljava/lang/Object;

    move-result-object v0

    .line 340
    instance-of v5, v0, Lorg/threeten/bp/zone/ZoneOffsetTransition;

    if-nez v5, :cond_0

    invoke-virtual {v4}, Lorg/threeten/bp/zone/ZoneOffsetTransition;->e()Lorg/threeten/bp/ZoneOffset;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 376
    :cond_0
    :goto_1
    return-object v0

    .line 338
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 348
    :cond_2
    iget-object v0, p0, Lorg/threeten/bp/zone/StandardZoneRules;->d:[Lorg/threeten/bp/LocalDateTime;

    invoke-static {v0, p1}, Ljava/util/Arrays;->binarySearch([Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v0

    .line 349
    const/4 v2, -0x1

    if-ne v0, v2, :cond_3

    .line 351
    iget-object v0, p0, Lorg/threeten/bp/zone/StandardZoneRules;->e:[Lorg/threeten/bp/ZoneOffset;

    aget-object v0, v0, v1

    goto :goto_1

    .line 353
    :cond_3
    if-gez v0, :cond_5

    .line 355
    neg-int v0, v0

    add-int/lit8 v0, v0, -0x2

    .line 361
    :cond_4
    :goto_2
    and-int/lit8 v1, v0, 0x1

    if-nez v1, :cond_7

    .line 363
    iget-object v1, p0, Lorg/threeten/bp/zone/StandardZoneRules;->d:[Lorg/threeten/bp/LocalDateTime;

    aget-object v1, v1, v0

    .line 364
    iget-object v2, p0, Lorg/threeten/bp/zone/StandardZoneRules;->d:[Lorg/threeten/bp/LocalDateTime;

    add-int/lit8 v3, v0, 0x1

    aget-object v2, v2, v3

    .line 365
    iget-object v3, p0, Lorg/threeten/bp/zone/StandardZoneRules;->e:[Lorg/threeten/bp/ZoneOffset;

    div-int/lit8 v4, v0, 0x2

    aget-object v3, v3, v4

    .line 366
    iget-object v4, p0, Lorg/threeten/bp/zone/StandardZoneRules;->e:[Lorg/threeten/bp/ZoneOffset;

    div-int/lit8 v0, v0, 0x2

    add-int/lit8 v0, v0, 0x1

    aget-object v4, v4, v0

    .line 367
    invoke-virtual {v4}, Lorg/threeten/bp/ZoneOffset;->f()I

    move-result v0

    invoke-virtual {v3}, Lorg/threeten/bp/ZoneOffset;->f()I

    move-result v5

    if-le v0, v5, :cond_6

    .line 369
    new-instance v0, Lorg/threeten/bp/zone/ZoneOffsetTransition;

    invoke-direct {v0, v1, v3, v4}, Lorg/threeten/bp/zone/ZoneOffsetTransition;-><init>(Lorg/threeten/bp/LocalDateTime;Lorg/threeten/bp/ZoneOffset;Lorg/threeten/bp/ZoneOffset;)V

    goto :goto_1

    .line 356
    :cond_5
    iget-object v1, p0, Lorg/threeten/bp/zone/StandardZoneRules;->d:[Lorg/threeten/bp/LocalDateTime;

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_4

    iget-object v1, p0, Lorg/threeten/bp/zone/StandardZoneRules;->d:[Lorg/threeten/bp/LocalDateTime;

    aget-object v1, v1, v0

    iget-object v2, p0, Lorg/threeten/bp/zone/StandardZoneRules;->d:[Lorg/threeten/bp/LocalDateTime;

    add-int/lit8 v3, v0, 0x1

    aget-object v2, v2, v3

    invoke-virtual {v1, v2}, Lorg/threeten/bp/LocalDateTime;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 359
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 372
    :cond_6
    new-instance v0, Lorg/threeten/bp/zone/ZoneOffsetTransition;

    invoke-direct {v0, v2, v3, v4}, Lorg/threeten/bp/zone/ZoneOffsetTransition;-><init>(Lorg/threeten/bp/LocalDateTime;Lorg/threeten/bp/ZoneOffset;Lorg/threeten/bp/ZoneOffset;)V

    goto :goto_1

    .line 376
    :cond_7
    iget-object v1, p0, Lorg/threeten/bp/zone/StandardZoneRules;->e:[Lorg/threeten/bp/ZoneOffset;

    div-int/lit8 v0, v0, 0x2

    add-int/lit8 v0, v0, 0x1

    aget-object v0, v1, v0

    goto :goto_1
.end method

.method private writeReplace()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 207
    new-instance v0, Lorg/threeten/bp/zone/Ser;

    const/4 v1, 0x1

    invoke-direct {v0, v1, p0}, Lorg/threeten/bp/zone/Ser;-><init>(BLjava/lang/Object;)V

    return-object v0
.end method


# virtual methods
.method public a(Lorg/threeten/bp/LocalDateTime;)Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/threeten/bp/LocalDateTime;",
            ")",
            "Ljava/util/List",
            "<",
            "Lorg/threeten/bp/ZoneOffset;",
            ">;"
        }
    .end annotation

    .prologue
    .line 319
    invoke-direct {p0, p1}, Lorg/threeten/bp/zone/StandardZoneRules;->c(Lorg/threeten/bp/LocalDateTime;)Ljava/lang/Object;

    move-result-object v0

    .line 320
    instance-of v1, v0, Lorg/threeten/bp/zone/ZoneOffsetTransition;

    if-eqz v1, :cond_0

    .line 321
    check-cast v0, Lorg/threeten/bp/zone/ZoneOffsetTransition;

    invoke-virtual {v0}, Lorg/threeten/bp/zone/ZoneOffsetTransition;->i()Ljava/util/List;

    move-result-object v0

    .line 323
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lorg/threeten/bp/ZoneOffset;

    invoke-static {v0}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    goto :goto_0
.end method

.method public a(Lorg/threeten/bp/Instant;)Lorg/threeten/bp/ZoneOffset;
    .locals 8

    .prologue
    .line 280
    invoke-virtual {p1}, Lorg/threeten/bp/Instant;->a()J

    move-result-wide v2

    .line 283
    iget-object v0, p0, Lorg/threeten/bp/zone/StandardZoneRules;->f:[Lorg/threeten/bp/zone/ZoneOffsetTransitionRule;

    array-length v0, v0

    if-lez v0, :cond_2

    iget-object v0, p0, Lorg/threeten/bp/zone/StandardZoneRules;->c:[J

    iget-object v1, p0, Lorg/threeten/bp/zone/StandardZoneRules;->c:[J

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    aget-wide v0, v0, v1

    cmp-long v0, v2, v0

    if-lez v0, :cond_2

    .line 285
    iget-object v0, p0, Lorg/threeten/bp/zone/StandardZoneRules;->e:[Lorg/threeten/bp/ZoneOffset;

    iget-object v1, p0, Lorg/threeten/bp/zone/StandardZoneRules;->e:[Lorg/threeten/bp/ZoneOffset;

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    aget-object v0, v0, v1

    invoke-direct {p0, v2, v3, v0}, Lorg/threeten/bp/zone/StandardZoneRules;->a(JLorg/threeten/bp/ZoneOffset;)I

    move-result v0

    .line 286
    invoke-direct {p0, v0}, Lorg/threeten/bp/zone/StandardZoneRules;->a(I)[Lorg/threeten/bp/zone/ZoneOffsetTransition;

    move-result-object v4

    .line 287
    const/4 v1, 0x0

    .line 288
    const/4 v0, 0x0

    :goto_0
    array-length v5, v4

    if-ge v0, v5, :cond_1

    .line 289
    aget-object v1, v4, v0

    .line 290
    invoke-virtual {v1}, Lorg/threeten/bp/zone/ZoneOffsetTransition;->b()J

    move-result-wide v6

    cmp-long v5, v2, v6

    if-gez v5, :cond_0

    .line 291
    invoke-virtual {v1}, Lorg/threeten/bp/zone/ZoneOffsetTransition;->e()Lorg/threeten/bp/ZoneOffset;

    move-result-object v0

    .line 303
    :goto_1
    return-object v0

    .line 288
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 294
    :cond_1
    invoke-virtual {v1}, Lorg/threeten/bp/zone/ZoneOffsetTransition;->f()Lorg/threeten/bp/ZoneOffset;

    move-result-object v0

    goto :goto_1

    .line 298
    :cond_2
    iget-object v0, p0, Lorg/threeten/bp/zone/StandardZoneRules;->c:[J

    invoke-static {v0, v2, v3}, Ljava/util/Arrays;->binarySearch([JJ)I

    move-result v0

    .line 299
    if-gez v0, :cond_3

    .line 301
    neg-int v0, v0

    add-int/lit8 v0, v0, -0x2

    .line 303
    :cond_3
    iget-object v1, p0, Lorg/threeten/bp/zone/StandardZoneRules;->e:[Lorg/threeten/bp/ZoneOffset;

    add-int/lit8 v0, v0, 0x1

    aget-object v0, v1, v0

    goto :goto_1
.end method

.method a(Ljava/io/DataOutput;)V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 217
    iget-object v1, p0, Lorg/threeten/bp/zone/StandardZoneRules;->a:[J

    array-length v1, v1

    invoke-interface {p1, v1}, Ljava/io/DataOutput;->writeInt(I)V

    .line 218
    iget-object v2, p0, Lorg/threeten/bp/zone/StandardZoneRules;->a:[J

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    aget-wide v4, v2, v1

    .line 219
    invoke-static {v4, v5, p1}, Lorg/threeten/bp/zone/Ser;->a(JLjava/io/DataOutput;)V

    .line 218
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 221
    :cond_0
    iget-object v2, p0, Lorg/threeten/bp/zone/StandardZoneRules;->b:[Lorg/threeten/bp/ZoneOffset;

    array-length v3, v2

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 222
    invoke-static {v4, p1}, Lorg/threeten/bp/zone/Ser;->a(Lorg/threeten/bp/ZoneOffset;Ljava/io/DataOutput;)V

    .line 221
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 224
    :cond_1
    iget-object v1, p0, Lorg/threeten/bp/zone/StandardZoneRules;->c:[J

    array-length v1, v1

    invoke-interface {p1, v1}, Ljava/io/DataOutput;->writeInt(I)V

    .line 225
    iget-object v2, p0, Lorg/threeten/bp/zone/StandardZoneRules;->c:[J

    array-length v3, v2

    move v1, v0

    :goto_2
    if-ge v1, v3, :cond_2

    aget-wide v4, v2, v1

    .line 226
    invoke-static {v4, v5, p1}, Lorg/threeten/bp/zone/Ser;->a(JLjava/io/DataOutput;)V

    .line 225
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 228
    :cond_2
    iget-object v2, p0, Lorg/threeten/bp/zone/StandardZoneRules;->e:[Lorg/threeten/bp/ZoneOffset;

    array-length v3, v2

    move v1, v0

    :goto_3
    if-ge v1, v3, :cond_3

    aget-object v4, v2, v1

    .line 229
    invoke-static {v4, p1}, Lorg/threeten/bp/zone/Ser;->a(Lorg/threeten/bp/ZoneOffset;Ljava/io/DataOutput;)V

    .line 228
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 231
    :cond_3
    iget-object v1, p0, Lorg/threeten/bp/zone/StandardZoneRules;->f:[Lorg/threeten/bp/zone/ZoneOffsetTransitionRule;

    array-length v1, v1

    invoke-interface {p1, v1}, Ljava/io/DataOutput;->writeByte(I)V

    .line 232
    iget-object v1, p0, Lorg/threeten/bp/zone/StandardZoneRules;->f:[Lorg/threeten/bp/zone/ZoneOffsetTransitionRule;

    array-length v2, v1

    :goto_4
    if-ge v0, v2, :cond_4

    aget-object v3, v1, v0

    .line 233
    invoke-virtual {v3, p1}, Lorg/threeten/bp/zone/ZoneOffsetTransitionRule;->a(Ljava/io/DataOutput;)V

    .line 232
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 235
    :cond_4
    return-void
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 274
    iget-object v0, p0, Lorg/threeten/bp/zone/StandardZoneRules;->c:[J

    array-length v0, v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Lorg/threeten/bp/LocalDateTime;Lorg/threeten/bp/ZoneOffset;)Z
    .locals 1

    .prologue
    .line 412
    invoke-virtual {p0, p1}, Lorg/threeten/bp/zone/StandardZoneRules;->a(Lorg/threeten/bp/LocalDateTime;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public b(Lorg/threeten/bp/Instant;)Lorg/threeten/bp/ZoneOffset;
    .locals 3

    .prologue
    .line 442
    invoke-virtual {p1}, Lorg/threeten/bp/Instant;->a()J

    move-result-wide v0

    .line 443
    iget-object v2, p0, Lorg/threeten/bp/zone/StandardZoneRules;->a:[J

    invoke-static {v2, v0, v1}, Ljava/util/Arrays;->binarySearch([JJ)I

    move-result v0

    .line 444
    if-gez v0, :cond_0

    .line 446
    neg-int v0, v0

    add-int/lit8 v0, v0, -0x2

    .line 448
    :cond_0
    iget-object v1, p0, Lorg/threeten/bp/zone/StandardZoneRules;->b:[Lorg/threeten/bp/ZoneOffset;

    add-int/lit8 v0, v0, 0x1

    aget-object v0, v1, v0

    return-object v0
.end method

.method public b(Lorg/threeten/bp/LocalDateTime;)Lorg/threeten/bp/zone/ZoneOffsetTransition;
    .locals 2

    .prologue
    .line 328
    invoke-direct {p0, p1}, Lorg/threeten/bp/zone/StandardZoneRules;->c(Lorg/threeten/bp/LocalDateTime;)Ljava/lang/Object;

    move-result-object v0

    .line 329
    instance-of v1, v0, Lorg/threeten/bp/zone/ZoneOffsetTransition;

    if-eqz v1, :cond_0

    check-cast v0, Lorg/threeten/bp/zone/ZoneOffsetTransition;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public c(Lorg/threeten/bp/Instant;)Z
    .locals 2

    .prologue
    .line 460
    invoke-virtual {p0, p1}, Lorg/threeten/bp/zone/StandardZoneRules;->b(Lorg/threeten/bp/Instant;)Lorg/threeten/bp/ZoneOffset;

    move-result-object v0

    invoke-virtual {p0, p1}, Lorg/threeten/bp/zone/StandardZoneRules;->a(Lorg/threeten/bp/Instant;)Lorg/threeten/bp/ZoneOffset;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/threeten/bp/ZoneOffset;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 571
    if-ne p0, p1, :cond_1

    .line 585
    :cond_0
    :goto_0
    return v0

    .line 574
    :cond_1
    instance-of v2, p1, Lorg/threeten/bp/zone/StandardZoneRules;

    if-eqz v2, :cond_3

    .line 575
    check-cast p1, Lorg/threeten/bp/zone/StandardZoneRules;

    .line 576
    iget-object v2, p0, Lorg/threeten/bp/zone/StandardZoneRules;->a:[J

    iget-object v3, p1, Lorg/threeten/bp/zone/StandardZoneRules;->a:[J

    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([J[J)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lorg/threeten/bp/zone/StandardZoneRules;->b:[Lorg/threeten/bp/ZoneOffset;

    iget-object v3, p1, Lorg/threeten/bp/zone/StandardZoneRules;->b:[Lorg/threeten/bp/ZoneOffset;

    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lorg/threeten/bp/zone/StandardZoneRules;->c:[J

    iget-object v3, p1, Lorg/threeten/bp/zone/StandardZoneRules;->c:[J

    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([J[J)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lorg/threeten/bp/zone/StandardZoneRules;->e:[Lorg/threeten/bp/ZoneOffset;

    iget-object v3, p1, Lorg/threeten/bp/zone/StandardZoneRules;->e:[Lorg/threeten/bp/ZoneOffset;

    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lorg/threeten/bp/zone/StandardZoneRules;->f:[Lorg/threeten/bp/zone/ZoneOffsetTransitionRule;

    iget-object v3, p1, Lorg/threeten/bp/zone/StandardZoneRules;->f:[Lorg/threeten/bp/zone/ZoneOffsetTransitionRule;

    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_2
    move v0, v1

    goto :goto_0

    .line 582
    :cond_3
    instance-of v2, p1, Lorg/threeten/bp/zone/ZoneRules$Fixed;

    if-eqz v2, :cond_5

    .line 583
    invoke-virtual {p0}, Lorg/threeten/bp/zone/StandardZoneRules;->a()Z

    move-result v2

    if-eqz v2, :cond_4

    sget-object v2, Lorg/threeten/bp/Instant;->a:Lorg/threeten/bp/Instant;

    invoke-virtual {p0, v2}, Lorg/threeten/bp/zone/StandardZoneRules;->a(Lorg/threeten/bp/Instant;)Lorg/threeten/bp/ZoneOffset;

    move-result-object v2

    check-cast p1, Lorg/threeten/bp/zone/ZoneRules$Fixed;

    sget-object v3, Lorg/threeten/bp/Instant;->a:Lorg/threeten/bp/Instant;

    invoke-virtual {p1, v3}, Lorg/threeten/bp/zone/ZoneRules$Fixed;->a(Lorg/threeten/bp/Instant;)Lorg/threeten/bp/ZoneOffset;

    move-result-object v3

    invoke-virtual {v2, v3}, Lorg/threeten/bp/ZoneOffset;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_4
    move v0, v1

    goto :goto_0

    :cond_5
    move v0, v1

    .line 585
    goto :goto_0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 590
    iget-object v0, p0, Lorg/threeten/bp/zone/StandardZoneRules;->a:[J

    invoke-static {v0}, Ljava/util/Arrays;->hashCode([J)I

    move-result v0

    iget-object v1, p0, Lorg/threeten/bp/zone/StandardZoneRules;->b:[Lorg/threeten/bp/ZoneOffset;

    invoke-static {v1}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v1

    xor-int/2addr v0, v1

    iget-object v1, p0, Lorg/threeten/bp/zone/StandardZoneRules;->c:[J

    invoke-static {v1}, Ljava/util/Arrays;->hashCode([J)I

    move-result v1

    xor-int/2addr v0, v1

    iget-object v1, p0, Lorg/threeten/bp/zone/StandardZoneRules;->e:[Lorg/threeten/bp/ZoneOffset;

    invoke-static {v1}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v1

    xor-int/2addr v0, v1

    iget-object v1, p0, Lorg/threeten/bp/zone/StandardZoneRules;->f:[Lorg/threeten/bp/zone/ZoneOffsetTransitionRule;

    invoke-static {v1}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v1

    xor-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 605
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "StandardZoneRules[currentStandardOffset="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lorg/threeten/bp/zone/StandardZoneRules;->b:[Lorg/threeten/bp/ZoneOffset;

    iget-object v2, p0, Lorg/threeten/bp/zone/StandardZoneRules;->b:[Lorg/threeten/bp/ZoneOffset;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
