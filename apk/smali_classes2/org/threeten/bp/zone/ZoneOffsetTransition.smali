.class public final Lorg/threeten/bp/zone/ZoneOffsetTransition;
.super Ljava/lang/Object;
.source "ZoneOffsetTransition.java"

# interfaces
.implements Ljava/io/Serializable;
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/io/Serializable;",
        "Ljava/lang/Comparable",
        "<",
        "Lorg/threeten/bp/zone/ZoneOffsetTransition;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Lorg/threeten/bp/LocalDateTime;

.field private final b:Lorg/threeten/bp/ZoneOffset;

.field private final c:Lorg/threeten/bp/ZoneOffset;


# direct methods
.method constructor <init>(JLorg/threeten/bp/ZoneOffset;Lorg/threeten/bp/ZoneOffset;)V
    .locals 1

    .prologue
    .line 134
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 135
    const/4 v0, 0x0

    invoke-static {p1, p2, v0, p3}, Lorg/threeten/bp/LocalDateTime;->a(JILorg/threeten/bp/ZoneOffset;)Lorg/threeten/bp/LocalDateTime;

    move-result-object v0

    iput-object v0, p0, Lorg/threeten/bp/zone/ZoneOffsetTransition;->a:Lorg/threeten/bp/LocalDateTime;

    .line 136
    iput-object p3, p0, Lorg/threeten/bp/zone/ZoneOffsetTransition;->b:Lorg/threeten/bp/ZoneOffset;

    .line 137
    iput-object p4, p0, Lorg/threeten/bp/zone/ZoneOffsetTransition;->c:Lorg/threeten/bp/ZoneOffset;

    .line 138
    return-void
.end method

.method constructor <init>(Lorg/threeten/bp/LocalDateTime;Lorg/threeten/bp/ZoneOffset;Lorg/threeten/bp/ZoneOffset;)V
    .locals 0

    .prologue
    .line 121
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 122
    iput-object p1, p0, Lorg/threeten/bp/zone/ZoneOffsetTransition;->a:Lorg/threeten/bp/LocalDateTime;

    .line 123
    iput-object p2, p0, Lorg/threeten/bp/zone/ZoneOffsetTransition;->b:Lorg/threeten/bp/ZoneOffset;

    .line 124
    iput-object p3, p0, Lorg/threeten/bp/zone/ZoneOffsetTransition;->c:Lorg/threeten/bp/ZoneOffset;

    .line 125
    return-void
.end method

.method static a(Ljava/io/DataInput;)Lorg/threeten/bp/zone/ZoneOffsetTransition;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 170
    invoke-static {p0}, Lorg/threeten/bp/zone/Ser;->c(Ljava/io/DataInput;)J

    move-result-wide v0

    .line 171
    invoke-static {p0}, Lorg/threeten/bp/zone/Ser;->b(Ljava/io/DataInput;)Lorg/threeten/bp/ZoneOffset;

    move-result-object v2

    .line 172
    invoke-static {p0}, Lorg/threeten/bp/zone/Ser;->b(Ljava/io/DataInput;)Lorg/threeten/bp/ZoneOffset;

    move-result-object v3

    .line 173
    invoke-virtual {v2, v3}, Lorg/threeten/bp/ZoneOffset;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 174
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Offsets must not be equal"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 176
    :cond_0
    new-instance v4, Lorg/threeten/bp/zone/ZoneOffsetTransition;

    invoke-direct {v4, v0, v1, v2, v3}, Lorg/threeten/bp/zone/ZoneOffsetTransition;-><init>(JLorg/threeten/bp/ZoneOffset;Lorg/threeten/bp/ZoneOffset;)V

    return-object v4
.end method

.method private j()I
    .locals 2

    .prologue
    .line 276
    invoke-virtual {p0}, Lorg/threeten/bp/zone/ZoneOffsetTransition;->f()Lorg/threeten/bp/ZoneOffset;

    move-result-object v0

    invoke-virtual {v0}, Lorg/threeten/bp/ZoneOffset;->f()I

    move-result v0

    invoke-virtual {p0}, Lorg/threeten/bp/zone/ZoneOffsetTransition;->e()Lorg/threeten/bp/ZoneOffset;

    move-result-object v1

    invoke-virtual {v1}, Lorg/threeten/bp/ZoneOffset;->f()I

    move-result v1

    sub-int/2addr v0, v1

    return v0
.end method

.method private writeReplace()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 147
    new-instance v0, Lorg/threeten/bp/zone/Ser;

    const/4 v1, 0x2

    invoke-direct {v0, v1, p0}, Lorg/threeten/bp/zone/Ser;-><init>(BLjava/lang/Object;)V

    return-object v0
.end method


# virtual methods
.method public a(Lorg/threeten/bp/zone/ZoneOffsetTransition;)I
    .locals 2

    .prologue
    .line 345
    invoke-virtual {p0}, Lorg/threeten/bp/zone/ZoneOffsetTransition;->a()Lorg/threeten/bp/Instant;

    move-result-object v0

    invoke-virtual {p1}, Lorg/threeten/bp/zone/ZoneOffsetTransition;->a()Lorg/threeten/bp/Instant;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/threeten/bp/Instant;->a(Lorg/threeten/bp/Instant;)I

    move-result v0

    return v0
.end method

.method public a()Lorg/threeten/bp/Instant;
    .locals 2

    .prologue
    .line 192
    iget-object v0, p0, Lorg/threeten/bp/zone/ZoneOffsetTransition;->a:Lorg/threeten/bp/LocalDateTime;

    iget-object v1, p0, Lorg/threeten/bp/zone/ZoneOffsetTransition;->b:Lorg/threeten/bp/ZoneOffset;

    invoke-virtual {v0, v1}, Lorg/threeten/bp/LocalDateTime;->b(Lorg/threeten/bp/ZoneOffset;)Lorg/threeten/bp/Instant;

    move-result-object v0

    return-object v0
.end method

.method a(Ljava/io/DataOutput;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 157
    invoke-virtual {p0}, Lorg/threeten/bp/zone/ZoneOffsetTransition;->b()J

    move-result-wide v0

    invoke-static {v0, v1, p1}, Lorg/threeten/bp/zone/Ser;->a(JLjava/io/DataOutput;)V

    .line 158
    iget-object v0, p0, Lorg/threeten/bp/zone/ZoneOffsetTransition;->b:Lorg/threeten/bp/ZoneOffset;

    invoke-static {v0, p1}, Lorg/threeten/bp/zone/Ser;->a(Lorg/threeten/bp/ZoneOffset;Ljava/io/DataOutput;)V

    .line 159
    iget-object v0, p0, Lorg/threeten/bp/zone/ZoneOffsetTransition;->c:Lorg/threeten/bp/ZoneOffset;

    invoke-static {v0, p1}, Lorg/threeten/bp/zone/Ser;->a(Lorg/threeten/bp/ZoneOffset;Ljava/io/DataOutput;)V

    .line 160
    return-void
.end method

.method public b()J
    .locals 2

    .prologue
    .line 201
    iget-object v0, p0, Lorg/threeten/bp/zone/ZoneOffsetTransition;->a:Lorg/threeten/bp/LocalDateTime;

    iget-object v1, p0, Lorg/threeten/bp/zone/ZoneOffsetTransition;->b:Lorg/threeten/bp/ZoneOffset;

    invoke-virtual {v0, v1}, Lorg/threeten/bp/LocalDateTime;->c(Lorg/threeten/bp/ZoneOffset;)J

    move-result-wide v0

    return-wide v0
.end method

.method public c()Lorg/threeten/bp/LocalDateTime;
    .locals 1

    .prologue
    .line 218
    iget-object v0, p0, Lorg/threeten/bp/zone/ZoneOffsetTransition;->a:Lorg/threeten/bp/LocalDateTime;

    return-object v0
.end method

.method public synthetic compareTo(Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 66
    check-cast p1, Lorg/threeten/bp/zone/ZoneOffsetTransition;

    invoke-virtual {p0, p1}, Lorg/threeten/bp/zone/ZoneOffsetTransition;->a(Lorg/threeten/bp/zone/ZoneOffsetTransition;)I

    move-result v0

    return v0
.end method

.method public d()Lorg/threeten/bp/LocalDateTime;
    .locals 4

    .prologue
    .line 232
    iget-object v0, p0, Lorg/threeten/bp/zone/ZoneOffsetTransition;->a:Lorg/threeten/bp/LocalDateTime;

    invoke-direct {p0}, Lorg/threeten/bp/zone/ZoneOffsetTransition;->j()I

    move-result v1

    int-to-long v2, v1

    invoke-virtual {v0, v2, v3}, Lorg/threeten/bp/LocalDateTime;->d(J)Lorg/threeten/bp/LocalDateTime;

    move-result-object v0

    return-object v0
.end method

.method public e()Lorg/threeten/bp/ZoneOffset;
    .locals 1

    .prologue
    .line 243
    iget-object v0, p0, Lorg/threeten/bp/zone/ZoneOffsetTransition;->b:Lorg/threeten/bp/ZoneOffset;

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 359
    if-ne p1, p0, :cond_1

    .line 367
    :cond_0
    :goto_0
    return v0

    .line 362
    :cond_1
    instance-of v2, p1, Lorg/threeten/bp/zone/ZoneOffsetTransition;

    if-eqz v2, :cond_3

    .line 363
    check-cast p1, Lorg/threeten/bp/zone/ZoneOffsetTransition;

    .line 364
    iget-object v2, p0, Lorg/threeten/bp/zone/ZoneOffsetTransition;->a:Lorg/threeten/bp/LocalDateTime;

    iget-object v3, p1, Lorg/threeten/bp/zone/ZoneOffsetTransition;->a:Lorg/threeten/bp/LocalDateTime;

    invoke-virtual {v2, v3}, Lorg/threeten/bp/LocalDateTime;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lorg/threeten/bp/zone/ZoneOffsetTransition;->b:Lorg/threeten/bp/ZoneOffset;

    iget-object v3, p1, Lorg/threeten/bp/zone/ZoneOffsetTransition;->b:Lorg/threeten/bp/ZoneOffset;

    invoke-virtual {v2, v3}, Lorg/threeten/bp/ZoneOffset;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lorg/threeten/bp/zone/ZoneOffsetTransition;->c:Lorg/threeten/bp/ZoneOffset;

    iget-object v3, p1, Lorg/threeten/bp/zone/ZoneOffsetTransition;->c:Lorg/threeten/bp/ZoneOffset;

    invoke-virtual {v2, v3}, Lorg/threeten/bp/ZoneOffset;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_2
    move v0, v1

    goto :goto_0

    :cond_3
    move v0, v1

    .line 367
    goto :goto_0
.end method

.method public f()Lorg/threeten/bp/ZoneOffset;
    .locals 1

    .prologue
    .line 254
    iget-object v0, p0, Lorg/threeten/bp/zone/ZoneOffsetTransition;->c:Lorg/threeten/bp/ZoneOffset;

    return-object v0
.end method

.method public g()Lorg/threeten/bp/Duration;
    .locals 2

    .prologue
    .line 267
    invoke-direct {p0}, Lorg/threeten/bp/zone/ZoneOffsetTransition;->j()I

    move-result v0

    int-to-long v0, v0

    invoke-static {v0, v1}, Lorg/threeten/bp/Duration;->a(J)Lorg/threeten/bp/Duration;

    move-result-object v0

    return-object v0
.end method

.method public h()Z
    .locals 2

    .prologue
    .line 289
    invoke-virtual {p0}, Lorg/threeten/bp/zone/ZoneOffsetTransition;->f()Lorg/threeten/bp/ZoneOffset;

    move-result-object v0

    invoke-virtual {v0}, Lorg/threeten/bp/ZoneOffset;->f()I

    move-result v0

    invoke-virtual {p0}, Lorg/threeten/bp/zone/ZoneOffsetTransition;->e()Lorg/threeten/bp/ZoneOffset;

    move-result-object v1

    invoke-virtual {v1}, Lorg/threeten/bp/ZoneOffset;->f()I

    move-result v1

    if-le v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 377
    iget-object v0, p0, Lorg/threeten/bp/zone/ZoneOffsetTransition;->a:Lorg/threeten/bp/LocalDateTime;

    invoke-virtual {v0}, Lorg/threeten/bp/LocalDateTime;->hashCode()I

    move-result v0

    iget-object v1, p0, Lorg/threeten/bp/zone/ZoneOffsetTransition;->b:Lorg/threeten/bp/ZoneOffset;

    invoke-virtual {v1}, Lorg/threeten/bp/ZoneOffset;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    iget-object v1, p0, Lorg/threeten/bp/zone/ZoneOffsetTransition;->c:Lorg/threeten/bp/ZoneOffset;

    invoke-virtual {v1}, Lorg/threeten/bp/ZoneOffset;->hashCode()I

    move-result v1

    const/16 v2, 0x10

    invoke-static {v1, v2}, Ljava/lang/Integer;->rotateLeft(II)I

    move-result v1

    xor-int/2addr v0, v1

    return v0
.end method

.method i()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lorg/threeten/bp/ZoneOffset;",
            ">;"
        }
    .end annotation

    .prologue
    .line 327
    invoke-virtual {p0}, Lorg/threeten/bp/zone/ZoneOffsetTransition;->h()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 328
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    .line 330
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x2

    new-array v0, v0, [Lorg/threeten/bp/ZoneOffset;

    const/4 v1, 0x0

    invoke-virtual {p0}, Lorg/threeten/bp/zone/ZoneOffsetTransition;->e()Lorg/threeten/bp/ZoneOffset;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    invoke-virtual {p0}, Lorg/threeten/bp/zone/ZoneOffsetTransition;->f()Lorg/threeten/bp/ZoneOffset;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 388
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 389
    const-string v0, "Transition["

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lorg/threeten/bp/zone/ZoneOffsetTransition;->h()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "Gap"

    :goto_0
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " at "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lorg/threeten/bp/zone/ZoneOffsetTransition;->a:Lorg/threeten/bp/LocalDateTime;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lorg/threeten/bp/zone/ZoneOffsetTransition;->b:Lorg/threeten/bp/ZoneOffset;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " to "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lorg/threeten/bp/zone/ZoneOffsetTransition;->c:Lorg/threeten/bp/ZoneOffset;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v2, 0x5d

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 397
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 389
    :cond_0
    const-string v0, "Overlap"

    goto :goto_0
.end method
