.class public final Lorg/threeten/bp/zone/ZoneOffsetTransitionRule;
.super Ljava/lang/Object;
.source "ZoneOffsetTransitionRule.java"

# interfaces
.implements Ljava/io/Serializable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/threeten/bp/zone/ZoneOffsetTransitionRule$1;,
        Lorg/threeten/bp/zone/ZoneOffsetTransitionRule$TimeDefinition;
    }
.end annotation


# instance fields
.field private final a:Lorg/threeten/bp/Month;

.field private final b:B

.field private final c:Lorg/threeten/bp/DayOfWeek;

.field private final d:Lorg/threeten/bp/LocalTime;

.field private final e:Z

.field private final f:Lorg/threeten/bp/zone/ZoneOffsetTransitionRule$TimeDefinition;

.field private final g:Lorg/threeten/bp/ZoneOffset;

.field private final h:Lorg/threeten/bp/ZoneOffset;

.field private final i:Lorg/threeten/bp/ZoneOffset;


# direct methods
.method constructor <init>(Lorg/threeten/bp/Month;ILorg/threeten/bp/DayOfWeek;Lorg/threeten/bp/LocalTime;ZLorg/threeten/bp/zone/ZoneOffsetTransitionRule$TimeDefinition;Lorg/threeten/bp/ZoneOffset;Lorg/threeten/bp/ZoneOffset;Lorg/threeten/bp/ZoneOffset;)V
    .locals 1

    .prologue
    .line 189
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 190
    iput-object p1, p0, Lorg/threeten/bp/zone/ZoneOffsetTransitionRule;->a:Lorg/threeten/bp/Month;

    .line 191
    int-to-byte v0, p2

    iput-byte v0, p0, Lorg/threeten/bp/zone/ZoneOffsetTransitionRule;->b:B

    .line 192
    iput-object p3, p0, Lorg/threeten/bp/zone/ZoneOffsetTransitionRule;->c:Lorg/threeten/bp/DayOfWeek;

    .line 193
    iput-object p4, p0, Lorg/threeten/bp/zone/ZoneOffsetTransitionRule;->d:Lorg/threeten/bp/LocalTime;

    .line 194
    iput-boolean p5, p0, Lorg/threeten/bp/zone/ZoneOffsetTransitionRule;->e:Z

    .line 195
    iput-object p6, p0, Lorg/threeten/bp/zone/ZoneOffsetTransitionRule;->f:Lorg/threeten/bp/zone/ZoneOffsetTransitionRule$TimeDefinition;

    .line 196
    iput-object p7, p0, Lorg/threeten/bp/zone/ZoneOffsetTransitionRule;->g:Lorg/threeten/bp/ZoneOffset;

    .line 197
    iput-object p8, p0, Lorg/threeten/bp/zone/ZoneOffsetTransitionRule;->h:Lorg/threeten/bp/ZoneOffset;

    .line 198
    iput-object p9, p0, Lorg/threeten/bp/zone/ZoneOffsetTransitionRule;->i:Lorg/threeten/bp/ZoneOffset;

    .line 199
    return-void
.end method

.method static a(Ljava/io/DataInput;)Lorg/threeten/bp/zone/ZoneOffsetTransitionRule;
    .locals 13
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v12, 0x3

    const/4 v4, 0x0

    .line 258
    invoke-interface {p0}, Ljava/io/DataInput;->readInt()I

    move-result v3

    .line 259
    ushr-int/lit8 v0, v3, 0x1c

    invoke-static {v0}, Lorg/threeten/bp/Month;->a(I)Lorg/threeten/bp/Month;

    move-result-object v0

    .line 260
    const/high16 v1, 0xfc00000

    and-int/2addr v1, v3

    ushr-int/lit8 v1, v1, 0x16

    add-int/lit8 v1, v1, -0x20

    .line 261
    const/high16 v2, 0x380000

    and-int/2addr v2, v3

    ushr-int/lit8 v2, v2, 0x13

    .line 262
    if-nez v2, :cond_1

    const/4 v2, 0x0

    .line 263
    :goto_0
    const v5, 0x7c000

    and-int/2addr v5, v3

    ushr-int/lit8 v9, v5, 0xe

    .line 264
    invoke-static {}, Lorg/threeten/bp/zone/ZoneOffsetTransitionRule$TimeDefinition;->values()[Lorg/threeten/bp/zone/ZoneOffsetTransitionRule$TimeDefinition;

    move-result-object v5

    and-int/lit16 v6, v3, 0x3000

    ushr-int/lit8 v6, v6, 0xc

    aget-object v5, v5, v6

    .line 265
    and-int/lit16 v6, v3, 0xff0

    ushr-int/lit8 v6, v6, 0x4

    .line 266
    and-int/lit8 v7, v3, 0xc

    ushr-int/lit8 v7, v7, 0x2

    .line 267
    and-int/lit8 v8, v3, 0x3

    .line 268
    const/16 v3, 0x1f

    if-ne v9, v3, :cond_2

    invoke-interface {p0}, Ljava/io/DataInput;->readInt()I

    move-result v3

    int-to-long v10, v3

    invoke-static {v10, v11}, Lorg/threeten/bp/LocalTime;->a(J)Lorg/threeten/bp/LocalTime;

    move-result-object v3

    .line 269
    :goto_1
    const/16 v10, 0xff

    if-ne v6, v10, :cond_3

    invoke-interface {p0}, Ljava/io/DataInput;->readInt()I

    move-result v6

    invoke-static {v6}, Lorg/threeten/bp/ZoneOffset;->a(I)Lorg/threeten/bp/ZoneOffset;

    move-result-object v6

    .line 270
    :goto_2
    if-ne v7, v12, :cond_4

    invoke-interface {p0}, Ljava/io/DataInput;->readInt()I

    move-result v7

    invoke-static {v7}, Lorg/threeten/bp/ZoneOffset;->a(I)Lorg/threeten/bp/ZoneOffset;

    move-result-object v7

    .line 271
    :goto_3
    if-ne v8, v12, :cond_5

    invoke-interface {p0}, Ljava/io/DataInput;->readInt()I

    move-result v8

    invoke-static {v8}, Lorg/threeten/bp/ZoneOffset;->a(I)Lorg/threeten/bp/ZoneOffset;

    move-result-object v8

    .line 272
    :goto_4
    const/16 v10, 0x18

    if-ne v9, v10, :cond_0

    const/4 v4, 0x1

    :cond_0
    invoke-static/range {v0 .. v8}, Lorg/threeten/bp/zone/ZoneOffsetTransitionRule;->a(Lorg/threeten/bp/Month;ILorg/threeten/bp/DayOfWeek;Lorg/threeten/bp/LocalTime;ZLorg/threeten/bp/zone/ZoneOffsetTransitionRule$TimeDefinition;Lorg/threeten/bp/ZoneOffset;Lorg/threeten/bp/ZoneOffset;Lorg/threeten/bp/ZoneOffset;)Lorg/threeten/bp/zone/ZoneOffsetTransitionRule;

    move-result-object v0

    return-object v0

    .line 262
    :cond_1
    invoke-static {v2}, Lorg/threeten/bp/DayOfWeek;->a(I)Lorg/threeten/bp/DayOfWeek;

    move-result-object v2

    goto :goto_0

    .line 268
    :cond_2
    rem-int/lit8 v3, v9, 0x18

    invoke-static {v3, v4}, Lorg/threeten/bp/LocalTime;->a(II)Lorg/threeten/bp/LocalTime;

    move-result-object v3

    goto :goto_1

    .line 269
    :cond_3
    add-int/lit8 v6, v6, -0x80

    mul-int/lit16 v6, v6, 0x384

    invoke-static {v6}, Lorg/threeten/bp/ZoneOffset;->a(I)Lorg/threeten/bp/ZoneOffset;

    move-result-object v6

    goto :goto_2

    .line 270
    :cond_4
    invoke-virtual {v6}, Lorg/threeten/bp/ZoneOffset;->f()I

    move-result v10

    mul-int/lit16 v7, v7, 0x708

    add-int/2addr v7, v10

    invoke-static {v7}, Lorg/threeten/bp/ZoneOffset;->a(I)Lorg/threeten/bp/ZoneOffset;

    move-result-object v7

    goto :goto_3

    .line 271
    :cond_5
    invoke-virtual {v6}, Lorg/threeten/bp/ZoneOffset;->f()I

    move-result v10

    mul-int/lit16 v8, v8, 0x708

    add-int/2addr v8, v10

    invoke-static {v8}, Lorg/threeten/bp/ZoneOffset;->a(I)Lorg/threeten/bp/ZoneOffset;

    move-result-object v8

    goto :goto_4
.end method

.method public static a(Lorg/threeten/bp/Month;ILorg/threeten/bp/DayOfWeek;Lorg/threeten/bp/LocalTime;ZLorg/threeten/bp/zone/ZoneOffsetTransitionRule$TimeDefinition;Lorg/threeten/bp/ZoneOffset;Lorg/threeten/bp/ZoneOffset;Lorg/threeten/bp/ZoneOffset;)Lorg/threeten/bp/zone/ZoneOffsetTransitionRule;
    .locals 11

    .prologue
    .line 148
    const-string v1, "month"

    invoke-static {p0, v1}, Lorg/threeten/bp/jdk8/Jdk8Methods;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 149
    const-string v1, "time"

    invoke-static {p3, v1}, Lorg/threeten/bp/jdk8/Jdk8Methods;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 150
    const-string v1, "timeDefnition"

    move-object/from16 v0, p5

    invoke-static {v0, v1}, Lorg/threeten/bp/jdk8/Jdk8Methods;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 151
    const-string v1, "standardOffset"

    move-object/from16 v0, p6

    invoke-static {v0, v1}, Lorg/threeten/bp/jdk8/Jdk8Methods;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 152
    const-string v1, "offsetBefore"

    move-object/from16 v0, p7

    invoke-static {v0, v1}, Lorg/threeten/bp/jdk8/Jdk8Methods;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 153
    const-string v1, "offsetAfter"

    move-object/from16 v0, p8

    invoke-static {v0, v1}, Lorg/threeten/bp/jdk8/Jdk8Methods;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 154
    const/16 v1, -0x1c

    if-lt p1, v1, :cond_0

    const/16 v1, 0x1f

    if-gt p1, v1, :cond_0

    if-nez p1, :cond_1

    .line 155
    :cond_0
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "Day of month indicator must be between -28 and 31 inclusive excluding zero"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 157
    :cond_1
    if-eqz p4, :cond_2

    sget-object v1, Lorg/threeten/bp/LocalTime;->c:Lorg/threeten/bp/LocalTime;

    invoke-virtual {p3, v1}, Lorg/threeten/bp/LocalTime;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 158
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "Time must be midnight when end of day flag is true"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 160
    :cond_2
    new-instance v1, Lorg/threeten/bp/zone/ZoneOffsetTransitionRule;

    move-object v2, p0

    move v3, p1

    move-object v4, p2

    move-object v5, p3

    move v6, p4

    move-object/from16 v7, p5

    move-object/from16 v8, p6

    move-object/from16 v9, p7

    move-object/from16 v10, p8

    invoke-direct/range {v1 .. v10}, Lorg/threeten/bp/zone/ZoneOffsetTransitionRule;-><init>(Lorg/threeten/bp/Month;ILorg/threeten/bp/DayOfWeek;Lorg/threeten/bp/LocalTime;ZLorg/threeten/bp/zone/ZoneOffsetTransitionRule$TimeDefinition;Lorg/threeten/bp/ZoneOffset;Lorg/threeten/bp/ZoneOffset;Lorg/threeten/bp/ZoneOffset;)V

    return-object v1
.end method

.method private writeReplace()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 208
    new-instance v0, Lorg/threeten/bp/zone/Ser;

    const/4 v1, 0x3

    invoke-direct {v0, v1, p0}, Lorg/threeten/bp/zone/Ser;-><init>(BLjava/lang/Object;)V

    return-object v0
.end method


# virtual methods
.method public a(I)Lorg/threeten/bp/zone/ZoneOffsetTransition;
    .locals 6

    .prologue
    .line 401
    iget-byte v0, p0, Lorg/threeten/bp/zone/ZoneOffsetTransitionRule;->b:B

    if-gez v0, :cond_2

    .line 402
    iget-object v0, p0, Lorg/threeten/bp/zone/ZoneOffsetTransitionRule;->a:Lorg/threeten/bp/Month;

    iget-object v1, p0, Lorg/threeten/bp/zone/ZoneOffsetTransitionRule;->a:Lorg/threeten/bp/Month;

    sget-object v2, Lorg/threeten/bp/chrono/IsoChronology;->b:Lorg/threeten/bp/chrono/IsoChronology;

    int-to-long v4, p1

    invoke-virtual {v2, v4, v5}, Lorg/threeten/bp/chrono/IsoChronology;->a(J)Z

    move-result v2

    invoke-virtual {v1, v2}, Lorg/threeten/bp/Month;->a(Z)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    iget-byte v2, p0, Lorg/threeten/bp/zone/ZoneOffsetTransitionRule;->b:B

    add-int/2addr v1, v2

    invoke-static {p1, v0, v1}, Lorg/threeten/bp/LocalDate;->a(ILorg/threeten/bp/Month;I)Lorg/threeten/bp/LocalDate;

    move-result-object v0

    .line 403
    iget-object v1, p0, Lorg/threeten/bp/zone/ZoneOffsetTransitionRule;->c:Lorg/threeten/bp/DayOfWeek;

    if-eqz v1, :cond_0

    .line 404
    iget-object v1, p0, Lorg/threeten/bp/zone/ZoneOffsetTransitionRule;->c:Lorg/threeten/bp/DayOfWeek;

    invoke-static {v1}, Lorg/threeten/bp/temporal/TemporalAdjusters;->b(Lorg/threeten/bp/DayOfWeek;)Lorg/threeten/bp/temporal/TemporalAdjuster;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/threeten/bp/LocalDate;->a(Lorg/threeten/bp/temporal/TemporalAdjuster;)Lorg/threeten/bp/LocalDate;

    move-result-object v0

    .line 412
    :cond_0
    :goto_0
    iget-boolean v1, p0, Lorg/threeten/bp/zone/ZoneOffsetTransitionRule;->e:Z

    if-eqz v1, :cond_1

    .line 413
    const-wide/16 v2, 0x1

    invoke-virtual {v0, v2, v3}, Lorg/threeten/bp/LocalDate;->e(J)Lorg/threeten/bp/LocalDate;

    move-result-object v0

    .line 415
    :cond_1
    iget-object v1, p0, Lorg/threeten/bp/zone/ZoneOffsetTransitionRule;->d:Lorg/threeten/bp/LocalTime;

    invoke-static {v0, v1}, Lorg/threeten/bp/LocalDateTime;->a(Lorg/threeten/bp/LocalDate;Lorg/threeten/bp/LocalTime;)Lorg/threeten/bp/LocalDateTime;

    move-result-object v0

    .line 416
    iget-object v1, p0, Lorg/threeten/bp/zone/ZoneOffsetTransitionRule;->f:Lorg/threeten/bp/zone/ZoneOffsetTransitionRule$TimeDefinition;

    iget-object v2, p0, Lorg/threeten/bp/zone/ZoneOffsetTransitionRule;->g:Lorg/threeten/bp/ZoneOffset;

    iget-object v3, p0, Lorg/threeten/bp/zone/ZoneOffsetTransitionRule;->h:Lorg/threeten/bp/ZoneOffset;

    invoke-virtual {v1, v0, v2, v3}, Lorg/threeten/bp/zone/ZoneOffsetTransitionRule$TimeDefinition;->a(Lorg/threeten/bp/LocalDateTime;Lorg/threeten/bp/ZoneOffset;Lorg/threeten/bp/ZoneOffset;)Lorg/threeten/bp/LocalDateTime;

    move-result-object v0

    .line 417
    new-instance v1, Lorg/threeten/bp/zone/ZoneOffsetTransition;

    iget-object v2, p0, Lorg/threeten/bp/zone/ZoneOffsetTransitionRule;->h:Lorg/threeten/bp/ZoneOffset;

    iget-object v3, p0, Lorg/threeten/bp/zone/ZoneOffsetTransitionRule;->i:Lorg/threeten/bp/ZoneOffset;

    invoke-direct {v1, v0, v2, v3}, Lorg/threeten/bp/zone/ZoneOffsetTransition;-><init>(Lorg/threeten/bp/LocalDateTime;Lorg/threeten/bp/ZoneOffset;Lorg/threeten/bp/ZoneOffset;)V

    return-object v1

    .line 407
    :cond_2
    iget-object v0, p0, Lorg/threeten/bp/zone/ZoneOffsetTransitionRule;->a:Lorg/threeten/bp/Month;

    iget-byte v1, p0, Lorg/threeten/bp/zone/ZoneOffsetTransitionRule;->b:B

    invoke-static {p1, v0, v1}, Lorg/threeten/bp/LocalDate;->a(ILorg/threeten/bp/Month;I)Lorg/threeten/bp/LocalDate;

    move-result-object v0

    .line 408
    iget-object v1, p0, Lorg/threeten/bp/zone/ZoneOffsetTransitionRule;->c:Lorg/threeten/bp/DayOfWeek;

    if-eqz v1, :cond_0

    .line 409
    iget-object v1, p0, Lorg/threeten/bp/zone/ZoneOffsetTransitionRule;->c:Lorg/threeten/bp/DayOfWeek;

    invoke-static {v1}, Lorg/threeten/bp/temporal/TemporalAdjusters;->a(Lorg/threeten/bp/DayOfWeek;)Lorg/threeten/bp/temporal/TemporalAdjuster;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/threeten/bp/LocalDate;->a(Lorg/threeten/bp/temporal/TemporalAdjuster;)Lorg/threeten/bp/LocalDate;

    move-result-object v0

    goto :goto_0
.end method

.method a(Ljava/io/DataOutput;)V
    .locals 13
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/16 v12, 0xe10

    const/16 v11, 0x708

    const/16 v8, 0xff

    const/16 v2, 0x1f

    const/4 v4, 0x3

    .line 218
    iget-boolean v0, p0, Lorg/threeten/bp/zone/ZoneOffsetTransitionRule;->e:Z

    if-eqz v0, :cond_6

    const v0, 0x15180

    .line 219
    :goto_0
    iget-object v1, p0, Lorg/threeten/bp/zone/ZoneOffsetTransitionRule;->g:Lorg/threeten/bp/ZoneOffset;

    invoke-virtual {v1}, Lorg/threeten/bp/ZoneOffset;->f()I

    move-result v9

    .line 220
    iget-object v1, p0, Lorg/threeten/bp/zone/ZoneOffsetTransitionRule;->h:Lorg/threeten/bp/ZoneOffset;

    invoke-virtual {v1}, Lorg/threeten/bp/ZoneOffset;->f()I

    move-result v1

    sub-int v5, v1, v9

    .line 221
    iget-object v1, p0, Lorg/threeten/bp/zone/ZoneOffsetTransitionRule;->i:Lorg/threeten/bp/ZoneOffset;

    invoke-virtual {v1}, Lorg/threeten/bp/ZoneOffset;->f()I

    move-result v1

    sub-int v10, v1, v9

    .line 222
    rem-int/lit16 v1, v0, 0xe10

    if-nez v1, :cond_8

    iget-boolean v1, p0, Lorg/threeten/bp/zone/ZoneOffsetTransitionRule;->e:Z

    if-eqz v1, :cond_7

    const/16 v1, 0x18

    .line 223
    :goto_1
    rem-int/lit16 v3, v9, 0x384

    if-nez v3, :cond_9

    div-int/lit16 v3, v9, 0x384

    add-int/lit16 v3, v3, 0x80

    move v7, v3

    .line 224
    :goto_2
    if-eqz v5, :cond_0

    if-eq v5, v11, :cond_0

    if-ne v5, v12, :cond_a

    :cond_0
    div-int/lit16 v3, v5, 0x708

    move v6, v3

    .line 225
    :goto_3
    if-eqz v10, :cond_1

    if-eq v10, v11, :cond_1

    if-ne v10, v12, :cond_b

    :cond_1
    div-int/lit16 v3, v10, 0x708

    .line 226
    :goto_4
    iget-object v5, p0, Lorg/threeten/bp/zone/ZoneOffsetTransitionRule;->c:Lorg/threeten/bp/DayOfWeek;

    if-nez v5, :cond_c

    const/4 v5, 0x0

    .line 227
    :goto_5
    iget-object v10, p0, Lorg/threeten/bp/zone/ZoneOffsetTransitionRule;->a:Lorg/threeten/bp/Month;

    invoke-virtual {v10}, Lorg/threeten/bp/Month;->a()I

    move-result v10

    shl-int/lit8 v10, v10, 0x1c

    iget-byte v11, p0, Lorg/threeten/bp/zone/ZoneOffsetTransitionRule;->b:B

    add-int/lit8 v11, v11, 0x20

    shl-int/lit8 v11, v11, 0x16

    add-int/2addr v10, v11

    shl-int/lit8 v5, v5, 0x13

    add-int/2addr v5, v10

    shl-int/lit8 v10, v1, 0xe

    add-int/2addr v5, v10

    iget-object v10, p0, Lorg/threeten/bp/zone/ZoneOffsetTransitionRule;->f:Lorg/threeten/bp/zone/ZoneOffsetTransitionRule$TimeDefinition;

    invoke-virtual {v10}, Lorg/threeten/bp/zone/ZoneOffsetTransitionRule$TimeDefinition;->ordinal()I

    move-result v10

    shl-int/lit8 v10, v10, 0xc

    add-int/2addr v5, v10

    shl-int/lit8 v10, v7, 0x4

    add-int/2addr v5, v10

    shl-int/lit8 v10, v6, 0x2

    add-int/2addr v5, v10

    add-int/2addr v5, v3

    .line 235
    invoke-interface {p1, v5}, Ljava/io/DataOutput;->writeInt(I)V

    .line 236
    if-ne v1, v2, :cond_2

    .line 237
    invoke-interface {p1, v0}, Ljava/io/DataOutput;->writeInt(I)V

    .line 239
    :cond_2
    if-ne v7, v8, :cond_3

    .line 240
    invoke-interface {p1, v9}, Ljava/io/DataOutput;->writeInt(I)V

    .line 242
    :cond_3
    if-ne v6, v4, :cond_4

    .line 243
    iget-object v0, p0, Lorg/threeten/bp/zone/ZoneOffsetTransitionRule;->h:Lorg/threeten/bp/ZoneOffset;

    invoke-virtual {v0}, Lorg/threeten/bp/ZoneOffset;->f()I

    move-result v0

    invoke-interface {p1, v0}, Ljava/io/DataOutput;->writeInt(I)V

    .line 245
    :cond_4
    if-ne v3, v4, :cond_5

    .line 246
    iget-object v0, p0, Lorg/threeten/bp/zone/ZoneOffsetTransitionRule;->i:Lorg/threeten/bp/ZoneOffset;

    invoke-virtual {v0}, Lorg/threeten/bp/ZoneOffset;->f()I

    move-result v0

    invoke-interface {p1, v0}, Ljava/io/DataOutput;->writeInt(I)V

    .line 248
    :cond_5
    return-void

    .line 218
    :cond_6
    iget-object v0, p0, Lorg/threeten/bp/zone/ZoneOffsetTransitionRule;->d:Lorg/threeten/bp/LocalTime;

    invoke-virtual {v0}, Lorg/threeten/bp/LocalTime;->e()I

    move-result v0

    goto/16 :goto_0

    .line 222
    :cond_7
    iget-object v1, p0, Lorg/threeten/bp/zone/ZoneOffsetTransitionRule;->d:Lorg/threeten/bp/LocalTime;

    invoke-virtual {v1}, Lorg/threeten/bp/LocalTime;->a()I

    move-result v1

    goto :goto_1

    :cond_8
    move v1, v2

    goto :goto_1

    :cond_9
    move v7, v8

    .line 223
    goto :goto_2

    :cond_a
    move v6, v4

    .line 224
    goto :goto_3

    :cond_b
    move v3, v4

    .line 225
    goto :goto_4

    .line 226
    :cond_c
    iget-object v5, p0, Lorg/threeten/bp/zone/ZoneOffsetTransitionRule;->c:Lorg/threeten/bp/DayOfWeek;

    invoke-virtual {v5}, Lorg/threeten/bp/DayOfWeek;->a()I

    move-result v5

    goto :goto_5
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 431
    if-ne p1, p0, :cond_1

    .line 444
    :cond_0
    :goto_0
    return v0

    .line 434
    :cond_1
    instance-of v2, p1, Lorg/threeten/bp/zone/ZoneOffsetTransitionRule;

    if-eqz v2, :cond_3

    .line 435
    check-cast p1, Lorg/threeten/bp/zone/ZoneOffsetTransitionRule;

    .line 436
    iget-object v2, p0, Lorg/threeten/bp/zone/ZoneOffsetTransitionRule;->a:Lorg/threeten/bp/Month;

    iget-object v3, p1, Lorg/threeten/bp/zone/ZoneOffsetTransitionRule;->a:Lorg/threeten/bp/Month;

    if-ne v2, v3, :cond_2

    iget-byte v2, p0, Lorg/threeten/bp/zone/ZoneOffsetTransitionRule;->b:B

    iget-byte v3, p1, Lorg/threeten/bp/zone/ZoneOffsetTransitionRule;->b:B

    if-ne v2, v3, :cond_2

    iget-object v2, p0, Lorg/threeten/bp/zone/ZoneOffsetTransitionRule;->c:Lorg/threeten/bp/DayOfWeek;

    iget-object v3, p1, Lorg/threeten/bp/zone/ZoneOffsetTransitionRule;->c:Lorg/threeten/bp/DayOfWeek;

    if-ne v2, v3, :cond_2

    iget-object v2, p0, Lorg/threeten/bp/zone/ZoneOffsetTransitionRule;->f:Lorg/threeten/bp/zone/ZoneOffsetTransitionRule$TimeDefinition;

    iget-object v3, p1, Lorg/threeten/bp/zone/ZoneOffsetTransitionRule;->f:Lorg/threeten/bp/zone/ZoneOffsetTransitionRule$TimeDefinition;

    if-ne v2, v3, :cond_2

    iget-object v2, p0, Lorg/threeten/bp/zone/ZoneOffsetTransitionRule;->d:Lorg/threeten/bp/LocalTime;

    iget-object v3, p1, Lorg/threeten/bp/zone/ZoneOffsetTransitionRule;->d:Lorg/threeten/bp/LocalTime;

    invoke-virtual {v2, v3}, Lorg/threeten/bp/LocalTime;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-boolean v2, p0, Lorg/threeten/bp/zone/ZoneOffsetTransitionRule;->e:Z

    iget-boolean v3, p1, Lorg/threeten/bp/zone/ZoneOffsetTransitionRule;->e:Z

    if-ne v2, v3, :cond_2

    iget-object v2, p0, Lorg/threeten/bp/zone/ZoneOffsetTransitionRule;->g:Lorg/threeten/bp/ZoneOffset;

    iget-object v3, p1, Lorg/threeten/bp/zone/ZoneOffsetTransitionRule;->g:Lorg/threeten/bp/ZoneOffset;

    invoke-virtual {v2, v3}, Lorg/threeten/bp/ZoneOffset;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lorg/threeten/bp/zone/ZoneOffsetTransitionRule;->h:Lorg/threeten/bp/ZoneOffset;

    iget-object v3, p1, Lorg/threeten/bp/zone/ZoneOffsetTransitionRule;->h:Lorg/threeten/bp/ZoneOffset;

    invoke-virtual {v2, v3}, Lorg/threeten/bp/ZoneOffset;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lorg/threeten/bp/zone/ZoneOffsetTransitionRule;->i:Lorg/threeten/bp/ZoneOffset;

    iget-object v3, p1, Lorg/threeten/bp/zone/ZoneOffsetTransitionRule;->i:Lorg/threeten/bp/ZoneOffset;

    invoke-virtual {v2, v3}, Lorg/threeten/bp/ZoneOffset;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_2
    move v0, v1

    goto :goto_0

    :cond_3
    move v0, v1

    .line 444
    goto :goto_0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 454
    iget-object v0, p0, Lorg/threeten/bp/zone/ZoneOffsetTransitionRule;->d:Lorg/threeten/bp/LocalTime;

    invoke-virtual {v0}, Lorg/threeten/bp/LocalTime;->e()I

    move-result v1

    iget-boolean v0, p0, Lorg/threeten/bp/zone/ZoneOffsetTransitionRule;->e:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    add-int/2addr v0, v1

    shl-int/lit8 v0, v0, 0xf

    iget-object v1, p0, Lorg/threeten/bp/zone/ZoneOffsetTransitionRule;->a:Lorg/threeten/bp/Month;

    invoke-virtual {v1}, Lorg/threeten/bp/Month;->ordinal()I

    move-result v1

    shl-int/lit8 v1, v1, 0xb

    add-int/2addr v0, v1

    iget-byte v1, p0, Lorg/threeten/bp/zone/ZoneOffsetTransitionRule;->b:B

    add-int/lit8 v1, v1, 0x20

    shl-int/lit8 v1, v1, 0x5

    add-int/2addr v1, v0

    iget-object v0, p0, Lorg/threeten/bp/zone/ZoneOffsetTransitionRule;->c:Lorg/threeten/bp/DayOfWeek;

    if-nez v0, :cond_1

    const/4 v0, 0x7

    :goto_1
    shl-int/lit8 v0, v0, 0x2

    add-int/2addr v0, v1

    iget-object v1, p0, Lorg/threeten/bp/zone/ZoneOffsetTransitionRule;->f:Lorg/threeten/bp/zone/ZoneOffsetTransitionRule$TimeDefinition;

    invoke-virtual {v1}, Lorg/threeten/bp/zone/ZoneOffsetTransitionRule$TimeDefinition;->ordinal()I

    move-result v1

    add-int/2addr v0, v1

    .line 457
    iget-object v1, p0, Lorg/threeten/bp/zone/ZoneOffsetTransitionRule;->g:Lorg/threeten/bp/ZoneOffset;

    invoke-virtual {v1}, Lorg/threeten/bp/ZoneOffset;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    iget-object v1, p0, Lorg/threeten/bp/zone/ZoneOffsetTransitionRule;->h:Lorg/threeten/bp/ZoneOffset;

    invoke-virtual {v1}, Lorg/threeten/bp/ZoneOffset;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    iget-object v1, p0, Lorg/threeten/bp/zone/ZoneOffsetTransitionRule;->i:Lorg/threeten/bp/ZoneOffset;

    invoke-virtual {v1}, Lorg/threeten/bp/ZoneOffset;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    return v0

    .line 454
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lorg/threeten/bp/zone/ZoneOffsetTransitionRule;->c:Lorg/threeten/bp/DayOfWeek;

    invoke-virtual {v0}, Lorg/threeten/bp/DayOfWeek;->ordinal()I

    move-result v0

    goto :goto_1
.end method

.method public toString()Ljava/lang/String;
    .locals 5

    .prologue
    const/16 v4, 0x20

    .line 469
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 470
    const-string v0, "TransitionRule["

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v0, p0, Lorg/threeten/bp/zone/ZoneOffsetTransitionRule;->h:Lorg/threeten/bp/ZoneOffset;

    iget-object v3, p0, Lorg/threeten/bp/zone/ZoneOffsetTransitionRule;->i:Lorg/threeten/bp/ZoneOffset;

    invoke-virtual {v0, v3}, Lorg/threeten/bp/ZoneOffset;->a(Lorg/threeten/bp/ZoneOffset;)I

    move-result v0

    if-lez v0, :cond_0

    const-string v0, "Gap "

    :goto_0
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lorg/threeten/bp/zone/ZoneOffsetTransitionRule;->h:Lorg/threeten/bp/ZoneOffset;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " to "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lorg/threeten/bp/zone/ZoneOffsetTransitionRule;->i:Lorg/threeten/bp/ZoneOffset;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 473
    iget-object v0, p0, Lorg/threeten/bp/zone/ZoneOffsetTransitionRule;->c:Lorg/threeten/bp/DayOfWeek;

    if-eqz v0, :cond_3

    .line 474
    iget-byte v0, p0, Lorg/threeten/bp/zone/ZoneOffsetTransitionRule;->b:B

    const/4 v2, -0x1

    if-ne v0, v2, :cond_1

    .line 475
    iget-object v0, p0, Lorg/threeten/bp/zone/ZoneOffsetTransitionRule;->c:Lorg/threeten/bp/DayOfWeek;

    invoke-virtual {v0}, Lorg/threeten/bp/DayOfWeek;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " on or before last day of "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lorg/threeten/bp/zone/ZoneOffsetTransitionRule;->a:Lorg/threeten/bp/Month;

    invoke-virtual {v2}, Lorg/threeten/bp/Month;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 484
    :goto_1
    const-string v0, " at "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v0, p0, Lorg/threeten/bp/zone/ZoneOffsetTransitionRule;->e:Z

    if-eqz v0, :cond_4

    const-string v0, "24:00"

    :goto_2
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lorg/threeten/bp/zone/ZoneOffsetTransitionRule;->f:Lorg/threeten/bp/zone/ZoneOffsetTransitionRule$TimeDefinition;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", standard offset "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lorg/threeten/bp/zone/ZoneOffsetTransitionRule;->g:Lorg/threeten/bp/ZoneOffset;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v2, 0x5d

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 488
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 470
    :cond_0
    const-string v0, "Overlap "

    goto :goto_0

    .line 476
    :cond_1
    iget-byte v0, p0, Lorg/threeten/bp/zone/ZoneOffsetTransitionRule;->b:B

    if-gez v0, :cond_2

    .line 477
    iget-object v0, p0, Lorg/threeten/bp/zone/ZoneOffsetTransitionRule;->c:Lorg/threeten/bp/DayOfWeek;

    invoke-virtual {v0}, Lorg/threeten/bp/DayOfWeek;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " on or before last day minus "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-byte v2, p0, Lorg/threeten/bp/zone/ZoneOffsetTransitionRule;->b:B

    neg-int v2, v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " of "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lorg/threeten/bp/zone/ZoneOffsetTransitionRule;->a:Lorg/threeten/bp/Month;

    invoke-virtual {v2}, Lorg/threeten/bp/Month;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 479
    :cond_2
    iget-object v0, p0, Lorg/threeten/bp/zone/ZoneOffsetTransitionRule;->c:Lorg/threeten/bp/DayOfWeek;

    invoke-virtual {v0}, Lorg/threeten/bp/DayOfWeek;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " on or after "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lorg/threeten/bp/zone/ZoneOffsetTransitionRule;->a:Lorg/threeten/bp/Month;

    invoke-virtual {v2}, Lorg/threeten/bp/Month;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-byte v2, p0, Lorg/threeten/bp/zone/ZoneOffsetTransitionRule;->b:B

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    goto/16 :goto_1

    .line 482
    :cond_3
    iget-object v0, p0, Lorg/threeten/bp/zone/ZoneOffsetTransitionRule;->a:Lorg/threeten/bp/Month;

    invoke-virtual {v0}, Lorg/threeten/bp/Month;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-byte v2, p0, Lorg/threeten/bp/zone/ZoneOffsetTransitionRule;->b:B

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    goto/16 :goto_1

    .line 484
    :cond_4
    iget-object v0, p0, Lorg/threeten/bp/zone/ZoneOffsetTransitionRule;->d:Lorg/threeten/bp/LocalTime;

    invoke-virtual {v0}, Lorg/threeten/bp/LocalTime;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_2
.end method
