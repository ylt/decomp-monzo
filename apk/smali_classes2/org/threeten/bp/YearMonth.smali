.class public final Lorg/threeten/bp/YearMonth;
.super Lorg/threeten/bp/jdk8/DefaultInterfaceTemporalAccessor;
.source "YearMonth.java"

# interfaces
.implements Ljava/io/Serializable;
.implements Ljava/lang/Comparable;
.implements Lorg/threeten/bp/temporal/Temporal;
.implements Lorg/threeten/bp/temporal/TemporalAdjuster;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/threeten/bp/YearMonth$2;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lorg/threeten/bp/jdk8/DefaultInterfaceTemporalAccessor;",
        "Ljava/io/Serializable;",
        "Ljava/lang/Comparable",
        "<",
        "Lorg/threeten/bp/YearMonth;",
        ">;",
        "Lorg/threeten/bp/temporal/Temporal;",
        "Lorg/threeten/bp/temporal/TemporalAdjuster;"
    }
.end annotation


# static fields
.field public static final a:Lorg/threeten/bp/temporal/TemporalQuery;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lorg/threeten/bp/temporal/TemporalQuery",
            "<",
            "Lorg/threeten/bp/YearMonth;",
            ">;"
        }
    .end annotation
.end field

.field private static final b:Lorg/threeten/bp/format/DateTimeFormatter;


# instance fields
.field private final c:I

.field private final d:I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    .line 101
    new-instance v0, Lorg/threeten/bp/YearMonth$1;

    invoke-direct {v0}, Lorg/threeten/bp/YearMonth$1;-><init>()V

    sput-object v0, Lorg/threeten/bp/YearMonth;->a:Lorg/threeten/bp/temporal/TemporalQuery;

    .line 115
    new-instance v0, Lorg/threeten/bp/format/DateTimeFormatterBuilder;

    invoke-direct {v0}, Lorg/threeten/bp/format/DateTimeFormatterBuilder;-><init>()V

    sget-object v1, Lorg/threeten/bp/temporal/ChronoField;->A:Lorg/threeten/bp/temporal/ChronoField;

    const/4 v2, 0x4

    const/16 v3, 0xa

    sget-object v4, Lorg/threeten/bp/format/SignStyle;->e:Lorg/threeten/bp/format/SignStyle;

    invoke-virtual {v0, v1, v2, v3, v4}, Lorg/threeten/bp/format/DateTimeFormatterBuilder;->a(Lorg/threeten/bp/temporal/TemporalField;IILorg/threeten/bp/format/SignStyle;)Lorg/threeten/bp/format/DateTimeFormatterBuilder;

    move-result-object v0

    const/16 v1, 0x2d

    invoke-virtual {v0, v1}, Lorg/threeten/bp/format/DateTimeFormatterBuilder;->a(C)Lorg/threeten/bp/format/DateTimeFormatterBuilder;

    move-result-object v0

    sget-object v1, Lorg/threeten/bp/temporal/ChronoField;->x:Lorg/threeten/bp/temporal/ChronoField;

    const/4 v2, 0x2

    invoke-virtual {v0, v1, v2}, Lorg/threeten/bp/format/DateTimeFormatterBuilder;->a(Lorg/threeten/bp/temporal/TemporalField;I)Lorg/threeten/bp/format/DateTimeFormatterBuilder;

    move-result-object v0

    invoke-virtual {v0}, Lorg/threeten/bp/format/DateTimeFormatterBuilder;->j()Lorg/threeten/bp/format/DateTimeFormatter;

    move-result-object v0

    sput-object v0, Lorg/threeten/bp/YearMonth;->b:Lorg/threeten/bp/format/DateTimeFormatter;

    return-void
.end method

.method private constructor <init>(II)V
    .locals 0

    .prologue
    .line 278
    invoke-direct {p0}, Lorg/threeten/bp/jdk8/DefaultInterfaceTemporalAccessor;-><init>()V

    .line 279
    iput p1, p0, Lorg/threeten/bp/YearMonth;->c:I

    .line 280
    iput p2, p0, Lorg/threeten/bp/YearMonth;->d:I

    .line 281
    return-void
.end method

.method public static a()Lorg/threeten/bp/YearMonth;
    .locals 1

    .prologue
    .line 144
    invoke-static {}, Lorg/threeten/bp/Clock;->a()Lorg/threeten/bp/Clock;

    move-result-object v0

    invoke-static {v0}, Lorg/threeten/bp/YearMonth;->a(Lorg/threeten/bp/Clock;)Lorg/threeten/bp/YearMonth;

    move-result-object v0

    return-object v0
.end method

.method public static a(II)Lorg/threeten/bp/YearMonth;
    .locals 4

    .prologue
    .line 201
    sget-object v0, Lorg/threeten/bp/temporal/ChronoField;->A:Lorg/threeten/bp/temporal/ChronoField;

    int-to-long v2, p0

    invoke-virtual {v0, v2, v3}, Lorg/threeten/bp/temporal/ChronoField;->a(J)J

    .line 202
    sget-object v0, Lorg/threeten/bp/temporal/ChronoField;->x:Lorg/threeten/bp/temporal/ChronoField;

    int-to-long v2, p1

    invoke-virtual {v0, v2, v3}, Lorg/threeten/bp/temporal/ChronoField;->a(J)J

    .line 203
    new-instance v0, Lorg/threeten/bp/YearMonth;

    invoke-direct {v0, p0, p1}, Lorg/threeten/bp/YearMonth;-><init>(II)V

    return-object v0
.end method

.method public static a(ILorg/threeten/bp/Month;)Lorg/threeten/bp/YearMonth;
    .locals 1

    .prologue
    .line 188
    const-string v0, "month"

    invoke-static {p1, v0}, Lorg/threeten/bp/jdk8/Jdk8Methods;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 189
    invoke-virtual {p1}, Lorg/threeten/bp/Month;->a()I

    move-result v0

    invoke-static {p0, v0}, Lorg/threeten/bp/YearMonth;->a(II)Lorg/threeten/bp/YearMonth;

    move-result-object v0

    return-object v0
.end method

.method static a(Ljava/io/DataInput;)Lorg/threeten/bp/YearMonth;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1103
    invoke-interface {p0}, Ljava/io/DataInput;->readInt()I

    move-result v0

    .line 1104
    invoke-interface {p0}, Ljava/io/DataInput;->readByte()B

    move-result v1

    .line 1105
    invoke-static {v0, v1}, Lorg/threeten/bp/YearMonth;->a(II)Lorg/threeten/bp/YearMonth;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/lang/CharSequence;)Lorg/threeten/bp/YearMonth;
    .locals 1

    .prologue
    .line 253
    sget-object v0, Lorg/threeten/bp/YearMonth;->b:Lorg/threeten/bp/format/DateTimeFormatter;

    invoke-static {p0, v0}, Lorg/threeten/bp/YearMonth;->a(Ljava/lang/CharSequence;Lorg/threeten/bp/format/DateTimeFormatter;)Lorg/threeten/bp/YearMonth;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/lang/CharSequence;Lorg/threeten/bp/format/DateTimeFormatter;)Lorg/threeten/bp/YearMonth;
    .locals 1

    .prologue
    .line 267
    const-string v0, "formatter"

    invoke-static {p1, v0}, Lorg/threeten/bp/jdk8/Jdk8Methods;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 268
    sget-object v0, Lorg/threeten/bp/YearMonth;->a:Lorg/threeten/bp/temporal/TemporalQuery;

    invoke-virtual {p1, p0, v0}, Lorg/threeten/bp/format/DateTimeFormatter;->a(Ljava/lang/CharSequence;Lorg/threeten/bp/temporal/TemporalQuery;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/threeten/bp/YearMonth;

    return-object v0
.end method

.method public static a(Lorg/threeten/bp/Clock;)Lorg/threeten/bp/YearMonth;
    .locals 2

    .prologue
    .line 174
    invoke-static {p0}, Lorg/threeten/bp/LocalDate;->a(Lorg/threeten/bp/Clock;)Lorg/threeten/bp/LocalDate;

    move-result-object v0

    .line 175
    invoke-virtual {v0}, Lorg/threeten/bp/LocalDate;->d()I

    move-result v1

    invoke-virtual {v0}, Lorg/threeten/bp/LocalDate;->f()Lorg/threeten/bp/Month;

    move-result-object v0

    invoke-static {v1, v0}, Lorg/threeten/bp/YearMonth;->a(ILorg/threeten/bp/Month;)Lorg/threeten/bp/YearMonth;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lorg/threeten/bp/temporal/TemporalAccessor;)Lorg/threeten/bp/YearMonth;
    .locals 3

    .prologue
    .line 226
    instance-of v0, p0, Lorg/threeten/bp/YearMonth;

    if-eqz v0, :cond_0

    .line 227
    check-cast p0, Lorg/threeten/bp/YearMonth;

    .line 233
    :goto_0
    return-object p0

    .line 230
    :cond_0
    :try_start_0
    sget-object v0, Lorg/threeten/bp/chrono/IsoChronology;->b:Lorg/threeten/bp/chrono/IsoChronology;

    invoke-static {p0}, Lorg/threeten/bp/chrono/Chronology;->a(Lorg/threeten/bp/temporal/TemporalAccessor;)Lorg/threeten/bp/chrono/Chronology;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/threeten/bp/chrono/IsoChronology;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 231
    invoke-static {p0}, Lorg/threeten/bp/LocalDate;->a(Lorg/threeten/bp/temporal/TemporalAccessor;)Lorg/threeten/bp/LocalDate;

    move-result-object p0

    .line 233
    :cond_1
    sget-object v0, Lorg/threeten/bp/temporal/ChronoField;->A:Lorg/threeten/bp/temporal/ChronoField;

    invoke-interface {p0, v0}, Lorg/threeten/bp/temporal/TemporalAccessor;->c(Lorg/threeten/bp/temporal/TemporalField;)I

    move-result v0

    sget-object v1, Lorg/threeten/bp/temporal/ChronoField;->x:Lorg/threeten/bp/temporal/ChronoField;

    invoke-interface {p0, v1}, Lorg/threeten/bp/temporal/TemporalAccessor;->c(Lorg/threeten/bp/temporal/TemporalField;)I

    move-result v1

    invoke-static {v0, v1}, Lorg/threeten/bp/YearMonth;->a(II)Lorg/threeten/bp/YearMonth;
    :try_end_0
    .catch Lorg/threeten/bp/DateTimeException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object p0

    goto :goto_0

    .line 234
    :catch_0
    move-exception v0

    .line 235
    new-instance v0, Lorg/threeten/bp/DateTimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unable to obtain YearMonth from TemporalAccessor: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", type "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/threeten/bp/DateTimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private b(II)Lorg/threeten/bp/YearMonth;
    .locals 1

    .prologue
    .line 292
    iget v0, p0, Lorg/threeten/bp/YearMonth;->c:I

    if-ne v0, p1, :cond_0

    iget v0, p0, Lorg/threeten/bp/YearMonth;->d:I

    if-ne v0, p2, :cond_0

    .line 295
    :goto_0
    return-object p0

    :cond_0
    new-instance p0, Lorg/threeten/bp/YearMonth;

    invoke-direct {p0, p1, p2}, Lorg/threeten/bp/YearMonth;-><init>(II)V

    goto :goto_0
.end method

.method private g()J
    .locals 4

    .prologue
    .line 441
    iget v0, p0, Lorg/threeten/bp/YearMonth;->c:I

    int-to-long v0, v0

    const-wide/16 v2, 0xc

    mul-long/2addr v0, v2

    iget v2, p0, Lorg/threeten/bp/YearMonth;->d:I

    add-int/lit8 v2, v2, -0x1

    int-to-long v2, v2

    add-long/2addr v0, v2

    return-wide v0
.end method

.method private readResolve()Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/ObjectStreamException;
        }
    .end annotation

    .prologue
    .line 1094
    new-instance v0, Ljava/io/InvalidObjectException;

    const-string v1, "Deserialization via serialization delegate"

    invoke-direct {v0, v1}, Ljava/io/InvalidObjectException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private writeReplace()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 1085
    new-instance v0, Lorg/threeten/bp/Ser;

    const/16 v1, 0x44

    invoke-direct {v0, v1, p0}, Lorg/threeten/bp/Ser;-><init>(BLjava/lang/Object;)V

    return-object v0
.end method


# virtual methods
.method public a(Lorg/threeten/bp/YearMonth;)I
    .locals 2

    .prologue
    .line 984
    iget v0, p0, Lorg/threeten/bp/YearMonth;->c:I

    iget v1, p1, Lorg/threeten/bp/YearMonth;->c:I

    sub-int/2addr v0, v1

    .line 985
    if-nez v0, :cond_0

    .line 986
    iget v0, p0, Lorg/threeten/bp/YearMonth;->d:I

    iget v1, p1, Lorg/threeten/bp/YearMonth;->d:I

    sub-int/2addr v0, v1

    .line 988
    :cond_0
    return v0
.end method

.method public a(Lorg/threeten/bp/temporal/Temporal;Lorg/threeten/bp/temporal/TemporalUnit;)J
    .locals 6

    .prologue
    .line 916
    invoke-static {p1}, Lorg/threeten/bp/YearMonth;->a(Lorg/threeten/bp/temporal/TemporalAccessor;)Lorg/threeten/bp/YearMonth;

    move-result-object v1

    .line 917
    instance-of v0, p2, Lorg/threeten/bp/temporal/ChronoUnit;

    if-eqz v0, :cond_0

    .line 918
    invoke-direct {v1}, Lorg/threeten/bp/YearMonth;->g()J

    move-result-wide v2

    invoke-direct {p0}, Lorg/threeten/bp/YearMonth;->g()J

    move-result-wide v4

    sub-long/2addr v2, v4

    .line 919
    sget-object v4, Lorg/threeten/bp/YearMonth$2;->b:[I

    move-object v0, p2

    check-cast v0, Lorg/threeten/bp/temporal/ChronoUnit;

    invoke-virtual {v0}, Lorg/threeten/bp/temporal/ChronoUnit;->ordinal()I

    move-result v0

    aget v0, v4, v0

    packed-switch v0, :pswitch_data_0

    .line 927
    new-instance v0, Lorg/threeten/bp/temporal/UnsupportedTemporalTypeException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unsupported unit: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/threeten/bp/temporal/UnsupportedTemporalTypeException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_0
    move-wide v0, v2

    .line 929
    :goto_0
    return-wide v0

    .line 921
    :pswitch_1
    const-wide/16 v0, 0xc

    div-long v0, v2, v0

    goto :goto_0

    .line 922
    :pswitch_2
    const-wide/16 v0, 0x78

    div-long v0, v2, v0

    goto :goto_0

    .line 923
    :pswitch_3
    const-wide/16 v0, 0x4b0

    div-long v0, v2, v0

    goto :goto_0

    .line 924
    :pswitch_4
    const-wide/16 v0, 0x2ee0

    div-long v0, v2, v0

    goto :goto_0

    .line 925
    :pswitch_5
    sget-object v0, Lorg/threeten/bp/temporal/ChronoField;->B:Lorg/threeten/bp/temporal/ChronoField;

    invoke-virtual {v1, v0}, Lorg/threeten/bp/YearMonth;->d(Lorg/threeten/bp/temporal/TemporalField;)J

    move-result-wide v0

    sget-object v2, Lorg/threeten/bp/temporal/ChronoField;->B:Lorg/threeten/bp/temporal/ChronoField;

    invoke-virtual {p0, v2}, Lorg/threeten/bp/YearMonth;->d(Lorg/threeten/bp/temporal/TemporalField;)J

    move-result-wide v2

    sub-long/2addr v0, v2

    goto :goto_0

    .line 929
    :cond_0
    invoke-interface {p2, p0, v1}, Lorg/threeten/bp/temporal/TemporalUnit;->a(Lorg/threeten/bp/temporal/Temporal;Lorg/threeten/bp/temporal/Temporal;)J

    move-result-wide v0

    goto :goto_0

    .line 919
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public a(Lorg/threeten/bp/temporal/TemporalQuery;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<R:",
            "Ljava/lang/Object;",
            ">(",
            "Lorg/threeten/bp/temporal/TemporalQuery",
            "<TR;>;)TR;"
        }
    .end annotation

    .prologue
    .line 826
    invoke-static {}, Lorg/threeten/bp/temporal/TemporalQueries;->b()Lorg/threeten/bp/temporal/TemporalQuery;

    move-result-object v0

    if-ne p1, v0, :cond_0

    .line 827
    sget-object v0, Lorg/threeten/bp/chrono/IsoChronology;->b:Lorg/threeten/bp/chrono/IsoChronology;

    .line 834
    :goto_0
    return-object v0

    .line 828
    :cond_0
    invoke-static {}, Lorg/threeten/bp/temporal/TemporalQueries;->c()Lorg/threeten/bp/temporal/TemporalQuery;

    move-result-object v0

    if-ne p1, v0, :cond_1

    .line 829
    sget-object v0, Lorg/threeten/bp/temporal/ChronoUnit;->j:Lorg/threeten/bp/temporal/ChronoUnit;

    goto :goto_0

    .line 830
    :cond_1
    invoke-static {}, Lorg/threeten/bp/temporal/TemporalQueries;->f()Lorg/threeten/bp/temporal/TemporalQuery;

    move-result-object v0

    if-eq p1, v0, :cond_2

    invoke-static {}, Lorg/threeten/bp/temporal/TemporalQueries;->g()Lorg/threeten/bp/temporal/TemporalQuery;

    move-result-object v0

    if-eq p1, v0, :cond_2

    invoke-static {}, Lorg/threeten/bp/temporal/TemporalQueries;->d()Lorg/threeten/bp/temporal/TemporalQuery;

    move-result-object v0

    if-eq p1, v0, :cond_2

    invoke-static {}, Lorg/threeten/bp/temporal/TemporalQueries;->a()Lorg/threeten/bp/temporal/TemporalQuery;

    move-result-object v0

    if-eq p1, v0, :cond_2

    invoke-static {}, Lorg/threeten/bp/temporal/TemporalQueries;->e()Lorg/threeten/bp/temporal/TemporalQuery;

    move-result-object v0

    if-ne p1, v0, :cond_3

    .line 832
    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    .line 834
    :cond_3
    invoke-super {p0, p1}, Lorg/threeten/bp/jdk8/DefaultInterfaceTemporalAccessor;->a(Lorg/threeten/bp/temporal/TemporalQuery;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method public a(I)Lorg/threeten/bp/YearMonth;
    .locals 4

    .prologue
    .line 648
    sget-object v0, Lorg/threeten/bp/temporal/ChronoField;->A:Lorg/threeten/bp/temporal/ChronoField;

    int-to-long v2, p1

    invoke-virtual {v0, v2, v3}, Lorg/threeten/bp/temporal/ChronoField;->a(J)J

    .line 649
    iget v0, p0, Lorg/threeten/bp/YearMonth;->d:I

    invoke-direct {p0, p1, v0}, Lorg/threeten/bp/YearMonth;->b(II)Lorg/threeten/bp/YearMonth;

    move-result-object v0

    return-object v0
.end method

.method public a(J)Lorg/threeten/bp/YearMonth;
    .locals 5

    .prologue
    .line 719
    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-nez v0, :cond_0

    .line 723
    :goto_0
    return-object p0

    .line 722
    :cond_0
    sget-object v0, Lorg/threeten/bp/temporal/ChronoField;->A:Lorg/threeten/bp/temporal/ChronoField;

    iget v1, p0, Lorg/threeten/bp/YearMonth;->c:I

    int-to-long v2, v1

    add-long/2addr v2, p1

    invoke-virtual {v0, v2, v3}, Lorg/threeten/bp/temporal/ChronoField;->b(J)I

    move-result v0

    .line 723
    iget v1, p0, Lorg/threeten/bp/YearMonth;->d:I

    invoke-direct {p0, v0, v1}, Lorg/threeten/bp/YearMonth;->b(II)Lorg/threeten/bp/YearMonth;

    move-result-object p0

    goto :goto_0
.end method

.method public a(JLorg/threeten/bp/temporal/TemporalUnit;)Lorg/threeten/bp/YearMonth;
    .locals 5

    .prologue
    .line 695
    instance-of v0, p3, Lorg/threeten/bp/temporal/ChronoUnit;

    if-eqz v0, :cond_0

    .line 696
    sget-object v1, Lorg/threeten/bp/YearMonth$2;->b:[I

    move-object v0, p3

    check-cast v0, Lorg/threeten/bp/temporal/ChronoUnit;

    invoke-virtual {v0}, Lorg/threeten/bp/temporal/ChronoUnit;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 704
    new-instance v0, Lorg/threeten/bp/temporal/UnsupportedTemporalTypeException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unsupported unit: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/threeten/bp/temporal/UnsupportedTemporalTypeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 697
    :pswitch_0
    invoke-virtual {p0, p1, p2}, Lorg/threeten/bp/YearMonth;->b(J)Lorg/threeten/bp/YearMonth;

    move-result-object v0

    .line 706
    :goto_0
    return-object v0

    .line 698
    :pswitch_1
    invoke-virtual {p0, p1, p2}, Lorg/threeten/bp/YearMonth;->a(J)Lorg/threeten/bp/YearMonth;

    move-result-object v0

    goto :goto_0

    .line 699
    :pswitch_2
    const/16 v0, 0xa

    invoke-static {p1, p2, v0}, Lorg/threeten/bp/jdk8/Jdk8Methods;->a(JI)J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lorg/threeten/bp/YearMonth;->a(J)Lorg/threeten/bp/YearMonth;

    move-result-object v0

    goto :goto_0

    .line 700
    :pswitch_3
    const/16 v0, 0x64

    invoke-static {p1, p2, v0}, Lorg/threeten/bp/jdk8/Jdk8Methods;->a(JI)J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lorg/threeten/bp/YearMonth;->a(J)Lorg/threeten/bp/YearMonth;

    move-result-object v0

    goto :goto_0

    .line 701
    :pswitch_4
    const/16 v0, 0x3e8

    invoke-static {p1, p2, v0}, Lorg/threeten/bp/jdk8/Jdk8Methods;->a(JI)J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lorg/threeten/bp/YearMonth;->a(J)Lorg/threeten/bp/YearMonth;

    move-result-object v0

    goto :goto_0

    .line 702
    :pswitch_5
    sget-object v0, Lorg/threeten/bp/temporal/ChronoField;->B:Lorg/threeten/bp/temporal/ChronoField;

    sget-object v1, Lorg/threeten/bp/temporal/ChronoField;->B:Lorg/threeten/bp/temporal/ChronoField;

    invoke-virtual {p0, v1}, Lorg/threeten/bp/YearMonth;->d(Lorg/threeten/bp/temporal/TemporalField;)J

    move-result-wide v2

    invoke-static {v2, v3, p1, p2}, Lorg/threeten/bp/jdk8/Jdk8Methods;->b(JJ)J

    move-result-wide v2

    invoke-virtual {p0, v0, v2, v3}, Lorg/threeten/bp/YearMonth;->a(Lorg/threeten/bp/temporal/TemporalField;J)Lorg/threeten/bp/YearMonth;

    move-result-object v0

    goto :goto_0

    .line 706
    :cond_0
    invoke-interface {p3, p0, p1, p2}, Lorg/threeten/bp/temporal/TemporalUnit;->a(Lorg/threeten/bp/temporal/Temporal;J)Lorg/threeten/bp/temporal/Temporal;

    move-result-object v0

    check-cast v0, Lorg/threeten/bp/YearMonth;

    goto :goto_0

    .line 696
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public a(Lorg/threeten/bp/temporal/TemporalAdjuster;)Lorg/threeten/bp/YearMonth;
    .locals 1

    .prologue
    .line 570
    invoke-interface {p1, p0}, Lorg/threeten/bp/temporal/TemporalAdjuster;->a(Lorg/threeten/bp/temporal/Temporal;)Lorg/threeten/bp/temporal/Temporal;

    move-result-object v0

    check-cast v0, Lorg/threeten/bp/YearMonth;

    return-object v0
.end method

.method public a(Lorg/threeten/bp/temporal/TemporalField;J)Lorg/threeten/bp/YearMonth;
    .locals 4

    .prologue
    .line 622
    instance-of v0, p1, Lorg/threeten/bp/temporal/ChronoField;

    if-eqz v0, :cond_2

    move-object v0, p1

    .line 623
    check-cast v0, Lorg/threeten/bp/temporal/ChronoField;

    .line 624
    invoke-virtual {v0, p2, p3}, Lorg/threeten/bp/temporal/ChronoField;->a(J)J

    .line 625
    sget-object v1, Lorg/threeten/bp/YearMonth$2;->a:[I

    invoke-virtual {v0}, Lorg/threeten/bp/temporal/ChronoField;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 632
    new-instance v0, Lorg/threeten/bp/temporal/UnsupportedTemporalTypeException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unsupported field: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/threeten/bp/temporal/UnsupportedTemporalTypeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 626
    :pswitch_0
    long-to-int v0, p2

    invoke-virtual {p0, v0}, Lorg/threeten/bp/YearMonth;->b(I)Lorg/threeten/bp/YearMonth;

    move-result-object p0

    .line 634
    :cond_0
    :goto_0
    return-object p0

    .line 627
    :pswitch_1
    sget-object v0, Lorg/threeten/bp/temporal/ChronoField;->y:Lorg/threeten/bp/temporal/ChronoField;

    invoke-virtual {p0, v0}, Lorg/threeten/bp/YearMonth;->d(Lorg/threeten/bp/temporal/TemporalField;)J

    move-result-wide v0

    sub-long v0, p2, v0

    invoke-virtual {p0, v0, v1}, Lorg/threeten/bp/YearMonth;->b(J)Lorg/threeten/bp/YearMonth;

    move-result-object p0

    goto :goto_0

    .line 628
    :pswitch_2
    iget v0, p0, Lorg/threeten/bp/YearMonth;->c:I

    const/4 v1, 0x1

    if-ge v0, v1, :cond_1

    const-wide/16 v0, 0x1

    sub-long p2, v0, p2

    :cond_1
    long-to-int v0, p2

    invoke-virtual {p0, v0}, Lorg/threeten/bp/YearMonth;->a(I)Lorg/threeten/bp/YearMonth;

    move-result-object p0

    goto :goto_0

    .line 629
    :pswitch_3
    long-to-int v0, p2

    invoke-virtual {p0, v0}, Lorg/threeten/bp/YearMonth;->a(I)Lorg/threeten/bp/YearMonth;

    move-result-object p0

    goto :goto_0

    .line 630
    :pswitch_4
    sget-object v0, Lorg/threeten/bp/temporal/ChronoField;->B:Lorg/threeten/bp/temporal/ChronoField;

    invoke-virtual {p0, v0}, Lorg/threeten/bp/YearMonth;->d(Lorg/threeten/bp/temporal/TemporalField;)J

    move-result-wide v0

    cmp-long v0, v0, p2

    if-eqz v0, :cond_0

    iget v0, p0, Lorg/threeten/bp/YearMonth;->c:I

    rsub-int/lit8 v0, v0, 0x1

    invoke-virtual {p0, v0}, Lorg/threeten/bp/YearMonth;->a(I)Lorg/threeten/bp/YearMonth;

    move-result-object p0

    goto :goto_0

    .line 634
    :cond_2
    invoke-interface {p1, p0, p2, p3}, Lorg/threeten/bp/temporal/TemporalField;->a(Lorg/threeten/bp/temporal/Temporal;J)Lorg/threeten/bp/temporal/Temporal;

    move-result-object v0

    check-cast v0, Lorg/threeten/bp/YearMonth;

    move-object p0, v0

    goto :goto_0

    .line 625
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public a(Lorg/threeten/bp/temporal/Temporal;)Lorg/threeten/bp/temporal/Temporal;
    .locals 4

    .prologue
    .line 865
    invoke-static {p1}, Lorg/threeten/bp/chrono/Chronology;->a(Lorg/threeten/bp/temporal/TemporalAccessor;)Lorg/threeten/bp/chrono/Chronology;

    move-result-object v0

    sget-object v1, Lorg/threeten/bp/chrono/IsoChronology;->b:Lorg/threeten/bp/chrono/IsoChronology;

    invoke-virtual {v0, v1}, Lorg/threeten/bp/chrono/Chronology;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 866
    new-instance v0, Lorg/threeten/bp/DateTimeException;

    const-string v1, "Adjustment only supported on ISO date-time"

    invoke-direct {v0, v1}, Lorg/threeten/bp/DateTimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 868
    :cond_0
    sget-object v0, Lorg/threeten/bp/temporal/ChronoField;->y:Lorg/threeten/bp/temporal/ChronoField;

    invoke-direct {p0}, Lorg/threeten/bp/YearMonth;->g()J

    move-result-wide v2

    invoke-interface {p1, v0, v2, v3}, Lorg/threeten/bp/temporal/Temporal;->b(Lorg/threeten/bp/temporal/TemporalField;J)Lorg/threeten/bp/temporal/Temporal;

    move-result-object v0

    return-object v0
.end method

.method a(Ljava/io/DataOutput;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1098
    iget v0, p0, Lorg/threeten/bp/YearMonth;->c:I

    invoke-interface {p1, v0}, Ljava/io/DataOutput;->writeInt(I)V

    .line 1099
    iget v0, p0, Lorg/threeten/bp/YearMonth;->d:I

    invoke-interface {p1, v0}, Ljava/io/DataOutput;->writeByte(I)V

    .line 1100
    return-void
.end method

.method public a(Lorg/threeten/bp/temporal/TemporalField;)Z
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 329
    instance-of v2, p1, Lorg/threeten/bp/temporal/ChronoField;

    if-eqz v2, :cond_2

    .line 330
    sget-object v2, Lorg/threeten/bp/temporal/ChronoField;->A:Lorg/threeten/bp/temporal/ChronoField;

    if-eq p1, v2, :cond_0

    sget-object v2, Lorg/threeten/bp/temporal/ChronoField;->x:Lorg/threeten/bp/temporal/ChronoField;

    if-eq p1, v2, :cond_0

    sget-object v2, Lorg/threeten/bp/temporal/ChronoField;->y:Lorg/threeten/bp/temporal/ChronoField;

    if-eq p1, v2, :cond_0

    sget-object v2, Lorg/threeten/bp/temporal/ChronoField;->z:Lorg/threeten/bp/temporal/ChronoField;

    if-eq p1, v2, :cond_0

    sget-object v2, Lorg/threeten/bp/temporal/ChronoField;->B:Lorg/threeten/bp/temporal/ChronoField;

    if-ne p1, v2, :cond_1

    :cond_0
    move v0, v1

    .line 333
    :cond_1
    :goto_0
    return v0

    :cond_2
    if-eqz p1, :cond_3

    invoke-interface {p1, p0}, Lorg/threeten/bp/temporal/TemporalField;->a(Lorg/threeten/bp/temporal/TemporalAccessor;)Z

    move-result v2

    if-eqz v2, :cond_3

    :goto_1
    move v0, v1

    goto :goto_0

    :cond_3
    move v1, v0

    goto :goto_1
.end method

.method public b()I
    .locals 1

    .prologue
    .line 455
    iget v0, p0, Lorg/threeten/bp/YearMonth;->c:I

    return v0
.end method

.method public b(I)Lorg/threeten/bp/YearMonth;
    .locals 4

    .prologue
    .line 662
    sget-object v0, Lorg/threeten/bp/temporal/ChronoField;->x:Lorg/threeten/bp/temporal/ChronoField;

    int-to-long v2, p1

    invoke-virtual {v0, v2, v3}, Lorg/threeten/bp/temporal/ChronoField;->a(J)J

    .line 663
    iget v0, p0, Lorg/threeten/bp/YearMonth;->c:I

    invoke-direct {p0, v0, p1}, Lorg/threeten/bp/YearMonth;->b(II)Lorg/threeten/bp/YearMonth;

    move-result-object v0

    return-object v0
.end method

.method public b(J)Lorg/threeten/bp/YearMonth;
    .locals 7

    .prologue
    const-wide/16 v4, 0xc

    .line 736
    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-nez v0, :cond_0

    .line 743
    :goto_0
    return-object p0

    .line 739
    :cond_0
    iget v0, p0, Lorg/threeten/bp/YearMonth;->c:I

    int-to-long v0, v0

    mul-long/2addr v0, v4

    iget v2, p0, Lorg/threeten/bp/YearMonth;->d:I

    add-int/lit8 v2, v2, -0x1

    int-to-long v2, v2

    add-long/2addr v0, v2

    .line 740
    add-long/2addr v0, p1

    .line 741
    sget-object v2, Lorg/threeten/bp/temporal/ChronoField;->A:Lorg/threeten/bp/temporal/ChronoField;

    invoke-static {v0, v1, v4, v5}, Lorg/threeten/bp/jdk8/Jdk8Methods;->e(JJ)J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Lorg/threeten/bp/temporal/ChronoField;->b(J)I

    move-result v2

    .line 742
    const/16 v3, 0xc

    invoke-static {v0, v1, v3}, Lorg/threeten/bp/jdk8/Jdk8Methods;->b(JI)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    .line 743
    invoke-direct {p0, v2, v0}, Lorg/threeten/bp/YearMonth;->b(II)Lorg/threeten/bp/YearMonth;

    move-result-object p0

    goto :goto_0
.end method

.method public b(JLorg/threeten/bp/temporal/TemporalUnit;)Lorg/threeten/bp/YearMonth;
    .locals 5

    .prologue
    .line 775
    const-wide/high16 v0, -0x8000000000000000L

    cmp-long v0, p1, v0

    if-nez v0, :cond_0

    const-wide v0, 0x7fffffffffffffffL

    invoke-virtual {p0, v0, v1, p3}, Lorg/threeten/bp/YearMonth;->a(JLorg/threeten/bp/temporal/TemporalUnit;)Lorg/threeten/bp/YearMonth;

    move-result-object v0

    const-wide/16 v2, 0x1

    invoke-virtual {v0, v2, v3, p3}, Lorg/threeten/bp/YearMonth;->a(JLorg/threeten/bp/temporal/TemporalUnit;)Lorg/threeten/bp/YearMonth;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    neg-long v0, p1

    invoke-virtual {p0, v0, v1, p3}, Lorg/threeten/bp/YearMonth;->a(JLorg/threeten/bp/temporal/TemporalUnit;)Lorg/threeten/bp/YearMonth;

    move-result-object v0

    goto :goto_0
.end method

.method public synthetic b(Lorg/threeten/bp/temporal/TemporalAdjuster;)Lorg/threeten/bp/temporal/Temporal;
    .locals 1

    .prologue
    .line 94
    invoke-virtual {p0, p1}, Lorg/threeten/bp/YearMonth;->a(Lorg/threeten/bp/temporal/TemporalAdjuster;)Lorg/threeten/bp/YearMonth;

    move-result-object v0

    return-object v0
.end method

.method public synthetic b(Lorg/threeten/bp/temporal/TemporalField;J)Lorg/threeten/bp/temporal/Temporal;
    .locals 2

    .prologue
    .line 94
    invoke-virtual {p0, p1, p2, p3}, Lorg/threeten/bp/YearMonth;->a(Lorg/threeten/bp/temporal/TemporalField;J)Lorg/threeten/bp/YearMonth;

    move-result-object v0

    return-object v0
.end method

.method public b(Lorg/threeten/bp/temporal/TemporalField;)Lorg/threeten/bp/temporal/ValueRange;
    .locals 4

    .prologue
    const-wide/16 v2, 0x1

    .line 368
    sget-object v0, Lorg/threeten/bp/temporal/ChronoField;->z:Lorg/threeten/bp/temporal/ChronoField;

    if-ne p1, v0, :cond_1

    .line 369
    invoke-virtual {p0}, Lorg/threeten/bp/YearMonth;->b()I

    move-result v0

    if-gtz v0, :cond_0

    const-wide/32 v0, 0x3b9aca00

    invoke-static {v2, v3, v0, v1}, Lorg/threeten/bp/temporal/ValueRange;->a(JJ)Lorg/threeten/bp/temporal/ValueRange;

    move-result-object v0

    .line 371
    :goto_0
    return-object v0

    .line 369
    :cond_0
    const-wide/32 v0, 0x3b9ac9ff

    invoke-static {v2, v3, v0, v1}, Lorg/threeten/bp/temporal/ValueRange;->a(JJ)Lorg/threeten/bp/temporal/ValueRange;

    move-result-object v0

    goto :goto_0

    .line 371
    :cond_1
    invoke-super {p0, p1}, Lorg/threeten/bp/jdk8/DefaultInterfaceTemporalAccessor;->b(Lorg/threeten/bp/temporal/TemporalField;)Lorg/threeten/bp/temporal/ValueRange;

    move-result-object v0

    goto :goto_0
.end method

.method public b(Lorg/threeten/bp/YearMonth;)Z
    .locals 1

    .prologue
    .line 998
    invoke-virtual {p0, p1}, Lorg/threeten/bp/YearMonth;->a(Lorg/threeten/bp/YearMonth;)I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public c(Lorg/threeten/bp/temporal/TemporalField;)I
    .locals 4

    .prologue
    .line 400
    invoke-virtual {p0, p1}, Lorg/threeten/bp/YearMonth;->b(Lorg/threeten/bp/temporal/TemporalField;)Lorg/threeten/bp/temporal/ValueRange;

    move-result-object v0

    invoke-virtual {p0, p1}, Lorg/threeten/bp/YearMonth;->d(Lorg/threeten/bp/temporal/TemporalField;)J

    move-result-wide v2

    invoke-virtual {v0, v2, v3, p1}, Lorg/threeten/bp/temporal/ValueRange;->b(JLorg/threeten/bp/temporal/TemporalField;)I

    move-result v0

    return v0
.end method

.method public c(I)Lorg/threeten/bp/LocalDate;
    .locals 2

    .prologue
    .line 951
    iget v0, p0, Lorg/threeten/bp/YearMonth;->c:I

    iget v1, p0, Lorg/threeten/bp/YearMonth;->d:I

    invoke-static {v0, v1, p1}, Lorg/threeten/bp/LocalDate;->a(III)Lorg/threeten/bp/LocalDate;

    move-result-object v0

    return-object v0
.end method

.method public c()Lorg/threeten/bp/Month;
    .locals 1

    .prologue
    .line 483
    iget v0, p0, Lorg/threeten/bp/YearMonth;->d:I

    invoke-static {v0}, Lorg/threeten/bp/Month;->a(I)Lorg/threeten/bp/Month;

    move-result-object v0

    return-object v0
.end method

.method public synthetic c(JLorg/threeten/bp/temporal/TemporalUnit;)Lorg/threeten/bp/temporal/Temporal;
    .locals 1

    .prologue
    .line 94
    invoke-virtual {p0, p1, p2, p3}, Lorg/threeten/bp/YearMonth;->b(JLorg/threeten/bp/temporal/TemporalUnit;)Lorg/threeten/bp/YearMonth;

    move-result-object v0

    return-object v0
.end method

.method public synthetic compareTo(Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 94
    check-cast p1, Lorg/threeten/bp/YearMonth;

    invoke-virtual {p0, p1}, Lorg/threeten/bp/YearMonth;->a(Lorg/threeten/bp/YearMonth;)I

    move-result v0

    return v0
.end method

.method public d(Lorg/threeten/bp/temporal/TemporalField;)J
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 427
    instance-of v0, p1, Lorg/threeten/bp/temporal/ChronoField;

    if-eqz v0, :cond_2

    .line 428
    sget-object v2, Lorg/threeten/bp/YearMonth$2;->a:[I

    move-object v0, p1

    check-cast v0, Lorg/threeten/bp/temporal/ChronoField;

    invoke-virtual {v0}, Lorg/threeten/bp/temporal/ChronoField;->ordinal()I

    move-result v0

    aget v0, v2, v0

    packed-switch v0, :pswitch_data_0

    .line 435
    new-instance v0, Lorg/threeten/bp/temporal/UnsupportedTemporalTypeException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unsupported field: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/threeten/bp/temporal/UnsupportedTemporalTypeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 429
    :pswitch_0
    iget v0, p0, Lorg/threeten/bp/YearMonth;->d:I

    int-to-long v0, v0

    .line 437
    :goto_0
    return-wide v0

    .line 430
    :pswitch_1
    invoke-direct {p0}, Lorg/threeten/bp/YearMonth;->g()J

    move-result-wide v0

    goto :goto_0

    .line 431
    :pswitch_2
    iget v0, p0, Lorg/threeten/bp/YearMonth;->c:I

    if-ge v0, v1, :cond_0

    iget v0, p0, Lorg/threeten/bp/YearMonth;->c:I

    rsub-int/lit8 v0, v0, 0x1

    :goto_1
    int-to-long v0, v0

    goto :goto_0

    :cond_0
    iget v0, p0, Lorg/threeten/bp/YearMonth;->c:I

    goto :goto_1

    .line 432
    :pswitch_3
    iget v0, p0, Lorg/threeten/bp/YearMonth;->c:I

    int-to-long v0, v0

    goto :goto_0

    .line 433
    :pswitch_4
    iget v0, p0, Lorg/threeten/bp/YearMonth;->c:I

    if-ge v0, v1, :cond_1

    const/4 v0, 0x0

    :goto_2
    int-to-long v0, v0

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_2

    .line 437
    :cond_2
    invoke-interface {p1, p0}, Lorg/threeten/bp/temporal/TemporalField;->c(Lorg/threeten/bp/temporal/TemporalAccessor;)J

    move-result-wide v0

    goto :goto_0

    .line 428
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public synthetic d(JLorg/threeten/bp/temporal/TemporalUnit;)Lorg/threeten/bp/temporal/Temporal;
    .locals 1

    .prologue
    .line 94
    invoke-virtual {p0, p1, p2, p3}, Lorg/threeten/bp/YearMonth;->a(JLorg/threeten/bp/temporal/TemporalUnit;)Lorg/threeten/bp/YearMonth;

    move-result-object v0

    return-object v0
.end method

.method public d()Z
    .locals 4

    .prologue
    .line 506
    sget-object v0, Lorg/threeten/bp/chrono/IsoChronology;->b:Lorg/threeten/bp/chrono/IsoChronology;

    iget v1, p0, Lorg/threeten/bp/YearMonth;->c:I

    int-to-long v2, v1

    invoke-virtual {v0, v2, v3}, Lorg/threeten/bp/chrono/IsoChronology;->a(J)Z

    move-result v0

    return v0
.end method

.method public e()I
    .locals 2

    .prologue
    .line 531
    invoke-virtual {p0}, Lorg/threeten/bp/YearMonth;->c()Lorg/threeten/bp/Month;

    move-result-object v0

    invoke-virtual {p0}, Lorg/threeten/bp/YearMonth;->d()Z

    move-result v1

    invoke-virtual {v0, v1}, Lorg/threeten/bp/Month;->a(Z)I

    move-result v0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1022
    if-ne p0, p1, :cond_1

    .line 1029
    :cond_0
    :goto_0
    return v0

    .line 1025
    :cond_1
    instance-of v2, p1, Lorg/threeten/bp/YearMonth;

    if-eqz v2, :cond_3

    .line 1026
    check-cast p1, Lorg/threeten/bp/YearMonth;

    .line 1027
    iget v2, p0, Lorg/threeten/bp/YearMonth;->c:I

    iget v3, p1, Lorg/threeten/bp/YearMonth;->c:I

    if-ne v2, v3, :cond_2

    iget v2, p0, Lorg/threeten/bp/YearMonth;->d:I

    iget v3, p1, Lorg/threeten/bp/YearMonth;->d:I

    if-eq v2, v3, :cond_0

    :cond_2
    move v0, v1

    goto :goto_0

    :cond_3
    move v0, v1

    .line 1029
    goto :goto_0
.end method

.method public f()Lorg/threeten/bp/LocalDate;
    .locals 3

    .prologue
    .line 969
    iget v0, p0, Lorg/threeten/bp/YearMonth;->c:I

    iget v1, p0, Lorg/threeten/bp/YearMonth;->d:I

    invoke-virtual {p0}, Lorg/threeten/bp/YearMonth;->e()I

    move-result v2

    invoke-static {v0, v1, v2}, Lorg/threeten/bp/LocalDate;->a(III)Lorg/threeten/bp/LocalDate;

    move-result-object v0

    return-object v0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 1039
    iget v0, p0, Lorg/threeten/bp/YearMonth;->c:I

    iget v1, p0, Lorg/threeten/bp/YearMonth;->d:I

    shl-int/lit8 v1, v1, 0x1b

    xor-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 1052
    iget v0, p0, Lorg/threeten/bp/YearMonth;->c:I

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    .line 1053
    new-instance v1, Ljava/lang/StringBuilder;

    const/16 v2, 0x9

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 1054
    const/16 v2, 0x3e8

    if-ge v0, v2, :cond_1

    .line 1055
    iget v0, p0, Lorg/threeten/bp/YearMonth;->c:I

    if-gez v0, :cond_0

    .line 1056
    iget v0, p0, Lorg/threeten/bp/YearMonth;->c:I

    add-int/lit16 v0, v0, -0x2710

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->deleteCharAt(I)Ljava/lang/StringBuilder;

    .line 1063
    :goto_0
    iget v0, p0, Lorg/threeten/bp/YearMonth;->d:I

    const/16 v2, 0xa

    if-ge v0, v2, :cond_2

    const-string v0, "-0"

    :goto_1
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lorg/threeten/bp/YearMonth;->d:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 1058
    :cond_0
    iget v0, p0, Lorg/threeten/bp/YearMonth;->c:I

    add-int/lit16 v0, v0, 0x2710

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->deleteCharAt(I)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 1061
    :cond_1
    iget v0, p0, Lorg/threeten/bp/YearMonth;->c:I

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 1063
    :cond_2
    const-string v0, "-"

    goto :goto_1
.end method
