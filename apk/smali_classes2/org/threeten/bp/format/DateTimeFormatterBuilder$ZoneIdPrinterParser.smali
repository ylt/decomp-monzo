.class final Lorg/threeten/bp/format/DateTimeFormatterBuilder$ZoneIdPrinterParser;
.super Ljava/lang/Object;
.source "DateTimeFormatterBuilder.java"

# interfaces
.implements Lorg/threeten/bp/format/DateTimeFormatterBuilder$DateTimePrinterParser;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/threeten/bp/format/DateTimeFormatterBuilder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "ZoneIdPrinterParser"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/threeten/bp/format/DateTimeFormatterBuilder$ZoneIdPrinterParser$SubstringTree;
    }
.end annotation


# static fields
.field private static volatile c:Ljava/util/Map$Entry;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map$Entry",
            "<",
            "Ljava/lang/Integer;",
            "Lorg/threeten/bp/format/DateTimeFormatterBuilder$ZoneIdPrinterParser$SubstringTree;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final a:Lorg/threeten/bp/temporal/TemporalQuery;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lorg/threeten/bp/temporal/TemporalQuery",
            "<",
            "Lorg/threeten/bp/ZoneId;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Ljava/lang/String;


# direct methods
.method constructor <init>(Lorg/threeten/bp/temporal/TemporalQuery;Ljava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/threeten/bp/temporal/TemporalQuery",
            "<",
            "Lorg/threeten/bp/ZoneId;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 3419
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 3420
    iput-object p1, p0, Lorg/threeten/bp/format/DateTimeFormatterBuilder$ZoneIdPrinterParser;->a:Lorg/threeten/bp/temporal/TemporalQuery;

    .line 3421
    iput-object p2, p0, Lorg/threeten/bp/format/DateTimeFormatterBuilder$ZoneIdPrinterParser;->b:Ljava/lang/String;

    .line 3422
    return-void
.end method

.method private a(Lorg/threeten/bp/format/DateTimeParseContext;Ljava/lang/CharSequence;II)I
    .locals 4

    .prologue
    .line 3549
    invoke-interface {p2, p3, p4}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v1

    .line 3550
    invoke-virtual {p1}, Lorg/threeten/bp/format/DateTimeParseContext;->a()Lorg/threeten/bp/format/DateTimeParseContext;

    move-result-object v2

    .line 3551
    invoke-interface {p2}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-ge p4, v0, :cond_0

    invoke-interface {p2, p4}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v0

    const/16 v3, 0x5a

    invoke-virtual {p1, v0, v3}, Lorg/threeten/bp/format/DateTimeParseContext;->a(CC)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 3552
    sget-object v0, Lorg/threeten/bp/ZoneOffset;->d:Lorg/threeten/bp/ZoneOffset;

    invoke-static {v1, v0}, Lorg/threeten/bp/ZoneId;->a(Ljava/lang/String;Lorg/threeten/bp/ZoneOffset;)Lorg/threeten/bp/ZoneId;

    move-result-object v0

    invoke-virtual {p1, v0}, Lorg/threeten/bp/format/DateTimeParseContext;->a(Lorg/threeten/bp/ZoneId;)V

    .line 3563
    :goto_0
    return p4

    .line 3555
    :cond_0
    sget-object v0, Lorg/threeten/bp/format/DateTimeFormatterBuilder$OffsetIdPrinterParser;->b:Lorg/threeten/bp/format/DateTimeFormatterBuilder$OffsetIdPrinterParser;

    invoke-virtual {v0, v2, p2, p4}, Lorg/threeten/bp/format/DateTimeFormatterBuilder$OffsetIdPrinterParser;->a(Lorg/threeten/bp/format/DateTimeParseContext;Ljava/lang/CharSequence;I)I

    move-result v0

    .line 3556
    if-gez v0, :cond_1

    .line 3557
    sget-object v0, Lorg/threeten/bp/ZoneOffset;->d:Lorg/threeten/bp/ZoneOffset;

    invoke-static {v1, v0}, Lorg/threeten/bp/ZoneId;->a(Ljava/lang/String;Lorg/threeten/bp/ZoneOffset;)Lorg/threeten/bp/ZoneId;

    move-result-object v0

    invoke-virtual {p1, v0}, Lorg/threeten/bp/format/DateTimeParseContext;->a(Lorg/threeten/bp/ZoneId;)V

    goto :goto_0

    .line 3560
    :cond_1
    sget-object v3, Lorg/threeten/bp/temporal/ChronoField;->D:Lorg/threeten/bp/temporal/ChronoField;

    invoke-virtual {v2, v3}, Lorg/threeten/bp/format/DateTimeParseContext;->a(Lorg/threeten/bp/temporal/TemporalField;)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    long-to-int v2, v2

    .line 3561
    invoke-static {v2}, Lorg/threeten/bp/ZoneOffset;->a(I)Lorg/threeten/bp/ZoneOffset;

    move-result-object v2

    .line 3562
    invoke-static {v1, v2}, Lorg/threeten/bp/ZoneId;->a(Ljava/lang/String;Lorg/threeten/bp/ZoneOffset;)Lorg/threeten/bp/ZoneId;

    move-result-object v1

    invoke-virtual {p1, v1}, Lorg/threeten/bp/format/DateTimeParseContext;->a(Lorg/threeten/bp/ZoneId;)V

    move p4, v0

    .line 3563
    goto :goto_0
.end method

.method private a(Ljava/util/Set;Ljava/lang/String;Z)Lorg/threeten/bp/ZoneId;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "Z)",
            "Lorg/threeten/bp/ZoneId;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 3533
    if-nez p2, :cond_1

    .line 3544
    :cond_0
    :goto_0
    return-object v1

    .line 3536
    :cond_1
    if-eqz p3, :cond_3

    .line 3537
    invoke-interface {p1, p2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-static {p2}, Lorg/threeten/bp/ZoneId;->a(Ljava/lang/String;)Lorg/threeten/bp/ZoneId;

    move-result-object v0

    :goto_1
    move-object v1, v0

    goto :goto_0

    :cond_2
    move-object v0, v1

    goto :goto_1

    .line 3539
    :cond_3
    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_4
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 3540
    invoke-virtual {v0, p2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 3541
    invoke-static {v0}, Lorg/threeten/bp/ZoneId;->a(Ljava/lang/String;)Lorg/threeten/bp/ZoneId;

    move-result-object v1

    goto :goto_0
.end method

.method private static a(Ljava/util/Set;)Lorg/threeten/bp/format/DateTimeFormatterBuilder$ZoneIdPrinterParser$SubstringTree;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lorg/threeten/bp/format/DateTimeFormatterBuilder$ZoneIdPrinterParser$SubstringTree;"
        }
    .end annotation

    .prologue
    .line 3651
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1, p0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 3652
    sget-object v0, Lorg/threeten/bp/format/DateTimeFormatterBuilder;->a:Ljava/util/Comparator;

    invoke-static {v1, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 3655
    new-instance v2, Lorg/threeten/bp/format/DateTimeFormatterBuilder$ZoneIdPrinterParser$SubstringTree;

    const/4 v0, 0x0

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    const/4 v3, 0x0

    invoke-direct {v2, v0, v3}, Lorg/threeten/bp/format/DateTimeFormatterBuilder$ZoneIdPrinterParser$SubstringTree;-><init>(ILorg/threeten/bp/format/DateTimeFormatterBuilder$1;)V

    .line 3656
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 3657
    invoke-static {v2, v0}, Lorg/threeten/bp/format/DateTimeFormatterBuilder$ZoneIdPrinterParser$SubstringTree;->a(Lorg/threeten/bp/format/DateTimeFormatterBuilder$ZoneIdPrinterParser$SubstringTree;Ljava/lang/String;)V

    goto :goto_0

    .line 3659
    :cond_0
    return-object v2
.end method


# virtual methods
.method public a(Lorg/threeten/bp/format/DateTimeParseContext;Ljava/lang/CharSequence;I)I
    .locals 8

    .prologue
    const/4 v2, 0x0

    const/16 v3, 0x54

    .line 3453
    invoke-interface {p2}, Ljava/lang/CharSequence;->length()I

    move-result v4

    .line 3454
    if-le p3, v4, :cond_0

    .line 3455
    new-instance v0, Ljava/lang/IndexOutOfBoundsException;

    invoke-direct {v0}, Ljava/lang/IndexOutOfBoundsException;-><init>()V

    throw v0

    .line 3457
    :cond_0
    if-ne p3, v4, :cond_2

    .line 3458
    xor-int/lit8 v0, p3, -0x1

    .line 3529
    :cond_1
    :goto_0
    return v0

    .line 3462
    :cond_2
    invoke-interface {p2, p3}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v5

    .line 3463
    const/16 v0, 0x2b

    if-eq v5, v0, :cond_3

    const/16 v0, 0x2d

    if-ne v5, v0, :cond_4

    .line 3464
    :cond_3
    invoke-virtual {p1}, Lorg/threeten/bp/format/DateTimeParseContext;->a()Lorg/threeten/bp/format/DateTimeParseContext;

    move-result-object v1

    .line 3465
    sget-object v0, Lorg/threeten/bp/format/DateTimeFormatterBuilder$OffsetIdPrinterParser;->b:Lorg/threeten/bp/format/DateTimeFormatterBuilder$OffsetIdPrinterParser;

    invoke-virtual {v0, v1, p2, p3}, Lorg/threeten/bp/format/DateTimeFormatterBuilder$OffsetIdPrinterParser;->a(Lorg/threeten/bp/format/DateTimeParseContext;Ljava/lang/CharSequence;I)I

    move-result v0

    .line 3466
    if-ltz v0, :cond_1

    .line 3469
    sget-object v2, Lorg/threeten/bp/temporal/ChronoField;->D:Lorg/threeten/bp/temporal/ChronoField;

    invoke-virtual {v1, v2}, Lorg/threeten/bp/format/DateTimeParseContext;->a(Lorg/threeten/bp/temporal/TemporalField;)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    long-to-int v1, v2

    .line 3470
    invoke-static {v1}, Lorg/threeten/bp/ZoneOffset;->a(I)Lorg/threeten/bp/ZoneOffset;

    move-result-object v1

    .line 3471
    invoke-virtual {p1, v1}, Lorg/threeten/bp/format/DateTimeParseContext;->a(Lorg/threeten/bp/ZoneId;)V

    goto :goto_0

    .line 3473
    :cond_4
    add-int/lit8 v0, p3, 0x2

    if-lt v4, v0, :cond_7

    .line 3474
    add-int/lit8 v0, p3, 0x1

    invoke-interface {p2, v0}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v0

    .line 3475
    const/16 v1, 0x55

    invoke-virtual {p1, v5, v1}, Lorg/threeten/bp/format/DateTimeParseContext;->a(CC)Z

    move-result v1

    if-eqz v1, :cond_6

    invoke-virtual {p1, v0, v3}, Lorg/threeten/bp/format/DateTimeParseContext;->a(CC)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 3477
    add-int/lit8 v0, p3, 0x3

    if-lt v4, v0, :cond_5

    add-int/lit8 v0, p3, 0x2

    invoke-interface {p2, v0}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v0

    const/16 v1, 0x43

    invoke-virtual {p1, v0, v1}, Lorg/threeten/bp/format/DateTimeParseContext;->a(CC)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 3479
    add-int/lit8 v0, p3, 0x3

    invoke-direct {p0, p1, p2, p3, v0}, Lorg/threeten/bp/format/DateTimeFormatterBuilder$ZoneIdPrinterParser;->a(Lorg/threeten/bp/format/DateTimeParseContext;Ljava/lang/CharSequence;II)I

    move-result v0

    goto :goto_0

    .line 3481
    :cond_5
    add-int/lit8 v0, p3, 0x2

    invoke-direct {p0, p1, p2, p3, v0}, Lorg/threeten/bp/format/DateTimeFormatterBuilder$ZoneIdPrinterParser;->a(Lorg/threeten/bp/format/DateTimeParseContext;Ljava/lang/CharSequence;II)I

    move-result v0

    goto :goto_0

    .line 3482
    :cond_6
    const/16 v1, 0x47

    invoke-virtual {p1, v5, v1}, Lorg/threeten/bp/format/DateTimeParseContext;->a(CC)Z

    move-result v1

    if-eqz v1, :cond_7

    add-int/lit8 v1, p3, 0x3

    if-lt v4, v1, :cond_7

    const/16 v1, 0x4d

    invoke-virtual {p1, v0, v1}, Lorg/threeten/bp/format/DateTimeParseContext;->a(CC)Z

    move-result v0

    if-eqz v0, :cond_7

    add-int/lit8 v0, p3, 0x2

    invoke-interface {p2, v0}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v0

    invoke-virtual {p1, v0, v3}, Lorg/threeten/bp/format/DateTimeParseContext;->a(CC)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 3486
    add-int/lit8 v0, p3, 0x3

    invoke-direct {p0, p1, p2, p3, v0}, Lorg/threeten/bp/format/DateTimeFormatterBuilder$ZoneIdPrinterParser;->a(Lorg/threeten/bp/format/DateTimeParseContext;Ljava/lang/CharSequence;II)I

    move-result v0

    goto/16 :goto_0

    .line 3491
    :cond_7
    invoke-static {}, Lorg/threeten/bp/zone/ZoneRulesProvider;->b()Ljava/util/Set;

    move-result-object v6

    .line 3492
    invoke-interface {v6}, Ljava/util/Set;->size()I

    move-result v3

    .line 3493
    sget-object v1, Lorg/threeten/bp/format/DateTimeFormatterBuilder$ZoneIdPrinterParser;->c:Ljava/util/Map$Entry;

    .line 3494
    if-eqz v1, :cond_8

    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-eq v0, v3, :cond_10

    .line 3495
    :cond_8
    monitor-enter p0

    .line 3496
    :try_start_0
    sget-object v1, Lorg/threeten/bp/format/DateTimeFormatterBuilder$ZoneIdPrinterParser;->c:Ljava/util/Map$Entry;

    .line 3497
    if-eqz v1, :cond_9

    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-eq v0, v3, :cond_f

    .line 3498
    :cond_9
    new-instance v0, Ljava/util/AbstractMap$SimpleImmutableEntry;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v6}, Lorg/threeten/bp/format/DateTimeFormatterBuilder$ZoneIdPrinterParser;->a(Ljava/util/Set;)Lorg/threeten/bp/format/DateTimeFormatterBuilder$ZoneIdPrinterParser$SubstringTree;

    move-result-object v3

    invoke-direct {v0, v1, v3}, Ljava/util/AbstractMap$SimpleImmutableEntry;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    sput-object v0, Lorg/threeten/bp/format/DateTimeFormatterBuilder$ZoneIdPrinterParser;->c:Ljava/util/Map$Entry;

    .line 3500
    :goto_1
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 3502
    :goto_2
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/threeten/bp/format/DateTimeFormatterBuilder$ZoneIdPrinterParser$SubstringTree;

    move-object v1, v2

    move-object v3, v0

    .line 3507
    :goto_3
    if-eqz v3, :cond_a

    .line 3508
    iget v0, v3, Lorg/threeten/bp/format/DateTimeFormatterBuilder$ZoneIdPrinterParser$SubstringTree;->a:I

    .line 3509
    add-int v7, p3, v0

    if-le v7, v4, :cond_b

    .line 3516
    :cond_a
    invoke-virtual {p1}, Lorg/threeten/bp/format/DateTimeParseContext;->e()Z

    move-result v0

    invoke-direct {p0, v6, v2, v0}, Lorg/threeten/bp/format/DateTimeFormatterBuilder$ZoneIdPrinterParser;->a(Ljava/util/Set;Ljava/lang/String;Z)Lorg/threeten/bp/ZoneId;

    move-result-object v0

    .line 3517
    if-nez v0, :cond_d

    .line 3518
    invoke-virtual {p1}, Lorg/threeten/bp/format/DateTimeParseContext;->e()Z

    move-result v0

    invoke-direct {p0, v6, v1, v0}, Lorg/threeten/bp/format/DateTimeFormatterBuilder$ZoneIdPrinterParser;->a(Ljava/util/Set;Ljava/lang/String;Z)Lorg/threeten/bp/ZoneId;

    move-result-object v0

    .line 3519
    if-nez v0, :cond_e

    .line 3520
    const/16 v0, 0x5a

    invoke-virtual {p1, v5, v0}, Lorg/threeten/bp/format/DateTimeParseContext;->a(CC)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 3521
    sget-object v0, Lorg/threeten/bp/ZoneOffset;->d:Lorg/threeten/bp/ZoneOffset;

    invoke-virtual {p1, v0}, Lorg/threeten/bp/format/DateTimeParseContext;->a(Lorg/threeten/bp/ZoneId;)V

    .line 3522
    add-int/lit8 v0, p3, 0x1

    goto/16 :goto_0

    .line 3500
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 3513
    :cond_b
    add-int/2addr v0, p3

    invoke-interface {p2, p3, v0}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 3514
    invoke-virtual {p1}, Lorg/threeten/bp/format/DateTimeParseContext;->e()Z

    move-result v1

    invoke-static {v3, v0, v1}, Lorg/threeten/bp/format/DateTimeFormatterBuilder$ZoneIdPrinterParser$SubstringTree;->a(Lorg/threeten/bp/format/DateTimeFormatterBuilder$ZoneIdPrinterParser$SubstringTree;Ljava/lang/CharSequence;Z)Lorg/threeten/bp/format/DateTimeFormatterBuilder$ZoneIdPrinterParser$SubstringTree;

    move-result-object v1

    move-object v3, v1

    move-object v1, v2

    move-object v2, v0

    .line 3515
    goto :goto_3

    .line 3524
    :cond_c
    xor-int/lit8 v0, p3, -0x1

    goto/16 :goto_0

    :cond_d
    move-object v1, v2

    .line 3528
    :cond_e
    invoke-virtual {p1, v0}, Lorg/threeten/bp/format/DateTimeParseContext;->a(Lorg/threeten/bp/ZoneId;)V

    .line 3529
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v0

    add-int/2addr v0, p3

    goto/16 :goto_0

    :cond_f
    move-object v0, v1

    goto :goto_1

    :cond_10
    move-object v0, v1

    goto :goto_2
.end method

.method public a(Lorg/threeten/bp/format/DateTimePrintContext;Ljava/lang/StringBuilder;)Z
    .locals 1

    .prologue
    .line 3427
    iget-object v0, p0, Lorg/threeten/bp/format/DateTimeFormatterBuilder$ZoneIdPrinterParser;->a:Lorg/threeten/bp/temporal/TemporalQuery;

    invoke-virtual {p1, v0}, Lorg/threeten/bp/format/DateTimePrintContext;->a(Lorg/threeten/bp/temporal/TemporalQuery;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/threeten/bp/ZoneId;

    .line 3428
    if-nez v0, :cond_0

    .line 3429
    const/4 v0, 0x0

    .line 3432
    :goto_0
    return v0

    .line 3431
    :cond_0
    invoke-virtual {v0}, Lorg/threeten/bp/ZoneId;->c()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 3432
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 3665
    iget-object v0, p0, Lorg/threeten/bp/format/DateTimeFormatterBuilder$ZoneIdPrinterParser;->b:Ljava/lang/String;

    return-object v0
.end method
