.class Lorg/threeten/bp/format/DateTimeFormatterBuilder$2;
.super Lorg/threeten/bp/format/DateTimeTextProvider;
.source "DateTimeFormatterBuilder.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/threeten/bp/format/DateTimeFormatterBuilder;->a(Lorg/threeten/bp/temporal/TemporalField;Ljava/util/Map;)Lorg/threeten/bp/format/DateTimeFormatterBuilder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lorg/threeten/bp/format/SimpleDateTimeTextProvider$LocaleStore;

.field final synthetic b:Lorg/threeten/bp/format/DateTimeFormatterBuilder;


# direct methods
.method constructor <init>(Lorg/threeten/bp/format/DateTimeFormatterBuilder;Lorg/threeten/bp/format/SimpleDateTimeTextProvider$LocaleStore;)V
    .locals 0

    .prologue
    .line 727
    iput-object p1, p0, Lorg/threeten/bp/format/DateTimeFormatterBuilder$2;->b:Lorg/threeten/bp/format/DateTimeFormatterBuilder;

    iput-object p2, p0, Lorg/threeten/bp/format/DateTimeFormatterBuilder$2;->a:Lorg/threeten/bp/format/SimpleDateTimeTextProvider$LocaleStore;

    invoke-direct {p0}, Lorg/threeten/bp/format/DateTimeTextProvider;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lorg/threeten/bp/temporal/TemporalField;JLorg/threeten/bp/format/TextStyle;Ljava/util/Locale;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 730
    iget-object v0, p0, Lorg/threeten/bp/format/DateTimeFormatterBuilder$2;->a:Lorg/threeten/bp/format/SimpleDateTimeTextProvider$LocaleStore;

    invoke-virtual {v0, p2, p3, p4}, Lorg/threeten/bp/format/SimpleDateTimeTextProvider$LocaleStore;->a(JLorg/threeten/bp/format/TextStyle;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public a(Lorg/threeten/bp/temporal/TemporalField;Lorg/threeten/bp/format/TextStyle;Ljava/util/Locale;)Ljava/util/Iterator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/threeten/bp/temporal/TemporalField;",
            "Lorg/threeten/bp/format/TextStyle;",
            "Ljava/util/Locale;",
            ")",
            "Ljava/util/Iterator",
            "<",
            "Ljava/util/Map$Entry",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Long;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 734
    iget-object v0, p0, Lorg/threeten/bp/format/DateTimeFormatterBuilder$2;->a:Lorg/threeten/bp/format/SimpleDateTimeTextProvider$LocaleStore;

    invoke-virtual {v0, p2}, Lorg/threeten/bp/format/SimpleDateTimeTextProvider$LocaleStore;->a(Lorg/threeten/bp/format/TextStyle;)Ljava/util/Iterator;

    move-result-object v0

    return-object v0
.end method
