.class final Lorg/threeten/bp/format/DateTimeFormatterBuilder$InstantPrinterParser;
.super Ljava/lang/Object;
.source "DateTimeFormatterBuilder.java"

# interfaces
.implements Lorg/threeten/bp/format/DateTimeFormatterBuilder$DateTimePrinterParser;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/threeten/bp/format/DateTimeFormatterBuilder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "InstantPrinterParser"
.end annotation


# instance fields
.field private final a:I


# direct methods
.method constructor <init>(I)V
    .locals 0

    .prologue
    .line 2921
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2922
    iput p1, p0, Lorg/threeten/bp/format/DateTimeFormatterBuilder$InstantPrinterParser;->a:I

    .line 2923
    return-void
.end method


# virtual methods
.method public a(Lorg/threeten/bp/format/DateTimeParseContext;Ljava/lang/CharSequence;I)I
    .locals 12

    .prologue
    .line 3001
    invoke-virtual {p1}, Lorg/threeten/bp/format/DateTimeParseContext;->a()Lorg/threeten/bp/format/DateTimeParseContext;

    move-result-object v3

    .line 3002
    iget v0, p0, Lorg/threeten/bp/format/DateTimeFormatterBuilder$InstantPrinterParser;->a:I

    if-gez v0, :cond_0

    const/4 v0, 0x0

    .line 3003
    :goto_0
    iget v1, p0, Lorg/threeten/bp/format/DateTimeFormatterBuilder$InstantPrinterParser;->a:I

    if-gez v1, :cond_1

    const/16 v1, 0x9

    .line 3004
    :goto_1
    new-instance v2, Lorg/threeten/bp/format/DateTimeFormatterBuilder;

    invoke-direct {v2}, Lorg/threeten/bp/format/DateTimeFormatterBuilder;-><init>()V

    sget-object v4, Lorg/threeten/bp/format/DateTimeFormatter;->a:Lorg/threeten/bp/format/DateTimeFormatter;

    invoke-virtual {v2, v4}, Lorg/threeten/bp/format/DateTimeFormatterBuilder;->a(Lorg/threeten/bp/format/DateTimeFormatter;)Lorg/threeten/bp/format/DateTimeFormatterBuilder;

    move-result-object v2

    const/16 v4, 0x54

    invoke-virtual {v2, v4}, Lorg/threeten/bp/format/DateTimeFormatterBuilder;->a(C)Lorg/threeten/bp/format/DateTimeFormatterBuilder;

    move-result-object v2

    sget-object v4, Lorg/threeten/bp/temporal/ChronoField;->m:Lorg/threeten/bp/temporal/ChronoField;

    const/4 v5, 0x2

    invoke-virtual {v2, v4, v5}, Lorg/threeten/bp/format/DateTimeFormatterBuilder;->a(Lorg/threeten/bp/temporal/TemporalField;I)Lorg/threeten/bp/format/DateTimeFormatterBuilder;

    move-result-object v2

    const/16 v4, 0x3a

    invoke-virtual {v2, v4}, Lorg/threeten/bp/format/DateTimeFormatterBuilder;->a(C)Lorg/threeten/bp/format/DateTimeFormatterBuilder;

    move-result-object v2

    sget-object v4, Lorg/threeten/bp/temporal/ChronoField;->i:Lorg/threeten/bp/temporal/ChronoField;

    const/4 v5, 0x2

    invoke-virtual {v2, v4, v5}, Lorg/threeten/bp/format/DateTimeFormatterBuilder;->a(Lorg/threeten/bp/temporal/TemporalField;I)Lorg/threeten/bp/format/DateTimeFormatterBuilder;

    move-result-object v2

    const/16 v4, 0x3a

    invoke-virtual {v2, v4}, Lorg/threeten/bp/format/DateTimeFormatterBuilder;->a(C)Lorg/threeten/bp/format/DateTimeFormatterBuilder;

    move-result-object v2

    sget-object v4, Lorg/threeten/bp/temporal/ChronoField;->g:Lorg/threeten/bp/temporal/ChronoField;

    const/4 v5, 0x2

    invoke-virtual {v2, v4, v5}, Lorg/threeten/bp/format/DateTimeFormatterBuilder;->a(Lorg/threeten/bp/temporal/TemporalField;I)Lorg/threeten/bp/format/DateTimeFormatterBuilder;

    move-result-object v2

    sget-object v4, Lorg/threeten/bp/temporal/ChronoField;->a:Lorg/threeten/bp/temporal/ChronoField;

    const/4 v5, 0x1

    invoke-virtual {v2, v4, v0, v1, v5}, Lorg/threeten/bp/format/DateTimeFormatterBuilder;->a(Lorg/threeten/bp/temporal/TemporalField;IIZ)Lorg/threeten/bp/format/DateTimeFormatterBuilder;

    move-result-object v0

    const/16 v1, 0x5a

    invoke-virtual {v0, v1}, Lorg/threeten/bp/format/DateTimeFormatterBuilder;->a(C)Lorg/threeten/bp/format/DateTimeFormatterBuilder;

    move-result-object v0

    invoke-virtual {v0}, Lorg/threeten/bp/format/DateTimeFormatterBuilder;->j()Lorg/threeten/bp/format/DateTimeFormatter;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lorg/threeten/bp/format/DateTimeFormatter;->a(Z)Lorg/threeten/bp/format/DateTimeFormatterBuilder$CompositePrinterParser;

    move-result-object v0

    .line 3009
    invoke-virtual {v0, v3, p2, p3}, Lorg/threeten/bp/format/DateTimeFormatterBuilder$CompositePrinterParser;->a(Lorg/threeten/bp/format/DateTimeParseContext;Ljava/lang/CharSequence;I)I

    move-result v8

    .line 3010
    if-gez v8, :cond_2

    move v0, v8

    .line 3043
    :goto_2
    return v0

    .line 3002
    :cond_0
    iget v0, p0, Lorg/threeten/bp/format/DateTimeFormatterBuilder$InstantPrinterParser;->a:I

    goto :goto_0

    .line 3003
    :cond_1
    iget v1, p0, Lorg/threeten/bp/format/DateTimeFormatterBuilder$InstantPrinterParser;->a:I

    goto :goto_1

    .line 3015
    :cond_2
    sget-object v0, Lorg/threeten/bp/temporal/ChronoField;->A:Lorg/threeten/bp/temporal/ChronoField;

    invoke-virtual {v3, v0}, Lorg/threeten/bp/format/DateTimeParseContext;->a(Lorg/threeten/bp/temporal/TemporalField;)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v10

    .line 3016
    sget-object v0, Lorg/threeten/bp/temporal/ChronoField;->x:Lorg/threeten/bp/temporal/ChronoField;

    invoke-virtual {v3, v0}, Lorg/threeten/bp/format/DateTimeParseContext;->a(Lorg/threeten/bp/temporal/TemporalField;)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->intValue()I

    move-result v1

    .line 3017
    sget-object v0, Lorg/threeten/bp/temporal/ChronoField;->s:Lorg/threeten/bp/temporal/ChronoField;

    invoke-virtual {v3, v0}, Lorg/threeten/bp/format/DateTimeParseContext;->a(Lorg/threeten/bp/temporal/TemporalField;)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->intValue()I

    move-result v2

    .line 3018
    sget-object v0, Lorg/threeten/bp/temporal/ChronoField;->m:Lorg/threeten/bp/temporal/ChronoField;

    invoke-virtual {v3, v0}, Lorg/threeten/bp/format/DateTimeParseContext;->a(Lorg/threeten/bp/temporal/TemporalField;)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->intValue()I

    move-result v6

    .line 3019
    sget-object v0, Lorg/threeten/bp/temporal/ChronoField;->i:Lorg/threeten/bp/temporal/ChronoField;

    invoke-virtual {v3, v0}, Lorg/threeten/bp/format/DateTimeParseContext;->a(Lorg/threeten/bp/temporal/TemporalField;)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->intValue()I

    move-result v4

    .line 3020
    sget-object v0, Lorg/threeten/bp/temporal/ChronoField;->g:Lorg/threeten/bp/temporal/ChronoField;

    invoke-virtual {v3, v0}, Lorg/threeten/bp/format/DateTimeParseContext;->a(Lorg/threeten/bp/temporal/TemporalField;)Ljava/lang/Long;

    move-result-object v0

    .line 3021
    sget-object v5, Lorg/threeten/bp/temporal/ChronoField;->a:Lorg/threeten/bp/temporal/ChronoField;

    invoke-virtual {v3, v5}, Lorg/threeten/bp/format/DateTimeParseContext;->a(Lorg/threeten/bp/temporal/TemporalField;)Ljava/lang/Long;

    move-result-object v3

    .line 3022
    if-eqz v0, :cond_3

    invoke-virtual {v0}, Ljava/lang/Long;->intValue()I

    move-result v5

    .line 3023
    :goto_3
    if-eqz v3, :cond_4

    invoke-virtual {v3}, Ljava/lang/Long;->intValue()I

    move-result v0

    move v7, v0

    .line 3024
    :goto_4
    long-to-int v0, v10

    rem-int/lit16 v0, v0, 0x2710

    .line 3025
    const/4 v3, 0x0

    .line 3026
    const/16 v9, 0x18

    if-ne v6, v9, :cond_5

    if-nez v4, :cond_5

    if-nez v5, :cond_5

    if-nez v7, :cond_5

    .line 3027
    const/4 v6, 0x0

    .line 3028
    const/4 v3, 0x1

    move v9, v3

    move v3, v6

    .line 3035
    :goto_5
    const/4 v6, 0x0

    :try_start_0
    invoke-static/range {v0 .. v6}, Lorg/threeten/bp/LocalDateTime;->a(IIIIIII)Lorg/threeten/bp/LocalDateTime;

    move-result-object v0

    int-to-long v2, v9

    invoke-virtual {v0, v2, v3}, Lorg/threeten/bp/LocalDateTime;->a(J)Lorg/threeten/bp/LocalDateTime;

    move-result-object v0

    .line 3036
    sget-object v1, Lorg/threeten/bp/ZoneOffset;->d:Lorg/threeten/bp/ZoneOffset;

    invoke-virtual {v0, v1}, Lorg/threeten/bp/LocalDateTime;->c(Lorg/threeten/bp/ZoneOffset;)J

    move-result-wide v0

    .line 3037
    const-wide/16 v2, 0x2710

    div-long v2, v10, v2

    const-wide v4, 0x497968bd80L

    invoke-static {v2, v3, v4, v5}, Lorg/threeten/bp/jdk8/Jdk8Methods;->d(JJ)J
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v2

    add-long/2addr v2, v0

    .line 3042
    sget-object v1, Lorg/threeten/bp/temporal/ChronoField;->C:Lorg/threeten/bp/temporal/ChronoField;

    move-object v0, p1

    move v4, p3

    move v5, v8

    invoke-virtual/range {v0 .. v5}, Lorg/threeten/bp/format/DateTimeParseContext;->a(Lorg/threeten/bp/temporal/TemporalField;JII)I

    move-result v5

    .line 3043
    sget-object v1, Lorg/threeten/bp/temporal/ChronoField;->a:Lorg/threeten/bp/temporal/ChronoField;

    int-to-long v2, v7

    move-object v0, p1

    move v4, p3

    invoke-virtual/range {v0 .. v5}, Lorg/threeten/bp/format/DateTimeParseContext;->a(Lorg/threeten/bp/temporal/TemporalField;JII)I

    move-result v0

    goto/16 :goto_2

    .line 3022
    :cond_3
    const/4 v5, 0x0

    goto :goto_3

    .line 3023
    :cond_4
    const/4 v0, 0x0

    move v7, v0

    goto :goto_4

    .line 3029
    :cond_5
    const/16 v9, 0x17

    if-ne v6, v9, :cond_6

    const/16 v9, 0x3b

    if-ne v4, v9, :cond_6

    const/16 v9, 0x3c

    if-ne v5, v9, :cond_6

    .line 3030
    invoke-virtual {p1}, Lorg/threeten/bp/format/DateTimeParseContext;->h()V

    .line 3031
    const/16 v5, 0x3b

    move v9, v3

    move v3, v6

    goto :goto_5

    .line 3038
    :catch_0
    move-exception v0

    .line 3039
    xor-int/lit8 v0, p3, -0x1

    goto/16 :goto_2

    :cond_6
    move v9, v3

    move v3, v6

    goto :goto_5
.end method

.method public a(Lorg/threeten/bp/format/DateTimePrintContext;Ljava/lang/StringBuilder;)Z
    .locals 10

    .prologue
    .line 2928
    sget-object v0, Lorg/threeten/bp/temporal/ChronoField;->C:Lorg/threeten/bp/temporal/ChronoField;

    invoke-virtual {p1, v0}, Lorg/threeten/bp/format/DateTimePrintContext;->a(Lorg/threeten/bp/temporal/TemporalField;)Ljava/lang/Long;

    move-result-object v1

    .line 2929
    const-wide/16 v2, 0x0

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    .line 2930
    invoke-virtual {p1}, Lorg/threeten/bp/format/DateTimePrintContext;->a()Lorg/threeten/bp/temporal/TemporalAccessor;

    move-result-object v2

    sget-object v3, Lorg/threeten/bp/temporal/ChronoField;->a:Lorg/threeten/bp/temporal/ChronoField;

    invoke-interface {v2, v3}, Lorg/threeten/bp/temporal/TemporalAccessor;->a(Lorg/threeten/bp/temporal/TemporalField;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2931
    invoke-virtual {p1}, Lorg/threeten/bp/format/DateTimePrintContext;->a()Lorg/threeten/bp/temporal/TemporalAccessor;

    move-result-object v0

    sget-object v2, Lorg/threeten/bp/temporal/ChronoField;->a:Lorg/threeten/bp/temporal/ChronoField;

    invoke-interface {v0, v2}, Lorg/threeten/bp/temporal/TemporalAccessor;->d(Lorg/threeten/bp/temporal/TemporalField;)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    .line 2933
    :cond_0
    if-nez v1, :cond_1

    .line 2934
    const/4 v0, 0x0

    .line 2995
    :goto_0
    return v0

    .line 2936
    :cond_1
    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    .line 2937
    sget-object v1, Lorg/threeten/bp/temporal/ChronoField;->a:Lorg/threeten/bp/temporal/ChronoField;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lorg/threeten/bp/temporal/ChronoField;->b(J)I

    move-result v2

    .line 2938
    const-wide v0, -0xe79747c00L

    cmp-long v0, v4, v0

    if-ltz v0, :cond_5

    .line 2940
    const-wide v0, 0x497968bd80L

    sub-long v0, v4, v0

    const-wide v4, 0xe79747c00L

    add-long/2addr v0, v4

    .line 2941
    const-wide v4, 0x497968bd80L

    invoke-static {v0, v1, v4, v5}, Lorg/threeten/bp/jdk8/Jdk8Methods;->e(JJ)J

    move-result-wide v4

    const-wide/16 v6, 0x1

    add-long/2addr v4, v6

    .line 2942
    const-wide v6, 0x497968bd80L

    invoke-static {v0, v1, v6, v7}, Lorg/threeten/bp/jdk8/Jdk8Methods;->f(JJ)J

    move-result-wide v0

    .line 2943
    const-wide v6, 0xe79747c00L

    sub-long/2addr v0, v6

    const/4 v3, 0x0

    sget-object v6, Lorg/threeten/bp/ZoneOffset;->d:Lorg/threeten/bp/ZoneOffset;

    invoke-static {v0, v1, v3, v6}, Lorg/threeten/bp/LocalDateTime;->a(JILorg/threeten/bp/ZoneOffset;)Lorg/threeten/bp/LocalDateTime;

    move-result-object v0

    .line 2944
    const-wide/16 v6, 0x0

    cmp-long v1, v4, v6

    if-lez v1, :cond_2

    .line 2945
    const/16 v1, 0x2b

    invoke-virtual {p2, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 2947
    :cond_2
    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 2948
    invoke-virtual {v0}, Lorg/threeten/bp/LocalDateTime;->h()I

    move-result v0

    if-nez v0, :cond_3

    .line 2949
    const-string v0, ":00"

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2973
    :cond_3
    :goto_1
    iget v0, p0, Lorg/threeten/bp/format/DateTimeFormatterBuilder$InstantPrinterParser;->a:I

    const/4 v1, -0x2

    if-ne v0, v1, :cond_b

    .line 2974
    if-eqz v2, :cond_4

    .line 2975
    const/16 v0, 0x2e

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 2976
    const v0, 0xf4240

    rem-int v0, v2, v0

    if-nez v0, :cond_9

    .line 2977
    const v0, 0xf4240

    div-int v0, v2, v0

    add-int/lit16 v0, v0, 0x3e8

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2994
    :cond_4
    :goto_2
    const/16 v0, 0x5a

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 2995
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 2953
    :cond_5
    const-wide v0, 0xe79747c00L

    add-long/2addr v0, v4

    .line 2954
    const-wide v4, 0x497968bd80L

    div-long v4, v0, v4

    .line 2955
    const-wide v6, 0x497968bd80L

    rem-long/2addr v0, v6

    .line 2956
    const-wide v6, 0xe79747c00L

    sub-long v6, v0, v6

    const/4 v3, 0x0

    sget-object v8, Lorg/threeten/bp/ZoneOffset;->d:Lorg/threeten/bp/ZoneOffset;

    invoke-static {v6, v7, v3, v8}, Lorg/threeten/bp/LocalDateTime;->a(JILorg/threeten/bp/ZoneOffset;)Lorg/threeten/bp/LocalDateTime;

    move-result-object v3

    .line 2957
    invoke-virtual {p2}, Ljava/lang/StringBuilder;->length()I

    move-result v6

    .line 2958
    invoke-virtual {p2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 2959
    invoke-virtual {v3}, Lorg/threeten/bp/LocalDateTime;->h()I

    move-result v7

    if-nez v7, :cond_6

    .line 2960
    const-string v7, ":00"

    invoke-virtual {p2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2962
    :cond_6
    const-wide/16 v8, 0x0

    cmp-long v7, v4, v8

    if-gez v7, :cond_3

    .line 2963
    invoke-virtual {v3}, Lorg/threeten/bp/LocalDateTime;->b()I

    move-result v3

    const/16 v7, -0x2710

    if-ne v3, v7, :cond_7

    .line 2964
    add-int/lit8 v0, v6, 0x2

    const-wide/16 v8, 0x1

    sub-long/2addr v4, v8

    invoke-static {v4, v5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v6, v0, v1}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 2965
    :cond_7
    const-wide/16 v8, 0x0

    cmp-long v0, v0, v8

    if-nez v0, :cond_8

    .line 2966
    invoke-virtual {p2, v6, v4, v5}, Ljava/lang/StringBuilder;->insert(IJ)Ljava/lang/StringBuilder;

    goto/16 :goto_1

    .line 2968
    :cond_8
    add-int/lit8 v0, v6, 0x1

    invoke-static {v4, v5}, Ljava/lang/Math;->abs(J)J

    move-result-wide v4

    invoke-virtual {p2, v0, v4, v5}, Ljava/lang/StringBuilder;->insert(IJ)Ljava/lang/StringBuilder;

    goto/16 :goto_1

    .line 2978
    :cond_9
    rem-int/lit16 v0, v2, 0x3e8

    if-nez v0, :cond_a

    .line 2979
    div-int/lit16 v0, v2, 0x3e8

    const v1, 0xf4240

    add-int/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_2

    .line 2981
    :cond_a
    const v0, 0x3b9aca00

    add-int/2addr v0, v2

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_2

    .line 2984
    :cond_b
    iget v0, p0, Lorg/threeten/bp/format/DateTimeFormatterBuilder$InstantPrinterParser;->a:I

    if-gtz v0, :cond_c

    iget v0, p0, Lorg/threeten/bp/format/DateTimeFormatterBuilder$InstantPrinterParser;->a:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_4

    if-lez v2, :cond_4

    .line 2985
    :cond_c
    const/16 v0, 0x2e

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 2986
    const v1, 0x5f5e100

    .line 2987
    const/4 v0, 0x0

    :goto_3
    iget v3, p0, Lorg/threeten/bp/format/DateTimeFormatterBuilder$InstantPrinterParser;->a:I

    const/4 v4, -0x1

    if-ne v3, v4, :cond_d

    if-gtz v2, :cond_e

    :cond_d
    iget v3, p0, Lorg/threeten/bp/format/DateTimeFormatterBuilder$InstantPrinterParser;->a:I

    if-ge v0, v3, :cond_4

    .line 2988
    :cond_e
    div-int v3, v2, v1

    .line 2989
    add-int/lit8 v4, v3, 0x30

    int-to-char v4, v4

    invoke-virtual {p2, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 2990
    mul-int/2addr v3, v1

    sub-int/2addr v2, v3

    .line 2991
    div-int/lit8 v1, v1, 0xa

    .line 2987
    add-int/lit8 v0, v0, 0x1

    goto :goto_3
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 3048
    const-string v0, "Instant()"

    return-object v0
.end method
