.class final Lorg/threeten/bp/format/DateTimePrintContext;
.super Ljava/lang/Object;
.source "DateTimePrintContext.java"


# instance fields
.field private a:Lorg/threeten/bp/temporal/TemporalAccessor;

.field private b:Ljava/util/Locale;

.field private c:Lorg/threeten/bp/format/DecimalStyle;

.field private d:I


# direct methods
.method constructor <init>(Lorg/threeten/bp/temporal/TemporalAccessor;Lorg/threeten/bp/format/DateTimeFormatter;)V
    .locals 1

    .prologue
    .line 91
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 92
    invoke-static {p1, p2}, Lorg/threeten/bp/format/DateTimePrintContext;->a(Lorg/threeten/bp/temporal/TemporalAccessor;Lorg/threeten/bp/format/DateTimeFormatter;)Lorg/threeten/bp/temporal/TemporalAccessor;

    move-result-object v0

    iput-object v0, p0, Lorg/threeten/bp/format/DateTimePrintContext;->a:Lorg/threeten/bp/temporal/TemporalAccessor;

    .line 93
    invoke-virtual {p2}, Lorg/threeten/bp/format/DateTimeFormatter;->a()Ljava/util/Locale;

    move-result-object v0

    iput-object v0, p0, Lorg/threeten/bp/format/DateTimePrintContext;->b:Ljava/util/Locale;

    .line 94
    invoke-virtual {p2}, Lorg/threeten/bp/format/DateTimeFormatter;->b()Lorg/threeten/bp/format/DecimalStyle;

    move-result-object v0

    iput-object v0, p0, Lorg/threeten/bp/format/DateTimePrintContext;->c:Lorg/threeten/bp/format/DecimalStyle;

    .line 95
    return-void
.end method

.method private static a(Lorg/threeten/bp/temporal/TemporalAccessor;Lorg/threeten/bp/format/DateTimeFormatter;)Lorg/threeten/bp/temporal/TemporalAccessor;
    .locals 9

    .prologue
    const/4 v3, 0x0

    .line 106
    invoke-virtual {p1}, Lorg/threeten/bp/format/DateTimeFormatter;->c()Lorg/threeten/bp/chrono/Chronology;

    move-result-object v2

    .line 107
    invoke-virtual {p1}, Lorg/threeten/bp/format/DateTimeFormatter;->d()Lorg/threeten/bp/ZoneId;

    move-result-object v6

    .line 108
    if-nez v2, :cond_1

    if-nez v6, :cond_1

    .line 161
    :cond_0
    :goto_0
    return-object p0

    .line 113
    :cond_1
    invoke-static {}, Lorg/threeten/bp/temporal/TemporalQueries;->b()Lorg/threeten/bp/temporal/TemporalQuery;

    move-result-object v0

    invoke-interface {p0, v0}, Lorg/threeten/bp/temporal/TemporalAccessor;->a(Lorg/threeten/bp/temporal/TemporalQuery;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/threeten/bp/chrono/Chronology;

    .line 114
    invoke-static {}, Lorg/threeten/bp/temporal/TemporalQueries;->a()Lorg/threeten/bp/temporal/TemporalQuery;

    move-result-object v1

    invoke-interface {p0, v1}, Lorg/threeten/bp/temporal/TemporalAccessor;->a(Lorg/threeten/bp/temporal/TemporalQuery;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/threeten/bp/ZoneId;

    .line 115
    invoke-static {v0, v2}, Lorg/threeten/bp/jdk8/Jdk8Methods;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    move-object v2, v3

    .line 118
    :cond_2
    invoke-static {v1, v6}, Lorg/threeten/bp/jdk8/Jdk8Methods;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    move-object v6, v3

    .line 121
    :cond_3
    if-nez v2, :cond_4

    if-eqz v6, :cond_0

    .line 124
    :cond_4
    if-eqz v2, :cond_5

    move-object v4, v2

    .line 125
    :goto_1
    if-eqz v6, :cond_6

    move-object v5, v6

    .line 128
    :goto_2
    if-eqz v6, :cond_9

    .line 130
    sget-object v1, Lorg/threeten/bp/temporal/ChronoField;->C:Lorg/threeten/bp/temporal/ChronoField;

    invoke-interface {p0, v1}, Lorg/threeten/bp/temporal/TemporalAccessor;->a(Lorg/threeten/bp/temporal/TemporalField;)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 131
    if-eqz v4, :cond_7

    move-object v0, v4

    .line 132
    :goto_3
    invoke-static {p0}, Lorg/threeten/bp/Instant;->a(Lorg/threeten/bp/temporal/TemporalAccessor;)Lorg/threeten/bp/Instant;

    move-result-object v1

    invoke-virtual {v0, v1, v6}, Lorg/threeten/bp/chrono/Chronology;->a(Lorg/threeten/bp/Instant;Lorg/threeten/bp/ZoneId;)Lorg/threeten/bp/chrono/ChronoZonedDateTime;

    move-result-object p0

    goto :goto_0

    :cond_5
    move-object v4, v0

    .line 124
    goto :goto_1

    :cond_6
    move-object v5, v1

    .line 125
    goto :goto_2

    .line 131
    :cond_7
    sget-object v0, Lorg/threeten/bp/chrono/IsoChronology;->b:Lorg/threeten/bp/chrono/IsoChronology;

    goto :goto_3

    .line 135
    :cond_8
    invoke-virtual {v6}, Lorg/threeten/bp/ZoneId;->e()Lorg/threeten/bp/ZoneId;

    move-result-object v7

    .line 136
    invoke-static {}, Lorg/threeten/bp/temporal/TemporalQueries;->e()Lorg/threeten/bp/temporal/TemporalQuery;

    move-result-object v1

    invoke-interface {p0, v1}, Lorg/threeten/bp/temporal/TemporalAccessor;->a(Lorg/threeten/bp/temporal/TemporalQuery;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/threeten/bp/ZoneOffset;

    .line 137
    instance-of v8, v7, Lorg/threeten/bp/ZoneOffset;

    if-eqz v8, :cond_9

    if-eqz v1, :cond_9

    invoke-virtual {v7, v1}, Lorg/threeten/bp/ZoneId;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_9

    .line 138
    new-instance v0, Lorg/threeten/bp/DateTimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Invalid override zone for temporal: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/threeten/bp/DateTimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 142
    :cond_9
    if-eqz v2, :cond_a

    .line 143
    sget-object v1, Lorg/threeten/bp/temporal/ChronoField;->u:Lorg/threeten/bp/temporal/ChronoField;

    invoke-interface {p0, v1}, Lorg/threeten/bp/temporal/TemporalAccessor;->a(Lorg/threeten/bp/temporal/TemporalField;)Z

    move-result v1

    if-eqz v1, :cond_b

    .line 144
    invoke-virtual {v4, p0}, Lorg/threeten/bp/chrono/Chronology;->b(Lorg/threeten/bp/temporal/TemporalAccessor;)Lorg/threeten/bp/chrono/ChronoLocalDate;

    move-result-object v3

    .line 161
    :cond_a
    new-instance v0, Lorg/threeten/bp/format/DateTimePrintContext$1;

    invoke-direct {v0, v3, p0, v4, v5}, Lorg/threeten/bp/format/DateTimePrintContext$1;-><init>(Lorg/threeten/bp/chrono/ChronoLocalDate;Lorg/threeten/bp/temporal/TemporalAccessor;Lorg/threeten/bp/chrono/Chronology;Lorg/threeten/bp/ZoneId;)V

    move-object p0, v0

    goto/16 :goto_0

    .line 147
    :cond_b
    sget-object v1, Lorg/threeten/bp/chrono/IsoChronology;->b:Lorg/threeten/bp/chrono/IsoChronology;

    if-ne v2, v1, :cond_c

    if-eqz v0, :cond_a

    .line 148
    :cond_c
    invoke-static {}, Lorg/threeten/bp/temporal/ChronoField;->values()[Lorg/threeten/bp/temporal/ChronoField;

    move-result-object v1

    array-length v6, v1

    const/4 v0, 0x0

    :goto_4
    if-ge v0, v6, :cond_a

    aget-object v7, v1, v0

    .line 149
    invoke-virtual {v7}, Lorg/threeten/bp/temporal/ChronoField;->b()Z

    move-result v8

    if-eqz v8, :cond_d

    invoke-interface {p0, v7}, Lorg/threeten/bp/temporal/TemporalAccessor;->a(Lorg/threeten/bp/temporal/TemporalField;)Z

    move-result v7

    if-eqz v7, :cond_d

    .line 150
    new-instance v0, Lorg/threeten/bp/DateTimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Invalid override chronology for temporal: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/threeten/bp/DateTimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 148
    :cond_d
    add-int/lit8 v0, v0, 0x1

    goto :goto_4
.end method


# virtual methods
.method a(Lorg/threeten/bp/temporal/TemporalField;)Ljava/lang/Long;
    .locals 2

    .prologue
    .line 274
    :try_start_0
    iget-object v0, p0, Lorg/threeten/bp/format/DateTimePrintContext;->a:Lorg/threeten/bp/temporal/TemporalAccessor;

    invoke-interface {v0, p1}, Lorg/threeten/bp/temporal/TemporalAccessor;->d(Lorg/threeten/bp/temporal/TemporalField;)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;
    :try_end_0
    .catch Lorg/threeten/bp/DateTimeException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 277
    :goto_0
    return-object v0

    .line 275
    :catch_0
    move-exception v0

    .line 276
    iget v1, p0, Lorg/threeten/bp/format/DateTimePrintContext;->d:I

    if-lez v1, :cond_0

    .line 277
    const/4 v0, 0x0

    goto :goto_0

    .line 279
    :cond_0
    throw v0
.end method

.method a(Lorg/threeten/bp/temporal/TemporalQuery;)Ljava/lang/Object;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<R:",
            "Ljava/lang/Object;",
            ">(",
            "Lorg/threeten/bp/temporal/TemporalQuery",
            "<TR;>;)TR;"
        }
    .end annotation

    .prologue
    .line 256
    iget-object v0, p0, Lorg/threeten/bp/format/DateTimePrintContext;->a:Lorg/threeten/bp/temporal/TemporalAccessor;

    invoke-interface {v0, p1}, Lorg/threeten/bp/temporal/TemporalAccessor;->a(Lorg/threeten/bp/temporal/TemporalQuery;)Ljava/lang/Object;

    move-result-object v0

    .line 257
    if-nez v0, :cond_0

    iget v1, p0, Lorg/threeten/bp/format/DateTimePrintContext;->d:I

    if-nez v1, :cond_0

    .line 258
    new-instance v0, Lorg/threeten/bp/DateTimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unable to extract value: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lorg/threeten/bp/format/DateTimePrintContext;->a:Lorg/threeten/bp/temporal/TemporalAccessor;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/threeten/bp/DateTimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 260
    :cond_0
    return-object v0
.end method

.method a()Lorg/threeten/bp/temporal/TemporalAccessor;
    .locals 1

    .prologue
    .line 207
    iget-object v0, p0, Lorg/threeten/bp/format/DateTimePrintContext;->a:Lorg/threeten/bp/temporal/TemporalAccessor;

    return-object v0
.end method

.method b()Ljava/util/Locale;
    .locals 1

    .prologue
    .line 219
    iget-object v0, p0, Lorg/threeten/bp/format/DateTimePrintContext;->b:Ljava/util/Locale;

    return-object v0
.end method

.method c()Lorg/threeten/bp/format/DecimalStyle;
    .locals 1

    .prologue
    .line 230
    iget-object v0, p0, Lorg/threeten/bp/format/DateTimePrintContext;->c:Lorg/threeten/bp/format/DecimalStyle;

    return-object v0
.end method

.method d()V
    .locals 1

    .prologue
    .line 238
    iget v0, p0, Lorg/threeten/bp/format/DateTimePrintContext;->d:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lorg/threeten/bp/format/DateTimePrintContext;->d:I

    .line 239
    return-void
.end method

.method e()V
    .locals 1

    .prologue
    .line 245
    iget v0, p0, Lorg/threeten/bp/format/DateTimePrintContext;->d:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lorg/threeten/bp/format/DateTimePrintContext;->d:I

    .line 246
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 291
    iget-object v0, p0, Lorg/threeten/bp/format/DateTimePrintContext;->a:Lorg/threeten/bp/temporal/TemporalAccessor;

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
