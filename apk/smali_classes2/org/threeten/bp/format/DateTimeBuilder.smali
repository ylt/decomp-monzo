.class final Lorg/threeten/bp/format/DateTimeBuilder;
.super Lorg/threeten/bp/jdk8/DefaultInterfaceTemporalAccessor;
.source "DateTimeBuilder.java"

# interfaces
.implements Ljava/lang/Cloneable;
.implements Lorg/threeten/bp/temporal/TemporalAccessor;


# instance fields
.field final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lorg/threeten/bp/temporal/TemporalField;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field b:Lorg/threeten/bp/chrono/Chronology;

.field c:Lorg/threeten/bp/ZoneId;

.field d:Lorg/threeten/bp/chrono/ChronoLocalDate;

.field e:Lorg/threeten/bp/LocalTime;

.field f:Z

.field g:Lorg/threeten/bp/Period;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 132
    invoke-direct {p0}, Lorg/threeten/bp/jdk8/DefaultInterfaceTemporalAccessor;-><init>()V

    .line 102
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lorg/threeten/bp/format/DateTimeBuilder;->a:Ljava/util/Map;

    .line 133
    return-void
.end method

.method private a()V
    .locals 2

    .prologue
    .line 539
    iget-object v0, p0, Lorg/threeten/bp/format/DateTimeBuilder;->a:Ljava/util/Map;

    sget-object v1, Lorg/threeten/bp/temporal/ChronoField;->C:Lorg/threeten/bp/temporal/ChronoField;

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 540
    iget-object v0, p0, Lorg/threeten/bp/format/DateTimeBuilder;->c:Lorg/threeten/bp/ZoneId;

    if-eqz v0, :cond_1

    .line 541
    iget-object v0, p0, Lorg/threeten/bp/format/DateTimeBuilder;->c:Lorg/threeten/bp/ZoneId;

    invoke-direct {p0, v0}, Lorg/threeten/bp/format/DateTimeBuilder;->a(Lorg/threeten/bp/ZoneId;)V

    .line 550
    :cond_0
    :goto_0
    return-void

    .line 543
    :cond_1
    iget-object v0, p0, Lorg/threeten/bp/format/DateTimeBuilder;->a:Ljava/util/Map;

    sget-object v1, Lorg/threeten/bp/temporal/ChronoField;->D:Lorg/threeten/bp/temporal/ChronoField;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    .line 544
    if-eqz v0, :cond_0

    .line 545
    invoke-virtual {v0}, Ljava/lang/Long;->intValue()I

    move-result v0

    invoke-static {v0}, Lorg/threeten/bp/ZoneOffset;->a(I)Lorg/threeten/bp/ZoneOffset;

    move-result-object v0

    .line 546
    invoke-direct {p0, v0}, Lorg/threeten/bp/format/DateTimeBuilder;->a(Lorg/threeten/bp/ZoneId;)V

    goto :goto_0
.end method

.method private a(Lorg/threeten/bp/LocalDate;)V
    .locals 8

    .prologue
    .line 307
    if-eqz p1, :cond_1

    .line 308
    invoke-virtual {p0, p1}, Lorg/threeten/bp/format/DateTimeBuilder;->a(Lorg/threeten/bp/chrono/ChronoLocalDate;)V

    .line 309
    iget-object v0, p0, Lorg/threeten/bp/format/DateTimeBuilder;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/threeten/bp/temporal/TemporalField;

    .line 310
    instance-of v1, v0, Lorg/threeten/bp/temporal/ChronoField;

    if-eqz v1, :cond_0

    .line 311
    invoke-interface {v0}, Lorg/threeten/bp/temporal/TemporalField;->b()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 314
    :try_start_0
    invoke-virtual {p1, v0}, Lorg/threeten/bp/LocalDate;->d(Lorg/threeten/bp/temporal/TemporalField;)J
    :try_end_0
    .catch Lorg/threeten/bp/DateTimeException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v4

    .line 318
    iget-object v1, p0, Lorg/threeten/bp/format/DateTimeBuilder;->a:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    .line 319
    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    cmp-long v3, v4, v6

    if-eqz v3, :cond_0

    .line 320
    new-instance v2, Lorg/threeten/bp/DateTimeException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Conflict found: Field "

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v6, " "

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " differs from "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " derived from "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lorg/threeten/bp/DateTimeException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 315
    :catch_0
    move-exception v0

    goto :goto_0

    .line 326
    :cond_1
    return-void
.end method

.method private a(Lorg/threeten/bp/ZoneId;)V
    .locals 4

    .prologue
    .line 553
    iget-object v0, p0, Lorg/threeten/bp/format/DateTimeBuilder;->a:Ljava/util/Map;

    sget-object v1, Lorg/threeten/bp/temporal/ChronoField;->C:Lorg/threeten/bp/temporal/ChronoField;

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-static {v0, v1}, Lorg/threeten/bp/Instant;->a(J)Lorg/threeten/bp/Instant;

    move-result-object v0

    .line 554
    iget-object v1, p0, Lorg/threeten/bp/format/DateTimeBuilder;->b:Lorg/threeten/bp/chrono/Chronology;

    invoke-virtual {v1, v0, p1}, Lorg/threeten/bp/chrono/Chronology;->a(Lorg/threeten/bp/Instant;Lorg/threeten/bp/ZoneId;)Lorg/threeten/bp/chrono/ChronoZonedDateTime;

    move-result-object v0

    .line 555
    iget-object v1, p0, Lorg/threeten/bp/format/DateTimeBuilder;->d:Lorg/threeten/bp/chrono/ChronoLocalDate;

    if-nez v1, :cond_0

    .line 556
    invoke-virtual {v0}, Lorg/threeten/bp/chrono/ChronoZonedDateTime;->i()Lorg/threeten/bp/chrono/ChronoLocalDate;

    move-result-object v1

    invoke-virtual {p0, v1}, Lorg/threeten/bp/format/DateTimeBuilder;->a(Lorg/threeten/bp/chrono/ChronoLocalDate;)V

    .line 560
    :goto_0
    sget-object v1, Lorg/threeten/bp/temporal/ChronoField;->h:Lorg/threeten/bp/temporal/ChronoField;

    invoke-virtual {v0}, Lorg/threeten/bp/chrono/ChronoZonedDateTime;->f()Lorg/threeten/bp/LocalTime;

    move-result-object v0

    invoke-virtual {v0}, Lorg/threeten/bp/LocalTime;->e()I

    move-result v0

    int-to-long v2, v0

    invoke-virtual {p0, v1, v2, v3}, Lorg/threeten/bp/format/DateTimeBuilder;->a(Lorg/threeten/bp/temporal/TemporalField;J)Lorg/threeten/bp/format/DateTimeBuilder;

    .line 561
    return-void

    .line 558
    :cond_0
    sget-object v1, Lorg/threeten/bp/temporal/ChronoField;->C:Lorg/threeten/bp/temporal/ChronoField;

    invoke-virtual {v0}, Lorg/threeten/bp/chrono/ChronoZonedDateTime;->i()Lorg/threeten/bp/chrono/ChronoLocalDate;

    move-result-object v2

    invoke-direct {p0, v1, v2}, Lorg/threeten/bp/format/DateTimeBuilder;->a(Lorg/threeten/bp/temporal/TemporalField;Lorg/threeten/bp/chrono/ChronoLocalDate;)V

    goto :goto_0
.end method

.method private a(Lorg/threeten/bp/temporal/TemporalAccessor;)V
    .locals 8

    .prologue
    .line 577
    iget-object v0, p0, Lorg/threeten/bp/format/DateTimeBuilder;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 578
    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 579
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 580
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/threeten/bp/temporal/TemporalField;

    .line 581
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    .line 582
    invoke-interface {p1, v1}, Lorg/threeten/bp/temporal/TemporalAccessor;->a(Lorg/threeten/bp/temporal/TemporalField;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 585
    :try_start_0
    invoke-interface {p1, v1}, Lorg/threeten/bp/temporal/TemporalAccessor;->d(Lorg/threeten/bp/temporal/TemporalField;)J
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v6

    .line 589
    cmp-long v0, v6, v4

    if-eqz v0, :cond_1

    .line 590
    new-instance v0, Lorg/threeten/bp/DateTimeException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Cross check failed: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " vs "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/threeten/bp/DateTimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 593
    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    .line 586
    :catch_0
    move-exception v0

    goto :goto_0

    .line 596
    :cond_2
    return-void
.end method

.method private a(Lorg/threeten/bp/temporal/TemporalField;Lorg/threeten/bp/LocalTime;)V
    .locals 6

    .prologue
    .line 286
    invoke-virtual {p2}, Lorg/threeten/bp/LocalTime;->f()J

    move-result-wide v2

    .line 287
    iget-object v0, p0, Lorg/threeten/bp/format/DateTimeBuilder;->a:Ljava/util/Map;

    sget-object v1, Lorg/threeten/bp/temporal/ChronoField;->b:Lorg/threeten/bp/temporal/ChronoField;

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-interface {v0, v1, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    .line 288
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    cmp-long v1, v4, v2

    if-eqz v1, :cond_0

    .line 289
    new-instance v1, Lorg/threeten/bp/DateTimeException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Conflict found: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-static {v4, v5}, Lorg/threeten/bp/LocalTime;->b(J)Lorg/threeten/bp/LocalTime;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " differs from "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " while resolving  "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lorg/threeten/bp/DateTimeException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 293
    :cond_0
    return-void
.end method

.method private a(Lorg/threeten/bp/temporal/TemporalField;Lorg/threeten/bp/chrono/ChronoLocalDate;)V
    .locals 8

    .prologue
    .line 273
    iget-object v0, p0, Lorg/threeten/bp/format/DateTimeBuilder;->b:Lorg/threeten/bp/chrono/Chronology;

    invoke-virtual {p2}, Lorg/threeten/bp/chrono/ChronoLocalDate;->o()Lorg/threeten/bp/chrono/Chronology;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/threeten/bp/chrono/Chronology;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 274
    new-instance v0, Lorg/threeten/bp/DateTimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "ChronoLocalDate must use the effective parsed chronology: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lorg/threeten/bp/format/DateTimeBuilder;->b:Lorg/threeten/bp/chrono/Chronology;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/threeten/bp/DateTimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 276
    :cond_0
    invoke-virtual {p2}, Lorg/threeten/bp/chrono/ChronoLocalDate;->n()J

    move-result-wide v2

    .line 277
    iget-object v0, p0, Lorg/threeten/bp/format/DateTimeBuilder;->a:Ljava/util/Map;

    sget-object v1, Lorg/threeten/bp/temporal/ChronoField;->u:Lorg/threeten/bp/temporal/ChronoField;

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-interface {v0, v1, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    .line 278
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    cmp-long v1, v4, v2

    if-eqz v1, :cond_1

    .line 279
    new-instance v1, Lorg/threeten/bp/DateTimeException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Conflict found: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    invoke-static {v6, v7}, Lorg/threeten/bp/LocalDate;->a(J)Lorg/threeten/bp/LocalDate;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, " differs from "

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v2, v3}, Lorg/threeten/bp/LocalDate;->a(J)Lorg/threeten/bp/LocalDate;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " while resolving  "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lorg/threeten/bp/DateTimeException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 283
    :cond_1
    return-void
.end method

.method private a(Lorg/threeten/bp/format/ResolverStyle;)Z
    .locals 7

    .prologue
    const/16 v6, 0x64

    const/4 v2, 0x0

    .line 225
    move v3, v2

    .line 227
    :goto_0
    if-ge v3, v6, :cond_8

    .line 228
    iget-object v0, p0, Lorg/threeten/bp/format/DateTimeBuilder;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 229
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/threeten/bp/temporal/TemporalField;

    .line 230
    iget-object v1, p0, Lorg/threeten/bp/format/DateTimeBuilder;->a:Ljava/util/Map;

    invoke-interface {v0, v1, p0, p1}, Lorg/threeten/bp/temporal/TemporalField;->a(Ljava/util/Map;Lorg/threeten/bp/temporal/TemporalAccessor;Lorg/threeten/bp/format/ResolverStyle;)Lorg/threeten/bp/temporal/TemporalAccessor;

    move-result-object v1

    .line 231
    if-eqz v1, :cond_7

    .line 232
    instance-of v4, v1, Lorg/threeten/bp/chrono/ChronoZonedDateTime;

    if-eqz v4, :cond_2

    .line 233
    check-cast v1, Lorg/threeten/bp/chrono/ChronoZonedDateTime;

    .line 234
    iget-object v4, p0, Lorg/threeten/bp/format/DateTimeBuilder;->c:Lorg/threeten/bp/ZoneId;

    if-nez v4, :cond_3

    .line 235
    invoke-virtual {v1}, Lorg/threeten/bp/chrono/ChronoZonedDateTime;->b()Lorg/threeten/bp/ZoneId;

    move-result-object v4

    iput-object v4, p0, Lorg/threeten/bp/format/DateTimeBuilder;->c:Lorg/threeten/bp/ZoneId;

    .line 239
    :cond_1
    invoke-virtual {v1}, Lorg/threeten/bp/chrono/ChronoZonedDateTime;->h()Lorg/threeten/bp/chrono/ChronoLocalDateTime;

    move-result-object v1

    .line 241
    :cond_2
    instance-of v4, v1, Lorg/threeten/bp/chrono/ChronoLocalDate;

    if-eqz v4, :cond_4

    .line 242
    check-cast v1, Lorg/threeten/bp/chrono/ChronoLocalDate;

    invoke-direct {p0, v0, v1}, Lorg/threeten/bp/format/DateTimeBuilder;->a(Lorg/threeten/bp/temporal/TemporalField;Lorg/threeten/bp/chrono/ChronoLocalDate;)V

    .line 243
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    .line 244
    goto :goto_0

    .line 236
    :cond_3
    iget-object v4, p0, Lorg/threeten/bp/format/DateTimeBuilder;->c:Lorg/threeten/bp/ZoneId;

    invoke-virtual {v1}, Lorg/threeten/bp/chrono/ChronoZonedDateTime;->b()Lorg/threeten/bp/ZoneId;

    move-result-object v5

    invoke-virtual {v4, v5}, Lorg/threeten/bp/ZoneId;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 237
    new-instance v0, Lorg/threeten/bp/DateTimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "ChronoZonedDateTime must use the effective parsed zone: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lorg/threeten/bp/format/DateTimeBuilder;->c:Lorg/threeten/bp/ZoneId;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/threeten/bp/DateTimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 246
    :cond_4
    instance-of v4, v1, Lorg/threeten/bp/LocalTime;

    if-eqz v4, :cond_5

    .line 247
    check-cast v1, Lorg/threeten/bp/LocalTime;

    invoke-direct {p0, v0, v1}, Lorg/threeten/bp/format/DateTimeBuilder;->a(Lorg/threeten/bp/temporal/TemporalField;Lorg/threeten/bp/LocalTime;)V

    .line 248
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    .line 249
    goto :goto_0

    .line 251
    :cond_5
    instance-of v4, v1, Lorg/threeten/bp/chrono/ChronoLocalDateTime;

    if-eqz v4, :cond_6

    .line 252
    check-cast v1, Lorg/threeten/bp/chrono/ChronoLocalDateTime;

    .line 253
    invoke-virtual {v1}, Lorg/threeten/bp/chrono/ChronoLocalDateTime;->l()Lorg/threeten/bp/chrono/ChronoLocalDate;

    move-result-object v4

    invoke-direct {p0, v0, v4}, Lorg/threeten/bp/format/DateTimeBuilder;->a(Lorg/threeten/bp/temporal/TemporalField;Lorg/threeten/bp/chrono/ChronoLocalDate;)V

    .line 254
    invoke-virtual {v1}, Lorg/threeten/bp/chrono/ChronoLocalDateTime;->k()Lorg/threeten/bp/LocalTime;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lorg/threeten/bp/format/DateTimeBuilder;->a(Lorg/threeten/bp/temporal/TemporalField;Lorg/threeten/bp/LocalTime;)V

    .line 255
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    .line 256
    goto/16 :goto_0

    .line 258
    :cond_6
    new-instance v0, Lorg/threeten/bp/DateTimeException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unknown type: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/threeten/bp/DateTimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 259
    :cond_7
    iget-object v1, p0, Lorg/threeten/bp/format/DateTimeBuilder;->a:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 260
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    .line 261
    goto/16 :goto_0

    .line 266
    :cond_8
    if-ne v3, v6, :cond_9

    .line 267
    new-instance v0, Lorg/threeten/bp/DateTimeException;

    const-string v1, "Badly written field"

    invoke-direct {v0, v1}, Lorg/threeten/bp/DateTimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 269
    :cond_9
    if-lez v3, :cond_a

    const/4 v0, 0x1

    :goto_1
    return v0

    :cond_a
    move v0, v2

    goto :goto_1
.end method

.method private b(Lorg/threeten/bp/temporal/TemporalField;J)Lorg/threeten/bp/format/DateTimeBuilder;
    .locals 2

    .prologue
    .line 176
    iget-object v0, p0, Lorg/threeten/bp/format/DateTimeBuilder;->a:Ljava/util/Map;

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 177
    return-object p0
.end method

.method private b()V
    .locals 2

    .prologue
    .line 565
    iget-object v0, p0, Lorg/threeten/bp/format/DateTimeBuilder;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 566
    iget-object v0, p0, Lorg/threeten/bp/format/DateTimeBuilder;->d:Lorg/threeten/bp/chrono/ChronoLocalDate;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lorg/threeten/bp/format/DateTimeBuilder;->e:Lorg/threeten/bp/LocalTime;

    if-eqz v0, :cond_1

    .line 567
    iget-object v0, p0, Lorg/threeten/bp/format/DateTimeBuilder;->d:Lorg/threeten/bp/chrono/ChronoLocalDate;

    iget-object v1, p0, Lorg/threeten/bp/format/DateTimeBuilder;->e:Lorg/threeten/bp/LocalTime;

    invoke-virtual {v0, v1}, Lorg/threeten/bp/chrono/ChronoLocalDate;->b(Lorg/threeten/bp/LocalTime;)Lorg/threeten/bp/chrono/ChronoLocalDateTime;

    move-result-object v0

    invoke-direct {p0, v0}, Lorg/threeten/bp/format/DateTimeBuilder;->a(Lorg/threeten/bp/temporal/TemporalAccessor;)V

    .line 574
    :cond_0
    :goto_0
    return-void

    .line 568
    :cond_1
    iget-object v0, p0, Lorg/threeten/bp/format/DateTimeBuilder;->d:Lorg/threeten/bp/chrono/ChronoLocalDate;

    if-eqz v0, :cond_2

    .line 569
    iget-object v0, p0, Lorg/threeten/bp/format/DateTimeBuilder;->d:Lorg/threeten/bp/chrono/ChronoLocalDate;

    invoke-direct {p0, v0}, Lorg/threeten/bp/format/DateTimeBuilder;->a(Lorg/threeten/bp/temporal/TemporalAccessor;)V

    goto :goto_0

    .line 570
    :cond_2
    iget-object v0, p0, Lorg/threeten/bp/format/DateTimeBuilder;->e:Lorg/threeten/bp/LocalTime;

    if-eqz v0, :cond_0

    .line 571
    iget-object v0, p0, Lorg/threeten/bp/format/DateTimeBuilder;->e:Lorg/threeten/bp/LocalTime;

    invoke-direct {p0, v0}, Lorg/threeten/bp/format/DateTimeBuilder;->a(Lorg/threeten/bp/temporal/TemporalAccessor;)V

    goto :goto_0
.end method

.method private b(Lorg/threeten/bp/format/ResolverStyle;)V
    .locals 2

    .prologue
    .line 296
    iget-object v0, p0, Lorg/threeten/bp/format/DateTimeBuilder;->b:Lorg/threeten/bp/chrono/Chronology;

    instance-of v0, v0, Lorg/threeten/bp/chrono/IsoChronology;

    if-eqz v0, :cond_1

    .line 297
    sget-object v0, Lorg/threeten/bp/chrono/IsoChronology;->b:Lorg/threeten/bp/chrono/IsoChronology;

    iget-object v1, p0, Lorg/threeten/bp/format/DateTimeBuilder;->a:Ljava/util/Map;

    invoke-virtual {v0, v1, p1}, Lorg/threeten/bp/chrono/IsoChronology;->a(Ljava/util/Map;Lorg/threeten/bp/format/ResolverStyle;)Lorg/threeten/bp/LocalDate;

    move-result-object v0

    invoke-direct {p0, v0}, Lorg/threeten/bp/format/DateTimeBuilder;->a(Lorg/threeten/bp/LocalDate;)V

    .line 304
    :cond_0
    :goto_0
    return-void

    .line 299
    :cond_1
    iget-object v0, p0, Lorg/threeten/bp/format/DateTimeBuilder;->a:Ljava/util/Map;

    sget-object v1, Lorg/threeten/bp/temporal/ChronoField;->u:Lorg/threeten/bp/temporal/ChronoField;

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 300
    iget-object v0, p0, Lorg/threeten/bp/format/DateTimeBuilder;->a:Ljava/util/Map;

    sget-object v1, Lorg/threeten/bp/temporal/ChronoField;->u:Lorg/threeten/bp/temporal/ChronoField;

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-static {v0, v1}, Lorg/threeten/bp/LocalDate;->a(J)Lorg/threeten/bp/LocalDate;

    move-result-object v0

    invoke-direct {p0, v0}, Lorg/threeten/bp/format/DateTimeBuilder;->a(Lorg/threeten/bp/LocalDate;)V

    goto :goto_0
.end method

.method private c()V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    .line 599
    iget-object v0, p0, Lorg/threeten/bp/format/DateTimeBuilder;->e:Lorg/threeten/bp/LocalTime;

    if-nez v0, :cond_1

    iget-object v0, p0, Lorg/threeten/bp/format/DateTimeBuilder;->a:Ljava/util/Map;

    sget-object v1, Lorg/threeten/bp/temporal/ChronoField;->C:Lorg/threeten/bp/temporal/ChronoField;

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lorg/threeten/bp/format/DateTimeBuilder;->a:Ljava/util/Map;

    sget-object v1, Lorg/threeten/bp/temporal/ChronoField;->h:Lorg/threeten/bp/temporal/ChronoField;

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lorg/threeten/bp/format/DateTimeBuilder;->a:Ljava/util/Map;

    sget-object v1, Lorg/threeten/bp/temporal/ChronoField;->g:Lorg/threeten/bp/temporal/ChronoField;

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 603
    :cond_0
    iget-object v0, p0, Lorg/threeten/bp/format/DateTimeBuilder;->a:Ljava/util/Map;

    sget-object v1, Lorg/threeten/bp/temporal/ChronoField;->a:Lorg/threeten/bp/temporal/ChronoField;

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 604
    iget-object v0, p0, Lorg/threeten/bp/format/DateTimeBuilder;->a:Ljava/util/Map;

    sget-object v1, Lorg/threeten/bp/temporal/ChronoField;->a:Lorg/threeten/bp/temporal/ChronoField;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    .line 605
    iget-object v2, p0, Lorg/threeten/bp/format/DateTimeBuilder;->a:Ljava/util/Map;

    sget-object v3, Lorg/threeten/bp/temporal/ChronoField;->c:Lorg/threeten/bp/temporal/ChronoField;

    const-wide/16 v4, 0x3e8

    div-long v4, v0, v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-interface {v2, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 606
    iget-object v2, p0, Lorg/threeten/bp/format/DateTimeBuilder;->a:Ljava/util/Map;

    sget-object v3, Lorg/threeten/bp/temporal/ChronoField;->e:Lorg/threeten/bp/temporal/ChronoField;

    const-wide/32 v4, 0xf4240

    div-long/2addr v0, v4

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-interface {v2, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 613
    :cond_1
    :goto_0
    return-void

    .line 608
    :cond_2
    iget-object v0, p0, Lorg/threeten/bp/format/DateTimeBuilder;->a:Ljava/util/Map;

    sget-object v1, Lorg/threeten/bp/temporal/ChronoField;->a:Lorg/threeten/bp/temporal/ChronoField;

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 609
    iget-object v0, p0, Lorg/threeten/bp/format/DateTimeBuilder;->a:Ljava/util/Map;

    sget-object v1, Lorg/threeten/bp/temporal/ChronoField;->c:Lorg/threeten/bp/temporal/ChronoField;

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 610
    iget-object v0, p0, Lorg/threeten/bp/format/DateTimeBuilder;->a:Ljava/util/Map;

    sget-object v1, Lorg/threeten/bp/temporal/ChronoField;->e:Lorg/threeten/bp/temporal/ChronoField;

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method private c(Lorg/threeten/bp/format/ResolverStyle;)V
    .locals 8

    .prologue
    .line 329
    iget-object v0, p0, Lorg/threeten/bp/format/DateTimeBuilder;->a:Ljava/util/Map;

    sget-object v1, Lorg/threeten/bp/temporal/ChronoField;->n:Lorg/threeten/bp/temporal/ChronoField;

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 330
    iget-object v0, p0, Lorg/threeten/bp/format/DateTimeBuilder;->a:Ljava/util/Map;

    sget-object v1, Lorg/threeten/bp/temporal/ChronoField;->n:Lorg/threeten/bp/temporal/ChronoField;

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    .line 331
    sget-object v2, Lorg/threeten/bp/format/ResolverStyle;->c:Lorg/threeten/bp/format/ResolverStyle;

    if-eq p1, v2, :cond_0

    .line 332
    sget-object v2, Lorg/threeten/bp/format/ResolverStyle;->b:Lorg/threeten/bp/format/ResolverStyle;

    if-ne p1, v2, :cond_19

    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-nez v2, :cond_19

    .line 338
    :cond_0
    :goto_0
    sget-object v2, Lorg/threeten/bp/temporal/ChronoField;->m:Lorg/threeten/bp/temporal/ChronoField;

    const-wide/16 v4, 0x18

    cmp-long v3, v0, v4

    if-nez v3, :cond_1

    const-wide/16 v0, 0x0

    :cond_1
    invoke-virtual {p0, v2, v0, v1}, Lorg/threeten/bp/format/DateTimeBuilder;->a(Lorg/threeten/bp/temporal/TemporalField;J)Lorg/threeten/bp/format/DateTimeBuilder;

    .line 340
    :cond_2
    iget-object v0, p0, Lorg/threeten/bp/format/DateTimeBuilder;->a:Ljava/util/Map;

    sget-object v1, Lorg/threeten/bp/temporal/ChronoField;->l:Lorg/threeten/bp/temporal/ChronoField;

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 341
    iget-object v0, p0, Lorg/threeten/bp/format/DateTimeBuilder;->a:Ljava/util/Map;

    sget-object v1, Lorg/threeten/bp/temporal/ChronoField;->l:Lorg/threeten/bp/temporal/ChronoField;

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    .line 342
    sget-object v2, Lorg/threeten/bp/format/ResolverStyle;->c:Lorg/threeten/bp/format/ResolverStyle;

    if-eq p1, v2, :cond_3

    .line 343
    sget-object v2, Lorg/threeten/bp/format/ResolverStyle;->b:Lorg/threeten/bp/format/ResolverStyle;

    if-ne p1, v2, :cond_1a

    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-nez v2, :cond_1a

    .line 349
    :cond_3
    :goto_1
    sget-object v2, Lorg/threeten/bp/temporal/ChronoField;->k:Lorg/threeten/bp/temporal/ChronoField;

    const-wide/16 v4, 0xc

    cmp-long v3, v0, v4

    if-nez v3, :cond_4

    const-wide/16 v0, 0x0

    :cond_4
    invoke-virtual {p0, v2, v0, v1}, Lorg/threeten/bp/format/DateTimeBuilder;->a(Lorg/threeten/bp/temporal/TemporalField;J)Lorg/threeten/bp/format/DateTimeBuilder;

    .line 351
    :cond_5
    sget-object v0, Lorg/threeten/bp/format/ResolverStyle;->c:Lorg/threeten/bp/format/ResolverStyle;

    if-eq p1, v0, :cond_7

    .line 352
    iget-object v0, p0, Lorg/threeten/bp/format/DateTimeBuilder;->a:Ljava/util/Map;

    sget-object v1, Lorg/threeten/bp/temporal/ChronoField;->o:Lorg/threeten/bp/temporal/ChronoField;

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 353
    sget-object v1, Lorg/threeten/bp/temporal/ChronoField;->o:Lorg/threeten/bp/temporal/ChronoField;

    iget-object v0, p0, Lorg/threeten/bp/format/DateTimeBuilder;->a:Ljava/util/Map;

    sget-object v2, Lorg/threeten/bp/temporal/ChronoField;->o:Lorg/threeten/bp/temporal/ChronoField;

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lorg/threeten/bp/temporal/ChronoField;->a(J)J

    .line 355
    :cond_6
    iget-object v0, p0, Lorg/threeten/bp/format/DateTimeBuilder;->a:Ljava/util/Map;

    sget-object v1, Lorg/threeten/bp/temporal/ChronoField;->k:Lorg/threeten/bp/temporal/ChronoField;

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 356
    sget-object v1, Lorg/threeten/bp/temporal/ChronoField;->k:Lorg/threeten/bp/temporal/ChronoField;

    iget-object v0, p0, Lorg/threeten/bp/format/DateTimeBuilder;->a:Ljava/util/Map;

    sget-object v2, Lorg/threeten/bp/temporal/ChronoField;->k:Lorg/threeten/bp/temporal/ChronoField;

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lorg/threeten/bp/temporal/ChronoField;->a(J)J

    .line 359
    :cond_7
    iget-object v0, p0, Lorg/threeten/bp/format/DateTimeBuilder;->a:Ljava/util/Map;

    sget-object v1, Lorg/threeten/bp/temporal/ChronoField;->o:Lorg/threeten/bp/temporal/ChronoField;

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    iget-object v0, p0, Lorg/threeten/bp/format/DateTimeBuilder;->a:Ljava/util/Map;

    sget-object v1, Lorg/threeten/bp/temporal/ChronoField;->k:Lorg/threeten/bp/temporal/ChronoField;

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 360
    iget-object v0, p0, Lorg/threeten/bp/format/DateTimeBuilder;->a:Ljava/util/Map;

    sget-object v1, Lorg/threeten/bp/temporal/ChronoField;->o:Lorg/threeten/bp/temporal/ChronoField;

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    .line 361
    iget-object v0, p0, Lorg/threeten/bp/format/DateTimeBuilder;->a:Ljava/util/Map;

    sget-object v1, Lorg/threeten/bp/temporal/ChronoField;->k:Lorg/threeten/bp/temporal/ChronoField;

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    .line 362
    sget-object v4, Lorg/threeten/bp/temporal/ChronoField;->m:Lorg/threeten/bp/temporal/ChronoField;

    const-wide/16 v6, 0xc

    mul-long/2addr v2, v6

    add-long/2addr v0, v2

    invoke-virtual {p0, v4, v0, v1}, Lorg/threeten/bp/format/DateTimeBuilder;->a(Lorg/threeten/bp/temporal/TemporalField;J)Lorg/threeten/bp/format/DateTimeBuilder;

    .line 374
    :cond_8
    iget-object v0, p0, Lorg/threeten/bp/format/DateTimeBuilder;->a:Ljava/util/Map;

    sget-object v1, Lorg/threeten/bp/temporal/ChronoField;->b:Lorg/threeten/bp/temporal/ChronoField;

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 375
    iget-object v0, p0, Lorg/threeten/bp/format/DateTimeBuilder;->a:Ljava/util/Map;

    sget-object v1, Lorg/threeten/bp/temporal/ChronoField;->b:Lorg/threeten/bp/temporal/ChronoField;

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    .line 376
    sget-object v2, Lorg/threeten/bp/format/ResolverStyle;->c:Lorg/threeten/bp/format/ResolverStyle;

    if-eq p1, v2, :cond_9

    .line 377
    sget-object v2, Lorg/threeten/bp/temporal/ChronoField;->b:Lorg/threeten/bp/temporal/ChronoField;

    invoke-virtual {v2, v0, v1}, Lorg/threeten/bp/temporal/ChronoField;->a(J)J

    .line 379
    :cond_9
    sget-object v2, Lorg/threeten/bp/temporal/ChronoField;->h:Lorg/threeten/bp/temporal/ChronoField;

    const-wide/32 v4, 0x3b9aca00

    div-long v4, v0, v4

    invoke-virtual {p0, v2, v4, v5}, Lorg/threeten/bp/format/DateTimeBuilder;->a(Lorg/threeten/bp/temporal/TemporalField;J)Lorg/threeten/bp/format/DateTimeBuilder;

    .line 380
    sget-object v2, Lorg/threeten/bp/temporal/ChronoField;->a:Lorg/threeten/bp/temporal/ChronoField;

    const-wide/32 v4, 0x3b9aca00

    rem-long/2addr v0, v4

    invoke-virtual {p0, v2, v0, v1}, Lorg/threeten/bp/format/DateTimeBuilder;->a(Lorg/threeten/bp/temporal/TemporalField;J)Lorg/threeten/bp/format/DateTimeBuilder;

    .line 382
    :cond_a
    iget-object v0, p0, Lorg/threeten/bp/format/DateTimeBuilder;->a:Ljava/util/Map;

    sget-object v1, Lorg/threeten/bp/temporal/ChronoField;->d:Lorg/threeten/bp/temporal/ChronoField;

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 383
    iget-object v0, p0, Lorg/threeten/bp/format/DateTimeBuilder;->a:Ljava/util/Map;

    sget-object v1, Lorg/threeten/bp/temporal/ChronoField;->d:Lorg/threeten/bp/temporal/ChronoField;

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    .line 384
    sget-object v2, Lorg/threeten/bp/format/ResolverStyle;->c:Lorg/threeten/bp/format/ResolverStyle;

    if-eq p1, v2, :cond_b

    .line 385
    sget-object v2, Lorg/threeten/bp/temporal/ChronoField;->d:Lorg/threeten/bp/temporal/ChronoField;

    invoke-virtual {v2, v0, v1}, Lorg/threeten/bp/temporal/ChronoField;->a(J)J

    .line 387
    :cond_b
    sget-object v2, Lorg/threeten/bp/temporal/ChronoField;->h:Lorg/threeten/bp/temporal/ChronoField;

    const-wide/32 v4, 0xf4240

    div-long v4, v0, v4

    invoke-virtual {p0, v2, v4, v5}, Lorg/threeten/bp/format/DateTimeBuilder;->a(Lorg/threeten/bp/temporal/TemporalField;J)Lorg/threeten/bp/format/DateTimeBuilder;

    .line 388
    sget-object v2, Lorg/threeten/bp/temporal/ChronoField;->c:Lorg/threeten/bp/temporal/ChronoField;

    const-wide/32 v4, 0xf4240

    rem-long/2addr v0, v4

    invoke-virtual {p0, v2, v0, v1}, Lorg/threeten/bp/format/DateTimeBuilder;->a(Lorg/threeten/bp/temporal/TemporalField;J)Lorg/threeten/bp/format/DateTimeBuilder;

    .line 390
    :cond_c
    iget-object v0, p0, Lorg/threeten/bp/format/DateTimeBuilder;->a:Ljava/util/Map;

    sget-object v1, Lorg/threeten/bp/temporal/ChronoField;->f:Lorg/threeten/bp/temporal/ChronoField;

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 391
    iget-object v0, p0, Lorg/threeten/bp/format/DateTimeBuilder;->a:Ljava/util/Map;

    sget-object v1, Lorg/threeten/bp/temporal/ChronoField;->f:Lorg/threeten/bp/temporal/ChronoField;

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    .line 392
    sget-object v2, Lorg/threeten/bp/format/ResolverStyle;->c:Lorg/threeten/bp/format/ResolverStyle;

    if-eq p1, v2, :cond_d

    .line 393
    sget-object v2, Lorg/threeten/bp/temporal/ChronoField;->f:Lorg/threeten/bp/temporal/ChronoField;

    invoke-virtual {v2, v0, v1}, Lorg/threeten/bp/temporal/ChronoField;->a(J)J

    .line 395
    :cond_d
    sget-object v2, Lorg/threeten/bp/temporal/ChronoField;->h:Lorg/threeten/bp/temporal/ChronoField;

    const-wide/16 v4, 0x3e8

    div-long v4, v0, v4

    invoke-virtual {p0, v2, v4, v5}, Lorg/threeten/bp/format/DateTimeBuilder;->a(Lorg/threeten/bp/temporal/TemporalField;J)Lorg/threeten/bp/format/DateTimeBuilder;

    .line 396
    sget-object v2, Lorg/threeten/bp/temporal/ChronoField;->e:Lorg/threeten/bp/temporal/ChronoField;

    const-wide/16 v4, 0x3e8

    rem-long/2addr v0, v4

    invoke-virtual {p0, v2, v0, v1}, Lorg/threeten/bp/format/DateTimeBuilder;->a(Lorg/threeten/bp/temporal/TemporalField;J)Lorg/threeten/bp/format/DateTimeBuilder;

    .line 398
    :cond_e
    iget-object v0, p0, Lorg/threeten/bp/format/DateTimeBuilder;->a:Ljava/util/Map;

    sget-object v1, Lorg/threeten/bp/temporal/ChronoField;->h:Lorg/threeten/bp/temporal/ChronoField;

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_10

    .line 399
    iget-object v0, p0, Lorg/threeten/bp/format/DateTimeBuilder;->a:Ljava/util/Map;

    sget-object v1, Lorg/threeten/bp/temporal/ChronoField;->h:Lorg/threeten/bp/temporal/ChronoField;

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    .line 400
    sget-object v2, Lorg/threeten/bp/format/ResolverStyle;->c:Lorg/threeten/bp/format/ResolverStyle;

    if-eq p1, v2, :cond_f

    .line 401
    sget-object v2, Lorg/threeten/bp/temporal/ChronoField;->h:Lorg/threeten/bp/temporal/ChronoField;

    invoke-virtual {v2, v0, v1}, Lorg/threeten/bp/temporal/ChronoField;->a(J)J

    .line 403
    :cond_f
    sget-object v2, Lorg/threeten/bp/temporal/ChronoField;->m:Lorg/threeten/bp/temporal/ChronoField;

    const-wide/16 v4, 0xe10

    div-long v4, v0, v4

    invoke-virtual {p0, v2, v4, v5}, Lorg/threeten/bp/format/DateTimeBuilder;->a(Lorg/threeten/bp/temporal/TemporalField;J)Lorg/threeten/bp/format/DateTimeBuilder;

    .line 404
    sget-object v2, Lorg/threeten/bp/temporal/ChronoField;->i:Lorg/threeten/bp/temporal/ChronoField;

    const-wide/16 v4, 0x3c

    div-long v4, v0, v4

    const-wide/16 v6, 0x3c

    rem-long/2addr v4, v6

    invoke-virtual {p0, v2, v4, v5}, Lorg/threeten/bp/format/DateTimeBuilder;->a(Lorg/threeten/bp/temporal/TemporalField;J)Lorg/threeten/bp/format/DateTimeBuilder;

    .line 405
    sget-object v2, Lorg/threeten/bp/temporal/ChronoField;->g:Lorg/threeten/bp/temporal/ChronoField;

    const-wide/16 v4, 0x3c

    rem-long/2addr v0, v4

    invoke-virtual {p0, v2, v0, v1}, Lorg/threeten/bp/format/DateTimeBuilder;->a(Lorg/threeten/bp/temporal/TemporalField;J)Lorg/threeten/bp/format/DateTimeBuilder;

    .line 407
    :cond_10
    iget-object v0, p0, Lorg/threeten/bp/format/DateTimeBuilder;->a:Ljava/util/Map;

    sget-object v1, Lorg/threeten/bp/temporal/ChronoField;->j:Lorg/threeten/bp/temporal/ChronoField;

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_12

    .line 408
    iget-object v0, p0, Lorg/threeten/bp/format/DateTimeBuilder;->a:Ljava/util/Map;

    sget-object v1, Lorg/threeten/bp/temporal/ChronoField;->j:Lorg/threeten/bp/temporal/ChronoField;

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    .line 409
    sget-object v2, Lorg/threeten/bp/format/ResolverStyle;->c:Lorg/threeten/bp/format/ResolverStyle;

    if-eq p1, v2, :cond_11

    .line 410
    sget-object v2, Lorg/threeten/bp/temporal/ChronoField;->j:Lorg/threeten/bp/temporal/ChronoField;

    invoke-virtual {v2, v0, v1}, Lorg/threeten/bp/temporal/ChronoField;->a(J)J

    .line 412
    :cond_11
    sget-object v2, Lorg/threeten/bp/temporal/ChronoField;->m:Lorg/threeten/bp/temporal/ChronoField;

    const-wide/16 v4, 0x3c

    div-long v4, v0, v4

    invoke-virtual {p0, v2, v4, v5}, Lorg/threeten/bp/format/DateTimeBuilder;->a(Lorg/threeten/bp/temporal/TemporalField;J)Lorg/threeten/bp/format/DateTimeBuilder;

    .line 413
    sget-object v2, Lorg/threeten/bp/temporal/ChronoField;->i:Lorg/threeten/bp/temporal/ChronoField;

    const-wide/16 v4, 0x3c

    rem-long/2addr v0, v4

    invoke-virtual {p0, v2, v0, v1}, Lorg/threeten/bp/format/DateTimeBuilder;->a(Lorg/threeten/bp/temporal/TemporalField;J)Lorg/threeten/bp/format/DateTimeBuilder;

    .line 421
    :cond_12
    sget-object v0, Lorg/threeten/bp/format/ResolverStyle;->c:Lorg/threeten/bp/format/ResolverStyle;

    if-eq p1, v0, :cond_14

    .line 422
    iget-object v0, p0, Lorg/threeten/bp/format/DateTimeBuilder;->a:Ljava/util/Map;

    sget-object v1, Lorg/threeten/bp/temporal/ChronoField;->e:Lorg/threeten/bp/temporal/ChronoField;

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_13

    .line 423
    sget-object v1, Lorg/threeten/bp/temporal/ChronoField;->e:Lorg/threeten/bp/temporal/ChronoField;

    iget-object v0, p0, Lorg/threeten/bp/format/DateTimeBuilder;->a:Ljava/util/Map;

    sget-object v2, Lorg/threeten/bp/temporal/ChronoField;->e:Lorg/threeten/bp/temporal/ChronoField;

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lorg/threeten/bp/temporal/ChronoField;->a(J)J

    .line 425
    :cond_13
    iget-object v0, p0, Lorg/threeten/bp/format/DateTimeBuilder;->a:Ljava/util/Map;

    sget-object v1, Lorg/threeten/bp/temporal/ChronoField;->c:Lorg/threeten/bp/temporal/ChronoField;

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_14

    .line 426
    sget-object v1, Lorg/threeten/bp/temporal/ChronoField;->c:Lorg/threeten/bp/temporal/ChronoField;

    iget-object v0, p0, Lorg/threeten/bp/format/DateTimeBuilder;->a:Ljava/util/Map;

    sget-object v2, Lorg/threeten/bp/temporal/ChronoField;->c:Lorg/threeten/bp/temporal/ChronoField;

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lorg/threeten/bp/temporal/ChronoField;->a(J)J

    .line 429
    :cond_14
    iget-object v0, p0, Lorg/threeten/bp/format/DateTimeBuilder;->a:Ljava/util/Map;

    sget-object v1, Lorg/threeten/bp/temporal/ChronoField;->e:Lorg/threeten/bp/temporal/ChronoField;

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_15

    iget-object v0, p0, Lorg/threeten/bp/format/DateTimeBuilder;->a:Ljava/util/Map;

    sget-object v1, Lorg/threeten/bp/temporal/ChronoField;->c:Lorg/threeten/bp/temporal/ChronoField;

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_15

    .line 430
    iget-object v0, p0, Lorg/threeten/bp/format/DateTimeBuilder;->a:Ljava/util/Map;

    sget-object v1, Lorg/threeten/bp/temporal/ChronoField;->e:Lorg/threeten/bp/temporal/ChronoField;

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    .line 431
    iget-object v0, p0, Lorg/threeten/bp/format/DateTimeBuilder;->a:Ljava/util/Map;

    sget-object v1, Lorg/threeten/bp/temporal/ChronoField;->c:Lorg/threeten/bp/temporal/ChronoField;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    .line 432
    sget-object v4, Lorg/threeten/bp/temporal/ChronoField;->c:Lorg/threeten/bp/temporal/ChronoField;

    const-wide/16 v6, 0x3e8

    mul-long/2addr v2, v6

    const-wide/16 v6, 0x3e8

    rem-long/2addr v0, v6

    add-long/2addr v0, v2

    invoke-virtual {p0, v4, v0, v1}, Lorg/threeten/bp/format/DateTimeBuilder;->a(Lorg/threeten/bp/temporal/TemporalField;J)Lorg/threeten/bp/format/DateTimeBuilder;

    .line 434
    :cond_15
    iget-object v0, p0, Lorg/threeten/bp/format/DateTimeBuilder;->a:Ljava/util/Map;

    sget-object v1, Lorg/threeten/bp/temporal/ChronoField;->c:Lorg/threeten/bp/temporal/ChronoField;

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_16

    iget-object v0, p0, Lorg/threeten/bp/format/DateTimeBuilder;->a:Ljava/util/Map;

    sget-object v1, Lorg/threeten/bp/temporal/ChronoField;->a:Lorg/threeten/bp/temporal/ChronoField;

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_16

    .line 435
    iget-object v0, p0, Lorg/threeten/bp/format/DateTimeBuilder;->a:Ljava/util/Map;

    sget-object v1, Lorg/threeten/bp/temporal/ChronoField;->a:Lorg/threeten/bp/temporal/ChronoField;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    .line 436
    sget-object v2, Lorg/threeten/bp/temporal/ChronoField;->c:Lorg/threeten/bp/temporal/ChronoField;

    const-wide/16 v4, 0x3e8

    div-long/2addr v0, v4

    invoke-virtual {p0, v2, v0, v1}, Lorg/threeten/bp/format/DateTimeBuilder;->a(Lorg/threeten/bp/temporal/TemporalField;J)Lorg/threeten/bp/format/DateTimeBuilder;

    .line 437
    iget-object v0, p0, Lorg/threeten/bp/format/DateTimeBuilder;->a:Ljava/util/Map;

    sget-object v1, Lorg/threeten/bp/temporal/ChronoField;->c:Lorg/threeten/bp/temporal/ChronoField;

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 439
    :cond_16
    iget-object v0, p0, Lorg/threeten/bp/format/DateTimeBuilder;->a:Ljava/util/Map;

    sget-object v1, Lorg/threeten/bp/temporal/ChronoField;->e:Lorg/threeten/bp/temporal/ChronoField;

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_17

    iget-object v0, p0, Lorg/threeten/bp/format/DateTimeBuilder;->a:Ljava/util/Map;

    sget-object v1, Lorg/threeten/bp/temporal/ChronoField;->a:Lorg/threeten/bp/temporal/ChronoField;

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_17

    .line 440
    iget-object v0, p0, Lorg/threeten/bp/format/DateTimeBuilder;->a:Ljava/util/Map;

    sget-object v1, Lorg/threeten/bp/temporal/ChronoField;->a:Lorg/threeten/bp/temporal/ChronoField;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    .line 441
    sget-object v2, Lorg/threeten/bp/temporal/ChronoField;->e:Lorg/threeten/bp/temporal/ChronoField;

    const-wide/32 v4, 0xf4240

    div-long/2addr v0, v4

    invoke-virtual {p0, v2, v0, v1}, Lorg/threeten/bp/format/DateTimeBuilder;->a(Lorg/threeten/bp/temporal/TemporalField;J)Lorg/threeten/bp/format/DateTimeBuilder;

    .line 442
    iget-object v0, p0, Lorg/threeten/bp/format/DateTimeBuilder;->a:Ljava/util/Map;

    sget-object v1, Lorg/threeten/bp/temporal/ChronoField;->e:Lorg/threeten/bp/temporal/ChronoField;

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 444
    :cond_17
    iget-object v0, p0, Lorg/threeten/bp/format/DateTimeBuilder;->a:Ljava/util/Map;

    sget-object v1, Lorg/threeten/bp/temporal/ChronoField;->c:Lorg/threeten/bp/temporal/ChronoField;

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1b

    .line 445
    iget-object v0, p0, Lorg/threeten/bp/format/DateTimeBuilder;->a:Ljava/util/Map;

    sget-object v1, Lorg/threeten/bp/temporal/ChronoField;->c:Lorg/threeten/bp/temporal/ChronoField;

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    .line 446
    sget-object v2, Lorg/threeten/bp/temporal/ChronoField;->a:Lorg/threeten/bp/temporal/ChronoField;

    const-wide/16 v4, 0x3e8

    mul-long/2addr v0, v4

    invoke-virtual {p0, v2, v0, v1}, Lorg/threeten/bp/format/DateTimeBuilder;->a(Lorg/threeten/bp/temporal/TemporalField;J)Lorg/threeten/bp/format/DateTimeBuilder;

    .line 451
    :cond_18
    :goto_2
    return-void

    .line 335
    :cond_19
    sget-object v2, Lorg/threeten/bp/temporal/ChronoField;->n:Lorg/threeten/bp/temporal/ChronoField;

    invoke-virtual {v2, v0, v1}, Lorg/threeten/bp/temporal/ChronoField;->a(J)J

    goto/16 :goto_0

    .line 346
    :cond_1a
    sget-object v2, Lorg/threeten/bp/temporal/ChronoField;->l:Lorg/threeten/bp/temporal/ChronoField;

    invoke-virtual {v2, v0, v1}, Lorg/threeten/bp/temporal/ChronoField;->a(J)J

    goto/16 :goto_1

    .line 447
    :cond_1b
    iget-object v0, p0, Lorg/threeten/bp/format/DateTimeBuilder;->a:Ljava/util/Map;

    sget-object v1, Lorg/threeten/bp/temporal/ChronoField;->e:Lorg/threeten/bp/temporal/ChronoField;

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_18

    .line 448
    iget-object v0, p0, Lorg/threeten/bp/format/DateTimeBuilder;->a:Ljava/util/Map;

    sget-object v1, Lorg/threeten/bp/temporal/ChronoField;->e:Lorg/threeten/bp/temporal/ChronoField;

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    .line 449
    sget-object v2, Lorg/threeten/bp/temporal/ChronoField;->a:Lorg/threeten/bp/temporal/ChronoField;

    const-wide/32 v4, 0xf4240

    mul-long/2addr v0, v4

    invoke-virtual {p0, v2, v0, v1}, Lorg/threeten/bp/format/DateTimeBuilder;->a(Lorg/threeten/bp/temporal/TemporalField;J)Lorg/threeten/bp/format/DateTimeBuilder;

    goto :goto_2
.end method

.method private d()V
    .locals 4

    .prologue
    .line 616
    iget-object v0, p0, Lorg/threeten/bp/format/DateTimeBuilder;->d:Lorg/threeten/bp/chrono/ChronoLocalDate;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/threeten/bp/format/DateTimeBuilder;->e:Lorg/threeten/bp/LocalTime;

    if-eqz v0, :cond_0

    .line 617
    iget-object v0, p0, Lorg/threeten/bp/format/DateTimeBuilder;->c:Lorg/threeten/bp/ZoneId;

    if-eqz v0, :cond_1

    .line 618
    iget-object v0, p0, Lorg/threeten/bp/format/DateTimeBuilder;->d:Lorg/threeten/bp/chrono/ChronoLocalDate;

    iget-object v1, p0, Lorg/threeten/bp/format/DateTimeBuilder;->e:Lorg/threeten/bp/LocalTime;

    invoke-virtual {v0, v1}, Lorg/threeten/bp/chrono/ChronoLocalDate;->b(Lorg/threeten/bp/LocalTime;)Lorg/threeten/bp/chrono/ChronoLocalDateTime;

    move-result-object v0

    iget-object v1, p0, Lorg/threeten/bp/format/DateTimeBuilder;->c:Lorg/threeten/bp/ZoneId;

    invoke-virtual {v0, v1}, Lorg/threeten/bp/chrono/ChronoLocalDateTime;->b(Lorg/threeten/bp/ZoneId;)Lorg/threeten/bp/chrono/ChronoZonedDateTime;

    move-result-object v0

    sget-object v1, Lorg/threeten/bp/temporal/ChronoField;->C:Lorg/threeten/bp/temporal/ChronoField;

    invoke-virtual {v0, v1}, Lorg/threeten/bp/chrono/ChronoZonedDateTime;->d(Lorg/threeten/bp/temporal/TemporalField;)J

    move-result-wide v0

    .line 619
    iget-object v2, p0, Lorg/threeten/bp/format/DateTimeBuilder;->a:Ljava/util/Map;

    sget-object v3, Lorg/threeten/bp/temporal/ChronoField;->C:Lorg/threeten/bp/temporal/ChronoField;

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-interface {v2, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 629
    :cond_0
    :goto_0
    return-void

    .line 621
    :cond_1
    iget-object v0, p0, Lorg/threeten/bp/format/DateTimeBuilder;->a:Ljava/util/Map;

    sget-object v1, Lorg/threeten/bp/temporal/ChronoField;->D:Lorg/threeten/bp/temporal/ChronoField;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    .line 622
    if-eqz v0, :cond_0

    .line 623
    invoke-virtual {v0}, Ljava/lang/Long;->intValue()I

    move-result v0

    invoke-static {v0}, Lorg/threeten/bp/ZoneOffset;->a(I)Lorg/threeten/bp/ZoneOffset;

    move-result-object v0

    .line 624
    iget-object v1, p0, Lorg/threeten/bp/format/DateTimeBuilder;->d:Lorg/threeten/bp/chrono/ChronoLocalDate;

    iget-object v2, p0, Lorg/threeten/bp/format/DateTimeBuilder;->e:Lorg/threeten/bp/LocalTime;

    invoke-virtual {v1, v2}, Lorg/threeten/bp/chrono/ChronoLocalDate;->b(Lorg/threeten/bp/LocalTime;)Lorg/threeten/bp/chrono/ChronoLocalDateTime;

    move-result-object v1

    invoke-virtual {v1, v0}, Lorg/threeten/bp/chrono/ChronoLocalDateTime;->b(Lorg/threeten/bp/ZoneId;)Lorg/threeten/bp/chrono/ChronoZonedDateTime;

    move-result-object v0

    sget-object v1, Lorg/threeten/bp/temporal/ChronoField;->C:Lorg/threeten/bp/temporal/ChronoField;

    invoke-virtual {v0, v1}, Lorg/threeten/bp/chrono/ChronoZonedDateTime;->d(Lorg/threeten/bp/temporal/TemporalField;)J

    move-result-wide v0

    .line 625
    iget-object v2, p0, Lorg/threeten/bp/format/DateTimeBuilder;->a:Ljava/util/Map;

    sget-object v3, Lorg/threeten/bp/temporal/ChronoField;->C:Lorg/threeten/bp/temporal/ChronoField;

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-interface {v2, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method private d(Lorg/threeten/bp/format/ResolverStyle;)V
    .locals 8

    .prologue
    .line 454
    iget-object v0, p0, Lorg/threeten/bp/format/DateTimeBuilder;->a:Ljava/util/Map;

    sget-object v1, Lorg/threeten/bp/temporal/ChronoField;->m:Lorg/threeten/bp/temporal/ChronoField;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    .line 455
    iget-object v1, p0, Lorg/threeten/bp/format/DateTimeBuilder;->a:Ljava/util/Map;

    sget-object v2, Lorg/threeten/bp/temporal/ChronoField;->i:Lorg/threeten/bp/temporal/ChronoField;

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    .line 456
    iget-object v2, p0, Lorg/threeten/bp/format/DateTimeBuilder;->a:Ljava/util/Map;

    sget-object v3, Lorg/threeten/bp/temporal/ChronoField;->g:Lorg/threeten/bp/temporal/ChronoField;

    invoke-interface {v2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Long;

    .line 457
    iget-object v3, p0, Lorg/threeten/bp/format/DateTimeBuilder;->a:Ljava/util/Map;

    sget-object v4, Lorg/threeten/bp/temporal/ChronoField;->a:Lorg/threeten/bp/temporal/ChronoField;

    invoke-interface {v3, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Long;

    .line 458
    if-nez v0, :cond_1

    .line 535
    :cond_0
    :goto_0
    return-void

    .line 461
    :cond_1
    if-nez v1, :cond_2

    if-nez v2, :cond_0

    if-nez v3, :cond_0

    .line 464
    :cond_2
    if-eqz v1, :cond_3

    if-nez v2, :cond_3

    if-nez v3, :cond_0

    .line 467
    :cond_3
    sget-object v4, Lorg/threeten/bp/format/ResolverStyle;->c:Lorg/threeten/bp/format/ResolverStyle;

    if-eq p1, v4, :cond_c

    .line 468
    if-eqz v0, :cond_8

    .line 469
    sget-object v4, Lorg/threeten/bp/format/ResolverStyle;->b:Lorg/threeten/bp/format/ResolverStyle;

    if-ne p1, v4, :cond_7

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    const-wide/16 v6, 0x18

    cmp-long v4, v4, v6

    if-nez v4, :cond_7

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    const-wide/16 v6, 0x0

    cmp-long v4, v4, v6

    if-nez v4, :cond_7

    :cond_4
    if-eqz v2, :cond_5

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    const-wide/16 v6, 0x0

    cmp-long v4, v4, v6

    if-nez v4, :cond_7

    :cond_5
    if-eqz v3, :cond_6

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    const-wide/16 v6, 0x0

    cmp-long v4, v4, v6

    if-nez v4, :cond_7

    .line 474
    :cond_6
    const-wide/16 v4, 0x0

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    .line 475
    const/4 v4, 0x1

    invoke-static {v4}, Lorg/threeten/bp/Period;->a(I)Lorg/threeten/bp/Period;

    move-result-object v4

    iput-object v4, p0, Lorg/threeten/bp/format/DateTimeBuilder;->g:Lorg/threeten/bp/Period;

    .line 477
    :cond_7
    sget-object v4, Lorg/threeten/bp/temporal/ChronoField;->m:Lorg/threeten/bp/temporal/ChronoField;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    invoke-virtual {v4, v6, v7}, Lorg/threeten/bp/temporal/ChronoField;->b(J)I

    move-result v0

    .line 478
    if-eqz v1, :cond_b

    .line 479
    sget-object v4, Lorg/threeten/bp/temporal/ChronoField;->i:Lorg/threeten/bp/temporal/ChronoField;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    invoke-virtual {v4, v6, v7}, Lorg/threeten/bp/temporal/ChronoField;->b(J)I

    move-result v1

    .line 480
    if-eqz v2, :cond_a

    .line 481
    sget-object v4, Lorg/threeten/bp/temporal/ChronoField;->g:Lorg/threeten/bp/temporal/ChronoField;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    invoke-virtual {v4, v6, v7}, Lorg/threeten/bp/temporal/ChronoField;->b(J)I

    move-result v2

    .line 482
    if-eqz v3, :cond_9

    .line 483
    sget-object v4, Lorg/threeten/bp/temporal/ChronoField;->a:Lorg/threeten/bp/temporal/ChronoField;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    invoke-virtual {v4, v6, v7}, Lorg/threeten/bp/temporal/ChronoField;->b(J)I

    move-result v3

    .line 484
    invoke-static {v0, v1, v2, v3}, Lorg/threeten/bp/LocalTime;->a(IIII)Lorg/threeten/bp/LocalTime;

    move-result-object v0

    invoke-virtual {p0, v0}, Lorg/threeten/bp/format/DateTimeBuilder;->a(Lorg/threeten/bp/LocalTime;)V

    .line 531
    :cond_8
    :goto_1
    iget-object v0, p0, Lorg/threeten/bp/format/DateTimeBuilder;->a:Ljava/util/Map;

    sget-object v1, Lorg/threeten/bp/temporal/ChronoField;->m:Lorg/threeten/bp/temporal/ChronoField;

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 532
    iget-object v0, p0, Lorg/threeten/bp/format/DateTimeBuilder;->a:Ljava/util/Map;

    sget-object v1, Lorg/threeten/bp/temporal/ChronoField;->i:Lorg/threeten/bp/temporal/ChronoField;

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 533
    iget-object v0, p0, Lorg/threeten/bp/format/DateTimeBuilder;->a:Ljava/util/Map;

    sget-object v1, Lorg/threeten/bp/temporal/ChronoField;->g:Lorg/threeten/bp/temporal/ChronoField;

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 534
    iget-object v0, p0, Lorg/threeten/bp/format/DateTimeBuilder;->a:Ljava/util/Map;

    sget-object v1, Lorg/threeten/bp/temporal/ChronoField;->a:Lorg/threeten/bp/temporal/ChronoField;

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_0

    .line 486
    :cond_9
    invoke-static {v0, v1, v2}, Lorg/threeten/bp/LocalTime;->a(III)Lorg/threeten/bp/LocalTime;

    move-result-object v0

    invoke-virtual {p0, v0}, Lorg/threeten/bp/format/DateTimeBuilder;->a(Lorg/threeten/bp/LocalTime;)V

    goto :goto_1

    .line 489
    :cond_a
    if-nez v3, :cond_8

    .line 490
    invoke-static {v0, v1}, Lorg/threeten/bp/LocalTime;->a(II)Lorg/threeten/bp/LocalTime;

    move-result-object v0

    invoke-virtual {p0, v0}, Lorg/threeten/bp/format/DateTimeBuilder;->a(Lorg/threeten/bp/LocalTime;)V

    goto :goto_1

    .line 494
    :cond_b
    if-nez v2, :cond_8

    if-nez v3, :cond_8

    .line 495
    const/4 v1, 0x0

    invoke-static {v0, v1}, Lorg/threeten/bp/LocalTime;->a(II)Lorg/threeten/bp/LocalTime;

    move-result-object v0

    invoke-virtual {p0, v0}, Lorg/threeten/bp/format/DateTimeBuilder;->a(Lorg/threeten/bp/LocalTime;)V

    goto :goto_1

    .line 500
    :cond_c
    if-eqz v0, :cond_8

    .line 501
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    .line 502
    if-eqz v1, :cond_f

    .line 503
    if-eqz v2, :cond_e

    .line 504
    if-nez v3, :cond_d

    .line 505
    const-wide/16 v6, 0x0

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    .line 507
    :cond_d
    const-wide v6, 0x34630b8a000L

    invoke-static {v4, v5, v6, v7}, Lorg/threeten/bp/jdk8/Jdk8Methods;->d(JJ)J

    move-result-wide v4

    .line 508
    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    const-wide v6, 0xdf8475800L

    invoke-static {v0, v1, v6, v7}, Lorg/threeten/bp/jdk8/Jdk8Methods;->d(JJ)J

    move-result-wide v0

    invoke-static {v4, v5, v0, v1}, Lorg/threeten/bp/jdk8/Jdk8Methods;->b(JJ)J

    move-result-wide v0

    .line 509
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    const-wide/32 v6, 0x3b9aca00

    invoke-static {v4, v5, v6, v7}, Lorg/threeten/bp/jdk8/Jdk8Methods;->d(JJ)J

    move-result-wide v4

    invoke-static {v0, v1, v4, v5}, Lorg/threeten/bp/jdk8/Jdk8Methods;->b(JJ)J

    move-result-wide v0

    .line 510
    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v0, v1, v2, v3}, Lorg/threeten/bp/jdk8/Jdk8Methods;->b(JJ)J

    move-result-wide v0

    .line 511
    const-wide v2, 0x4e94914f0000L

    invoke-static {v0, v1, v2, v3}, Lorg/threeten/bp/jdk8/Jdk8Methods;->e(JJ)J

    move-result-wide v2

    long-to-int v2, v2

    .line 512
    const-wide v4, 0x4e94914f0000L

    invoke-static {v0, v1, v4, v5}, Lorg/threeten/bp/jdk8/Jdk8Methods;->f(JJ)J

    move-result-wide v0

    .line 513
    invoke-static {v0, v1}, Lorg/threeten/bp/LocalTime;->b(J)Lorg/threeten/bp/LocalTime;

    move-result-object v0

    invoke-virtual {p0, v0}, Lorg/threeten/bp/format/DateTimeBuilder;->a(Lorg/threeten/bp/LocalTime;)V

    .line 514
    invoke-static {v2}, Lorg/threeten/bp/Period;->a(I)Lorg/threeten/bp/Period;

    move-result-object v0

    iput-object v0, p0, Lorg/threeten/bp/format/DateTimeBuilder;->g:Lorg/threeten/bp/Period;

    goto/16 :goto_1

    .line 516
    :cond_e
    const-wide/16 v2, 0xe10

    invoke-static {v4, v5, v2, v3}, Lorg/threeten/bp/jdk8/Jdk8Methods;->d(JJ)J

    move-result-wide v2

    .line 517
    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    const-wide/16 v4, 0x3c

    invoke-static {v0, v1, v4, v5}, Lorg/threeten/bp/jdk8/Jdk8Methods;->d(JJ)J

    move-result-wide v0

    invoke-static {v2, v3, v0, v1}, Lorg/threeten/bp/jdk8/Jdk8Methods;->b(JJ)J

    move-result-wide v0

    .line 518
    const-wide/32 v2, 0x15180

    invoke-static {v0, v1, v2, v3}, Lorg/threeten/bp/jdk8/Jdk8Methods;->e(JJ)J

    move-result-wide v2

    long-to-int v2, v2

    .line 519
    const-wide/32 v4, 0x15180

    invoke-static {v0, v1, v4, v5}, Lorg/threeten/bp/jdk8/Jdk8Methods;->f(JJ)J

    move-result-wide v0

    .line 520
    invoke-static {v0, v1}, Lorg/threeten/bp/LocalTime;->a(J)Lorg/threeten/bp/LocalTime;

    move-result-object v0

    invoke-virtual {p0, v0}, Lorg/threeten/bp/format/DateTimeBuilder;->a(Lorg/threeten/bp/LocalTime;)V

    .line 521
    invoke-static {v2}, Lorg/threeten/bp/Period;->a(I)Lorg/threeten/bp/Period;

    move-result-object v0

    iput-object v0, p0, Lorg/threeten/bp/format/DateTimeBuilder;->g:Lorg/threeten/bp/Period;

    goto/16 :goto_1

    .line 524
    :cond_f
    const-wide/16 v0, 0x18

    invoke-static {v4, v5, v0, v1}, Lorg/threeten/bp/jdk8/Jdk8Methods;->e(JJ)J

    move-result-wide v0

    invoke-static {v0, v1}, Lorg/threeten/bp/jdk8/Jdk8Methods;->a(J)I

    move-result v0

    .line 525
    const/16 v1, 0x18

    invoke-static {v4, v5, v1}, Lorg/threeten/bp/jdk8/Jdk8Methods;->b(JI)I

    move-result v1

    int-to-long v2, v1

    .line 526
    long-to-int v1, v2

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lorg/threeten/bp/LocalTime;->a(II)Lorg/threeten/bp/LocalTime;

    move-result-object v1

    invoke-virtual {p0, v1}, Lorg/threeten/bp/format/DateTimeBuilder;->a(Lorg/threeten/bp/LocalTime;)V

    .line 527
    invoke-static {v0}, Lorg/threeten/bp/Period;->a(I)Lorg/threeten/bp/Period;

    move-result-object v0

    iput-object v0, p0, Lorg/threeten/bp/format/DateTimeBuilder;->g:Lorg/threeten/bp/Period;

    goto/16 :goto_1
.end method

.method private e(Lorg/threeten/bp/temporal/TemporalField;)Ljava/lang/Long;
    .locals 1

    .prologue
    .line 149
    iget-object v0, p0, Lorg/threeten/bp/format/DateTimeBuilder;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    return-object v0
.end method


# virtual methods
.method public a(Lorg/threeten/bp/temporal/TemporalQuery;)Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<R:",
            "Ljava/lang/Object;",
            ">(",
            "Lorg/threeten/bp/temporal/TemporalQuery",
            "<TR;>;)TR;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 677
    invoke-static {}, Lorg/threeten/bp/temporal/TemporalQueries;->a()Lorg/threeten/bp/temporal/TemporalQuery;

    move-result-object v1

    if-ne p1, v1, :cond_1

    .line 678
    iget-object v0, p0, Lorg/threeten/bp/format/DateTimeBuilder;->c:Lorg/threeten/bp/ZoneId;

    .line 692
    :cond_0
    :goto_0
    return-object v0

    .line 679
    :cond_1
    invoke-static {}, Lorg/threeten/bp/temporal/TemporalQueries;->b()Lorg/threeten/bp/temporal/TemporalQuery;

    move-result-object v1

    if-ne p1, v1, :cond_2

    .line 680
    iget-object v0, p0, Lorg/threeten/bp/format/DateTimeBuilder;->b:Lorg/threeten/bp/chrono/Chronology;

    goto :goto_0

    .line 681
    :cond_2
    invoke-static {}, Lorg/threeten/bp/temporal/TemporalQueries;->f()Lorg/threeten/bp/temporal/TemporalQuery;

    move-result-object v1

    if-ne p1, v1, :cond_3

    .line 682
    iget-object v1, p0, Lorg/threeten/bp/format/DateTimeBuilder;->d:Lorg/threeten/bp/chrono/ChronoLocalDate;

    if-eqz v1, :cond_0

    iget-object v0, p0, Lorg/threeten/bp/format/DateTimeBuilder;->d:Lorg/threeten/bp/chrono/ChronoLocalDate;

    invoke-static {v0}, Lorg/threeten/bp/LocalDate;->a(Lorg/threeten/bp/temporal/TemporalAccessor;)Lorg/threeten/bp/LocalDate;

    move-result-object v0

    goto :goto_0

    .line 683
    :cond_3
    invoke-static {}, Lorg/threeten/bp/temporal/TemporalQueries;->g()Lorg/threeten/bp/temporal/TemporalQuery;

    move-result-object v1

    if-ne p1, v1, :cond_4

    .line 684
    iget-object v0, p0, Lorg/threeten/bp/format/DateTimeBuilder;->e:Lorg/threeten/bp/LocalTime;

    goto :goto_0

    .line 685
    :cond_4
    invoke-static {}, Lorg/threeten/bp/temporal/TemporalQueries;->d()Lorg/threeten/bp/temporal/TemporalQuery;

    move-result-object v1

    if-eq p1, v1, :cond_5

    invoke-static {}, Lorg/threeten/bp/temporal/TemporalQueries;->e()Lorg/threeten/bp/temporal/TemporalQuery;

    move-result-object v1

    if-ne p1, v1, :cond_6

    .line 686
    :cond_5
    invoke-interface {p1, p0}, Lorg/threeten/bp/temporal/TemporalQuery;->b(Lorg/threeten/bp/temporal/TemporalAccessor;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0

    .line 687
    :cond_6
    invoke-static {}, Lorg/threeten/bp/temporal/TemporalQueries;->c()Lorg/threeten/bp/temporal/TemporalQuery;

    move-result-object v1

    if-eq p1, v1, :cond_0

    .line 692
    invoke-interface {p1, p0}, Lorg/threeten/bp/temporal/TemporalQuery;->b(Lorg/threeten/bp/temporal/TemporalAccessor;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method public a(Lorg/threeten/bp/format/ResolverStyle;Ljava/util/Set;)Lorg/threeten/bp/format/DateTimeBuilder;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/threeten/bp/format/ResolverStyle;",
            "Ljava/util/Set",
            "<",
            "Lorg/threeten/bp/temporal/TemporalField;",
            ">;)",
            "Lorg/threeten/bp/format/DateTimeBuilder;"
        }
    .end annotation

    .prologue
    .line 201
    if-eqz p2, :cond_0

    .line 202
    iget-object v0, p0, Lorg/threeten/bp/format/DateTimeBuilder;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0, p2}, Ljava/util/Set;->retainAll(Ljava/util/Collection;)Z

    .line 205
    :cond_0
    invoke-direct {p0}, Lorg/threeten/bp/format/DateTimeBuilder;->a()V

    .line 206
    invoke-direct {p0, p1}, Lorg/threeten/bp/format/DateTimeBuilder;->b(Lorg/threeten/bp/format/ResolverStyle;)V

    .line 207
    invoke-direct {p0, p1}, Lorg/threeten/bp/format/DateTimeBuilder;->c(Lorg/threeten/bp/format/ResolverStyle;)V

    .line 208
    invoke-direct {p0, p1}, Lorg/threeten/bp/format/DateTimeBuilder;->a(Lorg/threeten/bp/format/ResolverStyle;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 209
    invoke-direct {p0}, Lorg/threeten/bp/format/DateTimeBuilder;->a()V

    .line 210
    invoke-direct {p0, p1}, Lorg/threeten/bp/format/DateTimeBuilder;->b(Lorg/threeten/bp/format/ResolverStyle;)V

    .line 211
    invoke-direct {p0, p1}, Lorg/threeten/bp/format/DateTimeBuilder;->c(Lorg/threeten/bp/format/ResolverStyle;)V

    .line 213
    :cond_1
    invoke-direct {p0, p1}, Lorg/threeten/bp/format/DateTimeBuilder;->d(Lorg/threeten/bp/format/ResolverStyle;)V

    .line 214
    invoke-direct {p0}, Lorg/threeten/bp/format/DateTimeBuilder;->b()V

    .line 215
    iget-object v0, p0, Lorg/threeten/bp/format/DateTimeBuilder;->g:Lorg/threeten/bp/Period;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lorg/threeten/bp/format/DateTimeBuilder;->g:Lorg/threeten/bp/Period;

    invoke-virtual {v0}, Lorg/threeten/bp/Period;->b()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lorg/threeten/bp/format/DateTimeBuilder;->d:Lorg/threeten/bp/chrono/ChronoLocalDate;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lorg/threeten/bp/format/DateTimeBuilder;->e:Lorg/threeten/bp/LocalTime;

    if-eqz v0, :cond_2

    .line 216
    iget-object v0, p0, Lorg/threeten/bp/format/DateTimeBuilder;->d:Lorg/threeten/bp/chrono/ChronoLocalDate;

    iget-object v1, p0, Lorg/threeten/bp/format/DateTimeBuilder;->g:Lorg/threeten/bp/Period;

    invoke-virtual {v0, v1}, Lorg/threeten/bp/chrono/ChronoLocalDate;->b(Lorg/threeten/bp/temporal/TemporalAmount;)Lorg/threeten/bp/chrono/ChronoLocalDate;

    move-result-object v0

    iput-object v0, p0, Lorg/threeten/bp/format/DateTimeBuilder;->d:Lorg/threeten/bp/chrono/ChronoLocalDate;

    .line 217
    sget-object v0, Lorg/threeten/bp/Period;->a:Lorg/threeten/bp/Period;

    iput-object v0, p0, Lorg/threeten/bp/format/DateTimeBuilder;->g:Lorg/threeten/bp/Period;

    .line 219
    :cond_2
    invoke-direct {p0}, Lorg/threeten/bp/format/DateTimeBuilder;->c()V

    .line 220
    invoke-direct {p0}, Lorg/threeten/bp/format/DateTimeBuilder;->d()V

    .line 221
    return-object p0
.end method

.method a(Lorg/threeten/bp/temporal/TemporalField;J)Lorg/threeten/bp/format/DateTimeBuilder;
    .locals 4

    .prologue
    .line 167
    const-string v0, "field"

    invoke-static {p1, v0}, Lorg/threeten/bp/jdk8/Jdk8Methods;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 168
    invoke-direct {p0, p1}, Lorg/threeten/bp/format/DateTimeBuilder;->e(Lorg/threeten/bp/temporal/TemporalField;)Ljava/lang/Long;

    move-result-object v0

    .line 169
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    cmp-long v1, v2, p2

    if-eqz v1, :cond_0

    .line 170
    new-instance v1, Lorg/threeten/bp/DateTimeException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Conflict found: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " differs from "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ": "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lorg/threeten/bp/DateTimeException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 172
    :cond_0
    invoke-direct {p0, p1, p2, p3}, Lorg/threeten/bp/format/DateTimeBuilder;->b(Lorg/threeten/bp/temporal/TemporalField;J)Lorg/threeten/bp/format/DateTimeBuilder;

    move-result-object v0

    return-object v0
.end method

.method a(Lorg/threeten/bp/LocalTime;)V
    .locals 0

    .prologue
    .line 186
    iput-object p1, p0, Lorg/threeten/bp/format/DateTimeBuilder;->e:Lorg/threeten/bp/LocalTime;

    .line 187
    return-void
.end method

.method a(Lorg/threeten/bp/chrono/ChronoLocalDate;)V
    .locals 0

    .prologue
    .line 182
    iput-object p1, p0, Lorg/threeten/bp/format/DateTimeBuilder;->d:Lorg/threeten/bp/chrono/ChronoLocalDate;

    .line 183
    return-void
.end method

.method public a(Lorg/threeten/bp/temporal/TemporalField;)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 650
    if-nez p1, :cond_1

    .line 653
    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Lorg/threeten/bp/format/DateTimeBuilder;->a:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    iget-object v1, p0, Lorg/threeten/bp/format/DateTimeBuilder;->d:Lorg/threeten/bp/chrono/ChronoLocalDate;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lorg/threeten/bp/format/DateTimeBuilder;->d:Lorg/threeten/bp/chrono/ChronoLocalDate;

    invoke-virtual {v1, p1}, Lorg/threeten/bp/chrono/ChronoLocalDate;->a(Lorg/threeten/bp/temporal/TemporalField;)Z

    move-result v1

    if-nez v1, :cond_3

    :cond_2
    iget-object v1, p0, Lorg/threeten/bp/format/DateTimeBuilder;->e:Lorg/threeten/bp/LocalTime;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lorg/threeten/bp/format/DateTimeBuilder;->e:Lorg/threeten/bp/LocalTime;

    invoke-virtual {v1, p1}, Lorg/threeten/bp/LocalTime;->a(Lorg/threeten/bp/temporal/TemporalField;)Z

    move-result v1

    if-eqz v1, :cond_0

    :cond_3
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public b(Lorg/threeten/bp/temporal/TemporalQuery;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<R:",
            "Ljava/lang/Object;",
            ">(",
            "Lorg/threeten/bp/temporal/TemporalQuery",
            "<TR;>;)TR;"
        }
    .end annotation

    .prologue
    .line 644
    invoke-interface {p1, p0}, Lorg/threeten/bp/temporal/TemporalQuery;->b(Lorg/threeten/bp/temporal/TemporalAccessor;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public d(Lorg/threeten/bp/temporal/TemporalField;)J
    .locals 3

    .prologue
    .line 660
    const-string v0, "field"

    invoke-static {p1, v0}, Lorg/threeten/bp/jdk8/Jdk8Methods;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 661
    invoke-direct {p0, p1}, Lorg/threeten/bp/format/DateTimeBuilder;->e(Lorg/threeten/bp/temporal/TemporalField;)Ljava/lang/Long;

    move-result-object v0

    .line 662
    if-nez v0, :cond_2

    .line 663
    iget-object v0, p0, Lorg/threeten/bp/format/DateTimeBuilder;->d:Lorg/threeten/bp/chrono/ChronoLocalDate;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/threeten/bp/format/DateTimeBuilder;->d:Lorg/threeten/bp/chrono/ChronoLocalDate;

    invoke-virtual {v0, p1}, Lorg/threeten/bp/chrono/ChronoLocalDate;->a(Lorg/threeten/bp/temporal/TemporalField;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 664
    iget-object v0, p0, Lorg/threeten/bp/format/DateTimeBuilder;->d:Lorg/threeten/bp/chrono/ChronoLocalDate;

    invoke-virtual {v0, p1}, Lorg/threeten/bp/chrono/ChronoLocalDate;->d(Lorg/threeten/bp/temporal/TemporalField;)J

    move-result-wide v0

    .line 671
    :goto_0
    return-wide v0

    .line 666
    :cond_0
    iget-object v0, p0, Lorg/threeten/bp/format/DateTimeBuilder;->e:Lorg/threeten/bp/LocalTime;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lorg/threeten/bp/format/DateTimeBuilder;->e:Lorg/threeten/bp/LocalTime;

    invoke-virtual {v0, p1}, Lorg/threeten/bp/LocalTime;->a(Lorg/threeten/bp/temporal/TemporalField;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 667
    iget-object v0, p0, Lorg/threeten/bp/format/DateTimeBuilder;->e:Lorg/threeten/bp/LocalTime;

    invoke-virtual {v0, p1}, Lorg/threeten/bp/LocalTime;->d(Lorg/threeten/bp/temporal/TemporalField;)J

    move-result-wide v0

    goto :goto_0

    .line 669
    :cond_1
    new-instance v0, Lorg/threeten/bp/DateTimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Field not found: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/threeten/bp/DateTimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 671
    :cond_2
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 698
    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 699
    const-string v1, "DateTimeBuilder["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 700
    iget-object v1, p0, Lorg/threeten/bp/format/DateTimeBuilder;->a:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->size()I

    move-result v1

    if-lez v1, :cond_0

    .line 701
    const-string v1, "fields="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lorg/threeten/bp/format/DateTimeBuilder;->a:Ljava/util/Map;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 703
    :cond_0
    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lorg/threeten/bp/format/DateTimeBuilder;->b:Lorg/threeten/bp/chrono/Chronology;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 704
    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lorg/threeten/bp/format/DateTimeBuilder;->c:Lorg/threeten/bp/ZoneId;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 705
    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lorg/threeten/bp/format/DateTimeBuilder;->d:Lorg/threeten/bp/chrono/ChronoLocalDate;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 706
    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lorg/threeten/bp/format/DateTimeBuilder;->e:Lorg/threeten/bp/LocalTime;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 707
    const/16 v1, 0x5d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 708
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
