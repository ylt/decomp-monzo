.class final Lorg/threeten/bp/format/DateTimeFormatterBuilder$FractionPrinterParser;
.super Ljava/lang/Object;
.source "DateTimeFormatterBuilder.java"

# interfaces
.implements Lorg/threeten/bp/format/DateTimeFormatterBuilder$DateTimePrinterParser;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/threeten/bp/format/DateTimeFormatterBuilder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "FractionPrinterParser"
.end annotation


# instance fields
.field private final a:Lorg/threeten/bp/temporal/TemporalField;

.field private final b:I

.field private final c:I

.field private final d:Z


# direct methods
.method constructor <init>(Lorg/threeten/bp/temporal/TemporalField;IIZ)V
    .locals 3

    .prologue
    const/16 v1, 0x9

    .line 2674
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2675
    const-string v0, "field"

    invoke-static {p1, v0}, Lorg/threeten/bp/jdk8/Jdk8Methods;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 2676
    invoke-interface {p1}, Lorg/threeten/bp/temporal/TemporalField;->a()Lorg/threeten/bp/temporal/ValueRange;

    move-result-object v0

    invoke-virtual {v0}, Lorg/threeten/bp/temporal/ValueRange;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2677
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Field must have a fixed set of values: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2679
    :cond_0
    if-ltz p2, :cond_1

    if-le p2, v1, :cond_2

    .line 2680
    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Minimum width must be from 0 to 9 inclusive but was "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2682
    :cond_2
    const/4 v0, 0x1

    if-lt p3, v0, :cond_3

    if-le p3, v1, :cond_4

    .line 2683
    :cond_3
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Maximum width must be from 1 to 9 inclusive but was "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2685
    :cond_4
    if-ge p3, p2, :cond_5

    .line 2686
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Maximum width must exceed or equal the minimum width but "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " < "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2689
    :cond_5
    iput-object p1, p0, Lorg/threeten/bp/format/DateTimeFormatterBuilder$FractionPrinterParser;->a:Lorg/threeten/bp/temporal/TemporalField;

    .line 2690
    iput p2, p0, Lorg/threeten/bp/format/DateTimeFormatterBuilder$FractionPrinterParser;->b:I

    .line 2691
    iput p3, p0, Lorg/threeten/bp/format/DateTimeFormatterBuilder$FractionPrinterParser;->c:I

    .line 2692
    iput-boolean p4, p0, Lorg/threeten/bp/format/DateTimeFormatterBuilder$FractionPrinterParser;->d:Z

    .line 2693
    return-void
.end method

.method private a(Ljava/math/BigDecimal;)J
    .locals 4

    .prologue
    .line 2809
    iget-object v0, p0, Lorg/threeten/bp/format/DateTimeFormatterBuilder$FractionPrinterParser;->a:Lorg/threeten/bp/temporal/TemporalField;

    invoke-interface {v0}, Lorg/threeten/bp/temporal/TemporalField;->a()Lorg/threeten/bp/temporal/ValueRange;

    move-result-object v0

    .line 2810
    invoke-virtual {v0}, Lorg/threeten/bp/temporal/ValueRange;->b()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/math/BigDecimal;->valueOf(J)Ljava/math/BigDecimal;

    move-result-object v1

    .line 2811
    invoke-virtual {v0}, Lorg/threeten/bp/temporal/ValueRange;->c()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/math/BigDecimal;->valueOf(J)Ljava/math/BigDecimal;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/math/BigDecimal;->subtract(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object v0

    sget-object v2, Ljava/math/BigDecimal;->ONE:Ljava/math/BigDecimal;

    invoke-virtual {v0, v2}, Ljava/math/BigDecimal;->add(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object v0

    .line 2812
    invoke-virtual {p1, v0}, Ljava/math/BigDecimal;->multiply(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object v0

    const/4 v2, 0x0

    sget-object v3, Ljava/math/RoundingMode;->FLOOR:Ljava/math/RoundingMode;

    invoke-virtual {v0, v2, v3}, Ljava/math/BigDecimal;->setScale(ILjava/math/RoundingMode;)Ljava/math/BigDecimal;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/math/BigDecimal;->add(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object v0

    .line 2813
    invoke-virtual {v0}, Ljava/math/BigDecimal;->longValueExact()J

    move-result-wide v0

    return-wide v0
.end method

.method private a(J)Ljava/math/BigDecimal;
    .locals 5

    .prologue
    .line 2782
    iget-object v0, p0, Lorg/threeten/bp/format/DateTimeFormatterBuilder$FractionPrinterParser;->a:Lorg/threeten/bp/temporal/TemporalField;

    invoke-interface {v0}, Lorg/threeten/bp/temporal/TemporalField;->a()Lorg/threeten/bp/temporal/ValueRange;

    move-result-object v0

    .line 2783
    iget-object v1, p0, Lorg/threeten/bp/format/DateTimeFormatterBuilder$FractionPrinterParser;->a:Lorg/threeten/bp/temporal/TemporalField;

    invoke-virtual {v0, p1, p2, v1}, Lorg/threeten/bp/temporal/ValueRange;->a(JLorg/threeten/bp/temporal/TemporalField;)J

    .line 2784
    invoke-virtual {v0}, Lorg/threeten/bp/temporal/ValueRange;->b()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/math/BigDecimal;->valueOf(J)Ljava/math/BigDecimal;

    move-result-object v1

    .line 2785
    invoke-virtual {v0}, Lorg/threeten/bp/temporal/ValueRange;->c()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/math/BigDecimal;->valueOf(J)Ljava/math/BigDecimal;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/math/BigDecimal;->subtract(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object v0

    sget-object v2, Ljava/math/BigDecimal;->ONE:Ljava/math/BigDecimal;

    invoke-virtual {v0, v2}, Ljava/math/BigDecimal;->add(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object v0

    .line 2786
    invoke-static {p1, p2}, Ljava/math/BigDecimal;->valueOf(J)Ljava/math/BigDecimal;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/math/BigDecimal;->subtract(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object v1

    .line 2787
    const/16 v2, 0x9

    sget-object v3, Ljava/math/RoundingMode;->FLOOR:Ljava/math/RoundingMode;

    invoke-virtual {v1, v0, v2, v3}, Ljava/math/BigDecimal;->divide(Ljava/math/BigDecimal;ILjava/math/RoundingMode;)Ljava/math/BigDecimal;

    move-result-object v0

    .line 2789
    sget-object v1, Ljava/math/BigDecimal;->ZERO:Ljava/math/BigDecimal;

    invoke-virtual {v0, v1}, Ljava/math/BigDecimal;->compareTo(Ljava/math/BigDecimal;)I

    move-result v1

    if-nez v1, :cond_0

    sget-object v0, Ljava/math/BigDecimal;->ZERO:Ljava/math/BigDecimal;

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {v0}, Ljava/math/BigDecimal;->stripTrailingZeros()Ljava/math/BigDecimal;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public a(Lorg/threeten/bp/format/DateTimeParseContext;Ljava/lang/CharSequence;I)I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 2727
    invoke-virtual {p1}, Lorg/threeten/bp/format/DateTimeParseContext;->f()Z

    move-result v0

    if-eqz v0, :cond_1

    iget v0, p0, Lorg/threeten/bp/format/DateTimeFormatterBuilder$FractionPrinterParser;->b:I

    .line 2728
    :goto_0
    invoke-virtual {p1}, Lorg/threeten/bp/format/DateTimeParseContext;->f()Z

    move-result v2

    if-eqz v2, :cond_2

    iget v2, p0, Lorg/threeten/bp/format/DateTimeFormatterBuilder$FractionPrinterParser;->c:I

    .line 2729
    :goto_1
    invoke-interface {p2}, Ljava/lang/CharSequence;->length()I

    move-result v3

    .line 2730
    if-ne p3, v3, :cond_3

    .line 2732
    if-lez v0, :cond_0

    xor-int/lit8 p3, p3, -0x1

    .line 2762
    :cond_0
    :goto_2
    return p3

    :cond_1
    move v0, v1

    .line 2727
    goto :goto_0

    .line 2728
    :cond_2
    const/16 v2, 0x9

    goto :goto_1

    .line 2734
    :cond_3
    iget-boolean v4, p0, Lorg/threeten/bp/format/DateTimeFormatterBuilder$FractionPrinterParser;->d:Z

    if-eqz v4, :cond_9

    .line 2735
    invoke-interface {p2, p3}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v4

    invoke-virtual {p1}, Lorg/threeten/bp/format/DateTimeParseContext;->c()Lorg/threeten/bp/format/DecimalStyle;

    move-result-object v5

    invoke-virtual {v5}, Lorg/threeten/bp/format/DecimalStyle;->d()C

    move-result v5

    if-eq v4, v5, :cond_4

    .line 2737
    if-lez v0, :cond_0

    xor-int/lit8 p3, p3, -0x1

    goto :goto_2

    .line 2739
    :cond_4
    add-int/lit8 v4, p3, 0x1

    .line 2741
    :goto_3
    add-int v6, v4, v0

    .line 2742
    if-le v6, v3, :cond_5

    .line 2743
    xor-int/lit8 p3, v4, -0x1

    goto :goto_2

    .line 2745
    :cond_5
    add-int v0, v4, v2

    invoke-static {v0, v3}, Ljava/lang/Math;->min(II)I

    move-result v2

    move v5, v4

    .line 2748
    :goto_4
    if-ge v5, v2, :cond_7

    .line 2749
    add-int/lit8 v0, v5, 0x1

    invoke-interface {p2, v5}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v3

    .line 2750
    invoke-virtual {p1}, Lorg/threeten/bp/format/DateTimeParseContext;->c()Lorg/threeten/bp/format/DecimalStyle;

    move-result-object v5

    invoke-virtual {v5, v3}, Lorg/threeten/bp/format/DecimalStyle;->a(C)I

    move-result v3

    .line 2751
    if-gez v3, :cond_8

    .line 2752
    if-ge v0, v6, :cond_6

    .line 2753
    xor-int/lit8 p3, v4, -0x1

    goto :goto_2

    .line 2755
    :cond_6
    add-int/lit8 v5, v0, -0x1

    .line 2760
    :cond_7
    new-instance v0, Ljava/math/BigDecimal;

    invoke-direct {v0, v1}, Ljava/math/BigDecimal;-><init>(I)V

    sub-int v1, v5, v4

    invoke-virtual {v0, v1}, Ljava/math/BigDecimal;->movePointLeft(I)Ljava/math/BigDecimal;

    move-result-object v0

    .line 2761
    invoke-direct {p0, v0}, Lorg/threeten/bp/format/DateTimeFormatterBuilder$FractionPrinterParser;->a(Ljava/math/BigDecimal;)J

    move-result-wide v2

    .line 2762
    iget-object v1, p0, Lorg/threeten/bp/format/DateTimeFormatterBuilder$FractionPrinterParser;->a:Lorg/threeten/bp/temporal/TemporalField;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Lorg/threeten/bp/format/DateTimeParseContext;->a(Lorg/threeten/bp/temporal/TemporalField;JII)I

    move-result p3

    goto :goto_2

    .line 2758
    :cond_8
    mul-int/lit8 v1, v1, 0xa

    add-int/2addr v1, v3

    move v5, v0

    .line 2759
    goto :goto_4

    :cond_9
    move v4, p3

    goto :goto_3
.end method

.method public a(Lorg/threeten/bp/format/DateTimePrintContext;Ljava/lang/StringBuilder;)Z
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 2697
    iget-object v1, p0, Lorg/threeten/bp/format/DateTimeFormatterBuilder$FractionPrinterParser;->a:Lorg/threeten/bp/temporal/TemporalField;

    invoke-virtual {p1, v1}, Lorg/threeten/bp/format/DateTimePrintContext;->a(Lorg/threeten/bp/temporal/TemporalField;)Ljava/lang/Long;

    move-result-object v1

    .line 2698
    if-nez v1, :cond_0

    .line 2722
    :goto_0
    return v0

    .line 2701
    :cond_0
    invoke-virtual {p1}, Lorg/threeten/bp/format/DateTimePrintContext;->c()Lorg/threeten/bp/format/DecimalStyle;

    move-result-object v2

    .line 2702
    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-direct {p0, v4, v5}, Lorg/threeten/bp/format/DateTimeFormatterBuilder$FractionPrinterParser;->a(J)Ljava/math/BigDecimal;

    move-result-object v1

    .line 2703
    invoke-virtual {v1}, Ljava/math/BigDecimal;->scale()I

    move-result v3

    if-nez v3, :cond_2

    .line 2704
    iget v1, p0, Lorg/threeten/bp/format/DateTimeFormatterBuilder$FractionPrinterParser;->b:I

    if-lez v1, :cond_4

    .line 2705
    iget-boolean v1, p0, Lorg/threeten/bp/format/DateTimeFormatterBuilder$FractionPrinterParser;->d:Z

    if-eqz v1, :cond_1

    .line 2706
    invoke-virtual {v2}, Lorg/threeten/bp/format/DecimalStyle;->d()C

    move-result v1

    invoke-virtual {p2, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 2708
    :cond_1
    :goto_1
    iget v1, p0, Lorg/threeten/bp/format/DateTimeFormatterBuilder$FractionPrinterParser;->b:I

    if-ge v0, v1, :cond_4

    .line 2709
    invoke-virtual {v2}, Lorg/threeten/bp/format/DecimalStyle;->a()C

    move-result v1

    invoke-virtual {p2, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 2708
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 2713
    :cond_2
    invoke-virtual {v1}, Ljava/math/BigDecimal;->scale()I

    move-result v0

    iget v3, p0, Lorg/threeten/bp/format/DateTimeFormatterBuilder$FractionPrinterParser;->b:I

    invoke-static {v0, v3}, Ljava/lang/Math;->max(II)I

    move-result v0

    iget v3, p0, Lorg/threeten/bp/format/DateTimeFormatterBuilder$FractionPrinterParser;->c:I

    invoke-static {v0, v3}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 2714
    sget-object v3, Ljava/math/RoundingMode;->FLOOR:Ljava/math/RoundingMode;

    invoke-virtual {v1, v0, v3}, Ljava/math/BigDecimal;->setScale(ILjava/math/RoundingMode;)Ljava/math/BigDecimal;

    move-result-object v0

    .line 2715
    invoke-virtual {v0}, Ljava/math/BigDecimal;->toPlainString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 2716
    invoke-virtual {v2, v0}, Lorg/threeten/bp/format/DecimalStyle;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2717
    iget-boolean v1, p0, Lorg/threeten/bp/format/DateTimeFormatterBuilder$FractionPrinterParser;->d:Z

    if-eqz v1, :cond_3

    .line 2718
    invoke-virtual {v2}, Lorg/threeten/bp/format/DecimalStyle;->d()C

    move-result v1

    invoke-virtual {p2, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 2720
    :cond_3
    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2722
    :cond_4
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 2818
    iget-boolean v0, p0, Lorg/threeten/bp/format/DateTimeFormatterBuilder$FractionPrinterParser;->d:Z

    if-eqz v0, :cond_0

    const-string v0, ",DecimalPoint"

    .line 2819
    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Fraction("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lorg/threeten/bp/format/DateTimeFormatterBuilder$FractionPrinterParser;->a:Lorg/threeten/bp/temporal/TemporalField;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lorg/threeten/bp/format/DateTimeFormatterBuilder$FractionPrinterParser;->b:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lorg/threeten/bp/format/DateTimeFormatterBuilder$FractionPrinterParser;->c:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 2818
    :cond_0
    const-string v0, ""

    goto :goto_0
.end method
