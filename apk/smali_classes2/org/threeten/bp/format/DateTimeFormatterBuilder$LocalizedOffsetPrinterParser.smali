.class final Lorg/threeten/bp/format/DateTimeFormatterBuilder$LocalizedOffsetPrinterParser;
.super Ljava/lang/Object;
.source "DateTimeFormatterBuilder.java"

# interfaces
.implements Lorg/threeten/bp/format/DateTimeFormatterBuilder$DateTimePrinterParser;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/threeten/bp/format/DateTimeFormatterBuilder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "LocalizedOffsetPrinterParser"
.end annotation


# instance fields
.field private final a:Lorg/threeten/bp/format/TextStyle;


# direct methods
.method public constructor <init>(Lorg/threeten/bp/format/TextStyle;)V
    .locals 0

    .prologue
    .line 3212
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 3213
    iput-object p1, p0, Lorg/threeten/bp/format/DateTimeFormatterBuilder$LocalizedOffsetPrinterParser;->a:Lorg/threeten/bp/format/TextStyle;

    .line 3214
    return-void
.end method


# virtual methods
.method public a(Lorg/threeten/bp/format/DateTimeParseContext;Ljava/lang/CharSequence;I)I
    .locals 11

    .prologue
    const/16 v10, 0x3b

    const/16 v9, 0x3a

    const/16 v6, 0x2d

    const/16 v8, 0x39

    const/16 v7, 0x30

    .line 3246
    const-string v3, "GMT"

    const/4 v4, 0x0

    const/4 v5, 0x3

    move-object v0, p1

    move-object v1, p2

    move v2, p3

    invoke-virtual/range {v0 .. v5}, Lorg/threeten/bp/format/DateTimeParseContext;->a(Ljava/lang/CharSequence;ILjava/lang/CharSequence;II)Z

    move-result v0

    if-nez v0, :cond_0

    .line 3247
    xor-int/lit8 v0, p3, -0x1

    .line 3332
    :goto_0
    return v0

    .line 3249
    :cond_0
    add-int/lit8 v4, p3, 0x3

    .line 3250
    iget-object v0, p0, Lorg/threeten/bp/format/DateTimeFormatterBuilder$LocalizedOffsetPrinterParser;->a:Lorg/threeten/bp/format/TextStyle;

    sget-object v1, Lorg/threeten/bp/format/TextStyle;->a:Lorg/threeten/bp/format/TextStyle;

    if-ne v0, v1, :cond_1

    .line 3251
    new-instance v0, Lorg/threeten/bp/format/DateTimeFormatterBuilder$OffsetIdPrinterParser;

    const-string v1, ""

    const-string v2, "+HH:MM:ss"

    invoke-direct {v0, v1, v2}, Lorg/threeten/bp/format/DateTimeFormatterBuilder$OffsetIdPrinterParser;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, p1, p2, v4}, Lorg/threeten/bp/format/DateTimeFormatterBuilder$OffsetIdPrinterParser;->a(Lorg/threeten/bp/format/DateTimeParseContext;Ljava/lang/CharSequence;I)I

    move-result v0

    goto :goto_0

    .line 3253
    :cond_1
    invoke-interface {p2}, Ljava/lang/CharSequence;->length()I

    move-result v2

    .line 3254
    if-ne v4, v2, :cond_2

    .line 3255
    sget-object v1, Lorg/threeten/bp/temporal/ChronoField;->D:Lorg/threeten/bp/temporal/ChronoField;

    const-wide/16 v2, 0x0

    move-object v0, p1

    move v5, v4

    invoke-virtual/range {v0 .. v5}, Lorg/threeten/bp/format/DateTimeParseContext;->a(Lorg/threeten/bp/temporal/TemporalField;JII)I

    move-result v0

    goto :goto_0

    .line 3257
    :cond_2
    invoke-interface {p2, v4}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v0

    .line 3258
    const/16 v1, 0x2b

    if-eq v0, v1, :cond_3

    if-eq v0, v6, :cond_3

    .line 3259
    sget-object v1, Lorg/threeten/bp/temporal/ChronoField;->D:Lorg/threeten/bp/temporal/ChronoField;

    const-wide/16 v2, 0x0

    move-object v0, p1

    move v5, v4

    invoke-virtual/range {v0 .. v5}, Lorg/threeten/bp/format/DateTimeParseContext;->a(Lorg/threeten/bp/temporal/TemporalField;JII)I

    move-result v0

    goto :goto_0

    .line 3261
    :cond_3
    if-ne v0, v6, :cond_4

    const/4 v0, -0x1

    move v1, v0

    .line 3262
    :goto_1
    if-ne v4, v2, :cond_5

    .line 3263
    xor-int/lit8 v0, v4, -0x1

    goto :goto_0

    .line 3261
    :cond_4
    const/4 v0, 0x1

    move v1, v0

    goto :goto_1

    .line 3265
    :cond_5
    add-int/lit8 v0, v4, 0x1

    .line 3267
    invoke-interface {p2, v0}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v3

    .line 3268
    if-lt v3, v7, :cond_6

    if-le v3, v8, :cond_7

    .line 3269
    :cond_6
    xor-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 3271
    :cond_7
    add-int/lit8 v4, v0, 0x1

    .line 3272
    add-int/lit8 v0, v3, -0x30

    .line 3273
    if-eq v4, v2, :cond_9

    .line 3274
    invoke-interface {p2, v4}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v3

    .line 3275
    if-lt v3, v7, :cond_9

    if-gt v3, v8, :cond_9

    .line 3276
    mul-int/lit8 v0, v0, 0xa

    add-int/lit8 v3, v3, -0x30

    add-int/2addr v0, v3

    .line 3277
    const/16 v3, 0x17

    if-le v0, v3, :cond_8

    .line 3278
    xor-int/lit8 v0, v4, -0x1

    goto :goto_0

    .line 3280
    :cond_8
    add-int/lit8 v4, v4, 0x1

    .line 3283
    :cond_9
    if-eq v4, v2, :cond_a

    invoke-interface {p2, v4}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v3

    if-eq v3, v9, :cond_b

    .line 3284
    :cond_a
    mul-int/lit16 v1, v1, 0xe10

    mul-int/2addr v0, v1

    .line 3285
    sget-object v1, Lorg/threeten/bp/temporal/ChronoField;->D:Lorg/threeten/bp/temporal/ChronoField;

    int-to-long v2, v0

    move-object v0, p1

    move v5, v4

    invoke-virtual/range {v0 .. v5}, Lorg/threeten/bp/format/DateTimeParseContext;->a(Lorg/threeten/bp/temporal/TemporalField;JII)I

    move-result v0

    goto/16 :goto_0

    .line 3287
    :cond_b
    add-int/lit8 v3, v4, 0x1

    .line 3289
    add-int/lit8 v4, v2, -0x2

    if-le v3, v4, :cond_c

    .line 3290
    xor-int/lit8 v0, v3, -0x1

    goto/16 :goto_0

    .line 3292
    :cond_c
    invoke-interface {p2, v3}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v4

    .line 3293
    if-lt v4, v7, :cond_d

    if-le v4, v8, :cond_e

    .line 3294
    :cond_d
    xor-int/lit8 v0, v3, -0x1

    goto/16 :goto_0

    .line 3296
    :cond_e
    add-int/lit8 v3, v3, 0x1

    .line 3297
    add-int/lit8 v5, v4, -0x30

    .line 3298
    invoke-interface {p2, v3}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v6

    .line 3299
    if-lt v6, v7, :cond_f

    if-le v6, v8, :cond_10

    .line 3300
    :cond_f
    xor-int/lit8 v0, v3, -0x1

    goto/16 :goto_0

    .line 3302
    :cond_10
    add-int/lit8 v4, v3, 0x1

    .line 3303
    mul-int/lit8 v3, v5, 0xa

    add-int/lit8 v5, v6, -0x30

    add-int/2addr v3, v5

    .line 3304
    if-le v3, v10, :cond_11

    .line 3305
    xor-int/lit8 v0, v4, -0x1

    goto/16 :goto_0

    .line 3307
    :cond_11
    if-eq v4, v2, :cond_12

    invoke-interface {p2, v4}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v5

    if-eq v5, v9, :cond_13

    .line 3308
    :cond_12
    mul-int/lit16 v0, v0, 0xe10

    mul-int/lit8 v2, v3, 0x3c

    add-int/2addr v0, v2

    mul-int/2addr v0, v1

    .line 3309
    sget-object v1, Lorg/threeten/bp/temporal/ChronoField;->D:Lorg/threeten/bp/temporal/ChronoField;

    int-to-long v2, v0

    move-object v0, p1

    move v5, v4

    invoke-virtual/range {v0 .. v5}, Lorg/threeten/bp/format/DateTimeParseContext;->a(Lorg/threeten/bp/temporal/TemporalField;JII)I

    move-result v0

    goto/16 :goto_0

    .line 3311
    :cond_13
    add-int/lit8 v4, v4, 0x1

    .line 3313
    add-int/lit8 v2, v2, -0x2

    if-le v4, v2, :cond_14

    .line 3314
    xor-int/lit8 v0, v4, -0x1

    goto/16 :goto_0

    .line 3316
    :cond_14
    invoke-interface {p2, v4}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v2

    .line 3317
    if-lt v2, v7, :cond_15

    if-le v2, v8, :cond_16

    .line 3318
    :cond_15
    xor-int/lit8 v0, v4, -0x1

    goto/16 :goto_0

    .line 3320
    :cond_16
    add-int/lit8 v4, v4, 0x1

    .line 3321
    add-int/lit8 v2, v2, -0x30

    .line 3322
    invoke-interface {p2, v4}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v5

    .line 3323
    if-lt v5, v7, :cond_17

    if-le v5, v8, :cond_18

    .line 3324
    :cond_17
    xor-int/lit8 v0, v4, -0x1

    goto/16 :goto_0

    .line 3326
    :cond_18
    add-int/lit8 v4, v4, 0x1

    .line 3327
    mul-int/lit8 v2, v2, 0xa

    add-int/lit8 v5, v5, -0x30

    add-int/2addr v2, v5

    .line 3328
    if-le v2, v10, :cond_19

    .line 3329
    xor-int/lit8 v0, v4, -0x1

    goto/16 :goto_0

    .line 3331
    :cond_19
    mul-int/lit16 v0, v0, 0xe10

    mul-int/lit8 v3, v3, 0x3c

    add-int/2addr v0, v3

    add-int/2addr v0, v2

    mul-int/2addr v0, v1

    .line 3332
    sget-object v1, Lorg/threeten/bp/temporal/ChronoField;->D:Lorg/threeten/bp/temporal/ChronoField;

    int-to-long v2, v0

    move-object v0, p1

    move v5, v4

    invoke-virtual/range {v0 .. v5}, Lorg/threeten/bp/format/DateTimeParseContext;->a(Lorg/threeten/bp/temporal/TemporalField;JII)I

    move-result v0

    goto/16 :goto_0
.end method

.method public a(Lorg/threeten/bp/format/DateTimePrintContext;Ljava/lang/StringBuilder;)Z
    .locals 4

    .prologue
    .line 3218
    sget-object v0, Lorg/threeten/bp/temporal/ChronoField;->D:Lorg/threeten/bp/temporal/ChronoField;

    invoke-virtual {p1, v0}, Lorg/threeten/bp/format/DateTimePrintContext;->a(Lorg/threeten/bp/temporal/TemporalField;)Ljava/lang/Long;

    move-result-object v0

    .line 3219
    if-nez v0, :cond_0

    .line 3220
    const/4 v0, 0x0

    .line 3241
    :goto_0
    return v0

    .line 3222
    :cond_0
    const-string v1, "GMT"

    invoke-virtual {p2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 3223
    iget-object v1, p0, Lorg/threeten/bp/format/DateTimeFormatterBuilder$LocalizedOffsetPrinterParser;->a:Lorg/threeten/bp/format/TextStyle;

    sget-object v2, Lorg/threeten/bp/format/TextStyle;->a:Lorg/threeten/bp/format/TextStyle;

    if-ne v1, v2, :cond_1

    .line 3224
    new-instance v0, Lorg/threeten/bp/format/DateTimeFormatterBuilder$OffsetIdPrinterParser;

    const-string v1, ""

    const-string v2, "+HH:MM:ss"

    invoke-direct {v0, v1, v2}, Lorg/threeten/bp/format/DateTimeFormatterBuilder$OffsetIdPrinterParser;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, p1, p2}, Lorg/threeten/bp/format/DateTimeFormatterBuilder$OffsetIdPrinterParser;->a(Lorg/threeten/bp/format/DateTimePrintContext;Ljava/lang/StringBuilder;)Z

    move-result v0

    goto :goto_0

    .line 3226
    :cond_1
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-static {v0, v1}, Lorg/threeten/bp/jdk8/Jdk8Methods;->a(J)I

    move-result v0

    .line 3227
    if-eqz v0, :cond_3

    .line 3228
    div-int/lit16 v1, v0, 0xe10

    rem-int/lit8 v1, v1, 0x64

    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    move-result v1

    .line 3229
    div-int/lit8 v2, v0, 0x3c

    rem-int/lit8 v2, v2, 0x3c

    invoke-static {v2}, Ljava/lang/Math;->abs(I)I

    move-result v2

    .line 3230
    rem-int/lit8 v3, v0, 0x3c

    invoke-static {v3}, Ljava/lang/Math;->abs(I)I

    move-result v3

    .line 3231
    if-gez v0, :cond_4

    const-string v0, "-"

    :goto_1
    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 3232
    if-gtz v2, :cond_2

    if-lez v3, :cond_3

    .line 3233
    :cond_2
    const-string v0, ":"

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    div-int/lit8 v1, v2, 0xa

    add-int/lit8 v1, v1, 0x30

    int-to-char v1, v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    rem-int/lit8 v1, v2, 0xa

    add-int/lit8 v1, v1, 0x30

    int-to-char v1, v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 3235
    if-lez v3, :cond_3

    .line 3236
    const-string v0, ":"

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    div-int/lit8 v1, v3, 0xa

    add-int/lit8 v1, v1, 0x30

    int-to-char v1, v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    rem-int/lit8 v1, v3, 0xa

    add-int/lit8 v1, v1, 0x30

    int-to-char v1, v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 3241
    :cond_3
    const/4 v0, 0x1

    goto :goto_0

    .line 3231
    :cond_4
    const-string v0, "+"

    goto :goto_1
.end method
