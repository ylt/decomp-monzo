.class final Lorg/threeten/bp/format/DateTimeParseContext;
.super Ljava/lang/Object;
.source "DateTimeParseContext.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/threeten/bp/format/DateTimeParseContext$1;,
        Lorg/threeten/bp/format/DateTimeParseContext$Parsed;
    }
.end annotation


# instance fields
.field private a:Ljava/util/Locale;

.field private b:Lorg/threeten/bp/format/DecimalStyle;

.field private c:Lorg/threeten/bp/chrono/Chronology;

.field private d:Lorg/threeten/bp/ZoneId;

.field private e:Z

.field private f:Z

.field private final g:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lorg/threeten/bp/format/DateTimeParseContext$Parsed;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lorg/threeten/bp/format/DateTimeFormatter;)V
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 104
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 88
    iput-boolean v0, p0, Lorg/threeten/bp/format/DateTimeParseContext;->e:Z

    .line 92
    iput-boolean v0, p0, Lorg/threeten/bp/format/DateTimeParseContext;->f:Z

    .line 96
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lorg/threeten/bp/format/DateTimeParseContext;->g:Ljava/util/ArrayList;

    .line 105
    invoke-virtual {p1}, Lorg/threeten/bp/format/DateTimeFormatter;->a()Ljava/util/Locale;

    move-result-object v0

    iput-object v0, p0, Lorg/threeten/bp/format/DateTimeParseContext;->a:Ljava/util/Locale;

    .line 106
    invoke-virtual {p1}, Lorg/threeten/bp/format/DateTimeFormatter;->b()Lorg/threeten/bp/format/DecimalStyle;

    move-result-object v0

    iput-object v0, p0, Lorg/threeten/bp/format/DateTimeParseContext;->b:Lorg/threeten/bp/format/DecimalStyle;

    .line 107
    invoke-virtual {p1}, Lorg/threeten/bp/format/DateTimeFormatter;->c()Lorg/threeten/bp/chrono/Chronology;

    move-result-object v0

    iput-object v0, p0, Lorg/threeten/bp/format/DateTimeParseContext;->c:Lorg/threeten/bp/chrono/Chronology;

    .line 108
    invoke-virtual {p1}, Lorg/threeten/bp/format/DateTimeFormatter;->d()Lorg/threeten/bp/ZoneId;

    move-result-object v0

    iput-object v0, p0, Lorg/threeten/bp/format/DateTimeParseContext;->d:Lorg/threeten/bp/ZoneId;

    .line 109
    iget-object v0, p0, Lorg/threeten/bp/format/DateTimeParseContext;->g:Ljava/util/ArrayList;

    new-instance v1, Lorg/threeten/bp/format/DateTimeParseContext$Parsed;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lorg/threeten/bp/format/DateTimeParseContext$Parsed;-><init>(Lorg/threeten/bp/format/DateTimeParseContext;Lorg/threeten/bp/format/DateTimeParseContext$1;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 110
    return-void
.end method

.method constructor <init>(Lorg/threeten/bp/format/DateTimeParseContext;)V
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 123
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 88
    iput-boolean v0, p0, Lorg/threeten/bp/format/DateTimeParseContext;->e:Z

    .line 92
    iput-boolean v0, p0, Lorg/threeten/bp/format/DateTimeParseContext;->f:Z

    .line 96
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lorg/threeten/bp/format/DateTimeParseContext;->g:Ljava/util/ArrayList;

    .line 124
    iget-object v0, p1, Lorg/threeten/bp/format/DateTimeParseContext;->a:Ljava/util/Locale;

    iput-object v0, p0, Lorg/threeten/bp/format/DateTimeParseContext;->a:Ljava/util/Locale;

    .line 125
    iget-object v0, p1, Lorg/threeten/bp/format/DateTimeParseContext;->b:Lorg/threeten/bp/format/DecimalStyle;

    iput-object v0, p0, Lorg/threeten/bp/format/DateTimeParseContext;->b:Lorg/threeten/bp/format/DecimalStyle;

    .line 126
    iget-object v0, p1, Lorg/threeten/bp/format/DateTimeParseContext;->c:Lorg/threeten/bp/chrono/Chronology;

    iput-object v0, p0, Lorg/threeten/bp/format/DateTimeParseContext;->c:Lorg/threeten/bp/chrono/Chronology;

    .line 127
    iget-object v0, p1, Lorg/threeten/bp/format/DateTimeParseContext;->d:Lorg/threeten/bp/ZoneId;

    iput-object v0, p0, Lorg/threeten/bp/format/DateTimeParseContext;->d:Lorg/threeten/bp/ZoneId;

    .line 128
    iget-boolean v0, p1, Lorg/threeten/bp/format/DateTimeParseContext;->e:Z

    iput-boolean v0, p0, Lorg/threeten/bp/format/DateTimeParseContext;->e:Z

    .line 129
    iget-boolean v0, p1, Lorg/threeten/bp/format/DateTimeParseContext;->f:Z

    iput-boolean v0, p0, Lorg/threeten/bp/format/DateTimeParseContext;->f:Z

    .line 130
    iget-object v0, p0, Lorg/threeten/bp/format/DateTimeParseContext;->g:Ljava/util/ArrayList;

    new-instance v1, Lorg/threeten/bp/format/DateTimeParseContext$Parsed;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lorg/threeten/bp/format/DateTimeParseContext$Parsed;-><init>(Lorg/threeten/bp/format/DateTimeParseContext;Lorg/threeten/bp/format/DateTimeParseContext$1;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 131
    return-void
.end method

.method static synthetic a(Lorg/threeten/bp/format/DateTimeParseContext;)Lorg/threeten/bp/ZoneId;
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Lorg/threeten/bp/format/DateTimeParseContext;->d:Lorg/threeten/bp/ZoneId;

    return-object v0
.end method

.method static b(CC)Z
    .locals 2

    .prologue
    .line 258
    if-eq p0, p1, :cond_0

    invoke-static {p0}, Ljava/lang/Character;->toUpperCase(C)C

    move-result v0

    invoke-static {p1}, Ljava/lang/Character;->toUpperCase(C)C

    move-result v1

    if-eq v0, v1, :cond_0

    invoke-static {p0}, Ljava/lang/Character;->toLowerCase(C)C

    move-result v0

    invoke-static {p1}, Ljava/lang/Character;->toLowerCase(C)C

    move-result v1

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private j()Lorg/threeten/bp/format/DateTimeParseContext$Parsed;
    .locals 2

    .prologue
    .line 312
    iget-object v0, p0, Lorg/threeten/bp/format/DateTimeParseContext;->g:Ljava/util/ArrayList;

    iget-object v1, p0, Lorg/threeten/bp/format/DateTimeParseContext;->g:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/threeten/bp/format/DateTimeParseContext$Parsed;

    return-object v0
.end method


# virtual methods
.method a(Lorg/threeten/bp/temporal/TemporalField;JII)I
    .locals 2

    .prologue
    .line 344
    const-string v0, "field"

    invoke-static {p1, v0}, Lorg/threeten/bp/jdk8/Jdk8Methods;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 345
    invoke-direct {p0}, Lorg/threeten/bp/format/DateTimeParseContext;->j()Lorg/threeten/bp/format/DateTimeParseContext$Parsed;

    move-result-object v0

    iget-object v0, v0, Lorg/threeten/bp/format/DateTimeParseContext$Parsed;->c:Ljava/util/Map;

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    .line 346
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    cmp-long v0, v0, p2

    if-eqz v0, :cond_0

    xor-int/lit8 p5, p4, -0x1

    :cond_0
    return p5
.end method

.method a(Lorg/threeten/bp/temporal/TemporalField;)Ljava/lang/Long;
    .locals 1

    .prologue
    .line 328
    invoke-direct {p0}, Lorg/threeten/bp/format/DateTimeParseContext;->j()Lorg/threeten/bp/format/DateTimeParseContext$Parsed;

    move-result-object v0

    iget-object v0, v0, Lorg/threeten/bp/format/DateTimeParseContext$Parsed;->c:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    return-object v0
.end method

.method a()Lorg/threeten/bp/format/DateTimeParseContext;
    .locals 1

    .prologue
    .line 137
    new-instance v0, Lorg/threeten/bp/format/DateTimeParseContext;

    invoke-direct {v0, p0}, Lorg/threeten/bp/format/DateTimeParseContext;-><init>(Lorg/threeten/bp/format/DateTimeParseContext;)V

    return-object v0
.end method

.method a(Lorg/threeten/bp/ZoneId;)V
    .locals 1

    .prologue
    .line 388
    const-string v0, "zone"

    invoke-static {p1, v0}, Lorg/threeten/bp/jdk8/Jdk8Methods;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 389
    invoke-direct {p0}, Lorg/threeten/bp/format/DateTimeParseContext;->j()Lorg/threeten/bp/format/DateTimeParseContext$Parsed;

    move-result-object v0

    iput-object p1, v0, Lorg/threeten/bp/format/DateTimeParseContext$Parsed;->b:Lorg/threeten/bp/ZoneId;

    .line 390
    return-void
.end method

.method a(Lorg/threeten/bp/format/DateTimeFormatterBuilder$ReducedPrinterParser;JII)V
    .locals 6

    .prologue
    const/4 v4, 0x2

    .line 372
    invoke-direct {p0}, Lorg/threeten/bp/format/DateTimeParseContext;->j()Lorg/threeten/bp/format/DateTimeParseContext$Parsed;

    move-result-object v0

    .line 373
    iget-object v1, v0, Lorg/threeten/bp/format/DateTimeParseContext$Parsed;->f:Ljava/util/List;

    if-nez v1, :cond_0

    .line 374
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1, v4}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v1, v0, Lorg/threeten/bp/format/DateTimeParseContext$Parsed;->f:Ljava/util/List;

    .line 376
    :cond_0
    iget-object v0, v0, Lorg/threeten/bp/format/DateTimeParseContext$Parsed;->f:Ljava/util/List;

    const/4 v1, 0x4

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    const/4 v2, 0x1

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v4

    const/4 v2, 0x3

    invoke-static {p5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 377
    return-void
.end method

.method a(Z)V
    .locals 0

    .prologue
    .line 196
    iput-boolean p1, p0, Lorg/threeten/bp/format/DateTimeParseContext;->e:Z

    .line 197
    return-void
.end method

.method a(CC)Z
    .locals 1

    .prologue
    .line 244
    invoke-virtual {p0}, Lorg/threeten/bp/format/DateTimeParseContext;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 245
    if-ne p1, p2, :cond_0

    const/4 v0, 0x1

    .line 247
    :goto_0
    return v0

    .line 245
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 247
    :cond_1
    invoke-static {p1, p2}, Lorg/threeten/bp/format/DateTimeParseContext;->b(CC)Z

    move-result v0

    goto :goto_0
.end method

.method a(Ljava/lang/CharSequence;ILjava/lang/CharSequence;II)Z
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 211
    add-int v1, p2, p5

    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v2

    if-gt v1, v2, :cond_0

    add-int v1, p4, p5

    invoke-interface {p3}, Ljava/lang/CharSequence;->length()I

    move-result v2

    if-le v1, v2, :cond_1

    .line 232
    :cond_0
    :goto_0
    return v0

    .line 214
    :cond_1
    invoke-virtual {p0}, Lorg/threeten/bp/format/DateTimeParseContext;->e()Z

    move-result v1

    if-eqz v1, :cond_2

    move v1, v0

    .line 215
    :goto_1
    if-ge v1, p5, :cond_4

    .line 216
    add-int v2, p2, v1

    invoke-interface {p1, v2}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v2

    .line 217
    add-int v3, p4, v1

    invoke-interface {p3, v3}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v3

    .line 218
    if-ne v2, v3, :cond_0

    .line 215
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_2
    move v1, v0

    .line 223
    :goto_2
    if-ge v1, p5, :cond_4

    .line 224
    add-int v2, p2, v1

    invoke-interface {p1, v2}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v2

    .line 225
    add-int v3, p4, v1

    invoke-interface {p3, v3}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v3

    .line 226
    if-eq v2, v3, :cond_3

    invoke-static {v2}, Ljava/lang/Character;->toUpperCase(C)C

    move-result v4

    invoke-static {v3}, Ljava/lang/Character;->toUpperCase(C)C

    move-result v5

    if-eq v4, v5, :cond_3

    invoke-static {v2}, Ljava/lang/Character;->toLowerCase(C)C

    move-result v2

    invoke-static {v3}, Ljava/lang/Character;->toLowerCase(C)C

    move-result v3

    if-ne v2, v3, :cond_0

    .line 223
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 232
    :cond_4
    const/4 v0, 0x1

    goto :goto_0
.end method

.method b()Ljava/util/Locale;
    .locals 1

    .prologue
    .line 150
    iget-object v0, p0, Lorg/threeten/bp/format/DateTimeParseContext;->a:Ljava/util/Locale;

    return-object v0
.end method

.method b(Z)V
    .locals 0

    .prologue
    .line 281
    iput-boolean p1, p0, Lorg/threeten/bp/format/DateTimeParseContext;->f:Z

    .line 282
    return-void
.end method

.method c()Lorg/threeten/bp/format/DecimalStyle;
    .locals 1

    .prologue
    .line 161
    iget-object v0, p0, Lorg/threeten/bp/format/DateTimeParseContext;->b:Lorg/threeten/bp/format/DecimalStyle;

    return-object v0
.end method

.method c(Z)V
    .locals 2

    .prologue
    .line 298
    if-eqz p1, :cond_0

    .line 299
    iget-object v0, p0, Lorg/threeten/bp/format/DateTimeParseContext;->g:Ljava/util/ArrayList;

    iget-object v1, p0, Lorg/threeten/bp/format/DateTimeParseContext;->g:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x2

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 303
    :goto_0
    return-void

    .line 301
    :cond_0
    iget-object v0, p0, Lorg/threeten/bp/format/DateTimeParseContext;->g:Ljava/util/ArrayList;

    iget-object v1, p0, Lorg/threeten/bp/format/DateTimeParseContext;->g:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    goto :goto_0
.end method

.method d()Lorg/threeten/bp/chrono/Chronology;
    .locals 1

    .prologue
    .line 170
    invoke-direct {p0}, Lorg/threeten/bp/format/DateTimeParseContext;->j()Lorg/threeten/bp/format/DateTimeParseContext$Parsed;

    move-result-object v0

    iget-object v0, v0, Lorg/threeten/bp/format/DateTimeParseContext$Parsed;->a:Lorg/threeten/bp/chrono/Chronology;

    .line 171
    if-nez v0, :cond_0

    .line 172
    iget-object v0, p0, Lorg/threeten/bp/format/DateTimeParseContext;->c:Lorg/threeten/bp/chrono/Chronology;

    .line 173
    if-nez v0, :cond_0

    .line 174
    sget-object v0, Lorg/threeten/bp/chrono/IsoChronology;->b:Lorg/threeten/bp/chrono/IsoChronology;

    .line 177
    :cond_0
    return-object v0
.end method

.method e()Z
    .locals 1

    .prologue
    .line 187
    iget-boolean v0, p0, Lorg/threeten/bp/format/DateTimeParseContext;->e:Z

    return v0
.end method

.method f()Z
    .locals 1

    .prologue
    .line 272
    iget-boolean v0, p0, Lorg/threeten/bp/format/DateTimeParseContext;->f:Z

    return v0
.end method

.method g()V
    .locals 2

    .prologue
    .line 289
    iget-object v0, p0, Lorg/threeten/bp/format/DateTimeParseContext;->g:Ljava/util/ArrayList;

    invoke-direct {p0}, Lorg/threeten/bp/format/DateTimeParseContext;->j()Lorg/threeten/bp/format/DateTimeParseContext$Parsed;

    move-result-object v1

    invoke-virtual {v1}, Lorg/threeten/bp/format/DateTimeParseContext$Parsed;->a()Lorg/threeten/bp/format/DateTimeParseContext$Parsed;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 290
    return-void
.end method

.method h()V
    .locals 2

    .prologue
    .line 396
    invoke-direct {p0}, Lorg/threeten/bp/format/DateTimeParseContext;->j()Lorg/threeten/bp/format/DateTimeParseContext$Parsed;

    move-result-object v0

    const/4 v1, 0x1

    iput-boolean v1, v0, Lorg/threeten/bp/format/DateTimeParseContext$Parsed;->d:Z

    .line 397
    return-void
.end method

.method i()Lorg/threeten/bp/format/DateTimeParseContext$Parsed;
    .locals 1

    .prologue
    .line 407
    invoke-direct {p0}, Lorg/threeten/bp/format/DateTimeParseContext;->j()Lorg/threeten/bp/format/DateTimeParseContext$Parsed;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 418
    invoke-direct {p0}, Lorg/threeten/bp/format/DateTimeParseContext;->j()Lorg/threeten/bp/format/DateTimeParseContext$Parsed;

    move-result-object v0

    invoke-virtual {v0}, Lorg/threeten/bp/format/DateTimeParseContext$Parsed;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
