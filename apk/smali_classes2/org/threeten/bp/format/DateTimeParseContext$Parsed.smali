.class final Lorg/threeten/bp/format/DateTimeParseContext$Parsed;
.super Lorg/threeten/bp/jdk8/DefaultInterfaceTemporalAccessor;
.source "DateTimeParseContext.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/threeten/bp/format/DateTimeParseContext;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x10
    name = "Parsed"
.end annotation


# instance fields
.field a:Lorg/threeten/bp/chrono/Chronology;

.field b:Lorg/threeten/bp/ZoneId;

.field final c:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lorg/threeten/bp/temporal/TemporalField;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field d:Z

.field e:Lorg/threeten/bp/Period;

.field f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<[",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic g:Lorg/threeten/bp/format/DateTimeParseContext;


# direct methods
.method private constructor <init>(Lorg/threeten/bp/format/DateTimeParseContext;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 433
    iput-object p1, p0, Lorg/threeten/bp/format/DateTimeParseContext$Parsed;->g:Lorg/threeten/bp/format/DateTimeParseContext;

    invoke-direct {p0}, Lorg/threeten/bp/jdk8/DefaultInterfaceTemporalAccessor;-><init>()V

    .line 426
    iput-object v0, p0, Lorg/threeten/bp/format/DateTimeParseContext$Parsed;->a:Lorg/threeten/bp/chrono/Chronology;

    .line 427
    iput-object v0, p0, Lorg/threeten/bp/format/DateTimeParseContext$Parsed;->b:Lorg/threeten/bp/ZoneId;

    .line 428
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lorg/threeten/bp/format/DateTimeParseContext$Parsed;->c:Ljava/util/Map;

    .line 430
    sget-object v0, Lorg/threeten/bp/Period;->a:Lorg/threeten/bp/Period;

    iput-object v0, p0, Lorg/threeten/bp/format/DateTimeParseContext$Parsed;->e:Lorg/threeten/bp/Period;

    .line 434
    return-void
.end method

.method synthetic constructor <init>(Lorg/threeten/bp/format/DateTimeParseContext;Lorg/threeten/bp/format/DateTimeParseContext$1;)V
    .locals 0

    .prologue
    .line 425
    invoke-direct {p0, p1}, Lorg/threeten/bp/format/DateTimeParseContext$Parsed;-><init>(Lorg/threeten/bp/format/DateTimeParseContext;)V

    return-void
.end method


# virtual methods
.method public a(Lorg/threeten/bp/temporal/TemporalQuery;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<R:",
            "Ljava/lang/Object;",
            ">(",
            "Lorg/threeten/bp/temporal/TemporalQuery",
            "<TR;>;)TR;"
        }
    .end annotation

    .prologue
    .line 469
    invoke-static {}, Lorg/threeten/bp/temporal/TemporalQueries;->b()Lorg/threeten/bp/temporal/TemporalQuery;

    move-result-object v0

    if-ne p1, v0, :cond_0

    .line 470
    iget-object v0, p0, Lorg/threeten/bp/format/DateTimeParseContext$Parsed;->a:Lorg/threeten/bp/chrono/Chronology;

    .line 475
    :goto_0
    return-object v0

    .line 472
    :cond_0
    invoke-static {}, Lorg/threeten/bp/temporal/TemporalQueries;->a()Lorg/threeten/bp/temporal/TemporalQuery;

    move-result-object v0

    if-eq p1, v0, :cond_1

    invoke-static {}, Lorg/threeten/bp/temporal/TemporalQueries;->d()Lorg/threeten/bp/temporal/TemporalQuery;

    move-result-object v0

    if-ne p1, v0, :cond_2

    .line 473
    :cond_1
    iget-object v0, p0, Lorg/threeten/bp/format/DateTimeParseContext$Parsed;->b:Lorg/threeten/bp/ZoneId;

    goto :goto_0

    .line 475
    :cond_2
    invoke-super {p0, p1}, Lorg/threeten/bp/jdk8/DefaultInterfaceTemporalAccessor;->a(Lorg/threeten/bp/temporal/TemporalQuery;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method protected a()Lorg/threeten/bp/format/DateTimeParseContext$Parsed;
    .locals 3

    .prologue
    .line 436
    new-instance v0, Lorg/threeten/bp/format/DateTimeParseContext$Parsed;

    iget-object v1, p0, Lorg/threeten/bp/format/DateTimeParseContext$Parsed;->g:Lorg/threeten/bp/format/DateTimeParseContext;

    invoke-direct {v0, v1}, Lorg/threeten/bp/format/DateTimeParseContext$Parsed;-><init>(Lorg/threeten/bp/format/DateTimeParseContext;)V

    .line 437
    iget-object v1, p0, Lorg/threeten/bp/format/DateTimeParseContext$Parsed;->a:Lorg/threeten/bp/chrono/Chronology;

    iput-object v1, v0, Lorg/threeten/bp/format/DateTimeParseContext$Parsed;->a:Lorg/threeten/bp/chrono/Chronology;

    .line 438
    iget-object v1, p0, Lorg/threeten/bp/format/DateTimeParseContext$Parsed;->b:Lorg/threeten/bp/ZoneId;

    iput-object v1, v0, Lorg/threeten/bp/format/DateTimeParseContext$Parsed;->b:Lorg/threeten/bp/ZoneId;

    .line 439
    iget-object v1, v0, Lorg/threeten/bp/format/DateTimeParseContext$Parsed;->c:Ljava/util/Map;

    iget-object v2, p0, Lorg/threeten/bp/format/DateTimeParseContext$Parsed;->c:Ljava/util/Map;

    invoke-interface {v1, v2}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    .line 440
    iget-boolean v1, p0, Lorg/threeten/bp/format/DateTimeParseContext$Parsed;->d:Z

    iput-boolean v1, v0, Lorg/threeten/bp/format/DateTimeParseContext$Parsed;->d:Z

    .line 441
    return-object v0
.end method

.method public a(Lorg/threeten/bp/temporal/TemporalField;)Z
    .locals 1

    .prologue
    .line 449
    iget-object v0, p0, Lorg/threeten/bp/format/DateTimeParseContext$Parsed;->c:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method b()Lorg/threeten/bp/format/DateTimeBuilder;
    .locals 3

    .prologue
    .line 491
    new-instance v0, Lorg/threeten/bp/format/DateTimeBuilder;

    invoke-direct {v0}, Lorg/threeten/bp/format/DateTimeBuilder;-><init>()V

    .line 492
    iget-object v1, v0, Lorg/threeten/bp/format/DateTimeBuilder;->a:Ljava/util/Map;

    iget-object v2, p0, Lorg/threeten/bp/format/DateTimeParseContext$Parsed;->c:Ljava/util/Map;

    invoke-interface {v1, v2}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    .line 493
    iget-object v1, p0, Lorg/threeten/bp/format/DateTimeParseContext$Parsed;->g:Lorg/threeten/bp/format/DateTimeParseContext;

    invoke-virtual {v1}, Lorg/threeten/bp/format/DateTimeParseContext;->d()Lorg/threeten/bp/chrono/Chronology;

    move-result-object v1

    iput-object v1, v0, Lorg/threeten/bp/format/DateTimeBuilder;->b:Lorg/threeten/bp/chrono/Chronology;

    .line 494
    iget-object v1, p0, Lorg/threeten/bp/format/DateTimeParseContext$Parsed;->b:Lorg/threeten/bp/ZoneId;

    if-eqz v1, :cond_0

    .line 495
    iget-object v1, p0, Lorg/threeten/bp/format/DateTimeParseContext$Parsed;->b:Lorg/threeten/bp/ZoneId;

    iput-object v1, v0, Lorg/threeten/bp/format/DateTimeBuilder;->c:Lorg/threeten/bp/ZoneId;

    .line 499
    :goto_0
    iget-boolean v1, p0, Lorg/threeten/bp/format/DateTimeParseContext$Parsed;->d:Z

    iput-boolean v1, v0, Lorg/threeten/bp/format/DateTimeBuilder;->f:Z

    .line 500
    iget-object v1, p0, Lorg/threeten/bp/format/DateTimeParseContext$Parsed;->e:Lorg/threeten/bp/Period;

    iput-object v1, v0, Lorg/threeten/bp/format/DateTimeBuilder;->g:Lorg/threeten/bp/Period;

    .line 501
    return-object v0

    .line 497
    :cond_0
    iget-object v1, p0, Lorg/threeten/bp/format/DateTimeParseContext$Parsed;->g:Lorg/threeten/bp/format/DateTimeParseContext;

    invoke-static {v1}, Lorg/threeten/bp/format/DateTimeParseContext;->a(Lorg/threeten/bp/format/DateTimeParseContext;)Lorg/threeten/bp/ZoneId;

    move-result-object v1

    iput-object v1, v0, Lorg/threeten/bp/format/DateTimeBuilder;->c:Lorg/threeten/bp/ZoneId;

    goto :goto_0
.end method

.method public c(Lorg/threeten/bp/temporal/TemporalField;)I
    .locals 3

    .prologue
    .line 453
    iget-object v0, p0, Lorg/threeten/bp/format/DateTimeParseContext$Parsed;->c:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 454
    new-instance v0, Lorg/threeten/bp/temporal/UnsupportedTemporalTypeException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unsupported field: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/threeten/bp/temporal/UnsupportedTemporalTypeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 456
    :cond_0
    iget-object v0, p0, Lorg/threeten/bp/format/DateTimeParseContext$Parsed;->c:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    .line 457
    invoke-static {v0, v1}, Lorg/threeten/bp/jdk8/Jdk8Methods;->a(J)I

    move-result v0

    return v0
.end method

.method public d(Lorg/threeten/bp/temporal/TemporalField;)J
    .locals 3

    .prologue
    .line 461
    iget-object v0, p0, Lorg/threeten/bp/format/DateTimeParseContext$Parsed;->c:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 462
    new-instance v0, Lorg/threeten/bp/temporal/UnsupportedTemporalTypeException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unsupported field: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/threeten/bp/temporal/UnsupportedTemporalTypeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 464
    :cond_0
    iget-object v0, p0, Lorg/threeten/bp/format/DateTimeParseContext$Parsed;->c:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    return-wide v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 445
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lorg/threeten/bp/format/DateTimeParseContext$Parsed;->c:Ljava/util/Map;

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lorg/threeten/bp/format/DateTimeParseContext$Parsed;->a:Lorg/threeten/bp/chrono/Chronology;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lorg/threeten/bp/format/DateTimeParseContext$Parsed;->b:Lorg/threeten/bp/ZoneId;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
