.class Lorg/threeten/bp/temporal/WeekFields$ComputedDayOfField;
.super Ljava/lang/Object;
.source "WeekFields.java"

# interfaces
.implements Lorg/threeten/bp/temporal/TemporalField;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/threeten/bp/temporal/WeekFields;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "ComputedDayOfField"
.end annotation


# static fields
.field private static final f:Lorg/threeten/bp/temporal/ValueRange;

.field private static final g:Lorg/threeten/bp/temporal/ValueRange;

.field private static final h:Lorg/threeten/bp/temporal/ValueRange;

.field private static final i:Lorg/threeten/bp/temporal/ValueRange;

.field private static final j:Lorg/threeten/bp/temporal/ValueRange;


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Lorg/threeten/bp/temporal/WeekFields;

.field private final c:Lorg/threeten/bp/temporal/TemporalUnit;

.field private final d:Lorg/threeten/bp/temporal/TemporalUnit;

.field private final e:Lorg/threeten/bp/temporal/ValueRange;


# direct methods
.method static constructor <clinit>()V
    .locals 10

    .prologue
    const-wide/16 v8, 0x34

    const-wide/16 v0, 0x0

    const-wide/16 v2, 0x1

    .line 625
    const-wide/16 v4, 0x7

    invoke-static {v2, v3, v4, v5}, Lorg/threeten/bp/temporal/ValueRange;->a(JJ)Lorg/threeten/bp/temporal/ValueRange;

    move-result-object v4

    sput-object v4, Lorg/threeten/bp/temporal/WeekFields$ComputedDayOfField;->f:Lorg/threeten/bp/temporal/ValueRange;

    .line 626
    const-wide/16 v4, 0x4

    const-wide/16 v6, 0x6

    invoke-static/range {v0 .. v7}, Lorg/threeten/bp/temporal/ValueRange;->a(JJJJ)Lorg/threeten/bp/temporal/ValueRange;

    move-result-object v4

    sput-object v4, Lorg/threeten/bp/temporal/WeekFields$ComputedDayOfField;->g:Lorg/threeten/bp/temporal/ValueRange;

    .line 627
    const-wide/16 v6, 0x36

    move-wide v4, v8

    invoke-static/range {v0 .. v7}, Lorg/threeten/bp/temporal/ValueRange;->a(JJJJ)Lorg/threeten/bp/temporal/ValueRange;

    move-result-object v0

    sput-object v0, Lorg/threeten/bp/temporal/WeekFields$ComputedDayOfField;->h:Lorg/threeten/bp/temporal/ValueRange;

    .line 628
    const-wide/16 v6, 0x35

    move-wide v4, v8

    invoke-static/range {v2 .. v7}, Lorg/threeten/bp/temporal/ValueRange;->a(JJJ)Lorg/threeten/bp/temporal/ValueRange;

    move-result-object v0

    sput-object v0, Lorg/threeten/bp/temporal/WeekFields$ComputedDayOfField;->i:Lorg/threeten/bp/temporal/ValueRange;

    .line 629
    sget-object v0, Lorg/threeten/bp/temporal/ChronoField;->A:Lorg/threeten/bp/temporal/ChronoField;

    invoke-virtual {v0}, Lorg/threeten/bp/temporal/ChronoField;->a()Lorg/threeten/bp/temporal/ValueRange;

    move-result-object v0

    sput-object v0, Lorg/threeten/bp/temporal/WeekFields$ComputedDayOfField;->j:Lorg/threeten/bp/temporal/ValueRange;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;Lorg/threeten/bp/temporal/WeekFields;Lorg/threeten/bp/temporal/TemporalUnit;Lorg/threeten/bp/temporal/TemporalUnit;Lorg/threeten/bp/temporal/ValueRange;)V
    .locals 0

    .prologue
    .line 617
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 618
    iput-object p1, p0, Lorg/threeten/bp/temporal/WeekFields$ComputedDayOfField;->a:Ljava/lang/String;

    .line 619
    iput-object p2, p0, Lorg/threeten/bp/temporal/WeekFields$ComputedDayOfField;->b:Lorg/threeten/bp/temporal/WeekFields;

    .line 620
    iput-object p3, p0, Lorg/threeten/bp/temporal/WeekFields$ComputedDayOfField;->c:Lorg/threeten/bp/temporal/TemporalUnit;

    .line 621
    iput-object p4, p0, Lorg/threeten/bp/temporal/WeekFields$ComputedDayOfField;->d:Lorg/threeten/bp/temporal/TemporalUnit;

    .line 622
    iput-object p5, p0, Lorg/threeten/bp/temporal/WeekFields$ComputedDayOfField;->e:Lorg/threeten/bp/temporal/ValueRange;

    .line 623
    return-void
.end method

.method private a(II)I
    .locals 4

    .prologue
    .line 723
    sub-int v0, p1, p2

    const/4 v1, 0x7

    invoke-static {v0, v1}, Lorg/threeten/bp/jdk8/Jdk8Methods;->d(II)I

    move-result v1

    .line 724
    neg-int v0, v1

    .line 725
    add-int/lit8 v2, v1, 0x1

    iget-object v3, p0, Lorg/threeten/bp/temporal/WeekFields$ComputedDayOfField;->b:Lorg/threeten/bp/temporal/WeekFields;

    invoke-virtual {v3}, Lorg/threeten/bp/temporal/WeekFields;->b()I

    move-result v3

    if-le v2, v3, :cond_0

    .line 727
    rsub-int/lit8 v0, v1, 0x7

    .line 729
    :cond_0
    return v0
.end method

.method private a(Lorg/threeten/bp/temporal/TemporalAccessor;I)I
    .locals 2

    .prologue
    .line 658
    sget-object v0, Lorg/threeten/bp/temporal/ChronoField;->p:Lorg/threeten/bp/temporal/ChronoField;

    invoke-interface {p1, v0}, Lorg/threeten/bp/temporal/TemporalAccessor;->c(Lorg/threeten/bp/temporal/TemporalField;)I

    move-result v0

    .line 659
    sub-int/2addr v0, p2

    const/4 v1, 0x7

    invoke-static {v0, v1}, Lorg/threeten/bp/jdk8/Jdk8Methods;->d(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    return v0
.end method

.method static a(Lorg/threeten/bp/temporal/WeekFields;)Lorg/threeten/bp/temporal/WeekFields$ComputedDayOfField;
    .locals 6

    .prologue
    .line 567
    new-instance v0, Lorg/threeten/bp/temporal/WeekFields$ComputedDayOfField;

    const-string v1, "DayOfWeek"

    sget-object v3, Lorg/threeten/bp/temporal/ChronoUnit;->h:Lorg/threeten/bp/temporal/ChronoUnit;

    sget-object v4, Lorg/threeten/bp/temporal/ChronoUnit;->i:Lorg/threeten/bp/temporal/ChronoUnit;

    sget-object v5, Lorg/threeten/bp/temporal/WeekFields$ComputedDayOfField;->f:Lorg/threeten/bp/temporal/ValueRange;

    move-object v2, p0

    invoke-direct/range {v0 .. v5}, Lorg/threeten/bp/temporal/WeekFields$ComputedDayOfField;-><init>(Ljava/lang/String;Lorg/threeten/bp/temporal/WeekFields;Lorg/threeten/bp/temporal/TemporalUnit;Lorg/threeten/bp/temporal/TemporalUnit;Lorg/threeten/bp/temporal/ValueRange;)V

    return-object v0
.end method

.method private b(II)I
    .locals 2

    .prologue
    .line 741
    add-int/lit8 v0, p1, 0x7

    add-int/lit8 v1, p2, -0x1

    add-int/2addr v0, v1

    div-int/lit8 v0, v0, 0x7

    return v0
.end method

.method private b(Lorg/threeten/bp/temporal/TemporalAccessor;I)J
    .locals 2

    .prologue
    .line 663
    sget-object v0, Lorg/threeten/bp/temporal/ChronoField;->s:Lorg/threeten/bp/temporal/ChronoField;

    invoke-interface {p1, v0}, Lorg/threeten/bp/temporal/TemporalAccessor;->c(Lorg/threeten/bp/temporal/TemporalField;)I

    move-result v0

    .line 664
    invoke-direct {p0, v0, p2}, Lorg/threeten/bp/temporal/WeekFields$ComputedDayOfField;->a(II)I

    move-result v1

    .line 665
    invoke-direct {p0, v1, v0}, Lorg/threeten/bp/temporal/WeekFields$ComputedDayOfField;->b(II)I

    move-result v0

    int-to-long v0, v0

    return-wide v0
.end method

.method static b(Lorg/threeten/bp/temporal/WeekFields;)Lorg/threeten/bp/temporal/WeekFields$ComputedDayOfField;
    .locals 6

    .prologue
    .line 577
    new-instance v0, Lorg/threeten/bp/temporal/WeekFields$ComputedDayOfField;

    const-string v1, "WeekOfMonth"

    sget-object v3, Lorg/threeten/bp/temporal/ChronoUnit;->i:Lorg/threeten/bp/temporal/ChronoUnit;

    sget-object v4, Lorg/threeten/bp/temporal/ChronoUnit;->j:Lorg/threeten/bp/temporal/ChronoUnit;

    sget-object v5, Lorg/threeten/bp/temporal/WeekFields$ComputedDayOfField;->g:Lorg/threeten/bp/temporal/ValueRange;

    move-object v2, p0

    invoke-direct/range {v0 .. v5}, Lorg/threeten/bp/temporal/WeekFields$ComputedDayOfField;-><init>(Ljava/lang/String;Lorg/threeten/bp/temporal/WeekFields;Lorg/threeten/bp/temporal/TemporalUnit;Lorg/threeten/bp/temporal/TemporalUnit;Lorg/threeten/bp/temporal/ValueRange;)V

    return-object v0
.end method

.method private c(Lorg/threeten/bp/temporal/TemporalAccessor;I)J
    .locals 2

    .prologue
    .line 669
    sget-object v0, Lorg/threeten/bp/temporal/ChronoField;->t:Lorg/threeten/bp/temporal/ChronoField;

    invoke-interface {p1, v0}, Lorg/threeten/bp/temporal/TemporalAccessor;->c(Lorg/threeten/bp/temporal/TemporalField;)I

    move-result v0

    .line 670
    invoke-direct {p0, v0, p2}, Lorg/threeten/bp/temporal/WeekFields$ComputedDayOfField;->a(II)I

    move-result v1

    .line 671
    invoke-direct {p0, v1, v0}, Lorg/threeten/bp/temporal/WeekFields$ComputedDayOfField;->b(II)I

    move-result v0

    int-to-long v0, v0

    return-wide v0
.end method

.method static c(Lorg/threeten/bp/temporal/WeekFields;)Lorg/threeten/bp/temporal/WeekFields$ComputedDayOfField;
    .locals 6

    .prologue
    .line 587
    new-instance v0, Lorg/threeten/bp/temporal/WeekFields$ComputedDayOfField;

    const-string v1, "WeekOfYear"

    sget-object v3, Lorg/threeten/bp/temporal/ChronoUnit;->i:Lorg/threeten/bp/temporal/ChronoUnit;

    sget-object v4, Lorg/threeten/bp/temporal/ChronoUnit;->k:Lorg/threeten/bp/temporal/ChronoUnit;

    sget-object v5, Lorg/threeten/bp/temporal/WeekFields$ComputedDayOfField;->h:Lorg/threeten/bp/temporal/ValueRange;

    move-object v2, p0

    invoke-direct/range {v0 .. v5}, Lorg/threeten/bp/temporal/WeekFields$ComputedDayOfField;-><init>(Ljava/lang/String;Lorg/threeten/bp/temporal/WeekFields;Lorg/threeten/bp/temporal/TemporalUnit;Lorg/threeten/bp/temporal/TemporalUnit;Lorg/threeten/bp/temporal/ValueRange;)V

    return-object v0
.end method

.method private d(Lorg/threeten/bp/temporal/TemporalAccessor;)I
    .locals 6

    .prologue
    .line 675
    iget-object v0, p0, Lorg/threeten/bp/temporal/WeekFields$ComputedDayOfField;->b:Lorg/threeten/bp/temporal/WeekFields;

    invoke-virtual {v0}, Lorg/threeten/bp/temporal/WeekFields;->a()Lorg/threeten/bp/DayOfWeek;

    move-result-object v0

    invoke-virtual {v0}, Lorg/threeten/bp/DayOfWeek;->a()I

    move-result v0

    .line 676
    sget-object v1, Lorg/threeten/bp/temporal/ChronoField;->p:Lorg/threeten/bp/temporal/ChronoField;

    invoke-interface {p1, v1}, Lorg/threeten/bp/temporal/TemporalAccessor;->c(Lorg/threeten/bp/temporal/TemporalField;)I

    move-result v1

    .line 677
    sub-int v0, v1, v0

    const/4 v1, 0x7

    invoke-static {v0, v1}, Lorg/threeten/bp/jdk8/Jdk8Methods;->d(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    .line 678
    invoke-direct {p0, p1, v0}, Lorg/threeten/bp/temporal/WeekFields$ComputedDayOfField;->c(Lorg/threeten/bp/temporal/TemporalAccessor;I)J

    move-result-wide v2

    .line 679
    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-nez v1, :cond_0

    .line 680
    invoke-static {p1}, Lorg/threeten/bp/chrono/Chronology;->a(Lorg/threeten/bp/temporal/TemporalAccessor;)Lorg/threeten/bp/chrono/Chronology;

    move-result-object v1

    invoke-virtual {v1, p1}, Lorg/threeten/bp/chrono/Chronology;->b(Lorg/threeten/bp/temporal/TemporalAccessor;)Lorg/threeten/bp/chrono/ChronoLocalDate;

    move-result-object v1

    const-wide/16 v2, 0x1

    sget-object v4, Lorg/threeten/bp/temporal/ChronoUnit;->i:Lorg/threeten/bp/temporal/ChronoUnit;

    invoke-virtual {v1, v2, v3, v4}, Lorg/threeten/bp/chrono/ChronoLocalDate;->e(JLorg/threeten/bp/temporal/TemporalUnit;)Lorg/threeten/bp/chrono/ChronoLocalDate;

    move-result-object v1

    .line 681
    invoke-direct {p0, v1, v0}, Lorg/threeten/bp/temporal/WeekFields$ComputedDayOfField;->c(Lorg/threeten/bp/temporal/TemporalAccessor;I)J

    move-result-wide v0

    long-to-int v0, v0

    add-int/lit8 v0, v0, 0x1

    .line 691
    :goto_0
    return v0

    .line 682
    :cond_0
    const-wide/16 v4, 0x35

    cmp-long v1, v2, v4

    if-ltz v1, :cond_2

    .line 683
    sget-object v1, Lorg/threeten/bp/temporal/ChronoField;->t:Lorg/threeten/bp/temporal/ChronoField;

    invoke-interface {p1, v1}, Lorg/threeten/bp/temporal/TemporalAccessor;->c(Lorg/threeten/bp/temporal/TemporalField;)I

    move-result v1

    invoke-direct {p0, v1, v0}, Lorg/threeten/bp/temporal/WeekFields$ComputedDayOfField;->a(II)I

    move-result v1

    .line 684
    sget-object v0, Lorg/threeten/bp/temporal/ChronoField;->A:Lorg/threeten/bp/temporal/ChronoField;

    invoke-interface {p1, v0}, Lorg/threeten/bp/temporal/TemporalAccessor;->c(Lorg/threeten/bp/temporal/TemporalField;)I

    move-result v0

    .line 685
    int-to-long v4, v0

    invoke-static {v4, v5}, Lorg/threeten/bp/Year;->a(J)Z

    move-result v0

    if-eqz v0, :cond_1

    const/16 v0, 0x16e

    .line 686
    :goto_1
    iget-object v4, p0, Lorg/threeten/bp/temporal/WeekFields$ComputedDayOfField;->b:Lorg/threeten/bp/temporal/WeekFields;

    invoke-virtual {v4}, Lorg/threeten/bp/temporal/WeekFields;->b()I

    move-result v4

    add-int/2addr v0, v4

    invoke-direct {p0, v1, v0}, Lorg/threeten/bp/temporal/WeekFields$ComputedDayOfField;->b(II)I

    move-result v0

    .line 687
    int-to-long v4, v0

    cmp-long v1, v2, v4

    if-ltz v1, :cond_2

    .line 688
    add-int/lit8 v0, v0, -0x1

    int-to-long v0, v0

    sub-long v0, v2, v0

    long-to-int v0, v0

    goto :goto_0

    .line 685
    :cond_1
    const/16 v0, 0x16d

    goto :goto_1

    .line 691
    :cond_2
    long-to-int v0, v2

    goto :goto_0
.end method

.method static d(Lorg/threeten/bp/temporal/WeekFields;)Lorg/threeten/bp/temporal/WeekFields$ComputedDayOfField;
    .locals 6

    .prologue
    .line 597
    new-instance v0, Lorg/threeten/bp/temporal/WeekFields$ComputedDayOfField;

    const-string v1, "WeekOfWeekBasedYear"

    sget-object v3, Lorg/threeten/bp/temporal/ChronoUnit;->i:Lorg/threeten/bp/temporal/ChronoUnit;

    sget-object v4, Lorg/threeten/bp/temporal/IsoFields;->e:Lorg/threeten/bp/temporal/TemporalUnit;

    sget-object v5, Lorg/threeten/bp/temporal/WeekFields$ComputedDayOfField;->i:Lorg/threeten/bp/temporal/ValueRange;

    move-object v2, p0

    invoke-direct/range {v0 .. v5}, Lorg/threeten/bp/temporal/WeekFields$ComputedDayOfField;-><init>(Ljava/lang/String;Lorg/threeten/bp/temporal/WeekFields;Lorg/threeten/bp/temporal/TemporalUnit;Lorg/threeten/bp/temporal/TemporalUnit;Lorg/threeten/bp/temporal/ValueRange;)V

    return-object v0
.end method

.method private e(Lorg/threeten/bp/temporal/TemporalAccessor;)I
    .locals 8

    .prologue
    .line 695
    iget-object v0, p0, Lorg/threeten/bp/temporal/WeekFields$ComputedDayOfField;->b:Lorg/threeten/bp/temporal/WeekFields;

    invoke-virtual {v0}, Lorg/threeten/bp/temporal/WeekFields;->a()Lorg/threeten/bp/DayOfWeek;

    move-result-object v0

    invoke-virtual {v0}, Lorg/threeten/bp/DayOfWeek;->a()I

    move-result v0

    .line 696
    sget-object v1, Lorg/threeten/bp/temporal/ChronoField;->p:Lorg/threeten/bp/temporal/ChronoField;

    invoke-interface {p1, v1}, Lorg/threeten/bp/temporal/TemporalAccessor;->c(Lorg/threeten/bp/temporal/TemporalField;)I

    move-result v1

    .line 697
    sub-int v0, v1, v0

    const/4 v1, 0x7

    invoke-static {v0, v1}, Lorg/threeten/bp/jdk8/Jdk8Methods;->d(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    .line 698
    sget-object v1, Lorg/threeten/bp/temporal/ChronoField;->A:Lorg/threeten/bp/temporal/ChronoField;

    invoke-interface {p1, v1}, Lorg/threeten/bp/temporal/TemporalAccessor;->c(Lorg/threeten/bp/temporal/TemporalField;)I

    move-result v1

    .line 699
    invoke-direct {p0, p1, v0}, Lorg/threeten/bp/temporal/WeekFields$ComputedDayOfField;->c(Lorg/threeten/bp/temporal/TemporalAccessor;I)J

    move-result-wide v2

    .line 700
    const-wide/16 v4, 0x0

    cmp-long v4, v2, v4

    if-nez v4, :cond_0

    .line 701
    add-int/lit8 v0, v1, -0x1

    .line 711
    :goto_0
    return v0

    .line 702
    :cond_0
    const-wide/16 v4, 0x35

    cmp-long v4, v2, v4

    if-gez v4, :cond_1

    move v0, v1

    .line 703
    goto :goto_0

    .line 705
    :cond_1
    sget-object v4, Lorg/threeten/bp/temporal/ChronoField;->t:Lorg/threeten/bp/temporal/ChronoField;

    invoke-interface {p1, v4}, Lorg/threeten/bp/temporal/TemporalAccessor;->c(Lorg/threeten/bp/temporal/TemporalField;)I

    move-result v4

    invoke-direct {p0, v4, v0}, Lorg/threeten/bp/temporal/WeekFields$ComputedDayOfField;->a(II)I

    move-result v4

    .line 706
    int-to-long v6, v1

    invoke-static {v6, v7}, Lorg/threeten/bp/Year;->a(J)Z

    move-result v0

    if-eqz v0, :cond_2

    const/16 v0, 0x16e

    .line 707
    :goto_1
    iget-object v5, p0, Lorg/threeten/bp/temporal/WeekFields$ComputedDayOfField;->b:Lorg/threeten/bp/temporal/WeekFields;

    invoke-virtual {v5}, Lorg/threeten/bp/temporal/WeekFields;->b()I

    move-result v5

    add-int/2addr v0, v5

    invoke-direct {p0, v4, v0}, Lorg/threeten/bp/temporal/WeekFields$ComputedDayOfField;->b(II)I

    move-result v0

    .line 708
    int-to-long v4, v0

    cmp-long v0, v2, v4

    if-ltz v0, :cond_3

    .line 709
    add-int/lit8 v0, v1, 0x1

    goto :goto_0

    .line 706
    :cond_2
    const/16 v0, 0x16d

    goto :goto_1

    :cond_3
    move v0, v1

    .line 711
    goto :goto_0
.end method

.method static e(Lorg/threeten/bp/temporal/WeekFields;)Lorg/threeten/bp/temporal/WeekFields$ComputedDayOfField;
    .locals 6

    .prologue
    .line 607
    new-instance v0, Lorg/threeten/bp/temporal/WeekFields$ComputedDayOfField;

    const-string v1, "WeekBasedYear"

    sget-object v3, Lorg/threeten/bp/temporal/IsoFields;->e:Lorg/threeten/bp/temporal/TemporalUnit;

    sget-object v4, Lorg/threeten/bp/temporal/ChronoUnit;->p:Lorg/threeten/bp/temporal/ChronoUnit;

    sget-object v5, Lorg/threeten/bp/temporal/WeekFields$ComputedDayOfField;->j:Lorg/threeten/bp/temporal/ValueRange;

    move-object v2, p0

    invoke-direct/range {v0 .. v5}, Lorg/threeten/bp/temporal/WeekFields$ComputedDayOfField;-><init>(Ljava/lang/String;Lorg/threeten/bp/temporal/WeekFields;Lorg/threeten/bp/temporal/TemporalUnit;Lorg/threeten/bp/temporal/TemporalUnit;Lorg/threeten/bp/temporal/ValueRange;)V

    return-object v0
.end method

.method private f(Lorg/threeten/bp/temporal/TemporalAccessor;)Lorg/threeten/bp/temporal/ValueRange;
    .locals 8

    .prologue
    const-wide/16 v6, 0x2

    .line 979
    iget-object v0, p0, Lorg/threeten/bp/temporal/WeekFields$ComputedDayOfField;->b:Lorg/threeten/bp/temporal/WeekFields;

    invoke-virtual {v0}, Lorg/threeten/bp/temporal/WeekFields;->a()Lorg/threeten/bp/DayOfWeek;

    move-result-object v0

    invoke-virtual {v0}, Lorg/threeten/bp/DayOfWeek;->a()I

    move-result v0

    .line 980
    sget-object v1, Lorg/threeten/bp/temporal/ChronoField;->p:Lorg/threeten/bp/temporal/ChronoField;

    invoke-interface {p1, v1}, Lorg/threeten/bp/temporal/TemporalAccessor;->c(Lorg/threeten/bp/temporal/TemporalField;)I

    move-result v1

    .line 981
    sub-int v0, v1, v0

    const/4 v1, 0x7

    invoke-static {v0, v1}, Lorg/threeten/bp/jdk8/Jdk8Methods;->d(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    .line 982
    invoke-direct {p0, p1, v0}, Lorg/threeten/bp/temporal/WeekFields$ComputedDayOfField;->c(Lorg/threeten/bp/temporal/TemporalAccessor;I)J

    move-result-wide v2

    .line 983
    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-nez v1, :cond_0

    .line 984
    invoke-static {p1}, Lorg/threeten/bp/chrono/Chronology;->a(Lorg/threeten/bp/temporal/TemporalAccessor;)Lorg/threeten/bp/chrono/Chronology;

    move-result-object v0

    invoke-virtual {v0, p1}, Lorg/threeten/bp/chrono/Chronology;->b(Lorg/threeten/bp/temporal/TemporalAccessor;)Lorg/threeten/bp/chrono/ChronoLocalDate;

    move-result-object v0

    sget-object v1, Lorg/threeten/bp/temporal/ChronoUnit;->i:Lorg/threeten/bp/temporal/ChronoUnit;

    invoke-virtual {v0, v6, v7, v1}, Lorg/threeten/bp/chrono/ChronoLocalDate;->e(JLorg/threeten/bp/temporal/TemporalUnit;)Lorg/threeten/bp/chrono/ChronoLocalDate;

    move-result-object v0

    invoke-direct {p0, v0}, Lorg/threeten/bp/temporal/WeekFields$ComputedDayOfField;->f(Lorg/threeten/bp/temporal/TemporalAccessor;)Lorg/threeten/bp/temporal/ValueRange;

    move-result-object v0

    .line 993
    :goto_0
    return-object v0

    .line 986
    :cond_0
    sget-object v1, Lorg/threeten/bp/temporal/ChronoField;->t:Lorg/threeten/bp/temporal/ChronoField;

    invoke-interface {p1, v1}, Lorg/threeten/bp/temporal/TemporalAccessor;->c(Lorg/threeten/bp/temporal/TemporalField;)I

    move-result v1

    invoke-direct {p0, v1, v0}, Lorg/threeten/bp/temporal/WeekFields$ComputedDayOfField;->a(II)I

    move-result v1

    .line 987
    sget-object v0, Lorg/threeten/bp/temporal/ChronoField;->A:Lorg/threeten/bp/temporal/ChronoField;

    invoke-interface {p1, v0}, Lorg/threeten/bp/temporal/TemporalAccessor;->c(Lorg/threeten/bp/temporal/TemporalField;)I

    move-result v0

    .line 988
    int-to-long v4, v0

    invoke-static {v4, v5}, Lorg/threeten/bp/Year;->a(J)Z

    move-result v0

    if-eqz v0, :cond_1

    const/16 v0, 0x16e

    .line 989
    :goto_1
    iget-object v4, p0, Lorg/threeten/bp/temporal/WeekFields$ComputedDayOfField;->b:Lorg/threeten/bp/temporal/WeekFields;

    invoke-virtual {v4}, Lorg/threeten/bp/temporal/WeekFields;->b()I

    move-result v4

    add-int/2addr v0, v4

    invoke-direct {p0, v1, v0}, Lorg/threeten/bp/temporal/WeekFields$ComputedDayOfField;->b(II)I

    move-result v0

    .line 990
    int-to-long v4, v0

    cmp-long v1, v2, v4

    if-ltz v1, :cond_2

    .line 991
    invoke-static {p1}, Lorg/threeten/bp/chrono/Chronology;->a(Lorg/threeten/bp/temporal/TemporalAccessor;)Lorg/threeten/bp/chrono/Chronology;

    move-result-object v0

    invoke-virtual {v0, p1}, Lorg/threeten/bp/chrono/Chronology;->b(Lorg/threeten/bp/temporal/TemporalAccessor;)Lorg/threeten/bp/chrono/ChronoLocalDate;

    move-result-object v0

    sget-object v1, Lorg/threeten/bp/temporal/ChronoUnit;->i:Lorg/threeten/bp/temporal/ChronoUnit;

    invoke-virtual {v0, v6, v7, v1}, Lorg/threeten/bp/chrono/ChronoLocalDate;->f(JLorg/threeten/bp/temporal/TemporalUnit;)Lorg/threeten/bp/chrono/ChronoLocalDate;

    move-result-object v0

    invoke-direct {p0, v0}, Lorg/threeten/bp/temporal/WeekFields$ComputedDayOfField;->f(Lorg/threeten/bp/temporal/TemporalAccessor;)Lorg/threeten/bp/temporal/ValueRange;

    move-result-object v0

    goto :goto_0

    .line 988
    :cond_1
    const/16 v0, 0x16d

    goto :goto_1

    .line 993
    :cond_2
    const-wide/16 v2, 0x1

    add-int/lit8 v0, v0, -0x1

    int-to-long v0, v0

    invoke-static {v2, v3, v0, v1}, Lorg/threeten/bp/temporal/ValueRange;->a(JJ)Lorg/threeten/bp/temporal/ValueRange;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public a(Lorg/threeten/bp/temporal/Temporal;J)Lorg/threeten/bp/temporal/Temporal;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<R::",
            "Lorg/threeten/bp/temporal/Temporal;",
            ">(TR;J)TR;"
        }
    .end annotation

    .prologue
    .line 748
    iget-object v0, p0, Lorg/threeten/bp/temporal/WeekFields$ComputedDayOfField;->e:Lorg/threeten/bp/temporal/ValueRange;

    invoke-virtual {v0, p2, p3, p0}, Lorg/threeten/bp/temporal/ValueRange;->b(JLorg/threeten/bp/temporal/TemporalField;)I

    move-result v1

    .line 749
    invoke-interface {p1, p0}, Lorg/threeten/bp/temporal/Temporal;->c(Lorg/threeten/bp/temporal/TemporalField;)I

    move-result v0

    .line 750
    if-ne v1, v0, :cond_0

    .line 779
    :goto_0
    return-object p1

    .line 753
    :cond_0
    iget-object v2, p0, Lorg/threeten/bp/temporal/WeekFields$ComputedDayOfField;->d:Lorg/threeten/bp/temporal/TemporalUnit;

    sget-object v3, Lorg/threeten/bp/temporal/ChronoUnit;->p:Lorg/threeten/bp/temporal/ChronoUnit;

    if-ne v2, v3, :cond_4

    .line 755
    iget-object v2, p0, Lorg/threeten/bp/temporal/WeekFields$ComputedDayOfField;->b:Lorg/threeten/bp/temporal/WeekFields;

    invoke-static {v2}, Lorg/threeten/bp/temporal/WeekFields;->a(Lorg/threeten/bp/temporal/WeekFields;)Lorg/threeten/bp/temporal/TemporalField;

    move-result-object v2

    invoke-interface {p1, v2}, Lorg/threeten/bp/temporal/Temporal;->c(Lorg/threeten/bp/temporal/TemporalField;)I

    move-result v2

    .line 756
    int-to-long v4, v0

    sub-long v4, p2, v4

    long-to-double v4, v4

    const-wide v6, 0x404a16b851eb851fL    # 52.1775

    mul-double/2addr v4, v6

    double-to-long v4, v4

    .line 757
    sget-object v0, Lorg/threeten/bp/temporal/ChronoUnit;->i:Lorg/threeten/bp/temporal/ChronoUnit;

    invoke-interface {p1, v4, v5, v0}, Lorg/threeten/bp/temporal/Temporal;->d(JLorg/threeten/bp/temporal/TemporalUnit;)Lorg/threeten/bp/temporal/Temporal;

    move-result-object v0

    .line 758
    invoke-interface {v0, p0}, Lorg/threeten/bp/temporal/Temporal;->c(Lorg/threeten/bp/temporal/TemporalField;)I

    move-result v3

    if-le v3, v1, :cond_2

    .line 761
    iget-object v1, p0, Lorg/threeten/bp/temporal/WeekFields$ComputedDayOfField;->b:Lorg/threeten/bp/temporal/WeekFields;

    invoke-static {v1}, Lorg/threeten/bp/temporal/WeekFields;->a(Lorg/threeten/bp/temporal/WeekFields;)Lorg/threeten/bp/temporal/TemporalField;

    move-result-object v1

    invoke-interface {v0, v1}, Lorg/threeten/bp/temporal/Temporal;->c(Lorg/threeten/bp/temporal/TemporalField;)I

    move-result v1

    .line 762
    int-to-long v2, v1

    sget-object v1, Lorg/threeten/bp/temporal/ChronoUnit;->i:Lorg/threeten/bp/temporal/ChronoUnit;

    invoke-interface {v0, v2, v3, v1}, Lorg/threeten/bp/temporal/Temporal;->c(JLorg/threeten/bp/temporal/TemporalUnit;)Lorg/threeten/bp/temporal/Temporal;

    move-result-object v0

    :cond_1
    :goto_1
    move-object p1, v0

    .line 775
    goto :goto_0

    .line 764
    :cond_2
    invoke-interface {v0, p0}, Lorg/threeten/bp/temporal/Temporal;->c(Lorg/threeten/bp/temporal/TemporalField;)I

    move-result v3

    if-ge v3, v1, :cond_3

    .line 766
    const-wide/16 v4, 0x2

    sget-object v3, Lorg/threeten/bp/temporal/ChronoUnit;->i:Lorg/threeten/bp/temporal/ChronoUnit;

    invoke-interface {v0, v4, v5, v3}, Lorg/threeten/bp/temporal/Temporal;->d(JLorg/threeten/bp/temporal/TemporalUnit;)Lorg/threeten/bp/temporal/Temporal;

    move-result-object v0

    .line 769
    :cond_3
    iget-object v3, p0, Lorg/threeten/bp/temporal/WeekFields$ComputedDayOfField;->b:Lorg/threeten/bp/temporal/WeekFields;

    invoke-static {v3}, Lorg/threeten/bp/temporal/WeekFields;->a(Lorg/threeten/bp/temporal/WeekFields;)Lorg/threeten/bp/temporal/TemporalField;

    move-result-object v3

    invoke-interface {v0, v3}, Lorg/threeten/bp/temporal/Temporal;->c(Lorg/threeten/bp/temporal/TemporalField;)I

    move-result v3

    .line 770
    sub-int/2addr v2, v3

    int-to-long v2, v2

    sget-object v4, Lorg/threeten/bp/temporal/ChronoUnit;->i:Lorg/threeten/bp/temporal/ChronoUnit;

    invoke-interface {v0, v2, v3, v4}, Lorg/threeten/bp/temporal/Temporal;->d(JLorg/threeten/bp/temporal/TemporalUnit;)Lorg/threeten/bp/temporal/Temporal;

    move-result-object v0

    .line 771
    invoke-interface {v0, p0}, Lorg/threeten/bp/temporal/Temporal;->c(Lorg/threeten/bp/temporal/TemporalField;)I

    move-result v2

    if-le v2, v1, :cond_1

    .line 772
    const-wide/16 v2, 0x1

    sget-object v1, Lorg/threeten/bp/temporal/ChronoUnit;->i:Lorg/threeten/bp/temporal/ChronoUnit;

    invoke-interface {v0, v2, v3, v1}, Lorg/threeten/bp/temporal/Temporal;->c(JLorg/threeten/bp/temporal/TemporalUnit;)Lorg/threeten/bp/temporal/Temporal;

    move-result-object v0

    goto :goto_1

    .line 778
    :cond_4
    sub-int v0, v1, v0

    .line 779
    int-to-long v0, v0

    iget-object v2, p0, Lorg/threeten/bp/temporal/WeekFields$ComputedDayOfField;->c:Lorg/threeten/bp/temporal/TemporalUnit;

    invoke-interface {p1, v0, v1, v2}, Lorg/threeten/bp/temporal/Temporal;->d(JLorg/threeten/bp/temporal/TemporalUnit;)Lorg/threeten/bp/temporal/Temporal;

    move-result-object p1

    goto :goto_0
.end method

.method public a(Ljava/util/Map;Lorg/threeten/bp/temporal/TemporalAccessor;Lorg/threeten/bp/format/ResolverStyle;)Lorg/threeten/bp/temporal/TemporalAccessor;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Lorg/threeten/bp/temporal/TemporalField;",
            "Ljava/lang/Long;",
            ">;",
            "Lorg/threeten/bp/temporal/TemporalAccessor;",
            "Lorg/threeten/bp/format/ResolverStyle;",
            ")",
            "Lorg/threeten/bp/temporal/TemporalAccessor;"
        }
    .end annotation

    .prologue
    const/4 v6, 0x7

    const/4 v1, 0x0

    const-wide/16 v10, 0x7

    const/4 v8, 0x1

    .line 785
    iget-object v0, p0, Lorg/threeten/bp/temporal/WeekFields$ComputedDayOfField;->b:Lorg/threeten/bp/temporal/WeekFields;

    invoke-virtual {v0}, Lorg/threeten/bp/temporal/WeekFields;->a()Lorg/threeten/bp/DayOfWeek;

    move-result-object v0

    invoke-virtual {v0}, Lorg/threeten/bp/DayOfWeek;->a()I

    move-result v3

    .line 786
    iget-object v0, p0, Lorg/threeten/bp/temporal/WeekFields$ComputedDayOfField;->d:Lorg/threeten/bp/temporal/TemporalUnit;

    sget-object v2, Lorg/threeten/bp/temporal/ChronoUnit;->i:Lorg/threeten/bp/temporal/ChronoUnit;

    if-ne v0, v2, :cond_0

    .line 787
    invoke-interface {p1, p0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    .line 788
    iget-object v0, p0, Lorg/threeten/bp/temporal/WeekFields$ComputedDayOfField;->e:Lorg/threeten/bp/temporal/ValueRange;

    invoke-virtual {v0, v4, v5, p0}, Lorg/threeten/bp/temporal/ValueRange;->b(JLorg/threeten/bp/temporal/TemporalField;)I

    move-result v0

    .line 789
    add-int/lit8 v2, v3, -0x1

    add-int/lit8 v0, v0, -0x1

    add-int/2addr v0, v2

    invoke-static {v0, v6}, Lorg/threeten/bp/jdk8/Jdk8Methods;->d(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    .line 790
    sget-object v2, Lorg/threeten/bp/temporal/ChronoField;->p:Lorg/threeten/bp/temporal/ChronoField;

    int-to-long v4, v0

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-interface {p1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-object v0, v1

    .line 897
    :goto_0
    return-object v0

    .line 793
    :cond_0
    sget-object v0, Lorg/threeten/bp/temporal/ChronoField;->p:Lorg/threeten/bp/temporal/ChronoField;

    invoke-interface {p1, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    move-object v0, v1

    .line 794
    goto :goto_0

    .line 798
    :cond_1
    iget-object v0, p0, Lorg/threeten/bp/temporal/WeekFields$ComputedDayOfField;->d:Lorg/threeten/bp/temporal/TemporalUnit;

    sget-object v2, Lorg/threeten/bp/temporal/ChronoUnit;->p:Lorg/threeten/bp/temporal/ChronoUnit;

    if-ne v0, v2, :cond_5

    .line 799
    iget-object v0, p0, Lorg/threeten/bp/temporal/WeekFields$ComputedDayOfField;->b:Lorg/threeten/bp/temporal/WeekFields;

    invoke-static {v0}, Lorg/threeten/bp/temporal/WeekFields;->a(Lorg/threeten/bp/temporal/WeekFields;)Lorg/threeten/bp/temporal/TemporalField;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    move-object v0, v1

    .line 800
    goto :goto_0

    .line 802
    :cond_2
    invoke-static {p2}, Lorg/threeten/bp/chrono/Chronology;->a(Lorg/threeten/bp/temporal/TemporalAccessor;)Lorg/threeten/bp/chrono/Chronology;

    move-result-object v1

    .line 803
    sget-object v2, Lorg/threeten/bp/temporal/ChronoField;->p:Lorg/threeten/bp/temporal/ChronoField;

    sget-object v0, Lorg/threeten/bp/temporal/ChronoField;->p:Lorg/threeten/bp/temporal/ChronoField;

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Lorg/threeten/bp/temporal/ChronoField;->b(J)I

    move-result v0

    .line 804
    sub-int/2addr v0, v3

    invoke-static {v0, v6}, Lorg/threeten/bp/jdk8/Jdk8Methods;->d(II)I

    move-result v0

    add-int/lit8 v4, v0, 0x1

    .line 805
    invoke-virtual {p0}, Lorg/threeten/bp/temporal/WeekFields$ComputedDayOfField;->a()Lorg/threeten/bp/temporal/ValueRange;

    move-result-object v2

    invoke-interface {p1, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    invoke-virtual {v2, v6, v7, p0}, Lorg/threeten/bp/temporal/ValueRange;->b(JLorg/threeten/bp/temporal/TemporalField;)I

    move-result v0

    .line 808
    sget-object v2, Lorg/threeten/bp/format/ResolverStyle;->c:Lorg/threeten/bp/format/ResolverStyle;

    if-ne p3, v2, :cond_3

    .line 809
    iget-object v2, p0, Lorg/threeten/bp/temporal/WeekFields$ComputedDayOfField;->b:Lorg/threeten/bp/temporal/WeekFields;

    invoke-virtual {v2}, Lorg/threeten/bp/temporal/WeekFields;->b()I

    move-result v2

    invoke-virtual {v1, v0, v8, v2}, Lorg/threeten/bp/chrono/Chronology;->a(III)Lorg/threeten/bp/chrono/ChronoLocalDate;

    move-result-object v2

    .line 810
    iget-object v0, p0, Lorg/threeten/bp/temporal/WeekFields$ComputedDayOfField;->b:Lorg/threeten/bp/temporal/WeekFields;

    invoke-static {v0}, Lorg/threeten/bp/temporal/WeekFields;->a(Lorg/threeten/bp/temporal/WeekFields;)Lorg/threeten/bp/temporal/TemporalField;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    .line 811
    invoke-direct {p0, v2, v3}, Lorg/threeten/bp/temporal/WeekFields$ComputedDayOfField;->a(Lorg/threeten/bp/temporal/TemporalAccessor;I)I

    move-result v3

    .line 812
    invoke-direct {p0, v2, v3}, Lorg/threeten/bp/temporal/WeekFields$ComputedDayOfField;->c(Lorg/threeten/bp/temporal/TemporalAccessor;I)J

    move-result-wide v6

    sub-long/2addr v0, v6

    .line 813
    mul-long/2addr v0, v10

    sub-int v3, v4, v3

    int-to-long v4, v3

    add-long/2addr v0, v4

    .line 822
    :goto_1
    sget-object v3, Lorg/threeten/bp/temporal/ChronoUnit;->h:Lorg/threeten/bp/temporal/ChronoUnit;

    invoke-virtual {v2, v0, v1, v3}, Lorg/threeten/bp/chrono/ChronoLocalDate;->f(JLorg/threeten/bp/temporal/TemporalUnit;)Lorg/threeten/bp/chrono/ChronoLocalDate;

    move-result-object v1

    .line 823
    sget-object v0, Lorg/threeten/bp/format/ResolverStyle;->a:Lorg/threeten/bp/format/ResolverStyle;

    if-ne p3, v0, :cond_4

    .line 824
    invoke-virtual {v1, p0}, Lorg/threeten/bp/chrono/ChronoLocalDate;->d(Lorg/threeten/bp/temporal/TemporalField;)J

    move-result-wide v2

    invoke-interface {p1, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    cmp-long v0, v2, v4

    if-eqz v0, :cond_4

    .line 825
    new-instance v0, Lorg/threeten/bp/DateTimeException;

    const-string v1, "Strict mode rejected date parsed to a different year"

    invoke-direct {v0, v1}, Lorg/threeten/bp/DateTimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 815
    :cond_3
    iget-object v2, p0, Lorg/threeten/bp/temporal/WeekFields$ComputedDayOfField;->b:Lorg/threeten/bp/temporal/WeekFields;

    invoke-virtual {v2}, Lorg/threeten/bp/temporal/WeekFields;->b()I

    move-result v2

    invoke-virtual {v1, v0, v8, v2}, Lorg/threeten/bp/chrono/Chronology;->a(III)Lorg/threeten/bp/chrono/ChronoLocalDate;

    move-result-object v2

    .line 816
    iget-object v0, p0, Lorg/threeten/bp/temporal/WeekFields$ComputedDayOfField;->b:Lorg/threeten/bp/temporal/WeekFields;

    invoke-static {v0}, Lorg/threeten/bp/temporal/WeekFields;->a(Lorg/threeten/bp/temporal/WeekFields;)Lorg/threeten/bp/temporal/TemporalField;

    move-result-object v0

    invoke-interface {v0}, Lorg/threeten/bp/temporal/TemporalField;->a()Lorg/threeten/bp/temporal/ValueRange;

    move-result-object v1

    iget-object v0, p0, Lorg/threeten/bp/temporal/WeekFields$ComputedDayOfField;->b:Lorg/threeten/bp/temporal/WeekFields;

    invoke-static {v0}, Lorg/threeten/bp/temporal/WeekFields;->a(Lorg/threeten/bp/temporal/WeekFields;)Lorg/threeten/bp/temporal/TemporalField;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    iget-object v0, p0, Lorg/threeten/bp/temporal/WeekFields$ComputedDayOfField;->b:Lorg/threeten/bp/temporal/WeekFields;

    invoke-static {v0}, Lorg/threeten/bp/temporal/WeekFields;->a(Lorg/threeten/bp/temporal/WeekFields;)Lorg/threeten/bp/temporal/TemporalField;

    move-result-object v0

    invoke-virtual {v1, v6, v7, v0}, Lorg/threeten/bp/temporal/ValueRange;->b(JLorg/threeten/bp/temporal/TemporalField;)I

    move-result v0

    int-to-long v0, v0

    .line 818
    invoke-direct {p0, v2, v3}, Lorg/threeten/bp/temporal/WeekFields$ComputedDayOfField;->a(Lorg/threeten/bp/temporal/TemporalAccessor;I)I

    move-result v3

    .line 819
    invoke-direct {p0, v2, v3}, Lorg/threeten/bp/temporal/WeekFields$ComputedDayOfField;->c(Lorg/threeten/bp/temporal/TemporalAccessor;I)J

    move-result-wide v6

    sub-long/2addr v0, v6

    .line 820
    mul-long/2addr v0, v10

    sub-int v3, v4, v3

    int-to-long v4, v3

    add-long/2addr v0, v4

    goto :goto_1

    .line 828
    :cond_4
    invoke-interface {p1, p0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 829
    iget-object v0, p0, Lorg/threeten/bp/temporal/WeekFields$ComputedDayOfField;->b:Lorg/threeten/bp/temporal/WeekFields;

    invoke-static {v0}, Lorg/threeten/bp/temporal/WeekFields;->a(Lorg/threeten/bp/temporal/WeekFields;)Lorg/threeten/bp/temporal/TemporalField;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 830
    sget-object v0, Lorg/threeten/bp/temporal/ChronoField;->p:Lorg/threeten/bp/temporal/ChronoField;

    invoke-interface {p1, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-object v0, v1

    .line 831
    goto/16 :goto_0

    .line 834
    :cond_5
    sget-object v0, Lorg/threeten/bp/temporal/ChronoField;->A:Lorg/threeten/bp/temporal/ChronoField;

    invoke-interface {p1, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_6

    move-object v0, v1

    .line 835
    goto/16 :goto_0

    .line 837
    :cond_6
    sget-object v2, Lorg/threeten/bp/temporal/ChronoField;->p:Lorg/threeten/bp/temporal/ChronoField;

    sget-object v0, Lorg/threeten/bp/temporal/ChronoField;->p:Lorg/threeten/bp/temporal/ChronoField;

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Lorg/threeten/bp/temporal/ChronoField;->b(J)I

    move-result v0

    .line 838
    sub-int/2addr v0, v3

    invoke-static {v0, v6}, Lorg/threeten/bp/jdk8/Jdk8Methods;->d(II)I

    move-result v0

    add-int/lit8 v4, v0, 0x1

    .line 839
    sget-object v2, Lorg/threeten/bp/temporal/ChronoField;->A:Lorg/threeten/bp/temporal/ChronoField;

    sget-object v0, Lorg/threeten/bp/temporal/ChronoField;->A:Lorg/threeten/bp/temporal/ChronoField;

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    invoke-virtual {v2, v6, v7}, Lorg/threeten/bp/temporal/ChronoField;->b(J)I

    move-result v2

    .line 840
    invoke-static {p2}, Lorg/threeten/bp/chrono/Chronology;->a(Lorg/threeten/bp/temporal/TemporalAccessor;)Lorg/threeten/bp/chrono/Chronology;

    move-result-object v5

    .line 841
    iget-object v0, p0, Lorg/threeten/bp/temporal/WeekFields$ComputedDayOfField;->d:Lorg/threeten/bp/temporal/TemporalUnit;

    sget-object v6, Lorg/threeten/bp/temporal/ChronoUnit;->j:Lorg/threeten/bp/temporal/ChronoUnit;

    if-ne v0, v6, :cond_a

    .line 842
    sget-object v0, Lorg/threeten/bp/temporal/ChronoField;->x:Lorg/threeten/bp/temporal/ChronoField;

    invoke-interface {p1, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_7

    move-object v0, v1

    .line 843
    goto/16 :goto_0

    .line 845
    :cond_7
    invoke-interface {p1, p0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    .line 848
    sget-object v0, Lorg/threeten/bp/format/ResolverStyle;->c:Lorg/threeten/bp/format/ResolverStyle;

    if-ne p3, v0, :cond_8

    .line 849
    sget-object v0, Lorg/threeten/bp/temporal/ChronoField;->x:Lorg/threeten/bp/temporal/ChronoField;

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    .line 850
    invoke-virtual {v5, v2, v8, v8}, Lorg/threeten/bp/chrono/Chronology;->a(III)Lorg/threeten/bp/chrono/ChronoLocalDate;

    move-result-object v2

    .line 851
    const-wide/16 v8, 0x1

    sub-long/2addr v0, v8

    sget-object v5, Lorg/threeten/bp/temporal/ChronoUnit;->j:Lorg/threeten/bp/temporal/ChronoUnit;

    invoke-virtual {v2, v0, v1, v5}, Lorg/threeten/bp/chrono/ChronoLocalDate;->f(JLorg/threeten/bp/temporal/TemporalUnit;)Lorg/threeten/bp/chrono/ChronoLocalDate;

    move-result-object v2

    .line 852
    invoke-direct {p0, v2, v3}, Lorg/threeten/bp/temporal/WeekFields$ComputedDayOfField;->a(Lorg/threeten/bp/temporal/TemporalAccessor;I)I

    move-result v0

    .line 853
    invoke-direct {p0, v2, v0}, Lorg/threeten/bp/temporal/WeekFields$ComputedDayOfField;->b(Lorg/threeten/bp/temporal/TemporalAccessor;I)J

    move-result-wide v8

    sub-long/2addr v6, v8

    .line 854
    mul-long/2addr v6, v10

    sub-int v0, v4, v0

    int-to-long v0, v0

    add-long/2addr v0, v6

    .line 863
    :goto_2
    sget-object v3, Lorg/threeten/bp/temporal/ChronoUnit;->h:Lorg/threeten/bp/temporal/ChronoUnit;

    invoke-virtual {v2, v0, v1, v3}, Lorg/threeten/bp/chrono/ChronoLocalDate;->f(JLorg/threeten/bp/temporal/TemporalUnit;)Lorg/threeten/bp/chrono/ChronoLocalDate;

    move-result-object v1

    .line 864
    sget-object v0, Lorg/threeten/bp/format/ResolverStyle;->a:Lorg/threeten/bp/format/ResolverStyle;

    if-ne p3, v0, :cond_9

    .line 865
    sget-object v0, Lorg/threeten/bp/temporal/ChronoField;->x:Lorg/threeten/bp/temporal/ChronoField;

    invoke-virtual {v1, v0}, Lorg/threeten/bp/chrono/ChronoLocalDate;->d(Lorg/threeten/bp/temporal/TemporalField;)J

    move-result-wide v2

    sget-object v0, Lorg/threeten/bp/temporal/ChronoField;->x:Lorg/threeten/bp/temporal/ChronoField;

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    cmp-long v0, v2, v4

    if-eqz v0, :cond_9

    .line 866
    new-instance v0, Lorg/threeten/bp/DateTimeException;

    const-string v1, "Strict mode rejected date parsed to a different month"

    invoke-direct {v0, v1}, Lorg/threeten/bp/DateTimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 856
    :cond_8
    sget-object v1, Lorg/threeten/bp/temporal/ChronoField;->x:Lorg/threeten/bp/temporal/ChronoField;

    sget-object v0, Lorg/threeten/bp/temporal/ChronoField;->x:Lorg/threeten/bp/temporal/ChronoField;

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    invoke-virtual {v1, v8, v9}, Lorg/threeten/bp/temporal/ChronoField;->b(J)I

    move-result v0

    .line 857
    const/16 v1, 0x8

    invoke-virtual {v5, v2, v0, v1}, Lorg/threeten/bp/chrono/Chronology;->a(III)Lorg/threeten/bp/chrono/ChronoLocalDate;

    move-result-object v2

    .line 858
    invoke-direct {p0, v2, v3}, Lorg/threeten/bp/temporal/WeekFields$ComputedDayOfField;->a(Lorg/threeten/bp/temporal/TemporalAccessor;I)I

    move-result v0

    .line 859
    iget-object v1, p0, Lorg/threeten/bp/temporal/WeekFields$ComputedDayOfField;->e:Lorg/threeten/bp/temporal/ValueRange;

    invoke-virtual {v1, v6, v7, p0}, Lorg/threeten/bp/temporal/ValueRange;->b(JLorg/threeten/bp/temporal/TemporalField;)I

    move-result v1

    .line 860
    int-to-long v6, v1

    invoke-direct {p0, v2, v0}, Lorg/threeten/bp/temporal/WeekFields$ComputedDayOfField;->b(Lorg/threeten/bp/temporal/TemporalAccessor;I)J

    move-result-wide v8

    sub-long/2addr v6, v8

    .line 861
    mul-long/2addr v6, v10

    sub-int v0, v4, v0

    int-to-long v0, v0

    add-long/2addr v0, v6

    goto :goto_2

    .line 869
    :cond_9
    invoke-interface {p1, p0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 870
    sget-object v0, Lorg/threeten/bp/temporal/ChronoField;->A:Lorg/threeten/bp/temporal/ChronoField;

    invoke-interface {p1, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 871
    sget-object v0, Lorg/threeten/bp/temporal/ChronoField;->x:Lorg/threeten/bp/temporal/ChronoField;

    invoke-interface {p1, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 872
    sget-object v0, Lorg/threeten/bp/temporal/ChronoField;->p:Lorg/threeten/bp/temporal/ChronoField;

    invoke-interface {p1, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-object v0, v1

    .line 873
    goto/16 :goto_0

    .line 874
    :cond_a
    iget-object v0, p0, Lorg/threeten/bp/temporal/WeekFields$ComputedDayOfField;->d:Lorg/threeten/bp/temporal/TemporalUnit;

    sget-object v1, Lorg/threeten/bp/temporal/ChronoUnit;->k:Lorg/threeten/bp/temporal/ChronoUnit;

    if-ne v0, v1, :cond_d

    .line 875
    invoke-interface {p1, p0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    .line 876
    invoke-virtual {v5, v2, v8, v8}, Lorg/threeten/bp/chrono/Chronology;->a(III)Lorg/threeten/bp/chrono/ChronoLocalDate;

    move-result-object v2

    .line 878
    sget-object v5, Lorg/threeten/bp/format/ResolverStyle;->c:Lorg/threeten/bp/format/ResolverStyle;

    if-ne p3, v5, :cond_b

    .line 879
    invoke-direct {p0, v2, v3}, Lorg/threeten/bp/temporal/WeekFields$ComputedDayOfField;->a(Lorg/threeten/bp/temporal/TemporalAccessor;I)I

    move-result v3

    .line 880
    invoke-direct {p0, v2, v3}, Lorg/threeten/bp/temporal/WeekFields$ComputedDayOfField;->c(Lorg/threeten/bp/temporal/TemporalAccessor;I)J

    move-result-wide v6

    sub-long/2addr v0, v6

    .line 881
    mul-long/2addr v0, v10

    sub-int v3, v4, v3

    int-to-long v4, v3

    add-long/2addr v0, v4

    .line 888
    :goto_3
    sget-object v3, Lorg/threeten/bp/temporal/ChronoUnit;->h:Lorg/threeten/bp/temporal/ChronoUnit;

    invoke-virtual {v2, v0, v1, v3}, Lorg/threeten/bp/chrono/ChronoLocalDate;->f(JLorg/threeten/bp/temporal/TemporalUnit;)Lorg/threeten/bp/chrono/ChronoLocalDate;

    move-result-object v1

    .line 889
    sget-object v0, Lorg/threeten/bp/format/ResolverStyle;->a:Lorg/threeten/bp/format/ResolverStyle;

    if-ne p3, v0, :cond_c

    .line 890
    sget-object v0, Lorg/threeten/bp/temporal/ChronoField;->A:Lorg/threeten/bp/temporal/ChronoField;

    invoke-virtual {v1, v0}, Lorg/threeten/bp/chrono/ChronoLocalDate;->d(Lorg/threeten/bp/temporal/TemporalField;)J

    move-result-wide v2

    sget-object v0, Lorg/threeten/bp/temporal/ChronoField;->A:Lorg/threeten/bp/temporal/ChronoField;

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    cmp-long v0, v2, v4

    if-eqz v0, :cond_c

    .line 891
    new-instance v0, Lorg/threeten/bp/DateTimeException;

    const-string v1, "Strict mode rejected date parsed to a different year"

    invoke-direct {v0, v1}, Lorg/threeten/bp/DateTimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 883
    :cond_b
    invoke-direct {p0, v2, v3}, Lorg/threeten/bp/temporal/WeekFields$ComputedDayOfField;->a(Lorg/threeten/bp/temporal/TemporalAccessor;I)I

    move-result v3

    .line 884
    iget-object v5, p0, Lorg/threeten/bp/temporal/WeekFields$ComputedDayOfField;->e:Lorg/threeten/bp/temporal/ValueRange;

    invoke-virtual {v5, v0, v1, p0}, Lorg/threeten/bp/temporal/ValueRange;->b(JLorg/threeten/bp/temporal/TemporalField;)I

    move-result v0

    .line 885
    int-to-long v0, v0

    invoke-direct {p0, v2, v3}, Lorg/threeten/bp/temporal/WeekFields$ComputedDayOfField;->c(Lorg/threeten/bp/temporal/TemporalAccessor;I)J

    move-result-wide v6

    sub-long/2addr v0, v6

    .line 886
    mul-long/2addr v0, v10

    sub-int v3, v4, v3

    int-to-long v4, v3

    add-long/2addr v0, v4

    goto :goto_3

    .line 894
    :cond_c
    invoke-interface {p1, p0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 895
    sget-object v0, Lorg/threeten/bp/temporal/ChronoField;->A:Lorg/threeten/bp/temporal/ChronoField;

    invoke-interface {p1, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 896
    sget-object v0, Lorg/threeten/bp/temporal/ChronoField;->p:Lorg/threeten/bp/temporal/ChronoField;

    invoke-interface {p1, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-object v0, v1

    .line 897
    goto/16 :goto_0

    .line 899
    :cond_d
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "unreachable"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public a()Lorg/threeten/bp/temporal/ValueRange;
    .locals 1

    .prologue
    .line 916
    iget-object v0, p0, Lorg/threeten/bp/temporal/WeekFields$ComputedDayOfField;->e:Lorg/threeten/bp/temporal/ValueRange;

    return-object v0
.end method

.method public a(Lorg/threeten/bp/temporal/TemporalAccessor;)Z
    .locals 2

    .prologue
    .line 932
    sget-object v0, Lorg/threeten/bp/temporal/ChronoField;->p:Lorg/threeten/bp/temporal/ChronoField;

    invoke-interface {p1, v0}, Lorg/threeten/bp/temporal/TemporalAccessor;->a(Lorg/threeten/bp/temporal/TemporalField;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 933
    iget-object v0, p0, Lorg/threeten/bp/temporal/WeekFields$ComputedDayOfField;->d:Lorg/threeten/bp/temporal/TemporalUnit;

    sget-object v1, Lorg/threeten/bp/temporal/ChronoUnit;->i:Lorg/threeten/bp/temporal/ChronoUnit;

    if-ne v0, v1, :cond_0

    .line 934
    const/4 v0, 0x1

    .line 945
    :goto_0
    return v0

    .line 935
    :cond_0
    iget-object v0, p0, Lorg/threeten/bp/temporal/WeekFields$ComputedDayOfField;->d:Lorg/threeten/bp/temporal/TemporalUnit;

    sget-object v1, Lorg/threeten/bp/temporal/ChronoUnit;->j:Lorg/threeten/bp/temporal/ChronoUnit;

    if-ne v0, v1, :cond_1

    .line 936
    sget-object v0, Lorg/threeten/bp/temporal/ChronoField;->s:Lorg/threeten/bp/temporal/ChronoField;

    invoke-interface {p1, v0}, Lorg/threeten/bp/temporal/TemporalAccessor;->a(Lorg/threeten/bp/temporal/TemporalField;)Z

    move-result v0

    goto :goto_0

    .line 937
    :cond_1
    iget-object v0, p0, Lorg/threeten/bp/temporal/WeekFields$ComputedDayOfField;->d:Lorg/threeten/bp/temporal/TemporalUnit;

    sget-object v1, Lorg/threeten/bp/temporal/ChronoUnit;->k:Lorg/threeten/bp/temporal/ChronoUnit;

    if-ne v0, v1, :cond_2

    .line 938
    sget-object v0, Lorg/threeten/bp/temporal/ChronoField;->t:Lorg/threeten/bp/temporal/ChronoField;

    invoke-interface {p1, v0}, Lorg/threeten/bp/temporal/TemporalAccessor;->a(Lorg/threeten/bp/temporal/TemporalField;)Z

    move-result v0

    goto :goto_0

    .line 939
    :cond_2
    iget-object v0, p0, Lorg/threeten/bp/temporal/WeekFields$ComputedDayOfField;->d:Lorg/threeten/bp/temporal/TemporalUnit;

    sget-object v1, Lorg/threeten/bp/temporal/IsoFields;->e:Lorg/threeten/bp/temporal/TemporalUnit;

    if-ne v0, v1, :cond_3

    .line 940
    sget-object v0, Lorg/threeten/bp/temporal/ChronoField;->u:Lorg/threeten/bp/temporal/ChronoField;

    invoke-interface {p1, v0}, Lorg/threeten/bp/temporal/TemporalAccessor;->a(Lorg/threeten/bp/temporal/TemporalField;)Z

    move-result v0

    goto :goto_0

    .line 941
    :cond_3
    iget-object v0, p0, Lorg/threeten/bp/temporal/WeekFields$ComputedDayOfField;->d:Lorg/threeten/bp/temporal/TemporalUnit;

    sget-object v1, Lorg/threeten/bp/temporal/ChronoUnit;->p:Lorg/threeten/bp/temporal/ChronoUnit;

    if-ne v0, v1, :cond_4

    .line 942
    sget-object v0, Lorg/threeten/bp/temporal/ChronoField;->u:Lorg/threeten/bp/temporal/ChronoField;

    invoke-interface {p1, v0}, Lorg/threeten/bp/temporal/TemporalAccessor;->a(Lorg/threeten/bp/temporal/TemporalField;)Z

    move-result v0

    goto :goto_0

    .line 945
    :cond_4
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b(Lorg/threeten/bp/temporal/TemporalAccessor;)Lorg/threeten/bp/temporal/ValueRange;
    .locals 6

    .prologue
    .line 950
    iget-object v0, p0, Lorg/threeten/bp/temporal/WeekFields$ComputedDayOfField;->d:Lorg/threeten/bp/temporal/TemporalUnit;

    sget-object v1, Lorg/threeten/bp/temporal/ChronoUnit;->i:Lorg/threeten/bp/temporal/ChronoUnit;

    if-ne v0, v1, :cond_0

    .line 951
    iget-object v0, p0, Lorg/threeten/bp/temporal/WeekFields$ComputedDayOfField;->e:Lorg/threeten/bp/temporal/ValueRange;

    .line 974
    :goto_0
    return-object v0

    .line 955
    :cond_0
    iget-object v0, p0, Lorg/threeten/bp/temporal/WeekFields$ComputedDayOfField;->d:Lorg/threeten/bp/temporal/TemporalUnit;

    sget-object v1, Lorg/threeten/bp/temporal/ChronoUnit;->j:Lorg/threeten/bp/temporal/ChronoUnit;

    if-ne v0, v1, :cond_1

    .line 956
    sget-object v0, Lorg/threeten/bp/temporal/ChronoField;->s:Lorg/threeten/bp/temporal/ChronoField;

    .line 968
    :goto_1
    iget-object v1, p0, Lorg/threeten/bp/temporal/WeekFields$ComputedDayOfField;->b:Lorg/threeten/bp/temporal/WeekFields;

    invoke-virtual {v1}, Lorg/threeten/bp/temporal/WeekFields;->a()Lorg/threeten/bp/DayOfWeek;

    move-result-object v1

    invoke-virtual {v1}, Lorg/threeten/bp/DayOfWeek;->a()I

    move-result v1

    .line 969
    sget-object v2, Lorg/threeten/bp/temporal/ChronoField;->p:Lorg/threeten/bp/temporal/ChronoField;

    invoke-interface {p1, v2}, Lorg/threeten/bp/temporal/TemporalAccessor;->c(Lorg/threeten/bp/temporal/TemporalField;)I

    move-result v2

    .line 970
    sub-int v1, v2, v1

    const/4 v2, 0x7

    invoke-static {v1, v2}, Lorg/threeten/bp/jdk8/Jdk8Methods;->d(II)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    .line 972
    invoke-interface {p1, v0}, Lorg/threeten/bp/temporal/TemporalAccessor;->c(Lorg/threeten/bp/temporal/TemporalField;)I

    move-result v2

    invoke-direct {p0, v2, v1}, Lorg/threeten/bp/temporal/WeekFields$ComputedDayOfField;->a(II)I

    move-result v1

    .line 973
    invoke-interface {p1, v0}, Lorg/threeten/bp/temporal/TemporalAccessor;->b(Lorg/threeten/bp/temporal/TemporalField;)Lorg/threeten/bp/temporal/ValueRange;

    move-result-object v0

    .line 974
    invoke-virtual {v0}, Lorg/threeten/bp/temporal/ValueRange;->b()J

    move-result-wide v2

    long-to-int v2, v2

    invoke-direct {p0, v1, v2}, Lorg/threeten/bp/temporal/WeekFields$ComputedDayOfField;->b(II)I

    move-result v2

    int-to-long v2, v2

    invoke-virtual {v0}, Lorg/threeten/bp/temporal/ValueRange;->c()J

    move-result-wide v4

    long-to-int v0, v4

    invoke-direct {p0, v1, v0}, Lorg/threeten/bp/temporal/WeekFields$ComputedDayOfField;->b(II)I

    move-result v0

    int-to-long v0, v0

    invoke-static {v2, v3, v0, v1}, Lorg/threeten/bp/temporal/ValueRange;->a(JJ)Lorg/threeten/bp/temporal/ValueRange;

    move-result-object v0

    goto :goto_0

    .line 957
    :cond_1
    iget-object v0, p0, Lorg/threeten/bp/temporal/WeekFields$ComputedDayOfField;->d:Lorg/threeten/bp/temporal/TemporalUnit;

    sget-object v1, Lorg/threeten/bp/temporal/ChronoUnit;->k:Lorg/threeten/bp/temporal/ChronoUnit;

    if-ne v0, v1, :cond_2

    .line 958
    sget-object v0, Lorg/threeten/bp/temporal/ChronoField;->t:Lorg/threeten/bp/temporal/ChronoField;

    goto :goto_1

    .line 959
    :cond_2
    iget-object v0, p0, Lorg/threeten/bp/temporal/WeekFields$ComputedDayOfField;->d:Lorg/threeten/bp/temporal/TemporalUnit;

    sget-object v1, Lorg/threeten/bp/temporal/IsoFields;->e:Lorg/threeten/bp/temporal/TemporalUnit;

    if-ne v0, v1, :cond_3

    .line 960
    invoke-direct {p0, p1}, Lorg/threeten/bp/temporal/WeekFields$ComputedDayOfField;->f(Lorg/threeten/bp/temporal/TemporalAccessor;)Lorg/threeten/bp/temporal/ValueRange;

    move-result-object v0

    goto :goto_0

    .line 961
    :cond_3
    iget-object v0, p0, Lorg/threeten/bp/temporal/WeekFields$ComputedDayOfField;->d:Lorg/threeten/bp/temporal/TemporalUnit;

    sget-object v1, Lorg/threeten/bp/temporal/ChronoUnit;->p:Lorg/threeten/bp/temporal/ChronoUnit;

    if-ne v0, v1, :cond_4

    .line 962
    sget-object v0, Lorg/threeten/bp/temporal/ChronoField;->A:Lorg/threeten/bp/temporal/ChronoField;

    invoke-interface {p1, v0}, Lorg/threeten/bp/temporal/TemporalAccessor;->b(Lorg/threeten/bp/temporal/TemporalField;)Lorg/threeten/bp/temporal/ValueRange;

    move-result-object v0

    goto :goto_0

    .line 964
    :cond_4
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "unreachable"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public b()Z
    .locals 1

    .prologue
    .line 922
    const/4 v0, 0x1

    return v0
.end method

.method public c(Lorg/threeten/bp/temporal/TemporalAccessor;)J
    .locals 3

    .prologue
    .line 634
    iget-object v0, p0, Lorg/threeten/bp/temporal/WeekFields$ComputedDayOfField;->b:Lorg/threeten/bp/temporal/WeekFields;

    invoke-virtual {v0}, Lorg/threeten/bp/temporal/WeekFields;->a()Lorg/threeten/bp/DayOfWeek;

    move-result-object v0

    invoke-virtual {v0}, Lorg/threeten/bp/DayOfWeek;->a()I

    move-result v0

    .line 635
    sget-object v1, Lorg/threeten/bp/temporal/ChronoField;->p:Lorg/threeten/bp/temporal/ChronoField;

    invoke-interface {p1, v1}, Lorg/threeten/bp/temporal/TemporalAccessor;->c(Lorg/threeten/bp/temporal/TemporalField;)I

    move-result v1

    .line 636
    sub-int v0, v1, v0

    const/4 v1, 0x7

    invoke-static {v0, v1}, Lorg/threeten/bp/jdk8/Jdk8Methods;->d(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    .line 638
    iget-object v1, p0, Lorg/threeten/bp/temporal/WeekFields$ComputedDayOfField;->d:Lorg/threeten/bp/temporal/TemporalUnit;

    sget-object v2, Lorg/threeten/bp/temporal/ChronoUnit;->i:Lorg/threeten/bp/temporal/ChronoUnit;

    if-ne v1, v2, :cond_0

    .line 639
    int-to-long v0, v0

    .line 651
    :goto_0
    return-wide v0

    .line 640
    :cond_0
    iget-object v1, p0, Lorg/threeten/bp/temporal/WeekFields$ComputedDayOfField;->d:Lorg/threeten/bp/temporal/TemporalUnit;

    sget-object v2, Lorg/threeten/bp/temporal/ChronoUnit;->j:Lorg/threeten/bp/temporal/ChronoUnit;

    if-ne v1, v2, :cond_1

    .line 641
    sget-object v1, Lorg/threeten/bp/temporal/ChronoField;->s:Lorg/threeten/bp/temporal/ChronoField;

    invoke-interface {p1, v1}, Lorg/threeten/bp/temporal/TemporalAccessor;->c(Lorg/threeten/bp/temporal/TemporalField;)I

    move-result v1

    .line 642
    invoke-direct {p0, v1, v0}, Lorg/threeten/bp/temporal/WeekFields$ComputedDayOfField;->a(II)I

    move-result v0

    .line 643
    invoke-direct {p0, v0, v1}, Lorg/threeten/bp/temporal/WeekFields$ComputedDayOfField;->b(II)I

    move-result v0

    int-to-long v0, v0

    goto :goto_0

    .line 644
    :cond_1
    iget-object v1, p0, Lorg/threeten/bp/temporal/WeekFields$ComputedDayOfField;->d:Lorg/threeten/bp/temporal/TemporalUnit;

    sget-object v2, Lorg/threeten/bp/temporal/ChronoUnit;->k:Lorg/threeten/bp/temporal/ChronoUnit;

    if-ne v1, v2, :cond_2

    .line 645
    sget-object v1, Lorg/threeten/bp/temporal/ChronoField;->t:Lorg/threeten/bp/temporal/ChronoField;

    invoke-interface {p1, v1}, Lorg/threeten/bp/temporal/TemporalAccessor;->c(Lorg/threeten/bp/temporal/TemporalField;)I

    move-result v1

    .line 646
    invoke-direct {p0, v1, v0}, Lorg/threeten/bp/temporal/WeekFields$ComputedDayOfField;->a(II)I

    move-result v0

    .line 647
    invoke-direct {p0, v0, v1}, Lorg/threeten/bp/temporal/WeekFields$ComputedDayOfField;->b(II)I

    move-result v0

    int-to-long v0, v0

    goto :goto_0

    .line 648
    :cond_2
    iget-object v0, p0, Lorg/threeten/bp/temporal/WeekFields$ComputedDayOfField;->d:Lorg/threeten/bp/temporal/TemporalUnit;

    sget-object v1, Lorg/threeten/bp/temporal/IsoFields;->e:Lorg/threeten/bp/temporal/TemporalUnit;

    if-ne v0, v1, :cond_3

    .line 649
    invoke-direct {p0, p1}, Lorg/threeten/bp/temporal/WeekFields$ComputedDayOfField;->d(Lorg/threeten/bp/temporal/TemporalAccessor;)I

    move-result v0

    int-to-long v0, v0

    goto :goto_0

    .line 650
    :cond_3
    iget-object v0, p0, Lorg/threeten/bp/temporal/WeekFields$ComputedDayOfField;->d:Lorg/threeten/bp/temporal/TemporalUnit;

    sget-object v1, Lorg/threeten/bp/temporal/ChronoUnit;->p:Lorg/threeten/bp/temporal/ChronoUnit;

    if-ne v0, v1, :cond_4

    .line 651
    invoke-direct {p0, p1}, Lorg/threeten/bp/temporal/WeekFields$ComputedDayOfField;->e(Lorg/threeten/bp/temporal/TemporalAccessor;)I

    move-result v0

    int-to-long v0, v0

    goto :goto_0

    .line 653
    :cond_4
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "unreachable"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public c()Z
    .locals 1

    .prologue
    .line 927
    const/4 v0, 0x0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1008
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lorg/threeten/bp/temporal/WeekFields$ComputedDayOfField;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lorg/threeten/bp/temporal/WeekFields$ComputedDayOfField;->b:Lorg/threeten/bp/temporal/WeekFields;

    invoke-virtual {v1}, Lorg/threeten/bp/temporal/WeekFields;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
