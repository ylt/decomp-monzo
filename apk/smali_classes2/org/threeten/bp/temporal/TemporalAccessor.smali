.class public interface abstract Lorg/threeten/bp/temporal/TemporalAccessor;
.super Ljava/lang/Object;
.source "TemporalAccessor.java"


# virtual methods
.method public abstract a(Lorg/threeten/bp/temporal/TemporalQuery;)Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<R:",
            "Ljava/lang/Object;",
            ">(",
            "Lorg/threeten/bp/temporal/TemporalQuery",
            "<TR;>;)TR;"
        }
    .end annotation
.end method

.method public abstract a(Lorg/threeten/bp/temporal/TemporalField;)Z
.end method

.method public abstract b(Lorg/threeten/bp/temporal/TemporalField;)Lorg/threeten/bp/temporal/ValueRange;
.end method

.method public abstract c(Lorg/threeten/bp/temporal/TemporalField;)I
.end method

.method public abstract d(Lorg/threeten/bp/temporal/TemporalField;)J
.end method
