.class abstract enum Lorg/threeten/bp/temporal/IsoFields$Field;
.super Ljava/lang/Enum;
.source "IsoFields.java"

# interfaces
.implements Lorg/threeten/bp/temporal/TemporalField;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/threeten/bp/temporal/IsoFields;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x440a
    name = "Field"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lorg/threeten/bp/temporal/IsoFields$Field;",
        ">;",
        "Lorg/threeten/bp/temporal/TemporalField;"
    }
.end annotation


# static fields
.field public static final enum a:Lorg/threeten/bp/temporal/IsoFields$Field;

.field public static final enum b:Lorg/threeten/bp/temporal/IsoFields$Field;

.field public static final enum c:Lorg/threeten/bp/temporal/IsoFields$Field;

.field public static final enum d:Lorg/threeten/bp/temporal/IsoFields$Field;

.field private static final e:[I

.field private static final synthetic f:[Lorg/threeten/bp/temporal/IsoFields$Field;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 206
    new-instance v0, Lorg/threeten/bp/temporal/IsoFields$Field$1;

    const-string v1, "DAY_OF_QUARTER"

    invoke-direct {v0, v1, v2}, Lorg/threeten/bp/temporal/IsoFields$Field$1;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/threeten/bp/temporal/IsoFields$Field;->a:Lorg/threeten/bp/temporal/IsoFields$Field;

    .line 298
    new-instance v0, Lorg/threeten/bp/temporal/IsoFields$Field$2;

    const-string v1, "QUARTER_OF_YEAR"

    invoke-direct {v0, v1, v3}, Lorg/threeten/bp/temporal/IsoFields$Field$2;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/threeten/bp/temporal/IsoFields$Field;->b:Lorg/threeten/bp/temporal/IsoFields$Field;

    .line 339
    new-instance v0, Lorg/threeten/bp/temporal/IsoFields$Field$3;

    const-string v1, "WEEK_OF_WEEK_BASED_YEAR"

    invoke-direct {v0, v1, v4}, Lorg/threeten/bp/temporal/IsoFields$Field$3;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/threeten/bp/temporal/IsoFields$Field;->c:Lorg/threeten/bp/temporal/IsoFields$Field;

    .line 425
    new-instance v0, Lorg/threeten/bp/temporal/IsoFields$Field$4;

    const-string v1, "WEEK_BASED_YEAR"

    invoke-direct {v0, v1, v5}, Lorg/threeten/bp/temporal/IsoFields$Field$4;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/threeten/bp/temporal/IsoFields$Field;->d:Lorg/threeten/bp/temporal/IsoFields$Field;

    .line 205
    const/4 v0, 0x4

    new-array v0, v0, [Lorg/threeten/bp/temporal/IsoFields$Field;

    sget-object v1, Lorg/threeten/bp/temporal/IsoFields$Field;->a:Lorg/threeten/bp/temporal/IsoFields$Field;

    aput-object v1, v0, v2

    sget-object v1, Lorg/threeten/bp/temporal/IsoFields$Field;->b:Lorg/threeten/bp/temporal/IsoFields$Field;

    aput-object v1, v0, v3

    sget-object v1, Lorg/threeten/bp/temporal/IsoFields$Field;->c:Lorg/threeten/bp/temporal/IsoFields$Field;

    aput-object v1, v0, v4

    sget-object v1, Lorg/threeten/bp/temporal/IsoFields$Field;->d:Lorg/threeten/bp/temporal/IsoFields$Field;

    aput-object v1, v0, v5

    sput-object v0, Lorg/threeten/bp/temporal/IsoFields$Field;->f:[Lorg/threeten/bp/temporal/IsoFields$Field;

    .line 490
    const/16 v0, 0x8

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lorg/threeten/bp/temporal/IsoFields$Field;->e:[I

    return-void

    nop

    :array_0
    .array-data 4
        0x0
        0x5a
        0xb5
        0x111
        0x0
        0x5b
        0xb6
        0x112
    .end array-data
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 205
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;ILorg/threeten/bp/temporal/IsoFields$1;)V
    .locals 0

    .prologue
    .line 205
    invoke-direct {p0, p1, p2}, Lorg/threeten/bp/temporal/IsoFields$Field;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method static synthetic a(I)I
    .locals 1

    .prologue
    .line 205
    invoke-static {p0}, Lorg/threeten/bp/temporal/IsoFields$Field;->b(I)I

    move-result v0

    return v0
.end method

.method static synthetic a(Lorg/threeten/bp/LocalDate;)Lorg/threeten/bp/temporal/ValueRange;
    .locals 1

    .prologue
    .line 205
    invoke-static {p0}, Lorg/threeten/bp/temporal/IsoFields$Field;->d(Lorg/threeten/bp/LocalDate;)Lorg/threeten/bp/temporal/ValueRange;

    move-result-object v0

    return-object v0
.end method

.method private static b(I)I
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 511
    invoke-static {p0, v0, v0}, Lorg/threeten/bp/LocalDate;->a(III)Lorg/threeten/bp/LocalDate;

    move-result-object v0

    .line 513
    invoke-virtual {v0}, Lorg/threeten/bp/LocalDate;->i()Lorg/threeten/bp/DayOfWeek;

    move-result-object v1

    sget-object v2, Lorg/threeten/bp/DayOfWeek;->d:Lorg/threeten/bp/DayOfWeek;

    if-eq v1, v2, :cond_0

    invoke-virtual {v0}, Lorg/threeten/bp/LocalDate;->i()Lorg/threeten/bp/DayOfWeek;

    move-result-object v1

    sget-object v2, Lorg/threeten/bp/DayOfWeek;->c:Lorg/threeten/bp/DayOfWeek;

    if-ne v1, v2, :cond_1

    invoke-virtual {v0}, Lorg/threeten/bp/LocalDate;->j()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 514
    :cond_0
    const/16 v0, 0x35

    .line 516
    :goto_0
    return v0

    :cond_1
    const/16 v0, 0x34

    goto :goto_0
.end method

.method static synthetic b(Lorg/threeten/bp/LocalDate;)I
    .locals 1

    .prologue
    .line 205
    invoke-static {p0}, Lorg/threeten/bp/temporal/IsoFields$Field;->e(Lorg/threeten/bp/LocalDate;)I

    move-result v0

    return v0
.end method

.method static synthetic c(Lorg/threeten/bp/LocalDate;)I
    .locals 1

    .prologue
    .line 205
    invoke-static {p0}, Lorg/threeten/bp/temporal/IsoFields$Field;->f(Lorg/threeten/bp/LocalDate;)I

    move-result v0

    return v0
.end method

.method private static d(Lorg/threeten/bp/LocalDate;)Lorg/threeten/bp/temporal/ValueRange;
    .locals 4

    .prologue
    .line 506
    invoke-static {p0}, Lorg/threeten/bp/temporal/IsoFields$Field;->f(Lorg/threeten/bp/LocalDate;)I

    move-result v0

    .line 507
    const-wide/16 v2, 0x1

    invoke-static {v0}, Lorg/threeten/bp/temporal/IsoFields$Field;->b(I)I

    move-result v0

    int-to-long v0, v0

    invoke-static {v2, v3, v0, v1}, Lorg/threeten/bp/temporal/ValueRange;->a(JJ)Lorg/threeten/bp/temporal/ValueRange;

    move-result-object v0

    return-object v0
.end method

.method static synthetic d(Lorg/threeten/bp/temporal/TemporalAccessor;)Z
    .locals 1

    .prologue
    .line 205
    invoke-static {p0}, Lorg/threeten/bp/temporal/IsoFields$Field;->e(Lorg/threeten/bp/temporal/TemporalAccessor;)Z

    move-result v0

    return v0
.end method

.method static synthetic d()[I
    .locals 1

    .prologue
    .line 205
    sget-object v0, Lorg/threeten/bp/temporal/IsoFields$Field;->e:[I

    return-object v0
.end method

.method private static e(Lorg/threeten/bp/LocalDate;)I
    .locals 5

    .prologue
    const/4 v0, 0x1

    const/4 v4, -0x3

    .line 520
    invoke-virtual {p0}, Lorg/threeten/bp/LocalDate;->i()Lorg/threeten/bp/DayOfWeek;

    move-result-object v1

    invoke-virtual {v1}, Lorg/threeten/bp/DayOfWeek;->ordinal()I

    move-result v1

    .line 521
    invoke-virtual {p0}, Lorg/threeten/bp/LocalDate;->h()I

    move-result v2

    add-int/lit8 v3, v2, -0x1

    .line 522
    rsub-int/lit8 v1, v1, 0x3

    add-int/2addr v1, v3

    .line 523
    div-int/lit8 v2, v1, 0x7

    .line 524
    mul-int/lit8 v2, v2, 0x7

    sub-int/2addr v1, v2

    .line 525
    add-int/lit8 v1, v1, -0x3

    .line 526
    if-ge v1, v4, :cond_5

    .line 527
    add-int/lit8 v1, v1, 0x7

    move v2, v1

    .line 529
    :goto_0
    if-ge v3, v2, :cond_1

    .line 530
    const/16 v0, 0xb4

    invoke-virtual {p0, v0}, Lorg/threeten/bp/LocalDate;->d(I)Lorg/threeten/bp/LocalDate;

    move-result-object v0

    const-wide/16 v2, 0x1

    invoke-virtual {v0, v2, v3}, Lorg/threeten/bp/LocalDate;->f(J)Lorg/threeten/bp/LocalDate;

    move-result-object v0

    invoke-static {v0}, Lorg/threeten/bp/temporal/IsoFields$Field;->d(Lorg/threeten/bp/LocalDate;)Lorg/threeten/bp/temporal/ValueRange;

    move-result-object v0

    invoke-virtual {v0}, Lorg/threeten/bp/temporal/ValueRange;->c()J

    move-result-wide v0

    long-to-int v0, v0

    .line 538
    :cond_0
    :goto_1
    return v0

    .line 532
    :cond_1
    sub-int v1, v3, v2

    div-int/lit8 v1, v1, 0x7

    add-int/lit8 v1, v1, 0x1

    .line 533
    const/16 v3, 0x35

    if-ne v1, v3, :cond_3

    .line 534
    if-eq v2, v4, :cond_2

    const/4 v3, -0x2

    if-ne v2, v3, :cond_4

    invoke-virtual {p0}, Lorg/threeten/bp/LocalDate;->j()Z

    move-result v2

    if-eqz v2, :cond_4

    :cond_2
    move v2, v0

    :goto_2
    if-eqz v2, :cond_0

    :cond_3
    move v0, v1

    goto :goto_1

    :cond_4
    const/4 v2, 0x0

    goto :goto_2

    :cond_5
    move v2, v1

    goto :goto_0
.end method

.method private static e(Lorg/threeten/bp/temporal/TemporalAccessor;)Z
    .locals 2

    .prologue
    .line 502
    invoke-static {p0}, Lorg/threeten/bp/chrono/Chronology;->a(Lorg/threeten/bp/temporal/TemporalAccessor;)Lorg/threeten/bp/chrono/Chronology;

    move-result-object v0

    sget-object v1, Lorg/threeten/bp/chrono/IsoChronology;->b:Lorg/threeten/bp/chrono/IsoChronology;

    invoke-virtual {v0, v1}, Lorg/threeten/bp/chrono/Chronology;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method private static f(Lorg/threeten/bp/LocalDate;)I
    .locals 4

    .prologue
    .line 542
    invoke-virtual {p0}, Lorg/threeten/bp/LocalDate;->d()I

    move-result v0

    .line 543
    invoke-virtual {p0}, Lorg/threeten/bp/LocalDate;->h()I

    move-result v1

    .line 544
    const/4 v2, 0x3

    if-gt v1, v2, :cond_1

    .line 545
    invoke-virtual {p0}, Lorg/threeten/bp/LocalDate;->i()Lorg/threeten/bp/DayOfWeek;

    move-result-object v2

    invoke-virtual {v2}, Lorg/threeten/bp/DayOfWeek;->ordinal()I

    move-result v2

    .line 546
    sub-int/2addr v1, v2

    const/4 v2, -0x2

    if-ge v1, v2, :cond_0

    .line 547
    add-int/lit8 v0, v0, -0x1

    .line 556
    :cond_0
    :goto_0
    return v0

    .line 549
    :cond_1
    const/16 v2, 0x16b

    if-lt v1, v2, :cond_0

    .line 550
    invoke-virtual {p0}, Lorg/threeten/bp/LocalDate;->i()Lorg/threeten/bp/DayOfWeek;

    move-result-object v2

    invoke-virtual {v2}, Lorg/threeten/bp/DayOfWeek;->ordinal()I

    move-result v2

    .line 551
    add-int/lit16 v3, v1, -0x16b

    invoke-virtual {p0}, Lorg/threeten/bp/LocalDate;->j()Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v1, 0x1

    :goto_1
    sub-int v1, v3, v1

    .line 552
    sub-int/2addr v1, v2

    if-ltz v1, :cond_0

    .line 553
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 551
    :cond_2
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public static valueOf(Ljava/lang/String;)Lorg/threeten/bp/temporal/IsoFields$Field;
    .locals 1

    .prologue
    .line 205
    const-class v0, Lorg/threeten/bp/temporal/IsoFields$Field;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lorg/threeten/bp/temporal/IsoFields$Field;

    return-object v0
.end method

.method public static values()[Lorg/threeten/bp/temporal/IsoFields$Field;
    .locals 1

    .prologue
    .line 205
    sget-object v0, Lorg/threeten/bp/temporal/IsoFields$Field;->f:[Lorg/threeten/bp/temporal/IsoFields$Field;

    invoke-virtual {v0}, [Lorg/threeten/bp/temporal/IsoFields$Field;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lorg/threeten/bp/temporal/IsoFields$Field;

    return-object v0
.end method


# virtual methods
.method public a(Ljava/util/Map;Lorg/threeten/bp/temporal/TemporalAccessor;Lorg/threeten/bp/format/ResolverStyle;)Lorg/threeten/bp/temporal/TemporalAccessor;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Lorg/threeten/bp/temporal/TemporalField;",
            "Ljava/lang/Long;",
            ">;",
            "Lorg/threeten/bp/temporal/TemporalAccessor;",
            "Lorg/threeten/bp/format/ResolverStyle;",
            ")",
            "Lorg/threeten/bp/temporal/TemporalAccessor;"
        }
    .end annotation

    .prologue
    .line 486
    const/4 v0, 0x0

    return-object v0
.end method

.method public b()Z
    .locals 1

    .prologue
    .line 494
    const/4 v0, 0x1

    return v0
.end method

.method public c()Z
    .locals 1

    .prologue
    .line 498
    const/4 v0, 0x0

    return v0
.end method
