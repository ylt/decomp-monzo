.class public final Lorg/threeten/bp/temporal/TemporalQueries;
.super Ljava/lang/Object;
.source "TemporalQueries.java"


# static fields
.field static final a:Lorg/threeten/bp/temporal/TemporalQuery;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lorg/threeten/bp/temporal/TemporalQuery",
            "<",
            "Lorg/threeten/bp/ZoneId;",
            ">;"
        }
    .end annotation
.end field

.field static final b:Lorg/threeten/bp/temporal/TemporalQuery;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lorg/threeten/bp/temporal/TemporalQuery",
            "<",
            "Lorg/threeten/bp/chrono/Chronology;",
            ">;"
        }
    .end annotation
.end field

.field static final c:Lorg/threeten/bp/temporal/TemporalQuery;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lorg/threeten/bp/temporal/TemporalQuery",
            "<",
            "Lorg/threeten/bp/temporal/TemporalUnit;",
            ">;"
        }
    .end annotation
.end field

.field static final d:Lorg/threeten/bp/temporal/TemporalQuery;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lorg/threeten/bp/temporal/TemporalQuery",
            "<",
            "Lorg/threeten/bp/ZoneId;",
            ">;"
        }
    .end annotation
.end field

.field static final e:Lorg/threeten/bp/temporal/TemporalQuery;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lorg/threeten/bp/temporal/TemporalQuery",
            "<",
            "Lorg/threeten/bp/ZoneOffset;",
            ">;"
        }
    .end annotation
.end field

.field static final f:Lorg/threeten/bp/temporal/TemporalQuery;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lorg/threeten/bp/temporal/TemporalQuery",
            "<",
            "Lorg/threeten/bp/LocalDate;",
            ">;"
        }
    .end annotation
.end field

.field static final g:Lorg/threeten/bp/temporal/TemporalQuery;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lorg/threeten/bp/temporal/TemporalQuery",
            "<",
            "Lorg/threeten/bp/LocalTime;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 123
    new-instance v0, Lorg/threeten/bp/temporal/TemporalQueries$1;

    invoke-direct {v0}, Lorg/threeten/bp/temporal/TemporalQueries$1;-><init>()V

    sput-object v0, Lorg/threeten/bp/temporal/TemporalQueries;->a:Lorg/threeten/bp/temporal/TemporalQuery;

    .line 168
    new-instance v0, Lorg/threeten/bp/temporal/TemporalQueries$2;

    invoke-direct {v0}, Lorg/threeten/bp/temporal/TemporalQueries$2;-><init>()V

    sput-object v0, Lorg/threeten/bp/temporal/TemporalQueries;->b:Lorg/threeten/bp/temporal/TemporalQuery;

    .line 211
    new-instance v0, Lorg/threeten/bp/temporal/TemporalQueries$3;

    invoke-direct {v0}, Lorg/threeten/bp/temporal/TemporalQueries$3;-><init>()V

    sput-object v0, Lorg/threeten/bp/temporal/TemporalQueries;->c:Lorg/threeten/bp/temporal/TemporalQuery;

    .line 242
    new-instance v0, Lorg/threeten/bp/temporal/TemporalQueries$4;

    invoke-direct {v0}, Lorg/threeten/bp/temporal/TemporalQueries$4;-><init>()V

    sput-object v0, Lorg/threeten/bp/temporal/TemporalQueries;->d:Lorg/threeten/bp/temporal/TemporalQuery;

    .line 265
    new-instance v0, Lorg/threeten/bp/temporal/TemporalQueries$5;

    invoke-direct {v0}, Lorg/threeten/bp/temporal/TemporalQueries$5;-><init>()V

    sput-object v0, Lorg/threeten/bp/temporal/TemporalQueries;->e:Lorg/threeten/bp/temporal/TemporalQuery;

    .line 290
    new-instance v0, Lorg/threeten/bp/temporal/TemporalQueries$6;

    invoke-direct {v0}, Lorg/threeten/bp/temporal/TemporalQueries$6;-><init>()V

    sput-object v0, Lorg/threeten/bp/temporal/TemporalQueries;->f:Lorg/threeten/bp/temporal/TemporalQuery;

    .line 315
    new-instance v0, Lorg/threeten/bp/temporal/TemporalQueries$7;

    invoke-direct {v0}, Lorg/threeten/bp/temporal/TemporalQueries$7;-><init>()V

    sput-object v0, Lorg/threeten/bp/temporal/TemporalQueries;->g:Lorg/threeten/bp/temporal/TemporalQuery;

    return-void
.end method

.method public static final a()Lorg/threeten/bp/temporal/TemporalQuery;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lorg/threeten/bp/temporal/TemporalQuery",
            "<",
            "Lorg/threeten/bp/ZoneId;",
            ">;"
        }
    .end annotation

    .prologue
    .line 121
    sget-object v0, Lorg/threeten/bp/temporal/TemporalQueries;->a:Lorg/threeten/bp/temporal/TemporalQuery;

    return-object v0
.end method

.method public static final b()Lorg/threeten/bp/temporal/TemporalQuery;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lorg/threeten/bp/temporal/TemporalQuery",
            "<",
            "Lorg/threeten/bp/chrono/Chronology;",
            ">;"
        }
    .end annotation

    .prologue
    .line 166
    sget-object v0, Lorg/threeten/bp/temporal/TemporalQueries;->b:Lorg/threeten/bp/temporal/TemporalQuery;

    return-object v0
.end method

.method public static final c()Lorg/threeten/bp/temporal/TemporalQuery;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lorg/threeten/bp/temporal/TemporalQuery",
            "<",
            "Lorg/threeten/bp/temporal/TemporalUnit;",
            ">;"
        }
    .end annotation

    .prologue
    .line 209
    sget-object v0, Lorg/threeten/bp/temporal/TemporalQueries;->c:Lorg/threeten/bp/temporal/TemporalQuery;

    return-object v0
.end method

.method public static final d()Lorg/threeten/bp/temporal/TemporalQuery;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lorg/threeten/bp/temporal/TemporalQuery",
            "<",
            "Lorg/threeten/bp/ZoneId;",
            ">;"
        }
    .end annotation

    .prologue
    .line 240
    sget-object v0, Lorg/threeten/bp/temporal/TemporalQueries;->d:Lorg/threeten/bp/temporal/TemporalQuery;

    return-object v0
.end method

.method public static final e()Lorg/threeten/bp/temporal/TemporalQuery;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lorg/threeten/bp/temporal/TemporalQuery",
            "<",
            "Lorg/threeten/bp/ZoneOffset;",
            ">;"
        }
    .end annotation

    .prologue
    .line 263
    sget-object v0, Lorg/threeten/bp/temporal/TemporalQueries;->e:Lorg/threeten/bp/temporal/TemporalQuery;

    return-object v0
.end method

.method public static final f()Lorg/threeten/bp/temporal/TemporalQuery;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lorg/threeten/bp/temporal/TemporalQuery",
            "<",
            "Lorg/threeten/bp/LocalDate;",
            ">;"
        }
    .end annotation

    .prologue
    .line 288
    sget-object v0, Lorg/threeten/bp/temporal/TemporalQueries;->f:Lorg/threeten/bp/temporal/TemporalQuery;

    return-object v0
.end method

.method public static final g()Lorg/threeten/bp/temporal/TemporalQuery;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lorg/threeten/bp/temporal/TemporalQuery",
            "<",
            "Lorg/threeten/bp/LocalTime;",
            ">;"
        }
    .end annotation

    .prologue
    .line 313
    sget-object v0, Lorg/threeten/bp/temporal/TemporalQueries;->g:Lorg/threeten/bp/temporal/TemporalQuery;

    return-object v0
.end method
