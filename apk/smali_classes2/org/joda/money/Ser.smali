.class final Lorg/joda/money/Ser;
.super Ljava/lang/Object;
.source "Ser.java"

# interfaces
.implements Ljava/io/Externalizable;


# static fields
.field static final BIG_MONEY:B = 0x42t

.field static final CURRENCY_UNIT:B = 0x43t

.field static final MONEY:B = 0x4dt


# instance fields
.field private object:Ljava/lang/Object;

.field private type:B


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 51
    return-void
.end method

.method constructor <init>(BLjava/lang/Object;)V
    .locals 0

    .prologue
    .line 59
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 60
    iput-byte p1, p0, Lorg/joda/money/Ser;->type:B

    .line 61
    iput-object p2, p0, Lorg/joda/money/Ser;->object:Ljava/lang/Object;

    .line 62
    return-void
.end method

.method private readBigMoney(Ljava/io/ObjectInput;)Lorg/joda/money/BigMoney;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 136
    invoke-direct {p0, p1}, Lorg/joda/money/Ser;->readCurrency(Ljava/io/ObjectInput;)Lorg/joda/money/CurrencyUnit;

    move-result-object v0

    .line 137
    invoke-interface {p1}, Ljava/io/ObjectInput;->readInt()I

    move-result v1

    new-array v1, v1, [B

    .line 138
    invoke-interface {p1, v1}, Ljava/io/ObjectInput;->readFully([B)V

    .line 139
    new-instance v2, Ljava/math/BigDecimal;

    new-instance v3, Ljava/math/BigInteger;

    invoke-direct {v3, v1}, Ljava/math/BigInteger;-><init>([B)V

    invoke-interface {p1}, Ljava/io/ObjectInput;->readInt()I

    move-result v1

    invoke-direct {v2, v3, v1}, Ljava/math/BigDecimal;-><init>(Ljava/math/BigInteger;I)V

    .line 140
    new-instance v1, Lorg/joda/money/BigMoney;

    invoke-direct {v1, v0, v2}, Lorg/joda/money/BigMoney;-><init>(Lorg/joda/money/CurrencyUnit;Ljava/math/BigDecimal;)V

    .line 141
    return-object v1
.end method

.method private readCurrency(Ljava/io/ObjectInput;)Lorg/joda/money/CurrencyUnit;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 145
    invoke-interface {p1}, Ljava/io/ObjectInput;->readUTF()Ljava/lang/String;

    move-result-object v0

    .line 146
    invoke-static {v0}, Lorg/joda/money/CurrencyUnit;->of(Ljava/lang/String;)Lorg/joda/money/CurrencyUnit;

    move-result-object v1

    .line 147
    invoke-virtual {v1}, Lorg/joda/money/CurrencyUnit;->getNumericCode()I

    move-result v2

    invoke-interface {p1}, Ljava/io/ObjectInput;->readShort()S

    move-result v3

    if-eq v2, v3, :cond_0

    .line 148
    new-instance v1, Ljava/io/InvalidObjectException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Deserialization found a mismatch in the numeric code for currency "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/io/InvalidObjectException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 150
    :cond_0
    invoke-virtual {v1}, Lorg/joda/money/CurrencyUnit;->getDefaultFractionDigits()I

    move-result v2

    invoke-interface {p1}, Ljava/io/ObjectInput;->readShort()S

    move-result v3

    if-eq v2, v3, :cond_1

    .line 151
    new-instance v1, Ljava/io/InvalidObjectException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Deserialization found a mismatch in the decimal places for currency "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/io/InvalidObjectException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 153
    :cond_1
    return-object v1
.end method

.method private readResolve()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 162
    iget-object v0, p0, Lorg/joda/money/Ser;->object:Ljava/lang/Object;

    return-object v0
.end method

.method private writeBigMoney(Ljava/io/ObjectOutput;Lorg/joda/money/BigMoney;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 96
    invoke-virtual {p2}, Lorg/joda/money/BigMoney;->getCurrencyUnit()Lorg/joda/money/CurrencyUnit;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lorg/joda/money/Ser;->writeCurrency(Ljava/io/ObjectOutput;Lorg/joda/money/CurrencyUnit;)V

    .line 97
    invoke-virtual {p2}, Lorg/joda/money/BigMoney;->getAmount()Ljava/math/BigDecimal;

    move-result-object v0

    invoke-virtual {v0}, Ljava/math/BigDecimal;->unscaledValue()Ljava/math/BigInteger;

    move-result-object v0

    invoke-virtual {v0}, Ljava/math/BigInteger;->toByteArray()[B

    move-result-object v0

    .line 98
    array-length v1, v0

    invoke-interface {p1, v1}, Ljava/io/ObjectOutput;->writeInt(I)V

    .line 99
    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->write([B)V

    .line 100
    invoke-virtual {p2}, Lorg/joda/money/BigMoney;->getScale()I

    move-result v0

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeInt(I)V

    .line 101
    return-void
.end method

.method private writeCurrency(Ljava/io/ObjectOutput;Lorg/joda/money/CurrencyUnit;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 104
    invoke-virtual {p2}, Lorg/joda/money/CurrencyUnit;->getCode()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeUTF(Ljava/lang/String;)V

    .line 105
    invoke-virtual {p2}, Lorg/joda/money/CurrencyUnit;->getNumericCode()I

    move-result v0

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeShort(I)V

    .line 106
    invoke-virtual {p2}, Lorg/joda/money/CurrencyUnit;->getDefaultFractionDigits()I

    move-result v0

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeShort(I)V

    .line 107
    return-void
.end method


# virtual methods
.method public readExternal(Ljava/io/ObjectInput;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 117
    invoke-interface {p1}, Ljava/io/ObjectInput;->readByte()B

    move-result v0

    iput-byte v0, p0, Lorg/joda/money/Ser;->type:B

    .line 118
    iget-byte v0, p0, Lorg/joda/money/Ser;->type:B

    sparse-switch v0, :sswitch_data_0

    .line 132
    new-instance v0, Ljava/io/StreamCorruptedException;

    const-string v1, "Serialization input has invalid type"

    invoke-direct {v0, v1}, Ljava/io/StreamCorruptedException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 120
    :sswitch_0
    invoke-direct {p0, p1}, Lorg/joda/money/Ser;->readBigMoney(Ljava/io/ObjectInput;)Lorg/joda/money/BigMoney;

    move-result-object v0

    iput-object v0, p0, Lorg/joda/money/Ser;->object:Ljava/lang/Object;

    .line 129
    :goto_0
    return-void

    .line 124
    :sswitch_1
    new-instance v0, Lorg/joda/money/Money;

    invoke-direct {p0, p1}, Lorg/joda/money/Ser;->readBigMoney(Ljava/io/ObjectInput;)Lorg/joda/money/BigMoney;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/joda/money/Money;-><init>(Lorg/joda/money/BigMoney;)V

    iput-object v0, p0, Lorg/joda/money/Ser;->object:Ljava/lang/Object;

    goto :goto_0

    .line 128
    :sswitch_2
    invoke-direct {p0, p1}, Lorg/joda/money/Ser;->readCurrency(Ljava/io/ObjectInput;)Lorg/joda/money/CurrencyUnit;

    move-result-object v0

    iput-object v0, p0, Lorg/joda/money/Ser;->object:Ljava/lang/Object;

    goto :goto_0

    .line 118
    nop

    :sswitch_data_0
    .sparse-switch
        0x42 -> :sswitch_0
        0x43 -> :sswitch_2
        0x4d -> :sswitch_1
    .end sparse-switch
.end method

.method public writeExternal(Ljava/io/ObjectOutput;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 74
    iget-byte v0, p0, Lorg/joda/money/Ser;->type:B

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeByte(I)V

    .line 75
    iget-byte v0, p0, Lorg/joda/money/Ser;->type:B

    sparse-switch v0, :sswitch_data_0

    .line 92
    new-instance v0, Ljava/io/InvalidClassException;

    const-string v1, "Joda-Money bug: Serialization broken"

    invoke-direct {v0, v1}, Ljava/io/InvalidClassException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 77
    :sswitch_0
    iget-object v0, p0, Lorg/joda/money/Ser;->object:Ljava/lang/Object;

    check-cast v0, Lorg/joda/money/BigMoney;

    .line 78
    invoke-direct {p0, p1, v0}, Lorg/joda/money/Ser;->writeBigMoney(Ljava/io/ObjectOutput;Lorg/joda/money/BigMoney;)V

    .line 89
    :goto_0
    return-void

    .line 82
    :sswitch_1
    iget-object v0, p0, Lorg/joda/money/Ser;->object:Ljava/lang/Object;

    check-cast v0, Lorg/joda/money/Money;

    .line 83
    invoke-virtual {v0}, Lorg/joda/money/Money;->toBigMoney()Lorg/joda/money/BigMoney;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lorg/joda/money/Ser;->writeBigMoney(Ljava/io/ObjectOutput;Lorg/joda/money/BigMoney;)V

    goto :goto_0

    .line 87
    :sswitch_2
    iget-object v0, p0, Lorg/joda/money/Ser;->object:Ljava/lang/Object;

    check-cast v0, Lorg/joda/money/CurrencyUnit;

    .line 88
    invoke-direct {p0, p1, v0}, Lorg/joda/money/Ser;->writeCurrency(Ljava/io/ObjectOutput;Lorg/joda/money/CurrencyUnit;)V

    goto :goto_0

    .line 75
    :sswitch_data_0
    .sparse-switch
        0x42 -> :sswitch_0
        0x43 -> :sswitch_2
        0x4d -> :sswitch_1
    .end sparse-switch
.end method
