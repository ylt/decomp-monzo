.class public final Lorg/joda/money/format/MoneyParseContext;
.super Ljava/lang/Object;
.source "MoneyParseContext.java"


# instance fields
.field private amount:Ljava/math/BigDecimal;

.field private currency:Lorg/joda/money/CurrencyUnit;

.field private locale:Ljava/util/Locale;

.field private text:Ljava/lang/CharSequence;

.field private textErrorIndex:I

.field private textIndex:I


# direct methods
.method constructor <init>(Ljava/util/Locale;Ljava/lang/CharSequence;I)V
    .locals 1

    .prologue
    .line 65
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    const/4 v0, -0x1

    iput v0, p0, Lorg/joda/money/format/MoneyParseContext;->textErrorIndex:I

    .line 66
    iput-object p1, p0, Lorg/joda/money/format/MoneyParseContext;->locale:Ljava/util/Locale;

    .line 67
    iput-object p2, p0, Lorg/joda/money/format/MoneyParseContext;->text:Ljava/lang/CharSequence;

    .line 68
    iput p3, p0, Lorg/joda/money/format/MoneyParseContext;->textIndex:I

    .line 69
    return-void
.end method

.method constructor <init>(Ljava/util/Locale;Ljava/lang/CharSequence;IILorg/joda/money/CurrencyUnit;Ljava/math/BigDecimal;)V
    .locals 1

    .prologue
    .line 81
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    const/4 v0, -0x1

    iput v0, p0, Lorg/joda/money/format/MoneyParseContext;->textErrorIndex:I

    .line 82
    iput-object p1, p0, Lorg/joda/money/format/MoneyParseContext;->locale:Ljava/util/Locale;

    .line 83
    iput-object p2, p0, Lorg/joda/money/format/MoneyParseContext;->text:Ljava/lang/CharSequence;

    .line 84
    iput p3, p0, Lorg/joda/money/format/MoneyParseContext;->textIndex:I

    .line 85
    iput p4, p0, Lorg/joda/money/format/MoneyParseContext;->textErrorIndex:I

    .line 86
    iput-object p5, p0, Lorg/joda/money/format/MoneyParseContext;->currency:Lorg/joda/money/CurrencyUnit;

    .line 87
    iput-object p6, p0, Lorg/joda/money/format/MoneyParseContext;->amount:Ljava/math/BigDecimal;

    .line 88
    return-void
.end method


# virtual methods
.method createChild()Lorg/joda/money/format/MoneyParseContext;
    .locals 7

    .prologue
    .line 268
    new-instance v0, Lorg/joda/money/format/MoneyParseContext;

    iget-object v1, p0, Lorg/joda/money/format/MoneyParseContext;->locale:Ljava/util/Locale;

    iget-object v2, p0, Lorg/joda/money/format/MoneyParseContext;->text:Ljava/lang/CharSequence;

    iget v3, p0, Lorg/joda/money/format/MoneyParseContext;->textIndex:I

    iget v4, p0, Lorg/joda/money/format/MoneyParseContext;->textErrorIndex:I

    iget-object v5, p0, Lorg/joda/money/format/MoneyParseContext;->currency:Lorg/joda/money/CurrencyUnit;

    iget-object v6, p0, Lorg/joda/money/format/MoneyParseContext;->amount:Ljava/math/BigDecimal;

    invoke-direct/range {v0 .. v6}, Lorg/joda/money/format/MoneyParseContext;-><init>(Ljava/util/Locale;Ljava/lang/CharSequence;IILorg/joda/money/CurrencyUnit;Ljava/math/BigDecimal;)V

    return-object v0
.end method

.method public getAmount()Ljava/math/BigDecimal;
    .locals 1

    .prologue
    .line 220
    iget-object v0, p0, Lorg/joda/money/format/MoneyParseContext;->amount:Ljava/math/BigDecimal;

    return-object v0
.end method

.method public getCurrency()Lorg/joda/money/CurrencyUnit;
    .locals 1

    .prologue
    .line 201
    iget-object v0, p0, Lorg/joda/money/format/MoneyParseContext;->currency:Lorg/joda/money/CurrencyUnit;

    return-object v0
.end method

.method public getErrorIndex()I
    .locals 1

    .prologue
    .line 175
    iget v0, p0, Lorg/joda/money/format/MoneyParseContext;->textErrorIndex:I

    return v0
.end method

.method public getIndex()I
    .locals 1

    .prologue
    .line 156
    iget v0, p0, Lorg/joda/money/format/MoneyParseContext;->textIndex:I

    return v0
.end method

.method public getLocale()Ljava/util/Locale;
    .locals 1

    .prologue
    .line 97
    iget-object v0, p0, Lorg/joda/money/format/MoneyParseContext;->locale:Ljava/util/Locale;

    return-object v0
.end method

.method public getText()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 116
    iget-object v0, p0, Lorg/joda/money/format/MoneyParseContext;->text:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public getTextLength()I
    .locals 1

    .prologue
    .line 135
    iget-object v0, p0, Lorg/joda/money/format/MoneyParseContext;->text:Ljava/lang/CharSequence;

    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v0

    return v0
.end method

.method public getTextSubstring(II)Ljava/lang/String;
    .locals 1

    .prologue
    .line 146
    iget-object v0, p0, Lorg/joda/money/format/MoneyParseContext;->text:Ljava/lang/CharSequence;

    invoke-interface {v0, p1, p2}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public isComplete()Z
    .locals 1

    .prologue
    .line 258
    iget-object v0, p0, Lorg/joda/money/format/MoneyParseContext;->currency:Lorg/joda/money/CurrencyUnit;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/joda/money/format/MoneyParseContext;->amount:Ljava/math/BigDecimal;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isError()Z
    .locals 1

    .prologue
    .line 239
    iget v0, p0, Lorg/joda/money/format/MoneyParseContext;->textErrorIndex:I

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isFullyParsed()Z
    .locals 2

    .prologue
    .line 248
    iget v0, p0, Lorg/joda/money/format/MoneyParseContext;->textIndex:I

    invoke-virtual {p0}, Lorg/joda/money/format/MoneyParseContext;->getTextLength()I

    move-result v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method mergeChild(Lorg/joda/money/format/MoneyParseContext;)V
    .locals 1

    .prologue
    .line 277
    invoke-virtual {p1}, Lorg/joda/money/format/MoneyParseContext;->getLocale()Ljava/util/Locale;

    move-result-object v0

    invoke-virtual {p0, v0}, Lorg/joda/money/format/MoneyParseContext;->setLocale(Ljava/util/Locale;)V

    .line 278
    invoke-virtual {p1}, Lorg/joda/money/format/MoneyParseContext;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p0, v0}, Lorg/joda/money/format/MoneyParseContext;->setText(Ljava/lang/CharSequence;)V

    .line 279
    invoke-virtual {p1}, Lorg/joda/money/format/MoneyParseContext;->getIndex()I

    move-result v0

    invoke-virtual {p0, v0}, Lorg/joda/money/format/MoneyParseContext;->setIndex(I)V

    .line 280
    invoke-virtual {p1}, Lorg/joda/money/format/MoneyParseContext;->getErrorIndex()I

    move-result v0

    invoke-virtual {p0, v0}, Lorg/joda/money/format/MoneyParseContext;->setErrorIndex(I)V

    .line 281
    invoke-virtual {p1}, Lorg/joda/money/format/MoneyParseContext;->getCurrency()Lorg/joda/money/CurrencyUnit;

    move-result-object v0

    invoke-virtual {p0, v0}, Lorg/joda/money/format/MoneyParseContext;->setCurrency(Lorg/joda/money/CurrencyUnit;)V

    .line 282
    invoke-virtual {p1}, Lorg/joda/money/format/MoneyParseContext;->getAmount()Ljava/math/BigDecimal;

    move-result-object v0

    invoke-virtual {p0, v0}, Lorg/joda/money/format/MoneyParseContext;->setAmount(Ljava/math/BigDecimal;)V

    .line 283
    return-void
.end method

.method public setAmount(Ljava/math/BigDecimal;)V
    .locals 0

    .prologue
    .line 229
    iput-object p1, p0, Lorg/joda/money/format/MoneyParseContext;->amount:Ljava/math/BigDecimal;

    .line 230
    return-void
.end method

.method public setCurrency(Lorg/joda/money/CurrencyUnit;)V
    .locals 0

    .prologue
    .line 210
    iput-object p1, p0, Lorg/joda/money/format/MoneyParseContext;->currency:Lorg/joda/money/CurrencyUnit;

    .line 211
    return-void
.end method

.method public setError()V
    .locals 1

    .prologue
    .line 191
    iget v0, p0, Lorg/joda/money/format/MoneyParseContext;->textIndex:I

    iput v0, p0, Lorg/joda/money/format/MoneyParseContext;->textErrorIndex:I

    .line 192
    return-void
.end method

.method public setErrorIndex(I)V
    .locals 0

    .prologue
    .line 184
    iput p1, p0, Lorg/joda/money/format/MoneyParseContext;->textErrorIndex:I

    .line 185
    return-void
.end method

.method public setIndex(I)V
    .locals 0

    .prologue
    .line 165
    iput p1, p0, Lorg/joda/money/format/MoneyParseContext;->textIndex:I

    .line 166
    return-void
.end method

.method public setLocale(Ljava/util/Locale;)V
    .locals 1

    .prologue
    .line 106
    const-string v0, "Locale must not be null"

    invoke-static {p1, v0}, Lorg/joda/money/format/MoneyFormatter;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 107
    iput-object p1, p0, Lorg/joda/money/format/MoneyParseContext;->locale:Ljava/util/Locale;

    .line 108
    return-void
.end method

.method public setText(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 125
    const-string v0, "Text must not be null"

    invoke-static {p1, v0}, Lorg/joda/money/format/MoneyFormatter;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 126
    iput-object p1, p0, Lorg/joda/money/format/MoneyParseContext;->text:Ljava/lang/CharSequence;

    .line 127
    return-void
.end method

.method public toBigMoney()Lorg/joda/money/BigMoney;
    .locals 2

    .prologue
    .line 304
    iget-object v0, p0, Lorg/joda/money/format/MoneyParseContext;->currency:Lorg/joda/money/CurrencyUnit;

    if-nez v0, :cond_0

    .line 305
    new-instance v0, Lorg/joda/money/format/MoneyFormatException;

    const-string v1, "Cannot convert to BigMoney as no currency found"

    invoke-direct {v0, v1}, Lorg/joda/money/format/MoneyFormatException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 307
    :cond_0
    iget-object v0, p0, Lorg/joda/money/format/MoneyParseContext;->amount:Ljava/math/BigDecimal;

    if-nez v0, :cond_1

    .line 308
    new-instance v0, Lorg/joda/money/format/MoneyFormatException;

    const-string v1, "Cannot convert to BigMoney as no amount found"

    invoke-direct {v0, v1}, Lorg/joda/money/format/MoneyFormatException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 310
    :cond_1
    iget-object v0, p0, Lorg/joda/money/format/MoneyParseContext;->currency:Lorg/joda/money/CurrencyUnit;

    iget-object v1, p0, Lorg/joda/money/format/MoneyParseContext;->amount:Ljava/math/BigDecimal;

    invoke-static {v0, v1}, Lorg/joda/money/BigMoney;->of(Lorg/joda/money/CurrencyUnit;Ljava/math/BigDecimal;)Lorg/joda/money/BigMoney;

    move-result-object v0

    return-object v0
.end method

.method public toParsePosition()Ljava/text/ParsePosition;
    .locals 2

    .prologue
    .line 292
    new-instance v0, Ljava/text/ParsePosition;

    iget v1, p0, Lorg/joda/money/format/MoneyParseContext;->textIndex:I

    invoke-direct {v0, v1}, Ljava/text/ParsePosition;-><init>(I)V

    .line 293
    iget v1, p0, Lorg/joda/money/format/MoneyParseContext;->textErrorIndex:I

    invoke-virtual {v0, v1}, Ljava/text/ParsePosition;->setErrorIndex(I)V

    .line 294
    return-object v0
.end method
