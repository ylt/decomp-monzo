.class final enum Lorg/joda/money/format/MoneyFormatterBuilder$Singletons$1;
.super Lorg/joda/money/format/MoneyFormatterBuilder$Singletons;
.source "MoneyFormatterBuilder.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/joda/money/format/MoneyFormatterBuilder$Singletons;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4008
    name = null
.end annotation


# direct methods
.method constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 1

    .prologue
    .line 312
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, Lorg/joda/money/format/MoneyFormatterBuilder$Singletons;-><init>(Ljava/lang/String;ILjava/lang/String;Lorg/joda/money/format/MoneyFormatterBuilder$1;)V

    return-void
.end method


# virtual methods
.method public parse(Lorg/joda/money/format/MoneyParseContext;)V
    .locals 2

    .prologue
    .line 319
    invoke-virtual {p1}, Lorg/joda/money/format/MoneyParseContext;->getIndex()I

    move-result v0

    add-int/lit8 v0, v0, 0x3

    .line 320
    invoke-virtual {p1}, Lorg/joda/money/format/MoneyParseContext;->getTextLength()I

    move-result v1

    if-le v0, v1, :cond_0

    .line 321
    invoke-virtual {p1}, Lorg/joda/money/format/MoneyParseContext;->setError()V

    .line 331
    :goto_0
    return-void

    .line 323
    :cond_0
    invoke-virtual {p1}, Lorg/joda/money/format/MoneyParseContext;->getIndex()I

    move-result v1

    invoke-virtual {p1, v1, v0}, Lorg/joda/money/format/MoneyParseContext;->getTextSubstring(II)Ljava/lang/String;

    move-result-object v1

    .line 325
    :try_start_0
    invoke-static {v1}, Lorg/joda/money/CurrencyUnit;->of(Ljava/lang/String;)Lorg/joda/money/CurrencyUnit;

    move-result-object v1

    invoke-virtual {p1, v1}, Lorg/joda/money/format/MoneyParseContext;->setCurrency(Lorg/joda/money/CurrencyUnit;)V

    .line 326
    invoke-virtual {p1, v0}, Lorg/joda/money/format/MoneyParseContext;->setIndex(I)V
    :try_end_0
    .catch Lorg/joda/money/IllegalCurrencyException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 327
    :catch_0
    move-exception v0

    .line 328
    invoke-virtual {p1}, Lorg/joda/money/format/MoneyParseContext;->setError()V

    goto :goto_0
.end method

.method public print(Lorg/joda/money/format/MoneyPrintContext;Ljava/lang/Appendable;Lorg/joda/money/BigMoney;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 315
    invoke-virtual {p3}, Lorg/joda/money/BigMoney;->getCurrencyUnit()Lorg/joda/money/CurrencyUnit;

    move-result-object v0

    invoke-virtual {v0}, Lorg/joda/money/CurrencyUnit;->getCode()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p2, v0}, Ljava/lang/Appendable;->append(Ljava/lang/CharSequence;)Ljava/lang/Appendable;

    .line 316
    return-void
.end method
