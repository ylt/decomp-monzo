.class final Lorg/joda/money/format/SignedPrinterParser;
.super Ljava/lang/Object;
.source "SignedPrinterParser.java"

# interfaces
.implements Ljava/io/Serializable;
.implements Lorg/joda/money/format/MoneyParser;
.implements Lorg/joda/money/format/MoneyPrinter;


# static fields
.field private static final serialVersionUID:J = 0x1L


# instance fields
.field private final whenNegative:Lorg/joda/money/format/MoneyFormatter;

.field private final whenPositive:Lorg/joda/money/format/MoneyFormatter;

.field private final whenZero:Lorg/joda/money/format/MoneyFormatter;


# direct methods
.method constructor <init>(Lorg/joda/money/format/MoneyFormatter;Lorg/joda/money/format/MoneyFormatter;Lorg/joda/money/format/MoneyFormatter;)V
    .locals 0

    .prologue
    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    iput-object p1, p0, Lorg/joda/money/format/SignedPrinterParser;->whenPositive:Lorg/joda/money/format/MoneyFormatter;

    .line 49
    iput-object p2, p0, Lorg/joda/money/format/SignedPrinterParser;->whenZero:Lorg/joda/money/format/MoneyFormatter;

    .line 50
    iput-object p3, p0, Lorg/joda/money/format/SignedPrinterParser;->whenNegative:Lorg/joda/money/format/MoneyFormatter;

    .line 51
    return-void
.end method


# virtual methods
.method public parse(Lorg/joda/money/format/MoneyParseContext;)V
    .locals 5

    .prologue
    .line 62
    invoke-virtual {p1}, Lorg/joda/money/format/MoneyParseContext;->createChild()Lorg/joda/money/format/MoneyParseContext;

    move-result-object v0

    .line 63
    iget-object v1, p0, Lorg/joda/money/format/SignedPrinterParser;->whenPositive:Lorg/joda/money/format/MoneyFormatter;

    invoke-virtual {v1}, Lorg/joda/money/format/MoneyFormatter;->getPrinterParser()Lorg/joda/money/format/MultiPrinterParser;

    move-result-object v1

    invoke-virtual {v1, v0}, Lorg/joda/money/format/MultiPrinterParser;->parse(Lorg/joda/money/format/MoneyParseContext;)V

    .line 64
    invoke-virtual {p1}, Lorg/joda/money/format/MoneyParseContext;->createChild()Lorg/joda/money/format/MoneyParseContext;

    move-result-object v2

    .line 65
    iget-object v1, p0, Lorg/joda/money/format/SignedPrinterParser;->whenZero:Lorg/joda/money/format/MoneyFormatter;

    invoke-virtual {v1}, Lorg/joda/money/format/MoneyFormatter;->getPrinterParser()Lorg/joda/money/format/MultiPrinterParser;

    move-result-object v1

    invoke-virtual {v1, v2}, Lorg/joda/money/format/MultiPrinterParser;->parse(Lorg/joda/money/format/MoneyParseContext;)V

    .line 66
    invoke-virtual {p1}, Lorg/joda/money/format/MoneyParseContext;->createChild()Lorg/joda/money/format/MoneyParseContext;

    move-result-object v3

    .line 67
    iget-object v1, p0, Lorg/joda/money/format/SignedPrinterParser;->whenNegative:Lorg/joda/money/format/MoneyFormatter;

    invoke-virtual {v1}, Lorg/joda/money/format/MoneyFormatter;->getPrinterParser()Lorg/joda/money/format/MultiPrinterParser;

    move-result-object v1

    invoke-virtual {v1, v3}, Lorg/joda/money/format/MultiPrinterParser;->parse(Lorg/joda/money/format/MoneyParseContext;)V

    .line 68
    const/4 v1, 0x0

    .line 69
    invoke-virtual {v0}, Lorg/joda/money/format/MoneyParseContext;->isError()Z

    move-result v4

    if-nez v4, :cond_8

    .line 72
    :goto_0
    invoke-virtual {v2}, Lorg/joda/money/format/MoneyParseContext;->isError()Z

    move-result v1

    if-nez v1, :cond_1

    .line 73
    if-eqz v0, :cond_0

    invoke-virtual {v2}, Lorg/joda/money/format/MoneyParseContext;->getIndex()I

    move-result v1

    invoke-virtual {v0}, Lorg/joda/money/format/MoneyParseContext;->getIndex()I

    move-result v4

    if-le v1, v4, :cond_1

    :cond_0
    move-object v0, v2

    .line 77
    :cond_1
    invoke-virtual {v3}, Lorg/joda/money/format/MoneyParseContext;->isError()Z

    move-result v1

    if-nez v1, :cond_3

    .line 78
    if-eqz v0, :cond_2

    invoke-virtual {v3}, Lorg/joda/money/format/MoneyParseContext;->getIndex()I

    move-result v1

    invoke-virtual {v0}, Lorg/joda/money/format/MoneyParseContext;->getIndex()I

    move-result v4

    if-le v1, v4, :cond_3

    :cond_2
    move-object v0, v3

    .line 82
    :cond_3
    if-nez v0, :cond_5

    .line 83
    invoke-virtual {p1}, Lorg/joda/money/format/MoneyParseContext;->setError()V

    .line 94
    :cond_4
    :goto_1
    return-void

    .line 85
    :cond_5
    invoke-virtual {p1, v0}, Lorg/joda/money/format/MoneyParseContext;->mergeChild(Lorg/joda/money/format/MoneyParseContext;)V

    .line 86
    if-ne v0, v2, :cond_7

    .line 87
    invoke-virtual {p1}, Lorg/joda/money/format/MoneyParseContext;->getAmount()Ljava/math/BigDecimal;

    move-result-object v0

    if-eqz v0, :cond_6

    invoke-virtual {p1}, Lorg/joda/money/format/MoneyParseContext;->getAmount()Ljava/math/BigDecimal;

    move-result-object v0

    sget-object v1, Ljava/math/BigDecimal;->ZERO:Ljava/math/BigDecimal;

    invoke-virtual {v0, v1}, Ljava/math/BigDecimal;->compareTo(Ljava/math/BigDecimal;)I

    move-result v0

    if-eqz v0, :cond_4

    .line 88
    :cond_6
    sget-object v0, Ljava/math/BigDecimal;->ZERO:Ljava/math/BigDecimal;

    invoke-virtual {p1, v0}, Lorg/joda/money/format/MoneyParseContext;->setAmount(Ljava/math/BigDecimal;)V

    goto :goto_1

    .line 90
    :cond_7
    if-ne v0, v3, :cond_4

    invoke-virtual {p1}, Lorg/joda/money/format/MoneyParseContext;->getAmount()Ljava/math/BigDecimal;

    move-result-object v0

    sget-object v1, Ljava/math/BigDecimal;->ZERO:Ljava/math/BigDecimal;

    invoke-virtual {v0, v1}, Ljava/math/BigDecimal;->compareTo(Ljava/math/BigDecimal;)I

    move-result v0

    if-lez v0, :cond_4

    .line 91
    invoke-virtual {p1}, Lorg/joda/money/format/MoneyParseContext;->getAmount()Ljava/math/BigDecimal;

    move-result-object v0

    invoke-virtual {v0}, Ljava/math/BigDecimal;->negate()Ljava/math/BigDecimal;

    move-result-object v0

    invoke-virtual {p1, v0}, Lorg/joda/money/format/MoneyParseContext;->setAmount(Ljava/math/BigDecimal;)V

    goto :goto_1

    :cond_8
    move-object v0, v1

    goto :goto_0
.end method

.method public print(Lorg/joda/money/format/MoneyPrintContext;Ljava/lang/Appendable;Lorg/joda/money/BigMoney;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 56
    invoke-virtual {p3}, Lorg/joda/money/BigMoney;->isZero()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/joda/money/format/SignedPrinterParser;->whenZero:Lorg/joda/money/format/MoneyFormatter;

    .line 57
    :goto_0
    invoke-virtual {v0}, Lorg/joda/money/format/MoneyFormatter;->getPrinterParser()Lorg/joda/money/format/MultiPrinterParser;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3}, Lorg/joda/money/format/MultiPrinterParser;->print(Lorg/joda/money/format/MoneyPrintContext;Ljava/lang/Appendable;Lorg/joda/money/BigMoney;)V

    .line 58
    return-void

    .line 56
    :cond_0
    invoke-virtual {p3}, Lorg/joda/money/BigMoney;->isPositive()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lorg/joda/money/format/SignedPrinterParser;->whenPositive:Lorg/joda/money/format/MoneyFormatter;

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lorg/joda/money/format/SignedPrinterParser;->whenNegative:Lorg/joda/money/format/MoneyFormatter;

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 98
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "PositiveZeroNegative("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lorg/joda/money/format/SignedPrinterParser;->whenPositive:Lorg/joda/money/format/MoneyFormatter;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lorg/joda/money/format/SignedPrinterParser;->whenZero:Lorg/joda/money/format/MoneyFormatter;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lorg/joda/money/format/SignedPrinterParser;->whenNegative:Lorg/joda/money/format/MoneyFormatter;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
