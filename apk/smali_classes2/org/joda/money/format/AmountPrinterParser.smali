.class final Lorg/joda/money/format/AmountPrinterParser;
.super Ljava/lang/Object;
.source "AmountPrinterParser.java"

# interfaces
.implements Ljava/io/Serializable;
.implements Lorg/joda/money/format/MoneyParser;
.implements Lorg/joda/money/format/MoneyPrinter;


# static fields
.field private static final serialVersionUID:J = 0x1L


# instance fields
.field private final style:Lorg/joda/money/format/MoneyAmountStyle;


# direct methods
.method constructor <init>(Lorg/joda/money/format/MoneyAmountStyle;)V
    .locals 0

    .prologue
    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    iput-object p1, p0, Lorg/joda/money/format/AmountPrinterParser;->style:Lorg/joda/money/format/MoneyAmountStyle;

    .line 43
    return-void
.end method

.method private isPostGroupingPoint(IIII)Z
    .locals 5

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 120
    add-int/lit8 v2, p1, 0x1

    if-lt v2, p2, :cond_1

    move v2, v0

    .line 121
    :goto_0
    if-le p1, p3, :cond_3

    .line 122
    sub-int v3, p1, p3

    rem-int/2addr v3, p4

    add-int/lit8 v4, p4, -0x1

    if-ne v3, v4, :cond_2

    if-nez v2, :cond_2

    .line 124
    :cond_0
    :goto_1
    return v0

    :cond_1
    move v2, v1

    .line 120
    goto :goto_0

    :cond_2
    move v0, v1

    .line 122
    goto :goto_1

    .line 124
    :cond_3
    rem-int v3, p1, p3

    add-int/lit8 v4, p3, -0x1

    if-ne v3, v4, :cond_4

    if-eqz v2, :cond_0

    :cond_4
    move v0, v1

    goto :goto_1
.end method

.method private isPreGroupingPoint(III)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 113
    add-int v2, p2, p3

    if-lt p1, v2, :cond_2

    .line 114
    sub-int v2, p1, p2

    rem-int/2addr v2, p3

    if-nez v2, :cond_1

    .line 116
    :cond_0
    :goto_0
    return v0

    :cond_1
    move v0, v1

    .line 114
    goto :goto_0

    .line 116
    :cond_2
    rem-int v2, p1, p2

    if-eqz v2, :cond_0

    move v0, v1

    goto :goto_0
.end method


# virtual methods
.method public parse(Lorg/joda/money/format/MoneyParseContext;)V
    .locals 13

    .prologue
    const/16 v11, 0x2e

    const/4 v2, 0x0

    const/4 v3, 0x1

    .line 129
    invoke-virtual {p1}, Lorg/joda/money/format/MoneyParseContext;->getTextLength()I

    move-result v6

    .line 130
    iget-object v0, p0, Lorg/joda/money/format/AmountPrinterParser;->style:Lorg/joda/money/format/MoneyAmountStyle;

    invoke-virtual {p1}, Lorg/joda/money/format/MoneyParseContext;->getLocale()Ljava/util/Locale;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/joda/money/format/MoneyAmountStyle;->localize(Ljava/util/Locale;)Lorg/joda/money/format/MoneyAmountStyle;

    move-result-object v7

    .line 131
    invoke-virtual {p1}, Lorg/joda/money/format/MoneyParseContext;->getIndex()I

    move-result v0

    sub-int v0, v6, v0

    new-array v8, v0, [C

    .line 134
    invoke-virtual {p1}, Lorg/joda/money/format/MoneyParseContext;->getIndex()I

    move-result v1

    .line 135
    if-ge v1, v6, :cond_8

    .line 136
    invoke-virtual {p1}, Lorg/joda/money/format/MoneyParseContext;->getText()Ljava/lang/CharSequence;

    move-result-object v4

    add-int/lit8 v0, v1, 0x1

    invoke-interface {v4, v1}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v1

    .line 137
    invoke-virtual {v7}, Lorg/joda/money/format/MoneyAmountStyle;->getNegativeSignCharacter()Ljava/lang/Character;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Character;->charValue()C

    move-result v4

    if-ne v1, v4, :cond_0

    .line 138
    const/16 v1, 0x2d

    aput-char v1, v8, v2

    move v1, v2

    move v4, v3

    :goto_0
    move v5, v0

    move v0, v1

    move v1, v2

    .line 152
    :goto_1
    if-ge v5, v6, :cond_6

    .line 153
    invoke-virtual {p1}, Lorg/joda/money/format/MoneyParseContext;->getText()Ljava/lang/CharSequence;

    move-result-object v9

    invoke-interface {v9, v5}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v9

    .line 154
    invoke-virtual {v7}, Lorg/joda/money/format/MoneyAmountStyle;->getZeroCharacter()Ljava/lang/Character;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/Character;->charValue()C

    move-result v10

    if-lt v9, v10, :cond_4

    invoke-virtual {v7}, Lorg/joda/money/format/MoneyAmountStyle;->getZeroCharacter()Ljava/lang/Character;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/Character;->charValue()C

    move-result v10

    add-int/lit8 v10, v10, 0xa

    if-ge v9, v10, :cond_4

    .line 155
    add-int/lit8 v1, v4, 0x1

    add-int/lit8 v9, v9, 0x30

    invoke-virtual {v7}, Lorg/joda/money/format/MoneyAmountStyle;->getZeroCharacter()Ljava/lang/Character;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/Character;->charValue()C

    move-result v10

    sub-int/2addr v9, v10

    int-to-char v9, v9

    aput-char v9, v8, v4

    move v4, v1

    move v1, v0

    move v0, v2

    .line 152
    :goto_2
    add-int/lit8 v5, v5, 0x1

    move v12, v0

    move v0, v1

    move v1, v12

    goto :goto_1

    .line 139
    :cond_0
    invoke-virtual {v7}, Lorg/joda/money/format/MoneyAmountStyle;->getPositiveSignCharacter()Ljava/lang/Character;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Character;->charValue()C

    move-result v4

    if-ne v1, v4, :cond_1

    .line 140
    const/16 v1, 0x2b

    aput-char v1, v8, v2

    move v1, v2

    move v4, v3

    goto :goto_0

    .line 141
    :cond_1
    invoke-virtual {v7}, Lorg/joda/money/format/MoneyAmountStyle;->getZeroCharacter()Ljava/lang/Character;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Character;->charValue()C

    move-result v4

    if-lt v1, v4, :cond_2

    invoke-virtual {v7}, Lorg/joda/money/format/MoneyAmountStyle;->getZeroCharacter()Ljava/lang/Character;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Character;->charValue()C

    move-result v4

    add-int/lit8 v4, v4, 0xa

    if-ge v1, v4, :cond_2

    .line 142
    add-int/lit8 v1, v1, 0x30

    invoke-virtual {v7}, Lorg/joda/money/format/MoneyAmountStyle;->getZeroCharacter()Ljava/lang/Character;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Character;->charValue()C

    move-result v4

    sub-int/2addr v1, v4

    int-to-char v1, v1

    aput-char v1, v8, v2

    move v1, v2

    move v4, v3

    goto :goto_0

    .line 143
    :cond_2
    invoke-virtual {v7}, Lorg/joda/money/format/MoneyAmountStyle;->getDecimalPointCharacter()Ljava/lang/Character;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Character;->charValue()C

    move-result v4

    if-ne v1, v4, :cond_3

    .line 144
    aput-char v11, v8, v2

    move v1, v3

    move v4, v3

    .line 145
    goto/16 :goto_0

    .line 147
    :cond_3
    invoke-virtual {p1}, Lorg/joda/money/format/MoneyParseContext;->setError()V

    .line 176
    :goto_3
    return-void

    .line 157
    :cond_4
    invoke-virtual {v7}, Lorg/joda/money/format/MoneyAmountStyle;->getDecimalPointCharacter()Ljava/lang/Character;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/Character;->charValue()C

    move-result v10

    if-ne v9, v10, :cond_5

    if-nez v0, :cond_5

    .line 158
    add-int/lit8 v0, v4, 0x1

    aput-char v11, v8, v4

    move v1, v3

    move v4, v0

    move v0, v2

    .line 160
    goto :goto_2

    .line 161
    :cond_5
    invoke-virtual {v7}, Lorg/joda/money/format/MoneyAmountStyle;->getGroupingCharacter()Ljava/lang/Character;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/Character;->charValue()C

    move-result v10

    if-ne v9, v10, :cond_6

    if-nez v1, :cond_6

    move v1, v0

    move v0, v3

    .line 162
    goto :goto_2

    .line 167
    :cond_6
    if-eqz v1, :cond_7

    .line 168
    add-int/lit8 v0, v5, -0x1

    .line 171
    :goto_4
    :try_start_0
    new-instance v1, Ljava/math/BigDecimal;

    const/4 v2, 0x0

    invoke-direct {v1, v8, v2, v4}, Ljava/math/BigDecimal;-><init>([CII)V

    invoke-virtual {p1, v1}, Lorg/joda/money/format/MoneyParseContext;->setAmount(Ljava/math/BigDecimal;)V

    .line 172
    invoke-virtual {p1, v0}, Lorg/joda/money/format/MoneyParseContext;->setIndex(I)V
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_3

    .line 173
    :catch_0
    move-exception v0

    .line 174
    invoke-virtual {p1}, Lorg/joda/money/format/MoneyParseContext;->setError()V

    goto :goto_3

    :cond_7
    move v0, v5

    goto :goto_4

    :cond_8
    move v0, v1

    move v4, v2

    move v1, v2

    goto/16 :goto_0
.end method

.method public print(Lorg/joda/money/format/MoneyPrintContext;Ljava/lang/Appendable;Lorg/joda/money/BigMoney;)V
    .locals 12
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/16 v7, 0x30

    const/4 v1, 0x0

    .line 48
    iget-object v0, p0, Lorg/joda/money/format/AmountPrinterParser;->style:Lorg/joda/money/format/MoneyAmountStyle;

    invoke-virtual {p1}, Lorg/joda/money/format/MoneyPrintContext;->getLocale()Ljava/util/Locale;

    move-result-object v2

    invoke-virtual {v0, v2}, Lorg/joda/money/format/MoneyAmountStyle;->localize(Ljava/util/Locale;)Lorg/joda/money/format/MoneyAmountStyle;

    move-result-object v8

    .line 49
    invoke-virtual {p3}, Lorg/joda/money/BigMoney;->isNegative()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 50
    invoke-virtual {p3}, Lorg/joda/money/BigMoney;->negated()Lorg/joda/money/BigMoney;

    move-result-object p3

    .line 51
    invoke-virtual {v8}, Lorg/joda/money/format/MoneyAmountStyle;->isAbsValue()Z

    move-result v0

    if-nez v0, :cond_0

    .line 52
    invoke-virtual {v8}, Lorg/joda/money/format/MoneyAmountStyle;->getNegativeSignCharacter()Ljava/lang/Character;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Character;->charValue()C

    move-result v0

    invoke-interface {p2, v0}, Ljava/lang/Appendable;->append(C)Ljava/lang/Appendable;

    .line 55
    :cond_0
    invoke-virtual {p3}, Lorg/joda/money/BigMoney;->getAmount()Ljava/math/BigDecimal;

    move-result-object v0

    invoke-virtual {v0}, Ljava/math/BigDecimal;->toPlainString()Ljava/lang/String;

    move-result-object v2

    .line 56
    invoke-virtual {v8}, Lorg/joda/money/format/MoneyAmountStyle;->getZeroCharacter()Ljava/lang/Character;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Character;->charValue()C

    move-result v0

    .line 57
    if-eq v0, v7, :cond_f

    .line 58
    add-int/lit8 v3, v0, -0x30

    .line 59
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move v0, v1

    .line 60
    :goto_0
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v5

    if-ge v0, v5, :cond_2

    .line 61
    invoke-virtual {v2, v0}, Ljava/lang/String;->charAt(I)C

    move-result v5

    .line 62
    if-lt v5, v7, :cond_1

    const/16 v6, 0x39

    if-gt v5, v6, :cond_1

    .line 63
    add-int/2addr v5, v3

    int-to-char v5, v5

    invoke-virtual {v4, v0, v5}, Ljava/lang/StringBuilder;->setCharAt(IC)V

    .line 60
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 66
    :cond_2
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 68
    :goto_1
    const/16 v2, 0x2e

    invoke-virtual {v0, v2}, Ljava/lang/String;->indexOf(I)I

    move-result v7

    .line 69
    add-int/lit8 v9, v7, 0x1

    .line 70
    invoke-virtual {v8}, Lorg/joda/money/format/MoneyAmountStyle;->getGroupingStyle()Lorg/joda/money/format/GroupingStyle;

    move-result-object v2

    sget-object v3, Lorg/joda/money/format/GroupingStyle;->NONE:Lorg/joda/money/format/GroupingStyle;

    if-ne v2, v3, :cond_5

    .line 71
    if-gez v7, :cond_4

    .line 72
    invoke-interface {p2, v0}, Ljava/lang/Appendable;->append(Ljava/lang/CharSequence;)Ljava/lang/Appendable;

    .line 73
    invoke-virtual {v8}, Lorg/joda/money/format/MoneyAmountStyle;->isForcedDecimalPoint()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 74
    invoke-virtual {v8}, Lorg/joda/money/format/MoneyAmountStyle;->getDecimalPointCharacter()Ljava/lang/Character;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Character;->charValue()C

    move-result v0

    invoke-interface {p2, v0}, Ljava/lang/Appendable;->append(C)Ljava/lang/Appendable;

    .line 110
    :cond_3
    :goto_2
    return-void

    .line 77
    :cond_4
    invoke-virtual {v0, v1, v7}, Ljava/lang/String;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {p2, v1}, Ljava/lang/Appendable;->append(Ljava/lang/CharSequence;)Ljava/lang/Appendable;

    move-result-object v1

    invoke-virtual {v8}, Lorg/joda/money/format/MoneyAmountStyle;->getDecimalPointCharacter()Ljava/lang/Character;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Character;->charValue()C

    move-result v2

    invoke-interface {v1, v2}, Ljava/lang/Appendable;->append(C)Ljava/lang/Appendable;

    move-result-object v1

    invoke-virtual {v0, v9}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/lang/Appendable;->append(Ljava/lang/CharSequence;)Ljava/lang/Appendable;

    goto :goto_2

    .line 81
    :cond_5
    invoke-virtual {v8}, Lorg/joda/money/format/MoneyAmountStyle;->getGroupingSize()Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v3

    .line 82
    invoke-virtual {v8}, Lorg/joda/money/format/MoneyAmountStyle;->getExtendedGroupingSize()Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    .line 83
    if-nez v2, :cond_6

    move v2, v3

    .line 84
    :cond_6
    invoke-virtual {v8}, Lorg/joda/money/format/MoneyAmountStyle;->getGroupingCharacter()Ljava/lang/Character;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Character;->charValue()C

    move-result v10

    .line 85
    if-gez v7, :cond_8

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v4

    move v6, v4

    .line 86
    :goto_3
    if-gez v7, :cond_9

    move v4, v1

    .line 87
    :goto_4
    invoke-virtual {v0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v5

    invoke-interface {p2, v5}, Ljava/lang/Appendable;->append(C)Ljava/lang/Appendable;

    .line 88
    const/4 v5, 0x1

    :goto_5
    if-ge v5, v6, :cond_a

    .line 89
    sub-int v11, v6, v5

    invoke-direct {p0, v11, v3, v2}, Lorg/joda/money/format/AmountPrinterParser;->isPreGroupingPoint(III)Z

    move-result v11

    if-eqz v11, :cond_7

    .line 90
    invoke-interface {p2, v10}, Ljava/lang/Appendable;->append(C)Ljava/lang/Appendable;

    .line 92
    :cond_7
    invoke-virtual {v0, v5}, Ljava/lang/String;->charAt(I)C

    move-result v11

    invoke-interface {p2, v11}, Ljava/lang/Appendable;->append(C)Ljava/lang/Appendable;

    .line 88
    add-int/lit8 v5, v5, 0x1

    goto :goto_5

    :cond_8
    move v6, v7

    .line 85
    goto :goto_3

    .line 86
    :cond_9
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v4

    sub-int/2addr v4, v7

    add-int/lit8 v4, v4, -0x1

    goto :goto_4

    .line 94
    :cond_a
    if-gez v7, :cond_b

    invoke-virtual {v8}, Lorg/joda/money/format/MoneyAmountStyle;->isForcedDecimalPoint()Z

    move-result v5

    if-eqz v5, :cond_c

    .line 95
    :cond_b
    invoke-virtual {v8}, Lorg/joda/money/format/MoneyAmountStyle;->getDecimalPointCharacter()Ljava/lang/Character;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Character;->charValue()C

    move-result v5

    invoke-interface {p2, v5}, Ljava/lang/Appendable;->append(C)Ljava/lang/Appendable;

    .line 97
    :cond_c
    invoke-virtual {v8}, Lorg/joda/money/format/MoneyAmountStyle;->getGroupingStyle()Lorg/joda/money/format/GroupingStyle;

    move-result-object v5

    sget-object v6, Lorg/joda/money/format/GroupingStyle;->BEFORE_DECIMAL_POINT:Lorg/joda/money/format/GroupingStyle;

    if-ne v5, v6, :cond_d

    .line 98
    if-ltz v7, :cond_3

    .line 99
    invoke-virtual {v0, v9}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p2, v0}, Ljava/lang/Appendable;->append(Ljava/lang/CharSequence;)Ljava/lang/Appendable;

    goto/16 :goto_2

    .line 102
    :cond_d
    :goto_6
    if-ge v1, v4, :cond_3

    .line 103
    add-int v5, v1, v9

    invoke-virtual {v0, v5}, Ljava/lang/String;->charAt(I)C

    move-result v5

    invoke-interface {p2, v5}, Ljava/lang/Appendable;->append(C)Ljava/lang/Appendable;

    .line 104
    invoke-direct {p0, v1, v4, v3, v2}, Lorg/joda/money/format/AmountPrinterParser;->isPostGroupingPoint(IIII)Z

    move-result v5

    if-eqz v5, :cond_e

    .line 105
    invoke-interface {p2, v10}, Ljava/lang/Appendable;->append(C)Ljava/lang/Appendable;

    .line 102
    :cond_e
    add-int/lit8 v1, v1, 0x1

    goto :goto_6

    :cond_f
    move-object v0, v2

    goto/16 :goto_1
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 180
    const-string v0, "${amount}"

    return-object v0
.end method
