.class final Lorg/joda/money/format/LiteralPrinterParser;
.super Ljava/lang/Object;
.source "LiteralPrinterParser.java"

# interfaces
.implements Ljava/io/Serializable;
.implements Lorg/joda/money/format/MoneyParser;
.implements Lorg/joda/money/format/MoneyPrinter;


# static fields
.field private static final serialVersionUID:J = 0x1L


# instance fields
.field private final literal:Ljava/lang/String;


# direct methods
.method constructor <init>(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    iput-object p1, p0, Lorg/joda/money/format/LiteralPrinterParser;->literal:Ljava/lang/String;

    .line 42
    return-void
.end method


# virtual methods
.method public parse(Lorg/joda/money/format/MoneyParseContext;)V
    .locals 3

    .prologue
    .line 52
    invoke-virtual {p1}, Lorg/joda/money/format/MoneyParseContext;->getIndex()I

    move-result v0

    iget-object v1, p0, Lorg/joda/money/format/LiteralPrinterParser;->literal:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/2addr v0, v1

    .line 53
    invoke-virtual {p1}, Lorg/joda/money/format/MoneyParseContext;->getTextLength()I

    move-result v1

    if-gt v0, v1, :cond_0

    invoke-virtual {p1}, Lorg/joda/money/format/MoneyParseContext;->getIndex()I

    move-result v1

    invoke-virtual {p1, v1, v0}, Lorg/joda/money/format/MoneyParseContext;->getTextSubstring(II)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lorg/joda/money/format/LiteralPrinterParser;->literal:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 55
    invoke-virtual {p1, v0}, Lorg/joda/money/format/MoneyParseContext;->setIndex(I)V

    .line 59
    :goto_0
    return-void

    .line 57
    :cond_0
    invoke-virtual {p1}, Lorg/joda/money/format/MoneyParseContext;->setError()V

    goto :goto_0
.end method

.method public print(Lorg/joda/money/format/MoneyPrintContext;Ljava/lang/Appendable;Lorg/joda/money/BigMoney;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 47
    iget-object v0, p0, Lorg/joda/money/format/LiteralPrinterParser;->literal:Ljava/lang/String;

    invoke-interface {p2, v0}, Ljava/lang/Appendable;->append(Ljava/lang/CharSequence;)Ljava/lang/Appendable;

    .line 48
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 63
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lorg/joda/money/format/LiteralPrinterParser;->literal:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
