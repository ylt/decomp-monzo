.class public final Lorg/joda/money/format/MoneyFormatter;
.super Ljava/lang/Object;
.source "MoneyFormatter.java"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field static final synthetic $assertionsDisabled:Z

.field private static final serialVersionUID:J = 0x8e2d7ed2L


# instance fields
.field private final locale:Ljava/util/Locale;

.field private final printerParser:Lorg/joda/money/format/MultiPrinterParser;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 34
    const-class v0, Lorg/joda/money/format/MoneyFormatter;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/joda/money/format/MoneyFormatter;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private constructor <init>(Ljava/util/Locale;Lorg/joda/money/format/MultiPrinterParser;)V
    .locals 1

    .prologue
    .line 87
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 88
    sget-boolean v0, Lorg/joda/money/format/MoneyFormatter;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 89
    :cond_0
    sget-boolean v0, Lorg/joda/money/format/MoneyFormatter;->$assertionsDisabled:Z

    if-nez v0, :cond_1

    if-nez p2, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 90
    :cond_1
    iput-object p1, p0, Lorg/joda/money/format/MoneyFormatter;->locale:Ljava/util/Locale;

    .line 91
    iput-object p2, p0, Lorg/joda/money/format/MoneyFormatter;->printerParser:Lorg/joda/money/format/MultiPrinterParser;

    .line 92
    return-void
.end method

.method constructor <init>(Ljava/util/Locale;[Lorg/joda/money/format/MoneyPrinter;[Lorg/joda/money/format/MoneyParser;)V
    .locals 2

    .prologue
    .line 72
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 73
    sget-boolean v0, Lorg/joda/money/format/MoneyFormatter;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 74
    :cond_0
    sget-boolean v0, Lorg/joda/money/format/MoneyFormatter;->$assertionsDisabled:Z

    if-nez v0, :cond_1

    if-nez p2, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 75
    :cond_1
    sget-boolean v0, Lorg/joda/money/format/MoneyFormatter;->$assertionsDisabled:Z

    if-nez v0, :cond_2

    if-nez p3, :cond_2

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 76
    :cond_2
    sget-boolean v0, Lorg/joda/money/format/MoneyFormatter;->$assertionsDisabled:Z

    if-nez v0, :cond_3

    array-length v0, p2

    array-length v1, p3

    if-eq v0, v1, :cond_3

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 77
    :cond_3
    iput-object p1, p0, Lorg/joda/money/format/MoneyFormatter;->locale:Ljava/util/Locale;

    .line 78
    new-instance v0, Lorg/joda/money/format/MultiPrinterParser;

    invoke-direct {v0, p2, p3}, Lorg/joda/money/format/MultiPrinterParser;-><init>([Lorg/joda/money/format/MoneyPrinter;[Lorg/joda/money/format/MoneyParser;)V

    iput-object v0, p0, Lorg/joda/money/format/MoneyFormatter;->printerParser:Lorg/joda/money/format/MultiPrinterParser;

    .line 79
    return-void
.end method

.method static checkNotNull(Ljava/lang/Object;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 59
    if-nez p0, :cond_0

    .line 60
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0, p1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 62
    :cond_0
    return-void
.end method


# virtual methods
.method public getLocale()Ljava/util/Locale;
    .locals 1

    .prologue
    .line 111
    iget-object v0, p0, Lorg/joda/money/format/MoneyFormatter;->locale:Ljava/util/Locale;

    return-object v0
.end method

.method getPrinterParser()Lorg/joda/money/format/MultiPrinterParser;
    .locals 1

    .prologue
    .line 101
    iget-object v0, p0, Lorg/joda/money/format/MoneyFormatter;->printerParser:Lorg/joda/money/format/MultiPrinterParser;

    return-object v0
.end method

.method public isParser()Z
    .locals 1

    .prologue
    .line 150
    iget-object v0, p0, Lorg/joda/money/format/MoneyFormatter;->printerParser:Lorg/joda/money/format/MultiPrinterParser;

    invoke-virtual {v0}, Lorg/joda/money/format/MultiPrinterParser;->isParser()Z

    move-result v0

    return v0
.end method

.method public isPrinter()Z
    .locals 1

    .prologue
    .line 138
    iget-object v0, p0, Lorg/joda/money/format/MoneyFormatter;->printerParser:Lorg/joda/money/format/MultiPrinterParser;

    invoke-virtual {v0}, Lorg/joda/money/format/MultiPrinterParser;->isPrinter()Z

    move-result v0

    return v0
.end method

.method public parse(Ljava/lang/CharSequence;I)Lorg/joda/money/format/MoneyParseContext;
    .locals 3

    .prologue
    .line 276
    const-string v0, "Text must not be null"

    invoke-static {p1, v0}, Lorg/joda/money/format/MoneyFormatter;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 277
    if-ltz p2, :cond_0

    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-le p2, v0, :cond_1

    .line 278
    :cond_0
    new-instance v0, Ljava/lang/StringIndexOutOfBoundsException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Invalid start index: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 280
    :cond_1
    invoke-virtual {p0}, Lorg/joda/money/format/MoneyFormatter;->isParser()Z

    move-result v0

    if-nez v0, :cond_2

    .line 281
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "MoneyFomatter has not been configured to be able to parse"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 283
    :cond_2
    new-instance v0, Lorg/joda/money/format/MoneyParseContext;

    iget-object v1, p0, Lorg/joda/money/format/MoneyFormatter;->locale:Ljava/util/Locale;

    invoke-direct {v0, v1, p1, p2}, Lorg/joda/money/format/MoneyParseContext;-><init>(Ljava/util/Locale;Ljava/lang/CharSequence;I)V

    .line 284
    iget-object v1, p0, Lorg/joda/money/format/MoneyFormatter;->printerParser:Lorg/joda/money/format/MultiPrinterParser;

    invoke-virtual {v1, v0}, Lorg/joda/money/format/MultiPrinterParser;->parse(Lorg/joda/money/format/MoneyParseContext;)V

    .line 285
    return-object v0
.end method

.method public parseBigMoney(Ljava/lang/CharSequence;)Lorg/joda/money/BigMoney;
    .locals 5

    .prologue
    const/16 v3, 0x40

    const/4 v2, 0x0

    .line 228
    const-string v0, "Text must not be null"

    invoke-static {p1, v0}, Lorg/joda/money/format/MoneyFormatter;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 229
    invoke-virtual {p0, p1, v2}, Lorg/joda/money/format/MoneyFormatter;->parse(Ljava/lang/CharSequence;I)Lorg/joda/money/format/MoneyParseContext;

    move-result-object v1

    .line 230
    invoke-virtual {v1}, Lorg/joda/money/format/MoneyParseContext;->isError()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {v1}, Lorg/joda/money/format/MoneyParseContext;->isFullyParsed()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {v1}, Lorg/joda/money/format/MoneyParseContext;->isComplete()Z

    move-result v0

    if-nez v0, :cond_4

    .line 231
    :cond_0
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-le v0, v3, :cond_1

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-interface {p1, v2, v3}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "..."

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 232
    :goto_0
    invoke-virtual {v1}, Lorg/joda/money/format/MoneyParseContext;->isError()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 233
    new-instance v2, Lorg/joda/money/format/MoneyFormatException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Text could not be parsed at index "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Lorg/joda/money/format/MoneyParseContext;->getErrorIndex()I

    move-result v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ": "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lorg/joda/money/format/MoneyFormatException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 231
    :cond_1
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 234
    :cond_2
    invoke-virtual {v1}, Lorg/joda/money/format/MoneyParseContext;->isFullyParsed()Z

    move-result v2

    if-nez v2, :cond_3

    .line 235
    new-instance v2, Lorg/joda/money/format/MoneyFormatException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Unparsed text found at index "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Lorg/joda/money/format/MoneyParseContext;->getIndex()I

    move-result v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ": "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lorg/joda/money/format/MoneyFormatException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 237
    :cond_3
    new-instance v1, Lorg/joda/money/format/MoneyFormatException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Parsing did not find both currency and amount: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lorg/joda/money/format/MoneyFormatException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 240
    :cond_4
    invoke-virtual {v1}, Lorg/joda/money/format/MoneyParseContext;->toBigMoney()Lorg/joda/money/BigMoney;

    move-result-object v0

    return-object v0
.end method

.method public parseMoney(Ljava/lang/CharSequence;)Lorg/joda/money/Money;
    .locals 1

    .prologue
    .line 258
    invoke-virtual {p0, p1}, Lorg/joda/money/format/MoneyFormatter;->parseBigMoney(Ljava/lang/CharSequence;)Lorg/joda/money/BigMoney;

    move-result-object v0

    invoke-virtual {v0}, Lorg/joda/money/BigMoney;->toMoney()Lorg/joda/money/Money;

    move-result-object v0

    return-object v0
.end method

.method public print(Lorg/joda/money/BigMoneyProvider;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 163
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 164
    invoke-virtual {p0, v0, p1}, Lorg/joda/money/format/MoneyFormatter;->print(Ljava/lang/Appendable;Lorg/joda/money/BigMoneyProvider;)V

    .line 165
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public print(Ljava/lang/Appendable;Lorg/joda/money/BigMoneyProvider;)V
    .locals 3

    .prologue
    .line 183
    :try_start_0
    invoke-virtual {p0, p1, p2}, Lorg/joda/money/format/MoneyFormatter;->printIO(Ljava/lang/Appendable;Lorg/joda/money/BigMoneyProvider;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 187
    return-void

    .line 184
    :catch_0
    move-exception v0

    .line 185
    new-instance v1, Lorg/joda/money/format/MoneyFormatException;

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Lorg/joda/money/format/MoneyFormatException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method public printIO(Ljava/lang/Appendable;Lorg/joda/money/BigMoneyProvider;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 204
    const-string v0, "BigMoneyProvider must not be null"

    invoke-static {p2, v0}, Lorg/joda/money/format/MoneyFormatter;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 205
    invoke-virtual {p0}, Lorg/joda/money/format/MoneyFormatter;->isPrinter()Z

    move-result v0

    if-nez v0, :cond_0

    .line 206
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "MoneyFomatter has not been configured to be able to print"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 209
    :cond_0
    invoke-static {p2}, Lorg/joda/money/BigMoney;->of(Lorg/joda/money/BigMoneyProvider;)Lorg/joda/money/BigMoney;

    move-result-object v0

    .line 210
    new-instance v1, Lorg/joda/money/format/MoneyPrintContext;

    iget-object v2, p0, Lorg/joda/money/format/MoneyFormatter;->locale:Ljava/util/Locale;

    invoke-direct {v1, v2}, Lorg/joda/money/format/MoneyPrintContext;-><init>(Ljava/util/Locale;)V

    .line 211
    iget-object v2, p0, Lorg/joda/money/format/MoneyFormatter;->printerParser:Lorg/joda/money/format/MultiPrinterParser;

    invoke-virtual {v2, v1, p1, v0}, Lorg/joda/money/format/MultiPrinterParser;->print(Lorg/joda/money/format/MoneyPrintContext;Ljava/lang/Appendable;Lorg/joda/money/BigMoney;)V

    .line 212
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 296
    iget-object v0, p0, Lorg/joda/money/format/MoneyFormatter;->printerParser:Lorg/joda/money/format/MultiPrinterParser;

    invoke-virtual {v0}, Lorg/joda/money/format/MultiPrinterParser;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public withLocale(Ljava/util/Locale;)Lorg/joda/money/format/MoneyFormatter;
    .locals 2

    .prologue
    .line 124
    const-string v0, "Locale must not be null"

    invoke-static {p1, v0}, Lorg/joda/money/format/MoneyFormatter;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 125
    new-instance v0, Lorg/joda/money/format/MoneyFormatter;

    iget-object v1, p0, Lorg/joda/money/format/MoneyFormatter;->printerParser:Lorg/joda/money/format/MultiPrinterParser;

    invoke-direct {v0, p1, v1}, Lorg/joda/money/format/MoneyFormatter;-><init>(Ljava/util/Locale;Lorg/joda/money/format/MultiPrinterParser;)V

    return-object v0
.end method
