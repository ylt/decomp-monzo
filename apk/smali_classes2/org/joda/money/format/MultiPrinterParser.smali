.class final Lorg/joda/money/format/MultiPrinterParser;
.super Ljava/lang/Object;
.source "MultiPrinterParser.java"

# interfaces
.implements Ljava/io/Serializable;
.implements Lorg/joda/money/format/MoneyParser;
.implements Lorg/joda/money/format/MoneyPrinter;


# static fields
.field private static final serialVersionUID:J = 0x1L


# instance fields
.field private final parsers:[Lorg/joda/money/format/MoneyParser;

.field private final printers:[Lorg/joda/money/format/MoneyPrinter;


# direct methods
.method constructor <init>([Lorg/joda/money/format/MoneyPrinter;[Lorg/joda/money/format/MoneyParser;)V
    .locals 0

    .prologue
    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    iput-object p1, p0, Lorg/joda/money/format/MultiPrinterParser;->printers:[Lorg/joda/money/format/MoneyPrinter;

    .line 49
    iput-object p2, p0, Lorg/joda/money/format/MultiPrinterParser;->parsers:[Lorg/joda/money/format/MoneyParser;

    .line 50
    return-void
.end method


# virtual methods
.method appendTo(Lorg/joda/money/format/MoneyFormatterBuilder;)V
    .locals 3

    .prologue
    .line 62
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lorg/joda/money/format/MultiPrinterParser;->printers:[Lorg/joda/money/format/MoneyPrinter;

    array-length v1, v1

    if-ge v0, v1, :cond_0

    .line 63
    iget-object v1, p0, Lorg/joda/money/format/MultiPrinterParser;->printers:[Lorg/joda/money/format/MoneyPrinter;

    aget-object v1, v1, v0

    iget-object v2, p0, Lorg/joda/money/format/MultiPrinterParser;->parsers:[Lorg/joda/money/format/MoneyParser;

    aget-object v2, v2, v0

    invoke-virtual {p1, v1, v2}, Lorg/joda/money/format/MoneyFormatterBuilder;->append(Lorg/joda/money/format/MoneyPrinter;Lorg/joda/money/format/MoneyParser;)Lorg/joda/money/format/MoneyFormatterBuilder;

    .line 62
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 65
    :cond_0
    return-void
.end method

.method isParser()Z
    .locals 2

    .prologue
    .line 58
    iget-object v0, p0, Lorg/joda/money/format/MultiPrinterParser;->parsers:[Lorg/joda/money/format/MoneyParser;

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method isPrinter()Z
    .locals 2

    .prologue
    .line 54
    iget-object v0, p0, Lorg/joda/money/format/MultiPrinterParser;->printers:[Lorg/joda/money/format/MoneyPrinter;

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public parse(Lorg/joda/money/format/MoneyParseContext;)V
    .locals 4

    .prologue
    .line 77
    iget-object v1, p0, Lorg/joda/money/format/MultiPrinterParser;->parsers:[Lorg/joda/money/format/MoneyParser;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    .line 78
    invoke-interface {v3, p1}, Lorg/joda/money/format/MoneyParser;->parse(Lorg/joda/money/format/MoneyParseContext;)V

    .line 79
    invoke-virtual {p1}, Lorg/joda/money/format/MoneyParseContext;->isError()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 83
    :cond_0
    return-void

    .line 77
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public print(Lorg/joda/money/format/MoneyPrintContext;Ljava/lang/Appendable;Lorg/joda/money/BigMoney;)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 70
    iget-object v1, p0, Lorg/joda/money/format/MultiPrinterParser;->printers:[Lorg/joda/money/format/MoneyPrinter;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    .line 71
    invoke-interface {v3, p1, p2, p3}, Lorg/joda/money/format/MoneyPrinter;->print(Lorg/joda/money/format/MoneyPrintContext;Ljava/lang/Appendable;Lorg/joda/money/BigMoney;)V

    .line 70
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 73
    :cond_0
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 87
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 88
    invoke-virtual {p0}, Lorg/joda/money/format/MultiPrinterParser;->isPrinter()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 89
    iget-object v3, p0, Lorg/joda/money/format/MultiPrinterParser;->printers:[Lorg/joda/money/format/MoneyPrinter;

    array-length v4, v3

    move v1, v0

    :goto_0
    if-ge v1, v4, :cond_0

    aget-object v5, v3, v1

    .line 90
    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 89
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 93
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 94
    invoke-virtual {p0}, Lorg/joda/money/format/MultiPrinterParser;->isParser()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 95
    iget-object v3, p0, Lorg/joda/money/format/MultiPrinterParser;->parsers:[Lorg/joda/money/format/MoneyParser;

    array-length v4, v3

    :goto_1
    if-ge v0, v4, :cond_1

    aget-object v5, v3, v0

    .line 96
    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 95
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 99
    :cond_1
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 100
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 101
    invoke-virtual {p0}, Lorg/joda/money/format/MultiPrinterParser;->isPrinter()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-virtual {p0}, Lorg/joda/money/format/MultiPrinterParser;->isParser()Z

    move-result v2

    if-nez v2, :cond_3

    .line 108
    :cond_2
    :goto_2
    return-object v0

    .line 103
    :cond_3
    invoke-virtual {p0}, Lorg/joda/money/format/MultiPrinterParser;->isParser()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-virtual {p0}, Lorg/joda/money/format/MultiPrinterParser;->isPrinter()Z

    move-result v2

    if-nez v2, :cond_4

    move-object v0, v1

    .line 104
    goto :goto_2

    .line 105
    :cond_4
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 108
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ":"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_2
.end method
