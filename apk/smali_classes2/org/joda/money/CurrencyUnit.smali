.class public final Lorg/joda/money/CurrencyUnit;
.super Ljava/lang/Object;
.source "CurrencyUnit.java"

# interfaces
.implements Ljava/io/Serializable;
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/io/Serializable;",
        "Ljava/lang/Comparable",
        "<",
        "Lorg/joda/money/CurrencyUnit;",
        ">;"
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z

.field public static final AUD:Lorg/joda/money/CurrencyUnit;

.field public static final CAD:Lorg/joda/money/CurrencyUnit;

.field public static final CHF:Lorg/joda/money/CurrencyUnit;

.field private static final CODE:Ljava/util/regex/Pattern;

.field public static final EUR:Lorg/joda/money/CurrencyUnit;

.field public static final GBP:Lorg/joda/money/CurrencyUnit;

.field public static final JPY:Lorg/joda/money/CurrencyUnit;

.field public static final USD:Lorg/joda/money/CurrencyUnit;

.field private static final currenciesByCode:Ljava/util/concurrent/ConcurrentMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentMap",
            "<",
            "Ljava/lang/String;",
            "Lorg/joda/money/CurrencyUnit;",
            ">;"
        }
    .end annotation
.end field

.field private static final currenciesByCountry:Ljava/util/concurrent/ConcurrentMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentMap",
            "<",
            "Ljava/lang/String;",
            "Lorg/joda/money/CurrencyUnit;",
            ">;"
        }
    .end annotation
.end field

.field private static final currenciesByNumericCode:Ljava/util/concurrent/ConcurrentMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentMap",
            "<",
            "Ljava/lang/Integer;",
            "Lorg/joda/money/CurrencyUnit;",
            ">;"
        }
    .end annotation
.end field

.field private static final serialVersionUID:J = 0x4c54817ef7L


# instance fields
.field private final code:Ljava/lang/String;

.field private final decimalPlaces:S

.field private final numericCode:S


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 49
    const-class v0, Lorg/joda/money/CurrencyUnit;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/joda/money/CurrencyUnit;->$assertionsDisabled:Z

    .line 58
    const-string v0, "[A-Z][A-Z][A-Z]"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lorg/joda/money/CurrencyUnit;->CODE:Ljava/util/regex/Pattern;

    .line 62
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    sput-object v0, Lorg/joda/money/CurrencyUnit;->currenciesByCode:Ljava/util/concurrent/ConcurrentMap;

    .line 66
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    sput-object v0, Lorg/joda/money/CurrencyUnit;->currenciesByNumericCode:Ljava/util/concurrent/ConcurrentMap;

    .line 70
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    sput-object v0, Lorg/joda/money/CurrencyUnit;->currenciesByCountry:Ljava/util/concurrent/ConcurrentMap;

    .line 75
    :try_start_0
    const-string v0, "org.joda.money.CurrencyUnitDataProvider"

    const-string v1, "org.joda.money.DefaultCurrencyUnitDataProvider"

    invoke-static {v0, v1}, Ljava/lang/System;->getProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 77
    const-class v1, Lorg/joda/money/CurrencyUnit;

    invoke-virtual {v1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/ClassLoader;->loadClass(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    const-class v1, Lorg/joda/money/CurrencyUnitDataProvider;

    invoke-virtual {v0, v1}, Ljava/lang/Class;->asSubclass(Ljava/lang/Class;)Ljava/lang/Class;

    move-result-object v0

    .line 79
    invoke-virtual {v0}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/joda/money/CurrencyUnitDataProvider;

    invoke-virtual {v0}, Lorg/joda/money/CurrencyUnitDataProvider;->registerCurrencies()V
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    .line 94
    :goto_1
    const-string v0, "USD"

    invoke-static {v0}, Lorg/joda/money/CurrencyUnit;->of(Ljava/lang/String;)Lorg/joda/money/CurrencyUnit;

    move-result-object v0

    sput-object v0, Lorg/joda/money/CurrencyUnit;->USD:Lorg/joda/money/CurrencyUnit;

    .line 98
    const-string v0, "EUR"

    invoke-static {v0}, Lorg/joda/money/CurrencyUnit;->of(Ljava/lang/String;)Lorg/joda/money/CurrencyUnit;

    move-result-object v0

    sput-object v0, Lorg/joda/money/CurrencyUnit;->EUR:Lorg/joda/money/CurrencyUnit;

    .line 102
    const-string v0, "JPY"

    invoke-static {v0}, Lorg/joda/money/CurrencyUnit;->of(Ljava/lang/String;)Lorg/joda/money/CurrencyUnit;

    move-result-object v0

    sput-object v0, Lorg/joda/money/CurrencyUnit;->JPY:Lorg/joda/money/CurrencyUnit;

    .line 106
    const-string v0, "GBP"

    invoke-static {v0}, Lorg/joda/money/CurrencyUnit;->of(Ljava/lang/String;)Lorg/joda/money/CurrencyUnit;

    move-result-object v0

    sput-object v0, Lorg/joda/money/CurrencyUnit;->GBP:Lorg/joda/money/CurrencyUnit;

    .line 110
    const-string v0, "CHF"

    invoke-static {v0}, Lorg/joda/money/CurrencyUnit;->of(Ljava/lang/String;)Lorg/joda/money/CurrencyUnit;

    move-result-object v0

    sput-object v0, Lorg/joda/money/CurrencyUnit;->CHF:Lorg/joda/money/CurrencyUnit;

    .line 114
    const-string v0, "AUD"

    invoke-static {v0}, Lorg/joda/money/CurrencyUnit;->of(Ljava/lang/String;)Lorg/joda/money/CurrencyUnit;

    move-result-object v0

    sput-object v0, Lorg/joda/money/CurrencyUnit;->AUD:Lorg/joda/money/CurrencyUnit;

    .line 118
    const-string v0, "CAD"

    invoke-static {v0}, Lorg/joda/money/CurrencyUnit;->of(Ljava/lang/String;)Lorg/joda/money/CurrencyUnit;

    move-result-object v0

    sput-object v0, Lorg/joda/money/CurrencyUnit;->CAD:Lorg/joda/money/CurrencyUnit;

    return-void

    .line 49
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 80
    :catch_0
    move-exception v0

    .line 81
    :try_start_1
    new-instance v0, Lorg/joda/money/DefaultCurrencyUnitDataProvider;

    invoke-direct {v0}, Lorg/joda/money/DefaultCurrencyUnitDataProvider;-><init>()V

    invoke-virtual {v0}, Lorg/joda/money/DefaultCurrencyUnitDataProvider;->registerCurrencies()V
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2

    goto :goto_1

    .line 83
    :catch_1
    move-exception v0

    .line 84
    throw v0

    .line 85
    :catch_2
    move-exception v0

    .line 86
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method constructor <init>(Ljava/lang/String;SS)V
    .locals 2

    .prologue
    .line 399
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 400
    sget-boolean v0, Lorg/joda/money/CurrencyUnit;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "Joda-Money bug: Currency code must not be null"

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 401
    :cond_0
    iput-object p1, p0, Lorg/joda/money/CurrencyUnit;->code:Ljava/lang/String;

    .line 402
    iput-short p2, p0, Lorg/joda/money/CurrencyUnit;->numericCode:S

    .line 403
    iput-short p3, p0, Lorg/joda/money/CurrencyUnit;->decimalPlaces:S

    .line 404
    return-void
.end method

.method public static getInstance(Ljava/lang/String;)Lorg/joda/money/CurrencyUnit;
    .locals 1

    .prologue
    .line 375
    invoke-static {p0}, Lorg/joda/money/CurrencyUnit;->of(Ljava/lang/String;)Lorg/joda/money/CurrencyUnit;

    move-result-object v0

    return-object v0
.end method

.method public static getInstance(Ljava/util/Locale;)Lorg/joda/money/CurrencyUnit;
    .locals 1

    .prologue
    .line 388
    invoke-static {p0}, Lorg/joda/money/CurrencyUnit;->of(Ljava/util/Locale;)Lorg/joda/money/CurrencyUnit;

    move-result-object v0

    return-object v0
.end method

.method public static of(Ljava/lang/String;)Lorg/joda/money/CurrencyUnit;
    .locals 3
    .annotation runtime Lorg/joda/convert/FromString;
    .end annotation

    .prologue
    .line 275
    const-string v0, "Currency code must not be null"

    invoke-static {p0, v0}, Lorg/joda/money/MoneyUtils;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 276
    sget-object v0, Lorg/joda/money/CurrencyUnit;->currenciesByCode:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v0, p0}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/joda/money/CurrencyUnit;

    .line 277
    if-nez v0, :cond_0

    .line 278
    new-instance v0, Lorg/joda/money/IllegalCurrencyException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown currency \'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/16 v2, 0x27

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/joda/money/IllegalCurrencyException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 280
    :cond_0
    return-object v0
.end method

.method public static of(Ljava/util/Currency;)Lorg/joda/money/CurrencyUnit;
    .locals 1

    .prologue
    .line 259
    const-string v0, "Currency must not be null"

    invoke-static {p0, v0}, Lorg/joda/money/MoneyUtils;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 260
    invoke-virtual {p0}, Ljava/util/Currency;->getCurrencyCode()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lorg/joda/money/CurrencyUnit;->of(Ljava/lang/String;)Lorg/joda/money/CurrencyUnit;

    move-result-object v0

    return-object v0
.end method

.method public static of(Ljava/util/Locale;)Lorg/joda/money/CurrencyUnit;
    .locals 3

    .prologue
    .line 337
    const-string v0, "Locale must not be null"

    invoke-static {p0, v0}, Lorg/joda/money/MoneyUtils;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 338
    sget-object v0, Lorg/joda/money/CurrencyUnit;->currenciesByCountry:Ljava/util/concurrent/ConcurrentMap;

    invoke-virtual {p0}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/joda/money/CurrencyUnit;

    .line 339
    if-nez v0, :cond_0

    .line 340
    new-instance v0, Lorg/joda/money/IllegalCurrencyException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown currency for locale \'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/16 v2, 0x27

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/joda/money/IllegalCurrencyException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 342
    :cond_0
    return-object v0
.end method

.method public static ofCountry(Ljava/lang/String;)Lorg/joda/money/CurrencyUnit;
    .locals 3

    .prologue
    .line 356
    const-string v0, "Country code must not be null"

    invoke-static {p0, v0}, Lorg/joda/money/MoneyUtils;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 357
    sget-object v0, Lorg/joda/money/CurrencyUnit;->currenciesByCountry:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v0, p0}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/joda/money/CurrencyUnit;

    .line 358
    if-nez v0, :cond_0

    .line 359
    new-instance v0, Lorg/joda/money/IllegalCurrencyException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown currency for country \'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/16 v2, 0x27

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/joda/money/IllegalCurrencyException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 361
    :cond_0
    return-object v0
.end method

.method public static ofNumericCode(I)Lorg/joda/money/CurrencyUnit;
    .locals 3

    .prologue
    .line 320
    sget-object v0, Lorg/joda/money/CurrencyUnit;->currenciesByNumericCode:Ljava/util/concurrent/ConcurrentMap;

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/joda/money/CurrencyUnit;

    .line 321
    if-nez v0, :cond_0

    .line 322
    new-instance v0, Lorg/joda/money/IllegalCurrencyException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown currency \'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const/16 v2, 0x27

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/joda/money/IllegalCurrencyException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 324
    :cond_0
    return-object v0
.end method

.method public static ofNumericCode(Ljava/lang/String;)Lorg/joda/money/CurrencyUnit;
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 294
    const-string v0, "Currency code must not be null"

    invoke-static {p0, v0}, Lorg/joda/money/MoneyUtils;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 295
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 306
    new-instance v0, Lorg/joda/money/IllegalCurrencyException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown currency \'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/16 v2, 0x27

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/joda/money/IllegalCurrencyException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 297
    :pswitch_0
    invoke-virtual {p0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v0

    add-int/lit8 v0, v0, -0x30

    invoke-static {v0}, Lorg/joda/money/CurrencyUnit;->ofNumericCode(I)Lorg/joda/money/CurrencyUnit;

    move-result-object v0

    .line 302
    :goto_0
    return-object v0

    .line 299
    :pswitch_1
    invoke-virtual {p0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v0

    add-int/lit8 v0, v0, -0x30

    mul-int/lit8 v0, v0, 0xa

    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v1

    add-int/2addr v0, v1

    add-int/lit8 v0, v0, -0x30

    invoke-static {v0}, Lorg/joda/money/CurrencyUnit;->ofNumericCode(I)Lorg/joda/money/CurrencyUnit;

    move-result-object v0

    goto :goto_0

    .line 302
    :pswitch_2
    invoke-virtual {p0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v0

    add-int/lit8 v0, v0, -0x30

    mul-int/lit8 v0, v0, 0x64

    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v1

    add-int/lit8 v1, v1, -0x30

    mul-int/lit8 v1, v1, 0xa

    add-int/2addr v0, v1

    const/4 v1, 0x2

    invoke-virtual {p0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v1

    add-int/2addr v0, v1

    add-int/lit8 v0, v0, -0x30

    invoke-static {v0}, Lorg/joda/money/CurrencyUnit;->ofNumericCode(I)Lorg/joda/money/CurrencyUnit;

    move-result-object v0

    goto :goto_0

    .line 295
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private readObject(Ljava/io/ObjectInputStream;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/InvalidObjectException;
        }
    .end annotation

    .prologue
    .line 413
    new-instance v0, Ljava/io/InvalidObjectException;

    const-string v1, "Serialization delegate required"

    invoke-direct {v0, v1}, Ljava/io/InvalidObjectException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static declared-synchronized registerCurrency(Ljava/lang/String;IILjava/util/List;)Lorg/joda/money/CurrencyUnit;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "II",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lorg/joda/money/CurrencyUnit;"
        }
    .end annotation

    .prologue
    .line 158
    const-class v0, Lorg/joda/money/CurrencyUnit;

    monitor-enter v0

    const/4 v1, 0x0

    :try_start_0
    invoke-static {p0, p1, p2, p3, v1}, Lorg/joda/money/CurrencyUnit;->registerCurrency(Ljava/lang/String;IILjava/util/List;Z)Lorg/joda/money/CurrencyUnit;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    monitor-exit v0

    return-object v1

    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method

.method public static declared-synchronized registerCurrency(Ljava/lang/String;IILjava/util/List;Z)Lorg/joda/money/CurrencyUnit;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "II",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;Z)",
            "Lorg/joda/money/CurrencyUnit;"
        }
    .end annotation

    .prologue
    const/4 v3, -0x1

    .line 191
    const-class v1, Lorg/joda/money/CurrencyUnit;

    monitor-enter v1

    :try_start_0
    const-string v0, "Currency code must not be null"

    invoke-static {p0, v0}, Lorg/joda/money/MoneyUtils;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 192
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    const/4 v2, 0x3

    if-eq v0, v2, :cond_0

    .line 193
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v2, "Invalid string code, must be length 3"

    invoke-direct {v0, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 191
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 195
    :cond_0
    :try_start_1
    sget-object v0, Lorg/joda/money/CurrencyUnit;->CODE:Ljava/util/regex/Pattern;

    invoke-virtual {v0, p0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/regex/Matcher;->matches()Z

    move-result v0

    if-nez v0, :cond_1

    .line 196
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v2, "Invalid string code, must be ASCII upper-case letters"

    invoke-direct {v0, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 198
    :cond_1
    if-lt p1, v3, :cond_2

    const/16 v0, 0x3e7

    if-le p1, v0, :cond_3

    .line 199
    :cond_2
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v2, "Invalid numeric code"

    invoke-direct {v0, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 201
    :cond_3
    if-lt p2, v3, :cond_4

    const/16 v0, 0x9

    if-le p2, v0, :cond_5

    .line 202
    :cond_4
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v2, "Invalid number of decimal places"

    invoke-direct {v0, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 204
    :cond_5
    const-string v0, "Country codes must not be null"

    invoke-static {p3, v0}, Lorg/joda/money/MoneyUtils;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 206
    new-instance v2, Lorg/joda/money/CurrencyUnit;

    int-to-short v0, p1

    int-to-short v3, p2

    invoke-direct {v2, p0, v0, v3}, Lorg/joda/money/CurrencyUnit;-><init>(Ljava/lang/String;SS)V

    .line 207
    if-eqz p4, :cond_6

    .line 208
    sget-object v0, Lorg/joda/money/CurrencyUnit;->currenciesByCode:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v0, p0}, Ljava/util/concurrent/ConcurrentMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 209
    sget-object v0, Lorg/joda/money/CurrencyUnit;->currenciesByNumericCode:Ljava/util/concurrent/ConcurrentMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/concurrent/ConcurrentMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 210
    invoke-interface {p3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_a

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 211
    sget-object v4, Lorg/joda/money/CurrencyUnit;->currenciesByCountry:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v4, v0}, Ljava/util/concurrent/ConcurrentMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 214
    :cond_6
    sget-object v0, Lorg/joda/money/CurrencyUnit;->currenciesByCode:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v0, p0}, Ljava/util/concurrent/ConcurrentMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_7

    sget-object v0, Lorg/joda/money/CurrencyUnit;->currenciesByNumericCode:Ljava/util/concurrent/ConcurrentMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/concurrent/ConcurrentMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 215
    :cond_7
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Currency already registered: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 217
    :cond_8
    invoke-interface {p3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_9
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_a

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 218
    sget-object v4, Lorg/joda/money/CurrencyUnit;->currenciesByCountry:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v4, v0}, Ljava/util/concurrent/ConcurrentMap;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_9

    .line 219
    new-instance v2, Ljava/lang/IllegalArgumentException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Currency already registered for country: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 223
    :cond_a
    sget-object v0, Lorg/joda/money/CurrencyUnit;->currenciesByCode:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v0, p0, v2}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 224
    if-ltz p1, :cond_b

    .line 225
    sget-object v0, Lorg/joda/money/CurrencyUnit;->currenciesByNumericCode:Ljava/util/concurrent/ConcurrentMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v0, v3, v2}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 227
    :cond_b
    invoke-interface {p3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_c

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 228
    sget-object v4, Lorg/joda/money/CurrencyUnit;->currenciesByCountry:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v4, v0, v2}, Ljava/util/concurrent/ConcurrentMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 230
    :cond_c
    sget-object v0, Lorg/joda/money/CurrencyUnit;->currenciesByCode:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v0, p0}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/joda/money/CurrencyUnit;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit v1

    return-object v0
.end method

.method public static registeredCurrencies()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lorg/joda/money/CurrencyUnit;",
            ">;"
        }
    .end annotation

    .prologue
    .line 243
    new-instance v0, Ljava/util/ArrayList;

    sget-object v1, Lorg/joda/money/CurrencyUnit;->currenciesByCode:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v1}, Ljava/util/concurrent/ConcurrentMap;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 244
    invoke-static {v0}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    .line 245
    return-object v0
.end method

.method private writeReplace()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 422
    new-instance v0, Lorg/joda/money/Ser;

    const/16 v1, 0x43

    invoke-direct {v0, v1, p0}, Lorg/joda/money/Ser;-><init>(BLjava/lang/Object;)V

    return-object v0
.end method


# virtual methods
.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 49
    check-cast p1, Lorg/joda/money/CurrencyUnit;

    invoke-virtual {p0, p1}, Lorg/joda/money/CurrencyUnit;->compareTo(Lorg/joda/money/CurrencyUnit;)I

    move-result v0

    return v0
.end method

.method public compareTo(Lorg/joda/money/CurrencyUnit;)I
    .locals 2

    .prologue
    .line 603
    iget-object v0, p0, Lorg/joda/money/CurrencyUnit;->code:Ljava/lang/String;

    iget-object v1, p1, Lorg/joda/money/CurrencyUnit;->code:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 616
    if-ne p1, p0, :cond_0

    .line 617
    const/4 v0, 0x1

    .line 622
    :goto_0
    return v0

    .line 619
    :cond_0
    instance-of v0, p1, Lorg/joda/money/CurrencyUnit;

    if-eqz v0, :cond_1

    .line 620
    iget-object v0, p0, Lorg/joda/money/CurrencyUnit;->code:Ljava/lang/String;

    check-cast p1, Lorg/joda/money/CurrencyUnit;

    iget-object v1, p1, Lorg/joda/money/CurrencyUnit;->code:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0

    .line 622
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getCode()Ljava/lang/String;
    .locals 1

    .prologue
    .line 434
    iget-object v0, p0, Lorg/joda/money/CurrencyUnit;->code:Ljava/lang/String;

    return-object v0
.end method

.method public getCountryCodes()Ljava/util/Set;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 480
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    .line 481
    sget-object v0, Lorg/joda/money/CurrencyUnit;->currenciesByCountry:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v0}, Ljava/util/concurrent/ConcurrentMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 482
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {p0, v3}, Lorg/joda/money/CurrencyUnit;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 483
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 486
    :cond_1
    return-object v1
.end method

.method public getCurrencyCode()Ljava/lang/String;
    .locals 1

    .prologue
    .line 523
    iget-object v0, p0, Lorg/joda/money/CurrencyUnit;->code:Ljava/lang/String;

    return-object v0
.end method

.method public getDecimalPlaces()I
    .locals 1

    .prologue
    .line 502
    iget-short v0, p0, Lorg/joda/money/CurrencyUnit;->decimalPlaces:S

    if-gez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-short v0, p0, Lorg/joda/money/CurrencyUnit;->decimalPlaces:S

    goto :goto_0
.end method

.method public getDefaultFractionDigits()I
    .locals 1

    .prologue
    .line 539
    iget-short v0, p0, Lorg/joda/money/CurrencyUnit;->decimalPlaces:S

    return v0
.end method

.method public getNumeric3Code()Ljava/lang/String;
    .locals 3

    .prologue
    .line 457
    iget-short v0, p0, Lorg/joda/money/CurrencyUnit;->numericCode:S

    if-gez v0, :cond_1

    .line 458
    const-string v0, ""

    .line 467
    :cond_0
    :goto_0
    return-object v0

    .line 460
    :cond_1
    iget-short v0, p0, Lorg/joda/money/CurrencyUnit;->numericCode:S

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    .line 461
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_2

    .line 462
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "00"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 464
    :cond_2
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    const/4 v2, 0x2

    if-ne v1, v2, :cond_0

    .line 465
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "0"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getNumericCode()I
    .locals 1

    .prologue
    .line 445
    iget-short v0, p0, Lorg/joda/money/CurrencyUnit;->numericCode:S

    return v0
.end method

.method public getSymbol()Ljava/lang/String;
    .locals 1

    .prologue
    .line 555
    :try_start_0
    iget-object v0, p0, Lorg/joda/money/CurrencyUnit;->code:Ljava/lang/String;

    invoke-static {v0}, Ljava/util/Currency;->getInstance(Ljava/lang/String;)Ljava/util/Currency;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Currency;->getSymbol()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 557
    :goto_0
    return-object v0

    .line 556
    :catch_0
    move-exception v0

    .line 557
    iget-object v0, p0, Lorg/joda/money/CurrencyUnit;->code:Ljava/lang/String;

    goto :goto_0
.end method

.method public getSymbol(Ljava/util/Locale;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 573
    const-string v0, "Locale must not be null"

    invoke-static {p1, v0}, Lorg/joda/money/MoneyUtils;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 575
    :try_start_0
    iget-object v0, p0, Lorg/joda/money/CurrencyUnit;->code:Ljava/lang/String;

    invoke-static {v0}, Ljava/util/Currency;->getInstance(Ljava/lang/String;)Ljava/util/Currency;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/util/Currency;->getSymbol(Ljava/util/Locale;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 577
    :goto_0
    return-object v0

    .line 576
    :catch_0
    move-exception v0

    .line 577
    iget-object v0, p0, Lorg/joda/money/CurrencyUnit;->code:Ljava/lang/String;

    goto :goto_0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 632
    iget-object v0, p0, Lorg/joda/money/CurrencyUnit;->code:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    return v0
.end method

.method public isPseudoCurrency()Z
    .locals 1

    .prologue
    .line 511
    iget-short v0, p0, Lorg/joda/money/CurrencyUnit;->decimalPlaces:S

    if-gez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public toCurrency()Ljava/util/Currency;
    .locals 1

    .prologue
    .line 591
    iget-object v0, p0, Lorg/joda/money/CurrencyUnit;->code:Ljava/lang/String;

    invoke-static {v0}, Ljava/util/Currency;->getInstance(Ljava/lang/String;)Ljava/util/Currency;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1
    .annotation runtime Lorg/joda/convert/ToString;
    .end annotation

    .prologue
    .line 644
    iget-object v0, p0, Lorg/joda/money/CurrencyUnit;->code:Ljava/lang/String;

    return-object v0
.end method
