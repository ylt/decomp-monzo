.class public final Lorg/joda/money/Money;
.super Ljava/lang/Object;
.source "Money.java"

# interfaces
.implements Ljava/io/Serializable;
.implements Ljava/lang/Comparable;
.implements Lorg/joda/money/BigMoneyProvider;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/io/Serializable;",
        "Ljava/lang/Comparable",
        "<",
        "Lorg/joda/money/BigMoneyProvider;",
        ">;",
        "Lorg/joda/money/BigMoneyProvider;"
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z

.field private static final serialVersionUID:J = 0x1L


# instance fields
.field private final money:Lorg/joda/money/BigMoney;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 46
    const-class v0, Lorg/joda/money/Money;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/joda/money/Money;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 372
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 373
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/joda/money/Money;->money:Lorg/joda/money/BigMoney;

    .line 374
    return-void
.end method

.method constructor <init>(Lorg/joda/money/BigMoney;)V
    .locals 2

    .prologue
    .line 381
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 382
    sget-boolean v0, Lorg/joda/money/Money;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "Joda-Money bug: BigMoney must not be null"

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 383
    :cond_0
    sget-boolean v0, Lorg/joda/money/Money;->$assertionsDisabled:Z

    if-nez v0, :cond_1

    invoke-virtual {p1}, Lorg/joda/money/BigMoney;->isCurrencyScale()Z

    move-result v0

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "Joda-Money bug: Only currency scale is valid for Money"

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 384
    :cond_1
    iput-object p1, p0, Lorg/joda/money/Money;->money:Lorg/joda/money/BigMoney;

    .line 385
    return-void
.end method

.method public static nonNull(Lorg/joda/money/Money;Lorg/joda/money/CurrencyUnit;)Lorg/joda/money/Money;
    .locals 2

    .prologue
    .line 357
    if-nez p0, :cond_1

    .line 358
    invoke-static {p1}, Lorg/joda/money/Money;->zero(Lorg/joda/money/CurrencyUnit;)Lorg/joda/money/Money;

    move-result-object p0

    .line 364
    :cond_0
    return-object p0

    .line 360
    :cond_1
    invoke-virtual {p0}, Lorg/joda/money/Money;->getCurrencyUnit()Lorg/joda/money/CurrencyUnit;

    move-result-object v0

    invoke-virtual {v0, p1}, Lorg/joda/money/CurrencyUnit;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 361
    const-string v0, "Currency must not be null"

    invoke-static {p1, v0}, Lorg/joda/money/MoneyUtils;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 362
    new-instance v0, Lorg/joda/money/CurrencyMismatchException;

    invoke-virtual {p0}, Lorg/joda/money/Money;->getCurrencyUnit()Lorg/joda/money/CurrencyUnit;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Lorg/joda/money/CurrencyMismatchException;-><init>(Lorg/joda/money/CurrencyUnit;Lorg/joda/money/CurrencyUnit;)V

    throw v0
.end method

.method public static of(Lorg/joda/money/BigMoneyProvider;)Lorg/joda/money/Money;
    .locals 1

    .prologue
    .line 212
    sget-object v0, Ljava/math/RoundingMode;->UNNECESSARY:Ljava/math/RoundingMode;

    invoke-static {p0, v0}, Lorg/joda/money/Money;->of(Lorg/joda/money/BigMoneyProvider;Ljava/math/RoundingMode;)Lorg/joda/money/Money;

    move-result-object v0

    return-object v0
.end method

.method public static of(Lorg/joda/money/BigMoneyProvider;Ljava/math/RoundingMode;)Lorg/joda/money/Money;
    .locals 2

    .prologue
    .line 228
    const-string v0, "BigMoneyProvider must not be null"

    invoke-static {p0, v0}, Lorg/joda/money/MoneyUtils;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 229
    const-string v0, "RoundingMode must not be null"

    invoke-static {p1, v0}, Lorg/joda/money/MoneyUtils;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 230
    new-instance v0, Lorg/joda/money/Money;

    invoke-static {p0}, Lorg/joda/money/BigMoney;->of(Lorg/joda/money/BigMoneyProvider;)Lorg/joda/money/BigMoney;

    move-result-object v1

    invoke-virtual {v1, p1}, Lorg/joda/money/BigMoney;->withCurrencyScale(Ljava/math/RoundingMode;)Lorg/joda/money/BigMoney;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/joda/money/Money;-><init>(Lorg/joda/money/BigMoney;)V

    return-object v0
.end method

.method public static of(Lorg/joda/money/CurrencyUnit;D)Lorg/joda/money/Money;
    .locals 1

    .prologue
    .line 122
    invoke-static {p1, p2}, Ljava/math/BigDecimal;->valueOf(D)Ljava/math/BigDecimal;

    move-result-object v0

    invoke-static {p0, v0}, Lorg/joda/money/Money;->of(Lorg/joda/money/CurrencyUnit;Ljava/math/BigDecimal;)Lorg/joda/money/Money;

    move-result-object v0

    return-object v0
.end method

.method public static of(Lorg/joda/money/CurrencyUnit;DLjava/math/RoundingMode;)Lorg/joda/money/Money;
    .locals 1

    .prologue
    .line 146
    invoke-static {p1, p2}, Ljava/math/BigDecimal;->valueOf(D)Ljava/math/BigDecimal;

    move-result-object v0

    invoke-static {p0, v0, p3}, Lorg/joda/money/Money;->of(Lorg/joda/money/CurrencyUnit;Ljava/math/BigDecimal;Ljava/math/RoundingMode;)Lorg/joda/money/Money;

    move-result-object v0

    return-object v0
.end method

.method public static of(Lorg/joda/money/CurrencyUnit;Ljava/math/BigDecimal;)Lorg/joda/money/Money;
    .locals 3

    .prologue
    .line 72
    const-string v0, "Currency must not be null"

    invoke-static {p0, v0}, Lorg/joda/money/MoneyUtils;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 73
    const-string v0, "Amount must not be null"

    invoke-static {p1, v0}, Lorg/joda/money/MoneyUtils;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 74
    invoke-virtual {p1}, Ljava/math/BigDecimal;->scale()I

    move-result v0

    invoke-virtual {p0}, Lorg/joda/money/CurrencyUnit;->getDecimalPlaces()I

    move-result v1

    if-le v0, v1, :cond_0

    .line 75
    new-instance v0, Ljava/lang/ArithmeticException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Scale of amount "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is greater than the scale of the currency "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/ArithmeticException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 77
    :cond_0
    sget-object v0, Ljava/math/RoundingMode;->UNNECESSARY:Ljava/math/RoundingMode;

    invoke-static {p0, p1, v0}, Lorg/joda/money/Money;->of(Lorg/joda/money/CurrencyUnit;Ljava/math/BigDecimal;Ljava/math/RoundingMode;)Lorg/joda/money/Money;

    move-result-object v0

    return-object v0
.end method

.method public static of(Lorg/joda/money/CurrencyUnit;Ljava/math/BigDecimal;Ljava/math/RoundingMode;)Lorg/joda/money/Money;
    .locals 2

    .prologue
    .line 94
    const-string v0, "CurrencyUnit must not be null"

    invoke-static {p0, v0}, Lorg/joda/money/MoneyUtils;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 95
    const-string v0, "Amount must not be null"

    invoke-static {p1, v0}, Lorg/joda/money/MoneyUtils;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 96
    const-string v0, "RoundingMode must not be null"

    invoke-static {p2, v0}, Lorg/joda/money/MoneyUtils;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 97
    invoke-virtual {p0}, Lorg/joda/money/CurrencyUnit;->getDecimalPlaces()I

    move-result v0

    invoke-virtual {p1, v0, p2}, Ljava/math/BigDecimal;->setScale(ILjava/math/RoundingMode;)Ljava/math/BigDecimal;

    move-result-object v0

    .line 98
    new-instance v1, Lorg/joda/money/Money;

    invoke-static {p0, v0}, Lorg/joda/money/BigMoney;->of(Lorg/joda/money/CurrencyUnit;Ljava/math/BigDecimal;)Lorg/joda/money/BigMoney;

    move-result-object v0

    invoke-direct {v1, v0}, Lorg/joda/money/Money;-><init>(Lorg/joda/money/BigMoney;)V

    return-object v1
.end method

.method public static ofMajor(Lorg/joda/money/CurrencyUnit;J)Lorg/joda/money/Money;
    .locals 3

    .prologue
    .line 163
    invoke-static {p1, p2}, Ljava/math/BigDecimal;->valueOf(J)Ljava/math/BigDecimal;

    move-result-object v0

    sget-object v1, Ljava/math/RoundingMode;->UNNECESSARY:Ljava/math/RoundingMode;

    invoke-static {p0, v0, v1}, Lorg/joda/money/Money;->of(Lorg/joda/money/CurrencyUnit;Ljava/math/BigDecimal;Ljava/math/RoundingMode;)Lorg/joda/money/Money;

    move-result-object v0

    return-object v0
.end method

.method public static ofMinor(Lorg/joda/money/CurrencyUnit;J)Lorg/joda/money/Money;
    .locals 3

    .prologue
    .line 180
    new-instance v0, Lorg/joda/money/Money;

    invoke-static {p0, p1, p2}, Lorg/joda/money/BigMoney;->ofMinor(Lorg/joda/money/CurrencyUnit;J)Lorg/joda/money/BigMoney;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/joda/money/Money;-><init>(Lorg/joda/money/BigMoney;)V

    return-object v0
.end method

.method public static parse(Ljava/lang/String;)Lorg/joda/money/Money;
    .locals 1
    .annotation runtime Lorg/joda/convert/FromString;
    .end annotation

    .prologue
    .line 340
    invoke-static {p0}, Lorg/joda/money/BigMoney;->parse(Ljava/lang/String;)Lorg/joda/money/BigMoney;

    move-result-object v0

    invoke-static {v0}, Lorg/joda/money/Money;->of(Lorg/joda/money/BigMoneyProvider;)Lorg/joda/money/Money;

    move-result-object v0

    return-object v0
.end method

.method private readObject(Ljava/io/ObjectInputStream;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/InvalidObjectException;
        }
    .end annotation

    .prologue
    .line 394
    new-instance v0, Ljava/io/InvalidObjectException;

    const-string v1, "Serialization delegate required"

    invoke-direct {v0, v1}, Ljava/io/InvalidObjectException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static total(Ljava/lang/Iterable;)Lorg/joda/money/Money;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<",
            "Lorg/joda/money/Money;",
            ">;)",
            "Lorg/joda/money/Money;"
        }
    .end annotation

    .prologue
    .line 272
    const-string v0, "Money iterator must not be null"

    invoke-static {p0, v0}, Lorg/joda/money/MoneyUtils;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 273
    invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 274
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_0

    .line 275
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Money iterator must not be empty"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 277
    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/joda/money/Money;

    .line 278
    const-string v1, "Money iterator must not contain null entries"

    invoke-static {v0, v1}, Lorg/joda/money/MoneyUtils;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v1, v0

    .line 279
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 280
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/joda/money/Money;

    invoke-virtual {v1, v0}, Lorg/joda/money/Money;->plus(Lorg/joda/money/Money;)Lorg/joda/money/Money;

    move-result-object v0

    move-object v1, v0

    goto :goto_0

    .line 282
    :cond_1
    return-object v1
.end method

.method public static total(Lorg/joda/money/CurrencyUnit;Ljava/lang/Iterable;)Lorg/joda/money/Money;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/joda/money/CurrencyUnit;",
            "Ljava/lang/Iterable",
            "<",
            "Lorg/joda/money/Money;",
            ">;)",
            "Lorg/joda/money/Money;"
        }
    .end annotation

    .prologue
    .line 316
    invoke-static {p0}, Lorg/joda/money/Money;->zero(Lorg/joda/money/CurrencyUnit;)Lorg/joda/money/Money;

    move-result-object v0

    invoke-virtual {v0, p1}, Lorg/joda/money/Money;->plus(Ljava/lang/Iterable;)Lorg/joda/money/Money;

    move-result-object v0

    return-object v0
.end method

.method public static varargs total(Lorg/joda/money/CurrencyUnit;[Lorg/joda/money/Money;)Lorg/joda/money/Money;
    .locals 2

    .prologue
    .line 299
    invoke-static {p0}, Lorg/joda/money/Money;->zero(Lorg/joda/money/CurrencyUnit;)Lorg/joda/money/Money;

    move-result-object v0

    invoke-static {p1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/joda/money/Money;->plus(Ljava/lang/Iterable;)Lorg/joda/money/Money;

    move-result-object v0

    return-object v0
.end method

.method public static varargs total([Lorg/joda/money/Money;)Lorg/joda/money/Money;
    .locals 3

    .prologue
    .line 247
    const-string v0, "Money array must not be null"

    invoke-static {p0, v0}, Lorg/joda/money/MoneyUtils;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 248
    array-length v0, p0

    if-nez v0, :cond_0

    .line 249
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Money array must not be empty"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 251
    :cond_0
    const/4 v0, 0x0

    aget-object v1, p0, v0

    .line 252
    const-string v0, "Money arary must not contain null entries"

    invoke-static {v1, v0}, Lorg/joda/money/MoneyUtils;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 253
    const/4 v0, 0x1

    :goto_0
    array-length v2, p0

    if-ge v0, v2, :cond_1

    .line 254
    aget-object v2, p0, v0

    invoke-virtual {v1, v2}, Lorg/joda/money/Money;->plus(Lorg/joda/money/Money;)Lorg/joda/money/Money;

    move-result-object v1

    .line 253
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 256
    :cond_1
    return-object v1
.end method

.method private with(Lorg/joda/money/BigMoney;)Lorg/joda/money/Money;
    .locals 1

    .prologue
    .line 416
    iget-object v0, p0, Lorg/joda/money/Money;->money:Lorg/joda/money/BigMoney;

    invoke-virtual {v0, p1}, Lorg/joda/money/BigMoney;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 419
    :goto_0
    return-object p0

    :cond_0
    new-instance p0, Lorg/joda/money/Money;

    invoke-direct {p0, p1}, Lorg/joda/money/Money;-><init>(Lorg/joda/money/BigMoney;)V

    goto :goto_0
.end method

.method private writeReplace()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 403
    new-instance v0, Lorg/joda/money/Ser;

    const/16 v1, 0x4d

    invoke-direct {v0, v1, p0}, Lorg/joda/money/Ser;-><init>(BLjava/lang/Object;)V

    return-object v0
.end method

.method public static zero(Lorg/joda/money/CurrencyUnit;)Lorg/joda/money/Money;
    .locals 3

    .prologue
    .line 193
    const-string v0, "Currency must not be null"

    invoke-static {p0, v0}, Lorg/joda/money/MoneyUtils;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 194
    const-wide/16 v0, 0x0

    invoke-virtual {p0}, Lorg/joda/money/CurrencyUnit;->getDecimalPlaces()I

    move-result v2

    invoke-static {v0, v1, v2}, Ljava/math/BigDecimal;->valueOf(JI)Ljava/math/BigDecimal;

    move-result-object v0

    .line 195
    new-instance v1, Lorg/joda/money/Money;

    invoke-static {p0, v0}, Lorg/joda/money/BigMoney;->of(Lorg/joda/money/CurrencyUnit;Ljava/math/BigDecimal;)Lorg/joda/money/BigMoney;

    move-result-object v0

    invoke-direct {v1, v0}, Lorg/joda/money/Money;-><init>(Lorg/joda/money/BigMoney;)V

    return-object v1
.end method


# virtual methods
.method public abs()Lorg/joda/money/Money;
    .locals 1

    .prologue
    .line 1164
    invoke-virtual {p0}, Lorg/joda/money/Money;->isNegative()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lorg/joda/money/Money;->negated()Lorg/joda/money/Money;

    move-result-object p0

    :cond_0
    return-object p0
.end method

.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 46
    check-cast p1, Lorg/joda/money/BigMoneyProvider;

    invoke-virtual {p0, p1}, Lorg/joda/money/Money;->compareTo(Lorg/joda/money/BigMoneyProvider;)I

    move-result v0

    return v0
.end method

.method public compareTo(Lorg/joda/money/BigMoneyProvider;)I
    .locals 1

    .prologue
    .line 1250
    iget-object v0, p0, Lorg/joda/money/Money;->money:Lorg/joda/money/BigMoney;

    invoke-virtual {v0, p1}, Lorg/joda/money/BigMoney;->compareTo(Lorg/joda/money/BigMoneyProvider;)I

    move-result v0

    return v0
.end method

.method public convertedTo(Lorg/joda/money/CurrencyUnit;Ljava/math/BigDecimal;Ljava/math/RoundingMode;)Lorg/joda/money/Money;
    .locals 1

    .prologue
    .line 1210
    iget-object v0, p0, Lorg/joda/money/Money;->money:Lorg/joda/money/BigMoney;

    invoke-virtual {v0, p1, p2}, Lorg/joda/money/BigMoney;->convertedTo(Lorg/joda/money/CurrencyUnit;Ljava/math/BigDecimal;)Lorg/joda/money/BigMoney;

    move-result-object v0

    invoke-virtual {v0, p3}, Lorg/joda/money/BigMoney;->withCurrencyScale(Ljava/math/RoundingMode;)Lorg/joda/money/BigMoney;

    move-result-object v0

    invoke-direct {p0, v0}, Lorg/joda/money/Money;->with(Lorg/joda/money/BigMoney;)Lorg/joda/money/Money;

    move-result-object v0

    return-object v0
.end method

.method public dividedBy(DLjava/math/RoundingMode;)Lorg/joda/money/Money;
    .locals 1

    .prologue
    .line 1123
    iget-object v0, p0, Lorg/joda/money/Money;->money:Lorg/joda/money/BigMoney;

    invoke-virtual {v0, p1, p2, p3}, Lorg/joda/money/BigMoney;->dividedBy(DLjava/math/RoundingMode;)Lorg/joda/money/BigMoney;

    move-result-object v0

    invoke-direct {p0, v0}, Lorg/joda/money/Money;->with(Lorg/joda/money/BigMoney;)Lorg/joda/money/Money;

    move-result-object v0

    return-object v0
.end method

.method public dividedBy(JLjava/math/RoundingMode;)Lorg/joda/money/Money;
    .locals 1

    .prologue
    .line 1141
    iget-object v0, p0, Lorg/joda/money/Money;->money:Lorg/joda/money/BigMoney;

    invoke-virtual {v0, p1, p2, p3}, Lorg/joda/money/BigMoney;->dividedBy(JLjava/math/RoundingMode;)Lorg/joda/money/BigMoney;

    move-result-object v0

    invoke-direct {p0, v0}, Lorg/joda/money/Money;->with(Lorg/joda/money/BigMoney;)Lorg/joda/money/Money;

    move-result-object v0

    return-object v0
.end method

.method public dividedBy(Ljava/math/BigDecimal;Ljava/math/RoundingMode;)Lorg/joda/money/Money;
    .locals 1

    .prologue
    .line 1099
    iget-object v0, p0, Lorg/joda/money/Money;->money:Lorg/joda/money/BigMoney;

    invoke-virtual {v0, p1, p2}, Lorg/joda/money/BigMoney;->dividedBy(Ljava/math/BigDecimal;Ljava/math/RoundingMode;)Lorg/joda/money/BigMoney;

    move-result-object v0

    invoke-direct {p0, v0}, Lorg/joda/money/Money;->with(Lorg/joda/money/BigMoney;)Lorg/joda/money/Money;

    move-result-object v0

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 1311
    if-ne p0, p1, :cond_0

    .line 1312
    const/4 v0, 0x1

    .line 1318
    :goto_0
    return v0

    .line 1314
    :cond_0
    instance-of v0, p1, Lorg/joda/money/Money;

    if-eqz v0, :cond_1

    .line 1315
    check-cast p1, Lorg/joda/money/Money;

    .line 1316
    iget-object v0, p0, Lorg/joda/money/Money;->money:Lorg/joda/money/BigMoney;

    iget-object v1, p1, Lorg/joda/money/Money;->money:Lorg/joda/money/BigMoney;

    invoke-virtual {v0, v1}, Lorg/joda/money/BigMoney;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0

    .line 1318
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getAmount()Ljava/math/BigDecimal;
    .locals 1

    .prologue
    .line 496
    iget-object v0, p0, Lorg/joda/money/Money;->money:Lorg/joda/money/BigMoney;

    invoke-virtual {v0}, Lorg/joda/money/BigMoney;->getAmount()Ljava/math/BigDecimal;

    move-result-object v0

    return-object v0
.end method

.method public getAmountMajor()Ljava/math/BigDecimal;
    .locals 1

    .prologue
    .line 513
    iget-object v0, p0, Lorg/joda/money/Money;->money:Lorg/joda/money/BigMoney;

    invoke-virtual {v0}, Lorg/joda/money/BigMoney;->getAmountMajor()Ljava/math/BigDecimal;

    move-result-object v0

    return-object v0
.end method

.method public getAmountMajorInt()I
    .locals 1

    .prologue
    .line 541
    iget-object v0, p0, Lorg/joda/money/Money;->money:Lorg/joda/money/BigMoney;

    invoke-virtual {v0}, Lorg/joda/money/BigMoney;->getAmountMajorInt()I

    move-result v0

    return v0
.end method

.method public getAmountMajorLong()J
    .locals 2

    .prologue
    .line 527
    iget-object v0, p0, Lorg/joda/money/Money;->money:Lorg/joda/money/BigMoney;

    invoke-virtual {v0}, Lorg/joda/money/BigMoney;->getAmountMajorLong()J

    move-result-wide v0

    return-wide v0
.end method

.method public getAmountMinor()Ljava/math/BigDecimal;
    .locals 1

    .prologue
    .line 558
    iget-object v0, p0, Lorg/joda/money/Money;->money:Lorg/joda/money/BigMoney;

    invoke-virtual {v0}, Lorg/joda/money/BigMoney;->getAmountMinor()Ljava/math/BigDecimal;

    move-result-object v0

    return-object v0
.end method

.method public getAmountMinorInt()I
    .locals 1

    .prologue
    .line 586
    iget-object v0, p0, Lorg/joda/money/Money;->money:Lorg/joda/money/BigMoney;

    invoke-virtual {v0}, Lorg/joda/money/BigMoney;->getAmountMinorInt()I

    move-result v0

    return v0
.end method

.method public getAmountMinorLong()J
    .locals 2

    .prologue
    .line 572
    iget-object v0, p0, Lorg/joda/money/Money;->money:Lorg/joda/money/BigMoney;

    invoke-virtual {v0}, Lorg/joda/money/BigMoney;->getAmountMinorLong()J

    move-result-wide v0

    return-wide v0
.end method

.method public getCurrencyUnit()Lorg/joda/money/CurrencyUnit;
    .locals 1

    .prologue
    .line 429
    iget-object v0, p0, Lorg/joda/money/Money;->money:Lorg/joda/money/BigMoney;

    invoke-virtual {v0}, Lorg/joda/money/BigMoney;->getCurrencyUnit()Lorg/joda/money/CurrencyUnit;

    move-result-object v0

    return-object v0
.end method

.method public getMinorPart()I
    .locals 1

    .prologue
    .line 602
    iget-object v0, p0, Lorg/joda/money/Money;->money:Lorg/joda/money/BigMoney;

    invoke-virtual {v0}, Lorg/joda/money/BigMoney;->getMinorPart()I

    move-result v0

    return v0
.end method

.method public getScale()I
    .locals 1

    .prologue
    .line 483
    iget-object v0, p0, Lorg/joda/money/Money;->money:Lorg/joda/money/BigMoney;

    invoke-virtual {v0}, Lorg/joda/money/BigMoney;->getScale()I

    move-result v0

    return v0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 1328
    iget-object v0, p0, Lorg/joda/money/Money;->money:Lorg/joda/money/BigMoney;

    invoke-virtual {v0}, Lorg/joda/money/BigMoney;->hashCode()I

    move-result v0

    add-int/lit8 v0, v0, 0x3

    return v0
.end method

.method public isEqual(Lorg/joda/money/BigMoneyProvider;)Z
    .locals 1

    .prologue
    .line 1266
    iget-object v0, p0, Lorg/joda/money/Money;->money:Lorg/joda/money/BigMoney;

    invoke-virtual {v0, p1}, Lorg/joda/money/BigMoney;->isEqual(Lorg/joda/money/BigMoneyProvider;)Z

    move-result v0

    return v0
.end method

.method public isGreaterThan(Lorg/joda/money/BigMoneyProvider;)Z
    .locals 1

    .prologue
    .line 1281
    iget-object v0, p0, Lorg/joda/money/Money;->money:Lorg/joda/money/BigMoney;

    invoke-virtual {v0, p1}, Lorg/joda/money/BigMoney;->isGreaterThan(Lorg/joda/money/BigMoneyProvider;)Z

    move-result v0

    return v0
.end method

.method public isLessThan(Lorg/joda/money/BigMoneyProvider;)Z
    .locals 1

    .prologue
    .line 1296
    iget-object v0, p0, Lorg/joda/money/Money;->money:Lorg/joda/money/BigMoney;

    invoke-virtual {v0, p1}, Lorg/joda/money/BigMoney;->isLessThan(Lorg/joda/money/BigMoneyProvider;)Z

    move-result v0

    return v0
.end method

.method public isNegative()Z
    .locals 1

    .prologue
    .line 639
    iget-object v0, p0, Lorg/joda/money/Money;->money:Lorg/joda/money/BigMoney;

    invoke-virtual {v0}, Lorg/joda/money/BigMoney;->isNegative()Z

    move-result v0

    return v0
.end method

.method public isNegativeOrZero()Z
    .locals 1

    .prologue
    .line 648
    iget-object v0, p0, Lorg/joda/money/Money;->money:Lorg/joda/money/BigMoney;

    invoke-virtual {v0}, Lorg/joda/money/BigMoney;->isNegativeOrZero()Z

    move-result v0

    return v0
.end method

.method public isPositive()Z
    .locals 1

    .prologue
    .line 621
    iget-object v0, p0, Lorg/joda/money/Money;->money:Lorg/joda/money/BigMoney;

    invoke-virtual {v0}, Lorg/joda/money/BigMoney;->isPositive()Z

    move-result v0

    return v0
.end method

.method public isPositiveOrZero()Z
    .locals 1

    .prologue
    .line 630
    iget-object v0, p0, Lorg/joda/money/Money;->money:Lorg/joda/money/BigMoney;

    invoke-virtual {v0}, Lorg/joda/money/BigMoney;->isPositiveOrZero()Z

    move-result v0

    return v0
.end method

.method public isSameCurrency(Lorg/joda/money/BigMoneyProvider;)Z
    .locals 1

    .prologue
    .line 1233
    iget-object v0, p0, Lorg/joda/money/Money;->money:Lorg/joda/money/BigMoney;

    invoke-virtual {v0, p1}, Lorg/joda/money/BigMoney;->isSameCurrency(Lorg/joda/money/BigMoneyProvider;)Z

    move-result v0

    return v0
.end method

.method public isZero()Z
    .locals 1

    .prologue
    .line 612
    iget-object v0, p0, Lorg/joda/money/Money;->money:Lorg/joda/money/BigMoney;

    invoke-virtual {v0}, Lorg/joda/money/BigMoney;->isZero()Z

    move-result v0

    return v0
.end method

.method public minus(D)Lorg/joda/money/Money;
    .locals 1

    .prologue
    .line 972
    sget-object v0, Ljava/math/RoundingMode;->UNNECESSARY:Ljava/math/RoundingMode;

    invoke-virtual {p0, p1, p2, v0}, Lorg/joda/money/Money;->minus(DLjava/math/RoundingMode;)Lorg/joda/money/Money;

    move-result-object v0

    return-object v0
.end method

.method public minus(DLjava/math/RoundingMode;)Lorg/joda/money/Money;
    .locals 1

    .prologue
    .line 995
    iget-object v0, p0, Lorg/joda/money/Money;->money:Lorg/joda/money/BigMoney;

    invoke-virtual {v0, p1, p2, p3}, Lorg/joda/money/BigMoney;->minusRetainScale(DLjava/math/RoundingMode;)Lorg/joda/money/BigMoney;

    move-result-object v0

    invoke-direct {p0, v0}, Lorg/joda/money/Money;->with(Lorg/joda/money/BigMoney;)Lorg/joda/money/Money;

    move-result-object v0

    return-object v0
.end method

.method public minus(Ljava/lang/Iterable;)Lorg/joda/money/Money;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<",
            "Lorg/joda/money/Money;",
            ">;)",
            "Lorg/joda/money/Money;"
        }
    .end annotation

    .prologue
    .line 895
    iget-object v0, p0, Lorg/joda/money/Money;->money:Lorg/joda/money/BigMoney;

    invoke-virtual {v0, p1}, Lorg/joda/money/BigMoney;->minus(Ljava/lang/Iterable;)Lorg/joda/money/BigMoney;

    move-result-object v0

    invoke-direct {p0, v0}, Lorg/joda/money/Money;->with(Lorg/joda/money/BigMoney;)Lorg/joda/money/Money;

    move-result-object v0

    return-object v0
.end method

.method public minus(Ljava/math/BigDecimal;)Lorg/joda/money/Money;
    .locals 1

    .prologue
    .line 932
    sget-object v0, Ljava/math/RoundingMode;->UNNECESSARY:Ljava/math/RoundingMode;

    invoke-virtual {p0, p1, v0}, Lorg/joda/money/Money;->minus(Ljava/math/BigDecimal;Ljava/math/RoundingMode;)Lorg/joda/money/Money;

    move-result-object v0

    return-object v0
.end method

.method public minus(Ljava/math/BigDecimal;Ljava/math/RoundingMode;)Lorg/joda/money/Money;
    .locals 1

    .prologue
    .line 949
    iget-object v0, p0, Lorg/joda/money/Money;->money:Lorg/joda/money/BigMoney;

    invoke-virtual {v0, p1, p2}, Lorg/joda/money/BigMoney;->minusRetainScale(Ljava/math/BigDecimal;Ljava/math/RoundingMode;)Lorg/joda/money/BigMoney;

    move-result-object v0

    invoke-direct {p0, v0}, Lorg/joda/money/Money;->with(Lorg/joda/money/BigMoney;)Lorg/joda/money/Money;

    move-result-object v0

    return-object v0
.end method

.method public minus(Lorg/joda/money/Money;)Lorg/joda/money/Money;
    .locals 1

    .prologue
    .line 915
    iget-object v0, p0, Lorg/joda/money/Money;->money:Lorg/joda/money/BigMoney;

    invoke-virtual {v0, p1}, Lorg/joda/money/BigMoney;->minus(Lorg/joda/money/BigMoneyProvider;)Lorg/joda/money/BigMoney;

    move-result-object v0

    invoke-direct {p0, v0}, Lorg/joda/money/Money;->with(Lorg/joda/money/BigMoney;)Lorg/joda/money/Money;

    move-result-object v0

    return-object v0
.end method

.method public minusMajor(J)Lorg/joda/money/Money;
    .locals 1

    .prologue
    .line 1010
    iget-object v0, p0, Lorg/joda/money/Money;->money:Lorg/joda/money/BigMoney;

    invoke-virtual {v0, p1, p2}, Lorg/joda/money/BigMoney;->minusMajor(J)Lorg/joda/money/BigMoney;

    move-result-object v0

    invoke-direct {p0, v0}, Lorg/joda/money/Money;->with(Lorg/joda/money/BigMoney;)Lorg/joda/money/Money;

    move-result-object v0

    return-object v0
.end method

.method public minusMinor(J)Lorg/joda/money/Money;
    .locals 1

    .prologue
    .line 1025
    iget-object v0, p0, Lorg/joda/money/Money;->money:Lorg/joda/money/BigMoney;

    invoke-virtual {v0, p1, p2}, Lorg/joda/money/BigMoney;->minusMinor(J)Lorg/joda/money/BigMoney;

    move-result-object v0

    invoke-direct {p0, v0}, Lorg/joda/money/Money;->with(Lorg/joda/money/BigMoney;)Lorg/joda/money/Money;

    move-result-object v0

    return-object v0
.end method

.method public multipliedBy(DLjava/math/RoundingMode;)Lorg/joda/money/Money;
    .locals 1

    .prologue
    .line 1066
    iget-object v0, p0, Lorg/joda/money/Money;->money:Lorg/joda/money/BigMoney;

    invoke-virtual {v0, p1, p2, p3}, Lorg/joda/money/BigMoney;->multiplyRetainScale(DLjava/math/RoundingMode;)Lorg/joda/money/BigMoney;

    move-result-object v0

    invoke-direct {p0, v0}, Lorg/joda/money/Money;->with(Lorg/joda/money/BigMoney;)Lorg/joda/money/Money;

    move-result-object v0

    return-object v0
.end method

.method public multipliedBy(J)Lorg/joda/money/Money;
    .locals 1

    .prologue
    .line 1080
    iget-object v0, p0, Lorg/joda/money/Money;->money:Lorg/joda/money/BigMoney;

    invoke-virtual {v0, p1, p2}, Lorg/joda/money/BigMoney;->multipliedBy(J)Lorg/joda/money/BigMoney;

    move-result-object v0

    invoke-direct {p0, v0}, Lorg/joda/money/Money;->with(Lorg/joda/money/BigMoney;)Lorg/joda/money/Money;

    move-result-object v0

    return-object v0
.end method

.method public multipliedBy(Ljava/math/BigDecimal;Ljava/math/RoundingMode;)Lorg/joda/money/Money;
    .locals 1

    .prologue
    .line 1043
    iget-object v0, p0, Lorg/joda/money/Money;->money:Lorg/joda/money/BigMoney;

    invoke-virtual {v0, p1, p2}, Lorg/joda/money/BigMoney;->multiplyRetainScale(Ljava/math/BigDecimal;Ljava/math/RoundingMode;)Lorg/joda/money/BigMoney;

    move-result-object v0

    invoke-direct {p0, v0}, Lorg/joda/money/Money;->with(Lorg/joda/money/BigMoney;)Lorg/joda/money/Money;

    move-result-object v0

    return-object v0
.end method

.method public negated()Lorg/joda/money/Money;
    .locals 1

    .prologue
    .line 1153
    iget-object v0, p0, Lorg/joda/money/Money;->money:Lorg/joda/money/BigMoney;

    invoke-virtual {v0}, Lorg/joda/money/BigMoney;->negated()Lorg/joda/money/BigMoney;

    move-result-object v0

    invoke-direct {p0, v0}, Lorg/joda/money/Money;->with(Lorg/joda/money/BigMoney;)Lorg/joda/money/Money;

    move-result-object v0

    return-object v0
.end method

.method public plus(D)Lorg/joda/money/Money;
    .locals 1

    .prologue
    .line 825
    sget-object v0, Ljava/math/RoundingMode;->UNNECESSARY:Ljava/math/RoundingMode;

    invoke-virtual {p0, p1, p2, v0}, Lorg/joda/money/Money;->plus(DLjava/math/RoundingMode;)Lorg/joda/money/Money;

    move-result-object v0

    return-object v0
.end method

.method public plus(DLjava/math/RoundingMode;)Lorg/joda/money/Money;
    .locals 1

    .prologue
    .line 848
    iget-object v0, p0, Lorg/joda/money/Money;->money:Lorg/joda/money/BigMoney;

    invoke-virtual {v0, p1, p2, p3}, Lorg/joda/money/BigMoney;->plusRetainScale(DLjava/math/RoundingMode;)Lorg/joda/money/BigMoney;

    move-result-object v0

    invoke-direct {p0, v0}, Lorg/joda/money/Money;->with(Lorg/joda/money/BigMoney;)Lorg/joda/money/Money;

    move-result-object v0

    return-object v0
.end method

.method public plus(Ljava/lang/Iterable;)Lorg/joda/money/Money;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<",
            "Lorg/joda/money/Money;",
            ">;)",
            "Lorg/joda/money/Money;"
        }
    .end annotation

    .prologue
    .line 748
    iget-object v0, p0, Lorg/joda/money/Money;->money:Lorg/joda/money/BigMoney;

    invoke-virtual {v0, p1}, Lorg/joda/money/BigMoney;->plus(Ljava/lang/Iterable;)Lorg/joda/money/BigMoney;

    move-result-object v0

    invoke-direct {p0, v0}, Lorg/joda/money/Money;->with(Lorg/joda/money/BigMoney;)Lorg/joda/money/Money;

    move-result-object v0

    return-object v0
.end method

.method public plus(Ljava/math/BigDecimal;)Lorg/joda/money/Money;
    .locals 1

    .prologue
    .line 785
    sget-object v0, Ljava/math/RoundingMode;->UNNECESSARY:Ljava/math/RoundingMode;

    invoke-virtual {p0, p1, v0}, Lorg/joda/money/Money;->plus(Ljava/math/BigDecimal;Ljava/math/RoundingMode;)Lorg/joda/money/Money;

    move-result-object v0

    return-object v0
.end method

.method public plus(Ljava/math/BigDecimal;Ljava/math/RoundingMode;)Lorg/joda/money/Money;
    .locals 1

    .prologue
    .line 802
    iget-object v0, p0, Lorg/joda/money/Money;->money:Lorg/joda/money/BigMoney;

    invoke-virtual {v0, p1, p2}, Lorg/joda/money/BigMoney;->plusRetainScale(Ljava/math/BigDecimal;Ljava/math/RoundingMode;)Lorg/joda/money/BigMoney;

    move-result-object v0

    invoke-direct {p0, v0}, Lorg/joda/money/Money;->with(Lorg/joda/money/BigMoney;)Lorg/joda/money/Money;

    move-result-object v0

    return-object v0
.end method

.method public plus(Lorg/joda/money/Money;)Lorg/joda/money/Money;
    .locals 1

    .prologue
    .line 768
    iget-object v0, p0, Lorg/joda/money/Money;->money:Lorg/joda/money/BigMoney;

    invoke-virtual {v0, p1}, Lorg/joda/money/BigMoney;->plus(Lorg/joda/money/BigMoneyProvider;)Lorg/joda/money/BigMoney;

    move-result-object v0

    invoke-direct {p0, v0}, Lorg/joda/money/Money;->with(Lorg/joda/money/BigMoney;)Lorg/joda/money/Money;

    move-result-object v0

    return-object v0
.end method

.method public plusMajor(J)Lorg/joda/money/Money;
    .locals 1

    .prologue
    .line 863
    iget-object v0, p0, Lorg/joda/money/Money;->money:Lorg/joda/money/BigMoney;

    invoke-virtual {v0, p1, p2}, Lorg/joda/money/BigMoney;->plusMajor(J)Lorg/joda/money/BigMoney;

    move-result-object v0

    invoke-direct {p0, v0}, Lorg/joda/money/Money;->with(Lorg/joda/money/BigMoney;)Lorg/joda/money/Money;

    move-result-object v0

    return-object v0
.end method

.method public plusMinor(J)Lorg/joda/money/Money;
    .locals 1

    .prologue
    .line 878
    iget-object v0, p0, Lorg/joda/money/Money;->money:Lorg/joda/money/BigMoney;

    invoke-virtual {v0, p1, p2}, Lorg/joda/money/BigMoney;->plusMinor(J)Lorg/joda/money/BigMoney;

    move-result-object v0

    invoke-direct {p0, v0}, Lorg/joda/money/Money;->with(Lorg/joda/money/BigMoney;)Lorg/joda/money/Money;

    move-result-object v0

    return-object v0
.end method

.method public rounded(ILjava/math/RoundingMode;)Lorg/joda/money/Money;
    .locals 1

    .prologue
    .line 1190
    iget-object v0, p0, Lorg/joda/money/Money;->money:Lorg/joda/money/BigMoney;

    invoke-virtual {v0, p1, p2}, Lorg/joda/money/BigMoney;->rounded(ILjava/math/RoundingMode;)Lorg/joda/money/BigMoney;

    move-result-object v0

    invoke-direct {p0, v0}, Lorg/joda/money/Money;->with(Lorg/joda/money/BigMoney;)Lorg/joda/money/Money;

    move-result-object v0

    return-object v0
.end method

.method public toBigMoney()Lorg/joda/money/BigMoney;
    .locals 1

    .prologue
    .line 1222
    iget-object v0, p0, Lorg/joda/money/Money;->money:Lorg/joda/money/BigMoney;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1
    .annotation runtime Lorg/joda/convert/ToString;
    .end annotation

    .prologue
    .line 1343
    iget-object v0, p0, Lorg/joda/money/Money;->money:Lorg/joda/money/BigMoney;

    invoke-virtual {v0}, Lorg/joda/money/BigMoney;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public withAmount(D)Lorg/joda/money/Money;
    .locals 1

    .prologue
    .line 707
    sget-object v0, Ljava/math/RoundingMode;->UNNECESSARY:Ljava/math/RoundingMode;

    invoke-virtual {p0, p1, p2, v0}, Lorg/joda/money/Money;->withAmount(DLjava/math/RoundingMode;)Lorg/joda/money/Money;

    move-result-object v0

    return-object v0
.end method

.method public withAmount(DLjava/math/RoundingMode;)Lorg/joda/money/Money;
    .locals 1

    .prologue
    .line 731
    iget-object v0, p0, Lorg/joda/money/Money;->money:Lorg/joda/money/BigMoney;

    invoke-virtual {v0, p1, p2}, Lorg/joda/money/BigMoney;->withAmount(D)Lorg/joda/money/BigMoney;

    move-result-object v0

    invoke-virtual {v0, p3}, Lorg/joda/money/BigMoney;->withCurrencyScale(Ljava/math/RoundingMode;)Lorg/joda/money/BigMoney;

    move-result-object v0

    invoke-direct {p0, v0}, Lorg/joda/money/Money;->with(Lorg/joda/money/BigMoney;)Lorg/joda/money/Money;

    move-result-object v0

    return-object v0
.end method

.method public withAmount(Ljava/math/BigDecimal;)Lorg/joda/money/Money;
    .locals 1

    .prologue
    .line 666
    sget-object v0, Ljava/math/RoundingMode;->UNNECESSARY:Ljava/math/RoundingMode;

    invoke-virtual {p0, p1, v0}, Lorg/joda/money/Money;->withAmount(Ljava/math/BigDecimal;Ljava/math/RoundingMode;)Lorg/joda/money/Money;

    move-result-object v0

    return-object v0
.end method

.method public withAmount(Ljava/math/BigDecimal;Ljava/math/RoundingMode;)Lorg/joda/money/Money;
    .locals 1

    .prologue
    .line 683
    iget-object v0, p0, Lorg/joda/money/Money;->money:Lorg/joda/money/BigMoney;

    invoke-virtual {v0, p1}, Lorg/joda/money/BigMoney;->withAmount(Ljava/math/BigDecimal;)Lorg/joda/money/BigMoney;

    move-result-object v0

    invoke-virtual {v0, p2}, Lorg/joda/money/BigMoney;->withCurrencyScale(Ljava/math/RoundingMode;)Lorg/joda/money/BigMoney;

    move-result-object v0

    invoke-direct {p0, v0}, Lorg/joda/money/Money;->with(Lorg/joda/money/BigMoney;)Lorg/joda/money/Money;

    move-result-object v0

    return-object v0
.end method

.method public withCurrencyUnit(Lorg/joda/money/CurrencyUnit;)Lorg/joda/money/Money;
    .locals 1

    .prologue
    .line 448
    sget-object v0, Ljava/math/RoundingMode;->UNNECESSARY:Ljava/math/RoundingMode;

    invoke-virtual {p0, p1, v0}, Lorg/joda/money/Money;->withCurrencyUnit(Lorg/joda/money/CurrencyUnit;Ljava/math/RoundingMode;)Lorg/joda/money/Money;

    move-result-object v0

    return-object v0
.end method

.method public withCurrencyUnit(Lorg/joda/money/CurrencyUnit;Ljava/math/RoundingMode;)Lorg/joda/money/Money;
    .locals 1

    .prologue
    .line 466
    iget-object v0, p0, Lorg/joda/money/Money;->money:Lorg/joda/money/BigMoney;

    invoke-virtual {v0, p1}, Lorg/joda/money/BigMoney;->withCurrencyUnit(Lorg/joda/money/CurrencyUnit;)Lorg/joda/money/BigMoney;

    move-result-object v0

    invoke-virtual {v0, p2}, Lorg/joda/money/BigMoney;->withCurrencyScale(Ljava/math/RoundingMode;)Lorg/joda/money/BigMoney;

    move-result-object v0

    invoke-direct {p0, v0}, Lorg/joda/money/Money;->with(Lorg/joda/money/BigMoney;)Lorg/joda/money/Money;

    move-result-object v0

    return-object v0
.end method
