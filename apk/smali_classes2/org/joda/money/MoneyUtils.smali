.class public final Lorg/joda/money/MoneyUtils;
.super Ljava/lang/Object;
.source "MoneyUtils.java"


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    return-void
.end method

.method public static add(Lorg/joda/money/BigMoney;Lorg/joda/money/BigMoney;)Lorg/joda/money/BigMoney;
    .locals 0

    .prologue
    .line 248
    if-nez p0, :cond_0

    .line 254
    :goto_0
    return-object p1

    .line 251
    :cond_0
    if-nez p1, :cond_1

    move-object p1, p0

    .line 252
    goto :goto_0

    .line 254
    :cond_1
    invoke-virtual {p0, p1}, Lorg/joda/money/BigMoney;->plus(Lorg/joda/money/BigMoneyProvider;)Lorg/joda/money/BigMoney;

    move-result-object p1

    goto :goto_0
.end method

.method public static add(Lorg/joda/money/Money;Lorg/joda/money/Money;)Lorg/joda/money/Money;
    .locals 0

    .prologue
    .line 161
    if-nez p0, :cond_0

    .line 167
    :goto_0
    return-object p1

    .line 164
    :cond_0
    if-nez p1, :cond_1

    move-object p1, p0

    .line 165
    goto :goto_0

    .line 167
    :cond_1
    invoke-virtual {p0, p1}, Lorg/joda/money/Money;->plus(Lorg/joda/money/Money;)Lorg/joda/money/Money;

    move-result-object p1

    goto :goto_0
.end method

.method static checkNotNull(Ljava/lang/Object;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 32
    if-nez p0, :cond_0

    .line 33
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0, p1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 35
    :cond_0
    return-void
.end method

.method public static isNegative(Lorg/joda/money/BigMoneyProvider;)Z
    .locals 1

    .prologue
    .line 90
    if-eqz p0, :cond_0

    invoke-interface {p0}, Lorg/joda/money/BigMoneyProvider;->toBigMoney()Lorg/joda/money/BigMoney;

    move-result-object v0

    invoke-virtual {v0}, Lorg/joda/money/BigMoney;->isNegative()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isNegativeOrZero(Lorg/joda/money/BigMoneyProvider;)Z
    .locals 1

    .prologue
    .line 102
    if-eqz p0, :cond_0

    invoke-interface {p0}, Lorg/joda/money/BigMoneyProvider;->toBigMoney()Lorg/joda/money/BigMoney;

    move-result-object v0

    invoke-virtual {v0}, Lorg/joda/money/BigMoney;->isNegativeOrZero()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isPositive(Lorg/joda/money/BigMoneyProvider;)Z
    .locals 1

    .prologue
    .line 66
    if-eqz p0, :cond_0

    invoke-interface {p0}, Lorg/joda/money/BigMoneyProvider;->toBigMoney()Lorg/joda/money/BigMoney;

    move-result-object v0

    invoke-virtual {v0}, Lorg/joda/money/BigMoney;->isPositive()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isPositiveOrZero(Lorg/joda/money/BigMoneyProvider;)Z
    .locals 1

    .prologue
    .line 78
    if-eqz p0, :cond_0

    invoke-interface {p0}, Lorg/joda/money/BigMoneyProvider;->toBigMoney()Lorg/joda/money/BigMoney;

    move-result-object v0

    invoke-virtual {v0}, Lorg/joda/money/BigMoney;->isPositiveOrZero()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isZero(Lorg/joda/money/BigMoneyProvider;)Z
    .locals 1

    .prologue
    .line 54
    if-eqz p0, :cond_0

    invoke-interface {p0}, Lorg/joda/money/BigMoneyProvider;->toBigMoney()Lorg/joda/money/BigMoney;

    move-result-object v0

    invoke-virtual {v0}, Lorg/joda/money/BigMoney;->isZero()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static max(Lorg/joda/money/BigMoney;Lorg/joda/money/BigMoney;)Lorg/joda/money/BigMoney;
    .locals 1

    .prologue
    .line 205
    if-nez p0, :cond_1

    move-object p0, p1

    .line 211
    :cond_0
    :goto_0
    return-object p0

    .line 208
    :cond_1
    if-eqz p1, :cond_0

    .line 211
    invoke-virtual {p0, p1}, Lorg/joda/money/BigMoney;->compareTo(Lorg/joda/money/BigMoneyProvider;)I

    move-result v0

    if-gtz v0, :cond_0

    move-object p0, p1

    goto :goto_0
.end method

.method public static max(Lorg/joda/money/Money;Lorg/joda/money/Money;)Lorg/joda/money/Money;
    .locals 1

    .prologue
    .line 118
    if-nez p0, :cond_1

    move-object p0, p1

    .line 124
    :cond_0
    :goto_0
    return-object p0

    .line 121
    :cond_1
    if-eqz p1, :cond_0

    .line 124
    invoke-virtual {p0, p1}, Lorg/joda/money/Money;->compareTo(Lorg/joda/money/BigMoneyProvider;)I

    move-result v0

    if-gtz v0, :cond_0

    move-object p0, p1

    goto :goto_0
.end method

.method public static min(Lorg/joda/money/BigMoney;Lorg/joda/money/BigMoney;)Lorg/joda/money/BigMoney;
    .locals 1

    .prologue
    .line 226
    if-nez p0, :cond_1

    move-object p0, p1

    .line 232
    :cond_0
    :goto_0
    return-object p0

    .line 229
    :cond_1
    if-eqz p1, :cond_0

    .line 232
    invoke-virtual {p0, p1}, Lorg/joda/money/BigMoney;->compareTo(Lorg/joda/money/BigMoneyProvider;)I

    move-result v0

    if-ltz v0, :cond_0

    move-object p0, p1

    goto :goto_0
.end method

.method public static min(Lorg/joda/money/Money;Lorg/joda/money/Money;)Lorg/joda/money/Money;
    .locals 1

    .prologue
    .line 139
    if-nez p0, :cond_1

    move-object p0, p1

    .line 145
    :cond_0
    :goto_0
    return-object p0

    .line 142
    :cond_1
    if-eqz p1, :cond_0

    .line 145
    invoke-virtual {p0, p1}, Lorg/joda/money/Money;->compareTo(Lorg/joda/money/BigMoneyProvider;)I

    move-result v0

    if-ltz v0, :cond_0

    move-object p0, p1

    goto :goto_0
.end method

.method public static subtract(Lorg/joda/money/BigMoney;Lorg/joda/money/BigMoney;)Lorg/joda/money/BigMoney;
    .locals 0

    .prologue
    .line 270
    if-nez p1, :cond_0

    .line 276
    :goto_0
    return-object p0

    .line 273
    :cond_0
    if-nez p0, :cond_1

    .line 274
    invoke-virtual {p1}, Lorg/joda/money/BigMoney;->negated()Lorg/joda/money/BigMoney;

    move-result-object p0

    goto :goto_0

    .line 276
    :cond_1
    invoke-virtual {p0, p1}, Lorg/joda/money/BigMoney;->minus(Lorg/joda/money/BigMoneyProvider;)Lorg/joda/money/BigMoney;

    move-result-object p0

    goto :goto_0
.end method

.method public static subtract(Lorg/joda/money/Money;Lorg/joda/money/Money;)Lorg/joda/money/Money;
    .locals 0

    .prologue
    .line 183
    if-nez p1, :cond_0

    .line 189
    :goto_0
    return-object p0

    .line 186
    :cond_0
    if-nez p0, :cond_1

    .line 187
    invoke-virtual {p1}, Lorg/joda/money/Money;->negated()Lorg/joda/money/Money;

    move-result-object p0

    goto :goto_0

    .line 189
    :cond_1
    invoke-virtual {p0, p1}, Lorg/joda/money/Money;->minus(Lorg/joda/money/Money;)Lorg/joda/money/Money;

    move-result-object p0

    goto :goto_0
.end method
