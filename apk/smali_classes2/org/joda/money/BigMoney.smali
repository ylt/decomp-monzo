.class public final Lorg/joda/money/BigMoney;
.super Ljava/lang/Object;
.source "BigMoney.java"

# interfaces
.implements Ljava/io/Serializable;
.implements Ljava/lang/Comparable;
.implements Lorg/joda/money/BigMoneyProvider;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/io/Serializable;",
        "Ljava/lang/Comparable",
        "<",
        "Lorg/joda/money/BigMoneyProvider;",
        ">;",
        "Lorg/joda/money/BigMoneyProvider;"
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z

.field private static final PARSE_REGEX:Ljava/util/regex/Pattern;

.field private static final serialVersionUID:J = 0x1L


# instance fields
.field private final amount:Ljava/math/BigDecimal;

.field private final currency:Lorg/joda/money/CurrencyUnit;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 45
    const-class v0, Lorg/joda/money/BigMoney;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/joda/money/BigMoney;->$assertionsDisabled:Z

    .line 54
    const-string v0, "[+-]?[0-9]*[.]?[0-9]*"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lorg/joda/money/BigMoney;->PARSE_REGEX:Ljava/util/regex/Pattern;

    return-void

    .line 45
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 417
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 418
    iput-object v0, p0, Lorg/joda/money/BigMoney;->currency:Lorg/joda/money/CurrencyUnit;

    .line 419
    iput-object v0, p0, Lorg/joda/money/BigMoney;->amount:Ljava/math/BigDecimal;

    .line 420
    return-void
.end method

.method constructor <init>(Lorg/joda/money/CurrencyUnit;Ljava/math/BigDecimal;)V
    .locals 2

    .prologue
    .line 428
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 429
    sget-boolean v0, Lorg/joda/money/BigMoney;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "Joda-Money bug: Currency must not be null"

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 430
    :cond_0
    sget-boolean v0, Lorg/joda/money/BigMoney;->$assertionsDisabled:Z

    if-nez v0, :cond_1

    if-nez p2, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "Joda-Money bug: Amount must not be null"

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 431
    :cond_1
    iput-object p1, p0, Lorg/joda/money/BigMoney;->currency:Lorg/joda/money/CurrencyUnit;

    .line 432
    invoke-virtual {p2}, Ljava/math/BigDecimal;->scale()I

    move-result v0

    if-gez v0, :cond_2

    const/4 v0, 0x0

    invoke-virtual {p2, v0}, Ljava/math/BigDecimal;->setScale(I)Ljava/math/BigDecimal;

    move-result-object p2

    :cond_2
    iput-object p2, p0, Lorg/joda/money/BigMoney;->amount:Ljava/math/BigDecimal;

    .line 433
    return-void
.end method

.method private checkCurrencyEqual(Lorg/joda/money/BigMoneyProvider;)Lorg/joda/money/BigMoney;
    .locals 3

    .prologue
    .line 829
    invoke-static {p1}, Lorg/joda/money/BigMoney;->of(Lorg/joda/money/BigMoneyProvider;)Lorg/joda/money/BigMoney;

    move-result-object v0

    .line 830
    invoke-virtual {p0, v0}, Lorg/joda/money/BigMoney;->isSameCurrency(Lorg/joda/money/BigMoneyProvider;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 831
    new-instance v1, Lorg/joda/money/CurrencyMismatchException;

    invoke-virtual {p0}, Lorg/joda/money/BigMoney;->getCurrencyUnit()Lorg/joda/money/CurrencyUnit;

    move-result-object v2

    invoke-virtual {v0}, Lorg/joda/money/BigMoney;->getCurrencyUnit()Lorg/joda/money/CurrencyUnit;

    move-result-object v0

    invoke-direct {v1, v2, v0}, Lorg/joda/money/CurrencyMismatchException;-><init>(Lorg/joda/money/CurrencyUnit;Lorg/joda/money/CurrencyUnit;)V

    throw v1

    .line 833
    :cond_0
    return-object v0
.end method

.method public static nonNull(Lorg/joda/money/BigMoney;Lorg/joda/money/CurrencyUnit;)Lorg/joda/money/BigMoney;
    .locals 2

    .prologue
    .line 402
    if-nez p0, :cond_1

    .line 403
    invoke-static {p1}, Lorg/joda/money/BigMoney;->zero(Lorg/joda/money/CurrencyUnit;)Lorg/joda/money/BigMoney;

    move-result-object p0

    .line 409
    :cond_0
    return-object p0

    .line 405
    :cond_1
    invoke-virtual {p0}, Lorg/joda/money/BigMoney;->getCurrencyUnit()Lorg/joda/money/CurrencyUnit;

    move-result-object v0

    invoke-virtual {v0, p1}, Lorg/joda/money/CurrencyUnit;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 406
    const-string v0, "Currency must not be null"

    invoke-static {p1, v0}, Lorg/joda/money/MoneyUtils;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 407
    new-instance v0, Lorg/joda/money/CurrencyMismatchException;

    invoke-virtual {p0}, Lorg/joda/money/BigMoney;->getCurrencyUnit()Lorg/joda/money/CurrencyUnit;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Lorg/joda/money/CurrencyMismatchException;-><init>(Lorg/joda/money/CurrencyUnit;Lorg/joda/money/CurrencyUnit;)V

    throw v0
.end method

.method public static of(Lorg/joda/money/BigMoneyProvider;)Lorg/joda/money/BigMoney;
    .locals 2

    .prologue
    .line 258
    const-string v0, "BigMoneyProvider must not be null"

    invoke-static {p0, v0}, Lorg/joda/money/MoneyUtils;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 259
    invoke-interface {p0}, Lorg/joda/money/BigMoneyProvider;->toBigMoney()Lorg/joda/money/BigMoney;

    move-result-object v0

    .line 260
    const-string v1, "BigMoneyProvider must not return null"

    invoke-static {v0, v1}, Lorg/joda/money/MoneyUtils;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 261
    return-object v0
.end method

.method public static of(Lorg/joda/money/CurrencyUnit;D)Lorg/joda/money/BigMoney;
    .locals 1

    .prologue
    .line 112
    const-string v0, "Currency must not be null"

    invoke-static {p0, v0}, Lorg/joda/money/MoneyUtils;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 113
    invoke-static {p1, p2}, Ljava/math/BigDecimal;->valueOf(D)Ljava/math/BigDecimal;

    move-result-object v0

    invoke-virtual {v0}, Ljava/math/BigDecimal;->stripTrailingZeros()Ljava/math/BigDecimal;

    move-result-object v0

    invoke-static {p0, v0}, Lorg/joda/money/BigMoney;->of(Lorg/joda/money/CurrencyUnit;Ljava/math/BigDecimal;)Lorg/joda/money/BigMoney;

    move-result-object v0

    return-object v0
.end method

.method public static of(Lorg/joda/money/CurrencyUnit;Ljava/math/BigDecimal;)Lorg/joda/money/BigMoney;
    .locals 3

    .prologue
    .line 79
    const-string v0, "Currency must not be null"

    invoke-static {p0, v0}, Lorg/joda/money/MoneyUtils;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 80
    const-string v0, "Amount must not be null"

    invoke-static {p1, v0}, Lorg/joda/money/MoneyUtils;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 81
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-class v1, Ljava/math/BigDecimal;

    if-eq v0, v1, :cond_1

    .line 82
    invoke-virtual {p1}, Ljava/math/BigDecimal;->unscaledValue()Ljava/math/BigInteger;

    move-result-object v1

    .line 83
    if-nez v1, :cond_0

    .line 84
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Illegal BigDecimal subclass"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 86
    :cond_0
    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-class v2, Ljava/math/BigInteger;

    if-eq v0, v2, :cond_2

    .line 87
    new-instance v0, Ljava/math/BigInteger;

    invoke-virtual {v1}, Ljava/math/BigInteger;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/math/BigInteger;-><init>(Ljava/lang/String;)V

    .line 89
    :goto_0
    new-instance v1, Ljava/math/BigDecimal;

    invoke-virtual {p1}, Ljava/math/BigDecimal;->scale()I

    move-result v2

    invoke-direct {v1, v0, v2}, Ljava/math/BigDecimal;-><init>(Ljava/math/BigInteger;I)V

    move-object p1, v1

    .line 91
    :cond_1
    new-instance v0, Lorg/joda/money/BigMoney;

    invoke-direct {v0, p0, p1}, Lorg/joda/money/BigMoney;-><init>(Lorg/joda/money/CurrencyUnit;Ljava/math/BigDecimal;)V

    return-object v0

    :cond_2
    move-object v0, v1

    goto :goto_0
.end method

.method public static ofMajor(Lorg/joda/money/CurrencyUnit;J)Lorg/joda/money/BigMoney;
    .locals 1

    .prologue
    .line 194
    const-string v0, "CurrencyUnit must not be null"

    invoke-static {p0, v0}, Lorg/joda/money/MoneyUtils;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 195
    invoke-static {p1, p2}, Ljava/math/BigDecimal;->valueOf(J)Ljava/math/BigDecimal;

    move-result-object v0

    invoke-static {p0, v0}, Lorg/joda/money/BigMoney;->of(Lorg/joda/money/CurrencyUnit;Ljava/math/BigDecimal;)Lorg/joda/money/BigMoney;

    move-result-object v0

    return-object v0
.end method

.method public static ofMinor(Lorg/joda/money/CurrencyUnit;J)Lorg/joda/money/BigMoney;
    .locals 1

    .prologue
    .line 214
    const-string v0, "CurrencyUnit must not be null"

    invoke-static {p0, v0}, Lorg/joda/money/MoneyUtils;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 215
    invoke-virtual {p0}, Lorg/joda/money/CurrencyUnit;->getDecimalPlaces()I

    move-result v0

    invoke-static {p1, p2, v0}, Ljava/math/BigDecimal;->valueOf(JI)Ljava/math/BigDecimal;

    move-result-object v0

    invoke-static {p0, v0}, Lorg/joda/money/BigMoney;->of(Lorg/joda/money/CurrencyUnit;Ljava/math/BigDecimal;)Lorg/joda/money/BigMoney;

    move-result-object v0

    return-object v0
.end method

.method public static ofScale(Lorg/joda/money/CurrencyUnit;JI)Lorg/joda/money/BigMoney;
    .locals 1

    .prologue
    .line 174
    const-string v0, "Currency must not be null"

    invoke-static {p0, v0}, Lorg/joda/money/MoneyUtils;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 175
    invoke-static {p1, p2, p3}, Ljava/math/BigDecimal;->valueOf(JI)Ljava/math/BigDecimal;

    move-result-object v0

    invoke-static {p0, v0}, Lorg/joda/money/BigMoney;->of(Lorg/joda/money/CurrencyUnit;Ljava/math/BigDecimal;)Lorg/joda/money/BigMoney;

    move-result-object v0

    return-object v0
.end method

.method public static ofScale(Lorg/joda/money/CurrencyUnit;Ljava/math/BigDecimal;I)Lorg/joda/money/BigMoney;
    .locals 1

    .prologue
    .line 132
    sget-object v0, Ljava/math/RoundingMode;->UNNECESSARY:Ljava/math/RoundingMode;

    invoke-static {p0, p1, p2, v0}, Lorg/joda/money/BigMoney;->ofScale(Lorg/joda/money/CurrencyUnit;Ljava/math/BigDecimal;ILjava/math/RoundingMode;)Lorg/joda/money/BigMoney;

    move-result-object v0

    return-object v0
.end method

.method public static ofScale(Lorg/joda/money/CurrencyUnit;Ljava/math/BigDecimal;ILjava/math/RoundingMode;)Lorg/joda/money/BigMoney;
    .locals 1

    .prologue
    .line 152
    const-string v0, "CurrencyUnit must not be null"

    invoke-static {p0, v0}, Lorg/joda/money/MoneyUtils;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 153
    const-string v0, "Amount must not be null"

    invoke-static {p1, v0}, Lorg/joda/money/MoneyUtils;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 154
    const-string v0, "RoundingMode must not be null"

    invoke-static {p3, v0}, Lorg/joda/money/MoneyUtils;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 155
    invoke-virtual {p1, p2, p3}, Ljava/math/BigDecimal;->setScale(ILjava/math/RoundingMode;)Ljava/math/BigDecimal;

    move-result-object v0

    .line 156
    invoke-static {p0, v0}, Lorg/joda/money/BigMoney;->of(Lorg/joda/money/CurrencyUnit;Ljava/math/BigDecimal;)Lorg/joda/money/BigMoney;

    move-result-object v0

    return-object v0
.end method

.method public static parse(Ljava/lang/String;)Lorg/joda/money/BigMoney;
    .locals 4
    .annotation runtime Lorg/joda/convert/FromString;
    .end annotation

    .prologue
    const/4 v0, 0x3

    .line 371
    const-string v1, "Money must not be null"

    invoke-static {p0, v1}, Lorg/joda/money/MoneyUtils;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 372
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    const/4 v2, 0x4

    if-ge v1, v2, :cond_0

    .line 373
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Money \'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\' cannot be parsed"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 375
    :cond_0
    const/4 v1, 0x0

    invoke-virtual {p0, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    .line 377
    :goto_0
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v2

    if-ge v0, v2, :cond_1

    invoke-virtual {p0, v0}, Ljava/lang/String;->charAt(I)C

    move-result v2

    const/16 v3, 0x20

    if-ne v2, v3, :cond_1

    .line 378
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 380
    :cond_1
    invoke-virtual {p0, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 381
    sget-object v2, Lorg/joda/money/BigMoney;->PARSE_REGEX:Ljava/util/regex/Pattern;

    invoke-virtual {v2, v0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/regex/Matcher;->matches()Z

    move-result v2

    if-nez v2, :cond_2

    .line 382
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Money amount \'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\' cannot be parsed"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 384
    :cond_2
    invoke-static {v1}, Lorg/joda/money/CurrencyUnit;->of(Ljava/lang/String;)Lorg/joda/money/CurrencyUnit;

    move-result-object v1

    new-instance v2, Ljava/math/BigDecimal;

    invoke-direct {v2, v0}, Ljava/math/BigDecimal;-><init>(Ljava/lang/String;)V

    invoke-static {v1, v2}, Lorg/joda/money/BigMoney;->of(Lorg/joda/money/CurrencyUnit;Ljava/math/BigDecimal;)Lorg/joda/money/BigMoney;

    move-result-object v0

    return-object v0
.end method

.method private readObject(Ljava/io/ObjectInputStream;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/InvalidObjectException;
        }
    .end annotation

    .prologue
    .line 442
    new-instance v0, Ljava/io/InvalidObjectException;

    const-string v1, "Serialization delegate required"

    invoke-direct {v0, v1}, Ljava/io/InvalidObjectException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static total(Ljava/lang/Iterable;)Lorg/joda/money/BigMoney;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<+",
            "Lorg/joda/money/BigMoneyProvider;",
            ">;)",
            "Lorg/joda/money/BigMoney;"
        }
    .end annotation

    .prologue
    .line 303
    const-string v0, "Money iterator must not be null"

    invoke-static {p0, v0}, Lorg/joda/money/MoneyUtils;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 304
    invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 305
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_0

    .line 306
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Money iterator must not be empty"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 308
    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/joda/money/BigMoneyProvider;

    invoke-static {v0}, Lorg/joda/money/BigMoney;->of(Lorg/joda/money/BigMoneyProvider;)Lorg/joda/money/BigMoney;

    move-result-object v0

    .line 309
    const-string v1, "Money iterator must not contain null entries"

    invoke-static {v0, v1}, Lorg/joda/money/MoneyUtils;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v1, v0

    .line 310
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 311
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/joda/money/BigMoneyProvider;

    invoke-virtual {v1, v0}, Lorg/joda/money/BigMoney;->plus(Lorg/joda/money/BigMoneyProvider;)Lorg/joda/money/BigMoney;

    move-result-object v0

    move-object v1, v0

    goto :goto_0

    .line 313
    :cond_1
    return-object v1
.end method

.method public static total(Lorg/joda/money/CurrencyUnit;Ljava/lang/Iterable;)Lorg/joda/money/BigMoney;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/joda/money/CurrencyUnit;",
            "Ljava/lang/Iterable",
            "<+",
            "Lorg/joda/money/BigMoneyProvider;",
            ">;)",
            "Lorg/joda/money/BigMoney;"
        }
    .end annotation

    .prologue
    .line 347
    invoke-static {p0}, Lorg/joda/money/BigMoney;->zero(Lorg/joda/money/CurrencyUnit;)Lorg/joda/money/BigMoney;

    move-result-object v0

    invoke-virtual {v0, p1}, Lorg/joda/money/BigMoney;->plus(Ljava/lang/Iterable;)Lorg/joda/money/BigMoney;

    move-result-object v0

    return-object v0
.end method

.method public static varargs total(Lorg/joda/money/CurrencyUnit;[Lorg/joda/money/BigMoneyProvider;)Lorg/joda/money/BigMoney;
    .locals 2

    .prologue
    .line 330
    invoke-static {p0}, Lorg/joda/money/BigMoney;->zero(Lorg/joda/money/CurrencyUnit;)Lorg/joda/money/BigMoney;

    move-result-object v0

    invoke-static {p1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/joda/money/BigMoney;->plus(Ljava/lang/Iterable;)Lorg/joda/money/BigMoney;

    move-result-object v0

    return-object v0
.end method

.method public static varargs total([Lorg/joda/money/BigMoneyProvider;)Lorg/joda/money/BigMoney;
    .locals 3

    .prologue
    .line 278
    const-string v0, "Money array must not be null"

    invoke-static {p0, v0}, Lorg/joda/money/MoneyUtils;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 279
    array-length v0, p0

    if-nez v0, :cond_0

    .line 280
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Money array must not be empty"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 282
    :cond_0
    const/4 v0, 0x0

    aget-object v0, p0, v0

    invoke-static {v0}, Lorg/joda/money/BigMoney;->of(Lorg/joda/money/BigMoneyProvider;)Lorg/joda/money/BigMoney;

    move-result-object v1

    .line 283
    const-string v0, "Money array must not contain null entries"

    invoke-static {v1, v0}, Lorg/joda/money/MoneyUtils;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 284
    const/4 v0, 0x1

    :goto_0
    array-length v2, p0

    if-ge v0, v2, :cond_1

    .line 285
    aget-object v2, p0, v0

    invoke-static {v2}, Lorg/joda/money/BigMoney;->of(Lorg/joda/money/BigMoneyProvider;)Lorg/joda/money/BigMoney;

    move-result-object v2

    invoke-virtual {v1, v2}, Lorg/joda/money/BigMoney;->plus(Lorg/joda/money/BigMoneyProvider;)Lorg/joda/money/BigMoney;

    move-result-object v1

    .line 284
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 287
    :cond_1
    return-object v1
.end method

.method private with(Ljava/math/BigDecimal;)Lorg/joda/money/BigMoney;
    .locals 2

    .prologue
    .line 464
    iget-object v0, p0, Lorg/joda/money/BigMoney;->amount:Ljava/math/BigDecimal;

    if-ne p1, v0, :cond_0

    .line 467
    :goto_0
    return-object p0

    :cond_0
    new-instance v0, Lorg/joda/money/BigMoney;

    iget-object v1, p0, Lorg/joda/money/BigMoney;->currency:Lorg/joda/money/CurrencyUnit;

    invoke-direct {v0, v1, p1}, Lorg/joda/money/BigMoney;-><init>(Lorg/joda/money/CurrencyUnit;Ljava/math/BigDecimal;)V

    move-object p0, v0

    goto :goto_0
.end method

.method private writeReplace()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 451
    new-instance v0, Lorg/joda/money/Ser;

    const/16 v1, 0x42

    invoke-direct {v0, v1, p0}, Lorg/joda/money/Ser;-><init>(BLjava/lang/Object;)V

    return-object v0
.end method

.method public static zero(Lorg/joda/money/CurrencyUnit;)Lorg/joda/money/BigMoney;
    .locals 1

    .prologue
    .line 229
    sget-object v0, Ljava/math/BigDecimal;->ZERO:Ljava/math/BigDecimal;

    invoke-static {p0, v0}, Lorg/joda/money/BigMoney;->of(Lorg/joda/money/CurrencyUnit;Ljava/math/BigDecimal;)Lorg/joda/money/BigMoney;

    move-result-object v0

    return-object v0
.end method

.method public static zero(Lorg/joda/money/CurrencyUnit;I)Lorg/joda/money/BigMoney;
    .locals 2

    .prologue
    .line 243
    const-wide/16 v0, 0x0

    invoke-static {v0, v1, p1}, Ljava/math/BigDecimal;->valueOf(JI)Ljava/math/BigDecimal;

    move-result-object v0

    invoke-static {p0, v0}, Lorg/joda/money/BigMoney;->of(Lorg/joda/money/CurrencyUnit;Ljava/math/BigDecimal;)Lorg/joda/money/BigMoney;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public abs()Lorg/joda/money/BigMoney;
    .locals 1

    .prologue
    .line 1484
    invoke-virtual {p0}, Lorg/joda/money/BigMoney;->isNegative()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lorg/joda/money/BigMoney;->negated()Lorg/joda/money/BigMoney;

    move-result-object p0

    :cond_0
    return-object p0
.end method

.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 45
    check-cast p1, Lorg/joda/money/BigMoneyProvider;

    invoke-virtual {p0, p1}, Lorg/joda/money/BigMoney;->compareTo(Lorg/joda/money/BigMoneyProvider;)I

    move-result v0

    return v0
.end method

.method public compareTo(Lorg/joda/money/BigMoneyProvider;)I
    .locals 3

    .prologue
    .line 1630
    invoke-static {p1}, Lorg/joda/money/BigMoney;->of(Lorg/joda/money/BigMoneyProvider;)Lorg/joda/money/BigMoney;

    move-result-object v0

    .line 1631
    iget-object v1, p0, Lorg/joda/money/BigMoney;->currency:Lorg/joda/money/CurrencyUnit;

    iget-object v2, v0, Lorg/joda/money/BigMoney;->currency:Lorg/joda/money/CurrencyUnit;

    invoke-virtual {v1, v2}, Lorg/joda/money/CurrencyUnit;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1632
    new-instance v1, Lorg/joda/money/CurrencyMismatchException;

    invoke-virtual {p0}, Lorg/joda/money/BigMoney;->getCurrencyUnit()Lorg/joda/money/CurrencyUnit;

    move-result-object v2

    invoke-virtual {v0}, Lorg/joda/money/BigMoney;->getCurrencyUnit()Lorg/joda/money/CurrencyUnit;

    move-result-object v0

    invoke-direct {v1, v2, v0}, Lorg/joda/money/CurrencyMismatchException;-><init>(Lorg/joda/money/CurrencyUnit;Lorg/joda/money/CurrencyUnit;)V

    throw v1

    .line 1634
    :cond_0
    iget-object v1, p0, Lorg/joda/money/BigMoney;->amount:Ljava/math/BigDecimal;

    iget-object v0, v0, Lorg/joda/money/BigMoney;->amount:Ljava/math/BigDecimal;

    invoke-virtual {v1, v0}, Ljava/math/BigDecimal;->compareTo(Ljava/math/BigDecimal;)I

    move-result v0

    return v0
.end method

.method public convertRetainScale(Lorg/joda/money/CurrencyUnit;Ljava/math/BigDecimal;Ljava/math/RoundingMode;)Lorg/joda/money/BigMoney;
    .locals 2

    .prologue
    .line 1571
    invoke-virtual {p0, p1, p2}, Lorg/joda/money/BigMoney;->convertedTo(Lorg/joda/money/CurrencyUnit;Ljava/math/BigDecimal;)Lorg/joda/money/BigMoney;

    move-result-object v0

    invoke-virtual {p0}, Lorg/joda/money/BigMoney;->getScale()I

    move-result v1

    invoke-virtual {v0, v1, p3}, Lorg/joda/money/BigMoney;->withScale(ILjava/math/RoundingMode;)Lorg/joda/money/BigMoney;

    move-result-object v0

    return-object v0
.end method

.method public convertedTo(Lorg/joda/money/CurrencyUnit;Ljava/math/BigDecimal;)Lorg/joda/money/BigMoney;
    .locals 2

    .prologue
    .line 1537
    const-string v0, "CurrencyUnit must not be null"

    invoke-static {p1, v0}, Lorg/joda/money/MoneyUtils;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1538
    const-string v0, "Multiplier must not be null"

    invoke-static {p2, v0}, Lorg/joda/money/MoneyUtils;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1539
    iget-object v0, p0, Lorg/joda/money/BigMoney;->currency:Lorg/joda/money/CurrencyUnit;

    if-ne v0, p1, :cond_1

    .line 1540
    sget-object v0, Ljava/math/BigDecimal;->ONE:Ljava/math/BigDecimal;

    invoke-virtual {p2, v0}, Ljava/math/BigDecimal;->compareTo(Ljava/math/BigDecimal;)I

    move-result v0

    if-nez v0, :cond_0

    .line 1549
    :goto_0
    return-object p0

    .line 1543
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Cannot convert to the same currency"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1545
    :cond_1
    sget-object v0, Ljava/math/BigDecimal;->ZERO:Ljava/math/BigDecimal;

    invoke-virtual {p2, v0}, Ljava/math/BigDecimal;->compareTo(Ljava/math/BigDecimal;)I

    move-result v0

    if-gez v0, :cond_2

    .line 1546
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Cannot convert using a negative conversion multiplier"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1548
    :cond_2
    iget-object v0, p0, Lorg/joda/money/BigMoney;->amount:Ljava/math/BigDecimal;

    invoke-virtual {v0, p2}, Ljava/math/BigDecimal;->multiply(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object v0

    .line 1549
    invoke-static {p1, v0}, Lorg/joda/money/BigMoney;->of(Lorg/joda/money/CurrencyUnit;Ljava/math/BigDecimal;)Lorg/joda/money/BigMoney;

    move-result-object p0

    goto :goto_0
.end method

.method public dividedBy(DLjava/math/RoundingMode;)Lorg/joda/money/BigMoney;
    .locals 3

    .prologue
    .line 1430
    const-string v0, "RoundingMode must not be null"

    invoke-static {p3, v0}, Lorg/joda/money/MoneyUtils;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1431
    const-wide/high16 v0, 0x3ff0000000000000L    # 1.0

    cmpl-double v0, p1, v0

    if-nez v0, :cond_0

    .line 1435
    :goto_0
    return-object p0

    .line 1434
    :cond_0
    iget-object v0, p0, Lorg/joda/money/BigMoney;->amount:Ljava/math/BigDecimal;

    invoke-static {p1, p2}, Ljava/math/BigDecimal;->valueOf(D)Ljava/math/BigDecimal;

    move-result-object v1

    invoke-virtual {v0, v1, p3}, Ljava/math/BigDecimal;->divide(Ljava/math/BigDecimal;Ljava/math/RoundingMode;)Ljava/math/BigDecimal;

    move-result-object v0

    .line 1435
    iget-object v1, p0, Lorg/joda/money/BigMoney;->currency:Lorg/joda/money/CurrencyUnit;

    invoke-static {v1, v0}, Lorg/joda/money/BigMoney;->of(Lorg/joda/money/CurrencyUnit;Ljava/math/BigDecimal;)Lorg/joda/money/BigMoney;

    move-result-object p0

    goto :goto_0
.end method

.method public dividedBy(JLjava/math/RoundingMode;)Lorg/joda/money/BigMoney;
    .locals 3

    .prologue
    .line 1454
    const-wide/16 v0, 0x1

    cmp-long v0, p1, v0

    if-nez v0, :cond_0

    .line 1458
    :goto_0
    return-object p0

    .line 1457
    :cond_0
    iget-object v0, p0, Lorg/joda/money/BigMoney;->amount:Ljava/math/BigDecimal;

    invoke-static {p1, p2}, Ljava/math/BigDecimal;->valueOf(J)Ljava/math/BigDecimal;

    move-result-object v1

    invoke-virtual {v0, v1, p3}, Ljava/math/BigDecimal;->divide(Ljava/math/BigDecimal;Ljava/math/RoundingMode;)Ljava/math/BigDecimal;

    move-result-object v0

    .line 1458
    iget-object v1, p0, Lorg/joda/money/BigMoney;->currency:Lorg/joda/money/CurrencyUnit;

    invoke-static {v1, v0}, Lorg/joda/money/BigMoney;->of(Lorg/joda/money/CurrencyUnit;Ljava/math/BigDecimal;)Lorg/joda/money/BigMoney;

    move-result-object p0

    goto :goto_0
.end method

.method public dividedBy(Ljava/math/BigDecimal;Ljava/math/RoundingMode;)Lorg/joda/money/BigMoney;
    .locals 2

    .prologue
    .line 1398
    const-string v0, "Divisor must not be null"

    invoke-static {p1, v0}, Lorg/joda/money/MoneyUtils;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1399
    const-string v0, "RoundingMode must not be null"

    invoke-static {p2, v0}, Lorg/joda/money/MoneyUtils;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1400
    sget-object v0, Ljava/math/BigDecimal;->ONE:Ljava/math/BigDecimal;

    invoke-virtual {p1, v0}, Ljava/math/BigDecimal;->compareTo(Ljava/math/BigDecimal;)I

    move-result v0

    if-nez v0, :cond_0

    .line 1404
    :goto_0
    return-object p0

    .line 1403
    :cond_0
    iget-object v0, p0, Lorg/joda/money/BigMoney;->amount:Ljava/math/BigDecimal;

    invoke-virtual {v0, p1, p2}, Ljava/math/BigDecimal;->divide(Ljava/math/BigDecimal;Ljava/math/RoundingMode;)Ljava/math/BigDecimal;

    move-result-object v0

    .line 1404
    iget-object v1, p0, Lorg/joda/money/BigMoney;->currency:Lorg/joda/money/CurrencyUnit;

    invoke-static {v1, v0}, Lorg/joda/money/BigMoney;->of(Lorg/joda/money/CurrencyUnit;Ljava/math/BigDecimal;)Lorg/joda/money/BigMoney;

    move-result-object p0

    goto :goto_0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1693
    if-ne p0, p1, :cond_1

    .line 1701
    :cond_0
    :goto_0
    return v0

    .line 1696
    :cond_1
    instance-of v2, p1, Lorg/joda/money/BigMoney;

    if-eqz v2, :cond_3

    .line 1697
    check-cast p1, Lorg/joda/money/BigMoney;

    .line 1698
    iget-object v2, p0, Lorg/joda/money/BigMoney;->currency:Lorg/joda/money/CurrencyUnit;

    invoke-virtual {p1}, Lorg/joda/money/BigMoney;->getCurrencyUnit()Lorg/joda/money/CurrencyUnit;

    move-result-object v3

    invoke-virtual {v2, v3}, Lorg/joda/money/CurrencyUnit;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lorg/joda/money/BigMoney;->amount:Ljava/math/BigDecimal;

    iget-object v3, p1, Lorg/joda/money/BigMoney;->amount:Ljava/math/BigDecimal;

    invoke-virtual {v2, v3}, Ljava/math/BigDecimal;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_2
    move v0, v1

    goto :goto_0

    :cond_3
    move v0, v1

    .line 1701
    goto :goto_0
.end method

.method public getAmount()Ljava/math/BigDecimal;
    .locals 1

    .prologue
    .line 620
    iget-object v0, p0, Lorg/joda/money/BigMoney;->amount:Ljava/math/BigDecimal;

    return-object v0
.end method

.method public getAmountMajor()Ljava/math/BigDecimal;
    .locals 3

    .prologue
    .line 637
    iget-object v0, p0, Lorg/joda/money/BigMoney;->amount:Ljava/math/BigDecimal;

    const/4 v1, 0x0

    sget-object v2, Ljava/math/RoundingMode;->DOWN:Ljava/math/RoundingMode;

    invoke-virtual {v0, v1, v2}, Ljava/math/BigDecimal;->setScale(ILjava/math/RoundingMode;)Ljava/math/BigDecimal;

    move-result-object v0

    return-object v0
.end method

.method public getAmountMajorInt()I
    .locals 1

    .prologue
    .line 665
    invoke-virtual {p0}, Lorg/joda/money/BigMoney;->getAmountMajor()Ljava/math/BigDecimal;

    move-result-object v0

    invoke-virtual {v0}, Ljava/math/BigDecimal;->intValueExact()I

    move-result v0

    return v0
.end method

.method public getAmountMajorLong()J
    .locals 2

    .prologue
    .line 651
    invoke-virtual {p0}, Lorg/joda/money/BigMoney;->getAmountMajor()Ljava/math/BigDecimal;

    move-result-object v0

    invoke-virtual {v0}, Ljava/math/BigDecimal;->longValueExact()J

    move-result-wide v0

    return-wide v0
.end method

.method public getAmountMinor()Ljava/math/BigDecimal;
    .locals 3

    .prologue
    .line 682
    invoke-virtual {p0}, Lorg/joda/money/BigMoney;->getCurrencyUnit()Lorg/joda/money/CurrencyUnit;

    move-result-object v0

    invoke-virtual {v0}, Lorg/joda/money/CurrencyUnit;->getDecimalPlaces()I

    move-result v0

    .line 683
    iget-object v1, p0, Lorg/joda/money/BigMoney;->amount:Ljava/math/BigDecimal;

    sget-object v2, Ljava/math/RoundingMode;->DOWN:Ljava/math/RoundingMode;

    invoke-virtual {v1, v0, v2}, Ljava/math/BigDecimal;->setScale(ILjava/math/RoundingMode;)Ljava/math/BigDecimal;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/math/BigDecimal;->movePointRight(I)Ljava/math/BigDecimal;

    move-result-object v0

    return-object v0
.end method

.method public getAmountMinorInt()I
    .locals 1

    .prologue
    .line 711
    invoke-virtual {p0}, Lorg/joda/money/BigMoney;->getAmountMinor()Ljava/math/BigDecimal;

    move-result-object v0

    invoke-virtual {v0}, Ljava/math/BigDecimal;->intValueExact()I

    move-result v0

    return v0
.end method

.method public getAmountMinorLong()J
    .locals 2

    .prologue
    .line 697
    invoke-virtual {p0}, Lorg/joda/money/BigMoney;->getAmountMinor()Ljava/math/BigDecimal;

    move-result-object v0

    invoke-virtual {v0}, Ljava/math/BigDecimal;->longValueExact()J

    move-result-wide v0

    return-wide v0
.end method

.method public getCurrencyUnit()Lorg/joda/money/CurrencyUnit;
    .locals 1

    .prologue
    .line 477
    iget-object v0, p0, Lorg/joda/money/BigMoney;->currency:Lorg/joda/money/CurrencyUnit;

    return-object v0
.end method

.method public getMinorPart()I
    .locals 3

    .prologue
    .line 727
    invoke-virtual {p0}, Lorg/joda/money/BigMoney;->getCurrencyUnit()Lorg/joda/money/CurrencyUnit;

    move-result-object v0

    invoke-virtual {v0}, Lorg/joda/money/CurrencyUnit;->getDecimalPlaces()I

    move-result v0

    .line 728
    iget-object v1, p0, Lorg/joda/money/BigMoney;->amount:Ljava/math/BigDecimal;

    sget-object v2, Ljava/math/RoundingMode;->DOWN:Ljava/math/RoundingMode;

    invoke-virtual {v1, v0, v2}, Ljava/math/BigDecimal;->setScale(ILjava/math/RoundingMode;)Ljava/math/BigDecimal;

    move-result-object v1

    sget-object v2, Ljava/math/BigDecimal;->ONE:Ljava/math/BigDecimal;

    invoke-virtual {v1, v2}, Ljava/math/BigDecimal;->remainder(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/math/BigDecimal;->movePointRight(I)Ljava/math/BigDecimal;

    move-result-object v0

    invoke-virtual {v0}, Ljava/math/BigDecimal;->intValueExact()I

    move-result v0

    return v0
.end method

.method public getScale()I
    .locals 1

    .prologue
    .line 514
    iget-object v0, p0, Lorg/joda/money/BigMoney;->amount:Ljava/math/BigDecimal;

    invoke-virtual {v0}, Ljava/math/BigDecimal;->scale()I

    move-result v0

    return v0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 1711
    iget-object v0, p0, Lorg/joda/money/BigMoney;->currency:Lorg/joda/money/CurrencyUnit;

    invoke-virtual {v0}, Lorg/joda/money/CurrencyUnit;->hashCode()I

    move-result v0

    iget-object v1, p0, Lorg/joda/money/BigMoney;->amount:Ljava/math/BigDecimal;

    invoke-virtual {v1}, Ljava/math/BigDecimal;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    return v0
.end method

.method public isCurrencyScale()Z
    .locals 2

    .prologue
    .line 526
    iget-object v0, p0, Lorg/joda/money/BigMoney;->amount:Ljava/math/BigDecimal;

    invoke-virtual {v0}, Ljava/math/BigDecimal;->scale()I

    move-result v0

    iget-object v1, p0, Lorg/joda/money/BigMoney;->currency:Lorg/joda/money/CurrencyUnit;

    invoke-virtual {v1}, Lorg/joda/money/CurrencyUnit;->getDecimalPlaces()I

    move-result v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isEqual(Lorg/joda/money/BigMoneyProvider;)Z
    .locals 1

    .prologue
    .line 1651
    invoke-virtual {p0, p1}, Lorg/joda/money/BigMoney;->compareTo(Lorg/joda/money/BigMoneyProvider;)I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isGreaterThan(Lorg/joda/money/BigMoneyProvider;)Z
    .locals 1

    .prologue
    .line 1663
    invoke-virtual {p0, p1}, Lorg/joda/money/BigMoney;->compareTo(Lorg/joda/money/BigMoneyProvider;)I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isLessThan(Lorg/joda/money/BigMoneyProvider;)Z
    .locals 1

    .prologue
    .line 1675
    invoke-virtual {p0, p1}, Lorg/joda/money/BigMoney;->compareTo(Lorg/joda/money/BigMoneyProvider;)I

    move-result v0

    if-gez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isNegative()Z
    .locals 2

    .prologue
    .line 767
    iget-object v0, p0, Lorg/joda/money/BigMoney;->amount:Ljava/math/BigDecimal;

    sget-object v1, Ljava/math/BigDecimal;->ZERO:Ljava/math/BigDecimal;

    invoke-virtual {v0, v1}, Ljava/math/BigDecimal;->compareTo(Ljava/math/BigDecimal;)I

    move-result v0

    if-gez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isNegativeOrZero()Z
    .locals 2

    .prologue
    .line 776
    iget-object v0, p0, Lorg/joda/money/BigMoney;->amount:Ljava/math/BigDecimal;

    sget-object v1, Ljava/math/BigDecimal;->ZERO:Ljava/math/BigDecimal;

    invoke-virtual {v0, v1}, Ljava/math/BigDecimal;->compareTo(Ljava/math/BigDecimal;)I

    move-result v0

    if-gtz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isPositive()Z
    .locals 2

    .prologue
    .line 749
    iget-object v0, p0, Lorg/joda/money/BigMoney;->amount:Ljava/math/BigDecimal;

    sget-object v1, Ljava/math/BigDecimal;->ZERO:Ljava/math/BigDecimal;

    invoke-virtual {v0, v1}, Ljava/math/BigDecimal;->compareTo(Ljava/math/BigDecimal;)I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isPositiveOrZero()Z
    .locals 2

    .prologue
    .line 758
    iget-object v0, p0, Lorg/joda/money/BigMoney;->amount:Ljava/math/BigDecimal;

    sget-object v1, Ljava/math/BigDecimal;->ZERO:Ljava/math/BigDecimal;

    invoke-virtual {v0, v1}, Ljava/math/BigDecimal;->compareTo(Ljava/math/BigDecimal;)I

    move-result v0

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isSameCurrency(Lorg/joda/money/BigMoneyProvider;)Z
    .locals 2

    .prologue
    .line 1616
    iget-object v0, p0, Lorg/joda/money/BigMoney;->currency:Lorg/joda/money/CurrencyUnit;

    invoke-static {p1}, Lorg/joda/money/BigMoney;->of(Lorg/joda/money/BigMoneyProvider;)Lorg/joda/money/BigMoney;

    move-result-object v1

    invoke-virtual {v1}, Lorg/joda/money/BigMoney;->getCurrencyUnit()Lorg/joda/money/CurrencyUnit;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/joda/money/CurrencyUnit;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public isZero()Z
    .locals 2

    .prologue
    .line 740
    iget-object v0, p0, Lorg/joda/money/BigMoney;->amount:Ljava/math/BigDecimal;

    sget-object v1, Ljava/math/BigDecimal;->ZERO:Ljava/math/BigDecimal;

    invoke-virtual {v0, v1}, Ljava/math/BigDecimal;->compareTo(Ljava/math/BigDecimal;)I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public minus(D)Lorg/joda/money/BigMoney;
    .locals 3

    .prologue
    .line 1137
    const-wide/16 v0, 0x0

    cmpl-double v0, p1, v0

    if-nez v0, :cond_0

    .line 1141
    :goto_0
    return-object p0

    .line 1140
    :cond_0
    iget-object v0, p0, Lorg/joda/money/BigMoney;->amount:Ljava/math/BigDecimal;

    invoke-static {p1, p2}, Ljava/math/BigDecimal;->valueOf(D)Ljava/math/BigDecimal;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/math/BigDecimal;->subtract(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object v0

    .line 1141
    iget-object v1, p0, Lorg/joda/money/BigMoney;->currency:Lorg/joda/money/CurrencyUnit;

    invoke-static {v1, v0}, Lorg/joda/money/BigMoney;->of(Lorg/joda/money/CurrencyUnit;Ljava/math/BigDecimal;)Lorg/joda/money/BigMoney;

    move-result-object p0

    goto :goto_0
.end method

.method public minus(Ljava/lang/Iterable;)Lorg/joda/money/BigMoney;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<+",
            "Lorg/joda/money/BigMoneyProvider;",
            ">;)",
            "Lorg/joda/money/BigMoney;"
        }
    .end annotation

    .prologue
    .line 1063
    iget-object v0, p0, Lorg/joda/money/BigMoney;->amount:Ljava/math/BigDecimal;

    .line 1064
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move-object v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/joda/money/BigMoneyProvider;

    .line 1065
    invoke-direct {p0, v0}, Lorg/joda/money/BigMoney;->checkCurrencyEqual(Lorg/joda/money/BigMoneyProvider;)Lorg/joda/money/BigMoney;

    move-result-object v0

    .line 1066
    iget-object v0, v0, Lorg/joda/money/BigMoney;->amount:Ljava/math/BigDecimal;

    invoke-virtual {v1, v0}, Ljava/math/BigDecimal;->subtract(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object v0

    move-object v1, v0

    .line 1067
    goto :goto_0

    .line 1068
    :cond_0
    invoke-direct {p0, v1}, Lorg/joda/money/BigMoney;->with(Ljava/math/BigDecimal;)Lorg/joda/money/BigMoney;

    move-result-object v0

    return-object v0
.end method

.method public minus(Ljava/math/BigDecimal;)Lorg/joda/money/BigMoney;
    .locals 2

    .prologue
    .line 1108
    const-string v0, "Amount must not be null"

    invoke-static {p1, v0}, Lorg/joda/money/MoneyUtils;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1109
    sget-object v0, Ljava/math/BigDecimal;->ZERO:Ljava/math/BigDecimal;

    invoke-virtual {p1, v0}, Ljava/math/BigDecimal;->compareTo(Ljava/math/BigDecimal;)I

    move-result v0

    if-nez v0, :cond_0

    .line 1113
    :goto_0
    return-object p0

    .line 1112
    :cond_0
    iget-object v0, p0, Lorg/joda/money/BigMoney;->amount:Ljava/math/BigDecimal;

    invoke-virtual {v0, p1}, Ljava/math/BigDecimal;->subtract(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object v0

    .line 1113
    iget-object v1, p0, Lorg/joda/money/BigMoney;->currency:Lorg/joda/money/CurrencyUnit;

    invoke-static {v1, v0}, Lorg/joda/money/BigMoney;->of(Lorg/joda/money/CurrencyUnit;Ljava/math/BigDecimal;)Lorg/joda/money/BigMoney;

    move-result-object p0

    goto :goto_0
.end method

.method public minus(Lorg/joda/money/BigMoneyProvider;)Lorg/joda/money/BigMoney;
    .locals 1

    .prologue
    .line 1089
    invoke-direct {p0, p1}, Lorg/joda/money/BigMoney;->checkCurrencyEqual(Lorg/joda/money/BigMoneyProvider;)Lorg/joda/money/BigMoney;

    move-result-object v0

    .line 1090
    invoke-virtual {v0}, Lorg/joda/money/BigMoney;->getAmount()Ljava/math/BigDecimal;

    move-result-object v0

    invoke-virtual {p0, v0}, Lorg/joda/money/BigMoney;->minus(Ljava/math/BigDecimal;)Lorg/joda/money/BigMoney;

    move-result-object v0

    return-object v0
.end method

.method public minusMajor(J)Lorg/joda/money/BigMoney;
    .locals 3

    .prologue
    .line 1160
    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-nez v0, :cond_0

    .line 1164
    :goto_0
    return-object p0

    .line 1163
    :cond_0
    iget-object v0, p0, Lorg/joda/money/BigMoney;->amount:Ljava/math/BigDecimal;

    invoke-static {p1, p2}, Ljava/math/BigDecimal;->valueOf(J)Ljava/math/BigDecimal;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/math/BigDecimal;->subtract(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object v0

    .line 1164
    iget-object v1, p0, Lorg/joda/money/BigMoney;->currency:Lorg/joda/money/CurrencyUnit;

    invoke-static {v1, v0}, Lorg/joda/money/BigMoney;->of(Lorg/joda/money/CurrencyUnit;Ljava/math/BigDecimal;)Lorg/joda/money/BigMoney;

    move-result-object p0

    goto :goto_0
.end method

.method public minusMinor(J)Lorg/joda/money/BigMoney;
    .locals 3

    .prologue
    .line 1183
    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-nez v0, :cond_0

    .line 1187
    :goto_0
    return-object p0

    .line 1186
    :cond_0
    iget-object v0, p0, Lorg/joda/money/BigMoney;->amount:Ljava/math/BigDecimal;

    iget-object v1, p0, Lorg/joda/money/BigMoney;->currency:Lorg/joda/money/CurrencyUnit;

    invoke-virtual {v1}, Lorg/joda/money/CurrencyUnit;->getDecimalPlaces()I

    move-result v1

    invoke-static {p1, p2, v1}, Ljava/math/BigDecimal;->valueOf(JI)Ljava/math/BigDecimal;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/math/BigDecimal;->subtract(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object v0

    .line 1187
    iget-object v1, p0, Lorg/joda/money/BigMoney;->currency:Lorg/joda/money/CurrencyUnit;

    invoke-static {v1, v0}, Lorg/joda/money/BigMoney;->of(Lorg/joda/money/CurrencyUnit;Ljava/math/BigDecimal;)Lorg/joda/money/BigMoney;

    move-result-object p0

    goto :goto_0
.end method

.method public minusRetainScale(DLjava/math/RoundingMode;)Lorg/joda/money/BigMoney;
    .locals 3

    .prologue
    .line 1252
    const-wide/16 v0, 0x0

    cmpl-double v0, p1, v0

    if-nez v0, :cond_0

    .line 1257
    :goto_0
    return-object p0

    .line 1255
    :cond_0
    iget-object v0, p0, Lorg/joda/money/BigMoney;->amount:Ljava/math/BigDecimal;

    invoke-static {p1, p2}, Ljava/math/BigDecimal;->valueOf(D)Ljava/math/BigDecimal;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/math/BigDecimal;->subtract(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object v0

    .line 1256
    invoke-virtual {p0}, Lorg/joda/money/BigMoney;->getScale()I

    move-result v1

    invoke-virtual {v0, v1, p3}, Ljava/math/BigDecimal;->setScale(ILjava/math/RoundingMode;)Ljava/math/BigDecimal;

    move-result-object v0

    .line 1257
    iget-object v1, p0, Lorg/joda/money/BigMoney;->currency:Lorg/joda/money/CurrencyUnit;

    invoke-static {v1, v0}, Lorg/joda/money/BigMoney;->of(Lorg/joda/money/CurrencyUnit;Ljava/math/BigDecimal;)Lorg/joda/money/BigMoney;

    move-result-object p0

    goto :goto_0
.end method

.method public minusRetainScale(Ljava/math/BigDecimal;Ljava/math/RoundingMode;)Lorg/joda/money/BigMoney;
    .locals 2

    .prologue
    .line 1223
    const-string v0, "Amount must not be null"

    invoke-static {p1, v0}, Lorg/joda/money/MoneyUtils;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1224
    sget-object v0, Ljava/math/BigDecimal;->ZERO:Ljava/math/BigDecimal;

    invoke-virtual {p1, v0}, Ljava/math/BigDecimal;->compareTo(Ljava/math/BigDecimal;)I

    move-result v0

    if-nez v0, :cond_0

    .line 1229
    :goto_0
    return-object p0

    .line 1227
    :cond_0
    iget-object v0, p0, Lorg/joda/money/BigMoney;->amount:Ljava/math/BigDecimal;

    invoke-virtual {v0, p1}, Ljava/math/BigDecimal;->subtract(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object v0

    .line 1228
    invoke-virtual {p0}, Lorg/joda/money/BigMoney;->getScale()I

    move-result v1

    invoke-virtual {v0, v1, p2}, Ljava/math/BigDecimal;->setScale(ILjava/math/RoundingMode;)Ljava/math/BigDecimal;

    move-result-object v0

    .line 1229
    iget-object v1, p0, Lorg/joda/money/BigMoney;->currency:Lorg/joda/money/CurrencyUnit;

    invoke-static {v1, v0}, Lorg/joda/money/BigMoney;->of(Lorg/joda/money/CurrencyUnit;Ljava/math/BigDecimal;)Lorg/joda/money/BigMoney;

    move-result-object p0

    goto :goto_0
.end method

.method public minusRetainScale(Lorg/joda/money/BigMoneyProvider;Ljava/math/RoundingMode;)Lorg/joda/money/BigMoney;
    .locals 1

    .prologue
    .line 1205
    invoke-direct {p0, p1}, Lorg/joda/money/BigMoney;->checkCurrencyEqual(Lorg/joda/money/BigMoneyProvider;)Lorg/joda/money/BigMoney;

    move-result-object v0

    .line 1206
    invoke-virtual {v0}, Lorg/joda/money/BigMoney;->getAmount()Ljava/math/BigDecimal;

    move-result-object v0

    invoke-virtual {p0, v0, p2}, Lorg/joda/money/BigMoney;->minusRetainScale(Ljava/math/BigDecimal;Ljava/math/RoundingMode;)Lorg/joda/money/BigMoney;

    move-result-object v0

    return-object v0
.end method

.method public multipliedBy(D)Lorg/joda/money/BigMoney;
    .locals 3

    .prologue
    .line 1301
    const-wide/high16 v0, 0x3ff0000000000000L    # 1.0

    cmpl-double v0, p1, v0

    if-nez v0, :cond_0

    .line 1305
    :goto_0
    return-object p0

    .line 1304
    :cond_0
    iget-object v0, p0, Lorg/joda/money/BigMoney;->amount:Ljava/math/BigDecimal;

    invoke-static {p1, p2}, Ljava/math/BigDecimal;->valueOf(D)Ljava/math/BigDecimal;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/math/BigDecimal;->multiply(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object v0

    .line 1305
    iget-object v1, p0, Lorg/joda/money/BigMoney;->currency:Lorg/joda/money/CurrencyUnit;

    invoke-static {v1, v0}, Lorg/joda/money/BigMoney;->of(Lorg/joda/money/CurrencyUnit;Ljava/math/BigDecimal;)Lorg/joda/money/BigMoney;

    move-result-object p0

    goto :goto_0
.end method

.method public multipliedBy(J)Lorg/joda/money/BigMoney;
    .locals 3

    .prologue
    .line 1321
    const-wide/16 v0, 0x1

    cmp-long v0, p1, v0

    if-nez v0, :cond_0

    .line 1325
    :goto_0
    return-object p0

    .line 1324
    :cond_0
    iget-object v0, p0, Lorg/joda/money/BigMoney;->amount:Ljava/math/BigDecimal;

    invoke-static {p1, p2}, Ljava/math/BigDecimal;->valueOf(J)Ljava/math/BigDecimal;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/math/BigDecimal;->multiply(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object v0

    .line 1325
    iget-object v1, p0, Lorg/joda/money/BigMoney;->currency:Lorg/joda/money/CurrencyUnit;

    invoke-static {v1, v0}, Lorg/joda/money/BigMoney;->of(Lorg/joda/money/CurrencyUnit;Ljava/math/BigDecimal;)Lorg/joda/money/BigMoney;

    move-result-object p0

    goto :goto_0
.end method

.method public multipliedBy(Ljava/math/BigDecimal;)Lorg/joda/money/BigMoney;
    .locals 2

    .prologue
    .line 1274
    const-string v0, "Multiplier must not be null"

    invoke-static {p1, v0}, Lorg/joda/money/MoneyUtils;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1275
    sget-object v0, Ljava/math/BigDecimal;->ONE:Ljava/math/BigDecimal;

    invoke-virtual {p1, v0}, Ljava/math/BigDecimal;->compareTo(Ljava/math/BigDecimal;)I

    move-result v0

    if-nez v0, :cond_0

    .line 1279
    :goto_0
    return-object p0

    .line 1278
    :cond_0
    iget-object v0, p0, Lorg/joda/money/BigMoney;->amount:Ljava/math/BigDecimal;

    invoke-virtual {v0, p1}, Ljava/math/BigDecimal;->multiply(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object v0

    .line 1279
    iget-object v1, p0, Lorg/joda/money/BigMoney;->currency:Lorg/joda/money/CurrencyUnit;

    invoke-static {v1, v0}, Lorg/joda/money/BigMoney;->of(Lorg/joda/money/CurrencyUnit;Ljava/math/BigDecimal;)Lorg/joda/money/BigMoney;

    move-result-object p0

    goto :goto_0
.end method

.method public multiplyRetainScale(DLjava/math/RoundingMode;)Lorg/joda/money/BigMoney;
    .locals 1

    .prologue
    .line 1377
    invoke-static {p1, p2}, Ljava/math/BigDecimal;->valueOf(D)Ljava/math/BigDecimal;

    move-result-object v0

    invoke-virtual {p0, v0, p3}, Lorg/joda/money/BigMoney;->multiplyRetainScale(Ljava/math/BigDecimal;Ljava/math/RoundingMode;)Lorg/joda/money/BigMoney;

    move-result-object v0

    return-object v0
.end method

.method public multiplyRetainScale(Ljava/math/BigDecimal;Ljava/math/RoundingMode;)Lorg/joda/money/BigMoney;
    .locals 2

    .prologue
    .line 1345
    const-string v0, "Multiplier must not be null"

    invoke-static {p1, v0}, Lorg/joda/money/MoneyUtils;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1346
    const-string v0, "RoundingMode must not be null"

    invoke-static {p2, v0}, Lorg/joda/money/MoneyUtils;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1347
    sget-object v0, Ljava/math/BigDecimal;->ONE:Ljava/math/BigDecimal;

    invoke-virtual {p1, v0}, Ljava/math/BigDecimal;->compareTo(Ljava/math/BigDecimal;)I

    move-result v0

    if-nez v0, :cond_0

    .line 1352
    :goto_0
    return-object p0

    .line 1350
    :cond_0
    iget-object v0, p0, Lorg/joda/money/BigMoney;->amount:Ljava/math/BigDecimal;

    invoke-virtual {v0, p1}, Ljava/math/BigDecimal;->multiply(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object v0

    .line 1351
    invoke-virtual {p0}, Lorg/joda/money/BigMoney;->getScale()I

    move-result v1

    invoke-virtual {v0, v1, p2}, Ljava/math/BigDecimal;->setScale(ILjava/math/RoundingMode;)Ljava/math/BigDecimal;

    move-result-object v0

    .line 1352
    iget-object v1, p0, Lorg/joda/money/BigMoney;->currency:Lorg/joda/money/CurrencyUnit;

    invoke-static {v1, v0}, Lorg/joda/money/BigMoney;->of(Lorg/joda/money/CurrencyUnit;Ljava/math/BigDecimal;)Lorg/joda/money/BigMoney;

    move-result-object p0

    goto :goto_0
.end method

.method public negated()Lorg/joda/money/BigMoney;
    .locals 2

    .prologue
    .line 1470
    invoke-virtual {p0}, Lorg/joda/money/BigMoney;->isZero()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1473
    :goto_0
    return-object p0

    :cond_0
    iget-object v0, p0, Lorg/joda/money/BigMoney;->currency:Lorg/joda/money/CurrencyUnit;

    iget-object v1, p0, Lorg/joda/money/BigMoney;->amount:Ljava/math/BigDecimal;

    invoke-virtual {v1}, Ljava/math/BigDecimal;->negate()Ljava/math/BigDecimal;

    move-result-object v1

    invoke-static {v0, v1}, Lorg/joda/money/BigMoney;->of(Lorg/joda/money/CurrencyUnit;Ljava/math/BigDecimal;)Lorg/joda/money/BigMoney;

    move-result-object p0

    goto :goto_0
.end method

.method public plus(D)Lorg/joda/money/BigMoney;
    .locals 3

    .prologue
    .line 925
    const-wide/16 v0, 0x0

    cmpl-double v0, p1, v0

    if-nez v0, :cond_0

    .line 929
    :goto_0
    return-object p0

    .line 928
    :cond_0
    iget-object v0, p0, Lorg/joda/money/BigMoney;->amount:Ljava/math/BigDecimal;

    invoke-static {p1, p2}, Ljava/math/BigDecimal;->valueOf(D)Ljava/math/BigDecimal;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/math/BigDecimal;->add(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object v0

    .line 929
    iget-object v1, p0, Lorg/joda/money/BigMoney;->currency:Lorg/joda/money/CurrencyUnit;

    invoke-static {v1, v0}, Lorg/joda/money/BigMoney;->of(Lorg/joda/money/CurrencyUnit;Ljava/math/BigDecimal;)Lorg/joda/money/BigMoney;

    move-result-object p0

    goto :goto_0
.end method

.method public plus(Ljava/lang/Iterable;)Lorg/joda/money/BigMoney;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<+",
            "Lorg/joda/money/BigMoneyProvider;",
            ">;)",
            "Lorg/joda/money/BigMoney;"
        }
    .end annotation

    .prologue
    .line 851
    iget-object v0, p0, Lorg/joda/money/BigMoney;->amount:Ljava/math/BigDecimal;

    .line 852
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move-object v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/joda/money/BigMoneyProvider;

    .line 853
    invoke-direct {p0, v0}, Lorg/joda/money/BigMoney;->checkCurrencyEqual(Lorg/joda/money/BigMoneyProvider;)Lorg/joda/money/BigMoney;

    move-result-object v0

    .line 854
    iget-object v0, v0, Lorg/joda/money/BigMoney;->amount:Ljava/math/BigDecimal;

    invoke-virtual {v1, v0}, Ljava/math/BigDecimal;->add(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object v0

    move-object v1, v0

    .line 855
    goto :goto_0

    .line 856
    :cond_0
    invoke-direct {p0, v1}, Lorg/joda/money/BigMoney;->with(Ljava/math/BigDecimal;)Lorg/joda/money/BigMoney;

    move-result-object v0

    return-object v0
.end method

.method public plus(Ljava/math/BigDecimal;)Lorg/joda/money/BigMoney;
    .locals 2

    .prologue
    .line 896
    const-string v0, "Amount must not be null"

    invoke-static {p1, v0}, Lorg/joda/money/MoneyUtils;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 897
    sget-object v0, Ljava/math/BigDecimal;->ZERO:Ljava/math/BigDecimal;

    invoke-virtual {p1, v0}, Ljava/math/BigDecimal;->compareTo(Ljava/math/BigDecimal;)I

    move-result v0

    if-nez v0, :cond_0

    .line 901
    :goto_0
    return-object p0

    .line 900
    :cond_0
    iget-object v0, p0, Lorg/joda/money/BigMoney;->amount:Ljava/math/BigDecimal;

    invoke-virtual {v0, p1}, Ljava/math/BigDecimal;->add(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object v0

    .line 901
    iget-object v1, p0, Lorg/joda/money/BigMoney;->currency:Lorg/joda/money/CurrencyUnit;

    invoke-static {v1, v0}, Lorg/joda/money/BigMoney;->of(Lorg/joda/money/CurrencyUnit;Ljava/math/BigDecimal;)Lorg/joda/money/BigMoney;

    move-result-object p0

    goto :goto_0
.end method

.method public plus(Lorg/joda/money/BigMoneyProvider;)Lorg/joda/money/BigMoney;
    .locals 1

    .prologue
    .line 877
    invoke-direct {p0, p1}, Lorg/joda/money/BigMoney;->checkCurrencyEqual(Lorg/joda/money/BigMoneyProvider;)Lorg/joda/money/BigMoney;

    move-result-object v0

    .line 878
    invoke-virtual {v0}, Lorg/joda/money/BigMoney;->getAmount()Ljava/math/BigDecimal;

    move-result-object v0

    invoke-virtual {p0, v0}, Lorg/joda/money/BigMoney;->plus(Ljava/math/BigDecimal;)Lorg/joda/money/BigMoney;

    move-result-object v0

    return-object v0
.end method

.method public plusMajor(J)Lorg/joda/money/BigMoney;
    .locals 3

    .prologue
    .line 948
    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-nez v0, :cond_0

    .line 952
    :goto_0
    return-object p0

    .line 951
    :cond_0
    iget-object v0, p0, Lorg/joda/money/BigMoney;->amount:Ljava/math/BigDecimal;

    invoke-static {p1, p2}, Ljava/math/BigDecimal;->valueOf(J)Ljava/math/BigDecimal;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/math/BigDecimal;->add(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object v0

    .line 952
    iget-object v1, p0, Lorg/joda/money/BigMoney;->currency:Lorg/joda/money/CurrencyUnit;

    invoke-static {v1, v0}, Lorg/joda/money/BigMoney;->of(Lorg/joda/money/CurrencyUnit;Ljava/math/BigDecimal;)Lorg/joda/money/BigMoney;

    move-result-object p0

    goto :goto_0
.end method

.method public plusMinor(J)Lorg/joda/money/BigMoney;
    .locals 3

    .prologue
    .line 971
    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-nez v0, :cond_0

    .line 975
    :goto_0
    return-object p0

    .line 974
    :cond_0
    iget-object v0, p0, Lorg/joda/money/BigMoney;->amount:Ljava/math/BigDecimal;

    iget-object v1, p0, Lorg/joda/money/BigMoney;->currency:Lorg/joda/money/CurrencyUnit;

    invoke-virtual {v1}, Lorg/joda/money/CurrencyUnit;->getDecimalPlaces()I

    move-result v1

    invoke-static {p1, p2, v1}, Ljava/math/BigDecimal;->valueOf(JI)Ljava/math/BigDecimal;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/math/BigDecimal;->add(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object v0

    .line 975
    iget-object v1, p0, Lorg/joda/money/BigMoney;->currency:Lorg/joda/money/CurrencyUnit;

    invoke-static {v1, v0}, Lorg/joda/money/BigMoney;->of(Lorg/joda/money/CurrencyUnit;Ljava/math/BigDecimal;)Lorg/joda/money/BigMoney;

    move-result-object p0

    goto :goto_0
.end method

.method public plusRetainScale(DLjava/math/RoundingMode;)Lorg/joda/money/BigMoney;
    .locals 3

    .prologue
    .line 1040
    const-wide/16 v0, 0x0

    cmpl-double v0, p1, v0

    if-nez v0, :cond_0

    .line 1045
    :goto_0
    return-object p0

    .line 1043
    :cond_0
    iget-object v0, p0, Lorg/joda/money/BigMoney;->amount:Ljava/math/BigDecimal;

    invoke-static {p1, p2}, Ljava/math/BigDecimal;->valueOf(D)Ljava/math/BigDecimal;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/math/BigDecimal;->add(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object v0

    .line 1044
    invoke-virtual {p0}, Lorg/joda/money/BigMoney;->getScale()I

    move-result v1

    invoke-virtual {v0, v1, p3}, Ljava/math/BigDecimal;->setScale(ILjava/math/RoundingMode;)Ljava/math/BigDecimal;

    move-result-object v0

    .line 1045
    iget-object v1, p0, Lorg/joda/money/BigMoney;->currency:Lorg/joda/money/CurrencyUnit;

    invoke-static {v1, v0}, Lorg/joda/money/BigMoney;->of(Lorg/joda/money/CurrencyUnit;Ljava/math/BigDecimal;)Lorg/joda/money/BigMoney;

    move-result-object p0

    goto :goto_0
.end method

.method public plusRetainScale(Ljava/math/BigDecimal;Ljava/math/RoundingMode;)Lorg/joda/money/BigMoney;
    .locals 2

    .prologue
    .line 1011
    const-string v0, "Amount must not be null"

    invoke-static {p1, v0}, Lorg/joda/money/MoneyUtils;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1012
    sget-object v0, Ljava/math/BigDecimal;->ZERO:Ljava/math/BigDecimal;

    invoke-virtual {p1, v0}, Ljava/math/BigDecimal;->compareTo(Ljava/math/BigDecimal;)I

    move-result v0

    if-nez v0, :cond_0

    .line 1017
    :goto_0
    return-object p0

    .line 1015
    :cond_0
    iget-object v0, p0, Lorg/joda/money/BigMoney;->amount:Ljava/math/BigDecimal;

    invoke-virtual {v0, p1}, Ljava/math/BigDecimal;->add(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object v0

    .line 1016
    invoke-virtual {p0}, Lorg/joda/money/BigMoney;->getScale()I

    move-result v1

    invoke-virtual {v0, v1, p2}, Ljava/math/BigDecimal;->setScale(ILjava/math/RoundingMode;)Ljava/math/BigDecimal;

    move-result-object v0

    .line 1017
    iget-object v1, p0, Lorg/joda/money/BigMoney;->currency:Lorg/joda/money/CurrencyUnit;

    invoke-static {v1, v0}, Lorg/joda/money/BigMoney;->of(Lorg/joda/money/CurrencyUnit;Ljava/math/BigDecimal;)Lorg/joda/money/BigMoney;

    move-result-object p0

    goto :goto_0
.end method

.method public plusRetainScale(Lorg/joda/money/BigMoneyProvider;Ljava/math/RoundingMode;)Lorg/joda/money/BigMoney;
    .locals 1

    .prologue
    .line 993
    invoke-direct {p0, p1}, Lorg/joda/money/BigMoney;->checkCurrencyEqual(Lorg/joda/money/BigMoneyProvider;)Lorg/joda/money/BigMoney;

    move-result-object v0

    .line 994
    invoke-virtual {v0}, Lorg/joda/money/BigMoney;->getAmount()Ljava/math/BigDecimal;

    move-result-object v0

    invoke-virtual {p0, v0, p2}, Lorg/joda/money/BigMoney;->plusRetainScale(Ljava/math/BigDecimal;Ljava/math/RoundingMode;)Lorg/joda/money/BigMoney;

    move-result-object v0

    return-object v0
.end method

.method public rounded(ILjava/math/RoundingMode;)Lorg/joda/money/BigMoney;
    .locals 2

    .prologue
    .line 1510
    const-string v0, "RoundingMode must not be null"

    invoke-static {p2, v0}, Lorg/joda/money/MoneyUtils;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1511
    invoke-virtual {p0}, Lorg/joda/money/BigMoney;->getScale()I

    move-result v0

    if-lt p1, v0, :cond_0

    .line 1516
    :goto_0
    return-object p0

    .line 1514
    :cond_0
    iget-object v0, p0, Lorg/joda/money/BigMoney;->amount:Ljava/math/BigDecimal;

    invoke-virtual {v0}, Ljava/math/BigDecimal;->scale()I

    move-result v0

    .line 1515
    iget-object v1, p0, Lorg/joda/money/BigMoney;->amount:Ljava/math/BigDecimal;

    invoke-virtual {v1, p1, p2}, Ljava/math/BigDecimal;->setScale(ILjava/math/RoundingMode;)Ljava/math/BigDecimal;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/math/BigDecimal;->setScale(I)Ljava/math/BigDecimal;

    move-result-object v0

    .line 1516
    iget-object v1, p0, Lorg/joda/money/BigMoney;->currency:Lorg/joda/money/CurrencyUnit;

    invoke-static {v1, v0}, Lorg/joda/money/BigMoney;->of(Lorg/joda/money/CurrencyUnit;Ljava/math/BigDecimal;)Lorg/joda/money/BigMoney;

    move-result-object p0

    goto :goto_0
.end method

.method public toBigMoney()Lorg/joda/money/BigMoney;
    .locals 0

    .prologue
    .line 1583
    return-object p0
.end method

.method public toMoney()Lorg/joda/money/Money;
    .locals 1

    .prologue
    .line 1594
    invoke-static {p0}, Lorg/joda/money/Money;->of(Lorg/joda/money/BigMoneyProvider;)Lorg/joda/money/Money;

    move-result-object v0

    return-object v0
.end method

.method public toMoney(Ljava/math/RoundingMode;)Lorg/joda/money/Money;
    .locals 1

    .prologue
    .line 1605
    invoke-static {p0, p1}, Lorg/joda/money/Money;->of(Lorg/joda/money/BigMoneyProvider;Ljava/math/RoundingMode;)Lorg/joda/money/Money;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2
    .annotation runtime Lorg/joda/convert/ToString;
    .end annotation

    .prologue
    .line 1726
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lorg/joda/money/BigMoney;->currency:Lorg/joda/money/CurrencyUnit;

    invoke-virtual {v1}, Lorg/joda/money/CurrencyUnit;->getCode()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x20

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lorg/joda/money/BigMoney;->amount:Ljava/math/BigDecimal;

    invoke-virtual {v1}, Ljava/math/BigDecimal;->toPlainString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public withAmount(D)Lorg/joda/money/BigMoney;
    .locals 1

    .prologue
    .line 818
    invoke-static {p1, p2}, Ljava/math/BigDecimal;->valueOf(D)Ljava/math/BigDecimal;

    move-result-object v0

    invoke-virtual {p0, v0}, Lorg/joda/money/BigMoney;->withAmount(Ljava/math/BigDecimal;)Lorg/joda/money/BigMoney;

    move-result-object v0

    return-object v0
.end method

.method public withAmount(Ljava/math/BigDecimal;)Lorg/joda/money/BigMoney;
    .locals 1

    .prologue
    .line 792
    const-string v0, "Amount must not be null"

    invoke-static {p1, v0}, Lorg/joda/money/MoneyUtils;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 793
    iget-object v0, p0, Lorg/joda/money/BigMoney;->amount:Ljava/math/BigDecimal;

    invoke-virtual {v0, p1}, Ljava/math/BigDecimal;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 796
    :goto_0
    return-object p0

    :cond_0
    iget-object v0, p0, Lorg/joda/money/BigMoney;->currency:Lorg/joda/money/CurrencyUnit;

    invoke-static {v0, p1}, Lorg/joda/money/BigMoney;->of(Lorg/joda/money/CurrencyUnit;Ljava/math/BigDecimal;)Lorg/joda/money/BigMoney;

    move-result-object p0

    goto :goto_0
.end method

.method public withCurrencyScale()Lorg/joda/money/BigMoney;
    .locals 2

    .prologue
    .line 590
    iget-object v0, p0, Lorg/joda/money/BigMoney;->currency:Lorg/joda/money/CurrencyUnit;

    invoke-virtual {v0}, Lorg/joda/money/CurrencyUnit;->getDecimalPlaces()I

    move-result v0

    sget-object v1, Ljava/math/RoundingMode;->UNNECESSARY:Ljava/math/RoundingMode;

    invoke-virtual {p0, v0, v1}, Lorg/joda/money/BigMoney;->withScale(ILjava/math/RoundingMode;)Lorg/joda/money/BigMoney;

    move-result-object v0

    return-object v0
.end method

.method public withCurrencyScale(Ljava/math/RoundingMode;)Lorg/joda/money/BigMoney;
    .locals 1

    .prologue
    .line 607
    iget-object v0, p0, Lorg/joda/money/BigMoney;->currency:Lorg/joda/money/CurrencyUnit;

    invoke-virtual {v0}, Lorg/joda/money/CurrencyUnit;->getDecimalPlaces()I

    move-result v0

    invoke-virtual {p0, v0, p1}, Lorg/joda/money/BigMoney;->withScale(ILjava/math/RoundingMode;)Lorg/joda/money/BigMoney;

    move-result-object v0

    return-object v0
.end method

.method public withCurrencyUnit(Lorg/joda/money/CurrencyUnit;)Lorg/joda/money/BigMoney;
    .locals 2

    .prologue
    .line 493
    const-string v0, "CurrencyUnit must not be null"

    invoke-static {p1, v0}, Lorg/joda/money/MoneyUtils;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 494
    iget-object v0, p0, Lorg/joda/money/BigMoney;->currency:Lorg/joda/money/CurrencyUnit;

    if-ne v0, p1, :cond_0

    .line 497
    :goto_0
    return-object p0

    :cond_0
    new-instance v0, Lorg/joda/money/BigMoney;

    iget-object v1, p0, Lorg/joda/money/BigMoney;->amount:Ljava/math/BigDecimal;

    invoke-direct {v0, p1, v1}, Lorg/joda/money/BigMoney;-><init>(Lorg/joda/money/CurrencyUnit;Ljava/math/BigDecimal;)V

    move-object p0, v0

    goto :goto_0
.end method

.method public withScale(I)Lorg/joda/money/BigMoney;
    .locals 1

    .prologue
    .line 547
    sget-object v0, Ljava/math/RoundingMode;->UNNECESSARY:Ljava/math/RoundingMode;

    invoke-virtual {p0, p1, v0}, Lorg/joda/money/BigMoney;->withScale(ILjava/math/RoundingMode;)Lorg/joda/money/BigMoney;

    move-result-object v0

    return-object v0
.end method

.method public withScale(ILjava/math/RoundingMode;)Lorg/joda/money/BigMoney;
    .locals 2

    .prologue
    .line 567
    const-string v0, "RoundingMode must not be null"

    invoke-static {p2, v0}, Lorg/joda/money/MoneyUtils;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 568
    iget-object v0, p0, Lorg/joda/money/BigMoney;->amount:Ljava/math/BigDecimal;

    invoke-virtual {v0}, Ljava/math/BigDecimal;->scale()I

    move-result v0

    if-ne p1, v0, :cond_0

    .line 571
    :goto_0
    return-object p0

    :cond_0
    iget-object v0, p0, Lorg/joda/money/BigMoney;->currency:Lorg/joda/money/CurrencyUnit;

    iget-object v1, p0, Lorg/joda/money/BigMoney;->amount:Ljava/math/BigDecimal;

    invoke-virtual {v1, p1, p2}, Ljava/math/BigDecimal;->setScale(ILjava/math/RoundingMode;)Ljava/math/BigDecimal;

    move-result-object v1

    invoke-static {v0, v1}, Lorg/joda/money/BigMoney;->of(Lorg/joda/money/CurrencyUnit;Ljava/math/BigDecimal;)Lorg/joda/money/BigMoney;

    move-result-object p0

    goto :goto_0
.end method
