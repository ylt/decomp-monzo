.class Lorg/joda/money/DefaultCurrencyUnitDataProvider;
.super Lorg/joda/money/CurrencyUnitDataProvider;
.source "DefaultCurrencyUnitDataProvider.java"


# static fields
.field private static final REGEX_LINE:Ljava/util/regex/Pattern;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 40
    const-string v0, "([A-Z]{3}),(-1|[0-9]{1,3}),(-1|[0-9]),([A-Z]*)#?.*"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lorg/joda/money/DefaultCurrencyUnitDataProvider;->REGEX_LINE:Ljava/util/regex/Pattern;

    return-void
.end method

.method constructor <init>()V
    .locals 0

    .prologue
    .line 37
    invoke-direct {p0}, Lorg/joda/money/CurrencyUnitDataProvider;-><init>()V

    return-void
.end method

.method private loadCurrenciesFromFile(Ljava/lang/String;Z)V
    .locals 11
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    const/4 v9, 0x1

    .line 61
    .line 64
    :try_start_0
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/Class;->getResourceAsStream(Ljava/lang/String;)Ljava/io/InputStream;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_4
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    move-result-object v1

    .line 65
    if-nez v1, :cond_1

    if-eqz p2, :cond_1

    .line 66
    :try_start_1
    new-instance v0, Ljava/io/FileNotFoundException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Data file "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " not found"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v3}, Ljava/io/FileNotFoundException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 89
    :catch_0
    move-exception v0

    .line 91
    :goto_0
    :try_start_2
    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 93
    :catchall_0
    move-exception v2

    move-object v10, v2

    move-object v2, v0

    move-object v0, v10

    :goto_1
    if-eqz v1, :cond_0

    .line 94
    if-eqz v2, :cond_9

    .line 96
    :try_start_3
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_3

    .line 101
    :cond_0
    :goto_2
    throw v0

    .line 67
    :cond_1
    if-nez v1, :cond_4

    if-nez p2, :cond_4

    .line 93
    if-eqz v1, :cond_2

    .line 94
    if-eqz v2, :cond_3

    .line 96
    :try_start_4
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1

    .line 105
    :cond_2
    :goto_3
    return-void

    .line 97
    :catch_1
    move-exception v0

    .line 98
    throw v2

    .line 101
    :cond_3
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V

    goto :goto_3

    .line 70
    :cond_4
    :try_start_5
    new-instance v3, Ljava/io/BufferedReader;

    new-instance v0, Ljava/io/InputStreamReader;

    const-string v4, "UTF-8"

    invoke-direct {v0, v1, v4}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;Ljava/lang/String;)V

    invoke-direct {v3, v0}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    .line 72
    :cond_5
    :goto_4
    invoke-virtual {v3}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_7

    .line 73
    sget-object v4, Lorg/joda/money/DefaultCurrencyUnitDataProvider;->REGEX_LINE:Ljava/util/regex/Pattern;

    invoke-virtual {v4, v0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v4

    .line 74
    invoke-virtual {v4}, Ljava/util/regex/Matcher;->matches()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 75
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 76
    const/4 v0, 0x4

    invoke-virtual {v4, v0}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v6

    .line 77
    const/4 v0, 0x1

    invoke-virtual {v4, v0}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v7

    .line 78
    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v0

    rem-int/lit8 v0, v0, 0x2

    if-eq v0, v9, :cond_5

    .line 81
    const/4 v0, 0x0

    :goto_5
    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v8

    if-ge v0, v8, :cond_6

    .line 82
    add-int/lit8 v8, v0, 0x2

    invoke-virtual {v6, v0, v8}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v8

    invoke-interface {v5, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 81
    add-int/lit8 v0, v0, 0x2

    goto :goto_5

    .line 84
    :cond_6
    const/4 v0, 0x2

    invoke-virtual {v4, v0}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    .line 85
    const/4 v6, 0x3

    invoke-virtual {v4, v6}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    .line 86
    invoke-virtual {p0, v7, v0, v4, v5}, Lorg/joda/money/DefaultCurrencyUnitDataProvider;->registerCurrency(Ljava/lang/String;IILjava/util/List;)V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    goto :goto_4

    .line 93
    :catchall_1
    move-exception v0

    goto :goto_1

    :cond_7
    if-eqz v1, :cond_2

    .line 94
    if-eqz v2, :cond_8

    .line 96
    :try_start_6
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_2

    goto :goto_3

    .line 97
    :catch_2
    move-exception v0

    .line 98
    throw v2

    .line 101
    :cond_8
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V

    goto :goto_3

    .line 97
    :catch_3
    move-exception v0

    .line 98
    throw v2

    .line 101
    :cond_9
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V

    goto/16 :goto_2

    .line 93
    :catchall_2
    move-exception v0

    move-object v1, v2

    goto/16 :goto_1

    .line 89
    :catch_4
    move-exception v0

    move-object v1, v2

    goto/16 :goto_0
.end method


# virtual methods
.method protected registerCurrencies()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 49
    const-string v0, "/org/joda/money/MoneyData.csv"

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lorg/joda/money/DefaultCurrencyUnitDataProvider;->loadCurrenciesFromFile(Ljava/lang/String;Z)V

    .line 50
    const-string v0, "/org/joda/money/MoneyDataExtension.csv"

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lorg/joda/money/DefaultCurrencyUnitDataProvider;->loadCurrenciesFromFile(Ljava/lang/String;Z)V

    .line 51
    return-void
.end method
