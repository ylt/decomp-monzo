.class public final Lco/uk/getmondo/feed/welcome/WelcomeToMonzoActivity;
.super Lco/uk/getmondo/common/activities/b;
.source "WelcomeToMonzoActivity.kt"

# interfaces
.implements Lco/uk/getmondo/feed/welcome/b$a;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lco/uk/getmondo/feed/welcome/WelcomeToMonzoActivity$a;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u00006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0010\u000e\n\u0002\u0008\u0003\u0018\u0000 \u00172\u00020\u00012\u00020\u0002:\u0001\u0017B\u0005\u00a2\u0006\u0002\u0010\u0003J\u0012\u0010\n\u001a\u00020\u000b2\u0008\u0010\u000c\u001a\u0004\u0018\u00010\rH\u0014J\u0008\u0010\u000e\u001a\u00020\u000bH\u0014J\u000e\u0010\u000f\u001a\u0008\u0012\u0004\u0012\u00020\u000b0\u0010H\u0016J\u000e\u0010\u0011\u001a\u0008\u0012\u0004\u0012\u00020\u000b0\u0010H\u0016J\u0008\u0010\u0012\u001a\u00020\u000bH\u0016J\u0018\u0010\u0013\u001a\u00020\u000b2\u0006\u0010\u0014\u001a\u00020\u00152\u0006\u0010\u0016\u001a\u00020\u0015H\u0016R\u001e\u0010\u0004\u001a\u00020\u00058\u0006@\u0006X\u0087.\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u0006\u0010\u0007\"\u0004\u0008\u0008\u0010\t\u00a8\u0006\u0018"
    }
    d2 = {
        "Lco/uk/getmondo/feed/welcome/WelcomeToMonzoActivity;",
        "Lco/uk/getmondo/common/activities/BaseActivity;",
        "Lco/uk/getmondo/feed/welcome/WelcomeToMonzoPresenter$View;",
        "()V",
        "presenter",
        "Lco/uk/getmondo/feed/welcome/WelcomeToMonzoPresenter;",
        "getPresenter",
        "()Lco/uk/getmondo/feed/welcome/WelcomeToMonzoPresenter;",
        "setPresenter",
        "(Lco/uk/getmondo/feed/welcome/WelcomeToMonzoPresenter;)V",
        "onCreate",
        "",
        "savedInstanceState",
        "Landroid/os/Bundle;",
        "onDestroy",
        "onNotNowClicked",
        "Lio/reactivex/Observable;",
        "onShowMeClicked",
        "openCard",
        "showSortCodeAndAccountNumber",
        "sortCode",
        "",
        "accountNumber",
        "Companion",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# static fields
.field public static final b:Lco/uk/getmondo/feed/welcome/WelcomeToMonzoActivity$a;


# instance fields
.field public a:Lco/uk/getmondo/feed/welcome/b;

.field private c:Ljava/util/HashMap;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lco/uk/getmondo/feed/welcome/WelcomeToMonzoActivity$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lco/uk/getmondo/feed/welcome/WelcomeToMonzoActivity$a;-><init>(Lkotlin/d/b/i;)V

    sput-object v0, Lco/uk/getmondo/feed/welcome/WelcomeToMonzoActivity;->b:Lco/uk/getmondo/feed/welcome/WelcomeToMonzoActivity$a;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Lco/uk/getmondo/common/activities/b;-><init>()V

    return-void
.end method

.method public static final a(Landroid/content/Context;)Landroid/content/Intent;
    .locals 1

    const-string v0, "context"

    invoke-static {p0, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v0, Lco/uk/getmondo/feed/welcome/WelcomeToMonzoActivity;->b:Lco/uk/getmondo/feed/welcome/WelcomeToMonzoActivity$a;

    invoke-virtual {v0, p0}, Lco/uk/getmondo/feed/welcome/WelcomeToMonzoActivity$a;->a(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public a(I)Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lco/uk/getmondo/feed/welcome/WelcomeToMonzoActivity;->c:Ljava/util/HashMap;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lco/uk/getmondo/feed/welcome/WelcomeToMonzoActivity;->c:Ljava/util/HashMap;

    :cond_0
    iget-object v0, p0, Lco/uk/getmondo/feed/welcome/WelcomeToMonzoActivity;->c:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    if-nez v0, :cond_1

    invoke-virtual {p0, p1}, Landroid/support/v4/app/j;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/feed/welcome/WelcomeToMonzoActivity;->c:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    return-object v0
.end method

.method public a()Lio/reactivex/n;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Lkotlin/n;",
            ">;"
        }
    .end annotation

    .prologue
    .line 34
    sget v0, Lco/uk/getmondo/c$a;->welcomeShowMeButton:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/feed/welcome/WelcomeToMonzoActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 58
    invoke-static {v0}, Lcom/b/a/c/c;->a(Landroid/view/View;)Lio/reactivex/n;

    move-result-object v1

    sget-object v0, Lcom/b/a/a/d;->a:Lcom/b/a/a/d;

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {v1, v0}, Lio/reactivex/n;->map(Lio/reactivex/c/h;)Lio/reactivex/n;

    move-result-object v0

    const-string v1, "RxView.clicks(this).map(VoidToUnit)"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    const-string v0, "sortCode"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "accountNumber"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 42
    sget v0, Lco/uk/getmondo/c$a;->welcomeSortCodeTextView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/feed/welcome/WelcomeToMonzoActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    check-cast p1, Ljava/lang/CharSequence;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 43
    sget v0, Lco/uk/getmondo/c$a;->welcomeAccountNumberTextView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/feed/welcome/WelcomeToMonzoActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    check-cast p2, Ljava/lang/CharSequence;

    invoke-virtual {v0, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 44
    return-void
.end method

.method public b()Lio/reactivex/n;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Lkotlin/n;",
            ">;"
        }
    .end annotation

    .prologue
    .line 38
    sget v0, Lco/uk/getmondo/c$a;->welcomeNotRightNowButton:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/feed/welcome/WelcomeToMonzoActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 59
    invoke-static {v0}, Lcom/b/a/c/c;->a(Landroid/view/View;)Lio/reactivex/n;

    move-result-object v1

    sget-object v0, Lcom/b/a/a/d;->a:Lcom/b/a/a/d;

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {v1, v0}, Lio/reactivex/n;->map(Lio/reactivex/c/h;)Lio/reactivex/n;

    move-result-object v0

    const-string v1, "RxView.clicks(this).map(VoidToUnit)"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public c()V
    .locals 3

    .prologue
    .line 47
    sget-object v1, Lco/uk/getmondo/main/HomeActivity;->f:Lco/uk/getmondo/main/HomeActivity$a;

    move-object v0, p0

    check-cast v0, Landroid/content/Context;

    sget-object v2, Lco/uk/getmondo/main/g;->c:Lco/uk/getmondo/main/g;

    invoke-virtual {v1, v0, v2}, Lco/uk/getmondo/main/HomeActivity$a;->a(Landroid/content/Context;Lco/uk/getmondo/main/g;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lco/uk/getmondo/feed/welcome/WelcomeToMonzoActivity;->startActivity(Landroid/content/Intent;)V

    .line 48
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 19
    invoke-super {p0, p1}, Lco/uk/getmondo/common/activities/b;->onCreate(Landroid/os/Bundle;)V

    .line 21
    const v0, 0x7f050077

    invoke-virtual {p0, v0}, Lco/uk/getmondo/feed/welcome/WelcomeToMonzoActivity;->setContentView(I)V

    .line 22
    invoke-virtual {p0}, Lco/uk/getmondo/feed/welcome/WelcomeToMonzoActivity;->l()Lco/uk/getmondo/common/h/b/b;

    move-result-object v0

    invoke-interface {v0, p0}, Lco/uk/getmondo/common/h/b/b;->a(Lco/uk/getmondo/feed/welcome/WelcomeToMonzoActivity;)V

    .line 24
    iget-object v0, p0, Lco/uk/getmondo/feed/welcome/WelcomeToMonzoActivity;->a:Lco/uk/getmondo/feed/welcome/b;

    if-nez v0, :cond_0

    const-string v1, "presenter"

    invoke-static {v1}, Lkotlin/d/b/l;->b(Ljava/lang/String;)V

    :cond_0
    check-cast p0, Lco/uk/getmondo/feed/welcome/b$a;

    invoke-virtual {v0, p0}, Lco/uk/getmondo/feed/welcome/b;->a(Lco/uk/getmondo/feed/welcome/b$a;)V

    .line 25
    return-void
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    .line 28
    iget-object v0, p0, Lco/uk/getmondo/feed/welcome/WelcomeToMonzoActivity;->a:Lco/uk/getmondo/feed/welcome/b;

    if-nez v0, :cond_0

    const-string v1, "presenter"

    invoke-static {v1}, Lkotlin/d/b/l;->b(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0}, Lco/uk/getmondo/feed/welcome/b;->b()V

    .line 30
    invoke-super {p0}, Lco/uk/getmondo/common/activities/b;->onDestroy()V

    .line 31
    return-void
.end method
