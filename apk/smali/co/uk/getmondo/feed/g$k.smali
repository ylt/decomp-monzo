.class final Lco/uk/getmondo/feed/g$k;
.super Ljava/lang/Object;
.source "HomeFeedPresenter.kt"

# interfaces
.implements Lio/reactivex/c/g;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lco/uk/getmondo/feed/g;->a(Lco/uk/getmondo/feed/g$a;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/g",
        "<",
        "Lco/uk/getmondo/d/b;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "",
        "accountBalance",
        "Lco/uk/getmondo/model/AccountBalance;",
        "kotlin.jvm.PlatformType",
        "accept"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field final synthetic a:Lco/uk/getmondo/feed/g;

.field final synthetic b:Lco/uk/getmondo/feed/g$a;


# direct methods
.method constructor <init>(Lco/uk/getmondo/feed/g;Lco/uk/getmondo/feed/g$a;)V
    .locals 0

    iput-object p1, p0, Lco/uk/getmondo/feed/g$k;->a:Lco/uk/getmondo/feed/g;

    iput-object p2, p0, Lco/uk/getmondo/feed/g$k;->b:Lco/uk/getmondo/feed/g$a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lco/uk/getmondo/d/b;)V
    .locals 5

    .prologue
    .line 108
    iget-object v0, p0, Lco/uk/getmondo/feed/g$k;->b:Lco/uk/getmondo/feed/g$a;

    invoke-virtual {p1}, Lco/uk/getmondo/d/b;->a()Lco/uk/getmondo/d/c;

    move-result-object v1

    const-string v2, "accountBalance.balance"

    invoke-static {v1, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Lco/uk/getmondo/feed/g$a;->a(Lco/uk/getmondo/d/c;)V

    .line 109
    iget-object v0, p0, Lco/uk/getmondo/feed/g$k;->b:Lco/uk/getmondo/feed/g$a;

    invoke-virtual {p1}, Lco/uk/getmondo/d/b;->b()Lco/uk/getmondo/d/c;

    move-result-object v1

    const-string v2, "accountBalance.spentToday"

    invoke-static {v1, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Lco/uk/getmondo/feed/g$a;->b(Lco/uk/getmondo/d/c;)V

    .line 110
    invoke-virtual {p1}, Lco/uk/getmondo/d/b;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 111
    new-instance v1, Ljava/util/ArrayList;

    invoke-virtual {p1}, Lco/uk/getmondo/d/b;->c()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    move-object v0, v1

    .line 112
    check-cast v0, Ljava/util/List;

    iget-object v2, p0, Lco/uk/getmondo/feed/g$k;->a:Lco/uk/getmondo/feed/g;

    invoke-static {v2}, Lco/uk/getmondo/feed/g;->h(Lco/uk/getmondo/feed/g;)Lco/uk/getmondo/a/k;

    move-result-object v2

    check-cast v2, Ljava/util/Comparator;

    invoke-static {v0, v2}, Lkotlin/a/m;->a(Ljava/util/List;Ljava/util/Comparator;)V

    .line 113
    check-cast v1, Ljava/lang/Iterable;

    .line 160
    new-instance v0, Ljava/util/ArrayList;

    const/16 v2, 0xa

    invoke-static {v1, v2}, Lkotlin/a/m;->a(Ljava/lang/Iterable;I)I

    move-result v2

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v0, Ljava/util/Collection;

    .line 161
    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 162
    check-cast v1, Lco/uk/getmondo/d/t;

    .line 113
    iget-object v3, p0, Lco/uk/getmondo/feed/g$k;->a:Lco/uk/getmondo/feed/g;

    invoke-static {v3}, Lco/uk/getmondo/feed/g;->i(Lco/uk/getmondo/feed/g;)Lco/uk/getmondo/common/i/b;

    move-result-object v3

    invoke-virtual {v1}, Lco/uk/getmondo/d/t;->a()Lco/uk/getmondo/d/c;

    move-result-object v1

    const-string v4, "it.spendToday"

    invoke-static {v1, v4}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Lco/uk/getmondo/common/i/b;->a(Lco/uk/getmondo/d/c;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 163
    :cond_0
    check-cast v0, Ljava/util/List;

    .line 114
    iget-object v1, p0, Lco/uk/getmondo/feed/g$k;->b:Lco/uk/getmondo/feed/g$a;

    invoke-interface {v1, v0}, Lco/uk/getmondo/feed/g$a;->a(Ljava/util/List;)V

    .line 117
    :goto_1
    return-void

    .line 116
    :cond_1
    iget-object v0, p0, Lco/uk/getmondo/feed/g$k;->b:Lco/uk/getmondo/feed/g$a;

    invoke-interface {v0}, Lco/uk/getmondo/feed/g$a;->j()V

    goto :goto_1
.end method

.method public bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 26
    check-cast p1, Lco/uk/getmondo/d/b;

    invoke-virtual {p0, p1}, Lco/uk/getmondo/feed/g$k;->a(Lco/uk/getmondo/d/b;)V

    return-void
.end method
