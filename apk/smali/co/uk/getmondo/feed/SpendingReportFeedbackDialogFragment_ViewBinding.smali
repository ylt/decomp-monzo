.class public Lco/uk/getmondo/feed/SpendingReportFeedbackDialogFragment_ViewBinding;
.super Ljava/lang/Object;
.source "SpendingReportFeedbackDialogFragment_ViewBinding.java"

# interfaces
.implements Lbutterknife/Unbinder;


# instance fields
.field private a:Lco/uk/getmondo/feed/SpendingReportFeedbackDialogFragment;

.field private b:Landroid/view/View;

.field private c:Landroid/view/View;

.field private d:Landroid/view/View;


# direct methods
.method public constructor <init>(Lco/uk/getmondo/feed/SpendingReportFeedbackDialogFragment;Landroid/view/View;)V
    .locals 6

    .prologue
    const v5, 0x7f1102cc

    const v4, 0x7f1102cb

    const v3, 0x7f1102ca

    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    iput-object p1, p0, Lco/uk/getmondo/feed/SpendingReportFeedbackDialogFragment_ViewBinding;->a:Lco/uk/getmondo/feed/SpendingReportFeedbackDialogFragment;

    .line 29
    const-string v0, "field \'positiveFeedbackTextView\' and method \'onCategoryClicked\'"

    invoke-static {p2, v3, v0}, Lbutterknife/internal/Utils;->findRequiredView(Landroid/view/View;ILjava/lang/String;)Landroid/view/View;

    move-result-object v1

    .line 30
    const-string v0, "field \'positiveFeedbackTextView\'"

    const-class v2, Landroid/widget/TextView;

    invoke-static {v1, v3, v0, v2}, Lbutterknife/internal/Utils;->castView(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p1, Lco/uk/getmondo/feed/SpendingReportFeedbackDialogFragment;->positiveFeedbackTextView:Landroid/widget/TextView;

    .line 31
    iput-object v1, p0, Lco/uk/getmondo/feed/SpendingReportFeedbackDialogFragment_ViewBinding;->b:Landroid/view/View;

    .line 32
    new-instance v0, Lco/uk/getmondo/feed/SpendingReportFeedbackDialogFragment_ViewBinding$1;

    invoke-direct {v0, p0, p1}, Lco/uk/getmondo/feed/SpendingReportFeedbackDialogFragment_ViewBinding$1;-><init>(Lco/uk/getmondo/feed/SpendingReportFeedbackDialogFragment_ViewBinding;Lco/uk/getmondo/feed/SpendingReportFeedbackDialogFragment;)V

    invoke-virtual {v1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 38
    const-string v0, "field \'neutralFeedbackTextView\' and method \'onCategoryClicked\'"

    invoke-static {p2, v4, v0}, Lbutterknife/internal/Utils;->findRequiredView(Landroid/view/View;ILjava/lang/String;)Landroid/view/View;

    move-result-object v1

    .line 39
    const-string v0, "field \'neutralFeedbackTextView\'"

    const-class v2, Landroid/widget/TextView;

    invoke-static {v1, v4, v0, v2}, Lbutterknife/internal/Utils;->castView(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p1, Lco/uk/getmondo/feed/SpendingReportFeedbackDialogFragment;->neutralFeedbackTextView:Landroid/widget/TextView;

    .line 40
    iput-object v1, p0, Lco/uk/getmondo/feed/SpendingReportFeedbackDialogFragment_ViewBinding;->c:Landroid/view/View;

    .line 41
    new-instance v0, Lco/uk/getmondo/feed/SpendingReportFeedbackDialogFragment_ViewBinding$2;

    invoke-direct {v0, p0, p1}, Lco/uk/getmondo/feed/SpendingReportFeedbackDialogFragment_ViewBinding$2;-><init>(Lco/uk/getmondo/feed/SpendingReportFeedbackDialogFragment_ViewBinding;Lco/uk/getmondo/feed/SpendingReportFeedbackDialogFragment;)V

    invoke-virtual {v1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 47
    const-string v0, "field \'negativeFeedbackTextView\' and method \'onCategoryClicked\'"

    invoke-static {p2, v5, v0}, Lbutterknife/internal/Utils;->findRequiredView(Landroid/view/View;ILjava/lang/String;)Landroid/view/View;

    move-result-object v1

    .line 48
    const-string v0, "field \'negativeFeedbackTextView\'"

    const-class v2, Landroid/widget/TextView;

    invoke-static {v1, v5, v0, v2}, Lbutterknife/internal/Utils;->castView(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p1, Lco/uk/getmondo/feed/SpendingReportFeedbackDialogFragment;->negativeFeedbackTextView:Landroid/widget/TextView;

    .line 49
    iput-object v1, p0, Lco/uk/getmondo/feed/SpendingReportFeedbackDialogFragment_ViewBinding;->d:Landroid/view/View;

    .line 50
    new-instance v0, Lco/uk/getmondo/feed/SpendingReportFeedbackDialogFragment_ViewBinding$3;

    invoke-direct {v0, p0, p1}, Lco/uk/getmondo/feed/SpendingReportFeedbackDialogFragment_ViewBinding$3;-><init>(Lco/uk/getmondo/feed/SpendingReportFeedbackDialogFragment_ViewBinding;Lco/uk/getmondo/feed/SpendingReportFeedbackDialogFragment;)V

    invoke-virtual {v1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 56
    return-void
.end method


# virtual methods
.method public unbind()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 61
    iget-object v0, p0, Lco/uk/getmondo/feed/SpendingReportFeedbackDialogFragment_ViewBinding;->a:Lco/uk/getmondo/feed/SpendingReportFeedbackDialogFragment;

    .line 62
    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Bindings already cleared."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 63
    :cond_0
    iput-object v1, p0, Lco/uk/getmondo/feed/SpendingReportFeedbackDialogFragment_ViewBinding;->a:Lco/uk/getmondo/feed/SpendingReportFeedbackDialogFragment;

    .line 65
    iput-object v1, v0, Lco/uk/getmondo/feed/SpendingReportFeedbackDialogFragment;->positiveFeedbackTextView:Landroid/widget/TextView;

    .line 66
    iput-object v1, v0, Lco/uk/getmondo/feed/SpendingReportFeedbackDialogFragment;->neutralFeedbackTextView:Landroid/widget/TextView;

    .line 67
    iput-object v1, v0, Lco/uk/getmondo/feed/SpendingReportFeedbackDialogFragment;->negativeFeedbackTextView:Landroid/widget/TextView;

    .line 69
    iget-object v0, p0, Lco/uk/getmondo/feed/SpendingReportFeedbackDialogFragment_ViewBinding;->b:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 70
    iput-object v1, p0, Lco/uk/getmondo/feed/SpendingReportFeedbackDialogFragment_ViewBinding;->b:Landroid/view/View;

    .line 71
    iget-object v0, p0, Lco/uk/getmondo/feed/SpendingReportFeedbackDialogFragment_ViewBinding;->c:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 72
    iput-object v1, p0, Lco/uk/getmondo/feed/SpendingReportFeedbackDialogFragment_ViewBinding;->c:Landroid/view/View;

    .line 73
    iget-object v0, p0, Lco/uk/getmondo/feed/SpendingReportFeedbackDialogFragment_ViewBinding;->d:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 74
    iput-object v1, p0, Lco/uk/getmondo/feed/SpendingReportFeedbackDialogFragment_ViewBinding;->d:Landroid/view/View;

    .line 75
    return-void
.end method
