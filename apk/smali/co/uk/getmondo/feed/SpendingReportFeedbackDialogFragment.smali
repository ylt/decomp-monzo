.class public Lco/uk/getmondo/feed/SpendingReportFeedbackDialogFragment;
.super Landroid/app/DialogFragment;
.source "SpendingReportFeedbackDialogFragment.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lco/uk/getmondo/feed/SpendingReportFeedbackDialogFragment$a;,
        Lco/uk/getmondo/feed/SpendingReportFeedbackDialogFragment$b;
    }
.end annotation


# instance fields
.field private a:Lbutterknife/Unbinder;

.field private b:Lco/uk/getmondo/feed/SpendingReportFeedbackDialogFragment$b;

.field negativeFeedbackTextView:Landroid/widget/TextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f1102cc
    .end annotation
.end field

.field neutralFeedbackTextView:Landroid/widget/TextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f1102cb
    .end annotation
.end field

.field positiveFeedbackTextView:Landroid/widget/TextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f1102ca
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    return-void
.end method

.method public static a()Lco/uk/getmondo/feed/SpendingReportFeedbackDialogFragment;
    .locals 1

    .prologue
    .line 27
    new-instance v0, Lco/uk/getmondo/feed/SpendingReportFeedbackDialogFragment;

    invoke-direct {v0}, Lco/uk/getmondo/feed/SpendingReportFeedbackDialogFragment;-><init>()V

    return-object v0
.end method


# virtual methods
.method public a(Lco/uk/getmondo/feed/SpendingReportFeedbackDialogFragment$b;)V
    .locals 0

    .prologue
    .line 31
    iput-object p1, p0, Lco/uk/getmondo/feed/SpendingReportFeedbackDialogFragment;->b:Lco/uk/getmondo/feed/SpendingReportFeedbackDialogFragment$b;

    .line 32
    return-void
.end method

.method onCategoryClicked(Landroid/widget/TextView;)V
    .locals 2
    .annotation build Lbutterknife/OnClick;
        value = {
            0x7f1102ca,
            0x7f1102cb,
            0x7f1102cc
        }
    .end annotation

    .prologue
    .line 56
    iget-object v0, p0, Lco/uk/getmondo/feed/SpendingReportFeedbackDialogFragment;->b:Lco/uk/getmondo/feed/SpendingReportFeedbackDialogFragment$b;

    if-eqz v0, :cond_0

    .line 57
    invoke-virtual {p1}, Landroid/widget/TextView;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 69
    :cond_0
    :goto_0
    invoke-virtual {p0}, Lco/uk/getmondo/feed/SpendingReportFeedbackDialogFragment;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    .line 70
    return-void

    .line 59
    :pswitch_0
    iget-object v0, p0, Lco/uk/getmondo/feed/SpendingReportFeedbackDialogFragment;->b:Lco/uk/getmondo/feed/SpendingReportFeedbackDialogFragment$b;

    sget-object v1, Lco/uk/getmondo/feed/SpendingReportFeedbackDialogFragment$a;->a:Lco/uk/getmondo/feed/SpendingReportFeedbackDialogFragment$a;

    invoke-interface {v0, v1}, Lco/uk/getmondo/feed/SpendingReportFeedbackDialogFragment$b;->a(Lco/uk/getmondo/feed/SpendingReportFeedbackDialogFragment$a;)V

    goto :goto_0

    .line 62
    :pswitch_1
    iget-object v0, p0, Lco/uk/getmondo/feed/SpendingReportFeedbackDialogFragment;->b:Lco/uk/getmondo/feed/SpendingReportFeedbackDialogFragment$b;

    sget-object v1, Lco/uk/getmondo/feed/SpendingReportFeedbackDialogFragment$a;->c:Lco/uk/getmondo/feed/SpendingReportFeedbackDialogFragment$a;

    invoke-interface {v0, v1}, Lco/uk/getmondo/feed/SpendingReportFeedbackDialogFragment$b;->a(Lco/uk/getmondo/feed/SpendingReportFeedbackDialogFragment$a;)V

    goto :goto_0

    .line 65
    :pswitch_2
    iget-object v0, p0, Lco/uk/getmondo/feed/SpendingReportFeedbackDialogFragment;->b:Lco/uk/getmondo/feed/SpendingReportFeedbackDialogFragment$b;

    sget-object v1, Lco/uk/getmondo/feed/SpendingReportFeedbackDialogFragment$a;->b:Lco/uk/getmondo/feed/SpendingReportFeedbackDialogFragment$a;

    invoke-interface {v0, v1}, Lco/uk/getmondo/feed/SpendingReportFeedbackDialogFragment$b;->a(Lco/uk/getmondo/feed/SpendingReportFeedbackDialogFragment$a;)V

    goto :goto_0

    .line 57
    nop

    :pswitch_data_0
    .packed-switch 0x7f1102ca
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 4

    .prologue
    .line 36
    invoke-virtual {p0}, Lco/uk/getmondo/feed/SpendingReportFeedbackDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f050097

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 37
    invoke-static {p0, v0}, Lbutterknife/ButterKnife;->bind(Ljava/lang/Object;Landroid/view/View;)Lbutterknife/Unbinder;

    move-result-object v1

    iput-object v1, p0, Lco/uk/getmondo/feed/SpendingReportFeedbackDialogFragment;->a:Lbutterknife/Unbinder;

    .line 39
    iget-object v1, p0, Lco/uk/getmondo/feed/SpendingReportFeedbackDialogFragment;->positiveFeedbackTextView:Landroid/widget/TextView;

    invoke-static {}, Lco/uk/getmondo/common/k/e;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 40
    iget-object v1, p0, Lco/uk/getmondo/feed/SpendingReportFeedbackDialogFragment;->neutralFeedbackTextView:Landroid/widget/TextView;

    invoke-static {}, Lco/uk/getmondo/common/k/e;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 41
    iget-object v1, p0, Lco/uk/getmondo/feed/SpendingReportFeedbackDialogFragment;->negativeFeedbackTextView:Landroid/widget/TextView;

    invoke-static {}, Lco/uk/getmondo/common/k/e;->f()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 43
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lco/uk/getmondo/feed/SpendingReportFeedbackDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 44
    invoke-virtual {v1, v0}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 45
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 43
    return-object v0
.end method

.method public onDestroyView()V
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lco/uk/getmondo/feed/SpendingReportFeedbackDialogFragment;->a:Lbutterknife/Unbinder;

    invoke-interface {v0}, Lbutterknife/Unbinder;->unbind()V

    .line 51
    invoke-super {p0}, Landroid/app/DialogFragment;->onDestroyView()V

    .line 52
    return-void
.end method
