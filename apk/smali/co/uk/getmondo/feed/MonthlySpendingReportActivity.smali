.class public Lco/uk/getmondo/feed/MonthlySpendingReportActivity;
.super Lco/uk/getmondo/common/activities/b;
.source "MonthlySpendingReportActivity.java"


# instance fields
.field a:Lco/uk/getmondo/common/a;

.field amountView:Lco/uk/getmondo/common/ui/AmountView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f11022c
    .end annotation
.end field

.field private b:Lorg/threeten/bp/YearMonth;

.field goToSpendingButton:Landroid/widget/Button;
    .annotation build Lbutterknife/BindView;
        value = 0x7f11022f
    .end annotation
.end field

.field monthTextView:Landroid/widget/TextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f11022d
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Lco/uk/getmondo/common/activities/b;-><init>()V

    return-void
.end method

.method public static a(Landroid/content/Context;Lco/uk/getmondo/d/v;)Landroid/content/Intent;
    .locals 3

    .prologue
    .line 44
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lco/uk/getmondo/feed/MonthlySpendingReportActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 45
    const-string v1, "KEY_MONTHLY_SPENDING_AMOUNT"

    invoke-virtual {p1}, Lco/uk/getmondo/d/v;->a()Lco/uk/getmondo/d/c;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 46
    const-string v1, "KEY_MONTHLY_SPENDING_MONTH"

    invoke-virtual {p1}, Lco/uk/getmondo/d/v;->b()Lorg/threeten/bp/YearMonth;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 47
    return-object v0
.end method

.method static synthetic a(Lco/uk/getmondo/feed/MonthlySpendingReportActivity;Lco/uk/getmondo/feed/SpendingReportFeedbackDialogFragment$a;)V
    .locals 2

    .prologue
    .line 79
    iget-object v0, p0, Lco/uk/getmondo/feed/MonthlySpendingReportActivity;->b:Lorg/threeten/bp/YearMonth;

    invoke-static {p1, v0}, Lco/uk/getmondo/api/model/tracking/Impression;->a(Lco/uk/getmondo/feed/SpendingReportFeedbackDialogFragment$a;Lorg/threeten/bp/YearMonth;)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    .line 80
    iget-object v1, p0, Lco/uk/getmondo/feed/MonthlySpendingReportActivity;->a:Lco/uk/getmondo/common/a;

    invoke-virtual {v1, v0}, Lco/uk/getmondo/common/a;->a(Lco/uk/getmondo/api/model/tracking/Impression;)V

    .line 81
    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 52
    invoke-super {p0, p1}, Lco/uk/getmondo/common/activities/b;->onCreate(Landroid/os/Bundle;)V

    .line 54
    const v0, 0x7f050065

    invoke-virtual {p0, v0}, Lco/uk/getmondo/feed/MonthlySpendingReportActivity;->setContentView(I)V

    .line 55
    invoke-static {p0}, Lbutterknife/ButterKnife;->bind(Landroid/app/Activity;)Lbutterknife/Unbinder;

    .line 56
    invoke-virtual {p0}, Lco/uk/getmondo/feed/MonthlySpendingReportActivity;->l()Lco/uk/getmondo/common/h/b/b;

    move-result-object v0

    invoke-interface {v0, p0}, Lco/uk/getmondo/common/h/b/b;->a(Lco/uk/getmondo/feed/MonthlySpendingReportActivity;)V

    .line 58
    invoke-virtual {p0}, Lco/uk/getmondo/feed/MonthlySpendingReportActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "KEY_MONTHLY_SPENDING_AMOUNT"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/d/c;

    .line 59
    invoke-virtual {p0}, Lco/uk/getmondo/feed/MonthlySpendingReportActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "KEY_MONTHLY_SPENDING_MONTH"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v1

    check-cast v1, Lorg/threeten/bp/YearMonth;

    iput-object v1, p0, Lco/uk/getmondo/feed/MonthlySpendingReportActivity;->b:Lorg/threeten/bp/YearMonth;

    .line 60
    iget-object v1, p0, Lco/uk/getmondo/feed/MonthlySpendingReportActivity;->a:Lco/uk/getmondo/common/a;

    iget-object v2, p0, Lco/uk/getmondo/feed/MonthlySpendingReportActivity;->b:Lorg/threeten/bp/YearMonth;

    invoke-static {v2}, Lco/uk/getmondo/api/model/tracking/Impression;->a(Lorg/threeten/bp/YearMonth;)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v2

    invoke-virtual {v1, v2}, Lco/uk/getmondo/common/a;->a(Lco/uk/getmondo/api/model/tracking/Impression;)V

    .line 62
    iget-object v1, p0, Lco/uk/getmondo/feed/MonthlySpendingReportActivity;->amountView:Lco/uk/getmondo/common/ui/AmountView;

    invoke-virtual {v1, v0}, Lco/uk/getmondo/common/ui/AmountView;->setAmount(Lco/uk/getmondo/d/c;)V

    .line 63
    iget-object v0, p0, Lco/uk/getmondo/feed/MonthlySpendingReportActivity;->b:Lorg/threeten/bp/YearMonth;

    invoke-virtual {v0}, Lorg/threeten/bp/YearMonth;->c()Lorg/threeten/bp/Month;

    move-result-object v0

    sget-object v1, Lorg/threeten/bp/format/TextStyle;->a:Lorg/threeten/bp/format/TextStyle;

    sget-object v2, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {v0, v1, v2}, Lorg/threeten/bp/Month;->a(Lorg/threeten/bp/format/TextStyle;Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    .line 64
    iget-object v1, p0, Lco/uk/getmondo/feed/MonthlySpendingReportActivity;->monthTextView:Landroid/widget/TextView;

    const v2, 0x7f0a0293

    new-array v3, v5, [Ljava/lang/Object;

    aput-object v0, v3, v4

    invoke-virtual {p0, v2, v3}, Lco/uk/getmondo/feed/MonthlySpendingReportActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 65
    iget-object v1, p0, Lco/uk/getmondo/feed/MonthlySpendingReportActivity;->goToSpendingButton:Landroid/widget/Button;

    const v2, 0x7f0a0294

    new-array v3, v5, [Ljava/lang/Object;

    aput-object v0, v3, v4

    invoke-virtual {p0, v2, v3}, Lco/uk/getmondo/feed/MonthlySpendingReportActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 66
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 2

    .prologue
    .line 70
    invoke-virtual {p0}, Lco/uk/getmondo/feed/MonthlySpendingReportActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const v1, 0x7f130008

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 71
    const/4 v0, 0x1

    return v0
.end method

.method public onGoToSpendingButtonClicked()V
    .locals 1
    .annotation build Lbutterknife/OnClick;
        value = {
            0x7f11022f
        }
    .end annotation

    .prologue
    .line 90
    iget-object v0, p0, Lco/uk/getmondo/feed/MonthlySpendingReportActivity;->b:Lorg/threeten/bp/YearMonth;

    invoke-static {p0, v0}, Lco/uk/getmondo/main/HomeActivity;->a(Landroid/content/Context;Lorg/threeten/bp/YearMonth;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lco/uk/getmondo/feed/MonthlySpendingReportActivity;->startActivity(Landroid/content/Intent;)V

    .line 91
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 3

    .prologue
    .line 76
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x7f1104d7

    if-ne v0, v1, :cond_0

    .line 77
    invoke-static {}, Lco/uk/getmondo/feed/SpendingReportFeedbackDialogFragment;->a()Lco/uk/getmondo/feed/SpendingReportFeedbackDialogFragment;

    move-result-object v0

    .line 78
    invoke-static {p0}, Lco/uk/getmondo/feed/i;->a(Lco/uk/getmondo/feed/MonthlySpendingReportActivity;)Lco/uk/getmondo/feed/SpendingReportFeedbackDialogFragment$b;

    move-result-object v1

    invoke-virtual {v0, v1}, Lco/uk/getmondo/feed/SpendingReportFeedbackDialogFragment;->a(Lco/uk/getmondo/feed/SpendingReportFeedbackDialogFragment$b;)V

    .line 82
    invoke-virtual {p0}, Lco/uk/getmondo/feed/MonthlySpendingReportActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const-string v2, "spending_report"

    invoke-virtual {v0, v1, v2}, Lco/uk/getmondo/feed/SpendingReportFeedbackDialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    .line 83
    const/4 v0, 0x1

    .line 85
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
