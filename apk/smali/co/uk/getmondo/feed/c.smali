.class public Lco/uk/getmondo/feed/c;
.super Ljava/lang/Object;
.source "FeedItemNavigator.java"


# instance fields
.field private final a:Landroid/content/Context;


# direct methods
.method constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    iput-object p1, p0, Lco/uk/getmondo/feed/c;->a:Landroid/content/Context;

    .line 34
    return-void
.end method


# virtual methods
.method public a(Lco/uk/getmondo/d/m;)V
    .locals 7

    .prologue
    .line 38
    sget-object v0, Lco/uk/getmondo/feed/c$1;->a:[I

    invoke-virtual {p1}, Lco/uk/getmondo/d/m;->j()Lco/uk/getmondo/feed/a/a/a;

    move-result-object v1

    invoke-virtual {v1}, Lco/uk/getmondo/feed/a/a/a;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 93
    :cond_0
    :goto_0
    return-void

    .line 41
    :pswitch_0
    invoke-virtual {p1}, Lco/uk/getmondo/d/m;->e()Lcom/c/b/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/c/b/b;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/d/aj;

    invoke-virtual {v0}, Lco/uk/getmondo/d/aj;->t()Z

    move-result v0

    if-nez v0, :cond_0

    .line 42
    iget-object v1, p0, Lco/uk/getmondo/feed/c;->a:Landroid/content/Context;

    iget-object v2, p0, Lco/uk/getmondo/feed/c;->a:Landroid/content/Context;

    invoke-virtual {p1}, Lco/uk/getmondo/d/m;->e()Lcom/c/b/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/c/b/b;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/d/aj;

    invoke-virtual {v0}, Lco/uk/getmondo/d/aj;->w()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lco/uk/getmondo/transaction/details/TransactionDetailsActivity;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 46
    :pswitch_1
    iget-object v0, p0, Lco/uk/getmondo/feed/c;->a:Landroid/content/Context;

    iget-object v1, p0, Lco/uk/getmondo/feed/c;->a:Landroid/content/Context;

    sget-object v2, Lco/uk/getmondo/signup/identity_verification/a/j;->a:Lco/uk/getmondo/signup/identity_verification/a/j;

    sget-object v3, Lco/uk/getmondo/api/model/tracking/Impression$KycFrom;->FEED:Lco/uk/getmondo/api/model/tracking/Impression$KycFrom;

    sget-object v4, Lco/uk/getmondo/api/model/signup/SignupSource;->LEGACY_PREPAID:Lco/uk/getmondo/api/model/signup/SignupSource;

    invoke-static {v1, v2, v3, v4}, Lco/uk/getmondo/signup/identity_verification/IdentityVerificationActivity;->a(Landroid/content/Context;Lco/uk/getmondo/signup/identity_verification/a/j;Lco/uk/getmondo/api/model/tracking/Impression$KycFrom;Lco/uk/getmondo/api/model/signup/SignupSource;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 51
    :pswitch_2
    iget-object v0, p0, Lco/uk/getmondo/feed/c;->a:Landroid/content/Context;

    invoke-static {v0}, Lco/uk/getmondo/signup/identity_verification/IdentityApprovedActivity;->a(Landroid/content/Context;)V

    goto :goto_0

    .line 54
    :pswitch_3
    iget-object v0, p0, Lco/uk/getmondo/feed/c;->a:Landroid/content/Context;

    invoke-virtual {p1}, Lco/uk/getmondo/d/m;->f()Lco/uk/getmondo/d/o;

    move-result-object v1

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Lco/uk/getmondo/golden_ticket/GoldenTicketActivity;->a(Landroid/content/Context;Lco/uk/getmondo/d/o;Z)V

    goto :goto_0

    .line 57
    :pswitch_4
    iget-object v0, p0, Lco/uk/getmondo/feed/c;->a:Landroid/content/Context;

    invoke-virtual {p1}, Lco/uk/getmondo/d/m;->f()Lco/uk/getmondo/d/o;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lco/uk/getmondo/golden_ticket/GoldenTicketActivity;->a(Landroid/content/Context;Lco/uk/getmondo/d/o;Z)V

    goto :goto_0

    .line 60
    :pswitch_5
    iget-object v0, p0, Lco/uk/getmondo/feed/c;->a:Landroid/content/Context;

    iget-object v1, p0, Lco/uk/getmondo/feed/c;->a:Landroid/content/Context;

    invoke-virtual {p1}, Lco/uk/getmondo/d/m;->g()Lco/uk/getmondo/d/v;

    move-result-object v2

    invoke-static {v1, v2}, Lco/uk/getmondo/feed/MonthlySpendingReportActivity;->a(Landroid/content/Context;Lco/uk/getmondo/d/v;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 63
    :pswitch_6
    iget-object v0, p0, Lco/uk/getmondo/feed/c;->a:Landroid/content/Context;

    iget-object v1, p0, Lco/uk/getmondo/feed/c;->a:Landroid/content/Context;

    invoke-static {v1}, Lco/uk/getmondo/main/EddLimitsActivity;->a(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 66
    :pswitch_7
    invoke-virtual {p1}, Lco/uk/getmondo/d/m;->h()Lco/uk/getmondo/d/r;

    move-result-object v0

    .line 67
    invoke-virtual {v0}, Lco/uk/getmondo/d/r;->b()Ljava/lang/String;

    move-result-object v5

    .line 68
    invoke-static {v5}, Lco/uk/getmondo/common/k/p;->d(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 69
    iget-object v1, p0, Lco/uk/getmondo/feed/c;->a:Landroid/content/Context;

    invoke-virtual {v0}, Lco/uk/getmondo/d/r;->a()Lco/uk/getmondo/signup/identity_verification/y;

    move-result-object v0

    invoke-virtual {v0}, Lco/uk/getmondo/signup/identity_verification/y;->b()I

    move-result v0

    invoke-virtual {v1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 71
    :cond_1
    iget-object v6, p0, Lco/uk/getmondo/feed/c;->a:Landroid/content/Context;

    iget-object v0, p0, Lco/uk/getmondo/feed/c;->a:Landroid/content/Context;

    sget-object v1, Lco/uk/getmondo/signup/identity_verification/a/j;->a:Lco/uk/getmondo/signup/identity_verification/a/j;

    sget-object v2, Lco/uk/getmondo/api/model/tracking/Impression$KycFrom;->FEED:Lco/uk/getmondo/api/model/tracking/Impression$KycFrom;

    sget-object v3, Lco/uk/getmondo/api/model/signup/SignupSource;->LEGACY_PREPAID:Lco/uk/getmondo/api/model/signup/SignupSource;

    sget-object v4, Lco/uk/getmondo/signup/j;->b:Lco/uk/getmondo/signup/j;

    invoke-static/range {v0 .. v5}, Lco/uk/getmondo/signup/identity_verification/IdentityVerificationActivity;->a(Landroid/content/Context;Lco/uk/getmondo/signup/identity_verification/a/j;Lco/uk/getmondo/api/model/tracking/Impression$KycFrom;Lco/uk/getmondo/api/model/signup/SignupSource;Lco/uk/getmondo/signup/j;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v6, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 76
    :pswitch_8
    iget-object v0, p0, Lco/uk/getmondo/feed/c;->a:Landroid/content/Context;

    iget-object v1, p0, Lco/uk/getmondo/feed/c;->a:Landroid/content/Context;

    sget-object v2, Lco/uk/getmondo/signup/identity_verification/sdd/j;->a:Lco/uk/getmondo/signup/identity_verification/sdd/j;

    invoke-static {v1, v2}, Lco/uk/getmondo/signup/identity_verification/sdd/IdentityVerificationSddActivity;->a(Landroid/content/Context;Lco/uk/getmondo/signup/identity_verification/sdd/j;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 79
    :pswitch_9
    iget-object v0, p0, Lco/uk/getmondo/feed/c;->a:Landroid/content/Context;

    iget-object v1, p0, Lco/uk/getmondo/feed/c;->a:Landroid/content/Context;

    sget-object v2, Lco/uk/getmondo/signup/identity_verification/a/j;->b:Lco/uk/getmondo/signup/identity_verification/a/j;

    sget-object v3, Lco/uk/getmondo/api/model/tracking/Impression$KycFrom;->FEED:Lco/uk/getmondo/api/model/tracking/Impression$KycFrom;

    sget-object v4, Lco/uk/getmondo/api/model/signup/SignupSource;->SDD_MIGRATION:Lco/uk/getmondo/api/model/signup/SignupSource;

    invoke-static {v1, v2, v3, v4}, Lco/uk/getmondo/signup/identity_verification/IdentityVerificationActivity;->a(Landroid/content/Context;Lco/uk/getmondo/signup/identity_verification/a/j;Lco/uk/getmondo/api/model/tracking/Impression$KycFrom;Lco/uk/getmondo/api/model/signup/SignupSource;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 83
    :pswitch_a
    invoke-virtual {p1}, Lco/uk/getmondo/d/m;->d()Landroid/net/Uri;

    move-result-object v0

    .line 84
    if-eqz v0, :cond_0

    const-string v1, "url"

    invoke-virtual {v0, v1}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 85
    iget-object v1, p0, Lco/uk/getmondo/feed/c;->a:Landroid/content/Context;

    iget-object v2, p0, Lco/uk/getmondo/feed/c;->a:Landroid/content/Context;

    const-string v3, "url"

    invoke-virtual {v0, v3}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "style"

    .line 86
    invoke-virtual {v0, v4}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 85
    invoke-static {v2, v3, v0}, Lco/uk/getmondo/feed/InternalWebActivity;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 90
    :pswitch_b
    iget-object v0, p0, Lco/uk/getmondo/feed/c;->a:Landroid/content/Context;

    iget-object v1, p0, Lco/uk/getmondo/feed/c;->a:Landroid/content/Context;

    invoke-static {v1}, Lco/uk/getmondo/feed/welcome/WelcomeToMonzoActivity;->a(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 38
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
    .end packed-switch
.end method
