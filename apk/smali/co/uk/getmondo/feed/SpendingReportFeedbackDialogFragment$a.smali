.class public final enum Lco/uk/getmondo/feed/SpendingReportFeedbackDialogFragment$a;
.super Ljava/lang/Enum;
.source "SpendingReportFeedbackDialogFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lco/uk/getmondo/feed/SpendingReportFeedbackDialogFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lco/uk/getmondo/feed/SpendingReportFeedbackDialogFragment$a;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lco/uk/getmondo/feed/SpendingReportFeedbackDialogFragment$a;

.field public static final enum b:Lco/uk/getmondo/feed/SpendingReportFeedbackDialogFragment$a;

.field public static final enum c:Lco/uk/getmondo/feed/SpendingReportFeedbackDialogFragment$a;

.field private static final synthetic e:[Lco/uk/getmondo/feed/SpendingReportFeedbackDialogFragment$a;


# instance fields
.field private final d:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 77
    new-instance v0, Lco/uk/getmondo/feed/SpendingReportFeedbackDialogFragment$a;

    const-string v1, "POSITIVE"

    const-string v2, "positive"

    invoke-direct {v0, v1, v3, v2}, Lco/uk/getmondo/feed/SpendingReportFeedbackDialogFragment$a;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lco/uk/getmondo/feed/SpendingReportFeedbackDialogFragment$a;->a:Lco/uk/getmondo/feed/SpendingReportFeedbackDialogFragment$a;

    .line 78
    new-instance v0, Lco/uk/getmondo/feed/SpendingReportFeedbackDialogFragment$a;

    const-string v1, "NEGATIVE"

    const-string v2, "negative"

    invoke-direct {v0, v1, v4, v2}, Lco/uk/getmondo/feed/SpendingReportFeedbackDialogFragment$a;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lco/uk/getmondo/feed/SpendingReportFeedbackDialogFragment$a;->b:Lco/uk/getmondo/feed/SpendingReportFeedbackDialogFragment$a;

    .line 79
    new-instance v0, Lco/uk/getmondo/feed/SpendingReportFeedbackDialogFragment$a;

    const-string v1, "NEUTRAL"

    const-string v2, "neutral"

    invoke-direct {v0, v1, v5, v2}, Lco/uk/getmondo/feed/SpendingReportFeedbackDialogFragment$a;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lco/uk/getmondo/feed/SpendingReportFeedbackDialogFragment$a;->c:Lco/uk/getmondo/feed/SpendingReportFeedbackDialogFragment$a;

    .line 76
    const/4 v0, 0x3

    new-array v0, v0, [Lco/uk/getmondo/feed/SpendingReportFeedbackDialogFragment$a;

    sget-object v1, Lco/uk/getmondo/feed/SpendingReportFeedbackDialogFragment$a;->a:Lco/uk/getmondo/feed/SpendingReportFeedbackDialogFragment$a;

    aput-object v1, v0, v3

    sget-object v1, Lco/uk/getmondo/feed/SpendingReportFeedbackDialogFragment$a;->b:Lco/uk/getmondo/feed/SpendingReportFeedbackDialogFragment$a;

    aput-object v1, v0, v4

    sget-object v1, Lco/uk/getmondo/feed/SpendingReportFeedbackDialogFragment$a;->c:Lco/uk/getmondo/feed/SpendingReportFeedbackDialogFragment$a;

    aput-object v1, v0, v5

    sput-object v0, Lco/uk/getmondo/feed/SpendingReportFeedbackDialogFragment$a;->e:[Lco/uk/getmondo/feed/SpendingReportFeedbackDialogFragment$a;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 83
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 84
    iput-object p3, p0, Lco/uk/getmondo/feed/SpendingReportFeedbackDialogFragment$a;->d:Ljava/lang/String;

    .line 85
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lco/uk/getmondo/feed/SpendingReportFeedbackDialogFragment$a;
    .locals 1

    .prologue
    .line 76
    const-class v0, Lco/uk/getmondo/feed/SpendingReportFeedbackDialogFragment$a;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/feed/SpendingReportFeedbackDialogFragment$a;

    return-object v0
.end method

.method public static values()[Lco/uk/getmondo/feed/SpendingReportFeedbackDialogFragment$a;
    .locals 1

    .prologue
    .line 76
    sget-object v0, Lco/uk/getmondo/feed/SpendingReportFeedbackDialogFragment$a;->e:[Lco/uk/getmondo/feed/SpendingReportFeedbackDialogFragment$a;

    invoke-virtual {v0}, [Lco/uk/getmondo/feed/SpendingReportFeedbackDialogFragment$a;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lco/uk/getmondo/feed/SpendingReportFeedbackDialogFragment$a;

    return-object v0
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lco/uk/getmondo/feed/SpendingReportFeedbackDialogFragment$a;->d:Ljava/lang/String;

    return-object v0
.end method
