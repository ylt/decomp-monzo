.class public final Lco/uk/getmondo/feed/InternalWebActivity$b;
.super Landroid/webkit/WebViewClient;
.source "InternalWebActivity.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lco/uk/getmondo/feed/InternalWebActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x11
    name = "b"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u00006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0002\u0008\u0004\u0008\u0086\u0004\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0018\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u00082\u0006\u0010\t\u001a\u00020\nH\u0016J\"\u0010\u000b\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u00082\u0006\u0010\t\u001a\u00020\n2\u0008\u0010\u000c\u001a\u0004\u0018\u00010\rH\u0016J(\u0010\u000e\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u00082\u0006\u0010\u000f\u001a\u00020\u00102\u0006\u0010\u0011\u001a\u00020\n2\u0006\u0010\u0012\u001a\u00020\nH\u0016J\u0018\u0010\u0013\u001a\u00020\u00042\u0006\u0010\u0007\u001a\u00020\u00082\u0006\u0010\t\u001a\u00020\nH\u0016R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0014"
    }
    d2 = {
        "Lco/uk/getmondo/feed/InternalWebActivity$CustomWebViewClient;",
        "Landroid/webkit/WebViewClient;",
        "(Lco/uk/getmondo/feed/InternalWebActivity;)V",
        "errorReceived",
        "",
        "onPageFinished",
        "",
        "view",
        "Landroid/webkit/WebView;",
        "url",
        "",
        "onPageStarted",
        "favicon",
        "Landroid/graphics/Bitmap;",
        "onReceivedError",
        "errorCode",
        "",
        "description",
        "failingUrl",
        "shouldOverrideUrlLoading",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field final synthetic a:Lco/uk/getmondo/feed/InternalWebActivity;

.field private b:Z


# direct methods
.method public constructor <init>(Lco/uk/getmondo/feed/InternalWebActivity;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 102
    iput-object p1, p0, Lco/uk/getmondo/feed/InternalWebActivity$b;->a:Lco/uk/getmondo/feed/InternalWebActivity;

    invoke-direct {p0}, Landroid/webkit/WebViewClient;-><init>()V

    return-void
.end method


# virtual methods
.method public onPageFinished(Landroid/webkit/WebView;Ljava/lang/String;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/16 v2, 0x8

    const-string v0, "view"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "url"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 115
    iget-object v0, p0, Lco/uk/getmondo/feed/InternalWebActivity$b;->a:Lco/uk/getmondo/feed/InternalWebActivity;

    sget v1, Lco/uk/getmondo/c$a;->webView:I

    invoke-virtual {v0, v1}, Lco/uk/getmondo/feed/InternalWebActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/webkit/WebView;

    iget-boolean v1, p0, Lco/uk/getmondo/feed/InternalWebActivity$b;->b:Z

    if-eqz v1, :cond_0

    move v1, v2

    :goto_0
    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->setVisibility(I)V

    .line 116
    iget-object v0, p0, Lco/uk/getmondo/feed/InternalWebActivity$b;->a:Lco/uk/getmondo/feed/InternalWebActivity;

    sget v1, Lco/uk/getmondo/c$a;->errorLayout:I

    invoke-virtual {v0, v1}, Lco/uk/getmondo/feed/InternalWebActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iget-boolean v1, p0, Lco/uk/getmondo/feed/InternalWebActivity$b;->b:Z

    if-eqz v1, :cond_1

    :goto_1
    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 117
    iget-object v0, p0, Lco/uk/getmondo/feed/InternalWebActivity$b;->a:Lco/uk/getmondo/feed/InternalWebActivity;

    sget v1, Lco/uk/getmondo/c$a;->progress:I

    invoke-virtual {v0, v1}, Lco/uk/getmondo/feed/InternalWebActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    invoke-virtual {v0, v2}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 118
    return-void

    :cond_0
    move v1, v3

    .line 115
    goto :goto_0

    :cond_1
    move v3, v2

    .line 116
    goto :goto_1
.end method

.method public onPageStarted(Landroid/webkit/WebView;Ljava/lang/String;Landroid/graphics/Bitmap;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/4 v2, 0x0

    const-string v0, "view"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "url"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 108
    iput-boolean v2, p0, Lco/uk/getmondo/feed/InternalWebActivity$b;->b:Z

    .line 109
    iget-object v0, p0, Lco/uk/getmondo/feed/InternalWebActivity$b;->a:Lco/uk/getmondo/feed/InternalWebActivity;

    sget v1, Lco/uk/getmondo/c$a;->progress:I

    invoke-virtual {v0, v1}, Lco/uk/getmondo/feed/InternalWebActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    invoke-virtual {v0, v2}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 110
    iget-object v0, p0, Lco/uk/getmondo/feed/InternalWebActivity$b;->a:Lco/uk/getmondo/feed/InternalWebActivity;

    sget v1, Lco/uk/getmondo/c$a;->webView:I

    invoke-virtual {v0, v1}, Lco/uk/getmondo/feed/InternalWebActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/webkit/WebView;

    invoke-virtual {v0, v3}, Landroid/webkit/WebView;->setVisibility(I)V

    .line 111
    iget-object v0, p0, Lco/uk/getmondo/feed/InternalWebActivity$b;->a:Lco/uk/getmondo/feed/InternalWebActivity;

    sget v1, Lco/uk/getmondo/c$a;->errorLayout:I

    invoke-virtual {v0, v1}, Lco/uk/getmondo/feed/InternalWebActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 112
    return-void
.end method

.method public onReceivedError(Landroid/webkit/WebView;ILjava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    const-string v0, "view"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "description"

    invoke-static {p3, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "failingUrl"

    invoke-static {p4, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 121
    const/4 v0, 0x1

    iput-boolean v0, p0, Lco/uk/getmondo/feed/InternalWebActivity$b;->b:Z

    .line 122
    return-void
.end method

.method public shouldOverrideUrlLoading(Landroid/webkit/WebView;Ljava/lang/String;)Z
    .locals 3

    .prologue
    const-string v0, "view"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "url"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 105
    iget-object v0, p0, Lco/uk/getmondo/feed/InternalWebActivity$b;->a:Lco/uk/getmondo/feed/InternalWebActivity;

    invoke-static {p2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    const-string v2, "Uri.parse(url)"

    invoke-static {v1, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v0, v1}, Lco/uk/getmondo/feed/InternalWebActivity;->a(Lco/uk/getmondo/feed/InternalWebActivity;Landroid/net/Uri;)Z

    move-result v0

    return v0
.end method
