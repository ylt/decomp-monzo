.class final Lco/uk/getmondo/feed/a/a$b;
.super Lkotlin/d/b/m;
.source "FeedItemResourceProvider.kt"

# interfaces
.implements Lkotlin/d/a/a;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lco/uk/getmondo/feed/a/a;-><init>(Landroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/d/b/m;",
        "Lkotlin/d/a/a",
        "<",
        "Ljava/util/Map",
        "<",
        "Lco/uk/getmondo/feed/a/a/a;",
        "+",
        "Ljava/lang/String;",
        ">;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000\u0012\n\u0000\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\u0008\u0002\u0010\u0000\u001a\u0016\u0012\u0004\u0012\u00020\u0002\u0012\u000c\u0012\n \u0004*\u0004\u0018\u00010\u00030\u00030\u0001H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "",
        "Lco/uk/getmondo/feed/data/model/FeedItemType;",
        "",
        "kotlin.jvm.PlatformType",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field final synthetic a:Lco/uk/getmondo/feed/a/a;


# direct methods
.method constructor <init>(Lco/uk/getmondo/feed/a/a;)V
    .locals 1

    iput-object p1, p0, Lco/uk/getmondo/feed/a/a$b;->a:Lco/uk/getmondo/feed/a/a;

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lkotlin/d/b/m;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final b()Ljava/util/Map;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Lco/uk/getmondo/feed/a/a/a;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 19
    const/16 v0, 0xb

    new-array v0, v0, [Lkotlin/h;

    .line 20
    const/4 v1, 0x0

    sget-object v2, Lco/uk/getmondo/feed/a/a/a;->b:Lco/uk/getmondo/feed/a/a/a;

    iget-object v3, p0, Lco/uk/getmondo/feed/a/a$b;->a:Lco/uk/getmondo/feed/a/a;

    invoke-static {v3}, Lco/uk/getmondo/feed/a/a;->a(Lco/uk/getmondo/feed/a/a;)Landroid/content/Context;

    move-result-object v3

    const v4, 0x7f0a0213

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lkotlin/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/h;

    move-result-object v2

    aput-object v2, v0, v1

    .line 21
    const/4 v1, 0x1

    sget-object v2, Lco/uk/getmondo/feed/a/a/a;->c:Lco/uk/getmondo/feed/a/a/a;

    iget-object v3, p0, Lco/uk/getmondo/feed/a/a$b;->a:Lco/uk/getmondo/feed/a/a;

    invoke-static {v3}, Lco/uk/getmondo/feed/a/a;->a(Lco/uk/getmondo/feed/a/a;)Landroid/content/Context;

    move-result-object v3

    const v4, 0x7f0a020e

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lkotlin/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/h;

    move-result-object v2

    aput-object v2, v0, v1

    .line 22
    const/4 v1, 0x2

    sget-object v2, Lco/uk/getmondo/feed/a/a/a;->h:Lco/uk/getmondo/feed/a/a/a;

    iget-object v3, p0, Lco/uk/getmondo/feed/a/a$b;->a:Lco/uk/getmondo/feed/a/a;

    invoke-static {v3}, Lco/uk/getmondo/feed/a/a;->a(Lco/uk/getmondo/feed/a/a;)Landroid/content/Context;

    move-result-object v3

    const v4, 0x7f0a019c

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lkotlin/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/h;

    move-result-object v2

    aput-object v2, v0, v1

    .line 23
    const/4 v1, 0x3

    sget-object v2, Lco/uk/getmondo/feed/a/a/a;->i:Lco/uk/getmondo/feed/a/a/a;

    iget-object v3, p0, Lco/uk/getmondo/feed/a/a$b;->a:Lco/uk/getmondo/feed/a/a;

    invoke-static {v3}, Lco/uk/getmondo/feed/a/a;->a(Lco/uk/getmondo/feed/a/a;)Landroid/content/Context;

    move-result-object v3

    const v4, 0x7f0a01a2

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lkotlin/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/h;

    move-result-object v2

    aput-object v2, v0, v1

    .line 24
    const/4 v1, 0x4

    sget-object v2, Lco/uk/getmondo/feed/a/a/a;->k:Lco/uk/getmondo/feed/a/a/a;

    iget-object v3, p0, Lco/uk/getmondo/feed/a/a$b;->a:Lco/uk/getmondo/feed/a/a;

    invoke-static {v3}, Lco/uk/getmondo/feed/a/a;->a(Lco/uk/getmondo/feed/a/a;)Landroid/content/Context;

    move-result-object v3

    const v4, 0x7f0a0177

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lkotlin/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/h;

    move-result-object v2

    aput-object v2, v0, v1

    .line 25
    const/4 v1, 0x5

    sget-object v2, Lco/uk/getmondo/feed/a/a/a;->d:Lco/uk/getmondo/feed/a/a/a;

    iget-object v3, p0, Lco/uk/getmondo/feed/a/a$b;->a:Lco/uk/getmondo/feed/a/a;

    invoke-static {v3}, Lco/uk/getmondo/feed/a/a;->a(Lco/uk/getmondo/feed/a/a;)Landroid/content/Context;

    move-result-object v3

    const v4, 0x7f0a0232

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lkotlin/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/h;

    move-result-object v2

    aput-object v2, v0, v1

    .line 26
    const/4 v1, 0x6

    sget-object v2, Lco/uk/getmondo/feed/a/a/a;->e:Lco/uk/getmondo/feed/a/a/a;

    iget-object v3, p0, Lco/uk/getmondo/feed/a/a$b;->a:Lco/uk/getmondo/feed/a/a;

    invoke-static {v3}, Lco/uk/getmondo/feed/a/a;->a(Lco/uk/getmondo/feed/a/a;)Landroid/content/Context;

    move-result-object v3

    const v4, 0x7f0a0244

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lkotlin/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/h;

    move-result-object v2

    aput-object v2, v0, v1

    .line 27
    const/4 v1, 0x7

    sget-object v2, Lco/uk/getmondo/feed/a/a/a;->g:Lco/uk/getmondo/feed/a/a/a;

    iget-object v3, p0, Lco/uk/getmondo/feed/a/a$b;->a:Lco/uk/getmondo/feed/a/a;

    invoke-static {v3}, Lco/uk/getmondo/feed/a/a;->a(Lco/uk/getmondo/feed/a/a;)Landroid/content/Context;

    move-result-object v3

    const v4, 0x7f0a0242

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lkotlin/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/h;

    move-result-object v2

    aput-object v2, v0, v1

    .line 28
    const/16 v1, 0x8

    sget-object v2, Lco/uk/getmondo/feed/a/a/a;->f:Lco/uk/getmondo/feed/a/a/a;

    iget-object v3, p0, Lco/uk/getmondo/feed/a/a$b;->a:Lco/uk/getmondo/feed/a/a;

    invoke-static {v3}, Lco/uk/getmondo/feed/a/a;->a(Lco/uk/getmondo/feed/a/a;)Landroid/content/Context;

    move-result-object v3

    const v4, 0x7f0a0240

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lkotlin/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/h;

    move-result-object v2

    aput-object v2, v0, v1

    .line 29
    const/16 v1, 0x9

    sget-object v2, Lco/uk/getmondo/feed/a/a/a;->j:Lco/uk/getmondo/feed/a/a/a;

    iget-object v3, p0, Lco/uk/getmondo/feed/a/a$b;->a:Lco/uk/getmondo/feed/a/a;

    invoke-static {v3}, Lco/uk/getmondo/feed/a/a;->a(Lco/uk/getmondo/feed/a/a;)Landroid/content/Context;

    move-result-object v3

    const v4, 0x7f0a0296

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lkotlin/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/h;

    move-result-object v2

    aput-object v2, v0, v1

    .line 30
    const/16 v1, 0xa

    sget-object v2, Lco/uk/getmondo/feed/a/a/a;->l:Lco/uk/getmondo/feed/a/a/a;

    iget-object v3, p0, Lco/uk/getmondo/feed/a/a$b;->a:Lco/uk/getmondo/feed/a/a;

    invoke-static {v3}, Lco/uk/getmondo/feed/a/a;->a(Lco/uk/getmondo/feed/a/a;)Landroid/content/Context;

    move-result-object v3

    const v4, 0x7f0a02c7

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lkotlin/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/h;

    move-result-object v2

    aput-object v2, v0, v1

    .line 19
    invoke-static {v0}, Lkotlin/a/ab;->a([Lkotlin/h;)Ljava/util/Map;

    move-result-object v0

    .line 31
    return-object v0
.end method

.method public synthetic v_()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 15
    invoke-virtual {p0}, Lco/uk/getmondo/feed/a/a$b;->b()Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method
