.class public final enum Lco/uk/getmondo/feed/a/a/a;
.super Ljava/lang/Enum;
.source "FeedItemType.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lco/uk/getmondo/feed/a/a/a$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lco/uk/getmondo/feed/a/a/a;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0013\u0008\u0086\u0001\u0018\u0000 \u00152\u0008\u0012\u0004\u0012\u00020\u00000\u0001:\u0001\u0015B\u000f\u0008\u0002\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0005\u0010\u0006j\u0002\u0008\u0007j\u0002\u0008\u0008j\u0002\u0008\tj\u0002\u0008\nj\u0002\u0008\u000bj\u0002\u0008\u000cj\u0002\u0008\rj\u0002\u0008\u000ej\u0002\u0008\u000fj\u0002\u0008\u0010j\u0002\u0008\u0011j\u0002\u0008\u0012j\u0002\u0008\u0013j\u0002\u0008\u0014\u00a8\u0006\u0016"
    }
    d2 = {
        "Lco/uk/getmondo/feed/data/model/FeedItemType;",
        "",
        "type",
        "",
        "(Ljava/lang/String;ILjava/lang/String;)V",
        "getType",
        "()Ljava/lang/String;",
        "TRANSACTION",
        "KYC_REQUEST",
        "KYC_PASSED",
        "KYC_REJECTED",
        "KYC_SDD_REQUEST",
        "KYC_SDD_APPROVED",
        "KYC_SDD_REJECTED",
        "GOLDEN_TICKET_AWARDED",
        "GOLDEN_TICKET_SUBSEQUENT",
        "MONTHLY_SPENDING_REPORT",
        "EDD_LIMITS",
        "WELCOME_FEED_ITEM",
        "BASIC_TITLE_AND_BODY",
        "UNKNOWN",
        "Find",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# static fields
.field public static final enum a:Lco/uk/getmondo/feed/a/a/a;

.field public static final enum b:Lco/uk/getmondo/feed/a/a/a;

.field public static final enum c:Lco/uk/getmondo/feed/a/a/a;

.field public static final enum d:Lco/uk/getmondo/feed/a/a/a;

.field public static final enum e:Lco/uk/getmondo/feed/a/a/a;

.field public static final enum f:Lco/uk/getmondo/feed/a/a/a;

.field public static final enum g:Lco/uk/getmondo/feed/a/a/a;

.field public static final enum h:Lco/uk/getmondo/feed/a/a/a;

.field public static final enum i:Lco/uk/getmondo/feed/a/a/a;

.field public static final enum j:Lco/uk/getmondo/feed/a/a/a;

.field public static final enum k:Lco/uk/getmondo/feed/a/a/a;

.field public static final enum l:Lco/uk/getmondo/feed/a/a/a;

.field public static final enum m:Lco/uk/getmondo/feed/a/a/a;

.field public static final enum n:Lco/uk/getmondo/feed/a/a/a;

.field public static final o:Lco/uk/getmondo/feed/a/a/a$a;

.field private static final synthetic p:[Lco/uk/getmondo/feed/a/a/a;


# instance fields
.field private final q:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    const/16 v0, 0xe

    new-array v0, v0, [Lco/uk/getmondo/feed/a/a/a;

    new-instance v1, Lco/uk/getmondo/feed/a/a/a;

    const-string v2, "TRANSACTION"

    .line 4
    const-string v3, "transaction"

    invoke-direct {v1, v2, v4, v3}, Lco/uk/getmondo/feed/a/a/a;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lco/uk/getmondo/feed/a/a/a;->a:Lco/uk/getmondo/feed/a/a/a;

    aput-object v1, v0, v4

    new-instance v1, Lco/uk/getmondo/feed/a/a/a;

    const-string v2, "KYC_REQUEST"

    .line 5
    const-string v3, "kyc_request"

    invoke-direct {v1, v2, v5, v3}, Lco/uk/getmondo/feed/a/a/a;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lco/uk/getmondo/feed/a/a/a;->b:Lco/uk/getmondo/feed/a/a/a;

    aput-object v1, v0, v5

    new-instance v1, Lco/uk/getmondo/feed/a/a/a;

    const-string v2, "KYC_PASSED"

    .line 6
    const-string v3, "kyc_passed"

    invoke-direct {v1, v2, v6, v3}, Lco/uk/getmondo/feed/a/a/a;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lco/uk/getmondo/feed/a/a/a;->c:Lco/uk/getmondo/feed/a/a/a;

    aput-object v1, v0, v6

    new-instance v1, Lco/uk/getmondo/feed/a/a/a;

    const-string v2, "KYC_REJECTED"

    .line 7
    const-string v3, "kyc_rejected"

    invoke-direct {v1, v2, v7, v3}, Lco/uk/getmondo/feed/a/a/a;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lco/uk/getmondo/feed/a/a/a;->d:Lco/uk/getmondo/feed/a/a/a;

    aput-object v1, v0, v7

    new-instance v1, Lco/uk/getmondo/feed/a/a/a;

    const-string v2, "KYC_SDD_REQUEST"

    .line 8
    const-string v3, "sdd_migration_request"

    invoke-direct {v1, v2, v8, v3}, Lco/uk/getmondo/feed/a/a/a;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lco/uk/getmondo/feed/a/a/a;->e:Lco/uk/getmondo/feed/a/a/a;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    new-instance v2, Lco/uk/getmondo/feed/a/a/a;

    const-string v3, "KYC_SDD_APPROVED"

    const/4 v4, 0x5

    .line 9
    const-string v5, "sdd_migration_approved"

    invoke-direct {v2, v3, v4, v5}, Lco/uk/getmondo/feed/a/a/a;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v2, Lco/uk/getmondo/feed/a/a/a;->f:Lco/uk/getmondo/feed/a/a/a;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    new-instance v2, Lco/uk/getmondo/feed/a/a/a;

    const-string v3, "KYC_SDD_REJECTED"

    const/4 v4, 0x6

    .line 10
    const-string v5, "sdd_migration_rejected"

    invoke-direct {v2, v3, v4, v5}, Lco/uk/getmondo/feed/a/a/a;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v2, Lco/uk/getmondo/feed/a/a/a;->g:Lco/uk/getmondo/feed/a/a/a;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    new-instance v2, Lco/uk/getmondo/feed/a/a/a;

    const-string v3, "GOLDEN_TICKET_AWARDED"

    const/4 v4, 0x7

    .line 11
    const-string v5, "golden_ticket_awarded"

    invoke-direct {v2, v3, v4, v5}, Lco/uk/getmondo/feed/a/a/a;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v2, Lco/uk/getmondo/feed/a/a/a;->h:Lco/uk/getmondo/feed/a/a/a;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    new-instance v2, Lco/uk/getmondo/feed/a/a/a;

    const-string v3, "GOLDEN_TICKET_SUBSEQUENT"

    const/16 v4, 0x8

    .line 12
    const-string v5, "golden_ticket_subsequent_award"

    invoke-direct {v2, v3, v4, v5}, Lco/uk/getmondo/feed/a/a/a;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v2, Lco/uk/getmondo/feed/a/a/a;->i:Lco/uk/getmondo/feed/a/a/a;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    new-instance v2, Lco/uk/getmondo/feed/a/a/a;

    const-string v3, "MONTHLY_SPENDING_REPORT"

    const/16 v4, 0x9

    .line 13
    const-string v5, "spending_report"

    invoke-direct {v2, v3, v4, v5}, Lco/uk/getmondo/feed/a/a/a;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v2, Lco/uk/getmondo/feed/a/a/a;->j:Lco/uk/getmondo/feed/a/a/a;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    new-instance v2, Lco/uk/getmondo/feed/a/a/a;

    const-string v3, "EDD_LIMITS"

    const/16 v4, 0xa

    .line 14
    const-string v5, "kyc_enhanced_limits"

    invoke-direct {v2, v3, v4, v5}, Lco/uk/getmondo/feed/a/a/a;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v2, Lco/uk/getmondo/feed/a/a/a;->k:Lco/uk/getmondo/feed/a/a/a;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    new-instance v2, Lco/uk/getmondo/feed/a/a/a;

    const-string v3, "WELCOME_FEED_ITEM"

    const/16 v4, 0xb

    .line 15
    const-string v5, "onboarding_welcome"

    invoke-direct {v2, v3, v4, v5}, Lco/uk/getmondo/feed/a/a/a;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v2, Lco/uk/getmondo/feed/a/a/a;->l:Lco/uk/getmondo/feed/a/a/a;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    new-instance v2, Lco/uk/getmondo/feed/a/a/a;

    const-string v3, "BASIC_TITLE_AND_BODY"

    const/16 v4, 0xc

    .line 16
    const-string v5, "basic_title_and_body"

    invoke-direct {v2, v3, v4, v5}, Lco/uk/getmondo/feed/a/a/a;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v2, Lco/uk/getmondo/feed/a/a/a;->m:Lco/uk/getmondo/feed/a/a/a;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    new-instance v2, Lco/uk/getmondo/feed/a/a/a;

    const-string v3, "UNKNOWN"

    const/16 v4, 0xd

    .line 17
    const-string v5, "unknown"

    invoke-direct {v2, v3, v4, v5}, Lco/uk/getmondo/feed/a/a/a;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v2, Lco/uk/getmondo/feed/a/a/a;->n:Lco/uk/getmondo/feed/a/a/a;

    aput-object v2, v0, v1

    sput-object v0, Lco/uk/getmondo/feed/a/a/a;->p:[Lco/uk/getmondo/feed/a/a/a;

    new-instance v0, Lco/uk/getmondo/feed/a/a/a$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lco/uk/getmondo/feed/a/a/a$a;-><init>(Lkotlin/d/b/i;)V

    sput-object v0, Lco/uk/getmondo/feed/a/a/a;->o:Lco/uk/getmondo/feed/a/a/a$a;

    return-void
.end method

.method protected constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    const-string v0, "type"

    invoke-static {p3, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 3
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput-object p3, p0, Lco/uk/getmondo/feed/a/a/a;->q:Ljava/lang/String;

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lco/uk/getmondo/feed/a/a/a;
    .locals 1

    const-class v0, Lco/uk/getmondo/feed/a/a/a;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/feed/a/a/a;

    return-object v0
.end method

.method public static values()[Lco/uk/getmondo/feed/a/a/a;
    .locals 1

    sget-object v0, Lco/uk/getmondo/feed/a/a/a;->p:[Lco/uk/getmondo/feed/a/a/a;

    invoke-virtual {v0}, [Lco/uk/getmondo/feed/a/a/a;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lco/uk/getmondo/feed/a/a/a;

    return-object v0
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 3
    iget-object v0, p0, Lco/uk/getmondo/feed/a/a/a;->q:Ljava/lang/String;

    return-object v0
.end method
