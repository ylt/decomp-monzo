.class final Lco/uk/getmondo/feed/a/r$d;
.super Ljava/lang/Object;
.source "FeedStorage.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lco/uk/getmondo/feed/a/r;->b()Lio/reactivex/n;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h",
        "<TT;TR;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000\u0014\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u0010\u0012\u000c\u0012\n \u0003*\u0004\u0018\u00010\u00020\u00020\u00012\u0014\u0010\u0004\u001a\u0010\u0012\u000c\u0012\n \u0003*\u0004\u0018\u00010\u00020\u00020\u0005H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "Lco/uk/getmondo/common/data/QueryResults;",
        "Lco/uk/getmondo/model/FeedItem;",
        "kotlin.jvm.PlatformType",
        "it",
        "Lco/uk/getmondo/common/rx/RxRealm$Result;",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# static fields
.field public static final a:Lco/uk/getmondo/feed/a/r$d;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lco/uk/getmondo/feed/a/r$d;

    invoke-direct {v0}, Lco/uk/getmondo/feed/a/r$d;-><init>()V

    sput-object v0, Lco/uk/getmondo/feed/a/r$d;->a:Lco/uk/getmondo/feed/a/r$d;

    return-void
.end method

.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lco/uk/getmondo/common/j/g$a;)Lco/uk/getmondo/common/b/b;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lco/uk/getmondo/common/j/g$a",
            "<+",
            "Lco/uk/getmondo/d/m;",
            ">;)",
            "Lco/uk/getmondo/common/b/b",
            "<",
            "Lco/uk/getmondo/d/m;",
            ">;"
        }
    .end annotation

    .prologue
    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 114
    invoke-virtual {p1}, Lco/uk/getmondo/common/j/g$a;->e()Lco/uk/getmondo/common/b/b;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 20
    check-cast p1, Lco/uk/getmondo/common/j/g$a;

    invoke-virtual {p0, p1}, Lco/uk/getmondo/feed/a/r$d;->a(Lco/uk/getmondo/common/j/g$a;)Lco/uk/getmondo/common/b/b;

    move-result-object v0

    return-object v0
.end method
