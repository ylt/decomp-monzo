.class public final Lco/uk/getmondo/feed/a/a;
.super Ljava/lang/Object;
.source "FeedItemResourceProvider.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000:\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\u0008\t\n\u0002\u0010\u0008\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\"\n\u0002\u0008\u0002\u0018\u00002\u00020\u0001B\u0011\u0008\u0007\u0012\u0008\u0008\u0001\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0010\u0010\u0011\u001a\u00020\u00122\u0006\u0010\u0013\u001a\u00020\u0007H\u0007J\u000e\u0010\u0014\u001a\u00020\u00082\u0006\u0010\u0015\u001a\u00020\u0016J\u000e\u0010\u0017\u001a\u00020\u00082\u0006\u0010\u0015\u001a\u00020\u0016J\u0014\u0010\u0018\u001a\u0008\u0012\u0004\u0012\u00020\u00070\u00192\u0006\u0010\u001a\u001a\u00020\u0008R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R/\u0010\u0005\u001a\u0016\u0012\u0004\u0012\u00020\u0007\u0012\u000c\u0012\n \t*\u0004\u0018\u00010\u00080\u00080\u00068BX\u0082\u0084\u0002\u00a2\u0006\u000c\n\u0004\u0008\u000c\u0010\r\u001a\u0004\u0008\n\u0010\u000bR/\u0010\u000e\u001a\u0016\u0012\u0004\u0012\u00020\u0007\u0012\u000c\u0012\n \t*\u0004\u0018\u00010\u00080\u00080\u00068BX\u0082\u0084\u0002\u00a2\u0006\u000c\n\u0004\u0008\u0010\u0010\r\u001a\u0004\u0008\u000f\u0010\u000b\u00a8\u0006\u001b"
    }
    d2 = {
        "Lco/uk/getmondo/feed/data/FeedItemResourceProvider;",
        "",
        "context",
        "Landroid/content/Context;",
        "(Landroid/content/Context;)V",
        "subtitlesMap",
        "",
        "Lco/uk/getmondo/feed/data/model/FeedItemType;",
        "",
        "kotlin.jvm.PlatformType",
        "getSubtitlesMap",
        "()Ljava/util/Map;",
        "subtitlesMap$delegate",
        "Lkotlin/Lazy;",
        "titlesMap",
        "getTitlesMap",
        "titlesMap$delegate",
        "getIconRes",
        "",
        "type",
        "getSubtitle",
        "feedItem",
        "Lco/uk/getmondo/model/FeedItem;",
        "getTitle",
        "searchInResources",
        "",
        "searchTerm",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# static fields
.field static final synthetic a:[Lkotlin/reflect/l;


# instance fields
.field private final b:Lkotlin/c;

.field private final c:Lkotlin/c;

.field private final d:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const/4 v0, 0x2

    new-array v1, v0, [Lkotlin/reflect/l;

    const/4 v2, 0x0

    new-instance v0, Lkotlin/d/b/w;

    const-class v3, Lco/uk/getmondo/feed/a/a;

    invoke-static {v3}, Lkotlin/d/b/y;->a(Ljava/lang/Class;)Lkotlin/reflect/c;

    move-result-object v3

    const-string v4, "titlesMap"

    const-string v5, "getTitlesMap()Ljava/util/Map;"

    invoke-direct {v0, v3, v4, v5}, Lkotlin/d/b/w;-><init>(Lkotlin/reflect/e;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v0}, Lkotlin/d/b/y;->a(Lkotlin/d/b/v;)Lkotlin/reflect/n;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/l;

    aput-object v0, v1, v2

    const/4 v2, 0x1

    new-instance v0, Lkotlin/d/b/w;

    const-class v3, Lco/uk/getmondo/feed/a/a;

    invoke-static {v3}, Lkotlin/d/b/y;->a(Ljava/lang/Class;)Lkotlin/reflect/c;

    move-result-object v3

    const-string v4, "subtitlesMap"

    const-string v5, "getSubtitlesMap()Ljava/util/Map;"

    invoke-direct {v0, v3, v4, v5}, Lkotlin/d/b/w;-><init>(Lkotlin/reflect/e;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v0}, Lkotlin/d/b/y;->a(Lkotlin/d/b/v;)Lkotlin/reflect/n;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/l;

    aput-object v0, v1, v2

    sput-object v1, Lco/uk/getmondo/feed/a/a;->a:[Lkotlin/reflect/l;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lco/uk/getmondo/feed/a/a;->d:Landroid/content/Context;

    .line 18
    new-instance v0, Lco/uk/getmondo/feed/a/a$b;

    invoke-direct {v0, p0}, Lco/uk/getmondo/feed/a/a$b;-><init>(Lco/uk/getmondo/feed/a/a;)V

    check-cast v0, Lkotlin/d/a/a;

    invoke-static {v0}, Lkotlin/d;->a(Lkotlin/d/a/a;)Lkotlin/c;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/feed/a/a;->b:Lkotlin/c;

    .line 34
    new-instance v0, Lco/uk/getmondo/feed/a/a$a;

    invoke-direct {v0, p0}, Lco/uk/getmondo/feed/a/a$a;-><init>(Lco/uk/getmondo/feed/a/a;)V

    check-cast v0, Lkotlin/d/a/a;

    invoke-static {v0}, Lkotlin/d;->a(Lkotlin/d/a/a;)Lkotlin/c;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/feed/a/a;->c:Lkotlin/c;

    return-void
.end method

.method public static final synthetic a(Lco/uk/getmondo/feed/a/a;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 15
    iget-object v0, p0, Lco/uk/getmondo/feed/a/a;->d:Landroid/content/Context;

    return-object v0
.end method

.method private final a()Ljava/util/Map;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Lco/uk/getmondo/feed/a/a/a;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lco/uk/getmondo/feed/a/a;->b:Lkotlin/c;

    sget-object v1, Lco/uk/getmondo/feed/a/a;->a:[Lkotlin/reflect/l;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-interface {v0}, Lkotlin/c;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    return-object v0
.end method

.method private final b()Ljava/util/Map;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Lco/uk/getmondo/feed/a/a/a;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lco/uk/getmondo/feed/a/a;->c:Lkotlin/c;

    sget-object v1, Lco/uk/getmondo/feed/a/a;->a:[Lkotlin/reflect/l;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-interface {v0}, Lkotlin/c;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    return-object v0
.end method


# virtual methods
.method public final a(Lco/uk/getmondo/feed/a/a/a;)I
    .locals 2

    .prologue
    const-string v0, "type"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 50
    sget-object v0, Lco/uk/getmondo/feed/a/b;->a:[I

    invoke-virtual {p1}, Lco/uk/getmondo/feed/a/a/a;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 52
    const v0, 0x7f0201de

    .line 50
    :goto_0
    return v0

    .line 51
    :pswitch_0
    const v0, 0x7f02017c

    goto :goto_0

    .line 50
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public final a(Lco/uk/getmondo/d/m;)Ljava/lang/String;
    .locals 6

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    const-string v0, "feedItem"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 57
    invoke-virtual {p1}, Lco/uk/getmondo/d/m;->j()Lco/uk/getmondo/feed/a/a/a;

    move-result-object v0

    if-nez v0, :cond_1

    .line 65
    :goto_0
    invoke-direct {p0}, Lco/uk/getmondo/feed/a/a;->a()Ljava/util/Map;

    move-result-object v0

    invoke-virtual {p1}, Lco/uk/getmondo/d/m;->j()Lco/uk/getmondo/feed/a/a/a;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    if-eqz v0, :cond_4

    move-object v1, v0

    :goto_1
    move-object v0, v1

    .line 68
    check-cast v0, Ljava/lang/CharSequence;

    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-nez v0, :cond_5

    move v0, v2

    :goto_2
    if-eqz v0, :cond_0

    .line 69
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "There isn\'t title for feed item type "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Lco/uk/getmondo/d/m;->j()Lco/uk/getmondo/feed/a/a/a;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    invoke-static {v0}, Ld/a/a;->a(Ljava/lang/Throwable;)V

    .line 71
    :cond_0
    const-string v0, "title"

    invoke-static {v1, v0}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v1

    .line 57
    :cond_1
    sget-object v1, Lco/uk/getmondo/feed/a/b;->b:[I

    invoke-virtual {v0}, Lco/uk/getmondo/feed/a/a/a;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 59
    :pswitch_0
    invoke-virtual {p1}, Lco/uk/getmondo/d/m;->g()Lco/uk/getmondo/d/v;

    move-result-object v0

    .line 60
    if-nez v0, :cond_2

    invoke-static {}, Lkotlin/d/b/l;->a()V

    :cond_2
    invoke-virtual {v0}, Lco/uk/getmondo/d/v;->b()Lorg/threeten/bp/YearMonth;

    move-result-object v0

    invoke-virtual {v0}, Lorg/threeten/bp/YearMonth;->c()Lorg/threeten/bp/Month;

    move-result-object v0

    sget-object v1, Lorg/threeten/bp/format/TextStyle;->a:Lorg/threeten/bp/format/TextStyle;

    sget-object v4, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {v0, v1, v4}, Lorg/threeten/bp/Month;->a(Lorg/threeten/bp/format/TextStyle;Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    .line 61
    iget-object v1, p0, Lco/uk/getmondo/feed/a/a;->d:Landroid/content/Context;

    const v4, 0x7f0a0296

    new-array v5, v2, [Ljava/lang/Object;

    aput-object v0, v5, v3

    invoke-virtual {v1, v4, v5}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    goto :goto_1

    .line 63
    :pswitch_1
    invoke-virtual {p1}, Lco/uk/getmondo/d/m;->i()Lco/uk/getmondo/d/f;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-virtual {v0}, Lco/uk/getmondo/d/f;->a()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_3

    :goto_3
    move-object v1, v0

    goto :goto_1

    :cond_3
    const-string v0, ""

    goto :goto_3

    .line 65
    :cond_4
    const-string v0, ""

    move-object v1, v0

    goto :goto_1

    :cond_5
    move v0, v3

    .line 68
    goto :goto_2

    .line 57
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final a(Ljava/lang/String;)Ljava/util/Set;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/Set",
            "<",
            "Lco/uk/getmondo/feed/a/a/a;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v6, 0x1

    const-string v0, "searchTerm"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 89
    invoke-direct {p0}, Lco/uk/getmondo/feed/a/a;->a()Ljava/util/Map;

    move-result-object v1

    .line 95
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    check-cast v0, Ljava/util/Map;

    .line 96
    invoke-interface {v1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 89
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/CharSequence;

    move-object v3, p1

    check-cast v3, Ljava/lang/CharSequence;

    invoke-static {v2, v3, v6}, Lkotlin/h/j;->b(Ljava/lang/CharSequence;Ljava/lang/CharSequence;Z)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 98
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 101
    :cond_1
    nop

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v4

    .line 90
    invoke-direct {p0}, Lco/uk/getmondo/feed/a/a;->b()Ljava/util/Map;

    move-result-object v1

    .line 102
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    check-cast v0, Ljava/util/Map;

    .line 103
    invoke-interface {v1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_2
    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 90
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/CharSequence;

    move-object v3, p1

    check-cast v3, Ljava/lang/CharSequence;

    invoke-static {v2, v3, v6}, Lkotlin/h/j;->b(Ljava/lang/CharSequence;Ljava/lang/CharSequence;Z)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 105
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 108
    :cond_3
    nop

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    .line 91
    check-cast v0, Ljava/lang/Iterable;

    invoke-static {v4, v0}, Lkotlin/a/ah;->a(Ljava/util/Set;Ljava/lang/Iterable;)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public final b(Lco/uk/getmondo/d/m;)Ljava/lang/String;
    .locals 4

    .prologue
    const-string v0, "feedItem"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 75
    invoke-virtual {p1}, Lco/uk/getmondo/d/m;->j()Lco/uk/getmondo/feed/a/a/a;

    move-result-object v0

    if-nez v0, :cond_1

    .line 79
    :goto_0
    invoke-direct {p0}, Lco/uk/getmondo/feed/a/a;->b()Ljava/util/Map;

    move-result-object v0

    invoke-virtual {p1}, Lco/uk/getmondo/d/m;->j()Lco/uk/getmondo/feed/a/a/a;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    if-eqz v0, :cond_4

    move-object v1, v0

    :goto_1
    move-object v0, v1

    .line 82
    check-cast v0, Ljava/lang/CharSequence;

    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-nez v0, :cond_5

    const/4 v0, 0x1

    :goto_2
    if-eqz v0, :cond_0

    .line 83
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "There isn\'t subtitle for feed item type "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Lco/uk/getmondo/d/m;->j()Lco/uk/getmondo/feed/a/a/a;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    invoke-static {v0}, Ld/a/a;->a(Ljava/lang/Throwable;)V

    .line 85
    :cond_0
    const-string v0, "subtitle"

    invoke-static {v1, v0}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v1

    .line 75
    :cond_1
    sget-object v1, Lco/uk/getmondo/feed/a/b;->c:[I

    invoke-virtual {v0}, Lco/uk/getmondo/feed/a/a/a;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 76
    :pswitch_0
    iget-object v0, p0, Lco/uk/getmondo/feed/a/a;->d:Landroid/content/Context;

    invoke-virtual {p1}, Lco/uk/getmondo/d/m;->h()Lco/uk/getmondo/d/r;

    move-result-object v1

    if-nez v1, :cond_2

    invoke-static {}, Lkotlin/d/b/l;->a()V

    :cond_2
    invoke-virtual {v1}, Lco/uk/getmondo/d/r;->a()Lco/uk/getmondo/signup/identity_verification/y;

    move-result-object v1

    invoke-virtual {v1}, Lco/uk/getmondo/signup/identity_verification/y;->b()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    goto :goto_1

    .line 77
    :pswitch_1
    invoke-virtual {p1}, Lco/uk/getmondo/d/m;->i()Lco/uk/getmondo/d/f;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-virtual {v0}, Lco/uk/getmondo/d/f;->b()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_3

    :goto_3
    move-object v1, v0

    goto :goto_1

    :cond_3
    const-string v0, ""

    goto :goto_3

    .line 79
    :cond_4
    const-string v0, ""

    move-object v1, v0

    goto :goto_1

    .line 82
    :cond_5
    const/4 v0, 0x0

    goto :goto_2

    .line 75
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
