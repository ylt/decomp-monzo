.class public final Lco/uk/getmondo/feed/a/t;
.super Ljava/lang/Object;
.source "PeerManager.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\u0008\u0007\u0018\u00002\u00020\u0001B\u0017\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\u000c\u0010\u0007\u001a\u0008\u0012\u0004\u0012\u00020\u00010\u0008R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\t"
    }
    d2 = {
        "Lco/uk/getmondo/feed/data/PeerManager;",
        "",
        "rxContacts",
        "Lco/uk/getmondo/payments/send/contacts/RxContacts;",
        "peerStorage",
        "Lco/uk/getmondo/feed/data/PeerStorage;",
        "(Lco/uk/getmondo/payments/send/contacts/RxContacts;Lco/uk/getmondo/feed/data/PeerStorage;)V",
        "keepPeersEnriched",
        "Lio/reactivex/Observable;",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field private final a:Lco/uk/getmondo/payments/send/a/e;

.field private final b:Lco/uk/getmondo/feed/a/v;


# direct methods
.method public constructor <init>(Lco/uk/getmondo/payments/send/a/e;Lco/uk/getmondo/feed/a/v;)V
    .locals 1

    .prologue
    const-string v0, "rxContacts"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "peerStorage"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lco/uk/getmondo/feed/a/t;->a:Lco/uk/getmondo/payments/send/a/e;

    iput-object p2, p0, Lco/uk/getmondo/feed/a/t;->b:Lco/uk/getmondo/feed/a/v;

    return-void
.end method

.method public static final synthetic a(Lco/uk/getmondo/feed/a/t;)Lco/uk/getmondo/feed/a/v;
    .locals 1

    .prologue
    .line 8
    iget-object v0, p0, Lco/uk/getmondo/feed/a/t;->b:Lco/uk/getmondo/feed/a/v;

    return-object v0
.end method


# virtual methods
.method public final a()Lio/reactivex/n;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 22
    iget-object v0, p0, Lco/uk/getmondo/feed/a/t;->a:Lco/uk/getmondo/payments/send/a/e;

    invoke-virtual {v0}, Lco/uk/getmondo/payments/send/a/e;->a()Lio/reactivex/n;

    move-result-object v1

    .line 23
    new-instance v0, Lco/uk/getmondo/feed/a/t$a;

    invoke-direct {v0, p0}, Lco/uk/getmondo/feed/a/t$a;-><init>(Lco/uk/getmondo/feed/a/t;)V

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {v1, v0}, Lio/reactivex/n;->flatMap(Lio/reactivex/c/h;)Lio/reactivex/n;

    move-result-object v0

    const-string v1, "rxContacts.latestContact\u2026fault(0).toObservable() }"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method
