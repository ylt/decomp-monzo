.class public final Lco/uk/getmondo/feed/a/r;
.super Ljava/lang/Object;
.source "FeedStorage.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lco/uk/getmondo/feed/a/r$a;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000d\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0008\u0005\n\u0002\u0010\"\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0008\u0001\u0018\u0000 %2\u00020\u0001:\u0001%B\u0007\u0008\u0007\u00a2\u0006\u0002\u0010\u0002J\u000e\u0010\u0007\u001a\u00020\u00082\u0006\u0010\t\u001a\u00020\nJ\u0012\u0010\u000b\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u000e0\r0\u000cJ\u000c\u0010\u000f\u001a\u0008\u0012\u0004\u0012\u00020\u000e0\u000cJ\u0010\u0010\u0010\u001a\u0004\u0018\u00010\u00112\u0006\u0010\u0012\u001a\u00020\nJ\u0018\u0010\u0013\u001a\u00020\u00142\u0006\u0010\u0015\u001a\u00020\u00162\u0006\u0010\u0017\u001a\u00020\u0018H\u0002J\u0014\u0010\u0013\u001a\u00020\u00082\u000c\u0010\u0019\u001a\u0008\u0012\u0004\u0012\u00020\u00180\u001aJ\u000e\u0010\u001b\u001a\u00020\u00082\u0006\u0010\u001c\u001a\u00020\u000eJ<\u0010\u001d\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u000e0\r0\u000c2\u0008\u0008\u0002\u0010\u001e\u001a\u00020\n2\u000e\u0008\u0002\u0010\u001f\u001a\u0008\u0012\u0004\u0012\u00020!0 2\u000e\u0008\u0002\u0010\"\u001a\u0008\u0012\u0004\u0012\u00020#0 J \u0010$\u001a\u00020\u00142\u0006\u0010\u0015\u001a\u00020\u00162\u0006\u0010\u001c\u001a\u00020\u000e2\u0006\u0010\u0012\u001a\u00020\nH\u0002R\u0013\u0010\u0003\u001a\u0004\u0018\u00010\u00048F\u00a2\u0006\u0006\u001a\u0004\u0008\u0005\u0010\u0006\u00a8\u0006&"
    }
    d2 = {
        "Lco/uk/getmondo/feed/data/FeedStorage;",
        "",
        "()V",
        "feedMetadata",
        "Lco/uk/getmondo/model/FeedMetadata;",
        "getFeedMetadata",
        "()Lco/uk/getmondo/model/FeedMetadata;",
        "deleteFeedItem",
        "Lio/reactivex/Completable;",
        "feedItemId",
        "",
        "feedItems",
        "Lio/reactivex/Observable;",
        "Lco/uk/getmondo/common/data/QueryResults;",
        "Lco/uk/getmondo/model/FeedItem;",
        "knowYourCustomerRequestFeedItem",
        "mostRecentItemDateWithAccountId",
        "Ljava/util/Date;",
        "accountId",
        "saveFeed",
        "",
        "realm",
        "Lio/realm/Realm;",
        "feed",
        "Lco/uk/getmondo/feed/data/model/GetFeedResult;",
        "feedResults",
        "",
        "saveFeedItem",
        "feedItem",
        "searchFeedItems",
        "searchTerm",
        "types",
        "",
        "Lco/uk/getmondo/feed/data/model/FeedItemType;",
        "categories",
        "Lco/uk/getmondo/model/Category;",
        "updateIfDifferent",
        "Companion",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# static fields
.field public static final a:Lco/uk/getmondo/feed/a/r$a;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lco/uk/getmondo/feed/a/r$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lco/uk/getmondo/feed/a/r$a;-><init>(Lkotlin/d/b/i;)V

    sput-object v0, Lco/uk/getmondo/feed/a/r;->a:Lco/uk/getmondo/feed/a/r$a;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static final synthetic a(Lco/uk/getmondo/feed/a/r;Lio/realm/av;Lco/uk/getmondo/d/m;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0, p1, p2, p3}, Lco/uk/getmondo/feed/a/r;->a(Lio/realm/av;Lco/uk/getmondo/d/m;Ljava/lang/String;)V

    return-void
.end method

.method public static final synthetic a(Lco/uk/getmondo/feed/a/r;Lio/realm/av;Lco/uk/getmondo/feed/a/a/b;)V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0, p1, p2}, Lco/uk/getmondo/feed/a/r;->a(Lio/realm/av;Lco/uk/getmondo/feed/a/a/b;)V

    return-void
.end method

.method private final a(Lio/realm/av;Lco/uk/getmondo/d/m;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 90
    const-class v0, Lco/uk/getmondo/d/m;

    invoke-virtual {p1, v0}, Lio/realm/av;->a(Ljava/lang/Class;)Lio/realm/bf;

    move-result-object v0

    .line 91
    const-string v1, "id"

    invoke-virtual {p2}, Lco/uk/getmondo/d/m;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lio/realm/bf;->a(Ljava/lang/String;Ljava/lang/String;)Lio/realm/bf;

    move-result-object v0

    .line 92
    const-string v1, "accountId"

    invoke-virtual {v0, v1, p3}, Lio/realm/bf;->a(Ljava/lang/String;Ljava/lang/String;)Lio/realm/bf;

    move-result-object v0

    .line 93
    invoke-virtual {v0}, Lio/realm/bf;->h()Lio/realm/bb;

    move-result-object v0

    .line 90
    check-cast v0, Lco/uk/getmondo/d/m;

    .line 94
    if-eqz v0, :cond_0

    check-cast v0, Lio/realm/bb;

    invoke-virtual {p1, v0}, Lio/realm/av;->e(Lio/realm/bb;)Lio/realm/bb;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/d/m;

    invoke-static {p2, v0}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_1

    .line 97
    :cond_0
    check-cast p2, Lio/realm/bb;

    invoke-virtual {p1, p2}, Lio/realm/av;->d(Lio/realm/bb;)V

    .line 99
    :cond_1
    return-void
.end method

.method private final a(Lio/realm/av;Lco/uk/getmondo/feed/a/a/b;)V
    .locals 8

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 67
    invoke-virtual {p2}, Lco/uk/getmondo/feed/a/a/b;->b()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    new-array v1, v0, [Ljava/lang/String;

    .line 68
    invoke-virtual {p2}, Lco/uk/getmondo/feed/a/a/b;->a()Ljava/lang/String;

    move-result-object v5

    .line 69
    invoke-virtual {p2}, Lco/uk/getmondo/feed/a/a/b;->b()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    invoke-static {v0}, Lkotlin/a/m;->a(Ljava/util/Collection;)Lkotlin/f/c;

    move-result-object v0

    check-cast v0, Lkotlin/f/a;

    invoke-static {v0}, Lkotlin/f/d;->a(Lkotlin/f/a;)Lkotlin/f/a;

    move-result-object v2

    invoke-virtual {v2}, Lkotlin/f/a;->a()I

    move-result v0

    invoke-virtual {v2}, Lkotlin/f/a;->b()I

    move-result v6

    invoke-virtual {v2}, Lkotlin/f/a;->c()I

    move-result v7

    if-lez v7, :cond_0

    if-gt v0, v6, :cond_1

    move v2, v0

    .line 70
    :goto_0
    invoke-virtual {p2}, Lco/uk/getmondo/feed/a/a/b;->b()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/d/m;

    .line 71
    invoke-direct {p0, p1, v0, v5}, Lco/uk/getmondo/feed/a/r;->a(Lio/realm/av;Lco/uk/getmondo/d/m;Ljava/lang/String;)V

    .line 72
    invoke-virtual {v0}, Lco/uk/getmondo/d/m;->a()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v1, v2

    .line 69
    if-eq v2, v6, :cond_1

    add-int v0, v2, v7

    move v2, v0

    goto :goto_0

    :cond_0
    if-lt v0, v6, :cond_1

    move v2, v0

    goto :goto_0

    .line 76
    :cond_1
    invoke-virtual {p2}, Lco/uk/getmondo/feed/a/a/b;->c()Ljava/util/Date;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 77
    const-class v0, Lco/uk/getmondo/d/m;

    invoke-virtual {p1, v0}, Lio/realm/av;->a(Ljava/lang/Class;)Lio/realm/bf;

    move-result-object v0

    .line 78
    const-string v2, "accountId"

    invoke-virtual {v0, v2, v5}, Lio/realm/bf;->a(Ljava/lang/String;Ljava/lang/String;)Lio/realm/bf;

    move-result-object v0

    .line 79
    const-string v2, "created"

    invoke-virtual {p2}, Lco/uk/getmondo/feed/a/a/b;->c()Ljava/util/Date;

    move-result-object v5

    invoke-virtual {v0, v2, v5}, Lio/realm/bf;->a(Ljava/lang/String;Ljava/util/Date;)Lio/realm/bf;

    move-result-object v2

    move-object v0, v1

    .line 80
    check-cast v0, [Ljava/lang/Object;

    array-length v0, v0

    if-nez v0, :cond_4

    move v0, v3

    :goto_1
    if-nez v0, :cond_5

    move v0, v3

    :goto_2
    if-eqz v0, :cond_2

    .line 81
    invoke-virtual {v2}, Lio/realm/bf;->d()Lio/realm/bf;

    move-result-object v0

    const-string v3, "id"

    invoke-virtual {v0, v3, v1}, Lio/realm/bf;->a(Ljava/lang/String;[Ljava/lang/String;)Lio/realm/bf;

    .line 84
    :cond_2
    const-string v0, "id"

    const-string v1, "overdraft_charges_item_id"

    invoke-virtual {v2, v0, v1}, Lio/realm/bf;->b(Ljava/lang/String;Ljava/lang/String;)Lio/realm/bf;

    .line 85
    invoke-virtual {v2}, Lio/realm/bf;->f()Lio/realm/bg;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/bg;->b()Z

    .line 87
    :cond_3
    return-void

    :cond_4
    move v0, v4

    .line 80
    goto :goto_1

    :cond_5
    move v0, v4

    goto :goto_2
.end method


# virtual methods
.method public final a()Lco/uk/getmondo/d/n;
    .locals 5

    .prologue
    .line 24
    invoke-static {}, Lio/realm/av;->n()Lio/realm/av;

    move-result-object v1

    check-cast v1, Ljava/io/Closeable;

    const/4 v4, 0x0

    nop

    :try_start_0
    move-object v0, v1

    check-cast v0, Lio/realm/av;

    move-object v2, v0

    .line 25
    const-class v3, Lco/uk/getmondo/d/n;

    invoke-virtual {v2, v3}, Lio/realm/av;->a(Ljava/lang/Class;)Lio/realm/bf;

    move-result-object v3

    invoke-virtual {v3}, Lio/realm/bf;->h()Lio/realm/bb;

    move-result-object v3

    check-cast v3, Lco/uk/getmondo/d/n;

    .line 26
    if-eqz v3, :cond_1

    .line 27
    check-cast v3, Lio/realm/bb;

    invoke-virtual {v2, v3}, Lio/realm/av;->e(Lio/realm/bb;)Lio/realm/bb;

    move-result-object v2

    check-cast v2, Lco/uk/getmondo/d/n;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    if-eqz v1, :cond_0

    invoke-interface {v1}, Ljava/io/Closeable;->close()V

    .line 30
    :cond_0
    :goto_0
    return-object v2

    .line 29
    :cond_1
    nop

    :try_start_1
    sget-object v2, Lkotlin/n;->a:Lkotlin/n;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 24
    if-eqz v1, :cond_2

    invoke-interface {v1}, Ljava/io/Closeable;->close()V

    .line 30
    :cond_2
    const/4 v2, 0x0

    goto :goto_0

    .line 24
    :catch_0
    move-exception v2

    const/4 v3, 0x1

    nop

    if-eqz v1, :cond_3

    :try_start_2
    invoke-interface {v1}, Ljava/io/Closeable;->close()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :cond_3
    :goto_1
    :try_start_3
    check-cast v2, Ljava/lang/Throwable;

    throw v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :catchall_0
    move-exception v2

    :goto_2
    if-nez v3, :cond_4

    if-eqz v1, :cond_4

    invoke-interface {v1}, Ljava/io/Closeable;->close()V

    :cond_4
    throw v2

    :catch_1
    move-exception v4

    goto :goto_1

    :catchall_1
    move-exception v2

    move v3, v4

    goto :goto_2
.end method

.method public final a(Lco/uk/getmondo/d/m;)Lio/reactivex/b;
    .locals 1

    .prologue
    const-string v0, "feedItem"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 59
    new-instance v0, Lco/uk/getmondo/feed/a/r$i;

    invoke-direct {v0, p0, p1}, Lco/uk/getmondo/feed/a/r$i;-><init>(Lco/uk/getmondo/feed/a/r;Lco/uk/getmondo/d/m;)V

    check-cast v0, Lio/realm/av$a;

    invoke-static {v0}, Lco/uk/getmondo/common/j/g;->a(Lio/realm/av$a;)Lio/reactivex/b;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/util/List;)Lio/reactivex/b;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lco/uk/getmondo/feed/a/a/b;",
            ">;)",
            "Lio/reactivex/b;"
        }
    .end annotation

    .prologue
    const-string v0, "feedResults"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 45
    new-instance v0, Lco/uk/getmondo/feed/a/r$h;

    invoke-direct {v0, p0, p1}, Lco/uk/getmondo/feed/a/r$h;-><init>(Lco/uk/getmondo/feed/a/r;Ljava/util/List;)V

    check-cast v0, Lio/realm/av$a;

    invoke-static {v0}, Lco/uk/getmondo/common/j/g;->a(Lio/realm/av$a;)Lio/reactivex/b;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;Ljava/util/Set;Ljava/util/Set;)Lio/reactivex/n;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Set",
            "<+",
            "Lco/uk/getmondo/feed/a/a/a;",
            ">;",
            "Ljava/util/Set",
            "<+",
            "Lco/uk/getmondo/d/h;",
            ">;)",
            "Lio/reactivex/n",
            "<",
            "Lco/uk/getmondo/common/b/b",
            "<",
            "Lco/uk/getmondo/d/m;",
            ">;>;"
        }
    .end annotation

    .prologue
    const-string v0, "searchTerm"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "types"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "categories"

    invoke-static {p3, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 123
    new-instance v0, Lco/uk/getmondo/feed/a/r$j;

    invoke-direct {v0, p1, p2, p3}, Lco/uk/getmondo/feed/a/r$j;-><init>(Ljava/lang/String;Ljava/util/Set;Ljava/util/Set;)V

    check-cast v0, Lkotlin/d/a/b;

    invoke-static {v0}, Lco/uk/getmondo/common/j/g;->a(Lkotlin/d/a/b;)Lio/reactivex/n;

    move-result-object v1

    .line 146
    sget-object v0, Lco/uk/getmondo/feed/a/r$k;->a:Lco/uk/getmondo/feed/a/r$k;

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {v1, v0}, Lio/reactivex/n;->map(Lio/reactivex/c/h;)Lio/reactivex/n;

    move-result-object v0

    const-string v1, "RxRealm.asObservable { r\u2026}.map { it.queryResults }"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public final a(Ljava/lang/String;)Ljava/util/Date;
    .locals 5

    .prologue
    const-string v1, "accountId"

    invoke-static {p1, v1}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 34
    invoke-static {}, Lio/realm/av;->n()Lio/realm/av;

    move-result-object v1

    check-cast v1, Ljava/io/Closeable;

    const/4 v3, 0x0

    nop

    :try_start_0
    move-object v0, v1

    check-cast v0, Lio/realm/av;

    move-object v2, v0

    .line 35
    const-class v4, Lco/uk/getmondo/d/m;

    invoke-virtual {v2, v4}, Lio/realm/av;->a(Ljava/lang/Class;)Lio/realm/bf;

    move-result-object v2

    .line 36
    const-string v4, "accountId"

    invoke-virtual {v2, v4, p1}, Lio/realm/bf;->a(Ljava/lang/String;Ljava/lang/String;)Lio/realm/bf;

    move-result-object v2

    .line 37
    const-string v4, "created"

    invoke-virtual {v2, v4}, Lio/realm/bf;->d(Ljava/lang/String;)Ljava/util/Date;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v2

    if-eqz v1, :cond_0

    invoke-interface {v1}, Ljava/io/Closeable;->close()V

    :cond_0
    return-object v2

    .line 34
    :catch_0
    move-exception v2

    const/4 v3, 0x1

    nop

    if-eqz v1, :cond_1

    :try_start_1
    invoke-interface {v1}, Ljava/io/Closeable;->close()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_1
    :goto_0
    :try_start_2
    check-cast v2, Ljava/lang/Throwable;

    throw v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :catchall_0
    move-exception v2

    if-nez v3, :cond_2

    if-eqz v1, :cond_2

    invoke-interface {v1}, Ljava/io/Closeable;->close()V

    :cond_2
    throw v2

    :catch_1
    move-exception v4

    goto :goto_0
.end method

.method public final b(Ljava/lang/String;)Lio/reactivex/b;
    .locals 1

    .prologue
    const-string v0, "feedItemId"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 102
    new-instance v0, Lco/uk/getmondo/feed/a/r$b;

    invoke-direct {v0, p1}, Lco/uk/getmondo/feed/a/r$b;-><init>(Ljava/lang/String;)V

    check-cast v0, Lio/realm/av$a;

    invoke-static {v0}, Lco/uk/getmondo/common/j/g;->a(Lio/realm/av$a;)Lio/reactivex/b;

    move-result-object v0

    return-object v0
.end method

.method public final b()Lio/reactivex/n;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Lco/uk/getmondo/common/b/b",
            "<",
            "Lco/uk/getmondo/d/m;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 111
    sget-object v0, Lco/uk/getmondo/feed/a/r$c;->a:Lco/uk/getmondo/feed/a/r$c;

    check-cast v0, Lkotlin/d/a/b;

    invoke-static {v0}, Lco/uk/getmondo/common/j/g;->a(Lkotlin/d/a/b;)Lio/reactivex/n;

    move-result-object v1

    .line 114
    sget-object v0, Lco/uk/getmondo/feed/a/r$d;->a:Lco/uk/getmondo/feed/a/r$d;

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {v1, v0}, Lio/reactivex/n;->map(Lio/reactivex/c/h;)Lio/reactivex/n;

    move-result-object v0

    const-string v1, "RxRealm.asObservable { r\u2026}.map { it.queryResults }"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public final c()Lio/reactivex/n;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Lco/uk/getmondo/d/m;",
            ">;"
        }
    .end annotation

    .prologue
    .line 150
    sget-object v0, Lco/uk/getmondo/feed/a/r$e;->a:Lco/uk/getmondo/feed/a/r$e;

    check-cast v0, Lkotlin/d/a/b;

    invoke-static {v0}, Lco/uk/getmondo/common/j/g;->a(Lkotlin/d/a/b;)Lio/reactivex/n;

    move-result-object v1

    .line 154
    sget-object v0, Lco/uk/getmondo/feed/a/r$f;->a:Lco/uk/getmondo/feed/a/r$f;

    check-cast v0, Lio/reactivex/c/q;

    invoke-virtual {v1, v0}, Lio/reactivex/n;->filter(Lio/reactivex/c/q;)Lio/reactivex/n;

    move-result-object v1

    sget-object v0, Lco/uk/getmondo/feed/a/r$g;->a:Lco/uk/getmondo/feed/a/r$g;

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {v1, v0}, Lio/reactivex/n;->map(Lio/reactivex/c/h;)Lio/reactivex/n;

    move-result-object v0

    const-string v1, "RxRealm.asObservable { r\u2026.copyFirstFromRealm()!! }"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method
