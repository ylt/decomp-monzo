.class public final Lco/uk/getmondo/feed/a/v;
.super Ljava/lang/Object;
.source "PeerStorage.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010$\n\u0002\u0010\u000e\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0007\u0008\u0007\u00a2\u0006\u0002\u0010\u0002J\u001a\u0010\u0003\u001a\u00020\u00042\u0012\u0010\u0005\u001a\u000e\u0012\u0004\u0012\u00020\u0007\u0012\u0004\u0012\u00020\u00080\u0006\u00a8\u0006\t"
    }
    d2 = {
        "Lco/uk/getmondo/feed/data/PeerStorage;",
        "",
        "()V",
        "enrichPeers",
        "Lio/reactivex/Completable;",
        "contactsMap",
        "",
        "",
        "Lco/uk/getmondo/payments/send/contacts/Contact;",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/util/Map;)Lio/reactivex/b;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lco/uk/getmondo/payments/send/a/b;",
            ">;)",
            "Lio/reactivex/b;"
        }
    .end annotation

    .prologue
    const-string v0, "contactsMap"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 14
    new-instance v0, Lco/uk/getmondo/feed/a/v$a;

    invoke-direct {v0, p1}, Lco/uk/getmondo/feed/a/v$a;-><init>(Ljava/util/Map;)V

    check-cast v0, Lio/realm/av$a;

    invoke-static {v0}, Lco/uk/getmondo/common/j/g;->a(Lio/realm/av$a;)Lio/reactivex/b;

    move-result-object v0

    return-object v0
.end method
