.class final Lco/uk/getmondo/feed/a/v$a;
.super Ljava/lang/Object;
.source "PeerStorage.kt"

# interfaces
.implements Lio/realm/av$a;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lco/uk/getmondo/feed/a/v;->a(Ljava/util/Map;)Lio/reactivex/b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "",
        "realm",
        "Lio/realm/Realm;",
        "kotlin.jvm.PlatformType",
        "execute"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field final synthetic a:Ljava/util/Map;


# direct methods
.method constructor <init>(Ljava/util/Map;)V
    .locals 0

    iput-object p1, p0, Lco/uk/getmondo/feed/a/v$a;->a:Ljava/util/Map;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lio/realm/av;)V
    .locals 7

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 15
    const-class v0, Lco/uk/getmondo/d/aa;

    invoke-virtual {p1, v0}, Lio/realm/av;->a(Ljava/lang/Class;)Lio/realm/bf;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/bf;->f()Lio/realm/bg;

    move-result-object v0

    .line 16
    check-cast v0, Ljava/lang/Iterable;

    .line 30
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/d/aa;

    .line 17
    invoke-virtual {v0}, Lco/uk/getmondo/d/aa;->d()Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    if-eqz v1, :cond_0

    invoke-static {v1}, Lkotlin/h/j;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    move v1, v3

    :goto_1
    if-nez v1, :cond_3

    .line 18
    iget-object v1, p0, Lco/uk/getmondo/feed/a/v$a;->a:Ljava/util/Map;

    invoke-virtual {v0}, Lco/uk/getmondo/d/aa;->d()Ljava/lang/String;

    move-result-object v5

    if-nez v1, :cond_2

    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type kotlin.collections.Map<K, V>"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    move v1, v2

    .line 17
    goto :goto_1

    .line 18
    :cond_2
    invoke-interface {v1, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lco/uk/getmondo/payments/send/a/b;

    .line 19
    invoke-virtual {v0, v1}, Lco/uk/getmondo/d/aa;->a(Lco/uk/getmondo/payments/send/a/b;)Lco/uk/getmondo/d/aa;

    .line 23
    :goto_2
    nop

    goto :goto_0

    .line 21
    :cond_3
    const-string v1, "Skipping enrichment of peer %s(phone: %s), phone number empty"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    .line 22
    invoke-virtual {v0}, Lco/uk/getmondo/d/aa;->a()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v2

    invoke-virtual {v0}, Lco/uk/getmondo/d/aa;->d()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v5, v3

    .line 21
    invoke-static {v1, v5}, Ld/a/a;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_2

    .line 31
    :cond_4
    nop

    .line 25
    return-void
.end method
