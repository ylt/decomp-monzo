.class final Lco/uk/getmondo/feed/a/r$j;
.super Lkotlin/d/b/m;
.source "FeedStorage.kt"

# interfaces
.implements Lkotlin/d/a/b;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lco/uk/getmondo/feed/a/r;->a(Ljava/lang/String;Ljava/util/Set;Ljava/util/Set;)Lio/reactivex/n;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/d/b/m;",
        "Lkotlin/d/a/b",
        "<",
        "Lio/realm/av;",
        "Lio/realm/bg",
        "<",
        "Lco/uk/getmondo/d/m;",
        ">;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000\u0014\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a&\u0012\u000c\u0012\n \u0003*\u0004\u0018\u00010\u00020\u0002 \u0003*\u0012\u0012\u000c\u0012\n \u0003*\u0004\u0018\u00010\u00020\u0002\u0018\u00010\u00010\u00012\u0006\u0010\u0004\u001a\u00020\u0005H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "Lio/realm/RealmResults;",
        "Lco/uk/getmondo/model/FeedItem;",
        "kotlin.jvm.PlatformType",
        "realm",
        "Lio/realm/Realm;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field final synthetic a:Ljava/lang/String;

.field final synthetic b:Ljava/util/Set;

.field final synthetic c:Ljava/util/Set;


# direct methods
.method constructor <init>(Ljava/lang/String;Ljava/util/Set;Ljava/util/Set;)V
    .locals 1

    iput-object p1, p0, Lco/uk/getmondo/feed/a/r$j;->a:Ljava/lang/String;

    iput-object p2, p0, Lco/uk/getmondo/feed/a/r$j;->b:Ljava/util/Set;

    iput-object p3, p0, Lco/uk/getmondo/feed/a/r$j;->c:Ljava/util/Set;

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lkotlin/d/b/m;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final a(Lio/realm/av;)Lio/realm/bg;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/realm/av;",
            ")",
            "Lio/realm/bg",
            "<",
            "Lco/uk/getmondo/d/m;",
            ">;"
        }
    .end annotation

    .prologue
    const/16 v8, 0xa

    const/4 v2, 0x1

    const/4 v3, 0x0

    const-string v0, "realm"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 124
    const-class v0, Lco/uk/getmondo/d/m;

    invoke-virtual {p1, v0}, Lio/realm/av;->a(Ljava/lang/Class;)Lio/realm/bf;

    move-result-object v4

    .line 125
    iget-object v0, p0, Lco/uk/getmondo/feed/a/r$j;->a:Ljava/lang/String;

    check-cast v0, Ljava/lang/CharSequence;

    invoke-static {v0}, Lkotlin/h/j;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v2

    :goto_0
    if-eqz v0, :cond_0

    .line 126
    const-string v0, "transaction.merchant.name"

    iget-object v1, p0, Lco/uk/getmondo/feed/a/r$j;->a:Ljava/lang/String;

    sget-object v5, Lio/realm/l;->b:Lio/realm/l;

    invoke-virtual {v4, v0, v1, v5}, Lio/realm/bf;->c(Ljava/lang/String;Ljava/lang/String;Lio/realm/l;)Lio/realm/bf;

    move-result-object v0

    .line 127
    invoke-virtual {v0}, Lio/realm/bf;->c()Lio/realm/bf;

    move-result-object v0

    const-string v1, "transaction.merchant.formattedAddress"

    iget-object v5, p0, Lco/uk/getmondo/feed/a/r$j;->a:Ljava/lang/String;

    sget-object v6, Lio/realm/l;->b:Lio/realm/l;

    invoke-virtual {v0, v1, v5, v6}, Lio/realm/bf;->c(Ljava/lang/String;Ljava/lang/String;Lio/realm/l;)Lio/realm/bf;

    move-result-object v0

    .line 128
    invoke-virtual {v0}, Lio/realm/bf;->c()Lio/realm/bf;

    move-result-object v0

    const-string v1, "transaction.merchant.emoji"

    iget-object v5, p0, Lco/uk/getmondo/feed/a/r$j;->a:Ljava/lang/String;

    sget-object v6, Lio/realm/l;->b:Lio/realm/l;

    invoke-virtual {v0, v1, v5, v6}, Lio/realm/bf;->c(Ljava/lang/String;Ljava/lang/String;Lio/realm/l;)Lio/realm/bf;

    move-result-object v0

    .line 129
    invoke-virtual {v0}, Lio/realm/bf;->c()Lio/realm/bf;

    move-result-object v0

    const-string v1, "transaction.peer.peerName"

    iget-object v5, p0, Lco/uk/getmondo/feed/a/r$j;->a:Ljava/lang/String;

    sget-object v6, Lio/realm/l;->b:Lio/realm/l;

    invoke-virtual {v0, v1, v5, v6}, Lio/realm/bf;->c(Ljava/lang/String;Ljava/lang/String;Lio/realm/l;)Lio/realm/bf;

    move-result-object v0

    .line 130
    invoke-virtual {v0}, Lio/realm/bf;->c()Lio/realm/bf;

    move-result-object v0

    const-string v1, "transaction.peer.contactName"

    iget-object v5, p0, Lco/uk/getmondo/feed/a/r$j;->a:Ljava/lang/String;

    sget-object v6, Lio/realm/l;->b:Lio/realm/l;

    invoke-virtual {v0, v1, v5, v6}, Lio/realm/bf;->c(Ljava/lang/String;Ljava/lang/String;Lio/realm/l;)Lio/realm/bf;

    move-result-object v0

    .line 131
    invoke-virtual {v0}, Lio/realm/bf;->c()Lio/realm/bf;

    move-result-object v0

    const-string v1, "transaction.peer.phoneNumber"

    iget-object v5, p0, Lco/uk/getmondo/feed/a/r$j;->a:Ljava/lang/String;

    sget-object v6, Lio/realm/l;->b:Lio/realm/l;

    invoke-virtual {v0, v1, v5, v6}, Lio/realm/bf;->c(Ljava/lang/String;Ljava/lang/String;Lio/realm/l;)Lio/realm/bf;

    move-result-object v0

    .line 132
    invoke-virtual {v0}, Lio/realm/bf;->c()Lio/realm/bf;

    move-result-object v0

    const-string v1, "transaction.declineReason"

    iget-object v5, p0, Lco/uk/getmondo/feed/a/r$j;->a:Ljava/lang/String;

    sget-object v6, Lio/realm/l;->b:Lio/realm/l;

    invoke-virtual {v0, v1, v5, v6}, Lio/realm/bf;->c(Ljava/lang/String;Ljava/lang/String;Lio/realm/l;)Lio/realm/bf;

    move-result-object v0

    .line 133
    invoke-virtual {v0}, Lio/realm/bf;->c()Lio/realm/bf;

    move-result-object v0

    const-string v1, "transaction.notes"

    iget-object v5, p0, Lco/uk/getmondo/feed/a/r$j;->a:Ljava/lang/String;

    sget-object v6, Lio/realm/l;->b:Lio/realm/l;

    invoke-virtual {v0, v1, v5, v6}, Lio/realm/bf;->c(Ljava/lang/String;Ljava/lang/String;Lio/realm/l;)Lio/realm/bf;

    move-result-object v0

    .line 134
    invoke-virtual {v0}, Lio/realm/bf;->c()Lio/realm/bf;

    move-result-object v0

    const-string v1, "transaction.description"

    iget-object v5, p0, Lco/uk/getmondo/feed/a/r$j;->a:Ljava/lang/String;

    sget-object v6, Lio/realm/l;->b:Lio/realm/l;

    invoke-virtual {v0, v1, v5, v6}, Lio/realm/bf;->c(Ljava/lang/String;Ljava/lang/String;Lio/realm/l;)Lio/realm/bf;

    move-result-object v0

    .line 135
    invoke-virtual {v0}, Lio/realm/bf;->c()Lio/realm/bf;

    move-result-object v0

    const-string v1, "transaction.createdDateFormatted"

    iget-object v5, p0, Lco/uk/getmondo/feed/a/r$j;->a:Ljava/lang/String;

    sget-object v6, Lio/realm/l;->b:Lio/realm/l;

    invoke-virtual {v0, v1, v5, v6}, Lio/realm/bf;->c(Ljava/lang/String;Ljava/lang/String;Lio/realm/l;)Lio/realm/bf;

    move-result-object v0

    .line 136
    invoke-virtual {v0}, Lio/realm/bf;->c()Lio/realm/bf;

    move-result-object v0

    const-string v1, "basicItemInfo.title"

    iget-object v5, p0, Lco/uk/getmondo/feed/a/r$j;->a:Ljava/lang/String;

    sget-object v6, Lio/realm/l;->b:Lio/realm/l;

    invoke-virtual {v0, v1, v5, v6}, Lio/realm/bf;->c(Ljava/lang/String;Ljava/lang/String;Lio/realm/l;)Lio/realm/bf;

    move-result-object v0

    .line 137
    invoke-virtual {v0}, Lio/realm/bf;->c()Lio/realm/bf;

    move-result-object v0

    const-string v1, "basicItemInfo.subtitle"

    iget-object v5, p0, Lco/uk/getmondo/feed/a/r$j;->a:Ljava/lang/String;

    sget-object v6, Lio/realm/l;->b:Lio/realm/l;

    invoke-virtual {v0, v1, v5, v6}, Lio/realm/bf;->c(Ljava/lang/String;Ljava/lang/String;Lio/realm/l;)Lio/realm/bf;

    .line 139
    :cond_0
    iget-object v0, p0, Lco/uk/getmondo/feed/a/r$j;->b:Ljava/util/Set;

    check-cast v0, Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v2

    :goto_1
    if-eqz v0, :cond_5

    .line 140
    invoke-virtual {v4}, Lio/realm/bf;->c()Lio/realm/bf;

    move-result-object v5

    const-string v6, "type"

    iget-object v0, p0, Lco/uk/getmondo/feed/a/r$j;->b:Ljava/util/Set;

    check-cast v0, Ljava/lang/Iterable;

    .line 164
    new-instance v1, Ljava/util/ArrayList;

    invoke-static {v0, v8}, Lkotlin/a/m;->a(Ljava/lang/Iterable;I)I

    move-result v7

    invoke-direct {v1, v7}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v1, Ljava/util/Collection;

    .line 165
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_2
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .line 166
    check-cast v0, Lco/uk/getmondo/feed/a/a/a;

    .line 140
    invoke-virtual {v0}, Lco/uk/getmondo/feed/a/a/a;->a()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_1
    move v0, v3

    .line 125
    goto/16 :goto_0

    :cond_2
    move v0, v3

    .line 139
    goto :goto_1

    .line 167
    :cond_3
    check-cast v1, Ljava/util/List;

    check-cast v1, Ljava/util/Collection;

    .line 169
    invoke-interface {v1}, Ljava/util/Collection;->size()I

    move-result v0

    new-array v0, v0, [Ljava/lang/String;

    invoke-interface {v1, v0}, Ljava/util/Collection;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_4

    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type kotlin.Array<T>"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_4
    check-cast v0, [Ljava/lang/String;

    .line 140
    invoke-virtual {v5, v6, v0}, Lio/realm/bf;->a(Ljava/lang/String;[Ljava/lang/String;)Lio/realm/bf;

    .line 142
    :cond_5
    iget-object v0, p0, Lco/uk/getmondo/feed/a/r$j;->c:Ljava/util/Set;

    check-cast v0, Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_6

    move v0, v2

    :goto_3
    if-eqz v0, :cond_9

    .line 143
    invoke-virtual {v4}, Lio/realm/bf;->c()Lio/realm/bf;

    move-result-object v2

    const-string v3, "transaction.category"

    iget-object v0, p0, Lco/uk/getmondo/feed/a/r$j;->c:Ljava/util/Set;

    check-cast v0, Ljava/lang/Iterable;

    .line 170
    new-instance v1, Ljava/util/ArrayList;

    invoke-static {v0, v8}, Lkotlin/a/m;->a(Ljava/lang/Iterable;I)I

    move-result v5

    invoke-direct {v1, v5}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v1, Ljava/util/Collection;

    .line 171
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_4
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .line 172
    check-cast v0, Lco/uk/getmondo/d/h;

    .line 143
    invoke-virtual {v0}, Lco/uk/getmondo/d/h;->f()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_4

    :cond_6
    move v0, v3

    .line 142
    goto :goto_3

    .line 173
    :cond_7
    check-cast v1, Ljava/util/List;

    check-cast v1, Ljava/util/Collection;

    .line 175
    invoke-interface {v1}, Ljava/util/Collection;->size()I

    move-result v0

    new-array v0, v0, [Ljava/lang/String;

    invoke-interface {v1, v0}, Ljava/util/Collection;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_8

    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type kotlin.Array<T>"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_8
    check-cast v0, [Ljava/lang/String;

    .line 143
    invoke-virtual {v2, v3, v0}, Lio/realm/bf;->a(Ljava/lang/String;[Ljava/lang/String;)Lio/realm/bf;

    .line 145
    :cond_9
    const-string v0, "created"

    sget-object v1, Lio/realm/bl;->b:Lio/realm/bl;

    invoke-virtual {v4, v0, v1}, Lio/realm/bf;->a(Ljava/lang/String;Lio/realm/bl;)Lio/realm/bg;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 20
    check-cast p1, Lio/realm/av;

    invoke-virtual {p0, p1}, Lco/uk/getmondo/feed/a/r$j;->a(Lio/realm/av;)Lio/realm/bg;

    move-result-object v0

    return-object v0
.end method
