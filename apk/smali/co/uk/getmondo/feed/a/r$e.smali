.class final Lco/uk/getmondo/feed/a/r$e;
.super Lkotlin/d/b/m;
.source "FeedStorage.kt"

# interfaces
.implements Lkotlin/d/a/b;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lco/uk/getmondo/feed/a/r;->c()Lio/reactivex/n;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/d/b/m;",
        "Lkotlin/d/a/b",
        "<",
        "Lio/realm/av;",
        "Lio/realm/bg",
        "<",
        "Lco/uk/getmondo/d/m;",
        ">;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000\u0014\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a&\u0012\u000c\u0012\n \u0003*\u0004\u0018\u00010\u00020\u0002 \u0003*\u0012\u0012\u000c\u0012\n \u0003*\u0004\u0018\u00010\u00020\u0002\u0018\u00010\u00010\u00012\u0006\u0010\u0004\u001a\u00020\u0005H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "Lio/realm/RealmResults;",
        "Lco/uk/getmondo/model/FeedItem;",
        "kotlin.jvm.PlatformType",
        "realm",
        "Lio/realm/Realm;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# static fields
.field public static final a:Lco/uk/getmondo/feed/a/r$e;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lco/uk/getmondo/feed/a/r$e;

    invoke-direct {v0}, Lco/uk/getmondo/feed/a/r$e;-><init>()V

    sput-object v0, Lco/uk/getmondo/feed/a/r$e;->a:Lco/uk/getmondo/feed/a/r$e;

    return-void
.end method

.method constructor <init>()V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lkotlin/d/b/m;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final a(Lio/realm/av;)Lio/realm/bg;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/realm/av;",
            ")",
            "Lio/realm/bg",
            "<",
            "Lco/uk/getmondo/d/m;",
            ">;"
        }
    .end annotation

    .prologue
    const-string v0, "realm"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 151
    const-class v0, Lco/uk/getmondo/d/m;

    invoke-virtual {p1, v0}, Lio/realm/av;->a(Ljava/lang/Class;)Lio/realm/bf;

    move-result-object v0

    .line 152
    const-string v1, "type"

    sget-object v2, Lco/uk/getmondo/feed/a/a/a;->b:Lco/uk/getmondo/feed/a/a/a;

    invoke-virtual {v2}, Lco/uk/getmondo/feed/a/a/a;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lio/realm/bf;->a(Ljava/lang/String;Ljava/lang/String;)Lio/realm/bf;

    move-result-object v0

    .line 153
    invoke-virtual {v0}, Lio/realm/bf;->g()Lio/realm/bg;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 20
    check-cast p1, Lio/realm/av;

    invoke-virtual {p0, p1}, Lco/uk/getmondo/feed/a/r$e;->a(Lio/realm/av;)Lio/realm/bg;

    move-result-object v0

    return-object v0
.end method
