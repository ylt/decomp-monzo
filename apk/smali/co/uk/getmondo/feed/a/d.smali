.class public Lco/uk/getmondo/feed/a/d;
.super Ljava/lang/Object;
.source "FeedManager.java"


# instance fields
.field private final a:Lco/uk/getmondo/feed/a/r;

.field private final b:Lco/uk/getmondo/api/MonzoApi;

.field private final c:Lco/uk/getmondo/d/a/f;

.field private final d:Lco/uk/getmondo/payments/send/payment_category/b;

.field private final e:Lco/uk/getmondo/feed/a/a;

.field private final f:Lco/uk/getmondo/feed/search/a;

.field private final g:Lco/uk/getmondo/payments/send/a/e;

.field private final h:Lco/uk/getmondo/common/accounts/b;

.field private final i:Lio/reactivex/u;


# direct methods
.method public constructor <init>(Lco/uk/getmondo/feed/a/r;Lco/uk/getmondo/api/MonzoApi;Lco/uk/getmondo/d/a/f;Lco/uk/getmondo/payments/send/payment_category/b;Lco/uk/getmondo/feed/a/a;Lco/uk/getmondo/feed/search/a;Lco/uk/getmondo/payments/send/a/e;Lco/uk/getmondo/common/accounts/b;Lio/reactivex/u;)V
    .locals 0

    .prologue
    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 51
    iput-object p1, p0, Lco/uk/getmondo/feed/a/d;->a:Lco/uk/getmondo/feed/a/r;

    .line 52
    iput-object p2, p0, Lco/uk/getmondo/feed/a/d;->b:Lco/uk/getmondo/api/MonzoApi;

    .line 53
    iput-object p3, p0, Lco/uk/getmondo/feed/a/d;->c:Lco/uk/getmondo/d/a/f;

    .line 54
    iput-object p4, p0, Lco/uk/getmondo/feed/a/d;->d:Lco/uk/getmondo/payments/send/payment_category/b;

    .line 55
    iput-object p5, p0, Lco/uk/getmondo/feed/a/d;->e:Lco/uk/getmondo/feed/a/a;

    .line 56
    iput-object p6, p0, Lco/uk/getmondo/feed/a/d;->f:Lco/uk/getmondo/feed/search/a;

    .line 57
    iput-object p7, p0, Lco/uk/getmondo/feed/a/d;->g:Lco/uk/getmondo/payments/send/a/e;

    .line 58
    iput-object p8, p0, Lco/uk/getmondo/feed/a/d;->h:Lco/uk/getmondo/common/accounts/b;

    .line 59
    iput-object p9, p0, Lco/uk/getmondo/feed/a/d;->i:Lio/reactivex/u;

    .line 60
    return-void
.end method

.method static synthetic a(Ljava/lang/String;Ljava/util/Date;Ljava/util/List;)Lco/uk/getmondo/feed/a/a/b;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 116
    new-instance v0, Lco/uk/getmondo/feed/a/a/b;

    invoke-direct {v0, p0, p2, p1}, Lco/uk/getmondo/feed/a/a/b;-><init>(Ljava/lang/String;Ljava/util/List;Ljava/util/Date;)V

    return-object v0
.end method

.method static synthetic a(Lco/uk/getmondo/api/model/feed/ApiFeedResponse;)Lio/reactivex/r;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 109
    invoke-virtual {p0}, Lco/uk/getmondo/api/model/feed/ApiFeedResponse;->a()Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lio/reactivex/n;->fromIterable(Ljava/lang/Iterable;)Lio/reactivex/n;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lco/uk/getmondo/feed/a/d;Ljava/lang/String;)Lio/reactivex/r;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 140
    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    .line 141
    iget-object v1, p0, Lco/uk/getmondo/feed/a/d;->e:Lco/uk/getmondo/feed/a/a;

    invoke-virtual {v1, v0}, Lco/uk/getmondo/feed/a/a;->a(Ljava/lang/String;)Ljava/util/Set;

    move-result-object v1

    .line 142
    iget-object v2, p0, Lco/uk/getmondo/feed/a/d;->f:Lco/uk/getmondo/feed/search/a;

    invoke-virtual {v2, v0}, Lco/uk/getmondo/feed/search/a;->a(Ljava/lang/String;)Ljava/util/Set;

    move-result-object v2

    .line 143
    iget-object v3, p0, Lco/uk/getmondo/feed/a/d;->a:Lco/uk/getmondo/feed/a/r;

    invoke-virtual {v3, v0, v1, v2}, Lco/uk/getmondo/feed/a/r;->a(Ljava/lang/String;Ljava/util/Set;Ljava/util/Set;)Lio/reactivex/n;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Ljava/util/Map;Ljava/util/List;)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 84
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/feed/a/a/b;

    .line 85
    invoke-virtual {v0}, Lco/uk/getmondo/feed/a/a/b;->b()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/d/m;

    .line 86
    invoke-virtual {v0}, Lco/uk/getmondo/d/m;->e()Lcom/c/b/b;

    move-result-object v0

    invoke-static {p0}, Lco/uk/getmondo/feed/a/h;->a(Ljava/util/Map;)Lcom/c/a/a;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/c/b/b;->a(Lcom/c/a/a;)Lcom/c/b/b;

    goto :goto_0

    .line 95
    :cond_1
    return-object p1
.end method

.method static synthetic a([Ljava/lang/Object;)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 70
    new-instance v2, Ljava/util/ArrayList;

    array-length v0, p0

    invoke-direct {v2, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 71
    array-length v3, p0

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    aget-object v0, p0, v1

    .line 72
    check-cast v0, Lco/uk/getmondo/feed/a/a/b;

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 71
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 74
    :cond_0
    return-object v2
.end method

.method static synthetic a(Ljava/util/Map;Lco/uk/getmondo/d/aj;)V
    .locals 2

    .prologue
    .line 87
    invoke-virtual {p1}, Lco/uk/getmondo/d/aj;->B()Lco/uk/getmondo/d/aa;

    move-result-object v1

    .line 88
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lco/uk/getmondo/d/aa;->d()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 89
    invoke-virtual {v1}, Lco/uk/getmondo/d/aa;->d()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p0, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/payments/send/a/b;

    .line 90
    invoke-virtual {v1, v0}, Lco/uk/getmondo/d/aa;->a(Lco/uk/getmondo/payments/send/a/b;)Lco/uk/getmondo/d/aa;

    .line 92
    :cond_0
    return-void
.end method

.method static synthetic a(Lco/uk/getmondo/feed/a/d;Lco/uk/getmondo/d/m;)Z
    .locals 1

    invoke-direct {p0, p1}, Lco/uk/getmondo/feed/a/d;->c(Lco/uk/getmondo/d/m;)Z

    move-result v0

    return v0
.end method

.method static synthetic b(Lco/uk/getmondo/feed/a/d;Ljava/lang/String;)Lio/reactivex/z;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 102
    iget-object v1, p0, Lco/uk/getmondo/feed/a/d;->a:Lco/uk/getmondo/feed/a/r;

    invoke-virtual {v1, p1}, Lco/uk/getmondo/feed/a/r;->a(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v1

    .line 103
    if-eqz v1, :cond_1

    .line 104
    invoke-static {v1}, Lco/uk/getmondo/common/c/a;->b(Ljava/util/Date;)Ljava/util/Date;

    move-result-object v1

    invoke-static {}, Lco/uk/getmondo/common/c/a;->a()Ljava/util/Date;

    move-result-object v2

    invoke-static {v1, v2}, Lco/uk/getmondo/common/c/a;->a(Ljava/util/Date;Ljava/util/Date;)Ljava/util/Date;

    move-result-object v1

    .line 106
    :goto_0
    if-eqz v1, :cond_0

    invoke-static {v1}, Lco/uk/getmondo/common/c/a;->c(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    .line 108
    :cond_0
    iget-object v2, p0, Lco/uk/getmondo/feed/a/d;->b:Lco/uk/getmondo/api/MonzoApi;

    invoke-interface {v2, p1, v0}, Lco/uk/getmondo/api/MonzoApi;->feed(Ljava/lang/String;Ljava/lang/String;)Lio/reactivex/v;

    move-result-object v0

    invoke-static {}, Lco/uk/getmondo/feed/a/m;->a()Lio/reactivex/c/h;

    move-result-object v2

    .line 109
    invoke-virtual {v0, v2}, Lio/reactivex/v;->b(Lio/reactivex/c/h;)Lio/reactivex/n;

    move-result-object v0

    iget-object v2, p0, Lco/uk/getmondo/feed/a/d;->d:Lco/uk/getmondo/payments/send/payment_category/b;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    invoke-static {v2}, Lco/uk/getmondo/feed/a/n;->a(Lco/uk/getmondo/payments/send/payment_category/b;)Lio/reactivex/c/h;

    move-result-object v2

    .line 111
    invoke-virtual {v0, v2}, Lio/reactivex/n;->flatMapSingle(Lio/reactivex/c/h;)Lio/reactivex/n;

    move-result-object v0

    iget-object v2, p0, Lco/uk/getmondo/feed/a/d;->c:Lco/uk/getmondo/d/a/f;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    invoke-static {v2}, Lco/uk/getmondo/feed/a/o;->a(Lco/uk/getmondo/d/a/f;)Lio/reactivex/c/h;

    move-result-object v2

    .line 112
    invoke-virtual {v0, v2}, Lio/reactivex/n;->map(Lio/reactivex/c/h;)Lio/reactivex/n;

    move-result-object v0

    invoke-static {p0}, Lco/uk/getmondo/feed/a/p;->a(Lco/uk/getmondo/feed/a/d;)Lio/reactivex/c/q;

    move-result-object v2

    .line 113
    invoke-virtual {v0, v2}, Lio/reactivex/n;->filter(Lio/reactivex/c/q;)Lio/reactivex/n;

    move-result-object v0

    invoke-static {p0}, Lco/uk/getmondo/feed/a/f;->a(Lco/uk/getmondo/feed/a/d;)Lio/reactivex/c/q;

    move-result-object v2

    .line 114
    invoke-virtual {v0, v2}, Lio/reactivex/n;->filter(Lio/reactivex/c/q;)Lio/reactivex/n;

    move-result-object v0

    .line 115
    invoke-virtual {v0}, Lio/reactivex/n;->toList()Lio/reactivex/v;

    move-result-object v0

    invoke-static {p1, v1}, Lco/uk/getmondo/feed/a/g;->a(Ljava/lang/String;Ljava/util/Date;)Lio/reactivex/c/h;

    move-result-object v1

    .line 116
    invoke-virtual {v0, v1}, Lio/reactivex/v;->d(Lio/reactivex/c/h;)Lio/reactivex/v;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/feed/a/d;->i:Lio/reactivex/u;

    .line 117
    invoke-virtual {v0, v1}, Lio/reactivex/v;->b(Lio/reactivex/u;)Lio/reactivex/v;

    move-result-object v0

    .line 108
    return-object v0

    :cond_1
    move-object v1, v0

    .line 104
    goto :goto_0
.end method

.method private b(Lco/uk/getmondo/d/m;)Z
    .locals 2

    .prologue
    .line 122
    iget-object v0, p0, Lco/uk/getmondo/feed/a/d;->h:Lco/uk/getmondo/common/accounts/b;

    invoke-virtual {v0}, Lco/uk/getmondo/common/accounts/b;->b()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, Lco/uk/getmondo/d/m;->j()Lco/uk/getmondo/feed/a/a/a;

    move-result-object v0

    sget-object v1, Lco/uk/getmondo/feed/a/a/a;->l:Lco/uk/getmondo/feed/a/a/a;

    if-eq v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic b(Lco/uk/getmondo/feed/a/d;Lco/uk/getmondo/d/m;)Z
    .locals 1

    invoke-direct {p0, p1}, Lco/uk/getmondo/feed/a/d;->b(Lco/uk/getmondo/d/m;)Z

    move-result v0

    return v0
.end method

.method private c(Lco/uk/getmondo/d/m;)Z
    .locals 2

    .prologue
    .line 127
    invoke-virtual {p1}, Lco/uk/getmondo/d/m;->j()Lco/uk/getmondo/feed/a/a/a;

    move-result-object v0

    sget-object v1, Lco/uk/getmondo/feed/a/a/a;->n:Lco/uk/getmondo/feed/a/a/a;

    if-eq v0, v1, :cond_1

    .line 128
    invoke-virtual {p1}, Lco/uk/getmondo/d/m;->e()Lcom/c/b/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/c/b/b;->b()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, Lco/uk/getmondo/d/m;->j()Lco/uk/getmondo/feed/a/a/a;

    move-result-object v0

    sget-object v1, Lco/uk/getmondo/feed/a/a/a;->a:Lco/uk/getmondo/feed/a/a/a;

    if-eq v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    .line 127
    :goto_0
    return v0

    .line 128
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private d(Ljava/lang/String;)Lio/reactivex/v;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/v",
            "<",
            "Lco/uk/getmondo/feed/a/a/b;",
            ">;"
        }
    .end annotation

    .prologue
    .line 101
    invoke-static {p0, p1}, Lco/uk/getmondo/feed/a/k;->a(Lco/uk/getmondo/feed/a/d;Ljava/lang/String;)Ljava/util/concurrent/Callable;

    move-result-object v0

    invoke-static {v0}, Lio/reactivex/v;->a(Ljava/util/concurrent/Callable;)Lio/reactivex/v;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public a(Lco/uk/getmondo/d/m;)Lio/reactivex/b;
    .locals 1

    .prologue
    .line 161
    iget-object v0, p0, Lco/uk/getmondo/feed/a/d;->a:Lco/uk/getmondo/feed/a/r;

    invoke-virtual {v0, p1}, Lco/uk/getmondo/feed/a/r;->a(Lco/uk/getmondo/d/m;)Lio/reactivex/b;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/util/List;)Lio/reactivex/b;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lio/reactivex/b;"
        }
    .end annotation

    .prologue
    .line 63
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 64
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 65
    invoke-direct {p0, v0}, Lco/uk/getmondo/feed/a/d;->d(Ljava/lang/String;)Lio/reactivex/v;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 68
    :cond_0
    iget-object v0, p0, Lco/uk/getmondo/feed/a/d;->g:Lco/uk/getmondo/payments/send/a/e;

    invoke-virtual {v0}, Lco/uk/getmondo/payments/send/a/e;->a()Lio/reactivex/n;

    move-result-object v0

    invoke-virtual {v0}, Lio/reactivex/n;->firstOrError()Lio/reactivex/v;

    move-result-object v0

    .line 69
    invoke-static {}, Lco/uk/getmondo/feed/a/e;->a()Lio/reactivex/c/h;

    move-result-object v2

    invoke-static {v1, v2}, Lio/reactivex/v;->a(Ljava/lang/Iterable;Lio/reactivex/c/h;)Lio/reactivex/v;

    move-result-object v1

    .line 80
    invoke-static {}, Lco/uk/getmondo/feed/a/i;->a()Lio/reactivex/c/c;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lio/reactivex/v;->a(Lio/reactivex/z;Lio/reactivex/z;Lio/reactivex/c/c;)Lio/reactivex/v;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/feed/a/d;->a:Lco/uk/getmondo/feed/a/r;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    invoke-static {v1}, Lco/uk/getmondo/feed/a/j;->a(Lco/uk/getmondo/feed/a/r;)Lio/reactivex/c/h;

    move-result-object v1

    .line 97
    invoke-virtual {v0, v1}, Lio/reactivex/v;->c(Lio/reactivex/c/h;)Lio/reactivex/b;

    move-result-object v0

    .line 80
    return-object v0
.end method

.method public a()Lio/reactivex/n;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Lco/uk/getmondo/common/b/b",
            "<",
            "Lco/uk/getmondo/d/m;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 135
    iget-object v0, p0, Lco/uk/getmondo/feed/a/d;->a:Lco/uk/getmondo/feed/a/r;

    invoke-virtual {v0}, Lco/uk/getmondo/feed/a/r;->b()Lio/reactivex/n;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/lang/String;)Lio/reactivex/n;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/n",
            "<",
            "Lco/uk/getmondo/common/b/b",
            "<",
            "Lco/uk/getmondo/d/m;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 139
    invoke-static {p0, p1}, Lco/uk/getmondo/feed/a/l;->a(Lco/uk/getmondo/feed/a/d;Ljava/lang/String;)Ljava/util/concurrent/Callable;

    move-result-object v0

    invoke-static {v0}, Lio/reactivex/n;->defer(Ljava/util/concurrent/Callable;)Lio/reactivex/n;

    move-result-object v0

    return-object v0
.end method

.method public b(Ljava/lang/String;)Lio/reactivex/b;
    .locals 2

    .prologue
    .line 152
    iget-object v0, p0, Lco/uk/getmondo/feed/a/d;->a:Lco/uk/getmondo/feed/a/r;

    invoke-virtual {v0, p1}, Lco/uk/getmondo/feed/a/r;->b(Ljava/lang/String;)Lio/reactivex/b;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/feed/a/d;->b:Lco/uk/getmondo/api/MonzoApi;

    .line 153
    invoke-interface {v1, p1}, Lco/uk/getmondo/api/MonzoApi;->deleteFeedItem(Ljava/lang/String;)Lio/reactivex/b;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/b;->b(Lio/reactivex/d;)Lio/reactivex/b;

    move-result-object v0

    .line 152
    return-object v0
.end method

.method public b()Lio/reactivex/n;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Lco/uk/getmondo/d/m;",
            ">;"
        }
    .end annotation

    .prologue
    .line 148
    iget-object v0, p0, Lco/uk/getmondo/feed/a/d;->a:Lco/uk/getmondo/feed/a/r;

    invoke-virtual {v0}, Lco/uk/getmondo/feed/a/r;->c()Lio/reactivex/n;

    move-result-object v0

    return-object v0
.end method

.method public c()Lco/uk/getmondo/d/n;
    .locals 1

    .prologue
    .line 166
    iget-object v0, p0, Lco/uk/getmondo/feed/a/d;->a:Lco/uk/getmondo/feed/a/r;

    invoke-virtual {v0}, Lco/uk/getmondo/feed/a/r;->a()Lco/uk/getmondo/d/n;

    move-result-object v0

    return-object v0
.end method

.method public c(Ljava/lang/String;)Lio/reactivex/b;
    .locals 1

    .prologue
    .line 157
    iget-object v0, p0, Lco/uk/getmondo/feed/a/d;->a:Lco/uk/getmondo/feed/a/r;

    invoke-virtual {v0, p1}, Lco/uk/getmondo/feed/a/r;->b(Ljava/lang/String;)Lio/reactivex/b;

    move-result-object v0

    return-object v0
.end method
