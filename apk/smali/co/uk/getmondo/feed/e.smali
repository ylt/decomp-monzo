.class public final Lco/uk/getmondo/feed/e;
.super Lco/uk/getmondo/common/f/a;
.source "HomeFeedFragment.kt"

# interfaces
.implements Lco/uk/getmondo/feed/adapter/a$a;
.implements Lco/uk/getmondo/feed/g$a;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000\u009e\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0006\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\t\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0007\n\u0002\u0010 \n\u0002\u0008\u0003\u0018\u00002\u00020\u00012\u00020\u00022\u00020\u0003B\u0005\u00a2\u0006\u0002\u0010\u0004J\u0008\u0010\u001f\u001a\u00020 H\u0016J\u0008\u0010!\u001a\u00020 H\u0016J\u0012\u0010\"\u001a\u00020 2\u0008\u0010#\u001a\u0004\u0018\u00010$H\u0016J\u001a\u0010%\u001a\u00020 2\u0008\u0010&\u001a\u0004\u0018\u00010\'2\u0006\u0010(\u001a\u00020)H\u0016J$\u0010*\u001a\u00020+2\u0006\u0010(\u001a\u00020,2\u0008\u0010-\u001a\u0004\u0018\u00010.2\u0008\u0010#\u001a\u0004\u0018\u00010$H\u0016J\u000e\u0010/\u001a\u0008\u0012\u0004\u0012\u00020 00H\u0016J\u0008\u00101\u001a\u00020 H\u0016J\u0010\u00102\u001a\u00020 2\u0006\u00103\u001a\u00020\u000fH\u0016J\u0010\u00104\u001a\u00020 2\u0006\u00103\u001a\u00020\u000fH\u0016J\u000e\u00105\u001a\u0008\u0012\u0004\u0012\u00020\u000f00H\u0016J\u0010\u00106\u001a\u0002072\u0006\u00108\u001a\u000209H\u0016J\u000e\u0010:\u001a\u0008\u0012\u0004\u0012\u00020 00H\u0016J\u0012\u0010;\u001a\u00020 2\u0008\u0010<\u001a\u0004\u0018\u00010$H\u0016J\u000e\u0010=\u001a\u0008\u0012\u0004\u0012\u00020\u001e00H\u0016J\u001a\u0010>\u001a\u00020 2\u0006\u0010?\u001a\u00020+2\u0008\u0010#\u001a\u0004\u0018\u00010$H\u0016J\u0008\u0010@\u001a\u00020 H\u0016J\u0010\u0010A\u001a\u00020 2\u0006\u0010B\u001a\u00020CH\u0016J\u0016\u0010D\u001a\u00020 2\u000c\u0010E\u001a\u0008\u0012\u0004\u0012\u00020\u000f0FH\u0016J\u0010\u0010G\u001a\u00020 2\u0006\u0010H\u001a\u000207H\u0016J\u0010\u0010I\u001a\u00020 2\u0006\u0010J\u001a\u00020CH\u0016J\u0008\u0010K\u001a\u00020 H\u0016J\u0016\u0010L\u001a\u00020 2\u000c\u0010M\u001a\u0008\u0012\u0004\u0012\u00020\u00060NH\u0016J\u0008\u0010O\u001a\u00020 H\u0016J\u0008\u0010P\u001a\u00020 H\u0016R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082D\u00a2\u0006\u0002\n\u0000R\u001e\u0010\u0007\u001a\u00020\u00088\u0006@\u0006X\u0087.\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\t\u0010\n\"\u0004\u0008\u000b\u0010\u000cR2\u0010\r\u001a&\u0012\u000c\u0012\n \u0010*\u0004\u0018\u00010\u000f0\u000f \u0010*\u0012\u0012\u000c\u0012\n \u0010*\u0004\u0018\u00010\u000f0\u000f\u0018\u00010\u000e0\u000eX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001e\u0010\u0011\u001a\u00020\u00128\u0006@\u0006X\u0087.\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u0013\u0010\u0014\"\u0004\u0008\u0015\u0010\u0016R\u001e\u0010\u0017\u001a\u00020\u00188\u0006@\u0006X\u0087.\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u0019\u0010\u001a\"\u0004\u0008\u001b\u0010\u001cR2\u0010\u001d\u001a&\u0012\u000c\u0012\n \u0010*\u0004\u0018\u00010\u001e0\u001e \u0010*\u0012\u0012\u000c\u0012\n \u0010*\u0004\u0018\u00010\u001e0\u001e\u0018\u00010\u000e0\u000eX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006Q"
    }
    d2 = {
        "Lco/uk/getmondo/feed/HomeFeedFragment;",
        "Lco/uk/getmondo/common/fragments/BaseFragment;",
        "Lco/uk/getmondo/feed/HomeFeedPresenter$View;",
        "Lco/uk/getmondo/feed/adapter/FeedAdapter$OnFeedItemActionListener;",
        "()V",
        "FEED_STATE_KEY",
        "",
        "feedAdapter",
        "Lco/uk/getmondo/feed/adapter/FeedAdapter;",
        "getFeedAdapter",
        "()Lco/uk/getmondo/feed/adapter/FeedAdapter;",
        "setFeedAdapter",
        "(Lco/uk/getmondo/feed/adapter/FeedAdapter;)V",
        "feedItemDeletionRelay",
        "Lcom/jakewharton/rxrelay2/PublishRelay;",
        "Lco/uk/getmondo/model/FeedItem;",
        "kotlin.jvm.PlatformType",
        "feedItemNavigator",
        "Lco/uk/getmondo/feed/FeedItemNavigator;",
        "getFeedItemNavigator",
        "()Lco/uk/getmondo/feed/FeedItemNavigator;",
        "setFeedItemNavigator",
        "(Lco/uk/getmondo/feed/FeedItemNavigator;)V",
        "homeFeedPresenter",
        "Lco/uk/getmondo/feed/HomeFeedPresenter;",
        "getHomeFeedPresenter",
        "()Lco/uk/getmondo/feed/HomeFeedPresenter;",
        "setHomeFeedPresenter",
        "(Lco/uk/getmondo/feed/HomeFeedPresenter;)V",
        "searchRelay",
        "",
        "hideForeignCurrencyAmounts",
        "",
        "hideProgressIndicator",
        "onCreate",
        "savedInstanceState",
        "Landroid/os/Bundle;",
        "onCreateOptionsMenu",
        "menu",
        "Landroid/view/Menu;",
        "inflater",
        "Landroid/view/MenuInflater;",
        "onCreateView",
        "Landroid/view/View;",
        "Landroid/view/LayoutInflater;",
        "container",
        "Landroid/view/ViewGroup;",
        "onDateChanged",
        "Lio/reactivex/Observable;",
        "onDestroyView",
        "onFeedItemClicked",
        "feedItem",
        "onFeedItemDeleteRequested",
        "onFeedItemDeleted",
        "onOptionsItemSelected",
        "",
        "item",
        "Landroid/view/MenuItem;",
        "onRefreshAction",
        "onSaveInstanceState",
        "outState",
        "onSearch",
        "onViewCreated",
        "view",
        "openSearch",
        "setBalance",
        "balance",
        "Lco/uk/getmondo/model/Amount;",
        "setFeedItems",
        "feedItems",
        "Lco/uk/getmondo/common/data/QueryResults;",
        "setRefreshing",
        "refreshing",
        "setSpentToday",
        "spentToday",
        "showCardFrozen",
        "showForeignCurrencyAmounts",
        "amounts",
        "",
        "showProgressIndicator",
        "showTitle",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field public a:Lco/uk/getmondo/feed/adapter/a;

.field public c:Lco/uk/getmondo/feed/g;

.field public d:Lco/uk/getmondo/feed/c;

.field private final e:Ljava/lang/String;

.field private final f:Lcom/b/b/c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/b/b/c",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private final g:Lcom/b/b/c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/b/b/c",
            "<",
            "Lco/uk/getmondo/d/m;",
            ">;"
        }
    .end annotation
.end field

.field private h:Ljava/util/HashMap;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 30
    invoke-direct {p0}, Lco/uk/getmondo/common/f/a;-><init>()V

    .line 31
    const-string v0, "FEED_STATE_KEY"

    iput-object v0, p0, Lco/uk/getmondo/feed/e;->e:Ljava/lang/String;

    .line 33
    invoke-static {}, Lcom/b/b/c;->a()Lcom/b/b/c;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/feed/e;->f:Lcom/b/b/c;

    .line 34
    invoke-static {}, Lcom/b/b/c;->a()Lcom/b/b/c;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/feed/e;->g:Lcom/b/b/c;

    return-void
.end method


# virtual methods
.method public a(I)Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lco/uk/getmondo/feed/e;->h:Ljava/util/HashMap;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lco/uk/getmondo/feed/e;->h:Ljava/util/HashMap;

    :cond_0
    iget-object v0, p0, Lco/uk/getmondo/feed/e;->h:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    if-nez v0, :cond_1

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getView()Landroid/view/View;

    move-result-object v0

    if-nez v0, :cond_2

    const/4 v0, 0x0

    :cond_1
    :goto_0
    return-object v0

    :cond_2
    invoke-virtual {v0, p1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/feed/e;->h:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method public a()Lio/reactivex/n;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Lkotlin/n;",
            ">;"
        }
    .end annotation

    .prologue
    .line 115
    new-instance v0, Lco/uk/getmondo/feed/e$b;

    invoke-direct {v0, p0}, Lco/uk/getmondo/feed/e$b;-><init>(Lco/uk/getmondo/feed/e;)V

    check-cast v0, Lio/reactivex/p;

    invoke-static {v0}, Lio/reactivex/n;->create(Lio/reactivex/p;)Lio/reactivex/n;

    move-result-object v0

    const-string v1, "Observable.create { emit\u2026istener(null) }\n        }"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public a(Lco/uk/getmondo/common/b/b;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lco/uk/getmondo/common/b/b",
            "<+",
            "Lco/uk/getmondo/d/m;",
            ">;)V"
        }
    .end annotation

    .prologue
    const-string v0, "feedItems"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 138
    iget-object v0, p0, Lco/uk/getmondo/feed/e;->a:Lco/uk/getmondo/feed/adapter/a;

    if-nez v0, :cond_0

    const-string v1, "feedAdapter"

    invoke-static {v1}, Lkotlin/d/b/l;->b(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0, p1}, Lco/uk/getmondo/feed/adapter/a;->a(Lco/uk/getmondo/common/b/b;)V

    .line 139
    return-void
.end method

.method public a(Lco/uk/getmondo/d/c;)V
    .locals 1

    .prologue
    const-string v0, "balance"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 182
    sget v0, Lco/uk/getmondo/c$a;->balanceAmount:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/feed/e;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/common/ui/AmountView;

    invoke-virtual {v0, p1}, Lco/uk/getmondo/common/ui/AmountView;->setAmount(Lco/uk/getmondo/d/c;)V

    .line 183
    return-void
.end method

.method public a(Lco/uk/getmondo/d/m;)V
    .locals 2

    .prologue
    const-string v0, "feedItem"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 150
    iget-object v0, p0, Lco/uk/getmondo/feed/e;->d:Lco/uk/getmondo/feed/c;

    if-nez v0, :cond_0

    const-string v1, "feedItemNavigator"

    invoke-static {v1}, Lkotlin/d/b/l;->b(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0, p1}, Lco/uk/getmondo/feed/c;->a(Lco/uk/getmondo/d/m;)V

    .line 151
    return-void
.end method

.method public a(Ljava/util/List;)V
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    const/4 v2, 0x0

    const-string v0, "amounts"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 173
    sget v0, Lco/uk/getmondo/c$a;->spentTodayLabel:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/feed/e;->a(I)Landroid/view/View;

    move-result-object v0

    move-object v9, v0

    check-cast v9, Landroid/widget/TextView;

    const v10, 0x7f0a0266

    const/4 v0, 0x1

    new-array v11, v0, [Ljava/lang/Object;

    move-object v0, p1

    check-cast v0, Ljava/lang/Iterable;

    const-string v1, " + "

    check-cast v1, Ljava/lang/CharSequence;

    const/16 v7, 0x3e

    move-object v3, v2

    move-object v5, v2

    move-object v6, v2

    move-object v8, v2

    invoke-static/range {v0 .. v8}, Lkotlin/a/m;->a(Ljava/lang/Iterable;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;ILjava/lang/CharSequence;Lkotlin/d/a/b;ILjava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v11, v4

    invoke-virtual {p0, v10, v11}, Lco/uk/getmondo/feed/e;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-virtual {v9, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 174
    sget v0, Lco/uk/getmondo/c$a;->spendTodayBalanceView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/feed/e;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 175
    return-void
.end method

.method public a(Z)V
    .locals 2

    .prologue
    .line 130
    sget v0, Lco/uk/getmondo/c$a;->feedRecyclerView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/feed/e;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView;

    new-instance v1, Lco/uk/getmondo/feed/e$d;

    invoke-direct {v1, p0, p1}, Lco/uk/getmondo/feed/e$d;-><init>(Lco/uk/getmondo/feed/e;Z)V

    check-cast v1, Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->post(Ljava/lang/Runnable;)Z

    .line 135
    return-void
.end method

.method public b()Lio/reactivex/n;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 121
    iget-object v0, p0, Lco/uk/getmondo/feed/e;->f:Lcom/b/b/c;

    const-string v1, "searchRelay"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lio/reactivex/n;

    return-object v0
.end method

.method public b(Lco/uk/getmondo/d/c;)V
    .locals 1

    .prologue
    const-string v0, "spentToday"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 186
    sget v0, Lco/uk/getmondo/c$a;->spentTodayAmount:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/feed/e;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/common/ui/AmountView;

    invoke-virtual {v0, p1}, Lco/uk/getmondo/common/ui/AmountView;->setAmount(Lco/uk/getmondo/d/c;)V

    .line 187
    return-void
.end method

.method public b(Lco/uk/getmondo/d/m;)V
    .locals 1

    .prologue
    const-string v0, "feedItem"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 154
    iget-object v0, p0, Lco/uk/getmondo/feed/e;->g:Lcom/b/b/c;

    invoke-virtual {v0, p1}, Lcom/b/b/c;->a(Ljava/lang/Object;)V

    .line 155
    return-void
.end method

.method public c()Lio/reactivex/n;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Lco/uk/getmondo/d/m;",
            ">;"
        }
    .end annotation

    .prologue
    .line 123
    iget-object v0, p0, Lco/uk/getmondo/feed/e;->g:Lcom/b/b/c;

    const-string v1, "feedItemDeletionRelay"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lio/reactivex/n;

    return-object v0
.end method

.method public d()V
    .locals 3

    .prologue
    .line 126
    new-instance v1, Landroid/content/Intent;

    invoke-virtual {p0}, Lco/uk/getmondo/feed/e;->getActivity()Landroid/support/v4/app/j;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    const-class v2, Lco/uk/getmondo/feed/search/FeedSearchActivity;

    invoke-direct {v1, v0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v1}, Lco/uk/getmondo/feed/e;->startActivity(Landroid/content/Intent;)V

    .line 127
    return-void
.end method

.method public e()V
    .locals 1

    .prologue
    .line 142
    sget v0, Lco/uk/getmondo/c$a;->progress:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/feed/e;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    invoke-static {v0}, Lco/uk/getmondo/common/ae;->a(Landroid/view/View;)V

    .line 143
    return-void
.end method

.method public f()V
    .locals 1

    .prologue
    .line 146
    sget v0, Lco/uk/getmondo/c$a;->progress:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/feed/e;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    invoke-static {v0}, Lco/uk/getmondo/common/ae;->b(Landroid/view/View;)V

    .line 147
    return-void
.end method

.method public g()V
    .locals 3

    .prologue
    .line 158
    sget v0, Lco/uk/getmondo/c$a;->toolbar:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/feed/e;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/Toolbar;

    const v1, 0x7f0a00dc

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/Toolbar;->setTitle(I)V

    .line 159
    sget v0, Lco/uk/getmondo/c$a;->toolbar:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/feed/e;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/Toolbar;

    invoke-virtual {p0}, Lco/uk/getmondo/feed/e;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0f00ea

    invoke-static {v1, v2}, Landroid/support/v4/content/a;->c(Landroid/content/Context;I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/Toolbar;->setTitleTextColor(I)V

    .line 160
    return-void
.end method

.method public h()V
    .locals 3

    .prologue
    .line 163
    sget v0, Lco/uk/getmondo/c$a;->toolbar:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/feed/e;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/Toolbar;

    const v1, 0x7f0a02b1

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/Toolbar;->setTitle(I)V

    .line 164
    sget v0, Lco/uk/getmondo/c$a;->toolbar:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/feed/e;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/Toolbar;

    invoke-virtual {p0}, Lco/uk/getmondo/feed/e;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0f0079

    invoke-static {v1, v2}, Landroid/support/v4/content/a;->c(Landroid/content/Context;I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/Toolbar;->setTitleTextColor(I)V

    .line 165
    return-void
.end method

.method public i()Lio/reactivex/n;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Lkotlin/n;",
            ">;"
        }
    .end annotation

    .prologue
    .line 168
    invoke-virtual {p0}, Lco/uk/getmondo/feed/e;->getContext()Landroid/content/Context;

    move-result-object v0

    new-instance v1, Landroid/content/IntentFilter;

    const-string v2, "android.intent.action.DATE_CHANGED"

    invoke-direct {v1, v2}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-static {v0, v1}, Lco/uk/getmondo/common/j/b;->a(Landroid/content/Context;Landroid/content/IntentFilter;)Lio/reactivex/n;

    move-result-object v1

    .line 169
    sget-object v0, Lco/uk/getmondo/feed/e$a;->a:Lco/uk/getmondo/feed/e$a;

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {v1, v0}, Lio/reactivex/n;->map(Lio/reactivex/c/h;)Lio/reactivex/n;

    move-result-object v0

    const-string v1, "RxBroadcastReceiver.crea\u2026\n                .map { }"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public j()V
    .locals 2

    .prologue
    .line 178
    sget v0, Lco/uk/getmondo/c$a;->spentTodayLabel:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/feed/e;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const v1, 0x7f0a03b5

    invoke-virtual {p0, v1}, Lco/uk/getmondo/feed/e;->getString(I)Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 179
    return-void
.end method

.method public k()V
    .locals 1

    iget-object v0, p0, Lco/uk/getmondo/feed/e;->h:Ljava/util/HashMap;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lco/uk/getmondo/feed/e;->h:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    :cond_0
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 41
    invoke-super {p0, p1}, Lco/uk/getmondo/common/f/a;->onCreate(Landroid/os/Bundle;)V

    .line 42
    invoke-virtual {p0}, Lco/uk/getmondo/feed/e;->B()Lco/uk/getmondo/common/h/b/b;

    move-result-object v0

    invoke-interface {v0, p0}, Lco/uk/getmondo/common/h/b/b;->a(Lco/uk/getmondo/feed/e;)V

    .line 43
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V
    .locals 1

    .prologue
    const-string v0, "inflater"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 82
    invoke-super {p0, p1, p2}, Lco/uk/getmondo/common/f/a;->onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V

    .line 83
    const v0, 0x7f130002

    invoke-virtual {p2, v0, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 84
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2

    .prologue
    const-string v0, "inflater"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 46
    const v0, 0x7f05009e

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    const-string v1, "inflater.inflate(R.layou\u2026t_feed, container, false)"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public onDestroyView()V
    .locals 2

    .prologue
    .line 98
    sget v0, Lco/uk/getmondo/c$a;->feedRecyclerView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/feed/e;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView;

    const/4 v1, 0x0

    check-cast v1, Landroid/support/v7/widget/RecyclerView$a;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setAdapter(Landroid/support/v7/widget/RecyclerView$a;)V

    .line 100
    iget-object v0, p0, Lco/uk/getmondo/feed/e;->c:Lco/uk/getmondo/feed/g;

    if-nez v0, :cond_0

    const-string v1, "homeFeedPresenter"

    invoke-static {v1}, Lkotlin/d/b/l;->b(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0}, Lco/uk/getmondo/feed/g;->b()V

    .line 103
    sget v0, Lco/uk/getmondo/c$a;->feedSwipeToRefreshLayout:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/feed/e;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v4/widget/SwipeRefreshLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/SwipeRefreshLayout;->setRefreshing(Z)V

    .line 104
    sget v0, Lco/uk/getmondo/c$a;->feedSwipeToRefreshLayout:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/feed/e;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v4/widget/SwipeRefreshLayout;

    invoke-virtual {v0}, Landroid/support/v4/widget/SwipeRefreshLayout;->destroyDrawingCache()V

    .line 105
    sget v0, Lco/uk/getmondo/c$a;->feedSwipeToRefreshLayout:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/feed/e;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v4/widget/SwipeRefreshLayout;

    invoke-virtual {v0}, Landroid/support/v4/widget/SwipeRefreshLayout;->clearAnimation()V

    .line 106
    invoke-super {p0}, Lco/uk/getmondo/common/f/a;->onDestroyView()V

    .line 107
    invoke-virtual {p0}, Lco/uk/getmondo/feed/e;->k()V

    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2

    .prologue
    const-string v0, "item"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 87
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    .line 88
    const v1, 0x7f1104d0

    if-ne v0, v1, :cond_0

    .line 89
    iget-object v0, p0, Lco/uk/getmondo/feed/e;->f:Lcom/b/b/c;

    sget-object v1, Lco/uk/getmondo/common/b/a;->a:Lco/uk/getmondo/common/b/a;

    invoke-virtual {v0, v1}, Lcom/b/b/c;->a(Ljava/lang/Object;)V

    .line 90
    const/4 v0, 0x1

    .line 92
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Lco/uk/getmondo/common/f/a;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    goto :goto_0
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 110
    invoke-super {p0, p1}, Lco/uk/getmondo/common/f/a;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 111
    if-nez p1, :cond_0

    invoke-static {}, Lkotlin/d/b/l;->a()V

    :cond_0
    iget-object v1, p0, Lco/uk/getmondo/feed/e;->e:Ljava/lang/String;

    sget v0, Lco/uk/getmondo/c$a;->feedRecyclerView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/feed/e;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView;->getLayoutManager()Landroid/support/v7/widget/RecyclerView$h;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView$h;->e()Landroid/os/Parcelable;

    move-result-object v0

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 112
    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x1

    const-string v0, "view"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 49
    invoke-super {p0, p1, p2}, Lco/uk/getmondo/common/f/a;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 50
    invoke-virtual {p0, v6}, Lco/uk/getmondo/feed/e;->setHasOptionsMenu(Z)V

    .line 52
    new-instance v2, Landroid/support/v7/widget/LinearLayoutManager;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {v2, v0, v6, v7}, Landroid/support/v7/widget/LinearLayoutManager;-><init>(Landroid/content/Context;IZ)V

    .line 53
    sget v0, Lco/uk/getmondo/c$a;->feedRecyclerView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/feed/e;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView;

    move-object v1, v2

    check-cast v1, Landroid/support/v7/widget/RecyclerView$h;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(Landroid/support/v7/widget/RecyclerView$h;)V

    .line 54
    iget-object v1, p0, Lco/uk/getmondo/feed/e;->a:Lco/uk/getmondo/feed/adapter/a;

    if-nez v1, :cond_0

    const-string v0, "feedAdapter"

    invoke-static {v0}, Lkotlin/d/b/l;->b(Ljava/lang/String;)V

    :cond_0
    new-instance v0, Lco/uk/getmondo/feed/e$c;

    invoke-direct {v0, v2}, Lco/uk/getmondo/feed/e$c;-><init>(Landroid/support/v7/widget/LinearLayoutManager;)V

    check-cast v0, Landroid/support/v7/widget/RecyclerView$c;

    invoke-virtual {v1, v0}, Lco/uk/getmondo/feed/adapter/a;->registerAdapterDataObserver(Landroid/support/v7/widget/RecyclerView$c;)V

    .line 60
    iget-object v1, p0, Lco/uk/getmondo/feed/e;->a:Lco/uk/getmondo/feed/adapter/a;

    if-nez v1, :cond_1

    const-string v0, "feedAdapter"

    invoke-static {v0}, Lkotlin/d/b/l;->b(Ljava/lang/String;)V

    :cond_1
    move-object v0, p0

    check-cast v0, Lco/uk/getmondo/feed/adapter/a$a;

    invoke-virtual {v1, v0}, Lco/uk/getmondo/feed/adapter/a;->a(Lco/uk/getmondo/feed/adapter/a$a;)V

    .line 61
    sget v0, Lco/uk/getmondo/c$a;->feedRecyclerView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/feed/e;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView;

    iget-object v1, p0, Lco/uk/getmondo/feed/e;->a:Lco/uk/getmondo/feed/adapter/a;

    if-nez v1, :cond_2

    const-string v2, "feedAdapter"

    invoke-static {v2}, Lkotlin/d/b/l;->b(Ljava/lang/String;)V

    :cond_2
    check-cast v1, Landroid/support/v7/widget/RecyclerView$a;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setAdapter(Landroid/support/v7/widget/RecyclerView$a;)V

    .line 62
    sget v0, Lco/uk/getmondo/c$a;->feedRecyclerView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/feed/e;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0, v6}, Landroid/support/v7/widget/RecyclerView;->setHasFixedSize(Z)V

    .line 63
    invoke-virtual {p0}, Lco/uk/getmondo/feed/e;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0119

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    .line 64
    sget v0, Lco/uk/getmondo/c$a;->feedRecyclerView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/feed/e;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView;

    new-instance v2, Lco/uk/getmondo/common/ui/h;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v4

    iget-object v1, p0, Lco/uk/getmondo/feed/e;->a:Lco/uk/getmondo/feed/adapter/a;

    if-nez v1, :cond_3

    const-string v5, "feedAdapter"

    invoke-static {v5}, Lkotlin/d/b/l;->b(Ljava/lang/String;)V

    :cond_3
    check-cast v1, La/a/a/a/a/a;

    invoke-direct {v2, v4, v1, v3}, Lco/uk/getmondo/common/ui/h;-><init>(Landroid/content/Context;La/a/a/a/a/a;I)V

    move-object v1, v2

    check-cast v1, Landroid/support/v7/widget/RecyclerView$g;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->a(Landroid/support/v7/widget/RecyclerView$g;)V

    .line 65
    sget v0, Lco/uk/getmondo/c$a;->feedRecyclerView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/feed/e;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView;

    new-instance v2, La/a/a/a/a/b;

    iget-object v1, p0, Lco/uk/getmondo/feed/e;->a:Lco/uk/getmondo/feed/adapter/a;

    if-nez v1, :cond_4

    const-string v3, "feedAdapter"

    invoke-static {v3}, Lkotlin/d/b/l;->b(Ljava/lang/String;)V

    :cond_4
    check-cast v1, La/a/a/a/a/a;

    invoke-direct {v2, v1}, La/a/a/a/a/b;-><init>(La/a/a/a/a/a;)V

    move-object v1, v2

    check-cast v1, Landroid/support/v7/widget/RecyclerView$g;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->a(Landroid/support/v7/widget/RecyclerView$g;)V

    .line 67
    sget v0, Lco/uk/getmondo/c$a;->feedSwipeToRefreshLayout:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/feed/e;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v4/widget/SwipeRefreshLayout;

    new-array v1, v6, [I

    const v2, 0x7f0f0056

    aput v2, v1, v7

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/SwipeRefreshLayout;->setColorSchemeResources([I)V

    .line 69
    if-eqz p2, :cond_5

    .line 70
    iget-object v0, p0, Lco/uk/getmondo/feed/e;->e:Ljava/lang/String;

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    .line 71
    sget v0, Lco/uk/getmondo/c$a;->feedRecyclerView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/feed/e;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView;->getLayoutManager()Landroid/support/v7/widget/RecyclerView$h;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView$h;->a(Landroid/os/Parcelable;)V

    .line 74
    :cond_5
    invoke-virtual {p0}, Lco/uk/getmondo/feed/e;->getActivity()Landroid/support/v4/app/j;

    move-result-object v0

    if-nez v0, :cond_6

    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type co.uk.getmondo.main.HomeActivity"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_6
    check-cast v0, Lco/uk/getmondo/main/HomeActivity;

    .line 75
    sget v1, Lco/uk/getmondo/c$a;->toolbar:I

    invoke-virtual {p0, v1}, Lco/uk/getmondo/feed/e;->a(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/support/v7/widget/Toolbar;

    invoke-virtual {v0, v1}, Lco/uk/getmondo/main/HomeActivity;->setSupportActionBar(Landroid/support/v7/widget/Toolbar;)V

    .line 76
    sget v1, Lco/uk/getmondo/c$a;->toolbar:I

    invoke-virtual {p0, v1}, Lco/uk/getmondo/feed/e;->a(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/support/v7/widget/Toolbar;

    const-string v2, "toolbar"

    invoke-static {v1, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lco/uk/getmondo/main/HomeActivity;->a(Landroid/support/v7/widget/Toolbar;)V

    .line 78
    iget-object v0, p0, Lco/uk/getmondo/feed/e;->c:Lco/uk/getmondo/feed/g;

    if-nez v0, :cond_7

    const-string v1, "homeFeedPresenter"

    invoke-static {v1}, Lkotlin/d/b/l;->b(Ljava/lang/String;)V

    :cond_7
    check-cast p0, Lco/uk/getmondo/feed/g$a;

    invoke-virtual {v0, p0}, Lco/uk/getmondo/feed/g;->a(Lco/uk/getmondo/feed/g$a;)V

    .line 79
    return-void
.end method
