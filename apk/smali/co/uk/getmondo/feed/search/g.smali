.class Lco/uk/getmondo/feed/search/g;
.super Lco/uk/getmondo/common/ui/b;
.source "FeedSearchPresenter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lco/uk/getmondo/feed/search/g$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lco/uk/getmondo/common/ui/b",
        "<",
        "Lco/uk/getmondo/feed/search/g$a;",
        ">;"
    }
.end annotation


# instance fields
.field private final c:Lio/reactivex/u;

.field private final d:Lio/reactivex/u;

.field private final e:Lco/uk/getmondo/common/e/a;

.field private final f:Lco/uk/getmondo/feed/a/d;

.field private final g:Lco/uk/getmondo/common/a;


# direct methods
.method constructor <init>(Lio/reactivex/u;Lio/reactivex/u;Lco/uk/getmondo/common/e/a;Lco/uk/getmondo/feed/a/d;Lco/uk/getmondo/common/a;)V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0}, Lco/uk/getmondo/common/ui/b;-><init>()V

    .line 33
    iput-object p1, p0, Lco/uk/getmondo/feed/search/g;->c:Lio/reactivex/u;

    .line 34
    iput-object p2, p0, Lco/uk/getmondo/feed/search/g;->d:Lio/reactivex/u;

    .line 35
    iput-object p3, p0, Lco/uk/getmondo/feed/search/g;->e:Lco/uk/getmondo/common/e/a;

    .line 36
    iput-object p4, p0, Lco/uk/getmondo/feed/search/g;->f:Lco/uk/getmondo/feed/a/d;

    .line 37
    iput-object p5, p0, Lco/uk/getmondo/feed/search/g;->g:Lco/uk/getmondo/common/a;

    .line 38
    return-void
.end method

.method static synthetic a(Lco/uk/getmondo/feed/search/g;Lco/uk/getmondo/feed/search/g$a;Lco/uk/getmondo/d/m;)Lio/reactivex/d;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 58
    iget-object v0, p0, Lco/uk/getmondo/feed/search/g;->f:Lco/uk/getmondo/feed/a/d;

    invoke-virtual {p2}, Lco/uk/getmondo/d/m;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lco/uk/getmondo/feed/a/d;->b(Ljava/lang/String;)Lio/reactivex/b;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/feed/search/g;->d:Lio/reactivex/u;

    .line 59
    invoke-virtual {v0, v1}, Lio/reactivex/b;->b(Lio/reactivex/u;)Lio/reactivex/b;

    move-result-object v0

    invoke-static {p0, p1}, Lco/uk/getmondo/feed/search/m;->a(Lco/uk/getmondo/feed/search/g;Lco/uk/getmondo/feed/search/g$a;)Lio/reactivex/c/g;

    move-result-object v1

    .line 60
    invoke-virtual {v0, v1}, Lio/reactivex/b;->a(Lio/reactivex/c/g;)Lio/reactivex/b;

    move-result-object v0

    .line 61
    invoke-virtual {v0}, Lio/reactivex/b;->b()Lio/reactivex/b;

    move-result-object v0

    .line 58
    return-object v0
.end method

.method static synthetic a(Lco/uk/getmondo/feed/search/g$a;Lco/uk/getmondo/common/b/b;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 49
    invoke-virtual {p1}, Lco/uk/getmondo/common/b/b;->a()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 50
    invoke-interface {p0}, Lco/uk/getmondo/feed/search/g$a;->c()V

    .line 55
    :goto_0
    return-void

    .line 52
    :cond_0
    invoke-interface {p0}, Lco/uk/getmondo/feed/search/g$a;->d()V

    .line 53
    invoke-interface {p0, p1}, Lco/uk/getmondo/feed/search/g$a;->a(Lco/uk/getmondo/common/b/b;)V

    goto :goto_0
.end method

.method static synthetic a(Lco/uk/getmondo/feed/search/g;Lco/uk/getmondo/feed/search/g$a;Ljava/lang/Throwable;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 60
    iget-object v0, p0, Lco/uk/getmondo/feed/search/g;->e:Lco/uk/getmondo/common/e/a;

    invoke-virtual {v0, p2, p1}, Lco/uk/getmondo/common/e/a;->a(Ljava/lang/Throwable;Lco/uk/getmondo/common/e/a$a;)Z

    return-void
.end method


# virtual methods
.method public bridge synthetic a(Lco/uk/getmondo/common/ui/f;)V
    .locals 0

    .prologue
    .line 21
    check-cast p1, Lco/uk/getmondo/feed/search/g$a;

    invoke-virtual {p0, p1}, Lco/uk/getmondo/feed/search/g;->a(Lco/uk/getmondo/feed/search/g$a;)V

    return-void
.end method

.method public a(Lco/uk/getmondo/feed/search/g$a;)V
    .locals 3

    .prologue
    .line 42
    invoke-super {p0, p1}, Lco/uk/getmondo/common/ui/b;->a(Lco/uk/getmondo/common/ui/f;)V

    .line 43
    iget-object v0, p0, Lco/uk/getmondo/feed/search/g;->g:Lco/uk/getmondo/common/a;

    invoke-static {}, Lco/uk/getmondo/api/model/tracking/Impression;->W()Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v1

    invoke-virtual {v0, v1}, Lco/uk/getmondo/common/a;->a(Lco/uk/getmondo/api/model/tracking/Impression;)V

    .line 45
    invoke-interface {p1}, Lco/uk/getmondo/feed/search/g$a;->a()Lio/reactivex/n;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/feed/search/g;->c:Lio/reactivex/u;

    .line 46
    invoke-virtual {v0, v1}, Lio/reactivex/n;->observeOn(Lio/reactivex/u;)Lio/reactivex/n;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/feed/search/g;->f:Lco/uk/getmondo/feed/a/d;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    invoke-static {v1}, Lco/uk/getmondo/feed/search/h;->a(Lco/uk/getmondo/feed/a/d;)Lio/reactivex/c/h;

    move-result-object v1

    .line 47
    invoke-virtual {v0, v1}, Lio/reactivex/n;->switchMap(Lio/reactivex/c/h;)Lio/reactivex/n;

    move-result-object v0

    invoke-static {p1}, Lco/uk/getmondo/feed/search/i;->a(Lco/uk/getmondo/feed/search/g$a;)Lio/reactivex/c/g;

    move-result-object v1

    invoke-static {}, Lco/uk/getmondo/feed/search/j;->a()Lio/reactivex/c/g;

    move-result-object v2

    .line 48
    invoke-virtual {v0, v1, v2}, Lio/reactivex/n;->subscribe(Lio/reactivex/c/g;Lio/reactivex/c/g;)Lio/reactivex/b/b;

    move-result-object v0

    .line 45
    invoke-virtual {p0, v0}, Lco/uk/getmondo/feed/search/g;->a(Lio/reactivex/b/b;)V

    .line 57
    invoke-interface {p1}, Lco/uk/getmondo/feed/search/g$a;->b()Lio/reactivex/n;

    move-result-object v0

    invoke-static {p0, p1}, Lco/uk/getmondo/feed/search/k;->a(Lco/uk/getmondo/feed/search/g;Lco/uk/getmondo/feed/search/g$a;)Lio/reactivex/c/h;

    move-result-object v1

    .line 58
    invoke-virtual {v0, v1}, Lio/reactivex/n;->flatMapCompletable(Lio/reactivex/c/h;)Lio/reactivex/b;

    move-result-object v0

    .line 62
    invoke-static {}, Lco/uk/getmondo/common/j/a;->a()Lio/reactivex/c/a;

    move-result-object v1

    invoke-static {}, Lco/uk/getmondo/feed/search/l;->a()Lio/reactivex/c/g;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lio/reactivex/b;->a(Lio/reactivex/c/a;Lio/reactivex/c/g;)Lio/reactivex/b/b;

    move-result-object v0

    .line 57
    invoke-virtual {p0, v0}, Lco/uk/getmondo/feed/search/g;->a(Lio/reactivex/b/b;)V

    .line 63
    return-void
.end method

.method public b()V
    .locals 2

    .prologue
    .line 67
    iget-object v0, p0, Lco/uk/getmondo/feed/search/g;->g:Lco/uk/getmondo/common/a;

    invoke-static {}, Lco/uk/getmondo/api/model/tracking/Impression;->X()Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v1

    invoke-virtual {v0, v1}, Lco/uk/getmondo/common/a;->a(Lco/uk/getmondo/api/model/tracking/Impression;)V

    .line 68
    invoke-super {p0}, Lco/uk/getmondo/common/ui/b;->b()V

    .line 69
    return-void
.end method
