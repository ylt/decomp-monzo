.class public Lco/uk/getmondo/feed/search/FeedSearchActivity;
.super Lco/uk/getmondo/common/activities/b;
.source "FeedSearchActivity.java"

# interfaces
.implements Lco/uk/getmondo/feed/adapter/a$a;
.implements Lco/uk/getmondo/feed/search/g$a;


# instance fields
.field a:Lco/uk/getmondo/feed/adapter/a;

.field b:Lco/uk/getmondo/feed/c;

.field c:Lco/uk/getmondo/feed/search/g;

.field private final e:Lcom/b/b/c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/b/b/c",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final f:Lcom/b/b/c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/b/b/c",
            "<",
            "Lco/uk/getmondo/d/m;",
            ">;"
        }
    .end annotation
.end field

.field noResults:Landroid/view/View;
    .annotation build Lbutterknife/BindView;
        value = 0x7f11016d
    .end annotation
.end field

.field searchRecyclerView:Landroid/support/v7/widget/RecyclerView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f11016c
    .end annotation
.end field

.field searchView:Landroid/support/v7/widget/SearchView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f11016b
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 34
    invoke-direct {p0}, Lco/uk/getmondo/common/activities/b;-><init>()V

    .line 46
    invoke-static {}, Lcom/b/b/c;->a()Lcom/b/b/c;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/feed/search/FeedSearchActivity;->e:Lcom/b/b/c;

    .line 47
    invoke-static {}, Lcom/b/b/c;->a()Lcom/b/b/c;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/feed/search/FeedSearchActivity;->f:Lcom/b/b/c;

    return-void
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 50
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lco/uk/getmondo/feed/search/FeedSearchActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "EXTRA_QUERY"

    .line 51
    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const/high16 v1, 0x4000000

    .line 52
    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    move-result-object v0

    .line 50
    return-object v0
.end method

.method static synthetic a(Ljava/lang/String;)Lio/reactivex/r;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 117
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 118
    invoke-static {p0}, Lio/reactivex/n;->just(Ljava/lang/Object;)Lio/reactivex/n;

    move-result-object v0

    .line 117
    :goto_0
    return-object v0

    .line 119
    :cond_0
    invoke-static {p0}, Lio/reactivex/n;->just(Ljava/lang/Object;)Lio/reactivex/n;

    move-result-object v0

    const-wide/16 v2, 0x190

    sget-object v1, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v2, v3, v1}, Lio/reactivex/n;->delay(JLjava/util/concurrent/TimeUnit;)Lio/reactivex/n;

    move-result-object v0

    goto :goto_0
.end method

.method static synthetic a(Lco/uk/getmondo/feed/search/FeedSearchActivity;)V
    .locals 2

    .prologue
    .line 130
    iget-object v0, p0, Lco/uk/getmondo/feed/search/FeedSearchActivity;->searchRecyclerView:Landroid/support/v7/widget/RecyclerView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->a(I)V

    return-void
.end method

.method static synthetic b(Lco/uk/getmondo/feed/search/FeedSearchActivity;)V
    .locals 1

    .prologue
    .line 92
    iget-object v0, p0, Lco/uk/getmondo/feed/search/FeedSearchActivity;->searchView:Landroid/support/v7/widget/SearchView;

    invoke-virtual {v0}, Landroid/support/v7/widget/SearchView;->clearFocus()V

    return-void
.end method

.method static synthetic c(Lco/uk/getmondo/feed/search/FeedSearchActivity;)Lcom/b/b/c;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lco/uk/getmondo/feed/search/FeedSearchActivity;->e:Lcom/b/b/c;

    return-object v0
.end method

.method private e()V
    .locals 2

    .prologue
    .line 100
    iget-object v0, p0, Lco/uk/getmondo/feed/search/FeedSearchActivity;->searchView:Landroid/support/v7/widget/SearchView;

    const v1, 0x7f0a018a

    invoke-virtual {p0, v1}, Lco/uk/getmondo/feed/search/FeedSearchActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/SearchView;->setQueryHint(Ljava/lang/CharSequence;)V

    .line 101
    iget-object v0, p0, Lco/uk/getmondo/feed/search/FeedSearchActivity;->searchView:Landroid/support/v7/widget/SearchView;

    new-instance v1, Lco/uk/getmondo/feed/search/FeedSearchActivity$1;

    invoke-direct {v1, p0}, Lco/uk/getmondo/feed/search/FeedSearchActivity$1;-><init>(Lco/uk/getmondo/feed/search/FeedSearchActivity;)V

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/SearchView;->setOnQueryTextListener(Landroid/support/v7/widget/SearchView$c;)V

    .line 113
    return-void
.end method


# virtual methods
.method public a()Lio/reactivex/n;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 117
    iget-object v0, p0, Lco/uk/getmondo/feed/search/FeedSearchActivity;->e:Lcom/b/b/c;

    invoke-static {}, Lco/uk/getmondo/feed/search/d;->a()Lio/reactivex/c/h;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/b/b/c;->debounce(Lio/reactivex/c/h;)Lio/reactivex/n;

    move-result-object v0

    return-object v0
.end method

.method public a(Lco/uk/getmondo/common/b/b;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lco/uk/getmondo/common/b/b",
            "<",
            "Lco/uk/getmondo/d/m;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 129
    iget-object v0, p0, Lco/uk/getmondo/feed/search/FeedSearchActivity;->a:Lco/uk/getmondo/feed/adapter/a;

    invoke-virtual {v0, p1}, Lco/uk/getmondo/feed/adapter/a;->a(Lco/uk/getmondo/common/b/b;)V

    .line 130
    iget-object v0, p0, Lco/uk/getmondo/feed/search/FeedSearchActivity;->searchRecyclerView:Landroid/support/v7/widget/RecyclerView;

    invoke-static {p0}, Lco/uk/getmondo/feed/search/e;->a(Lco/uk/getmondo/feed/search/FeedSearchActivity;)Ljava/lang/Runnable;

    move-result-object v1

    const-wide/16 v2, 0x32

    invoke-virtual {v0, v1, v2, v3}, Landroid/support/v7/widget/RecyclerView;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 131
    return-void
.end method

.method public a(Lco/uk/getmondo/d/m;)V
    .locals 1

    .prologue
    .line 158
    iget-object v0, p0, Lco/uk/getmondo/feed/search/FeedSearchActivity;->b:Lco/uk/getmondo/feed/c;

    invoke-virtual {v0, p1}, Lco/uk/getmondo/feed/c;->a(Lco/uk/getmondo/d/m;)V

    .line 159
    return-void
.end method

.method public b()Lio/reactivex/n;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Lco/uk/getmondo/d/m;",
            ">;"
        }
    .end annotation

    .prologue
    .line 124
    iget-object v0, p0, Lco/uk/getmondo/feed/search/FeedSearchActivity;->f:Lcom/b/b/c;

    return-object v0
.end method

.method public b(Lco/uk/getmondo/d/m;)V
    .locals 1

    .prologue
    .line 163
    iget-object v0, p0, Lco/uk/getmondo/feed/search/FeedSearchActivity;->f:Lcom/b/b/c;

    invoke-virtual {v0, p1}, Lcom/b/b/c;->a(Ljava/lang/Object;)V

    .line 164
    return-void
.end method

.method public c()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x78

    .line 135
    iget-object v0, p0, Lco/uk/getmondo/feed/search/FeedSearchActivity;->noResults:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 136
    iget-object v0, p0, Lco/uk/getmondo/feed/search/FeedSearchActivity;->noResults:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const/high16 v1, 0x3f800000    # 1.0f

    .line 137
    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    .line 138
    invoke-virtual {v0, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    .line 141
    iget-object v0, p0, Lco/uk/getmondo/feed/search/FeedSearchActivity;->searchRecyclerView:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const/4 v1, 0x0

    .line 142
    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    .line 143
    invoke-virtual {v0, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    .line 144
    return-void
.end method

.method public d()V
    .locals 2

    .prologue
    .line 148
    iget-object v0, p0, Lco/uk/getmondo/feed/search/FeedSearchActivity;->noResults:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->cancel()V

    .line 149
    iget-object v0, p0, Lco/uk/getmondo/feed/search/FeedSearchActivity;->noResults:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setAlpha(F)V

    .line 150
    iget-object v0, p0, Lco/uk/getmondo/feed/search/FeedSearchActivity;->noResults:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 152
    iget-object v0, p0, Lco/uk/getmondo/feed/search/FeedSearchActivity;->searchRecyclerView:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->cancel()V

    .line 153
    iget-object v0, p0, Lco/uk/getmondo/feed/search/FeedSearchActivity;->searchRecyclerView:Landroid/support/v7/widget/RecyclerView;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setAlpha(F)V

    .line 154
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    const/4 v2, 0x1

    .line 57
    invoke-super {p0, p1}, Lco/uk/getmondo/common/activities/b;->onCreate(Landroid/os/Bundle;)V

    .line 58
    const v0, 0x7f050039

    invoke-virtual {p0, v0}, Lco/uk/getmondo/feed/search/FeedSearchActivity;->setContentView(I)V

    .line 59
    invoke-static {p0}, Lbutterknife/ButterKnife;->bind(Landroid/app/Activity;)Lbutterknife/Unbinder;

    .line 61
    invoke-virtual {p0}, Lco/uk/getmondo/feed/search/FeedSearchActivity;->l()Lco/uk/getmondo/common/h/b/b;

    move-result-object v0

    invoke-interface {v0, p0}, Lco/uk/getmondo/common/h/b/b;->a(Lco/uk/getmondo/feed/search/FeedSearchActivity;)V

    .line 63
    invoke-direct {p0}, Lco/uk/getmondo/feed/search/FeedSearchActivity;->e()V

    .line 65
    iget-object v0, p0, Lco/uk/getmondo/feed/search/FeedSearchActivity;->a:Lco/uk/getmondo/feed/adapter/a;

    invoke-virtual {v0, p0}, Lco/uk/getmondo/feed/adapter/a;->a(Lco/uk/getmondo/feed/adapter/a$a;)V

    .line 67
    new-instance v0, Landroid/support/v7/widget/LinearLayoutManager;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v2, v1}, Landroid/support/v7/widget/LinearLayoutManager;-><init>(Landroid/content/Context;IZ)V

    .line 68
    iget-object v1, p0, Lco/uk/getmondo/feed/search/FeedSearchActivity;->searchRecyclerView:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v1, v0}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(Landroid/support/v7/widget/RecyclerView$h;)V

    .line 69
    iget-object v0, p0, Lco/uk/getmondo/feed/search/FeedSearchActivity;->searchRecyclerView:Landroid/support/v7/widget/RecyclerView;

    iget-object v1, p0, Lco/uk/getmondo/feed/search/FeedSearchActivity;->a:Lco/uk/getmondo/feed/adapter/a;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setAdapter(Landroid/support/v7/widget/RecyclerView$a;)V

    .line 70
    iget-object v0, p0, Lco/uk/getmondo/feed/search/FeedSearchActivity;->searchRecyclerView:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0, v2}, Landroid/support/v7/widget/RecyclerView;->setHasFixedSize(Z)V

    .line 71
    invoke-virtual {p0}, Lco/uk/getmondo/feed/search/FeedSearchActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0119

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 72
    iget-object v1, p0, Lco/uk/getmondo/feed/search/FeedSearchActivity;->searchRecyclerView:Landroid/support/v7/widget/RecyclerView;

    new-instance v2, Lco/uk/getmondo/common/ui/h;

    iget-object v3, p0, Lco/uk/getmondo/feed/search/FeedSearchActivity;->a:Lco/uk/getmondo/feed/adapter/a;

    invoke-direct {v2, p0, v3, v0}, Lco/uk/getmondo/common/ui/h;-><init>(Landroid/content/Context;La/a/a/a/a/a;I)V

    invoke-virtual {v1, v2}, Landroid/support/v7/widget/RecyclerView;->a(Landroid/support/v7/widget/RecyclerView$g;)V

    .line 73
    iget-object v0, p0, Lco/uk/getmondo/feed/search/FeedSearchActivity;->searchRecyclerView:Landroid/support/v7/widget/RecyclerView;

    new-instance v1, La/a/a/a/a/b;

    iget-object v2, p0, Lco/uk/getmondo/feed/search/FeedSearchActivity;->a:Lco/uk/getmondo/feed/adapter/a;

    invoke-direct {v1, v2}, La/a/a/a/a/b;-><init>(La/a/a/a/a/a;)V

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->a(Landroid/support/v7/widget/RecyclerView$g;)V

    .line 75
    iget-object v0, p0, Lco/uk/getmondo/feed/search/FeedSearchActivity;->c:Lco/uk/getmondo/feed/search/g;

    invoke-virtual {v0, p0}, Lco/uk/getmondo/feed/search/g;->a(Lco/uk/getmondo/feed/search/g$a;)V

    .line 77
    invoke-virtual {p0}, Lco/uk/getmondo/feed/search/FeedSearchActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lco/uk/getmondo/feed/search/FeedSearchActivity;->onNewIntent(Landroid/content/Intent;)V

    .line 78
    return-void
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lco/uk/getmondo/feed/search/FeedSearchActivity;->c:Lco/uk/getmondo/feed/search/g;

    invoke-virtual {v0}, Lco/uk/getmondo/feed/search/g;->b()V

    .line 83
    invoke-super {p0}, Lco/uk/getmondo/common/activities/b;->onDestroy()V

    .line 84
    return-void
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 3

    .prologue
    .line 88
    const-string v0, "EXTRA_QUERY"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 89
    const-string v0, "EXTRA_QUERY"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 90
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 91
    iget-object v1, p0, Lco/uk/getmondo/feed/search/FeedSearchActivity;->searchView:Landroid/support/v7/widget/SearchView;

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Landroid/support/v7/widget/SearchView;->a(Ljava/lang/CharSequence;Z)V

    .line 92
    iget-object v0, p0, Lco/uk/getmondo/feed/search/FeedSearchActivity;->searchView:Landroid/support/v7/widget/SearchView;

    invoke-static {p0}, Lco/uk/getmondo/feed/search/c;->a(Lco/uk/getmondo/feed/search/FeedSearchActivity;)Ljava/lang/Runnable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/SearchView;->post(Ljava/lang/Runnable;)Z

    .line 97
    :cond_0
    :goto_0
    return-void

    .line 95
    :cond_1
    iget-object v0, p0, Lco/uk/getmondo/feed/search/FeedSearchActivity;->e:Lcom/b/b/c;

    const-string v1, ""

    invoke-virtual {v0, v1}, Lcom/b/b/c;->a(Ljava/lang/Object;)V

    goto :goto_0
.end method
