.class final Lco/uk/getmondo/feed/g$n;
.super Ljava/lang/Object;
.source "HomeFeedPresenter.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lco/uk/getmondo/feed/g;->a(Lco/uk/getmondo/feed/g$a;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h",
        "<",
        "Lkotlin/n;",
        "Lio/reactivex/d;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000\u0012\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\n \u0002*\u0004\u0018\u00010\u00010\u00012\u0006\u0010\u0003\u001a\u00020\u0004H\n\u00a2\u0006\u0004\u0008\u0005\u0010\u0006"
    }
    d2 = {
        "<anonymous>",
        "Lio/reactivex/Completable;",
        "kotlin.jvm.PlatformType",
        "it",
        "",
        "apply",
        "(Lkotlin/Unit;)Lio/reactivex/Completable;"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field final synthetic a:Lco/uk/getmondo/feed/g;

.field final synthetic b:Lco/uk/getmondo/feed/g$a;


# direct methods
.method constructor <init>(Lco/uk/getmondo/feed/g;Lco/uk/getmondo/feed/g$a;)V
    .locals 0

    iput-object p1, p0, Lco/uk/getmondo/feed/g$n;->a:Lco/uk/getmondo/feed/g;

    iput-object p2, p0, Lco/uk/getmondo/feed/g$n;->b:Lco/uk/getmondo/feed/g$a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lkotlin/n;)Lio/reactivex/b;
    .locals 2

    .prologue
    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 123
    iget-object v0, p0, Lco/uk/getmondo/feed/g$n;->a:Lco/uk/getmondo/feed/g;

    invoke-static {v0}, Lco/uk/getmondo/feed/g;->j(Lco/uk/getmondo/feed/g;)Lco/uk/getmondo/common/accounts/b;

    move-result-object v0

    invoke-virtual {v0}, Lco/uk/getmondo/common/accounts/b;->e()Lio/reactivex/n;

    move-result-object v1

    .line 124
    new-instance v0, Lco/uk/getmondo/feed/g$n$1;

    invoke-direct {v0, p0}, Lco/uk/getmondo/feed/g$n$1;-><init>(Lco/uk/getmondo/feed/g$n;)V

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {v1, v0}, Lio/reactivex/n;->flatMapCompletable(Lio/reactivex/c/h;)Lio/reactivex/b;

    move-result-object v0

    .line 125
    iget-object v1, p0, Lco/uk/getmondo/feed/g$n;->a:Lco/uk/getmondo/feed/g;

    invoke-static {v1}, Lco/uk/getmondo/feed/g;->c(Lco/uk/getmondo/feed/g;)Lio/reactivex/u;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/b;->b(Lio/reactivex/u;)Lio/reactivex/b;

    move-result-object v0

    .line 126
    iget-object v1, p0, Lco/uk/getmondo/feed/g$n;->a:Lco/uk/getmondo/feed/g;

    invoke-static {v1}, Lco/uk/getmondo/feed/g;->e(Lco/uk/getmondo/feed/g;)Lio/reactivex/u;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/b;->a(Lio/reactivex/u;)Lio/reactivex/b;

    move-result-object v1

    .line 127
    new-instance v0, Lco/uk/getmondo/feed/g$n$2;

    invoke-direct {v0, p0}, Lco/uk/getmondo/feed/g$n$2;-><init>(Lco/uk/getmondo/feed/g$n;)V

    check-cast v0, Lio/reactivex/c/g;

    invoke-virtual {v1, v0}, Lio/reactivex/b;->a(Lio/reactivex/c/g;)Lio/reactivex/b;

    move-result-object v0

    .line 128
    invoke-virtual {v0}, Lio/reactivex/b;->b()Lio/reactivex/b;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 26
    check-cast p1, Lkotlin/n;

    invoke-virtual {p0, p1}, Lco/uk/getmondo/feed/g$n;->a(Lkotlin/n;)Lio/reactivex/b;

    move-result-object v0

    return-object v0
.end method
