.class public Lco/uk/getmondo/feed/a;
.super Ljava/lang/Object;
.source "DeclineReasonStringProvider.java"


# instance fields
.field private final a:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    iput-object p1, p0, Lco/uk/getmondo/feed/a;->a:Landroid/content/Context;

    .line 18
    return-void
.end method


# virtual methods
.method public a(Lco/uk/getmondo/d/j;)Ljava/lang/String;
    .locals 4

    .prologue
    const v3, 0x7f0a0144

    const v2, 0x7f0a013c

    .line 21
    sget-object v0, Lco/uk/getmondo/feed/a$1;->a:[I

    invoke-virtual {p1}, Lco/uk/getmondo/d/j;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 61
    iget-object v0, p0, Lco/uk/getmondo/feed/a;->a:Landroid/content/Context;

    invoke-virtual {v0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    .line 23
    :pswitch_0
    const/4 v0, 0x0

    goto :goto_0

    .line 25
    :pswitch_1
    iget-object v0, p0, Lco/uk/getmondo/feed/a;->a:Landroid/content/Context;

    invoke-virtual {v0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 27
    :pswitch_2
    iget-object v0, p0, Lco/uk/getmondo/feed/a;->a:Landroid/content/Context;

    const v1, 0x7f0a013d

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 29
    :pswitch_3
    iget-object v0, p0, Lco/uk/getmondo/feed/a;->a:Landroid/content/Context;

    const v1, 0x7f0a0135

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 31
    :pswitch_4
    iget-object v0, p0, Lco/uk/getmondo/feed/a;->a:Landroid/content/Context;

    const v1, 0x7f0a0136

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 33
    :pswitch_5
    iget-object v0, p0, Lco/uk/getmondo/feed/a;->a:Landroid/content/Context;

    const v1, 0x7f0a0140

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 35
    :pswitch_6
    iget-object v0, p0, Lco/uk/getmondo/feed/a;->a:Landroid/content/Context;

    const v1, 0x7f0a0143

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 37
    :pswitch_7
    iget-object v0, p0, Lco/uk/getmondo/feed/a;->a:Landroid/content/Context;

    const v1, 0x7f0a013f

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 39
    :pswitch_8
    iget-object v0, p0, Lco/uk/getmondo/feed/a;->a:Landroid/content/Context;

    const v1, 0x7f0a013e

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 41
    :pswitch_9
    iget-object v0, p0, Lco/uk/getmondo/feed/a;->a:Landroid/content/Context;

    const v1, 0x7f0a0137

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 43
    :pswitch_a
    iget-object v0, p0, Lco/uk/getmondo/feed/a;->a:Landroid/content/Context;

    const v1, 0x7f0a0142

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 45
    :pswitch_b
    iget-object v0, p0, Lco/uk/getmondo/feed/a;->a:Landroid/content/Context;

    const v1, 0x7f0a0138

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 47
    :pswitch_c
    iget-object v0, p0, Lco/uk/getmondo/feed/a;->a:Landroid/content/Context;

    const v1, 0x7f0a013a

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 49
    :pswitch_d
    iget-object v0, p0, Lco/uk/getmondo/feed/a;->a:Landroid/content/Context;

    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 51
    :pswitch_e
    iget-object v0, p0, Lco/uk/getmondo/feed/a;->a:Landroid/content/Context;

    const v1, 0x7f0a0139

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 53
    :pswitch_f
    iget-object v0, p0, Lco/uk/getmondo/feed/a;->a:Landroid/content/Context;

    const v1, 0x7f0a013b

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 55
    :pswitch_10
    iget-object v0, p0, Lco/uk/getmondo/feed/a;->a:Landroid/content/Context;

    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 57
    :pswitch_11
    iget-object v0, p0, Lco/uk/getmondo/feed/a;->a:Landroid/content/Context;

    const v1, 0x7f0a0145

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 59
    :pswitch_12
    iget-object v0, p0, Lco/uk/getmondo/feed/a;->a:Landroid/content/Context;

    const v1, 0x7f0a0141

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 21
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_11
        :pswitch_12
    .end packed-switch
.end method
