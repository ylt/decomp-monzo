.class final Lco/uk/getmondo/feed/g$b;
.super Ljava/lang/Object;
.source "HomeFeedPresenter.kt"

# interfaces
.implements Lio/reactivex/c/g;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lco/uk/getmondo/feed/g;->a(Lco/uk/getmondo/feed/g$a;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/g",
        "<",
        "Lco/uk/getmondo/common/b/b",
        "<+",
        "Lco/uk/getmondo/d/m;",
        ">;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000\u0014\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012*\u0010\u0002\u001a&\u0012\u000c\u0012\n \u0005*\u0004\u0018\u00010\u00040\u0004 \u0005*\u0012\u0012\u000c\u0012\n \u0005*\u0004\u0018\u00010\u00040\u0004\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "",
        "queryResults",
        "Lco/uk/getmondo/common/data/QueryResults;",
        "Lco/uk/getmondo/model/FeedItem;",
        "kotlin.jvm.PlatformType",
        "accept"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field final synthetic a:Lco/uk/getmondo/feed/g;

.field final synthetic b:Lco/uk/getmondo/feed/g$a;


# direct methods
.method constructor <init>(Lco/uk/getmondo/feed/g;Lco/uk/getmondo/feed/g$a;)V
    .locals 0

    iput-object p1, p0, Lco/uk/getmondo/feed/g$b;->a:Lco/uk/getmondo/feed/g;

    iput-object p2, p0, Lco/uk/getmondo/feed/g$b;->b:Lco/uk/getmondo/feed/g$a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lco/uk/getmondo/common/b/b;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lco/uk/getmondo/common/b/b",
            "<+",
            "Lco/uk/getmondo/d/m;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 49
    invoke-virtual {p1}, Lco/uk/getmondo/common/b/b;->a()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lco/uk/getmondo/feed/g$b;->a:Lco/uk/getmondo/feed/g;

    invoke-static {v0}, Lco/uk/getmondo/feed/g;->a(Lco/uk/getmondo/feed/g;)Lco/uk/getmondo/feed/a/d;

    move-result-object v0

    invoke-virtual {v0}, Lco/uk/getmondo/feed/a/d;->c()Lco/uk/getmondo/d/n;

    move-result-object v0

    if-nez v0, :cond_0

    .line 50
    iget-object v0, p0, Lco/uk/getmondo/feed/g$b;->b:Lco/uk/getmondo/feed/g$a;

    invoke-interface {v0}, Lco/uk/getmondo/feed/g$a;->e()V

    .line 53
    :goto_0
    iget-object v0, p0, Lco/uk/getmondo/feed/g$b;->b:Lco/uk/getmondo/feed/g$a;

    const-string v1, "queryResults"

    invoke-static {p1, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v0, p1}, Lco/uk/getmondo/feed/g$a;->a(Lco/uk/getmondo/common/b/b;)V

    .line 55
    return-void

    .line 52
    :cond_0
    iget-object v0, p0, Lco/uk/getmondo/feed/g$b;->b:Lco/uk/getmondo/feed/g$a;

    invoke-interface {v0}, Lco/uk/getmondo/feed/g$a;->f()V

    goto :goto_0
.end method

.method public bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 26
    check-cast p1, Lco/uk/getmondo/common/b/b;

    invoke-virtual {p0, p1}, Lco/uk/getmondo/feed/g$b;->a(Lco/uk/getmondo/common/b/b;)V

    return-void
.end method
