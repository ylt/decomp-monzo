.class public final Lco/uk/getmondo/feed/adapter/c;
.super Landroid/widget/PopupMenu;
.source "FeedItemPopupMenu.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u00002\u00020\u0001B%\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u00a2\u0006\u0002\u0010\n\u00a8\u0006\u000b"
    }
    d2 = {
        "Lco/uk/getmondo/feed/adapter/FeedItemPopupMenu;",
        "Landroid/widget/PopupMenu;",
        "context",
        "Landroid/content/Context;",
        "anchor",
        "Landroid/view/View;",
        "feedItem",
        "Lco/uk/getmondo/model/FeedItem;",
        "listener",
        "Lco/uk/getmondo/feed/adapter/FeedAdapter$OnFeedItemActionListener;",
        "(Landroid/content/Context;Landroid/view/View;Lco/uk/getmondo/model/FeedItem;Lco/uk/getmondo/feed/adapter/FeedAdapter$OnFeedItemActionListener;)V",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/view/View;Lco/uk/getmondo/d/m;Lco/uk/getmondo/feed/adapter/a$a;)V
    .locals 3

    .prologue
    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "anchor"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "feedItem"

    invoke-static {p3, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "listener"

    invoke-static {p4, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 11
    invoke-direct {p0, p1, p2}, Landroid/widget/PopupMenu;-><init>(Landroid/content/Context;Landroid/view/View;)V

    .line 14
    invoke-virtual {p0}, Lco/uk/getmondo/feed/adapter/c;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const v1, 0x7f130003

    invoke-virtual {p0}, Lco/uk/getmondo/feed/adapter/c;->getMenu()Landroid/view/Menu;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 15
    new-instance v0, Lco/uk/getmondo/feed/adapter/c$1;

    invoke-direct {v0, p4, p3}, Lco/uk/getmondo/feed/adapter/c$1;-><init>(Lco/uk/getmondo/feed/adapter/a$a;Lco/uk/getmondo/d/m;)V

    check-cast v0, Landroid/widget/PopupMenu$OnMenuItemClickListener;

    invoke-virtual {p0, v0}, Lco/uk/getmondo/feed/adapter/c;->setOnMenuItemClickListener(Landroid/widget/PopupMenu$OnMenuItemClickListener;)V

    return-void
.end method
