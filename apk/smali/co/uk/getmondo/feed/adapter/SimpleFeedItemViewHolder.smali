.class Lco/uk/getmondo/feed/adapter/SimpleFeedItemViewHolder;
.super Landroid/support/v7/widget/RecyclerView$w;
.source "SimpleFeedItemViewHolder.java"


# instance fields
.field private final a:Lco/uk/getmondo/feed/a/a;

.field private final b:Lco/uk/getmondo/feed/adapter/a$a;

.field private c:Lco/uk/getmondo/d/m;

.field descriptionTextView:Landroid/widget/TextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f11041e
    .end annotation
.end field

.field iconImageView:Landroid/widget/ImageView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f11041b
    .end annotation
.end field

.field overflowButton:Landroid/view/View;
    .annotation build Lbutterknife/BindView;
        value = 0x7f11041c
    .end annotation
.end field

.field titleTextView:Landroid/widget/TextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f11041d
    .end annotation
.end field


# direct methods
.method constructor <init>(Landroid/view/View;Lco/uk/getmondo/feed/a/a;Lco/uk/getmondo/feed/adapter/a$a;)V
    .locals 2

    .prologue
    .line 32
    invoke-direct {p0, p1}, Landroid/support/v7/widget/RecyclerView$w;-><init>(Landroid/view/View;)V

    .line 33
    iput-object p2, p0, Lco/uk/getmondo/feed/adapter/SimpleFeedItemViewHolder;->a:Lco/uk/getmondo/feed/a/a;

    .line 34
    iput-object p3, p0, Lco/uk/getmondo/feed/adapter/SimpleFeedItemViewHolder;->b:Lco/uk/getmondo/feed/adapter/a$a;

    .line 35
    invoke-static {p0, p1}, Lbutterknife/ButterKnife;->bind(Ljava/lang/Object;Landroid/view/View;)Lbutterknife/Unbinder;

    .line 36
    invoke-static {p0, p3}, Lco/uk/getmondo/feed/adapter/d;->a(Lco/uk/getmondo/feed/adapter/SimpleFeedItemViewHolder;Lco/uk/getmondo/feed/adapter/a$a;)Landroid/view/View$OnClickListener;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 37
    iget-object v0, p0, Lco/uk/getmondo/feed/adapter/SimpleFeedItemViewHolder;->overflowButton:Landroid/view/View;

    invoke-static {p0}, Lco/uk/getmondo/feed/adapter/e;->a(Lco/uk/getmondo/feed/adapter/SimpleFeedItemViewHolder;)Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 38
    return-void
.end method

.method static synthetic a(Lco/uk/getmondo/feed/adapter/SimpleFeedItemViewHolder;Landroid/view/View;)V
    .locals 4

    .prologue
    .line 37
    new-instance v0, Lco/uk/getmondo/feed/adapter/c;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lco/uk/getmondo/feed/adapter/SimpleFeedItemViewHolder;->c:Lco/uk/getmondo/d/m;

    iget-object v3, p0, Lco/uk/getmondo/feed/adapter/SimpleFeedItemViewHolder;->b:Lco/uk/getmondo/feed/adapter/a$a;

    invoke-direct {v0, v1, p1, v2, v3}, Lco/uk/getmondo/feed/adapter/c;-><init>(Landroid/content/Context;Landroid/view/View;Lco/uk/getmondo/d/m;Lco/uk/getmondo/feed/adapter/a$a;)V

    invoke-virtual {v0}, Lco/uk/getmondo/feed/adapter/c;->show()V

    return-void
.end method

.method static synthetic a(Lco/uk/getmondo/feed/adapter/SimpleFeedItemViewHolder;Lco/uk/getmondo/feed/adapter/a$a;Landroid/view/View;)V
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lco/uk/getmondo/feed/adapter/SimpleFeedItemViewHolder;->c:Lco/uk/getmondo/d/m;

    invoke-interface {p1, v0}, Lco/uk/getmondo/feed/adapter/a$a;->a(Lco/uk/getmondo/d/m;)V

    return-void
.end method


# virtual methods
.method a(Lco/uk/getmondo/d/m;)V
    .locals 3

    .prologue
    .line 41
    iput-object p1, p0, Lco/uk/getmondo/feed/adapter/SimpleFeedItemViewHolder;->c:Lco/uk/getmondo/d/m;

    .line 43
    invoke-virtual {p1}, Lco/uk/getmondo/d/m;->i()Lco/uk/getmondo/d/f;

    move-result-object v0

    .line 44
    iget-object v1, p0, Lco/uk/getmondo/feed/adapter/SimpleFeedItemViewHolder;->a:Lco/uk/getmondo/feed/a/a;

    invoke-virtual {p1}, Lco/uk/getmondo/d/m;->j()Lco/uk/getmondo/feed/a/a/a;

    move-result-object v2

    invoke-virtual {v1, v2}, Lco/uk/getmondo/feed/a/a;->a(Lco/uk/getmondo/feed/a/a/a;)I

    move-result v1

    .line 45
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lco/uk/getmondo/d/f;->c()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 46
    iget-object v2, p0, Lco/uk/getmondo/feed/adapter/SimpleFeedItemViewHolder;->iconImageView:Landroid/widget/ImageView;

    invoke-virtual {v2}, Landroid/widget/ImageView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/bumptech/glide/g;->b(Landroid/content/Context;)Lcom/bumptech/glide/j;

    move-result-object v2

    .line 47
    invoke-virtual {v0}, Lco/uk/getmondo/d/f;->c()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/bumptech/glide/j;->a(Ljava/lang/String;)Lcom/bumptech/glide/d;

    move-result-object v0

    .line 48
    invoke-virtual {v0}, Lcom/bumptech/glide/d;->a()Lcom/bumptech/glide/c;

    move-result-object v0

    .line 49
    invoke-virtual {v0, v1}, Lcom/bumptech/glide/c;->a(I)Lcom/bumptech/glide/c;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/feed/adapter/SimpleFeedItemViewHolder;->iconImageView:Landroid/widget/ImageView;

    .line 50
    invoke-virtual {v0, v1}, Lcom/bumptech/glide/c;->a(Landroid/widget/ImageView;)Lcom/bumptech/glide/g/b/j;

    .line 54
    :goto_0
    iget-object v0, p0, Lco/uk/getmondo/feed/adapter/SimpleFeedItemViewHolder;->iconImageView:Landroid/widget/ImageView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setClipToOutline(Z)V

    .line 56
    iget-object v0, p0, Lco/uk/getmondo/feed/adapter/SimpleFeedItemViewHolder;->titleTextView:Landroid/widget/TextView;

    iget-object v1, p0, Lco/uk/getmondo/feed/adapter/SimpleFeedItemViewHolder;->a:Lco/uk/getmondo/feed/a/a;

    invoke-virtual {v1, p1}, Lco/uk/getmondo/feed/a/a;->a(Lco/uk/getmondo/d/m;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 57
    iget-object v0, p0, Lco/uk/getmondo/feed/adapter/SimpleFeedItemViewHolder;->descriptionTextView:Landroid/widget/TextView;

    iget-object v1, p0, Lco/uk/getmondo/feed/adapter/SimpleFeedItemViewHolder;->a:Lco/uk/getmondo/feed/a/a;

    invoke-virtual {v1, p1}, Lco/uk/getmondo/feed/a/a;->b(Lco/uk/getmondo/d/m;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 58
    iget-object v1, p0, Lco/uk/getmondo/feed/adapter/SimpleFeedItemViewHolder;->overflowButton:Landroid/view/View;

    invoke-virtual {p1}, Lco/uk/getmondo/d/m;->k()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    :goto_1
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 59
    return-void

    .line 52
    :cond_0
    iget-object v0, p0, Lco/uk/getmondo/feed/adapter/SimpleFeedItemViewHolder;->iconImageView:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0

    .line 58
    :cond_1
    const/16 v0, 0x8

    goto :goto_1
.end method
