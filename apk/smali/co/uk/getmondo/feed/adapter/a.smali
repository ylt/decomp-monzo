.class public Lco/uk/getmondo/feed/adapter/a;
.super Landroid/support/v7/widget/RecyclerView$a;
.source "FeedAdapter.java"

# interfaces
.implements La/a/a/a/a/a;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lco/uk/getmondo/feed/adapter/a$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/support/v7/widget/RecyclerView$a",
        "<",
        "Landroid/support/v7/widget/RecyclerView$w;",
        ">;",
        "La/a/a/a/a/a",
        "<",
        "Landroid/support/v7/widget/RecyclerView$w;",
        ">;"
    }
.end annotation


# instance fields
.field protected a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lco/uk/getmondo/d/m;",
            ">;"
        }
    .end annotation
.end field

.field private b:Lco/uk/getmondo/feed/adapter/a$a;

.field private final c:Lco/uk/getmondo/feed/a/a;


# direct methods
.method public constructor <init>(Lco/uk/getmondo/feed/a/a;)V
    .locals 1

    .prologue
    .line 39
    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView$a;-><init>()V

    .line 34
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/feed/adapter/a;->a:Ljava/util/List;

    .line 40
    iput-object p1, p0, Lco/uk/getmondo/feed/adapter/a;->c:Lco/uk/getmondo/feed/a/a;

    .line 41
    return-void
.end method

.method private varargs a(Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 81
    invoke-static {p1, p2}, Ld/a/a;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 82
    sget-object v0, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-static {v0, p1, p2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/crashlytics/android/Crashlytics;->log(Ljava/lang/String;)V

    .line 83
    return-void
.end method


# virtual methods
.method public a(I)J
    .locals 2

    .prologue
    .line 126
    iget-object v0, p0, Lco/uk/getmondo/feed/adapter/a;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/d/m;

    invoke-virtual {v0}, Lco/uk/getmondo/d/m;->c()Ljava/util/Date;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Date;->getTime()J

    move-result-wide v0

    invoke-static {v0, v1}, Lco/uk/getmondo/common/c/a;->a(J)I

    move-result v0

    int-to-long v0, v0

    return-wide v0
.end method

.method public a(Landroid/view/ViewGroup;)Landroid/support/v7/widget/RecyclerView$w;
    .locals 4

    .prologue
    .line 131
    new-instance v0, Lco/uk/getmondo/feed/adapter/HeaderViewHolder;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f050103

    const/4 v3, 0x0

    invoke-virtual {v1, v2, p1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    invoke-direct {v0, v1}, Lco/uk/getmondo/feed/adapter/HeaderViewHolder;-><init>(Landroid/view/View;)V

    return-object v0
.end method

.method public a(Landroid/support/v7/widget/RecyclerView$w;I)V
    .locals 4

    .prologue
    .line 136
    move-object v0, p1

    check-cast v0, Lco/uk/getmondo/feed/adapter/HeaderViewHolder;

    iget-object v1, v0, Lco/uk/getmondo/feed/adapter/HeaderViewHolder;->dateView:Landroid/widget/TextView;

    iget-object v0, p0, Lco/uk/getmondo/feed/adapter/a;->a:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/d/m;

    invoke-virtual {v0}, Lco/uk/getmondo/d/m;->c()Ljava/util/Date;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Date;->getTime()J

    move-result-wide v2

    const/4 v0, 0x1

    invoke-static {v2, v3, v0}, Lco/uk/getmondo/common/c/a;->a(JZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 137
    check-cast p1, Lco/uk/getmondo/feed/adapter/HeaderViewHolder;

    iget-object v0, p1, Lco/uk/getmondo/feed/adapter/HeaderViewHolder;->dateView:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 138
    return-void
.end method

.method public a(Lco/uk/getmondo/common/b/b;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lco/uk/getmondo/common/b/b",
            "<",
            "Lco/uk/getmondo/d/m;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 48
    const-string v0, "Setting feed items from query results - size: %s changes: %s"

    new-array v1, v7, [Ljava/lang/Object;

    .line 49
    invoke-virtual {p1}, Lco/uk/getmondo/common/b/b;->a()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v5

    invoke-virtual {p1}, Lco/uk/getmondo/common/b/b;->b()Ljava/util/List;

    move-result-object v2

    aput-object v2, v1, v6

    .line 48
    invoke-direct {p0, v0, v1}, Lco/uk/getmondo/feed/adapter/a;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 55
    invoke-virtual {p1}, Lco/uk/getmondo/common/b/b;->a()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    :goto_0
    iput-object v0, p0, Lco/uk/getmondo/feed/adapter/a;->a:Ljava/util/List;

    .line 56
    invoke-virtual {p1}, Lco/uk/getmondo/common/b/b;->b()Ljava/util/List;

    move-result-object v0

    if-nez v0, :cond_2

    .line 57
    const-string v0, "notifyDataSetChanged called"

    new-array v1, v5, [Ljava/lang/Object;

    invoke-direct {p0, v0, v1}, Lco/uk/getmondo/feed/adapter/a;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 58
    invoke-virtual {p0}, Lco/uk/getmondo/feed/adapter/a;->notifyDataSetChanged()V

    .line 77
    :cond_0
    return-void

    .line 55
    :cond_1
    invoke-virtual {p1}, Lco/uk/getmondo/common/b/b;->a()Ljava/util/List;

    move-result-object v0

    goto :goto_0

    .line 60
    :cond_2
    invoke-virtual {p1}, Lco/uk/getmondo/common/b/b;->b()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/common/b/b$a;

    .line 61
    sget-object v2, Lco/uk/getmondo/feed/adapter/a$1;->a:[I

    invoke-virtual {v0}, Lco/uk/getmondo/common/b/b$a;->a()Lco/uk/getmondo/common/b/b$a$a;

    move-result-object v3

    invoke-virtual {v3}, Lco/uk/getmondo/common/b/b$a$a;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    goto :goto_1

    .line 63
    :pswitch_0
    const-string v2, "notifyItemRangeInserted called startIndex: %d length: %d"

    new-array v3, v7, [Ljava/lang/Object;

    invoke-virtual {v0}, Lco/uk/getmondo/common/b/b$a;->b()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-virtual {v0}, Lco/uk/getmondo/common/b/b$a;->c()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-direct {p0, v2, v3}, Lco/uk/getmondo/feed/adapter/a;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 64
    invoke-virtual {v0}, Lco/uk/getmondo/common/b/b$a;->b()I

    move-result v2

    invoke-virtual {v0}, Lco/uk/getmondo/common/b/b$a;->c()I

    move-result v0

    invoke-virtual {p0, v2, v0}, Lco/uk/getmondo/feed/adapter/a;->notifyItemRangeInserted(II)V

    goto :goto_1

    .line 67
    :pswitch_1
    const-string v2, "notifyItemRangeRemoved called startIndex: %d length: %d"

    new-array v3, v7, [Ljava/lang/Object;

    invoke-virtual {v0}, Lco/uk/getmondo/common/b/b$a;->b()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-virtual {v0}, Lco/uk/getmondo/common/b/b$a;->c()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-direct {p0, v2, v3}, Lco/uk/getmondo/feed/adapter/a;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 68
    invoke-virtual {v0}, Lco/uk/getmondo/common/b/b$a;->b()I

    move-result v2

    invoke-virtual {v0}, Lco/uk/getmondo/common/b/b$a;->c()I

    move-result v0

    invoke-virtual {p0, v2, v0}, Lco/uk/getmondo/feed/adapter/a;->notifyItemRangeRemoved(II)V

    goto :goto_1

    .line 71
    :pswitch_2
    const-string v2, "notifyItemRangeChanged called startIndex: %d length: %d"

    new-array v3, v7, [Ljava/lang/Object;

    invoke-virtual {v0}, Lco/uk/getmondo/common/b/b$a;->b()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-virtual {v0}, Lco/uk/getmondo/common/b/b$a;->c()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-direct {p0, v2, v3}, Lco/uk/getmondo/feed/adapter/a;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 72
    invoke-virtual {v0}, Lco/uk/getmondo/common/b/b$a;->b()I

    move-result v2

    invoke-virtual {v0}, Lco/uk/getmondo/common/b/b$a;->c()I

    move-result v0

    invoke-virtual {p0, v2, v0}, Lco/uk/getmondo/feed/adapter/a;->notifyItemRangeChanged(II)V

    goto/16 :goto_1

    .line 61
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public a(Lco/uk/getmondo/feed/adapter/a$a;)V
    .locals 0

    .prologue
    .line 44
    iput-object p1, p0, Lco/uk/getmondo/feed/adapter/a;->b:Lco/uk/getmondo/feed/adapter/a$a;

    .line 45
    return-void
.end method

.method public getItemCount()I
    .locals 1

    .prologue
    .line 121
    iget-object v0, p0, Lco/uk/getmondo/feed/adapter/a;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getItemViewType(I)I
    .locals 2

    .prologue
    .line 87
    iget-object v0, p0, Lco/uk/getmondo/feed/adapter/a;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/d/m;

    invoke-virtual {v0}, Lco/uk/getmondo/d/m;->j()Lco/uk/getmondo/feed/a/a/a;

    move-result-object v0

    .line 88
    sget-object v1, Lco/uk/getmondo/feed/a/a/a;->a:Lco/uk/getmondo/feed/a/a/a;

    if-ne v0, v1, :cond_0

    .line 89
    const/4 v0, 0x0

    .line 91
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public onBindViewHolder(Landroid/support/v7/widget/RecyclerView$w;I)V
    .locals 2

    .prologue
    .line 111
    iget-object v0, p0, Lco/uk/getmondo/feed/adapter/a;->a:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/d/m;

    .line 112
    instance-of v1, p1, Lco/uk/getmondo/feed/adapter/f;

    if-eqz v1, :cond_1

    .line 113
    check-cast p1, Lco/uk/getmondo/feed/adapter/f;

    invoke-virtual {p1, v0}, Lco/uk/getmondo/feed/adapter/f;->a(Lco/uk/getmondo/d/m;)V

    .line 117
    :cond_0
    :goto_0
    return-void

    .line 114
    :cond_1
    instance-of v1, p1, Lco/uk/getmondo/feed/adapter/SimpleFeedItemViewHolder;

    if-eqz v1, :cond_0

    .line 115
    check-cast p1, Lco/uk/getmondo/feed/adapter/SimpleFeedItemViewHolder;

    invoke-virtual {p1, v0}, Lco/uk/getmondo/feed/adapter/SimpleFeedItemViewHolder;->a(Lco/uk/getmondo/d/m;)V

    goto :goto_0
.end method

.method public onCreateViewHolder(Landroid/view/ViewGroup;I)Landroid/support/v7/widget/RecyclerView$w;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 97
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    .line 98
    packed-switch p2, :pswitch_data_0

    .line 105
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Unknown feed item type: 16843169"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 100
    :pswitch_0
    new-instance v0, Lco/uk/getmondo/feed/adapter/f;

    const v2, 0x7f05010c

    invoke-virtual {v1, v2, p1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    iget-object v2, p0, Lco/uk/getmondo/feed/adapter/a;->b:Lco/uk/getmondo/feed/adapter/a$a;

    invoke-direct {v0, v1, v2}, Lco/uk/getmondo/feed/adapter/f;-><init>(Landroid/view/View;Lco/uk/getmondo/feed/adapter/a$a;)V

    .line 102
    :goto_0
    return-object v0

    :pswitch_1
    new-instance v0, Lco/uk/getmondo/feed/adapter/SimpleFeedItemViewHolder;

    const v2, 0x7f050102

    invoke-virtual {v1, v2, p1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    iget-object v2, p0, Lco/uk/getmondo/feed/adapter/a;->c:Lco/uk/getmondo/feed/a/a;

    iget-object v3, p0, Lco/uk/getmondo/feed/adapter/a;->b:Lco/uk/getmondo/feed/adapter/a$a;

    invoke-direct {v0, v1, v2, v3}, Lco/uk/getmondo/feed/adapter/SimpleFeedItemViewHolder;-><init>(Landroid/view/View;Lco/uk/getmondo/feed/a/a;Lco/uk/getmondo/feed/adapter/a$a;)V

    goto :goto_0

    .line 98
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
