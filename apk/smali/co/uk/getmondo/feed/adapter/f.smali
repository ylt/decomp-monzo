.class public final Lco/uk/getmondo/feed/adapter/f;
.super Landroid/support/v7/widget/RecyclerView$w;
.source "TransactionViewHolder.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000:\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0002\u0008\u0002\u0008\u0000\u0018\u00002\u00020\u0001B\u0017\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0008\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u00a2\u0006\u0002\u0010\u0006J\u000e\u0010\r\u001a\u00020\u000e2\u0006\u0010\u000b\u001a\u00020\u000cJ\u001a\u0010\u000f\u001a\u00020\u000e2\u0008\u0010\u0010\u001a\u0004\u0018\u00010\u00112\u0006\u0010\u0012\u001a\u00020\u0011H\u0002R\u000e\u0010\u0007\u001a\u00020\u0008X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\nX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u000cX\u0082.\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0004\u001a\u0004\u0018\u00010\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0013"
    }
    d2 = {
        "Lco/uk/getmondo/feed/adapter/TransactionViewHolder;",
        "Landroid/support/v7/widget/RecyclerView$ViewHolder;",
        "view",
        "Landroid/view/View;",
        "listener",
        "Lco/uk/getmondo/feed/adapter/FeedAdapter$OnFeedItemActionListener;",
        "(Landroid/view/View;Lco/uk/getmondo/feed/adapter/FeedAdapter$OnFeedItemActionListener;)V",
        "amountFormatter",
        "Lco/uk/getmondo/common/money/AmountFormatter;",
        "avatarGenerator",
        "Lco/uk/getmondo/common/ui/AvatarGenerator;",
        "feedItem",
        "Lco/uk/getmondo/model/FeedItem;",
        "bind",
        "",
        "bindP2pFeedItem",
        "photoUrl",
        "",
        "name",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field private final a:Lco/uk/getmondo/common/i/b;

.field private final b:Lco/uk/getmondo/common/ui/a;

.field private c:Lco/uk/getmondo/d/m;

.field private final d:Lco/uk/getmondo/feed/adapter/a$a;


# direct methods
.method public constructor <init>(Landroid/view/View;Lco/uk/getmondo/feed/adapter/a$a;)V
    .locals 6

    .prologue
    const/4 v2, 0x0

    const-string v0, "view"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 23
    invoke-direct {p0, p1}, Landroid/support/v7/widget/RecyclerView$w;-><init>(Landroid/view/View;)V

    iput-object p2, p0, Lco/uk/getmondo/feed/adapter/f;->d:Lco/uk/getmondo/feed/adapter/a$a;

    .line 24
    new-instance v0, Lco/uk/getmondo/common/i/b;

    const/4 v1, 0x1

    const/4 v4, 0x6

    const/4 v5, 0x0

    move v3, v2

    invoke-direct/range {v0 .. v5}, Lco/uk/getmondo/common/i/b;-><init>(ZZZILkotlin/d/b/i;)V

    iput-object v0, p0, Lco/uk/getmondo/feed/adapter/f;->a:Lco/uk/getmondo/common/i/b;

    .line 25
    sget-object v0, Lco/uk/getmondo/common/ui/a;->a:Lco/uk/getmondo/common/ui/a$a;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "view.context"

    invoke-static {v1, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lco/uk/getmondo/common/ui/a$a;->a(Landroid/content/Context;)Lco/uk/getmondo/common/ui/a;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/feed/adapter/f;->b:Lco/uk/getmondo/common/ui/a;

    .line 30
    new-instance v0, Lco/uk/getmondo/feed/adapter/f$1;

    invoke-direct {v0, p0}, Lco/uk/getmondo/feed/adapter/f$1;-><init>(Lco/uk/getmondo/feed/adapter/f;)V

    check-cast v0, Landroid/view/View$OnClickListener;

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 36
    iget-object v0, p0, Lco/uk/getmondo/feed/adapter/f;->itemView:Landroid/view/View;

    sget v1, Lco/uk/getmondo/c$a;->overflowButton:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    new-instance v1, Lco/uk/getmondo/feed/adapter/f$2;

    invoke-direct {v1, p0}, Lco/uk/getmondo/feed/adapter/f$2;-><init>(Lco/uk/getmondo/feed/adapter/f;)V

    check-cast v1, Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public static final synthetic a(Lco/uk/getmondo/feed/adapter/f;)Lco/uk/getmondo/feed/adapter/a$a;
    .locals 1

    .prologue
    .line 20
    iget-object v0, p0, Lco/uk/getmondo/feed/adapter/f;->d:Lco/uk/getmondo/feed/adapter/a$a;

    return-object v0
.end method

.method private final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 7

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 139
    move-object v0, p1

    check-cast v0, Ljava/lang/CharSequence;

    if-eqz v0, :cond_0

    invoke-static {v0}, Lkotlin/h/j;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    if-nez v0, :cond_2

    .line 140
    iget-object v0, p0, Lco/uk/getmondo/feed/adapter/f;->itemView:Landroid/view/View;

    sget v2, Lco/uk/getmondo/c$a;->merchantIconView:I

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/bumptech/glide/g;->b(Landroid/content/Context;)Lcom/bumptech/glide/j;

    move-result-object v0

    .line 141
    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/bumptech/glide/j;->a(Landroid/net/Uri;)Lcom/bumptech/glide/d;

    move-result-object v0

    .line 142
    invoke-virtual {v0}, Lcom/bumptech/glide/d;->h()Lcom/bumptech/glide/b;

    move-result-object v0

    .line 144
    sget-object v2, Lcom/bumptech/glide/load/engine/b;->b:Lcom/bumptech/glide/load/engine/b;

    invoke-virtual {v0, v2}, Lcom/bumptech/glide/b;->a(Lcom/bumptech/glide/load/engine/b;)Lcom/bumptech/glide/a;

    move-result-object v0

    .line 145
    invoke-virtual {v0}, Lcom/bumptech/glide/a;->a()Lcom/bumptech/glide/a;

    move-result-object v3

    .line 146
    new-instance v2, Lco/uk/getmondo/common/ui/c;

    iget-object v0, p0, Lco/uk/getmondo/feed/adapter/f;->itemView:Landroid/view/View;

    sget v4, Lco/uk/getmondo/c$a;->merchantIconView:I

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    invoke-direct {v2, v0}, Lco/uk/getmondo/common/ui/c;-><init>(Landroid/widget/ImageView;)V

    move-object v0, v2

    check-cast v0, Lcom/bumptech/glide/g/b/j;

    invoke-virtual {v3, v0}, Lcom/bumptech/glide/a;->a(Lcom/bumptech/glide/g/b/j;)Lcom/bumptech/glide/g/b/j;

    .line 149
    :goto_1
    iget-object v0, p0, Lco/uk/getmondo/feed/adapter/f;->itemView:Landroid/view/View;

    sget v2, Lco/uk/getmondo/c$a;->merchantIconView:I

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 151
    iget-object v0, p0, Lco/uk/getmondo/feed/adapter/f;->itemView:Landroid/view/View;

    sget v2, Lco/uk/getmondo/c$a;->merchantIconView:I

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setClipToOutline(Z)V

    .line 152
    iget-object v0, p0, Lco/uk/getmondo/feed/adapter/f;->itemView:Landroid/view/View;

    sget v1, Lco/uk/getmondo/c$a;->transactionDescriptionTextView:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    check-cast p2, Ljava/lang/CharSequence;

    invoke-virtual {v0, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 153
    return-void

    :cond_1
    move v0, v1

    .line 139
    goto :goto_0

    .line 148
    :cond_2
    iget-object v0, p0, Lco/uk/getmondo/feed/adapter/f;->itemView:Landroid/view/View;

    sget v3, Lco/uk/getmondo/c$a;->merchantIconView:I

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Landroid/widget/ImageView;

    iget-object v0, p0, Lco/uk/getmondo/feed/adapter/f;->b:Lco/uk/getmondo/common/ui/a;

    invoke-virtual {v0, p2}, Lco/uk/getmondo/common/ui/a;->a(Ljava/lang/String;)Lco/uk/getmondo/common/ui/a$b;

    move-result-object v0

    const/4 v4, 0x7

    move v3, v1

    move-object v5, v2

    invoke-static/range {v0 .. v5}, Lco/uk/getmondo/common/ui/a$b;->a(Lco/uk/getmondo/common/ui/a$b;ILandroid/graphics/Typeface;ZILjava/lang/Object;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v6, v0}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_1
.end method

.method public static final synthetic b(Lco/uk/getmondo/feed/adapter/f;)Lco/uk/getmondo/d/m;
    .locals 2

    .prologue
    .line 20
    iget-object v0, p0, Lco/uk/getmondo/feed/adapter/f;->c:Lco/uk/getmondo/d/m;

    if-nez v0, :cond_0

    const-string v1, "feedItem"

    invoke-static {v1}, Lkotlin/d/b/l;->b(Ljava/lang/String;)V

    :cond_0
    return-object v0
.end method


# virtual methods
.method public final a(Lco/uk/getmondo/d/m;)V
    .locals 12

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    const-string v0, "feedItem"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 42
    iput-object p1, p0, Lco/uk/getmondo/feed/adapter/f;->c:Lco/uk/getmondo/d/m;

    .line 44
    invoke-virtual {p1}, Lco/uk/getmondo/d/m;->e()Lcom/c/b/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/c/b/b;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 45
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Trying to bind a FeedItem in TransactionViewHolder, but there\'s no Transaction! Feed item type: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 46
    invoke-virtual {p1}, Lco/uk/getmondo/d/m;->j()Lco/uk/getmondo/feed/a/a/a;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " id: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p1}, Lco/uk/getmondo/d/m;->a()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 45
    invoke-direct {v0, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    invoke-static {v0}, Ld/a/a;->a(Ljava/lang/Throwable;)V

    .line 48
    :cond_0
    invoke-virtual {p1}, Lco/uk/getmondo/d/m;->e()Lcom/c/b/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/c/b/b;->a()Ljava/lang/Object;

    move-result-object v0

    move-object v10, v0

    check-cast v10, Lco/uk/getmondo/d/aj;

    .line 50
    invoke-virtual {v10}, Lco/uk/getmondo/d/aj;->s()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 52
    invoke-virtual {v10}, Lco/uk/getmondo/d/aj;->C()Lco/uk/getmondo/payments/send/data/a/a;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lco/uk/getmondo/payments/send/data/a/a;->a()Ljava/lang/String;

    move-result-object v6

    if-eqz v6, :cond_1

    .line 53
    :goto_0
    iget-object v0, p0, Lco/uk/getmondo/feed/adapter/f;->b:Lco/uk/getmondo/common/ui/a;

    const-string v4, "title"

    invoke-static {v6, v4}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0, v6}, Lco/uk/getmondo/common/ui/a;->a(Ljava/lang/String;)Lco/uk/getmondo/common/ui/a$b;

    move-result-object v0

    const/4 v4, 0x3

    move-object v5, v2

    invoke-static/range {v0 .. v5}, Lco/uk/getmondo/common/ui/a$b;->a(Lco/uk/getmondo/common/ui/a$b;ILandroid/graphics/Typeface;ZILjava/lang/Object;)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 54
    iget-object v0, p0, Lco/uk/getmondo/feed/adapter/f;->itemView:Landroid/view/View;

    sget v4, Lco/uk/getmondo/c$a;->merchantIconView:I

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 55
    iget-object v0, p0, Lco/uk/getmondo/feed/adapter/f;->itemView:Landroid/view/View;

    sget v2, Lco/uk/getmondo/c$a;->transactionDescriptionTextView:I

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    move-object v2, v6

    check-cast v2, Ljava/lang/CharSequence;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 87
    :goto_1
    invoke-virtual {v10}, Lco/uk/getmondo/d/aj;->r()Z

    move-result v0

    if-eqz v0, :cond_d

    .line 90
    iget-object v0, p0, Lco/uk/getmondo/feed/adapter/f;->itemView:Landroid/view/View;

    sget v2, Lco/uk/getmondo/c$a;->merchantIconView:I

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setClipToOutline(Z)V

    .line 91
    iget-object v0, p0, Lco/uk/getmondo/feed/adapter/f;->itemView:Landroid/view/View;

    sget v2, Lco/uk/getmondo/c$a;->merchantIconView:I

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 95
    :goto_2
    invoke-virtual {v10}, Lco/uk/getmondo/d/aj;->t()Z

    move-result v0

    if-eqz v0, :cond_e

    .line 98
    iget-object v0, p0, Lco/uk/getmondo/feed/adapter/f;->itemView:Landroid/view/View;

    sget v2, Lco/uk/getmondo/c$a;->amountView:I

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/common/ui/AmountView;

    const v2, 0x7f0c00b9

    invoke-virtual {v10}, Lco/uk/getmondo/d/aj;->g()Lco/uk/getmondo/d/c;

    move-result-object v3

    const-string v4, "transaction.amount"

    invoke-static {v3, v4}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0, v2, v3}, Lco/uk/getmondo/common/ui/AmountView;->a(ILco/uk/getmondo/d/c;)V

    .line 101
    :goto_3
    invoke-virtual {v10}, Lco/uk/getmondo/d/aj;->d()Lcom/c/b/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/c/b/b;->b()Z

    move-result v0

    if-eqz v0, :cond_f

    .line 104
    iget-object v0, p0, Lco/uk/getmondo/feed/adapter/f;->itemView:Landroid/view/View;

    sget v2, Lco/uk/getmondo/c$a;->declinedView:I

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v10}, Lco/uk/getmondo/d/aj;->d()Lcom/c/b/b;

    move-result-object v2

    invoke-virtual {v2}, Lcom/c/b/b;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/CharSequence;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 105
    iget-object v0, p0, Lco/uk/getmondo/feed/adapter/f;->itemView:Landroid/view/View;

    sget v2, Lco/uk/getmondo/c$a;->declinedView:I

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-static {v0}, Lco/uk/getmondo/common/ae;->a(Landroid/view/View;)V

    .line 108
    :goto_4
    invoke-virtual {v10}, Lco/uk/getmondo/d/aj;->m()Z

    move-result v0

    if-eqz v0, :cond_10

    iget-object v0, p0, Lco/uk/getmondo/feed/adapter/f;->itemView:Landroid/view/View;

    sget v2, Lco/uk/getmondo/c$a;->declinedView:I

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_10

    .line 110
    iget-object v0, p0, Lco/uk/getmondo/feed/adapter/f;->itemView:Landroid/view/View;

    sget v2, Lco/uk/getmondo/c$a;->notesView:I

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v10}, Lco/uk/getmondo/d/aj;->e()Ljava/lang/String;

    move-result-object v2

    check-cast v2, Ljava/lang/CharSequence;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 111
    iget-object v0, p0, Lco/uk/getmondo/feed/adapter/f;->itemView:Landroid/view/View;

    sget v2, Lco/uk/getmondo/c$a;->notesView:I

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-static {v0}, Lco/uk/getmondo/common/ae;->a(Landroid/view/View;)V

    .line 114
    :goto_5
    invoke-virtual {v10}, Lco/uk/getmondo/d/aj;->y()Z

    move-result v0

    if-eqz v0, :cond_11

    .line 117
    iget-object v0, p0, Lco/uk/getmondo/feed/adapter/f;->itemView:Landroid/view/View;

    sget v2, Lco/uk/getmondo/c$a;->amountContainer:I

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    invoke-static {v0}, Lco/uk/getmondo/common/ae;->b(Landroid/view/View;)V

    .line 120
    :goto_6
    invoke-virtual {v10}, Lco/uk/getmondo/d/aj;->i()Z

    move-result v0

    if-eqz v0, :cond_12

    .line 122
    iget-object v0, p0, Lco/uk/getmondo/feed/adapter/f;->itemView:Landroid/view/View;

    sget v2, Lco/uk/getmondo/c$a;->localAmountView:I

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-static {v0}, Lco/uk/getmondo/common/ae;->a(Landroid/view/View;)V

    .line 123
    iget-object v0, p0, Lco/uk/getmondo/feed/adapter/f;->itemView:Landroid/view/View;

    sget v2, Lco/uk/getmondo/c$a;->localAmountView:I

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v2, p0, Lco/uk/getmondo/feed/adapter/f;->a:Lco/uk/getmondo/common/i/b;

    invoke-virtual {v10}, Lco/uk/getmondo/d/aj;->h()Lco/uk/getmondo/d/c;

    move-result-object v3

    const-string v4, "transaction.localAmount"

    invoke-static {v3, v4}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v2, v3}, Lco/uk/getmondo/common/i/b;->a(Lco/uk/getmondo/d/c;)Ljava/lang/String;

    move-result-object v2

    check-cast v2, Ljava/lang/CharSequence;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 126
    :goto_7
    invoke-virtual {p1}, Lco/uk/getmondo/d/m;->k()Z

    move-result v0

    if-eqz v0, :cond_14

    .line 129
    iget-object v0, p0, Lco/uk/getmondo/feed/adapter/f;->itemView:Landroid/view/View;

    sget v2, Lco/uk/getmondo/c$a;->overflowButton:I

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    invoke-static {v0}, Lco/uk/getmondo/common/ae;->a(Landroid/view/View;)V

    .line 130
    iget-object v0, p0, Lco/uk/getmondo/feed/adapter/f;->itemView:Landroid/view/View;

    sget v2, Lco/uk/getmondo/c$a;->amountContainer:I

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    if-nez v0, :cond_13

    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type android.view.ViewGroup.MarginLayoutParams"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 52
    :cond_1
    invoke-virtual {v10}, Lco/uk/getmondo/d/aj;->x()Ljava/lang/String;

    move-result-object v6

    goto/16 :goto_0

    .line 57
    :cond_2
    invoke-virtual {v10}, Lco/uk/getmondo/d/aj;->r()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 58
    invoke-virtual {v10}, Lco/uk/getmondo/d/aj;->B()Lco/uk/getmondo/d/aa;

    move-result-object v0

    invoke-virtual {v0}, Lco/uk/getmondo/d/aa;->b()Ljava/lang/String;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-static {v0}, Lkotlin/h/j;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 59
    invoke-virtual {v10}, Lco/uk/getmondo/d/aj;->C()Lco/uk/getmondo/payments/send/data/a/a;

    move-result-object v0

    if-eqz v0, :cond_4

    invoke-virtual {v0}, Lco/uk/getmondo/payments/send/data/a/a;->b()Ljava/lang/String;

    move-result-object v0

    :goto_8
    if-nez v0, :cond_3

    invoke-static {}, Lkotlin/d/b/l;->a()V

    :cond_3
    invoke-static {v0}, Lco/uk/getmondo/common/k/o;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 60
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, ""

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, " \u2022 "

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v10}, Lco/uk/getmondo/d/aj;->C()Lco/uk/getmondo/payments/send/data/a/a;

    move-result-object v0

    if-eqz v0, :cond_5

    invoke-virtual {v0}, Lco/uk/getmondo/payments/send/data/a/a;->c()Ljava/lang/String;

    move-result-object v0

    :goto_9
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    .line 64
    :goto_a
    iget-object v0, p0, Lco/uk/getmondo/feed/adapter/f;->itemView:Landroid/view/View;

    sget v4, Lco/uk/getmondo/c$a;->merchantIconView:I

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iget-object v4, p0, Lco/uk/getmondo/feed/adapter/f;->b:Lco/uk/getmondo/common/ui/a;

    invoke-virtual {v4, v11}, Lco/uk/getmondo/common/ui/a;->a(Ljava/lang/String;)Lco/uk/getmondo/common/ui/a$b;

    move-result-object v4

    const/4 v8, 0x7

    move v5, v1

    move-object v6, v2

    move v7, v1

    move-object v9, v2

    invoke-static/range {v4 .. v9}, Lco/uk/getmondo/common/ui/a$b;->a(Lco/uk/getmondo/common/ui/a$b;ILandroid/graphics/Typeface;ZILjava/lang/Object;)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 65
    iget-object v0, p0, Lco/uk/getmondo/feed/adapter/f;->itemView:Landroid/view/View;

    sget v2, Lco/uk/getmondo/c$a;->transactionDescriptionTextView:I

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    move-object v2, v11

    check-cast v2, Ljava/lang/CharSequence;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    :cond_4
    move-object v0, v2

    .line 59
    goto :goto_8

    :cond_5
    move-object v0, v2

    .line 60
    goto :goto_9

    .line 62
    :cond_6
    invoke-virtual {v10}, Lco/uk/getmondo/d/aj;->B()Lco/uk/getmondo/d/aa;

    move-result-object v0

    invoke-virtual {v0}, Lco/uk/getmondo/d/aa;->b()Ljava/lang/String;

    move-result-object v11

    const-string v0, "transaction.peer.name"

    invoke-static {v11, v0}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_a

    .line 67
    :cond_7
    invoke-virtual {v10}, Lco/uk/getmondo/d/aj;->t()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 68
    iget-object v0, p0, Lco/uk/getmondo/feed/adapter/f;->itemView:Landroid/view/View;

    sget v2, Lco/uk/getmondo/c$a;->transactionDescriptionTextView:I

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v10}, Lco/uk/getmondo/d/aj;->x()Ljava/lang/String;

    move-result-object v2

    check-cast v2, Ljava/lang/CharSequence;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 69
    iget-object v0, p0, Lco/uk/getmondo/feed/adapter/f;->itemView:Landroid/view/View;

    sget v2, Lco/uk/getmondo/c$a;->merchantIconView:I

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    const v2, 0x7f0201de

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_1

    .line 71
    :cond_8
    invoke-virtual {v10}, Lco/uk/getmondo/d/aj;->q()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 72
    invoke-virtual {v10}, Lco/uk/getmondo/d/aj;->B()Lco/uk/getmondo/d/aa;

    move-result-object v0

    invoke-virtual {v0}, Lco/uk/getmondo/d/aa;->g()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v10}, Lco/uk/getmondo/d/aj;->B()Lco/uk/getmondo/d/aa;

    move-result-object v2

    invoke-virtual {v2}, Lco/uk/getmondo/d/aa;->b()Ljava/lang/String;

    move-result-object v2

    const-string v4, "transaction.peer.name"

    invoke-static {v2, v4}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, v0, v2}, Lco/uk/getmondo/feed/adapter/f;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 75
    :cond_9
    invoke-virtual {v10}, Lco/uk/getmondo/d/aj;->n()Z

    move-result v0

    if-eqz v0, :cond_b

    .line 81
    iget-object v0, p0, Lco/uk/getmondo/feed/adapter/f;->itemView:Landroid/view/View;

    sget v2, Lco/uk/getmondo/c$a;->merchantIconView:I

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/bumptech/glide/g;->b(Landroid/content/Context;)Lcom/bumptech/glide/j;

    move-result-object v0

    .line 77
    invoke-virtual {v10}, Lco/uk/getmondo/d/aj;->f()Lco/uk/getmondo/d/u;

    move-result-object v2

    if-nez v2, :cond_a

    invoke-static {}, Lkotlin/d/b/l;->a()V

    :cond_a
    invoke-virtual {v2}, Lco/uk/getmondo/d/u;->j()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/bumptech/glide/j;->a(Ljava/lang/String;)Lcom/bumptech/glide/d;

    move-result-object v0

    .line 78
    invoke-virtual {v0}, Lcom/bumptech/glide/d;->a()Lcom/bumptech/glide/c;

    move-result-object v0

    .line 79
    invoke-virtual {v10}, Lco/uk/getmondo/d/aj;->c()Lco/uk/getmondo/d/h;

    move-result-object v2

    invoke-virtual {v2}, Lco/uk/getmondo/d/h;->c()I

    move-result v2

    invoke-virtual {v0, v2}, Lcom/bumptech/glide/c;->a(I)Lcom/bumptech/glide/c;

    move-result-object v0

    .line 80
    invoke-virtual {v0}, Lcom/bumptech/glide/c;->c()Lcom/bumptech/glide/c;

    move-result-object v2

    .line 81
    iget-object v0, p0, Lco/uk/getmondo/feed/adapter/f;->itemView:Landroid/view/View;

    sget v4, Lco/uk/getmondo/c$a;->merchantIconView:I

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    invoke-virtual {v2, v0}, Lcom/bumptech/glide/c;->a(Landroid/widget/ImageView;)Lcom/bumptech/glide/g/b/j;

    .line 84
    :goto_b
    iget-object v0, p0, Lco/uk/getmondo/feed/adapter/f;->itemView:Landroid/view/View;

    sget v2, Lco/uk/getmondo/c$a;->transactionDescriptionTextView:I

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v10}, Lco/uk/getmondo/d/aj;->x()Ljava/lang/String;

    move-result-object v2

    check-cast v2, Ljava/lang/CharSequence;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 82
    :cond_b
    invoke-virtual {v10}, Lco/uk/getmondo/d/aj;->z()Z

    move-result v0

    if-eqz v0, :cond_c

    iget-object v0, p0, Lco/uk/getmondo/feed/adapter/f;->itemView:Landroid/view/View;

    sget v2, Lco/uk/getmondo/c$a;->merchantIconView:I

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    const v2, 0x7f02015a

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_b

    .line 83
    :cond_c
    iget-object v0, p0, Lco/uk/getmondo/feed/adapter/f;->itemView:Landroid/view/View;

    sget v2, Lco/uk/getmondo/c$a;->merchantIconView:I

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    invoke-virtual {v10}, Lco/uk/getmondo/d/aj;->c()Lco/uk/getmondo/d/h;

    move-result-object v2

    invoke-virtual {v2}, Lco/uk/getmondo/d/h;->c()I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_b

    .line 93
    :cond_d
    iget-object v0, p0, Lco/uk/getmondo/feed/adapter/f;->itemView:Landroid/view/View;

    sget v2, Lco/uk/getmondo/c$a;->merchantIconView:I

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setClipToOutline(Z)V

    .line 94
    iget-object v0, p0, Lco/uk/getmondo/feed/adapter/f;->itemView:Landroid/view/View;

    sget v2, Lco/uk/getmondo/c$a;->merchantIconView:I

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    const v2, 0x7f0201f8

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    goto/16 :goto_2

    .line 100
    :cond_e
    iget-object v0, p0, Lco/uk/getmondo/feed/adapter/f;->itemView:Landroid/view/View;

    sget v2, Lco/uk/getmondo/c$a;->amountView:I

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/common/ui/AmountView;

    const v2, 0x7f0c00b8

    invoke-virtual {v10}, Lco/uk/getmondo/d/aj;->g()Lco/uk/getmondo/d/c;

    move-result-object v3

    const-string v4, "transaction.amount"

    invoke-static {v3, v4}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0, v2, v3}, Lco/uk/getmondo/common/ui/AmountView;->a(ILco/uk/getmondo/d/c;)V

    goto/16 :goto_3

    .line 107
    :cond_f
    iget-object v0, p0, Lco/uk/getmondo/feed/adapter/f;->itemView:Landroid/view/View;

    sget v2, Lco/uk/getmondo/c$a;->declinedView:I

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-static {v0}, Lco/uk/getmondo/common/ae;->b(Landroid/view/View;)V

    goto/16 :goto_4

    .line 113
    :cond_10
    iget-object v0, p0, Lco/uk/getmondo/feed/adapter/f;->itemView:Landroid/view/View;

    sget v2, Lco/uk/getmondo/c$a;->notesView:I

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-static {v0}, Lco/uk/getmondo/common/ae;->b(Landroid/view/View;)V

    goto/16 :goto_5

    .line 119
    :cond_11
    iget-object v0, p0, Lco/uk/getmondo/feed/adapter/f;->itemView:Landroid/view/View;

    sget v2, Lco/uk/getmondo/c$a;->amountContainer:I

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    invoke-static {v0}, Lco/uk/getmondo/common/ae;->a(Landroid/view/View;)V

    goto/16 :goto_6

    .line 125
    :cond_12
    iget-object v0, p0, Lco/uk/getmondo/feed/adapter/f;->itemView:Landroid/view/View;

    sget v2, Lco/uk/getmondo/c$a;->localAmountView:I

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-static {v0}, Lco/uk/getmondo/common/ae;->b(Landroid/view/View;)V

    goto/16 :goto_7

    .line 130
    :cond_13
    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup$MarginLayoutParams;->setMarginEnd(I)V

    .line 135
    :goto_c
    return-void

    .line 132
    :cond_14
    iget-object v0, p0, Lco/uk/getmondo/feed/adapter/f;->itemView:Landroid/view/View;

    sget v1, Lco/uk/getmondo/c$a;->overflowButton:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    invoke-static {v0}, Lco/uk/getmondo/common/ae;->b(Landroid/view/View;)V

    .line 133
    iget-object v0, p0, Lco/uk/getmondo/feed/adapter/f;->itemView:Landroid/view/View;

    sget v1, Lco/uk/getmondo/c$a;->amountContainer:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0113

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 134
    iget-object v0, p0, Lco/uk/getmondo/feed/adapter/f;->itemView:Landroid/view/View;

    sget v2, Lco/uk/getmondo/c$a;->amountContainer:I

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    if-nez v0, :cond_15

    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type android.view.ViewGroup.MarginLayoutParams"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_15
    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup$MarginLayoutParams;->setMarginEnd(I)V

    goto :goto_c
.end method
