.class final Lco/uk/getmondo/feed/g$r;
.super Ljava/lang/Object;
.source "HomeFeedPresenter.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lco/uk/getmondo/feed/g;->a(Lco/uk/getmondo/feed/g$a;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h",
        "<",
        "Lco/uk/getmondo/d/m;",
        "Lio/reactivex/d;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\n \u0002*\u0004\u0018\u00010\u00010\u00012\u0006\u0010\u0003\u001a\u00020\u0004H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "Lio/reactivex/Completable;",
        "kotlin.jvm.PlatformType",
        "kycFeedItem",
        "Lco/uk/getmondo/model/FeedItem;",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field final synthetic a:Lco/uk/getmondo/feed/g;


# direct methods
.method constructor <init>(Lco/uk/getmondo/feed/g;)V
    .locals 0

    iput-object p1, p0, Lco/uk/getmondo/feed/g$r;->a:Lco/uk/getmondo/feed/g;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lco/uk/getmondo/d/m;)Lio/reactivex/b;
    .locals 2

    .prologue
    const-string v0, "kycFeedItem"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 60
    iget-object v0, p0, Lco/uk/getmondo/feed/g$r;->a:Lco/uk/getmondo/feed/g;

    invoke-static {v0}, Lco/uk/getmondo/feed/g;->a(Lco/uk/getmondo/feed/g;)Lco/uk/getmondo/feed/a/d;

    move-result-object v0

    invoke-virtual {p1}, Lco/uk/getmondo/d/m;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lco/uk/getmondo/feed/a/d;->b(Ljava/lang/String;)Lio/reactivex/b;

    move-result-object v1

    .line 61
    new-instance v0, Lco/uk/getmondo/feed/g$r$1;

    invoke-direct {v0, p0}, Lco/uk/getmondo/feed/g$r$1;-><init>(Lco/uk/getmondo/feed/g$r;)V

    check-cast v0, Lio/reactivex/c/a;

    invoke-virtual {v1, v0}, Lio/reactivex/b;->b(Lio/reactivex/c/a;)Lio/reactivex/b;

    move-result-object v0

    .line 62
    iget-object v1, p0, Lco/uk/getmondo/feed/g$r;->a:Lco/uk/getmondo/feed/g;

    invoke-static {v1}, Lco/uk/getmondo/feed/g;->c(Lco/uk/getmondo/feed/g;)Lio/reactivex/u;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/b;->b(Lio/reactivex/u;)Lio/reactivex/b;

    move-result-object v1

    .line 63
    sget-object v0, Lco/uk/getmondo/feed/g$r$2;->a:Lco/uk/getmondo/feed/g$r$2;

    check-cast v0, Lio/reactivex/c/g;

    invoke-virtual {v1, v0}, Lio/reactivex/b;->a(Lio/reactivex/c/g;)Lio/reactivex/b;

    move-result-object v0

    .line 64
    invoke-virtual {v0}, Lio/reactivex/b;->b()Lio/reactivex/b;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 26
    check-cast p1, Lco/uk/getmondo/d/m;

    invoke-virtual {p0, p1}, Lco/uk/getmondo/feed/g$r;->a(Lco/uk/getmondo/d/m;)Lio/reactivex/b;

    move-result-object v0

    return-object v0
.end method
