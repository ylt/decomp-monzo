.class public final Lco/uk/getmondo/feed/g;
.super Lco/uk/getmondo/common/ui/b;
.source "HomeFeedPresenter.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lco/uk/getmondo/feed/g$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lco/uk/getmondo/common/ui/b",
        "<",
        "Lco/uk/getmondo/feed/g$a;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000V\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0003\u0008\u0007\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00020\u0001:\u0001\u001cBS\u0008\u0007\u0012\u0008\u0008\u0001\u0010\u0003\u001a\u00020\u0004\u0012\u0008\u0008\u0001\u0010\u0005\u001a\u00020\u0004\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b\u0012\u0006\u0010\u000c\u001a\u00020\r\u0012\u0006\u0010\u000e\u001a\u00020\u000f\u0012\u0006\u0010\u0010\u001a\u00020\u0011\u0012\u0006\u0010\u0012\u001a\u00020\u0013\u00a2\u0006\u0002\u0010\u0014J\u0010\u0010\u0019\u001a\u00020\u001a2\u0006\u0010\u001b\u001a\u00020\u0002H\u0016R\u000e\u0010\u0012\u001a\u00020\u0013X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0015\u001a\u00020\u0016X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0010\u001a\u00020\u0011X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u000fX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0017\u001a\u00020\u0018X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000c\u001a\u00020\rX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001d"
    }
    d2 = {
        "Lco/uk/getmondo/feed/HomeFeedPresenter;",
        "Lco/uk/getmondo/common/ui/BasePresenter;",
        "Lco/uk/getmondo/feed/HomeFeedPresenter$View;",
        "uiScheduler",
        "Lio/reactivex/Scheduler;",
        "ioScheduler",
        "apiErrorHandler",
        "Lco/uk/getmondo/common/errors/ApiErrorHandler;",
        "deleteFeedItemStorage",
        "Lco/uk/getmondo/common/DeleteFeedItemStorage;",
        "feedManager",
        "Lco/uk/getmondo/feed/data/FeedManager;",
        "syncManager",
        "Lco/uk/getmondo/background_sync/SyncManager;",
        "cardManager",
        "Lco/uk/getmondo/card/CardManager;",
        "balanceRepository",
        "Lco/uk/getmondo/account_balance/BalanceRepository;",
        "accountManager",
        "Lco/uk/getmondo/common/accounts/AccountManager;",
        "(Lio/reactivex/Scheduler;Lio/reactivex/Scheduler;Lco/uk/getmondo/common/errors/ApiErrorHandler;Lco/uk/getmondo/common/DeleteFeedItemStorage;Lco/uk/getmondo/feed/data/FeedManager;Lco/uk/getmondo/background_sync/SyncManager;Lco/uk/getmondo/card/CardManager;Lco/uk/getmondo/account_balance/BalanceRepository;Lco/uk/getmondo/common/accounts/AccountManager;)V",
        "amountFormatter",
        "Lco/uk/getmondo/common/money/AmountFormatter;",
        "localSpendComparator",
        "Lco/uk/getmondo/account_balance/LocalSpendComparator;",
        "register",
        "",
        "view",
        "View",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field private final c:Lco/uk/getmondo/common/i/b;

.field private final d:Lco/uk/getmondo/a/k;

.field private final e:Lio/reactivex/u;

.field private final f:Lio/reactivex/u;

.field private final g:Lco/uk/getmondo/common/e/a;

.field private final h:Lco/uk/getmondo/common/i;

.field private final i:Lco/uk/getmondo/feed/a/d;

.field private final j:Lco/uk/getmondo/background_sync/d;

.field private final k:Lco/uk/getmondo/card/c;

.field private final l:Lco/uk/getmondo/a/a;

.field private final m:Lco/uk/getmondo/common/accounts/b;


# direct methods
.method public constructor <init>(Lio/reactivex/u;Lio/reactivex/u;Lco/uk/getmondo/common/e/a;Lco/uk/getmondo/common/i;Lco/uk/getmondo/feed/a/d;Lco/uk/getmondo/background_sync/d;Lco/uk/getmondo/card/c;Lco/uk/getmondo/a/a;Lco/uk/getmondo/common/accounts/b;)V
    .locals 6

    .prologue
    const/4 v2, 0x0

    const-string v0, "uiScheduler"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "ioScheduler"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "apiErrorHandler"

    invoke-static {p3, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "deleteFeedItemStorage"

    invoke-static {p4, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "feedManager"

    invoke-static {p5, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "syncManager"

    invoke-static {p6, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "cardManager"

    invoke-static {p7, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "balanceRepository"

    invoke-static {p8, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "accountManager"

    invoke-static {p9, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 37
    invoke-direct {p0}, Lco/uk/getmondo/common/ui/b;-><init>()V

    iput-object p1, p0, Lco/uk/getmondo/feed/g;->e:Lio/reactivex/u;

    iput-object p2, p0, Lco/uk/getmondo/feed/g;->f:Lio/reactivex/u;

    iput-object p3, p0, Lco/uk/getmondo/feed/g;->g:Lco/uk/getmondo/common/e/a;

    iput-object p4, p0, Lco/uk/getmondo/feed/g;->h:Lco/uk/getmondo/common/i;

    iput-object p5, p0, Lco/uk/getmondo/feed/g;->i:Lco/uk/getmondo/feed/a/d;

    iput-object p6, p0, Lco/uk/getmondo/feed/g;->j:Lco/uk/getmondo/background_sync/d;

    iput-object p7, p0, Lco/uk/getmondo/feed/g;->k:Lco/uk/getmondo/card/c;

    iput-object p8, p0, Lco/uk/getmondo/feed/g;->l:Lco/uk/getmondo/a/a;

    iput-object p9, p0, Lco/uk/getmondo/feed/g;->m:Lco/uk/getmondo/common/accounts/b;

    .line 39
    new-instance v0, Lco/uk/getmondo/common/i/b;

    const/4 v1, 0x1

    const/4 v4, 0x6

    const/4 v5, 0x0

    move v3, v2

    invoke-direct/range {v0 .. v5}, Lco/uk/getmondo/common/i/b;-><init>(ZZZILkotlin/d/b/i;)V

    iput-object v0, p0, Lco/uk/getmondo/feed/g;->c:Lco/uk/getmondo/common/i/b;

    .line 40
    new-instance v0, Lco/uk/getmondo/a/k;

    invoke-direct {v0}, Lco/uk/getmondo/a/k;-><init>()V

    iput-object v0, p0, Lco/uk/getmondo/feed/g;->d:Lco/uk/getmondo/a/k;

    return-void
.end method

.method public static final synthetic a(Lco/uk/getmondo/feed/g;)Lco/uk/getmondo/feed/a/d;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lco/uk/getmondo/feed/g;->i:Lco/uk/getmondo/feed/a/d;

    return-object v0
.end method

.method public static final synthetic b(Lco/uk/getmondo/feed/g;)Lco/uk/getmondo/common/i;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lco/uk/getmondo/feed/g;->h:Lco/uk/getmondo/common/i;

    return-object v0
.end method

.method public static final synthetic c(Lco/uk/getmondo/feed/g;)Lio/reactivex/u;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lco/uk/getmondo/feed/g;->f:Lio/reactivex/u;

    return-object v0
.end method

.method public static final synthetic d(Lco/uk/getmondo/feed/g;)Lco/uk/getmondo/background_sync/d;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lco/uk/getmondo/feed/g;->j:Lco/uk/getmondo/background_sync/d;

    return-object v0
.end method

.method public static final synthetic e(Lco/uk/getmondo/feed/g;)Lio/reactivex/u;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lco/uk/getmondo/feed/g;->e:Lio/reactivex/u;

    return-object v0
.end method

.method public static final synthetic f(Lco/uk/getmondo/feed/g;)Lco/uk/getmondo/common/e/a;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lco/uk/getmondo/feed/g;->g:Lco/uk/getmondo/common/e/a;

    return-object v0
.end method

.method public static final synthetic g(Lco/uk/getmondo/feed/g;)Lco/uk/getmondo/a/a;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lco/uk/getmondo/feed/g;->l:Lco/uk/getmondo/a/a;

    return-object v0
.end method

.method public static final synthetic h(Lco/uk/getmondo/feed/g;)Lco/uk/getmondo/a/k;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lco/uk/getmondo/feed/g;->d:Lco/uk/getmondo/a/k;

    return-object v0
.end method

.method public static final synthetic i(Lco/uk/getmondo/feed/g;)Lco/uk/getmondo/common/i/b;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lco/uk/getmondo/feed/g;->c:Lco/uk/getmondo/common/i/b;

    return-object v0
.end method

.method public static final synthetic j(Lco/uk/getmondo/feed/g;)Lco/uk/getmondo/common/accounts/b;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lco/uk/getmondo/feed/g;->m:Lco/uk/getmondo/common/accounts/b;

    return-object v0
.end method


# virtual methods
.method public bridge synthetic a(Lco/uk/getmondo/common/ui/f;)V
    .locals 0

    .prologue
    .line 26
    check-cast p1, Lco/uk/getmondo/feed/g$a;

    invoke-virtual {p0, p1}, Lco/uk/getmondo/feed/g;->a(Lco/uk/getmondo/feed/g$a;)V

    return-void
.end method

.method public a(Lco/uk/getmondo/feed/g$a;)V
    .locals 4

    .prologue
    const-string v0, "view"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v0, p1

    .line 43
    check-cast v0, Lco/uk/getmondo/common/ui/f;

    invoke-super {p0, v0}, Lco/uk/getmondo/common/ui/b;->a(Lco/uk/getmondo/common/ui/f;)V

    .line 45
    iget-object v2, p0, Lco/uk/getmondo/feed/g;->b:Lio/reactivex/b/a;

    .line 47
    iget-object v0, p0, Lco/uk/getmondo/feed/g;->i:Lco/uk/getmondo/feed/a/d;

    invoke-virtual {v0}, Lco/uk/getmondo/feed/a/d;->a()Lio/reactivex/n;

    move-result-object v0

    .line 46
    iget-object v1, p0, Lco/uk/getmondo/feed/g;->e:Lio/reactivex/u;

    invoke-virtual {v0, v1}, Lio/reactivex/n;->observeOn(Lio/reactivex/u;)Lio/reactivex/n;

    move-result-object v3

    .line 47
    new-instance v0, Lco/uk/getmondo/feed/g$b;

    invoke-direct {v0, p0, p1}, Lco/uk/getmondo/feed/g$b;-><init>(Lco/uk/getmondo/feed/g;Lco/uk/getmondo/feed/g$a;)V

    check-cast v0, Lio/reactivex/c/g;

    .line 55
    sget-object v1, Lco/uk/getmondo/feed/g$m;->a:Lco/uk/getmondo/feed/g$m;

    check-cast v1, Lio/reactivex/c/g;

    .line 47
    invoke-virtual {v3, v0, v1}, Lio/reactivex/n;->subscribe(Lio/reactivex/c/g;Lio/reactivex/c/g;)Lio/reactivex/b/b;

    move-result-object v0

    const-string v1, "feedManager.feedItems()\n\u2026     }, { Timber.e(it) })"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 45
    invoke-static {v2, v0}, Lco/uk/getmondo/common/j/f;->a(Lio/reactivex/b/a;Lio/reactivex/b/b;)Lio/reactivex/b/a;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/feed/g;->b:Lio/reactivex/b/a;

    .line 57
    iget-object v2, p0, Lco/uk/getmondo/feed/g;->b:Lio/reactivex/b/a;

    .line 66
    iget-object v0, p0, Lco/uk/getmondo/feed/g;->i:Lco/uk/getmondo/feed/a/d;

    invoke-virtual {v0}, Lco/uk/getmondo/feed/a/d;->b()Lio/reactivex/n;

    move-result-object v1

    .line 58
    new-instance v0, Lco/uk/getmondo/feed/g$q;

    invoke-direct {v0, p0}, Lco/uk/getmondo/feed/g$q;-><init>(Lco/uk/getmondo/feed/g;)V

    check-cast v0, Lio/reactivex/c/q;

    invoke-virtual {v1, v0}, Lio/reactivex/n;->filter(Lio/reactivex/c/q;)Lio/reactivex/n;

    move-result-object v1

    .line 59
    new-instance v0, Lco/uk/getmondo/feed/g$r;

    invoke-direct {v0, p0}, Lco/uk/getmondo/feed/g$r;-><init>(Lco/uk/getmondo/feed/g;)V

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {v1, v0}, Lio/reactivex/n;->flatMapCompletable(Lio/reactivex/c/h;)Lio/reactivex/b;

    move-result-object v3

    .line 66
    sget-object v0, Lco/uk/getmondo/feed/g$s;->a:Lco/uk/getmondo/feed/g$s;

    check-cast v0, Lio/reactivex/c/a;

    sget-object v1, Lco/uk/getmondo/feed/g$t;->a:Lco/uk/getmondo/feed/g$t;

    check-cast v1, Lio/reactivex/c/g;

    invoke-virtual {v3, v0, v1}, Lio/reactivex/b;->a(Lio/reactivex/c/a;Lio/reactivex/c/g;)Lio/reactivex/b/b;

    move-result-object v0

    const-string v1, "feedManager.knowYourCust\u2026ibe({}, { Timber.e(it) })"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 57
    invoke-static {v2, v0}, Lco/uk/getmondo/common/j/f;->a(Lio/reactivex/b/a;Lio/reactivex/b/b;)Lio/reactivex/b/a;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/feed/g;->b:Lio/reactivex/b/a;

    .line 68
    iget-object v2, p0, Lco/uk/getmondo/feed/g;->b:Lio/reactivex/b/a;

    .line 78
    invoke-interface {p1}, Lco/uk/getmondo/feed/g$a;->a()Lio/reactivex/n;

    move-result-object v1

    .line 69
    new-instance v0, Lco/uk/getmondo/feed/g$u;

    invoke-direct {v0, p0, p1}, Lco/uk/getmondo/feed/g$u;-><init>(Lco/uk/getmondo/feed/g;Lco/uk/getmondo/feed/g$a;)V

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {v1, v0}, Lio/reactivex/n;->flatMapCompletable(Lio/reactivex/c/h;)Lio/reactivex/b;

    move-result-object v3

    .line 78
    sget-object v0, Lco/uk/getmondo/feed/g$v;->a:Lco/uk/getmondo/feed/g$v;

    check-cast v0, Lio/reactivex/c/a;

    sget-object v1, Lco/uk/getmondo/feed/g$w;->a:Lco/uk/getmondo/feed/g$w;

    check-cast v1, Lio/reactivex/c/g;

    invoke-virtual {v3, v0, v1}, Lio/reactivex/b;->a(Lio/reactivex/c/a;Lio/reactivex/c/g;)Lio/reactivex/b/b;

    move-result-object v0

    const-string v1, "view.onRefreshAction()\n \u2026ce\") }, { Timber.e(it) })"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 68
    invoke-static {v2, v0}, Lco/uk/getmondo/common/j/f;->a(Lio/reactivex/b/a;Lio/reactivex/b/b;)Lio/reactivex/b/a;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/feed/g;->b:Lio/reactivex/b/a;

    .line 80
    iget-object v2, p0, Lco/uk/getmondo/feed/g;->b:Lio/reactivex/b/a;

    .line 81
    invoke-interface {p1}, Lco/uk/getmondo/feed/g$a;->b()Lio/reactivex/n;

    move-result-object v3

    new-instance v0, Lco/uk/getmondo/feed/g$c;

    invoke-direct {v0, p1}, Lco/uk/getmondo/feed/g$c;-><init>(Lco/uk/getmondo/feed/g$a;)V

    check-cast v0, Lio/reactivex/c/g;

    sget-object v1, Lco/uk/getmondo/feed/g$d;->a:Lco/uk/getmondo/feed/g$d;

    check-cast v1, Lio/reactivex/c/g;

    invoke-virtual {v3, v0, v1}, Lio/reactivex/n;->subscribe(Lio/reactivex/c/g;Lio/reactivex/c/g;)Lio/reactivex/b/b;

    move-result-object v0

    const-string v1, "view.onSearch()\n        \u2026ch() }, { Timber.e(it) })"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 80
    invoke-static {v2, v0}, Lco/uk/getmondo/common/j/f;->a(Lio/reactivex/b/a;Lio/reactivex/b/b;)Lio/reactivex/b/a;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/feed/g;->b:Lio/reactivex/b/a;

    .line 83
    iget-object v2, p0, Lco/uk/getmondo/feed/g;->b:Lio/reactivex/b/a;

    .line 90
    invoke-interface {p1}, Lco/uk/getmondo/feed/g$a;->c()Lio/reactivex/n;

    move-result-object v1

    .line 84
    new-instance v0, Lco/uk/getmondo/feed/g$e;

    invoke-direct {v0, p0, p1}, Lco/uk/getmondo/feed/g$e;-><init>(Lco/uk/getmondo/feed/g;Lco/uk/getmondo/feed/g$a;)V

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {v1, v0}, Lio/reactivex/n;->flatMapCompletable(Lio/reactivex/c/h;)Lio/reactivex/b;

    move-result-object v3

    .line 90
    sget-object v0, Lco/uk/getmondo/feed/g$f;->a:Lco/uk/getmondo/feed/g$f;

    check-cast v0, Lio/reactivex/c/a;

    sget-object v1, Lco/uk/getmondo/feed/g$g;->a:Lco/uk/getmondo/feed/g$g;

    check-cast v1, Lio/reactivex/c/g;

    invoke-virtual {v3, v0, v1}, Lio/reactivex/b;->a(Lio/reactivex/c/a;Lio/reactivex/c/g;)Lio/reactivex/b/b;

    move-result-object v0

    const-string v1, "view.onFeedItemDeleted()\u2026ibe({}, { Timber.e(it) })"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 83
    invoke-static {v2, v0}, Lco/uk/getmondo/common/j/f;->a(Lio/reactivex/b/a;Lio/reactivex/b/b;)Lio/reactivex/b/a;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/feed/g;->b:Lio/reactivex/b/a;

    .line 92
    iget-object v2, p0, Lco/uk/getmondo/feed/g;->b:Lio/reactivex/b/a;

    .line 94
    iget-object v0, p0, Lco/uk/getmondo/feed/g;->k:Lco/uk/getmondo/card/c;

    invoke-virtual {v0}, Lco/uk/getmondo/card/c;->a()Lio/reactivex/n;

    move-result-object v0

    .line 93
    iget-object v1, p0, Lco/uk/getmondo/feed/g;->e:Lio/reactivex/u;

    invoke-virtual {v0, v1}, Lio/reactivex/n;->observeOn(Lio/reactivex/u;)Lio/reactivex/n;

    move-result-object v3

    .line 94
    new-instance v0, Lco/uk/getmondo/feed/g$h;

    invoke-direct {v0, p1}, Lco/uk/getmondo/feed/g$h;-><init>(Lco/uk/getmondo/feed/g$a;)V

    check-cast v0, Lio/reactivex/c/g;

    .line 100
    sget-object v1, Lco/uk/getmondo/feed/g$i;->a:Lco/uk/getmondo/feed/g$i;

    check-cast v1, Lio/reactivex/c/g;

    .line 94
    invoke-virtual {v3, v0, v1}, Lio/reactivex/n;->subscribe(Lio/reactivex/c/g;Lio/reactivex/c/g;)Lio/reactivex/b/b;

    move-result-object v0

    const-string v1, "cardManager.card()\n     \u2026led to get card state\") }"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 92
    invoke-static {v2, v0}, Lco/uk/getmondo/common/j/f;->a(Lio/reactivex/b/a;Lio/reactivex/b/b;)Lio/reactivex/b/a;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/feed/g;->b:Lio/reactivex/b/a;

    .line 104
    iget-object v2, p0, Lco/uk/getmondo/feed/g;->b:Lio/reactivex/b/a;

    .line 107
    iget-object v0, p0, Lco/uk/getmondo/feed/g;->m:Lco/uk/getmondo/common/accounts/b;

    invoke-virtual {v0}, Lco/uk/getmondo/common/accounts/b;->e()Lio/reactivex/n;

    move-result-object v1

    .line 105
    new-instance v0, Lco/uk/getmondo/feed/g$j;

    invoke-direct {v0, p0}, Lco/uk/getmondo/feed/g$j;-><init>(Lco/uk/getmondo/feed/g;)V

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {v1, v0}, Lio/reactivex/n;->flatMap(Lio/reactivex/c/h;)Lio/reactivex/n;

    move-result-object v0

    .line 106
    iget-object v1, p0, Lco/uk/getmondo/feed/g;->e:Lio/reactivex/u;

    invoke-virtual {v0, v1}, Lio/reactivex/n;->observeOn(Lio/reactivex/u;)Lio/reactivex/n;

    move-result-object v3

    .line 107
    new-instance v0, Lco/uk/getmondo/feed/g$k;

    invoke-direct {v0, p0, p1}, Lco/uk/getmondo/feed/g$k;-><init>(Lco/uk/getmondo/feed/g;Lco/uk/getmondo/feed/g$a;)V

    check-cast v0, Lio/reactivex/c/g;

    .line 118
    sget-object v1, Lco/uk/getmondo/feed/g$l;->a:Lco/uk/getmondo/feed/g$l;

    check-cast v1, Lio/reactivex/c/g;

    .line 107
    invoke-virtual {v3, v0, v1}, Lio/reactivex/n;->subscribe(Lio/reactivex/c/g;Lio/reactivex/c/g;)Lio/reactivex/b/b;

    move-result-object v0

    const-string v1, "accountManager.accountId\u2026     }, { Timber.e(it) })"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 104
    invoke-static {v2, v0}, Lco/uk/getmondo/common/j/f;->a(Lio/reactivex/b/a;Lio/reactivex/b/b;)Lio/reactivex/b/a;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/feed/g;->b:Lio/reactivex/b/a;

    .line 120
    iget-object v2, p0, Lco/uk/getmondo/feed/g;->b:Lio/reactivex/b/a;

    .line 130
    invoke-interface {p1}, Lco/uk/getmondo/feed/g$a;->i()Lio/reactivex/n;

    move-result-object v0

    .line 121
    sget-object v1, Lkotlin/n;->a:Lkotlin/n;

    invoke-virtual {v0, v1}, Lio/reactivex/n;->startWith(Ljava/lang/Object;)Lio/reactivex/n;

    move-result-object v1

    .line 122
    new-instance v0, Lco/uk/getmondo/feed/g$n;

    invoke-direct {v0, p0, p1}, Lco/uk/getmondo/feed/g$n;-><init>(Lco/uk/getmondo/feed/g;Lco/uk/getmondo/feed/g$a;)V

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {v1, v0}, Lio/reactivex/n;->flatMapCompletable(Lio/reactivex/c/h;)Lio/reactivex/b;

    move-result-object v3

    .line 130
    sget-object v0, Lco/uk/getmondo/feed/g$o;->a:Lco/uk/getmondo/feed/g$o;

    check-cast v0, Lio/reactivex/c/a;

    sget-object v1, Lco/uk/getmondo/feed/g$p;->a:Lco/uk/getmondo/feed/g$p;

    check-cast v1, Lio/reactivex/c/g;

    invoke-virtual {v3, v0, v1}, Lio/reactivex/b;->a(Lio/reactivex/c/a;Lio/reactivex/c/g;)Lio/reactivex/b/b;

    move-result-object v0

    const-string v1, "view.onDateChanged()\n   \u2026ibe({}, { Timber.e(it) })"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 120
    invoke-static {v2, v0}, Lco/uk/getmondo/common/j/f;->a(Lio/reactivex/b/a;Lio/reactivex/b/b;)Lio/reactivex/b/a;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/feed/g;->b:Lio/reactivex/b/a;

    .line 132
    return-void
.end method
