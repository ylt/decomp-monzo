.class public Lco/uk/getmondo/top/NotInCountryActivity;
.super Lco/uk/getmondo/common/activities/b;
.source "NotInCountryActivity.java"

# interfaces
.implements Lco/uk/getmondo/top/b$a;


# instance fields
.field a:Lco/uk/getmondo/top/b;

.field desc:Landroid/widget/TextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f11012a
    .end annotation
.end field

.field image:Landroid/widget/ImageView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f1100ce
    .end annotation
.end field

.field notified:Landroid/widget/TextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f11013d
    .end annotation
.end field

.field notifyMe:Landroid/widget/Button;
    .annotation build Lbutterknife/BindView;
        value = 0x7f11013c
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Lco/uk/getmondo/common/activities/b;-><init>()V

    return-void
.end method

.method public static a(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 29
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lco/uk/getmondo/top/NotInCountryActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 30
    const v1, 0x10018000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 31
    invoke-virtual {p0, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 32
    return-void
.end method

.method private b()V
    .locals 3

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 56
    iget-object v0, p0, Lco/uk/getmondo/top/NotInCountryActivity;->a:Lco/uk/getmondo/top/b;

    invoke-virtual {v0}, Lco/uk/getmondo/top/b;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 57
    iget-object v0, p0, Lco/uk/getmondo/top/NotInCountryActivity;->notifyMe:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setVisibility(I)V

    .line 58
    iget-object v0, p0, Lco/uk/getmondo/top/NotInCountryActivity;->notified:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 63
    :goto_0
    return-void

    .line 60
    :cond_0
    iget-object v0, p0, Lco/uk/getmondo/top/NotInCountryActivity;->notifyMe:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 61
    iget-object v0, p0, Lco/uk/getmondo/top/NotInCountryActivity;->notified:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method

.method public static b(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 35
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lco/uk/getmondo/top/NotInCountryActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 36
    return-void
.end method


# virtual methods
.method public a()V
    .locals 0

    .prologue
    .line 83
    invoke-static {p0}, Lco/uk/getmondo/main/HomeActivity;->a(Landroid/content/Context;)V

    .line 84
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 78
    iget-object v0, p0, Lco/uk/getmondo/top/NotInCountryActivity;->desc:Landroid/widget/TextView;

    const v1, 0x7f0a0121

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-virtual {p0, v1, v2}, Lco/uk/getmondo/top/NotInCountryActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 79
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 40
    invoke-super {p0, p1}, Lco/uk/getmondo/common/activities/b;->onCreate(Landroid/os/Bundle;)V

    .line 41
    const v0, 0x7f05002b

    invoke-virtual {p0, v0}, Lco/uk/getmondo/top/NotInCountryActivity;->setContentView(I)V

    .line 42
    invoke-static {p0}, Lbutterknife/ButterKnife;->bind(Landroid/app/Activity;)Lbutterknife/Unbinder;

    .line 43
    invoke-virtual {p0}, Lco/uk/getmondo/top/NotInCountryActivity;->l()Lco/uk/getmondo/common/h/b/b;

    move-result-object v0

    invoke-interface {v0, p0}, Lco/uk/getmondo/common/h/b/b;->a(Lco/uk/getmondo/top/NotInCountryActivity;)V

    .line 44
    iget-object v0, p0, Lco/uk/getmondo/top/NotInCountryActivity;->a:Lco/uk/getmondo/top/b;

    invoke-virtual {v0, p0}, Lco/uk/getmondo/top/b;->a(Lco/uk/getmondo/top/b$a;)V

    .line 45
    iget-object v0, p0, Lco/uk/getmondo/top/NotInCountryActivity;->image:Landroid/widget/ImageView;

    const v1, 0x7f02020d

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 46
    invoke-direct {p0}, Lco/uk/getmondo/top/NotInCountryActivity;->b()V

    .line 47
    return-void
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lco/uk/getmondo/top/NotInCountryActivity;->a:Lco/uk/getmondo/top/b;

    invoke-virtual {v0}, Lco/uk/getmondo/top/b;->b()V

    .line 52
    invoke-super {p0}, Lco/uk/getmondo/common/activities/b;->onDestroy()V

    .line 53
    return-void
.end method

.method onNotify()V
    .locals 1
    .annotation build Lbutterknife/OnClick;
        value = {
            0x7f11013c
        }
    .end annotation

    .prologue
    .line 67
    iget-object v0, p0, Lco/uk/getmondo/top/NotInCountryActivity;->a:Lco/uk/getmondo/top/b;

    invoke-virtual {v0}, Lco/uk/getmondo/top/b;->c()V

    .line 68
    invoke-direct {p0}, Lco/uk/getmondo/top/NotInCountryActivity;->b()V

    .line 69
    return-void
.end method

.method onShare()V
    .locals 1
    .annotation build Lbutterknife/OnClick;
        value = {
            0x7f11011e
        }
    .end annotation

    .prologue
    .line 73
    iget-object v0, p0, Lco/uk/getmondo/top/NotInCountryActivity;->a:Lco/uk/getmondo/top/b;

    invoke-virtual {v0, p0}, Lco/uk/getmondo/top/b;->a(Landroid/app/Activity;)V

    .line 74
    return-void
.end method
