.class public Lco/uk/getmondo/top/TopActivity;
.super Lco/uk/getmondo/common/activities/b;
.source "TopActivity.java"

# interfaces
.implements Lco/uk/getmondo/top/g$a;


# instance fields
.field a:Lco/uk/getmondo/top/g;

.field getCardButton:Landroid/widget/Button;
    .annotation build Lbutterknife/BindView;
        value = 0x7f11023e
    .end annotation
.end field

.field titleTextView:Landroid/widget/TextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f1101e2
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Lco/uk/getmondo/common/activities/b;-><init>()V

    return-void
.end method

.method public static a(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 33
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lco/uk/getmondo/top/TopActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 34
    const v1, 0x10018000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 35
    invoke-virtual {p0, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 36
    return-void
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    .line 69
    sget-object v0, Lco/uk/getmondo/common/activities/b$a;->a:Lco/uk/getmondo/common/activities/b$a;

    const/4 v1, 0x1

    invoke-static {p0, v0, v1}, Lco/uk/getmondo/common/address/SelectAddressActivity;->a(Landroid/content/Context;Lco/uk/getmondo/common/activities/b$a;Z)V

    .line 70
    return-void
.end method

.method public a(Lco/uk/getmondo/d/c;)V
    .locals 0

    .prologue
    .line 84
    invoke-static {p0, p1}, Lco/uk/getmondo/create_account/topup/InitialTopupActivity;->a(Landroid/content/Context;Lco/uk/getmondo/d/c;)V

    .line 85
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 64
    iget-object v0, p0, Lco/uk/getmondo/top/TopActivity;->titleTextView:Landroid/widget/TextView;

    const v1, 0x7f0a0458

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    const/4 v3, 0x1

    invoke-static {}, Lco/uk/getmondo/common/k/e;->a()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {p0, v1, v2}, Lco/uk/getmondo/top/TopActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 65
    return-void
.end method

.method public b()V
    .locals 1

    .prologue
    .line 74
    const/4 v0, 0x1

    invoke-static {p0, v0}, Lco/uk/getmondo/create_account/wait/CardShippedActivity;->a(Landroid/content/Context;Z)V

    .line 75
    return-void
.end method

.method public c()V
    .locals 0

    .prologue
    .line 79
    invoke-static {p0}, Lco/uk/getmondo/main/HomeActivity;->a(Landroid/content/Context;)V

    .line 80
    return-void
.end method

.method public d()V
    .locals 0

    .prologue
    .line 89
    invoke-static {p0}, Lco/uk/getmondo/top/NotInCountryActivity;->a(Landroid/content/Context;)V

    .line 90
    return-void
.end method

.method public e()V
    .locals 0

    .prologue
    .line 99
    invoke-static {p0}, Lco/uk/getmondo/signup_old/CreateProfileActivity;->a(Landroid/content/Context;)V

    .line 100
    return-void
.end method

.method public f()V
    .locals 2

    .prologue
    .line 104
    iget-object v0, p0, Lco/uk/getmondo/top/TopActivity;->getCardButton:Landroid/widget/Button;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 105
    return-void
.end method

.method public g()V
    .locals 2

    .prologue
    .line 109
    iget-object v0, p0, Lco/uk/getmondo/top/TopActivity;->getCardButton:Landroid/widget/Button;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 110
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 44
    invoke-super {p0, p1}, Lco/uk/getmondo/common/activities/b;->onCreate(Landroid/os/Bundle;)V

    .line 45
    const v0, 0x7f05006b

    invoke-virtual {p0, v0}, Lco/uk/getmondo/top/TopActivity;->setContentView(I)V

    .line 46
    invoke-static {p0}, Lbutterknife/ButterKnife;->bind(Landroid/app/Activity;)Lbutterknife/Unbinder;

    .line 47
    invoke-virtual {p0}, Lco/uk/getmondo/top/TopActivity;->l()Lco/uk/getmondo/common/h/b/b;

    move-result-object v0

    invoke-interface {v0, p0}, Lco/uk/getmondo/common/h/b/b;->a(Lco/uk/getmondo/top/TopActivity;)V

    .line 48
    iget-object v0, p0, Lco/uk/getmondo/top/TopActivity;->a:Lco/uk/getmondo/top/g;

    invoke-virtual {v0, p0}, Lco/uk/getmondo/top/g;->a(Lco/uk/getmondo/top/g$a;)V

    .line 49
    return-void
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lco/uk/getmondo/top/TopActivity;->a:Lco/uk/getmondo/top/g;

    invoke-virtual {v0}, Lco/uk/getmondo/top/g;->b()V

    .line 54
    invoke-super {p0}, Lco/uk/getmondo/common/activities/b;->onDestroy()V

    .line 55
    return-void
.end method

.method onGetCardClick()V
    .locals 1
    .annotation build Lbutterknife/OnClick;
        value = {
            0x7f11023e
        }
    .end annotation

    .prologue
    .line 59
    iget-object v0, p0, Lco/uk/getmondo/top/TopActivity;->a:Lco/uk/getmondo/top/g;

    invoke-virtual {v0}, Lco/uk/getmondo/top/g;->a()V

    .line 60
    return-void
.end method
