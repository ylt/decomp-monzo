.class public Lco/uk/getmondo/top/NotInCountryActivity_ViewBinding;
.super Ljava/lang/Object;
.source "NotInCountryActivity_ViewBinding.java"

# interfaces
.implements Lbutterknife/Unbinder;


# instance fields
.field private a:Lco/uk/getmondo/top/NotInCountryActivity;

.field private b:Landroid/view/View;

.field private c:Landroid/view/View;


# direct methods
.method public constructor <init>(Lco/uk/getmondo/top/NotInCountryActivity;Landroid/view/View;)V
    .locals 4

    .prologue
    const v3, 0x7f11013c

    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    iput-object p1, p0, Lco/uk/getmondo/top/NotInCountryActivity_ViewBinding;->a:Lco/uk/getmondo/top/NotInCountryActivity;

    .line 34
    const v0, 0x7f1100ce

    const-string v1, "field \'image\'"

    const-class v2, Landroid/widget/ImageView;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p1, Lco/uk/getmondo/top/NotInCountryActivity;->image:Landroid/widget/ImageView;

    .line 35
    const v0, 0x7f11013d

    const-string v1, "field \'notified\'"

    const-class v2, Landroid/widget/TextView;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p1, Lco/uk/getmondo/top/NotInCountryActivity;->notified:Landroid/widget/TextView;

    .line 36
    const-string v0, "field \'notifyMe\' and method \'onNotify\'"

    invoke-static {p2, v3, v0}, Lbutterknife/internal/Utils;->findRequiredView(Landroid/view/View;ILjava/lang/String;)Landroid/view/View;

    move-result-object v1

    .line 37
    const-string v0, "field \'notifyMe\'"

    const-class v2, Landroid/widget/Button;

    invoke-static {v1, v3, v0, v2}, Lbutterknife/internal/Utils;->castView(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p1, Lco/uk/getmondo/top/NotInCountryActivity;->notifyMe:Landroid/widget/Button;

    .line 38
    iput-object v1, p0, Lco/uk/getmondo/top/NotInCountryActivity_ViewBinding;->b:Landroid/view/View;

    .line 39
    new-instance v0, Lco/uk/getmondo/top/NotInCountryActivity_ViewBinding$1;

    invoke-direct {v0, p0, p1}, Lco/uk/getmondo/top/NotInCountryActivity_ViewBinding$1;-><init>(Lco/uk/getmondo/top/NotInCountryActivity_ViewBinding;Lco/uk/getmondo/top/NotInCountryActivity;)V

    invoke-virtual {v1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 45
    const v0, 0x7f11012a

    const-string v1, "field \'desc\'"

    const-class v2, Landroid/widget/TextView;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p1, Lco/uk/getmondo/top/NotInCountryActivity;->desc:Landroid/widget/TextView;

    .line 46
    const v0, 0x7f11011e

    const-string v1, "method \'onShare\'"

    invoke-static {p2, v0, v1}, Lbutterknife/internal/Utils;->findRequiredView(Landroid/view/View;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 47
    iput-object v0, p0, Lco/uk/getmondo/top/NotInCountryActivity_ViewBinding;->c:Landroid/view/View;

    .line 48
    new-instance v1, Lco/uk/getmondo/top/NotInCountryActivity_ViewBinding$2;

    invoke-direct {v1, p0, p1}, Lco/uk/getmondo/top/NotInCountryActivity_ViewBinding$2;-><init>(Lco/uk/getmondo/top/NotInCountryActivity_ViewBinding;Lco/uk/getmondo/top/NotInCountryActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 54
    return-void
.end method


# virtual methods
.method public unbind()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 59
    iget-object v0, p0, Lco/uk/getmondo/top/NotInCountryActivity_ViewBinding;->a:Lco/uk/getmondo/top/NotInCountryActivity;

    .line 60
    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Bindings already cleared."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 61
    :cond_0
    iput-object v1, p0, Lco/uk/getmondo/top/NotInCountryActivity_ViewBinding;->a:Lco/uk/getmondo/top/NotInCountryActivity;

    .line 63
    iput-object v1, v0, Lco/uk/getmondo/top/NotInCountryActivity;->image:Landroid/widget/ImageView;

    .line 64
    iput-object v1, v0, Lco/uk/getmondo/top/NotInCountryActivity;->notified:Landroid/widget/TextView;

    .line 65
    iput-object v1, v0, Lco/uk/getmondo/top/NotInCountryActivity;->notifyMe:Landroid/widget/Button;

    .line 66
    iput-object v1, v0, Lco/uk/getmondo/top/NotInCountryActivity;->desc:Landroid/widget/TextView;

    .line 68
    iget-object v0, p0, Lco/uk/getmondo/top/NotInCountryActivity_ViewBinding;->b:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 69
    iput-object v1, p0, Lco/uk/getmondo/top/NotInCountryActivity_ViewBinding;->b:Landroid/view/View;

    .line 70
    iget-object v0, p0, Lco/uk/getmondo/top/NotInCountryActivity_ViewBinding;->c:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 71
    iput-object v1, p0, Lco/uk/getmondo/top/NotInCountryActivity_ViewBinding;->c:Landroid/view/View;

    .line 72
    return-void
.end method
