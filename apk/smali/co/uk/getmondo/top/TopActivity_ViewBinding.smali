.class public Lco/uk/getmondo/top/TopActivity_ViewBinding;
.super Ljava/lang/Object;
.source "TopActivity_ViewBinding.java"

# interfaces
.implements Lbutterknife/Unbinder;


# instance fields
.field private a:Lco/uk/getmondo/top/TopActivity;

.field private b:Landroid/view/View;


# direct methods
.method public constructor <init>(Lco/uk/getmondo/top/TopActivity;Landroid/view/View;)V
    .locals 4

    .prologue
    const v3, 0x7f11023e

    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    iput-object p1, p0, Lco/uk/getmondo/top/TopActivity_ViewBinding;->a:Lco/uk/getmondo/top/TopActivity;

    .line 31
    const v0, 0x7f1101e2

    const-string v1, "field \'titleTextView\'"

    const-class v2, Landroid/widget/TextView;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p1, Lco/uk/getmondo/top/TopActivity;->titleTextView:Landroid/widget/TextView;

    .line 32
    const-string v0, "field \'getCardButton\' and method \'onGetCardClick\'"

    invoke-static {p2, v3, v0}, Lbutterknife/internal/Utils;->findRequiredView(Landroid/view/View;ILjava/lang/String;)Landroid/view/View;

    move-result-object v1

    .line 33
    const-string v0, "field \'getCardButton\'"

    const-class v2, Landroid/widget/Button;

    invoke-static {v1, v3, v0, v2}, Lbutterknife/internal/Utils;->castView(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p1, Lco/uk/getmondo/top/TopActivity;->getCardButton:Landroid/widget/Button;

    .line 34
    iput-object v1, p0, Lco/uk/getmondo/top/TopActivity_ViewBinding;->b:Landroid/view/View;

    .line 35
    new-instance v0, Lco/uk/getmondo/top/TopActivity_ViewBinding$1;

    invoke-direct {v0, p0, p1}, Lco/uk/getmondo/top/TopActivity_ViewBinding$1;-><init>(Lco/uk/getmondo/top/TopActivity_ViewBinding;Lco/uk/getmondo/top/TopActivity;)V

    invoke-virtual {v1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 41
    return-void
.end method


# virtual methods
.method public unbind()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 46
    iget-object v0, p0, Lco/uk/getmondo/top/TopActivity_ViewBinding;->a:Lco/uk/getmondo/top/TopActivity;

    .line 47
    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Bindings already cleared."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 48
    :cond_0
    iput-object v1, p0, Lco/uk/getmondo/top/TopActivity_ViewBinding;->a:Lco/uk/getmondo/top/TopActivity;

    .line 50
    iput-object v1, v0, Lco/uk/getmondo/top/TopActivity;->titleTextView:Landroid/widget/TextView;

    .line 51
    iput-object v1, v0, Lco/uk/getmondo/top/TopActivity;->getCardButton:Landroid/widget/Button;

    .line 53
    iget-object v0, p0, Lco/uk/getmondo/top/TopActivity_ViewBinding;->b:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 54
    iput-object v1, p0, Lco/uk/getmondo/top/TopActivity_ViewBinding;->b:Landroid/view/View;

    .line 55
    return-void
.end method
