.class public Lco/uk/getmondo/top/b;
.super Lco/uk/getmondo/common/ui/b;
.source "NotInCountryPresenter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lco/uk/getmondo/top/b$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lco/uk/getmondo/common/ui/b",
        "<",
        "Lco/uk/getmondo/top/b$a;",
        ">;"
    }
.end annotation


# instance fields
.field private final c:Lio/reactivex/u;

.field private final d:Lio/reactivex/u;

.field private final e:Lco/uk/getmondo/common/accounts/d;

.field private final f:Lco/uk/getmondo/common/e/a;

.field private final g:Lco/uk/getmondo/common/s;

.field private final h:Lco/uk/getmondo/api/b/a;

.field private final i:Lco/uk/getmondo/common/a;


# direct methods
.method constructor <init>(Lio/reactivex/u;Lio/reactivex/u;Lco/uk/getmondo/common/accounts/d;Lco/uk/getmondo/common/e/a;Lco/uk/getmondo/common/s;Lco/uk/getmondo/api/b/a;Lco/uk/getmondo/common/a;)V
    .locals 0

    .prologue
    .line 38
    invoke-direct {p0}, Lco/uk/getmondo/common/ui/b;-><init>()V

    .line 39
    iput-object p1, p0, Lco/uk/getmondo/top/b;->c:Lio/reactivex/u;

    .line 40
    iput-object p2, p0, Lco/uk/getmondo/top/b;->d:Lio/reactivex/u;

    .line 41
    iput-object p3, p0, Lco/uk/getmondo/top/b;->e:Lco/uk/getmondo/common/accounts/d;

    .line 42
    iput-object p4, p0, Lco/uk/getmondo/top/b;->f:Lco/uk/getmondo/common/e/a;

    .line 43
    iput-object p5, p0, Lco/uk/getmondo/top/b;->g:Lco/uk/getmondo/common/s;

    .line 44
    iput-object p6, p0, Lco/uk/getmondo/top/b;->h:Lco/uk/getmondo/api/b/a;

    .line 45
    iput-object p7, p0, Lco/uk/getmondo/top/b;->i:Lco/uk/getmondo/common/a;

    .line 46
    return-void
.end method

.method private static a(Lco/uk/getmondo/d/ak;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 49
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lco/uk/getmondo/d/ak;->d()Lco/uk/getmondo/d/ac;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lco/uk/getmondo/d/ak;->d()Lco/uk/getmondo/d/ac;

    move-result-object v0

    invoke-virtual {v0}, Lco/uk/getmondo/d/ac;->h()Lco/uk/getmondo/d/s;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lco/uk/getmondo/d/ak;->d()Lco/uk/getmondo/d/ac;

    move-result-object v0

    invoke-virtual {v0}, Lco/uk/getmondo/d/ac;->h()Lco/uk/getmondo/d/s;

    move-result-object v0

    invoke-virtual {v0}, Lco/uk/getmondo/d/s;->j()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    .line 50
    :cond_0
    const-string v0, ""

    .line 57
    :goto_0
    return-object v0

    .line 53
    :cond_1
    invoke-virtual {p0}, Lco/uk/getmondo/d/ak;->d()Lco/uk/getmondo/d/ac;

    move-result-object v0

    invoke-virtual {v0}, Lco/uk/getmondo/d/ac;->h()Lco/uk/getmondo/d/s;

    move-result-object v0

    invoke-virtual {v0}, Lco/uk/getmondo/d/s;->j()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lco/uk/getmondo/d/i;->a(Ljava/lang/String;)Lco/uk/getmondo/d/i;

    move-result-object v0

    .line 54
    if-nez v0, :cond_2

    .line 55
    const-string v0, ""

    goto :goto_0

    .line 57
    :cond_2
    invoke-virtual {v0}, Lco/uk/getmondo/d/i;->c()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method static synthetic a(Lco/uk/getmondo/top/b$a;Lco/uk/getmondo/d/ak;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 71
    invoke-static {p1}, Lco/uk/getmondo/top/b;->a(Lco/uk/getmondo/d/ak;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p0, v0}, Lco/uk/getmondo/top/b$a;->a(Ljava/lang/String;)V

    .line 72
    invoke-virtual {p1}, Lco/uk/getmondo/d/ak;->c()Lco/uk/getmondo/d/a;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 73
    invoke-interface {p0}, Lco/uk/getmondo/top/b$a;->a()V

    .line 75
    :cond_0
    return-void
.end method

.method static synthetic a(Lco/uk/getmondo/top/b;Lco/uk/getmondo/top/b$a;Ljava/lang/Throwable;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 75
    iget-object v0, p0, Lco/uk/getmondo/top/b;->f:Lco/uk/getmondo/common/e/a;

    invoke-virtual {v0, p2, p1}, Lco/uk/getmondo/common/e/a;->a(Ljava/lang/Throwable;Lco/uk/getmondo/common/e/a$a;)Z

    return-void
.end method


# virtual methods
.method public a(Landroid/app/Activity;)V
    .locals 2

    .prologue
    .line 87
    const v0, 0x7f0a00dc

    invoke-virtual {p1, v0}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 88
    iget-object v1, p0, Lco/uk/getmondo/top/b;->e:Lco/uk/getmondo/common/accounts/d;

    invoke-virtual {v1}, Lco/uk/getmondo/common/accounts/d;->b()Lco/uk/getmondo/d/ak;

    move-result-object v1

    invoke-virtual {v1}, Lco/uk/getmondo/d/ak;->e()Lco/uk/getmondo/d/an;

    move-result-object v1

    invoke-virtual {v1}, Lco/uk/getmondo/d/an;->g()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lco/uk/getmondo/common/k/j;->a(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    invoke-static {v1, v0}, Landroid/content/Intent;->createChooser(Landroid/content/Intent;Ljava/lang/CharSequence;)Landroid/content/Intent;

    .line 89
    return-void
.end method

.method public bridge synthetic a(Lco/uk/getmondo/common/ui/f;)V
    .locals 0

    .prologue
    .line 25
    check-cast p1, Lco/uk/getmondo/top/b$a;

    invoke-virtual {p0, p1}, Lco/uk/getmondo/top/b;->a(Lco/uk/getmondo/top/b$a;)V

    return-void
.end method

.method public a(Lco/uk/getmondo/top/b$a;)V
    .locals 3

    .prologue
    .line 62
    invoke-super {p0, p1}, Lco/uk/getmondo/common/ui/b;->a(Lco/uk/getmondo/common/ui/f;)V

    .line 64
    iget-object v0, p0, Lco/uk/getmondo/top/b;->i:Lco/uk/getmondo/common/a;

    invoke-static {}, Lco/uk/getmondo/api/model/tracking/Impression;->j()Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v1

    invoke-virtual {v0, v1}, Lco/uk/getmondo/common/a;->a(Lco/uk/getmondo/api/model/tracking/Impression;)V

    .line 65
    iget-object v0, p0, Lco/uk/getmondo/top/b;->e:Lco/uk/getmondo/common/accounts/d;

    invoke-virtual {v0}, Lco/uk/getmondo/common/accounts/d;->b()Lco/uk/getmondo/d/ak;

    move-result-object v0

    invoke-static {v0}, Lco/uk/getmondo/top/b;->a(Lco/uk/getmondo/d/ak;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Lco/uk/getmondo/top/b$a;->a(Ljava/lang/String;)V

    .line 67
    iget-object v0, p0, Lco/uk/getmondo/top/b;->h:Lco/uk/getmondo/api/b/a;

    invoke-virtual {v0}, Lco/uk/getmondo/api/b/a;->a()Lio/reactivex/v;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/top/b;->c:Lio/reactivex/u;

    .line 68
    invoke-virtual {v0, v1}, Lio/reactivex/v;->b(Lio/reactivex/u;)Lio/reactivex/v;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/top/b;->d:Lio/reactivex/u;

    .line 69
    invoke-virtual {v0, v1}, Lio/reactivex/v;->a(Lio/reactivex/u;)Lio/reactivex/v;

    move-result-object v0

    invoke-static {p1}, Lco/uk/getmondo/top/c;->a(Lco/uk/getmondo/top/b$a;)Lio/reactivex/c/g;

    move-result-object v1

    invoke-static {p0, p1}, Lco/uk/getmondo/top/d;->a(Lco/uk/getmondo/top/b;Lco/uk/getmondo/top/b$a;)Lio/reactivex/c/g;

    move-result-object v2

    .line 70
    invoke-virtual {v0, v1, v2}, Lio/reactivex/v;->a(Lio/reactivex/c/g;Lio/reactivex/c/g;)Lio/reactivex/b/b;

    move-result-object v0

    .line 67
    invoke-virtual {p0, v0}, Lco/uk/getmondo/top/b;->a(Lio/reactivex/b/b;)V

    .line 76
    return-void
.end method

.method a()Z
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lco/uk/getmondo/top/b;->g:Lco/uk/getmondo/common/s;

    invoke-interface {v0}, Lco/uk/getmondo/common/s;->g()Z

    move-result v0

    return v0
.end method

.method c()V
    .locals 2

    .prologue
    .line 83
    iget-object v0, p0, Lco/uk/getmondo/top/b;->g:Lco/uk/getmondo/common/s;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lco/uk/getmondo/common/s;->b(Z)V

    .line 84
    return-void
.end method
