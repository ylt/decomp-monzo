.class public Lco/uk/getmondo/top/g;
.super Lco/uk/getmondo/common/ui/b;
.source "TopPresenter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lco/uk/getmondo/top/g$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lco/uk/getmondo/common/ui/b",
        "<",
        "Lco/uk/getmondo/top/g$a;",
        ">;"
    }
.end annotation


# instance fields
.field private final c:Lio/reactivex/u;

.field private final d:Lio/reactivex/u;

.field private final e:Lco/uk/getmondo/common/accounts/d;

.field private final f:Lco/uk/getmondo/common/e/a;

.field private final g:Lco/uk/getmondo/api/b/a;

.field private final h:Lco/uk/getmondo/common/a;

.field private final i:Lco/uk/getmondo/waitlist/i;

.field private final j:Lco/uk/getmondo/create_account/topup/c;


# direct methods
.method constructor <init>(Lio/reactivex/u;Lio/reactivex/u;Lco/uk/getmondo/common/accounts/d;Lco/uk/getmondo/common/e/a;Lco/uk/getmondo/api/b/a;Lco/uk/getmondo/common/a;Lco/uk/getmondo/waitlist/i;Lco/uk/getmondo/create_account/topup/c;)V
    .locals 0

    .prologue
    .line 39
    invoke-direct {p0}, Lco/uk/getmondo/common/ui/b;-><init>()V

    .line 40
    iput-object p1, p0, Lco/uk/getmondo/top/g;->c:Lio/reactivex/u;

    .line 41
    iput-object p2, p0, Lco/uk/getmondo/top/g;->d:Lio/reactivex/u;

    .line 42
    iput-object p3, p0, Lco/uk/getmondo/top/g;->e:Lco/uk/getmondo/common/accounts/d;

    .line 43
    iput-object p4, p0, Lco/uk/getmondo/top/g;->f:Lco/uk/getmondo/common/e/a;

    .line 44
    iput-object p5, p0, Lco/uk/getmondo/top/g;->g:Lco/uk/getmondo/api/b/a;

    .line 45
    iput-object p6, p0, Lco/uk/getmondo/top/g;->h:Lco/uk/getmondo/common/a;

    .line 46
    iput-object p7, p0, Lco/uk/getmondo/top/g;->i:Lco/uk/getmondo/waitlist/i;

    .line 47
    iput-object p8, p0, Lco/uk/getmondo/top/g;->j:Lco/uk/getmondo/create_account/topup/c;

    .line 48
    return-void
.end method

.method static synthetic a(Lco/uk/getmondo/top/g;Landroid/support/v4/g/j;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 67
    iget-object v0, p0, Lco/uk/getmondo/top/g;->a:Lco/uk/getmondo/common/ui/f;

    check-cast v0, Lco/uk/getmondo/top/g$a;

    invoke-interface {v0}, Lco/uk/getmondo/top/g$a;->g()V

    .line 69
    sget-object v1, Lco/uk/getmondo/top/g$1;->a:[I

    iget-object v0, p1, Landroid/support/v4/g/j;->a:Ljava/lang/Object;

    check-cast v0, Lco/uk/getmondo/d/ak;

    invoke-virtual {v0}, Lco/uk/getmondo/d/ak;->a()Lco/uk/getmondo/d/ak$a;

    move-result-object v0

    invoke-virtual {v0}, Lco/uk/getmondo/d/ak$a;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 86
    :goto_0
    return-void

    .line 71
    :pswitch_0
    iget-object v0, p0, Lco/uk/getmondo/top/g;->a:Lco/uk/getmondo/common/ui/f;

    check-cast v0, Lco/uk/getmondo/top/g$a;

    invoke-interface {v0}, Lco/uk/getmondo/top/g$a;->b()V

    goto :goto_0

    .line 74
    :pswitch_1
    iget-object v0, p0, Lco/uk/getmondo/top/g;->a:Lco/uk/getmondo/common/ui/f;

    check-cast v0, Lco/uk/getmondo/top/g$a;

    iget-object v1, p1, Landroid/support/v4/g/j;->b:Ljava/lang/Object;

    check-cast v1, Lco/uk/getmondo/d/c;

    invoke-interface {v0, v1}, Lco/uk/getmondo/top/g$a;->a(Lco/uk/getmondo/d/c;)V

    goto :goto_0

    .line 77
    :pswitch_2
    iget-object v0, p0, Lco/uk/getmondo/top/g;->a:Lco/uk/getmondo/common/ui/f;

    check-cast v0, Lco/uk/getmondo/top/g$a;

    invoke-interface {v0}, Lco/uk/getmondo/top/g$a;->c()V

    goto :goto_0

    .line 80
    :pswitch_3
    iget-object v0, p0, Lco/uk/getmondo/top/g;->a:Lco/uk/getmondo/common/ui/f;

    check-cast v0, Lco/uk/getmondo/top/g$a;

    invoke-interface {v0}, Lco/uk/getmondo/top/g$a;->d()V

    goto :goto_0

    .line 83
    :pswitch_4
    iget-object v0, p0, Lco/uk/getmondo/top/g;->a:Lco/uk/getmondo/common/ui/f;

    check-cast v0, Lco/uk/getmondo/top/g$a;

    invoke-interface {v0}, Lco/uk/getmondo/top/g$a;->e()V

    goto :goto_0

    .line 69
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method static synthetic a(Lco/uk/getmondo/top/g;Ljava/lang/Throwable;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 86
    iget-object v1, p0, Lco/uk/getmondo/top/g;->f:Lco/uk/getmondo/common/e/a;

    iget-object v0, p0, Lco/uk/getmondo/top/g;->a:Lco/uk/getmondo/common/ui/f;

    check-cast v0, Lco/uk/getmondo/common/e/a$a;

    invoke-virtual {v1, p1, v0}, Lco/uk/getmondo/common/e/a;->a(Ljava/lang/Throwable;Lco/uk/getmondo/common/e/a$a;)Z

    return-void
.end method

.method private c()V
    .locals 3

    .prologue
    .line 59
    iget-object v0, p0, Lco/uk/getmondo/top/g;->a:Lco/uk/getmondo/common/ui/f;

    check-cast v0, Lco/uk/getmondo/top/g$a;

    invoke-interface {v0}, Lco/uk/getmondo/top/g$a;->f()V

    .line 61
    iget-object v0, p0, Lco/uk/getmondo/top/g;->g:Lco/uk/getmondo/api/b/a;

    invoke-virtual {v0}, Lco/uk/getmondo/api/b/a;->a()Lio/reactivex/v;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/top/g;->j:Lco/uk/getmondo/create_account/topup/c;

    .line 62
    invoke-virtual {v1}, Lco/uk/getmondo/create_account/topup/c;->b()Lio/reactivex/v;

    move-result-object v1

    invoke-static {}, Lco/uk/getmondo/top/h;->a()Lio/reactivex/c/c;

    move-result-object v2

    .line 61
    invoke-static {v0, v1, v2}, Lio/reactivex/v;->a(Lio/reactivex/z;Lio/reactivex/z;Lio/reactivex/c/c;)Lio/reactivex/v;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/top/g;->d:Lio/reactivex/u;

    .line 64
    invoke-virtual {v0, v1}, Lio/reactivex/v;->b(Lio/reactivex/u;)Lio/reactivex/v;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/top/g;->c:Lio/reactivex/u;

    .line 65
    invoke-virtual {v0, v1}, Lio/reactivex/v;->a(Lio/reactivex/u;)Lio/reactivex/v;

    move-result-object v0

    invoke-static {p0}, Lco/uk/getmondo/top/i;->a(Lco/uk/getmondo/top/g;)Lio/reactivex/c/g;

    move-result-object v1

    invoke-static {p0}, Lco/uk/getmondo/top/j;->a(Lco/uk/getmondo/top/g;)Lio/reactivex/c/g;

    move-result-object v2

    .line 66
    invoke-virtual {v0, v1, v2}, Lio/reactivex/v;->a(Lio/reactivex/c/g;Lio/reactivex/c/g;)Lio/reactivex/b/b;

    move-result-object v0

    .line 61
    invoke-virtual {p0, v0}, Lco/uk/getmondo/top/g;->a(Lio/reactivex/b/b;)V

    .line 87
    return-void
.end method


# virtual methods
.method a()V
    .locals 1

    .prologue
    .line 90
    iget-object v0, p0, Lco/uk/getmondo/top/g;->a:Lco/uk/getmondo/common/ui/f;

    check-cast v0, Lco/uk/getmondo/top/g$a;

    invoke-interface {v0}, Lco/uk/getmondo/top/g$a;->a()V

    .line 91
    return-void
.end method

.method public bridge synthetic a(Lco/uk/getmondo/common/ui/f;)V
    .locals 0

    .prologue
    .line 25
    check-cast p1, Lco/uk/getmondo/top/g$a;

    invoke-virtual {p0, p1}, Lco/uk/getmondo/top/g;->a(Lco/uk/getmondo/top/g$a;)V

    return-void
.end method

.method public a(Lco/uk/getmondo/top/g$a;)V
    .locals 2

    .prologue
    .line 52
    invoke-super {p0, p1}, Lco/uk/getmondo/common/ui/b;->a(Lco/uk/getmondo/common/ui/f;)V

    .line 53
    iget-object v0, p0, Lco/uk/getmondo/top/g;->h:Lco/uk/getmondo/common/a;

    invoke-static {}, Lco/uk/getmondo/api/model/tracking/Impression;->i()Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v1

    invoke-virtual {v0, v1}, Lco/uk/getmondo/common/a;->a(Lco/uk/getmondo/api/model/tracking/Impression;)V

    .line 54
    iget-object v0, p0, Lco/uk/getmondo/top/g;->e:Lco/uk/getmondo/common/accounts/d;

    invoke-virtual {v0}, Lco/uk/getmondo/common/accounts/d;->b()Lco/uk/getmondo/d/ak;

    move-result-object v0

    invoke-virtual {v0}, Lco/uk/getmondo/d/ak;->d()Lco/uk/getmondo/d/ac;

    move-result-object v0

    invoke-virtual {v0}, Lco/uk/getmondo/d/ac;->c()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lco/uk/getmondo/common/k/p;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Lco/uk/getmondo/top/g$a;->a(Ljava/lang/String;)V

    .line 55
    invoke-direct {p0}, Lco/uk/getmondo/top/g;->c()V

    .line 56
    return-void
.end method
