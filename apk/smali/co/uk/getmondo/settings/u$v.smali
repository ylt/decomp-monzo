.class final Lco/uk/getmondo/settings/u$v;
.super Ljava/lang/Object;
.source "SettingsPresenter.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lco/uk/getmondo/settings/u;->a(Lco/uk/getmondo/settings/u$a;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h",
        "<TT;",
        "Lio/reactivex/l",
        "<+TR;>;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000\u0016\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u0002\u0010\u0000\u001a\u0010\u0012\u000c\u0012\n \u0003*\u0004\u0018\u00010\u00020\u00020\u00012\u0006\u0010\u0004\u001a\u00020\u0005H\n\u00a2\u0006\u0004\u0008\u0006\u0010\u0007"
    }
    d2 = {
        "<anonymous>",
        "Lio/reactivex/Maybe;",
        "Lco/uk/getmondo/api/model/ApiAccountSettings;",
        "kotlin.jvm.PlatformType",
        "state",
        "",
        "apply",
        "(Ljava/lang/Boolean;)Lio/reactivex/Maybe;"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field final synthetic a:Lco/uk/getmondo/settings/u;

.field final synthetic b:Lco/uk/getmondo/d/a;

.field final synthetic c:Lco/uk/getmondo/settings/u$a;


# direct methods
.method constructor <init>(Lco/uk/getmondo/settings/u;Lco/uk/getmondo/d/a;Lco/uk/getmondo/settings/u$a;)V
    .locals 0

    iput-object p1, p0, Lco/uk/getmondo/settings/u$v;->a:Lco/uk/getmondo/settings/u;

    iput-object p2, p0, Lco/uk/getmondo/settings/u$v;->b:Lco/uk/getmondo/d/a;

    iput-object p3, p0, Lco/uk/getmondo/settings/u$v;->c:Lco/uk/getmondo/settings/u$a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Boolean;)Lio/reactivex/h;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Boolean;",
            ")",
            "Lio/reactivex/h",
            "<",
            "Lco/uk/getmondo/api/model/ApiAccountSettings;",
            ">;"
        }
    .end annotation

    .prologue
    const-string v0, "state"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 130
    iget-object v0, p0, Lco/uk/getmondo/settings/u$v;->a:Lco/uk/getmondo/settings/u;

    invoke-static {v0}, Lco/uk/getmondo/settings/u;->e(Lco/uk/getmondo/settings/u;)Lco/uk/getmondo/api/MonzoApi;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/settings/u$v;->b:Lco/uk/getmondo/d/a;

    invoke-interface {v1}, Lco/uk/getmondo/d/a;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-interface {v0, v1, v2}, Lco/uk/getmondo/api/MonzoApi;->updateSettings(Ljava/lang/String;Z)Lio/reactivex/v;

    move-result-object v0

    .line 131
    iget-object v1, p0, Lco/uk/getmondo/settings/u$v;->a:Lco/uk/getmondo/settings/u;

    invoke-static {v1}, Lco/uk/getmondo/settings/u;->f(Lco/uk/getmondo/settings/u;)Lio/reactivex/u;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/v;->b(Lio/reactivex/u;)Lio/reactivex/v;

    move-result-object v0

    .line 132
    iget-object v1, p0, Lco/uk/getmondo/settings/u$v;->a:Lco/uk/getmondo/settings/u;

    invoke-static {v1}, Lco/uk/getmondo/settings/u;->g(Lco/uk/getmondo/settings/u;)Lio/reactivex/u;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/v;->a(Lio/reactivex/u;)Lio/reactivex/v;

    move-result-object v1

    .line 133
    new-instance v0, Lco/uk/getmondo/settings/u$v$1;

    invoke-direct {v0, p0}, Lco/uk/getmondo/settings/u$v$1;-><init>(Lco/uk/getmondo/settings/u$v;)V

    check-cast v0, Lio/reactivex/c/g;

    invoke-virtual {v1, v0}, Lio/reactivex/v;->d(Lio/reactivex/c/g;)Lio/reactivex/v;

    move-result-object v0

    .line 137
    invoke-static {v0}, Lco/uk/getmondo/common/j/f;->a(Lio/reactivex/v;)Lio/reactivex/h;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 36
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Lco/uk/getmondo/settings/u$v;->a(Ljava/lang/Boolean;)Lio/reactivex/h;

    move-result-object v0

    return-object v0
.end method
