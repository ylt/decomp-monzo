.class public Lco/uk/getmondo/settings/k;
.super Ljava/lang/Object;
.source "LocalUserSettingStorage.java"


# instance fields
.field private a:Landroid/content/SharedPreferences;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    const-string v0, "local_user_settings"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/settings/k;->a:Landroid/content/SharedPreferences;

    .line 28
    return-void
.end method

.method static synthetic a(Lco/uk/getmondo/settings/k;Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 46
    iget-object v0, p0, Lco/uk/getmondo/settings/k;->a:Landroid/content/SharedPreferences;

    invoke-interface {v0, p1}, Landroid/content/SharedPreferences;->unregisterOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    return-void
.end method

.method static synthetic a(Lco/uk/getmondo/settings/k;Lio/reactivex/o;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 40
    invoke-static {p1}, Lco/uk/getmondo/settings/m;->a(Lio/reactivex/o;)Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;

    move-result-object v0

    .line 45
    iget-object v1, p0, Lco/uk/getmondo/settings/k;->a:Landroid/content/SharedPreferences;

    invoke-interface {v1, v0}, Landroid/content/SharedPreferences;->registerOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    .line 46
    invoke-static {p0, v0}, Lco/uk/getmondo/settings/n;->a(Lco/uk/getmondo/settings/k;Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)Lio/reactivex/c/f;

    move-result-object v0

    invoke-interface {p1, v0}, Lio/reactivex/o;->a(Lio/reactivex/c/f;)V

    .line 47
    return-void
.end method

.method static synthetic a(Lio/reactivex/o;Landroid/content/SharedPreferences;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 41
    const-string v0, "KEY_NOTIFICATIONS_ENABLED"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 42
    const/4 v0, 0x1

    invoke-interface {p1, p2, v0}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-interface {p0, v0}, Lio/reactivex/o;->a(Ljava/lang/Object;)V

    .line 44
    :cond_0
    return-void
.end method


# virtual methods
.method a(Z)V
    .locals 2

    .prologue
    .line 31
    iget-object v0, p0, Lco/uk/getmondo/settings/k;->a:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "KEY_NOTIFICATIONS_ENABLED"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 32
    return-void
.end method

.method public a()Z
    .locals 3

    .prologue
    .line 35
    iget-object v0, p0, Lco/uk/getmondo/settings/k;->a:Landroid/content/SharedPreferences;

    const-string v1, "KEY_NOTIFICATIONS_ENABLED"

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public a(Ljava/lang/String;)Z
    .locals 3

    .prologue
    .line 61
    iget-object v0, p0, Lco/uk/getmondo/settings/k;->a:Landroid/content/SharedPreferences;

    const-string v1, "KEY_SEEN_NEWS_ITEM"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getStringSet(Ljava/lang/String;Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    .line 62
    if-eqz v0, :cond_0

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method b()Lio/reactivex/n;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .prologue
    .line 39
    invoke-static {p0}, Lco/uk/getmondo/settings/l;->a(Lco/uk/getmondo/settings/k;)Lio/reactivex/p;

    move-result-object v0

    invoke-static {v0}, Lio/reactivex/n;->create(Lio/reactivex/p;)Lio/reactivex/n;

    move-result-object v0

    return-object v0
.end method

.method public b(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 66
    new-instance v0, Landroid/support/v4/g/b;

    iget-object v1, p0, Lco/uk/getmondo/settings/k;->a:Landroid/content/SharedPreferences;

    const-string v2, "KEY_SEEN_NEWS_ITEM"

    new-instance v3, Landroid/support/v4/g/b;

    invoke-direct {v3}, Landroid/support/v4/g/b;-><init>()V

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getStringSet(Ljava/lang/String;Ljava/util/Set;)Ljava/util/Set;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/support/v4/g/b;-><init>(Ljava/util/Collection;)V

    .line 67
    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 68
    iget-object v1, p0, Lco/uk/getmondo/settings/k;->a:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "KEY_SEEN_NEWS_ITEM"

    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences$Editor;->putStringSet(Ljava/lang/String;Ljava/util/Set;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 69
    return-void
.end method

.method public b(Z)V
    .locals 2

    .prologue
    .line 55
    iget-object v0, p0, Lco/uk/getmondo/settings/k;->a:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "MONZOME_ONBOARDING_KEY"

    .line 56
    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 57
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 58
    return-void
.end method

.method public c(Z)V
    .locals 2

    .prologue
    .line 86
    iget-object v0, p0, Lco/uk/getmondo/settings/k;->a:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "KEY_CARD_ON_ITS_WAY"

    .line 87
    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 88
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 89
    return-void
.end method

.method public c()Z
    .locals 3

    .prologue
    .line 51
    iget-object v0, p0, Lco/uk/getmondo/settings/k;->a:Landroid/content/SharedPreferences;

    const-string v1, "MONZOME_ONBOARDING_KEY"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public d()Z
    .locals 3

    .prologue
    .line 82
    iget-object v0, p0, Lco/uk/getmondo/settings/k;->a:Landroid/content/SharedPreferences;

    const-string v1, "KEY_CARD_ON_ITS_WAY"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public e()V
    .locals 1

    .prologue
    .line 92
    iget-object v0, p0, Lco/uk/getmondo/settings/k;->a:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->clear()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 93
    return-void
.end method
