.class public Lco/uk/getmondo/settings/a;
.super Landroid/support/v4/app/i;
.source "ChangeInformationDialogFragment.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Landroid/support/v4/app/i;-><init>()V

    return-void
.end method

.method public static a()Lco/uk/getmondo/settings/a;
    .locals 1

    .prologue
    .line 16
    new-instance v0, Lco/uk/getmondo/settings/a;

    invoke-direct {v0}, Lco/uk/getmondo/settings/a;-><init>()V

    return-object v0
.end method

.method static synthetic a(Landroid/app/Activity;Landroid/content/DialogInterface;I)V
    .locals 1

    .prologue
    .line 27
    invoke-static {p0}, Lco/uk/getmondo/MonzoApplication;->a(Landroid/content/Context;)Lco/uk/getmondo/MonzoApplication;

    move-result-object v0

    invoke-virtual {v0}, Lco/uk/getmondo/MonzoApplication;->b()Lco/uk/getmondo/common/h/a/a;

    move-result-object v0

    .line 28
    invoke-interface {v0}, Lco/uk/getmondo/common/h/a/a;->p()Lco/uk/getmondo/common/q;

    move-result-object v0

    invoke-virtual {v0}, Lco/uk/getmondo/common/q;->a()V

    .line 29
    return-void
.end method


# virtual methods
.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 3

    .prologue
    .line 22
    invoke-virtual {p0}, Lco/uk/getmondo/settings/a;->getActivity()Landroid/support/v4/app/j;

    move-result-object v0

    .line 23
    new-instance v1, Landroid/app/AlertDialog$Builder;

    const v2, 0x7f0c0110

    invoke-direct {v1, v0, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    const v2, 0x7f0a0369

    .line 24
    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f0a0368

    .line 25
    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f0a036a

    .line 26
    invoke-virtual {p0, v2}, Lco/uk/getmondo/settings/a;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0}, Lco/uk/getmondo/settings/b;->a(Landroid/app/Activity;)Landroid/content/DialogInterface$OnClickListener;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0a02c3

    .line 30
    invoke-virtual {p0, v1}, Lco/uk/getmondo/settings/a;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 31
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 23
    return-object v0
.end method
