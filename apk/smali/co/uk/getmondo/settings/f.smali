.class public final Lco/uk/getmondo/settings/f;
.super Landroid/support/v7/widget/RecyclerView$a;
.source "LimitsAdapter.kt"

# interfaces
.implements La/a/a/a/a/a;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lco/uk/getmondo/settings/f$a;,
        Lco/uk/getmondo/settings/f$b;,
        Lco/uk/getmondo/settings/f$c;,
        Lco/uk/getmondo/settings/f$d;,
        Lco/uk/getmondo/settings/f$e;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/support/v7/widget/RecyclerView$a",
        "<",
        "Lco/uk/getmondo/settings/f$d;",
        ">;",
        "La/a/a/a/a/a",
        "<",
        "Lco/uk/getmondo/settings/f$a;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000x\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u0002\n\u0002\u0010!\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0006\u0008\u0000\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00020\u00012\u0008\u0012\u0004\u0012\u00020\u00040\u0003:\u0005,-./0B\u0019\u0012\u0012\u0010\u0005\u001a\u000e\u0012\u0004\u0012\u00020\u0007\u0012\u0004\u0012\u00020\u00080\u0006\u00a2\u0006\u0002\u0010\tJ\u0012\u0010\r\u001a\u00020\u000e2\u0008\u0010\u000f\u001a\u0004\u0018\u00010\u0010H\u0002J\u0010\u0010\u0011\u001a\u00020\u00122\u0006\u0010\u0013\u001a\u00020\u0014H\u0016J\u0008\u0010\u0015\u001a\u00020\u0014H\u0016J \u0010\u0016\u001a\u00020\u00172\u0006\u0010\u0018\u001a\u00020\u00172\u0006\u0010\u0019\u001a\u00020\u000e2\u0006\u0010\u001a\u001a\u00020\u001bH\u0002J\u0018\u0010\u001c\u001a\u00020\u00082\u0006\u0010\u001d\u001a\u00020\u00042\u0006\u0010\u0013\u001a\u00020\u0014H\u0016J\u0018\u0010\u001e\u001a\u00020\u00082\u0006\u0010\u001d\u001a\u00020\u00022\u0006\u0010\u0013\u001a\u00020\u0014H\u0016J\u0010\u0010\u001f\u001a\u00020\u00042\u0006\u0010 \u001a\u00020!H\u0016J\u0018\u0010\"\u001a\u00020\u00022\u0006\u0010 \u001a\u00020!2\u0006\u0010#\u001a\u00020\u0014H\u0016J\u0016\u0010$\u001a\u00020\u00082\u0006\u0010%\u001a\u00020&2\u0006\u0010\'\u001a\u00020(J\u0016\u0010)\u001a\u00020\u00082\u0006\u0010*\u001a\u00020+2\u0006\u0010\'\u001a\u00020(R\u0018\u0010\n\u001a\u000c\u0012\u0008\u0012\u00060\u000cR\u00020\u00000\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001a\u0010\u0005\u001a\u000e\u0012\u0004\u0012\u00020\u0007\u0012\u0004\u0012\u00020\u00080\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u00061"
    }
    d2 = {
        "Lco/uk/getmondo/settings/LimitsAdapter;",
        "Landroid/support/v7/widget/RecyclerView$Adapter;",
        "Lco/uk/getmondo/settings/LimitsAdapter$LimitItemHolder;",
        "Lca/barrenechea/widget/recyclerview/decoration/StickyHeaderAdapter;",
        "Lco/uk/getmondo/settings/LimitsAdapter$HeaderViewHolder;",
        "onLimitDetailsClicked",
        "Lkotlin/Function1;",
        "Lco/uk/getmondo/settings/LimitsAdapter$LimitDetails;",
        "",
        "(Lkotlin/jvm/functions/Function1;)V",
        "limits",
        "",
        "Lco/uk/getmondo/settings/LimitsAdapter$LimitItem;",
        "amountToString",
        "",
        "amount",
        "Lco/uk/getmondo/model/Amount;",
        "getHeaderId",
        "",
        "position",
        "",
        "getItemCount",
        "makeSubstringClickable",
        "Landroid/text/SpannableString;",
        "spannable",
        "substring",
        "span",
        "Landroid/text/style/ClickableSpan;",
        "onBindHeaderViewHolder",
        "holder",
        "onBindViewHolder",
        "onCreateHeaderViewHolder",
        "parent",
        "Landroid/view/ViewGroup;",
        "onCreateViewHolder",
        "viewType",
        "setBalanceLimitPrepaid",
        "limit",
        "Lco/uk/getmondo/model/BalanceLimit;",
        "resources",
        "Landroid/content/res/Resources;",
        "setPaymentLimits",
        "paymentLimits",
        "Lco/uk/getmondo/model/PaymentLimits;",
        "HeaderViewHolder",
        "LimitDetails",
        "LimitItem",
        "LimitItemHolder",
        "NoUnderlineClickableSpan",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field private final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lco/uk/getmondo/settings/f$c;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Lkotlin/d/a/b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/d/a/b",
            "<",
            "Lco/uk/getmondo/settings/f$b;",
            "Lkotlin/n;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lkotlin/d/a/b;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/d/a/b",
            "<-",
            "Lco/uk/getmondo/settings/f$b;",
            "Lkotlin/n;",
            ">;)V"
        }
    .end annotation

    .prologue
    const-string v0, "onLimitDetailsClicked"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 26
    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView$a;-><init>()V

    iput-object p1, p0, Lco/uk/getmondo/settings/f;->b:Lkotlin/d/a/b;

    .line 28
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    check-cast v0, Ljava/util/List;

    iput-object v0, p0, Lco/uk/getmondo/settings/f;->a:Ljava/util/List;

    return-void
.end method

.method private final a(Landroid/text/SpannableString;Ljava/lang/String;Landroid/text/style/ClickableSpan;)Landroid/text/SpannableString;
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 195
    move-object v0, p1

    check-cast v0, Ljava/lang/CharSequence;

    const/4 v4, 0x6

    const/4 v5, 0x0

    move-object v1, p2

    move v3, v2

    invoke-static/range {v0 .. v5}, Lkotlin/h/j;->a(Ljava/lang/CharSequence;Ljava/lang/String;IZILjava/lang/Object;)I

    move-result v0

    .line 196
    const/4 v1, -0x1

    if-le v0, v1, :cond_0

    .line 197
    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v1

    add-int/2addr v1, v0

    .line 198
    const/16 v2, 0x21

    invoke-virtual {p1, p3, v0, v1, v2}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 200
    :cond_0
    return-object p1
.end method

.method public static final synthetic a(Lco/uk/getmondo/settings/f;Landroid/text/SpannableString;Ljava/lang/String;Landroid/text/style/ClickableSpan;)Landroid/text/SpannableString;
    .locals 1

    .prologue
    .line 25
    invoke-direct {p0, p1, p2, p3}, Lco/uk/getmondo/settings/f;->a(Landroid/text/SpannableString;Ljava/lang/String;Landroid/text/style/ClickableSpan;)Landroid/text/SpannableString;

    move-result-object v0

    return-object v0
.end method

.method private final a(Lco/uk/getmondo/d/c;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 50
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lco/uk/getmondo/d/c;->toString()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    const-string v0, ""

    goto :goto_0
.end method

.method public static final synthetic a(Lco/uk/getmondo/settings/f;)Lkotlin/d/a/b;
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, Lco/uk/getmondo/settings/f;->b:Lkotlin/d/a/b;

    return-object v0
.end method


# virtual methods
.method public a(I)J
    .locals 2

    .prologue
    .line 41
    iget-object v0, p0, Lco/uk/getmondo/settings/f;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/settings/f$c;

    invoke-virtual {v0}, Lco/uk/getmondo/settings/f$c;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    int-to-long v0, v0

    return-wide v0
.end method

.method public synthetic a(Landroid/view/ViewGroup;)Landroid/support/v7/widget/RecyclerView$w;
    .locals 1

    .prologue
    .line 25
    invoke-virtual {p0, p1}, Lco/uk/getmondo/settings/f;->b(Landroid/view/ViewGroup;)Lco/uk/getmondo/settings/f$a;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView$w;

    return-object v0
.end method

.method public a(Landroid/view/ViewGroup;I)Lco/uk/getmondo/settings/f$d;
    .locals 4

    .prologue
    const-string v0, "parent"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 31
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 32
    new-instance v1, Lco/uk/getmondo/settings/f$d;

    const v2, 0x7f050105

    const/4 v3, 0x0

    invoke-virtual {v0, v2, p1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    const-string v2, "layoutInflater.inflate(R\u2026em_limits, parent, false)"

    invoke-static {v0, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v1, v0}, Lco/uk/getmondo/settings/f$d;-><init>(Landroid/view/View;)V

    return-object v1
.end method

.method public bridge synthetic a(Landroid/support/v7/widget/RecyclerView$w;I)V
    .locals 0

    .prologue
    .line 25
    check-cast p1, Lco/uk/getmondo/settings/f$a;

    invoke-virtual {p0, p1, p2}, Lco/uk/getmondo/settings/f;->a(Lco/uk/getmondo/settings/f$a;I)V

    return-void
.end method

.method public final a(Lco/uk/getmondo/d/e;Landroid/content/res/Resources;)V
    .locals 16

    .prologue
    const-string v1, "limit"

    move-object/from16 v0, p1

    invoke-static {v0, v1}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "resources"

    move-object/from16 v0, p2

    invoke-static {v0, v1}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 54
    const v1, 0x7f0a025e

    move-object/from16 v0, p2

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 55
    const v1, 0x7f0a025b

    move-object/from16 v0, p2

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v11

    .line 56
    const v1, 0x7f0a025a

    move-object/from16 v0, p2

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v12

    .line 57
    const v1, 0x7f0a025d

    move-object/from16 v0, p2

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v13

    .line 58
    const v1, 0x7f0a025c

    move-object/from16 v0, p2

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v14

    .line 60
    move-object/from16 v0, p0

    iget-object v15, v0, Lco/uk/getmondo/settings/f;->a:Ljava/util/List;

    new-instance v1, Lco/uk/getmondo/settings/f$c;

    .line 61
    const-string v2, "headerUsingYourCard"

    invoke-static {v4, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const v2, 0x7f0a0253

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    const-string v2, "resources.getString(R.st\u2026g.limits_max_single_card)"

    invoke-static {v5, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v5, Ljava/lang/CharSequence;

    invoke-virtual/range {p1 .. p1}, Lco/uk/getmondo/d/e;->b()Lco/uk/getmondo/d/c;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lco/uk/getmondo/settings/f;->a(Lco/uk/getmondo/d/c;)Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/16 v9, 0x30

    const/4 v10, 0x0

    move-object/from16 v2, p0

    move-object/from16 v3, p2

    .line 60
    invoke-direct/range {v1 .. v10}, Lco/uk/getmondo/settings/f$c;-><init>(Lco/uk/getmondo/settings/f;Landroid/content/res/Resources;Ljava/lang/String;Ljava/lang/CharSequence;Ljava/lang/String;Ljava/lang/String;Lco/uk/getmondo/settings/f$b;ILkotlin/d/b/i;)V

    invoke-interface {v15, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 63
    move-object/from16 v0, p0

    iget-object v15, v0, Lco/uk/getmondo/settings/f;->a:Ljava/util/List;

    new-instance v1, Lco/uk/getmondo/settings/f$c;

    .line 64
    const-string v2, "headerBalanceAndTopups"

    invoke-static {v11, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const v2, 0x7f0a0250

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    const-string v2, "resources.getString(R.string.limits_max_balance)"

    invoke-static {v5, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v5, Ljava/lang/CharSequence;

    invoke-virtual/range {p1 .. p1}, Lco/uk/getmondo/d/e;->c()Lco/uk/getmondo/d/c;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lco/uk/getmondo/settings/f;->a(Lco/uk/getmondo/d/c;)Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/16 v9, 0x30

    const/4 v10, 0x0

    move-object/from16 v2, p0

    move-object/from16 v3, p2

    move-object v4, v11

    .line 63
    invoke-direct/range {v1 .. v10}, Lco/uk/getmondo/settings/f$c;-><init>(Lco/uk/getmondo/settings/f;Landroid/content/res/Resources;Ljava/lang/String;Ljava/lang/CharSequence;Ljava/lang/String;Ljava/lang/String;Lco/uk/getmondo/settings/f$b;ILkotlin/d/b/i;)V

    invoke-interface {v15, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 65
    move-object/from16 v0, p0

    iget-object v15, v0, Lco/uk/getmondo/settings/f;->a:Ljava/util/List;

    new-instance v1, Lco/uk/getmondo/settings/f$c;

    const v2, 0x7f0a0249

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    const-string v2, "resources.getString(R.string.limits_daily_top_up)"

    invoke-static {v5, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v5, Ljava/lang/CharSequence;

    .line 66
    invoke-virtual/range {p1 .. p1}, Lco/uk/getmondo/d/e;->d()Lco/uk/getmondo/d/c;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lco/uk/getmondo/settings/f;->a(Lco/uk/getmondo/d/c;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual/range {p1 .. p1}, Lco/uk/getmondo/d/e;->e()Lco/uk/getmondo/d/c;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lco/uk/getmondo/settings/f;->a(Lco/uk/getmondo/d/c;)Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x0

    const/16 v9, 0x20

    const/4 v10, 0x0

    move-object/from16 v2, p0

    move-object/from16 v3, p2

    move-object v4, v11

    .line 65
    invoke-direct/range {v1 .. v10}, Lco/uk/getmondo/settings/f$c;-><init>(Lco/uk/getmondo/settings/f;Landroid/content/res/Resources;Ljava/lang/String;Ljava/lang/CharSequence;Ljava/lang/String;Ljava/lang/String;Lco/uk/getmondo/settings/f$b;ILkotlin/d/b/i;)V

    invoke-interface {v15, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 67
    move-object/from16 v0, p0

    iget-object v9, v0, Lco/uk/getmondo/settings/f;->a:Ljava/util/List;

    new-instance v1, Lco/uk/getmondo/settings/f$c;

    const v2, 0x7f0a0251

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    const-string v2, "resources.getString(R.st\u2026.limits_max_over_30_days)"

    invoke-static {v5, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v5, Ljava/lang/CharSequence;

    .line 68
    invoke-virtual/range {p1 .. p1}, Lco/uk/getmondo/d/e;->f()Lco/uk/getmondo/d/c;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lco/uk/getmondo/settings/f;->a(Lco/uk/getmondo/d/c;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual/range {p1 .. p1}, Lco/uk/getmondo/d/e;->g()Lco/uk/getmondo/d/c;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lco/uk/getmondo/settings/f;->a(Lco/uk/getmondo/d/c;)Ljava/lang/String;

    move-result-object v7

    .line 69
    new-instance v8, Lco/uk/getmondo/settings/f$b;

    const v2, 0x7f0a024d

    .line 70
    const v3, 0x7f0a024e

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v10, 0x0

    invoke-virtual/range {p1 .. p1}, Lco/uk/getmondo/d/e;->f()Lco/uk/getmondo/d/c;

    move-result-object v15

    aput-object v15, v4, v10

    move-object/from16 v0, p2

    invoke-virtual {v0, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "resources.getString(R.st\u2026limit.maxTopUpOver30Days)"

    invoke-static {v3, v4}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 69
    invoke-direct {v8, v2, v3}, Lco/uk/getmondo/settings/f$b;-><init>(ILjava/lang/String;)V

    move-object/from16 v2, p0

    move-object/from16 v3, p2

    move-object v4, v11

    .line 67
    invoke-direct/range {v1 .. v8}, Lco/uk/getmondo/settings/f$c;-><init>(Lco/uk/getmondo/settings/f;Landroid/content/res/Resources;Ljava/lang/String;Ljava/lang/CharSequence;Ljava/lang/String;Ljava/lang/String;Lco/uk/getmondo/settings/f$b;)V

    invoke-interface {v9, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 71
    move-object/from16 v0, p0

    iget-object v15, v0, Lco/uk/getmondo/settings/f;->a:Ljava/util/List;

    new-instance v1, Lco/uk/getmondo/settings/f$c;

    const v2, 0x7f0a024f

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    const-string v2, "resources.getString(R.st\u2026limits_max_annual_top_up)"

    invoke-static {v5, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v5, Ljava/lang/CharSequence;

    .line 72
    invoke-virtual/range {p1 .. p1}, Lco/uk/getmondo/d/e;->h()Lco/uk/getmondo/d/c;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lco/uk/getmondo/settings/f;->a(Lco/uk/getmondo/d/c;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual/range {p1 .. p1}, Lco/uk/getmondo/d/e;->i()Lco/uk/getmondo/d/c;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lco/uk/getmondo/settings/f;->a(Lco/uk/getmondo/d/c;)Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x0

    const/16 v9, 0x20

    const/4 v10, 0x0

    move-object/from16 v2, p0

    move-object/from16 v3, p2

    move-object v4, v11

    .line 71
    invoke-direct/range {v1 .. v10}, Lco/uk/getmondo/settings/f$c;-><init>(Lco/uk/getmondo/settings/f;Landroid/content/res/Resources;Ljava/lang/String;Ljava/lang/CharSequence;Ljava/lang/String;Ljava/lang/String;Lco/uk/getmondo/settings/f$b;ILkotlin/d/b/i;)V

    invoke-interface {v15, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 74
    move-object/from16 v0, p0

    iget-object v11, v0, Lco/uk/getmondo/settings/f;->a:Ljava/util/List;

    new-instance v1, Lco/uk/getmondo/settings/f$c;

    const-string v2, "headerAtmWithdrawals"

    invoke-static {v12, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const v2, 0x7f0a024a

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    const-string v2, "resources.getString(R.st\u2026.limits_daily_withdrawal)"

    invoke-static {v5, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v5, Ljava/lang/CharSequence;

    .line 75
    invoke-virtual/range {p1 .. p1}, Lco/uk/getmondo/d/e;->j()Lco/uk/getmondo/d/c;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lco/uk/getmondo/settings/f;->a(Lco/uk/getmondo/d/c;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual/range {p1 .. p1}, Lco/uk/getmondo/d/e;->k()Lco/uk/getmondo/d/c;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lco/uk/getmondo/settings/f;->a(Lco/uk/getmondo/d/c;)Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x0

    const/16 v9, 0x20

    const/4 v10, 0x0

    move-object/from16 v2, p0

    move-object/from16 v3, p2

    move-object v4, v12

    .line 74
    invoke-direct/range {v1 .. v10}, Lco/uk/getmondo/settings/f$c;-><init>(Lco/uk/getmondo/settings/f;Landroid/content/res/Resources;Ljava/lang/String;Ljava/lang/CharSequence;Ljava/lang/String;Ljava/lang/String;Lco/uk/getmondo/settings/f$b;ILkotlin/d/b/i;)V

    invoke-interface {v11, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 76
    move-object/from16 v0, p0

    iget-object v11, v0, Lco/uk/getmondo/settings/f;->a:Ljava/util/List;

    new-instance v1, Lco/uk/getmondo/settings/f$c;

    const v2, 0x7f0a0256

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    const-string v2, "resources.getString(R.st\u2026g.limits_rolling_30_days)"

    invoke-static {v5, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v5, Ljava/lang/CharSequence;

    .line 77
    invoke-virtual/range {p1 .. p1}, Lco/uk/getmondo/d/e;->l()Lco/uk/getmondo/d/c;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lco/uk/getmondo/settings/f;->a(Lco/uk/getmondo/d/c;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual/range {p1 .. p1}, Lco/uk/getmondo/d/e;->m()Lco/uk/getmondo/d/c;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lco/uk/getmondo/settings/f;->a(Lco/uk/getmondo/d/c;)Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x0

    const/16 v9, 0x20

    const/4 v10, 0x0

    move-object/from16 v2, p0

    move-object/from16 v3, p2

    move-object v4, v12

    .line 76
    invoke-direct/range {v1 .. v10}, Lco/uk/getmondo/settings/f$c;-><init>(Lco/uk/getmondo/settings/f;Landroid/content/res/Resources;Ljava/lang/String;Ljava/lang/CharSequence;Ljava/lang/String;Ljava/lang/String;Lco/uk/getmondo/settings/f$b;ILkotlin/d/b/i;)V

    invoke-interface {v11, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 78
    move-object/from16 v0, p0

    iget-object v11, v0, Lco/uk/getmondo/settings/f;->a:Ljava/util/List;

    new-instance v1, Lco/uk/getmondo/settings/f$c;

    const v2, 0x7f0a0248

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    const-string v2, "resources.getString(R.st\u2026limits_annual_withdrawal)"

    invoke-static {v5, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v5, Ljava/lang/CharSequence;

    .line 79
    invoke-virtual/range {p1 .. p1}, Lco/uk/getmondo/d/e;->n()Lco/uk/getmondo/d/c;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lco/uk/getmondo/settings/f;->a(Lco/uk/getmondo/d/c;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual/range {p1 .. p1}, Lco/uk/getmondo/d/e;->o()Lco/uk/getmondo/d/c;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lco/uk/getmondo/settings/f;->a(Lco/uk/getmondo/d/c;)Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x0

    const/16 v9, 0x20

    const/4 v10, 0x0

    move-object/from16 v2, p0

    move-object/from16 v3, p2

    move-object v4, v12

    .line 78
    invoke-direct/range {v1 .. v10}, Lco/uk/getmondo/settings/f$c;-><init>(Lco/uk/getmondo/settings/f;Landroid/content/res/Resources;Ljava/lang/String;Ljava/lang/CharSequence;Ljava/lang/String;Ljava/lang/String;Lco/uk/getmondo/settings/f$b;ILkotlin/d/b/i;)V

    invoke-interface {v11, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 81
    move-object/from16 v0, p0

    iget-object v11, v0, Lco/uk/getmondo/settings/f;->a:Ljava/util/List;

    new-instance v1, Lco/uk/getmondo/settings/f$c;

    .line 82
    const-string v2, "headerPayments"

    invoke-static {v13, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const v2, 0x7f0a0254

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    const-string v2, "resources.getString(R.st\u2026imits_max_single_payment)"

    invoke-static {v5, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v5, Ljava/lang/CharSequence;

    invoke-virtual/range {p1 .. p1}, Lco/uk/getmondo/d/e;->p()Lco/uk/getmondo/d/c;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lco/uk/getmondo/settings/f;->a(Lco/uk/getmondo/d/c;)Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/16 v9, 0x30

    const/4 v10, 0x0

    move-object/from16 v2, p0

    move-object/from16 v3, p2

    move-object v4, v13

    .line 81
    invoke-direct/range {v1 .. v10}, Lco/uk/getmondo/settings/f$c;-><init>(Lco/uk/getmondo/settings/f;Landroid/content/res/Resources;Ljava/lang/String;Ljava/lang/CharSequence;Ljava/lang/String;Ljava/lang/String;Lco/uk/getmondo/settings/f$b;ILkotlin/d/b/i;)V

    invoke-interface {v11, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 83
    move-object/from16 v0, p0

    iget-object v9, v0, Lco/uk/getmondo/settings/f;->a:Ljava/util/List;

    new-instance v1, Lco/uk/getmondo/settings/f$c;

    const v2, 0x7f0a0252

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    const-string v2, "resources.getString(R.st\u2026ax_received_over_30_days)"

    invoke-static {v5, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v5, Ljava/lang/CharSequence;

    .line 84
    invoke-virtual/range {p1 .. p1}, Lco/uk/getmondo/d/e;->q()Lco/uk/getmondo/d/c;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lco/uk/getmondo/settings/f;->a(Lco/uk/getmondo/d/c;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual/range {p1 .. p1}, Lco/uk/getmondo/d/e;->r()Lco/uk/getmondo/d/c;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lco/uk/getmondo/settings/f;->a(Lco/uk/getmondo/d/c;)Ljava/lang/String;

    move-result-object v7

    .line 85
    new-instance v8, Lco/uk/getmondo/settings/f$b;

    const v2, 0x7f0a024d

    .line 86
    const v3, 0x7f0a024c

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v10, 0x0

    invoke-virtual/range {p1 .. p1}, Lco/uk/getmondo/d/e;->q()Lco/uk/getmondo/d/c;

    move-result-object v11

    aput-object v11, v4, v10

    move-object/from16 v0, p2

    invoke-virtual {v0, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "resources.getString(R.st\u2026ntsMaxReceivedOver30Days)"

    invoke-static {v3, v4}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 85
    invoke-direct {v8, v2, v3}, Lco/uk/getmondo/settings/f$b;-><init>(ILjava/lang/String;)V

    move-object/from16 v2, p0

    move-object/from16 v3, p2

    move-object v4, v13

    .line 83
    invoke-direct/range {v1 .. v8}, Lco/uk/getmondo/settings/f$c;-><init>(Lco/uk/getmondo/settings/f;Landroid/content/res/Resources;Ljava/lang/String;Ljava/lang/CharSequence;Ljava/lang/String;Ljava/lang/String;Lco/uk/getmondo/settings/f$b;)V

    invoke-interface {v9, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 88
    invoke-virtual/range {p1 .. p1}, Lco/uk/getmondo/d/e;->a()Lco/uk/getmondo/api/model/VerificationType;

    move-result-object v1

    sget-object v2, Lco/uk/getmondo/api/model/VerificationType;->FULL:Lco/uk/getmondo/api/model/VerificationType;

    if-eq v1, v2, :cond_0

    invoke-virtual/range {p1 .. p1}, Lco/uk/getmondo/d/e;->a()Lco/uk/getmondo/api/model/VerificationType;

    move-result-object v1

    sget-object v2, Lco/uk/getmondo/api/model/VerificationType;->EXTENDED:Lco/uk/getmondo/api/model/VerificationType;

    if-ne v1, v2, :cond_1

    .line 89
    :cond_0
    move-object/from16 v0, p0

    iget-object v11, v0, Lco/uk/getmondo/settings/f;->a:Ljava/util/List;

    new-instance v1, Lco/uk/getmondo/settings/f$c;

    .line 90
    const-string v2, "headerMonzoMe"

    invoke-static {v14, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const v2, 0x7f0a0254

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    const-string v2, "resources.getString(R.st\u2026imits_max_single_payment)"

    invoke-static {v5, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v5, Ljava/lang/CharSequence;

    invoke-virtual/range {p1 .. p1}, Lco/uk/getmondo/d/e;->s()Lco/uk/getmondo/d/c;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lco/uk/getmondo/settings/f;->a(Lco/uk/getmondo/d/c;)Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/16 v9, 0x30

    const/4 v10, 0x0

    move-object/from16 v2, p0

    move-object/from16 v3, p2

    move-object v4, v14

    .line 89
    invoke-direct/range {v1 .. v10}, Lco/uk/getmondo/settings/f$c;-><init>(Lco/uk/getmondo/settings/f;Landroid/content/res/Resources;Ljava/lang/String;Ljava/lang/CharSequence;Ljava/lang/String;Ljava/lang/String;Lco/uk/getmondo/settings/f$b;ILkotlin/d/b/i;)V

    invoke-interface {v11, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 91
    move-object/from16 v0, p0

    iget-object v9, v0, Lco/uk/getmondo/settings/f;->a:Ljava/util/List;

    new-instance v1, Lco/uk/getmondo/settings/f$c;

    .line 92
    const v2, 0x7f0a0252

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    const-string v2, "resources.getString(R.st\u2026ax_received_over_30_days)"

    invoke-static {v5, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v5, Ljava/lang/CharSequence;

    .line 93
    invoke-virtual/range {p1 .. p1}, Lco/uk/getmondo/d/e;->t()Lco/uk/getmondo/d/c;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lco/uk/getmondo/settings/f;->a(Lco/uk/getmondo/d/c;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual/range {p1 .. p1}, Lco/uk/getmondo/d/e;->u()Lco/uk/getmondo/d/c;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lco/uk/getmondo/settings/f;->a(Lco/uk/getmondo/d/c;)Ljava/lang/String;

    move-result-object v7

    .line 94
    new-instance v8, Lco/uk/getmondo/settings/f$b;

    const v2, 0x7f0a024d

    .line 95
    const v3, 0x7f0a024b

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v10, 0x0

    invoke-virtual/range {p1 .. p1}, Lco/uk/getmondo/d/e;->t()Lco/uk/getmondo/d/c;

    move-result-object v11

    aput-object v11, v4, v10

    move-object/from16 v0, p2

    invoke-virtual {v0, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "resources.getString(R.st\u2026oMeMaxReceivedOver30Days)"

    invoke-static {v3, v4}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 94
    invoke-direct {v8, v2, v3}, Lco/uk/getmondo/settings/f$b;-><init>(ILjava/lang/String;)V

    move-object/from16 v2, p0

    move-object/from16 v3, p2

    move-object v4, v14

    .line 91
    invoke-direct/range {v1 .. v8}, Lco/uk/getmondo/settings/f$c;-><init>(Lco/uk/getmondo/settings/f;Landroid/content/res/Resources;Ljava/lang/String;Ljava/lang/CharSequence;Ljava/lang/String;Ljava/lang/String;Lco/uk/getmondo/settings/f$b;)V

    invoke-interface {v9, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 98
    :cond_1
    invoke-virtual/range {p0 .. p0}, Lco/uk/getmondo/settings/f;->notifyDataSetChanged()V

    .line 99
    return-void
.end method

.method public final a(Lco/uk/getmondo/d/y;Landroid/content/res/Resources;)V
    .locals 16

    .prologue
    const-string v2, "paymentLimits"

    move-object/from16 v0, p1

    invoke-static {v0, v2}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v2, "resources"

    move-object/from16 v0, p2

    invoke-static {v0, v2}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 102
    invoke-virtual/range {p1 .. p1}, Lco/uk/getmondo/d/y;->a()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v14

    :goto_0
    invoke-interface {v14}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-interface {v14}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    move-object v12, v2

    check-cast v12, Lco/uk/getmondo/d/z;

    .line 103
    invoke-virtual {v12}, Lco/uk/getmondo/d/z;->b()Ljava/util/List;

    move-result-object v2

    check-cast v2, Ljava/lang/Iterable;

    move-object/from16 v0, p0

    iget-object v3, v0, Lco/uk/getmondo/settings/f;->a:Ljava/util/List;

    move-object v13, v3

    check-cast v13, Ljava/util/Collection;

    .line 204
    invoke-interface {v2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v15

    :goto_1
    invoke-interface {v15}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v15}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    move-object v3, v2

    .line 205
    check-cast v3, Lco/uk/getmondo/d/x;

    .line 105
    instance-of v2, v3, Lco/uk/getmondo/d/x$a;

    if-eqz v2, :cond_0

    move-object v2, v3

    .line 106
    check-cast v2, Lco/uk/getmondo/d/x$a;

    invoke-virtual {v2}, Lco/uk/getmondo/d/x$a;->b()Lco/uk/getmondo/d/c;

    move-result-object v4

    move-object v2, v3

    check-cast v2, Lco/uk/getmondo/d/x$a;

    invoke-virtual {v2}, Lco/uk/getmondo/d/x$a;->c()Lco/uk/getmondo/d/c;

    move-result-object v2

    invoke-virtual {v2}, Lco/uk/getmondo/d/c;->k()J

    move-result-wide v6

    invoke-virtual {v4, v6, v7}, Lco/uk/getmondo/d/c;->b(J)Lco/uk/getmondo/d/c;

    move-result-object v8

    .line 107
    new-instance v2, Lco/uk/getmondo/settings/f$c;

    invoke-virtual {v12}, Lco/uk/getmondo/d/z;->a()Ljava/lang/String;

    move-result-object v5

    move-object v4, v3

    check-cast v4, Lco/uk/getmondo/d/x$a;

    invoke-virtual {v4}, Lco/uk/getmondo/d/x$a;->a()Ljava/lang/String;

    move-result-object v6

    check-cast v6, Ljava/lang/CharSequence;

    check-cast v3, Lco/uk/getmondo/d/x$a;

    invoke-virtual {v3}, Lco/uk/getmondo/d/x$a;->b()Lco/uk/getmondo/d/c;

    move-result-object v3

    invoke-virtual {v3}, Lco/uk/getmondo/d/c;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v8}, Lco/uk/getmondo/d/c;->toString()Ljava/lang/String;

    move-result-object v8

    const/4 v9, 0x0

    const/16 v10, 0x20

    const/4 v11, 0x0

    move-object/from16 v3, p0

    move-object/from16 v4, p2

    invoke-direct/range {v2 .. v11}, Lco/uk/getmondo/settings/f$c;-><init>(Lco/uk/getmondo/settings/f;Landroid/content/res/Resources;Ljava/lang/String;Ljava/lang/CharSequence;Ljava/lang/String;Ljava/lang/String;Lco/uk/getmondo/settings/f$b;ILkotlin/d/b/i;)V

    .line 116
    :goto_2
    invoke-interface {v13, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 109
    :cond_0
    instance-of v2, v3, Lco/uk/getmondo/d/x$b;

    if-eqz v2, :cond_1

    move-object v2, v3

    .line 110
    check-cast v2, Lco/uk/getmondo/d/x$b;

    invoke-virtual {v2}, Lco/uk/getmondo/d/x$b;->b()J

    move-result-wide v4

    move-object v2, v3

    check-cast v2, Lco/uk/getmondo/d/x$b;

    invoke-virtual {v2}, Lco/uk/getmondo/d/x$b;->c()J

    move-result-wide v6

    sub-long v8, v4, v6

    .line 111
    new-instance v2, Lco/uk/getmondo/settings/f$c;

    invoke-virtual {v12}, Lco/uk/getmondo/d/z;->a()Ljava/lang/String;

    move-result-object v5

    move-object v4, v3

    check-cast v4, Lco/uk/getmondo/d/x$b;

    invoke-virtual {v4}, Lco/uk/getmondo/d/x$b;->a()Ljava/lang/String;

    move-result-object v6

    check-cast v6, Ljava/lang/CharSequence;

    check-cast v3, Lco/uk/getmondo/d/x$b;

    invoke-virtual {v3}, Lco/uk/getmondo/d/x$b;->b()J

    move-result-wide v10

    invoke-static {v10, v11}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v7

    invoke-static {v8, v9}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v8

    const/4 v9, 0x0

    const/16 v10, 0x20

    const/4 v11, 0x0

    move-object/from16 v3, p0

    move-object/from16 v4, p2

    invoke-direct/range {v2 .. v11}, Lco/uk/getmondo/settings/f$c;-><init>(Lco/uk/getmondo/settings/f;Landroid/content/res/Resources;Ljava/lang/String;Ljava/lang/CharSequence;Ljava/lang/String;Ljava/lang/String;Lco/uk/getmondo/settings/f$b;ILkotlin/d/b/i;)V

    goto :goto_2

    .line 113
    :cond_1
    instance-of v2, v3, Lco/uk/getmondo/d/x$c;

    if-eqz v2, :cond_2

    .line 114
    new-instance v2, Lco/uk/getmondo/settings/f$c;

    invoke-virtual {v12}, Lco/uk/getmondo/d/z;->a()Ljava/lang/String;

    move-result-object v5

    check-cast v3, Lco/uk/getmondo/d/x$c;

    invoke-virtual {v3}, Lco/uk/getmondo/d/x$c;->a()Ljava/lang/String;

    move-result-object v6

    check-cast v6, Ljava/lang/CharSequence;

    const v3, 0x7f0a025f

    move-object/from16 v0, p2

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    const-string v3, "resources.getString(R.string.limits_unlimited)"

    invoke-static {v7, v3}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/16 v10, 0x20

    const/4 v11, 0x0

    move-object/from16 v3, p0

    move-object/from16 v4, p2

    invoke-direct/range {v2 .. v11}, Lco/uk/getmondo/settings/f$c;-><init>(Lco/uk/getmondo/settings/f;Landroid/content/res/Resources;Ljava/lang/String;Ljava/lang/CharSequence;Ljava/lang/String;Ljava/lang/String;Lco/uk/getmondo/settings/f$b;ILkotlin/d/b/i;)V

    goto :goto_2

    :cond_2
    new-instance v2, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {v2}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw v2

    .line 206
    :cond_3
    nop

    .line 102
    goto/16 :goto_0

    .line 119
    :cond_4
    invoke-virtual/range {p0 .. p0}, Lco/uk/getmondo/settings/f;->notifyDataSetChanged()V

    .line 120
    return-void
.end method

.method public a(Lco/uk/getmondo/settings/f$a;I)V
    .locals 1

    .prologue
    const-string v0, "holder"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 47
    iget-object v0, p0, Lco/uk/getmondo/settings/f;->a:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/settings/f$c;

    invoke-virtual {p1, v0}, Lco/uk/getmondo/settings/f$a;->a(Lco/uk/getmondo/settings/f$c;)V

    .line 48
    return-void
.end method

.method public a(Lco/uk/getmondo/settings/f$d;I)V
    .locals 1

    .prologue
    const-string v0, "holder"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 36
    iget-object v0, p0, Lco/uk/getmondo/settings/f;->a:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/settings/f$c;

    invoke-virtual {p1, v0}, Lco/uk/getmondo/settings/f$d;->a(Lco/uk/getmondo/settings/f$c;)V

    .line 37
    return-void
.end method

.method public b(Landroid/view/ViewGroup;)Lco/uk/getmondo/settings/f$a;
    .locals 4

    .prologue
    const-string v0, "parent"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 43
    new-instance v0, Lco/uk/getmondo/settings/f$a;

    .line 44
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f050103

    const/4 v3, 0x0

    invoke-virtual {v1, v2, p1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const-string v2, "LayoutInflater.from(pare\u2026em_header, parent, false)"

    invoke-static {v1, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 43
    invoke-direct {v0, v1}, Lco/uk/getmondo/settings/f$a;-><init>(Landroid/view/View;)V

    .line 44
    return-object v0
.end method

.method public getItemCount()I
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lco/uk/getmondo/settings/f;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public synthetic onBindViewHolder(Landroid/support/v7/widget/RecyclerView$w;I)V
    .locals 0

    .prologue
    .line 25
    check-cast p1, Lco/uk/getmondo/settings/f$d;

    invoke-virtual {p0, p1, p2}, Lco/uk/getmondo/settings/f;->a(Lco/uk/getmondo/settings/f$d;I)V

    return-void
.end method

.method public synthetic onCreateViewHolder(Landroid/view/ViewGroup;I)Landroid/support/v7/widget/RecyclerView$w;
    .locals 1

    .prologue
    .line 25
    invoke-virtual {p0, p1, p2}, Lco/uk/getmondo/settings/f;->a(Landroid/view/ViewGroup;I)Lco/uk/getmondo/settings/f$d;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView$w;

    return-object v0
.end method
