.class public Lco/uk/getmondo/settings/SwitchView_ViewBinding;
.super Ljava/lang/Object;
.source "SwitchView_ViewBinding.java"

# interfaces
.implements Lbutterknife/Unbinder;


# instance fields
.field private a:Lco/uk/getmondo/settings/SwitchView;


# direct methods
.method public constructor <init>(Lco/uk/getmondo/settings/SwitchView;Landroid/view/View;)V
    .locals 3

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    iput-object p1, p0, Lco/uk/getmondo/settings/SwitchView_ViewBinding;->a:Lco/uk/getmondo/settings/SwitchView;

    .line 28
    const v0, 0x7f1104be

    const-string v1, "field \'titleTextView\'"

    const-class v2, Landroid/widget/TextView;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p1, Lco/uk/getmondo/settings/SwitchView;->titleTextView:Landroid/widget/TextView;

    .line 29
    const v0, 0x7f1104c1

    const-string v1, "field \'descriptionTextView\'"

    const-class v2, Landroid/widget/TextView;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p1, Lco/uk/getmondo/settings/SwitchView;->descriptionTextView:Landroid/widget/TextView;

    .line 30
    const v0, 0x7f1104c0

    const-string v1, "field \'settingSwitch\'"

    const-class v2, Landroid/widget/Switch;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/Switch;

    iput-object v0, p1, Lco/uk/getmondo/settings/SwitchView;->settingSwitch:Landroid/widget/Switch;

    .line 31
    const v0, 0x7f1104bf

    const-string v1, "field \'progressBar\'"

    const-class v2, Landroid/widget/ProgressBar;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p1, Lco/uk/getmondo/settings/SwitchView;->progressBar:Landroid/widget/ProgressBar;

    .line 32
    return-void
.end method


# virtual methods
.method public unbind()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 37
    iget-object v0, p0, Lco/uk/getmondo/settings/SwitchView_ViewBinding;->a:Lco/uk/getmondo/settings/SwitchView;

    .line 38
    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Bindings already cleared."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 39
    :cond_0
    iput-object v1, p0, Lco/uk/getmondo/settings/SwitchView_ViewBinding;->a:Lco/uk/getmondo/settings/SwitchView;

    .line 41
    iput-object v1, v0, Lco/uk/getmondo/settings/SwitchView;->titleTextView:Landroid/widget/TextView;

    .line 42
    iput-object v1, v0, Lco/uk/getmondo/settings/SwitchView;->descriptionTextView:Landroid/widget/TextView;

    .line 43
    iput-object v1, v0, Lco/uk/getmondo/settings/SwitchView;->settingSwitch:Landroid/widget/Switch;

    .line 44
    iput-object v1, v0, Lco/uk/getmondo/settings/SwitchView;->progressBar:Landroid/widget/ProgressBar;

    .line 45
    return-void
.end method
