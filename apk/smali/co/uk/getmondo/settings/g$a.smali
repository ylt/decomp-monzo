.class public final Lco/uk/getmondo/settings/g$a;
.super Ljava/lang/Object;
.source "LimitsDetailsDialogFragment.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lco/uk/getmondo/settings/g;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0008\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u000e\u0010\u0006\u001a\u00020\u00072\u0006\u0010\u0008\u001a\u00020\tR\u000e\u0010\u0003\u001a\u00020\u0004X\u0082T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0082T\u00a2\u0006\u0002\n\u0000\u00a8\u0006\n"
    }
    d2 = {
        "Lco/uk/getmondo/settings/LimitsDetailsDialogFragment$Companion;",
        "",
        "()V",
        "KEY_DETAILS",
        "",
        "KEY_TITLE",
        "newInstance",
        "Lco/uk/getmondo/settings/LimitsDetailsDialogFragment;",
        "limitDetails",
        "Lco/uk/getmondo/settings/LimitsAdapter$LimitDetails;",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/d/b/i;)V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Lco/uk/getmondo/settings/g$a;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lco/uk/getmondo/settings/f$b;)Lco/uk/getmondo/settings/g;
    .locals 4

    .prologue
    const-string v0, "limitDetails"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 24
    new-instance v0, Lco/uk/getmondo/settings/g;

    invoke-direct {v0}, Lco/uk/getmondo/settings/g;-><init>()V

    .line 25
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 26
    const-string v2, "KEY_TITLE"

    invoke-virtual {p1}, Lco/uk/getmondo/settings/f$b;->a()I

    move-result v3

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 27
    const-string v2, "KEY_DETAILS"

    invoke-virtual {p1}, Lco/uk/getmondo/settings/f$b;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 28
    invoke-virtual {v0, v1}, Lco/uk/getmondo/settings/g;->setArguments(Landroid/os/Bundle;)V

    .line 29
    return-object v0
.end method
