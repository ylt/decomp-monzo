.class public final Lco/uk/getmondo/settings/LimitsActivity;
.super Lco/uk/getmondo/common/activities/b;
.source "LimitsActivity.kt"

# interfaces
.implements Lco/uk/getmondo/settings/h$a;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lco/uk/getmondo/settings/LimitsActivity$a;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000J\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0008\u0007\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0004\u0018\u0000 %2\u00020\u00012\u00020\u0002:\u0001%B\u0005\u00a2\u0006\u0002\u0010\u0003J\u0008\u0010\u0013\u001a\u00020\u0008H\u0016J\u0012\u0010\u0014\u001a\u00020\u00082\u0008\u0010\u0015\u001a\u0004\u0018\u00010\u0016H\u0016J\u0008\u0010\u0017\u001a\u00020\u0008H\u0014J\u0008\u0010\u0018\u001a\u00020\u0008H\u0016J\u0010\u0010\u0019\u001a\u00020\u00082\u0006\u0010\u001a\u001a\u00020\u001bH\u0016J\u0010\u0010\u001c\u001a\u00020\u00082\u0006\u0010\u001d\u001a\u00020\u001eH\u0016J\u0008\u0010\u001f\u001a\u00020\u0008H\u0016J\u0010\u0010 \u001a\u00020\u00082\u0006\u0010!\u001a\u00020\"H\u0002J\u0008\u0010#\u001a\u00020\u0008H\u0016J\u0008\u0010$\u001a\u00020\u0008H\u0016R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082.\u00a2\u0006\u0002\n\u0000R\u001a\u0010\u0006\u001a\u0008\u0012\u0004\u0012\u00020\u00080\u00078VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\t\u0010\nR\u001a\u0010\u000b\u001a\u0008\u0012\u0004\u0012\u00020\u00080\u00078VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u000c\u0010\nR\u001e\u0010\r\u001a\u00020\u000e8\u0000@\u0000X\u0081.\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u000f\u0010\u0010\"\u0004\u0008\u0011\u0010\u0012\u00a8\u0006&"
    }
    d2 = {
        "Lco/uk/getmondo/settings/LimitsActivity;",
        "Lco/uk/getmondo/common/activities/BaseActivity;",
        "Lco/uk/getmondo/settings/LimitsPresenter$View;",
        "()V",
        "adapter",
        "Lco/uk/getmondo/settings/LimitsAdapter;",
        "onRequestHigherLimitsClicked",
        "Lio/reactivex/Observable;",
        "",
        "getOnRequestHigherLimitsClicked",
        "()Lio/reactivex/Observable;",
        "onRetryClicked",
        "getOnRetryClicked",
        "presenter",
        "Lco/uk/getmondo/settings/LimitsPresenter;",
        "getPresenter$app_monzoPrepaidRelease",
        "()Lco/uk/getmondo/settings/LimitsPresenter;",
        "setPresenter$app_monzoPrepaidRelease",
        "(Lco/uk/getmondo/settings/LimitsPresenter;)V",
        "hideLoading",
        "onCreate",
        "savedInstanceState",
        "Landroid/os/Bundle;",
        "onDestroy",
        "openKycOnboarding",
        "setBalanceLimitPrepaid",
        "balanceLimit",
        "Lco/uk/getmondo/model/BalanceLimit;",
        "setPaymentLimits",
        "paymentLimits",
        "Lco/uk/getmondo/model/PaymentLimits;",
        "showError",
        "showLimitDetails",
        "limitDetails",
        "Lco/uk/getmondo/settings/LimitsAdapter$LimitDetails;",
        "showLoading",
        "showRequestHigherLimitsInfo",
        "Companion",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# static fields
.field public static final b:Lco/uk/getmondo/settings/LimitsActivity$a;


# instance fields
.field public a:Lco/uk/getmondo/settings/h;

.field private c:Lco/uk/getmondo/settings/f;

.field private e:Ljava/util/HashMap;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lco/uk/getmondo/settings/LimitsActivity$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lco/uk/getmondo/settings/LimitsActivity$a;-><init>(Lkotlin/d/b/i;)V

    sput-object v0, Lco/uk/getmondo/settings/LimitsActivity;->b:Lco/uk/getmondo/settings/LimitsActivity$a;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, Lco/uk/getmondo/common/activities/b;-><init>()V

    return-void
.end method

.method public static final a(Landroid/content/Context;)V
    .locals 1

    const-string v0, "context"

    invoke-static {p0, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v0, Lco/uk/getmondo/settings/LimitsActivity;->b:Lco/uk/getmondo/settings/LimitsActivity$a;

    invoke-virtual {v0, p0}, Lco/uk/getmondo/settings/LimitsActivity$a;->a(Landroid/content/Context;)V

    return-void
.end method

.method public static final synthetic a(Lco/uk/getmondo/settings/LimitsActivity;Lco/uk/getmondo/settings/f$b;)V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0, p1}, Lco/uk/getmondo/settings/LimitsActivity;->a(Lco/uk/getmondo/settings/f$b;)V

    return-void
.end method

.method private final a(Lco/uk/getmondo/settings/f$b;)V
    .locals 3

    .prologue
    .line 86
    sget-object v0, Lco/uk/getmondo/settings/g;->a:Lco/uk/getmondo/settings/g$a;

    invoke-virtual {v0, p1}, Lco/uk/getmondo/settings/g$a;->a(Lco/uk/getmondo/settings/f$b;)Lco/uk/getmondo/settings/g;

    move-result-object v0

    invoke-virtual {p0}, Lco/uk/getmondo/settings/LimitsActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const-string v2, "TAG_LIMITS_DETAILS"

    invoke-virtual {v0, v1, v2}, Lco/uk/getmondo/settings/g;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    .line 87
    return-void
.end method


# virtual methods
.method public a(I)Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lco/uk/getmondo/settings/LimitsActivity;->e:Ljava/util/HashMap;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lco/uk/getmondo/settings/LimitsActivity;->e:Ljava/util/HashMap;

    :cond_0
    iget-object v0, p0, Lco/uk/getmondo/settings/LimitsActivity;->e:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    if-nez v0, :cond_1

    invoke-virtual {p0, p1}, Landroid/support/v4/app/j;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/settings/LimitsActivity;->e:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    return-object v0
.end method

.method public a()Lio/reactivex/n;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Lkotlin/n;",
            ">;"
        }
    .end annotation

    .prologue
    .line 57
    sget v0, Lco/uk/getmondo/c$a;->limitsRequestHigherButton:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/settings/LimitsActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 107
    invoke-static {v0}, Lcom/b/a/c/c;->a(Landroid/view/View;)Lio/reactivex/n;

    move-result-object v1

    sget-object v0, Lcom/b/a/a/d;->a:Lcom/b/a/a/d;

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {v1, v0}, Lio/reactivex/n;->map(Lio/reactivex/c/h;)Lio/reactivex/n;

    move-result-object v0

    const-string v1, "RxView.clicks(this).map(VoidToUnit)"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 57
    return-object v0
.end method

.method public a(Lco/uk/getmondo/d/e;)V
    .locals 3

    .prologue
    const-string v0, "balanceLimit"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 63
    sget v0, Lco/uk/getmondo/c$a;->limitsSubtitleTextView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/settings/LimitsActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-static {v0}, Lco/uk/getmondo/common/ae;->a(Landroid/view/View;)V

    .line 64
    sget v0, Lco/uk/getmondo/c$a;->limitsSubtitleTextView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/settings/LimitsActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {p1}, Lco/uk/getmondo/d/e;->a()Lco/uk/getmondo/api/model/VerificationType;

    move-result-object v1

    invoke-virtual {v1}, Lco/uk/getmondo/api/model/VerificationType;->b()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 65
    iget-object v0, p0, Lco/uk/getmondo/settings/LimitsActivity;->c:Lco/uk/getmondo/settings/f;

    if-nez v0, :cond_0

    const-string v1, "adapter"

    invoke-static {v1}, Lkotlin/d/b/l;->b(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {p0}, Lco/uk/getmondo/settings/LimitsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const-string v2, "resources"

    invoke-static {v1, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0, p1, v1}, Lco/uk/getmondo/settings/f;->a(Lco/uk/getmondo/d/e;Landroid/content/res/Resources;)V

    .line 66
    return-void
.end method

.method public a(Lco/uk/getmondo/d/y;)V
    .locals 3

    .prologue
    const-string v0, "paymentLimits"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 69
    iget-object v0, p0, Lco/uk/getmondo/settings/LimitsActivity;->c:Lco/uk/getmondo/settings/f;

    if-nez v0, :cond_0

    const-string v1, "adapter"

    invoke-static {v1}, Lkotlin/d/b/l;->b(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {p0}, Lco/uk/getmondo/settings/LimitsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const-string v2, "resources"

    invoke-static {v1, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0, p1, v1}, Lco/uk/getmondo/settings/f;->a(Lco/uk/getmondo/d/y;Landroid/content/res/Resources;)V

    .line 70
    return-void
.end method

.method public b()Lio/reactivex/n;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Lkotlin/n;",
            ">;"
        }
    .end annotation

    .prologue
    .line 60
    sget v0, Lco/uk/getmondo/c$a;->limitsErrorView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/settings/LimitsActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/common/ui/LoadingErrorView;

    invoke-virtual {v0}, Lco/uk/getmondo/common/ui/LoadingErrorView;->c()Lio/reactivex/n;

    move-result-object v0

    return-object v0
.end method

.method public c()V
    .locals 1

    .prologue
    .line 73
    sget v0, Lco/uk/getmondo/c$a;->limitsProgressBar:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/settings/LimitsActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    invoke-static {v0}, Lco/uk/getmondo/common/ae;->a(Landroid/view/View;)V

    .line 74
    sget v0, Lco/uk/getmondo/c$a;->limitsErrorView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/settings/LimitsActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/common/ui/LoadingErrorView;

    invoke-static {v0}, Lco/uk/getmondo/common/ae;->b(Landroid/view/View;)V

    .line 75
    return-void
.end method

.method public d()V
    .locals 1

    .prologue
    .line 78
    sget v0, Lco/uk/getmondo/c$a;->limitsProgressBar:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/settings/LimitsActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    invoke-static {v0}, Lco/uk/getmondo/common/ae;->b(Landroid/view/View;)V

    .line 79
    return-void
.end method

.method public e()V
    .locals 1

    .prologue
    .line 82
    sget v0, Lco/uk/getmondo/c$a;->limitsErrorView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/settings/LimitsActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/common/ui/LoadingErrorView;

    invoke-static {v0}, Lco/uk/getmondo/common/ae;->a(Landroid/view/View;)V

    .line 83
    return-void
.end method

.method public f()V
    .locals 1

    .prologue
    .line 90
    sget v0, Lco/uk/getmondo/c$a;->limitsRequestHigherViewGroup:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/settings/LimitsActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    invoke-static {v0}, Lco/uk/getmondo/common/ae;->a(Landroid/view/View;)V

    .line 91
    return-void
.end method

.method public g()V
    .locals 9

    .prologue
    const/4 v5, 0x0

    .line 94
    sget-object v0, Lco/uk/getmondo/signup/identity_verification/IdentityVerificationActivity;->g:Lco/uk/getmondo/signup/identity_verification/IdentityVerificationActivity$a;

    move-object v1, p0

    .line 95
    check-cast v1, Landroid/content/Context;

    sget-object v2, Lco/uk/getmondo/signup/identity_verification/a/j;->a:Lco/uk/getmondo/signup/identity_verification/a/j;

    sget-object v3, Lco/uk/getmondo/api/model/tracking/Impression$KycFrom;->LIMITS:Lco/uk/getmondo/api/model/tracking/Impression$KycFrom;

    sget-object v4, Lco/uk/getmondo/api/model/signup/SignupSource;->LEGACY_PREPAID:Lco/uk/getmondo/api/model/signup/SignupSource;

    const/16 v7, 0x30

    move-object v6, v5

    move-object v8, v5

    .line 94
    invoke-static/range {v0 .. v8}, Lco/uk/getmondo/signup/identity_verification/IdentityVerificationActivity$a;->a(Lco/uk/getmondo/signup/identity_verification/IdentityVerificationActivity$a;Landroid/content/Context;Lco/uk/getmondo/signup/identity_verification/a/j;Lco/uk/getmondo/api/model/tracking/Impression$KycFrom;Lco/uk/getmondo/api/model/signup/SignupSource;Lco/uk/getmondo/signup/j;Ljava/lang/String;ILjava/lang/Object;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lco/uk/getmondo/settings/LimitsActivity;->startActivity(Landroid/content/Intent;)V

    .line 96
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    .line 34
    invoke-super {p0, p1}, Lco/uk/getmondo/common/activities/b;->onCreate(Landroid/os/Bundle;)V

    .line 36
    const v0, 0x7f050048

    invoke-virtual {p0, v0}, Lco/uk/getmondo/settings/LimitsActivity;->setContentView(I)V

    move-object v0, p0

    .line 37
    check-cast v0, Landroid/app/Activity;

    invoke-static {v0}, Lbutterknife/ButterKnife;->bind(Landroid/app/Activity;)Lbutterknife/Unbinder;

    .line 39
    invoke-virtual {p0}, Lco/uk/getmondo/settings/LimitsActivity;->l()Lco/uk/getmondo/common/h/b/b;

    move-result-object v0

    invoke-interface {v0, p0}, Lco/uk/getmondo/common/h/b/b;->a(Lco/uk/getmondo/settings/LimitsActivity;)V

    .line 40
    iget-object v1, p0, Lco/uk/getmondo/settings/LimitsActivity;->a:Lco/uk/getmondo/settings/h;

    if-nez v1, :cond_0

    const-string v0, "presenter"

    invoke-static {v0}, Lkotlin/d/b/l;->b(Ljava/lang/String;)V

    :cond_0
    move-object v0, p0

    check-cast v0, Lco/uk/getmondo/settings/h$a;

    invoke-virtual {v1, v0}, Lco/uk/getmondo/settings/h;->a(Lco/uk/getmondo/settings/h$a;)V

    .line 42
    new-instance v1, Lco/uk/getmondo/settings/f;

    new-instance v0, Lco/uk/getmondo/settings/LimitsActivity$b;

    invoke-direct {v0, p0}, Lco/uk/getmondo/settings/LimitsActivity$b;-><init>(Lco/uk/getmondo/settings/LimitsActivity;)V

    check-cast v0, Lkotlin/d/a/b;

    invoke-direct {v1, v0}, Lco/uk/getmondo/settings/f;-><init>(Lkotlin/d/a/b;)V

    iput-object v1, p0, Lco/uk/getmondo/settings/LimitsActivity;->c:Lco/uk/getmondo/settings/f;

    .line 43
    sget v0, Lco/uk/getmondo/c$a;->limitsRecyclerView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/settings/LimitsActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView;

    new-instance v3, Lco/uk/getmondo/common/ui/h;

    move-object v1, p0

    check-cast v1, Landroid/content/Context;

    iget-object v2, p0, Lco/uk/getmondo/settings/LimitsActivity;->c:Lco/uk/getmondo/settings/f;

    if-nez v2, :cond_1

    const-string v4, "adapter"

    invoke-static {v4}, Lkotlin/d/b/l;->b(Ljava/lang/String;)V

    :cond_1
    check-cast v2, La/a/a/a/a/a;

    const/4 v4, 0x0

    invoke-direct {v3, v1, v2, v4}, Lco/uk/getmondo/common/ui/h;-><init>(Landroid/content/Context;La/a/a/a/a/a;I)V

    move-object v1, v3

    check-cast v1, Landroid/support/v7/widget/RecyclerView$g;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->a(Landroid/support/v7/widget/RecyclerView$g;)V

    .line 44
    sget v0, Lco/uk/getmondo/c$a;->limitsRecyclerView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/settings/LimitsActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setHasFixedSize(Z)V

    .line 45
    sget v0, Lco/uk/getmondo/c$a;->limitsRecyclerView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/settings/LimitsActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView;

    new-instance v2, Landroid/support/v7/widget/LinearLayoutManager;

    move-object v1, p0

    check-cast v1, Landroid/content/Context;

    invoke-direct {v2, v1}, Landroid/support/v7/widget/LinearLayoutManager;-><init>(Landroid/content/Context;)V

    move-object v1, v2

    check-cast v1, Landroid/support/v7/widget/RecyclerView$h;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(Landroid/support/v7/widget/RecyclerView$h;)V

    .line 46
    sget v0, Lco/uk/getmondo/c$a;->limitsRecyclerView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/settings/LimitsActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView;

    iget-object v1, p0, Lco/uk/getmondo/settings/LimitsActivity;->c:Lco/uk/getmondo/settings/f;

    if-nez v1, :cond_2

    const-string v2, "adapter"

    invoke-static {v2}, Lkotlin/d/b/l;->b(Ljava/lang/String;)V

    :cond_2
    check-cast v1, Landroid/support/v7/widget/RecyclerView$a;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setAdapter(Landroid/support/v7/widget/RecyclerView$a;)V

    .line 47
    sget v0, Lco/uk/getmondo/c$a;->limitsRecyclerView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/settings/LimitsActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView;

    new-instance v2, La/a/a/a/a/b;

    iget-object v1, p0, Lco/uk/getmondo/settings/LimitsActivity;->c:Lco/uk/getmondo/settings/f;

    if-nez v1, :cond_3

    const-string v3, "adapter"

    invoke-static {v3}, Lkotlin/d/b/l;->b(Ljava/lang/String;)V

    :cond_3
    check-cast v1, La/a/a/a/a/a;

    invoke-direct {v2, v1}, La/a/a/a/a/b;-><init>(La/a/a/a/a/a;)V

    move-object v1, v2

    check-cast v1, Landroid/support/v7/widget/RecyclerView$g;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->a(Landroid/support/v7/widget/RecyclerView$g;)V

    .line 48
    return-void
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    .line 51
    iget-object v0, p0, Lco/uk/getmondo/settings/LimitsActivity;->a:Lco/uk/getmondo/settings/h;

    if-nez v0, :cond_0

    const-string v1, "presenter"

    invoke-static {v1}, Lkotlin/d/b/l;->b(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0}, Lco/uk/getmondo/settings/h;->b()V

    .line 53
    invoke-super {p0}, Lco/uk/getmondo/common/activities/b;->onDestroy()V

    .line 54
    return-void
.end method
