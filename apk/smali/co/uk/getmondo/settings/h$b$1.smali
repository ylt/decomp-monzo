.class final Lco/uk/getmondo/settings/h$b$1;
.super Ljava/lang/Object;
.source "LimitsPresenter.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lco/uk/getmondo/settings/h$b;->a(Lkotlin/n;)Lio/reactivex/h;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h",
        "<TT;TR;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\n \u0002*\u0004\u0018\u00010\u00010\u00012\u0006\u0010\u0003\u001a\u00020\u0004H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "Lco/uk/getmondo/model/BalanceLimit;",
        "kotlin.jvm.PlatformType",
        "apiBalanceLimits",
        "Lco/uk/getmondo/api/model/ApiBalanceLimits;",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# static fields
.field public static final a:Lco/uk/getmondo/settings/h$b$1;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lco/uk/getmondo/settings/h$b$1;

    invoke-direct {v0}, Lco/uk/getmondo/settings/h$b$1;-><init>()V

    sput-object v0, Lco/uk/getmondo/settings/h$b$1;->a:Lco/uk/getmondo/settings/h$b$1;

    return-void
.end method

.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lco/uk/getmondo/api/model/ApiBalanceLimits;)Lco/uk/getmondo/d/e;
    .locals 1

    .prologue
    const-string v0, "apiBalanceLimits"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 51
    new-instance v0, Lco/uk/getmondo/d/a/c;

    invoke-direct {v0}, Lco/uk/getmondo/d/a/c;-><init>()V

    invoke-virtual {v0, p1}, Lco/uk/getmondo/d/a/c;->a(Lco/uk/getmondo/api/model/ApiBalanceLimits;)Lco/uk/getmondo/d/e;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 30
    check-cast p1, Lco/uk/getmondo/api/model/ApiBalanceLimits;

    invoke-virtual {p0, p1}, Lco/uk/getmondo/settings/h$b$1;->a(Lco/uk/getmondo/api/model/ApiBalanceLimits;)Lco/uk/getmondo/d/e;

    move-result-object v0

    return-object v0
.end method
