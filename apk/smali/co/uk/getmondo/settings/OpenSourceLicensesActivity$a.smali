.class final enum Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;
.super Ljava/lang/Enum;
.source "OpenSourceLicensesActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lco/uk/getmondo/settings/OpenSourceLicensesActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic A:[Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;

.field public static final enum a:Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;

.field public static final enum b:Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;

.field public static final enum c:Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;

.field public static final enum d:Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;

.field public static final enum e:Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;

.field public static final enum f:Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;

.field public static final enum g:Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;

.field public static final enum h:Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;

.field public static final enum i:Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;

.field public static final enum j:Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;

.field public static final enum k:Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;

.field public static final enum l:Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;

.field public static final enum m:Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;

.field public static final enum n:Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;

.field public static final enum o:Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;

.field public static final enum p:Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;

.field public static final enum q:Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;

.field public static final enum r:Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;

.field public static final enum s:Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;

.field public static final enum t:Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;

.field public static final enum u:Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;

.field public static final enum v:Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;

.field public static final enum w:Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;

.field public static final enum x:Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;


# instance fields
.field private final y:I

.field private final z:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 218
    new-instance v0, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;

    const-string v1, "AMULYA_KHARE"

    const-string v2, "Amulya Khare"

    invoke-direct {v0, v1, v4, v2}, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;->a:Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;

    .line 219
    new-instance v0, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;

    const-string v1, "ANDROID"

    const-string v2, "Android"

    invoke-direct {v0, v1, v5, v2}, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;->b:Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;

    .line 220
    new-instance v0, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;

    const-string v1, "ARTEM_ZIM"

    const-string v2, "Artem Zinnatullin"

    invoke-direct {v0, v1, v6, v2}, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;->c:Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;

    .line 221
    new-instance v0, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;

    const-string v1, "CHRIS_BANES"

    const-string v2, "Chris Banes"

    invoke-direct {v0, v1, v7, v2}, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;->d:Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;

    .line 222
    new-instance v0, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;

    const-string v1, "EDUARDO_BARRENECHEA"

    const-string v2, "Eduardo Barrenechea"

    invoke-direct {v0, v1, v8, v2}, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;->e:Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;

    .line 223
    new-instance v0, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;

    const-string v1, "ESKO_LUNTOLA"

    const/4 v2, 0x5

    const-string v3, "Esko Luontola and contributors"

    invoke-direct {v0, v1, v2, v3}, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;->f:Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;

    .line 224
    new-instance v0, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;

    const-string v1, "FACEBOOK"

    const/4 v2, 0x6

    const-string v3, "Facebook"

    invoke-direct {v0, v1, v2, v3}, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;->g:Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;

    .line 225
    new-instance v0, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;

    const-string v1, "GOOGLE"

    const/4 v2, 0x7

    const-string v3, "Google"

    invoke-direct {v0, v1, v2, v3}, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;->h:Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;

    .line 226
    new-instance v0, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;

    const-string v1, "JAKE_WHARTON"

    const/16 v2, 0x8

    const-string v3, "Jake Wharton"

    invoke-direct {v0, v1, v2, v3}, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;->i:Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;

    .line 227
    new-instance v0, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;

    const-string v1, "AIRBNB"

    const/16 v2, 0x9

    const-string v3, "Airbnb"

    invoke-direct {v0, v1, v2, v3}, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;->j:Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;

    .line 228
    new-instance v0, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;

    const-string v1, "MEMOIZR"

    const/16 v2, 0xa

    const-string v3, "memoizr"

    invoke-direct {v0, v1, v2, v3}, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;->k:Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;

    .line 229
    new-instance v0, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;

    const-string v1, "MICHEAL_ROCKS"

    const/16 v2, 0xb

    const-string v3, "Michael Rozumyanskiy"

    invoke-direct {v0, v1, v2, v3}, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;->l:Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;

    .line 230
    new-instance v0, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;

    const-string v1, "MONZO"

    const/16 v2, 0xc

    const-string v3, "Monzo"

    invoke-direct {v0, v1, v2, v3}, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;->m:Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;

    .line 231
    new-instance v0, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;

    const-string v1, "REACTIVE_X"

    const/16 v2, 0xd

    const-string v3, "ReactiveX"

    invoke-direct {v0, v1, v2, v3}, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;->n:Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;

    .line 232
    new-instance v0, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;

    const-string v1, "RELEX"

    const/16 v2, 0xe

    const-string v3, "relex"

    invoke-direct {v0, v1, v2, v3}, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;->o:Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;

    .line 233
    new-instance v0, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;

    const-string v1, "SQUARE"

    const/16 v2, 0xf

    const-string v3, "Square"

    invoke-direct {v0, v1, v2, v3}, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;->p:Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;

    .line 234
    new-instance v0, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;

    const-string v1, "STRIPE"

    const/16 v2, 0x10

    const-string v3, "Stripe"

    invoke-direct {v0, v1, v2, v3}, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;->q:Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;

    .line 235
    new-instance v0, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;

    const-string v1, "SANNIES"

    const/16 v2, 0x11

    const-string v3, "sannies"

    invoke-direct {v0, v1, v2, v3}, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;->r:Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;

    .line 236
    new-instance v0, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;

    const-string v1, "JUNIT"

    const/16 v2, 0x12

    const-string v3, "JUnit"

    invoke-direct {v0, v1, v2, v3}, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;->s:Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;

    .line 237
    new-instance v0, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;

    const-string v1, "ASSERTJ_CONTRIB"

    const/16 v2, 0x13

    const-string v3, "AssertJ contributors"

    invoke-direct {v0, v1, v2, v3}, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;->t:Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;

    .line 238
    new-instance v0, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;

    const-string v1, "MOCKITO_CONTRIB"

    const/16 v2, 0x14

    const-string v3, "Mockito contributors"

    invoke-direct {v0, v1, v2, v3}, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;->u:Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;

    .line 239
    new-instance v0, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;

    const-string v1, "LINKEDIN"

    const/16 v2, 0x15

    const-string v3, "LinkedIn"

    invoke-direct {v0, v1, v2, v3}, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;->v:Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;

    .line 240
    new-instance v0, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;

    const-string v1, "REALM"

    const/16 v2, 0x16

    const-string v3, "Realm"

    invoke-direct {v0, v1, v2, v3}, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;->w:Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;

    .line 241
    new-instance v0, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;

    const-string v1, "JODA_ORG"

    const/16 v2, 0x17

    const-string v3, "JodaOrg"

    invoke-direct {v0, v1, v2, v3}, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;->x:Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;

    .line 217
    const/16 v0, 0x18

    new-array v0, v0, [Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;

    sget-object v1, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;->a:Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;

    aput-object v1, v0, v4

    sget-object v1, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;->b:Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;

    aput-object v1, v0, v5

    sget-object v1, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;->c:Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;

    aput-object v1, v0, v6

    sget-object v1, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;->d:Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;

    aput-object v1, v0, v7

    sget-object v1, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;->e:Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;->f:Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;->g:Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;->h:Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;->i:Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;->j:Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;->k:Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;->l:Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;->m:Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;->n:Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;->o:Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;->p:Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;->q:Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;->r:Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;->s:Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;->t:Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;->u:Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;->v:Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;

    aput-object v2, v0, v1

    const/16 v1, 0x16

    sget-object v2, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;->w:Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;

    aput-object v2, v0, v1

    const/16 v1, 0x17

    sget-object v2, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;->x:Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;

    aput-object v2, v0, v1

    sput-object v0, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;->A:[Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 246
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 247
    invoke-static {}, Lco/uk/getmondo/settings/OpenSourceLicensesActivity;->a()Ljava/util/concurrent/atomic/AtomicInteger;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->getAndIncrement()I

    move-result v0

    iput v0, p0, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;->y:I

    .line 248
    iput-object p3, p0, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;->z:Ljava/lang/String;

    .line 249
    return-void
.end method

.method static synthetic a(Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;)I
    .locals 1

    .prologue
    .line 217
    iget v0, p0, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;->y:I

    return v0
.end method

.method static synthetic b(Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 217
    iget-object v0, p0, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;->z:Ljava/lang/String;

    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;
    .locals 1

    .prologue
    .line 217
    const-class v0, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;

    return-object v0
.end method

.method public static values()[Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;
    .locals 1

    .prologue
    .line 217
    sget-object v0, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;->A:[Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;

    invoke-virtual {v0}, [Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;

    return-object v0
.end method
