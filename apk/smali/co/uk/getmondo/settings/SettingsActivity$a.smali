.class public final Lco/uk/getmondo/settings/SettingsActivity$a;
.super Ljava/lang/Object;
.source "SettingsActivity.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lco/uk/getmondo/settings/SettingsActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000&\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0002\u0008\u000b\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\u0008\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u000e\u0010\u000f\u001a\u00020\u00102\u0006\u0010\u0011\u001a\u00020\u0012J\u0010\u0010\u0013\u001a\u00020\u00142\u0006\u0010\u0011\u001a\u00020\u0012H\u0007R\u0014\u0010\u0003\u001a\u00020\u0004X\u0082D\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0005\u0010\u0006R\u0014\u0010\u0007\u001a\u00020\u0004X\u0082D\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0008\u0010\u0006R\u0014\u0010\t\u001a\u00020\u0004X\u0082D\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\n\u0010\u0006R\u0014\u0010\u000b\u001a\u00020\u0004X\u0082D\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000c\u0010\u0006R\u0014\u0010\r\u001a\u00020\u0004X\u0082D\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000e\u0010\u0006\u00a8\u0006\u0015"
    }
    d2 = {
        "Lco/uk/getmondo/settings/SettingsActivity$Companion;",
        "",
        "()V",
        "TAG_CHANGE_PERSONAL_INFORMATION",
        "",
        "getTAG_CHANGE_PERSONAL_INFORMATION",
        "()Ljava/lang/String;",
        "TAG_CLOSE_ACCOUNT",
        "getTAG_CLOSE_ACCOUNT",
        "TAG_ERROR_P2P_BLOCKED",
        "getTAG_ERROR_P2P_BLOCKED",
        "TAG_LOG_OUT_CONFIRMATION",
        "getTAG_LOG_OUT_CONFIRMATION",
        "TAG_UPDATE_ADDRESS",
        "getTAG_UPDATE_ADDRESS",
        "buildIntent",
        "Landroid/content/Intent;",
        "context",
        "Landroid/content/Context;",
        "start",
        "",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 271
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/d/b/i;)V
    .locals 0

    .prologue
    .line 271
    invoke-direct {p0}, Lco/uk/getmondo/settings/SettingsActivity$a;-><init>()V

    return-void
.end method

.method private final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 272
    invoke-static {}, Lco/uk/getmondo/settings/SettingsActivity;->Z()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static final synthetic a(Lco/uk/getmondo/settings/SettingsActivity$a;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 271
    invoke-direct {p0}, Lco/uk/getmondo/settings/SettingsActivity$a;->a()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 273
    invoke-static {}, Lco/uk/getmondo/settings/SettingsActivity;->aa()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static final synthetic b(Lco/uk/getmondo/settings/SettingsActivity$a;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 271
    invoke-direct {p0}, Lco/uk/getmondo/settings/SettingsActivity$a;->e()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 274
    invoke-static {}, Lco/uk/getmondo/settings/SettingsActivity;->ab()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static final synthetic c(Lco/uk/getmondo/settings/SettingsActivity$a;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 271
    invoke-direct {p0}, Lco/uk/getmondo/settings/SettingsActivity$a;->c()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private final d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 275
    invoke-static {}, Lco/uk/getmondo/settings/SettingsActivity;->ac()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static final synthetic d(Lco/uk/getmondo/settings/SettingsActivity$a;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 271
    invoke-direct {p0}, Lco/uk/getmondo/settings/SettingsActivity$a;->d()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private final e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 276
    invoke-static {}, Lco/uk/getmondo/settings/SettingsActivity;->ad()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static final synthetic e(Lco/uk/getmondo/settings/SettingsActivity$a;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 271
    invoke-direct {p0}, Lco/uk/getmondo/settings/SettingsActivity$a;->b()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(Landroid/content/Context;)V
    .locals 2

    .prologue
    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 280
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lco/uk/getmondo/settings/SettingsActivity;

    invoke-direct {v0, p1, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 281
    return-void
.end method

.method public final b(Landroid/content/Context;)Landroid/content/Intent;
    .locals 2

    .prologue
    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 284
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lco/uk/getmondo/settings/SettingsActivity;

    invoke-direct {v0, p1, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    return-object v0
.end method
