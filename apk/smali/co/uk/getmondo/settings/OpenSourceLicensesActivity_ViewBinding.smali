.class public Lco/uk/getmondo/settings/OpenSourceLicensesActivity_ViewBinding;
.super Ljava/lang/Object;
.source "OpenSourceLicensesActivity_ViewBinding.java"

# interfaces
.implements Lbutterknife/Unbinder;


# instance fields
.field private a:Lco/uk/getmondo/settings/OpenSourceLicensesActivity;


# direct methods
.method public constructor <init>(Lco/uk/getmondo/settings/OpenSourceLicensesActivity;Landroid/view/View;)V
    .locals 3

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    iput-object p1, p0, Lco/uk/getmondo/settings/OpenSourceLicensesActivity_ViewBinding;->a:Lco/uk/getmondo/settings/OpenSourceLicensesActivity;

    .line 29
    const v0, 0x7f1101d0

    const-string v1, "field \'recyclerView\'"

    const-class v2, Landroid/support/v7/widget/RecyclerView;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView;

    iput-object v0, p1, Lco/uk/getmondo/settings/OpenSourceLicensesActivity;->recyclerView:Landroid/support/v7/widget/RecyclerView;

    .line 30
    const v0, 0x7f1101cd

    const-string v1, "field \'appBarLayout\'"

    const-class v2, Landroid/support/design/widget/AppBarLayout;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/design/widget/AppBarLayout;

    iput-object v0, p1, Lco/uk/getmondo/settings/OpenSourceLicensesActivity;->appBarLayout:Landroid/support/design/widget/AppBarLayout;

    .line 31
    const v0, 0x7f1101ce

    const-string v1, "field \'collapsingToolbarLayout\'"

    const-class v2, Landroid/support/design/widget/CollapsingToolbarLayout;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/design/widget/CollapsingToolbarLayout;

    iput-object v0, p1, Lco/uk/getmondo/settings/OpenSourceLicensesActivity;->collapsingToolbarLayout:Landroid/support/design/widget/CollapsingToolbarLayout;

    .line 32
    const v0, 0x7f1101cf

    const-string v1, "field \'descriptionTextView\'"

    const-class v2, Landroid/widget/TextView;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p1, Lco/uk/getmondo/settings/OpenSourceLicensesActivity;->descriptionTextView:Landroid/widget/TextView;

    .line 33
    return-void
.end method


# virtual methods
.method public unbind()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 38
    iget-object v0, p0, Lco/uk/getmondo/settings/OpenSourceLicensesActivity_ViewBinding;->a:Lco/uk/getmondo/settings/OpenSourceLicensesActivity;

    .line 39
    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Bindings already cleared."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 40
    :cond_0
    iput-object v1, p0, Lco/uk/getmondo/settings/OpenSourceLicensesActivity_ViewBinding;->a:Lco/uk/getmondo/settings/OpenSourceLicensesActivity;

    .line 42
    iput-object v1, v0, Lco/uk/getmondo/settings/OpenSourceLicensesActivity;->recyclerView:Landroid/support/v7/widget/RecyclerView;

    .line 43
    iput-object v1, v0, Lco/uk/getmondo/settings/OpenSourceLicensesActivity;->appBarLayout:Landroid/support/design/widget/AppBarLayout;

    .line 44
    iput-object v1, v0, Lco/uk/getmondo/settings/OpenSourceLicensesActivity;->collapsingToolbarLayout:Landroid/support/design/widget/CollapsingToolbarLayout;

    .line 45
    iput-object v1, v0, Lco/uk/getmondo/settings/OpenSourceLicensesActivity;->descriptionTextView:Landroid/widget/TextView;

    .line 46
    return-void
.end method
