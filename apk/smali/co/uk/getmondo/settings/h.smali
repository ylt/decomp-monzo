.class public final Lco/uk/getmondo/settings/h;
.super Lco/uk/getmondo/common/ui/b;
.source "LimitsPresenter.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lco/uk/getmondo/settings/h$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lco/uk/getmondo/common/ui/b",
        "<",
        "Lco/uk/getmondo/settings/h$a;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000D\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u0003\u0008\u0001\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00020\u0001:\u0001\u0016BK\u0008\u0007\u0012\u0008\u0008\u0001\u0010\u0003\u001a\u00020\u0004\u0012\u0008\u0008\u0001\u0010\u0005\u001a\u00020\u0004\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b\u0012\u0006\u0010\u000c\u001a\u00020\r\u0012\u0006\u0010\u000e\u001a\u00020\u000f\u0012\u0006\u0010\u0010\u001a\u00020\u0011\u00a2\u0006\u0002\u0010\u0012J\u0010\u0010\u0013\u001a\u00020\u00142\u0006\u0010\u0015\u001a\u00020\u0002H\u0016R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000c\u001a\u00020\rX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u000fX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0010\u001a\u00020\u0011X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0017"
    }
    d2 = {
        "Lco/uk/getmondo/settings/LimitsPresenter;",
        "Lco/uk/getmondo/common/ui/BasePresenter;",
        "Lco/uk/getmondo/settings/LimitsPresenter$View;",
        "uiScheduler",
        "Lio/reactivex/Scheduler;",
        "ioScheduler",
        "accountService",
        "Lco/uk/getmondo/common/accounts/AccountService;",
        "monzoApi",
        "Lco/uk/getmondo/api/MonzoApi;",
        "paymentLimitsApi",
        "Lco/uk/getmondo/api/PaymentLimitsApi;",
        "analyticsService",
        "Lco/uk/getmondo/common/AnalyticsService;",
        "apiErrorHandler",
        "Lco/uk/getmondo/common/errors/ApiErrorHandler;",
        "identityVerificationStatus",
        "Lco/uk/getmondo/signup/identity_verification/data/IdentityVerificationStatus;",
        "(Lio/reactivex/Scheduler;Lio/reactivex/Scheduler;Lco/uk/getmondo/common/accounts/AccountService;Lco/uk/getmondo/api/MonzoApi;Lco/uk/getmondo/api/PaymentLimitsApi;Lco/uk/getmondo/common/AnalyticsService;Lco/uk/getmondo/common/errors/ApiErrorHandler;Lco/uk/getmondo/signup/identity_verification/data/IdentityVerificationStatus;)V",
        "register",
        "",
        "view",
        "View",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field private final c:Lio/reactivex/u;

.field private final d:Lio/reactivex/u;

.field private final e:Lco/uk/getmondo/common/accounts/d;

.field private final f:Lco/uk/getmondo/api/MonzoApi;

.field private final g:Lco/uk/getmondo/api/PaymentLimitsApi;

.field private final h:Lco/uk/getmondo/common/a;

.field private final i:Lco/uk/getmondo/common/e/a;

.field private final j:Lco/uk/getmondo/signup/identity_verification/a/f;


# direct methods
.method public constructor <init>(Lio/reactivex/u;Lio/reactivex/u;Lco/uk/getmondo/common/accounts/d;Lco/uk/getmondo/api/MonzoApi;Lco/uk/getmondo/api/PaymentLimitsApi;Lco/uk/getmondo/common/a;Lco/uk/getmondo/common/e/a;Lco/uk/getmondo/signup/identity_verification/a/f;)V
    .locals 1

    .prologue
    const-string v0, "uiScheduler"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "ioScheduler"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "accountService"

    invoke-static {p3, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "monzoApi"

    invoke-static {p4, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "paymentLimitsApi"

    invoke-static {p5, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "analyticsService"

    invoke-static {p6, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "apiErrorHandler"

    invoke-static {p7, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "identityVerificationStatus"

    invoke-static {p8, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 39
    invoke-direct {p0}, Lco/uk/getmondo/common/ui/b;-><init>()V

    iput-object p1, p0, Lco/uk/getmondo/settings/h;->c:Lio/reactivex/u;

    iput-object p2, p0, Lco/uk/getmondo/settings/h;->d:Lio/reactivex/u;

    iput-object p3, p0, Lco/uk/getmondo/settings/h;->e:Lco/uk/getmondo/common/accounts/d;

    iput-object p4, p0, Lco/uk/getmondo/settings/h;->f:Lco/uk/getmondo/api/MonzoApi;

    iput-object p5, p0, Lco/uk/getmondo/settings/h;->g:Lco/uk/getmondo/api/PaymentLimitsApi;

    iput-object p6, p0, Lco/uk/getmondo/settings/h;->h:Lco/uk/getmondo/common/a;

    iput-object p7, p0, Lco/uk/getmondo/settings/h;->i:Lco/uk/getmondo/common/e/a;

    iput-object p8, p0, Lco/uk/getmondo/settings/h;->j:Lco/uk/getmondo/signup/identity_verification/a/f;

    return-void
.end method

.method public static final synthetic a(Lco/uk/getmondo/settings/h;)Lco/uk/getmondo/api/MonzoApi;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lco/uk/getmondo/settings/h;->f:Lco/uk/getmondo/api/MonzoApi;

    return-object v0
.end method

.method public static final synthetic b(Lco/uk/getmondo/settings/h;)Lio/reactivex/u;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lco/uk/getmondo/settings/h;->d:Lio/reactivex/u;

    return-object v0
.end method

.method public static final synthetic c(Lco/uk/getmondo/settings/h;)Lio/reactivex/u;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lco/uk/getmondo/settings/h;->c:Lio/reactivex/u;

    return-object v0
.end method

.method public static final synthetic d(Lco/uk/getmondo/settings/h;)Lco/uk/getmondo/common/e/a;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lco/uk/getmondo/settings/h;->i:Lco/uk/getmondo/common/e/a;

    return-object v0
.end method

.method public static final synthetic e(Lco/uk/getmondo/settings/h;)Lco/uk/getmondo/common/a;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lco/uk/getmondo/settings/h;->h:Lco/uk/getmondo/common/a;

    return-object v0
.end method

.method public static final synthetic f(Lco/uk/getmondo/settings/h;)Lco/uk/getmondo/signup/identity_verification/a/f;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lco/uk/getmondo/settings/h;->j:Lco/uk/getmondo/signup/identity_verification/a/f;

    return-object v0
.end method

.method public static final synthetic g(Lco/uk/getmondo/settings/h;)Lco/uk/getmondo/api/PaymentLimitsApi;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lco/uk/getmondo/settings/h;->g:Lco/uk/getmondo/api/PaymentLimitsApi;

    return-object v0
.end method


# virtual methods
.method public bridge synthetic a(Lco/uk/getmondo/common/ui/f;)V
    .locals 0

    .prologue
    .line 30
    check-cast p1, Lco/uk/getmondo/settings/h$a;

    invoke-virtual {p0, p1}, Lco/uk/getmondo/settings/h;->a(Lco/uk/getmondo/settings/h$a;)V

    return-void
.end method

.method public a(Lco/uk/getmondo/settings/h$a;)V
    .locals 5

    .prologue
    const-string v0, "view"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v0, p1

    .line 42
    check-cast v0, Lco/uk/getmondo/common/ui/f;

    invoke-super {p0, v0}, Lco/uk/getmondo/common/ui/b;->a(Lco/uk/getmondo/common/ui/f;)V

    .line 44
    iget-object v0, p0, Lco/uk/getmondo/settings/h;->e:Lco/uk/getmondo/common/accounts/d;

    invoke-virtual {v0}, Lco/uk/getmondo/common/accounts/d;->b()Lco/uk/getmondo/d/ak;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-static {}, Lkotlin/d/b/l;->a()V

    :cond_0
    invoke-virtual {v0}, Lco/uk/getmondo/d/ak;->c()Lco/uk/getmondo/d/a;

    move-result-object v1

    if-nez v1, :cond_1

    invoke-static {}, Lkotlin/d/b/l;->a()V

    .line 46
    :cond_1
    invoke-interface {v1}, Lco/uk/getmondo/d/a;->d()Lco/uk/getmondo/d/a$b;

    move-result-object v0

    sget-object v2, Lco/uk/getmondo/d/a$b;->PREPAID:Lco/uk/getmondo/d/a$b;

    invoke-static {v0, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 47
    iget-object v3, p0, Lco/uk/getmondo/settings/h;->b:Lio/reactivex/b/a;

    .line 59
    invoke-interface {p1}, Lco/uk/getmondo/settings/h$a;->b()Lio/reactivex/n;

    move-result-object v0

    .line 48
    sget-object v2, Lkotlin/n;->a:Lkotlin/n;

    invoke-virtual {v0, v2}, Lio/reactivex/n;->startWith(Ljava/lang/Object;)Lio/reactivex/n;

    move-result-object v2

    .line 49
    new-instance v0, Lco/uk/getmondo/settings/h$b;

    invoke-direct {v0, p0, v1, p1}, Lco/uk/getmondo/settings/h$b;-><init>(Lco/uk/getmondo/settings/h;Lco/uk/getmondo/d/a;Lco/uk/getmondo/settings/h$a;)V

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {v2, v0}, Lio/reactivex/n;->flatMapMaybe(Lio/reactivex/c/h;)Lio/reactivex/n;

    move-result-object v4

    .line 59
    new-instance v0, Lco/uk/getmondo/settings/h$c;

    invoke-direct {v0, p0, p1}, Lco/uk/getmondo/settings/h$c;-><init>(Lco/uk/getmondo/settings/h;Lco/uk/getmondo/settings/h$a;)V

    check-cast v0, Lio/reactivex/c/g;

    .line 70
    sget-object v1, Lco/uk/getmondo/settings/h$d;->a:Lco/uk/getmondo/settings/h$d;

    check-cast v1, Lkotlin/d/a/b;

    if-eqz v1, :cond_2

    new-instance v2, Lco/uk/getmondo/settings/i;

    invoke-direct {v2, v1}, Lco/uk/getmondo/settings/i;-><init>(Lkotlin/d/a/b;)V

    move-object v1, v2

    :cond_2
    check-cast v1, Lio/reactivex/c/g;

    .line 59
    invoke-virtual {v4, v0, v1}, Lio/reactivex/n;->subscribe(Lio/reactivex/c/g;Lio/reactivex/c/g;)Lio/reactivex/b/b;

    move-result-object v0

    const-string v1, "view.onRetryClicked\n    \u2026            }, Timber::e)"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 47
    invoke-static {v3, v0}, Lco/uk/getmondo/common/j/f;->a(Lio/reactivex/b/a;Lio/reactivex/b/b;)Lio/reactivex/b/a;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/settings/h;->b:Lio/reactivex/b/a;

    .line 88
    :goto_0
    iget-object v1, p0, Lco/uk/getmondo/settings/h;->b:Lio/reactivex/b/a;

    invoke-interface {p1}, Lco/uk/getmondo/settings/h$a;->a()Lio/reactivex/n;

    move-result-object v2

    new-instance v0, Lco/uk/getmondo/settings/h$h;

    invoke-direct {v0, p1}, Lco/uk/getmondo/settings/h$h;-><init>(Lco/uk/getmondo/settings/h$a;)V

    check-cast v0, Lio/reactivex/c/g;

    invoke-virtual {v2, v0}, Lio/reactivex/n;->subscribe(Lio/reactivex/c/g;)Lio/reactivex/b/b;

    move-result-object v0

    const-string v2, "view.onRequestHigherLimi\u2026iew.openKycOnboarding() }"

    invoke-static {v0, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v1, v0}, Lco/uk/getmondo/common/j/f;->a(Lio/reactivex/b/a;Lio/reactivex/b/b;)Lio/reactivex/b/a;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/settings/h;->b:Lio/reactivex/b/a;

    .line 91
    return-void

    .line 72
    :cond_3
    iget-object v0, p0, Lco/uk/getmondo/settings/h;->h:Lco/uk/getmondo/common/a;

    sget-object v2, Lco/uk/getmondo/api/model/tracking/Impression;->Companion:Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    invoke-virtual {v2}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->aq()Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v2

    invoke-virtual {v0, v2}, Lco/uk/getmondo/common/a;->a(Lco/uk/getmondo/api/model/tracking/Impression;)V

    .line 73
    iget-object v3, p0, Lco/uk/getmondo/settings/h;->b:Lio/reactivex/b/a;

    .line 85
    invoke-interface {p1}, Lco/uk/getmondo/settings/h$a;->b()Lio/reactivex/n;

    move-result-object v0

    .line 74
    sget-object v2, Lkotlin/n;->a:Lkotlin/n;

    invoke-virtual {v0, v2}, Lio/reactivex/n;->startWith(Ljava/lang/Object;)Lio/reactivex/n;

    move-result-object v2

    .line 75
    new-instance v0, Lco/uk/getmondo/settings/h$e;

    invoke-direct {v0, p0, v1, p1}, Lco/uk/getmondo/settings/h$e;-><init>(Lco/uk/getmondo/settings/h;Lco/uk/getmondo/d/a;Lco/uk/getmondo/settings/h$a;)V

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {v2, v0}, Lio/reactivex/n;->flatMapMaybe(Lio/reactivex/c/h;)Lio/reactivex/n;

    move-result-object v4

    .line 85
    new-instance v0, Lco/uk/getmondo/settings/h$f;

    invoke-direct {v0, p1}, Lco/uk/getmondo/settings/h$f;-><init>(Lco/uk/getmondo/settings/h$a;)V

    check-cast v0, Lio/reactivex/c/g;

    .line 87
    sget-object v1, Lco/uk/getmondo/settings/h$g;->a:Lco/uk/getmondo/settings/h$g;

    check-cast v1, Lkotlin/d/a/b;

    if-eqz v1, :cond_4

    new-instance v2, Lco/uk/getmondo/settings/i;

    invoke-direct {v2, v1}, Lco/uk/getmondo/settings/i;-><init>(Lkotlin/d/a/b;)V

    move-object v1, v2

    :cond_4
    check-cast v1, Lio/reactivex/c/g;

    .line 85
    invoke-virtual {v4, v0, v1}, Lio/reactivex/n;->subscribe(Lio/reactivex/c/g;Lio/reactivex/c/g;)Lio/reactivex/b/b;

    move-result-object v0

    const-string v1, "view.onRetryClicked\n    \u2026            }, Timber::e)"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 73
    invoke-static {v3, v0}, Lco/uk/getmondo/common/j/f;->a(Lio/reactivex/b/a;Lio/reactivex/b/b;)Lio/reactivex/b/a;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/settings/h;->b:Lio/reactivex/b/a;

    goto :goto_0
.end method
