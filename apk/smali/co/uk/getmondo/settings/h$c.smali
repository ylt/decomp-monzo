.class final Lco/uk/getmondo/settings/h$c;
.super Ljava/lang/Object;
.source "LimitsPresenter.kt"

# interfaces
.implements Lio/reactivex/c/g;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lco/uk/getmondo/settings/h;->a(Lco/uk/getmondo/settings/h$a;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/g",
        "<",
        "Lco/uk/getmondo/d/e;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "",
        "balanceLimits",
        "Lco/uk/getmondo/model/BalanceLimit;",
        "kotlin.jvm.PlatformType",
        "accept"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field final synthetic a:Lco/uk/getmondo/settings/h;

.field final synthetic b:Lco/uk/getmondo/settings/h$a;


# direct methods
.method constructor <init>(Lco/uk/getmondo/settings/h;Lco/uk/getmondo/settings/h$a;)V
    .locals 0

    iput-object p1, p0, Lco/uk/getmondo/settings/h$c;->a:Lco/uk/getmondo/settings/h;

    iput-object p2, p0, Lco/uk/getmondo/settings/h$c;->b:Lco/uk/getmondo/settings/h$a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lco/uk/getmondo/d/e;)V
    .locals 2

    .prologue
    .line 60
    sget-object v0, Lco/uk/getmondo/api/model/tracking/Impression;->Companion:Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    invoke-virtual {p1}, Lco/uk/getmondo/d/e;->a()Lco/uk/getmondo/api/model/VerificationType;

    move-result-object v1

    invoke-virtual {v0, v1}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->a(Lco/uk/getmondo/api/model/VerificationType;)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    .line 61
    iget-object v1, p0, Lco/uk/getmondo/settings/h$c;->a:Lco/uk/getmondo/settings/h;

    invoke-static {v1}, Lco/uk/getmondo/settings/h;->e(Lco/uk/getmondo/settings/h;)Lco/uk/getmondo/common/a;

    move-result-object v1

    invoke-virtual {v1, v0}, Lco/uk/getmondo/common/a;->a(Lco/uk/getmondo/api/model/tracking/Impression;)V

    .line 63
    invoke-virtual {p1}, Lco/uk/getmondo/d/e;->a()Lco/uk/getmondo/api/model/VerificationType;

    move-result-object v0

    sget-object v1, Lco/uk/getmondo/api/model/VerificationType;->STANDARD:Lco/uk/getmondo/api/model/VerificationType;

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, Lco/uk/getmondo/d/e;->a()Lco/uk/getmondo/api/model/VerificationType;

    move-result-object v0

    sget-object v1, Lco/uk/getmondo/api/model/VerificationType;->BLOCKED:Lco/uk/getmondo/api/model/VerificationType;

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_0
    const/4 v0, 0x1

    .line 66
    :goto_0
    if-eqz v0, :cond_1

    iget-object v0, p0, Lco/uk/getmondo/settings/h$c;->a:Lco/uk/getmondo/settings/h;

    invoke-static {v0}, Lco/uk/getmondo/settings/h;->f(Lco/uk/getmondo/settings/h;)Lco/uk/getmondo/signup/identity_verification/a/f;

    move-result-object v0

    invoke-virtual {v0}, Lco/uk/getmondo/signup/identity_verification/a/f;->a()Z

    move-result v0

    if-nez v0, :cond_1

    .line 67
    iget-object v0, p0, Lco/uk/getmondo/settings/h$c;->b:Lco/uk/getmondo/settings/h$a;

    invoke-interface {v0}, Lco/uk/getmondo/settings/h$a;->f()V

    .line 69
    :cond_1
    iget-object v0, p0, Lco/uk/getmondo/settings/h$c;->b:Lco/uk/getmondo/settings/h$a;

    const-string v1, "balanceLimits"

    invoke-static {p1, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v0, p1}, Lco/uk/getmondo/settings/h$a;->a(Lco/uk/getmondo/d/e;)V

    .line 70
    return-void

    .line 63
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 30
    check-cast p1, Lco/uk/getmondo/d/e;

    invoke-virtual {p0, p1}, Lco/uk/getmondo/settings/h$c;->a(Lco/uk/getmondo/d/e;)V

    return-void
.end method
