.class final Lco/uk/getmondo/settings/u$w;
.super Ljava/lang/Object;
.source "SettingsPresenter.kt"

# interfaces
.implements Lio/reactivex/c/g;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lco/uk/getmondo/settings/u;->a(Lco/uk/getmondo/settings/u$a;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/g",
        "<",
        "Lco/uk/getmondo/api/model/ApiAccountSettings;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "",
        "accountSettings",
        "Lco/uk/getmondo/api/model/ApiAccountSettings;",
        "kotlin.jvm.PlatformType",
        "accept"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field final synthetic a:Lco/uk/getmondo/settings/u;

.field final synthetic b:Lco/uk/getmondo/settings/u$a;


# direct methods
.method constructor <init>(Lco/uk/getmondo/settings/u;Lco/uk/getmondo/settings/u$a;)V
    .locals 0

    iput-object p1, p0, Lco/uk/getmondo/settings/u$w;->a:Lco/uk/getmondo/settings/u;

    iput-object p2, p0, Lco/uk/getmondo/settings/u$w;->b:Lco/uk/getmondo/settings/u$a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lco/uk/getmondo/api/model/ApiAccountSettings;)V
    .locals 3

    .prologue
    .line 140
    iget-object v0, p0, Lco/uk/getmondo/settings/u$w;->b:Lco/uk/getmondo/settings/u$a;

    invoke-interface {v0}, Lco/uk/getmondo/settings/u$a;->N()V

    .line 141
    iget-object v0, p0, Lco/uk/getmondo/settings/u$w;->a:Lco/uk/getmondo/settings/u;

    iget-object v1, p0, Lco/uk/getmondo/settings/u$w;->b:Lco/uk/getmondo/settings/u$a;

    const-string v2, "accountSettings"

    invoke-static {p1, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v0, v1, p1}, Lco/uk/getmondo/settings/u;->a(Lco/uk/getmondo/settings/u;Lco/uk/getmondo/settings/u$a;Lco/uk/getmondo/api/model/ApiAccountSettings;)V

    .line 142
    return-void
.end method

.method public bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 36
    check-cast p1, Lco/uk/getmondo/api/model/ApiAccountSettings;

    invoke-virtual {p0, p1}, Lco/uk/getmondo/settings/u$w;->a(Lco/uk/getmondo/api/model/ApiAccountSettings;)V

    return-void
.end method
