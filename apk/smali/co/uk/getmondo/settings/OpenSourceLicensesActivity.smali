.class public Lco/uk/getmondo/settings/OpenSourceLicensesActivity;
.super Lco/uk/getmondo/common/activities/b;
.source "OpenSourceLicensesActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;,
        Lco/uk/getmondo/settings/OpenSourceLicensesActivity$b;,
        Lco/uk/getmondo/settings/OpenSourceLicensesActivity$HeaderViewHolder;,
        Lco/uk/getmondo/settings/OpenSourceLicensesActivity$OpenSourceLibraryHolder;,
        Lco/uk/getmondo/settings/OpenSourceLicensesActivity$d;,
        Lco/uk/getmondo/settings/OpenSourceLicensesActivity$c;
    }
.end annotation


# static fields
.field private static final a:Ljava/util/concurrent/atomic/AtomicInteger;


# instance fields
.field appBarLayout:Landroid/support/design/widget/AppBarLayout;
    .annotation build Lbutterknife/BindView;
        value = 0x7f1101cd
    .end annotation
.end field

.field collapsingToolbarLayout:Landroid/support/design/widget/CollapsingToolbarLayout;
    .annotation build Lbutterknife/BindView;
        value = 0x7f1101ce
    .end annotation
.end field

.field descriptionTextView:Landroid/widget/TextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f1101cf
    .end annotation
.end field

.field recyclerView:Landroid/support/v7/widget/RecyclerView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f1101d0
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 33
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    sput-object v0, Lco/uk/getmondo/settings/OpenSourceLicensesActivity;->a:Ljava/util/concurrent/atomic/AtomicInteger;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Lco/uk/getmondo/common/activities/b;-><init>()V

    return-void
.end method

.method static synthetic a()Ljava/util/concurrent/atomic/AtomicInteger;
    .locals 1

    .prologue
    .line 30
    sget-object v0, Lco/uk/getmondo/settings/OpenSourceLicensesActivity;->a:Ljava/util/concurrent/atomic/AtomicInteger;

    return-object v0
.end method

.method public static a(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 41
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lco/uk/getmondo/settings/OpenSourceLicensesActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 42
    return-void
.end method

.method static synthetic a(Lco/uk/getmondo/settings/OpenSourceLicensesActivity;Landroid/support/design/widget/AppBarLayout;I)V
    .locals 4

    .prologue
    .line 59
    int-to-float v0, p2

    invoke-virtual {p1}, Landroid/support/design/widget/AppBarLayout;->getTotalScrollRange()I

    move-result v1

    int-to-float v1, v1

    div-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    .line 60
    iget-object v1, p0, Lco/uk/getmondo/settings/OpenSourceLicensesActivity;->descriptionTextView:Landroid/widget/TextView;

    const/high16 v2, 0x3f800000    # 1.0f

    const/high16 v3, 0x3fc00000    # 1.5f

    mul-float/2addr v3, v0

    sub-float/2addr v2, v3

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setAlpha(F)V

    .line 62
    const v1, 0x106000d

    invoke-static {p0, v1}, Landroid/support/v4/content/a;->c(Landroid/content/Context;I)I

    move-result v1

    .line 63
    const v2, 0x7f0f00ea

    invoke-static {p0, v2}, Landroid/support/v4/content/a;->c(Landroid/content/Context;I)I

    move-result v2

    .line 64
    new-instance v3, Landroid/animation/ArgbEvaluator;

    invoke-direct {v3}, Landroid/animation/ArgbEvaluator;-><init>()V

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v3, v0, v1, v2}, Landroid/animation/ArgbEvaluator;->evaluate(FLjava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 65
    iget-object v2, p0, Lco/uk/getmondo/settings/OpenSourceLicensesActivity;->collapsingToolbarLayout:Landroid/support/design/widget/CollapsingToolbarLayout;

    move-object v0, v1

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v2, v0}, Landroid/support/design/widget/CollapsingToolbarLayout;->setCollapsedTitleTextColor(I)V

    .line 66
    iget-object v0, p0, Lco/uk/getmondo/settings/OpenSourceLicensesActivity;->collapsingToolbarLayout:Landroid/support/design/widget/CollapsingToolbarLayout;

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/support/design/widget/CollapsingToolbarLayout;->setExpandedTitleColor(I)V

    .line 67
    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 46
    invoke-super {p0, p1}, Lco/uk/getmondo/common/activities/b;->onCreate(Landroid/os/Bundle;)V

    .line 48
    const v0, 0x7f05004e

    invoke-virtual {p0, v0}, Lco/uk/getmondo/settings/OpenSourceLicensesActivity;->setContentView(I)V

    .line 49
    invoke-static {p0}, Lbutterknife/ButterKnife;->bind(Landroid/app/Activity;)Lbutterknife/Unbinder;

    .line 51
    iget-object v0, p0, Lco/uk/getmondo/settings/OpenSourceLicensesActivity;->descriptionTextView:Landroid/widget/TextView;

    const v1, 0x7f0a037c

    new-array v2, v5, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {}, Lco/uk/getmondo/common/k/e;->b()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {p0, v1, v2}, Lco/uk/getmondo/settings/OpenSourceLicensesActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 53
    new-instance v0, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$d;

    invoke-direct {v0}, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$d;-><init>()V

    .line 54
    iget-object v1, p0, Lco/uk/getmondo/settings/OpenSourceLicensesActivity;->recyclerView:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v1, v5}, Landroid/support/v7/widget/RecyclerView;->setHasFixedSize(Z)V

    .line 55
    iget-object v1, p0, Lco/uk/getmondo/settings/OpenSourceLicensesActivity;->recyclerView:Landroid/support/v7/widget/RecyclerView;

    new-instance v2, Landroid/support/v7/widget/LinearLayoutManager;

    invoke-direct {v2, p0}, Landroid/support/v7/widget/LinearLayoutManager;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, v2}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(Landroid/support/v7/widget/RecyclerView$h;)V

    .line 56
    iget-object v1, p0, Lco/uk/getmondo/settings/OpenSourceLicensesActivity;->recyclerView:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v1, v0}, Landroid/support/v7/widget/RecyclerView;->setAdapter(Landroid/support/v7/widget/RecyclerView$a;)V

    .line 57
    iget-object v1, p0, Lco/uk/getmondo/settings/OpenSourceLicensesActivity;->recyclerView:Landroid/support/v7/widget/RecyclerView;

    new-instance v2, La/a/a/a/a/b;

    invoke-direct {v2, v0}, La/a/a/a/a/b;-><init>(La/a/a/a/a/a;)V

    invoke-virtual {v1, v2}, Landroid/support/v7/widget/RecyclerView;->a(Landroid/support/v7/widget/RecyclerView$g;)V

    .line 58
    iget-object v0, p0, Lco/uk/getmondo/settings/OpenSourceLicensesActivity;->appBarLayout:Landroid/support/design/widget/AppBarLayout;

    invoke-static {p0}, Lco/uk/getmondo/settings/r;->a(Lco/uk/getmondo/settings/OpenSourceLicensesActivity;)Landroid/support/design/widget/AppBarLayout$b;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/design/widget/AppBarLayout;->a(Landroid/support/design/widget/AppBarLayout$b;)V

    .line 68
    return-void
.end method
