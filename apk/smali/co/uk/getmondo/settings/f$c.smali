.class public final Lco/uk/getmondo/settings/f$c;
.super Ljava/lang/Object;
.source "LimitsAdapter.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lco/uk/getmondo/settings/f;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x11
    name = "c"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000&\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\r\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\n\u0008\u0080\u0004\u0018\u00002\u00020\u0001B=\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\u0005\u0012\n\u0008\u0002\u0010\t\u001a\u0004\u0018\u00010\u0005\u0012\n\u0008\u0002\u0010\n\u001a\u0004\u0018\u00010\u000b\u00a2\u0006\u0002\u0010\u000cR\u0013\u0010\t\u001a\u0004\u0018\u00010\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\r\u0010\u000eR\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000f\u0010\u000eR\u0011\u0010\u0008\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0010\u0010\u000eR\u0013\u0010\n\u001a\u0004\u0018\u00010\u000b\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0011\u0010\u0012R\u0011\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0013\u0010\u0014\u00a8\u0006\u0015"
    }
    d2 = {
        "Lco/uk/getmondo/settings/LimitsAdapter$LimitItem;",
        "",
        "resources",
        "Landroid/content/res/Resources;",
        "category",
        "",
        "name",
        "",
        "limit",
        "amountRemaining",
        "limitDetails",
        "Lco/uk/getmondo/settings/LimitsAdapter$LimitDetails;",
        "(Lco/uk/getmondo/settings/LimitsAdapter;Landroid/content/res/Resources;Ljava/lang/String;Ljava/lang/CharSequence;Ljava/lang/String;Ljava/lang/String;Lco/uk/getmondo/settings/LimitsAdapter$LimitDetails;)V",
        "getAmountRemaining",
        "()Ljava/lang/String;",
        "getCategory",
        "getLimit",
        "getLimitDetails",
        "()Lco/uk/getmondo/settings/LimitsAdapter$LimitDetails;",
        "getName",
        "()Ljava/lang/CharSequence;",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field final synthetic a:Lco/uk/getmondo/settings/f;

.field private final b:Ljava/lang/CharSequence;

.field private final c:Ljava/lang/String;

.field private final d:Ljava/lang/String;

.field private final e:Ljava/lang/String;

.field private final f:Lco/uk/getmondo/settings/f$b;


# direct methods
.method public constructor <init>(Lco/uk/getmondo/settings/f;Landroid/content/res/Resources;Ljava/lang/String;Ljava/lang/CharSequence;Ljava/lang/String;Ljava/lang/String;Lco/uk/getmondo/settings/f$b;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/res/Resources;",
            "Ljava/lang/String;",
            "Ljava/lang/CharSequence;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lco/uk/getmondo/settings/f$b;",
            ")V"
        }
    .end annotation

    .prologue
    const-string v0, "resources"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "category"

    invoke-static {p3, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "name"

    invoke-static {p4, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "limit"

    invoke-static {p5, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 132
    iput-object p1, p0, Lco/uk/getmondo/settings/f$c;->a:Lco/uk/getmondo/settings/f;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p3, p0, Lco/uk/getmondo/settings/f$c;->c:Ljava/lang/String;

    iput-object p5, p0, Lco/uk/getmondo/settings/f$c;->d:Ljava/lang/String;

    iput-object p6, p0, Lco/uk/getmondo/settings/f$c;->e:Ljava/lang/String;

    iput-object p7, p0, Lco/uk/getmondo/settings/f$c;->f:Lco/uk/getmondo/settings/f$b;

    .line 143
    iget-object v0, p0, Lco/uk/getmondo/settings/f$c;->f:Lco/uk/getmondo/settings/f$b;

    if-eqz v0, :cond_0

    .line 145
    new-instance v1, Landroid/text/SpannableString;

    invoke-direct {v1, p4}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 146
    const v0, 0x7f0a0247

    invoke-virtual {p2, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    const-string v0, "resources.getString(R.st\u2026ng.limit_details_matcher)"

    invoke-static {v2, v0}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 147
    new-instance v0, Lco/uk/getmondo/settings/f$c$1;

    invoke-direct {v0, p0}, Lco/uk/getmondo/settings/f$c$1;-><init>(Lco/uk/getmondo/settings/f$c;)V

    check-cast v0, Landroid/text/style/ClickableSpan;

    .line 144
    invoke-static {p1, v1, v2, v0}, Lco/uk/getmondo/settings/f;->a(Lco/uk/getmondo/settings/f;Landroid/text/SpannableString;Ljava/lang/String;Landroid/text/style/ClickableSpan;)Landroid/text/SpannableString;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    iput-object v0, p0, Lco/uk/getmondo/settings/f$c;->b:Ljava/lang/CharSequence;

    .line 154
    :goto_0
    return-void

    .line 153
    :cond_0
    iput-object p4, p0, Lco/uk/getmondo/settings/f$c;->b:Ljava/lang/CharSequence;

    goto :goto_0
.end method

.method public synthetic constructor <init>(Lco/uk/getmondo/settings/f;Landroid/content/res/Resources;Ljava/lang/String;Ljava/lang/CharSequence;Ljava/lang/String;Ljava/lang/String;Lco/uk/getmondo/settings/f$b;ILkotlin/d/b/i;)V
    .locals 8

    .prologue
    and-int/lit8 v0, p8, 0x10

    if-eqz v0, :cond_1

    .line 137
    const/4 v0, 0x0

    check-cast v0, Ljava/lang/String;

    move-object v6, v0

    :goto_0
    and-int/lit8 v0, p8, 0x20

    if-eqz v0, :cond_0

    .line 138
    const/4 v0, 0x0

    check-cast v0, Lco/uk/getmondo/settings/f$b;

    move-object v7, v0

    :goto_1
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v7}, Lco/uk/getmondo/settings/f$c;-><init>(Lco/uk/getmondo/settings/f;Landroid/content/res/Resources;Ljava/lang/String;Ljava/lang/CharSequence;Ljava/lang/String;Ljava/lang/String;Lco/uk/getmondo/settings/f$b;)V

    return-void

    :cond_0
    move-object v7, p7

    goto :goto_1

    :cond_1
    move-object v6, p6

    goto :goto_0
.end method


# virtual methods
.method public final a()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 140
    iget-object v0, p0, Lco/uk/getmondo/settings/f$c;->b:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 134
    iget-object v0, p0, Lco/uk/getmondo/settings/f$c;->c:Ljava/lang/String;

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 136
    iget-object v0, p0, Lco/uk/getmondo/settings/f$c;->d:Ljava/lang/String;

    return-object v0
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 137
    iget-object v0, p0, Lco/uk/getmondo/settings/f$c;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final e()Lco/uk/getmondo/settings/f$b;
    .locals 1

    .prologue
    .line 138
    iget-object v0, p0, Lco/uk/getmondo/settings/f$c;->f:Lco/uk/getmondo/settings/f$b;

    return-object v0
.end method
