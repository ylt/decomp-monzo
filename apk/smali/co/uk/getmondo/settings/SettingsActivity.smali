.class public final Lco/uk/getmondo/settings/SettingsActivity;
.super Lco/uk/getmondo/common/activities/b;
.source "SettingsActivity.kt"

# interfaces
.implements Lco/uk/getmondo/settings/p$a;
.implements Lco/uk/getmondo/settings/u$a;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lco/uk/getmondo/settings/SettingsActivity$a;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0008\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0008\u0006\n\u0002\u0010\u000b\n\u0002\u0008\u000f\n\u0002\u0010\u000e\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0008\n\u0002\u0010\u0008\n\u0002\u0008\u000f\u0018\u0000 U2\u00020\u00012\u00020\u00022\u00020\u0003:\u0001UB\u0005\u00a2\u0006\u0002\u0010\u0004J\u0008\u0010\u0015\u001a\u00020\rH\u0016J\u0008\u0010\u0016\u001a\u00020\rH\u0016J\u0008\u0010\u0017\u001a\u00020\rH\u0016J\u000e\u0010\u0018\u001a\u0008\u0012\u0004\u0012\u00020\r0\u0019H\u0016J\u000e\u0010\u001a\u001a\u0008\u0012\u0004\u0012\u00020\r0\u0019H\u0016J\u000e\u0010\u001b\u001a\u0008\u0012\u0004\u0012\u00020\r0\u0019H\u0016J\u000e\u0010\u001c\u001a\u0008\u0012\u0004\u0012\u00020\r0\u0019H\u0016J\u0012\u0010\u001d\u001a\u00020\r2\u0008\u0010\u001e\u001a\u0004\u0018\u00010\u001fH\u0016J\u0008\u0010 \u001a\u00020\rH\u0014J\u000e\u0010!\u001a\u0008\u0012\u0004\u0012\u00020\r0\u0019H\u0016J\u000e\u0010\"\u001a\u0008\u0012\u0004\u0012\u00020\r0\u0019H\u0016J\u000e\u0010#\u001a\u0008\u0012\u0004\u0012\u00020\r0\u0019H\u0016J\u0008\u0010$\u001a\u00020\rH\u0016J\u000e\u0010%\u001a\u0008\u0012\u0004\u0012\u00020&0\u0019H\u0016J\u000e\u0010\'\u001a\u0008\u0012\u0004\u0012\u00020\r0\u0019H\u0016J\u000e\u0010(\u001a\u0008\u0012\u0004\u0012\u00020&0\u0019H\u0016J\u000e\u0010)\u001a\u0008\u0012\u0004\u0012\u00020\r0\u0019H\u0016J\u000e\u0010*\u001a\u0008\u0012\u0004\u0012\u00020&0\u0019H\u0016J\u000e\u0010+\u001a\u0008\u0012\u0004\u0012\u00020\r0\u0019H\u0016J\u000e\u0010,\u001a\u0008\u0012\u0004\u0012\u00020\r0\u0019H\u0016J\u000e\u0010-\u001a\u0008\u0012\u0004\u0012\u00020\r0\u0019H\u0016J\u0008\u0010.\u001a\u00020\rH\u0016J\u0008\u0010/\u001a\u00020\rH\u0016J\u0008\u00100\u001a\u00020\rH\u0016J\u0008\u00101\u001a\u00020\rH\u0016J\u0008\u00102\u001a\u00020\rH\u0016J\u0008\u00103\u001a\u00020\rH\u0016J\u0010\u00104\u001a\u00020\r2\u0006\u00105\u001a\u000206H\u0002J\u0018\u00107\u001a\u00020\r2\u0006\u00108\u001a\u0002062\u0006\u00109\u001a\u000206H\u0016J\u001a\u0010:\u001a\u00020\r2\u0006\u0010;\u001a\u00020<2\u0008\u0010=\u001a\u0004\u0018\u00010>H\u0016J \u0010?\u001a\u00020\r2\u0006\u0010@\u001a\u0002062\u0006\u00108\u001a\u0002062\u0006\u00109\u001a\u000206H\u0016J\u0008\u0010A\u001a\u00020\rH\u0016J\u0008\u0010B\u001a\u00020\rH\u0016J\u0008\u0010C\u001a\u00020\rH\u0016J\u0008\u0010D\u001a\u00020\rH\u0016J\u0018\u0010E\u001a\u00020\r2\u0006\u0010F\u001a\u00020G2\u0006\u0010H\u001a\u00020GH\u0016J\u0008\u0010I\u001a\u00020\rH\u0016J\u0008\u0010J\u001a\u00020\rH\u0016J\u0008\u0010K\u001a\u00020\rH\u0016J\u0008\u0010L\u001a\u00020\rH\u0016J\u0008\u0010M\u001a\u00020\rH\u0016J\u0008\u0010N\u001a\u00020\rH\u0016J\u0008\u0010O\u001a\u00020\rH\u0016J\u0008\u0010P\u001a\u00020\rH\u0016J\u0008\u0010Q\u001a\u00020\rH\u0016J\u0008\u0010R\u001a\u00020\rH\u0016J\u0008\u0010S\u001a\u00020\rH\u0016J\u0008\u0010T\u001a\u00020\rH\u0016R\u001b\u0010\u0005\u001a\u00020\u00068BX\u0082\u0084\u0002\u00a2\u0006\u000c\n\u0004\u0008\t\u0010\n\u001a\u0004\u0008\u0007\u0010\u0008R2\u0010\u000b\u001a&\u0012\u000c\u0012\n \u000e*\u0004\u0018\u00010\r0\r \u000e*\u0012\u0012\u000c\u0012\n \u000e*\u0004\u0018\u00010\r0\r\u0018\u00010\u000c0\u000cX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001e\u0010\u000f\u001a\u00020\u00108\u0000@\u0000X\u0081.\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u0011\u0010\u0012\"\u0004\u0008\u0013\u0010\u0014\u00a8\u0006V"
    }
    d2 = {
        "Lco/uk/getmondo/settings/SettingsActivity;",
        "Lco/uk/getmondo/common/activities/BaseActivity;",
        "Lco/uk/getmondo/settings/SettingsPresenter$View;",
        "Lco/uk/getmondo/settings/LogOutConfirmationDialogFragment$OnLogOutConfirmedListener;",
        "()V",
        "avatarGenerator",
        "Lco/uk/getmondo/common/ui/AvatarGenerator;",
        "getAvatarGenerator",
        "()Lco/uk/getmondo/common/ui/AvatarGenerator;",
        "avatarGenerator$delegate",
        "Lkotlin/Lazy;",
        "logOutConfirmationRelay",
        "Lcom/jakewharton/rxrelay2/PublishRelay;",
        "",
        "kotlin.jvm.PlatformType",
        "presenter",
        "Lco/uk/getmondo/settings/SettingsPresenter;",
        "getPresenter$app_monzoPrepaidRelease",
        "()Lco/uk/getmondo/settings/SettingsPresenter;",
        "setPresenter$app_monzoPrepaidRelease",
        "(Lco/uk/getmondo/settings/SettingsPresenter;)V",
        "hideMagStripeLoading",
        "hidePaymentsLoading",
        "hidePaymentsUi",
        "onAboutMonzoClicked",
        "Lio/reactivex/Observable;",
        "onAddressClicked",
        "onCloseAccountClicked",
        "onConfirmLogOutClicked",
        "onCreate",
        "savedInstanceState",
        "Landroid/os/Bundle;",
        "onDestroy",
        "onFscsProtectionClicked",
        "onLimitsClicked",
        "onLogOutClicked",
        "onLogOutConfirmed",
        "onMagStripeStateToggled",
        "",
        "onNameClicked",
        "onNotificationsStateToggled",
        "onOpenSourceLicensesClicked",
        "onPaymentsStateToggled",
        "onPrivacyPolicyClicked",
        "onShareAccountDetailsClicked",
        "onTermsAndConditionsClicked",
        "openAboutMonzo",
        "openFscsProtection",
        "openLimits",
        "openPrivacyPolicy",
        "openTermsAndConditions",
        "openUpdateAddress",
        "openUrl",
        "url",
        "",
        "setAccountInformation",
        "accountNumber",
        "sortCode",
        "setProfileInformation",
        "profile",
        "Lco/uk/getmondo/model/Profile;",
        "created",
        "Lorg/threeten/bp/LocalDateTime;",
        "shareAccountDetails",
        "profileName",
        "showChangeInformation",
        "showCloseAccount",
        "showLogoutConfirmation",
        "showMagStripeDisabled",
        "showMagStripeEnabled",
        "hours",
        "",
        "minutes",
        "showMagStripeLoading",
        "showNotificationsChecked",
        "showNotificationsUnchecked",
        "showOpenSourceLicenses",
        "showPaymentsBlocked",
        "showPaymentsDisabled",
        "showPaymentsEnabled",
        "showPaymentsLoading",
        "showPaymentsUi",
        "showPrepaidUi",
        "showRetailUi",
        "showUpdateAddressSupport",
        "Companion",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# static fields
.field static final synthetic a:[Lkotlin/reflect/l;

.field public static final c:Lco/uk/getmondo/settings/SettingsActivity$a;

# The value of this static final field might be set in the static constructor
.field private static final g:Ljava/lang/String; = "TAG_CHANGE_PERSONAL_INFORMATION"

# The value of this static final field might be set in the static constructor
.field private static final h:Ljava/lang/String; = "TAG_ERROR_P2P_BLOCKED"

# The value of this static final field might be set in the static constructor
.field private static final i:Ljava/lang/String; = "TAG_CLOSE_ACCOUNT"

# The value of this static final field might be set in the static constructor
.field private static final j:Ljava/lang/String; = "TAG_LOG_OUT_CONFIRMATION"

# The value of this static final field might be set in the static constructor
.field private static final k:Ljava/lang/String; = "TAG_UPDATE_ADDRESS"


# instance fields
.field public b:Lco/uk/getmondo/settings/u;

.field private final e:Lcom/b/b/c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/b/b/c",
            "<",
            "Lkotlin/n;",
            ">;"
        }
    .end annotation
.end field

.field private final f:Lkotlin/c;

.field private l:Ljava/util/HashMap;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v0, 0x1

    new-array v1, v0, [Lkotlin/reflect/l;

    const/4 v2, 0x0

    new-instance v0, Lkotlin/d/b/w;

    const-class v3, Lco/uk/getmondo/settings/SettingsActivity;

    invoke-static {v3}, Lkotlin/d/b/y;->a(Ljava/lang/Class;)Lkotlin/reflect/c;

    move-result-object v3

    const-string v4, "avatarGenerator"

    const-string v5, "getAvatarGenerator()Lco/uk/getmondo/common/ui/AvatarGenerator;"

    invoke-direct {v0, v3, v4, v5}, Lkotlin/d/b/w;-><init>(Lkotlin/reflect/e;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v0}, Lkotlin/d/b/y;->a(Lkotlin/d/b/v;)Lkotlin/reflect/n;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/l;

    aput-object v0, v1, v2

    sput-object v1, Lco/uk/getmondo/settings/SettingsActivity;->a:[Lkotlin/reflect/l;

    new-instance v0, Lco/uk/getmondo/settings/SettingsActivity$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lco/uk/getmondo/settings/SettingsActivity$a;-><init>(Lkotlin/d/b/i;)V

    sput-object v0, Lco/uk/getmondo/settings/SettingsActivity;->c:Lco/uk/getmondo/settings/SettingsActivity$a;

    .line 272
    const-string v0, "TAG_CHANGE_PERSONAL_INFORMATION"

    sput-object v0, Lco/uk/getmondo/settings/SettingsActivity;->g:Ljava/lang/String;

    .line 273
    const-string v0, "TAG_ERROR_P2P_BLOCKED"

    sput-object v0, Lco/uk/getmondo/settings/SettingsActivity;->h:Ljava/lang/String;

    .line 274
    const-string v0, "TAG_CLOSE_ACCOUNT"

    sput-object v0, Lco/uk/getmondo/settings/SettingsActivity;->i:Ljava/lang/String;

    .line 275
    const-string v0, "TAG_LOG_OUT_CONFIRMATION"

    sput-object v0, Lco/uk/getmondo/settings/SettingsActivity;->j:Ljava/lang/String;

    .line 276
    const-string v0, "TAG_UPDATE_ADDRESS"

    sput-object v0, Lco/uk/getmondo/settings/SettingsActivity;->k:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 33
    invoke-direct {p0}, Lco/uk/getmondo/common/activities/b;-><init>()V

    .line 35
    invoke-static {}, Lcom/b/b/c;->a()Lcom/b/b/c;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/settings/SettingsActivity;->e:Lcom/b/b/c;

    .line 36
    new-instance v0, Lco/uk/getmondo/settings/SettingsActivity$b;

    invoke-direct {v0, p0}, Lco/uk/getmondo/settings/SettingsActivity$b;-><init>(Lco/uk/getmondo/settings/SettingsActivity;)V

    check-cast v0, Lkotlin/d/a/a;

    invoke-static {v0}, Lkotlin/d;->a(Lkotlin/d/a/a;)Lkotlin/c;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/settings/SettingsActivity;->f:Lkotlin/c;

    return-void
.end method

.method public static final synthetic Z()Ljava/lang/String;
    .locals 1

    .prologue
    .line 33
    sget-object v0, Lco/uk/getmondo/settings/SettingsActivity;->g:Ljava/lang/String;

    return-object v0
.end method

.method private final a(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 265
    new-instance v1, Landroid/support/b/a$a;

    invoke-direct {v1}, Landroid/support/b/a$a;-><init>()V

    move-object v0, p0

    .line 266
    check-cast v0, Landroid/content/Context;

    const v2, 0x7f0f000f

    invoke-static {v0, v2}, Landroid/support/v4/content/a;->c(Landroid/content/Context;I)I

    move-result v0

    invoke-virtual {v1, v0}, Landroid/support/b/a$a;->a(I)Landroid/support/b/a$a;

    move-result-object v0

    .line 267
    invoke-virtual {v0}, Landroid/support/b/a$a;->a()Landroid/support/b/a;

    move-result-object v0

    .line 268
    check-cast p0, Landroid/content/Context;

    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, p0, v1}, Landroid/support/b/a;->a(Landroid/content/Context;Landroid/net/Uri;)V

    .line 269
    return-void
.end method

.method public static final synthetic aa()Ljava/lang/String;
    .locals 1

    .prologue
    .line 33
    sget-object v0, Lco/uk/getmondo/settings/SettingsActivity;->h:Ljava/lang/String;

    return-object v0
.end method

.method public static final synthetic ab()Ljava/lang/String;
    .locals 1

    .prologue
    .line 33
    sget-object v0, Lco/uk/getmondo/settings/SettingsActivity;->i:Ljava/lang/String;

    return-object v0
.end method

.method public static final synthetic ac()Ljava/lang/String;
    .locals 1

    .prologue
    .line 33
    sget-object v0, Lco/uk/getmondo/settings/SettingsActivity;->j:Ljava/lang/String;

    return-object v0
.end method

.method public static final synthetic ad()Ljava/lang/String;
    .locals 1

    .prologue
    .line 33
    sget-object v0, Lco/uk/getmondo/settings/SettingsActivity;->k:Ljava/lang/String;

    return-object v0
.end method

.method private final ae()Lco/uk/getmondo/common/ui/a;
    .locals 3

    iget-object v0, p0, Lco/uk/getmondo/settings/SettingsActivity;->f:Lkotlin/c;

    sget-object v1, Lco/uk/getmondo/settings/SettingsActivity;->a:[Lkotlin/reflect/l;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-interface {v0}, Lkotlin/c;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/common/ui/a;

    return-object v0
.end method


# virtual methods
.method public A()V
    .locals 3

    .prologue
    .line 141
    invoke-static {}, Lco/uk/getmondo/settings/a;->a()Lco/uk/getmondo/settings/a;

    move-result-object v0

    invoke-virtual {p0}, Lco/uk/getmondo/settings/SettingsActivity;->getSupportFragmentManager()Landroid/support/v4/app/n;

    move-result-object v1

    sget-object v2, Lco/uk/getmondo/settings/SettingsActivity;->c:Lco/uk/getmondo/settings/SettingsActivity$a;

    invoke-static {v2}, Lco/uk/getmondo/settings/SettingsActivity$a;->a(Lco/uk/getmondo/settings/SettingsActivity$a;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lco/uk/getmondo/settings/a;->show(Landroid/support/v4/app/n;Ljava/lang/String;)V

    .line 142
    return-void
.end method

.method public B()V
    .locals 3

    .prologue
    .line 145
    sget-object v0, Lco/uk/getmondo/settings/x;->a:Lco/uk/getmondo/settings/x$a;

    invoke-virtual {v0}, Lco/uk/getmondo/settings/x$a;->a()Lco/uk/getmondo/settings/x;

    move-result-object v0

    invoke-virtual {p0}, Lco/uk/getmondo/settings/SettingsActivity;->getSupportFragmentManager()Landroid/support/v4/app/n;

    move-result-object v1

    sget-object v2, Lco/uk/getmondo/settings/SettingsActivity;->c:Lco/uk/getmondo/settings/SettingsActivity$a;

    invoke-static {v2}, Lco/uk/getmondo/settings/SettingsActivity$a;->b(Lco/uk/getmondo/settings/SettingsActivity$a;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lco/uk/getmondo/settings/x;->show(Landroid/support/v4/app/n;Ljava/lang/String;)V

    .line 146
    return-void
.end method

.method public C()V
    .locals 2

    .prologue
    .line 149
    sget-object v1, Lco/uk/getmondo/profile/address/SelectAddressActivity;->b:Lco/uk/getmondo/profile/address/SelectAddressActivity$a;

    move-object v0, p0

    check-cast v0, Landroid/content/Context;

    invoke-virtual {v1, v0}, Lco/uk/getmondo/profile/address/SelectAddressActivity$a;->a(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lco/uk/getmondo/settings/SettingsActivity;->startActivity(Landroid/content/Intent;)V

    .line 150
    return-void
.end method

.method public D()V
    .locals 0

    .prologue
    .line 153
    check-cast p0, Landroid/content/Context;

    invoke-static {p0}, Lco/uk/getmondo/settings/OpenSourceLicensesActivity;->a(Landroid/content/Context;)V

    .line 154
    return-void
.end method

.method public E()V
    .locals 3

    .prologue
    .line 157
    invoke-static {}, Lco/uk/getmondo/settings/c;->a()Lco/uk/getmondo/settings/c;

    move-result-object v0

    invoke-virtual {p0}, Lco/uk/getmondo/settings/SettingsActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    sget-object v2, Lco/uk/getmondo/settings/SettingsActivity;->c:Lco/uk/getmondo/settings/SettingsActivity$a;

    invoke-static {v2}, Lco/uk/getmondo/settings/SettingsActivity$a;->c(Lco/uk/getmondo/settings/SettingsActivity$a;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lco/uk/getmondo/settings/c;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    .line 158
    return-void
.end method

.method public F()V
    .locals 1

    .prologue
    .line 161
    sget-object v0, Lco/uk/getmondo/settings/LimitsActivity;->b:Lco/uk/getmondo/settings/LimitsActivity$a;

    check-cast p0, Landroid/content/Context;

    invoke-virtual {v0, p0}, Lco/uk/getmondo/settings/LimitsActivity$a;->a(Landroid/content/Context;)V

    .line 162
    return-void
.end method

.method public G()V
    .locals 1

    .prologue
    .line 165
    const-string v0, "https://monzo.com/about"

    invoke-direct {p0, v0}, Lco/uk/getmondo/settings/SettingsActivity;->a(Ljava/lang/String;)V

    .line 166
    return-void
.end method

.method public H()V
    .locals 1

    .prologue
    .line 169
    move-object v0, p0

    check-cast v0, Landroid/content/Context;

    invoke-static {v0}, Lco/uk/getmondo/terms_and_conditions/TermsAndConditionsActivity;->a(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lco/uk/getmondo/settings/SettingsActivity;->startActivity(Landroid/content/Intent;)V

    .line 170
    return-void
.end method

.method public I()V
    .locals 1

    .prologue
    .line 173
    const-string v0, "https://monzo.com/privacy"

    invoke-direct {p0, v0}, Lco/uk/getmondo/settings/SettingsActivity;->a(Ljava/lang/String;)V

    .line 174
    return-void
.end method

.method public J()V
    .locals 1

    .prologue
    .line 177
    const-string v0, "https://monzo.com/fscs-information"

    invoke-direct {p0, v0}, Lco/uk/getmondo/settings/SettingsActivity;->a(Ljava/lang/String;)V

    .line 178
    return-void
.end method

.method public K()V
    .locals 3

    .prologue
    .line 181
    invoke-static {}, Lco/uk/getmondo/settings/p;->a()Lco/uk/getmondo/settings/p;

    move-result-object v1

    move-object v0, p0

    .line 182
    check-cast v0, Lco/uk/getmondo/settings/p$a;

    invoke-virtual {v1, v0}, Lco/uk/getmondo/settings/p;->a(Lco/uk/getmondo/settings/p$a;)V

    .line 183
    invoke-virtual {p0}, Lco/uk/getmondo/settings/SettingsActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    sget-object v2, Lco/uk/getmondo/settings/SettingsActivity;->c:Lco/uk/getmondo/settings/SettingsActivity$a;

    invoke-static {v2}, Lco/uk/getmondo/settings/SettingsActivity$a;->d(Lco/uk/getmondo/settings/SettingsActivity$a;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lco/uk/getmondo/settings/p;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    .line 184
    return-void
.end method

.method public L()V
    .locals 2

    .prologue
    .line 195
    sget v0, Lco/uk/getmondo/c$a;->magStripeSwitchView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/settings/SettingsActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/settings/SwitchView;

    const v1, 0x7f0a0375

    invoke-virtual {p0, v1}, Lco/uk/getmondo/settings/SettingsActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lco/uk/getmondo/settings/SwitchView;->setDescription(Ljava/lang/String;)V

    .line 196
    sget v0, Lco/uk/getmondo/c$a;->magStripeSwitchView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/settings/SettingsActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/settings/SwitchView;

    invoke-virtual {v0}, Lco/uk/getmondo/settings/SwitchView;->d()V

    .line 197
    return-void
.end method

.method public M()V
    .locals 1

    .prologue
    .line 200
    sget v0, Lco/uk/getmondo/c$a;->magStripeSwitchView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/settings/SettingsActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/settings/SwitchView;

    invoke-virtual {v0}, Lco/uk/getmondo/settings/SwitchView;->a()V

    .line 201
    return-void
.end method

.method public N()V
    .locals 1

    .prologue
    .line 204
    sget v0, Lco/uk/getmondo/c$a;->magStripeSwitchView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/settings/SettingsActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/settings/SwitchView;

    invoke-virtual {v0}, Lco/uk/getmondo/settings/SwitchView;->b()V

    .line 205
    return-void
.end method

.method public O()V
    .locals 1

    .prologue
    .line 208
    sget v0, Lco/uk/getmondo/c$a;->paymentsTitleTextView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/settings/SettingsActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-static {v0}, Lco/uk/getmondo/common/ae;->a(Landroid/view/View;)V

    .line 209
    sget v0, Lco/uk/getmondo/c$a;->paymentsSwitchView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/settings/SettingsActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/settings/SwitchView;

    invoke-static {v0}, Lco/uk/getmondo/common/ae;->a(Landroid/view/View;)V

    .line 210
    return-void
.end method

.method public P()V
    .locals 1

    .prologue
    .line 213
    sget v0, Lco/uk/getmondo/c$a;->paymentsTitleTextView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/settings/SettingsActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-static {v0}, Lco/uk/getmondo/common/ae;->b(Landroid/view/View;)V

    .line 214
    sget v0, Lco/uk/getmondo/c$a;->paymentsSwitchView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/settings/SettingsActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/settings/SwitchView;

    invoke-static {v0}, Lco/uk/getmondo/common/ae;->b(Landroid/view/View;)V

    .line 215
    return-void
.end method

.method public Q()V
    .locals 3

    .prologue
    .line 218
    invoke-static {}, Lco/uk/getmondo/common/d/e;->a()Lco/uk/getmondo/common/d/e;

    move-result-object v0

    invoke-virtual {p0}, Lco/uk/getmondo/settings/SettingsActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    sget-object v2, Lco/uk/getmondo/settings/SettingsActivity;->c:Lco/uk/getmondo/settings/SettingsActivity$a;

    invoke-static {v2}, Lco/uk/getmondo/settings/SettingsActivity$a;->e(Lco/uk/getmondo/settings/SettingsActivity$a;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lco/uk/getmondo/common/d/e;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    .line 219
    return-void
.end method

.method public R()V
    .locals 1

    .prologue
    .line 222
    sget v0, Lco/uk/getmondo/c$a;->paymentsSwitchView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/settings/SettingsActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/settings/SwitchView;

    invoke-virtual {v0}, Lco/uk/getmondo/settings/SwitchView;->c()V

    .line 223
    return-void
.end method

.method public S()V
    .locals 1

    .prologue
    .line 226
    sget v0, Lco/uk/getmondo/c$a;->paymentsSwitchView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/settings/SettingsActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/settings/SwitchView;

    invoke-virtual {v0}, Lco/uk/getmondo/settings/SwitchView;->d()V

    .line 227
    return-void
.end method

.method public T()V
    .locals 1

    .prologue
    .line 230
    sget v0, Lco/uk/getmondo/c$a;->paymentsSwitchView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/settings/SettingsActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/settings/SwitchView;

    invoke-virtual {v0}, Lco/uk/getmondo/settings/SwitchView;->a()V

    .line 231
    return-void
.end method

.method public U()V
    .locals 1

    .prologue
    .line 234
    sget v0, Lco/uk/getmondo/c$a;->paymentsSwitchView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/settings/SettingsActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/settings/SwitchView;

    invoke-virtual {v0}, Lco/uk/getmondo/settings/SwitchView;->b()V

    .line 235
    return-void
.end method

.method public V()V
    .locals 1

    .prologue
    .line 238
    sget v0, Lco/uk/getmondo/c$a;->notificationsSwitchView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/settings/SettingsActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/settings/SwitchView;

    invoke-virtual {v0}, Lco/uk/getmondo/settings/SwitchView;->c()V

    .line 239
    return-void
.end method

.method public W()V
    .locals 1

    .prologue
    .line 242
    sget v0, Lco/uk/getmondo/c$a;->notificationsSwitchView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/settings/SettingsActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/settings/SwitchView;

    invoke-virtual {v0}, Lco/uk/getmondo/settings/SwitchView;->d()V

    .line 243
    return-void
.end method

.method public X()V
    .locals 1

    .prologue
    .line 246
    sget v0, Lco/uk/getmondo/c$a;->fscsProtectionView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/settings/SettingsActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-static {v0}, Lco/uk/getmondo/common/ae;->b(Landroid/view/View;)V

    .line 247
    sget v0, Lco/uk/getmondo/c$a;->fscsProtectionSeparatorView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/settings/SettingsActivity;->a(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Lco/uk/getmondo/common/ae;->b(Landroid/view/View;)V

    .line 248
    sget v0, Lco/uk/getmondo/c$a;->bankAccountInfoViewGroup:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/settings/SettingsActivity;->a(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Lco/uk/getmondo/common/ae;->b(Landroid/view/View;)V

    .line 249
    return-void
.end method

.method public Y()V
    .locals 1

    .prologue
    .line 252
    sget v0, Lco/uk/getmondo/c$a;->paymentsTitleTextView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/settings/SettingsActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-static {v0}, Lco/uk/getmondo/common/ae;->b(Landroid/view/View;)V

    .line 253
    sget v0, Lco/uk/getmondo/c$a;->paymentsSwitchView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/settings/SettingsActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/settings/SwitchView;

    invoke-static {v0}, Lco/uk/getmondo/common/ae;->b(Landroid/view/View;)V

    .line 254
    return-void
.end method

.method public a(I)Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lco/uk/getmondo/settings/SettingsActivity;->l:Ljava/util/HashMap;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lco/uk/getmondo/settings/SettingsActivity;->l:Ljava/util/HashMap;

    :cond_0
    iget-object v0, p0, Lco/uk/getmondo/settings/SettingsActivity;->l:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    if-nez v0, :cond_1

    invoke-virtual {p0, p1}, Landroid/support/v4/app/j;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/settings/SettingsActivity;->l:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    return-object v0
.end method

.method public a()V
    .locals 2

    .prologue
    .line 261
    iget-object v0, p0, Lco/uk/getmondo/settings/SettingsActivity;->e:Lcom/b/b/c;

    sget-object v1, Lkotlin/n;->a:Lkotlin/n;

    invoke-virtual {v0, v1}, Lcom/b/b/c;->a(Ljava/lang/Object;)V

    .line 262
    return-void
.end method

.method public a(II)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 187
    invoke-virtual {p0}, Lco/uk/getmondo/settings/SettingsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f120006

    new-array v2, v6, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-virtual {v0, v1, p1, v2}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 188
    invoke-virtual {p0}, Lco/uk/getmondo/settings/SettingsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f120007

    new-array v3, v6, [Ljava/lang/Object;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-virtual {v1, v2, p2, v3}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 189
    invoke-virtual {p0}, Lco/uk/getmondo/settings/SettingsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a01c8

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    aput-object v0, v4, v5

    aput-object v1, v4, v6

    invoke-virtual {v2, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 190
    sget v0, Lco/uk/getmondo/c$a;->magStripeSwitchView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/settings/SettingsActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/settings/SwitchView;

    const v2, 0x7f0a0376

    new-array v3, v6, [Ljava/lang/Object;

    aput-object v1, v3, v5

    invoke-virtual {p0, v2, v3}, Lco/uk/getmondo/settings/SettingsActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lco/uk/getmondo/settings/SwitchView;->setDescription(Ljava/lang/String;)V

    .line 191
    sget v0, Lco/uk/getmondo/c$a;->magStripeSwitchView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/settings/SettingsActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/settings/SwitchView;

    invoke-virtual {v0}, Lco/uk/getmondo/settings/SwitchView;->c()V

    .line 192
    return-void
.end method

.method public a(Lco/uk/getmondo/d/ac;Lorg/threeten/bp/LocalDateTime;)V
    .locals 8

    .prologue
    const/4 v2, 0x0

    const/4 v6, 0x2

    const/4 v7, 0x1

    const/4 v3, 0x0

    const-string v0, "profile"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 116
    sget v0, Lco/uk/getmondo/c$a;->nameTextView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/settings/SettingsActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {p1}, Lco/uk/getmondo/d/ac;->a()Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 117
    sget v0, Lco/uk/getmondo/c$a;->emailTextView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/settings/SettingsActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {p1}, Lco/uk/getmondo/d/ac;->d()Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 118
    sget v0, Lco/uk/getmondo/c$a;->addressTextView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/settings/SettingsActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {p1}, Lco/uk/getmondo/d/ac;->h()Lco/uk/getmondo/d/s;

    move-result-object v1

    invoke-static {v1}, Lco/uk/getmondo/common/k/a;->b(Lco/uk/getmondo/d/s;)Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 119
    sget-object v0, Lkotlin/d/b/ab;->a:Lkotlin/d/b/ab;

    const v0, 0x7f0a0379

    invoke-virtual {p0, v0}, Lco/uk/getmondo/settings/SettingsActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "getString(R.string.settings_monzo_user_format)"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    new-array v1, v7, [Ljava/lang/Object;

    invoke-virtual {p1}, Lco/uk/getmondo/d/ac;->g()Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v1, v3

    array-length v4, v1

    invoke-static {v1, v4}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v1

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    const-string v0, "java.lang.String.format(format, *args)"

    invoke-static {v4, v0}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 120
    invoke-virtual {p1}, Lco/uk/getmondo/d/ac;->g()Ljava/lang/Integer;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    :goto_0
    invoke-static {v0}, Lco/uk/getmondo/common/k/e;->a(I)Ljava/lang/String;

    move-result-object v1

    move-object v0, v1

    .line 121
    check-cast v0, Ljava/lang/CharSequence;

    if-eqz v0, :cond_0

    invoke-static {v0}, Lkotlin/h/j;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_0
    move v0, v7

    :goto_1
    if-nez v0, :cond_4

    .line 122
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, " "

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 124
    :goto_2
    sget v0, Lco/uk/getmondo/c$a;->monzoUserNumberTextView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/settings/SettingsActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 125
    if-eqz p2, :cond_1

    .line 126
    invoke-virtual {p2}, Lorg/threeten/bp/LocalDateTime;->d()Lorg/threeten/bp/Month;

    move-result-object v0

    sget-object v1, Lorg/threeten/bp/format/TextStyle;->a:Lorg/threeten/bp/format/TextStyle;

    sget-object v4, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {v0, v1, v4}, Lorg/threeten/bp/Month;->a(Lorg/threeten/bp/format/TextStyle;Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v1

    .line 127
    sget v0, Lco/uk/getmondo/c$a;->memberSinceTextView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/settings/SettingsActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    sget-object v4, Lkotlin/d/b/ab;->a:Lkotlin/d/b/ab;

    const v4, 0x7f0a0378

    invoke-virtual {p0, v4}, Lco/uk/getmondo/settings/SettingsActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    const-string v5, "getString(R.string.settings_member_since_format)"

    invoke-static {v4, v5}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    new-array v5, v6, [Ljava/lang/Object;

    aput-object v1, v5, v3

    invoke-virtual {p2}, Lorg/threeten/bp/LocalDateTime;->b()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v5, v7

    array-length v1, v5

    invoke-static {v5, v1}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v1

    invoke-static {v4, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const-string v4, "java.lang.String.format(format, *args)"

    invoke-static {v1, v4}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 130
    :cond_1
    const/high16 v0, 0x41900000    # 18.0f

    invoke-virtual {p0}, Lco/uk/getmondo/settings/SettingsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    invoke-static {v6, v0, v1}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v0

    float-to-int v1, v0

    .line 131
    sget v0, Lco/uk/getmondo/c$a;->userImageView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/settings/SettingsActivity;->a(I)Landroid/view/View;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Landroid/widget/ImageView;

    invoke-direct {p0}, Lco/uk/getmondo/settings/SettingsActivity;->ae()Lco/uk/getmondo/common/ui/a;

    move-result-object v0

    invoke-virtual {p1}, Lco/uk/getmondo/d/ac;->c()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Lco/uk/getmondo/common/ui/a;->a(Ljava/lang/String;)Lco/uk/getmondo/common/ui/a$b;

    move-result-object v0

    const/4 v4, 0x6

    move-object v5, v2

    invoke-static/range {v0 .. v5}, Lco/uk/getmondo/common/ui/a$b;->a(Lco/uk/getmondo/common/ui/a$b;ILandroid/graphics/Typeface;ZILjava/lang/Object;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v6, v0}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 132
    sget v0, Lco/uk/getmondo/c$a;->appVersionTextView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/settings/SettingsActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const v1, 0x7f0a0361

    new-array v2, v7, [Ljava/lang/Object;

    const-string v4, "1.14.1"

    aput-object v4, v2, v3

    invoke-virtual {p0, v1, v2}, Lco/uk/getmondo/settings/SettingsActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 133
    return-void

    :cond_2
    move v0, v3

    .line 120
    goto/16 :goto_0

    :cond_3
    move v0, v3

    .line 121
    goto/16 :goto_1

    :cond_4
    move-object v1, v4

    goto/16 :goto_2
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    const-string v0, "accountNumber"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "sortCode"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 136
    sget v0, Lco/uk/getmondo/c$a;->accountNumberTextView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/settings/SettingsActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    check-cast p1, Ljava/lang/CharSequence;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 137
    sget v0, Lco/uk/getmondo/c$a;->sortCodeTextView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/settings/SettingsActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    check-cast p2, Ljava/lang/CharSequence;

    invoke-virtual {v0, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 138
    return-void
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    const-string v0, "profileName"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "accountNumber"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "sortCode"

    invoke-static {p3, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 257
    const v0, 0x7f0a0381

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    const/4 v2, 0x1

    aput-object p2, v1, v2

    const/4 v2, 0x2

    aput-object p3, v1, v2

    invoke-virtual {p0, v0, v1}, Lco/uk/getmondo/settings/SettingsActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lco/uk/getmondo/common/k/j;->a(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lco/uk/getmondo/settings/SettingsActivity;->startActivity(Landroid/content/Intent;)V

    .line 258
    return-void
.end method

.method public b()Lio/reactivex/n;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Lkotlin/n;",
            ">;"
        }
    .end annotation

    .prologue
    .line 56
    sget v0, Lco/uk/getmondo/c$a;->nameViewGroup:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/settings/SettingsActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 289
    invoke-static {v0}, Lcom/b/a/c/c;->a(Landroid/view/View;)Lio/reactivex/n;

    move-result-object v1

    sget-object v0, Lcom/b/a/a/d;->a:Lcom/b/a/a/d;

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {v1, v0}, Lio/reactivex/n;->map(Lio/reactivex/c/h;)Lio/reactivex/n;

    move-result-object v0

    const-string v1, "RxView.clicks(this).map(VoidToUnit)"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public c()Lio/reactivex/n;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Lkotlin/n;",
            ">;"
        }
    .end annotation

    .prologue
    .line 60
    sget v0, Lco/uk/getmondo/c$a;->addressViewGroup:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/settings/SettingsActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 290
    invoke-static {v0}, Lcom/b/a/c/c;->a(Landroid/view/View;)Lio/reactivex/n;

    move-result-object v1

    sget-object v0, Lcom/b/a/a/d;->a:Lcom/b/a/a/d;

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {v1, v0}, Lio/reactivex/n;->map(Lio/reactivex/c/h;)Lio/reactivex/n;

    move-result-object v0

    const-string v1, "RxView.clicks(this).map(VoidToUnit)"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public d()Lio/reactivex/n;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Lkotlin/n;",
            ">;"
        }
    .end annotation

    .prologue
    .line 64
    sget v0, Lco/uk/getmondo/c$a;->aboutMonzoView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/settings/SettingsActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 291
    invoke-static {v0}, Lcom/b/a/c/c;->a(Landroid/view/View;)Lio/reactivex/n;

    move-result-object v1

    sget-object v0, Lcom/b/a/a/d;->a:Lcom/b/a/a/d;

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {v1, v0}, Lio/reactivex/n;->map(Lio/reactivex/c/h;)Lio/reactivex/n;

    move-result-object v0

    const-string v1, "RxView.clicks(this).map(VoidToUnit)"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public e()Lio/reactivex/n;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Lkotlin/n;",
            ">;"
        }
    .end annotation

    .prologue
    .line 68
    sget v0, Lco/uk/getmondo/c$a;->termsAndConditionsView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/settings/SettingsActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 292
    invoke-static {v0}, Lcom/b/a/c/c;->a(Landroid/view/View;)Lio/reactivex/n;

    move-result-object v1

    sget-object v0, Lcom/b/a/a/d;->a:Lcom/b/a/a/d;

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {v1, v0}, Lio/reactivex/n;->map(Lio/reactivex/c/h;)Lio/reactivex/n;

    move-result-object v0

    const-string v1, "RxView.clicks(this).map(VoidToUnit)"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public f()Lio/reactivex/n;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Lkotlin/n;",
            ">;"
        }
    .end annotation

    .prologue
    .line 72
    sget v0, Lco/uk/getmondo/c$a;->privacyPolicyView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/settings/SettingsActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 293
    invoke-static {v0}, Lcom/b/a/c/c;->a(Landroid/view/View;)Lio/reactivex/n;

    move-result-object v1

    sget-object v0, Lcom/b/a/a/d;->a:Lcom/b/a/a/d;

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {v1, v0}, Lio/reactivex/n;->map(Lio/reactivex/c/h;)Lio/reactivex/n;

    move-result-object v0

    const-string v1, "RxView.clicks(this).map(VoidToUnit)"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public g()Lio/reactivex/n;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Lkotlin/n;",
            ">;"
        }
    .end annotation

    .prologue
    .line 76
    sget v0, Lco/uk/getmondo/c$a;->fscsProtectionView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/settings/SettingsActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 294
    invoke-static {v0}, Lcom/b/a/c/c;->a(Landroid/view/View;)Lio/reactivex/n;

    move-result-object v1

    sget-object v0, Lcom/b/a/a/d;->a:Lcom/b/a/a/d;

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {v1, v0}, Lio/reactivex/n;->map(Lio/reactivex/c/h;)Lio/reactivex/n;

    move-result-object v0

    const-string v1, "RxView.clicks(this).map(VoidToUnit)"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public h()Lio/reactivex/n;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Lkotlin/n;",
            ">;"
        }
    .end annotation

    .prologue
    .line 80
    sget v0, Lco/uk/getmondo/c$a;->limitsTextView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/settings/SettingsActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 295
    invoke-static {v0}, Lcom/b/a/c/c;->a(Landroid/view/View;)Lio/reactivex/n;

    move-result-object v1

    sget-object v0, Lcom/b/a/a/d;->a:Lcom/b/a/a/d;

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {v1, v0}, Lio/reactivex/n;->map(Lio/reactivex/c/h;)Lio/reactivex/n;

    move-result-object v0

    const-string v1, "RxView.clicks(this).map(VoidToUnit)"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public i()Lio/reactivex/n;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Lkotlin/n;",
            ">;"
        }
    .end annotation

    .prologue
    .line 84
    sget v0, Lco/uk/getmondo/c$a;->openSourceLicensesView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/settings/SettingsActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 296
    invoke-static {v0}, Lcom/b/a/c/c;->a(Landroid/view/View;)Lio/reactivex/n;

    move-result-object v1

    sget-object v0, Lcom/b/a/a/d;->a:Lcom/b/a/a/d;

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {v1, v0}, Lio/reactivex/n;->map(Lio/reactivex/c/h;)Lio/reactivex/n;

    move-result-object v0

    const-string v1, "RxView.clicks(this).map(VoidToUnit)"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public j()Lio/reactivex/n;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Lkotlin/n;",
            ">;"
        }
    .end annotation

    .prologue
    .line 88
    sget v0, Lco/uk/getmondo/c$a;->logOutView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/settings/SettingsActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 297
    invoke-static {v0}, Lcom/b/a/c/c;->a(Landroid/view/View;)Lio/reactivex/n;

    move-result-object v1

    sget-object v0, Lcom/b/a/a/d;->a:Lcom/b/a/a/d;

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {v1, v0}, Lio/reactivex/n;->map(Lio/reactivex/c/h;)Lio/reactivex/n;

    move-result-object v0

    const-string v1, "RxView.clicks(this).map(VoidToUnit)"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public k()Lio/reactivex/n;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Lkotlin/n;",
            ">;"
        }
    .end annotation

    .prologue
    .line 92
    iget-object v0, p0, Lco/uk/getmondo/settings/SettingsActivity;->e:Lcom/b/b/c;

    const-string v1, "logOutConfirmationRelay"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lio/reactivex/n;

    return-object v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 41
    invoke-super {p0, p1}, Lco/uk/getmondo/common/activities/b;->onCreate(Landroid/os/Bundle;)V

    .line 43
    const v0, 0x7f05005d

    invoke-virtual {p0, v0}, Lco/uk/getmondo/settings/SettingsActivity;->setContentView(I)V

    .line 45
    invoke-virtual {p0}, Lco/uk/getmondo/settings/SettingsActivity;->l()Lco/uk/getmondo/common/h/b/b;

    move-result-object v0

    invoke-interface {v0, p0}, Lco/uk/getmondo/common/h/b/b;->a(Lco/uk/getmondo/settings/SettingsActivity;)V

    .line 46
    iget-object v0, p0, Lco/uk/getmondo/settings/SettingsActivity;->b:Lco/uk/getmondo/settings/u;

    if-nez v0, :cond_0

    const-string v1, "presenter"

    invoke-static {v1}, Lkotlin/d/b/l;->b(Ljava/lang/String;)V

    :cond_0
    check-cast p0, Lco/uk/getmondo/settings/u$a;

    invoke-virtual {v0, p0}, Lco/uk/getmondo/settings/u;->a(Lco/uk/getmondo/settings/u$a;)V

    .line 47
    return-void
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    .line 50
    iget-object v0, p0, Lco/uk/getmondo/settings/SettingsActivity;->b:Lco/uk/getmondo/settings/u;

    if-nez v0, :cond_0

    const-string v1, "presenter"

    invoke-static {v1}, Lkotlin/d/b/l;->b(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0}, Lco/uk/getmondo/settings/u;->b()V

    .line 52
    invoke-super {p0}, Lco/uk/getmondo/common/activities/b;->onDestroy()V

    .line 53
    return-void
.end method

.method public v()Lio/reactivex/n;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Lkotlin/n;",
            ">;"
        }
    .end annotation

    .prologue
    .line 96
    sget v0, Lco/uk/getmondo/c$a;->closeAccountView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/settings/SettingsActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 298
    invoke-static {v0}, Lcom/b/a/c/c;->a(Landroid/view/View;)Lio/reactivex/n;

    move-result-object v1

    sget-object v0, Lcom/b/a/a/d;->a:Lcom/b/a/a/d;

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {v1, v0}, Lio/reactivex/n;->map(Lio/reactivex/c/h;)Lio/reactivex/n;

    move-result-object v0

    const-string v1, "RxView.clicks(this).map(VoidToUnit)"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public w()Lio/reactivex/n;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .prologue
    .line 100
    sget v0, Lco/uk/getmondo/c$a;->notificationsSwitchView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/settings/SettingsActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/settings/SwitchView;

    .line 299
    invoke-static {v0}, Lcom/b/a/c/c;->a(Landroid/view/View;)Lio/reactivex/n;

    move-result-object v1

    sget-object v0, Lcom/b/a/a/d;->a:Lcom/b/a/a/d;

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {v1, v0}, Lio/reactivex/n;->map(Lio/reactivex/c/h;)Lio/reactivex/n;

    move-result-object v1

    const-string v0, "RxView.clicks(this).map(VoidToUnit)"

    invoke-static {v1, v0}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 100
    new-instance v0, Lco/uk/getmondo/settings/SettingsActivity$d;

    invoke-direct {v0, p0}, Lco/uk/getmondo/settings/SettingsActivity$d;-><init>(Lco/uk/getmondo/settings/SettingsActivity;)V

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {v1, v0}, Lio/reactivex/n;->map(Lio/reactivex/c/h;)Lio/reactivex/n;

    move-result-object v0

    const-string v1, "notificationsSwitchView.\u2026onsSwitchView.isChecked }"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public x()Lio/reactivex/n;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .prologue
    .line 104
    sget v0, Lco/uk/getmondo/c$a;->magStripeSwitchView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/settings/SettingsActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/settings/SwitchView;

    .line 300
    invoke-static {v0}, Lcom/b/a/c/c;->a(Landroid/view/View;)Lio/reactivex/n;

    move-result-object v1

    sget-object v0, Lcom/b/a/a/d;->a:Lcom/b/a/a/d;

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {v1, v0}, Lio/reactivex/n;->map(Lio/reactivex/c/h;)Lio/reactivex/n;

    move-result-object v1

    const-string v0, "RxView.clicks(this).map(VoidToUnit)"

    invoke-static {v1, v0}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 104
    new-instance v0, Lco/uk/getmondo/settings/SettingsActivity$c;

    invoke-direct {v0, p0}, Lco/uk/getmondo/settings/SettingsActivity$c;-><init>(Lco/uk/getmondo/settings/SettingsActivity;)V

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {v1, v0}, Lio/reactivex/n;->map(Lio/reactivex/c/h;)Lio/reactivex/n;

    move-result-object v0

    const-string v1, "magStripeSwitchView.clic\u2026ipeSwitchView.isChecked }"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public y()Lio/reactivex/n;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .prologue
    .line 108
    sget v0, Lco/uk/getmondo/c$a;->paymentsSwitchView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/settings/SettingsActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/settings/SwitchView;

    .line 301
    invoke-static {v0}, Lcom/b/a/c/c;->a(Landroid/view/View;)Lio/reactivex/n;

    move-result-object v1

    sget-object v0, Lcom/b/a/a/d;->a:Lcom/b/a/a/d;

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {v1, v0}, Lio/reactivex/n;->map(Lio/reactivex/c/h;)Lio/reactivex/n;

    move-result-object v1

    const-string v0, "RxView.clicks(this).map(VoidToUnit)"

    invoke-static {v1, v0}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 108
    new-instance v0, Lco/uk/getmondo/settings/SettingsActivity$e;

    invoke-direct {v0, p0}, Lco/uk/getmondo/settings/SettingsActivity$e;-><init>(Lco/uk/getmondo/settings/SettingsActivity;)V

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {v1, v0}, Lio/reactivex/n;->map(Lio/reactivex/c/h;)Lio/reactivex/n;

    move-result-object v0

    const-string v1, "paymentsSwitchView.click\u2026ntsSwitchView.isChecked }"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public z()Lio/reactivex/n;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Lkotlin/n;",
            ">;"
        }
    .end annotation

    .prologue
    .line 112
    sget v0, Lco/uk/getmondo/c$a;->shareAccountDetailsButton:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/settings/SettingsActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 302
    invoke-static {v0}, Lcom/b/a/c/c;->a(Landroid/view/View;)Lio/reactivex/n;

    move-result-object v1

    sget-object v0, Lcom/b/a/a/d;->a:Lcom/b/a/a/d;

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {v1, v0}, Lio/reactivex/n;->map(Lio/reactivex/c/h;)Lio/reactivex/n;

    move-result-object v0

    const-string v1, "RxView.clicks(this).map(VoidToUnit)"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method
