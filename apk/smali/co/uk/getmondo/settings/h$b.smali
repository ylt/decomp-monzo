.class final Lco/uk/getmondo/settings/h$b;
.super Ljava/lang/Object;
.source "LimitsPresenter.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lco/uk/getmondo/settings/h;->a(Lco/uk/getmondo/settings/h$a;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h",
        "<TT;",
        "Lio/reactivex/l",
        "<+TR;>;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000\u0016\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u0010\u0012\u000c\u0012\n \u0003*\u0004\u0018\u00010\u00020\u00020\u00012\u0006\u0010\u0004\u001a\u00020\u0005H\n\u00a2\u0006\u0004\u0008\u0006\u0010\u0007"
    }
    d2 = {
        "<anonymous>",
        "Lio/reactivex/Maybe;",
        "Lco/uk/getmondo/model/BalanceLimit;",
        "kotlin.jvm.PlatformType",
        "it",
        "",
        "apply",
        "(Lkotlin/Unit;)Lio/reactivex/Maybe;"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field final synthetic a:Lco/uk/getmondo/settings/h;

.field final synthetic b:Lco/uk/getmondo/d/a;

.field final synthetic c:Lco/uk/getmondo/settings/h$a;


# direct methods
.method constructor <init>(Lco/uk/getmondo/settings/h;Lco/uk/getmondo/d/a;Lco/uk/getmondo/settings/h$a;)V
    .locals 0

    iput-object p1, p0, Lco/uk/getmondo/settings/h$b;->a:Lco/uk/getmondo/settings/h;

    iput-object p2, p0, Lco/uk/getmondo/settings/h$b;->b:Lco/uk/getmondo/d/a;

    iput-object p3, p0, Lco/uk/getmondo/settings/h$b;->c:Lco/uk/getmondo/settings/h$a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lkotlin/n;)Lio/reactivex/h;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/n;",
            ")",
            "Lio/reactivex/h",
            "<",
            "Lco/uk/getmondo/d/e;",
            ">;"
        }
    .end annotation

    .prologue
    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 50
    iget-object v0, p0, Lco/uk/getmondo/settings/h$b;->a:Lco/uk/getmondo/settings/h;

    invoke-static {v0}, Lco/uk/getmondo/settings/h;->a(Lco/uk/getmondo/settings/h;)Lco/uk/getmondo/api/MonzoApi;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/settings/h$b;->b:Lco/uk/getmondo/d/a;

    invoke-interface {v1}, Lco/uk/getmondo/d/a;->a()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lco/uk/getmondo/api/MonzoApi;->balanceLimits(Ljava/lang/String;)Lio/reactivex/v;

    move-result-object v1

    .line 51
    sget-object v0, Lco/uk/getmondo/settings/h$b$1;->a:Lco/uk/getmondo/settings/h$b$1;

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {v1, v0}, Lio/reactivex/v;->d(Lio/reactivex/c/h;)Lio/reactivex/v;

    move-result-object v0

    .line 52
    iget-object v1, p0, Lco/uk/getmondo/settings/h$b;->a:Lco/uk/getmondo/settings/h;

    invoke-static {v1}, Lco/uk/getmondo/settings/h;->b(Lco/uk/getmondo/settings/h;)Lio/reactivex/u;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/v;->b(Lio/reactivex/u;)Lio/reactivex/v;

    move-result-object v0

    .line 53
    iget-object v1, p0, Lco/uk/getmondo/settings/h$b;->a:Lco/uk/getmondo/settings/h;

    invoke-static {v1}, Lco/uk/getmondo/settings/h;->c(Lco/uk/getmondo/settings/h;)Lio/reactivex/u;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/v;->a(Lio/reactivex/u;)Lio/reactivex/v;

    move-result-object v1

    .line 54
    new-instance v0, Lco/uk/getmondo/settings/h$b$2;

    invoke-direct {v0, p0}, Lco/uk/getmondo/settings/h$b$2;-><init>(Lco/uk/getmondo/settings/h$b;)V

    check-cast v0, Lio/reactivex/c/g;

    invoke-virtual {v1, v0}, Lio/reactivex/v;->b(Lio/reactivex/c/g;)Lio/reactivex/v;

    move-result-object v1

    .line 55
    new-instance v0, Lco/uk/getmondo/settings/h$b$3;

    invoke-direct {v0, p0}, Lco/uk/getmondo/settings/h$b$3;-><init>(Lco/uk/getmondo/settings/h$b;)V

    check-cast v0, Lio/reactivex/c/b;

    invoke-virtual {v1, v0}, Lio/reactivex/v;->a(Lio/reactivex/c/b;)Lio/reactivex/v;

    move-result-object v1

    .line 56
    new-instance v0, Lco/uk/getmondo/settings/h$b$4;

    invoke-direct {v0, p0}, Lco/uk/getmondo/settings/h$b$4;-><init>(Lco/uk/getmondo/settings/h$b;)V

    check-cast v0, Lio/reactivex/c/g;

    invoke-virtual {v1, v0}, Lio/reactivex/v;->d(Lio/reactivex/c/g;)Lio/reactivex/v;

    move-result-object v0

    .line 57
    invoke-static {v0}, Lco/uk/getmondo/common/j/f;->a(Lio/reactivex/v;)Lio/reactivex/h;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 30
    check-cast p1, Lkotlin/n;

    invoke-virtual {p0, p1}, Lco/uk/getmondo/settings/h$b;->a(Lkotlin/n;)Lio/reactivex/h;

    move-result-object v0

    return-object v0
.end method
