.class public Lco/uk/getmondo/settings/OpenSourceLicensesActivity$OpenSourceLibraryHolder_ViewBinding;
.super Ljava/lang/Object;
.source "OpenSourceLicensesActivity$OpenSourceLibraryHolder_ViewBinding.java"

# interfaces
.implements Lbutterknife/Unbinder;


# instance fields
.field private a:Lco/uk/getmondo/settings/OpenSourceLicensesActivity$OpenSourceLibraryHolder;


# direct methods
.method public constructor <init>(Lco/uk/getmondo/settings/OpenSourceLicensesActivity$OpenSourceLibraryHolder;Landroid/view/View;)V
    .locals 3

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    iput-object p1, p0, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$OpenSourceLibraryHolder_ViewBinding;->a:Lco/uk/getmondo/settings/OpenSourceLicensesActivity$OpenSourceLibraryHolder;

    .line 21
    const v0, 0x7f110424

    const-string v1, "field \'libraryNameTextView\'"

    const-class v2, Landroid/widget/TextView;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p1, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$OpenSourceLibraryHolder;->libraryNameTextView:Landroid/widget/TextView;

    .line 22
    const v0, 0x7f110425

    const-string v1, "field \'libraryLicenseTextView\'"

    const-class v2, Landroid/widget/TextView;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p1, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$OpenSourceLibraryHolder;->libraryLicenseTextView:Landroid/widget/TextView;

    .line 23
    return-void
.end method


# virtual methods
.method public unbind()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 28
    iget-object v0, p0, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$OpenSourceLibraryHolder_ViewBinding;->a:Lco/uk/getmondo/settings/OpenSourceLicensesActivity$OpenSourceLibraryHolder;

    .line 29
    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Bindings already cleared."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 30
    :cond_0
    iput-object v1, p0, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$OpenSourceLibraryHolder_ViewBinding;->a:Lco/uk/getmondo/settings/OpenSourceLicensesActivity$OpenSourceLibraryHolder;

    .line 32
    iput-object v1, v0, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$OpenSourceLibraryHolder;->libraryNameTextView:Landroid/widget/TextView;

    .line 33
    iput-object v1, v0, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$OpenSourceLibraryHolder;->libraryLicenseTextView:Landroid/widget/TextView;

    .line 34
    return-void
.end method
