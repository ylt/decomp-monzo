.class Lco/uk/getmondo/settings/OpenSourceLicensesActivity$d;
.super Landroid/support/v7/widget/RecyclerView$a;
.source "OpenSourceLicensesActivity.java"

# interfaces
.implements La/a/a/a/a/a;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lco/uk/getmondo/settings/OpenSourceLicensesActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "d"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/support/v7/widget/RecyclerView$a",
        "<",
        "Lco/uk/getmondo/settings/OpenSourceLicensesActivity$OpenSourceLibraryHolder;",
        ">;",
        "La/a/a/a/a/a",
        "<",
        "Lco/uk/getmondo/settings/OpenSourceLicensesActivity$HeaderViewHolder;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lco/uk/getmondo/settings/OpenSourceLicensesActivity$c;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>()V
    .locals 6

    .prologue
    .line 132
    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView$a;-><init>()V

    .line 84
    const/16 v0, 0x2d

    new-array v0, v0, [Lco/uk/getmondo/settings/OpenSourceLicensesActivity$c;

    const/4 v1, 0x0

    new-instance v2, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$c;

    const-string v3, "AppCompat"

    sget-object v4, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$b;->a:Lco/uk/getmondo/settings/OpenSourceLicensesActivity$b;

    sget-object v5, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;->b:Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;

    invoke-direct {v2, v3, v4, v5}, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$c;-><init>(Ljava/lang/String;Lco/uk/getmondo/settings/OpenSourceLicensesActivity$b;Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;)V

    aput-object v2, v0, v1

    const/4 v1, 0x1

    new-instance v2, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$c;

    const-string v3, "AOSP"

    sget-object v4, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$b;->a:Lco/uk/getmondo/settings/OpenSourceLicensesActivity$b;

    sget-object v5, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;->b:Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;

    invoke-direct {v2, v3, v4, v5}, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$c;-><init>(Ljava/lang/String;Lco/uk/getmondo/settings/OpenSourceLicensesActivity$b;Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;)V

    aput-object v2, v0, v1

    const/4 v1, 0x2

    new-instance v2, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$c;

    const-string v3, "CardView"

    sget-object v4, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$b;->a:Lco/uk/getmondo/settings/OpenSourceLicensesActivity$b;

    sget-object v5, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;->b:Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;

    invoke-direct {v2, v3, v4, v5}, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$c;-><init>(Ljava/lang/String;Lco/uk/getmondo/settings/OpenSourceLicensesActivity$b;Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;)V

    aput-object v2, v0, v1

    const/4 v1, 0x3

    new-instance v2, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$c;

    const-string v3, "Constraint Layout"

    sget-object v4, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$b;->a:Lco/uk/getmondo/settings/OpenSourceLicensesActivity$b;

    sget-object v5, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;->b:Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;

    invoke-direct {v2, v3, v4, v5}, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$c;-><init>(Ljava/lang/String;Lco/uk/getmondo/settings/OpenSourceLicensesActivity$b;Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;)V

    aput-object v2, v0, v1

    const/4 v1, 0x4

    new-instance v2, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$c;

    const-string v3, "Custom Tabs"

    sget-object v4, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$b;->a:Lco/uk/getmondo/settings/OpenSourceLicensesActivity$b;

    sget-object v5, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;->b:Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;

    invoke-direct {v2, v3, v4, v5}, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$c;-><init>(Ljava/lang/String;Lco/uk/getmondo/settings/OpenSourceLicensesActivity$b;Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;)V

    aput-object v2, v0, v1

    const/4 v1, 0x5

    new-instance v2, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$c;

    const-string v3, "Design"

    sget-object v4, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$b;->a:Lco/uk/getmondo/settings/OpenSourceLicensesActivity$b;

    sget-object v5, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;->b:Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;

    invoke-direct {v2, v3, v4, v5}, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$c;-><init>(Ljava/lang/String;Lco/uk/getmondo/settings/OpenSourceLicensesActivity$b;Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;)V

    aput-object v2, v0, v1

    const/4 v1, 0x6

    new-instance v2, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$c;

    const-string v3, "ExifInterface"

    sget-object v4, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$b;->a:Lco/uk/getmondo/settings/OpenSourceLicensesActivity$b;

    sget-object v5, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;->b:Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;

    invoke-direct {v2, v3, v4, v5}, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$c;-><init>(Ljava/lang/String;Lco/uk/getmondo/settings/OpenSourceLicensesActivity$b;Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;)V

    aput-object v2, v0, v1

    const/4 v1, 0x7

    new-instance v2, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$c;

    const-string v3, "MultiDex"

    sget-object v4, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$b;->a:Lco/uk/getmondo/settings/OpenSourceLicensesActivity$b;

    sget-object v5, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;->b:Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;

    invoke-direct {v2, v3, v4, v5}, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$c;-><init>(Ljava/lang/String;Lco/uk/getmondo/settings/OpenSourceLicensesActivity$b;Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;)V

    aput-object v2, v0, v1

    const/16 v1, 0x8

    new-instance v2, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$c;

    const-string v3, "RecyclerView"

    sget-object v4, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$b;->a:Lco/uk/getmondo/settings/OpenSourceLicensesActivity$b;

    sget-object v5, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;->b:Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;

    invoke-direct {v2, v3, v4, v5}, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$c;-><init>(Ljava/lang/String;Lco/uk/getmondo/settings/OpenSourceLicensesActivity$b;Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;)V

    aput-object v2, v0, v1

    const/16 v1, 0x9

    new-instance v2, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$c;

    const-string v3, "ButterKnife"

    sget-object v4, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$b;->a:Lco/uk/getmondo/settings/OpenSourceLicensesActivity$b;

    sget-object v5, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;->i:Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;

    invoke-direct {v2, v3, v4, v5}, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$c;-><init>(Ljava/lang/String;Lco/uk/getmondo/settings/OpenSourceLicensesActivity$b;Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;)V

    aput-object v2, v0, v1

    const/16 v1, 0xa

    new-instance v2, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$c;

    const-string v3, "Glide"

    sget-object v4, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$b;->a:Lco/uk/getmondo/settings/OpenSourceLicensesActivity$b;

    sget-object v5, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;->h:Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;

    invoke-direct {v2, v3, v4, v5}, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$c;-><init>(Ljava/lang/String;Lco/uk/getmondo/settings/OpenSourceLicensesActivity$b;Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;)V

    aput-object v2, v0, v1

    const/16 v1, 0xb

    new-instance v2, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$c;

    const-string v3, "PhotoView"

    sget-object v4, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$b;->a:Lco/uk/getmondo/settings/OpenSourceLicensesActivity$b;

    sget-object v5, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;->d:Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;

    invoke-direct {v2, v3, v4, v5}, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$c;-><init>(Ljava/lang/String;Lco/uk/getmondo/settings/OpenSourceLicensesActivity$b;Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;)V

    aput-object v2, v0, v1

    const/16 v1, 0xc

    new-instance v2, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$c;

    const-string v3, "CircleIndicator"

    sget-object v4, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$b;->a:Lco/uk/getmondo/settings/OpenSourceLicensesActivity$b;

    sget-object v5, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;->o:Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;

    invoke-direct {v2, v3, v4, v5}, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$c;-><init>(Ljava/lang/String;Lco/uk/getmondo/settings/OpenSourceLicensesActivity$b;Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;)V

    aput-object v2, v0, v1

    const/16 v1, 0xd

    new-instance v2, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$c;

    const-string v3, "TextDrawable"

    sget-object v4, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$b;->b:Lco/uk/getmondo/settings/OpenSourceLicensesActivity$b;

    sget-object v5, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;->a:Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;

    invoke-direct {v2, v3, v4, v5}, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$c;-><init>(Ljava/lang/String;Lco/uk/getmondo/settings/OpenSourceLicensesActivity$b;Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;)V

    aput-object v2, v0, v1

    const/16 v1, 0xe

    new-instance v2, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$c;

    const-string v3, "power-optional"

    sget-object v4, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$b;->a:Lco/uk/getmondo/settings/OpenSourceLicensesActivity$b;

    sget-object v5, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;->k:Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;

    invoke-direct {v2, v3, v4, v5}, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$c;-><init>(Ljava/lang/String;Lco/uk/getmondo/settings/OpenSourceLicensesActivity$b;Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;)V

    aput-object v2, v0, v1

    const/16 v1, 0xf

    new-instance v2, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$c;

    const-string v3, "header-decor"

    sget-object v4, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$b;->a:Lco/uk/getmondo/settings/OpenSourceLicensesActivity$b;

    sget-object v5, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;->e:Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;

    invoke-direct {v2, v3, v4, v5}, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$c;-><init>(Ljava/lang/String;Lco/uk/getmondo/settings/OpenSourceLicensesActivity$b;Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;)V

    aput-object v2, v0, v1

    const/16 v1, 0x10

    new-instance v2, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$c;

    const-string v3, "Gson"

    sget-object v4, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$b;->a:Lco/uk/getmondo/settings/OpenSourceLicensesActivity$b;

    sget-object v5, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;->h:Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;

    invoke-direct {v2, v3, v4, v5}, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$c;-><init>(Ljava/lang/String;Lco/uk/getmondo/settings/OpenSourceLicensesActivity$b;Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;)V

    aput-object v2, v0, v1

    const/16 v1, 0x11

    new-instance v2, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$c;

    const-string v3, "Retrofit 2"

    sget-object v4, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$b;->a:Lco/uk/getmondo/settings/OpenSourceLicensesActivity$b;

    sget-object v5, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;->p:Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;

    invoke-direct {v2, v3, v4, v5}, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$c;-><init>(Ljava/lang/String;Lco/uk/getmondo/settings/OpenSourceLicensesActivity$b;Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;)V

    aput-object v2, v0, v1

    const/16 v1, 0x12

    new-instance v2, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$c;

    const-string v3, "Retrofit 2 Converter (Gson)"

    sget-object v4, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$b;->a:Lco/uk/getmondo/settings/OpenSourceLicensesActivity$b;

    sget-object v5, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;->p:Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;

    invoke-direct {v2, v3, v4, v5}, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$c;-><init>(Ljava/lang/String;Lco/uk/getmondo/settings/OpenSourceLicensesActivity$b;Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;)V

    aput-object v2, v0, v1

    const/16 v1, 0x13

    new-instance v2, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$c;

    const-string v3, "Retrofit 2 Adapter RxJava"

    sget-object v4, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$b;->a:Lco/uk/getmondo/settings/OpenSourceLicensesActivity$b;

    sget-object v5, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;->p:Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;

    invoke-direct {v2, v3, v4, v5}, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$c;-><init>(Ljava/lang/String;Lco/uk/getmondo/settings/OpenSourceLicensesActivity$b;Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;)V

    aput-object v2, v0, v1

    const/16 v1, 0x14

    new-instance v2, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$c;

    const-string v3, "OkHttp"

    sget-object v4, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$b;->a:Lco/uk/getmondo/settings/OpenSourceLicensesActivity$b;

    sget-object v5, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;->p:Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;

    invoke-direct {v2, v3, v4, v5}, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$c;-><init>(Ljava/lang/String;Lco/uk/getmondo/settings/OpenSourceLicensesActivity$b;Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;)V

    aput-object v2, v0, v1

    const/16 v1, 0x15

    new-instance v2, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$c;

    const-string v3, "OkHttp Logging Interceptor"

    sget-object v4, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$b;->a:Lco/uk/getmondo/settings/OpenSourceLicensesActivity$b;

    sget-object v5, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;->p:Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;

    invoke-direct {v2, v3, v4, v5}, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$c;-><init>(Ljava/lang/String;Lco/uk/getmondo/settings/OpenSourceLicensesActivity$b;Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;)V

    aput-object v2, v0, v1

    const/16 v1, 0x16

    new-instance v2, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$c;

    const-string v3, "OkHttp MockWebServer"

    sget-object v4, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$b;->a:Lco/uk/getmondo/settings/OpenSourceLicensesActivity$b;

    sget-object v5, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;->p:Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;

    invoke-direct {v2, v3, v4, v5}, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$c;-><init>(Ljava/lang/String;Lco/uk/getmondo/settings/OpenSourceLicensesActivity$b;Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;)V

    aput-object v2, v0, v1

    const/16 v1, 0x17

    new-instance v2, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$c;

    const-string v3, "Okio"

    sget-object v4, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$b;->a:Lco/uk/getmondo/settings/OpenSourceLicensesActivity$b;

    sget-object v5, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;->p:Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;

    invoke-direct {v2, v3, v4, v5}, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$c;-><init>(Ljava/lang/String;Lco/uk/getmondo/settings/OpenSourceLicensesActivity$b;Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;)V

    aput-object v2, v0, v1

    const/16 v1, 0x18

    new-instance v2, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$c;

    const-string v3, "Leak Canary"

    sget-object v4, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$b;->a:Lco/uk/getmondo/settings/OpenSourceLicensesActivity$b;

    sget-object v5, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;->p:Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;

    invoke-direct {v2, v3, v4, v5}, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$c;-><init>(Ljava/lang/String;Lco/uk/getmondo/settings/OpenSourceLicensesActivity$b;Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;)V

    aput-object v2, v0, v1

    const/16 v1, 0x19

    new-instance v2, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$c;

    const-string v3, "Facebook SDK"

    sget-object v4, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$b;->c:Lco/uk/getmondo/settings/OpenSourceLicensesActivity$b;

    sget-object v5, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;->g:Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;

    invoke-direct {v2, v3, v4, v5}, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$c;-><init>(Ljava/lang/String;Lco/uk/getmondo/settings/OpenSourceLicensesActivity$b;Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;)V

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    new-instance v2, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$c;

    const-string v3, "Stripe"

    sget-object v4, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$b;->a:Lco/uk/getmondo/settings/OpenSourceLicensesActivity$b;

    sget-object v5, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;->q:Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;

    invoke-direct {v2, v3, v4, v5}, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$c;-><init>(Ljava/lang/String;Lco/uk/getmondo/settings/OpenSourceLicensesActivity$b;Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;)V

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    new-instance v2, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$c;

    const-string v3, "libphonenumber"

    sget-object v4, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$b;->a:Lco/uk/getmondo/settings/OpenSourceLicensesActivity$b;

    sget-object v5, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;->l:Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;

    invoke-direct {v2, v3, v4, v5}, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$c;-><init>(Ljava/lang/String;Lco/uk/getmondo/settings/OpenSourceLicensesActivity$b;Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;)V

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    new-instance v2, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$c;

    const-string v3, "Retrolambda"

    sget-object v4, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$b;->a:Lco/uk/getmondo/settings/OpenSourceLicensesActivity$b;

    sget-object v5, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;->f:Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;

    invoke-direct {v2, v3, v4, v5}, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$c;-><init>(Ljava/lang/String;Lco/uk/getmondo/settings/OpenSourceLicensesActivity$b;Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;)V

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    new-instance v2, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$c;

    const-string v3, "RxJava"

    sget-object v4, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$b;->a:Lco/uk/getmondo/settings/OpenSourceLicensesActivity$b;

    sget-object v5, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;->n:Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;

    invoke-direct {v2, v3, v4, v5}, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$c;-><init>(Ljava/lang/String;Lco/uk/getmondo/settings/OpenSourceLicensesActivity$b;Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;)V

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    new-instance v2, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$c;

    const-string v3, "RxAndroid"

    sget-object v4, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$b;->a:Lco/uk/getmondo/settings/OpenSourceLicensesActivity$b;

    sget-object v5, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;->n:Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;

    invoke-direct {v2, v3, v4, v5}, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$c;-><init>(Ljava/lang/String;Lco/uk/getmondo/settings/OpenSourceLicensesActivity$b;Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;)V

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    new-instance v2, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$c;

    const-string v3, "RxJava Proguard Rules"

    sget-object v4, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$b;->a:Lco/uk/getmondo/settings/OpenSourceLicensesActivity$b;

    sget-object v5, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;->c:Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;

    invoke-direct {v2, v3, v4, v5}, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$c;-><init>(Ljava/lang/String;Lco/uk/getmondo/settings/OpenSourceLicensesActivity$b;Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;)V

    aput-object v2, v0, v1

    const/16 v1, 0x20

    new-instance v2, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$c;

    const-string v3, "Timber"

    sget-object v4, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$b;->a:Lco/uk/getmondo/settings/OpenSourceLicensesActivity$b;

    sget-object v5, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;->i:Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;

    invoke-direct {v2, v3, v4, v5}, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$c;-><init>(Ljava/lang/String;Lco/uk/getmondo/settings/OpenSourceLicensesActivity$b;Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;)V

    aput-object v2, v0, v1

    const/16 v1, 0x21

    new-instance v2, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$c;

    const-string v3, "RxBinding"

    sget-object v4, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$b;->a:Lco/uk/getmondo/settings/OpenSourceLicensesActivity$b;

    sget-object v5, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;->i:Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;

    invoke-direct {v2, v3, v4, v5}, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$c;-><init>(Ljava/lang/String;Lco/uk/getmondo/settings/OpenSourceLicensesActivity$b;Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;)V

    aput-object v2, v0, v1

    const/16 v1, 0x22

    new-instance v2, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$c;

    const-string v3, "ThreeTenAbp"

    sget-object v4, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$b;->a:Lco/uk/getmondo/settings/OpenSourceLicensesActivity$b;

    sget-object v5, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;->i:Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;

    invoke-direct {v2, v3, v4, v5}, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$c;-><init>(Ljava/lang/String;Lco/uk/getmondo/settings/OpenSourceLicensesActivity$b;Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;)V

    aput-object v2, v0, v1

    const/16 v1, 0x23

    new-instance v2, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$c;

    const-string v3, "Dagger 2"

    sget-object v4, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$b;->a:Lco/uk/getmondo/settings/OpenSourceLicensesActivity$b;

    sget-object v5, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;->h:Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;

    invoke-direct {v2, v3, v4, v5}, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$c;-><init>(Ljava/lang/String;Lco/uk/getmondo/settings/OpenSourceLicensesActivity$b;Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;)V

    aput-object v2, v0, v1

    const/16 v1, 0x24

    new-instance v2, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$c;

    const-string v3, "cameraview"

    sget-object v4, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$b;->a:Lco/uk/getmondo/settings/OpenSourceLicensesActivity$b;

    sget-object v5, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;->m:Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;

    invoke-direct {v2, v3, v4, v5}, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$c;-><init>(Ljava/lang/String;Lco/uk/getmondo/settings/OpenSourceLicensesActivity$b;Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;)V

    aput-object v2, v0, v1

    const/16 v1, 0x25

    new-instance v2, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$c;

    const-string v3, "mp4parser"

    sget-object v4, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$b;->a:Lco/uk/getmondo/settings/OpenSourceLicensesActivity$b;

    sget-object v5, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;->r:Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;

    invoke-direct {v2, v3, v4, v5}, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$c;-><init>(Ljava/lang/String;Lco/uk/getmondo/settings/OpenSourceLicensesActivity$b;Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;)V

    aput-object v2, v0, v1

    const/16 v1, 0x26

    new-instance v2, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$c;

    const-string v3, "JUnit"

    sget-object v4, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$b;->d:Lco/uk/getmondo/settings/OpenSourceLicensesActivity$b;

    sget-object v5, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;->s:Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;

    invoke-direct {v2, v3, v4, v5}, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$c;-><init>(Ljava/lang/String;Lco/uk/getmondo/settings/OpenSourceLicensesActivity$b;Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;)V

    aput-object v2, v0, v1

    const/16 v1, 0x27

    new-instance v2, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$c;

    const-string v3, "AssertJ"

    sget-object v4, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$b;->a:Lco/uk/getmondo/settings/OpenSourceLicensesActivity$b;

    sget-object v5, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;->t:Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;

    invoke-direct {v2, v3, v4, v5}, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$c;-><init>(Ljava/lang/String;Lco/uk/getmondo/settings/OpenSourceLicensesActivity$b;Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;)V

    aput-object v2, v0, v1

    const/16 v1, 0x28

    new-instance v2, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$c;

    const-string v3, "Espresso"

    sget-object v4, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$b;->a:Lco/uk/getmondo/settings/OpenSourceLicensesActivity$b;

    sget-object v5, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;->h:Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;

    invoke-direct {v2, v3, v4, v5}, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$c;-><init>(Ljava/lang/String;Lco/uk/getmondo/settings/OpenSourceLicensesActivity$b;Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;)V

    aput-object v2, v0, v1

    const/16 v1, 0x29

    new-instance v2, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$c;

    const-string v3, "Dexmaker"

    sget-object v4, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$b;->a:Lco/uk/getmondo/settings/OpenSourceLicensesActivity$b;

    sget-object v5, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;->v:Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;

    invoke-direct {v2, v3, v4, v5}, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$c;-><init>(Ljava/lang/String;Lco/uk/getmondo/settings/OpenSourceLicensesActivity$b;Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;)V

    aput-object v2, v0, v1

    const/16 v1, 0x2a

    new-instance v2, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$c;

    const-string v3, "Realm Java"

    sget-object v4, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$b;->a:Lco/uk/getmondo/settings/OpenSourceLicensesActivity$b;

    sget-object v5, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;->w:Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;

    invoke-direct {v2, v3, v4, v5}, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$c;-><init>(Ljava/lang/String;Lco/uk/getmondo/settings/OpenSourceLicensesActivity$b;Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;)V

    aput-object v2, v0, v1

    const/16 v1, 0x2b

    new-instance v2, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$c;

    const-string v3, "Joda Money"

    sget-object v4, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$b;->a:Lco/uk/getmondo/settings/OpenSourceLicensesActivity$b;

    sget-object v5, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;->x:Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;

    invoke-direct {v2, v3, v4, v5}, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$c;-><init>(Ljava/lang/String;Lco/uk/getmondo/settings/OpenSourceLicensesActivity$b;Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;)V

    aput-object v2, v0, v1

    const/16 v1, 0x2c

    new-instance v2, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$c;

    const-string v3, "Lottie"

    sget-object v4, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$b;->a:Lco/uk/getmondo/settings/OpenSourceLicensesActivity$b;

    sget-object v5, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;->j:Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;

    invoke-direct {v2, v3, v4, v5}, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$c;-><init>(Ljava/lang/String;Lco/uk/getmondo/settings/OpenSourceLicensesActivity$b;Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;)V

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$d;->a:Ljava/util/List;

    .line 133
    iget-object v0, p0, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$d;->a:Ljava/util/List;

    invoke-static {}, Lco/uk/getmondo/settings/s;->a()Ljava/util/Comparator;

    move-result-object v1

    invoke-static {v0, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 139
    return-void
.end method

.method static synthetic a(Lco/uk/getmondo/settings/OpenSourceLicensesActivity$c;Lco/uk/getmondo/settings/OpenSourceLicensesActivity$c;)I
    .locals 2

    .prologue
    .line 134
    invoke-static {p0}, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$c;->a(Lco/uk/getmondo/settings/OpenSourceLicensesActivity$c;)Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;

    move-result-object v0

    invoke-static {v0}, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;->a(Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;)I

    move-result v0

    invoke-static {p1}, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$c;->a(Lco/uk/getmondo/settings/OpenSourceLicensesActivity$c;)Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;

    move-result-object v1

    invoke-static {v1}, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;->a(Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;)I

    move-result v1

    if-ne v0, v1, :cond_0

    .line 135
    invoke-static {p0}, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$c;->b(Lco/uk/getmondo/settings/OpenSourceLicensesActivity$c;)Ljava/lang/String;

    move-result-object v0

    invoke-static {p1}, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$c;->b(Lco/uk/getmondo/settings/OpenSourceLicensesActivity$c;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    .line 137
    :goto_0
    return v0

    :cond_0
    invoke-static {p0}, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$c;->a(Lco/uk/getmondo/settings/OpenSourceLicensesActivity$c;)Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;

    move-result-object v0

    invoke-static {v0}, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;->b(Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;)Ljava/lang/String;

    move-result-object v0

    invoke-static {p1}, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$c;->a(Lco/uk/getmondo/settings/OpenSourceLicensesActivity$c;)Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;

    move-result-object v1

    invoke-static {v1}, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;->b(Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    goto :goto_0
.end method


# virtual methods
.method public a(I)J
    .locals 2

    .prologue
    .line 159
    iget-object v0, p0, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$d;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$c;

    invoke-static {v0}, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$c;->a(Lco/uk/getmondo/settings/OpenSourceLicensesActivity$c;)Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;

    move-result-object v0

    invoke-static {v0}, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;->a(Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;)I

    move-result v0

    int-to-long v0, v0

    return-wide v0
.end method

.method public synthetic a(Landroid/view/ViewGroup;)Landroid/support/v7/widget/RecyclerView$w;
    .locals 1

    .prologue
    .line 82
    invoke-virtual {p0, p1}, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$d;->b(Landroid/view/ViewGroup;)Lco/uk/getmondo/settings/OpenSourceLicensesActivity$HeaderViewHolder;

    move-result-object v0

    return-object v0
.end method

.method public a(Landroid/view/ViewGroup;I)Lco/uk/getmondo/settings/OpenSourceLicensesActivity$OpenSourceLibraryHolder;
    .locals 4

    .prologue
    .line 143
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 144
    new-instance v1, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$OpenSourceLibraryHolder;

    const v2, 0x7f050106

    const/4 v3, 0x0

    invoke-virtual {v0, v2, p1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    const/4 v2, 0x0

    invoke-direct {v1, v0, v2}, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$OpenSourceLibraryHolder;-><init>(Landroid/view/View;Lco/uk/getmondo/settings/OpenSourceLicensesActivity$1;)V

    return-object v1
.end method

.method public bridge synthetic a(Landroid/support/v7/widget/RecyclerView$w;I)V
    .locals 0

    .prologue
    .line 82
    check-cast p1, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$HeaderViewHolder;

    invoke-virtual {p0, p1, p2}, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$d;->a(Lco/uk/getmondo/settings/OpenSourceLicensesActivity$HeaderViewHolder;I)V

    return-void
.end method

.method public a(Lco/uk/getmondo/settings/OpenSourceLicensesActivity$HeaderViewHolder;I)V
    .locals 1

    .prologue
    .line 169
    iget-object v0, p0, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$d;->a:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$c;

    invoke-virtual {p1, v0}, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$HeaderViewHolder;->a(Lco/uk/getmondo/settings/OpenSourceLicensesActivity$c;)V

    .line 170
    return-void
.end method

.method public a(Lco/uk/getmondo/settings/OpenSourceLicensesActivity$OpenSourceLibraryHolder;I)V
    .locals 1

    .prologue
    .line 149
    iget-object v0, p0, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$d;->a:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$c;

    invoke-virtual {p1, v0}, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$OpenSourceLibraryHolder;->a(Lco/uk/getmondo/settings/OpenSourceLicensesActivity$c;)V

    .line 150
    return-void
.end method

.method public b(Landroid/view/ViewGroup;)Lco/uk/getmondo/settings/OpenSourceLicensesActivity$HeaderViewHolder;
    .locals 4

    .prologue
    .line 164
    new-instance v0, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$HeaderViewHolder;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f050103

    const/4 v3, 0x0

    invoke-virtual {v1, v2, p1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    invoke-direct {v0, v1}, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$HeaderViewHolder;-><init>(Landroid/view/View;)V

    return-object v0
.end method

.method public getItemCount()I
    .locals 1

    .prologue
    .line 154
    iget-object v0, p0, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$d;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public synthetic onBindViewHolder(Landroid/support/v7/widget/RecyclerView$w;I)V
    .locals 0

    .prologue
    .line 82
    check-cast p1, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$OpenSourceLibraryHolder;

    invoke-virtual {p0, p1, p2}, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$d;->a(Lco/uk/getmondo/settings/OpenSourceLicensesActivity$OpenSourceLibraryHolder;I)V

    return-void
.end method

.method public synthetic onCreateViewHolder(Landroid/view/ViewGroup;I)Landroid/support/v7/widget/RecyclerView$w;
    .locals 1

    .prologue
    .line 82
    invoke-virtual {p0, p1, p2}, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$d;->a(Landroid/view/ViewGroup;I)Lco/uk/getmondo/settings/OpenSourceLicensesActivity$OpenSourceLibraryHolder;

    move-result-object v0

    return-object v0
.end method
