.class final Lco/uk/getmondo/settings/u$al;
.super Ljava/lang/Object;
.source "SettingsPresenter.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lco/uk/getmondo/settings/u;->a(Lco/uk/getmondo/settings/u$a;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h",
        "<TT;",
        "Lio/reactivex/z",
        "<+TR;>;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000\u0016\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u0002\u0010\u0000\u001a&\u0012\u000c\u0012\n \u0003*\u0004\u0018\u00010\u00020\u0002 \u0003*\u0012\u0012\u000c\u0012\n \u0003*\u0004\u0018\u00010\u00020\u0002\u0018\u00010\u00010\u00012\u0006\u0010\u0004\u001a\u00020\u0005H\n\u00a2\u0006\u0004\u0008\u0006\u0010\u0007"
    }
    d2 = {
        "<anonymous>",
        "Lio/reactivex/Single;",
        "",
        "kotlin.jvm.PlatformType",
        "state",
        "",
        "apply",
        "(Ljava/lang/Boolean;)Lio/reactivex/Single;"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field final synthetic a:Lco/uk/getmondo/settings/u;

.field final synthetic b:Lco/uk/getmondo/settings/u$a;


# direct methods
.method constructor <init>(Lco/uk/getmondo/settings/u;Lco/uk/getmondo/settings/u$a;)V
    .locals 0

    iput-object p1, p0, Lco/uk/getmondo/settings/u$al;->a:Lco/uk/getmondo/settings/u;

    iput-object p2, p0, Lco/uk/getmondo/settings/u$al;->b:Lco/uk/getmondo/settings/u$a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Boolean;)Lio/reactivex/v;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Boolean;",
            ")",
            "Lio/reactivex/v",
            "<",
            "Lkotlin/n;",
            ">;"
        }
    .end annotation

    .prologue
    const-string v0, "state"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 190
    iget-object v0, p0, Lco/uk/getmondo/settings/u$al;->a:Lco/uk/getmondo/settings/u;

    invoke-static {v0}, Lco/uk/getmondo/settings/u;->j(Lco/uk/getmondo/settings/u;)Lco/uk/getmondo/payments/send/data/h;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {v0, v1}, Lco/uk/getmondo/payments/send/data/h;->a(Z)Lio/reactivex/v;

    move-result-object v1

    .line 191
    sget-object v0, Lco/uk/getmondo/settings/u$al$1;->a:Lco/uk/getmondo/settings/u$al$1;

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {v1, v0}, Lio/reactivex/v;->d(Lio/reactivex/c/h;)Lio/reactivex/v;

    move-result-object v0

    .line 192
    iget-object v1, p0, Lco/uk/getmondo/settings/u$al;->a:Lco/uk/getmondo/settings/u;

    invoke-static {v1}, Lco/uk/getmondo/settings/u;->f(Lco/uk/getmondo/settings/u;)Lio/reactivex/u;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/v;->b(Lio/reactivex/u;)Lio/reactivex/v;

    move-result-object v0

    .line 193
    iget-object v1, p0, Lco/uk/getmondo/settings/u$al;->a:Lco/uk/getmondo/settings/u;

    invoke-static {v1}, Lco/uk/getmondo/settings/u;->g(Lco/uk/getmondo/settings/u;)Lio/reactivex/u;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/v;->a(Lio/reactivex/u;)Lio/reactivex/v;

    move-result-object v1

    .line 194
    new-instance v0, Lco/uk/getmondo/settings/u$al$2;

    invoke-direct {v0, p0}, Lco/uk/getmondo/settings/u$al$2;-><init>(Lco/uk/getmondo/settings/u$al;)V

    check-cast v0, Lio/reactivex/c/g;

    invoke-virtual {v1, v0}, Lio/reactivex/v;->d(Lio/reactivex/c/g;)Lio/reactivex/v;

    move-result-object v0

    .line 195
    sget-object v1, Lkotlin/n;->a:Lkotlin/n;

    invoke-static {v1}, Lio/reactivex/v;->a(Ljava/lang/Object;)Lio/reactivex/v;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/v;->a(Lio/reactivex/v;)Lio/reactivex/v;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 36
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Lco/uk/getmondo/settings/u$al;->a(Ljava/lang/Boolean;)Lio/reactivex/v;

    move-result-object v0

    return-object v0
.end method
