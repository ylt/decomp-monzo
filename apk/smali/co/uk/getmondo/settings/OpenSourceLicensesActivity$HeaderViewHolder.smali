.class Lco/uk/getmondo/settings/OpenSourceLicensesActivity$HeaderViewHolder;
.super Landroid/support/v7/widget/RecyclerView$w;
.source "OpenSourceLicensesActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lco/uk/getmondo/settings/OpenSourceLicensesActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "HeaderViewHolder"
.end annotation


# instance fields
.field titleTextView:Landroid/widget/TextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f11041f
    .end annotation
.end field


# direct methods
.method constructor <init>(Landroid/view/View;)V
    .locals 0

    .prologue
    .line 193
    invoke-direct {p0, p1}, Landroid/support/v7/widget/RecyclerView$w;-><init>(Landroid/view/View;)V

    .line 195
    invoke-static {p0, p1}, Lbutterknife/ButterKnife;->bind(Ljava/lang/Object;Landroid/view/View;)Lbutterknife/Unbinder;

    .line 196
    return-void
.end method


# virtual methods
.method public a(Lco/uk/getmondo/settings/OpenSourceLicensesActivity$c;)V
    .locals 2

    .prologue
    .line 199
    iget-object v0, p0, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$HeaderViewHolder;->titleTextView:Landroid/widget/TextView;

    invoke-static {p1}, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$c;->a(Lco/uk/getmondo/settings/OpenSourceLicensesActivity$c;)Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;

    move-result-object v1

    invoke-static {v1}, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;->b(Lco/uk/getmondo/settings/OpenSourceLicensesActivity$a;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 200
    iget-object v0, p0, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$HeaderViewHolder;->titleTextView:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 201
    return-void
.end method
