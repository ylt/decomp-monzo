.class Lco/uk/getmondo/settings/OpenSourceLicensesActivity$OpenSourceLibraryHolder;
.super Landroid/support/v7/widget/RecyclerView$w;
.source "OpenSourceLicensesActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lco/uk/getmondo/settings/OpenSourceLicensesActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "OpenSourceLibraryHolder"
.end annotation


# instance fields
.field libraryLicenseTextView:Landroid/widget/TextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f110425
    .end annotation
.end field

.field libraryNameTextView:Landroid/widget/TextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f110424
    .end annotation
.end field


# direct methods
.method private constructor <init>(Landroid/view/View;)V
    .locals 0

    .prologue
    .line 178
    invoke-direct {p0, p1}, Landroid/support/v7/widget/RecyclerView$w;-><init>(Landroid/view/View;)V

    .line 180
    invoke-static {p0, p1}, Lbutterknife/ButterKnife;->bind(Ljava/lang/Object;Landroid/view/View;)Lbutterknife/Unbinder;

    .line 181
    return-void
.end method

.method synthetic constructor <init>(Landroid/view/View;Lco/uk/getmondo/settings/OpenSourceLicensesActivity$1;)V
    .locals 0

    .prologue
    .line 173
    invoke-direct {p0, p1}, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$OpenSourceLibraryHolder;-><init>(Landroid/view/View;)V

    return-void
.end method


# virtual methods
.method public a(Lco/uk/getmondo/settings/OpenSourceLicensesActivity$c;)V
    .locals 2

    .prologue
    .line 184
    iget-object v0, p0, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$OpenSourceLibraryHolder;->libraryNameTextView:Landroid/widget/TextView;

    invoke-static {p1}, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$c;->b(Lco/uk/getmondo/settings/OpenSourceLicensesActivity$c;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 185
    iget-object v0, p0, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$OpenSourceLibraryHolder;->libraryLicenseTextView:Landroid/widget/TextView;

    invoke-static {p1}, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$c;->c(Lco/uk/getmondo/settings/OpenSourceLicensesActivity$c;)Lco/uk/getmondo/settings/OpenSourceLicensesActivity$b;

    move-result-object v1

    invoke-static {v1}, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$b;->a(Lco/uk/getmondo/settings/OpenSourceLicensesActivity$b;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 186
    return-void
.end method
