.class public final Lco/uk/getmondo/settings/u;
.super Lco/uk/getmondo/common/ui/b;
.source "SettingsPresenter.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lco/uk/getmondo/settings/u$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lco/uk/getmondo/common/ui/b",
        "<",
        "Lco/uk/getmondo/settings/u$a;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000l\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\u0007\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00020\u0001:\u0001$Bk\u0008\u0007\u0012\u0008\u0008\u0001\u0010\u0003\u001a\u00020\u0004\u0012\u0008\u0008\u0001\u0010\u0005\u001a\u00020\u0004\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b\u0012\u0006\u0010\u000c\u001a\u00020\r\u0012\u0006\u0010\u000e\u001a\u00020\u000f\u0012\u0006\u0010\u0010\u001a\u00020\u0011\u0012\u0006\u0010\u0012\u001a\u00020\u0013\u0012\u0006\u0010\u0014\u001a\u00020\u0015\u0012\u0006\u0010\u0016\u001a\u00020\u0017\u0012\u0006\u0010\u0018\u001a\u00020\u0019\u00a2\u0006\u0002\u0010\u001aJ\u0010\u0010\u001e\u001a\u00020\u001f2\u0006\u0010 \u001a\u00020\u0002H\u0016J\u0018\u0010!\u001a\u00020\u001f2\u0006\u0010 \u001a\u00020\u00022\u0006\u0010\"\u001a\u00020#H\u0002R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u000fX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0018\u001a\u00020\u0019X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0014\u001a\u00020\u0015X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001b\u001a\u00020\u001cX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000c\u001a\u00020\rX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001d\u001a\u00020\u001cX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0012\u001a\u00020\u0013X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0016\u001a\u00020\u0017X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0010\u001a\u00020\u0011X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006%"
    }
    d2 = {
        "Lco/uk/getmondo/settings/SettingsPresenter;",
        "Lco/uk/getmondo/common/ui/BasePresenter;",
        "Lco/uk/getmondo/settings/SettingsPresenter$View;",
        "uiScheduler",
        "Lio/reactivex/Scheduler;",
        "ioScheduler",
        "accountService",
        "Lco/uk/getmondo/common/accounts/AccountService;",
        "analyticsService",
        "Lco/uk/getmondo/common/AnalyticsService;",
        "userSettingsRepository",
        "Lco/uk/getmondo/payments/send/data/UserSettingsRepository;",
        "monzoApi",
        "Lco/uk/getmondo/api/MonzoApi;",
        "apiErrorHandler",
        "Lco/uk/getmondo/common/errors/ApiErrorHandler;",
        "userSettingsStorage",
        "Lco/uk/getmondo/payments/send/data/UserSettingsStorage;",
        "userAccessTokenManager",
        "Lco/uk/getmondo/common/accounts/UserAccessTokenManager;",
        "localUserSettingStorage",
        "Lco/uk/getmondo/settings/LocalUserSettingStorage;",
        "userInteractor",
        "Lco/uk/getmondo/api/interactors/UserInteractor;",
        "featureFlagsStorage",
        "Lco/uk/getmondo/common/FeatureFlagsStorage;",
        "(Lio/reactivex/Scheduler;Lio/reactivex/Scheduler;Lco/uk/getmondo/common/accounts/AccountService;Lco/uk/getmondo/common/AnalyticsService;Lco/uk/getmondo/payments/send/data/UserSettingsRepository;Lco/uk/getmondo/api/MonzoApi;Lco/uk/getmondo/common/errors/ApiErrorHandler;Lco/uk/getmondo/payments/send/data/UserSettingsStorage;Lco/uk/getmondo/common/accounts/UserAccessTokenManager;Lco/uk/getmondo/settings/LocalUserSettingStorage;Lco/uk/getmondo/api/interactors/UserInteractor;Lco/uk/getmondo/common/FeatureFlagsStorage;)V",
        "magStripeInitialised",
        "",
        "paymentsInitialised",
        "register",
        "",
        "view",
        "updateMagStripeDescription",
        "accountSettings",
        "Lco/uk/getmondo/api/model/ApiAccountSettings;",
        "View",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field private c:Z

.field private d:Z

.field private final e:Lio/reactivex/u;

.field private final f:Lio/reactivex/u;

.field private final g:Lco/uk/getmondo/common/accounts/d;

.field private final h:Lco/uk/getmondo/common/a;

.field private final i:Lco/uk/getmondo/payments/send/data/h;

.field private final j:Lco/uk/getmondo/api/MonzoApi;

.field private final k:Lco/uk/getmondo/common/e/a;

.field private final l:Lco/uk/getmondo/payments/send/data/p;

.field private final m:Lco/uk/getmondo/common/accounts/o;

.field private final n:Lco/uk/getmondo/settings/k;

.field private final o:Lco/uk/getmondo/api/b/a;

.field private final p:Lco/uk/getmondo/common/o;


# direct methods
.method public constructor <init>(Lio/reactivex/u;Lio/reactivex/u;Lco/uk/getmondo/common/accounts/d;Lco/uk/getmondo/common/a;Lco/uk/getmondo/payments/send/data/h;Lco/uk/getmondo/api/MonzoApi;Lco/uk/getmondo/common/e/a;Lco/uk/getmondo/payments/send/data/p;Lco/uk/getmondo/common/accounts/o;Lco/uk/getmondo/settings/k;Lco/uk/getmondo/api/b/a;Lco/uk/getmondo/common/o;)V
    .locals 1

    .prologue
    const-string v0, "uiScheduler"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "ioScheduler"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "accountService"

    invoke-static {p3, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "analyticsService"

    invoke-static {p4, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "userSettingsRepository"

    invoke-static {p5, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "monzoApi"

    invoke-static {p6, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "apiErrorHandler"

    invoke-static {p7, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "userSettingsStorage"

    invoke-static {p8, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "userAccessTokenManager"

    invoke-static {p9, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "localUserSettingStorage"

    invoke-static {p10, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "userInteractor"

    invoke-static {p11, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "featureFlagsStorage"

    invoke-static {p12, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 50
    invoke-direct {p0}, Lco/uk/getmondo/common/ui/b;-><init>()V

    iput-object p1, p0, Lco/uk/getmondo/settings/u;->e:Lio/reactivex/u;

    iput-object p2, p0, Lco/uk/getmondo/settings/u;->f:Lio/reactivex/u;

    iput-object p3, p0, Lco/uk/getmondo/settings/u;->g:Lco/uk/getmondo/common/accounts/d;

    iput-object p4, p0, Lco/uk/getmondo/settings/u;->h:Lco/uk/getmondo/common/a;

    iput-object p5, p0, Lco/uk/getmondo/settings/u;->i:Lco/uk/getmondo/payments/send/data/h;

    iput-object p6, p0, Lco/uk/getmondo/settings/u;->j:Lco/uk/getmondo/api/MonzoApi;

    iput-object p7, p0, Lco/uk/getmondo/settings/u;->k:Lco/uk/getmondo/common/e/a;

    iput-object p8, p0, Lco/uk/getmondo/settings/u;->l:Lco/uk/getmondo/payments/send/data/p;

    iput-object p9, p0, Lco/uk/getmondo/settings/u;->m:Lco/uk/getmondo/common/accounts/o;

    iput-object p10, p0, Lco/uk/getmondo/settings/u;->n:Lco/uk/getmondo/settings/k;

    iput-object p11, p0, Lco/uk/getmondo/settings/u;->o:Lco/uk/getmondo/api/b/a;

    iput-object p12, p0, Lco/uk/getmondo/settings/u;->p:Lco/uk/getmondo/common/o;

    return-void
.end method

.method public static final synthetic a(Lco/uk/getmondo/settings/u;)Lco/uk/getmondo/common/a;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lco/uk/getmondo/settings/u;->h:Lco/uk/getmondo/common/a;

    return-object v0
.end method

.method private final a(Lco/uk/getmondo/settings/u$a;Lco/uk/getmondo/api/model/ApiAccountSettings;)V
    .locals 8

    .prologue
    .line 228
    invoke-virtual {p2}, Lco/uk/getmondo/api/model/ApiAccountSettings;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 229
    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {p2}, Lco/uk/getmondo/api/model/ApiAccountSettings;->b()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toHours(J)J

    move-result-wide v0

    long-to-int v0, v0

    .line 230
    sget-object v1, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {p2}, Lco/uk/getmondo/api/model/ApiAccountSettings;->b()J

    move-result-wide v2

    sget-object v4, Ljava/util/concurrent/TimeUnit;->HOURS:Ljava/util/concurrent/TimeUnit;

    int-to-long v6, v0

    invoke-virtual {v4, v6, v7}, Ljava/util/concurrent/TimeUnit;->toSeconds(J)J

    move-result-wide v4

    sub-long/2addr v2, v4

    invoke-virtual {v1, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMinutes(J)J

    move-result-wide v2

    long-to-int v1, v2

    .line 231
    invoke-interface {p1, v0, v1}, Lco/uk/getmondo/settings/u$a;->a(II)V

    .line 234
    :goto_0
    return-void

    .line 233
    :cond_0
    invoke-interface {p1}, Lco/uk/getmondo/settings/u$a;->L()V

    goto :goto_0
.end method

.method public static final synthetic a(Lco/uk/getmondo/settings/u;Lco/uk/getmondo/settings/u$a;Lco/uk/getmondo/api/model/ApiAccountSettings;)V
    .locals 0

    .prologue
    .line 36
    invoke-direct {p0, p1, p2}, Lco/uk/getmondo/settings/u;->a(Lco/uk/getmondo/settings/u$a;Lco/uk/getmondo/api/model/ApiAccountSettings;)V

    return-void
.end method

.method public static final synthetic a(Lco/uk/getmondo/settings/u;Z)V
    .locals 0

    .prologue
    .line 36
    iput-boolean p1, p0, Lco/uk/getmondo/settings/u;->c:Z

    return-void
.end method

.method public static final synthetic b(Lco/uk/getmondo/settings/u;)Lco/uk/getmondo/settings/k;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lco/uk/getmondo/settings/u;->n:Lco/uk/getmondo/settings/k;

    return-object v0
.end method

.method public static final synthetic b(Lco/uk/getmondo/settings/u;Z)V
    .locals 0

    .prologue
    .line 36
    iput-boolean p1, p0, Lco/uk/getmondo/settings/u;->d:Z

    return-void
.end method

.method public static final synthetic c(Lco/uk/getmondo/settings/u;)Z
    .locals 1

    .prologue
    .line 36
    iget-boolean v0, p0, Lco/uk/getmondo/settings/u;->c:Z

    return v0
.end method

.method public static final synthetic d(Lco/uk/getmondo/settings/u;)Lco/uk/getmondo/common/e/a;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lco/uk/getmondo/settings/u;->k:Lco/uk/getmondo/common/e/a;

    return-object v0
.end method

.method public static final synthetic e(Lco/uk/getmondo/settings/u;)Lco/uk/getmondo/api/MonzoApi;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lco/uk/getmondo/settings/u;->j:Lco/uk/getmondo/api/MonzoApi;

    return-object v0
.end method

.method public static final synthetic f(Lco/uk/getmondo/settings/u;)Lio/reactivex/u;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lco/uk/getmondo/settings/u;->f:Lio/reactivex/u;

    return-object v0
.end method

.method public static final synthetic g(Lco/uk/getmondo/settings/u;)Lio/reactivex/u;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lco/uk/getmondo/settings/u;->e:Lio/reactivex/u;

    return-object v0
.end method

.method public static final synthetic h(Lco/uk/getmondo/settings/u;)Z
    .locals 1

    .prologue
    .line 36
    iget-boolean v0, p0, Lco/uk/getmondo/settings/u;->d:Z

    return v0
.end method

.method public static final synthetic i(Lco/uk/getmondo/settings/u;)Lco/uk/getmondo/payments/send/data/p;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lco/uk/getmondo/settings/u;->l:Lco/uk/getmondo/payments/send/data/p;

    return-object v0
.end method

.method public static final synthetic j(Lco/uk/getmondo/settings/u;)Lco/uk/getmondo/payments/send/data/h;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lco/uk/getmondo/settings/u;->i:Lco/uk/getmondo/payments/send/data/h;

    return-object v0
.end method

.method public static final synthetic k(Lco/uk/getmondo/settings/u;)Lco/uk/getmondo/common/accounts/o;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lco/uk/getmondo/settings/u;->m:Lco/uk/getmondo/common/accounts/o;

    return-object v0
.end method

.method public static final synthetic l(Lco/uk/getmondo/settings/u;)Lco/uk/getmondo/api/b/a;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lco/uk/getmondo/settings/u;->o:Lco/uk/getmondo/api/b/a;

    return-object v0
.end method

.method public static final synthetic m(Lco/uk/getmondo/settings/u;)Lco/uk/getmondo/common/accounts/d;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lco/uk/getmondo/settings/u;->g:Lco/uk/getmondo/common/accounts/d;

    return-object v0
.end method


# virtual methods
.method public bridge synthetic a(Lco/uk/getmondo/common/ui/f;)V
    .locals 0

    .prologue
    .line 36
    check-cast p1, Lco/uk/getmondo/settings/u$a;

    invoke-virtual {p0, p1}, Lco/uk/getmondo/settings/u;->a(Lco/uk/getmondo/settings/u$a;)V

    return-void
.end method

.method public a(Lco/uk/getmondo/settings/u$a;)V
    .locals 7

    .prologue
    const-string v0, "view"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v0, p1

    .line 57
    check-cast v0, Lco/uk/getmondo/common/ui/f;

    invoke-super {p0, v0}, Lco/uk/getmondo/common/ui/b;->a(Lco/uk/getmondo/common/ui/f;)V

    .line 59
    iget-object v0, p0, Lco/uk/getmondo/settings/u;->g:Lco/uk/getmondo/common/accounts/d;

    invoke-virtual {v0}, Lco/uk/getmondo/common/accounts/d;->b()Lco/uk/getmondo/d/ak;

    move-result-object v3

    .line 60
    if-nez v3, :cond_0

    invoke-static {}, Lkotlin/d/b/l;->a()V

    :cond_0
    invoke-virtual {v3}, Lco/uk/getmondo/d/ak;->c()Lco/uk/getmondo/d/a;

    move-result-object v2

    .line 61
    iget-object v0, p0, Lco/uk/getmondo/settings/u;->g:Lco/uk/getmondo/common/accounts/d;

    invoke-virtual {v0}, Lco/uk/getmondo/common/accounts/d;->b()Lco/uk/getmondo/d/ak;

    move-result-object v0

    if-nez v0, :cond_1

    invoke-static {}, Lkotlin/d/b/l;->a()V

    :cond_1
    invoke-virtual {v0}, Lco/uk/getmondo/d/ak;->d()Lco/uk/getmondo/d/ac;

    move-result-object v4

    if-nez v4, :cond_2

    invoke-static {}, Lkotlin/d/b/l;->a()V

    .line 62
    :cond_2
    if-nez v2, :cond_3

    invoke-static {}, Lkotlin/d/b/l;->a()V

    :cond_3
    invoke-interface {v2}, Lco/uk/getmondo/d/a;->b()Lorg/threeten/bp/LocalDateTime;

    move-result-object v0

    invoke-interface {p1, v4, v0}, Lco/uk/getmondo/settings/u$a;->a(Lco/uk/getmondo/d/ac;Lorg/threeten/bp/LocalDateTime;)V

    .line 63
    iget-object v0, p0, Lco/uk/getmondo/settings/u;->h:Lco/uk/getmondo/common/a;

    sget-object v1, Lco/uk/getmondo/api/model/tracking/Impression;->Companion:Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    invoke-virtual {v1}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->ap()Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v1

    invoke-virtual {v0, v1}, Lco/uk/getmondo/common/a;->a(Lco/uk/getmondo/api/model/tracking/Impression;)V

    .line 65
    iget-object v5, p0, Lco/uk/getmondo/settings/u;->b:Lio/reactivex/b/a;

    .line 67
    invoke-interface {p1}, Lco/uk/getmondo/settings/u$a;->b()Lio/reactivex/n;

    move-result-object v1

    .line 66
    new-instance v0, Lco/uk/getmondo/settings/u$b;

    invoke-direct {v0, p0}, Lco/uk/getmondo/settings/u$b;-><init>(Lco/uk/getmondo/settings/u;)V

    check-cast v0, Lio/reactivex/c/g;

    invoke-virtual {v1, v0}, Lio/reactivex/n;->doOnNext(Lio/reactivex/c/g;)Lio/reactivex/n;

    move-result-object v6

    .line 67
    new-instance v0, Lco/uk/getmondo/settings/u$m;

    invoke-direct {v0, p1}, Lco/uk/getmondo/settings/u$m;-><init>(Lco/uk/getmondo/settings/u$a;)V

    check-cast v0, Lio/reactivex/c/g;

    sget-object v1, Lco/uk/getmondo/settings/u$x;->a:Lco/uk/getmondo/settings/u$x;

    check-cast v1, Lio/reactivex/c/g;

    invoke-virtual {v6, v0, v1}, Lio/reactivex/n;->subscribe(Lio/reactivex/c/g;Lio/reactivex/c/g;)Lio/reactivex/b/b;

    move-result-object v0

    const-string v1, "view.onNameClicked()\n   \u2026on() }, { Timber.e(it) })"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 65
    invoke-static {v5, v0}, Lco/uk/getmondo/common/j/f;->a(Lio/reactivex/b/a;Lio/reactivex/b/b;)Lio/reactivex/b/a;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/settings/u;->b:Lio/reactivex/b/a;

    .line 69
    iget-object v5, p0, Lco/uk/getmondo/settings/u;->b:Lio/reactivex/b/a;

    .line 71
    invoke-interface {p1}, Lco/uk/getmondo/settings/u$a;->c()Lio/reactivex/n;

    move-result-object v1

    .line 70
    new-instance v0, Lco/uk/getmondo/settings/u$ai;

    invoke-direct {v0, p0}, Lco/uk/getmondo/settings/u$ai;-><init>(Lco/uk/getmondo/settings/u;)V

    check-cast v0, Lio/reactivex/c/g;

    invoke-virtual {v1, v0}, Lio/reactivex/n;->doOnNext(Lio/reactivex/c/g;)Lio/reactivex/n;

    move-result-object v6

    .line 71
    new-instance v0, Lco/uk/getmondo/settings/u$at;

    invoke-direct {v0, v3, p1}, Lco/uk/getmondo/settings/u$at;-><init>(Lco/uk/getmondo/d/ak;Lco/uk/getmondo/settings/u$a;)V

    check-cast v0, Lio/reactivex/c/g;

    .line 77
    sget-object v1, Lco/uk/getmondo/settings/u$bd;->a:Lco/uk/getmondo/settings/u$bd;

    check-cast v1, Lkotlin/d/a/b;

    if-eqz v1, :cond_4

    new-instance v3, Lco/uk/getmondo/settings/v;

    invoke-direct {v3, v1}, Lco/uk/getmondo/settings/v;-><init>(Lkotlin/d/a/b;)V

    move-object v1, v3

    :cond_4
    check-cast v1, Lio/reactivex/c/g;

    .line 71
    invoke-virtual {v6, v0, v1}, Lio/reactivex/n;->subscribe(Lio/reactivex/c/g;Lio/reactivex/c/g;)Lio/reactivex/b/b;

    move-result-object v0

    const-string v1, "view.onAddressClicked()\n\u2026            }, Timber::e)"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 69
    invoke-static {v5, v0}, Lco/uk/getmondo/common/j/f;->a(Lio/reactivex/b/a;Lio/reactivex/b/b;)Lio/reactivex/b/a;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/settings/u;->b:Lio/reactivex/b/a;

    .line 79
    iget-object v1, p0, Lco/uk/getmondo/settings/u;->b:Lio/reactivex/b/a;

    .line 81
    iget-object v0, p0, Lco/uk/getmondo/settings/u;->n:Lco/uk/getmondo/settings/k;

    invoke-virtual {v0}, Lco/uk/getmondo/settings/k;->b()Lio/reactivex/n;

    move-result-object v0

    .line 80
    iget-object v3, p0, Lco/uk/getmondo/settings/u;->n:Lco/uk/getmondo/settings/k;

    invoke-virtual {v3}, Lco/uk/getmondo/settings/k;->a()Z

    move-result v3

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v0, v3}, Lio/reactivex/n;->startWith(Ljava/lang/Object;)Lio/reactivex/n;

    move-result-object v3

    .line 81
    new-instance v0, Lco/uk/getmondo/settings/u$be;

    invoke-direct {v0, p1}, Lco/uk/getmondo/settings/u$be;-><init>(Lco/uk/getmondo/settings/u$a;)V

    check-cast v0, Lio/reactivex/c/g;

    invoke-virtual {v3, v0}, Lio/reactivex/n;->subscribe(Lio/reactivex/c/g;)Lio/reactivex/b/b;

    move-result-object v0

    const-string v3, "localUserSettingStorage.\u2026      }\n                }"

    invoke-static {v0, v3}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 79
    invoke-static {v1, v0}, Lco/uk/getmondo/common/j/f;->a(Lio/reactivex/b/a;Lio/reactivex/b/b;)Lio/reactivex/b/a;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/settings/u;->b:Lio/reactivex/b/a;

    .line 89
    iget-object v3, p0, Lco/uk/getmondo/settings/u;->b:Lio/reactivex/b/a;

    .line 92
    invoke-interface {p1}, Lco/uk/getmondo/settings/u$a;->w()Lio/reactivex/n;

    move-result-object v1

    .line 90
    new-instance v0, Lco/uk/getmondo/settings/u$bf;

    invoke-direct {v0, p0}, Lco/uk/getmondo/settings/u$bf;-><init>(Lco/uk/getmondo/settings/u;)V

    check-cast v0, Lio/reactivex/c/g;

    invoke-virtual {v1, v0}, Lio/reactivex/n;->doOnNext(Lio/reactivex/c/g;)Lio/reactivex/n;

    move-result-object v0

    .line 91
    iget-object v1, p0, Lco/uk/getmondo/settings/u;->e:Lio/reactivex/u;

    invoke-virtual {v0, v1}, Lio/reactivex/n;->observeOn(Lio/reactivex/u;)Lio/reactivex/n;

    move-result-object v5

    .line 92
    new-instance v0, Lco/uk/getmondo/settings/u$bg;

    invoke-direct {v0, p0}, Lco/uk/getmondo/settings/u$bg;-><init>(Lco/uk/getmondo/settings/u;)V

    check-cast v0, Lio/reactivex/c/g;

    sget-object v1, Lco/uk/getmondo/settings/u$c;->a:Lco/uk/getmondo/settings/u$c;

    check-cast v1, Lio/reactivex/c/g;

    invoke-virtual {v5, v0, v1}, Lio/reactivex/n;->subscribe(Lio/reactivex/c/g;Lio/reactivex/c/g;)Lio/reactivex/b/b;

    move-result-object v0

    const-string v1, "view.onNotificationsStat\u2026(it) }, { Timber.e(it) })"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 89
    invoke-static {v3, v0}, Lco/uk/getmondo/common/j/f;->a(Lio/reactivex/b/a;Lio/reactivex/b/b;)Lio/reactivex/b/a;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/settings/u;->b:Lio/reactivex/b/a;

    .line 94
    iget-object v3, p0, Lco/uk/getmondo/settings/u;->b:Lio/reactivex/b/a;

    .line 96
    invoke-interface {p1}, Lco/uk/getmondo/settings/u$a;->d()Lio/reactivex/n;

    move-result-object v1

    .line 95
    new-instance v0, Lco/uk/getmondo/settings/u$d;

    invoke-direct {v0, p0}, Lco/uk/getmondo/settings/u$d;-><init>(Lco/uk/getmondo/settings/u;)V

    check-cast v0, Lio/reactivex/c/g;

    invoke-virtual {v1, v0}, Lio/reactivex/n;->doOnNext(Lio/reactivex/c/g;)Lio/reactivex/n;

    move-result-object v5

    .line 96
    new-instance v0, Lco/uk/getmondo/settings/u$e;

    invoke-direct {v0, p1}, Lco/uk/getmondo/settings/u$e;-><init>(Lco/uk/getmondo/settings/u$a;)V

    check-cast v0, Lio/reactivex/c/g;

    sget-object v1, Lco/uk/getmondo/settings/u$f;->a:Lco/uk/getmondo/settings/u$f;

    check-cast v1, Lio/reactivex/c/g;

    invoke-virtual {v5, v0, v1}, Lio/reactivex/n;->subscribe(Lio/reactivex/c/g;Lio/reactivex/c/g;)Lio/reactivex/b/b;

    move-result-object v0

    const-string v1, "view.onAboutMonzoClicked\u2026zo() }, { Timber.e(it) })"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 94
    invoke-static {v3, v0}, Lco/uk/getmondo/common/j/f;->a(Lio/reactivex/b/a;Lio/reactivex/b/b;)Lio/reactivex/b/a;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/settings/u;->b:Lio/reactivex/b/a;

    .line 98
    iget-object v3, p0, Lco/uk/getmondo/settings/u;->b:Lio/reactivex/b/a;

    .line 100
    invoke-interface {p1}, Lco/uk/getmondo/settings/u$a;->e()Lio/reactivex/n;

    move-result-object v1

    .line 99
    new-instance v0, Lco/uk/getmondo/settings/u$g;

    invoke-direct {v0, p0}, Lco/uk/getmondo/settings/u$g;-><init>(Lco/uk/getmondo/settings/u;)V

    check-cast v0, Lio/reactivex/c/g;

    invoke-virtual {v1, v0}, Lio/reactivex/n;->doOnNext(Lio/reactivex/c/g;)Lio/reactivex/n;

    move-result-object v5

    .line 100
    new-instance v0, Lco/uk/getmondo/settings/u$h;

    invoke-direct {v0, p1}, Lco/uk/getmondo/settings/u$h;-><init>(Lco/uk/getmondo/settings/u$a;)V

    check-cast v0, Lio/reactivex/c/g;

    sget-object v1, Lco/uk/getmondo/settings/u$i;->a:Lco/uk/getmondo/settings/u$i;

    check-cast v1, Lio/reactivex/c/g;

    invoke-virtual {v5, v0, v1}, Lio/reactivex/n;->subscribe(Lio/reactivex/c/g;Lio/reactivex/c/g;)Lio/reactivex/b/b;

    move-result-object v0

    const-string v1, "view.onTermsAndCondition\u2026ns() }, { Timber.e(it) })"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 98
    invoke-static {v3, v0}, Lco/uk/getmondo/common/j/f;->a(Lio/reactivex/b/a;Lio/reactivex/b/b;)Lio/reactivex/b/a;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/settings/u;->b:Lio/reactivex/b/a;

    .line 102
    iget-object v3, p0, Lco/uk/getmondo/settings/u;->b:Lio/reactivex/b/a;

    .line 104
    invoke-interface {p1}, Lco/uk/getmondo/settings/u$a;->f()Lio/reactivex/n;

    move-result-object v1

    .line 103
    new-instance v0, Lco/uk/getmondo/settings/u$j;

    invoke-direct {v0, p0}, Lco/uk/getmondo/settings/u$j;-><init>(Lco/uk/getmondo/settings/u;)V

    check-cast v0, Lio/reactivex/c/g;

    invoke-virtual {v1, v0}, Lio/reactivex/n;->doOnNext(Lio/reactivex/c/g;)Lio/reactivex/n;

    move-result-object v5

    .line 104
    new-instance v0, Lco/uk/getmondo/settings/u$k;

    invoke-direct {v0, p1}, Lco/uk/getmondo/settings/u$k;-><init>(Lco/uk/getmondo/settings/u$a;)V

    check-cast v0, Lio/reactivex/c/g;

    sget-object v1, Lco/uk/getmondo/settings/u$l;->a:Lco/uk/getmondo/settings/u$l;

    check-cast v1, Lio/reactivex/c/g;

    invoke-virtual {v5, v0, v1}, Lio/reactivex/n;->subscribe(Lio/reactivex/c/g;Lio/reactivex/c/g;)Lio/reactivex/b/b;

    move-result-object v0

    const-string v1, "view.onPrivacyPolicyClic\u2026cy() }, { Timber.e(it) })"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 102
    invoke-static {v3, v0}, Lco/uk/getmondo/common/j/f;->a(Lio/reactivex/b/a;Lio/reactivex/b/b;)Lio/reactivex/b/a;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/settings/u;->b:Lio/reactivex/b/a;

    .line 106
    iget-object v3, p0, Lco/uk/getmondo/settings/u;->b:Lio/reactivex/b/a;

    .line 108
    invoke-interface {p1}, Lco/uk/getmondo/settings/u$a;->g()Lio/reactivex/n;

    move-result-object v1

    .line 107
    new-instance v0, Lco/uk/getmondo/settings/u$n;

    invoke-direct {v0, p0}, Lco/uk/getmondo/settings/u$n;-><init>(Lco/uk/getmondo/settings/u;)V

    check-cast v0, Lio/reactivex/c/g;

    invoke-virtual {v1, v0}, Lio/reactivex/n;->doOnNext(Lio/reactivex/c/g;)Lio/reactivex/n;

    move-result-object v5

    .line 108
    new-instance v0, Lco/uk/getmondo/settings/u$o;

    invoke-direct {v0, p1}, Lco/uk/getmondo/settings/u$o;-><init>(Lco/uk/getmondo/settings/u$a;)V

    check-cast v0, Lio/reactivex/c/g;

    sget-object v1, Lco/uk/getmondo/settings/u$p;->a:Lco/uk/getmondo/settings/u$p;

    check-cast v1, Lio/reactivex/c/g;

    invoke-virtual {v5, v0, v1}, Lio/reactivex/n;->subscribe(Lio/reactivex/c/g;Lio/reactivex/c/g;)Lio/reactivex/b/b;

    move-result-object v0

    const-string v1, "view.onFscsProtectionCli\u2026on() }, { Timber.e(it) })"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 106
    invoke-static {v3, v0}, Lco/uk/getmondo/common/j/f;->a(Lio/reactivex/b/a;Lio/reactivex/b/b;)Lio/reactivex/b/a;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/settings/u;->b:Lio/reactivex/b/a;

    .line 111
    invoke-interface {p1}, Lco/uk/getmondo/settings/u$a;->L()V

    .line 112
    iget-object v3, p0, Lco/uk/getmondo/settings/u;->b:Lio/reactivex/b/a;

    .line 115
    iget-object v0, p0, Lco/uk/getmondo/settings/u;->j:Lco/uk/getmondo/api/MonzoApi;

    invoke-interface {v2}, Lco/uk/getmondo/d/a;->a()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lco/uk/getmondo/api/MonzoApi;->accountSettings(Ljava/lang/String;)Lio/reactivex/v;

    move-result-object v0

    .line 113
    iget-object v1, p0, Lco/uk/getmondo/settings/u;->f:Lio/reactivex/u;

    invoke-virtual {v0, v1}, Lio/reactivex/v;->b(Lio/reactivex/u;)Lio/reactivex/v;

    move-result-object v0

    .line 114
    iget-object v1, p0, Lco/uk/getmondo/settings/u;->e:Lio/reactivex/u;

    invoke-virtual {v0, v1}, Lio/reactivex/v;->a(Lio/reactivex/u;)Lio/reactivex/v;

    move-result-object v5

    .line 115
    new-instance v0, Lco/uk/getmondo/settings/u$q;

    invoke-direct {v0, p0, p1}, Lco/uk/getmondo/settings/u$q;-><init>(Lco/uk/getmondo/settings/u;Lco/uk/getmondo/settings/u$a;)V

    check-cast v0, Lio/reactivex/c/g;

    .line 118
    new-instance v1, Lco/uk/getmondo/settings/u$r;

    invoke-direct {v1, p0, p1}, Lco/uk/getmondo/settings/u$r;-><init>(Lco/uk/getmondo/settings/u;Lco/uk/getmondo/settings/u$a;)V

    check-cast v1, Lio/reactivex/c/g;

    .line 115
    invoke-virtual {v5, v0, v1}, Lio/reactivex/v;->a(Lio/reactivex/c/g;Lio/reactivex/c/g;)Lio/reactivex/b/b;

    move-result-object v0

    const-string v1, "monzoApi.accountSettings\u2026 view)\n                })"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 112
    invoke-static {v3, v0}, Lco/uk/getmondo/common/j/f;->a(Lio/reactivex/b/a;Lio/reactivex/b/b;)Lio/reactivex/b/a;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/settings/u;->b:Lio/reactivex/b/a;

    .line 125
    iget-object v3, p0, Lco/uk/getmondo/settings/u;->b:Lio/reactivex/b/a;

    .line 139
    invoke-interface {p1}, Lco/uk/getmondo/settings/u$a;->x()Lio/reactivex/n;

    move-result-object v1

    .line 126
    new-instance v0, Lco/uk/getmondo/settings/u$s;

    invoke-direct {v0, p0}, Lco/uk/getmondo/settings/u$s;-><init>(Lco/uk/getmondo/settings/u;)V

    check-cast v0, Lio/reactivex/c/q;

    invoke-virtual {v1, v0}, Lio/reactivex/n;->filter(Lio/reactivex/c/q;)Lio/reactivex/n;

    move-result-object v1

    .line 127
    new-instance v0, Lco/uk/getmondo/settings/u$t;

    invoke-direct {v0, p0}, Lco/uk/getmondo/settings/u$t;-><init>(Lco/uk/getmondo/settings/u;)V

    check-cast v0, Lio/reactivex/c/g;

    invoke-virtual {v1, v0}, Lio/reactivex/n;->doOnNext(Lio/reactivex/c/g;)Lio/reactivex/n;

    move-result-object v1

    .line 128
    new-instance v0, Lco/uk/getmondo/settings/u$u;

    invoke-direct {v0, p1}, Lco/uk/getmondo/settings/u$u;-><init>(Lco/uk/getmondo/settings/u$a;)V

    check-cast v0, Lio/reactivex/c/g;

    invoke-virtual {v1, v0}, Lio/reactivex/n;->doOnNext(Lio/reactivex/c/g;)Lio/reactivex/n;

    move-result-object v1

    .line 129
    new-instance v0, Lco/uk/getmondo/settings/u$v;

    invoke-direct {v0, p0, v2, p1}, Lco/uk/getmondo/settings/u$v;-><init>(Lco/uk/getmondo/settings/u;Lco/uk/getmondo/d/a;Lco/uk/getmondo/settings/u$a;)V

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {v1, v0}, Lio/reactivex/n;->flatMapMaybe(Lio/reactivex/c/h;)Lio/reactivex/n;

    move-result-object v5

    .line 139
    new-instance v0, Lco/uk/getmondo/settings/u$w;

    invoke-direct {v0, p0, p1}, Lco/uk/getmondo/settings/u$w;-><init>(Lco/uk/getmondo/settings/u;Lco/uk/getmondo/settings/u$a;)V

    check-cast v0, Lio/reactivex/c/g;

    .line 142
    sget-object v1, Lco/uk/getmondo/settings/u$y;->a:Lco/uk/getmondo/settings/u$y;

    check-cast v1, Lio/reactivex/c/g;

    .line 139
    invoke-virtual {v5, v0, v1}, Lio/reactivex/n;->subscribe(Lio/reactivex/c/g;Lio/reactivex/c/g;)Lio/reactivex/b/b;

    move-result-object v0

    const-string v1, "view.onMagStripeStateTog\u2026     }, { Timber.e(it) })"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 125
    invoke-static {v3, v0}, Lco/uk/getmondo/common/j/f;->a(Lio/reactivex/b/a;Lio/reactivex/b/b;)Lio/reactivex/b/a;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/settings/u;->b:Lio/reactivex/b/a;

    .line 144
    invoke-interface {v2}, Lco/uk/getmondo/d/a;->d()Lco/uk/getmondo/d/a$b;

    move-result-object v0

    sget-object v1, Lco/uk/getmondo/d/a$b;->PREPAID:Lco/uk/getmondo/d/a$b;

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 145
    invoke-interface {p1}, Lco/uk/getmondo/settings/u$a;->X()V

    .line 164
    :goto_0
    iget-object v2, p0, Lco/uk/getmondo/settings/u;->b:Lio/reactivex/b/a;

    invoke-interface {p1}, Lco/uk/getmondo/settings/u$a;->h()Lio/reactivex/n;

    move-result-object v3

    new-instance v0, Lco/uk/getmondo/settings/u$ab;

    invoke-direct {v0, p1}, Lco/uk/getmondo/settings/u$ab;-><init>(Lco/uk/getmondo/settings/u$a;)V

    check-cast v0, Lio/reactivex/c/g;

    sget-object v1, Lco/uk/getmondo/settings/u$ac;->a:Lco/uk/getmondo/settings/u$ac;

    check-cast v1, Lio/reactivex/c/g;

    invoke-virtual {v3, v0, v1}, Lio/reactivex/n;->subscribe(Lio/reactivex/c/g;Lio/reactivex/c/g;)Lio/reactivex/b/b;

    move-result-object v0

    const-string v1, "view.onLimitsClicked().s\u2026ts() }, { Timber.e(it) })"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v2, v0}, Lco/uk/getmondo/common/j/f;->a(Lio/reactivex/b/a;Lio/reactivex/b/b;)Lio/reactivex/b/a;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/settings/u;->b:Lio/reactivex/b/a;

    .line 168
    iget-object v1, p0, Lco/uk/getmondo/settings/u;->b:Lio/reactivex/b/a;

    .line 170
    iget-object v0, p0, Lco/uk/getmondo/settings/u;->i:Lco/uk/getmondo/payments/send/data/h;

    invoke-virtual {v0}, Lco/uk/getmondo/payments/send/data/h;->b()Lio/reactivex/n;

    move-result-object v2

    .line 169
    sget-object v0, Lco/uk/getmondo/settings/u$ad;->a:Lco/uk/getmondo/settings/u$ad;

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {v2, v0}, Lio/reactivex/n;->map(Lio/reactivex/c/h;)Lio/reactivex/n;

    move-result-object v2

    .line 170
    new-instance v0, Lco/uk/getmondo/settings/u$ae;

    invoke-direct {v0, p0, p1}, Lco/uk/getmondo/settings/u$ae;-><init>(Lco/uk/getmondo/settings/u;Lco/uk/getmondo/settings/u$a;)V

    check-cast v0, Lio/reactivex/c/g;

    invoke-virtual {v2, v0}, Lio/reactivex/n;->subscribe(Lio/reactivex/c/g;)Lio/reactivex/b/b;

    move-result-object v0

    const-string v2, "userSettingsRepository.o\u2026 = true\n                }"

    invoke-static {v0, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 168
    invoke-static {v1, v0}, Lco/uk/getmondo/common/j/f;->a(Lio/reactivex/b/a;Lio/reactivex/b/b;)Lio/reactivex/b/a;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/settings/u;->b:Lio/reactivex/b/a;

    .line 179
    iget-object v2, p0, Lco/uk/getmondo/settings/u;->b:Lio/reactivex/b/a;

    .line 197
    invoke-interface {p1}, Lco/uk/getmondo/settings/u$a;->y()Lio/reactivex/n;

    move-result-object v1

    .line 180
    new-instance v0, Lco/uk/getmondo/settings/u$af;

    invoke-direct {v0, p0}, Lco/uk/getmondo/settings/u$af;-><init>(Lco/uk/getmondo/settings/u;)V

    check-cast v0, Lio/reactivex/c/q;

    invoke-virtual {v1, v0}, Lio/reactivex/n;->filter(Lio/reactivex/c/q;)Lio/reactivex/n;

    move-result-object v1

    .line 181
    new-instance v0, Lco/uk/getmondo/settings/u$ag;

    invoke-direct {v0, p0}, Lco/uk/getmondo/settings/u$ag;-><init>(Lco/uk/getmondo/settings/u;)V

    check-cast v0, Lio/reactivex/c/g;

    invoke-virtual {v1, v0}, Lio/reactivex/n;->doOnNext(Lio/reactivex/c/g;)Lio/reactivex/n;

    move-result-object v1

    .line 182
    new-instance v0, Lco/uk/getmondo/settings/u$ah;

    invoke-direct {v0, p0, p1}, Lco/uk/getmondo/settings/u$ah;-><init>(Lco/uk/getmondo/settings/u;Lco/uk/getmondo/settings/u$a;)V

    check-cast v0, Lio/reactivex/c/g;

    invoke-virtual {v1, v0}, Lio/reactivex/n;->doOnNext(Lio/reactivex/c/g;)Lio/reactivex/n;

    move-result-object v1

    .line 187
    new-instance v0, Lco/uk/getmondo/settings/u$aj;

    invoke-direct {v0, p0}, Lco/uk/getmondo/settings/u$aj;-><init>(Lco/uk/getmondo/settings/u;)V

    check-cast v0, Lio/reactivex/c/q;

    invoke-virtual {v1, v0}, Lio/reactivex/n;->filter(Lio/reactivex/c/q;)Lio/reactivex/n;

    move-result-object v1

    .line 188
    new-instance v0, Lco/uk/getmondo/settings/u$ak;

    invoke-direct {v0, p1}, Lco/uk/getmondo/settings/u$ak;-><init>(Lco/uk/getmondo/settings/u$a;)V

    check-cast v0, Lio/reactivex/c/g;

    invoke-virtual {v1, v0}, Lio/reactivex/n;->doOnNext(Lio/reactivex/c/g;)Lio/reactivex/n;

    move-result-object v1

    .line 189
    new-instance v0, Lco/uk/getmondo/settings/u$al;

    invoke-direct {v0, p0, p1}, Lco/uk/getmondo/settings/u$al;-><init>(Lco/uk/getmondo/settings/u;Lco/uk/getmondo/settings/u$a;)V

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {v1, v0}, Lio/reactivex/n;->switchMapSingle(Lio/reactivex/c/h;)Lio/reactivex/n;

    move-result-object v3

    .line 197
    new-instance v0, Lco/uk/getmondo/settings/u$am;

    invoke-direct {v0, p1}, Lco/uk/getmondo/settings/u$am;-><init>(Lco/uk/getmondo/settings/u$a;)V

    check-cast v0, Lio/reactivex/c/g;

    sget-object v1, Lco/uk/getmondo/settings/u$an;->a:Lco/uk/getmondo/settings/u$an;

    check-cast v1, Lio/reactivex/c/g;

    invoke-virtual {v3, v0, v1}, Lio/reactivex/n;->subscribe(Lio/reactivex/c/g;Lio/reactivex/c/g;)Lio/reactivex/b/b;

    move-result-object v0

    const-string v1, "view.onPaymentsStateTogg\u2026ng() }, { Timber.e(it) })"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 179
    invoke-static {v2, v0}, Lco/uk/getmondo/common/j/f;->a(Lio/reactivex/b/a;Lio/reactivex/b/b;)Lio/reactivex/b/a;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/settings/u;->b:Lio/reactivex/b/a;

    .line 200
    iget-object v2, p0, Lco/uk/getmondo/settings/u;->b:Lio/reactivex/b/a;

    .line 202
    invoke-interface {p1}, Lco/uk/getmondo/settings/u$a;->i()Lio/reactivex/n;

    move-result-object v1

    .line 201
    new-instance v0, Lco/uk/getmondo/settings/u$ao;

    invoke-direct {v0, p0}, Lco/uk/getmondo/settings/u$ao;-><init>(Lco/uk/getmondo/settings/u;)V

    check-cast v0, Lio/reactivex/c/g;

    invoke-virtual {v1, v0}, Lio/reactivex/n;->doOnNext(Lio/reactivex/c/g;)Lio/reactivex/n;

    move-result-object v3

    .line 202
    new-instance v0, Lco/uk/getmondo/settings/u$ap;

    invoke-direct {v0, p1}, Lco/uk/getmondo/settings/u$ap;-><init>(Lco/uk/getmondo/settings/u$a;)V

    check-cast v0, Lio/reactivex/c/g;

    sget-object v1, Lco/uk/getmondo/settings/u$aq;->a:Lco/uk/getmondo/settings/u$aq;

    check-cast v1, Lio/reactivex/c/g;

    invoke-virtual {v3, v0, v1}, Lio/reactivex/n;->subscribe(Lio/reactivex/c/g;Lio/reactivex/c/g;)Lio/reactivex/b/b;

    move-result-object v0

    const-string v1, "view.onOpenSourceLicense\u2026es() }, { Timber.e(it) })"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 200
    invoke-static {v2, v0}, Lco/uk/getmondo/common/j/f;->a(Lio/reactivex/b/a;Lio/reactivex/b/b;)Lio/reactivex/b/a;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/settings/u;->b:Lio/reactivex/b/a;

    .line 204
    iget-object v2, p0, Lco/uk/getmondo/settings/u;->b:Lio/reactivex/b/a;

    .line 205
    invoke-interface {p1}, Lco/uk/getmondo/settings/u$a;->j()Lio/reactivex/n;

    move-result-object v3

    new-instance v0, Lco/uk/getmondo/settings/u$ar;

    invoke-direct {v0, p1}, Lco/uk/getmondo/settings/u$ar;-><init>(Lco/uk/getmondo/settings/u$a;)V

    check-cast v0, Lio/reactivex/c/g;

    sget-object v1, Lco/uk/getmondo/settings/u$as;->a:Lco/uk/getmondo/settings/u$as;

    check-cast v1, Lio/reactivex/c/g;

    invoke-virtual {v3, v0, v1}, Lio/reactivex/n;->subscribe(Lio/reactivex/c/g;Lio/reactivex/c/g;)Lio/reactivex/b/b;

    move-result-object v0

    const-string v1, "view.onLogOutClicked()\n \u2026on() }, { Timber.e(it) })"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 204
    invoke-static {v2, v0}, Lco/uk/getmondo/common/j/f;->a(Lio/reactivex/b/a;Lio/reactivex/b/b;)Lio/reactivex/b/a;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/settings/u;->b:Lio/reactivex/b/a;

    .line 207
    iget-object v2, p0, Lco/uk/getmondo/settings/u;->b:Lio/reactivex/b/a;

    .line 219
    invoke-interface {p1}, Lco/uk/getmondo/settings/u$a;->k()Lio/reactivex/n;

    move-result-object v1

    .line 208
    new-instance v0, Lco/uk/getmondo/settings/u$au;

    invoke-direct {v0, p0}, Lco/uk/getmondo/settings/u$au;-><init>(Lco/uk/getmondo/settings/u;)V

    check-cast v0, Lio/reactivex/c/g;

    invoke-virtual {v1, v0}, Lio/reactivex/n;->doOnNext(Lio/reactivex/c/g;)Lio/reactivex/n;

    move-result-object v1

    .line 210
    new-instance v0, Lco/uk/getmondo/settings/u$av;

    invoke-direct {v0, p0}, Lco/uk/getmondo/settings/u$av;-><init>(Lco/uk/getmondo/settings/u;)V

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {v1, v0}, Lio/reactivex/n;->flatMapMaybe(Lio/reactivex/c/h;)Lio/reactivex/n;

    move-result-object v1

    .line 211
    new-instance v0, Lco/uk/getmondo/settings/u$aw;

    invoke-direct {v0, p0}, Lco/uk/getmondo/settings/u$aw;-><init>(Lco/uk/getmondo/settings/u;)V

    check-cast v0, Lio/reactivex/c/g;

    invoke-virtual {v1, v0}, Lio/reactivex/n;->doOnNext(Lio/reactivex/c/g;)Lio/reactivex/n;

    move-result-object v1

    .line 218
    new-instance v0, Lco/uk/getmondo/settings/u$ax;

    invoke-direct {v0, p0}, Lco/uk/getmondo/settings/u$ax;-><init>(Lco/uk/getmondo/settings/u;)V

    check-cast v0, Lio/reactivex/c/g;

    invoke-virtual {v1, v0}, Lio/reactivex/n;->doOnNext(Lio/reactivex/c/g;)Lio/reactivex/n;

    move-result-object v3

    .line 219
    new-instance v0, Lco/uk/getmondo/settings/u$ay;

    invoke-direct {v0, p1}, Lco/uk/getmondo/settings/u$ay;-><init>(Lco/uk/getmondo/settings/u$a;)V

    check-cast v0, Lio/reactivex/c/g;

    sget-object v1, Lco/uk/getmondo/settings/u$az;->a:Lco/uk/getmondo/settings/u$az;

    check-cast v1, Lio/reactivex/c/g;

    invoke-virtual {v3, v0, v1}, Lio/reactivex/n;->subscribe(Lio/reactivex/c/g;Lio/reactivex/c/g;)Lio/reactivex/b/b;

    move-result-object v0

    const-string v1, "view.onConfirmLogOutClic\u2026sh() }, { Timber.e(it) })"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 207
    invoke-static {v2, v0}, Lco/uk/getmondo/common/j/f;->a(Lio/reactivex/b/a;Lio/reactivex/b/b;)Lio/reactivex/b/a;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/settings/u;->b:Lio/reactivex/b/a;

    .line 221
    iget-object v2, p0, Lco/uk/getmondo/settings/u;->b:Lio/reactivex/b/a;

    .line 223
    invoke-interface {p1}, Lco/uk/getmondo/settings/u$a;->v()Lio/reactivex/n;

    move-result-object v1

    .line 222
    new-instance v0, Lco/uk/getmondo/settings/u$ba;

    invoke-direct {v0, p0}, Lco/uk/getmondo/settings/u$ba;-><init>(Lco/uk/getmondo/settings/u;)V

    check-cast v0, Lio/reactivex/c/g;

    invoke-virtual {v1, v0}, Lio/reactivex/n;->doOnNext(Lio/reactivex/c/g;)Lio/reactivex/n;

    move-result-object v3

    .line 223
    new-instance v0, Lco/uk/getmondo/settings/u$bb;

    invoke-direct {v0, p1}, Lco/uk/getmondo/settings/u$bb;-><init>(Lco/uk/getmondo/settings/u$a;)V

    check-cast v0, Lio/reactivex/c/g;

    sget-object v1, Lco/uk/getmondo/settings/u$bc;->a:Lco/uk/getmondo/settings/u$bc;

    check-cast v1, Lio/reactivex/c/g;

    invoke-virtual {v3, v0, v1}, Lio/reactivex/n;->subscribe(Lio/reactivex/c/g;Lio/reactivex/c/g;)Lio/reactivex/b/b;

    move-result-object v0

    const-string v1, "view.onCloseAccountClick\u2026nt() }, { Timber.e(it) })"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 221
    invoke-static {v2, v0}, Lco/uk/getmondo/common/j/f;->a(Lio/reactivex/b/a;Lio/reactivex/b/b;)Lio/reactivex/b/a;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/settings/u;->b:Lio/reactivex/b/a;

    .line 225
    return-void

    .line 147
    :cond_5
    invoke-interface {p1}, Lco/uk/getmondo/settings/u$a;->Y()V

    move-object v0, v2

    .line 149
    check-cast v0, Lco/uk/getmondo/d/ad;

    invoke-virtual {v0}, Lco/uk/getmondo/d/ad;->j()Ljava/lang/String;

    move-result-object v1

    .line 150
    check-cast v2, Lco/uk/getmondo/d/ad;

    invoke-virtual {v2}, Lco/uk/getmondo/d/ad;->g()Ljava/lang/String;

    move-result-object v2

    .line 151
    invoke-interface {p1, v1, v2}, Lco/uk/getmondo/settings/u$a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 152
    iget-object v3, p0, Lco/uk/getmondo/settings/u;->b:Lio/reactivex/b/a;

    .line 153
    iget-object v0, p0, Lco/uk/getmondo/settings/u;->p:Lco/uk/getmondo/common/o;

    invoke-virtual {v0}, Lco/uk/getmondo/common/o;->a()Lio/reactivex/n;

    move-result-object v5

    new-instance v0, Lco/uk/getmondo/settings/u$z;

    invoke-direct {v0, p1}, Lco/uk/getmondo/settings/u$z;-><init>(Lco/uk/getmondo/settings/u$a;)V

    check-cast v0, Lio/reactivex/c/g;

    invoke-virtual {v5, v0}, Lio/reactivex/n;->subscribe(Lio/reactivex/c/g;)Lio/reactivex/b/b;

    move-result-object v0

    const-string v5, "featureFlagsStorage.feat\u2026  }\n                    }"

    invoke-static {v0, v5}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 152
    invoke-static {v3, v0}, Lco/uk/getmondo/common/j/f;->a(Lio/reactivex/b/a;Lio/reactivex/b/b;)Lio/reactivex/b/a;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/settings/u;->b:Lio/reactivex/b/a;

    .line 160
    iget-object v3, p0, Lco/uk/getmondo/settings/u;->b:Lio/reactivex/b/a;

    .line 161
    invoke-interface {p1}, Lco/uk/getmondo/settings/u$a;->z()Lio/reactivex/n;

    move-result-object v5

    new-instance v0, Lco/uk/getmondo/settings/u$aa;

    invoke-direct {v0, p1, v4, v1, v2}, Lco/uk/getmondo/settings/u$aa;-><init>(Lco/uk/getmondo/settings/u$a;Lco/uk/getmondo/d/ac;Ljava/lang/String;Ljava/lang/String;)V

    check-cast v0, Lio/reactivex/c/g;

    invoke-virtual {v5, v0}, Lio/reactivex/n;->subscribe(Lio/reactivex/c/g;)Lio/reactivex/b/b;

    move-result-object v0

    const-string v1, "view.onShareAccountDetai\u2026de)\n                    }"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 160
    invoke-static {v3, v0}, Lco/uk/getmondo/common/j/f;->a(Lio/reactivex/b/a;Lio/reactivex/b/b;)Lio/reactivex/b/a;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/settings/u;->b:Lio/reactivex/b/a;

    goto/16 :goto_0
.end method
