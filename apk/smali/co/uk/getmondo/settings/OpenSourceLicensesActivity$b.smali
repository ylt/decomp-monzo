.class final enum Lco/uk/getmondo/settings/OpenSourceLicensesActivity$b;
.super Ljava/lang/Enum;
.source "OpenSourceLicensesActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lco/uk/getmondo/settings/OpenSourceLicensesActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lco/uk/getmondo/settings/OpenSourceLicensesActivity$b;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lco/uk/getmondo/settings/OpenSourceLicensesActivity$b;

.field public static final enum b:Lco/uk/getmondo/settings/OpenSourceLicensesActivity$b;

.field public static final enum c:Lco/uk/getmondo/settings/OpenSourceLicensesActivity$b;

.field public static final enum d:Lco/uk/getmondo/settings/OpenSourceLicensesActivity$b;

.field private static final synthetic f:[Lco/uk/getmondo/settings/OpenSourceLicensesActivity$b;


# instance fields
.field private final e:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 205
    new-instance v0, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$b;

    const-string v1, "APACHE_2"

    const-string v2, "Apache 2.0"

    invoke-direct {v0, v1, v3, v2}, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$b;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$b;->a:Lco/uk/getmondo/settings/OpenSourceLicensesActivity$b;

    .line 206
    new-instance v0, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$b;

    const-string v1, "MIT"

    const-string v2, "MIT"

    invoke-direct {v0, v1, v4, v2}, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$b;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$b;->b:Lco/uk/getmondo/settings/OpenSourceLicensesActivity$b;

    .line 207
    new-instance v0, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$b;

    const-string v1, "FACEBOOK_PLATFORM_LICENSE"

    const-string v2, "Facebook Platform License"

    invoke-direct {v0, v1, v5, v2}, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$b;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$b;->c:Lco/uk/getmondo/settings/OpenSourceLicensesActivity$b;

    .line 208
    new-instance v0, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$b;

    const-string v1, "ECLIPSE_1"

    const-string v2, "Eclipse Public License 1.0"

    invoke-direct {v0, v1, v6, v2}, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$b;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$b;->d:Lco/uk/getmondo/settings/OpenSourceLicensesActivity$b;

    .line 204
    const/4 v0, 0x4

    new-array v0, v0, [Lco/uk/getmondo/settings/OpenSourceLicensesActivity$b;

    sget-object v1, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$b;->a:Lco/uk/getmondo/settings/OpenSourceLicensesActivity$b;

    aput-object v1, v0, v3

    sget-object v1, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$b;->b:Lco/uk/getmondo/settings/OpenSourceLicensesActivity$b;

    aput-object v1, v0, v4

    sget-object v1, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$b;->c:Lco/uk/getmondo/settings/OpenSourceLicensesActivity$b;

    aput-object v1, v0, v5

    sget-object v1, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$b;->d:Lco/uk/getmondo/settings/OpenSourceLicensesActivity$b;

    aput-object v1, v0, v6

    sput-object v0, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$b;->f:[Lco/uk/getmondo/settings/OpenSourceLicensesActivity$b;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 212
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 213
    iput-object p3, p0, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$b;->e:Ljava/lang/String;

    .line 214
    return-void
.end method

.method static synthetic a(Lco/uk/getmondo/settings/OpenSourceLicensesActivity$b;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 204
    iget-object v0, p0, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$b;->e:Ljava/lang/String;

    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)Lco/uk/getmondo/settings/OpenSourceLicensesActivity$b;
    .locals 1

    .prologue
    .line 204
    const-class v0, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$b;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$b;

    return-object v0
.end method

.method public static values()[Lco/uk/getmondo/settings/OpenSourceLicensesActivity$b;
    .locals 1

    .prologue
    .line 204
    sget-object v0, Lco/uk/getmondo/settings/OpenSourceLicensesActivity$b;->f:[Lco/uk/getmondo/settings/OpenSourceLicensesActivity$b;

    invoke-virtual {v0}, [Lco/uk/getmondo/settings/OpenSourceLicensesActivity$b;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lco/uk/getmondo/settings/OpenSourceLicensesActivity$b;

    return-object v0
.end method
