.class public Lco/uk/getmondo/settings/p;
.super Landroid/app/DialogFragment;
.source "LogOutConfirmationDialogFragment.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lco/uk/getmondo/settings/p$a;
    }
.end annotation


# instance fields
.field private a:Lco/uk/getmondo/settings/p$a;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    return-void
.end method

.method public static a()Lco/uk/getmondo/settings/p;
    .locals 1

    .prologue
    .line 16
    new-instance v0, Lco/uk/getmondo/settings/p;

    invoke-direct {v0}, Lco/uk/getmondo/settings/p;-><init>()V

    return-object v0
.end method

.method static synthetic a(Lco/uk/getmondo/settings/p;Landroid/content/DialogInterface;I)V
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lco/uk/getmondo/settings/p;->a:Lco/uk/getmondo/settings/p$a;

    invoke-interface {v0}, Lco/uk/getmondo/settings/p$a;->a()V

    return-void
.end method


# virtual methods
.method public a(Lco/uk/getmondo/settings/p$a;)V
    .locals 0

    .prologue
    .line 32
    iput-object p1, p0, Lco/uk/getmondo/settings/p;->a:Lco/uk/getmondo/settings/p$a;

    .line 33
    return-void
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 3

    .prologue
    .line 22
    invoke-virtual {p0}, Lco/uk/getmondo/settings/p;->getActivity()Landroid/app/Activity;

    move-result-object v0

    .line 23
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, v0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v0, 0x7f0a0374

    .line 24
    invoke-virtual {v1, v0}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0a0373

    .line 25
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0a015e

    invoke-static {p0}, Lco/uk/getmondo/settings/q;->a(Lco/uk/getmondo/settings/p;)Landroid/content/DialogInterface$OnClickListener;

    move-result-object v2

    .line 26
    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0a015c

    const/4 v2, 0x0

    .line 27
    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 28
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 23
    return-object v0
.end method
