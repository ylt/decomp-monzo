.class final Lco/uk/getmondo/settings/u$aw;
.super Ljava/lang/Object;
.source "SettingsPresenter.kt"

# interfaces
.implements Lio/reactivex/c/g;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lco/uk/getmondo/settings/u;->a(Lco/uk/getmondo/settings/u$a;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/g",
        "<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "",
        "accessToken",
        "",
        "kotlin.jvm.PlatformType",
        "accept"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field final synthetic a:Lco/uk/getmondo/settings/u;


# direct methods
.method constructor <init>(Lco/uk/getmondo/settings/u;)V
    .locals 0

    iput-object p1, p0, Lco/uk/getmondo/settings/u$aw;->a:Lco/uk/getmondo/settings/u;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 36
    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1}, Lco/uk/getmondo/settings/u$aw;->a(Ljava/lang/String;)V

    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 213
    sget-object v0, Lco/uk/getmondo/a;->b:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_0

    .line 215
    iget-object v0, p0, Lco/uk/getmondo/settings/u$aw;->a:Lco/uk/getmondo/settings/u;

    invoke-static {v0}, Lco/uk/getmondo/settings/u;->l(Lco/uk/getmondo/settings/u;)Lco/uk/getmondo/api/b/a;

    move-result-object v0

    invoke-virtual {v0, p1}, Lco/uk/getmondo/api/b/a;->b(Ljava/lang/String;)Lio/reactivex/b;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/settings/u$aw;->a:Lco/uk/getmondo/settings/u;

    invoke-static {v1}, Lco/uk/getmondo/settings/u;->f(Lco/uk/getmondo/settings/u;)Lio/reactivex/u;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/b;->b(Lio/reactivex/u;)Lio/reactivex/b;

    move-result-object v2

    sget-object v0, Lco/uk/getmondo/settings/u$aw$1;->a:Lco/uk/getmondo/settings/u$aw$1;

    check-cast v0, Lio/reactivex/c/a;

    sget-object v1, Lco/uk/getmondo/settings/u$aw$2;->a:Lco/uk/getmondo/settings/u$aw$2;

    check-cast v1, Lio/reactivex/c/g;

    invoke-virtual {v2, v0, v1}, Lio/reactivex/b;->a(Lio/reactivex/c/a;Lio/reactivex/c/g;)Lio/reactivex/b/b;

    .line 217
    :cond_0
    return-void
.end method
