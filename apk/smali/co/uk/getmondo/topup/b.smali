.class Lco/uk/getmondo/topup/b;
.super Ljava/lang/Object;
.source "AndroidPayDelegate.java"

# interfaces
.implements Lcom/google/android/gms/common/api/GoogleApiClient$OnConnectionFailedListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lco/uk/getmondo/topup/b$c;,
        Lco/uk/getmondo/topup/b$b;,
        Lco/uk/getmondo/topup/b$d;,
        Lco/uk/getmondo/topup/b$a;
    }
.end annotation


# instance fields
.field private final a:Lco/uk/getmondo/common/i/b;

.field private b:Lco/uk/getmondo/d/c;

.field private c:Lcom/google/android/gms/common/api/GoogleApiClient;

.field private d:Lco/uk/getmondo/topup/b$d;

.field private e:Lco/uk/getmondo/topup/b$b;

.field private f:Lco/uk/getmondo/topup/b$c;


# direct methods
.method constructor <init>()V
    .locals 4

    .prologue
    const/4 v2, 0x1

    .line 56
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    new-instance v0, Lco/uk/getmondo/common/i/b;

    const/4 v1, 0x0

    invoke-direct {v0, v2, v1, v2}, Lco/uk/getmondo/common/i/b;-><init>(ZZZ)V

    iput-object v0, p0, Lco/uk/getmondo/topup/b;->a:Lco/uk/getmondo/common/i/b;

    .line 48
    new-instance v0, Lco/uk/getmondo/d/c;

    const-wide/16 v2, 0x3e8

    sget-object v1, Lco/uk/getmondo/common/i/c;->a:Lco/uk/getmondo/common/i/c;

    invoke-direct {v0, v2, v3, v1}, Lco/uk/getmondo/d/c;-><init>(JLco/uk/getmondo/common/i/c;)V

    iput-object v0, p0, Lco/uk/getmondo/topup/b;->b:Lco/uk/getmondo/d/c;

    .line 57
    return-void
.end method

.method private a(Landroid/content/Intent;)I
    .locals 2

    .prologue
    const/4 v0, -0x1

    .line 203
    .line 204
    if-eqz p1, :cond_0

    .line 205
    const-string v1, "com.google.android.gms.wallet.EXTRA_ERROR_CODE"

    invoke-virtual {p1, v1, v0}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 207
    :cond_0
    return v0
.end method

.method private a(Lcom/google/android/gms/wallet/MaskedWallet;)Lcom/google/android/gms/wallet/FullWalletRequest;
    .locals 4

    .prologue
    .line 211
    iget-object v0, p0, Lco/uk/getmondo/topup/b;->a:Lco/uk/getmondo/common/i/b;

    iget-object v1, p0, Lco/uk/getmondo/topup/b;->b:Lco/uk/getmondo/d/c;

    invoke-virtual {v0, v1}, Lco/uk/getmondo/common/i/b;->a(Lco/uk/getmondo/d/c;)Ljava/lang/String;

    move-result-object v0

    .line 213
    invoke-static {}, Lcom/google/android/gms/wallet/LineItem;->newBuilder()Lcom/google/android/gms/wallet/LineItem$Builder;

    move-result-object v1

    iget-object v2, p0, Lco/uk/getmondo/topup/b;->b:Lco/uk/getmondo/d/c;

    .line 214
    invoke-virtual {v2}, Lco/uk/getmondo/d/c;->l()Lco/uk/getmondo/common/i/c;

    move-result-object v2

    invoke-virtual {v2}, Lco/uk/getmondo/common/i/c;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/gms/wallet/LineItem$Builder;->setCurrencyCode(Ljava/lang/String;)Lcom/google/android/gms/wallet/LineItem$Builder;

    move-result-object v1

    const-string v2, "1"

    .line 215
    invoke-virtual {v1, v2}, Lcom/google/android/gms/wallet/LineItem$Builder;->setQuantity(Ljava/lang/String;)Lcom/google/android/gms/wallet/LineItem$Builder;

    move-result-object v1

    .line 216
    invoke-virtual {v1, v0}, Lcom/google/android/gms/wallet/LineItem$Builder;->setTotalPrice(Ljava/lang/String;)Lcom/google/android/gms/wallet/LineItem$Builder;

    move-result-object v1

    .line 217
    invoke-virtual {v1}, Lcom/google/android/gms/wallet/LineItem$Builder;->build()Lcom/google/android/gms/wallet/LineItem;

    move-result-object v1

    .line 218
    invoke-static {}, Lcom/google/android/gms/wallet/Cart;->newBuilder()Lcom/google/android/gms/wallet/Cart$Builder;

    move-result-object v2

    iget-object v3, p0, Lco/uk/getmondo/topup/b;->b:Lco/uk/getmondo/d/c;

    .line 219
    invoke-virtual {v3}, Lco/uk/getmondo/d/c;->l()Lco/uk/getmondo/common/i/c;

    move-result-object v3

    invoke-virtual {v3}, Lco/uk/getmondo/common/i/c;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/android/gms/wallet/Cart$Builder;->setCurrencyCode(Ljava/lang/String;)Lcom/google/android/gms/wallet/Cart$Builder;

    move-result-object v2

    .line 220
    invoke-virtual {v2, v0}, Lcom/google/android/gms/wallet/Cart$Builder;->setTotalPrice(Ljava/lang/String;)Lcom/google/android/gms/wallet/Cart$Builder;

    move-result-object v0

    .line 221
    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/Cart$Builder;->addLineItem(Lcom/google/android/gms/wallet/LineItem;)Lcom/google/android/gms/wallet/Cart$Builder;

    move-result-object v0

    .line 222
    invoke-virtual {v0}, Lcom/google/android/gms/wallet/Cart$Builder;->build()Lcom/google/android/gms/wallet/Cart;

    move-result-object v0

    .line 224
    invoke-static {}, Lcom/google/android/gms/wallet/FullWalletRequest;->newBuilder()Lcom/google/android/gms/wallet/FullWalletRequest$Builder;

    move-result-object v1

    .line 225
    invoke-virtual {v1, v0}, Lcom/google/android/gms/wallet/FullWalletRequest$Builder;->setCart(Lcom/google/android/gms/wallet/Cart;)Lcom/google/android/gms/wallet/FullWalletRequest$Builder;

    move-result-object v0

    .line 226
    invoke-virtual {p1}, Lcom/google/android/gms/wallet/MaskedWallet;->getGoogleTransactionId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/FullWalletRequest$Builder;->setGoogleTransactionId(Ljava/lang/String;)Lcom/google/android/gms/wallet/FullWalletRequest$Builder;

    move-result-object v0

    .line 227
    invoke-virtual {v0}, Lcom/google/android/gms/wallet/FullWalletRequest$Builder;->build()Lcom/google/android/gms/wallet/FullWalletRequest;

    move-result-object v0

    .line 224
    return-object v0
.end method

.method private a()Lcom/google/android/gms/wallet/fragment/SupportWalletFragment;
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 111
    new-instance v0, Lcom/google/android/gms/wallet/fragment/WalletFragmentStyle;

    invoke-direct {v0}, Lcom/google/android/gms/wallet/fragment/WalletFragmentStyle;-><init>()V

    const/4 v1, 0x6

    .line 112
    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/fragment/WalletFragmentStyle;->setBuyButtonText(I)Lcom/google/android/gms/wallet/fragment/WalletFragmentStyle;

    move-result-object v0

    const/4 v1, 0x4

    .line 113
    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/fragment/WalletFragmentStyle;->setBuyButtonAppearance(I)Lcom/google/android/gms/wallet/fragment/WalletFragmentStyle;

    move-result-object v0

    const/4 v1, -0x1

    .line 114
    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/fragment/WalletFragmentStyle;->setBuyButtonWidth(I)Lcom/google/android/gms/wallet/fragment/WalletFragmentStyle;

    move-result-object v0

    .line 116
    invoke-static {}, Lcom/google/android/gms/wallet/fragment/WalletFragmentOptions;->newBuilder()Lcom/google/android/gms/wallet/fragment/WalletFragmentOptions$Builder;

    move-result-object v1

    .line 117
    invoke-virtual {v1, v2}, Lcom/google/android/gms/wallet/fragment/WalletFragmentOptions$Builder;->setEnvironment(I)Lcom/google/android/gms/wallet/fragment/WalletFragmentOptions$Builder;

    move-result-object v1

    .line 118
    invoke-virtual {v1, v0}, Lcom/google/android/gms/wallet/fragment/WalletFragmentOptions$Builder;->setFragmentStyle(Lcom/google/android/gms/wallet/fragment/WalletFragmentStyle;)Lcom/google/android/gms/wallet/fragment/WalletFragmentOptions$Builder;

    move-result-object v0

    .line 119
    invoke-virtual {v0, v2}, Lcom/google/android/gms/wallet/fragment/WalletFragmentOptions$Builder;->setTheme(I)Lcom/google/android/gms/wallet/fragment/WalletFragmentOptions$Builder;

    move-result-object v0

    .line 120
    invoke-virtual {v0, v2}, Lcom/google/android/gms/wallet/fragment/WalletFragmentOptions$Builder;->setMode(I)Lcom/google/android/gms/wallet/fragment/WalletFragmentOptions$Builder;

    move-result-object v0

    .line 121
    invoke-virtual {v0}, Lcom/google/android/gms/wallet/fragment/WalletFragmentOptions$Builder;->build()Lcom/google/android/gms/wallet/fragment/WalletFragmentOptions;

    move-result-object v0

    .line 123
    invoke-static {v0}, Lcom/google/android/gms/wallet/fragment/SupportWalletFragment;->newInstance(Lcom/google/android/gms/wallet/fragment/WalletFragmentOptions;)Lcom/google/android/gms/wallet/fragment/SupportWalletFragment;

    move-result-object v0

    .line 124
    invoke-direct {p0}, Lco/uk/getmondo/topup/b;->b()Lcom/google/android/gms/wallet/fragment/WalletFragmentInitParams;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/fragment/SupportWalletFragment;->initialize(Lcom/google/android/gms/wallet/fragment/WalletFragmentInitParams;)V

    .line 125
    return-object v0
.end method

.method static synthetic a(Lco/uk/getmondo/topup/b;Lco/uk/getmondo/topup/b$a;Lcom/google/android/gms/common/api/BooleanResult;)V
    .locals 5

    .prologue
    .line 93
    invoke-virtual {p2}, Lcom/google/android/gms/common/api/BooleanResult;->getStatus()Lcom/google/android/gms/common/api/Status;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/Status;->isSuccess()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 94
    invoke-virtual {p2}, Lcom/google/android/gms/common/api/BooleanResult;->getValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 95
    invoke-direct {p0}, Lco/uk/getmondo/topup/b;->a()Lcom/google/android/gms/wallet/fragment/SupportWalletFragment;

    move-result-object v0

    invoke-interface {p1, v0}, Lco/uk/getmondo/topup/b$a;->a(Lcom/google/android/gms/wallet/fragment/SupportWalletFragment;)V

    .line 106
    :goto_0
    return-void

    .line 98
    :cond_0
    invoke-interface {p1}, Lco/uk/getmondo/topup/b$a;->a()V

    goto :goto_0

    .line 101
    :cond_1
    invoke-virtual {p2}, Lcom/google/android/gms/common/api/BooleanResult;->getStatus()Lcom/google/android/gms/common/api/Status;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/Status;->getStatusMessage()Ljava/lang/String;

    move-result-object v0

    .line 102
    invoke-virtual {p2}, Lcom/google/android/gms/common/api/BooleanResult;->getStatus()Lcom/google/android/gms/common/api/Status;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/common/api/Status;->getStatusCode()I

    move-result v1

    .line 103
    new-instance v2, Ljava/lang/RuntimeException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "AndroidPayDelegate, failed to determine if user ready to pay, message: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " errorCode: "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    invoke-static {v2}, Ld/a/a;->a(Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method private b()Lcom/google/android/gms/wallet/fragment/WalletFragmentInitParams;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 129
    invoke-static {}, Lcom/google/android/gms/wallet/PaymentMethodTokenizationParameters;->newBuilder()Lcom/google/android/gms/wallet/PaymentMethodTokenizationParameters$Builder;

    move-result-object v0

    const/4 v1, 0x1

    .line 130
    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/PaymentMethodTokenizationParameters$Builder;->setPaymentMethodTokenizationType(I)Lcom/google/android/gms/wallet/PaymentMethodTokenizationParameters$Builder;

    move-result-object v0

    const-string v1, "gateway"

    const-string v2, "stripe"

    .line 131
    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/wallet/PaymentMethodTokenizationParameters$Builder;->addParameter(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/wallet/PaymentMethodTokenizationParameters$Builder;

    move-result-object v0

    const-string v1, "stripe:publishableKey"

    const-string v2, "pk_live_LW3b2bi1iXHicd88NnwlCkaA"

    .line 132
    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/wallet/PaymentMethodTokenizationParameters$Builder;->addParameter(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/wallet/PaymentMethodTokenizationParameters$Builder;

    move-result-object v0

    const-string v1, "stripe:version"

    const-string v2, "4.1.3"

    .line 133
    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/wallet/PaymentMethodTokenizationParameters$Builder;->addParameter(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/wallet/PaymentMethodTokenizationParameters$Builder;

    move-result-object v0

    .line 134
    invoke-virtual {v0}, Lcom/google/android/gms/wallet/PaymentMethodTokenizationParameters$Builder;->build()Lcom/google/android/gms/wallet/PaymentMethodTokenizationParameters;

    move-result-object v0

    .line 136
    invoke-static {}, Lcom/google/android/gms/wallet/MaskedWalletRequest;->newBuilder()Lcom/google/android/gms/wallet/MaskedWalletRequest$Builder;

    move-result-object v1

    const-string v2, "Monzo"

    .line 137
    invoke-virtual {v1, v2}, Lcom/google/android/gms/wallet/MaskedWalletRequest$Builder;->setMerchantName(Ljava/lang/String;)Lcom/google/android/gms/wallet/MaskedWalletRequest$Builder;

    move-result-object v1

    .line 138
    invoke-virtual {v1, v3}, Lcom/google/android/gms/wallet/MaskedWalletRequest$Builder;->setPhoneNumberRequired(Z)Lcom/google/android/gms/wallet/MaskedWalletRequest$Builder;

    move-result-object v1

    .line 139
    invoke-virtual {v1, v3}, Lcom/google/android/gms/wallet/MaskedWalletRequest$Builder;->setShippingAddressRequired(Z)Lcom/google/android/gms/wallet/MaskedWalletRequest$Builder;

    move-result-object v1

    iget-object v2, p0, Lco/uk/getmondo/topup/b;->b:Lco/uk/getmondo/d/c;

    .line 140
    invoke-virtual {v2}, Lco/uk/getmondo/d/c;->l()Lco/uk/getmondo/common/i/c;

    move-result-object v2

    invoke-virtual {v2}, Lco/uk/getmondo/common/i/c;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/gms/wallet/MaskedWalletRequest$Builder;->setCurrencyCode(Ljava/lang/String;)Lcom/google/android/gms/wallet/MaskedWalletRequest$Builder;

    move-result-object v1

    iget-object v2, p0, Lco/uk/getmondo/topup/b;->a:Lco/uk/getmondo/common/i/b;

    iget-object v3, p0, Lco/uk/getmondo/topup/b;->b:Lco/uk/getmondo/d/c;

    .line 141
    invoke-virtual {v2, v3}, Lco/uk/getmondo/common/i/b;->a(Lco/uk/getmondo/d/c;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/gms/wallet/MaskedWalletRequest$Builder;->setEstimatedTotalPrice(Ljava/lang/String;)Lcom/google/android/gms/wallet/MaskedWalletRequest$Builder;

    move-result-object v1

    .line 142
    invoke-virtual {v1, v0}, Lcom/google/android/gms/wallet/MaskedWalletRequest$Builder;->setPaymentMethodTokenizationParameters(Lcom/google/android/gms/wallet/PaymentMethodTokenizationParameters;)Lcom/google/android/gms/wallet/MaskedWalletRequest$Builder;

    move-result-object v0

    .line 143
    invoke-virtual {v0}, Lcom/google/android/gms/wallet/MaskedWalletRequest$Builder;->build()Lcom/google/android/gms/wallet/MaskedWalletRequest;

    move-result-object v0

    .line 145
    invoke-static {}, Lcom/google/android/gms/wallet/fragment/WalletFragmentInitParams;->newBuilder()Lcom/google/android/gms/wallet/fragment/WalletFragmentInitParams$Builder;

    move-result-object v1

    .line 146
    invoke-virtual {v1, v0}, Lcom/google/android/gms/wallet/fragment/WalletFragmentInitParams$Builder;->setMaskedWalletRequest(Lcom/google/android/gms/wallet/MaskedWalletRequest;)Lcom/google/android/gms/wallet/fragment/WalletFragmentInitParams$Builder;

    move-result-object v0

    const/16 v1, 0x3e9

    .line 147
    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/fragment/WalletFragmentInitParams$Builder;->setMaskedWalletRequestCode(I)Lcom/google/android/gms/wallet/fragment/WalletFragmentInitParams$Builder;

    move-result-object v0

    .line 148
    invoke-virtual {v0}, Lcom/google/android/gms/wallet/fragment/WalletFragmentInitParams$Builder;->build()Lcom/google/android/gms/wallet/fragment/WalletFragmentInitParams;

    move-result-object v0

    .line 145
    return-object v0
.end method


# virtual methods
.method a(Landroid/support/v4/app/j;Lco/uk/getmondo/topup/b$a;)V
    .locals 3

    .prologue
    .line 76
    new-instance v0, Lcom/google/android/gms/wallet/Wallet$WalletOptions$Builder;

    invoke-direct {v0}, Lcom/google/android/gms/wallet/Wallet$WalletOptions$Builder;-><init>()V

    const/4 v1, 0x1

    .line 77
    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/Wallet$WalletOptions$Builder;->setEnvironment(I)Lcom/google/android/gms/wallet/Wallet$WalletOptions$Builder;

    move-result-object v0

    const/4 v1, 0x0

    .line 78
    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/Wallet$WalletOptions$Builder;->setTheme(I)Lcom/google/android/gms/wallet/Wallet$WalletOptions$Builder;

    move-result-object v0

    .line 79
    invoke-virtual {v0}, Lcom/google/android/gms/wallet/Wallet$WalletOptions$Builder;->build()Lcom/google/android/gms/wallet/Wallet$WalletOptions;

    move-result-object v0

    .line 81
    new-instance v1, Lcom/google/android/gms/common/api/GoogleApiClient$Builder;

    invoke-direct {v1, p1}, Lcom/google/android/gms/common/api/GoogleApiClient$Builder;-><init>(Landroid/content/Context;)V

    sget-object v2, Lcom/google/android/gms/wallet/Wallet;->API:Lcom/google/android/gms/common/api/Api;

    .line 82
    invoke-virtual {v1, v2, v0}, Lcom/google/android/gms/common/api/GoogleApiClient$Builder;->addApi(Lcom/google/android/gms/common/api/Api;Lcom/google/android/gms/common/api/Api$ApiOptions$HasOptions;)Lcom/google/android/gms/common/api/GoogleApiClient$Builder;

    move-result-object v0

    const/16 v1, 0x3e8

    .line 83
    invoke-virtual {v0, p1, v1, p0}, Lcom/google/android/gms/common/api/GoogleApiClient$Builder;->enableAutoManage(Landroid/support/v4/app/j;ILcom/google/android/gms/common/api/GoogleApiClient$OnConnectionFailedListener;)Lcom/google/android/gms/common/api/GoogleApiClient$Builder;

    move-result-object v0

    .line 84
    invoke-virtual {v0}, Lcom/google/android/gms/common/api/GoogleApiClient$Builder;->build()Lcom/google/android/gms/common/api/GoogleApiClient;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/topup/b;->c:Lcom/google/android/gms/common/api/GoogleApiClient;

    .line 86
    invoke-static {}, Lcom/google/android/gms/wallet/IsReadyToPayRequest;->newBuilder()Lcom/google/android/gms/wallet/IsReadyToPayRequest$Builder;

    move-result-object v0

    const/4 v1, 0x4

    .line 87
    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/IsReadyToPayRequest$Builder;->addAllowedCardNetwork(I)Lcom/google/android/gms/wallet/IsReadyToPayRequest$Builder;

    move-result-object v0

    const/4 v1, 0x5

    .line 88
    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/IsReadyToPayRequest$Builder;->addAllowedCardNetwork(I)Lcom/google/android/gms/wallet/IsReadyToPayRequest$Builder;

    move-result-object v0

    .line 89
    invoke-virtual {v0}, Lcom/google/android/gms/wallet/IsReadyToPayRequest$Builder;->build()Lcom/google/android/gms/wallet/IsReadyToPayRequest;

    move-result-object v0

    .line 91
    sget-object v1, Lcom/google/android/gms/wallet/Wallet;->Payments:Lcom/google/android/gms/wallet/Payments;

    iget-object v2, p0, Lco/uk/getmondo/topup/b;->c:Lcom/google/android/gms/common/api/GoogleApiClient;

    invoke-interface {v1, v2, v0}, Lcom/google/android/gms/wallet/Payments;->isReadyToPay(Lcom/google/android/gms/common/api/GoogleApiClient;Lcom/google/android/gms/wallet/IsReadyToPayRequest;)Lcom/google/android/gms/common/api/PendingResult;

    move-result-object v0

    invoke-static {p0, p2}, Lco/uk/getmondo/topup/c;->a(Lco/uk/getmondo/topup/b;Lco/uk/getmondo/topup/b$a;)Lcom/google/android/gms/common/api/ResultCallback;

    move-result-object v1

    .line 92
    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/api/PendingResult;->setResultCallback(Lcom/google/android/gms/common/api/ResultCallback;)V

    .line 108
    return-void
.end method

.method a(Lco/uk/getmondo/d/c;)V
    .locals 0

    .prologue
    .line 72
    iput-object p1, p0, Lco/uk/getmondo/topup/b;->b:Lco/uk/getmondo/d/c;

    .line 73
    return-void
.end method

.method a(Lco/uk/getmondo/topup/b$b;)V
    .locals 0

    .prologue
    .line 64
    iput-object p1, p0, Lco/uk/getmondo/topup/b;->e:Lco/uk/getmondo/topup/b$b;

    .line 65
    return-void
.end method

.method a(Lco/uk/getmondo/topup/b$c;)V
    .locals 0

    .prologue
    .line 68
    iput-object p1, p0, Lco/uk/getmondo/topup/b;->f:Lco/uk/getmondo/topup/b$c;

    .line 69
    return-void
.end method

.method a(Lco/uk/getmondo/topup/b$d;)V
    .locals 0

    .prologue
    .line 60
    iput-object p1, p0, Lco/uk/getmondo/topup/b;->d:Lco/uk/getmondo/topup/b$d;

    .line 61
    return-void
.end method

.method a(IILandroid/content/Intent;)Z
    .locals 5

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 159
    sparse-switch p1, :sswitch_data_0

    .line 198
    :goto_0
    return v0

    .line 161
    :sswitch_0
    packed-switch p2, :pswitch_data_0

    .line 174
    iget-object v2, p0, Lco/uk/getmondo/topup/b;->f:Lco/uk/getmondo/topup/b$c;

    invoke-interface {v2, v0}, Lco/uk/getmondo/topup/b$c;->a(Z)V

    .line 175
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "AndroidPayDelegate, masked wallet request failed, error code:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-direct {p0, p3}, Lco/uk/getmondo/topup/b;->a(Landroid/content/Intent;)I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Ld/a/a;->a(Ljava/lang/Throwable;)V

    :goto_1
    move v0, v1

    .line 178
    goto :goto_0

    .line 165
    :pswitch_0
    iget-object v0, p0, Lco/uk/getmondo/topup/b;->e:Lco/uk/getmondo/topup/b$b;

    invoke-interface {v0}, Lco/uk/getmondo/topup/b$b;->a()V

    .line 167
    const-string v0, "com.google.android.gms.wallet.EXTRA_MASKED_WALLET"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/MaskedWallet;

    invoke-direct {p0, v0}, Lco/uk/getmondo/topup/b;->a(Lcom/google/android/gms/wallet/MaskedWallet;)Lcom/google/android/gms/wallet/FullWalletRequest;

    move-result-object v0

    .line 168
    sget-object v2, Lcom/google/android/gms/wallet/Wallet;->Payments:Lcom/google/android/gms/wallet/Payments;

    iget-object v3, p0, Lco/uk/getmondo/topup/b;->c:Lcom/google/android/gms/common/api/GoogleApiClient;

    const/16 v4, 0x3ea

    invoke-interface {v2, v3, v0, v4}, Lcom/google/android/gms/wallet/Payments;->loadFullWallet(Lcom/google/android/gms/common/api/GoogleApiClient;Lcom/google/android/gms/wallet/FullWalletRequest;I)V

    goto :goto_1

    .line 171
    :pswitch_1
    iget-object v0, p0, Lco/uk/getmondo/topup/b;->f:Lco/uk/getmondo/topup/b$c;

    invoke-interface {v0, v1}, Lco/uk/getmondo/topup/b$c;->a(Z)V

    goto :goto_1

    .line 180
    :sswitch_1
    packed-switch p2, :pswitch_data_1

    .line 189
    iget-object v2, p0, Lco/uk/getmondo/topup/b;->f:Lco/uk/getmondo/topup/b$c;

    invoke-interface {v2, v0}, Lco/uk/getmondo/topup/b$c;->a(Z)V

    .line 190
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "AndroidPayDelegate, full wallet request failed, error code:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-direct {p0, p3}, Lco/uk/getmondo/topup/b;->a(Landroid/content/Intent;)I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Ld/a/a;->a(Ljava/lang/Throwable;)V

    :goto_2
    move v0, v1

    .line 192
    goto :goto_0

    .line 182
    :pswitch_2
    const-string v0, "com.google.android.gms.wallet.EXTRA_FULL_WALLET"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/FullWallet;

    .line 183
    iget-object v2, p0, Lco/uk/getmondo/topup/b;->d:Lco/uk/getmondo/topup/b$d;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/FullWallet;->getPaymentMethodToken()Lcom/google/android/gms/wallet/PaymentMethodToken;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/PaymentMethodToken;->getToken()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v2, v0}, Lco/uk/getmondo/topup/b$d;->a(Ljava/lang/String;)V

    goto :goto_2

    .line 186
    :pswitch_3
    iget-object v0, p0, Lco/uk/getmondo/topup/b;->f:Lco/uk/getmondo/topup/b$c;

    invoke-interface {v0, v1}, Lco/uk/getmondo/topup/b$c;->a(Z)V

    goto :goto_2

    .line 194
    :sswitch_2
    iget-object v2, p0, Lco/uk/getmondo/topup/b;->f:Lco/uk/getmondo/topup/b$c;

    invoke-interface {v2, v0}, Lco/uk/getmondo/topup/b$c;->a(Z)V

    .line 195
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "AndroidPayDelegate, wallet constants error:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-direct {p0, p3}, Lco/uk/getmondo/topup/b;->a(Landroid/content/Intent;)I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Ld/a/a;->a(Ljava/lang/Throwable;)V

    move v0, v1

    .line 196
    goto/16 :goto_0

    .line 159
    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_2
        0x3e9 -> :sswitch_0
        0x3ea -> :sswitch_1
    .end sparse-switch

    .line 161
    :pswitch_data_0
    .packed-switch -0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch

    .line 180
    :pswitch_data_1
    .packed-switch -0x1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public onConnectionFailed(Lcom/google/android/gms/common/ConnectionResult;)V
    .locals 3

    .prologue
    .line 154
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "AndroidPayDelegate, failed to connect. Message: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/android/gms/common/ConnectionResult;->getErrorMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", errorCode: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 155
    invoke-virtual {p1}, Lcom/google/android/gms/common/ConnectionResult;->getErrorCode()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    .line 154
    invoke-static {v0}, Ld/a/a;->a(Ljava/lang/Throwable;)V

    .line 156
    return-void
.end method
