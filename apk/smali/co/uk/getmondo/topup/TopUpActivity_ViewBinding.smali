.class public Lco/uk/getmondo/topup/TopUpActivity_ViewBinding;
.super Ljava/lang/Object;
.source "TopUpActivity_ViewBinding.java"

# interfaces
.implements Lbutterknife/Unbinder;


# instance fields
.field private a:Lco/uk/getmondo/topup/TopUpActivity;

.field private b:Landroid/view/View;

.field private c:Landroid/view/View;

.field private d:Landroid/view/View;


# direct methods
.method public constructor <init>(Lco/uk/getmondo/topup/TopUpActivity;Landroid/view/View;)V
    .locals 5

    .prologue
    const v4, 0x7f110246

    const v3, 0x7f110244

    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    iput-object p1, p0, Lco/uk/getmondo/topup/TopUpActivity_ViewBinding;->a:Lco/uk/getmondo/topup/TopUpActivity;

    .line 38
    const v0, 0x7f110245

    const-string v1, "field \'toLoadAmountView\'"

    const-class v2, Lco/uk/getmondo/common/ui/AmountView;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/common/ui/AmountView;

    iput-object v0, p1, Lco/uk/getmondo/topup/TopUpActivity;->toLoadAmountView:Lco/uk/getmondo/common/ui/AmountView;

    .line 39
    const-string v0, "field \'decreaseButton\' and method \'onDecreaseClicked\'"

    invoke-static {p2, v3, v0}, Lbutterknife/internal/Utils;->findRequiredView(Landroid/view/View;ILjava/lang/String;)Landroid/view/View;

    move-result-object v1

    .line 40
    const-string v0, "field \'decreaseButton\'"

    const-class v2, Landroid/widget/ImageButton;

    invoke-static {v1, v3, v0, v2}, Lbutterknife/internal/Utils;->castView(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p1, Lco/uk/getmondo/topup/TopUpActivity;->decreaseButton:Landroid/widget/ImageButton;

    .line 41
    iput-object v1, p0, Lco/uk/getmondo/topup/TopUpActivity_ViewBinding;->b:Landroid/view/View;

    .line 42
    new-instance v0, Lco/uk/getmondo/topup/TopUpActivity_ViewBinding$1;

    invoke-direct {v0, p0, p1}, Lco/uk/getmondo/topup/TopUpActivity_ViewBinding$1;-><init>(Lco/uk/getmondo/topup/TopUpActivity_ViewBinding;Lco/uk/getmondo/topup/TopUpActivity;)V

    invoke-virtual {v1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 48
    const-string v0, "field \'increaseButton\' and method \'onIncreaseClicked\'"

    invoke-static {p2, v4, v0}, Lbutterknife/internal/Utils;->findRequiredView(Landroid/view/View;ILjava/lang/String;)Landroid/view/View;

    move-result-object v1

    .line 49
    const-string v0, "field \'increaseButton\'"

    const-class v2, Landroid/widget/ImageButton;

    invoke-static {v1, v4, v0, v2}, Lbutterknife/internal/Utils;->castView(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p1, Lco/uk/getmondo/topup/TopUpActivity;->increaseButton:Landroid/widget/ImageButton;

    .line 50
    iput-object v1, p0, Lco/uk/getmondo/topup/TopUpActivity_ViewBinding;->c:Landroid/view/View;

    .line 51
    new-instance v0, Lco/uk/getmondo/topup/TopUpActivity_ViewBinding$2;

    invoke-direct {v0, p0, p1}, Lco/uk/getmondo/topup/TopUpActivity_ViewBinding$2;-><init>(Lco/uk/getmondo/topup/TopUpActivity_ViewBinding;Lco/uk/getmondo/topup/TopUpActivity;)V

    invoke-virtual {v1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 57
    const v0, 0x7f110247

    const-string v1, "field \'expectedBalanceView\'"

    const-class v2, Landroid/widget/TextView;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p1, Lco/uk/getmondo/topup/TopUpActivity;->expectedBalanceView:Landroid/widget/TextView;

    .line 58
    const v0, 0x7f110242

    const-string v1, "field \'topUpView\'"

    invoke-static {p2, v0, v1}, Lbutterknife/internal/Utils;->findRequiredView(Landroid/view/View;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p1, Lco/uk/getmondo/topup/TopUpActivity;->topUpView:Landroid/view/View;

    .line 59
    const v0, 0x7f110240

    const-string v1, "field \'topUpUnavailableView\'"

    invoke-static {p2, v0, v1}, Lbutterknife/internal/Utils;->findRequiredView(Landroid/view/View;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p1, Lco/uk/getmondo/topup/TopUpActivity;->topUpUnavailableView:Landroid/view/View;

    .line 60
    const v0, 0x7f110249

    const-string v1, "field \'topUpWithCardButton\'"

    const-class v2, Landroid/widget/Button;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p1, Lco/uk/getmondo/topup/TopUpActivity;->topUpWithCardButton:Landroid/widget/Button;

    .line 61
    const v0, 0x7f110248

    const-string v1, "field \'topUpWithBankButton\'"

    const-class v2, Landroid/widget/Button;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p1, Lco/uk/getmondo/topup/TopUpActivity;->topUpWithBankButton:Landroid/widget/Button;

    .line 62
    const v0, 0x7f11024b

    const-string v1, "field \'overlayView\'"

    invoke-static {p2, v0, v1}, Lbutterknife/internal/Utils;->findRequiredView(Landroid/view/View;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p1, Lco/uk/getmondo/topup/TopUpActivity;->overlayView:Landroid/view/View;

    .line 63
    const v0, 0x7f11024c

    const-string v1, "field \'progressBar\'"

    const-class v2, Landroid/widget/ProgressBar;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p1, Lco/uk/getmondo/topup/TopUpActivity;->progressBar:Landroid/widget/ProgressBar;

    .line 64
    const v0, 0x7f110241

    const-string v1, "method \'onContactSupportClicked\'"

    invoke-static {p2, v0, v1}, Lbutterknife/internal/Utils;->findRequiredView(Landroid/view/View;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 65
    iput-object v0, p0, Lco/uk/getmondo/topup/TopUpActivity_ViewBinding;->d:Landroid/view/View;

    .line 66
    new-instance v1, Lco/uk/getmondo/topup/TopUpActivity_ViewBinding$3;

    invoke-direct {v1, p0, p1}, Lco/uk/getmondo/topup/TopUpActivity_ViewBinding$3;-><init>(Lco/uk/getmondo/topup/TopUpActivity_ViewBinding;Lco/uk/getmondo/topup/TopUpActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 72
    return-void
.end method


# virtual methods
.method public unbind()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 77
    iget-object v0, p0, Lco/uk/getmondo/topup/TopUpActivity_ViewBinding;->a:Lco/uk/getmondo/topup/TopUpActivity;

    .line 78
    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Bindings already cleared."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 79
    :cond_0
    iput-object v1, p0, Lco/uk/getmondo/topup/TopUpActivity_ViewBinding;->a:Lco/uk/getmondo/topup/TopUpActivity;

    .line 81
    iput-object v1, v0, Lco/uk/getmondo/topup/TopUpActivity;->toLoadAmountView:Lco/uk/getmondo/common/ui/AmountView;

    .line 82
    iput-object v1, v0, Lco/uk/getmondo/topup/TopUpActivity;->decreaseButton:Landroid/widget/ImageButton;

    .line 83
    iput-object v1, v0, Lco/uk/getmondo/topup/TopUpActivity;->increaseButton:Landroid/widget/ImageButton;

    .line 84
    iput-object v1, v0, Lco/uk/getmondo/topup/TopUpActivity;->expectedBalanceView:Landroid/widget/TextView;

    .line 85
    iput-object v1, v0, Lco/uk/getmondo/topup/TopUpActivity;->topUpView:Landroid/view/View;

    .line 86
    iput-object v1, v0, Lco/uk/getmondo/topup/TopUpActivity;->topUpUnavailableView:Landroid/view/View;

    .line 87
    iput-object v1, v0, Lco/uk/getmondo/topup/TopUpActivity;->topUpWithCardButton:Landroid/widget/Button;

    .line 88
    iput-object v1, v0, Lco/uk/getmondo/topup/TopUpActivity;->topUpWithBankButton:Landroid/widget/Button;

    .line 89
    iput-object v1, v0, Lco/uk/getmondo/topup/TopUpActivity;->overlayView:Landroid/view/View;

    .line 90
    iput-object v1, v0, Lco/uk/getmondo/topup/TopUpActivity;->progressBar:Landroid/widget/ProgressBar;

    .line 92
    iget-object v0, p0, Lco/uk/getmondo/topup/TopUpActivity_ViewBinding;->b:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 93
    iput-object v1, p0, Lco/uk/getmondo/topup/TopUpActivity_ViewBinding;->b:Landroid/view/View;

    .line 94
    iget-object v0, p0, Lco/uk/getmondo/topup/TopUpActivity_ViewBinding;->c:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 95
    iput-object v1, p0, Lco/uk/getmondo/topup/TopUpActivity_ViewBinding;->c:Landroid/view/View;

    .line 96
    iget-object v0, p0, Lco/uk/getmondo/topup/TopUpActivity_ViewBinding;->d:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 97
    iput-object v1, p0, Lco/uk/getmondo/topup/TopUpActivity_ViewBinding;->d:Landroid/view/View;

    .line 98
    return-void
.end method
