.class public Lco/uk/getmondo/topup/q;
.super Lco/uk/getmondo/common/ui/b;
.source "TopUpPresenter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lco/uk/getmondo/topup/q$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lco/uk/getmondo/common/ui/b",
        "<",
        "Lco/uk/getmondo/topup/q$a;",
        ">;"
    }
.end annotation


# instance fields
.field private final c:Lco/uk/getmondo/a/a;

.field private final d:Lio/reactivex/u;

.field private final e:Lio/reactivex/u;

.field private final f:Lco/uk/getmondo/common/accounts/d;

.field private final g:Lco/uk/getmondo/common/e/a;

.field private final h:Lco/uk/getmondo/api/MonzoApi;

.field private final i:Lco/uk/getmondo/topup/ao;

.field private j:Lco/uk/getmondo/topup/a;

.field private k:Lco/uk/getmondo/d/ae;

.field private l:Lco/uk/getmondo/common/a;

.field private final m:Lco/uk/getmondo/topup/a/c;

.field private final n:Lco/uk/getmondo/topup/a/w;


# direct methods
.method constructor <init>(Lio/reactivex/u;Lio/reactivex/u;Lco/uk/getmondo/common/accounts/d;Lco/uk/getmondo/common/e/a;Lco/uk/getmondo/common/a;Lco/uk/getmondo/api/MonzoApi;Lco/uk/getmondo/topup/ao;Lco/uk/getmondo/a/a;Lco/uk/getmondo/topup/a/c;Lco/uk/getmondo/topup/a/w;)V
    .locals 0

    .prologue
    .line 59
    invoke-direct {p0}, Lco/uk/getmondo/common/ui/b;-><init>()V

    .line 60
    iput-object p1, p0, Lco/uk/getmondo/topup/q;->d:Lio/reactivex/u;

    .line 61
    iput-object p2, p0, Lco/uk/getmondo/topup/q;->e:Lio/reactivex/u;

    .line 62
    iput-object p3, p0, Lco/uk/getmondo/topup/q;->f:Lco/uk/getmondo/common/accounts/d;

    .line 63
    iput-object p4, p0, Lco/uk/getmondo/topup/q;->g:Lco/uk/getmondo/common/e/a;

    .line 64
    iput-object p6, p0, Lco/uk/getmondo/topup/q;->h:Lco/uk/getmondo/api/MonzoApi;

    .line 65
    iput-object p7, p0, Lco/uk/getmondo/topup/q;->i:Lco/uk/getmondo/topup/ao;

    .line 66
    iput-object p8, p0, Lco/uk/getmondo/topup/q;->c:Lco/uk/getmondo/a/a;

    .line 67
    iput-object p5, p0, Lco/uk/getmondo/topup/q;->l:Lco/uk/getmondo/common/a;

    .line 68
    iput-object p9, p0, Lco/uk/getmondo/topup/q;->m:Lco/uk/getmondo/topup/a/c;

    .line 69
    iput-object p10, p0, Lco/uk/getmondo/topup/q;->n:Lco/uk/getmondo/topup/a/w;

    .line 70
    return-void
.end method

.method static synthetic a(Ljava/lang/Integer;)Lco/uk/getmondo/d/c;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 117
    new-instance v0, Lco/uk/getmondo/d/c;

    invoke-virtual {p0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    mul-int/lit8 v1, v1, 0x64

    int-to-long v2, v1

    const-string v1, "GBP"

    invoke-direct {v0, v2, v3, v1}, Lco/uk/getmondo/d/c;-><init>(JLjava/lang/String;)V

    return-object v0
.end method

.method static synthetic a(Lco/uk/getmondo/topup/q;Ljava/lang/String;Lco/uk/getmondo/topup/q$a;Ljava/lang/String;)Lio/reactivex/l;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 166
    new-instance v0, Lco/uk/getmondo/d/c;

    iget-object v1, p0, Lco/uk/getmondo/topup/q;->j:Lco/uk/getmondo/topup/a;

    invoke-virtual {v1}, Lco/uk/getmondo/topup/a;->g()J

    move-result-wide v2

    const-string v1, "GBP"

    invoke-direct {v0, v2, v3, v1}, Lco/uk/getmondo/d/c;-><init>(JLjava/lang/String;)V

    .line 167
    iget-object v1, p0, Lco/uk/getmondo/topup/q;->m:Lco/uk/getmondo/topup/a/c;

    invoke-virtual {v1, p1, p3, v0}, Lco/uk/getmondo/topup/a/c;->a(Ljava/lang/String;Ljava/lang/String;Lco/uk/getmondo/d/c;)Lio/reactivex/b;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/topup/q;->e:Lio/reactivex/u;

    .line 168
    invoke-virtual {v0, v1}, Lio/reactivex/b;->b(Lio/reactivex/u;)Lio/reactivex/b;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/topup/q;->d:Lio/reactivex/u;

    .line 169
    invoke-virtual {v0, v1}, Lio/reactivex/b;->a(Lio/reactivex/u;)Lio/reactivex/b;

    move-result-object v0

    invoke-static {p0, p2}, Lco/uk/getmondo/topup/ab;->a(Lco/uk/getmondo/topup/q;Lco/uk/getmondo/topup/q$a;)Lio/reactivex/c/g;

    move-result-object v1

    .line 170
    invoke-virtual {v0, v1}, Lio/reactivex/b;->a(Lio/reactivex/c/g;)Lio/reactivex/b;

    move-result-object v0

    sget-object v1, Lco/uk/getmondo/common/b/a;->a:Lco/uk/getmondo/common/b/a;

    .line 182
    invoke-virtual {v0, v1}, Lio/reactivex/b;->a(Ljava/lang/Object;)Lio/reactivex/v;

    move-result-object v0

    .line 183
    invoke-virtual {v0}, Lio/reactivex/v;->e()Lio/reactivex/h;

    move-result-object v0

    .line 184
    invoke-static {}, Lio/reactivex/h;->a()Lio/reactivex/h;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/h;->a(Lio/reactivex/l;)Lio/reactivex/h;

    move-result-object v0

    .line 167
    return-object v0
.end method

.method static synthetic a(Lco/uk/getmondo/topup/q;Ljava/lang/String;Lco/uk/getmondo/topup/a;)Lio/reactivex/r;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 115
    .line 116
    invoke-virtual {p2}, Lco/uk/getmondo/topup/a;->f()Lio/reactivex/n;

    move-result-object v0

    invoke-static {}, Lco/uk/getmondo/topup/ad;->a()Lio/reactivex/c/h;

    move-result-object v1

    .line 117
    invoke-virtual {v0, v1}, Lio/reactivex/n;->map(Lio/reactivex/c/h;)Lio/reactivex/n;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/topup/q;->c:Lco/uk/getmondo/a/a;

    .line 118
    invoke-virtual {v1, p1}, Lco/uk/getmondo/a/a;->a(Ljava/lang/String;)Lio/reactivex/n;

    move-result-object v1

    invoke-static {}, Lco/uk/getmondo/topup/ae;->a()Lio/reactivex/c/c;

    move-result-object v2

    .line 115
    invoke-static {v0, v1, v2}, Lio/reactivex/n;->combineLatest(Lio/reactivex/r;Lio/reactivex/r;Lio/reactivex/c/c;)Lio/reactivex/n;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lco/uk/getmondo/d/a/r;Ljava/util/List;)Lio/reactivex/z;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 86
    invoke-static {p1}, Lio/reactivex/n;->fromIterable(Ljava/lang/Iterable;)Lio/reactivex/n;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    invoke-static {p0}, Lco/uk/getmondo/topup/af;->a(Lco/uk/getmondo/d/a/r;)Lio/reactivex/c/h;

    move-result-object v1

    .line 87
    invoke-virtual {v0, v1}, Lio/reactivex/n;->map(Lio/reactivex/c/h;)Lio/reactivex/n;

    move-result-object v0

    .line 88
    invoke-virtual {v0}, Lio/reactivex/n;->toList()Lio/reactivex/v;

    move-result-object v0

    .line 86
    return-object v0
.end method

.method private a(Lco/uk/getmondo/topup/q$a;Lco/uk/getmondo/d/b;)V
    .locals 4

    .prologue
    .line 222
    if-nez p2, :cond_0

    .line 229
    :goto_0
    return-void

    .line 227
    :cond_0
    invoke-virtual {p2}, Lco/uk/getmondo/d/b;->a()Lco/uk/getmondo/d/c;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/topup/q;->j:Lco/uk/getmondo/topup/a;

    invoke-virtual {v1}, Lco/uk/getmondo/topup/a;->e()I

    move-result v1

    mul-int/lit8 v1, v1, 0x64

    int-to-long v2, v1

    invoke-virtual {v0, v2, v3}, Lco/uk/getmondo/d/c;->a(J)Lco/uk/getmondo/d/c;

    move-result-object v0

    .line 228
    iget-object v1, p0, Lco/uk/getmondo/topup/q;->i:Lco/uk/getmondo/topup/ao;

    invoke-virtual {v1, v0}, Lco/uk/getmondo/topup/ao;->a(Lco/uk/getmondo/d/c;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Lco/uk/getmondo/topup/q$a;->a(Ljava/lang/String;)V

    goto :goto_0
.end method

.method static synthetic a(Lco/uk/getmondo/topup/q$a;Ljava/lang/Boolean;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 193
    invoke-interface {p0}, Lco/uk/getmondo/topup/q$a;->g()V

    .line 194
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_0

    .line 195
    invoke-interface {p0}, Lco/uk/getmondo/topup/q$a;->e()V

    .line 197
    :cond_0
    return-void
.end method

.method static synthetic a(Lco/uk/getmondo/topup/q;Lco/uk/getmondo/topup/q$a;Landroid/support/v4/g/j;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 123
    iget-object v0, p2, Landroid/support/v4/g/j;->a:Ljava/lang/Object;

    check-cast v0, Lco/uk/getmondo/d/c;

    invoke-interface {p1, v0}, Lco/uk/getmondo/topup/q$a;->a(Lco/uk/getmondo/d/c;)V

    .line 124
    invoke-direct {p0, p1}, Lco/uk/getmondo/topup/q;->b(Lco/uk/getmondo/topup/q$a;)V

    .line 125
    iget-object v0, p2, Landroid/support/v4/g/j;->b:Ljava/lang/Object;

    check-cast v0, Lco/uk/getmondo/d/b;

    invoke-direct {p0, p1, v0}, Lco/uk/getmondo/topup/q;->a(Lco/uk/getmondo/topup/q$a;Lco/uk/getmondo/d/b;)V

    .line 126
    return-void
.end method

.method static synthetic a(Lco/uk/getmondo/topup/q;Lco/uk/getmondo/topup/q$a;Lco/uk/getmondo/common/b/a;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 187
    iget-object v0, p0, Lco/uk/getmondo/topup/q;->l:Lco/uk/getmondo/common/a;

    sget-object v1, Lco/uk/getmondo/api/model/tracking/Impression$TopUpSuccessType;->ANDROID_PAY:Lco/uk/getmondo/api/model/tracking/Impression$TopUpSuccessType;

    invoke-static {v1}, Lco/uk/getmondo/api/model/tracking/Impression;->a(Lco/uk/getmondo/api/model/tracking/Impression$TopUpSuccessType;)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v1

    invoke-virtual {v0, v1}, Lco/uk/getmondo/common/a;->a(Lco/uk/getmondo/api/model/tracking/Impression;)V

    .line 188
    invoke-interface {p1}, Lco/uk/getmondo/topup/q$a;->d()V

    .line 189
    return-void
.end method

.method static synthetic a(Lco/uk/getmondo/topup/q;Lco/uk/getmondo/topup/q$a;Ljava/lang/Object;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 160
    invoke-interface {p1}, Lco/uk/getmondo/topup/q$a;->f()V

    .line 161
    iget-object v0, p0, Lco/uk/getmondo/topup/q;->l:Lco/uk/getmondo/common/a;

    sget-object v1, Lco/uk/getmondo/api/model/tracking/Impression$TopUpTapFrom;->ANDROID_PAY:Lco/uk/getmondo/api/model/tracking/Impression$TopUpTapFrom;

    invoke-static {v1}, Lco/uk/getmondo/api/model/tracking/Impression;->a(Lco/uk/getmondo/api/model/tracking/Impression$TopUpTapFrom;)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v1

    invoke-virtual {v0, v1}, Lco/uk/getmondo/common/a;->a(Lco/uk/getmondo/api/model/tracking/Impression;)V

    .line 162
    return-void
.end method

.method static synthetic a(Lco/uk/getmondo/topup/q;Lco/uk/getmondo/topup/q$a;Ljava/lang/Throwable;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 171
    invoke-interface {p1}, Lco/uk/getmondo/topup/q$a;->g()V

    .line 172
    instance-of v0, p2, Lorg/json/JSONException;

    if-eqz v0, :cond_1

    .line 173
    invoke-interface {p1}, Lco/uk/getmondo/topup/q$a;->e()V

    .line 181
    :cond_0
    :goto_0
    return-void

    .line 175
    :cond_1
    iget-object v0, p0, Lco/uk/getmondo/topup/q;->n:Lco/uk/getmondo/topup/a/w;

    invoke-virtual {v0, p1, p2}, Lco/uk/getmondo/topup/a/w;->a(Lco/uk/getmondo/common/e/e;Ljava/lang/Throwable;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 179
    iget-object v0, p0, Lco/uk/getmondo/topup/q;->g:Lco/uk/getmondo/common/e/a;

    invoke-virtual {v0, p2, p1}, Lco/uk/getmondo/common/e/a;->a(Ljava/lang/Throwable;Lco/uk/getmondo/common/e/a$a;)Z

    goto :goto_0
.end method

.method static synthetic a(Lco/uk/getmondo/topup/q$a;Landroid/support/v4/g/j;)Z
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 95
    iget-object v0, p1, Landroid/support/v4/g/j;->a:Ljava/lang/Object;

    check-cast v0, Lco/uk/getmondo/api/model/topup/ApiTopUpLimits;

    .line 96
    invoke-virtual {v0}, Lco/uk/getmondo/api/model/topup/ApiTopUpLimits;->b()F

    move-result v1

    const/high16 v2, 0x41200000    # 10.0f

    cmpg-float v1, v1, v2

    if-ltz v1, :cond_0

    invoke-virtual {v0}, Lco/uk/getmondo/api/model/topup/ApiTopUpLimits;->b()F

    move-result v1

    invoke-virtual {v0}, Lco/uk/getmondo/api/model/topup/ApiTopUpLimits;->a()F

    move-result v0

    cmpg-float v0, v1, v0

    if-gez v0, :cond_1

    .line 97
    :cond_0
    invoke-interface {p0}, Lco/uk/getmondo/topup/q$a;->b()V

    .line 98
    invoke-interface {p0}, Lco/uk/getmondo/topup/q$a;->g()V

    .line 99
    const/4 v0, 0x0

    .line 101
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method static synthetic b(Lco/uk/getmondo/topup/q;Lco/uk/getmondo/topup/q$a;Landroid/support/v4/g/j;)Lco/uk/getmondo/topup/a;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 104
    iget-object v0, p2, Landroid/support/v4/g/j;->a:Ljava/lang/Object;

    check-cast v0, Lco/uk/getmondo/api/model/topup/ApiTopUpLimits;

    .line 105
    iget-object v1, p2, Landroid/support/v4/g/j;->b:Ljava/lang/Object;

    check-cast v1, Ljava/util/List;

    .line 107
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v2

    if-lez v2, :cond_0

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lco/uk/getmondo/d/ae;

    :goto_0
    iput-object v1, p0, Lco/uk/getmondo/topup/q;->k:Lco/uk/getmondo/d/ae;

    .line 108
    new-instance v1, Lco/uk/getmondo/topup/a;

    invoke-virtual {v0}, Lco/uk/getmondo/api/model/topup/ApiTopUpLimits;->a()F

    move-result v2

    invoke-virtual {v0}, Lco/uk/getmondo/api/model/topup/ApiTopUpLimits;->b()F

    move-result v0

    const/16 v3, 0x64

    invoke-direct {v1, v2, v0, v3}, Lco/uk/getmondo/topup/a;-><init>(FFI)V

    iput-object v1, p0, Lco/uk/getmondo/topup/q;->j:Lco/uk/getmondo/topup/a;

    .line 110
    invoke-interface {p1}, Lco/uk/getmondo/topup/q$a;->a()V

    .line 111
    const/4 v0, 0x1

    invoke-interface {p1, v0}, Lco/uk/getmondo/topup/q$a;->c(Z)V

    .line 112
    invoke-interface {p1}, Lco/uk/getmondo/topup/q$a;->g()V

    .line 113
    iget-object v0, p0, Lco/uk/getmondo/topup/q;->j:Lco/uk/getmondo/topup/a;

    return-object v0

    .line 107
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private b(Lco/uk/getmondo/topup/q$a;)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 217
    iget-object v0, p0, Lco/uk/getmondo/topup/q;->j:Lco/uk/getmondo/topup/a;

    invoke-virtual {v0}, Lco/uk/getmondo/topup/a;->d()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    invoke-interface {p1, v0}, Lco/uk/getmondo/topup/q$a;->a(Z)V

    .line 218
    iget-object v0, p0, Lco/uk/getmondo/topup/q;->j:Lco/uk/getmondo/topup/a;

    invoke-virtual {v0}, Lco/uk/getmondo/topup/a;->c()Z

    move-result v0

    if-nez v0, :cond_1

    :goto_1
    invoke-interface {p1, v1}, Lco/uk/getmondo/topup/q$a;->b(Z)V

    .line 219
    return-void

    :cond_0
    move v0, v2

    .line 217
    goto :goto_0

    :cond_1
    move v1, v2

    .line 218
    goto :goto_1
.end method

.method static synthetic b(Lco/uk/getmondo/topup/q;Lco/uk/getmondo/topup/q$a;Ljava/lang/Object;)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 150
    new-instance v0, Lco/uk/getmondo/d/c;

    iget-object v1, p0, Lco/uk/getmondo/topup/q;->j:Lco/uk/getmondo/topup/a;

    invoke-virtual {v1}, Lco/uk/getmondo/topup/a;->g()J

    move-result-wide v2

    const-string v1, "GBP"

    invoke-direct {v0, v2, v3, v1}, Lco/uk/getmondo/d/c;-><init>(JLjava/lang/String;)V

    .line 151
    iget-object v1, p0, Lco/uk/getmondo/topup/q;->k:Lco/uk/getmondo/d/ae;

    if-eqz v1, :cond_0

    .line 152
    iget-object v1, p0, Lco/uk/getmondo/topup/q;->k:Lco/uk/getmondo/d/ae;

    invoke-interface {p1, v0, v1}, Lco/uk/getmondo/topup/q$a;->a(Lco/uk/getmondo/d/c;Lco/uk/getmondo/d/ae;)V

    .line 156
    :goto_0
    return-void

    .line 154
    :cond_0
    invoke-interface {p1, v0}, Lco/uk/getmondo/topup/q$a;->b(Lco/uk/getmondo/d/c;)V

    goto :goto_0
.end method

.method static synthetic b(Lco/uk/getmondo/topup/q;Lco/uk/getmondo/topup/q$a;Ljava/lang/Throwable;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 140
    iget-object v0, p0, Lco/uk/getmondo/topup/q;->g:Lco/uk/getmondo/common/e/a;

    invoke-virtual {v0, p2, p1}, Lco/uk/getmondo/common/e/a;->a(Ljava/lang/Throwable;Lco/uk/getmondo/common/e/a$a;)Z

    return-void
.end method

.method static synthetic c(Lco/uk/getmondo/topup/q;Lco/uk/getmondo/topup/q$a;Ljava/lang/Object;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 144
    iget-object v0, p0, Lco/uk/getmondo/topup/q;->l:Lco/uk/getmondo/common/a;

    invoke-static {}, Lco/uk/getmondo/api/model/tracking/Impression;->k()Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v1

    invoke-virtual {v0, v1}, Lco/uk/getmondo/common/a;->a(Lco/uk/getmondo/api/model/tracking/Impression;)V

    .line 145
    invoke-interface {p1}, Lco/uk/getmondo/topup/q$a;->c()V

    .line 146
    return-void
.end method

.method static synthetic c(Lco/uk/getmondo/topup/q;Lco/uk/getmondo/topup/q$a;Ljava/lang/Throwable;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 127
    invoke-interface {p1}, Lco/uk/getmondo/topup/q$a;->g()V

    .line 131
    iget-object v0, p0, Lco/uk/getmondo/topup/q;->g:Lco/uk/getmondo/common/e/a;

    invoke-virtual {v0, p2, p1}, Lco/uk/getmondo/common/e/a;->a(Ljava/lang/Throwable;Lco/uk/getmondo/common/e/a$a;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 132
    invoke-interface {p1}, Lco/uk/getmondo/topup/q$a;->h()V

    .line 134
    :cond_0
    return-void
.end method

.method static synthetic d()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 139
    const-string v0, "Balance refreshed"

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Ld/a/a;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public bridge synthetic a(Lco/uk/getmondo/common/ui/f;)V
    .locals 0

    .prologue
    .line 38
    check-cast p1, Lco/uk/getmondo/topup/q$a;

    invoke-virtual {p0, p1}, Lco/uk/getmondo/topup/q;->a(Lco/uk/getmondo/topup/q$a;)V

    return-void
.end method

.method public a(Lco/uk/getmondo/topup/q$a;)V
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 74
    invoke-super {p0, p1}, Lco/uk/getmondo/common/ui/b;->a(Lco/uk/getmondo/common/ui/f;)V

    .line 77
    invoke-interface {p1, v0}, Lco/uk/getmondo/topup/q$a;->a(Z)V

    .line 78
    invoke-interface {p1, v0}, Lco/uk/getmondo/topup/q$a;->b(Z)V

    .line 80
    iget-object v0, p0, Lco/uk/getmondo/topup/q;->f:Lco/uk/getmondo/common/accounts/d;

    invoke-virtual {v0}, Lco/uk/getmondo/common/accounts/d;->b()Lco/uk/getmondo/d/ak;

    move-result-object v0

    invoke-virtual {v0}, Lco/uk/getmondo/d/ak;->c()Lco/uk/getmondo/d/a;

    move-result-object v0

    invoke-interface {v0}, Lco/uk/getmondo/d/a;->a()Ljava/lang/String;

    move-result-object v0

    .line 83
    new-instance v1, Lco/uk/getmondo/d/a/r;

    invoke-direct {v1}, Lco/uk/getmondo/d/a/r;-><init>()V

    .line 84
    iget-object v2, p0, Lco/uk/getmondo/topup/q;->h:Lco/uk/getmondo/api/MonzoApi;

    invoke-interface {v2, v0}, Lco/uk/getmondo/api/MonzoApi;->stripeCards(Ljava/lang/String;)Lio/reactivex/v;

    move-result-object v2

    invoke-static {}, Lco/uk/getmondo/topup/r;->a()Lio/reactivex/c/h;

    move-result-object v3

    .line 85
    invoke-virtual {v2, v3}, Lio/reactivex/v;->d(Lio/reactivex/c/h;)Lio/reactivex/v;

    move-result-object v2

    invoke-static {v1}, Lco/uk/getmondo/topup/ac;->a(Lco/uk/getmondo/d/a/r;)Lio/reactivex/c/h;

    move-result-object v1

    .line 86
    invoke-virtual {v2, v1}, Lio/reactivex/v;->a(Lio/reactivex/c/h;)Lio/reactivex/v;

    move-result-object v1

    .line 90
    invoke-interface {p1}, Lco/uk/getmondo/topup/q$a;->f()V

    .line 91
    iget-object v2, p0, Lco/uk/getmondo/topup/q;->h:Lco/uk/getmondo/api/MonzoApi;

    invoke-interface {v2, v0}, Lco/uk/getmondo/api/MonzoApi;->topUpLimits(Ljava/lang/String;)Lio/reactivex/v;

    move-result-object v2

    invoke-static {}, Lco/uk/getmondo/topup/ag;->a()Lio/reactivex/c/c;

    move-result-object v3

    invoke-static {v2, v1, v3}, Lio/reactivex/v;->a(Lio/reactivex/z;Lio/reactivex/z;Lio/reactivex/c/c;)Lio/reactivex/v;

    move-result-object v1

    iget-object v2, p0, Lco/uk/getmondo/topup/q;->e:Lio/reactivex/u;

    .line 92
    invoke-virtual {v1, v2}, Lio/reactivex/v;->b(Lio/reactivex/u;)Lio/reactivex/v;

    move-result-object v1

    iget-object v2, p0, Lco/uk/getmondo/topup/q;->d:Lio/reactivex/u;

    .line 93
    invoke-virtual {v1, v2}, Lio/reactivex/v;->a(Lio/reactivex/u;)Lio/reactivex/v;

    move-result-object v1

    invoke-static {p1}, Lco/uk/getmondo/topup/ah;->a(Lco/uk/getmondo/topup/q$a;)Lio/reactivex/c/q;

    move-result-object v2

    .line 94
    invoke-virtual {v1, v2}, Lio/reactivex/v;->a(Lio/reactivex/c/q;)Lio/reactivex/h;

    move-result-object v1

    invoke-static {p0, p1}, Lco/uk/getmondo/topup/ai;->a(Lco/uk/getmondo/topup/q;Lco/uk/getmondo/topup/q$a;)Lio/reactivex/c/h;

    move-result-object v2

    .line 103
    invoke-virtual {v1, v2}, Lio/reactivex/h;->c(Lio/reactivex/c/h;)Lio/reactivex/h;

    move-result-object v1

    invoke-static {p0, v0}, Lco/uk/getmondo/topup/aj;->a(Lco/uk/getmondo/topup/q;Ljava/lang/String;)Lio/reactivex/c/h;

    move-result-object v2

    .line 115
    invoke-virtual {v1, v2}, Lio/reactivex/h;->a(Lio/reactivex/c/h;)Lio/reactivex/n;

    move-result-object v1

    iget-object v2, p0, Lco/uk/getmondo/topup/q;->d:Lio/reactivex/u;

    .line 121
    invoke-virtual {v1, v2}, Lio/reactivex/n;->observeOn(Lio/reactivex/u;)Lio/reactivex/n;

    move-result-object v1

    invoke-static {p0, p1}, Lco/uk/getmondo/topup/ak;->a(Lco/uk/getmondo/topup/q;Lco/uk/getmondo/topup/q$a;)Lio/reactivex/c/g;

    move-result-object v2

    invoke-static {p0, p1}, Lco/uk/getmondo/topup/al;->a(Lco/uk/getmondo/topup/q;Lco/uk/getmondo/topup/q$a;)Lio/reactivex/c/g;

    move-result-object v3

    .line 122
    invoke-virtual {v1, v2, v3}, Lio/reactivex/n;->subscribe(Lio/reactivex/c/g;Lio/reactivex/c/g;)Lio/reactivex/b/b;

    move-result-object v1

    .line 91
    invoke-virtual {p0, v1}, Lco/uk/getmondo/topup/q;->a(Lio/reactivex/b/b;)V

    .line 136
    iget-object v1, p0, Lco/uk/getmondo/topup/q;->c:Lco/uk/getmondo/a/a;

    invoke-virtual {v1, v0}, Lco/uk/getmondo/a/a;->b(Ljava/lang/String;)Lio/reactivex/b;

    move-result-object v1

    iget-object v2, p0, Lco/uk/getmondo/topup/q;->e:Lio/reactivex/u;

    .line 137
    invoke-virtual {v1, v2}, Lio/reactivex/b;->b(Lio/reactivex/u;)Lio/reactivex/b;

    move-result-object v1

    iget-object v2, p0, Lco/uk/getmondo/topup/q;->d:Lio/reactivex/u;

    .line 138
    invoke-virtual {v1, v2}, Lio/reactivex/b;->a(Lio/reactivex/u;)Lio/reactivex/b;

    move-result-object v1

    invoke-static {}, Lco/uk/getmondo/topup/am;->b()Lio/reactivex/c/a;

    move-result-object v2

    invoke-static {p0, p1}, Lco/uk/getmondo/topup/s;->a(Lco/uk/getmondo/topup/q;Lco/uk/getmondo/topup/q$a;)Lio/reactivex/c/g;

    move-result-object v3

    .line 139
    invoke-virtual {v1, v2, v3}, Lio/reactivex/b;->a(Lio/reactivex/c/a;Lio/reactivex/c/g;)Lio/reactivex/b/b;

    move-result-object v1

    .line 136
    invoke-virtual {p0, v1}, Lco/uk/getmondo/topup/q;->a(Lio/reactivex/b/b;)V

    .line 142
    invoke-interface {p1}, Lco/uk/getmondo/topup/q$a;->j()Lio/reactivex/n;

    move-result-object v1

    invoke-static {p0, p1}, Lco/uk/getmondo/topup/t;->a(Lco/uk/getmondo/topup/q;Lco/uk/getmondo/topup/q$a;)Lio/reactivex/c/g;

    move-result-object v2

    .line 143
    invoke-virtual {v1, v2}, Lio/reactivex/n;->subscribe(Lio/reactivex/c/g;)Lio/reactivex/b/b;

    move-result-object v1

    .line 142
    invoke-virtual {p0, v1}, Lco/uk/getmondo/topup/q;->a(Lio/reactivex/b/b;)V

    .line 148
    invoke-interface {p1}, Lco/uk/getmondo/topup/q$a;->i()Lio/reactivex/n;

    move-result-object v1

    invoke-static {p0, p1}, Lco/uk/getmondo/topup/u;->a(Lco/uk/getmondo/topup/q;Lco/uk/getmondo/topup/q$a;)Lio/reactivex/c/g;

    move-result-object v2

    .line 149
    invoke-virtual {v1, v2}, Lio/reactivex/n;->subscribe(Lio/reactivex/c/g;)Lio/reactivex/b/b;

    move-result-object v1

    .line 148
    invoke-virtual {p0, v1}, Lco/uk/getmondo/topup/q;->a(Lio/reactivex/b/b;)V

    .line 158
    invoke-interface {p1}, Lco/uk/getmondo/topup/q$a;->v()Lio/reactivex/n;

    move-result-object v1

    invoke-static {p0, p1}, Lco/uk/getmondo/topup/v;->a(Lco/uk/getmondo/topup/q;Lco/uk/getmondo/topup/q$a;)Lio/reactivex/c/g;

    move-result-object v2

    .line 159
    invoke-virtual {v1, v2}, Lio/reactivex/n;->subscribe(Lio/reactivex/c/g;)Lio/reactivex/b/b;

    move-result-object v1

    .line 158
    invoke-virtual {p0, v1}, Lco/uk/getmondo/topup/q;->a(Lio/reactivex/b/b;)V

    .line 164
    invoke-interface {p1}, Lco/uk/getmondo/topup/q$a;->k()Lio/reactivex/n;

    move-result-object v1

    invoke-static {p0, v0, p1}, Lco/uk/getmondo/topup/w;->a(Lco/uk/getmondo/topup/q;Ljava/lang/String;Lco/uk/getmondo/topup/q$a;)Lio/reactivex/c/h;

    move-result-object v0

    .line 165
    invoke-virtual {v1, v0}, Lio/reactivex/n;->flatMapMaybe(Lio/reactivex/c/h;)Lio/reactivex/n;

    move-result-object v0

    invoke-static {p0, p1}, Lco/uk/getmondo/topup/x;->a(Lco/uk/getmondo/topup/q;Lco/uk/getmondo/topup/q$a;)Lio/reactivex/c/g;

    move-result-object v1

    invoke-static {}, Lco/uk/getmondo/topup/y;->a()Lio/reactivex/c/g;

    move-result-object v2

    .line 186
    invoke-virtual {v0, v1, v2}, Lio/reactivex/n;->subscribe(Lio/reactivex/c/g;Lio/reactivex/c/g;)Lio/reactivex/b/b;

    move-result-object v0

    .line 164
    invoke-virtual {p0, v0}, Lco/uk/getmondo/topup/q;->a(Lio/reactivex/b/b;)V

    .line 191
    invoke-interface {p1}, Lco/uk/getmondo/topup/q$a;->w()Lio/reactivex/n;

    move-result-object v0

    invoke-static {p1}, Lco/uk/getmondo/topup/z;->a(Lco/uk/getmondo/topup/q$a;)Lio/reactivex/c/g;

    move-result-object v1

    invoke-static {}, Lco/uk/getmondo/topup/aa;->a()Lio/reactivex/c/g;

    move-result-object v2

    .line 192
    invoke-virtual {v0, v1, v2}, Lio/reactivex/n;->subscribe(Lio/reactivex/c/g;Lio/reactivex/c/g;)Lio/reactivex/b/b;

    move-result-object v0

    .line 191
    invoke-virtual {p0, v0}, Lco/uk/getmondo/topup/q;->a(Lio/reactivex/b/b;)V

    .line 198
    return-void
.end method

.method a()Z
    .locals 2

    .prologue
    .line 201
    iget-object v0, p0, Lco/uk/getmondo/topup/q;->j:Lco/uk/getmondo/topup/a;

    if-nez v0, :cond_0

    .line 202
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Tried to increase load without loading limits"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 205
    :cond_0
    iget-object v0, p0, Lco/uk/getmondo/topup/q;->j:Lco/uk/getmondo/topup/a;

    invoke-virtual {v0}, Lco/uk/getmondo/topup/a;->a()Z

    move-result v0

    return v0
.end method

.method c()Z
    .locals 2

    .prologue
    .line 209
    iget-object v0, p0, Lco/uk/getmondo/topup/q;->j:Lco/uk/getmondo/topup/a;

    if-nez v0, :cond_0

    .line 210
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Tried to decrease load without loading limits"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 213
    :cond_0
    iget-object v0, p0, Lco/uk/getmondo/topup/q;->j:Lco/uk/getmondo/topup/a;

    invoke-virtual {v0}, Lco/uk/getmondo/topup/a;->b()Z

    move-result v0

    return v0
.end method
