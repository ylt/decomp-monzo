.class public final Lco/uk/getmondo/topup/p;
.super Ljava/lang/Object;
.source "TopUpActivity_MembersInjector.java"

# interfaces
.implements Lb/a;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lb/a",
        "<",
        "Lco/uk/getmondo/topup/TopUpActivity;",
        ">;"
    }
.end annotation


# static fields
.field static final synthetic a:Z


# instance fields
.field private final b:Ljavax/a/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/topup/q;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Ljavax/a/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/common/q;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Ljavax/a/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/common/a/c;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Ljavax/a/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/topup/b;",
            ">;"
        }
    .end annotation
.end field

.field private final f:Ljavax/a/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/common/a;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 10
    const-class v0, Lco/uk/getmondo/topup/p;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lco/uk/getmondo/topup/p;->a:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/topup/q;",
            ">;",
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/common/q;",
            ">;",
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/common/a/c;",
            ">;",
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/topup/b;",
            ">;",
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/common/a;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    sget-boolean v0, Lco/uk/getmondo/topup/p;->a:Z

    if-nez v0, :cond_0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 32
    :cond_0
    iput-object p1, p0, Lco/uk/getmondo/topup/p;->b:Ljavax/a/a;

    .line 33
    sget-boolean v0, Lco/uk/getmondo/topup/p;->a:Z

    if-nez v0, :cond_1

    if-nez p2, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 34
    :cond_1
    iput-object p2, p0, Lco/uk/getmondo/topup/p;->c:Ljavax/a/a;

    .line 35
    sget-boolean v0, Lco/uk/getmondo/topup/p;->a:Z

    if-nez v0, :cond_2

    if-nez p3, :cond_2

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 36
    :cond_2
    iput-object p3, p0, Lco/uk/getmondo/topup/p;->d:Ljavax/a/a;

    .line 37
    sget-boolean v0, Lco/uk/getmondo/topup/p;->a:Z

    if-nez v0, :cond_3

    if-nez p4, :cond_3

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 38
    :cond_3
    iput-object p4, p0, Lco/uk/getmondo/topup/p;->e:Ljavax/a/a;

    .line 39
    sget-boolean v0, Lco/uk/getmondo/topup/p;->a:Z

    if-nez v0, :cond_4

    if-nez p5, :cond_4

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 40
    :cond_4
    iput-object p5, p0, Lco/uk/getmondo/topup/p;->f:Ljavax/a/a;

    .line 41
    return-void
.end method

.method public static a(Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;)Lb/a;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/topup/q;",
            ">;",
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/common/q;",
            ">;",
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/common/a/c;",
            ">;",
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/topup/b;",
            ">;",
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/common/a;",
            ">;)",
            "Lb/a",
            "<",
            "Lco/uk/getmondo/topup/TopUpActivity;",
            ">;"
        }
    .end annotation

    .prologue
    .line 49
    new-instance v0, Lco/uk/getmondo/topup/p;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lco/uk/getmondo/topup/p;-><init>(Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;)V

    return-object v0
.end method


# virtual methods
.method public a(Lco/uk/getmondo/topup/TopUpActivity;)V
    .locals 2

    .prologue
    .line 59
    if-nez p1, :cond_0

    .line 60
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Cannot inject members into a null reference"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 62
    :cond_0
    iget-object v0, p0, Lco/uk/getmondo/topup/p;->b:Ljavax/a/a;

    invoke-interface {v0}, Ljavax/a/a;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/topup/q;

    iput-object v0, p1, Lco/uk/getmondo/topup/TopUpActivity;->a:Lco/uk/getmondo/topup/q;

    .line 63
    iget-object v0, p0, Lco/uk/getmondo/topup/p;->c:Ljavax/a/a;

    invoke-interface {v0}, Ljavax/a/a;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/common/q;

    iput-object v0, p1, Lco/uk/getmondo/topup/TopUpActivity;->b:Lco/uk/getmondo/common/q;

    .line 64
    iget-object v0, p0, Lco/uk/getmondo/topup/p;->d:Ljavax/a/a;

    invoke-interface {v0}, Ljavax/a/a;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/common/a/c;

    iput-object v0, p1, Lco/uk/getmondo/topup/TopUpActivity;->c:Lco/uk/getmondo/common/a/c;

    .line 65
    iget-object v0, p0, Lco/uk/getmondo/topup/p;->e:Ljavax/a/a;

    invoke-interface {v0}, Ljavax/a/a;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/topup/b;

    iput-object v0, p1, Lco/uk/getmondo/topup/TopUpActivity;->e:Lco/uk/getmondo/topup/b;

    .line 66
    iget-object v0, p0, Lco/uk/getmondo/topup/p;->f:Ljavax/a/a;

    invoke-interface {v0}, Ljavax/a/a;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/common/a;

    iput-object v0, p1, Lco/uk/getmondo/topup/TopUpActivity;->f:Lco/uk/getmondo/common/a;

    .line 67
    return-void
.end method

.method public bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 10
    check-cast p1, Lco/uk/getmondo/topup/TopUpActivity;

    invoke-virtual {p0, p1}, Lco/uk/getmondo/topup/p;->a(Lco/uk/getmondo/topup/TopUpActivity;)V

    return-void
.end method
