.class public Lco/uk/getmondo/topup/e;
.super Ljava/lang/Object;
.source "LongPressHelper.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lco/uk/getmondo/topup/e$a;
    }
.end annotation


# instance fields
.field private final a:Landroid/os/Handler;

.field private b:Lco/uk/getmondo/topup/e$a;

.field private c:Landroid/view/View;

.field private d:Landroid/view/View;

.field private e:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Runnable;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final f:Ljava/lang/Runnable;

.field private final g:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 52
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lco/uk/getmondo/topup/e;->a:Landroid/os/Handler;

    .line 25
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lco/uk/getmondo/topup/e;->e:Ljava/util/Map;

    .line 26
    new-instance v0, Lco/uk/getmondo/topup/e$1;

    invoke-direct {v0, p0}, Lco/uk/getmondo/topup/e$1;-><init>(Lco/uk/getmondo/topup/e;)V

    iput-object v0, p0, Lco/uk/getmondo/topup/e;->f:Ljava/lang/Runnable;

    .line 38
    new-instance v0, Lco/uk/getmondo/topup/e$2;

    invoke-direct {v0, p0}, Lco/uk/getmondo/topup/e$2;-><init>(Lco/uk/getmondo/topup/e;)V

    iput-object v0, p0, Lco/uk/getmondo/topup/e;->g:Ljava/lang/Runnable;

    .line 53
    return-void
.end method

.method private a(Ljava/lang/Runnable;)Landroid/view/View$OnTouchListener;
    .locals 1

    .prologue
    .line 78
    invoke-static {p0, p1}, Lco/uk/getmondo/topup/f;->a(Lco/uk/getmondo/topup/e;Ljava/lang/Runnable;)Landroid/view/View$OnTouchListener;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lco/uk/getmondo/topup/e;)Ljava/util/Map;
    .locals 1

    .prologue
    .line 17
    iget-object v0, p0, Lco/uk/getmondo/topup/e;->e:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic a(Lco/uk/getmondo/topup/e;Ljava/lang/Runnable;Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 79
    invoke-virtual {p3}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    .line 99
    :goto_0
    :pswitch_0
    return v0

    .line 81
    :pswitch_1
    invoke-virtual {p2, v1}, Landroid/view/View;->setPressed(Z)V

    .line 82
    iget-object v2, p0, Lco/uk/getmondo/topup/e;->e:Ljava/util/Map;

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-interface {v2, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 83
    iget-object v0, p0, Lco/uk/getmondo/topup/e;->a:Landroid/os/Handler;

    const-wide/16 v2, 0x258

    invoke-virtual {v0, p1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    move v0, v1

    .line 84
    goto :goto_0

    .line 86
    :pswitch_2
    invoke-virtual {p2, v0}, Landroid/view/View;->setPressed(Z)V

    .line 87
    iget-object v0, p0, Lco/uk/getmondo/topup/e;->a:Landroid/os/Handler;

    invoke-virtual {v0, p1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    move v0, v1

    .line 88
    goto :goto_0

    .line 90
    :pswitch_3
    invoke-virtual {p2, v0}, Landroid/view/View;->setPressed(Z)V

    .line 91
    iget-object v2, p0, Lco/uk/getmondo/topup/e;->a:Landroid/os/Handler;

    invoke-virtual {v2, p1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 92
    iget-object v2, p0, Lco/uk/getmondo/topup/e;->e:Ljava/util/Map;

    invoke-interface {v2, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v0, p0, Lco/uk/getmondo/topup/e;->e:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    .line 93
    :cond_0
    if-nez v0, :cond_1

    .line 95
    invoke-virtual {p2}, Landroid/view/View;->performClick()Z

    :cond_1
    move v0, v1

    .line 97
    goto :goto_0

    .line 79
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_3
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method static synthetic b(Lco/uk/getmondo/topup/e;)Lco/uk/getmondo/topup/e$a;
    .locals 1

    .prologue
    .line 17
    iget-object v0, p0, Lco/uk/getmondo/topup/e;->b:Lco/uk/getmondo/topup/e$a;

    return-object v0
.end method

.method static synthetic c(Lco/uk/getmondo/topup/e;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 17
    iget-object v0, p0, Lco/uk/getmondo/topup/e;->a:Landroid/os/Handler;

    return-object v0
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 67
    iget-object v0, p0, Lco/uk/getmondo/topup/e;->c:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 68
    iget-object v0, p0, Lco/uk/getmondo/topup/e;->c:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 70
    :cond_0
    iget-object v0, p0, Lco/uk/getmondo/topup/e;->d:Landroid/view/View;

    if-eqz v0, :cond_1

    .line 71
    iget-object v0, p0, Lco/uk/getmondo/topup/e;->d:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 73
    :cond_1
    iget-object v0, p0, Lco/uk/getmondo/topup/e;->a:Landroid/os/Handler;

    iget-object v1, p0, Lco/uk/getmondo/topup/e;->g:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 74
    iget-object v0, p0, Lco/uk/getmondo/topup/e;->a:Landroid/os/Handler;

    iget-object v1, p0, Lco/uk/getmondo/topup/e;->f:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 75
    return-void
.end method

.method public a(Landroid/view/View;Landroid/view/View;)V
    .locals 2

    .prologue
    .line 60
    iput-object p1, p0, Lco/uk/getmondo/topup/e;->c:Landroid/view/View;

    .line 61
    iput-object p2, p0, Lco/uk/getmondo/topup/e;->d:Landroid/view/View;

    .line 62
    iget-object v0, p0, Lco/uk/getmondo/topup/e;->c:Landroid/view/View;

    iget-object v1, p0, Lco/uk/getmondo/topup/e;->f:Ljava/lang/Runnable;

    invoke-direct {p0, v1}, Lco/uk/getmondo/topup/e;->a(Ljava/lang/Runnable;)Landroid/view/View$OnTouchListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 63
    iget-object v0, p0, Lco/uk/getmondo/topup/e;->d:Landroid/view/View;

    iget-object v1, p0, Lco/uk/getmondo/topup/e;->g:Ljava/lang/Runnable;

    invoke-direct {p0, v1}, Lco/uk/getmondo/topup/e;->a(Ljava/lang/Runnable;)Landroid/view/View$OnTouchListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 64
    return-void
.end method

.method public a(Lco/uk/getmondo/topup/e$a;)V
    .locals 0

    .prologue
    .line 56
    iput-object p1, p0, Lco/uk/getmondo/topup/e;->b:Lco/uk/getmondo/topup/e$a;

    .line 57
    return-void
.end method
