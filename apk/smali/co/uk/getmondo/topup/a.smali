.class Lco/uk/getmondo/topup/a;
.super Ljava/lang/Object;
.source "AmountStepper.java"


# instance fields
.field private final a:I

.field private final b:I

.field private final c:I

.field private d:I

.field private e:Lcom/b/b/b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/b/b/b",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(FFI)V
    .locals 2

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    float-to-double v0, p1

    invoke-static {v0, v1}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v0

    double-to-int v0, v0

    iput v0, p0, Lco/uk/getmondo/topup/a;->a:I

    .line 31
    float-to-double v0, p2

    invoke-static {v0, v1}, Ljava/lang/Math;->floor(D)D

    move-result-wide v0

    double-to-int v0, v0

    iput v0, p0, Lco/uk/getmondo/topup/a;->b:I

    .line 32
    iput p3, p0, Lco/uk/getmondo/topup/a;->c:I

    .line 33
    iget v0, p0, Lco/uk/getmondo/topup/a;->a:I

    iput v0, p0, Lco/uk/getmondo/topup/a;->d:I

    .line 34
    iget v0, p0, Lco/uk/getmondo/topup/a;->d:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {v0}, Lcom/b/b/b;->b(Ljava/lang/Object;)Lcom/b/b/b;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/topup/a;->e:Lcom/b/b/b;

    .line 35
    return-void
.end method

.method private a(I)V
    .locals 2

    .prologue
    .line 107
    iput p1, p0, Lco/uk/getmondo/topup/a;->d:I

    .line 108
    iget-object v0, p0, Lco/uk/getmondo/topup/a;->e:Lcom/b/b/b;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/b/b/b;->a(Ljava/lang/Object;)V

    .line 109
    return-void
.end method

.method private h()I
    .locals 2

    .prologue
    .line 91
    iget v0, p0, Lco/uk/getmondo/topup/a;->d:I

    iget v1, p0, Lco/uk/getmondo/topup/a;->c:I

    if-ge v0, v1, :cond_0

    .line 92
    const/16 v0, 0xa

    .line 94
    :goto_0
    return v0

    :cond_0
    const/16 v0, 0x32

    goto :goto_0
.end method

.method private i()I
    .locals 2

    .prologue
    .line 99
    iget v0, p0, Lco/uk/getmondo/topup/a;->d:I

    iget v1, p0, Lco/uk/getmondo/topup/a;->c:I

    if-gt v0, v1, :cond_0

    .line 100
    const/16 v0, 0xa

    .line 102
    :goto_0
    return v0

    :cond_0
    const/16 v0, 0x32

    goto :goto_0
.end method


# virtual methods
.method a()Z
    .locals 2

    .prologue
    .line 43
    iget v0, p0, Lco/uk/getmondo/topup/a;->d:I

    invoke-direct {p0}, Lco/uk/getmondo/topup/a;->h()I

    move-result v1

    add-int/2addr v0, v1

    .line 44
    iget v1, p0, Lco/uk/getmondo/topup/a;->b:I

    if-gt v0, v1, :cond_0

    .line 45
    invoke-direct {p0, v0}, Lco/uk/getmondo/topup/a;->a(I)V

    .line 47
    :cond_0
    invoke-virtual {p0}, Lco/uk/getmondo/topup/a;->c()Z

    move-result v0

    return v0
.end method

.method b()Z
    .locals 2

    .prologue
    .line 56
    iget v0, p0, Lco/uk/getmondo/topup/a;->d:I

    invoke-direct {p0}, Lco/uk/getmondo/topup/a;->i()I

    move-result v1

    sub-int/2addr v0, v1

    .line 57
    iget v1, p0, Lco/uk/getmondo/topup/a;->a:I

    if-lt v0, v1, :cond_0

    .line 58
    invoke-direct {p0, v0}, Lco/uk/getmondo/topup/a;->a(I)V

    .line 60
    :cond_0
    invoke-virtual {p0}, Lco/uk/getmondo/topup/a;->d()Z

    move-result v0

    return v0
.end method

.method c()Z
    .locals 2

    .prologue
    .line 64
    iget v0, p0, Lco/uk/getmondo/topup/a;->d:I

    iget v1, p0, Lco/uk/getmondo/topup/a;->b:I

    if-ge v0, v1, :cond_0

    iget v0, p0, Lco/uk/getmondo/topup/a;->d:I

    invoke-direct {p0}, Lco/uk/getmondo/topup/a;->h()I

    move-result v1

    add-int/2addr v0, v1

    iget v1, p0, Lco/uk/getmondo/topup/a;->b:I

    if-le v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method d()Z
    .locals 2

    .prologue
    .line 68
    iget v0, p0, Lco/uk/getmondo/topup/a;->d:I

    iget v1, p0, Lco/uk/getmondo/topup/a;->a:I

    if-le v0, v1, :cond_0

    iget v0, p0, Lco/uk/getmondo/topup/a;->d:I

    invoke-direct {p0}, Lco/uk/getmondo/topup/a;->i()I

    move-result v1

    sub-int/2addr v0, v1

    iget v1, p0, Lco/uk/getmondo/topup/a;->a:I

    if-ge v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method e()I
    .locals 1

    .prologue
    .line 72
    iget v0, p0, Lco/uk/getmondo/topup/a;->d:I

    return v0
.end method

.method f()Lio/reactivex/n;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 83
    iget-object v0, p0, Lco/uk/getmondo/topup/a;->e:Lcom/b/b/b;

    return-object v0
.end method

.method g()J
    .locals 2

    .prologue
    .line 87
    iget v0, p0, Lco/uk/getmondo/topup/a;->d:I

    mul-int/lit8 v0, v0, 0x64

    int-to-long v0, v0

    return-wide v0
.end method
