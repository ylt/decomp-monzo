.class final Lco/uk/getmondo/topup/bank/b$d;
.super Ljava/lang/Object;
.source "TopUpInstructionsPresenter.kt"

# interfaces
.implements Lio/reactivex/c/g;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lco/uk/getmondo/topup/bank/b;->a(Lco/uk/getmondo/topup/bank/b$a;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/g",
        "<",
        "Lkotlin/n;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000\n\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0004\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0003*\u0004\u0018\u00010\u00010\u0001H\n\u00a2\u0006\u0004\u0008\u0004\u0010\u0005"
    }
    d2 = {
        "<anonymous>",
        "",
        "it",
        "kotlin.jvm.PlatformType",
        "accept",
        "(Lkotlin/Unit;)V"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field final synthetic a:Lco/uk/getmondo/topup/bank/b;

.field final synthetic b:Lco/uk/getmondo/d/a;

.field final synthetic c:Lco/uk/getmondo/d/ac;


# direct methods
.method constructor <init>(Lco/uk/getmondo/topup/bank/b;Lco/uk/getmondo/d/a;Lco/uk/getmondo/d/ac;)V
    .locals 0

    iput-object p1, p0, Lco/uk/getmondo/topup/bank/b$d;->a:Lco/uk/getmondo/topup/bank/b;

    iput-object p2, p0, Lco/uk/getmondo/topup/bank/b$d;->b:Lco/uk/getmondo/d/a;

    iput-object p3, p0, Lco/uk/getmondo/topup/bank/b$d;->c:Lco/uk/getmondo/d/ac;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 19
    check-cast p1, Lkotlin/n;

    invoke-virtual {p0, p1}, Lco/uk/getmondo/topup/bank/b$d;->a(Lkotlin/n;)V

    return-void
.end method

.method public final a(Lkotlin/n;)V
    .locals 6

    .prologue
    .line 48
    iget-object v0, p0, Lco/uk/getmondo/topup/bank/b$d;->a:Lco/uk/getmondo/topup/bank/b;

    invoke-static {v0}, Lco/uk/getmondo/topup/bank/b;->c(Lco/uk/getmondo/topup/bank/b;)Lco/uk/getmondo/common/a;

    move-result-object v0

    sget-object v1, Lco/uk/getmondo/api/model/tracking/Impression;->Companion:Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    invoke-virtual {v1}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->p()Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v1

    invoke-virtual {v0, v1}, Lco/uk/getmondo/common/a;->a(Lco/uk/getmondo/api/model/tracking/Impression;)V

    .line 49
    iget-object v0, p0, Lco/uk/getmondo/topup/bank/b$d;->b:Lco/uk/getmondo/d/a;

    instance-of v0, v0, Lco/uk/getmondo/d/ad;

    if-eqz v0, :cond_0

    .line 50
    iget-object v0, p0, Lco/uk/getmondo/topup/bank/b$d;->a:Lco/uk/getmondo/topup/bank/b;

    invoke-static {v0}, Lco/uk/getmondo/topup/bank/b;->a(Lco/uk/getmondo/topup/bank/b;)Lco/uk/getmondo/topup/bank/b$a;

    move-result-object v1

    iget-object v0, p0, Lco/uk/getmondo/topup/bank/b$d;->a:Lco/uk/getmondo/topup/bank/b;

    invoke-static {v0}, Lco/uk/getmondo/topup/bank/b;->d(Lco/uk/getmondo/topup/bank/b;)Lco/uk/getmondo/topup/ao;

    move-result-object v0

    invoke-virtual {v0}, Lco/uk/getmondo/topup/ao;->a()Ljava/lang/String;

    move-result-object v2

    const-string v0, "topUpStringProvider.instructionsTitle"

    invoke-static {v2, v0}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lco/uk/getmondo/topup/bank/b$d;->a:Lco/uk/getmondo/topup/bank/b;

    invoke-static {v0}, Lco/uk/getmondo/topup/bank/b;->d(Lco/uk/getmondo/topup/bank/b;)Lco/uk/getmondo/topup/ao;

    move-result-object v3

    .line 51
    iget-object v0, p0, Lco/uk/getmondo/topup/bank/b$d;->c:Lco/uk/getmondo/d/ac;

    invoke-virtual {v0}, Lco/uk/getmondo/d/ac;->c()Ljava/lang/String;

    move-result-object v4

    iget-object v0, p0, Lco/uk/getmondo/topup/bank/b$d;->b:Lco/uk/getmondo/d/a;

    check-cast v0, Lco/uk/getmondo/d/ad;

    invoke-virtual {v0}, Lco/uk/getmondo/d/ad;->j()Ljava/lang/String;

    move-result-object v5

    iget-object v0, p0, Lco/uk/getmondo/topup/bank/b$d;->b:Lco/uk/getmondo/d/a;

    check-cast v0, Lco/uk/getmondo/d/ad;

    invoke-virtual {v0}, Lco/uk/getmondo/d/ad;->i()Ljava/lang/String;

    move-result-object v0

    .line 50
    invoke-virtual {v3, v4, v5, v0}, Lco/uk/getmondo/topup/ao;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v3, "topUpStringProvider.getI\u2026Number, account.sortCode)"

    invoke-static {v0, v3}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v1, v2, v0}, Lco/uk/getmondo/topup/bank/b$a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 55
    :goto_0
    return-void

    .line 53
    :cond_0
    iget-object v0, p0, Lco/uk/getmondo/topup/bank/b$d;->a:Lco/uk/getmondo/topup/bank/b;

    invoke-static {v0}, Lco/uk/getmondo/topup/bank/b;->a(Lco/uk/getmondo/topup/bank/b;)Lco/uk/getmondo/topup/bank/b$a;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/topup/bank/b$d;->a:Lco/uk/getmondo/topup/bank/b;

    invoke-static {v1}, Lco/uk/getmondo/topup/bank/b;->d(Lco/uk/getmondo/topup/bank/b;)Lco/uk/getmondo/topup/ao;

    move-result-object v1

    invoke-virtual {v1}, Lco/uk/getmondo/topup/ao;->a()Ljava/lang/String;

    move-result-object v1

    const-string v2, "topUpStringProvider.instructionsTitle"

    invoke-static {v1, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v2, p0, Lco/uk/getmondo/topup/bank/b$d;->a:Lco/uk/getmondo/topup/bank/b;

    invoke-static {v2}, Lco/uk/getmondo/topup/bank/b;->d(Lco/uk/getmondo/topup/bank/b;)Lco/uk/getmondo/topup/ao;

    move-result-object v2

    .line 54
    iget-object v3, p0, Lco/uk/getmondo/topup/bank/b$d;->a:Lco/uk/getmondo/topup/bank/b;

    invoke-static {v3}, Lco/uk/getmondo/topup/bank/b;->b(Lco/uk/getmondo/topup/bank/b;)Ljava/lang/String;

    move-result-object v3

    .line 53
    invoke-virtual {v2, v3}, Lco/uk/getmondo/topup/ao;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "topUpStringProvider.getI\u2026               lastToken)"

    invoke-static {v2, v3}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v0, v1, v2}, Lco/uk/getmondo/topup/bank/b$a;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
