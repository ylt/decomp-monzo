.class public final Lco/uk/getmondo/topup/bank/TopUpInstructionsActivity;
.super Lco/uk/getmondo/common/activities/b;
.source "TopUpInstructionsActivity.kt"

# interfaces
.implements Lco/uk/getmondo/topup/bank/b$a;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lco/uk/getmondo/topup/bank/TopUpInstructionsActivity$a;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000P\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0006\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0002\u0008\t\u0018\u0000 \'2\u00020\u00012\u00020\u0002:\u0001\'B\u0005\u00a2\u0006\u0002\u0010\u0003J\u0012\u0010\u0012\u001a\u00020\u00062\u0008\u0010\u0013\u001a\u0004\u0018\u00010\u0014H\u0014J\u0010\u0010\u0015\u001a\u00020\u00162\u0006\u0010\u0017\u001a\u00020\u0018H\u0016J\u0008\u0010\u0019\u001a\u00020\u0006H\u0014J\u0010\u0010\u001a\u001a\u00020\u00162\u0006\u0010\u001b\u001a\u00020\u001cH\u0016J\u0018\u0010\u001d\u001a\u00020\u00062\u0006\u0010\u001e\u001a\u00020\u001f2\u0006\u0010 \u001a\u00020\u001fH\u0016J\u0010\u0010!\u001a\u00020\u00062\u0006\u0010\"\u001a\u00020\u001fH\u0016J \u0010#\u001a\u00020\u00062\u0006\u0010$\u001a\u00020\u001f2\u0006\u0010%\u001a\u00020\u001f2\u0006\u0010&\u001a\u00020\u001fH\u0016R\u001a\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0005X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0007\u0010\u0008R2\u0010\t\u001a&\u0012\u000c\u0012\n \u000b*\u0004\u0018\u00010\u00060\u0006 \u000b*\u0012\u0012\u000c\u0012\n \u000b*\u0004\u0018\u00010\u00060\u0006\u0018\u00010\n0\nX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001e\u0010\u000c\u001a\u00020\r8\u0006@\u0006X\u0087.\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u000e\u0010\u000f\"\u0004\u0008\u0010\u0010\u0011\u00a8\u0006("
    }
    d2 = {
        "Lco/uk/getmondo/topup/bank/TopUpInstructionsActivity;",
        "Lco/uk/getmondo/common/activities/BaseActivity;",
        "Lco/uk/getmondo/topup/bank/TopUpInstructionsPresenter$TopUpInstructionsView;",
        "()V",
        "shareClicks",
        "Lio/reactivex/Observable;",
        "",
        "getShareClicks",
        "()Lio/reactivex/Observable;",
        "shareClicksRelay",
        "Lcom/jakewharton/rxrelay2/PublishRelay;",
        "kotlin.jvm.PlatformType",
        "topUpInstructionsPresenter",
        "Lco/uk/getmondo/topup/bank/TopUpInstructionsPresenter;",
        "getTopUpInstructionsPresenter",
        "()Lco/uk/getmondo/topup/bank/TopUpInstructionsPresenter;",
        "setTopUpInstructionsPresenter",
        "(Lco/uk/getmondo/topup/bank/TopUpInstructionsPresenter;)V",
        "onCreate",
        "savedInstanceState",
        "Landroid/os/Bundle;",
        "onCreateOptionsMenu",
        "",
        "menu",
        "Landroid/view/Menu;",
        "onDestroy",
        "onOptionsItemSelected",
        "item",
        "Landroid/view/MenuItem;",
        "share",
        "subject",
        "",
        "text",
        "showMonzoDetails",
        "cardNumber",
        "showRetailCardDetails",
        "beneficiary",
        "accountNumber",
        "sortCode",
        "Companion",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# static fields
.field public static final b:Lco/uk/getmondo/topup/bank/TopUpInstructionsActivity$a;


# instance fields
.field public a:Lco/uk/getmondo/topup/bank/b;

.field private final c:Lcom/b/b/c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/b/b/c",
            "<",
            "Lkotlin/n;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Lio/reactivex/n;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/n",
            "<",
            "Lkotlin/n;",
            ">;"
        }
    .end annotation
.end field

.field private f:Ljava/util/HashMap;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lco/uk/getmondo/topup/bank/TopUpInstructionsActivity$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lco/uk/getmondo/topup/bank/TopUpInstructionsActivity$a;-><init>(Lkotlin/d/b/i;)V

    sput-object v0, Lco/uk/getmondo/topup/bank/TopUpInstructionsActivity;->b:Lco/uk/getmondo/topup/bank/TopUpInstructionsActivity$a;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 21
    invoke-direct {p0}, Lco/uk/getmondo/common/activities/b;-><init>()V

    .line 24
    invoke-static {}, Lcom/b/b/c;->a()Lcom/b/b/c;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/topup/bank/TopUpInstructionsActivity;->c:Lcom/b/b/c;

    .line 87
    iget-object v0, p0, Lco/uk/getmondo/topup/bank/TopUpInstructionsActivity;->c:Lcom/b/b/c;

    const-string v1, "shareClicksRelay"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lio/reactivex/n;

    iput-object v0, p0, Lco/uk/getmondo/topup/bank/TopUpInstructionsActivity;->e:Lio/reactivex/n;

    return-void
.end method

.method public static final a(Landroid/content/Context;)Landroid/content/Intent;
    .locals 1

    const-string v0, "context"

    invoke-static {p0, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v0, Lco/uk/getmondo/topup/bank/TopUpInstructionsActivity;->b:Lco/uk/getmondo/topup/bank/TopUpInstructionsActivity$a;

    invoke-virtual {v0, p0}, Lco/uk/getmondo/topup/bank/TopUpInstructionsActivity$a;->a(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public a(I)Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lco/uk/getmondo/topup/bank/TopUpInstructionsActivity;->f:Ljava/util/HashMap;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lco/uk/getmondo/topup/bank/TopUpInstructionsActivity;->f:Ljava/util/HashMap;

    :cond_0
    iget-object v0, p0, Lco/uk/getmondo/topup/bank/TopUpInstructionsActivity;->f:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    if-nez v0, :cond_1

    invoke-virtual {p0, p1}, Landroid/support/v4/app/j;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/topup/bank/TopUpInstructionsActivity;->f:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    return-object v0
.end method

.method public a()Lio/reactivex/n;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Lkotlin/n;",
            ">;"
        }
    .end annotation

    .prologue
    .line 87
    iget-object v0, p0, Lco/uk/getmondo/topup/bank/TopUpInstructionsActivity;->e:Lio/reactivex/n;

    return-object v0
.end method

.method public a(Ljava/lang/String;)V
    .locals 2

    .prologue
    const-string v0, "cardNumber"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 53
    sget v0, Lco/uk/getmondo/c$a;->topUpInstructionsTextView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/topup/bank/TopUpInstructionsActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/text/emoji/widget/EmojiTextView;

    const v1, 0x7f0a03eb

    invoke-virtual {p0, v1}, Lco/uk/getmondo/topup/bank/TopUpInstructionsActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Landroid/support/text/emoji/widget/EmojiTextView;->setText(Ljava/lang/CharSequence;)V

    .line 55
    sget v0, Lco/uk/getmondo/c$a;->topUpBeneficiaryLayout:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/topup/bank/TopUpInstructionsActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/common/ui/CopyableLinearLayout;

    invoke-static {v0}, Lco/uk/getmondo/common/ae;->a(Landroid/view/View;)V

    .line 56
    sget v0, Lco/uk/getmondo/c$a;->topUpBeneficiaryValue:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/topup/bank/TopUpInstructionsActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const v1, 0x7f0a03e5

    invoke-virtual {p0, v1}, Lco/uk/getmondo/topup/bank/TopUpInstructionsActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 58
    sget v0, Lco/uk/getmondo/c$a;->topUpAccountNumberLayout:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/topup/bank/TopUpInstructionsActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/common/ui/CopyableLinearLayout;

    invoke-static {v0}, Lco/uk/getmondo/common/ae;->a(Landroid/view/View;)V

    .line 59
    sget v0, Lco/uk/getmondo/c$a;->topUpAccountNumberValue:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/topup/bank/TopUpInstructionsActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const v1, 0x7f0a03e1

    invoke-virtual {p0, v1}, Lco/uk/getmondo/topup/bank/TopUpInstructionsActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 61
    sget v0, Lco/uk/getmondo/c$a;->topUpSortCodeLayout:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/topup/bank/TopUpInstructionsActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/common/ui/CopyableLinearLayout;

    invoke-static {v0}, Lco/uk/getmondo/common/ae;->a(Landroid/view/View;)V

    .line 62
    sget v0, Lco/uk/getmondo/c$a;->topUpSortCodeValue:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/topup/bank/TopUpInstructionsActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const v1, 0x7f0a03fe

    invoke-virtual {p0, v1}, Lco/uk/getmondo/topup/bank/TopUpInstructionsActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 64
    sget v0, Lco/uk/getmondo/c$a;->topUpReferenceLayout:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/topup/bank/TopUpInstructionsActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/common/ui/CopyableLinearLayout;

    invoke-static {v0}, Lco/uk/getmondo/common/ae;->a(Landroid/view/View;)V

    .line 65
    sget v0, Lco/uk/getmondo/c$a;->topUpReferenceValue:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/topup/bank/TopUpInstructionsActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    check-cast p1, Ljava/lang/CharSequence;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 66
    return-void
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    const-string v0, "subject"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "text"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v0, p0

    .line 49
    check-cast v0, Landroid/content/Context;

    sget-object v1, Lco/uk/getmondo/api/model/tracking/a;->TOP_UP_SHARE:Lco/uk/getmondo/api/model/tracking/a;

    invoke-static {v0, p1, p2, v1}, Lco/uk/getmondo/common/k/j;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lco/uk/getmondo/api/model/tracking/a;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lco/uk/getmondo/topup/bank/TopUpInstructionsActivity;->startActivity(Landroid/content/Intent;)V

    .line 50
    return-void
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 5

    .prologue
    const-string v0, "beneficiary"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "accountNumber"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "sortCode"

    invoke-static {p3, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 69
    const v0, 0x1f389

    invoke-static {v0}, Lco/uk/getmondo/common/k/e;->b(I)Ljava/lang/String;

    move-result-object v1

    .line 70
    sget v0, Lco/uk/getmondo/c$a;->topUpInstructionsTextView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/topup/bank/TopUpInstructionsActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/text/emoji/widget/EmojiTextView;

    const v2, 0x7f0a03fb

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v1, v3, v4

    invoke-virtual {p0, v2, v3}, Lco/uk/getmondo/topup/bank/TopUpInstructionsActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Landroid/support/text/emoji/widget/EmojiTextView;->setText(Ljava/lang/CharSequence;)V

    .line 72
    sget v0, Lco/uk/getmondo/c$a;->topUpBeneficiaryLayout:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/topup/bank/TopUpInstructionsActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/common/ui/CopyableLinearLayout;

    invoke-static {v0}, Lco/uk/getmondo/common/ae;->a(Landroid/view/View;)V

    .line 73
    sget v0, Lco/uk/getmondo/c$a;->topUpBeneficiaryValue:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/topup/bank/TopUpInstructionsActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    check-cast p1, Ljava/lang/CharSequence;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 75
    sget v0, Lco/uk/getmondo/c$a;->topUpAccountNumberLayout:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/topup/bank/TopUpInstructionsActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/common/ui/CopyableLinearLayout;

    invoke-static {v0}, Lco/uk/getmondo/common/ae;->a(Landroid/view/View;)V

    .line 76
    sget v0, Lco/uk/getmondo/c$a;->topUpAccountNumberValue:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/topup/bank/TopUpInstructionsActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    check-cast p2, Ljava/lang/CharSequence;

    invoke-virtual {v0, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 78
    sget v0, Lco/uk/getmondo/c$a;->topUpSortCodeLayout:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/topup/bank/TopUpInstructionsActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/common/ui/CopyableLinearLayout;

    invoke-static {v0}, Lco/uk/getmondo/common/ae;->a(Landroid/view/View;)V

    .line 79
    sget v0, Lco/uk/getmondo/c$a;->topUpSortCodeValue:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/topup/bank/TopUpInstructionsActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    check-cast p3, Ljava/lang/CharSequence;

    invoke-virtual {v0, p3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 80
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 27
    invoke-super {p0, p1}, Lco/uk/getmondo/common/activities/b;->onCreate(Landroid/os/Bundle;)V

    .line 28
    const v0, 0x7f05006e

    invoke-virtual {p0, v0}, Lco/uk/getmondo/topup/bank/TopUpInstructionsActivity;->setContentView(I)V

    .line 29
    invoke-virtual {p0}, Lco/uk/getmondo/topup/bank/TopUpInstructionsActivity;->l()Lco/uk/getmondo/common/h/b/b;

    move-result-object v0

    invoke-interface {v0, p0}, Lco/uk/getmondo/common/h/b/b;->a(Lco/uk/getmondo/topup/bank/TopUpInstructionsActivity;)V

    .line 30
    invoke-virtual {p0}, Lco/uk/getmondo/topup/bank/TopUpInstructionsActivity;->getSupportActionBar()Landroid/support/v7/app/a;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/support/v7/app/a;->b(Z)V

    .line 31
    :cond_0
    iget-object v0, p0, Lco/uk/getmondo/topup/bank/TopUpInstructionsActivity;->a:Lco/uk/getmondo/topup/bank/b;

    if-nez v0, :cond_1

    const-string v1, "topUpInstructionsPresenter"

    invoke-static {v1}, Lkotlin/d/b/l;->b(Ljava/lang/String;)V

    :cond_1
    check-cast p0, Lco/uk/getmondo/topup/bank/b$a;

    invoke-virtual {v0, p0}, Lco/uk/getmondo/topup/bank/b;->a(Lco/uk/getmondo/topup/bank/b$a;)V

    .line 32
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 2

    .prologue
    const-string v0, "menu"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 35
    invoke-virtual {p0}, Lco/uk/getmondo/topup/bank/TopUpInstructionsActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const v1, 0x7f130009

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 36
    const/4 v0, 0x1

    return v0
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    .line 83
    iget-object v0, p0, Lco/uk/getmondo/topup/bank/TopUpInstructionsActivity;->a:Lco/uk/getmondo/topup/bank/b;

    if-nez v0, :cond_0

    const-string v1, "topUpInstructionsPresenter"

    invoke-static {v1}, Lkotlin/d/b/l;->b(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0}, Lco/uk/getmondo/topup/bank/b;->b()V

    .line 84
    invoke-super {p0}, Lco/uk/getmondo/common/activities/b;->onDestroy()V

    .line 85
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2

    .prologue
    const-string v0, "item"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 40
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    .line 41
    const v1, 0x7f1104d8

    if-ne v0, v1, :cond_0

    .line 42
    iget-object v0, p0, Lco/uk/getmondo/topup/bank/TopUpInstructionsActivity;->c:Lcom/b/b/c;

    sget-object v1, Lkotlin/n;->a:Lkotlin/n;

    invoke-virtual {v0, v1}, Lcom/b/b/c;->a(Ljava/lang/Object;)V

    .line 43
    const/4 v0, 0x1

    .line 45
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Lco/uk/getmondo/common/activities/b;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    goto :goto_0
.end method
