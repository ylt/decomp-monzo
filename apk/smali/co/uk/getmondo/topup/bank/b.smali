.class public final Lco/uk/getmondo/topup/bank/b;
.super Lco/uk/getmondo/common/ui/b;
.source "TopUpInstructionsPresenter.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lco/uk/getmondo/topup/bank/b$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lco/uk/getmondo/common/ui/b",
        "<",
        "Lco/uk/getmondo/topup/bank/b$a;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u00006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0003\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00020\u0001:\u0001\u0011B\'\u0008\u0007\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u0006\u0010\u0007\u001a\u00020\u0008\u0012\u0006\u0010\t\u001a\u00020\n\u00a2\u0006\u0002\u0010\u000bJ\u0010\u0010\u000e\u001a\u00020\u000f2\u0006\u0010\u0010\u001a\u00020\u0002H\u0016R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0008X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u000c\u001a\u0004\u0018\u00010\rX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\nX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0012"
    }
    d2 = {
        "Lco/uk/getmondo/topup/bank/TopUpInstructionsPresenter;",
        "Lco/uk/getmondo/common/ui/BasePresenter;",
        "Lco/uk/getmondo/topup/bank/TopUpInstructionsPresenter$TopUpInstructionsView;",
        "accountService",
        "Lco/uk/getmondo/common/accounts/AccountService;",
        "analyticsService",
        "Lco/uk/getmondo/common/AnalyticsService;",
        "cardManager",
        "Lco/uk/getmondo/card/CardManager;",
        "topUpStringProvider",
        "Lco/uk/getmondo/topup/TopUpStringProvider;",
        "(Lco/uk/getmondo/common/accounts/AccountService;Lco/uk/getmondo/common/AnalyticsService;Lco/uk/getmondo/card/CardManager;Lco/uk/getmondo/topup/TopUpStringProvider;)V",
        "lastToken",
        "",
        "register",
        "",
        "topUpInstructionsView",
        "TopUpInstructionsView",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field private c:Ljava/lang/String;

.field private final d:Lco/uk/getmondo/common/accounts/d;

.field private final e:Lco/uk/getmondo/common/a;

.field private final f:Lco/uk/getmondo/card/c;

.field private final g:Lco/uk/getmondo/topup/ao;


# direct methods
.method public constructor <init>(Lco/uk/getmondo/common/accounts/d;Lco/uk/getmondo/common/a;Lco/uk/getmondo/card/c;Lco/uk/getmondo/topup/ao;)V
    .locals 1

    .prologue
    const-string v0, "accountService"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "analyticsService"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "cardManager"

    invoke-static {p3, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "topUpStringProvider"

    invoke-static {p4, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 24
    invoke-direct {p0}, Lco/uk/getmondo/common/ui/b;-><init>()V

    iput-object p1, p0, Lco/uk/getmondo/topup/bank/b;->d:Lco/uk/getmondo/common/accounts/d;

    iput-object p2, p0, Lco/uk/getmondo/topup/bank/b;->e:Lco/uk/getmondo/common/a;

    iput-object p3, p0, Lco/uk/getmondo/topup/bank/b;->f:Lco/uk/getmondo/card/c;

    iput-object p4, p0, Lco/uk/getmondo/topup/bank/b;->g:Lco/uk/getmondo/topup/ao;

    return-void
.end method

.method public static final synthetic a(Lco/uk/getmondo/topup/bank/b;)Lco/uk/getmondo/topup/bank/b$a;
    .locals 1

    .prologue
    .line 19
    iget-object v0, p0, Lco/uk/getmondo/topup/bank/b;->a:Lco/uk/getmondo/common/ui/f;

    check-cast v0, Lco/uk/getmondo/topup/bank/b$a;

    return-object v0
.end method

.method public static final synthetic a(Lco/uk/getmondo/topup/bank/b;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 19
    iput-object p1, p0, Lco/uk/getmondo/topup/bank/b;->c:Ljava/lang/String;

    return-void
.end method

.method public static final synthetic b(Lco/uk/getmondo/topup/bank/b;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 19
    iget-object v0, p0, Lco/uk/getmondo/topup/bank/b;->c:Ljava/lang/String;

    return-object v0
.end method

.method public static final synthetic c(Lco/uk/getmondo/topup/bank/b;)Lco/uk/getmondo/common/a;
    .locals 1

    .prologue
    .line 19
    iget-object v0, p0, Lco/uk/getmondo/topup/bank/b;->e:Lco/uk/getmondo/common/a;

    return-object v0
.end method

.method public static final synthetic d(Lco/uk/getmondo/topup/bank/b;)Lco/uk/getmondo/topup/ao;
    .locals 1

    .prologue
    .line 19
    iget-object v0, p0, Lco/uk/getmondo/topup/bank/b;->g:Lco/uk/getmondo/topup/ao;

    return-object v0
.end method


# virtual methods
.method public bridge synthetic a(Lco/uk/getmondo/common/ui/f;)V
    .locals 0

    .prologue
    .line 19
    check-cast p1, Lco/uk/getmondo/topup/bank/b$a;

    invoke-virtual {p0, p1}, Lco/uk/getmondo/topup/bank/b;->a(Lco/uk/getmondo/topup/bank/b$a;)V

    return-void
.end method

.method public a(Lco/uk/getmondo/topup/bank/b$a;)V
    .locals 6

    .prologue
    const-string v0, "topUpInstructionsView"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 28
    check-cast p1, Lco/uk/getmondo/common/ui/f;

    invoke-super {p0, p1}, Lco/uk/getmondo/common/ui/b;->a(Lco/uk/getmondo/common/ui/f;)V

    .line 30
    iget-object v0, p0, Lco/uk/getmondo/topup/bank/b;->d:Lco/uk/getmondo/common/accounts/d;

    invoke-virtual {v0}, Lco/uk/getmondo/common/accounts/d;->b()Lco/uk/getmondo/d/ak;

    move-result-object v0

    .line 31
    if-nez v0, :cond_0

    invoke-static {}, Lkotlin/d/b/l;->a()V

    :cond_0
    invoke-virtual {v0}, Lco/uk/getmondo/d/ak;->c()Lco/uk/getmondo/d/a;

    move-result-object v2

    if-nez v2, :cond_1

    invoke-static {}, Lkotlin/d/b/l;->a()V

    .line 32
    :cond_1
    invoke-virtual {v0}, Lco/uk/getmondo/d/ak;->d()Lco/uk/getmondo/d/ac;

    move-result-object v3

    if-nez v3, :cond_2

    invoke-static {}, Lkotlin/d/b/l;->a()V

    .line 34
    :cond_2
    instance-of v0, v2, Lco/uk/getmondo/d/ad;

    if-eqz v0, :cond_3

    .line 35
    iget-object v0, p0, Lco/uk/getmondo/topup/bank/b;->e:Lco/uk/getmondo/common/a;

    sget-object v1, Lco/uk/getmondo/api/model/tracking/Impression;->Companion:Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    invoke-virtual {v1}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->n()Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v1

    invoke-virtual {v0, v1}, Lco/uk/getmondo/common/a;->a(Lco/uk/getmondo/api/model/tracking/Impression;)V

    .line 36
    iget-object v0, p0, Lco/uk/getmondo/topup/bank/b;->a:Lco/uk/getmondo/common/ui/f;

    check-cast v0, Lco/uk/getmondo/topup/bank/b$a;

    invoke-virtual {v3}, Lco/uk/getmondo/d/ac;->c()Ljava/lang/String;

    move-result-object v4

    move-object v1, v2

    check-cast v1, Lco/uk/getmondo/d/ad;

    invoke-virtual {v1}, Lco/uk/getmondo/d/ad;->j()Ljava/lang/String;

    move-result-object v5

    move-object v1, v2

    check-cast v1, Lco/uk/getmondo/d/ad;

    invoke-virtual {v1}, Lco/uk/getmondo/d/ad;->i()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v4, v5, v1}, Lco/uk/getmondo/topup/bank/b$a;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 44
    :goto_0
    iget-object v1, p0, Lco/uk/getmondo/topup/bank/b;->b:Lio/reactivex/b/a;

    .line 47
    iget-object v0, p0, Lco/uk/getmondo/topup/bank/b;->a:Lco/uk/getmondo/common/ui/f;

    check-cast v0, Lco/uk/getmondo/topup/bank/b$a;

    invoke-interface {v0}, Lco/uk/getmondo/topup/bank/b$a;->a()Lio/reactivex/n;

    move-result-object v4

    new-instance v0, Lco/uk/getmondo/topup/bank/b$d;

    invoke-direct {v0, p0, v2, v3}, Lco/uk/getmondo/topup/bank/b$d;-><init>(Lco/uk/getmondo/topup/bank/b;Lco/uk/getmondo/d/a;Lco/uk/getmondo/d/ac;)V

    check-cast v0, Lio/reactivex/c/g;

    invoke-virtual {v4, v0}, Lio/reactivex/n;->subscribe(Lio/reactivex/c/g;)Lio/reactivex/b/b;

    move-result-object v0

    const-string v2, "view.shareClicks\n       \u2026      }\n                }"

    invoke-static {v0, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 46
    invoke-static {v1, v0}, Lco/uk/getmondo/common/j/f;->a(Lio/reactivex/b/a;Lio/reactivex/b/b;)Lio/reactivex/b/a;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/topup/bank/b;->b:Lio/reactivex/b/a;

    .line 57
    return-void

    .line 38
    :cond_3
    iget-object v4, p0, Lco/uk/getmondo/topup/bank/b;->b:Lio/reactivex/b/a;

    .line 39
    iget-object v0, p0, Lco/uk/getmondo/topup/bank/b;->f:Lco/uk/getmondo/card/c;

    invoke-virtual {v0}, Lco/uk/getmondo/card/c;->a()Lio/reactivex/n;

    move-result-object v5

    new-instance v0, Lco/uk/getmondo/topup/bank/b$b;

    invoke-direct {v0, p0}, Lco/uk/getmondo/topup/bank/b$b;-><init>(Lco/uk/getmondo/topup/bank/b;)V

    check-cast v0, Lio/reactivex/c/g;

    .line 43
    sget-object v1, Lco/uk/getmondo/topup/bank/b$c;->a:Lco/uk/getmondo/topup/bank/b$c;

    check-cast v1, Lio/reactivex/c/g;

    .line 39
    invoke-virtual {v5, v0, v1}, Lio/reactivex/n;->subscribe(Lio/reactivex/c/g;Lio/reactivex/c/g;)Lio/reactivex/b/b;

    move-result-object v0

    const-string v1, "cardManager.card()\n     \u2026     }, { Timber.e(it) })"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 38
    invoke-static {v4, v0}, Lco/uk/getmondo/common/j/f;->a(Lio/reactivex/b/a;Lio/reactivex/b/b;)Lio/reactivex/b/a;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/topup/bank/b;->b:Lio/reactivex/b/a;

    goto :goto_0
.end method
