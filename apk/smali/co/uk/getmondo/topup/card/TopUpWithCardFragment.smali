.class public Lco/uk/getmondo/topup/card/TopUpWithCardFragment;
.super Lco/uk/getmondo/common/f/a;
.source "TopUpWithCardFragment.java"

# interfaces
.implements Lco/uk/getmondo/topup/card/b$a;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lco/uk/getmondo/topup/card/TopUpWithCardFragment$a;
    }
.end annotation


# instance fields
.field a:Lco/uk/getmondo/topup/card/b;

.field billingPostcode:Landroid/widget/EditText;
    .annotation build Lbutterknife/BindView;
        value = 0x7f110355
    .end annotation
.end field

.field billingPostcodeWrapper:Landroid/support/design/widget/TextInputLayout;
    .annotation build Lbutterknife/BindView;
        value = 0x7f110354
    .end annotation
.end field

.field c:Lco/uk/getmondo/common/a;

.field cardHolderName:Landroid/widget/EditText;
    .annotation build Lbutterknife/BindView;
        value = 0x7f11034d
    .end annotation
.end field

.field cardHolderNameWrapper:Landroid/support/design/widget/TextInputLayout;
    .annotation build Lbutterknife/BindView;
        value = 0x7f11034c
    .end annotation
.end field

.field cardNumber:Landroid/widget/EditText;
    .annotation build Lbutterknife/BindView;
        value = 0x7f11034f
    .end annotation
.end field

.field cardNumberWrapper:Landroid/support/design/widget/TextInputLayout;
    .annotation build Lbutterknife/BindView;
        value = 0x7f11034e
    .end annotation
.end field

.field cvv:Landroid/widget/EditText;
    .annotation build Lbutterknife/BindView;
        value = 0x7f110353
    .end annotation
.end field

.field cvvWrapper:Landroid/support/design/widget/TextInputLayout;
    .annotation build Lbutterknife/BindView;
        value = 0x7f110352
    .end annotation
.end field

.field private d:Lco/uk/getmondo/d/c;

.field descriptionView:Landroid/widget/TextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f11012a
    .end annotation
.end field

.field private e:Landroid/app/ProgressDialog;

.field expiryDate:Landroid/widget/EditText;
    .annotation build Lbutterknife/BindView;
        value = 0x7f110351
    .end annotation
.end field

.field expiryDateWrapper:Landroid/support/design/widget/TextInputLayout;
    .annotation build Lbutterknife/BindView;
        value = 0x7f110350
    .end annotation
.end field

.field private f:Ljava/lang/Boolean;

.field private g:Lco/uk/getmondo/topup/three_d_secure/ThreeDsResolver;

.field private h:Lbutterknife/Unbinder;

.field private i:Lco/uk/getmondo/topup/card/TopUpWithCardFragment$a;

.field topUpButton:Landroid/widget/Button;
    .annotation build Lbutterknife/BindView;
        value = 0x7f11025b
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 38
    invoke-direct {p0}, Lco/uk/getmondo/common/f/a;-><init>()V

    return-void
.end method

.method public static a(Lco/uk/getmondo/d/c;Z)Landroid/support/v4/app/Fragment;
    .locals 3

    .prologue
    .line 71
    new-instance v0, Lco/uk/getmondo/topup/card/TopUpWithCardFragment;

    invoke-direct {v0}, Lco/uk/getmondo/topup/card/TopUpWithCardFragment;-><init>()V

    .line 72
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 73
    const-string v2, "ARG_AMOUNT"

    invoke-virtual {v1, v2, p0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 74
    const-string v2, "ARG_INITIAL_TOPUP"

    invoke-virtual {v1, v2, p1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 75
    invoke-virtual {v0, v1}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 76
    return-object v0
.end method

.method private h()V
    .locals 3

    .prologue
    .line 149
    new-instance v0, Lco/uk/getmondo/create_account/c;

    invoke-direct {v0}, Lco/uk/getmondo/create_account/c;-><init>()V

    .line 150
    iget-object v1, p0, Lco/uk/getmondo/topup/card/TopUpWithCardFragment;->expiryDate:Landroid/widget/EditText;

    new-instance v2, Lco/uk/getmondo/topup/card/TopUpWithCardFragment$1;

    invoke-direct {v2, p0, v0}, Lco/uk/getmondo/topup/card/TopUpWithCardFragment$1;-><init>(Lco/uk/getmondo/topup/card/TopUpWithCardFragment;Lco/uk/getmondo/create_account/c;)V

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 168
    return-void
.end method

.method private i()V
    .locals 2

    .prologue
    .line 171
    iget-object v0, p0, Lco/uk/getmondo/topup/card/TopUpWithCardFragment;->cardNumber:Landroid/widget/EditText;

    new-instance v1, Lco/uk/getmondo/topup/card/TopUpWithCardFragment$2;

    invoke-direct {v1, p0}, Lco/uk/getmondo/topup/card/TopUpWithCardFragment$2;-><init>(Lco/uk/getmondo/topup/card/TopUpWithCardFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 190
    return-void
.end method

.method private j()V
    .locals 2

    .prologue
    .line 193
    iget-object v0, p0, Lco/uk/getmondo/topup/card/TopUpWithCardFragment;->cvv:Landroid/widget/EditText;

    new-instance v1, Lco/uk/getmondo/topup/card/TopUpWithCardFragment$3;

    invoke-direct {v1, p0}, Lco/uk/getmondo/topup/card/TopUpWithCardFragment$3;-><init>(Lco/uk/getmondo/topup/card/TopUpWithCardFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 211
    return-void
.end method


# virtual methods
.method public a()V
    .locals 3

    .prologue
    .line 215
    iget-object v0, p0, Lco/uk/getmondo/topup/card/TopUpWithCardFragment;->cardNumberWrapper:Landroid/support/design/widget/TextInputLayout;

    invoke-virtual {p0}, Lco/uk/getmondo/topup/card/TopUpWithCardFragment;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0a03f5

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/design/widget/TextInputLayout;->setError(Ljava/lang/CharSequence;)V

    .line 216
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 270
    iget-object v0, p0, Lco/uk/getmondo/topup/card/TopUpWithCardFragment;->cardHolderName:Landroid/widget/EditText;

    invoke-virtual {v0, p1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 271
    iget-object v0, p0, Lco/uk/getmondo/topup/card/TopUpWithCardFragment;->cardNumber:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->requestFocus()Z

    .line 272
    return-void
.end method

.method public a(Z)V
    .locals 1

    .prologue
    .line 235
    iget-object v0, p0, Lco/uk/getmondo/topup/card/TopUpWithCardFragment;->topUpButton:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 236
    return-void
.end method

.method public a(Ljava/lang/Throwable;)Z
    .locals 1

    .prologue
    .line 265
    iget-object v0, p0, Lco/uk/getmondo/topup/card/TopUpWithCardFragment;->i:Lco/uk/getmondo/topup/card/TopUpWithCardFragment$a;

    invoke-interface {v0, p1}, Lco/uk/getmondo/topup/card/TopUpWithCardFragment$a;->a(Ljava/lang/Throwable;)Z

    move-result v0

    return v0
.end method

.method public b()V
    .locals 3

    .prologue
    .line 220
    iget-object v0, p0, Lco/uk/getmondo/topup/card/TopUpWithCardFragment;->expiryDateWrapper:Landroid/support/design/widget/TextInputLayout;

    invoke-virtual {p0}, Lco/uk/getmondo/topup/card/TopUpWithCardFragment;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0a03f7

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/design/widget/TextInputLayout;->setError(Ljava/lang/CharSequence;)V

    .line 221
    return-void
.end method

.method public c()V
    .locals 3

    .prologue
    .line 225
    iget-object v0, p0, Lco/uk/getmondo/topup/card/TopUpWithCardFragment;->cvvWrapper:Landroid/support/design/widget/TextInputLayout;

    invoke-virtual {p0}, Lco/uk/getmondo/topup/card/TopUpWithCardFragment;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0a03f6

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/design/widget/TextInputLayout;->setError(Ljava/lang/CharSequence;)V

    .line 226
    return-void
.end method

.method public d()V
    .locals 3

    .prologue
    .line 230
    iget-object v0, p0, Lco/uk/getmondo/topup/card/TopUpWithCardFragment;->billingPostcodeWrapper:Landroid/support/design/widget/TextInputLayout;

    invoke-virtual {p0}, Lco/uk/getmondo/topup/card/TopUpWithCardFragment;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0a03f8

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/design/widget/TextInputLayout;->setError(Ljava/lang/CharSequence;)V

    .line 231
    return-void
.end method

.method public e()V
    .locals 1

    .prologue
    .line 240
    iget-object v0, p0, Lco/uk/getmondo/topup/card/TopUpWithCardFragment;->i:Lco/uk/getmondo/topup/card/TopUpWithCardFragment$a;

    invoke-interface {v0}, Lco/uk/getmondo/topup/card/TopUpWithCardFragment$a;->b()V

    .line 241
    return-void
.end method

.method public f()V
    .locals 3

    .prologue
    .line 245
    iget-object v0, p0, Lco/uk/getmondo/topup/card/TopUpWithCardFragment;->e:Landroid/app/ProgressDialog;

    if-nez v0, :cond_0

    .line 246
    new-instance v0, Landroid/app/ProgressDialog;

    invoke-virtual {p0}, Lco/uk/getmondo/topup/card/TopUpWithCardFragment;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0c010e

    invoke-direct {v0, v1, v2}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lco/uk/getmondo/topup/card/TopUpWithCardFragment;->e:Landroid/app/ProgressDialog;

    .line 247
    iget-object v0, p0, Lco/uk/getmondo/topup/card/TopUpWithCardFragment;->e:Landroid/app/ProgressDialog;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    .line 248
    iget-object v0, p0, Lco/uk/getmondo/topup/card/TopUpWithCardFragment;->e:Landroid/app/ProgressDialog;

    invoke-virtual {p0}, Lco/uk/getmondo/topup/card/TopUpWithCardFragment;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0a03f9

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 251
    :cond_0
    iget-object v0, p0, Lco/uk/getmondo/topup/card/TopUpWithCardFragment;->e:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-nez v0, :cond_1

    .line 252
    iget-object v0, p0, Lco/uk/getmondo/topup/card/TopUpWithCardFragment;->e:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    .line 254
    :cond_1
    return-void
.end method

.method public g()V
    .locals 1

    .prologue
    .line 258
    iget-object v0, p0, Lco/uk/getmondo/topup/card/TopUpWithCardFragment;->e:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lco/uk/getmondo/topup/card/TopUpWithCardFragment;->e:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 259
    iget-object v0, p0, Lco/uk/getmondo/topup/card/TopUpWithCardFragment;->e:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 261
    :cond_0
    return-void
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 1

    .prologue
    .line 117
    iget-object v0, p0, Lco/uk/getmondo/topup/card/TopUpWithCardFragment;->g:Lco/uk/getmondo/topup/three_d_secure/ThreeDsResolver;

    invoke-interface {v0, p1, p2, p3}, Lco/uk/getmondo/topup/three_d_secure/ThreeDsResolver;->a(IILandroid/content/Intent;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 118
    invoke-super {p0, p1, p2, p3}, Lco/uk/getmondo/common/f/a;->onActivityResult(IILandroid/content/Intent;)V

    .line 120
    :cond_0
    return-void
.end method

.method public onAttach(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 81
    invoke-super {p0, p1}, Lco/uk/getmondo/common/f/a;->onAttach(Landroid/content/Context;)V

    .line 82
    check-cast p1, Lco/uk/getmondo/topup/card/TopUpWithCardFragment$a;

    iput-object p1, p0, Lco/uk/getmondo/topup/card/TopUpWithCardFragment;->i:Lco/uk/getmondo/topup/card/TopUpWithCardFragment$a;

    .line 83
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 87
    invoke-super {p0, p1}, Lco/uk/getmondo/common/f/a;->onCreate(Landroid/os/Bundle;)V

    .line 88
    invoke-virtual {p0}, Lco/uk/getmondo/topup/card/TopUpWithCardFragment;->B()Lco/uk/getmondo/common/h/b/b;

    move-result-object v0

    invoke-interface {v0, p0}, Lco/uk/getmondo/common/h/b/b;->a(Lco/uk/getmondo/topup/card/TopUpWithCardFragment;)V

    .line 89
    new-instance v0, Lco/uk/getmondo/topup/three_d_secure/b;

    iget-object v1, p0, Lco/uk/getmondo/topup/card/TopUpWithCardFragment;->c:Lco/uk/getmondo/common/a;

    invoke-direct {v0, p0, v1}, Lco/uk/getmondo/topup/three_d_secure/b;-><init>(Landroid/support/v4/app/Fragment;Lco/uk/getmondo/common/a;)V

    iput-object v0, p0, Lco/uk/getmondo/topup/card/TopUpWithCardFragment;->g:Lco/uk/getmondo/topup/three_d_secure/ThreeDsResolver;

    .line 90
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2

    .prologue
    .line 95
    const v0, 0x7f0500b0

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public onDestroyView()V
    .locals 1

    .prologue
    .line 124
    iget-object v0, p0, Lco/uk/getmondo/topup/card/TopUpWithCardFragment;->a:Lco/uk/getmondo/topup/card/b;

    invoke-virtual {v0}, Lco/uk/getmondo/topup/card/b;->b()V

    .line 125
    iget-object v0, p0, Lco/uk/getmondo/topup/card/TopUpWithCardFragment;->h:Lbutterknife/Unbinder;

    invoke-interface {v0}, Lbutterknife/Unbinder;->unbind()V

    .line 126
    invoke-super {p0}, Lco/uk/getmondo/common/f/a;->onDestroyView()V

    .line 127
    return-void
.end method

.method public onPostcodeTextChanged(Ljava/lang/CharSequence;)V
    .locals 2
    .annotation build Lbutterknife/OnTextChanged;
        value = {
            0x7f110355
        }
    .end annotation

    .prologue
    .line 145
    iget-object v0, p0, Lco/uk/getmondo/topup/card/TopUpWithCardFragment;->billingPostcodeWrapper:Landroid/support/design/widget/TextInputLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/design/widget/TextInputLayout;->setError(Ljava/lang/CharSequence;)V

    .line 146
    return-void
.end method

.method public onTopUpClicked()V
    .locals 10
    .annotation build Lbutterknife/OnClick;
        value = {
            0x7f11025b
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 131
    iget-object v0, p0, Lco/uk/getmondo/topup/card/TopUpWithCardFragment;->cardNumberWrapper:Landroid/support/design/widget/TextInputLayout;

    invoke-virtual {v0, v1}, Landroid/support/design/widget/TextInputLayout;->setError(Ljava/lang/CharSequence;)V

    .line 132
    iget-object v0, p0, Lco/uk/getmondo/topup/card/TopUpWithCardFragment;->expiryDateWrapper:Landroid/support/design/widget/TextInputLayout;

    invoke-virtual {v0, v1}, Landroid/support/design/widget/TextInputLayout;->setError(Ljava/lang/CharSequence;)V

    .line 133
    iget-object v0, p0, Lco/uk/getmondo/topup/card/TopUpWithCardFragment;->cvvWrapper:Landroid/support/design/widget/TextInputLayout;

    invoke-virtual {v0, v1}, Landroid/support/design/widget/TextInputLayout;->setError(Ljava/lang/CharSequence;)V

    .line 134
    iget-object v0, p0, Lco/uk/getmondo/topup/card/TopUpWithCardFragment;->cardHolderNameWrapper:Landroid/support/design/widget/TextInputLayout;

    invoke-virtual {v0, v1}, Landroid/support/design/widget/TextInputLayout;->setError(Ljava/lang/CharSequence;)V

    .line 135
    iget-object v0, p0, Lco/uk/getmondo/topup/card/TopUpWithCardFragment;->billingPostcodeWrapper:Landroid/support/design/widget/TextInputLayout;

    invoke-virtual {v0, v1}, Landroid/support/design/widget/TextInputLayout;->setError(Ljava/lang/CharSequence;)V

    .line 136
    iget-object v0, p0, Lco/uk/getmondo/topup/card/TopUpWithCardFragment;->a:Lco/uk/getmondo/topup/card/b;

    iget-object v1, p0, Lco/uk/getmondo/topup/card/TopUpWithCardFragment;->cardHolderName:Landroid/widget/EditText;

    .line 137
    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lco/uk/getmondo/topup/card/TopUpWithCardFragment;->cardNumber:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lco/uk/getmondo/topup/card/TopUpWithCardFragment;->expiryDate:Landroid/widget/EditText;

    invoke-virtual {v3}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lco/uk/getmondo/topup/card/TopUpWithCardFragment;->cvv:Landroid/widget/EditText;

    .line 138
    invoke-virtual {v4}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lco/uk/getmondo/topup/card/TopUpWithCardFragment;->billingPostcode:Landroid/widget/EditText;

    invoke-virtual {v5}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lco/uk/getmondo/topup/card/TopUpWithCardFragment;->d:Lco/uk/getmondo/d/c;

    iget-object v7, p0, Lco/uk/getmondo/topup/card/TopUpWithCardFragment;->f:Ljava/lang/Boolean;

    .line 139
    invoke-virtual {v7}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v7

    const/4 v8, 0x1

    iget-object v9, p0, Lco/uk/getmondo/topup/card/TopUpWithCardFragment;->g:Lco/uk/getmondo/topup/three_d_secure/ThreeDsResolver;

    .line 137
    invoke-virtual/range {v0 .. v9}, Lco/uk/getmondo/topup/card/b;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lco/uk/getmondo/d/c;ZZLco/uk/getmondo/topup/three_d_secure/ThreeDsResolver;)V

    .line 141
    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 100
    invoke-super {p0, p1, p2}, Lco/uk/getmondo/common/f/a;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 101
    invoke-static {p0, p1}, Lbutterknife/ButterKnife;->bind(Ljava/lang/Object;Landroid/view/View;)Lbutterknife/Unbinder;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/topup/card/TopUpWithCardFragment;->h:Lbutterknife/Unbinder;

    .line 102
    iget-object v0, p0, Lco/uk/getmondo/topup/card/TopUpWithCardFragment;->a:Lco/uk/getmondo/topup/card/b;

    invoke-virtual {v0, p0}, Lco/uk/getmondo/topup/card/b;->a(Lco/uk/getmondo/topup/card/b$a;)V

    .line 103
    invoke-virtual {p0}, Lco/uk/getmondo/topup/card/TopUpWithCardFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "ARG_AMOUNT"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/d/c;

    iput-object v0, p0, Lco/uk/getmondo/topup/card/TopUpWithCardFragment;->d:Lco/uk/getmondo/d/c;

    .line 104
    invoke-virtual {p0}, Lco/uk/getmondo/topup/card/TopUpWithCardFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "ARG_INITIAL_TOPUP"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/topup/card/TopUpWithCardFragment;->f:Ljava/lang/Boolean;

    .line 106
    iget-object v0, p0, Lco/uk/getmondo/topup/card/TopUpWithCardFragment;->d:Lco/uk/getmondo/d/c;

    invoke-virtual {v0}, Lco/uk/getmondo/d/c;->toString()Ljava/lang/String;

    move-result-object v0

    .line 107
    iget-object v1, p0, Lco/uk/getmondo/topup/card/TopUpWithCardFragment;->descriptionView:Landroid/widget/TextView;

    invoke-virtual {p0}, Lco/uk/getmondo/topup/card/TopUpWithCardFragment;->getContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f0a0405

    new-array v4, v6, [Ljava/lang/Object;

    aput-object v0, v4, v5

    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 108
    iget-object v1, p0, Lco/uk/getmondo/topup/card/TopUpWithCardFragment;->topUpButton:Landroid/widget/Button;

    invoke-virtual {p0}, Lco/uk/getmondo/topup/card/TopUpWithCardFragment;->getContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f0a03e6

    new-array v4, v6, [Ljava/lang/Object;

    aput-object v0, v4, v5

    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 110
    invoke-direct {p0}, Lco/uk/getmondo/topup/card/TopUpWithCardFragment;->h()V

    .line 111
    invoke-direct {p0}, Lco/uk/getmondo/topup/card/TopUpWithCardFragment;->i()V

    .line 112
    invoke-direct {p0}, Lco/uk/getmondo/topup/card/TopUpWithCardFragment;->j()V

    .line 113
    return-void
.end method
