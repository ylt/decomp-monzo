.class Lco/uk/getmondo/topup/card/TopUpWithCardFragment$2;
.super Ljava/lang/Object;
.source "TopUpWithCardFragment.java"

# interfaces
.implements Landroid/text/TextWatcher;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lco/uk/getmondo/topup/card/TopUpWithCardFragment;->i()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lco/uk/getmondo/topup/card/TopUpWithCardFragment;


# direct methods
.method constructor <init>(Lco/uk/getmondo/topup/card/TopUpWithCardFragment;)V
    .locals 0

    .prologue
    .line 171
    iput-object p1, p0, Lco/uk/getmondo/topup/card/TopUpWithCardFragment$2;->a:Lco/uk/getmondo/topup/card/TopUpWithCardFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 2

    .prologue
    .line 185
    iget-object v0, p0, Lco/uk/getmondo/topup/card/TopUpWithCardFragment$2;->a:Lco/uk/getmondo/topup/card/TopUpWithCardFragment;

    iget-object v0, v0, Lco/uk/getmondo/topup/card/TopUpWithCardFragment;->cardNumber:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->length()I

    move-result v0

    const/16 v1, 0x13

    if-ne v0, v1, :cond_0

    .line 186
    iget-object v0, p0, Lco/uk/getmondo/topup/card/TopUpWithCardFragment$2;->a:Lco/uk/getmondo/topup/card/TopUpWithCardFragment;

    iget-object v0, v0, Lco/uk/getmondo/topup/card/TopUpWithCardFragment;->expiryDate:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->requestFocus()Z

    .line 188
    :cond_0
    return-void
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 175
    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 2

    .prologue
    .line 179
    iget-object v0, p0, Lco/uk/getmondo/topup/card/TopUpWithCardFragment$2;->a:Lco/uk/getmondo/topup/card/TopUpWithCardFragment;

    iget-object v0, v0, Lco/uk/getmondo/topup/card/TopUpWithCardFragment;->cardNumberWrapper:Landroid/support/design/widget/TextInputLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/design/widget/TextInputLayout;->setError(Ljava/lang/CharSequence;)V

    .line 180
    iget-object v0, p0, Lco/uk/getmondo/topup/card/TopUpWithCardFragment$2;->a:Lco/uk/getmondo/topup/card/TopUpWithCardFragment;

    iget-object v0, v0, Lco/uk/getmondo/topup/card/TopUpWithCardFragment;->cardNumber:Landroid/widget/EditText;

    invoke-static {p1, p4, v0, p0}, Lco/uk/getmondo/create_account/a;->a(Ljava/lang/CharSequence;ILandroid/widget/EditText;Landroid/text/TextWatcher;)V

    .line 181
    return-void
.end method
