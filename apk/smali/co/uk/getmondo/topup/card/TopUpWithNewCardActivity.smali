.class public Lco/uk/getmondo/topup/card/TopUpWithNewCardActivity;
.super Lco/uk/getmondo/common/activities/b;
.source "TopUpWithNewCardActivity.java"

# interfaces
.implements Lco/uk/getmondo/topup/card/TopUpWithCardFragment$a;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Lco/uk/getmondo/common/activities/b;-><init>()V

    return-void
.end method

.method public static a(Landroid/content/Context;Lco/uk/getmondo/d/c;)V
    .locals 2

    .prologue
    .line 17
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lco/uk/getmondo/topup/card/TopUpWithNewCardActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 18
    const-string v1, "EXTRA_TOPUP_AMOUNT"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 19
    invoke-virtual {p0, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 20
    return-void
.end method


# virtual methods
.method public a(Ljava/lang/Throwable;)Z
    .locals 1

    .prologue
    .line 48
    const/4 v0, 0x0

    return v0
.end method

.method public b()V
    .locals 1

    .prologue
    .line 43
    invoke-static {p0}, Lco/uk/getmondo/main/HomeActivity;->b(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    invoke-static {p0, v0}, Lco/uk/getmondo/common/activities/ConfirmationActivity;->a(Landroid/content/Context;Landroid/content/Intent;)V

    .line 44
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 6

    .prologue
    const v5, 0x1020002

    const/4 v4, 0x0

    .line 24
    invoke-super {p0, p1}, Lco/uk/getmondo/common/activities/b;->onCreate(Landroid/os/Bundle;)V

    .line 25
    invoke-virtual {p0}, Lco/uk/getmondo/topup/card/TopUpWithNewCardActivity;->l()Lco/uk/getmondo/common/h/b/b;

    move-result-object v0

    invoke-interface {v0, p0}, Lco/uk/getmondo/common/h/b/b;->a(Lco/uk/getmondo/topup/card/TopUpWithNewCardActivity;)V

    .line 26
    invoke-virtual {p0}, Lco/uk/getmondo/topup/card/TopUpWithNewCardActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "EXTRA_TOPUP_AMOUNT"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/d/c;

    .line 27
    if-nez v0, :cond_0

    .line 28
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Top up amount required"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 31
    :cond_0
    const v1, 0x7f0a0404

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0}, Lco/uk/getmondo/d/c;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-virtual {p0, v1, v2}, Lco/uk/getmondo/topup/card/TopUpWithNewCardActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lco/uk/getmondo/topup/card/TopUpWithNewCardActivity;->setTitle(Ljava/lang/CharSequence;)V

    .line 33
    invoke-virtual {p0}, Lco/uk/getmondo/topup/card/TopUpWithNewCardActivity;->getSupportFragmentManager()Landroid/support/v4/app/n;

    move-result-object v1

    invoke-virtual {v1, v5}, Landroid/support/v4/app/n;->a(I)Landroid/support/v4/app/Fragment;

    move-result-object v1

    if-nez v1, :cond_1

    .line 34
    invoke-virtual {p0}, Lco/uk/getmondo/topup/card/TopUpWithNewCardActivity;->getSupportFragmentManager()Landroid/support/v4/app/n;

    move-result-object v1

    .line 35
    invoke-virtual {v1}, Landroid/support/v4/app/n;->a()Landroid/support/v4/app/t;

    move-result-object v1

    .line 36
    invoke-static {v0, v4}, Lco/uk/getmondo/topup/card/TopUpWithCardFragment;->a(Lco/uk/getmondo/d/c;Z)Landroid/support/v4/app/Fragment;

    move-result-object v0

    invoke-virtual {v1, v5, v0}, Landroid/support/v4/app/t;->b(ILandroid/support/v4/app/Fragment;)Landroid/support/v4/app/t;

    move-result-object v0

    .line 37
    invoke-virtual {v0}, Landroid/support/v4/app/t;->c()I

    .line 39
    :cond_1
    return-void
.end method
