.class public Lco/uk/getmondo/topup/card/TopUpWithSavedCardActivity_ViewBinding;
.super Ljava/lang/Object;
.source "TopUpWithSavedCardActivity_ViewBinding.java"

# interfaces
.implements Lbutterknife/Unbinder;


# instance fields
.field private a:Lco/uk/getmondo/topup/card/TopUpWithSavedCardActivity;

.field private b:Landroid/view/View;

.field private c:Landroid/view/View;


# direct methods
.method public constructor <init>(Lco/uk/getmondo/topup/card/TopUpWithSavedCardActivity;Landroid/view/View;)V
    .locals 4

    .prologue
    const v3, 0x7f11025b

    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    iput-object p1, p0, Lco/uk/getmondo/topup/card/TopUpWithSavedCardActivity_ViewBinding;->a:Lco/uk/getmondo/topup/card/TopUpWithSavedCardActivity;

    .line 33
    const v0, 0x7f11025c

    const-string v1, "field \'savedCardView\'"

    const-class v2, Lco/uk/getmondo/topup/card/SavedCardView;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/topup/card/SavedCardView;

    iput-object v0, p1, Lco/uk/getmondo/topup/card/TopUpWithSavedCardActivity;->savedCardView:Lco/uk/getmondo/topup/card/SavedCardView;

    .line 34
    const v0, 0x7f11012a

    const-string v1, "field \'descriptionView\'"

    const-class v2, Landroid/widget/TextView;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p1, Lco/uk/getmondo/topup/card/TopUpWithSavedCardActivity;->descriptionView:Landroid/widget/TextView;

    .line 35
    const-string v0, "field \'topUpButton\' and method \'topUpClicked\'"

    invoke-static {p2, v3, v0}, Lbutterknife/internal/Utils;->findRequiredView(Landroid/view/View;ILjava/lang/String;)Landroid/view/View;

    move-result-object v1

    .line 36
    const-string v0, "field \'topUpButton\'"

    const-class v2, Landroid/widget/Button;

    invoke-static {v1, v3, v0, v2}, Lbutterknife/internal/Utils;->castView(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p1, Lco/uk/getmondo/topup/card/TopUpWithSavedCardActivity;->topUpButton:Landroid/widget/Button;

    .line 37
    iput-object v1, p0, Lco/uk/getmondo/topup/card/TopUpWithSavedCardActivity_ViewBinding;->b:Landroid/view/View;

    .line 38
    new-instance v0, Lco/uk/getmondo/topup/card/TopUpWithSavedCardActivity_ViewBinding$1;

    invoke-direct {v0, p0, p1}, Lco/uk/getmondo/topup/card/TopUpWithSavedCardActivity_ViewBinding$1;-><init>(Lco/uk/getmondo/topup/card/TopUpWithSavedCardActivity_ViewBinding;Lco/uk/getmondo/topup/card/TopUpWithSavedCardActivity;)V

    invoke-virtual {v1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 44
    const v0, 0x7f11025d

    const-string v1, "method \'onAddNewCardClicked\'"

    invoke-static {p2, v0, v1}, Lbutterknife/internal/Utils;->findRequiredView(Landroid/view/View;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 45
    iput-object v0, p0, Lco/uk/getmondo/topup/card/TopUpWithSavedCardActivity_ViewBinding;->c:Landroid/view/View;

    .line 46
    new-instance v1, Lco/uk/getmondo/topup/card/TopUpWithSavedCardActivity_ViewBinding$2;

    invoke-direct {v1, p0, p1}, Lco/uk/getmondo/topup/card/TopUpWithSavedCardActivity_ViewBinding$2;-><init>(Lco/uk/getmondo/topup/card/TopUpWithSavedCardActivity_ViewBinding;Lco/uk/getmondo/topup/card/TopUpWithSavedCardActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 52
    return-void
.end method


# virtual methods
.method public unbind()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 57
    iget-object v0, p0, Lco/uk/getmondo/topup/card/TopUpWithSavedCardActivity_ViewBinding;->a:Lco/uk/getmondo/topup/card/TopUpWithSavedCardActivity;

    .line 58
    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Bindings already cleared."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 59
    :cond_0
    iput-object v1, p0, Lco/uk/getmondo/topup/card/TopUpWithSavedCardActivity_ViewBinding;->a:Lco/uk/getmondo/topup/card/TopUpWithSavedCardActivity;

    .line 61
    iput-object v1, v0, Lco/uk/getmondo/topup/card/TopUpWithSavedCardActivity;->savedCardView:Lco/uk/getmondo/topup/card/SavedCardView;

    .line 62
    iput-object v1, v0, Lco/uk/getmondo/topup/card/TopUpWithSavedCardActivity;->descriptionView:Landroid/widget/TextView;

    .line 63
    iput-object v1, v0, Lco/uk/getmondo/topup/card/TopUpWithSavedCardActivity;->topUpButton:Landroid/widget/Button;

    .line 65
    iget-object v0, p0, Lco/uk/getmondo/topup/card/TopUpWithSavedCardActivity_ViewBinding;->b:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 66
    iput-object v1, p0, Lco/uk/getmondo/topup/card/TopUpWithSavedCardActivity_ViewBinding;->b:Landroid/view/View;

    .line 67
    iget-object v0, p0, Lco/uk/getmondo/topup/card/TopUpWithSavedCardActivity_ViewBinding;->c:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 68
    iput-object v1, p0, Lco/uk/getmondo/topup/card/TopUpWithSavedCardActivity_ViewBinding;->c:Landroid/view/View;

    .line 69
    return-void
.end method
