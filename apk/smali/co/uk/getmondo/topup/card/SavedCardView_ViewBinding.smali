.class public Lco/uk/getmondo/topup/card/SavedCardView_ViewBinding;
.super Ljava/lang/Object;
.source "SavedCardView_ViewBinding.java"

# interfaces
.implements Lbutterknife/Unbinder;


# instance fields
.field private a:Lco/uk/getmondo/topup/card/SavedCardView;


# direct methods
.method public constructor <init>(Lco/uk/getmondo/topup/card/SavedCardView;Landroid/view/View;)V
    .locals 3

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    iput-object p1, p0, Lco/uk/getmondo/topup/card/SavedCardView_ViewBinding;->a:Lco/uk/getmondo/topup/card/SavedCardView;

    .line 27
    const v0, 0x7f1104bb

    const-string v1, "field \'cardImageView\'"

    const-class v2, Landroid/widget/ImageView;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p1, Lco/uk/getmondo/topup/card/SavedCardView;->cardImageView:Landroid/widget/ImageView;

    .line 28
    const v0, 0x7f11034d

    const-string v1, "field \'cardHolderNameView\'"

    const-class v2, Landroid/widget/TextView;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p1, Lco/uk/getmondo/topup/card/SavedCardView;->cardHolderNameView:Landroid/widget/TextView;

    .line 29
    const v0, 0x7f1104bc

    const-string v1, "field \'cardLastFourView\'"

    const-class v2, Landroid/widget/TextView;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p1, Lco/uk/getmondo/topup/card/SavedCardView;->cardLastFourView:Landroid/widget/TextView;

    .line 30
    const v0, 0x7f1104bd

    const-string v1, "field \'cardExpiryView\'"

    const-class v2, Landroid/widget/TextView;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p1, Lco/uk/getmondo/topup/card/SavedCardView;->cardExpiryView:Landroid/widget/TextView;

    .line 31
    return-void
.end method


# virtual methods
.method public unbind()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 36
    iget-object v0, p0, Lco/uk/getmondo/topup/card/SavedCardView_ViewBinding;->a:Lco/uk/getmondo/topup/card/SavedCardView;

    .line 37
    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Bindings already cleared."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 38
    :cond_0
    iput-object v1, p0, Lco/uk/getmondo/topup/card/SavedCardView_ViewBinding;->a:Lco/uk/getmondo/topup/card/SavedCardView;

    .line 40
    iput-object v1, v0, Lco/uk/getmondo/topup/card/SavedCardView;->cardImageView:Landroid/widget/ImageView;

    .line 41
    iput-object v1, v0, Lco/uk/getmondo/topup/card/SavedCardView;->cardHolderNameView:Landroid/widget/TextView;

    .line 42
    iput-object v1, v0, Lco/uk/getmondo/topup/card/SavedCardView;->cardLastFourView:Landroid/widget/TextView;

    .line 43
    iput-object v1, v0, Lco/uk/getmondo/topup/card/SavedCardView;->cardExpiryView:Landroid/widget/TextView;

    .line 44
    return-void
.end method
