.class Lco/uk/getmondo/topup/card/TopUpWithCardFragment$1;
.super Ljava/lang/Object;
.source "TopUpWithCardFragment.java"

# interfaces
.implements Landroid/text/TextWatcher;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lco/uk/getmondo/topup/card/TopUpWithCardFragment;->h()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lco/uk/getmondo/create_account/c;

.field final synthetic b:Lco/uk/getmondo/topup/card/TopUpWithCardFragment;


# direct methods
.method constructor <init>(Lco/uk/getmondo/topup/card/TopUpWithCardFragment;Lco/uk/getmondo/create_account/c;)V
    .locals 0

    .prologue
    .line 150
    iput-object p1, p0, Lco/uk/getmondo/topup/card/TopUpWithCardFragment$1;->b:Lco/uk/getmondo/topup/card/TopUpWithCardFragment;

    iput-object p2, p0, Lco/uk/getmondo/topup/card/TopUpWithCardFragment$1;->a:Lco/uk/getmondo/create_account/c;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 2

    .prologue
    .line 163
    iget-object v0, p0, Lco/uk/getmondo/topup/card/TopUpWithCardFragment$1;->b:Lco/uk/getmondo/topup/card/TopUpWithCardFragment;

    iget-object v0, v0, Lco/uk/getmondo/topup/card/TopUpWithCardFragment;->expiryDate:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->length()I

    move-result v0

    const/4 v1, 0x7

    if-ne v0, v1, :cond_0

    .line 164
    iget-object v0, p0, Lco/uk/getmondo/topup/card/TopUpWithCardFragment$1;->b:Lco/uk/getmondo/topup/card/TopUpWithCardFragment;

    iget-object v0, v0, Lco/uk/getmondo/topup/card/TopUpWithCardFragment;->cvv:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->requestFocus()Z

    .line 166
    :cond_0
    return-void
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 153
    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 6

    .prologue
    .line 157
    iget-object v0, p0, Lco/uk/getmondo/topup/card/TopUpWithCardFragment$1;->b:Lco/uk/getmondo/topup/card/TopUpWithCardFragment;

    iget-object v0, v0, Lco/uk/getmondo/topup/card/TopUpWithCardFragment;->expiryDateWrapper:Landroid/support/design/widget/TextInputLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/design/widget/TextInputLayout;->setError(Ljava/lang/CharSequence;)V

    .line 158
    iget-object v0, p0, Lco/uk/getmondo/topup/card/TopUpWithCardFragment$1;->a:Lco/uk/getmondo/create_account/c;

    iget-object v1, p0, Lco/uk/getmondo/topup/card/TopUpWithCardFragment$1;->b:Lco/uk/getmondo/topup/card/TopUpWithCardFragment;

    iget-object v1, v1, Lco/uk/getmondo/topup/card/TopUpWithCardFragment;->expiryDateWrapper:Landroid/support/design/widget/TextInputLayout;

    invoke-virtual {v1}, Landroid/support/design/widget/TextInputLayout;->getEditText()Landroid/widget/EditText;

    move-result-object v5

    move-object v1, p1

    move v2, p3

    move v3, p4

    move-object v4, p0

    invoke-virtual/range {v0 .. v5}, Lco/uk/getmondo/create_account/c;->a(Ljava/lang/CharSequence;IILandroid/text/TextWatcher;Landroid/widget/EditText;)V

    .line 159
    return-void
.end method
