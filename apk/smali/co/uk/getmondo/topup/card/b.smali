.class public Lco/uk/getmondo/topup/card/b;
.super Lco/uk/getmondo/common/ui/b;
.source "TopUpWithCardPresenter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lco/uk/getmondo/topup/card/b$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lco/uk/getmondo/common/ui/b",
        "<",
        "Lco/uk/getmondo/topup/card/b$a;",
        ">;"
    }
.end annotation


# static fields
.field private static final c:Ljava/text/SimpleDateFormat;


# instance fields
.field private final d:Lio/reactivex/u;

.field private final e:Lio/reactivex/u;

.field private final f:Lco/uk/getmondo/common/e/a;

.field private final g:Lco/uk/getmondo/common/accounts/d;

.field private final h:Lco/uk/getmondo/topup/a/c;

.field private final i:Lco/uk/getmondo/topup/a/w;

.field private final j:Lco/uk/getmondo/common/a;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 37
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "MM/yy"

    sget-object v2, Ljava/util/Locale;->UK:Ljava/util/Locale;

    invoke-direct {v0, v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    sput-object v0, Lco/uk/getmondo/topup/card/b;->c:Ljava/text/SimpleDateFormat;

    return-void
.end method

.method constructor <init>(Lio/reactivex/u;Lio/reactivex/u;Lco/uk/getmondo/common/e/a;Lco/uk/getmondo/common/accounts/d;Lco/uk/getmondo/common/a;Lco/uk/getmondo/topup/a/c;Lco/uk/getmondo/topup/a/w;)V
    .locals 0

    .prologue
    .line 49
    invoke-direct {p0}, Lco/uk/getmondo/common/ui/b;-><init>()V

    .line 50
    iput-object p1, p0, Lco/uk/getmondo/topup/card/b;->d:Lio/reactivex/u;

    .line 51
    iput-object p2, p0, Lco/uk/getmondo/topup/card/b;->e:Lio/reactivex/u;

    .line 52
    iput-object p3, p0, Lco/uk/getmondo/topup/card/b;->f:Lco/uk/getmondo/common/e/a;

    .line 53
    iput-object p4, p0, Lco/uk/getmondo/topup/card/b;->g:Lco/uk/getmondo/common/accounts/d;

    .line 54
    iput-object p6, p0, Lco/uk/getmondo/topup/card/b;->h:Lco/uk/getmondo/topup/a/c;

    .line 55
    iput-object p7, p0, Lco/uk/getmondo/topup/card/b;->i:Lco/uk/getmondo/topup/a/w;

    .line 56
    iput-object p5, p0, Lco/uk/getmondo/topup/card/b;->j:Lco/uk/getmondo/common/a;

    .line 57
    return-void
.end method

.method static synthetic a(Lco/uk/getmondo/topup/card/b;Lcom/stripe/android/model/Card;ZLco/uk/getmondo/d/c;ZLco/uk/getmondo/topup/three_d_secure/ThreeDsResolver;Ljava/lang/String;)Lio/reactivex/d;
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 103
    iget-object v0, p0, Lco/uk/getmondo/topup/card/b;->h:Lco/uk/getmondo/topup/a/c;

    move-object v1, p6

    move-object v2, p1

    move v3, p2

    move-object v4, p3

    move v5, p4

    move-object v6, p5

    invoke-virtual/range {v0 .. v6}, Lco/uk/getmondo/topup/a/c;->a(Ljava/lang/String;Lcom/stripe/android/model/Card;ZLco/uk/getmondo/d/c;ZLco/uk/getmondo/topup/three_d_secure/ThreeDsResolver;)Lio/reactivex/b;

    move-result-object v0

    return-object v0
.end method

.method private a(Ljava/lang/String;)Ljava/util/Calendar;
    .locals 2

    .prologue
    .line 158
    :try_start_0
    sget-object v0, Lco/uk/getmondo/topup/card/b;->c:Ljava/text/SimpleDateFormat;

    invoke-virtual {v0, p1}, Ljava/text/SimpleDateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v1

    .line 159
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 160
    invoke-virtual {v0, v1}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V
    :try_end_0
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_0} :catch_0

    .line 163
    :goto_0
    return-object v0

    .line 162
    :catch_0
    move-exception v0

    .line 163
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a()V
    .locals 2

    .prologue
    .line 122
    iget-object v0, p0, Lco/uk/getmondo/topup/card/b;->j:Lco/uk/getmondo/common/a;

    sget-object v1, Lco/uk/getmondo/api/model/tracking/Impression$TopUpSuccessType;->NEW_CARD:Lco/uk/getmondo/api/model/tracking/Impression$TopUpSuccessType;

    invoke-static {v1}, Lco/uk/getmondo/api/model/tracking/Impression;->a(Lco/uk/getmondo/api/model/tracking/Impression$TopUpSuccessType;)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v1

    invoke-virtual {v0, v1}, Lco/uk/getmondo/common/a;->a(Lco/uk/getmondo/api/model/tracking/Impression;)V

    .line 124
    iget-object v0, p0, Lco/uk/getmondo/topup/card/b;->a:Lco/uk/getmondo/common/ui/f;

    check-cast v0, Lco/uk/getmondo/topup/card/b$a;

    invoke-interface {v0}, Lco/uk/getmondo/topup/card/b$a;->g()V

    .line 125
    iget-object v0, p0, Lco/uk/getmondo/topup/card/b;->a:Lco/uk/getmondo/common/ui/f;

    check-cast v0, Lco/uk/getmondo/topup/card/b$a;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lco/uk/getmondo/topup/card/b$a;->a(Z)V

    .line 126
    iget-object v0, p0, Lco/uk/getmondo/topup/card/b;->a:Lco/uk/getmondo/common/ui/f;

    check-cast v0, Lco/uk/getmondo/topup/card/b$a;

    invoke-interface {v0}, Lco/uk/getmondo/topup/card/b$a;->e()V

    .line 127
    return-void
.end method

.method static synthetic a(Lco/uk/getmondo/topup/card/b;)V
    .locals 0

    invoke-direct {p0}, Lco/uk/getmondo/topup/card/b;->a()V

    return-void
.end method

.method static synthetic a(Lco/uk/getmondo/topup/card/b;Ljava/lang/Throwable;)V
    .locals 0

    invoke-direct {p0, p1}, Lco/uk/getmondo/topup/card/b;->a(Ljava/lang/Throwable;)V

    return-void
.end method

.method private a(Lcom/stripe/android/model/Card;Lco/uk/getmondo/topup/card/b$a;)V
    .locals 1

    .prologue
    .line 110
    invoke-virtual {p1}, Lcom/stripe/android/model/Card;->validateNumber()Z

    move-result v0

    if-nez v0, :cond_0

    .line 111
    invoke-interface {p2}, Lco/uk/getmondo/topup/card/b$a;->a()V

    .line 113
    :cond_0
    invoke-virtual {p1}, Lcom/stripe/android/model/Card;->validateCVC()Z

    move-result v0

    if-nez v0, :cond_1

    .line 114
    invoke-interface {p2}, Lco/uk/getmondo/topup/card/b$a;->c()V

    .line 116
    :cond_1
    invoke-virtual {p1}, Lcom/stripe/android/model/Card;->validateExpiryDate()Z

    move-result v0

    if-nez v0, :cond_2

    .line 117
    invoke-interface {p2}, Lco/uk/getmondo/topup/card/b$a;->b()V

    .line 119
    :cond_2
    return-void
.end method

.method private a(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 130
    const-string v0, "Error topping up"

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {p1, v0, v1}, Ld/a/a;->a(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 132
    iget-object v0, p0, Lco/uk/getmondo/topup/card/b;->j:Lco/uk/getmondo/common/a;

    invoke-virtual {p1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lco/uk/getmondo/api/model/tracking/Impression;->e(Ljava/lang/String;)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v1

    invoke-virtual {v0, v1}, Lco/uk/getmondo/common/a;->a(Lco/uk/getmondo/api/model/tracking/Impression;)V

    .line 134
    iget-object v0, p0, Lco/uk/getmondo/topup/card/b;->a:Lco/uk/getmondo/common/ui/f;

    check-cast v0, Lco/uk/getmondo/topup/card/b$a;

    invoke-interface {v0}, Lco/uk/getmondo/topup/card/b$a;->g()V

    .line 135
    iget-object v0, p0, Lco/uk/getmondo/topup/card/b;->a:Lco/uk/getmondo/common/ui/f;

    check-cast v0, Lco/uk/getmondo/topup/card/b$a;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lco/uk/getmondo/topup/card/b$a;->a(Z)V

    .line 138
    iget-object v0, p0, Lco/uk/getmondo/topup/card/b;->a:Lco/uk/getmondo/common/ui/f;

    check-cast v0, Lco/uk/getmondo/topup/card/b$a;

    invoke-interface {v0, p1}, Lco/uk/getmondo/topup/card/b$a;->a(Ljava/lang/Throwable;)Z

    move-result v0

    .line 139
    if-eqz v0, :cond_1

    .line 153
    :cond_0
    :goto_0
    return-void

    .line 144
    :cond_1
    iget-object v1, p0, Lco/uk/getmondo/topup/card/b;->i:Lco/uk/getmondo/topup/a/w;

    iget-object v0, p0, Lco/uk/getmondo/topup/card/b;->a:Lco/uk/getmondo/common/ui/f;

    check-cast v0, Lco/uk/getmondo/common/e/e;

    invoke-virtual {v1, v0, p1}, Lco/uk/getmondo/topup/a/w;->a(Lco/uk/getmondo/common/e/e;Ljava/lang/Throwable;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 149
    iget-object v1, p0, Lco/uk/getmondo/topup/card/b;->f:Lco/uk/getmondo/common/e/a;

    iget-object v0, p0, Lco/uk/getmondo/topup/card/b;->a:Lco/uk/getmondo/common/ui/f;

    check-cast v0, Lco/uk/getmondo/common/e/a$a;

    invoke-virtual {v1, p1, v0}, Lco/uk/getmondo/common/e/a;->a(Ljava/lang/Throwable;Lco/uk/getmondo/common/e/a$a;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 151
    iget-object v0, p0, Lco/uk/getmondo/topup/card/b;->a:Lco/uk/getmondo/common/ui/f;

    check-cast v0, Lco/uk/getmondo/topup/card/b$a;

    const v1, 0x7f0a03f2

    invoke-interface {v0, v1}, Lco/uk/getmondo/topup/card/b$a;->b(I)V

    goto :goto_0
.end method

.method private b(Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 168
    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    const-string v1, "\\s+"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public bridge synthetic a(Lco/uk/getmondo/common/ui/f;)V
    .locals 0

    .prologue
    .line 35
    check-cast p1, Lco/uk/getmondo/topup/card/b$a;

    invoke-virtual {p0, p1}, Lco/uk/getmondo/topup/card/b;->a(Lco/uk/getmondo/topup/card/b$a;)V

    return-void
.end method

.method public a(Lco/uk/getmondo/topup/card/b$a;)V
    .locals 2

    .prologue
    .line 61
    invoke-super {p0, p1}, Lco/uk/getmondo/common/ui/b;->a(Lco/uk/getmondo/common/ui/f;)V

    .line 62
    iget-object v0, p0, Lco/uk/getmondo/topup/card/b;->j:Lco/uk/getmondo/common/a;

    invoke-static {}, Lco/uk/getmondo/api/model/tracking/Impression;->m()Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v1

    invoke-virtual {v0, v1}, Lco/uk/getmondo/common/a;->a(Lco/uk/getmondo/api/model/tracking/Impression;)V

    .line 64
    iget-object v0, p0, Lco/uk/getmondo/topup/card/b;->g:Lco/uk/getmondo/common/accounts/d;

    invoke-virtual {v0}, Lco/uk/getmondo/common/accounts/d;->b()Lco/uk/getmondo/d/ak;

    move-result-object v0

    invoke-virtual {v0}, Lco/uk/getmondo/d/ak;->d()Lco/uk/getmondo/d/ac;

    move-result-object v0

    .line 65
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lco/uk/getmondo/d/ac;->c()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lco/uk/getmondo/common/k/p;->d(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 66
    invoke-virtual {v0}, Lco/uk/getmondo/d/ac;->c()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Lco/uk/getmondo/topup/card/b$a;->a(Ljava/lang/String;)V

    .line 68
    :cond_0
    return-void
.end method

.method a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lco/uk/getmondo/d/c;ZZLco/uk/getmondo/topup/three_d_secure/ThreeDsResolver;)V
    .locals 7

    .prologue
    .line 73
    iget-object v0, p0, Lco/uk/getmondo/topup/card/b;->j:Lco/uk/getmondo/common/a;

    sget-object v1, Lco/uk/getmondo/api/model/tracking/Impression$TopUpTapFrom;->FROM_NEW_CARD:Lco/uk/getmondo/api/model/tracking/Impression$TopUpTapFrom;

    invoke-static {v1}, Lco/uk/getmondo/api/model/tracking/Impression;->a(Lco/uk/getmondo/api/model/tracking/Impression$TopUpTapFrom;)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v1

    invoke-virtual {v0, v1}, Lco/uk/getmondo/common/a;->a(Lco/uk/getmondo/api/model/tracking/Impression;)V

    .line 75
    invoke-virtual {p5}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    .line 76
    invoke-direct {p0, p3}, Lco/uk/getmondo/topup/card/b;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lco/uk/getmondo/topup/card/b;->a(Ljava/lang/String;)Ljava/util/Calendar;

    move-result-object v3

    .line 78
    if-nez v3, :cond_2

    const/4 v0, 0x0

    move v1, v0

    .line 79
    :goto_0
    if-nez v3, :cond_3

    const/4 v0, 0x0

    .line 80
    :goto_1
    new-instance v3, Lcom/stripe/android/model/Card$Builder;

    invoke-direct {p0, p2}, Lco/uk/getmondo/topup/card/b;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-direct {v3, v4, v1, v0, p4}, Lcom/stripe/android/model/Card$Builder;-><init>(Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/String;)V

    .line 81
    invoke-virtual {v3, v2}, Lcom/stripe/android/model/Card$Builder;->addressZip(Ljava/lang/String;)Lcom/stripe/android/model/Card$Builder;

    move-result-object v0

    .line 82
    invoke-virtual {v0, p1}, Lcom/stripe/android/model/Card$Builder;->name(Ljava/lang/String;)Lcom/stripe/android/model/Card$Builder;

    move-result-object v0

    .line 83
    invoke-virtual {v0}, Lcom/stripe/android/model/Card$Builder;->build()Lcom/stripe/android/model/Card;

    move-result-object v1

    .line 85
    invoke-virtual {v1}, Lcom/stripe/android/model/Card;->validateCard()Z

    move-result v4

    .line 86
    invoke-virtual {v2}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_4

    const/4 v0, 0x1

    move v3, v0

    .line 87
    :goto_2
    if-eqz v4, :cond_5

    if-eqz v3, :cond_5

    const/4 v0, 0x1

    move v2, v0

    .line 89
    :goto_3
    if-nez v4, :cond_0

    .line 90
    iget-object v0, p0, Lco/uk/getmondo/topup/card/b;->a:Lco/uk/getmondo/common/ui/f;

    check-cast v0, Lco/uk/getmondo/topup/card/b$a;

    invoke-direct {p0, v1, v0}, Lco/uk/getmondo/topup/card/b;->a(Lcom/stripe/android/model/Card;Lco/uk/getmondo/topup/card/b$a;)V

    .line 92
    :cond_0
    if-nez v3, :cond_1

    .line 93
    iget-object v0, p0, Lco/uk/getmondo/topup/card/b;->a:Lco/uk/getmondo/common/ui/f;

    check-cast v0, Lco/uk/getmondo/topup/card/b$a;

    invoke-interface {v0}, Lco/uk/getmondo/topup/card/b$a;->d()V

    .line 96
    :cond_1
    if-nez v2, :cond_6

    .line 107
    :goto_4
    return-void

    .line 78
    :cond_2
    const/4 v0, 0x2

    invoke-virtual {v3, v0}, Ljava/util/Calendar;->get(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    move v1, v0

    goto :goto_0

    .line 79
    :cond_3
    const/4 v0, 0x1

    invoke-virtual {v3, v0}, Ljava/util/Calendar;->get(I)I

    move-result v0

    goto :goto_1

    .line 86
    :cond_4
    const/4 v0, 0x0

    move v3, v0

    goto :goto_2

    .line 87
    :cond_5
    const/4 v0, 0x0

    move v2, v0

    goto :goto_3

    .line 100
    :cond_6
    iget-object v0, p0, Lco/uk/getmondo/topup/card/b;->a:Lco/uk/getmondo/common/ui/f;

    check-cast v0, Lco/uk/getmondo/topup/card/b$a;

    invoke-interface {v0}, Lco/uk/getmondo/topup/card/b$a;->f()V

    .line 101
    iget-object v0, p0, Lco/uk/getmondo/topup/card/b;->a:Lco/uk/getmondo/common/ui/f;

    check-cast v0, Lco/uk/getmondo/topup/card/b$a;

    const/4 v2, 0x0

    invoke-interface {v0, v2}, Lco/uk/getmondo/topup/card/b$a;->a(Z)V

    .line 102
    iget-object v0, p0, Lco/uk/getmondo/topup/card/b;->g:Lco/uk/getmondo/common/accounts/d;

    invoke-virtual {v0}, Lco/uk/getmondo/common/accounts/d;->d()Lio/reactivex/v;

    move-result-object v6

    move-object v0, p0

    move v2, p8

    move-object v3, p6

    move v4, p7

    move-object/from16 v5, p9

    invoke-static/range {v0 .. v5}, Lco/uk/getmondo/topup/card/c;->a(Lco/uk/getmondo/topup/card/b;Lcom/stripe/android/model/Card;ZLco/uk/getmondo/d/c;ZLco/uk/getmondo/topup/three_d_secure/ThreeDsResolver;)Lio/reactivex/c/h;

    move-result-object v0

    .line 103
    invoke-virtual {v6, v0}, Lio/reactivex/v;->c(Lio/reactivex/c/h;)Lio/reactivex/b;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/topup/card/b;->e:Lio/reactivex/u;

    .line 104
    invoke-virtual {v0, v1}, Lio/reactivex/b;->b(Lio/reactivex/u;)Lio/reactivex/b;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/topup/card/b;->d:Lio/reactivex/u;

    .line 105
    invoke-virtual {v0, v1}, Lio/reactivex/b;->a(Lio/reactivex/u;)Lio/reactivex/b;

    move-result-object v0

    invoke-static {p0}, Lco/uk/getmondo/topup/card/d;->a(Lco/uk/getmondo/topup/card/b;)Lio/reactivex/c/a;

    move-result-object v1

    invoke-static {p0}, Lco/uk/getmondo/topup/card/e;->a(Lco/uk/getmondo/topup/card/b;)Lio/reactivex/c/g;

    move-result-object v2

    .line 106
    invoke-virtual {v0, v1, v2}, Lio/reactivex/b;->a(Lio/reactivex/c/a;Lio/reactivex/c/g;)Lio/reactivex/b/b;

    move-result-object v0

    .line 102
    invoke-virtual {p0, v0}, Lco/uk/getmondo/topup/card/b;->a(Lio/reactivex/b/b;)V

    goto :goto_4
.end method
