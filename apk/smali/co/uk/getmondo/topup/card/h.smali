.class Lco/uk/getmondo/topup/card/h;
.super Lco/uk/getmondo/common/ui/b;
.source "TopUpWithSavedCardPresenter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lco/uk/getmondo/topup/card/h$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lco/uk/getmondo/common/ui/b",
        "<",
        "Lco/uk/getmondo/topup/card/h$a;",
        ">;"
    }
.end annotation


# instance fields
.field private final c:Lio/reactivex/u;

.field private final d:Lio/reactivex/u;

.field private final e:Lco/uk/getmondo/common/e/a;

.field private final f:Lco/uk/getmondo/common/accounts/d;

.field private final g:Lco/uk/getmondo/topup/a/c;

.field private final h:Lco/uk/getmondo/topup/a/w;

.field private final i:Lco/uk/getmondo/common/a;

.field private final j:Lco/uk/getmondo/topup/three_d_secure/ThreeDsResolver;


# direct methods
.method constructor <init>(Lio/reactivex/u;Lio/reactivex/u;Lco/uk/getmondo/common/e/a;Lco/uk/getmondo/common/accounts/d;Lco/uk/getmondo/common/a;Lco/uk/getmondo/topup/a/c;Lco/uk/getmondo/topup/a/w;Lco/uk/getmondo/topup/three_d_secure/ThreeDsResolver;)V
    .locals 0

    .prologue
    .line 35
    invoke-direct {p0}, Lco/uk/getmondo/common/ui/b;-><init>()V

    .line 36
    iput-object p1, p0, Lco/uk/getmondo/topup/card/h;->c:Lio/reactivex/u;

    .line 37
    iput-object p2, p0, Lco/uk/getmondo/topup/card/h;->d:Lio/reactivex/u;

    .line 38
    iput-object p3, p0, Lco/uk/getmondo/topup/card/h;->e:Lco/uk/getmondo/common/e/a;

    .line 39
    iput-object p4, p0, Lco/uk/getmondo/topup/card/h;->f:Lco/uk/getmondo/common/accounts/d;

    .line 40
    iput-object p6, p0, Lco/uk/getmondo/topup/card/h;->g:Lco/uk/getmondo/topup/a/c;

    .line 41
    iput-object p7, p0, Lco/uk/getmondo/topup/card/h;->h:Lco/uk/getmondo/topup/a/w;

    .line 42
    iput-object p5, p0, Lco/uk/getmondo/topup/card/h;->i:Lco/uk/getmondo/common/a;

    .line 43
    iput-object p8, p0, Lco/uk/getmondo/topup/card/h;->j:Lco/uk/getmondo/topup/three_d_secure/ThreeDsResolver;

    .line 44
    return-void
.end method

.method static synthetic a(Lco/uk/getmondo/topup/card/h;Ljava/lang/String;Lco/uk/getmondo/d/c;Ljava/lang/String;)Lio/reactivex/d;
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 59
    iget-object v0, p0, Lco/uk/getmondo/topup/card/h;->g:Lco/uk/getmondo/topup/a/c;

    const/4 v4, 0x0

    iget-object v5, p0, Lco/uk/getmondo/topup/card/h;->j:Lco/uk/getmondo/topup/three_d_secure/ThreeDsResolver;

    move-object v1, p3

    move-object v2, p1

    move-object v3, p2

    invoke-virtual/range {v0 .. v5}, Lco/uk/getmondo/topup/a/c;->a(Ljava/lang/String;Ljava/lang/String;Lco/uk/getmondo/d/c;ZLco/uk/getmondo/topup/three_d_secure/ThreeDsResolver;)Lio/reactivex/b;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lco/uk/getmondo/topup/card/h;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 63
    iget-object v0, p0, Lco/uk/getmondo/topup/card/h;->i:Lco/uk/getmondo/common/a;

    sget-object v1, Lco/uk/getmondo/api/model/tracking/Impression$TopUpSuccessType;->SAVED_CARD:Lco/uk/getmondo/api/model/tracking/Impression$TopUpSuccessType;

    invoke-static {v1}, Lco/uk/getmondo/api/model/tracking/Impression;->a(Lco/uk/getmondo/api/model/tracking/Impression$TopUpSuccessType;)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v1

    invoke-virtual {v0, v1}, Lco/uk/getmondo/common/a;->a(Lco/uk/getmondo/api/model/tracking/Impression;)V

    .line 65
    iget-object v0, p0, Lco/uk/getmondo/topup/card/h;->a:Lco/uk/getmondo/common/ui/f;

    check-cast v0, Lco/uk/getmondo/topup/card/h$a;

    invoke-interface {v0}, Lco/uk/getmondo/topup/card/h$a;->b()V

    .line 66
    iget-object v0, p0, Lco/uk/getmondo/topup/card/h;->a:Lco/uk/getmondo/common/ui/f;

    check-cast v0, Lco/uk/getmondo/topup/card/h$a;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lco/uk/getmondo/topup/card/h$a;->a(Z)V

    .line 67
    iget-object v0, p0, Lco/uk/getmondo/topup/card/h;->a:Lco/uk/getmondo/common/ui/f;

    check-cast v0, Lco/uk/getmondo/topup/card/h$a;

    invoke-interface {v0}, Lco/uk/getmondo/topup/card/h$a;->c()V

    .line 68
    return-void
.end method

.method static synthetic a(Lco/uk/getmondo/topup/card/h;Ljava/lang/Throwable;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 69
    iget-object v0, p0, Lco/uk/getmondo/topup/card/h;->i:Lco/uk/getmondo/common/a;

    invoke-virtual {p1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lco/uk/getmondo/api/model/tracking/Impression;->e(Ljava/lang/String;)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v1

    invoke-virtual {v0, v1}, Lco/uk/getmondo/common/a;->a(Lco/uk/getmondo/api/model/tracking/Impression;)V

    .line 71
    iget-object v0, p0, Lco/uk/getmondo/topup/card/h;->a:Lco/uk/getmondo/common/ui/f;

    check-cast v0, Lco/uk/getmondo/topup/card/h$a;

    invoke-interface {v0}, Lco/uk/getmondo/topup/card/h$a;->b()V

    .line 72
    iget-object v0, p0, Lco/uk/getmondo/topup/card/h;->a:Lco/uk/getmondo/common/ui/f;

    check-cast v0, Lco/uk/getmondo/topup/card/h$a;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lco/uk/getmondo/topup/card/h$a;->a(Z)V

    .line 75
    iget-object v1, p0, Lco/uk/getmondo/topup/card/h;->h:Lco/uk/getmondo/topup/a/w;

    iget-object v0, p0, Lco/uk/getmondo/topup/card/h;->a:Lco/uk/getmondo/common/ui/f;

    check-cast v0, Lco/uk/getmondo/common/e/e;

    invoke-virtual {v1, v0, p1}, Lco/uk/getmondo/topup/a/w;->a(Lco/uk/getmondo/common/e/e;Ljava/lang/Throwable;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 84
    :cond_0
    :goto_0
    return-void

    .line 80
    :cond_1
    iget-object v1, p0, Lco/uk/getmondo/topup/card/h;->e:Lco/uk/getmondo/common/e/a;

    iget-object v0, p0, Lco/uk/getmondo/topup/card/h;->a:Lco/uk/getmondo/common/ui/f;

    check-cast v0, Lco/uk/getmondo/common/e/a$a;

    invoke-virtual {v1, p1, v0}, Lco/uk/getmondo/common/e/a;->a(Ljava/lang/Throwable;Lco/uk/getmondo/common/e/a$a;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 82
    iget-object v0, p0, Lco/uk/getmondo/topup/card/h;->a:Lco/uk/getmondo/common/ui/f;

    check-cast v0, Lco/uk/getmondo/topup/card/h$a;

    const v1, 0x7f0a03f2

    invoke-interface {v0, v1}, Lco/uk/getmondo/topup/card/h$a;->b(I)V

    goto :goto_0
.end method


# virtual methods
.method public bridge synthetic a(Lco/uk/getmondo/common/ui/f;)V
    .locals 0

    .prologue
    .line 21
    check-cast p1, Lco/uk/getmondo/topup/card/h$a;

    invoke-virtual {p0, p1}, Lco/uk/getmondo/topup/card/h;->a(Lco/uk/getmondo/topup/card/h$a;)V

    return-void
.end method

.method a(Lco/uk/getmondo/d/c;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 53
    iget-object v0, p0, Lco/uk/getmondo/topup/card/h;->i:Lco/uk/getmondo/common/a;

    sget-object v1, Lco/uk/getmondo/api/model/tracking/Impression$TopUpTapFrom;->FROM_SAVED_CARD:Lco/uk/getmondo/api/model/tracking/Impression$TopUpTapFrom;

    invoke-static {v1}, Lco/uk/getmondo/api/model/tracking/Impression;->a(Lco/uk/getmondo/api/model/tracking/Impression$TopUpTapFrom;)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v1

    invoke-virtual {v0, v1}, Lco/uk/getmondo/common/a;->a(Lco/uk/getmondo/api/model/tracking/Impression;)V

    .line 55
    iget-object v0, p0, Lco/uk/getmondo/topup/card/h;->a:Lco/uk/getmondo/common/ui/f;

    check-cast v0, Lco/uk/getmondo/topup/card/h$a;

    invoke-interface {v0}, Lco/uk/getmondo/topup/card/h$a;->a()V

    .line 56
    iget-object v0, p0, Lco/uk/getmondo/topup/card/h;->a:Lco/uk/getmondo/common/ui/f;

    check-cast v0, Lco/uk/getmondo/topup/card/h$a;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lco/uk/getmondo/topup/card/h$a;->a(Z)V

    .line 58
    iget-object v0, p0, Lco/uk/getmondo/topup/card/h;->f:Lco/uk/getmondo/common/accounts/d;

    invoke-virtual {v0}, Lco/uk/getmondo/common/accounts/d;->d()Lio/reactivex/v;

    move-result-object v0

    invoke-static {p0, p2, p1}, Lco/uk/getmondo/topup/card/i;->a(Lco/uk/getmondo/topup/card/h;Ljava/lang/String;Lco/uk/getmondo/d/c;)Lio/reactivex/c/h;

    move-result-object v1

    .line 59
    invoke-virtual {v0, v1}, Lio/reactivex/v;->c(Lio/reactivex/c/h;)Lio/reactivex/b;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/topup/card/h;->d:Lio/reactivex/u;

    .line 60
    invoke-virtual {v0, v1}, Lio/reactivex/b;->b(Lio/reactivex/u;)Lio/reactivex/b;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/topup/card/h;->c:Lio/reactivex/u;

    .line 61
    invoke-virtual {v0, v1}, Lio/reactivex/b;->a(Lio/reactivex/u;)Lio/reactivex/b;

    move-result-object v0

    invoke-static {p0}, Lco/uk/getmondo/topup/card/j;->a(Lco/uk/getmondo/topup/card/h;)Lio/reactivex/c/a;

    move-result-object v1

    invoke-static {p0}, Lco/uk/getmondo/topup/card/k;->a(Lco/uk/getmondo/topup/card/h;)Lio/reactivex/c/g;

    move-result-object v2

    .line 62
    invoke-virtual {v0, v1, v2}, Lio/reactivex/b;->a(Lio/reactivex/c/a;Lio/reactivex/c/g;)Lio/reactivex/b/b;

    move-result-object v0

    .line 58
    invoke-virtual {p0, v0}, Lco/uk/getmondo/topup/card/h;->a(Lio/reactivex/b/b;)V

    .line 85
    return-void
.end method

.method public a(Lco/uk/getmondo/topup/card/h$a;)V
    .locals 2

    .prologue
    .line 48
    invoke-super {p0, p1}, Lco/uk/getmondo/common/ui/b;->a(Lco/uk/getmondo/common/ui/f;)V

    .line 49
    iget-object v0, p0, Lco/uk/getmondo/topup/card/h;->i:Lco/uk/getmondo/common/a;

    invoke-static {}, Lco/uk/getmondo/api/model/tracking/Impression;->l()Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v1

    invoke-virtual {v0, v1}, Lco/uk/getmondo/common/a;->a(Lco/uk/getmondo/api/model/tracking/Impression;)V

    .line 50
    return-void
.end method
