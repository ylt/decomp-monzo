.class public Lco/uk/getmondo/topup/card/SavedCardView;
.super Landroid/widget/FrameLayout;
.source "SavedCardView.java"


# instance fields
.field cardExpiryView:Landroid/widget/TextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f1104bd
    .end annotation
.end field

.field cardHolderNameView:Landroid/widget/TextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f11034d
    .end annotation
.end field

.field cardImageView:Landroid/widget/ImageView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f1104bb
    .end annotation
.end field

.field cardLastFourView:Landroid/widget/TextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f1104bc
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 28
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lco/uk/getmondo/topup/card/SavedCardView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 29
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 32
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lco/uk/getmondo/topup/card/SavedCardView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 33
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 36
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, Lco/uk/getmondo/topup/card/SavedCardView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V

    .line 37
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V
    .locals 3

    .prologue
    .line 40
    invoke-direct {p0, p1, p2, p3, p4}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V

    .line 41
    invoke-virtual {p0}, Lco/uk/getmondo/topup/card/SavedCardView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f050144

    const/4 v2, 0x1

    invoke-virtual {v0, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 42
    invoke-static {v0, p0}, Lbutterknife/ButterKnife;->bind(Ljava/lang/Object;Landroid/view/View;)Lbutterknife/Unbinder;

    .line 43
    return-void
.end method

.method private a(Ljava/lang/String;)I
    .locals 1

    .prologue
    .line 53
    const-string v0, "visa"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 54
    const v0, 0x7f0200b4

    .line 58
    :goto_0
    return v0

    .line 55
    :cond_0
    const-string v0, "mastercard"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 56
    const v0, 0x7f020082

    goto :goto_0

    .line 58
    :cond_1
    const v0, 0x7f020080

    goto :goto_0
.end method


# virtual methods
.method public a(Lco/uk/getmondo/d/ae;)V
    .locals 2

    .prologue
    .line 46
    iget-object v0, p0, Lco/uk/getmondo/topup/card/SavedCardView;->cardImageView:Landroid/widget/ImageView;

    invoke-virtual {p1}, Lco/uk/getmondo/d/ae;->d()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lco/uk/getmondo/topup/card/SavedCardView;->a(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 47
    iget-object v0, p0, Lco/uk/getmondo/topup/card/SavedCardView;->cardLastFourView:Landroid/widget/TextView;

    invoke-virtual {p1}, Lco/uk/getmondo/d/ae;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 48
    iget-object v0, p0, Lco/uk/getmondo/topup/card/SavedCardView;->cardExpiryView:Landroid/widget/TextView;

    invoke-virtual {p1}, Lco/uk/getmondo/d/ae;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 49
    return-void
.end method
