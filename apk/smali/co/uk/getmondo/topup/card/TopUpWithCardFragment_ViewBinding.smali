.class public Lco/uk/getmondo/topup/card/TopUpWithCardFragment_ViewBinding;
.super Ljava/lang/Object;
.source "TopUpWithCardFragment_ViewBinding.java"

# interfaces
.implements Lbutterknife/Unbinder;


# instance fields
.field private a:Lco/uk/getmondo/topup/card/TopUpWithCardFragment;

.field private b:Landroid/view/View;

.field private c:Landroid/text/TextWatcher;

.field private d:Landroid/view/View;


# direct methods
.method public constructor <init>(Lco/uk/getmondo/topup/card/TopUpWithCardFragment;Landroid/view/View;)V
    .locals 5

    .prologue
    const v4, 0x7f110355

    const v3, 0x7f11025b

    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    iput-object p1, p0, Lco/uk/getmondo/topup/card/TopUpWithCardFragment_ViewBinding;->a:Lco/uk/getmondo/topup/card/TopUpWithCardFragment;

    .line 35
    const v0, 0x7f11012a

    const-string v1, "field \'descriptionView\'"

    const-class v2, Landroid/widget/TextView;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p1, Lco/uk/getmondo/topup/card/TopUpWithCardFragment;->descriptionView:Landroid/widget/TextView;

    .line 36
    const v0, 0x7f11034c

    const-string v1, "field \'cardHolderNameWrapper\'"

    const-class v2, Landroid/support/design/widget/TextInputLayout;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/design/widget/TextInputLayout;

    iput-object v0, p1, Lco/uk/getmondo/topup/card/TopUpWithCardFragment;->cardHolderNameWrapper:Landroid/support/design/widget/TextInputLayout;

    .line 37
    const v0, 0x7f11034d

    const-string v1, "field \'cardHolderName\'"

    const-class v2, Landroid/widget/EditText;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p1, Lco/uk/getmondo/topup/card/TopUpWithCardFragment;->cardHolderName:Landroid/widget/EditText;

    .line 38
    const v0, 0x7f11034e

    const-string v1, "field \'cardNumberWrapper\'"

    const-class v2, Landroid/support/design/widget/TextInputLayout;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/design/widget/TextInputLayout;

    iput-object v0, p1, Lco/uk/getmondo/topup/card/TopUpWithCardFragment;->cardNumberWrapper:Landroid/support/design/widget/TextInputLayout;

    .line 39
    const v0, 0x7f11034f

    const-string v1, "field \'cardNumber\'"

    const-class v2, Landroid/widget/EditText;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p1, Lco/uk/getmondo/topup/card/TopUpWithCardFragment;->cardNumber:Landroid/widget/EditText;

    .line 40
    const v0, 0x7f110350

    const-string v1, "field \'expiryDateWrapper\'"

    const-class v2, Landroid/support/design/widget/TextInputLayout;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/design/widget/TextInputLayout;

    iput-object v0, p1, Lco/uk/getmondo/topup/card/TopUpWithCardFragment;->expiryDateWrapper:Landroid/support/design/widget/TextInputLayout;

    .line 41
    const v0, 0x7f110351

    const-string v1, "field \'expiryDate\'"

    const-class v2, Landroid/widget/EditText;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p1, Lco/uk/getmondo/topup/card/TopUpWithCardFragment;->expiryDate:Landroid/widget/EditText;

    .line 42
    const v0, 0x7f110352

    const-string v1, "field \'cvvWrapper\'"

    const-class v2, Landroid/support/design/widget/TextInputLayout;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/design/widget/TextInputLayout;

    iput-object v0, p1, Lco/uk/getmondo/topup/card/TopUpWithCardFragment;->cvvWrapper:Landroid/support/design/widget/TextInputLayout;

    .line 43
    const v0, 0x7f110353

    const-string v1, "field \'cvv\'"

    const-class v2, Landroid/widget/EditText;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p1, Lco/uk/getmondo/topup/card/TopUpWithCardFragment;->cvv:Landroid/widget/EditText;

    .line 44
    const v0, 0x7f110354

    const-string v1, "field \'billingPostcodeWrapper\'"

    const-class v2, Landroid/support/design/widget/TextInputLayout;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/design/widget/TextInputLayout;

    iput-object v0, p1, Lco/uk/getmondo/topup/card/TopUpWithCardFragment;->billingPostcodeWrapper:Landroid/support/design/widget/TextInputLayout;

    .line 45
    const-string v0, "field \'billingPostcode\' and method \'onPostcodeTextChanged\'"

    invoke-static {p2, v4, v0}, Lbutterknife/internal/Utils;->findRequiredView(Landroid/view/View;ILjava/lang/String;)Landroid/view/View;

    move-result-object v1

    .line 46
    const-string v0, "field \'billingPostcode\'"

    const-class v2, Landroid/widget/EditText;

    invoke-static {v1, v4, v0, v2}, Lbutterknife/internal/Utils;->castView(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p1, Lco/uk/getmondo/topup/card/TopUpWithCardFragment;->billingPostcode:Landroid/widget/EditText;

    .line 47
    iput-object v1, p0, Lco/uk/getmondo/topup/card/TopUpWithCardFragment_ViewBinding;->b:Landroid/view/View;

    .line 48
    new-instance v0, Lco/uk/getmondo/topup/card/TopUpWithCardFragment_ViewBinding$1;

    invoke-direct {v0, p0, p1}, Lco/uk/getmondo/topup/card/TopUpWithCardFragment_ViewBinding$1;-><init>(Lco/uk/getmondo/topup/card/TopUpWithCardFragment_ViewBinding;Lco/uk/getmondo/topup/card/TopUpWithCardFragment;)V

    iput-object v0, p0, Lco/uk/getmondo/topup/card/TopUpWithCardFragment_ViewBinding;->c:Landroid/text/TextWatcher;

    move-object v0, v1

    .line 62
    check-cast v0, Landroid/widget/TextView;

    iget-object v1, p0, Lco/uk/getmondo/topup/card/TopUpWithCardFragment_ViewBinding;->c:Landroid/text/TextWatcher;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 63
    const-string v0, "field \'topUpButton\' and method \'onTopUpClicked\'"

    invoke-static {p2, v3, v0}, Lbutterknife/internal/Utils;->findRequiredView(Landroid/view/View;ILjava/lang/String;)Landroid/view/View;

    move-result-object v1

    .line 64
    const-string v0, "field \'topUpButton\'"

    const-class v2, Landroid/widget/Button;

    invoke-static {v1, v3, v0, v2}, Lbutterknife/internal/Utils;->castView(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p1, Lco/uk/getmondo/topup/card/TopUpWithCardFragment;->topUpButton:Landroid/widget/Button;

    .line 65
    iput-object v1, p0, Lco/uk/getmondo/topup/card/TopUpWithCardFragment_ViewBinding;->d:Landroid/view/View;

    .line 66
    new-instance v0, Lco/uk/getmondo/topup/card/TopUpWithCardFragment_ViewBinding$2;

    invoke-direct {v0, p0, p1}, Lco/uk/getmondo/topup/card/TopUpWithCardFragment_ViewBinding$2;-><init>(Lco/uk/getmondo/topup/card/TopUpWithCardFragment_ViewBinding;Lco/uk/getmondo/topup/card/TopUpWithCardFragment;)V

    invoke-virtual {v1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 72
    return-void
.end method


# virtual methods
.method public unbind()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 77
    iget-object v0, p0, Lco/uk/getmondo/topup/card/TopUpWithCardFragment_ViewBinding;->a:Lco/uk/getmondo/topup/card/TopUpWithCardFragment;

    .line 78
    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Bindings already cleared."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 79
    :cond_0
    iput-object v2, p0, Lco/uk/getmondo/topup/card/TopUpWithCardFragment_ViewBinding;->a:Lco/uk/getmondo/topup/card/TopUpWithCardFragment;

    .line 81
    iput-object v2, v0, Lco/uk/getmondo/topup/card/TopUpWithCardFragment;->descriptionView:Landroid/widget/TextView;

    .line 82
    iput-object v2, v0, Lco/uk/getmondo/topup/card/TopUpWithCardFragment;->cardHolderNameWrapper:Landroid/support/design/widget/TextInputLayout;

    .line 83
    iput-object v2, v0, Lco/uk/getmondo/topup/card/TopUpWithCardFragment;->cardHolderName:Landroid/widget/EditText;

    .line 84
    iput-object v2, v0, Lco/uk/getmondo/topup/card/TopUpWithCardFragment;->cardNumberWrapper:Landroid/support/design/widget/TextInputLayout;

    .line 85
    iput-object v2, v0, Lco/uk/getmondo/topup/card/TopUpWithCardFragment;->cardNumber:Landroid/widget/EditText;

    .line 86
    iput-object v2, v0, Lco/uk/getmondo/topup/card/TopUpWithCardFragment;->expiryDateWrapper:Landroid/support/design/widget/TextInputLayout;

    .line 87
    iput-object v2, v0, Lco/uk/getmondo/topup/card/TopUpWithCardFragment;->expiryDate:Landroid/widget/EditText;

    .line 88
    iput-object v2, v0, Lco/uk/getmondo/topup/card/TopUpWithCardFragment;->cvvWrapper:Landroid/support/design/widget/TextInputLayout;

    .line 89
    iput-object v2, v0, Lco/uk/getmondo/topup/card/TopUpWithCardFragment;->cvv:Landroid/widget/EditText;

    .line 90
    iput-object v2, v0, Lco/uk/getmondo/topup/card/TopUpWithCardFragment;->billingPostcodeWrapper:Landroid/support/design/widget/TextInputLayout;

    .line 91
    iput-object v2, v0, Lco/uk/getmondo/topup/card/TopUpWithCardFragment;->billingPostcode:Landroid/widget/EditText;

    .line 92
    iput-object v2, v0, Lco/uk/getmondo/topup/card/TopUpWithCardFragment;->topUpButton:Landroid/widget/Button;

    .line 94
    iget-object v0, p0, Lco/uk/getmondo/topup/card/TopUpWithCardFragment_ViewBinding;->b:Landroid/view/View;

    check-cast v0, Landroid/widget/TextView;

    iget-object v1, p0, Lco/uk/getmondo/topup/card/TopUpWithCardFragment_ViewBinding;->c:Landroid/text/TextWatcher;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->removeTextChangedListener(Landroid/text/TextWatcher;)V

    .line 95
    iput-object v2, p0, Lco/uk/getmondo/topup/card/TopUpWithCardFragment_ViewBinding;->c:Landroid/text/TextWatcher;

    .line 96
    iput-object v2, p0, Lco/uk/getmondo/topup/card/TopUpWithCardFragment_ViewBinding;->b:Landroid/view/View;

    .line 97
    iget-object v0, p0, Lco/uk/getmondo/topup/card/TopUpWithCardFragment_ViewBinding;->d:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 98
    iput-object v2, p0, Lco/uk/getmondo/topup/card/TopUpWithCardFragment_ViewBinding;->d:Landroid/view/View;

    .line 99
    return-void
.end method
