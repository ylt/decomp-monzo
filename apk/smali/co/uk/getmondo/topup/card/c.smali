.class final synthetic Lco/uk/getmondo/topup/card/c;
.super Ljava/lang/Object;

# interfaces
.implements Lio/reactivex/c/h;


# instance fields
.field private final a:Lco/uk/getmondo/topup/card/b;

.field private final b:Lcom/stripe/android/model/Card;

.field private final c:Z

.field private final d:Lco/uk/getmondo/d/c;

.field private final e:Z

.field private final f:Lco/uk/getmondo/topup/three_d_secure/ThreeDsResolver;


# direct methods
.method private constructor <init>(Lco/uk/getmondo/topup/card/b;Lcom/stripe/android/model/Card;ZLco/uk/getmondo/d/c;ZLco/uk/getmondo/topup/three_d_secure/ThreeDsResolver;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lco/uk/getmondo/topup/card/c;->a:Lco/uk/getmondo/topup/card/b;

    iput-object p2, p0, Lco/uk/getmondo/topup/card/c;->b:Lcom/stripe/android/model/Card;

    iput-boolean p3, p0, Lco/uk/getmondo/topup/card/c;->c:Z

    iput-object p4, p0, Lco/uk/getmondo/topup/card/c;->d:Lco/uk/getmondo/d/c;

    iput-boolean p5, p0, Lco/uk/getmondo/topup/card/c;->e:Z

    iput-object p6, p0, Lco/uk/getmondo/topup/card/c;->f:Lco/uk/getmondo/topup/three_d_secure/ThreeDsResolver;

    return-void
.end method

.method public static a(Lco/uk/getmondo/topup/card/b;Lcom/stripe/android/model/Card;ZLco/uk/getmondo/d/c;ZLco/uk/getmondo/topup/three_d_secure/ThreeDsResolver;)Lio/reactivex/c/h;
    .locals 7

    new-instance v0, Lco/uk/getmondo/topup/card/c;

    move-object v1, p0

    move-object v2, p1

    move v3, p2

    move-object v4, p3

    move v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lco/uk/getmondo/topup/card/c;-><init>(Lco/uk/getmondo/topup/card/b;Lcom/stripe/android/model/Card;ZLco/uk/getmondo/d/c;ZLco/uk/getmondo/topup/three_d_secure/ThreeDsResolver;)V

    return-object v0
.end method


# virtual methods
.method public a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 7

    iget-object v0, p0, Lco/uk/getmondo/topup/card/c;->a:Lco/uk/getmondo/topup/card/b;

    iget-object v1, p0, Lco/uk/getmondo/topup/card/c;->b:Lcom/stripe/android/model/Card;

    iget-boolean v2, p0, Lco/uk/getmondo/topup/card/c;->c:Z

    iget-object v3, p0, Lco/uk/getmondo/topup/card/c;->d:Lco/uk/getmondo/d/c;

    iget-boolean v4, p0, Lco/uk/getmondo/topup/card/c;->e:Z

    iget-object v5, p0, Lco/uk/getmondo/topup/card/c;->f:Lco/uk/getmondo/topup/three_d_secure/ThreeDsResolver;

    move-object v6, p1

    check-cast v6, Ljava/lang/String;

    invoke-static/range {v0 .. v6}, Lco/uk/getmondo/topup/card/b;->a(Lco/uk/getmondo/topup/card/b;Lcom/stripe/android/model/Card;ZLco/uk/getmondo/d/c;ZLco/uk/getmondo/topup/three_d_secure/ThreeDsResolver;Ljava/lang/String;)Lio/reactivex/d;

    move-result-object v0

    return-object v0
.end method
