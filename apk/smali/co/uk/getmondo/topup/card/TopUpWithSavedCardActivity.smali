.class public Lco/uk/getmondo/topup/card/TopUpWithSavedCardActivity;
.super Lco/uk/getmondo/common/activities/b;
.source "TopUpWithSavedCardActivity.java"

# interfaces
.implements Lco/uk/getmondo/topup/card/h$a;


# instance fields
.field a:Lco/uk/getmondo/common/a;

.field b:Lco/uk/getmondo/topup/card/h;

.field c:Lco/uk/getmondo/topup/three_d_secure/ThreeDsResolver;

.field descriptionView:Landroid/widget/TextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f11012a
    .end annotation
.end field

.field private e:Landroid/app/ProgressDialog;

.field private f:Lco/uk/getmondo/d/ae;

.field private g:Lco/uk/getmondo/d/c;

.field savedCardView:Lco/uk/getmondo/topup/card/SavedCardView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f11025c
    .end annotation
.end field

.field topUpButton:Landroid/widget/Button;
    .annotation build Lbutterknife/BindView;
        value = 0x7f11025b
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, Lco/uk/getmondo/common/activities/b;-><init>()V

    return-void
.end method

.method public static a(Landroid/content/Context;Lco/uk/getmondo/d/c;Lco/uk/getmondo/d/ae;)V
    .locals 1

    .prologue
    .line 44
    invoke-static {p0, p1, p2}, Lco/uk/getmondo/topup/card/TopUpWithSavedCardActivity;->b(Landroid/content/Context;Lco/uk/getmondo/d/c;Lco/uk/getmondo/d/ae;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 45
    return-void
.end method

.method public static b(Landroid/content/Context;Lco/uk/getmondo/d/c;Lco/uk/getmondo/d/ae;)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 48
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lco/uk/getmondo/topup/card/TopUpWithSavedCardActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 49
    const-string v1, "EXTRA_CARD"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 50
    const-string v1, "EXTRA_AMOUNT"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 51
    return-object v0
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    .line 100
    iget-object v0, p0, Lco/uk/getmondo/topup/card/TopUpWithSavedCardActivity;->e:Landroid/app/ProgressDialog;

    if-nez v0, :cond_0

    .line 101
    new-instance v0, Landroid/app/ProgressDialog;

    invoke-direct {v0, p0}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lco/uk/getmondo/topup/card/TopUpWithSavedCardActivity;->e:Landroid/app/ProgressDialog;

    .line 102
    iget-object v0, p0, Lco/uk/getmondo/topup/card/TopUpWithSavedCardActivity;->e:Landroid/app/ProgressDialog;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    .line 103
    iget-object v0, p0, Lco/uk/getmondo/topup/card/TopUpWithSavedCardActivity;->e:Landroid/app/ProgressDialog;

    const v1, 0x7f0a03f9

    invoke-virtual {p0, v1}, Lco/uk/getmondo/topup/card/TopUpWithSavedCardActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 106
    :cond_0
    iget-object v0, p0, Lco/uk/getmondo/topup/card/TopUpWithSavedCardActivity;->e:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-nez v0, :cond_1

    .line 107
    iget-object v0, p0, Lco/uk/getmondo/topup/card/TopUpWithSavedCardActivity;->e:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    .line 109
    :cond_1
    return-void
.end method

.method public a(Z)V
    .locals 1

    .prologue
    .line 126
    iget-object v0, p0, Lco/uk/getmondo/topup/card/TopUpWithSavedCardActivity;->topUpButton:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 127
    return-void
.end method

.method public b()V
    .locals 1

    .prologue
    .line 113
    iget-object v0, p0, Lco/uk/getmondo/topup/card/TopUpWithSavedCardActivity;->e:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lco/uk/getmondo/topup/card/TopUpWithSavedCardActivity;->e:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 114
    iget-object v0, p0, Lco/uk/getmondo/topup/card/TopUpWithSavedCardActivity;->e:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 116
    :cond_0
    return-void
.end method

.method public c()V
    .locals 1

    .prologue
    .line 120
    invoke-static {p0}, Lco/uk/getmondo/main/HomeActivity;->b(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    .line 121
    invoke-static {p0, v0}, Lco/uk/getmondo/common/activities/ConfirmationActivity;->a(Landroid/content/Context;Landroid/content/Intent;)V

    .line 122
    return-void
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 1

    .prologue
    .line 83
    iget-object v0, p0, Lco/uk/getmondo/topup/card/TopUpWithSavedCardActivity;->c:Lco/uk/getmondo/topup/three_d_secure/ThreeDsResolver;

    invoke-interface {v0, p1, p2, p3}, Lco/uk/getmondo/topup/three_d_secure/ThreeDsResolver;->a(IILandroid/content/Intent;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 84
    invoke-super {p0, p1, p2, p3}, Lco/uk/getmondo/common/activities/b;->onActivityResult(IILandroid/content/Intent;)V

    .line 86
    :cond_0
    return-void
.end method

.method public onAddNewCardClicked()V
    .locals 1
    .annotation build Lbutterknife/OnClick;
        value = {
            0x7f11025d
        }
    .end annotation

    .prologue
    .line 90
    iget-object v0, p0, Lco/uk/getmondo/topup/card/TopUpWithSavedCardActivity;->g:Lco/uk/getmondo/d/c;

    invoke-static {p0, v0}, Lco/uk/getmondo/topup/card/TopUpWithNewCardActivity;->a(Landroid/content/Context;Lco/uk/getmondo/d/c;)V

    .line 91
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 56
    invoke-super {p0, p1}, Lco/uk/getmondo/common/activities/b;->onCreate(Landroid/os/Bundle;)V

    .line 58
    invoke-virtual {p0}, Lco/uk/getmondo/topup/card/TopUpWithSavedCardActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "EXTRA_CARD"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/d/ae;

    iput-object v0, p0, Lco/uk/getmondo/topup/card/TopUpWithSavedCardActivity;->f:Lco/uk/getmondo/d/ae;

    .line 59
    invoke-virtual {p0}, Lco/uk/getmondo/topup/card/TopUpWithSavedCardActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "EXTRA_AMOUNT"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/d/c;

    iput-object v0, p0, Lco/uk/getmondo/topup/card/TopUpWithSavedCardActivity;->g:Lco/uk/getmondo/d/c;

    .line 60
    iget-object v0, p0, Lco/uk/getmondo/topup/card/TopUpWithSavedCardActivity;->f:Lco/uk/getmondo/d/ae;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lco/uk/getmondo/topup/card/TopUpWithSavedCardActivity;->g:Lco/uk/getmondo/d/c;

    if-nez v0, :cond_1

    .line 61
    :cond_0
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Card and amount are required"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 63
    :cond_1
    invoke-virtual {p0}, Lco/uk/getmondo/topup/card/TopUpWithSavedCardActivity;->l()Lco/uk/getmondo/common/h/b/b;

    move-result-object v0

    invoke-interface {v0, p0}, Lco/uk/getmondo/common/h/b/b;->a(Lco/uk/getmondo/topup/card/TopUpWithSavedCardActivity;)V

    .line 64
    iget-object v0, p0, Lco/uk/getmondo/topup/card/TopUpWithSavedCardActivity;->b:Lco/uk/getmondo/topup/card/h;

    invoke-virtual {v0, p0}, Lco/uk/getmondo/topup/card/h;->a(Lco/uk/getmondo/topup/card/h$a;)V

    .line 65
    const v0, 0x7f05006f

    invoke-virtual {p0, v0}, Lco/uk/getmondo/topup/card/TopUpWithSavedCardActivity;->setContentView(I)V

    .line 66
    invoke-static {p0}, Lbutterknife/ButterKnife;->bind(Landroid/app/Activity;)Lbutterknife/Unbinder;

    .line 68
    iget-object v0, p0, Lco/uk/getmondo/topup/card/TopUpWithSavedCardActivity;->savedCardView:Lco/uk/getmondo/topup/card/SavedCardView;

    iget-object v1, p0, Lco/uk/getmondo/topup/card/TopUpWithSavedCardActivity;->f:Lco/uk/getmondo/d/ae;

    invoke-virtual {v0, v1}, Lco/uk/getmondo/topup/card/SavedCardView;->a(Lco/uk/getmondo/d/ae;)V

    .line 69
    iget-object v0, p0, Lco/uk/getmondo/topup/card/TopUpWithSavedCardActivity;->g:Lco/uk/getmondo/d/c;

    invoke-virtual {v0}, Lco/uk/getmondo/d/c;->toString()Ljava/lang/String;

    move-result-object v0

    .line 70
    const v1, 0x7f0a0404

    new-array v2, v5, [Ljava/lang/Object;

    aput-object v0, v2, v4

    invoke-virtual {p0, v1, v2}, Lco/uk/getmondo/topup/card/TopUpWithSavedCardActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lco/uk/getmondo/topup/card/TopUpWithSavedCardActivity;->setTitle(Ljava/lang/CharSequence;)V

    .line 71
    iget-object v1, p0, Lco/uk/getmondo/topup/card/TopUpWithSavedCardActivity;->topUpButton:Landroid/widget/Button;

    const v2, 0x7f0a03e6

    new-array v3, v5, [Ljava/lang/Object;

    aput-object v0, v3, v4

    invoke-virtual {p0, v2, v3}, Lco/uk/getmondo/topup/card/TopUpWithSavedCardActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 72
    iget-object v1, p0, Lco/uk/getmondo/topup/card/TopUpWithSavedCardActivity;->descriptionView:Landroid/widget/TextView;

    const v2, 0x7f0a03fc

    new-array v3, v5, [Ljava/lang/Object;

    aput-object v0, v3, v4

    invoke-virtual {p0, v2, v3}, Lco/uk/getmondo/topup/card/TopUpWithSavedCardActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 73
    return-void
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 77
    iget-object v0, p0, Lco/uk/getmondo/topup/card/TopUpWithSavedCardActivity;->b:Lco/uk/getmondo/topup/card/h;

    invoke-virtual {v0}, Lco/uk/getmondo/topup/card/h;->b()V

    .line 78
    invoke-super {p0}, Lco/uk/getmondo/common/activities/b;->onDestroy()V

    .line 79
    return-void
.end method

.method public topUpClicked()V
    .locals 3
    .annotation build Lbutterknife/OnClick;
        value = {
            0x7f11025b
        }
    .end annotation

    .prologue
    .line 95
    iget-object v0, p0, Lco/uk/getmondo/topup/card/TopUpWithSavedCardActivity;->b:Lco/uk/getmondo/topup/card/h;

    iget-object v1, p0, Lco/uk/getmondo/topup/card/TopUpWithSavedCardActivity;->g:Lco/uk/getmondo/d/c;

    iget-object v2, p0, Lco/uk/getmondo/topup/card/TopUpWithSavedCardActivity;->f:Lco/uk/getmondo/d/ae;

    invoke-virtual {v2}, Lco/uk/getmondo/d/ae;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lco/uk/getmondo/topup/card/h;->a(Lco/uk/getmondo/d/c;Ljava/lang/String;)V

    .line 96
    return-void
.end method
