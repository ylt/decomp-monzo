.class public Lco/uk/getmondo/topup/a/w;
.super Ljava/lang/Object;
.source "TopUpErrorHandler.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    return-void
.end method


# virtual methods
.method public a(Lco/uk/getmondo/common/e/e;Ljava/lang/Throwable;)Z
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 35
    instance-of v0, p2, Lcom/stripe/android/exception/CardException;

    if-eqz v0, :cond_0

    .line 36
    const v0, 0x7f0a03ed

    invoke-interface {p1, v0}, Lco/uk/getmondo/common/e/e;->b(I)V

    move v0, v1

    .line 65
    :goto_0
    return v0

    .line 42
    :cond_0
    instance-of v0, p2, Lco/uk/getmondo/api/ApiException;

    if-eqz v0, :cond_1

    move-object v0, p2

    check-cast v0, Lco/uk/getmondo/api/ApiException;

    invoke-virtual {v0}, Lco/uk/getmondo/api/ApiException;->e()Lco/uk/getmondo/api/model/b;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 43
    invoke-static {}, Lco/uk/getmondo/topup/a/v;->values()[Lco/uk/getmondo/topup/a/v;

    move-result-object v2

    invoke-virtual {v0}, Lco/uk/getmondo/api/model/b;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lco/uk/getmondo/common/e/d;->a([Lco/uk/getmondo/common/e/f;Ljava/lang/String;)Lco/uk/getmondo/common/e/f;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/topup/a/v;

    .line 44
    if-eqz v0, :cond_1

    .line 45
    invoke-virtual {v0}, Lco/uk/getmondo/topup/a/v;->b()I

    move-result v0

    invoke-interface {p1, v0}, Lco/uk/getmondo/common/e/e;->b(I)V

    move v0, v1

    .line 46
    goto :goto_0

    .line 51
    :cond_1
    instance-of v0, p2, Lco/uk/getmondo/topup/three_d_secure/ThreeDsResolver$ThreeDsUnsuccessfulException;

    if-eqz v0, :cond_2

    .line 52
    sget-object v0, Lco/uk/getmondo/topup/a/w$1;->a:[I

    check-cast p2, Lco/uk/getmondo/topup/three_d_secure/ThreeDsResolver$ThreeDsUnsuccessfulException;

    iget-object v2, p2, Lco/uk/getmondo/topup/three_d_secure/ThreeDsResolver$ThreeDsUnsuccessfulException;->a:Lco/uk/getmondo/topup/three_d_secure/ThreeDsResolver$a;

    invoke-virtual {v2}, Lco/uk/getmondo/topup/three_d_secure/ThreeDsResolver$a;->ordinal()I

    move-result v2

    aget v0, v0, v2

    packed-switch v0, :pswitch_data_0

    :goto_1
    move v0, v1

    .line 62
    goto :goto_0

    .line 56
    :pswitch_0
    const v0, 0x7f0a03ec

    invoke-interface {p1, v0}, Lco/uk/getmondo/common/e/e;->b(I)V

    goto :goto_1

    .line 65
    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    .line 52
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method
