.class final synthetic Lco/uk/getmondo/topup/a/p;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/util/concurrent/Callable;


# instance fields
.field private final a:Lco/uk/getmondo/topup/a/c;

.field private final b:Ljava/lang/String;

.field private final c:Lco/uk/getmondo/d/c;

.field private final d:Z

.field private final e:Ljava/lang/String;


# direct methods
.method private constructor <init>(Lco/uk/getmondo/topup/a/c;Ljava/lang/String;Lco/uk/getmondo/d/c;ZLjava/lang/String;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lco/uk/getmondo/topup/a/p;->a:Lco/uk/getmondo/topup/a/c;

    iput-object p2, p0, Lco/uk/getmondo/topup/a/p;->b:Ljava/lang/String;

    iput-object p3, p0, Lco/uk/getmondo/topup/a/p;->c:Lco/uk/getmondo/d/c;

    iput-boolean p4, p0, Lco/uk/getmondo/topup/a/p;->d:Z

    iput-object p5, p0, Lco/uk/getmondo/topup/a/p;->e:Ljava/lang/String;

    return-void
.end method

.method public static a(Lco/uk/getmondo/topup/a/c;Ljava/lang/String;Lco/uk/getmondo/d/c;ZLjava/lang/String;)Ljava/util/concurrent/Callable;
    .locals 6

    new-instance v0, Lco/uk/getmondo/topup/a/p;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lco/uk/getmondo/topup/a/p;-><init>(Lco/uk/getmondo/topup/a/c;Ljava/lang/String;Lco/uk/getmondo/d/c;ZLjava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method public call()Ljava/lang/Object;
    .locals 5

    iget-object v0, p0, Lco/uk/getmondo/topup/a/p;->a:Lco/uk/getmondo/topup/a/c;

    iget-object v1, p0, Lco/uk/getmondo/topup/a/p;->b:Ljava/lang/String;

    iget-object v2, p0, Lco/uk/getmondo/topup/a/p;->c:Lco/uk/getmondo/d/c;

    iget-boolean v3, p0, Lco/uk/getmondo/topup/a/p;->d:Z

    iget-object v4, p0, Lco/uk/getmondo/topup/a/p;->e:Ljava/lang/String;

    invoke-static {v0, v1, v2, v3, v4}, Lco/uk/getmondo/topup/a/c;->a(Lco/uk/getmondo/topup/a/c;Ljava/lang/String;Lco/uk/getmondo/d/c;ZLjava/lang/String;)Lio/reactivex/z;

    move-result-object v0

    return-object v0
.end method
