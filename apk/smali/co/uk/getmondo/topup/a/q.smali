.class final synthetic Lco/uk/getmondo/topup/a/q;
.super Ljava/lang/Object;

# interfaces
.implements Lio/reactivex/c/h;


# instance fields
.field private final a:Lco/uk/getmondo/topup/a/c;

.field private final b:Lco/uk/getmondo/topup/three_d_secure/ThreeDsResolver;

.field private final c:Z


# direct methods
.method private constructor <init>(Lco/uk/getmondo/topup/a/c;Lco/uk/getmondo/topup/three_d_secure/ThreeDsResolver;Z)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lco/uk/getmondo/topup/a/q;->a:Lco/uk/getmondo/topup/a/c;

    iput-object p2, p0, Lco/uk/getmondo/topup/a/q;->b:Lco/uk/getmondo/topup/three_d_secure/ThreeDsResolver;

    iput-boolean p3, p0, Lco/uk/getmondo/topup/a/q;->c:Z

    return-void
.end method

.method public static a(Lco/uk/getmondo/topup/a/c;Lco/uk/getmondo/topup/three_d_secure/ThreeDsResolver;Z)Lio/reactivex/c/h;
    .locals 1

    new-instance v0, Lco/uk/getmondo/topup/a/q;

    invoke-direct {v0, p0, p1, p2}, Lco/uk/getmondo/topup/a/q;-><init>(Lco/uk/getmondo/topup/a/c;Lco/uk/getmondo/topup/three_d_secure/ThreeDsResolver;Z)V

    return-object v0
.end method


# virtual methods
.method public a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3

    iget-object v0, p0, Lco/uk/getmondo/topup/a/q;->a:Lco/uk/getmondo/topup/a/c;

    iget-object v1, p0, Lco/uk/getmondo/topup/a/q;->b:Lco/uk/getmondo/topup/three_d_secure/ThreeDsResolver;

    iget-boolean v2, p0, Lco/uk/getmondo/topup/a/q;->c:Z

    check-cast p1, Landroid/support/v4/g/j;

    invoke-static {v0, v1, v2, p1}, Lco/uk/getmondo/topup/a/c;->a(Lco/uk/getmondo/topup/a/c;Lco/uk/getmondo/topup/three_d_secure/ThreeDsResolver;ZLandroid/support/v4/g/j;)Lio/reactivex/z;

    move-result-object v0

    return-object v0
.end method
