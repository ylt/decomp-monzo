.class final synthetic Lco/uk/getmondo/topup/a/n;
.super Ljava/lang/Object;

# interfaces
.implements Lio/reactivex/c/h;


# instance fields
.field private final a:Lco/uk/getmondo/topup/a/c;

.field private final b:Lcom/stripe/android/model/Card;

.field private final c:Ljava/lang/String;

.field private final d:Z


# direct methods
.method private constructor <init>(Lco/uk/getmondo/topup/a/c;Lcom/stripe/android/model/Card;Ljava/lang/String;Z)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lco/uk/getmondo/topup/a/n;->a:Lco/uk/getmondo/topup/a/c;

    iput-object p2, p0, Lco/uk/getmondo/topup/a/n;->b:Lcom/stripe/android/model/Card;

    iput-object p3, p0, Lco/uk/getmondo/topup/a/n;->c:Ljava/lang/String;

    iput-boolean p4, p0, Lco/uk/getmondo/topup/a/n;->d:Z

    return-void
.end method

.method public static a(Lco/uk/getmondo/topup/a/c;Lcom/stripe/android/model/Card;Ljava/lang/String;Z)Lio/reactivex/c/h;
    .locals 1

    new-instance v0, Lco/uk/getmondo/topup/a/n;

    invoke-direct {v0, p0, p1, p2, p3}, Lco/uk/getmondo/topup/a/n;-><init>(Lco/uk/getmondo/topup/a/c;Lcom/stripe/android/model/Card;Ljava/lang/String;Z)V

    return-object v0
.end method


# virtual methods
.method public a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4

    iget-object v0, p0, Lco/uk/getmondo/topup/a/n;->a:Lco/uk/getmondo/topup/a/c;

    iget-object v1, p0, Lco/uk/getmondo/topup/a/n;->b:Lcom/stripe/android/model/Card;

    iget-object v2, p0, Lco/uk/getmondo/topup/a/n;->c:Ljava/lang/String;

    iget-boolean v3, p0, Lco/uk/getmondo/topup/a/n;->d:Z

    check-cast p1, Lcom/stripe/android/model/Token;

    invoke-static {v0, v1, v2, v3, p1}, Lco/uk/getmondo/topup/a/c;->a(Lco/uk/getmondo/topup/a/c;Lcom/stripe/android/model/Card;Ljava/lang/String;ZLcom/stripe/android/model/Token;)Lio/reactivex/z;

    move-result-object v0

    return-object v0
.end method
