.class public final Lco/uk/getmondo/topup/a/t;
.super Ljava/lang/Object;
.source "StripeTopUpHelper_Factory.java"

# interfaces
.implements Lb/a/b;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lb/a/b",
        "<",
        "Lco/uk/getmondo/topup/a/c;",
        ">;"
    }
.end annotation


# static fields
.field static final synthetic a:Z


# instance fields
.field private final b:Ljavax/a/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/api/MonzoApi;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Ljavax/a/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/a/a",
            "<",
            "Lio/reactivex/u;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Ljavax/a/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/topup/a/a;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Ljavax/a/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/a/a",
            "<",
            "Lcom/stripe/android/Stripe;",
            ">;"
        }
    .end annotation
.end field

.field private final f:Ljavax/a/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/topup/a/c$a;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 10
    const-class v0, Lco/uk/getmondo/topup/a/t;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lco/uk/getmondo/topup/a/t;->a:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/api/MonzoApi;",
            ">;",
            "Ljavax/a/a",
            "<",
            "Lio/reactivex/u;",
            ">;",
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/topup/a/a;",
            ">;",
            "Ljavax/a/a",
            "<",
            "Lcom/stripe/android/Stripe;",
            ">;",
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/topup/a/c$a;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    sget-boolean v0, Lco/uk/getmondo/topup/a/t;->a:Z

    if-nez v0, :cond_0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 32
    :cond_0
    iput-object p1, p0, Lco/uk/getmondo/topup/a/t;->b:Ljavax/a/a;

    .line 33
    sget-boolean v0, Lco/uk/getmondo/topup/a/t;->a:Z

    if-nez v0, :cond_1

    if-nez p2, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 34
    :cond_1
    iput-object p2, p0, Lco/uk/getmondo/topup/a/t;->c:Ljavax/a/a;

    .line 35
    sget-boolean v0, Lco/uk/getmondo/topup/a/t;->a:Z

    if-nez v0, :cond_2

    if-nez p3, :cond_2

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 36
    :cond_2
    iput-object p3, p0, Lco/uk/getmondo/topup/a/t;->d:Ljavax/a/a;

    .line 37
    sget-boolean v0, Lco/uk/getmondo/topup/a/t;->a:Z

    if-nez v0, :cond_3

    if-nez p4, :cond_3

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 38
    :cond_3
    iput-object p4, p0, Lco/uk/getmondo/topup/a/t;->e:Ljavax/a/a;

    .line 39
    sget-boolean v0, Lco/uk/getmondo/topup/a/t;->a:Z

    if-nez v0, :cond_4

    if-nez p5, :cond_4

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 40
    :cond_4
    iput-object p5, p0, Lco/uk/getmondo/topup/a/t;->f:Ljavax/a/a;

    .line 41
    return-void
.end method

.method public static a(Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;)Lb/a/b;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/api/MonzoApi;",
            ">;",
            "Ljavax/a/a",
            "<",
            "Lio/reactivex/u;",
            ">;",
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/topup/a/a;",
            ">;",
            "Ljavax/a/a",
            "<",
            "Lcom/stripe/android/Stripe;",
            ">;",
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/topup/a/c$a;",
            ">;)",
            "Lb/a/b",
            "<",
            "Lco/uk/getmondo/topup/a/c;",
            ">;"
        }
    .end annotation

    .prologue
    .line 59
    new-instance v0, Lco/uk/getmondo/topup/a/t;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lco/uk/getmondo/topup/a/t;-><init>(Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;)V

    return-object v0
.end method


# virtual methods
.method public a()Lco/uk/getmondo/topup/a/c;
    .locals 6

    .prologue
    .line 45
    new-instance v0, Lco/uk/getmondo/topup/a/c;

    iget-object v1, p0, Lco/uk/getmondo/topup/a/t;->b:Ljavax/a/a;

    .line 46
    invoke-interface {v1}, Ljavax/a/a;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lco/uk/getmondo/api/MonzoApi;

    iget-object v2, p0, Lco/uk/getmondo/topup/a/t;->c:Ljavax/a/a;

    .line 47
    invoke-interface {v2}, Ljavax/a/a;->b()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lio/reactivex/u;

    iget-object v3, p0, Lco/uk/getmondo/topup/a/t;->d:Ljavax/a/a;

    .line 48
    invoke-interface {v3}, Ljavax/a/a;->b()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lco/uk/getmondo/topup/a/a;

    iget-object v4, p0, Lco/uk/getmondo/topup/a/t;->e:Ljavax/a/a;

    .line 49
    invoke-interface {v4}, Ljavax/a/a;->b()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/stripe/android/Stripe;

    iget-object v5, p0, Lco/uk/getmondo/topup/a/t;->f:Ljavax/a/a;

    .line 50
    invoke-interface {v5}, Ljavax/a/a;->b()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lco/uk/getmondo/topup/a/c$a;

    invoke-direct/range {v0 .. v5}, Lco/uk/getmondo/topup/a/c;-><init>(Lco/uk/getmondo/api/MonzoApi;Lio/reactivex/u;Lco/uk/getmondo/topup/a/a;Lcom/stripe/android/Stripe;Lco/uk/getmondo/topup/a/c$a;)V

    .line 45
    return-object v0
.end method

.method public synthetic b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 10
    invoke-virtual {p0}, Lco/uk/getmondo/topup/a/t;->a()Lco/uk/getmondo/topup/a/c;

    move-result-object v0

    return-object v0
.end method
