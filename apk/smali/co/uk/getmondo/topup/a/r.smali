.class final synthetic Lco/uk/getmondo/topup/a/r;
.super Ljava/lang/Object;

# interfaces
.implements Lio/reactivex/c/h;


# instance fields
.field private final a:Lco/uk/getmondo/topup/a/c;

.field private final b:Ljava/lang/String;

.field private final c:Ljava/lang/String;

.field private final d:Lco/uk/getmondo/d/c;

.field private final e:Z


# direct methods
.method private constructor <init>(Lco/uk/getmondo/topup/a/c;Ljava/lang/String;Ljava/lang/String;Lco/uk/getmondo/d/c;Z)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lco/uk/getmondo/topup/a/r;->a:Lco/uk/getmondo/topup/a/c;

    iput-object p2, p0, Lco/uk/getmondo/topup/a/r;->b:Ljava/lang/String;

    iput-object p3, p0, Lco/uk/getmondo/topup/a/r;->c:Ljava/lang/String;

    iput-object p4, p0, Lco/uk/getmondo/topup/a/r;->d:Lco/uk/getmondo/d/c;

    iput-boolean p5, p0, Lco/uk/getmondo/topup/a/r;->e:Z

    return-void
.end method

.method public static a(Lco/uk/getmondo/topup/a/c;Ljava/lang/String;Ljava/lang/String;Lco/uk/getmondo/d/c;Z)Lio/reactivex/c/h;
    .locals 6

    new-instance v0, Lco/uk/getmondo/topup/a/r;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move v5, p4

    invoke-direct/range {v0 .. v5}, Lco/uk/getmondo/topup/a/r;-><init>(Lco/uk/getmondo/topup/a/c;Ljava/lang/String;Ljava/lang/String;Lco/uk/getmondo/d/c;Z)V

    return-object v0
.end method


# virtual methods
.method public a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 6

    iget-object v0, p0, Lco/uk/getmondo/topup/a/r;->a:Lco/uk/getmondo/topup/a/c;

    iget-object v1, p0, Lco/uk/getmondo/topup/a/r;->b:Ljava/lang/String;

    iget-object v2, p0, Lco/uk/getmondo/topup/a/r;->c:Ljava/lang/String;

    iget-object v3, p0, Lco/uk/getmondo/topup/a/r;->d:Lco/uk/getmondo/d/c;

    iget-boolean v4, p0, Lco/uk/getmondo/topup/a/r;->e:Z

    move-object v5, p1

    check-cast v5, Lco/uk/getmondo/api/model/topup/ApiThreeDsResponse;

    invoke-static/range {v0 .. v5}, Lco/uk/getmondo/topup/a/c;->a(Lco/uk/getmondo/topup/a/c;Ljava/lang/String;Ljava/lang/String;Lco/uk/getmondo/d/c;ZLco/uk/getmondo/api/model/topup/ApiThreeDsResponse;)Lio/reactivex/d;

    move-result-object v0

    return-object v0
.end method
