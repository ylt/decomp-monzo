.class public Lco/uk/getmondo/topup/a/c;
.super Ljava/lang/Object;
.source "StripeTopUpHelper.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lco/uk/getmondo/topup/a/c$a;
    }
.end annotation


# instance fields
.field private final a:Lco/uk/getmondo/api/MonzoApi;

.field private final b:Lio/reactivex/u;

.field private final c:Lco/uk/getmondo/topup/a/a;

.field private final d:Lcom/stripe/android/Stripe;

.field private final e:Lco/uk/getmondo/topup/a/c$a;


# direct methods
.method public constructor <init>(Lco/uk/getmondo/api/MonzoApi;Lio/reactivex/u;Lco/uk/getmondo/topup/a/a;Lcom/stripe/android/Stripe;Lco/uk/getmondo/topup/a/c$a;)V
    .locals 0

    .prologue
    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    iput-object p1, p0, Lco/uk/getmondo/topup/a/c;->a:Lco/uk/getmondo/api/MonzoApi;

    .line 44
    iput-object p2, p0, Lco/uk/getmondo/topup/a/c;->b:Lio/reactivex/u;

    .line 45
    iput-object p3, p0, Lco/uk/getmondo/topup/a/c;->c:Lco/uk/getmondo/topup/a/a;

    .line 46
    iput-object p4, p0, Lco/uk/getmondo/topup/a/c;->d:Lcom/stripe/android/Stripe;

    .line 47
    iput-object p5, p0, Lco/uk/getmondo/topup/a/c;->e:Lco/uk/getmondo/topup/a/c$a;

    .line 48
    return-void
.end method

.method static synthetic a(Lco/uk/getmondo/api/model/topup/ThreeDSecureRequest;Lco/uk/getmondo/api/model/topup/ApiThreeDsResponse;)Landroid/support/v4/g/j;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 113
    new-instance v0, Landroid/support/v4/g/j;

    invoke-direct {v0, p1, p0}, Landroid/support/v4/g/j;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    return-object v0
.end method

.method static synthetic a(Lco/uk/getmondo/topup/a/c;Ljava/lang/String;Ljava/lang/String;)Lco/uk/getmondo/api/model/topup/AddStripeCardRequest;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 59
    iget-object v0, p0, Lco/uk/getmondo/topup/a/c;->e:Lco/uk/getmondo/topup/a/c$a;

    invoke-virtual {v0, p1}, Lco/uk/getmondo/topup/a/c$a;->a(Ljava/lang/String;)Lcom/stripe/android/model/Token;

    move-result-object v0

    .line 60
    new-instance v1, Lco/uk/getmondo/api/model/topup/AddStripeCardRequest;

    invoke-virtual {v0}, Lcom/stripe/android/model/Token;->getId()Ljava/lang/String;

    move-result-object v0

    const-string v2, ""

    const/4 v3, 0x0

    invoke-direct {v1, p2, v0, v2, v3}, Lco/uk/getmondo/api/model/topup/AddStripeCardRequest;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    return-object v1
.end method

.method static synthetic a(Lco/uk/getmondo/topup/a/c;Lcom/stripe/android/model/Card;)Lcom/stripe/android/model/Token;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 81
    iget-object v0, p0, Lco/uk/getmondo/topup/a/c;->d:Lcom/stripe/android/Stripe;

    invoke-virtual {v0, p1}, Lcom/stripe/android/Stripe;->createTokenSynchronous(Lcom/stripe/android/model/Card;)Lcom/stripe/android/model/Token;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lco/uk/getmondo/topup/a/c;Ljava/lang/String;Lco/uk/getmondo/d/c;Lco/uk/getmondo/api/model/topup/AddStripeCardRequest;)Lio/reactivex/d;
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 61
    iget-object v0, p0, Lco/uk/getmondo/topup/a/c;->a:Lco/uk/getmondo/api/MonzoApi;

    invoke-virtual {p3}, Lco/uk/getmondo/api/model/topup/AddStripeCardRequest;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p3}, Lco/uk/getmondo/api/model/topup/AddStripeCardRequest;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p3}, Lco/uk/getmondo/api/model/topup/AddStripeCardRequest;->c()Ljava/lang/String;

    move-result-object v3

    .line 62
    invoke-virtual {p3}, Lco/uk/getmondo/api/model/topup/AddStripeCardRequest;->d()Z

    move-result v4

    iget-object v5, p0, Lco/uk/getmondo/topup/a/c;->c:Lco/uk/getmondo/topup/a/a;

    invoke-virtual {v5, p3}, Lco/uk/getmondo/topup/a/a;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    .line 61
    invoke-interface/range {v0 .. v5}, Lco/uk/getmondo/api/MonzoApi;->addStripeCard(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;)Lio/reactivex/v;

    move-result-object v0

    invoke-static {p0, p3}, Lco/uk/getmondo/topup/a/j;->a(Lco/uk/getmondo/topup/a/c;Lco/uk/getmondo/api/model/topup/AddStripeCardRequest;)Lio/reactivex/c/g;

    move-result-object v1

    .line 63
    invoke-virtual {v0, v1}, Lio/reactivex/v;->d(Lio/reactivex/c/g;)Lio/reactivex/v;

    move-result-object v0

    invoke-static {p0, p1, p2}, Lco/uk/getmondo/topup/a/k;->a(Lco/uk/getmondo/topup/a/c;Ljava/lang/String;Lco/uk/getmondo/d/c;)Lio/reactivex/c/h;

    move-result-object v1

    .line 65
    invoke-virtual {v0, v1}, Lio/reactivex/v;->c(Lio/reactivex/c/h;)Lio/reactivex/b;

    move-result-object v0

    .line 61
    return-object v0
.end method

.method static synthetic a(Lco/uk/getmondo/topup/a/c;Ljava/lang/String;Lco/uk/getmondo/d/c;Lco/uk/getmondo/api/model/topup/ApiStripeCard;)Lio/reactivex/d;
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 65
    invoke-virtual {p3}, Lco/uk/getmondo/api/model/topup/ApiStripeCard;->a()Ljava/lang/String;

    move-result-object v2

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v3, p2

    invoke-virtual/range {v0 .. v5}, Lco/uk/getmondo/topup/a/c;->a(Ljava/lang/String;Ljava/lang/String;Lco/uk/getmondo/d/c;ZLco/uk/getmondo/topup/three_d_secure/ThreeDsResolver;)Lio/reactivex/b;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lco/uk/getmondo/topup/a/c;Ljava/lang/String;Lco/uk/getmondo/d/c;ZLco/uk/getmondo/topup/three_d_secure/ThreeDsResolver;Lco/uk/getmondo/api/model/topup/ApiStripeCard;)Lio/reactivex/d;
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 91
    invoke-virtual {p5}, Lco/uk/getmondo/api/model/topup/ApiStripeCard;->a()Ljava/lang/String;

    move-result-object v2

    move-object v0, p0

    move-object v1, p1

    move-object v3, p2

    move v4, p3

    move-object v5, p4

    invoke-virtual/range {v0 .. v5}, Lco/uk/getmondo/topup/a/c;->a(Ljava/lang/String;Ljava/lang/String;Lco/uk/getmondo/d/c;ZLco/uk/getmondo/topup/three_d_secure/ThreeDsResolver;)Lio/reactivex/b;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lco/uk/getmondo/topup/a/c;Ljava/lang/String;Ljava/lang/String;Lco/uk/getmondo/d/c;ZLco/uk/getmondo/api/model/topup/ApiThreeDsResponse;)Lio/reactivex/d;
    .locals 11
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 123
    new-instance v0, Lco/uk/getmondo/api/model/topup/TopUpRequest;

    .line 125
    invoke-virtual/range {p5 .. p5}, Lco/uk/getmondo/api/model/topup/ApiThreeDsResponse;->b()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual/range {p5 .. p5}, Lco/uk/getmondo/api/model/topup/ApiThreeDsResponse;->d()Ljava/lang/String;

    move-result-object v2

    .line 126
    :goto_0
    invoke-virtual {p3}, Lco/uk/getmondo/d/c;->l()Lco/uk/getmondo/common/i/c;

    move-result-object v1

    invoke-virtual {v1}, Lco/uk/getmondo/common/i/c;->b()Ljava/lang/String;

    move-result-object v3

    .line 127
    invoke-virtual {p3}, Lco/uk/getmondo/d/c;->k()J

    move-result-wide v4

    .line 129
    invoke-virtual/range {p5 .. p5}, Lco/uk/getmondo/api/model/topup/ApiThreeDsResponse;->c()Ljava/lang/String;

    move-result-object v7

    move-object v1, p1

    move v6, p4

    invoke-direct/range {v0 .. v7}, Lco/uk/getmondo/api/model/topup/TopUpRequest;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JZLjava/lang/String;)V

    .line 130
    iget-object v2, p0, Lco/uk/getmondo/topup/a/c;->a:Lco/uk/getmondo/api/MonzoApi;

    .line 131
    invoke-virtual {v0}, Lco/uk/getmondo/api/model/topup/TopUpRequest;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0}, Lco/uk/getmondo/api/model/topup/TopUpRequest;->b()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0}, Lco/uk/getmondo/api/model/topup/TopUpRequest;->c()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0}, Lco/uk/getmondo/api/model/topup/TopUpRequest;->d()J

    move-result-wide v6

    .line 132
    invoke-virtual {v0}, Lco/uk/getmondo/api/model/topup/TopUpRequest;->e()Z

    move-result v8

    invoke-virtual {v0}, Lco/uk/getmondo/api/model/topup/TopUpRequest;->f()Ljava/lang/String;

    move-result-object v9

    iget-object v1, p0, Lco/uk/getmondo/topup/a/c;->c:Lco/uk/getmondo/topup/a/a;

    invoke-virtual {v1, v0}, Lco/uk/getmondo/topup/a/a;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    .line 131
    invoke-interface/range {v2 .. v10}, Lco/uk/getmondo/api/MonzoApi;->topUpThreeDSecure(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JZLjava/lang/String;Ljava/lang/String;)Lio/reactivex/b;

    move-result-object v1

    invoke-static {p0, v0}, Lco/uk/getmondo/topup/a/f;->a(Lco/uk/getmondo/topup/a/c;Lco/uk/getmondo/api/model/topup/TopUpRequest;)Lio/reactivex/c/g;

    move-result-object v0

    .line 133
    invoke-virtual {v1, v0}, Lio/reactivex/b;->a(Lio/reactivex/c/g;)Lio/reactivex/b;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/topup/a/c;->b:Lio/reactivex/u;

    .line 134
    invoke-virtual {v0, v1}, Lio/reactivex/b;->b(Lio/reactivex/u;)Lio/reactivex/b;

    move-result-object v0

    .line 130
    return-object v0

    :cond_0
    move-object v2, p2

    .line 125
    goto :goto_0
.end method

.method private a(Lco/uk/getmondo/api/model/topup/ApiThreeDsResponse;Lco/uk/getmondo/api/model/topup/ThreeDSecureRequest;Lco/uk/getmondo/topup/three_d_secure/ThreeDsResolver;Z)Lio/reactivex/v;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lco/uk/getmondo/api/model/topup/ApiThreeDsResponse;",
            "Lco/uk/getmondo/api/model/topup/ThreeDSecureRequest;",
            "Lco/uk/getmondo/topup/three_d_secure/ThreeDsResolver;",
            "Z)",
            "Lio/reactivex/v",
            "<",
            "Lco/uk/getmondo/api/model/topup/ApiThreeDsResponse;",
            ">;"
        }
    .end annotation

    .prologue
    .line 140
    invoke-virtual {p1}, Lco/uk/getmondo/api/model/topup/ApiThreeDsResponse;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 141
    invoke-virtual {p1}, Lco/uk/getmondo/api/model/topup/ApiThreeDsResponse;->e()Ljava/lang/String;

    move-result-object v0

    const-string v1, "https://threeds.monzo.com"

    invoke-interface {p3, v0, v1, p4}, Lco/uk/getmondo/topup/three_d_secure/ThreeDsResolver;->a(Ljava/lang/String;Ljava/lang/String;Z)Lio/reactivex/v;

    move-result-object v0

    invoke-static {p0, p2}, Lco/uk/getmondo/topup/a/s;->a(Lco/uk/getmondo/topup/a/c;Lco/uk/getmondo/api/model/topup/ThreeDSecureRequest;)Lio/reactivex/c/g;

    move-result-object v1

    .line 142
    invoke-virtual {v0, v1}, Lio/reactivex/v;->c(Lio/reactivex/c/g;)Lio/reactivex/v;

    move-result-object v0

    invoke-static {p1}, Lco/uk/getmondo/topup/a/e;->a(Lco/uk/getmondo/api/model/topup/ApiThreeDsResponse;)Lio/reactivex/c/h;

    move-result-object v1

    .line 148
    invoke-virtual {v0, v1}, Lio/reactivex/v;->a(Lio/reactivex/c/h;)Lio/reactivex/v;

    move-result-object v0

    .line 156
    :goto_0
    return-object v0

    :cond_0
    invoke-static {p1}, Lio/reactivex/v;->a(Ljava/lang/Object;)Lio/reactivex/v;

    move-result-object v0

    goto :goto_0
.end method

.method static synthetic a(Lco/uk/getmondo/api/model/topup/ApiThreeDsResponse;Lco/uk/getmondo/topup/three_d_secure/ThreeDsResolver$a;)Lio/reactivex/z;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 150
    sget-object v0, Lco/uk/getmondo/topup/three_d_secure/ThreeDsResolver$a;->a:Lco/uk/getmondo/topup/three_d_secure/ThreeDsResolver$a;

    if-eq p1, v0, :cond_0

    .line 151
    new-instance v0, Lco/uk/getmondo/topup/three_d_secure/ThreeDsResolver$ThreeDsUnsuccessfulException;

    invoke-direct {v0, p1}, Lco/uk/getmondo/topup/three_d_secure/ThreeDsResolver$ThreeDsUnsuccessfulException;-><init>(Lco/uk/getmondo/topup/three_d_secure/ThreeDsResolver$a;)V

    invoke-static {v0}, Lio/reactivex/v;->a(Ljava/lang/Throwable;)Lio/reactivex/v;

    move-result-object v0

    .line 153
    :goto_0
    return-object v0

    :cond_0
    invoke-static {p0}, Lio/reactivex/v;->a(Ljava/lang/Object;)Lio/reactivex/v;

    move-result-object v0

    goto :goto_0
.end method

.method static synthetic a(Lco/uk/getmondo/topup/a/c;Lco/uk/getmondo/topup/three_d_secure/ThreeDsResolver;ZLandroid/support/v4/g/j;)Lio/reactivex/z;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 120
    iget-object v0, p3, Landroid/support/v4/g/j;->a:Ljava/lang/Object;

    check-cast v0, Lco/uk/getmondo/api/model/topup/ApiThreeDsResponse;

    iget-object v1, p3, Landroid/support/v4/g/j;->b:Ljava/lang/Object;

    check-cast v1, Lco/uk/getmondo/api/model/topup/ThreeDSecureRequest;

    invoke-direct {p0, v0, v1, p1, p2}, Lco/uk/getmondo/topup/a/c;->a(Lco/uk/getmondo/api/model/topup/ApiThreeDsResponse;Lco/uk/getmondo/api/model/topup/ThreeDSecureRequest;Lco/uk/getmondo/topup/three_d_secure/ThreeDsResolver;Z)Lio/reactivex/v;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lco/uk/getmondo/topup/a/c;Lcom/stripe/android/model/Card;Ljava/lang/String;ZLcom/stripe/android/model/Token;)Lio/reactivex/z;
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 84
    invoke-virtual {p1}, Lcom/stripe/android/model/Card;->getNumber()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x6

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 85
    new-instance v6, Lco/uk/getmondo/api/model/topup/AddStripeCardRequest;

    invoke-virtual {p4}, Lcom/stripe/android/model/Token;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v6, p2, v1, v0, p3}, Lco/uk/getmondo/api/model/topup/AddStripeCardRequest;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 86
    iget-object v0, p0, Lco/uk/getmondo/topup/a/c;->a:Lco/uk/getmondo/api/MonzoApi;

    invoke-virtual {v6}, Lco/uk/getmondo/api/model/topup/AddStripeCardRequest;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v6}, Lco/uk/getmondo/api/model/topup/AddStripeCardRequest;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v6}, Lco/uk/getmondo/api/model/topup/AddStripeCardRequest;->c()Ljava/lang/String;

    move-result-object v3

    .line 87
    invoke-virtual {v6}, Lco/uk/getmondo/api/model/topup/AddStripeCardRequest;->d()Z

    move-result v4

    iget-object v5, p0, Lco/uk/getmondo/topup/a/c;->c:Lco/uk/getmondo/topup/a/a;

    invoke-virtual {v5, v6}, Lco/uk/getmondo/topup/a/a;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    .line 86
    invoke-interface/range {v0 .. v5}, Lco/uk/getmondo/api/MonzoApi;->addStripeCard(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;)Lio/reactivex/v;

    move-result-object v0

    invoke-static {p0, v6}, Lco/uk/getmondo/topup/a/i;->a(Lco/uk/getmondo/topup/a/c;Lco/uk/getmondo/api/model/topup/AddStripeCardRequest;)Lio/reactivex/c/g;

    move-result-object v1

    .line 88
    invoke-virtual {v0, v1}, Lio/reactivex/v;->d(Lio/reactivex/c/g;)Lio/reactivex/v;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/topup/a/c;->b:Lio/reactivex/u;

    .line 89
    invoke-virtual {v0, v1}, Lio/reactivex/v;->b(Lio/reactivex/u;)Lio/reactivex/v;

    move-result-object v0

    .line 86
    return-object v0
.end method

.method static synthetic a(Lco/uk/getmondo/topup/a/c;Ljava/lang/String;Lco/uk/getmondo/d/c;ZLjava/lang/String;)Lio/reactivex/z;
    .locals 12
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 105
    new-instance v1, Lco/uk/getmondo/api/model/topup/ThreeDSecureRequest;

    .line 106
    invoke-virtual {p2}, Lco/uk/getmondo/d/c;->l()Lco/uk/getmondo/common/i/c;

    move-result-object v0

    invoke-virtual {v0}, Lco/uk/getmondo/common/i/c;->b()Ljava/lang/String;

    move-result-object v3

    .line 107
    invoke-virtual {p2}, Lco/uk/getmondo/d/c;->k()J

    move-result-wide v4

    const-string v8, "https://threeds.monzo.com"

    move-object v2, p1

    move v6, p3

    move-object/from16 v7, p4

    invoke-direct/range {v1 .. v8}, Lco/uk/getmondo/api/model/topup/ThreeDSecureRequest;-><init>(Ljava/lang/String;Ljava/lang/String;JZLjava/lang/String;Ljava/lang/String;)V

    .line 111
    iget-object v3, p0, Lco/uk/getmondo/topup/a/c;->a:Lco/uk/getmondo/api/MonzoApi;

    invoke-virtual {v1}, Lco/uk/getmondo/api/model/topup/ThreeDSecureRequest;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1}, Lco/uk/getmondo/api/model/topup/ThreeDSecureRequest;->b()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1}, Lco/uk/getmondo/api/model/topup/ThreeDSecureRequest;->c()J

    move-result-wide v6

    invoke-virtual {v1}, Lco/uk/getmondo/api/model/topup/ThreeDSecureRequest;->d()Z

    move-result v8

    .line 112
    invoke-virtual {v1}, Lco/uk/getmondo/api/model/topup/ThreeDSecureRequest;->e()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v1}, Lco/uk/getmondo/api/model/topup/ThreeDSecureRequest;->f()Ljava/lang/String;

    move-result-object v10

    iget-object v0, p0, Lco/uk/getmondo/topup/a/c;->c:Lco/uk/getmondo/topup/a/a;

    invoke-virtual {v0, v1}, Lco/uk/getmondo/topup/a/a;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v11

    .line 111
    invoke-interface/range {v3 .. v11}, Lco/uk/getmondo/api/MonzoApi;->threeDSecure(Ljava/lang/String;Ljava/lang/String;JZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lio/reactivex/v;

    move-result-object v0

    invoke-static {v1}, Lco/uk/getmondo/topup/a/g;->a(Lco/uk/getmondo/api/model/topup/ThreeDSecureRequest;)Lio/reactivex/c/h;

    move-result-object v2

    .line 113
    invoke-virtual {v0, v2}, Lio/reactivex/v;->d(Lio/reactivex/c/h;)Lio/reactivex/v;

    move-result-object v0

    invoke-static {p0, v1}, Lco/uk/getmondo/topup/a/h;->a(Lco/uk/getmondo/topup/a/c;Lco/uk/getmondo/api/model/topup/ThreeDSecureRequest;)Lio/reactivex/c/g;

    move-result-object v1

    .line 114
    invoke-virtual {v0, v1}, Lio/reactivex/v;->d(Lio/reactivex/c/g;)Lio/reactivex/v;

    move-result-object v0

    .line 111
    return-object v0
.end method

.method static synthetic a(Lco/uk/getmondo/topup/a/c;Lco/uk/getmondo/api/model/topup/AddStripeCardRequest;Ljava/lang/Throwable;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 88
    invoke-direct {p0, p2, p1}, Lco/uk/getmondo/topup/a/c;->a(Ljava/lang/Throwable;Ljava/lang/Object;)Z

    return-void
.end method

.method static synthetic a(Lco/uk/getmondo/topup/a/c;Lco/uk/getmondo/api/model/topup/ThreeDSecureRequest;Lco/uk/getmondo/topup/three_d_secure/ThreeDsResolver$a;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 144
    sget-object v0, Lco/uk/getmondo/topup/three_d_secure/ThreeDsResolver$a;->b:Lco/uk/getmondo/topup/three_d_secure/ThreeDsResolver$a;

    if-ne p2, v0, :cond_0

    .line 145
    iget-object v0, p0, Lco/uk/getmondo/topup/a/c;->c:Lco/uk/getmondo/topup/a/a;

    invoke-virtual {v0, p1}, Lco/uk/getmondo/topup/a/a;->b(Ljava/lang/Object;)V

    .line 147
    :cond_0
    return-void
.end method

.method static synthetic a(Lco/uk/getmondo/topup/a/c;Lco/uk/getmondo/api/model/topup/ThreeDSecureRequest;Ljava/lang/Throwable;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 114
    invoke-direct {p0, p2, p1}, Lco/uk/getmondo/topup/a/c;->a(Ljava/lang/Throwable;Ljava/lang/Object;)Z

    return-void
.end method

.method static synthetic a(Lco/uk/getmondo/topup/a/c;Lco/uk/getmondo/api/model/topup/TopUpRequest;Ljava/lang/Throwable;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 133
    invoke-direct {p0, p2, p1}, Lco/uk/getmondo/topup/a/c;->a(Ljava/lang/Throwable;Ljava/lang/Object;)Z

    return-void
.end method

.method private a(Lco/uk/getmondo/api/model/b;)Z
    .locals 2

    .prologue
    .line 169
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lco/uk/getmondo/api/model/b;->a()Ljava/lang/String;

    move-result-object v0

    .line 170
    :goto_0
    if-eqz v0, :cond_1

    const-string v1, "bad_request.card_error"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_1
    return v0

    .line 169
    :cond_0
    const-string v0, ""

    goto :goto_0

    .line 170
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private a(Ljava/lang/Throwable;Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 161
    instance-of v0, p1, Lco/uk/getmondo/api/ApiException;

    if-eqz v0, :cond_0

    check-cast p1, Lco/uk/getmondo/api/ApiException;

    invoke-virtual {p1}, Lco/uk/getmondo/api/ApiException;->e()Lco/uk/getmondo/api/model/b;

    move-result-object v0

    invoke-direct {p0, v0}, Lco/uk/getmondo/topup/a/c;->a(Lco/uk/getmondo/api/model/b;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 162
    iget-object v0, p0, Lco/uk/getmondo/topup/a/c;->c:Lco/uk/getmondo/topup/a/a;

    invoke-virtual {v0, p2}, Lco/uk/getmondo/topup/a/a;->b(Ljava/lang/Object;)V

    .line 163
    const/4 v0, 0x1

    .line 165
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic b(Lco/uk/getmondo/topup/a/c;Lco/uk/getmondo/api/model/topup/AddStripeCardRequest;Ljava/lang/Throwable;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 63
    invoke-direct {p0, p2, p1}, Lco/uk/getmondo/topup/a/c;->a(Ljava/lang/Throwable;Ljava/lang/Object;)Z

    return-void
.end method


# virtual methods
.method public a(Ljava/lang/String;Lcom/stripe/android/model/Card;ZLco/uk/getmondo/d/c;ZLco/uk/getmondo/topup/three_d_secure/ThreeDsResolver;)Lio/reactivex/b;
    .locals 2

    .prologue
    .line 81
    invoke-static {p0, p2}, Lco/uk/getmondo/topup/a/m;->a(Lco/uk/getmondo/topup/a/c;Lcom/stripe/android/model/Card;)Ljava/util/concurrent/Callable;

    move-result-object v0

    invoke-static {v0}, Lio/reactivex/v;->c(Ljava/util/concurrent/Callable;)Lio/reactivex/v;

    move-result-object v0

    invoke-static {p0, p2, p1, p3}, Lco/uk/getmondo/topup/a/n;->a(Lco/uk/getmondo/topup/a/c;Lcom/stripe/android/model/Card;Ljava/lang/String;Z)Lio/reactivex/c/h;

    move-result-object v1

    .line 83
    invoke-virtual {v0, v1}, Lio/reactivex/v;->a(Lio/reactivex/c/h;)Lio/reactivex/v;

    move-result-object v0

    invoke-static {p0, p1, p4, p5, p6}, Lco/uk/getmondo/topup/a/o;->a(Lco/uk/getmondo/topup/a/c;Ljava/lang/String;Lco/uk/getmondo/d/c;ZLco/uk/getmondo/topup/three_d_secure/ThreeDsResolver;)Lio/reactivex/c/h;

    move-result-object v1

    .line 91
    invoke-virtual {v0, v1}, Lio/reactivex/v;->c(Lio/reactivex/c/h;)Lio/reactivex/b;

    move-result-object v0

    .line 81
    return-object v0
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;Lco/uk/getmondo/d/c;)Lio/reactivex/b;
    .locals 2

    .prologue
    .line 58
    invoke-static {p0, p2, p1}, Lco/uk/getmondo/topup/a/d;->a(Lco/uk/getmondo/topup/a/c;Ljava/lang/String;Ljava/lang/String;)Ljava/util/concurrent/Callable;

    move-result-object v0

    invoke-static {v0}, Lio/reactivex/v;->c(Ljava/util/concurrent/Callable;)Lio/reactivex/v;

    move-result-object v0

    invoke-static {p0, p1, p3}, Lco/uk/getmondo/topup/a/l;->a(Lco/uk/getmondo/topup/a/c;Ljava/lang/String;Lco/uk/getmondo/d/c;)Lio/reactivex/c/h;

    move-result-object v1

    .line 61
    invoke-virtual {v0, v1}, Lio/reactivex/v;->c(Lio/reactivex/c/h;)Lio/reactivex/b;

    move-result-object v0

    .line 58
    return-object v0
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;Lco/uk/getmondo/d/c;ZLco/uk/getmondo/topup/three_d_secure/ThreeDsResolver;)Lio/reactivex/b;
    .locals 2

    .prologue
    .line 104
    invoke-static {p0, p1, p3, p4, p2}, Lco/uk/getmondo/topup/a/p;->a(Lco/uk/getmondo/topup/a/c;Ljava/lang/String;Lco/uk/getmondo/d/c;ZLjava/lang/String;)Ljava/util/concurrent/Callable;

    move-result-object v0

    invoke-static {v0}, Lio/reactivex/v;->a(Ljava/util/concurrent/Callable;)Lio/reactivex/v;

    move-result-object v0

    .line 118
    invoke-static {p0, p5, p4}, Lco/uk/getmondo/topup/a/q;->a(Lco/uk/getmondo/topup/a/c;Lco/uk/getmondo/topup/three_d_secure/ThreeDsResolver;Z)Lio/reactivex/c/h;

    move-result-object v1

    .line 120
    invoke-virtual {v0, v1}, Lio/reactivex/v;->a(Lio/reactivex/c/h;)Lio/reactivex/v;

    move-result-object v0

    invoke-static {p0, p1, p2, p3, p4}, Lco/uk/getmondo/topup/a/r;->a(Lco/uk/getmondo/topup/a/c;Ljava/lang/String;Ljava/lang/String;Lco/uk/getmondo/d/c;Z)Lio/reactivex/c/h;

    move-result-object v1

    .line 122
    invoke-virtual {v0, v1}, Lio/reactivex/v;->c(Lio/reactivex/c/h;)Lio/reactivex/b;

    move-result-object v0

    .line 118
    return-object v0
.end method
