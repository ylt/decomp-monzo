.class final synthetic Lco/uk/getmondo/topup/a/m;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/util/concurrent/Callable;


# instance fields
.field private final a:Lco/uk/getmondo/topup/a/c;

.field private final b:Lcom/stripe/android/model/Card;


# direct methods
.method private constructor <init>(Lco/uk/getmondo/topup/a/c;Lcom/stripe/android/model/Card;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lco/uk/getmondo/topup/a/m;->a:Lco/uk/getmondo/topup/a/c;

    iput-object p2, p0, Lco/uk/getmondo/topup/a/m;->b:Lcom/stripe/android/model/Card;

    return-void
.end method

.method public static a(Lco/uk/getmondo/topup/a/c;Lcom/stripe/android/model/Card;)Ljava/util/concurrent/Callable;
    .locals 1

    new-instance v0, Lco/uk/getmondo/topup/a/m;

    invoke-direct {v0, p0, p1}, Lco/uk/getmondo/topup/a/m;-><init>(Lco/uk/getmondo/topup/a/c;Lcom/stripe/android/model/Card;)V

    return-object v0
.end method


# virtual methods
.method public call()Ljava/lang/Object;
    .locals 2

    iget-object v0, p0, Lco/uk/getmondo/topup/a/m;->a:Lco/uk/getmondo/topup/a/c;

    iget-object v1, p0, Lco/uk/getmondo/topup/a/m;->b:Lcom/stripe/android/model/Card;

    invoke-static {v0, v1}, Lco/uk/getmondo/topup/a/c;->a(Lco/uk/getmondo/topup/a/c;Lcom/stripe/android/model/Card;)Lcom/stripe/android/model/Token;

    move-result-object v0

    return-object v0
.end method
