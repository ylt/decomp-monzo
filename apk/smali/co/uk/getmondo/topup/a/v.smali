.class public final enum Lco/uk/getmondo/topup/a/v;
.super Ljava/lang/Enum;
.source "TopUpError.java"

# interfaces
.implements Lco/uk/getmondo/common/e/f;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lco/uk/getmondo/topup/a/v;",
        ">;",
        "Lco/uk/getmondo/common/e/f;"
    }
.end annotation


# static fields
.field public static final enum a:Lco/uk/getmondo/topup/a/v;

.field public static final enum b:Lco/uk/getmondo/topup/a/v;

.field public static final enum c:Lco/uk/getmondo/topup/a/v;

.field public static final enum d:Lco/uk/getmondo/topup/a/v;

.field public static final enum e:Lco/uk/getmondo/topup/a/v;

.field public static final enum f:Lco/uk/getmondo/topup/a/v;

.field public static final enum g:Lco/uk/getmondo/topup/a/v;

.field private static final synthetic j:[Lco/uk/getmondo/topup/a/v;


# instance fields
.field public final h:I

.field private final i:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 10

    .prologue
    const/4 v9, 0x4

    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 17
    new-instance v0, Lco/uk/getmondo/topup/a/v;

    const-string v1, "CARD_ERROR"

    const-string v2, "bad_request.card_error"

    const v3, 0x7f0a03ed

    invoke-direct {v0, v1, v5, v2, v3}, Lco/uk/getmondo/topup/a/v;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, Lco/uk/getmondo/topup/a/v;->a:Lco/uk/getmondo/topup/a/v;

    .line 18
    new-instance v0, Lco/uk/getmondo/topup/a/v;

    const-string v1, "CARD_ERROR_INVALID_ZIP"

    const-string v2, "bad_request.card_error.incorrect_zip"

    const v3, 0x7f0a03f3

    invoke-direct {v0, v1, v6, v2, v3}, Lco/uk/getmondo/topup/a/v;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, Lco/uk/getmondo/topup/a/v;->b:Lco/uk/getmondo/topup/a/v;

    .line 19
    new-instance v0, Lco/uk/getmondo/topup/a/v;

    const-string v1, "CARD_ERROR_UNSUPPORTED"

    const-string v2, "bad_request.card_error.unsupported_card"

    const v3, 0x7f0a03ef

    invoke-direct {v0, v1, v7, v2, v3}, Lco/uk/getmondo/topup/a/v;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, Lco/uk/getmondo/topup/a/v;->c:Lco/uk/getmondo/topup/a/v;

    .line 20
    new-instance v0, Lco/uk/getmondo/topup/a/v;

    const-string v1, "CARD_ERROR_UNSUPPORTED_CREDIT"

    const-string v2, "bad_request.card_error.unsupported_card.credit_card"

    const v3, 0x7f0a03f0

    invoke-direct {v0, v1, v8, v2, v3}, Lco/uk/getmondo/topup/a/v;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, Lco/uk/getmondo/topup/a/v;->d:Lco/uk/getmondo/topup/a/v;

    .line 21
    new-instance v0, Lco/uk/getmondo/topup/a/v;

    const-string v1, "CARD_ERROR_DECLINED"

    const-string v2, "bad_request.card_error.card_declined"

    const v3, 0x7f0a03ee

    invoke-direct {v0, v1, v9, v2, v3}, Lco/uk/getmondo/topup/a/v;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, Lco/uk/getmondo/topup/a/v;->e:Lco/uk/getmondo/topup/a/v;

    .line 22
    new-instance v0, Lco/uk/getmondo/topup/a/v;

    const-string v1, "CARD_ERROR_DECLINED_CREDIT"

    const/4 v2, 0x5

    const-string v3, "bad_request.card_error.card_declined.insufficient_funds"

    const v4, 0x7f0a013d

    invoke-direct {v0, v1, v2, v3, v4}, Lco/uk/getmondo/topup/a/v;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, Lco/uk/getmondo/topup/a/v;->f:Lco/uk/getmondo/topup/a/v;

    .line 23
    new-instance v0, Lco/uk/getmondo/topup/a/v;

    const-string v1, "DUPLICATE_INITIAL_TOP_UP"

    const/4 v2, 0x6

    const-string v3, "bad_request.duplicate_initial_topup"

    const v4, 0x7f0a03f1

    invoke-direct {v0, v1, v2, v3, v4}, Lco/uk/getmondo/topup/a/v;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, Lco/uk/getmondo/topup/a/v;->g:Lco/uk/getmondo/topup/a/v;

    .line 15
    const/4 v0, 0x7

    new-array v0, v0, [Lco/uk/getmondo/topup/a/v;

    sget-object v1, Lco/uk/getmondo/topup/a/v;->a:Lco/uk/getmondo/topup/a/v;

    aput-object v1, v0, v5

    sget-object v1, Lco/uk/getmondo/topup/a/v;->b:Lco/uk/getmondo/topup/a/v;

    aput-object v1, v0, v6

    sget-object v1, Lco/uk/getmondo/topup/a/v;->c:Lco/uk/getmondo/topup/a/v;

    aput-object v1, v0, v7

    sget-object v1, Lco/uk/getmondo/topup/a/v;->d:Lco/uk/getmondo/topup/a/v;

    aput-object v1, v0, v8

    sget-object v1, Lco/uk/getmondo/topup/a/v;->e:Lco/uk/getmondo/topup/a/v;

    aput-object v1, v0, v9

    const/4 v1, 0x5

    sget-object v2, Lco/uk/getmondo/topup/a/v;->f:Lco/uk/getmondo/topup/a/v;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lco/uk/getmondo/topup/a/v;->g:Lco/uk/getmondo/topup/a/v;

    aput-object v2, v0, v1

    sput-object v0, Lco/uk/getmondo/topup/a/v;->j:[Lco/uk/getmondo/topup/a/v;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "I)V"
        }
    .end annotation

    .prologue
    .line 28
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 29
    iput-object p3, p0, Lco/uk/getmondo/topup/a/v;->i:Ljava/lang/String;

    .line 30
    iput p4, p0, Lco/uk/getmondo/topup/a/v;->h:I

    .line 31
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lco/uk/getmondo/topup/a/v;
    .locals 1

    .prologue
    .line 15
    const-class v0, Lco/uk/getmondo/topup/a/v;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/topup/a/v;

    return-object v0
.end method

.method public static values()[Lco/uk/getmondo/topup/a/v;
    .locals 1

    .prologue
    .line 15
    sget-object v0, Lco/uk/getmondo/topup/a/v;->j:[Lco/uk/getmondo/topup/a/v;

    invoke-virtual {v0}, [Lco/uk/getmondo/topup/a/v;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lco/uk/getmondo/topup/a/v;

    return-object v0
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lco/uk/getmondo/topup/a/v;->i:Ljava/lang/String;

    return-object v0
.end method

.method public b()I
    .locals 1

    .prologue
    .line 41
    iget v0, p0, Lco/uk/getmondo/topup/a/v;->h:I

    return v0
.end method
