.class public Lco/uk/getmondo/topup/TopUpActivity;
.super Lco/uk/getmondo/common/activities/b;
.source "TopUpActivity.java"

# interfaces
.implements Lco/uk/getmondo/topup/q$a;


# instance fields
.field a:Lco/uk/getmondo/topup/q;

.field b:Lco/uk/getmondo/common/q;

.field c:Lco/uk/getmondo/common/a/c;

.field decreaseButton:Landroid/widget/ImageButton;
    .annotation build Lbutterknife/BindView;
        value = 0x7f110244
    .end annotation
.end field

.field e:Lco/uk/getmondo/topup/b;

.field expectedBalanceView:Landroid/widget/TextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f110247
    .end annotation
.end field

.field f:Lco/uk/getmondo/common/a;

.field private g:Lco/uk/getmondo/topup/e;

.field increaseButton:Landroid/widget/ImageButton;
    .annotation build Lbutterknife/BindView;
        value = 0x7f110246
    .end annotation
.end field

.field overlayView:Landroid/view/View;
    .annotation build Lbutterknife/BindView;
        value = 0x7f11024b
    .end annotation
.end field

.field progressBar:Landroid/widget/ProgressBar;
    .annotation build Lbutterknife/BindView;
        value = 0x7f11024c
    .end annotation
.end field

.field toLoadAmountView:Lco/uk/getmondo/common/ui/AmountView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f110245
    .end annotation
.end field

.field topUpUnavailableView:Landroid/view/View;
    .annotation build Lbutterknife/BindView;
        value = 0x7f110240
    .end annotation
.end field

.field topUpView:Landroid/view/View;
    .annotation build Lbutterknife/BindView;
        value = 0x7f110242
    .end annotation
.end field

.field topUpWithBankButton:Landroid/widget/Button;
    .annotation build Lbutterknife/BindView;
        value = 0x7f110248
    .end annotation
.end field

.field topUpWithCardButton:Landroid/widget/Button;
    .annotation build Lbutterknife/BindView;
        value = 0x7f110249
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 44
    invoke-direct {p0}, Lco/uk/getmondo/common/activities/b;-><init>()V

    return-void
.end method

.method public static a(Landroid/content/Context;Z)Landroid/content/Intent;
    .locals 4

    .prologue
    .line 77
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.MAIN"

    sget-object v2, Landroid/net/Uri;->EMPTY:Landroid/net/Uri;

    const-class v3, Lco/uk/getmondo/topup/TopUpActivity;

    invoke-direct {v0, v1, v2, p0, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "KEY_FROM_SHORTCUT"

    .line 78
    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    .line 77
    return-object v0
.end method

.method public static a(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 68
    invoke-static {p0}, Lco/uk/getmondo/topup/TopUpActivity;->b(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    .line 69
    invoke-virtual {p0, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 70
    return-void
.end method

.method static synthetic a(Lco/uk/getmondo/topup/TopUpActivity;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 277
    iget-object v0, p0, Lco/uk/getmondo/topup/TopUpActivity;->e:Lco/uk/getmondo/topup/b;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lco/uk/getmondo/topup/b;->a(Lco/uk/getmondo/topup/b$c;)V

    return-void
.end method

.method static synthetic a(Lco/uk/getmondo/topup/TopUpActivity;Lio/reactivex/o;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 276
    iget-object v0, p0, Lco/uk/getmondo/topup/TopUpActivity;->e:Lco/uk/getmondo/topup/b;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    invoke-static {p1}, Lco/uk/getmondo/topup/j;->a(Lio/reactivex/o;)Lco/uk/getmondo/topup/b$c;

    move-result-object v1

    invoke-virtual {v0, v1}, Lco/uk/getmondo/topup/b;->a(Lco/uk/getmondo/topup/b$c;)V

    .line 277
    invoke-static {p0}, Lco/uk/getmondo/topup/k;->a(Lco/uk/getmondo/topup/TopUpActivity;)Lio/reactivex/c/f;

    move-result-object v0

    invoke-interface {p1, v0}, Lio/reactivex/o;->a(Lio/reactivex/c/f;)V

    .line 278
    return-void
.end method

.method static synthetic a(Lio/reactivex/o;)V
    .locals 1

    .prologue
    .line 268
    sget-object v0, Lco/uk/getmondo/common/b/a;->a:Lco/uk/getmondo/common/b/a;

    invoke-interface {p0, v0}, Lio/reactivex/o;->a(Ljava/lang/Object;)V

    return-void
.end method

.method public static b(Landroid/content/Context;)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 73
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lco/uk/getmondo/topup/TopUpActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    return-object v0
.end method

.method static synthetic b(Lco/uk/getmondo/topup/TopUpActivity;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 269
    iget-object v0, p0, Lco/uk/getmondo/topup/TopUpActivity;->e:Lco/uk/getmondo/topup/b;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lco/uk/getmondo/topup/b;->a(Lco/uk/getmondo/topup/b$b;)V

    return-void
.end method

.method static synthetic b(Lco/uk/getmondo/topup/TopUpActivity;Lio/reactivex/o;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 268
    iget-object v0, p0, Lco/uk/getmondo/topup/TopUpActivity;->e:Lco/uk/getmondo/topup/b;

    invoke-static {p1}, Lco/uk/getmondo/topup/l;->a(Lio/reactivex/o;)Lco/uk/getmondo/topup/b$b;

    move-result-object v1

    invoke-virtual {v0, v1}, Lco/uk/getmondo/topup/b;->a(Lco/uk/getmondo/topup/b$b;)V

    .line 269
    invoke-static {p0}, Lco/uk/getmondo/topup/m;->a(Lco/uk/getmondo/topup/TopUpActivity;)Lio/reactivex/c/f;

    move-result-object v0

    invoke-interface {p1, v0}, Lio/reactivex/o;->a(Lio/reactivex/c/f;)V

    .line 270
    return-void
.end method

.method static synthetic c(Lco/uk/getmondo/topup/TopUpActivity;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 261
    iget-object v0, p0, Lco/uk/getmondo/topup/TopUpActivity;->e:Lco/uk/getmondo/topup/b;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lco/uk/getmondo/topup/b;->a(Lco/uk/getmondo/topup/b$d;)V

    return-void
.end method

.method static synthetic c(Lco/uk/getmondo/topup/TopUpActivity;Lio/reactivex/o;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 260
    iget-object v0, p0, Lco/uk/getmondo/topup/TopUpActivity;->e:Lco/uk/getmondo/topup/b;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    invoke-static {p1}, Lco/uk/getmondo/topup/n;->a(Lio/reactivex/o;)Lco/uk/getmondo/topup/b$d;

    move-result-object v1

    invoke-virtual {v0, v1}, Lco/uk/getmondo/topup/b;->a(Lco/uk/getmondo/topup/b$d;)V

    .line 261
    invoke-static {p0}, Lco/uk/getmondo/topup/o;->a(Lco/uk/getmondo/topup/TopUpActivity;)Lio/reactivex/c/f;

    move-result-object v0

    invoke-interface {p1, v0}, Lio/reactivex/o;->a(Lio/reactivex/c/f;)V

    .line 262
    return-void
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    .line 168
    iget-object v0, p0, Lco/uk/getmondo/topup/TopUpActivity;->topUpView:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 169
    iget-object v0, p0, Lco/uk/getmondo/topup/TopUpActivity;->topUpUnavailableView:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 170
    return-void
.end method

.method public a(Lco/uk/getmondo/d/c;)V
    .locals 1

    .prologue
    .line 180
    iget-object v0, p0, Lco/uk/getmondo/topup/TopUpActivity;->toLoadAmountView:Lco/uk/getmondo/common/ui/AmountView;

    invoke-virtual {v0, p1}, Lco/uk/getmondo/common/ui/AmountView;->setAmount(Lco/uk/getmondo/d/c;)V

    .line 181
    iget-object v0, p0, Lco/uk/getmondo/topup/TopUpActivity;->e:Lco/uk/getmondo/topup/b;

    invoke-virtual {v0, p1}, Lco/uk/getmondo/topup/b;->a(Lco/uk/getmondo/d/c;)V

    .line 182
    return-void
.end method

.method public a(Lco/uk/getmondo/d/c;Lco/uk/getmondo/d/ae;)V
    .locals 0

    .prologue
    .line 211
    invoke-static {p0, p1, p2}, Lco/uk/getmondo/topup/card/TopUpWithSavedCardActivity;->a(Landroid/content/Context;Lco/uk/getmondo/d/c;Lco/uk/getmondo/d/ae;)V

    .line 212
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 201
    iget-object v0, p0, Lco/uk/getmondo/topup/TopUpActivity;->expectedBalanceView:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 202
    return-void
.end method

.method public a(Z)V
    .locals 1

    .prologue
    .line 186
    iget-object v0, p0, Lco/uk/getmondo/topup/TopUpActivity;->decreaseButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, p1}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 187
    return-void
.end method

.method public b()V
    .locals 2

    .prologue
    .line 174
    iget-object v0, p0, Lco/uk/getmondo/topup/TopUpActivity;->topUpUnavailableView:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 175
    iget-object v0, p0, Lco/uk/getmondo/topup/TopUpActivity;->topUpView:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 176
    return-void
.end method

.method public b(Lco/uk/getmondo/d/c;)V
    .locals 0

    .prologue
    .line 206
    invoke-static {p0, p1}, Lco/uk/getmondo/topup/card/TopUpWithNewCardActivity;->a(Landroid/content/Context;Lco/uk/getmondo/d/c;)V

    .line 207
    return-void
.end method

.method public b(Z)V
    .locals 1

    .prologue
    .line 191
    iget-object v0, p0, Lco/uk/getmondo/topup/TopUpActivity;->increaseButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, p1}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 192
    return-void
.end method

.method public c()V
    .locals 1

    .prologue
    .line 216
    invoke-static {p0}, Lco/uk/getmondo/topup/bank/TopUpInstructionsActivity;->a(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lco/uk/getmondo/topup/TopUpActivity;->startActivity(Landroid/content/Intent;)V

    .line 217
    return-void
.end method

.method public c(Z)V
    .locals 1

    .prologue
    .line 196
    iget-object v0, p0, Lco/uk/getmondo/topup/TopUpActivity;->topUpWithCardButton:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 197
    return-void
.end method

.method public d()V
    .locals 1

    .prologue
    .line 221
    invoke-static {p0}, Lco/uk/getmondo/main/HomeActivity;->b(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    invoke-static {p0, v0}, Lco/uk/getmondo/common/activities/ConfirmationActivity;->a(Landroid/content/Context;Landroid/content/Intent;)V

    .line 222
    return-void
.end method

.method public e()V
    .locals 1

    .prologue
    .line 226
    const v0, 0x7f0a00db

    invoke-virtual {p0, v0}, Lco/uk/getmondo/topup/TopUpActivity;->b(I)V

    .line 227
    return-void
.end method

.method public f()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 231
    iget-object v0, p0, Lco/uk/getmondo/topup/TopUpActivity;->overlayView:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 232
    iget-object v0, p0, Lco/uk/getmondo/topup/TopUpActivity;->progressBar:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 233
    return-void
.end method

.method public g()V
    .locals 2

    .prologue
    const/16 v1, 0x8

    .line 237
    iget-object v0, p0, Lco/uk/getmondo/topup/TopUpActivity;->overlayView:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 238
    iget-object v0, p0, Lco/uk/getmondo/topup/TopUpActivity;->progressBar:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 239
    return-void
.end method

.method public h()V
    .locals 3

    .prologue
    .line 243
    const v0, 0x7f0a0403

    invoke-virtual {p0, v0}, Lco/uk/getmondo/topup/TopUpActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    const v1, 0x7f0a0402

    invoke-virtual {p0, v1}, Lco/uk/getmondo/topup/TopUpActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Lco/uk/getmondo/common/d/a;->a(Ljava/lang/String;Ljava/lang/String;Z)Lco/uk/getmondo/common/d/a;

    move-result-object v0

    .line 244
    invoke-virtual {p0}, Lco/uk/getmondo/topup/TopUpActivity;->getSupportFragmentManager()Landroid/support/v4/app/n;

    move-result-object v1

    const-string v2, "TAG_ERROR"

    invoke-virtual {v0, v1, v2}, Lco/uk/getmondo/common/d/a;->show(Landroid/support/v4/app/n;Ljava/lang/String;)V

    .line 245
    return-void
.end method

.method public i()Lio/reactivex/n;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 249
    iget-object v0, p0, Lco/uk/getmondo/topup/TopUpActivity;->topUpWithCardButton:Landroid/widget/Button;

    invoke-static {v0}, Lcom/b/a/c/c;->a(Landroid/view/View;)Lio/reactivex/n;

    move-result-object v0

    return-object v0
.end method

.method public j()Lio/reactivex/n;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 254
    iget-object v0, p0, Lco/uk/getmondo/topup/TopUpActivity;->topUpWithBankButton:Landroid/widget/Button;

    invoke-static {v0}, Lcom/b/a/c/c;->a(Landroid/view/View;)Lio/reactivex/n;

    move-result-object v0

    return-object v0
.end method

.method public k()Lio/reactivex/n;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 259
    invoke-static {p0}, Lco/uk/getmondo/topup/g;->a(Lco/uk/getmondo/topup/TopUpActivity;)Lio/reactivex/p;

    move-result-object v0

    invoke-static {v0}, Lio/reactivex/n;->create(Lio/reactivex/p;)Lio/reactivex/n;

    move-result-object v0

    return-object v0
.end method

.method protected o()V
    .locals 2

    .prologue
    .line 136
    invoke-static {p0}, Landroid/support/v4/app/y;->a(Landroid/app/Activity;)Landroid/content/Intent;

    move-result-object v0

    .line 137
    invoke-static {p0, v0}, Landroid/support/v4/app/y;->a(Landroid/app/Activity;Landroid/content/Intent;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p0}, Lco/uk/getmondo/topup/TopUpActivity;->isTaskRoot()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 138
    :cond_0
    invoke-static {p0}, Landroid/support/v4/app/as;->a(Landroid/content/Context;)Landroid/support/v4/app/as;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/support/v4/app/as;->b(Landroid/content/Intent;)Landroid/support/v4/app/as;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/as;->a()V

    .line 142
    :goto_0
    return-void

    .line 140
    :cond_1
    invoke-virtual {p0}, Lco/uk/getmondo/topup/TopUpActivity;->finish()V

    goto :goto_0
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 1

    .prologue
    .line 129
    iget-object v0, p0, Lco/uk/getmondo/topup/TopUpActivity;->e:Lco/uk/getmondo/topup/b;

    invoke-virtual {v0, p1, p2, p3}, Lco/uk/getmondo/topup/b;->a(IILandroid/content/Intent;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 130
    invoke-super {p0, p1, p2, p3}, Lco/uk/getmondo/common/activities/b;->onActivityResult(IILandroid/content/Intent;)V

    .line 132
    :cond_0
    return-void
.end method

.method onContactSupportClicked()V
    .locals 1
    .annotation build Lbutterknife/OnClick;
        value = {
            0x7f110241
        }
    .end annotation

    .prologue
    .line 163
    iget-object v0, p0, Lco/uk/getmondo/topup/TopUpActivity;->b:Lco/uk/getmondo/common/q;

    invoke-virtual {v0}, Lco/uk/getmondo/common/q;->a()V

    .line 164
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    .line 83
    invoke-super {p0, p1}, Lco/uk/getmondo/common/activities/b;->onCreate(Landroid/os/Bundle;)V

    .line 85
    const v0, 0x7f05006c

    invoke-virtual {p0, v0}, Lco/uk/getmondo/topup/TopUpActivity;->setContentView(I)V

    .line 86
    invoke-static {p0}, Lbutterknife/ButterKnife;->bind(Landroid/app/Activity;)Lbutterknife/Unbinder;

    .line 88
    invoke-virtual {p0}, Lco/uk/getmondo/topup/TopUpActivity;->l()Lco/uk/getmondo/common/h/b/b;

    move-result-object v0

    invoke-interface {v0, p0}, Lco/uk/getmondo/common/h/b/b;->a(Lco/uk/getmondo/topup/TopUpActivity;)V

    .line 89
    iget-object v0, p0, Lco/uk/getmondo/topup/TopUpActivity;->a:Lco/uk/getmondo/topup/q;

    invoke-virtual {v0, p0}, Lco/uk/getmondo/topup/q;->a(Lco/uk/getmondo/topup/q$a;)V

    .line 91
    new-instance v0, Lco/uk/getmondo/topup/e;

    invoke-direct {v0}, Lco/uk/getmondo/topup/e;-><init>()V

    iput-object v0, p0, Lco/uk/getmondo/topup/TopUpActivity;->g:Lco/uk/getmondo/topup/e;

    .line 92
    iget-object v0, p0, Lco/uk/getmondo/topup/TopUpActivity;->g:Lco/uk/getmondo/topup/e;

    iget-object v1, p0, Lco/uk/getmondo/topup/TopUpActivity;->increaseButton:Landroid/widget/ImageButton;

    iget-object v2, p0, Lco/uk/getmondo/topup/TopUpActivity;->decreaseButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1, v2}, Lco/uk/getmondo/topup/e;->a(Landroid/view/View;Landroid/view/View;)V

    .line 93
    iget-object v0, p0, Lco/uk/getmondo/topup/TopUpActivity;->g:Lco/uk/getmondo/topup/e;

    new-instance v1, Lco/uk/getmondo/topup/TopUpActivity$1;

    invoke-direct {v1, p0}, Lco/uk/getmondo/topup/TopUpActivity$1;-><init>(Lco/uk/getmondo/topup/TopUpActivity;)V

    invoke-virtual {v0, v1}, Lco/uk/getmondo/topup/e;->a(Lco/uk/getmondo/topup/e$a;)V

    .line 105
    invoke-virtual {p0}, Lco/uk/getmondo/topup/TopUpActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "KEY_FROM_SHORTCUT"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 106
    iget-object v0, p0, Lco/uk/getmondo/topup/TopUpActivity;->c:Lco/uk/getmondo/common/a/c;

    sget-object v1, Lco/uk/getmondo/common/a/b;->b:Lco/uk/getmondo/common/a/b;

    invoke-interface {v0, v1}, Lco/uk/getmondo/common/a/c;->a(Lco/uk/getmondo/common/a/b;)V

    .line 109
    :cond_0
    iget-object v0, p0, Lco/uk/getmondo/topup/TopUpActivity;->e:Lco/uk/getmondo/topup/b;

    new-instance v1, Lco/uk/getmondo/topup/TopUpActivity$2;

    invoke-direct {v1, p0}, Lco/uk/getmondo/topup/TopUpActivity$2;-><init>(Lco/uk/getmondo/topup/TopUpActivity;)V

    invoke-virtual {v0, p0, v1}, Lco/uk/getmondo/topup/b;->a(Landroid/support/v4/app/j;Lco/uk/getmondo/topup/b$a;)V

    .line 124
    iget-object v0, p0, Lco/uk/getmondo/topup/TopUpActivity;->toLoadAmountView:Lco/uk/getmondo/common/ui/AmountView;

    new-instance v1, Lco/uk/getmondo/d/c;

    const-wide/16 v2, 0x3e8

    sget-object v4, Lco/uk/getmondo/common/i/c;->a:Lco/uk/getmondo/common/i/c;

    invoke-direct {v1, v2, v3, v4}, Lco/uk/getmondo/d/c;-><init>(JLco/uk/getmondo/common/i/c;)V

    invoke-virtual {v0, v1}, Lco/uk/getmondo/common/ui/AmountView;->setAmount(Lco/uk/getmondo/d/c;)V

    .line 125
    return-void
.end method

.method onDecreaseClicked()V
    .locals 1
    .annotation build Lbutterknife/OnClick;
        value = {
            0x7f110244
        }
    .end annotation

    .prologue
    .line 158
    iget-object v0, p0, Lco/uk/getmondo/topup/TopUpActivity;->a:Lco/uk/getmondo/topup/q;

    invoke-virtual {v0}, Lco/uk/getmondo/topup/q;->c()Z

    .line 159
    return-void
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 146
    invoke-super {p0}, Lco/uk/getmondo/common/activities/b;->onDestroy()V

    .line 147
    iget-object v0, p0, Lco/uk/getmondo/topup/TopUpActivity;->g:Lco/uk/getmondo/topup/e;

    invoke-virtual {v0}, Lco/uk/getmondo/topup/e;->a()V

    .line 148
    iget-object v0, p0, Lco/uk/getmondo/topup/TopUpActivity;->a:Lco/uk/getmondo/topup/q;

    invoke-virtual {v0}, Lco/uk/getmondo/topup/q;->b()V

    .line 149
    return-void
.end method

.method onIncreaseClicked()V
    .locals 1
    .annotation build Lbutterknife/OnClick;
        value = {
            0x7f110246
        }
    .end annotation

    .prologue
    .line 153
    iget-object v0, p0, Lco/uk/getmondo/topup/TopUpActivity;->a:Lco/uk/getmondo/topup/q;

    invoke-virtual {v0}, Lco/uk/getmondo/topup/q;->a()Z

    .line 154
    return-void
.end method

.method public v()Lio/reactivex/n;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 267
    invoke-static {p0}, Lco/uk/getmondo/topup/h;->a(Lco/uk/getmondo/topup/TopUpActivity;)Lio/reactivex/p;

    move-result-object v0

    invoke-static {v0}, Lio/reactivex/n;->create(Lio/reactivex/p;)Lio/reactivex/n;

    move-result-object v0

    return-object v0
.end method

.method public w()Lio/reactivex/n;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .prologue
    .line 275
    invoke-static {p0}, Lco/uk/getmondo/topup/i;->a(Lco/uk/getmondo/topup/TopUpActivity;)Lio/reactivex/p;

    move-result-object v0

    invoke-static {v0}, Lio/reactivex/n;->create(Lio/reactivex/p;)Lio/reactivex/n;

    move-result-object v0

    return-object v0
.end method
