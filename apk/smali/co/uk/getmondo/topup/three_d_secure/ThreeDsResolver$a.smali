.class public final enum Lco/uk/getmondo/topup/three_d_secure/ThreeDsResolver$a;
.super Ljava/lang/Enum;
.source "ThreeDsResolver.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lco/uk/getmondo/topup/three_d_secure/ThreeDsResolver;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lco/uk/getmondo/topup/three_d_secure/ThreeDsResolver$a;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lco/uk/getmondo/topup/three_d_secure/ThreeDsResolver$a;

.field public static final enum b:Lco/uk/getmondo/topup/three_d_secure/ThreeDsResolver$a;

.field public static final enum c:Lco/uk/getmondo/topup/three_d_secure/ThreeDsResolver$a;

.field public static final enum d:Lco/uk/getmondo/topup/three_d_secure/ThreeDsResolver$a;

.field private static final synthetic e:[Lco/uk/getmondo/topup/three_d_secure/ThreeDsResolver$a;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 17
    new-instance v0, Lco/uk/getmondo/topup/three_d_secure/ThreeDsResolver$a;

    const-string v1, "SUCCESS"

    invoke-direct {v0, v1, v2}, Lco/uk/getmondo/topup/three_d_secure/ThreeDsResolver$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lco/uk/getmondo/topup/three_d_secure/ThreeDsResolver$a;->a:Lco/uk/getmondo/topup/three_d_secure/ThreeDsResolver$a;

    .line 18
    new-instance v0, Lco/uk/getmondo/topup/three_d_secure/ThreeDsResolver$a;

    const-string v1, "FAILURE"

    invoke-direct {v0, v1, v3}, Lco/uk/getmondo/topup/three_d_secure/ThreeDsResolver$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lco/uk/getmondo/topup/three_d_secure/ThreeDsResolver$a;->b:Lco/uk/getmondo/topup/three_d_secure/ThreeDsResolver$a;

    .line 19
    new-instance v0, Lco/uk/getmondo/topup/three_d_secure/ThreeDsResolver$a;

    const-string v1, "CANCELLED"

    invoke-direct {v0, v1, v4}, Lco/uk/getmondo/topup/three_d_secure/ThreeDsResolver$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lco/uk/getmondo/topup/three_d_secure/ThreeDsResolver$a;->c:Lco/uk/getmondo/topup/three_d_secure/ThreeDsResolver$a;

    .line 20
    new-instance v0, Lco/uk/getmondo/topup/three_d_secure/ThreeDsResolver$a;

    const-string v1, "UNKNOWN"

    invoke-direct {v0, v1, v5}, Lco/uk/getmondo/topup/three_d_secure/ThreeDsResolver$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lco/uk/getmondo/topup/three_d_secure/ThreeDsResolver$a;->d:Lco/uk/getmondo/topup/three_d_secure/ThreeDsResolver$a;

    .line 16
    const/4 v0, 0x4

    new-array v0, v0, [Lco/uk/getmondo/topup/three_d_secure/ThreeDsResolver$a;

    sget-object v1, Lco/uk/getmondo/topup/three_d_secure/ThreeDsResolver$a;->a:Lco/uk/getmondo/topup/three_d_secure/ThreeDsResolver$a;

    aput-object v1, v0, v2

    sget-object v1, Lco/uk/getmondo/topup/three_d_secure/ThreeDsResolver$a;->b:Lco/uk/getmondo/topup/three_d_secure/ThreeDsResolver$a;

    aput-object v1, v0, v3

    sget-object v1, Lco/uk/getmondo/topup/three_d_secure/ThreeDsResolver$a;->c:Lco/uk/getmondo/topup/three_d_secure/ThreeDsResolver$a;

    aput-object v1, v0, v4

    sget-object v1, Lco/uk/getmondo/topup/three_d_secure/ThreeDsResolver$a;->d:Lco/uk/getmondo/topup/three_d_secure/ThreeDsResolver$a;

    aput-object v1, v0, v5

    sput-object v0, Lco/uk/getmondo/topup/three_d_secure/ThreeDsResolver$a;->e:[Lco/uk/getmondo/topup/three_d_secure/ThreeDsResolver$a;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 16
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lco/uk/getmondo/topup/three_d_secure/ThreeDsResolver$a;
    .locals 1

    .prologue
    .line 16
    const-class v0, Lco/uk/getmondo/topup/three_d_secure/ThreeDsResolver$a;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/topup/three_d_secure/ThreeDsResolver$a;

    return-object v0
.end method

.method public static values()[Lco/uk/getmondo/topup/three_d_secure/ThreeDsResolver$a;
    .locals 1

    .prologue
    .line 16
    sget-object v0, Lco/uk/getmondo/topup/three_d_secure/ThreeDsResolver$a;->e:[Lco/uk/getmondo/topup/three_d_secure/ThreeDsResolver$a;

    invoke-virtual {v0}, [Lco/uk/getmondo/topup/three_d_secure/ThreeDsResolver$a;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lco/uk/getmondo/topup/three_d_secure/ThreeDsResolver$a;

    return-object v0
.end method
