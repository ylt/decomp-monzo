.class public Lco/uk/getmondo/topup/three_d_secure/ThreeDsResolver$ThreeDsUnsuccessfulException;
.super Ljava/lang/RuntimeException;
.source "ThreeDsResolver.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lco/uk/getmondo/topup/three_d_secure/ThreeDsResolver;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ThreeDsUnsuccessfulException"
.end annotation


# instance fields
.field public final a:Lco/uk/getmondo/topup/three_d_secure/ThreeDsResolver$a;


# direct methods
.method public constructor <init>(Lco/uk/getmondo/topup/three_d_secure/ThreeDsResolver$a;)V
    .locals 2

    .prologue
    .line 28
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Problem resolving 3DS redirection. Result="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    .line 29
    iput-object p1, p0, Lco/uk/getmondo/topup/three_d_secure/ThreeDsResolver$ThreeDsUnsuccessfulException;->a:Lco/uk/getmondo/topup/three_d_secure/ThreeDsResolver$a;

    .line 30
    return-void
.end method
