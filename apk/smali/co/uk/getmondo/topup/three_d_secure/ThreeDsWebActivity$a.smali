.class Lco/uk/getmondo/topup/three_d_secure/ThreeDsWebActivity$a;
.super Landroid/webkit/WebViewClient;
.source "ThreeDsWebActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lco/uk/getmondo/topup/three_d_secure/ThreeDsWebActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "a"
.end annotation


# instance fields
.field final synthetic a:Lco/uk/getmondo/topup/three_d_secure/ThreeDsWebActivity;


# direct methods
.method private constructor <init>(Lco/uk/getmondo/topup/three_d_secure/ThreeDsWebActivity;)V
    .locals 0

    .prologue
    .line 114
    iput-object p1, p0, Lco/uk/getmondo/topup/three_d_secure/ThreeDsWebActivity$a;->a:Lco/uk/getmondo/topup/three_d_secure/ThreeDsWebActivity;

    invoke-direct {p0}, Landroid/webkit/WebViewClient;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lco/uk/getmondo/topup/three_d_secure/ThreeDsWebActivity;Lco/uk/getmondo/topup/three_d_secure/ThreeDsWebActivity$1;)V
    .locals 0

    .prologue
    .line 114
    invoke-direct {p0, p1}, Lco/uk/getmondo/topup/three_d_secure/ThreeDsWebActivity$a;-><init>(Lco/uk/getmondo/topup/three_d_secure/ThreeDsWebActivity;)V

    return-void
.end method


# virtual methods
.method public onReceivedError(Landroid/webkit/WebView;ILjava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 159
    invoke-super {p0, p1, p2, p3, p4}, Landroid/webkit/WebViewClient;->onReceivedError(Landroid/webkit/WebView;ILjava/lang/String;Ljava/lang/String;)V

    .line 161
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "generic_error_dep, code: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", description: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", failingUrl: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 162
    iget-object v1, p0, Lco/uk/getmondo/topup/three_d_secure/ThreeDsWebActivity$a;->a:Lco/uk/getmondo/topup/three_d_secure/ThreeDsWebActivity;

    iget-object v1, v1, Lco/uk/getmondo/topup/three_d_secure/ThreeDsWebActivity;->a:Lco/uk/getmondo/common/a;

    iget-object v2, p0, Lco/uk/getmondo/topup/three_d_secure/ThreeDsWebActivity$a;->a:Lco/uk/getmondo/topup/three_d_secure/ThreeDsWebActivity;

    invoke-static {v2}, Lco/uk/getmondo/topup/three_d_secure/ThreeDsWebActivity;->b(Lco/uk/getmondo/topup/three_d_secure/ThreeDsWebActivity;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, v0}, Lco/uk/getmondo/api/model/tracking/Impression;->a(Ljava/lang/String;Ljava/lang/String;)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    invoke-virtual {v1, v0}, Lco/uk/getmondo/common/a;->a(Lco/uk/getmondo/api/model/tracking/Impression;)V

    .line 163
    return-void
.end method

.method public onReceivedError(Landroid/webkit/WebView;Landroid/webkit/WebResourceRequest;Landroid/webkit/WebResourceError;)V
    .locals 3

    .prologue
    .line 146
    invoke-super {p0, p1, p2, p3}, Landroid/webkit/WebViewClient;->onReceivedError(Landroid/webkit/WebView;Landroid/webkit/WebResourceRequest;Landroid/webkit/WebResourceError;)V

    .line 148
    const-string v0, "generic_error"

    .line 149
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x17

    if-lt v1, v2, :cond_0

    .line 150
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", code: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p3}, Landroid/webkit/WebResourceError;->getErrorCode()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", description: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p3}, Landroid/webkit/WebResourceError;->getDescription()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 153
    :cond_0
    iget-object v1, p0, Lco/uk/getmondo/topup/three_d_secure/ThreeDsWebActivity$a;->a:Lco/uk/getmondo/topup/three_d_secure/ThreeDsWebActivity;

    iget-object v1, v1, Lco/uk/getmondo/topup/three_d_secure/ThreeDsWebActivity;->a:Lco/uk/getmondo/common/a;

    iget-object v2, p0, Lco/uk/getmondo/topup/three_d_secure/ThreeDsWebActivity$a;->a:Lco/uk/getmondo/topup/three_d_secure/ThreeDsWebActivity;

    invoke-static {v2}, Lco/uk/getmondo/topup/three_d_secure/ThreeDsWebActivity;->b(Lco/uk/getmondo/topup/three_d_secure/ThreeDsWebActivity;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, v0}, Lco/uk/getmondo/api/model/tracking/Impression;->a(Ljava/lang/String;Ljava/lang/String;)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    invoke-virtual {v1, v0}, Lco/uk/getmondo/common/a;->a(Lco/uk/getmondo/api/model/tracking/Impression;)V

    .line 154
    return-void
.end method

.method public onReceivedHttpError(Landroid/webkit/WebView;Landroid/webkit/WebResourceRequest;Landroid/webkit/WebResourceResponse;)V
    .locals 3

    .prologue
    .line 138
    invoke-super {p0, p1, p2, p3}, Landroid/webkit/WebViewClient;->onReceivedHttpError(Landroid/webkit/WebView;Landroid/webkit/WebResourceRequest;Landroid/webkit/WebResourceResponse;)V

    .line 140
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "http_error, code: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p3}, Landroid/webkit/WebResourceResponse;->getStatusCode()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p3}, Landroid/webkit/WebResourceResponse;->getReasonPhrase()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 141
    iget-object v1, p0, Lco/uk/getmondo/topup/three_d_secure/ThreeDsWebActivity$a;->a:Lco/uk/getmondo/topup/three_d_secure/ThreeDsWebActivity;

    iget-object v1, v1, Lco/uk/getmondo/topup/three_d_secure/ThreeDsWebActivity;->a:Lco/uk/getmondo/common/a;

    iget-object v2, p0, Lco/uk/getmondo/topup/three_d_secure/ThreeDsWebActivity$a;->a:Lco/uk/getmondo/topup/three_d_secure/ThreeDsWebActivity;

    invoke-static {v2}, Lco/uk/getmondo/topup/three_d_secure/ThreeDsWebActivity;->b(Lco/uk/getmondo/topup/three_d_secure/ThreeDsWebActivity;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, v0}, Lco/uk/getmondo/api/model/tracking/Impression;->a(Ljava/lang/String;Ljava/lang/String;)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    invoke-virtual {v1, v0}, Lco/uk/getmondo/common/a;->a(Lco/uk/getmondo/api/model/tracking/Impression;)V

    .line 142
    return-void
.end method

.method public onReceivedSslError(Landroid/webkit/WebView;Landroid/webkit/SslErrorHandler;Landroid/net/http/SslError;)V
    .locals 3

    .prologue
    .line 130
    invoke-super {p0, p1, p2, p3}, Landroid/webkit/WebViewClient;->onReceivedSslError(Landroid/webkit/WebView;Landroid/webkit/SslErrorHandler;Landroid/net/http/SslError;)V

    .line 132
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "ssl_error: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p3}, Landroid/net/http/SslError;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 133
    iget-object v1, p0, Lco/uk/getmondo/topup/three_d_secure/ThreeDsWebActivity$a;->a:Lco/uk/getmondo/topup/three_d_secure/ThreeDsWebActivity;

    iget-object v1, v1, Lco/uk/getmondo/topup/three_d_secure/ThreeDsWebActivity;->a:Lco/uk/getmondo/common/a;

    iget-object v2, p0, Lco/uk/getmondo/topup/three_d_secure/ThreeDsWebActivity$a;->a:Lco/uk/getmondo/topup/three_d_secure/ThreeDsWebActivity;

    invoke-static {v2}, Lco/uk/getmondo/topup/three_d_secure/ThreeDsWebActivity;->b(Lco/uk/getmondo/topup/three_d_secure/ThreeDsWebActivity;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, v0}, Lco/uk/getmondo/api/model/tracking/Impression;->a(Ljava/lang/String;Ljava/lang/String;)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    invoke-virtual {v1, v0}, Lco/uk/getmondo/common/a;->a(Lco/uk/getmondo/api/model/tracking/Impression;)V

    .line 134
    return-void
.end method

.method public shouldOverrideUrlLoading(Landroid/webkit/WebView;Ljava/lang/String;)Z
    .locals 5

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 118
    invoke-static {p2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 119
    invoke-virtual {v2}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lco/uk/getmondo/topup/three_d_secure/ThreeDsWebActivity$a;->a:Lco/uk/getmondo/topup/three_d_secure/ThreeDsWebActivity;

    invoke-static {v4}, Lco/uk/getmondo/topup/three_d_secure/ThreeDsWebActivity;->a(Lco/uk/getmondo/topup/three_d_secure/ThreeDsWebActivity;)Landroid/net/Uri;

    move-result-object v4

    invoke-virtual {v4}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 120
    const-string v3, "3DS return url intercepted: %s"

    new-array v4, v0, [Ljava/lang/Object;

    aput-object p2, v4, v1

    invoke-static {v3, v4}, Ld/a/a;->c(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 121
    iget-object v1, p0, Lco/uk/getmondo/topup/three_d_secure/ThreeDsWebActivity$a;->a:Lco/uk/getmondo/topup/three_d_secure/ThreeDsWebActivity;

    invoke-static {v1, v2}, Lco/uk/getmondo/topup/three_d_secure/ThreeDsWebActivity;->a(Lco/uk/getmondo/topup/three_d_secure/ThreeDsWebActivity;Landroid/net/Uri;)V

    .line 125
    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method
