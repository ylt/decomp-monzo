.class public Lco/uk/getmondo/topup/three_d_secure/b;
.super Ljava/lang/Object;
.source "ThreeDsWebResolver.java"

# interfaces
.implements Lco/uk/getmondo/topup/three_d_secure/ThreeDsResolver;


# instance fields
.field private a:Landroid/app/Activity;

.field private b:Landroid/support/v4/app/Fragment;

.field private c:Lco/uk/getmondo/common/a;

.field private d:Lio/reactivex/w;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/w",
            "<",
            "Lco/uk/getmondo/topup/three_d_secure/ThreeDsResolver$a;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/app/Activity;Lco/uk/getmondo/common/a;)V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    iput-object p1, p0, Lco/uk/getmondo/topup/three_d_secure/b;->a:Landroid/app/Activity;

    .line 31
    iput-object p2, p0, Lco/uk/getmondo/topup/three_d_secure/b;->c:Lco/uk/getmondo/common/a;

    .line 32
    return-void
.end method

.method public constructor <init>(Landroid/support/v4/app/Fragment;Lco/uk/getmondo/common/a;)V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    iput-object p1, p0, Lco/uk/getmondo/topup/three_d_secure/b;->b:Landroid/support/v4/app/Fragment;

    .line 36
    iput-object p2, p0, Lco/uk/getmondo/topup/three_d_secure/b;->c:Lco/uk/getmondo/common/a;

    .line 37
    return-void
.end method

.method private a(Lco/uk/getmondo/topup/three_d_secure/ThreeDsResolver$a;Z)V
    .locals 2

    .prologue
    .line 82
    if-nez p1, :cond_0

    .line 94
    :goto_0
    return-void

    .line 86
    :cond_0
    sget-object v0, Lco/uk/getmondo/topup/three_d_secure/b$1;->a:[I

    invoke-virtual {p1}, Lco/uk/getmondo/topup/three_d_secure/ThreeDsResolver$a;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 88
    :pswitch_0
    iget-object v0, p0, Lco/uk/getmondo/topup/three_d_secure/b;->c:Lco/uk/getmondo/common/a;

    invoke-static {p2}, Lco/uk/getmondo/api/model/tracking/Impression;->d(Z)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v1

    invoke-virtual {v0, v1}, Lco/uk/getmondo/common/a;->a(Lco/uk/getmondo/api/model/tracking/Impression;)V

    goto :goto_0

    .line 91
    :pswitch_1
    iget-object v0, p0, Lco/uk/getmondo/topup/three_d_secure/b;->c:Lco/uk/getmondo/common/a;

    invoke-static {p2}, Lco/uk/getmondo/api/model/tracking/Impression;->c(Z)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v1

    invoke-virtual {v0, v1}, Lco/uk/getmondo/common/a;->a(Lco/uk/getmondo/api/model/tracking/Impression;)V

    goto :goto_0

    .line 86
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method static synthetic a(Lco/uk/getmondo/topup/three_d_secure/b;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 54
    const/4 v0, 0x0

    iput-object v0, p0, Lco/uk/getmondo/topup/three_d_secure/b;->d:Lio/reactivex/w;

    return-void
.end method

.method static synthetic a(Lco/uk/getmondo/topup/three_d_secure/b;ZLco/uk/getmondo/topup/three_d_secure/ThreeDsResolver$a;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 56
    invoke-direct {p0, p2, p1}, Lco/uk/getmondo/topup/three_d_secure/b;->a(Lco/uk/getmondo/topup/three_d_secure/ThreeDsResolver$a;Z)V

    .line 57
    iget-object v0, p0, Lco/uk/getmondo/topup/three_d_secure/b;->c:Lco/uk/getmondo/common/a;

    invoke-static {p1}, Lco/uk/getmondo/api/model/tracking/Impression;->e(Z)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v1

    invoke-virtual {v0, v1}, Lco/uk/getmondo/common/a;->a(Lco/uk/getmondo/api/model/tracking/Impression;)V

    .line 58
    return-void
.end method

.method static synthetic a(Lco/uk/getmondo/topup/three_d_secure/b;ZLjava/lang/String;Ljava/lang/String;Lio/reactivex/w;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    const/16 v2, 0x1f5

    .line 42
    iget-object v0, p0, Lco/uk/getmondo/topup/three_d_secure/b;->c:Lco/uk/getmondo/common/a;

    invoke-static {p1}, Lco/uk/getmondo/api/model/tracking/Impression;->b(Z)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v1

    invoke-virtual {v0, v1}, Lco/uk/getmondo/common/a;->a(Lco/uk/getmondo/api/model/tracking/Impression;)V

    .line 43
    iget-object v0, p0, Lco/uk/getmondo/topup/three_d_secure/b;->b:Landroid/support/v4/app/Fragment;

    if-eqz v0, :cond_0

    .line 44
    iget-object v0, p0, Lco/uk/getmondo/topup/three_d_secure/b;->b:Landroid/support/v4/app/Fragment;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, p2, p3, p1}, Lco/uk/getmondo/topup/three_d_secure/ThreeDsWebActivity;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    .line 45
    iget-object v1, p0, Lco/uk/getmondo/topup/three_d_secure/b;->b:Landroid/support/v4/app/Fragment;

    invoke-virtual {v1, v0, v2}, Landroid/support/v4/app/Fragment;->startActivityForResult(Landroid/content/Intent;I)V

    .line 53
    :goto_0
    iput-object p4, p0, Lco/uk/getmondo/topup/three_d_secure/b;->d:Lio/reactivex/w;

    .line 54
    invoke-static {p0}, Lco/uk/getmondo/topup/three_d_secure/e;->a(Lco/uk/getmondo/topup/three_d_secure/b;)Lio/reactivex/c/f;

    move-result-object v0

    invoke-interface {p4, v0}, Lio/reactivex/w;->a(Lio/reactivex/c/f;)V

    .line 55
    return-void

    .line 46
    :cond_0
    iget-object v0, p0, Lco/uk/getmondo/topup/three_d_secure/b;->a:Landroid/app/Activity;

    if-eqz v0, :cond_1

    .line 47
    iget-object v0, p0, Lco/uk/getmondo/topup/three_d_secure/b;->a:Landroid/app/Activity;

    invoke-static {v0, p2, p3, p1}, Lco/uk/getmondo/topup/three_d_secure/ThreeDsWebActivity;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    .line 48
    iget-object v1, p0, Lco/uk/getmondo/topup/three_d_secure/b;->a:Landroid/app/Activity;

    invoke-virtual {v1, v0, v2}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0

    .line 50
    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "ThreeDsWebResolver requires a Fragment or Activity to launch ThreeDsWebActivity"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method public a(Ljava/lang/String;Ljava/lang/String;Z)Lio/reactivex/v;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Z)",
            "Lio/reactivex/v",
            "<",
            "Lco/uk/getmondo/topup/three_d_secure/ThreeDsResolver$a;",
            ">;"
        }
    .end annotation

    .prologue
    .line 41
    invoke-static {p0, p3, p1, p2}, Lco/uk/getmondo/topup/three_d_secure/c;->a(Lco/uk/getmondo/topup/three_d_secure/b;ZLjava/lang/String;Ljava/lang/String;)Lio/reactivex/y;

    move-result-object v0

    invoke-static {v0}, Lio/reactivex/v;->a(Lio/reactivex/y;)Lio/reactivex/v;

    move-result-object v0

    invoke-static {p0, p3}, Lco/uk/getmondo/topup/three_d_secure/d;->a(Lco/uk/getmondo/topup/three_d_secure/b;Z)Lio/reactivex/c/g;

    move-result-object v1

    .line 55
    invoke-virtual {v0, v1}, Lio/reactivex/v;->c(Lio/reactivex/c/g;)Lio/reactivex/v;

    move-result-object v0

    .line 58
    invoke-static {}, Lio/reactivex/a/b/a;->a()Lio/reactivex/u;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/v;->b(Lio/reactivex/u;)Lio/reactivex/v;

    move-result-object v0

    .line 41
    return-object v0
.end method

.method public a(IILandroid/content/Intent;)Z
    .locals 2

    .prologue
    .line 68
    iget-object v0, p0, Lco/uk/getmondo/topup/three_d_secure/b;->d:Lio/reactivex/w;

    if-eqz v0, :cond_0

    const/16 v0, 0x1f5

    if-eq v0, p1, :cond_1

    .line 69
    :cond_0
    const/4 v0, 0x0

    .line 78
    :goto_0
    return v0

    .line 73
    :cond_1
    :try_start_0
    iget-object v1, p0, Lco/uk/getmondo/topup/three_d_secure/b;->d:Lio/reactivex/w;

    if-nez p2, :cond_2

    sget-object v0, Lco/uk/getmondo/topup/three_d_secure/ThreeDsResolver$a;->c:Lco/uk/getmondo/topup/three_d_secure/ThreeDsResolver$a;

    :goto_1
    invoke-interface {v1, v0}, Lio/reactivex/w;->a(Ljava/lang/Object;)V

    .line 78
    :goto_2
    const/4 v0, 0x1

    goto :goto_0

    .line 73
    :cond_2
    invoke-static {p3}, Lco/uk/getmondo/topup/three_d_secure/ThreeDsWebActivity;->a(Landroid/content/Intent;)Lco/uk/getmondo/topup/three_d_secure/ThreeDsResolver$a;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_1

    .line 74
    :catch_0
    move-exception v0

    .line 75
    iget-object v1, p0, Lco/uk/getmondo/topup/three_d_secure/b;->d:Lio/reactivex/w;

    invoke-interface {v1, v0}, Lio/reactivex/w;->a(Ljava/lang/Throwable;)V

    goto :goto_2
.end method
