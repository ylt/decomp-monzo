.class public Lco/uk/getmondo/topup/three_d_secure/ThreeDsWebActivity;
.super Lco/uk/getmondo/common/activities/b;
.source "ThreeDsWebActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lco/uk/getmondo/topup/three_d_secure/ThreeDsWebActivity$a;
    }
.end annotation


# instance fields
.field a:Lco/uk/getmondo/common/a;

.field private b:Landroid/net/Uri;

.field private c:Ljava/lang/String;

.field webView:Landroid/webkit/WebView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f11023d
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Lco/uk/getmondo/common/activities/b;-><init>()V

    return-void
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Z)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 45
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lco/uk/getmondo/topup/three_d_secure/ThreeDsWebActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 46
    const-string v1, "EXTRA_REDIRECT_URL"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 47
    const-string v1, "EXTRA_RETURN_URL"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 48
    const-string v1, "EXTRA_INITIAL_TOP_UP"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 49
    return-object v0
.end method

.method static synthetic a(Lco/uk/getmondo/topup/three_d_secure/ThreeDsWebActivity;)Landroid/net/Uri;
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, Lco/uk/getmondo/topup/three_d_secure/ThreeDsWebActivity;->b:Landroid/net/Uri;

    return-object v0
.end method

.method public static a(Landroid/content/Intent;)Lco/uk/getmondo/topup/three_d_secure/ThreeDsResolver$a;
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 53
    const-string v0, "status"

    invoke-virtual {p0, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 54
    sget-object v0, Lco/uk/getmondo/topup/three_d_secure/ThreeDsResolver$a;->d:Lco/uk/getmondo/topup/three_d_secure/ThreeDsResolver$a;

    .line 65
    :goto_0
    return-object v0

    .line 57
    :cond_0
    const-string v0, "status"

    invoke-virtual {p0, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 58
    const/4 v0, -0x1

    invoke-virtual {v3}, Ljava/lang/String;->hashCode()I

    move-result v4

    sparse-switch v4, :sswitch_data_0

    :cond_1
    :goto_1
    packed-switch v0, :pswitch_data_0

    .line 64
    const-string v0, "Unknown 3DS status found, status=%s"

    new-array v2, v2, [Ljava/lang/Object;

    aput-object v3, v2, v1

    invoke-static {v0, v2}, Ld/a/a;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 65
    sget-object v0, Lco/uk/getmondo/topup/three_d_secure/ThreeDsResolver$a;->d:Lco/uk/getmondo/topup/three_d_secure/ThreeDsResolver$a;

    goto :goto_0

    .line 58
    :sswitch_0
    const-string v4, "succeeded"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    move v0, v1

    goto :goto_1

    :sswitch_1
    const-string v4, "failed"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    move v0, v2

    goto :goto_1

    .line 60
    :pswitch_0
    sget-object v0, Lco/uk/getmondo/topup/three_d_secure/ThreeDsResolver$a;->a:Lco/uk/getmondo/topup/three_d_secure/ThreeDsResolver$a;

    goto :goto_0

    .line 62
    :pswitch_1
    sget-object v0, Lco/uk/getmondo/topup/three_d_secure/ThreeDsResolver$a;->b:Lco/uk/getmondo/topup/three_d_secure/ThreeDsResolver$a;

    goto :goto_0

    .line 58
    :sswitch_data_0
    .sparse-switch
        -0x4c696bc3 -> :sswitch_1
        0x385ec261 -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private a(Landroid/net/Uri;)V
    .locals 3

    .prologue
    .line 107
    const-string v0, "status"

    invoke-virtual {p1, v0}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 108
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 109
    const-string v2, "status"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 110
    const/4 v0, -0x1

    invoke-virtual {p0, v0, v1}, Lco/uk/getmondo/topup/three_d_secure/ThreeDsWebActivity;->setResult(ILandroid/content/Intent;)V

    .line 111
    invoke-virtual {p0}, Lco/uk/getmondo/topup/three_d_secure/ThreeDsWebActivity;->finish()V

    .line 112
    return-void
.end method

.method static synthetic a(Lco/uk/getmondo/topup/three_d_secure/ThreeDsWebActivity;Landroid/net/Uri;)V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0, p1}, Lco/uk/getmondo/topup/three_d_secure/ThreeDsWebActivity;->a(Landroid/net/Uri;)V

    return-void
.end method

.method static synthetic b(Lco/uk/getmondo/topup/three_d_secure/ThreeDsWebActivity;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, Lco/uk/getmondo/topup/three_d_secure/ThreeDsWebActivity;->c:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method protected o()V
    .locals 0

    .prologue
    .line 103
    invoke-virtual {p0}, Lco/uk/getmondo/topup/three_d_secure/ThreeDsWebActivity;->finish()V

    .line 104
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 7
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "SetJavaScriptEnabled"
        }
    .end annotation

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 72
    invoke-super {p0, p1}, Lco/uk/getmondo/common/activities/b;->onCreate(Landroid/os/Bundle;)V

    .line 74
    invoke-virtual {p0}, Lco/uk/getmondo/topup/three_d_secure/ThreeDsWebActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "EXTRA_REDIRECT_URL"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/topup/three_d_secure/ThreeDsWebActivity;->c:Ljava/lang/String;

    .line 75
    invoke-virtual {p0}, Lco/uk/getmondo/topup/three_d_secure/ThreeDsWebActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "EXTRA_INITIAL_TOP_UP"

    invoke-virtual {v0, v1, v5}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    .line 76
    invoke-virtual {p0}, Lco/uk/getmondo/topup/three_d_secure/ThreeDsWebActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "EXTRA_RETURN_URL"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iput-object v1, p0, Lco/uk/getmondo/topup/three_d_secure/ThreeDsWebActivity;->b:Landroid/net/Uri;

    .line 77
    iget-object v1, p0, Lco/uk/getmondo/topup/three_d_secure/ThreeDsWebActivity;->c:Ljava/lang/String;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lco/uk/getmondo/topup/three_d_secure/ThreeDsWebActivity;->b:Landroid/net/Uri;

    if-nez v1, :cond_1

    .line 78
    :cond_0
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Redirect url and return url are required"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 81
    :cond_1
    const v1, 0x7f05006a

    invoke-virtual {p0, v1}, Lco/uk/getmondo/topup/three_d_secure/ThreeDsWebActivity;->setContentView(I)V

    .line 82
    invoke-static {p0}, Lbutterknife/ButterKnife;->bind(Landroid/app/Activity;)Lbutterknife/Unbinder;

    .line 83
    invoke-virtual {p0}, Lco/uk/getmondo/topup/three_d_secure/ThreeDsWebActivity;->l()Lco/uk/getmondo/common/h/b/b;

    move-result-object v1

    invoke-interface {v1, p0}, Lco/uk/getmondo/common/h/b/b;->a(Lco/uk/getmondo/topup/three_d_secure/ThreeDsWebActivity;)V

    .line 85
    invoke-virtual {p0}, Lco/uk/getmondo/topup/three_d_secure/ThreeDsWebActivity;->getSupportActionBar()Landroid/support/v7/app/a;

    move-result-object v1

    .line 86
    if-eqz v1, :cond_2

    .line 87
    invoke-virtual {v1, v4}, Landroid/support/v7/app/a;->b(Z)V

    .line 88
    const v2, 0x7f02014c

    invoke-virtual {v1, v2}, Landroid/support/v7/app/a;->a(I)V

    .line 91
    :cond_2
    const-string v1, "Redirecting to url %s"

    new-array v2, v4, [Ljava/lang/Object;

    iget-object v3, p0, Lco/uk/getmondo/topup/three_d_secure/ThreeDsWebActivity;->c:Ljava/lang/String;

    aput-object v3, v2, v5

    invoke-static {v1, v2}, Ld/a/a;->c(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 93
    iget-object v1, p0, Lco/uk/getmondo/topup/three_d_secure/ThreeDsWebActivity;->webView:Landroid/webkit/WebView;

    invoke-virtual {v1}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/webkit/WebSettings;->setJavaScriptEnabled(Z)V

    .line 94
    iget-object v1, p0, Lco/uk/getmondo/topup/three_d_secure/ThreeDsWebActivity;->webView:Landroid/webkit/WebView;

    new-instance v2, Lco/uk/getmondo/topup/three_d_secure/ThreeDsWebActivity$a;

    invoke-direct {v2, p0, v6}, Lco/uk/getmondo/topup/three_d_secure/ThreeDsWebActivity$a;-><init>(Lco/uk/getmondo/topup/three_d_secure/ThreeDsWebActivity;Lco/uk/getmondo/topup/three_d_secure/ThreeDsWebActivity$1;)V

    invoke-virtual {v1, v2}, Landroid/webkit/WebView;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    .line 96
    iget-object v1, p0, Lco/uk/getmondo/topup/three_d_secure/ThreeDsWebActivity;->a:Lco/uk/getmondo/common/a;

    iget-object v2, p0, Lco/uk/getmondo/topup/three_d_secure/ThreeDsWebActivity;->c:Ljava/lang/String;

    invoke-static {v0, v2}, Lco/uk/getmondo/api/model/tracking/Impression;->a(ZLjava/lang/String;)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    invoke-virtual {v1, v0}, Lco/uk/getmondo/common/a;->a(Lco/uk/getmondo/api/model/tracking/Impression;)V

    .line 97
    iget-object v0, p0, Lco/uk/getmondo/topup/three_d_secure/ThreeDsWebActivity;->webView:Landroid/webkit/WebView;

    iget-object v1, p0, Lco/uk/getmondo/topup/three_d_secure/ThreeDsWebActivity;->c:Ljava/lang/String;

    invoke-virtual {v0, v1, v6}, Landroid/webkit/WebView;->postUrl(Ljava/lang/String;[B)V

    .line 98
    return-void
.end method
