.class public final Lco/uk/getmondo/c$a;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lco/uk/getmondo/c;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation


# static fields
.field public static final ALT:I = 0x7f110090

.field public static final CTRL:I = 0x7f110091

.field public static final FUNCTION:I = 0x7f110092

.field public static final META:I = 0x7f110093

.field public static final SHIFT:I = 0x7f110094

.field public static final SYM:I = 0x7f110095

.field public static final aboutMonzoView:I = 0x7f11020d

.field public static final accountNumberTextView:I = 0x7f1104a7

.field public static final accountNumberViewGroup:I = 0x7f1104a6

.field public static final accountTextView:I = 0x7f11043e

.field public static final account_number_edit_text:I = 0x7f11035b

.field public static final account_number_input_layout:I = 0x7f11035a

.field public static final account_number_textview:I = 0x7f1101d6

.field public static final action0:I = 0x7f110442

.field public static final action_bar:I = 0x7f1100eb

.field public static final action_bar_activity_content:I = 0x7f110000

.field public static final action_bar_container:I = 0x7f1100ea

.field public static final action_bar_root:I = 0x7f1100e6

.field public static final action_bar_spinner:I = 0x7f110001

.field public static final action_bar_subtitle:I = 0x7f1100ca

.field public static final action_bar_title:I = 0x7f1100c9

.field public static final action_button:I = 0x7f1103c0

.field public static final action_container:I = 0x7f11043f

.field public static final action_context_bar:I = 0x7f1100ec

.field public static final action_divider:I = 0x7f110446

.field public static final action_image:I = 0x7f110440

.field public static final action_menu_divider:I = 0x7f110002

.field public static final action_menu_presenter:I = 0x7f110003

.field public static final action_mode_bar:I = 0x7f1100e8

.field public static final action_mode_bar_stub:I = 0x7f1100e7

.field public static final action_mode_close_button:I = 0x7f1100cb

.field public static final action_select_from_device:I = 0x7f110105

.field public static final action_text:I = 0x7f110441

.field public static final actions:I = 0x7f11044e

.field public static final activateCardActionView:I = 0x7f1102da

.field public static final activateCardEditText:I = 0x7f1100ff

.field public static final activateCardImageView:I = 0x7f1100fa

.field public static final activateCardInstructions:I = 0x7f1100fb

.field public static final activateCardOverlayView:I = 0x7f110101

.field public static final activateCardProgressBar:I = 0x7f110100

.field public static final activity_chooser_view_content:I = 0x7f1100cc

.field public static final add:I = 0x7f110059

.field public static final addPreferredNameButton:I = 0x7f110326

.field public static final add_attachement_progressbar:I = 0x7f110289

.field public static final add_attachment_frame:I = 0x7f110287

.field public static final add_attachment_image:I = 0x7f110288

.field public static final add_attachment_take_button:I = 0x7f110286

.field public static final address:I = 0x7f11026d

.field public static final addressBodyText:I = 0x7f110498

.field public static final addressCityEditText:I = 0x7f110494

.field public static final addressCityInputLayout:I = 0x7f110493

.field public static final addressConfirmButton:I = 0x7f11015e

.field public static final addressConfirmScrollView:I = 0x7f11015c

.field public static final addressItemText:I = 0x7f110412

.field public static final addressNotInListButton:I = 0x7f11049f

.field public static final addressPickerLabelText:I = 0x7f11049d

.field public static final addressPickerLayout:I = 0x7f11049c

.field public static final addressPostcodeEditText:I = 0x7f110496

.field public static final addressPostcodeInputLayout:I = 0x7f110495

.field public static final addressPrimaryButton:I = 0x7f11049b

.field public static final addressRecyclerView:I = 0x7f11049e

.field public static final addressResidenceEditText:I = 0x7f11049a

.field public static final addressResidenceInputLayout:I = 0x7f110499

.field public static final addressScrollView:I = 0x7f110497

.field public static final addressSelectionView:I = 0x7f1101fc

.field public static final addressStreetEditText:I = 0x7f110492

.field public static final addressStreetInputLayout:I = 0x7f110491

.field public static final addressTextView:I = 0x7f110204

.field public static final addressViewGroup:I = 0x7f110203

.field public static final address_container:I = 0x7f110126

.field public static final address_not_in_list:I = 0x7f110411

.field public static final address_postcode:I = 0x7f110162

.field public static final address_title_textview:I = 0x7f110125

.field public static final adjust_height:I = 0x7f110089

.field public static final adjust_width:I = 0x7f11008a

.field public static final adress_postcode_wrapper:I = 0x7f110161

.field public static final ahead:I = 0x7f11040c

.field public static final alertTitle:I = 0x7f1100df

.field public static final all:I = 0x7f11003b

.field public static final altTinTypeText:I = 0x7f1104c5

.field public static final always:I = 0x7f110096

.field public static final amountContainer:I = 0x7f110436

.field public static final amountView:I = 0x7f11043a

.field public static final amount_edit_text:I = 0x7f1104a2

.field public static final amount_editor:I = 0x7f110243

.field public static final amount_input_layout:I = 0x7f1104a1

.field public static final amount_view:I = 0x7f1101db

.field public static final amountview:I = 0x7f1101d5

.field public static final android_pay:I = 0x7f1100bb

.field public static final android_pay_button_viewgroup:I = 0x7f11024a

.field public static final android_pay_dark:I = 0x7f1100b2

.field public static final android_pay_light:I = 0x7f1100b3

.field public static final android_pay_light_with_border:I = 0x7f1100b4

.field public static final animation:I = 0x7f11012b

.field public static final animationview:I = 0x7f110462

.field public static final appBarLayout:I = 0x7f1102ce

.field public static final appVersionTextView:I = 0x7f110217

.field public static final app_bar_layout:I = 0x7f110228

.field public static final applyForOverdraftActionView:I = 0x7f1102dc

.field public static final async:I = 0x7f110085

.field public static final atmInfoText:I = 0x7f11010c

.field public static final attribution:I = 0x7f1103e0

.field public static final author:I = 0x7f110366

.field public static final auto:I = 0x7f11005c

.field public static final automatic:I = 0x7f1100c2

.field public static final avatar:I = 0x7f1103cf

.field public static final avatarImageView:I = 0x7f11043d

.field public static final avatarView:I = 0x7f1103de

.field public static final avatar_view:I = 0x7f110365

.field public static final back:I = 0x7f11005d

.field public static final backgroundImage:I = 0x7f11047a

.field public static final backgroundImageOverlay:I = 0x7f11047b

.field public static final balanceAmount:I = 0x7f1102fd

.field public static final balanceLabel:I = 0x7f1102fc

.field public static final balanceView:I = 0x7f1102fb

.field public static final bankAccountInfoViewGroup:I = 0x7f110208

.field public static final bankAccountTitleTextView:I = 0x7f1104a3

.field public static final bankAddressViewGroup:I = 0x7f1104a8

.field public static final bank_payment_amount_input:I = 0x7f110111

.field public static final bank_payment_amount_next:I = 0x7f11011b

.field public static final bank_payment_next_button:I = 0x7f11035c

.field public static final baseline:I = 0x7f110080

.field public static final basic:I = 0x7f11003c

.field public static final beginning:I = 0x7f110082

.field public static final behind:I = 0x7f11040d

.field public static final bicNumberViewGroup:I = 0x7f1104a9

.field public static final billing_postcode:I = 0x7f110355

.field public static final billing_postcode_wrapper:I = 0x7f110354

.field public static final blocking:I = 0x7f110086

.field public static final blue_tick:I = 0x7f110456

.field public static final book_now:I = 0x7f1100ab

.field public static final bottom:I = 0x7f11002f

.field public static final bottomNavShadow:I = 0x7f1101a0

.field public static final bottomNavigationView:I = 0x7f1101a1

.field public static final bottom_container:I = 0x7f110278

.field public static final bottom_section_view:I = 0x7f1102bb

.field public static final box_count:I = 0x7f1100bf

.field public static final btn_add_new_card:I = 0x7f11025d

.field public static final btn_contact_support:I = 0x7f110241

.field public static final btn_decrease:I = 0x7f110244

.field public static final btn_increase:I = 0x7f110246

.field public static final btn_send_code:I = 0x7f11016a

.field public static final btn_submit_code:I = 0x7f110168

.field public static final btn_take_picture:I = 0x7f110235

.field public static final btn_top_up:I = 0x7f11025b

.field public static final btn_topup_bank:I = 0x7f110248

.field public static final btn_topup_card:I = 0x7f110249

.field public static final btn_use_picture:I = 0x7f110236

.field public static final bump:I = 0x7f11027a

.field public static final bump_description:I = 0x7f110219

.field public static final button:I = 0x7f1100c0

.field public static final buttonPanel:I = 0x7f1100d2

.field public static final buttons:I = 0x7f11012c

.field public static final buyButton:I = 0x7f1100a8

.field public static final buy_now:I = 0x7f1100ac

.field public static final buy_with:I = 0x7f1100ad

.field public static final buy_with_google:I = 0x7f1100ae

.field public static final cameraOverlayView:I = 0x7f1101f2

.field public static final cameraView:I = 0x7f1101ed

.field public static final camera_view:I = 0x7f110231

.field public static final cancel_action:I = 0x7f110443

.field public static final cancel_button:I = 0x7f1102a3

.field public static final cardNumbers1Textview:I = 0x7f1102d0

.field public static final cardNumbers2Textview:I = 0x7f1102d1

.field public static final cardNumbers3Textview:I = 0x7f1102d2

.field public static final cardNumbers4Textview:I = 0x7f1102d3

.field public static final cardOnItsWayBodyTextView:I = 0x7f110121

.field public static final cardOnItsWayButton:I = 0x7f110122

.field public static final cardOnItsWayTitleTextView:I = 0x7f110120

.field public static final cardOrderingAnimation:I = 0x7f11011f

.field public static final cardProgressbar:I = 0x7f1102d7

.field public static final cardView:I = 0x7f1102cf

.field public static final card_expiry:I = 0x7f1104bd

.field public static final card_has_arrived:I = 0x7f11012d

.field public static final card_has_not_arrived:I = 0x7f11012e

.field public static final card_holder_name:I = 0x7f11034d

.field public static final card_holder_name_wrapper:I = 0x7f11034c

.field public static final card_image:I = 0x7f1104bb

.field public static final card_last_four:I = 0x7f1104bc

.field public static final card_number:I = 0x7f11034f

.field public static final card_number_label:I = 0x7f11029a

.field public static final card_number_wrapper:I = 0x7f11034e

.field public static final card_replacement_textview:I = 0x7f110124

.field public static final card_view:I = 0x7f110431

.field public static final cards_container:I = 0x7f1104ba

.field public static final categoriesLayout:I = 0x7f11017a

.field public static final categoriesTitle:I = 0x7f110179

.field public static final categoryHelpTitle:I = 0x7f110415

.field public static final categoryRecyclerView:I = 0x7f110186

.field public static final categorySectionTitle:I = 0x7f110416

.field public static final categoryTopicTitle:I = 0x7f110417

.field public static final category_imageview:I = 0x7f1103ff

.field public static final category_selection_title:I = 0x7f1101dd

.field public static final category_textview:I = 0x7f110400

.field public static final category_view:I = 0x7f1101de

.field public static final cellLayout:I = 0x7f1103af

.field public static final cell_content:I = 0x7f1103e8

.field public static final center:I = 0x7f110065

.field public static final center_guideline:I = 0x7f1102ba

.field public static final center_horizontal:I = 0x7f110066

.field public static final center_vertical:I = 0x7f110067

.field public static final chains:I = 0x7f11003d

.field public static final changeLimitDescriptionView:I = 0x7f110131

.field public static final change_category_bills_viewgroup:I = 0x7f1102bf

.field public static final change_category_cash_viewgroup:I = 0x7f1102c2

.field public static final change_category_eatingout_viewgroup:I = 0x7f1102be

.field public static final change_category_entertainment_viewgroup:I = 0x7f1102c3

.field public static final change_category_expenses_viewgroup:I = 0x7f1102c5

.field public static final change_category_general_viewgroup:I = 0x7f1102c0

.field public static final change_category_groceries_viewgroup:I = 0x7f1102c1

.field public static final change_category_holidays_viewgroup:I = 0x7f1102c4

.field public static final change_category_shopping_viewgroup:I = 0x7f1102bc

.field public static final change_category_transport_viewgroup:I = 0x7f1102bd

.field public static final changed_number_button:I = 0x7f1102c8

.field public static final chatWithUsButton:I = 0x7f110138

.field public static final chat_avatar_container:I = 0x7f1103d6

.field public static final chat_full_body:I = 0x7f1103d2

.field public static final chat_overlay_overflow_fade:I = 0x7f1103d5

.field public static final chat_with_us_body_textview:I = 0x7f110137

.field public static final chat_with_us_image_animationview:I = 0x7f110135

.field public static final chat_with_us_title_textview:I = 0x7f110136

.field public static final chathead_avatar:I = 0x7f1103d7

.field public static final chathead_root:I = 0x7f1103d1

.field public static final chathead_text_body:I = 0x7f1103d8

.field public static final chathead_text_container:I = 0x7f1103d3

.field public static final chathead_text_header:I = 0x7f1103d4

.field public static final checkbox:I = 0x7f1100e2

.field public static final choosePinBadgeTextView:I = 0x7f1102e2

.field public static final choosePinCardImageView:I = 0x7f1102de

.field public static final choosePinEntryView:I = 0x7f1102e3

.field public static final choosePinNameOnCardTextView:I = 0x7f1102df

.field public static final choosePinOverlayView:I = 0x7f1102e4

.field public static final choosePinProgressBar:I = 0x7f1102e5

.field public static final choosePinSubtitleTextView:I = 0x7f1102e1

.field public static final choosePinTitleTextView:I = 0x7f1102e0

.field public static final chronometer:I = 0x7f11044b

.field public static final city:I = 0x7f110164

.field public static final city_wrapper:I = 0x7f110163

.field public static final classic:I = 0x7f1100b5

.field public static final clear_search:I = 0x7f1103ce

.field public static final clip_horizontal:I = 0x7f110068

.field public static final clip_vertical:I = 0x7f110069

.field public static final closeAccountView:I = 0x7f110214

.field public static final closeMonzoButton:I = 0x7f110224

.field public static final collapseActionView:I = 0x7f110097

.field public static final collapsingToolbar:I = 0x7f110477

.field public static final collapsing_background_image:I = 0x7f110381

.field public static final column:I = 0x7f110076

.field public static final column_reverse:I = 0x7f110077

.field public static final com_facebook_body_frame:I = 0x7f1102a7

.field public static final com_facebook_button_xout:I = 0x7f1102a9

.field public static final com_facebook_device_auth_instructions:I = 0x7f1102a2

.field public static final com_facebook_fragment_container:I = 0x7f11029f

.field public static final com_facebook_login_activity_progress_bar:I = 0x7f1102a4

.field public static final com_facebook_smart_instructions_0:I = 0x7f1102a5

.field public static final com_facebook_smart_instructions_or:I = 0x7f1102a6

.field public static final com_facebook_tooltip_bubble_view_bottom_pointer:I = 0x7f1102ab

.field public static final com_facebook_tooltip_bubble_view_text_body:I = 0x7f1102aa

.field public static final com_facebook_tooltip_bubble_view_top_pointer:I = 0x7f1102a8

.field public static final communityCategoryGroup:I = 0x7f110184

.field public static final communityHomeCategory:I = 0x7f110183

.field public static final communityTitle:I = 0x7f110180

.field public static final compose_action_button:I = 0x7f1103cc

.field public static final composer_container:I = 0x7f1103a6

.field public static final composer_edit_text_layout:I = 0x7f1103a9

.field public static final composer_holder:I = 0x7f1103c7

.field public static final composer_holder_shadow:I = 0x7f1103c9

.field public static final composer_input_icons_recycler_view:I = 0x7f1103aa

.field public static final composer_input_view:I = 0x7f1103c4

.field public static final composer_lower_border:I = 0x7f1103ad

.field public static final composer_upper_border:I = 0x7f1103a8

.field public static final composer_view_pager:I = 0x7f1103ae

.field public static final confirmCardDetailsContainer:I = 0x7f1102e8

.field public static final confirmLimitButton:I = 0x7f110130

.field public static final confirmation_code:I = 0x7f1102a0

.field public static final confirmation_text_view:I = 0x7f1101dc

.field public static final contact_action_textview:I = 0x7f110419

.field public static final contact_imageview:I = 0x7f1101d3

.field public static final contact_name_textview:I = 0x7f1101d4

.field public static final contact_number_textview:I = 0x7f110418

.field public static final contactsFab:I = 0x7f1102fa

.field public static final contactsTabLayout:I = 0x7f1102f8

.field public static final contactsViewPager:I = 0x7f1102f9

.field public static final container:I = 0x7f1102ae

.field public static final content:I = 0x7f11006e

.field public static final contentPanel:I = 0x7f1100d5

.field public static final content_textview:I = 0x7f11023f

.field public static final continueButton:I = 0x7f110336

.field public static final conversation_coordinator:I = 0x7f11037a

.field public static final conversation_coordinator_layout:I = 0x7f1103b0

.field public static final conversation_fragment:I = 0x7f110373

.field public static final conversation_fragment_root:I = 0x7f1103c5

.field public static final conversation_list:I = 0x7f1103b3

.field public static final conversation_web_view_layout:I = 0x7f1103b4

.field public static final conversations_list_root:I = 0x7f1103ca

.field public static final coordinator:I = 0x7f1102af

.field public static final correct:I = 0x7f11026e

.field public static final countries_recyclerview:I = 0x7f11013e

.field public static final country:I = 0x7f110165

.field public static final countryNameTextView:I = 0x7f11041a

.field public static final country_edittext:I = 0x7f110148

.field public static final country_wrapper:I = 0x7f110147

.field public static final create:I = 0x7f110149

.field public static final currentLimitView:I = 0x7f110132

.field public static final custom:I = 0x7f1100dc

.field public static final customPanel:I = 0x7f1100db

.field public static final customise_amount_input:I = 0x7f11014e

.field public static final customise_notes_editext:I = 0x7f11014f

.field public static final customise_share_button:I = 0x7f110150

.field public static final cvv:I = 0x7f110353

.field public static final cvv_wrapper:I = 0x7f110352

.field public static final dark:I = 0x7f110072

.field public static final date:I = 0x7f110144

.field public static final dateOfBirthEditText:I = 0x7f11032a

.field public static final dateOfBirthInputLayout:I = 0x7f110329

.field public static final date_of_birth:I = 0x7f11026c

.field public static final date_of_birth_edittext:I = 0x7f1102c6

.field public static final date_of_birth_wrapper:I = 0x7f11026b

.field public static final date_wrapper:I = 0x7f110143

.field public static final declinedView:I = 0x7f110438

.field public static final decor_content_parent:I = 0x7f1100e9

.field public static final defaultStrategy:I = 0x7f110074

.field public static final default_activity_button:I = 0x7f1100cf

.field public static final delete:I = 0x7f11010f

.field public static final delete_attachment_progressbar:I = 0x7f11010e

.field public static final description:I = 0x7f11012a

.field public static final descriptionTextView:I = 0x7f1101b2

.field public static final description_once_it_arrives:I = 0x7f11012f

.field public static final design_bottom_sheet:I = 0x7f1102b1

.field public static final design_menu_item_action_area:I = 0x7f1102b8

.field public static final design_menu_item_action_area_stub:I = 0x7f1102b7

.field public static final design_menu_item_text:I = 0x7f1102b6

.field public static final design_navigation_view:I = 0x7f1102b5

.field public static final disableHome:I = 0x7f110049

.field public static final dismiss:I = 0x7f110361

.field public static final display_always:I = 0x7f1100c3

.field public static final divideTopView:I = 0x7f1101f0

.field public static final divider:I = 0x7f110127

.field public static final dividerBottomView:I = 0x7f1101b6

.field public static final dividerTopView:I = 0x7f1101b5

.field public static final docsBodyTextView:I = 0x7f110317

.field public static final docsContinueButton:I = 0x7f11031b

.field public static final docsFscsProtectionButton:I = 0x7f11031a

.field public static final docsImageView:I = 0x7f110315

.field public static final docsPrivacyPolicyButton:I = 0x7f110319

.field public static final docsTermsAndConditionsButton:I = 0x7f110318

.field public static final docsTitleTextView:I = 0x7f110316

.field public static final document_position_textview:I = 0x7f110234

.field public static final document_type_driving_license:I = 0x7f11028a

.field public static final document_type_national_id:I = 0x7f11028b

.field public static final donate_with:I = 0x7f1100af

.field public static final donate_with_google:I = 0x7f1100b0

.field public static final dot1:I = 0x7f11037d

.field public static final dot2:I = 0x7f11037e

.field public static final dot3:I = 0x7f11037f

.field public static final drawerLayout:I = 0x7f110196

.field public static final edd_body_textview:I = 0x7f110157

.field public static final edd_not_now_button:I = 0x7f110159

.field public static final edd_view_limits_button:I = 0x7f110158

.field public static final edit_address_button:I = 0x7f110129

.field public static final edit_query:I = 0x7f1100ed

.field public static final emailTextView:I = 0x7f110202

.field public static final email_edittext:I = 0x7f11021d

.field public static final email_wrapper:I = 0x7f11021c

.field public static final empty_action_button:I = 0x7f11039a

.field public static final empty_text_subtitle:I = 0x7f110399

.field public static final empty_text_title:I = 0x7f110398

.field public static final empty_view_layout:I = 0x7f110397

.field public static final end:I = 0x7f110030

.field public static final end_padder:I = 0x7f110451

.field public static final enterAlways:I = 0x7f11004f

.field public static final enterAlwaysCollapsed:I = 0x7f110050

.field public static final enter_an_email:I = 0x7f11021b

.field public static final errorLayout:I = 0x7f1101af

.field public static final error_layout_article:I = 0x7f1103bf

.field public static final error_layout_conversation:I = 0x7f1103c1

.field public static final error_layout_inbox:I = 0x7f1103c2

.field public static final et_card_number:I = 0x7f11029b

.field public static final et_cvc_number:I = 0x7f11029e

.field public static final et_expiry_date:I = 0x7f11029d

.field public static final event_bump:I = 0x7f11011d

.field public static final event_message:I = 0x7f11011c

.field public static final event_name:I = 0x7f1103e1

.field public static final event_web_view:I = 0x7f11027b

.field public static final exclude:I = 0x7f11006f

.field public static final exitUntilCollapsed:I = 0x7f110051

.field public static final exoPlayerView:I = 0x7f1101b3

.field public static final exo_artwork:I = 0x7f110004

.field public static final exo_content_frame:I = 0x7f110005

.field public static final exo_controller:I = 0x7f110006

.field public static final exo_controller_placeholder:I = 0x7f110007

.field public static final exo_duration:I = 0x7f110008

.field public static final exo_ffwd:I = 0x7f110009

.field public static final exo_next:I = 0x7f11000a

.field public static final exo_overlay:I = 0x7f11000b

.field public static final exo_pause:I = 0x7f11000c

.field public static final exo_play:I = 0x7f11000d

.field public static final exo_position:I = 0x7f11000e

.field public static final exo_prev:I = 0x7f11000f

.field public static final exo_progress:I = 0x7f110010

.field public static final exo_repeat_toggle:I = 0x7f110011

.field public static final exo_rew:I = 0x7f110012

.field public static final exo_shutter:I = 0x7f110013

.field public static final exo_subtitles:I = 0x7f110014

.field public static final expand_activities_button:I = 0x7f1100cd

.field public static final expand_arrow:I = 0x7f1103ea

.field public static final expanded_fragment:I = 0x7f110395

.field public static final expanded_menu:I = 0x7f1100e1

.field public static final expected_balance:I = 0x7f110247

.field public static final expiresView:I = 0x7f1102d4

.field public static final expiry_date:I = 0x7f110351

.field public static final expiry_date_label:I = 0x7f11029c

.field public static final expiry_date_wrapper:I = 0x7f110350

.field public static final exportAction:I = 0x7f1104d6

.field public static final exportFormatLayout:I = 0x7f11028c

.field public static final exportSheetTitleText:I = 0x7f11028d

.field public static final fallbackBottomGuideline:I = 0x7f110275

.field public static final fallbackDocumentBodyTextView:I = 0x7f110151

.field public static final fallbackDocumentPreviewImageView:I = 0x7f110152

.field public static final fallbackOverlayView:I = 0x7f110272

.field public static final fallbackVideoConstraintLayout:I = 0x7f11026f

.field public static final fallbackVideoExoPlayer:I = 0x7f110271

.field public static final fallbackVideoInstructionsTextView:I = 0x7f110270

.field public static final fallbackVideoMuteButton:I = 0x7f110273

.field public static final featureIdeasCategory:I = 0x7f110182

.field public static final feedRecyclerView:I = 0x7f110302

.field public static final feedSwipeToRefreshLayout:I = 0x7f110301

.field public static final feed_description_textview:I = 0x7f11041e

.field public static final feed_icon_imageview:I = 0x7f11041b

.field public static final feed_item_overflow_button:I = 0x7f11041c

.field public static final feed_title_textview:I = 0x7f11041d

.field public static final feedbackLabel:I = 0x7f110191

.field public static final feedback_negative:I = 0x7f1102cc

.field public static final feedback_neutral:I = 0x7f1102cb

.field public static final feedback_positive:I = 0x7f1102ca

.field public static final fill:I = 0x7f110040

.field public static final fill_horizontal:I = 0x7f11006a

.field public static final fill_vertical:I = 0x7f11006b

.field public static final fit:I = 0x7f110041

.field public static final fixed:I = 0x7f11009e

.field public static final fixedHeight:I = 0x7f1100a0

.field public static final fixedWidth:I = 0x7f1100a1

.field public static final fixed_height:I = 0x7f110042

.field public static final fixed_width:I = 0x7f110043

.field public static final flagTextView:I = 0x7f1104c2

.field public static final flex_end:I = 0x7f11007c

.field public static final flex_start:I = 0x7f11007d

.field public static final footer_textview:I = 0x7f110461

.field public static final force_upgrade_button:I = 0x7f11016e

.field public static final forever:I = 0x7f110087

.field public static final fourDigitsPinText:I = 0x7f110470

.field public static final fragment_container:I = 0x7f110394

.field public static final frame_container:I = 0x7f110299

.field public static final front:I = 0x7f11005e

.field public static final fscsProtectionSeparatorView:I = 0x7f110211

.field public static final fscsProtectionView:I = 0x7f110210

.field public static final full_image:I = 0x7f110370

.field public static final gallery_content_layout:I = 0x7f11039d

.field public static final gallery_empty_view:I = 0x7f11039f

.field public static final gallery_expand_button:I = 0x7f1103c3

.field public static final gallery_recycler_view:I = 0x7f11039e

.field public static final gallery_root_view:I = 0x7f11039c

.field public static final get_card_button:I = 0x7f11023e

.field public static final ghostBanner:I = 0x7f110197

.field public static final ghostBannerLogoutButton:I = 0x7f110199

.field public static final ghostBannerTextView:I = 0x7f110198

.field public static final ghost_view:I = 0x7f110015

.field public static final golden_ticket_body_textview:I = 0x7f110175

.field public static final golden_ticket_image_viewgroup:I = 0x7f110171

.field public static final golden_ticket_imageview:I = 0x7f110174

.field public static final golden_ticket_progressbar:I = 0x7f110170

.field public static final golden_ticket_share_link_button:I = 0x7f110176

.field public static final golden_ticket_title_textview:I = 0x7f110172

.field public static final golden_ticket_videoview:I = 0x7f110173

.field public static final google_wallet_classic:I = 0x7f1100b6

.field public static final google_wallet_grayscale:I = 0x7f1100b7

.field public static final google_wallet_monochrome:I = 0x7f1100b8

.field public static final grayscale:I = 0x7f1100b9

.field public static final guideline:I = 0x7f11027d

.field public static final headerTextView:I = 0x7f11041f

.field public static final height:I = 0x7f1100c7

.field public static final helpButton:I = 0x7f110185

.field public static final helpCategoryBackground:I = 0x7f1104ab

.field public static final helpCategoryIcon:I = 0x7f1104ad

.field public static final helpCategoryLabel:I = 0x7f1104ac

.field public static final helpSearchInstruction:I = 0x7f110188

.field public static final helpSearchLayout:I = 0x7f110177

.field public static final helpSearchRecyclerView:I = 0x7f11018d

.field public static final helpSearchSuggestion1:I = 0x7f110189

.field public static final helpSearchSuggestion2:I = 0x7f11018a

.field public static final helpSearchSuggestion3:I = 0x7f11018b

.field public static final helpSearchSuggestions:I = 0x7f11018c

.field public static final help_center_web_view:I = 0x7f11036e

.field public static final holo_dark:I = 0x7f1100a2

.field public static final holo_light:I = 0x7f1100a3

.field public static final home:I = 0x7f110016

.field public static final homeAsUp:I = 0x7f11004a

.field public static final homeCoordinatorlayout:I = 0x7f1102cd

.field public static final home_coordinatorlayout:I = 0x7f1102f7

.field public static final horizontal:I = 0x7f110063

.field public static final hybrid:I = 0x7f11008d

.field public static final icon:I = 0x7f1100d1

.field public static final iconBackgroundView:I = 0x7f1104b7

.field public static final iconImageView:I = 0x7f1104b8

.field public static final iconLayout:I = 0x7f1104b3

.field public static final icon_group:I = 0x7f11044f

.field public static final icon_only:I = 0x7f11009b

.field public static final idvErrorView:I = 0x7f1101a6

.field public static final idvFragmentContainer:I = 0x7f1101a8

.field public static final idvOnboardingNextButton:I = 0x7f110311

.field public static final idvOnboardingViewPager:I = 0x7f11030f

.field public static final idvOnboardingViewPagerIndicator:I = 0x7f110310

.field public static final idvStatusProgressBar:I = 0x7f1101a7

.field public static final ifRoom:I = 0x7f110098

.field public static final image:I = 0x7f1100ce

.field public static final imageView:I = 0x7f11033d

.field public static final image_pager:I = 0x7f110103

.field public static final image_preview:I = 0x7f110232

.field public static final inbox_fragment:I = 0x7f110372

.field public static final inbox_recycler_view:I = 0x7f1103cb

.field public static final include:I = 0x7f11046f

.field public static final info:I = 0x7f11044c

.field public static final inline:I = 0x7f1100c1

.field public static final inputExtractAccessories:I = 0x7f11035e

.field public static final inputExtractAction:I = 0x7f11035f

.field public static final input_icon_image_view:I = 0x7f1103a7

.field public static final input_text:I = 0x7f110396

.field public static final instructions:I = 0x7f110230

.field public static final intercom_author_avatar:I = 0x7f1103e2

.field public static final intercom_avatar_spacer:I = 0x7f110389

.field public static final intercom_bottom_spacer:I = 0x7f110390

.field public static final intercom_bubble:I = 0x7f1103dd

.field public static final intercom_collapsing_bio:I = 0x7f11038c

.field public static final intercom_collapsing_location:I = 0x7f11038b

.field public static final intercom_collapsing_office_hours:I = 0x7f1103f4

.field public static final intercom_collapsing_role:I = 0x7f11038a

.field public static final intercom_collapsing_subtitle:I = 0x7f110388

.field public static final intercom_collapsing_team_avatar1:I = 0x7f1103ed

.field public static final intercom_collapsing_team_avatar2:I = 0x7f1103ef

.field public static final intercom_collapsing_team_avatar3:I = 0x7f1103f1

.field public static final intercom_collapsing_team_bio:I = 0x7f1103f3

.field public static final intercom_collapsing_team_name_1:I = 0x7f1103ee

.field public static final intercom_collapsing_team_name_2:I = 0x7f1103f0

.field public static final intercom_collapsing_team_name_3:I = 0x7f1103f2

.field public static final intercom_collapsing_teammate_active_state:I = 0x7f110385

.field public static final intercom_collapsing_teammate_avatar1:I = 0x7f110384

.field public static final intercom_collapsing_teammate_avatar2:I = 0x7f110383

.field public static final intercom_collapsing_teammate_avatar3:I = 0x7f110382

.field public static final intercom_collapsing_title:I = 0x7f110386

.field public static final intercom_collapsing_title_name_only:I = 0x7f110387

.field public static final intercom_container_card_title:I = 0x7f1103e7

.field public static final intercom_container_fade_view:I = 0x7f1103e9

.field public static final intercom_conversation_indicator:I = 0x7f1103e5

.field public static final intercom_group_avatar_holder:I = 0x7f110393

.field public static final intercom_group_conversations_banner:I = 0x7f110391

.field public static final intercom_group_conversations_banner_title:I = 0x7f110392

.field public static final intercom_left_item_layout:I = 0x7f1103f7

.field public static final intercom_link:I = 0x7f1103b5

.field public static final intercom_message_summary:I = 0x7f1103e6

.field public static final intercom_office_hours_banner:I = 0x7f1103f6

.field public static final intercom_overlay_root:I = 0x7f110017

.field public static final intercom_rating_options_layout:I = 0x7f1103b8

.field public static final intercom_rating_tell_us_more_button:I = 0x7f1103b9

.field public static final intercom_team_profile:I = 0x7f1103eb

.field public static final intercom_team_profile_separator:I = 0x7f1103f5

.field public static final intercom_team_profiles_layout:I = 0x7f1103ec

.field public static final intercom_teammate_profile_container_view:I = 0x7f110380

.field public static final intercom_time_stamp:I = 0x7f1103e3

.field public static final intercom_toolbar:I = 0x7f110375

.field public static final intercom_toolbar_avatar:I = 0x7f1103f9

.field public static final intercom_toolbar_avatar_active_state:I = 0x7f1103fa

.field public static final intercom_toolbar_close:I = 0x7f1103fb

.field public static final intercom_toolbar_divider:I = 0x7f1103fe

.field public static final intercom_toolbar_inbox:I = 0x7f1103f8

.field public static final intercom_toolbar_subtitle:I = 0x7f1103fd

.field public static final intercom_toolbar_title:I = 0x7f1103fc

.field public static final intercom_user_name:I = 0x7f1103e4

.field public static final intercom_you_rated_image_view:I = 0x7f1103bb

.field public static final intercom_you_rated_layout:I = 0x7f1103ba

.field public static final interval_desc_textview:I = 0x7f1104b0

.field public static final interval_title_radiobutton:I = 0x7f1104af

.field public static final invite_friends_textview:I = 0x7f110279

.field public static final italic:I = 0x7f110088

.field public static final itemBankTransferDescription:I = 0x7f110414

.field public static final itemBankTransferTitle:I = 0x7f110413

.field public static final item_touch_helper_previous_elevation:I = 0x7f110018

.field public static final iv_card_icon:I = 0x7f110298

.field public static final know_your_customer_approved_button:I = 0x7f1101a5

.field public static final know_your_customer_photo_container:I = 0x7f110304

.field public static final know_your_customer_photo_subtitle_textview:I = 0x7f110306

.field public static final know_your_customer_video_subtitle_textview:I = 0x7f11030b

.field public static final label:I = 0x7f110070

.field public static final label_select_address:I = 0x7f1101fd

.field public static final large:I = 0x7f1100c5

.field public static final largeLabel:I = 0x7f1102ad

.field public static final launcher_badge_count:I = 0x7f1103be

.field public static final launcher_icon:I = 0x7f1103bd

.field public static final launcher_root:I = 0x7f1103bc

.field public static final left:I = 0x7f110031

.field public static final legalDocumentsContentFrame:I = 0x7f1101b8

.field public static final legalNameEditText:I = 0x7f110325

.field public static final legalNameInputLayout:I = 0x7f110324

.field public static final library_license_textview:I = 0x7f110425

.field public static final library_name_textview:I = 0x7f110424

.field public static final light:I = 0x7f110073

.field public static final lightbox_annotation_controls_space:I = 0x7f1103a3

.field public static final lightbox_button_layout:I = 0x7f1103a0

.field public static final lightbox_close_button:I = 0x7f1103a1

.field public static final lightbox_image:I = 0x7f1103a5

.field public static final lightbox_send_button:I = 0x7f1103a4

.field public static final lightbox_undo_button:I = 0x7f1103a2

.field public static final limitAmountRemainingTextView:I = 0x7f110423

.field public static final limitAmountTextView:I = 0x7f110422

.field public static final limitNameTextView:I = 0x7f110421

.field public static final limitsErrorView:I = 0x7f1101bf

.field public static final limitsProgressBar:I = 0x7f1101be

.field public static final limitsRecyclerView:I = 0x7f1101bb

.field public static final limitsRequestHigherButton:I = 0x7f1101bd

.field public static final limitsRequestHigherViewGroup:I = 0x7f1101bc

.field public static final limitsSubtitleTextView:I = 0x7f1101ba

.field public static final limitsTextView:I = 0x7f110207

.field public static final limitsTitleTextView:I = 0x7f1101b9

.field public static final limits_image_animationview:I = 0x7f110155

.field public static final limits_title_textview:I = 0x7f110156

.field public static final line1:I = 0x7f110019

.field public static final line3:I = 0x7f11001a

.field public static final link:I = 0x7f110218

.field public static final link_composer_container:I = 0x7f110368

.field public static final link_container:I = 0x7f110364

.field public static final link_root:I = 0x7f110360

.field public static final link_title_bar:I = 0x7f11036b

.field public static final link_view:I = 0x7f110362

.field public static final linkedin_button:I = 0x7f11038f

.field public static final listMode:I = 0x7f110046

.field public static final list_item:I = 0x7f1100d0

.field public static final loadingErrorView:I = 0x7f110187

.field public static final loadingOverlay:I = 0x7f1104b1

.field public static final loadingProgress:I = 0x7f1104b5

.field public static final loading_view:I = 0x7f11036d

.field public static final localAmountView:I = 0x7f11043b

.field public static final logOutView:I = 0x7f110213

.field public static final logo_imageview:I = 0x7f110432

.field public static final logo_only:I = 0x7f1100b1

.field public static final long_description:I = 0x7f11013a

.field public static final lottie_layer_name:I = 0x7f11001b

.field public static final magStripeSwitchView:I = 0x7f11020a

.field public static final magStripeTitleTextView:I = 0x7f110209

.field public static final mainActionButton:I = 0x7f110153

.field public static final mainScrollContent:I = 0x7f110260

.field public static final main_content:I = 0x7f11010b

.field public static final main_framelayout:I = 0x7f11019a

.field public static final manageOverdraftActionView:I = 0x7f1102dd

.field public static final map:I = 0x7f110479

.field public static final mapTouchWrapper:I = 0x7f110478

.field public static final marketingOptInContentFrame:I = 0x7f1101c0

.field public static final maskImageView:I = 0x7f1101ef

.field public static final masked:I = 0x7f1104c7

.field public static final match_parent:I = 0x7f1100aa

.field public static final media_actions:I = 0x7f110445

.field public static final memberSinceTextView:I = 0x7f110216

.field public static final menuHelp:I = 0x7f1104d5

.field public static final menuSnooze:I = 0x7f1104d4

.field public static final menuTerms:I = 0x7f1104d2

.field public static final menu_delete_feed_item:I = 0x7f1104d1

.field public static final menu_search:I = 0x7f1104d0

.field public static final merchantIconView:I = 0x7f110434

.field public static final message:I = 0x7f110473

.field public static final messageText:I = 0x7f1104b6

.field public static final messenger_container:I = 0x7f110371

.field public static final messenger_send_button:I = 0x7f11043c

.field public static final metadata:I = 0x7f1103df

.field public static final middle:I = 0x7f110083

.field public static final migrationBanner:I = 0x7f11019b

.field public static final migrationBannerButton:I = 0x7f11019f

.field public static final migrationBannerSubtitle:I = 0x7f11019e

.field public static final migrationBannerTitle:I = 0x7f11019d

.field public static final migrationBody:I = 0x7f1101c3

.field public static final migrationItem1Body:I = 0x7f110467

.field public static final migrationItem1Number:I = 0x7f110465

.field public static final migrationItem1Title:I = 0x7f110466

.field public static final migrationItem2Body:I = 0x7f11046a

.field public static final migrationItem2Number:I = 0x7f110469

.field public static final migrationItem2Title:I = 0x7f110468

.field public static final migrationItem3Body:I = 0x7f11046d

.field public static final migrationItem3Number:I = 0x7f11046c

.field public static final migrationItem3Title:I = 0x7f11046b

.field public static final migrationLottieAnimationView:I = 0x7f1101c1

.field public static final migrationPrimaryButton:I = 0x7f1101c4

.field public static final migrationSecondaryButton:I = 0x7f1101c5

.field public static final migrationSubtitle:I = 0x7f110464

.field public static final migrationTitle:I = 0x7f1101c2

.field public static final migrationTourButton:I = 0x7f1101c7

.field public static final migrationTourViewPager:I = 0x7f1101c6

.field public static final mini:I = 0x7f110084

.field public static final minusButton:I = 0x7f110133

.field public static final monochrome:I = 0x7f1100ba

.field public static final month_name_textview:I = 0x7f110430

.field public static final month_spending_view:I = 0x7f110340

.field public static final monzoDocsContent:I = 0x7f110314

.field public static final monzoDocsErrorView:I = 0x7f110313

.field public static final monzoDocsProgress:I = 0x7f110312

.field public static final monzoImageView:I = 0x7f110463

.field public static final monzoPackageTitle:I = 0x7f11046e

.field public static final monzoUserNumberTextView:I = 0x7f110215

.field public static final monzome_onboarding_button:I = 0x7f1101ca

.field public static final monzome_onboarding_progress:I = 0x7f1101cb

.field public static final monzome_onboarding_view_pager:I = 0x7f1101c8

.field public static final monzome_onboarding_viewpagerindicator:I = 0x7f1101c9

.field public static final more_info_button:I = 0x7f1101e4

.field public static final more_info_webview:I = 0x7f1101e5

.field public static final multiply:I = 0x7f110054

.field public static final muteButton:I = 0x7f1101b7

.field public static final name:I = 0x7f110142

.field public static final nameSuggestionsText:I = 0x7f11014c

.field public static final nameTextView:I = 0x7f110201

.field public static final nameViewGroup:I = 0x7f1101ff

.field public static final name_wrapper:I = 0x7f110141

.field public static final navChat:I = 0x7f1104cb

.field public static final navCommunity:I = 0x7f1104c9

.field public static final navHelp:I = 0x7f1104ca

.field public static final navSettings:I = 0x7f1104c8

.field public static final nav_card:I = 0x7f1104ce

.field public static final nav_contacts:I = 0x7f1104cf

.field public static final nav_feed:I = 0x7f1104cc

.field public static final nav_spending:I = 0x7f1104cd

.field public static final navigationDrawer:I = 0x7f1101a4

.field public static final navigation_header_container:I = 0x7f1102b4

.field public static final negativeFeedbackEmojum:I = 0x7f110194

.field public static final never:I = 0x7f110099

.field public static final never_display:I = 0x7f1100c4

.field public static final news_webview:I = 0x7f1101cc

.field public static final nextButton:I = 0x7f11014d

.field public static final noTinButton:I = 0x7f110349

.field public static final no_mail:I = 0x7f11015b

.field public static final no_results_view:I = 0x7f11016d

.field public static final nonExistent:I = 0x7f110075

.field public static final none:I = 0x7f11003e

.field public static final normal:I = 0x7f110047

.field public static final not_now_button:I = 0x7f1102c9

.field public static final note_composer_container:I = 0x7f110377

.field public static final note_layout:I = 0x7f110374

.field public static final note_touch_target:I = 0x7f110378

.field public static final note_view:I = 0x7f110376

.field public static final notes:I = 0x7f110102

.field public static final notesView:I = 0x7f110439

.field public static final notfied:I = 0x7f11013d

.field public static final notification_background:I = 0x7f11044d

.field public static final notification_main_column:I = 0x7f110448

.field public static final notification_main_column_container:I = 0x7f110447

.field public static final notification_pill:I = 0x7f1103d0

.field public static final notification_root:I = 0x7f1103d9

.field public static final notificationsSwitchView:I = 0x7f110206

.field public static final notify_container:I = 0x7f11013b

.field public static final notify_me:I = 0x7f11013c

.field public static final nowrap:I = 0x7f11007a

.field public static final off:I = 0x7f11005f

.field public static final on:I = 0x7f110060

.field public static final onboarding_log_in_button:I = 0x7f11010a

.field public static final onboarding_main_button:I = 0x7f110109

.field public static final onboarding_viewpager:I = 0x7f110108

.field public static final one:I = 0x7f11003f

.field public static final openSourceLicensesView:I = 0x7f110212

.field public static final open_graph:I = 0x7f1100bc

.field public static final open_mail:I = 0x7f11015a

.field public static final open_source_appbarlayout:I = 0x7f1101cd

.field public static final open_source_collapsingtoolbarlayout:I = 0x7f1101ce

.field public static final open_source_description_textview:I = 0x7f1101cf

.field public static final open_source_recyclerview:I = 0x7f1101d0

.field public static final optInBodyTextView:I = 0x7f11031e

.field public static final optInButton:I = 0x7f11031f

.field public static final optInImageView:I = 0x7f11031c

.field public static final optInTitleTextView:I = 0x7f11031d

.field public static final optOutButton:I = 0x7f110320

.field public static final orderCardAddressTextView:I = 0x7f1102f5

.field public static final orderCardContainer:I = 0x7f1101d1

.field public static final orderCardDividerView:I = 0x7f1102f0

.field public static final orderCardErrorView:I = 0x7f1102e6

.field public static final orderCardImageView:I = 0x7f1102e9

.field public static final orderCardLegalNameImageView:I = 0x7f1102ef

.field public static final orderCardLegalNameTextView:I = 0x7f1102ee

.field public static final orderCardLegalNameTitleTextView:I = 0x7f1102ed

.field public static final orderCardLegalNameViewGroup:I = 0x7f1102ec

.field public static final orderCardNameOnCardTextView:I = 0x7f1102ea

.field public static final orderCardNextButton:I = 0x7f1102f6

.field public static final orderCardPreferredNameImageView:I = 0x7f1102f4

.field public static final orderCardPreferredNameTextView:I = 0x7f1102f3

.field public static final orderCardPreferredNameTitleTextView:I = 0x7f1102f2

.field public static final orderCardPreferredNameViewGroup:I = 0x7f1102f1

.field public static final orderCardProgressBar:I = 0x7f1102e7

.field public static final orderCardTitleTextView:I = 0x7f1102eb

.field public static final orderReplacementActionView:I = 0x7f1102d9

.field public static final outageBanner:I = 0x7f11019c

.field public static final outageBannerButton:I = 0x7f1101a3

.field public static final outageBannerTextView:I = 0x7f1101a2

.field public static final overflowButton:I = 0x7f110435

.field public static final overlay_view:I = 0x7f110233

.field public static final packed:I = 0x7f110039

.field public static final page:I = 0x7f1100bd

.field public static final parallax:I = 0x7f11006c

.field public static final parallax_1:I = 0x7f110403

.field public static final parallax_2:I = 0x7f110404

.field public static final parallax_3:I = 0x7f110405

.field public static final parallax_4:I = 0x7f110406

.field public static final parallax_5:I = 0x7f110407

.field public static final parallax_6:I = 0x7f110408

.field public static final parallax_7:I = 0x7f11040a

.field public static final parallax_7_image:I = 0x7f11040b

.field public static final parallax_layout:I = 0x7f110277

.field public static final parallax_stars:I = 0x7f110402

.field public static final parent:I = 0x7f110035

.field public static final parentPanel:I = 0x7f1100d4

.field public static final parent_matrix:I = 0x7f11001c

.field public static final passport_answer_negative:I = 0x7f11028f

.field public static final passport_answer_positive:I = 0x7f11028e

.field public static final payee_action_textview:I = 0x7f110429

.field public static final payee_identifier_textview:I = 0x7f110428

.field public static final payee_imageview:I = 0x7f110426

.field public static final payee_name_textview:I = 0x7f110427

.field public static final payee_progressbar:I = 0x7f11035d

.field public static final paymentAmount:I = 0x7f11042d

.field public static final paymentAmountSubtitle:I = 0x7f11042e

.field public static final paymentCancellationPinEntry:I = 0x7f1101f8

.field public static final paymentCancellationSubtitle:I = 0x7f1101f7

.field public static final paymentCancellationTitle:I = 0x7f1101f6

.field public static final paymentSubtitle:I = 0x7f11042c

.field public static final paymentTitle:I = 0x7f11042b

.field public static final payment_reference_edit_text:I = 0x7f110113

.field public static final payment_reference_input_layout:I = 0x7f110112

.field public static final paymentsSwitchView:I = 0x7f11020c

.field public static final paymentsTitleTextView:I = 0x7f11020b

.field public static final peer_payment_amount_input:I = 0x7f1101df

.field public static final peer_to_peer_enable_button:I = 0x7f1101e3

.field public static final peer_to_peer_notes_edittext:I = 0x7f1101e0

.field public static final peer_to_peer_transfer_button:I = 0x7f1101e1

.field public static final percent:I = 0x7f110036

.field public static final phoneNumberEditText:I = 0x7f110335

.field public static final phoneNumberInputLayout:I = 0x7f110334

.field public static final phoneVerificationContentFrame:I = 0x7f1101e6

.field public static final phone_number:I = 0x7f110169

.field public static final photoCompleteImageView:I = 0x7f110305

.field public static final photo_id_driving_licence:I = 0x7f110290

.field public static final photo_id_passport:I = 0x7f110291

.field public static final pill:I = 0x7f1103c8

.field public static final pin:I = 0x7f11006d

.field public static final pinActionView:I = 0x7f1101e9

.field public static final pinEntry:I = 0x7f1101d8

.field public static final pinEntryView:I = 0x7f110472

.field public static final pinLabelText:I = 0x7f110471

.field public static final pin_background:I = 0x7f11016f

.field public static final pin_badge_textview:I = 0x7f1101e8

.field public static final pin_textview:I = 0x7f1101e7

.field public static final plusButton:I = 0x7f110134

.field public static final positiveFeedbackEmojum:I = 0x7f110195

.field public static final post_container:I = 0x7f110379

.field public static final post_touch_target:I = 0x7f11037c

.field public static final post_view:I = 0x7f11037b

.field public static final postcode:I = 0x7f110146

.field public static final postcode_wrapper:I = 0x7f110145

.field public static final potNameEditText:I = 0x7f11014b

.field public static final potNameInputLayout:I = 0x7f11014a

.field public static final potNameLabelText:I = 0x7f110401

.field public static final potNameRecyclerView:I = 0x7f110140

.field public static final potPurposeText:I = 0x7f11013f

.field public static final preferredNameEditText:I = 0x7f110328

.field public static final preferredNameInputLayout:I = 0x7f110327

.field public static final preview_avatar:I = 0x7f1103da

.field public static final preview_name:I = 0x7f1103db

.field public static final preview_summary:I = 0x7f1103dc

.field public static final primaryDocumentThumbnail:I = 0x7f110307

.field public static final privacyPolicyView:I = 0x7f11020f

.field public static final production:I = 0x7f1100a4

.field public static final profileAddressInputView:I = 0x7f11015d

.field public static final profileCreationContentFrame:I = 0x7f1101ec

.field public static final profileDetailsFormContainer:I = 0x7f110323

.field public static final profileDetailsNextButton:I = 0x7f11032b

.field public static final profileDetailsProgress:I = 0x7f110322

.field public static final profile_toolbar:I = 0x7f1103b2

.field public static final profile_toolbar_coordinator:I = 0x7f1103b1

.field public static final progress:I = 0x7f1101b1

.field public static final progressBackgroundView:I = 0x7f1104b4

.field public static final progressBar:I = 0x7f1101eb

.field public static final progressBarOverlay:I = 0x7f1101f9

.field public static final progressLayout:I = 0x7f1104b2

.field public static final progressOverlay:I = 0x7f1101ea

.field public static final progress_bar:I = 0x7f1102a1

.field public static final progress_circular:I = 0x7f11001d

.field public static final progress_horizontal:I = 0x7f11001e

.field public static final progressbar:I = 0x7f1101da

.field public static final progressbar_overlay:I = 0x7f1101d9

.field public static final queryField:I = 0x7f11018e

.field public static final radio:I = 0x7f1100e4

.field public static final rate_your_conversation_text_view:I = 0x7f1103b7

.field public static final reaction_input_view:I = 0x7f11036a

.field public static final reaction_text:I = 0x7f110369

.field public static final recipient_name_edit_text:I = 0x7f110357

.field public static final recipient_name_input_layout:I = 0x7f110356

.field public static final recordVideoButton:I = 0x7f1101ee

.field public static final recurringBannerSubtitle:I = 0x7f11033f

.field public static final recurringBannerTitle:I = 0x7f11033e

.field public static final recurringPaymentImage:I = 0x7f11042a

.field public static final recurringPaymentsBanner:I = 0x7f11033c

.field public static final recurringRecyclerView:I = 0x7f1101fa

.field public static final redEye:I = 0x7f110061

.field public static final replacement_address_textview:I = 0x7f110128

.field public static final request_customise_amount_button:I = 0x7f110330

.field public static final request_icon_imageview:I = 0x7f11032c

.field public static final request_money_info:I = 0x7f1104d3

.field public static final request_monzome_link_textview:I = 0x7f11032e

.field public static final request_name_textview:I = 0x7f11032d

.field public static final request_share_link_button:I = 0x7f11032f

.field public static final residencyCountriesRecyclerView:I = 0x7f110343

.field public static final retakePhotoButton:I = 0x7f110154

.field public static final retakeVideoButton:I = 0x7f110274

.field public static final retryButton:I = 0x7f1101b0

.field public static final right:I = 0x7f110032

.field public static final right_icon:I = 0x7f110450

.field public static final right_side:I = 0x7f110449

.field public static final rootView:I = 0x7f110303

.field public static final root_view:I = 0x7f11036f

.field public static final row:I = 0x7f110078

.field public static final row_reverse:I = 0x7f110079

.field public static final rv_addresses:I = 0x7f1101fe

.field public static final sandbox:I = 0x7f1100a5

.field public static final satellite:I = 0x7f11008e

.field public static final save_image_matrix:I = 0x7f11001f

.field public static final save_non_transition_alpha:I = 0x7f110020

.field public static final save_scale_type:I = 0x7f110021

.field public static final savingTextView:I = 0x7f1101f5

.field public static final schedule_desc_textview:I = 0x7f1101d7

.field public static final schedule_payment_end_row:I = 0x7f110119

.field public static final schedule_payment_end_textview:I = 0x7f11011a

.field public static final schedule_payment_interval_row:I = 0x7f110117

.field public static final schedule_payment_interval_textview:I = 0x7f110118

.field public static final schedule_payment_start_row:I = 0x7f110115

.field public static final schedule_payment_start_textview:I = 0x7f110116

.field public static final schedule_payment_switch:I = 0x7f110114

.field public static final screen:I = 0x7f110055

.field public static final scroll:I = 0x7f110052

.field public static final scrollIndicatorDown:I = 0x7f1100da

.field public static final scrollIndicatorUp:I = 0x7f1100d6

.field public static final scrollView:I = 0x7f1100d7

.field public static final scroll_view:I = 0x7f110363

.field public static final scrollable:I = 0x7f11009f

.field public static final sdd_body_textview:I = 0x7f1101ab

.field public static final sdd_image_animationview:I = 0x7f1101a9

.field public static final sdd_not_now_button:I = 0x7f1101ad

.field public static final sdd_title_textview:I = 0x7f1101aa

.field public static final sdd_verify_button:I = 0x7f1101ac

.field public static final searchIcon:I = 0x7f110178

.field public static final search_badge:I = 0x7f1100ef

.field public static final search_bar:I = 0x7f1100ee

.field public static final search_button:I = 0x7f1100f0

.field public static final search_close_btn:I = 0x7f1100f5

.field public static final search_edit_frame:I = 0x7f1100f1

.field public static final search_feed_recyclerview:I = 0x7f11016c

.field public static final search_go_btn:I = 0x7f1100f7

.field public static final search_mag_icon:I = 0x7f1100f2

.field public static final search_plate:I = 0x7f1100f3

.field public static final search_src_text:I = 0x7f1100f4

.field public static final search_view:I = 0x7f11016b

.field public static final search_voice_btn:I = 0x7f1100f8

.field public static final secondaryDocumentThumbnail:I = 0x7f110308

.field public static final selectAddressToolbar:I = 0x7f1101fb

.field public static final select_dialog_listview:I = 0x7f1100f9

.field public static final selectionDetails:I = 0x7f1100a9

.field public static final send_button:I = 0x7f1103ac

.field public static final send_button_fading_background:I = 0x7f1103ab

.field public static final send_money_viewgroup:I = 0x7f1101d2

.field public static final send_replacement_card_button:I = 0x7f110123

.field public static final sendmoney_progressbar:I = 0x7f110331

.field public static final sendmoney_recyclerview:I = 0x7f110333

.field public static final sendmoney_swiperefreshlayout:I = 0x7f110332

.field public static final share:I = 0x7f11021a

.field public static final shareAccountDetailsButton:I = 0x7f1104aa

.field public static final share_1_text:I = 0x7f110293

.field public static final share_2_text:I = 0x7f110294

.field public static final share_3_text:I = 0x7f110295

.field public static final share_4_text:I = 0x7f110296

.field public static final share_custom_text:I = 0x7f110297

.field public static final share_more_invites:I = 0x7f11011e

.field public static final share_text_container:I = 0x7f110292

.field public static final shortcut:I = 0x7f1100e3

.field public static final showCustom:I = 0x7f11004b

.field public static final showHome:I = 0x7f11004c

.field public static final showTitle:I = 0x7f11004d

.field public static final signupAddressConfirmButton:I = 0x7f110107

.field public static final signupAddressInputView:I = 0x7f110106

.field public static final signupAddressSelectionView:I = 0x7f110321

.field public static final signupPendingBackButton:I = 0x7f110221

.field public static final signupPendingLottie:I = 0x7f11021f

.field public static final signupPendingTitle:I = 0x7f110220

.field public static final signupRejectedLottie:I = 0x7f110222

.field public static final signupRejectedTitle:I = 0x7f110223

.field public static final signupStatusErrorView:I = 0x7f110226

.field public static final signupStatusProgress:I = 0x7f110225

.field public static final signup_continue:I = 0x7f11021e

.field public static final slide:I = 0x7f110071

.field public static final small:I = 0x7f1100c6

.field public static final smallLabel:I = 0x7f1102ac

.field public static final snackbar_action:I = 0x7f1102b3

.field public static final snackbar_text:I = 0x7f1102b2

.field public static final snap:I = 0x7f110053

.field public static final sneakPeeksCategory:I = 0x7f110181

.field public static final social_button_layout:I = 0x7f11038d

.field public static final sortCodeTextView:I = 0x7f1104a5

.field public static final sortCodeViewGroup:I = 0x7f1104a4

.field public static final sort_code_edit_text:I = 0x7f110359

.field public static final sort_code_input_layout:I = 0x7f110358

.field public static final space_around:I = 0x7f11007e

.field public static final space_between:I = 0x7f11007f

.field public static final spacer:I = 0x7f1100d3

.field public static final spendTodayBalanceView:I = 0x7f1102fe

.field public static final spendingEmptyView:I = 0x7f11033a

.field public static final spendingRecyclerView:I = 0x7f110339

.field public static final spendingReportFeedback:I = 0x7f1104d7

.field public static final spending_amountview:I = 0x7f1104b9

.field public static final spending_content_frame:I = 0x7f11022a

.field public static final spending_recyclerview:I = 0x7f110341

.field public static final spending_report_amountview:I = 0x7f11022c

.field public static final spending_report_month_textview:I = 0x7f11022d

.field public static final spending_report_spending_button:I = 0x7f11022f

.field public static final spending_report_subtext_textview:I = 0x7f11022e

.field public static final spending_report_title_textview:I = 0x7f11022b

.field public static final spentTodayAmount:I = 0x7f110300

.field public static final spentTodayLabel:I = 0x7f1102ff

.field public static final split_action_bar:I = 0x7f110022

.field public static final spread:I = 0x7f110037

.field public static final spread_inside:I = 0x7f11003a

.field public static final src_atop:I = 0x7f110056

.field public static final src_in:I = 0x7f110057

.field public static final src_over:I = 0x7f110058

.field public static final standard:I = 0x7f11009c

.field public static final start:I = 0x7f110033

.field public static final status_bar_latest_event_content:I = 0x7f110444

.field public static final street_address:I = 0x7f110160

.field public static final street_address_wrapper:I = 0x7f11015f

.field public static final stretch:I = 0x7f110081

.field public static final strict_sandbox:I = 0x7f1100a6

.field public static final strong:I = 0x7f11008b

.field public static final submenuarrow:I = 0x7f1100e5

.field public static final submit:I = 0x7f110166

.field public static final submitVerificationButton:I = 0x7f11030e

.field public static final submit_area:I = 0x7f1100f6

.field public static final subtitle:I = 0x7f110139

.field public static final subtitle_textview:I = 0x7f110433

.field public static final surface_view:I = 0x7f110044

.field public static final swipe_refresh:I = 0x7f110409

.field public static final switchview_description_textview:I = 0x7f1104c1

.field public static final switchview_progressbar:I = 0x7f1104bf

.field public static final switchview_switch:I = 0x7f1104c0

.field public static final switchview_title_textview:I = 0x7f1104be

.field public static final tabMode:I = 0x7f110048

.field public static final tab_layout:I = 0x7f110229

.field public static final takeLaterButton:I = 0x7f1101f1

.field public static final takePhotoButton:I = 0x7f110309

.field public static final takeVideoButton:I = 0x7f11030d

.field public static final taxNumberEditText:I = 0x7f1104c4

.field public static final taxNumberInputLayout:I = 0x7f1104c3

.field public static final taxNumberView:I = 0x7f110348

.field public static final taxResidencyBody:I = 0x7f110342

.field public static final taxResidencyElsewhereButton:I = 0x7f110347

.field public static final taxResidencyErrorView:I = 0x7f110239

.field public static final taxResidencyFragmentContainer:I = 0x7f110238

.field public static final taxResidencyImage:I = 0x7f110344

.field public static final taxResidencyJustUkButton:I = 0x7f110346

.field public static final taxResidencyNonUsCitizenButton:I = 0x7f11034b

.field public static final taxResidencyProgress:I = 0x7f11023a

.field public static final taxResidencyTitle:I = 0x7f110345

.field public static final taxResidencyUsCitizenButton:I = 0x7f11034a

.field public static final termsAndConditionsView:I = 0x7f11020e

.field public static final terms_and_conditions:I = 0x7f1104ae

.field public static final terms_next_page_button:I = 0x7f11023c

.field public static final terms_webview:I = 0x7f11023b

.field public static final terrain:I = 0x7f11008f

.field public static final test:I = 0x7f1100a7

.field public static final text:I = 0x7f110023

.field public static final text2:I = 0x7f110024

.field public static final textSpacerNoButtons:I = 0x7f1100d9

.field public static final textSpacerNoTitle:I = 0x7f1100d8

.field public static final textToReadTextView:I = 0x7f1101f4

.field public static final textView:I = 0x7f110205

.field public static final text_input_password_toggle:I = 0x7f1102b9

.field public static final text_me_button:I = 0x7f1102c7

.field public static final textinput_counter:I = 0x7f110025

.field public static final textinput_error:I = 0x7f110026

.field public static final texture_view:I = 0x7f110045

.field public static final three_finger_swipe_detector_view:I = 0x7f110276

.field public static final thumbnail:I = 0x7f11039b

.field public static final thumbnail_imageview:I = 0x7f110237

.field public static final time:I = 0x7f11044a

.field public static final title:I = 0x7f110027

.field public static final titleDividerNoCustom:I = 0x7f1100e0

.field public static final titleView:I = 0x7f11047c

.field public static final title_bar_text:I = 0x7f11036c

.field public static final title_template:I = 0x7f1100de

.field public static final title_text_view:I = 0x7f1104a0

.field public static final title_textview:I = 0x7f1101e2

.field public static final toggleCardActionView:I = 0x7f1102d8

.field public static final toggleCardOverlayView:I = 0x7f1102d6

.field public static final tokenEditText:I = 0x7f1100fd

.field public static final tokenTextInputLayout:I = 0x7f1100fc

.field public static final tokenView:I = 0x7f1102d5

.field public static final toolbar:I = 0x7f1100fe

.field public static final toolbar_shadow:I = 0x7f1103b6

.field public static final toolbar_title:I = 0x7f1103cd

.field public static final top:I = 0x7f110034

.field public static final topPanel:I = 0x7f1100dd

.field public static final topShadow:I = 0x7f11033b

.field public static final topUpAccountNumberLabel:I = 0x7f110253

.field public static final topUpAccountNumberLayout:I = 0x7f110252

.field public static final topUpAccountNumberValue:I = 0x7f110254

.field public static final topUpBeneficiaryLabel:I = 0x7f110250

.field public static final topUpBeneficiaryLayout:I = 0x7f11024f

.field public static final topUpBeneficiaryValue:I = 0x7f110251

.field public static final topUpInstructionsTextView:I = 0x7f11024e

.field public static final topUpReferenceLabel:I = 0x7f110259

.field public static final topUpReferenceLayout:I = 0x7f110258

.field public static final topUpReferenceValue:I = 0x7f11025a

.field public static final topUpSortCodeLabel:I = 0x7f110256

.field public static final topUpSortCodeLayout:I = 0x7f110255

.field public static final topUpSortCodeValue:I = 0x7f110257

.field public static final topicContent:I = 0x7f110190

.field public static final topicFeedbackNegative:I = 0x7f110192

.field public static final topicFeedbackPositive:I = 0x7f110193

.field public static final topicTitle:I = 0x7f11018f

.field public static final topupActionView:I = 0x7f1102db

.field public static final topupShare:I = 0x7f1104d8

.field public static final topup_overlay_view:I = 0x7f11024b

.field public static final topup_progressbar:I = 0x7f11024c

.field public static final topup_toload_amountview:I = 0x7f110245

.field public static final topup_with_debit_card:I = 0x7f11024d

.field public static final torch:I = 0x7f110062

.field public static final total_spent_amountview:I = 0x7f11042f

.field public static final touch_interceptor:I = 0x7f11040e

.field public static final touch_outside:I = 0x7f1102b0

.field public static final transactionActionFab:I = 0x7f110266

.field public static final transactionActionIconImageView:I = 0x7f110474

.field public static final transactionActionSubtitleTextView:I = 0x7f110476

.field public static final transactionActionTitleTextView:I = 0x7f110475

.field public static final transactionActionsView:I = 0x7f110262

.field public static final transactionAmountView:I = 0x7f110483

.field public static final transactionAppBarLayout:I = 0x7f11025f

.field public static final transactionAvatarImageView:I = 0x7f11047e

.field public static final transactionAvatarView:I = 0x7f110265

.field public static final transactionContentView:I = 0x7f110263

.field public static final transactionDateTextView:I = 0x7f110487

.field public static final transactionDeclinedExtraInformationTextView:I = 0x7f110489

.field public static final transactionDeclinedTextView:I = 0x7f11048b

.field public static final transactionDescriptionTextView:I = 0x7f110437

.field public static final transactionExtraInformationTextView:I = 0x7f110488

.field public static final transactionFooterTextView:I = 0x7f110482

.field public static final transactionFooterView:I = 0x7f110264

.field public static final transactionFurtherInformationButton:I = 0x7f11047f

.field public static final transactionHeaderBarrier:I = 0x7f110485

.field public static final transactionHeaderView:I = 0x7f110261

.field public static final transactionHistoryHeaderTextView:I = 0x7f110480

.field public static final transactionHistorySubtitleTextView:I = 0x7f11048d

.field public static final transactionHistoryTitleTextView:I = 0x7f11048c

.field public static final transactionHistoryValueTextView:I = 0x7f11048e

.field public static final transactionHistoryViewGroup:I = 0x7f110481

.field public static final transactionLocalAmountTextView:I = 0x7f11048a

.field public static final transactionLogoOutlineView:I = 0x7f11047d

.field public static final transactionRootView:I = 0x7f11025e

.field public static final transactionSubtitleTextView:I = 0x7f110486

.field public static final transactionTitleTextView:I = 0x7f110484

.field public static final transition_current_scene:I = 0x7f110028

.field public static final transition_layout_save:I = 0x7f110029

.field public static final transition_position:I = 0x7f11002a

.field public static final transition_scene_layoutid_cache:I = 0x7f11002b

.field public static final transition_transform:I = 0x7f11002c

.field public static final trendingErrorText:I = 0x7f11017f

.field public static final trendingLayout:I = 0x7f11017c

.field public static final trendingProgress:I = 0x7f11017e

.field public static final trendingRecyclerView:I = 0x7f11017d

.field public static final trendingTitle:I = 0x7f11017b

.field public static final trendingTopicTitle:I = 0x7f110420

.field public static final tv_address_line_1:I = 0x7f11040f

.field public static final tv_address_line_2:I = 0x7f110410

.field public static final twitter_button:I = 0x7f11038e

.field public static final uk_bank_payment_fragment:I = 0x7f110110

.field public static final uniform:I = 0x7f11005a

.field public static final unknown:I = 0x7f1100be

.field public static final up:I = 0x7f11002d

.field public static final updated:I = 0x7f110367

.field public static final useLogo:I = 0x7f11004e

.field public static final useThisVideoButton:I = 0x7f1101b4

.field public static final userImageView:I = 0x7f110200

.field public static final verificationCodeEditText:I = 0x7f110338

.field public static final verificationCodeInputLayout:I = 0x7f110337

.field public static final verificationPendingImage:I = 0x7f110267

.field public static final verificationPendingProgress:I = 0x7f11026a

.field public static final verificationPendingSubtitle:I = 0x7f110269

.field public static final verificationPendingTitle:I = 0x7f110268

.field public static final verification_code:I = 0x7f110167

.field public static final vertical:I = 0x7f110064

.field public static final videoCompleteImageView:I = 0x7f11030a

.field public static final videoThumbnail:I = 0x7f11030c

.field public static final video_view_exoplayer:I = 0x7f11048f

.field public static final video_view_placeholder_imageview:I = 0x7f110490

.field public static final view_offset_helper:I = 0x7f11002e

.field public static final view_pager:I = 0x7f110227

.field public static final view_saved_card:I = 0x7f11025c

.field public static final view_topup:I = 0x7f110242

.field public static final view_topup_unavailable:I = 0x7f110240

.field public static final viewpager_indicator:I = 0x7f110104

.field public static final visible:I = 0x7f1104c6

.field public static final wallpaper:I = 0x7f1103c6

.field public static final warmingUpTextView:I = 0x7f1101f3

.field public static final weak:I = 0x7f11008c

.field public static final webView:I = 0x7f1101ae

.field public static final web_view:I = 0x7f11023d

.field public static final welcomeAccountNumberTextView:I = 0x7f11027e

.field public static final welcomeAccountNumberTitleTextView:I = 0x7f11027f

.field public static final welcomeBodyTextView:I = 0x7f110283

.field public static final welcomeLottieView:I = 0x7f11027c

.field public static final welcomeNotRightNowButton:I = 0x7f110285

.field public static final welcomeShowMeButton:I = 0x7f110284

.field public static final welcomeSortCodeTextView:I = 0x7f110280

.field public static final welcomeSortCodeTitleTextView:I = 0x7f110281

.field public static final welcomeTitleTextView:I = 0x7f110282

.field public static final welcome_logo_imageview:I = 0x7f11045d

.field public static final welcome_logo_lottieview:I = 0x7f11045e

.field public static final welcome_rootview:I = 0x7f11045b

.field public static final welcome_videoimageview:I = 0x7f11045c

.field public static final what_you_get_feature1_textview:I = 0x7f110454

.field public static final what_you_get_feature2_textview:I = 0x7f110455

.field public static final what_you_get_feature3_extra_textview:I = 0x7f110458

.field public static final what_you_get_feature3_textview:I = 0x7f110457

.field public static final what_you_get_feature4_extra_imageview:I = 0x7f11045a

.field public static final what_you_get_feature4_textview:I = 0x7f110459

.field public static final what_you_get_guideline:I = 0x7f110453

.field public static final what_you_get_imageview:I = 0x7f11045f

.field public static final what_you_get_left_margin_guideline:I = 0x7f110452

.field public static final what_you_get_title:I = 0x7f110460

.field public static final wide:I = 0x7f11009d

.field public static final width:I = 0x7f1100c8

.field public static final withText:I = 0x7f11009a

.field public static final wrap:I = 0x7f110038

.field public static final wrap_content:I = 0x7f11005b

.field public static final wrap_reverse:I = 0x7f11007b

.field public static final zoomable_imageview:I = 0x7f11010d
