.class public final Lco/uk/getmondo/main/b;
.super Ljava/lang/Object;
.source "HomeActivity_MembersInjector.java"

# interfaces
.implements Lb/a;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lb/a",
        "<",
        "Lco/uk/getmondo/main/HomeActivity;",
        ">;"
    }
.end annotation


# static fields
.field static final synthetic a:Z


# instance fields
.field private final b:Ljavax/a/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/main/c;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Ljavax/a/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/common/q;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Ljavax/a/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/common/a/c;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 9
    const-class v0, Lco/uk/getmondo/main/b;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lco/uk/getmondo/main/b;->a:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/main/c;",
            ">;",
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/common/q;",
            ">;",
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/common/a/c;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    sget-boolean v0, Lco/uk/getmondo/main/b;->a:Z

    if-nez v0, :cond_0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 25
    :cond_0
    iput-object p1, p0, Lco/uk/getmondo/main/b;->b:Ljavax/a/a;

    .line 26
    sget-boolean v0, Lco/uk/getmondo/main/b;->a:Z

    if-nez v0, :cond_1

    if-nez p2, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 27
    :cond_1
    iput-object p2, p0, Lco/uk/getmondo/main/b;->c:Ljavax/a/a;

    .line 28
    sget-boolean v0, Lco/uk/getmondo/main/b;->a:Z

    if-nez v0, :cond_2

    if-nez p3, :cond_2

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 29
    :cond_2
    iput-object p3, p0, Lco/uk/getmondo/main/b;->d:Ljavax/a/a;

    .line 30
    return-void
.end method

.method public static a(Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;)Lb/a;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/main/c;",
            ">;",
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/common/q;",
            ">;",
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/common/a/c;",
            ">;)",
            "Lb/a",
            "<",
            "Lco/uk/getmondo/main/HomeActivity;",
            ">;"
        }
    .end annotation

    .prologue
    .line 36
    new-instance v0, Lco/uk/getmondo/main/b;

    invoke-direct {v0, p0, p1, p2}, Lco/uk/getmondo/main/b;-><init>(Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;)V

    return-object v0
.end method


# virtual methods
.method public a(Lco/uk/getmondo/main/HomeActivity;)V
    .locals 2

    .prologue
    .line 42
    if-nez p1, :cond_0

    .line 43
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Cannot inject members into a null reference"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 45
    :cond_0
    iget-object v0, p0, Lco/uk/getmondo/main/b;->b:Ljavax/a/a;

    invoke-interface {v0}, Ljavax/a/a;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/main/c;

    iput-object v0, p1, Lco/uk/getmondo/main/HomeActivity;->b:Lco/uk/getmondo/main/c;

    .line 46
    iget-object v0, p0, Lco/uk/getmondo/main/b;->c:Ljavax/a/a;

    invoke-interface {v0}, Ljavax/a/a;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/common/q;

    iput-object v0, p1, Lco/uk/getmondo/main/HomeActivity;->c:Lco/uk/getmondo/common/q;

    .line 47
    iget-object v0, p0, Lco/uk/getmondo/main/b;->d:Ljavax/a/a;

    invoke-interface {v0}, Ljavax/a/a;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/common/a/c;

    iput-object v0, p1, Lco/uk/getmondo/main/HomeActivity;->e:Lco/uk/getmondo/common/a/c;

    .line 48
    return-void
.end method

.method public bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 9
    check-cast p1, Lco/uk/getmondo/main/HomeActivity;

    invoke-virtual {p0, p1}, Lco/uk/getmondo/main/b;->a(Lco/uk/getmondo/main/HomeActivity;)V

    return-void
.end method
