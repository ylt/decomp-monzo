.class final Lco/uk/getmondo/main/c$ag$1;
.super Ljava/lang/Object;
.source "HomePresenter.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lco/uk/getmondo/main/c$ag;->a(Lkotlin/n;)Lio/reactivex/h;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h",
        "<TT;",
        "Lio/reactivex/r",
        "<+TR;>;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000\u0014\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a&\u0012\u000c\u0012\n \u0003*\u0004\u0018\u00010\u00020\u0002 \u0003*\u0012\u0012\u000c\u0012\n \u0003*\u0004\u0018\u00010\u00020\u0002\u0018\u00010\u00010\u00012\u0006\u0010\u0004\u001a\u00020\u0005H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "Lio/reactivex/Observable;",
        "Lco/uk/getmondo/api/model/service_status/ServiceStatusIncident;",
        "kotlin.jvm.PlatformType",
        "<name for destructuring parameter 0>",
        "Lco/uk/getmondo/api/model/service_status/ServiceStatus;",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# static fields
.field public static final a:Lco/uk/getmondo/main/c$ag$1;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lco/uk/getmondo/main/c$ag$1;

    invoke-direct {v0}, Lco/uk/getmondo/main/c$ag$1;-><init>()V

    sput-object v0, Lco/uk/getmondo/main/c$ag$1;->a:Lco/uk/getmondo/main/c$ag$1;

    return-void
.end method

.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lco/uk/getmondo/api/model/service_status/ServiceStatus;)Lio/reactivex/n;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lco/uk/getmondo/api/model/service_status/ServiceStatus;",
            ")",
            "Lio/reactivex/n",
            "<",
            "Lco/uk/getmondo/api/model/service_status/ServiceStatusIncident;",
            ">;"
        }
    .end annotation

    .prologue
    const-string v0, "<name for destructuring parameter 0>"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lco/uk/getmondo/api/model/service_status/ServiceStatus;->a()Ljava/util/List;

    move-result-object v0

    .line 109
    check-cast v0, Ljava/lang/Iterable;

    invoke-static {v0}, Lio/reactivex/n;->fromIterable(Ljava/lang/Iterable;)Lio/reactivex/n;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 45
    check-cast p1, Lco/uk/getmondo/api/model/service_status/ServiceStatus;

    invoke-virtual {p0, p1}, Lco/uk/getmondo/main/c$ag$1;->a(Lco/uk/getmondo/api/model/service_status/ServiceStatus;)Lio/reactivex/n;

    move-result-object v0

    return-object v0
.end method
