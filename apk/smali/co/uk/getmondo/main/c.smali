.class public final Lco/uk/getmondo/main/c;
.super Lco/uk/getmondo/common/ui/b;
.source "HomePresenter.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lco/uk/getmondo/main/c$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lco/uk/getmondo/common/ui/b",
        "<",
        "Lco/uk/getmondo/main/c$a;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000\u008c\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0004\u0008\u0007\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00020\u0001:\u0001/B\u00a3\u0001\u0008\u0007\u0012\u0008\u0008\u0001\u0010\u0003\u001a\u00020\u0004\u0012\u0008\u0008\u0001\u0010\u0005\u001a\u00020\u0004\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b\u0012\u0006\u0010\u000c\u001a\u00020\r\u0012\u0006\u0010\u000e\u001a\u00020\u000f\u0012\u0006\u0010\u0010\u001a\u00020\u0011\u0012\u0006\u0010\u0012\u001a\u00020\u0013\u0012\u0006\u0010\u0014\u001a\u00020\u0015\u0012\u0006\u0010\u0016\u001a\u00020\u0017\u0012\u0006\u0010\u0018\u001a\u00020\u0019\u0012\u0006\u0010\u001a\u001a\u00020\u001b\u0012\u0006\u0010\u001c\u001a\u00020\u001d\u0012\u0006\u0010\u001e\u001a\u00020\u001f\u0012\u0006\u0010 \u001a\u00020!\u0012\u0006\u0010\"\u001a\u00020#\u0012\u0006\u0010$\u001a\u00020%\u0012\u0006\u0010&\u001a\u00020\'\u00a2\u0006\u0002\u0010(J\u0010\u0010+\u001a\u00020,2\u0006\u0010)\u001a\u00020*H\u0002J\u0010\u0010-\u001a\u00020,2\u0006\u0010.\u001a\u00020\u0002H\u0016R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000c\u001a\u00020\rX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010$\u001a\u00020%X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\"\u001a\u00020#X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001c\u001a\u00020\u001dX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010)\u001a\u0004\u0018\u00010*X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0016\u001a\u00020\u0017X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010 \u001a\u00020!X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0014\u001a\u00020\u0015X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001e\u001a\u00020\u001fX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001a\u001a\u00020\u001bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u000fX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010&\u001a\u00020\'X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0018\u001a\u00020\u0019X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0010\u001a\u00020\u0011X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0012\u001a\u00020\u0013X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u00060"
    }
    d2 = {
        "Lco/uk/getmondo/main/HomePresenter;",
        "Lco/uk/getmondo/common/ui/BasePresenter;",
        "Lco/uk/getmondo/main/HomePresenter$View;",
        "uiScheduler",
        "Lio/reactivex/Scheduler;",
        "ioScheduler",
        "accountService",
        "Lco/uk/getmondo/common/accounts/AccountService;",
        "apiErrorHandler",
        "Lco/uk/getmondo/common/errors/ApiErrorHandler;",
        "userInteractor",
        "Lco/uk/getmondo/api/interactors/UserInteractor;",
        "analyticsService",
        "Lco/uk/getmondo/common/AnalyticsService;",
        "serviceStatusApi",
        "Lco/uk/getmondo/api/ServiceStatusApi;",
        "userSettingsRepository",
        "Lco/uk/getmondo/payments/send/data/UserSettingsRepository;",
        "userSettingsStorage",
        "Lco/uk/getmondo/payments/send/data/UserSettingsStorage;",
        "peerToPeerRepository",
        "Lco/uk/getmondo/payments/send/data/PeerToPeerRepository;",
        "newsManager",
        "Lco/uk/getmondo/news/NewsManager;",
        "syncManager",
        "Lco/uk/getmondo/background_sync/SyncManager;",
        "sddMigrationStorage",
        "Lco/uk/getmondo/common/SddMigrationStorage;",
        "identityVerificationApi",
        "Lco/uk/getmondo/api/IdentityVerificationApi;",
        "sddDismissibleState",
        "Lco/uk/getmondo/main/SddDismissibleState;",
        "peerManager",
        "Lco/uk/getmondo/feed/data/PeerManager;",
        "featureFlagsStorage",
        "Lco/uk/getmondo/common/FeatureFlagsStorage;",
        "cardManager",
        "Lco/uk/getmondo/card/CardManager;",
        "signupStatusManager",
        "Lco/uk/getmondo/signup/status/SignupStatusManager;",
        "(Lio/reactivex/Scheduler;Lio/reactivex/Scheduler;Lco/uk/getmondo/common/accounts/AccountService;Lco/uk/getmondo/common/errors/ApiErrorHandler;Lco/uk/getmondo/api/interactors/UserInteractor;Lco/uk/getmondo/common/AnalyticsService;Lco/uk/getmondo/api/ServiceStatusApi;Lco/uk/getmondo/payments/send/data/UserSettingsRepository;Lco/uk/getmondo/payments/send/data/UserSettingsStorage;Lco/uk/getmondo/payments/send/data/PeerToPeerRepository;Lco/uk/getmondo/news/NewsManager;Lco/uk/getmondo/background_sync/SyncManager;Lco/uk/getmondo/common/SddMigrationStorage;Lco/uk/getmondo/api/IdentityVerificationApi;Lco/uk/getmondo/main/SddDismissibleState;Lco/uk/getmondo/feed/data/PeerManager;Lco/uk/getmondo/common/FeatureFlagsStorage;Lco/uk/getmondo/card/CardManager;Lco/uk/getmondo/signup/status/SignupStatusManager;)V",
        "migrationInfo",
        "Lco/uk/getmondo/api/model/sign_up/MigrationInfo;",
        "displayMigrationBanner",
        "",
        "register",
        "view",
        "View",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field private c:Lco/uk/getmondo/api/model/sign_up/MigrationInfo;

.field private final d:Lio/reactivex/u;

.field private final e:Lio/reactivex/u;

.field private final f:Lco/uk/getmondo/common/accounts/d;

.field private final g:Lco/uk/getmondo/common/e/a;

.field private final h:Lco/uk/getmondo/api/b/a;

.field private final i:Lco/uk/getmondo/common/a;

.field private final j:Lco/uk/getmondo/api/ServiceStatusApi;

.field private final k:Lco/uk/getmondo/payments/send/data/h;

.field private final l:Lco/uk/getmondo/payments/send/data/p;

.field private final m:Lco/uk/getmondo/payments/send/data/PeerToPeerRepository;

.field private final n:Lco/uk/getmondo/news/d;

.field private final o:Lco/uk/getmondo/background_sync/d;

.field private final p:Lco/uk/getmondo/common/x;

.field private final q:Lco/uk/getmondo/api/IdentityVerificationApi;

.field private final r:Lco/uk/getmondo/main/h;

.field private final s:Lco/uk/getmondo/feed/a/t;

.field private final t:Lco/uk/getmondo/common/o;

.field private final u:Lco/uk/getmondo/card/c;

.field private final v:Lco/uk/getmondo/signup/status/b;


# direct methods
.method public constructor <init>(Lio/reactivex/u;Lio/reactivex/u;Lco/uk/getmondo/common/accounts/d;Lco/uk/getmondo/common/e/a;Lco/uk/getmondo/api/b/a;Lco/uk/getmondo/common/a;Lco/uk/getmondo/api/ServiceStatusApi;Lco/uk/getmondo/payments/send/data/h;Lco/uk/getmondo/payments/send/data/p;Lco/uk/getmondo/payments/send/data/PeerToPeerRepository;Lco/uk/getmondo/news/d;Lco/uk/getmondo/background_sync/d;Lco/uk/getmondo/common/x;Lco/uk/getmondo/api/IdentityVerificationApi;Lco/uk/getmondo/main/h;Lco/uk/getmondo/feed/a/t;Lco/uk/getmondo/common/o;Lco/uk/getmondo/card/c;Lco/uk/getmondo/signup/status/b;)V
    .locals 2

    .prologue
    const-string v1, "uiScheduler"

    invoke-static {p1, v1}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "ioScheduler"

    invoke-static {p2, v1}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "accountService"

    invoke-static {p3, v1}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "apiErrorHandler"

    invoke-static {p4, v1}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "userInteractor"

    invoke-static {p5, v1}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "analyticsService"

    invoke-static {p6, v1}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "serviceStatusApi"

    invoke-static {p7, v1}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "userSettingsRepository"

    invoke-static {p8, v1}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "userSettingsStorage"

    invoke-static {p9, v1}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "peerToPeerRepository"

    invoke-static {p10, v1}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "newsManager"

    invoke-static {p11, v1}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "syncManager"

    invoke-static {p12, v1}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "sddMigrationStorage"

    invoke-static {p13, v1}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "identityVerificationApi"

    move-object/from16 v0, p14

    invoke-static {v0, v1}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "sddDismissibleState"

    move-object/from16 v0, p15

    invoke-static {v0, v1}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "peerManager"

    move-object/from16 v0, p16

    invoke-static {v0, v1}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "featureFlagsStorage"

    move-object/from16 v0, p17

    invoke-static {v0, v1}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "cardManager"

    move-object/from16 v0, p18

    invoke-static {v0, v1}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "signupStatusManager"

    move-object/from16 v0, p19

    invoke-static {v0, v1}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 66
    invoke-direct {p0}, Lco/uk/getmondo/common/ui/b;-><init>()V

    iput-object p1, p0, Lco/uk/getmondo/main/c;->d:Lio/reactivex/u;

    iput-object p2, p0, Lco/uk/getmondo/main/c;->e:Lio/reactivex/u;

    iput-object p3, p0, Lco/uk/getmondo/main/c;->f:Lco/uk/getmondo/common/accounts/d;

    iput-object p4, p0, Lco/uk/getmondo/main/c;->g:Lco/uk/getmondo/common/e/a;

    iput-object p5, p0, Lco/uk/getmondo/main/c;->h:Lco/uk/getmondo/api/b/a;

    iput-object p6, p0, Lco/uk/getmondo/main/c;->i:Lco/uk/getmondo/common/a;

    iput-object p7, p0, Lco/uk/getmondo/main/c;->j:Lco/uk/getmondo/api/ServiceStatusApi;

    iput-object p8, p0, Lco/uk/getmondo/main/c;->k:Lco/uk/getmondo/payments/send/data/h;

    iput-object p9, p0, Lco/uk/getmondo/main/c;->l:Lco/uk/getmondo/payments/send/data/p;

    iput-object p10, p0, Lco/uk/getmondo/main/c;->m:Lco/uk/getmondo/payments/send/data/PeerToPeerRepository;

    iput-object p11, p0, Lco/uk/getmondo/main/c;->n:Lco/uk/getmondo/news/d;

    iput-object p12, p0, Lco/uk/getmondo/main/c;->o:Lco/uk/getmondo/background_sync/d;

    iput-object p13, p0, Lco/uk/getmondo/main/c;->p:Lco/uk/getmondo/common/x;

    move-object/from16 v0, p14

    iput-object v0, p0, Lco/uk/getmondo/main/c;->q:Lco/uk/getmondo/api/IdentityVerificationApi;

    move-object/from16 v0, p15

    iput-object v0, p0, Lco/uk/getmondo/main/c;->r:Lco/uk/getmondo/main/h;

    move-object/from16 v0, p16

    iput-object v0, p0, Lco/uk/getmondo/main/c;->s:Lco/uk/getmondo/feed/a/t;

    move-object/from16 v0, p17

    iput-object v0, p0, Lco/uk/getmondo/main/c;->t:Lco/uk/getmondo/common/o;

    move-object/from16 v0, p18

    iput-object v0, p0, Lco/uk/getmondo/main/c;->u:Lco/uk/getmondo/card/c;

    move-object/from16 v0, p19

    iput-object v0, p0, Lco/uk/getmondo/main/c;->v:Lco/uk/getmondo/signup/status/b;

    return-void
.end method

.method public static final synthetic a(Lco/uk/getmondo/main/c;)Lco/uk/getmondo/common/e/a;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lco/uk/getmondo/main/c;->g:Lco/uk/getmondo/common/e/a;

    return-object v0
.end method

.method private final a(Lco/uk/getmondo/api/model/sign_up/MigrationInfo;)V
    .locals 4

    .prologue
    .line 305
    invoke-virtual {p1}, Lco/uk/getmondo/api/model/sign_up/MigrationInfo;->e()Lco/uk/getmondo/api/model/signup/SignupInfo$Status;

    move-result-object v0

    .line 306
    invoke-virtual {p1}, Lco/uk/getmondo/api/model/sign_up/MigrationInfo;->f()Lco/uk/getmondo/api/model/signup/SignupInfo$Stage;

    move-result-object v1

    .line 307
    invoke-virtual {p1}, Lco/uk/getmondo/api/model/sign_up/MigrationInfo;->b()Z

    move-result v2

    if-nez v2, :cond_0

    .line 309
    iget-object v0, p0, Lco/uk/getmondo/main/c;->a:Lco/uk/getmondo/common/ui/f;

    check-cast v0, Lco/uk/getmondo/main/c$a;

    invoke-virtual {p1}, Lco/uk/getmondo/api/model/sign_up/MigrationInfo;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lco/uk/getmondo/api/model/sign_up/MigrationInfo;->d()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-interface {v0, v1, v2, v3}, Lco/uk/getmondo/main/c$a;->a(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 310
    iget-object v0, p0, Lco/uk/getmondo/main/c;->i:Lco/uk/getmondo/common/a;

    sget-object v1, Lco/uk/getmondo/api/model/tracking/Impression;->Companion:Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    invoke-virtual {v1}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->aX()Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v1

    invoke-virtual {v0, v1}, Lco/uk/getmondo/common/a;->a(Lco/uk/getmondo/api/model/tracking/Impression;)V

    .line 323
    :goto_0
    return-void

    .line 312
    :cond_0
    sget-object v2, Lco/uk/getmondo/api/model/signup/SignupInfo$Status;->NOT_STARTED:Lco/uk/getmondo/api/model/signup/SignupInfo$Status;

    invoke-static {v0, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 313
    iget-object v0, p0, Lco/uk/getmondo/main/c;->a:Lco/uk/getmondo/common/ui/f;

    check-cast v0, Lco/uk/getmondo/main/c$a;

    invoke-virtual {p1}, Lco/uk/getmondo/api/model/sign_up/MigrationInfo;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lco/uk/getmondo/api/model/sign_up/MigrationInfo;->d()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    invoke-interface {v0, v1, v2, v3}, Lco/uk/getmondo/main/c$a;->a(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 314
    iget-object v0, p0, Lco/uk/getmondo/main/c;->i:Lco/uk/getmondo/common/a;

    sget-object v1, Lco/uk/getmondo/api/model/tracking/Impression;->Companion:Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    invoke-virtual {v1}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->ba()Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v1

    invoke-virtual {v0, v1}, Lco/uk/getmondo/common/a;->a(Lco/uk/getmondo/api/model/tracking/Impression;)V

    goto :goto_0

    .line 316
    :cond_1
    sget-object v0, Lco/uk/getmondo/api/model/signup/SignupInfo$Stage;->CARD_ACTIVATION:Lco/uk/getmondo/api/model/signup/SignupInfo$Stage;

    invoke-static {v1, v0}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 317
    iget-object v0, p0, Lco/uk/getmondo/main/c;->a:Lco/uk/getmondo/common/ui/f;

    check-cast v0, Lco/uk/getmondo/main/c$a;

    invoke-interface {v0}, Lco/uk/getmondo/main/c$a;->M()V

    goto :goto_0

    .line 320
    :cond_2
    iget-object v0, p0, Lco/uk/getmondo/main/c;->a:Lco/uk/getmondo/common/ui/f;

    check-cast v0, Lco/uk/getmondo/main/c$a;

    invoke-interface {v0}, Lco/uk/getmondo/main/c$a;->L()V

    .line 321
    iget-object v0, p0, Lco/uk/getmondo/main/c;->i:Lco/uk/getmondo/common/a;

    sget-object v1, Lco/uk/getmondo/api/model/tracking/Impression;->Companion:Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    invoke-virtual {v1}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->bg()Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v1

    invoke-virtual {v0, v1}, Lco/uk/getmondo/common/a;->a(Lco/uk/getmondo/api/model/tracking/Impression;)V

    goto :goto_0
.end method

.method public static final synthetic a(Lco/uk/getmondo/main/c;Lco/uk/getmondo/api/model/sign_up/MigrationInfo;)V
    .locals 0

    .prologue
    .line 45
    iput-object p1, p0, Lco/uk/getmondo/main/c;->c:Lco/uk/getmondo/api/model/sign_up/MigrationInfo;

    return-void
.end method

.method public static final synthetic b(Lco/uk/getmondo/main/c;)Lco/uk/getmondo/api/ServiceStatusApi;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lco/uk/getmondo/main/c;->j:Lco/uk/getmondo/api/ServiceStatusApi;

    return-object v0
.end method

.method public static final synthetic b(Lco/uk/getmondo/main/c;Lco/uk/getmondo/api/model/sign_up/MigrationInfo;)V
    .locals 0

    .prologue
    .line 45
    invoke-direct {p0, p1}, Lco/uk/getmondo/main/c;->a(Lco/uk/getmondo/api/model/sign_up/MigrationInfo;)V

    return-void
.end method

.method public static final synthetic c(Lco/uk/getmondo/main/c;)Lio/reactivex/u;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lco/uk/getmondo/main/c;->e:Lio/reactivex/u;

    return-object v0
.end method

.method public static final synthetic d(Lco/uk/getmondo/main/c;)Lco/uk/getmondo/payments/send/data/h;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lco/uk/getmondo/main/c;->k:Lco/uk/getmondo/payments/send/data/h;

    return-object v0
.end method

.method public static final synthetic e(Lco/uk/getmondo/main/c;)Lio/reactivex/u;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lco/uk/getmondo/main/c;->d:Lio/reactivex/u;

    return-object v0
.end method

.method public static final synthetic f(Lco/uk/getmondo/main/c;)Lco/uk/getmondo/api/IdentityVerificationApi;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lco/uk/getmondo/main/c;->q:Lco/uk/getmondo/api/IdentityVerificationApi;

    return-object v0
.end method

.method public static final synthetic g(Lco/uk/getmondo/main/c;)Lco/uk/getmondo/main/h;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lco/uk/getmondo/main/c;->r:Lco/uk/getmondo/main/h;

    return-object v0
.end method

.method public static final synthetic h(Lco/uk/getmondo/main/c;)Lco/uk/getmondo/payments/send/data/PeerToPeerRepository;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lco/uk/getmondo/main/c;->m:Lco/uk/getmondo/payments/send/data/PeerToPeerRepository;

    return-object v0
.end method

.method public static final synthetic i(Lco/uk/getmondo/main/c;)Lco/uk/getmondo/payments/send/data/p;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lco/uk/getmondo/main/c;->l:Lco/uk/getmondo/payments/send/data/p;

    return-object v0
.end method

.method public static final synthetic j(Lco/uk/getmondo/main/c;)Lco/uk/getmondo/common/a;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lco/uk/getmondo/main/c;->i:Lco/uk/getmondo/common/a;

    return-object v0
.end method

.method public static final synthetic k(Lco/uk/getmondo/main/c;)Lco/uk/getmondo/common/accounts/d;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lco/uk/getmondo/main/c;->f:Lco/uk/getmondo/common/accounts/d;

    return-object v0
.end method

.method public static final synthetic l(Lco/uk/getmondo/main/c;)Lco/uk/getmondo/common/o;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lco/uk/getmondo/main/c;->t:Lco/uk/getmondo/common/o;

    return-object v0
.end method

.method public static final synthetic m(Lco/uk/getmondo/main/c;)Lco/uk/getmondo/signup/status/b;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lco/uk/getmondo/main/c;->v:Lco/uk/getmondo/signup/status/b;

    return-object v0
.end method

.method public static final synthetic n(Lco/uk/getmondo/main/c;)Lco/uk/getmondo/api/model/sign_up/MigrationInfo;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lco/uk/getmondo/main/c;->c:Lco/uk/getmondo/api/model/sign_up/MigrationInfo;

    return-object v0
.end method


# virtual methods
.method public bridge synthetic a(Lco/uk/getmondo/common/ui/f;)V
    .locals 0

    .prologue
    .line 45
    check-cast p1, Lco/uk/getmondo/main/c$a;

    invoke-virtual {p0, p1}, Lco/uk/getmondo/main/c;->a(Lco/uk/getmondo/main/c$a;)V

    return-void
.end method

.method public a(Lco/uk/getmondo/main/c$a;)V
    .locals 6

    .prologue
    const-string v0, "view"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v0, p1

    .line 71
    check-cast v0, Lco/uk/getmondo/common/ui/f;

    invoke-super {p0, v0}, Lco/uk/getmondo/common/ui/b;->a(Lco/uk/getmondo/common/ui/f;)V

    .line 73
    iget-object v0, p0, Lco/uk/getmondo/main/c;->f:Lco/uk/getmondo/common/accounts/d;

    invoke-virtual {v0}, Lco/uk/getmondo/common/accounts/d;->b()Lco/uk/getmondo/d/ak;

    move-result-object v0

    .line 74
    if-nez v0, :cond_0

    invoke-static {}, Lkotlin/d/b/l;->a()V

    :cond_0
    invoke-virtual {v0}, Lco/uk/getmondo/d/ak;->d()Lco/uk/getmondo/d/ac;

    move-result-object v3

    .line 75
    invoke-virtual {v0}, Lco/uk/getmondo/d/ak;->c()Lco/uk/getmondo/d/a;

    move-result-object v0

    .line 76
    sget-object v1, Lco/uk/getmondo/a;->b:Ljava/lang/Boolean;

    const-string v2, "BuildConfig.GHOST"

    invoke-static {v1, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 77
    if-nez v3, :cond_1

    invoke-static {}, Lkotlin/d/b/l;->a()V

    :cond_1
    invoke-virtual {v3}, Lco/uk/getmondo/d/ac;->d()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v1}, Lco/uk/getmondo/main/c$a;->a(Ljava/lang/String;)V

    .line 79
    :cond_2
    if-eqz v0, :cond_7

    invoke-interface {v0}, Lco/uk/getmondo/d/a;->e()Z

    move-result v1

    if-eqz v1, :cond_7

    const/4 v1, 0x1

    move v2, v1

    .line 80
    :goto_0
    if-eqz v2, :cond_4

    .line 81
    check-cast v0, Lco/uk/getmondo/d/ad;

    .line 82
    if-nez v0, :cond_3

    invoke-static {}, Lkotlin/d/b/l;->a()V

    :cond_3
    invoke-virtual {v0}, Lco/uk/getmondo/d/ad;->g()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Lco/uk/getmondo/d/ad;->j()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v1, v0}, Lco/uk/getmondo/main/c$a;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 85
    :cond_4
    if-nez v3, :cond_5

    invoke-static {}, Lkotlin/d/b/l;->a()V

    :cond_5
    invoke-virtual {v3}, Lco/uk/getmondo/d/ac;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3}, Lco/uk/getmondo/d/ac;->d()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v0, v1}, Lco/uk/getmondo/main/c$a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 87
    iget-object v4, p0, Lco/uk/getmondo/main/c;->b:Lio/reactivex/b/a;

    .line 90
    iget-object v0, p0, Lco/uk/getmondo/main/c;->h:Lco/uk/getmondo/api/b/a;

    invoke-virtual {v0}, Lco/uk/getmondo/api/b/a;->a()Lio/reactivex/v;

    move-result-object v0

    .line 88
    iget-object v1, p0, Lco/uk/getmondo/main/c;->e:Lio/reactivex/u;

    invoke-virtual {v0, v1}, Lio/reactivex/v;->b(Lio/reactivex/u;)Lio/reactivex/v;

    move-result-object v0

    .line 89
    iget-object v1, p0, Lco/uk/getmondo/main/c;->d:Lio/reactivex/u;

    invoke-virtual {v0, v1}, Lio/reactivex/v;->a(Lio/reactivex/u;)Lio/reactivex/v;

    move-result-object v5

    .line 90
    sget-object v0, Lco/uk/getmondo/main/c$b;->a:Lco/uk/getmondo/main/c$b;

    check-cast v0, Lio/reactivex/c/g;

    .line 91
    new-instance v1, Lco/uk/getmondo/main/c$m;

    invoke-direct {v1, p0, p1}, Lco/uk/getmondo/main/c$m;-><init>(Lco/uk/getmondo/main/c;Lco/uk/getmondo/main/c$a;)V

    check-cast v1, Lio/reactivex/c/g;

    .line 90
    invoke-virtual {v5, v0, v1}, Lio/reactivex/v;->a(Lio/reactivex/c/g;Lio/reactivex/c/g;)Lio/reactivex/b/b;

    move-result-object v0

    const-string v1, "userInteractor.refreshAc\u2026ndleError(error, view) })"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 87
    invoke-static {v4, v0}, Lio/reactivex/rxkotlin/a;->a(Lio/reactivex/b/a;Lio/reactivex/b/b;)V

    .line 93
    iget-object v4, p0, Lco/uk/getmondo/main/c;->b:Lio/reactivex/b/a;

    .line 96
    iget-object v0, p0, Lco/uk/getmondo/main/c;->u:Lco/uk/getmondo/card/c;

    invoke-virtual {v0}, Lco/uk/getmondo/card/c;->c()Lio/reactivex/b;

    move-result-object v0

    .line 94
    iget-object v1, p0, Lco/uk/getmondo/main/c;->e:Lio/reactivex/u;

    invoke-virtual {v0, v1}, Lio/reactivex/b;->b(Lio/reactivex/u;)Lio/reactivex/b;

    move-result-object v0

    .line 95
    iget-object v1, p0, Lco/uk/getmondo/main/c;->d:Lio/reactivex/u;

    invoke-virtual {v0, v1}, Lio/reactivex/b;->a(Lio/reactivex/u;)Lio/reactivex/b;

    move-result-object v5

    .line 97
    sget-object v0, Lco/uk/getmondo/main/c$x;->a:Lco/uk/getmondo/main/c$x;

    check-cast v0, Lio/reactivex/c/a;

    .line 98
    new-instance v1, Lco/uk/getmondo/main/c$ad;

    invoke-direct {v1, p0, p1}, Lco/uk/getmondo/main/c$ad;-><init>(Lco/uk/getmondo/main/c;Lco/uk/getmondo/main/c$a;)V

    check-cast v1, Lio/reactivex/c/g;

    .line 96
    invoke-virtual {v5, v0, v1}, Lio/reactivex/b;->a(Lio/reactivex/c/a;Lio/reactivex/c/g;)Lio/reactivex/b/b;

    move-result-object v0

    const-string v1, "cardManager.syncCard()\n \u2026view) }\n                )"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 93
    invoke-static {v4, v0}, Lio/reactivex/rxkotlin/a;->a(Lio/reactivex/b/a;Lio/reactivex/b/b;)V

    .line 101
    iget-object v4, p0, Lco/uk/getmondo/main/c;->b:Lio/reactivex/b/a;

    .line 104
    iget-object v0, p0, Lco/uk/getmondo/main/c;->n:Lco/uk/getmondo/news/d;

    invoke-virtual {v0}, Lco/uk/getmondo/news/d;->a()Lio/reactivex/h;

    move-result-object v0

    .line 102
    iget-object v1, p0, Lco/uk/getmondo/main/c;->e:Lio/reactivex/u;

    invoke-virtual {v0, v1}, Lio/reactivex/h;->b(Lio/reactivex/u;)Lio/reactivex/h;

    move-result-object v0

    .line 103
    iget-object v1, p0, Lco/uk/getmondo/main/c;->d:Lio/reactivex/u;

    invoke-virtual {v0, v1}, Lio/reactivex/h;->a(Lio/reactivex/u;)Lio/reactivex/h;

    move-result-object v5

    .line 104
    new-instance v0, Lco/uk/getmondo/main/c$ae;

    invoke-direct {v0, p1}, Lco/uk/getmondo/main/c$ae;-><init>(Lco/uk/getmondo/main/c$a;)V

    check-cast v0, Lio/reactivex/c/g;

    new-instance v1, Lco/uk/getmondo/main/c$af;

    invoke-direct {v1, p0, p1}, Lco/uk/getmondo/main/c$af;-><init>(Lco/uk/getmondo/main/c;Lco/uk/getmondo/main/c$a;)V

    check-cast v1, Lio/reactivex/c/g;

    invoke-virtual {v5, v0, v1}, Lio/reactivex/h;->a(Lio/reactivex/c/g;Lio/reactivex/c/g;)Lio/reactivex/b/b;

    move-result-object v0

    const-string v1, "newsManager.unreadNewsIt\u2026ndleError(error, view) })"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 101
    invoke-static {v4, v0}, Lio/reactivex/rxkotlin/a;->a(Lio/reactivex/b/a;Lio/reactivex/b/b;)V

    .line 106
    iget-object v1, p0, Lco/uk/getmondo/main/c;->b:Lio/reactivex/b/a;

    .line 117
    invoke-interface {p1}, Lco/uk/getmondo/main/c$a;->h()Lio/reactivex/n;

    move-result-object v4

    .line 107
    new-instance v0, Lco/uk/getmondo/main/c$ag;

    invoke-direct {v0, p0}, Lco/uk/getmondo/main/c$ag;-><init>(Lco/uk/getmondo/main/c;)V

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {v4, v0}, Lio/reactivex/n;->flatMapMaybe(Lio/reactivex/c/h;)Lio/reactivex/n;

    move-result-object v0

    .line 116
    iget-object v4, p0, Lco/uk/getmondo/main/c;->d:Lio/reactivex/u;

    invoke-virtual {v0, v4}, Lio/reactivex/n;->observeOn(Lio/reactivex/u;)Lio/reactivex/n;

    move-result-object v4

    .line 117
    new-instance v0, Lco/uk/getmondo/main/c$ah;

    invoke-direct {v0, p1}, Lco/uk/getmondo/main/c$ah;-><init>(Lco/uk/getmondo/main/c$a;)V

    check-cast v0, Lio/reactivex/c/g;

    invoke-virtual {v4, v0}, Lio/reactivex/n;->subscribe(Lio/reactivex/c/g;)Lio/reactivex/b/b;

    move-result-object v0

    const-string v4, "view.onRefreshIncidents(\u2026      }\n                }"

    invoke-static {v0, v4}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 106
    invoke-static {v1, v0}, Lio/reactivex/rxkotlin/a;->a(Lio/reactivex/b/a;Lio/reactivex/b/b;)V

    .line 125
    iget-object v4, p0, Lco/uk/getmondo/main/c;->b:Lio/reactivex/b/a;

    .line 133
    invoke-interface {p1}, Lco/uk/getmondo/main/c$a;->b()Lio/reactivex/n;

    move-result-object v1

    .line 126
    new-instance v0, Lco/uk/getmondo/main/c$ai;

    invoke-direct {v0, p0, p1}, Lco/uk/getmondo/main/c$ai;-><init>(Lco/uk/getmondo/main/c;Lco/uk/getmondo/main/c$a;)V

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {v1, v0}, Lio/reactivex/n;->flatMapCompletable(Lio/reactivex/c/h;)Lio/reactivex/b;

    move-result-object v5

    .line 133
    sget-object v0, Lco/uk/getmondo/main/c$c;->a:Lco/uk/getmondo/main/c$c;

    check-cast v0, Lio/reactivex/c/a;

    sget-object v1, Lco/uk/getmondo/main/c$d;->a:Lco/uk/getmondo/main/c$d;

    check-cast v1, Lio/reactivex/c/g;

    invoke-virtual {v5, v0, v1}, Lio/reactivex/b;->a(Lio/reactivex/c/a;Lio/reactivex/c/g;)Lio/reactivex/b/b;

    move-result-object v0

    const-string v1, "view.onRefreshUserSettin\u2026ibe({}, { Timber.e(it) })"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 125
    invoke-static {v4, v0}, Lio/reactivex/rxkotlin/a;->a(Lio/reactivex/b/a;Lio/reactivex/b/b;)V

    .line 135
    iget-object v4, p0, Lco/uk/getmondo/main/c;->b:Lio/reactivex/b/a;

    .line 146
    iget-object v0, p0, Lco/uk/getmondo/main/c;->p:Lco/uk/getmondo/common/x;

    invoke-virtual {v0}, Lco/uk/getmondo/common/x;->a()Lio/reactivex/n;

    move-result-object v0

    .line 136
    invoke-virtual {v0}, Lio/reactivex/n;->distinctUntilChanged()Lio/reactivex/n;

    move-result-object v1

    .line 137
    sget-object v0, Lco/uk/getmondo/main/c$e;->a:Lco/uk/getmondo/main/c$e;

    check-cast v0, Lio/reactivex/c/q;

    invoke-virtual {v1, v0}, Lio/reactivex/n;->filter(Lio/reactivex/c/q;)Lio/reactivex/n;

    move-result-object v1

    .line 138
    new-instance v0, Lco/uk/getmondo/main/c$f;

    invoke-direct {v0, p0, p1}, Lco/uk/getmondo/main/c$f;-><init>(Lco/uk/getmondo/main/c;Lco/uk/getmondo/main/c$a;)V

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {v1, v0}, Lio/reactivex/n;->switchMapSingle(Lio/reactivex/c/h;)Lio/reactivex/n;

    move-result-object v5

    .line 146
    new-instance v0, Lco/uk/getmondo/main/c$g;

    invoke-direct {v0, p0, p1}, Lco/uk/getmondo/main/c$g;-><init>(Lco/uk/getmondo/main/c;Lco/uk/getmondo/main/c$a;)V

    check-cast v0, Lio/reactivex/c/g;

    .line 166
    sget-object v1, Lco/uk/getmondo/main/c$h;->a:Lco/uk/getmondo/main/c$h;

    check-cast v1, Lio/reactivex/c/g;

    .line 146
    invoke-virtual {v5, v0, v1}, Lio/reactivex/n;->subscribe(Lio/reactivex/c/g;Lio/reactivex/c/g;)Lio/reactivex/b/b;

    move-result-object v0

    const-string v1, "sddMigrationStorage.sddM\u2026     }, { Timber.e(it) })"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 135
    invoke-static {v4, v0}, Lio/reactivex/rxkotlin/a;->a(Lio/reactivex/b/a;Lio/reactivex/b/b;)V

    .line 168
    iget-object v4, p0, Lco/uk/getmondo/main/c;->b:Lio/reactivex/b/a;

    .line 189
    invoke-interface {p1}, Lco/uk/getmondo/main/c$a;->D()Lio/reactivex/n;

    move-result-object v0

    .line 169
    iget-object v1, p0, Lco/uk/getmondo/main/c;->d:Lio/reactivex/u;

    invoke-virtual {v0, v1}, Lio/reactivex/n;->observeOn(Lio/reactivex/u;)Lio/reactivex/n;

    move-result-object v1

    .line 170
    new-instance v0, Lco/uk/getmondo/main/c$i;

    invoke-direct {v0, p1}, Lco/uk/getmondo/main/c$i;-><init>(Lco/uk/getmondo/main/c$a;)V

    check-cast v0, Lio/reactivex/c/g;

    invoke-virtual {v1, v0}, Lio/reactivex/n;->doOnNext(Lio/reactivex/c/g;)Lio/reactivex/n;

    move-result-object v1

    .line 171
    new-instance v0, Lco/uk/getmondo/main/c$j;

    invoke-direct {v0, p0, p1}, Lco/uk/getmondo/main/c$j;-><init>(Lco/uk/getmondo/main/c;Lco/uk/getmondo/main/c$a;)V

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {v1, v0}, Lio/reactivex/n;->switchMap(Lio/reactivex/c/h;)Lio/reactivex/n;

    move-result-object v0

    .line 188
    iget-object v1, p0, Lco/uk/getmondo/main/c;->d:Lio/reactivex/u;

    invoke-virtual {v0, v1}, Lio/reactivex/n;->observeOn(Lio/reactivex/u;)Lio/reactivex/n;

    move-result-object v5

    .line 189
    new-instance v0, Lco/uk/getmondo/main/c$k;

    invoke-direct {v0, p0, p1, v3}, Lco/uk/getmondo/main/c$k;-><init>(Lco/uk/getmondo/main/c;Lco/uk/getmondo/main/c$a;Lco/uk/getmondo/d/ac;)V

    check-cast v0, Lio/reactivex/c/g;

    .line 202
    sget-object v1, Lco/uk/getmondo/main/c$l;->a:Lco/uk/getmondo/main/c$l;

    check-cast v1, Lio/reactivex/c/g;

    .line 189
    invoke-virtual {v5, v0, v1}, Lio/reactivex/n;->subscribe(Lio/reactivex/c/g;Lio/reactivex/c/g;)Lio/reactivex/b/b;

    move-result-object v0

    const-string v1, "view.onPendingMonzoMeDat\u2026     }, { Timber.e(it) })"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 168
    invoke-static {v4, v0}, Lio/reactivex/rxkotlin/a;->a(Lio/reactivex/b/a;Lio/reactivex/b/b;)V

    .line 204
    iget-object v1, p0, Lco/uk/getmondo/main/c;->b:Lio/reactivex/b/a;

    .line 205
    invoke-interface {p1}, Lco/uk/getmondo/main/c$a;->j()Lio/reactivex/n;

    move-result-object v3

    new-instance v0, Lco/uk/getmondo/main/c$n;

    invoke-direct {v0, p0, p1}, Lco/uk/getmondo/main/c$n;-><init>(Lco/uk/getmondo/main/c;Lco/uk/getmondo/main/c$a;)V

    check-cast v0, Lio/reactivex/c/g;

    invoke-virtual {v3, v0}, Lio/reactivex/n;->subscribe(Lio/reactivex/c/g;)Lio/reactivex/b/b;

    move-result-object v0

    const-string v3, "view.onSettingsClicked()\u2026tings()\n                }"

    invoke-static {v0, v3}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 204
    invoke-static {v1, v0}, Lio/reactivex/rxkotlin/a;->a(Lio/reactivex/b/a;Lio/reactivex/b/b;)V

    .line 210
    iget-object v1, p0, Lco/uk/getmondo/main/c;->b:Lio/reactivex/b/a;

    .line 211
    invoke-interface {p1}, Lco/uk/getmondo/main/c$a;->k()Lio/reactivex/n;

    move-result-object v3

    new-instance v0, Lco/uk/getmondo/main/c$o;

    invoke-direct {v0, p0, p1}, Lco/uk/getmondo/main/c$o;-><init>(Lco/uk/getmondo/main/c;Lco/uk/getmondo/main/c$a;)V

    check-cast v0, Lio/reactivex/c/g;

    invoke-virtual {v3, v0}, Lio/reactivex/n;->subscribe(Lio/reactivex/c/g;)Lio/reactivex/b/b;

    move-result-object v0

    const-string v3, "view.onCommunityClicked(\u2026unity()\n                }"

    invoke-static {v0, v3}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 210
    invoke-static {v1, v0}, Lio/reactivex/rxkotlin/a;->a(Lio/reactivex/b/a;Lio/reactivex/b/b;)V

    .line 216
    iget-object v1, p0, Lco/uk/getmondo/main/c;->b:Lio/reactivex/b/a;

    .line 217
    invoke-interface {p1}, Lco/uk/getmondo/main/c$a;->w()Lio/reactivex/n;

    move-result-object v3

    new-instance v0, Lco/uk/getmondo/main/c$p;

    invoke-direct {v0, p0, p1}, Lco/uk/getmondo/main/c$p;-><init>(Lco/uk/getmondo/main/c;Lco/uk/getmondo/main/c$a;)V

    check-cast v0, Lio/reactivex/c/g;

    invoke-virtual {v3, v0}, Lio/reactivex/n;->subscribe(Lio/reactivex/c/g;)Lio/reactivex/b/b;

    move-result-object v0

    const-string v3, "view.onChatClicked()\n   \u2026nChat()\n                }"

    invoke-static {v0, v3}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 216
    invoke-static {v1, v0}, Lio/reactivex/rxkotlin/a;->a(Lio/reactivex/b/a;Lio/reactivex/b/b;)V

    .line 222
    iget-object v1, p0, Lco/uk/getmondo/main/c;->b:Lio/reactivex/b/a;

    .line 223
    invoke-interface {p1}, Lco/uk/getmondo/main/c$a;->v()Lio/reactivex/n;

    move-result-object v3

    new-instance v0, Lco/uk/getmondo/main/c$q;

    invoke-direct {v0, v2, p1}, Lco/uk/getmondo/main/c$q;-><init>(ZLco/uk/getmondo/main/c$a;)V

    check-cast v0, Lio/reactivex/c/g;

    invoke-virtual {v3, v0}, Lio/reactivex/n;->subscribe(Lio/reactivex/c/g;)Lio/reactivex/b/b;

    move-result-object v0

    const-string v3, "view.onHelpClicked()\n   \u2026      }\n                }"

    invoke-static {v0, v3}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 222
    invoke-static {v1, v0}, Lio/reactivex/rxkotlin/a;->a(Lio/reactivex/b/a;Lio/reactivex/b/b;)V

    .line 231
    iget-object v1, p0, Lco/uk/getmondo/main/c;->b:Lio/reactivex/b/a;

    .line 233
    invoke-interface {p1}, Lco/uk/getmondo/main/c$a;->a()Lio/reactivex/n;

    move-result-object v3

    .line 232
    new-instance v0, Lco/uk/getmondo/main/c$r;

    invoke-direct {v0, p0}, Lco/uk/getmondo/main/c$r;-><init>(Lco/uk/getmondo/main/c;)V

    check-cast v0, Lio/reactivex/c/g;

    invoke-virtual {v3, v0}, Lio/reactivex/n;->doOnNext(Lio/reactivex/c/g;)Lio/reactivex/n;

    move-result-object v3

    .line 233
    new-instance v0, Lco/uk/getmondo/main/c$s;

    invoke-direct {v0, p1}, Lco/uk/getmondo/main/c$s;-><init>(Lco/uk/getmondo/main/c$a;)V

    check-cast v0, Lio/reactivex/c/g;

    invoke-virtual {v3, v0}, Lio/reactivex/n;->subscribe(Lio/reactivex/c/g;)Lio/reactivex/b/b;

    move-result-object v0

    const-string v3, "view.onGhostLogOutClicke\u2026ibe { view.openSplash() }"

    invoke-static {v0, v3}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 231
    invoke-static {v1, v0}, Lio/reactivex/rxkotlin/a;->a(Lio/reactivex/b/a;Lio/reactivex/b/b;)V

    .line 235
    iget-object v3, p0, Lco/uk/getmondo/main/c;->b:Lio/reactivex/b/a;

    .line 237
    iget-object v0, p0, Lco/uk/getmondo/main/c;->o:Lco/uk/getmondo/background_sync/d;

    invoke-virtual {v0}, Lco/uk/getmondo/background_sync/d;->a()Lio/reactivex/b;

    move-result-object v0

    .line 236
    iget-object v1, p0, Lco/uk/getmondo/main/c;->e:Lio/reactivex/u;

    invoke-virtual {v0, v1}, Lio/reactivex/b;->b(Lio/reactivex/u;)Lio/reactivex/b;

    move-result-object v4

    .line 237
    sget-object v0, Lco/uk/getmondo/main/c$t;->a:Lco/uk/getmondo/main/c$t;

    check-cast v0, Lio/reactivex/c/a;

    .line 238
    new-instance v1, Lco/uk/getmondo/main/c$u;

    invoke-direct {v1, p0, p1}, Lco/uk/getmondo/main/c$u;-><init>(Lco/uk/getmondo/main/c;Lco/uk/getmondo/main/c$a;)V

    check-cast v1, Lio/reactivex/c/g;

    .line 237
    invoke-virtual {v4, v0, v1}, Lio/reactivex/b;->a(Lio/reactivex/c/a;Lio/reactivex/c/g;)Lio/reactivex/b/b;

    move-result-object v0

    const-string v1, "syncManager.syncFeedAndB\u2026Error(throwable, view) })"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 235
    invoke-static {v3, v0}, Lio/reactivex/rxkotlin/a;->a(Lio/reactivex/b/a;Lio/reactivex/b/b;)V

    .line 240
    iget-object v3, p0, Lco/uk/getmondo/main/c;->b:Lio/reactivex/b/a;

    .line 241
    iget-object v0, p0, Lco/uk/getmondo/main/c;->s:Lco/uk/getmondo/feed/a/t;

    invoke-virtual {v0}, Lco/uk/getmondo/feed/a/t;->a()Lio/reactivex/n;

    move-result-object v4

    sget-object v0, Lco/uk/getmondo/main/c$v;->a:Lco/uk/getmondo/main/c$v;

    check-cast v0, Lio/reactivex/c/g;

    .line 242
    sget-object v1, Lco/uk/getmondo/main/c$w;->a:Lco/uk/getmondo/main/c$w;

    check-cast v1, Lio/reactivex/c/g;

    .line 241
    invoke-virtual {v4, v0, v1}, Lio/reactivex/n;->subscribe(Lio/reactivex/c/g;Lio/reactivex/c/g;)Lio/reactivex/b/b;

    move-result-object v0

    const-string v1, "peerManager.keepPeersEnr\u2026iching peers\", error)) })"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 240
    invoke-static {v3, v0}, Lio/reactivex/rxkotlin/a;->a(Lio/reactivex/b/a;Lio/reactivex/b/b;)V

    .line 245
    invoke-interface {p1}, Lco/uk/getmondo/main/c$a;->i()Lio/reactivex/n;

    move-result-object v0

    invoke-virtual {v0}, Lio/reactivex/n;->replay()Lio/reactivex/e/a;

    move-result-object v0

    invoke-virtual {v0}, Lio/reactivex/e/a;->a()Lio/reactivex/n;

    move-result-object v1

    .line 247
    iget-object v3, p0, Lco/uk/getmondo/main/c;->b:Lio/reactivex/b/a;

    .line 248
    new-instance v0, Lco/uk/getmondo/main/c$y;

    invoke-direct {v0, p0, v2, p1}, Lco/uk/getmondo/main/c$y;-><init>(Lco/uk/getmondo/main/c;ZLco/uk/getmondo/main/c$a;)V

    check-cast v0, Lio/reactivex/c/g;

    invoke-virtual {v1, v0}, Lio/reactivex/n;->subscribe(Lio/reactivex/c/g;)Lio/reactivex/b/b;

    move-result-object v0

    const-string v2, "onScreenChanged\n        \u2026      }\n                }"

    invoke-static {v0, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 247
    invoke-static {v3, v0}, Lio/reactivex/rxkotlin/a;->a(Lio/reactivex/b/a;Lio/reactivex/b/b;)V

    .line 268
    sget-object v0, Lco/uk/getmondo/a;->c:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_6

    iget-object v0, p0, Lco/uk/getmondo/main/c;->v:Lco/uk/getmondo/signup/status/b;

    invoke-virtual {v0}, Lco/uk/getmondo/signup/status/b;->a()Z

    move-result v0

    if-nez v0, :cond_6

    .line 270
    iget-object v2, p0, Lco/uk/getmondo/main/c;->b:Lio/reactivex/b/a;

    .line 277
    invoke-interface {p1}, Lco/uk/getmondo/main/c$a;->J()Lio/reactivex/n;

    move-result-object v3

    .line 271
    new-instance v0, Lco/uk/getmondo/main/c$z;

    invoke-direct {v0, p0, v1}, Lco/uk/getmondo/main/c$z;-><init>(Lco/uk/getmondo/main/c;Lio/reactivex/n;)V

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {v3, v0}, Lio/reactivex/n;->switchMap(Lio/reactivex/c/h;)Lio/reactivex/n;

    move-result-object v0

    .line 276
    iget-object v1, p0, Lco/uk/getmondo/main/c;->d:Lio/reactivex/u;

    invoke-virtual {v0, v1}, Lio/reactivex/n;->observeOn(Lio/reactivex/u;)Lio/reactivex/n;

    move-result-object v3

    .line 277
    new-instance v0, Lco/uk/getmondo/main/c$aa;

    invoke-direct {v0, p0, p1}, Lco/uk/getmondo/main/c$aa;-><init>(Lco/uk/getmondo/main/c;Lco/uk/getmondo/main/c$a;)V

    check-cast v0, Lio/reactivex/c/g;

    .line 284
    new-instance v1, Lco/uk/getmondo/main/c$ab;

    invoke-direct {v1, p0, p1}, Lco/uk/getmondo/main/c$ab;-><init>(Lco/uk/getmondo/main/c;Lco/uk/getmondo/main/c$a;)V

    check-cast v1, Lio/reactivex/c/g;

    .line 277
    invoke-virtual {v3, v0, v1}, Lio/reactivex/n;->subscribe(Lio/reactivex/c/g;Lio/reactivex/c/g;)Lio/reactivex/b/b;

    move-result-object v0

    const-string v1, "view.onRefreshMigrationB\u2026.handleError(it, view) })"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 270
    invoke-static {v2, v0}, Lio/reactivex/rxkotlin/a;->a(Lio/reactivex/b/a;Lio/reactivex/b/b;)V

    .line 286
    iget-object v1, p0, Lco/uk/getmondo/main/c;->b:Lio/reactivex/b/a;

    .line 287
    invoke-interface {p1}, Lco/uk/getmondo/main/c$a;->K()Lio/reactivex/n;

    move-result-object v2

    new-instance v0, Lco/uk/getmondo/main/c$ac;

    invoke-direct {v0, p0, p1}, Lco/uk/getmondo/main/c$ac;-><init>(Lco/uk/getmondo/main/c;Lco/uk/getmondo/main/c$a;)V

    check-cast v0, Lio/reactivex/c/g;

    invoke-virtual {v2, v0}, Lio/reactivex/n;->subscribe(Lio/reactivex/c/g;)Lio/reactivex/b/b;

    move-result-object v0

    const-string v2, "view.onMigrationBannerCl\u2026  }\n                    }"

    invoke-static {v0, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 286
    invoke-static {v1, v0}, Lio/reactivex/rxkotlin/a;->a(Lio/reactivex/b/a;Lio/reactivex/b/b;)V

    .line 302
    :cond_6
    return-void

    .line 79
    :cond_7
    const/4 v1, 0x0

    move v2, v1

    goto/16 :goto_0
.end method
