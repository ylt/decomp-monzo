.class public Lco/uk/getmondo/main/EddLimitsActivity;
.super Lco/uk/getmondo/common/activities/b;
.source "EddLimitsActivity.java"


# instance fields
.field a:Lco/uk/getmondo/common/a;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Lco/uk/getmondo/common/activities/b;-><init>()V

    return-void
.end method

.method public static a(Landroid/content/Context;)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 32
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lco/uk/getmondo/main/EddLimitsActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    return-object v0
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 22
    invoke-super {p0, p1}, Lco/uk/getmondo/common/activities/b;->onCreate(Landroid/os/Bundle;)V

    .line 24
    const v0, 0x7f050033

    invoke-virtual {p0, v0}, Lco/uk/getmondo/main/EddLimitsActivity;->setContentView(I)V

    .line 25
    invoke-static {p0}, Lbutterknife/ButterKnife;->bind(Landroid/app/Activity;)Lbutterknife/Unbinder;

    .line 26
    invoke-virtual {p0}, Lco/uk/getmondo/main/EddLimitsActivity;->l()Lco/uk/getmondo/common/h/b/b;

    move-result-object v0

    invoke-interface {v0, p0}, Lco/uk/getmondo/common/h/b/b;->a(Lco/uk/getmondo/main/EddLimitsActivity;)V

    .line 28
    iget-object v0, p0, Lco/uk/getmondo/main/EddLimitsActivity;->a:Lco/uk/getmondo/common/a;

    invoke-static {}, Lco/uk/getmondo/api/model/tracking/Impression;->V()Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v1

    invoke-virtual {v0, v1}, Lco/uk/getmondo/common/a;->a(Lco/uk/getmondo/api/model/tracking/Impression;)V

    .line 29
    return-void
.end method

.method public onNotNowClicked()V
    .locals 0
    .annotation build Lbutterknife/OnClick;
        value = {
            0x7f110159
        }
    .end annotation

    .prologue
    .line 42
    invoke-virtual {p0}, Lco/uk/getmondo/main/EddLimitsActivity;->finish()V

    .line 43
    return-void
.end method

.method public onViewLimitsClicked()V
    .locals 0
    .annotation build Lbutterknife/OnClick;
        value = {
            0x7f110158
        }
    .end annotation

    .prologue
    .line 37
    invoke-static {p0}, Lco/uk/getmondo/settings/LimitsActivity;->a(Landroid/content/Context;)V

    .line 38
    return-void
.end method
