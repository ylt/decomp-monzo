.class final Lco/uk/getmondo/main/c$g;
.super Ljava/lang/Object;
.source "HomePresenter.kt"

# interfaces
.implements Lio/reactivex/c/g;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lco/uk/getmondo/main/c;->a(Lco/uk/getmondo/main/c$a;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/g",
        "<",
        "Lkotlin/h",
        "<+",
        "Lco/uk/getmondo/d/ag;",
        "+",
        "Lco/uk/getmondo/api/model/identity_verification/IdentityVerification;",
        ">;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000\u0018\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012&\u0010\u0002\u001a\"\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u0005 \u0006*\u0010\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u0005\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\u0007"
    }
    d2 = {
        "<anonymous>",
        "",
        "<name for destructuring parameter 0>",
        "Lkotlin/Pair;",
        "Lco/uk/getmondo/model/SddMigration;",
        "Lco/uk/getmondo/api/model/identity_verification/IdentityVerification;",
        "kotlin.jvm.PlatformType",
        "accept"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field final synthetic a:Lco/uk/getmondo/main/c;

.field final synthetic b:Lco/uk/getmondo/main/c$a;


# direct methods
.method constructor <init>(Lco/uk/getmondo/main/c;Lco/uk/getmondo/main/c$a;)V
    .locals 0

    iput-object p1, p0, Lco/uk/getmondo/main/c$g;->a:Lco/uk/getmondo/main/c;

    iput-object p2, p0, Lco/uk/getmondo/main/c$g;->b:Lco/uk/getmondo/main/c$a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 45
    check-cast p1, Lkotlin/h;

    invoke-virtual {p0, p1}, Lco/uk/getmondo/main/c$g;->a(Lkotlin/h;)V

    return-void
.end method

.method public final a(Lkotlin/h;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/h",
            "<+",
            "Lco/uk/getmondo/d/ag;",
            "Lco/uk/getmondo/api/model/identity_verification/IdentityVerification;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    invoke-virtual {p1}, Lkotlin/h;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/d/ag;

    invoke-virtual {p1}, Lkotlin/h;->d()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lco/uk/getmondo/api/model/identity_verification/IdentityVerification;

    .line 147
    invoke-virtual {v1}, Lco/uk/getmondo/api/model/identity_verification/IdentityVerification;->d()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1}, Lco/uk/getmondo/api/model/identity_verification/IdentityVerification;->e()Lco/uk/getmondo/api/model/identity_verification/IdentityVerification$Status;

    move-result-object v4

    .line 148
    sget-object v1, Lco/uk/getmondo/api/model/identity_verification/IdentityVerification$Status;->PENDING_SUBMISSION:Lco/uk/getmondo/api/model/identity_verification/IdentityVerification$Status;

    if-eq v4, v1, :cond_0

    sget-object v1, Lco/uk/getmondo/api/model/identity_verification/IdentityVerification$Status;->NOT_STARTED:Lco/uk/getmondo/api/model/identity_verification/IdentityVerification$Status;

    if-ne v4, v1, :cond_2

    :cond_0
    move v1, v2

    .line 150
    :goto_0
    invoke-virtual {v0}, Lco/uk/getmondo/d/ag;->a()Lco/uk/getmondo/d/ah;

    move-result-object v0

    if-nez v0, :cond_3

    .line 163
    :cond_1
    :goto_1
    return-void

    .line 148
    :cond_2
    const/4 v1, 0x0

    goto :goto_0

    .line 150
    :cond_3
    sget-object v5, Lco/uk/getmondo/main/d;->a:[I

    invoke-virtual {v0}, Lco/uk/getmondo/d/ah;->ordinal()I

    move-result v0

    aget v0, v5, v0

    packed-switch v0, :pswitch_data_0

    goto :goto_1

    .line 152
    :pswitch_0
    iget-object v0, p0, Lco/uk/getmondo/main/c$g;->a:Lco/uk/getmondo/main/c;

    invoke-static {v0}, Lco/uk/getmondo/main/c;->g(Lco/uk/getmondo/main/c;)Lco/uk/getmondo/main/h;

    move-result-object v0

    invoke-virtual {v0}, Lco/uk/getmondo/main/h;->a()Z

    move-result v0

    if-nez v0, :cond_1

    if-eqz v1, :cond_1

    .line 153
    iget-object v0, p0, Lco/uk/getmondo/main/c$g;->b:Lco/uk/getmondo/main/c$a;

    invoke-interface {v0, v3}, Lco/uk/getmondo/main/c$a;->d(Ljava/lang/String;)V

    .line 154
    iget-object v0, p0, Lco/uk/getmondo/main/c$g;->a:Lco/uk/getmondo/main/c;

    invoke-static {v0}, Lco/uk/getmondo/main/c;->g(Lco/uk/getmondo/main/c;)Lco/uk/getmondo/main/h;

    move-result-object v0

    invoke-virtual {v0, v2}, Lco/uk/getmondo/main/h;->a(Z)V

    goto :goto_1

    .line 158
    :pswitch_1
    if-eqz v1, :cond_4

    .line 159
    iget-object v0, p0, Lco/uk/getmondo/main/c$g;->b:Lco/uk/getmondo/main/c$a;

    invoke-interface {v0, v3}, Lco/uk/getmondo/main/c$a;->e(Ljava/lang/String;)V

    goto :goto_1

    .line 160
    :cond_4
    sget-object v0, Lco/uk/getmondo/api/model/identity_verification/IdentityVerification$Status;->PENDING_REVIEW:Lco/uk/getmondo/api/model/identity_verification/IdentityVerification$Status;

    if-eq v4, v0, :cond_5

    sget-object v0, Lco/uk/getmondo/api/model/identity_verification/IdentityVerification$Status;->BLOCKED:Lco/uk/getmondo/api/model/identity_verification/IdentityVerification$Status;

    if-ne v4, v0, :cond_1

    .line 162
    :cond_5
    iget-object v0, p0, Lco/uk/getmondo/main/c$g;->b:Lco/uk/getmondo/main/c$a;

    invoke-interface {v0}, Lco/uk/getmondo/main/c$a;->H()V

    goto :goto_1

    .line 150
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
