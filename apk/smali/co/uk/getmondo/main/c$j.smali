.class final Lco/uk/getmondo/main/c$j;
.super Ljava/lang/Object;
.source "HomePresenter.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lco/uk/getmondo/main/c;->a(Lco/uk/getmondo/main/c$a;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h",
        "<TT;",
        "Lio/reactivex/r",
        "<+TR;>;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000\u0016\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0010\u0000\u001aV\u0012$\u0012\"\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u0004 \u0005*\u0010\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u0004\u0018\u00010\u00020\u0002 \u0005**\u0012$\u0012\"\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u0004 \u0005*\u0010\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u0004\u0018\u00010\u00020\u0002\u0018\u00010\u00010\u00012\u0006\u0010\u0006\u001a\u00020\u0004H\n\u00a2\u0006\u0002\u0008\u0007"
    }
    d2 = {
        "<anonymous>",
        "Lio/reactivex/Observable;",
        "Lkotlin/Pair;",
        "Lco/uk/getmondo/model/Peer;",
        "Lco/uk/getmondo/main/MonzoMeData;",
        "kotlin.jvm.PlatformType",
        "monzoMeData",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field final synthetic a:Lco/uk/getmondo/main/c;

.field final synthetic b:Lco/uk/getmondo/main/c$a;


# direct methods
.method constructor <init>(Lco/uk/getmondo/main/c;Lco/uk/getmondo/main/c$a;)V
    .locals 0

    iput-object p1, p0, Lco/uk/getmondo/main/c$j;->a:Lco/uk/getmondo/main/c;

    iput-object p2, p0, Lco/uk/getmondo/main/c$j;->b:Lco/uk/getmondo/main/c$a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lco/uk/getmondo/main/f;)Lio/reactivex/n;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lco/uk/getmondo/main/f;",
            ")",
            "Lio/reactivex/n",
            "<",
            "Lkotlin/h",
            "<",
            "Lco/uk/getmondo/d/aa;",
            "Lco/uk/getmondo/main/f;",
            ">;>;"
        }
    .end annotation

    .prologue
    const-string v0, "monzoMeData"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 172
    iget-object v0, p0, Lco/uk/getmondo/main/c$j;->a:Lco/uk/getmondo/main/c;

    invoke-static {v0}, Lco/uk/getmondo/main/c;->h(Lco/uk/getmondo/main/c;)Lco/uk/getmondo/payments/send/data/PeerToPeerRepository;

    move-result-object v0

    invoke-virtual {p1}, Lco/uk/getmondo/main/f;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lco/uk/getmondo/payments/send/data/PeerToPeerRepository;->a(Ljava/lang/String;)Lio/reactivex/v;

    move-result-object v1

    .line 173
    new-instance v0, Lco/uk/getmondo/main/c$j$1;

    invoke-direct {v0, p1}, Lco/uk/getmondo/main/c$j$1;-><init>(Lco/uk/getmondo/main/f;)V

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {v1, v0}, Lio/reactivex/v;->d(Lio/reactivex/c/h;)Lio/reactivex/v;

    move-result-object v0

    .line 174
    iget-object v1, p0, Lco/uk/getmondo/main/c$j;->a:Lco/uk/getmondo/main/c;

    invoke-static {v1}, Lco/uk/getmondo/main/c;->c(Lco/uk/getmondo/main/c;)Lio/reactivex/u;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/v;->b(Lio/reactivex/u;)Lio/reactivex/v;

    move-result-object v0

    .line 175
    iget-object v1, p0, Lco/uk/getmondo/main/c$j;->a:Lco/uk/getmondo/main/c;

    invoke-static {v1}, Lco/uk/getmondo/main/c;->e(Lco/uk/getmondo/main/c;)Lio/reactivex/u;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/v;->a(Lio/reactivex/u;)Lio/reactivex/v;

    move-result-object v1

    .line 176
    new-instance v0, Lco/uk/getmondo/main/c$j$2;

    invoke-direct {v0, p0}, Lco/uk/getmondo/main/c$j$2;-><init>(Lco/uk/getmondo/main/c$j;)V

    check-cast v0, Lio/reactivex/c/g;

    invoke-virtual {v1, v0}, Lio/reactivex/v;->d(Lio/reactivex/c/g;)Lio/reactivex/v;

    move-result-object v0

    .line 185
    invoke-virtual {v0}, Lio/reactivex/v;->f()Lio/reactivex/n;

    move-result-object v1

    .line 186
    invoke-static {}, Lio/reactivex/n;->empty()Lio/reactivex/n;

    move-result-object v0

    check-cast v0, Lio/reactivex/r;

    invoke-virtual {v1, v0}, Lio/reactivex/n;->onErrorResumeNext(Lio/reactivex/r;)Lio/reactivex/n;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 45
    check-cast p1, Lco/uk/getmondo/main/f;

    invoke-virtual {p0, p1}, Lco/uk/getmondo/main/c$j;->a(Lco/uk/getmondo/main/f;)Lio/reactivex/n;

    move-result-object v0

    return-object v0
.end method
