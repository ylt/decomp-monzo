.class public final enum Lco/uk/getmondo/main/g;
.super Ljava/lang/Enum;
.source "Screen.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lco/uk/getmondo/main/g;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lco/uk/getmondo/main/g;

.field public static final enum b:Lco/uk/getmondo/main/g;

.field public static final enum c:Lco/uk/getmondo/main/g;

.field public static final enum d:Lco/uk/getmondo/main/g;

.field private static final synthetic h:[Lco/uk/getmondo/main/g;


# instance fields
.field final e:I

.field f:I

.field g:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<+",
            "Landroid/support/v4/app/Fragment;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 15

    .prologue
    const/4 v11, 0x4

    const/4 v2, 0x0

    const/4 v14, 0x3

    const/4 v7, 0x2

    const/4 v3, 0x1

    .line 18
    new-instance v0, Lco/uk/getmondo/main/g;

    const-string v1, "FEED"

    const v4, 0x7f1104cc

    const-class v5, Lco/uk/getmondo/feed/e;

    invoke-direct/range {v0 .. v5}, Lco/uk/getmondo/main/g;-><init>(Ljava/lang/String;IIILjava/lang/Class;)V

    sput-object v0, Lco/uk/getmondo/main/g;->a:Lco/uk/getmondo/main/g;

    .line 19
    new-instance v4, Lco/uk/getmondo/main/g;

    const-string v5, "SPENDING"

    const v8, 0x7f1104cd

    const-class v9, Lco/uk/getmondo/spending/b;

    move v6, v3

    invoke-direct/range {v4 .. v9}, Lco/uk/getmondo/main/g;-><init>(Ljava/lang/String;IIILjava/lang/Class;)V

    sput-object v4, Lco/uk/getmondo/main/g;->b:Lco/uk/getmondo/main/g;

    .line 20
    new-instance v5, Lco/uk/getmondo/main/g;

    const-string v6, "CARD"

    const v9, 0x7f1104ce

    const-class v10, Lco/uk/getmondo/card/a;

    move v8, v14

    invoke-direct/range {v5 .. v10}, Lco/uk/getmondo/main/g;-><init>(Ljava/lang/String;IIILjava/lang/Class;)V

    sput-object v5, Lco/uk/getmondo/main/g;->c:Lco/uk/getmondo/main/g;

    .line 21
    new-instance v8, Lco/uk/getmondo/main/g;

    const-string v9, "CONTACTS"

    const v12, 0x7f1104cf

    const-class v13, Lco/uk/getmondo/c/a;

    move v10, v14

    invoke-direct/range {v8 .. v13}, Lco/uk/getmondo/main/g;-><init>(Ljava/lang/String;IIILjava/lang/Class;)V

    sput-object v8, Lco/uk/getmondo/main/g;->d:Lco/uk/getmondo/main/g;

    .line 16
    new-array v0, v11, [Lco/uk/getmondo/main/g;

    sget-object v1, Lco/uk/getmondo/main/g;->a:Lco/uk/getmondo/main/g;

    aput-object v1, v0, v2

    sget-object v1, Lco/uk/getmondo/main/g;->b:Lco/uk/getmondo/main/g;

    aput-object v1, v0, v3

    sget-object v1, Lco/uk/getmondo/main/g;->c:Lco/uk/getmondo/main/g;

    aput-object v1, v0, v7

    sget-object v1, Lco/uk/getmondo/main/g;->d:Lco/uk/getmondo/main/g;

    aput-object v1, v0, v14

    sput-object v0, Lco/uk/getmondo/main/g;->h:[Lco/uk/getmondo/main/g;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IIILjava/lang/Class;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II",
            "Ljava/lang/Class",
            "<+",
            "Landroid/support/v4/app/Fragment;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 27
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 28
    iput p3, p0, Lco/uk/getmondo/main/g;->e:I

    .line 29
    iput p4, p0, Lco/uk/getmondo/main/g;->f:I

    .line 30
    iput-object p5, p0, Lco/uk/getmondo/main/g;->g:Ljava/lang/Class;

    .line 31
    return-void
.end method

.method static a(I)Lco/uk/getmondo/main/g;
    .locals 5

    .prologue
    .line 35
    invoke-static {}, Lco/uk/getmondo/main/g;->values()[Lco/uk/getmondo/main/g;

    move-result-object v2

    array-length v3, v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v0, v2, v1

    .line 36
    iget v4, v0, Lco/uk/getmondo/main/g;->e:I

    if-ne v4, p0, :cond_0

    .line 40
    :goto_1
    return-object v0

    .line 35
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 40
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public static b(I)Lco/uk/getmondo/main/g;
    .locals 5

    .prologue
    .line 45
    invoke-static {}, Lco/uk/getmondo/main/g;->values()[Lco/uk/getmondo/main/g;

    move-result-object v2

    array-length v3, v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v0, v2, v1

    .line 46
    iget v4, v0, Lco/uk/getmondo/main/g;->f:I

    if-ne v4, p0, :cond_0

    .line 50
    :goto_1
    return-object v0

    .line 45
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 50
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public static valueOf(Ljava/lang/String;)Lco/uk/getmondo/main/g;
    .locals 1

    .prologue
    .line 16
    const-class v0, Lco/uk/getmondo/main/g;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/main/g;

    return-object v0
.end method

.method public static values()[Lco/uk/getmondo/main/g;
    .locals 1

    .prologue
    .line 16
    sget-object v0, Lco/uk/getmondo/main/g;->h:[Lco/uk/getmondo/main/g;

    invoke-virtual {v0}, [Lco/uk/getmondo/main/g;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lco/uk/getmondo/main/g;

    return-object v0
.end method


# virtual methods
.method a()Landroid/support/v4/app/Fragment;
    .locals 2

    .prologue
    .line 66
    :try_start_0
    iget-object v0, p0, Lco/uk/getmondo/main/g;->g:Ljava/lang/Class;

    invoke-virtual {v0}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/Fragment;
    :try_end_0
    .catch Ljava/lang/InstantiationException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1

    return-object v0

    .line 67
    :catch_0
    move-exception v0

    .line 68
    :goto_0
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 67
    :catch_1
    move-exception v0

    goto :goto_0
.end method
