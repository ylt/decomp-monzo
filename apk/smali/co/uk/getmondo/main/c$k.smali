.class final Lco/uk/getmondo/main/c$k;
.super Ljava/lang/Object;
.source "HomePresenter.kt"

# interfaces
.implements Lio/reactivex/c/g;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lco/uk/getmondo/main/c;->a(Lco/uk/getmondo/main/c$a;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/g",
        "<",
        "Lkotlin/h",
        "<+",
        "Lco/uk/getmondo/d/aa;",
        "+",
        "Lco/uk/getmondo/main/f;",
        ">;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000\u0018\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012&\u0010\u0002\u001a\"\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u0005 \u0006*\u0010\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u0005\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\u0007"
    }
    d2 = {
        "<anonymous>",
        "",
        "peerAndMonzoMeData",
        "Lkotlin/Pair;",
        "Lco/uk/getmondo/model/Peer;",
        "Lco/uk/getmondo/main/MonzoMeData;",
        "kotlin.jvm.PlatformType",
        "accept"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field final synthetic a:Lco/uk/getmondo/main/c;

.field final synthetic b:Lco/uk/getmondo/main/c$a;

.field final synthetic c:Lco/uk/getmondo/d/ac;


# direct methods
.method constructor <init>(Lco/uk/getmondo/main/c;Lco/uk/getmondo/main/c$a;Lco/uk/getmondo/d/ac;)V
    .locals 0

    iput-object p1, p0, Lco/uk/getmondo/main/c$k;->a:Lco/uk/getmondo/main/c;

    iput-object p2, p0, Lco/uk/getmondo/main/c$k;->b:Lco/uk/getmondo/main/c$a;

    iput-object p3, p0, Lco/uk/getmondo/main/c$k;->c:Lco/uk/getmondo/d/ac;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 45
    check-cast p1, Lkotlin/h;

    invoke-virtual {p0, p1}, Lco/uk/getmondo/main/c$k;->a(Lkotlin/h;)V

    return-void
.end method

.method public final a(Lkotlin/h;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/h",
            "<+",
            "Lco/uk/getmondo/d/aa;",
            "Lco/uk/getmondo/main/f;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 190
    iget-object v0, p0, Lco/uk/getmondo/main/c$k;->b:Lco/uk/getmondo/main/c$a;

    invoke-interface {v0}, Lco/uk/getmondo/main/c$a;->G()V

    .line 191
    invoke-virtual {p1}, Lkotlin/h;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/d/aa;

    .line 192
    invoke-virtual {p1}, Lkotlin/h;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lco/uk/getmondo/main/f;

    .line 193
    iget-object v2, p0, Lco/uk/getmondo/main/c$k;->c:Lco/uk/getmondo/d/ac;

    invoke-virtual {v2}, Lco/uk/getmondo/d/ac;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Lco/uk/getmondo/d/aa;->a()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 194
    iget-object v0, p0, Lco/uk/getmondo/main/c$k;->b:Lco/uk/getmondo/main/c$a;

    invoke-interface {v0}, Lco/uk/getmondo/main/c$a;->y()V

    .line 200
    :cond_0
    :goto_0
    return-void

    .line 196
    :cond_1
    iget-object v2, p0, Lco/uk/getmondo/main/c$k;->a:Lco/uk/getmondo/main/c;

    invoke-static {v2}, Lco/uk/getmondo/main/c;->i(Lco/uk/getmondo/main/c;)Lco/uk/getmondo/payments/send/data/p;

    move-result-object v2

    invoke-virtual {v2}, Lco/uk/getmondo/payments/send/data/p;->a()Lco/uk/getmondo/payments/send/data/a/d;

    move-result-object v2

    sget-object v3, Lco/uk/getmondo/payments/send/data/a/d;->b:Lco/uk/getmondo/payments/send/data/a/d;

    invoke-static {v2, v3}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 197
    iget-object v2, p0, Lco/uk/getmondo/main/c$k;->b:Lco/uk/getmondo/main/c$a;

    const-string v3, "peer"

    invoke-static {v0, v3}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Lco/uk/getmondo/main/f;->a(Lco/uk/getmondo/d/aa;)Lco/uk/getmondo/payments/send/data/a/g;

    move-result-object v0

    invoke-interface {v2, v0}, Lco/uk/getmondo/main/c$a;->b(Lco/uk/getmondo/payments/send/data/a/g;)V

    goto :goto_0

    .line 198
    :cond_2
    iget-object v2, p0, Lco/uk/getmondo/main/c$k;->a:Lco/uk/getmondo/main/c;

    invoke-static {v2}, Lco/uk/getmondo/main/c;->i(Lco/uk/getmondo/main/c;)Lco/uk/getmondo/payments/send/data/p;

    move-result-object v2

    invoke-virtual {v2}, Lco/uk/getmondo/payments/send/data/p;->a()Lco/uk/getmondo/payments/send/data/a/d;

    move-result-object v2

    sget-object v3, Lco/uk/getmondo/payments/send/data/a/d;->a:Lco/uk/getmondo/payments/send/data/a/d;

    invoke-static {v2, v3}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 199
    iget-object v2, p0, Lco/uk/getmondo/main/c$k;->b:Lco/uk/getmondo/main/c$a;

    const-string v3, "peer"

    invoke-static {v0, v3}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Lco/uk/getmondo/main/f;->a(Lco/uk/getmondo/d/aa;)Lco/uk/getmondo/payments/send/data/a/g;

    move-result-object v0

    invoke-interface {v2, v0}, Lco/uk/getmondo/main/c$a;->a(Lco/uk/getmondo/payments/send/data/a/g;)V

    goto :goto_0
.end method
