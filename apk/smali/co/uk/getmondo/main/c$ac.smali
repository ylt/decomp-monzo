.class final Lco/uk/getmondo/main/c$ac;
.super Ljava/lang/Object;
.source "HomePresenter.kt"

# interfaces
.implements Lio/reactivex/c/g;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lco/uk/getmondo/main/c;->a(Lco/uk/getmondo/main/c$a;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/g",
        "<",
        "Lkotlin/n;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000\n\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0004\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0003*\u0004\u0018\u00010\u00010\u0001H\n\u00a2\u0006\u0004\u0008\u0004\u0010\u0005"
    }
    d2 = {
        "<anonymous>",
        "",
        "it",
        "kotlin.jvm.PlatformType",
        "accept",
        "(Lkotlin/Unit;)V"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field final synthetic a:Lco/uk/getmondo/main/c;

.field final synthetic b:Lco/uk/getmondo/main/c$a;


# direct methods
.method constructor <init>(Lco/uk/getmondo/main/c;Lco/uk/getmondo/main/c$a;)V
    .locals 0

    iput-object p1, p0, Lco/uk/getmondo/main/c$ac;->a:Lco/uk/getmondo/main/c;

    iput-object p2, p0, Lco/uk/getmondo/main/c$ac;->b:Lco/uk/getmondo/main/c$a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 45
    check-cast p1, Lkotlin/n;

    invoke-virtual {p0, p1}, Lco/uk/getmondo/main/c$ac;->a(Lkotlin/n;)V

    return-void
.end method

.method public final a(Lkotlin/n;)V
    .locals 3

    .prologue
    .line 288
    iget-object v0, p0, Lco/uk/getmondo/main/c$ac;->a:Lco/uk/getmondo/main/c;

    invoke-static {v0}, Lco/uk/getmondo/main/c;->n(Lco/uk/getmondo/main/c;)Lco/uk/getmondo/api/model/sign_up/MigrationInfo;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 289
    invoke-virtual {v0}, Lco/uk/getmondo/api/model/sign_up/MigrationInfo;->b()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {v0}, Lco/uk/getmondo/api/model/sign_up/MigrationInfo;->e()Lco/uk/getmondo/api/model/signup/SignupInfo$Status;

    move-result-object v1

    sget-object v2, Lco/uk/getmondo/api/model/signup/SignupInfo$Status;->NOT_STARTED:Lco/uk/getmondo/api/model/signup/SignupInfo$Status;

    invoke-static {v1, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    xor-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_1

    .line 291
    iget-object v0, p0, Lco/uk/getmondo/main/c$ac;->b:Lco/uk/getmondo/main/c$a;

    invoke-interface {v0}, Lco/uk/getmondo/main/c$a;->Q()V

    .line 292
    iget-object v0, p0, Lco/uk/getmondo/main/c$ac;->a:Lco/uk/getmondo/main/c;

    invoke-static {v0}, Lco/uk/getmondo/main/c;->j(Lco/uk/getmondo/main/c;)Lco/uk/getmondo/common/a;

    move-result-object v0

    sget-object v1, Lco/uk/getmondo/api/model/tracking/Impression;->Companion:Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    invoke-virtual {v1}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->bh()Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v1

    invoke-virtual {v0, v1}, Lco/uk/getmondo/common/a;->a(Lco/uk/getmondo/api/model/tracking/Impression;)V

    .line 298
    :goto_0
    nop

    .line 300
    :cond_0
    return-void

    .line 294
    :cond_1
    invoke-virtual {v0}, Lco/uk/getmondo/api/model/sign_up/MigrationInfo;->b()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lco/uk/getmondo/main/c$ac;->b:Lco/uk/getmondo/main/c$a;

    invoke-interface {v0}, Lco/uk/getmondo/main/c$a;->P()V

    goto :goto_0

    .line 296
    :cond_2
    iget-object v0, p0, Lco/uk/getmondo/main/c$ac;->b:Lco/uk/getmondo/main/c$a;

    invoke-interface {v0}, Lco/uk/getmondo/main/c$a;->O()V

    goto :goto_0
.end method
