.class final Lco/uk/getmondo/main/c$aa;
.super Ljava/lang/Object;
.source "HomePresenter.kt"

# interfaces
.implements Lio/reactivex/c/g;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lco/uk/getmondo/main/c;->a(Lco/uk/getmondo/main/c$a;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/g",
        "<",
        "Lkotlin/h",
        "<+",
        "Lco/uk/getmondo/api/model/sign_up/MigrationInfo;",
        "+",
        "Lco/uk/getmondo/main/g;",
        ">;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000\u0018\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012F\u0010\u0002\u001aB\u0012\u000c\u0012\n \u0005*\u0004\u0018\u00010\u00040\u0004\u0012\u000c\u0012\n \u0005*\u0004\u0018\u00010\u00060\u0006 \u0005* \u0012\u000c\u0012\n \u0005*\u0004\u0018\u00010\u00040\u0004\u0012\u000c\u0012\n \u0005*\u0004\u0018\u00010\u00060\u0006\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\u0007"
    }
    d2 = {
        "<anonymous>",
        "",
        "<name for destructuring parameter 0>",
        "Lkotlin/Pair;",
        "Lco/uk/getmondo/api/model/sign_up/MigrationInfo;",
        "kotlin.jvm.PlatformType",
        "Lco/uk/getmondo/main/Screen;",
        "accept"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field final synthetic a:Lco/uk/getmondo/main/c;

.field final synthetic b:Lco/uk/getmondo/main/c$a;


# direct methods
.method constructor <init>(Lco/uk/getmondo/main/c;Lco/uk/getmondo/main/c$a;)V
    .locals 0

    iput-object p1, p0, Lco/uk/getmondo/main/c$aa;->a:Lco/uk/getmondo/main/c;

    iput-object p2, p0, Lco/uk/getmondo/main/c$aa;->b:Lco/uk/getmondo/main/c$a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 45
    check-cast p1, Lkotlin/h;

    invoke-virtual {p0, p1}, Lco/uk/getmondo/main/c$aa;->a(Lkotlin/h;)V

    return-void
.end method

.method public final a(Lkotlin/h;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/h",
            "<",
            "Lco/uk/getmondo/api/model/sign_up/MigrationInfo;",
            "+",
            "Lco/uk/getmondo/main/g;",
            ">;)V"
        }
    .end annotation

    .prologue
    invoke-virtual {p1}, Lkotlin/h;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/api/model/sign_up/MigrationInfo;

    invoke-virtual {p1}, Lkotlin/h;->d()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lco/uk/getmondo/main/g;

    .line 278
    iget-object v2, p0, Lco/uk/getmondo/main/c$aa;->a:Lco/uk/getmondo/main/c;

    invoke-static {v2, v0}, Lco/uk/getmondo/main/c;->a(Lco/uk/getmondo/main/c;Lco/uk/getmondo/api/model/sign_up/MigrationInfo;)V

    .line 279
    invoke-virtual {v0}, Lco/uk/getmondo/api/model/sign_up/MigrationInfo;->a()Z

    move-result v2

    if-eqz v2, :cond_0

    sget-object v2, Lco/uk/getmondo/main/g;->a:Lco/uk/getmondo/main/g;

    invoke-static {v1, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 280
    iget-object v1, p0, Lco/uk/getmondo/main/c$aa;->a:Lco/uk/getmondo/main/c;

    const-string v2, "migrationInfo"

    invoke-static {v0, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v1, v0}, Lco/uk/getmondo/main/c;->b(Lco/uk/getmondo/main/c;Lco/uk/getmondo/api/model/sign_up/MigrationInfo;)V

    .line 283
    :goto_0
    return-void

    .line 282
    :cond_0
    iget-object v0, p0, Lco/uk/getmondo/main/c$aa;->b:Lco/uk/getmondo/main/c$a;

    invoke-interface {v0}, Lco/uk/getmondo/main/c$a;->N()V

    goto :goto_0
.end method
