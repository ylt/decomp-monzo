.class public final Lco/uk/getmondo/main/HomeActivity$a;
.super Ljava/lang/Object;
.source "HomeActivity.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lco/uk/getmondo/main/HomeActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000R\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0002\u0008\u0005\n\u0002\u0010\u0008\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0008\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J \u0010\r\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u00102\u0006\u0010\u0011\u001a\u00020\u00122\u0006\u0010\u0013\u001a\u00020\u0014H\u0007J\u0010\u0010\u0015\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u0010H\u0007J\u0018\u0010\u0016\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u00102\u0006\u0010\u0011\u001a\u00020\u0012H\u0007J\u0010\u0010\u0017\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u0010H\u0007J\u0018\u0010\u0018\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u00102\u0006\u0010\u0019\u001a\u00020\u001aH\u0007J\u0010\u0010\u001b\u001a\u00020\u001c2\u0006\u0010\u001d\u001a\u00020\u001eH\u0002J\u0010\u0010\u001f\u001a\u00020\u001c2\u0006\u0010\u000f\u001a\u00020\u0010H\u0007J,\u0010\u001f\u001a\u00020\u001c2\u0006\u0010\u000f\u001a\u00020\u00102\u0006\u0010 \u001a\u00020\u00042\u0008\u0010!\u001a\u0004\u0018\u00010\"2\u0008\u0010#\u001a\u0004\u0018\u00010\u0004H\u0007J\u0010\u0010$\u001a\u00020\u001c2\u0006\u0010\u000f\u001a\u00020\u0010H\u0007R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0082T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0004X\u0082T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0004X\u0082T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0008\u001a\u00020\u0004X\u0082T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\nX\u0082T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u0004X\u0082T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000c\u001a\u00020\u0004X\u0082T\u00a2\u0006\u0002\n\u0000\u00a8\u0006%"
    }
    d2 = {
        "Lco/uk/getmondo/main/HomeActivity$Companion;",
        "",
        "()V",
        "KEY_ANALYTIC_FROM_SHORTCUT",
        "",
        "KEY_CURRENT_SCREEN",
        "KEY_ID_FROM_SHORTCUT",
        "KEY_MONZO_ME_DATA",
        "KEY_YEAR_MONTH",
        "REQUEST_P2P_ONBOARDING",
        "",
        "TAG_ERROR_DIALOG_FRAGMENT",
        "TAG_ERROR_P2P_BLOCKED",
        "buildAppShortcutIntent",
        "Landroid/content/Intent;",
        "context",
        "Landroid/content/Context;",
        "screen",
        "Lco/uk/getmondo/main/Screen;",
        "shortcut",
        "Lco/uk/getmondo/common/app_shortcuts/AppShortcut;",
        "buildIntent",
        "buildIntentForScreen",
        "buildNotificationIntent",
        "buildSpendingIntent",
        "yearMonth",
        "Lorg/threeten/bp/YearMonth;",
        "disableShiftMode",
        "",
        "view",
        "Landroid/support/design/widget/BottomNavigationView;",
        "start",
        "username",
        "amount",
        "Lco/uk/getmondo/model/Amount;",
        "notes",
        "startWithoutClear",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 506
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/d/b/i;)V
    .locals 0

    .prologue
    .line 506
    invoke-direct {p0}, Lco/uk/getmondo/main/HomeActivity$a;-><init>()V

    return-void
.end method

.method private final a(Landroid/support/design/widget/BottomNavigationView;)V
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 577
    invoke-virtual {p1, v3}, Landroid/support/design/widget/BottomNavigationView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    if-nez v0, :cond_0

    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type android.support.design.internal.BottomNavigationMenuView"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    check-cast v0, Landroid/support/design/internal/c;

    .line 578
    nop

    .line 579
    :try_start_0
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    const-string v2, "mShiftingMode"

    invoke-virtual {v1, v2}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v1

    .line 580
    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Ljava/lang/reflect/Field;->setAccessible(Z)V

    .line 581
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Ljava/lang/reflect/Field;->setBoolean(Ljava/lang/Object;Z)V

    .line 582
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/lang/reflect/Field;->setAccessible(Z)V

    .line 583
    invoke-virtual {v0}, Landroid/support/design/internal/c;->getChildCount()I

    move-result v4

    move v2, v3

    :goto_0
    if-ge v2, v4, :cond_1

    .line 584
    invoke-virtual {v0, v2}, Landroid/support/design/internal/c;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    if-nez v1, :cond_2

    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type android.support.design.internal.BottomNavigationItemView"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Ljava/lang/NoSuchFieldException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1

    .line 591
    :catch_0
    move-exception v0

    .line 592
    check-cast v0, Ljava/lang/Throwable;

    const-string v1, "Unable to get shift mode field"

    new-array v2, v3, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Ld/a/a;->a(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 595
    :cond_1
    :goto_1
    return-void

    .line 584
    :cond_2
    :try_start_1
    check-cast v1, Landroid/support/design/internal/a;

    .line 586
    const/4 v5, 0x0

    invoke-virtual {v1, v5}, Landroid/support/design/internal/a;->setShiftingMode(Z)V

    .line 589
    invoke-virtual {v1}, Landroid/support/design/internal/a;->getItemData()Landroid/support/v7/view/menu/j;

    move-result-object v5

    invoke-virtual {v5}, Landroid/support/v7/view/menu/j;->isChecked()Z

    move-result v5

    invoke-virtual {v1, v5}, Landroid/support/design/internal/a;->setChecked(Z)V
    :try_end_1
    .catch Ljava/lang/NoSuchFieldException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_1 .. :try_end_1} :catch_1

    .line 583
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_0

    .line 593
    :catch_1
    move-exception v0

    .line 594
    check-cast v0, Ljava/lang/Throwable;

    const-string v1, "Unable to change value of shift mode"

    new-array v2, v3, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Ld/a/a;->a(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1
.end method

.method public static final synthetic a(Lco/uk/getmondo/main/HomeActivity$a;Landroid/support/design/widget/BottomNavigationView;)V
    .locals 0

    .prologue
    .line 506
    invoke-direct {p0, p1}, Lco/uk/getmondo/main/HomeActivity$a;->a(Landroid/support/design/widget/BottomNavigationView;)V

    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;Lco/uk/getmondo/main/g;)Landroid/content/Intent;
    .locals 3

    .prologue
    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "screen"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 533
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lco/uk/getmondo/main/HomeActivity;

    invoke-direct {v0, p1, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 534
    const-string v1, "KEY_CURRENT_SCREEN"

    iget v2, p2, Lco/uk/getmondo/main/g;->e:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    .line 535
    const/high16 v1, 0x4000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "Intent(context, HomeActi\u2026.FLAG_ACTIVITY_CLEAR_TOP)"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public final a(Landroid/content/Context;Lco/uk/getmondo/main/g;Lco/uk/getmondo/common/a/b;)Landroid/content/Intent;
    .locals 4

    .prologue
    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "screen"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "shortcut"

    invoke-static {p3, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 546
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.MAIN"

    sget-object v2, Landroid/net/Uri;->EMPTY:Landroid/net/Uri;

    const-class v3, Lco/uk/getmondo/main/HomeActivity;

    invoke-direct {v0, v1, v2, p1, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;Landroid/content/Context;Ljava/lang/Class;)V

    .line 547
    const-string v1, "KEY_CURRENT_SCREEN"

    iget v2, p2, Lco/uk/getmondo/main/g;->e:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    .line 548
    const-string v1, "KEY_ID_FROM_SHORTCUT"

    invoke-virtual {p3}, Lco/uk/getmondo/common/a/b;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 549
    const-string v1, "KEY_ANALYTIC_FROM_SHORTCUT"

    invoke-virtual {p3}, Lco/uk/getmondo/common/a/b;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 550
    const v1, 0x8000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "Intent(Intent.ACTION_MAI\u2026FLAG_ACTIVITY_CLEAR_TASK)"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public final a(Landroid/content/Context;Lorg/threeten/bp/YearMonth;)Landroid/content/Intent;
    .locals 3

    .prologue
    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "yearMonth"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 539
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lco/uk/getmondo/main/HomeActivity;

    invoke-direct {v0, p1, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 540
    const-string v1, "KEY_CURRENT_SCREEN"

    sget-object v2, Lco/uk/getmondo/main/g;->b:Lco/uk/getmondo/main/g;

    iget v2, v2, Lco/uk/getmondo/main/g;->e:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    .line 541
    const-string v1, "KEY_YEAR_MONTH"

    check-cast p2, Ljava/io/Serializable;

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    move-result-object v0

    .line 542
    const/high16 v1, 0x4000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "Intent(context, HomeActi\u2026.FLAG_ACTIVITY_CLEAR_TOP)"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public final a(Landroid/content/Context;)V
    .locals 2

    .prologue
    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 523
    check-cast p0, Lco/uk/getmondo/main/HomeActivity$a;

    invoke-virtual {p0, p1}, Lco/uk/getmondo/main/HomeActivity$a;->b(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    .line 524
    const v1, 0x10018000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    move-result-object v0

    .line 525
    invoke-virtual {p1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 526
    return-void
.end method

.method public final a(Landroid/content/Context;Ljava/lang/String;Lco/uk/getmondo/d/c;Ljava/lang/String;)V
    .locals 3

    .prologue
    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "username"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 565
    new-instance v1, Landroid/content/Intent;

    const-class v0, Lco/uk/getmondo/main/HomeActivity;

    invoke-direct {v1, p1, v0}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 566
    const-string v2, "KEY_MONZO_ME_DATA"

    new-instance v0, Lco/uk/getmondo/main/f;

    invoke-direct {v0, p2, p3, p4}, Lco/uk/getmondo/main/f;-><init>(Ljava/lang/String;Lco/uk/getmondo/d/c;Ljava/lang/String;)V

    check-cast v0, Landroid/os/Parcelable;

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 567
    const/high16 v0, 0x14000000

    invoke-virtual {v1, v0}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 568
    invoke-virtual {p1, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 569
    return-void
.end method

.method public final b(Landroid/content/Context;)Landroid/content/Intent;
    .locals 2

    .prologue
    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 529
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lco/uk/getmondo/main/HomeActivity;

    invoke-direct {v0, p1, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    return-object v0
.end method

.method public final c(Landroid/content/Context;)Landroid/content/Intent;
    .locals 3

    .prologue
    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 554
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lco/uk/getmondo/main/HomeActivity;

    invoke-direct {v0, p1, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 555
    const/high16 v1, 0x14000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 556
    const-string v1, "KEY_CURRENT_SCREEN"

    sget-object v2, Lco/uk/getmondo/main/g;->a:Lco/uk/getmondo/main/g;

    iget v2, v2, Lco/uk/getmondo/main/g;->e:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 557
    return-object v0
.end method
