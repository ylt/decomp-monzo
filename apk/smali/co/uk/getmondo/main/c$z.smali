.class final Lco/uk/getmondo/main/c$z;
.super Ljava/lang/Object;
.source "HomePresenter.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lco/uk/getmondo/main/c;->a(Lco/uk/getmondo/main/c$a;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h",
        "<TT;",
        "Lio/reactivex/r",
        "<+TR;>;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000\u001e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0002\u0010\u0000\u001aH\u0012D\u0012B\u0012\u000c\u0012\n \u0004*\u0004\u0018\u00010\u00030\u0003\u0012\u000c\u0012\n \u0004*\u0004\u0018\u00010\u00050\u0005 \u0004* \u0012\u000c\u0012\n \u0004*\u0004\u0018\u00010\u00030\u0003\u0012\u000c\u0012\n \u0004*\u0004\u0018\u00010\u00050\u0005\u0018\u00010\u00020\u00020\u00012\u0006\u0010\u0006\u001a\u00020\u0007H\n\u00a2\u0006\u0004\u0008\u0008\u0010\t"
    }
    d2 = {
        "<anonymous>",
        "Lio/reactivex/Observable;",
        "Lkotlin/Pair;",
        "Lco/uk/getmondo/api/model/sign_up/MigrationInfo;",
        "kotlin.jvm.PlatformType",
        "Lco/uk/getmondo/main/Screen;",
        "it",
        "",
        "apply",
        "(Lkotlin/Unit;)Lio/reactivex/Observable;"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field final synthetic a:Lco/uk/getmondo/main/c;

.field final synthetic b:Lio/reactivex/n;


# direct methods
.method constructor <init>(Lco/uk/getmondo/main/c;Lio/reactivex/n;)V
    .locals 0

    iput-object p1, p0, Lco/uk/getmondo/main/c$z;->a:Lco/uk/getmondo/main/c;

    iput-object p2, p0, Lco/uk/getmondo/main/c$z;->b:Lio/reactivex/n;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lkotlin/n;)Lio/reactivex/n;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/n;",
            ")",
            "Lio/reactivex/n",
            "<",
            "Lkotlin/h",
            "<",
            "Lco/uk/getmondo/api/model/sign_up/MigrationInfo;",
            "Lco/uk/getmondo/main/g;",
            ">;>;"
        }
    .end annotation

    .prologue
    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 272
    sget-object v0, Lio/reactivex/rxkotlin/b;->a:Lio/reactivex/rxkotlin/b;

    .line 273
    iget-object v1, p0, Lco/uk/getmondo/main/c$z;->a:Lco/uk/getmondo/main/c;

    invoke-static {v1}, Lco/uk/getmondo/main/c;->m(Lco/uk/getmondo/main/c;)Lco/uk/getmondo/signup/status/b;

    move-result-object v1

    invoke-virtual {v1}, Lco/uk/getmondo/signup/status/b;->d()Lio/reactivex/n;

    move-result-object v1

    iget-object v2, p0, Lco/uk/getmondo/main/c$z;->a:Lco/uk/getmondo/main/c;

    invoke-static {v2}, Lco/uk/getmondo/main/c;->c(Lco/uk/getmondo/main/c;)Lio/reactivex/u;

    move-result-object v2

    invoke-virtual {v1, v2}, Lio/reactivex/n;->subscribeOn(Lio/reactivex/u;)Lio/reactivex/n;

    move-result-object v1

    const-string v2, "signupStatusManager.migr\u2026.subscribeOn(ioScheduler)"

    invoke-static {v1, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 274
    iget-object v2, p0, Lco/uk/getmondo/main/c$z;->b:Lio/reactivex/n;

    const-string v3, "onScreenChanged"

    invoke-static {v2, v3}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 272
    invoke-virtual {v0, v1, v2}, Lio/reactivex/rxkotlin/b;->a(Lio/reactivex/n;Lio/reactivex/n;)Lio/reactivex/n;

    move-result-object v0

    .line 274
    return-object v0
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 45
    check-cast p1, Lkotlin/n;

    invoke-virtual {p0, p1}, Lco/uk/getmondo/main/c$z;->a(Lkotlin/n;)Lio/reactivex/n;

    move-result-object v0

    return-object v0
.end method
