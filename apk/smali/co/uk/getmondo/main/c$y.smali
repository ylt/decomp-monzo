.class final Lco/uk/getmondo/main/c$y;
.super Ljava/lang/Object;
.source "HomePresenter.kt"

# interfaces
.implements Lio/reactivex/c/g;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lco/uk/getmondo/main/c;->a(Lco/uk/getmondo/main/c$a;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/g",
        "<",
        "Lco/uk/getmondo/main/g;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "",
        "screen",
        "Lco/uk/getmondo/main/Screen;",
        "kotlin.jvm.PlatformType",
        "accept"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field final synthetic a:Lco/uk/getmondo/main/c;

.field final synthetic b:Z

.field final synthetic c:Lco/uk/getmondo/main/c$a;


# direct methods
.method constructor <init>(Lco/uk/getmondo/main/c;ZLco/uk/getmondo/main/c$a;)V
    .locals 0

    iput-object p1, p0, Lco/uk/getmondo/main/c$y;->a:Lco/uk/getmondo/main/c;

    iput-boolean p2, p0, Lco/uk/getmondo/main/c$y;->b:Z

    iput-object p3, p0, Lco/uk/getmondo/main/c$y;->c:Lco/uk/getmondo/main/c$a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lco/uk/getmondo/main/g;)V
    .locals 2

    .prologue
    .line 251
    sget-object v0, Lco/uk/getmondo/main/g;->d:Lco/uk/getmondo/main/g;

    invoke-static {p1, v0}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 252
    iget-boolean v0, p0, Lco/uk/getmondo/main/c$y;->b:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lco/uk/getmondo/main/c$y;->a:Lco/uk/getmondo/main/c;

    invoke-static {v0}, Lco/uk/getmondo/main/c;->l(Lco/uk/getmondo/main/c;)Lco/uk/getmondo/common/o;

    move-result-object v0

    invoke-virtual {v0}, Lco/uk/getmondo/common/o;->b()Lco/uk/getmondo/d/l;

    move-result-object v0

    invoke-virtual {v0}, Lco/uk/getmondo/d/l;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    .line 253
    :goto_0
    if-eqz v0, :cond_3

    .line 254
    iget-object v0, p0, Lco/uk/getmondo/main/c$y;->a:Lco/uk/getmondo/main/c;

    invoke-static {v0}, Lco/uk/getmondo/main/c;->i(Lco/uk/getmondo/main/c;)Lco/uk/getmondo/payments/send/data/p;

    move-result-object v0

    invoke-virtual {v0}, Lco/uk/getmondo/payments/send/data/p;->a()Lco/uk/getmondo/payments/send/data/a/d;

    move-result-object v0

    if-nez v0, :cond_2

    .line 264
    :goto_1
    return-void

    .line 252
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 254
    :cond_2
    sget-object v1, Lco/uk/getmondo/main/d;->b:[I

    invoke-virtual {v0}, Lco/uk/getmondo/payments/send/data/a/d;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    goto :goto_1

    .line 255
    :pswitch_0
    iget-object v0, p0, Lco/uk/getmondo/main/c$y;->c:Lco/uk/getmondo/main/c$a;

    sget-object v1, Lco/uk/getmondo/main/g;->d:Lco/uk/getmondo/main/g;

    invoke-interface {v0, v1}, Lco/uk/getmondo/main/c$a;->a(Lco/uk/getmondo/main/g;)V

    goto :goto_1

    .line 256
    :pswitch_1
    iget-object v0, p0, Lco/uk/getmondo/main/c$y;->c:Lco/uk/getmondo/main/c$a;

    invoke-interface {v0}, Lco/uk/getmondo/main/c$a;->d()V

    goto :goto_1

    .line 257
    :pswitch_2
    iget-object v0, p0, Lco/uk/getmondo/main/c$y;->c:Lco/uk/getmondo/main/c$a;

    invoke-interface {v0}, Lco/uk/getmondo/main/c$a;->C()V

    goto :goto_1

    .line 260
    :cond_3
    iget-object v0, p0, Lco/uk/getmondo/main/c$y;->c:Lco/uk/getmondo/main/c$a;

    invoke-interface {v0, p1}, Lco/uk/getmondo/main/c$a;->a(Lco/uk/getmondo/main/g;)V

    goto :goto_1

    .line 263
    :cond_4
    iget-object v0, p0, Lco/uk/getmondo/main/c$y;->c:Lco/uk/getmondo/main/c$a;

    const-string v1, "screen"

    invoke-static {p1, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v0, p1}, Lco/uk/getmondo/main/c$a;->a(Lco/uk/getmondo/main/g;)V

    goto :goto_1

    .line 254
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 45
    check-cast p1, Lco/uk/getmondo/main/g;

    invoke-virtual {p0, p1}, Lco/uk/getmondo/main/c$y;->a(Lco/uk/getmondo/main/g;)V

    return-void
.end method
