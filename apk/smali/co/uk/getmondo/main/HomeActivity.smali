.class public final Lco/uk/getmondo/main/HomeActivity;
.super Lco/uk/getmondo/common/activities/b;
.source "HomeActivity.kt"

# interfaces
.implements Lco/uk/getmondo/main/c$a;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lco/uk/getmondo/main/HomeActivity$a;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000\u00b4\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u0008\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0008\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u001a\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u000e\n\u0002\u0008\u000b\n\u0002\u0018\u0002\n\u0002\u0008\u0013\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0008\u0018\u0000 \u008e\u00012\u00020\u00012\u00020\u0002:\u0002\u008e\u0001B\u0005\u00a2\u0006\u0002\u0010\u0003J\u0008\u00103\u001a\u000202H\u0002J\n\u00104\u001a\u0004\u0018\u000105H\u0002J\n\u00106\u001a\u0004\u0018\u00010\'H\u0016J\u0008\u00107\u001a\u000202H\u0016J\u0008\u00108\u001a\u000202H\u0016J\u0008\u00109\u001a\u000202H\u0016J\"\u0010:\u001a\u0002022\u0006\u0010;\u001a\u00020+2\u0006\u0010<\u001a\u00020+2\u0008\u0010=\u001a\u0004\u0018\u00010>H\u0014J\u0008\u0010?\u001a\u000202H\u0016J\u000e\u0010@\u001a\u0008\u0012\u0004\u0012\u0002020AH\u0016J\u000e\u0010B\u001a\u0008\u0012\u0004\u0012\u0002020AH\u0016J\u0012\u0010C\u001a\u0002022\u0008\u0010D\u001a\u0004\u0018\u00010EH\u0014J\u0008\u0010F\u001a\u000202H\u0014J\u000e\u0010G\u001a\u0008\u0012\u0004\u0012\u0002020AH\u0016J\u000e\u0010H\u001a\u0008\u0012\u0004\u0012\u0002020AH\u0016J\u0010\u0010I\u001a\u0002022\u0006\u0010J\u001a\u00020>H\u0002J\u000e\u0010K\u001a\u0008\u0012\u0004\u0012\u0002020AH\u0016J\u0010\u0010L\u001a\u0002022\u0006\u0010J\u001a\u00020>H\u0014J\u000e\u0010M\u001a\u0008\u0012\u0004\u0012\u00020$0AH\u0016J\u000e\u0010N\u001a\u0008\u0012\u0004\u0012\u0002020AH\u0016J,\u0010O\u001a&\u0012\u000c\u0012\n %*\u0004\u0018\u00010202 %*\u0012\u0012\u000c\u0012\n %*\u0004\u0018\u00010202\u0018\u00010#0#H\u0016J\u000e\u0010P\u001a\u0008\u0012\u0004\u0012\u0002020AH\u0016J\u0008\u0010Q\u001a\u000202H\u0014J\u0010\u0010R\u001a\u0002022\u0006\u0010S\u001a\u00020EH\u0014J\u000e\u0010T\u001a\u0008\u0012\u0004\u0012\u00020\u00130AH\u0016J\u000e\u0010U\u001a\u0008\u0012\u0004\u0012\u0002020AH\u0016J\u0008\u0010V\u001a\u000202H\u0014J\u0008\u0010W\u001a\u000202H\u0016J\u0008\u0010X\u001a\u000202H\u0016J\u0008\u0010Y\u001a\u000202H\u0016J\u0008\u0010Z\u001a\u000202H\u0016J\u0008\u0010[\u001a\u000202H\u0016J\u0008\u0010\\\u001a\u000202H\u0016J\u0008\u0010]\u001a\u000202H\u0016J\u0010\u0010^\u001a\u0002022\u0006\u0010_\u001a\u00020`H\u0016J\u0010\u0010a\u001a\u0002022\u0006\u0010_\u001a\u00020`H\u0016J\u0012\u0010b\u001a\u0002022\u0008\u0010c\u001a\u0004\u0018\u00010dH\u0016J\u0012\u0010e\u001a\u0002022\u0008\u0010c\u001a\u0004\u0018\u00010dH\u0016J\u0008\u0010f\u001a\u000202H\u0016J\u0008\u0010g\u001a\u000202H\u0016J\u0008\u0010h\u001a\u000202H\u0016J\u0010\u0010i\u001a\u0002022\u0006\u0010j\u001a\u00020!H\u0002J\u001a\u0010k\u001a\u0002022\u0008\u0010l\u001a\u0004\u0018\u00010d2\u0006\u0010m\u001a\u00020dH\u0016J\u000e\u0010n\u001a\u0002022\u0006\u0010o\u001a\u00020pJ\u0008\u0010q\u001a\u000202H\u0016J\u000e\u0010r\u001a\u0002022\u0006\u0010s\u001a\u00020!J\u0008\u0010t\u001a\u000202H\u0016J\u0008\u0010u\u001a\u000202H\u0016J\u0008\u0010v\u001a\u000202H\u0016J\u0008\u0010w\u001a\u000202H\u0016J\u0012\u0010x\u001a\u0002022\u0008\u0008\u0001\u0010y\u001a\u00020+H\u0016J\u0010\u0010x\u001a\u0002022\u0006\u0010y\u001a\u00020dH\u0016J\u0010\u0010z\u001a\u0002022\u0006\u0010{\u001a\u00020dH\u0016J\u0008\u0010|\u001a\u000202H\u0016J!\u0010}\u001a\u0002022\u0006\u0010~\u001a\u00020d2\u0006\u0010\u007f\u001a\u00020d2\u0007\u0010\u0080\u0001\u001a\u00020!H\u0016J\t\u0010\u0081\u0001\u001a\u000202H\u0016J\u0013\u0010\u0082\u0001\u001a\u0002022\u0008\u0010\u0083\u0001\u001a\u00030\u0084\u0001H\u0016J\u0013\u0010\u0085\u0001\u001a\u0002022\u0008\u0010\u0086\u0001\u001a\u00030\u0087\u0001H\u0016J\t\u0010\u0088\u0001\u001a\u000202H\u0016J\u001b\u0010\u0089\u0001\u001a\u0002022\u0007\u0010\u008a\u0001\u001a\u00020d2\u0007\u0010\u008b\u0001\u001a\u00020dH\u0016J\u0012\u0010\u008c\u0001\u001a\u0002022\u0007\u0010\u008d\u0001\u001a\u00020\u0013H\u0016R\u0010\u0010\u0004\u001a\u0004\u0018\u00010\u0005X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u001e\u0010\u0006\u001a\u00020\u00078\u0006@\u0006X\u0087.\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u0008\u0010\t\"\u0004\u0008\n\u0010\u000bR\u001b\u0010\u000c\u001a\u00020\r8BX\u0082\u0084\u0002\u00a2\u0006\u000c\n\u0004\u0008\u0010\u0010\u0011\u001a\u0004\u0008\u000e\u0010\u000fR\u0010\u0010\u0012\u001a\u0004\u0018\u00010\u0013X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u001e\u0010\u0014\u001a\u00020\u00158\u0006@\u0006X\u0087.\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u0016\u0010\u0017\"\u0004\u0008\u0018\u0010\u0019R\u001e\u0010\u001a\u001a\u00020\u001b8\u0006@\u0006X\u0087.\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u001c\u0010\u001d\"\u0004\u0008\u001e\u0010\u001fR\u000e\u0010 \u001a\u00020!X\u0082\u000e\u00a2\u0006\u0002\n\u0000R2\u0010\"\u001a&\u0012\u000c\u0012\n %*\u0004\u0018\u00010$0$ %*\u0012\u0012\u000c\u0012\n %*\u0004\u0018\u00010$0$\u0018\u00010#0#X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010&\u001a\u00020\'8BX\u0082\u0004\u00a2\u0006\u0006\u001a\u0004\u0008(\u0010)R\u001b\u0010*\u001a\u00020+8BX\u0082\u0084\u0002\u00a2\u0006\u000c\n\u0004\u0008.\u0010\u0011\u001a\u0004\u0008,\u0010-R\u0010\u0010/\u001a\u0004\u0018\u000100X\u0082\u000e\u00a2\u0006\u0002\n\u0000R2\u00101\u001a&\u0012\u000c\u0012\n %*\u0004\u0018\u00010202 %*\u0012\u0012\u000c\u0012\n %*\u0004\u0018\u00010202\u0018\u00010#0#X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u008f\u0001"
    }
    d2 = {
        "Lco/uk/getmondo/main/HomeActivity;",
        "Lco/uk/getmondo/common/activities/BaseActivity;",
        "Lco/uk/getmondo/main/HomePresenter$View;",
        "()V",
        "actionBarDrawerToggle",
        "Landroid/support/v7/app/ActionBarDrawerToggle;",
        "appShortcuts",
        "Lco/uk/getmondo/common/app_shortcuts/AppShortcuts;",
        "getAppShortcuts",
        "()Lco/uk/getmondo/common/app_shortcuts/AppShortcuts;",
        "setAppShortcuts",
        "(Lco/uk/getmondo/common/app_shortcuts/AppShortcuts;)V",
        "avatarGenerator",
        "Lco/uk/getmondo/common/ui/AvatarGenerator;",
        "getAvatarGenerator",
        "()Lco/uk/getmondo/common/ui/AvatarGenerator;",
        "avatarGenerator$delegate",
        "Lkotlin/Lazy;",
        "currentScreen",
        "Lco/uk/getmondo/main/Screen;",
        "homePresenter",
        "Lco/uk/getmondo/main/HomePresenter;",
        "getHomePresenter",
        "()Lco/uk/getmondo/main/HomePresenter;",
        "setHomePresenter",
        "(Lco/uk/getmondo/main/HomePresenter;)V",
        "intercomService",
        "Lco/uk/getmondo/common/IntercomService;",
        "getIntercomService",
        "()Lco/uk/getmondo/common/IntercomService;",
        "setIntercomService",
        "(Lco/uk/getmondo/common/IntercomService;)V",
        "isRecoveringState",
        "",
        "monzoMeDataChangedSubject",
        "Lcom/jakewharton/rxrelay2/PublishRelay;",
        "Lco/uk/getmondo/main/MonzoMeData;",
        "kotlin.jvm.PlatformType",
        "navigationDrawerHeader",
        "Landroid/view/View;",
        "getNavigationDrawerHeader",
        "()Landroid/view/View;",
        "profileInitialFontSize",
        "",
        "getProfileInitialFontSize",
        "()I",
        "profileInitialFontSize$delegate",
        "spendingYearMonth",
        "Lorg/threeten/bp/YearMonth;",
        "startSubject",
        "",
        "closeDrawer",
        "findCurrentFragment",
        "Lco/uk/getmondo/common/fragments/BaseFragment;",
        "getSnackbarView",
        "hideLoading",
        "hideMigrationBanner",
        "hideOutageWarning",
        "onActivityResult",
        "requestCode",
        "resultCode",
        "data",
        "Landroid/content/Intent;",
        "onBackPressed",
        "onChatClicked",
        "Lio/reactivex/Observable;",
        "onCommunityClicked",
        "onCreate",
        "savedInstanceState",
        "Landroid/os/Bundle;",
        "onDestroy",
        "onGhostLogOutClicked",
        "onHelpClicked",
        "onIntent",
        "intent",
        "onMigrationBannerClicked",
        "onNewIntent",
        "onPendingMonzoMeDataChanged",
        "onRefreshIncidents",
        "onRefreshMigrationBanner",
        "onRefreshUserSettings",
        "onResume",
        "onSaveInstanceState",
        "outState",
        "onScreenChanged",
        "onSettingsClicked",
        "onStart",
        "openChat",
        "openCommunity",
        "openCurrentAccountComing",
        "openCurrentAccountHere",
        "openFaqs",
        "openHelp",
        "openP2pOnboardingFromContacts",
        "openP2pOnboardingFromMonzoMeDeepLink",
        "pendingPayment",
        "Lco/uk/getmondo/payments/send/data/model/PeerPayment;",
        "openPaymentInfo",
        "openSddMigrationDismissible",
        "rejectionNote",
        "",
        "openSddMigrationPersistent",
        "openSettings",
        "openSignup",
        "openVerificationPending",
        "setMigrationBannerStyle",
        "loudStyle",
        "setProfileInformation",
        "nameToDisplay",
        "emailAddress",
        "setupNavigationDrawer",
        "toolbar",
        "Landroid/support/v7/widget/Toolbar;",
        "showActivateCardBanner",
        "showBottomNavShadow",
        "show",
        "showCannotPayYourself",
        "showContactHasP2pDisabled",
        "showContactNotOnMonzo",
        "showContinueSignupBanner",
        "showError",
        "message",
        "showGhostBanner",
        "email",
        "showLoading",
        "showMigrationBanner",
        "title",
        "subtitle",
        "signupAllowed",
        "showMonzoMeGenericError",
        "showNews",
        "newsItem",
        "Lco/uk/getmondo/news/NewsItem;",
        "showOutageWarning",
        "incident",
        "Lco/uk/getmondo/api/model/service_status/ServiceStatusIncident;",
        "showP2pBlocked",
        "showRetailUi",
        "sortCode",
        "accountNumber",
        "showScreen",
        "screen",
        "Companion",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# static fields
.field static final synthetic a:[Lkotlin/reflect/l;

.field public static final f:Lco/uk/getmondo/main/HomeActivity$a;


# instance fields
.field public b:Lco/uk/getmondo/main/c;

.field public c:Lco/uk/getmondo/common/q;

.field public e:Lco/uk/getmondo/common/a/c;

.field private final g:Lcom/b/b/c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/b/b/c",
            "<",
            "Lco/uk/getmondo/main/f;",
            ">;"
        }
    .end annotation
.end field

.field private final h:Lcom/b/b/c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/b/b/c",
            "<",
            "Lkotlin/n;",
            ">;"
        }
    .end annotation
.end field

.field private final i:Lkotlin/c;

.field private final j:Lkotlin/c;

.field private k:Z

.field private l:Lorg/threeten/bp/YearMonth;

.field private m:Landroid/support/v7/app/b;

.field private n:Lco/uk/getmondo/main/g;

.field private o:Ljava/util/HashMap;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const/4 v0, 0x2

    new-array v1, v0, [Lkotlin/reflect/l;

    const/4 v2, 0x0

    new-instance v0, Lkotlin/d/b/w;

    const-class v3, Lco/uk/getmondo/main/HomeActivity;

    invoke-static {v3}, Lkotlin/d/b/y;->a(Ljava/lang/Class;)Lkotlin/reflect/c;

    move-result-object v3

    const-string v4, "avatarGenerator"

    const-string v5, "getAvatarGenerator()Lco/uk/getmondo/common/ui/AvatarGenerator;"

    invoke-direct {v0, v3, v4, v5}, Lkotlin/d/b/w;-><init>(Lkotlin/reflect/e;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v0}, Lkotlin/d/b/y;->a(Lkotlin/d/b/v;)Lkotlin/reflect/n;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/l;

    aput-object v0, v1, v2

    const/4 v2, 0x1

    new-instance v0, Lkotlin/d/b/w;

    const-class v3, Lco/uk/getmondo/main/HomeActivity;

    invoke-static {v3}, Lkotlin/d/b/y;->a(Ljava/lang/Class;)Lkotlin/reflect/c;

    move-result-object v3

    const-string v4, "profileInitialFontSize"

    const-string v5, "getProfileInitialFontSize()I"

    invoke-direct {v0, v3, v4, v5}, Lkotlin/d/b/w;-><init>(Lkotlin/reflect/e;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v0}, Lkotlin/d/b/y;->a(Lkotlin/d/b/v;)Lkotlin/reflect/n;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/l;

    aput-object v0, v1, v2

    sput-object v1, Lco/uk/getmondo/main/HomeActivity;->a:[Lkotlin/reflect/l;

    new-instance v0, Lco/uk/getmondo/main/HomeActivity$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lco/uk/getmondo/main/HomeActivity$a;-><init>(Lkotlin/d/b/i;)V

    sput-object v0, Lco/uk/getmondo/main/HomeActivity;->f:Lco/uk/getmondo/main/HomeActivity$a;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 63
    invoke-direct {p0}, Lco/uk/getmondo/common/activities/b;-><init>()V

    .line 65
    invoke-static {}, Lcom/b/b/c;->a()Lcom/b/b/c;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/main/HomeActivity;->g:Lcom/b/b/c;

    .line 66
    invoke-static {}, Lcom/b/b/c;->a()Lcom/b/b/c;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/main/HomeActivity;->h:Lcom/b/b/c;

    .line 67
    new-instance v0, Lco/uk/getmondo/main/HomeActivity$b;

    invoke-direct {v0, p0}, Lco/uk/getmondo/main/HomeActivity$b;-><init>(Lco/uk/getmondo/main/HomeActivity;)V

    check-cast v0, Lkotlin/d/a/a;

    invoke-static {v0}, Lkotlin/d;->a(Lkotlin/d/a/a;)Lkotlin/c;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/main/HomeActivity;->i:Lkotlin/c;

    .line 68
    sget-object v0, Lco/uk/getmondo/main/HomeActivity$k;->a:Lco/uk/getmondo/main/HomeActivity$k;

    check-cast v0, Lkotlin/d/a/a;

    invoke-static {v0}, Lkotlin/d;->a(Lkotlin/d/a/a;)Lkotlin/c;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/main/HomeActivity;->j:Lkotlin/c;

    return-void
.end method

.method private final R()Lco/uk/getmondo/common/ui/a;
    .locals 3

    iget-object v0, p0, Lco/uk/getmondo/main/HomeActivity;->i:Lkotlin/c;

    sget-object v1, Lco/uk/getmondo/main/HomeActivity;->a:[Lkotlin/reflect/l;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-interface {v0}, Lkotlin/c;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/common/ui/a;

    return-object v0
.end method

.method private final S()I
    .locals 3

    iget-object v0, p0, Lco/uk/getmondo/main/HomeActivity;->j:Lkotlin/c;

    sget-object v1, Lco/uk/getmondo/main/HomeActivity;->a:[Lkotlin/reflect/l;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-interface {v0}, Lkotlin/c;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Number;

    invoke-virtual {v0}, Ljava/lang/Number;->intValue()I

    move-result v0

    return v0
.end method

.method private final T()Landroid/view/View;
    .locals 2

    .prologue
    .line 80
    sget v0, Lco/uk/getmondo/c$a;->navigationDrawer:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/main/HomeActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/design/widget/NavigationView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/design/widget/NavigationView;->c(I)Landroid/view/View;

    move-result-object v0

    const-string v1, "navigationDrawer.getHeaderView(0)"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method private final U()V
    .locals 2

    .prologue
    .line 273
    sget v0, Lco/uk/getmondo/c$a;->drawerLayout:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/main/HomeActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v4/widget/DrawerLayout;

    new-instance v1, Lco/uk/getmondo/main/HomeActivity$c;

    invoke-direct {v1, p0}, Lco/uk/getmondo/main/HomeActivity$c;-><init>(Lco/uk/getmondo/main/HomeActivity;)V

    check-cast v1, Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/DrawerLayout;->post(Ljava/lang/Runnable;)Z

    .line 274
    return-void
.end method

.method private final V()Lco/uk/getmondo/common/f/a;
    .locals 2

    .prologue
    .line 481
    invoke-virtual {p0}, Lco/uk/getmondo/main/HomeActivity;->getSupportFragmentManager()Landroid/support/v4/app/n;

    move-result-object v0

    const v1, 0x7f11019a

    invoke-virtual {v0, v1}, Landroid/support/v4/app/n;->a(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/common/f/a;

    return-object v0
.end method

.method public static final a(Landroid/content/Context;Lorg/threeten/bp/YearMonth;)Landroid/content/Intent;
    .locals 1

    const-string v0, "context"

    invoke-static {p0, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "yearMonth"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v0, Lco/uk/getmondo/main/HomeActivity;->f:Lco/uk/getmondo/main/HomeActivity$a;

    invoke-virtual {v0, p0, p1}, Lco/uk/getmondo/main/HomeActivity$a;->a(Landroid/content/Context;Lorg/threeten/bp/YearMonth;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public static final synthetic a(Lco/uk/getmondo/main/HomeActivity;)Lco/uk/getmondo/main/g;
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Lco/uk/getmondo/main/HomeActivity;->n:Lco/uk/getmondo/main/g;

    return-object v0
.end method

.method public static final a(Landroid/content/Context;)V
    .locals 1

    const-string v0, "context"

    invoke-static {p0, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v0, Lco/uk/getmondo/main/HomeActivity;->f:Lco/uk/getmondo/main/HomeActivity$a;

    invoke-virtual {v0, p0}, Lco/uk/getmondo/main/HomeActivity$a;->a(Landroid/content/Context;)V

    return-void
.end method

.method public static final a(Landroid/content/Context;Ljava/lang/String;Lco/uk/getmondo/d/c;Ljava/lang/String;)V
    .locals 1

    const-string v0, "context"

    invoke-static {p0, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "username"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v0, Lco/uk/getmondo/main/HomeActivity;->f:Lco/uk/getmondo/main/HomeActivity$a;

    invoke-virtual {v0, p0, p1, p2, p3}, Lco/uk/getmondo/main/HomeActivity$a;->a(Landroid/content/Context;Ljava/lang/String;Lco/uk/getmondo/d/c;Ljava/lang/String;)V

    return-void
.end method

.method private final a(Landroid/content/Intent;)V
    .locals 3

    .prologue
    .line 108
    const-string v0, "KEY_ID_FROM_SHORTCUT"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 109
    iget-object v0, p0, Lco/uk/getmondo/main/HomeActivity;->e:Lco/uk/getmondo/common/a/c;

    if-nez v0, :cond_0

    const-string v1, "appShortcuts"

    invoke-static {v1}, Lkotlin/d/b/l;->b(Ljava/lang/String;)V

    .line 110
    :cond_0
    const-string v1, "KEY_ID_FROM_SHORTCUT"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 111
    const-string v2, "KEY_ANALYTIC_FROM_SHORTCUT"

    invoke-virtual {p1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 109
    invoke-interface {v0, v1, v2}, Lco/uk/getmondo/common/a/c;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 114
    :cond_1
    const-string v0, "KEY_YEAR_MONTH"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 115
    const-string v0, "KEY_YEAR_MONTH"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    if-nez v0, :cond_2

    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type org.threeten.bp.YearMonth"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    check-cast v0, Lorg/threeten/bp/YearMonth;

    iput-object v0, p0, Lco/uk/getmondo/main/HomeActivity;->l:Lorg/threeten/bp/YearMonth;

    .line 118
    :cond_3
    const-string v0, "KEY_CURRENT_SCREEN"

    sget-object v1, Lco/uk/getmondo/main/g;->a:Lco/uk/getmondo/main/g;

    iget v1, v1, Lco/uk/getmondo/main/g;->e:I

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    invoke-static {v0}, Lco/uk/getmondo/main/g;->a(I)Lco/uk/getmondo/main/g;

    move-result-object v1

    .line 119
    if-eqz v1, :cond_4

    .line 120
    sget v0, Lco/uk/getmondo/c$a;->bottomNavigationView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/main/HomeActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/design/widget/BottomNavigationView;

    iget v1, v1, Lco/uk/getmondo/main/g;->f:I

    invoke-virtual {v0, v1}, Landroid/support/design/widget/BottomNavigationView;->setSelectedItemId(I)V

    .line 122
    :cond_4
    return-void
.end method

.method public static final b(Landroid/content/Context;)Landroid/content/Intent;
    .locals 1

    const-string v0, "context"

    invoke-static {p0, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v0, Lco/uk/getmondo/main/HomeActivity;->f:Lco/uk/getmondo/main/HomeActivity$a;

    invoke-virtual {v0, p0}, Lco/uk/getmondo/main/HomeActivity$a;->b(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public static final synthetic b(Lco/uk/getmondo/main/HomeActivity;)V
    .locals 0

    .prologue
    .line 63
    invoke-direct {p0}, Lco/uk/getmondo/main/HomeActivity;->U()V

    return-void
.end method

.method private final b(Z)V
    .locals 4

    .prologue
    const v3, 0x7f0f0005

    const/4 v1, -0x1

    .line 421
    if-eqz p1, :cond_0

    .line 422
    sget v0, Lco/uk/getmondo/c$a;->migrationBanner:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/main/HomeActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/constraint/ConstraintLayout;

    invoke-virtual {v0, v3}, Landroid/support/constraint/ConstraintLayout;->setBackgroundResource(I)V

    .line 423
    sget v0, Lco/uk/getmondo/c$a;->migrationBannerTitle:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/main/HomeActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 424
    sget v0, Lco/uk/getmondo/c$a;->migrationBannerSubtitle:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/main/HomeActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 425
    sget v0, Lco/uk/getmondo/c$a;->migrationBannerButton:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/main/HomeActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setTextColor(I)V

    .line 431
    :goto_0
    return-void

    .line 427
    :cond_0
    sget v0, Lco/uk/getmondo/c$a;->migrationBanner:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/main/HomeActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/constraint/ConstraintLayout;

    const v1, 0x7f0f0075

    invoke-virtual {v0, v1}, Landroid/support/constraint/ConstraintLayout;->setBackgroundResource(I)V

    .line 428
    sget v0, Lco/uk/getmondo/c$a;->migrationBannerTitle:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/main/HomeActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    move-object v1, p0

    check-cast v1, Landroid/content/Context;

    const v2, 0x7f0f0036

    invoke-static {v1, v2}, Landroid/support/v4/content/a;->c(Landroid/content/Context;I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 429
    sget v0, Lco/uk/getmondo/c$a;->migrationBannerSubtitle:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/main/HomeActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    move-object v1, p0

    check-cast v1, Landroid/content/Context;

    const v2, 0x7f0f006e

    invoke-static {v1, v2}, Landroid/support/v4/content/a;->c(Landroid/content/Context;I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 430
    sget v0, Lco/uk/getmondo/c$a;->migrationBannerButton:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/main/HomeActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    check-cast p0, Landroid/content/Context;

    invoke-static {p0, v3}, Landroid/support/v4/content/a;->c(Landroid/content/Context;I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setTextColor(I)V

    goto :goto_0
.end method

.method public static final c(Landroid/content/Context;)Landroid/content/Intent;
    .locals 1

    const-string v0, "context"

    invoke-static {p0, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v0, Lco/uk/getmondo/main/HomeActivity;->f:Lco/uk/getmondo/main/HomeActivity$a;

    invoke-virtual {v0, p0}, Lco/uk/getmondo/main/HomeActivity$a;->c(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public A()V
    .locals 3

    .prologue
    .line 317
    const v0, 0x7f0a0359

    invoke-virtual {p0, v0}, Lco/uk/getmondo/main/HomeActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    const v1, 0x7f0a034c

    invoke-virtual {p0, v1}, Lco/uk/getmondo/main/HomeActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lco/uk/getmondo/common/d/a;->a(Ljava/lang/String;Ljava/lang/String;Z)Lco/uk/getmondo/common/d/a;

    move-result-object v0

    .line 318
    invoke-virtual {p0}, Lco/uk/getmondo/main/HomeActivity;->getSupportFragmentManager()Landroid/support/v4/app/n;

    move-result-object v1

    const-string v2, "TAG_ERROR_DIALOG_FRAGMENT"

    invoke-virtual {v0, v1, v2}, Lco/uk/getmondo/common/d/a;->show(Landroid/support/v4/app/n;Ljava/lang/String;)V

    .line 319
    return-void
.end method

.method public B()V
    .locals 3

    .prologue
    .line 322
    const v0, 0x7f0a0359

    invoke-virtual {p0, v0}, Lco/uk/getmondo/main/HomeActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    const v1, 0x7f0a0352

    invoke-virtual {p0, v1}, Lco/uk/getmondo/main/HomeActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lco/uk/getmondo/common/d/a;->a(Ljava/lang/String;Ljava/lang/String;Z)Lco/uk/getmondo/common/d/a;

    move-result-object v0

    .line 323
    invoke-virtual {p0}, Lco/uk/getmondo/main/HomeActivity;->getSupportFragmentManager()Landroid/support/v4/app/n;

    move-result-object v1

    const-string v2, "TAG_ERROR_DIALOG_FRAGMENT"

    invoke-virtual {v0, v1, v2}, Lco/uk/getmondo/common/d/a;->show(Landroid/support/v4/app/n;Ljava/lang/String;)V

    .line 324
    return-void
.end method

.method public C()V
    .locals 3

    .prologue
    .line 327
    invoke-static {}, Lco/uk/getmondo/common/d/e;->a()Lco/uk/getmondo/common/d/e;

    move-result-object v0

    invoke-virtual {p0}, Lco/uk/getmondo/main/HomeActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const-string v2, "TAG_ERROR_P2P_BLOCKED"

    invoke-virtual {v0, v1, v2}, Lco/uk/getmondo/common/d/e;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    .line 330
    sget v0, Lco/uk/getmondo/c$a;->bottomNavigationView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/main/HomeActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/design/widget/BottomNavigationView;

    new-instance v1, Lco/uk/getmondo/main/HomeActivity$m;

    invoke-direct {v1, p0}, Lco/uk/getmondo/main/HomeActivity$m;-><init>(Lco/uk/getmondo/main/HomeActivity;)V

    check-cast v1, Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/support/design/widget/BottomNavigationView;->post(Ljava/lang/Runnable;)Z

    .line 333
    return-void
.end method

.method public D()Lio/reactivex/n;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Lco/uk/getmondo/main/f;",
            ">;"
        }
    .end annotation

    .prologue
    .line 335
    iget-object v0, p0, Lco/uk/getmondo/main/HomeActivity;->g:Lcom/b/b/c;

    const-string v1, "monzoMeDataChangedSubject"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lio/reactivex/n;

    return-object v0
.end method

.method public E()V
    .locals 2

    .prologue
    .line 350
    sget v0, Lco/uk/getmondo/c$a;->outageBanner:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/main/HomeActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 351
    return-void
.end method

.method public F()V
    .locals 0

    .prologue
    .line 364
    invoke-virtual {p0}, Lco/uk/getmondo/main/HomeActivity;->s()V

    .line 365
    return-void
.end method

.method public G()V
    .locals 0

    .prologue
    .line 368
    invoke-virtual {p0}, Lco/uk/getmondo/main/HomeActivity;->t()V

    .line 369
    return-void
.end method

.method public H()V
    .locals 4

    .prologue
    .line 382
    sget-object v1, Lco/uk/getmondo/signup/identity_verification/VerificationPendingActivity;->c:Lco/uk/getmondo/signup/identity_verification/VerificationPendingActivity$a;

    move-object v0, p0

    check-cast v0, Landroid/content/Context;

    sget-object v2, Lco/uk/getmondo/signup/identity_verification/a/j;->b:Lco/uk/getmondo/signup/identity_verification/a/j;

    sget-object v3, Lco/uk/getmondo/api/model/tracking/Impression$KycFrom;->KYC_SDD_BLOCKED:Lco/uk/getmondo/api/model/tracking/Impression$KycFrom;

    invoke-virtual {v1, v0, v2, v3}, Lco/uk/getmondo/signup/identity_verification/VerificationPendingActivity$a;->a(Landroid/content/Context;Lco/uk/getmondo/signup/identity_verification/a/j;Lco/uk/getmondo/api/model/tracking/Impression$KycFrom;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lco/uk/getmondo/main/HomeActivity;->startActivity(Landroid/content/Intent;)V

    .line 383
    invoke-virtual {p0}, Lco/uk/getmondo/main/HomeActivity;->finishAffinity()V

    .line 384
    return-void
.end method

.method public I()Lcom/b/b/c;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/b/b/c",
            "<",
            "Lkotlin/n;",
            ">;"
        }
    .end annotation

    .prologue
    .line 386
    iget-object v0, p0, Lco/uk/getmondo/main/HomeActivity;->h:Lcom/b/b/c;

    return-object v0
.end method

.method public synthetic J()Lio/reactivex/n;
    .locals 1

    .prologue
    .line 63
    invoke-virtual {p0}, Lco/uk/getmondo/main/HomeActivity;->I()Lcom/b/b/c;

    move-result-object v0

    check-cast v0, Lio/reactivex/n;

    return-object v0
.end method

.method public K()Lio/reactivex/n;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Lkotlin/n;",
            ">;"
        }
    .end annotation

    .prologue
    .line 389
    sget v0, Lco/uk/getmondo/c$a;->migrationBanner:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/main/HomeActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/constraint/ConstraintLayout;

    .line 607
    invoke-static {v0}, Lcom/b/a/c/c;->a(Landroid/view/View;)Lio/reactivex/n;

    move-result-object v1

    sget-object v0, Lcom/b/a/a/d;->a:Lcom/b/a/a/d;

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {v1, v0}, Lio/reactivex/n;->map(Lio/reactivex/c/h;)Lio/reactivex/n;

    move-result-object v0

    const-string v1, "RxView.clicks(this).map(VoidToUnit)"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lio/reactivex/r;

    .line 389
    sget v1, Lco/uk/getmondo/c$a;->migrationBannerButton:I

    invoke-virtual {p0, v1}, Lco/uk/getmondo/main/HomeActivity;->a(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    .line 608
    invoke-static {v1}, Lcom/b/a/c/c;->a(Landroid/view/View;)Lio/reactivex/n;

    move-result-object v2

    sget-object v1, Lcom/b/a/a/d;->a:Lcom/b/a/a/d;

    check-cast v1, Lio/reactivex/c/h;

    invoke-virtual {v2, v1}, Lio/reactivex/n;->map(Lio/reactivex/c/h;)Lio/reactivex/n;

    move-result-object v1

    const-string v2, "RxView.clicks(this).map(VoidToUnit)"

    invoke-static {v1, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v1, Lio/reactivex/r;

    .line 389
    invoke-static {v0, v1}, Lio/reactivex/n;->merge(Lio/reactivex/r;Lio/reactivex/r;)Lio/reactivex/n;

    move-result-object v0

    const-string v1, "Observable.merge(migrati\u2026ionBannerButton.clicks())"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public L()V
    .locals 2

    .prologue
    .line 403
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lco/uk/getmondo/main/HomeActivity;->b(Z)V

    .line 405
    sget v0, Lco/uk/getmondo/c$a;->migrationBannerTitle:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/main/HomeActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const v1, 0x7f0a027a

    invoke-virtual {p0, v1}, Lco/uk/getmondo/main/HomeActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 406
    sget v0, Lco/uk/getmondo/c$a;->migrationBannerButton:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/main/HomeActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    const v1, 0x7f0a0275

    invoke-virtual {p0, v1}, Lco/uk/getmondo/main/HomeActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 407
    sget v0, Lco/uk/getmondo/c$a;->migrationBannerSubtitle:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/main/HomeActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-static {v0}, Lco/uk/getmondo/common/ae;->b(Landroid/view/View;)V

    .line 408
    sget v0, Lco/uk/getmondo/c$a;->migrationBanner:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/main/HomeActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/constraint/ConstraintLayout;

    invoke-static {v0}, Lco/uk/getmondo/common/ae;->a(Landroid/view/View;)V

    .line 409
    return-void
.end method

.method public M()V
    .locals 2

    .prologue
    .line 412
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lco/uk/getmondo/main/HomeActivity;->b(Z)V

    .line 414
    sget v0, Lco/uk/getmondo/c$a;->migrationBannerTitle:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/main/HomeActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const v1, 0x7f0a0278

    invoke-virtual {p0, v1}, Lco/uk/getmondo/main/HomeActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 415
    sget v0, Lco/uk/getmondo/c$a;->migrationBannerButton:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/main/HomeActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    const v1, 0x7f0a0279

    invoke-virtual {p0, v1}, Lco/uk/getmondo/main/HomeActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 416
    sget v0, Lco/uk/getmondo/c$a;->migrationBannerSubtitle:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/main/HomeActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-static {v0}, Lco/uk/getmondo/common/ae;->b(Landroid/view/View;)V

    .line 417
    sget v0, Lco/uk/getmondo/c$a;->migrationBanner:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/main/HomeActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/constraint/ConstraintLayout;

    invoke-static {v0}, Lco/uk/getmondo/common/ae;->a(Landroid/view/View;)V

    .line 418
    return-void
.end method

.method public N()V
    .locals 1

    .prologue
    .line 435
    sget v0, Lco/uk/getmondo/c$a;->migrationBanner:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/main/HomeActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/constraint/ConstraintLayout;

    invoke-static {v0}, Lco/uk/getmondo/common/ae;->b(Landroid/view/View;)V

    .line 436
    return-void
.end method

.method public O()V
    .locals 5

    .prologue
    .line 439
    sget-object v1, Lco/uk/getmondo/migration/MigrationAnnouncementActivity;->c:Lco/uk/getmondo/migration/MigrationAnnouncementActivity$a;

    move-object v0, p0

    .line 440
    check-cast v0, Landroid/content/Context;

    .line 441
    const v2, 0x7f0a027c

    invoke-virtual {p0, v2}, Lco/uk/getmondo/main/HomeActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    const-string v3, "getString(R.string.migra\u2026ent_account_coming_title)"

    invoke-static {v2, v3}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 442
    const v3, 0x7f0a027b

    invoke-virtual {p0, v3}, Lco/uk/getmondo/main/HomeActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    const-string v4, "getString(R.string.migra\u2026rent_account_coming_body)"

    invoke-static {v3, v4}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 443
    const/4 v4, 0x0

    .line 439
    invoke-virtual {v1, v0, v2, v3, v4}, Lco/uk/getmondo/migration/MigrationAnnouncementActivity$a;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lco/uk/getmondo/main/HomeActivity;->startActivity(Landroid/content/Intent;)V

    .line 445
    return-void
.end method

.method public P()V
    .locals 5

    .prologue
    .line 448
    sget-object v1, Lco/uk/getmondo/migration/MigrationAnnouncementActivity;->c:Lco/uk/getmondo/migration/MigrationAnnouncementActivity$a;

    move-object v0, p0

    .line 449
    check-cast v0, Landroid/content/Context;

    .line 450
    const v2, 0x7f0a027e

    invoke-virtual {p0, v2}, Lco/uk/getmondo/main/HomeActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    const-string v3, "getString(R.string.migra\u2026rrent_account_here_title)"

    invoke-static {v2, v3}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 451
    const v3, 0x7f0a027d

    invoke-virtual {p0, v3}, Lco/uk/getmondo/main/HomeActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    const-string v4, "getString(R.string.migra\u2026urrent_account_here_body)"

    invoke-static {v3, v4}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 452
    const/4 v4, 0x1

    .line 448
    invoke-virtual {v1, v0, v2, v3, v4}, Lco/uk/getmondo/migration/MigrationAnnouncementActivity$a;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lco/uk/getmondo/main/HomeActivity;->startActivity(Landroid/content/Intent;)V

    .line 454
    return-void
.end method

.method public Q()V
    .locals 3

    .prologue
    .line 457
    sget-object v1, Lco/uk/getmondo/signup/status/SignupStatusActivity;->c:Lco/uk/getmondo/signup/status/SignupStatusActivity$a;

    move-object v0, p0

    check-cast v0, Landroid/content/Context;

    sget-object v2, Lco/uk/getmondo/signup/j;->a:Lco/uk/getmondo/signup/j;

    invoke-virtual {v1, v0, v2}, Lco/uk/getmondo/signup/status/SignupStatusActivity$a;->a(Landroid/content/Context;Lco/uk/getmondo/signup/j;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lco/uk/getmondo/main/HomeActivity;->startActivity(Landroid/content/Intent;)V

    .line 458
    return-void
.end method

.method public a(I)Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lco/uk/getmondo/main/HomeActivity;->o:Ljava/util/HashMap;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lco/uk/getmondo/main/HomeActivity;->o:Ljava/util/HashMap;

    :cond_0
    iget-object v0, p0, Lco/uk/getmondo/main/HomeActivity;->o:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    if-nez v0, :cond_1

    invoke-virtual {p0, p1}, Landroid/support/v4/app/j;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/main/HomeActivity;->o:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    return-object v0
.end method

.method public a()Lio/reactivex/n;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Lkotlin/n;",
            ">;"
        }
    .end annotation

    .prologue
    .line 183
    sget v0, Lco/uk/getmondo/c$a;->ghostBannerLogoutButton:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/main/HomeActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 601
    invoke-static {v0}, Lcom/b/a/c/c;->a(Landroid/view/View;)Lio/reactivex/n;

    move-result-object v1

    sget-object v0, Lcom/b/a/a/d;->a:Lcom/b/a/a/d;

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {v1, v0}, Lio/reactivex/n;->map(Lio/reactivex/c/h;)Lio/reactivex/n;

    move-result-object v0

    const-string v1, "RxView.clicks(this).map(VoidToUnit)"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 183
    return-object v0
.end method

.method public final a(Landroid/support/v7/widget/Toolbar;)V
    .locals 6

    .prologue
    const-string v0, "toolbar"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 485
    iget-object v1, p0, Lco/uk/getmondo/main/HomeActivity;->m:Landroid/support/v7/app/b;

    if-eqz v1, :cond_0

    .line 486
    sget v0, Lco/uk/getmondo/c$a;->drawerLayout:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/main/HomeActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v4/widget/DrawerLayout;

    check-cast v1, Landroid/support/v4/widget/DrawerLayout$c;

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/DrawerLayout;->b(Landroid/support/v4/widget/DrawerLayout$c;)V

    .line 487
    nop

    .line 488
    :cond_0
    new-instance v0, Landroid/support/v7/app/b;

    move-object v1, p0

    .line 489
    check-cast v1, Landroid/app/Activity;

    .line 490
    sget v2, Lco/uk/getmondo/c$a;->drawerLayout:I

    invoke-virtual {p0, v2}, Lco/uk/getmondo/main/HomeActivity;->a(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/support/v4/widget/DrawerLayout;

    .line 492
    const v4, 0x7f0a02bc

    .line 493
    const v5, 0x7f0a02bb

    move-object v3, p1

    .line 488
    invoke-direct/range {v0 .. v5}, Landroid/support/v7/app/b;-><init>(Landroid/app/Activity;Landroid/support/v4/widget/DrawerLayout;Landroid/support/v7/widget/Toolbar;II)V

    iput-object v0, p0, Lco/uk/getmondo/main/HomeActivity;->m:Landroid/support/v7/app/b;

    .line 496
    iget-object v2, p0, Lco/uk/getmondo/main/HomeActivity;->m:Landroid/support/v7/app/b;

    if-eqz v2, :cond_1

    .line 497
    sget v0, Lco/uk/getmondo/c$a;->drawerLayout:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/main/HomeActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v4/widget/DrawerLayout;

    move-object v1, v2

    check-cast v1, Landroid/support/v4/widget/DrawerLayout$c;

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/DrawerLayout;->a(Landroid/support/v4/widget/DrawerLayout$c;)V

    .line 498
    invoke-virtual {v2}, Landroid/support/v7/app/b;->a()V

    .line 499
    nop

    .line 500
    :cond_1
    return-void
.end method

.method public a(Lco/uk/getmondo/api/model/service_status/ServiceStatusIncident;)V
    .locals 2

    .prologue
    const-string v0, "incident"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 338
    sget v0, Lco/uk/getmondo/c$a;->outageBanner:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/main/HomeActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 340
    sget v0, Lco/uk/getmondo/c$a;->outageBannerTextView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/main/HomeActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {p1}, Lco/uk/getmondo/api/model/service_status/ServiceStatusIncident;->b()Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 341
    sget v0, Lco/uk/getmondo/c$a;->outageBannerButton:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/main/HomeActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    new-instance v1, Lco/uk/getmondo/main/HomeActivity$l;

    invoke-direct {v1, p0, p1}, Lco/uk/getmondo/main/HomeActivity$l;-><init>(Lco/uk/getmondo/main/HomeActivity;Lco/uk/getmondo/api/model/service_status/ServiceStatusIncident;)V

    check-cast v1, Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 347
    return-void
.end method

.method public a(Lco/uk/getmondo/main/g;)V
    .locals 4

    .prologue
    const-string v0, "screen"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 277
    iget-object v0, p0, Lco/uk/getmondo/main/HomeActivity;->n:Lco/uk/getmondo/main/g;

    invoke-static {p1, v0}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 300
    :goto_0
    return-void

    .line 281
    :cond_0
    iget-boolean v0, p0, Lco/uk/getmondo/main/HomeActivity;->k:Z

    if-nez v0, :cond_2

    .line 282
    invoke-virtual {p1}, Lco/uk/getmondo/main/g;->a()Landroid/support/v4/app/Fragment;

    move-result-object v1

    .line 283
    iget-object v0, p0, Lco/uk/getmondo/main/HomeActivity;->l:Lorg/threeten/bp/YearMonth;

    if-eqz v0, :cond_1

    sget-object v0, Lco/uk/getmondo/main/g;->b:Lco/uk/getmondo/main/g;

    invoke-static {p1, v0}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 284
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 285
    const-string v3, "KEY_YEAR_MONTH"

    iget-object v0, p0, Lco/uk/getmondo/main/HomeActivity;->l:Lorg/threeten/bp/YearMonth;

    check-cast v0, Ljava/io/Serializable;

    invoke-virtual {v2, v3, v0}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 286
    invoke-virtual {v1, v2}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 287
    const/4 v0, 0x0

    check-cast v0, Lorg/threeten/bp/YearMonth;

    iput-object v0, p0, Lco/uk/getmondo/main/HomeActivity;->l:Lorg/threeten/bp/YearMonth;

    .line 289
    :cond_1
    invoke-virtual {p0}, Lco/uk/getmondo/main/HomeActivity;->getSupportFragmentManager()Landroid/support/v4/app/n;

    move-result-object v0

    .line 290
    invoke-virtual {v0}, Landroid/support/v4/app/n;->a()Landroid/support/v4/app/t;

    move-result-object v0

    .line 291
    const v2, 0x7f11019a

    invoke-virtual {v0, v2, v1}, Landroid/support/v4/app/t;->b(ILandroid/support/v4/app/Fragment;)Landroid/support/v4/app/t;

    move-result-object v0

    .line 292
    invoke-virtual {v0}, Landroid/support/v4/app/t;->c()I

    .line 295
    :cond_2
    iput-object p1, p0, Lco/uk/getmondo/main/HomeActivity;->n:Lco/uk/getmondo/main/g;

    .line 296
    const/4 v0, 0x0

    iput-boolean v0, p0, Lco/uk/getmondo/main/HomeActivity;->k:Z

    .line 299
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lco/uk/getmondo/main/HomeActivity;->a(Z)V

    goto :goto_0
.end method

.method public a(Lco/uk/getmondo/news/c;)V
    .locals 1

    .prologue
    const-string v0, "newsItem"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v0, p0

    .line 360
    check-cast v0, Landroid/content/Context;

    invoke-static {v0, p1}, Lco/uk/getmondo/news/NewsActivity;->a(Landroid/content/Context;Lco/uk/getmondo/news/c;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lco/uk/getmondo/main/HomeActivity;->startActivity(Landroid/content/Intent;)V

    .line 361
    return-void
.end method

.method public a(Lco/uk/getmondo/payments/send/data/a/g;)V
    .locals 2

    .prologue
    const-string v0, "pendingPayment"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v0, p0

    .line 217
    check-cast v0, Landroid/content/Context;

    sget-object v1, Lco/uk/getmondo/api/model/tracking/Impression$PaymentFlowFrom;->FROM_MONZO_ME:Lco/uk/getmondo/api/model/tracking/Impression$PaymentFlowFrom;

    invoke-static {v0, p1, v1}, Lco/uk/getmondo/payments/send/peer/PeerPaymentActivity;->a(Landroid/content/Context;Lco/uk/getmondo/payments/send/data/a/g;Lco/uk/getmondo/api/model/tracking/Impression$PaymentFlowFrom;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lco/uk/getmondo/main/HomeActivity;->startActivity(Landroid/content/Intent;)V

    .line 218
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 3

    .prologue
    const-string v0, "email"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 354
    invoke-virtual {p0}, Lco/uk/getmondo/main/HomeActivity;->getWindow()Landroid/view/Window;

    move-result-object v1

    move-object v0, p0

    check-cast v0, Landroid/content/Context;

    const v2, 0x7f0f00f1

    invoke-static {v0, v2}, Landroid/support/v4/content/a;->c(Landroid/content/Context;I)I

    move-result v0

    invoke-virtual {v1, v0}, Landroid/view/Window;->setStatusBarColor(I)V

    .line 355
    sget v0, Lco/uk/getmondo/c$a;->ghostBannerTextView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/main/HomeActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Lco/uk/getmondo/common/k/e;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/16 v2, 0x20

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 356
    sget v0, Lco/uk/getmondo/c$a;->ghostBanner:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/main/HomeActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 357
    return-void
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 6

    .prologue
    const/4 v2, 0x0

    const/4 v3, 0x0

    const-string v0, "emailAddress"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 192
    invoke-direct {p0}, Lco/uk/getmondo/main/HomeActivity;->T()Landroid/view/View;

    move-result-object v0

    sget v1, Lco/uk/getmondo/c$a;->nameTextView:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    move-object v1, p1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 193
    invoke-direct {p0}, Lco/uk/getmondo/main/HomeActivity;->T()Landroid/view/View;

    move-result-object v0

    sget v1, Lco/uk/getmondo/c$a;->emailTextView:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    check-cast p2, Ljava/lang/CharSequence;

    invoke-virtual {v0, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 194
    if-eqz p1, :cond_0

    move-object v0, p1

    check-cast v0, Ljava/lang/CharSequence;

    invoke-static {v0}, Lkotlin/h/j;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_0

    .line 195
    invoke-direct {p0}, Lco/uk/getmondo/main/HomeActivity;->R()Lco/uk/getmondo/common/ui/a;

    move-result-object v0

    invoke-virtual {v0, p1}, Lco/uk/getmondo/common/ui/a;->a(Ljava/lang/String;)Lco/uk/getmondo/common/ui/a$b;

    move-result-object v0

    invoke-direct {p0}, Lco/uk/getmondo/main/HomeActivity;->S()I

    move-result v1

    const/4 v4, 0x6

    move-object v5, v2

    invoke-static/range {v0 .. v5}, Lco/uk/getmondo/common/ui/a$b;->a(Lco/uk/getmondo/common/ui/a$b;ILandroid/graphics/Typeface;ZILjava/lang/Object;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 196
    invoke-direct {p0}, Lco/uk/getmondo/main/HomeActivity;->T()Landroid/view/View;

    move-result-object v0

    sget v2, Lco/uk/getmondo/c$a;->avatarImageView:I

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 198
    :cond_0
    return-void

    :cond_1
    move v0, v3

    .line 194
    goto :goto_0
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 2

    .prologue
    const-string v0, "title"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "subtitle"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 392
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lco/uk/getmondo/main/HomeActivity;->b(Z)V

    .line 394
    sget v0, Lco/uk/getmondo/c$a;->migrationBannerTitle:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/main/HomeActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    check-cast p1, Ljava/lang/CharSequence;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 395
    sget v0, Lco/uk/getmondo/c$a;->migrationBannerSubtitle:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/main/HomeActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    check-cast p2, Ljava/lang/CharSequence;

    invoke-virtual {v0, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 396
    if-eqz p3, :cond_0

    const v0, 0x7f0a0277

    move v1, v0

    .line 397
    :goto_0
    sget v0, Lco/uk/getmondo/c$a;->migrationBannerButton:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/main/HomeActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    invoke-virtual {p0, v1}, Lco/uk/getmondo/main/HomeActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 398
    sget v0, Lco/uk/getmondo/c$a;->migrationBannerSubtitle:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/main/HomeActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-static {v0}, Lco/uk/getmondo/common/ae;->a(Landroid/view/View;)V

    .line 399
    sget v0, Lco/uk/getmondo/c$a;->migrationBanner:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/main/HomeActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/constraint/ConstraintLayout;

    invoke-static {v0}, Lco/uk/getmondo/common/ae;->a(Landroid/view/View;)V

    .line 400
    return-void

    .line 396
    :cond_0
    const v0, 0x7f0a0276

    move v1, v0

    goto :goto_0
.end method

.method public final a(Z)V
    .locals 2

    .prologue
    .line 503
    sget v0, Lco/uk/getmondo/c$a;->bottomNavShadow:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/main/HomeActivity;->a(I)Landroid/view/View;

    move-result-object v1

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 504
    return-void

    .line 503
    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public b()Lio/reactivex/n;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Lkotlin/n;",
            ">;"
        }
    .end annotation

    .prologue
    .line 185
    iget-object v0, p0, Lco/uk/getmondo/main/HomeActivity;->h:Lcom/b/b/c;

    const-string v1, "startSubject"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lio/reactivex/n;

    return-object v0
.end method

.method public b(I)V
    .locals 2

    .prologue
    .line 461
    invoke-direct {p0}, Lco/uk/getmondo/main/HomeActivity;->V()Lco/uk/getmondo/common/f/a;

    move-result-object v0

    .line 462
    if-eqz v0, :cond_0

    .line 463
    invoke-virtual {v0, p1}, Lco/uk/getmondo/common/f/a;->b(I)V

    .line 467
    :goto_0
    return-void

    .line 465
    :cond_0
    invoke-super {p0, p1}, Lco/uk/getmondo/common/activities/b;->b(I)V

    .line 466
    const-string v0, "Tried to show error but couldn\'t find current fragment"

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Ld/a/a;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public b(Lco/uk/getmondo/payments/send/data/a/g;)V
    .locals 2

    .prologue
    const-string v0, "pendingPayment"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v0, p0

    .line 221
    check-cast v0, Landroid/content/Context;

    sget-object v1, Lco/uk/getmondo/common/t;->a:Lco/uk/getmondo/common/t;

    invoke-static {v0, v1, p1}, Lco/uk/getmondo/payments/send/onboarding/PeerToPeerIntroActivity;->a(Landroid/content/Context;Lco/uk/getmondo/common/t;Lco/uk/getmondo/payments/send/data/a/g;)Landroid/content/Intent;

    move-result-object v0

    .line 222
    const/4 v1, 0x1

    .line 221
    invoke-virtual {p0, v0, v1}, Lco/uk/getmondo/main/HomeActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 223
    return-void
.end method

.method public b(Ljava/lang/String;)V
    .locals 2

    .prologue
    const-string v0, "message"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 471
    invoke-direct {p0}, Lco/uk/getmondo/main/HomeActivity;->V()Lco/uk/getmondo/common/f/a;

    move-result-object v0

    .line 472
    if-eqz v0, :cond_0

    .line 473
    invoke-virtual {v0, p1}, Lco/uk/getmondo/common/f/a;->d(Ljava/lang/String;)V

    .line 477
    :goto_0
    return-void

    .line 475
    :cond_0
    invoke-super {p0, p1}, Lco/uk/getmondo/common/activities/b;->b(Ljava/lang/String;)V

    .line 476
    const-string v0, "Tried to show error but couldn\'t find current fragment"

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Ld/a/a;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public b(Ljava/lang/String;Ljava/lang/String;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    const-string v0, "sortCode"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "accountNumber"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 201
    invoke-direct {p0}, Lco/uk/getmondo/main/HomeActivity;->T()Landroid/view/View;

    move-result-object v0

    sget v1, Lco/uk/getmondo/c$a;->accountTextView:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    sget-object v1, Lkotlin/d/b/ab;->a:Lkotlin/d/b/ab;

    const-string v1, "%s  \u2022  %s"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    aput-object p1, v2, v5

    aput-object p2, v2, v4

    array-length v3, v2

    invoke-static {v2, v3}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v2

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "java.lang.String.format(format, *args)"

    invoke-static {v1, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 202
    invoke-direct {p0}, Lco/uk/getmondo/main/HomeActivity;->T()Landroid/view/View;

    move-result-object v0

    sget v1, Lco/uk/getmondo/c$a;->accountTextView:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 205
    sget v0, Lco/uk/getmondo/c$a;->bottomNavigationView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/main/HomeActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/design/widget/BottomNavigationView;

    invoke-virtual {v0}, Landroid/support/design/widget/BottomNavigationView;->getMenu()Landroid/view/Menu;

    move-result-object v0

    const v1, 0x7f1104cf

    invoke-interface {v0, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    const v1, 0x7f0a02b9

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setTitle(I)Landroid/view/MenuItem;

    .line 207
    sget v0, Lco/uk/getmondo/c$a;->navigationDrawer:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/main/HomeActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/design/widget/NavigationView;

    invoke-virtual {v0}, Landroid/support/design/widget/NavigationView;->getMenu()Landroid/view/Menu;

    move-result-object v0

    const v1, 0x7f1104ca

    invoke-interface {v0, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 208
    const v1, 0x7f0a02b8

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setTitle(I)Landroid/view/MenuItem;

    .line 209
    const v1, 0x7f020165

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    .line 211
    sget v0, Lco/uk/getmondo/c$a;->navigationDrawer:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/main/HomeActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/design/widget/NavigationView;

    invoke-virtual {v0}, Landroid/support/design/widget/NavigationView;->getMenu()Landroid/view/Menu;

    move-result-object v0

    const v1, 0x7f1104cb

    invoke-interface {v0, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 212
    invoke-interface {v0, v4}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 213
    invoke-interface {v0, v4}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 214
    return-void
.end method

.method public c()V
    .locals 2

    .prologue
    .line 188
    sget-object v1, Lco/uk/getmondo/help/HelpActivity;->c:Lco/uk/getmondo/help/HelpActivity$a;

    move-object v0, p0

    check-cast v0, Landroid/content/Context;

    invoke-virtual {v1, v0}, Lco/uk/getmondo/help/HelpActivity$a;->a(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lco/uk/getmondo/main/HomeActivity;->startActivity(Landroid/content/Intent;)V

    .line 189
    return-void
.end method

.method public d()V
    .locals 2

    .prologue
    .line 226
    move-object v0, p0

    check-cast v0, Landroid/content/Context;

    sget-object v1, Lco/uk/getmondo/common/t;->b:Lco/uk/getmondo/common/t;

    invoke-static {v0, v1}, Lco/uk/getmondo/payments/send/onboarding/PeerToPeerIntroActivity;->a(Landroid/content/Context;Lco/uk/getmondo/common/t;)Landroid/content/Intent;

    move-result-object v0

    .line 227
    const/4 v1, 0x1

    .line 226
    invoke-virtual {p0, v0, v1}, Lco/uk/getmondo/main/HomeActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 228
    return-void
.end method

.method public d(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 372
    move-object v0, p0

    check-cast v0, Landroid/content/Context;

    sget-object v1, Lco/uk/getmondo/signup/identity_verification/sdd/j;->b:Lco/uk/getmondo/signup/identity_verification/sdd/j;

    invoke-static {v0, v1, p1}, Lco/uk/getmondo/signup/identity_verification/sdd/IdentityVerificationSddActivity;->a(Landroid/content/Context;Lco/uk/getmondo/signup/identity_verification/sdd/j;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lco/uk/getmondo/main/HomeActivity;->startActivity(Landroid/content/Intent;)V

    .line 373
    return-void
.end method

.method public e()V
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 231
    const-string v1, "https://community.monzo.com"

    const/4 v4, 0x6

    const/4 v5, 0x0

    move-object v0, p0

    move v3, v2

    invoke-static/range {v0 .. v5}, Lco/uk/getmondo/common/activities/a;->a(Landroid/app/Activity;Ljava/lang/String;IZILjava/lang/Object;)V

    .line 232
    return-void
.end method

.method public e(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 376
    move-object v0, p0

    check-cast v0, Landroid/content/Context;

    sget-object v1, Lco/uk/getmondo/signup/identity_verification/sdd/j;->c:Lco/uk/getmondo/signup/identity_verification/sdd/j;

    invoke-static {v0, v1, p1}, Lco/uk/getmondo/signup/identity_verification/sdd/IdentityVerificationSddActivity;->a(Landroid/content/Context;Lco/uk/getmondo/signup/identity_verification/sdd/j;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 377
    const v1, 0x10008000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    move-result-object v0

    .line 378
    invoke-virtual {p0, v0}, Lco/uk/getmondo/main/HomeActivity;->startActivity(Landroid/content/Intent;)V

    .line 379
    return-void
.end method

.method public f()V
    .locals 3

    .prologue
    .line 235
    new-instance v1, Landroid/support/b/a$a;

    invoke-direct {v1}, Landroid/support/b/a$a;-><init>()V

    move-object v0, p0

    .line 236
    check-cast v0, Landroid/content/Context;

    const v2, 0x7f0f000f

    invoke-static {v0, v2}, Landroid/support/v4/content/a;->c(Landroid/content/Context;I)I

    move-result v0

    invoke-virtual {v1, v0}, Landroid/support/b/a$a;->a(I)Landroid/support/b/a$a;

    move-result-object v0

    .line 237
    invoke-virtual {v0}, Landroid/support/b/a$a;->a()Landroid/support/b/a;

    move-result-object v0

    .line 238
    check-cast p0, Landroid/content/Context;

    const-string v1, "https://monzo.com/faq/in-app/android/"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, p0, v1}, Landroid/support/b/a;->a(Landroid/content/Context;Landroid/net/Uri;)V

    .line 239
    return-void
.end method

.method public g()V
    .locals 2

    .prologue
    .line 242
    iget-object v0, p0, Lco/uk/getmondo/main/HomeActivity;->c:Lco/uk/getmondo/common/q;

    if-nez v0, :cond_0

    const-string v1, "intercomService"

    invoke-static {v1}, Lkotlin/d/b/l;->b(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0}, Lco/uk/getmondo/common/q;->a()V

    .line 243
    return-void
.end method

.method public h()Lio/reactivex/n;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Lkotlin/n;",
            ">;"
        }
    .end annotation

    .prologue
    .line 245
    iget-object v0, p0, Lco/uk/getmondo/main/HomeActivity;->h:Lcom/b/b/c;

    const-string v1, "startSubject"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lio/reactivex/n;

    return-object v0
.end method

.method public i()Lio/reactivex/n;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Lco/uk/getmondo/main/g;",
            ">;"
        }
    .end annotation

    .prologue
    .line 248
    sget v0, Lco/uk/getmondo/c$a;->bottomNavigationView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/main/HomeActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/design/widget/BottomNavigationView;

    .line 602
    invoke-static {v0}, Lcom/b/a/b/a/a/b;->a(Landroid/support/design/widget/BottomNavigationView;)Lio/reactivex/n;

    move-result-object v1

    const-string v0, "RxBottomNavigationView.itemSelections(this)"

    invoke-static {v1, v0}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 249
    sget-object v0, Lco/uk/getmondo/main/HomeActivity$i;->a:Lco/uk/getmondo/main/HomeActivity$i;

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {v1, v0}, Lio/reactivex/n;->map(Lio/reactivex/c/h;)Lio/reactivex/n;

    move-result-object v0

    const-string v1, "bottomNavigationView.ite\u2026urceId(menuItem.itemId) }"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public j()Lio/reactivex/n;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Lkotlin/n;",
            ">;"
        }
    .end annotation

    .prologue
    .line 253
    sget v0, Lco/uk/getmondo/c$a;->navigationDrawer:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/main/HomeActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/design/widget/NavigationView;

    invoke-virtual {v0}, Landroid/support/design/widget/NavigationView;->getMenu()Landroid/view/Menu;

    move-result-object v0

    const v1, 0x7f1104c8

    invoke-interface {v0, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 603
    invoke-static {v0}, Lcom/b/a/c/b;->a(Landroid/view/MenuItem;)Lio/reactivex/n;

    move-result-object v1

    sget-object v0, Lcom/b/a/a/d;->a:Lcom/b/a/a/d;

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {v1, v0}, Lio/reactivex/n;->map(Lio/reactivex/c/h;)Lio/reactivex/n;

    move-result-object v1

    const-string v0, "RxMenuItem.clicks(this).map(VoidToUnit)"

    invoke-static {v1, v0}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 254
    new-instance v0, Lco/uk/getmondo/main/HomeActivity$j;

    invoke-direct {v0, p0}, Lco/uk/getmondo/main/HomeActivity$j;-><init>(Lco/uk/getmondo/main/HomeActivity;)V

    check-cast v0, Lio/reactivex/c/g;

    invoke-virtual {v1, v0}, Lio/reactivex/n;->doOnNext(Lio/reactivex/c/g;)Lio/reactivex/n;

    move-result-object v0

    const-string v1, "navigationDrawer.menu.fi\u2026oOnNext { closeDrawer() }"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public k()Lio/reactivex/n;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Lkotlin/n;",
            ">;"
        }
    .end annotation

    .prologue
    .line 258
    sget v0, Lco/uk/getmondo/c$a;->navigationDrawer:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/main/HomeActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/design/widget/NavigationView;

    invoke-virtual {v0}, Landroid/support/design/widget/NavigationView;->getMenu()Landroid/view/Menu;

    move-result-object v0

    const v1, 0x7f1104c9

    invoke-interface {v0, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 604
    invoke-static {v0}, Lcom/b/a/c/b;->a(Landroid/view/MenuItem;)Lio/reactivex/n;

    move-result-object v1

    sget-object v0, Lcom/b/a/a/d;->a:Lcom/b/a/a/d;

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {v1, v0}, Lio/reactivex/n;->map(Lio/reactivex/c/h;)Lio/reactivex/n;

    move-result-object v1

    const-string v0, "RxMenuItem.clicks(this).map(VoidToUnit)"

    invoke-static {v1, v0}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 259
    new-instance v0, Lco/uk/getmondo/main/HomeActivity$g;

    invoke-direct {v0, p0}, Lco/uk/getmondo/main/HomeActivity$g;-><init>(Lco/uk/getmondo/main/HomeActivity;)V

    check-cast v0, Lio/reactivex/c/g;

    invoke-virtual {v1, v0}, Lio/reactivex/n;->doOnNext(Lio/reactivex/c/g;)Lio/reactivex/n;

    move-result-object v0

    const-string v1, "navigationDrawer.menu.fi\u2026oOnNext { closeDrawer() }"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public m()Landroid/view/View;
    .locals 1

    .prologue
    .line 172
    const v0, 0x7f1102f7

    invoke-virtual {p0, v0}, Lco/uk/getmondo/main/HomeActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    const v0, 0x7f11019a

    invoke-virtual {p0, v0}, Lco/uk/getmondo/main/HomeActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    goto :goto_0
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 2

    .prologue
    .line 150
    packed-switch p1, :pswitch_data_0

    .line 167
    invoke-super {p0, p1, p2, p3}, Lco/uk/getmondo/common/activities/b;->onActivityResult(IILandroid/content/Intent;)V

    .line 168
    :cond_0
    :goto_0
    return-void

    .line 152
    :pswitch_0
    if-nez p2, :cond_1

    .line 154
    sget v0, Lco/uk/getmondo/c$a;->bottomNavigationView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/main/HomeActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/design/widget/BottomNavigationView;

    new-instance v1, Lco/uk/getmondo/main/HomeActivity$d;

    invoke-direct {v1, p0}, Lco/uk/getmondo/main/HomeActivity$d;-><init>(Lco/uk/getmondo/main/HomeActivity;)V

    check-cast v1, Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/support/design/widget/BottomNavigationView;->post(Ljava/lang/Runnable;)Z

    goto :goto_0

    .line 158
    :cond_1
    invoke-static {p3}, Lco/uk/getmondo/payments/send/onboarding/PeerToPeerIntroActivity;->a(Landroid/content/Intent;)Lco/uk/getmondo/common/t;

    move-result-object v0

    .line 159
    sget-object v1, Lco/uk/getmondo/common/t;->b:Lco/uk/getmondo/common/t;

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 162
    sget v0, Lco/uk/getmondo/c$a;->bottomNavigationView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/main/HomeActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/design/widget/BottomNavigationView;

    new-instance v1, Lco/uk/getmondo/main/HomeActivity$e;

    invoke-direct {v1, p0}, Lco/uk/getmondo/main/HomeActivity$e;-><init>(Lco/uk/getmondo/main/HomeActivity;)V

    check-cast v1, Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/support/design/widget/BottomNavigationView;->post(Ljava/lang/Runnable;)Z

    goto :goto_0

    .line 163
    :cond_2
    sget-object v1, Lco/uk/getmondo/common/t;->a:Lco/uk/getmondo/common/t;

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 164
    invoke-static {p3}, Lco/uk/getmondo/payments/send/onboarding/PeerToPeerIntroActivity;->b(Landroid/content/Intent;)Lco/uk/getmondo/payments/send/data/a/g;

    move-result-object v0

    if-nez v0, :cond_3

    invoke-static {}, Lkotlin/d/b/l;->a()V

    :cond_3
    const-string v1, "PeerToPeerIntroActivity.getPendingPayment(data)!!"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lco/uk/getmondo/main/HomeActivity;->a(Lco/uk/getmondo/payments/send/data/a/g;)V

    goto :goto_0

    .line 150
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public onBackPressed()V
    .locals 2

    .prologue
    const v1, 0x800003

    .line 176
    sget v0, Lco/uk/getmondo/c$a;->drawerLayout:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/main/HomeActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v4/widget/DrawerLayout;

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/DrawerLayout;->g(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 177
    sget v0, Lco/uk/getmondo/c$a;->drawerLayout:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/main/HomeActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v4/widget/DrawerLayout;

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/DrawerLayout;->f(I)V

    .line 180
    :goto_0
    return-void

    .line 179
    :cond_0
    invoke-super {p0}, Lco/uk/getmondo/common/activities/b;->onBackPressed()V

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 83
    invoke-super {p0, p1}, Lco/uk/getmondo/common/activities/b;->onCreate(Landroid/os/Bundle;)V

    .line 85
    const v0, 0x7f050041

    invoke-virtual {p0, v0}, Lco/uk/getmondo/main/HomeActivity;->setContentView(I)V

    .line 86
    invoke-virtual {p0}, Lco/uk/getmondo/main/HomeActivity;->l()Lco/uk/getmondo/common/h/b/b;

    move-result-object v0

    invoke-interface {v0, p0}, Lco/uk/getmondo/common/h/b/b;->a(Lco/uk/getmondo/main/HomeActivity;)V

    .line 88
    if-eqz p1, :cond_2

    .line 89
    const/4 v0, 0x1

    iput-boolean v0, p0, Lco/uk/getmondo/main/HomeActivity;->k:Z

    .line 90
    const-string v0, "KEY_CURRENT_SCREEN"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    if-nez v0, :cond_0

    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type co.uk.getmondo.main.Screen"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    check-cast v0, Lco/uk/getmondo/main/g;

    .line 91
    sget v1, Lco/uk/getmondo/c$a;->bottomNavigationView:I

    invoke-virtual {p0, v1}, Lco/uk/getmondo/main/HomeActivity;->a(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/support/design/widget/BottomNavigationView;

    iget v0, v0, Lco/uk/getmondo/main/g;->f:I

    invoke-virtual {v1, v0}, Landroid/support/design/widget/BottomNavigationView;->setSelectedItemId(I)V

    .line 94
    :goto_0
    sget-object v1, Lco/uk/getmondo/main/HomeActivity;->f:Lco/uk/getmondo/main/HomeActivity$a;

    sget v0, Lco/uk/getmondo/c$a;->bottomNavigationView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/main/HomeActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/design/widget/BottomNavigationView;

    const-string v2, "bottomNavigationView"

    invoke-static {v0, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v1, v0}, Lco/uk/getmondo/main/HomeActivity$a;->a(Lco/uk/getmondo/main/HomeActivity$a;Landroid/support/design/widget/BottomNavigationView;)V

    .line 97
    iget-object v0, p0, Lco/uk/getmondo/main/HomeActivity;->b:Lco/uk/getmondo/main/c;

    if-nez v0, :cond_1

    const-string v1, "homePresenter"

    invoke-static {v1}, Lkotlin/d/b/l;->b(Ljava/lang/String;)V

    :cond_1
    check-cast p0, Lco/uk/getmondo/main/c$a;

    invoke-virtual {v0, p0}, Lco/uk/getmondo/main/c;->a(Lco/uk/getmondo/main/c$a;)V

    .line 98
    return-void

    .line 93
    :cond_2
    invoke-virtual {p0}, Lco/uk/getmondo/main/HomeActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "intent"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lco/uk/getmondo/main/HomeActivity;->a(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    .line 140
    iget-object v0, p0, Lco/uk/getmondo/main/HomeActivity;->b:Lco/uk/getmondo/main/c;

    if-nez v0, :cond_0

    const-string v1, "homePresenter"

    invoke-static {v1}, Lkotlin/d/b/l;->b(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0}, Lco/uk/getmondo/main/c;->b()V

    .line 141
    invoke-super {p0}, Lco/uk/getmondo/common/activities/b;->onDestroy()V

    .line 142
    return-void
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 1

    .prologue
    const-string v0, "intent"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 101
    invoke-super {p0, p1}, Lco/uk/getmondo/common/activities/b;->onNewIntent(Landroid/content/Intent;)V

    .line 103
    invoke-virtual {p0, p1}, Lco/uk/getmondo/main/HomeActivity;->setIntent(Landroid/content/Intent;)V

    .line 104
    invoke-direct {p0, p1}, Lco/uk/getmondo/main/HomeActivity;->a(Landroid/content/Intent;)V

    .line 105
    return-void
.end method

.method protected onResume()V
    .locals 2

    .prologue
    .line 130
    invoke-super {p0}, Lco/uk/getmondo/common/activities/b;->onResume()V

    .line 132
    invoke-virtual {p0}, Lco/uk/getmondo/main/HomeActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "KEY_MONZO_ME_DATA"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 133
    invoke-virtual {p0}, Lco/uk/getmondo/main/HomeActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "KEY_MONZO_ME_DATA"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/main/f;

    .line 134
    iget-object v1, p0, Lco/uk/getmondo/main/HomeActivity;->g:Lcom/b/b/c;

    invoke-virtual {v1, v0}, Lcom/b/b/c;->a(Ljava/lang/Object;)V

    .line 135
    invoke-virtual {p0}, Lco/uk/getmondo/main/HomeActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "KEY_MONZO_ME_DATA"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->removeExtra(Ljava/lang/String;)V

    .line 137
    :cond_0
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    const-string v0, "outState"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 145
    invoke-super {p0, p1}, Lco/uk/getmondo/common/activities/b;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 146
    const-string v1, "KEY_CURRENT_SCREEN"

    iget-object v0, p0, Lco/uk/getmondo/main/HomeActivity;->n:Lco/uk/getmondo/main/g;

    check-cast v0, Ljava/io/Serializable;

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 147
    return-void
.end method

.method protected onStart()V
    .locals 2

    .prologue
    .line 125
    invoke-super {p0}, Lco/uk/getmondo/common/activities/b;->onStart()V

    .line 126
    iget-object v0, p0, Lco/uk/getmondo/main/HomeActivity;->h:Lcom/b/b/c;

    sget-object v1, Lkotlin/n;->a:Lkotlin/n;

    invoke-virtual {v0, v1}, Lcom/b/b/c;->a(Ljava/lang/Object;)V

    .line 127
    return-void
.end method

.method public v()Lio/reactivex/n;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Lkotlin/n;",
            ">;"
        }
    .end annotation

    .prologue
    .line 263
    sget v0, Lco/uk/getmondo/c$a;->navigationDrawer:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/main/HomeActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/design/widget/NavigationView;

    invoke-virtual {v0}, Landroid/support/design/widget/NavigationView;->getMenu()Landroid/view/Menu;

    move-result-object v0

    const v1, 0x7f1104ca

    invoke-interface {v0, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 605
    invoke-static {v0}, Lcom/b/a/c/b;->a(Landroid/view/MenuItem;)Lio/reactivex/n;

    move-result-object v1

    sget-object v0, Lcom/b/a/a/d;->a:Lcom/b/a/a/d;

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {v1, v0}, Lio/reactivex/n;->map(Lio/reactivex/c/h;)Lio/reactivex/n;

    move-result-object v1

    const-string v0, "RxMenuItem.clicks(this).map(VoidToUnit)"

    invoke-static {v1, v0}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 264
    new-instance v0, Lco/uk/getmondo/main/HomeActivity$h;

    invoke-direct {v0, p0}, Lco/uk/getmondo/main/HomeActivity$h;-><init>(Lco/uk/getmondo/main/HomeActivity;)V

    check-cast v0, Lio/reactivex/c/g;

    invoke-virtual {v1, v0}, Lio/reactivex/n;->doOnNext(Lio/reactivex/c/g;)Lio/reactivex/n;

    move-result-object v0

    const-string v1, "navigationDrawer.menu.fi\u2026oOnNext { closeDrawer() }"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public w()Lio/reactivex/n;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Lkotlin/n;",
            ">;"
        }
    .end annotation

    .prologue
    .line 268
    sget v0, Lco/uk/getmondo/c$a;->navigationDrawer:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/main/HomeActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/design/widget/NavigationView;

    invoke-virtual {v0}, Landroid/support/design/widget/NavigationView;->getMenu()Landroid/view/Menu;

    move-result-object v0

    const v1, 0x7f1104cb

    invoke-interface {v0, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 606
    invoke-static {v0}, Lcom/b/a/c/b;->a(Landroid/view/MenuItem;)Lio/reactivex/n;

    move-result-object v1

    sget-object v0, Lcom/b/a/a/d;->a:Lcom/b/a/a/d;

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {v1, v0}, Lio/reactivex/n;->map(Lio/reactivex/c/h;)Lio/reactivex/n;

    move-result-object v1

    const-string v0, "RxMenuItem.clicks(this).map(VoidToUnit)"

    invoke-static {v1, v0}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 269
    new-instance v0, Lco/uk/getmondo/main/HomeActivity$f;

    invoke-direct {v0, p0}, Lco/uk/getmondo/main/HomeActivity$f;-><init>(Lco/uk/getmondo/main/HomeActivity;)V

    check-cast v0, Lio/reactivex/c/g;

    invoke-virtual {v1, v0}, Lio/reactivex/n;->doOnNext(Lio/reactivex/c/g;)Lio/reactivex/n;

    move-result-object v0

    const-string v1, "navigationDrawer.menu.fi\u2026oOnNext { closeDrawer() }"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public x()V
    .locals 1

    .prologue
    .line 303
    sget-object v0, Lco/uk/getmondo/settings/SettingsActivity;->c:Lco/uk/getmondo/settings/SettingsActivity$a;

    check-cast p0, Landroid/content/Context;

    invoke-virtual {v0, p0}, Lco/uk/getmondo/settings/SettingsActivity$a;->a(Landroid/content/Context;)V

    .line 304
    return-void
.end method

.method public y()V
    .locals 3

    .prologue
    .line 307
    const v0, 0x7f0a0359

    invoke-virtual {p0, v0}, Lco/uk/getmondo/main/HomeActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    const v1, 0x7f0a034f

    invoke-virtual {p0, v1}, Lco/uk/getmondo/main/HomeActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lco/uk/getmondo/common/d/a;->a(Ljava/lang/String;Ljava/lang/String;Z)Lco/uk/getmondo/common/d/a;

    move-result-object v0

    .line 308
    invoke-virtual {p0}, Lco/uk/getmondo/main/HomeActivity;->getSupportFragmentManager()Landroid/support/v4/app/n;

    move-result-object v1

    const-string v2, "TAG_ERROR_DIALOG_FRAGMENT"

    invoke-virtual {v0, v1, v2}, Lco/uk/getmondo/common/d/a;->show(Landroid/support/v4/app/n;Ljava/lang/String;)V

    .line 309
    return-void
.end method

.method public z()V
    .locals 3

    .prologue
    .line 312
    const v0, 0x7f0a0359

    invoke-virtual {p0, v0}, Lco/uk/getmondo/main/HomeActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    const v1, 0x7f0a034d

    invoke-virtual {p0, v1}, Lco/uk/getmondo/main/HomeActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lco/uk/getmondo/common/d/a;->a(Ljava/lang/String;Ljava/lang/String;Z)Lco/uk/getmondo/common/d/a;

    move-result-object v0

    .line 313
    invoke-virtual {p0}, Lco/uk/getmondo/main/HomeActivity;->getSupportFragmentManager()Landroid/support/v4/app/n;

    move-result-object v1

    const-string v2, "TAG_ERROR_DIALOG_FRAGMENT"

    invoke-virtual {v0, v1, v2}, Lco/uk/getmondo/common/d/a;->show(Landroid/support/v4/app/n;Ljava/lang/String;)V

    .line 314
    return-void
.end method
