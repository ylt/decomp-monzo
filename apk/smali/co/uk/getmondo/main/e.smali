.class public final Lco/uk/getmondo/main/e;
.super Ljava/lang/Object;
.source "HomePresenter_Factory.java"

# interfaces
.implements Lb/a/b;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lb/a/b",
        "<",
        "Lco/uk/getmondo/main/c;",
        ">;"
    }
.end annotation


# static fields
.field static final synthetic a:Z


# instance fields
.field private final b:Lb/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lb/a",
            "<",
            "Lco/uk/getmondo/main/c;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Ljavax/a/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/a/a",
            "<",
            "Lio/reactivex/u;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Ljavax/a/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/a/a",
            "<",
            "Lio/reactivex/u;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Ljavax/a/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/common/accounts/d;",
            ">;"
        }
    .end annotation
.end field

.field private final f:Ljavax/a/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/common/e/a;",
            ">;"
        }
    .end annotation
.end field

.field private final g:Ljavax/a/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/api/b/a;",
            ">;"
        }
    .end annotation
.end field

.field private final h:Ljavax/a/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/common/a;",
            ">;"
        }
    .end annotation
.end field

.field private final i:Ljavax/a/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/api/ServiceStatusApi;",
            ">;"
        }
    .end annotation
.end field

.field private final j:Ljavax/a/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/payments/send/data/h;",
            ">;"
        }
    .end annotation
.end field

.field private final k:Ljavax/a/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/payments/send/data/p;",
            ">;"
        }
    .end annotation
.end field

.field private final l:Ljavax/a/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/payments/send/data/PeerToPeerRepository;",
            ">;"
        }
    .end annotation
.end field

.field private final m:Ljavax/a/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/news/d;",
            ">;"
        }
    .end annotation
.end field

.field private final n:Ljavax/a/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/background_sync/d;",
            ">;"
        }
    .end annotation
.end field

.field private final o:Ljavax/a/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/common/x;",
            ">;"
        }
    .end annotation
.end field

.field private final p:Ljavax/a/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/api/IdentityVerificationApi;",
            ">;"
        }
    .end annotation
.end field

.field private final q:Ljavax/a/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/main/h;",
            ">;"
        }
    .end annotation
.end field

.field private final r:Ljavax/a/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/feed/a/t;",
            ">;"
        }
    .end annotation
.end field

.field private final s:Ljavax/a/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/common/o;",
            ">;"
        }
    .end annotation
.end field

.field private final t:Ljavax/a/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/card/c;",
            ">;"
        }
    .end annotation
.end field

.field private final u:Ljavax/a/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/signup/status/b;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 26
    const-class v0, Lco/uk/getmondo/main/e;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lco/uk/getmondo/main/e;->a:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lb/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lb/a",
            "<",
            "Lco/uk/getmondo/main/c;",
            ">;",
            "Ljavax/a/a",
            "<",
            "Lio/reactivex/u;",
            ">;",
            "Ljavax/a/a",
            "<",
            "Lio/reactivex/u;",
            ">;",
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/common/accounts/d;",
            ">;",
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/common/e/a;",
            ">;",
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/api/b/a;",
            ">;",
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/common/a;",
            ">;",
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/api/ServiceStatusApi;",
            ">;",
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/payments/send/data/h;",
            ">;",
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/payments/send/data/p;",
            ">;",
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/payments/send/data/PeerToPeerRepository;",
            ">;",
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/news/d;",
            ">;",
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/background_sync/d;",
            ">;",
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/common/x;",
            ">;",
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/api/IdentityVerificationApi;",
            ">;",
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/main/h;",
            ">;",
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/feed/a/t;",
            ">;",
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/common/o;",
            ">;",
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/card/c;",
            ">;",
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/signup/status/b;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 91
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 92
    sget-boolean v1, Lco/uk/getmondo/main/e;->a:Z

    if-nez v1, :cond_0

    if-nez p1, :cond_0

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 93
    :cond_0
    iput-object p1, p0, Lco/uk/getmondo/main/e;->b:Lb/a;

    .line 94
    sget-boolean v1, Lco/uk/getmondo/main/e;->a:Z

    if-nez v1, :cond_1

    if-nez p2, :cond_1

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 95
    :cond_1
    iput-object p2, p0, Lco/uk/getmondo/main/e;->c:Ljavax/a/a;

    .line 96
    sget-boolean v1, Lco/uk/getmondo/main/e;->a:Z

    if-nez v1, :cond_2

    if-nez p3, :cond_2

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 97
    :cond_2
    iput-object p3, p0, Lco/uk/getmondo/main/e;->d:Ljavax/a/a;

    .line 98
    sget-boolean v1, Lco/uk/getmondo/main/e;->a:Z

    if-nez v1, :cond_3

    if-nez p4, :cond_3

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 99
    :cond_3
    iput-object p4, p0, Lco/uk/getmondo/main/e;->e:Ljavax/a/a;

    .line 100
    sget-boolean v1, Lco/uk/getmondo/main/e;->a:Z

    if-nez v1, :cond_4

    if-nez p5, :cond_4

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 101
    :cond_4
    iput-object p5, p0, Lco/uk/getmondo/main/e;->f:Ljavax/a/a;

    .line 102
    sget-boolean v1, Lco/uk/getmondo/main/e;->a:Z

    if-nez v1, :cond_5

    if-nez p6, :cond_5

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 103
    :cond_5
    iput-object p6, p0, Lco/uk/getmondo/main/e;->g:Ljavax/a/a;

    .line 104
    sget-boolean v1, Lco/uk/getmondo/main/e;->a:Z

    if-nez v1, :cond_6

    if-nez p7, :cond_6

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 105
    :cond_6
    iput-object p7, p0, Lco/uk/getmondo/main/e;->h:Ljavax/a/a;

    .line 106
    sget-boolean v1, Lco/uk/getmondo/main/e;->a:Z

    if-nez v1, :cond_7

    if-nez p8, :cond_7

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 107
    :cond_7
    iput-object p8, p0, Lco/uk/getmondo/main/e;->i:Ljavax/a/a;

    .line 108
    sget-boolean v1, Lco/uk/getmondo/main/e;->a:Z

    if-nez v1, :cond_8

    if-nez p9, :cond_8

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 109
    :cond_8
    iput-object p9, p0, Lco/uk/getmondo/main/e;->j:Ljavax/a/a;

    .line 110
    sget-boolean v1, Lco/uk/getmondo/main/e;->a:Z

    if-nez v1, :cond_9

    if-nez p10, :cond_9

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 111
    :cond_9
    iput-object p10, p0, Lco/uk/getmondo/main/e;->k:Ljavax/a/a;

    .line 112
    sget-boolean v1, Lco/uk/getmondo/main/e;->a:Z

    if-nez v1, :cond_a

    if-nez p11, :cond_a

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 113
    :cond_a
    iput-object p11, p0, Lco/uk/getmondo/main/e;->l:Ljavax/a/a;

    .line 114
    sget-boolean v1, Lco/uk/getmondo/main/e;->a:Z

    if-nez v1, :cond_b

    if-nez p12, :cond_b

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 115
    :cond_b
    iput-object p12, p0, Lco/uk/getmondo/main/e;->m:Ljavax/a/a;

    .line 116
    sget-boolean v1, Lco/uk/getmondo/main/e;->a:Z

    if-nez v1, :cond_c

    if-nez p13, :cond_c

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 117
    :cond_c
    iput-object p13, p0, Lco/uk/getmondo/main/e;->n:Ljavax/a/a;

    .line 118
    sget-boolean v1, Lco/uk/getmondo/main/e;->a:Z

    if-nez v1, :cond_d

    if-nez p14, :cond_d

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 119
    :cond_d
    move-object/from16 v0, p14

    iput-object v0, p0, Lco/uk/getmondo/main/e;->o:Ljavax/a/a;

    .line 120
    sget-boolean v1, Lco/uk/getmondo/main/e;->a:Z

    if-nez v1, :cond_e

    if-nez p15, :cond_e

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 121
    :cond_e
    move-object/from16 v0, p15

    iput-object v0, p0, Lco/uk/getmondo/main/e;->p:Ljavax/a/a;

    .line 122
    sget-boolean v1, Lco/uk/getmondo/main/e;->a:Z

    if-nez v1, :cond_f

    if-nez p16, :cond_f

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 123
    :cond_f
    move-object/from16 v0, p16

    iput-object v0, p0, Lco/uk/getmondo/main/e;->q:Ljavax/a/a;

    .line 124
    sget-boolean v1, Lco/uk/getmondo/main/e;->a:Z

    if-nez v1, :cond_10

    if-nez p17, :cond_10

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 125
    :cond_10
    move-object/from16 v0, p17

    iput-object v0, p0, Lco/uk/getmondo/main/e;->r:Ljavax/a/a;

    .line 126
    sget-boolean v1, Lco/uk/getmondo/main/e;->a:Z

    if-nez v1, :cond_11

    if-nez p18, :cond_11

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 127
    :cond_11
    move-object/from16 v0, p18

    iput-object v0, p0, Lco/uk/getmondo/main/e;->s:Ljavax/a/a;

    .line 128
    sget-boolean v1, Lco/uk/getmondo/main/e;->a:Z

    if-nez v1, :cond_12

    if-nez p19, :cond_12

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 129
    :cond_12
    move-object/from16 v0, p19

    iput-object v0, p0, Lco/uk/getmondo/main/e;->t:Ljavax/a/a;

    .line 130
    sget-boolean v1, Lco/uk/getmondo/main/e;->a:Z

    if-nez v1, :cond_13

    if-nez p20, :cond_13

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 131
    :cond_13
    move-object/from16 v0, p20

    iput-object v0, p0, Lco/uk/getmondo/main/e;->u:Ljavax/a/a;

    .line 132
    return-void
.end method

.method public static a(Lb/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;)Lb/a/b;
    .locals 21
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lb/a",
            "<",
            "Lco/uk/getmondo/main/c;",
            ">;",
            "Ljavax/a/a",
            "<",
            "Lio/reactivex/u;",
            ">;",
            "Ljavax/a/a",
            "<",
            "Lio/reactivex/u;",
            ">;",
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/common/accounts/d;",
            ">;",
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/common/e/a;",
            ">;",
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/api/b/a;",
            ">;",
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/common/a;",
            ">;",
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/api/ServiceStatusApi;",
            ">;",
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/payments/send/data/h;",
            ">;",
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/payments/send/data/p;",
            ">;",
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/payments/send/data/PeerToPeerRepository;",
            ">;",
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/news/d;",
            ">;",
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/background_sync/d;",
            ">;",
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/common/x;",
            ">;",
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/api/IdentityVerificationApi;",
            ">;",
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/main/h;",
            ">;",
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/feed/a/t;",
            ">;",
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/common/o;",
            ">;",
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/card/c;",
            ">;",
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/signup/status/b;",
            ">;)",
            "Lb/a/b",
            "<",
            "Lco/uk/getmondo/main/c;",
            ">;"
        }
    .end annotation

    .prologue
    .line 181
    new-instance v0, Lco/uk/getmondo/main/e;

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    move-object/from16 v13, p12

    move-object/from16 v14, p13

    move-object/from16 v15, p14

    move-object/from16 v16, p15

    move-object/from16 v17, p16

    move-object/from16 v18, p17

    move-object/from16 v19, p18

    move-object/from16 v20, p19

    invoke-direct/range {v0 .. v20}, Lco/uk/getmondo/main/e;-><init>(Lb/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;)V

    return-object v0
.end method


# virtual methods
.method public a()Lco/uk/getmondo/main/c;
    .locals 22

    .prologue
    .line 136
    move-object/from16 v0, p0

    iget-object v0, v0, Lco/uk/getmondo/main/e;->b:Lb/a;

    move-object/from16 v21, v0

    new-instance v1, Lco/uk/getmondo/main/c;

    move-object/from16 v0, p0

    iget-object v2, v0, Lco/uk/getmondo/main/e;->c:Ljavax/a/a;

    .line 139
    invoke-interface {v2}, Ljavax/a/a;->b()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lio/reactivex/u;

    move-object/from16 v0, p0

    iget-object v3, v0, Lco/uk/getmondo/main/e;->d:Ljavax/a/a;

    .line 140
    invoke-interface {v3}, Ljavax/a/a;->b()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lio/reactivex/u;

    move-object/from16 v0, p0

    iget-object v4, v0, Lco/uk/getmondo/main/e;->e:Ljavax/a/a;

    .line 141
    invoke-interface {v4}, Ljavax/a/a;->b()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lco/uk/getmondo/common/accounts/d;

    move-object/from16 v0, p0

    iget-object v5, v0, Lco/uk/getmondo/main/e;->f:Ljavax/a/a;

    .line 142
    invoke-interface {v5}, Ljavax/a/a;->b()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lco/uk/getmondo/common/e/a;

    move-object/from16 v0, p0

    iget-object v6, v0, Lco/uk/getmondo/main/e;->g:Ljavax/a/a;

    .line 143
    invoke-interface {v6}, Ljavax/a/a;->b()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lco/uk/getmondo/api/b/a;

    move-object/from16 v0, p0

    iget-object v7, v0, Lco/uk/getmondo/main/e;->h:Ljavax/a/a;

    .line 144
    invoke-interface {v7}, Ljavax/a/a;->b()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lco/uk/getmondo/common/a;

    move-object/from16 v0, p0

    iget-object v8, v0, Lco/uk/getmondo/main/e;->i:Ljavax/a/a;

    .line 145
    invoke-interface {v8}, Ljavax/a/a;->b()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lco/uk/getmondo/api/ServiceStatusApi;

    move-object/from16 v0, p0

    iget-object v9, v0, Lco/uk/getmondo/main/e;->j:Ljavax/a/a;

    .line 146
    invoke-interface {v9}, Ljavax/a/a;->b()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lco/uk/getmondo/payments/send/data/h;

    move-object/from16 v0, p0

    iget-object v10, v0, Lco/uk/getmondo/main/e;->k:Ljavax/a/a;

    .line 147
    invoke-interface {v10}, Ljavax/a/a;->b()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lco/uk/getmondo/payments/send/data/p;

    move-object/from16 v0, p0

    iget-object v11, v0, Lco/uk/getmondo/main/e;->l:Ljavax/a/a;

    .line 148
    invoke-interface {v11}, Ljavax/a/a;->b()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lco/uk/getmondo/payments/send/data/PeerToPeerRepository;

    move-object/from16 v0, p0

    iget-object v12, v0, Lco/uk/getmondo/main/e;->m:Ljavax/a/a;

    .line 149
    invoke-interface {v12}, Ljavax/a/a;->b()Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lco/uk/getmondo/news/d;

    move-object/from16 v0, p0

    iget-object v13, v0, Lco/uk/getmondo/main/e;->n:Ljavax/a/a;

    .line 150
    invoke-interface {v13}, Ljavax/a/a;->b()Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Lco/uk/getmondo/background_sync/d;

    move-object/from16 v0, p0

    iget-object v14, v0, Lco/uk/getmondo/main/e;->o:Ljavax/a/a;

    .line 151
    invoke-interface {v14}, Ljavax/a/a;->b()Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lco/uk/getmondo/common/x;

    move-object/from16 v0, p0

    iget-object v15, v0, Lco/uk/getmondo/main/e;->p:Ljavax/a/a;

    .line 152
    invoke-interface {v15}, Ljavax/a/a;->b()Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Lco/uk/getmondo/api/IdentityVerificationApi;

    move-object/from16 v0, p0

    iget-object v0, v0, Lco/uk/getmondo/main/e;->q:Ljavax/a/a;

    move-object/from16 v16, v0

    .line 153
    invoke-interface/range {v16 .. v16}, Ljavax/a/a;->b()Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Lco/uk/getmondo/main/h;

    move-object/from16 v0, p0

    iget-object v0, v0, Lco/uk/getmondo/main/e;->r:Ljavax/a/a;

    move-object/from16 v17, v0

    .line 154
    invoke-interface/range {v17 .. v17}, Ljavax/a/a;->b()Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Lco/uk/getmondo/feed/a/t;

    move-object/from16 v0, p0

    iget-object v0, v0, Lco/uk/getmondo/main/e;->s:Ljavax/a/a;

    move-object/from16 v18, v0

    .line 155
    invoke-interface/range {v18 .. v18}, Ljavax/a/a;->b()Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Lco/uk/getmondo/common/o;

    move-object/from16 v0, p0

    iget-object v0, v0, Lco/uk/getmondo/main/e;->t:Ljavax/a/a;

    move-object/from16 v19, v0

    .line 156
    invoke-interface/range {v19 .. v19}, Ljavax/a/a;->b()Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Lco/uk/getmondo/card/c;

    move-object/from16 v0, p0

    iget-object v0, v0, Lco/uk/getmondo/main/e;->u:Ljavax/a/a;

    move-object/from16 v20, v0

    .line 157
    invoke-interface/range {v20 .. v20}, Ljavax/a/a;->b()Ljava/lang/Object;

    move-result-object v20

    check-cast v20, Lco/uk/getmondo/signup/status/b;

    invoke-direct/range {v1 .. v20}, Lco/uk/getmondo/main/c;-><init>(Lio/reactivex/u;Lio/reactivex/u;Lco/uk/getmondo/common/accounts/d;Lco/uk/getmondo/common/e/a;Lco/uk/getmondo/api/b/a;Lco/uk/getmondo/common/a;Lco/uk/getmondo/api/ServiceStatusApi;Lco/uk/getmondo/payments/send/data/h;Lco/uk/getmondo/payments/send/data/p;Lco/uk/getmondo/payments/send/data/PeerToPeerRepository;Lco/uk/getmondo/news/d;Lco/uk/getmondo/background_sync/d;Lco/uk/getmondo/common/x;Lco/uk/getmondo/api/IdentityVerificationApi;Lco/uk/getmondo/main/h;Lco/uk/getmondo/feed/a/t;Lco/uk/getmondo/common/o;Lco/uk/getmondo/card/c;Lco/uk/getmondo/signup/status/b;)V

    .line 136
    move-object/from16 v0, v21

    invoke-static {v0, v1}, Lb/a/c;->a(Lb/a;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lco/uk/getmondo/main/c;

    return-object v1
.end method

.method public synthetic b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 26
    invoke-virtual {p0}, Lco/uk/getmondo/main/e;->a()Lco/uk/getmondo/main/c;

    move-result-object v0

    return-object v0
.end method
