.class final Lco/uk/getmondo/main/c$j$2;
.super Ljava/lang/Object;
.source "HomePresenter.kt"

# interfaces
.implements Lio/reactivex/c/g;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lco/uk/getmondo/main/c$j;->a(Lco/uk/getmondo/main/f;)Lio/reactivex/n;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/g",
        "<",
        "Ljava/lang/Throwable;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u0003\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "",
        "error",
        "",
        "kotlin.jvm.PlatformType",
        "accept"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field final synthetic a:Lco/uk/getmondo/main/c$j;


# direct methods
.method constructor <init>(Lco/uk/getmondo/main/c$j;)V
    .locals 0

    iput-object p1, p0, Lco/uk/getmondo/main/c$j$2;->a:Lco/uk/getmondo/main/c$j;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 45
    check-cast p1, Ljava/lang/Throwable;

    invoke-virtual {p0, p1}, Lco/uk/getmondo/main/c$j$2;->a(Ljava/lang/Throwable;)V

    return-void
.end method

.method public final a(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 177
    iget-object v0, p0, Lco/uk/getmondo/main/c$j$2;->a:Lco/uk/getmondo/main/c$j;

    iget-object v0, v0, Lco/uk/getmondo/main/c$j;->b:Lco/uk/getmondo/main/c$a;

    invoke-interface {v0}, Lco/uk/getmondo/main/c$a;->G()V

    .line 178
    const-string v0, "Failed opening a monzo.me link"

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {p1, v0, v1}, Ld/a/a;->a(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 180
    instance-of v0, p1, Lco/uk/getmondo/payments/send/data/PeerToPeerRepository$a;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lco/uk/getmondo/main/c$j$2;->a:Lco/uk/getmondo/main/c$j;

    iget-object v0, v0, Lco/uk/getmondo/main/c$j;->b:Lco/uk/getmondo/main/c$a;

    invoke-interface {v0}, Lco/uk/getmondo/main/c$a;->z()V

    .line 183
    :goto_0
    return-void

    .line 181
    :cond_0
    instance-of v0, p1, Lco/uk/getmondo/payments/send/data/PeerToPeerRepository$UserHasP2pDisabledException;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lco/uk/getmondo/main/c$j$2;->a:Lco/uk/getmondo/main/c$j;

    iget-object v0, v0, Lco/uk/getmondo/main/c$j;->b:Lco/uk/getmondo/main/c$a;

    invoke-interface {v0}, Lco/uk/getmondo/main/c$a;->A()V

    goto :goto_0

    .line 182
    :cond_1
    iget-object v0, p0, Lco/uk/getmondo/main/c$j$2;->a:Lco/uk/getmondo/main/c$j;

    iget-object v0, v0, Lco/uk/getmondo/main/c$j;->b:Lco/uk/getmondo/main/c$a;

    invoke-interface {v0}, Lco/uk/getmondo/main/c$a;->B()V

    goto :goto_0
.end method
