.class public Lco/uk/getmondo/force_upgrade/c;
.super Lco/uk/getmondo/common/ui/b;
.source "ForceUpgradePresenter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lco/uk/getmondo/force_upgrade/c$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lco/uk/getmondo/common/ui/b",
        "<",
        "Lco/uk/getmondo/force_upgrade/c$a;",
        ">;"
    }
.end annotation


# instance fields
.field private final c:Lco/uk/getmondo/common/a;


# direct methods
.method constructor <init>(Lco/uk/getmondo/common/a;)V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Lco/uk/getmondo/common/ui/b;-><init>()V

    .line 20
    iput-object p1, p0, Lco/uk/getmondo/force_upgrade/c;->c:Lco/uk/getmondo/common/a;

    .line 21
    return-void
.end method

.method static synthetic a(Lco/uk/getmondo/force_upgrade/c$a;Lco/uk/getmondo/common/b/a;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 30
    invoke-interface {p0}, Lco/uk/getmondo/force_upgrade/c$a;->a()V

    return-void
.end method


# virtual methods
.method public bridge synthetic a(Lco/uk/getmondo/common/ui/f;)V
    .locals 0

    .prologue
    .line 14
    check-cast p1, Lco/uk/getmondo/force_upgrade/c$a;

    invoke-virtual {p0, p1}, Lco/uk/getmondo/force_upgrade/c;->a(Lco/uk/getmondo/force_upgrade/c$a;)V

    return-void
.end method

.method public a(Lco/uk/getmondo/force_upgrade/c$a;)V
    .locals 3

    .prologue
    .line 25
    invoke-super {p0, p1}, Lco/uk/getmondo/common/ui/b;->a(Lco/uk/getmondo/common/ui/f;)V

    .line 27
    iget-object v0, p0, Lco/uk/getmondo/force_upgrade/c;->c:Lco/uk/getmondo/common/a;

    invoke-static {}, Lco/uk/getmondo/api/model/tracking/Impression;->Q()Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v1

    invoke-virtual {v0, v1}, Lco/uk/getmondo/common/a;->a(Lco/uk/getmondo/api/model/tracking/Impression;)V

    .line 29
    invoke-interface {p1}, Lco/uk/getmondo/force_upgrade/c$a;->b()Lio/reactivex/n;

    move-result-object v0

    invoke-static {p1}, Lco/uk/getmondo/force_upgrade/d;->a(Lco/uk/getmondo/force_upgrade/c$a;)Lio/reactivex/c/g;

    move-result-object v1

    invoke-static {}, Lco/uk/getmondo/force_upgrade/e;->a()Lio/reactivex/c/g;

    move-result-object v2

    .line 30
    invoke-virtual {v0, v1, v2}, Lio/reactivex/n;->subscribe(Lio/reactivex/c/g;Lio/reactivex/c/g;)Lio/reactivex/b/b;

    move-result-object v0

    .line 29
    invoke-virtual {p0, v0}, Lco/uk/getmondo/force_upgrade/c;->a(Lio/reactivex/b/b;)V

    .line 31
    return-void
.end method
