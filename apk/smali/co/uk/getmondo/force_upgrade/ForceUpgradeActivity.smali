.class public Lco/uk/getmondo/force_upgrade/ForceUpgradeActivity;
.super Lco/uk/getmondo/common/activities/b;
.source "ForceUpgradeActivity.java"

# interfaces
.implements Lco/uk/getmondo/force_upgrade/c$a;


# instance fields
.field a:Lco/uk/getmondo/force_upgrade/c;

.field upgradeButton:Landroid/widget/Button;
    .annotation build Lbutterknife/BindView;
        value = 0x7f11016e
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Lco/uk/getmondo/common/activities/b;-><init>()V

    return-void
.end method

.method public static a(Landroid/content/Context;)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 28
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lco/uk/getmondo/force_upgrade/ForceUpgradeActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const v1, 0x10018000

    .line 29
    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    move-result-object v0

    .line 28
    return-object v0
.end method

.method static synthetic a(Ljava/lang/Object;)Lco/uk/getmondo/common/b/a;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 62
    sget-object v0, Lco/uk/getmondo/common/b/a;->a:Lco/uk/getmondo/common/b/a;

    return-object v0
.end method


# virtual methods
.method public a()V
    .locals 3

    .prologue
    .line 53
    :try_start_0
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    const-string v2, "market://details?id=co.uk.getmondo"

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    invoke-virtual {p0, v0}, Lco/uk/getmondo/force_upgrade/ForceUpgradeActivity;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 57
    :goto_0
    return-void

    .line 54
    :catch_0
    move-exception v0

    .line 55
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    const-string v2, "https://play.google.com/store/apps/details?id=co.uk.getmondo"

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    invoke-virtual {p0, v0}, Lco/uk/getmondo/force_upgrade/ForceUpgradeActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method public b()Lio/reactivex/n;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Lco/uk/getmondo/common/b/a;",
            ">;"
        }
    .end annotation

    .prologue
    .line 61
    iget-object v0, p0, Lco/uk/getmondo/force_upgrade/ForceUpgradeActivity;->upgradeButton:Landroid/widget/Button;

    invoke-static {v0}, Lcom/b/a/c/c;->a(Landroid/view/View;)Lio/reactivex/n;

    move-result-object v0

    invoke-static {}, Lco/uk/getmondo/force_upgrade/a;->a()Lio/reactivex/c/h;

    move-result-object v1

    .line 62
    invoke-virtual {v0, v1}, Lio/reactivex/n;->map(Lio/reactivex/c/h;)Lio/reactivex/n;

    move-result-object v0

    .line 61
    return-object v0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 34
    invoke-super {p0, p1}, Lco/uk/getmondo/common/activities/b;->onCreate(Landroid/os/Bundle;)V

    .line 35
    const v0, 0x7f05003a

    invoke-virtual {p0, v0}, Lco/uk/getmondo/force_upgrade/ForceUpgradeActivity;->setContentView(I)V

    .line 37
    invoke-static {p0}, Lbutterknife/ButterKnife;->bind(Landroid/app/Activity;)Lbutterknife/Unbinder;

    .line 38
    invoke-virtual {p0}, Lco/uk/getmondo/force_upgrade/ForceUpgradeActivity;->l()Lco/uk/getmondo/common/h/b/b;

    move-result-object v0

    invoke-interface {v0, p0}, Lco/uk/getmondo/common/h/b/b;->a(Lco/uk/getmondo/force_upgrade/ForceUpgradeActivity;)V

    .line 40
    iget-object v0, p0, Lco/uk/getmondo/force_upgrade/ForceUpgradeActivity;->a:Lco/uk/getmondo/force_upgrade/c;

    invoke-virtual {v0, p0}, Lco/uk/getmondo/force_upgrade/c;->a(Lco/uk/getmondo/force_upgrade/c$a;)V

    .line 41
    return-void
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lco/uk/getmondo/force_upgrade/ForceUpgradeActivity;->a:Lco/uk/getmondo/force_upgrade/c;

    invoke-virtual {v0}, Lco/uk/getmondo/force_upgrade/c;->b()V

    .line 47
    invoke-super {p0}, Lco/uk/getmondo/common/activities/b;->onDestroy()V

    .line 48
    return-void
.end method
