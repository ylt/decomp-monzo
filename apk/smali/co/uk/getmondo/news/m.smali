.class Lco/uk/getmondo/news/m;
.super Lco/uk/getmondo/common/ui/b;
.source "NewsPresenter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lco/uk/getmondo/news/m$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lco/uk/getmondo/common/ui/b",
        "<",
        "Lco/uk/getmondo/news/m$a;",
        ">;"
    }
.end annotation


# instance fields
.field private final c:Lco/uk/getmondo/news/c;

.field private final d:Lco/uk/getmondo/settings/k;

.field private final e:Lco/uk/getmondo/common/a;


# direct methods
.method constructor <init>(Lco/uk/getmondo/news/c;Lco/uk/getmondo/settings/k;Lco/uk/getmondo/common/a;)V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Lco/uk/getmondo/common/ui/b;-><init>()V

    .line 21
    iput-object p1, p0, Lco/uk/getmondo/news/m;->c:Lco/uk/getmondo/news/c;

    .line 22
    iput-object p2, p0, Lco/uk/getmondo/news/m;->d:Lco/uk/getmondo/settings/k;

    .line 23
    iput-object p3, p0, Lco/uk/getmondo/news/m;->e:Lco/uk/getmondo/common/a;

    .line 24
    return-void
.end method


# virtual methods
.method public bridge synthetic a(Lco/uk/getmondo/common/ui/f;)V
    .locals 0

    .prologue
    .line 13
    check-cast p1, Lco/uk/getmondo/news/m$a;

    invoke-virtual {p0, p1}, Lco/uk/getmondo/news/m;->a(Lco/uk/getmondo/news/m$a;)V

    return-void
.end method

.method public a(Lco/uk/getmondo/news/m$a;)V
    .locals 2

    .prologue
    .line 28
    invoke-super {p0, p1}, Lco/uk/getmondo/common/ui/b;->a(Lco/uk/getmondo/common/ui/f;)V

    .line 30
    iget-object v0, p0, Lco/uk/getmondo/news/m;->c:Lco/uk/getmondo/news/c;

    invoke-interface {p1, v0}, Lco/uk/getmondo/news/m$a;->a(Lco/uk/getmondo/news/c;)V

    .line 32
    iget-object v0, p0, Lco/uk/getmondo/news/m;->d:Lco/uk/getmondo/settings/k;

    iget-object v1, p0, Lco/uk/getmondo/news/m;->c:Lco/uk/getmondo/news/c;

    invoke-virtual {v1}, Lco/uk/getmondo/news/c;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lco/uk/getmondo/settings/k;->b(Ljava/lang/String;)V

    .line 33
    iget-object v0, p0, Lco/uk/getmondo/news/m;->e:Lco/uk/getmondo/common/a;

    invoke-static {}, Lco/uk/getmondo/api/model/tracking/Impression;->Y()Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v1

    invoke-virtual {v0, v1}, Lco/uk/getmondo/common/a;->a(Lco/uk/getmondo/api/model/tracking/Impression;)V

    .line 34
    return-void
.end method
