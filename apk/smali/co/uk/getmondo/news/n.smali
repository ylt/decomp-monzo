.class public final Lco/uk/getmondo/news/n;
.super Ljava/lang/Object;
.source "NewsPresenter_Factory.java"

# interfaces
.implements Lb/a/b;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lb/a/b",
        "<",
        "Lco/uk/getmondo/news/m;",
        ">;"
    }
.end annotation


# static fields
.field static final synthetic a:Z


# instance fields
.field private final b:Lb/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lb/a",
            "<",
            "Lco/uk/getmondo/news/m;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Ljavax/a/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/news/c;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Ljavax/a/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/settings/k;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Ljavax/a/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/common/a;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 11
    const-class v0, Lco/uk/getmondo/news/n;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lco/uk/getmondo/news/n;->a:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lb/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lb/a",
            "<",
            "Lco/uk/getmondo/news/m;",
            ">;",
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/news/c;",
            ">;",
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/settings/k;",
            ">;",
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/common/a;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    sget-boolean v0, Lco/uk/getmondo/news/n;->a:Z

    if-nez v0, :cond_0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 30
    :cond_0
    iput-object p1, p0, Lco/uk/getmondo/news/n;->b:Lb/a;

    .line 31
    sget-boolean v0, Lco/uk/getmondo/news/n;->a:Z

    if-nez v0, :cond_1

    if-nez p2, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 32
    :cond_1
    iput-object p2, p0, Lco/uk/getmondo/news/n;->c:Ljavax/a/a;

    .line 33
    sget-boolean v0, Lco/uk/getmondo/news/n;->a:Z

    if-nez v0, :cond_2

    if-nez p3, :cond_2

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 34
    :cond_2
    iput-object p3, p0, Lco/uk/getmondo/news/n;->d:Ljavax/a/a;

    .line 35
    sget-boolean v0, Lco/uk/getmondo/news/n;->a:Z

    if-nez v0, :cond_3

    if-nez p4, :cond_3

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 36
    :cond_3
    iput-object p4, p0, Lco/uk/getmondo/news/n;->e:Ljavax/a/a;

    .line 37
    return-void
.end method

.method public static a(Lb/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;)Lb/a/b;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lb/a",
            "<",
            "Lco/uk/getmondo/news/m;",
            ">;",
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/news/c;",
            ">;",
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/settings/k;",
            ">;",
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/common/a;",
            ">;)",
            "Lb/a/b",
            "<",
            "Lco/uk/getmondo/news/m;",
            ">;"
        }
    .end annotation

    .prologue
    .line 54
    new-instance v0, Lco/uk/getmondo/news/n;

    invoke-direct {v0, p0, p1, p2, p3}, Lco/uk/getmondo/news/n;-><init>(Lb/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;)V

    return-object v0
.end method


# virtual methods
.method public a()Lco/uk/getmondo/news/m;
    .locals 5

    .prologue
    .line 41
    iget-object v3, p0, Lco/uk/getmondo/news/n;->b:Lb/a;

    new-instance v4, Lco/uk/getmondo/news/m;

    iget-object v0, p0, Lco/uk/getmondo/news/n;->c:Ljavax/a/a;

    .line 44
    invoke-interface {v0}, Ljavax/a/a;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/news/c;

    iget-object v1, p0, Lco/uk/getmondo/news/n;->d:Ljavax/a/a;

    .line 45
    invoke-interface {v1}, Ljavax/a/a;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lco/uk/getmondo/settings/k;

    iget-object v2, p0, Lco/uk/getmondo/news/n;->e:Ljavax/a/a;

    .line 46
    invoke-interface {v2}, Ljavax/a/a;->b()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lco/uk/getmondo/common/a;

    invoke-direct {v4, v0, v1, v2}, Lco/uk/getmondo/news/m;-><init>(Lco/uk/getmondo/news/c;Lco/uk/getmondo/settings/k;Lco/uk/getmondo/common/a;)V

    .line 41
    invoke-static {v3, v4}, Lb/a/c;->a(Lb/a;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/news/m;

    return-object v0
.end method

.method public synthetic b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 11
    invoke-virtual {p0}, Lco/uk/getmondo/news/n;->a()Lco/uk/getmondo/news/m;

    move-result-object v0

    return-object v0
.end method
