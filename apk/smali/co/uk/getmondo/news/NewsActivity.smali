.class public Lco/uk/getmondo/news/NewsActivity;
.super Lco/uk/getmondo/common/activities/b;
.source "NewsActivity.java"

# interfaces
.implements Lco/uk/getmondo/news/m$a;


# instance fields
.field a:Lco/uk/getmondo/news/m;

.field newsWebView:Landroid/webkit/WebView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f1101cc
    .end annotation
.end field

.field toolbar:Landroid/support/v7/widget/Toolbar;
    .annotation build Lbutterknife/BindView;
        value = 0x7f1100fe
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Lco/uk/getmondo/common/activities/b;-><init>()V

    return-void
.end method

.method public static a(Landroid/content/Context;Lco/uk/getmondo/news/c;)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 26
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lco/uk/getmondo/news/NewsActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 27
    const-string v1, "KEY_NEWS_ITEM"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 28
    return-object v0
.end method


# virtual methods
.method public a(Lco/uk/getmondo/news/c;)V
    .locals 2

    .prologue
    .line 51
    iget-object v0, p0, Lco/uk/getmondo/news/NewsActivity;->toolbar:Landroid/support/v7/widget/Toolbar;

    const v1, 0x7f02014d

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/Toolbar;->setNavigationIcon(I)V

    .line 52
    iget-object v0, p0, Lco/uk/getmondo/news/NewsActivity;->newsWebView:Landroid/webkit/WebView;

    invoke-virtual {p1}, Lco/uk/getmondo/news/c;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    .line 53
    invoke-virtual {p1}, Lco/uk/getmondo/news/c;->c()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lco/uk/getmondo/news/NewsActivity;->setTitle(Ljava/lang/CharSequence;)V

    .line 54
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 33
    invoke-super {p0, p1}, Lco/uk/getmondo/common/activities/b;->onCreate(Landroid/os/Bundle;)V

    .line 35
    const v0, 0x7f05004d

    invoke-virtual {p0, v0}, Lco/uk/getmondo/news/NewsActivity;->setContentView(I)V

    .line 36
    invoke-static {p0}, Lbutterknife/ButterKnife;->bind(Landroid/app/Activity;)Lbutterknife/Unbinder;

    .line 37
    invoke-virtual {p0}, Lco/uk/getmondo/news/NewsActivity;->l()Lco/uk/getmondo/common/h/b/b;

    move-result-object v1

    new-instance v2, Lco/uk/getmondo/news/k;

    invoke-virtual {p0}, Lco/uk/getmondo/news/NewsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v3, "KEY_NEWS_ITEM"

    invoke-virtual {v0, v3}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/news/c;

    invoke-direct {v2, v0}, Lco/uk/getmondo/news/k;-><init>(Lco/uk/getmondo/news/c;)V

    invoke-interface {v1, v2}, Lco/uk/getmondo/common/h/b/b;->a(Lco/uk/getmondo/news/k;)Lco/uk/getmondo/news/b;

    move-result-object v0

    invoke-interface {v0, p0}, Lco/uk/getmondo/news/b;->a(Lco/uk/getmondo/news/NewsActivity;)V

    .line 39
    iget-object v0, p0, Lco/uk/getmondo/news/NewsActivity;->a:Lco/uk/getmondo/news/m;

    invoke-virtual {v0, p0}, Lco/uk/getmondo/news/m;->a(Lco/uk/getmondo/news/m$a;)V

    .line 40
    return-void
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lco/uk/getmondo/news/NewsActivity;->a:Lco/uk/getmondo/news/m;

    invoke-virtual {v0}, Lco/uk/getmondo/news/m;->b()V

    .line 46
    invoke-super {p0}, Lco/uk/getmondo/common/activities/b;->onDestroy()V

    .line 47
    return-void
.end method
