.class public Lco/uk/getmondo/news/d;
.super Ljava/lang/Object;
.source "NewsManager.java"


# instance fields
.field private final a:Lco/uk/getmondo/api/MonzoApi;

.field private final b:Lco/uk/getmondo/settings/k;


# direct methods
.method public constructor <init>(Lco/uk/getmondo/api/MonzoApi;Lco/uk/getmondo/settings/k;)V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    iput-object p1, p0, Lco/uk/getmondo/news/d;->a:Lco/uk/getmondo/api/MonzoApi;

    .line 21
    iput-object p2, p0, Lco/uk/getmondo/news/d;->b:Lco/uk/getmondo/settings/k;

    .line 22
    return-void
.end method

.method static synthetic a(Ljava/util/List;)Lco/uk/getmondo/news/c;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 35
    const/4 v0, 0x0

    invoke-interface {p0, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/api/model/ApiNewsItem;

    .line 36
    new-instance v1, Lco/uk/getmondo/news/c;

    invoke-virtual {v0}, Lco/uk/getmondo/api/model/ApiNewsItem;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Lco/uk/getmondo/api/model/ApiNewsItem;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0}, Lco/uk/getmondo/api/model/ApiNewsItem;->c()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v2, v3, v0}, Lco/uk/getmondo/news/c;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object v1
.end method

.method static synthetic a(Lco/uk/getmondo/news/d;Ljava/util/List;)Lio/reactivex/z;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 30
    invoke-static {p1}, Lio/reactivex/n;->fromIterable(Ljava/lang/Iterable;)Lio/reactivex/n;

    move-result-object v0

    invoke-static {p0}, Lco/uk/getmondo/news/i;->a(Lco/uk/getmondo/news/d;)Lio/reactivex/c/q;

    move-result-object v1

    .line 31
    invoke-virtual {v0, v1}, Lio/reactivex/n;->filter(Lio/reactivex/c/q;)Lio/reactivex/n;

    move-result-object v0

    .line 32
    invoke-virtual {v0}, Lio/reactivex/n;->toList()Lio/reactivex/v;

    move-result-object v0

    .line 30
    return-object v0
.end method

.method private a(Lco/uk/getmondo/api/model/ApiNewsItem;)Z
    .locals 2

    .prologue
    .line 41
    invoke-static {}, Lorg/threeten/bp/LocalDateTime;->a()Lorg/threeten/bp/LocalDateTime;

    move-result-object v0

    invoke-virtual {p1}, Lco/uk/getmondo/api/model/ApiNewsItem;->d()Lorg/threeten/bp/LocalDateTime;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/threeten/bp/LocalDateTime;->c(Lorg/threeten/bp/chrono/ChronoLocalDateTime;)Z

    move-result v0

    return v0
.end method

.method static synthetic a(Lco/uk/getmondo/news/d;Lco/uk/getmondo/api/model/ApiNewsItem;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 31
    invoke-direct {p0, p1}, Lco/uk/getmondo/news/d;->a(Lco/uk/getmondo/api/model/ApiNewsItem;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0, p1}, Lco/uk/getmondo/news/d;->b(Lco/uk/getmondo/api/model/ApiNewsItem;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private b(Lco/uk/getmondo/api/model/ApiNewsItem;)Z
    .locals 2

    .prologue
    .line 45
    iget-object v0, p0, Lco/uk/getmondo/news/d;->b:Lco/uk/getmondo/settings/k;

    invoke-virtual {p1}, Lco/uk/getmondo/api/model/ApiNewsItem;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lco/uk/getmondo/settings/k;->a(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method static synthetic b(Ljava/util/List;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 33
    invoke-interface {p0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a()Lio/reactivex/h;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/h",
            "<",
            "Lco/uk/getmondo/news/c;",
            ">;"
        }
    .end annotation

    .prologue
    .line 28
    iget-object v0, p0, Lco/uk/getmondo/news/d;->a:Lco/uk/getmondo/api/MonzoApi;

    invoke-interface {v0}, Lco/uk/getmondo/api/MonzoApi;->news()Lio/reactivex/v;

    move-result-object v0

    invoke-static {}, Lco/uk/getmondo/news/e;->a()Lio/reactivex/c/h;

    move-result-object v1

    .line 29
    invoke-virtual {v0, v1}, Lio/reactivex/v;->d(Lio/reactivex/c/h;)Lio/reactivex/v;

    move-result-object v0

    invoke-static {p0}, Lco/uk/getmondo/news/f;->a(Lco/uk/getmondo/news/d;)Lio/reactivex/c/h;

    move-result-object v1

    .line 30
    invoke-virtual {v0, v1}, Lio/reactivex/v;->a(Lio/reactivex/c/h;)Lio/reactivex/v;

    move-result-object v0

    invoke-static {}, Lco/uk/getmondo/news/g;->a()Lio/reactivex/c/q;

    move-result-object v1

    .line 33
    invoke-virtual {v0, v1}, Lio/reactivex/v;->a(Lio/reactivex/c/q;)Lio/reactivex/h;

    move-result-object v0

    invoke-static {}, Lco/uk/getmondo/news/h;->a()Lio/reactivex/c/h;

    move-result-object v1

    .line 34
    invoke-virtual {v0, v1}, Lio/reactivex/h;->c(Lio/reactivex/c/h;)Lio/reactivex/h;

    move-result-object v0

    .line 28
    return-object v0
.end method
