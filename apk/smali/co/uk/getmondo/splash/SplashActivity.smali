.class public Lco/uk/getmondo/splash/SplashActivity;
.super Lco/uk/getmondo/common/activities/b;
.source "SplashActivity.java"

# interfaces
.implements Lco/uk/getmondo/splash/b$a;


# instance fields
.field a:Lco/uk/getmondo/splash/b;

.field b:Lco/uk/getmondo/common/q;

.field c:Lco/uk/getmondo/common/a/c;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Lco/uk/getmondo/common/activities/b;-><init>()V

    return-void
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 38
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lco/uk/getmondo/splash/SplashActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 39
    const v1, 0x10018000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 40
    if-eqz p1, :cond_0

    .line 41
    const-string v1, "KEY_NOTICE_MESSAGE"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 43
    :cond_0
    return-object v0
.end method

.method public static a(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 30
    const/4 v0, 0x0

    invoke-static {p0, v0}, Lco/uk/getmondo/splash/SplashActivity;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 31
    return-void
.end method

.method public static b(Landroid/content/Context;)Landroid/content/Intent;
    .locals 1

    .prologue
    .line 34
    const/4 v0, 0x0

    invoke-static {p0, v0}, Lco/uk/getmondo/splash/SplashActivity;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public static c(Landroid/content/Context;)Landroid/content/Intent;
    .locals 4

    .prologue
    .line 47
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.MAIN"

    sget-object v2, Landroid/net/Uri;->EMPTY:Landroid/net/Uri;

    const-class v3, Lco/uk/getmondo/splash/SplashActivity;

    invoke-direct {v0, v1, v2, p0, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "KEY_OPEN_INTERCOM"

    const/4 v2, 0x1

    .line 48
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    const v1, 0x8000

    .line 49
    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    move-result-object v0

    .line 47
    return-object v0
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 83
    invoke-static {p0}, Lco/uk/getmondo/welcome/WelcomeOnboardingActivity;->a(Landroid/app/Activity;)V

    .line 84
    invoke-virtual {p0, v0, v0}, Lco/uk/getmondo/splash/SplashActivity;->overridePendingTransition(II)V

    .line 85
    invoke-virtual {p0}, Lco/uk/getmondo/splash/SplashActivity;->finish()V

    .line 86
    return-void
.end method

.method public b()V
    .locals 1

    .prologue
    .line 90
    invoke-static {p0}, Lco/uk/getmondo/main/HomeActivity;->b(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lco/uk/getmondo/splash/SplashActivity;->startActivity(Landroid/content/Intent;)V

    .line 91
    invoke-virtual {p0}, Lco/uk/getmondo/splash/SplashActivity;->finish()V

    .line 92
    return-void
.end method

.method public c()V
    .locals 1

    .prologue
    .line 96
    sget-object v0, Lco/uk/getmondo/signup/j;->b:Lco/uk/getmondo/signup/j;

    invoke-static {p0, v0}, Lco/uk/getmondo/signup/status/SignupStatusActivity;->a(Landroid/content/Context;Lco/uk/getmondo/signup/j;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lco/uk/getmondo/splash/SplashActivity;->startActivity(Landroid/content/Intent;)V

    .line 97
    invoke-virtual {p0}, Lco/uk/getmondo/splash/SplashActivity;->finish()V

    .line 98
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 54
    invoke-super {p0, p1}, Lco/uk/getmondo/common/activities/b;->onCreate(Landroid/os/Bundle;)V

    .line 56
    invoke-virtual {p0}, Lco/uk/getmondo/splash/SplashActivity;->l()Lco/uk/getmondo/common/h/b/b;

    move-result-object v0

    invoke-interface {v0, p0}, Lco/uk/getmondo/common/h/b/b;->a(Lco/uk/getmondo/splash/SplashActivity;)V

    .line 58
    iget-object v0, p0, Lco/uk/getmondo/splash/SplashActivity;->a:Lco/uk/getmondo/splash/b;

    invoke-virtual {v0, p0}, Lco/uk/getmondo/splash/b;->a(Lco/uk/getmondo/splash/b$a;)V

    .line 60
    invoke-virtual {p0}, Lco/uk/getmondo/splash/SplashActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "consumed_by_intercom"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 61
    iget-object v0, p0, Lco/uk/getmondo/splash/SplashActivity;->b:Lco/uk/getmondo/common/q;

    invoke-virtual {v0}, Lco/uk/getmondo/common/q;->a()V

    .line 64
    :cond_0
    invoke-virtual {p0}, Lco/uk/getmondo/splash/SplashActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "KEY_OPEN_INTERCOM"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 65
    iget-object v0, p0, Lco/uk/getmondo/splash/SplashActivity;->b:Lco/uk/getmondo/common/q;

    invoke-virtual {v0}, Lco/uk/getmondo/common/q;->a()V

    .line 66
    iget-object v0, p0, Lco/uk/getmondo/splash/SplashActivity;->c:Lco/uk/getmondo/common/a/c;

    sget-object v1, Lco/uk/getmondo/common/a/b;->d:Lco/uk/getmondo/common/a/b;

    invoke-interface {v0, v1}, Lco/uk/getmondo/common/a/c;->a(Lco/uk/getmondo/common/a/b;)V

    .line 69
    :cond_1
    invoke-virtual {p0}, Lco/uk/getmondo/splash/SplashActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "KEY_NOTICE_MESSAGE"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 70
    if-eqz v0, :cond_2

    .line 71
    const/4 v1, 0x1

    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 73
    :cond_2
    return-void
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 77
    iget-object v0, p0, Lco/uk/getmondo/splash/SplashActivity;->a:Lco/uk/getmondo/splash/b;

    invoke-virtual {v0}, Lco/uk/getmondo/splash/b;->b()V

    .line 78
    invoke-super {p0}, Lco/uk/getmondo/common/activities/b;->onDestroy()V

    .line 79
    return-void
.end method
