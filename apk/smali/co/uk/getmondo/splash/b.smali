.class public final Lco/uk/getmondo/splash/b;
.super Lco/uk/getmondo/common/ui/b;
.source "SplashPresenter.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lco/uk/getmondo/splash/b$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lco/uk/getmondo/common/ui/b",
        "<",
        "Lco/uk/getmondo/splash/b$a;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u0003\u0008\u0007\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00020\u0001:\u0001\u000bB\u0017\u0008\u0007\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\u0002\u0010\u0007J\u0010\u0010\u0008\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\u0002H\u0016R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000c"
    }
    d2 = {
        "Lco/uk/getmondo/splash/SplashPresenter;",
        "Lco/uk/getmondo/common/ui/BasePresenter;",
        "Lco/uk/getmondo/splash/SplashPresenter$View;",
        "accountService",
        "Lco/uk/getmondo/common/accounts/AccountService;",
        "signupStatusManager",
        "Lco/uk/getmondo/signup/status/SignupStatusManager;",
        "(Lco/uk/getmondo/common/accounts/AccountService;Lco/uk/getmondo/signup/status/SignupStatusManager;)V",
        "register",
        "",
        "view",
        "View",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field private final c:Lco/uk/getmondo/common/accounts/d;

.field private final d:Lco/uk/getmondo/signup/status/b;


# direct methods
.method public constructor <init>(Lco/uk/getmondo/common/accounts/d;Lco/uk/getmondo/signup/status/b;)V
    .locals 1

    .prologue
    const-string v0, "accountService"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "signupStatusManager"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 15
    invoke-direct {p0}, Lco/uk/getmondo/common/ui/b;-><init>()V

    iput-object p1, p0, Lco/uk/getmondo/splash/b;->c:Lco/uk/getmondo/common/accounts/d;

    iput-object p2, p0, Lco/uk/getmondo/splash/b;->d:Lco/uk/getmondo/signup/status/b;

    return-void
.end method


# virtual methods
.method public bridge synthetic a(Lco/uk/getmondo/common/ui/f;)V
    .locals 0

    .prologue
    .line 11
    check-cast p1, Lco/uk/getmondo/splash/b$a;

    invoke-virtual {p0, p1}, Lco/uk/getmondo/splash/b;->a(Lco/uk/getmondo/splash/b$a;)V

    return-void
.end method

.method public a(Lco/uk/getmondo/splash/b$a;)V
    .locals 2

    .prologue
    const-string v0, "view"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v0, p1

    .line 18
    check-cast v0, Lco/uk/getmondo/common/ui/f;

    invoke-super {p0, v0}, Lco/uk/getmondo/common/ui/b;->a(Lco/uk/getmondo/common/ui/f;)V

    .line 20
    iget-object v0, p0, Lco/uk/getmondo/splash/b;->c:Lco/uk/getmondo/common/accounts/d;

    invoke-virtual {v0}, Lco/uk/getmondo/common/accounts/d;->b()Lco/uk/getmondo/d/ak;

    move-result-object v0

    .line 21
    if-nez v0, :cond_0

    .line 22
    invoke-interface {p1}, Lco/uk/getmondo/splash/b$a;->a()V

    .line 30
    :goto_0
    return-void

    .line 26
    :cond_0
    sget-object v1, Lco/uk/getmondo/a;->c:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-nez v1, :cond_1

    invoke-virtual {v0}, Lco/uk/getmondo/d/ak;->c()Lco/uk/getmondo/d/a;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-interface {v0}, Lco/uk/getmondo/d/a;->f()Z

    move-result v0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_2

    :cond_1
    iget-object v0, p0, Lco/uk/getmondo/splash/b;->d:Lco/uk/getmondo/signup/status/b;

    invoke-virtual {v0}, Lco/uk/getmondo/signup/status/b;->a()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 27
    :cond_2
    invoke-interface {p1}, Lco/uk/getmondo/splash/b$a;->b()V

    goto :goto_0

    .line 29
    :cond_3
    invoke-interface {p1}, Lco/uk/getmondo/splash/b$a;->c()V

    goto :goto_0
.end method
