.class public final Lco/uk/getmondo/card/a;
.super Lco/uk/getmondo/common/f/a;
.source "CardFragment.kt"

# interfaces
.implements Lco/uk/getmondo/card/e$a;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000b\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0008\u0017\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\n\n\u0002\u0010\u000b\n\u0002\u0008\u000b\n\u0002\u0018\u0002\n\u0002\u0008\r\u0018\u00002\u00020\u00012\u00020\u0002B\u0005\u00a2\u0006\u0002\u0010\u0003J\u0008\u0010\u001f\u001a\u00020\nH\u0016J\u0008\u0010 \u001a\u00020\nH\u0016J\u0008\u0010!\u001a\u00020\nH\u0016J\u0008\u0010\"\u001a\u00020\nH\u0016J\u0008\u0010#\u001a\u00020\nH\u0016J\"\u0010$\u001a\u00020\n2\u0006\u0010%\u001a\u00020\u00072\u0006\u0010&\u001a\u00020\u00072\u0008\u0010\'\u001a\u0004\u0018\u00010(H\u0016J\u0012\u0010)\u001a\u00020\n2\u0008\u0010*\u001a\u0004\u0018\u00010+H\u0016J&\u0010,\u001a\u0004\u0018\u00010-2\u0006\u0010.\u001a\u00020/2\u0008\u00100\u001a\u0004\u0018\u0001012\u0008\u0010*\u001a\u0004\u0018\u00010+H\u0016J\u0008\u00102\u001a\u00020\nH\u0016J\u001a\u00103\u001a\u00020\n2\u0006\u00104\u001a\u00020-2\u0008\u0010*\u001a\u0004\u0018\u00010+H\u0016J\u0008\u00105\u001a\u00020\nH\u0016J\u0008\u00106\u001a\u00020\nH\u0016J\u0008\u00107\u001a\u00020\nH\u0016J\u0008\u00108\u001a\u00020\nH\u0016J\u0008\u00109\u001a\u00020\nH\u0016J\u0010\u0010:\u001a\u00020\n2\u0006\u0010;\u001a\u00020<H\u0016J\u0008\u0010=\u001a\u00020\nH\u0016J\u0008\u0010>\u001a\u00020\nH\u0016J\u0010\u0010?\u001a\u00020\n2\u0006\u0010@\u001a\u00020\u0005H\u0016J\u0010\u0010A\u001a\u00020\n2\u0006\u0010B\u001a\u00020\u0005H\u0016J\u0010\u0010C\u001a\u00020\n2\u0006\u0010D\u001a\u00020<H\u0016J\u0008\u0010E\u001a\u00020\nH\u0016J\u0018\u0010F\u001a\u00020\n2\u0006\u0010G\u001a\u00020H2\u0006\u0010I\u001a\u00020<H\u0016J\u0008\u0010J\u001a\u00020\nH\u0016J\u0008\u0010K\u001a\u00020\nH\u0016J\u0008\u0010L\u001a\u00020\nH\u0016J\u0008\u0010M\u001a\u00020\nH\u0016J\u0018\u0010N\u001a\u00020\n2\u0006\u0010O\u001a\u00020<2\u0006\u0010P\u001a\u00020HH\u0016J\u0010\u0010Q\u001a\u00020\n2\u0006\u0010R\u001a\u00020\u0005H\u0016J\u0008\u0010S\u001a\u00020\nH\u0016J\u0008\u0010T\u001a\u00020\nH\u0016R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082D\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082D\u00a2\u0006\u0002\n\u0000R\u001a\u0010\u0008\u001a\u0008\u0012\u0004\u0012\u00020\n0\t8VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u000b\u0010\u000cR\u001a\u0010\r\u001a\u0008\u0012\u0004\u0012\u00020\n0\t8VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u000e\u0010\u000cR\u001e\u0010\u000f\u001a\u00020\u00108\u0006@\u0006X\u0087.\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u0011\u0010\u0012\"\u0004\u0008\u0013\u0010\u0014R\u001a\u0010\u0015\u001a\u0008\u0012\u0004\u0012\u00020\n0\t8VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0016\u0010\u000cR\u001a\u0010\u0017\u001a\u0008\u0012\u0004\u0012\u00020\n0\t8VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0018\u0010\u000cR\u001a\u0010\u0019\u001a\u0008\u0012\u0004\u0012\u00020\n0\t8VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u001a\u0010\u000cR\u001a\u0010\u001b\u001a\u0008\u0012\u0004\u0012\u00020\n0\t8VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u001c\u0010\u000cR\u001a\u0010\u001d\u001a\u0008\u0012\u0004\u0012\u00020\n0\t8VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u001e\u0010\u000c\u00a8\u0006U"
    }
    d2 = {
        "Lco/uk/getmondo/card/CardFragment;",
        "Lco/uk/getmondo/common/fragments/BaseFragment;",
        "Lco/uk/getmondo/card/CardPresenter$View;",
        "()V",
        "KEY_FORGOT_PIN",
        "",
        "REQUEST_CODE_PIN",
        "",
        "activateReplacementCardClicks",
        "Lio/reactivex/Observable;",
        "",
        "getActivateReplacementCardClicks",
        "()Lio/reactivex/Observable;",
        "applyForOverdraftClicks",
        "getApplyForOverdraftClicks",
        "cardPresenter",
        "Lco/uk/getmondo/card/CardPresenter;",
        "getCardPresenter",
        "()Lco/uk/getmondo/card/CardPresenter;",
        "setCardPresenter",
        "(Lco/uk/getmondo/card/CardPresenter;)V",
        "cardReplacementClicks",
        "getCardReplacementClicks",
        "forgotPinClicks",
        "getForgotPinClicks",
        "manageOverdraftClicks",
        "getManageOverdraftClicks",
        "toggleCardClicks",
        "getToggleCardClicks",
        "topUpClicks",
        "getTopUpClicks",
        "hideActivateReplacementCardButton",
        "hideCardLoading",
        "hideCardReplacementButton",
        "hideOverdraftAction",
        "hideToggleButton",
        "onActivityResult",
        "requestCode",
        "resultCode",
        "data",
        "Landroid/content/Intent;",
        "onCreate",
        "savedInstanceState",
        "Landroid/os/Bundle;",
        "onCreateView",
        "Landroid/view/View;",
        "inflater",
        "Landroid/view/LayoutInflater;",
        "container",
        "Landroid/view/ViewGroup;",
        "onDestroyView",
        "onViewCreated",
        "view",
        "openActivateCard",
        "openCardReplacement",
        "openForgotPin",
        "openOverdraftManagement",
        "openOverdraftTour",
        "openTopUp",
        "isRetail",
        "",
        "setCardActive",
        "setCardInactive",
        "setExpirationDate",
        "expires",
        "setLastFourDigits",
        "lastDigits",
        "setToggleButtonEnabled",
        "enabled",
        "showActivateReplacementCardButton",
        "showBalance",
        "balance",
        "Lco/uk/getmondo/model/Amount;",
        "showOnToolbar",
        "showCardFrozen",
        "showCardLoading",
        "showCardReplacementButton",
        "showForgotPinDialog",
        "showOverdraftAction",
        "hasOverdraft",
        "limit",
        "showProcessorToken",
        "processorToken",
        "showTitle",
        "showToggleButton",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field public a:Lco/uk/getmondo/card/e;

.field private final c:I

.field private final d:Ljava/lang/String;

.field private e:Ljava/util/HashMap;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 25
    invoke-direct {p0}, Lco/uk/getmondo/common/f/a;-><init>()V

    .line 26
    const/4 v0, 0x1

    iput v0, p0, Lco/uk/getmondo/card/a;->c:I

    .line 27
    const-string v0, "KEY_FORGOT_PIN"

    iput-object v0, p0, Lco/uk/getmondo/card/a;->d:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public A()V
    .locals 1

    iget-object v0, p0, Lco/uk/getmondo/card/a;->e:Ljava/util/HashMap;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lco/uk/getmondo/card/a;->e:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    :cond_0
    return-void
.end method

.method public a(I)Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lco/uk/getmondo/card/a;->e:Ljava/util/HashMap;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lco/uk/getmondo/card/a;->e:Ljava/util/HashMap;

    :cond_0
    iget-object v0, p0, Lco/uk/getmondo/card/a;->e:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    if-nez v0, :cond_1

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getView()Landroid/view/View;

    move-result-object v0

    if-nez v0, :cond_2

    const/4 v0, 0x0

    :cond_1
    :goto_0
    return-object v0

    :cond_2
    invoke-virtual {v0, p1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/card/a;->e:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method public a()Lio/reactivex/n;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Lkotlin/n;",
            ">;"
        }
    .end annotation

    .prologue
    .line 65
    sget v0, Lco/uk/getmondo/c$a;->toggleCardActionView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/card/a;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/common/ui/ActionView;

    .line 219
    invoke-static {v0}, Lcom/b/a/c/c;->a(Landroid/view/View;)Lio/reactivex/n;

    move-result-object v1

    sget-object v0, Lcom/b/a/a/d;->a:Lcom/b/a/a/d;

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {v1, v0}, Lio/reactivex/n;->map(Lio/reactivex/c/h;)Lio/reactivex/n;

    move-result-object v0

    const-string v1, "RxView.clicks(this).map(VoidToUnit)"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 65
    return-object v0
.end method

.method public a(Lco/uk/getmondo/d/c;Z)V
    .locals 4

    .prologue
    const-string v0, "balance"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 99
    sget-object v0, Lkotlin/d/b/ab;->a:Lkotlin/d/b/ab;

    const v0, 0x7f0a0102

    invoke-virtual {p0, v0}, Lco/uk/getmondo/card/a;->getString(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "getString(R.string.card_balance_format)"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-virtual {p1}, Lco/uk/getmondo/d/c;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    array-length v2, v1

    invoke-static {v1, v2}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v1

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const-string v0, "java.lang.String.format(format, *args)"

    invoke-static {v1, v0}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 100
    if-eqz p2, :cond_0

    .line 101
    sget v0, Lco/uk/getmondo/c$a;->toolbar:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/card/a;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/Toolbar;

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/Toolbar;->setSubtitle(Ljava/lang/CharSequence;)V

    .line 104
    :goto_0
    return-void

    .line 103
    :cond_0
    sget v0, Lco/uk/getmondo/c$a;->topupActionView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/card/a;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/common/ui/ActionView;

    invoke-virtual {v0, v1}, Lco/uk/getmondo/common/ui/ActionView;->setActionSubtitle(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public a(Ljava/lang/String;)V
    .locals 1

    .prologue
    const-string v0, "processorToken"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 86
    sget v0, Lco/uk/getmondo/c$a;->tokenView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/card/a;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    check-cast p1, Ljava/lang/CharSequence;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 87
    sget v0, Lco/uk/getmondo/c$a;->tokenView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/card/a;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-static {v0}, Lco/uk/getmondo/common/ae;->a(Landroid/view/View;)V

    .line 88
    return-void
.end method

.method public a(Z)V
    .locals 1

    .prologue
    .line 122
    sget v0, Lco/uk/getmondo/c$a;->toggleCardActionView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/card/a;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/common/ui/ActionView;

    invoke-virtual {v0, p1}, Lco/uk/getmondo/common/ui/ActionView;->setEnabled(Z)V

    .line 123
    return-void
.end method

.method public a(ZLco/uk/getmondo/d/c;)V
    .locals 4

    .prologue
    const-string v0, "limit"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 194
    if-eqz p1, :cond_0

    .line 195
    sget v0, Lco/uk/getmondo/c$a;->manageOverdraftActionView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/card/a;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/common/ui/ActionView;

    invoke-static {v0}, Lco/uk/getmondo/common/ae;->a(Landroid/view/View;)V

    .line 196
    sget v0, Lco/uk/getmondo/c$a;->manageOverdraftActionView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/card/a;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/common/ui/ActionView;

    const v1, 0x7f0a00f9

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p2, v2, v3

    invoke-virtual {p0, v1, v2}, Lco/uk/getmondo/card/a;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lco/uk/getmondo/common/ui/ActionView;->setActionSubtitle(Ljava/lang/String;)V

    .line 197
    sget v0, Lco/uk/getmondo/c$a;->applyForOverdraftActionView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/card/a;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/common/ui/ActionView;

    invoke-static {v0}, Lco/uk/getmondo/common/ae;->b(Landroid/view/View;)V

    .line 201
    :goto_0
    return-void

    .line 199
    :cond_0
    sget v0, Lco/uk/getmondo/c$a;->applyForOverdraftActionView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/card/a;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/common/ui/ActionView;

    invoke-static {v0}, Lco/uk/getmondo/common/ae;->a(Landroid/view/View;)V

    .line 200
    sget v0, Lco/uk/getmondo/c$a;->manageOverdraftActionView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/card/a;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/common/ui/ActionView;

    invoke-static {v0}, Lco/uk/getmondo/common/ae;->b(Landroid/view/View;)V

    goto :goto_0
.end method

.method public a_()V
    .locals 1

    .prologue
    .line 156
    sget v0, Lco/uk/getmondo/c$a;->activateCardActionView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/card/a;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/common/ui/ActionView;

    invoke-static {v0}, Lco/uk/getmondo/common/ae;->a(Landroid/view/View;)V

    .line 157
    return-void
.end method

.method public b()Lio/reactivex/n;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Lkotlin/n;",
            ">;"
        }
    .end annotation

    .prologue
    .line 68
    sget v0, Lco/uk/getmondo/c$a;->orderReplacementActionView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/card/a;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/common/ui/ActionView;

    .line 220
    invoke-static {v0}, Lcom/b/a/c/c;->a(Landroid/view/View;)Lio/reactivex/n;

    move-result-object v1

    sget-object v0, Lcom/b/a/a/d;->a:Lcom/b/a/a/d;

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {v1, v0}, Lio/reactivex/n;->map(Lio/reactivex/c/h;)Lio/reactivex/n;

    move-result-object v0

    const-string v1, "RxView.clicks(this).map(VoidToUnit)"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 68
    return-object v0
.end method

.method public b(Ljava/lang/String;)V
    .locals 1

    .prologue
    const-string v0, "expires"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 91
    sget v0, Lco/uk/getmondo/c$a;->expiresView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/card/a;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    check-cast p1, Ljava/lang/CharSequence;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 92
    return-void
.end method

.method public b(Z)V
    .locals 4

    .prologue
    .line 178
    if-eqz p1, :cond_0

    .line 179
    invoke-virtual {p0}, Lco/uk/getmondo/card/a;->getContext()Landroid/content/Context;

    move-result-object v0

    sget-object v1, Lco/uk/getmondo/topup/bank/TopUpInstructionsActivity;->b:Lco/uk/getmondo/topup/bank/TopUpInstructionsActivity$a;

    invoke-virtual {p0}, Lco/uk/getmondo/card/a;->getContext()Landroid/content/Context;

    move-result-object v2

    const-string v3, "context"

    invoke-static {v2, v3}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Lco/uk/getmondo/topup/bank/TopUpInstructionsActivity$a;->a(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 182
    :goto_0
    return-void

    .line 181
    :cond_0
    invoke-virtual {p0}, Lco/uk/getmondo/card/a;->getActivity()Landroid/support/v4/app/j;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-static {v0}, Lco/uk/getmondo/topup/TopUpActivity;->a(Landroid/content/Context;)V

    goto :goto_0
.end method

.method public b_()V
    .locals 3

    .prologue
    .line 173
    new-instance v0, Lco/uk/getmondo/common/d/c;

    invoke-direct {v0}, Lco/uk/getmondo/common/d/c;-><init>()V

    .line 174
    invoke-virtual {p0}, Lco/uk/getmondo/card/a;->getFragmentManager()Landroid/support/v4/app/n;

    move-result-object v1

    iget-object v2, p0, Lco/uk/getmondo/card/a;->d:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lco/uk/getmondo/common/d/c;->show(Landroid/support/v4/app/n;Ljava/lang/String;)V

    .line 175
    return-void
.end method

.method public c()Lio/reactivex/n;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Lkotlin/n;",
            ">;"
        }
    .end annotation

    .prologue
    .line 71
    sget v0, Lco/uk/getmondo/c$a;->activateCardActionView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/card/a;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/common/ui/ActionView;

    .line 221
    invoke-static {v0}, Lcom/b/a/c/c;->a(Landroid/view/View;)Lio/reactivex/n;

    move-result-object v1

    sget-object v0, Lcom/b/a/a/d;->a:Lcom/b/a/a/d;

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {v1, v0}, Lio/reactivex/n;->map(Lio/reactivex/c/h;)Lio/reactivex/n;

    move-result-object v0

    const-string v1, "RxView.clicks(this).map(VoidToUnit)"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 71
    return-object v0
.end method

.method public c(Ljava/lang/String;)V
    .locals 1

    .prologue
    const-string v0, "lastDigits"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 95
    sget v0, Lco/uk/getmondo/c$a;->cardNumbers4Textview:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/card/a;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    check-cast p1, Ljava/lang/CharSequence;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 96
    return-void
.end method

.method public d()Lio/reactivex/n;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Lkotlin/n;",
            ">;"
        }
    .end annotation

    .prologue
    .line 74
    sget v0, Lco/uk/getmondo/c$a;->pinActionView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/card/a;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/common/ui/ActionView;

    .line 222
    invoke-static {v0}, Lcom/b/a/c/c;->a(Landroid/view/View;)Lio/reactivex/n;

    move-result-object v1

    sget-object v0, Lcom/b/a/a/d;->a:Lcom/b/a/a/d;

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {v1, v0}, Lio/reactivex/n;->map(Lio/reactivex/c/h;)Lio/reactivex/n;

    move-result-object v0

    const-string v1, "RxView.clicks(this).map(VoidToUnit)"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 74
    return-object v0
.end method

.method public e()Lio/reactivex/n;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Lkotlin/n;",
            ">;"
        }
    .end annotation

    .prologue
    .line 77
    sget v0, Lco/uk/getmondo/c$a;->topupActionView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/card/a;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/common/ui/ActionView;

    .line 223
    invoke-static {v0}, Lcom/b/a/c/c;->a(Landroid/view/View;)Lio/reactivex/n;

    move-result-object v1

    sget-object v0, Lcom/b/a/a/d;->a:Lcom/b/a/a/d;

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {v1, v0}, Lio/reactivex/n;->map(Lio/reactivex/c/h;)Lio/reactivex/n;

    move-result-object v0

    const-string v1, "RxView.clicks(this).map(VoidToUnit)"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 77
    return-object v0
.end method

.method public f()Lio/reactivex/n;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Lkotlin/n;",
            ">;"
        }
    .end annotation

    .prologue
    .line 80
    sget v0, Lco/uk/getmondo/c$a;->applyForOverdraftActionView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/card/a;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/common/ui/ActionView;

    .line 224
    invoke-static {v0}, Lcom/b/a/c/c;->a(Landroid/view/View;)Lio/reactivex/n;

    move-result-object v1

    sget-object v0, Lcom/b/a/a/d;->a:Lcom/b/a/a/d;

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {v1, v0}, Lio/reactivex/n;->map(Lio/reactivex/c/h;)Lio/reactivex/n;

    move-result-object v0

    const-string v1, "RxView.clicks(this).map(VoidToUnit)"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 80
    return-object v0
.end method

.method public g()Lio/reactivex/n;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Lkotlin/n;",
            ">;"
        }
    .end annotation

    .prologue
    .line 83
    sget v0, Lco/uk/getmondo/c$a;->manageOverdraftActionView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/card/a;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/common/ui/ActionView;

    .line 225
    invoke-static {v0}, Lcom/b/a/c/c;->a(Landroid/view/View;)Lio/reactivex/n;

    move-result-object v1

    sget-object v0, Lcom/b/a/a/d;->a:Lcom/b/a/a/d;

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {v1, v0}, Lio/reactivex/n;->map(Lio/reactivex/c/h;)Lio/reactivex/n;

    move-result-object v0

    const-string v1, "RxView.clicks(this).map(VoidToUnit)"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 83
    return-object v0
.end method

.method public h()V
    .locals 3

    .prologue
    .line 108
    sget v0, Lco/uk/getmondo/c$a;->toggleCardActionView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/card/a;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/common/ui/ActionView;

    const v1, 0x7f0a00fc

    invoke-virtual {p0, v1}, Lco/uk/getmondo/card/a;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lco/uk/getmondo/common/ui/ActionView;->setActionTitle(Ljava/lang/String;)V

    .line 109
    sget v0, Lco/uk/getmondo/c$a;->toggleCardActionView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/card/a;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/common/ui/ActionView;

    const v1, 0x7f0f00c1

    invoke-virtual {v0, v1}, Lco/uk/getmondo/common/ui/ActionView;->setActionTitleColor(I)V

    .line 110
    sget v0, Lco/uk/getmondo/c$a;->toggleCardActionView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/card/a;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/common/ui/ActionView;

    const v1, 0x7f020110

    invoke-virtual {v0, v1}, Lco/uk/getmondo/common/ui/ActionView;->setPlaceholderIcon(I)V

    .line 111
    sget v0, Lco/uk/getmondo/c$a;->cardView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/card/a;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    invoke-virtual {p0}, Lco/uk/getmondo/card/a;->getActivity()Landroid/support/v4/app/j;

    move-result-object v1

    const v2, 0x7f020084

    invoke-virtual {v1, v2}, Landroid/support/v4/app/j;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 112
    return-void
.end method

.method public i()V
    .locals 3

    .prologue
    .line 115
    sget v0, Lco/uk/getmondo/c$a;->toggleCardActionView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/card/a;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/common/ui/ActionView;

    const v1, 0x7f0a00f7

    invoke-virtual {p0, v1}, Lco/uk/getmondo/card/a;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lco/uk/getmondo/common/ui/ActionView;->setActionTitle(Ljava/lang/String;)V

    .line 116
    sget v0, Lco/uk/getmondo/c$a;->toggleCardActionView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/card/a;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/common/ui/ActionView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lco/uk/getmondo/common/ui/ActionView;->setActionTitleColor(I)V

    .line 117
    sget v0, Lco/uk/getmondo/c$a;->toggleCardActionView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/card/a;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/common/ui/ActionView;

    const v1, 0x7f020111

    invoke-virtual {v0, v1}, Lco/uk/getmondo/common/ui/ActionView;->setPlaceholderIcon(I)V

    .line 118
    sget v0, Lco/uk/getmondo/c$a;->cardView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/card/a;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    invoke-virtual {p0}, Lco/uk/getmondo/card/a;->getActivity()Landroid/support/v4/app/j;

    move-result-object v1

    const v2, 0x7f020083

    invoke-virtual {v1, v2}, Landroid/support/v4/app/j;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 119
    return-void
.end method

.method public j()V
    .locals 1

    .prologue
    .line 126
    sget v0, Lco/uk/getmondo/c$a;->toggleCardActionView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/card/a;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/common/ui/ActionView;

    invoke-static {v0}, Lco/uk/getmondo/common/ae;->a(Landroid/view/View;)V

    .line 127
    return-void
.end method

.method public k()V
    .locals 1

    .prologue
    .line 130
    sget v0, Lco/uk/getmondo/c$a;->toggleCardActionView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/card/a;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/common/ui/ActionView;

    invoke-static {v0}, Lco/uk/getmondo/common/ae;->b(Landroid/view/View;)V

    .line 131
    return-void
.end method

.method public l()V
    .locals 1

    .prologue
    .line 134
    sget v0, Lco/uk/getmondo/c$a;->orderReplacementActionView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/card/a;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/common/ui/ActionView;

    invoke-static {v0}, Lco/uk/getmondo/common/ae;->a(Landroid/view/View;)V

    .line 135
    return-void
.end method

.method public m()V
    .locals 1

    .prologue
    .line 138
    sget v0, Lco/uk/getmondo/c$a;->orderReplacementActionView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/card/a;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/common/ui/ActionView;

    invoke-static {v0}, Lco/uk/getmondo/common/ae;->b(Landroid/view/View;)V

    .line 139
    return-void
.end method

.method public n()V
    .locals 1

    .prologue
    .line 142
    invoke-virtual {p0}, Lco/uk/getmondo/card/a;->getActivity()Landroid/support/v4/app/j;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-static {v0}, Lco/uk/getmondo/card/CardReplacementActivity;->a(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lco/uk/getmondo/card/a;->startActivity(Landroid/content/Intent;)V

    .line 143
    return-void
.end method

.method public o()V
    .locals 1

    .prologue
    .line 146
    sget v0, Lco/uk/getmondo/c$a;->cardProgressbar:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/card/a;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    invoke-static {v0}, Lco/uk/getmondo/common/ae;->a(Landroid/view/View;)V

    .line 147
    sget v0, Lco/uk/getmondo/c$a;->toggleCardOverlayView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/card/a;->a(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Lco/uk/getmondo/common/ae;->a(Landroid/view/View;)V

    .line 148
    return-void
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 1

    .prologue
    .line 56
    .line 57
    iget v0, p0, Lco/uk/getmondo/card/a;->c:I

    if-ne p1, v0, :cond_1

    if-eqz p3, :cond_0

    const-string v0, "pin_error_msg"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 58
    const-string v0, "pin_error_msg"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lco/uk/getmondo/card/a;->d(Ljava/lang/String;)V

    .line 61
    :cond_0
    :goto_0
    return-void

    .line 60
    :cond_1
    invoke-super {p0, p1, p2, p3}, Lco/uk/getmondo/common/f/a;->onActivityResult(IILandroid/content/Intent;)V

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 32
    invoke-super {p0, p1}, Lco/uk/getmondo/common/f/a;->onCreate(Landroid/os/Bundle;)V

    .line 33
    invoke-virtual {p0}, Lco/uk/getmondo/card/a;->B()Lco/uk/getmondo/common/h/b/b;

    move-result-object v0

    invoke-interface {v0, p0}, Lco/uk/getmondo/common/h/b/b;->a(Lco/uk/getmondo/card/a;)V

    .line 34
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2

    .prologue
    const-string v0, "inflater"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 37
    const v0, 0x7f05009a

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public onDestroyView()V
    .locals 2

    .prologue
    .line 51
    iget-object v0, p0, Lco/uk/getmondo/card/a;->a:Lco/uk/getmondo/card/e;

    if-nez v0, :cond_0

    const-string v1, "cardPresenter"

    invoke-static {v1}, Lkotlin/d/b/l;->b(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0}, Lco/uk/getmondo/card/e;->b()V

    .line 52
    invoke-super {p0}, Lco/uk/getmondo/common/f/a;->onDestroyView()V

    .line 53
    invoke-virtual {p0}, Lco/uk/getmondo/card/a;->A()V

    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 3

    .prologue
    const-string v0, "view"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 41
    invoke-super {p0, p1, p2}, Lco/uk/getmondo/common/f/a;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 43
    iget-object v1, p0, Lco/uk/getmondo/card/a;->a:Lco/uk/getmondo/card/e;

    if-nez v1, :cond_0

    const-string v0, "cardPresenter"

    invoke-static {v0}, Lkotlin/d/b/l;->b(Ljava/lang/String;)V

    :cond_0
    move-object v0, p0

    check-cast v0, Lco/uk/getmondo/card/e$a;

    invoke-virtual {v1, v0}, Lco/uk/getmondo/card/e;->a(Lco/uk/getmondo/card/e$a;)V

    .line 45
    invoke-virtual {p0}, Lco/uk/getmondo/card/a;->getActivity()Landroid/support/v4/app/j;

    move-result-object v0

    if-nez v0, :cond_1

    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type co.uk.getmondo.main.HomeActivity"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    check-cast v0, Lco/uk/getmondo/main/HomeActivity;

    .line 46
    sget v1, Lco/uk/getmondo/c$a;->toolbar:I

    invoke-virtual {p0, v1}, Lco/uk/getmondo/card/a;->a(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/support/v7/widget/Toolbar;

    invoke-virtual {v0, v1}, Lco/uk/getmondo/main/HomeActivity;->setSupportActionBar(Landroid/support/v7/widget/Toolbar;)V

    .line 47
    sget v1, Lco/uk/getmondo/c$a;->toolbar:I

    invoke-virtual {p0, v1}, Lco/uk/getmondo/card/a;->a(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/support/v7/widget/Toolbar;

    const-string v2, "toolbar"

    invoke-static {v1, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lco/uk/getmondo/main/HomeActivity;->a(Landroid/support/v7/widget/Toolbar;)V

    .line 48
    return-void
.end method

.method public p()V
    .locals 1

    .prologue
    .line 151
    sget v0, Lco/uk/getmondo/c$a;->cardProgressbar:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/card/a;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    invoke-static {v0}, Lco/uk/getmondo/common/ae;->b(Landroid/view/View;)V

    .line 152
    sget v0, Lco/uk/getmondo/c$a;->toggleCardOverlayView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/card/a;->a(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Lco/uk/getmondo/common/ae;->b(Landroid/view/View;)V

    .line 153
    return-void
.end method

.method public r()V
    .locals 1

    .prologue
    .line 160
    sget v0, Lco/uk/getmondo/c$a;->activateCardActionView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/card/a;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/common/ui/ActionView;

    invoke-static {v0}, Lco/uk/getmondo/common/ae;->b(Landroid/view/View;)V

    .line 161
    return-void
.end method

.method public s()V
    .locals 4

    .prologue
    .line 164
    sget-object v1, Lco/uk/getmondo/card/activate/ActivateCardActivity;->b:Lco/uk/getmondo/card/activate/ActivateCardActivity$a;

    invoke-virtual {p0}, Lco/uk/getmondo/card/a;->getActivity()Landroid/support/v4/app/j;

    move-result-object v0

    const-string v2, "activity"

    invoke-static {v0, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/content/Context;

    const/4 v2, 0x1

    sget-object v3, Lco/uk/getmondo/common/activities/b$a;->b:Lco/uk/getmondo/common/activities/b$a;

    invoke-virtual {v1, v0, v2, v3}, Lco/uk/getmondo/card/activate/ActivateCardActivity$a;->a(Landroid/content/Context;ZLco/uk/getmondo/common/activities/b$a;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lco/uk/getmondo/card/a;->startActivity(Landroid/content/Intent;)V

    .line 165
    return-void
.end method

.method public t()V
    .locals 3

    .prologue
    .line 168
    new-instance v1, Landroid/content/Intent;

    invoke-virtual {p0}, Lco/uk/getmondo/card/a;->getActivity()Landroid/support/v4/app/j;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    const-class v2, Lco/uk/getmondo/pin/ForgotPinActivity;

    invoke-direct {v1, v0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 169
    iget v0, p0, Lco/uk/getmondo/card/a;->c:I

    invoke-virtual {p0, v1, v0}, Lco/uk/getmondo/card/a;->startActivityForResult(Landroid/content/Intent;I)V

    .line 170
    return-void
.end method

.method public v()V
    .locals 2

    .prologue
    .line 186
    sget v0, Lco/uk/getmondo/c$a;->toolbar:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/card/a;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/Toolbar;

    const v1, 0x7f0a02b0

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/Toolbar;->setTitle(I)V

    .line 187
    return-void
.end method

.method public w()V
    .locals 2

    .prologue
    .line 190
    sget v0, Lco/uk/getmondo/c$a;->toolbar:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/card/a;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/Toolbar;

    const v1, 0x7f0a02b1

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/Toolbar;->setTitle(I)V

    .line 191
    return-void
.end method

.method public x()V
    .locals 1

    .prologue
    .line 205
    sget v0, Lco/uk/getmondo/c$a;->applyForOverdraftActionView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/card/a;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/common/ui/ActionView;

    invoke-static {v0}, Lco/uk/getmondo/common/ae;->b(Landroid/view/View;)V

    .line 206
    sget v0, Lco/uk/getmondo/c$a;->manageOverdraftActionView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/card/a;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/common/ui/ActionView;

    invoke-static {v0}, Lco/uk/getmondo/common/ae;->b(Landroid/view/View;)V

    .line 207
    return-void
.end method

.method public y()V
    .locals 3

    .prologue
    .line 211
    sget-object v1, Lco/uk/getmondo/overdraft/ChangeOverdraftLimitActivity;->b:Lco/uk/getmondo/overdraft/ChangeOverdraftLimitActivity$a;

    invoke-virtual {p0}, Lco/uk/getmondo/card/a;->getActivity()Landroid/support/v4/app/j;

    move-result-object v0

    const-string v2, "activity"

    invoke-static {v0, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/content/Context;

    invoke-virtual {v1, v0}, Lco/uk/getmondo/overdraft/ChangeOverdraftLimitActivity$a;->a(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lco/uk/getmondo/card/a;->startActivity(Landroid/content/Intent;)V

    .line 212
    return-void
.end method

.method public z()V
    .locals 3

    .prologue
    .line 215
    sget-object v1, Lco/uk/getmondo/overdraft/ChangeOverdraftLimitActivity;->b:Lco/uk/getmondo/overdraft/ChangeOverdraftLimitActivity$a;

    invoke-virtual {p0}, Lco/uk/getmondo/card/a;->getActivity()Landroid/support/v4/app/j;

    move-result-object v0

    const-string v2, "activity"

    invoke-static {v0, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/content/Context;

    invoke-virtual {v1, v0}, Lco/uk/getmondo/overdraft/ChangeOverdraftLimitActivity$a;->a(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lco/uk/getmondo/card/a;->startActivity(Landroid/content/Intent;)V

    .line 216
    return-void
.end method
