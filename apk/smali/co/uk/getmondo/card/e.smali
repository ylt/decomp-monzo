.class public final Lco/uk/getmondo/card/e;
.super Lco/uk/getmondo/common/ui/b;
.source "CardPresenter.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lco/uk/getmondo/card/e$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lco/uk/getmondo/common/ui/b",
        "<",
        "Lco/uk/getmondo/card/e$a;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000b\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u000b\n\u0002\u0008\u0004\u0008\u0007\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00020\u0001:\u0001#BS\u0008\u0007\u0012\u0008\u0008\u0001\u0010\u0003\u001a\u00020\u0004\u0012\u0008\u0008\u0001\u0010\u0005\u001a\u00020\u0004\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b\u0012\u0006\u0010\u000c\u001a\u00020\r\u0012\u0006\u0010\u000e\u001a\u00020\u000f\u0012\u0006\u0010\u0010\u001a\u00020\u0011\u0012\u0006\u0010\u0012\u001a\u00020\u0013\u00a2\u0006\u0002\u0010\u0014J\u0010\u0010\u0015\u001a\u00020\u00162\u0006\u0010\u0017\u001a\u00020\u0016H\u0002J\u0018\u0010\u0018\u001a\u00020\u00192\u0006\u0010\u001a\u001a\u00020\u00022\u0006\u0010\u001b\u001a\u00020\u001cH\u0002J\u0010\u0010\u001d\u001a\u00020\u00192\u0006\u0010\u001a\u001a\u00020\u0002H\u0016J\u0018\u0010\u001e\u001a\u00020\u00192\u0006\u0010\u001f\u001a\u00020 2\u0006\u0010\u001a\u001a\u00020\u0002H\u0002J\u0018\u0010!\u001a\u00020\u00192\u0006\u0010\u001f\u001a\u00020 2\u0006\u0010\u001a\u001a\u00020\u0002H\u0002J\u0018\u0010\"\u001a\u00020\u00192\u0006\u0010\u001a\u001a\u00020\u00022\u0006\u0010\u001b\u001a\u00020\u001cH\u0002R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u000fX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000c\u001a\u00020\rX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0010\u001a\u00020\u0011X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0012\u001a\u00020\u0013X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006$"
    }
    d2 = {
        "Lco/uk/getmondo/card/CardPresenter;",
        "Lco/uk/getmondo/common/ui/BasePresenter;",
        "Lco/uk/getmondo/card/CardPresenter$View;",
        "uiScheduler",
        "Lio/reactivex/Scheduler;",
        "ioScheduler",
        "accountManager",
        "Lco/uk/getmondo/common/accounts/AccountManager;",
        "apiErrorHandler",
        "Lco/uk/getmondo/common/errors/ApiErrorHandler;",
        "cardManager",
        "Lco/uk/getmondo/card/CardManager;",
        "balanceRepository",
        "Lco/uk/getmondo/account_balance/BalanceRepository;",
        "analyticsService",
        "Lco/uk/getmondo/common/AnalyticsService;",
        "developerOptionsPreferences",
        "Lco/uk/getmondo/developer_options/DeveloperOptionsPreferences;",
        "overdraftManager",
        "Lco/uk/getmondo/overdraft/data/OverdraftManager;",
        "(Lio/reactivex/Scheduler;Lio/reactivex/Scheduler;Lco/uk/getmondo/common/accounts/AccountManager;Lco/uk/getmondo/common/errors/ApiErrorHandler;Lco/uk/getmondo/card/CardManager;Lco/uk/getmondo/account_balance/BalanceRepository;Lco/uk/getmondo/common/AnalyticsService;Lco/uk/getmondo/developer_options/DeveloperOptionsPreferences;Lco/uk/getmondo/overdraft/data/OverdraftManager;)V",
        "formatExpirationDate",
        "",
        "expirationDate",
        "onCard",
        "",
        "view",
        "card",
        "Lco/uk/getmondo/model/Card;",
        "register",
        "setActivateReplacementButtonVisible",
        "visible",
        "",
        "setCardReplacementButtonVisible",
        "updateUiForCard",
        "View",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field private final c:Lio/reactivex/u;

.field private final d:Lio/reactivex/u;

.field private final e:Lco/uk/getmondo/common/accounts/b;

.field private final f:Lco/uk/getmondo/common/e/a;

.field private final g:Lco/uk/getmondo/card/c;

.field private final h:Lco/uk/getmondo/a/a;

.field private final i:Lco/uk/getmondo/common/a;

.field private final j:Lco/uk/getmondo/developer_options/a;

.field private final k:Lco/uk/getmondo/overdraft/a/c;


# direct methods
.method public constructor <init>(Lio/reactivex/u;Lio/reactivex/u;Lco/uk/getmondo/common/accounts/b;Lco/uk/getmondo/common/e/a;Lco/uk/getmondo/card/c;Lco/uk/getmondo/a/a;Lco/uk/getmondo/common/a;Lco/uk/getmondo/developer_options/a;Lco/uk/getmondo/overdraft/a/c;)V
    .locals 1

    .prologue
    const-string v0, "uiScheduler"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "ioScheduler"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "accountManager"

    invoke-static {p3, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "apiErrorHandler"

    invoke-static {p4, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "cardManager"

    invoke-static {p5, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "balanceRepository"

    invoke-static {p6, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "analyticsService"

    invoke-static {p7, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "developerOptionsPreferences"

    invoke-static {p8, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "overdraftManager"

    invoke-static {p9, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 38
    invoke-direct {p0}, Lco/uk/getmondo/common/ui/b;-><init>()V

    iput-object p1, p0, Lco/uk/getmondo/card/e;->c:Lio/reactivex/u;

    iput-object p2, p0, Lco/uk/getmondo/card/e;->d:Lio/reactivex/u;

    iput-object p3, p0, Lco/uk/getmondo/card/e;->e:Lco/uk/getmondo/common/accounts/b;

    iput-object p4, p0, Lco/uk/getmondo/card/e;->f:Lco/uk/getmondo/common/e/a;

    iput-object p5, p0, Lco/uk/getmondo/card/e;->g:Lco/uk/getmondo/card/c;

    iput-object p6, p0, Lco/uk/getmondo/card/e;->h:Lco/uk/getmondo/a/a;

    iput-object p7, p0, Lco/uk/getmondo/card/e;->i:Lco/uk/getmondo/common/a;

    iput-object p8, p0, Lco/uk/getmondo/card/e;->j:Lco/uk/getmondo/developer_options/a;

    iput-object p9, p0, Lco/uk/getmondo/card/e;->k:Lco/uk/getmondo/overdraft/a/c;

    return-void
.end method

.method public static final synthetic a(Lco/uk/getmondo/card/e;)Lco/uk/getmondo/common/e/a;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lco/uk/getmondo/card/e;->f:Lco/uk/getmondo/common/e/a;

    return-object v0
.end method

.method private final a(Ljava/lang/String;)Ljava/lang/String;
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v2, 0x0

    .line 150
    move-object v0, p1

    check-cast v0, Ljava/lang/CharSequence;

    new-array v1, v6, [Ljava/lang/String;

    const-string v3, "/"

    aput-object v3, v1, v2

    const/4 v4, 0x6

    const/4 v5, 0x0

    move v3, v2

    invoke-static/range {v0 .. v5}, Lkotlin/h/j;->b(Ljava/lang/CharSequence;[Ljava/lang/String;ZIILjava/lang/Object;)Ljava/util/List;

    move-result-object v1

    .line 151
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v0

    if-ne v0, v7, :cond_1

    invoke-interface {v1, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-ne v0, v8, :cond_1

    .line 152
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "/"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-interface {v1, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    if-nez v0, :cond_0

    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type java.lang.String"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    invoke-virtual {v0, v7, v8}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    const-string v1, "(this as java.lang.Strin\u2026ing(startIndex, endIndex)"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 151
    :cond_1
    return-object p1
.end method

.method private final a(Lco/uk/getmondo/card/e$a;Lco/uk/getmondo/d/g;)V
    .locals 2

    .prologue
    .line 139
    iget-object v0, p0, Lco/uk/getmondo/card/e;->e:Lco/uk/getmondo/common/accounts/b;

    invoke-virtual {v0}, Lco/uk/getmondo/common/accounts/b;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 140
    invoke-virtual {p2}, Lco/uk/getmondo/d/g;->e()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lco/uk/getmondo/common/k/p;->f(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 141
    const-string v1, "token"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {p1, v0}, Lco/uk/getmondo/card/e$a;->a(Ljava/lang/String;)V

    .line 143
    :cond_0
    invoke-virtual {p2}, Lco/uk/getmondo/d/g;->c()Ljava/lang/String;

    move-result-object v0

    const-string v1, "card.expires"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lco/uk/getmondo/card/e;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 144
    invoke-interface {p1, v0}, Lco/uk/getmondo/card/e$a;->b(Ljava/lang/String;)V

    .line 145
    invoke-virtual {p2}, Lco/uk/getmondo/d/g;->d()Ljava/lang/String;

    move-result-object v0

    const-string v1, "card.lastDigits"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {p1, v0}, Lco/uk/getmondo/card/e$a;->c(Ljava/lang/String;)V

    .line 146
    invoke-direct {p0, p1, p2}, Lco/uk/getmondo/card/e;->b(Lco/uk/getmondo/card/e$a;Lco/uk/getmondo/d/g;)V

    .line 147
    return-void
.end method

.method public static final synthetic a(Lco/uk/getmondo/card/e;Lco/uk/getmondo/card/e$a;Lco/uk/getmondo/d/g;)V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0, p1, p2}, Lco/uk/getmondo/card/e;->a(Lco/uk/getmondo/card/e$a;Lco/uk/getmondo/d/g;)V

    return-void
.end method

.method private final a(ZLco/uk/getmondo/card/e$a;)V
    .locals 0

    .prologue
    .line 185
    if-eqz p1, :cond_0

    .line 186
    invoke-interface {p2}, Lco/uk/getmondo/card/e$a;->a_()V

    .line 189
    :goto_0
    return-void

    .line 188
    :cond_0
    invoke-interface {p2}, Lco/uk/getmondo/card/e$a;->r()V

    goto :goto_0
.end method

.method public static final synthetic b(Lco/uk/getmondo/card/e;)Lco/uk/getmondo/common/accounts/b;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lco/uk/getmondo/card/e;->e:Lco/uk/getmondo/common/accounts/b;

    return-object v0
.end method

.method private final b(Lco/uk/getmondo/card/e$a;Lco/uk/getmondo/d/g;)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 157
    invoke-virtual {p2}, Lco/uk/getmondo/d/g;->f()Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    .line 158
    :goto_0
    invoke-virtual {p2}, Lco/uk/getmondo/d/g;->g()Lco/uk/getmondo/api/model/a;

    move-result-object v3

    if-nez v3, :cond_2

    .line 175
    :goto_1
    iget-object v3, p0, Lco/uk/getmondo/card/e;->e:Lco/uk/getmondo/common/accounts/b;

    invoke-virtual {v3}, Lco/uk/getmondo/common/accounts/b;->b()Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lco/uk/getmondo/card/e;->j:Lco/uk/getmondo/developer_options/a;

    invoke-virtual {v3}, Lco/uk/getmondo/developer_options/a;->a()Z

    move-result v3

    if-eqz v3, :cond_3

    :cond_0
    move v3, v1

    .line 180
    :goto_2
    invoke-virtual {p2}, Lco/uk/getmondo/d/g;->f()Z

    move-result v4

    invoke-direct {p0, v4, p1}, Lco/uk/getmondo/card/e;->a(ZLco/uk/getmondo/card/e$a;)V

    .line 181
    if-eqz v0, :cond_4

    if-eqz v3, :cond_4

    :goto_3
    invoke-direct {p0, v1, p1}, Lco/uk/getmondo/card/e;->b(ZLco/uk/getmondo/card/e$a;)V

    .line 182
    return-void

    :cond_1
    move v0, v2

    .line 157
    goto :goto_0

    .line 158
    :cond_2
    sget-object v4, Lco/uk/getmondo/card/f;->a:[I

    invoke-virtual {v3}, Lco/uk/getmondo/api/model/a;->ordinal()I

    move-result v3

    aget v3, v4, v3

    packed-switch v3, :pswitch_data_0

    goto :goto_1

    .line 160
    :pswitch_0
    invoke-interface {p1}, Lco/uk/getmondo/card/e$a;->i()V

    .line 162
    invoke-interface {p1}, Lco/uk/getmondo/card/e$a;->j()V

    .line 163
    invoke-interface {p1}, Lco/uk/getmondo/card/e$a;->v()V

    move v0, v2

    goto :goto_1

    .line 166
    :pswitch_1
    invoke-interface {p1}, Lco/uk/getmondo/card/e$a;->w()V

    .line 167
    invoke-interface {p1}, Lco/uk/getmondo/card/e$a;->h()V

    .line 168
    invoke-interface {p1}, Lco/uk/getmondo/card/e$a;->j()V

    goto :goto_1

    .line 171
    :pswitch_2
    invoke-interface {p1}, Lco/uk/getmondo/card/e$a;->w()V

    .line 172
    invoke-interface {p1}, Lco/uk/getmondo/card/e$a;->h()V

    .line 173
    invoke-interface {p1}, Lco/uk/getmondo/card/e$a;->k()V

    goto :goto_1

    :cond_3
    move v3, v2

    .line 175
    goto :goto_2

    :cond_4
    move v1, v2

    .line 181
    goto :goto_3

    .line 158
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private final b(ZLco/uk/getmondo/card/e$a;)V
    .locals 0

    .prologue
    .line 193
    if-eqz p1, :cond_0

    .line 194
    invoke-interface {p2}, Lco/uk/getmondo/card/e$a;->l()V

    .line 197
    :goto_0
    return-void

    .line 196
    :cond_0
    invoke-interface {p2}, Lco/uk/getmondo/card/e$a;->m()V

    goto :goto_0
.end method

.method public static final synthetic c(Lco/uk/getmondo/card/e;)Lco/uk/getmondo/card/c;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lco/uk/getmondo/card/e;->g:Lco/uk/getmondo/card/c;

    return-object v0
.end method

.method public static final synthetic d(Lco/uk/getmondo/card/e;)Lco/uk/getmondo/common/a;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lco/uk/getmondo/card/e;->i:Lco/uk/getmondo/common/a;

    return-object v0
.end method

.method public static final synthetic e(Lco/uk/getmondo/card/e;)Lio/reactivex/u;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lco/uk/getmondo/card/e;->c:Lio/reactivex/u;

    return-object v0
.end method

.method public static final synthetic f(Lco/uk/getmondo/card/e;)Lio/reactivex/u;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lco/uk/getmondo/card/e;->d:Lio/reactivex/u;

    return-object v0
.end method


# virtual methods
.method public a(Lco/uk/getmondo/card/e$a;)V
    .locals 6

    .prologue
    const-string v0, "view"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v0, p1

    .line 41
    check-cast v0, Lco/uk/getmondo/common/ui/f;

    invoke-super {p0, v0}, Lco/uk/getmondo/common/ui/b;->a(Lco/uk/getmondo/common/ui/f;)V

    .line 42
    iget-object v0, p0, Lco/uk/getmondo/card/e;->i:Lco/uk/getmondo/common/a;

    sget-object v1, Lco/uk/getmondo/api/model/tracking/Impression;->Companion:Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    invoke-virtual {v1}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->s()Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v1

    invoke-virtual {v0, v1}, Lco/uk/getmondo/common/a;->a(Lco/uk/getmondo/api/model/tracking/Impression;)V

    .line 44
    iget-object v0, p0, Lco/uk/getmondo/card/e;->e:Lco/uk/getmondo/common/accounts/b;

    invoke-virtual {v0}, Lco/uk/getmondo/common/accounts/b;->b()Z

    move-result v3

    .line 45
    iget-object v0, p0, Lco/uk/getmondo/card/e;->e:Lco/uk/getmondo/common/accounts/b;

    invoke-virtual {v0}, Lco/uk/getmondo/common/accounts/b;->c()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-static {}, Lkotlin/d/b/l;->a()V

    .line 46
    :cond_0
    if-eqz v3, :cond_1

    .line 47
    iget-object v1, p0, Lco/uk/getmondo/card/e;->b:Lio/reactivex/b/a;

    .line 48
    iget-object v2, p0, Lco/uk/getmondo/card/e;->k:Lco/uk/getmondo/overdraft/a/c;

    invoke-virtual {v2, v0}, Lco/uk/getmondo/overdraft/a/c;->b(Ljava/lang/String;)Lio/reactivex/n;

    move-result-object v2

    new-instance v0, Lco/uk/getmondo/card/e$b;

    invoke-direct {v0, p1}, Lco/uk/getmondo/card/e$b;-><init>(Lco/uk/getmondo/card/e$a;)V

    check-cast v0, Lio/reactivex/c/g;

    invoke-virtual {v2, v0}, Lio/reactivex/n;->subscribe(Lio/reactivex/c/g;)Lio/reactivex/b/b;

    move-result-object v0

    const-string v2, "overdraftManager.status(\u2026  }\n                    }"

    invoke-static {v0, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 47
    invoke-static {v1, v0}, Lco/uk/getmondo/common/j/f;->a(Lio/reactivex/b/a;Lio/reactivex/b/b;)Lio/reactivex/b/a;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/card/e;->b:Lio/reactivex/b/a;

    .line 56
    iget-object v1, p0, Lco/uk/getmondo/card/e;->b:Lio/reactivex/b/a;

    .line 57
    invoke-interface {p1}, Lco/uk/getmondo/card/e$a;->f()Lio/reactivex/n;

    move-result-object v2

    new-instance v0, Lco/uk/getmondo/card/e$k;

    invoke-direct {v0, p1}, Lco/uk/getmondo/card/e$k;-><init>(Lco/uk/getmondo/card/e$a;)V

    check-cast v0, Lio/reactivex/c/g;

    invoke-virtual {v2, v0}, Lio/reactivex/n;->subscribe(Lio/reactivex/c/g;)Lio/reactivex/b/b;

    move-result-object v0

    const-string v2, "view.applyForOverdraftCl\u2026iew.openOverdraftTour() }"

    invoke-static {v0, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 56
    invoke-static {v1, v0}, Lco/uk/getmondo/common/j/f;->a(Lio/reactivex/b/a;Lio/reactivex/b/b;)Lio/reactivex/b/a;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/card/e;->b:Lio/reactivex/b/a;

    .line 59
    iget-object v1, p0, Lco/uk/getmondo/card/e;->b:Lio/reactivex/b/a;

    .line 60
    invoke-interface {p1}, Lco/uk/getmondo/card/e$a;->g()Lio/reactivex/n;

    move-result-object v2

    new-instance v0, Lco/uk/getmondo/card/e$l;

    invoke-direct {v0, p1}, Lco/uk/getmondo/card/e$l;-><init>(Lco/uk/getmondo/card/e$a;)V

    check-cast v0, Lio/reactivex/c/g;

    invoke-virtual {v2, v0}, Lio/reactivex/n;->subscribe(Lio/reactivex/c/g;)Lio/reactivex/b/b;

    move-result-object v0

    const-string v2, "view.manageOverdraftClic\u2026enOverdraftManagement() }"

    invoke-static {v0, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 59
    invoke-static {v1, v0}, Lco/uk/getmondo/common/j/f;->a(Lio/reactivex/b/a;Lio/reactivex/b/b;)Lio/reactivex/b/a;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/card/e;->b:Lio/reactivex/b/a;

    .line 63
    :cond_1
    iget-object v4, p0, Lco/uk/getmondo/card/e;->b:Lio/reactivex/b/a;

    .line 65
    iget-object v0, p0, Lco/uk/getmondo/card/e;->h:Lco/uk/getmondo/a/a;

    iget-object v1, p0, Lco/uk/getmondo/card/e;->e:Lco/uk/getmondo/common/accounts/b;

    invoke-virtual {v1}, Lco/uk/getmondo/common/accounts/b;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lco/uk/getmondo/a/a;->a(Ljava/lang/String;)Lio/reactivex/n;

    move-result-object v0

    .line 64
    iget-object v1, p0, Lco/uk/getmondo/card/e;->c:Lio/reactivex/u;

    invoke-virtual {v0, v1}, Lio/reactivex/n;->observeOn(Lio/reactivex/u;)Lio/reactivex/n;

    move-result-object v5

    .line 65
    new-instance v0, Lco/uk/getmondo/card/e$m;

    invoke-direct {v0, p1, v3}, Lco/uk/getmondo/card/e$m;-><init>(Lco/uk/getmondo/card/e$a;Z)V

    check-cast v0, Lio/reactivex/c/g;

    sget-object v1, Lco/uk/getmondo/card/e$n;->a:Lco/uk/getmondo/card/e$n;

    check-cast v1, Lkotlin/d/a/b;

    if-eqz v1, :cond_2

    new-instance v2, Lco/uk/getmondo/card/g;

    invoke-direct {v2, v1}, Lco/uk/getmondo/card/g;-><init>(Lkotlin/d/a/b;)V

    move-object v1, v2

    :cond_2
    check-cast v1, Lio/reactivex/c/g;

    invoke-virtual {v5, v0, v1}, Lio/reactivex/n;->subscribe(Lio/reactivex/c/g;Lio/reactivex/c/g;)Lio/reactivex/b/b;

    move-result-object v0

    const-string v1, "balanceRepository.balanc\u2026ailAccount) }, Timber::e)"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 63
    invoke-static {v4, v0}, Lco/uk/getmondo/common/j/f;->a(Lio/reactivex/b/a;Lio/reactivex/b/b;)Lio/reactivex/b/a;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/card/e;->b:Lio/reactivex/b/a;

    .line 69
    iget-object v2, p0, Lco/uk/getmondo/card/e;->b:Lio/reactivex/b/a;

    .line 72
    iget-object v0, p0, Lco/uk/getmondo/card/e;->g:Lco/uk/getmondo/card/c;

    invoke-virtual {v0}, Lco/uk/getmondo/card/c;->c()Lio/reactivex/b;

    move-result-object v0

    .line 70
    iget-object v1, p0, Lco/uk/getmondo/card/e;->d:Lio/reactivex/u;

    invoke-virtual {v0, v1}, Lio/reactivex/b;->b(Lio/reactivex/u;)Lio/reactivex/b;

    move-result-object v0

    .line 71
    iget-object v1, p0, Lco/uk/getmondo/card/e;->c:Lio/reactivex/u;

    invoke-virtual {v0, v1}, Lio/reactivex/b;->a(Lio/reactivex/u;)Lio/reactivex/b;

    move-result-object v4

    .line 73
    sget-object v0, Lco/uk/getmondo/card/e$o;->a:Lco/uk/getmondo/card/e$o;

    check-cast v0, Lio/reactivex/c/a;

    .line 74
    new-instance v1, Lco/uk/getmondo/card/e$p;

    invoke-direct {v1, p0, p1}, Lco/uk/getmondo/card/e$p;-><init>(Lco/uk/getmondo/card/e;Lco/uk/getmondo/card/e$a;)V

    check-cast v1, Lio/reactivex/c/g;

    .line 72
    invoke-virtual {v4, v0, v1}, Lio/reactivex/b;->a(Lio/reactivex/c/a;Lio/reactivex/c/g;)Lio/reactivex/b/b;

    move-result-object v0

    const-string v1, "cardManager.syncCard()\n \u2026view) }\n                )"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 69
    invoke-static {v2, v0}, Lco/uk/getmondo/common/j/f;->a(Lio/reactivex/b/a;Lio/reactivex/b/b;)Lio/reactivex/b/a;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/card/e;->b:Lio/reactivex/b/a;

    .line 77
    iget-object v2, p0, Lco/uk/getmondo/card/e;->b:Lio/reactivex/b/a;

    .line 80
    iget-object v0, p0, Lco/uk/getmondo/card/e;->h:Lco/uk/getmondo/a/a;

    iget-object v1, p0, Lco/uk/getmondo/card/e;->e:Lco/uk/getmondo/common/accounts/b;

    invoke-virtual {v1}, Lco/uk/getmondo/common/accounts/b;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lco/uk/getmondo/a/a;->b(Ljava/lang/String;)Lio/reactivex/b;

    move-result-object v0

    .line 78
    iget-object v1, p0, Lco/uk/getmondo/card/e;->d:Lio/reactivex/u;

    invoke-virtual {v0, v1}, Lio/reactivex/b;->b(Lio/reactivex/u;)Lio/reactivex/b;

    move-result-object v0

    .line 79
    iget-object v1, p0, Lco/uk/getmondo/card/e;->c:Lio/reactivex/u;

    invoke-virtual {v0, v1}, Lio/reactivex/b;->a(Lio/reactivex/u;)Lio/reactivex/b;

    move-result-object v4

    .line 80
    sget-object v0, Lco/uk/getmondo/card/e$q;->a:Lco/uk/getmondo/card/e$q;

    check-cast v0, Lio/reactivex/c/a;

    new-instance v1, Lco/uk/getmondo/card/e$r;

    invoke-direct {v1, p0, p1}, Lco/uk/getmondo/card/e$r;-><init>(Lco/uk/getmondo/card/e;Lco/uk/getmondo/card/e$a;)V

    check-cast v1, Lio/reactivex/c/g;

    invoke-virtual {v4, v0, v1}, Lio/reactivex/b;->a(Lio/reactivex/c/a;Lio/reactivex/c/g;)Lio/reactivex/b/b;

    move-result-object v0

    const-string v1, "balanceRepository.refres\u2026ndleError(error, view) })"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 77
    invoke-static {v2, v0}, Lco/uk/getmondo/common/j/f;->a(Lio/reactivex/b/a;Lio/reactivex/b/b;)Lio/reactivex/b/a;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/card/e;->b:Lio/reactivex/b/a;

    .line 82
    iget-object v4, p0, Lco/uk/getmondo/card/e;->b:Lio/reactivex/b/a;

    .line 84
    iget-object v0, p0, Lco/uk/getmondo/card/e;->g:Lco/uk/getmondo/card/c;

    invoke-virtual {v0}, Lco/uk/getmondo/card/c;->a()Lio/reactivex/n;

    move-result-object v0

    .line 83
    iget-object v1, p0, Lco/uk/getmondo/card/e;->c:Lio/reactivex/u;

    invoke-virtual {v0, v1}, Lio/reactivex/n;->observeOn(Lio/reactivex/u;)Lio/reactivex/n;

    move-result-object v5

    .line 84
    new-instance v0, Lco/uk/getmondo/card/e$c;

    invoke-direct {v0, p0, p1}, Lco/uk/getmondo/card/e$c;-><init>(Lco/uk/getmondo/card/e;Lco/uk/getmondo/card/e$a;)V

    check-cast v0, Lio/reactivex/c/g;

    sget-object v1, Lco/uk/getmondo/card/e$d;->a:Lco/uk/getmondo/card/e$d;

    check-cast v1, Lkotlin/d/a/b;

    if-eqz v1, :cond_3

    new-instance v2, Lco/uk/getmondo/card/g;

    invoke-direct {v2, v1}, Lco/uk/getmondo/card/g;-><init>(Lkotlin/d/a/b;)V

    move-object v1, v2

    :cond_3
    check-cast v1, Lio/reactivex/c/g;

    invoke-virtual {v5, v0, v1}, Lio/reactivex/n;->subscribe(Lio/reactivex/c/g;Lio/reactivex/c/g;)Lio/reactivex/b/b;

    move-result-object v0

    const-string v1, "cardManager.card()\n     \u2026view, card) }, Timber::e)"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 82
    invoke-static {v4, v0}, Lco/uk/getmondo/common/j/f;->a(Lio/reactivex/b/a;Lio/reactivex/b/b;)Lio/reactivex/b/a;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/card/e;->b:Lio/reactivex/b/a;

    .line 86
    iget-object v1, p0, Lco/uk/getmondo/card/e;->b:Lio/reactivex/b/a;

    .line 87
    invoke-interface {p1}, Lco/uk/getmondo/card/e$a;->e()Lio/reactivex/n;

    move-result-object v2

    new-instance v0, Lco/uk/getmondo/card/e$e;

    invoke-direct {v0, p0, p1}, Lco/uk/getmondo/card/e$e;-><init>(Lco/uk/getmondo/card/e;Lco/uk/getmondo/card/e$a;)V

    check-cast v0, Lio/reactivex/c/g;

    invoke-virtual {v2, v0}, Lio/reactivex/n;->subscribe(Lio/reactivex/c/g;)Lio/reactivex/b/b;

    move-result-object v0

    const-string v2, "view.topUpClicks\n       \u2026anager.isRetailAccount) }"

    invoke-static {v0, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 86
    invoke-static {v1, v0}, Lco/uk/getmondo/common/j/f;->a(Lio/reactivex/b/a;Lio/reactivex/b/b;)Lio/reactivex/b/a;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/card/e;->b:Lio/reactivex/b/a;

    .line 89
    iget-object v1, p0, Lco/uk/getmondo/card/e;->b:Lio/reactivex/b/a;

    .line 118
    invoke-interface {p1}, Lco/uk/getmondo/card/e$a;->a()Lio/reactivex/n;

    move-result-object v2

    .line 90
    new-instance v0, Lco/uk/getmondo/card/e$f;

    invoke-direct {v0, p0, p1}, Lco/uk/getmondo/card/e$f;-><init>(Lco/uk/getmondo/card/e;Lco/uk/getmondo/card/e$a;)V

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {v2, v0}, Lio/reactivex/n;->flatMapCompletable(Lio/reactivex/c/h;)Lio/reactivex/b;

    move-result-object v0

    .line 118
    invoke-virtual {v0}, Lio/reactivex/b;->c()Lio/reactivex/b/b;

    move-result-object v0

    const-string v2, "view.toggleCardClicks\n  \u2026             .subscribe()"

    invoke-static {v0, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 89
    invoke-static {v1, v0}, Lco/uk/getmondo/common/j/f;->a(Lio/reactivex/b/a;Lio/reactivex/b/b;)Lio/reactivex/b/a;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/card/e;->b:Lio/reactivex/b/a;

    .line 120
    iget-object v1, p0, Lco/uk/getmondo/card/e;->b:Lio/reactivex/b/a;

    .line 122
    invoke-interface {p1}, Lco/uk/getmondo/card/e$a;->b()Lio/reactivex/n;

    move-result-object v2

    .line 121
    new-instance v0, Lco/uk/getmondo/card/e$g;

    invoke-direct {v0, p0}, Lco/uk/getmondo/card/e$g;-><init>(Lco/uk/getmondo/card/e;)V

    check-cast v0, Lio/reactivex/c/g;

    invoke-virtual {v2, v0}, Lio/reactivex/n;->doOnNext(Lio/reactivex/c/g;)Lio/reactivex/n;

    move-result-object v2

    .line 122
    new-instance v0, Lco/uk/getmondo/card/e$h;

    invoke-direct {v0, p1}, Lco/uk/getmondo/card/e$h;-><init>(Lco/uk/getmondo/card/e$a;)V

    check-cast v0, Lio/reactivex/c/g;

    invoke-virtual {v2, v0}, Lio/reactivex/n;->subscribe(Lio/reactivex/c/g;)Lio/reactivex/b/b;

    move-result-object v0

    const-string v2, "view.cardReplacementClic\u2026w.openCardReplacement() }"

    invoke-static {v0, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 120
    invoke-static {v1, v0}, Lco/uk/getmondo/common/j/f;->a(Lio/reactivex/b/a;Lio/reactivex/b/b;)Lio/reactivex/b/a;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/card/e;->b:Lio/reactivex/b/a;

    .line 124
    iget-object v1, p0, Lco/uk/getmondo/card/e;->b:Lio/reactivex/b/a;

    .line 125
    invoke-interface {p1}, Lco/uk/getmondo/card/e$a;->c()Lio/reactivex/n;

    move-result-object v2

    new-instance v0, Lco/uk/getmondo/card/e$i;

    invoke-direct {v0, p1}, Lco/uk/getmondo/card/e$i;-><init>(Lco/uk/getmondo/card/e$a;)V

    check-cast v0, Lio/reactivex/c/g;

    invoke-virtual {v2, v0}, Lio/reactivex/n;->subscribe(Lio/reactivex/c/g;)Lio/reactivex/b/b;

    move-result-object v0

    const-string v2, "view.activateReplacement\u2026view.openActivateCard() }"

    invoke-static {v0, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 124
    invoke-static {v1, v0}, Lco/uk/getmondo/common/j/f;->a(Lio/reactivex/b/a;Lio/reactivex/b/b;)Lio/reactivex/b/a;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/card/e;->b:Lio/reactivex/b/a;

    .line 127
    iget-object v1, p0, Lco/uk/getmondo/card/e;->b:Lio/reactivex/b/a;

    .line 128
    invoke-interface {p1}, Lco/uk/getmondo/card/e$a;->d()Lio/reactivex/n;

    move-result-object v2

    new-instance v0, Lco/uk/getmondo/card/e$j;

    invoke-direct {v0, p0, v3, p1}, Lco/uk/getmondo/card/e$j;-><init>(Lco/uk/getmondo/card/e;ZLco/uk/getmondo/card/e$a;)V

    check-cast v0, Lio/reactivex/c/g;

    invoke-virtual {v2, v0}, Lio/reactivex/n;->subscribe(Lio/reactivex/c/g;)Lio/reactivex/b/b;

    move-result-object v0

    const-string v2, "view.forgotPinClicks\n   \u2026      }\n                }"

    invoke-static {v0, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 127
    invoke-static {v1, v0}, Lco/uk/getmondo/common/j/f;->a(Lio/reactivex/b/a;Lio/reactivex/b/b;)Lio/reactivex/b/a;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/card/e;->b:Lio/reactivex/b/a;

    .line 136
    return-void
.end method

.method public bridge synthetic a(Lco/uk/getmondo/common/ui/f;)V
    .locals 0

    .prologue
    .line 27
    check-cast p1, Lco/uk/getmondo/card/e$a;

    invoke-virtual {p0, p1}, Lco/uk/getmondo/card/e;->a(Lco/uk/getmondo/card/e$a;)V

    return-void
.end method
