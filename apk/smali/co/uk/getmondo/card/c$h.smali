.class final Lco/uk/getmondo/card/c$h;
.super Ljava/lang/Object;
.source "CardManager.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lco/uk/getmondo/card/c;->d(Ljava/lang/String;)Lio/reactivex/h;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h",
        "<TT;TR;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000\u0014\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\n \u0002*\u0004\u0018\u00010\u00010\u00012\u000c\u0010\u0003\u001a\u0008\u0012\u0004\u0012\u00020\u00050\u0004H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "Lco/uk/getmondo/model/Card;",
        "kotlin.jvm.PlatformType",
        "apiCards",
        "",
        "Lco/uk/getmondo/api/model/ApiCard;",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# static fields
.field public static final a:Lco/uk/getmondo/card/c$h;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lco/uk/getmondo/card/c$h;

    invoke-direct {v0}, Lco/uk/getmondo/card/c$h;-><init>()V

    sput-object v0, Lco/uk/getmondo/card/c$h;->a:Lco/uk/getmondo/card/c$h;

    return-void
.end method

.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/util/List;)Lco/uk/getmondo/d/g;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lco/uk/getmondo/api/model/ApiCard;",
            ">;)",
            "Lco/uk/getmondo/d/g;"
        }
    .end annotation

    .prologue
    const-string v0, "apiCards"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 95
    new-instance v1, Lco/uk/getmondo/d/a/e;

    invoke-direct {v1}, Lco/uk/getmondo/d/a/e;-><init>()V

    const/4 v0, 0x0

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/api/model/ApiCard;

    invoke-virtual {v1, v0}, Lco/uk/getmondo/d/a/e;->a(Lco/uk/getmondo/api/model/ApiCard;)Lco/uk/getmondo/d/g;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 18
    check-cast p1, Ljava/util/List;

    invoke-virtual {p0, p1}, Lco/uk/getmondo/card/c$h;->a(Ljava/util/List;)Lco/uk/getmondo/d/g;

    move-result-object v0

    return-object v0
.end method
