.class final Lco/uk/getmondo/card/c$a;
.super Ljava/lang/Object;
.source "CardManager.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lco/uk/getmondo/card/c;->b(Ljava/lang/String;)Lio/reactivex/v;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h",
        "<TT;",
        "Lio/reactivex/z",
        "<+TR;>;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000\u0014\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0000\u0010\u0000\u001a&\u0012\u000c\u0012\n \u0003*\u0004\u0018\u00010\u00020\u0002 \u0003*\u0012\u0012\u000c\u0012\n \u0003*\u0004\u0018\u00010\u00020\u0002\u0018\u00010\u00010\u00012\u0006\u0010\u0004\u001a\u00020\u0005H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "Lio/reactivex/Single;",
        "Lco/uk/getmondo/model/Card;",
        "kotlin.jvm.PlatformType",
        "accountId",
        "",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field final synthetic a:Lco/uk/getmondo/card/c;

.field final synthetic b:Ljava/lang/String;


# direct methods
.method constructor <init>(Lco/uk/getmondo/card/c;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lco/uk/getmondo/card/c$a;->a:Lco/uk/getmondo/card/c;

    iput-object p2, p0, Lco/uk/getmondo/card/c$a;->b:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Lio/reactivex/v;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/v",
            "<",
            "Lco/uk/getmondo/d/g;",
            ">;"
        }
    .end annotation

    .prologue
    const-string v0, "accountId"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 136
    iget-object v0, p0, Lco/uk/getmondo/card/c$a;->a:Lco/uk/getmondo/card/c;

    invoke-static {v0}, Lco/uk/getmondo/card/c;->c(Lco/uk/getmondo/card/c;)Lco/uk/getmondo/api/MonzoApi;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/card/c$a;->b:Ljava/lang/String;

    invoke-interface {v0, p1, v1}, Lco/uk/getmondo/api/MonzoApi;->activateCardBank(Ljava/lang/String;Ljava/lang/String;)Lio/reactivex/v;

    move-result-object v1

    .line 137
    sget-object v0, Lco/uk/getmondo/card/c$a$1;->a:Lco/uk/getmondo/card/c$a$1;

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {v1, v0}, Lio/reactivex/v;->d(Lio/reactivex/c/h;)Lio/reactivex/v;

    move-result-object v1

    .line 138
    new-instance v0, Lco/uk/getmondo/card/c$a$2;

    invoke-direct {v0, p0}, Lco/uk/getmondo/card/c$a$2;-><init>(Lco/uk/getmondo/card/c$a;)V

    check-cast v0, Lio/reactivex/c/g;

    invoke-virtual {v1, v0}, Lio/reactivex/v;->c(Lio/reactivex/c/g;)Lio/reactivex/v;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 18
    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1}, Lco/uk/getmondo/card/c$a;->a(Ljava/lang/String;)Lio/reactivex/v;

    move-result-object v0

    return-object v0
.end method
