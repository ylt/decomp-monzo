.class public Lco/uk/getmondo/card/CardReplacementActivity;
.super Lco/uk/getmondo/common/activities/b;
.source "CardReplacementActivity.java"

# interfaces
.implements Lco/uk/getmondo/card/j$a;


# static fields
.field private static final b:Ljava/text/SimpleDateFormat;


# instance fields
.field a:Lco/uk/getmondo/card/j;

.field addressTextView:Landroid/widget/TextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f110128
    .end annotation
.end field

.field private c:Lcom/b/b/c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/b/b/c",
            "<",
            "Lco/uk/getmondo/d/s;",
            ">;"
        }
    .end annotation
.end field

.field editAddressButton:Landroid/widget/Button;
    .annotation build Lbutterknife/BindView;
        value = 0x7f110129
    .end annotation
.end field

.field sendReplacementCardButton:Landroid/widget/Button;
    .annotation build Lbutterknife/BindView;
        value = 0x7f110123
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 31
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "EEE d"

    sget-object v2, Ljava/util/Locale;->UK:Ljava/util/Locale;

    invoke-direct {v0, v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    sput-object v0, Lco/uk/getmondo/card/CardReplacementActivity;->b:Ljava/text/SimpleDateFormat;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 30
    invoke-direct {p0}, Lco/uk/getmondo/common/activities/b;-><init>()V

    .line 41
    invoke-static {}, Lcom/b/b/c;->a()Lcom/b/b/c;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/card/CardReplacementActivity;->c:Lcom/b/b/c;

    return-void
.end method

.method public static a(Landroid/content/Context;)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 48
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lco/uk/getmondo/card/CardReplacementActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    return-object v0
.end method

.method private static b(Ljava/util/Date;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 127
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 128
    invoke-virtual {v0, p0}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    .line 129
    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Ljava/util/Calendar;->get(I)I

    move-result v0

    .line 131
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lco/uk/getmondo/card/CardReplacementActivity;->b:Ljava/text/SimpleDateFormat;

    invoke-virtual {v2, p0}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {v0}, Lco/uk/getmondo/common/c/a;->a(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public a()Lio/reactivex/n;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 85
    iget-object v0, p0, Lco/uk/getmondo/card/CardReplacementActivity;->sendReplacementCardButton:Landroid/widget/Button;

    invoke-static {v0}, Lcom/b/a/c/c;->a(Landroid/view/View;)Lio/reactivex/n;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 100
    iget-object v0, p0, Lco/uk/getmondo/card/CardReplacementActivity;->addressTextView:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 101
    return-void
.end method

.method public a(Ljava/util/Date;)V
    .locals 4

    .prologue
    .line 117
    if-nez p1, :cond_0

    .line 118
    const-string v0, ""

    .line 122
    :goto_0
    const v1, 0x7f0a010a

    invoke-virtual {p0, v1}, Lco/uk/getmondo/card/CardReplacementActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v1, v0}, Lco/uk/getmondo/common/activities/ConfirmationActivity;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 123
    invoke-virtual {p0}, Lco/uk/getmondo/card/CardReplacementActivity;->finish()V

    .line 124
    return-void

    .line 120
    :cond_0
    const v0, 0x7f0a0109

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-static {p1}, Lco/uk/getmondo/card/CardReplacementActivity;->b(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {p0, v0, v1}, Lco/uk/getmondo/card/CardReplacementActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public a(Z)V
    .locals 1

    .prologue
    .line 105
    iget-object v0, p0, Lco/uk/getmondo/card/CardReplacementActivity;->sendReplacementCardButton:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 106
    return-void
.end method

.method public b()Lio/reactivex/n;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 90
    iget-object v0, p0, Lco/uk/getmondo/card/CardReplacementActivity;->editAddressButton:Landroid/widget/Button;

    invoke-static {v0}, Lcom/b/a/c/c;->a(Landroid/view/View;)Lio/reactivex/n;

    move-result-object v0

    return-object v0
.end method

.method public c()Lio/reactivex/n;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Lco/uk/getmondo/d/s;",
            ">;"
        }
    .end annotation

    .prologue
    .line 95
    iget-object v0, p0, Lco/uk/getmondo/card/CardReplacementActivity;->c:Lcom/b/b/c;

    return-object v0
.end method

.method public d()V
    .locals 2

    .prologue
    .line 110
    sget-object v0, Lco/uk/getmondo/common/activities/b$a;->b:Lco/uk/getmondo/common/activities/b$a;

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Lco/uk/getmondo/common/address/SelectAddressActivity;->b(Landroid/content/Context;Lco/uk/getmondo/common/activities/b$a;Z)Landroid/content/Intent;

    move-result-object v0

    .line 111
    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lco/uk/getmondo/card/CardReplacementActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 112
    return-void
.end method

.method public e()V
    .locals 0

    .prologue
    .line 136
    invoke-virtual {p0}, Lco/uk/getmondo/card/CardReplacementActivity;->s()V

    .line 137
    return-void
.end method

.method public f()V
    .locals 0

    .prologue
    .line 141
    invoke-virtual {p0}, Lco/uk/getmondo/card/CardReplacementActivity;->t()V

    .line 142
    return-void
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 2

    .prologue
    .line 68
    packed-switch p1, :pswitch_data_0

    .line 79
    invoke-super {p0, p1, p2, p3}, Lco/uk/getmondo/common/activities/b;->onActivityResult(IILandroid/content/Intent;)V

    .line 81
    :cond_0
    :goto_0
    return-void

    .line 70
    :pswitch_0
    invoke-virtual {p0}, Lco/uk/getmondo/card/CardReplacementActivity;->finish()V

    goto :goto_0

    .line 73
    :pswitch_1
    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    .line 74
    invoke-static {p3}, Lco/uk/getmondo/common/address/SelectAddressActivity;->a(Landroid/content/Intent;)Lco/uk/getmondo/d/s;

    move-result-object v0

    .line 75
    iget-object v1, p0, Lco/uk/getmondo/card/CardReplacementActivity;->c:Lcom/b/b/c;

    invoke-virtual {v1, v0}, Lcom/b/b/c;->a(Ljava/lang/Object;)V

    goto :goto_0

    .line 68
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 53
    invoke-super {p0, p1}, Lco/uk/getmondo/common/activities/b;->onCreate(Landroid/os/Bundle;)V

    .line 54
    const v0, 0x7f050026

    invoke-virtual {p0, v0}, Lco/uk/getmondo/card/CardReplacementActivity;->setContentView(I)V

    .line 55
    invoke-static {p0}, Lbutterknife/ButterKnife;->bind(Landroid/app/Activity;)Lbutterknife/Unbinder;

    .line 56
    invoke-virtual {p0}, Lco/uk/getmondo/card/CardReplacementActivity;->l()Lco/uk/getmondo/common/h/b/b;

    move-result-object v0

    invoke-interface {v0, p0}, Lco/uk/getmondo/common/h/b/b;->a(Lco/uk/getmondo/card/CardReplacementActivity;)V

    .line 57
    iget-object v0, p0, Lco/uk/getmondo/card/CardReplacementActivity;->a:Lco/uk/getmondo/card/j;

    invoke-virtual {v0, p0}, Lco/uk/getmondo/card/j;->a(Lco/uk/getmondo/card/j$a;)V

    .line 58
    return-void
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lco/uk/getmondo/card/CardReplacementActivity;->a:Lco/uk/getmondo/card/j;

    invoke-virtual {v0}, Lco/uk/getmondo/card/j;->b()V

    .line 63
    invoke-super {p0}, Lco/uk/getmondo/common/activities/b;->onDestroy()V

    .line 64
    return-void
.end method
