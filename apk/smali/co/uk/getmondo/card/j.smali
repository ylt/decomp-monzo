.class Lco/uk/getmondo/card/j;
.super Lco/uk/getmondo/common/ui/b;
.source "CardReplacementPresenter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lco/uk/getmondo/card/j$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lco/uk/getmondo/common/ui/b",
        "<",
        "Lco/uk/getmondo/card/j$a;",
        ">;"
    }
.end annotation


# instance fields
.field private final c:Lio/reactivex/u;

.field private final d:Lio/reactivex/u;

.field private final e:Lco/uk/getmondo/common/accounts/d;

.field private final f:Lco/uk/getmondo/common/e/a;

.field private final g:Lco/uk/getmondo/card/c;

.field private final h:Lco/uk/getmondo/common/a;

.field private i:Lco/uk/getmondo/d/s;


# direct methods
.method constructor <init>(Lio/reactivex/u;Lio/reactivex/u;Lco/uk/getmondo/common/accounts/d;Lco/uk/getmondo/common/e/a;Lco/uk/getmondo/card/c;Lco/uk/getmondo/common/a;)V
    .locals 0

    .prologue
    .line 40
    invoke-direct {p0}, Lco/uk/getmondo/common/ui/b;-><init>()V

    .line 41
    iput-object p1, p0, Lco/uk/getmondo/card/j;->c:Lio/reactivex/u;

    .line 42
    iput-object p2, p0, Lco/uk/getmondo/card/j;->d:Lio/reactivex/u;

    .line 43
    iput-object p3, p0, Lco/uk/getmondo/card/j;->e:Lco/uk/getmondo/common/accounts/d;

    .line 44
    iput-object p4, p0, Lco/uk/getmondo/card/j;->f:Lco/uk/getmondo/common/e/a;

    .line 45
    iput-object p5, p0, Lco/uk/getmondo/card/j;->g:Lco/uk/getmondo/card/c;

    .line 46
    iput-object p6, p0, Lco/uk/getmondo/card/j;->h:Lco/uk/getmondo/common/a;

    .line 47
    return-void
.end method

.method static synthetic a(Lco/uk/getmondo/card/j;Lco/uk/getmondo/card/j$a;Ljava/lang/Object;)Lio/reactivex/l;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 70
    iget-object v0, p0, Lco/uk/getmondo/card/j;->g:Lco/uk/getmondo/card/c;

    iget-object v1, p0, Lco/uk/getmondo/card/j;->i:Lco/uk/getmondo/d/s;

    invoke-virtual {v0, v1}, Lco/uk/getmondo/card/c;->a(Lco/uk/getmondo/d/s;)Lio/reactivex/v;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/card/j;->d:Lio/reactivex/u;

    .line 71
    invoke-virtual {v0, v1}, Lio/reactivex/v;->b(Lio/reactivex/u;)Lio/reactivex/v;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/card/j;->c:Lio/reactivex/u;

    .line 72
    invoke-virtual {v0, v1}, Lio/reactivex/v;->a(Lio/reactivex/u;)Lio/reactivex/v;

    move-result-object v0

    invoke-static {p0, p1}, Lco/uk/getmondo/card/q;->a(Lco/uk/getmondo/card/j;Lco/uk/getmondo/card/j$a;)Lio/reactivex/c/g;

    move-result-object v1

    .line 73
    invoke-virtual {v0, v1}, Lio/reactivex/v;->d(Lio/reactivex/c/g;)Lio/reactivex/v;

    move-result-object v0

    .line 80
    invoke-virtual {v0}, Lio/reactivex/v;->e()Lio/reactivex/h;

    move-result-object v0

    .line 81
    invoke-virtual {v0}, Lio/reactivex/h;->f()Lio/reactivex/h;

    move-result-object v0

    .line 70
    return-object v0
.end method

.method private a()V
    .locals 2

    .prologue
    .line 97
    iget-object v0, p0, Lco/uk/getmondo/card/j;->i:Lco/uk/getmondo/d/s;

    if-nez v0, :cond_0

    .line 104
    :goto_0
    return-void

    .line 101
    :cond_0
    iget-object v0, p0, Lco/uk/getmondo/card/j;->e:Lco/uk/getmondo/common/accounts/d;

    invoke-virtual {v0}, Lco/uk/getmondo/common/accounts/d;->b()Lco/uk/getmondo/d/ak;

    move-result-object v0

    .line 102
    invoke-virtual {v0}, Lco/uk/getmondo/d/ak;->d()Lco/uk/getmondo/d/ac;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/card/j;->i:Lco/uk/getmondo/d/s;

    invoke-virtual {v0, v1}, Lco/uk/getmondo/d/ac;->a(Lco/uk/getmondo/d/s;)Lco/uk/getmondo/d/ac;

    move-result-object v0

    .line 103
    iget-object v1, p0, Lco/uk/getmondo/card/j;->e:Lco/uk/getmondo/common/accounts/d;

    invoke-virtual {v1, v0}, Lco/uk/getmondo/common/accounts/d;->a(Lco/uk/getmondo/d/ac;)V

    goto :goto_0
.end method

.method static synthetic a(Lco/uk/getmondo/card/j$a;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 56
    invoke-interface {p0}, Lco/uk/getmondo/card/j$a;->d()V

    return-void
.end method

.method static synthetic a(Lco/uk/getmondo/card/j;Lco/uk/getmondo/card/j$a;Lco/uk/getmondo/d/s;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 60
    iput-object p2, p0, Lco/uk/getmondo/card/j;->i:Lco/uk/getmondo/d/s;

    .line 61
    invoke-static {p2}, Lco/uk/getmondo/common/k/a;->b(Lco/uk/getmondo/d/s;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Lco/uk/getmondo/card/j$a;->a(Ljava/lang/String;)V

    .line 62
    return-void
.end method

.method static synthetic a(Lco/uk/getmondo/card/j;Lco/uk/getmondo/card/j$a;Lcom/c/b/b;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 83
    invoke-direct {p0}, Lco/uk/getmondo/card/j;->a()V

    .line 84
    invoke-interface {p1}, Lco/uk/getmondo/card/j$a;->f()V

    .line 85
    const/4 v0, 0x1

    invoke-interface {p1, v0}, Lco/uk/getmondo/card/j$a;->a(Z)V

    .line 87
    invoke-virtual {p2}, Lcom/c/b/b;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 88
    invoke-virtual {p2}, Lcom/c/b/b;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Date;

    .line 92
    :goto_0
    invoke-interface {p1, v0}, Lco/uk/getmondo/card/j$a;->a(Ljava/util/Date;)V

    .line 93
    return-void

    .line 90
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic a(Lco/uk/getmondo/card/j;Lco/uk/getmondo/card/j$a;Ljava/lang/Throwable;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 74
    invoke-interface {p1}, Lco/uk/getmondo/card/j$a;->f()V

    .line 75
    const/4 v0, 0x1

    invoke-interface {p1, v0}, Lco/uk/getmondo/card/j$a;->a(Z)V

    .line 76
    iget-object v0, p0, Lco/uk/getmondo/card/j;->f:Lco/uk/getmondo/common/e/a;

    invoke-virtual {v0, p2, p1}, Lco/uk/getmondo/common/e/a;->a(Ljava/lang/Throwable;Lco/uk/getmondo/common/e/a$a;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 77
    const v0, 0x7f0a010d

    invoke-interface {p1, v0}, Lco/uk/getmondo/card/j$a;->b(I)V

    .line 79
    :cond_0
    return-void
.end method

.method static synthetic b(Lco/uk/getmondo/card/j;Lco/uk/getmondo/card/j$a;Ljava/lang/Object;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 66
    iget-object v0, p0, Lco/uk/getmondo/card/j;->h:Lco/uk/getmondo/common/a;

    invoke-static {}, Lco/uk/getmondo/api/model/tracking/Impression;->H()Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v1

    invoke-virtual {v0, v1}, Lco/uk/getmondo/common/a;->a(Lco/uk/getmondo/api/model/tracking/Impression;)V

    .line 67
    invoke-interface {p1}, Lco/uk/getmondo/card/j$a;->e()V

    .line 68
    const/4 v0, 0x0

    invoke-interface {p1, v0}, Lco/uk/getmondo/card/j$a;->a(Z)V

    .line 69
    return-void
.end method


# virtual methods
.method public a(Lco/uk/getmondo/card/j$a;)V
    .locals 3

    .prologue
    .line 51
    invoke-super {p0, p1}, Lco/uk/getmondo/common/ui/b;->a(Lco/uk/getmondo/common/ui/f;)V

    .line 53
    iget-object v0, p0, Lco/uk/getmondo/card/j;->e:Lco/uk/getmondo/common/accounts/d;

    invoke-virtual {v0}, Lco/uk/getmondo/common/accounts/d;->b()Lco/uk/getmondo/d/ak;

    move-result-object v0

    invoke-virtual {v0}, Lco/uk/getmondo/d/ak;->d()Lco/uk/getmondo/d/ac;

    move-result-object v0

    invoke-virtual {v0}, Lco/uk/getmondo/d/ac;->h()Lco/uk/getmondo/d/s;

    move-result-object v0

    invoke-static {v0}, Lco/uk/getmondo/common/k/a;->b(Lco/uk/getmondo/d/s;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Lco/uk/getmondo/card/j$a;->a(Ljava/lang/String;)V

    .line 55
    invoke-interface {p1}, Lco/uk/getmondo/card/j$a;->b()Lio/reactivex/n;

    move-result-object v0

    invoke-static {p1}, Lco/uk/getmondo/card/k;->a(Lco/uk/getmondo/card/j$a;)Lio/reactivex/c/g;

    move-result-object v1

    .line 56
    invoke-virtual {v0, v1}, Lio/reactivex/n;->subscribe(Lio/reactivex/c/g;)Lio/reactivex/b/b;

    move-result-object v0

    .line 55
    invoke-virtual {p0, v0}, Lco/uk/getmondo/card/j;->a(Lio/reactivex/b/b;)V

    .line 58
    invoke-interface {p1}, Lco/uk/getmondo/card/j$a;->c()Lio/reactivex/n;

    move-result-object v0

    invoke-static {p0, p1}, Lco/uk/getmondo/card/l;->a(Lco/uk/getmondo/card/j;Lco/uk/getmondo/card/j$a;)Lio/reactivex/c/g;

    move-result-object v1

    .line 59
    invoke-virtual {v0, v1}, Lio/reactivex/n;->subscribe(Lio/reactivex/c/g;)Lio/reactivex/b/b;

    move-result-object v0

    .line 58
    invoke-virtual {p0, v0}, Lco/uk/getmondo/card/j;->a(Lio/reactivex/b/b;)V

    .line 64
    invoke-interface {p1}, Lco/uk/getmondo/card/j$a;->a()Lio/reactivex/n;

    move-result-object v0

    invoke-static {p0, p1}, Lco/uk/getmondo/card/m;->a(Lco/uk/getmondo/card/j;Lco/uk/getmondo/card/j$a;)Lio/reactivex/c/g;

    move-result-object v1

    .line 65
    invoke-virtual {v0, v1}, Lio/reactivex/n;->doOnNext(Lio/reactivex/c/g;)Lio/reactivex/n;

    move-result-object v0

    invoke-static {p0, p1}, Lco/uk/getmondo/card/n;->a(Lco/uk/getmondo/card/j;Lco/uk/getmondo/card/j$a;)Lio/reactivex/c/h;

    move-result-object v1

    .line 70
    invoke-virtual {v0, v1}, Lio/reactivex/n;->flatMapMaybe(Lio/reactivex/c/h;)Lio/reactivex/n;

    move-result-object v0

    invoke-static {p0, p1}, Lco/uk/getmondo/card/o;->a(Lco/uk/getmondo/card/j;Lco/uk/getmondo/card/j$a;)Lio/reactivex/c/g;

    move-result-object v1

    invoke-static {}, Lco/uk/getmondo/card/p;->a()Lio/reactivex/c/g;

    move-result-object v2

    .line 82
    invoke-virtual {v0, v1, v2}, Lio/reactivex/n;->subscribe(Lio/reactivex/c/g;Lio/reactivex/c/g;)Lio/reactivex/b/b;

    move-result-object v0

    .line 64
    invoke-virtual {p0, v0}, Lco/uk/getmondo/card/j;->a(Lio/reactivex/b/b;)V

    .line 94
    return-void
.end method

.method public bridge synthetic a(Lco/uk/getmondo/common/ui/f;)V
    .locals 0

    .prologue
    .line 27
    check-cast p1, Lco/uk/getmondo/card/j$a;

    invoke-virtual {p0, p1}, Lco/uk/getmondo/card/j;->a(Lco/uk/getmondo/card/j$a;)V

    return-void
.end method
