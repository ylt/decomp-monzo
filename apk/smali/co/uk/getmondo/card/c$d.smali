.class final Lco/uk/getmondo/card/c$d;
.super Ljava/lang/Object;
.source "CardManager.kt"

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lco/uk/getmondo/card/c;->a(Ljava/lang/String;Z)Lio/reactivex/v;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Lio/reactivex/z",
        "<+TT;>;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a&\u0012\u000c\u0012\n \u0003*\u0004\u0018\u00010\u00020\u0002 \u0003*\u0012\u0012\u000c\u0012\n \u0003*\u0004\u0018\u00010\u00020\u0002\u0018\u00010\u00010\u0001H\n\u00a2\u0006\u0002\u0008\u0004"
    }
    d2 = {
        "<anonymous>",
        "Lio/reactivex/Single;",
        "Lco/uk/getmondo/api/model/ApiCard;",
        "kotlin.jvm.PlatformType",
        "call"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field final synthetic a:Lco/uk/getmondo/card/c;

.field final synthetic b:Z

.field final synthetic c:Ljava/lang/String;


# direct methods
.method constructor <init>(Lco/uk/getmondo/card/c;ZLjava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lco/uk/getmondo/card/c$d;->a:Lco/uk/getmondo/card/c;

    iput-boolean p2, p0, Lco/uk/getmondo/card/c$d;->b:Z

    iput-object p3, p0, Lco/uk/getmondo/card/c$d;->c:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Lio/reactivex/v;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/v",
            "<",
            "Lco/uk/getmondo/api/model/ApiCard;",
            ">;"
        }
    .end annotation

    .prologue
    .line 115
    iget-object v0, p0, Lco/uk/getmondo/card/c$d;->a:Lco/uk/getmondo/card/c;

    invoke-static {v0}, Lco/uk/getmondo/card/c;->a(Lco/uk/getmondo/card/c;)Lco/uk/getmondo/common/accounts/b;

    move-result-object v0

    invoke-virtual {v0}, Lco/uk/getmondo/common/accounts/b;->c()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-static {}, Lkotlin/d/b/l;->a()V

    .line 116
    :cond_0
    iget-boolean v1, p0, Lco/uk/getmondo/card/c$d;->b:Z

    if-eqz v1, :cond_2

    .line 117
    iget-object v1, p0, Lco/uk/getmondo/card/c$d;->a:Lco/uk/getmondo/card/c;

    invoke-static {v1}, Lco/uk/getmondo/card/c;->b(Lco/uk/getmondo/card/c;)Lco/uk/getmondo/card/s;

    move-result-object v1

    invoke-virtual {v1, v0}, Lco/uk/getmondo/card/s;->a(Ljava/lang/String;)Lco/uk/getmondo/d/g;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 118
    iget-object v2, p0, Lco/uk/getmondo/card/c$d;->a:Lco/uk/getmondo/card/c;

    invoke-static {v2}, Lco/uk/getmondo/card/c;->c(Lco/uk/getmondo/card/c;)Lco/uk/getmondo/api/MonzoApi;

    move-result-object v2

    invoke-virtual {v1}, Lco/uk/getmondo/d/g;->a()Ljava/lang/String;

    move-result-object v1

    iget-object v3, p0, Lco/uk/getmondo/card/c$d;->c:Ljava/lang/String;

    invoke-interface {v2, v0, v1, v3}, Lco/uk/getmondo/api/MonzoApi;->activateReplacementCard(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lio/reactivex/v;

    move-result-object v1

    sget-object v0, Lco/uk/getmondo/card/c$d$1;->a:Lco/uk/getmondo/card/c$d$1;

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {v1, v0}, Lio/reactivex/v;->d(Lio/reactivex/c/h;)Lio/reactivex/v;

    move-result-object v0

    .line 116
    :goto_0
    return-object v0

    .line 117
    :cond_1
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Trying to activate a replacement card but there isn\'t a previous card saved"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    .line 120
    :cond_2
    iget-object v1, p0, Lco/uk/getmondo/card/c$d;->a:Lco/uk/getmondo/card/c;

    invoke-static {v1}, Lco/uk/getmondo/card/c;->c(Lco/uk/getmondo/card/c;)Lco/uk/getmondo/api/MonzoApi;

    move-result-object v1

    iget-object v2, p0, Lco/uk/getmondo/card/c$d;->c:Ljava/lang/String;

    invoke-interface {v1, v0, v2}, Lco/uk/getmondo/api/MonzoApi;->activateCardPrepaid(Ljava/lang/String;Ljava/lang/String;)Lio/reactivex/v;

    move-result-object v1

    sget-object v0, Lco/uk/getmondo/card/c$d$2;->a:Lco/uk/getmondo/card/c$d$2;

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {v1, v0}, Lio/reactivex/v;->d(Lio/reactivex/c/h;)Lio/reactivex/v;

    move-result-object v0

    goto :goto_0
.end method

.method public synthetic call()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 18
    invoke-virtual {p0}, Lco/uk/getmondo/card/c$d;->a()Lio/reactivex/v;

    move-result-object v0

    return-object v0
.end method
