.class public final Lco/uk/getmondo/card/c;
.super Ljava/lang/Object;
.source "CardManager.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000f\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0003\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0007\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u001f\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\u0008J\u0014\u0010\r\u001a\u0008\u0012\u0004\u0012\u00020\n0\u000e2\u0006\u0010\u000f\u001a\u00020\u0010J\u001c\u0010\u0011\u001a\u0008\u0012\u0004\u0012\u00020\n0\u000e2\u0006\u0010\u0012\u001a\u00020\u00102\u0006\u0010\u0013\u001a\u00020\u0014J\u000c\u0010\u0015\u001a\u0008\u0012\u0004\u0012\u00020\n0\u0016J\u0016\u0010\u0017\u001a\u0008\u0012\u0004\u0012\u00020\n0\u00182\u0006\u0010\u0019\u001a\u00020\u0010H\u0002J\u0014\u0010\u001a\u001a\u0008\u0012\u0004\u0012\u00020\u00140\u000e2\u0006\u0010\u0019\u001a\u00020\u0010J\u001e\u0010\u001b\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u001d0\u001c0\u000e2\n\u0008\u0002\u0010\u001e\u001a\u0004\u0018\u00010\u001fJ\u001a\u0010 \u001a\u00020!2\u0006\u0010\u0019\u001a\u00020\u00102\u0008\u0010\u001e\u001a\u0004\u0018\u00010\u001fH\u0002J \u0010\"\u001a\u0008\u0012\u0004\u0012\u00020\u001d0\u000e2\u0006\u0010\u0019\u001a\u00020\u00102\u0008\u0010\u001e\u001a\u0004\u0018\u00010\u001fH\u0002J\u0006\u0010#\u001a\u00020!J\u0018\u0010$\u001a\n %*\u0004\u0018\u00010!0!2\u0006\u0010\u0019\u001a\u00020\u0010H\u0002J\u0016\u0010&\u001a\u00020!2\u0006\u0010\'\u001a\u00020\u00102\u0006\u0010(\u001a\u00020)R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0013\u0010\t\u001a\u0004\u0018\u00010\n8F\u00a2\u0006\u0006\u001a\u0004\u0008\u000b\u0010\u000cR\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006*"
    }
    d2 = {
        "Lco/uk/getmondo/card/CardManager;",
        "",
        "accountManager",
        "Lco/uk/getmondo/common/accounts/AccountManager;",
        "monzoApi",
        "Lco/uk/getmondo/api/MonzoApi;",
        "cardStorage",
        "Lco/uk/getmondo/card/CardStorage;",
        "(Lco/uk/getmondo/common/accounts/AccountManager;Lco/uk/getmondo/api/MonzoApi;Lco/uk/getmondo/card/CardStorage;)V",
        "cachedCard",
        "Lco/uk/getmondo/model/Card;",
        "getCachedCard",
        "()Lco/uk/getmondo/model/Card;",
        "activateDebitCard",
        "Lio/reactivex/Single;",
        "cardPan",
        "",
        "activatePrepaidCard",
        "cardToken",
        "isReplacementCard",
        "",
        "card",
        "Lio/reactivex/Observable;",
        "getCardFromApi",
        "Lio/reactivex/Maybe;",
        "accountId",
        "hasCard",
        "replaceCard",
        "Lcom/memoizrlabs/poweroptional/Optional;",
        "Ljava/util/Date;",
        "address",
        "Lco/uk/getmondo/model/LegacyAddress;",
        "replaceCardBank",
        "Lio/reactivex/Completable;",
        "replaceCardPrepaid",
        "syncCard",
        "syncCardForAccount",
        "kotlin.jvm.PlatformType",
        "toggleCard",
        "cardId",
        "apiCardStatus",
        "Lco/uk/getmondo/api/model/ApiCardStatus;",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field private final a:Lco/uk/getmondo/common/accounts/b;

.field private final b:Lco/uk/getmondo/api/MonzoApi;

.field private final c:Lco/uk/getmondo/card/s;


# direct methods
.method public constructor <init>(Lco/uk/getmondo/common/accounts/b;Lco/uk/getmondo/api/MonzoApi;Lco/uk/getmondo/card/s;)V
    .locals 1

    .prologue
    const-string v0, "accountManager"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "monzoApi"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "cardStorage"

    invoke-static {p3, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lco/uk/getmondo/card/c;->a:Lco/uk/getmondo/common/accounts/b;

    iput-object p2, p0, Lco/uk/getmondo/card/c;->b:Lco/uk/getmondo/api/MonzoApi;

    iput-object p3, p0, Lco/uk/getmondo/card/c;->c:Lco/uk/getmondo/card/s;

    return-void
.end method

.method public static final synthetic a(Lco/uk/getmondo/card/c;)Lco/uk/getmondo/common/accounts/b;
    .locals 1

    .prologue
    .line 18
    iget-object v0, p0, Lco/uk/getmondo/card/c;->a:Lco/uk/getmondo/common/accounts/b;

    return-object v0
.end method

.method public static final synthetic a(Lco/uk/getmondo/card/c;Ljava/lang/String;)Lio/reactivex/b;
    .locals 1

    .prologue
    .line 18
    invoke-direct {p0, p1}, Lco/uk/getmondo/card/c;->c(Ljava/lang/String;)Lio/reactivex/b;

    move-result-object v0

    return-object v0
.end method

.method public static final synthetic a(Lco/uk/getmondo/card/c;Ljava/lang/String;Lco/uk/getmondo/d/s;)Lio/reactivex/b;
    .locals 1

    .prologue
    .line 18
    invoke-direct {p0, p1, p2}, Lco/uk/getmondo/card/c;->a(Ljava/lang/String;Lco/uk/getmondo/d/s;)Lio/reactivex/b;

    move-result-object v0

    return-object v0
.end method

.method private final a(Ljava/lang/String;Lco/uk/getmondo/d/s;)Lio/reactivex/b;
    .locals 2

    .prologue
    .line 46
    new-instance v0, Lco/uk/getmondo/card/c$l;

    invoke-direct {v0, p0, p1}, Lco/uk/getmondo/card/c$l;-><init>(Lco/uk/getmondo/card/c;Ljava/lang/String;)V

    check-cast v0, Ljava/util/concurrent/Callable;

    invoke-static {v0}, Lio/reactivex/v;->c(Ljava/util/concurrent/Callable;)Lio/reactivex/v;

    move-result-object v1

    .line 47
    new-instance v0, Lco/uk/getmondo/card/c$m;

    invoke-direct {v0, p0, p2}, Lco/uk/getmondo/card/c$m;-><init>(Lco/uk/getmondo/card/c;Lco/uk/getmondo/d/s;)V

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {v1, v0}, Lio/reactivex/v;->c(Lio/reactivex/c/h;)Lio/reactivex/b;

    move-result-object v1

    .line 55
    invoke-direct {p0, p1}, Lco/uk/getmondo/card/c;->c(Ljava/lang/String;)Lio/reactivex/b;

    move-result-object v0

    check-cast v0, Lio/reactivex/d;

    invoke-virtual {v1, v0}, Lio/reactivex/b;->b(Lio/reactivex/d;)Lio/reactivex/b;

    move-result-object v0

    const-string v1, "Single.fromCallable({ ca\u2026ardForAccount(accountId))"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public static final synthetic b(Lco/uk/getmondo/card/c;)Lco/uk/getmondo/card/s;
    .locals 1

    .prologue
    .line 18
    iget-object v0, p0, Lco/uk/getmondo/card/c;->c:Lco/uk/getmondo/card/s;

    return-object v0
.end method

.method public static final synthetic b(Lco/uk/getmondo/card/c;Ljava/lang/String;)Lio/reactivex/h;
    .locals 1

    .prologue
    .line 18
    invoke-direct {p0, p1}, Lco/uk/getmondo/card/c;->d(Ljava/lang/String;)Lio/reactivex/h;

    move-result-object v0

    return-object v0
.end method

.method public static final synthetic b(Lco/uk/getmondo/card/c;Ljava/lang/String;Lco/uk/getmondo/d/s;)Lio/reactivex/v;
    .locals 1

    .prologue
    .line 18
    invoke-direct {p0, p1, p2}, Lco/uk/getmondo/card/c;->b(Ljava/lang/String;Lco/uk/getmondo/d/s;)Lio/reactivex/v;

    move-result-object v0

    return-object v0
.end method

.method private final b(Ljava/lang/String;Lco/uk/getmondo/d/s;)Lio/reactivex/v;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lco/uk/getmondo/d/s;",
            ")",
            "Lio/reactivex/v",
            "<",
            "Ljava/util/Date;",
            ">;"
        }
    .end annotation

    .prologue
    .line 59
    new-instance v0, Lco/uk/getmondo/card/c$n;

    invoke-direct {v0, p0, p1}, Lco/uk/getmondo/card/c$n;-><init>(Lco/uk/getmondo/card/c;Ljava/lang/String;)V

    check-cast v0, Ljava/util/concurrent/Callable;

    invoke-static {v0}, Lio/reactivex/v;->c(Ljava/util/concurrent/Callable;)Lio/reactivex/v;

    move-result-object v1

    .line 60
    new-instance v0, Lco/uk/getmondo/card/c$o;

    invoke-direct {v0, p0, p2}, Lco/uk/getmondo/card/c$o;-><init>(Lco/uk/getmondo/card/c;Lco/uk/getmondo/d/s;)V

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {v1, v0}, Lio/reactivex/v;->a(Lio/reactivex/c/h;)Lio/reactivex/v;

    move-result-object v1

    .line 68
    sget-object v0, Lco/uk/getmondo/card/c$p;->a:Lco/uk/getmondo/card/c$p;

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {v1, v0}, Lio/reactivex/v;->d(Lio/reactivex/c/h;)Lio/reactivex/v;

    move-result-object v1

    .line 69
    new-instance v0, Lco/uk/getmondo/card/c$q;

    invoke-direct {v0, p0, p1}, Lco/uk/getmondo/card/c$q;-><init>(Lco/uk/getmondo/card/c;Ljava/lang/String;)V

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {v1, v0}, Lio/reactivex/v;->a(Lio/reactivex/c/h;)Lio/reactivex/v;

    move-result-object v0

    const-string v1, "Single.fromCallable { ca\u2026Id).toSingleDefault(it) }"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public static final synthetic c(Lco/uk/getmondo/card/c;)Lco/uk/getmondo/api/MonzoApi;
    .locals 1

    .prologue
    .line 18
    iget-object v0, p0, Lco/uk/getmondo/card/c;->b:Lco/uk/getmondo/api/MonzoApi;

    return-object v0
.end method

.method private final c(Ljava/lang/String;)Lio/reactivex/b;
    .locals 1

    .prologue
    .line 89
    invoke-direct {p0, p1}, Lco/uk/getmondo/card/c;->d(Ljava/lang/String;)Lio/reactivex/h;

    move-result-object v0

    invoke-virtual {v0}, Lio/reactivex/h;->c()Lio/reactivex/b;

    move-result-object v0

    return-object v0
.end method

.method private final d(Ljava/lang/String;)Lio/reactivex/h;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/h",
            "<",
            "Lco/uk/getmondo/d/g;",
            ">;"
        }
    .end annotation

    .prologue
    .line 92
    iget-object v0, p0, Lco/uk/getmondo/card/c;->b:Lco/uk/getmondo/api/MonzoApi;

    invoke-interface {v0, p1}, Lco/uk/getmondo/api/MonzoApi;->cards(Ljava/lang/String;)Lio/reactivex/v;

    move-result-object v1

    .line 93
    sget-object v0, Lco/uk/getmondo/card/c$f;->a:Lco/uk/getmondo/card/c$f;

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {v1, v0}, Lio/reactivex/v;->d(Lio/reactivex/c/h;)Lio/reactivex/v;

    move-result-object v1

    .line 94
    sget-object v0, Lco/uk/getmondo/card/c$g;->a:Lco/uk/getmondo/card/c$g;

    check-cast v0, Lio/reactivex/c/q;

    invoke-virtual {v1, v0}, Lio/reactivex/v;->a(Lio/reactivex/c/q;)Lio/reactivex/h;

    move-result-object v1

    .line 95
    sget-object v0, Lco/uk/getmondo/card/c$h;->a:Lco/uk/getmondo/card/c$h;

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {v1, v0}, Lio/reactivex/h;->c(Lio/reactivex/c/h;)Lio/reactivex/h;

    move-result-object v1

    .line 96
    new-instance v0, Lco/uk/getmondo/card/c$i;

    invoke-direct {v0, p0}, Lco/uk/getmondo/card/c$i;-><init>(Lco/uk/getmondo/card/c;)V

    check-cast v0, Lio/reactivex/c/g;

    invoke-virtual {v1, v0}, Lio/reactivex/h;->c(Lio/reactivex/c/g;)Lio/reactivex/h;

    move-result-object v0

    const-string v1, "monzoApi.cards(accountId\u2026ardStorage.saveCard(it) }"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;Lco/uk/getmondo/api/model/a;)Lio/reactivex/b;
    .locals 2

    .prologue
    const-string v0, "cardId"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "apiCardStatus"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 100
    iget-object v0, p0, Lco/uk/getmondo/card/c;->a:Lco/uk/getmondo/common/accounts/b;

    invoke-virtual {v0}, Lco/uk/getmondo/common/accounts/b;->e()Lio/reactivex/n;

    move-result-object v0

    invoke-virtual {v0}, Lio/reactivex/n;->firstOrError()Lio/reactivex/v;

    move-result-object v1

    .line 101
    new-instance v0, Lco/uk/getmondo/card/c$s;

    invoke-direct {v0, p0, p1, p2}, Lco/uk/getmondo/card/c$s;-><init>(Lco/uk/getmondo/card/c;Ljava/lang/String;Lco/uk/getmondo/api/model/a;)V

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {v1, v0}, Lio/reactivex/v;->c(Lio/reactivex/c/h;)Lio/reactivex/b;

    move-result-object v0

    const-string v1, "accountManager.accountId\u2026untId))\n                }"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public final a()Lio/reactivex/n;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Lco/uk/getmondo/d/g;",
            ">;"
        }
    .end annotation

    .prologue
    .line 37
    iget-object v0, p0, Lco/uk/getmondo/card/c;->a:Lco/uk/getmondo/common/accounts/b;

    invoke-virtual {v0}, Lco/uk/getmondo/common/accounts/b;->e()Lio/reactivex/n;

    move-result-object v1

    .line 38
    new-instance v0, Lco/uk/getmondo/card/c$e;

    invoke-direct {v0, p0}, Lco/uk/getmondo/card/c$e;-><init>(Lco/uk/getmondo/card/c;)V

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {v1, v0}, Lio/reactivex/n;->flatMap(Lio/reactivex/c/h;)Lio/reactivex/n;

    move-result-object v0

    const-string v1, "accountManager.accountId\u2026 { cardStorage.card(it) }"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public final a(Lco/uk/getmondo/d/s;)Lio/reactivex/v;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lco/uk/getmondo/d/s;",
            ")",
            "Lio/reactivex/v",
            "<",
            "Lcom/c/b/b",
            "<",
            "Ljava/util/Date;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 26
    new-instance v0, Lco/uk/getmondo/card/c$k;

    invoke-direct {v0, p0, p1}, Lco/uk/getmondo/card/c$k;-><init>(Lco/uk/getmondo/card/c;Lco/uk/getmondo/d/s;)V

    check-cast v0, Ljava/util/concurrent/Callable;

    invoke-static {v0}, Lio/reactivex/v;->a(Ljava/util/concurrent/Callable;)Lio/reactivex/v;

    move-result-object v0

    const-string v1, "Single.defer {\n         \u2026}\n            }\n        }"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public final a(Ljava/lang/String;)Lio/reactivex/v;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/v",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .prologue
    const-string v0, "accountId"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 74
    new-instance v0, Lco/uk/getmondo/card/c$j;

    invoke-direct {v0, p0, p1}, Lco/uk/getmondo/card/c$j;-><init>(Lco/uk/getmondo/card/c;Ljava/lang/String;)V

    check-cast v0, Ljava/util/concurrent/Callable;

    invoke-static {v0}, Lio/reactivex/v;->a(Ljava/util/concurrent/Callable;)Lio/reactivex/v;

    move-result-object v0

    const-string v1, "Single.defer {\n         \u2026)\n            }\n        }"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public final a(Ljava/lang/String;Z)Lio/reactivex/v;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Z)",
            "Lio/reactivex/v",
            "<",
            "Lco/uk/getmondo/d/g;",
            ">;"
        }
    .end annotation

    .prologue
    const-string v0, "cardToken"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 114
    new-instance v0, Lco/uk/getmondo/card/c$d;

    invoke-direct {v0, p0, p2, p1}, Lco/uk/getmondo/card/c$d;-><init>(Lco/uk/getmondo/card/c;ZLjava/lang/String;)V

    check-cast v0, Ljava/util/concurrent/Callable;

    invoke-static {v0}, Lio/reactivex/v;->a(Ljava/util/concurrent/Callable;)Lio/reactivex/v;

    move-result-object v1

    .line 124
    sget-object v0, Lco/uk/getmondo/card/c$b;->a:Lco/uk/getmondo/card/c$b;

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {v1, v0}, Lio/reactivex/v;->d(Lio/reactivex/c/h;)Lio/reactivex/v;

    move-result-object v1

    .line 125
    new-instance v0, Lco/uk/getmondo/card/c$c;

    invoke-direct {v0, p0}, Lco/uk/getmondo/card/c$c;-><init>(Lco/uk/getmondo/card/c;)V

    check-cast v0, Lio/reactivex/c/g;

    invoke-virtual {v1, v0}, Lio/reactivex/v;->c(Lio/reactivex/c/g;)Lio/reactivex/v;

    move-result-object v0

    const-string v1, "activationObservable\n   \u2026dStorage.saveCard(card) }"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public final b()Lco/uk/getmondo/d/g;
    .locals 2

    .prologue
    .line 42
    iget-object v0, p0, Lco/uk/getmondo/card/c;->a:Lco/uk/getmondo/common/accounts/b;

    invoke-virtual {v0}, Lco/uk/getmondo/common/accounts/b;->c()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lco/uk/getmondo/card/c;->c:Lco/uk/getmondo/card/s;

    invoke-virtual {v1, v0}, Lco/uk/getmondo/card/s;->a(Ljava/lang/String;)Lco/uk/getmondo/d/g;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Ljava/lang/String;)Lio/reactivex/v;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/v",
            "<",
            "Lco/uk/getmondo/d/g;",
            ">;"
        }
    .end annotation

    .prologue
    const-string v0, "cardPan"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 134
    iget-object v0, p0, Lco/uk/getmondo/card/c;->a:Lco/uk/getmondo/common/accounts/b;

    invoke-virtual {v0}, Lco/uk/getmondo/common/accounts/b;->e()Lio/reactivex/n;

    move-result-object v0

    invoke-virtual {v0}, Lio/reactivex/n;->firstOrError()Lio/reactivex/v;

    move-result-object v1

    .line 135
    new-instance v0, Lco/uk/getmondo/card/c$a;

    invoke-direct {v0, p0, p1}, Lco/uk/getmondo/card/c$a;-><init>(Lco/uk/getmondo/card/c;Ljava/lang/String;)V

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {v1, v0}, Lio/reactivex/v;->a(Lio/reactivex/c/h;)Lio/reactivex/v;

    move-result-object v0

    const-string v1, "accountManager.accountId\u2026card) }\n                }"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public final c()Lio/reactivex/b;
    .locals 2

    .prologue
    .line 85
    iget-object v0, p0, Lco/uk/getmondo/card/c;->a:Lco/uk/getmondo/common/accounts/b;

    invoke-virtual {v0}, Lco/uk/getmondo/common/accounts/b;->e()Lio/reactivex/n;

    move-result-object v0

    invoke-virtual {v0}, Lio/reactivex/n;->firstOrError()Lio/reactivex/v;

    move-result-object v1

    .line 86
    new-instance v0, Lco/uk/getmondo/card/c$r;

    invoke-direct {v0, p0}, Lco/uk/getmondo/card/c$r;-><init>(Lco/uk/getmondo/card/c;)V

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {v1, v0}, Lio/reactivex/v;->c(Lio/reactivex/c/h;)Lio/reactivex/b;

    move-result-object v0

    const-string v1, "accountManager.accountId\u2026 syncCardForAccount(it) }"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method
