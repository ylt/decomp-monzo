.class public final Lco/uk/getmondo/card/activate/ActivateCardActivity$b;
.super Ljava/lang/Object;
.source "ActivateCardActivity.kt"

# interfaces
.implements Landroid/text/TextWatcher;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lco/uk/getmondo/card/activate/ActivateCardActivity;->a(IZ)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000\'\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\r\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0005*\u0001\u0000\u0008\n\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0010\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006H\u0016J(\u0010\u0007\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00082\u0006\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\n2\u0006\u0010\u000c\u001a\u00020\nH\u0016J(\u0010\r\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00082\u0006\u0010\t\u001a\u00020\n2\u0006\u0010\u000e\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\nH\u0016\u00a8\u0006\u000f"
    }
    d2 = {
        "co/uk/getmondo/card/activate/ActivateCardActivity$setTextChangeListener$1",
        "Landroid/text/TextWatcher;",
        "(Lco/uk/getmondo/card/activate/ActivateCardActivity;IZ)V",
        "afterTextChanged",
        "",
        "s",
        "Landroid/text/Editable;",
        "beforeTextChanged",
        "",
        "start",
        "",
        "count",
        "after",
        "onTextChanged",
        "before",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field final synthetic a:Lco/uk/getmondo/card/activate/ActivateCardActivity;

.field final synthetic b:I

.field final synthetic c:Z


# direct methods
.method constructor <init>(Lco/uk/getmondo/card/activate/ActivateCardActivity;IZ)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(IZ)V"
        }
    .end annotation

    .prologue
    .line 114
    iput-object p1, p0, Lco/uk/getmondo/card/activate/ActivateCardActivity$b;->a:Lco/uk/getmondo/card/activate/ActivateCardActivity;

    iput p2, p0, Lco/uk/getmondo/card/activate/ActivateCardActivity$b;->b:I

    iput-boolean p3, p0, Lco/uk/getmondo/card/activate/ActivateCardActivity$b;->c:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 2

    .prologue
    const-string v0, "s"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 122
    invoke-interface {p1}, Landroid/text/Editable;->length()I

    move-result v0

    iget v1, p0, Lco/uk/getmondo/card/activate/ActivateCardActivity$b;->b:I

    if-lt v0, v1, :cond_0

    .line 123
    iget-object v0, p0, Lco/uk/getmondo/card/activate/ActivateCardActivity$b;->a:Lco/uk/getmondo/card/activate/ActivateCardActivity;

    sget v1, Lco/uk/getmondo/c$a;->tokenEditText:I

    invoke-virtual {v0, v1}, Lco/uk/getmondo/card/activate/ActivateCardActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setEnabled(Z)V

    .line 124
    iget-boolean v0, p0, Lco/uk/getmondo/card/activate/ActivateCardActivity$b;->c:Z

    if-eqz v0, :cond_1

    .line 125
    iget-object v0, p0, Lco/uk/getmondo/card/activate/ActivateCardActivity$b;->a:Lco/uk/getmondo/card/activate/ActivateCardActivity;

    invoke-virtual {v0}, Lco/uk/getmondo/card/activate/ActivateCardActivity;->a()Lco/uk/getmondo/card/activate/e;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lco/uk/getmondo/card/activate/e;->a(Ljava/lang/String;)V

    .line 128
    :cond_0
    :goto_0
    return-void

    .line 127
    :cond_1
    iget-object v0, p0, Lco/uk/getmondo/card/activate/ActivateCardActivity$b;->a:Lco/uk/getmondo/card/activate/ActivateCardActivity;

    invoke-static {v0}, Lco/uk/getmondo/card/activate/ActivateCardActivity;->a(Lco/uk/getmondo/card/activate/ActivateCardActivity;)Lcom/b/b/c;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/b/b/c;->a(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 1

    .prologue
    const-string v0, "s"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 119
    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 2

    .prologue
    const-string v0, "s"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 116
    iget-object v0, p0, Lco/uk/getmondo/card/activate/ActivateCardActivity$b;->a:Lco/uk/getmondo/card/activate/ActivateCardActivity;

    sget v1, Lco/uk/getmondo/c$a;->tokenTextInputLayout:I

    invoke-virtual {v0, v1}, Lco/uk/getmondo/card/activate/ActivateCardActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/design/widget/TextInputLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/design/widget/TextInputLayout;->setErrorEnabled(Z)V

    .line 117
    return-void
.end method
