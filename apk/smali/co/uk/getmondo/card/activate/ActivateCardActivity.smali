.class public final Lco/uk/getmondo/card/activate/ActivateCardActivity;
.super Lco/uk/getmondo/common/activities/b;
.source "ActivateCardActivity.kt"

# interfaces
.implements Lco/uk/getmondo/card/activate/e$a;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lco/uk/getmondo/card/activate/ActivateCardActivity$a;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000H\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0007\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0008\u0018\u0000 %2\u00020\u00012\u00020\u0002:\u0001%B\u0005\u00a2\u0006\u0002\u0010\u0003J\u0008\u0010\u000e\u001a\u00020\u000fH\u0016J\u0012\u0010\u0010\u001a\u00020\u000f2\u0008\u0010\u0011\u001a\u0004\u0018\u00010\u0012H\u0014J\u000e\u0010\u0013\u001a\u0008\u0012\u0004\u0012\u00020\u000c0\u0014H\u0016J\u0008\u0010\u0015\u001a\u00020\u000fH\u0014J\u0008\u0010\u0016\u001a\u00020\u000fH\u0016J\u0008\u0010\u0017\u001a\u00020\u000fH\u0016J\u0008\u0010\u0018\u001a\u00020\u000fH\u0016J\u0008\u0010\u0019\u001a\u00020\u000fH\u0016J\u0018\u0010\u001a\u001a\u00020\u000f2\u0006\u0010\u001b\u001a\u00020\u001c2\u0006\u0010\u001d\u001a\u00020\u001eH\u0002J\u0010\u0010\u001f\u001a\u00020\u000f2\u0006\u0010 \u001a\u00020\u000cH\u0016J\u0008\u0010!\u001a\u00020\u000fH\u0016J\u0008\u0010\"\u001a\u00020\u000fH\u0016J\u0008\u0010#\u001a\u00020\u000fH\u0016J\u0008\u0010$\u001a\u00020\u000fH\u0016R\u001e\u0010\u0004\u001a\u00020\u00058\u0006@\u0006X\u0087.\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u0006\u0010\u0007\"\u0004\u0008\u0008\u0010\tR2\u0010\n\u001a&\u0012\u000c\u0012\n \r*\u0004\u0018\u00010\u000c0\u000c \r*\u0012\u0012\u000c\u0012\n \r*\u0004\u0018\u00010\u000c0\u000c\u0018\u00010\u000b0\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006&"
    }
    d2 = {
        "Lco/uk/getmondo/card/activate/ActivateCardActivity;",
        "Lco/uk/getmondo/common/activities/BaseActivity;",
        "Lco/uk/getmondo/card/activate/ActivateCardPresenter$View;",
        "()V",
        "activateCardPresenter",
        "Lco/uk/getmondo/card/activate/ActivateCardPresenter;",
        "getActivateCardPresenter",
        "()Lco/uk/getmondo/card/activate/ActivateCardPresenter;",
        "setActivateCardPresenter",
        "(Lco/uk/getmondo/card/activate/ActivateCardPresenter;)V",
        "cardNumberRelay",
        "Lcom/jakewharton/rxrelay2/PublishRelay;",
        "",
        "kotlin.jvm.PlatformType",
        "hideLoading",
        "",
        "onCreate",
        "savedInstanceState",
        "Landroid/os/Bundle;",
        "onDebitPanEntered",
        "Lio/reactivex/Observable;",
        "onDestroy",
        "openCardActivatedConfirmation",
        "openHomeFeed",
        "openTopOfWaitinglist",
        "resetTokenInput",
        "setTextChangeListener",
        "maxLength",
        "",
        "isPrepaid",
        "",
        "setToken",
        "cardToken",
        "showLoading",
        "showPrepaidUi",
        "showRetailUi",
        "showTokenError",
        "Companion",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# static fields
.field public static final b:Lco/uk/getmondo/card/activate/ActivateCardActivity$a;


# instance fields
.field public a:Lco/uk/getmondo/card/activate/e;

.field private final c:Lcom/b/b/c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/b/b/c",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private e:Ljava/util/HashMap;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lco/uk/getmondo/card/activate/ActivateCardActivity$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lco/uk/getmondo/card/activate/ActivateCardActivity$a;-><init>(Lkotlin/d/b/i;)V

    sput-object v0, Lco/uk/getmondo/card/activate/ActivateCardActivity;->b:Lco/uk/getmondo/card/activate/ActivateCardActivity$a;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 27
    invoke-direct {p0}, Lco/uk/getmondo/common/activities/b;-><init>()V

    .line 28
    invoke-static {}, Lcom/b/b/c;->a()Lcom/b/b/c;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/card/activate/ActivateCardActivity;->c:Lcom/b/b/c;

    return-void
.end method

.method public static final a(Landroid/content/Context;)Landroid/content/Intent;
    .locals 6

    const/4 v3, 0x0

    sget-object v0, Lco/uk/getmondo/card/activate/ActivateCardActivity;->b:Lco/uk/getmondo/card/activate/ActivateCardActivity$a;

    const/4 v2, 0x0

    const/4 v4, 0x6

    move-object v1, p0

    move-object v5, v3

    invoke-static/range {v0 .. v5}, Lco/uk/getmondo/card/activate/ActivateCardActivity$a;->a(Lco/uk/getmondo/card/activate/ActivateCardActivity$a;Landroid/content/Context;ZLco/uk/getmondo/common/activities/b$a;ILjava/lang/Object;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public static final synthetic a(Lco/uk/getmondo/card/activate/ActivateCardActivity;)Lcom/b/b/c;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lco/uk/getmondo/card/activate/ActivateCardActivity;->c:Lcom/b/b/c;

    return-object v0
.end method

.method private final a(IZ)V
    .locals 2

    .prologue
    .line 114
    sget v0, Lco/uk/getmondo/c$a;->tokenEditText:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/card/activate/ActivateCardActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    new-instance v1, Lco/uk/getmondo/card/activate/ActivateCardActivity$b;

    invoke-direct {v1, p0, p1, p2}, Lco/uk/getmondo/card/activate/ActivateCardActivity$b;-><init>(Lco/uk/getmondo/card/activate/ActivateCardActivity;IZ)V

    check-cast v1, Landroid/text/TextWatcher;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 132
    return-void
.end method


# virtual methods
.method public a(I)Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lco/uk/getmondo/card/activate/ActivateCardActivity;->e:Ljava/util/HashMap;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lco/uk/getmondo/card/activate/ActivateCardActivity;->e:Ljava/util/HashMap;

    :cond_0
    iget-object v0, p0, Lco/uk/getmondo/card/activate/ActivateCardActivity;->e:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    if-nez v0, :cond_1

    invoke-virtual {p0, p1}, Landroid/support/v4/app/j;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/card/activate/ActivateCardActivity;->e:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    return-object v0
.end method

.method public final a()Lco/uk/getmondo/card/activate/e;
    .locals 2

    .prologue
    .line 30
    iget-object v0, p0, Lco/uk/getmondo/card/activate/ActivateCardActivity;->a:Lco/uk/getmondo/card/activate/e;

    if-nez v0, :cond_0

    const-string v1, "activateCardPresenter"

    invoke-static {v1}, Lkotlin/d/b/l;->b(Ljava/lang/String;)V

    :cond_0
    return-object v0
.end method

.method public a(Ljava/lang/String;)V
    .locals 1

    .prologue
    const-string v0, "cardToken"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 102
    sget v0, Lco/uk/getmondo/c$a;->tokenEditText:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/card/activate/ActivateCardActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    check-cast p1, Ljava/lang/CharSequence;

    invoke-virtual {v0, p1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 103
    return-void
.end method

.method public b()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 54
    sget v0, Lco/uk/getmondo/c$a;->activateCardImageView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/card/activate/ActivateCardActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    invoke-static {v0}, Lco/uk/getmondo/common/ae;->b(Landroid/view/View;)V

    .line 55
    sget v0, Lco/uk/getmondo/c$a;->activateCardInstructions:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/card/activate/ActivateCardActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const v1, 0x7f0a00c4

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 56
    invoke-virtual {p0}, Lco/uk/getmondo/card/activate/ActivateCardActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0d0009

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v3

    .line 57
    sget v0, Lco/uk/getmondo/c$a;->tokenEditText:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/card/activate/ActivateCardActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    const/4 v1, 0x1

    new-array v2, v1, [Landroid/text/InputFilter;

    new-instance v1, Landroid/text/InputFilter$LengthFilter;

    invoke-direct {v1, v3}, Landroid/text/InputFilter$LengthFilter;-><init>(I)V

    check-cast v1, Landroid/text/InputFilter;

    aput-object v1, v2, v4

    move-object v1, v2

    check-cast v1, [Ljava/lang/Object;

    .line 148
    check-cast v1, [Landroid/text/InputFilter;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setFilters([Landroid/text/InputFilter;)V

    .line 58
    invoke-direct {p0, v3, v4}, Lco/uk/getmondo/card/activate/ActivateCardActivity;->a(IZ)V

    .line 60
    const/16 v0, 0x20

    invoke-static {v0}, Lco/uk/getmondo/common/k/i;->a(C)Lco/uk/getmondo/common/k/i;

    move-result-object v0

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lco/uk/getmondo/common/k/i;->a(I)Lco/uk/getmondo/common/k/i;

    move-result-object v1

    .line 61
    sget v0, Lco/uk/getmondo/c$a;->tokenEditText:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/card/activate/ActivateCardActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    invoke-virtual {v1}, Lco/uk/getmondo/common/k/i;->a()Landroid/text/TextWatcher;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 62
    return-void
.end method

.method public c()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 65
    sget v0, Lco/uk/getmondo/c$a;->activateCardImageView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/card/activate/ActivateCardActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    invoke-static {v0}, Lco/uk/getmondo/common/ae;->a(Landroid/view/View;)V

    .line 66
    sget v0, Lco/uk/getmondo/c$a;->activateCardInstructions:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/card/activate/ActivateCardActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const v1, 0x7f0a00c6

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 67
    invoke-virtual {p0}, Lco/uk/getmondo/card/activate/ActivateCardActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0d000d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v3

    .line 68
    sget v0, Lco/uk/getmondo/c$a;->tokenEditText:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/card/activate/ActivateCardActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    new-array v2, v5, [Landroid/text/InputFilter;

    const/4 v4, 0x0

    new-instance v1, Landroid/text/InputFilter$LengthFilter;

    invoke-direct {v1, v3}, Landroid/text/InputFilter$LengthFilter;-><init>(I)V

    check-cast v1, Landroid/text/InputFilter;

    aput-object v1, v2, v4

    move-object v1, v2

    check-cast v1, [Ljava/lang/Object;

    .line 149
    check-cast v1, [Landroid/text/InputFilter;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setFilters([Landroid/text/InputFilter;)V

    .line 69
    invoke-direct {p0, v3, v5}, Lco/uk/getmondo/card/activate/ActivateCardActivity;->a(IZ)V

    .line 70
    return-void
.end method

.method public d()Lio/reactivex/n;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 73
    iget-object v0, p0, Lco/uk/getmondo/card/activate/ActivateCardActivity;->c:Lcom/b/b/c;

    const-string v1, "cardNumberRelay"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lio/reactivex/n;

    return-object v0
.end method

.method public e()V
    .locals 0

    .prologue
    .line 77
    check-cast p0, Landroid/content/Context;

    invoke-static {p0}, Lco/uk/getmondo/top/TopActivity;->a(Landroid/content/Context;)V

    .line 78
    return-void
.end method

.method public f()V
    .locals 1

    .prologue
    .line 81
    sget-object v0, Lco/uk/getmondo/main/HomeActivity;->f:Lco/uk/getmondo/main/HomeActivity$a;

    check-cast p0, Landroid/content/Context;

    invoke-virtual {v0, p0}, Lco/uk/getmondo/main/HomeActivity$a;->a(Landroid/content/Context;)V

    .line 82
    return-void
.end method

.method public g()V
    .locals 4

    .prologue
    .line 85
    sget-object v1, Lco/uk/getmondo/main/HomeActivity;->f:Lco/uk/getmondo/main/HomeActivity$a;

    move-object v0, p0

    check-cast v0, Landroid/content/Context;

    invoke-virtual {v1, v0}, Lco/uk/getmondo/main/HomeActivity$a;->b(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v1

    move-object v0, p0

    .line 86
    check-cast v0, Landroid/content/Context;

    const v2, 0x7f0a00ff

    invoke-virtual {p0, v2}, Lco/uk/getmondo/card/activate/ActivateCardActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 87
    const v3, 0x7f0a00fe

    invoke-virtual {p0, v3}, Lco/uk/getmondo/card/activate/ActivateCardActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 86
    invoke-static {v0, v2, v3, v1}, Lco/uk/getmondo/common/activities/ConfirmationActivity;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Landroid/content/Intent;)V

    .line 88
    return-void
.end method

.method public h()V
    .locals 2

    .prologue
    .line 91
    sget v0, Lco/uk/getmondo/c$a;->tokenEditText:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/card/activate/ActivateCardActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setEnabled(Z)V

    .line 92
    sget v0, Lco/uk/getmondo/c$a;->tokenEditText:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/card/activate/ActivateCardActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    const-string v1, ""

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 93
    sget v0, Lco/uk/getmondo/c$a;->tokenEditText:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/card/activate/ActivateCardActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->requestFocus()Z

    .line 94
    return-void
.end method

.method public i()V
    .locals 2

    .prologue
    .line 97
    sget v0, Lco/uk/getmondo/c$a;->tokenTextInputLayout:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/card/activate/ActivateCardActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/design/widget/TextInputLayout;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/support/design/widget/TextInputLayout;->setErrorEnabled(Z)V

    .line 98
    sget v0, Lco/uk/getmondo/c$a;->tokenTextInputLayout:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/card/activate/ActivateCardActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/design/widget/TextInputLayout;

    const v1, 0x7f0a0100

    invoke-virtual {p0, v1}, Lco/uk/getmondo/card/activate/ActivateCardActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Landroid/support/design/widget/TextInputLayout;->setError(Ljava/lang/CharSequence;)V

    .line 99
    return-void
.end method

.method public j()V
    .locals 0

    .prologue
    .line 106
    invoke-virtual {p0}, Lco/uk/getmondo/card/activate/ActivateCardActivity;->s()V

    .line 107
    return-void
.end method

.method public k()V
    .locals 0

    .prologue
    .line 110
    invoke-virtual {p0}, Lco/uk/getmondo/card/activate/ActivateCardActivity;->t()V

    .line 111
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 33
    invoke-virtual {p0}, Lco/uk/getmondo/card/activate/ActivateCardActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "EXTRA_THEME"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/common/activities/b$a;

    .line 34
    if-eqz v0, :cond_0

    .line 35
    sget-object v1, Lco/uk/getmondo/common/activities/b$a;->b:Lco/uk/getmondo/common/activities/b$a;

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    const v0, 0x7f0c0128

    .line 36
    :goto_0
    invoke-virtual {p0, v0}, Lco/uk/getmondo/card/activate/ActivateCardActivity;->setTheme(I)V

    .line 39
    :cond_0
    invoke-super {p0, p1}, Lco/uk/getmondo/common/activities/b;->onCreate(Landroid/os/Bundle;)V

    .line 40
    const v0, 0x7f05001b

    invoke-virtual {p0, v0}, Lco/uk/getmondo/card/activate/ActivateCardActivity;->setContentView(I)V

    .line 42
    invoke-virtual {p0}, Lco/uk/getmondo/card/activate/ActivateCardActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "EXTRA_IS_REPLACEMENT"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    .line 43
    invoke-virtual {p0}, Lco/uk/getmondo/card/activate/ActivateCardActivity;->l()Lco/uk/getmondo/common/h/b/b;

    move-result-object v1

    new-instance v2, Lco/uk/getmondo/card/activate/c;

    invoke-direct {v2, v0}, Lco/uk/getmondo/card/activate/c;-><init>(Z)V

    invoke-interface {v1, v2}, Lco/uk/getmondo/common/h/b/b;->a(Lco/uk/getmondo/card/activate/c;)Lco/uk/getmondo/card/activate/b;

    move-result-object v0

    invoke-interface {v0, p0}, Lco/uk/getmondo/card/activate/b;->a(Lco/uk/getmondo/card/activate/ActivateCardActivity;)V

    .line 44
    iget-object v0, p0, Lco/uk/getmondo/card/activate/ActivateCardActivity;->a:Lco/uk/getmondo/card/activate/e;

    if-nez v0, :cond_1

    const-string v1, "activateCardPresenter"

    invoke-static {v1}, Lkotlin/d/b/l;->b(Ljava/lang/String;)V

    :cond_1
    check-cast p0, Lco/uk/getmondo/card/activate/e$a;

    invoke-virtual {v0, p0}, Lco/uk/getmondo/card/activate/e;->a(Lco/uk/getmondo/card/activate/e$a;)V

    .line 46
    return-void

    .line 35
    :cond_2
    const v0, 0x7f0c0125

    goto :goto_0
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    .line 49
    iget-object v0, p0, Lco/uk/getmondo/card/activate/ActivateCardActivity;->a:Lco/uk/getmondo/card/activate/e;

    if-nez v0, :cond_0

    const-string v1, "activateCardPresenter"

    invoke-static {v1}, Lkotlin/d/b/l;->b(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0}, Lco/uk/getmondo/card/activate/e;->b()V

    .line 50
    invoke-super {p0}, Lco/uk/getmondo/common/activities/b;->onDestroy()V

    .line 51
    return-void
.end method
