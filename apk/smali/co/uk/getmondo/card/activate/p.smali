.class final enum Lco/uk/getmondo/card/activate/p;
.super Ljava/lang/Enum;
.source "CardActivationError.java"

# interfaces
.implements Lco/uk/getmondo/common/e/f;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lco/uk/getmondo/card/activate/p;",
        ">;",
        "Lco/uk/getmondo/common/e/f;"
    }
.end annotation


# static fields
.field public static final enum a:Lco/uk/getmondo/card/activate/p;

.field public static final enum b:Lco/uk/getmondo/card/activate/p;

.field private static final synthetic e:[Lco/uk/getmondo/card/activate/p;


# instance fields
.field public final c:I

.field private final d:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 11
    new-instance v0, Lco/uk/getmondo/card/activate/p;

    const-string v1, "INVALID_TOKEN"

    const-string v2, "bad_request.bad_param.processor_token"

    const v3, 0x7f0a0100

    invoke-direct {v0, v1, v4, v2, v3}, Lco/uk/getmondo/card/activate/p;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, Lco/uk/getmondo/card/activate/p;->a:Lco/uk/getmondo/card/activate/p;

    .line 12
    new-instance v0, Lco/uk/getmondo/card/activate/p;

    const-string v1, "ACCOUNT_CANNOT_USE_TOKEN"

    const-string v2, "bad_request.activation_failed"

    const v3, 0x7f0a00fd

    invoke-direct {v0, v1, v5, v2, v3}, Lco/uk/getmondo/card/activate/p;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, Lco/uk/getmondo/card/activate/p;->b:Lco/uk/getmondo/card/activate/p;

    .line 9
    const/4 v0, 0x2

    new-array v0, v0, [Lco/uk/getmondo/card/activate/p;

    sget-object v1, Lco/uk/getmondo/card/activate/p;->a:Lco/uk/getmondo/card/activate/p;

    aput-object v1, v0, v4

    sget-object v1, Lco/uk/getmondo/card/activate/p;->b:Lco/uk/getmondo/card/activate/p;

    aput-object v1, v0, v5

    sput-object v0, Lco/uk/getmondo/card/activate/p;->e:[Lco/uk/getmondo/card/activate/p;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "I)V"
        }
    .end annotation

    .prologue
    .line 17
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 18
    iput-object p3, p0, Lco/uk/getmondo/card/activate/p;->d:Ljava/lang/String;

    .line 19
    iput p4, p0, Lco/uk/getmondo/card/activate/p;->c:I

    .line 20
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lco/uk/getmondo/card/activate/p;
    .locals 1

    .prologue
    .line 9
    const-class v0, Lco/uk/getmondo/card/activate/p;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/card/activate/p;

    return-object v0
.end method

.method public static values()[Lco/uk/getmondo/card/activate/p;
    .locals 1

    .prologue
    .line 9
    sget-object v0, Lco/uk/getmondo/card/activate/p;->e:[Lco/uk/getmondo/card/activate/p;

    invoke-virtual {v0}, [Lco/uk/getmondo/card/activate/p;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lco/uk/getmondo/card/activate/p;

    return-object v0
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, Lco/uk/getmondo/card/activate/p;->d:Ljava/lang/String;

    return-object v0
.end method

.method public b()I
    .locals 1

    .prologue
    .line 29
    iget v0, p0, Lco/uk/getmondo/card/activate/p;->c:I

    return v0
.end method
