.class public Lco/uk/getmondo/card/activate/e;
.super Lco/uk/getmondo/common/ui/b;
.source "ActivateCardPresenter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lco/uk/getmondo/card/activate/e$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lco/uk/getmondo/common/ui/b",
        "<",
        "Lco/uk/getmondo/card/activate/e$a;",
        ">;"
    }
.end annotation


# instance fields
.field private final c:Lio/reactivex/u;

.field private final d:Lio/reactivex/u;

.field private final e:Lco/uk/getmondo/common/accounts/d;

.field private final f:Lco/uk/getmondo/common/e/a;

.field private final g:Lco/uk/getmondo/card/c;

.field private final h:Lco/uk/getmondo/common/a;

.field private final i:Lco/uk/getmondo/waitlist/i;

.field private final j:Z


# direct methods
.method constructor <init>(Lio/reactivex/u;Lio/reactivex/u;Lco/uk/getmondo/common/accounts/d;Lco/uk/getmondo/common/e/a;Lco/uk/getmondo/card/c;Lco/uk/getmondo/common/a;Lco/uk/getmondo/waitlist/i;Z)V
    .locals 0

    .prologue
    .line 40
    invoke-direct {p0}, Lco/uk/getmondo/common/ui/b;-><init>()V

    .line 41
    iput-object p1, p0, Lco/uk/getmondo/card/activate/e;->c:Lio/reactivex/u;

    .line 42
    iput-object p2, p0, Lco/uk/getmondo/card/activate/e;->d:Lio/reactivex/u;

    .line 43
    iput-object p3, p0, Lco/uk/getmondo/card/activate/e;->e:Lco/uk/getmondo/common/accounts/d;

    .line 44
    iput-object p4, p0, Lco/uk/getmondo/card/activate/e;->f:Lco/uk/getmondo/common/e/a;

    .line 45
    iput-object p5, p0, Lco/uk/getmondo/card/activate/e;->g:Lco/uk/getmondo/card/c;

    .line 46
    iput-object p6, p0, Lco/uk/getmondo/card/activate/e;->h:Lco/uk/getmondo/common/a;

    .line 47
    iput-object p7, p0, Lco/uk/getmondo/card/activate/e;->i:Lco/uk/getmondo/waitlist/i;

    .line 48
    iput-boolean p8, p0, Lco/uk/getmondo/card/activate/e;->j:Z

    .line 49
    return-void
.end method

.method static synthetic a(Lco/uk/getmondo/card/activate/e;Lco/uk/getmondo/card/activate/e$a;Ljava/lang/String;)Lio/reactivex/l;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 68
    iget-object v0, p0, Lco/uk/getmondo/card/activate/e;->g:Lco/uk/getmondo/card/c;

    const-string v1, " "

    const-string v2, ""

    invoke-virtual {p2, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lco/uk/getmondo/card/c;->b(Ljava/lang/String;)Lio/reactivex/v;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/card/activate/e;->d:Lio/reactivex/u;

    .line 69
    invoke-virtual {v0, v1}, Lio/reactivex/v;->b(Lio/reactivex/u;)Lio/reactivex/v;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/card/activate/e;->c:Lio/reactivex/u;

    .line 70
    invoke-virtual {v0, v1}, Lio/reactivex/v;->a(Lio/reactivex/u;)Lio/reactivex/v;

    move-result-object v0

    invoke-static {p1}, Lco/uk/getmondo/card/activate/l;->a(Lco/uk/getmondo/card/activate/e$a;)Lio/reactivex/c/g;

    move-result-object v1

    .line 71
    invoke-virtual {v0, v1}, Lio/reactivex/v;->b(Lio/reactivex/c/g;)Lio/reactivex/v;

    move-result-object v0

    invoke-static {p0, p1}, Lco/uk/getmondo/card/activate/m;->a(Lco/uk/getmondo/card/activate/e;Lco/uk/getmondo/card/activate/e$a;)Lio/reactivex/c/g;

    move-result-object v1

    .line 72
    invoke-virtual {v0, v1}, Lio/reactivex/v;->d(Lio/reactivex/c/g;)Lio/reactivex/v;

    move-result-object v0

    invoke-static {p1}, Lco/uk/getmondo/card/activate/n;->a(Lco/uk/getmondo/card/activate/e$a;)Lio/reactivex/c/b;

    move-result-object v1

    .line 73
    invoke-virtual {v0, v1}, Lio/reactivex/v;->a(Lio/reactivex/c/b;)Lio/reactivex/v;

    move-result-object v0

    .line 77
    invoke-virtual {v0}, Lio/reactivex/v;->e()Lio/reactivex/h;

    move-result-object v0

    .line 78
    invoke-virtual {v0}, Lio/reactivex/h;->f()Lio/reactivex/h;

    move-result-object v0

    .line 68
    return-object v0
.end method

.method static synthetic a(Lco/uk/getmondo/card/activate/e$a;Lco/uk/getmondo/d/g;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 79
    invoke-interface {p0}, Lco/uk/getmondo/card/activate/e$a;->f()V

    return-void
.end method

.method static synthetic a(Lco/uk/getmondo/card/activate/e$a;Lco/uk/getmondo/d/g;Ljava/lang/Throwable;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 74
    invoke-interface {p0}, Lco/uk/getmondo/card/activate/e$a;->k()V

    .line 75
    invoke-interface {p0}, Lco/uk/getmondo/card/activate/e$a;->h()V

    .line 76
    return-void
.end method

.method static synthetic a(Lco/uk/getmondo/card/activate/e$a;Lio/reactivex/b/b;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 71
    invoke-interface {p0}, Lco/uk/getmondo/card/activate/e$a;->j()V

    return-void
.end method

.method static synthetic a(Lco/uk/getmondo/card/activate/e;Lco/uk/getmondo/card/activate/e$a;Ljava/lang/Throwable;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 72
    iget-object v0, p0, Lco/uk/getmondo/card/activate/e;->f:Lco/uk/getmondo/common/e/a;

    invoke-virtual {v0, p2, p1}, Lco/uk/getmondo/common/e/a;->a(Ljava/lang/Throwable;Lco/uk/getmondo/common/e/a$a;)Z

    return-void
.end method

.method static synthetic a(Lco/uk/getmondo/card/activate/e;Lco/uk/getmondo/d/ak;Lco/uk/getmondo/d/g;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 97
    iget-object v0, p0, Lco/uk/getmondo/card/activate/e;->a:Lco/uk/getmondo/common/ui/f;

    check-cast v0, Lco/uk/getmondo/card/activate/e$a;

    invoke-interface {v0}, Lco/uk/getmondo/card/activate/e$a;->k()V

    .line 98
    invoke-virtual {p1}, Lco/uk/getmondo/d/ak;->c()Lco/uk/getmondo/d/a;

    move-result-object v0

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lco/uk/getmondo/d/a;->a(Z)Lco/uk/getmondo/d/a;

    move-result-object v0

    .line 99
    iget-object v1, p0, Lco/uk/getmondo/card/activate/e;->e:Lco/uk/getmondo/common/accounts/d;

    invoke-virtual {v1, v0}, Lco/uk/getmondo/common/accounts/d;->a(Lco/uk/getmondo/d/a;)V

    .line 100
    iget-boolean v0, p0, Lco/uk/getmondo/card/activate/e;->j:Z

    if-eqz v0, :cond_0

    .line 101
    iget-object v0, p0, Lco/uk/getmondo/card/activate/e;->a:Lco/uk/getmondo/common/ui/f;

    check-cast v0, Lco/uk/getmondo/card/activate/e$a;

    invoke-interface {v0}, Lco/uk/getmondo/card/activate/e$a;->g()V

    .line 105
    :goto_0
    return-void

    .line 103
    :cond_0
    iget-object v0, p0, Lco/uk/getmondo/card/activate/e;->a:Lco/uk/getmondo/common/ui/f;

    check-cast v0, Lco/uk/getmondo/card/activate/e$a;

    invoke-interface {v0}, Lco/uk/getmondo/card/activate/e$a;->f()V

    goto :goto_0
.end method

.method static synthetic a(Lco/uk/getmondo/card/activate/e;Lco/uk/getmondo/d/g;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 93
    iget-object v0, p0, Lco/uk/getmondo/card/activate/e;->i:Lco/uk/getmondo/waitlist/i;

    invoke-virtual {v0}, Lco/uk/getmondo/waitlist/i;->c()V

    return-void
.end method

.method static synthetic a(Lco/uk/getmondo/card/activate/e;Ljava/lang/Throwable;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 106
    iget-object v0, p0, Lco/uk/getmondo/card/activate/e;->a:Lco/uk/getmondo/common/ui/f;

    check-cast v0, Lco/uk/getmondo/card/activate/e$a;

    invoke-interface {v0}, Lco/uk/getmondo/card/activate/e$a;->k()V

    .line 107
    iget-object v0, p0, Lco/uk/getmondo/card/activate/e;->a:Lco/uk/getmondo/common/ui/f;

    check-cast v0, Lco/uk/getmondo/card/activate/e$a;

    invoke-interface {v0}, Lco/uk/getmondo/card/activate/e$a;->h()V

    .line 109
    instance-of v0, p1, Lco/uk/getmondo/api/ApiException;

    if-eqz v0, :cond_1

    move-object v0, p1

    check-cast v0, Lco/uk/getmondo/api/ApiException;

    invoke-virtual {v0}, Lco/uk/getmondo/api/ApiException;->e()Lco/uk/getmondo/api/model/b;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 110
    invoke-static {}, Lco/uk/getmondo/card/activate/p;->values()[Lco/uk/getmondo/card/activate/p;

    move-result-object v1

    .line 111
    invoke-virtual {v0}, Lco/uk/getmondo/api/model/b;->a()Ljava/lang/String;

    move-result-object v0

    .line 110
    invoke-static {v1, v0}, Lco/uk/getmondo/common/e/d;->a([Lco/uk/getmondo/common/e/f;Ljava/lang/String;)Lco/uk/getmondo/common/e/f;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/card/activate/p;

    .line 112
    sget-object v1, Lco/uk/getmondo/card/activate/p;->a:Lco/uk/getmondo/card/activate/p;

    invoke-virtual {v1, v0}, Lco/uk/getmondo/card/activate/p;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 113
    iget-object v0, p0, Lco/uk/getmondo/card/activate/e;->a:Lco/uk/getmondo/common/ui/f;

    check-cast v0, Lco/uk/getmondo/card/activate/e$a;

    invoke-interface {v0}, Lco/uk/getmondo/card/activate/e$a;->i()V

    .line 121
    :goto_0
    return-void

    .line 115
    :cond_0
    if-eqz v0, :cond_1

    .line 116
    iget-object v1, p0, Lco/uk/getmondo/card/activate/e;->a:Lco/uk/getmondo/common/ui/f;

    check-cast v1, Lco/uk/getmondo/card/activate/e$a;

    invoke-virtual {v0}, Lco/uk/getmondo/card/activate/p;->b()I

    move-result v0

    invoke-interface {v1, v0}, Lco/uk/getmondo/card/activate/e$a;->b(I)V

    goto :goto_0

    .line 120
    :cond_1
    iget-object v1, p0, Lco/uk/getmondo/card/activate/e;->f:Lco/uk/getmondo/common/e/a;

    iget-object v0, p0, Lco/uk/getmondo/card/activate/e;->a:Lco/uk/getmondo/common/ui/f;

    check-cast v0, Lco/uk/getmondo/common/e/a$a;

    invoke-virtual {v1, p1, v0}, Lco/uk/getmondo/common/e/a;->a(Ljava/lang/Throwable;Lco/uk/getmondo/common/e/a$a;)Z

    goto :goto_0
.end method


# virtual methods
.method public a(Lco/uk/getmondo/card/activate/e$a;)V
    .locals 3

    .prologue
    .line 53
    invoke-super {p0, p1}, Lco/uk/getmondo/common/ui/b;->a(Lco/uk/getmondo/common/ui/f;)V

    .line 54
    iget-object v1, p0, Lco/uk/getmondo/card/activate/e;->h:Lco/uk/getmondo/common/a;

    iget-boolean v0, p0, Lco/uk/getmondo/card/activate/e;->j:Z

    if-eqz v0, :cond_1

    invoke-static {}, Lco/uk/getmondo/api/model/tracking/Impression;->I()Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v0}, Lco/uk/getmondo/common/a;->a(Lco/uk/getmondo/api/model/tracking/Impression;)V

    .line 56
    iget-object v0, p0, Lco/uk/getmondo/card/activate/e;->e:Lco/uk/getmondo/common/accounts/d;

    invoke-virtual {v0}, Lco/uk/getmondo/common/accounts/d;->b()Lco/uk/getmondo/d/ak;

    move-result-object v0

    .line 57
    invoke-virtual {v0}, Lco/uk/getmondo/d/ak;->a()Lco/uk/getmondo/d/ak$a;

    move-result-object v1

    sget-object v2, Lco/uk/getmondo/d/ak$a;->NO_CARD:Lco/uk/getmondo/d/ak$a;

    if-ne v1, v2, :cond_0

    iget-object v1, p0, Lco/uk/getmondo/card/activate/e;->i:Lco/uk/getmondo/waitlist/i;

    invoke-virtual {v1}, Lco/uk/getmondo/waitlist/i;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lco/uk/getmondo/common/k/p;->c(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 58
    iget-object v1, p0, Lco/uk/getmondo/card/activate/e;->i:Lco/uk/getmondo/waitlist/i;

    invoke-virtual {v1}, Lco/uk/getmondo/waitlist/i;->b()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v1}, Lco/uk/getmondo/card/activate/e$a;->a(Ljava/lang/String;)V

    .line 59
    iget-object v1, p0, Lco/uk/getmondo/card/activate/e;->i:Lco/uk/getmondo/waitlist/i;

    invoke-virtual {v1}, Lco/uk/getmondo/waitlist/i;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lco/uk/getmondo/card/activate/e;->a(Ljava/lang/String;)V

    .line 61
    :cond_0
    invoke-virtual {v0}, Lco/uk/getmondo/d/ak;->c()Lco/uk/getmondo/d/a;

    move-result-object v0

    invoke-interface {v0}, Lco/uk/getmondo/d/a;->e()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 62
    invoke-interface {p1}, Lco/uk/getmondo/card/activate/e$a;->b()V

    .line 67
    :goto_1
    invoke-interface {p1}, Lco/uk/getmondo/card/activate/e$a;->d()Lio/reactivex/n;

    move-result-object v0

    invoke-static {p0, p1}, Lco/uk/getmondo/card/activate/f;->a(Lco/uk/getmondo/card/activate/e;Lco/uk/getmondo/card/activate/e$a;)Lio/reactivex/c/h;

    move-result-object v1

    .line 68
    invoke-virtual {v0, v1}, Lio/reactivex/n;->flatMapMaybe(Lio/reactivex/c/h;)Lio/reactivex/n;

    move-result-object v0

    invoke-static {p1}, Lco/uk/getmondo/card/activate/g;->a(Lco/uk/getmondo/card/activate/e$a;)Lio/reactivex/c/g;

    move-result-object v1

    invoke-static {}, Lco/uk/getmondo/card/activate/h;->a()Lio/reactivex/c/g;

    move-result-object v2

    .line 79
    invoke-virtual {v0, v1, v2}, Lio/reactivex/n;->subscribe(Lio/reactivex/c/g;Lio/reactivex/c/g;)Lio/reactivex/b/b;

    move-result-object v0

    .line 67
    invoke-virtual {p0, v0}, Lco/uk/getmondo/card/activate/e;->a(Lio/reactivex/b/b;)V

    .line 80
    return-void

    .line 54
    :cond_1
    invoke-static {}, Lco/uk/getmondo/api/model/tracking/Impression;->B()Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    goto :goto_0

    .line 64
    :cond_2
    invoke-interface {p1}, Lco/uk/getmondo/card/activate/e$a;->c()V

    goto :goto_1
.end method

.method public bridge synthetic a(Lco/uk/getmondo/common/ui/f;)V
    .locals 0

    .prologue
    .line 26
    check-cast p1, Lco/uk/getmondo/card/activate/e$a;

    invoke-virtual {p0, p1}, Lco/uk/getmondo/card/activate/e;->a(Lco/uk/getmondo/card/activate/e$a;)V

    return-void
.end method

.method a(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 83
    iget-object v0, p0, Lco/uk/getmondo/card/activate/e;->e:Lco/uk/getmondo/common/accounts/d;

    invoke-virtual {v0}, Lco/uk/getmondo/common/accounts/d;->b()Lco/uk/getmondo/d/ak;

    move-result-object v1

    .line 87
    iget-boolean v0, p0, Lco/uk/getmondo/card/activate/e;->j:Z

    if-nez v0, :cond_0

    invoke-virtual {v1}, Lco/uk/getmondo/d/ak;->a()Lco/uk/getmondo/d/ak$a;

    move-result-object v0

    sget-object v2, Lco/uk/getmondo/d/ak$a;->ON_WAITLIST:Lco/uk/getmondo/d/ak$a;

    if-ne v0, v2, :cond_0

    iget-object v0, p0, Lco/uk/getmondo/card/activate/e;->i:Lco/uk/getmondo/waitlist/i;

    invoke-virtual {v0}, Lco/uk/getmondo/waitlist/i;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 88
    iget-object v0, p0, Lco/uk/getmondo/card/activate/e;->i:Lco/uk/getmondo/waitlist/i;

    invoke-virtual {v0, p1}, Lco/uk/getmondo/waitlist/i;->a(Ljava/lang/String;)V

    .line 89
    iget-object v0, p0, Lco/uk/getmondo/card/activate/e;->a:Lco/uk/getmondo/common/ui/f;

    check-cast v0, Lco/uk/getmondo/card/activate/e$a;

    invoke-interface {v0}, Lco/uk/getmondo/card/activate/e$a;->e()V

    .line 123
    :goto_0
    return-void

    .line 91
    :cond_0
    iget-object v0, p0, Lco/uk/getmondo/card/activate/e;->a:Lco/uk/getmondo/common/ui/f;

    check-cast v0, Lco/uk/getmondo/card/activate/e$a;

    invoke-interface {v0}, Lco/uk/getmondo/card/activate/e$a;->j()V

    .line 92
    iget-object v0, p0, Lco/uk/getmondo/card/activate/e;->g:Lco/uk/getmondo/card/c;

    iget-boolean v2, p0, Lco/uk/getmondo/card/activate/e;->j:Z

    invoke-virtual {v0, p1, v2}, Lco/uk/getmondo/card/c;->a(Ljava/lang/String;Z)Lio/reactivex/v;

    move-result-object v0

    invoke-static {p0}, Lco/uk/getmondo/card/activate/i;->a(Lco/uk/getmondo/card/activate/e;)Lio/reactivex/c/g;

    move-result-object v2

    .line 93
    invoke-virtual {v0, v2}, Lio/reactivex/v;->c(Lio/reactivex/c/g;)Lio/reactivex/v;

    move-result-object v0

    iget-object v2, p0, Lco/uk/getmondo/card/activate/e;->d:Lio/reactivex/u;

    .line 94
    invoke-virtual {v0, v2}, Lio/reactivex/v;->b(Lio/reactivex/u;)Lio/reactivex/v;

    move-result-object v0

    iget-object v2, p0, Lco/uk/getmondo/card/activate/e;->c:Lio/reactivex/u;

    .line 95
    invoke-virtual {v0, v2}, Lio/reactivex/v;->a(Lio/reactivex/u;)Lio/reactivex/v;

    move-result-object v0

    invoke-static {p0, v1}, Lco/uk/getmondo/card/activate/j;->a(Lco/uk/getmondo/card/activate/e;Lco/uk/getmondo/d/ak;)Lio/reactivex/c/g;

    move-result-object v1

    invoke-static {p0}, Lco/uk/getmondo/card/activate/k;->a(Lco/uk/getmondo/card/activate/e;)Lio/reactivex/c/g;

    move-result-object v2

    .line 96
    invoke-virtual {v0, v1, v2}, Lio/reactivex/v;->a(Lio/reactivex/c/g;Lio/reactivex/c/g;)Lio/reactivex/b/b;

    move-result-object v0

    .line 92
    invoke-virtual {p0, v0}, Lco/uk/getmondo/card/activate/e;->a(Lio/reactivex/b/b;)V

    goto :goto_0
.end method
