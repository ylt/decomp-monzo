.class final Lco/uk/getmondo/card/c$s;
.super Ljava/lang/Object;
.source "CardManager.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lco/uk/getmondo/card/c;->a(Ljava/lang/String;Lco/uk/getmondo/api/model/a;)Lio/reactivex/b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h",
        "<",
        "Ljava/lang/String;",
        "Lio/reactivex/d;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0000\u0010\u0000\u001a\n \u0002*\u0004\u0018\u00010\u00010\u00012\u0006\u0010\u0003\u001a\u00020\u0004H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "Lio/reactivex/Completable;",
        "kotlin.jvm.PlatformType",
        "accountId",
        "",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field final synthetic a:Lco/uk/getmondo/card/c;

.field final synthetic b:Ljava/lang/String;

.field final synthetic c:Lco/uk/getmondo/api/model/a;


# direct methods
.method constructor <init>(Lco/uk/getmondo/card/c;Ljava/lang/String;Lco/uk/getmondo/api/model/a;)V
    .locals 0

    iput-object p1, p0, Lco/uk/getmondo/card/c$s;->a:Lco/uk/getmondo/card/c;

    iput-object p2, p0, Lco/uk/getmondo/card/c$s;->b:Ljava/lang/String;

    iput-object p3, p0, Lco/uk/getmondo/card/c$s;->c:Lco/uk/getmondo/api/model/a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Lio/reactivex/b;
    .locals 3

    .prologue
    const-string v0, "accountId"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 102
    iget-object v0, p0, Lco/uk/getmondo/card/c$s;->a:Lco/uk/getmondo/card/c;

    invoke-static {v0}, Lco/uk/getmondo/card/c;->c(Lco/uk/getmondo/card/c;)Lco/uk/getmondo/api/MonzoApi;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/card/c$s;->b:Ljava/lang/String;

    iget-object v2, p0, Lco/uk/getmondo/card/c$s;->c:Lco/uk/getmondo/api/model/a;

    invoke-interface {v0, v1, v2}, Lco/uk/getmondo/api/MonzoApi;->toggleCard(Ljava/lang/String;Lco/uk/getmondo/api/model/a;)Lio/reactivex/b;

    move-result-object v1

    .line 103
    iget-object v0, p0, Lco/uk/getmondo/card/c$s;->a:Lco/uk/getmondo/card/c;

    invoke-static {v0, p1}, Lco/uk/getmondo/card/c;->a(Lco/uk/getmondo/card/c;Ljava/lang/String;)Lio/reactivex/b;

    move-result-object v0

    check-cast v0, Lio/reactivex/d;

    invoke-virtual {v1, v0}, Lio/reactivex/b;->b(Lio/reactivex/d;)Lio/reactivex/b;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 18
    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1}, Lco/uk/getmondo/card/c$s;->a(Ljava/lang/String;)Lio/reactivex/b;

    move-result-object v0

    return-object v0
.end method
