.class final Lco/uk/getmondo/card/c$o;
.super Ljava/lang/Object;
.source "CardManager.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lco/uk/getmondo/card/c;->b(Ljava/lang/String;Lco/uk/getmondo/d/s;)Lio/reactivex/v;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h",
        "<TT;",
        "Lio/reactivex/z",
        "<+TR;>;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000\u0014\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a&\u0012\u000c\u0012\n \u0003*\u0004\u0018\u00010\u00020\u0002 \u0003*\u0012\u0012\u000c\u0012\n \u0003*\u0004\u0018\u00010\u00020\u0002\u0018\u00010\u00010\u00012\u0006\u0010\u0004\u001a\u00020\u0005H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "Lio/reactivex/Single;",
        "Lco/uk/getmondo/api/model/ApiCardReplacementOrder;",
        "kotlin.jvm.PlatformType",
        "card",
        "Lco/uk/getmondo/model/Card;",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field final synthetic a:Lco/uk/getmondo/card/c;

.field final synthetic b:Lco/uk/getmondo/d/s;


# direct methods
.method constructor <init>(Lco/uk/getmondo/card/c;Lco/uk/getmondo/d/s;)V
    .locals 0

    iput-object p1, p0, Lco/uk/getmondo/card/c$o;->a:Lco/uk/getmondo/card/c;

    iput-object p2, p0, Lco/uk/getmondo/card/c$o;->b:Lco/uk/getmondo/d/s;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lco/uk/getmondo/d/g;)Lio/reactivex/v;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lco/uk/getmondo/d/g;",
            ")",
            "Lio/reactivex/v",
            "<",
            "Lco/uk/getmondo/api/model/ApiCardReplacementOrder;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    const-string v0, "card"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 61
    iget-object v0, p0, Lco/uk/getmondo/card/c$o;->b:Lco/uk/getmondo/d/s;

    if-nez v0, :cond_0

    .line 62
    iget-object v0, p0, Lco/uk/getmondo/card/c$o;->a:Lco/uk/getmondo/card/c;

    invoke-static {v0}, Lco/uk/getmondo/card/c;->c(Lco/uk/getmondo/card/c;)Lco/uk/getmondo/api/MonzoApi;

    move-result-object v0

    invoke-virtual {p1}, Lco/uk/getmondo/d/g;->a()Ljava/lang/String;

    move-result-object v1

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    move-object v6, v2

    invoke-interface/range {v0 .. v6}, Lco/uk/getmondo/api/MonzoApi;->orderReplacementCardPrepaid(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lio/reactivex/v;

    move-result-object v0

    .line 61
    :goto_0
    return-object v0

    .line 64
    :cond_0
    iget-object v0, p0, Lco/uk/getmondo/card/c$o;->a:Lco/uk/getmondo/card/c;

    invoke-static {v0}, Lco/uk/getmondo/card/c;->c(Lco/uk/getmondo/card/c;)Lco/uk/getmondo/api/MonzoApi;

    move-result-object v0

    invoke-virtual {p1}, Lco/uk/getmondo/d/g;->a()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lco/uk/getmondo/card/c$o;->b:Lco/uk/getmondo/d/s;

    invoke-virtual {v2}, Lco/uk/getmondo/d/s;->h()[Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lco/uk/getmondo/card/c$o;->b:Lco/uk/getmondo/d/s;

    invoke-virtual {v3}, Lco/uk/getmondo/d/s;->i()Ljava/lang/String;

    move-result-object v3

    .line 65
    iget-object v4, p0, Lco/uk/getmondo/card/c$o;->b:Lco/uk/getmondo/d/s;

    invoke-virtual {v4}, Lco/uk/getmondo/d/s;->b()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lco/uk/getmondo/card/c$o;->b:Lco/uk/getmondo/d/s;

    invoke-virtual {v5}, Lco/uk/getmondo/d/s;->c()Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lco/uk/getmondo/card/c$o;->b:Lco/uk/getmondo/d/s;

    invoke-virtual {v6}, Lco/uk/getmondo/d/s;->j()Ljava/lang/String;

    move-result-object v6

    .line 64
    invoke-interface/range {v0 .. v6}, Lco/uk/getmondo/api/MonzoApi;->orderReplacementCardPrepaid(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lio/reactivex/v;

    move-result-object v0

    goto :goto_0
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 18
    check-cast p1, Lco/uk/getmondo/d/g;

    invoke-virtual {p0, p1}, Lco/uk/getmondo/card/c$o;->a(Lco/uk/getmondo/d/g;)Lio/reactivex/v;

    move-result-object v0

    return-object v0
.end method
