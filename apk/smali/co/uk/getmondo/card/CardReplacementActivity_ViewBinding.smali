.class public Lco/uk/getmondo/card/CardReplacementActivity_ViewBinding;
.super Ljava/lang/Object;
.source "CardReplacementActivity_ViewBinding.java"

# interfaces
.implements Lbutterknife/Unbinder;


# instance fields
.field private a:Lco/uk/getmondo/card/CardReplacementActivity;


# direct methods
.method public constructor <init>(Lco/uk/getmondo/card/CardReplacementActivity;Landroid/view/View;)V
    .locals 3

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    iput-object p1, p0, Lco/uk/getmondo/card/CardReplacementActivity_ViewBinding;->a:Lco/uk/getmondo/card/CardReplacementActivity;

    .line 27
    const v0, 0x7f110123

    const-string v1, "field \'sendReplacementCardButton\'"

    const-class v2, Landroid/widget/Button;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p1, Lco/uk/getmondo/card/CardReplacementActivity;->sendReplacementCardButton:Landroid/widget/Button;

    .line 28
    const v0, 0x7f110129

    const-string v1, "field \'editAddressButton\'"

    const-class v2, Landroid/widget/Button;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p1, Lco/uk/getmondo/card/CardReplacementActivity;->editAddressButton:Landroid/widget/Button;

    .line 29
    const v0, 0x7f110128

    const-string v1, "field \'addressTextView\'"

    const-class v2, Landroid/widget/TextView;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p1, Lco/uk/getmondo/card/CardReplacementActivity;->addressTextView:Landroid/widget/TextView;

    .line 30
    return-void
.end method


# virtual methods
.method public unbind()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 35
    iget-object v0, p0, Lco/uk/getmondo/card/CardReplacementActivity_ViewBinding;->a:Lco/uk/getmondo/card/CardReplacementActivity;

    .line 36
    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Bindings already cleared."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 37
    :cond_0
    iput-object v1, p0, Lco/uk/getmondo/card/CardReplacementActivity_ViewBinding;->a:Lco/uk/getmondo/card/CardReplacementActivity;

    .line 39
    iput-object v1, v0, Lco/uk/getmondo/card/CardReplacementActivity;->sendReplacementCardButton:Landroid/widget/Button;

    .line 40
    iput-object v1, v0, Lco/uk/getmondo/card/CardReplacementActivity;->editAddressButton:Landroid/widget/Button;

    .line 41
    iput-object v1, v0, Lco/uk/getmondo/card/CardReplacementActivity;->addressTextView:Landroid/widget/TextView;

    .line 42
    return-void
.end method
