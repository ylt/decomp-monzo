.class final Lco/uk/getmondo/card/e$f;
.super Ljava/lang/Object;
.source "CardPresenter.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lco/uk/getmondo/card/e;->a(Lco/uk/getmondo/card/e$a;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h",
        "<",
        "Lkotlin/n;",
        "Lio/reactivex/d;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000\u0012\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\n \u0002*\u0004\u0018\u00010\u00010\u00012\u0006\u0010\u0003\u001a\u00020\u0004H\n\u00a2\u0006\u0004\u0008\u0005\u0010\u0006"
    }
    d2 = {
        "<anonymous>",
        "Lio/reactivex/Completable;",
        "kotlin.jvm.PlatformType",
        "it",
        "",
        "apply",
        "(Lkotlin/Unit;)Lio/reactivex/Completable;"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field final synthetic a:Lco/uk/getmondo/card/e;

.field final synthetic b:Lco/uk/getmondo/card/e$a;


# direct methods
.method constructor <init>(Lco/uk/getmondo/card/e;Lco/uk/getmondo/card/e$a;)V
    .locals 0

    iput-object p1, p0, Lco/uk/getmondo/card/e$f;->a:Lco/uk/getmondo/card/e;

    iput-object p2, p0, Lco/uk/getmondo/card/e$f;->b:Lco/uk/getmondo/card/e$a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lkotlin/n;)Lio/reactivex/b;
    .locals 4

    .prologue
    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 91
    iget-object v0, p0, Lco/uk/getmondo/card/e$f;->a:Lco/uk/getmondo/card/e;

    invoke-static {v0}, Lco/uk/getmondo/card/e;->c(Lco/uk/getmondo/card/e;)Lco/uk/getmondo/card/c;

    move-result-object v0

    invoke-virtual {v0}, Lco/uk/getmondo/card/c;->b()Lco/uk/getmondo/d/g;

    move-result-object v2

    .line 92
    if-nez v2, :cond_0

    invoke-static {}, Lkotlin/d/b/l;->a()V

    :cond_0
    invoke-virtual {v2}, Lco/uk/getmondo/d/g;->g()Lco/uk/getmondo/api/model/a;

    move-result-object v0

    sget-object v1, Lco/uk/getmondo/api/model/a;->ACTIVE:Lco/uk/getmondo/api/model/a;

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v0, Lco/uk/getmondo/api/model/a;->INACTIVE:Lco/uk/getmondo/api/model/a;

    .line 93
    :goto_0
    invoke-virtual {v2}, Lco/uk/getmondo/d/g;->g()Lco/uk/getmondo/api/model/a;

    move-result-object v1

    sget-object v3, Lco/uk/getmondo/api/model/a;->ACTIVE:Lco/uk/getmondo/api/model/a;

    invoke-static {v1, v3}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 94
    sget-object v1, Lco/uk/getmondo/api/model/tracking/Impression;->Companion:Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    invoke-virtual {v1}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->t()Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v1

    .line 98
    :goto_1
    iget-object v3, p0, Lco/uk/getmondo/card/e$f;->a:Lco/uk/getmondo/card/e;

    invoke-static {v3}, Lco/uk/getmondo/card/e;->d(Lco/uk/getmondo/card/e;)Lco/uk/getmondo/common/a;

    move-result-object v3

    invoke-virtual {v3, v1}, Lco/uk/getmondo/common/a;->a(Lco/uk/getmondo/api/model/tracking/Impression;)V

    .line 100
    iget-object v1, p0, Lco/uk/getmondo/card/e$f;->a:Lco/uk/getmondo/card/e;

    invoke-static {v1}, Lco/uk/getmondo/card/e;->c(Lco/uk/getmondo/card/e;)Lco/uk/getmondo/card/c;

    move-result-object v1

    invoke-virtual {v2}, Lco/uk/getmondo/d/g;->a()Ljava/lang/String;

    move-result-object v2

    const-string v3, "card.id"

    invoke-static {v2, v3}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v1, v2, v0}, Lco/uk/getmondo/card/c;->a(Ljava/lang/String;Lco/uk/getmondo/api/model/a;)Lio/reactivex/b;

    move-result-object v0

    .line 101
    iget-object v1, p0, Lco/uk/getmondo/card/e$f;->a:Lco/uk/getmondo/card/e;

    invoke-static {v1}, Lco/uk/getmondo/card/e;->e(Lco/uk/getmondo/card/e;)Lio/reactivex/u;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/b;->a(Lio/reactivex/u;)Lio/reactivex/b;

    move-result-object v0

    .line 102
    iget-object v1, p0, Lco/uk/getmondo/card/e$f;->a:Lco/uk/getmondo/card/e;

    invoke-static {v1}, Lco/uk/getmondo/card/e;->f(Lco/uk/getmondo/card/e;)Lio/reactivex/u;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/b;->b(Lio/reactivex/u;)Lio/reactivex/b;

    move-result-object v1

    .line 103
    new-instance v0, Lco/uk/getmondo/card/e$f$1;

    invoke-direct {v0, p0}, Lco/uk/getmondo/card/e$f$1;-><init>(Lco/uk/getmondo/card/e$f;)V

    check-cast v0, Lio/reactivex/c/g;

    invoke-virtual {v1, v0}, Lio/reactivex/b;->c(Lio/reactivex/c/g;)Lio/reactivex/b;

    move-result-object v1

    .line 107
    new-instance v0, Lco/uk/getmondo/card/e$f$2;

    invoke-direct {v0, p0}, Lco/uk/getmondo/card/e$f$2;-><init>(Lco/uk/getmondo/card/e$f;)V

    check-cast v0, Lio/reactivex/c/g;

    invoke-virtual {v1, v0}, Lio/reactivex/b;->b(Lio/reactivex/c/g;)Lio/reactivex/b;

    move-result-object v1

    .line 111
    new-instance v0, Lco/uk/getmondo/card/e$f$3;

    invoke-direct {v0, p0}, Lco/uk/getmondo/card/e$f$3;-><init>(Lco/uk/getmondo/card/e$f;)V

    check-cast v0, Lio/reactivex/c/g;

    invoke-virtual {v1, v0}, Lio/reactivex/b;->a(Lio/reactivex/c/g;)Lio/reactivex/b;

    move-result-object v0

    .line 116
    invoke-virtual {v0}, Lio/reactivex/b;->b()Lio/reactivex/b;

    move-result-object v0

    return-object v0

    .line 92
    :cond_1
    sget-object v0, Lco/uk/getmondo/api/model/a;->ACTIVE:Lco/uk/getmondo/api/model/a;

    goto :goto_0

    .line 96
    :cond_2
    sget-object v1, Lco/uk/getmondo/api/model/tracking/Impression;->Companion:Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    invoke-virtual {v1}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->u()Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v1

    goto :goto_1
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 27
    check-cast p1, Lkotlin/n;

    invoke-virtual {p0, p1}, Lco/uk/getmondo/card/e$f;->a(Lkotlin/n;)Lio/reactivex/b;

    move-result-object v0

    return-object v0
.end method
