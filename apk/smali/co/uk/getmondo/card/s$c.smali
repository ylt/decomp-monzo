.class final Lco/uk/getmondo/card/s$c;
.super Ljava/lang/Object;
.source "CardStorage.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lco/uk/getmondo/card/s;->b(Ljava/lang/String;)Lio/reactivex/n;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h",
        "<TT;TR;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u0004\u0018\u00010\u00012\u0014\u0010\u0002\u001a\u0010\u0012\u000c\u0012\n \u0004*\u0004\u0018\u00010\u00010\u00010\u0003H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "Lco/uk/getmondo/model/Card;",
        "it",
        "Lco/uk/getmondo/common/rx/RxRealm$Result;",
        "kotlin.jvm.PlatformType",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# static fields
.field public static final a:Lco/uk/getmondo/card/s$c;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lco/uk/getmondo/card/s$c;

    invoke-direct {v0}, Lco/uk/getmondo/card/s$c;-><init>()V

    sput-object v0, Lco/uk/getmondo/card/s$c;->a:Lco/uk/getmondo/card/s$c;

    return-void
.end method

.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lco/uk/getmondo/common/j/g$a;)Lco/uk/getmondo/d/g;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lco/uk/getmondo/common/j/g$a",
            "<+",
            "Lco/uk/getmondo/d/g;",
            ">;)",
            "Lco/uk/getmondo/d/g;"
        }
    .end annotation

    .prologue
    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 41
    invoke-virtual {p1}, Lco/uk/getmondo/common/j/g$a;->d()Lio/realm/bb;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/d/g;

    return-object v0
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 9
    check-cast p1, Lco/uk/getmondo/common/j/g$a;

    invoke-virtual {p0, p1}, Lco/uk/getmondo/card/s$c;->a(Lco/uk/getmondo/common/j/g$a;)Lco/uk/getmondo/d/g;

    move-result-object v0

    return-object v0
.end method
