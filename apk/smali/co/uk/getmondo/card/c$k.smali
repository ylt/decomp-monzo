.class final Lco/uk/getmondo/card/c$k;
.super Ljava/lang/Object;
.source "CardManager.kt"

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lco/uk/getmondo/card/c;->a(Lco/uk/getmondo/d/s;)Lio/reactivex/v;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Lio/reactivex/z",
        "<+TT;>;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000\u0012\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a^\u0012(\u0012&\u0012\u000c\u0012\n \u0004*\u0004\u0018\u00010\u00030\u0003 \u0004*\u0012\u0012\u000c\u0012\n \u0004*\u0004\u0018\u00010\u00030\u0003\u0018\u00010\u00020\u0002 \u0004*.\u0012(\u0012&\u0012\u000c\u0012\n \u0004*\u0004\u0018\u00010\u00030\u0003 \u0004*\u0012\u0012\u000c\u0012\n \u0004*\u0004\u0018\u00010\u00030\u0003\u0018\u00010\u00020\u0002\u0018\u00010\u00010\u0001H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "Lio/reactivex/Single;",
        "Lcom/memoizrlabs/poweroptional/Optional;",
        "Ljava/util/Date;",
        "kotlin.jvm.PlatformType",
        "call"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field final synthetic a:Lco/uk/getmondo/card/c;

.field final synthetic b:Lco/uk/getmondo/d/s;


# direct methods
.method constructor <init>(Lco/uk/getmondo/card/c;Lco/uk/getmondo/d/s;)V
    .locals 0

    iput-object p1, p0, Lco/uk/getmondo/card/c$k;->a:Lco/uk/getmondo/card/c;

    iput-object p2, p0, Lco/uk/getmondo/card/c$k;->b:Lco/uk/getmondo/d/s;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Lio/reactivex/v;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/v",
            "<",
            "Lcom/c/b/b",
            "<",
            "Ljava/util/Date;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 27
    iget-object v0, p0, Lco/uk/getmondo/card/c$k;->a:Lco/uk/getmondo/card/c;

    invoke-static {v0}, Lco/uk/getmondo/card/c;->a(Lco/uk/getmondo/card/c;)Lco/uk/getmondo/common/accounts/b;

    move-result-object v0

    invoke-virtual {v0}, Lco/uk/getmondo/common/accounts/b;->a()Lco/uk/getmondo/d/a;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-static {}, Lkotlin/d/b/l;->a()V

    .line 28
    :cond_0
    invoke-interface {v0}, Lco/uk/getmondo/d/a;->e()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 29
    iget-object v1, p0, Lco/uk/getmondo/card/c$k;->a:Lco/uk/getmondo/card/c;

    invoke-interface {v0}, Lco/uk/getmondo/d/a;->a()Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lco/uk/getmondo/card/c$k;->b:Lco/uk/getmondo/d/s;

    invoke-static {v1, v0, v2}, Lco/uk/getmondo/card/c;->a(Lco/uk/getmondo/card/c;Ljava/lang/String;Lco/uk/getmondo/d/s;)Lio/reactivex/b;

    move-result-object v0

    invoke-static {}, Lcom/c/b/b;->c()Lcom/c/b/b;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/b;->a(Ljava/lang/Object;)Lio/reactivex/v;

    move-result-object v0

    .line 28
    :goto_0
    return-object v0

    .line 31
    :cond_1
    iget-object v1, p0, Lco/uk/getmondo/card/c$k;->a:Lco/uk/getmondo/card/c;

    invoke-interface {v0}, Lco/uk/getmondo/d/a;->a()Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lco/uk/getmondo/card/c$k;->b:Lco/uk/getmondo/d/s;

    invoke-static {v1, v0, v2}, Lco/uk/getmondo/card/c;->b(Lco/uk/getmondo/card/c;Ljava/lang/String;Lco/uk/getmondo/d/s;)Lio/reactivex/v;

    move-result-object v1

    sget-object v0, Lco/uk/getmondo/card/c$k$1;->a:Lco/uk/getmondo/card/c$k$1;

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {v1, v0}, Lio/reactivex/v;->d(Lio/reactivex/c/h;)Lio/reactivex/v;

    move-result-object v0

    goto :goto_0
.end method

.method public synthetic call()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 18
    invoke-virtual {p0}, Lco/uk/getmondo/card/c$k;->a()Lio/reactivex/v;

    move-result-object v0

    return-object v0
.end method
