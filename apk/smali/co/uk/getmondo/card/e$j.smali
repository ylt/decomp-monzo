.class final Lco/uk/getmondo/card/e$j;
.super Ljava/lang/Object;
.source "CardPresenter.kt"

# interfaces
.implements Lio/reactivex/c/g;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lco/uk/getmondo/card/e;->a(Lco/uk/getmondo/card/e$a;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/g",
        "<",
        "Lkotlin/n;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000\n\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0004\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0003*\u0004\u0018\u00010\u00010\u0001H\n\u00a2\u0006\u0004\u0008\u0004\u0010\u0005"
    }
    d2 = {
        "<anonymous>",
        "",
        "it",
        "kotlin.jvm.PlatformType",
        "accept",
        "(Lkotlin/Unit;)V"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field final synthetic a:Lco/uk/getmondo/card/e;

.field final synthetic b:Z

.field final synthetic c:Lco/uk/getmondo/card/e$a;


# direct methods
.method constructor <init>(Lco/uk/getmondo/card/e;ZLco/uk/getmondo/card/e$a;)V
    .locals 0

    iput-object p1, p0, Lco/uk/getmondo/card/e$j;->a:Lco/uk/getmondo/card/e;

    iput-boolean p2, p0, Lco/uk/getmondo/card/e$j;->b:Z

    iput-object p3, p0, Lco/uk/getmondo/card/e$j;->c:Lco/uk/getmondo/card/e$a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 27
    check-cast p1, Lkotlin/n;

    invoke-virtual {p0, p1}, Lco/uk/getmondo/card/e$j;->a(Lkotlin/n;)V

    return-void
.end method

.method public final a(Lkotlin/n;)V
    .locals 3

    .prologue
    .line 129
    iget-object v0, p0, Lco/uk/getmondo/card/e$j;->a:Lco/uk/getmondo/card/e;

    invoke-static {v0}, Lco/uk/getmondo/card/e;->d(Lco/uk/getmondo/card/e;)Lco/uk/getmondo/common/a;

    move-result-object v0

    sget-object v1, Lco/uk/getmondo/api/model/tracking/Impression;->Companion:Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    sget-object v2, Lco/uk/getmondo/api/model/tracking/Impression$PinFrom;->CARD:Lco/uk/getmondo/api/model/tracking/Impression$PinFrom;

    invoke-virtual {v1, v2}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->a(Lco/uk/getmondo/api/model/tracking/Impression$PinFrom;)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v1

    invoke-virtual {v0, v1}, Lco/uk/getmondo/common/a;->a(Lco/uk/getmondo/api/model/tracking/Impression;)V

    .line 130
    iget-boolean v0, p0, Lco/uk/getmondo/card/e$j;->b:Z

    if-eqz v0, :cond_0

    .line 131
    iget-object v0, p0, Lco/uk/getmondo/card/e$j;->c:Lco/uk/getmondo/card/e$a;

    invoke-interface {v0}, Lco/uk/getmondo/card/e$a;->b_()V

    .line 134
    :goto_0
    return-void

    .line 133
    :cond_0
    iget-object v0, p0, Lco/uk/getmondo/card/e$j;->c:Lco/uk/getmondo/card/e$a;

    invoke-interface {v0}, Lco/uk/getmondo/card/e$a;->t()V

    goto :goto_0
.end method
