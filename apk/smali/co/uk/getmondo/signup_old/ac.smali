.class public final Lco/uk/getmondo/signup_old/ac;
.super Ljava/lang/Object;
.source "SignupAdapter.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J \u0010\u0003\u001a\u0004\u0018\u00010\u00042\u0006\u0010\u0005\u001a\u00020\u00062\u000c\u0010\u0007\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0008H\u0007\u00a8\u0006\t"
    }
    d2 = {
        "Lco/uk/getmondo/signup_old/SignupAdapter;",
        "",
        "()V",
        "fromJson",
        "Lco/uk/getmondo/api/model/signup/SignupInfo$Stage;",
        "jsonReader",
        "Lcom/squareup/moshi/JsonReader;",
        "delegate",
        "Lcom/squareup/moshi/JsonAdapter;",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final fromJson(Lcom/squareup/moshi/k;Lcom/squareup/moshi/i;)Lco/uk/getmondo/api/model/signup/SignupInfo$Stage;
    .locals 5
    .annotation runtime Lcom/squareup/moshi/g;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/moshi/k;",
            "Lcom/squareup/moshi/i",
            "<",
            "Lco/uk/getmondo/api/model/signup/SignupInfo$Stage;",
            ">;)",
            "Lco/uk/getmondo/api/model/signup/SignupInfo$Stage;"
        }
    .end annotation

    .prologue
    const-string v0, "jsonReader"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "delegate"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 11
    invoke-virtual {p1}, Lcom/squareup/moshi/k;->j()Ljava/lang/String;

    move-result-object v0

    .line 12
    const-string v1, "pending"

    const/4 v2, 0x0

    const/4 v3, 0x2

    const/4 v4, 0x0

    invoke-static {v0, v1, v2, v3, v4}, Lkotlin/h/j;->a(Ljava/lang/String;Ljava/lang/String;ZILjava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    sget-object v0, Lco/uk/getmondo/api/model/signup/SignupInfo$Stage;->PENDING:Lco/uk/getmondo/api/model/signup/SignupInfo$Stage;

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p2, v0}, Lcom/squareup/moshi/i;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/api/model/signup/SignupInfo$Stage;

    goto :goto_0
.end method
