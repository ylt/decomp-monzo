.class public Lco/uk/getmondo/signup_old/SignUpActivity_ViewBinding;
.super Ljava/lang/Object;
.source "SignUpActivity_ViewBinding.java"

# interfaces
.implements Lbutterknife/Unbinder;


# instance fields
.field private a:Lco/uk/getmondo/signup_old/SignUpActivity;

.field private b:Landroid/view/View;

.field private c:Landroid/text/TextWatcher;


# direct methods
.method public constructor <init>(Lco/uk/getmondo/signup_old/SignUpActivity;Landroid/view/View;)V
    .locals 4

    .prologue
    const v3, 0x7f11021d

    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    iput-object p1, p0, Lco/uk/getmondo/signup_old/SignUpActivity_ViewBinding;->a:Lco/uk/getmondo/signup_old/SignUpActivity;

    .line 37
    const v0, 0x7f11021c

    const-string v1, "field \'emailWrapper\'"

    const-class v2, Landroid/support/design/widget/TextInputLayout;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/design/widget/TextInputLayout;

    iput-object v0, p1, Lco/uk/getmondo/signup_old/SignUpActivity;->emailWrapper:Landroid/support/design/widget/TextInputLayout;

    .line 38
    const-string v0, "field \'emailEditText\' and method \'onTextChanged\'"

    invoke-static {p2, v3, v0}, Lbutterknife/internal/Utils;->findRequiredView(Landroid/view/View;ILjava/lang/String;)Landroid/view/View;

    move-result-object v1

    .line 39
    const-string v0, "field \'emailEditText\'"

    const-class v2, Landroid/widget/EditText;

    invoke-static {v1, v3, v0, v2}, Lbutterknife/internal/Utils;->castView(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p1, Lco/uk/getmondo/signup_old/SignUpActivity;->emailEditText:Landroid/widget/EditText;

    .line 40
    iput-object v1, p0, Lco/uk/getmondo/signup_old/SignUpActivity_ViewBinding;->b:Landroid/view/View;

    .line 41
    new-instance v0, Lco/uk/getmondo/signup_old/SignUpActivity_ViewBinding$1;

    invoke-direct {v0, p0, p1}, Lco/uk/getmondo/signup_old/SignUpActivity_ViewBinding$1;-><init>(Lco/uk/getmondo/signup_old/SignUpActivity_ViewBinding;Lco/uk/getmondo/signup_old/SignUpActivity;)V

    iput-object v0, p0, Lco/uk/getmondo/signup_old/SignUpActivity_ViewBinding;->c:Landroid/text/TextWatcher;

    move-object v0, v1

    .line 55
    check-cast v0, Landroid/widget/TextView;

    iget-object v1, p0, Lco/uk/getmondo/signup_old/SignUpActivity_ViewBinding;->c:Landroid/text/TextWatcher;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 56
    const v0, 0x7f11021e

    const-string v1, "field \'continueButton\'"

    const-class v2, Landroid/widget/Button;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p1, Lco/uk/getmondo/signup_old/SignUpActivity;->continueButton:Landroid/widget/Button;

    .line 57
    return-void
.end method


# virtual methods
.method public unbind()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 62
    iget-object v0, p0, Lco/uk/getmondo/signup_old/SignUpActivity_ViewBinding;->a:Lco/uk/getmondo/signup_old/SignUpActivity;

    .line 63
    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Bindings already cleared."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 64
    :cond_0
    iput-object v2, p0, Lco/uk/getmondo/signup_old/SignUpActivity_ViewBinding;->a:Lco/uk/getmondo/signup_old/SignUpActivity;

    .line 66
    iput-object v2, v0, Lco/uk/getmondo/signup_old/SignUpActivity;->emailWrapper:Landroid/support/design/widget/TextInputLayout;

    .line 67
    iput-object v2, v0, Lco/uk/getmondo/signup_old/SignUpActivity;->emailEditText:Landroid/widget/EditText;

    .line 68
    iput-object v2, v0, Lco/uk/getmondo/signup_old/SignUpActivity;->continueButton:Landroid/widget/Button;

    .line 70
    iget-object v0, p0, Lco/uk/getmondo/signup_old/SignUpActivity_ViewBinding;->b:Landroid/view/View;

    check-cast v0, Landroid/widget/TextView;

    iget-object v1, p0, Lco/uk/getmondo/signup_old/SignUpActivity_ViewBinding;->c:Landroid/text/TextWatcher;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->removeTextChangedListener(Landroid/text/TextWatcher;)V

    .line 71
    iput-object v2, p0, Lco/uk/getmondo/signup_old/SignUpActivity_ViewBinding;->c:Landroid/text/TextWatcher;

    .line 72
    iput-object v2, p0, Lco/uk/getmondo/signup_old/SignUpActivity_ViewBinding;->b:Landroid/view/View;

    .line 73
    return-void
.end method
