.class public Lco/uk/getmondo/signup_old/CreateProfileActivity;
.super Lco/uk/getmondo/common/activities/b;
.source "CreateProfileActivity.java"

# interfaces
.implements Lco/uk/getmondo/signup_old/f$a;


# instance fields
.field a:Lco/uk/getmondo/signup_old/f;

.field private final b:Landroid/os/Handler;

.field private final c:Ljava/lang/Runnable;

.field countryEditText:Landroid/widget/EditText;
    .annotation build Lbutterknife/BindView;
        value = 0x7f110148
    .end annotation
.end field

.field countryWrapper:Landroid/support/design/widget/TextInputLayout;
    .annotation build Lbutterknife/BindView;
        value = 0x7f110147
    .end annotation
.end field

.field createAccountButton:Landroid/widget/Button;
    .annotation build Lbutterknife/BindView;
        value = 0x7f110149
    .end annotation
.end field

.field dateWrapper:Landroid/support/design/widget/TextInputLayout;
    .annotation build Lbutterknife/BindView;
        value = 0x7f110143
    .end annotation
.end field

.field nameWrapper:Landroid/support/design/widget/TextInputLayout;
    .annotation build Lbutterknife/BindView;
        value = 0x7f110141
    .end annotation
.end field

.field postcodeWrapper:Landroid/support/design/widget/TextInputLayout;
    .annotation build Lbutterknife/BindView;
        value = 0x7f110145
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 31
    invoke-direct {p0}, Lco/uk/getmondo/common/activities/b;-><init>()V

    .line 34
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lco/uk/getmondo/signup_old/CreateProfileActivity;->b:Landroid/os/Handler;

    .line 35
    invoke-static {p0}, Lco/uk/getmondo/signup_old/c;->a(Lco/uk/getmondo/signup_old/CreateProfileActivity;)Ljava/lang/Runnable;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/signup_old/CreateProfileActivity;->c:Ljava/lang/Runnable;

    return-void
.end method

.method static synthetic a(Lco/uk/getmondo/signup_old/CreateProfileActivity;Ljava/lang/Object;)Lco/uk/getmondo/signup_old/a/a/a;
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 69
    iget-object v0, p0, Lco/uk/getmondo/signup_old/CreateProfileActivity;->nameWrapper:Landroid/support/design/widget/TextInputLayout;

    invoke-direct {p0, v0}, Lco/uk/getmondo/signup_old/CreateProfileActivity;->a(Landroid/support/design/widget/TextInputLayout;)Ljava/lang/String;

    move-result-object v6

    .line 70
    iget-object v0, p0, Lco/uk/getmondo/signup_old/CreateProfileActivity;->dateWrapper:Landroid/support/design/widget/TextInputLayout;

    invoke-direct {p0, v0}, Lco/uk/getmondo/signup_old/CreateProfileActivity;->a(Landroid/support/design/widget/TextInputLayout;)Ljava/lang/String;

    move-result-object v7

    .line 71
    iget-object v0, p0, Lco/uk/getmondo/signup_old/CreateProfileActivity;->postcodeWrapper:Landroid/support/design/widget/TextInputLayout;

    invoke-direct {p0, v0}, Lco/uk/getmondo/signup_old/CreateProfileActivity;->a(Landroid/support/design/widget/TextInputLayout;)Ljava/lang/String;

    move-result-object v1

    .line 72
    new-instance v0, Lco/uk/getmondo/d/s;

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    invoke-direct/range {v0 .. v5}, Lco/uk/getmondo/d/s;-><init>(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 73
    new-instance v1, Lco/uk/getmondo/signup_old/a/a/a;

    invoke-direct {v1, v6, v7, v0}, Lco/uk/getmondo/signup_old/a/a/a;-><init>(Ljava/lang/String;Ljava/lang/String;Lco/uk/getmondo/d/s;)V

    return-object v1
.end method

.method private a(Landroid/support/design/widget/TextInputLayout;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 78
    invoke-virtual {p1}, Landroid/support/design/widget/TextInputLayout;->getEditText()Landroid/widget/EditText;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 79
    invoke-virtual {p1}, Landroid/support/design/widget/TextInputLayout;->getEditText()Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 81
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;)V
    .locals 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 48
    invoke-static {p0}, Lco/uk/getmondo/signup_old/CreateProfileActivity;->b(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    .line 49
    const v1, 0x10018000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 50
    invoke-virtual {p0, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 51
    return-void
.end method

.method private a(Landroid/support/design/widget/TextInputLayout;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 85
    invoke-virtual {p1}, Landroid/support/design/widget/TextInputLayout;->getEditText()Landroid/widget/EditText;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 86
    invoke-virtual {p1}, Landroid/support/design/widget/TextInputLayout;->getEditText()Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0, p2}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 88
    :cond_0
    return-void
.end method

.method public static b(Landroid/content/Context;)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 54
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lco/uk/getmondo/signup_old/CreateProfileActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    return-object v0
.end method

.method private k()V
    .locals 3

    .prologue
    .line 148
    new-instance v0, Lco/uk/getmondo/signup_old/r;

    invoke-direct {v0}, Lco/uk/getmondo/signup_old/r;-><init>()V

    .line 149
    iget-object v1, p0, Lco/uk/getmondo/signup_old/CreateProfileActivity;->dateWrapper:Landroid/support/design/widget/TextInputLayout;

    invoke-virtual {v1}, Landroid/support/design/widget/TextInputLayout;->getEditText()Landroid/widget/EditText;

    move-result-object v1

    .line 150
    if-eqz v1, :cond_0

    .line 151
    new-instance v2, Lco/uk/getmondo/signup_old/CreateProfileActivity$1;

    invoke-direct {v2, p0, v0}, Lco/uk/getmondo/signup_old/CreateProfileActivity$1;-><init>(Lco/uk/getmondo/signup_old/CreateProfileActivity;Lco/uk/getmondo/signup_old/r;)V

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 167
    :cond_0
    return-void
.end method


# virtual methods
.method public a()Lio/reactivex/n;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Lco/uk/getmondo/signup_old/a/a/a;",
            ">;"
        }
    .end annotation

    .prologue
    .line 67
    iget-object v0, p0, Lco/uk/getmondo/signup_old/CreateProfileActivity;->createAccountButton:Landroid/widget/Button;

    invoke-static {v0}, Lcom/b/a/c/c;->a(Landroid/view/View;)Lio/reactivex/n;

    move-result-object v0

    invoke-static {p0}, Lco/uk/getmondo/signup_old/d;->a(Lco/uk/getmondo/signup_old/CreateProfileActivity;)Lio/reactivex/c/h;

    move-result-object v1

    .line 68
    invoke-virtual {v0, v1}, Lio/reactivex/n;->map(Lio/reactivex/c/h;)Lio/reactivex/n;

    move-result-object v0

    .line 67
    return-object v0
.end method

.method public a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 107
    iget-object v0, p0, Lco/uk/getmondo/signup_old/CreateProfileActivity;->nameWrapper:Landroid/support/design/widget/TextInputLayout;

    invoke-direct {p0, v0, p1}, Lco/uk/getmondo/signup_old/CreateProfileActivity;->a(Landroid/support/design/widget/TextInputLayout;Ljava/lang/String;)V

    .line 108
    return-void
.end method

.method public b()Lio/reactivex/n;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 102
    iget-object v0, p0, Lco/uk/getmondo/signup_old/CreateProfileActivity;->countryEditText:Landroid/widget/EditText;

    invoke-static {v0}, Lcom/b/a/c/c;->a(Landroid/view/View;)Lio/reactivex/n;

    move-result-object v0

    return-object v0
.end method

.method public c()V
    .locals 1

    .prologue
    .line 177
    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup_old/CreateProfileActivity;->setResult(I)V

    .line 178
    invoke-virtual {p0}, Lco/uk/getmondo/signup_old/CreateProfileActivity;->finish()V

    .line 179
    return-void
.end method

.method public d()V
    .locals 2

    .prologue
    .line 183
    iget-object v0, p0, Lco/uk/getmondo/signup_old/CreateProfileActivity;->nameWrapper:Landroid/support/design/widget/TextInputLayout;

    const v1, 0x7f0a017d

    invoke-virtual {p0, v1}, Lco/uk/getmondo/signup_old/CreateProfileActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/design/widget/TextInputLayout;->setError(Ljava/lang/CharSequence;)V

    .line 184
    return-void
.end method

.method public d(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 112
    iget-object v0, p0, Lco/uk/getmondo/signup_old/CreateProfileActivity;->dateWrapper:Landroid/support/design/widget/TextInputLayout;

    invoke-direct {p0, v0, p1}, Lco/uk/getmondo/signup_old/CreateProfileActivity;->a(Landroid/support/design/widget/TextInputLayout;Ljava/lang/String;)V

    .line 113
    return-void
.end method

.method public e()V
    .locals 2

    .prologue
    .line 188
    iget-object v0, p0, Lco/uk/getmondo/signup_old/CreateProfileActivity;->dateWrapper:Landroid/support/design/widget/TextInputLayout;

    const v1, 0x7f0a017b

    invoke-virtual {p0, v1}, Lco/uk/getmondo/signup_old/CreateProfileActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/design/widget/TextInputLayout;->setError(Ljava/lang/CharSequence;)V

    .line 189
    return-void
.end method

.method public e(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 117
    iget-object v0, p0, Lco/uk/getmondo/signup_old/CreateProfileActivity;->postcodeWrapper:Landroid/support/design/widget/TextInputLayout;

    invoke-direct {p0, v0, p1}, Lco/uk/getmondo/signup_old/CreateProfileActivity;->a(Landroid/support/design/widget/TextInputLayout;Ljava/lang/String;)V

    .line 118
    return-void
.end method

.method public f()V
    .locals 2

    .prologue
    .line 193
    iget-object v0, p0, Lco/uk/getmondo/signup_old/CreateProfileActivity;->postcodeWrapper:Landroid/support/design/widget/TextInputLayout;

    const v1, 0x7f0a017e

    invoke-virtual {p0, v1}, Lco/uk/getmondo/signup_old/CreateProfileActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/design/widget/TextInputLayout;->setError(Ljava/lang/CharSequence;)V

    .line 194
    return-void
.end method

.method public f(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 122
    iget-object v0, p0, Lco/uk/getmondo/signup_old/CreateProfileActivity;->countryEditText:Landroid/widget/EditText;

    invoke-virtual {v0, p1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 123
    iget-object v0, p0, Lco/uk/getmondo/signup_old/CreateProfileActivity;->countryEditText:Landroid/widget/EditText;

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setSelection(I)V

    .line 124
    return-void
.end method

.method public g()V
    .locals 2

    .prologue
    .line 198
    iget-object v0, p0, Lco/uk/getmondo/signup_old/CreateProfileActivity;->dateWrapper:Landroid/support/design/widget/TextInputLayout;

    const v1, 0x7f0a0457

    invoke-virtual {p0, v1}, Lco/uk/getmondo/signup_old/CreateProfileActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/design/widget/TextInputLayout;->setError(Ljava/lang/CharSequence;)V

    .line 199
    return-void
.end method

.method public h()V
    .locals 4

    .prologue
    .line 203
    iget-object v0, p0, Lco/uk/getmondo/signup_old/CreateProfileActivity;->b:Landroid/os/Handler;

    iget-object v1, p0, Lco/uk/getmondo/signup_old/CreateProfileActivity;->c:Ljava/lang/Runnable;

    const-wide/16 v2, 0x12c

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 204
    return-void
.end method

.method public i()V
    .locals 2

    .prologue
    .line 208
    iget-object v0, p0, Lco/uk/getmondo/signup_old/CreateProfileActivity;->b:Landroid/os/Handler;

    iget-object v1, p0, Lco/uk/getmondo/signup_old/CreateProfileActivity;->c:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 209
    invoke-virtual {p0}, Lco/uk/getmondo/signup_old/CreateProfileActivity;->t()V

    .line 210
    return-void
.end method

.method public j()V
    .locals 4

    .prologue
    .line 214
    const v0, 0x7f0a0428

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup_old/CreateProfileActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    const v1, 0x7f0a0429

    invoke-virtual {p0, v1}, Lco/uk/getmondo/signup_old/CreateProfileActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    const v2, 0x7f0a01a9

    const/4 v3, 0x0

    invoke-static {v0, v1, v2, v3}, Lco/uk/getmondo/common/d/a;->a(Ljava/lang/String;Ljava/lang/String;IZ)Lco/uk/getmondo/common/d/a;

    move-result-object v0

    .line 215
    invoke-virtual {p0}, Lco/uk/getmondo/signup_old/CreateProfileActivity;->getSupportFragmentManager()Landroid/support/v4/app/n;

    move-result-object v1

    const-string v2, "TAG_UNAVAILABLE_OUTSIDE_UK"

    invoke-virtual {v0, v1, v2}, Lco/uk/getmondo/common/d/a;->show(Landroid/support/v4/app/n;Ljava/lang/String;)V

    .line 216
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 134
    invoke-super {p0, p1}, Lco/uk/getmondo/common/activities/b;->onCreate(Landroid/os/Bundle;)V

    .line 135
    const v0, 0x7f05002e

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup_old/CreateProfileActivity;->setContentView(I)V

    .line 136
    invoke-virtual {p0}, Lco/uk/getmondo/signup_old/CreateProfileActivity;->l()Lco/uk/getmondo/common/h/b/b;

    move-result-object v0

    invoke-interface {v0, p0}, Lco/uk/getmondo/common/h/b/b;->a(Lco/uk/getmondo/signup_old/CreateProfileActivity;)V

    .line 137
    invoke-static {p0}, Lbutterknife/ButterKnife;->bind(Landroid/app/Activity;)Lbutterknife/Unbinder;

    .line 139
    iget-object v0, p0, Lco/uk/getmondo/signup_old/CreateProfileActivity;->a:Lco/uk/getmondo/signup_old/f;

    invoke-virtual {v0, p0}, Lco/uk/getmondo/signup_old/f;->a(Lco/uk/getmondo/signup_old/f$a;)V

    .line 140
    invoke-direct {p0}, Lco/uk/getmondo/signup_old/CreateProfileActivity;->k()V

    .line 141
    sget-object v0, Lco/uk/getmondo/a;->c:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 143
    iget-object v0, p0, Lco/uk/getmondo/signup_old/CreateProfileActivity;->countryEditText:Landroid/widget/EditText;

    const/high16 v1, 0x3f000000    # 0.5f

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setAlpha(F)V

    .line 145
    :cond_0
    return-void
.end method

.method onDateAction()Z
    .locals 1
    .annotation build Lbutterknife/OnEditorAction;
        value = {
            0x7f110144
        }
    .end annotation

    .prologue
    .line 128
    iget-object v0, p0, Lco/uk/getmondo/signup_old/CreateProfileActivity;->postcodeWrapper:Landroid/support/design/widget/TextInputLayout;

    invoke-virtual {v0}, Landroid/support/design/widget/TextInputLayout;->requestFocus()Z

    .line 129
    const/4 v0, 0x1

    return v0
.end method

.method onDateFocusChange(Z)V
    .locals 2
    .annotation build Lbutterknife/OnFocusChange;
        value = {
            0x7f110144
        }
    .end annotation

    .prologue
    .line 59
    iget-object v0, p0, Lco/uk/getmondo/signup_old/CreateProfileActivity;->dateWrapper:Landroid/support/design/widget/TextInputLayout;

    invoke-direct {p0, v0}, Lco/uk/getmondo/signup_old/CreateProfileActivity;->a(Landroid/support/design/widget/TextInputLayout;)Ljava/lang/String;

    move-result-object v0

    .line 60
    if-eqz p1, :cond_0

    iget-object v1, p0, Lco/uk/getmondo/signup_old/CreateProfileActivity;->dateWrapper:Landroid/support/design/widget/TextInputLayout;

    invoke-virtual {v1}, Landroid/support/design/widget/TextInputLayout;->getEditText()Landroid/widget/EditText;

    move-result-object v1

    if-eqz v1, :cond_0

    if-eqz v0, :cond_0

    .line 61
    iget-object v1, p0, Lco/uk/getmondo/signup_old/CreateProfileActivity;->dateWrapper:Landroid/support/design/widget/TextInputLayout;

    invoke-virtual {v1}, Landroid/support/design/widget/TextInputLayout;->getEditText()Landroid/widget/EditText;

    move-result-object v1

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    invoke-virtual {v1, v0}, Landroid/widget/EditText;->setSelection(I)V

    .line 63
    :cond_0
    return-void
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 171
    iget-object v0, p0, Lco/uk/getmondo/signup_old/CreateProfileActivity;->a:Lco/uk/getmondo/signup_old/f;

    invoke-virtual {v0}, Lco/uk/getmondo/signup_old/f;->b()V

    .line 172
    invoke-super {p0}, Lco/uk/getmondo/common/activities/b;->onDestroy()V

    .line 173
    return-void
.end method

.method onNameChanged()V
    .locals 2
    .annotation build Lbutterknife/OnTextChanged;
        value = {
            0x7f110142
        }
    .end annotation

    .prologue
    .line 92
    iget-object v0, p0, Lco/uk/getmondo/signup_old/CreateProfileActivity;->nameWrapper:Landroid/support/design/widget/TextInputLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/design/widget/TextInputLayout;->setError(Ljava/lang/CharSequence;)V

    .line 93
    return-void
.end method

.method onPostcodeChanged()V
    .locals 2
    .annotation build Lbutterknife/OnTextChanged;
        value = {
            0x7f110146
        }
    .end annotation

    .prologue
    .line 97
    iget-object v0, p0, Lco/uk/getmondo/signup_old/CreateProfileActivity;->postcodeWrapper:Landroid/support/design/widget/TextInputLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/design/widget/TextInputLayout;->setError(Ljava/lang/CharSequence;)V

    .line 98
    return-void
.end method
