.class public Lco/uk/getmondo/signup_old/CountryDialogFragment;
.super Landroid/app/DialogFragment;
.source "CountryDialogFragment.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lco/uk/getmondo/signup_old/CountryDialogFragment$b;,
        Lco/uk/getmondo/signup_old/CountryDialogFragment$a;
    }
.end annotation


# instance fields
.field private a:Lbutterknife/Unbinder;

.field private b:Lco/uk/getmondo/signup_old/CountryDialogFragment$a;

.field private c:Lco/uk/getmondo/signup_old/CountryDialogFragment$b;

.field recyclerView:Landroid/support/v7/widget/RecyclerView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f11013e
    .end annotation
.end field


# direct methods
.method static synthetic a(Lco/uk/getmondo/signup_old/CountryDialogFragment;Lco/uk/getmondo/d/i;)Lkotlin/n;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lco/uk/getmondo/signup_old/CountryDialogFragment;->b:Lco/uk/getmondo/signup_old/CountryDialogFragment$a;

    invoke-interface {v0, p1}, Lco/uk/getmondo/signup_old/CountryDialogFragment$a;->a(Lco/uk/getmondo/d/i;)V

    .line 46
    invoke-virtual {p0}, Lco/uk/getmondo/signup_old/CountryDialogFragment;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    .line 47
    sget-object v0, Lkotlin/n;->a:Lkotlin/n;

    return-object v0
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 34
    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onCreate(Landroid/os/Bundle;)V

    .line 35
    const/4 v0, 0x0

    const v1, 0x7f0c010e

    invoke-virtual {p0, v0, v1}, Lco/uk/getmondo/signup_old/CountryDialogFragment;->setStyle(II)V

    .line 36
    return-void
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 5
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "InflateParams"
        }
    .end annotation

    .prologue
    .line 41
    invoke-virtual {p0}, Lco/uk/getmondo/signup_old/CountryDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f050094

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 42
    invoke-static {p0, v0}, Lbutterknife/ButterKnife;->bind(Ljava/lang/Object;Landroid/view/View;)Lbutterknife/Unbinder;

    move-result-object v1

    iput-object v1, p0, Lco/uk/getmondo/signup_old/CountryDialogFragment;->a:Lbutterknife/Unbinder;

    .line 44
    new-instance v1, Lco/uk/getmondo/signup_old/a;

    invoke-static {}, Lco/uk/getmondo/d/i;->j()Ljava/util/List;

    move-result-object v2

    invoke-static {p0}, Lco/uk/getmondo/signup_old/b;->a(Lco/uk/getmondo/signup_old/CountryDialogFragment;)Lkotlin/d/a/b;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lco/uk/getmondo/signup_old/a;-><init>(Ljava/util/List;Lkotlin/d/a/b;)V

    .line 49
    iget-object v2, p0, Lco/uk/getmondo/signup_old/CountryDialogFragment;->recyclerView:Landroid/support/v7/widget/RecyclerView;

    new-instance v3, Landroid/support/v7/widget/LinearLayoutManager;

    invoke-virtual {p0}, Lco/uk/getmondo/signup_old/CountryDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v4

    invoke-direct {v3, v4}, Landroid/support/v7/widget/LinearLayoutManager;-><init>(Landroid/content/Context;)V

    invoke-virtual {v2, v3}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(Landroid/support/v7/widget/RecyclerView$h;)V

    .line 50
    iget-object v2, p0, Lco/uk/getmondo/signup_old/CountryDialogFragment;->recyclerView:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v2, v1}, Landroid/support/v7/widget/RecyclerView;->setAdapter(Landroid/support/v7/widget/RecyclerView$a;)V

    .line 51
    iget-object v1, p0, Lco/uk/getmondo/signup_old/CountryDialogFragment;->recyclerView:Landroid/support/v7/widget/RecyclerView;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/support/v7/widget/RecyclerView;->setHasFixedSize(Z)V

    .line 52
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lco/uk/getmondo/signup_old/CountryDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 53
    invoke-virtual {v1, v0}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 54
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 52
    return-object v0
.end method

.method public onDestroyView()V
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Lco/uk/getmondo/signup_old/CountryDialogFragment;->a:Lbutterknife/Unbinder;

    invoke-interface {v0}, Lbutterknife/Unbinder;->unbind()V

    .line 68
    invoke-super {p0}, Landroid/app/DialogFragment;->onDestroyView()V

    .line 69
    return-void
.end method

.method public onDismiss(Landroid/content/DialogInterface;)V
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lco/uk/getmondo/signup_old/CountryDialogFragment;->c:Lco/uk/getmondo/signup_old/CountryDialogFragment$b;

    if-eqz v0, :cond_0

    .line 60
    iget-object v0, p0, Lco/uk/getmondo/signup_old/CountryDialogFragment;->c:Lco/uk/getmondo/signup_old/CountryDialogFragment$b;

    invoke-interface {v0}, Lco/uk/getmondo/signup_old/CountryDialogFragment$b;->a()V

    .line 62
    :cond_0
    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onDismiss(Landroid/content/DialogInterface;)V

    .line 63
    return-void
.end method
