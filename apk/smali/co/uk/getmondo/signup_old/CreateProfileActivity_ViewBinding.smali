.class public Lco/uk/getmondo/signup_old/CreateProfileActivity_ViewBinding;
.super Ljava/lang/Object;
.source "CreateProfileActivity_ViewBinding.java"

# interfaces
.implements Lbutterknife/Unbinder;


# instance fields
.field private a:Lco/uk/getmondo/signup_old/CreateProfileActivity;

.field private b:Landroid/view/View;

.field private c:Landroid/view/View;

.field private d:Landroid/text/TextWatcher;

.field private e:Landroid/view/View;

.field private f:Landroid/text/TextWatcher;


# direct methods
.method public constructor <init>(Lco/uk/getmondo/signup_old/CreateProfileActivity;Landroid/view/View;)V
    .locals 3

    .prologue
    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    iput-object p1, p0, Lco/uk/getmondo/signup_old/CreateProfileActivity_ViewBinding;->a:Lco/uk/getmondo/signup_old/CreateProfileActivity;

    .line 44
    const v0, 0x7f110141

    const-string v1, "field \'nameWrapper\'"

    const-class v2, Landroid/support/design/widget/TextInputLayout;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/design/widget/TextInputLayout;

    iput-object v0, p1, Lco/uk/getmondo/signup_old/CreateProfileActivity;->nameWrapper:Landroid/support/design/widget/TextInputLayout;

    .line 45
    const v0, 0x7f110143

    const-string v1, "field \'dateWrapper\'"

    const-class v2, Landroid/support/design/widget/TextInputLayout;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/design/widget/TextInputLayout;

    iput-object v0, p1, Lco/uk/getmondo/signup_old/CreateProfileActivity;->dateWrapper:Landroid/support/design/widget/TextInputLayout;

    .line 46
    const v0, 0x7f110145

    const-string v1, "field \'postcodeWrapper\'"

    const-class v2, Landroid/support/design/widget/TextInputLayout;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/design/widget/TextInputLayout;

    iput-object v0, p1, Lco/uk/getmondo/signup_old/CreateProfileActivity;->postcodeWrapper:Landroid/support/design/widget/TextInputLayout;

    .line 47
    const v0, 0x7f110147

    const-string v1, "field \'countryWrapper\'"

    const-class v2, Landroid/support/design/widget/TextInputLayout;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/design/widget/TextInputLayout;

    iput-object v0, p1, Lco/uk/getmondo/signup_old/CreateProfileActivity;->countryWrapper:Landroid/support/design/widget/TextInputLayout;

    .line 48
    const v0, 0x7f110148

    const-string v1, "field \'countryEditText\'"

    const-class v2, Landroid/widget/EditText;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p1, Lco/uk/getmondo/signup_old/CreateProfileActivity;->countryEditText:Landroid/widget/EditText;

    .line 49
    const v0, 0x7f110149

    const-string v1, "field \'createAccountButton\'"

    const-class v2, Landroid/widget/Button;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p1, Lco/uk/getmondo/signup_old/CreateProfileActivity;->createAccountButton:Landroid/widget/Button;

    .line 50
    const v0, 0x7f110144

    const-string v1, "method \'onDateAction\' and method \'onDateFocusChange\'"

    invoke-static {p2, v0, v1}, Lbutterknife/internal/Utils;->findRequiredView(Landroid/view/View;ILjava/lang/String;)Landroid/view/View;

    move-result-object v1

    .line 51
    iput-object v1, p0, Lco/uk/getmondo/signup_old/CreateProfileActivity_ViewBinding;->b:Landroid/view/View;

    move-object v0, v1

    .line 52
    check-cast v0, Landroid/widget/TextView;

    new-instance v2, Lco/uk/getmondo/signup_old/CreateProfileActivity_ViewBinding$1;

    invoke-direct {v2, p0, p1}, Lco/uk/getmondo/signup_old/CreateProfileActivity_ViewBinding$1;-><init>(Lco/uk/getmondo/signup_old/CreateProfileActivity_ViewBinding;Lco/uk/getmondo/signup_old/CreateProfileActivity;)V

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 58
    new-instance v0, Lco/uk/getmondo/signup_old/CreateProfileActivity_ViewBinding$2;

    invoke-direct {v0, p0, p1}, Lco/uk/getmondo/signup_old/CreateProfileActivity_ViewBinding$2;-><init>(Lco/uk/getmondo/signup_old/CreateProfileActivity_ViewBinding;Lco/uk/getmondo/signup_old/CreateProfileActivity;)V

    invoke-virtual {v1, v0}, Landroid/view/View;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 64
    const v0, 0x7f110142

    const-string v1, "method \'onNameChanged\'"

    invoke-static {p2, v0, v1}, Lbutterknife/internal/Utils;->findRequiredView(Landroid/view/View;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 65
    iput-object v0, p0, Lco/uk/getmondo/signup_old/CreateProfileActivity_ViewBinding;->c:Landroid/view/View;

    .line 66
    new-instance v1, Lco/uk/getmondo/signup_old/CreateProfileActivity_ViewBinding$3;

    invoke-direct {v1, p0, p1}, Lco/uk/getmondo/signup_old/CreateProfileActivity_ViewBinding$3;-><init>(Lco/uk/getmondo/signup_old/CreateProfileActivity_ViewBinding;Lco/uk/getmondo/signup_old/CreateProfileActivity;)V

    iput-object v1, p0, Lco/uk/getmondo/signup_old/CreateProfileActivity_ViewBinding;->d:Landroid/text/TextWatcher;

    .line 80
    check-cast v0, Landroid/widget/TextView;

    iget-object v1, p0, Lco/uk/getmondo/signup_old/CreateProfileActivity_ViewBinding;->d:Landroid/text/TextWatcher;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 81
    const v0, 0x7f110146

    const-string v1, "method \'onPostcodeChanged\'"

    invoke-static {p2, v0, v1}, Lbutterknife/internal/Utils;->findRequiredView(Landroid/view/View;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 82
    iput-object v0, p0, Lco/uk/getmondo/signup_old/CreateProfileActivity_ViewBinding;->e:Landroid/view/View;

    .line 83
    new-instance v1, Lco/uk/getmondo/signup_old/CreateProfileActivity_ViewBinding$4;

    invoke-direct {v1, p0, p1}, Lco/uk/getmondo/signup_old/CreateProfileActivity_ViewBinding$4;-><init>(Lco/uk/getmondo/signup_old/CreateProfileActivity_ViewBinding;Lco/uk/getmondo/signup_old/CreateProfileActivity;)V

    iput-object v1, p0, Lco/uk/getmondo/signup_old/CreateProfileActivity_ViewBinding;->f:Landroid/text/TextWatcher;

    .line 97
    check-cast v0, Landroid/widget/TextView;

    iget-object v1, p0, Lco/uk/getmondo/signup_old/CreateProfileActivity_ViewBinding;->f:Landroid/text/TextWatcher;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 98
    return-void
.end method


# virtual methods
.method public unbind()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 103
    iget-object v0, p0, Lco/uk/getmondo/signup_old/CreateProfileActivity_ViewBinding;->a:Lco/uk/getmondo/signup_old/CreateProfileActivity;

    .line 104
    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Bindings already cleared."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 105
    :cond_0
    iput-object v2, p0, Lco/uk/getmondo/signup_old/CreateProfileActivity_ViewBinding;->a:Lco/uk/getmondo/signup_old/CreateProfileActivity;

    .line 107
    iput-object v2, v0, Lco/uk/getmondo/signup_old/CreateProfileActivity;->nameWrapper:Landroid/support/design/widget/TextInputLayout;

    .line 108
    iput-object v2, v0, Lco/uk/getmondo/signup_old/CreateProfileActivity;->dateWrapper:Landroid/support/design/widget/TextInputLayout;

    .line 109
    iput-object v2, v0, Lco/uk/getmondo/signup_old/CreateProfileActivity;->postcodeWrapper:Landroid/support/design/widget/TextInputLayout;

    .line 110
    iput-object v2, v0, Lco/uk/getmondo/signup_old/CreateProfileActivity;->countryWrapper:Landroid/support/design/widget/TextInputLayout;

    .line 111
    iput-object v2, v0, Lco/uk/getmondo/signup_old/CreateProfileActivity;->countryEditText:Landroid/widget/EditText;

    .line 112
    iput-object v2, v0, Lco/uk/getmondo/signup_old/CreateProfileActivity;->createAccountButton:Landroid/widget/Button;

    .line 114
    iget-object v0, p0, Lco/uk/getmondo/signup_old/CreateProfileActivity_ViewBinding;->b:Landroid/view/View;

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 115
    iget-object v0, p0, Lco/uk/getmondo/signup_old/CreateProfileActivity_ViewBinding;->b:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 116
    iput-object v2, p0, Lco/uk/getmondo/signup_old/CreateProfileActivity_ViewBinding;->b:Landroid/view/View;

    .line 117
    iget-object v0, p0, Lco/uk/getmondo/signup_old/CreateProfileActivity_ViewBinding;->c:Landroid/view/View;

    check-cast v0, Landroid/widget/TextView;

    iget-object v1, p0, Lco/uk/getmondo/signup_old/CreateProfileActivity_ViewBinding;->d:Landroid/text/TextWatcher;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->removeTextChangedListener(Landroid/text/TextWatcher;)V

    .line 118
    iput-object v2, p0, Lco/uk/getmondo/signup_old/CreateProfileActivity_ViewBinding;->d:Landroid/text/TextWatcher;

    .line 119
    iput-object v2, p0, Lco/uk/getmondo/signup_old/CreateProfileActivity_ViewBinding;->c:Landroid/view/View;

    .line 120
    iget-object v0, p0, Lco/uk/getmondo/signup_old/CreateProfileActivity_ViewBinding;->e:Landroid/view/View;

    check-cast v0, Landroid/widget/TextView;

    iget-object v1, p0, Lco/uk/getmondo/signup_old/CreateProfileActivity_ViewBinding;->f:Landroid/text/TextWatcher;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->removeTextChangedListener(Landroid/text/TextWatcher;)V

    .line 121
    iput-object v2, p0, Lco/uk/getmondo/signup_old/CreateProfileActivity_ViewBinding;->f:Landroid/text/TextWatcher;

    .line 122
    iput-object v2, p0, Lco/uk/getmondo/signup_old/CreateProfileActivity_ViewBinding;->e:Landroid/view/View;

    .line 123
    return-void
.end method
