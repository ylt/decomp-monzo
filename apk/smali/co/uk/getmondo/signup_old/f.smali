.class public Lco/uk/getmondo/signup_old/f;
.super Lco/uk/getmondo/common/ui/b;
.source "CreateProfilePresenter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lco/uk/getmondo/signup_old/f$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lco/uk/getmondo/common/ui/b",
        "<",
        "Lco/uk/getmondo/signup_old/f$a;",
        ">;"
    }
.end annotation


# instance fields
.field private final c:Lco/uk/getmondo/common/s;

.field private final d:Lio/reactivex/u;

.field private final e:Lio/reactivex/u;

.field private final f:Lco/uk/getmondo/common/e/a;

.field private final g:Lco/uk/getmondo/common/accounts/d;

.field private final h:Lco/uk/getmondo/common/m;

.field private final i:Lco/uk/getmondo/common/a;

.field private final j:Lco/uk/getmondo/fcm/e;

.field private final k:Lco/uk/getmondo/api/b/a;

.field private l:Lco/uk/getmondo/d/i;


# direct methods
.method constructor <init>(Lio/reactivex/u;Lio/reactivex/u;Lco/uk/getmondo/common/e/a;Lco/uk/getmondo/common/accounts/d;Lco/uk/getmondo/common/s;Lco/uk/getmondo/common/m;Lco/uk/getmondo/common/a;Lco/uk/getmondo/fcm/e;Lco/uk/getmondo/api/b/a;)V
    .locals 0

    .prologue
    .line 52
    invoke-direct {p0}, Lco/uk/getmondo/common/ui/b;-><init>()V

    .line 53
    iput-object p1, p0, Lco/uk/getmondo/signup_old/f;->d:Lio/reactivex/u;

    .line 54
    iput-object p2, p0, Lco/uk/getmondo/signup_old/f;->e:Lio/reactivex/u;

    .line 55
    iput-object p3, p0, Lco/uk/getmondo/signup_old/f;->f:Lco/uk/getmondo/common/e/a;

    .line 56
    iput-object p4, p0, Lco/uk/getmondo/signup_old/f;->g:Lco/uk/getmondo/common/accounts/d;

    .line 57
    iput-object p5, p0, Lco/uk/getmondo/signup_old/f;->c:Lco/uk/getmondo/common/s;

    .line 58
    iput-object p6, p0, Lco/uk/getmondo/signup_old/f;->h:Lco/uk/getmondo/common/m;

    .line 59
    iput-object p7, p0, Lco/uk/getmondo/signup_old/f;->i:Lco/uk/getmondo/common/a;

    .line 60
    iput-object p8, p0, Lco/uk/getmondo/signup_old/f;->j:Lco/uk/getmondo/fcm/e;

    .line 61
    iput-object p9, p0, Lco/uk/getmondo/signup_old/f;->k:Lco/uk/getmondo/api/b/a;

    .line 62
    return-void
.end method

.method static synthetic a(Lco/uk/getmondo/signup_old/f;Lco/uk/getmondo/signup_old/a/a/a;)Lco/uk/getmondo/signup_old/a/a/a;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 93
    invoke-direct {p0, p1}, Lco/uk/getmondo/signup_old/f;->a(Lco/uk/getmondo/signup_old/a/a/a;)Z

    move-result v0

    .line 94
    invoke-virtual {p1, v0}, Lco/uk/getmondo/signup_old/a/a/a;->a(Z)Lco/uk/getmondo/signup_old/a/a/a;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lco/uk/getmondo/signup_old/f;Lco/uk/getmondo/signup_old/f$a;Lco/uk/getmondo/signup_old/a/a/a;)Lio/reactivex/l;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 97
    iget-object v0, p0, Lco/uk/getmondo/signup_old/f;->k:Lco/uk/getmondo/api/b/a;

    invoke-virtual {v0, p2}, Lco/uk/getmondo/api/b/a;->a(Lco/uk/getmondo/signup_old/a/a/a;)Lio/reactivex/b;

    move-result-object v0

    sget-object v1, Lco/uk/getmondo/common/b/a;->a:Lco/uk/getmondo/common/b/a;

    .line 98
    invoke-virtual {v0, v1}, Lio/reactivex/b;->a(Ljava/lang/Object;)Lio/reactivex/v;

    move-result-object v0

    .line 99
    invoke-virtual {v0}, Lio/reactivex/v;->e()Lio/reactivex/h;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/signup_old/f;->e:Lio/reactivex/u;

    .line 100
    invoke-virtual {v0, v1}, Lio/reactivex/h;->b(Lio/reactivex/u;)Lio/reactivex/h;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/signup_old/f;->d:Lio/reactivex/u;

    .line 101
    invoke-virtual {v0, v1}, Lio/reactivex/h;->a(Lio/reactivex/u;)Lio/reactivex/h;

    move-result-object v0

    invoke-static {p1}, Lco/uk/getmondo/signup_old/o;->a(Lco/uk/getmondo/signup_old/f$a;)Lio/reactivex/c/g;

    move-result-object v1

    .line 102
    invoke-virtual {v0, v1}, Lio/reactivex/h;->b(Lio/reactivex/c/g;)Lio/reactivex/h;

    move-result-object v0

    invoke-static {p0, p1}, Lco/uk/getmondo/signup_old/p;->a(Lco/uk/getmondo/signup_old/f;Lco/uk/getmondo/signup_old/f$a;)Lio/reactivex/c/g;

    move-result-object v1

    .line 103
    invoke-virtual {v0, v1}, Lio/reactivex/h;->a(Lio/reactivex/c/g;)Lio/reactivex/h;

    move-result-object v0

    invoke-static {p1}, Lco/uk/getmondo/signup_old/h;->a(Lco/uk/getmondo/signup_old/f$a;)Lio/reactivex/c/b;

    move-result-object v1

    .line 109
    invoke-virtual {v0, v1}, Lio/reactivex/h;->a(Lio/reactivex/c/b;)Lio/reactivex/h;

    move-result-object v0

    .line 110
    invoke-static {}, Lio/reactivex/h;->a()Lio/reactivex/h;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/h;->a(Lio/reactivex/l;)Lio/reactivex/h;

    move-result-object v0

    .line 97
    return-object v0
.end method

.method static synthetic a(Lco/uk/getmondo/signup_old/f$a;Lco/uk/getmondo/common/b/a;Ljava/lang/Throwable;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 109
    invoke-interface {p0}, Lco/uk/getmondo/signup_old/f$a;->i()V

    return-void
.end method

.method static synthetic a(Lco/uk/getmondo/signup_old/f$a;Lio/reactivex/b/b;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 102
    invoke-interface {p0}, Lco/uk/getmondo/signup_old/f$a;->h()V

    return-void
.end method

.method static synthetic a(Lco/uk/getmondo/signup_old/f$a;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 120
    invoke-interface {p0}, Lco/uk/getmondo/signup_old/f$a;->j()V

    return-void
.end method

.method static synthetic a(Lco/uk/getmondo/signup_old/f;Lco/uk/getmondo/signup_old/f$a;Lco/uk/getmondo/common/b/a;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 112
    iget-object v0, p0, Lco/uk/getmondo/signup_old/f;->h:Lco/uk/getmondo/common/m;

    invoke-virtual {v0}, Lco/uk/getmondo/common/m;->a()V

    .line 113
    iget-object v0, p0, Lco/uk/getmondo/signup_old/f;->j:Lco/uk/getmondo/fcm/e;

    invoke-virtual {v0}, Lco/uk/getmondo/fcm/e;->a()V

    .line 114
    iget-object v0, p0, Lco/uk/getmondo/signup_old/f;->c:Lco/uk/getmondo/common/s;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lco/uk/getmondo/common/s;->a(Z)V

    .line 115
    invoke-interface {p1}, Lco/uk/getmondo/signup_old/f$a;->c()V

    .line 116
    return-void
.end method

.method static synthetic a(Lco/uk/getmondo/signup_old/f;Lco/uk/getmondo/signup_old/f$a;Ljava/lang/Throwable;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 104
    invoke-static {p2}, Ld/a/a;->a(Ljava/lang/Throwable;)V

    .line 105
    iget-object v0, p0, Lco/uk/getmondo/signup_old/f;->f:Lco/uk/getmondo/common/e/a;

    invoke-virtual {v0, p2, p1}, Lco/uk/getmondo/common/e/a;->a(Ljava/lang/Throwable;Lco/uk/getmondo/common/e/a$a;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 106
    const v0, 0x7f0a0434

    invoke-interface {p1, v0}, Lco/uk/getmondo/signup_old/f$a;->b(I)V

    .line 108
    :cond_0
    return-void
.end method

.method private a(Lco/uk/getmondo/signup_old/a/a/a;)Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 125
    invoke-virtual {p1}, Lco/uk/getmondo/signup_old/a/a/a;->a()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lco/uk/getmondo/signup_old/f;->c(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 126
    iget-object v0, p0, Lco/uk/getmondo/signup_old/f;->a:Lco/uk/getmondo/common/ui/f;

    check-cast v0, Lco/uk/getmondo/signup_old/f$a;

    invoke-interface {v0}, Lco/uk/getmondo/signup_old/f$a;->d()V

    move v0, v1

    .line 150
    :goto_0
    return v0

    .line 129
    :cond_0
    invoke-virtual {p1}, Lco/uk/getmondo/signup_old/a/a/a;->b()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lco/uk/getmondo/signup_old/f;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 130
    iget-object v0, p0, Lco/uk/getmondo/signup_old/f;->a:Lco/uk/getmondo/common/ui/f;

    check-cast v0, Lco/uk/getmondo/signup_old/f$a;

    invoke-interface {v0}, Lco/uk/getmondo/signup_old/f$a;->e()V

    move v0, v1

    .line 131
    goto :goto_0

    .line 133
    :cond_1
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0}, Ljava/util/Date;-><init>()V

    .line 136
    :try_start_0
    invoke-virtual {p1}, Lco/uk/getmondo/signup_old/a/a/a;->b()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lco/uk/getmondo/create_account/b;->a(Ljava/lang/String;)Ljava/util/Date;
    :try_end_0
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 141
    invoke-static {v2, v0}, Lco/uk/getmondo/create_account/b;->a(Ljava/util/Date;Ljava/util/Date;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 142
    iget-object v0, p0, Lco/uk/getmondo/signup_old/f;->a:Lco/uk/getmondo/common/ui/f;

    check-cast v0, Lco/uk/getmondo/signup_old/f$a;

    invoke-interface {v0}, Lco/uk/getmondo/signup_old/f$a;->g()V

    move v0, v1

    .line 143
    goto :goto_0

    .line 137
    :catch_0
    move-exception v0

    .line 138
    iget-object v0, p0, Lco/uk/getmondo/signup_old/f;->a:Lco/uk/getmondo/common/ui/f;

    check-cast v0, Lco/uk/getmondo/signup_old/f$a;

    invoke-interface {v0}, Lco/uk/getmondo/signup_old/f$a;->e()V

    move v0, v1

    .line 139
    goto :goto_0

    .line 146
    :cond_2
    iget-object v0, p0, Lco/uk/getmondo/signup_old/f;->l:Lco/uk/getmondo/d/i;

    invoke-virtual {v0}, Lco/uk/getmondo/d/i;->a()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p1}, Lco/uk/getmondo/signup_old/a/a/a;->c()Lco/uk/getmondo/d/s;

    move-result-object v0

    invoke-virtual {v0}, Lco/uk/getmondo/d/s;->c()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lco/uk/getmondo/signup_old/f;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 147
    iget-object v0, p0, Lco/uk/getmondo/signup_old/f;->a:Lco/uk/getmondo/common/ui/f;

    check-cast v0, Lco/uk/getmondo/signup_old/f$a;

    invoke-interface {v0}, Lco/uk/getmondo/signup_old/f$a;->f()V

    move v0, v1

    .line 148
    goto :goto_0

    .line 150
    :cond_3
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private a(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 154
    invoke-static {p1}, Lco/uk/getmondo/common/k/p;->c(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method static synthetic b(Lco/uk/getmondo/signup_old/f;Lco/uk/getmondo/signup_old/a/a/a;)Lco/uk/getmondo/signup_old/a/a/a;
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 88
    iget-object v0, p0, Lco/uk/getmondo/signup_old/f;->l:Lco/uk/getmondo/d/i;

    invoke-virtual {v0}, Lco/uk/getmondo/d/i;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lco/uk/getmondo/signup_old/a/a/a;->c()Lco/uk/getmondo/d/s;

    move-result-object v0

    invoke-virtual {v0}, Lco/uk/getmondo/d/s;->c()Ljava/lang/String;

    move-result-object v1

    .line 89
    :goto_0
    new-instance v0, Lco/uk/getmondo/d/s;

    iget-object v3, p0, Lco/uk/getmondo/signup_old/f;->l:Lco/uk/getmondo/d/i;

    invoke-virtual {v3}, Lco/uk/getmondo/d/i;->e()Ljava/lang/String;

    move-result-object v4

    move-object v3, v2

    move-object v5, v2

    invoke-direct/range {v0 .. v5}, Lco/uk/getmondo/d/s;-><init>(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 90
    invoke-virtual {p1, v0}, Lco/uk/getmondo/signup_old/a/a/a;->a(Lco/uk/getmondo/d/s;)Lco/uk/getmondo/signup_old/a/a/a;

    move-result-object v0

    return-object v0

    :cond_0
    move-object v1, v2

    .line 88
    goto :goto_0
.end method

.method private b(Ljava/lang/String;)Z
    .locals 2

    .prologue
    .line 158
    if-nez p1, :cond_0

    .line 159
    const/4 v0, 0x0

    .line 162
    :goto_0
    return v0

    .line 161
    :cond_0
    const-string v0, " "

    const-string v1, ""

    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    .line 162
    new-instance v1, Lco/uk/getmondo/signup_old/s;

    invoke-direct {v1}, Lco/uk/getmondo/signup_old/s;-><init>()V

    invoke-virtual {v1, v0}, Lco/uk/getmondo/signup_old/s;->a(Ljava/lang/String;)Z

    move-result v0

    goto :goto_0
.end method

.method private c(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 166
    invoke-static {p1}, Lco/uk/getmondo/common/k/p;->c(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public bridge synthetic a(Lco/uk/getmondo/common/ui/f;)V
    .locals 0

    .prologue
    .line 35
    check-cast p1, Lco/uk/getmondo/signup_old/f$a;

    invoke-virtual {p0, p1}, Lco/uk/getmondo/signup_old/f;->a(Lco/uk/getmondo/signup_old/f$a;)V

    return-void
.end method

.method public a(Lco/uk/getmondo/signup_old/f$a;)V
    .locals 3

    .prologue
    .line 66
    invoke-super {p0, p1}, Lco/uk/getmondo/common/ui/b;->a(Lco/uk/getmondo/common/ui/f;)V

    .line 68
    iget-object v0, p0, Lco/uk/getmondo/signup_old/f;->i:Lco/uk/getmondo/common/a;

    invoke-static {}, Lco/uk/getmondo/api/model/tracking/Impression;->f()Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v1

    invoke-virtual {v0, v1}, Lco/uk/getmondo/common/a;->a(Lco/uk/getmondo/api/model/tracking/Impression;)V

    .line 70
    invoke-static {}, Lco/uk/getmondo/d/i;->i()Lco/uk/getmondo/d/i;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/signup_old/f;->l:Lco/uk/getmondo/d/i;

    .line 72
    iget-object v0, p0, Lco/uk/getmondo/signup_old/f;->l:Lco/uk/getmondo/d/i;

    invoke-virtual {v0}, Lco/uk/getmondo/d/i;->b()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Lco/uk/getmondo/signup_old/f$a;->f(Ljava/lang/String;)V

    .line 75
    iget-object v0, p0, Lco/uk/getmondo/signup_old/f;->g:Lco/uk/getmondo/common/accounts/d;

    invoke-virtual {v0}, Lco/uk/getmondo/common/accounts/d;->b()Lco/uk/getmondo/d/ak;

    move-result-object v1

    .line 76
    const/4 v0, 0x0

    .line 77
    if-eqz v1, :cond_0

    .line 78
    invoke-virtual {v1}, Lco/uk/getmondo/d/ak;->d()Lco/uk/getmondo/d/ac;

    move-result-object v0

    .line 80
    :cond_0
    if-eqz v0, :cond_1

    .line 81
    invoke-virtual {v0}, Lco/uk/getmondo/d/ac;->c()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v1}, Lco/uk/getmondo/signup_old/f$a;->a(Ljava/lang/String;)V

    .line 82
    invoke-virtual {v0}, Lco/uk/getmondo/d/ac;->e()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lco/uk/getmondo/create_account/b;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v1}, Lco/uk/getmondo/signup_old/f$a;->d(Ljava/lang/String;)V

    .line 83
    invoke-virtual {v0}, Lco/uk/getmondo/d/ac;->h()Lco/uk/getmondo/d/s;

    move-result-object v0

    invoke-virtual {v0}, Lco/uk/getmondo/d/s;->c()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Lco/uk/getmondo/signup_old/f$a;->e(Ljava/lang/String;)V

    .line 86
    :cond_1
    invoke-interface {p1}, Lco/uk/getmondo/signup_old/f$a;->a()Lio/reactivex/n;

    move-result-object v0

    invoke-static {p0}, Lco/uk/getmondo/signup_old/g;->a(Lco/uk/getmondo/signup_old/f;)Lio/reactivex/c/h;

    move-result-object v1

    .line 87
    invoke-virtual {v0, v1}, Lio/reactivex/n;->map(Lio/reactivex/c/h;)Lio/reactivex/n;

    move-result-object v0

    invoke-static {p0}, Lco/uk/getmondo/signup_old/i;->a(Lco/uk/getmondo/signup_old/f;)Lio/reactivex/c/h;

    move-result-object v1

    .line 92
    invoke-virtual {v0, v1}, Lio/reactivex/n;->map(Lio/reactivex/c/h;)Lio/reactivex/n;

    move-result-object v0

    invoke-static {}, Lco/uk/getmondo/signup_old/j;->a()Lio/reactivex/c/q;

    move-result-object v1

    .line 96
    invoke-virtual {v0, v1}, Lio/reactivex/n;->filter(Lio/reactivex/c/q;)Lio/reactivex/n;

    move-result-object v0

    invoke-static {p0, p1}, Lco/uk/getmondo/signup_old/k;->a(Lco/uk/getmondo/signup_old/f;Lco/uk/getmondo/signup_old/f$a;)Lio/reactivex/c/h;

    move-result-object v1

    .line 97
    invoke-virtual {v0, v1}, Lio/reactivex/n;->flatMapMaybe(Lio/reactivex/c/h;)Lio/reactivex/n;

    move-result-object v0

    invoke-static {p0, p1}, Lco/uk/getmondo/signup_old/l;->a(Lco/uk/getmondo/signup_old/f;Lco/uk/getmondo/signup_old/f$a;)Lio/reactivex/c/g;

    move-result-object v1

    invoke-static {}, Lco/uk/getmondo/signup_old/m;->a()Lio/reactivex/c/g;

    move-result-object v2

    .line 111
    invoke-virtual {v0, v1, v2}, Lio/reactivex/n;->subscribe(Lio/reactivex/c/g;Lio/reactivex/c/g;)Lio/reactivex/b/b;

    move-result-object v0

    .line 86
    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup_old/f;->a(Lio/reactivex/b/b;)V

    .line 119
    invoke-interface {p1}, Lco/uk/getmondo/signup_old/f$a;->b()Lio/reactivex/n;

    move-result-object v0

    invoke-static {p1}, Lco/uk/getmondo/signup_old/n;->a(Lco/uk/getmondo/signup_old/f$a;)Lio/reactivex/c/g;

    move-result-object v1

    .line 120
    invoke-virtual {v0, v1}, Lio/reactivex/n;->subscribe(Lio/reactivex/c/g;)Lio/reactivex/b/b;

    move-result-object v0

    .line 119
    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup_old/f;->a(Lio/reactivex/b/b;)V

    .line 122
    return-void
.end method
