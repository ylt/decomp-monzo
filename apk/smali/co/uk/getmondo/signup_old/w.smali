.class public Lco/uk/getmondo/signup_old/w;
.super Lco/uk/getmondo/common/ui/b;
.source "SignUpPresenter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lco/uk/getmondo/signup_old/w$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lco/uk/getmondo/common/ui/b",
        "<",
        "Lco/uk/getmondo/signup_old/w$a;",
        ">;"
    }
.end annotation


# static fields
.field private static final c:Ljava/util/regex/Pattern;


# instance fields
.field private final d:Lio/reactivex/u;

.field private final e:Lio/reactivex/u;

.field private final f:Lco/uk/getmondo/common/e/a;

.field private final g:Lco/uk/getmondo/api/b/a;

.field private final h:Lco/uk/getmondo/common/a;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 28
    const-string v0, "[a-zA-Z0-9\\+\\.\\_\\%\\-\\+]{1,256}\\@[a-zA-Z0-9][a-zA-Z0-9\\-]{0,64}(\\.[a-zA-Z0-9][a-zA-Z0-9\\-]{0,25})+"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lco/uk/getmondo/signup_old/w;->c:Ljava/util/regex/Pattern;

    return-void
.end method

.method constructor <init>(Lio/reactivex/u;Lio/reactivex/u;Lco/uk/getmondo/common/e/a;Lco/uk/getmondo/api/b/a;Lco/uk/getmondo/common/a;)V
    .locals 0

    .prologue
    .line 44
    invoke-direct {p0}, Lco/uk/getmondo/common/ui/b;-><init>()V

    .line 45
    iput-object p1, p0, Lco/uk/getmondo/signup_old/w;->d:Lio/reactivex/u;

    .line 46
    iput-object p2, p0, Lco/uk/getmondo/signup_old/w;->e:Lio/reactivex/u;

    .line 47
    iput-object p3, p0, Lco/uk/getmondo/signup_old/w;->f:Lco/uk/getmondo/common/e/a;

    .line 48
    iput-object p4, p0, Lco/uk/getmondo/signup_old/w;->g:Lco/uk/getmondo/api/b/a;

    .line 49
    iput-object p5, p0, Lco/uk/getmondo/signup_old/w;->h:Lco/uk/getmondo/common/a;

    .line 50
    return-void
.end method

.method static synthetic a(Lco/uk/getmondo/signup_old/w;Lco/uk/getmondo/signup_old/w$a;Ljava/lang/Object;)Lio/reactivex/l;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 59
    invoke-interface {p1}, Lco/uk/getmondo/signup_old/w$a;->c()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lco/uk/getmondo/signup_old/w;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 60
    invoke-interface {p1}, Lco/uk/getmondo/signup_old/w$a;->f()V

    .line 61
    invoke-static {}, Lio/reactivex/h;->a()Lio/reactivex/h;

    move-result-object v0

    .line 67
    :goto_0
    return-object v0

    .line 63
    :cond_0
    invoke-interface {p1}, Lco/uk/getmondo/signup_old/w$a;->e()V

    .line 67
    iget-object v0, p0, Lco/uk/getmondo/signup_old/w;->g:Lco/uk/getmondo/api/b/a;

    invoke-interface {p1}, Lco/uk/getmondo/signup_old/w$a;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lco/uk/getmondo/api/b/a;->a(Ljava/lang/String;)Lio/reactivex/b;

    move-result-object v0

    sget-object v1, Lco/uk/getmondo/common/b/a;->a:Lco/uk/getmondo/common/b/a;

    .line 68
    invoke-virtual {v0, v1}, Lio/reactivex/b;->a(Ljava/lang/Object;)Lio/reactivex/v;

    move-result-object v0

    .line 69
    invoke-virtual {v0}, Lio/reactivex/v;->e()Lio/reactivex/h;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/signup_old/w;->e:Lio/reactivex/u;

    .line 70
    invoke-virtual {v0, v1}, Lio/reactivex/h;->b(Lio/reactivex/u;)Lio/reactivex/h;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/signup_old/w;->d:Lio/reactivex/u;

    .line 71
    invoke-virtual {v0, v1}, Lio/reactivex/h;->a(Lio/reactivex/u;)Lio/reactivex/h;

    move-result-object v0

    invoke-static {p0, p1}, Lco/uk/getmondo/signup_old/aa;->a(Lco/uk/getmondo/signup_old/w;Lco/uk/getmondo/signup_old/w$a;)Lio/reactivex/c/g;

    move-result-object v1

    .line 72
    invoke-virtual {v0, v1}, Lio/reactivex/h;->a(Lio/reactivex/c/g;)Lio/reactivex/h;

    move-result-object v0

    .line 77
    invoke-static {}, Lio/reactivex/h;->a()Lio/reactivex/h;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/h;->a(Lio/reactivex/l;)Lio/reactivex/h;

    move-result-object v0

    goto :goto_0
.end method

.method static synthetic a(Lco/uk/getmondo/signup_old/w$a;Lco/uk/getmondo/common/b/a;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 80
    invoke-interface {p0}, Lco/uk/getmondo/signup_old/w$a;->d()V

    return-void
.end method

.method static synthetic a(Lco/uk/getmondo/signup_old/w;Lco/uk/getmondo/signup_old/w$a;Ljava/lang/Throwable;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 73
    iget-object v0, p0, Lco/uk/getmondo/signup_old/w;->f:Lco/uk/getmondo/common/e/a;

    invoke-virtual {v0, p2, p1}, Lco/uk/getmondo/common/e/a;->a(Ljava/lang/Throwable;Lco/uk/getmondo/common/e/a$a;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 74
    const v0, 0x7f0a0395

    invoke-interface {p1, v0}, Lco/uk/getmondo/signup_old/w$a;->b(I)V

    .line 76
    :cond_0
    return-void
.end method

.method private a(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 85
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_1

    .line 86
    :cond_0
    const/4 v0, 0x0

    .line 88
    :goto_0
    return v0

    :cond_1
    sget-object v0, Lco/uk/getmondo/signup_old/w;->c:Ljava/util/regex/Pattern;

    invoke-virtual {v0, p1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/regex/Matcher;->matches()Z

    move-result v0

    goto :goto_0
.end method

.method private b(Lco/uk/getmondo/signup_old/w$a;)I
    .locals 3

    .prologue
    .line 93
    :try_start_0
    check-cast p1, Lco/uk/getmondo/signup_old/SignUpActivity;

    invoke-virtual {p1}, Lco/uk/getmondo/signup_old/SignUpActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 94
    const-string v1, "page_extra"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 96
    :goto_0
    return v0

    .line 95
    :catch_0
    move-exception v0

    .line 96
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public bridge synthetic a(Lco/uk/getmondo/common/ui/f;)V
    .locals 0

    .prologue
    .line 25
    check-cast p1, Lco/uk/getmondo/signup_old/w$a;

    invoke-virtual {p0, p1}, Lco/uk/getmondo/signup_old/w;->a(Lco/uk/getmondo/signup_old/w$a;)V

    return-void
.end method

.method public a(Lco/uk/getmondo/signup_old/w$a;)V
    .locals 3

    .prologue
    .line 54
    invoke-super {p0, p1}, Lco/uk/getmondo/common/ui/b;->a(Lco/uk/getmondo/common/ui/f;)V

    .line 55
    iget-object v0, p0, Lco/uk/getmondo/signup_old/w;->h:Lco/uk/getmondo/common/a;

    invoke-direct {p0, p1}, Lco/uk/getmondo/signup_old/w;->b(Lco/uk/getmondo/signup_old/w$a;)I

    move-result v1

    invoke-static {v1}, Lco/uk/getmondo/api/model/tracking/Impression;->a(I)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v1

    invoke-virtual {v0, v1}, Lco/uk/getmondo/common/a;->a(Lco/uk/getmondo/api/model/tracking/Impression;)V

    .line 57
    invoke-interface {p1}, Lco/uk/getmondo/signup_old/w$a;->b()Lio/reactivex/n;

    move-result-object v0

    invoke-interface {p1}, Lco/uk/getmondo/signup_old/w$a;->a()Lio/reactivex/n;

    move-result-object v1

    invoke-static {v0, v1}, Lio/reactivex/n;->merge(Lio/reactivex/r;Lio/reactivex/r;)Lio/reactivex/n;

    move-result-object v0

    invoke-static {p0, p1}, Lco/uk/getmondo/signup_old/x;->a(Lco/uk/getmondo/signup_old/w;Lco/uk/getmondo/signup_old/w$a;)Lio/reactivex/c/h;

    move-result-object v1

    .line 58
    invoke-virtual {v0, v1}, Lio/reactivex/n;->flatMapMaybe(Lio/reactivex/c/h;)Lio/reactivex/n;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/signup_old/w;->d:Lio/reactivex/u;

    .line 79
    invoke-virtual {v0, v1}, Lio/reactivex/n;->observeOn(Lio/reactivex/u;)Lio/reactivex/n;

    move-result-object v0

    invoke-static {p1}, Lco/uk/getmondo/signup_old/y;->a(Lco/uk/getmondo/signup_old/w$a;)Lio/reactivex/c/g;

    move-result-object v1

    invoke-static {}, Lco/uk/getmondo/signup_old/z;->a()Lio/reactivex/c/g;

    move-result-object v2

    .line 80
    invoke-virtual {v0, v1, v2}, Lio/reactivex/n;->subscribe(Lio/reactivex/c/g;Lio/reactivex/c/g;)Lio/reactivex/b/b;

    move-result-object v0

    .line 57
    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup_old/w;->a(Lio/reactivex/b/b;)V

    .line 81
    return-void
.end method
