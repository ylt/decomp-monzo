.class public Lco/uk/getmondo/signup_old/SignUpActivity;
.super Lco/uk/getmondo/common/activities/b;
.source "SignUpActivity.java"

# interfaces
.implements Lco/uk/getmondo/signup_old/w$a;


# instance fields
.field a:Lco/uk/getmondo/signup_old/w;

.field continueButton:Landroid/widget/Button;
    .annotation build Lbutterknife/BindView;
        value = 0x7f11021e
    .end annotation
.end field

.field emailEditText:Landroid/widget/EditText;
    .annotation build Lbutterknife/BindView;
        value = 0x7f11021d
    .end annotation
.end field

.field emailWrapper:Landroid/support/design/widget/TextInputLayout;
    .annotation build Lbutterknife/BindView;
        value = 0x7f11021c
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, Lco/uk/getmondo/common/activities/b;-><init>()V

    return-void
.end method

.method public static a(Landroid/app/Activity;I)V
    .locals 2

    .prologue
    .line 32
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lco/uk/getmondo/signup_old/SignUpActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 33
    const-string v1, "page_extra"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 34
    invoke-virtual {p0, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    .line 35
    return-void
.end method


# virtual methods
.method public a()Lio/reactivex/n;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 46
    iget-object v0, p0, Lco/uk/getmondo/signup_old/SignUpActivity;->emailEditText:Landroid/widget/EditText;

    invoke-static {v0}, Lcom/b/a/d/e;->a(Landroid/widget/TextView;)Lio/reactivex/n;

    move-result-object v0

    invoke-static {}, Lco/uk/getmondo/signup_old/u;->a()Lio/reactivex/c/h;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/n;->map(Lio/reactivex/c/h;)Lio/reactivex/n;

    move-result-object v0

    return-object v0
.end method

.method public b()Lio/reactivex/n;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 51
    iget-object v0, p0, Lco/uk/getmondo/signup_old/SignUpActivity;->continueButton:Landroid/widget/Button;

    invoke-static {v0}, Lcom/b/a/c/c;->a(Landroid/view/View;)Lio/reactivex/n;

    move-result-object v0

    return-object v0
.end method

.method public c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lco/uk/getmondo/signup_old/SignUpActivity;->emailEditText:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public d()V
    .locals 0

    .prologue
    .line 76
    invoke-static {p0}, Lco/uk/getmondo/signup/EmailActivity;->a(Landroid/app/Activity;)V

    .line 77
    invoke-virtual {p0}, Lco/uk/getmondo/signup_old/SignUpActivity;->t()V

    .line 78
    return-void
.end method

.method public e()V
    .locals 2

    .prologue
    .line 82
    iget-object v0, p0, Lco/uk/getmondo/signup_old/SignUpActivity;->emailWrapper:Landroid/support/design/widget/TextInputLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/design/widget/TextInputLayout;->setError(Ljava/lang/CharSequence;)V

    .line 83
    return-void
.end method

.method public f()V
    .locals 2

    .prologue
    .line 87
    iget-object v0, p0, Lco/uk/getmondo/signup_old/SignUpActivity;->emailWrapper:Landroid/support/design/widget/TextInputLayout;

    const v1, 0x7f0a017c

    invoke-virtual {p0, v1}, Lco/uk/getmondo/signup_old/SignUpActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/design/widget/TextInputLayout;->setError(Ljava/lang/CharSequence;)V

    .line 88
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 61
    invoke-super {p0, p1}, Lco/uk/getmondo/common/activities/b;->onCreate(Landroid/os/Bundle;)V

    .line 62
    const v0, 0x7f05005f

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup_old/SignUpActivity;->setContentView(I)V

    .line 63
    invoke-virtual {p0}, Lco/uk/getmondo/signup_old/SignUpActivity;->l()Lco/uk/getmondo/common/h/b/b;

    move-result-object v0

    invoke-interface {v0, p0}, Lco/uk/getmondo/common/h/b/b;->a(Lco/uk/getmondo/signup_old/SignUpActivity;)V

    .line 64
    invoke-static {p0}, Lbutterknife/ButterKnife;->bind(Landroid/app/Activity;)Lbutterknife/Unbinder;

    .line 65
    iget-object v0, p0, Lco/uk/getmondo/signup_old/SignUpActivity;->a:Lco/uk/getmondo/signup_old/w;

    invoke-virtual {v0, p0}, Lco/uk/getmondo/signup_old/w;->a(Lco/uk/getmondo/signup_old/w$a;)V

    .line 66
    return-void
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 70
    iget-object v0, p0, Lco/uk/getmondo/signup_old/SignUpActivity;->a:Lco/uk/getmondo/signup_old/w;

    invoke-virtual {v0}, Lco/uk/getmondo/signup_old/w;->b()V

    .line 71
    invoke-super {p0}, Lco/uk/getmondo/common/activities/b;->onDestroy()V

    .line 72
    return-void
.end method

.method onTextChanged(Ljava/lang/CharSequence;)V
    .locals 2
    .annotation build Lbutterknife/OnTextChanged;
        value = {
            0x7f11021d
        }
    .end annotation

    .prologue
    .line 39
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-lez v0, :cond_0

    .line 40
    iget-object v0, p0, Lco/uk/getmondo/signup_old/SignUpActivity;->emailWrapper:Landroid/support/design/widget/TextInputLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/design/widget/TextInputLayout;->setError(Ljava/lang/CharSequence;)V

    .line 42
    :cond_0
    return-void
.end method
