.class public final Lco/uk/getmondo/signup_old/a/a/a;
.super Ljava/lang/Object;
.source "ProfileData.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u000f\n\u0002\u0010\u0008\n\u0002\u0008\u0004\u0008\u0086\u0008\u0018\u00002\u00020\u0001B)\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u0008\u0008\u0002\u0010\u0007\u001a\u00020\u0008\u00a2\u0006\u0002\u0010\tJ\t\u0010\u0010\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0011\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0012\u001a\u00020\u0006H\u00c6\u0003J\t\u0010\u0013\u001a\u00020\u0008H\u00c6\u0003J1\u0010\u0014\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0005\u001a\u00020\u00062\u0008\u0008\u0002\u0010\u0007\u001a\u00020\u0008H\u00c6\u0001J\u0013\u0010\u0015\u001a\u00020\u00082\u0008\u0010\u0016\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u0010\u0017\u001a\u00020\u0018H\u00d6\u0001J\t\u0010\u0019\u001a\u00020\u0003H\u00d6\u0001J\u000e\u0010\u001a\u001a\u00020\u00002\u0006\u0010\u0005\u001a\u00020\u0006J\u000e\u0010\u001b\u001a\u00020\u00002\u0006\u0010\u0007\u001a\u00020\u0008R\u0011\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\n\u0010\u000bR\u0011\u0010\u0004\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000c\u0010\rR\u0011\u0010\u0007\u001a\u00020\u0008\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0007\u0010\u000eR\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000f\u0010\r\u00a8\u0006\u001c"
    }
    d2 = {
        "Lco/uk/getmondo/signup_old/data/model/ProfileData;",
        "",
        "name",
        "",
        "dateOfBirth",
        "address",
        "Lco/uk/getmondo/model/LegacyAddress;",
        "isValid",
        "",
        "(Ljava/lang/String;Ljava/lang/String;Lco/uk/getmondo/model/LegacyAddress;Z)V",
        "getAddress",
        "()Lco/uk/getmondo/model/LegacyAddress;",
        "getDateOfBirth",
        "()Ljava/lang/String;",
        "()Z",
        "getName",
        "component1",
        "component2",
        "component3",
        "component4",
        "copy",
        "equals",
        "other",
        "hashCode",
        "",
        "toString",
        "withAddress",
        "withValidity",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Ljava/lang/String;

.field private final c:Lco/uk/getmondo/d/s;

.field private final d:Z


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Lco/uk/getmondo/d/s;)V
    .locals 7

    const/4 v4, 0x0

    const/16 v5, 0x8

    const/4 v6, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    invoke-direct/range {v0 .. v6}, Lco/uk/getmondo/signup_old/a/a/a;-><init>(Ljava/lang/String;Ljava/lang/String;Lco/uk/getmondo/d/s;ZILkotlin/d/b/i;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Lco/uk/getmondo/d/s;Z)V
    .locals 1

    .prologue
    const-string v0, "name"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "dateOfBirth"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "address"

    invoke-static {p3, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 5
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lco/uk/getmondo/signup_old/a/a/a;->a:Ljava/lang/String;

    iput-object p2, p0, Lco/uk/getmondo/signup_old/a/a/a;->b:Ljava/lang/String;

    iput-object p3, p0, Lco/uk/getmondo/signup_old/a/a/a;->c:Lco/uk/getmondo/d/s;

    iput-boolean p4, p0, Lco/uk/getmondo/signup_old/a/a/a;->d:Z

    return-void
.end method

.method public synthetic constructor <init>(Ljava/lang/String;Ljava/lang/String;Lco/uk/getmondo/d/s;ZILkotlin/d/b/i;)V
    .locals 1

    .prologue
    and-int/lit8 v0, p5, 0x8

    if-eqz v0, :cond_0

    .line 9
    const/4 p4, 0x0

    :cond_0
    invoke-direct {p0, p1, p2, p3, p4}, Lco/uk/getmondo/signup_old/a/a/a;-><init>(Ljava/lang/String;Ljava/lang/String;Lco/uk/getmondo/d/s;Z)V

    return-void
.end method

.method public static bridge synthetic a(Lco/uk/getmondo/signup_old/a/a/a;Ljava/lang/String;Ljava/lang/String;Lco/uk/getmondo/d/s;ZILjava/lang/Object;)Lco/uk/getmondo/signup_old/a/a/a;
    .locals 1

    and-int/lit8 v0, p5, 0x1

    if-eqz v0, :cond_0

    iget-object p1, p0, Lco/uk/getmondo/signup_old/a/a/a;->a:Ljava/lang/String;

    :cond_0
    and-int/lit8 v0, p5, 0x2

    if-eqz v0, :cond_1

    iget-object p2, p0, Lco/uk/getmondo/signup_old/a/a/a;->b:Ljava/lang/String;

    :cond_1
    and-int/lit8 v0, p5, 0x4

    if-eqz v0, :cond_2

    iget-object p3, p0, Lco/uk/getmondo/signup_old/a/a/a;->c:Lco/uk/getmondo/d/s;

    :cond_2
    and-int/lit8 v0, p5, 0x8

    if-eqz v0, :cond_3

    iget-boolean p4, p0, Lco/uk/getmondo/signup_old/a/a/a;->d:Z

    :cond_3
    invoke-virtual {p0, p1, p2, p3, p4}, Lco/uk/getmondo/signup_old/a/a/a;->a(Ljava/lang/String;Ljava/lang/String;Lco/uk/getmondo/d/s;Z)Lco/uk/getmondo/signup_old/a/a/a;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(Lco/uk/getmondo/d/s;)Lco/uk/getmondo/signup_old/a/a/a;
    .locals 7

    .prologue
    const/4 v1, 0x0

    const-string v0, "address"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 17
    const/4 v4, 0x0

    const/16 v5, 0xb

    move-object v0, p0

    move-object v2, v1

    move-object v3, p1

    move-object v6, v1

    invoke-static/range {v0 .. v6}, Lco/uk/getmondo/signup_old/a/a/a;->a(Lco/uk/getmondo/signup_old/a/a/a;Ljava/lang/String;Ljava/lang/String;Lco/uk/getmondo/d/s;ZILjava/lang/Object;)Lco/uk/getmondo/signup_old/a/a/a;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Lco/uk/getmondo/d/s;Z)Lco/uk/getmondo/signup_old/a/a/a;
    .locals 1

    const-string v0, "name"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "dateOfBirth"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "address"

    invoke-static {p3, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lco/uk/getmondo/signup_old/a/a/a;

    invoke-direct {v0, p1, p2, p3, p4}, Lco/uk/getmondo/signup_old/a/a/a;-><init>(Ljava/lang/String;Ljava/lang/String;Lco/uk/getmondo/d/s;Z)V

    return-object v0
.end method

.method public final a(Z)Lco/uk/getmondo/signup_old/a/a/a;
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 13
    const/4 v5, 0x7

    move-object v0, p0

    move-object v2, v1

    move-object v3, v1

    move v4, p1

    move-object v6, v1

    invoke-static/range {v0 .. v6}, Lco/uk/getmondo/signup_old/a/a/a;->a(Lco/uk/getmondo/signup_old/a/a/a;Ljava/lang/String;Ljava/lang/String;Lco/uk/getmondo/d/s;ZILjava/lang/Object;)Lco/uk/getmondo/signup_old/a/a/a;

    move-result-object v0

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 6
    iget-object v0, p0, Lco/uk/getmondo/signup_old/a/a/a;->a:Ljava/lang/String;

    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 7
    iget-object v0, p0, Lco/uk/getmondo/signup_old/a/a/a;->b:Ljava/lang/String;

    return-object v0
.end method

.method public final c()Lco/uk/getmondo/d/s;
    .locals 1

    .prologue
    .line 8
    iget-object v0, p0, Lco/uk/getmondo/signup_old/a/a/a;->c:Lco/uk/getmondo/d/s;

    return-object v0
.end method

.method public final d()Z
    .locals 1

    .prologue
    .line 9
    iget-boolean v0, p0, Lco/uk/getmondo/signup_old/a/a/a;->d:Z

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v1, 0x1

    const/4 v0, 0x0

    if-eq p0, p1, :cond_0

    instance-of v2, p1, Lco/uk/getmondo/signup_old/a/a/a;

    if-eqz v2, :cond_1

    check-cast p1, Lco/uk/getmondo/signup_old/a/a/a;

    iget-object v2, p0, Lco/uk/getmondo/signup_old/a/a/a;->a:Ljava/lang/String;

    iget-object v3, p1, Lco/uk/getmondo/signup_old/a/a/a;->a:Ljava/lang/String;

    invoke-static {v2, v3}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lco/uk/getmondo/signup_old/a/a/a;->b:Ljava/lang/String;

    iget-object v3, p1, Lco/uk/getmondo/signup_old/a/a/a;->b:Ljava/lang/String;

    invoke-static {v2, v3}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lco/uk/getmondo/signup_old/a/a/a;->c:Lco/uk/getmondo/d/s;

    iget-object v3, p1, Lco/uk/getmondo/signup_old/a/a/a;->c:Lco/uk/getmondo/d/s;

    invoke-static {v2, v3}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    iget-boolean v2, p0, Lco/uk/getmondo/signup_old/a/a/a;->d:Z

    iget-boolean v3, p1, Lco/uk/getmondo/signup_old/a/a/a;->d:Z

    if-ne v2, v3, :cond_2

    move v2, v1

    :goto_0
    if-eqz v2, :cond_1

    :cond_0
    move v0, v1

    :cond_1
    return v0

    :cond_2
    move v2, v0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    const/4 v1, 0x0

    iget-object v0, p0, Lco/uk/getmondo/signup_old/a/a/a;->a:Ljava/lang/String;

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    :goto_0
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lco/uk/getmondo/signup_old/a/a/a;->b:Ljava/lang/String;

    if-eqz v0, :cond_3

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lco/uk/getmondo/signup_old/a/a/a;->c:Lco/uk/getmondo/d/s;

    if-eqz v2, :cond_0

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_0
    add-int/2addr v0, v1

    mul-int/lit8 v1, v0, 0x1f

    iget-boolean v0, p0, Lco/uk/getmondo/signup_old/a/a/a;->d:Z

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :cond_1
    add-int/2addr v0, v1

    return v0

    :cond_2
    move v0, v1

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_1
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "ProfileData(name="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/signup_old/a/a/a;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", dateOfBirth="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/signup_old/a/a/a;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", address="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/signup_old/a/a/a;->c:Lco/uk/getmondo/d/s;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", isValid="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lco/uk/getmondo/signup_old/a/a/a;->d:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
