.class public final synthetic Lco/uk/getmondo/api/model/identity_verification/LegacyKyc$Status$WhenMappings;
.super Ljava/lang/Object;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# static fields
.field public static final synthetic $EnumSwitchMapping$0:[I


# direct methods
.method static synthetic constructor <clinit>()V
    .locals 3

    invoke-static {}, Lco/uk/getmondo/api/model/identity_verification/LegacyKyc$Status;->values()[Lco/uk/getmondo/api/model/identity_verification/LegacyKyc$Status;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lco/uk/getmondo/api/model/identity_verification/LegacyKyc$Status$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v0, Lco/uk/getmondo/api/model/identity_verification/LegacyKyc$Status$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lco/uk/getmondo/api/model/identity_verification/LegacyKyc$Status;->KYC_PASSED:Lco/uk/getmondo/api/model/identity_verification/LegacyKyc$Status;

    invoke-virtual {v1}, Lco/uk/getmondo/api/model/identity_verification/LegacyKyc$Status;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1

    sget-object v0, Lco/uk/getmondo/api/model/identity_verification/LegacyKyc$Status$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lco/uk/getmondo/api/model/identity_verification/LegacyKyc$Status;->KYC_PASSED_ENHANCED:Lco/uk/getmondo/api/model/identity_verification/LegacyKyc$Status;

    invoke-virtual {v1}, Lco/uk/getmondo/api/model/identity_verification/LegacyKyc$Status;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1

    sget-object v0, Lco/uk/getmondo/api/model/identity_verification/LegacyKyc$Status$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lco/uk/getmondo/api/model/identity_verification/LegacyKyc$Status;->KYC_FAILED:Lco/uk/getmondo/api/model/identity_verification/LegacyKyc$Status;

    invoke-virtual {v1}, Lco/uk/getmondo/api/model/identity_verification/LegacyKyc$Status;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1

    sget-object v0, Lco/uk/getmondo/api/model/identity_verification/LegacyKyc$Status$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lco/uk/getmondo/api/model/identity_verification/LegacyKyc$Status;->KYC_BLOCKED:Lco/uk/getmondo/api/model/identity_verification/LegacyKyc$Status;

    invoke-virtual {v1}, Lco/uk/getmondo/api/model/identity_verification/LegacyKyc$Status;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1

    return-void
.end method
