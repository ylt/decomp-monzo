.class public final enum Lco/uk/getmondo/api/model/identity_verification/ContentType;
.super Ljava/lang/Enum;
.source "ContentType.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lco/uk/getmondo/api/model/identity_verification/ContentType$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lco/uk/getmondo/api/model/identity_verification/ContentType;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\t\u0008\u0086\u0001\u0018\u0000 \u000b2\u0008\u0012\u0004\u0012\u00020\u00000\u0001:\u0001\u000bB\u000f\u0008\u0002\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0008\u0010\u0007\u001a\u00020\u0003H\u0016R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0005\u0010\u0006j\u0002\u0008\u0008j\u0002\u0008\tj\u0002\u0008\n\u00a8\u0006\u000c"
    }
    d2 = {
        "Lco/uk/getmondo/api/model/identity_verification/ContentType;",
        "",
        "mimeType",
        "",
        "(Ljava/lang/String;ILjava/lang/String;)V",
        "getMimeType",
        "()Ljava/lang/String;",
        "toString",
        "IMAGE_PNG",
        "IMAGE_JPEG",
        "VIDEO_MP4",
        "Companion",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lco/uk/getmondo/api/model/identity_verification/ContentType;

.field public static final Companion:Lco/uk/getmondo/api/model/identity_verification/ContentType$Companion;

.field public static final enum IMAGE_JPEG:Lco/uk/getmondo/api/model/identity_verification/ContentType;

.field public static final enum IMAGE_PNG:Lco/uk/getmondo/api/model/identity_verification/ContentType;

.field public static final enum VIDEO_MP4:Lco/uk/getmondo/api/model/identity_verification/ContentType;


# instance fields
.field private final mimeType:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    const/4 v0, 0x3

    new-array v0, v0, [Lco/uk/getmondo/api/model/identity_verification/ContentType;

    new-instance v1, Lco/uk/getmondo/api/model/identity_verification/ContentType;

    const-string v2, "IMAGE_PNG"

    .line 4
    const-string v3, "image/png"

    invoke-direct {v1, v2, v4, v3}, Lco/uk/getmondo/api/model/identity_verification/ContentType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lco/uk/getmondo/api/model/identity_verification/ContentType;->IMAGE_PNG:Lco/uk/getmondo/api/model/identity_verification/ContentType;

    aput-object v1, v0, v4

    new-instance v1, Lco/uk/getmondo/api/model/identity_verification/ContentType;

    const-string v2, "IMAGE_JPEG"

    .line 5
    const-string v3, "image/jpeg"

    invoke-direct {v1, v2, v5, v3}, Lco/uk/getmondo/api/model/identity_verification/ContentType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lco/uk/getmondo/api/model/identity_verification/ContentType;->IMAGE_JPEG:Lco/uk/getmondo/api/model/identity_verification/ContentType;

    aput-object v1, v0, v5

    new-instance v1, Lco/uk/getmondo/api/model/identity_verification/ContentType;

    const-string v2, "VIDEO_MP4"

    .line 6
    const-string v3, "video/mp4"

    invoke-direct {v1, v2, v6, v3}, Lco/uk/getmondo/api/model/identity_verification/ContentType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lco/uk/getmondo/api/model/identity_verification/ContentType;->VIDEO_MP4:Lco/uk/getmondo/api/model/identity_verification/ContentType;

    aput-object v1, v0, v6

    sput-object v0, Lco/uk/getmondo/api/model/identity_verification/ContentType;->$VALUES:[Lco/uk/getmondo/api/model/identity_verification/ContentType;

    new-instance v0, Lco/uk/getmondo/api/model/identity_verification/ContentType$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lco/uk/getmondo/api/model/identity_verification/ContentType$Companion;-><init>(Lkotlin/d/b/i;)V

    sput-object v0, Lco/uk/getmondo/api/model/identity_verification/ContentType;->Companion:Lco/uk/getmondo/api/model/identity_verification/ContentType$Companion;

    return-void
.end method

.method protected constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    const-string v0, "mimeType"

    invoke-static {p3, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 3
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput-object p3, p0, Lco/uk/getmondo/api/model/identity_verification/ContentType;->mimeType:Ljava/lang/String;

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lco/uk/getmondo/api/model/identity_verification/ContentType;
    .locals 1

    const-class v0, Lco/uk/getmondo/api/model/identity_verification/ContentType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/api/model/identity_verification/ContentType;

    return-object v0
.end method

.method public static values()[Lco/uk/getmondo/api/model/identity_verification/ContentType;
    .locals 1

    sget-object v0, Lco/uk/getmondo/api/model/identity_verification/ContentType;->$VALUES:[Lco/uk/getmondo/api/model/identity_verification/ContentType;

    invoke-virtual {v0}, [Lco/uk/getmondo/api/model/identity_verification/ContentType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lco/uk/getmondo/api/model/identity_verification/ContentType;

    return-object v0
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 3
    iget-object v0, p0, Lco/uk/getmondo/api/model/identity_verification/ContentType;->mimeType:Ljava/lang/String;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 9
    iget-object v0, p0, Lco/uk/getmondo/api/model/identity_verification/ContentType;->mimeType:Ljava/lang/String;

    return-object v0
.end method
