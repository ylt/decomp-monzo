.class public final Lco/uk/getmondo/api/model/identity_verification/IdentityDocumentType$Companion;
.super Ljava/lang/Object;
.source "IdentityDocumentType.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lco/uk/getmondo/api/model/identity_verification/IdentityDocumentType;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\u0008\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u0014\u0010\u0003\u001a\u0004\u0018\u00010\u00042\u0008\u0010\u0005\u001a\u0004\u0018\u00010\u0006H\u0007\u00a8\u0006\u0007"
    }
    d2 = {
        "Lco/uk/getmondo/api/model/identity_verification/IdentityDocumentType$Companion;",
        "",
        "()V",
        "from",
        "Lco/uk/getmondo/api/model/identity_verification/IdentityDocumentType;",
        "apiValue",
        "",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/d/b/i;)V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Lco/uk/getmondo/api/model/identity_verification/IdentityDocumentType$Companion;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Lco/uk/getmondo/api/model/identity_verification/IdentityDocumentType;
    .locals 4

    .prologue
    .line 22
    invoke-static {}, Lco/uk/getmondo/api/model/identity_verification/IdentityDocumentType;->values()[Lco/uk/getmondo/api/model/identity_verification/IdentityDocumentType;

    move-result-object v0

    check-cast v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    move v3, v1

    :goto_0
    array-length v1, v0

    if-ge v3, v1, :cond_1

    aget-object v2, v0, v3

    move-object v1, v2

    check-cast v1, Lco/uk/getmondo/api/model/identity_verification/IdentityDocumentType;

    invoke-virtual {v1}, Lco/uk/getmondo/api/model/identity_verification/IdentityDocumentType;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, p1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    move-object v0, v2

    :goto_1
    check-cast v0, Lco/uk/getmondo/api/model/identity_verification/IdentityDocumentType;

    return-object v0

    :cond_0
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method
