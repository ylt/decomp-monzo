.class public final enum Lco/uk/getmondo/api/model/identity_verification/IdentityDocumentType;
.super Ljava/lang/Enum;
.source "IdentityDocumentType.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lco/uk/getmondo/api/model/identity_verification/IdentityDocumentType$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lco/uk/getmondo/api/model/identity_verification/IdentityDocumentType;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u000c\u0008\u0086\u0001\u0018\u0000 \u00122\u0008\u0012\u0004\u0012\u00020\u00000\u0001:\u0001\u0012B!\u0008\u0002\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0008\u0008\u0001\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\u0008J\u0008\u0010\u000e\u001a\u00020\u0003H\u0016R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\t\u0010\nR\u0011\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000b\u0010\u000cR\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0004\u0010\rj\u0002\u0008\u000fj\u0002\u0008\u0010j\u0002\u0008\u0011\u00a8\u0006\u0013"
    }
    d2 = {
        "Lco/uk/getmondo/api/model/identity_verification/IdentityDocumentType;",
        "",
        "apiValue",
        "",
        "isBackRequired",
        "",
        "displayNameRes",
        "",
        "(Ljava/lang/String;ILjava/lang/String;ZI)V",
        "getApiValue",
        "()Ljava/lang/String;",
        "getDisplayNameRes",
        "()I",
        "()Z",
        "toString",
        "PASSPORT",
        "DRIVING_LICENSE",
        "NATIONAL_ID",
        "Companion",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lco/uk/getmondo/api/model/identity_verification/IdentityDocumentType;

.field public static final Companion:Lco/uk/getmondo/api/model/identity_verification/IdentityDocumentType$Companion;

.field public static final enum DRIVING_LICENSE:Lco/uk/getmondo/api/model/identity_verification/IdentityDocumentType;

.field public static final enum NATIONAL_ID:Lco/uk/getmondo/api/model/identity_verification/IdentityDocumentType;

.field public static final enum PASSPORT:Lco/uk/getmondo/api/model/identity_verification/IdentityDocumentType;


# instance fields
.field private final apiValue:Ljava/lang/String;

.field private final displayNameRes:I

.field private final isBackRequired:Z


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x2

    const/4 v2, 0x0

    const/4 v7, 0x1

    const/4 v0, 0x3

    new-array v6, v0, [Lco/uk/getmondo/api/model/identity_verification/IdentityDocumentType;

    new-instance v0, Lco/uk/getmondo/api/model/identity_verification/IdentityDocumentType;

    const-string v1, "PASSPORT"

    .line 11
    const-string v3, "passport"

    const v5, 0x7f0a01d4

    move v4, v2

    invoke-direct/range {v0 .. v5}, Lco/uk/getmondo/api/model/identity_verification/IdentityDocumentType;-><init>(Ljava/lang/String;ILjava/lang/String;ZI)V

    sput-object v0, Lco/uk/getmondo/api/model/identity_verification/IdentityDocumentType;->PASSPORT:Lco/uk/getmondo/api/model/identity_verification/IdentityDocumentType;

    aput-object v0, v6, v2

    new-instance v0, Lco/uk/getmondo/api/model/identity_verification/IdentityDocumentType;

    const-string v1, "DRIVING_LICENSE"

    .line 12
    const-string v3, "driving_license"

    const v5, 0x7f0a01d2

    move v2, v7

    move v4, v7

    invoke-direct/range {v0 .. v5}, Lco/uk/getmondo/api/model/identity_verification/IdentityDocumentType;-><init>(Ljava/lang/String;ILjava/lang/String;ZI)V

    sput-object v0, Lco/uk/getmondo/api/model/identity_verification/IdentityDocumentType;->DRIVING_LICENSE:Lco/uk/getmondo/api/model/identity_verification/IdentityDocumentType;

    aput-object v0, v6, v7

    new-instance v0, Lco/uk/getmondo/api/model/identity_verification/IdentityDocumentType;

    const-string v1, "NATIONAL_ID"

    .line 13
    const-string v3, "national_identity_card"

    const v5, 0x7f0a01d3

    move v2, v8

    move v4, v7

    invoke-direct/range {v0 .. v5}, Lco/uk/getmondo/api/model/identity_verification/IdentityDocumentType;-><init>(Ljava/lang/String;ILjava/lang/String;ZI)V

    sput-object v0, Lco/uk/getmondo/api/model/identity_verification/IdentityDocumentType;->NATIONAL_ID:Lco/uk/getmondo/api/model/identity_verification/IdentityDocumentType;

    aput-object v0, v6, v8

    sput-object v6, Lco/uk/getmondo/api/model/identity_verification/IdentityDocumentType;->$VALUES:[Lco/uk/getmondo/api/model/identity_verification/IdentityDocumentType;

    new-instance v0, Lco/uk/getmondo/api/model/identity_verification/IdentityDocumentType$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lco/uk/getmondo/api/model/identity_verification/IdentityDocumentType$Companion;-><init>(Lkotlin/d/b/i;)V

    sput-object v0, Lco/uk/getmondo/api/model/identity_verification/IdentityDocumentType;->Companion:Lco/uk/getmondo/api/model/identity_verification/IdentityDocumentType$Companion;

    return-void
.end method

.method protected constructor <init>(Ljava/lang/String;ILjava/lang/String;ZI)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "ZI)V"
        }
    .end annotation

    .prologue
    const-string v0, "apiValue"

    invoke-static {p3, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 6
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput-object p3, p0, Lco/uk/getmondo/api/model/identity_verification/IdentityDocumentType;->apiValue:Ljava/lang/String;

    iput-boolean p4, p0, Lco/uk/getmondo/api/model/identity_verification/IdentityDocumentType;->isBackRequired:Z

    iput p5, p0, Lco/uk/getmondo/api/model/identity_verification/IdentityDocumentType;->displayNameRes:I

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lco/uk/getmondo/api/model/identity_verification/IdentityDocumentType;
    .locals 1

    const-class v0, Lco/uk/getmondo/api/model/identity_verification/IdentityDocumentType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/api/model/identity_verification/IdentityDocumentType;

    return-object v0
.end method

.method public static values()[Lco/uk/getmondo/api/model/identity_verification/IdentityDocumentType;
    .locals 1

    sget-object v0, Lco/uk/getmondo/api/model/identity_verification/IdentityDocumentType;->$VALUES:[Lco/uk/getmondo/api/model/identity_verification/IdentityDocumentType;

    invoke-virtual {v0}, [Lco/uk/getmondo/api/model/identity_verification/IdentityDocumentType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lco/uk/getmondo/api/model/identity_verification/IdentityDocumentType;

    return-object v0
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 7
    iget-object v0, p0, Lco/uk/getmondo/api/model/identity_verification/IdentityDocumentType;->apiValue:Ljava/lang/String;

    return-object v0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 8
    iget-boolean v0, p0, Lco/uk/getmondo/api/model/identity_verification/IdentityDocumentType;->isBackRequired:Z

    return v0
.end method

.method public final c()I
    .locals 1

    .prologue
    .line 9
    iget v0, p0, Lco/uk/getmondo/api/model/identity_verification/IdentityDocumentType;->displayNameRes:I

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 16
    iget-object v0, p0, Lco/uk/getmondo/api/model/identity_verification/IdentityDocumentType;->apiValue:Ljava/lang/String;

    return-object v0
.end method
