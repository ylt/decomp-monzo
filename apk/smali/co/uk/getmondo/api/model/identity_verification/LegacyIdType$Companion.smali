.class public final Lco/uk/getmondo/api/model/identity_verification/LegacyIdType$Companion;
.super Ljava/lang/Object;
.source "LegacyIdType.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lco/uk/getmondo/api/model/identity_verification/LegacyIdType;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0000\u0008\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u0010\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006H\u0007J\u0010\u0010\u0007\u001a\u00020\u00042\u0006\u0010\u0008\u001a\u00020\tH\u0007\u00a8\u0006\n"
    }
    d2 = {
        "Lco/uk/getmondo/api/model/identity_verification/LegacyIdType$Companion;",
        "",
        "()V",
        "from",
        "Lco/uk/getmondo/api/model/identity_verification/LegacyIdType;",
        "identityDocumentType",
        "Lco/uk/getmondo/api/model/identity_verification/IdentityDocumentType;",
        "get",
        "ordinal",
        "",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/d/b/i;)V
    .locals 0

    .prologue
    .line 9
    invoke-direct {p0}, Lco/uk/getmondo/api/model/identity_verification/LegacyIdType$Companion;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lco/uk/getmondo/api/model/identity_verification/IdentityDocumentType;)Lco/uk/getmondo/api/model/identity_verification/LegacyIdType;
    .locals 2

    .prologue
    const-string v0, "identityDocumentType"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 18
    sget-object v0, Lco/uk/getmondo/api/model/identity_verification/LegacyIdType$Companion$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {p1}, Lco/uk/getmondo/api/model/identity_verification/IdentityDocumentType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 21
    new-instance v0, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {v0}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw v0

    .line 19
    :pswitch_0
    sget-object v0, Lco/uk/getmondo/api/model/identity_verification/LegacyIdType;->PASSPORT:Lco/uk/getmondo/api/model/identity_verification/LegacyIdType;

    .line 18
    :goto_0
    return-object v0

    .line 20
    :pswitch_1
    sget-object v0, Lco/uk/getmondo/api/model/identity_verification/LegacyIdType;->DRIVING_LICENSE:Lco/uk/getmondo/api/model/identity_verification/LegacyIdType;

    goto :goto_0

    .line 21
    :pswitch_2
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Cannot support national ID in legacy"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    .line 18
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
