.class public final Lco/uk/getmondo/api/model/identity_verification/IdentityVerification;
.super Ljava/lang/Object;
.source "IdentityVerification.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lco/uk/getmondo/api/model/identity_verification/IdentityVerification$Status;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u001a\n\u0002\u0010\u0008\n\u0002\u0008\u0003\u0008\u0086\u0008\u0018\u00002\u00020\u0001:\u0001%BO\u0012\n\u0008\u0002\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u0012\n\u0008\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u0003\u0012\u0008\u0010\u0005\u001a\u0004\u0018\u00010\u0006\u0012\u0008\u0008\u0002\u0010\u0007\u001a\u00020\u0008\u0012\u0008\u0008\u0002\u0010\t\u001a\u00020\u0008\u0012\u0008\u0008\u0002\u0010\n\u001a\u00020\u0008\u0012\u0008\u0008\u0002\u0010\u000b\u001a\u00020\u0008\u00a2\u0006\u0002\u0010\u000cJ\u000b\u0010\u0018\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003J\u000b\u0010\u0019\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003J\u000b\u0010\u001a\u001a\u0004\u0018\u00010\u0006H\u00c6\u0003J\t\u0010\u001b\u001a\u00020\u0008H\u00c6\u0003J\t\u0010\u001c\u001a\u00020\u0008H\u00c6\u0003J\t\u0010\u001d\u001a\u00020\u0008H\u00c6\u0003J\t\u0010\u001e\u001a\u00020\u0008H\u00c6\u0003JU\u0010\u001f\u001a\u00020\u00002\n\u0008\u0002\u0010\u0002\u001a\u0004\u0018\u00010\u00032\n\u0008\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u00032\n\u0008\u0002\u0010\u0005\u001a\u0004\u0018\u00010\u00062\u0008\u0008\u0002\u0010\u0007\u001a\u00020\u00082\u0008\u0008\u0002\u0010\t\u001a\u00020\u00082\u0008\u0008\u0002\u0010\n\u001a\u00020\u00082\u0008\u0008\u0002\u0010\u000b\u001a\u00020\u0008H\u00c6\u0001J\u0013\u0010 \u001a\u00020\u00082\u0008\u0010!\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u0010\"\u001a\u00020#H\u00d6\u0001J\t\u0010$\u001a\u00020\u0003H\u00d6\u0001R\u0011\u0010\n\u001a\u00020\u0008\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\r\u0010\u000eR\u0011\u0010\u000b\u001a\u00020\u0008\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000f\u0010\u000eR\u0011\u0010\u0007\u001a\u00020\u0008\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0010\u0010\u000eR\u0011\u0010\u0011\u001a\u00020\u00088F\u00a2\u0006\u0006\u001a\u0004\u0008\u0011\u0010\u000eR\u0013\u0010\u0004\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0012\u0010\u0013R\u0013\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0014\u0010\u0013R\u0011\u0010\t\u001a\u00020\u0008\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0015\u0010\u000eR\u0013\u0010\u0005\u001a\u0004\u0018\u00010\u0006\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0016\u0010\u0017\u00a8\u0006&"
    }
    d2 = {
        "Lco/uk/getmondo/api/model/identity_verification/IdentityVerification;",
        "",
        "rejectionReasonCode",
        "",
        "rejectionNote",
        "status",
        "Lco/uk/getmondo/api/model/identity_verification/IdentityVerification$Status;",
        "identityDocUploaded",
        "",
        "selfieUploaded",
        "allowPicker",
        "allowSystemCamera",
        "(Ljava/lang/String;Ljava/lang/String;Lco/uk/getmondo/api/model/identity_verification/IdentityVerification$Status;ZZZZ)V",
        "getAllowPicker",
        "()Z",
        "getAllowSystemCamera",
        "getIdentityDocUploaded",
        "isVerified",
        "getRejectionNote",
        "()Ljava/lang/String;",
        "getRejectionReasonCode",
        "getSelfieUploaded",
        "getStatus",
        "()Lco/uk/getmondo/api/model/identity_verification/IdentityVerification$Status;",
        "component1",
        "component2",
        "component3",
        "component4",
        "component5",
        "component6",
        "component7",
        "copy",
        "equals",
        "other",
        "hashCode",
        "",
        "toString",
        "Status",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field private final allowPicker:Z

.field private final allowSystemCamera:Z

.field private final identityDocUploaded:Z

.field private final rejectionNote:Ljava/lang/String;

.field private final rejectionReasonCode:Ljava/lang/String;

.field private final selfieUploaded:Z

.field private final status:Lco/uk/getmondo/api/model/identity_verification/IdentityVerification$Status;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Lco/uk/getmondo/api/model/identity_verification/IdentityVerification$Status;ZZZZ)V
    .locals 0

    .prologue
    .line 5
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lco/uk/getmondo/api/model/identity_verification/IdentityVerification;->rejectionReasonCode:Ljava/lang/String;

    iput-object p2, p0, Lco/uk/getmondo/api/model/identity_verification/IdentityVerification;->rejectionNote:Ljava/lang/String;

    iput-object p3, p0, Lco/uk/getmondo/api/model/identity_verification/IdentityVerification;->status:Lco/uk/getmondo/api/model/identity_verification/IdentityVerification$Status;

    iput-boolean p4, p0, Lco/uk/getmondo/api/model/identity_verification/IdentityVerification;->identityDocUploaded:Z

    iput-boolean p5, p0, Lco/uk/getmondo/api/model/identity_verification/IdentityVerification;->selfieUploaded:Z

    iput-boolean p6, p0, Lco/uk/getmondo/api/model/identity_verification/IdentityVerification;->allowPicker:Z

    iput-boolean p7, p0, Lco/uk/getmondo/api/model/identity_verification/IdentityVerification;->allowSystemCamera:Z

    return-void
.end method

.method public synthetic constructor <init>(Ljava/lang/String;Ljava/lang/String;Lco/uk/getmondo/api/model/identity_verification/IdentityVerification$Status;ZZZZILkotlin/d/b/i;)V
    .locals 8

    .prologue
    and-int/lit8 v0, p8, 0x1

    if-eqz v0, :cond_5

    .line 6
    const/4 v0, 0x0

    check-cast v0, Ljava/lang/String;

    move-object v1, v0

    :goto_0
    and-int/lit8 v0, p8, 0x2

    if-eqz v0, :cond_4

    .line 7
    const/4 v0, 0x0

    check-cast v0, Ljava/lang/String;

    move-object v2, v0

    :goto_1
    and-int/lit8 v0, p8, 0x8

    if-eqz v0, :cond_3

    .line 9
    const/4 v4, 0x0

    :goto_2
    and-int/lit8 v0, p8, 0x10

    if-eqz v0, :cond_2

    .line 10
    const/4 v5, 0x0

    :goto_3
    and-int/lit8 v0, p8, 0x20

    if-eqz v0, :cond_1

    .line 11
    const/4 v6, 0x0

    :goto_4
    and-int/lit8 v0, p8, 0x40

    if-eqz v0, :cond_0

    .line 12
    const/4 v7, 0x0

    :goto_5
    move-object v0, p0

    move-object v3, p3

    invoke-direct/range {v0 .. v7}, Lco/uk/getmondo/api/model/identity_verification/IdentityVerification;-><init>(Ljava/lang/String;Ljava/lang/String;Lco/uk/getmondo/api/model/identity_verification/IdentityVerification$Status;ZZZZ)V

    return-void

    :cond_0
    move v7, p7

    goto :goto_5

    :cond_1
    move v6, p6

    goto :goto_4

    :cond_2
    move v5, p5

    goto :goto_3

    :cond_3
    move v4, p4

    goto :goto_2

    :cond_4
    move-object v2, p2

    goto :goto_1

    :cond_5
    move-object v1, p1

    goto :goto_0
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 7
    iget-object v0, p0, Lco/uk/getmondo/api/model/identity_verification/IdentityVerification;->rejectionNote:Ljava/lang/String;

    return-object v0
.end method

.method public final b()Lco/uk/getmondo/api/model/identity_verification/IdentityVerification$Status;
    .locals 1

    .prologue
    .line 8
    iget-object v0, p0, Lco/uk/getmondo/api/model/identity_verification/IdentityVerification;->status:Lco/uk/getmondo/api/model/identity_verification/IdentityVerification$Status;

    return-object v0
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 12
    iget-boolean v0, p0, Lco/uk/getmondo/api/model/identity_verification/IdentityVerification;->allowSystemCamera:Z

    return v0
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lco/uk/getmondo/api/model/identity_verification/IdentityVerification;->rejectionNote:Ljava/lang/String;

    return-object v0
.end method

.method public final e()Lco/uk/getmondo/api/model/identity_verification/IdentityVerification$Status;
    .locals 1

    iget-object v0, p0, Lco/uk/getmondo/api/model/identity_verification/IdentityVerification;->status:Lco/uk/getmondo/api/model/identity_verification/IdentityVerification$Status;

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v1, 0x1

    const/4 v0, 0x0

    if-eq p0, p1, :cond_0

    instance-of v2, p1, Lco/uk/getmondo/api/model/identity_verification/IdentityVerification;

    if-eqz v2, :cond_1

    check-cast p1, Lco/uk/getmondo/api/model/identity_verification/IdentityVerification;

    iget-object v2, p0, Lco/uk/getmondo/api/model/identity_verification/IdentityVerification;->rejectionReasonCode:Ljava/lang/String;

    iget-object v3, p1, Lco/uk/getmondo/api/model/identity_verification/IdentityVerification;->rejectionReasonCode:Ljava/lang/String;

    invoke-static {v2, v3}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lco/uk/getmondo/api/model/identity_verification/IdentityVerification;->rejectionNote:Ljava/lang/String;

    iget-object v3, p1, Lco/uk/getmondo/api/model/identity_verification/IdentityVerification;->rejectionNote:Ljava/lang/String;

    invoke-static {v2, v3}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lco/uk/getmondo/api/model/identity_verification/IdentityVerification;->status:Lco/uk/getmondo/api/model/identity_verification/IdentityVerification$Status;

    iget-object v3, p1, Lco/uk/getmondo/api/model/identity_verification/IdentityVerification;->status:Lco/uk/getmondo/api/model/identity_verification/IdentityVerification$Status;

    invoke-static {v2, v3}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    iget-boolean v2, p0, Lco/uk/getmondo/api/model/identity_verification/IdentityVerification;->identityDocUploaded:Z

    iget-boolean v3, p1, Lco/uk/getmondo/api/model/identity_verification/IdentityVerification;->identityDocUploaded:Z

    if-ne v2, v3, :cond_2

    move v2, v1

    :goto_0
    if-eqz v2, :cond_1

    iget-boolean v2, p0, Lco/uk/getmondo/api/model/identity_verification/IdentityVerification;->selfieUploaded:Z

    iget-boolean v3, p1, Lco/uk/getmondo/api/model/identity_verification/IdentityVerification;->selfieUploaded:Z

    if-ne v2, v3, :cond_3

    move v2, v1

    :goto_1
    if-eqz v2, :cond_1

    iget-boolean v2, p0, Lco/uk/getmondo/api/model/identity_verification/IdentityVerification;->allowPicker:Z

    iget-boolean v3, p1, Lco/uk/getmondo/api/model/identity_verification/IdentityVerification;->allowPicker:Z

    if-ne v2, v3, :cond_4

    move v2, v1

    :goto_2
    if-eqz v2, :cond_1

    iget-boolean v2, p0, Lco/uk/getmondo/api/model/identity_verification/IdentityVerification;->allowSystemCamera:Z

    iget-boolean v3, p1, Lco/uk/getmondo/api/model/identity_verification/IdentityVerification;->allowSystemCamera:Z

    if-ne v2, v3, :cond_5

    move v2, v1

    :goto_3
    if-eqz v2, :cond_1

    :cond_0
    move v0, v1

    :cond_1
    return v0

    :cond_2
    move v2, v0

    goto :goto_0

    :cond_3
    move v2, v0

    goto :goto_1

    :cond_4
    move v2, v0

    goto :goto_2

    :cond_5
    move v2, v0

    goto :goto_3
.end method

.method public hashCode()I
    .locals 4

    const/4 v1, 0x0

    const/4 v2, 0x1

    iget-object v0, p0, Lco/uk/getmondo/api/model/identity_verification/IdentityVerification;->rejectionReasonCode:Ljava/lang/String;

    if-eqz v0, :cond_4

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    :goto_0
    mul-int/lit8 v3, v0, 0x1f

    iget-object v0, p0, Lco/uk/getmondo/api/model/identity_verification/IdentityVerification;->rejectionNote:Ljava/lang/String;

    if-eqz v0, :cond_5

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    :goto_1
    add-int/2addr v0, v3

    mul-int/lit8 v0, v0, 0x1f

    iget-object v3, p0, Lco/uk/getmondo/api/model/identity_verification/IdentityVerification;->status:Lco/uk/getmondo/api/model/identity_verification/IdentityVerification$Status;

    if-eqz v3, :cond_0

    invoke-virtual {v3}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_0
    add-int/2addr v0, v1

    mul-int/lit8 v1, v0, 0x1f

    iget-boolean v0, p0, Lco/uk/getmondo/api/model/identity_verification/IdentityVerification;->identityDocUploaded:Z

    if-eqz v0, :cond_1

    move v0, v2

    :cond_1
    add-int/2addr v0, v1

    mul-int/lit8 v1, v0, 0x1f

    iget-boolean v0, p0, Lco/uk/getmondo/api/model/identity_verification/IdentityVerification;->selfieUploaded:Z

    if-eqz v0, :cond_2

    move v0, v2

    :cond_2
    add-int/2addr v0, v1

    mul-int/lit8 v1, v0, 0x1f

    iget-boolean v0, p0, Lco/uk/getmondo/api/model/identity_verification/IdentityVerification;->allowPicker:Z

    if-eqz v0, :cond_3

    move v0, v2

    :cond_3
    add-int/2addr v0, v1

    mul-int/lit8 v1, v0, 0x1f

    iget-boolean v0, p0, Lco/uk/getmondo/api/model/identity_verification/IdentityVerification;->allowSystemCamera:Z

    if-eqz v0, :cond_6

    :goto_2
    add-int v0, v1, v2

    return v0

    :cond_4
    move v0, v1

    goto :goto_0

    :cond_5
    move v0, v1

    goto :goto_1

    :cond_6
    move v2, v0

    goto :goto_2
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "IdentityVerification(rejectionReasonCode="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/api/model/identity_verification/IdentityVerification;->rejectionReasonCode:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", rejectionNote="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/api/model/identity_verification/IdentityVerification;->rejectionNote:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", status="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/api/model/identity_verification/IdentityVerification;->status:Lco/uk/getmondo/api/model/identity_verification/IdentityVerification$Status;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", identityDocUploaded="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lco/uk/getmondo/api/model/identity_verification/IdentityVerification;->identityDocUploaded:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", selfieUploaded="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lco/uk/getmondo/api/model/identity_verification/IdentityVerification;->selfieUploaded:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", allowPicker="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lco/uk/getmondo/api/model/identity_verification/IdentityVerification;->allowPicker:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", allowSystemCamera="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lco/uk/getmondo/api/model/identity_verification/IdentityVerification;->allowSystemCamera:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
