.class public final enum Lco/uk/getmondo/api/model/identity_verification/LegacyKyc$Status;
.super Ljava/lang/Enum;
.source "LegacyKyc.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lco/uk/getmondo/api/model/identity_verification/LegacyKyc;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Status"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lco/uk/getmondo/api/model/identity_verification/LegacyKyc$Status;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0006\u0008\u0086\u0001\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00000\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u0008\u0010\u0003\u001a\u0004\u0018\u00010\u0004j\u0002\u0008\u0005j\u0002\u0008\u0006j\u0002\u0008\u0007j\u0002\u0008\u0008j\u0002\u0008\t\u00a8\u0006\n"
    }
    d2 = {
        "Lco/uk/getmondo/api/model/identity_verification/LegacyKyc$Status;",
        "",
        "(Ljava/lang/String;I)V",
        "toIdentityVerificationStatus",
        "Lco/uk/getmondo/api/model/identity_verification/IdentityVerification$Status;",
        "KYC_UNKNOWN",
        "KYC_PASSED",
        "KYC_FAILED",
        "KYC_BLOCKED",
        "KYC_PASSED_ENHANCED",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lco/uk/getmondo/api/model/identity_verification/LegacyKyc$Status;

.field public static final enum KYC_BLOCKED:Lco/uk/getmondo/api/model/identity_verification/LegacyKyc$Status;

.field public static final enum KYC_FAILED:Lco/uk/getmondo/api/model/identity_verification/LegacyKyc$Status;

.field public static final enum KYC_PASSED:Lco/uk/getmondo/api/model/identity_verification/LegacyKyc$Status;

.field public static final enum KYC_PASSED_ENHANCED:Lco/uk/getmondo/api/model/identity_verification/LegacyKyc$Status;

.field public static final enum KYC_UNKNOWN:Lco/uk/getmondo/api/model/identity_verification/LegacyKyc$Status;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    const/4 v0, 0x5

    new-array v0, v0, [Lco/uk/getmondo/api/model/identity_verification/LegacyKyc$Status;

    new-instance v1, Lco/uk/getmondo/api/model/identity_verification/LegacyKyc$Status;

    const-string v2, "KYC_UNKNOWN"

    invoke-direct {v1, v2, v3}, Lco/uk/getmondo/api/model/identity_verification/LegacyKyc$Status;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lco/uk/getmondo/api/model/identity_verification/LegacyKyc$Status;->KYC_UNKNOWN:Lco/uk/getmondo/api/model/identity_verification/LegacyKyc$Status;

    aput-object v1, v0, v3

    new-instance v1, Lco/uk/getmondo/api/model/identity_verification/LegacyKyc$Status;

    const-string v2, "KYC_PASSED"

    invoke-direct {v1, v2, v4}, Lco/uk/getmondo/api/model/identity_verification/LegacyKyc$Status;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lco/uk/getmondo/api/model/identity_verification/LegacyKyc$Status;->KYC_PASSED:Lco/uk/getmondo/api/model/identity_verification/LegacyKyc$Status;

    aput-object v1, v0, v4

    new-instance v1, Lco/uk/getmondo/api/model/identity_verification/LegacyKyc$Status;

    const-string v2, "KYC_FAILED"

    invoke-direct {v1, v2, v5}, Lco/uk/getmondo/api/model/identity_verification/LegacyKyc$Status;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lco/uk/getmondo/api/model/identity_verification/LegacyKyc$Status;->KYC_FAILED:Lco/uk/getmondo/api/model/identity_verification/LegacyKyc$Status;

    aput-object v1, v0, v5

    new-instance v1, Lco/uk/getmondo/api/model/identity_verification/LegacyKyc$Status;

    const-string v2, "KYC_BLOCKED"

    invoke-direct {v1, v2, v6}, Lco/uk/getmondo/api/model/identity_verification/LegacyKyc$Status;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lco/uk/getmondo/api/model/identity_verification/LegacyKyc$Status;->KYC_BLOCKED:Lco/uk/getmondo/api/model/identity_verification/LegacyKyc$Status;

    aput-object v1, v0, v6

    new-instance v1, Lco/uk/getmondo/api/model/identity_verification/LegacyKyc$Status;

    const-string v2, "KYC_PASSED_ENHANCED"

    invoke-direct {v1, v2, v7}, Lco/uk/getmondo/api/model/identity_verification/LegacyKyc$Status;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lco/uk/getmondo/api/model/identity_verification/LegacyKyc$Status;->KYC_PASSED_ENHANCED:Lco/uk/getmondo/api/model/identity_verification/LegacyKyc$Status;

    aput-object v1, v0, v7

    sput-object v0, Lco/uk/getmondo/api/model/identity_verification/LegacyKyc$Status;->$VALUES:[Lco/uk/getmondo/api/model/identity_verification/LegacyKyc$Status;

    return-void
.end method

.method protected constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 9
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lco/uk/getmondo/api/model/identity_verification/LegacyKyc$Status;
    .locals 1

    const-class v0, Lco/uk/getmondo/api/model/identity_verification/LegacyKyc$Status;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/api/model/identity_verification/LegacyKyc$Status;

    return-object v0
.end method

.method public static values()[Lco/uk/getmondo/api/model/identity_verification/LegacyKyc$Status;
    .locals 1

    sget-object v0, Lco/uk/getmondo/api/model/identity_verification/LegacyKyc$Status;->$VALUES:[Lco/uk/getmondo/api/model/identity_verification/LegacyKyc$Status;

    invoke-virtual {v0}, [Lco/uk/getmondo/api/model/identity_verification/LegacyKyc$Status;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lco/uk/getmondo/api/model/identity_verification/LegacyKyc$Status;

    return-object v0
.end method


# virtual methods
.method public final a()Lco/uk/getmondo/api/model/identity_verification/IdentityVerification$Status;
    .locals 2

    .prologue
    .line 17
    sget-object v0, Lco/uk/getmondo/api/model/identity_verification/LegacyKyc$Status$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {p0}, Lco/uk/getmondo/api/model/identity_verification/LegacyKyc$Status;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 21
    const/4 v0, 0x0

    .line 17
    :goto_0
    return-object v0

    .line 18
    :pswitch_0
    sget-object v0, Lco/uk/getmondo/api/model/identity_verification/IdentityVerification$Status;->APPROVED:Lco/uk/getmondo/api/model/identity_verification/IdentityVerification$Status;

    goto :goto_0

    .line 19
    :pswitch_1
    sget-object v0, Lco/uk/getmondo/api/model/identity_verification/IdentityVerification$Status;->PENDING_SUBMISSION:Lco/uk/getmondo/api/model/identity_verification/IdentityVerification$Status;

    goto :goto_0

    .line 20
    :pswitch_2
    sget-object v0, Lco/uk/getmondo/api/model/identity_verification/IdentityVerification$Status;->BLOCKED:Lco/uk/getmondo/api/model/identity_verification/IdentityVerification$Status;

    goto :goto_0

    .line 17
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
