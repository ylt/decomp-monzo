.class public final Lco/uk/getmondo/api/model/identity_verification/LegacyKyc;
.super Ljava/lang/Object;
.source "LegacyKyc.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lco/uk/getmondo/api/model/identity_verification/LegacyKyc$Status;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000,\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u000e\n\u0002\u0010\u0008\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0002\u0008\u0086\u0008\u0018\u00002\u00020\u0001:\u0001\u0019B\u001d\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0007J\t\u0010\r\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u000e\u001a\u00020\u0005H\u00c6\u0003J\t\u0010\u000f\u001a\u00020\u0005H\u00c6\u0003J\'\u0010\u0010\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u00052\u0008\u0008\u0002\u0010\u0006\u001a\u00020\u0005H\u00c6\u0001J\u0013\u0010\u0011\u001a\u00020\u00052\u0008\u0010\u0012\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u0010\u0013\u001a\u00020\u0014H\u00d6\u0001J\u0006\u0010\u0015\u001a\u00020\u0016J\t\u0010\u0017\u001a\u00020\u0018H\u00d6\u0001R\u0011\u0010\u0006\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0008\u0010\tR\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\n\u0010\tR\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000b\u0010\u000c\u00a8\u0006\u001a"
    }
    d2 = {
        "Lco/uk/getmondo/api/model/identity_verification/LegacyKyc;",
        "",
        "status",
        "Lco/uk/getmondo/api/model/identity_verification/LegacyKyc$Status;",
        "evidenceSubmitted",
        "",
        "evidenceReviewed",
        "(Lco/uk/getmondo/api/model/identity_verification/LegacyKyc$Status;ZZ)V",
        "getEvidenceReviewed",
        "()Z",
        "getEvidenceSubmitted",
        "getStatus",
        "()Lco/uk/getmondo/api/model/identity_verification/LegacyKyc$Status;",
        "component1",
        "component2",
        "component3",
        "copy",
        "equals",
        "other",
        "hashCode",
        "",
        "toIdentityVerification",
        "Lco/uk/getmondo/api/model/identity_verification/IdentityVerification;",
        "toString",
        "",
        "Status",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field private final evidenceReviewed:Z

.field private final evidenceSubmitted:Z

.field private final status:Lco/uk/getmondo/api/model/identity_verification/LegacyKyc$Status;


# direct methods
.method public constructor <init>(Lco/uk/getmondo/api/model/identity_verification/LegacyKyc$Status;ZZ)V
    .locals 1

    .prologue
    const-string v0, "status"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 5
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lco/uk/getmondo/api/model/identity_verification/LegacyKyc;->status:Lco/uk/getmondo/api/model/identity_verification/LegacyKyc$Status;

    iput-boolean p2, p0, Lco/uk/getmondo/api/model/identity_verification/LegacyKyc;->evidenceSubmitted:Z

    iput-boolean p3, p0, Lco/uk/getmondo/api/model/identity_verification/LegacyKyc;->evidenceReviewed:Z

    return-void
.end method


# virtual methods
.method public final a()Lco/uk/getmondo/api/model/identity_verification/IdentityVerification;
    .locals 10

    .prologue
    const/4 v6, 0x0

    const/4 v1, 0x0

    .line 27
    new-instance v0, Lco/uk/getmondo/api/model/identity_verification/IdentityVerification;

    .line 28
    iget-object v2, p0, Lco/uk/getmondo/api/model/identity_verification/LegacyKyc;->status:Lco/uk/getmondo/api/model/identity_verification/LegacyKyc$Status;

    invoke-virtual {v2}, Lco/uk/getmondo/api/model/identity_verification/LegacyKyc$Status;->a()Lco/uk/getmondo/api/model/identity_verification/IdentityVerification$Status;

    move-result-object v3

    .line 29
    iget-boolean v4, p0, Lco/uk/getmondo/api/model/identity_verification/LegacyKyc;->evidenceSubmitted:Z

    .line 30
    iget-boolean v5, p0, Lco/uk/getmondo/api/model/identity_verification/LegacyKyc;->evidenceSubmitted:Z

    const/16 v8, 0x63

    move-object v2, v1

    move v7, v6

    move-object v9, v1

    .line 27
    invoke-direct/range {v0 .. v9}, Lco/uk/getmondo/api/model/identity_verification/IdentityVerification;-><init>(Ljava/lang/String;Ljava/lang/String;Lco/uk/getmondo/api/model/identity_verification/IdentityVerification$Status;ZZZZILkotlin/d/b/i;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v1, 0x1

    const/4 v0, 0x0

    if-eq p0, p1, :cond_0

    instance-of v2, p1, Lco/uk/getmondo/api/model/identity_verification/LegacyKyc;

    if-eqz v2, :cond_1

    check-cast p1, Lco/uk/getmondo/api/model/identity_verification/LegacyKyc;

    iget-object v2, p0, Lco/uk/getmondo/api/model/identity_verification/LegacyKyc;->status:Lco/uk/getmondo/api/model/identity_verification/LegacyKyc$Status;

    iget-object v3, p1, Lco/uk/getmondo/api/model/identity_verification/LegacyKyc;->status:Lco/uk/getmondo/api/model/identity_verification/LegacyKyc$Status;

    invoke-static {v2, v3}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    iget-boolean v2, p0, Lco/uk/getmondo/api/model/identity_verification/LegacyKyc;->evidenceSubmitted:Z

    iget-boolean v3, p1, Lco/uk/getmondo/api/model/identity_verification/LegacyKyc;->evidenceSubmitted:Z

    if-ne v2, v3, :cond_2

    move v2, v1

    :goto_0
    if-eqz v2, :cond_1

    iget-boolean v2, p0, Lco/uk/getmondo/api/model/identity_verification/LegacyKyc;->evidenceReviewed:Z

    iget-boolean v3, p1, Lco/uk/getmondo/api/model/identity_verification/LegacyKyc;->evidenceReviewed:Z

    if-ne v2, v3, :cond_3

    move v2, v1

    :goto_1
    if-eqz v2, :cond_1

    :cond_0
    move v0, v1

    :cond_1
    return v0

    :cond_2
    move v2, v0

    goto :goto_0

    :cond_3
    move v2, v0

    goto :goto_1
.end method

.method public hashCode()I
    .locals 3

    const/4 v1, 0x1

    iget-object v0, p0, Lco/uk/getmondo/api/model/identity_verification/LegacyKyc;->status:Lco/uk/getmondo/api/model/identity_verification/LegacyKyc$Status;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    :goto_0
    mul-int/lit8 v2, v0, 0x1f

    iget-boolean v0, p0, Lco/uk/getmondo/api/model/identity_verification/LegacyKyc;->evidenceSubmitted:Z

    if-eqz v0, :cond_0

    move v0, v1

    :cond_0
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-boolean v0, p0, Lco/uk/getmondo/api/model/identity_verification/LegacyKyc;->evidenceReviewed:Z

    if-eqz v0, :cond_2

    :goto_1
    add-int v0, v2, v1

    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :cond_2
    move v1, v0

    goto :goto_1
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "LegacyKyc(status="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/api/model/identity_verification/LegacyKyc;->status:Lco/uk/getmondo/api/model/identity_verification/LegacyKyc$Status;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", evidenceSubmitted="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lco/uk/getmondo/api/model/identity_verification/LegacyKyc;->evidenceSubmitted:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", evidenceReviewed="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lco/uk/getmondo/api/model/identity_verification/LegacyKyc;->evidenceReviewed:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
