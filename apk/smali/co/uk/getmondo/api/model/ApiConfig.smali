.class public final Lco/uk/getmondo/api/model/ApiConfig;
.super Ljava/lang/Object;
.source "ApiConfig.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lco/uk/getmondo/api/model/ApiConfig$P2p;,
        Lco/uk/getmondo/api/model/ApiConfig$MonzoMeUsername;,
        Lco/uk/getmondo/api/model/ApiConfig$FeatureFlags;,
        Lco/uk/getmondo/api/model/ApiConfig$InboundP2p;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u00008\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0017\n\u0002\u0010\u0008\n\u0002\u0008\u0006\u0008\u0086\u0008\u0018\u00002\u00020\u0001:\u0004\'()*B=\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0008\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u0012\n\u0008\u0002\u0010\n\u001a\u0004\u0018\u00010\u000b\u0012\u0008\u0008\u0002\u0010\u000c\u001a\u00020\r\u00a2\u0006\u0002\u0010\u000eJ\t\u0010\u001b\u001a\u00020\u0003H\u00c6\u0003J\u000b\u0010\u001c\u001a\u0004\u0018\u00010\u0005H\u00c6\u0003J\t\u0010\u001d\u001a\u00020\u0007H\u00c6\u0003J\t\u0010\u001e\u001a\u00020\tH\u00c6\u0003J\u000b\u0010\u001f\u001a\u0004\u0018\u00010\u000bH\u00c6\u0003J\t\u0010 \u001a\u00020\rH\u00c6\u0003JI\u0010!\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\n\u0008\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u00052\u0008\u0008\u0002\u0010\u0006\u001a\u00020\u00072\u0008\u0008\u0002\u0010\u0008\u001a\u00020\t2\n\u0008\u0002\u0010\n\u001a\u0004\u0018\u00010\u000b2\u0008\u0008\u0002\u0010\u000c\u001a\u00020\rH\u00c6\u0001J\u0013\u0010\"\u001a\u00020\r2\u0008\u0010#\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u0010$\u001a\u00020%H\u00d6\u0001J\t\u0010&\u001a\u00020\u000bH\u00d6\u0001R\u0011\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000f\u0010\u0010R\u0011\u0010\u0008\u001a\u00020\t\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0011\u0010\u0012R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0013\u0010\u0014R\u0011\u0010\u000c\u001a\u00020\r\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0015\u0010\u0016R\u0013\u0010\n\u001a\u0004\u0018\u00010\u000b\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0017\u0010\u0018R\u0013\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0019\u0010\u001a\u00a8\u0006+"
    }
    d2 = {
        "Lco/uk/getmondo/api/model/ApiConfig;",
        "",
        "p2p",
        "Lco/uk/getmondo/api/model/ApiConfig$P2p;",
        "username",
        "Lco/uk/getmondo/api/model/ApiConfig$MonzoMeUsername;",
        "featureFlags",
        "Lco/uk/getmondo/api/model/ApiConfig$FeatureFlags;",
        "inboundP2p",
        "Lco/uk/getmondo/api/model/ApiConfig$InboundP2p;",
        "sddSplashscreen",
        "",
        "prepaidAccountMigrated",
        "",
        "(Lco/uk/getmondo/api/model/ApiConfig$P2p;Lco/uk/getmondo/api/model/ApiConfig$MonzoMeUsername;Lco/uk/getmondo/api/model/ApiConfig$FeatureFlags;Lco/uk/getmondo/api/model/ApiConfig$InboundP2p;Ljava/lang/String;Z)V",
        "getFeatureFlags",
        "()Lco/uk/getmondo/api/model/ApiConfig$FeatureFlags;",
        "getInboundP2p",
        "()Lco/uk/getmondo/api/model/ApiConfig$InboundP2p;",
        "getP2p",
        "()Lco/uk/getmondo/api/model/ApiConfig$P2p;",
        "getPrepaidAccountMigrated",
        "()Z",
        "getSddSplashscreen",
        "()Ljava/lang/String;",
        "getUsername",
        "()Lco/uk/getmondo/api/model/ApiConfig$MonzoMeUsername;",
        "component1",
        "component2",
        "component3",
        "component4",
        "component5",
        "component6",
        "copy",
        "equals",
        "other",
        "hashCode",
        "",
        "toString",
        "FeatureFlags",
        "InboundP2p",
        "MonzoMeUsername",
        "P2p",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field private final featureFlags:Lco/uk/getmondo/api/model/ApiConfig$FeatureFlags;

.field private final inboundP2p:Lco/uk/getmondo/api/model/ApiConfig$InboundP2p;

.field private final p2p:Lco/uk/getmondo/api/model/ApiConfig$P2p;

.field private final prepaidAccountMigrated:Z

.field private final sddSplashscreen:Ljava/lang/String;

.field private final username:Lco/uk/getmondo/api/model/ApiConfig$MonzoMeUsername;


# direct methods
.method public constructor <init>(Lco/uk/getmondo/api/model/ApiConfig$P2p;Lco/uk/getmondo/api/model/ApiConfig$MonzoMeUsername;Lco/uk/getmondo/api/model/ApiConfig$FeatureFlags;Lco/uk/getmondo/api/model/ApiConfig$InboundP2p;Ljava/lang/String;Z)V
    .locals 1

    .prologue
    const-string v0, "p2p"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "featureFlags"

    invoke-static {p3, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "inboundP2p"

    invoke-static {p4, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lco/uk/getmondo/api/model/ApiConfig;->p2p:Lco/uk/getmondo/api/model/ApiConfig$P2p;

    iput-object p2, p0, Lco/uk/getmondo/api/model/ApiConfig;->username:Lco/uk/getmondo/api/model/ApiConfig$MonzoMeUsername;

    iput-object p3, p0, Lco/uk/getmondo/api/model/ApiConfig;->featureFlags:Lco/uk/getmondo/api/model/ApiConfig$FeatureFlags;

    iput-object p4, p0, Lco/uk/getmondo/api/model/ApiConfig;->inboundP2p:Lco/uk/getmondo/api/model/ApiConfig$InboundP2p;

    iput-object p5, p0, Lco/uk/getmondo/api/model/ApiConfig;->sddSplashscreen:Ljava/lang/String;

    iput-boolean p6, p0, Lco/uk/getmondo/api/model/ApiConfig;->prepaidAccountMigrated:Z

    return-void
.end method

.method public synthetic constructor <init>(Lco/uk/getmondo/api/model/ApiConfig$P2p;Lco/uk/getmondo/api/model/ApiConfig$MonzoMeUsername;Lco/uk/getmondo/api/model/ApiConfig$FeatureFlags;Lco/uk/getmondo/api/model/ApiConfig$InboundP2p;Ljava/lang/String;ZILkotlin/d/b/i;)V
    .locals 7

    .prologue
    and-int/lit8 v0, p7, 0x10

    if-eqz v0, :cond_1

    .line 11
    const/4 v0, 0x0

    check-cast v0, Ljava/lang/String;

    move-object v5, v0

    :goto_0
    and-int/lit8 v0, p7, 0x20

    if-eqz v0, :cond_0

    .line 12
    const/4 v6, 0x0

    :goto_1
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v6}, Lco/uk/getmondo/api/model/ApiConfig;-><init>(Lco/uk/getmondo/api/model/ApiConfig$P2p;Lco/uk/getmondo/api/model/ApiConfig$MonzoMeUsername;Lco/uk/getmondo/api/model/ApiConfig$FeatureFlags;Lco/uk/getmondo/api/model/ApiConfig$InboundP2p;Ljava/lang/String;Z)V

    return-void

    :cond_0
    move v6, p6

    goto :goto_1

    :cond_1
    move-object v5, p5

    goto :goto_0
.end method


# virtual methods
.method public final a()Lco/uk/getmondo/api/model/ApiConfig$P2p;
    .locals 1

    .prologue
    .line 7
    iget-object v0, p0, Lco/uk/getmondo/api/model/ApiConfig;->p2p:Lco/uk/getmondo/api/model/ApiConfig$P2p;

    return-object v0
.end method

.method public final b()Lco/uk/getmondo/api/model/ApiConfig$MonzoMeUsername;
    .locals 1

    .prologue
    .line 8
    iget-object v0, p0, Lco/uk/getmondo/api/model/ApiConfig;->username:Lco/uk/getmondo/api/model/ApiConfig$MonzoMeUsername;

    return-object v0
.end method

.method public final c()Lco/uk/getmondo/api/model/ApiConfig$FeatureFlags;
    .locals 1

    .prologue
    .line 9
    iget-object v0, p0, Lco/uk/getmondo/api/model/ApiConfig;->featureFlags:Lco/uk/getmondo/api/model/ApiConfig$FeatureFlags;

    return-object v0
.end method

.method public final d()Lco/uk/getmondo/api/model/ApiConfig$InboundP2p;
    .locals 1

    .prologue
    .line 10
    iget-object v0, p0, Lco/uk/getmondo/api/model/ApiConfig;->inboundP2p:Lco/uk/getmondo/api/model/ApiConfig$InboundP2p;

    return-object v0
.end method

.method public final e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 11
    iget-object v0, p0, Lco/uk/getmondo/api/model/ApiConfig;->sddSplashscreen:Ljava/lang/String;

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v1, 0x1

    const/4 v0, 0x0

    if-eq p0, p1, :cond_0

    instance-of v2, p1, Lco/uk/getmondo/api/model/ApiConfig;

    if-eqz v2, :cond_1

    check-cast p1, Lco/uk/getmondo/api/model/ApiConfig;

    iget-object v2, p0, Lco/uk/getmondo/api/model/ApiConfig;->p2p:Lco/uk/getmondo/api/model/ApiConfig$P2p;

    iget-object v3, p1, Lco/uk/getmondo/api/model/ApiConfig;->p2p:Lco/uk/getmondo/api/model/ApiConfig$P2p;

    invoke-static {v2, v3}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lco/uk/getmondo/api/model/ApiConfig;->username:Lco/uk/getmondo/api/model/ApiConfig$MonzoMeUsername;

    iget-object v3, p1, Lco/uk/getmondo/api/model/ApiConfig;->username:Lco/uk/getmondo/api/model/ApiConfig$MonzoMeUsername;

    invoke-static {v2, v3}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lco/uk/getmondo/api/model/ApiConfig;->featureFlags:Lco/uk/getmondo/api/model/ApiConfig$FeatureFlags;

    iget-object v3, p1, Lco/uk/getmondo/api/model/ApiConfig;->featureFlags:Lco/uk/getmondo/api/model/ApiConfig$FeatureFlags;

    invoke-static {v2, v3}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lco/uk/getmondo/api/model/ApiConfig;->inboundP2p:Lco/uk/getmondo/api/model/ApiConfig$InboundP2p;

    iget-object v3, p1, Lco/uk/getmondo/api/model/ApiConfig;->inboundP2p:Lco/uk/getmondo/api/model/ApiConfig$InboundP2p;

    invoke-static {v2, v3}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lco/uk/getmondo/api/model/ApiConfig;->sddSplashscreen:Ljava/lang/String;

    iget-object v3, p1, Lco/uk/getmondo/api/model/ApiConfig;->sddSplashscreen:Ljava/lang/String;

    invoke-static {v2, v3}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    iget-boolean v2, p0, Lco/uk/getmondo/api/model/ApiConfig;->prepaidAccountMigrated:Z

    iget-boolean v3, p1, Lco/uk/getmondo/api/model/ApiConfig;->prepaidAccountMigrated:Z

    if-ne v2, v3, :cond_2

    move v2, v1

    :goto_0
    if-eqz v2, :cond_1

    :cond_0
    move v0, v1

    :cond_1
    return v0

    :cond_2
    move v2, v0

    goto :goto_0
.end method

.method public final f()Z
    .locals 1

    .prologue
    .line 12
    iget-boolean v0, p0, Lco/uk/getmondo/api/model/ApiConfig;->prepaidAccountMigrated:Z

    return v0
.end method

.method public hashCode()I
    .locals 3

    const/4 v1, 0x0

    iget-object v0, p0, Lco/uk/getmondo/api/model/ApiConfig;->p2p:Lco/uk/getmondo/api/model/ApiConfig$P2p;

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    :goto_0
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lco/uk/getmondo/api/model/ApiConfig;->username:Lco/uk/getmondo/api/model/ApiConfig$MonzoMeUsername;

    if-eqz v0, :cond_3

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lco/uk/getmondo/api/model/ApiConfig;->featureFlags:Lco/uk/getmondo/api/model/ApiConfig$FeatureFlags;

    if-eqz v0, :cond_4

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    :goto_2
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lco/uk/getmondo/api/model/ApiConfig;->inboundP2p:Lco/uk/getmondo/api/model/ApiConfig$InboundP2p;

    if-eqz v0, :cond_5

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    :goto_3
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lco/uk/getmondo/api/model/ApiConfig;->sddSplashscreen:Ljava/lang/String;

    if-eqz v2, :cond_0

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_0
    add-int/2addr v0, v1

    mul-int/lit8 v1, v0, 0x1f

    iget-boolean v0, p0, Lco/uk/getmondo/api/model/ApiConfig;->prepaidAccountMigrated:Z

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :cond_1
    add-int/2addr v0, v1

    return v0

    :cond_2
    move v0, v1

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_1

    :cond_4
    move v0, v1

    goto :goto_2

    :cond_5
    move v0, v1

    goto :goto_3
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "ApiConfig(p2p="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/api/model/ApiConfig;->p2p:Lco/uk/getmondo/api/model/ApiConfig$P2p;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", username="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/api/model/ApiConfig;->username:Lco/uk/getmondo/api/model/ApiConfig$MonzoMeUsername;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", featureFlags="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/api/model/ApiConfig;->featureFlags:Lco/uk/getmondo/api/model/ApiConfig$FeatureFlags;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", inboundP2p="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/api/model/ApiConfig;->inboundP2p:Lco/uk/getmondo/api/model/ApiConfig$InboundP2p;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", sddSplashscreen="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/api/model/ApiConfig;->sddSplashscreen:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", prepaidAccountMigrated="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lco/uk/getmondo/api/model/ApiConfig;->prepaidAccountMigrated:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
