.class public final Lco/uk/getmondo/api/model/topup/ApiThreeDsResponse$Companion;
.super Ljava/lang/Object;
.source "ApiThreeDsResponse.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lco/uk/getmondo/api/model/topup/ApiThreeDsResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0002\u0008\u0005\u0008\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002R\u0014\u0010\u0003\u001a\u00020\u0004X\u0086D\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0005\u0010\u0006R\u0014\u0010\u0007\u001a\u00020\u0004X\u0082D\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0008\u0010\u0006\u00a8\u0006\t"
    }
    d2 = {
        "Lco/uk/getmondo/api/model/topup/ApiThreeDsResponse$Companion;",
        "",
        "()V",
        "STATUS_REDIRECT_PENDING",
        "",
        "getSTATUS_REDIRECT_PENDING",
        "()Ljava/lang/String;",
        "STATUS_SUCCEEDED",
        "getSTATUS_SUCCEEDED",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/d/b/i;)V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, Lco/uk/getmondo/api/model/topup/ApiThreeDsResponse$Companion;-><init>()V

    return-void
.end method

.method public static final synthetic a(Lco/uk/getmondo/api/model/topup/ApiThreeDsResponse$Companion;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 28
    invoke-direct {p0}, Lco/uk/getmondo/api/model/topup/ApiThreeDsResponse$Companion;->b()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 29
    invoke-static {}, Lco/uk/getmondo/api/model/topup/ApiThreeDsResponse;->f()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 30
    invoke-static {}, Lco/uk/getmondo/api/model/topup/ApiThreeDsResponse;->g()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
