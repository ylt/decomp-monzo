.class public final Lco/uk/getmondo/api/model/topup/ApiThreeDsResponse;
.super Ljava/lang/Object;
.source "ApiThreeDsResponse.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lco/uk/getmondo/api/model/topup/ApiThreeDsResponse$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0014\n\u0002\u0010\u0008\n\u0002\u0008\u0004\u0008\u0086\u0008\u0018\u0000 \u001d2\u00020\u0001:\u0001\u001dB/\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0008\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0005\u0012\u0006\u0010\u0007\u001a\u00020\u0005\u0012\u0006\u0010\u0008\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\tJ\t\u0010\u0011\u001a\u00020\u0003H\u00c6\u0003J\u000b\u0010\u0012\u001a\u0004\u0018\u00010\u0005H\u00c6\u0003J\t\u0010\u0013\u001a\u00020\u0005H\u00c6\u0003J\t\u0010\u0014\u001a\u00020\u0005H\u00c6\u0003J\t\u0010\u0015\u001a\u00020\u0005H\u00c6\u0003J=\u0010\u0016\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\n\u0008\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u00052\u0008\u0008\u0002\u0010\u0006\u001a\u00020\u00052\u0008\u0008\u0002\u0010\u0007\u001a\u00020\u00052\u0008\u0008\u0002\u0010\u0008\u001a\u00020\u0005H\u00c6\u0001J\u0013\u0010\u0017\u001a\u00020\u00032\u0008\u0010\u0018\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u0010\u0019\u001a\u00020\u001aH\u00d6\u0001J\u0006\u0010\u001b\u001a\u00020\u0003J\t\u0010\u001c\u001a\u00020\u0005H\u00d6\u0001R\u0013\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\n\u0010\u000bR\u0011\u0010\u0008\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000c\u0010\u000bR\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\r\u0010\u000eR\u0011\u0010\u0007\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000f\u0010\u000bR\u0011\u0010\u0006\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0010\u0010\u000b\u00a8\u0006\u001e"
    }
    d2 = {
        "Lco/uk/getmondo/api/model/topup/ApiThreeDsResponse;",
        "",
        "required",
        "",
        "decision",
        "",
        "stripeSource",
        "status",
        "redirectUrl",
        "(ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V",
        "getDecision",
        "()Ljava/lang/String;",
        "getRedirectUrl",
        "getRequired",
        "()Z",
        "getStatus",
        "getStripeSource",
        "component1",
        "component2",
        "component3",
        "component4",
        "component5",
        "copy",
        "equals",
        "other",
        "hashCode",
        "",
        "requiresWebResolution",
        "toString",
        "Companion",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# static fields
.field public static final Companion:Lco/uk/getmondo/api/model/topup/ApiThreeDsResponse$Companion;

# The value of this static final field might be set in the static constructor
.field private static final STATUS_REDIRECT_PENDING:Ljava/lang/String; = "redirect_pending"

# The value of this static final field might be set in the static constructor
.field private static final STATUS_SUCCEEDED:Ljava/lang/String; = "succeeded"


# instance fields
.field private final decision:Ljava/lang/String;

.field private final redirectUrl:Ljava/lang/String;

.field private final required:Z

.field private final status:Ljava/lang/String;

.field private final stripeSource:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    new-instance v0, Lco/uk/getmondo/api/model/topup/ApiThreeDsResponse$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lco/uk/getmondo/api/model/topup/ApiThreeDsResponse$Companion;-><init>(Lkotlin/d/b/i;)V

    sput-object v0, Lco/uk/getmondo/api/model/topup/ApiThreeDsResponse;->Companion:Lco/uk/getmondo/api/model/topup/ApiThreeDsResponse$Companion;

    .line 29
    const-string v0, "succeeded"

    sput-object v0, Lco/uk/getmondo/api/model/topup/ApiThreeDsResponse;->STATUS_SUCCEEDED:Ljava/lang/String;

    .line 30
    const-string v0, "redirect_pending"

    sput-object v0, Lco/uk/getmondo/api/model/topup/ApiThreeDsResponse;->STATUS_REDIRECT_PENDING:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    const-string v0, "stripeSource"

    invoke-static {p3, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "status"

    invoke-static {p4, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "redirectUrl"

    invoke-static {p5, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean p1, p0, Lco/uk/getmondo/api/model/topup/ApiThreeDsResponse;->required:Z

    iput-object p2, p0, Lco/uk/getmondo/api/model/topup/ApiThreeDsResponse;->decision:Ljava/lang/String;

    iput-object p3, p0, Lco/uk/getmondo/api/model/topup/ApiThreeDsResponse;->stripeSource:Ljava/lang/String;

    iput-object p4, p0, Lco/uk/getmondo/api/model/topup/ApiThreeDsResponse;->status:Ljava/lang/String;

    iput-object p5, p0, Lco/uk/getmondo/api/model/topup/ApiThreeDsResponse;->redirectUrl:Ljava/lang/String;

    return-void
.end method

.method public static final synthetic f()Ljava/lang/String;
    .locals 1

    .prologue
    .line 3
    sget-object v0, Lco/uk/getmondo/api/model/topup/ApiThreeDsResponse;->STATUS_SUCCEEDED:Ljava/lang/String;

    return-object v0
.end method

.method public static final synthetic g()Ljava/lang/String;
    .locals 1

    .prologue
    .line 3
    sget-object v0, Lco/uk/getmondo/api/model/topup/ApiThreeDsResponse;->STATUS_REDIRECT_PENDING:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a()Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 15
    iget-boolean v2, p0, Lco/uk/getmondo/api/model/topup/ApiThreeDsResponse;->required:Z

    if-nez v2, :cond_1

    .line 22
    :cond_0
    :goto_0
    return v0

    .line 19
    :cond_1
    sget-object v2, Lco/uk/getmondo/api/model/topup/ApiThreeDsResponse;->Companion:Lco/uk/getmondo/api/model/topup/ApiThreeDsResponse$Companion;

    invoke-static {v2}, Lco/uk/getmondo/api/model/topup/ApiThreeDsResponse$Companion;->a(Lco/uk/getmondo/api/model/topup/ApiThreeDsResponse$Companion;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lco/uk/getmondo/api/model/topup/ApiThreeDsResponse;->status:Ljava/lang/String;

    invoke-static {v2, v3, v1}, Lkotlin/h/j;->a(Ljava/lang/String;Ljava/lang/String;Z)Z

    move-result v2

    if-nez v2, :cond_0

    .line 21
    sget-object v0, Lco/uk/getmondo/api/model/topup/ApiThreeDsResponse;->Companion:Lco/uk/getmondo/api/model/topup/ApiThreeDsResponse$Companion;

    invoke-virtual {v0}, Lco/uk/getmondo/api/model/topup/ApiThreeDsResponse$Companion;->a()Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lco/uk/getmondo/api/model/topup/ApiThreeDsResponse;->status:Ljava/lang/String;

    invoke-static {v0, v2, v1}, Lkotlin/h/j;->a(Ljava/lang/String;Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v1

    .line 22
    goto :goto_0

    .line 23
    :cond_2
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unsupported status found: status="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lco/uk/getmondo/api/model/topup/ApiThreeDsResponse;->status:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 4
    iget-boolean v0, p0, Lco/uk/getmondo/api/model/topup/ApiThreeDsResponse;->required:Z

    return v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 5
    iget-object v0, p0, Lco/uk/getmondo/api/model/topup/ApiThreeDsResponse;->decision:Ljava/lang/String;

    return-object v0
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 6
    iget-object v0, p0, Lco/uk/getmondo/api/model/topup/ApiThreeDsResponse;->stripeSource:Ljava/lang/String;

    return-object v0
.end method

.method public final e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 8
    iget-object v0, p0, Lco/uk/getmondo/api/model/topup/ApiThreeDsResponse;->redirectUrl:Ljava/lang/String;

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v1, 0x1

    const/4 v0, 0x0

    if-eq p0, p1, :cond_0

    instance-of v2, p1, Lco/uk/getmondo/api/model/topup/ApiThreeDsResponse;

    if-eqz v2, :cond_1

    check-cast p1, Lco/uk/getmondo/api/model/topup/ApiThreeDsResponse;

    iget-boolean v2, p0, Lco/uk/getmondo/api/model/topup/ApiThreeDsResponse;->required:Z

    iget-boolean v3, p1, Lco/uk/getmondo/api/model/topup/ApiThreeDsResponse;->required:Z

    if-ne v2, v3, :cond_2

    move v2, v1

    :goto_0
    if-eqz v2, :cond_1

    iget-object v2, p0, Lco/uk/getmondo/api/model/topup/ApiThreeDsResponse;->decision:Ljava/lang/String;

    iget-object v3, p1, Lco/uk/getmondo/api/model/topup/ApiThreeDsResponse;->decision:Ljava/lang/String;

    invoke-static {v2, v3}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lco/uk/getmondo/api/model/topup/ApiThreeDsResponse;->stripeSource:Ljava/lang/String;

    iget-object v3, p1, Lco/uk/getmondo/api/model/topup/ApiThreeDsResponse;->stripeSource:Ljava/lang/String;

    invoke-static {v2, v3}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lco/uk/getmondo/api/model/topup/ApiThreeDsResponse;->status:Ljava/lang/String;

    iget-object v3, p1, Lco/uk/getmondo/api/model/topup/ApiThreeDsResponse;->status:Ljava/lang/String;

    invoke-static {v2, v3}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lco/uk/getmondo/api/model/topup/ApiThreeDsResponse;->redirectUrl:Ljava/lang/String;

    iget-object v3, p1, Lco/uk/getmondo/api/model/topup/ApiThreeDsResponse;->redirectUrl:Ljava/lang/String;

    invoke-static {v2, v3}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    move v0, v1

    :cond_1
    return v0

    :cond_2
    move v2, v0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    const/4 v1, 0x0

    iget-boolean v0, p0, Lco/uk/getmondo/api/model/topup/ApiThreeDsResponse;->required:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :cond_0
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lco/uk/getmondo/api/model/topup/ApiThreeDsResponse;->decision:Ljava/lang/String;

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    :goto_0
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lco/uk/getmondo/api/model/topup/ApiThreeDsResponse;->stripeSource:Ljava/lang/String;

    if-eqz v0, :cond_3

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lco/uk/getmondo/api/model/topup/ApiThreeDsResponse;->status:Ljava/lang/String;

    if-eqz v0, :cond_4

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    :goto_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lco/uk/getmondo/api/model/topup/ApiThreeDsResponse;->redirectUrl:Ljava/lang/String;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_1
    add-int/2addr v0, v1

    return v0

    :cond_2
    move v0, v1

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_1

    :cond_4
    move v0, v1

    goto :goto_2
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "ApiThreeDsResponse(required="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lco/uk/getmondo/api/model/topup/ApiThreeDsResponse;->required:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", decision="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/api/model/topup/ApiThreeDsResponse;->decision:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", stripeSource="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/api/model/topup/ApiThreeDsResponse;->stripeSource:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", status="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/api/model/topup/ApiThreeDsResponse;->status:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", redirectUrl="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/api/model/topup/ApiThreeDsResponse;->redirectUrl:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
