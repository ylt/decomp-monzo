.class public final enum Lco/uk/getmondo/api/model/ApiGoldenTicket$GoldenTicketStatus;
.super Ljava/lang/Enum;
.source "ApiGoldenTicket.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lco/uk/getmondo/api/model/ApiGoldenTicket;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "GoldenTicketStatus"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lco/uk/getmondo/api/model/ApiGoldenTicket$GoldenTicketStatus;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0007\u0008\u0086\u0001\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00000\u0001B\u000f\u0008\u0002\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0005\u0010\u0006j\u0002\u0008\u0007j\u0002\u0008\u0008j\u0002\u0008\t\u00a8\u0006\n"
    }
    d2 = {
        "Lco/uk/getmondo/api/model/ApiGoldenTicket$GoldenTicketStatus;",
        "",
        "apiValue",
        "",
        "(Ljava/lang/String;ILjava/lang/String;)V",
        "getApiValue",
        "()Ljava/lang/String;",
        "ACTIVE",
        "INACTIVE",
        "CLAIMED",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lco/uk/getmondo/api/model/ApiGoldenTicket$GoldenTicketStatus;

.field public static final enum ACTIVE:Lco/uk/getmondo/api/model/ApiGoldenTicket$GoldenTicketStatus;

.field public static final enum CLAIMED:Lco/uk/getmondo/api/model/ApiGoldenTicket$GoldenTicketStatus;

.field public static final enum INACTIVE:Lco/uk/getmondo/api/model/ApiGoldenTicket$GoldenTicketStatus;


# instance fields
.field private final apiValue:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    const/4 v0, 0x3

    new-array v0, v0, [Lco/uk/getmondo/api/model/ApiGoldenTicket$GoldenTicketStatus;

    new-instance v1, Lco/uk/getmondo/api/model/ApiGoldenTicket$GoldenTicketStatus;

    const-string v2, "ACTIVE"

    .line 12
    const-string v3, "ACTIVE"

    invoke-direct {v1, v2, v4, v3}, Lco/uk/getmondo/api/model/ApiGoldenTicket$GoldenTicketStatus;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lco/uk/getmondo/api/model/ApiGoldenTicket$GoldenTicketStatus;->ACTIVE:Lco/uk/getmondo/api/model/ApiGoldenTicket$GoldenTicketStatus;

    aput-object v1, v0, v4

    new-instance v1, Lco/uk/getmondo/api/model/ApiGoldenTicket$GoldenTicketStatus;

    const-string v2, "INACTIVE"

    .line 13
    const-string v3, "INACTIVE"

    invoke-direct {v1, v2, v5, v3}, Lco/uk/getmondo/api/model/ApiGoldenTicket$GoldenTicketStatus;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lco/uk/getmondo/api/model/ApiGoldenTicket$GoldenTicketStatus;->INACTIVE:Lco/uk/getmondo/api/model/ApiGoldenTicket$GoldenTicketStatus;

    aput-object v1, v0, v5

    new-instance v1, Lco/uk/getmondo/api/model/ApiGoldenTicket$GoldenTicketStatus;

    const-string v2, "CLAIMED"

    .line 14
    const-string v3, "CLAIMED"

    invoke-direct {v1, v2, v6, v3}, Lco/uk/getmondo/api/model/ApiGoldenTicket$GoldenTicketStatus;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lco/uk/getmondo/api/model/ApiGoldenTicket$GoldenTicketStatus;->CLAIMED:Lco/uk/getmondo/api/model/ApiGoldenTicket$GoldenTicketStatus;

    aput-object v1, v0, v6

    sput-object v0, Lco/uk/getmondo/api/model/ApiGoldenTicket$GoldenTicketStatus;->$VALUES:[Lco/uk/getmondo/api/model/ApiGoldenTicket$GoldenTicketStatus;

    return-void
.end method

.method protected constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    const-string v0, "apiValue"

    invoke-static {p3, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 11
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput-object p3, p0, Lco/uk/getmondo/api/model/ApiGoldenTicket$GoldenTicketStatus;->apiValue:Ljava/lang/String;

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lco/uk/getmondo/api/model/ApiGoldenTicket$GoldenTicketStatus;
    .locals 1

    const-class v0, Lco/uk/getmondo/api/model/ApiGoldenTicket$GoldenTicketStatus;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/api/model/ApiGoldenTicket$GoldenTicketStatus;

    return-object v0
.end method

.method public static values()[Lco/uk/getmondo/api/model/ApiGoldenTicket$GoldenTicketStatus;
    .locals 1

    sget-object v0, Lco/uk/getmondo/api/model/ApiGoldenTicket$GoldenTicketStatus;->$VALUES:[Lco/uk/getmondo/api/model/ApiGoldenTicket$GoldenTicketStatus;

    invoke-virtual {v0}, [Lco/uk/getmondo/api/model/ApiGoldenTicket$GoldenTicketStatus;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lco/uk/getmondo/api/model/ApiGoldenTicket$GoldenTicketStatus;

    return-object v0
.end method
