.class public final enum Lco/uk/getmondo/api/model/a;
.super Ljava/lang/Enum;
.source "ApiCardStatus.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lco/uk/getmondo/api/model/a;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lco/uk/getmondo/api/model/a;

.field public static final enum ACTIVE:Lco/uk/getmondo/api/model/a;

.field public static final enum BLOCKED:Lco/uk/getmondo/api/model/a;

.field public static final enum INACTIVE:Lco/uk/getmondo/api/model/a;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 4
    new-instance v0, Lco/uk/getmondo/api/model/a;

    const-string v1, "ACTIVE"

    invoke-direct {v0, v1, v2}, Lco/uk/getmondo/api/model/a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lco/uk/getmondo/api/model/a;->ACTIVE:Lco/uk/getmondo/api/model/a;

    new-instance v0, Lco/uk/getmondo/api/model/a;

    const-string v1, "INACTIVE"

    invoke-direct {v0, v1, v3}, Lco/uk/getmondo/api/model/a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lco/uk/getmondo/api/model/a;->INACTIVE:Lco/uk/getmondo/api/model/a;

    new-instance v0, Lco/uk/getmondo/api/model/a;

    const-string v1, "BLOCKED"

    invoke-direct {v0, v1, v4}, Lco/uk/getmondo/api/model/a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lco/uk/getmondo/api/model/a;->BLOCKED:Lco/uk/getmondo/api/model/a;

    .line 3
    const/4 v0, 0x3

    new-array v0, v0, [Lco/uk/getmondo/api/model/a;

    sget-object v1, Lco/uk/getmondo/api/model/a;->ACTIVE:Lco/uk/getmondo/api/model/a;

    aput-object v1, v0, v2

    sget-object v1, Lco/uk/getmondo/api/model/a;->INACTIVE:Lco/uk/getmondo/api/model/a;

    aput-object v1, v0, v3

    sget-object v1, Lco/uk/getmondo/api/model/a;->BLOCKED:Lco/uk/getmondo/api/model/a;

    aput-object v1, v0, v4

    sput-object v0, Lco/uk/getmondo/api/model/a;->$VALUES:[Lco/uk/getmondo/api/model/a;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 3
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lco/uk/getmondo/api/model/a;
    .locals 1

    .prologue
    .line 3
    const-class v0, Lco/uk/getmondo/api/model/a;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/api/model/a;

    return-object v0
.end method

.method public static values()[Lco/uk/getmondo/api/model/a;
    .locals 1

    .prologue
    .line 3
    sget-object v0, Lco/uk/getmondo/api/model/a;->$VALUES:[Lco/uk/getmondo/api/model/a;

    invoke-virtual {v0}, [Lco/uk/getmondo/api/model/a;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lco/uk/getmondo/api/model/a;

    return-object v0
.end method
