.class public final enum Lco/uk/getmondo/api/model/VerificationType;
.super Ljava/lang/Enum;
.source "VerificationType.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lco/uk/getmondo/api/model/VerificationType;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\n\u0008\u0086\u0001\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00000\u0001B\u0017\u0008\u0002\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006R\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0007\u0010\u0008R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\t\u0010\nj\u0002\u0008\u000bj\u0002\u0008\u000cj\u0002\u0008\rj\u0002\u0008\u000e\u00a8\u0006\u000f"
    }
    d2 = {
        "Lco/uk/getmondo/api/model/VerificationType;",
        "",
        "dueDiligenceLevel",
        "",
        "descriptionResId",
        "",
        "(Ljava/lang/String;ILjava/lang/String;I)V",
        "getDescriptionResId",
        "()I",
        "getDueDiligenceLevel",
        "()Ljava/lang/String;",
        "STANDARD",
        "FULL",
        "EXTENDED",
        "BLOCKED",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lco/uk/getmondo/api/model/VerificationType;

.field public static final enum BLOCKED:Lco/uk/getmondo/api/model/VerificationType;
    .annotation runtime Lcom/google/gson/a/c;
        a = "BLOCKED"
    .end annotation
.end field

.field public static final enum EXTENDED:Lco/uk/getmondo/api/model/VerificationType;
    .annotation runtime Lcom/google/gson/a/c;
        a = "EDD"
    .end annotation
.end field

.field public static final enum FULL:Lco/uk/getmondo/api/model/VerificationType;
    .annotation runtime Lcom/google/gson/a/c;
        a = "FDD"
    .end annotation
.end field

.field public static final enum STANDARD:Lco/uk/getmondo/api/model/VerificationType;
    .annotation runtime Lcom/google/gson/a/c;
        a = "SDD"
    .end annotation
.end field


# instance fields
.field private final descriptionResId:I

.field private final dueDiligenceLevel:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const v8, 0x7f0a0259

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    const/4 v0, 0x4

    new-array v0, v0, [Lco/uk/getmondo/api/model/VerificationType;

    new-instance v1, Lco/uk/getmondo/api/model/VerificationType;

    const-string v2, "STANDARD"

    .line 7
    const-string v3, "sdd"

    invoke-direct {v1, v2, v4, v3, v8}, Lco/uk/getmondo/api/model/VerificationType;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v1, Lco/uk/getmondo/api/model/VerificationType;->STANDARD:Lco/uk/getmondo/api/model/VerificationType;

    aput-object v1, v0, v4

    new-instance v1, Lco/uk/getmondo/api/model/VerificationType;

    const-string v2, "FULL"

    .line 8
    const-string v3, "fdd"

    const v4, 0x7f0a0258

    invoke-direct {v1, v2, v5, v3, v4}, Lco/uk/getmondo/api/model/VerificationType;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v1, Lco/uk/getmondo/api/model/VerificationType;->FULL:Lco/uk/getmondo/api/model/VerificationType;

    aput-object v1, v0, v5

    new-instance v1, Lco/uk/getmondo/api/model/VerificationType;

    const-string v2, "EXTENDED"

    .line 9
    const-string v3, "edd"

    const v4, 0x7f0a0257

    invoke-direct {v1, v2, v6, v3, v4}, Lco/uk/getmondo/api/model/VerificationType;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v1, Lco/uk/getmondo/api/model/VerificationType;->EXTENDED:Lco/uk/getmondo/api/model/VerificationType;

    aput-object v1, v0, v6

    new-instance v1, Lco/uk/getmondo/api/model/VerificationType;

    const-string v2, "BLOCKED"

    .line 10
    const-string v3, "blocked"

    invoke-direct {v1, v2, v7, v3, v8}, Lco/uk/getmondo/api/model/VerificationType;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v1, Lco/uk/getmondo/api/model/VerificationType;->BLOCKED:Lco/uk/getmondo/api/model/VerificationType;

    aput-object v1, v0, v7

    sput-object v0, Lco/uk/getmondo/api/model/VerificationType;->$VALUES:[Lco/uk/getmondo/api/model/VerificationType;

    return-void
.end method

.method protected constructor <init>(Ljava/lang/String;ILjava/lang/String;I)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "I)V"
        }
    .end annotation

    .prologue
    const-string v0, "dueDiligenceLevel"

    invoke-static {p3, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 6
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput-object p3, p0, Lco/uk/getmondo/api/model/VerificationType;->dueDiligenceLevel:Ljava/lang/String;

    iput p4, p0, Lco/uk/getmondo/api/model/VerificationType;->descriptionResId:I

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lco/uk/getmondo/api/model/VerificationType;
    .locals 1

    const-class v0, Lco/uk/getmondo/api/model/VerificationType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/api/model/VerificationType;

    return-object v0
.end method

.method public static values()[Lco/uk/getmondo/api/model/VerificationType;
    .locals 1

    sget-object v0, Lco/uk/getmondo/api/model/VerificationType;->$VALUES:[Lco/uk/getmondo/api/model/VerificationType;

    invoke-virtual {v0}, [Lco/uk/getmondo/api/model/VerificationType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lco/uk/getmondo/api/model/VerificationType;

    return-object v0
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 6
    iget-object v0, p0, Lco/uk/getmondo/api/model/VerificationType;->dueDiligenceLevel:Ljava/lang/String;

    return-object v0
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 6
    iget v0, p0, Lco/uk/getmondo/api/model/VerificationType;->descriptionResId:I

    return v0
.end method
