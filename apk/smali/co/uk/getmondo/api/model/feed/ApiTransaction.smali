.class public final Lco/uk/getmondo/api/model/feed/ApiTransaction;
.super Ljava/lang/Object;
.source "ApiTransaction.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000P\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0006\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u00088\n\u0002\u0010\u0008\n\u0002\u0008\u0003\u0008\u0086\u0008\u0018\u00002\u00020\u0001B\u00a9\u0001\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0008\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\u0007\u0012\u0006\u0010\t\u001a\u00020\n\u0012\u0006\u0010\u000b\u001a\u00020\n\u0012\u0006\u0010\u000c\u001a\u00020\u0003\u0012\u0006\u0010\r\u001a\u00020\u0003\u0012\u0006\u0010\u000e\u001a\u00020\u0003\u0012\u0008\u0010\u000f\u001a\u0004\u0018\u00010\u0003\u0012\u0008\u0010\u0010\u001a\u0004\u0018\u00010\u0011\u0012\u0006\u0010\u0012\u001a\u00020\u0013\u0012\u0006\u0010\u0014\u001a\u00020\u0003\u0012\u0008\u0010\u0015\u001a\u0004\u0018\u00010\u0016\u0012\u0006\u0010\u0017\u001a\u00020\u0003\u0012\u000e\u0010\u0018\u001a\n\u0012\u0004\u0012\u00020\u001a\u0018\u00010\u0019\u0012\u0006\u0010\u001b\u001a\u00020\u0013\u0012\n\u0008\u0002\u0010\u001c\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\u0002\u0010\u001dJ\t\u00109\u001a\u00020\u0003H\u00c6\u0003J\u000b\u0010:\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003J\u000b\u0010;\u001a\u0004\u0018\u00010\u0011H\u00c6\u0003J\t\u0010<\u001a\u00020\u0013H\u00c6\u0003J\t\u0010=\u001a\u00020\u0003H\u00c6\u0003J\u000b\u0010>\u001a\u0004\u0018\u00010\u0016H\u00c6\u0003J\t\u0010?\u001a\u00020\u0003H\u00c6\u0003J\u0011\u0010@\u001a\n\u0012\u0004\u0012\u00020\u001a\u0018\u00010\u0019H\u00c6\u0003J\t\u0010A\u001a\u00020\u0013H\u00c6\u0003J\u000b\u0010B\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003J\u000b\u0010C\u001a\u0004\u0018\u00010\u0005H\u00c6\u0003J\t\u0010D\u001a\u00020\u0007H\u00c6\u0003J\t\u0010E\u001a\u00020\u0007H\u00c6\u0003J\t\u0010F\u001a\u00020\nH\u00c6\u0003J\t\u0010G\u001a\u00020\nH\u00c6\u0003J\t\u0010H\u001a\u00020\u0003H\u00c6\u0003J\t\u0010I\u001a\u00020\u0003H\u00c6\u0003J\t\u0010J\u001a\u00020\u0003H\u00c6\u0003J\u00cf\u0001\u0010K\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\n\u0008\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u00052\u0008\u0008\u0002\u0010\u0006\u001a\u00020\u00072\u0008\u0008\u0002\u0010\u0008\u001a\u00020\u00072\u0008\u0008\u0002\u0010\t\u001a\u00020\n2\u0008\u0008\u0002\u0010\u000b\u001a\u00020\n2\u0008\u0008\u0002\u0010\u000c\u001a\u00020\u00032\u0008\u0008\u0002\u0010\r\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u000e\u001a\u00020\u00032\n\u0008\u0002\u0010\u000f\u001a\u0004\u0018\u00010\u00032\n\u0008\u0002\u0010\u0010\u001a\u0004\u0018\u00010\u00112\u0008\u0008\u0002\u0010\u0012\u001a\u00020\u00132\u0008\u0008\u0002\u0010\u0014\u001a\u00020\u00032\n\u0008\u0002\u0010\u0015\u001a\u0004\u0018\u00010\u00162\u0008\u0008\u0002\u0010\u0017\u001a\u00020\u00032\u0010\u0008\u0002\u0010\u0018\u001a\n\u0012\u0004\u0012\u00020\u001a\u0018\u00010\u00192\u0008\u0008\u0002\u0010\u001b\u001a\u00020\u00132\n\u0008\u0002\u0010\u001c\u001a\u0004\u0018\u00010\u0003H\u00c6\u0001J\u0013\u0010L\u001a\u00020\u00132\u0008\u0010M\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\u0006\u0010N\u001a\u00020\u0013J\u0006\u0010O\u001a\u00020\u0013J\u0006\u0010P\u001a\u00020\u0013J\u0006\u0010Q\u001a\u00020\u0013J\t\u0010R\u001a\u00020SH\u00d6\u0001J\t\u0010T\u001a\u00020\u0003H\u00d6\u0001J\u000e\u0010U\u001a\u00020\u00002\u0006\u0010\u0017\u001a\u00020\u0003R\u0011\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001e\u0010\u001fR\u0019\u0010\u0018\u001a\n\u0012\u0004\u0012\u00020\u001a\u0018\u00010\u0019\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008 \u0010!R\u0011\u0010\u0017\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\"\u0010#R\u0013\u0010\u0010\u001a\u0004\u0018\u00010\u0011\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008$\u0010%R\u0011\u0010\t\u001a\u00020\n\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008&\u0010\'R\u0011\u0010\u000c\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008(\u0010#R\u0013\u0010\u000f\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008)\u0010#R\u0011\u0010\u000e\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008*\u0010#R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008+\u0010#R\u0011\u0010\u001b\u001a\u00020\u0013\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008,\u0010-R\u0011\u0010\u0012\u001a\u00020\u0013\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0012\u0010-R\u0011\u0010.\u001a\u00020\u00138F\u00a2\u0006\u0006\u001a\u0004\u0008.\u0010-R\u0011\u0010/\u001a\u00020\u00138F\u00a2\u0006\u0006\u001a\u0004\u0008/\u0010-R\u0011\u0010\u0008\u001a\u00020\u0007\u00a2\u0006\u0008\n\u0000\u001a\u0004\u00080\u0010\u001fR\u0011\u0010\r\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u00081\u0010#R\u0013\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u00082\u00103R\u0013\u0010\u0015\u001a\u0004\u0018\u00010\u0016\u00a2\u0006\u0008\n\u0000\u001a\u0004\u00084\u00105R\u0013\u0010\u001c\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u00086\u0010#R\u0011\u0010\u0014\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u00087\u0010#R\u0011\u0010\u000b\u001a\u00020\n\u00a2\u0006\u0008\n\u0000\u001a\u0004\u00088\u0010\'\u00a8\u0006V"
    }
    d2 = {
        "Lco/uk/getmondo/api/model/feed/ApiTransaction;",
        "",
        "id",
        "",
        "merchant",
        "Lco/uk/getmondo/api/model/feed/ApiMerchant;",
        "amount",
        "",
        "localAmount",
        "created",
        "Ljava/util/Date;",
        "updated",
        "currency",
        "localCurrency",
        "description",
        "declineReason",
        "counterparty",
        "Lco/uk/getmondo/api/model/feed/ApiCounterparty;",
        "isLoad",
        "",
        "settled",
        "metadata",
        "Lco/uk/getmondo/api/model/feed/ApiMetadata;",
        "category",
        "attachments",
        "",
        "Lco/uk/getmondo/api/model/feed/ApiAttachment;",
        "includeInSpending",
        "scheme",
        "(Ljava/lang/String;Lco/uk/getmondo/api/model/feed/ApiMerchant;JJLjava/util/Date;Ljava/util/Date;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lco/uk/getmondo/api/model/feed/ApiCounterparty;ZLjava/lang/String;Lco/uk/getmondo/api/model/feed/ApiMetadata;Ljava/lang/String;Ljava/util/List;ZLjava/lang/String;)V",
        "getAmount",
        "()J",
        "getAttachments",
        "()Ljava/util/List;",
        "getCategory",
        "()Ljava/lang/String;",
        "getCounterparty",
        "()Lco/uk/getmondo/api/model/feed/ApiCounterparty;",
        "getCreated",
        "()Ljava/util/Date;",
        "getCurrency",
        "getDeclineReason",
        "getDescription",
        "getId",
        "getIncludeInSpending",
        "()Z",
        "isPeerToPeer",
        "isTopup",
        "getLocalAmount",
        "getLocalCurrency",
        "getMerchant",
        "()Lco/uk/getmondo/api/model/feed/ApiMerchant;",
        "getMetadata",
        "()Lco/uk/getmondo/api/model/feed/ApiMetadata;",
        "getScheme",
        "getSettled",
        "getUpdated",
        "component1",
        "component10",
        "component11",
        "component12",
        "component13",
        "component14",
        "component15",
        "component16",
        "component17",
        "component18",
        "component2",
        "component3",
        "component4",
        "component5",
        "component6",
        "component7",
        "component8",
        "component9",
        "copy",
        "equals",
        "other",
        "fromAtm",
        "hasDeclineReason",
        "hasMerchant",
        "hasMetadata",
        "hashCode",
        "",
        "toString",
        "withCategory",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field private final amount:J

.field private final attachments:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lco/uk/getmondo/api/model/feed/ApiAttachment;",
            ">;"
        }
    .end annotation
.end field

.field private final category:Ljava/lang/String;

.field private final counterparty:Lco/uk/getmondo/api/model/feed/ApiCounterparty;

.field private final created:Ljava/util/Date;

.field private final currency:Ljava/lang/String;

.field private final declineReason:Ljava/lang/String;

.field private final description:Ljava/lang/String;

.field private final id:Ljava/lang/String;

.field private final includeInSpending:Z

.field private final isLoad:Z

.field private final localAmount:J

.field private final localCurrency:Ljava/lang/String;

.field private final merchant:Lco/uk/getmondo/api/model/feed/ApiMerchant;

.field private final metadata:Lco/uk/getmondo/api/model/feed/ApiMetadata;

.field private final scheme:Ljava/lang/String;

.field private final settled:Ljava/lang/String;

.field private final updated:Ljava/util/Date;


# direct methods
.method public constructor <init>(Ljava/lang/String;Lco/uk/getmondo/api/model/feed/ApiMerchant;JJLjava/util/Date;Ljava/util/Date;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lco/uk/getmondo/api/model/feed/ApiCounterparty;ZLjava/lang/String;Lco/uk/getmondo/api/model/feed/ApiMetadata;Ljava/lang/String;Ljava/util/List;ZLjava/lang/String;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lco/uk/getmondo/api/model/feed/ApiMerchant;",
            "JJ",
            "Ljava/util/Date;",
            "Ljava/util/Date;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lco/uk/getmondo/api/model/feed/ApiCounterparty;",
            "Z",
            "Ljava/lang/String;",
            "Lco/uk/getmondo/api/model/feed/ApiMetadata;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lco/uk/getmondo/api/model/feed/ApiAttachment;",
            ">;Z",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    const-string v1, "id"

    invoke-static {p1, v1}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "created"

    invoke-static {p7, v1}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "updated"

    invoke-static {p8, v1}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "currency"

    invoke-static {p9, v1}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "localCurrency"

    invoke-static {p10, v1}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "description"

    invoke-static {p11, v1}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "settled"

    move-object/from16 v0, p15

    invoke-static {v0, v1}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "category"

    move-object/from16 v0, p17

    invoke-static {v0, v1}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 5
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lco/uk/getmondo/api/model/feed/ApiTransaction;->id:Ljava/lang/String;

    iput-object p2, p0, Lco/uk/getmondo/api/model/feed/ApiTransaction;->merchant:Lco/uk/getmondo/api/model/feed/ApiMerchant;

    iput-wide p3, p0, Lco/uk/getmondo/api/model/feed/ApiTransaction;->amount:J

    iput-wide p5, p0, Lco/uk/getmondo/api/model/feed/ApiTransaction;->localAmount:J

    iput-object p7, p0, Lco/uk/getmondo/api/model/feed/ApiTransaction;->created:Ljava/util/Date;

    iput-object p8, p0, Lco/uk/getmondo/api/model/feed/ApiTransaction;->updated:Ljava/util/Date;

    iput-object p9, p0, Lco/uk/getmondo/api/model/feed/ApiTransaction;->currency:Ljava/lang/String;

    iput-object p10, p0, Lco/uk/getmondo/api/model/feed/ApiTransaction;->localCurrency:Ljava/lang/String;

    iput-object p11, p0, Lco/uk/getmondo/api/model/feed/ApiTransaction;->description:Ljava/lang/String;

    iput-object p12, p0, Lco/uk/getmondo/api/model/feed/ApiTransaction;->declineReason:Ljava/lang/String;

    move-object/from16 v0, p13

    iput-object v0, p0, Lco/uk/getmondo/api/model/feed/ApiTransaction;->counterparty:Lco/uk/getmondo/api/model/feed/ApiCounterparty;

    move/from16 v0, p14

    iput-boolean v0, p0, Lco/uk/getmondo/api/model/feed/ApiTransaction;->isLoad:Z

    move-object/from16 v0, p15

    iput-object v0, p0, Lco/uk/getmondo/api/model/feed/ApiTransaction;->settled:Ljava/lang/String;

    move-object/from16 v0, p16

    iput-object v0, p0, Lco/uk/getmondo/api/model/feed/ApiTransaction;->metadata:Lco/uk/getmondo/api/model/feed/ApiMetadata;

    move-object/from16 v0, p17

    iput-object v0, p0, Lco/uk/getmondo/api/model/feed/ApiTransaction;->category:Ljava/lang/String;

    move-object/from16 v0, p18

    iput-object v0, p0, Lco/uk/getmondo/api/model/feed/ApiTransaction;->attachments:Ljava/util/List;

    move/from16 v0, p19

    iput-boolean v0, p0, Lco/uk/getmondo/api/model/feed/ApiTransaction;->includeInSpending:Z

    move-object/from16 v0, p20

    iput-object v0, p0, Lco/uk/getmondo/api/model/feed/ApiTransaction;->scheme:Ljava/lang/String;

    return-void
.end method

.method public synthetic constructor <init>(Ljava/lang/String;Lco/uk/getmondo/api/model/feed/ApiMerchant;JJLjava/util/Date;Ljava/util/Date;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lco/uk/getmondo/api/model/feed/ApiCounterparty;ZLjava/lang/String;Lco/uk/getmondo/api/model/feed/ApiMetadata;Ljava/lang/String;Ljava/util/List;ZLjava/lang/String;ILkotlin/d/b/i;)V
    .locals 23

    .prologue
    const/high16 v0, 0x20000

    and-int v0, v0, p21

    if-eqz v0, :cond_0

    .line 23
    const/4 v0, 0x0

    check-cast v0, Ljava/lang/String;

    move-object/from16 v21, v0

    :goto_0
    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-wide/from16 v4, p3

    move-wide/from16 v6, p5

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    move-object/from16 v13, p12

    move-object/from16 v14, p13

    move/from16 v15, p14

    move-object/from16 v16, p15

    move-object/from16 v17, p16

    move-object/from16 v18, p17

    move-object/from16 v19, p18

    move/from16 v20, p19

    invoke-direct/range {v1 .. v21}, Lco/uk/getmondo/api/model/feed/ApiTransaction;-><init>(Ljava/lang/String;Lco/uk/getmondo/api/model/feed/ApiMerchant;JJLjava/util/Date;Ljava/util/Date;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lco/uk/getmondo/api/model/feed/ApiCounterparty;ZLjava/lang/String;Lco/uk/getmondo/api/model/feed/ApiMetadata;Ljava/lang/String;Ljava/util/List;ZLjava/lang/String;)V

    return-void

    :cond_0
    move-object/from16 v21, p20

    goto :goto_0
.end method

.method public static bridge synthetic a(Lco/uk/getmondo/api/model/feed/ApiTransaction;Ljava/lang/String;Lco/uk/getmondo/api/model/feed/ApiMerchant;JJLjava/util/Date;Ljava/util/Date;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lco/uk/getmondo/api/model/feed/ApiCounterparty;ZLjava/lang/String;Lco/uk/getmondo/api/model/feed/ApiMetadata;Ljava/lang/String;Ljava/util/List;ZLjava/lang/String;ILjava/lang/Object;)Lco/uk/getmondo/api/model/feed/ApiTransaction;
    .locals 25

    and-int/lit8 v2, p21, 0x1

    if-eqz v2, :cond_11

    move-object/from16 v0, p0

    iget-object v4, v0, Lco/uk/getmondo/api/model/feed/ApiTransaction;->id:Ljava/lang/String;

    :goto_0
    and-int/lit8 v2, p21, 0x2

    if-eqz v2, :cond_10

    move-object/from16 v0, p0

    iget-object v5, v0, Lco/uk/getmondo/api/model/feed/ApiTransaction;->merchant:Lco/uk/getmondo/api/model/feed/ApiMerchant;

    :goto_1
    and-int/lit8 v2, p21, 0x4

    if-eqz v2, :cond_f

    move-object/from16 v0, p0

    iget-wide v6, v0, Lco/uk/getmondo/api/model/feed/ApiTransaction;->amount:J

    :goto_2
    and-int/lit8 v2, p21, 0x8

    if-eqz v2, :cond_e

    move-object/from16 v0, p0

    iget-wide v8, v0, Lco/uk/getmondo/api/model/feed/ApiTransaction;->localAmount:J

    :goto_3
    and-int/lit8 v2, p21, 0x10

    if-eqz v2, :cond_d

    move-object/from16 v0, p0

    iget-object v10, v0, Lco/uk/getmondo/api/model/feed/ApiTransaction;->created:Ljava/util/Date;

    :goto_4
    and-int/lit8 v2, p21, 0x20

    if-eqz v2, :cond_c

    move-object/from16 v0, p0

    iget-object v11, v0, Lco/uk/getmondo/api/model/feed/ApiTransaction;->updated:Ljava/util/Date;

    :goto_5
    and-int/lit8 v2, p21, 0x40

    if-eqz v2, :cond_b

    move-object/from16 v0, p0

    iget-object v12, v0, Lco/uk/getmondo/api/model/feed/ApiTransaction;->currency:Ljava/lang/String;

    :goto_6
    move/from16 v0, p21

    and-int/lit16 v2, v0, 0x80

    if-eqz v2, :cond_a

    move-object/from16 v0, p0

    iget-object v13, v0, Lco/uk/getmondo/api/model/feed/ApiTransaction;->localCurrency:Ljava/lang/String;

    :goto_7
    move/from16 v0, p21

    and-int/lit16 v2, v0, 0x100

    if-eqz v2, :cond_9

    move-object/from16 v0, p0

    iget-object v14, v0, Lco/uk/getmondo/api/model/feed/ApiTransaction;->description:Ljava/lang/String;

    :goto_8
    move/from16 v0, p21

    and-int/lit16 v2, v0, 0x200

    if-eqz v2, :cond_8

    move-object/from16 v0, p0

    iget-object v15, v0, Lco/uk/getmondo/api/model/feed/ApiTransaction;->declineReason:Ljava/lang/String;

    :goto_9
    move/from16 v0, p21

    and-int/lit16 v2, v0, 0x400

    if-eqz v2, :cond_7

    move-object/from16 v0, p0

    iget-object v0, v0, Lco/uk/getmondo/api/model/feed/ApiTransaction;->counterparty:Lco/uk/getmondo/api/model/feed/ApiCounterparty;

    move-object/from16 v16, v0

    :goto_a
    move/from16 v0, p21

    and-int/lit16 v2, v0, 0x800

    if-eqz v2, :cond_6

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lco/uk/getmondo/api/model/feed/ApiTransaction;->isLoad:Z

    move/from16 v17, v0

    :goto_b
    move/from16 v0, p21

    and-int/lit16 v2, v0, 0x1000

    if-eqz v2, :cond_5

    move-object/from16 v0, p0

    iget-object v0, v0, Lco/uk/getmondo/api/model/feed/ApiTransaction;->settled:Ljava/lang/String;

    move-object/from16 v18, v0

    :goto_c
    move/from16 v0, p21

    and-int/lit16 v2, v0, 0x2000

    if-eqz v2, :cond_4

    move-object/from16 v0, p0

    iget-object v0, v0, Lco/uk/getmondo/api/model/feed/ApiTransaction;->metadata:Lco/uk/getmondo/api/model/feed/ApiMetadata;

    move-object/from16 v19, v0

    :goto_d
    move/from16 v0, p21

    and-int/lit16 v2, v0, 0x4000

    if-eqz v2, :cond_3

    move-object/from16 v0, p0

    iget-object v0, v0, Lco/uk/getmondo/api/model/feed/ApiTransaction;->category:Ljava/lang/String;

    move-object/from16 v20, v0

    :goto_e
    const v2, 0x8000

    and-int v2, v2, p21

    if-eqz v2, :cond_2

    move-object/from16 v0, p0

    iget-object v0, v0, Lco/uk/getmondo/api/model/feed/ApiTransaction;->attachments:Ljava/util/List;

    move-object/from16 v21, v0

    :goto_f
    const/high16 v2, 0x10000

    and-int v2, v2, p21

    if-eqz v2, :cond_1

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lco/uk/getmondo/api/model/feed/ApiTransaction;->includeInSpending:Z

    move/from16 v22, v0

    :goto_10
    const/high16 v2, 0x20000

    and-int v2, v2, p21

    if-eqz v2, :cond_0

    move-object/from16 v0, p0

    iget-object v0, v0, Lco/uk/getmondo/api/model/feed/ApiTransaction;->scheme:Ljava/lang/String;

    move-object/from16 v23, v0

    :goto_11
    move-object/from16 v3, p0

    invoke-virtual/range {v3 .. v23}, Lco/uk/getmondo/api/model/feed/ApiTransaction;->a(Ljava/lang/String;Lco/uk/getmondo/api/model/feed/ApiMerchant;JJLjava/util/Date;Ljava/util/Date;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lco/uk/getmondo/api/model/feed/ApiCounterparty;ZLjava/lang/String;Lco/uk/getmondo/api/model/feed/ApiMetadata;Ljava/lang/String;Ljava/util/List;ZLjava/lang/String;)Lco/uk/getmondo/api/model/feed/ApiTransaction;

    move-result-object v2

    return-object v2

    :cond_0
    move-object/from16 v23, p20

    goto :goto_11

    :cond_1
    move/from16 v22, p19

    goto :goto_10

    :cond_2
    move-object/from16 v21, p18

    goto :goto_f

    :cond_3
    move-object/from16 v20, p17

    goto :goto_e

    :cond_4
    move-object/from16 v19, p16

    goto :goto_d

    :cond_5
    move-object/from16 v18, p15

    goto :goto_c

    :cond_6
    move/from16 v17, p14

    goto :goto_b

    :cond_7
    move-object/from16 v16, p13

    goto :goto_a

    :cond_8
    move-object/from16 v15, p12

    goto/16 :goto_9

    :cond_9
    move-object/from16 v14, p11

    goto/16 :goto_8

    :cond_a
    move-object/from16 v13, p10

    goto/16 :goto_7

    :cond_b
    move-object/from16 v12, p9

    goto/16 :goto_6

    :cond_c
    move-object/from16 v11, p8

    goto/16 :goto_5

    :cond_d
    move-object/from16 v10, p7

    goto/16 :goto_4

    :cond_e
    move-wide/from16 v8, p5

    goto/16 :goto_3

    :cond_f
    move-wide/from16 v6, p3

    goto/16 :goto_2

    :cond_10
    move-object/from16 v5, p2

    goto/16 :goto_1

    :cond_11
    move-object/from16 v4, p1

    goto/16 :goto_0
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Lco/uk/getmondo/api/model/feed/ApiTransaction;
    .locals 26

    .prologue
    const-string v2, "category"

    move-object/from16 v0, p1

    invoke-static {v0, v2}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 49
    const/4 v4, 0x0

    const/4 v5, 0x0

    const-wide/16 v6, 0x0

    const-wide/16 v8, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    const/16 v17, 0x0

    const/16 v18, 0x0

    const/16 v19, 0x0

    const/16 v21, 0x0

    const/16 v22, 0x0

    const/16 v23, 0x0

    const v24, 0x3bfff

    const/16 v25, 0x0

    move-object/from16 v3, p0

    move-object/from16 v20, p1

    invoke-static/range {v3 .. v25}, Lco/uk/getmondo/api/model/feed/ApiTransaction;->a(Lco/uk/getmondo/api/model/feed/ApiTransaction;Ljava/lang/String;Lco/uk/getmondo/api/model/feed/ApiMerchant;JJLjava/util/Date;Ljava/util/Date;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lco/uk/getmondo/api/model/feed/ApiCounterparty;ZLjava/lang/String;Lco/uk/getmondo/api/model/feed/ApiMetadata;Ljava/lang/String;Ljava/util/List;ZLjava/lang/String;ILjava/lang/Object;)Lco/uk/getmondo/api/model/feed/ApiTransaction;

    move-result-object v2

    return-object v2
.end method

.method public final a(Ljava/lang/String;Lco/uk/getmondo/api/model/feed/ApiMerchant;JJLjava/util/Date;Ljava/util/Date;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lco/uk/getmondo/api/model/feed/ApiCounterparty;ZLjava/lang/String;Lco/uk/getmondo/api/model/feed/ApiMetadata;Ljava/lang/String;Ljava/util/List;ZLjava/lang/String;)Lco/uk/getmondo/api/model/feed/ApiTransaction;
    .locals 25
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lco/uk/getmondo/api/model/feed/ApiMerchant;",
            "JJ",
            "Ljava/util/Date;",
            "Ljava/util/Date;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lco/uk/getmondo/api/model/feed/ApiCounterparty;",
            "Z",
            "Ljava/lang/String;",
            "Lco/uk/getmondo/api/model/feed/ApiMetadata;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lco/uk/getmondo/api/model/feed/ApiAttachment;",
            ">;Z",
            "Ljava/lang/String;",
            ")",
            "Lco/uk/getmondo/api/model/feed/ApiTransaction;"
        }
    .end annotation

    const-string v2, "id"

    move-object/from16 v0, p1

    invoke-static {v0, v2}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v2, "created"

    move-object/from16 v0, p7

    invoke-static {v0, v2}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v2, "updated"

    move-object/from16 v0, p8

    invoke-static {v0, v2}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v2, "currency"

    move-object/from16 v0, p9

    invoke-static {v0, v2}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v2, "localCurrency"

    move-object/from16 v0, p10

    invoke-static {v0, v2}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v2, "description"

    move-object/from16 v0, p11

    invoke-static {v0, v2}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v2, "settled"

    move-object/from16 v0, p15

    invoke-static {v0, v2}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v2, "category"

    move-object/from16 v0, p17

    invoke-static {v0, v2}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v3, Lco/uk/getmondo/api/model/feed/ApiTransaction;

    move-object/from16 v4, p1

    move-object/from16 v5, p2

    move-wide/from16 v6, p3

    move-wide/from16 v8, p5

    move-object/from16 v10, p7

    move-object/from16 v11, p8

    move-object/from16 v12, p9

    move-object/from16 v13, p10

    move-object/from16 v14, p11

    move-object/from16 v15, p12

    move-object/from16 v16, p13

    move/from16 v17, p14

    move-object/from16 v18, p15

    move-object/from16 v19, p16

    move-object/from16 v20, p17

    move-object/from16 v21, p18

    move/from16 v22, p19

    move-object/from16 v23, p20

    invoke-direct/range {v3 .. v23}, Lco/uk/getmondo/api/model/feed/ApiTransaction;-><init>(Ljava/lang/String;Lco/uk/getmondo/api/model/feed/ApiMerchant;JJLjava/util/Date;Ljava/util/Date;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lco/uk/getmondo/api/model/feed/ApiCounterparty;ZLjava/lang/String;Lco/uk/getmondo/api/model/feed/ApiMetadata;Ljava/lang/String;Ljava/util/List;ZLjava/lang/String;)V

    return-object v3
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lco/uk/getmondo/api/model/feed/ApiTransaction;->merchant:Lco/uk/getmondo/api/model/feed/ApiMerchant;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lco/uk/getmondo/api/model/feed/ApiMerchant;->f()Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lco/uk/getmondo/api/model/feed/ApiTransaction;->merchant:Lco/uk/getmondo/api/model/feed/ApiMerchant;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 35
    invoke-virtual {p0}, Lco/uk/getmondo/api/model/feed/ApiTransaction;->d()Z

    move-result v0

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lco/uk/getmondo/api/model/feed/ApiTransaction;->isLoad:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 38
    iget-object v1, p0, Lco/uk/getmondo/api/model/feed/ApiTransaction;->metadata:Lco/uk/getmondo/api/model/feed/ApiMetadata;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lco/uk/getmondo/api/model/feed/ApiMetadata;->e()Ljava/lang/String;

    move-result-object v1

    :goto_0
    if-nez v1, :cond_1

    iget-object v1, p0, Lco/uk/getmondo/api/model/feed/ApiTransaction;->metadata:Lco/uk/getmondo/api/model/feed/ApiMetadata;

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lco/uk/getmondo/api/model/feed/ApiMetadata;->d()Ljava/lang/String;

    move-result-object v0

    :cond_0
    if-eqz v0, :cond_3

    :cond_1
    iget-object v0, p0, Lco/uk/getmondo/api/model/feed/ApiTransaction;->counterparty:Lco/uk/getmondo/api/model/feed/ApiCounterparty;

    if-eqz v0, :cond_3

    const/4 v0, 0x1

    :goto_1
    return v0

    :cond_2
    move-object v1, v0

    goto :goto_0

    :cond_3
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final e()Z
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lco/uk/getmondo/api/model/feed/ApiTransaction;->metadata:Lco/uk/getmondo/api/model/feed/ApiMetadata;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 6

    const/4 v1, 0x1

    const/4 v0, 0x0

    if-eq p0, p1, :cond_0

    instance-of v2, p1, Lco/uk/getmondo/api/model/feed/ApiTransaction;

    if-eqz v2, :cond_1

    check-cast p1, Lco/uk/getmondo/api/model/feed/ApiTransaction;

    iget-object v2, p0, Lco/uk/getmondo/api/model/feed/ApiTransaction;->id:Ljava/lang/String;

    iget-object v3, p1, Lco/uk/getmondo/api/model/feed/ApiTransaction;->id:Ljava/lang/String;

    invoke-static {v2, v3}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lco/uk/getmondo/api/model/feed/ApiTransaction;->merchant:Lco/uk/getmondo/api/model/feed/ApiMerchant;

    iget-object v3, p1, Lco/uk/getmondo/api/model/feed/ApiTransaction;->merchant:Lco/uk/getmondo/api/model/feed/ApiMerchant;

    invoke-static {v2, v3}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    iget-wide v2, p0, Lco/uk/getmondo/api/model/feed/ApiTransaction;->amount:J

    iget-wide v4, p1, Lco/uk/getmondo/api/model/feed/ApiTransaction;->amount:J

    cmp-long v2, v2, v4

    if-nez v2, :cond_2

    move v2, v1

    :goto_0
    if-eqz v2, :cond_1

    iget-wide v2, p0, Lco/uk/getmondo/api/model/feed/ApiTransaction;->localAmount:J

    iget-wide v4, p1, Lco/uk/getmondo/api/model/feed/ApiTransaction;->localAmount:J

    cmp-long v2, v2, v4

    if-nez v2, :cond_3

    move v2, v1

    :goto_1
    if-eqz v2, :cond_1

    iget-object v2, p0, Lco/uk/getmondo/api/model/feed/ApiTransaction;->created:Ljava/util/Date;

    iget-object v3, p1, Lco/uk/getmondo/api/model/feed/ApiTransaction;->created:Ljava/util/Date;

    invoke-static {v2, v3}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lco/uk/getmondo/api/model/feed/ApiTransaction;->updated:Ljava/util/Date;

    iget-object v3, p1, Lco/uk/getmondo/api/model/feed/ApiTransaction;->updated:Ljava/util/Date;

    invoke-static {v2, v3}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lco/uk/getmondo/api/model/feed/ApiTransaction;->currency:Ljava/lang/String;

    iget-object v3, p1, Lco/uk/getmondo/api/model/feed/ApiTransaction;->currency:Ljava/lang/String;

    invoke-static {v2, v3}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lco/uk/getmondo/api/model/feed/ApiTransaction;->localCurrency:Ljava/lang/String;

    iget-object v3, p1, Lco/uk/getmondo/api/model/feed/ApiTransaction;->localCurrency:Ljava/lang/String;

    invoke-static {v2, v3}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lco/uk/getmondo/api/model/feed/ApiTransaction;->description:Ljava/lang/String;

    iget-object v3, p1, Lco/uk/getmondo/api/model/feed/ApiTransaction;->description:Ljava/lang/String;

    invoke-static {v2, v3}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lco/uk/getmondo/api/model/feed/ApiTransaction;->declineReason:Ljava/lang/String;

    iget-object v3, p1, Lco/uk/getmondo/api/model/feed/ApiTransaction;->declineReason:Ljava/lang/String;

    invoke-static {v2, v3}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lco/uk/getmondo/api/model/feed/ApiTransaction;->counterparty:Lco/uk/getmondo/api/model/feed/ApiCounterparty;

    iget-object v3, p1, Lco/uk/getmondo/api/model/feed/ApiTransaction;->counterparty:Lco/uk/getmondo/api/model/feed/ApiCounterparty;

    invoke-static {v2, v3}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    iget-boolean v2, p0, Lco/uk/getmondo/api/model/feed/ApiTransaction;->isLoad:Z

    iget-boolean v3, p1, Lco/uk/getmondo/api/model/feed/ApiTransaction;->isLoad:Z

    if-ne v2, v3, :cond_4

    move v2, v1

    :goto_2
    if-eqz v2, :cond_1

    iget-object v2, p0, Lco/uk/getmondo/api/model/feed/ApiTransaction;->settled:Ljava/lang/String;

    iget-object v3, p1, Lco/uk/getmondo/api/model/feed/ApiTransaction;->settled:Ljava/lang/String;

    invoke-static {v2, v3}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lco/uk/getmondo/api/model/feed/ApiTransaction;->metadata:Lco/uk/getmondo/api/model/feed/ApiMetadata;

    iget-object v3, p1, Lco/uk/getmondo/api/model/feed/ApiTransaction;->metadata:Lco/uk/getmondo/api/model/feed/ApiMetadata;

    invoke-static {v2, v3}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lco/uk/getmondo/api/model/feed/ApiTransaction;->category:Ljava/lang/String;

    iget-object v3, p1, Lco/uk/getmondo/api/model/feed/ApiTransaction;->category:Ljava/lang/String;

    invoke-static {v2, v3}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lco/uk/getmondo/api/model/feed/ApiTransaction;->attachments:Ljava/util/List;

    iget-object v3, p1, Lco/uk/getmondo/api/model/feed/ApiTransaction;->attachments:Ljava/util/List;

    invoke-static {v2, v3}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    iget-boolean v2, p0, Lco/uk/getmondo/api/model/feed/ApiTransaction;->includeInSpending:Z

    iget-boolean v3, p1, Lco/uk/getmondo/api/model/feed/ApiTransaction;->includeInSpending:Z

    if-ne v2, v3, :cond_5

    move v2, v1

    :goto_3
    if-eqz v2, :cond_1

    iget-object v2, p0, Lco/uk/getmondo/api/model/feed/ApiTransaction;->scheme:Ljava/lang/String;

    iget-object v3, p1, Lco/uk/getmondo/api/model/feed/ApiTransaction;->scheme:Ljava/lang/String;

    invoke-static {v2, v3}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    move v0, v1

    :cond_1
    return v0

    :cond_2
    move v2, v0

    goto/16 :goto_0

    :cond_3
    move v2, v0

    goto/16 :goto_1

    :cond_4
    move v2, v0

    goto :goto_2

    :cond_5
    move v2, v0

    goto :goto_3
.end method

.method public final f()Z
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lco/uk/getmondo/api/model/feed/ApiTransaction;->declineReason:Ljava/lang/String;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final g()Ljava/lang/String;
    .locals 1

    .prologue
    .line 6
    iget-object v0, p0, Lco/uk/getmondo/api/model/feed/ApiTransaction;->id:Ljava/lang/String;

    return-object v0
.end method

.method public final h()Lco/uk/getmondo/api/model/feed/ApiMerchant;
    .locals 1

    .prologue
    .line 7
    iget-object v0, p0, Lco/uk/getmondo/api/model/feed/ApiTransaction;->merchant:Lco/uk/getmondo/api/model/feed/ApiMerchant;

    return-object v0
.end method

.method public hashCode()I
    .locals 9

    const/16 v8, 0x20

    const/4 v2, 0x1

    const/4 v1, 0x0

    iget-object v0, p0, Lco/uk/getmondo/api/model/feed/ApiTransaction;->id:Ljava/lang/String;

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    :goto_0
    mul-int/lit8 v3, v0, 0x1f

    iget-object v0, p0, Lco/uk/getmondo/api/model/feed/ApiTransaction;->merchant:Lco/uk/getmondo/api/model/feed/ApiMerchant;

    if-eqz v0, :cond_3

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    :goto_1
    add-int/2addr v0, v3

    mul-int/lit8 v0, v0, 0x1f

    iget-wide v4, p0, Lco/uk/getmondo/api/model/feed/ApiTransaction;->amount:J

    ushr-long v6, v4, v8

    xor-long/2addr v4, v6

    long-to-int v3, v4

    add-int/2addr v0, v3

    mul-int/lit8 v0, v0, 0x1f

    iget-wide v4, p0, Lco/uk/getmondo/api/model/feed/ApiTransaction;->localAmount:J

    ushr-long v6, v4, v8

    xor-long/2addr v4, v6

    long-to-int v3, v4

    add-int/2addr v0, v3

    mul-int/lit8 v3, v0, 0x1f

    iget-object v0, p0, Lco/uk/getmondo/api/model/feed/ApiTransaction;->created:Ljava/util/Date;

    if-eqz v0, :cond_4

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    :goto_2
    add-int/2addr v0, v3

    mul-int/lit8 v3, v0, 0x1f

    iget-object v0, p0, Lco/uk/getmondo/api/model/feed/ApiTransaction;->updated:Ljava/util/Date;

    if-eqz v0, :cond_5

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    :goto_3
    add-int/2addr v0, v3

    mul-int/lit8 v3, v0, 0x1f

    iget-object v0, p0, Lco/uk/getmondo/api/model/feed/ApiTransaction;->currency:Ljava/lang/String;

    if-eqz v0, :cond_6

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    :goto_4
    add-int/2addr v0, v3

    mul-int/lit8 v3, v0, 0x1f

    iget-object v0, p0, Lco/uk/getmondo/api/model/feed/ApiTransaction;->localCurrency:Ljava/lang/String;

    if-eqz v0, :cond_7

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    :goto_5
    add-int/2addr v0, v3

    mul-int/lit8 v3, v0, 0x1f

    iget-object v0, p0, Lco/uk/getmondo/api/model/feed/ApiTransaction;->description:Ljava/lang/String;

    if-eqz v0, :cond_8

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    :goto_6
    add-int/2addr v0, v3

    mul-int/lit8 v3, v0, 0x1f

    iget-object v0, p0, Lco/uk/getmondo/api/model/feed/ApiTransaction;->declineReason:Ljava/lang/String;

    if-eqz v0, :cond_9

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    :goto_7
    add-int/2addr v0, v3

    mul-int/lit8 v3, v0, 0x1f

    iget-object v0, p0, Lco/uk/getmondo/api/model/feed/ApiTransaction;->counterparty:Lco/uk/getmondo/api/model/feed/ApiCounterparty;

    if-eqz v0, :cond_a

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    :goto_8
    add-int/2addr v0, v3

    mul-int/lit8 v3, v0, 0x1f

    iget-boolean v0, p0, Lco/uk/getmondo/api/model/feed/ApiTransaction;->isLoad:Z

    if-eqz v0, :cond_0

    move v0, v2

    :cond_0
    add-int/2addr v0, v3

    mul-int/lit8 v3, v0, 0x1f

    iget-object v0, p0, Lco/uk/getmondo/api/model/feed/ApiTransaction;->settled:Ljava/lang/String;

    if-eqz v0, :cond_b

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    :goto_9
    add-int/2addr v0, v3

    mul-int/lit8 v3, v0, 0x1f

    iget-object v0, p0, Lco/uk/getmondo/api/model/feed/ApiTransaction;->metadata:Lco/uk/getmondo/api/model/feed/ApiMetadata;

    if-eqz v0, :cond_c

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    :goto_a
    add-int/2addr v0, v3

    mul-int/lit8 v3, v0, 0x1f

    iget-object v0, p0, Lco/uk/getmondo/api/model/feed/ApiTransaction;->category:Ljava/lang/String;

    if-eqz v0, :cond_d

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    :goto_b
    add-int/2addr v0, v3

    mul-int/lit8 v3, v0, 0x1f

    iget-object v0, p0, Lco/uk/getmondo/api/model/feed/ApiTransaction;->attachments:Ljava/util/List;

    if-eqz v0, :cond_e

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    :goto_c
    add-int/2addr v0, v3

    mul-int/lit8 v3, v0, 0x1f

    iget-boolean v0, p0, Lco/uk/getmondo/api/model/feed/ApiTransaction;->includeInSpending:Z

    if-eqz v0, :cond_f

    :goto_d
    add-int v0, v3, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lco/uk/getmondo/api/model/feed/ApiTransaction;->scheme:Ljava/lang/String;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_1
    add-int/2addr v0, v1

    return v0

    :cond_2
    move v0, v1

    goto/16 :goto_0

    :cond_3
    move v0, v1

    goto/16 :goto_1

    :cond_4
    move v0, v1

    goto/16 :goto_2

    :cond_5
    move v0, v1

    goto/16 :goto_3

    :cond_6
    move v0, v1

    goto/16 :goto_4

    :cond_7
    move v0, v1

    goto :goto_5

    :cond_8
    move v0, v1

    goto :goto_6

    :cond_9
    move v0, v1

    goto :goto_7

    :cond_a
    move v0, v1

    goto :goto_8

    :cond_b
    move v0, v1

    goto :goto_9

    :cond_c
    move v0, v1

    goto :goto_a

    :cond_d
    move v0, v1

    goto :goto_b

    :cond_e
    move v0, v1

    goto :goto_c

    :cond_f
    move v2, v0

    goto :goto_d
.end method

.method public final i()J
    .locals 2

    .prologue
    .line 8
    iget-wide v0, p0, Lco/uk/getmondo/api/model/feed/ApiTransaction;->amount:J

    return-wide v0
.end method

.method public final j()J
    .locals 2

    .prologue
    .line 9
    iget-wide v0, p0, Lco/uk/getmondo/api/model/feed/ApiTransaction;->localAmount:J

    return-wide v0
.end method

.method public final k()Ljava/util/Date;
    .locals 1

    .prologue
    .line 10
    iget-object v0, p0, Lco/uk/getmondo/api/model/feed/ApiTransaction;->created:Ljava/util/Date;

    return-object v0
.end method

.method public final l()Ljava/util/Date;
    .locals 1

    .prologue
    .line 11
    iget-object v0, p0, Lco/uk/getmondo/api/model/feed/ApiTransaction;->updated:Ljava/util/Date;

    return-object v0
.end method

.method public final m()Ljava/lang/String;
    .locals 1

    .prologue
    .line 12
    iget-object v0, p0, Lco/uk/getmondo/api/model/feed/ApiTransaction;->currency:Ljava/lang/String;

    return-object v0
.end method

.method public final n()Ljava/lang/String;
    .locals 1

    .prologue
    .line 13
    iget-object v0, p0, Lco/uk/getmondo/api/model/feed/ApiTransaction;->localCurrency:Ljava/lang/String;

    return-object v0
.end method

.method public final o()Ljava/lang/String;
    .locals 1

    .prologue
    .line 14
    iget-object v0, p0, Lco/uk/getmondo/api/model/feed/ApiTransaction;->description:Ljava/lang/String;

    return-object v0
.end method

.method public final p()Ljava/lang/String;
    .locals 1

    .prologue
    .line 15
    iget-object v0, p0, Lco/uk/getmondo/api/model/feed/ApiTransaction;->declineReason:Ljava/lang/String;

    return-object v0
.end method

.method public final q()Lco/uk/getmondo/api/model/feed/ApiCounterparty;
    .locals 1

    .prologue
    .line 16
    iget-object v0, p0, Lco/uk/getmondo/api/model/feed/ApiTransaction;->counterparty:Lco/uk/getmondo/api/model/feed/ApiCounterparty;

    return-object v0
.end method

.method public final r()Ljava/lang/String;
    .locals 1

    .prologue
    .line 18
    iget-object v0, p0, Lco/uk/getmondo/api/model/feed/ApiTransaction;->settled:Ljava/lang/String;

    return-object v0
.end method

.method public final s()Lco/uk/getmondo/api/model/feed/ApiMetadata;
    .locals 1

    .prologue
    .line 19
    iget-object v0, p0, Lco/uk/getmondo/api/model/feed/ApiTransaction;->metadata:Lco/uk/getmondo/api/model/feed/ApiMetadata;

    return-object v0
.end method

.method public final t()Ljava/lang/String;
    .locals 1

    .prologue
    .line 20
    iget-object v0, p0, Lco/uk/getmondo/api/model/feed/ApiTransaction;->category:Ljava/lang/String;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "ApiTransaction(id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/api/model/feed/ApiTransaction;->id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", merchant="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/api/model/feed/ApiTransaction;->merchant:Lco/uk/getmondo/api/model/feed/ApiMerchant;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", amount="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lco/uk/getmondo/api/model/feed/ApiTransaction;->amount:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", localAmount="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lco/uk/getmondo/api/model/feed/ApiTransaction;->localAmount:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", created="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/api/model/feed/ApiTransaction;->created:Ljava/util/Date;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", updated="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/api/model/feed/ApiTransaction;->updated:Ljava/util/Date;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", currency="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/api/model/feed/ApiTransaction;->currency:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", localCurrency="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/api/model/feed/ApiTransaction;->localCurrency:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", description="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/api/model/feed/ApiTransaction;->description:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", declineReason="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/api/model/feed/ApiTransaction;->declineReason:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", counterparty="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/api/model/feed/ApiTransaction;->counterparty:Lco/uk/getmondo/api/model/feed/ApiCounterparty;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", isLoad="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lco/uk/getmondo/api/model/feed/ApiTransaction;->isLoad:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", settled="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/api/model/feed/ApiTransaction;->settled:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", metadata="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/api/model/feed/ApiTransaction;->metadata:Lco/uk/getmondo/api/model/feed/ApiMetadata;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", category="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/api/model/feed/ApiTransaction;->category:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", attachments="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/api/model/feed/ApiTransaction;->attachments:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", includeInSpending="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lco/uk/getmondo/api/model/feed/ApiTransaction;->includeInSpending:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", scheme="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/api/model/feed/ApiTransaction;->scheme:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final u()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lco/uk/getmondo/api/model/feed/ApiAttachment;",
            ">;"
        }
    .end annotation

    .prologue
    .line 21
    iget-object v0, p0, Lco/uk/getmondo/api/model/feed/ApiTransaction;->attachments:Ljava/util/List;

    return-object v0
.end method

.method public final v()Z
    .locals 1

    .prologue
    .line 22
    iget-boolean v0, p0, Lco/uk/getmondo/api/model/feed/ApiTransaction;->includeInSpending:Z

    return v0
.end method

.method public final w()Ljava/lang/String;
    .locals 1

    .prologue
    .line 23
    iget-object v0, p0, Lco/uk/getmondo/api/model/feed/ApiTransaction;->scheme:Ljava/lang/String;

    return-object v0
.end method
