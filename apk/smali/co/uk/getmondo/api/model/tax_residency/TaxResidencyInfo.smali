.class public final Lco/uk/getmondo/api/model/tax_residency/TaxResidencyInfo;
.super Ljava/lang/Object;
.source "TaxResidencyInfo.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lco/uk/getmondo/api/model/tax_residency/TaxResidencyInfo$Status;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u00000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010 \n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0010\n\u0002\u0010\u0008\n\u0002\u0008\u0003\u0008\u0086\u0008\u0018\u00002\u00020\u0001:\u0001\u001dB9\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0008\u0008\u0001\u0010\u0004\u001a\u00020\u0005\u0012\u000e\u0008\u0003\u0010\u0006\u001a\u0008\u0012\u0004\u0012\u00020\u00080\u0007\u0012\u0010\u0008\u0002\u0010\t\u001a\n\u0012\u0004\u0012\u00020\n\u0018\u00010\u0007\u00a2\u0006\u0002\u0010\u000bJ\t\u0010\u0013\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0014\u001a\u00020\u0005H\u00c6\u0003J\u000f\u0010\u0015\u001a\u0008\u0012\u0004\u0012\u00020\u00080\u0007H\u00c6\u0003J\u0011\u0010\u0016\u001a\n\u0012\u0004\u0012\u00020\n\u0018\u00010\u0007H\u00c6\u0003J?\u0010\u0017\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0003\u0010\u0004\u001a\u00020\u00052\u000e\u0008\u0003\u0010\u0006\u001a\u0008\u0012\u0004\u0012\u00020\u00080\u00072\u0010\u0008\u0002\u0010\t\u001a\n\u0012\u0004\u0012\u00020\n\u0018\u00010\u0007H\u00c6\u0001J\u0013\u0010\u0018\u001a\u00020\u00052\u0008\u0010\u0019\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u0010\u001a\u001a\u00020\u001bH\u00d6\u0001J\t\u0010\u001c\u001a\u00020\u0008H\u00d6\u0001R\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000c\u0010\rR\u0019\u0010\t\u001a\n\u0012\u0004\u0012\u00020\n\u0018\u00010\u0007\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000e\u0010\u000fR\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0010\u0010\u0011R\u0017\u0010\u0006\u001a\u0008\u0012\u0004\u0012\u00020\u00080\u0007\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0012\u0010\u000f\u00a8\u0006\u001e"
    }
    d2 = {
        "Lco/uk/getmondo/api/model/tax_residency/TaxResidencyInfo;",
        "",
        "status",
        "Lco/uk/getmondo/api/model/tax_residency/TaxResidencyInfo$Status;",
        "allowSummaryScreen",
        "",
        "suggestedCountries",
        "",
        "",
        "jurisdictions",
        "Lco/uk/getmondo/api/model/tax_residency/Jurisdiction;",
        "(Lco/uk/getmondo/api/model/tax_residency/TaxResidencyInfo$Status;ZLjava/util/List;Ljava/util/List;)V",
        "getAllowSummaryScreen",
        "()Z",
        "getJurisdictions",
        "()Ljava/util/List;",
        "getStatus",
        "()Lco/uk/getmondo/api/model/tax_residency/TaxResidencyInfo$Status;",
        "getSuggestedCountries",
        "component1",
        "component2",
        "component3",
        "component4",
        "copy",
        "equals",
        "other",
        "hashCode",
        "",
        "toString",
        "Status",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field private final allowSummaryScreen:Z

.field private final jurisdictions:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lco/uk/getmondo/api/model/tax_residency/Jurisdiction;",
            ">;"
        }
    .end annotation
.end field

.field private final status:Lco/uk/getmondo/api/model/tax_residency/TaxResidencyInfo$Status;

.field private final suggestedCountries:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lco/uk/getmondo/api/model/tax_residency/TaxResidencyInfo$Status;ZLjava/util/List;Ljava/util/List;)V
    .locals 1
    .param p2    # Z
        .annotation runtime Lcom/squareup/moshi/h;
            a = "allow_summary_screen"
        .end annotation
    .end param
    .param p3    # Ljava/util/List;
        .annotation runtime Lcom/squareup/moshi/h;
            a = "suggested_countries"
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lco/uk/getmondo/api/model/tax_residency/TaxResidencyInfo$Status;",
            "Z",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lco/uk/getmondo/api/model/tax_residency/Jurisdiction;",
            ">;)V"
        }
    .end annotation

    .prologue
    const-string v0, "status"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "suggestedCountries"

    invoke-static {p3, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 5
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lco/uk/getmondo/api/model/tax_residency/TaxResidencyInfo;->status:Lco/uk/getmondo/api/model/tax_residency/TaxResidencyInfo$Status;

    iput-boolean p2, p0, Lco/uk/getmondo/api/model/tax_residency/TaxResidencyInfo;->allowSummaryScreen:Z

    iput-object p3, p0, Lco/uk/getmondo/api/model/tax_residency/TaxResidencyInfo;->suggestedCountries:Ljava/util/List;

    iput-object p4, p0, Lco/uk/getmondo/api/model/tax_residency/TaxResidencyInfo;->jurisdictions:Ljava/util/List;

    return-void
.end method

.method public synthetic constructor <init>(Lco/uk/getmondo/api/model/tax_residency/TaxResidencyInfo$Status;ZLjava/util/List;Ljava/util/List;ILkotlin/d/b/i;)V
    .locals 1

    .prologue
    and-int/lit8 v0, p5, 0x4

    if-eqz v0, :cond_0

    .line 8
    invoke-static {}, Lkotlin/a/m;->a()Ljava/util/List;

    move-result-object p3

    :cond_0
    and-int/lit8 v0, p5, 0x8

    if-eqz v0, :cond_1

    .line 9
    invoke-static {}, Lkotlin/a/m;->a()Ljava/util/List;

    move-result-object p4

    :cond_1
    invoke-direct {p0, p1, p2, p3, p4}, Lco/uk/getmondo/api/model/tax_residency/TaxResidencyInfo;-><init>(Lco/uk/getmondo/api/model/tax_residency/TaxResidencyInfo$Status;ZLjava/util/List;Ljava/util/List;)V

    return-void
.end method


# virtual methods
.method public final a()Lco/uk/getmondo/api/model/tax_residency/TaxResidencyInfo$Status;
    .locals 1

    .prologue
    .line 6
    iget-object v0, p0, Lco/uk/getmondo/api/model/tax_residency/TaxResidencyInfo;->status:Lco/uk/getmondo/api/model/tax_residency/TaxResidencyInfo$Status;

    return-object v0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 7
    iget-boolean v0, p0, Lco/uk/getmondo/api/model/tax_residency/TaxResidencyInfo;->allowSummaryScreen:Z

    return v0
.end method

.method public final c()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 8
    iget-object v0, p0, Lco/uk/getmondo/api/model/tax_residency/TaxResidencyInfo;->suggestedCountries:Ljava/util/List;

    return-object v0
.end method

.method public final d()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lco/uk/getmondo/api/model/tax_residency/Jurisdiction;",
            ">;"
        }
    .end annotation

    .prologue
    .line 9
    iget-object v0, p0, Lco/uk/getmondo/api/model/tax_residency/TaxResidencyInfo;->jurisdictions:Ljava/util/List;

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v1, 0x1

    const/4 v0, 0x0

    if-eq p0, p1, :cond_0

    instance-of v2, p1, Lco/uk/getmondo/api/model/tax_residency/TaxResidencyInfo;

    if-eqz v2, :cond_1

    check-cast p1, Lco/uk/getmondo/api/model/tax_residency/TaxResidencyInfo;

    iget-object v2, p0, Lco/uk/getmondo/api/model/tax_residency/TaxResidencyInfo;->status:Lco/uk/getmondo/api/model/tax_residency/TaxResidencyInfo$Status;

    iget-object v3, p1, Lco/uk/getmondo/api/model/tax_residency/TaxResidencyInfo;->status:Lco/uk/getmondo/api/model/tax_residency/TaxResidencyInfo$Status;

    invoke-static {v2, v3}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    iget-boolean v2, p0, Lco/uk/getmondo/api/model/tax_residency/TaxResidencyInfo;->allowSummaryScreen:Z

    iget-boolean v3, p1, Lco/uk/getmondo/api/model/tax_residency/TaxResidencyInfo;->allowSummaryScreen:Z

    if-ne v2, v3, :cond_2

    move v2, v1

    :goto_0
    if-eqz v2, :cond_1

    iget-object v2, p0, Lco/uk/getmondo/api/model/tax_residency/TaxResidencyInfo;->suggestedCountries:Ljava/util/List;

    iget-object v3, p1, Lco/uk/getmondo/api/model/tax_residency/TaxResidencyInfo;->suggestedCountries:Ljava/util/List;

    invoke-static {v2, v3}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lco/uk/getmondo/api/model/tax_residency/TaxResidencyInfo;->jurisdictions:Ljava/util/List;

    iget-object v3, p1, Lco/uk/getmondo/api/model/tax_residency/TaxResidencyInfo;->jurisdictions:Ljava/util/List;

    invoke-static {v2, v3}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    move v0, v1

    :cond_1
    return v0

    :cond_2
    move v2, v0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    const/4 v1, 0x0

    iget-object v0, p0, Lco/uk/getmondo/api/model/tax_residency/TaxResidencyInfo;->status:Lco/uk/getmondo/api/model/tax_residency/TaxResidencyInfo$Status;

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    :goto_0
    mul-int/lit8 v2, v0, 0x1f

    iget-boolean v0, p0, Lco/uk/getmondo/api/model/tax_residency/TaxResidencyInfo;->allowSummaryScreen:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :cond_0
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lco/uk/getmondo/api/model/tax_residency/TaxResidencyInfo;->suggestedCountries:Ljava/util/List;

    if-eqz v0, :cond_3

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lco/uk/getmondo/api/model/tax_residency/TaxResidencyInfo;->jurisdictions:Ljava/util/List;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_1
    add-int/2addr v0, v1

    return v0

    :cond_2
    move v0, v1

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_1
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "TaxResidencyInfo(status="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/api/model/tax_residency/TaxResidencyInfo;->status:Lco/uk/getmondo/api/model/tax_residency/TaxResidencyInfo$Status;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", allowSummaryScreen="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lco/uk/getmondo/api/model/tax_residency/TaxResidencyInfo;->allowSummaryScreen:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", suggestedCountries="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/api/model/tax_residency/TaxResidencyInfo;->suggestedCountries:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", jurisdictions="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/api/model/tax_residency/TaxResidencyInfo;->jurisdictions:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
