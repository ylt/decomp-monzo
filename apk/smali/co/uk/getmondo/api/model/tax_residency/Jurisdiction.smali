.class public final Lco/uk/getmondo/api/model/tax_residency/Jurisdiction;
.super Ljava/lang/Object;
.source "Jurisdiction.kt"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lco/uk/getmondo/api/model/tax_residency/Jurisdiction$TinType;,
        Lco/uk/getmondo/api/model/tax_residency/Jurisdiction$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000D\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0003\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0008\u0019\n\u0002\u0010\u0008\n\u0002\u0008\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0003\n\u0002\u0010\u0002\n\u0002\u0008\u0005\u0008\u0086\u0008\u0018\u0000 12\u00020\u0001:\u000212B\u000f\u0008\u0016\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004B9\u0012\u0008\u0008\u0001\u0010\u0005\u001a\u00020\u0006\u0012\u0006\u0010\u0007\u001a\u00020\u0008\u0012\u0006\u0010\t\u001a\u00020\u0008\u0012\u0008\u0008\u0001\u0010\n\u001a\u00020\u0008\u0012\u000e\u0008\u0001\u0010\u000b\u001a\u0008\u0012\u0004\u0012\u00020\r0\u000c\u00a2\u0006\u0002\u0010\u000eJ\t\u0010 \u001a\u00020\u0006H\u00c6\u0003J\t\u0010!\u001a\u00020\u0008H\u00c6\u0003J\t\u0010\"\u001a\u00020\u0008H\u00c6\u0003J\t\u0010#\u001a\u00020\u0008H\u00c6\u0003J\u000f\u0010$\u001a\u0008\u0012\u0004\u0012\u00020\r0\u000cH\u00c6\u0003JA\u0010%\u001a\u00020\u00002\u0008\u0008\u0003\u0010\u0005\u001a\u00020\u00062\u0008\u0008\u0002\u0010\u0007\u001a\u00020\u00082\u0008\u0008\u0002\u0010\t\u001a\u00020\u00082\u0008\u0008\u0003\u0010\n\u001a\u00020\u00082\u000e\u0008\u0003\u0010\u000b\u001a\u0008\u0012\u0004\u0012\u00020\r0\u000cH\u00c6\u0001J\u0008\u0010&\u001a\u00020\'H\u0016J\u0013\u0010(\u001a\u00020\u00082\u0008\u0010)\u001a\u0004\u0018\u00010*H\u00d6\u0003J\t\u0010+\u001a\u00020\'H\u00d6\u0001J\t\u0010,\u001a\u00020\u0006H\u00d6\u0001J\u0018\u0010-\u001a\u00020.2\u0006\u0010/\u001a\u00020\u00032\u0006\u00100\u001a\u00020\'H\u0016R\u0011\u0010\n\u001a\u00020\u0008\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000f\u0010\u0010R\u0013\u0010\u0011\u001a\u0004\u0018\u00010\u00068F\u00a2\u0006\u0006\u001a\u0004\u0008\u0012\u0010\u0013R\u0011\u0010\t\u001a\u00020\u0008\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0014\u0010\u0010R\u0011\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0015\u0010\u0013R\u0011\u0010\u0016\u001a\u00020\u00088F\u00a2\u0006\u0006\u001a\u0004\u0008\u0017\u0010\u0010R\u0011\u0010\u0018\u001a\u00020\r8F\u00a2\u0006\u0006\u001a\u0004\u0008\u0019\u0010\u001aR\u0011\u0010\u0007\u001a\u00020\u0008\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001b\u0010\u0010R\u0013\u0010\u001c\u001a\u0004\u0018\u00010\r8F\u00a2\u0006\u0006\u001a\u0004\u0008\u001d\u0010\u001aR\u0017\u0010\u000b\u001a\u0008\u0012\u0004\u0012\u00020\r0\u000c\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001e\u0010\u001f\u00a8\u00063"
    }
    d2 = {
        "Lco/uk/getmondo/api/model/tax_residency/Jurisdiction;",
        "Landroid/os/Parcelable;",
        "source",
        "Landroid/os/Parcel;",
        "(Landroid/os/Parcel;)V",
        "countryCode",
        "",
        "reportable",
        "",
        "completed",
        "allowNoTin",
        "tinTypes",
        "",
        "Lco/uk/getmondo/api/model/tax_residency/Jurisdiction$TinType;",
        "(Ljava/lang/String;ZZZLjava/util/List;)V",
        "getAllowNoTin",
        "()Z",
        "alpha2CountryCode",
        "getAlpha2CountryCode",
        "()Ljava/lang/String;",
        "getCompleted",
        "getCountryCode",
        "hasMultipleTinTypes",
        "getHasMultipleTinTypes",
        "primaryTinType",
        "getPrimaryTinType",
        "()Lco/uk/getmondo/api/model/tax_residency/Jurisdiction$TinType;",
        "getReportable",
        "secondaryTinType",
        "getSecondaryTinType",
        "getTinTypes",
        "()Ljava/util/List;",
        "component1",
        "component2",
        "component3",
        "component4",
        "component5",
        "copy",
        "describeContents",
        "",
        "equals",
        "other",
        "",
        "hashCode",
        "toString",
        "writeToParcel",
        "",
        "dest",
        "flags",
        "Companion",
        "TinType",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lco/uk/getmondo/api/model/tax_residency/Jurisdiction;",
            ">;"
        }
    .end annotation
.end field

.field public static final Companion:Lco/uk/getmondo/api/model/tax_residency/Jurisdiction$Companion;


# instance fields
.field private final allowNoTin:Z

.field private final completed:Z

.field private final countryCode:Ljava/lang/String;

.field private final reportable:Z

.field private final tinTypes:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lco/uk/getmondo/api/model/tax_residency/Jurisdiction$TinType;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    new-instance v0, Lco/uk/getmondo/api/model/tax_residency/Jurisdiction$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lco/uk/getmondo/api/model/tax_residency/Jurisdiction$Companion;-><init>(Lkotlin/d/b/i;)V

    sput-object v0, Lco/uk/getmondo/api/model/tax_residency/Jurisdiction;->Companion:Lco/uk/getmondo/api/model/tax_residency/Jurisdiction$Companion;

    .line 29
    new-instance v0, Lco/uk/getmondo/api/model/tax_residency/Jurisdiction$Companion$CREATOR$1;

    invoke-direct {v0}, Lco/uk/getmondo/api/model/tax_residency/Jurisdiction$Companion$CREATOR$1;-><init>()V

    check-cast v0, Landroid/os/Parcelable$Creator;

    sput-object v0, Lco/uk/getmondo/api/model/tax_residency/Jurisdiction;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 6

    .prologue
    const/4 v4, 0x0

    const/4 v0, 0x1

    const-string v1, "source"

    invoke-static {p1, v1}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 36
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "source.readString()"

    invoke-static {v1, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 37
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-ne v0, v2, :cond_1

    move v2, v0

    .line 38
    :goto_0
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v3

    if-ne v0, v3, :cond_2

    move v3, v0

    .line 39
    :goto_1
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v5

    if-ne v0, v5, :cond_0

    move v4, v0

    .line 40
    :cond_0
    sget-object v0, Lco/uk/getmondo/api/model/tax_residency/Jurisdiction$TinType;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->createTypedArrayList(Landroid/os/Parcelable$Creator;)Ljava/util/ArrayList;

    move-result-object v5

    const-string v0, "source.createTypedArrayList(TinType.CREATOR)"

    invoke-static {v5, v0}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v5, Ljava/util/List;

    move-object v0, p0

    .line 35
    invoke-direct/range {v0 .. v5}, Lco/uk/getmondo/api/model/tax_residency/Jurisdiction;-><init>(Ljava/lang/String;ZZZLjava/util/List;)V

    return-void

    :cond_1
    move v2, v4

    .line 37
    goto :goto_0

    :cond_2
    move v3, v4

    .line 38
    goto :goto_1
.end method

.method public constructor <init>(Ljava/lang/String;ZZZLjava/util/List;)V
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation runtime Lcom/squareup/moshi/h;
            a = "country_code"
        .end annotation
    .end param
    .param p4    # Z
        .annotation runtime Lcom/squareup/moshi/h;
            a = "allow_no_tin"
        .end annotation
    .end param
    .param p5    # Ljava/util/List;
        .annotation runtime Lcom/squareup/moshi/h;
            a = "tin_types"
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "ZZZ",
            "Ljava/util/List",
            "<",
            "Lco/uk/getmondo/api/model/tax_residency/Jurisdiction$TinType;",
            ">;)V"
        }
    .end annotation

    .prologue
    const-string v0, "countryCode"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "tinTypes"

    invoke-static {p5, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lco/uk/getmondo/api/model/tax_residency/Jurisdiction;->countryCode:Ljava/lang/String;

    iput-boolean p2, p0, Lco/uk/getmondo/api/model/tax_residency/Jurisdiction;->reportable:Z

    iput-boolean p3, p0, Lco/uk/getmondo/api/model/tax_residency/Jurisdiction;->completed:Z

    iput-boolean p4, p0, Lco/uk/getmondo/api/model/tax_residency/Jurisdiction;->allowNoTin:Z

    iput-object p5, p0, Lco/uk/getmondo/api/model/tax_residency/Jurisdiction;->tinTypes:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 2

    .prologue
    .line 17
    sget-object v0, Lco/uk/getmondo/d/i;->Companion:Lco/uk/getmondo/d/i$a;

    iget-object v1, p0, Lco/uk/getmondo/api/model/tax_residency/Jurisdiction;->countryCode:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lco/uk/getmondo/d/i$a;->a(Ljava/lang/String;)Lco/uk/getmondo/d/i;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lco/uk/getmondo/d/i;->d()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()Z
    .locals 2

    .prologue
    .line 20
    iget-object v0, p0, Lco/uk/getmondo/api/model/tax_residency/Jurisdiction;->tinTypes:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x2

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c()Lco/uk/getmondo/api/model/tax_residency/Jurisdiction$TinType;
    .locals 2

    .prologue
    .line 23
    iget-object v0, p0, Lco/uk/getmondo/api/model/tax_residency/Jurisdiction;->tinTypes:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/api/model/tax_residency/Jurisdiction$TinType;

    return-object v0
.end method

.method public final d()Lco/uk/getmondo/api/model/tax_residency/Jurisdiction$TinType;
    .locals 2

    .prologue
    .line 26
    iget-object v0, p0, Lco/uk/getmondo/api/model/tax_residency/Jurisdiction;->tinTypes:Ljava/util/List;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lkotlin/a/m;->c(Ljava/util/List;I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/api/model/tax_residency/Jurisdiction$TinType;

    return-object v0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 43
    const/4 v0, 0x0

    return v0
.end method

.method public final e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 9
    iget-object v0, p0, Lco/uk/getmondo/api/model/tax_residency/Jurisdiction;->countryCode:Ljava/lang/String;

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v1, 0x1

    const/4 v0, 0x0

    if-eq p0, p1, :cond_0

    instance-of v2, p1, Lco/uk/getmondo/api/model/tax_residency/Jurisdiction;

    if-eqz v2, :cond_1

    check-cast p1, Lco/uk/getmondo/api/model/tax_residency/Jurisdiction;

    iget-object v2, p0, Lco/uk/getmondo/api/model/tax_residency/Jurisdiction;->countryCode:Ljava/lang/String;

    iget-object v3, p1, Lco/uk/getmondo/api/model/tax_residency/Jurisdiction;->countryCode:Ljava/lang/String;

    invoke-static {v2, v3}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    iget-boolean v2, p0, Lco/uk/getmondo/api/model/tax_residency/Jurisdiction;->reportable:Z

    iget-boolean v3, p1, Lco/uk/getmondo/api/model/tax_residency/Jurisdiction;->reportable:Z

    if-ne v2, v3, :cond_2

    move v2, v1

    :goto_0
    if-eqz v2, :cond_1

    iget-boolean v2, p0, Lco/uk/getmondo/api/model/tax_residency/Jurisdiction;->completed:Z

    iget-boolean v3, p1, Lco/uk/getmondo/api/model/tax_residency/Jurisdiction;->completed:Z

    if-ne v2, v3, :cond_3

    move v2, v1

    :goto_1
    if-eqz v2, :cond_1

    iget-boolean v2, p0, Lco/uk/getmondo/api/model/tax_residency/Jurisdiction;->allowNoTin:Z

    iget-boolean v3, p1, Lco/uk/getmondo/api/model/tax_residency/Jurisdiction;->allowNoTin:Z

    if-ne v2, v3, :cond_4

    move v2, v1

    :goto_2
    if-eqz v2, :cond_1

    iget-object v2, p0, Lco/uk/getmondo/api/model/tax_residency/Jurisdiction;->tinTypes:Ljava/util/List;

    iget-object v3, p1, Lco/uk/getmondo/api/model/tax_residency/Jurisdiction;->tinTypes:Ljava/util/List;

    invoke-static {v2, v3}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    move v0, v1

    :cond_1
    return v0

    :cond_2
    move v2, v0

    goto :goto_0

    :cond_3
    move v2, v0

    goto :goto_1

    :cond_4
    move v2, v0

    goto :goto_2
.end method

.method public final f()Z
    .locals 1

    .prologue
    .line 10
    iget-boolean v0, p0, Lco/uk/getmondo/api/model/tax_residency/Jurisdiction;->reportable:Z

    return v0
.end method

.method public final g()Z
    .locals 1

    .prologue
    .line 11
    iget-boolean v0, p0, Lco/uk/getmondo/api/model/tax_residency/Jurisdiction;->completed:Z

    return v0
.end method

.method public final h()Z
    .locals 1

    .prologue
    .line 12
    iget-boolean v0, p0, Lco/uk/getmondo/api/model/tax_residency/Jurisdiction;->allowNoTin:Z

    return v0
.end method

.method public hashCode()I
    .locals 4

    const/4 v1, 0x0

    const/4 v2, 0x1

    iget-object v0, p0, Lco/uk/getmondo/api/model/tax_residency/Jurisdiction;->countryCode:Ljava/lang/String;

    if-eqz v0, :cond_3

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    :goto_0
    mul-int/lit8 v3, v0, 0x1f

    iget-boolean v0, p0, Lco/uk/getmondo/api/model/tax_residency/Jurisdiction;->reportable:Z

    if-eqz v0, :cond_0

    move v0, v2

    :cond_0
    add-int/2addr v0, v3

    mul-int/lit8 v3, v0, 0x1f

    iget-boolean v0, p0, Lco/uk/getmondo/api/model/tax_residency/Jurisdiction;->completed:Z

    if-eqz v0, :cond_1

    move v0, v2

    :cond_1
    add-int/2addr v0, v3

    mul-int/lit8 v3, v0, 0x1f

    iget-boolean v0, p0, Lco/uk/getmondo/api/model/tax_residency/Jurisdiction;->allowNoTin:Z

    if-eqz v0, :cond_4

    :goto_1
    add-int v0, v3, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lco/uk/getmondo/api/model/tax_residency/Jurisdiction;->tinTypes:Ljava/util/List;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_2
    add-int/2addr v0, v1

    return v0

    :cond_3
    move v0, v1

    goto :goto_0

    :cond_4
    move v2, v0

    goto :goto_1
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Jurisdiction(countryCode="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/api/model/tax_residency/Jurisdiction;->countryCode:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", reportable="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lco/uk/getmondo/api/model/tax_residency/Jurisdiction;->reportable:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", completed="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lco/uk/getmondo/api/model/tax_residency/Jurisdiction;->completed:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", allowNoTin="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lco/uk/getmondo/api/model/tax_residency/Jurisdiction;->allowNoTin:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", tinTypes="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/api/model/tax_residency/Jurisdiction;->tinTypes:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    const-string v0, "dest"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 46
    iget-object v0, p0, Lco/uk/getmondo/api/model/tax_residency/Jurisdiction;->countryCode:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 47
    iget-boolean v0, p0, Lco/uk/getmondo/api/model/tax_residency/Jurisdiction;->reportable:Z

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 48
    iget-boolean v0, p0, Lco/uk/getmondo/api/model/tax_residency/Jurisdiction;->completed:Z

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 49
    iget-boolean v0, p0, Lco/uk/getmondo/api/model/tax_residency/Jurisdiction;->allowNoTin:Z

    if-eqz v0, :cond_2

    :goto_2
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 50
    iget-object v0, p0, Lco/uk/getmondo/api/model/tax_residency/Jurisdiction;->tinTypes:Ljava/util/List;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    .line 51
    return-void

    :cond_0
    move v0, v2

    .line 47
    goto :goto_0

    :cond_1
    move v0, v2

    .line 48
    goto :goto_1

    :cond_2
    move v1, v2

    .line 49
    goto :goto_2
.end method
