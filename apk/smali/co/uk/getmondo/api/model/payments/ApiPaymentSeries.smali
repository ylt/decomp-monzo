.class public final Lco/uk/getmondo/api/model/payments/ApiPaymentSeries;
.super Ljava/lang/Object;
.source "ApiPaymentSeries.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lco/uk/getmondo/api/model/payments/ApiPaymentSeries$FpsSchemeData;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u00008\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0002\n\u0002\u0010\t\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u001e\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0002\u0008\u0003\u0008\u0086\u0008\u0018\u00002\u00020\u0001:\u00010BW\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u0006\u0010\u0007\u001a\u00020\u0003\u0012\u0006\u0010\u0008\u001a\u00020\u0003\u0012\u0006\u0010\t\u001a\u00020\n\u0012\u0006\u0010\u000b\u001a\u00020\u000c\u0012\u0006\u0010\r\u001a\u00020\u0003\u0012\u0006\u0010\u000e\u001a\u00020\u000c\u0012\u0008\u0010\u000f\u001a\u0004\u0018\u00010\u000c\u00a2\u0006\u0002\u0010\u0010J\t\u0010\u001f\u001a\u00020\u0003H\u00c6\u0003J\u000b\u0010 \u001a\u0004\u0018\u00010\u000cH\u00c6\u0003J\t\u0010!\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\"\u001a\u00020\u0006H\u00c6\u0003J\t\u0010#\u001a\u00020\u0003H\u00c6\u0003J\t\u0010$\u001a\u00020\u0003H\u00c6\u0003J\t\u0010%\u001a\u00020\nH\u00c6\u0003J\t\u0010&\u001a\u00020\u000cH\u00c6\u0003J\t\u0010\'\u001a\u00020\u0003H\u00c6\u0003J\t\u0010(\u001a\u00020\u000cH\u00c6\u0003Jo\u0010)\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0005\u001a\u00020\u00062\u0008\u0008\u0002\u0010\u0007\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0008\u001a\u00020\u00032\u0008\u0008\u0002\u0010\t\u001a\u00020\n2\u0008\u0008\u0002\u0010\u000b\u001a\u00020\u000c2\u0008\u0008\u0002\u0010\r\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u000e\u001a\u00020\u000c2\n\u0008\u0002\u0010\u000f\u001a\u0004\u0018\u00010\u000cH\u00c6\u0001J\u0013\u0010*\u001a\u00020+2\u0008\u0010,\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u0010-\u001a\u00020.H\u00d6\u0001J\t\u0010/\u001a\u00020\u0003H\u00d6\u0001R\u0011\u0010\u0004\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0011\u0010\u0012R\u0011\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0013\u0010\u0014R\u0011\u0010\u0007\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0015\u0010\u0012R\u0013\u0010\u000f\u001a\u0004\u0018\u00010\u000c\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0016\u0010\u0017R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0018\u0010\u0012R\u0011\u0010\r\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0019\u0010\u0012R\u0011\u0010\u000e\u001a\u00020\u000c\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001a\u0010\u0017R\u0011\u0010\u0008\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001b\u0010\u0012R\u0011\u0010\t\u001a\u00020\n\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001c\u0010\u001dR\u0011\u0010\u000b\u001a\u00020\u000c\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001e\u0010\u0017\u00a8\u00061"
    }
    d2 = {
        "Lco/uk/getmondo/api/model/payments/ApiPaymentSeries;",
        "",
        "id",
        "",
        "accountId",
        "amount",
        "",
        "currency",
        "scheme",
        "schemeData",
        "Lco/uk/getmondo/api/model/payments/ApiPaymentSeries$FpsSchemeData;",
        "startDate",
        "Lorg/threeten/bp/LocalDate;",
        "intervalType",
        "nextIterationDate",
        "endDate",
        "(Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;Lco/uk/getmondo/api/model/payments/ApiPaymentSeries$FpsSchemeData;Lorg/threeten/bp/LocalDate;Ljava/lang/String;Lorg/threeten/bp/LocalDate;Lorg/threeten/bp/LocalDate;)V",
        "getAccountId",
        "()Ljava/lang/String;",
        "getAmount",
        "()J",
        "getCurrency",
        "getEndDate",
        "()Lorg/threeten/bp/LocalDate;",
        "getId",
        "getIntervalType",
        "getNextIterationDate",
        "getScheme",
        "getSchemeData",
        "()Lco/uk/getmondo/api/model/payments/ApiPaymentSeries$FpsSchemeData;",
        "getStartDate",
        "component1",
        "component10",
        "component2",
        "component3",
        "component4",
        "component5",
        "component6",
        "component7",
        "component8",
        "component9",
        "copy",
        "equals",
        "",
        "other",
        "hashCode",
        "",
        "toString",
        "FpsSchemeData",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field private final accountId:Ljava/lang/String;

.field private final amount:J

.field private final currency:Ljava/lang/String;

.field private final endDate:Lorg/threeten/bp/LocalDate;

.field private final id:Ljava/lang/String;

.field private final intervalType:Ljava/lang/String;

.field private final nextIterationDate:Lorg/threeten/bp/LocalDate;

.field private final scheme:Ljava/lang/String;

.field private final schemeData:Lco/uk/getmondo/api/model/payments/ApiPaymentSeries$FpsSchemeData;

.field private final startDate:Lorg/threeten/bp/LocalDate;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;Lco/uk/getmondo/api/model/payments/ApiPaymentSeries$FpsSchemeData;Lorg/threeten/bp/LocalDate;Ljava/lang/String;Lorg/threeten/bp/LocalDate;Lorg/threeten/bp/LocalDate;)V
    .locals 1

    .prologue
    const-string v0, "id"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "accountId"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "currency"

    invoke-static {p5, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "scheme"

    invoke-static {p6, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "schemeData"

    invoke-static {p7, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "startDate"

    invoke-static {p8, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "intervalType"

    invoke-static {p9, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "nextIterationDate"

    invoke-static {p10, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 5
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lco/uk/getmondo/api/model/payments/ApiPaymentSeries;->id:Ljava/lang/String;

    iput-object p2, p0, Lco/uk/getmondo/api/model/payments/ApiPaymentSeries;->accountId:Ljava/lang/String;

    iput-wide p3, p0, Lco/uk/getmondo/api/model/payments/ApiPaymentSeries;->amount:J

    iput-object p5, p0, Lco/uk/getmondo/api/model/payments/ApiPaymentSeries;->currency:Ljava/lang/String;

    iput-object p6, p0, Lco/uk/getmondo/api/model/payments/ApiPaymentSeries;->scheme:Ljava/lang/String;

    iput-object p7, p0, Lco/uk/getmondo/api/model/payments/ApiPaymentSeries;->schemeData:Lco/uk/getmondo/api/model/payments/ApiPaymentSeries$FpsSchemeData;

    iput-object p8, p0, Lco/uk/getmondo/api/model/payments/ApiPaymentSeries;->startDate:Lorg/threeten/bp/LocalDate;

    iput-object p9, p0, Lco/uk/getmondo/api/model/payments/ApiPaymentSeries;->intervalType:Ljava/lang/String;

    iput-object p10, p0, Lco/uk/getmondo/api/model/payments/ApiPaymentSeries;->nextIterationDate:Lorg/threeten/bp/LocalDate;

    iput-object p11, p0, Lco/uk/getmondo/api/model/payments/ApiPaymentSeries;->endDate:Lorg/threeten/bp/LocalDate;

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 6
    iget-object v0, p0, Lco/uk/getmondo/api/model/payments/ApiPaymentSeries;->id:Ljava/lang/String;

    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 7
    iget-object v0, p0, Lco/uk/getmondo/api/model/payments/ApiPaymentSeries;->accountId:Ljava/lang/String;

    return-object v0
.end method

.method public final c()J
    .locals 2

    .prologue
    .line 8
    iget-wide v0, p0, Lco/uk/getmondo/api/model/payments/ApiPaymentSeries;->amount:J

    return-wide v0
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 9
    iget-object v0, p0, Lco/uk/getmondo/api/model/payments/ApiPaymentSeries;->currency:Ljava/lang/String;

    return-object v0
.end method

.method public final e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 10
    iget-object v0, p0, Lco/uk/getmondo/api/model/payments/ApiPaymentSeries;->scheme:Ljava/lang/String;

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 6

    const/4 v1, 0x1

    const/4 v0, 0x0

    if-eq p0, p1, :cond_0

    instance-of v2, p1, Lco/uk/getmondo/api/model/payments/ApiPaymentSeries;

    if-eqz v2, :cond_1

    check-cast p1, Lco/uk/getmondo/api/model/payments/ApiPaymentSeries;

    iget-object v2, p0, Lco/uk/getmondo/api/model/payments/ApiPaymentSeries;->id:Ljava/lang/String;

    iget-object v3, p1, Lco/uk/getmondo/api/model/payments/ApiPaymentSeries;->id:Ljava/lang/String;

    invoke-static {v2, v3}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lco/uk/getmondo/api/model/payments/ApiPaymentSeries;->accountId:Ljava/lang/String;

    iget-object v3, p1, Lco/uk/getmondo/api/model/payments/ApiPaymentSeries;->accountId:Ljava/lang/String;

    invoke-static {v2, v3}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    iget-wide v2, p0, Lco/uk/getmondo/api/model/payments/ApiPaymentSeries;->amount:J

    iget-wide v4, p1, Lco/uk/getmondo/api/model/payments/ApiPaymentSeries;->amount:J

    cmp-long v2, v2, v4

    if-nez v2, :cond_2

    move v2, v1

    :goto_0
    if-eqz v2, :cond_1

    iget-object v2, p0, Lco/uk/getmondo/api/model/payments/ApiPaymentSeries;->currency:Ljava/lang/String;

    iget-object v3, p1, Lco/uk/getmondo/api/model/payments/ApiPaymentSeries;->currency:Ljava/lang/String;

    invoke-static {v2, v3}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lco/uk/getmondo/api/model/payments/ApiPaymentSeries;->scheme:Ljava/lang/String;

    iget-object v3, p1, Lco/uk/getmondo/api/model/payments/ApiPaymentSeries;->scheme:Ljava/lang/String;

    invoke-static {v2, v3}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lco/uk/getmondo/api/model/payments/ApiPaymentSeries;->schemeData:Lco/uk/getmondo/api/model/payments/ApiPaymentSeries$FpsSchemeData;

    iget-object v3, p1, Lco/uk/getmondo/api/model/payments/ApiPaymentSeries;->schemeData:Lco/uk/getmondo/api/model/payments/ApiPaymentSeries$FpsSchemeData;

    invoke-static {v2, v3}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lco/uk/getmondo/api/model/payments/ApiPaymentSeries;->startDate:Lorg/threeten/bp/LocalDate;

    iget-object v3, p1, Lco/uk/getmondo/api/model/payments/ApiPaymentSeries;->startDate:Lorg/threeten/bp/LocalDate;

    invoke-static {v2, v3}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lco/uk/getmondo/api/model/payments/ApiPaymentSeries;->intervalType:Ljava/lang/String;

    iget-object v3, p1, Lco/uk/getmondo/api/model/payments/ApiPaymentSeries;->intervalType:Ljava/lang/String;

    invoke-static {v2, v3}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lco/uk/getmondo/api/model/payments/ApiPaymentSeries;->nextIterationDate:Lorg/threeten/bp/LocalDate;

    iget-object v3, p1, Lco/uk/getmondo/api/model/payments/ApiPaymentSeries;->nextIterationDate:Lorg/threeten/bp/LocalDate;

    invoke-static {v2, v3}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lco/uk/getmondo/api/model/payments/ApiPaymentSeries;->endDate:Lorg/threeten/bp/LocalDate;

    iget-object v3, p1, Lco/uk/getmondo/api/model/payments/ApiPaymentSeries;->endDate:Lorg/threeten/bp/LocalDate;

    invoke-static {v2, v3}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    move v0, v1

    :cond_1
    return v0

    :cond_2
    move v2, v0

    goto :goto_0
.end method

.method public final f()Lco/uk/getmondo/api/model/payments/ApiPaymentSeries$FpsSchemeData;
    .locals 1

    .prologue
    .line 11
    iget-object v0, p0, Lco/uk/getmondo/api/model/payments/ApiPaymentSeries;->schemeData:Lco/uk/getmondo/api/model/payments/ApiPaymentSeries$FpsSchemeData;

    return-object v0
.end method

.method public final g()Lorg/threeten/bp/LocalDate;
    .locals 1

    .prologue
    .line 12
    iget-object v0, p0, Lco/uk/getmondo/api/model/payments/ApiPaymentSeries;->startDate:Lorg/threeten/bp/LocalDate;

    return-object v0
.end method

.method public final h()Ljava/lang/String;
    .locals 1

    .prologue
    .line 13
    iget-object v0, p0, Lco/uk/getmondo/api/model/payments/ApiPaymentSeries;->intervalType:Ljava/lang/String;

    return-object v0
.end method

.method public hashCode()I
    .locals 6

    const/4 v1, 0x0

    iget-object v0, p0, Lco/uk/getmondo/api/model/payments/ApiPaymentSeries;->id:Ljava/lang/String;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    :goto_0
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lco/uk/getmondo/api/model/payments/ApiPaymentSeries;->accountId:Ljava/lang/String;

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-wide v2, p0, Lco/uk/getmondo/api/model/payments/ApiPaymentSeries;->amount:J

    const/16 v4, 0x20

    ushr-long v4, v2, v4

    xor-long/2addr v2, v4

    long-to-int v2, v2

    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lco/uk/getmondo/api/model/payments/ApiPaymentSeries;->currency:Ljava/lang/String;

    if-eqz v0, :cond_3

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    :goto_2
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lco/uk/getmondo/api/model/payments/ApiPaymentSeries;->scheme:Ljava/lang/String;

    if-eqz v0, :cond_4

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    :goto_3
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lco/uk/getmondo/api/model/payments/ApiPaymentSeries;->schemeData:Lco/uk/getmondo/api/model/payments/ApiPaymentSeries$FpsSchemeData;

    if-eqz v0, :cond_5

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    :goto_4
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lco/uk/getmondo/api/model/payments/ApiPaymentSeries;->startDate:Lorg/threeten/bp/LocalDate;

    if-eqz v0, :cond_6

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    :goto_5
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lco/uk/getmondo/api/model/payments/ApiPaymentSeries;->intervalType:Ljava/lang/String;

    if-eqz v0, :cond_7

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    :goto_6
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lco/uk/getmondo/api/model/payments/ApiPaymentSeries;->nextIterationDate:Lorg/threeten/bp/LocalDate;

    if-eqz v0, :cond_8

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    :goto_7
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lco/uk/getmondo/api/model/payments/ApiPaymentSeries;->endDate:Lorg/threeten/bp/LocalDate;

    if-eqz v2, :cond_0

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_0
    add-int/2addr v0, v1

    return v0

    :cond_1
    move v0, v1

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_1

    :cond_3
    move v0, v1

    goto :goto_2

    :cond_4
    move v0, v1

    goto :goto_3

    :cond_5
    move v0, v1

    goto :goto_4

    :cond_6
    move v0, v1

    goto :goto_5

    :cond_7
    move v0, v1

    goto :goto_6

    :cond_8
    move v0, v1

    goto :goto_7
.end method

.method public final i()Lorg/threeten/bp/LocalDate;
    .locals 1

    .prologue
    .line 14
    iget-object v0, p0, Lco/uk/getmondo/api/model/payments/ApiPaymentSeries;->nextIterationDate:Lorg/threeten/bp/LocalDate;

    return-object v0
.end method

.method public final j()Lorg/threeten/bp/LocalDate;
    .locals 1

    .prologue
    .line 15
    iget-object v0, p0, Lco/uk/getmondo/api/model/payments/ApiPaymentSeries;->endDate:Lorg/threeten/bp/LocalDate;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "ApiPaymentSeries(id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/api/model/payments/ApiPaymentSeries;->id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", accountId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/api/model/payments/ApiPaymentSeries;->accountId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", amount="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lco/uk/getmondo/api/model/payments/ApiPaymentSeries;->amount:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", currency="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/api/model/payments/ApiPaymentSeries;->currency:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", scheme="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/api/model/payments/ApiPaymentSeries;->scheme:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", schemeData="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/api/model/payments/ApiPaymentSeries;->schemeData:Lco/uk/getmondo/api/model/payments/ApiPaymentSeries$FpsSchemeData;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", startDate="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/api/model/payments/ApiPaymentSeries;->startDate:Lorg/threeten/bp/LocalDate;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", intervalType="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/api/model/payments/ApiPaymentSeries;->intervalType:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", nextIterationDate="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/api/model/payments/ApiPaymentSeries;->nextIterationDate:Lorg/threeten/bp/LocalDate;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", endDate="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/api/model/payments/ApiPaymentSeries;->endDate:Lorg/threeten/bp/LocalDate;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
