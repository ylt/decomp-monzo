.class public final Lco/uk/getmondo/api/model/signup/SignupInfo;
.super Ljava/lang/Object;
.source "SignupInfo.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lco/uk/getmondo/api/model/signup/SignupInfo$Status;,
        Lco/uk/getmondo/api/model/signup/SignupInfo$Stage;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000.\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\t\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0003\u0008\u0086\u0008\u0018\u00002\u00020\u0001:\u0002\u0015\u0016B\u0017\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\t\u0010\u000b\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u000c\u001a\u00020\u0005H\u00c6\u0003J\u001d\u0010\r\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u0005H\u00c6\u0001J\u0013\u0010\u000e\u001a\u00020\u000f2\u0008\u0010\u0010\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u0010\u0011\u001a\u00020\u0012H\u00d6\u0001J\t\u0010\u0013\u001a\u00020\u0014H\u00d6\u0001R\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0007\u0010\u0008R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\t\u0010\n\u00a8\u0006\u0017"
    }
    d2 = {
        "Lco/uk/getmondo/api/model/signup/SignupInfo;",
        "",
        "status",
        "Lco/uk/getmondo/api/model/signup/SignupInfo$Status;",
        "stage",
        "Lco/uk/getmondo/api/model/signup/SignupInfo$Stage;",
        "(Lco/uk/getmondo/api/model/signup/SignupInfo$Status;Lco/uk/getmondo/api/model/signup/SignupInfo$Stage;)V",
        "getStage",
        "()Lco/uk/getmondo/api/model/signup/SignupInfo$Stage;",
        "getStatus",
        "()Lco/uk/getmondo/api/model/signup/SignupInfo$Status;",
        "component1",
        "component2",
        "copy",
        "equals",
        "",
        "other",
        "hashCode",
        "",
        "toString",
        "",
        "Stage",
        "Status",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field private final stage:Lco/uk/getmondo/api/model/signup/SignupInfo$Stage;

.field private final status:Lco/uk/getmondo/api/model/signup/SignupInfo$Status;


# direct methods
.method public constructor <init>(Lco/uk/getmondo/api/model/signup/SignupInfo$Status;Lco/uk/getmondo/api/model/signup/SignupInfo$Stage;)V
    .locals 1

    .prologue
    const-string v0, "status"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "stage"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 5
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lco/uk/getmondo/api/model/signup/SignupInfo;->status:Lco/uk/getmondo/api/model/signup/SignupInfo$Status;

    iput-object p2, p0, Lco/uk/getmondo/api/model/signup/SignupInfo;->stage:Lco/uk/getmondo/api/model/signup/SignupInfo$Stage;

    return-void
.end method

.method public synthetic constructor <init>(Lco/uk/getmondo/api/model/signup/SignupInfo$Status;Lco/uk/getmondo/api/model/signup/SignupInfo$Stage;ILkotlin/d/b/i;)V
    .locals 1

    .prologue
    and-int/lit8 v0, p3, 0x2

    if-eqz v0, :cond_0

    .line 7
    sget-object p2, Lco/uk/getmondo/api/model/signup/SignupInfo$Stage;->NONE:Lco/uk/getmondo/api/model/signup/SignupInfo$Stage;

    :cond_0
    invoke-direct {p0, p1, p2}, Lco/uk/getmondo/api/model/signup/SignupInfo;-><init>(Lco/uk/getmondo/api/model/signup/SignupInfo$Status;Lco/uk/getmondo/api/model/signup/SignupInfo$Stage;)V

    return-void
.end method

.method public static bridge synthetic a(Lco/uk/getmondo/api/model/signup/SignupInfo;Lco/uk/getmondo/api/model/signup/SignupInfo$Status;Lco/uk/getmondo/api/model/signup/SignupInfo$Stage;ILjava/lang/Object;)Lco/uk/getmondo/api/model/signup/SignupInfo;
    .locals 1

    and-int/lit8 v0, p3, 0x1

    if-eqz v0, :cond_0

    iget-object p1, p0, Lco/uk/getmondo/api/model/signup/SignupInfo;->status:Lco/uk/getmondo/api/model/signup/SignupInfo$Status;

    :cond_0
    and-int/lit8 v0, p3, 0x2

    if-eqz v0, :cond_1

    iget-object p2, p0, Lco/uk/getmondo/api/model/signup/SignupInfo;->stage:Lco/uk/getmondo/api/model/signup/SignupInfo$Stage;

    :cond_1
    invoke-virtual {p0, p1, p2}, Lco/uk/getmondo/api/model/signup/SignupInfo;->a(Lco/uk/getmondo/api/model/signup/SignupInfo$Status;Lco/uk/getmondo/api/model/signup/SignupInfo$Stage;)Lco/uk/getmondo/api/model/signup/SignupInfo;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a()Lco/uk/getmondo/api/model/signup/SignupInfo$Status;
    .locals 1

    .prologue
    .line 6
    iget-object v0, p0, Lco/uk/getmondo/api/model/signup/SignupInfo;->status:Lco/uk/getmondo/api/model/signup/SignupInfo$Status;

    return-object v0
.end method

.method public final a(Lco/uk/getmondo/api/model/signup/SignupInfo$Status;Lco/uk/getmondo/api/model/signup/SignupInfo$Stage;)Lco/uk/getmondo/api/model/signup/SignupInfo;
    .locals 1

    const-string v0, "status"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "stage"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lco/uk/getmondo/api/model/signup/SignupInfo;

    invoke-direct {v0, p1, p2}, Lco/uk/getmondo/api/model/signup/SignupInfo;-><init>(Lco/uk/getmondo/api/model/signup/SignupInfo$Status;Lco/uk/getmondo/api/model/signup/SignupInfo$Stage;)V

    return-object v0
.end method

.method public final b()Lco/uk/getmondo/api/model/signup/SignupInfo$Stage;
    .locals 1

    .prologue
    .line 7
    iget-object v0, p0, Lco/uk/getmondo/api/model/signup/SignupInfo;->stage:Lco/uk/getmondo/api/model/signup/SignupInfo$Stage;

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_0

    instance-of v0, p1, Lco/uk/getmondo/api/model/signup/SignupInfo;

    if-eqz v0, :cond_1

    check-cast p1, Lco/uk/getmondo/api/model/signup/SignupInfo;

    iget-object v0, p0, Lco/uk/getmondo/api/model/signup/SignupInfo;->status:Lco/uk/getmondo/api/model/signup/SignupInfo$Status;

    iget-object v1, p1, Lco/uk/getmondo/api/model/signup/SignupInfo;->status:Lco/uk/getmondo/api/model/signup/SignupInfo$Status;

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lco/uk/getmondo/api/model/signup/SignupInfo;->stage:Lco/uk/getmondo/api/model/signup/SignupInfo$Stage;

    iget-object v1, p1, Lco/uk/getmondo/api/model/signup/SignupInfo;->stage:Lco/uk/getmondo/api/model/signup/SignupInfo$Stage;

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    const/4 v1, 0x0

    iget-object v0, p0, Lco/uk/getmondo/api/model/signup/SignupInfo;->status:Lco/uk/getmondo/api/model/signup/SignupInfo$Status;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lco/uk/getmondo/api/model/signup/SignupInfo;->stage:Lco/uk/getmondo/api/model/signup/SignupInfo$Stage;

    if-eqz v2, :cond_0

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_0
    add-int/2addr v0, v1

    return v0

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "SignupInfo(status="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/api/model/signup/SignupInfo;->status:Lco/uk/getmondo/api/model/signup/SignupInfo$Status;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", stage="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/api/model/signup/SignupInfo;->stage:Lco/uk/getmondo/api/model/signup/SignupInfo$Stage;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
