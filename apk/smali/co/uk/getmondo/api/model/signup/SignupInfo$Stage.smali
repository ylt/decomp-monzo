.class public final enum Lco/uk/getmondo/api/model/signup/SignupInfo$Stage;
.super Ljava/lang/Enum;
.source "SignupInfo.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lco/uk/getmondo/api/model/signup/SignupInfo;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Stage"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lco/uk/getmondo/api/model/signup/SignupInfo$Stage;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000\u000c\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\u0008\u0010\u0008\u0086\u0001\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00000\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002j\u0002\u0008\u0003j\u0002\u0008\u0004j\u0002\u0008\u0005j\u0002\u0008\u0006j\u0002\u0008\u0007j\u0002\u0008\u0008j\u0002\u0008\tj\u0002\u0008\nj\u0002\u0008\u000bj\u0002\u0008\u000cj\u0002\u0008\rj\u0002\u0008\u000ej\u0002\u0008\u000fj\u0002\u0008\u0010\u00a8\u0006\u0011"
    }
    d2 = {
        "Lco/uk/getmondo/api/model/signup/SignupInfo$Stage;",
        "",
        "(Ljava/lang/String;I)V",
        "PROFILE_DETAILS",
        "PHONE_VERIFICATION",
        "MARKETING_OPT_IN",
        "LEGAL_DOCUMENTS",
        "IDENTITY_VERIFICATION",
        "TAX_RESIDENCY",
        "CARD_ORDER",
        "DEVICE_AUTHENTICATION_ENROLMENT",
        "CARD_ACTIVATION",
        "PENDING",
        "DONE",
        "NONE",
        "WAIT_LIST_SIGNUP",
        "WAIT_LIST",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lco/uk/getmondo/api/model/signup/SignupInfo$Stage;

.field public static final enum CARD_ACTIVATION:Lco/uk/getmondo/api/model/signup/SignupInfo$Stage;
    .annotation runtime Lcom/squareup/moshi/h;
        a = "card-activation"
    .end annotation
.end field

.field public static final enum CARD_ORDER:Lco/uk/getmondo/api/model/signup/SignupInfo$Stage;
    .annotation runtime Lcom/squareup/moshi/h;
        a = "card-order"
    .end annotation
.end field

.field public static final enum DEVICE_AUTHENTICATION_ENROLMENT:Lco/uk/getmondo/api/model/signup/SignupInfo$Stage;
    .annotation runtime Lcom/squareup/moshi/h;
        a = "device-authentication-enrolment"
    .end annotation
.end field

.field public static final enum DONE:Lco/uk/getmondo/api/model/signup/SignupInfo$Stage;
    .annotation runtime Lcom/squareup/moshi/h;
        a = "done"
    .end annotation
.end field

.field public static final enum IDENTITY_VERIFICATION:Lco/uk/getmondo/api/model/signup/SignupInfo$Stage;
    .annotation runtime Lcom/squareup/moshi/h;
        a = "identity-verification"
    .end annotation
.end field

.field public static final enum LEGAL_DOCUMENTS:Lco/uk/getmondo/api/model/signup/SignupInfo$Stage;
    .annotation runtime Lcom/squareup/moshi/h;
        a = "legal-documents"
    .end annotation
.end field

.field public static final enum MARKETING_OPT_IN:Lco/uk/getmondo/api/model/signup/SignupInfo$Stage;
    .annotation runtime Lcom/squareup/moshi/h;
        a = "marketing-opt-in"
    .end annotation
.end field

.field public static final enum NONE:Lco/uk/getmondo/api/model/signup/SignupInfo$Stage;
    .annotation runtime Lcom/squareup/moshi/h;
        a = ""
    .end annotation
.end field

.field public static final enum PENDING:Lco/uk/getmondo/api/model/signup/SignupInfo$Stage;
    .annotation runtime Lcom/squareup/moshi/h;
        a = "pending"
    .end annotation
.end field

.field public static final enum PHONE_VERIFICATION:Lco/uk/getmondo/api/model/signup/SignupInfo$Stage;
    .annotation runtime Lcom/squareup/moshi/h;
        a = "phone-verification"
    .end annotation
.end field

.field public static final enum PROFILE_DETAILS:Lco/uk/getmondo/api/model/signup/SignupInfo$Stage;
    .annotation runtime Lcom/squareup/moshi/h;
        a = "profile-details"
    .end annotation
.end field

.field public static final enum TAX_RESIDENCY:Lco/uk/getmondo/api/model/signup/SignupInfo$Stage;
    .annotation runtime Lcom/squareup/moshi/h;
        a = "tax-residency"
    .end annotation
.end field

.field public static final enum WAIT_LIST:Lco/uk/getmondo/api/model/signup/SignupInfo$Stage;

.field public static final enum WAIT_LIST_SIGNUP:Lco/uk/getmondo/api/model/signup/SignupInfo$Stage;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    const/16 v0, 0xe

    new-array v0, v0, [Lco/uk/getmondo/api/model/signup/SignupInfo$Stage;

    new-instance v1, Lco/uk/getmondo/api/model/signup/SignupInfo$Stage;

    const-string v2, "PROFILE_DETAILS"

    invoke-direct {v1, v2, v3}, Lco/uk/getmondo/api/model/signup/SignupInfo$Stage;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lco/uk/getmondo/api/model/signup/SignupInfo$Stage;->PROFILE_DETAILS:Lco/uk/getmondo/api/model/signup/SignupInfo$Stage;

    aput-object v1, v0, v3

    new-instance v1, Lco/uk/getmondo/api/model/signup/SignupInfo$Stage;

    const-string v2, "PHONE_VERIFICATION"

    invoke-direct {v1, v2, v4}, Lco/uk/getmondo/api/model/signup/SignupInfo$Stage;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lco/uk/getmondo/api/model/signup/SignupInfo$Stage;->PHONE_VERIFICATION:Lco/uk/getmondo/api/model/signup/SignupInfo$Stage;

    aput-object v1, v0, v4

    new-instance v1, Lco/uk/getmondo/api/model/signup/SignupInfo$Stage;

    const-string v2, "MARKETING_OPT_IN"

    invoke-direct {v1, v2, v5}, Lco/uk/getmondo/api/model/signup/SignupInfo$Stage;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lco/uk/getmondo/api/model/signup/SignupInfo$Stage;->MARKETING_OPT_IN:Lco/uk/getmondo/api/model/signup/SignupInfo$Stage;

    aput-object v1, v0, v5

    new-instance v1, Lco/uk/getmondo/api/model/signup/SignupInfo$Stage;

    const-string v2, "LEGAL_DOCUMENTS"

    invoke-direct {v1, v2, v6}, Lco/uk/getmondo/api/model/signup/SignupInfo$Stage;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lco/uk/getmondo/api/model/signup/SignupInfo$Stage;->LEGAL_DOCUMENTS:Lco/uk/getmondo/api/model/signup/SignupInfo$Stage;

    aput-object v1, v0, v6

    new-instance v1, Lco/uk/getmondo/api/model/signup/SignupInfo$Stage;

    const-string v2, "IDENTITY_VERIFICATION"

    invoke-direct {v1, v2, v7}, Lco/uk/getmondo/api/model/signup/SignupInfo$Stage;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lco/uk/getmondo/api/model/signup/SignupInfo$Stage;->IDENTITY_VERIFICATION:Lco/uk/getmondo/api/model/signup/SignupInfo$Stage;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    new-instance v2, Lco/uk/getmondo/api/model/signup/SignupInfo$Stage;

    const-string v3, "TAX_RESIDENCY"

    const/4 v4, 0x5

    invoke-direct {v2, v3, v4}, Lco/uk/getmondo/api/model/signup/SignupInfo$Stage;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lco/uk/getmondo/api/model/signup/SignupInfo$Stage;->TAX_RESIDENCY:Lco/uk/getmondo/api/model/signup/SignupInfo$Stage;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    new-instance v2, Lco/uk/getmondo/api/model/signup/SignupInfo$Stage;

    const-string v3, "CARD_ORDER"

    const/4 v4, 0x6

    invoke-direct {v2, v3, v4}, Lco/uk/getmondo/api/model/signup/SignupInfo$Stage;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lco/uk/getmondo/api/model/signup/SignupInfo$Stage;->CARD_ORDER:Lco/uk/getmondo/api/model/signup/SignupInfo$Stage;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    new-instance v2, Lco/uk/getmondo/api/model/signup/SignupInfo$Stage;

    const-string v3, "DEVICE_AUTHENTICATION_ENROLMENT"

    const/4 v4, 0x7

    invoke-direct {v2, v3, v4}, Lco/uk/getmondo/api/model/signup/SignupInfo$Stage;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lco/uk/getmondo/api/model/signup/SignupInfo$Stage;->DEVICE_AUTHENTICATION_ENROLMENT:Lco/uk/getmondo/api/model/signup/SignupInfo$Stage;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    new-instance v2, Lco/uk/getmondo/api/model/signup/SignupInfo$Stage;

    const-string v3, "CARD_ACTIVATION"

    const/16 v4, 0x8

    invoke-direct {v2, v3, v4}, Lco/uk/getmondo/api/model/signup/SignupInfo$Stage;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lco/uk/getmondo/api/model/signup/SignupInfo$Stage;->CARD_ACTIVATION:Lco/uk/getmondo/api/model/signup/SignupInfo$Stage;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    new-instance v2, Lco/uk/getmondo/api/model/signup/SignupInfo$Stage;

    const-string v3, "PENDING"

    const/16 v4, 0x9

    invoke-direct {v2, v3, v4}, Lco/uk/getmondo/api/model/signup/SignupInfo$Stage;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lco/uk/getmondo/api/model/signup/SignupInfo$Stage;->PENDING:Lco/uk/getmondo/api/model/signup/SignupInfo$Stage;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    new-instance v2, Lco/uk/getmondo/api/model/signup/SignupInfo$Stage;

    const-string v3, "DONE"

    const/16 v4, 0xa

    invoke-direct {v2, v3, v4}, Lco/uk/getmondo/api/model/signup/SignupInfo$Stage;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lco/uk/getmondo/api/model/signup/SignupInfo$Stage;->DONE:Lco/uk/getmondo/api/model/signup/SignupInfo$Stage;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    new-instance v2, Lco/uk/getmondo/api/model/signup/SignupInfo$Stage;

    const-string v3, "NONE"

    const/16 v4, 0xb

    invoke-direct {v2, v3, v4}, Lco/uk/getmondo/api/model/signup/SignupInfo$Stage;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lco/uk/getmondo/api/model/signup/SignupInfo$Stage;->NONE:Lco/uk/getmondo/api/model/signup/SignupInfo$Stage;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    new-instance v2, Lco/uk/getmondo/api/model/signup/SignupInfo$Stage;

    const-string v3, "WAIT_LIST_SIGNUP"

    const/16 v4, 0xc

    invoke-direct {v2, v3, v4}, Lco/uk/getmondo/api/model/signup/SignupInfo$Stage;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lco/uk/getmondo/api/model/signup/SignupInfo$Stage;->WAIT_LIST_SIGNUP:Lco/uk/getmondo/api/model/signup/SignupInfo$Stage;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    new-instance v2, Lco/uk/getmondo/api/model/signup/SignupInfo$Stage;

    const-string v3, "WAIT_LIST"

    const/16 v4, 0xd

    invoke-direct {v2, v3, v4}, Lco/uk/getmondo/api/model/signup/SignupInfo$Stage;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lco/uk/getmondo/api/model/signup/SignupInfo$Stage;->WAIT_LIST:Lco/uk/getmondo/api/model/signup/SignupInfo$Stage;

    aput-object v2, v0, v1

    sput-object v0, Lco/uk/getmondo/api/model/signup/SignupInfo$Stage;->$VALUES:[Lco/uk/getmondo/api/model/signup/SignupInfo$Stage;

    return-void
.end method

.method protected constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 22
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lco/uk/getmondo/api/model/signup/SignupInfo$Stage;
    .locals 1

    const-class v0, Lco/uk/getmondo/api/model/signup/SignupInfo$Stage;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/api/model/signup/SignupInfo$Stage;

    return-object v0
.end method

.method public static values()[Lco/uk/getmondo/api/model/signup/SignupInfo$Stage;
    .locals 1

    sget-object v0, Lco/uk/getmondo/api/model/signup/SignupInfo$Stage;->$VALUES:[Lco/uk/getmondo/api/model/signup/SignupInfo$Stage;

    invoke-virtual {v0}, [Lco/uk/getmondo/api/model/signup/SignupInfo$Stage;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lco/uk/getmondo/api/model/signup/SignupInfo$Stage;

    return-object v0
.end method
