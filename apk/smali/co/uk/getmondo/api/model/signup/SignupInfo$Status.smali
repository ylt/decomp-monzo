.class public final enum Lco/uk/getmondo/api/model/signup/SignupInfo$Status;
.super Ljava/lang/Enum;
.source "SignupInfo.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lco/uk/getmondo/api/model/signup/SignupInfo;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Status"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lco/uk/getmondo/api/model/signup/SignupInfo$Status;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000\u000c\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\u0008\u0007\u0008\u0086\u0001\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00000\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002j\u0002\u0008\u0003j\u0002\u0008\u0004j\u0002\u0008\u0005j\u0002\u0008\u0006j\u0002\u0008\u0007\u00a8\u0006\u0008"
    }
    d2 = {
        "Lco/uk/getmondo/api/model/signup/SignupInfo$Status;",
        "",
        "(Ljava/lang/String;I)V",
        "NOT_STARTED",
        "IN_PROGRESS",
        "APPROVED",
        "COMPLETED",
        "REJECTED",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lco/uk/getmondo/api/model/signup/SignupInfo$Status;

.field public static final enum APPROVED:Lco/uk/getmondo/api/model/signup/SignupInfo$Status;
    .annotation runtime Lcom/squareup/moshi/h;
        a = "approved"
    .end annotation
.end field

.field public static final enum COMPLETED:Lco/uk/getmondo/api/model/signup/SignupInfo$Status;
    .annotation runtime Lcom/squareup/moshi/h;
        a = "completed"
    .end annotation
.end field

.field public static final enum IN_PROGRESS:Lco/uk/getmondo/api/model/signup/SignupInfo$Status;
    .annotation runtime Lcom/squareup/moshi/h;
        a = "in-progress"
    .end annotation
.end field

.field public static final enum NOT_STARTED:Lco/uk/getmondo/api/model/signup/SignupInfo$Status;
    .annotation runtime Lcom/squareup/moshi/h;
        a = "not-started"
    .end annotation
.end field

.field public static final enum REJECTED:Lco/uk/getmondo/api/model/signup/SignupInfo$Status;
    .annotation runtime Lcom/squareup/moshi/h;
        a = "rejected"
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    const/4 v0, 0x5

    new-array v0, v0, [Lco/uk/getmondo/api/model/signup/SignupInfo$Status;

    new-instance v1, Lco/uk/getmondo/api/model/signup/SignupInfo$Status;

    const-string v2, "NOT_STARTED"

    invoke-direct {v1, v2, v3}, Lco/uk/getmondo/api/model/signup/SignupInfo$Status;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lco/uk/getmondo/api/model/signup/SignupInfo$Status;->NOT_STARTED:Lco/uk/getmondo/api/model/signup/SignupInfo$Status;

    aput-object v1, v0, v3

    new-instance v1, Lco/uk/getmondo/api/model/signup/SignupInfo$Status;

    const-string v2, "IN_PROGRESS"

    invoke-direct {v1, v2, v4}, Lco/uk/getmondo/api/model/signup/SignupInfo$Status;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lco/uk/getmondo/api/model/signup/SignupInfo$Status;->IN_PROGRESS:Lco/uk/getmondo/api/model/signup/SignupInfo$Status;

    aput-object v1, v0, v4

    new-instance v1, Lco/uk/getmondo/api/model/signup/SignupInfo$Status;

    const-string v2, "APPROVED"

    invoke-direct {v1, v2, v5}, Lco/uk/getmondo/api/model/signup/SignupInfo$Status;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lco/uk/getmondo/api/model/signup/SignupInfo$Status;->APPROVED:Lco/uk/getmondo/api/model/signup/SignupInfo$Status;

    aput-object v1, v0, v5

    new-instance v1, Lco/uk/getmondo/api/model/signup/SignupInfo$Status;

    const-string v2, "COMPLETED"

    invoke-direct {v1, v2, v6}, Lco/uk/getmondo/api/model/signup/SignupInfo$Status;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lco/uk/getmondo/api/model/signup/SignupInfo$Status;->COMPLETED:Lco/uk/getmondo/api/model/signup/SignupInfo$Status;

    aput-object v1, v0, v6

    new-instance v1, Lco/uk/getmondo/api/model/signup/SignupInfo$Status;

    const-string v2, "REJECTED"

    invoke-direct {v1, v2, v7}, Lco/uk/getmondo/api/model/signup/SignupInfo$Status;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lco/uk/getmondo/api/model/signup/SignupInfo$Status;->REJECTED:Lco/uk/getmondo/api/model/signup/SignupInfo$Status;

    aput-object v1, v0, v7

    sput-object v0, Lco/uk/getmondo/api/model/signup/SignupInfo$Status;->$VALUES:[Lco/uk/getmondo/api/model/signup/SignupInfo$Status;

    return-void
.end method

.method protected constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 10
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lco/uk/getmondo/api/model/signup/SignupInfo$Status;
    .locals 1

    const-class v0, Lco/uk/getmondo/api/model/signup/SignupInfo$Status;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/api/model/signup/SignupInfo$Status;

    return-object v0
.end method

.method public static values()[Lco/uk/getmondo/api/model/signup/SignupInfo$Status;
    .locals 1

    sget-object v0, Lco/uk/getmondo/api/model/signup/SignupInfo$Status;->$VALUES:[Lco/uk/getmondo/api/model/signup/SignupInfo$Status;

    invoke-virtual {v0}, [Lco/uk/getmondo/api/model/signup/SignupInfo$Status;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lco/uk/getmondo/api/model/signup/SignupInfo$Status;

    return-object v0
.end method
