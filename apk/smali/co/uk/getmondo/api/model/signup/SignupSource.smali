.class public final enum Lco/uk/getmondo/api/model/signup/SignupSource;
.super Ljava/lang/Enum;
.source "SignupSource.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lco/uk/getmondo/api/model/signup/SignupSource;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\t\u0008\u0086\u0001\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00000\u0001B\u000f\u0008\u0002\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0008\u0010\u0007\u001a\u00020\u0003H\u0016R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0005\u0010\u0006j\u0002\u0008\u0008j\u0002\u0008\tj\u0002\u0008\nj\u0002\u0008\u000b\u00a8\u0006\u000c"
    }
    d2 = {
        "Lco/uk/getmondo/api/model/signup/SignupSource;",
        "",
        "apiValue",
        "",
        "(Ljava/lang/String;ILjava/lang/String;)V",
        "getApiValue",
        "()Ljava/lang/String;",
        "toString",
        "LEGACY_PREPAID",
        "LEGACY_PERSONAL_ACCOUNT",
        "PERSONAL_ACCOUNT",
        "SDD_MIGRATION",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lco/uk/getmondo/api/model/signup/SignupSource;

.field public static final enum LEGACY_PERSONAL_ACCOUNT:Lco/uk/getmondo/api/model/signup/SignupSource;

.field public static final enum LEGACY_PREPAID:Lco/uk/getmondo/api/model/signup/SignupSource;

.field public static final enum PERSONAL_ACCOUNT:Lco/uk/getmondo/api/model/signup/SignupSource;

.field public static final enum SDD_MIGRATION:Lco/uk/getmondo/api/model/signup/SignupSource;


# instance fields
.field private final apiValue:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    const/4 v0, 0x4

    new-array v0, v0, [Lco/uk/getmondo/api/model/signup/SignupSource;

    new-instance v1, Lco/uk/getmondo/api/model/signup/SignupSource;

    const-string v2, "LEGACY_PREPAID"

    .line 4
    const-string v3, "legacy_prepaid_signup"

    invoke-direct {v1, v2, v4, v3}, Lco/uk/getmondo/api/model/signup/SignupSource;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lco/uk/getmondo/api/model/signup/SignupSource;->LEGACY_PREPAID:Lco/uk/getmondo/api/model/signup/SignupSource;

    aput-object v1, v0, v4

    new-instance v1, Lco/uk/getmondo/api/model/signup/SignupSource;

    const-string v2, "LEGACY_PERSONAL_ACCOUNT"

    .line 5
    const-string v3, "legacy_personal_account_signup"

    invoke-direct {v1, v2, v5, v3}, Lco/uk/getmondo/api/model/signup/SignupSource;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lco/uk/getmondo/api/model/signup/SignupSource;->LEGACY_PERSONAL_ACCOUNT:Lco/uk/getmondo/api/model/signup/SignupSource;

    aput-object v1, v0, v5

    new-instance v1, Lco/uk/getmondo/api/model/signup/SignupSource;

    const-string v2, "PERSONAL_ACCOUNT"

    .line 6
    const-string v3, "personal_account_signup"

    invoke-direct {v1, v2, v6, v3}, Lco/uk/getmondo/api/model/signup/SignupSource;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lco/uk/getmondo/api/model/signup/SignupSource;->PERSONAL_ACCOUNT:Lco/uk/getmondo/api/model/signup/SignupSource;

    aput-object v1, v0, v6

    new-instance v1, Lco/uk/getmondo/api/model/signup/SignupSource;

    const-string v2, "SDD_MIGRATION"

    .line 7
    const-string v3, "sdd_migration"

    invoke-direct {v1, v2, v7, v3}, Lco/uk/getmondo/api/model/signup/SignupSource;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lco/uk/getmondo/api/model/signup/SignupSource;->SDD_MIGRATION:Lco/uk/getmondo/api/model/signup/SignupSource;

    aput-object v1, v0, v7

    sput-object v0, Lco/uk/getmondo/api/model/signup/SignupSource;->$VALUES:[Lco/uk/getmondo/api/model/signup/SignupSource;

    return-void
.end method

.method protected constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    const-string v0, "apiValue"

    invoke-static {p3, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 3
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput-object p3, p0, Lco/uk/getmondo/api/model/signup/SignupSource;->apiValue:Ljava/lang/String;

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lco/uk/getmondo/api/model/signup/SignupSource;
    .locals 1

    const-class v0, Lco/uk/getmondo/api/model/signup/SignupSource;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/api/model/signup/SignupSource;

    return-object v0
.end method

.method public static values()[Lco/uk/getmondo/api/model/signup/SignupSource;
    .locals 1

    sget-object v0, Lco/uk/getmondo/api/model/signup/SignupSource;->$VALUES:[Lco/uk/getmondo/api/model/signup/SignupSource;

    invoke-virtual {v0}, [Lco/uk/getmondo/api/model/signup/SignupSource;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lco/uk/getmondo/api/model/signup/SignupSource;

    return-object v0
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 3
    iget-object v0, p0, Lco/uk/getmondo/api/model/signup/SignupSource;->apiValue:Ljava/lang/String;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 9
    iget-object v0, p0, Lco/uk/getmondo/api/model/signup/SignupSource;->apiValue:Ljava/lang/String;

    return-object v0
.end method
