.class public final Lco/uk/getmondo/api/model/signup/SignUpProfile;
.super Ljava/lang/Object;
.source "SignUpProfile.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0012\n\u0002\u0010\u0008\n\u0002\u0008\u0002\u0008\u0086\u0008\u0018\u00002\u00020\u0001B9\u0012\u0008\u0008\u0003\u0010\u0002\u001a\u00020\u0003\u0012\u0008\u0008\u0003\u0010\u0004\u001a\u00020\u0003\u0012\u0008\u0008\u0003\u0010\u0005\u001a\u00020\u0006\u0012\n\u0008\u0003\u0010\u0007\u001a\u0004\u0018\u00010\u0008\u0012\u0008\u0008\u0003\u0010\t\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\nJ\t\u0010\u0012\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0013\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0014\u001a\u00020\u0006H\u00c6\u0003J\u000b\u0010\u0015\u001a\u0004\u0018\u00010\u0008H\u00c6\u0003J\t\u0010\u0016\u001a\u00020\u0003H\u00c6\u0003J=\u0010\u0017\u001a\u00020\u00002\u0008\u0008\u0003\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0003\u0010\u0004\u001a\u00020\u00032\u0008\u0008\u0003\u0010\u0005\u001a\u00020\u00062\n\u0008\u0003\u0010\u0007\u001a\u0004\u0018\u00010\u00082\u0008\u0008\u0003\u0010\t\u001a\u00020\u0003H\u00c6\u0001J\u0013\u0010\u0018\u001a\u00020\u00062\u0008\u0010\u0019\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u0010\u001a\u001a\u00020\u001bH\u00d6\u0001J\t\u0010\u001c\u001a\u00020\u0003H\u00d6\u0001R\u0013\u0010\u0007\u001a\u0004\u0018\u00010\u0008\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000b\u0010\u000cR\u0011\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0005\u0010\rR\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000e\u0010\u000fR\u0011\u0010\t\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0010\u0010\u000fR\u0011\u0010\u0004\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0011\u0010\u000f\u00a8\u0006\u001d"
    }
    d2 = {
        "Lco/uk/getmondo/api/model/signup/SignUpProfile;",
        "",
        "legalName",
        "",
        "preferredName",
        "isPreferredNameDerived",
        "",
        "dateOfBirth",
        "Lorg/threeten/bp/LocalDate;",
        "phoneNumber",
        "(Ljava/lang/String;Ljava/lang/String;ZLorg/threeten/bp/LocalDate;Ljava/lang/String;)V",
        "getDateOfBirth",
        "()Lorg/threeten/bp/LocalDate;",
        "()Z",
        "getLegalName",
        "()Ljava/lang/String;",
        "getPhoneNumber",
        "getPreferredName",
        "component1",
        "component2",
        "component3",
        "component4",
        "component5",
        "copy",
        "equals",
        "other",
        "hashCode",
        "",
        "toString",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field private final dateOfBirth:Lorg/threeten/bp/LocalDate;

.field private final isPreferredNameDerived:Z

.field private final legalName:Ljava/lang/String;

.field private final phoneNumber:Ljava/lang/String;

.field private final preferredName:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 8

    const/4 v1, 0x0

    const/4 v3, 0x0

    const/16 v6, 0x1f

    move-object v0, p0

    move-object v2, v1

    move-object v4, v1

    move-object v5, v1

    move-object v7, v1

    invoke-direct/range {v0 .. v7}, Lco/uk/getmondo/api/model/signup/SignUpProfile;-><init>(Ljava/lang/String;Ljava/lang/String;ZLorg/threeten/bp/LocalDate;Ljava/lang/String;ILkotlin/d/b/i;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;ZLorg/threeten/bp/LocalDate;Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation runtime Lcom/squareup/moshi/h;
            a = "legal_name"
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation runtime Lcom/squareup/moshi/h;
            a = "preferred_name"
        .end annotation
    .end param
    .param p3    # Z
        .annotation runtime Lcom/squareup/moshi/h;
            a = "is_preferred_name_derived"
        .end annotation
    .end param
    .param p4    # Lorg/threeten/bp/LocalDate;
        .annotation runtime Lcom/squareup/moshi/h;
            a = "date_of_birth"
        .end annotation
    .end param
    .param p5    # Ljava/lang/String;
        .annotation runtime Lcom/squareup/moshi/h;
            a = "phone_number"
        .end annotation
    .end param

    .prologue
    const-string v0, "legalName"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "preferredName"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "phoneNumber"

    invoke-static {p5, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lco/uk/getmondo/api/model/signup/SignUpProfile;->legalName:Ljava/lang/String;

    iput-object p2, p0, Lco/uk/getmondo/api/model/signup/SignUpProfile;->preferredName:Ljava/lang/String;

    iput-boolean p3, p0, Lco/uk/getmondo/api/model/signup/SignUpProfile;->isPreferredNameDerived:Z

    iput-object p4, p0, Lco/uk/getmondo/api/model/signup/SignUpProfile;->dateOfBirth:Lorg/threeten/bp/LocalDate;

    iput-object p5, p0, Lco/uk/getmondo/api/model/signup/SignUpProfile;->phoneNumber:Ljava/lang/String;

    return-void
.end method

.method public synthetic constructor <init>(Ljava/lang/String;Ljava/lang/String;ZLorg/threeten/bp/LocalDate;Ljava/lang/String;ILkotlin/d/b/i;)V
    .locals 6

    .prologue
    and-int/lit8 v0, p6, 0x1

    if-eqz v0, :cond_4

    .line 7
    const-string v1, ""

    :goto_0
    and-int/lit8 v0, p6, 0x2

    if-eqz v0, :cond_3

    .line 8
    const-string v2, ""

    :goto_1
    and-int/lit8 v0, p6, 0x4

    if-eqz v0, :cond_2

    .line 9
    const/4 v3, 0x1

    :goto_2
    and-int/lit8 v0, p6, 0x8

    if-eqz v0, :cond_1

    .line 10
    const/4 v0, 0x0

    check-cast v0, Lorg/threeten/bp/LocalDate;

    move-object v4, v0

    :goto_3
    and-int/lit8 v0, p6, 0x10

    if-eqz v0, :cond_0

    .line 11
    const-string v5, ""

    :goto_4
    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lco/uk/getmondo/api/model/signup/SignUpProfile;-><init>(Ljava/lang/String;Ljava/lang/String;ZLorg/threeten/bp/LocalDate;Ljava/lang/String;)V

    return-void

    :cond_0
    move-object v5, p5

    goto :goto_4

    :cond_1
    move-object v4, p4

    goto :goto_3

    :cond_2
    move v3, p3

    goto :goto_2

    :cond_3
    move-object v2, p2

    goto :goto_1

    :cond_4
    move-object v1, p1

    goto :goto_0
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 7
    iget-object v0, p0, Lco/uk/getmondo/api/model/signup/SignUpProfile;->legalName:Ljava/lang/String;

    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 8
    iget-object v0, p0, Lco/uk/getmondo/api/model/signup/SignUpProfile;->preferredName:Ljava/lang/String;

    return-object v0
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 9
    iget-boolean v0, p0, Lco/uk/getmondo/api/model/signup/SignUpProfile;->isPreferredNameDerived:Z

    return v0
.end method

.method public final d()Lorg/threeten/bp/LocalDate;
    .locals 1

    .prologue
    .line 10
    iget-object v0, p0, Lco/uk/getmondo/api/model/signup/SignUpProfile;->dateOfBirth:Lorg/threeten/bp/LocalDate;

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v1, 0x1

    const/4 v0, 0x0

    if-eq p0, p1, :cond_0

    instance-of v2, p1, Lco/uk/getmondo/api/model/signup/SignUpProfile;

    if-eqz v2, :cond_1

    check-cast p1, Lco/uk/getmondo/api/model/signup/SignUpProfile;

    iget-object v2, p0, Lco/uk/getmondo/api/model/signup/SignUpProfile;->legalName:Ljava/lang/String;

    iget-object v3, p1, Lco/uk/getmondo/api/model/signup/SignUpProfile;->legalName:Ljava/lang/String;

    invoke-static {v2, v3}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lco/uk/getmondo/api/model/signup/SignUpProfile;->preferredName:Ljava/lang/String;

    iget-object v3, p1, Lco/uk/getmondo/api/model/signup/SignUpProfile;->preferredName:Ljava/lang/String;

    invoke-static {v2, v3}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    iget-boolean v2, p0, Lco/uk/getmondo/api/model/signup/SignUpProfile;->isPreferredNameDerived:Z

    iget-boolean v3, p1, Lco/uk/getmondo/api/model/signup/SignUpProfile;->isPreferredNameDerived:Z

    if-ne v2, v3, :cond_2

    move v2, v1

    :goto_0
    if-eqz v2, :cond_1

    iget-object v2, p0, Lco/uk/getmondo/api/model/signup/SignUpProfile;->dateOfBirth:Lorg/threeten/bp/LocalDate;

    iget-object v3, p1, Lco/uk/getmondo/api/model/signup/SignUpProfile;->dateOfBirth:Lorg/threeten/bp/LocalDate;

    invoke-static {v2, v3}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lco/uk/getmondo/api/model/signup/SignUpProfile;->phoneNumber:Ljava/lang/String;

    iget-object v3, p1, Lco/uk/getmondo/api/model/signup/SignUpProfile;->phoneNumber:Ljava/lang/String;

    invoke-static {v2, v3}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    move v0, v1

    :cond_1
    return v0

    :cond_2
    move v2, v0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    const/4 v1, 0x0

    iget-object v0, p0, Lco/uk/getmondo/api/model/signup/SignUpProfile;->legalName:Ljava/lang/String;

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    :goto_0
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lco/uk/getmondo/api/model/signup/SignUpProfile;->preferredName:Ljava/lang/String;

    if-eqz v0, :cond_3

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-boolean v0, p0, Lco/uk/getmondo/api/model/signup/SignUpProfile;->isPreferredNameDerived:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :cond_0
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lco/uk/getmondo/api/model/signup/SignUpProfile;->dateOfBirth:Lorg/threeten/bp/LocalDate;

    if-eqz v0, :cond_4

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    :goto_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lco/uk/getmondo/api/model/signup/SignUpProfile;->phoneNumber:Ljava/lang/String;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_1
    add-int/2addr v0, v1

    return v0

    :cond_2
    move v0, v1

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_1

    :cond_4
    move v0, v1

    goto :goto_2
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "SignUpProfile(legalName="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/api/model/signup/SignUpProfile;->legalName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", preferredName="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/api/model/signup/SignUpProfile;->preferredName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", isPreferredNameDerived="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lco/uk/getmondo/api/model/signup/SignUpProfile;->isPreferredNameDerived:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", dateOfBirth="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/api/model/signup/SignUpProfile;->dateOfBirth:Lorg/threeten/bp/LocalDate;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", phoneNumber="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/api/model/signup/SignUpProfile;->phoneNumber:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
