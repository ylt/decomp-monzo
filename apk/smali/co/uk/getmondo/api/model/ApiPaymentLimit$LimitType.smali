.class public final enum Lco/uk/getmondo/api/model/ApiPaymentLimit$LimitType;
.super Ljava/lang/Enum;
.source "ApiPaymentLimits.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lco/uk/getmondo/api/model/ApiPaymentLimit;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "LimitType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lco/uk/getmondo/api/model/ApiPaymentLimit$LimitType;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000\u000c\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\u0008\u0005\u0008\u0086\u0001\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00000\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002j\u0002\u0008\u0003j\u0002\u0008\u0004j\u0002\u0008\u0005\u00a8\u0006\u0006"
    }
    d2 = {
        "Lco/uk/getmondo/api/model/ApiPaymentLimit$LimitType;",
        "",
        "(Ljava/lang/String;I)V",
        "AMOUNT",
        "COUNT",
        "VIRTUAL",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lco/uk/getmondo/api/model/ApiPaymentLimit$LimitType;

.field public static final enum AMOUNT:Lco/uk/getmondo/api/model/ApiPaymentLimit$LimitType;
    .annotation runtime Lcom/squareup/moshi/h;
        a = "amount"
    .end annotation
.end field

.field public static final enum COUNT:Lco/uk/getmondo/api/model/ApiPaymentLimit$LimitType;
    .annotation runtime Lcom/squareup/moshi/h;
        a = "count"
    .end annotation
.end field

.field public static final enum VIRTUAL:Lco/uk/getmondo/api/model/ApiPaymentLimit$LimitType;
    .annotation runtime Lcom/squareup/moshi/h;
        a = "virtual"
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    const/4 v0, 0x3

    new-array v0, v0, [Lco/uk/getmondo/api/model/ApiPaymentLimit$LimitType;

    new-instance v1, Lco/uk/getmondo/api/model/ApiPaymentLimit$LimitType;

    const-string v2, "AMOUNT"

    invoke-direct {v1, v2, v3}, Lco/uk/getmondo/api/model/ApiPaymentLimit$LimitType;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lco/uk/getmondo/api/model/ApiPaymentLimit$LimitType;->AMOUNT:Lco/uk/getmondo/api/model/ApiPaymentLimit$LimitType;

    aput-object v1, v0, v3

    new-instance v1, Lco/uk/getmondo/api/model/ApiPaymentLimit$LimitType;

    const-string v2, "COUNT"

    invoke-direct {v1, v2, v4}, Lco/uk/getmondo/api/model/ApiPaymentLimit$LimitType;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lco/uk/getmondo/api/model/ApiPaymentLimit$LimitType;->COUNT:Lco/uk/getmondo/api/model/ApiPaymentLimit$LimitType;

    aput-object v1, v0, v4

    new-instance v1, Lco/uk/getmondo/api/model/ApiPaymentLimit$LimitType;

    const-string v2, "VIRTUAL"

    invoke-direct {v1, v2, v5}, Lco/uk/getmondo/api/model/ApiPaymentLimit$LimitType;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lco/uk/getmondo/api/model/ApiPaymentLimit$LimitType;->VIRTUAL:Lco/uk/getmondo/api/model/ApiPaymentLimit$LimitType;

    aput-object v1, v0, v5

    sput-object v0, Lco/uk/getmondo/api/model/ApiPaymentLimit$LimitType;->$VALUES:[Lco/uk/getmondo/api/model/ApiPaymentLimit$LimitType;

    return-void
.end method

.method protected constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 24
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lco/uk/getmondo/api/model/ApiPaymentLimit$LimitType;
    .locals 1

    const-class v0, Lco/uk/getmondo/api/model/ApiPaymentLimit$LimitType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/api/model/ApiPaymentLimit$LimitType;

    return-object v0
.end method

.method public static values()[Lco/uk/getmondo/api/model/ApiPaymentLimit$LimitType;
    .locals 1

    sget-object v0, Lco/uk/getmondo/api/model/ApiPaymentLimit$LimitType;->$VALUES:[Lco/uk/getmondo/api/model/ApiPaymentLimit$LimitType;

    invoke-virtual {v0}, [Lco/uk/getmondo/api/model/ApiPaymentLimit$LimitType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lco/uk/getmondo/api/model/ApiPaymentLimit$LimitType;

    return-object v0
.end method
