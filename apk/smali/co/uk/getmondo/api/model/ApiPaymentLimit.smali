.class public final Lco/uk/getmondo/api/model/ApiPaymentLimit;
.super Ljava/lang/Object;
.source "ApiPaymentLimits.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lco/uk/getmondo/api/model/ApiPaymentLimit$LimitType;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u00002\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\t\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u001a\n\u0002\u0010\u0008\n\u0002\u0008\u0003\u0008\u0086\u0008\u0018\u00002\u00020\u0001:\u0001)BM\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\n\u0008\u0002\u0010\u0007\u001a\u0004\u0018\u00010\u0003\u0012\n\u0008\u0002\u0010\u0008\u001a\u0004\u0018\u00010\t\u0012\n\u0008\u0002\u0010\n\u001a\u0004\u0018\u00010\t\u0012\n\u0008\u0002\u0010\u000b\u001a\u0004\u0018\u00010\u000c\u00a2\u0006\u0002\u0010\rJ\t\u0010\u001b\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u001c\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u001d\u001a\u00020\u0006H\u00c6\u0003J\u000b\u0010\u001e\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003J\u0010\u0010\u001f\u001a\u0004\u0018\u00010\tH\u00c6\u0003\u00a2\u0006\u0002\u0010\u0012J\u0010\u0010 \u001a\u0004\u0018\u00010\tH\u00c6\u0003\u00a2\u0006\u0002\u0010\u0012J\u0010\u0010!\u001a\u0004\u0018\u00010\u000cH\u00c6\u0003\u00a2\u0006\u0002\u0010\u0018J\\\u0010\"\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0005\u001a\u00020\u00062\n\u0008\u0002\u0010\u0007\u001a\u0004\u0018\u00010\u00032\n\u0008\u0002\u0010\u0008\u001a\u0004\u0018\u00010\t2\n\u0008\u0002\u0010\n\u001a\u0004\u0018\u00010\t2\n\u0008\u0002\u0010\u000b\u001a\u0004\u0018\u00010\u000cH\u00c6\u0001\u00a2\u0006\u0002\u0010#J\u0013\u0010$\u001a\u00020\u000c2\u0008\u0010%\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u0010&\u001a\u00020\'H\u00d6\u0001J\t\u0010(\u001a\u00020\u0003H\u00d6\u0001R\u0013\u0010\u0007\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000e\u0010\u000fR\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0010\u0010\u000fR\u0015\u0010\u0008\u001a\u0004\u0018\u00010\t\u00a2\u0006\n\n\u0002\u0010\u0013\u001a\u0004\u0008\u0011\u0010\u0012R\u0011\u0010\u0004\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0014\u0010\u000fR\u0011\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0015\u0010\u0016R\u0015\u0010\u000b\u001a\u0004\u0018\u00010\u000c\u00a2\u0006\n\n\u0002\u0010\u0019\u001a\u0004\u0008\u0017\u0010\u0018R\u0015\u0010\n\u001a\u0004\u0018\u00010\t\u00a2\u0006\n\n\u0002\u0010\u0013\u001a\u0004\u0008\u001a\u0010\u0012\u00a8\u0006*"
    }
    d2 = {
        "Lco/uk/getmondo/api/model/ApiPaymentLimit;",
        "",
        "id",
        "",
        "name",
        "type",
        "Lco/uk/getmondo/api/model/ApiPaymentLimit$LimitType;",
        "currency",
        "limit",
        "",
        "utilization",
        "unlimited",
        "",
        "(Ljava/lang/String;Ljava/lang/String;Lco/uk/getmondo/api/model/ApiPaymentLimit$LimitType;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Boolean;)V",
        "getCurrency",
        "()Ljava/lang/String;",
        "getId",
        "getLimit",
        "()Ljava/lang/Long;",
        "Ljava/lang/Long;",
        "getName",
        "getType",
        "()Lco/uk/getmondo/api/model/ApiPaymentLimit$LimitType;",
        "getUnlimited",
        "()Ljava/lang/Boolean;",
        "Ljava/lang/Boolean;",
        "getUtilization",
        "component1",
        "component2",
        "component3",
        "component4",
        "component5",
        "component6",
        "component7",
        "copy",
        "(Ljava/lang/String;Ljava/lang/String;Lco/uk/getmondo/api/model/ApiPaymentLimit$LimitType;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Boolean;)Lco/uk/getmondo/api/model/ApiPaymentLimit;",
        "equals",
        "other",
        "hashCode",
        "",
        "toString",
        "LimitType",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field private final currency:Ljava/lang/String;

.field private final id:Ljava/lang/String;

.field private final limit:Ljava/lang/Long;

.field private final name:Ljava/lang/String;

.field private final type:Lco/uk/getmondo/api/model/ApiPaymentLimit$LimitType;

.field private final unlimited:Ljava/lang/Boolean;

.field private final utilization:Ljava/lang/Long;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Lco/uk/getmondo/api/model/ApiPaymentLimit$LimitType;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Boolean;)V
    .locals 1

    .prologue
    const-string v0, "id"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "name"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "type"

    invoke-static {p3, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lco/uk/getmondo/api/model/ApiPaymentLimit;->id:Ljava/lang/String;

    iput-object p2, p0, Lco/uk/getmondo/api/model/ApiPaymentLimit;->name:Ljava/lang/String;

    iput-object p3, p0, Lco/uk/getmondo/api/model/ApiPaymentLimit;->type:Lco/uk/getmondo/api/model/ApiPaymentLimit$LimitType;

    iput-object p4, p0, Lco/uk/getmondo/api/model/ApiPaymentLimit;->currency:Ljava/lang/String;

    iput-object p5, p0, Lco/uk/getmondo/api/model/ApiPaymentLimit;->limit:Ljava/lang/Long;

    iput-object p6, p0, Lco/uk/getmondo/api/model/ApiPaymentLimit;->utilization:Ljava/lang/Long;

    iput-object p7, p0, Lco/uk/getmondo/api/model/ApiPaymentLimit;->unlimited:Ljava/lang/Boolean;

    return-void
.end method

.method public synthetic constructor <init>(Ljava/lang/String;Ljava/lang/String;Lco/uk/getmondo/api/model/ApiPaymentLimit$LimitType;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Boolean;ILkotlin/d/b/i;)V
    .locals 8

    .prologue
    and-int/lit8 v0, p8, 0x8

    if-eqz v0, :cond_3

    .line 19
    const/4 v0, 0x0

    check-cast v0, Ljava/lang/String;

    move-object v4, v0

    :goto_0
    and-int/lit8 v0, p8, 0x10

    if-eqz v0, :cond_2

    .line 20
    const/4 v0, 0x0

    check-cast v0, Ljava/lang/Long;

    move-object v5, v0

    :goto_1
    and-int/lit8 v0, p8, 0x20

    if-eqz v0, :cond_1

    .line 21
    const/4 v0, 0x0

    check-cast v0, Ljava/lang/Long;

    move-object v6, v0

    :goto_2
    and-int/lit8 v0, p8, 0x40

    if-eqz v0, :cond_0

    .line 22
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v7

    :goto_3
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    invoke-direct/range {v0 .. v7}, Lco/uk/getmondo/api/model/ApiPaymentLimit;-><init>(Ljava/lang/String;Ljava/lang/String;Lco/uk/getmondo/api/model/ApiPaymentLimit$LimitType;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Boolean;)V

    return-void

    :cond_0
    move-object v7, p7

    goto :goto_3

    :cond_1
    move-object v6, p6

    goto :goto_2

    :cond_2
    move-object v5, p5

    goto :goto_1

    :cond_3
    move-object v4, p4

    goto :goto_0
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 16
    iget-object v0, p0, Lco/uk/getmondo/api/model/ApiPaymentLimit;->id:Ljava/lang/String;

    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 17
    iget-object v0, p0, Lco/uk/getmondo/api/model/ApiPaymentLimit;->name:Ljava/lang/String;

    return-object v0
.end method

.method public final c()Lco/uk/getmondo/api/model/ApiPaymentLimit$LimitType;
    .locals 1

    .prologue
    .line 18
    iget-object v0, p0, Lco/uk/getmondo/api/model/ApiPaymentLimit;->type:Lco/uk/getmondo/api/model/ApiPaymentLimit$LimitType;

    return-object v0
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 19
    iget-object v0, p0, Lco/uk/getmondo/api/model/ApiPaymentLimit;->currency:Ljava/lang/String;

    return-object v0
.end method

.method public final e()Ljava/lang/Long;
    .locals 1

    .prologue
    .line 20
    iget-object v0, p0, Lco/uk/getmondo/api/model/ApiPaymentLimit;->limit:Ljava/lang/Long;

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_0

    instance-of v0, p1, Lco/uk/getmondo/api/model/ApiPaymentLimit;

    if-eqz v0, :cond_1

    check-cast p1, Lco/uk/getmondo/api/model/ApiPaymentLimit;

    iget-object v0, p0, Lco/uk/getmondo/api/model/ApiPaymentLimit;->id:Ljava/lang/String;

    iget-object v1, p1, Lco/uk/getmondo/api/model/ApiPaymentLimit;->id:Ljava/lang/String;

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lco/uk/getmondo/api/model/ApiPaymentLimit;->name:Ljava/lang/String;

    iget-object v1, p1, Lco/uk/getmondo/api/model/ApiPaymentLimit;->name:Ljava/lang/String;

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lco/uk/getmondo/api/model/ApiPaymentLimit;->type:Lco/uk/getmondo/api/model/ApiPaymentLimit$LimitType;

    iget-object v1, p1, Lco/uk/getmondo/api/model/ApiPaymentLimit;->type:Lco/uk/getmondo/api/model/ApiPaymentLimit$LimitType;

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lco/uk/getmondo/api/model/ApiPaymentLimit;->currency:Ljava/lang/String;

    iget-object v1, p1, Lco/uk/getmondo/api/model/ApiPaymentLimit;->currency:Ljava/lang/String;

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lco/uk/getmondo/api/model/ApiPaymentLimit;->limit:Ljava/lang/Long;

    iget-object v1, p1, Lco/uk/getmondo/api/model/ApiPaymentLimit;->limit:Ljava/lang/Long;

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lco/uk/getmondo/api/model/ApiPaymentLimit;->utilization:Ljava/lang/Long;

    iget-object v1, p1, Lco/uk/getmondo/api/model/ApiPaymentLimit;->utilization:Ljava/lang/Long;

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lco/uk/getmondo/api/model/ApiPaymentLimit;->unlimited:Ljava/lang/Boolean;

    iget-object v1, p1, Lco/uk/getmondo/api/model/ApiPaymentLimit;->unlimited:Ljava/lang/Boolean;

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final f()Ljava/lang/Long;
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lco/uk/getmondo/api/model/ApiPaymentLimit;->utilization:Ljava/lang/Long;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    const/4 v1, 0x0

    iget-object v0, p0, Lco/uk/getmondo/api/model/ApiPaymentLimit;->id:Ljava/lang/String;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    :goto_0
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lco/uk/getmondo/api/model/ApiPaymentLimit;->name:Ljava/lang/String;

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lco/uk/getmondo/api/model/ApiPaymentLimit;->type:Lco/uk/getmondo/api/model/ApiPaymentLimit$LimitType;

    if-eqz v0, :cond_3

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    :goto_2
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lco/uk/getmondo/api/model/ApiPaymentLimit;->currency:Ljava/lang/String;

    if-eqz v0, :cond_4

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    :goto_3
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lco/uk/getmondo/api/model/ApiPaymentLimit;->limit:Ljava/lang/Long;

    if-eqz v0, :cond_5

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    :goto_4
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lco/uk/getmondo/api/model/ApiPaymentLimit;->utilization:Ljava/lang/Long;

    if-eqz v0, :cond_6

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    :goto_5
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lco/uk/getmondo/api/model/ApiPaymentLimit;->unlimited:Ljava/lang/Boolean;

    if-eqz v2, :cond_0

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_0
    add-int/2addr v0, v1

    return v0

    :cond_1
    move v0, v1

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_1

    :cond_3
    move v0, v1

    goto :goto_2

    :cond_4
    move v0, v1

    goto :goto_3

    :cond_5
    move v0, v1

    goto :goto_4

    :cond_6
    move v0, v1

    goto :goto_5
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "ApiPaymentLimit(id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/api/model/ApiPaymentLimit;->id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", name="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/api/model/ApiPaymentLimit;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/api/model/ApiPaymentLimit;->type:Lco/uk/getmondo/api/model/ApiPaymentLimit$LimitType;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", currency="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/api/model/ApiPaymentLimit;->currency:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", limit="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/api/model/ApiPaymentLimit;->limit:Ljava/lang/Long;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", utilization="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/api/model/ApiPaymentLimit;->utilization:Ljava/lang/Long;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", unlimited="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/api/model/ApiPaymentLimit;->unlimited:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
