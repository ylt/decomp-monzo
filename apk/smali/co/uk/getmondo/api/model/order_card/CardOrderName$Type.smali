.class public final enum Lco/uk/getmondo/api/model/order_card/CardOrderName$Type;
.super Ljava/lang/Enum;
.source "CardOrderOptions.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lco/uk/getmondo/api/model/order_card/CardOrderName;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Type"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lco/uk/getmondo/api/model/order_card/CardOrderName$Type;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0006\u0008\u0086\u0001\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00000\u0001B\u000f\u0008\u0002\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0005\u0010\u0006j\u0002\u0008\u0007j\u0002\u0008\u0008\u00a8\u0006\t"
    }
    d2 = {
        "Lco/uk/getmondo/api/model/order_card/CardOrderName$Type;",
        "",
        "apiValue",
        "",
        "(Ljava/lang/String;ILjava/lang/String;)V",
        "getApiValue",
        "()Ljava/lang/String;",
        "PREFERRED",
        "LEGAL",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lco/uk/getmondo/api/model/order_card/CardOrderName$Type;

.field public static final enum LEGAL:Lco/uk/getmondo/api/model/order_card/CardOrderName$Type;
    .annotation runtime Lcom/squareup/moshi/h;
        a = "legal"
    .end annotation
.end field

.field public static final enum PREFERRED:Lco/uk/getmondo/api/model/order_card/CardOrderName$Type;
    .annotation runtime Lcom/squareup/moshi/h;
        a = "preferred"
    .end annotation
.end field


# instance fields
.field private final apiValue:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    const/4 v0, 0x2

    new-array v0, v0, [Lco/uk/getmondo/api/model/order_card/CardOrderName$Type;

    new-instance v1, Lco/uk/getmondo/api/model/order_card/CardOrderName$Type;

    const-string v2, "PREFERRED"

    .line 11
    const-string v3, "preferred"

    invoke-direct {v1, v2, v4, v3}, Lco/uk/getmondo/api/model/order_card/CardOrderName$Type;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lco/uk/getmondo/api/model/order_card/CardOrderName$Type;->PREFERRED:Lco/uk/getmondo/api/model/order_card/CardOrderName$Type;

    aput-object v1, v0, v4

    new-instance v1, Lco/uk/getmondo/api/model/order_card/CardOrderName$Type;

    const-string v2, "LEGAL"

    .line 12
    const-string v3, "legal"

    invoke-direct {v1, v2, v5, v3}, Lco/uk/getmondo/api/model/order_card/CardOrderName$Type;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lco/uk/getmondo/api/model/order_card/CardOrderName$Type;->LEGAL:Lco/uk/getmondo/api/model/order_card/CardOrderName$Type;

    aput-object v1, v0, v5

    sput-object v0, Lco/uk/getmondo/api/model/order_card/CardOrderName$Type;->$VALUES:[Lco/uk/getmondo/api/model/order_card/CardOrderName$Type;

    return-void
.end method

.method protected constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    const-string v0, "apiValue"

    invoke-static {p3, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 10
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput-object p3, p0, Lco/uk/getmondo/api/model/order_card/CardOrderName$Type;->apiValue:Ljava/lang/String;

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lco/uk/getmondo/api/model/order_card/CardOrderName$Type;
    .locals 1

    const-class v0, Lco/uk/getmondo/api/model/order_card/CardOrderName$Type;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/api/model/order_card/CardOrderName$Type;

    return-object v0
.end method

.method public static values()[Lco/uk/getmondo/api/model/order_card/CardOrderName$Type;
    .locals 1

    sget-object v0, Lco/uk/getmondo/api/model/order_card/CardOrderName$Type;->$VALUES:[Lco/uk/getmondo/api/model/order_card/CardOrderName$Type;

    invoke-virtual {v0}, [Lco/uk/getmondo/api/model/order_card/CardOrderName$Type;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lco/uk/getmondo/api/model/order_card/CardOrderName$Type;

    return-object v0
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 10
    iget-object v0, p0, Lco/uk/getmondo/api/model/order_card/CardOrderName$Type;->apiValue:Ljava/lang/String;

    return-object v0
.end method
