.class public final Lco/uk/getmondo/api/model/overdraft/ApiOverdraftStatus;
.super Ljava/lang/Object;
.source "ApiOverdraftStatus.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0003\n\u0002\u0010\t\n\u0002\u0008!\n\u0002\u0010\u0008\n\u0002\u0008\u0002\u0008\u0086\u0008\u0018\u00002\u00020\u0001Be\u0012\u0008\u0008\u0001\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0005\u0012\u0008\u0008\u0002\u0010\u0007\u001a\u00020\u0003\u0012\u0008\u0008\u0003\u0010\u0008\u001a\u00020\t\u0012\u0008\u0008\u0003\u0010\n\u001a\u00020\t\u0012\u0008\u0008\u0003\u0010\u000b\u001a\u00020\t\u0012\u0008\u0008\u0003\u0010\u000c\u001a\u00020\t\u0012\u0008\u0008\u0003\u0010\r\u001a\u00020\t\u0012\u0008\u0008\u0003\u0010\u000e\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u000fJ\t\u0010\u001d\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u001e\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u001f\u001a\u00020\u0005H\u00c6\u0003J\t\u0010 \u001a\u00020\u0005H\u00c6\u0003J\t\u0010!\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\"\u001a\u00020\tH\u00c6\u0003J\t\u0010#\u001a\u00020\tH\u00c6\u0003J\t\u0010$\u001a\u00020\tH\u00c6\u0003J\t\u0010%\u001a\u00020\tH\u00c6\u0003J\t\u0010&\u001a\u00020\tH\u00c6\u0003Jm\u0010\'\u001a\u00020\u00002\u0008\u0008\u0003\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u00052\u0008\u0008\u0002\u0010\u0006\u001a\u00020\u00052\u0008\u0008\u0002\u0010\u0007\u001a\u00020\u00032\u0008\u0008\u0003\u0010\u0008\u001a\u00020\t2\u0008\u0008\u0003\u0010\n\u001a\u00020\t2\u0008\u0008\u0003\u0010\u000b\u001a\u00020\t2\u0008\u0008\u0003\u0010\u000c\u001a\u00020\t2\u0008\u0008\u0003\u0010\r\u001a\u00020\t2\u0008\u0008\u0003\u0010\u000e\u001a\u00020\u0003H\u00c6\u0001J\u0013\u0010(\u001a\u00020\u00052\u0008\u0010)\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u0010*\u001a\u00020+H\u00d6\u0001J\t\u0010,\u001a\u00020\u0003H\u00d6\u0001R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0010\u0010\u0011R\u0011\u0010\u0008\u001a\u00020\t\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0012\u0010\u0013R\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0014\u0010\u0015R\u0011\u0010\n\u001a\u00020\t\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0016\u0010\u0013R\u0011\u0010\u000e\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0017\u0010\u0011R\u0011\u0010\u0007\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0018\u0010\u0011R\u0011\u0010\u000b\u001a\u00020\t\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0019\u0010\u0013R\u0011\u0010\u0006\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001a\u0010\u0015R\u0011\u0010\u000c\u001a\u00020\t\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001b\u0010\u0013R\u0011\u0010\r\u001a\u00020\t\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001c\u0010\u0013\u00a8\u0006-"
    }
    d2 = {
        "Lco/uk/getmondo/api/model/overdraft/ApiOverdraftStatus;",
        "",
        "accountId",
        "",
        "active",
        "",
        "eligible",
        "currency",
        "accruedFeesAmount",
        "",
        "bufferAmount",
        "dailyFeeAmount",
        "limitAmount",
        "preapprovedLimitAmount",
        "chargeDate",
        "(Ljava/lang/String;ZZLjava/lang/String;JJJJJLjava/lang/String;)V",
        "getAccountId",
        "()Ljava/lang/String;",
        "getAccruedFeesAmount",
        "()J",
        "getActive",
        "()Z",
        "getBufferAmount",
        "getChargeDate",
        "getCurrency",
        "getDailyFeeAmount",
        "getEligible",
        "getLimitAmount",
        "getPreapprovedLimitAmount",
        "component1",
        "component10",
        "component2",
        "component3",
        "component4",
        "component5",
        "component6",
        "component7",
        "component8",
        "component9",
        "copy",
        "equals",
        "other",
        "hashCode",
        "",
        "toString",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field private final accountId:Ljava/lang/String;

.field private final accruedFeesAmount:J

.field private final active:Z

.field private final bufferAmount:J

.field private final chargeDate:Ljava/lang/String;

.field private final currency:Ljava/lang/String;

.field private final dailyFeeAmount:J

.field private final eligible:Z

.field private final limitAmount:J

.field private final preapprovedLimitAmount:J


# direct methods
.method public constructor <init>(Ljava/lang/String;ZZLjava/lang/String;JJJJJLjava/lang/String;)V
    .locals 5
    .param p1    # Ljava/lang/String;
        .annotation runtime Lcom/squareup/moshi/h;
            a = "account_id"
        .end annotation
    .end param
    .param p5    # J
        .annotation runtime Lcom/squareup/moshi/h;
            a = "accrued_fees_amount"
        .end annotation
    .end param
    .param p7    # J
        .annotation runtime Lcom/squareup/moshi/h;
            a = "buffer_amount"
        .end annotation
    .end param
    .param p9    # J
        .annotation runtime Lcom/squareup/moshi/h;
            a = "daily_fee_amount"
        .end annotation
    .end param
    .param p11    # J
        .annotation runtime Lcom/squareup/moshi/h;
            a = "limit_amount"
        .end annotation
    .end param
    .param p13    # J
        .annotation runtime Lcom/squareup/moshi/h;
            a = "preapproved_limit_amount"
        .end annotation
    .end param
    .param p15    # Ljava/lang/String;
        .annotation runtime Lcom/squareup/moshi/h;
            a = "charge_date"
        .end annotation
    .end param

    .prologue
    const-string v2, "accountId"

    invoke-static {p1, v2}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v2, "currency"

    invoke-static {p4, v2}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v2, "chargeDate"

    move-object/from16 v0, p15

    invoke-static {v0, v2}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lco/uk/getmondo/api/model/overdraft/ApiOverdraftStatus;->accountId:Ljava/lang/String;

    iput-boolean p2, p0, Lco/uk/getmondo/api/model/overdraft/ApiOverdraftStatus;->active:Z

    iput-boolean p3, p0, Lco/uk/getmondo/api/model/overdraft/ApiOverdraftStatus;->eligible:Z

    iput-object p4, p0, Lco/uk/getmondo/api/model/overdraft/ApiOverdraftStatus;->currency:Ljava/lang/String;

    iput-wide p5, p0, Lco/uk/getmondo/api/model/overdraft/ApiOverdraftStatus;->accruedFeesAmount:J

    iput-wide p7, p0, Lco/uk/getmondo/api/model/overdraft/ApiOverdraftStatus;->bufferAmount:J

    iput-wide p9, p0, Lco/uk/getmondo/api/model/overdraft/ApiOverdraftStatus;->dailyFeeAmount:J

    move-wide/from16 v0, p11

    iput-wide v0, p0, Lco/uk/getmondo/api/model/overdraft/ApiOverdraftStatus;->limitAmount:J

    move-wide/from16 v0, p13

    iput-wide v0, p0, Lco/uk/getmondo/api/model/overdraft/ApiOverdraftStatus;->preapprovedLimitAmount:J

    move-object/from16 v0, p15

    iput-object v0, p0, Lco/uk/getmondo/api/model/overdraft/ApiOverdraftStatus;->chargeDate:Ljava/lang/String;

    return-void
.end method

.method public synthetic constructor <init>(Ljava/lang/String;ZZLjava/lang/String;JJJJJLjava/lang/String;ILkotlin/d/b/i;)V
    .locals 19

    .prologue
    and-int/lit8 v2, p16, 0x8

    if-eqz v2, :cond_6

    .line 10
    const-string v7, ""

    :goto_0
    and-int/lit8 v2, p16, 0x10

    if-eqz v2, :cond_5

    .line 11
    const-wide/16 v8, 0x0

    :goto_1
    and-int/lit8 v2, p16, 0x20

    if-eqz v2, :cond_4

    .line 12
    const-wide/16 v10, 0x0

    :goto_2
    and-int/lit8 v2, p16, 0x40

    if-eqz v2, :cond_3

    .line 13
    const-wide/16 v12, 0x0

    :goto_3
    move/from16 v0, p16

    and-int/lit16 v2, v0, 0x80

    if-eqz v2, :cond_2

    .line 14
    const-wide/16 v14, 0x0

    :goto_4
    move/from16 v0, p16

    and-int/lit16 v2, v0, 0x100

    if-eqz v2, :cond_1

    .line 15
    const-wide/16 v16, 0x0

    :goto_5
    move/from16 v0, p16

    and-int/lit16 v2, v0, 0x200

    if-eqz v2, :cond_0

    .line 16
    const-string v18, ""

    :goto_6
    move-object/from16 v3, p0

    move-object/from16 v4, p1

    move/from16 v5, p2

    move/from16 v6, p3

    invoke-direct/range {v3 .. v18}, Lco/uk/getmondo/api/model/overdraft/ApiOverdraftStatus;-><init>(Ljava/lang/String;ZZLjava/lang/String;JJJJJLjava/lang/String;)V

    return-void

    :cond_0
    move-object/from16 v18, p15

    goto :goto_6

    :cond_1
    move-wide/from16 v16, p13

    goto :goto_5

    :cond_2
    move-wide/from16 v14, p11

    goto :goto_4

    :cond_3
    move-wide/from16 v12, p9

    goto :goto_3

    :cond_4
    move-wide/from16 v10, p7

    goto :goto_2

    :cond_5
    move-wide/from16 v8, p5

    goto :goto_1

    :cond_6
    move-object/from16 v7, p4

    goto :goto_0
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 7
    iget-object v0, p0, Lco/uk/getmondo/api/model/overdraft/ApiOverdraftStatus;->accountId:Ljava/lang/String;

    return-object v0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 8
    iget-boolean v0, p0, Lco/uk/getmondo/api/model/overdraft/ApiOverdraftStatus;->active:Z

    return v0
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 9
    iget-boolean v0, p0, Lco/uk/getmondo/api/model/overdraft/ApiOverdraftStatus;->eligible:Z

    return v0
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 10
    iget-object v0, p0, Lco/uk/getmondo/api/model/overdraft/ApiOverdraftStatus;->currency:Ljava/lang/String;

    return-object v0
.end method

.method public final e()J
    .locals 2

    .prologue
    .line 11
    iget-wide v0, p0, Lco/uk/getmondo/api/model/overdraft/ApiOverdraftStatus;->accruedFeesAmount:J

    return-wide v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 6

    const/4 v1, 0x1

    const/4 v0, 0x0

    if-eq p0, p1, :cond_0

    instance-of v2, p1, Lco/uk/getmondo/api/model/overdraft/ApiOverdraftStatus;

    if-eqz v2, :cond_1

    check-cast p1, Lco/uk/getmondo/api/model/overdraft/ApiOverdraftStatus;

    iget-object v2, p0, Lco/uk/getmondo/api/model/overdraft/ApiOverdraftStatus;->accountId:Ljava/lang/String;

    iget-object v3, p1, Lco/uk/getmondo/api/model/overdraft/ApiOverdraftStatus;->accountId:Ljava/lang/String;

    invoke-static {v2, v3}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    iget-boolean v2, p0, Lco/uk/getmondo/api/model/overdraft/ApiOverdraftStatus;->active:Z

    iget-boolean v3, p1, Lco/uk/getmondo/api/model/overdraft/ApiOverdraftStatus;->active:Z

    if-ne v2, v3, :cond_2

    move v2, v1

    :goto_0
    if-eqz v2, :cond_1

    iget-boolean v2, p0, Lco/uk/getmondo/api/model/overdraft/ApiOverdraftStatus;->eligible:Z

    iget-boolean v3, p1, Lco/uk/getmondo/api/model/overdraft/ApiOverdraftStatus;->eligible:Z

    if-ne v2, v3, :cond_3

    move v2, v1

    :goto_1
    if-eqz v2, :cond_1

    iget-object v2, p0, Lco/uk/getmondo/api/model/overdraft/ApiOverdraftStatus;->currency:Ljava/lang/String;

    iget-object v3, p1, Lco/uk/getmondo/api/model/overdraft/ApiOverdraftStatus;->currency:Ljava/lang/String;

    invoke-static {v2, v3}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    iget-wide v2, p0, Lco/uk/getmondo/api/model/overdraft/ApiOverdraftStatus;->accruedFeesAmount:J

    iget-wide v4, p1, Lco/uk/getmondo/api/model/overdraft/ApiOverdraftStatus;->accruedFeesAmount:J

    cmp-long v2, v2, v4

    if-nez v2, :cond_4

    move v2, v1

    :goto_2
    if-eqz v2, :cond_1

    iget-wide v2, p0, Lco/uk/getmondo/api/model/overdraft/ApiOverdraftStatus;->bufferAmount:J

    iget-wide v4, p1, Lco/uk/getmondo/api/model/overdraft/ApiOverdraftStatus;->bufferAmount:J

    cmp-long v2, v2, v4

    if-nez v2, :cond_5

    move v2, v1

    :goto_3
    if-eqz v2, :cond_1

    iget-wide v2, p0, Lco/uk/getmondo/api/model/overdraft/ApiOverdraftStatus;->dailyFeeAmount:J

    iget-wide v4, p1, Lco/uk/getmondo/api/model/overdraft/ApiOverdraftStatus;->dailyFeeAmount:J

    cmp-long v2, v2, v4

    if-nez v2, :cond_6

    move v2, v1

    :goto_4
    if-eqz v2, :cond_1

    iget-wide v2, p0, Lco/uk/getmondo/api/model/overdraft/ApiOverdraftStatus;->limitAmount:J

    iget-wide v4, p1, Lco/uk/getmondo/api/model/overdraft/ApiOverdraftStatus;->limitAmount:J

    cmp-long v2, v2, v4

    if-nez v2, :cond_7

    move v2, v1

    :goto_5
    if-eqz v2, :cond_1

    iget-wide v2, p0, Lco/uk/getmondo/api/model/overdraft/ApiOverdraftStatus;->preapprovedLimitAmount:J

    iget-wide v4, p1, Lco/uk/getmondo/api/model/overdraft/ApiOverdraftStatus;->preapprovedLimitAmount:J

    cmp-long v2, v2, v4

    if-nez v2, :cond_8

    move v2, v1

    :goto_6
    if-eqz v2, :cond_1

    iget-object v2, p0, Lco/uk/getmondo/api/model/overdraft/ApiOverdraftStatus;->chargeDate:Ljava/lang/String;

    iget-object v3, p1, Lco/uk/getmondo/api/model/overdraft/ApiOverdraftStatus;->chargeDate:Ljava/lang/String;

    invoke-static {v2, v3}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    move v0, v1

    :cond_1
    return v0

    :cond_2
    move v2, v0

    goto :goto_0

    :cond_3
    move v2, v0

    goto :goto_1

    :cond_4
    move v2, v0

    goto :goto_2

    :cond_5
    move v2, v0

    goto :goto_3

    :cond_6
    move v2, v0

    goto :goto_4

    :cond_7
    move v2, v0

    goto :goto_5

    :cond_8
    move v2, v0

    goto :goto_6
.end method

.method public final f()J
    .locals 2

    .prologue
    .line 12
    iget-wide v0, p0, Lco/uk/getmondo/api/model/overdraft/ApiOverdraftStatus;->bufferAmount:J

    return-wide v0
.end method

.method public final g()J
    .locals 2

    .prologue
    .line 13
    iget-wide v0, p0, Lco/uk/getmondo/api/model/overdraft/ApiOverdraftStatus;->dailyFeeAmount:J

    return-wide v0
.end method

.method public final h()J
    .locals 2

    .prologue
    .line 14
    iget-wide v0, p0, Lco/uk/getmondo/api/model/overdraft/ApiOverdraftStatus;->limitAmount:J

    return-wide v0
.end method

.method public hashCode()I
    .locals 7

    const/4 v2, 0x1

    const/4 v1, 0x0

    const/16 v6, 0x20

    iget-object v0, p0, Lco/uk/getmondo/api/model/overdraft/ApiOverdraftStatus;->accountId:Ljava/lang/String;

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    :goto_0
    mul-int/lit8 v3, v0, 0x1f

    iget-boolean v0, p0, Lco/uk/getmondo/api/model/overdraft/ApiOverdraftStatus;->active:Z

    if-eqz v0, :cond_0

    move v0, v2

    :cond_0
    add-int/2addr v0, v3

    mul-int/lit8 v3, v0, 0x1f

    iget-boolean v0, p0, Lco/uk/getmondo/api/model/overdraft/ApiOverdraftStatus;->eligible:Z

    if-eqz v0, :cond_4

    :goto_1
    add-int v0, v3, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lco/uk/getmondo/api/model/overdraft/ApiOverdraftStatus;->currency:Ljava/lang/String;

    if-eqz v0, :cond_3

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    :goto_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-wide v2, p0, Lco/uk/getmondo/api/model/overdraft/ApiOverdraftStatus;->accruedFeesAmount:J

    ushr-long v4, v2, v6

    xor-long/2addr v2, v4

    long-to-int v2, v2

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-wide v2, p0, Lco/uk/getmondo/api/model/overdraft/ApiOverdraftStatus;->bufferAmount:J

    ushr-long v4, v2, v6

    xor-long/2addr v2, v4

    long-to-int v2, v2

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-wide v2, p0, Lco/uk/getmondo/api/model/overdraft/ApiOverdraftStatus;->dailyFeeAmount:J

    ushr-long v4, v2, v6

    xor-long/2addr v2, v4

    long-to-int v2, v2

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-wide v2, p0, Lco/uk/getmondo/api/model/overdraft/ApiOverdraftStatus;->limitAmount:J

    ushr-long v4, v2, v6

    xor-long/2addr v2, v4

    long-to-int v2, v2

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-wide v2, p0, Lco/uk/getmondo/api/model/overdraft/ApiOverdraftStatus;->preapprovedLimitAmount:J

    ushr-long v4, v2, v6

    xor-long/2addr v2, v4

    long-to-int v2, v2

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lco/uk/getmondo/api/model/overdraft/ApiOverdraftStatus;->chargeDate:Ljava/lang/String;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_1
    add-int/2addr v0, v1

    return v0

    :cond_2
    move v0, v1

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_2

    :cond_4
    move v2, v0

    goto :goto_1
.end method

.method public final i()J
    .locals 2

    .prologue
    .line 15
    iget-wide v0, p0, Lco/uk/getmondo/api/model/overdraft/ApiOverdraftStatus;->preapprovedLimitAmount:J

    return-wide v0
.end method

.method public final j()Ljava/lang/String;
    .locals 1

    .prologue
    .line 16
    iget-object v0, p0, Lco/uk/getmondo/api/model/overdraft/ApiOverdraftStatus;->chargeDate:Ljava/lang/String;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "ApiOverdraftStatus(accountId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/api/model/overdraft/ApiOverdraftStatus;->accountId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", active="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lco/uk/getmondo/api/model/overdraft/ApiOverdraftStatus;->active:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", eligible="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lco/uk/getmondo/api/model/overdraft/ApiOverdraftStatus;->eligible:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", currency="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/api/model/overdraft/ApiOverdraftStatus;->currency:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", accruedFeesAmount="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lco/uk/getmondo/api/model/overdraft/ApiOverdraftStatus;->accruedFeesAmount:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", bufferAmount="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lco/uk/getmondo/api/model/overdraft/ApiOverdraftStatus;->bufferAmount:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", dailyFeeAmount="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lco/uk/getmondo/api/model/overdraft/ApiOverdraftStatus;->dailyFeeAmount:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", limitAmount="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lco/uk/getmondo/api/model/overdraft/ApiOverdraftStatus;->limitAmount:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", preapprovedLimitAmount="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lco/uk/getmondo/api/model/overdraft/ApiOverdraftStatus;->preapprovedLimitAmount:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", chargeDate="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/api/model/overdraft/ApiOverdraftStatus;->chargeDate:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
