.class public final Lco/uk/getmondo/api/model/ApiBalanceLimits;
.super Ljava/lang/Object;
.source "ApiBalanceLimits.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000.\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0006\n\u0002\u0008\u0014\n\u0002\u0018\u0002\n\u0002\u0008/\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u000e\n\u0000\u0008\u0086\u0008\u0018\u00002\u00020\u0001B\u00ad\u0001\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0003\u0012\u0006\u0010\u0006\u001a\u00020\u0003\u0012\u0006\u0010\u0007\u001a\u00020\u0003\u0012\u0006\u0010\u0008\u001a\u00020\u0003\u0012\u0006\u0010\t\u001a\u00020\u0003\u0012\u0006\u0010\n\u001a\u00020\u0003\u0012\u0006\u0010\u000b\u001a\u00020\u0003\u0012\u0006\u0010\u000c\u001a\u00020\u0003\u0012\u0006\u0010\r\u001a\u00020\u0003\u0012\u0006\u0010\u000e\u001a\u00020\u0003\u0012\u0006\u0010\u000f\u001a\u00020\u0003\u0012\u0006\u0010\u0010\u001a\u00020\u0003\u0012\u0006\u0010\u0011\u001a\u00020\u0003\u0012\u0006\u0010\u0012\u001a\u00020\u0003\u0012\u0006\u0010\u0013\u001a\u00020\u0003\u0012\u0006\u0010\u0014\u001a\u00020\u0003\u0012\u0006\u0010\u0015\u001a\u00020\u0003\u0012\u0006\u0010\u0016\u001a\u00020\u0003\u0012\u0006\u0010\u0017\u001a\u00020\u0018\u00a2\u0006\u0002\u0010\u0019J\t\u00101\u001a\u00020\u0003H\u00c6\u0003J\t\u00102\u001a\u00020\u0003H\u00c6\u0003J\t\u00103\u001a\u00020\u0003H\u00c6\u0003J\t\u00104\u001a\u00020\u0003H\u00c6\u0003J\t\u00105\u001a\u00020\u0003H\u00c6\u0003J\t\u00106\u001a\u00020\u0003H\u00c6\u0003J\t\u00107\u001a\u00020\u0003H\u00c6\u0003J\t\u00108\u001a\u00020\u0003H\u00c6\u0003J\t\u00109\u001a\u00020\u0003H\u00c6\u0003J\t\u0010:\u001a\u00020\u0003H\u00c6\u0003J\t\u0010;\u001a\u00020\u0003H\u00c6\u0003J\t\u0010<\u001a\u00020\u0003H\u00c6\u0003J\t\u0010=\u001a\u00020\u0003H\u00c6\u0003J\t\u0010>\u001a\u00020\u0018H\u00c6\u0003J\t\u0010?\u001a\u00020\u0003H\u00c6\u0003J\t\u0010@\u001a\u00020\u0003H\u00c6\u0003J\t\u0010A\u001a\u00020\u0003H\u00c6\u0003J\t\u0010B\u001a\u00020\u0003H\u00c6\u0003J\t\u0010C\u001a\u00020\u0003H\u00c6\u0003J\t\u0010D\u001a\u00020\u0003H\u00c6\u0003J\t\u0010E\u001a\u00020\u0003H\u00c6\u0003J\u00db\u0001\u0010F\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0005\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0006\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0007\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0008\u001a\u00020\u00032\u0008\u0008\u0002\u0010\t\u001a\u00020\u00032\u0008\u0008\u0002\u0010\n\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u000b\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u000c\u001a\u00020\u00032\u0008\u0008\u0002\u0010\r\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u000e\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u000f\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0010\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0011\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0012\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0013\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0014\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0015\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0016\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0017\u001a\u00020\u0018H\u00c6\u0001J\u0013\u0010G\u001a\u00020H2\u0008\u0010I\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u0010J\u001a\u00020KH\u00d6\u0001J\t\u0010L\u001a\u00020MH\u00d6\u0001R\u0011\u0010\u0010\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001a\u0010\u001bR\u0011\u0010\u000f\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001c\u0010\u001bR\u0011\u0010\n\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001d\u0010\u001bR\u0011\u0010\t\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001e\u0010\u001bR\u0011\u0010\u000c\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001f\u0010\u001bR\u0011\u0010\u000b\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008 \u0010\u001bR\u0011\u0010\u0006\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008!\u0010\u001bR\u0011\u0010\u0005\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\"\u0010\u001bR\u0011\u0010\u0014\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008#\u0010\u001bR\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008$\u0010\u001bR\u0011\u0010\u0011\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008%\u0010\u001bR\u0011\u0010\u0004\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008&\u0010\u001bR\u0011\u0010\u000e\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\'\u0010\u001bR\u0011\u0010\r\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008(\u0010\u001bR\u0011\u0010\u0016\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008)\u0010\u001bR\u0011\u0010\u0015\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008*\u0010\u001bR\u0011\u0010\u0008\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008+\u0010\u001bR\u0011\u0010\u0007\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008,\u0010\u001bR\u0011\u0010\u0013\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008-\u0010\u001bR\u0011\u0010\u0012\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008.\u0010\u001bR\u0011\u0010\u0017\u001a\u00020\u0018\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008/\u00100\u00a8\u0006N"
    }
    d2 = {
        "Lco/uk/getmondo/api/model/ApiBalanceLimits;",
        "",
        "maxBalance",
        "",
        "maxSinglePosLimit",
        "dailyLoadLimitTotal",
        "dailyLoadLimit",
        "monthlyLoadLimitTotal",
        "monthlyLoadLimit",
        "annualLoadLimitTotal",
        "annualLoadLimit",
        "dailyCashLimitTotal",
        "dailyCashLimit",
        "monthlyCashLimitTotal",
        "monthlyCashLimit",
        "annualCashLimitTotal",
        "annualCashLimit",
        "maxSingleP2pLimit",
        "monthlyP2pLimitTotal",
        "monthlyP2pLimit",
        "inboundP2pMax",
        "monthlyInboundP2pLimitTotal",
        "monthlyInboundP2pLimit",
        "verificationType",
        "Lco/uk/getmondo/api/model/VerificationType;",
        "(DDDDDDDDDDDDDDDDDDDDLco/uk/getmondo/api/model/VerificationType;)V",
        "getAnnualCashLimit",
        "()D",
        "getAnnualCashLimitTotal",
        "getAnnualLoadLimit",
        "getAnnualLoadLimitTotal",
        "getDailyCashLimit",
        "getDailyCashLimitTotal",
        "getDailyLoadLimit",
        "getDailyLoadLimitTotal",
        "getInboundP2pMax",
        "getMaxBalance",
        "getMaxSingleP2pLimit",
        "getMaxSinglePosLimit",
        "getMonthlyCashLimit",
        "getMonthlyCashLimitTotal",
        "getMonthlyInboundP2pLimit",
        "getMonthlyInboundP2pLimitTotal",
        "getMonthlyLoadLimit",
        "getMonthlyLoadLimitTotal",
        "getMonthlyP2pLimit",
        "getMonthlyP2pLimitTotal",
        "getVerificationType",
        "()Lco/uk/getmondo/api/model/VerificationType;",
        "component1",
        "component10",
        "component11",
        "component12",
        "component13",
        "component14",
        "component15",
        "component16",
        "component17",
        "component18",
        "component19",
        "component2",
        "component20",
        "component21",
        "component3",
        "component4",
        "component5",
        "component6",
        "component7",
        "component8",
        "component9",
        "copy",
        "equals",
        "",
        "other",
        "hashCode",
        "",
        "toString",
        "",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field private final annualCashLimit:D

.field private final annualCashLimitTotal:D

.field private final annualLoadLimit:D

.field private final annualLoadLimitTotal:D

.field private final dailyCashLimit:D

.field private final dailyCashLimitTotal:D

.field private final dailyLoadLimit:D

.field private final dailyLoadLimitTotal:D

.field private final inboundP2pMax:D

.field private final maxBalance:D

.field private final maxSingleP2pLimit:D

.field private final maxSinglePosLimit:D

.field private final monthlyCashLimit:D

.field private final monthlyCashLimitTotal:D

.field private final monthlyInboundP2pLimit:D

.field private final monthlyInboundP2pLimitTotal:D

.field private final monthlyLoadLimit:D

.field private final monthlyLoadLimitTotal:D

.field private final monthlyP2pLimit:D

.field private final monthlyP2pLimitTotal:D

.field private final verificationType:Lco/uk/getmondo/api/model/VerificationType;


# direct methods
.method public constructor <init>(DDDDDDDDDDDDDDDDDDDDLco/uk/getmondo/api/model/VerificationType;)V
    .locals 3

    .prologue
    const-string v2, "verificationType"

    move-object/from16 v0, p41

    invoke-static {v0, v2}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-wide p1, p0, Lco/uk/getmondo/api/model/ApiBalanceLimits;->maxBalance:D

    iput-wide p3, p0, Lco/uk/getmondo/api/model/ApiBalanceLimits;->maxSinglePosLimit:D

    iput-wide p5, p0, Lco/uk/getmondo/api/model/ApiBalanceLimits;->dailyLoadLimitTotal:D

    iput-wide p7, p0, Lco/uk/getmondo/api/model/ApiBalanceLimits;->dailyLoadLimit:D

    iput-wide p9, p0, Lco/uk/getmondo/api/model/ApiBalanceLimits;->monthlyLoadLimitTotal:D

    iput-wide p11, p0, Lco/uk/getmondo/api/model/ApiBalanceLimits;->monthlyLoadLimit:D

    move-wide/from16 v0, p13

    iput-wide v0, p0, Lco/uk/getmondo/api/model/ApiBalanceLimits;->annualLoadLimitTotal:D

    move-wide/from16 v0, p15

    iput-wide v0, p0, Lco/uk/getmondo/api/model/ApiBalanceLimits;->annualLoadLimit:D

    move-wide/from16 v0, p17

    iput-wide v0, p0, Lco/uk/getmondo/api/model/ApiBalanceLimits;->dailyCashLimitTotal:D

    move-wide/from16 v0, p19

    iput-wide v0, p0, Lco/uk/getmondo/api/model/ApiBalanceLimits;->dailyCashLimit:D

    move-wide/from16 v0, p21

    iput-wide v0, p0, Lco/uk/getmondo/api/model/ApiBalanceLimits;->monthlyCashLimitTotal:D

    move-wide/from16 v0, p23

    iput-wide v0, p0, Lco/uk/getmondo/api/model/ApiBalanceLimits;->monthlyCashLimit:D

    move-wide/from16 v0, p25

    iput-wide v0, p0, Lco/uk/getmondo/api/model/ApiBalanceLimits;->annualCashLimitTotal:D

    move-wide/from16 v0, p27

    iput-wide v0, p0, Lco/uk/getmondo/api/model/ApiBalanceLimits;->annualCashLimit:D

    move-wide/from16 v0, p29

    iput-wide v0, p0, Lco/uk/getmondo/api/model/ApiBalanceLimits;->maxSingleP2pLimit:D

    move-wide/from16 v0, p31

    iput-wide v0, p0, Lco/uk/getmondo/api/model/ApiBalanceLimits;->monthlyP2pLimitTotal:D

    move-wide/from16 v0, p33

    iput-wide v0, p0, Lco/uk/getmondo/api/model/ApiBalanceLimits;->monthlyP2pLimit:D

    move-wide/from16 v0, p35

    iput-wide v0, p0, Lco/uk/getmondo/api/model/ApiBalanceLimits;->inboundP2pMax:D

    move-wide/from16 v0, p37

    iput-wide v0, p0, Lco/uk/getmondo/api/model/ApiBalanceLimits;->monthlyInboundP2pLimitTotal:D

    move-wide/from16 v0, p39

    iput-wide v0, p0, Lco/uk/getmondo/api/model/ApiBalanceLimits;->monthlyInboundP2pLimit:D

    move-object/from16 v0, p41

    iput-object v0, p0, Lco/uk/getmondo/api/model/ApiBalanceLimits;->verificationType:Lco/uk/getmondo/api/model/VerificationType;

    return-void
.end method


# virtual methods
.method public final a()D
    .locals 2

    .prologue
    .line 4
    iget-wide v0, p0, Lco/uk/getmondo/api/model/ApiBalanceLimits;->maxBalance:D

    return-wide v0
.end method

.method public final b()D
    .locals 2

    .prologue
    .line 5
    iget-wide v0, p0, Lco/uk/getmondo/api/model/ApiBalanceLimits;->maxSinglePosLimit:D

    return-wide v0
.end method

.method public final c()D
    .locals 2

    .prologue
    .line 6
    iget-wide v0, p0, Lco/uk/getmondo/api/model/ApiBalanceLimits;->dailyLoadLimitTotal:D

    return-wide v0
.end method

.method public final d()D
    .locals 2

    .prologue
    .line 7
    iget-wide v0, p0, Lco/uk/getmondo/api/model/ApiBalanceLimits;->dailyLoadLimit:D

    return-wide v0
.end method

.method public final e()D
    .locals 2

    .prologue
    .line 8
    iget-wide v0, p0, Lco/uk/getmondo/api/model/ApiBalanceLimits;->monthlyLoadLimitTotal:D

    return-wide v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    if-eq p0, p1, :cond_0

    instance-of v0, p1, Lco/uk/getmondo/api/model/ApiBalanceLimits;

    if-eqz v0, :cond_1

    check-cast p1, Lco/uk/getmondo/api/model/ApiBalanceLimits;

    iget-wide v0, p0, Lco/uk/getmondo/api/model/ApiBalanceLimits;->maxBalance:D

    iget-wide v2, p1, Lco/uk/getmondo/api/model/ApiBalanceLimits;->maxBalance:D

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Double;->compare(DD)I

    move-result v0

    if-nez v0, :cond_1

    iget-wide v0, p0, Lco/uk/getmondo/api/model/ApiBalanceLimits;->maxSinglePosLimit:D

    iget-wide v2, p1, Lco/uk/getmondo/api/model/ApiBalanceLimits;->maxSinglePosLimit:D

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Double;->compare(DD)I

    move-result v0

    if-nez v0, :cond_1

    iget-wide v0, p0, Lco/uk/getmondo/api/model/ApiBalanceLimits;->dailyLoadLimitTotal:D

    iget-wide v2, p1, Lco/uk/getmondo/api/model/ApiBalanceLimits;->dailyLoadLimitTotal:D

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Double;->compare(DD)I

    move-result v0

    if-nez v0, :cond_1

    iget-wide v0, p0, Lco/uk/getmondo/api/model/ApiBalanceLimits;->dailyLoadLimit:D

    iget-wide v2, p1, Lco/uk/getmondo/api/model/ApiBalanceLimits;->dailyLoadLimit:D

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Double;->compare(DD)I

    move-result v0

    if-nez v0, :cond_1

    iget-wide v0, p0, Lco/uk/getmondo/api/model/ApiBalanceLimits;->monthlyLoadLimitTotal:D

    iget-wide v2, p1, Lco/uk/getmondo/api/model/ApiBalanceLimits;->monthlyLoadLimitTotal:D

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Double;->compare(DD)I

    move-result v0

    if-nez v0, :cond_1

    iget-wide v0, p0, Lco/uk/getmondo/api/model/ApiBalanceLimits;->monthlyLoadLimit:D

    iget-wide v2, p1, Lco/uk/getmondo/api/model/ApiBalanceLimits;->monthlyLoadLimit:D

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Double;->compare(DD)I

    move-result v0

    if-nez v0, :cond_1

    iget-wide v0, p0, Lco/uk/getmondo/api/model/ApiBalanceLimits;->annualLoadLimitTotal:D

    iget-wide v2, p1, Lco/uk/getmondo/api/model/ApiBalanceLimits;->annualLoadLimitTotal:D

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Double;->compare(DD)I

    move-result v0

    if-nez v0, :cond_1

    iget-wide v0, p0, Lco/uk/getmondo/api/model/ApiBalanceLimits;->annualLoadLimit:D

    iget-wide v2, p1, Lco/uk/getmondo/api/model/ApiBalanceLimits;->annualLoadLimit:D

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Double;->compare(DD)I

    move-result v0

    if-nez v0, :cond_1

    iget-wide v0, p0, Lco/uk/getmondo/api/model/ApiBalanceLimits;->dailyCashLimitTotal:D

    iget-wide v2, p1, Lco/uk/getmondo/api/model/ApiBalanceLimits;->dailyCashLimitTotal:D

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Double;->compare(DD)I

    move-result v0

    if-nez v0, :cond_1

    iget-wide v0, p0, Lco/uk/getmondo/api/model/ApiBalanceLimits;->dailyCashLimit:D

    iget-wide v2, p1, Lco/uk/getmondo/api/model/ApiBalanceLimits;->dailyCashLimit:D

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Double;->compare(DD)I

    move-result v0

    if-nez v0, :cond_1

    iget-wide v0, p0, Lco/uk/getmondo/api/model/ApiBalanceLimits;->monthlyCashLimitTotal:D

    iget-wide v2, p1, Lco/uk/getmondo/api/model/ApiBalanceLimits;->monthlyCashLimitTotal:D

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Double;->compare(DD)I

    move-result v0

    if-nez v0, :cond_1

    iget-wide v0, p0, Lco/uk/getmondo/api/model/ApiBalanceLimits;->monthlyCashLimit:D

    iget-wide v2, p1, Lco/uk/getmondo/api/model/ApiBalanceLimits;->monthlyCashLimit:D

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Double;->compare(DD)I

    move-result v0

    if-nez v0, :cond_1

    iget-wide v0, p0, Lco/uk/getmondo/api/model/ApiBalanceLimits;->annualCashLimitTotal:D

    iget-wide v2, p1, Lco/uk/getmondo/api/model/ApiBalanceLimits;->annualCashLimitTotal:D

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Double;->compare(DD)I

    move-result v0

    if-nez v0, :cond_1

    iget-wide v0, p0, Lco/uk/getmondo/api/model/ApiBalanceLimits;->annualCashLimit:D

    iget-wide v2, p1, Lco/uk/getmondo/api/model/ApiBalanceLimits;->annualCashLimit:D

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Double;->compare(DD)I

    move-result v0

    if-nez v0, :cond_1

    iget-wide v0, p0, Lco/uk/getmondo/api/model/ApiBalanceLimits;->maxSingleP2pLimit:D

    iget-wide v2, p1, Lco/uk/getmondo/api/model/ApiBalanceLimits;->maxSingleP2pLimit:D

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Double;->compare(DD)I

    move-result v0

    if-nez v0, :cond_1

    iget-wide v0, p0, Lco/uk/getmondo/api/model/ApiBalanceLimits;->monthlyP2pLimitTotal:D

    iget-wide v2, p1, Lco/uk/getmondo/api/model/ApiBalanceLimits;->monthlyP2pLimitTotal:D

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Double;->compare(DD)I

    move-result v0

    if-nez v0, :cond_1

    iget-wide v0, p0, Lco/uk/getmondo/api/model/ApiBalanceLimits;->monthlyP2pLimit:D

    iget-wide v2, p1, Lco/uk/getmondo/api/model/ApiBalanceLimits;->monthlyP2pLimit:D

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Double;->compare(DD)I

    move-result v0

    if-nez v0, :cond_1

    iget-wide v0, p0, Lco/uk/getmondo/api/model/ApiBalanceLimits;->inboundP2pMax:D

    iget-wide v2, p1, Lco/uk/getmondo/api/model/ApiBalanceLimits;->inboundP2pMax:D

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Double;->compare(DD)I

    move-result v0

    if-nez v0, :cond_1

    iget-wide v0, p0, Lco/uk/getmondo/api/model/ApiBalanceLimits;->monthlyInboundP2pLimitTotal:D

    iget-wide v2, p1, Lco/uk/getmondo/api/model/ApiBalanceLimits;->monthlyInboundP2pLimitTotal:D

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Double;->compare(DD)I

    move-result v0

    if-nez v0, :cond_1

    iget-wide v0, p0, Lco/uk/getmondo/api/model/ApiBalanceLimits;->monthlyInboundP2pLimit:D

    iget-wide v2, p1, Lco/uk/getmondo/api/model/ApiBalanceLimits;->monthlyInboundP2pLimit:D

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Double;->compare(DD)I

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lco/uk/getmondo/api/model/ApiBalanceLimits;->verificationType:Lco/uk/getmondo/api/model/VerificationType;

    iget-object v1, p1, Lco/uk/getmondo/api/model/ApiBalanceLimits;->verificationType:Lco/uk/getmondo/api/model/VerificationType;

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final f()D
    .locals 2

    .prologue
    .line 9
    iget-wide v0, p0, Lco/uk/getmondo/api/model/ApiBalanceLimits;->monthlyLoadLimit:D

    return-wide v0
.end method

.method public final g()D
    .locals 2

    .prologue
    .line 10
    iget-wide v0, p0, Lco/uk/getmondo/api/model/ApiBalanceLimits;->annualLoadLimitTotal:D

    return-wide v0
.end method

.method public final h()D
    .locals 2

    .prologue
    .line 11
    iget-wide v0, p0, Lco/uk/getmondo/api/model/ApiBalanceLimits;->annualLoadLimit:D

    return-wide v0
.end method

.method public hashCode()I
    .locals 7

    const/16 v6, 0x20

    iget-wide v0, p0, Lco/uk/getmondo/api/model/ApiBalanceLimits;->maxBalance:D

    invoke-static {v0, v1}, Ljava/lang/Double;->doubleToLongBits(D)J

    move-result-wide v0

    ushr-long v2, v0, v6

    xor-long/2addr v0, v2

    long-to-int v0, v0

    mul-int/lit8 v0, v0, 0x1f

    iget-wide v2, p0, Lco/uk/getmondo/api/model/ApiBalanceLimits;->maxSinglePosLimit:D

    invoke-static {v2, v3}, Ljava/lang/Double;->doubleToLongBits(D)J

    move-result-wide v2

    ushr-long v4, v2, v6

    xor-long/2addr v2, v4

    long-to-int v1, v2

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-wide v2, p0, Lco/uk/getmondo/api/model/ApiBalanceLimits;->dailyLoadLimitTotal:D

    invoke-static {v2, v3}, Ljava/lang/Double;->doubleToLongBits(D)J

    move-result-wide v2

    ushr-long v4, v2, v6

    xor-long/2addr v2, v4

    long-to-int v1, v2

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-wide v2, p0, Lco/uk/getmondo/api/model/ApiBalanceLimits;->dailyLoadLimit:D

    invoke-static {v2, v3}, Ljava/lang/Double;->doubleToLongBits(D)J

    move-result-wide v2

    ushr-long v4, v2, v6

    xor-long/2addr v2, v4

    long-to-int v1, v2

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-wide v2, p0, Lco/uk/getmondo/api/model/ApiBalanceLimits;->monthlyLoadLimitTotal:D

    invoke-static {v2, v3}, Ljava/lang/Double;->doubleToLongBits(D)J

    move-result-wide v2

    ushr-long v4, v2, v6

    xor-long/2addr v2, v4

    long-to-int v1, v2

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-wide v2, p0, Lco/uk/getmondo/api/model/ApiBalanceLimits;->monthlyLoadLimit:D

    invoke-static {v2, v3}, Ljava/lang/Double;->doubleToLongBits(D)J

    move-result-wide v2

    ushr-long v4, v2, v6

    xor-long/2addr v2, v4

    long-to-int v1, v2

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-wide v2, p0, Lco/uk/getmondo/api/model/ApiBalanceLimits;->annualLoadLimitTotal:D

    invoke-static {v2, v3}, Ljava/lang/Double;->doubleToLongBits(D)J

    move-result-wide v2

    ushr-long v4, v2, v6

    xor-long/2addr v2, v4

    long-to-int v1, v2

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-wide v2, p0, Lco/uk/getmondo/api/model/ApiBalanceLimits;->annualLoadLimit:D

    invoke-static {v2, v3}, Ljava/lang/Double;->doubleToLongBits(D)J

    move-result-wide v2

    ushr-long v4, v2, v6

    xor-long/2addr v2, v4

    long-to-int v1, v2

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-wide v2, p0, Lco/uk/getmondo/api/model/ApiBalanceLimits;->dailyCashLimitTotal:D

    invoke-static {v2, v3}, Ljava/lang/Double;->doubleToLongBits(D)J

    move-result-wide v2

    ushr-long v4, v2, v6

    xor-long/2addr v2, v4

    long-to-int v1, v2

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-wide v2, p0, Lco/uk/getmondo/api/model/ApiBalanceLimits;->dailyCashLimit:D

    invoke-static {v2, v3}, Ljava/lang/Double;->doubleToLongBits(D)J

    move-result-wide v2

    ushr-long v4, v2, v6

    xor-long/2addr v2, v4

    long-to-int v1, v2

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-wide v2, p0, Lco/uk/getmondo/api/model/ApiBalanceLimits;->monthlyCashLimitTotal:D

    invoke-static {v2, v3}, Ljava/lang/Double;->doubleToLongBits(D)J

    move-result-wide v2

    ushr-long v4, v2, v6

    xor-long/2addr v2, v4

    long-to-int v1, v2

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-wide v2, p0, Lco/uk/getmondo/api/model/ApiBalanceLimits;->monthlyCashLimit:D

    invoke-static {v2, v3}, Ljava/lang/Double;->doubleToLongBits(D)J

    move-result-wide v2

    ushr-long v4, v2, v6

    xor-long/2addr v2, v4

    long-to-int v1, v2

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-wide v2, p0, Lco/uk/getmondo/api/model/ApiBalanceLimits;->annualCashLimitTotal:D

    invoke-static {v2, v3}, Ljava/lang/Double;->doubleToLongBits(D)J

    move-result-wide v2

    ushr-long v4, v2, v6

    xor-long/2addr v2, v4

    long-to-int v1, v2

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-wide v2, p0, Lco/uk/getmondo/api/model/ApiBalanceLimits;->annualCashLimit:D

    invoke-static {v2, v3}, Ljava/lang/Double;->doubleToLongBits(D)J

    move-result-wide v2

    ushr-long v4, v2, v6

    xor-long/2addr v2, v4

    long-to-int v1, v2

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-wide v2, p0, Lco/uk/getmondo/api/model/ApiBalanceLimits;->maxSingleP2pLimit:D

    invoke-static {v2, v3}, Ljava/lang/Double;->doubleToLongBits(D)J

    move-result-wide v2

    ushr-long v4, v2, v6

    xor-long/2addr v2, v4

    long-to-int v1, v2

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-wide v2, p0, Lco/uk/getmondo/api/model/ApiBalanceLimits;->monthlyP2pLimitTotal:D

    invoke-static {v2, v3}, Ljava/lang/Double;->doubleToLongBits(D)J

    move-result-wide v2

    ushr-long v4, v2, v6

    xor-long/2addr v2, v4

    long-to-int v1, v2

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-wide v2, p0, Lco/uk/getmondo/api/model/ApiBalanceLimits;->monthlyP2pLimit:D

    invoke-static {v2, v3}, Ljava/lang/Double;->doubleToLongBits(D)J

    move-result-wide v2

    ushr-long v4, v2, v6

    xor-long/2addr v2, v4

    long-to-int v1, v2

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-wide v2, p0, Lco/uk/getmondo/api/model/ApiBalanceLimits;->inboundP2pMax:D

    invoke-static {v2, v3}, Ljava/lang/Double;->doubleToLongBits(D)J

    move-result-wide v2

    ushr-long v4, v2, v6

    xor-long/2addr v2, v4

    long-to-int v1, v2

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-wide v2, p0, Lco/uk/getmondo/api/model/ApiBalanceLimits;->monthlyInboundP2pLimitTotal:D

    invoke-static {v2, v3}, Ljava/lang/Double;->doubleToLongBits(D)J

    move-result-wide v2

    ushr-long v4, v2, v6

    xor-long/2addr v2, v4

    long-to-int v1, v2

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-wide v2, p0, Lco/uk/getmondo/api/model/ApiBalanceLimits;->monthlyInboundP2pLimit:D

    invoke-static {v2, v3}, Ljava/lang/Double;->doubleToLongBits(D)J

    move-result-wide v2

    ushr-long v4, v2, v6

    xor-long/2addr v2, v4

    long-to-int v1, v2

    add-int/2addr v0, v1

    mul-int/lit8 v1, v0, 0x1f

    iget-object v0, p0, Lco/uk/getmondo/api/model/ApiBalanceLimits;->verificationType:Lco/uk/getmondo/api/model/VerificationType;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    :goto_0
    add-int/2addr v0, v1

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final i()D
    .locals 2

    .prologue
    .line 12
    iget-wide v0, p0, Lco/uk/getmondo/api/model/ApiBalanceLimits;->dailyCashLimitTotal:D

    return-wide v0
.end method

.method public final j()D
    .locals 2

    .prologue
    .line 13
    iget-wide v0, p0, Lco/uk/getmondo/api/model/ApiBalanceLimits;->dailyCashLimit:D

    return-wide v0
.end method

.method public final k()D
    .locals 2

    .prologue
    .line 14
    iget-wide v0, p0, Lco/uk/getmondo/api/model/ApiBalanceLimits;->monthlyCashLimitTotal:D

    return-wide v0
.end method

.method public final l()D
    .locals 2

    .prologue
    .line 15
    iget-wide v0, p0, Lco/uk/getmondo/api/model/ApiBalanceLimits;->monthlyCashLimit:D

    return-wide v0
.end method

.method public final m()D
    .locals 2

    .prologue
    .line 16
    iget-wide v0, p0, Lco/uk/getmondo/api/model/ApiBalanceLimits;->annualCashLimitTotal:D

    return-wide v0
.end method

.method public final n()D
    .locals 2

    .prologue
    .line 17
    iget-wide v0, p0, Lco/uk/getmondo/api/model/ApiBalanceLimits;->annualCashLimit:D

    return-wide v0
.end method

.method public final o()D
    .locals 2

    .prologue
    .line 18
    iget-wide v0, p0, Lco/uk/getmondo/api/model/ApiBalanceLimits;->maxSingleP2pLimit:D

    return-wide v0
.end method

.method public final p()D
    .locals 2

    .prologue
    .line 19
    iget-wide v0, p0, Lco/uk/getmondo/api/model/ApiBalanceLimits;->monthlyP2pLimitTotal:D

    return-wide v0
.end method

.method public final q()D
    .locals 2

    .prologue
    .line 20
    iget-wide v0, p0, Lco/uk/getmondo/api/model/ApiBalanceLimits;->monthlyP2pLimit:D

    return-wide v0
.end method

.method public final r()D
    .locals 2

    .prologue
    .line 21
    iget-wide v0, p0, Lco/uk/getmondo/api/model/ApiBalanceLimits;->inboundP2pMax:D

    return-wide v0
.end method

.method public final s()D
    .locals 2

    .prologue
    .line 22
    iget-wide v0, p0, Lco/uk/getmondo/api/model/ApiBalanceLimits;->monthlyInboundP2pLimitTotal:D

    return-wide v0
.end method

.method public final t()D
    .locals 2

    .prologue
    .line 23
    iget-wide v0, p0, Lco/uk/getmondo/api/model/ApiBalanceLimits;->monthlyInboundP2pLimit:D

    return-wide v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "ApiBalanceLimits(maxBalance="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lco/uk/getmondo/api/model/ApiBalanceLimits;->maxBalance:D

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", maxSinglePosLimit="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lco/uk/getmondo/api/model/ApiBalanceLimits;->maxSinglePosLimit:D

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", dailyLoadLimitTotal="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lco/uk/getmondo/api/model/ApiBalanceLimits;->dailyLoadLimitTotal:D

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", dailyLoadLimit="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lco/uk/getmondo/api/model/ApiBalanceLimits;->dailyLoadLimit:D

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", monthlyLoadLimitTotal="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lco/uk/getmondo/api/model/ApiBalanceLimits;->monthlyLoadLimitTotal:D

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", monthlyLoadLimit="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lco/uk/getmondo/api/model/ApiBalanceLimits;->monthlyLoadLimit:D

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", annualLoadLimitTotal="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lco/uk/getmondo/api/model/ApiBalanceLimits;->annualLoadLimitTotal:D

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", annualLoadLimit="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lco/uk/getmondo/api/model/ApiBalanceLimits;->annualLoadLimit:D

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", dailyCashLimitTotal="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lco/uk/getmondo/api/model/ApiBalanceLimits;->dailyCashLimitTotal:D

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", dailyCashLimit="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lco/uk/getmondo/api/model/ApiBalanceLimits;->dailyCashLimit:D

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", monthlyCashLimitTotal="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lco/uk/getmondo/api/model/ApiBalanceLimits;->monthlyCashLimitTotal:D

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", monthlyCashLimit="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lco/uk/getmondo/api/model/ApiBalanceLimits;->monthlyCashLimit:D

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", annualCashLimitTotal="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lco/uk/getmondo/api/model/ApiBalanceLimits;->annualCashLimitTotal:D

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", annualCashLimit="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lco/uk/getmondo/api/model/ApiBalanceLimits;->annualCashLimit:D

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", maxSingleP2pLimit="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lco/uk/getmondo/api/model/ApiBalanceLimits;->maxSingleP2pLimit:D

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", monthlyP2pLimitTotal="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lco/uk/getmondo/api/model/ApiBalanceLimits;->monthlyP2pLimitTotal:D

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", monthlyP2pLimit="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lco/uk/getmondo/api/model/ApiBalanceLimits;->monthlyP2pLimit:D

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", inboundP2pMax="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lco/uk/getmondo/api/model/ApiBalanceLimits;->inboundP2pMax:D

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", monthlyInboundP2pLimitTotal="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lco/uk/getmondo/api/model/ApiBalanceLimits;->monthlyInboundP2pLimitTotal:D

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", monthlyInboundP2pLimit="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lco/uk/getmondo/api/model/ApiBalanceLimits;->monthlyInboundP2pLimit:D

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", verificationType="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/api/model/ApiBalanceLimits;->verificationType:Lco/uk/getmondo/api/model/VerificationType;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final u()Lco/uk/getmondo/api/model/VerificationType;
    .locals 1

    .prologue
    .line 24
    iget-object v0, p0, Lco/uk/getmondo/api/model/ApiBalanceLimits;->verificationType:Lco/uk/getmondo/api/model/VerificationType;

    return-object v0
.end method
