.class public final enum Lco/uk/getmondo/api/model/pin/a;
.super Ljava/lang/Enum;
.source "PinSmsError.java"

# interfaces
.implements Lco/uk/getmondo/common/e/f;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lco/uk/getmondo/api/model/pin/a;",
        ">;",
        "Lco/uk/getmondo/common/e/f;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lco/uk/getmondo/api/model/pin/a;

.field public static final enum CARD_NOT_FOUND:Lco/uk/getmondo/api/model/pin/a;

.field public static final enum DOB_CHECK_FAILED:Lco/uk/getmondo/api/model/pin/a;

.field public static final enum INCORRECT_DOB:Lco/uk/getmondo/api/model/pin/a;

.field public static final enum NUMBER_CHANGE:Lco/uk/getmondo/api/model/pin/a;

.field public static final enum NUMBER_NOT_FOUND:Lco/uk/getmondo/api/model/pin/a;

.field public static final enum PROFILE_NOT_FOUND:Lco/uk/getmondo/api/model/pin/a;


# instance fields
.field private final prefix:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 9
    new-instance v0, Lco/uk/getmondo/api/model/pin/a;

    const-string v1, "PROFILE_NOT_FOUND"

    const-string v2, "not_found.profile_not_found"

    invoke-direct {v0, v1, v4, v2}, Lco/uk/getmondo/api/model/pin/a;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lco/uk/getmondo/api/model/pin/a;->PROFILE_NOT_FOUND:Lco/uk/getmondo/api/model/pin/a;

    .line 10
    new-instance v0, Lco/uk/getmondo/api/model/pin/a;

    const-string v1, "CARD_NOT_FOUND"

    const-string v2, "not_found.card_not_found"

    invoke-direct {v0, v1, v5, v2}, Lco/uk/getmondo/api/model/pin/a;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lco/uk/getmondo/api/model/pin/a;->CARD_NOT_FOUND:Lco/uk/getmondo/api/model/pin/a;

    .line 11
    new-instance v0, Lco/uk/getmondo/api/model/pin/a;

    const-string v1, "NUMBER_NOT_FOUND"

    const-string v2, "not_found.phone_number_not_found"

    invoke-direct {v0, v1, v6, v2}, Lco/uk/getmondo/api/model/pin/a;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lco/uk/getmondo/api/model/pin/a;->NUMBER_NOT_FOUND:Lco/uk/getmondo/api/model/pin/a;

    .line 12
    new-instance v0, Lco/uk/getmondo/api/model/pin/a;

    const-string v1, "NUMBER_CHANGE"

    const-string v2, "forbidden.recent_phone_number_change"

    invoke-direct {v0, v1, v7, v2}, Lco/uk/getmondo/api/model/pin/a;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lco/uk/getmondo/api/model/pin/a;->NUMBER_CHANGE:Lco/uk/getmondo/api/model/pin/a;

    .line 13
    new-instance v0, Lco/uk/getmondo/api/model/pin/a;

    const-string v1, "DOB_CHECK_FAILED"

    const-string v2, "forbidden.dob_check_failed"

    invoke-direct {v0, v1, v8, v2}, Lco/uk/getmondo/api/model/pin/a;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lco/uk/getmondo/api/model/pin/a;->DOB_CHECK_FAILED:Lco/uk/getmondo/api/model/pin/a;

    .line 14
    new-instance v0, Lco/uk/getmondo/api/model/pin/a;

    const-string v1, "INCORRECT_DOB"

    const/4 v2, 0x5

    const-string v3, "bad_request.bad_param.date_of_birth"

    invoke-direct {v0, v1, v2, v3}, Lco/uk/getmondo/api/model/pin/a;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lco/uk/getmondo/api/model/pin/a;->INCORRECT_DOB:Lco/uk/getmondo/api/model/pin/a;

    .line 7
    const/4 v0, 0x6

    new-array v0, v0, [Lco/uk/getmondo/api/model/pin/a;

    sget-object v1, Lco/uk/getmondo/api/model/pin/a;->PROFILE_NOT_FOUND:Lco/uk/getmondo/api/model/pin/a;

    aput-object v1, v0, v4

    sget-object v1, Lco/uk/getmondo/api/model/pin/a;->CARD_NOT_FOUND:Lco/uk/getmondo/api/model/pin/a;

    aput-object v1, v0, v5

    sget-object v1, Lco/uk/getmondo/api/model/pin/a;->NUMBER_NOT_FOUND:Lco/uk/getmondo/api/model/pin/a;

    aput-object v1, v0, v6

    sget-object v1, Lco/uk/getmondo/api/model/pin/a;->NUMBER_CHANGE:Lco/uk/getmondo/api/model/pin/a;

    aput-object v1, v0, v7

    sget-object v1, Lco/uk/getmondo/api/model/pin/a;->DOB_CHECK_FAILED:Lco/uk/getmondo/api/model/pin/a;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lco/uk/getmondo/api/model/pin/a;->INCORRECT_DOB:Lco/uk/getmondo/api/model/pin/a;

    aput-object v2, v0, v1

    sput-object v0, Lco/uk/getmondo/api/model/pin/a;->$VALUES:[Lco/uk/getmondo/api/model/pin/a;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 18
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 19
    iput-object p3, p0, Lco/uk/getmondo/api/model/pin/a;->prefix:Ljava/lang/String;

    .line 20
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lco/uk/getmondo/api/model/pin/a;
    .locals 1

    .prologue
    .line 7
    const-class v0, Lco/uk/getmondo/api/model/pin/a;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/api/model/pin/a;

    return-object v0
.end method

.method public static values()[Lco/uk/getmondo/api/model/pin/a;
    .locals 1

    .prologue
    .line 7
    sget-object v0, Lco/uk/getmondo/api/model/pin/a;->$VALUES:[Lco/uk/getmondo/api/model/pin/a;

    invoke-virtual {v0}, [Lco/uk/getmondo/api/model/pin/a;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lco/uk/getmondo/api/model/pin/a;

    return-object v0
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, Lco/uk/getmondo/api/model/pin/a;->prefix:Ljava/lang/String;

    return-object v0
.end method
