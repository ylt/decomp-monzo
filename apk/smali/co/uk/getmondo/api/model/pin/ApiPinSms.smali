.class public final Lco/uk/getmondo/api/model/pin/ApiPinSms;
.super Ljava/lang/Object;
.source "ApiPinSms.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0005\u0018\u00002\u00020\u0001B\u0011\u0012\n\u0008\u0002\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\u0002\u0010\u0004R\u0015\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\n\n\u0002\u0010\u0007\u001a\u0004\u0008\u0005\u0010\u0006\u00a8\u0006\u0008"
    }
    d2 = {
        "Lco/uk/getmondo/api/model/pin/ApiPinSms;",
        "",
        "smsBlocked",
        "",
        "(Ljava/lang/Boolean;)V",
        "getSmsBlocked",
        "()Ljava/lang/Boolean;",
        "Ljava/lang/Boolean;",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field private final smsBlocked:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    const/4 v0, 0x1

    invoke-direct {p0, v1, v0, v1}, Lco/uk/getmondo/api/model/pin/ApiPinSms;-><init>(Ljava/lang/Boolean;ILkotlin/d/b/i;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/Boolean;)V
    .locals 0

    .prologue
    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lco/uk/getmondo/api/model/pin/ApiPinSms;->smsBlocked:Ljava/lang/Boolean;

    return-void
.end method

.method public synthetic constructor <init>(Ljava/lang/Boolean;ILkotlin/d/b/i;)V
    .locals 1

    .prologue
    and-int/lit8 v0, p2, 0x1

    if-eqz v0, :cond_0

    .line 3
    const/4 v0, 0x0

    check-cast v0, Ljava/lang/Boolean;

    :goto_0
    invoke-direct {p0, v0}, Lco/uk/getmondo/api/model/pin/ApiPinSms;-><init>(Ljava/lang/Boolean;)V

    return-void

    :cond_0
    move-object v0, p1

    goto :goto_0
.end method


# virtual methods
.method public final a()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 3
    iget-object v0, p0, Lco/uk/getmondo/api/model/pin/ApiPinSms;->smsBlocked:Ljava/lang/Boolean;

    return-object v0
.end method
