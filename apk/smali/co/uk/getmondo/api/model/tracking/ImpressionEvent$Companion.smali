.class public final Lco/uk/getmondo/api/model/tracking/ImpressionEvent$Companion;
.super Ljava/lang/Object;
.source "ImpressionEvent.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lco/uk/getmondo/api/model/tracking/ImpressionEvent;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0008\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002R\u0014\u0010\u0003\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0005\u0010\u0006\u00a8\u0006\u0007"
    }
    d2 = {
        "Lco/uk/getmondo/api/model/tracking/ImpressionEvent$Companion;",
        "",
        "()V",
        "DATE_FORMATTER",
        "Ljava/text/SimpleDateFormat;",
        "getDATE_FORMATTER",
        "()Ljava/text/SimpleDateFormat;",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/d/b/i;)V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0}, Lco/uk/getmondo/api/model/tracking/ImpressionEvent$Companion;-><init>()V

    return-void
.end method

.method private final a()Ljava/text/SimpleDateFormat;
    .locals 1

    .prologue
    .line 13
    invoke-static {}, Lco/uk/getmondo/api/model/tracking/ImpressionEvent;->a()Ljava/text/SimpleDateFormat;

    move-result-object v0

    return-object v0
.end method

.method public static final synthetic a(Lco/uk/getmondo/api/model/tracking/ImpressionEvent$Companion;)Ljava/text/SimpleDateFormat;
    .locals 1

    .prologue
    .line 12
    invoke-direct {p0}, Lco/uk/getmondo/api/model/tracking/ImpressionEvent$Companion;->a()Ljava/text/SimpleDateFormat;

    move-result-object v0

    return-object v0
.end method
