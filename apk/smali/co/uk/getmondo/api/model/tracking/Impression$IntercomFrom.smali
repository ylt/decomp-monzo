.class public final enum Lco/uk/getmondo/api/model/tracking/Impression$IntercomFrom;
.super Ljava/lang/Enum;
.source "Impression.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lco/uk/getmondo/api/model/tracking/Impression;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "IntercomFrom"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lco/uk/getmondo/api/model/tracking/Impression$IntercomFrom;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u000c\u0008\u0086\u0001\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00000\u0001B\u000f\u0008\u0002\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004R\u001a\u0010\u0002\u001a\u00020\u0003X\u0080\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u0005\u0010\u0006\"\u0004\u0008\u0007\u0010\u0008j\u0002\u0008\tj\u0002\u0008\nj\u0002\u0008\u000bj\u0002\u0008\u000cj\u0002\u0008\rj\u0002\u0008\u000e\u00a8\u0006\u000f"
    }
    d2 = {
        "Lco/uk/getmondo/api/model/tracking/Impression$IntercomFrom;",
        "",
        "value",
        "",
        "(Ljava/lang/String;ILjava/lang/String;)V",
        "getValue$app_monzoPrepaidRelease",
        "()Ljava/lang/String;",
        "setValue$app_monzoPrepaidRelease",
        "(Ljava/lang/String;)V",
        "MAIN_NAV",
        "INITIAL_TOPUP",
        "GOOD_TO_GO",
        "PIN",
        "ADDRESS_CHANGE",
        "HELP",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lco/uk/getmondo/api/model/tracking/Impression$IntercomFrom;

.field public static final enum ADDRESS_CHANGE:Lco/uk/getmondo/api/model/tracking/Impression$IntercomFrom;

.field public static final enum GOOD_TO_GO:Lco/uk/getmondo/api/model/tracking/Impression$IntercomFrom;

.field public static final enum HELP:Lco/uk/getmondo/api/model/tracking/Impression$IntercomFrom;

.field public static final enum INITIAL_TOPUP:Lco/uk/getmondo/api/model/tracking/Impression$IntercomFrom;

.field public static final enum MAIN_NAV:Lco/uk/getmondo/api/model/tracking/Impression$IntercomFrom;

.field public static final enum PIN:Lco/uk/getmondo/api/model/tracking/Impression$IntercomFrom;


# instance fields
.field private value:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    const/4 v0, 0x6

    new-array v0, v0, [Lco/uk/getmondo/api/model/tracking/Impression$IntercomFrom;

    new-instance v1, Lco/uk/getmondo/api/model/tracking/Impression$IntercomFrom;

    const-string v2, "MAIN_NAV"

    .line 72
    const-string v3, "main_nav"

    invoke-direct {v1, v2, v4, v3}, Lco/uk/getmondo/api/model/tracking/Impression$IntercomFrom;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lco/uk/getmondo/api/model/tracking/Impression$IntercomFrom;->MAIN_NAV:Lco/uk/getmondo/api/model/tracking/Impression$IntercomFrom;

    aput-object v1, v0, v4

    new-instance v1, Lco/uk/getmondo/api/model/tracking/Impression$IntercomFrom;

    const-string v2, "INITIAL_TOPUP"

    .line 73
    const-string v3, "initial_topup"

    invoke-direct {v1, v2, v5, v3}, Lco/uk/getmondo/api/model/tracking/Impression$IntercomFrom;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lco/uk/getmondo/api/model/tracking/Impression$IntercomFrom;->INITIAL_TOPUP:Lco/uk/getmondo/api/model/tracking/Impression$IntercomFrom;

    aput-object v1, v0, v5

    new-instance v1, Lco/uk/getmondo/api/model/tracking/Impression$IntercomFrom;

    const-string v2, "GOOD_TO_GO"

    .line 74
    const-string v3, "good_to_go"

    invoke-direct {v1, v2, v6, v3}, Lco/uk/getmondo/api/model/tracking/Impression$IntercomFrom;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lco/uk/getmondo/api/model/tracking/Impression$IntercomFrom;->GOOD_TO_GO:Lco/uk/getmondo/api/model/tracking/Impression$IntercomFrom;

    aput-object v1, v0, v6

    new-instance v1, Lco/uk/getmondo/api/model/tracking/Impression$IntercomFrom;

    const-string v2, "PIN"

    .line 75
    const-string v3, "pin"

    invoke-direct {v1, v2, v7, v3}, Lco/uk/getmondo/api/model/tracking/Impression$IntercomFrom;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lco/uk/getmondo/api/model/tracking/Impression$IntercomFrom;->PIN:Lco/uk/getmondo/api/model/tracking/Impression$IntercomFrom;

    aput-object v1, v0, v7

    new-instance v1, Lco/uk/getmondo/api/model/tracking/Impression$IntercomFrom;

    const-string v2, "ADDRESS_CHANGE"

    .line 76
    const-string v3, "address-change"

    invoke-direct {v1, v2, v8, v3}, Lco/uk/getmondo/api/model/tracking/Impression$IntercomFrom;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lco/uk/getmondo/api/model/tracking/Impression$IntercomFrom;->ADDRESS_CHANGE:Lco/uk/getmondo/api/model/tracking/Impression$IntercomFrom;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    new-instance v2, Lco/uk/getmondo/api/model/tracking/Impression$IntercomFrom;

    const-string v3, "HELP"

    const/4 v4, 0x5

    .line 77
    const-string v5, "help"

    invoke-direct {v2, v3, v4, v5}, Lco/uk/getmondo/api/model/tracking/Impression$IntercomFrom;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v2, Lco/uk/getmondo/api/model/tracking/Impression$IntercomFrom;->HELP:Lco/uk/getmondo/api/model/tracking/Impression$IntercomFrom;

    aput-object v2, v0, v1

    sput-object v0, Lco/uk/getmondo/api/model/tracking/Impression$IntercomFrom;->$VALUES:[Lco/uk/getmondo/api/model/tracking/Impression$IntercomFrom;

    return-void
.end method

.method protected constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    const-string v0, "value"

    invoke-static {p3, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 71
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput-object p3, p0, Lco/uk/getmondo/api/model/tracking/Impression$IntercomFrom;->value:Ljava/lang/String;

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lco/uk/getmondo/api/model/tracking/Impression$IntercomFrom;
    .locals 1

    const-class v0, Lco/uk/getmondo/api/model/tracking/Impression$IntercomFrom;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/api/model/tracking/Impression$IntercomFrom;

    return-object v0
.end method

.method public static values()[Lco/uk/getmondo/api/model/tracking/Impression$IntercomFrom;
    .locals 1

    sget-object v0, Lco/uk/getmondo/api/model/tracking/Impression$IntercomFrom;->$VALUES:[Lco/uk/getmondo/api/model/tracking/Impression$IntercomFrom;

    invoke-virtual {v0}, [Lco/uk/getmondo/api/model/tracking/Impression$IntercomFrom;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lco/uk/getmondo/api/model/tracking/Impression$IntercomFrom;

    return-object v0
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lco/uk/getmondo/api/model/tracking/Impression$IntercomFrom;->value:Ljava/lang/String;

    return-object v0
.end method
