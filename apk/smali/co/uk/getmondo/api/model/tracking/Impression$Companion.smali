.class public final Lco/uk/getmondo/api/model/tracking/Impression$Companion;
.super Ljava/lang/Object;
.source "Impression.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lco/uk/getmondo/api/model/tracking/Impression;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000\u00c6\u0001\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0010\u0008\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u000f\n\u0002\u0018\u0002\n\u0002\u0008\u000e\n\u0002\u0018\u0002\n\u0002\u0008\r\n\u0002\u0010\t\n\u0002\u0008\u0017\n\u0002\u0010\u0011\n\u0002\u0008\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0017\n\u0002\u0018\u0002\n\u0002\u0008\u001b\n\u0002\u0018\u0002\n\u0002\u0008\u000e\n\u0002\u0018\u0002\n\u0002\u0008\u000f\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\t\n\u0002\u0018\u0002\n\u0002\u0008\u000c\n\u0002\u0018\u0002\n\u0002\u0008\u0018\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u001c\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\n\u0008\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u0008\u0010\u0007\u001a\u00020\u0008H\u0007J\u0008\u0010\t\u001a\u00020\u0008H\u0007J\u0010\u0010\n\u001a\u00020\u00082\u0006\u0010\u000b\u001a\u00020\u0004H\u0007J,\u0010\u000c\u001a\u00020\u00082\u0006\u0010\r\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u00042\u0008\u0010\u0010\u001a\u0004\u0018\u00010\u00042\u0008\u0010\u000c\u001a\u0004\u0018\u00010\u0011H\u0007J\u0008\u0010\u0012\u001a\u00020\u0008H\u0007J\u0008\u0010\u0013\u001a\u00020\u0008H\u0007J \u0010\u0014\u001a\u00020\u00082\u0006\u0010\u0015\u001a\u00020\u00162\u0006\u0010\u0017\u001a\u00020\u000e2\u0006\u0010\u0018\u001a\u00020\u0019H\u0007J\u0010\u0010\u001a\u001a\u00020\u00082\u0006\u0010\u001b\u001a\u00020\u0004H\u0007J\u000e\u0010\u001c\u001a\u00020\u00082\u0006\u0010\u001d\u001a\u00020\u0019J\u0018\u0010\u001e\u001a\u00020\u00082\u0006\u0010\u001f\u001a\u00020\u00042\u0006\u0010 \u001a\u00020\u0004H\u0007J\u0008\u0010!\u001a\u00020\u0008H\u0007J\u000e\u0010\"\u001a\u00020\u00082\u0006\u0010#\u001a\u00020\u0004J\u0008\u0010$\u001a\u00020\u0008H\u0007J\u0008\u0010%\u001a\u00020\u0008H\u0007J\u0008\u0010&\u001a\u00020\u0008H\u0007J\u0018\u0010\'\u001a\u00020\u00082\u0006\u0010(\u001a\u00020)2\u0006\u0010*\u001a\u00020)H\u0007J\u0010\u0010+\u001a\u00020\u00082\u0006\u0010,\u001a\u00020\u0019H\u0007J\u0008\u0010-\u001a\u00020\u0008H\u0007J\u0008\u0010.\u001a\u00020\u0008H\u0007J\u0008\u0010/\u001a\u00020\u0008H\u0007J\u0018\u00100\u001a\u00020\u00082\u0008\u0008\u0002\u00101\u001a\u00020\u00042\u0006\u00102\u001a\u00020\u0004J\u0008\u00103\u001a\u00020\u0008H\u0007J\u0010\u00104\u001a\u00020\u00042\u0006\u00105\u001a\u00020\u0019H\u0007J\u0016\u00106\u001a\u00020\u00082\u0006\u00107\u001a\u0002082\u0006\u00109\u001a\u00020\u0004J\u0010\u0010:\u001a\u00020\u00082\u0006\u0010;\u001a\u00020\u0004H\u0007J\u0008\u0010<\u001a\u00020\u0008H\u0007J\u0008\u0010=\u001a\u00020\u0008H\u0007J\u0010\u0010>\u001a\u00020\u00082\u0006\u0010?\u001a\u00020\u0004H\u0007J\u0018\u0010@\u001a\u00020\u00082\u0006\u0010A\u001a\u00020\u00042\u0006\u0010B\u001a\u00020\u0019H\u0007J)\u0010C\u001a\u00020\u00082\u0006\u0010D\u001a\u00020\u00192\u0008\u0010E\u001a\u0004\u0018\u00010F2\u0008\u0010G\u001a\u0004\u0018\u00010FH\u0007\u00a2\u0006\u0002\u0010HJ\u0008\u0010I\u001a\u00020\u0008H\u0007J\u0008\u0010J\u001a\u00020\u0008H\u0007J,\u0010K\u001a\u00020\u00082\u0006\u0010\r\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u00042\u0008\u0010\u0010\u001a\u0004\u0018\u00010\u00042\u0008\u0010\u000c\u001a\u0004\u0018\u00010\u0011H\u0007J\u0008\u0010L\u001a\u00020\u0008H\u0007J\u0008\u0010M\u001a\u00020\u0008H\u0007J\u0008\u0010N\u001a\u00020\u0008H\u0007J\u0010\u0010O\u001a\u00020\u00082\u0006\u0010P\u001a\u00020\u000eH\u0007J\u0006\u0010Q\u001a\u00020\u0008J\u0006\u0010R\u001a\u00020\u0008J\u0006\u0010S\u001a\u00020\u0008J\u0006\u0010T\u001a\u00020\u0008J\u0006\u0010U\u001a\u00020\u0008J\u0006\u0010V\u001a\u00020\u0008J\u0006\u0010W\u001a\u00020\u0008J\u0006\u0010X\u001a\u00020\u0008J\u0006\u0010Y\u001a\u00020\u0008J\u0006\u0010Z\u001a\u00020\u0008J\u0006\u0010[\u001a\u00020\u0008J!\u0010\\\u001a\u00020\u00082\u0012\u0010]\u001a\n\u0012\u0006\u0008\u0001\u0012\u00020\u00040^\"\u00020\u0004H\u0007\u00a2\u0006\u0002\u0010_J\u001a\u0010`\u001a\u00020\u00082\u0006\u0010a\u001a\u00020\u00042\u0008\u0010b\u001a\u0004\u0018\u00010\u0004H\u0007J\u0010\u0010c\u001a\u00020\u00082\u0006\u0010d\u001a\u00020\u0004H\u0007J\u0008\u0010e\u001a\u00020\u0008H\u0007J\u0010\u0010f\u001a\u00020\u00082\u0006\u0010g\u001a\u00020hH\u0007J\u0010\u0010i\u001a\u00020\u00082\u0006\u00101\u001a\u00020jH\u0007J(\u0010k\u001a\u00020\u00082\u0006\u00101\u001a\u00020l2\n\u0008\u0002\u0010m\u001a\u0004\u0018\u00010\u00042\n\u0008\u0002\u00109\u001a\u0004\u0018\u00010\u0004H\u0007J\u0008\u0010n\u001a\u00020\u0008H\u0007J\u0010\u0010o\u001a\u00020\u00082\u0006\u0010d\u001a\u00020\u0004H\u0007J\u000e\u0010p\u001a\u00020\u00082\u0006\u0010q\u001a\u00020\u0019J\u0010\u0010r\u001a\u00020\u00082\u0006\u0010m\u001a\u00020)H\u0007J\u0018\u0010s\u001a\u00020\u00082\u0006\u0010t\u001a\u00020\u000e2\u0006\u0010u\u001a\u00020\u000eH\u0007J\u0010\u0010v\u001a\u00020\u00082\u0006\u0010w\u001a\u00020\u0019H\u0007J\u0010\u0010x\u001a\u00020\u00082\u0006\u0010y\u001a\u00020\u0004H\u0007J,\u0010z\u001a\u00020\u00082\u0006\u0010\r\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u00042\u0008\u0010\u0010\u001a\u0004\u0018\u00010\u00042\u0008\u0010\u000c\u001a\u0004\u0018\u00010\u0011H\u0007J\u0008\u0010{\u001a\u00020\u0008H\u0007J\u0010\u0010|\u001a\u00020\u00082\u0006\u0010P\u001a\u00020\u000eH\u0007J\u0008\u0010}\u001a\u00020\u0008H\u0007J\u0008\u0010~\u001a\u00020\u0008H\u0007J\u0008\u0010\u007f\u001a\u00020\u0008H\u0007J\t\u0010\u0080\u0001\u001a\u00020\u0008H\u0007J\t\u0010\u0081\u0001\u001a\u00020\u0008H\u0007J\u0013\u0010\u0082\u0001\u001a\u00020\u00082\u0008\u0010\u0083\u0001\u001a\u00030\u0084\u0001H\u0007J\t\u0010\u0085\u0001\u001a\u00020\u0008H\u0007J\t\u0010\u0086\u0001\u001a\u00020\u0008H\u0007J\t\u0010\u0087\u0001\u001a\u00020\u0008H\u0007J\t\u0010\u0088\u0001\u001a\u00020\u0008H\u0007J\t\u0010\u0089\u0001\u001a\u00020\u0008H\u0007J\t\u0010\u008a\u0001\u001a\u00020\u0008H\u0007J\u0012\u0010\u008b\u0001\u001a\u00020\u00082\u0007\u0010\u008c\u0001\u001a\u00020\u0019H\u0007J\t\u0010\u008d\u0001\u001a\u00020\u0008H\u0007J\t\u0010\u008e\u0001\u001a\u00020\u0008H\u0007J\u0007\u0010\u008f\u0001\u001a\u00020\u0008J\u0007\u0010\u0090\u0001\u001a\u00020\u0008J\u0007\u0010\u0091\u0001\u001a\u00020\u0008J\t\u0010\u0092\u0001\u001a\u00020\u0008H\u0007J\t\u0010\u0093\u0001\u001a\u00020\u0008H\u0007J\u0007\u0010\u0094\u0001\u001a\u00020\u0008J\u0007\u0010\u0095\u0001\u001a\u00020\u0008J\u0007\u0010\u0096\u0001\u001a\u00020\u0008J\u0007\u0010\u0097\u0001\u001a\u00020\u0008J\t\u0010\u0098\u0001\u001a\u00020\u0008H\u0007J\t\u0010\u0099\u0001\u001a\u00020\u0008H\u0007J\t\u0010\u009a\u0001\u001a\u00020\u0008H\u0007J\t\u0010\u009b\u0001\u001a\u00020\u0008H\u0007J\t\u0010\u009c\u0001\u001a\u00020\u0008H\u0007J\u0007\u0010\u009d\u0001\u001a\u00020\u0008J\u0013\u0010\u009e\u0001\u001a\u00020\u00082\u0008\u0010\u009f\u0001\u001a\u00030\u00a0\u0001H\u0007J\t\u0010\u00a1\u0001\u001a\u00020\u0008H\u0007J\t\u0010\u00a2\u0001\u001a\u00020\u0008H\u0007J\u0007\u0010\u00a3\u0001\u001a\u00020\u0008J\u0012\u0010\u00a4\u0001\u001a\u00020\u00082\u0007\u0010\u008c\u0001\u001a\u00020\u0019H\u0007J\u0013\u0010\u00a5\u0001\u001a\u00020\u00082\n\u0008\u0002\u0010m\u001a\u0004\u0018\u00010\u0004J\t\u0010\u00a6\u0001\u001a\u00020\u0008H\u0007J$\u0010\u00a6\u0001\u001a\u00020\u00082\u0007\u0010\u00a7\u0001\u001a\u00020\u00192\u0007\u0010\u00a8\u0001\u001a\u00020\u00192\u0007\u0010\u00a9\u0001\u001a\u00020\u0019H\u0007J\t\u0010\u00aa\u0001\u001a\u00020\u0008H\u0007J\t\u0010\u00ab\u0001\u001a\u00020\u0008H\u0007J\u001a\u0010\u00ac\u0001\u001a\u00020\u00082\u0006\u0010?\u001a\u00020\u00042\u0007\u0010\u00ad\u0001\u001a\u00020\u0019H\u0007J\u0012\u0010\u00ae\u0001\u001a\u00020\u00082\u0007\u00101\u001a\u00030\u00af\u0001H\u0007J\u0012\u0010\u00b0\u0001\u001a\u00020\u00082\u0007\u00101\u001a\u00030\u00af\u0001H\u0007J\u0012\u0010\u00b1\u0001\u001a\u00020\u00082\u0007\u00101\u001a\u00030\u00af\u0001H\u0007J\t\u0010\u00b2\u0001\u001a\u00020\u0008H\u0007J\t\u0010\u00b3\u0001\u001a\u00020\u0008H\u0007J\u001b\u0010\u00b4\u0001\u001a\u00020\u00082\u0007\u00101\u001a\u00030\u00af\u00012\u0007\u0010\u00ad\u0001\u001a\u00020\u0019H\u0007J\t\u0010\u00b5\u0001\u001a\u00020\u0008H\u0007J\u0012\u0010\u00b6\u0001\u001a\u00020\u00082\u0007\u0010\u00ad\u0001\u001a\u00020\u0019H\u0007J\u0011\u0010\u00b7\u0001\u001a\u00020\u00082\u0006\u0010?\u001a\u00020\u0004H\u0007J\t\u0010\u00b8\u0001\u001a\u00020\u0008H\u0007J\u0007\u0010\u00b9\u0001\u001a\u00020\u0008J\u0007\u0010\u00ba\u0001\u001a\u00020\u0008J\t\u0010\u00bb\u0001\u001a\u00020\u0008H\u0007J\t\u0010\u00bc\u0001\u001a\u00020\u0008H\u0007J\t\u0010\u00bd\u0001\u001a\u00020\u0008H\u0007J\u0013\u0010\u00be\u0001\u001a\u00020\u00082\u0008\u0010\u009f\u0001\u001a\u00030\u00bf\u0001H\u0007J\t\u0010\u00c0\u0001\u001a\u00020\u0008H\u0007J\t\u0010\u00c1\u0001\u001a\u00020\u0008H\u0007J\t\u0010\u00c2\u0001\u001a\u00020\u0008H\u0007J\u0012\u0010\u00c3\u0001\u001a\u00020\u00082\u0007\u00101\u001a\u00030\u00c4\u0001H\u0007J\u0011\u0010\u00c5\u0001\u001a\u00020\u00082\u0008\u0010\u00c6\u0001\u001a\u00030\u00c4\u0001J\t\u0010\u00c7\u0001\u001a\u00020\u0008H\u0007J\t\u0010\u00c8\u0001\u001a\u00020\u0008H\u0007J\u0007\u0010\u00c9\u0001\u001a\u00020\u0008J\t\u0010\u00ca\u0001\u001a\u00020\u0008H\u0007J\t\u0010\u00cb\u0001\u001a\u00020\u0008H\u0007J\u0013\u0010\u00cc\u0001\u001a\u00020\u00082\u0008\u0010\u00cd\u0001\u001a\u00030\u00ce\u0001H\u0007J\t\u0010\u00cf\u0001\u001a\u00020\u0008H\u0007J\t\u0010\u00d0\u0001\u001a\u00020\u0008H\u0007J\t\u0010\u00d1\u0001\u001a\u00020\u0008H\u0007J\t\u0010\u00d2\u0001\u001a\u00020\u0008H\u0007J\t\u0010\u00d3\u0001\u001a\u00020\u0008H\u0007J\t\u0010\u00d4\u0001\u001a\u00020\u0008H\u0007J\t\u0010\u00d5\u0001\u001a\u00020\u0008H\u0007J\u0007\u0010\u00d6\u0001\u001a\u00020\u0008J\u0007\u0010\u00d7\u0001\u001a\u00020\u0008J\t\u0010\u00d8\u0001\u001a\u00020\u0008H\u0007J\u001b\u0010\u00d9\u0001\u001a\u00020\u00082\u0006\u0010m\u001a\u00020)2\u0008\u0010\u00da\u0001\u001a\u00030\u00db\u0001H\u0007J\u001b\u0010\u00dc\u0001\u001a\u00020\u00082\u0006\u0010m\u001a\u00020)2\u0008\u0010\u00da\u0001\u001a\u00030\u00db\u0001H\u0007J\u001b\u0010\u00dd\u0001\u001a\u00020\u00082\u0006\u0010m\u001a\u00020)2\u0008\u0010\u00da\u0001\u001a\u00030\u00db\u0001H\u0007J\u0013\u0010\u00de\u0001\u001a\u00020\u00082\u0008\u0010\u00da\u0001\u001a\u00030\u00db\u0001H\u0007J\u0007\u0010\u00df\u0001\u001a\u00020\u0008J\u0007\u0010\u00e0\u0001\u001a\u00020\u0008J\u0007\u0010\u00e1\u0001\u001a\u00020\u0008J\u0007\u0010\u00e2\u0001\u001a\u00020\u0008J\u0007\u0010\u00e3\u0001\u001a\u00020\u0008J\t\u0010\u00e4\u0001\u001a\u00020\u0008H\u0007J\t\u0010\u00e5\u0001\u001a\u00020\u0008H\u0007J\t\u0010\u00e6\u0001\u001a\u00020\u0008H\u0007J\t\u0010\u00e7\u0001\u001a\u00020\u0008H\u0007J\t\u0010\u00e8\u0001\u001a\u00020\u0008H\u0007J\t\u0010\u00e9\u0001\u001a\u00020\u0008H\u0007J\u0012\u0010\u00ea\u0001\u001a\u00020\u00082\u0007\u0010\u00eb\u0001\u001a\u00020\u0004H\u0007J\u0007\u0010\u00ec\u0001\u001a\u00020\u0008J\t\u0010\u00ed\u0001\u001a\u00020\u0008H\u0007J\t\u0010\u00ee\u0001\u001a\u00020\u0008H\u0007J\t\u0010\u00ef\u0001\u001a\u00020\u0008H\u0007J\u0007\u0010\u00f0\u0001\u001a\u00020\u0008J\u0012\u0010\u00f1\u0001\u001a\u00020\u00082\u0007\u0010\u00f2\u0001\u001a\u00020\u0004H\u0007J\u001c\u0010\u00f1\u0001\u001a\u00020\u00082\u0007\u0010\u00f2\u0001\u001a\u00020\u00042\u0008\u0010\u00f3\u0001\u001a\u00030\u00f4\u0001H\u0007J\u001c\u0010\u00f5\u0001\u001a\u00020\u00082\u0007\u0010a\u001a\u00030\u00f6\u00012\u0008\u0010\u00da\u0001\u001a\u00030\u00db\u0001H\u0007J\t\u0010\u00f7\u0001\u001a\u00020\u0008H\u0007J\u0012\u0010\u00f8\u0001\u001a\u00020\u00082\u0007\u0010\u00f9\u0001\u001a\u00020\u000eH\u0007J\t\u0010\u00fa\u0001\u001a\u00020\u0008H\u0007J\t\u0010\u00fb\u0001\u001a\u00020\u0008H\u0007J\u0011\u0010\u00fc\u0001\u001a\u00020\u00082\u0006\u00105\u001a\u00020\u0019H\u0007J\u0011\u0010\u00fd\u0001\u001a\u00020\u00082\u0006\u00105\u001a\u00020\u0019H\u0007J\u0011\u0010\u00fe\u0001\u001a\u00020\u00082\u0006\u00105\u001a\u00020\u0019H\u0007J\u001a\u0010\u00ff\u0001\u001a\u00020\u00082\u0006\u00105\u001a\u00020\u00192\u0007\u0010\u00eb\u0001\u001a\u00020\u0004H\u0007J\u0011\u0010\u0080\u0002\u001a\u00020\u00082\u0006\u00105\u001a\u00020\u0019H\u0007J\u001b\u0010\u0081\u0002\u001a\u00020\u00082\u0007\u0010\u0082\u0002\u001a\u00020\u00042\u0007\u0010\u0083\u0002\u001a\u00020\u0004H\u0007J\u001a\u0010\u0084\u0002\u001a\u00020\u00082\t\u0010\u0085\u0002\u001a\u0004\u0018\u00010\u0019H\u0007\u00a2\u0006\u0003\u0010\u0086\u0002J\u001a\u0010\u0087\u0002\u001a\u00020\u00082\t\u0010\u0085\u0002\u001a\u0004\u0018\u00010\u0019H\u0007\u00a2\u0006\u0003\u0010\u0086\u0002J\u001a\u0010\u0088\u0002\u001a\u00020\u00082\t\u0010\u0085\u0002\u001a\u0004\u0018\u00010\u0019H\u0007\u00a2\u0006\u0003\u0010\u0086\u0002J\t\u0010\u0089\u0002\u001a\u00020\u0008H\u0007J\t\u0010\u008a\u0002\u001a\u00020\u0008H\u0007J\t\u0010\u008b\u0002\u001a\u00020\u0008H\u0007J\u0012\u0010\u008c\u0002\u001a\u00020\u00082\u0007\u0010\u008d\u0002\u001a\u00020\u0004H\u0007J\t\u0010\u008e\u0002\u001a\u00020\u0008H\u0007J\t\u0010\u008f\u0002\u001a\u00020\u0008H\u0007J\u0012\u0010\u0090\u0002\u001a\u00020\u00082\u0007\u0010\u0091\u0002\u001a\u00020\u0019H\u0007J\u0012\u0010\u0092\u0002\u001a\u00020\u00082\u0007\u0010a\u001a\u00030\u0093\u0002H\u0007J\u0013\u0010\u0094\u0002\u001a\u00020\u00082\u0008\u0010\u0095\u0002\u001a\u00030\u0096\u0002H\u0007J\u001b\u0010\u0097\u0002\u001a\u00020\u00082\u0007\u0010\u0098\u0002\u001a\u00020\u00042\u0007\u0010\u0099\u0002\u001a\u00020\u0004H\u0007J\t\u0010\u009a\u0002\u001a\u00020\u0008H\u0007J\t\u0010\u009b\u0002\u001a\u00020\u0008H\u0007J\u0012\u0010\u009c\u0002\u001a\u00020\u00082\u0007\u0010\u00eb\u0001\u001a\u00020\u0004H\u0007J\u0012\u0010\u009d\u0002\u001a\u00020\u00082\u0007\u0010\u00eb\u0001\u001a\u00020\u0004H\u0007J\u0012\u0010\u009e\u0002\u001a\u00020\u00082\u0007\u0010\u00eb\u0001\u001a\u00020\u0004H\u0007J\u0011\u0010\u009f\u0002\u001a\u00020\u00082\u0006\u0010P\u001a\u00020\u000eH\u0007R\u0014\u0010\u0003\u001a\u00020\u0004X\u0086D\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0005\u0010\u0006\u00a8\u0006\u00a0\u0002"
    }
    d2 = {
        "Lco/uk/getmondo/api/model/tracking/Impression$Companion;",
        "",
        "()V",
        "EVENT_WAILIST_SHOW",
        "",
        "getEVENT_WAILIST_SHOW",
        "()Ljava/lang/String;",
        "addAttachmentTapped",
        "Lco/uk/getmondo/api/model/tracking/Impression;",
        "addNoteTapped",
        "advertisingIdObserved",
        "advertisingId",
        "apiError",
        "apiStatusCode",
        "",
        "path",
        "traceId",
        "Lco/uk/getmondo/api/model/ApiError;",
        "appBackground",
        "appForeground",
        "appInitialised",
        "fingerprintSupportType",
        "Lco/uk/getmondo/api/model/tracking/Impression$FingerprintSupportType;",
        "activeNotifications",
        "nfcEnabled",
        "",
        "appShortcutUsed",
        "shortcutType",
        "authenticateAddressChange",
        "isSuccess",
        "autoLogout",
        "logoutReason",
        "userId",
        "bypassWaitlist",
        "cardDetailsConfirmationTap",
        "selectedNameType",
        "cardReplacementAddressAddTap",
        "cardReplacementOrderedTap",
        "cardReplacementTap",
        "changeTransactionCategory",
        "oldCategory",
        "Lco/uk/getmondo/model/Category;",
        "newCategory",
        "contactsPermissionGranted",
        "permissionGranted",
        "createProfile",
        "defrostCardTapped",
        "dismissSearch",
        "exportDataTap",
        "from",
        "fileType",
        "freezeCardTapped",
        "getThreeDsDescription",
        "initialTopUp",
        "helpOutcome",
        "outcome",
        "Lco/uk/getmondo/api/model/tracking/Impression$HelpOutcome;",
        "topicId",
        "installReferral",
        "referrer",
        "inviteContactFromAllContacts",
        "inviteContactFromContactOnMonzo",
        "kycDocumentOkTap",
        "identityDocumentType",
        "kycVideoError",
        "reason",
        "memoryError",
        "kycVideoProcessed",
        "sampleError",
        "firstSampleDelta",
        "",
        "secondSampleDelta",
        "(ZLjava/lang/Long;Ljava/lang/Long;)Lco/uk/getmondo/api/model/tracking/Impression;",
        "kycVideoSelfieOkTap",
        "kycVideoSelfieRecording",
        "logOutError",
        "loginConfirm",
        "loginMailTap",
        "loginNoEmailTap",
        "loginShow",
        "page",
        "migrationAnnouncementBannerShow",
        "migrationAnnouncementDetailsShow",
        "migrationAnnouncementNotifyTap",
        "migrationContinueBannerShow",
        "migrationContinueBannerTap",
        "migrationIntroPackageShow",
        "migrationIntroStepsShow",
        "migrationInvitationBannerShow",
        "migrationInvitationDetailsShow",
        "migrationInvitationLearnMoreTap",
        "migrationStartTap",
        "multipleImpressions",
        "names",
        "",
        "([Ljava/lang/String;)Lco/uk/getmondo/api/model/tracking/Impression;",
        "notificationReceived",
        "type",
        "subType",
        "openMapForTransaction",
        "transactionId",
        "openSplitCost",
        "openedCommunity",
        "openedFrom",
        "Lco/uk/getmondo/api/model/tracking/Impression$OpenedCommunityFrom;",
        "openedFaqs",
        "Lco/uk/getmondo/api/model/tracking/Impression$OpenedFaqsFrom;",
        "openedIntercom",
        "Lco/uk/getmondo/api/model/tracking/Impression$IntercomFrom;",
        "category",
        "openedSettings",
        "openedTransaction",
        "optInToMarketingEmails",
        "optIn",
        "p2pCategorized",
        "p2pEnabled",
        "numberOfContactsOnMonzo",
        "numberOfContactsNotOnMonzo",
        "p2pPaymentRequest",
        "transferSuccessful",
        "playstoreRedirect",
        "redirectId",
        "reportedError",
        "requestContactsPermission",
        "requestMoneyTour",
        "selectPhotoTapped",
        "sendMoneyErrorNotEnabled",
        "settingsCloseAccount",
        "settingsEditProfile",
        "settingsFscsProtection",
        "settingsLimits",
        "verificationType",
        "Lco/uk/getmondo/api/model/VerificationType;",
        "settingsLogOut",
        "settingsOpenSource",
        "settingsPaymentLimits",
        "settingsPrivacyPolicy",
        "settingsShowAbout",
        "settingsTermsAndConditions",
        "shareGoldenTicket",
        "isFirstGoldenTicket",
        "shareScreen",
        "showAddContact",
        "showAddressChangeConfirm",
        "showAddressChangeList",
        "showAddressChangePostcode",
        "showAddressPicker",
        "showCardActivation",
        "showCardDetailsConfirmation",
        "showCardOrderPinEntry",
        "showCardOrderPinVerify",
        "showCardOrderPinVerifyFailed",
        "showCardReplacementActivation",
        "showCardReplacementAddressPicker",
        "showCardScreen",
        "showCardShipped",
        "showChecklist",
        "showContacts",
        "showCustomiseMonzoMeLink",
        "entryPoint",
        "Lco/uk/getmondo/api/model/tracking/Impression$CustomiseMonzoMeLinkFrom;",
        "showEddFeedItem",
        "showForceUpgrade",
        "showFscsProtection",
        "showGoldenTicket",
        "showHelpContent",
        "showIdentityVerification",
        "photoIdDone",
        "selfieDone",
        "kycRejected",
        "showInitialTopUp",
        "showInitialTopUpChoice",
        "showKYCDocumentScan",
        "isFallback",
        "showKYCOnboardingPage1",
        "Lco/uk/getmondo/api/model/tracking/Impression$KycFrom;",
        "showKYCOnboardingPage2",
        "showKYCOnboardingPage3",
        "showKYCPending",
        "showKYCRequest",
        "showKYCSubmitted",
        "showKYCVerified",
        "showKYCVideoSelfie",
        "showKycCountrySelection",
        "showManualAddress",
        "showMarketingOptIn",
        "showMonzoDocs",
        "showNews",
        "showP2pOnboardingScreen1",
        "showP2pOnboardingScreen2",
        "showPaymentInfo",
        "Lco/uk/getmondo/api/model/tracking/Impression$PaymentFlowFrom;",
        "showPhoneEntry",
        "showPhoneVerify",
        "showPinContactUs",
        "showPinDobConfirm",
        "Lco/uk/getmondo/api/model/tracking/Impression$PinFrom;",
        "showPinEntry",
        "pinFrom",
        "showPinNumberChanged",
        "showPinSmsConfirm",
        "showPrivacyPolicy",
        "showRequestMoney",
        "showRequestShare",
        "showSddUpgradeLearnMore",
        "sddUpgradeLevelType",
        "Lco/uk/getmondo/signup/identity_verification/sdd/SddUpgradeLevelType;",
        "showSddUpgradeLevel1",
        "showSddUpgradeLevel2",
        "showSddUpgradeLevel3",
        "showSearch",
        "showSettings",
        "showShareChooser",
        "showShareMonzoMeLink",
        "showSignupPending",
        "showSingupRejected",
        "showSpending",
        "showSpendingCategoryMerchants",
        "yearMonth",
        "Lorg/threeten/bp/YearMonth;",
        "showSpendingCategoryTransactions",
        "showSpendingMerchantTransactions",
        "showSpendingReport",
        "showTaxResidencyCountry",
        "showTaxResidencyNumber",
        "showTaxResidencySummary",
        "showTaxResidencyUs",
        "showTermsAndConditions",
        "showTflInfo",
        "showTopAccountScreen",
        "showTopScreen",
        "showTopScreenNonUk",
        "showTransactionCategory",
        "showUSDRestaurantInfo",
        "showWebView",
        "url",
        "showWelcomeToMonzo",
        "signUpAddressEditTap",
        "signUpDateOfBirthEditTap",
        "signUpNameEditTap",
        "signupCardOnItsWay",
        "singleImpression",
        "name",
        "data",
        "Lco/uk/getmondo/api/model/tracking/Data;",
        "spendingReportFeedback",
        "Lco/uk/getmondo/feed/SpendingReportFeedbackDialogFragment$FeedbackType;",
        "splitCostCustomiseTapped",
        "splitCostWithTapped",
        "numberPeople",
        "takePhotoTapped",
        "tapPinSmsSend",
        "threeDSecureComplete",
        "threeDSecureEnter",
        "threeDSecureFail",
        "threeDSecurePageView",
        "threeDSecureSuccess",
        "threeDSecureWebError",
        "redirectUrl",
        "errorType",
        "toggleMagStripe",
        "enabled",
        "(Ljava/lang/Boolean;)Lco/uk/getmondo/api/model/tracking/Impression;",
        "toggleNotifications",
        "togglePayments",
        "topUpBankTransferShareSheetShow",
        "topUpBankTransferShow",
        "topUpBankTransferTap",
        "topUpError",
        "description",
        "topUpNewCardShow",
        "topUpSavedCardShow",
        "topUpShow",
        "androidPayEnabled",
        "topUpSuccess",
        "Lco/uk/getmondo/api/model/tracking/Impression$TopUpSuccessType;",
        "topUpTap",
        "cameFrom",
        "Lco/uk/getmondo/api/model/tracking/Impression$TopUpTapFrom;",
        "trackShareSheetAppUsed",
        "analyticName",
        "appName",
        "waitList",
        "waitListFirstTime",
        "webViewClose",
        "webViewExternalLink",
        "webViewShare",
        "welcomeTour",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 99
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/d/b/i;)V
    .locals 0

    .prologue
    .line 99
    invoke-direct {p0}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;-><init>()V

    return-void
.end method

.method public static bridge synthetic a(Lco/uk/getmondo/api/model/tracking/Impression$Companion;Lco/uk/getmondo/api/model/tracking/Impression$IntercomFrom;Ljava/lang/String;Ljava/lang/String;ILjava/lang/Object;)Lco/uk/getmondo/api/model/tracking/Impression;
    .locals 3

    .prologue
    const/4 v1, 0x0

    and-int/lit8 v0, p4, 0x2

    if-eqz v0, :cond_1

    move-object v0, v1

    .line 240
    check-cast v0, Ljava/lang/String;

    :goto_0
    and-int/lit8 v2, p4, 0x4

    if-eqz v2, :cond_0

    check-cast v1, Ljava/lang/String;

    :goto_1
    invoke-virtual {p0, p1, v0, v1}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->a(Lco/uk/getmondo/api/model/tracking/Impression$IntercomFrom;Ljava/lang/String;Ljava/lang/String;)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    return-object v0

    :cond_0
    move-object v1, p3

    goto :goto_1

    :cond_1
    move-object v0, p2

    goto :goto_0
.end method

.method public static synthetic a(Lco/uk/getmondo/api/model/tracking/Impression$Companion;Ljava/lang/String;Ljava/lang/String;ILjava/lang/Object;)Lco/uk/getmondo/api/model/tracking/Impression;
    .locals 1

    .prologue
    and-int/lit8 v0, p3, 0x1

    if-eqz v0, :cond_0

    .line 848
    const-string p1, "your_spend"

    :cond_0
    invoke-virtual {p0, p1, p2}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->e(Ljava/lang/String;Ljava/lang/String;)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final A()Lco/uk/getmondo/api/model/tracking/Impression;
    .locals 1

    .prologue
    .line 305
    check-cast p0, Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    const-string v0, "signup.phone-entry.show"

    invoke-virtual {p0, v0}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->n(Ljava/lang/String;)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    return-object v0
.end method

.method public final B()Lco/uk/getmondo/api/model/tracking/Impression;
    .locals 1

    .prologue
    .line 309
    check-cast p0, Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    const-string v0, "signup.phone-verify.show"

    invoke-virtual {p0, v0}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->n(Ljava/lang/String;)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    return-object v0
.end method

.method public final C()Lco/uk/getmondo/api/model/tracking/Impression;
    .locals 1

    .prologue
    .line 313
    check-cast p0, Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    const-string v0, "signup.identity-verification.show"

    invoke-virtual {p0, v0}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->n(Ljava/lang/String;)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    return-object v0
.end method

.method public final D()Lco/uk/getmondo/api/model/tracking/Impression;
    .locals 1

    .prologue
    .line 317
    check-cast p0, Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    const-string v0, "signup.name-edit.tap"

    invoke-virtual {p0, v0}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->n(Ljava/lang/String;)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    return-object v0
.end method

.method public final E()Lco/uk/getmondo/api/model/tracking/Impression;
    .locals 1

    .prologue
    .line 321
    check-cast p0, Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    const-string v0, "signup.dob-edit.tap"

    invoke-virtual {p0, v0}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->n(Ljava/lang/String;)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    return-object v0
.end method

.method public final F()Lco/uk/getmondo/api/model/tracking/Impression;
    .locals 1

    .prologue
    .line 325
    check-cast p0, Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    const-string v0, "signup.address-edit.tap"

    invoke-virtual {p0, v0}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->n(Ljava/lang/String;)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    return-object v0
.end method

.method public final G()Lco/uk/getmondo/api/model/tracking/Impression;
    .locals 3

    .prologue
    .line 329
    check-cast p0, Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    const-string v0, "signup.initial-topup-choice.show"

    sget-object v1, Lco/uk/getmondo/api/model/tracking/Data;->Companion:Lco/uk/getmondo/api/model/tracking/Data$Companion;

    const/16 v2, 0x64

    invoke-virtual {v1, v2}, Lco/uk/getmondo/api/model/tracking/Data$Companion;->a(I)Lco/uk/getmondo/api/model/tracking/Data;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->a(Ljava/lang/String;Lco/uk/getmondo/api/model/tracking/Data;)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    return-object v0
.end method

.method public final H()Lco/uk/getmondo/api/model/tracking/Impression;
    .locals 3

    .prologue
    .line 333
    check-cast p0, Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    const-string v0, "signup.topup.show"

    sget-object v1, Lco/uk/getmondo/api/model/tracking/Data;->Companion:Lco/uk/getmondo/api/model/tracking/Data$Companion;

    const/16 v2, 0x64

    invoke-virtual {v1, v2}, Lco/uk/getmondo/api/model/tracking/Data$Companion;->a(I)Lco/uk/getmondo/api/model/tracking/Data;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->a(Ljava/lang/String;Lco/uk/getmondo/api/model/tracking/Data;)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    return-object v0
.end method

.method public final I()Lco/uk/getmondo/api/model/tracking/Impression;
    .locals 1

    .prologue
    .line 337
    check-cast p0, Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    const-string v0, "signup.card-on-the-way.show"

    invoke-virtual {p0, v0}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->n(Ljava/lang/String;)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    return-object v0
.end method

.method public final J()Lco/uk/getmondo/api/model/tracking/Impression;
    .locals 1

    .prologue
    .line 341
    check-cast p0, Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    const-string v0, "signup.card-activation.show"

    invoke-virtual {p0, v0}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->n(Ljava/lang/String;)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    return-object v0
.end method

.method public final K()Lco/uk/getmondo/api/model/tracking/Impression;
    .locals 1

    .prologue
    .line 373
    check-cast p0, Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    const-string v0, "kyc-selfie-rec.show"

    invoke-virtual {p0, v0}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->n(Ljava/lang/String;)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    return-object v0
.end method

.method public final L()Lco/uk/getmondo/api/model/tracking/Impression;
    .locals 1

    .prologue
    .line 377
    check-cast p0, Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    const-string v0, "kyc-selfie-ok.tap"

    invoke-virtual {p0, v0}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->n(Ljava/lang/String;)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    return-object v0
.end method

.method public final M()Lco/uk/getmondo/api/model/tracking/Impression;
    .locals 1

    .prologue
    .line 385
    check-cast p0, Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    const-string v0, "kyc-verified.show"

    invoke-virtual {p0, v0}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->n(Ljava/lang/String;)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    return-object v0
.end method

.method public final N()Lco/uk/getmondo/api/model/tracking/Impression;
    .locals 1

    .prologue
    .line 398
    check-cast p0, Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    const-string v0, "replacement-card.tap"

    invoke-virtual {p0, v0}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->n(Ljava/lang/String;)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    return-object v0
.end method

.method public final O()Lco/uk/getmondo/api/model/tracking/Impression;
    .locals 1

    .prologue
    .line 402
    check-cast p0, Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    const-string v0, "card-replacement.address-picker.show"

    invoke-virtual {p0, v0}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->n(Ljava/lang/String;)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    return-object v0
.end method

.method public final P()Lco/uk/getmondo/api/model/tracking/Impression;
    .locals 1

    .prologue
    .line 406
    check-cast p0, Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    const-string v0, "card-replacement.address-add.tap"

    invoke-virtual {p0, v0}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->n(Ljava/lang/String;)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    return-object v0
.end method

.method public final Q()Lco/uk/getmondo/api/model/tracking/Impression;
    .locals 1

    .prologue
    .line 410
    check-cast p0, Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    const-string v0, "replacement-order.tap"

    invoke-virtual {p0, v0}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->n(Ljava/lang/String;)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    return-object v0
.end method

.method public final R()Lco/uk/getmondo/api/model/tracking/Impression;
    .locals 1

    .prologue
    .line 414
    check-cast p0, Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    const-string v0, "new-card-activation.show"

    invoke-virtual {p0, v0}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->n(Ljava/lang/String;)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    return-object v0
.end method

.method public final S()Lco/uk/getmondo/api/model/tracking/Impression;
    .locals 1

    .prologue
    .line 418
    check-cast p0, Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    const-string v0, "your-spending.show"

    invoke-virtual {p0, v0}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->n(Ljava/lang/String;)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    return-object v0
.end method

.method public final T()Lco/uk/getmondo/api/model/tracking/Impression;
    .locals 1

    .prologue
    .line 455
    check-cast p0, Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    const-string v0, "transaction.split-cost.tap"

    invoke-virtual {p0, v0}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->n(Ljava/lang/String;)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    return-object v0
.end method

.method public final U()Lco/uk/getmondo/api/model/tracking/Impression;
    .locals 3

    .prologue
    .line 463
    check-cast p0, Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    const-string v0, "split-cost.tap"

    sget-object v1, Lco/uk/getmondo/api/model/tracking/Data;->Companion:Lco/uk/getmondo/api/model/tracking/Data$Companion;

    const-string v2, "custom"

    invoke-virtual {v1, v2}, Lco/uk/getmondo/api/model/tracking/Data$Companion;->m(Ljava/lang/String;)Lco/uk/getmondo/api/model/tracking/Data;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->a(Ljava/lang/String;Lco/uk/getmondo/api/model/tracking/Data;)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    return-object v0
.end method

.method public final V()Lco/uk/getmondo/api/model/tracking/Impression;
    .locals 1

    .prologue
    .line 467
    check-cast p0, Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    const-string v0, "settings.open"

    invoke-virtual {p0, v0}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->n(Ljava/lang/String;)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    return-object v0
.end method

.method public final W()Lco/uk/getmondo/api/model/tracking/Impression;
    .locals 1

    .prologue
    .line 475
    check-cast p0, Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    const-string v0, "p2p.disabled.show"

    invoke-virtual {p0, v0}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->n(Ljava/lang/String;)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    return-object v0
.end method

.method public final X()Lco/uk/getmondo/api/model/tracking/Impression;
    .locals 1

    .prologue
    .line 479
    check-cast p0, Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    const-string v0, "p2p.explained.show"

    invoke-virtual {p0, v0}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->n(Ljava/lang/String;)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    return-object v0
.end method

.method public final Y()Lco/uk/getmondo/api/model/tracking/Impression;
    .locals 1

    .prologue
    .line 483
    check-cast p0, Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    const-string v0, "p2p.contacts-priming.request"

    invoke-virtual {p0, v0}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->n(Ljava/lang/String;)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    return-object v0
.end method

.method public final Z()Lco/uk/getmondo/api/model/tracking/Impression;
    .locals 3

    .prologue
    .line 495
    check-cast p0, Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    const-string v0, "p2p.invite.send"

    sget-object v1, Lco/uk/getmondo/api/model/tracking/Data;->Companion:Lco/uk/getmondo/api/model/tracking/Data$Companion;

    const-string v2, "contacts_on_monzo"

    invoke-virtual {v1, v2}, Lco/uk/getmondo/api/model/tracking/Data$Companion;->g(Ljava/lang/String;)Lco/uk/getmondo/api/model/tracking/Data;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->a(Ljava/lang/String;Lco/uk/getmondo/api/model/tracking/Data;)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    return-object v0
.end method

.method public final a(I)Lco/uk/getmondo/api/model/tracking/Impression;
    .locals 3

    .prologue
    .line 128
    check-cast p0, Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    const-string v0, "login.show"

    sget-object v1, Lco/uk/getmondo/api/model/tracking/Data;->Companion:Lco/uk/getmondo/api/model/tracking/Data$Companion;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Lco/uk/getmondo/api/model/tracking/Data$Companion;->a(Ljava/lang/Integer;)Lco/uk/getmondo/api/model/tracking/Data;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->a(Ljava/lang/String;Lco/uk/getmondo/api/model/tracking/Data;)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    return-object v0
.end method

.method public final a(II)Lco/uk/getmondo/api/model/tracking/Impression;
    .locals 2

    .prologue
    .line 491
    check-cast p0, Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    const-string v0, "p2p.enabled.show"

    sget-object v1, Lco/uk/getmondo/api/model/tracking/Data;->Companion:Lco/uk/getmondo/api/model/tracking/Data$Companion;

    invoke-virtual {v1, p1, p2}, Lco/uk/getmondo/api/model/tracking/Data$Companion;->a(II)Lco/uk/getmondo/api/model/tracking/Data;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->a(Ljava/lang/String;Lco/uk/getmondo/api/model/tracking/Data;)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    return-object v0
.end method

.method public final a(ILjava/lang/String;Ljava/lang/String;Lco/uk/getmondo/api/model/b;)Lco/uk/getmondo/api/model/tracking/Impression;
    .locals 2

    .prologue
    const-string v0, "path"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 527
    check-cast p0, Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    const-string v0, "error"

    sget-object v1, Lco/uk/getmondo/api/model/tracking/Data;->Companion:Lco/uk/getmondo/api/model/tracking/Data$Companion;

    invoke-virtual {v1, p1, p2, p3, p4}, Lco/uk/getmondo/api/model/tracking/Data$Companion;->a(ILjava/lang/String;Ljava/lang/String;Lco/uk/getmondo/api/model/b;)Lco/uk/getmondo/api/model/tracking/Data;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->a(Ljava/lang/String;Lco/uk/getmondo/api/model/tracking/Data;)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lco/uk/getmondo/api/model/VerificationType;)Lco/uk/getmondo/api/model/tracking/Impression;
    .locals 3

    .prologue
    const-string v0, "verificationType"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 640
    check-cast p0, Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    const-string v0, "limits.show"

    sget-object v1, Lco/uk/getmondo/api/model/tracking/Data;->Companion:Lco/uk/getmondo/api/model/tracking/Data$Companion;

    invoke-virtual {p1}, Lco/uk/getmondo/api/model/VerificationType;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lco/uk/getmondo/api/model/tracking/Data$Companion;->l(Ljava/lang/String;)Lco/uk/getmondo/api/model/tracking/Data;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->a(Ljava/lang/String;Lco/uk/getmondo/api/model/tracking/Data;)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lco/uk/getmondo/api/model/tracking/Impression$CustomiseMonzoMeLinkFrom;)Lco/uk/getmondo/api/model/tracking/Impression;
    .locals 2

    .prologue
    const-string v0, "entryPoint"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 555
    check-cast p0, Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    const-string v0, "request-custom.show"

    sget-object v1, Lco/uk/getmondo/api/model/tracking/Data;->Companion:Lco/uk/getmondo/api/model/tracking/Data$Companion;

    invoke-virtual {v1, p1}, Lco/uk/getmondo/api/model/tracking/Data$Companion;->a(Lco/uk/getmondo/api/model/tracking/Impression$CustomiseMonzoMeLinkFrom;)Lco/uk/getmondo/api/model/tracking/Data;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->a(Ljava/lang/String;Lco/uk/getmondo/api/model/tracking/Data;)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lco/uk/getmondo/api/model/tracking/Impression$FingerprintSupportType;IZ)Lco/uk/getmondo/api/model/tracking/Impression;
    .locals 63

    .prologue
    const-string v1, "fingerprintSupportType"

    move-object/from16 v0, p1

    invoke-static {v0, v1}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 103
    new-instance v1, Lco/uk/getmondo/api/model/tracking/Data;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    const/16 v17, 0x0

    const/16 v18, 0x0

    const/16 v19, 0x0

    const/16 v20, 0x0

    const/16 v21, 0x0

    const/16 v22, 0x0

    const/16 v23, 0x0

    const/16 v24, 0x0

    const/16 v25, 0x0

    const/16 v26, 0x0

    invoke-virtual/range {p1 .. p1}, Lco/uk/getmondo/api/model/tracking/Impression$FingerprintSupportType;->a()Ljava/lang/String;

    move-result-object v27

    invoke-static/range {p2 .. p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v28

    invoke-static/range {p3 .. p3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v29

    const/16 v30, 0x0

    const/16 v31, 0x0

    const/16 v32, 0x0

    const/16 v33, 0x0

    const/16 v34, 0x0

    const/16 v35, 0x0

    const/16 v36, 0x0

    const/16 v37, 0x0

    const/16 v38, 0x0

    const/16 v39, 0x0

    const/16 v40, 0x0

    const/16 v41, 0x0

    const/16 v42, 0x0

    const/16 v43, 0x0

    const/16 v44, 0x0

    const/16 v45, 0x0

    const/16 v46, 0x0

    const/16 v47, 0x0

    const/16 v48, 0x0

    const/16 v49, 0x0

    const/16 v50, 0x0

    const/16 v51, 0x0

    const/16 v52, 0x0

    const/16 v53, 0x0

    const/16 v54, 0x0

    const/16 v55, 0x0

    const/16 v56, 0x0

    const/16 v57, 0x0

    const/16 v58, 0x0

    const/16 v59, 0x0

    const v60, -0xe000001

    const v61, 0x3ffffff

    const/16 v62, 0x0

    invoke-direct/range {v1 .. v62}, Lco/uk/getmondo/api/model/tracking/Data;-><init>(Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Boolean;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IILkotlin/d/b/i;)V

    .line 104
    check-cast p0, Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    const-string v2, "app.init"

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v1}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->a(Ljava/lang/String;Lco/uk/getmondo/api/model/tracking/Data;)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v1

    return-object v1
.end method

.method public final a(Lco/uk/getmondo/api/model/tracking/Impression$HelpOutcome;Ljava/lang/String;)Lco/uk/getmondo/api/model/tracking/Impression;
    .locals 65

    .prologue
    const-string v2, "outcome"

    move-object/from16 v0, p1

    invoke-static {v0, v2}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v2, "topicId"

    move-object/from16 v0, p2

    invoke-static {v0, v2}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 817
    check-cast p0, Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    const-string v64, "help-v2.outcome"

    new-instance v2, Lco/uk/getmondo/api/model/tracking/Data;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    const/16 v17, 0x0

    const/16 v18, 0x0

    const/16 v19, 0x0

    const/16 v20, 0x0

    const/16 v21, 0x0

    const/16 v22, 0x0

    const/16 v23, 0x0

    const/16 v24, 0x0

    const/16 v25, 0x0

    const/16 v26, 0x0

    const/16 v27, 0x0

    const/16 v28, 0x0

    const/16 v29, 0x0

    const/16 v30, 0x0

    const/16 v31, 0x0

    const/16 v32, 0x0

    const/16 v33, 0x0

    const/16 v34, 0x0

    const/16 v35, 0x0

    const/16 v36, 0x0

    const/16 v37, 0x0

    const/16 v38, 0x0

    const/16 v39, 0x0

    const/16 v40, 0x0

    const/16 v41, 0x0

    const/16 v42, 0x0

    const/16 v43, 0x0

    const/16 v44, 0x0

    const/16 v45, 0x0

    const/16 v46, 0x0

    const/16 v47, 0x0

    const/16 v48, 0x0

    const/16 v49, 0x0

    const/16 v50, 0x0

    const/16 v51, 0x0

    const/16 v52, 0x0

    const/16 v53, 0x0

    const/16 v54, 0x0

    const/16 v55, 0x0

    const/16 v56, 0x0

    invoke-virtual/range {p1 .. p1}, Lco/uk/getmondo/api/model/tracking/Impression$HelpOutcome;->a()Ljava/lang/String;

    move-result-object v58

    const/16 v59, 0x0

    const/16 v60, 0x0

    const/16 v61, -0x1

    const v62, 0x33fffff

    const/16 v63, 0x0

    move-object/from16 v57, p2

    invoke-direct/range {v2 .. v63}, Lco/uk/getmondo/api/model/tracking/Data;-><init>(Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Boolean;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IILkotlin/d/b/i;)V

    move-object/from16 v0, p0

    move-object/from16 v1, v64

    invoke-virtual {v0, v1, v2}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->a(Ljava/lang/String;Lco/uk/getmondo/api/model/tracking/Data;)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v2

    return-object v2
.end method

.method public final a(Lco/uk/getmondo/api/model/tracking/Impression$IntercomFrom;Ljava/lang/String;Ljava/lang/String;)Lco/uk/getmondo/api/model/tracking/Impression;
    .locals 65

    .prologue
    const-string v2, "from"

    move-object/from16 v0, p1

    invoke-static {v0, v2}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 241
    check-cast p0, Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    const-string v64, "intercom.open"

    new-instance v2, Lco/uk/getmondo/api/model/tracking/Data;

    const/4 v3, 0x0

    invoke-virtual/range {p1 .. p1}, Lco/uk/getmondo/api/model/tracking/Impression$IntercomFrom;->a()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    const/16 v17, 0x0

    const/16 v18, 0x0

    const/16 v20, 0x0

    const/16 v21, 0x0

    const/16 v22, 0x0

    const/16 v23, 0x0

    const/16 v24, 0x0

    const/16 v25, 0x0

    const/16 v26, 0x0

    const/16 v27, 0x0

    const/16 v28, 0x0

    const/16 v29, 0x0

    const/16 v30, 0x0

    const/16 v31, 0x0

    const/16 v32, 0x0

    const/16 v33, 0x0

    const/16 v34, 0x0

    const/16 v35, 0x0

    const/16 v36, 0x0

    const/16 v37, 0x0

    const/16 v38, 0x0

    const/16 v39, 0x0

    const/16 v40, 0x0

    const/16 v41, 0x0

    const/16 v42, 0x0

    const/16 v43, 0x0

    const/16 v44, 0x0

    const/16 v45, 0x0

    const/16 v46, 0x0

    const/16 v47, 0x0

    const/16 v48, 0x0

    const/16 v49, 0x0

    const/16 v50, 0x0

    const/16 v51, 0x0

    const/16 v52, 0x0

    const/16 v53, 0x0

    const/16 v54, 0x0

    const/16 v55, 0x0

    const/16 v56, 0x0

    const/16 v58, 0x0

    const/16 v59, 0x0

    const/16 v60, 0x0

    const v61, -0x10003

    const v62, 0x3bfffff

    const/16 v63, 0x0

    move-object/from16 v19, p2

    move-object/from16 v57, p3

    invoke-direct/range {v2 .. v63}, Lco/uk/getmondo/api/model/tracking/Data;-><init>(Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Boolean;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IILkotlin/d/b/i;)V

    move-object/from16 v0, p0

    move-object/from16 v1, v64

    invoke-virtual {v0, v1, v2}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->a(Ljava/lang/String;Lco/uk/getmondo/api/model/tracking/Data;)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v2

    return-object v2
.end method

.method public final a(Lco/uk/getmondo/api/model/tracking/Impression$KycFrom;)Lco/uk/getmondo/api/model/tracking/Impression;
    .locals 3

    .prologue
    const-string v0, "from"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 345
    check-cast p0, Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    const-string v0, "kyc-tour-1.show"

    sget-object v1, Lco/uk/getmondo/api/model/tracking/Data;->Companion:Lco/uk/getmondo/api/model/tracking/Data$Companion;

    invoke-virtual {p1}, Lco/uk/getmondo/api/model/tracking/Impression$KycFrom;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lco/uk/getmondo/api/model/tracking/Data$Companion;->i(Ljava/lang/String;)Lco/uk/getmondo/api/model/tracking/Data;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->a(Ljava/lang/String;Lco/uk/getmondo/api/model/tracking/Data;)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lco/uk/getmondo/api/model/tracking/Impression$KycFrom;Z)Lco/uk/getmondo/api/model/tracking/Impression;
    .locals 65

    .prologue
    const-string v2, "from"

    move-object/from16 v0, p1

    invoke-static {v0, v2}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 381
    check-cast p0, Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    const-string v64, "kyc-done.show"

    new-instance v2, Lco/uk/getmondo/api/model/tracking/Data;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    const/16 v17, 0x0

    const/16 v18, 0x0

    const/16 v19, 0x0

    const/16 v20, 0x0

    const/16 v21, 0x0

    const/16 v22, 0x0

    const/16 v23, 0x0

    invoke-virtual/range {p1 .. p1}, Lco/uk/getmondo/api/model/tracking/Impression$KycFrom;->b()Ljava/lang/String;

    move-result-object v24

    const/16 v25, 0x0

    const/16 v26, 0x0

    const/16 v27, 0x0

    const/16 v28, 0x0

    const/16 v29, 0x0

    const/16 v30, 0x0

    const/16 v31, 0x0

    const/16 v32, 0x0

    const/16 v33, 0x0

    const/16 v34, 0x0

    const/16 v35, 0x0

    const/16 v36, 0x0

    const/16 v37, 0x0

    const/16 v38, 0x0

    const/16 v39, 0x0

    const/16 v40, 0x0

    const/16 v41, 0x0

    const/16 v42, 0x0

    const/16 v43, 0x0

    const/16 v44, 0x0

    const/16 v45, 0x0

    const/16 v46, 0x0

    const/16 v47, 0x0

    const/16 v48, 0x0

    const/16 v49, 0x0

    const/16 v50, 0x0

    const/16 v51, 0x0

    const/16 v52, 0x0

    invoke-static/range {p2 .. p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v53

    const/16 v54, 0x0

    const/16 v55, 0x0

    const/16 v56, 0x0

    const/16 v57, 0x0

    const/16 v58, 0x0

    const/16 v59, 0x0

    const/16 v60, 0x0

    const v61, -0x200001

    const v62, 0x3fbffff

    const/16 v63, 0x0

    invoke-direct/range {v2 .. v63}, Lco/uk/getmondo/api/model/tracking/Data;-><init>(Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Boolean;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IILkotlin/d/b/i;)V

    move-object/from16 v0, p0

    move-object/from16 v1, v64

    invoke-virtual {v0, v1, v2}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->a(Ljava/lang/String;Lco/uk/getmondo/api/model/tracking/Data;)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v2

    return-object v2
.end method

.method public final a(Lco/uk/getmondo/api/model/tracking/Impression$OpenedCommunityFrom;)Lco/uk/getmondo/api/model/tracking/Impression;
    .locals 3

    .prologue
    const-string v0, "openedFrom"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 471
    check-cast p0, Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    const-string v0, "community.open"

    sget-object v1, Lco/uk/getmondo/api/model/tracking/Data;->Companion:Lco/uk/getmondo/api/model/tracking/Data$Companion;

    invoke-virtual {p1}, Lco/uk/getmondo/api/model/tracking/Impression$OpenedCommunityFrom;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lco/uk/getmondo/api/model/tracking/Data$Companion;->b(Ljava/lang/String;)Lco/uk/getmondo/api/model/tracking/Data;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->a(Ljava/lang/String;Lco/uk/getmondo/api/model/tracking/Data;)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lco/uk/getmondo/api/model/tracking/Impression$PaymentFlowFrom;)Lco/uk/getmondo/api/model/tracking/Impression;
    .locals 3

    .prologue
    const-string v0, "entryPoint"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 503
    check-cast p0, Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    const-string v0, "p2p.form.show"

    sget-object v1, Lco/uk/getmondo/api/model/tracking/Data;->Companion:Lco/uk/getmondo/api/model/tracking/Data$Companion;

    invoke-virtual {p1}, Lco/uk/getmondo/api/model/tracking/Impression$PaymentFlowFrom;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lco/uk/getmondo/api/model/tracking/Data$Companion;->i(Ljava/lang/String;)Lco/uk/getmondo/api/model/tracking/Data;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->a(Ljava/lang/String;Lco/uk/getmondo/api/model/tracking/Data;)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lco/uk/getmondo/api/model/tracking/Impression$PinFrom;)Lco/uk/getmondo/api/model/tracking/Impression;
    .locals 3

    .prologue
    const-string v0, "from"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 575
    check-cast p0, Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    const-string v0, "pin-dob-confirm.show"

    sget-object v1, Lco/uk/getmondo/api/model/tracking/Data;->Companion:Lco/uk/getmondo/api/model/tracking/Data$Companion;

    invoke-virtual {p1}, Lco/uk/getmondo/api/model/tracking/Impression$PinFrom;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lco/uk/getmondo/api/model/tracking/Data$Companion;->i(Ljava/lang/String;)Lco/uk/getmondo/api/model/tracking/Data;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->a(Ljava/lang/String;Lco/uk/getmondo/api/model/tracking/Data;)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lco/uk/getmondo/api/model/tracking/Impression$TopUpSuccessType;)Lco/uk/getmondo/api/model/tracking/Impression;
    .locals 3

    .prologue
    const-string v0, "type"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 212
    check-cast p0, Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    const-string v0, "topup_success"

    sget-object v1, Lco/uk/getmondo/api/model/tracking/Data;->Companion:Lco/uk/getmondo/api/model/tracking/Data$Companion;

    invoke-virtual {p1}, Lco/uk/getmondo/api/model/tracking/Impression$TopUpSuccessType;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lco/uk/getmondo/api/model/tracking/Data$Companion;->g(Ljava/lang/String;)Lco/uk/getmondo/api/model/tracking/Data;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->a(Ljava/lang/String;Lco/uk/getmondo/api/model/tracking/Data;)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lco/uk/getmondo/api/model/tracking/Impression$TopUpTapFrom;)Lco/uk/getmondo/api/model/tracking/Impression;
    .locals 3

    .prologue
    const-string v0, "cameFrom"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 208
    check-cast p0, Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    const-string v0, "topup.tap"

    sget-object v1, Lco/uk/getmondo/api/model/tracking/Data;->Companion:Lco/uk/getmondo/api/model/tracking/Data$Companion;

    invoke-virtual {p1}, Lco/uk/getmondo/api/model/tracking/Impression$TopUpTapFrom;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lco/uk/getmondo/api/model/tracking/Data$Companion;->f(Ljava/lang/String;)Lco/uk/getmondo/api/model/tracking/Data;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->a(Ljava/lang/String;Lco/uk/getmondo/api/model/tracking/Data;)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lco/uk/getmondo/d/h;)Lco/uk/getmondo/api/model/tracking/Impression;
    .locals 2

    .prologue
    const-string v0, "category"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 511
    check-cast p0, Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    const-string v0, "p2p.categorise"

    sget-object v1, Lco/uk/getmondo/api/model/tracking/Data;->Companion:Lco/uk/getmondo/api/model/tracking/Data$Companion;

    invoke-virtual {v1, p1}, Lco/uk/getmondo/api/model/tracking/Data$Companion;->a(Lco/uk/getmondo/d/h;)Lco/uk/getmondo/api/model/tracking/Data;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->a(Ljava/lang/String;Lco/uk/getmondo/api/model/tracking/Data;)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lco/uk/getmondo/d/h;Lco/uk/getmondo/d/h;)Lco/uk/getmondo/api/model/tracking/Impression;
    .locals 5

    .prologue
    const-string v0, "oldCategory"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "newCategory"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 442
    check-cast p0, Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    const-string v0, "transaction.category.save"

    .line 443
    sget-object v1, Lco/uk/getmondo/api/model/tracking/Data;->Companion:Lco/uk/getmondo/api/model/tracking/Data$Companion;

    invoke-virtual {p1}, Lco/uk/getmondo/d/h;->f()Ljava/lang/String;

    move-result-object v2

    const-string v3, "oldCategory.apiValue"

    invoke-static {v2, v3}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p2}, Lco/uk/getmondo/d/h;->f()Ljava/lang/String;

    move-result-object v3

    const-string v4, "newCategory.apiValue"

    invoke-static {v3, v4}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v1, v2, v3}, Lco/uk/getmondo/api/model/tracking/Data$Companion;->a(Ljava/lang/String;Ljava/lang/String;)Lco/uk/getmondo/api/model/tracking/Data;

    move-result-object v1

    .line 442
    invoke-virtual {p0, v0, v1}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->a(Ljava/lang/String;Lco/uk/getmondo/api/model/tracking/Data;)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lco/uk/getmondo/d/h;Lorg/threeten/bp/YearMonth;)Lco/uk/getmondo/api/model/tracking/Impression;
    .locals 2

    .prologue
    const-string v0, "category"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "yearMonth"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 422
    check-cast p0, Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    const-string v0, "spend-category.show"

    sget-object v1, Lco/uk/getmondo/api/model/tracking/Data;->Companion:Lco/uk/getmondo/api/model/tracking/Data$Companion;

    invoke-virtual {v1, p1, p2}, Lco/uk/getmondo/api/model/tracking/Data$Companion;->a(Lco/uk/getmondo/d/h;Lorg/threeten/bp/YearMonth;)Lco/uk/getmondo/api/model/tracking/Data;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->a(Ljava/lang/String;Lco/uk/getmondo/api/model/tracking/Data;)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lco/uk/getmondo/feed/SpendingReportFeedbackDialogFragment$a;Lorg/threeten/bp/YearMonth;)Lco/uk/getmondo/api/model/tracking/Impression;
    .locals 63

    .prologue
    const-string v1, "type"

    move-object/from16 v0, p1

    invoke-static {v0, v1}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "yearMonth"

    move-object/from16 v0, p2

    invoke-static {v0, v1}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 696
    new-instance v1, Lco/uk/getmondo/api/model/tracking/Data;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    const/16 v17, 0x0

    const/16 v18, 0x0

    const/16 v19, 0x0

    const/16 v20, 0x0

    const/16 v21, 0x0

    const/16 v22, 0x0

    const/16 v23, 0x0

    const/16 v24, 0x0

    const/16 v25, 0x0

    const/16 v26, 0x0

    const/16 v27, 0x0

    const/16 v28, 0x0

    const/16 v29, 0x0

    const/16 v30, 0x0

    const/16 v31, 0x0

    const/16 v32, 0x0

    const/16 v33, 0x0

    const/16 v34, 0x0

    const/16 v35, 0x0

    const/16 v36, 0x0

    const/16 v37, 0x0

    const/16 v38, 0x0

    const/16 v39, 0x0

    const/16 v40, 0x0

    const/16 v41, 0x0

    const/16 v42, 0x0

    const/16 v43, 0x0

    const/16 v44, 0x0

    const/16 v45, 0x0

    const/16 v46, 0x0

    const/16 v47, 0x0

    const/16 v48, 0x0

    invoke-virtual/range {p1 .. p1}, Lco/uk/getmondo/feed/SpendingReportFeedbackDialogFragment$a;->a()Ljava/lang/String;

    move-result-object v49

    invoke-virtual/range {p2 .. p2}, Lorg/threeten/bp/YearMonth;->toString()Ljava/lang/String;

    move-result-object v50

    const/16 v51, 0x0

    const/16 v52, 0x0

    const/16 v53, 0x0

    const/16 v54, 0x0

    const/16 v55, 0x0

    const/16 v56, 0x0

    const/16 v57, 0x0

    const/16 v58, 0x0

    const/16 v59, 0x0

    const/16 v60, -0x1

    const v61, 0x3fe7fff

    const/16 v62, 0x0

    invoke-direct/range {v1 .. v62}, Lco/uk/getmondo/api/model/tracking/Data;-><init>(Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Boolean;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IILkotlin/d/b/i;)V

    .line 697
    check-cast p0, Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    const-string v2, "spending-report.feedback"

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v1}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->a(Ljava/lang/String;Lco/uk/getmondo/api/model/tracking/Data;)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v1

    return-object v1
.end method

.method public final a(Lco/uk/getmondo/signup/identity_verification/sdd/j;)Lco/uk/getmondo/api/model/tracking/Impression;
    .locals 3

    .prologue
    const-string v0, "sddUpgradeLevelType"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 737
    check-cast p0, Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    const-string v0, "kyc-june26-explained"

    sget-object v1, Lco/uk/getmondo/api/model/tracking/Data;->Companion:Lco/uk/getmondo/api/model/tracking/Data$Companion;

    invoke-virtual {p1}, Lco/uk/getmondo/signup/identity_verification/sdd/j;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lco/uk/getmondo/api/model/tracking/Data$Companion;->i(Ljava/lang/String;)Lco/uk/getmondo/api/model/tracking/Data;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->a(Ljava/lang/String;Lco/uk/getmondo/api/model/tracking/Data;)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Boolean;)Lco/uk/getmondo/api/model/tracking/Impression;
    .locals 2

    .prologue
    .line 648
    check-cast p0, Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    const-string v0, "profile-magstripe-atm.toggle"

    sget-object v1, Lco/uk/getmondo/api/model/tracking/Data;->Companion:Lco/uk/getmondo/api/model/tracking/Data$Companion;

    invoke-virtual {v1, p1}, Lco/uk/getmondo/api/model/tracking/Data$Companion;->a(Ljava/lang/Boolean;)Lco/uk/getmondo/api/model/tracking/Data;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->a(Ljava/lang/String;Lco/uk/getmondo/api/model/tracking/Data;)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;)Lco/uk/getmondo/api/model/tracking/Impression;
    .locals 2

    .prologue
    const-string v0, "url"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 168
    check-cast p0, Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    const-string v0, "wl.webview.show"

    sget-object v1, Lco/uk/getmondo/api/model/tracking/Data;->Companion:Lco/uk/getmondo/api/model/tracking/Data$Companion;

    invoke-virtual {v1, p1}, Lco/uk/getmondo/api/model/tracking/Data$Companion;->a(Ljava/lang/String;)Lco/uk/getmondo/api/model/tracking/Data;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->a(Ljava/lang/String;Lco/uk/getmondo/api/model/tracking/Data;)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;Lco/uk/getmondo/api/model/tracking/Data;)Lco/uk/getmondo/api/model/tracking/Impression;
    .locals 3

    .prologue
    const/4 v2, 0x0

    const-string v0, "name"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "data"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 583
    new-instance v0, Lco/uk/getmondo/api/model/tracking/Impression;

    const/4 v1, 0x1

    invoke-direct {v0, v2, v1, v2}, Lco/uk/getmondo/api/model/tracking/Impression;-><init>(Ljava/util/List;ILkotlin/d/b/i;)V

    .line 584
    new-instance v1, Lco/uk/getmondo/api/model/tracking/ImpressionEvent;

    invoke-direct {v1, p1, p2}, Lco/uk/getmondo/api/model/tracking/ImpressionEvent;-><init>(Ljava/lang/String;Lco/uk/getmondo/api/model/tracking/Data;)V

    .line 585
    invoke-virtual {v0, v1}, Lco/uk/getmondo/api/model/tracking/Impression;->a(Lco/uk/getmondo/api/model/tracking/ImpressionEvent;)V

    .line 586
    return-object v0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)Lco/uk/getmondo/api/model/tracking/Impression;
    .locals 2

    .prologue
    const-string v0, "logoutReason"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "userId"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 535
    check-cast p0, Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    const-string v0, "logout.auto"

    sget-object v1, Lco/uk/getmondo/api/model/tracking/Data;->Companion:Lco/uk/getmondo/api/model/tracking/Data$Companion;

    invoke-virtual {v1, p1, p2}, Lco/uk/getmondo/api/model/tracking/Data$Companion;->b(Ljava/lang/String;Ljava/lang/String;)Lco/uk/getmondo/api/model/tracking/Data;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->a(Ljava/lang/String;Lco/uk/getmondo/api/model/tracking/Data;)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;Z)Lco/uk/getmondo/api/model/tracking/Impression;
    .locals 65

    .prologue
    const-string v2, "identityDocumentType"

    move-object/from16 v0, p1

    invoke-static {v0, v2}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 361
    check-cast p0, Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    const-string v64, "kyc-document-scan.show"

    new-instance v2, Lco/uk/getmondo/api/model/tracking/Data;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/16 v16, 0x0

    const/16 v17, 0x0

    const/16 v18, 0x0

    const/16 v19, 0x0

    const/16 v20, 0x0

    const/16 v21, 0x0

    const/16 v22, 0x0

    const/16 v23, 0x0

    const/16 v24, 0x0

    const/16 v25, 0x0

    const/16 v26, 0x0

    const/16 v27, 0x0

    const/16 v28, 0x0

    const/16 v29, 0x0

    const/16 v30, 0x0

    const/16 v31, 0x0

    const/16 v32, 0x0

    const/16 v33, 0x0

    const/16 v34, 0x0

    const/16 v35, 0x0

    const/16 v36, 0x0

    const/16 v37, 0x0

    const/16 v38, 0x0

    const/16 v39, 0x0

    const/16 v40, 0x0

    const/16 v41, 0x0

    const/16 v42, 0x0

    const/16 v43, 0x0

    const/16 v44, 0x0

    const/16 v45, 0x0

    const/16 v46, 0x0

    const/16 v47, 0x0

    const/16 v48, 0x0

    const/16 v49, 0x0

    const/16 v50, 0x0

    const/16 v51, 0x0

    const/16 v52, 0x0

    invoke-static/range {p2 .. p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v53

    const/16 v54, 0x0

    const/16 v55, 0x0

    const/16 v56, 0x0

    const/16 v57, 0x0

    const/16 v58, 0x0

    const/16 v59, 0x0

    const/16 v60, 0x0

    const/16 v61, -0x1001

    const v62, 0x3fbffff

    const/16 v63, 0x0

    move-object/from16 v15, p1

    invoke-direct/range {v2 .. v63}, Lco/uk/getmondo/api/model/tracking/Data;-><init>(Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Boolean;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IILkotlin/d/b/i;)V

    move-object/from16 v0, p0

    move-object/from16 v1, v64

    invoke-virtual {v0, v1, v2}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->a(Ljava/lang/String;Lco/uk/getmondo/api/model/tracking/Data;)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v2

    return-object v2
.end method

.method public final a(Lorg/threeten/bp/YearMonth;)Lco/uk/getmondo/api/model/tracking/Impression;
    .locals 65

    .prologue
    const-string v2, "yearMonth"

    move-object/from16 v0, p1

    invoke-static {v0, v2}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 701
    check-cast p0, Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    const-string v64, "spending-report.show"

    new-instance v2, Lco/uk/getmondo/api/model/tracking/Data;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    const/16 v17, 0x0

    const/16 v18, 0x0

    const/16 v19, 0x0

    const/16 v20, 0x0

    const/16 v21, 0x0

    const/16 v22, 0x0

    const/16 v23, 0x0

    const/16 v24, 0x0

    const/16 v25, 0x0

    const/16 v26, 0x0

    const/16 v27, 0x0

    const/16 v28, 0x0

    const/16 v29, 0x0

    const/16 v30, 0x0

    const/16 v31, 0x0

    const/16 v32, 0x0

    const/16 v33, 0x0

    const/16 v34, 0x0

    const/16 v35, 0x0

    const/16 v36, 0x0

    const/16 v37, 0x0

    const/16 v38, 0x0

    const/16 v39, 0x0

    const/16 v40, 0x0

    const/16 v41, 0x0

    const/16 v42, 0x0

    const/16 v43, 0x0

    const/16 v44, 0x0

    const/16 v45, 0x0

    const/16 v46, 0x0

    const/16 v47, 0x0

    const/16 v48, 0x0

    const/16 v49, 0x0

    const/16 v50, 0x0

    invoke-virtual/range {p1 .. p1}, Lorg/threeten/bp/YearMonth;->toString()Ljava/lang/String;

    move-result-object v51

    const/16 v52, 0x0

    const/16 v53, 0x0

    const/16 v54, 0x0

    const/16 v55, 0x0

    const/16 v56, 0x0

    const/16 v57, 0x0

    const/16 v58, 0x0

    const/16 v59, 0x0

    const/16 v60, 0x0

    const/16 v61, -0x1

    const v62, 0x3feffff

    const/16 v63, 0x0

    invoke-direct/range {v2 .. v63}, Lco/uk/getmondo/api/model/tracking/Data;-><init>(Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Boolean;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IILkotlin/d/b/i;)V

    move-object/from16 v0, p0

    move-object/from16 v1, v64

    invoke-virtual {v0, v1, v2}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->a(Ljava/lang/String;Lco/uk/getmondo/api/model/tracking/Data;)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v2

    return-object v2
.end method

.method public final a(Z)Lco/uk/getmondo/api/model/tracking/Impression;
    .locals 65

    .prologue
    .line 184
    check-cast p0, Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    const-string v64, "topup.show"

    new-instance v2, Lco/uk/getmondo/api/model/tracking/Data;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    const/16 v17, 0x0

    const/16 v18, 0x0

    const/16 v19, 0x0

    const/16 v20, 0x0

    const/16 v21, 0x0

    const/16 v22, 0x0

    const/16 v23, 0x0

    const/16 v24, 0x0

    const/16 v25, 0x0

    const/16 v26, 0x0

    const/16 v27, 0x0

    const/16 v28, 0x0

    const/16 v29, 0x0

    const/16 v30, 0x0

    const/16 v31, 0x0

    const/16 v32, 0x0

    const/16 v33, 0x0

    const/16 v34, 0x0

    const/16 v35, 0x0

    const/16 v36, 0x0

    const/16 v37, 0x0

    const/16 v38, 0x0

    const/16 v39, 0x0

    const/16 v40, 0x0

    const/16 v41, 0x0

    const/16 v42, 0x0

    const/16 v43, 0x0

    const/16 v44, 0x0

    const/16 v45, 0x0

    const/16 v46, 0x0

    const/16 v47, 0x0

    const/16 v48, 0x0

    const/16 v49, 0x0

    const/16 v50, 0x0

    const/16 v51, 0x0

    invoke-static/range {p1 .. p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v52

    const/16 v53, 0x0

    const/16 v54, 0x0

    const/16 v55, 0x0

    const/16 v56, 0x0

    const/16 v57, 0x0

    const/16 v58, 0x0

    const/16 v59, 0x0

    const/16 v60, 0x0

    const/16 v61, -0x1

    const v62, 0x3fdffff

    const/16 v63, 0x0

    invoke-direct/range {v2 .. v63}, Lco/uk/getmondo/api/model/tracking/Data;-><init>(Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Boolean;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IILkotlin/d/b/i;)V

    move-object/from16 v0, p0

    move-object/from16 v1, v64

    invoke-virtual {v0, v1, v2}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->a(Ljava/lang/String;Lco/uk/getmondo/api/model/tracking/Data;)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v2

    return-object v2
.end method

.method public final a(ZLjava/lang/Long;Ljava/lang/Long;)Lco/uk/getmondo/api/model/tracking/Impression;
    .locals 2

    .prologue
    .line 543
    check-cast p0, Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    const-string v0, "kyc-video-processed"

    sget-object v1, Lco/uk/getmondo/api/model/tracking/Data;->Companion:Lco/uk/getmondo/api/model/tracking/Data$Companion;

    invoke-virtual {v1, p1, p2, p3}, Lco/uk/getmondo/api/model/tracking/Data$Companion;->a(ZLjava/lang/Long;Ljava/lang/Long;)Lco/uk/getmondo/api/model/tracking/Data;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->a(Ljava/lang/String;Lco/uk/getmondo/api/model/tracking/Data;)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    return-object v0
.end method

.method public final a(ZLjava/lang/String;)Lco/uk/getmondo/api/model/tracking/Impression;
    .locals 63

    .prologue
    const-string v1, "url"

    move-object/from16 v0, p2

    invoke-static {v0, v1}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 224
    new-instance v1, Lco/uk/getmondo/api/model/tracking/Data;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    move-object/from16 v7, p0

    check-cast v7, Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    move/from16 v0, p1

    invoke-virtual {v7, v0}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->g(Z)Ljava/lang/String;

    move-result-object v11

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    const/16 v17, 0x0

    const/16 v18, 0x0

    const/16 v19, 0x0

    const/16 v20, 0x0

    const/16 v21, 0x0

    const/16 v22, 0x0

    const/16 v23, 0x0

    const/16 v24, 0x0

    const/16 v25, 0x0

    const/16 v26, 0x0

    const/16 v27, 0x0

    const/16 v28, 0x0

    const/16 v29, 0x0

    const/16 v30, 0x0

    const/16 v31, 0x0

    const/16 v32, 0x0

    const/16 v33, 0x0

    const/16 v34, 0x0

    const/16 v35, 0x0

    const/16 v36, 0x0

    const/16 v37, 0x0

    const/16 v38, 0x0

    const/16 v39, 0x0

    const/16 v40, 0x0

    const/16 v41, 0x0

    const/16 v42, 0x0

    const/16 v43, 0x0

    const/16 v44, 0x0

    const/16 v45, 0x0

    const/16 v46, 0x0

    const/16 v47, 0x0

    const/16 v48, 0x0

    const/16 v49, 0x0

    const/16 v50, 0x0

    const/16 v51, 0x0

    const/16 v52, 0x0

    const/16 v53, 0x0

    const/16 v54, 0x0

    const/16 v55, 0x0

    const/16 v56, 0x0

    const/16 v57, 0x0

    const/16 v58, 0x0

    const/16 v59, 0x0

    const/16 v60, -0x221

    const v61, 0x3ffffff

    const/16 v62, 0x0

    move-object/from16 v7, p2

    invoke-direct/range {v1 .. v62}, Lco/uk/getmondo/api/model/tracking/Data;-><init>(Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Boolean;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IILkotlin/d/b/i;)V

    .line 225
    check-cast p0, Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    const-string v2, "topup-3dsecure.pageview"

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v1}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->a(Ljava/lang/String;Lco/uk/getmondo/api/model/tracking/Data;)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v1

    return-object v1
.end method

.method public final a(ZZZ)Lco/uk/getmondo/api/model/tracking/Impression;
    .locals 2

    .prologue
    .line 357
    check-cast p0, Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    const-string v0, "kyc-acc-verification.show"

    sget-object v1, Lco/uk/getmondo/api/model/tracking/Data;->Companion:Lco/uk/getmondo/api/model/tracking/Data$Companion;

    invoke-virtual {v1, p1, p2, p3}, Lco/uk/getmondo/api/model/tracking/Data$Companion;->a(ZZZ)Lco/uk/getmondo/api/model/tracking/Data;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->a(Ljava/lang/String;Lco/uk/getmondo/api/model/tracking/Data;)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    return-object v0
.end method

.method public final varargs a([Ljava/lang/String;)Lco/uk/getmondo/api/model/tracking/Impression;
    .locals 6

    .prologue
    const/4 v5, 0x0

    const-string v0, "names"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 596
    new-instance v1, Lco/uk/getmondo/api/model/tracking/Impression;

    const/4 v0, 0x1

    invoke-direct {v1, v5, v0, v5}, Lco/uk/getmondo/api/model/tracking/Impression;-><init>(Ljava/util/List;ILkotlin/d/b/i;)V

    .line 597
    const/4 v0, 0x0

    :goto_0
    array-length v2, p1

    if-ge v0, v2, :cond_0

    aget-object v2, p1, v0

    .line 598
    new-instance v3, Lco/uk/getmondo/api/model/tracking/ImpressionEvent;

    const/4 v4, 0x2

    invoke-direct {v3, v2, v5, v4, v5}, Lco/uk/getmondo/api/model/tracking/ImpressionEvent;-><init>(Ljava/lang/String;Lco/uk/getmondo/api/model/tracking/Data;ILkotlin/d/b/i;)V

    invoke-virtual {v1, v3}, Lco/uk/getmondo/api/model/tracking/Impression;->a(Lco/uk/getmondo/api/model/tracking/ImpressionEvent;)V

    .line 597
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 600
    :cond_0
    return-object v1
.end method

.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 100
    invoke-static {}, Lco/uk/getmondo/api/model/tracking/Impression;->a()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final aA()Lco/uk/getmondo/api/model/tracking/Impression;
    .locals 1

    .prologue
    .line 729
    check-cast p0, Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    const-string v0, "kyc-june26-warning.show"

    invoke-virtual {p0, v0}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->n(Ljava/lang/String;)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    return-object v0
.end method

.method public final aB()Lco/uk/getmondo/api/model/tracking/Impression;
    .locals 1

    .prologue
    .line 733
    check-cast p0, Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    const-string v0, "kyc-june26-blocked.show"

    invoke-virtual {p0, v0}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->n(Ljava/lang/String;)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    return-object v0
.end method

.method public final aC()Lco/uk/getmondo/api/model/tracking/Impression;
    .locals 1

    .prologue
    .line 741
    check-cast p0, Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    const-string v0, "contacts.show"

    invoke-virtual {p0, v0}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->n(Ljava/lang/String;)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    return-object v0
.end method

.method public final aD()Lco/uk/getmondo/api/model/tracking/Impression;
    .locals 1

    .prologue
    .line 749
    check-cast p0, Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    const-string v0, "gatca-one-last-thing.show"

    invoke-virtual {p0, v0}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->n(Ljava/lang/String;)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    return-object v0
.end method

.method public final aE()Lco/uk/getmondo/api/model/tracking/Impression;
    .locals 1

    .prologue
    .line 753
    check-cast p0, Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    const-string v0, "gatca-us.show"

    invoke-virtual {p0, v0}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->n(Ljava/lang/String;)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    return-object v0
.end method

.method public final aF()Lco/uk/getmondo/api/model/tracking/Impression;
    .locals 1

    .prologue
    .line 757
    check-cast p0, Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    const-string v0, "gatca-country-picker.show"

    invoke-virtual {p0, v0}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->n(Ljava/lang/String;)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    return-object v0
.end method

.method public final aG()Lco/uk/getmondo/api/model/tracking/Impression;
    .locals 1

    .prologue
    .line 761
    check-cast p0, Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    const-string v0, "gatca-country-form.show"

    invoke-virtual {p0, v0}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->n(Ljava/lang/String;)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    return-object v0
.end method

.method public final aH()Lco/uk/getmondo/api/model/tracking/Impression;
    .locals 1

    .prologue
    .line 765
    check-cast p0, Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    const-string v0, "marketing-opt-in.show"

    invoke-virtual {p0, v0}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->n(Ljava/lang/String;)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    return-object v0
.end method

.method public final aI()Lco/uk/getmondo/api/model/tracking/Impression;
    .locals 1

    .prologue
    .line 773
    check-cast p0, Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    const-string v0, "monzo-docs.show"

    invoke-virtual {p0, v0}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->n(Ljava/lang/String;)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    return-object v0
.end method

.method public final aJ()Lco/uk/getmondo/api/model/tracking/Impression;
    .locals 1

    .prologue
    .line 777
    check-cast p0, Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    const-string v0, "signup-terms-conditions.tap"

    invoke-virtual {p0, v0}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->n(Ljava/lang/String;)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    return-object v0
.end method

.method public final aK()Lco/uk/getmondo/api/model/tracking/Impression;
    .locals 1

    .prologue
    .line 781
    check-cast p0, Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    const-string v0, "signup-privacy-policy.tap"

    invoke-virtual {p0, v0}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->n(Ljava/lang/String;)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    return-object v0
.end method

.method public final aL()Lco/uk/getmondo/api/model/tracking/Impression;
    .locals 1

    .prologue
    .line 785
    check-cast p0, Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    const-string v0, "signup-fscs.tap"

    invoke-virtual {p0, v0}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->n(Ljava/lang/String;)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    return-object v0
.end method

.method public final aM()Lco/uk/getmondo/api/model/tracking/Impression;
    .locals 1

    .prologue
    .line 789
    check-cast p0, Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    const-string v0, "feeditem-current-account-welcome.show"

    invoke-virtual {p0, v0}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->n(Ljava/lang/String;)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    return-object v0
.end method

.method public final aN()Lco/uk/getmondo/api/model/tracking/Impression;
    .locals 1

    .prologue
    .line 793
    check-cast p0, Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    const-string v0, "address-change-postcode.show"

    invoke-virtual {p0, v0}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->n(Ljava/lang/String;)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    return-object v0
.end method

.method public final aO()Lco/uk/getmondo/api/model/tracking/Impression;
    .locals 1

    .prologue
    .line 797
    check-cast p0, Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    const-string v0, "address-change-addresslist.show"

    invoke-virtual {p0, v0}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->n(Ljava/lang/String;)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    return-object v0
.end method

.method public final aP()Lco/uk/getmondo/api/model/tracking/Impression;
    .locals 1

    .prologue
    .line 801
    check-cast p0, Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    const-string v0, "address-change-confirm.show"

    invoke-virtual {p0, v0}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->n(Ljava/lang/String;)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    return-object v0
.end method

.method public final aQ()Lco/uk/getmondo/api/model/tracking/Impression;
    .locals 1

    .prologue
    .line 821
    check-cast p0, Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    const-string v0, "signup-card-details-confirmation.show"

    invoke-virtual {p0, v0}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->n(Ljava/lang/String;)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    return-object v0
.end method

.method public final aR()Lco/uk/getmondo/api/model/tracking/Impression;
    .locals 1

    .prologue
    .line 829
    check-cast p0, Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    const-string v0, "signup.card-on-the-way.show"

    invoke-virtual {p0, v0}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->n(Ljava/lang/String;)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    return-object v0
.end method

.method public final aS()Lco/uk/getmondo/api/model/tracking/Impression;
    .locals 1

    .prologue
    .line 833
    check-cast p0, Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    const-string v0, "signup-card-order-pin-entry.show"

    invoke-virtual {p0, v0}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->n(Ljava/lang/String;)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    return-object v0
.end method

.method public final aT()Lco/uk/getmondo/api/model/tracking/Impression;
    .locals 1

    .prologue
    .line 837
    check-cast p0, Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    const-string v0, "signup-card-order-pin-verify.show"

    invoke-virtual {p0, v0}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->n(Ljava/lang/String;)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    return-object v0
.end method

.method public final aU()Lco/uk/getmondo/api/model/tracking/Impression;
    .locals 1

    .prologue
    .line 841
    check-cast p0, Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    const-string v0, "signup-card-order-pin-verify-incorrect.show"

    invoke-virtual {p0, v0}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->n(Ljava/lang/String;)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    return-object v0
.end method

.method public final aV()Lco/uk/getmondo/api/model/tracking/Impression;
    .locals 1

    .prologue
    .line 844
    check-cast p0, Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    const-string v0, "signup-pending.show"

    invoke-virtual {p0, v0}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->n(Ljava/lang/String;)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    return-object v0
.end method

.method public final aW()Lco/uk/getmondo/api/model/tracking/Impression;
    .locals 1

    .prologue
    .line 846
    check-cast p0, Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    const-string v0, "signup-rejected.show"

    invoke-virtual {p0, v0}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->n(Ljava/lang/String;)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    return-object v0
.end method

.method public final aX()Lco/uk/getmondo/api/model/tracking/Impression;
    .locals 1

    .prologue
    .line 852
    check-cast p0, Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    const-string v0, "migration-announcement-banner.show"

    invoke-virtual {p0, v0}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->n(Ljava/lang/String;)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    return-object v0
.end method

.method public final aY()Lco/uk/getmondo/api/model/tracking/Impression;
    .locals 1

    .prologue
    .line 854
    check-cast p0, Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    const-string v0, "migration-announcement-details.show"

    invoke-virtual {p0, v0}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->n(Ljava/lang/String;)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    return-object v0
.end method

.method public final aZ()Lco/uk/getmondo/api/model/tracking/Impression;
    .locals 1

    .prologue
    .line 856
    check-cast p0, Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    const-string v0, "migration-announcement-notify.tap"

    invoke-virtual {p0, v0}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->n(Ljava/lang/String;)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    return-object v0
.end method

.method public final aa()Lco/uk/getmondo/api/model/tracking/Impression;
    .locals 3

    .prologue
    .line 499
    check-cast p0, Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    const-string v0, "p2p.invite.send"

    sget-object v1, Lco/uk/getmondo/api/model/tracking/Data;->Companion:Lco/uk/getmondo/api/model/tracking/Data$Companion;

    const-string v2, "all_contacts"

    invoke-virtual {v1, v2}, Lco/uk/getmondo/api/model/tracking/Data$Companion;->g(Ljava/lang/String;)Lco/uk/getmondo/api/model/tracking/Data;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->a(Ljava/lang/String;Lco/uk/getmondo/api/model/tracking/Data;)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    return-object v0
.end method

.method public final ab()Lco/uk/getmondo/api/model/tracking/Impression;
    .locals 1

    .prologue
    .line 515
    check-cast p0, Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    const-string v0, "p2p.add-contact.show"

    invoke-virtual {p0, v0}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->n(Ljava/lang/String;)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    return-object v0
.end method

.method public final ac()Lco/uk/getmondo/api/model/tracking/Impression;
    .locals 1

    .prologue
    .line 559
    check-cast p0, Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    const-string v0, "pin-contactus.show"

    invoke-virtual {p0, v0}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->n(Ljava/lang/String;)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    return-object v0
.end method

.method public final ad()Lco/uk/getmondo/api/model/tracking/Impression;
    .locals 1

    .prologue
    .line 563
    check-cast p0, Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    const-string v0, "pin-number-changed.show"

    invoke-virtual {p0, v0}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->n(Ljava/lang/String;)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    return-object v0
.end method

.method public final ae()Lco/uk/getmondo/api/model/tracking/Impression;
    .locals 1

    .prologue
    .line 567
    check-cast p0, Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    const-string v0, "pin-sms-send.tap"

    invoke-virtual {p0, v0}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->n(Ljava/lang/String;)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    return-object v0
.end method

.method public final af()Lco/uk/getmondo/api/model/tracking/Impression;
    .locals 1

    .prologue
    .line 571
    check-cast p0, Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    const-string v0, "pin-sms-confirm.show"

    invoke-virtual {p0, v0}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->n(Ljava/lang/String;)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    return-object v0
.end method

.method public final ag()Lco/uk/getmondo/api/model/tracking/Impression;
    .locals 1

    .prologue
    .line 579
    check-cast p0, Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    const-string v0, "force-upgrade.show"

    invoke-virtual {p0, v0}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->n(Ljava/lang/String;)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    return-object v0
.end method

.method public final ah()Lco/uk/getmondo/api/model/tracking/Impression;
    .locals 1

    .prologue
    .line 604
    check-cast p0, Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    const-string v0, "profile-edit.tap"

    invoke-virtual {p0, v0}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->n(Ljava/lang/String;)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    return-object v0
.end method

.method public final ai()Lco/uk/getmondo/api/model/tracking/Impression;
    .locals 1

    .prologue
    .line 608
    check-cast p0, Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    const-string v0, "profile-about.tap"

    invoke-virtual {p0, v0}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->n(Ljava/lang/String;)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    return-object v0
.end method

.method public final aj()Lco/uk/getmondo/api/model/tracking/Impression;
    .locals 1

    .prologue
    .line 612
    check-cast p0, Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    const-string v0, "profile-terms-conditions.tap"

    invoke-virtual {p0, v0}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->n(Ljava/lang/String;)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    return-object v0
.end method

.method public final ak()Lco/uk/getmondo/api/model/tracking/Impression;
    .locals 1

    .prologue
    .line 616
    check-cast p0, Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    const-string v0, "profile-privacy-policy.tap"

    invoke-virtual {p0, v0}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->n(Ljava/lang/String;)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    return-object v0
.end method

.method public final al()Lco/uk/getmondo/api/model/tracking/Impression;
    .locals 1

    .prologue
    .line 620
    check-cast p0, Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    const-string v0, "profile-fscs.tap"

    invoke-virtual {p0, v0}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->n(Ljava/lang/String;)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    return-object v0
.end method

.method public final am()Lco/uk/getmondo/api/model/tracking/Impression;
    .locals 1

    .prologue
    .line 624
    check-cast p0, Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    const-string v0, "profile-open-source.tap"

    invoke-virtual {p0, v0}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->n(Ljava/lang/String;)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    return-object v0
.end method

.method public final an()Lco/uk/getmondo/api/model/tracking/Impression;
    .locals 1

    .prologue
    .line 628
    check-cast p0, Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    const-string v0, "logout.tap"

    invoke-virtual {p0, v0}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->n(Ljava/lang/String;)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    return-object v0
.end method

.method public final ao()Lco/uk/getmondo/api/model/tracking/Impression;
    .locals 1

    .prologue
    .line 632
    check-cast p0, Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    const-string v0, "close-account.tap"

    invoke-virtual {p0, v0}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->n(Ljava/lang/String;)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    return-object v0
.end method

.method public final ap()Lco/uk/getmondo/api/model/tracking/Impression;
    .locals 1

    .prologue
    .line 636
    check-cast p0, Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    const-string v0, "settings.show"

    invoke-virtual {p0, v0}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->n(Ljava/lang/String;)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    return-object v0
.end method

.method public final aq()Lco/uk/getmondo/api/model/tracking/Impression;
    .locals 1

    .prologue
    .line 644
    check-cast p0, Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    const-string v0, "payment-limits.show"

    invoke-virtual {p0, v0}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->n(Ljava/lang/String;)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    return-object v0
.end method

.method public final ar()Lco/uk/getmondo/api/model/tracking/Impression;
    .locals 1

    .prologue
    .line 660
    check-cast p0, Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    const-string v0, "p2p.sdd-error.show"

    invoke-virtual {p0, v0}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->n(Ljava/lang/String;)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    return-object v0
.end method

.method public final as()Lco/uk/getmondo/api/model/tracking/Impression;
    .locals 1

    .prologue
    .line 672
    check-cast p0, Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    const-string v0, "request-custom-share-sheet.show"

    invoke-virtual {p0, v0}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->n(Ljava/lang/String;)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    return-object v0
.end method

.method public final at()Lco/uk/getmondo/api/model/tracking/Impression;
    .locals 1

    .prologue
    .line 676
    check-cast p0, Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    const-string v0, "request.show"

    invoke-virtual {p0, v0}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->n(Ljava/lang/String;)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    return-object v0
.end method

.method public final au()Lco/uk/getmondo/api/model/tracking/Impression;
    .locals 1

    .prologue
    .line 680
    check-cast p0, Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    const-string v0, "request.share-sheet.show"

    invoke-virtual {p0, v0}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->n(Ljava/lang/String;)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    return-object v0
.end method

.method public final av()Lco/uk/getmondo/api/model/tracking/Impression;
    .locals 1

    .prologue
    .line 709
    check-cast p0, Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    const-string v0, "feeditem-edd.show"

    invoke-virtual {p0, v0}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->n(Ljava/lang/String;)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    return-object v0
.end method

.method public final aw()Lco/uk/getmondo/api/model/tracking/Impression;
    .locals 1

    .prologue
    .line 713
    check-cast p0, Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    const-string v0, "search.show"

    invoke-virtual {p0, v0}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->n(Ljava/lang/String;)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    return-object v0
.end method

.method public final ax()Lco/uk/getmondo/api/model/tracking/Impression;
    .locals 1

    .prologue
    .line 717
    check-cast p0, Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    const-string v0, "search.dismiss"

    invoke-virtual {p0, v0}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->n(Ljava/lang/String;)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    return-object v0
.end method

.method public final ay()Lco/uk/getmondo/api/model/tracking/Impression;
    .locals 1

    .prologue
    .line 721
    check-cast p0, Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    const-string v0, "news.show"

    invoke-virtual {p0, v0}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->n(Ljava/lang/String;)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    return-object v0
.end method

.method public final az()Lco/uk/getmondo/api/model/tracking/Impression;
    .locals 1

    .prologue
    .line 725
    check-cast p0, Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    const-string v0, "kyc-june26.show"

    invoke-virtual {p0, v0}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->n(Ljava/lang/String;)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    return-object v0
.end method

.method public final b()Lco/uk/getmondo/api/model/tracking/Impression;
    .locals 1

    .prologue
    .line 108
    check-cast p0, Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    const-string v0, "app.foreground"

    invoke-virtual {p0, v0}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->n(Ljava/lang/String;)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    return-object v0
.end method

.method public final b(I)Lco/uk/getmondo/api/model/tracking/Impression;
    .locals 3

    .prologue
    .line 144
    check-cast p0, Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    const-string v0, "welcome-tour.show"

    sget-object v1, Lco/uk/getmondo/api/model/tracking/Data;->Companion:Lco/uk/getmondo/api/model/tracking/Data$Companion;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Lco/uk/getmondo/api/model/tracking/Data$Companion;->a(Ljava/lang/Integer;)Lco/uk/getmondo/api/model/tracking/Data;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->a(Ljava/lang/String;Lco/uk/getmondo/api/model/tracking/Data;)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    return-object v0
.end method

.method public final b(ILjava/lang/String;Ljava/lang/String;Lco/uk/getmondo/api/model/b;)Lco/uk/getmondo/api/model/tracking/Impression;
    .locals 2

    .prologue
    const-string v0, "path"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 531
    check-cast p0, Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    const-string v0, "reported_error"

    sget-object v1, Lco/uk/getmondo/api/model/tracking/Data;->Companion:Lco/uk/getmondo/api/model/tracking/Data$Companion;

    invoke-virtual {v1, p1, p2, p3, p4}, Lco/uk/getmondo/api/model/tracking/Data$Companion;->a(ILjava/lang/String;Ljava/lang/String;Lco/uk/getmondo/api/model/b;)Lco/uk/getmondo/api/model/tracking/Data;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->a(Ljava/lang/String;Lco/uk/getmondo/api/model/tracking/Data;)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    return-object v0
.end method

.method public final b(Lco/uk/getmondo/api/model/tracking/Impression$KycFrom;)Lco/uk/getmondo/api/model/tracking/Impression;
    .locals 3

    .prologue
    const-string v0, "from"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 349
    check-cast p0, Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    const-string v0, "kyc-tour-2.show"

    sget-object v1, Lco/uk/getmondo/api/model/tracking/Data;->Companion:Lco/uk/getmondo/api/model/tracking/Data$Companion;

    invoke-virtual {p1}, Lco/uk/getmondo/api/model/tracking/Impression$KycFrom;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lco/uk/getmondo/api/model/tracking/Data$Companion;->i(Ljava/lang/String;)Lco/uk/getmondo/api/model/tracking/Data;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->a(Ljava/lang/String;Lco/uk/getmondo/api/model/tracking/Data;)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    return-object v0
.end method

.method public final b(Lco/uk/getmondo/api/model/tracking/Impression$PinFrom;)Lco/uk/getmondo/api/model/tracking/Impression;
    .locals 65

    .prologue
    const-string v2, "pinFrom"

    move-object/from16 v0, p1

    invoke-static {v0, v2}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 809
    check-cast p0, Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    const-string v64, "pin.show"

    new-instance v2, Lco/uk/getmondo/api/model/tracking/Data;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    const/16 v17, 0x0

    const/16 v18, 0x0

    const/16 v19, 0x0

    const/16 v20, 0x0

    const/16 v21, 0x0

    const/16 v22, 0x0

    const/16 v23, 0x0

    invoke-virtual/range {p1 .. p1}, Lco/uk/getmondo/api/model/tracking/Impression$PinFrom;->a()Ljava/lang/String;

    move-result-object v24

    const/16 v25, 0x0

    const/16 v26, 0x0

    const/16 v27, 0x0

    const/16 v28, 0x0

    const/16 v29, 0x0

    const/16 v30, 0x0

    const/16 v31, 0x0

    const/16 v32, 0x0

    const/16 v33, 0x0

    const/16 v34, 0x0

    const/16 v35, 0x0

    const/16 v36, 0x0

    const/16 v37, 0x0

    const/16 v38, 0x0

    const/16 v39, 0x0

    const/16 v40, 0x0

    const/16 v41, 0x0

    const/16 v42, 0x0

    const/16 v43, 0x0

    const/16 v44, 0x0

    const/16 v45, 0x0

    const/16 v46, 0x0

    const/16 v47, 0x0

    const/16 v48, 0x0

    const/16 v49, 0x0

    const/16 v50, 0x0

    const/16 v51, 0x0

    const/16 v52, 0x0

    const/16 v53, 0x0

    const/16 v54, 0x0

    const/16 v55, 0x0

    const/16 v56, 0x0

    const/16 v57, 0x0

    const/16 v58, 0x0

    const/16 v59, 0x0

    const/16 v60, 0x0

    const v61, -0x200001

    const v62, 0x3ffffff

    const/16 v63, 0x0

    invoke-direct/range {v2 .. v63}, Lco/uk/getmondo/api/model/tracking/Data;-><init>(Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Boolean;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IILkotlin/d/b/i;)V

    move-object/from16 v0, p0

    move-object/from16 v1, v64

    invoke-virtual {v0, v1, v2}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->a(Ljava/lang/String;Lco/uk/getmondo/api/model/tracking/Data;)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v2

    return-object v2
.end method

.method public final b(Lco/uk/getmondo/d/h;Lorg/threeten/bp/YearMonth;)Lco/uk/getmondo/api/model/tracking/Impression;
    .locals 2

    .prologue
    const-string v0, "category"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "yearMonth"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 426
    check-cast p0, Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    const-string v0, "spend-merchants.show"

    sget-object v1, Lco/uk/getmondo/api/model/tracking/Data;->Companion:Lco/uk/getmondo/api/model/tracking/Data$Companion;

    invoke-virtual {v1, p1, p2}, Lco/uk/getmondo/api/model/tracking/Data$Companion;->a(Lco/uk/getmondo/d/h;Lorg/threeten/bp/YearMonth;)Lco/uk/getmondo/api/model/tracking/Data;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->a(Ljava/lang/String;Lco/uk/getmondo/api/model/tracking/Data;)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    return-object v0
.end method

.method public final b(Ljava/lang/Boolean;)Lco/uk/getmondo/api/model/tracking/Impression;
    .locals 2

    .prologue
    .line 652
    check-cast p0, Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    const-string v0, "profile-payments.toggle"

    sget-object v1, Lco/uk/getmondo/api/model/tracking/Data;->Companion:Lco/uk/getmondo/api/model/tracking/Data$Companion;

    invoke-virtual {v1, p1}, Lco/uk/getmondo/api/model/tracking/Data$Companion;->a(Ljava/lang/Boolean;)Lco/uk/getmondo/api/model/tracking/Data;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->a(Ljava/lang/String;Lco/uk/getmondo/api/model/tracking/Data;)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    return-object v0
.end method

.method public final b(Ljava/lang/String;)Lco/uk/getmondo/api/model/tracking/Impression;
    .locals 2

    .prologue
    const-string v0, "url"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 172
    check-cast p0, Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    const-string v0, "wl.webview-close.tap"

    sget-object v1, Lco/uk/getmondo/api/model/tracking/Data;->Companion:Lco/uk/getmondo/api/model/tracking/Data$Companion;

    invoke-virtual {v1, p1}, Lco/uk/getmondo/api/model/tracking/Data$Companion;->a(Ljava/lang/String;)Lco/uk/getmondo/api/model/tracking/Data;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->a(Ljava/lang/String;Lco/uk/getmondo/api/model/tracking/Data;)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    return-object v0
.end method

.method public final b(Ljava/lang/String;Ljava/lang/String;)Lco/uk/getmondo/api/model/tracking/Impression;
    .locals 65

    .prologue
    const-string v2, "redirectUrl"

    move-object/from16 v0, p1

    invoke-static {v0, v2}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v2, "errorType"

    move-object/from16 v0, p2

    invoke-static {v0, v2}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 692
    check-cast p0, Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    const-string v64, "topup-3dsecure.pageview.error"

    new-instance v2, Lco/uk/getmondo/api/model/tracking/Data;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    const/16 v17, 0x0

    const/16 v18, 0x0

    const/16 v19, 0x0

    const/16 v20, 0x0

    const/16 v21, 0x0

    const/16 v22, 0x0

    const/16 v23, 0x0

    const/16 v24, 0x0

    const/16 v25, 0x0

    const/16 v26, 0x0

    const/16 v27, 0x0

    const/16 v28, 0x0

    const/16 v29, 0x0

    const/16 v30, 0x0

    const/16 v31, 0x0

    const/16 v32, 0x0

    const/16 v33, 0x0

    const/16 v34, 0x0

    const/16 v35, 0x0

    const/16 v36, 0x0

    const/16 v37, 0x0

    const/16 v38, 0x0

    const/16 v39, 0x0

    const/16 v40, 0x0

    const/16 v41, 0x0

    const/16 v43, 0x0

    const/16 v44, 0x0

    const/16 v45, 0x0

    const/16 v46, 0x0

    const/16 v47, 0x0

    const/16 v48, 0x0

    const/16 v49, 0x0

    const/16 v50, 0x0

    const/16 v51, 0x0

    const/16 v52, 0x0

    const/16 v53, 0x0

    const/16 v54, 0x0

    const/16 v55, 0x0

    const/16 v56, 0x0

    const/16 v57, 0x0

    const/16 v58, 0x0

    const/16 v59, 0x0

    const/16 v60, 0x0

    const/16 v61, -0x21

    const v62, 0x3ffff7f

    const/16 v63, 0x0

    move-object/from16 v8, p1

    move-object/from16 v42, p2

    invoke-direct/range {v2 .. v63}, Lco/uk/getmondo/api/model/tracking/Data;-><init>(Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Boolean;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IILkotlin/d/b/i;)V

    move-object/from16 v0, p0

    move-object/from16 v1, v64

    invoke-virtual {v0, v1, v2}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->a(Ljava/lang/String;Lco/uk/getmondo/api/model/tracking/Data;)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v2

    return-object v2
.end method

.method public final b(Ljava/lang/String;Z)Lco/uk/getmondo/api/model/tracking/Impression;
    .locals 2

    .prologue
    const-string v0, "reason"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 547
    check-cast p0, Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    const-string v0, "kyc-video-error"

    sget-object v1, Lco/uk/getmondo/api/model/tracking/Data;->Companion:Lco/uk/getmondo/api/model/tracking/Data$Companion;

    invoke-virtual {v1, p1, p2}, Lco/uk/getmondo/api/model/tracking/Data$Companion;->a(Ljava/lang/String;Z)Lco/uk/getmondo/api/model/tracking/Data;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->a(Ljava/lang/String;Lco/uk/getmondo/api/model/tracking/Data;)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    return-object v0
.end method

.method public final b(Z)Lco/uk/getmondo/api/model/tracking/Impression;
    .locals 4

    .prologue
    .line 220
    move-object v0, p0

    check-cast v0, Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    const-string v1, "topup-3dsecure.enter"

    sget-object v2, Lco/uk/getmondo/api/model/tracking/Data;->Companion:Lco/uk/getmondo/api/model/tracking/Data$Companion;

    check-cast p0, Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    invoke-virtual {p0, p1}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->g(Z)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lco/uk/getmondo/api/model/tracking/Data$Companion;->h(Ljava/lang/String;)Lco/uk/getmondo/api/model/tracking/Data;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->a(Ljava/lang/String;Lco/uk/getmondo/api/model/tracking/Data;)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    return-object v0
.end method

.method public final ba()Lco/uk/getmondo/api/model/tracking/Impression;
    .locals 1

    .prologue
    .line 858
    check-cast p0, Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    const-string v0, "migration-invitation-banner.show"

    invoke-virtual {p0, v0}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->n(Ljava/lang/String;)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    return-object v0
.end method

.method public final bb()Lco/uk/getmondo/api/model/tracking/Impression;
    .locals 1

    .prologue
    .line 860
    check-cast p0, Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    const-string v0, "migration-invitation-details.show"

    invoke-virtual {p0, v0}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->n(Ljava/lang/String;)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    return-object v0
.end method

.method public final bc()Lco/uk/getmondo/api/model/tracking/Impression;
    .locals 1

    .prologue
    .line 862
    check-cast p0, Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    const-string v0, "migration-invitation-learn-more.tap"

    invoke-virtual {p0, v0}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->n(Ljava/lang/String;)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    return-object v0
.end method

.method public final bd()Lco/uk/getmondo/api/model/tracking/Impression;
    .locals 1

    .prologue
    .line 864
    check-cast p0, Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    const-string v0, "migration-start.tap"

    invoke-virtual {p0, v0}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->n(Ljava/lang/String;)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    return-object v0
.end method

.method public final be()Lco/uk/getmondo/api/model/tracking/Impression;
    .locals 1

    .prologue
    .line 866
    check-cast p0, Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    const-string v0, "migration-intro-steps.show"

    invoke-virtual {p0, v0}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->n(Ljava/lang/String;)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    return-object v0
.end method

.method public final bf()Lco/uk/getmondo/api/model/tracking/Impression;
    .locals 1

    .prologue
    .line 868
    check-cast p0, Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    const-string v0, "migration-intro-package.show"

    invoke-virtual {p0, v0}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->n(Ljava/lang/String;)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    return-object v0
.end method

.method public final bg()Lco/uk/getmondo/api/model/tracking/Impression;
    .locals 1

    .prologue
    .line 870
    check-cast p0, Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    const-string v0, "migration-continue-banner.show"

    invoke-virtual {p0, v0}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->n(Ljava/lang/String;)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    return-object v0
.end method

.method public final bh()Lco/uk/getmondo/api/model/tracking/Impression;
    .locals 1

    .prologue
    .line 872
    check-cast p0, Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    const-string v0, "migration-continue-banner.tap"

    invoke-virtual {p0, v0}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->n(Ljava/lang/String;)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    return-object v0
.end method

.method public final c()Lco/uk/getmondo/api/model/tracking/Impression;
    .locals 1

    .prologue
    .line 112
    check-cast p0, Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    const-string v0, "app.background"

    invoke-virtual {p0, v0}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->n(Ljava/lang/String;)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    return-object v0
.end method

.method public final c(I)Lco/uk/getmondo/api/model/tracking/Impression;
    .locals 3

    .prologue
    .line 459
    check-cast p0, Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    const-string v0, "split-cost.tap"

    sget-object v1, Lco/uk/getmondo/api/model/tracking/Data;->Companion:Lco/uk/getmondo/api/model/tracking/Data$Companion;

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lco/uk/getmondo/api/model/tracking/Data$Companion;->m(Ljava/lang/String;)Lco/uk/getmondo/api/model/tracking/Data;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->a(Ljava/lang/String;Lco/uk/getmondo/api/model/tracking/Data;)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    return-object v0
.end method

.method public final c(ILjava/lang/String;Ljava/lang/String;Lco/uk/getmondo/api/model/b;)Lco/uk/getmondo/api/model/tracking/Impression;
    .locals 2

    .prologue
    const-string v0, "path"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 539
    check-cast p0, Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    const-string v0, "logout_error"

    sget-object v1, Lco/uk/getmondo/api/model/tracking/Data;->Companion:Lco/uk/getmondo/api/model/tracking/Data$Companion;

    invoke-virtual {v1, p1, p2, p3, p4}, Lco/uk/getmondo/api/model/tracking/Data$Companion;->a(ILjava/lang/String;Ljava/lang/String;Lco/uk/getmondo/api/model/b;)Lco/uk/getmondo/api/model/tracking/Data;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->a(Ljava/lang/String;Lco/uk/getmondo/api/model/tracking/Data;)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    return-object v0
.end method

.method public final c(Lco/uk/getmondo/api/model/tracking/Impression$KycFrom;)Lco/uk/getmondo/api/model/tracking/Impression;
    .locals 3

    .prologue
    const-string v0, "from"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 353
    check-cast p0, Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    const-string v0, "kyc-tour-3.show"

    sget-object v1, Lco/uk/getmondo/api/model/tracking/Data;->Companion:Lco/uk/getmondo/api/model/tracking/Data$Companion;

    invoke-virtual {p1}, Lco/uk/getmondo/api/model/tracking/Impression$KycFrom;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lco/uk/getmondo/api/model/tracking/Data$Companion;->i(Ljava/lang/String;)Lco/uk/getmondo/api/model/tracking/Data;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->a(Ljava/lang/String;Lco/uk/getmondo/api/model/tracking/Data;)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    return-object v0
.end method

.method public final c(Lco/uk/getmondo/d/h;Lorg/threeten/bp/YearMonth;)Lco/uk/getmondo/api/model/tracking/Impression;
    .locals 2

    .prologue
    const-string v0, "category"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "yearMonth"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 430
    check-cast p0, Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    const-string v0, "spend-merchants-list.show"

    sget-object v1, Lco/uk/getmondo/api/model/tracking/Data;->Companion:Lco/uk/getmondo/api/model/tracking/Data$Companion;

    invoke-virtual {v1, p1, p2}, Lco/uk/getmondo/api/model/tracking/Data$Companion;->a(Lco/uk/getmondo/d/h;Lorg/threeten/bp/YearMonth;)Lco/uk/getmondo/api/model/tracking/Data;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->a(Ljava/lang/String;Lco/uk/getmondo/api/model/tracking/Data;)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    return-object v0
.end method

.method public final c(Ljava/lang/Boolean;)Lco/uk/getmondo/api/model/tracking/Impression;
    .locals 2

    .prologue
    .line 656
    check-cast p0, Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    const-string v0, "profile-notifications.toggle"

    sget-object v1, Lco/uk/getmondo/api/model/tracking/Data;->Companion:Lco/uk/getmondo/api/model/tracking/Data$Companion;

    invoke-virtual {v1, p1}, Lco/uk/getmondo/api/model/tracking/Data$Companion;->a(Ljava/lang/Boolean;)Lco/uk/getmondo/api/model/tracking/Data;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->a(Ljava/lang/String;Lco/uk/getmondo/api/model/tracking/Data;)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    return-object v0
.end method

.method public final c(Ljava/lang/String;)Lco/uk/getmondo/api/model/tracking/Impression;
    .locals 2

    .prologue
    const-string v0, "url"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 176
    check-cast p0, Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    const-string v0, "wl.webview-share-sheet.tap"

    sget-object v1, Lco/uk/getmondo/api/model/tracking/Data;->Companion:Lco/uk/getmondo/api/model/tracking/Data$Companion;

    invoke-virtual {v1, p1}, Lco/uk/getmondo/api/model/tracking/Data$Companion;->a(Ljava/lang/String;)Lco/uk/getmondo/api/model/tracking/Data;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->a(Ljava/lang/String;Lco/uk/getmondo/api/model/tracking/Data;)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    return-object v0
.end method

.method public final c(Ljava/lang/String;Ljava/lang/String;)Lco/uk/getmondo/api/model/tracking/Impression;
    .locals 64

    .prologue
    const-string v2, "analyticName"

    move-object/from16 v0, p1

    invoke-static {v0, v2}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v2, "appName"

    move-object/from16 v0, p2

    invoke-static {v0, v2}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 705
    check-cast p0, Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    new-instance v2, Lco/uk/getmondo/api/model/tracking/Data;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    const/16 v17, 0x0

    const/16 v18, 0x0

    const/16 v19, 0x0

    const/16 v20, 0x0

    const/16 v21, 0x0

    const/16 v22, 0x0

    const/16 v23, 0x0

    const/16 v24, 0x0

    const/16 v25, 0x0

    const/16 v26, 0x0

    const/16 v27, 0x0

    const/16 v28, 0x0

    const/16 v29, 0x0

    const/16 v30, 0x0

    const/16 v31, 0x0

    const/16 v32, 0x0

    const/16 v33, 0x0

    const/16 v34, 0x0

    const/16 v36, 0x0

    const/16 v37, 0x0

    const/16 v38, 0x0

    const/16 v39, 0x0

    const/16 v40, 0x0

    const/16 v41, 0x0

    const/16 v42, 0x0

    const/16 v43, 0x0

    const/16 v44, 0x0

    const/16 v45, 0x0

    const/16 v46, 0x0

    const/16 v47, 0x0

    const/16 v48, 0x0

    const/16 v49, 0x0

    const/16 v50, 0x0

    const/16 v51, 0x0

    const/16 v52, 0x0

    const/16 v53, 0x0

    const/16 v54, 0x0

    const/16 v55, 0x0

    const/16 v56, 0x0

    const/16 v57, 0x0

    const/16 v58, 0x0

    const/16 v59, 0x0

    const/16 v60, 0x0

    const/16 v61, -0x1

    const v62, 0x3fffffe

    const/16 v63, 0x0

    move-object/from16 v35, p2

    invoke-direct/range {v2 .. v63}, Lco/uk/getmondo/api/model/tracking/Data;-><init>(Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Boolean;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IILkotlin/d/b/i;)V

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-virtual {v0, v1, v2}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->a(Ljava/lang/String;Lco/uk/getmondo/api/model/tracking/Data;)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v2

    return-object v2
.end method

.method public final c(Z)Lco/uk/getmondo/api/model/tracking/Impression;
    .locals 4

    .prologue
    .line 229
    move-object v0, p0

    check-cast v0, Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    const-string v1, "topup-3dsecure.fail"

    sget-object v2, Lco/uk/getmondo/api/model/tracking/Data;->Companion:Lco/uk/getmondo/api/model/tracking/Data$Companion;

    check-cast p0, Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    invoke-virtual {p0, p1}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->g(Z)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lco/uk/getmondo/api/model/tracking/Data$Companion;->h(Ljava/lang/String;)Lco/uk/getmondo/api/model/tracking/Data;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->a(Ljava/lang/String;Lco/uk/getmondo/api/model/tracking/Data;)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    return-object v0
.end method

.method public final d()Lco/uk/getmondo/api/model/tracking/Impression;
    .locals 4

    .prologue
    .line 116
    move-object v0, p0

    check-cast v0, Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "wl.welcome.show"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    check-cast p0, Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    invoke-virtual {p0}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->a()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->a([Ljava/lang/String;)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    return-object v0
.end method

.method public final d(I)Lco/uk/getmondo/api/model/tracking/Impression;
    .locals 3

    .prologue
    .line 551
    check-cast p0, Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    const-string v0, "request-tour.show"

    sget-object v1, Lco/uk/getmondo/api/model/tracking/Data;->Companion:Lco/uk/getmondo/api/model/tracking/Data$Companion;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Lco/uk/getmondo/api/model/tracking/Data$Companion;->a(Ljava/lang/Integer;)Lco/uk/getmondo/api/model/tracking/Data;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->a(Ljava/lang/String;Lco/uk/getmondo/api/model/tracking/Data;)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    return-object v0
.end method

.method public final d(Ljava/lang/String;)Lco/uk/getmondo/api/model/tracking/Impression;
    .locals 2

    .prologue
    const-string v0, "url"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 180
    check-cast p0, Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    const-string v0, "wl.webview-external-link.tap"

    sget-object v1, Lco/uk/getmondo/api/model/tracking/Data;->Companion:Lco/uk/getmondo/api/model/tracking/Data$Companion;

    invoke-virtual {v1, p1}, Lco/uk/getmondo/api/model/tracking/Data$Companion;->a(Ljava/lang/String;)Lco/uk/getmondo/api/model/tracking/Data;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->a(Ljava/lang/String;Lco/uk/getmondo/api/model/tracking/Data;)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    return-object v0
.end method

.method public final d(Ljava/lang/String;Ljava/lang/String;)Lco/uk/getmondo/api/model/tracking/Impression;
    .locals 65

    .prologue
    const-string v2, "type"

    move-object/from16 v0, p1

    invoke-static {v0, v2}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 745
    check-cast p0, Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    const-string v64, "app.notification-received"

    new-instance v2, Lco/uk/getmondo/api/model/tracking/Data;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    const/16 v17, 0x0

    const/16 v18, 0x0

    const/16 v19, 0x0

    const/16 v20, 0x0

    const/16 v21, 0x0

    const/16 v22, 0x0

    const/16 v23, 0x0

    const/16 v24, 0x0

    const/16 v25, 0x0

    const/16 v26, 0x0

    const/16 v27, 0x0

    const/16 v28, 0x0

    const/16 v29, 0x0

    const/16 v30, 0x0

    const/16 v31, 0x0

    const/16 v32, 0x0

    const/16 v33, 0x0

    const/16 v34, 0x0

    const/16 v35, 0x0

    const/16 v36, 0x0

    const/16 v37, 0x0

    const/16 v38, 0x0

    const/16 v39, 0x0

    const/16 v40, 0x0

    const/16 v41, 0x0

    const/16 v42, 0x0

    const/16 v43, 0x0

    const/16 v44, 0x0

    const/16 v45, 0x0

    const/16 v46, 0x0

    const/16 v47, 0x0

    const/16 v48, 0x0

    const/16 v49, 0x0

    const/16 v50, 0x0

    const/16 v51, 0x0

    const/16 v52, 0x0

    const/16 v53, 0x0

    const/16 v56, 0x0

    const/16 v57, 0x0

    const/16 v58, 0x0

    const/16 v59, 0x0

    const/16 v60, 0x0

    const/16 v61, -0x1

    const v62, 0x3e7ffff

    const/16 v63, 0x0

    move-object/from16 v54, p1

    move-object/from16 v55, p2

    invoke-direct/range {v2 .. v63}, Lco/uk/getmondo/api/model/tracking/Data;-><init>(Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Boolean;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IILkotlin/d/b/i;)V

    move-object/from16 v0, p0

    move-object/from16 v1, v64

    invoke-virtual {v0, v1, v2}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->a(Ljava/lang/String;Lco/uk/getmondo/api/model/tracking/Data;)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v2

    return-object v2
.end method

.method public final d(Z)Lco/uk/getmondo/api/model/tracking/Impression;
    .locals 4

    .prologue
    .line 233
    move-object v0, p0

    check-cast v0, Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    const-string v1, "topup-3dsecure.success"

    sget-object v2, Lco/uk/getmondo/api/model/tracking/Data;->Companion:Lco/uk/getmondo/api/model/tracking/Data$Companion;

    check-cast p0, Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    invoke-virtual {p0, p1}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->g(Z)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lco/uk/getmondo/api/model/tracking/Data$Companion;->h(Ljava/lang/String;)Lco/uk/getmondo/api/model/tracking/Data;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->a(Ljava/lang/String;Lco/uk/getmondo/api/model/tracking/Data;)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    return-object v0
.end method

.method public final e()Lco/uk/getmondo/api/model/tracking/Impression;
    .locals 2

    .prologue
    .line 120
    move-object v0, p0

    check-cast v0, Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    check-cast p0, Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    invoke-virtual {p0}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->n(Ljava/lang/String;)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    return-object v0
.end method

.method public final e(Ljava/lang/String;)Lco/uk/getmondo/api/model/tracking/Impression;
    .locals 2

    .prologue
    const-string v0, "description"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 216
    check-cast p0, Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    const-string v0, "topup_error"

    sget-object v1, Lco/uk/getmondo/api/model/tracking/Data;->Companion:Lco/uk/getmondo/api/model/tracking/Data$Companion;

    invoke-virtual {v1, p1}, Lco/uk/getmondo/api/model/tracking/Data$Companion;->h(Ljava/lang/String;)Lco/uk/getmondo/api/model/tracking/Data;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->a(Ljava/lang/String;Lco/uk/getmondo/api/model/tracking/Data;)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    return-object v0
.end method

.method public final e(Ljava/lang/String;Ljava/lang/String;)Lco/uk/getmondo/api/model/tracking/Impression;
    .locals 65

    .prologue
    const-string v2, "from"

    move-object/from16 v0, p1

    invoke-static {v0, v2}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v2, "fileType"

    move-object/from16 v0, p2

    invoke-static {v0, v2}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 849
    check-cast p0, Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    const-string v64, "export-data.tap"

    new-instance v2, Lco/uk/getmondo/api/model/tracking/Data;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    const/16 v17, 0x0

    const/16 v18, 0x0

    const/16 v19, 0x0

    const/16 v20, 0x0

    const/16 v21, 0x0

    const/16 v22, 0x0

    const/16 v23, 0x0

    const/16 v25, 0x0

    const/16 v26, 0x0

    const/16 v27, 0x0

    const/16 v28, 0x0

    const/16 v29, 0x0

    const/16 v30, 0x0

    const/16 v31, 0x0

    const/16 v32, 0x0

    const/16 v33, 0x0

    const/16 v34, 0x0

    const/16 v35, 0x0

    const/16 v36, 0x0

    const/16 v37, 0x0

    const/16 v38, 0x0

    const/16 v39, 0x0

    const/16 v40, 0x0

    const/16 v41, 0x0

    const/16 v42, 0x0

    const/16 v43, 0x0

    const/16 v44, 0x0

    const/16 v45, 0x0

    const/16 v46, 0x0

    const/16 v47, 0x0

    const/16 v48, 0x0

    const/16 v49, 0x0

    const/16 v50, 0x0

    const/16 v51, 0x0

    const/16 v52, 0x0

    const/16 v53, 0x0

    const/16 v54, 0x0

    const/16 v55, 0x0

    const/16 v56, 0x0

    const/16 v57, 0x0

    const/16 v58, 0x0

    const/16 v59, 0x0

    const v61, -0x200001

    const v62, 0x1ffffff

    const/16 v63, 0x0

    move-object/from16 v24, p1

    move-object/from16 v60, p2

    invoke-direct/range {v2 .. v63}, Lco/uk/getmondo/api/model/tracking/Data;-><init>(Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Boolean;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IILkotlin/d/b/i;)V

    move-object/from16 v0, p0

    move-object/from16 v1, v64

    invoke-virtual {v0, v1, v2}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->a(Ljava/lang/String;Lco/uk/getmondo/api/model/tracking/Data;)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v2

    return-object v2
.end method

.method public final e(Z)Lco/uk/getmondo/api/model/tracking/Impression;
    .locals 4

    .prologue
    .line 237
    move-object v0, p0

    check-cast v0, Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    const-string v1, "topup-3dsecure.complete"

    sget-object v2, Lco/uk/getmondo/api/model/tracking/Data;->Companion:Lco/uk/getmondo/api/model/tracking/Data$Companion;

    check-cast p0, Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    invoke-virtual {p0, p1}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->g(Z)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lco/uk/getmondo/api/model/tracking/Data$Companion;->h(Ljava/lang/String;)Lco/uk/getmondo/api/model/tracking/Data;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->a(Ljava/lang/String;Lco/uk/getmondo/api/model/tracking/Data;)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    return-object v0
.end method

.method public final f()Lco/uk/getmondo/api/model/tracking/Impression;
    .locals 1

    .prologue
    .line 124
    check-cast p0, Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    const-string v0, "wl.name-dob.show"

    invoke-virtual {p0, v0}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->n(Ljava/lang/String;)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    return-object v0
.end method

.method public final f(Ljava/lang/String;)Lco/uk/getmondo/api/model/tracking/Impression;
    .locals 2

    .prologue
    const-string v0, "transactionId"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 249
    check-cast p0, Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    const-string v0, "transaction.show"

    sget-object v1, Lco/uk/getmondo/api/model/tracking/Data;->Companion:Lco/uk/getmondo/api/model/tracking/Data$Companion;

    invoke-virtual {v1, p1}, Lco/uk/getmondo/api/model/tracking/Data$Companion;->c(Ljava/lang/String;)Lco/uk/getmondo/api/model/tracking/Data;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->a(Ljava/lang/String;Lco/uk/getmondo/api/model/tracking/Data;)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    return-object v0
.end method

.method public final f(Z)Lco/uk/getmondo/api/model/tracking/Impression;
    .locals 65

    .prologue
    .line 369
    check-cast p0, Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    const-string v64, "kyc-selfie.show"

    new-instance v2, Lco/uk/getmondo/api/model/tracking/Data;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    const/16 v17, 0x0

    const/16 v18, 0x0

    const/16 v19, 0x0

    const/16 v20, 0x0

    const/16 v21, 0x0

    const/16 v22, 0x0

    const/16 v23, 0x0

    const/16 v24, 0x0

    const/16 v25, 0x0

    const/16 v26, 0x0

    const/16 v27, 0x0

    const/16 v28, 0x0

    const/16 v29, 0x0

    const/16 v30, 0x0

    const/16 v31, 0x0

    const/16 v32, 0x0

    const/16 v33, 0x0

    const/16 v34, 0x0

    const/16 v35, 0x0

    const/16 v36, 0x0

    const/16 v37, 0x0

    const/16 v38, 0x0

    const/16 v39, 0x0

    const/16 v40, 0x0

    const/16 v41, 0x0

    const/16 v42, 0x0

    const/16 v43, 0x0

    const/16 v44, 0x0

    const/16 v45, 0x0

    const/16 v46, 0x0

    const/16 v47, 0x0

    const/16 v48, 0x0

    const/16 v49, 0x0

    const/16 v50, 0x0

    const/16 v51, 0x0

    const/16 v52, 0x0

    invoke-static/range {p1 .. p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v53

    const/16 v54, 0x0

    const/16 v55, 0x0

    const/16 v56, 0x0

    const/16 v57, 0x0

    const/16 v58, 0x0

    const/16 v59, 0x0

    const/16 v60, 0x0

    const/16 v61, -0x1

    const v62, 0x3fbffff

    const/16 v63, 0x0

    invoke-direct/range {v2 .. v63}, Lco/uk/getmondo/api/model/tracking/Data;-><init>(Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Boolean;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IILkotlin/d/b/i;)V

    move-object/from16 v0, p0

    move-object/from16 v1, v64

    invoke-virtual {v0, v1, v2}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->a(Ljava/lang/String;Lco/uk/getmondo/api/model/tracking/Data;)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v2

    return-object v2
.end method

.method public final g()Lco/uk/getmondo/api/model/tracking/Impression;
    .locals 1

    .prologue
    .line 132
    check-cast p0, Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    const-string v0, "login.confirm.show"

    invoke-virtual {p0, v0}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->n(Ljava/lang/String;)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    return-object v0
.end method

.method public final g(Ljava/lang/String;)Lco/uk/getmondo/api/model/tracking/Impression;
    .locals 2

    .prologue
    const-string v0, "transactionId"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 253
    check-cast p0, Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    const-string v0, "full-map.show"

    sget-object v1, Lco/uk/getmondo/api/model/tracking/Data;->Companion:Lco/uk/getmondo/api/model/tracking/Data$Companion;

    invoke-virtual {v1, p1}, Lco/uk/getmondo/api/model/tracking/Data$Companion;->c(Ljava/lang/String;)Lco/uk/getmondo/api/model/tracking/Data;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->a(Ljava/lang/String;Lco/uk/getmondo/api/model/tracking/Data;)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    return-object v0
.end method

.method public final g(Z)Ljava/lang/String;
    .locals 1

    .prologue
    .line 434
    if-eqz p1, :cond_0

    const-string v0, "initial_top_up"

    :goto_0
    return-object v0

    :cond_0
    const-string v0, "top_up"

    goto :goto_0
.end method

.method public final h()Lco/uk/getmondo/api/model/tracking/Impression;
    .locals 1

    .prologue
    .line 136
    check-cast p0, Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    const-string v0, "login.mail.tap"

    invoke-virtual {p0, v0}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->n(Ljava/lang/String;)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    return-object v0
.end method

.method public final h(Ljava/lang/String;)Lco/uk/getmondo/api/model/tracking/Impression;
    .locals 2

    .prologue
    const-string v0, "referrer"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 269
    check-cast p0, Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    const-string v0, "app.play-store-referrer"

    sget-object v1, Lco/uk/getmondo/api/model/tracking/Data;->Companion:Lco/uk/getmondo/api/model/tracking/Data$Companion;

    invoke-virtual {v1, p1}, Lco/uk/getmondo/api/model/tracking/Data$Companion;->d(Ljava/lang/String;)Lco/uk/getmondo/api/model/tracking/Data;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->a(Ljava/lang/String;Lco/uk/getmondo/api/model/tracking/Data;)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    return-object v0
.end method

.method public final h(Z)Lco/uk/getmondo/api/model/tracking/Impression;
    .locals 2

    .prologue
    .line 487
    check-cast p0, Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    const-string v0, "p2p.contacts-access.request"

    sget-object v1, Lco/uk/getmondo/api/model/tracking/Data;->Companion:Lco/uk/getmondo/api/model/tracking/Data$Companion;

    invoke-virtual {v1, p1}, Lco/uk/getmondo/api/model/tracking/Data$Companion;->a(Z)Lco/uk/getmondo/api/model/tracking/Data;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->a(Ljava/lang/String;Lco/uk/getmondo/api/model/tracking/Data;)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    return-object v0
.end method

.method public final i()Lco/uk/getmondo/api/model/tracking/Impression;
    .locals 1

    .prologue
    .line 140
    check-cast p0, Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    const-string v0, "login.nomail.tap"

    invoke-virtual {p0, v0}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->n(Ljava/lang/String;)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    return-object v0
.end method

.method public final i(Ljava/lang/String;)Lco/uk/getmondo/api/model/tracking/Impression;
    .locals 2

    .prologue
    const-string v0, "redirectId"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 273
    check-cast p0, Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    const-string v0, "app.play-store-redirect-event-id-received"

    sget-object v1, Lco/uk/getmondo/api/model/tracking/Data;->Companion:Lco/uk/getmondo/api/model/tracking/Data$Companion;

    invoke-virtual {v1, p1}, Lco/uk/getmondo/api/model/tracking/Data$Companion;->e(Ljava/lang/String;)Lco/uk/getmondo/api/model/tracking/Data;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->a(Ljava/lang/String;Lco/uk/getmondo/api/model/tracking/Data;)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    return-object v0
.end method

.method public final i(Z)Lco/uk/getmondo/api/model/tracking/Impression;
    .locals 2

    .prologue
    .line 507
    check-cast p0, Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    const-string v0, "p2p.authenticate"

    sget-object v1, Lco/uk/getmondo/api/model/tracking/Data;->Companion:Lco/uk/getmondo/api/model/tracking/Data$Companion;

    invoke-virtual {v1, p1}, Lco/uk/getmondo/api/model/tracking/Data$Companion;->b(Z)Lco/uk/getmondo/api/model/tracking/Data;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->a(Ljava/lang/String;Lco/uk/getmondo/api/model/tracking/Data;)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    return-object v0
.end method

.method public final j()Lco/uk/getmondo/api/model/tracking/Impression;
    .locals 1

    .prologue
    .line 148
    check-cast p0, Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    const-string v0, "wl.invite-friends.show"

    invoke-virtual {p0, v0}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->n(Ljava/lang/String;)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    return-object v0
.end method

.method public final j(Ljava/lang/String;)Lco/uk/getmondo/api/model/tracking/Impression;
    .locals 65

    .prologue
    const-string v2, "identityDocumentType"

    move-object/from16 v0, p1

    invoke-static {v0, v2}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 365
    check-cast p0, Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    const-string v64, "kyc-document-ok.tap"

    new-instance v2, Lco/uk/getmondo/api/model/tracking/Data;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/16 v16, 0x0

    const/16 v17, 0x0

    const/16 v18, 0x0

    const/16 v19, 0x0

    const/16 v20, 0x0

    const/16 v21, 0x0

    const/16 v22, 0x0

    const/16 v23, 0x0

    const/16 v24, 0x0

    const/16 v25, 0x0

    const/16 v26, 0x0

    const/16 v27, 0x0

    const/16 v28, 0x0

    const/16 v29, 0x0

    const/16 v30, 0x0

    const/16 v31, 0x0

    const/16 v32, 0x0

    const/16 v33, 0x0

    const/16 v34, 0x0

    const/16 v35, 0x0

    const/16 v36, 0x0

    const/16 v37, 0x0

    const/16 v38, 0x0

    const/16 v39, 0x0

    const/16 v40, 0x0

    const/16 v41, 0x0

    const/16 v42, 0x0

    const/16 v43, 0x0

    const/16 v44, 0x0

    const/16 v45, 0x0

    const/16 v46, 0x0

    const/16 v47, 0x0

    const/16 v48, 0x0

    const/16 v49, 0x0

    const/16 v50, 0x0

    const/16 v51, 0x0

    const/16 v52, 0x0

    const/16 v53, 0x0

    const/16 v54, 0x0

    const/16 v55, 0x0

    const/16 v56, 0x0

    const/16 v57, 0x0

    const/16 v58, 0x0

    const/16 v59, 0x0

    const/16 v60, 0x0

    const/16 v61, -0x1001

    const v62, 0x3ffffff

    const/16 v63, 0x0

    move-object/from16 v15, p1

    invoke-direct/range {v2 .. v63}, Lco/uk/getmondo/api/model/tracking/Data;-><init>(Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Boolean;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IILkotlin/d/b/i;)V

    move-object/from16 v0, p0

    move-object/from16 v1, v64

    invoke-virtual {v0, v1, v2}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->a(Ljava/lang/String;Lco/uk/getmondo/api/model/tracking/Data;)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v2

    return-object v2
.end method

.method public final j(Z)Lco/uk/getmondo/api/model/tracking/Impression;
    .locals 2

    .prologue
    .line 684
    check-cast p0, Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    const-string v0, "golden-ticket.show"

    sget-object v1, Lco/uk/getmondo/api/model/tracking/Data;->Companion:Lco/uk/getmondo/api/model/tracking/Data$Companion;

    invoke-virtual {v1, p1}, Lco/uk/getmondo/api/model/tracking/Data$Companion;->c(Z)Lco/uk/getmondo/api/model/tracking/Data;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->a(Ljava/lang/String;Lco/uk/getmondo/api/model/tracking/Data;)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    return-object v0
.end method

.method public final k()Lco/uk/getmondo/api/model/tracking/Impression;
    .locals 1

    .prologue
    .line 152
    check-cast p0, Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    const-string v0, "wl.share-sheet.show"

    invoke-virtual {p0, v0}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->n(Ljava/lang/String;)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    return-object v0
.end method

.method public final k(Ljava/lang/String;)Lco/uk/getmondo/api/model/tracking/Impression;
    .locals 65

    .prologue
    const-string v2, "identityDocumentType"

    move-object/from16 v0, p1

    invoke-static {v0, v2}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 389
    check-cast p0, Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    const-string v64, "kyc-document-country.show"

    new-instance v2, Lco/uk/getmondo/api/model/tracking/Data;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/16 v16, 0x0

    const/16 v17, 0x0

    const/16 v18, 0x0

    const/16 v19, 0x0

    const/16 v20, 0x0

    const/16 v21, 0x0

    const/16 v22, 0x0

    const/16 v23, 0x0

    const/16 v24, 0x0

    const/16 v25, 0x0

    const/16 v26, 0x0

    const/16 v27, 0x0

    const/16 v28, 0x0

    const/16 v29, 0x0

    const/16 v30, 0x0

    const/16 v31, 0x0

    const/16 v32, 0x0

    const/16 v33, 0x0

    const/16 v34, 0x0

    const/16 v35, 0x0

    const/16 v36, 0x0

    const/16 v37, 0x0

    const/16 v38, 0x0

    const/16 v39, 0x0

    const/16 v40, 0x0

    const/16 v41, 0x0

    const/16 v42, 0x0

    const/16 v43, 0x0

    const/16 v44, 0x0

    const/16 v45, 0x0

    const/16 v46, 0x0

    const/16 v47, 0x0

    const/16 v48, 0x0

    const/16 v49, 0x0

    const/16 v50, 0x0

    const/16 v51, 0x0

    const/16 v52, 0x0

    const/16 v53, 0x0

    const/16 v54, 0x0

    const/16 v55, 0x0

    const/16 v56, 0x0

    const/16 v57, 0x0

    const/16 v58, 0x0

    const/16 v59, 0x0

    const/16 v60, 0x0

    const/16 v61, -0x1001

    const v62, 0x3ffffff

    const/16 v63, 0x0

    move-object/from16 v15, p1

    invoke-direct/range {v2 .. v63}, Lco/uk/getmondo/api/model/tracking/Data;-><init>(Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Boolean;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IILkotlin/d/b/i;)V

    move-object/from16 v0, p0

    move-object/from16 v1, v64

    invoke-virtual {v0, v1, v2}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->a(Ljava/lang/String;Lco/uk/getmondo/api/model/tracking/Data;)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v2

    return-object v2
.end method

.method public final k(Z)Lco/uk/getmondo/api/model/tracking/Impression;
    .locals 2

    .prologue
    .line 688
    check-cast p0, Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    const-string v0, "golden-ticket.share-sheet.show"

    sget-object v1, Lco/uk/getmondo/api/model/tracking/Data;->Companion:Lco/uk/getmondo/api/model/tracking/Data$Companion;

    invoke-virtual {v1, p1}, Lco/uk/getmondo/api/model/tracking/Data$Companion;->c(Z)Lco/uk/getmondo/api/model/tracking/Data;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->a(Ljava/lang/String;Lco/uk/getmondo/api/model/tracking/Data;)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    return-object v0
.end method

.method public final l()Lco/uk/getmondo/api/model/tracking/Impression;
    .locals 3

    .prologue
    .line 156
    check-cast p0, Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    const-string v0, "wl.top.show"

    sget-object v1, Lco/uk/getmondo/api/model/tracking/Data;->Companion:Lco/uk/getmondo/api/model/tracking/Data$Companion;

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Lco/uk/getmondo/api/model/tracking/Data$Companion;->b(Ljava/lang/Integer;)Lco/uk/getmondo/api/model/tracking/Data;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->a(Ljava/lang/String;Lco/uk/getmondo/api/model/tracking/Data;)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    return-object v0
.end method

.method public final l(Ljava/lang/String;)Lco/uk/getmondo/api/model/tracking/Impression;
    .locals 2

    .prologue
    const-string v0, "advertisingId"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 519
    check-cast p0, Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    const-string v0, "app.advertising-id-observed"

    sget-object v1, Lco/uk/getmondo/api/model/tracking/Data;->Companion:Lco/uk/getmondo/api/model/tracking/Data$Companion;

    invoke-virtual {v1, p1}, Lco/uk/getmondo/api/model/tracking/Data$Companion;->j(Ljava/lang/String;)Lco/uk/getmondo/api/model/tracking/Data;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->a(Ljava/lang/String;Lco/uk/getmondo/api/model/tracking/Data;)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    return-object v0
.end method

.method public final l(Z)Lco/uk/getmondo/api/model/tracking/Impression;
    .locals 65

    .prologue
    .line 769
    check-cast p0, Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    const-string v64, "marketing-opt-in"

    new-instance v2, Lco/uk/getmondo/api/model/tracking/Data;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    const/16 v17, 0x0

    const/16 v18, 0x0

    const/16 v19, 0x0

    const/16 v20, 0x0

    const/16 v21, 0x0

    const/16 v22, 0x0

    const/16 v23, 0x0

    const/16 v24, 0x0

    const/16 v25, 0x0

    const/16 v26, 0x0

    const/16 v27, 0x0

    const/16 v28, 0x0

    const/16 v29, 0x0

    const/16 v30, 0x0

    const/16 v31, 0x0

    const/16 v32, 0x0

    const/16 v33, 0x0

    const/16 v34, 0x0

    const/16 v35, 0x0

    const/16 v36, 0x0

    const/16 v37, 0x0

    const/16 v38, 0x0

    const/16 v39, 0x0

    const/16 v40, 0x0

    const/16 v41, 0x0

    const/16 v42, 0x0

    const/16 v43, 0x0

    const/16 v44, 0x0

    const/16 v45, 0x0

    const/16 v46, 0x0

    const/16 v47, 0x0

    const/16 v48, 0x0

    const/16 v49, 0x0

    const/16 v50, 0x0

    const/16 v51, 0x0

    const/16 v52, 0x0

    const/16 v53, 0x0

    const/16 v54, 0x0

    const/16 v55, 0x0

    invoke-static/range {p1 .. p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v56

    const/16 v57, 0x0

    const/16 v58, 0x0

    const/16 v59, 0x0

    const/16 v60, 0x0

    const/16 v61, -0x1

    const v62, 0x3dfffff

    const/16 v63, 0x0

    invoke-direct/range {v2 .. v63}, Lco/uk/getmondo/api/model/tracking/Data;-><init>(Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Boolean;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IILkotlin/d/b/i;)V

    move-object/from16 v0, p0

    move-object/from16 v1, v64

    invoke-virtual {v0, v1, v2}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->a(Ljava/lang/String;Lco/uk/getmondo/api/model/tracking/Data;)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v2

    return-object v2
.end method

.method public final m()Lco/uk/getmondo/api/model/tracking/Impression;
    .locals 3

    .prologue
    .line 160
    check-cast p0, Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    const-string v0, "wl.top.show"

    sget-object v1, Lco/uk/getmondo/api/model/tracking/Data;->Companion:Lco/uk/getmondo/api/model/tracking/Data$Companion;

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Lco/uk/getmondo/api/model/tracking/Data$Companion;->b(Ljava/lang/Integer;)Lco/uk/getmondo/api/model/tracking/Data;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->a(Ljava/lang/String;Lco/uk/getmondo/api/model/tracking/Data;)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    return-object v0
.end method

.method public final m(Ljava/lang/String;)Lco/uk/getmondo/api/model/tracking/Impression;
    .locals 2

    .prologue
    const-string v0, "shortcutType"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 523
    check-cast p0, Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    const-string v0, "appshortcut.tap"

    sget-object v1, Lco/uk/getmondo/api/model/tracking/Data;->Companion:Lco/uk/getmondo/api/model/tracking/Data$Companion;

    invoke-virtual {v1, p1}, Lco/uk/getmondo/api/model/tracking/Data$Companion;->k(Ljava/lang/String;)Lco/uk/getmondo/api/model/tracking/Data;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->a(Ljava/lang/String;Lco/uk/getmondo/api/model/tracking/Data;)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    return-object v0
.end method

.method public final m(Z)Lco/uk/getmondo/api/model/tracking/Impression;
    .locals 2

    .prologue
    .line 805
    check-cast p0, Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    const-string v0, "address-change.authenticate"

    sget-object v1, Lco/uk/getmondo/api/model/tracking/Data;->Companion:Lco/uk/getmondo/api/model/tracking/Data$Companion;

    invoke-virtual {v1, p1}, Lco/uk/getmondo/api/model/tracking/Data$Companion;->b(Z)Lco/uk/getmondo/api/model/tracking/Data;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->a(Ljava/lang/String;Lco/uk/getmondo/api/model/tracking/Data;)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    return-object v0
.end method

.method public final n()Lco/uk/getmondo/api/model/tracking/Impression;
    .locals 1

    .prologue
    .line 188
    check-cast p0, Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    const-string v0, "topup.bank-transfer.show"

    invoke-virtual {p0, v0}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->n(Ljava/lang/String;)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    return-object v0
.end method

.method public final n(Ljava/lang/String;)Lco/uk/getmondo/api/model/tracking/Impression;
    .locals 4

    .prologue
    const/4 v3, 0x0

    const-string v0, "name"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 590
    new-instance v0, Lco/uk/getmondo/api/model/tracking/Impression;

    const/4 v1, 0x1

    invoke-direct {v0, v3, v1, v3}, Lco/uk/getmondo/api/model/tracking/Impression;-><init>(Ljava/util/List;ILkotlin/d/b/i;)V

    .line 591
    new-instance v1, Lco/uk/getmondo/api/model/tracking/ImpressionEvent;

    const/4 v2, 0x2

    invoke-direct {v1, p1, v3, v2, v3}, Lco/uk/getmondo/api/model/tracking/ImpressionEvent;-><init>(Ljava/lang/String;Lco/uk/getmondo/api/model/tracking/Data;ILkotlin/d/b/i;)V

    invoke-virtual {v0, v1}, Lco/uk/getmondo/api/model/tracking/Impression;->a(Lco/uk/getmondo/api/model/tracking/ImpressionEvent;)V

    .line 592
    return-object v0
.end method

.method public final o()Lco/uk/getmondo/api/model/tracking/Impression;
    .locals 1

    .prologue
    .line 192
    check-cast p0, Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    const-string v0, "topup.bank-transfer.tap"

    invoke-virtual {p0, v0}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->n(Ljava/lang/String;)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    return-object v0
.end method

.method public final o(Ljava/lang/String;)Lco/uk/getmondo/api/model/tracking/Impression;
    .locals 65

    .prologue
    .line 813
    check-cast p0, Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    const-string v64, "help-content.show"

    new-instance v2, Lco/uk/getmondo/api/model/tracking/Data;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    const/16 v17, 0x0

    const/16 v18, 0x0

    const/16 v20, 0x0

    const/16 v21, 0x0

    const/16 v22, 0x0

    const/16 v23, 0x0

    const/16 v24, 0x0

    const/16 v25, 0x0

    const/16 v26, 0x0

    const/16 v27, 0x0

    const/16 v28, 0x0

    const/16 v29, 0x0

    const/16 v30, 0x0

    const/16 v31, 0x0

    const/16 v32, 0x0

    const/16 v33, 0x0

    const/16 v34, 0x0

    const/16 v35, 0x0

    const/16 v36, 0x0

    const/16 v37, 0x0

    const/16 v38, 0x0

    const/16 v39, 0x0

    const/16 v40, 0x0

    const/16 v41, 0x0

    const/16 v42, 0x0

    const/16 v43, 0x0

    const/16 v44, 0x0

    const/16 v45, 0x0

    const/16 v46, 0x0

    const/16 v47, 0x0

    const/16 v48, 0x0

    const/16 v49, 0x0

    const/16 v50, 0x0

    const/16 v51, 0x0

    const/16 v52, 0x0

    const/16 v53, 0x0

    const/16 v54, 0x0

    const/16 v55, 0x0

    const/16 v56, 0x0

    const/16 v57, 0x0

    const/16 v58, 0x0

    const/16 v59, 0x0

    const/16 v60, 0x0

    const v61, -0x10001

    const v62, 0x3ffffff

    const/16 v63, 0x0

    move-object/from16 v19, p1

    invoke-direct/range {v2 .. v63}, Lco/uk/getmondo/api/model/tracking/Data;-><init>(Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Boolean;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IILkotlin/d/b/i;)V

    move-object/from16 v0, p0

    move-object/from16 v1, v64

    invoke-virtual {v0, v1, v2}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->a(Ljava/lang/String;Lco/uk/getmondo/api/model/tracking/Data;)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v2

    return-object v2
.end method

.method public final p()Lco/uk/getmondo/api/model/tracking/Impression;
    .locals 1

    .prologue
    .line 196
    check-cast p0, Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    const-string v0, "topup.share-sheet.show"

    invoke-virtual {p0, v0}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->n(Ljava/lang/String;)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    return-object v0
.end method

.method public final p(Ljava/lang/String;)Lco/uk/getmondo/api/model/tracking/Impression;
    .locals 65

    .prologue
    const-string v2, "selectedNameType"

    move-object/from16 v0, p1

    invoke-static {v0, v2}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 825
    check-cast p0, Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    const-string v64, "signup-card-details-confirmation.tap"

    new-instance v2, Lco/uk/getmondo/api/model/tracking/Data;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    const/16 v17, 0x0

    const/16 v18, 0x0

    const/16 v19, 0x0

    const/16 v20, 0x0

    const/16 v21, 0x0

    const/16 v22, 0x0

    const/16 v23, 0x0

    const/16 v24, 0x0

    const/16 v25, 0x0

    const/16 v26, 0x0

    const/16 v27, 0x0

    const/16 v28, 0x0

    const/16 v29, 0x0

    const/16 v30, 0x0

    const/16 v31, 0x0

    const/16 v32, 0x0

    const/16 v33, 0x0

    const/16 v34, 0x0

    const/16 v35, 0x0

    const/16 v36, 0x0

    const/16 v37, 0x0

    const/16 v38, 0x0

    const/16 v39, 0x0

    const/16 v40, 0x0

    const/16 v41, 0x0

    const/16 v42, 0x0

    const/16 v43, 0x0

    const/16 v44, 0x0

    const/16 v45, 0x0

    const/16 v46, 0x0

    const/16 v47, 0x0

    const/16 v48, 0x0

    const/16 v49, 0x0

    const/16 v50, 0x0

    const/16 v51, 0x0

    const/16 v52, 0x0

    const/16 v53, 0x0

    const/16 v54, 0x0

    const/16 v55, 0x0

    const/16 v56, 0x0

    const/16 v57, 0x0

    const/16 v58, 0x0

    const/16 v60, 0x0

    const/16 v61, -0x1

    const v62, 0x2ffffff

    const/16 v63, 0x0

    move-object/from16 v59, p1

    invoke-direct/range {v2 .. v63}, Lco/uk/getmondo/api/model/tracking/Data;-><init>(Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Boolean;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IILkotlin/d/b/i;)V

    move-object/from16 v0, p0

    move-object/from16 v1, v64

    invoke-virtual {v0, v1, v2}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->a(Ljava/lang/String;Lco/uk/getmondo/api/model/tracking/Data;)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v2

    return-object v2
.end method

.method public final q()Lco/uk/getmondo/api/model/tracking/Impression;
    .locals 1

    .prologue
    .line 200
    check-cast p0, Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    const-string v0, "topup-confirm.show"

    invoke-virtual {p0, v0}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->n(Ljava/lang/String;)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    return-object v0
.end method

.method public final r()Lco/uk/getmondo/api/model/tracking/Impression;
    .locals 1

    .prologue
    .line 204
    check-cast p0, Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    const-string v0, "topup-new-card.show"

    invoke-virtual {p0, v0}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->n(Ljava/lang/String;)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    return-object v0
.end method

.method public final s()Lco/uk/getmondo/api/model/tracking/Impression;
    .locals 1

    .prologue
    .line 257
    check-cast p0, Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    const-string v0, "card.show"

    invoke-virtual {p0, v0}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->n(Ljava/lang/String;)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    return-object v0
.end method

.method public final t()Lco/uk/getmondo/api/model/tracking/Impression;
    .locals 1

    .prologue
    .line 261
    check-cast p0, Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    const-string v0, "freeze-card.tap"

    invoke-virtual {p0, v0}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->n(Ljava/lang/String;)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    return-object v0
.end method

.method public final u()Lco/uk/getmondo/api/model/tracking/Impression;
    .locals 1

    .prologue
    .line 265
    check-cast p0, Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    const-string v0, "defrost-card.tap"

    invoke-virtual {p0, v0}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->n(Ljava/lang/String;)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    return-object v0
.end method

.method public final v()Lco/uk/getmondo/api/model/tracking/Impression;
    .locals 1

    .prologue
    .line 281
    check-cast p0, Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    const-string v0, "add-note.tap"

    invoke-virtual {p0, v0}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->n(Ljava/lang/String;)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    return-object v0
.end method

.method public final w()Lco/uk/getmondo/api/model/tracking/Impression;
    .locals 1

    .prologue
    .line 285
    check-cast p0, Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    const-string v0, "take-photo.tap"

    invoke-virtual {p0, v0}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->n(Ljava/lang/String;)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    return-object v0
.end method

.method public final x()Lco/uk/getmondo/api/model/tracking/Impression;
    .locals 1

    .prologue
    .line 289
    check-cast p0, Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    const-string v0, "select-photo.tap"

    invoke-virtual {p0, v0}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->n(Ljava/lang/String;)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    return-object v0
.end method

.method public final y()Lco/uk/getmondo/api/model/tracking/Impression;
    .locals 1

    .prologue
    .line 297
    check-cast p0, Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    const-string v0, "signup.address-picker.show"

    invoke-virtual {p0, v0}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->n(Ljava/lang/String;)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    return-object v0
.end method

.method public final z()Lco/uk/getmondo/api/model/tracking/Impression;
    .locals 1

    .prologue
    .line 301
    check-cast p0, Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    const-string v0, "signup.address-manual.show"

    invoke-virtual {p0, v0}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->n(Ljava/lang/String;)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    return-object v0
.end method
