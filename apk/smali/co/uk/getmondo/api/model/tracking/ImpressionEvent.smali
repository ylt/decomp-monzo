.class public final Lco/uk/getmondo/api/model/tracking/ImpressionEvent;
.super Ljava/lang/Object;
.source "ImpressionEvent.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lco/uk/getmondo/api/model/tracking/ImpressionEvent$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u000b\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0002\u0008\u0003\u0008\u0086\u0008\u0018\u0000 \u00162\u00020\u0001:\u0001\u0016B\u0019\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\n\u0008\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u00a2\u0006\u0002\u0010\u0006J\t\u0010\r\u001a\u00020\u0003H\u00c6\u0003J\u000b\u0010\u000e\u001a\u0004\u0018\u00010\u0005H\u00c6\u0003J\u001f\u0010\u000f\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\n\u0008\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u0005H\u00c6\u0001J\u0013\u0010\u0010\u001a\u00020\u00112\u0008\u0010\u0012\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u0010\u0013\u001a\u00020\u0014H\u00d6\u0001J\t\u0010\u0015\u001a\u00020\u0003H\u00d6\u0001R\u0013\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0007\u0010\u0008R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\t\u0010\nR\u0011\u0010\u000b\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000c\u0010\n\u00a8\u0006\u0017"
    }
    d2 = {
        "Lco/uk/getmondo/api/model/tracking/ImpressionEvent;",
        "",
        "name",
        "",
        "data",
        "Lco/uk/getmondo/api/model/tracking/Data;",
        "(Ljava/lang/String;Lco/uk/getmondo/api/model/tracking/Data;)V",
        "getData",
        "()Lco/uk/getmondo/api/model/tracking/Data;",
        "getName",
        "()Ljava/lang/String;",
        "timestamp",
        "getTimestamp",
        "component1",
        "component2",
        "copy",
        "equals",
        "",
        "other",
        "hashCode",
        "",
        "toString",
        "Companion",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# static fields
.field public static final Companion:Lco/uk/getmondo/api/model/tracking/ImpressionEvent$Companion;

.field private static final DATE_FORMATTER:Ljava/text/SimpleDateFormat;


# instance fields
.field private final data:Lco/uk/getmondo/api/model/tracking/Data;

.field private final name:Ljava/lang/String;

.field private final timestamp:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    new-instance v0, Lco/uk/getmondo/api/model/tracking/ImpressionEvent$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lco/uk/getmondo/api/model/tracking/ImpressionEvent$Companion;-><init>(Lkotlin/d/b/i;)V

    sput-object v0, Lco/uk/getmondo/api/model/tracking/ImpressionEvent;->Companion:Lco/uk/getmondo/api/model/tracking/ImpressionEvent$Companion;

    .line 13
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "yyyy-MM-dd\'T\'HH:mm:ssZ"

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    sput-object v0, Lco/uk/getmondo/api/model/tracking/ImpressionEvent;->DATE_FORMATTER:Ljava/text/SimpleDateFormat;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lco/uk/getmondo/api/model/tracking/Data;)V
    .locals 2

    .prologue
    const-string v0, "name"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lco/uk/getmondo/api/model/tracking/ImpressionEvent;->name:Ljava/lang/String;

    iput-object p2, p0, Lco/uk/getmondo/api/model/tracking/ImpressionEvent;->data:Lco/uk/getmondo/api/model/tracking/Data;

    .line 10
    sget-object v0, Lco/uk/getmondo/api/model/tracking/ImpressionEvent;->Companion:Lco/uk/getmondo/api/model/tracking/ImpressionEvent$Companion;

    invoke-static {v0}, Lco/uk/getmondo/api/model/tracking/ImpressionEvent$Companion;->a(Lco/uk/getmondo/api/model/tracking/ImpressionEvent$Companion;)Ljava/text/SimpleDateFormat;

    move-result-object v0

    new-instance v1, Ljava/util/Date;

    invoke-direct {v1}, Ljava/util/Date;-><init>()V

    invoke-virtual {v0, v1}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "ImpressionEvent.DATE_FORMATTER.format(Date())"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v0, p0, Lco/uk/getmondo/api/model/tracking/ImpressionEvent;->timestamp:Ljava/lang/String;

    return-void
.end method

.method public synthetic constructor <init>(Ljava/lang/String;Lco/uk/getmondo/api/model/tracking/Data;ILkotlin/d/b/i;)V
    .locals 1

    .prologue
    and-int/lit8 v0, p3, 0x2

    if-eqz v0, :cond_0

    .line 8
    const/4 v0, 0x0

    check-cast v0, Lco/uk/getmondo/api/model/tracking/Data;

    :goto_0
    invoke-direct {p0, p1, v0}, Lco/uk/getmondo/api/model/tracking/ImpressionEvent;-><init>(Ljava/lang/String;Lco/uk/getmondo/api/model/tracking/Data;)V

    return-void

    :cond_0
    move-object v0, p2

    goto :goto_0
.end method

.method public static final synthetic a()Ljava/text/SimpleDateFormat;
    .locals 1

    .prologue
    .line 6
    sget-object v0, Lco/uk/getmondo/api/model/tracking/ImpressionEvent;->DATE_FORMATTER:Ljava/text/SimpleDateFormat;

    return-object v0
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_0

    instance-of v0, p1, Lco/uk/getmondo/api/model/tracking/ImpressionEvent;

    if-eqz v0, :cond_1

    check-cast p1, Lco/uk/getmondo/api/model/tracking/ImpressionEvent;

    iget-object v0, p0, Lco/uk/getmondo/api/model/tracking/ImpressionEvent;->name:Ljava/lang/String;

    iget-object v1, p1, Lco/uk/getmondo/api/model/tracking/ImpressionEvent;->name:Ljava/lang/String;

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lco/uk/getmondo/api/model/tracking/ImpressionEvent;->data:Lco/uk/getmondo/api/model/tracking/Data;

    iget-object v1, p1, Lco/uk/getmondo/api/model/tracking/ImpressionEvent;->data:Lco/uk/getmondo/api/model/tracking/Data;

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    const/4 v1, 0x0

    iget-object v0, p0, Lco/uk/getmondo/api/model/tracking/ImpressionEvent;->name:Ljava/lang/String;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lco/uk/getmondo/api/model/tracking/ImpressionEvent;->data:Lco/uk/getmondo/api/model/tracking/Data;

    if-eqz v2, :cond_0

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_0
    add-int/2addr v0, v1

    return v0

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "ImpressionEvent(name="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/api/model/tracking/ImpressionEvent;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", data="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/api/model/tracking/ImpressionEvent;->data:Lco/uk/getmondo/api/model/tracking/Data;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
