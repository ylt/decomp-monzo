.class public final enum Lco/uk/getmondo/api/model/tracking/a;
.super Ljava/lang/Enum;
.source "ShareAnalyticType.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lco/uk/getmondo/api/model/tracking/a;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lco/uk/getmondo/api/model/tracking/a;

.field public static final enum GOLDEN_TICKET:Lco/uk/getmondo/api/model/tracking/a;

.field public static final enum INVITE_CONTACT_DISCOVERY:Lco/uk/getmondo/api/model/tracking/a;

.field public static final enum REQUEST_MONEY:Lco/uk/getmondo/api/model/tracking/a;

.field public static final enum REQUEST_MONEY_CUSTOMISE:Lco/uk/getmondo/api/model/tracking/a;

.field public static final enum TOP_UP_SHARE:Lco/uk/getmondo/api/model/tracking/a;

.field public static final enum WAITING_LIST_SHARE:Lco/uk/getmondo/api/model/tracking/a;

.field public static final enum WAITING_LIST_WEB_EVENT:Lco/uk/getmondo/api/model/tracking/a;


# instance fields
.field public final value:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 4
    new-instance v0, Lco/uk/getmondo/api/model/tracking/a;

    const-string v1, "WAITING_LIST_SHARE"

    const-string v2, "wl.share-sheet.share"

    invoke-direct {v0, v1, v4, v2}, Lco/uk/getmondo/api/model/tracking/a;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lco/uk/getmondo/api/model/tracking/a;->WAITING_LIST_SHARE:Lco/uk/getmondo/api/model/tracking/a;

    .line 5
    new-instance v0, Lco/uk/getmondo/api/model/tracking/a;

    const-string v1, "WAITING_LIST_WEB_EVENT"

    const-string v2, "wl.webview-share-sheet.share"

    invoke-direct {v0, v1, v5, v2}, Lco/uk/getmondo/api/model/tracking/a;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lco/uk/getmondo/api/model/tracking/a;->WAITING_LIST_WEB_EVENT:Lco/uk/getmondo/api/model/tracking/a;

    .line 6
    new-instance v0, Lco/uk/getmondo/api/model/tracking/a;

    const-string v1, "GOLDEN_TICKET"

    const-string v2, "golden-ticket.share-sheet.share"

    invoke-direct {v0, v1, v6, v2}, Lco/uk/getmondo/api/model/tracking/a;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lco/uk/getmondo/api/model/tracking/a;->GOLDEN_TICKET:Lco/uk/getmondo/api/model/tracking/a;

    .line 7
    new-instance v0, Lco/uk/getmondo/api/model/tracking/a;

    const-string v1, "INVITE_CONTACT_DISCOVERY"

    const-string v2, "p2p.invite.share-sheet.share"

    invoke-direct {v0, v1, v7, v2}, Lco/uk/getmondo/api/model/tracking/a;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lco/uk/getmondo/api/model/tracking/a;->INVITE_CONTACT_DISCOVERY:Lco/uk/getmondo/api/model/tracking/a;

    .line 8
    new-instance v0, Lco/uk/getmondo/api/model/tracking/a;

    const-string v1, "REQUEST_MONEY_CUSTOMISE"

    const-string v2, "request.custom-share-sheet.share"

    invoke-direct {v0, v1, v8, v2}, Lco/uk/getmondo/api/model/tracking/a;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lco/uk/getmondo/api/model/tracking/a;->REQUEST_MONEY_CUSTOMISE:Lco/uk/getmondo/api/model/tracking/a;

    .line 9
    new-instance v0, Lco/uk/getmondo/api/model/tracking/a;

    const-string v1, "REQUEST_MONEY"

    const/4 v2, 0x5

    const-string v3, "request.share-sheet.share"

    invoke-direct {v0, v1, v2, v3}, Lco/uk/getmondo/api/model/tracking/a;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lco/uk/getmondo/api/model/tracking/a;->REQUEST_MONEY:Lco/uk/getmondo/api/model/tracking/a;

    .line 10
    new-instance v0, Lco/uk/getmondo/api/model/tracking/a;

    const-string v1, "TOP_UP_SHARE"

    const/4 v2, 0x6

    const-string v3, "topup.share-sheet.share"

    invoke-direct {v0, v1, v2, v3}, Lco/uk/getmondo/api/model/tracking/a;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lco/uk/getmondo/api/model/tracking/a;->TOP_UP_SHARE:Lco/uk/getmondo/api/model/tracking/a;

    .line 3
    const/4 v0, 0x7

    new-array v0, v0, [Lco/uk/getmondo/api/model/tracking/a;

    sget-object v1, Lco/uk/getmondo/api/model/tracking/a;->WAITING_LIST_SHARE:Lco/uk/getmondo/api/model/tracking/a;

    aput-object v1, v0, v4

    sget-object v1, Lco/uk/getmondo/api/model/tracking/a;->WAITING_LIST_WEB_EVENT:Lco/uk/getmondo/api/model/tracking/a;

    aput-object v1, v0, v5

    sget-object v1, Lco/uk/getmondo/api/model/tracking/a;->GOLDEN_TICKET:Lco/uk/getmondo/api/model/tracking/a;

    aput-object v1, v0, v6

    sget-object v1, Lco/uk/getmondo/api/model/tracking/a;->INVITE_CONTACT_DISCOVERY:Lco/uk/getmondo/api/model/tracking/a;

    aput-object v1, v0, v7

    sget-object v1, Lco/uk/getmondo/api/model/tracking/a;->REQUEST_MONEY_CUSTOMISE:Lco/uk/getmondo/api/model/tracking/a;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lco/uk/getmondo/api/model/tracking/a;->REQUEST_MONEY:Lco/uk/getmondo/api/model/tracking/a;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lco/uk/getmondo/api/model/tracking/a;->TOP_UP_SHARE:Lco/uk/getmondo/api/model/tracking/a;

    aput-object v2, v0, v1

    sput-object v0, Lco/uk/getmondo/api/model/tracking/a;->$VALUES:[Lco/uk/getmondo/api/model/tracking/a;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 14
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 15
    iput-object p3, p0, Lco/uk/getmondo/api/model/tracking/a;->value:Ljava/lang/String;

    .line 16
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lco/uk/getmondo/api/model/tracking/a;
    .locals 1

    .prologue
    .line 3
    const-class v0, Lco/uk/getmondo/api/model/tracking/a;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/api/model/tracking/a;

    return-object v0
.end method

.method public static values()[Lco/uk/getmondo/api/model/tracking/a;
    .locals 1

    .prologue
    .line 3
    sget-object v0, Lco/uk/getmondo/api/model/tracking/a;->$VALUES:[Lco/uk/getmondo/api/model/tracking/a;

    invoke-virtual {v0}, [Lco/uk/getmondo/api/model/tracking/a;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lco/uk/getmondo/api/model/tracking/a;

    return-object v0
.end method
