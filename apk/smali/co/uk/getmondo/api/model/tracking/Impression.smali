.class public final Lco/uk/getmondo/api/model/tracking/Impression;
.super Ljava/lang/Object;
.source "Impression.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lco/uk/getmondo/api/model/tracking/Impression$KycFrom;,
        Lco/uk/getmondo/api/model/tracking/Impression$OpenedCommunityFrom;,
        Lco/uk/getmondo/api/model/tracking/Impression$TopUpTapFrom;,
        Lco/uk/getmondo/api/model/tracking/Impression$PaymentFlowFrom;,
        Lco/uk/getmondo/api/model/tracking/Impression$TopUpSuccessType;,
        Lco/uk/getmondo/api/model/tracking/Impression$FingerprintSupportType;,
        Lco/uk/getmondo/api/model/tracking/Impression$CustomiseMonzoMeLinkFrom;,
        Lco/uk/getmondo/api/model/tracking/Impression$IntercomFrom;,
        Lco/uk/getmondo/api/model/tracking/Impression$PinFrom;,
        Lco/uk/getmondo/api/model/tracking/Impression$HelpOutcome;,
        Lco/uk/getmondo/api/model/tracking/Impression$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u00004\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010!\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0010\u0002\n\u0002\u0008\u0004\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u000e\u0008\u0086\u0008\u0018\u0000 \u00142\u00020\u0001:\r\u0014\u0015\u0016\u0017\u0018\u0019\u001a\u001b\u001c\u001d\u001e\u001f B\u0015\u0012\u000e\u0008\u0002\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003\u00a2\u0006\u0002\u0010\u0005J\u000e\u0010\u0008\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\u0004J\u000f\u0010\u000b\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003H\u00c6\u0003J\u0019\u0010\u000c\u001a\u00020\u00002\u000e\u0008\u0002\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003H\u00c6\u0001J\u0013\u0010\r\u001a\u00020\u000e2\u0008\u0010\u000f\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u0010\u0010\u001a\u00020\u0011H\u00d6\u0001J\u0008\u0010\u0012\u001a\u00020\u0013H\u0016R\u0017\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0006\u0010\u0007\u00a8\u0006!"
    }
    d2 = {
        "Lco/uk/getmondo/api/model/tracking/Impression;",
        "",
        "events",
        "",
        "Lco/uk/getmondo/api/model/tracking/ImpressionEvent;",
        "(Ljava/util/List;)V",
        "getEvents",
        "()Ljava/util/List;",
        "add",
        "",
        "event",
        "component1",
        "copy",
        "equals",
        "",
        "other",
        "hashCode",
        "",
        "toString",
        "",
        "Companion",
        "CustomiseMonzoMeLinkFrom",
        "FingerprintSupportType",
        "HelpOutcome",
        "IntercomFrom",
        "InvestIntroFrom",
        "KycFrom",
        "OpenedCommunityFrom",
        "OpenedFaqsFrom",
        "PaymentFlowFrom",
        "PinFrom",
        "TopUpSuccessType",
        "TopUpTapFrom",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# static fields
.field public static final Companion:Lco/uk/getmondo/api/model/tracking/Impression$Companion;

# The value of this static final field might be set in the static constructor
.field private static final EVENT_WAILIST_SHOW:Ljava/lang/String; = "wl.waitinglist.show"


# instance fields
.field private final events:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lco/uk/getmondo/api/model/tracking/ImpressionEvent;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    new-instance v0, Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;-><init>(Lkotlin/d/b/i;)V

    sput-object v0, Lco/uk/getmondo/api/model/tracking/Impression;->Companion:Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    .line 100
    const-string v0, "wl.waitinglist.show"

    sput-object v0, Lco/uk/getmondo/api/model/tracking/Impression;->EVENT_WAILIST_SHOW:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    const/4 v0, 0x1

    invoke-direct {p0, v1, v0, v1}, Lco/uk/getmondo/api/model/tracking/Impression;-><init>(Ljava/util/List;ILkotlin/d/b/i;)V

    return-void
.end method

.method public constructor <init>(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lco/uk/getmondo/api/model/tracking/ImpressionEvent;",
            ">;)V"
        }
    .end annotation

    .prologue
    const-string v0, "events"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lco/uk/getmondo/api/model/tracking/Impression;->events:Ljava/util/List;

    return-void
.end method

.method public synthetic constructor <init>(Ljava/util/List;ILkotlin/d/b/i;)V
    .locals 1

    .prologue
    and-int/lit8 v0, p2, 0x1

    if-eqz v0, :cond_0

    .line 11
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    check-cast v0, Ljava/util/List;

    :goto_0
    invoke-direct {p0, v0}, Lco/uk/getmondo/api/model/tracking/Impression;-><init>(Ljava/util/List;)V

    return-void

    :cond_0
    move-object v0, p1

    goto :goto_0
.end method

.method public static final A()Lco/uk/getmondo/api/model/tracking/Impression;
    .locals 1

    sget-object v0, Lco/uk/getmondo/api/model/tracking/Impression;->Companion:Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    invoke-virtual {v0}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->I()Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    return-object v0
.end method

.method public static final B()Lco/uk/getmondo/api/model/tracking/Impression;
    .locals 1

    sget-object v0, Lco/uk/getmondo/api/model/tracking/Impression;->Companion:Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    invoke-virtual {v0}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->J()Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    return-object v0
.end method

.method public static final C()Lco/uk/getmondo/api/model/tracking/Impression;
    .locals 1

    sget-object v0, Lco/uk/getmondo/api/model/tracking/Impression;->Companion:Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    invoke-virtual {v0}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->K()Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    return-object v0
.end method

.method public static final D()Lco/uk/getmondo/api/model/tracking/Impression;
    .locals 1

    sget-object v0, Lco/uk/getmondo/api/model/tracking/Impression;->Companion:Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    invoke-virtual {v0}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->L()Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    return-object v0
.end method

.method public static final E()Lco/uk/getmondo/api/model/tracking/Impression;
    .locals 1

    sget-object v0, Lco/uk/getmondo/api/model/tracking/Impression;->Companion:Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    invoke-virtual {v0}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->M()Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    return-object v0
.end method

.method public static final F()Lco/uk/getmondo/api/model/tracking/Impression;
    .locals 1

    sget-object v0, Lco/uk/getmondo/api/model/tracking/Impression;->Companion:Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    invoke-virtual {v0}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->O()Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    return-object v0
.end method

.method public static final G()Lco/uk/getmondo/api/model/tracking/Impression;
    .locals 1

    sget-object v0, Lco/uk/getmondo/api/model/tracking/Impression;->Companion:Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    invoke-virtual {v0}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->P()Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    return-object v0
.end method

.method public static final H()Lco/uk/getmondo/api/model/tracking/Impression;
    .locals 1

    sget-object v0, Lco/uk/getmondo/api/model/tracking/Impression;->Companion:Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    invoke-virtual {v0}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->Q()Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    return-object v0
.end method

.method public static final I()Lco/uk/getmondo/api/model/tracking/Impression;
    .locals 1

    sget-object v0, Lco/uk/getmondo/api/model/tracking/Impression;->Companion:Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    invoke-virtual {v0}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->R()Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    return-object v0
.end method

.method public static final J()Lco/uk/getmondo/api/model/tracking/Impression;
    .locals 1

    sget-object v0, Lco/uk/getmondo/api/model/tracking/Impression;->Companion:Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    invoke-virtual {v0}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->W()Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    return-object v0
.end method

.method public static final K()Lco/uk/getmondo/api/model/tracking/Impression;
    .locals 1

    sget-object v0, Lco/uk/getmondo/api/model/tracking/Impression;->Companion:Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    invoke-virtual {v0}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->X()Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    return-object v0
.end method

.method public static final L()Lco/uk/getmondo/api/model/tracking/Impression;
    .locals 1

    sget-object v0, Lco/uk/getmondo/api/model/tracking/Impression;->Companion:Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    invoke-virtual {v0}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->Y()Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    return-object v0
.end method

.method public static final M()Lco/uk/getmondo/api/model/tracking/Impression;
    .locals 1

    sget-object v0, Lco/uk/getmondo/api/model/tracking/Impression;->Companion:Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    invoke-virtual {v0}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->ac()Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    return-object v0
.end method

.method public static final N()Lco/uk/getmondo/api/model/tracking/Impression;
    .locals 1

    sget-object v0, Lco/uk/getmondo/api/model/tracking/Impression;->Companion:Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    invoke-virtual {v0}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->ad()Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    return-object v0
.end method

.method public static final O()Lco/uk/getmondo/api/model/tracking/Impression;
    .locals 1

    sget-object v0, Lco/uk/getmondo/api/model/tracking/Impression;->Companion:Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    invoke-virtual {v0}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->ae()Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    return-object v0
.end method

.method public static final P()Lco/uk/getmondo/api/model/tracking/Impression;
    .locals 1

    sget-object v0, Lco/uk/getmondo/api/model/tracking/Impression;->Companion:Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    invoke-virtual {v0}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->af()Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    return-object v0
.end method

.method public static final Q()Lco/uk/getmondo/api/model/tracking/Impression;
    .locals 1

    sget-object v0, Lco/uk/getmondo/api/model/tracking/Impression;->Companion:Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    invoke-virtual {v0}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->ag()Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    return-object v0
.end method

.method public static final R()Lco/uk/getmondo/api/model/tracking/Impression;
    .locals 1

    sget-object v0, Lco/uk/getmondo/api/model/tracking/Impression;->Companion:Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    invoke-virtual {v0}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->ar()Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    return-object v0
.end method

.method public static final S()Lco/uk/getmondo/api/model/tracking/Impression;
    .locals 1

    sget-object v0, Lco/uk/getmondo/api/model/tracking/Impression;->Companion:Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    invoke-virtual {v0}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->as()Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    return-object v0
.end method

.method public static final T()Lco/uk/getmondo/api/model/tracking/Impression;
    .locals 1

    sget-object v0, Lco/uk/getmondo/api/model/tracking/Impression;->Companion:Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    invoke-virtual {v0}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->at()Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    return-object v0
.end method

.method public static final U()Lco/uk/getmondo/api/model/tracking/Impression;
    .locals 1

    sget-object v0, Lco/uk/getmondo/api/model/tracking/Impression;->Companion:Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    invoke-virtual {v0}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->au()Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    return-object v0
.end method

.method public static final V()Lco/uk/getmondo/api/model/tracking/Impression;
    .locals 1

    sget-object v0, Lco/uk/getmondo/api/model/tracking/Impression;->Companion:Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    invoke-virtual {v0}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->av()Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    return-object v0
.end method

.method public static final W()Lco/uk/getmondo/api/model/tracking/Impression;
    .locals 1

    sget-object v0, Lco/uk/getmondo/api/model/tracking/Impression;->Companion:Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    invoke-virtual {v0}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->aw()Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    return-object v0
.end method

.method public static final X()Lco/uk/getmondo/api/model/tracking/Impression;
    .locals 1

    sget-object v0, Lco/uk/getmondo/api/model/tracking/Impression;->Companion:Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    invoke-virtual {v0}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->ax()Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    return-object v0
.end method

.method public static final Y()Lco/uk/getmondo/api/model/tracking/Impression;
    .locals 1

    sget-object v0, Lco/uk/getmondo/api/model/tracking/Impression;->Companion:Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    invoke-virtual {v0}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->ay()Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    return-object v0
.end method

.method public static final a(I)Lco/uk/getmondo/api/model/tracking/Impression;
    .locals 1

    sget-object v0, Lco/uk/getmondo/api/model/tracking/Impression;->Companion:Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    invoke-virtual {v0, p0}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->a(I)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    return-object v0
.end method

.method public static final a(ILjava/lang/String;Ljava/lang/String;Lco/uk/getmondo/api/model/b;)Lco/uk/getmondo/api/model/tracking/Impression;
    .locals 1

    const-string v0, "path"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v0, Lco/uk/getmondo/api/model/tracking/Impression;->Companion:Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    invoke-virtual {v0, p0, p1, p2, p3}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->a(ILjava/lang/String;Ljava/lang/String;Lco/uk/getmondo/api/model/b;)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    return-object v0
.end method

.method public static final a(Lco/uk/getmondo/api/model/tracking/Impression$CustomiseMonzoMeLinkFrom;)Lco/uk/getmondo/api/model/tracking/Impression;
    .locals 1

    const-string v0, "entryPoint"

    invoke-static {p0, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v0, Lco/uk/getmondo/api/model/tracking/Impression;->Companion:Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    invoke-virtual {v0, p0}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->a(Lco/uk/getmondo/api/model/tracking/Impression$CustomiseMonzoMeLinkFrom;)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    return-object v0
.end method

.method public static final a(Lco/uk/getmondo/api/model/tracking/Impression$FingerprintSupportType;IZ)Lco/uk/getmondo/api/model/tracking/Impression;
    .locals 1

    const-string v0, "fingerprintSupportType"

    invoke-static {p0, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v0, Lco/uk/getmondo/api/model/tracking/Impression;->Companion:Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    invoke-virtual {v0, p0, p1, p2}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->a(Lco/uk/getmondo/api/model/tracking/Impression$FingerprintSupportType;IZ)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    return-object v0
.end method

.method public static final a(Lco/uk/getmondo/api/model/tracking/Impression$IntercomFrom;)Lco/uk/getmondo/api/model/tracking/Impression;
    .locals 6

    const/4 v2, 0x0

    sget-object v0, Lco/uk/getmondo/api/model/tracking/Impression;->Companion:Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    const/4 v4, 0x6

    move-object v1, p0

    move-object v3, v2

    move-object v5, v2

    invoke-static/range {v0 .. v5}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->a(Lco/uk/getmondo/api/model/tracking/Impression$Companion;Lco/uk/getmondo/api/model/tracking/Impression$IntercomFrom;Ljava/lang/String;Ljava/lang/String;ILjava/lang/Object;)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    return-object v0
.end method

.method public static final a(Lco/uk/getmondo/api/model/tracking/Impression$PaymentFlowFrom;)Lco/uk/getmondo/api/model/tracking/Impression;
    .locals 1

    const-string v0, "entryPoint"

    invoke-static {p0, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v0, Lco/uk/getmondo/api/model/tracking/Impression;->Companion:Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    invoke-virtual {v0, p0}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->a(Lco/uk/getmondo/api/model/tracking/Impression$PaymentFlowFrom;)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    return-object v0
.end method

.method public static final a(Lco/uk/getmondo/api/model/tracking/Impression$TopUpSuccessType;)Lco/uk/getmondo/api/model/tracking/Impression;
    .locals 1

    const-string v0, "type"

    invoke-static {p0, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v0, Lco/uk/getmondo/api/model/tracking/Impression;->Companion:Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    invoke-virtual {v0, p0}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->a(Lco/uk/getmondo/api/model/tracking/Impression$TopUpSuccessType;)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    return-object v0
.end method

.method public static final a(Lco/uk/getmondo/api/model/tracking/Impression$TopUpTapFrom;)Lco/uk/getmondo/api/model/tracking/Impression;
    .locals 1

    const-string v0, "cameFrom"

    invoke-static {p0, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v0, Lco/uk/getmondo/api/model/tracking/Impression;->Companion:Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    invoke-virtual {v0, p0}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->a(Lco/uk/getmondo/api/model/tracking/Impression$TopUpTapFrom;)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    return-object v0
.end method

.method public static final a(Lco/uk/getmondo/d/h;)Lco/uk/getmondo/api/model/tracking/Impression;
    .locals 1

    const-string v0, "category"

    invoke-static {p0, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v0, Lco/uk/getmondo/api/model/tracking/Impression;->Companion:Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    invoke-virtual {v0, p0}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->a(Lco/uk/getmondo/d/h;)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    return-object v0
.end method

.method public static final a(Lco/uk/getmondo/d/h;Lorg/threeten/bp/YearMonth;)Lco/uk/getmondo/api/model/tracking/Impression;
    .locals 1

    const-string v0, "category"

    invoke-static {p0, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "yearMonth"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v0, Lco/uk/getmondo/api/model/tracking/Impression;->Companion:Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    invoke-virtual {v0, p0, p1}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->a(Lco/uk/getmondo/d/h;Lorg/threeten/bp/YearMonth;)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    return-object v0
.end method

.method public static final a(Lco/uk/getmondo/feed/SpendingReportFeedbackDialogFragment$a;Lorg/threeten/bp/YearMonth;)Lco/uk/getmondo/api/model/tracking/Impression;
    .locals 1

    const-string v0, "type"

    invoke-static {p0, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "yearMonth"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v0, Lco/uk/getmondo/api/model/tracking/Impression;->Companion:Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    invoke-virtual {v0, p0, p1}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->a(Lco/uk/getmondo/feed/SpendingReportFeedbackDialogFragment$a;Lorg/threeten/bp/YearMonth;)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    return-object v0
.end method

.method public static final a(Lco/uk/getmondo/signup/identity_verification/sdd/j;)Lco/uk/getmondo/api/model/tracking/Impression;
    .locals 1

    const-string v0, "sddUpgradeLevelType"

    invoke-static {p0, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v0, Lco/uk/getmondo/api/model/tracking/Impression;->Companion:Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    invoke-virtual {v0, p0}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->a(Lco/uk/getmondo/signup/identity_verification/sdd/j;)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    return-object v0
.end method

.method public static final a(Ljava/lang/String;)Lco/uk/getmondo/api/model/tracking/Impression;
    .locals 1

    const-string v0, "url"

    invoke-static {p0, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v0, Lco/uk/getmondo/api/model/tracking/Impression;->Companion:Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    invoke-virtual {v0, p0}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->a(Ljava/lang/String;)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    return-object v0
.end method

.method public static final a(Ljava/lang/String;Ljava/lang/String;)Lco/uk/getmondo/api/model/tracking/Impression;
    .locals 1

    const-string v0, "redirectUrl"

    invoke-static {p0, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "errorType"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v0, Lco/uk/getmondo/api/model/tracking/Impression;->Companion:Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    invoke-virtual {v0, p0, p1}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->b(Ljava/lang/String;Ljava/lang/String;)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    return-object v0
.end method

.method public static final a(Ljava/lang/String;Z)Lco/uk/getmondo/api/model/tracking/Impression;
    .locals 1

    const-string v0, "identityDocumentType"

    invoke-static {p0, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v0, Lco/uk/getmondo/api/model/tracking/Impression;->Companion:Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    invoke-virtual {v0, p0, p1}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->a(Ljava/lang/String;Z)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    return-object v0
.end method

.method public static final a(Lorg/threeten/bp/YearMonth;)Lco/uk/getmondo/api/model/tracking/Impression;
    .locals 1

    const-string v0, "yearMonth"

    invoke-static {p0, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v0, Lco/uk/getmondo/api/model/tracking/Impression;->Companion:Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    invoke-virtual {v0, p0}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->a(Lorg/threeten/bp/YearMonth;)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    return-object v0
.end method

.method public static final a(Z)Lco/uk/getmondo/api/model/tracking/Impression;
    .locals 1

    sget-object v0, Lco/uk/getmondo/api/model/tracking/Impression;->Companion:Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    invoke-virtual {v0, p0}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->a(Z)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    return-object v0
.end method

.method public static final a(ZLjava/lang/Long;Ljava/lang/Long;)Lco/uk/getmondo/api/model/tracking/Impression;
    .locals 1

    sget-object v0, Lco/uk/getmondo/api/model/tracking/Impression;->Companion:Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    invoke-virtual {v0, p0, p1, p2}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->a(ZLjava/lang/Long;Ljava/lang/Long;)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    return-object v0
.end method

.method public static final a(ZLjava/lang/String;)Lco/uk/getmondo/api/model/tracking/Impression;
    .locals 1

    const-string v0, "url"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v0, Lco/uk/getmondo/api/model/tracking/Impression;->Companion:Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    invoke-virtual {v0, p0, p1}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->a(ZLjava/lang/String;)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    return-object v0
.end method

.method public static final synthetic a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 11
    sget-object v0, Lco/uk/getmondo/api/model/tracking/Impression;->EVENT_WAILIST_SHOW:Ljava/lang/String;

    return-object v0
.end method

.method public static final b()Lco/uk/getmondo/api/model/tracking/Impression;
    .locals 1

    sget-object v0, Lco/uk/getmondo/api/model/tracking/Impression;->Companion:Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    invoke-virtual {v0}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->b()Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    return-object v0
.end method

.method public static final b(I)Lco/uk/getmondo/api/model/tracking/Impression;
    .locals 1

    sget-object v0, Lco/uk/getmondo/api/model/tracking/Impression;->Companion:Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    invoke-virtual {v0, p0}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->b(I)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    return-object v0
.end method

.method public static final b(ILjava/lang/String;Ljava/lang/String;Lco/uk/getmondo/api/model/b;)Lco/uk/getmondo/api/model/tracking/Impression;
    .locals 1

    const-string v0, "path"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v0, Lco/uk/getmondo/api/model/tracking/Impression;->Companion:Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    invoke-virtual {v0, p0, p1, p2, p3}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->c(ILjava/lang/String;Ljava/lang/String;Lco/uk/getmondo/api/model/b;)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    return-object v0
.end method

.method public static final b(Lco/uk/getmondo/d/h;Lorg/threeten/bp/YearMonth;)Lco/uk/getmondo/api/model/tracking/Impression;
    .locals 1

    const-string v0, "category"

    invoke-static {p0, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "yearMonth"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v0, Lco/uk/getmondo/api/model/tracking/Impression;->Companion:Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    invoke-virtual {v0, p0, p1}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->b(Lco/uk/getmondo/d/h;Lorg/threeten/bp/YearMonth;)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    return-object v0
.end method

.method public static final b(Ljava/lang/String;)Lco/uk/getmondo/api/model/tracking/Impression;
    .locals 1

    const-string v0, "url"

    invoke-static {p0, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v0, Lco/uk/getmondo/api/model/tracking/Impression;->Companion:Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    invoke-virtual {v0, p0}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->b(Ljava/lang/String;)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    return-object v0
.end method

.method public static final b(Ljava/lang/String;Ljava/lang/String;)Lco/uk/getmondo/api/model/tracking/Impression;
    .locals 1

    const-string v0, "analyticName"

    invoke-static {p0, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "appName"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v0, Lco/uk/getmondo/api/model/tracking/Impression;->Companion:Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    invoke-virtual {v0, p0, p1}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->c(Ljava/lang/String;Ljava/lang/String;)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    return-object v0
.end method

.method public static final b(Ljava/lang/String;Z)Lco/uk/getmondo/api/model/tracking/Impression;
    .locals 1

    const-string v0, "reason"

    invoke-static {p0, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v0, Lco/uk/getmondo/api/model/tracking/Impression;->Companion:Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    invoke-virtual {v0, p0, p1}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->b(Ljava/lang/String;Z)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    return-object v0
.end method

.method public static final b(Z)Lco/uk/getmondo/api/model/tracking/Impression;
    .locals 1

    sget-object v0, Lco/uk/getmondo/api/model/tracking/Impression;->Companion:Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    invoke-virtual {v0, p0}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->b(Z)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    return-object v0
.end method

.method public static final c()Lco/uk/getmondo/api/model/tracking/Impression;
    .locals 1

    sget-object v0, Lco/uk/getmondo/api/model/tracking/Impression;->Companion:Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    invoke-virtual {v0}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->c()Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    return-object v0
.end method

.method public static final c(I)Lco/uk/getmondo/api/model/tracking/Impression;
    .locals 1

    sget-object v0, Lco/uk/getmondo/api/model/tracking/Impression;->Companion:Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    invoke-virtual {v0, p0}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->d(I)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    return-object v0
.end method

.method public static final c(Lco/uk/getmondo/d/h;Lorg/threeten/bp/YearMonth;)Lco/uk/getmondo/api/model/tracking/Impression;
    .locals 1

    const-string v0, "category"

    invoke-static {p0, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "yearMonth"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v0, Lco/uk/getmondo/api/model/tracking/Impression;->Companion:Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    invoke-virtual {v0, p0, p1}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->c(Lco/uk/getmondo/d/h;Lorg/threeten/bp/YearMonth;)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    return-object v0
.end method

.method public static final c(Ljava/lang/String;)Lco/uk/getmondo/api/model/tracking/Impression;
    .locals 1

    const-string v0, "url"

    invoke-static {p0, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v0, Lco/uk/getmondo/api/model/tracking/Impression;->Companion:Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    invoke-virtual {v0, p0}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->c(Ljava/lang/String;)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    return-object v0
.end method

.method public static final c(Ljava/lang/String;Ljava/lang/String;)Lco/uk/getmondo/api/model/tracking/Impression;
    .locals 1

    const-string v0, "type"

    invoke-static {p0, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v0, Lco/uk/getmondo/api/model/tracking/Impression;->Companion:Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    invoke-virtual {v0, p0, p1}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->d(Ljava/lang/String;Ljava/lang/String;)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    return-object v0
.end method

.method public static final c(Z)Lco/uk/getmondo/api/model/tracking/Impression;
    .locals 1

    sget-object v0, Lco/uk/getmondo/api/model/tracking/Impression;->Companion:Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    invoke-virtual {v0, p0}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->c(Z)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    return-object v0
.end method

.method public static final d()Lco/uk/getmondo/api/model/tracking/Impression;
    .locals 1

    sget-object v0, Lco/uk/getmondo/api/model/tracking/Impression;->Companion:Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    invoke-virtual {v0}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->d()Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    return-object v0
.end method

.method public static final d(Ljava/lang/String;)Lco/uk/getmondo/api/model/tracking/Impression;
    .locals 1

    const-string v0, "url"

    invoke-static {p0, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v0, Lco/uk/getmondo/api/model/tracking/Impression;->Companion:Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    invoke-virtual {v0, p0}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->d(Ljava/lang/String;)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    return-object v0
.end method

.method public static final d(Z)Lco/uk/getmondo/api/model/tracking/Impression;
    .locals 1

    sget-object v0, Lco/uk/getmondo/api/model/tracking/Impression;->Companion:Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    invoke-virtual {v0, p0}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->d(Z)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    return-object v0
.end method

.method public static final e()Lco/uk/getmondo/api/model/tracking/Impression;
    .locals 1

    sget-object v0, Lco/uk/getmondo/api/model/tracking/Impression;->Companion:Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    invoke-virtual {v0}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->e()Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    return-object v0
.end method

.method public static final e(Ljava/lang/String;)Lco/uk/getmondo/api/model/tracking/Impression;
    .locals 1

    const-string v0, "description"

    invoke-static {p0, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v0, Lco/uk/getmondo/api/model/tracking/Impression;->Companion:Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    invoke-virtual {v0, p0}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->e(Ljava/lang/String;)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    return-object v0
.end method

.method public static final e(Z)Lco/uk/getmondo/api/model/tracking/Impression;
    .locals 1

    sget-object v0, Lco/uk/getmondo/api/model/tracking/Impression;->Companion:Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    invoke-virtual {v0, p0}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->e(Z)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    return-object v0
.end method

.method public static final f()Lco/uk/getmondo/api/model/tracking/Impression;
    .locals 1

    sget-object v0, Lco/uk/getmondo/api/model/tracking/Impression;->Companion:Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    invoke-virtual {v0}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->f()Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    return-object v0
.end method

.method public static final f(Ljava/lang/String;)Lco/uk/getmondo/api/model/tracking/Impression;
    .locals 1

    const-string v0, "referrer"

    invoke-static {p0, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v0, Lco/uk/getmondo/api/model/tracking/Impression;->Companion:Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    invoke-virtual {v0, p0}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->h(Ljava/lang/String;)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    return-object v0
.end method

.method public static final f(Z)Lco/uk/getmondo/api/model/tracking/Impression;
    .locals 1

    sget-object v0, Lco/uk/getmondo/api/model/tracking/Impression;->Companion:Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    invoke-virtual {v0, p0}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->f(Z)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    return-object v0
.end method

.method public static final g()Lco/uk/getmondo/api/model/tracking/Impression;
    .locals 1

    sget-object v0, Lco/uk/getmondo/api/model/tracking/Impression;->Companion:Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    invoke-virtual {v0}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->j()Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    return-object v0
.end method

.method public static final g(Ljava/lang/String;)Lco/uk/getmondo/api/model/tracking/Impression;
    .locals 1

    const-string v0, "redirectId"

    invoke-static {p0, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v0, Lco/uk/getmondo/api/model/tracking/Impression;->Companion:Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    invoke-virtual {v0, p0}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->i(Ljava/lang/String;)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    return-object v0
.end method

.method public static final g(Z)Lco/uk/getmondo/api/model/tracking/Impression;
    .locals 1

    sget-object v0, Lco/uk/getmondo/api/model/tracking/Impression;->Companion:Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    invoke-virtual {v0, p0}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->h(Z)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    return-object v0
.end method

.method public static final h()Lco/uk/getmondo/api/model/tracking/Impression;
    .locals 1

    sget-object v0, Lco/uk/getmondo/api/model/tracking/Impression;->Companion:Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    invoke-virtual {v0}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->k()Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    return-object v0
.end method

.method public static final h(Ljava/lang/String;)Lco/uk/getmondo/api/model/tracking/Impression;
    .locals 1

    const-string v0, "identityDocumentType"

    invoke-static {p0, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v0, Lco/uk/getmondo/api/model/tracking/Impression;->Companion:Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    invoke-virtual {v0, p0}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->j(Ljava/lang/String;)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    return-object v0
.end method

.method public static final h(Z)Lco/uk/getmondo/api/model/tracking/Impression;
    .locals 1

    sget-object v0, Lco/uk/getmondo/api/model/tracking/Impression;->Companion:Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    invoke-virtual {v0, p0}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->i(Z)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    return-object v0
.end method

.method public static final i()Lco/uk/getmondo/api/model/tracking/Impression;
    .locals 1

    sget-object v0, Lco/uk/getmondo/api/model/tracking/Impression;->Companion:Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    invoke-virtual {v0}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->l()Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    return-object v0
.end method

.method public static final i(Ljava/lang/String;)Lco/uk/getmondo/api/model/tracking/Impression;
    .locals 1

    const-string v0, "identityDocumentType"

    invoke-static {p0, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v0, Lco/uk/getmondo/api/model/tracking/Impression;->Companion:Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    invoke-virtual {v0, p0}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->k(Ljava/lang/String;)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    return-object v0
.end method

.method public static final i(Z)Lco/uk/getmondo/api/model/tracking/Impression;
    .locals 1

    sget-object v0, Lco/uk/getmondo/api/model/tracking/Impression;->Companion:Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    invoke-virtual {v0, p0}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->j(Z)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    return-object v0
.end method

.method public static final j()Lco/uk/getmondo/api/model/tracking/Impression;
    .locals 1

    sget-object v0, Lco/uk/getmondo/api/model/tracking/Impression;->Companion:Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    invoke-virtual {v0}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->m()Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    return-object v0
.end method

.method public static final j(Ljava/lang/String;)Lco/uk/getmondo/api/model/tracking/Impression;
    .locals 1

    const-string v0, "advertisingId"

    invoke-static {p0, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v0, Lco/uk/getmondo/api/model/tracking/Impression;->Companion:Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    invoke-virtual {v0, p0}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->l(Ljava/lang/String;)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    return-object v0
.end method

.method public static final j(Z)Lco/uk/getmondo/api/model/tracking/Impression;
    .locals 1

    sget-object v0, Lco/uk/getmondo/api/model/tracking/Impression;->Companion:Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    invoke-virtual {v0, p0}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->k(Z)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    return-object v0
.end method

.method public static final k()Lco/uk/getmondo/api/model/tracking/Impression;
    .locals 1

    sget-object v0, Lco/uk/getmondo/api/model/tracking/Impression;->Companion:Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    invoke-virtual {v0}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->o()Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    return-object v0
.end method

.method public static final l()Lco/uk/getmondo/api/model/tracking/Impression;
    .locals 1

    sget-object v0, Lco/uk/getmondo/api/model/tracking/Impression;->Companion:Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    invoke-virtual {v0}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->q()Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    return-object v0
.end method

.method public static final m()Lco/uk/getmondo/api/model/tracking/Impression;
    .locals 1

    sget-object v0, Lco/uk/getmondo/api/model/tracking/Impression;->Companion:Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    invoke-virtual {v0}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->r()Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    return-object v0
.end method

.method public static final n()Lco/uk/getmondo/api/model/tracking/Impression;
    .locals 1

    sget-object v0, Lco/uk/getmondo/api/model/tracking/Impression;->Companion:Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    invoke-virtual {v0}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->v()Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    return-object v0
.end method

.method public static final o()Lco/uk/getmondo/api/model/tracking/Impression;
    .locals 1

    sget-object v0, Lco/uk/getmondo/api/model/tracking/Impression;->Companion:Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    invoke-virtual {v0}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->w()Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    return-object v0
.end method

.method public static final p()Lco/uk/getmondo/api/model/tracking/Impression;
    .locals 1

    sget-object v0, Lco/uk/getmondo/api/model/tracking/Impression;->Companion:Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    invoke-virtual {v0}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->x()Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    return-object v0
.end method

.method public static final q()Lco/uk/getmondo/api/model/tracking/Impression;
    .locals 1

    sget-object v0, Lco/uk/getmondo/api/model/tracking/Impression;->Companion:Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    invoke-virtual {v0}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->y()Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    return-object v0
.end method

.method public static final r()Lco/uk/getmondo/api/model/tracking/Impression;
    .locals 1

    sget-object v0, Lco/uk/getmondo/api/model/tracking/Impression;->Companion:Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    invoke-virtual {v0}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->z()Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    return-object v0
.end method

.method public static final s()Lco/uk/getmondo/api/model/tracking/Impression;
    .locals 1

    sget-object v0, Lco/uk/getmondo/api/model/tracking/Impression;->Companion:Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    invoke-virtual {v0}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->A()Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    return-object v0
.end method

.method public static final t()Lco/uk/getmondo/api/model/tracking/Impression;
    .locals 1

    sget-object v0, Lco/uk/getmondo/api/model/tracking/Impression;->Companion:Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    invoke-virtual {v0}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->B()Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    return-object v0
.end method

.method public static final u()Lco/uk/getmondo/api/model/tracking/Impression;
    .locals 1

    sget-object v0, Lco/uk/getmondo/api/model/tracking/Impression;->Companion:Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    invoke-virtual {v0}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->C()Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    return-object v0
.end method

.method public static final v()Lco/uk/getmondo/api/model/tracking/Impression;
    .locals 1

    sget-object v0, Lco/uk/getmondo/api/model/tracking/Impression;->Companion:Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    invoke-virtual {v0}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->D()Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    return-object v0
.end method

.method public static final w()Lco/uk/getmondo/api/model/tracking/Impression;
    .locals 1

    sget-object v0, Lco/uk/getmondo/api/model/tracking/Impression;->Companion:Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    invoke-virtual {v0}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->E()Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    return-object v0
.end method

.method public static final x()Lco/uk/getmondo/api/model/tracking/Impression;
    .locals 1

    sget-object v0, Lco/uk/getmondo/api/model/tracking/Impression;->Companion:Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    invoke-virtual {v0}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->F()Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    return-object v0
.end method

.method public static final y()Lco/uk/getmondo/api/model/tracking/Impression;
    .locals 1

    sget-object v0, Lco/uk/getmondo/api/model/tracking/Impression;->Companion:Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    invoke-virtual {v0}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->G()Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    return-object v0
.end method

.method public static final z()Lco/uk/getmondo/api/model/tracking/Impression;
    .locals 1

    sget-object v0, Lco/uk/getmondo/api/model/tracking/Impression;->Companion:Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    invoke-virtual {v0}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->H()Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(Lco/uk/getmondo/api/model/tracking/ImpressionEvent;)V
    .locals 1

    .prologue
    const-string v0, "event"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 13
    iget-object v0, p0, Lco/uk/getmondo/api/model/tracking/Impression;->events:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 14
    return-void
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_0

    instance-of v0, p1, Lco/uk/getmondo/api/model/tracking/Impression;

    if-eqz v0, :cond_1

    check-cast p1, Lco/uk/getmondo/api/model/tracking/Impression;

    iget-object v0, p0, Lco/uk/getmondo/api/model/tracking/Impression;->events:Ljava/util/List;

    iget-object v1, p1, Lco/uk/getmondo/api/model/tracking/Impression;->events:Ljava/util/List;

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 1

    iget-object v0, p0, Lco/uk/getmondo/api/model/tracking/Impression;->events:Ljava/util/List;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 91
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 92
    iget-object v0, p0, Lco/uk/getmondo/api/model/tracking/Impression;->events:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/api/model/tracking/ImpressionEvent;

    .line 93
    invoke-virtual {v0}, Lco/uk/getmondo/api/model/tracking/ImpressionEvent;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 94
    const-string v0, " "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 96
    :cond_0
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "stringBuilder.toString()"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method
