.class public final enum Lco/uk/getmondo/api/model/tracking/Impression$TopUpTapFrom;
.super Ljava/lang/Enum;
.source "Impression.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lco/uk/getmondo/api/model/tracking/Impression;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "TopUpTapFrom"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lco/uk/getmondo/api/model/tracking/Impression$TopUpTapFrom;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\t\u0008\u0086\u0001\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00000\u0001B\u000f\u0008\u0002\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004R\u001a\u0010\u0002\u001a\u00020\u0003X\u0080\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u0005\u0010\u0006\"\u0004\u0008\u0007\u0010\u0008j\u0002\u0008\tj\u0002\u0008\nj\u0002\u0008\u000b\u00a8\u0006\u000c"
    }
    d2 = {
        "Lco/uk/getmondo/api/model/tracking/Impression$TopUpTapFrom;",
        "",
        "value",
        "",
        "(Ljava/lang/String;ILjava/lang/String;)V",
        "getValue$app_monzoPrepaidRelease",
        "()Ljava/lang/String;",
        "setValue$app_monzoPrepaidRelease",
        "(Ljava/lang/String;)V",
        "FROM_NEW_CARD",
        "FROM_SAVED_CARD",
        "ANDROID_PAY",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lco/uk/getmondo/api/model/tracking/Impression$TopUpTapFrom;

.field public static final enum ANDROID_PAY:Lco/uk/getmondo/api/model/tracking/Impression$TopUpTapFrom;

.field public static final enum FROM_NEW_CARD:Lco/uk/getmondo/api/model/tracking/Impression$TopUpTapFrom;

.field public static final enum FROM_SAVED_CARD:Lco/uk/getmondo/api/model/tracking/Impression$TopUpTapFrom;


# instance fields
.field private value:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    const/4 v0, 0x3

    new-array v0, v0, [Lco/uk/getmondo/api/model/tracking/Impression$TopUpTapFrom;

    new-instance v1, Lco/uk/getmondo/api/model/tracking/Impression$TopUpTapFrom;

    const-string v2, "FROM_NEW_CARD"

    .line 38
    const-string v3, "new_card"

    invoke-direct {v1, v2, v4, v3}, Lco/uk/getmondo/api/model/tracking/Impression$TopUpTapFrom;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lco/uk/getmondo/api/model/tracking/Impression$TopUpTapFrom;->FROM_NEW_CARD:Lco/uk/getmondo/api/model/tracking/Impression$TopUpTapFrom;

    aput-object v1, v0, v4

    new-instance v1, Lco/uk/getmondo/api/model/tracking/Impression$TopUpTapFrom;

    const-string v2, "FROM_SAVED_CARD"

    .line 39
    const-string v3, "topup_confirm"

    invoke-direct {v1, v2, v5, v3}, Lco/uk/getmondo/api/model/tracking/Impression$TopUpTapFrom;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lco/uk/getmondo/api/model/tracking/Impression$TopUpTapFrom;->FROM_SAVED_CARD:Lco/uk/getmondo/api/model/tracking/Impression$TopUpTapFrom;

    aput-object v1, v0, v5

    new-instance v1, Lco/uk/getmondo/api/model/tracking/Impression$TopUpTapFrom;

    const-string v2, "ANDROID_PAY"

    .line 40
    const-string v3, "android_pay"

    invoke-direct {v1, v2, v6, v3}, Lco/uk/getmondo/api/model/tracking/Impression$TopUpTapFrom;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lco/uk/getmondo/api/model/tracking/Impression$TopUpTapFrom;->ANDROID_PAY:Lco/uk/getmondo/api/model/tracking/Impression$TopUpTapFrom;

    aput-object v1, v0, v6

    sput-object v0, Lco/uk/getmondo/api/model/tracking/Impression$TopUpTapFrom;->$VALUES:[Lco/uk/getmondo/api/model/tracking/Impression$TopUpTapFrom;

    return-void
.end method

.method protected constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    const-string v0, "value"

    invoke-static {p3, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 37
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput-object p3, p0, Lco/uk/getmondo/api/model/tracking/Impression$TopUpTapFrom;->value:Ljava/lang/String;

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lco/uk/getmondo/api/model/tracking/Impression$TopUpTapFrom;
    .locals 1

    const-class v0, Lco/uk/getmondo/api/model/tracking/Impression$TopUpTapFrom;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/api/model/tracking/Impression$TopUpTapFrom;

    return-object v0
.end method

.method public static values()[Lco/uk/getmondo/api/model/tracking/Impression$TopUpTapFrom;
    .locals 1

    sget-object v0, Lco/uk/getmondo/api/model/tracking/Impression$TopUpTapFrom;->$VALUES:[Lco/uk/getmondo/api/model/tracking/Impression$TopUpTapFrom;

    invoke-virtual {v0}, [Lco/uk/getmondo/api/model/tracking/Impression$TopUpTapFrom;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lco/uk/getmondo/api/model/tracking/Impression$TopUpTapFrom;

    return-object v0
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lco/uk/getmondo/api/model/tracking/Impression$TopUpTapFrom;->value:Ljava/lang/String;

    return-object v0
.end method
