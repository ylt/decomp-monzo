.class public final Lco/uk/getmondo/api/model/tracking/Data;
.super Ljava/lang/Object;
.source "Data.kt"

# interfaces
.implements Ljava/io/Serializable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lco/uk/getmondo/api/model/tracking/Data$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u00000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\t\n\u0002\u0010\u000b\n\u0002\u0008\u001f\n\u0002\u0010\t\n\u0002\u0008U\n\u0002\u0010\u0000\n\u0002\u0008\u0004\u0008\u0086\u0008\u0018\u0000 \u0088\u00012\u00020\u0001:\u0002\u0088\u0001B\u00bd\u0005\u0012\n\u0008\u0002\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u0012\n\u0008\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u0012\n\u0008\u0002\u0010\u0006\u001a\u0004\u0018\u00010\u0005\u0012\n\u0008\u0002\u0010\u0007\u001a\u0004\u0018\u00010\u0005\u0012\n\u0008\u0002\u0010\u0008\u001a\u0004\u0018\u00010\u0005\u0012\n\u0008\u0002\u0010\t\u001a\u0004\u0018\u00010\u0005\u0012\n\u0008\u0002\u0010\n\u001a\u0004\u0018\u00010\u0003\u0012\n\u0008\u0002\u0010\u000b\u001a\u0004\u0018\u00010\u0003\u0012\n\u0008\u0002\u0010\u000c\u001a\u0004\u0018\u00010\u0005\u0012\n\u0008\u0002\u0010\r\u001a\u0004\u0018\u00010\u0005\u0012\n\u0008\u0002\u0010\u000e\u001a\u0004\u0018\u00010\u000f\u0012\n\u0008\u0002\u0010\u0010\u001a\u0004\u0018\u00010\u000f\u0012\n\u0008\u0002\u0010\u0011\u001a\u0004\u0018\u00010\u0005\u0012\n\u0008\u0002\u0010\u0012\u001a\u0004\u0018\u00010\u000f\u0012\n\u0008\u0002\u0010\u0013\u001a\u0004\u0018\u00010\u0005\u0012\n\u0008\u0002\u0010\u0014\u001a\u0004\u0018\u00010\u0005\u0012\n\u0008\u0002\u0010\u0015\u001a\u0004\u0018\u00010\u0005\u0012\n\u0008\u0002\u0010\u0016\u001a\u0004\u0018\u00010\u0003\u0012\n\u0008\u0002\u0010\u0017\u001a\u0004\u0018\u00010\u000f\u0012\n\u0008\u0002\u0010\u0018\u001a\u0004\u0018\u00010\u0003\u0012\n\u0008\u0002\u0010\u0019\u001a\u0004\u0018\u00010\u0003\u0012\n\u0008\u0002\u0010\u001a\u001a\u0004\u0018\u00010\u0005\u0012\n\u0008\u0002\u0010\u001b\u001a\u0004\u0018\u00010\u0005\u0012\n\u0008\u0002\u0010\u001c\u001a\u0004\u0018\u00010\u000f\u0012\n\u0008\u0002\u0010\u001d\u001a\u0004\u0018\u00010\u0005\u0012\n\u0008\u0002\u0010\u001e\u001a\u0004\u0018\u00010\u0005\u0012\n\u0008\u0002\u0010\u001f\u001a\u0004\u0018\u00010\u0003\u0012\n\u0008\u0002\u0010 \u001a\u0004\u0018\u00010\u000f\u0012\n\u0008\u0002\u0010!\u001a\u0004\u0018\u00010\u0005\u0012\n\u0008\u0002\u0010\"\u001a\u0004\u0018\u00010\u000f\u0012\n\u0008\u0002\u0010#\u001a\u0004\u0018\u00010\u0005\u0012\n\u0008\u0002\u0010$\u001a\u0004\u0018\u00010\u0005\u0012\n\u0008\u0002\u0010%\u001a\u0004\u0018\u00010\u0005\u0012\n\u0008\u0002\u0010&\u001a\u0004\u0018\u00010\u0005\u0012\n\u0008\u0002\u0010\'\u001a\u0004\u0018\u00010\u0003\u0012\n\u0008\u0002\u0010(\u001a\u0004\u0018\u00010\u0005\u0012\n\u0008\u0002\u0010)\u001a\u0004\u0018\u00010\u0005\u0012\n\u0008\u0002\u0010*\u001a\u0004\u0018\u00010\u0005\u0012\n\u0008\u0002\u0010+\u001a\u0004\u0018\u00010\u0005\u0012\n\u0008\u0002\u0010,\u001a\u0004\u0018\u00010\u0005\u0012\n\u0008\u0002\u0010-\u001a\u0004\u0018\u00010\u000f\u0012\n\u0008\u0002\u0010.\u001a\u0004\u0018\u00010/\u0012\n\u0008\u0002\u00100\u001a\u0004\u0018\u00010/\u0012\n\u0008\u0002\u00101\u001a\u0004\u0018\u00010\u000f\u0012\n\u0008\u0002\u00102\u001a\u0004\u0018\u00010\u0005\u0012\n\u0008\u0002\u00103\u001a\u0004\u0018\u00010\u0005\u0012\n\u0008\u0002\u00104\u001a\u0004\u0018\u00010\u000f\u0012\n\u0008\u0002\u00105\u001a\u0004\u0018\u00010\u0005\u0012\n\u0008\u0002\u00106\u001a\u0004\u0018\u00010\u0005\u0012\n\u0008\u0002\u00107\u001a\u0004\u0018\u00010\u000f\u0012\n\u0008\u0002\u00108\u001a\u0004\u0018\u00010\u000f\u0012\n\u0008\u0002\u00109\u001a\u0004\u0018\u00010\u0005\u0012\n\u0008\u0002\u0010:\u001a\u0004\u0018\u00010\u0005\u0012\n\u0008\u0002\u0010;\u001a\u0004\u0018\u00010\u000f\u0012\n\u0008\u0002\u0010<\u001a\u0004\u0018\u00010\u0005\u0012\n\u0008\u0002\u0010=\u001a\u0004\u0018\u00010\u0005\u0012\n\u0008\u0002\u0010>\u001a\u0004\u0018\u00010\u0005\u0012\n\u0008\u0002\u0010?\u001a\u0004\u0018\u00010\u0005\u00a2\u0006\u0002\u0010@J\u0010\u0010D\u001a\u0004\u0018\u00010\u0003H\u00c2\u0003\u00a2\u0006\u0002\u0010EJ\u000b\u0010F\u001a\u0004\u0018\u00010\u0005H\u00c2\u0003J\u0010\u0010G\u001a\u0004\u0018\u00010\u000fH\u00c2\u0003\u00a2\u0006\u0002\u0010HJ\u0010\u0010I\u001a\u0004\u0018\u00010\u000fH\u00c2\u0003\u00a2\u0006\u0002\u0010HJ\u000b\u0010J\u001a\u0004\u0018\u00010\u0005H\u00c2\u0003J\u0010\u0010K\u001a\u0004\u0018\u00010\u000fH\u00c2\u0003\u00a2\u0006\u0002\u0010HJ\u000b\u0010L\u001a\u0004\u0018\u00010\u0005H\u00c2\u0003J\u000b\u0010M\u001a\u0004\u0018\u00010\u0005H\u00c2\u0003J\u000b\u0010N\u001a\u0004\u0018\u00010\u0005H\u00c2\u0003J\u0010\u0010O\u001a\u0004\u0018\u00010\u0003H\u00c2\u0003\u00a2\u0006\u0002\u0010EJ\u0010\u0010P\u001a\u0004\u0018\u00010\u000fH\u00c2\u0003\u00a2\u0006\u0002\u0010HJ\u000b\u0010Q\u001a\u0004\u0018\u00010\u0005H\u00c2\u0003J\u0010\u0010R\u001a\u0004\u0018\u00010\u0003H\u00c2\u0003\u00a2\u0006\u0002\u0010EJ\u0010\u0010S\u001a\u0004\u0018\u00010\u0003H\u00c2\u0003\u00a2\u0006\u0002\u0010EJ\u000b\u0010T\u001a\u0004\u0018\u00010\u0005H\u00c2\u0003J\u000b\u0010U\u001a\u0004\u0018\u00010\u0005H\u00c2\u0003J\u0010\u0010V\u001a\u0004\u0018\u00010\u000fH\u00c2\u0003\u00a2\u0006\u0002\u0010HJ\u000b\u0010W\u001a\u0004\u0018\u00010\u0005H\u00c2\u0003J\u000b\u0010X\u001a\u0004\u0018\u00010\u0005H\u00c2\u0003J\u0010\u0010Y\u001a\u0004\u0018\u00010\u0003H\u00c2\u0003\u00a2\u0006\u0002\u0010EJ\u0010\u0010Z\u001a\u0004\u0018\u00010\u000fH\u00c2\u0003\u00a2\u0006\u0002\u0010HJ\u000b\u0010[\u001a\u0004\u0018\u00010\u0005H\u00c2\u0003J\u000b\u0010\\\u001a\u0004\u0018\u00010\u0005H\u00c2\u0003J\u0010\u0010]\u001a\u0004\u0018\u00010\u000fH\u00c2\u0003\u00a2\u0006\u0002\u0010HJ\u000b\u0010^\u001a\u0004\u0018\u00010\u0005H\u00c2\u0003J\u000b\u0010_\u001a\u0004\u0018\u00010\u0005H\u00c2\u0003J\u000b\u0010`\u001a\u0004\u0018\u00010\u0005H\u00c2\u0003J\u000b\u0010a\u001a\u0004\u0018\u00010\u0005H\u00c2\u0003J\u0010\u0010b\u001a\u0004\u0018\u00010\u0003H\u00c2\u0003\u00a2\u0006\u0002\u0010EJ\u000b\u0010c\u001a\u0004\u0018\u00010\u0005H\u00c2\u0003J\u000b\u0010d\u001a\u0004\u0018\u00010\u0005H\u00c2\u0003J\u000b\u0010e\u001a\u0004\u0018\u00010\u0005H\u00c2\u0003J\u000b\u0010f\u001a\u0004\u0018\u00010\u0005H\u00c2\u0003J\u000b\u0010g\u001a\u0004\u0018\u00010\u0005H\u00c2\u0003J\u000b\u0010h\u001a\u0004\u0018\u00010\u0005H\u00c2\u0003J\u0010\u0010i\u001a\u0004\u0018\u00010\u000fH\u00c2\u0003\u00a2\u0006\u0002\u0010HJ\u0010\u0010j\u001a\u0004\u0018\u00010/H\u00c2\u0003\u00a2\u0006\u0002\u0010kJ\u0010\u0010l\u001a\u0004\u0018\u00010/H\u00c2\u0003\u00a2\u0006\u0002\u0010kJ\u0010\u0010m\u001a\u0004\u0018\u00010\u000fH\u00c2\u0003\u00a2\u0006\u0002\u0010HJ\u000b\u0010n\u001a\u0004\u0018\u00010\u0005H\u00c2\u0003J\u000b\u0010o\u001a\u0004\u0018\u00010\u0005H\u00c2\u0003J\u0010\u0010p\u001a\u0004\u0018\u00010\u000fH\u00c2\u0003\u00a2\u0006\u0002\u0010HJ\u000b\u0010q\u001a\u0004\u0018\u00010\u0005H\u00c2\u0003J\u000b\u0010r\u001a\u0004\u0018\u00010\u0005H\u00c2\u0003J\u000b\u0010s\u001a\u0004\u0018\u00010\u0005H\u00c2\u0003J\u0010\u0010t\u001a\u0004\u0018\u00010\u000fH\u00c2\u0003\u00a2\u0006\u0002\u0010HJ\u0010\u0010u\u001a\u0004\u0018\u00010\u000fH\u00c2\u0003\u00a2\u0006\u0002\u0010HJ\u000b\u0010v\u001a\u0004\u0018\u00010\u0005H\u00c2\u0003J\u000b\u0010w\u001a\u0004\u0018\u00010\u0005H\u00c2\u0003J\u0010\u0010x\u001a\u0004\u0018\u00010\u000fH\u00c2\u0003\u00a2\u0006\u0002\u0010HJ\u000b\u0010y\u001a\u0004\u0018\u00010\u0005H\u00c2\u0003J\u000b\u0010z\u001a\u0004\u0018\u00010\u0005H\u00c2\u0003J\u000b\u0010{\u001a\u0004\u0018\u00010\u0005H\u00c2\u0003J\u000b\u0010|\u001a\u0004\u0018\u00010\u0005H\u00c2\u0003J\u000b\u0010}\u001a\u0004\u0018\u00010\u0005H\u00c2\u0003J\u0010\u0010~\u001a\u0004\u0018\u00010\u0003H\u00c2\u0003\u00a2\u0006\u0002\u0010EJ\u0010\u0010\u007f\u001a\u0004\u0018\u00010\u0003H\u00c2\u0003\u00a2\u0006\u0002\u0010EJ\u000c\u0010\u0080\u0001\u001a\u0004\u0018\u00010\u0005H\u00c2\u0003J\u00c8\u0005\u0010\u0081\u0001\u001a\u00020\u00002\n\u0008\u0002\u0010\u0002\u001a\u0004\u0018\u00010\u00032\n\u0008\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u00052\n\u0008\u0002\u0010\u0006\u001a\u0004\u0018\u00010\u00052\n\u0008\u0002\u0010\u0007\u001a\u0004\u0018\u00010\u00052\n\u0008\u0002\u0010\u0008\u001a\u0004\u0018\u00010\u00052\n\u0008\u0002\u0010\t\u001a\u0004\u0018\u00010\u00052\n\u0008\u0002\u0010\n\u001a\u0004\u0018\u00010\u00032\n\u0008\u0002\u0010\u000b\u001a\u0004\u0018\u00010\u00032\n\u0008\u0002\u0010\u000c\u001a\u0004\u0018\u00010\u00052\n\u0008\u0002\u0010\r\u001a\u0004\u0018\u00010\u00052\n\u0008\u0002\u0010\u000e\u001a\u0004\u0018\u00010\u000f2\n\u0008\u0002\u0010\u0010\u001a\u0004\u0018\u00010\u000f2\n\u0008\u0002\u0010\u0011\u001a\u0004\u0018\u00010\u00052\n\u0008\u0002\u0010\u0012\u001a\u0004\u0018\u00010\u000f2\n\u0008\u0002\u0010\u0013\u001a\u0004\u0018\u00010\u00052\n\u0008\u0002\u0010\u0014\u001a\u0004\u0018\u00010\u00052\n\u0008\u0002\u0010\u0015\u001a\u0004\u0018\u00010\u00052\n\u0008\u0002\u0010\u0016\u001a\u0004\u0018\u00010\u00032\n\u0008\u0002\u0010\u0017\u001a\u0004\u0018\u00010\u000f2\n\u0008\u0002\u0010\u0018\u001a\u0004\u0018\u00010\u00032\n\u0008\u0002\u0010\u0019\u001a\u0004\u0018\u00010\u00032\n\u0008\u0002\u0010\u001a\u001a\u0004\u0018\u00010\u00052\n\u0008\u0002\u0010\u001b\u001a\u0004\u0018\u00010\u00052\n\u0008\u0002\u0010\u001c\u001a\u0004\u0018\u00010\u000f2\n\u0008\u0002\u0010\u001d\u001a\u0004\u0018\u00010\u00052\n\u0008\u0002\u0010\u001e\u001a\u0004\u0018\u00010\u00052\n\u0008\u0002\u0010\u001f\u001a\u0004\u0018\u00010\u00032\n\u0008\u0002\u0010 \u001a\u0004\u0018\u00010\u000f2\n\u0008\u0002\u0010!\u001a\u0004\u0018\u00010\u00052\n\u0008\u0002\u0010\"\u001a\u0004\u0018\u00010\u000f2\n\u0008\u0002\u0010#\u001a\u0004\u0018\u00010\u00052\n\u0008\u0002\u0010$\u001a\u0004\u0018\u00010\u00052\n\u0008\u0002\u0010%\u001a\u0004\u0018\u00010\u00052\n\u0008\u0002\u0010&\u001a\u0004\u0018\u00010\u00052\n\u0008\u0002\u0010\'\u001a\u0004\u0018\u00010\u00032\n\u0008\u0002\u0010(\u001a\u0004\u0018\u00010\u00052\n\u0008\u0002\u0010)\u001a\u0004\u0018\u00010\u00052\n\u0008\u0002\u0010*\u001a\u0004\u0018\u00010\u00052\n\u0008\u0002\u0010+\u001a\u0004\u0018\u00010\u00052\n\u0008\u0002\u0010,\u001a\u0004\u0018\u00010\u00052\n\u0008\u0002\u0010-\u001a\u0004\u0018\u00010\u000f2\n\u0008\u0002\u0010.\u001a\u0004\u0018\u00010/2\n\u0008\u0002\u00100\u001a\u0004\u0018\u00010/2\n\u0008\u0002\u00101\u001a\u0004\u0018\u00010\u000f2\n\u0008\u0002\u00102\u001a\u0004\u0018\u00010\u00052\n\u0008\u0002\u00103\u001a\u0004\u0018\u00010\u00052\n\u0008\u0002\u00104\u001a\u0004\u0018\u00010\u000f2\n\u0008\u0002\u00105\u001a\u0004\u0018\u00010\u00052\n\u0008\u0002\u00106\u001a\u0004\u0018\u00010\u00052\n\u0008\u0002\u00107\u001a\u0004\u0018\u00010\u000f2\n\u0008\u0002\u00108\u001a\u0004\u0018\u00010\u000f2\n\u0008\u0002\u00109\u001a\u0004\u0018\u00010\u00052\n\u0008\u0002\u0010:\u001a\u0004\u0018\u00010\u00052\n\u0008\u0002\u0010;\u001a\u0004\u0018\u00010\u000f2\n\u0008\u0002\u0010<\u001a\u0004\u0018\u00010\u00052\n\u0008\u0002\u0010=\u001a\u0004\u0018\u00010\u00052\n\u0008\u0002\u0010>\u001a\u0004\u0018\u00010\u00052\n\u0008\u0002\u0010?\u001a\u0004\u0018\u00010\u0005H\u00c6\u0001\u00a2\u0006\u0003\u0010\u0082\u0001J\u0016\u0010\u0083\u0001\u001a\u00020\u000f2\n\u0010\u0084\u0001\u001a\u0005\u0018\u00010\u0085\u0001H\u00d6\u0003J\n\u0010\u0086\u0001\u001a\u00020\u0003H\u00d6\u0001J\n\u0010\u0087\u0001\u001a\u00020\u0005H\u00d6\u0001R\u0012\u0010\u001f\u001a\u0004\u0018\u00010\u0003X\u0082\u0004\u00a2\u0006\u0004\n\u0002\u0010AR\u0012\u0010\u000b\u001a\u0004\u0018\u00010\u0003X\u0082\u0004\u00a2\u0006\u0004\n\u0002\u0010AR\u0014\u00107\u001a\u0004\u0018\u00010\u000f8\u0002X\u0083\u0004\u00a2\u0006\u0004\n\u0002\u0010BR\u0012\u0010(\u001a\u0004\u0018\u00010\u00058\u0002X\u0083\u0004\u00a2\u0006\u0002\n\u0000R\u0012\u0010)\u001a\u0004\u0018\u00010\u00058\u0002X\u0083\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\'\u001a\u0004\u0018\u00010\u00038\u0002X\u0083\u0004\u00a2\u0006\u0004\n\u0002\u0010AR\u0010\u0010\u001d\u001a\u0004\u0018\u00010\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0012\u0010\u001c\u001a\u0004\u0018\u00010\u000fX\u0082\u0004\u00a2\u0006\u0004\n\u0002\u0010BR\u0012\u0010\u000c\u001a\u0004\u0018\u00010\u00058\u0002X\u0083\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0015\u001a\u0004\u0018\u00010\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0012\u0010\u0018\u001a\u0004\u0018\u00010\u0003X\u0082\u0004\u00a2\u0006\u0004\n\u0002\u0010AR\u0012\u0010\u0019\u001a\u0004\u0018\u00010\u0003X\u0082\u0004\u00a2\u0006\u0004\n\u0002\u0010AR\u0010\u0010\r\u001a\u0004\u0018\u00010\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010!\u001a\u0004\u0018\u00010\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u000e\u001a\u0004\u0018\u00010\u000f8\u0002X\u0083\u0004\u00a2\u0006\u0004\n\u0002\u0010BR\u0012\u0010\u0011\u001a\u0004\u0018\u00010\u00058\u0002X\u0083\u0004\u00a2\u0006\u0002\n\u0000R\u0012\u0010\n\u001a\u0004\u0018\u00010\u0003X\u0082\u0004\u00a2\u0006\u0004\n\u0002\u0010AR\u0012\u0010\"\u001a\u0004\u0018\u00010\u000fX\u0082\u0004\u00a2\u0006\u0004\n\u0002\u0010BR\u0010\u0010,\u001a\u0004\u0018\u00010\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0012\u00108\u001a\u0004\u0018\u00010\u000fX\u0082\u0004\u00a2\u0006\u0004\n\u0002\u0010BR\u0010\u00105\u001a\u0004\u0018\u00010\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0012\u0010?\u001a\u0004\u0018\u00010\u00058\u0002X\u0083\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u001e\u001a\u0004\u0018\u00010\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0012\u0010.\u001a\u0004\u0018\u00010/X\u0082\u0004\u00a2\u0006\u0004\n\u0002\u0010CR\u0012\u00104\u001a\u0004\u0018\u00010\u000fX\u0082\u0004\u00a2\u0006\u0004\n\u0002\u0010BR\u0010\u0010\u001a\u001a\u0004\u0018\u00010\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0012\u0010#\u001a\u0004\u0018\u00010\u00058\u0002X\u0083\u0004\u00a2\u0006\u0002\n\u0000R\u0012\u0010\u0017\u001a\u0004\u0018\u00010\u000fX\u0082\u0004\u00a2\u0006\u0004\n\u0002\u0010BR\u0014\u0010\u0012\u001a\u0004\u0018\u00010\u000f8\u0002X\u0083\u0004\u00a2\u0006\u0004\n\u0002\u0010BR\u0012\u0010*\u001a\u0004\u0018\u00010\u00058\u0002X\u0083\u0004\u00a2\u0006\u0002\n\u0000R\u0012\u00101\u001a\u0004\u0018\u00010\u000fX\u0082\u0004\u00a2\u0006\u0004\n\u0002\u0010BR\u0010\u0010\u001b\u001a\u0004\u0018\u00010\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0012\u0010\u0016\u001a\u0004\u0018\u00010\u0003X\u0082\u0004\u00a2\u0006\u0004\n\u0002\u0010AR\u0012\u0010>\u001a\u0004\u0018\u00010\u00058\u0002X\u0083\u0004\u00a2\u0006\u0002\n\u0000R\u0012\u0010\u0014\u001a\u0004\u0018\u00010\u00058\u0002X\u0083\u0004\u00a2\u0006\u0002\n\u0000R\u0012\u0010 \u001a\u0004\u0018\u00010\u000fX\u0082\u0004\u00a2\u0006\u0004\n\u0002\u0010BR\u0012\u0010:\u001a\u0004\u0018\u00010\u00058\u0002X\u0083\u0004\u00a2\u0006\u0002\n\u0000R\u0012\u00109\u001a\u0004\u0018\u00010\u00058\u0002X\u0083\u0004\u00a2\u0006\u0002\n\u0000R\u0012\u0010\u0013\u001a\u0004\u0018\u00010\u00058\u0002X\u0083\u0004\u00a2\u0006\u0002\n\u0000R\u0012\u0010\u0004\u001a\u0004\u0018\u00010\u00058\u0002X\u0083\u0004\u00a2\u0006\u0002\n\u0000R\u0012\u0010;\u001a\u0004\u0018\u00010\u000fX\u0082\u0004\u00a2\u0006\u0004\n\u0002\u0010BR\u0010\u0010=\u001a\u0004\u0018\u00010\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010&\u001a\u0004\u0018\u00010\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0012\u0010\u0008\u001a\u0004\u0018\u00010\u00058\u0002X\u0083\u0004\u00a2\u0006\u0002\n\u0000R\u0012\u0010\u0007\u001a\u0004\u0018\u00010\u00058\u0002X\u0083\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u00106\u001a\u0004\u0018\u00010\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0012\u0010-\u001a\u0004\u0018\u00010\u000fX\u0082\u0004\u00a2\u0006\u0004\n\u0002\u0010BR\u0012\u00100\u001a\u0004\u0018\u00010/X\u0082\u0004\u00a2\u0006\u0004\n\u0002\u0010CR\u0014\u0010\u0010\u001a\u0004\u0018\u00010\u000f8\u0002X\u0083\u0004\u00a2\u0006\u0004\n\u0002\u0010BR\u0012\u00103\u001a\u0004\u0018\u00010\u00058\u0002X\u0083\u0004\u00a2\u0006\u0002\n\u0000R\u0012\u0010<\u001a\u0004\u0018\u00010\u00058\u0002X\u0083\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0002\u001a\u0004\u0018\u00010\u00038\u0002X\u0083\u0004\u00a2\u0006\u0004\n\u0002\u0010AR\u0012\u0010$\u001a\u0004\u0018\u00010\u00058\u0002X\u0083\u0004\u00a2\u0006\u0002\n\u0000R\u0012\u0010\u0006\u001a\u0004\u0018\u00010\u00058\u0002X\u0083\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010%\u001a\u0004\u0018\u00010\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\t\u001a\u0004\u0018\u00010\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0012\u0010+\u001a\u0004\u0018\u00010\u00058\u0002X\u0083\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u00102\u001a\u0004\u0018\u00010\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0089\u0001"
    }
    d2 = {
        "Lco/uk/getmondo/api/model/tracking/Data;",
        "Ljava/io/Serializable;",
        "tourPage",
        "",
        "openedFrom",
        "",
        "transactionId",
        "referrer",
        "redirectId",
        "url",
        "eligible",
        "amount",
        "cameFrom",
        "description",
        "docDone",
        "",
        "selfieDone",
        "docType",
        "kycRejected",
        "oldCategory",
        "newCategory",
        "category",
        "month",
        "granted",
        "contacts",
        "contactsOnMonzo",
        "from",
        "method",
        "authenticate",
        "appShortcutType",
        "fingerprintAuth",
        "activeNotifications",
        "nfcEnabled",
        "diligence",
        "enabled",
        "gpsAdid",
        "traceId",
        "type",
        "path",
        "apiStatusCode",
        "apiErrorCode",
        "apiErrorMessage",
        "logoutReason",
        "userId",
        "errorType",
        "sampleError",
        "firstSampleDelta",
        "",
        "secondSampleDelta",
        "memoryError",
        "videoErrorReason",
        "splitWith",
        "firstTime",
        "feedback",
        "reportMonth",
        "androidPayEnabled",
        "fallback",
        "notificationType",
        "notificationSubType",
        "optedIn",
        "topicId",
        "outcome",
        "nameOnCard",
        "fileType",
        "(Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Boolean;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V",
        "Ljava/lang/Integer;",
        "Ljava/lang/Boolean;",
        "Ljava/lang/Long;",
        "component1",
        "()Ljava/lang/Integer;",
        "component10",
        "component11",
        "()Ljava/lang/Boolean;",
        "component12",
        "component13",
        "component14",
        "component15",
        "component16",
        "component17",
        "component18",
        "component19",
        "component2",
        "component20",
        "component21",
        "component22",
        "component23",
        "component24",
        "component25",
        "component26",
        "component27",
        "component28",
        "component29",
        "component3",
        "component30",
        "component31",
        "component32",
        "component33",
        "component34",
        "component35",
        "component36",
        "component37",
        "component38",
        "component39",
        "component4",
        "component40",
        "component41",
        "component42",
        "()Ljava/lang/Long;",
        "component43",
        "component44",
        "component45",
        "component46",
        "component47",
        "component48",
        "component49",
        "component5",
        "component50",
        "component51",
        "component52",
        "component53",
        "component54",
        "component55",
        "component56",
        "component57",
        "component58",
        "component6",
        "component7",
        "component8",
        "component9",
        "copy",
        "(Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Boolean;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lco/uk/getmondo/api/model/tracking/Data;",
        "equals",
        "other",
        "",
        "hashCode",
        "toString",
        "Companion",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# static fields
.field public static final Companion:Lco/uk/getmondo/api/model/tracking/Data$Companion;


# instance fields
.field private final activeNotifications:Ljava/lang/Integer;

.field private final amount:Ljava/lang/Integer;

.field private final androidPayEnabled:Ljava/lang/Boolean;
    .annotation runtime Lcom/google/gson/a/c;
        a = "androidpay_enabled"
    .end annotation
.end field

.field private final apiErrorCode:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/a/c;
        a = "api_error_code"
    .end annotation
.end field

.field private final apiErrorMessage:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/a/c;
        a = "api_error_message"
    .end annotation
.end field

.field private final apiStatusCode:Ljava/lang/Integer;
    .annotation runtime Lcom/google/gson/a/c;
        a = "api_status_code"
    .end annotation
.end field

.field private final appShortcutType:Ljava/lang/String;

.field private final authenticate:Ljava/lang/Boolean;

.field private final cameFrom:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/a/c;
        a = "came_from"
    .end annotation
.end field

.field private final category:Ljava/lang/String;

.field private final contacts:Ljava/lang/Integer;

.field private final contactsOnMonzo:Ljava/lang/Integer;

.field private final description:Ljava/lang/String;

.field private final diligence:Ljava/lang/String;

.field private final docDone:Ljava/lang/Boolean;
    .annotation runtime Lcom/google/gson/a/c;
        a = "doc_done"
    .end annotation
.end field

.field private final docType:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/a/c;
        a = "doc_type"
    .end annotation
.end field

.field private final eligible:Ljava/lang/Integer;

.field private final enabled:Ljava/lang/Boolean;

.field private final errorType:Ljava/lang/String;

.field private final fallback:Ljava/lang/Boolean;

.field private final feedback:Ljava/lang/String;

.field private final fileType:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/a/c;
        a = "file_type"
    .end annotation
.end field

.field private final fingerprintAuth:Ljava/lang/String;

.field private final firstSampleDelta:Ljava/lang/Long;

.field private final firstTime:Ljava/lang/Boolean;

.field private final from:Ljava/lang/String;

.field private final gpsAdid:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/a/c;
        a = "gps_adid"
    .end annotation
.end field

.field private final granted:Ljava/lang/Boolean;

.field private final kycRejected:Ljava/lang/Boolean;
    .annotation runtime Lcom/google/gson/a/c;
        a = "kyc_rejected"
    .end annotation
.end field

.field private final logoutReason:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/a/c;
        a = "logout_reason"
    .end annotation
.end field

.field private final memoryError:Ljava/lang/Boolean;

.field private final method:Ljava/lang/String;

.field private final month:Ljava/lang/Integer;

.field private final nameOnCard:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/a/c;
        a = "name_on_card"
    .end annotation
.end field

.field private final newCategory:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/a/c;
        a = "new_category"
    .end annotation
.end field

.field private final nfcEnabled:Ljava/lang/Boolean;

.field private final notificationSubType:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/a/c;
        a = "notification_sub_type"
    .end annotation
.end field

.field private final notificationType:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/a/c;
        a = "notification_type"
    .end annotation
.end field

.field private final oldCategory:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/a/c;
        a = "old_category"
    .end annotation
.end field

.field private final openedFrom:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/a/c;
        a = "opened_from"
    .end annotation
.end field

.field private final optedIn:Ljava/lang/Boolean;

.field private final outcome:Ljava/lang/String;

.field private final path:Ljava/lang/String;

.field private final redirectId:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/a/c;
        a = "play_store_redirect_event_id"
    .end annotation
.end field

.field private final referrer:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/a/c;
        a = "referrer"
    .end annotation
.end field

.field private final reportMonth:Ljava/lang/String;

.field private final sampleError:Ljava/lang/Boolean;

.field private final secondSampleDelta:Ljava/lang/Long;

.field private final selfieDone:Ljava/lang/Boolean;
    .annotation runtime Lcom/google/gson/a/c;
        a = "selfie_done"
    .end annotation
.end field

.field private final splitWith:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/a/c;
        a = "split_with"
    .end annotation
.end field

.field private final topicId:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/a/c;
        a = "topic_id"
    .end annotation
.end field

.field private final tourPage:Ljava/lang/Integer;
    .annotation runtime Lcom/google/gson/a/c;
        a = "tour_page"
    .end annotation
.end field

.field private final traceId:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/a/c;
        a = "trace_id"
    .end annotation
.end field

.field private final transactionId:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/a/c;
        a = "transaction_id"
    .end annotation
.end field

.field private final type:Ljava/lang/String;

.field private final url:Ljava/lang/String;

.field private final userId:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/a/c;
        a = "user_id"
    .end annotation
.end field

.field private final videoErrorReason:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lco/uk/getmondo/api/model/tracking/Data$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lco/uk/getmondo/api/model/tracking/Data$Companion;-><init>(Lkotlin/d/b/i;)V

    sput-object v0, Lco/uk/getmondo/api/model/tracking/Data;->Companion:Lco/uk/getmondo/api/model/tracking/Data$Companion;

    return-void
.end method

.method public constructor <init>()V
    .locals 62

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    const/16 v17, 0x0

    const/16 v18, 0x0

    const/16 v19, 0x0

    const/16 v20, 0x0

    const/16 v21, 0x0

    const/16 v22, 0x0

    const/16 v23, 0x0

    const/16 v24, 0x0

    const/16 v25, 0x0

    const/16 v26, 0x0

    const/16 v27, 0x0

    const/16 v28, 0x0

    const/16 v29, 0x0

    const/16 v30, 0x0

    const/16 v31, 0x0

    const/16 v32, 0x0

    const/16 v33, 0x0

    const/16 v34, 0x0

    const/16 v35, 0x0

    const/16 v36, 0x0

    const/16 v37, 0x0

    const/16 v38, 0x0

    const/16 v39, 0x0

    const/16 v40, 0x0

    const/16 v41, 0x0

    const/16 v42, 0x0

    const/16 v43, 0x0

    const/16 v44, 0x0

    const/16 v45, 0x0

    const/16 v46, 0x0

    const/16 v47, 0x0

    const/16 v48, 0x0

    const/16 v49, 0x0

    const/16 v50, 0x0

    const/16 v51, 0x0

    const/16 v52, 0x0

    const/16 v53, 0x0

    const/16 v54, 0x0

    const/16 v55, 0x0

    const/16 v56, 0x0

    const/16 v57, 0x0

    const/16 v58, 0x0

    const/16 v59, -0x1

    const v60, 0x3ffffff

    const/16 v61, 0x0

    move-object/from16 v0, p0

    invoke-direct/range {v0 .. v61}, Lco/uk/getmondo/api/model/tracking/Data;-><init>(Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Boolean;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IILkotlin/d/b/i;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Boolean;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lco/uk/getmondo/api/model/tracking/Data;->tourPage:Ljava/lang/Integer;

    iput-object p2, p0, Lco/uk/getmondo/api/model/tracking/Data;->openedFrom:Ljava/lang/String;

    iput-object p3, p0, Lco/uk/getmondo/api/model/tracking/Data;->transactionId:Ljava/lang/String;

    iput-object p4, p0, Lco/uk/getmondo/api/model/tracking/Data;->referrer:Ljava/lang/String;

    iput-object p5, p0, Lco/uk/getmondo/api/model/tracking/Data;->redirectId:Ljava/lang/String;

    iput-object p6, p0, Lco/uk/getmondo/api/model/tracking/Data;->url:Ljava/lang/String;

    iput-object p7, p0, Lco/uk/getmondo/api/model/tracking/Data;->eligible:Ljava/lang/Integer;

    iput-object p8, p0, Lco/uk/getmondo/api/model/tracking/Data;->amount:Ljava/lang/Integer;

    iput-object p9, p0, Lco/uk/getmondo/api/model/tracking/Data;->cameFrom:Ljava/lang/String;

    iput-object p10, p0, Lco/uk/getmondo/api/model/tracking/Data;->description:Ljava/lang/String;

    iput-object p11, p0, Lco/uk/getmondo/api/model/tracking/Data;->docDone:Ljava/lang/Boolean;

    iput-object p12, p0, Lco/uk/getmondo/api/model/tracking/Data;->selfieDone:Ljava/lang/Boolean;

    iput-object p13, p0, Lco/uk/getmondo/api/model/tracking/Data;->docType:Ljava/lang/String;

    iput-object p14, p0, Lco/uk/getmondo/api/model/tracking/Data;->kycRejected:Ljava/lang/Boolean;

    move-object/from16 v0, p15

    iput-object v0, p0, Lco/uk/getmondo/api/model/tracking/Data;->oldCategory:Ljava/lang/String;

    move-object/from16 v0, p16

    iput-object v0, p0, Lco/uk/getmondo/api/model/tracking/Data;->newCategory:Ljava/lang/String;

    move-object/from16 v0, p17

    iput-object v0, p0, Lco/uk/getmondo/api/model/tracking/Data;->category:Ljava/lang/String;

    move-object/from16 v0, p18

    iput-object v0, p0, Lco/uk/getmondo/api/model/tracking/Data;->month:Ljava/lang/Integer;

    move-object/from16 v0, p19

    iput-object v0, p0, Lco/uk/getmondo/api/model/tracking/Data;->granted:Ljava/lang/Boolean;

    move-object/from16 v0, p20

    iput-object v0, p0, Lco/uk/getmondo/api/model/tracking/Data;->contacts:Ljava/lang/Integer;

    move-object/from16 v0, p21

    iput-object v0, p0, Lco/uk/getmondo/api/model/tracking/Data;->contactsOnMonzo:Ljava/lang/Integer;

    move-object/from16 v0, p22

    iput-object v0, p0, Lco/uk/getmondo/api/model/tracking/Data;->from:Ljava/lang/String;

    move-object/from16 v0, p23

    iput-object v0, p0, Lco/uk/getmondo/api/model/tracking/Data;->method:Ljava/lang/String;

    move-object/from16 v0, p24

    iput-object v0, p0, Lco/uk/getmondo/api/model/tracking/Data;->authenticate:Ljava/lang/Boolean;

    move-object/from16 v0, p25

    iput-object v0, p0, Lco/uk/getmondo/api/model/tracking/Data;->appShortcutType:Ljava/lang/String;

    move-object/from16 v0, p26

    iput-object v0, p0, Lco/uk/getmondo/api/model/tracking/Data;->fingerprintAuth:Ljava/lang/String;

    move-object/from16 v0, p27

    iput-object v0, p0, Lco/uk/getmondo/api/model/tracking/Data;->activeNotifications:Ljava/lang/Integer;

    move-object/from16 v0, p28

    iput-object v0, p0, Lco/uk/getmondo/api/model/tracking/Data;->nfcEnabled:Ljava/lang/Boolean;

    move-object/from16 v0, p29

    iput-object v0, p0, Lco/uk/getmondo/api/model/tracking/Data;->diligence:Ljava/lang/String;

    move-object/from16 v0, p30

    iput-object v0, p0, Lco/uk/getmondo/api/model/tracking/Data;->enabled:Ljava/lang/Boolean;

    move-object/from16 v0, p31

    iput-object v0, p0, Lco/uk/getmondo/api/model/tracking/Data;->gpsAdid:Ljava/lang/String;

    move-object/from16 v0, p32

    iput-object v0, p0, Lco/uk/getmondo/api/model/tracking/Data;->traceId:Ljava/lang/String;

    move-object/from16 v0, p33

    iput-object v0, p0, Lco/uk/getmondo/api/model/tracking/Data;->type:Ljava/lang/String;

    move-object/from16 v0, p34

    iput-object v0, p0, Lco/uk/getmondo/api/model/tracking/Data;->path:Ljava/lang/String;

    move-object/from16 v0, p35

    iput-object v0, p0, Lco/uk/getmondo/api/model/tracking/Data;->apiStatusCode:Ljava/lang/Integer;

    move-object/from16 v0, p36

    iput-object v0, p0, Lco/uk/getmondo/api/model/tracking/Data;->apiErrorCode:Ljava/lang/String;

    move-object/from16 v0, p37

    iput-object v0, p0, Lco/uk/getmondo/api/model/tracking/Data;->apiErrorMessage:Ljava/lang/String;

    move-object/from16 v0, p38

    iput-object v0, p0, Lco/uk/getmondo/api/model/tracking/Data;->logoutReason:Ljava/lang/String;

    move-object/from16 v0, p39

    iput-object v0, p0, Lco/uk/getmondo/api/model/tracking/Data;->userId:Ljava/lang/String;

    move-object/from16 v0, p40

    iput-object v0, p0, Lco/uk/getmondo/api/model/tracking/Data;->errorType:Ljava/lang/String;

    move-object/from16 v0, p41

    iput-object v0, p0, Lco/uk/getmondo/api/model/tracking/Data;->sampleError:Ljava/lang/Boolean;

    move-object/from16 v0, p42

    iput-object v0, p0, Lco/uk/getmondo/api/model/tracking/Data;->firstSampleDelta:Ljava/lang/Long;

    move-object/from16 v0, p43

    iput-object v0, p0, Lco/uk/getmondo/api/model/tracking/Data;->secondSampleDelta:Ljava/lang/Long;

    move-object/from16 v0, p44

    iput-object v0, p0, Lco/uk/getmondo/api/model/tracking/Data;->memoryError:Ljava/lang/Boolean;

    move-object/from16 v0, p45

    iput-object v0, p0, Lco/uk/getmondo/api/model/tracking/Data;->videoErrorReason:Ljava/lang/String;

    move-object/from16 v0, p46

    iput-object v0, p0, Lco/uk/getmondo/api/model/tracking/Data;->splitWith:Ljava/lang/String;

    move-object/from16 v0, p47

    iput-object v0, p0, Lco/uk/getmondo/api/model/tracking/Data;->firstTime:Ljava/lang/Boolean;

    move-object/from16 v0, p48

    iput-object v0, p0, Lco/uk/getmondo/api/model/tracking/Data;->feedback:Ljava/lang/String;

    move-object/from16 v0, p49

    iput-object v0, p0, Lco/uk/getmondo/api/model/tracking/Data;->reportMonth:Ljava/lang/String;

    move-object/from16 v0, p50

    iput-object v0, p0, Lco/uk/getmondo/api/model/tracking/Data;->androidPayEnabled:Ljava/lang/Boolean;

    move-object/from16 v0, p51

    iput-object v0, p0, Lco/uk/getmondo/api/model/tracking/Data;->fallback:Ljava/lang/Boolean;

    move-object/from16 v0, p52

    iput-object v0, p0, Lco/uk/getmondo/api/model/tracking/Data;->notificationType:Ljava/lang/String;

    move-object/from16 v0, p53

    iput-object v0, p0, Lco/uk/getmondo/api/model/tracking/Data;->notificationSubType:Ljava/lang/String;

    move-object/from16 v0, p54

    iput-object v0, p0, Lco/uk/getmondo/api/model/tracking/Data;->optedIn:Ljava/lang/Boolean;

    move-object/from16 v0, p55

    iput-object v0, p0, Lco/uk/getmondo/api/model/tracking/Data;->topicId:Ljava/lang/String;

    move-object/from16 v0, p56

    iput-object v0, p0, Lco/uk/getmondo/api/model/tracking/Data;->outcome:Ljava/lang/String;

    move-object/from16 v0, p57

    iput-object v0, p0, Lco/uk/getmondo/api/model/tracking/Data;->nameOnCard:Ljava/lang/String;

    move-object/from16 v0, p58

    iput-object v0, p0, Lco/uk/getmondo/api/model/tracking/Data;->fileType:Ljava/lang/String;

    return-void
.end method

.method public synthetic constructor <init>(Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Boolean;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IILkotlin/d/b/i;)V
    .locals 60

    .prologue
    and-int/lit8 v1, p59, 0x1

    if-eqz v1, :cond_39

    .line 11
    const/4 v1, 0x0

    check-cast v1, Ljava/lang/Integer;

    move-object v2, v1

    :goto_0
    and-int/lit8 v1, p59, 0x2

    if-eqz v1, :cond_38

    .line 12
    const/4 v1, 0x0

    check-cast v1, Ljava/lang/String;

    move-object v3, v1

    :goto_1
    and-int/lit8 v1, p59, 0x4

    if-eqz v1, :cond_37

    .line 13
    const/4 v1, 0x0

    check-cast v1, Ljava/lang/String;

    move-object v4, v1

    :goto_2
    and-int/lit8 v1, p59, 0x8

    if-eqz v1, :cond_36

    .line 14
    const/4 v1, 0x0

    check-cast v1, Ljava/lang/String;

    move-object v5, v1

    :goto_3
    and-int/lit8 v1, p59, 0x10

    if-eqz v1, :cond_35

    .line 15
    const/4 v1, 0x0

    check-cast v1, Ljava/lang/String;

    move-object v6, v1

    :goto_4
    and-int/lit8 v1, p59, 0x20

    if-eqz v1, :cond_34

    .line 16
    const/4 v1, 0x0

    check-cast v1, Ljava/lang/String;

    move-object v7, v1

    :goto_5
    and-int/lit8 v1, p59, 0x40

    if-eqz v1, :cond_33

    .line 17
    const/4 v1, 0x0

    check-cast v1, Ljava/lang/Integer;

    move-object v8, v1

    :goto_6
    move/from16 v0, p59

    and-int/lit16 v1, v0, 0x80

    if-eqz v1, :cond_32

    .line 18
    const/4 v1, 0x0

    check-cast v1, Ljava/lang/Integer;

    move-object v9, v1

    :goto_7
    move/from16 v0, p59

    and-int/lit16 v1, v0, 0x100

    if-eqz v1, :cond_31

    .line 19
    const/4 v1, 0x0

    check-cast v1, Ljava/lang/String;

    move-object v10, v1

    :goto_8
    move/from16 v0, p59

    and-int/lit16 v1, v0, 0x200

    if-eqz v1, :cond_30

    .line 20
    const/4 v1, 0x0

    check-cast v1, Ljava/lang/String;

    move-object v11, v1

    :goto_9
    move/from16 v0, p59

    and-int/lit16 v1, v0, 0x400

    if-eqz v1, :cond_2f

    .line 21
    const/4 v1, 0x0

    check-cast v1, Ljava/lang/Boolean;

    move-object v12, v1

    :goto_a
    move/from16 v0, p59

    and-int/lit16 v1, v0, 0x800

    if-eqz v1, :cond_2e

    .line 22
    const/4 v1, 0x0

    check-cast v1, Ljava/lang/Boolean;

    move-object v13, v1

    :goto_b
    move/from16 v0, p59

    and-int/lit16 v1, v0, 0x1000

    if-eqz v1, :cond_2d

    .line 23
    const/4 v1, 0x0

    check-cast v1, Ljava/lang/String;

    move-object v14, v1

    :goto_c
    move/from16 v0, p59

    and-int/lit16 v1, v0, 0x2000

    if-eqz v1, :cond_2c

    .line 24
    const/4 v1, 0x0

    check-cast v1, Ljava/lang/Boolean;

    move-object v15, v1

    :goto_d
    move/from16 v0, p59

    and-int/lit16 v1, v0, 0x4000

    if-eqz v1, :cond_2b

    .line 25
    const/4 v1, 0x0

    check-cast v1, Ljava/lang/String;

    move-object/from16 v16, v1

    :goto_e
    const v1, 0x8000

    and-int v1, v1, p59

    if-eqz v1, :cond_2a

    .line 26
    const/4 v1, 0x0

    check-cast v1, Ljava/lang/String;

    move-object/from16 v17, v1

    :goto_f
    const/high16 v1, 0x10000

    and-int v1, v1, p59

    if-eqz v1, :cond_29

    .line 27
    const/4 v1, 0x0

    check-cast v1, Ljava/lang/String;

    move-object/from16 v18, v1

    :goto_10
    const/high16 v1, 0x20000

    and-int v1, v1, p59

    if-eqz v1, :cond_28

    .line 28
    const/4 v1, 0x0

    check-cast v1, Ljava/lang/Integer;

    move-object/from16 v19, v1

    :goto_11
    const/high16 v1, 0x40000

    and-int v1, v1, p59

    if-eqz v1, :cond_27

    .line 29
    const/4 v1, 0x0

    check-cast v1, Ljava/lang/Boolean;

    move-object/from16 v20, v1

    :goto_12
    const/high16 v1, 0x80000

    and-int v1, v1, p59

    if-eqz v1, :cond_26

    .line 30
    const/4 v1, 0x0

    check-cast v1, Ljava/lang/Integer;

    move-object/from16 v21, v1

    :goto_13
    const/high16 v1, 0x100000

    and-int v1, v1, p59

    if-eqz v1, :cond_25

    .line 31
    const/4 v1, 0x0

    check-cast v1, Ljava/lang/Integer;

    move-object/from16 v22, v1

    :goto_14
    const/high16 v1, 0x200000

    and-int v1, v1, p59

    if-eqz v1, :cond_24

    .line 32
    const/4 v1, 0x0

    check-cast v1, Ljava/lang/String;

    move-object/from16 v23, v1

    :goto_15
    const/high16 v1, 0x400000

    and-int v1, v1, p59

    if-eqz v1, :cond_23

    .line 33
    const/4 v1, 0x0

    check-cast v1, Ljava/lang/String;

    move-object/from16 v24, v1

    :goto_16
    const/high16 v1, 0x800000

    and-int v1, v1, p59

    if-eqz v1, :cond_22

    .line 34
    const/4 v1, 0x0

    check-cast v1, Ljava/lang/Boolean;

    move-object/from16 v25, v1

    :goto_17
    const/high16 v1, 0x1000000

    and-int v1, v1, p59

    if-eqz v1, :cond_21

    .line 35
    const/4 v1, 0x0

    check-cast v1, Ljava/lang/String;

    move-object/from16 v26, v1

    :goto_18
    const/high16 v1, 0x2000000

    and-int v1, v1, p59

    if-eqz v1, :cond_20

    .line 36
    const/4 v1, 0x0

    check-cast v1, Ljava/lang/String;

    move-object/from16 v27, v1

    :goto_19
    const/high16 v1, 0x4000000

    and-int v1, v1, p59

    if-eqz v1, :cond_1f

    .line 37
    const/4 v1, 0x0

    check-cast v1, Ljava/lang/Integer;

    move-object/from16 v28, v1

    :goto_1a
    const/high16 v1, 0x8000000

    and-int v1, v1, p59

    if-eqz v1, :cond_1e

    .line 38
    const/4 v1, 0x0

    check-cast v1, Ljava/lang/Boolean;

    move-object/from16 v29, v1

    :goto_1b
    const/high16 v1, 0x10000000

    and-int v1, v1, p59

    if-eqz v1, :cond_1d

    .line 39
    const/4 v1, 0x0

    check-cast v1, Ljava/lang/String;

    move-object/from16 v30, v1

    :goto_1c
    const/high16 v1, 0x20000000

    and-int v1, v1, p59

    if-eqz v1, :cond_1c

    .line 40
    const/4 v1, 0x0

    check-cast v1, Ljava/lang/Boolean;

    move-object/from16 v31, v1

    :goto_1d
    const/high16 v1, 0x40000000    # 2.0f

    and-int v1, v1, p59

    if-eqz v1, :cond_1b

    .line 42
    const/4 v1, 0x0

    check-cast v1, Ljava/lang/String;

    move-object/from16 v32, v1

    :goto_1e
    const/high16 v1, -0x80000000

    and-int v1, v1, p59

    if-eqz v1, :cond_1a

    .line 43
    const/4 v1, 0x0

    check-cast v1, Ljava/lang/String;

    move-object/from16 v33, v1

    :goto_1f
    and-int/lit8 v1, p60, 0x1

    if-eqz v1, :cond_19

    .line 44
    const/4 v1, 0x0

    check-cast v1, Ljava/lang/String;

    move-object/from16 v34, v1

    :goto_20
    and-int/lit8 v1, p60, 0x2

    if-eqz v1, :cond_18

    .line 45
    const/4 v1, 0x0

    check-cast v1, Ljava/lang/String;

    move-object/from16 v35, v1

    :goto_21
    and-int/lit8 v1, p60, 0x4

    if-eqz v1, :cond_17

    .line 46
    const/4 v1, 0x0

    check-cast v1, Ljava/lang/Integer;

    move-object/from16 v36, v1

    :goto_22
    and-int/lit8 v1, p60, 0x8

    if-eqz v1, :cond_16

    .line 47
    const/4 v1, 0x0

    check-cast v1, Ljava/lang/String;

    move-object/from16 v37, v1

    :goto_23
    and-int/lit8 v1, p60, 0x10

    if-eqz v1, :cond_15

    .line 48
    const/4 v1, 0x0

    check-cast v1, Ljava/lang/String;

    move-object/from16 v38, v1

    :goto_24
    and-int/lit8 v1, p60, 0x20

    if-eqz v1, :cond_14

    .line 49
    const/4 v1, 0x0

    check-cast v1, Ljava/lang/String;

    move-object/from16 v39, v1

    :goto_25
    and-int/lit8 v1, p60, 0x40

    if-eqz v1, :cond_13

    .line 50
    const/4 v1, 0x0

    check-cast v1, Ljava/lang/String;

    move-object/from16 v40, v1

    :goto_26
    move/from16 v0, p60

    and-int/lit16 v1, v0, 0x80

    if-eqz v1, :cond_12

    .line 52
    const/4 v1, 0x0

    check-cast v1, Ljava/lang/String;

    move-object/from16 v41, v1

    :goto_27
    move/from16 v0, p60

    and-int/lit16 v1, v0, 0x100

    if-eqz v1, :cond_11

    .line 53
    const/4 v1, 0x0

    check-cast v1, Ljava/lang/Boolean;

    move-object/from16 v42, v1

    :goto_28
    move/from16 v0, p60

    and-int/lit16 v1, v0, 0x200

    if-eqz v1, :cond_10

    .line 54
    const/4 v1, 0x0

    check-cast v1, Ljava/lang/Long;

    move-object/from16 v43, v1

    :goto_29
    move/from16 v0, p60

    and-int/lit16 v1, v0, 0x400

    if-eqz v1, :cond_f

    .line 55
    const/4 v1, 0x0

    check-cast v1, Ljava/lang/Long;

    move-object/from16 v44, v1

    :goto_2a
    move/from16 v0, p60

    and-int/lit16 v1, v0, 0x800

    if-eqz v1, :cond_e

    .line 56
    const/4 v1, 0x0

    check-cast v1, Ljava/lang/Boolean;

    move-object/from16 v45, v1

    :goto_2b
    move/from16 v0, p60

    and-int/lit16 v1, v0, 0x1000

    if-eqz v1, :cond_d

    .line 57
    const/4 v1, 0x0

    check-cast v1, Ljava/lang/String;

    move-object/from16 v46, v1

    :goto_2c
    move/from16 v0, p60

    and-int/lit16 v1, v0, 0x2000

    if-eqz v1, :cond_c

    .line 58
    const/4 v1, 0x0

    check-cast v1, Ljava/lang/String;

    move-object/from16 v47, v1

    :goto_2d
    move/from16 v0, p60

    and-int/lit16 v1, v0, 0x4000

    if-eqz v1, :cond_b

    .line 59
    const/4 v1, 0x0

    check-cast v1, Ljava/lang/Boolean;

    move-object/from16 v48, v1

    :goto_2e
    const v1, 0x8000

    and-int v1, v1, p60

    if-eqz v1, :cond_a

    .line 61
    const/4 v1, 0x0

    check-cast v1, Ljava/lang/String;

    move-object/from16 v49, v1

    :goto_2f
    const/high16 v1, 0x10000

    and-int v1, v1, p60

    if-eqz v1, :cond_9

    .line 62
    const/4 v1, 0x0

    check-cast v1, Ljava/lang/String;

    move-object/from16 v50, v1

    :goto_30
    const/high16 v1, 0x20000

    and-int v1, v1, p60

    if-eqz v1, :cond_8

    .line 64
    const/4 v1, 0x0

    check-cast v1, Ljava/lang/Boolean;

    move-object/from16 v51, v1

    :goto_31
    const/high16 v1, 0x40000

    and-int v1, v1, p60

    if-eqz v1, :cond_7

    .line 66
    const/4 v1, 0x0

    check-cast v1, Ljava/lang/Boolean;

    move-object/from16 v52, v1

    :goto_32
    const/high16 v1, 0x80000

    and-int v1, v1, p60

    if-eqz v1, :cond_6

    .line 68
    const/4 v1, 0x0

    check-cast v1, Ljava/lang/String;

    move-object/from16 v53, v1

    :goto_33
    const/high16 v1, 0x100000

    and-int v1, v1, p60

    if-eqz v1, :cond_5

    .line 69
    const/4 v1, 0x0

    check-cast v1, Ljava/lang/String;

    move-object/from16 v54, v1

    :goto_34
    const/high16 v1, 0x200000

    and-int v1, v1, p60

    if-eqz v1, :cond_4

    .line 71
    const/4 v1, 0x0

    check-cast v1, Ljava/lang/Boolean;

    move-object/from16 v55, v1

    :goto_35
    const/high16 v1, 0x400000

    and-int v1, v1, p60

    if-eqz v1, :cond_3

    .line 73
    const/4 v1, 0x0

    check-cast v1, Ljava/lang/String;

    move-object/from16 v56, v1

    :goto_36
    const/high16 v1, 0x800000

    and-int v1, v1, p60

    if-eqz v1, :cond_2

    .line 74
    const/4 v1, 0x0

    check-cast v1, Ljava/lang/String;

    move-object/from16 v57, v1

    :goto_37
    const/high16 v1, 0x1000000

    and-int v1, v1, p60

    if-eqz v1, :cond_1

    .line 76
    const/4 v1, 0x0

    check-cast v1, Ljava/lang/String;

    move-object/from16 v58, v1

    :goto_38
    const/high16 v1, 0x2000000

    and-int v1, v1, p60

    if-eqz v1, :cond_0

    .line 78
    const/4 v1, 0x0

    check-cast v1, Ljava/lang/String;

    move-object/from16 v59, v1

    :goto_39
    move-object/from16 v1, p0

    invoke-direct/range {v1 .. v59}, Lco/uk/getmondo/api/model/tracking/Data;-><init>(Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Boolean;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void

    :cond_0
    move-object/from16 v59, p58

    goto :goto_39

    :cond_1
    move-object/from16 v58, p57

    goto :goto_38

    :cond_2
    move-object/from16 v57, p56

    goto :goto_37

    :cond_3
    move-object/from16 v56, p55

    goto :goto_36

    :cond_4
    move-object/from16 v55, p54

    goto :goto_35

    :cond_5
    move-object/from16 v54, p53

    goto :goto_34

    :cond_6
    move-object/from16 v53, p52

    goto :goto_33

    :cond_7
    move-object/from16 v52, p51

    goto :goto_32

    :cond_8
    move-object/from16 v51, p50

    goto :goto_31

    :cond_9
    move-object/from16 v50, p49

    goto/16 :goto_30

    :cond_a
    move-object/from16 v49, p48

    goto/16 :goto_2f

    :cond_b
    move-object/from16 v48, p47

    goto/16 :goto_2e

    :cond_c
    move-object/from16 v47, p46

    goto/16 :goto_2d

    :cond_d
    move-object/from16 v46, p45

    goto/16 :goto_2c

    :cond_e
    move-object/from16 v45, p44

    goto/16 :goto_2b

    :cond_f
    move-object/from16 v44, p43

    goto/16 :goto_2a

    :cond_10
    move-object/from16 v43, p42

    goto/16 :goto_29

    :cond_11
    move-object/from16 v42, p41

    goto/16 :goto_28

    :cond_12
    move-object/from16 v41, p40

    goto/16 :goto_27

    :cond_13
    move-object/from16 v40, p39

    goto/16 :goto_26

    :cond_14
    move-object/from16 v39, p38

    goto/16 :goto_25

    :cond_15
    move-object/from16 v38, p37

    goto/16 :goto_24

    :cond_16
    move-object/from16 v37, p36

    goto/16 :goto_23

    :cond_17
    move-object/from16 v36, p35

    goto/16 :goto_22

    :cond_18
    move-object/from16 v35, p34

    goto/16 :goto_21

    :cond_19
    move-object/from16 v34, p33

    goto/16 :goto_20

    :cond_1a
    move-object/from16 v33, p32

    goto/16 :goto_1f

    :cond_1b
    move-object/from16 v32, p31

    goto/16 :goto_1e

    :cond_1c
    move-object/from16 v31, p30

    goto/16 :goto_1d

    :cond_1d
    move-object/from16 v30, p29

    goto/16 :goto_1c

    :cond_1e
    move-object/from16 v29, p28

    goto/16 :goto_1b

    :cond_1f
    move-object/from16 v28, p27

    goto/16 :goto_1a

    :cond_20
    move-object/from16 v27, p26

    goto/16 :goto_19

    :cond_21
    move-object/from16 v26, p25

    goto/16 :goto_18

    :cond_22
    move-object/from16 v25, p24

    goto/16 :goto_17

    :cond_23
    move-object/from16 v24, p23

    goto/16 :goto_16

    :cond_24
    move-object/from16 v23, p22

    goto/16 :goto_15

    :cond_25
    move-object/from16 v22, p21

    goto/16 :goto_14

    :cond_26
    move-object/from16 v21, p20

    goto/16 :goto_13

    :cond_27
    move-object/from16 v20, p19

    goto/16 :goto_12

    :cond_28
    move-object/from16 v19, p18

    goto/16 :goto_11

    :cond_29
    move-object/from16 v18, p17

    goto/16 :goto_10

    :cond_2a
    move-object/from16 v17, p16

    goto/16 :goto_f

    :cond_2b
    move-object/from16 v16, p15

    goto/16 :goto_e

    :cond_2c
    move-object/from16 v15, p14

    goto/16 :goto_d

    :cond_2d
    move-object/from16 v14, p13

    goto/16 :goto_c

    :cond_2e
    move-object/from16 v13, p12

    goto/16 :goto_b

    :cond_2f
    move-object/from16 v12, p11

    goto/16 :goto_a

    :cond_30
    move-object/from16 v11, p10

    goto/16 :goto_9

    :cond_31
    move-object/from16 v10, p9

    goto/16 :goto_8

    :cond_32
    move-object/from16 v9, p8

    goto/16 :goto_7

    :cond_33
    move-object/from16 v8, p7

    goto/16 :goto_6

    :cond_34
    move-object/from16 v7, p6

    goto/16 :goto_5

    :cond_35
    move-object/from16 v6, p5

    goto/16 :goto_4

    :cond_36
    move-object/from16 v5, p4

    goto/16 :goto_3

    :cond_37
    move-object/from16 v4, p3

    goto/16 :goto_2

    :cond_38
    move-object/from16 v3, p2

    goto/16 :goto_1

    :cond_39
    move-object/from16 v2, p1

    goto/16 :goto_0
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_0

    instance-of v0, p1, Lco/uk/getmondo/api/model/tracking/Data;

    if-eqz v0, :cond_1

    check-cast p1, Lco/uk/getmondo/api/model/tracking/Data;

    iget-object v0, p0, Lco/uk/getmondo/api/model/tracking/Data;->tourPage:Ljava/lang/Integer;

    iget-object v1, p1, Lco/uk/getmondo/api/model/tracking/Data;->tourPage:Ljava/lang/Integer;

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lco/uk/getmondo/api/model/tracking/Data;->openedFrom:Ljava/lang/String;

    iget-object v1, p1, Lco/uk/getmondo/api/model/tracking/Data;->openedFrom:Ljava/lang/String;

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lco/uk/getmondo/api/model/tracking/Data;->transactionId:Ljava/lang/String;

    iget-object v1, p1, Lco/uk/getmondo/api/model/tracking/Data;->transactionId:Ljava/lang/String;

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lco/uk/getmondo/api/model/tracking/Data;->referrer:Ljava/lang/String;

    iget-object v1, p1, Lco/uk/getmondo/api/model/tracking/Data;->referrer:Ljava/lang/String;

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lco/uk/getmondo/api/model/tracking/Data;->redirectId:Ljava/lang/String;

    iget-object v1, p1, Lco/uk/getmondo/api/model/tracking/Data;->redirectId:Ljava/lang/String;

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lco/uk/getmondo/api/model/tracking/Data;->url:Ljava/lang/String;

    iget-object v1, p1, Lco/uk/getmondo/api/model/tracking/Data;->url:Ljava/lang/String;

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lco/uk/getmondo/api/model/tracking/Data;->eligible:Ljava/lang/Integer;

    iget-object v1, p1, Lco/uk/getmondo/api/model/tracking/Data;->eligible:Ljava/lang/Integer;

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lco/uk/getmondo/api/model/tracking/Data;->amount:Ljava/lang/Integer;

    iget-object v1, p1, Lco/uk/getmondo/api/model/tracking/Data;->amount:Ljava/lang/Integer;

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lco/uk/getmondo/api/model/tracking/Data;->cameFrom:Ljava/lang/String;

    iget-object v1, p1, Lco/uk/getmondo/api/model/tracking/Data;->cameFrom:Ljava/lang/String;

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lco/uk/getmondo/api/model/tracking/Data;->description:Ljava/lang/String;

    iget-object v1, p1, Lco/uk/getmondo/api/model/tracking/Data;->description:Ljava/lang/String;

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lco/uk/getmondo/api/model/tracking/Data;->docDone:Ljava/lang/Boolean;

    iget-object v1, p1, Lco/uk/getmondo/api/model/tracking/Data;->docDone:Ljava/lang/Boolean;

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lco/uk/getmondo/api/model/tracking/Data;->selfieDone:Ljava/lang/Boolean;

    iget-object v1, p1, Lco/uk/getmondo/api/model/tracking/Data;->selfieDone:Ljava/lang/Boolean;

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lco/uk/getmondo/api/model/tracking/Data;->docType:Ljava/lang/String;

    iget-object v1, p1, Lco/uk/getmondo/api/model/tracking/Data;->docType:Ljava/lang/String;

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lco/uk/getmondo/api/model/tracking/Data;->kycRejected:Ljava/lang/Boolean;

    iget-object v1, p1, Lco/uk/getmondo/api/model/tracking/Data;->kycRejected:Ljava/lang/Boolean;

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lco/uk/getmondo/api/model/tracking/Data;->oldCategory:Ljava/lang/String;

    iget-object v1, p1, Lco/uk/getmondo/api/model/tracking/Data;->oldCategory:Ljava/lang/String;

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lco/uk/getmondo/api/model/tracking/Data;->newCategory:Ljava/lang/String;

    iget-object v1, p1, Lco/uk/getmondo/api/model/tracking/Data;->newCategory:Ljava/lang/String;

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lco/uk/getmondo/api/model/tracking/Data;->category:Ljava/lang/String;

    iget-object v1, p1, Lco/uk/getmondo/api/model/tracking/Data;->category:Ljava/lang/String;

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lco/uk/getmondo/api/model/tracking/Data;->month:Ljava/lang/Integer;

    iget-object v1, p1, Lco/uk/getmondo/api/model/tracking/Data;->month:Ljava/lang/Integer;

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lco/uk/getmondo/api/model/tracking/Data;->granted:Ljava/lang/Boolean;

    iget-object v1, p1, Lco/uk/getmondo/api/model/tracking/Data;->granted:Ljava/lang/Boolean;

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lco/uk/getmondo/api/model/tracking/Data;->contacts:Ljava/lang/Integer;

    iget-object v1, p1, Lco/uk/getmondo/api/model/tracking/Data;->contacts:Ljava/lang/Integer;

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lco/uk/getmondo/api/model/tracking/Data;->contactsOnMonzo:Ljava/lang/Integer;

    iget-object v1, p1, Lco/uk/getmondo/api/model/tracking/Data;->contactsOnMonzo:Ljava/lang/Integer;

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lco/uk/getmondo/api/model/tracking/Data;->from:Ljava/lang/String;

    iget-object v1, p1, Lco/uk/getmondo/api/model/tracking/Data;->from:Ljava/lang/String;

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lco/uk/getmondo/api/model/tracking/Data;->method:Ljava/lang/String;

    iget-object v1, p1, Lco/uk/getmondo/api/model/tracking/Data;->method:Ljava/lang/String;

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lco/uk/getmondo/api/model/tracking/Data;->authenticate:Ljava/lang/Boolean;

    iget-object v1, p1, Lco/uk/getmondo/api/model/tracking/Data;->authenticate:Ljava/lang/Boolean;

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lco/uk/getmondo/api/model/tracking/Data;->appShortcutType:Ljava/lang/String;

    iget-object v1, p1, Lco/uk/getmondo/api/model/tracking/Data;->appShortcutType:Ljava/lang/String;

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lco/uk/getmondo/api/model/tracking/Data;->fingerprintAuth:Ljava/lang/String;

    iget-object v1, p1, Lco/uk/getmondo/api/model/tracking/Data;->fingerprintAuth:Ljava/lang/String;

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lco/uk/getmondo/api/model/tracking/Data;->activeNotifications:Ljava/lang/Integer;

    iget-object v1, p1, Lco/uk/getmondo/api/model/tracking/Data;->activeNotifications:Ljava/lang/Integer;

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lco/uk/getmondo/api/model/tracking/Data;->nfcEnabled:Ljava/lang/Boolean;

    iget-object v1, p1, Lco/uk/getmondo/api/model/tracking/Data;->nfcEnabled:Ljava/lang/Boolean;

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lco/uk/getmondo/api/model/tracking/Data;->diligence:Ljava/lang/String;

    iget-object v1, p1, Lco/uk/getmondo/api/model/tracking/Data;->diligence:Ljava/lang/String;

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lco/uk/getmondo/api/model/tracking/Data;->enabled:Ljava/lang/Boolean;

    iget-object v1, p1, Lco/uk/getmondo/api/model/tracking/Data;->enabled:Ljava/lang/Boolean;

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lco/uk/getmondo/api/model/tracking/Data;->gpsAdid:Ljava/lang/String;

    iget-object v1, p1, Lco/uk/getmondo/api/model/tracking/Data;->gpsAdid:Ljava/lang/String;

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lco/uk/getmondo/api/model/tracking/Data;->traceId:Ljava/lang/String;

    iget-object v1, p1, Lco/uk/getmondo/api/model/tracking/Data;->traceId:Ljava/lang/String;

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lco/uk/getmondo/api/model/tracking/Data;->type:Ljava/lang/String;

    iget-object v1, p1, Lco/uk/getmondo/api/model/tracking/Data;->type:Ljava/lang/String;

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lco/uk/getmondo/api/model/tracking/Data;->path:Ljava/lang/String;

    iget-object v1, p1, Lco/uk/getmondo/api/model/tracking/Data;->path:Ljava/lang/String;

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lco/uk/getmondo/api/model/tracking/Data;->apiStatusCode:Ljava/lang/Integer;

    iget-object v1, p1, Lco/uk/getmondo/api/model/tracking/Data;->apiStatusCode:Ljava/lang/Integer;

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lco/uk/getmondo/api/model/tracking/Data;->apiErrorCode:Ljava/lang/String;

    iget-object v1, p1, Lco/uk/getmondo/api/model/tracking/Data;->apiErrorCode:Ljava/lang/String;

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lco/uk/getmondo/api/model/tracking/Data;->apiErrorMessage:Ljava/lang/String;

    iget-object v1, p1, Lco/uk/getmondo/api/model/tracking/Data;->apiErrorMessage:Ljava/lang/String;

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lco/uk/getmondo/api/model/tracking/Data;->logoutReason:Ljava/lang/String;

    iget-object v1, p1, Lco/uk/getmondo/api/model/tracking/Data;->logoutReason:Ljava/lang/String;

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lco/uk/getmondo/api/model/tracking/Data;->userId:Ljava/lang/String;

    iget-object v1, p1, Lco/uk/getmondo/api/model/tracking/Data;->userId:Ljava/lang/String;

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lco/uk/getmondo/api/model/tracking/Data;->errorType:Ljava/lang/String;

    iget-object v1, p1, Lco/uk/getmondo/api/model/tracking/Data;->errorType:Ljava/lang/String;

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lco/uk/getmondo/api/model/tracking/Data;->sampleError:Ljava/lang/Boolean;

    iget-object v1, p1, Lco/uk/getmondo/api/model/tracking/Data;->sampleError:Ljava/lang/Boolean;

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lco/uk/getmondo/api/model/tracking/Data;->firstSampleDelta:Ljava/lang/Long;

    iget-object v1, p1, Lco/uk/getmondo/api/model/tracking/Data;->firstSampleDelta:Ljava/lang/Long;

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lco/uk/getmondo/api/model/tracking/Data;->secondSampleDelta:Ljava/lang/Long;

    iget-object v1, p1, Lco/uk/getmondo/api/model/tracking/Data;->secondSampleDelta:Ljava/lang/Long;

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lco/uk/getmondo/api/model/tracking/Data;->memoryError:Ljava/lang/Boolean;

    iget-object v1, p1, Lco/uk/getmondo/api/model/tracking/Data;->memoryError:Ljava/lang/Boolean;

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lco/uk/getmondo/api/model/tracking/Data;->videoErrorReason:Ljava/lang/String;

    iget-object v1, p1, Lco/uk/getmondo/api/model/tracking/Data;->videoErrorReason:Ljava/lang/String;

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lco/uk/getmondo/api/model/tracking/Data;->splitWith:Ljava/lang/String;

    iget-object v1, p1, Lco/uk/getmondo/api/model/tracking/Data;->splitWith:Ljava/lang/String;

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lco/uk/getmondo/api/model/tracking/Data;->firstTime:Ljava/lang/Boolean;

    iget-object v1, p1, Lco/uk/getmondo/api/model/tracking/Data;->firstTime:Ljava/lang/Boolean;

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lco/uk/getmondo/api/model/tracking/Data;->feedback:Ljava/lang/String;

    iget-object v1, p1, Lco/uk/getmondo/api/model/tracking/Data;->feedback:Ljava/lang/String;

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lco/uk/getmondo/api/model/tracking/Data;->reportMonth:Ljava/lang/String;

    iget-object v1, p1, Lco/uk/getmondo/api/model/tracking/Data;->reportMonth:Ljava/lang/String;

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lco/uk/getmondo/api/model/tracking/Data;->androidPayEnabled:Ljava/lang/Boolean;

    iget-object v1, p1, Lco/uk/getmondo/api/model/tracking/Data;->androidPayEnabled:Ljava/lang/Boolean;

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lco/uk/getmondo/api/model/tracking/Data;->fallback:Ljava/lang/Boolean;

    iget-object v1, p1, Lco/uk/getmondo/api/model/tracking/Data;->fallback:Ljava/lang/Boolean;

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lco/uk/getmondo/api/model/tracking/Data;->notificationType:Ljava/lang/String;

    iget-object v1, p1, Lco/uk/getmondo/api/model/tracking/Data;->notificationType:Ljava/lang/String;

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lco/uk/getmondo/api/model/tracking/Data;->notificationSubType:Ljava/lang/String;

    iget-object v1, p1, Lco/uk/getmondo/api/model/tracking/Data;->notificationSubType:Ljava/lang/String;

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lco/uk/getmondo/api/model/tracking/Data;->optedIn:Ljava/lang/Boolean;

    iget-object v1, p1, Lco/uk/getmondo/api/model/tracking/Data;->optedIn:Ljava/lang/Boolean;

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lco/uk/getmondo/api/model/tracking/Data;->topicId:Ljava/lang/String;

    iget-object v1, p1, Lco/uk/getmondo/api/model/tracking/Data;->topicId:Ljava/lang/String;

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lco/uk/getmondo/api/model/tracking/Data;->outcome:Ljava/lang/String;

    iget-object v1, p1, Lco/uk/getmondo/api/model/tracking/Data;->outcome:Ljava/lang/String;

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lco/uk/getmondo/api/model/tracking/Data;->nameOnCard:Ljava/lang/String;

    iget-object v1, p1, Lco/uk/getmondo/api/model/tracking/Data;->nameOnCard:Ljava/lang/String;

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lco/uk/getmondo/api/model/tracking/Data;->fileType:Ljava/lang/String;

    iget-object v1, p1, Lco/uk/getmondo/api/model/tracking/Data;->fileType:Ljava/lang/String;

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    const/4 v1, 0x0

    iget-object v0, p0, Lco/uk/getmondo/api/model/tracking/Data;->tourPage:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    :goto_0
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lco/uk/getmondo/api/model/tracking/Data;->openedFrom:Ljava/lang/String;

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lco/uk/getmondo/api/model/tracking/Data;->transactionId:Ljava/lang/String;

    if-eqz v0, :cond_3

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    :goto_2
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lco/uk/getmondo/api/model/tracking/Data;->referrer:Ljava/lang/String;

    if-eqz v0, :cond_4

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    :goto_3
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lco/uk/getmondo/api/model/tracking/Data;->redirectId:Ljava/lang/String;

    if-eqz v0, :cond_5

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    :goto_4
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lco/uk/getmondo/api/model/tracking/Data;->url:Ljava/lang/String;

    if-eqz v0, :cond_6

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    :goto_5
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lco/uk/getmondo/api/model/tracking/Data;->eligible:Ljava/lang/Integer;

    if-eqz v0, :cond_7

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    :goto_6
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lco/uk/getmondo/api/model/tracking/Data;->amount:Ljava/lang/Integer;

    if-eqz v0, :cond_8

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    :goto_7
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lco/uk/getmondo/api/model/tracking/Data;->cameFrom:Ljava/lang/String;

    if-eqz v0, :cond_9

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    :goto_8
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lco/uk/getmondo/api/model/tracking/Data;->description:Ljava/lang/String;

    if-eqz v0, :cond_a

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    :goto_9
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lco/uk/getmondo/api/model/tracking/Data;->docDone:Ljava/lang/Boolean;

    if-eqz v0, :cond_b

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    :goto_a
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lco/uk/getmondo/api/model/tracking/Data;->selfieDone:Ljava/lang/Boolean;

    if-eqz v0, :cond_c

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    :goto_b
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lco/uk/getmondo/api/model/tracking/Data;->docType:Ljava/lang/String;

    if-eqz v0, :cond_d

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    :goto_c
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lco/uk/getmondo/api/model/tracking/Data;->kycRejected:Ljava/lang/Boolean;

    if-eqz v0, :cond_e

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    :goto_d
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lco/uk/getmondo/api/model/tracking/Data;->oldCategory:Ljava/lang/String;

    if-eqz v0, :cond_f

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    :goto_e
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lco/uk/getmondo/api/model/tracking/Data;->newCategory:Ljava/lang/String;

    if-eqz v0, :cond_10

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    :goto_f
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lco/uk/getmondo/api/model/tracking/Data;->category:Ljava/lang/String;

    if-eqz v0, :cond_11

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    :goto_10
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lco/uk/getmondo/api/model/tracking/Data;->month:Ljava/lang/Integer;

    if-eqz v0, :cond_12

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    :goto_11
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lco/uk/getmondo/api/model/tracking/Data;->granted:Ljava/lang/Boolean;

    if-eqz v0, :cond_13

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    :goto_12
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lco/uk/getmondo/api/model/tracking/Data;->contacts:Ljava/lang/Integer;

    if-eqz v0, :cond_14

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    :goto_13
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lco/uk/getmondo/api/model/tracking/Data;->contactsOnMonzo:Ljava/lang/Integer;

    if-eqz v0, :cond_15

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    :goto_14
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lco/uk/getmondo/api/model/tracking/Data;->from:Ljava/lang/String;

    if-eqz v0, :cond_16

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    :goto_15
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lco/uk/getmondo/api/model/tracking/Data;->method:Ljava/lang/String;

    if-eqz v0, :cond_17

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    :goto_16
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lco/uk/getmondo/api/model/tracking/Data;->authenticate:Ljava/lang/Boolean;

    if-eqz v0, :cond_18

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    :goto_17
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lco/uk/getmondo/api/model/tracking/Data;->appShortcutType:Ljava/lang/String;

    if-eqz v0, :cond_19

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    :goto_18
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lco/uk/getmondo/api/model/tracking/Data;->fingerprintAuth:Ljava/lang/String;

    if-eqz v0, :cond_1a

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    :goto_19
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lco/uk/getmondo/api/model/tracking/Data;->activeNotifications:Ljava/lang/Integer;

    if-eqz v0, :cond_1b

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    :goto_1a
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lco/uk/getmondo/api/model/tracking/Data;->nfcEnabled:Ljava/lang/Boolean;

    if-eqz v0, :cond_1c

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    :goto_1b
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lco/uk/getmondo/api/model/tracking/Data;->diligence:Ljava/lang/String;

    if-eqz v0, :cond_1d

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    :goto_1c
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lco/uk/getmondo/api/model/tracking/Data;->enabled:Ljava/lang/Boolean;

    if-eqz v0, :cond_1e

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    :goto_1d
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lco/uk/getmondo/api/model/tracking/Data;->gpsAdid:Ljava/lang/String;

    if-eqz v0, :cond_1f

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    :goto_1e
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lco/uk/getmondo/api/model/tracking/Data;->traceId:Ljava/lang/String;

    if-eqz v0, :cond_20

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    :goto_1f
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lco/uk/getmondo/api/model/tracking/Data;->type:Ljava/lang/String;

    if-eqz v0, :cond_21

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    :goto_20
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lco/uk/getmondo/api/model/tracking/Data;->path:Ljava/lang/String;

    if-eqz v0, :cond_22

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    :goto_21
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lco/uk/getmondo/api/model/tracking/Data;->apiStatusCode:Ljava/lang/Integer;

    if-eqz v0, :cond_23

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    :goto_22
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lco/uk/getmondo/api/model/tracking/Data;->apiErrorCode:Ljava/lang/String;

    if-eqz v0, :cond_24

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    :goto_23
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lco/uk/getmondo/api/model/tracking/Data;->apiErrorMessage:Ljava/lang/String;

    if-eqz v0, :cond_25

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    :goto_24
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lco/uk/getmondo/api/model/tracking/Data;->logoutReason:Ljava/lang/String;

    if-eqz v0, :cond_26

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    :goto_25
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lco/uk/getmondo/api/model/tracking/Data;->userId:Ljava/lang/String;

    if-eqz v0, :cond_27

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    :goto_26
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lco/uk/getmondo/api/model/tracking/Data;->errorType:Ljava/lang/String;

    if-eqz v0, :cond_28

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    :goto_27
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lco/uk/getmondo/api/model/tracking/Data;->sampleError:Ljava/lang/Boolean;

    if-eqz v0, :cond_29

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    :goto_28
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lco/uk/getmondo/api/model/tracking/Data;->firstSampleDelta:Ljava/lang/Long;

    if-eqz v0, :cond_2a

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    :goto_29
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lco/uk/getmondo/api/model/tracking/Data;->secondSampleDelta:Ljava/lang/Long;

    if-eqz v0, :cond_2b

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    :goto_2a
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lco/uk/getmondo/api/model/tracking/Data;->memoryError:Ljava/lang/Boolean;

    if-eqz v0, :cond_2c

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    :goto_2b
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lco/uk/getmondo/api/model/tracking/Data;->videoErrorReason:Ljava/lang/String;

    if-eqz v0, :cond_2d

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    :goto_2c
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lco/uk/getmondo/api/model/tracking/Data;->splitWith:Ljava/lang/String;

    if-eqz v0, :cond_2e

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    :goto_2d
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lco/uk/getmondo/api/model/tracking/Data;->firstTime:Ljava/lang/Boolean;

    if-eqz v0, :cond_2f

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    :goto_2e
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lco/uk/getmondo/api/model/tracking/Data;->feedback:Ljava/lang/String;

    if-eqz v0, :cond_30

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    :goto_2f
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lco/uk/getmondo/api/model/tracking/Data;->reportMonth:Ljava/lang/String;

    if-eqz v0, :cond_31

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    :goto_30
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lco/uk/getmondo/api/model/tracking/Data;->androidPayEnabled:Ljava/lang/Boolean;

    if-eqz v0, :cond_32

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    :goto_31
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lco/uk/getmondo/api/model/tracking/Data;->fallback:Ljava/lang/Boolean;

    if-eqz v0, :cond_33

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    :goto_32
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lco/uk/getmondo/api/model/tracking/Data;->notificationType:Ljava/lang/String;

    if-eqz v0, :cond_34

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    :goto_33
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lco/uk/getmondo/api/model/tracking/Data;->notificationSubType:Ljava/lang/String;

    if-eqz v0, :cond_35

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    :goto_34
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lco/uk/getmondo/api/model/tracking/Data;->optedIn:Ljava/lang/Boolean;

    if-eqz v0, :cond_36

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    :goto_35
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lco/uk/getmondo/api/model/tracking/Data;->topicId:Ljava/lang/String;

    if-eqz v0, :cond_37

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    :goto_36
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lco/uk/getmondo/api/model/tracking/Data;->outcome:Ljava/lang/String;

    if-eqz v0, :cond_38

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    :goto_37
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lco/uk/getmondo/api/model/tracking/Data;->nameOnCard:Ljava/lang/String;

    if-eqz v0, :cond_39

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    :goto_38
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lco/uk/getmondo/api/model/tracking/Data;->fileType:Ljava/lang/String;

    if-eqz v2, :cond_0

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_0
    add-int/2addr v0, v1

    return v0

    :cond_1
    move v0, v1

    goto/16 :goto_0

    :cond_2
    move v0, v1

    goto/16 :goto_1

    :cond_3
    move v0, v1

    goto/16 :goto_2

    :cond_4
    move v0, v1

    goto/16 :goto_3

    :cond_5
    move v0, v1

    goto/16 :goto_4

    :cond_6
    move v0, v1

    goto/16 :goto_5

    :cond_7
    move v0, v1

    goto/16 :goto_6

    :cond_8
    move v0, v1

    goto/16 :goto_7

    :cond_9
    move v0, v1

    goto/16 :goto_8

    :cond_a
    move v0, v1

    goto/16 :goto_9

    :cond_b
    move v0, v1

    goto/16 :goto_a

    :cond_c
    move v0, v1

    goto/16 :goto_b

    :cond_d
    move v0, v1

    goto/16 :goto_c

    :cond_e
    move v0, v1

    goto/16 :goto_d

    :cond_f
    move v0, v1

    goto/16 :goto_e

    :cond_10
    move v0, v1

    goto/16 :goto_f

    :cond_11
    move v0, v1

    goto/16 :goto_10

    :cond_12
    move v0, v1

    goto/16 :goto_11

    :cond_13
    move v0, v1

    goto/16 :goto_12

    :cond_14
    move v0, v1

    goto/16 :goto_13

    :cond_15
    move v0, v1

    goto/16 :goto_14

    :cond_16
    move v0, v1

    goto/16 :goto_15

    :cond_17
    move v0, v1

    goto/16 :goto_16

    :cond_18
    move v0, v1

    goto/16 :goto_17

    :cond_19
    move v0, v1

    goto/16 :goto_18

    :cond_1a
    move v0, v1

    goto/16 :goto_19

    :cond_1b
    move v0, v1

    goto/16 :goto_1a

    :cond_1c
    move v0, v1

    goto/16 :goto_1b

    :cond_1d
    move v0, v1

    goto/16 :goto_1c

    :cond_1e
    move v0, v1

    goto/16 :goto_1d

    :cond_1f
    move v0, v1

    goto/16 :goto_1e

    :cond_20
    move v0, v1

    goto/16 :goto_1f

    :cond_21
    move v0, v1

    goto/16 :goto_20

    :cond_22
    move v0, v1

    goto/16 :goto_21

    :cond_23
    move v0, v1

    goto/16 :goto_22

    :cond_24
    move v0, v1

    goto/16 :goto_23

    :cond_25
    move v0, v1

    goto/16 :goto_24

    :cond_26
    move v0, v1

    goto/16 :goto_25

    :cond_27
    move v0, v1

    goto/16 :goto_26

    :cond_28
    move v0, v1

    goto/16 :goto_27

    :cond_29
    move v0, v1

    goto/16 :goto_28

    :cond_2a
    move v0, v1

    goto/16 :goto_29

    :cond_2b
    move v0, v1

    goto/16 :goto_2a

    :cond_2c
    move v0, v1

    goto/16 :goto_2b

    :cond_2d
    move v0, v1

    goto/16 :goto_2c

    :cond_2e
    move v0, v1

    goto/16 :goto_2d

    :cond_2f
    move v0, v1

    goto/16 :goto_2e

    :cond_30
    move v0, v1

    goto/16 :goto_2f

    :cond_31
    move v0, v1

    goto/16 :goto_30

    :cond_32
    move v0, v1

    goto/16 :goto_31

    :cond_33
    move v0, v1

    goto/16 :goto_32

    :cond_34
    move v0, v1

    goto/16 :goto_33

    :cond_35
    move v0, v1

    goto/16 :goto_34

    :cond_36
    move v0, v1

    goto/16 :goto_35

    :cond_37
    move v0, v1

    goto/16 :goto_36

    :cond_38
    move v0, v1

    goto/16 :goto_37

    :cond_39
    move v0, v1

    goto/16 :goto_38
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Data(tourPage="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/api/model/tracking/Data;->tourPage:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", openedFrom="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/api/model/tracking/Data;->openedFrom:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", transactionId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/api/model/tracking/Data;->transactionId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", referrer="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/api/model/tracking/Data;->referrer:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", redirectId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/api/model/tracking/Data;->redirectId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", url="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/api/model/tracking/Data;->url:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", eligible="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/api/model/tracking/Data;->eligible:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", amount="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/api/model/tracking/Data;->amount:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", cameFrom="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/api/model/tracking/Data;->cameFrom:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", description="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/api/model/tracking/Data;->description:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", docDone="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/api/model/tracking/Data;->docDone:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", selfieDone="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/api/model/tracking/Data;->selfieDone:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", docType="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/api/model/tracking/Data;->docType:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", kycRejected="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/api/model/tracking/Data;->kycRejected:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", oldCategory="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/api/model/tracking/Data;->oldCategory:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", newCategory="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/api/model/tracking/Data;->newCategory:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", category="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/api/model/tracking/Data;->category:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", month="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/api/model/tracking/Data;->month:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", granted="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/api/model/tracking/Data;->granted:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", contacts="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/api/model/tracking/Data;->contacts:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", contactsOnMonzo="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/api/model/tracking/Data;->contactsOnMonzo:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", from="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/api/model/tracking/Data;->from:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", method="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/api/model/tracking/Data;->method:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", authenticate="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/api/model/tracking/Data;->authenticate:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", appShortcutType="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/api/model/tracking/Data;->appShortcutType:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", fingerprintAuth="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/api/model/tracking/Data;->fingerprintAuth:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", activeNotifications="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/api/model/tracking/Data;->activeNotifications:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", nfcEnabled="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/api/model/tracking/Data;->nfcEnabled:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", diligence="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/api/model/tracking/Data;->diligence:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", enabled="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/api/model/tracking/Data;->enabled:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", gpsAdid="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/api/model/tracking/Data;->gpsAdid:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", traceId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/api/model/tracking/Data;->traceId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/api/model/tracking/Data;->type:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", path="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/api/model/tracking/Data;->path:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", apiStatusCode="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/api/model/tracking/Data;->apiStatusCode:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", apiErrorCode="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/api/model/tracking/Data;->apiErrorCode:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", apiErrorMessage="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/api/model/tracking/Data;->apiErrorMessage:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", logoutReason="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/api/model/tracking/Data;->logoutReason:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", userId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/api/model/tracking/Data;->userId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", errorType="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/api/model/tracking/Data;->errorType:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", sampleError="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/api/model/tracking/Data;->sampleError:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", firstSampleDelta="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/api/model/tracking/Data;->firstSampleDelta:Ljava/lang/Long;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", secondSampleDelta="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/api/model/tracking/Data;->secondSampleDelta:Ljava/lang/Long;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", memoryError="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/api/model/tracking/Data;->memoryError:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", videoErrorReason="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/api/model/tracking/Data;->videoErrorReason:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", splitWith="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/api/model/tracking/Data;->splitWith:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", firstTime="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/api/model/tracking/Data;->firstTime:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", feedback="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/api/model/tracking/Data;->feedback:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", reportMonth="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/api/model/tracking/Data;->reportMonth:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", androidPayEnabled="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/api/model/tracking/Data;->androidPayEnabled:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", fallback="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/api/model/tracking/Data;->fallback:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", notificationType="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/api/model/tracking/Data;->notificationType:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", notificationSubType="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/api/model/tracking/Data;->notificationSubType:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", optedIn="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/api/model/tracking/Data;->optedIn:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", topicId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/api/model/tracking/Data;->topicId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", outcome="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/api/model/tracking/Data;->outcome:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", nameOnCard="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/api/model/tracking/Data;->nameOnCard:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", fileType="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/api/model/tracking/Data;->fileType:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
