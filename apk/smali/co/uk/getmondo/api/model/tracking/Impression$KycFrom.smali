.class public final enum Lco/uk/getmondo/api/model/tracking/Impression$KycFrom;
.super Ljava/lang/Enum;
.source "Impression.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lco/uk/getmondo/api/model/tracking/Impression;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "KycFrom"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lco/uk/getmondo/api/model/tracking/Impression$KycFrom;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u000b\u0008\u0086\u0001\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00000\u0001B\u000f\u0008\u0002\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004R\u0011\u0010\u0005\u001a\u00020\u00068F\u00a2\u0006\u0006\u001a\u0004\u0008\u0005\u0010\u0007R\u0014\u0010\u0002\u001a\u00020\u0003X\u0080\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0008\u0010\tj\u0002\u0008\nj\u0002\u0008\u000bj\u0002\u0008\u000cj\u0002\u0008\rj\u0002\u0008\u000ej\u0002\u0008\u000fj\u0002\u0008\u0010\u00a8\u0006\u0011"
    }
    d2 = {
        "Lco/uk/getmondo/api/model/tracking/Impression$KycFrom;",
        "",
        "value",
        "",
        "(Ljava/lang/String;ILjava/lang/String;)V",
        "isSdd",
        "",
        "()Z",
        "getValue$app_monzoPrepaidRelease",
        "()Ljava/lang/String;",
        "FEED",
        "LIMITS",
        "REQUEST",
        "SIGNUP",
        "KYC_SDD_ENCOURAGE",
        "KYC_SDD_WARNING",
        "KYC_SDD_BLOCKED",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lco/uk/getmondo/api/model/tracking/Impression$KycFrom;

.field public static final enum FEED:Lco/uk/getmondo/api/model/tracking/Impression$KycFrom;

.field public static final enum KYC_SDD_BLOCKED:Lco/uk/getmondo/api/model/tracking/Impression$KycFrom;

.field public static final enum KYC_SDD_ENCOURAGE:Lco/uk/getmondo/api/model/tracking/Impression$KycFrom;

.field public static final enum KYC_SDD_WARNING:Lco/uk/getmondo/api/model/tracking/Impression$KycFrom;

.field public static final enum LIMITS:Lco/uk/getmondo/api/model/tracking/Impression$KycFrom;

.field public static final enum REQUEST:Lco/uk/getmondo/api/model/tracking/Impression$KycFrom;

.field public static final enum SIGNUP:Lco/uk/getmondo/api/model/tracking/Impression$KycFrom;


# instance fields
.field private final value:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    const/4 v0, 0x7

    new-array v0, v0, [Lco/uk/getmondo/api/model/tracking/Impression$KycFrom;

    new-instance v1, Lco/uk/getmondo/api/model/tracking/Impression$KycFrom;

    const-string v2, "FEED"

    .line 21
    const-string v3, "feed"

    invoke-direct {v1, v2, v4, v3}, Lco/uk/getmondo/api/model/tracking/Impression$KycFrom;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lco/uk/getmondo/api/model/tracking/Impression$KycFrom;->FEED:Lco/uk/getmondo/api/model/tracking/Impression$KycFrom;

    aput-object v1, v0, v4

    new-instance v1, Lco/uk/getmondo/api/model/tracking/Impression$KycFrom;

    const-string v2, "LIMITS"

    .line 22
    const-string v3, "limits"

    invoke-direct {v1, v2, v5, v3}, Lco/uk/getmondo/api/model/tracking/Impression$KycFrom;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lco/uk/getmondo/api/model/tracking/Impression$KycFrom;->LIMITS:Lco/uk/getmondo/api/model/tracking/Impression$KycFrom;

    aput-object v1, v0, v5

    new-instance v1, Lco/uk/getmondo/api/model/tracking/Impression$KycFrom;

    const-string v2, "REQUEST"

    .line 23
    const-string v3, "request"

    invoke-direct {v1, v2, v6, v3}, Lco/uk/getmondo/api/model/tracking/Impression$KycFrom;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lco/uk/getmondo/api/model/tracking/Impression$KycFrom;->REQUEST:Lco/uk/getmondo/api/model/tracking/Impression$KycFrom;

    aput-object v1, v0, v6

    new-instance v1, Lco/uk/getmondo/api/model/tracking/Impression$KycFrom;

    const-string v2, "SIGNUP"

    .line 24
    const-string v3, "signup"

    invoke-direct {v1, v2, v7, v3}, Lco/uk/getmondo/api/model/tracking/Impression$KycFrom;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lco/uk/getmondo/api/model/tracking/Impression$KycFrom;->SIGNUP:Lco/uk/getmondo/api/model/tracking/Impression$KycFrom;

    aput-object v1, v0, v7

    new-instance v1, Lco/uk/getmondo/api/model/tracking/Impression$KycFrom;

    const-string v2, "KYC_SDD_ENCOURAGE"

    .line 25
    const-string v3, "kyc_june26"

    invoke-direct {v1, v2, v8, v3}, Lco/uk/getmondo/api/model/tracking/Impression$KycFrom;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lco/uk/getmondo/api/model/tracking/Impression$KycFrom;->KYC_SDD_ENCOURAGE:Lco/uk/getmondo/api/model/tracking/Impression$KycFrom;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    new-instance v2, Lco/uk/getmondo/api/model/tracking/Impression$KycFrom;

    const-string v3, "KYC_SDD_WARNING"

    const/4 v4, 0x5

    .line 26
    const-string v5, "kyc_june26_warning"

    invoke-direct {v2, v3, v4, v5}, Lco/uk/getmondo/api/model/tracking/Impression$KycFrom;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v2, Lco/uk/getmondo/api/model/tracking/Impression$KycFrom;->KYC_SDD_WARNING:Lco/uk/getmondo/api/model/tracking/Impression$KycFrom;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    new-instance v2, Lco/uk/getmondo/api/model/tracking/Impression$KycFrom;

    const-string v3, "KYC_SDD_BLOCKED"

    const/4 v4, 0x6

    .line 27
    const-string v5, "kyc_june26_blocked"

    invoke-direct {v2, v3, v4, v5}, Lco/uk/getmondo/api/model/tracking/Impression$KycFrom;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v2, Lco/uk/getmondo/api/model/tracking/Impression$KycFrom;->KYC_SDD_BLOCKED:Lco/uk/getmondo/api/model/tracking/Impression$KycFrom;

    aput-object v2, v0, v1

    sput-object v0, Lco/uk/getmondo/api/model/tracking/Impression$KycFrom;->$VALUES:[Lco/uk/getmondo/api/model/tracking/Impression$KycFrom;

    return-void
.end method

.method protected constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    const-string v0, "value"

    invoke-static {p3, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 20
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput-object p3, p0, Lco/uk/getmondo/api/model/tracking/Impression$KycFrom;->value:Ljava/lang/String;

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lco/uk/getmondo/api/model/tracking/Impression$KycFrom;
    .locals 1

    const-class v0, Lco/uk/getmondo/api/model/tracking/Impression$KycFrom;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/api/model/tracking/Impression$KycFrom;

    return-object v0
.end method

.method public static values()[Lco/uk/getmondo/api/model/tracking/Impression$KycFrom;
    .locals 1

    sget-object v0, Lco/uk/getmondo/api/model/tracking/Impression$KycFrom;->$VALUES:[Lco/uk/getmondo/api/model/tracking/Impression$KycFrom;

    invoke-virtual {v0}, [Lco/uk/getmondo/api/model/tracking/Impression$KycFrom;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lco/uk/getmondo/api/model/tracking/Impression$KycFrom;

    return-object v0
.end method


# virtual methods
.method public final a()Z
    .locals 2

    .prologue
    .line 30
    move-object v0, p0

    check-cast v0, Lco/uk/getmondo/api/model/tracking/Impression$KycFrom;

    sget-object v1, Lco/uk/getmondo/api/model/tracking/Impression$KycFrom;->KYC_SDD_ENCOURAGE:Lco/uk/getmondo/api/model/tracking/Impression$KycFrom;

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    move-object v0, p0

    check-cast v0, Lco/uk/getmondo/api/model/tracking/Impression$KycFrom;

    sget-object v1, Lco/uk/getmondo/api/model/tracking/Impression$KycFrom;->KYC_SDD_WARNING:Lco/uk/getmondo/api/model/tracking/Impression$KycFrom;

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    check-cast p0, Lco/uk/getmondo/api/model/tracking/Impression$KycFrom;

    sget-object v0, Lco/uk/getmondo/api/model/tracking/Impression$KycFrom;->KYC_SDD_BLOCKED:Lco/uk/getmondo/api/model/tracking/Impression$KycFrom;

    invoke-static {p0, v0}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 20
    iget-object v0, p0, Lco/uk/getmondo/api/model/tracking/Impression$KycFrom;->value:Ljava/lang/String;

    return-object v0
.end method
