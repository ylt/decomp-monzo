.class public final enum Lco/uk/getmondo/api/model/tracking/Impression$PinFrom;
.super Ljava/lang/Enum;
.source "Impression.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lco/uk/getmondo/api/model/tracking/Impression;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "PinFrom"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lco/uk/getmondo/api/model/tracking/Impression$PinFrom;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0008\u0008\u0086\u0001\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00000\u0001B\u000f\u0008\u0002\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004R\u001a\u0010\u0002\u001a\u00020\u0003X\u0080\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u0005\u0010\u0006\"\u0004\u0008\u0007\u0010\u0008j\u0002\u0008\tj\u0002\u0008\n\u00a8\u0006\u000b"
    }
    d2 = {
        "Lco/uk/getmondo/api/model/tracking/Impression$PinFrom;",
        "",
        "value",
        "",
        "(Ljava/lang/String;ILjava/lang/String;)V",
        "getValue$app_monzoPrepaidRelease",
        "()Ljava/lang/String;",
        "setValue$app_monzoPrepaidRelease",
        "(Ljava/lang/String;)V",
        "CARD",
        "ADDRESS_CHANGE",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lco/uk/getmondo/api/model/tracking/Impression$PinFrom;

.field public static final enum ADDRESS_CHANGE:Lco/uk/getmondo/api/model/tracking/Impression$PinFrom;

.field public static final enum CARD:Lco/uk/getmondo/api/model/tracking/Impression$PinFrom;


# instance fields
.field private value:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    const/4 v0, 0x2

    new-array v0, v0, [Lco/uk/getmondo/api/model/tracking/Impression$PinFrom;

    new-instance v1, Lco/uk/getmondo/api/model/tracking/Impression$PinFrom;

    const-string v2, "CARD"

    .line 81
    const-string v3, "card"

    invoke-direct {v1, v2, v4, v3}, Lco/uk/getmondo/api/model/tracking/Impression$PinFrom;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lco/uk/getmondo/api/model/tracking/Impression$PinFrom;->CARD:Lco/uk/getmondo/api/model/tracking/Impression$PinFrom;

    aput-object v1, v0, v4

    new-instance v1, Lco/uk/getmondo/api/model/tracking/Impression$PinFrom;

    const-string v2, "ADDRESS_CHANGE"

    .line 82
    const-string v3, "address-change"

    invoke-direct {v1, v2, v5, v3}, Lco/uk/getmondo/api/model/tracking/Impression$PinFrom;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lco/uk/getmondo/api/model/tracking/Impression$PinFrom;->ADDRESS_CHANGE:Lco/uk/getmondo/api/model/tracking/Impression$PinFrom;

    aput-object v1, v0, v5

    sput-object v0, Lco/uk/getmondo/api/model/tracking/Impression$PinFrom;->$VALUES:[Lco/uk/getmondo/api/model/tracking/Impression$PinFrom;

    return-void
.end method

.method protected constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    const-string v0, "value"

    invoke-static {p3, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 80
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput-object p3, p0, Lco/uk/getmondo/api/model/tracking/Impression$PinFrom;->value:Ljava/lang/String;

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lco/uk/getmondo/api/model/tracking/Impression$PinFrom;
    .locals 1

    const-class v0, Lco/uk/getmondo/api/model/tracking/Impression$PinFrom;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/api/model/tracking/Impression$PinFrom;

    return-object v0
.end method

.method public static values()[Lco/uk/getmondo/api/model/tracking/Impression$PinFrom;
    .locals 1

    sget-object v0, Lco/uk/getmondo/api/model/tracking/Impression$PinFrom;->$VALUES:[Lco/uk/getmondo/api/model/tracking/Impression$PinFrom;

    invoke-virtual {v0}, [Lco/uk/getmondo/api/model/tracking/Impression$PinFrom;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lco/uk/getmondo/api/model/tracking/Impression$PinFrom;

    return-object v0
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 80
    iget-object v0, p0, Lco/uk/getmondo/api/model/tracking/Impression$PinFrom;->value:Ljava/lang/String;

    return-object v0
.end method
