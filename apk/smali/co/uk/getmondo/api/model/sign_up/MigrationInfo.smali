.class public final Lco/uk/getmondo/api/model/sign_up/MigrationInfo;
.super Ljava/lang/Object;
.source "MigrationInfo.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u00000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0015\n\u0002\u0010\u0008\n\u0002\u0008\u0002\u0008\u0086\u0008\u0018\u00002\u00020\u0001BA\u0012\u0008\u0008\u0003\u0010\u0002\u001a\u00020\u0003\u0012\u0008\u0008\u0003\u0010\u0004\u001a\u00020\u0003\u0012\u0008\u0008\u0003\u0010\u0005\u001a\u00020\u0006\u0012\u0008\u0008\u0003\u0010\u0007\u001a\u00020\u0006\u0012\u0008\u0008\u0003\u0010\u0008\u001a\u00020\t\u0012\u0008\u0008\u0003\u0010\n\u001a\u00020\u000b\u00a2\u0006\u0002\u0010\u000cJ\t\u0010\u0017\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0018\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0019\u001a\u00020\u0006H\u00c6\u0003J\t\u0010\u001a\u001a\u00020\u0006H\u00c6\u0003J\t\u0010\u001b\u001a\u00020\tH\u00c6\u0003J\t\u0010\u001c\u001a\u00020\u000bH\u00c6\u0003JE\u0010\u001d\u001a\u00020\u00002\u0008\u0008\u0003\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0003\u0010\u0004\u001a\u00020\u00032\u0008\u0008\u0003\u0010\u0005\u001a\u00020\u00062\u0008\u0008\u0003\u0010\u0007\u001a\u00020\u00062\u0008\u0008\u0003\u0010\u0008\u001a\u00020\t2\u0008\u0008\u0003\u0010\n\u001a\u00020\u000bH\u00c6\u0001J\u0013\u0010\u001e\u001a\u00020\u00032\u0008\u0010\u001f\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u0010 \u001a\u00020!H\u00d6\u0001J\t\u0010\"\u001a\u00020\u0006H\u00d6\u0001R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\r\u0010\u000eR\u0011\u0010\u0007\u001a\u00020\u0006\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000f\u0010\u0010R\u0011\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0011\u0010\u0010R\u0011\u0010\u0004\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0012\u0010\u000eR\u0011\u0010\n\u001a\u00020\u000b\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0013\u0010\u0014R\u0011\u0010\u0008\u001a\u00020\t\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0015\u0010\u0016\u00a8\u0006#"
    }
    d2 = {
        "Lco/uk/getmondo/api/model/sign_up/MigrationInfo;",
        "",
        "bannerEnabled",
        "",
        "signupAllowed",
        "bannerTitle",
        "",
        "bannerSubtitle",
        "signupStatus",
        "Lco/uk/getmondo/api/model/signup/SignupInfo$Status;",
        "signupStage",
        "Lco/uk/getmondo/api/model/signup/SignupInfo$Stage;",
        "(ZZLjava/lang/String;Ljava/lang/String;Lco/uk/getmondo/api/model/signup/SignupInfo$Status;Lco/uk/getmondo/api/model/signup/SignupInfo$Stage;)V",
        "getBannerEnabled",
        "()Z",
        "getBannerSubtitle",
        "()Ljava/lang/String;",
        "getBannerTitle",
        "getSignupAllowed",
        "getSignupStage",
        "()Lco/uk/getmondo/api/model/signup/SignupInfo$Stage;",
        "getSignupStatus",
        "()Lco/uk/getmondo/api/model/signup/SignupInfo$Status;",
        "component1",
        "component2",
        "component3",
        "component4",
        "component5",
        "component6",
        "copy",
        "equals",
        "other",
        "hashCode",
        "",
        "toString",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field private final bannerEnabled:Z

.field private final bannerSubtitle:Ljava/lang/String;

.field private final bannerTitle:Ljava/lang/String;

.field private final signupAllowed:Z

.field private final signupStage:Lco/uk/getmondo/api/model/signup/SignupInfo$Stage;

.field private final signupStatus:Lco/uk/getmondo/api/model/signup/SignupInfo$Status;


# direct methods
.method public constructor <init>()V
    .locals 9

    const/4 v1, 0x0

    const/4 v3, 0x0

    const/16 v7, 0x3f

    move-object v0, p0

    move v2, v1

    move-object v4, v3

    move-object v5, v3

    move-object v6, v3

    move-object v8, v3

    invoke-direct/range {v0 .. v8}, Lco/uk/getmondo/api/model/sign_up/MigrationInfo;-><init>(ZZLjava/lang/String;Ljava/lang/String;Lco/uk/getmondo/api/model/signup/SignupInfo$Status;Lco/uk/getmondo/api/model/signup/SignupInfo$Stage;ILkotlin/d/b/i;)V

    return-void
.end method

.method public constructor <init>(ZZLjava/lang/String;Ljava/lang/String;Lco/uk/getmondo/api/model/signup/SignupInfo$Status;Lco/uk/getmondo/api/model/signup/SignupInfo$Stage;)V
    .locals 1
    .param p1    # Z
        .annotation runtime Lcom/squareup/moshi/h;
            a = "banner_enabled"
        .end annotation
    .end param
    .param p2    # Z
        .annotation runtime Lcom/squareup/moshi/h;
            a = "signup_allowed"
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation runtime Lcom/squareup/moshi/h;
            a = "banner_title"
        .end annotation
    .end param
    .param p4    # Ljava/lang/String;
        .annotation runtime Lcom/squareup/moshi/h;
            a = "banner_subtitle"
        .end annotation
    .end param
    .param p5    # Lco/uk/getmondo/api/model/signup/SignupInfo$Status;
        .annotation runtime Lcom/squareup/moshi/h;
            a = "signup_status"
        .end annotation
    .end param
    .param p6    # Lco/uk/getmondo/api/model/signup/SignupInfo$Stage;
        .annotation runtime Lcom/squareup/moshi/h;
            a = "signup_stage"
        .end annotation
    .end param

    .prologue
    const-string v0, "bannerTitle"

    invoke-static {p3, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "bannerSubtitle"

    invoke-static {p4, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "signupStatus"

    invoke-static {p5, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "signupStage"

    invoke-static {p6, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean p1, p0, Lco/uk/getmondo/api/model/sign_up/MigrationInfo;->bannerEnabled:Z

    iput-boolean p2, p0, Lco/uk/getmondo/api/model/sign_up/MigrationInfo;->signupAllowed:Z

    iput-object p3, p0, Lco/uk/getmondo/api/model/sign_up/MigrationInfo;->bannerTitle:Ljava/lang/String;

    iput-object p4, p0, Lco/uk/getmondo/api/model/sign_up/MigrationInfo;->bannerSubtitle:Ljava/lang/String;

    iput-object p5, p0, Lco/uk/getmondo/api/model/sign_up/MigrationInfo;->signupStatus:Lco/uk/getmondo/api/model/signup/SignupInfo$Status;

    iput-object p6, p0, Lco/uk/getmondo/api/model/sign_up/MigrationInfo;->signupStage:Lco/uk/getmondo/api/model/signup/SignupInfo$Stage;

    return-void
.end method

.method public synthetic constructor <init>(ZZLjava/lang/String;Ljava/lang/String;Lco/uk/getmondo/api/model/signup/SignupInfo$Status;Lco/uk/getmondo/api/model/signup/SignupInfo$Stage;ILkotlin/d/b/i;)V
    .locals 7

    .prologue
    const/4 v2, 0x0

    and-int/lit8 v0, p7, 0x1

    if-eqz v0, :cond_5

    move v1, v2

    .line 7
    :goto_0
    and-int/lit8 v0, p7, 0x2

    if-eqz v0, :cond_4

    .line 8
    :goto_1
    and-int/lit8 v0, p7, 0x4

    if-eqz v0, :cond_3

    .line 9
    const-string v3, ""

    :goto_2
    and-int/lit8 v0, p7, 0x8

    if-eqz v0, :cond_2

    .line 10
    const-string v4, ""

    :goto_3
    and-int/lit8 v0, p7, 0x10

    if-eqz v0, :cond_1

    .line 11
    sget-object v5, Lco/uk/getmondo/api/model/signup/SignupInfo$Status;->NOT_STARTED:Lco/uk/getmondo/api/model/signup/SignupInfo$Status;

    :goto_4
    and-int/lit8 v0, p7, 0x20

    if-eqz v0, :cond_0

    .line 12
    sget-object v6, Lco/uk/getmondo/api/model/signup/SignupInfo$Stage;->NONE:Lco/uk/getmondo/api/model/signup/SignupInfo$Stage;

    :goto_5
    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lco/uk/getmondo/api/model/sign_up/MigrationInfo;-><init>(ZZLjava/lang/String;Ljava/lang/String;Lco/uk/getmondo/api/model/signup/SignupInfo$Status;Lco/uk/getmondo/api/model/signup/SignupInfo$Stage;)V

    return-void

    :cond_0
    move-object v6, p6

    goto :goto_5

    :cond_1
    move-object v5, p5

    goto :goto_4

    :cond_2
    move-object v4, p4

    goto :goto_3

    :cond_3
    move-object v3, p3

    goto :goto_2

    :cond_4
    move v2, p2

    goto :goto_1

    :cond_5
    move v1, p1

    goto :goto_0
.end method

.method public static bridge synthetic a(Lco/uk/getmondo/api/model/sign_up/MigrationInfo;ZZLjava/lang/String;Ljava/lang/String;Lco/uk/getmondo/api/model/signup/SignupInfo$Status;Lco/uk/getmondo/api/model/signup/SignupInfo$Stage;ILjava/lang/Object;)Lco/uk/getmondo/api/model/sign_up/MigrationInfo;
    .locals 7

    and-int/lit8 v0, p7, 0x1

    if-eqz v0, :cond_5

    iget-boolean v1, p0, Lco/uk/getmondo/api/model/sign_up/MigrationInfo;->bannerEnabled:Z

    :goto_0
    and-int/lit8 v0, p7, 0x2

    if-eqz v0, :cond_4

    iget-boolean v2, p0, Lco/uk/getmondo/api/model/sign_up/MigrationInfo;->signupAllowed:Z

    :goto_1
    and-int/lit8 v0, p7, 0x4

    if-eqz v0, :cond_3

    iget-object v3, p0, Lco/uk/getmondo/api/model/sign_up/MigrationInfo;->bannerTitle:Ljava/lang/String;

    :goto_2
    and-int/lit8 v0, p7, 0x8

    if-eqz v0, :cond_2

    iget-object v4, p0, Lco/uk/getmondo/api/model/sign_up/MigrationInfo;->bannerSubtitle:Ljava/lang/String;

    :goto_3
    and-int/lit8 v0, p7, 0x10

    if-eqz v0, :cond_1

    iget-object v5, p0, Lco/uk/getmondo/api/model/sign_up/MigrationInfo;->signupStatus:Lco/uk/getmondo/api/model/signup/SignupInfo$Status;

    :goto_4
    and-int/lit8 v0, p7, 0x20

    if-eqz v0, :cond_0

    iget-object v6, p0, Lco/uk/getmondo/api/model/sign_up/MigrationInfo;->signupStage:Lco/uk/getmondo/api/model/signup/SignupInfo$Stage;

    :goto_5
    move-object v0, p0

    invoke-virtual/range {v0 .. v6}, Lco/uk/getmondo/api/model/sign_up/MigrationInfo;->a(ZZLjava/lang/String;Ljava/lang/String;Lco/uk/getmondo/api/model/signup/SignupInfo$Status;Lco/uk/getmondo/api/model/signup/SignupInfo$Stage;)Lco/uk/getmondo/api/model/sign_up/MigrationInfo;

    move-result-object v0

    return-object v0

    :cond_0
    move-object v6, p6

    goto :goto_5

    :cond_1
    move-object v5, p5

    goto :goto_4

    :cond_2
    move-object v4, p4

    goto :goto_3

    :cond_3
    move-object v3, p3

    goto :goto_2

    :cond_4
    move v2, p2

    goto :goto_1

    :cond_5
    move v1, p1

    goto :goto_0
.end method


# virtual methods
.method public final a(ZZLjava/lang/String;Ljava/lang/String;Lco/uk/getmondo/api/model/signup/SignupInfo$Status;Lco/uk/getmondo/api/model/signup/SignupInfo$Stage;)Lco/uk/getmondo/api/model/sign_up/MigrationInfo;
    .locals 7
    .param p1    # Z
        .annotation runtime Lcom/squareup/moshi/h;
            a = "banner_enabled"
        .end annotation
    .end param
    .param p2    # Z
        .annotation runtime Lcom/squareup/moshi/h;
            a = "signup_allowed"
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation runtime Lcom/squareup/moshi/h;
            a = "banner_title"
        .end annotation
    .end param
    .param p4    # Ljava/lang/String;
        .annotation runtime Lcom/squareup/moshi/h;
            a = "banner_subtitle"
        .end annotation
    .end param
    .param p5    # Lco/uk/getmondo/api/model/signup/SignupInfo$Status;
        .annotation runtime Lcom/squareup/moshi/h;
            a = "signup_status"
        .end annotation
    .end param
    .param p6    # Lco/uk/getmondo/api/model/signup/SignupInfo$Stage;
        .annotation runtime Lcom/squareup/moshi/h;
            a = "signup_stage"
        .end annotation
    .end param

    const-string v0, "bannerTitle"

    invoke-static {p3, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "bannerSubtitle"

    invoke-static {p4, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "signupStatus"

    invoke-static {p5, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "signupStage"

    invoke-static {p6, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lco/uk/getmondo/api/model/sign_up/MigrationInfo;

    move v1, p1

    move v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    invoke-direct/range {v0 .. v6}, Lco/uk/getmondo/api/model/sign_up/MigrationInfo;-><init>(ZZLjava/lang/String;Ljava/lang/String;Lco/uk/getmondo/api/model/signup/SignupInfo$Status;Lco/uk/getmondo/api/model/signup/SignupInfo$Stage;)V

    return-object v0
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 7
    iget-boolean v0, p0, Lco/uk/getmondo/api/model/sign_up/MigrationInfo;->bannerEnabled:Z

    return v0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 8
    iget-boolean v0, p0, Lco/uk/getmondo/api/model/sign_up/MigrationInfo;->signupAllowed:Z

    return v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 9
    iget-object v0, p0, Lco/uk/getmondo/api/model/sign_up/MigrationInfo;->bannerTitle:Ljava/lang/String;

    return-object v0
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 10
    iget-object v0, p0, Lco/uk/getmondo/api/model/sign_up/MigrationInfo;->bannerSubtitle:Ljava/lang/String;

    return-object v0
.end method

.method public final e()Lco/uk/getmondo/api/model/signup/SignupInfo$Status;
    .locals 1

    .prologue
    .line 11
    iget-object v0, p0, Lco/uk/getmondo/api/model/sign_up/MigrationInfo;->signupStatus:Lco/uk/getmondo/api/model/signup/SignupInfo$Status;

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v1, 0x1

    const/4 v0, 0x0

    if-eq p0, p1, :cond_0

    instance-of v2, p1, Lco/uk/getmondo/api/model/sign_up/MigrationInfo;

    if-eqz v2, :cond_1

    check-cast p1, Lco/uk/getmondo/api/model/sign_up/MigrationInfo;

    iget-boolean v2, p0, Lco/uk/getmondo/api/model/sign_up/MigrationInfo;->bannerEnabled:Z

    iget-boolean v3, p1, Lco/uk/getmondo/api/model/sign_up/MigrationInfo;->bannerEnabled:Z

    if-ne v2, v3, :cond_2

    move v2, v1

    :goto_0
    if-eqz v2, :cond_1

    iget-boolean v2, p0, Lco/uk/getmondo/api/model/sign_up/MigrationInfo;->signupAllowed:Z

    iget-boolean v3, p1, Lco/uk/getmondo/api/model/sign_up/MigrationInfo;->signupAllowed:Z

    if-ne v2, v3, :cond_3

    move v2, v1

    :goto_1
    if-eqz v2, :cond_1

    iget-object v2, p0, Lco/uk/getmondo/api/model/sign_up/MigrationInfo;->bannerTitle:Ljava/lang/String;

    iget-object v3, p1, Lco/uk/getmondo/api/model/sign_up/MigrationInfo;->bannerTitle:Ljava/lang/String;

    invoke-static {v2, v3}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lco/uk/getmondo/api/model/sign_up/MigrationInfo;->bannerSubtitle:Ljava/lang/String;

    iget-object v3, p1, Lco/uk/getmondo/api/model/sign_up/MigrationInfo;->bannerSubtitle:Ljava/lang/String;

    invoke-static {v2, v3}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lco/uk/getmondo/api/model/sign_up/MigrationInfo;->signupStatus:Lco/uk/getmondo/api/model/signup/SignupInfo$Status;

    iget-object v3, p1, Lco/uk/getmondo/api/model/sign_up/MigrationInfo;->signupStatus:Lco/uk/getmondo/api/model/signup/SignupInfo$Status;

    invoke-static {v2, v3}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lco/uk/getmondo/api/model/sign_up/MigrationInfo;->signupStage:Lco/uk/getmondo/api/model/signup/SignupInfo$Stage;

    iget-object v3, p1, Lco/uk/getmondo/api/model/sign_up/MigrationInfo;->signupStage:Lco/uk/getmondo/api/model/signup/SignupInfo$Stage;

    invoke-static {v2, v3}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    move v0, v1

    :cond_1
    return v0

    :cond_2
    move v2, v0

    goto :goto_0

    :cond_3
    move v2, v0

    goto :goto_1
.end method

.method public final f()Lco/uk/getmondo/api/model/signup/SignupInfo$Stage;
    .locals 1

    .prologue
    .line 12
    iget-object v0, p0, Lco/uk/getmondo/api/model/sign_up/MigrationInfo;->signupStage:Lco/uk/getmondo/api/model/signup/SignupInfo$Stage;

    return-object v0
.end method

.method public hashCode()I
    .locals 4

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget-boolean v0, p0, Lco/uk/getmondo/api/model/sign_up/MigrationInfo;->bannerEnabled:Z

    if-eqz v0, :cond_0

    move v0, v1

    :cond_0
    mul-int/lit8 v3, v0, 0x1f

    iget-boolean v0, p0, Lco/uk/getmondo/api/model/sign_up/MigrationInfo;->signupAllowed:Z

    if-eqz v0, :cond_5

    :goto_0
    add-int v0, v3, v1

    mul-int/lit8 v1, v0, 0x1f

    iget-object v0, p0, Lco/uk/getmondo/api/model/sign_up/MigrationInfo;->bannerTitle:Ljava/lang/String;

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v1, v0, 0x1f

    iget-object v0, p0, Lco/uk/getmondo/api/model/sign_up/MigrationInfo;->bannerSubtitle:Ljava/lang/String;

    if-eqz v0, :cond_3

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v1, v0, 0x1f

    iget-object v0, p0, Lco/uk/getmondo/api/model/sign_up/MigrationInfo;->signupStatus:Lco/uk/getmondo/api/model/signup/SignupInfo$Status;

    if-eqz v0, :cond_4

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lco/uk/getmondo/api/model/sign_up/MigrationInfo;->signupStage:Lco/uk/getmondo/api/model/signup/SignupInfo$Stage;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v2

    :cond_1
    add-int/2addr v0, v2

    return v0

    :cond_2
    move v0, v2

    goto :goto_1

    :cond_3
    move v0, v2

    goto :goto_2

    :cond_4
    move v0, v2

    goto :goto_3

    :cond_5
    move v1, v0

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "MigrationInfo(bannerEnabled="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lco/uk/getmondo/api/model/sign_up/MigrationInfo;->bannerEnabled:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", signupAllowed="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lco/uk/getmondo/api/model/sign_up/MigrationInfo;->signupAllowed:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", bannerTitle="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/api/model/sign_up/MigrationInfo;->bannerTitle:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", bannerSubtitle="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/api/model/sign_up/MigrationInfo;->bannerSubtitle:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", signupStatus="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/api/model/sign_up/MigrationInfo;->signupStatus:Lco/uk/getmondo/api/model/signup/SignupInfo$Status;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", signupStage="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/api/model/sign_up/MigrationInfo;->signupStage:Lco/uk/getmondo/api/model/signup/SignupInfo$Stage;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
