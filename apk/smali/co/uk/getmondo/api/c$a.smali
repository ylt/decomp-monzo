.class final Lco/uk/getmondo/api/c$a;
.super Ljava/lang/Object;
.source "ApiModule.kt"

# interfaces
.implements Lcom/google/gson/k;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lco/uk/getmondo/api/c;->b()Lcom/google/gson/f;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/google/gson/k",
        "<",
        "Lorg/threeten/bp/LocalDateTime;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000\u001c\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u0004\u0018\u00010\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u00032\u000e\u0010\u0005\u001a\n \u0004*\u0004\u0018\u00010\u00060\u00062\u000e\u0010\u0007\u001a\n \u0004*\u0004\u0018\u00010\u00080\u0008H\n\u00a2\u0006\u0002\u0008\t"
    }
    d2 = {
        "<anonymous>",
        "Lorg/threeten/bp/LocalDateTime;",
        "json",
        "Lcom/google/gson/JsonElement;",
        "kotlin.jvm.PlatformType",
        "<anonymous parameter 1>",
        "Ljava/lang/reflect/Type;",
        "<anonymous parameter 2>",
        "Lcom/google/gson/JsonDeserializationContext;",
        "deserialize"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# static fields
.field public static final a:Lco/uk/getmondo/api/c$a;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lco/uk/getmondo/api/c$a;

    invoke-direct {v0}, Lco/uk/getmondo/api/c$a;-><init>()V

    sput-object v0, Lco/uk/getmondo/api/c$a;->a:Lco/uk/getmondo/api/c$a;

    return-void
.end method

.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public synthetic a(Lcom/google/gson/l;Ljava/lang/reflect/Type;Lcom/google/gson/j;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 44
    invoke-virtual {p0, p1, p2, p3}, Lco/uk/getmondo/api/c$a;->b(Lcom/google/gson/l;Ljava/lang/reflect/Type;Lcom/google/gson/j;)Lorg/threeten/bp/LocalDateTime;

    move-result-object v0

    return-object v0
.end method

.method public final b(Lcom/google/gson/l;Ljava/lang/reflect/Type;Lcom/google/gson/j;)Lorg/threeten/bp/LocalDateTime;
    .locals 2

    .prologue
    .line 74
    invoke-virtual {p1}, Lcom/google/gson/l;->b()Ljava/lang/String;

    move-result-object v1

    move-object v0, v1

    .line 75
    check-cast v0, Ljava/lang/CharSequence;

    invoke-static {v0}, Lkotlin/h/j;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_1

    .line 76
    check-cast v1, Ljava/lang/CharSequence;

    invoke-static {v1}, Lorg/threeten/bp/ZonedDateTime;->a(Ljava/lang/CharSequence;)Lorg/threeten/bp/ZonedDateTime;

    move-result-object v0

    invoke-virtual {v0}, Lorg/threeten/bp/ZonedDateTime;->d()Lorg/threeten/bp/LocalDateTime;

    move-result-object v0

    .line 75
    :goto_1
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 78
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method
