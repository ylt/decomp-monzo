.class public Lco/uk/getmondo/api/ApiException;
.super Ljava/io/IOException;
.source "ApiException.java"


# instance fields
.field private final a:I

.field private final b:Ljava/lang/String;

.field private final c:Ljava/lang/String;

.field private final d:Lco/uk/getmondo/api/model/b;


# direct methods
.method public constructor <init>(ILjava/lang/String;Ljava/lang/String;Lco/uk/getmondo/api/model/b;)V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/io/IOException;-><init>()V

    .line 17
    iput p1, p0, Lco/uk/getmondo/api/ApiException;->a:I

    .line 18
    iput-object p2, p0, Lco/uk/getmondo/api/ApiException;->b:Ljava/lang/String;

    .line 19
    iput-object p3, p0, Lco/uk/getmondo/api/ApiException;->c:Ljava/lang/String;

    .line 20
    iput-object p4, p0, Lco/uk/getmondo/api/ApiException;->d:Lco/uk/getmondo/api/model/b;

    .line 21
    return-void
.end method


# virtual methods
.method public a()Z
    .locals 2

    .prologue
    .line 24
    iget v0, p0, Lco/uk/getmondo/api/ApiException;->a:I

    const/16 v1, 0x194

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()I
    .locals 1

    .prologue
    .line 34
    iget v0, p0, Lco/uk/getmondo/api/ApiException;->a:I

    return v0
.end method

.method public c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lco/uk/getmondo/api/ApiException;->b:Ljava/lang/String;

    return-object v0
.end method

.method public d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lco/uk/getmondo/api/ApiException;->c:Ljava/lang/String;

    return-object v0
.end method

.method public e()Lco/uk/getmondo/api/model/b;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lco/uk/getmondo/api/ApiException;->d:Lco/uk/getmondo/api/model/b;

    return-object v0
.end method

.method public getMessage()Ljava/lang/String;
    .locals 6

    .prologue
    .line 29
    iget-object v0, p0, Lco/uk/getmondo/api/ApiException;->d:Lco/uk/getmondo/api/model/b;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lco/uk/getmondo/api/ApiException;->d:Lco/uk/getmondo/api/model/b;

    invoke-virtual {v0}, Lco/uk/getmondo/api/model/b;->toString()Ljava/lang/String;

    move-result-object v0

    .line 30
    :goto_0
    sget-object v1, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    const-string v2, "Request to %s failed with %d due to: %s"

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, Lco/uk/getmondo/api/ApiException;->b:Ljava/lang/String;

    aput-object v5, v3, v4

    const/4 v4, 0x1

    iget v5, p0, Lco/uk/getmondo/api/ApiException;->a:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x2

    aput-object v0, v3, v4

    invoke-static {v1, v2, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 29
    :cond_0
    const-string v0, ""

    goto :goto_0
.end method
