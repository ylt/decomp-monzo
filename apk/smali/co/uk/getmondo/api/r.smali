.class public final Lco/uk/getmondo/api/r;
.super Ljava/lang/Object;
.source "ApiModule_ProvideOAuthMonzoApi$app_monzoPrepaidReleaseFactory.java"

# interfaces
.implements Lb/a/b;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lb/a/b",
        "<",
        "Lco/uk/getmondo/api/authentication/MonzoOAuthApi;",
        ">;"
    }
.end annotation


# static fields
.field static final synthetic a:Z


# instance fields
.field private final b:Lco/uk/getmondo/api/c;

.field private final c:Ljavax/a/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/a/a",
            "<",
            "Lokhttp3/OkHttpClient;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Ljavax/a/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/a/a",
            "<",
            "Lcom/google/gson/f;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Ljavax/a/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/api/ac;",
            ">;"
        }
    .end annotation
.end field

.field private final f:Ljavax/a/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/a/a",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 11
    const-class v0, Lco/uk/getmondo/api/r;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lco/uk/getmondo/api/r;->a:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lco/uk/getmondo/api/c;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lco/uk/getmondo/api/c;",
            "Ljavax/a/a",
            "<",
            "Lokhttp3/OkHttpClient;",
            ">;",
            "Ljavax/a/a",
            "<",
            "Lcom/google/gson/f;",
            ">;",
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/api/ac;",
            ">;",
            "Ljavax/a/a",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    sget-boolean v0, Lco/uk/getmondo/api/r;->a:Z

    if-nez v0, :cond_0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 34
    :cond_0
    iput-object p1, p0, Lco/uk/getmondo/api/r;->b:Lco/uk/getmondo/api/c;

    .line 35
    sget-boolean v0, Lco/uk/getmondo/api/r;->a:Z

    if-nez v0, :cond_1

    if-nez p2, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 36
    :cond_1
    iput-object p2, p0, Lco/uk/getmondo/api/r;->c:Ljavax/a/a;

    .line 37
    sget-boolean v0, Lco/uk/getmondo/api/r;->a:Z

    if-nez v0, :cond_2

    if-nez p3, :cond_2

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 38
    :cond_2
    iput-object p3, p0, Lco/uk/getmondo/api/r;->d:Ljavax/a/a;

    .line 39
    sget-boolean v0, Lco/uk/getmondo/api/r;->a:Z

    if-nez v0, :cond_3

    if-nez p4, :cond_3

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 40
    :cond_3
    iput-object p4, p0, Lco/uk/getmondo/api/r;->e:Ljavax/a/a;

    .line 41
    sget-boolean v0, Lco/uk/getmondo/api/r;->a:Z

    if-nez v0, :cond_4

    if-nez p5, :cond_4

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 42
    :cond_4
    iput-object p5, p0, Lco/uk/getmondo/api/r;->f:Ljavax/a/a;

    .line 43
    return-void
.end method

.method public static a(Lco/uk/getmondo/api/c;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;)Lb/a/b;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lco/uk/getmondo/api/c;",
            "Ljavax/a/a",
            "<",
            "Lokhttp3/OkHttpClient;",
            ">;",
            "Ljavax/a/a",
            "<",
            "Lcom/google/gson/f;",
            ">;",
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/api/ac;",
            ">;",
            "Ljavax/a/a",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lb/a/b",
            "<",
            "Lco/uk/getmondo/api/authentication/MonzoOAuthApi;",
            ">;"
        }
    .end annotation

    .prologue
    .line 62
    new-instance v0, Lco/uk/getmondo/api/r;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lco/uk/getmondo/api/r;-><init>(Lco/uk/getmondo/api/c;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;)V

    return-object v0
.end method


# virtual methods
.method public a()Lco/uk/getmondo/api/authentication/MonzoOAuthApi;
    .locals 5

    .prologue
    .line 47
    iget-object v4, p0, Lco/uk/getmondo/api/r;->b:Lco/uk/getmondo/api/c;

    iget-object v0, p0, Lco/uk/getmondo/api/r;->c:Ljavax/a/a;

    .line 49
    invoke-interface {v0}, Ljavax/a/a;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lokhttp3/OkHttpClient;

    iget-object v1, p0, Lco/uk/getmondo/api/r;->d:Ljavax/a/a;

    .line 50
    invoke-interface {v1}, Ljavax/a/a;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/gson/f;

    iget-object v2, p0, Lco/uk/getmondo/api/r;->e:Ljavax/a/a;

    .line 51
    invoke-interface {v2}, Ljavax/a/a;->b()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lco/uk/getmondo/api/ac;

    iget-object v3, p0, Lco/uk/getmondo/api/r;->f:Ljavax/a/a;

    .line 52
    invoke-interface {v3}, Ljavax/a/a;->b()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 48
    invoke-virtual {v4, v0, v1, v2, v3}, Lco/uk/getmondo/api/c;->a(Lokhttp3/OkHttpClient;Lcom/google/gson/f;Lco/uk/getmondo/api/ac;Ljava/lang/String;)Lco/uk/getmondo/api/authentication/MonzoOAuthApi;

    move-result-object v0

    const-string v1, "Cannot return null from a non-@Nullable @Provides method"

    .line 47
    invoke-static {v0, v1}, Lb/a/d;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/api/authentication/MonzoOAuthApi;

    return-object v0
.end method

.method public synthetic b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 11
    invoke-virtual {p0}, Lco/uk/getmondo/api/r;->a()Lco/uk/getmondo/api/authentication/MonzoOAuthApi;

    move-result-object v0

    return-object v0
.end method
