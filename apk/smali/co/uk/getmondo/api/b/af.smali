.class public final Lco/uk/getmondo/api/b/af;
.super Ljava/lang/Object;
.source "UserInteractor_Factory.java"

# interfaces
.implements Lb/a/b;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lb/a/b",
        "<",
        "Lco/uk/getmondo/api/b/a;",
        ">;"
    }
.end annotation


# static fields
.field static final synthetic a:Z


# instance fields
.field private final b:Ljavax/a/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/common/s;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Ljavax/a/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/common/accounts/d;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Ljavax/a/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/api/MonzoApi;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Ljavax/a/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/api/authentication/MonzoOAuthApi;",
            ">;"
        }
    .end annotation
.end field

.field private final f:Ljavax/a/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/profile/data/MonzoProfileApi;",
            ">;"
        }
    .end annotation
.end field

.field private final g:Ljavax/a/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/card/c;",
            ">;"
        }
    .end annotation
.end field

.field private final h:Ljavax/a/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/common/k/f;",
            ">;"
        }
    .end annotation
.end field

.field private final i:Ljavax/a/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/common/a;",
            ">;"
        }
    .end annotation
.end field

.field private final j:Ljavax/a/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/common/q;",
            ">;"
        }
    .end annotation
.end field

.field private final k:Ljavax/a/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/common/g;",
            ">;"
        }
    .end annotation
.end field

.field private final l:Ljavax/a/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/common/accounts/m;",
            ">;"
        }
    .end annotation
.end field

.field private final m:Ljavax/a/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/payments/send/data/h;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 19
    const-class v0, Lco/uk/getmondo/api/b/af;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lco/uk/getmondo/api/b/af;->a:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/common/s;",
            ">;",
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/common/accounts/d;",
            ">;",
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/api/MonzoApi;",
            ">;",
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/api/authentication/MonzoOAuthApi;",
            ">;",
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/profile/data/MonzoProfileApi;",
            ">;",
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/card/c;",
            ">;",
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/common/k/f;",
            ">;",
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/common/a;",
            ">;",
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/common/q;",
            ">;",
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/common/g;",
            ">;",
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/common/accounts/m;",
            ">;",
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/payments/send/data/h;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 60
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 61
    sget-boolean v0, Lco/uk/getmondo/api/b/af;->a:Z

    if-nez v0, :cond_0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 62
    :cond_0
    iput-object p1, p0, Lco/uk/getmondo/api/b/af;->b:Ljavax/a/a;

    .line 63
    sget-boolean v0, Lco/uk/getmondo/api/b/af;->a:Z

    if-nez v0, :cond_1

    if-nez p2, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 64
    :cond_1
    iput-object p2, p0, Lco/uk/getmondo/api/b/af;->c:Ljavax/a/a;

    .line 65
    sget-boolean v0, Lco/uk/getmondo/api/b/af;->a:Z

    if-nez v0, :cond_2

    if-nez p3, :cond_2

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 66
    :cond_2
    iput-object p3, p0, Lco/uk/getmondo/api/b/af;->d:Ljavax/a/a;

    .line 67
    sget-boolean v0, Lco/uk/getmondo/api/b/af;->a:Z

    if-nez v0, :cond_3

    if-nez p4, :cond_3

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 68
    :cond_3
    iput-object p4, p0, Lco/uk/getmondo/api/b/af;->e:Ljavax/a/a;

    .line 69
    sget-boolean v0, Lco/uk/getmondo/api/b/af;->a:Z

    if-nez v0, :cond_4

    if-nez p5, :cond_4

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 70
    :cond_4
    iput-object p5, p0, Lco/uk/getmondo/api/b/af;->f:Ljavax/a/a;

    .line 71
    sget-boolean v0, Lco/uk/getmondo/api/b/af;->a:Z

    if-nez v0, :cond_5

    if-nez p6, :cond_5

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 72
    :cond_5
    iput-object p6, p0, Lco/uk/getmondo/api/b/af;->g:Ljavax/a/a;

    .line 73
    sget-boolean v0, Lco/uk/getmondo/api/b/af;->a:Z

    if-nez v0, :cond_6

    if-nez p7, :cond_6

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 74
    :cond_6
    iput-object p7, p0, Lco/uk/getmondo/api/b/af;->h:Ljavax/a/a;

    .line 75
    sget-boolean v0, Lco/uk/getmondo/api/b/af;->a:Z

    if-nez v0, :cond_7

    if-nez p8, :cond_7

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 76
    :cond_7
    iput-object p8, p0, Lco/uk/getmondo/api/b/af;->i:Ljavax/a/a;

    .line 77
    sget-boolean v0, Lco/uk/getmondo/api/b/af;->a:Z

    if-nez v0, :cond_8

    if-nez p9, :cond_8

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 78
    :cond_8
    iput-object p9, p0, Lco/uk/getmondo/api/b/af;->j:Ljavax/a/a;

    .line 79
    sget-boolean v0, Lco/uk/getmondo/api/b/af;->a:Z

    if-nez v0, :cond_9

    if-nez p10, :cond_9

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 80
    :cond_9
    iput-object p10, p0, Lco/uk/getmondo/api/b/af;->k:Ljavax/a/a;

    .line 81
    sget-boolean v0, Lco/uk/getmondo/api/b/af;->a:Z

    if-nez v0, :cond_a

    if-nez p11, :cond_a

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 82
    :cond_a
    iput-object p11, p0, Lco/uk/getmondo/api/b/af;->l:Ljavax/a/a;

    .line 83
    sget-boolean v0, Lco/uk/getmondo/api/b/af;->a:Z

    if-nez v0, :cond_b

    if-nez p12, :cond_b

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 84
    :cond_b
    iput-object p12, p0, Lco/uk/getmondo/api/b/af;->m:Ljavax/a/a;

    .line 85
    return-void
.end method

.method public static a(Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;)Lb/a/b;
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/common/s;",
            ">;",
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/common/accounts/d;",
            ">;",
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/api/MonzoApi;",
            ">;",
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/api/authentication/MonzoOAuthApi;",
            ">;",
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/profile/data/MonzoProfileApi;",
            ">;",
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/card/c;",
            ">;",
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/common/k/f;",
            ">;",
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/common/a;",
            ">;",
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/common/q;",
            ">;",
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/common/g;",
            ">;",
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/common/accounts/m;",
            ">;",
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/payments/send/data/h;",
            ">;)",
            "Lb/a/b",
            "<",
            "Lco/uk/getmondo/api/b/a;",
            ">;"
        }
    .end annotation

    .prologue
    .line 117
    new-instance v0, Lco/uk/getmondo/api/b/af;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    invoke-direct/range {v0 .. v12}, Lco/uk/getmondo/api/b/af;-><init>(Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;)V

    return-object v0
.end method


# virtual methods
.method public a()Lco/uk/getmondo/api/b/a;
    .locals 13

    .prologue
    .line 89
    new-instance v0, Lco/uk/getmondo/api/b/a;

    iget-object v1, p0, Lco/uk/getmondo/api/b/af;->b:Ljavax/a/a;

    .line 90
    invoke-interface {v1}, Ljavax/a/a;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lco/uk/getmondo/common/s;

    iget-object v2, p0, Lco/uk/getmondo/api/b/af;->c:Ljavax/a/a;

    .line 91
    invoke-interface {v2}, Ljavax/a/a;->b()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lco/uk/getmondo/common/accounts/d;

    iget-object v3, p0, Lco/uk/getmondo/api/b/af;->d:Ljavax/a/a;

    .line 92
    invoke-interface {v3}, Ljavax/a/a;->b()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lco/uk/getmondo/api/MonzoApi;

    iget-object v4, p0, Lco/uk/getmondo/api/b/af;->e:Ljavax/a/a;

    .line 93
    invoke-interface {v4}, Ljavax/a/a;->b()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lco/uk/getmondo/api/authentication/MonzoOAuthApi;

    iget-object v5, p0, Lco/uk/getmondo/api/b/af;->f:Ljavax/a/a;

    .line 94
    invoke-interface {v5}, Ljavax/a/a;->b()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lco/uk/getmondo/profile/data/MonzoProfileApi;

    iget-object v6, p0, Lco/uk/getmondo/api/b/af;->g:Ljavax/a/a;

    .line 95
    invoke-interface {v6}, Ljavax/a/a;->b()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lco/uk/getmondo/card/c;

    iget-object v7, p0, Lco/uk/getmondo/api/b/af;->h:Ljavax/a/a;

    .line 96
    invoke-interface {v7}, Ljavax/a/a;->b()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lco/uk/getmondo/common/k/f;

    iget-object v8, p0, Lco/uk/getmondo/api/b/af;->i:Ljavax/a/a;

    .line 97
    invoke-interface {v8}, Ljavax/a/a;->b()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lco/uk/getmondo/common/a;

    iget-object v9, p0, Lco/uk/getmondo/api/b/af;->j:Ljavax/a/a;

    .line 98
    invoke-interface {v9}, Ljavax/a/a;->b()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lco/uk/getmondo/common/q;

    iget-object v10, p0, Lco/uk/getmondo/api/b/af;->k:Ljavax/a/a;

    .line 99
    invoke-interface {v10}, Ljavax/a/a;->b()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lco/uk/getmondo/common/g;

    iget-object v11, p0, Lco/uk/getmondo/api/b/af;->l:Ljavax/a/a;

    .line 100
    invoke-interface {v11}, Ljavax/a/a;->b()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lco/uk/getmondo/common/accounts/m;

    iget-object v12, p0, Lco/uk/getmondo/api/b/af;->m:Ljavax/a/a;

    .line 101
    invoke-interface {v12}, Ljavax/a/a;->b()Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lco/uk/getmondo/payments/send/data/h;

    invoke-direct/range {v0 .. v12}, Lco/uk/getmondo/api/b/a;-><init>(Lco/uk/getmondo/common/s;Lco/uk/getmondo/common/accounts/d;Lco/uk/getmondo/api/MonzoApi;Lco/uk/getmondo/api/authentication/MonzoOAuthApi;Lco/uk/getmondo/profile/data/MonzoProfileApi;Lco/uk/getmondo/card/c;Lco/uk/getmondo/common/k/f;Lco/uk/getmondo/common/a;Lco/uk/getmondo/common/q;Lco/uk/getmondo/common/g;Lco/uk/getmondo/common/accounts/m;Lco/uk/getmondo/payments/send/data/h;)V

    .line 89
    return-object v0
.end method

.method public synthetic b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 19
    invoke-virtual {p0}, Lco/uk/getmondo/api/b/af;->a()Lco/uk/getmondo/api/b/a;

    move-result-object v0

    return-object v0
.end method
