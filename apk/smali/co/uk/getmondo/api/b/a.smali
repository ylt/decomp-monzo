.class public Lco/uk/getmondo/api/b/a;
.super Ljava/lang/Object;
.source "UserInteractor.java"


# instance fields
.field private final a:Lco/uk/getmondo/common/s;

.field private final b:Lco/uk/getmondo/common/accounts/d;

.field private final c:Lco/uk/getmondo/api/MonzoApi;

.field private final d:Lco/uk/getmondo/api/authentication/MonzoOAuthApi;

.field private final e:Lco/uk/getmondo/profile/data/MonzoProfileApi;

.field private final f:Lco/uk/getmondo/card/c;

.field private final g:Lco/uk/getmondo/common/k/f;

.field private final h:Lco/uk/getmondo/common/a;

.field private final i:Lco/uk/getmondo/common/q;

.field private final j:Lco/uk/getmondo/common/g;

.field private final k:Lco/uk/getmondo/common/accounts/m;

.field private l:Lco/uk/getmondo/payments/send/data/h;


# direct methods
.method public constructor <init>(Lco/uk/getmondo/common/s;Lco/uk/getmondo/common/accounts/d;Lco/uk/getmondo/api/MonzoApi;Lco/uk/getmondo/api/authentication/MonzoOAuthApi;Lco/uk/getmondo/profile/data/MonzoProfileApi;Lco/uk/getmondo/card/c;Lco/uk/getmondo/common/k/f;Lco/uk/getmondo/common/a;Lco/uk/getmondo/common/q;Lco/uk/getmondo/common/g;Lco/uk/getmondo/common/accounts/m;Lco/uk/getmondo/payments/send/data/h;)V
    .locals 0

    .prologue
    .line 81
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 82
    iput-object p1, p0, Lco/uk/getmondo/api/b/a;->a:Lco/uk/getmondo/common/s;

    .line 83
    iput-object p2, p0, Lco/uk/getmondo/api/b/a;->b:Lco/uk/getmondo/common/accounts/d;

    .line 84
    iput-object p3, p0, Lco/uk/getmondo/api/b/a;->c:Lco/uk/getmondo/api/MonzoApi;

    .line 85
    iput-object p4, p0, Lco/uk/getmondo/api/b/a;->d:Lco/uk/getmondo/api/authentication/MonzoOAuthApi;

    .line 86
    iput-object p5, p0, Lco/uk/getmondo/api/b/a;->e:Lco/uk/getmondo/profile/data/MonzoProfileApi;

    .line 87
    iput-object p6, p0, Lco/uk/getmondo/api/b/a;->f:Lco/uk/getmondo/card/c;

    .line 88
    iput-object p7, p0, Lco/uk/getmondo/api/b/a;->g:Lco/uk/getmondo/common/k/f;

    .line 89
    iput-object p8, p0, Lco/uk/getmondo/api/b/a;->h:Lco/uk/getmondo/common/a;

    .line 90
    iput-object p9, p0, Lco/uk/getmondo/api/b/a;->i:Lco/uk/getmondo/common/q;

    .line 91
    iput-object p10, p0, Lco/uk/getmondo/api/b/a;->j:Lco/uk/getmondo/common/g;

    .line 92
    iput-object p11, p0, Lco/uk/getmondo/api/b/a;->k:Lco/uk/getmondo/common/accounts/m;

    .line 93
    iput-object p12, p0, Lco/uk/getmondo/api/b/a;->l:Lco/uk/getmondo/payments/send/data/h;

    .line 94
    return-void
.end method

.method static synthetic a(Lco/uk/getmondo/api/model/ApiToken;)Landroid/support/v4/g/j;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 131
    new-instance v0, Lco/uk/getmondo/d/a/s;

    invoke-direct {v0}, Lco/uk/getmondo/d/a/s;-><init>()V

    invoke-virtual {v0, p0}, Lco/uk/getmondo/d/a/s;->a(Lco/uk/getmondo/api/model/ApiToken;)Lco/uk/getmondo/d/ai;

    move-result-object v0

    invoke-virtual {p0}, Lco/uk/getmondo/api/model/ApiToken;->d()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/support/v4/g/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Landroid/support/v4/g/j;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lco/uk/getmondo/api/b/a;Lco/uk/getmondo/d/ai;Ljava/lang/String;Ljava/lang/String;)Lco/uk/getmondo/d/ai;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 140
    iget-object v0, p0, Lco/uk/getmondo/api/b/a;->b:Lco/uk/getmondo/common/accounts/d;

    invoke-virtual {v0}, Lco/uk/getmondo/common/accounts/d;->e()V

    .line 142
    iget-object v0, p0, Lco/uk/getmondo/api/b/a;->b:Lco/uk/getmondo/common/accounts/d;

    invoke-virtual {v0, p1, p2, p3}, Lco/uk/getmondo/common/accounts/d;->a(Lco/uk/getmondo/d/ai;Ljava/lang/String;Ljava/lang/String;)V

    .line 143
    return-object p1
.end method

.method static synthetic a(Lco/uk/getmondo/api/b/a;Lco/uk/getmondo/common/accounts/l;)Lco/uk/getmondo/d/ak;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 165
    iget-object v0, p0, Lco/uk/getmondo/api/b/a;->b:Lco/uk/getmondo/common/accounts/d;

    invoke-virtual {v0}, Lco/uk/getmondo/common/accounts/d;->b()Lco/uk/getmondo/d/ak;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lco/uk/getmondo/api/b/a;Lco/uk/getmondo/api/model/ApiAccount;)Lio/reactivex/v;
    .locals 1

    invoke-direct {p0, p1}, Lco/uk/getmondo/api/b/a;->a(Lco/uk/getmondo/api/model/ApiAccount;)Lio/reactivex/v;

    move-result-object v0

    return-object v0
.end method

.method private a(Lco/uk/getmondo/api/model/ApiAccount;)Lio/reactivex/v;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lco/uk/getmondo/api/model/ApiAccount;",
            ")",
            "Lio/reactivex/v",
            "<",
            "Lco/uk/getmondo/d/a;",
            ">;"
        }
    .end annotation

    .prologue
    .line 202
    invoke-virtual {p1}, Lco/uk/getmondo/api/model/ApiAccount;->f()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lco/uk/getmondo/d/a$b;->a(Ljava/lang/String;)Lco/uk/getmondo/d/a$b;

    move-result-object v0

    sget-object v1, Lco/uk/getmondo/d/a$b;->RETAIL:Lco/uk/getmondo/d/a$b;

    if-ne v0, v1, :cond_0

    .line 204
    iget-object v0, p0, Lco/uk/getmondo/api/b/a;->f:Lco/uk/getmondo/card/c;

    invoke-virtual {p1}, Lco/uk/getmondo/api/model/ApiAccount;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lco/uk/getmondo/card/c;->a(Ljava/lang/String;)Lio/reactivex/v;

    move-result-object v0

    invoke-static {}, Lco/uk/getmondo/api/b/j;->a()Lio/reactivex/c/h;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/v;->d(Lio/reactivex/c/h;)Lio/reactivex/v;

    move-result-object v0

    .line 209
    :goto_0
    invoke-static {p1}, Lco/uk/getmondo/api/b/l;->a(Lco/uk/getmondo/api/model/ApiAccount;)Lio/reactivex/c/h;

    move-result-object v1

    .line 210
    invoke-virtual {v0, v1}, Lio/reactivex/v;->d(Lio/reactivex/c/h;)Lio/reactivex/v;

    move-result-object v0

    const-class v1, Lco/uk/getmondo/d/a;

    .line 211
    invoke-virtual {v0, v1}, Lio/reactivex/v;->a(Ljava/lang/Class;)Lio/reactivex/v;

    move-result-object v0

    .line 209
    return-object v0

    .line 206
    :cond_0
    invoke-virtual {p1}, Lco/uk/getmondo/api/model/ApiAccount;->a()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lco/uk/getmondo/api/b/a;->c(Ljava/lang/String;)Lio/reactivex/v;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/api/b/a;->f:Lco/uk/getmondo/card/c;

    invoke-virtual {p1}, Lco/uk/getmondo/api/model/ApiAccount;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lco/uk/getmondo/card/c;->a(Ljava/lang/String;)Lio/reactivex/v;

    move-result-object v1

    invoke-static {}, Lco/uk/getmondo/api/b/k;->a()Lio/reactivex/c/c;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lio/reactivex/v;->a(Lio/reactivex/z;Lio/reactivex/z;Lio/reactivex/c/c;)Lio/reactivex/v;

    move-result-object v0

    goto :goto_0
.end method

.method static synthetic a(Lco/uk/getmondo/api/b/a;)Lio/reactivex/z;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 197
    iget-object v0, p0, Lco/uk/getmondo/api/b/a;->l:Lco/uk/getmondo/payments/send/data/h;

    invoke-virtual {v0}, Lco/uk/getmondo/payments/send/data/h;->d()Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v0}, Lio/reactivex/v;->a(Ljava/lang/Object;)Lio/reactivex/v;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lco/uk/getmondo/api/b/a;Lco/uk/getmondo/d/ai;)Lio/reactivex/z;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 145
    .line 146
    invoke-direct {p0}, Lco/uk/getmondo/api/b/a;->c()Lio/reactivex/v;

    move-result-object v0

    .line 147
    invoke-direct {p0}, Lco/uk/getmondo/api/b/a;->e()Lio/reactivex/v;

    move-result-object v1

    .line 148
    invoke-direct {p0}, Lco/uk/getmondo/api/b/a;->d()Lio/reactivex/v;

    move-result-object v2

    iget-object v3, p0, Lco/uk/getmondo/api/b/a;->k:Lco/uk/getmondo/common/accounts/m;

    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    invoke-static {v3}, Lco/uk/getmondo/api/b/v;->a(Lco/uk/getmondo/common/accounts/m;)Lio/reactivex/c/i;

    move-result-object v3

    .line 145
    invoke-static {v0, v1, v2, v3}, Lio/reactivex/v;->a(Lio/reactivex/z;Lio/reactivex/z;Lio/reactivex/z;Lio/reactivex/c/i;)Lio/reactivex/v;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lco/uk/getmondo/api/b/a;Ljava/lang/String;Landroid/support/v4/g/j;)Lio/reactivex/z;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 132
    iget-object v0, p2, Landroid/support/v4/g/j;->a:Ljava/lang/Object;

    check-cast v0, Lco/uk/getmondo/d/ai;

    iget-object v1, p2, Landroid/support/v4/g/j;->b:Ljava/lang/Object;

    check-cast v1, Ljava/lang/String;

    invoke-virtual {p0, v0, p1, v1}, Lco/uk/getmondo/api/b/a;->a(Lco/uk/getmondo/d/ai;Ljava/lang/String;Ljava/lang/String;)Lio/reactivex/v;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lco/uk/getmondo/api/b/a;Ljava/lang/String;Ljava/lang/String;)Lio/reactivex/z;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 123
    iget-object v0, p0, Lco/uk/getmondo/api/b/a;->a:Lco/uk/getmondo/common/s;

    invoke-interface {v0}, Lco/uk/getmondo/common/s;->a()Ljava/lang/String;

    move-result-object v0

    .line 124
    iget-object v1, p0, Lco/uk/getmondo/api/b/a;->a:Lco/uk/getmondo/common/s;

    invoke-interface {v1}, Lco/uk/getmondo/common/s;->b()Ljava/lang/String;

    move-result-object v1

    .line 125
    invoke-direct {p0, v0, v1}, Lco/uk/getmondo/api/b/a;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 126
    new-instance v1, Lco/uk/getmondo/api/authentication/OAuthException;

    invoke-direct {v1, v0}, Lco/uk/getmondo/api/authentication/OAuthException;-><init>(Ljava/lang/String;)V

    invoke-static {v1}, Lio/reactivex/v;->a(Ljava/lang/Throwable;)Lio/reactivex/v;

    move-result-object v0

    .line 130
    :goto_0
    return-object v0

    :cond_0
    iget-object v1, p0, Lco/uk/getmondo/api/b/a;->d:Lco/uk/getmondo/api/authentication/MonzoOAuthApi;

    sget-object v2, Lco/uk/getmondo/api/authentication/MonzoOAuthApi;->a:Ljava/lang/String;

    const-string v3, "authorization_code"

    const-string v4, "https://monzo.com/-magic-auth"

    invoke-interface {v1, v2, v3, p2, v4}, Lco/uk/getmondo/api/authentication/MonzoOAuthApi;->requestUserToken(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lio/reactivex/v;

    move-result-object v1

    invoke-static {}, Lco/uk/getmondo/api/b/w;->a()Lio/reactivex/c/h;

    move-result-object v2

    .line 131
    invoke-virtual {v1, v2}, Lio/reactivex/v;->d(Lio/reactivex/c/h;)Lio/reactivex/v;

    move-result-object v1

    invoke-static {p0, v0}, Lco/uk/getmondo/api/b/y;->a(Lco/uk/getmondo/api/b/a;Ljava/lang/String;)Lio/reactivex/c/h;

    move-result-object v0

    .line 132
    invoke-virtual {v1, v0}, Lio/reactivex/v;->a(Lio/reactivex/c/h;)Lio/reactivex/v;

    move-result-object v0

    goto :goto_0
.end method

.method static synthetic a(Lco/uk/getmondo/api/b/a;Ljava/util/List;)Lio/reactivex/z;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 191
    invoke-static {p1}, Lio/reactivex/n;->fromIterable(Ljava/lang/Iterable;)Lio/reactivex/n;

    move-result-object v0

    invoke-static {p0}, Lco/uk/getmondo/api/b/u;->a(Lco/uk/getmondo/api/b/a;)Lio/reactivex/c/h;

    move-result-object v1

    .line 192
    invoke-virtual {v0, v1}, Lio/reactivex/n;->flatMapSingle(Lio/reactivex/c/h;)Lio/reactivex/n;

    move-result-object v0

    .line 193
    invoke-virtual {v0}, Lio/reactivex/n;->toList()Lio/reactivex/v;

    move-result-object v0

    .line 191
    return-object v0
.end method

.method static synthetic a(Ljava/lang/Throwable;)Lio/reactivex/z;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 226
    instance-of v0, p0, Lco/uk/getmondo/api/ApiException;

    if-eqz v0, :cond_0

    move-object v0, p0

    .line 227
    check-cast v0, Lco/uk/getmondo/api/ApiException;

    .line 228
    invoke-virtual {v0}, Lco/uk/getmondo/api/ApiException;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 229
    invoke-static {}, Lcom/c/b/b;->c()Lcom/c/b/b;

    move-result-object v0

    invoke-static {v0}, Lio/reactivex/v;->a(Ljava/lang/Object;)Lio/reactivex/v;

    move-result-object v0

    .line 232
    :goto_0
    return-object v0

    :cond_0
    invoke-static {p0}, Lio/reactivex/v;->a(Ljava/lang/Throwable;)Lio/reactivex/v;

    move-result-object v0

    goto :goto_0
.end method

.method static synthetic a(Lco/uk/getmondo/api/model/ApiAccount;Lco/uk/getmondo/d/a/a;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 210
    invoke-virtual {p1, p0}, Lco/uk/getmondo/d/a/a;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lco/uk/getmondo/api/b/a;Lco/uk/getmondo/d/ak;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 166
    iget-object v0, p0, Lco/uk/getmondo/api/b/a;->i:Lco/uk/getmondo/common/q;

    invoke-virtual {p1}, Lco/uk/getmondo/d/ak;->d()Lco/uk/getmondo/d/ac;

    move-result-object v1

    invoke-virtual {p1}, Lco/uk/getmondo/d/ak;->c()Lco/uk/getmondo/d/a;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lco/uk/getmondo/common/q;->a(Lco/uk/getmondo/d/ac;Lco/uk/getmondo/d/a;)V

    return-void
.end method

.method static synthetic a(Lco/uk/getmondo/api/b/a;Lco/uk/getmondo/signup_old/a/a/a;Ljava/lang/String;Lco/uk/getmondo/d/an;)V
    .locals 10
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 107
    new-instance v0, Lco/uk/getmondo/d/ac;

    invoke-virtual {p3}, Lco/uk/getmondo/d/an;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lco/uk/getmondo/signup_old/a/a/a;->a()Ljava/lang/String;

    move-result-object v2

    .line 108
    invoke-virtual {p1}, Lco/uk/getmondo/signup_old/a/a/a;->b()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1}, Lco/uk/getmondo/signup_old/a/a/a;->c()Lco/uk/getmondo/d/s;

    move-result-object v8

    const/4 v9, 0x0

    move-object v4, p2

    move-object v6, v3

    move-object v7, v3

    invoke-direct/range {v0 .. v9}, Lco/uk/getmondo/d/ac;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Lco/uk/getmondo/d/s;Z)V

    .line 110
    iget-object v1, p0, Lco/uk/getmondo/api/b/a;->b:Lco/uk/getmondo/common/accounts/d;

    invoke-virtual {v1, v0, p3}, Lco/uk/getmondo/common/accounts/d;->a(Lco/uk/getmondo/d/ac;Lco/uk/getmondo/d/an;)V

    .line 111
    return-void
.end method

.method static synthetic a(Lco/uk/getmondo/api/b/a;Ljava/lang/Throwable;)V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 245
    instance-of v0, p1, Lco/uk/getmondo/api/ApiException;

    if-eqz v0, :cond_0

    .line 246
    check-cast p1, Lco/uk/getmondo/api/ApiException;

    .line 247
    invoke-virtual {p1}, Lco/uk/getmondo/api/ApiException;->b()I

    move-result v0

    .line 248
    invoke-virtual {p1}, Lco/uk/getmondo/api/ApiException;->c()Ljava/lang/String;

    move-result-object v1

    .line 249
    invoke-virtual {p1}, Lco/uk/getmondo/api/ApiException;->d()Ljava/lang/String;

    move-result-object v2

    .line 250
    invoke-virtual {p1}, Lco/uk/getmondo/api/ApiException;->e()Lco/uk/getmondo/api/model/b;

    move-result-object v3

    .line 251
    iget-object v4, p0, Lco/uk/getmondo/api/b/a;->h:Lco/uk/getmondo/common/a;

    invoke-static {v0, v1, v2, v3}, Lco/uk/getmondo/api/model/tracking/Impression;->b(ILjava/lang/String;Ljava/lang/String;Lco/uk/getmondo/api/model/b;)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    invoke-virtual {v4, v0}, Lco/uk/getmondo/common/a;->a(Lco/uk/getmondo/api/model/tracking/Impression;)V

    .line 253
    :cond_0
    return-void
.end method

.method static synthetic b(Lco/uk/getmondo/api/b/a;Lco/uk/getmondo/common/accounts/l;)Lco/uk/getmondo/d/ak;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 152
    iget-object v0, p0, Lco/uk/getmondo/api/b/a;->b:Lco/uk/getmondo/common/accounts/d;

    invoke-virtual {v0}, Lco/uk/getmondo/common/accounts/d;->b()Lco/uk/getmondo/d/ak;

    move-result-object v0

    return-object v0
.end method

.method private b(Lco/uk/getmondo/signup_old/a/a/a;)Lio/reactivex/b;
    .locals 7

    .prologue
    .line 101
    iget-object v0, p0, Lco/uk/getmondo/api/b/a;->a:Lco/uk/getmondo/common/s;

    invoke-interface {v0}, Lco/uk/getmondo/common/s;->a()Ljava/lang/String;

    move-result-object v1

    .line 102
    iget-object v6, p0, Lco/uk/getmondo/api/b/a;->c:Lco/uk/getmondo/api/MonzoApi;

    new-instance v0, Lco/uk/getmondo/api/a/a;

    invoke-virtual {p1}, Lco/uk/getmondo/signup_old/a/a/a;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lco/uk/getmondo/signup_old/a/a/a;->c()Lco/uk/getmondo/d/s;

    move-result-object v3

    invoke-virtual {v3}, Lco/uk/getmondo/d/s;->j()Ljava/lang/String;

    move-result-object v3

    .line 103
    invoke-virtual {p1}, Lco/uk/getmondo/signup_old/a/a/a;->b()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1}, Lco/uk/getmondo/signup_old/a/a/a;->c()Lco/uk/getmondo/d/s;

    move-result-object v5

    invoke-virtual {v5}, Lco/uk/getmondo/d/s;->c()Ljava/lang/String;

    move-result-object v5

    invoke-direct/range {v0 .. v5}, Lco/uk/getmondo/api/a/a;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 104
    invoke-virtual {v0}, Lco/uk/getmondo/api/a/a;->a()Ljava/util/Map;

    move-result-object v0

    .line 102
    invoke-interface {v6, v0}, Lco/uk/getmondo/api/MonzoApi;->waitlistSignUp(Ljava/util/Map;)Lio/reactivex/v;

    move-result-object v0

    new-instance v2, Lco/uk/getmondo/d/a/v;

    invoke-direct {v2}, Lco/uk/getmondo/d/a/v;-><init>()V

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    invoke-static {v2}, Lco/uk/getmondo/api/b/b;->a(Lco/uk/getmondo/d/a/v;)Lio/reactivex/c/h;

    move-result-object v2

    .line 105
    invoke-virtual {v0, v2}, Lio/reactivex/v;->d(Lio/reactivex/c/h;)Lio/reactivex/v;

    move-result-object v0

    invoke-static {p0, p1, v1}, Lco/uk/getmondo/api/b/m;->a(Lco/uk/getmondo/api/b/a;Lco/uk/getmondo/signup_old/a/a/a;Ljava/lang/String;)Lio/reactivex/c/g;

    move-result-object v1

    .line 106
    invoke-virtual {v0, v1}, Lio/reactivex/v;->c(Lio/reactivex/c/g;)Lio/reactivex/v;

    move-result-object v0

    .line 112
    invoke-virtual {v0}, Lio/reactivex/v;->c()Lio/reactivex/b;

    move-result-object v0

    .line 102
    return-object v0
.end method

.method private b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 181
    :try_start_0
    iget-object v0, p0, Lco/uk/getmondo/api/b/a;->g:Lco/uk/getmondo/common/k/f;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lco/uk/getmondo/common/k/f;->a(Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object p2

    .line 185
    :goto_0
    return-object p2

    .line 182
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method static synthetic b(Lco/uk/getmondo/api/b/a;Lco/uk/getmondo/d/ak;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 156
    iget-object v0, p0, Lco/uk/getmondo/api/b/a;->i:Lco/uk/getmondo/common/q;

    invoke-virtual {p1}, Lco/uk/getmondo/d/ak;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lco/uk/getmondo/common/q;->a(Ljava/lang/String;)V

    .line 157
    iget-object v0, p0, Lco/uk/getmondo/api/b/a;->j:Lco/uk/getmondo/common/g;

    invoke-virtual {p1}, Lco/uk/getmondo/d/ak;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lco/uk/getmondo/common/g;->a(Ljava/lang/String;)V

    .line 158
    return-void
.end method

.method static synthetic b(Lco/uk/getmondo/api/b/a;Ljava/lang/Throwable;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 154
    iget-object v0, p0, Lco/uk/getmondo/api/b/a;->b:Lco/uk/getmondo/common/accounts/d;

    invoke-virtual {v0}, Lco/uk/getmondo/common/accounts/d;->e()V

    return-void
.end method

.method private c()Lio/reactivex/v;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/v",
            "<",
            "Ljava/util/List",
            "<",
            "Lco/uk/getmondo/d/a;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 189
    iget-object v0, p0, Lco/uk/getmondo/api/b/a;->c:Lco/uk/getmondo/api/MonzoApi;

    invoke-interface {v0}, Lco/uk/getmondo/api/MonzoApi;->accounts()Lio/reactivex/v;

    move-result-object v0

    invoke-static {}, Lco/uk/getmondo/api/b/g;->a()Lio/reactivex/c/h;

    move-result-object v1

    .line 190
    invoke-virtual {v0, v1}, Lio/reactivex/v;->d(Lio/reactivex/c/h;)Lio/reactivex/v;

    move-result-object v0

    invoke-static {p0}, Lco/uk/getmondo/api/b/h;->a(Lco/uk/getmondo/api/b/a;)Lio/reactivex/c/h;

    move-result-object v1

    .line 191
    invoke-virtual {v0, v1}, Lio/reactivex/v;->a(Lio/reactivex/c/h;)Lio/reactivex/v;

    move-result-object v0

    .line 189
    return-object v0
.end method

.method private c(Ljava/lang/String;)Lio/reactivex/v;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/v",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .prologue
    .line 215
    iget-object v0, p0, Lco/uk/getmondo/api/b/a;->c:Lco/uk/getmondo/api/MonzoApi;

    invoke-interface {v0, p1}, Lco/uk/getmondo/api/MonzoApi;->getInitialTopupStatus(Ljava/lang/String;)Lio/reactivex/v;

    move-result-object v0

    invoke-static {}, Lco/uk/getmondo/api/b/n;->a()Lio/reactivex/c/h;

    move-result-object v1

    .line 216
    invoke-virtual {v0, v1}, Lio/reactivex/v;->d(Lio/reactivex/c/h;)Lio/reactivex/v;

    move-result-object v0

    .line 215
    return-object v0
.end method

.method private d()Lio/reactivex/v;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/v",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .prologue
    .line 197
    iget-object v0, p0, Lco/uk/getmondo/api/b/a;->l:Lco/uk/getmondo/payments/send/data/h;

    invoke-virtual {v0}, Lco/uk/getmondo/payments/send/data/h;->a()Lio/reactivex/b;

    move-result-object v0

    invoke-static {p0}, Lco/uk/getmondo/api/b/i;->a(Lco/uk/getmondo/api/b/a;)Ljava/util/concurrent/Callable;

    move-result-object v1

    invoke-static {v1}, Lio/reactivex/v;->a(Ljava/util/concurrent/Callable;)Lio/reactivex/v;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/b;->a(Lio/reactivex/z;)Lio/reactivex/v;

    move-result-object v0

    return-object v0
.end method

.method private e()Lio/reactivex/v;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/v",
            "<",
            "Lcom/c/b/b",
            "<",
            "Lco/uk/getmondo/d/ac;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 222
    iget-object v0, p0, Lco/uk/getmondo/api/b/a;->e:Lco/uk/getmondo/profile/data/MonzoProfileApi;

    invoke-interface {v0}, Lco/uk/getmondo/profile/data/MonzoProfileApi;->profile()Lio/reactivex/v;

    move-result-object v0

    new-instance v1, Lco/uk/getmondo/d/a/p;

    invoke-direct {v1}, Lco/uk/getmondo/d/a/p;-><init>()V

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    invoke-static {v1}, Lco/uk/getmondo/api/b/o;->a(Lco/uk/getmondo/d/a/p;)Lio/reactivex/c/h;

    move-result-object v1

    .line 223
    invoke-virtual {v0, v1}, Lio/reactivex/v;->d(Lio/reactivex/c/h;)Lio/reactivex/v;

    move-result-object v0

    invoke-static {}, Lco/uk/getmondo/api/b/p;->a()Lio/reactivex/c/h;

    move-result-object v1

    .line 224
    invoke-virtual {v0, v1}, Lio/reactivex/v;->d(Lio/reactivex/c/h;)Lio/reactivex/v;

    move-result-object v0

    invoke-static {}, Lco/uk/getmondo/api/b/q;->a()Lio/reactivex/c/h;

    move-result-object v1

    .line 225
    invoke-virtual {v0, v1}, Lio/reactivex/v;->f(Lio/reactivex/c/h;)Lio/reactivex/v;

    move-result-object v0

    .line 222
    return-object v0
.end method


# virtual methods
.method public a(Lco/uk/getmondo/create_account/a/a/a;)Lio/reactivex/b;
    .locals 4

    .prologue
    .line 116
    iget-object v0, p0, Lco/uk/getmondo/api/b/a;->c:Lco/uk/getmondo/api/MonzoApi;

    sget-object v1, Lco/uk/getmondo/api/model/signup/SignupSource;->LEGACY_PREPAID:Lco/uk/getmondo/api/model/signup/SignupSource;

    invoke-virtual {p1}, Lco/uk/getmondo/create_account/a/a/a;->a()Ljava/util/Map;

    move-result-object v2

    invoke-virtual {p1}, Lco/uk/getmondo/create_account/a/a/a;->b()Lco/uk/getmondo/d/s;

    move-result-object v3

    invoke-virtual {v3}, Lco/uk/getmondo/d/s;->h()[Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v1, v2, v3}, Lco/uk/getmondo/api/MonzoApi;->createAccount(Lco/uk/getmondo/api/model/signup/SignupSource;Ljava/util/Map;[Ljava/lang/String;)Lio/reactivex/b;

    move-result-object v0

    .line 117
    invoke-virtual {p0}, Lco/uk/getmondo/api/b/a;->a()Lio/reactivex/v;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/b;->a(Lio/reactivex/z;)Lio/reactivex/v;

    move-result-object v0

    .line 118
    invoke-virtual {v0}, Lio/reactivex/v;->c()Lio/reactivex/b;

    move-result-object v0

    .line 116
    return-object v0
.end method

.method public a(Lco/uk/getmondo/signup_old/a/a/a;)Lio/reactivex/b;
    .locals 1

    .prologue
    .line 97
    invoke-direct {p0, p1}, Lco/uk/getmondo/api/b/a;->b(Lco/uk/getmondo/signup_old/a/a/a;)Lio/reactivex/b;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/lang/String;)Lio/reactivex/b;
    .locals 7

    .prologue
    .line 170
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v2

    .line 171
    iget-object v0, p0, Lco/uk/getmondo/api/b/a;->a:Lco/uk/getmondo/common/s;

    invoke-interface {v0, v2}, Lco/uk/getmondo/common/s;->b(Ljava/lang/String;)V

    .line 172
    iget-object v0, p0, Lco/uk/getmondo/api/b/a;->a:Lco/uk/getmondo/common/s;

    invoke-interface {v0, p1}, Lco/uk/getmondo/common/s;->a(Ljava/lang/String;)V

    .line 174
    iget-object v0, p0, Lco/uk/getmondo/api/b/a;->d:Lco/uk/getmondo/api/authentication/MonzoOAuthApi;

    sget-object v1, Lco/uk/getmondo/api/authentication/MonzoOAuthApi;->a:Ljava/lang/String;

    const-string v3, "oauthclient_000097JsUCy1aF4Hud2iJN"

    const-string v4, "code"

    const-string v5, "https://monzo.com/-magic-auth"

    .line 175
    invoke-direct {p0, p1, v2}, Lco/uk/getmondo/api/b/a;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    move-object v2, p1

    .line 174
    invoke-interface/range {v0 .. v6}, Lco/uk/getmondo/api/authentication/MonzoOAuthApi;->authorize(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lio/reactivex/b;

    move-result-object v0

    return-object v0
.end method

.method public a()Lio/reactivex/v;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/v",
            "<",
            "Lco/uk/getmondo/d/ak;",
            ">;"
        }
    .end annotation

    .prologue
    .line 162
    invoke-direct {p0}, Lco/uk/getmondo/api/b/a;->c()Lio/reactivex/v;

    move-result-object v0

    invoke-direct {p0}, Lco/uk/getmondo/api/b/a;->e()Lio/reactivex/v;

    move-result-object v1

    invoke-direct {p0}, Lco/uk/getmondo/api/b/a;->d()Lio/reactivex/v;

    move-result-object v2

    iget-object v3, p0, Lco/uk/getmondo/api/b/a;->k:Lco/uk/getmondo/common/accounts/m;

    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    invoke-static {v3}, Lco/uk/getmondo/api/b/c;->a(Lco/uk/getmondo/common/accounts/m;)Lio/reactivex/c/i;

    move-result-object v3

    invoke-static {v0, v1, v2, v3}, Lio/reactivex/v;->a(Lio/reactivex/z;Lio/reactivex/z;Lio/reactivex/z;Lio/reactivex/c/i;)Lio/reactivex/v;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/api/b/a;->b:Lco/uk/getmondo/common/accounts/d;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    invoke-static {v1}, Lco/uk/getmondo/api/b/d;->a(Lco/uk/getmondo/common/accounts/d;)Lio/reactivex/c/g;

    move-result-object v1

    .line 164
    invoke-virtual {v0, v1}, Lio/reactivex/v;->c(Lio/reactivex/c/g;)Lio/reactivex/v;

    move-result-object v0

    invoke-static {p0}, Lco/uk/getmondo/api/b/e;->a(Lco/uk/getmondo/api/b/a;)Lio/reactivex/c/h;

    move-result-object v1

    .line 165
    invoke-virtual {v0, v1}, Lio/reactivex/v;->d(Lio/reactivex/c/h;)Lio/reactivex/v;

    move-result-object v0

    invoke-static {p0}, Lco/uk/getmondo/api/b/f;->a(Lco/uk/getmondo/api/b/a;)Lio/reactivex/c/g;

    move-result-object v1

    .line 166
    invoke-virtual {v0, v1}, Lio/reactivex/v;->c(Lio/reactivex/c/g;)Lio/reactivex/v;

    move-result-object v0

    .line 162
    return-object v0
.end method

.method public a(Lco/uk/getmondo/d/ai;Ljava/lang/String;Ljava/lang/String;)Lio/reactivex/v;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lco/uk/getmondo/d/ai;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/v",
            "<",
            "Lco/uk/getmondo/d/ak;",
            ">;"
        }
    .end annotation

    .prologue
    .line 138
    invoke-static {p0, p1, p2, p3}, Lco/uk/getmondo/api/b/z;->a(Lco/uk/getmondo/api/b/a;Lco/uk/getmondo/d/ai;Ljava/lang/String;Ljava/lang/String;)Ljava/util/concurrent/Callable;

    move-result-object v0

    invoke-static {v0}, Lio/reactivex/v;->c(Ljava/util/concurrent/Callable;)Lio/reactivex/v;

    move-result-object v0

    invoke-static {p0}, Lco/uk/getmondo/api/b/aa;->a(Lco/uk/getmondo/api/b/a;)Lio/reactivex/c/h;

    move-result-object v1

    .line 145
    invoke-virtual {v0, v1}, Lio/reactivex/v;->a(Lio/reactivex/c/h;)Lio/reactivex/v;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/api/b/a;->b:Lco/uk/getmondo/common/accounts/d;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    invoke-static {v1}, Lco/uk/getmondo/api/b/ab;->a(Lco/uk/getmondo/common/accounts/d;)Lio/reactivex/c/g;

    move-result-object v1

    .line 151
    invoke-virtual {v0, v1}, Lio/reactivex/v;->c(Lio/reactivex/c/g;)Lio/reactivex/v;

    move-result-object v0

    invoke-static {p0}, Lco/uk/getmondo/api/b/ac;->a(Lco/uk/getmondo/api/b/a;)Lio/reactivex/c/h;

    move-result-object v1

    .line 152
    invoke-virtual {v0, v1}, Lio/reactivex/v;->d(Lio/reactivex/c/h;)Lio/reactivex/v;

    move-result-object v0

    invoke-static {p0}, Lco/uk/getmondo/api/b/ad;->a(Lco/uk/getmondo/api/b/a;)Lio/reactivex/c/g;

    move-result-object v1

    .line 154
    invoke-virtual {v0, v1}, Lio/reactivex/v;->d(Lio/reactivex/c/g;)Lio/reactivex/v;

    move-result-object v0

    invoke-static {p0}, Lco/uk/getmondo/api/b/ae;->a(Lco/uk/getmondo/api/b/a;)Lio/reactivex/c/g;

    move-result-object v1

    .line 155
    invoke-virtual {v0, v1}, Lio/reactivex/v;->c(Lio/reactivex/c/g;)Lio/reactivex/v;

    move-result-object v0

    .line 138
    return-object v0
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;)Lio/reactivex/v;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/v",
            "<",
            "Lco/uk/getmondo/d/ak;",
            ">;"
        }
    .end annotation

    .prologue
    .line 122
    invoke-static {p0, p2, p1}, Lco/uk/getmondo/api/b/x;->a(Lco/uk/getmondo/api/b/a;Ljava/lang/String;Ljava/lang/String;)Ljava/util/concurrent/Callable;

    move-result-object v0

    invoke-static {v0}, Lio/reactivex/v;->a(Ljava/util/concurrent/Callable;)Lio/reactivex/v;

    move-result-object v0

    return-object v0
.end method

.method public b(Ljava/lang/String;)Lio/reactivex/b;
    .locals 3

    .prologue
    .line 243
    iget-object v0, p0, Lco/uk/getmondo/api/b/a;->d:Lco/uk/getmondo/api/authentication/MonzoOAuthApi;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Bearer "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lco/uk/getmondo/api/authentication/MonzoOAuthApi;->logOut(Ljava/lang/String;)Lio/reactivex/b;

    move-result-object v0

    invoke-static {p0}, Lco/uk/getmondo/api/b/t;->a(Lco/uk/getmondo/api/b/a;)Lio/reactivex/c/g;

    move-result-object v1

    .line 244
    invoke-virtual {v0, v1}, Lio/reactivex/b;->a(Lio/reactivex/c/g;)Lio/reactivex/b;

    move-result-object v0

    .line 243
    return-object v0
.end method

.method public b()Lio/reactivex/v;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/v",
            "<",
            "Lco/uk/getmondo/d/an;",
            ">;"
        }
    .end annotation

    .prologue
    .line 237
    iget-object v0, p0, Lco/uk/getmondo/api/b/a;->c:Lco/uk/getmondo/api/MonzoApi;

    invoke-interface {v0}, Lco/uk/getmondo/api/MonzoApi;->waitlist()Lio/reactivex/v;

    move-result-object v0

    new-instance v1, Lco/uk/getmondo/d/a/v;

    invoke-direct {v1}, Lco/uk/getmondo/d/a/v;-><init>()V

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    invoke-static {v1}, Lco/uk/getmondo/api/b/r;->a(Lco/uk/getmondo/d/a/v;)Lio/reactivex/c/h;

    move-result-object v1

    .line 238
    invoke-virtual {v0, v1}, Lio/reactivex/v;->d(Lio/reactivex/c/h;)Lio/reactivex/v;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/api/b/a;->b:Lco/uk/getmondo/common/accounts/d;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    invoke-static {v1}, Lco/uk/getmondo/api/b/s;->a(Lco/uk/getmondo/common/accounts/d;)Lio/reactivex/c/g;

    move-result-object v1

    .line 239
    invoke-virtual {v0, v1}, Lio/reactivex/v;->c(Lio/reactivex/c/g;)Lio/reactivex/v;

    move-result-object v0

    .line 237
    return-object v0
.end method
