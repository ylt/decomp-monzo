.class public interface abstract Lco/uk/getmondo/api/TaxResidencyApi;
.super Ljava/lang/Object;
.source "TaxResidencyApi.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u00002\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0010\u0011\n\u0002\u0008\u0005\u0008f\u0018\u00002\u00020\u0001J\u0012\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0001\u0010\u0004\u001a\u00020\u0005H\'J\u000e\u0010\u0006\u001a\u0008\u0012\u0004\u0012\u00020\u00080\u0007H\'J\u0008\u0010\t\u001a\u00020\u0003H\'J1\u0010\n\u001a\u00020\u00032\u0008\u0008\u0001\u0010\u000b\u001a\u00020\u000c2\u0008\u0008\u0001\u0010\r\u001a\u00020\u000c2\u000e\u0008\u0001\u0010\u000e\u001a\u0008\u0012\u0004\u0012\u00020\u00050\u000fH\'\u00a2\u0006\u0002\u0010\u0010J&\u0010\u0011\u001a\u00020\u00032\u0008\u0008\u0001\u0010\u0004\u001a\u00020\u00052\u0008\u0008\u0001\u0010\u0012\u001a\u00020\u00052\u0008\u0008\u0001\u0010\u0013\u001a\u00020\u0005H\'\u00a8\u0006\u0014"
    }
    d2 = {
        "Lco/uk/getmondo/api/TaxResidencyApi;",
        "",
        "noTin",
        "Lio/reactivex/Completable;",
        "countryCode",
        "",
        "status",
        "Lio/reactivex/Single;",
        "Lco/uk/getmondo/api/model/tax_residency/TaxResidencyInfo;",
        "submit",
        "updateSelfCertification",
        "summaryScreen",
        "",
        "usCitizen",
        "countryCodes",
        "",
        "(ZZ[Ljava/lang/String;)Lio/reactivex/Completable;",
        "updateTin",
        "type",
        "value",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# virtual methods
.method public abstract noTin(Ljava/lang/String;)Lio/reactivex/b;
    .param p1    # Ljava/lang/String;
        .annotation runtime Lretrofit2/http/Field;
            value = "country_code"
        .end annotation
    .end param
    .annotation runtime Lretrofit2/http/FormUrlEncoded;
    .end annotation

    .annotation runtime Lretrofit2/http/POST;
        value = "tax-residency/no-tin"
    .end annotation
.end method

.method public abstract status()Lio/reactivex/v;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/v",
            "<",
            "Lco/uk/getmondo/api/model/tax_residency/TaxResidencyInfo;",
            ">;"
        }
    .end annotation

    .annotation runtime Lretrofit2/http/GET;
        value = "tax-residency/status"
    .end annotation
.end method

.method public abstract submit()Lio/reactivex/b;
    .annotation runtime Lretrofit2/http/POST;
        value = "tax-residency/submit"
    .end annotation
.end method

.method public abstract updateSelfCertification(ZZ[Ljava/lang/String;)Lio/reactivex/b;
    .param p1    # Z
        .annotation runtime Lretrofit2/http/Field;
            value = "from_summary_screen"
        .end annotation
    .end param
    .param p2    # Z
        .annotation runtime Lretrofit2/http/Field;
            value = "us_citizen"
        .end annotation
    .end param
    .param p3    # [Ljava/lang/String;
        .annotation runtime Lretrofit2/http/Field;
            value = "country_codes[]"
        .end annotation
    .end param
    .annotation runtime Lretrofit2/http/FormUrlEncoded;
    .end annotation

    .annotation runtime Lretrofit2/http/POST;
        value = "tax-residency/update-self-certification"
    .end annotation
.end method

.method public abstract updateTin(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lio/reactivex/b;
    .param p1    # Ljava/lang/String;
        .annotation runtime Lretrofit2/http/Field;
            value = "country_code"
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation runtime Lretrofit2/http/Field;
            value = "type"
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation runtime Lretrofit2/http/Field;
            value = "value"
        .end annotation
    .end param
    .annotation runtime Lretrofit2/http/FormUrlEncoded;
    .end annotation

    .annotation runtime Lretrofit2/http/POST;
        value = "tax-residency/update-tin"
    .end annotation
.end method
