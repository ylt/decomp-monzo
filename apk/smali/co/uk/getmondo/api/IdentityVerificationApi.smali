.class public interface abstract Lco/uk/getmondo/api/IdentityVerificationApi;
.super Ljava/lang/Object;
.source "IdentityVerificationApi.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lco/uk/getmondo/api/IdentityVerificationApi$DefaultImpls;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000\\\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0002\u0008\t\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008f\u0018\u00002\u00020\u0001J\"\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u00032\u0008\u0008\u0001\u0010\u0005\u001a\u00020\u00062\u0008\u0008\u0001\u0010\u0007\u001a\u00020\u0006H\'J\u000e\u0010\u0008\u001a\u0008\u0012\u0004\u0012\u00020\t0\u0003H\'JH\u0010\n\u001a\u00020\u000b2\u0008\u0008\u0001\u0010\u000c\u001a\u00020\r2\n\u0008\u0001\u0010\u000e\u001a\u0004\u0018\u00010\u000f2\u0008\u0008\u0001\u0010\u0010\u001a\u00020\u00062\u0008\u0008\u0001\u0010\u0011\u001a\u00020\u00122\u0008\u0008\u0001\u0010\u0013\u001a\u00020\u00062\n\u0008\u0003\u0010\u0014\u001a\u0004\u0018\u00010\u0006H\'J&\u0010\u0015\u001a\u00020\u000b2\u0008\u0008\u0001\u0010\u0016\u001a\u00020\u00062\u0008\u0008\u0001\u0010\u0017\u001a\u00020\u00062\u0008\u0008\u0001\u0010\u0018\u001a\u00020\u0006H\'J&\u0010\u0019\u001a\u00020\u000b2\u0008\u0008\u0001\u0010\u000c\u001a\u00020\r2\u0008\u0008\u0001\u0010\u001a\u001a\u00020\u00062\u0008\u0008\u0001\u0010\u0011\u001a\u00020\u0012H\'J@\u0010\u001b\u001a\u0008\u0012\u0004\u0012\u00020\u001c0\u00032\u0008\u0008\u0001\u0010\u000c\u001a\u00020\r2\u0008\u0008\u0001\u0010\u000e\u001a\u00020\u001d2\u0008\u0008\u0001\u0010\u0005\u001a\u00020\u00062\u0008\u0008\u0001\u0010\u001e\u001a\u00020\u001f2\u0008\u0008\u0001\u0010 \u001a\u00020!H\'J\u0018\u0010\"\u001a\u0008\u0012\u0004\u0012\u00020#0\u00032\u0008\u0008\u0001\u0010\u000c\u001a\u00020\rH\'J\u0012\u0010$\u001a\u00020\u000b2\u0008\u0008\u0001\u0010\u000c\u001a\u00020\rH\'\u00a8\u0006%"
    }
    d2 = {
        "Lco/uk/getmondo/api/IdentityVerificationApi;",
        "",
        "createKYCUploadUrl",
        "Lio/reactivex/Single;",
        "Lco/uk/getmondo/api/model/identity_verification/ApiUploadContainer;",
        "fileName",
        "",
        "fileType",
        "kycStatus",
        "Lco/uk/getmondo/api/model/identity_verification/LegacyKyc;",
        "registerIdentityDocument",
        "Lio/reactivex/Completable;",
        "source",
        "Lco/uk/getmondo/api/model/signup/SignupSource;",
        "type",
        "Lco/uk/getmondo/api/model/identity_verification/IdentityDocumentType;",
        "country",
        "systemCameraUsed",
        "",
        "primaryFileId",
        "secondaryFileId",
        "registerKYCUrl",
        "fileUrl",
        "groupId",
        "evidenceType",
        "registerSelfieVideo",
        "fileId",
        "requestFileUpload",
        "Lco/uk/getmondo/api/model/identity_verification/FileUpload;",
        "Lco/uk/getmondo/api/model/identity_verification/FileType;",
        "contentType",
        "Lco/uk/getmondo/api/model/identity_verification/ContentType;",
        "contentLength",
        "",
        "status",
        "Lco/uk/getmondo/api/model/identity_verification/IdentityVerification;",
        "submit",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# virtual methods
.method public abstract createKYCUploadUrl(Ljava/lang/String;Ljava/lang/String;)Lio/reactivex/v;
    .param p1    # Ljava/lang/String;
        .annotation runtime Lretrofit2/http/Field;
            value = "file_name"
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation runtime Lretrofit2/http/Field;
            value = "file_type"
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/v",
            "<",
            "Lco/uk/getmondo/api/model/identity_verification/ApiUploadContainer;",
            ">;"
        }
    .end annotation

    .annotation runtime Lretrofit2/http/FormUrlEncoded;
    .end annotation

    .annotation runtime Lretrofit2/http/POST;
        value = "kyc/upload"
    .end annotation
.end method

.method public abstract kycStatus()Lio/reactivex/v;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/v",
            "<",
            "Lco/uk/getmondo/api/model/identity_verification/LegacyKyc;",
            ">;"
        }
    .end annotation

    .annotation runtime Lretrofit2/http/GET;
        value = "kyc/status"
    .end annotation
.end method

.method public abstract registerIdentityDocument(Lco/uk/getmondo/api/model/signup/SignupSource;Lco/uk/getmondo/api/model/identity_verification/IdentityDocumentType;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;)Lio/reactivex/b;
    .param p1    # Lco/uk/getmondo/api/model/signup/SignupSource;
        .annotation runtime Lretrofit2/http/Query;
            value = "source"
        .end annotation
    .end param
    .param p2    # Lco/uk/getmondo/api/model/identity_verification/IdentityDocumentType;
        .annotation runtime Lretrofit2/http/Field;
            value = "type"
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation runtime Lretrofit2/http/Field;
            value = "issuing_country"
        .end annotation
    .end param
    .param p4    # Z
        .annotation runtime Lretrofit2/http/Field;
            value = "system_camera_used"
        .end annotation
    .end param
    .param p5    # Ljava/lang/String;
        .annotation runtime Lretrofit2/http/Field;
            value = "primary_image_file_id"
        .end annotation
    .end param
    .param p6    # Ljava/lang/String;
        .annotation runtime Lretrofit2/http/Field;
            value = "secondary_image_file_id"
        .end annotation
    .end param
    .annotation runtime Lretrofit2/http/FormUrlEncoded;
    .end annotation

    .annotation runtime Lretrofit2/http/POST;
        value = "identity-verification/register-identity-document"
    .end annotation
.end method

.method public abstract registerKYCUrl(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lio/reactivex/b;
    .param p1    # Ljava/lang/String;
        .annotation runtime Lretrofit2/http/Field;
            value = "file_url"
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation runtime Lretrofit2/http/Field;
            value = "group_id"
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation runtime Lretrofit2/http/Field;
            value = "evidence_type"
        .end annotation
    .end param
    .annotation runtime Lretrofit2/http/FormUrlEncoded;
    .end annotation

    .annotation runtime Lretrofit2/http/POST;
        value = "kyc/register"
    .end annotation
.end method

.method public abstract registerSelfieVideo(Lco/uk/getmondo/api/model/signup/SignupSource;Ljava/lang/String;Z)Lio/reactivex/b;
    .param p1    # Lco/uk/getmondo/api/model/signup/SignupSource;
        .annotation runtime Lretrofit2/http/Query;
            value = "source"
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation runtime Lretrofit2/http/Field;
            value = "file_id"
        .end annotation
    .end param
    .param p3    # Z
        .annotation runtime Lretrofit2/http/Field;
            value = "system_camera_used"
        .end annotation
    .end param
    .annotation runtime Lretrofit2/http/FormUrlEncoded;
    .end annotation

    .annotation runtime Lretrofit2/http/POST;
        value = "identity-verification/register-selfie-video"
    .end annotation
.end method

.method public abstract requestFileUpload(Lco/uk/getmondo/api/model/signup/SignupSource;Lco/uk/getmondo/api/model/identity_verification/FileType;Ljava/lang/String;Lco/uk/getmondo/api/model/identity_verification/ContentType;J)Lio/reactivex/v;
    .param p1    # Lco/uk/getmondo/api/model/signup/SignupSource;
        .annotation runtime Lretrofit2/http/Query;
            value = "source"
        .end annotation
    .end param
    .param p2    # Lco/uk/getmondo/api/model/identity_verification/FileType;
        .annotation runtime Lretrofit2/http/Field;
            value = "type"
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation runtime Lretrofit2/http/Field;
            value = "file_name"
        .end annotation
    .end param
    .param p4    # Lco/uk/getmondo/api/model/identity_verification/ContentType;
        .annotation runtime Lretrofit2/http/Field;
            value = "content_type"
        .end annotation
    .end param
    .param p5    # J
        .annotation runtime Lretrofit2/http/Field;
            value = "content_length"
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lco/uk/getmondo/api/model/signup/SignupSource;",
            "Lco/uk/getmondo/api/model/identity_verification/FileType;",
            "Ljava/lang/String;",
            "Lco/uk/getmondo/api/model/identity_verification/ContentType;",
            "J)",
            "Lio/reactivex/v",
            "<",
            "Lco/uk/getmondo/api/model/identity_verification/FileUpload;",
            ">;"
        }
    .end annotation

    .annotation runtime Lretrofit2/http/FormUrlEncoded;
    .end annotation

    .annotation runtime Lretrofit2/http/POST;
        value = "identity-verification/request-file-upload"
    .end annotation
.end method

.method public abstract status(Lco/uk/getmondo/api/model/signup/SignupSource;)Lio/reactivex/v;
    .param p1    # Lco/uk/getmondo/api/model/signup/SignupSource;
        .annotation runtime Lretrofit2/http/Query;
            value = "source"
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lco/uk/getmondo/api/model/signup/SignupSource;",
            ")",
            "Lio/reactivex/v",
            "<",
            "Lco/uk/getmondo/api/model/identity_verification/IdentityVerification;",
            ">;"
        }
    .end annotation

    .annotation runtime Lretrofit2/http/GET;
        value = "identity-verification/status"
    .end annotation
.end method

.method public abstract submit(Lco/uk/getmondo/api/model/signup/SignupSource;)Lio/reactivex/b;
    .param p1    # Lco/uk/getmondo/api/model/signup/SignupSource;
        .annotation runtime Lretrofit2/http/Query;
            value = "source"
        .end annotation
    .end param
    .annotation runtime Lretrofit2/http/POST;
        value = "identity-verification/submit"
    .end annotation
.end method
