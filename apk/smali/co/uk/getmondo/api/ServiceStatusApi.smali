.class public interface abstract Lco/uk/getmondo/api/ServiceStatusApi;
.super Ljava/lang/Object;
.source "ServiceStatusApi.java"


# virtual methods
.method public abstract getUnresolvedIncidents()Lio/reactivex/v;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/v",
            "<",
            "Lco/uk/getmondo/api/model/service_status/ServiceStatus;",
            ">;"
        }
    .end annotation

    .annotation runtime Lretrofit2/http/GET;
        value = "api/v2/incidents/unresolved.json"
    .end annotation
.end method
