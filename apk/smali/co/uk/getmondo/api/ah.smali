.class Lco/uk/getmondo/api/ah;
.super Lokhttp3/RequestBody;
.source "ImageInputStreamRequestBody.java"


# instance fields
.field private final a:Ljava/io/InputStream;

.field private final b:J

.field private final c:Ljava/lang/String;


# direct methods
.method constructor <init>(Ljava/io/InputStream;JLjava/lang/String;)V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Lokhttp3/RequestBody;-><init>()V

    .line 20
    iput-object p1, p0, Lco/uk/getmondo/api/ah;->a:Ljava/io/InputStream;

    .line 21
    iput-wide p2, p0, Lco/uk/getmondo/api/ah;->b:J

    .line 22
    iput-object p4, p0, Lco/uk/getmondo/api/ah;->c:Ljava/lang/String;

    .line 23
    return-void
.end method


# virtual methods
.method public contentLength()J
    .locals 2

    .prologue
    .line 32
    iget-wide v0, p0, Lco/uk/getmondo/api/ah;->b:J

    return-wide v0
.end method

.method public contentType()Lokhttp3/MediaType;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lco/uk/getmondo/api/ah;->c:Ljava/lang/String;

    invoke-static {v0}, Lokhttp3/MediaType;->parse(Ljava/lang/String;)Lokhttp3/MediaType;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lc/d;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 37
    const/4 v1, 0x0

    .line 39
    :try_start_0
    iget-object v0, p0, Lco/uk/getmondo/api/ah;->a:Ljava/io/InputStream;

    invoke-static {v0}, Lc/l;->a(Ljava/io/InputStream;)Lc/t;

    move-result-object v1

    .line 40
    invoke-interface {p1, v1}, Lc/d;->a(Lc/t;)J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 42
    invoke-static {v1}, Lokhttp3/internal/Util;->closeQuietly(Ljava/io/Closeable;)V

    .line 44
    return-void

    .line 42
    :catchall_0
    move-exception v0

    invoke-static {v1}, Lokhttp3/internal/Util;->closeQuietly(Ljava/io/Closeable;)V

    throw v0
.end method
