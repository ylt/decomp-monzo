.class public final Lco/uk/getmondo/api/s;
.super Ljava/lang/Object;
.source "ApiModule_ProvideOverdraftApi$app_monzoPrepaidReleaseFactory.java"

# interfaces
.implements Lb/a/b;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lb/a/b",
        "<",
        "Lco/uk/getmondo/api/OverdraftApi;",
        ">;"
    }
.end annotation


# static fields
.field static final synthetic a:Z


# instance fields
.field private final b:Lco/uk/getmondo/api/c;

.field private final c:Ljavax/a/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/a/a",
            "<",
            "Lokhttp3/OkHttpClient;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Ljavax/a/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/a/a",
            "<",
            "Lcom/squareup/moshi/v;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Ljavax/a/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/a/a",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 10
    const-class v0, Lco/uk/getmondo/api/s;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lco/uk/getmondo/api/s;->a:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lco/uk/getmondo/api/c;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lco/uk/getmondo/api/c;",
            "Ljavax/a/a",
            "<",
            "Lokhttp3/OkHttpClient;",
            ">;",
            "Ljavax/a/a",
            "<",
            "Lcom/squareup/moshi/v;",
            ">;",
            "Ljavax/a/a",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    sget-boolean v0, Lco/uk/getmondo/api/s;->a:Z

    if-nez v0, :cond_0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 30
    :cond_0
    iput-object p1, p0, Lco/uk/getmondo/api/s;->b:Lco/uk/getmondo/api/c;

    .line 31
    sget-boolean v0, Lco/uk/getmondo/api/s;->a:Z

    if-nez v0, :cond_1

    if-nez p2, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 32
    :cond_1
    iput-object p2, p0, Lco/uk/getmondo/api/s;->c:Ljavax/a/a;

    .line 33
    sget-boolean v0, Lco/uk/getmondo/api/s;->a:Z

    if-nez v0, :cond_2

    if-nez p3, :cond_2

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 34
    :cond_2
    iput-object p3, p0, Lco/uk/getmondo/api/s;->d:Ljavax/a/a;

    .line 35
    sget-boolean v0, Lco/uk/getmondo/api/s;->a:Z

    if-nez v0, :cond_3

    if-nez p4, :cond_3

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 36
    :cond_3
    iput-object p4, p0, Lco/uk/getmondo/api/s;->e:Ljavax/a/a;

    .line 37
    return-void
.end method

.method public static a(Lco/uk/getmondo/api/c;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;)Lb/a/b;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lco/uk/getmondo/api/c;",
            "Ljavax/a/a",
            "<",
            "Lokhttp3/OkHttpClient;",
            ">;",
            "Ljavax/a/a",
            "<",
            "Lcom/squareup/moshi/v;",
            ">;",
            "Ljavax/a/a",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lb/a/b",
            "<",
            "Lco/uk/getmondo/api/OverdraftApi;",
            ">;"
        }
    .end annotation

    .prologue
    .line 52
    new-instance v0, Lco/uk/getmondo/api/s;

    invoke-direct {v0, p0, p1, p2, p3}, Lco/uk/getmondo/api/s;-><init>(Lco/uk/getmondo/api/c;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;)V

    return-object v0
.end method


# virtual methods
.method public a()Lco/uk/getmondo/api/OverdraftApi;
    .locals 4

    .prologue
    .line 41
    iget-object v3, p0, Lco/uk/getmondo/api/s;->b:Lco/uk/getmondo/api/c;

    iget-object v0, p0, Lco/uk/getmondo/api/s;->c:Ljavax/a/a;

    .line 43
    invoke-interface {v0}, Ljavax/a/a;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lokhttp3/OkHttpClient;

    iget-object v1, p0, Lco/uk/getmondo/api/s;->d:Ljavax/a/a;

    invoke-interface {v1}, Ljavax/a/a;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/moshi/v;

    iget-object v2, p0, Lco/uk/getmondo/api/s;->e:Ljavax/a/a;

    invoke-interface {v2}, Ljavax/a/a;->b()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 42
    invoke-virtual {v3, v0, v1, v2}, Lco/uk/getmondo/api/c;->f(Lokhttp3/OkHttpClient;Lcom/squareup/moshi/v;Ljava/lang/String;)Lco/uk/getmondo/api/OverdraftApi;

    move-result-object v0

    const-string v1, "Cannot return null from a non-@Nullable @Provides method"

    .line 41
    invoke-static {v0, v1}, Lb/a/d;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/api/OverdraftApi;

    return-object v0
.end method

.method public synthetic b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 10
    invoke-virtual {p0}, Lco/uk/getmondo/api/s;->a()Lco/uk/getmondo/api/OverdraftApi;

    move-result-object v0

    return-object v0
.end method
