.class public final Lco/uk/getmondo/api/IdentityVerificationApi$DefaultImpls;
.super Ljava/lang/Object;
.source "IdentityVerificationApi.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lco/uk/getmondo/api/IdentityVerificationApi;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "DefaultImpls"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# direct methods
.method public static synthetic registerIdentityDocument$default(Lco/uk/getmondo/api/IdentityVerificationApi;Lco/uk/getmondo/api/model/signup/SignupSource;Lco/uk/getmondo/api/model/identity_verification/IdentityDocumentType;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;ILjava/lang/Object;)Lio/reactivex/b;
    .locals 7
    .annotation runtime Lretrofit2/http/FormUrlEncoded;
    .end annotation

    .annotation runtime Lretrofit2/http/POST;
        value = "identity-verification/register-identity-document"
    .end annotation

    .prologue
    if-eqz p8, :cond_0

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Super calls with default arguments not supported in this target, function: registerIdentityDocument"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    and-int/lit8 v0, p7, 0x20

    if-eqz v0, :cond_1

    .line 36
    const/4 v0, 0x0

    check-cast v0, Ljava/lang/String;

    move-object v6, v0

    :goto_0
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    move-object v5, p5

    invoke-interface/range {v0 .. v6}, Lco/uk/getmondo/api/IdentityVerificationApi;->registerIdentityDocument(Lco/uk/getmondo/api/model/signup/SignupSource;Lco/uk/getmondo/api/model/identity_verification/IdentityDocumentType;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;)Lio/reactivex/b;

    move-result-object v0

    return-object v0

    :cond_1
    move-object v6, p6

    goto :goto_0
.end method
