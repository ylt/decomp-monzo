.class public Lco/uk/getmondo/api/ac;
.super Ljava/lang/Object;
.source "ErrorHandlerInterceptor.java"

# interfaces
.implements Lokhttp3/Interceptor;


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Lcom/google/gson/f;

.field private c:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/gson/f;)V
    .locals 1

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    const/4 v0, 0x0

    iput-boolean v0, p0, Lco/uk/getmondo/api/ac;->c:Z

    .line 32
    iput-object p1, p0, Lco/uk/getmondo/api/ac;->a:Landroid/content/Context;

    .line 33
    iput-object p2, p0, Lco/uk/getmondo/api/ac;->b:Lcom/google/gson/f;

    .line 34
    return-void
.end method


# virtual methods
.method a(Z)V
    .locals 0

    .prologue
    .line 41
    iput-boolean p1, p0, Lco/uk/getmondo/api/ac;->c:Z

    .line 42
    return-void
.end method

.method public intercept(Lokhttp3/Interceptor$Chain;)Lokhttp3/Response;
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 46
    invoke-interface {p1}, Lokhttp3/Interceptor$Chain;->request()Lokhttp3/Request;

    move-result-object v0

    .line 47
    invoke-interface {p1, v0}, Lokhttp3/Interceptor$Chain;->proceed(Lokhttp3/Request;)Lokhttp3/Response;

    move-result-object v3

    .line 48
    invoke-virtual {v3}, Lokhttp3/Response;->code()I

    move-result v4

    .line 49
    invoke-virtual {v3}, Lokhttp3/Response;->isSuccessful()Z

    move-result v0

    if-nez v0, :cond_5

    .line 53
    const-wide/32 v0, 0x100000

    :try_start_0
    invoke-virtual {v3, v0, v1}, Lokhttp3/Response;->peekBody(J)Lokhttp3/ResponseBody;
    :try_end_0
    .catch Lcom/google/gson/JsonIOException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Lcom/google/gson/JsonSyntaxException; {:try_start_0 .. :try_end_0} :catch_3

    move-result-object v5

    .line 54
    :try_start_1
    iget-object v0, p0, Lco/uk/getmondo/api/ac;->b:Lcom/google/gson/f;

    invoke-virtual {v5}, Lokhttp3/ResponseBody;->charStream()Ljava/io/Reader;

    move-result-object v1

    const-class v6, Lco/uk/getmondo/api/model/b;

    invoke-virtual {v0, v1, v6}, Lcom/google/gson/f;->a(Ljava/io/Reader;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/api/model/b;
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 55
    if-eqz v5, :cond_0

    if-eqz v2, :cond_2

    :try_start_2
    invoke-virtual {v5}, Lokhttp3/ResponseBody;->close()V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_4
    .catch Lcom/google/gson/JsonIOException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Lcom/google/gson/JsonSyntaxException; {:try_start_2 .. :try_end_2} :catch_6

    .line 59
    :cond_0
    :goto_0
    const-string v1, "Trace-Id"

    invoke-virtual {v3, v1}, Lokhttp3/Response;->header(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 60
    invoke-virtual {v3}, Lokhttp3/Response;->request()Lokhttp3/Request;

    move-result-object v2

    invoke-virtual {v2}, Lokhttp3/Request;->url()Lokhttp3/HttpUrl;

    move-result-object v2

    invoke-virtual {v2}, Lokhttp3/HttpUrl;->encodedPath()Ljava/lang/String;

    move-result-object v2

    .line 62
    iget-boolean v3, p0, Lco/uk/getmondo/api/ac;->c:Z

    if-eqz v3, :cond_1

    .line 67
    iget-object v3, p0, Lco/uk/getmondo/api/ac;->a:Landroid/content/Context;

    invoke-static {v3}, Lco/uk/getmondo/MonzoApplication;->a(Landroid/content/Context;)Lco/uk/getmondo/MonzoApplication;

    move-result-object v3

    invoke-virtual {v3}, Lco/uk/getmondo/MonzoApplication;->b()Lco/uk/getmondo/common/h/a/a;

    move-result-object v3

    invoke-interface {v3}, Lco/uk/getmondo/common/h/a/a;->h()Lco/uk/getmondo/common/a;

    move-result-object v3

    .line 68
    invoke-static {v4, v2, v1, v0}, Lco/uk/getmondo/api/model/tracking/Impression;->a(ILjava/lang/String;Ljava/lang/String;Lco/uk/getmondo/api/model/b;)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v5

    invoke-virtual {v3, v5}, Lco/uk/getmondo/common/a;->a(Lco/uk/getmondo/api/model/tracking/Impression;)V

    .line 71
    :cond_1
    new-instance v3, Lco/uk/getmondo/api/ApiException;

    invoke-direct {v3, v4, v2, v1, v0}, Lco/uk/getmondo/api/ApiException;-><init>(ILjava/lang/String;Ljava/lang/String;Lco/uk/getmondo/api/model/b;)V

    throw v3

    .line 55
    :cond_2
    :try_start_3
    invoke-virtual {v5}, Lokhttp3/ResponseBody;->close()V
    :try_end_3
    .catch Lcom/google/gson/JsonIOException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Lcom/google/gson/JsonSyntaxException; {:try_start_3 .. :try_end_3} :catch_6

    goto :goto_0

    :catch_0
    move-exception v1

    move-object v7, v1

    move-object v1, v0

    move-object v0, v7

    .line 56
    :goto_1
    const-string v2, "Could not parse body of http error response"

    const/4 v5, 0x0

    new-array v5, v5, [Ljava/lang/Object;

    invoke-static {v0, v2, v5}, Ld/a/a;->a(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    move-object v0, v1

    goto :goto_0

    .line 53
    :catch_1
    move-exception v0

    :try_start_4
    throw v0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 55
    :catchall_0
    move-exception v1

    move-object v7, v1

    move-object v1, v0

    move-object v0, v7

    :goto_2
    if-eqz v5, :cond_3

    if-eqz v1, :cond_4

    :try_start_5
    invoke-virtual {v5}, Lokhttp3/ResponseBody;->close()V
    :try_end_5
    .catch Ljava/lang/Throwable; {:try_start_5 .. :try_end_5} :catch_5
    .catch Lcom/google/gson/JsonIOException; {:try_start_5 .. :try_end_5} :catch_2
    .catch Lcom/google/gson/JsonSyntaxException; {:try_start_5 .. :try_end_5} :catch_3

    :cond_3
    :goto_3
    :try_start_6
    throw v0

    :catch_2
    move-exception v0

    move-object v1, v2

    goto :goto_1

    :cond_4
    invoke-virtual {v5}, Lokhttp3/ResponseBody;->close()V
    :try_end_6
    .catch Lcom/google/gson/JsonIOException; {:try_start_6 .. :try_end_6} :catch_2
    .catch Lcom/google/gson/JsonSyntaxException; {:try_start_6 .. :try_end_6} :catch_3

    goto :goto_3

    :catch_3
    move-exception v0

    move-object v1, v2

    goto :goto_1

    :catch_4
    move-exception v1

    goto :goto_0

    :catch_5
    move-exception v1

    goto :goto_3

    .line 74
    :cond_5
    return-object v3

    .line 55
    :catch_6
    move-exception v1

    move-object v7, v1

    move-object v1, v0

    move-object v0, v7

    goto :goto_1

    :catchall_1
    move-exception v0

    move-object v1, v2

    goto :goto_2
.end method
