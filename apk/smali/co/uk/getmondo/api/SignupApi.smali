.class public interface abstract Lco/uk/getmondo/api/SignupApi;
.super Ljava/lang/Object;
.source "SignupApi.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lco/uk/getmondo/api/SignupApi$DefaultImpls;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000V\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0007\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0006\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0008\u0006\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010 \n\u0002\u0008\u0007\u0008f\u0018\u00002\u00020\u0001JD\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0001\u0010\u0004\u001a\u00020\u00052\u0008\u0008\u0001\u0010\u0006\u001a\u00020\u00072\u0008\u0008\u0001\u0010\u0008\u001a\u00020\u00052\u0008\u0008\u0001\u0010\t\u001a\u00020\u00072\u0008\u0008\u0001\u0010\n\u001a\u00020\u00052\u0008\u0008\u0001\u0010\u000b\u001a\u00020\u0007H\'J\u0012\u0010\u000c\u001a\u00020\u00032\u0008\u0008\u0001\u0010\r\u001a\u00020\u0007H\'J\u000e\u0010\u000e\u001a\u0008\u0012\u0004\u0012\u00020\u00100\u000fH\'J\u0008\u0010\u0011\u001a\u00020\u0003H\'J&\u0010\u0012\u001a\u00020\u00032\u0008\u0008\u0001\u0010\u0013\u001a\u00020\u00072\u0008\u0008\u0001\u0010\u0014\u001a\u00020\u00072\u0008\u0008\u0001\u0010\u0015\u001a\u00020\u0007H\'J\u000e\u0010\u0016\u001a\u0008\u0012\u0004\u0012\u00020\u00170\u000fH\'J\u000e\u0010\u0018\u001a\u0008\u0012\u0004\u0012\u00020\u00190\u000fH\'J\"\u0010\u001a\u001a\u0008\u0012\u0004\u0012\u00020\u001b0\u000f2\u0008\u0008\u0001\u0010\u001c\u001a\u00020\u00072\u0008\u0008\u0001\u0010\u001d\u001a\u00020\u0007H\'J\u0012\u0010\u001e\u001a\u00020\u00032\u0008\u0008\u0001\u0010\u001f\u001a\u00020\u0007H\'J\u000e\u0010 \u001a\u0008\u0012\u0004\u0012\u00020!0\u000fH\'J\u0008\u0010\"\u001a\u00020\u0003H\'J\u0008\u0010#\u001a\u00020\u0003H\'J(\u0010$\u001a\u00020\u00032\u0008\u0008\u0001\u0010%\u001a\u00020\u00072\n\u0008\u0003\u0010&\u001a\u0004\u0018\u00010\u00072\u0008\u0008\u0001\u0010\'\u001a\u00020(H\'JB\u0010)\u001a\u00020\u00032\n\u0008\u0003\u0010*\u001a\u0004\u0018\u00010\u00072\u000e\u0008\u0001\u0010+\u001a\u0008\u0012\u0004\u0012\u00020\u00070,2\u0008\u0008\u0001\u0010-\u001a\u00020\u00072\u0008\u0008\u0001\u0010\u001d\u001a\u00020\u00072\u0008\u0008\u0001\u0010\u001c\u001a\u00020\u0007H\'J\u001c\u0010.\u001a\u00020\u00032\u0008\u0008\u0001\u0010/\u001a\u00020\u00052\u0008\u0008\u0001\u00100\u001a\u00020\u0005H\'J\u001c\u00101\u001a\u00020\u00032\u0008\u0008\u0001\u0010\u001f\u001a\u00020\u00072\u0008\u0008\u0001\u00102\u001a\u00020\u0007H\'\u00a8\u00063"
    }
    d2 = {
        "Lco/uk/getmondo/api/SignupApi;",
        "",
        "acceptLegalDocuments",
        "Lio/reactivex/Completable;",
        "termsAndConditionsAccepted",
        "",
        "termsAndConditionsVersion",
        "",
        "privacyPolicyAccepted",
        "privacyPolicyVersion",
        "fscsInformationSheetAccepted",
        "fscsInformationSheetVersion",
        "activateCard",
        "pan",
        "cardOrderOptions",
        "Lio/reactivex/Single;",
        "Lco/uk/getmondo/api/model/order_card/CardOrderOptions;",
        "commitProfile",
        "createCardOrder",
        "nameType",
        "pinType",
        "pin",
        "legalDocuments",
        "Lco/uk/getmondo/api/model/signup/SignUpDocumentUrls;",
        "profile",
        "Lco/uk/getmondo/api/model/signup/SignUpProfile;",
        "searchAddress",
        "Lco/uk/getmondo/api/model/AddressesReponse;",
        "countryCode",
        "postalCode",
        "sendPhoneNumber",
        "phoneNumber",
        "signupStatus",
        "Lco/uk/getmondo/api/model/signup/SignupInfo;",
        "skipFingerprintEnrolment",
        "startSignUp",
        "submitProfile",
        "legalName",
        "preferredName",
        "dateOfBirth",
        "Lorg/threeten/bp/LocalDate;",
        "submitProfileAddress",
        "addressId",
        "addressLines",
        "",
        "administrativeArea",
        "subscribeToMarketing",
        "optIn",
        "optOut",
        "verifyPhoneNumber",
        "code",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# virtual methods
.method public abstract acceptLegalDocuments(ZLjava/lang/String;ZLjava/lang/String;ZLjava/lang/String;)Lio/reactivex/b;
    .param p1    # Z
        .annotation runtime Lretrofit2/http/Field;
            value = "terms_and_conditions_accepted"
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation runtime Lretrofit2/http/Field;
            value = "terms_and_conditions_version"
        .end annotation
    .end param
    .param p3    # Z
        .annotation runtime Lretrofit2/http/Field;
            value = "privacy_policy_accepted"
        .end annotation
    .end param
    .param p4    # Ljava/lang/String;
        .annotation runtime Lretrofit2/http/Field;
            value = "privacy_policy_version"
        .end annotation
    .end param
    .param p5    # Z
        .annotation runtime Lretrofit2/http/Field;
            value = "fscs_information_sheet_accepted"
        .end annotation
    .end param
    .param p6    # Ljava/lang/String;
        .annotation runtime Lretrofit2/http/Field;
            value = "fscs_information_sheet_version"
        .end annotation
    .end param
    .annotation runtime Lretrofit2/http/FormUrlEncoded;
    .end annotation

    .annotation runtime Lretrofit2/http/POST;
        value = "signup/legal-documents/accept"
    .end annotation
.end method

.method public abstract activateCard(Ljava/lang/String;)Lio/reactivex/b;
    .param p1    # Ljava/lang/String;
        .annotation runtime Lretrofit2/http/Field;
            value = "pan"
        .end annotation
    .end param
    .annotation runtime Lretrofit2/http/FormUrlEncoded;
    .end annotation

    .annotation runtime Lretrofit2/http/POST;
        value = "signup/personal-account/card/activate"
    .end annotation
.end method

.method public abstract cardOrderOptions()Lio/reactivex/v;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/v",
            "<",
            "Lco/uk/getmondo/api/model/order_card/CardOrderOptions;",
            ">;"
        }
    .end annotation

    .annotation runtime Lretrofit2/http/GET;
        value = "signup/personal-account/card-order/options"
    .end annotation
.end method

.method public abstract commitProfile()Lio/reactivex/b;
    .annotation runtime Lretrofit2/http/POST;
        value = "signup/profile/commit"
    .end annotation
.end method

.method public abstract createCardOrder(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lio/reactivex/b;
    .param p1    # Ljava/lang/String;
        .annotation runtime Lretrofit2/http/Field;
            value = "name_type"
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation runtime Lretrofit2/http/Field;
            value = "pin_type"
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation runtime Lretrofit2/http/Field;
            value = "pin"
        .end annotation
    .end param
    .annotation runtime Lretrofit2/http/FormUrlEncoded;
    .end annotation

    .annotation runtime Lretrofit2/http/POST;
        value = "signup/personal-account/card-order/create"
    .end annotation
.end method

.method public abstract legalDocuments()Lio/reactivex/v;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/v",
            "<",
            "Lco/uk/getmondo/api/model/signup/SignUpDocumentUrls;",
            ">;"
        }
    .end annotation

    .annotation runtime Lretrofit2/http/GET;
        value = "signup/legal-documents/documents"
    .end annotation
.end method

.method public abstract profile()Lio/reactivex/v;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/v",
            "<",
            "Lco/uk/getmondo/api/model/signup/SignUpProfile;",
            ">;"
        }
    .end annotation

    .annotation runtime Lretrofit2/http/GET;
        value = "signup/profile"
    .end annotation
.end method

.method public abstract searchAddress(Ljava/lang/String;Ljava/lang/String;)Lio/reactivex/v;
    .param p1    # Ljava/lang/String;
        .annotation runtime Lretrofit2/http/Query;
            value = "country_code"
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation runtime Lretrofit2/http/Query;
            value = "postal_code"
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/v",
            "<",
            "Lco/uk/getmondo/api/model/AddressesReponse;",
            ">;"
        }
    .end annotation

    .annotation runtime Lretrofit2/http/GET;
        value = "signup/profile/address/search"
    .end annotation
.end method

.method public abstract sendPhoneNumber(Ljava/lang/String;)Lio/reactivex/b;
    .param p1    # Ljava/lang/String;
        .annotation runtime Lretrofit2/http/Field;
            value = "phone_number"
        .end annotation
    .end param
    .annotation runtime Lretrofit2/http/FormUrlEncoded;
    .end annotation

    .annotation runtime Lretrofit2/http/POST;
        value = "signup/profile/phone/send"
    .end annotation
.end method

.method public abstract signupStatus()Lio/reactivex/v;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/v",
            "<",
            "Lco/uk/getmondo/api/model/signup/SignupInfo;",
            ">;"
        }
    .end annotation

    .annotation runtime Lretrofit2/http/GET;
        value = "signup/personal-account/status"
    .end annotation
.end method

.method public abstract skipFingerprintEnrolment()Lio/reactivex/b;
    .annotation runtime Lretrofit2/http/POST;
        value = "signup/secure-token/skip"
    .end annotation
.end method

.method public abstract startSignUp()Lio/reactivex/b;
    .annotation runtime Lretrofit2/http/POST;
        value = "signup/personal-account/start"
    .end annotation
.end method

.method public abstract submitProfile(Ljava/lang/String;Ljava/lang/String;Lorg/threeten/bp/LocalDate;)Lio/reactivex/b;
    .param p1    # Ljava/lang/String;
        .annotation runtime Lretrofit2/http/Field;
            value = "legal_name"
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation runtime Lretrofit2/http/Field;
            value = "preferred_name"
        .end annotation
    .end param
    .param p3    # Lorg/threeten/bp/LocalDate;
        .annotation runtime Lretrofit2/http/Field;
            value = "date_of_birth"
        .end annotation
    .end param
    .annotation runtime Lretrofit2/http/FormUrlEncoded;
    .end annotation

    .annotation runtime Lretrofit2/http/PUT;
        value = "signup/profile"
    .end annotation
.end method

.method public abstract submitProfileAddress(Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lio/reactivex/b;
    .param p1    # Ljava/lang/String;
        .annotation runtime Lretrofit2/http/Field;
            value = "address_id"
        .end annotation
    .end param
    .param p2    # Ljava/util/List;
        .annotation runtime Lretrofit2/http/Field;
            value = "address_lines[]"
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation runtime Lretrofit2/http/Field;
            value = "administrative_area"
        .end annotation
    .end param
    .param p4    # Ljava/lang/String;
        .annotation runtime Lretrofit2/http/Field;
            value = "postal_code"
        .end annotation
    .end param
    .param p5    # Ljava/lang/String;
        .annotation runtime Lretrofit2/http/Field;
            value = "country_code"
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/b;"
        }
    .end annotation

    .annotation runtime Lretrofit2/http/FormUrlEncoded;
    .end annotation

    .annotation runtime Lretrofit2/http/PUT;
        value = "signup/profile/address"
    .end annotation
.end method

.method public abstract subscribeToMarketing(ZZ)Lio/reactivex/b;
    .param p1    # Z
        .annotation runtime Lretrofit2/http/Field;
            value = "opt_in"
        .end annotation
    .end param
    .param p2    # Z
        .annotation runtime Lretrofit2/http/Field;
            value = "opt_out"
        .end annotation
    .end param
    .annotation runtime Lretrofit2/http/FormUrlEncoded;
    .end annotation

    .annotation runtime Lretrofit2/http/POST;
        value = "signup/marketing/subscribe"
    .end annotation
.end method

.method public abstract verifyPhoneNumber(Ljava/lang/String;Ljava/lang/String;)Lio/reactivex/b;
    .param p1    # Ljava/lang/String;
        .annotation runtime Lretrofit2/http/Field;
            value = "phone_number"
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation runtime Lretrofit2/http/Field;
            value = "code"
        .end annotation
    .end param
    .annotation runtime Lretrofit2/http/FormUrlEncoded;
    .end annotation

    .annotation runtime Lretrofit2/http/POST;
        value = "signup/profile/phone/verify"
    .end annotation
.end method
