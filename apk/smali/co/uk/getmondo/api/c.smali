.class public final Lco/uk/getmondo/api/c;
.super Ljava/lang/Object;
.source "ApiModule.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000\u00d0\u0001\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0006\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0006\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0005\u0008\u0007\u0018\u00002\u00020\u0001:\u0003[\\]B\u0005\u00a2\u0006\u0002\u0010\u0002J \u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u00082\u0006\u0010\t\u001a\u00020\nH\u0002JA\u0010\u000b\u001a\u00020\u00082\u0008\u0008\u0001\u0010\u000c\u001a\u00020\r2\u0006\u0010\u000e\u001a\u00020\u00082\u0006\u0010\u000f\u001a\u00020\u00102\u0006\u0010\u0011\u001a\u00020\u00122\u0008\u0008\u0001\u0010\u0013\u001a\u00020\u00062\u0006\u0010\u0014\u001a\u00020\u0015H\u0001\u00a2\u0006\u0002\u0008\u0016J\r\u0010\u0017\u001a\u00020\u0008H\u0001\u00a2\u0006\u0002\u0008\u0018J/\u0010\u0019\u001a\u00020\u00082\u0008\u0008\u0001\u0010\u001a\u001a\u00020\u00082\u0006\u0010\u001b\u001a\u00020\u001c2\u0006\u0010\u001d\u001a\u00020\u001e2\u0006\u0010\u001f\u001a\u00020 H\u0001\u00a2\u0006\u0002\u0008!J/\u0010\"\u001a\u00020\u00082\u0008\u0008\u0001\u0010\u001a\u001a\u00020\u00082\u0006\u0010\u001b\u001a\u00020\u001c2\u0006\u0010\u001d\u001a\u00020\u001e2\u0006\u0010\u001f\u001a\u00020 H\u0001\u00a2\u0006\u0002\u0008#J)\u0010$\u001a\u00020\u00042\u0008\u0008\u0001\u0010%\u001a\u00020\u00082\u0008\u0008\u0001\u0010\u0013\u001a\u00020\u00062\u0006\u0010&\u001a\u00020\'H\u0001\u00a2\u0006\u0002\u0008(J\r\u0010)\u001a\u00020\'H\u0001\u00a2\u0006\u0002\u0008*J)\u0010+\u001a\u00020,2\u0008\u0008\u0001\u0010-\u001a\u00020\u00082\u0006\u0010.\u001a\u00020/2\u0008\u0008\u0001\u0010\u0013\u001a\u00020\u0006H\u0001\u00a2\u0006\u0002\u00080J\r\u00101\u001a\u00020\u0010H\u0001\u00a2\u0006\u0002\u00082J\u0015\u00103\u001a\u0002042\u0006\u00105\u001a\u00020\u0004H\u0001\u00a2\u0006\u0002\u00086J)\u00107\u001a\u0002082\u0008\u0008\u0001\u0010-\u001a\u00020\u00082\u0006\u0010.\u001a\u00020/2\u0008\u0008\u0001\u0010\u0013\u001a\u00020\u0006H\u0001\u00a2\u0006\u0002\u00089JA\u0010:\u001a\u00020;2\u0008\u0008\u0001\u0010\u001a\u001a\u00020\u00082\u0006\u0010&\u001a\u00020\'2\u0006\u0010\u001b\u001a\u00020\u001c2\u0006\u0010\u001d\u001a\u00020\u001e2\u0006\u0010\u001f\u001a\u00020 2\u0008\u0008\u0001\u0010\u0013\u001a\u00020\u0006H\u0001\u00a2\u0006\u0002\u0008<J\u0015\u0010=\u001a\u00020>2\u0006\u00105\u001a\u00020\u0004H\u0001\u00a2\u0006\u0002\u0008?J\u0015\u0010@\u001a\u00020A2\u0006\u00105\u001a\u00020\u0004H\u0001\u00a2\u0006\u0002\u0008BJ\r\u0010C\u001a\u00020/H\u0001\u00a2\u0006\u0002\u0008DJ1\u0010E\u001a\u00020F2\u0008\u0008\u0001\u0010\u001a\u001a\u00020\u00082\u0006\u0010&\u001a\u00020\'2\u0006\u0010\u001d\u001a\u00020\u001e2\u0008\u0008\u0001\u0010\u0013\u001a\u00020\u0006H\u0001\u00a2\u0006\u0002\u0008GJ)\u0010H\u001a\u00020I2\u0008\u0008\u0001\u0010-\u001a\u00020\u00082\u0006\u0010.\u001a\u00020/2\u0008\u0008\u0001\u0010\u0013\u001a\u00020\u0006H\u0001\u00a2\u0006\u0002\u0008JJ)\u0010K\u001a\u00020L2\u0008\u0008\u0001\u0010-\u001a\u00020\u00082\u0006\u0010.\u001a\u00020/2\u0008\u0008\u0001\u0010\u0013\u001a\u00020\u0006H\u0001\u00a2\u0006\u0002\u0008MJ)\u0010N\u001a\u00020O2\u0008\u0008\u0001\u0010-\u001a\u00020\u00082\u0006\u0010.\u001a\u00020/2\u0008\u0008\u0001\u0010\u0013\u001a\u00020\u0006H\u0001\u00a2\u0006\u0002\u0008PJ/\u0010Q\u001a\u00020R2\u0006\u0010\u000e\u001a\u00020\u00082\u0006\u0010\u000f\u001a\u00020\u00102\u0006\u0010&\u001a\u00020\'2\u0008\u0008\u0001\u0010S\u001a\u00020\u0006H\u0001\u00a2\u0006\u0002\u0008TJ)\u0010U\u001a\u00020V2\u0008\u0008\u0001\u0010-\u001a\u00020\u00082\u0006\u0010.\u001a\u00020/2\u0008\u0008\u0001\u0010\u0013\u001a\u00020\u0006H\u0001\u00a2\u0006\u0002\u0008WJ)\u0010X\u001a\u00020Y2\u0008\u0008\u0001\u0010-\u001a\u00020\u00082\u0006\u0010.\u001a\u00020/2\u0008\u0008\u0001\u0010\u0013\u001a\u00020\u0006H\u0001\u00a2\u0006\u0002\u0008Z\u00a8\u0006^"
    }
    d2 = {
        "Lco/uk/getmondo/api/ApiModule;",
        "",
        "()V",
        "buildRetrofit",
        "Lretrofit2/Retrofit;",
        "baseUrl",
        "",
        "client",
        "Lokhttp3/OkHttpClient;",
        "converterFactory",
        "Lretrofit2/Converter$Factory;",
        "provideBaseMonzoOkHttpClient",
        "context",
        "Landroid/content/Context;",
        "baseOkHttpClient",
        "loggingInterceptor",
        "Lokhttp3/logging/HttpLoggingInterceptor;",
        "additionalHeadersInterceptor",
        "Lco/uk/getmondo/api/AdditionalHeadersInterceptor;",
        "apiUrl",
        "preferences",
        "Lco/uk/getmondo/developer_options/DeveloperOptionsPreferences;",
        "provideBaseMonzoOkHttpClient$app_monzoPrepaidRelease",
        "provideBaseOkHttpClient",
        "provideBaseOkHttpClient$app_monzoPrepaidRelease",
        "provideConfiguredForSignUpOkHttpClient",
        "baseMonzoHttpClient",
        "bearerAuthInterceptor",
        "Lco/uk/getmondo/api/authentication/BearerAuthInterceptor;",
        "errorHandlerInterceptor",
        "Lco/uk/getmondo/api/ErrorHandlerInterceptor;",
        "monzoAuthenticator",
        "Lco/uk/getmondo/api/authentication/MonzoAuthenticator;",
        "provideConfiguredForSignUpOkHttpClient$app_monzoPrepaidRelease",
        "provideConfiguredOkHttpClient",
        "provideConfiguredOkHttpClient$app_monzoPrepaidRelease",
        "provideDefaultMonzoRetrofit",
        "configuredMonzoHttpClient",
        "gson",
        "Lcom/google/gson/Gson;",
        "provideDefaultMonzoRetrofit$app_monzoPrepaidRelease",
        "provideGson",
        "provideGson$app_monzoPrepaidRelease",
        "provideHelpApi",
        "Lco/uk/getmondo/api/HelpApi;",
        "okHttpClient",
        "moshi",
        "Lcom/squareup/moshi/Moshi;",
        "provideHelpApi$app_monzoPrepaidRelease",
        "provideHttpLoggingInterceptor",
        "provideHttpLoggingInterceptor$app_monzoPrepaidRelease",
        "provideIdentityVerificationApi",
        "Lco/uk/getmondo/api/IdentityVerificationApi;",
        "retrofit",
        "provideIdentityVerificationApi$app_monzoPrepaidRelease",
        "provideMigrationApi",
        "Lco/uk/getmondo/api/MigrationApi;",
        "provideMigrationApi$app_monzoPrepaidRelease",
        "provideMonzoAnalyticsApi",
        "Lco/uk/getmondo/api/AnalyticsApi;",
        "provideMonzoAnalyticsApi$app_monzoPrepaidRelease",
        "provideMonzoApi",
        "Lco/uk/getmondo/api/MonzoApi;",
        "provideMonzoApi$app_monzoPrepaidRelease",
        "provideMonzoPaymentsApi",
        "Lco/uk/getmondo/api/PaymentsApi;",
        "provideMonzoPaymentsApi$app_monzoPrepaidRelease",
        "provideMoshi",
        "provideMoshi$app_monzoPrepaidRelease",
        "provideOAuthMonzoApi",
        "Lco/uk/getmondo/api/authentication/MonzoOAuthApi;",
        "provideOAuthMonzoApi$app_monzoPrepaidRelease",
        "provideOverdraftApi",
        "Lco/uk/getmondo/api/OverdraftApi;",
        "provideOverdraftApi$app_monzoPrepaidRelease",
        "providePaymentLimitsApi",
        "Lco/uk/getmondo/api/PaymentLimitsApi;",
        "providePaymentLimitsApi$app_monzoPrepaidRelease",
        "provideProfileApi",
        "Lco/uk/getmondo/profile/data/MonzoProfileApi;",
        "provideProfileApi$app_monzoPrepaidRelease",
        "provideServiceStatusApi",
        "Lco/uk/getmondo/api/ServiceStatusApi;",
        "statusApiUrl",
        "provideServiceStatusApi$app_monzoPrepaidRelease",
        "provideSignUpApi",
        "Lco/uk/getmondo/api/SignupApi;",
        "provideSignUpApi$app_monzoPrepaidRelease",
        "provideTaxResidencyApi",
        "Lco/uk/getmondo/api/TaxResidencyApi;",
        "provideTaxResidencyApi$app_monzoPrepaidRelease",
        "ConfiguredForSignUpMonzoHttpClient",
        "ConfiguredMonzoHttpClient",
        "MonzoHttpClient",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private final a(Ljava/lang/String;Lokhttp3/OkHttpClient;Lretrofit2/Converter$Factory;)Lretrofit2/Retrofit;
    .locals 2

    .prologue
    .line 332
    new-instance v0, Lretrofit2/Retrofit$Builder;

    invoke-direct {v0}, Lretrofit2/Retrofit$Builder;-><init>()V

    .line 333
    invoke-virtual {v0, p1}, Lretrofit2/Retrofit$Builder;->baseUrl(Ljava/lang/String;)Lretrofit2/Retrofit$Builder;

    move-result-object v0

    .line 334
    invoke-virtual {v0, p2}, Lretrofit2/Retrofit$Builder;->client(Lokhttp3/OkHttpClient;)Lretrofit2/Retrofit$Builder;

    move-result-object v0

    .line 335
    invoke-virtual {v0, p3}, Lretrofit2/Retrofit$Builder;->addConverterFactory(Lretrofit2/Converter$Factory;)Lretrofit2/Retrofit$Builder;

    move-result-object v1

    .line 336
    invoke-static {}, Lretrofit2/adapter/rxjava2/RxJava2CallAdapterFactory;->create()Lretrofit2/adapter/rxjava2/RxJava2CallAdapterFactory;

    move-result-object v0

    check-cast v0, Lretrofit2/CallAdapter$Factory;

    invoke-virtual {v1, v0}, Lretrofit2/Retrofit$Builder;->addCallAdapterFactory(Lretrofit2/CallAdapter$Factory;)Lretrofit2/Retrofit$Builder;

    move-result-object v0

    .line 337
    invoke-virtual {v0}, Lretrofit2/Retrofit$Builder;->build()Lretrofit2/Retrofit;

    move-result-object v0

    const-string v1, "Retrofit.Builder()\n     \u2026\n                .build()"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method public final a(Lokhttp3/OkHttpClient;Lcom/google/gson/f;Lco/uk/getmondo/api/authentication/b;Lco/uk/getmondo/api/ac;Lco/uk/getmondo/api/authentication/k;Ljava/lang/String;)Lco/uk/getmondo/api/AnalyticsApi;
    .locals 3

    .prologue
    const-string v0, "baseMonzoHttpClient"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "gson"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "bearerAuthInterceptor"

    invoke-static {p3, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "errorHandlerInterceptor"

    invoke-static {p4, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "monzoAuthenticator"

    invoke-static {p5, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "apiUrl"

    invoke-static {p6, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 276
    const/4 v0, 0x0

    invoke-virtual {p4, v0}, Lco/uk/getmondo/api/ac;->a(Z)V

    .line 277
    invoke-virtual {p1}, Lokhttp3/OkHttpClient;->newBuilder()Lokhttp3/OkHttpClient$Builder;

    move-result-object v0

    .line 278
    check-cast p5, Lokhttp3/Authenticator;

    invoke-virtual {v0, p5}, Lokhttp3/OkHttpClient$Builder;->authenticator(Lokhttp3/Authenticator;)Lokhttp3/OkHttpClient$Builder;

    move-result-object v0

    .line 279
    check-cast p3, Lokhttp3/Interceptor;

    invoke-virtual {v0, p3}, Lokhttp3/OkHttpClient$Builder;->addInterceptor(Lokhttp3/Interceptor;)Lokhttp3/OkHttpClient$Builder;

    move-result-object v0

    .line 280
    check-cast p4, Lokhttp3/Interceptor;

    invoke-virtual {v0, p4}, Lokhttp3/OkHttpClient$Builder;->addInterceptor(Lokhttp3/Interceptor;)Lokhttp3/OkHttpClient$Builder;

    move-result-object v0

    .line 281
    invoke-virtual {v0}, Lokhttp3/OkHttpClient$Builder;->build()Lokhttp3/OkHttpClient;

    move-result-object v1

    .line 283
    const-string v0, "okHttpClient"

    invoke-static {v1, v0}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p2}, Lretrofit2/converter/gson/GsonConverterFactory;->create(Lcom/google/gson/f;)Lretrofit2/converter/gson/GsonConverterFactory;

    move-result-object v0

    const-string v2, "GsonConverterFactory.create(gson)"

    invoke-static {v0, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lretrofit2/Converter$Factory;

    invoke-direct {p0, p6, v1, v0}, Lco/uk/getmondo/api/c;->a(Ljava/lang/String;Lokhttp3/OkHttpClient;Lretrofit2/Converter$Factory;)Lretrofit2/Retrofit;

    move-result-object v0

    .line 284
    const-class v1, Lco/uk/getmondo/api/AnalyticsApi;

    invoke-virtual {v0, v1}, Lretrofit2/Retrofit;->create(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    const-string v1, "buildRetrofit(apiUrl, ok\u2026AnalyticsApi::class.java)"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lco/uk/getmondo/api/AnalyticsApi;

    return-object v0
.end method

.method public final a(Lretrofit2/Retrofit;)Lco/uk/getmondo/api/MonzoApi;
    .locals 2

    .prologue
    const-string v0, "retrofit"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 186
    const-class v0, Lco/uk/getmondo/api/MonzoApi;

    invoke-virtual {p1, v0}, Lretrofit2/Retrofit;->create(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    const-string v1, "retrofit.create(MonzoApi::class.java)"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lco/uk/getmondo/api/MonzoApi;

    return-object v0
.end method

.method public final a(Lokhttp3/OkHttpClient;Lokhttp3/logging/HttpLoggingInterceptor;Lcom/google/gson/f;Ljava/lang/String;)Lco/uk/getmondo/api/ServiceStatusApi;
    .locals 2

    .prologue
    const-string v0, "baseOkHttpClient"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "loggingInterceptor"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "gson"

    invoke-static {p3, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "statusApiUrl"

    invoke-static {p4, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 311
    invoke-virtual {p1}, Lokhttp3/OkHttpClient;->newBuilder()Lokhttp3/OkHttpClient$Builder;

    move-result-object v0

    .line 312
    check-cast p2, Lokhttp3/Interceptor;

    invoke-virtual {v0, p2}, Lokhttp3/OkHttpClient$Builder;->addInterceptor(Lokhttp3/Interceptor;)Lokhttp3/OkHttpClient$Builder;

    move-result-object v0

    .line 313
    invoke-virtual {v0}, Lokhttp3/OkHttpClient$Builder;->build()Lokhttp3/OkHttpClient;

    move-result-object v0

    .line 315
    new-instance v1, Lretrofit2/Retrofit$Builder;

    invoke-direct {v1}, Lretrofit2/Retrofit$Builder;-><init>()V

    .line 316
    invoke-virtual {v1, p4}, Lretrofit2/Retrofit$Builder;->baseUrl(Ljava/lang/String;)Lretrofit2/Retrofit$Builder;

    move-result-object v1

    .line 317
    invoke-virtual {v1, v0}, Lretrofit2/Retrofit$Builder;->client(Lokhttp3/OkHttpClient;)Lretrofit2/Retrofit$Builder;

    move-result-object v1

    .line 318
    invoke-static {p3}, Lretrofit2/converter/gson/GsonConverterFactory;->create(Lcom/google/gson/f;)Lretrofit2/converter/gson/GsonConverterFactory;

    move-result-object v0

    check-cast v0, Lretrofit2/Converter$Factory;

    invoke-virtual {v1, v0}, Lretrofit2/Retrofit$Builder;->addConverterFactory(Lretrofit2/Converter$Factory;)Lretrofit2/Retrofit$Builder;

    move-result-object v1

    .line 319
    invoke-static {}, Lretrofit2/adapter/rxjava2/RxJava2CallAdapterFactory;->create()Lretrofit2/adapter/rxjava2/RxJava2CallAdapterFactory;

    move-result-object v0

    check-cast v0, Lretrofit2/CallAdapter$Factory;

    invoke-virtual {v1, v0}, Lretrofit2/Retrofit$Builder;->addCallAdapterFactory(Lretrofit2/CallAdapter$Factory;)Lretrofit2/Retrofit$Builder;

    move-result-object v0

    .line 320
    invoke-virtual {v0}, Lretrofit2/Retrofit$Builder;->build()Lretrofit2/Retrofit;

    move-result-object v0

    .line 321
    const-class v1, Lco/uk/getmondo/api/ServiceStatusApi;

    invoke-virtual {v0, v1}, Lretrofit2/Retrofit;->create(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    const-string v1, "Retrofit.Builder()\n     \u2026iceStatusApi::class.java)"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lco/uk/getmondo/api/ServiceStatusApi;

    return-object v0
.end method

.method public final a(Lokhttp3/OkHttpClient;Lcom/squareup/moshi/v;Ljava/lang/String;)Lco/uk/getmondo/api/TaxResidencyApi;
    .locals 2

    .prologue
    const-string v0, "okHttpClient"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "moshi"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "apiUrl"

    invoke-static {p3, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 202
    invoke-static {p2}, Lretrofit2/converter/moshi/MoshiConverterFactory;->create(Lcom/squareup/moshi/v;)Lretrofit2/converter/moshi/MoshiConverterFactory;

    move-result-object v0

    const-string v1, "MoshiConverterFactory.create(moshi)"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lretrofit2/Converter$Factory;

    invoke-direct {p0, p3, p1, v0}, Lco/uk/getmondo/api/c;->a(Ljava/lang/String;Lokhttp3/OkHttpClient;Lretrofit2/Converter$Factory;)Lretrofit2/Retrofit;

    move-result-object v0

    .line 203
    const-class v1, Lco/uk/getmondo/api/TaxResidencyApi;

    invoke-virtual {v0, v1}, Lretrofit2/Retrofit;->create(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    const-string v1, "buildRetrofit(apiUrl, ok\u2026ResidencyApi::class.java)"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lco/uk/getmondo/api/TaxResidencyApi;

    return-object v0
.end method

.method public final a(Lokhttp3/OkHttpClient;Lcom/google/gson/f;Lco/uk/getmondo/api/ac;Ljava/lang/String;)Lco/uk/getmondo/api/authentication/MonzoOAuthApi;
    .locals 3

    .prologue
    const-string v0, "baseMonzoHttpClient"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "gson"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "errorHandlerInterceptor"

    invoke-static {p3, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "apiUrl"

    invoke-static {p4, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 295
    const/4 v0, 0x0

    invoke-virtual {p3, v0}, Lco/uk/getmondo/api/ac;->a(Z)V

    .line 296
    invoke-virtual {p1}, Lokhttp3/OkHttpClient;->newBuilder()Lokhttp3/OkHttpClient$Builder;

    move-result-object v0

    .line 297
    check-cast p3, Lokhttp3/Interceptor;

    invoke-virtual {v0, p3}, Lokhttp3/OkHttpClient$Builder;->addInterceptor(Lokhttp3/Interceptor;)Lokhttp3/OkHttpClient$Builder;

    move-result-object v0

    .line 298
    invoke-virtual {v0}, Lokhttp3/OkHttpClient$Builder;->build()Lokhttp3/OkHttpClient;

    move-result-object v1

    .line 300
    const-string v0, "okHttpClient"

    invoke-static {v1, v0}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p2}, Lretrofit2/converter/gson/GsonConverterFactory;->create(Lcom/google/gson/f;)Lretrofit2/converter/gson/GsonConverterFactory;

    move-result-object v0

    const-string v2, "GsonConverterFactory.create(gson)"

    invoke-static {v0, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lretrofit2/Converter$Factory;

    invoke-direct {p0, p4, v1, v0}, Lco/uk/getmondo/api/c;->a(Ljava/lang/String;Lokhttp3/OkHttpClient;Lretrofit2/Converter$Factory;)Lretrofit2/Retrofit;

    move-result-object v0

    .line 301
    const-class v1, Lco/uk/getmondo/api/authentication/MonzoOAuthApi;

    invoke-virtual {v0, v1}, Lretrofit2/Retrofit;->create(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    const-string v1, "buildRetrofit(apiUrl, ok\u2026onzoOAuthApi::class.java)"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lco/uk/getmondo/api/authentication/MonzoOAuthApi;

    return-object v0
.end method

.method public final a()Lcom/squareup/moshi/v;
    .locals 2

    .prologue
    .line 60
    new-instance v1, Lcom/squareup/moshi/v$a;

    invoke-direct {v1}, Lcom/squareup/moshi/v$a;-><init>()V

    .line 61
    new-instance v0, Lcom/squareup/moshi/r;

    invoke-direct {v0}, Lcom/squareup/moshi/r;-><init>()V

    check-cast v0, Lcom/squareup/moshi/i$a;

    invoke-virtual {v1, v0}, Lcom/squareup/moshi/v$a;->a(Lcom/squareup/moshi/i$a;)Lcom/squareup/moshi/v$a;

    move-result-object v0

    .line 62
    new-instance v1, Lco/uk/getmondo/signup_old/ac;

    invoke-direct {v1}, Lco/uk/getmondo/signup_old/ac;-><init>()V

    invoke-virtual {v0, v1}, Lcom/squareup/moshi/v$a;->a(Ljava/lang/Object;)Lcom/squareup/moshi/v$a;

    move-result-object v0

    .line 63
    new-instance v1, Lco/uk/getmondo/common/c/b;

    invoke-direct {v1}, Lco/uk/getmondo/common/c/b;-><init>()V

    invoke-virtual {v0, v1}, Lcom/squareup/moshi/v$a;->a(Ljava/lang/Object;)Lcom/squareup/moshi/v$a;

    move-result-object v0

    .line 64
    invoke-virtual {v0}, Lcom/squareup/moshi/v$a;->a()Lcom/squareup/moshi/v;

    move-result-object v0

    const-string v1, "Moshi.Builder()\n        \u2026\n                .build()"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public final a(Landroid/content/Context;Lokhttp3/OkHttpClient;Lokhttp3/logging/HttpLoggingInterceptor;Lco/uk/getmondo/api/a;Ljava/lang/String;Lco/uk/getmondo/developer_options/a;)Lokhttp3/OkHttpClient;
    .locals 7

    .prologue
    const/4 v1, 0x0

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "baseOkHttpClient"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "loggingInterceptor"

    invoke-static {p3, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "additionalHeadersInterceptor"

    invoke-static {p4, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "apiUrl"

    invoke-static {p5, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "preferences"

    invoke-static {p6, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 114
    invoke-static {p5}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v2

    .line 115
    new-instance v3, Lokhttp3/CertificatePinner$Builder;

    invoke-direct {v3}, Lokhttp3/CertificatePinner$Builder;-><init>()V

    .line 116
    sget-object v4, Lco/uk/getmondo/a;->a:[Ljava/lang/String;

    move v0, v1

    :goto_0
    array-length v5, v4

    if-ge v0, v5, :cond_0

    aget-object v5, v4, v0

    .line 117
    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/String;

    aput-object v5, v6, v1

    invoke-virtual {v3, v2, v6}, Lokhttp3/CertificatePinner$Builder;->add(Ljava/lang/String;[Ljava/lang/String;)Lokhttp3/CertificatePinner$Builder;

    .line 116
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 120
    :cond_0
    invoke-virtual {p2}, Lokhttp3/OkHttpClient;->newBuilder()Lokhttp3/OkHttpClient$Builder;

    move-result-object v0

    .line 121
    check-cast p3, Lokhttp3/Interceptor;

    invoke-virtual {v0, p3}, Lokhttp3/OkHttpClient$Builder;->addInterceptor(Lokhttp3/Interceptor;)Lokhttp3/OkHttpClient$Builder;

    move-result-object v0

    .line 122
    check-cast p4, Lokhttp3/Interceptor;

    invoke-virtual {v0, p4}, Lokhttp3/OkHttpClient$Builder;->addInterceptor(Lokhttp3/Interceptor;)Lokhttp3/OkHttpClient$Builder;

    move-result-object v0

    .line 124
    invoke-virtual {v3}, Lokhttp3/CertificatePinner$Builder;->build()Lokhttp3/CertificatePinner;

    move-result-object v1

    invoke-virtual {v0, v1}, Lokhttp3/OkHttpClient$Builder;->certificatePinner(Lokhttp3/CertificatePinner;)Lokhttp3/OkHttpClient$Builder;

    .line 127
    nop

    .line 131
    invoke-virtual {v0}, Lokhttp3/OkHttpClient$Builder;->build()Lokhttp3/OkHttpClient;

    move-result-object v0

    const-string v1, "builder.build()"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public final a(Lokhttp3/OkHttpClient;Lco/uk/getmondo/api/authentication/b;Lco/uk/getmondo/api/ac;Lco/uk/getmondo/api/authentication/k;)Lokhttp3/OkHttpClient;
    .locals 2

    .prologue
    const-string v0, "baseMonzoHttpClient"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "bearerAuthInterceptor"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "errorHandlerInterceptor"

    invoke-static {p3, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "monzoAuthenticator"

    invoke-static {p4, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 142
    const/4 v0, 0x1

    invoke-virtual {p3, v0}, Lco/uk/getmondo/api/ac;->a(Z)V

    .line 143
    invoke-virtual {p1}, Lokhttp3/OkHttpClient;->newBuilder()Lokhttp3/OkHttpClient$Builder;

    move-result-object v0

    .line 144
    check-cast p4, Lokhttp3/Authenticator;

    invoke-virtual {v0, p4}, Lokhttp3/OkHttpClient$Builder;->authenticator(Lokhttp3/Authenticator;)Lokhttp3/OkHttpClient$Builder;

    move-result-object v0

    .line 145
    check-cast p3, Lokhttp3/Interceptor;

    invoke-virtual {v0, p3}, Lokhttp3/OkHttpClient$Builder;->addInterceptor(Lokhttp3/Interceptor;)Lokhttp3/OkHttpClient$Builder;

    move-result-object v0

    .line 146
    check-cast p2, Lokhttp3/Interceptor;

    invoke-virtual {v0, p2}, Lokhttp3/OkHttpClient$Builder;->addInterceptor(Lokhttp3/Interceptor;)Lokhttp3/OkHttpClient$Builder;

    move-result-object v0

    .line 147
    invoke-virtual {v0}, Lokhttp3/OkHttpClient$Builder;->build()Lokhttp3/OkHttpClient;

    move-result-object v0

    const-string v1, "baseMonzoHttpClient.newB\u2026\n                .build()"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public final a(Lokhttp3/OkHttpClient;Ljava/lang/String;Lcom/google/gson/f;)Lretrofit2/Retrofit;
    .locals 4

    .prologue
    const-string v0, "configuredMonzoHttpClient"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "apiUrl"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "gson"

    invoke-static {p3, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 178
    invoke-virtual {p1}, Lokhttp3/OkHttpClient;->newBuilder()Lokhttp3/OkHttpClient$Builder;

    move-result-object v0

    .line 179
    const-wide/16 v2, 0x78

    sget-object v1, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v2, v3, v1}, Lokhttp3/OkHttpClient$Builder;->readTimeout(JLjava/util/concurrent/TimeUnit;)Lokhttp3/OkHttpClient$Builder;

    move-result-object v0

    .line 180
    invoke-virtual {v0}, Lokhttp3/OkHttpClient$Builder;->build()Lokhttp3/OkHttpClient;

    move-result-object v1

    .line 182
    const-string v0, "okHttpClient"

    invoke-static {v1, v0}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p3}, Lretrofit2/converter/gson/GsonConverterFactory;->create(Lcom/google/gson/f;)Lretrofit2/converter/gson/GsonConverterFactory;

    move-result-object v0

    const-string v2, "GsonConverterFactory.create(gson)"

    invoke-static {v0, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lretrofit2/Converter$Factory;

    invoke-direct {p0, p2, v1, v0}, Lco/uk/getmondo/api/c;->a(Ljava/lang/String;Lokhttp3/OkHttpClient;Lretrofit2/Converter$Factory;)Lretrofit2/Retrofit;

    move-result-object v0

    return-object v0
.end method

.method public final b(Lretrofit2/Retrofit;)Lco/uk/getmondo/api/IdentityVerificationApi;
    .locals 2

    .prologue
    const-string v0, "retrofit"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 190
    const-class v0, Lco/uk/getmondo/api/IdentityVerificationApi;

    invoke-virtual {p1, v0}, Lretrofit2/Retrofit;->create(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    const-string v1, "retrofit.create(Identity\u2026ificationApi::class.java)"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lco/uk/getmondo/api/IdentityVerificationApi;

    return-object v0
.end method

.method public final b(Lokhttp3/OkHttpClient;Lcom/squareup/moshi/v;Ljava/lang/String;)Lco/uk/getmondo/profile/data/MonzoProfileApi;
    .locals 2

    .prologue
    const-string v0, "okHttpClient"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "moshi"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "apiUrl"

    invoke-static {p3, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 212
    invoke-static {p2}, Lretrofit2/converter/moshi/MoshiConverterFactory;->create(Lcom/squareup/moshi/v;)Lretrofit2/converter/moshi/MoshiConverterFactory;

    move-result-object v0

    const-string v1, "MoshiConverterFactory.create(moshi)"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lretrofit2/Converter$Factory;

    invoke-direct {p0, p3, p1, v0}, Lco/uk/getmondo/api/c;->a(Ljava/lang/String;Lokhttp3/OkHttpClient;Lretrofit2/Converter$Factory;)Lretrofit2/Retrofit;

    move-result-object v0

    .line 213
    const-class v1, Lco/uk/getmondo/profile/data/MonzoProfileApi;

    invoke-virtual {v0, v1}, Lretrofit2/Retrofit;->create(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    const-string v1, "buildRetrofit(apiUrl, ok\u2026zoProfileApi::class.java)"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lco/uk/getmondo/profile/data/MonzoProfileApi;

    return-object v0
.end method

.method public final b()Lcom/google/gson/f;
    .locals 3

    .prologue
    .line 69
    new-instance v0, Lcom/google/gson/g;

    invoke-direct {v0}, Lcom/google/gson/g;-><init>()V

    .line 70
    sget-object v1, Lcom/google/gson/d;->d:Lcom/google/gson/d;

    invoke-virtual {v0, v1}, Lcom/google/gson/g;->a(Lcom/google/gson/d;)Lcom/google/gson/g;

    move-result-object v1

    .line 71
    new-instance v0, Lco/uk/getmondo/common/g/a;

    invoke-direct {v0}, Lco/uk/getmondo/common/g/a;-><init>()V

    check-cast v0, Lcom/google/gson/t;

    invoke-virtual {v1, v0}, Lcom/google/gson/g;->a(Lcom/google/gson/t;)Lcom/google/gson/g;

    move-result-object v1

    .line 73
    const-class v0, Lorg/threeten/bp/LocalDateTime;

    check-cast v0, Ljava/lang/reflect/Type;

    sget-object v2, Lco/uk/getmondo/api/c$a;->a:Lco/uk/getmondo/api/c$a;

    invoke-virtual {v1, v0, v2}, Lcom/google/gson/g;->a(Ljava/lang/reflect/Type;Ljava/lang/Object;)Lcom/google/gson/g;

    move-result-object v1

    .line 81
    const-class v0, Lorg/threeten/bp/LocalDate;

    check-cast v0, Ljava/lang/reflect/Type;

    sget-object v2, Lco/uk/getmondo/api/c$b;->a:Lco/uk/getmondo/api/c$b;

    invoke-virtual {v1, v0, v2}, Lcom/google/gson/g;->a(Ljava/lang/reflect/Type;Ljava/lang/Object;)Lcom/google/gson/g;

    move-result-object v1

    .line 89
    const-class v0, Lorg/threeten/bp/ZonedDateTime;

    check-cast v0, Ljava/lang/reflect/Type;

    sget-object v2, Lco/uk/getmondo/api/c$c;->a:Lco/uk/getmondo/api/c$c;

    invoke-virtual {v1, v0, v2}, Lcom/google/gson/g;->a(Ljava/lang/reflect/Type;Ljava/lang/Object;)Lcom/google/gson/g;

    move-result-object v0

    .line 97
    const-string v1, "yyyy-MM-dd\'T\'HH:mm:ss.SSSZ"

    invoke-virtual {v0, v1}, Lcom/google/gson/g;->a(Ljava/lang/String;)Lcom/google/gson/g;

    move-result-object v0

    .line 98
    invoke-virtual {v0}, Lcom/google/gson/g;->a()Lcom/google/gson/f;

    move-result-object v0

    const-string v1, "GsonBuilder()\n          \u2026                .create()"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public final b(Lokhttp3/OkHttpClient;Lco/uk/getmondo/api/authentication/b;Lco/uk/getmondo/api/ac;Lco/uk/getmondo/api/authentication/k;)Lokhttp3/OkHttpClient;
    .locals 5

    .prologue
    const-string v0, "baseMonzoHttpClient"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "bearerAuthInterceptor"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "errorHandlerInterceptor"

    invoke-static {p3, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "monzoAuthenticator"

    invoke-static {p4, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 161
    const/4 v0, 0x1

    invoke-virtual {p3, v0}, Lco/uk/getmondo/api/ac;->a(Z)V

    .line 162
    invoke-virtual {p1}, Lokhttp3/OkHttpClient;->newBuilder()Lokhttp3/OkHttpClient$Builder;

    move-result-object v0

    .line 163
    check-cast p4, Lokhttp3/Authenticator;

    invoke-virtual {v0, p4}, Lokhttp3/OkHttpClient$Builder;->authenticator(Lokhttp3/Authenticator;)Lokhttp3/OkHttpClient$Builder;

    move-result-object v0

    .line 164
    check-cast p3, Lokhttp3/Interceptor;

    invoke-virtual {v0, p3}, Lokhttp3/OkHttpClient$Builder;->addInterceptor(Lokhttp3/Interceptor;)Lokhttp3/OkHttpClient$Builder;

    move-result-object v0

    .line 165
    check-cast p2, Lokhttp3/Interceptor;

    invoke-virtual {v0, p2}, Lokhttp3/OkHttpClient$Builder;->addInterceptor(Lokhttp3/Interceptor;)Lokhttp3/OkHttpClient$Builder;

    move-result-object v0

    .line 168
    invoke-virtual {v0}, Lokhttp3/OkHttpClient$Builder;->interceptors()Ljava/util/List;

    move-result-object v1

    const/4 v2, 0x0

    new-instance v3, Lco/uk/getmondo/api/ai;

    sget-object v4, Lco/uk/getmondo/api/model/signup/SignupSource;->PERSONAL_ACCOUNT:Lco/uk/getmondo/api/model/signup/SignupSource;

    invoke-direct {v3, v4}, Lco/uk/getmondo/api/ai;-><init>(Lco/uk/getmondo/api/model/signup/SignupSource;)V

    invoke-interface {v1, v2, v3}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 169
    invoke-virtual {v0}, Lokhttp3/OkHttpClient$Builder;->build()Lokhttp3/OkHttpClient;

    move-result-object v0

    const-string v1, "builder.build()"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public final c(Lokhttp3/OkHttpClient;Lcom/squareup/moshi/v;Ljava/lang/String;)Lco/uk/getmondo/api/HelpApi;
    .locals 2

    .prologue
    const-string v0, "okHttpClient"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "moshi"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "apiUrl"

    invoke-static {p3, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 222
    invoke-static {p2}, Lretrofit2/converter/moshi/MoshiConverterFactory;->create(Lcom/squareup/moshi/v;)Lretrofit2/converter/moshi/MoshiConverterFactory;

    move-result-object v0

    const-string v1, "MoshiConverterFactory.create(moshi)"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lretrofit2/Converter$Factory;

    invoke-direct {p0, p3, p1, v0}, Lco/uk/getmondo/api/c;->a(Ljava/lang/String;Lokhttp3/OkHttpClient;Lretrofit2/Converter$Factory;)Lretrofit2/Retrofit;

    move-result-object v0

    .line 223
    const-class v1, Lco/uk/getmondo/api/HelpApi;

    invoke-virtual {v0, v1}, Lretrofit2/Retrofit;->create(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    const-string v1, "buildRetrofit(apiUrl, ok\u2026eate(HelpApi::class.java)"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lco/uk/getmondo/api/HelpApi;

    return-object v0
.end method

.method public final c(Lretrofit2/Retrofit;)Lco/uk/getmondo/api/PaymentsApi;
    .locals 2

    .prologue
    const-string v0, "retrofit"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 194
    const-class v0, Lco/uk/getmondo/api/PaymentsApi;

    invoke-virtual {p1, v0}, Lretrofit2/Retrofit;->create(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    const-string v1, "retrofit.create(PaymentsApi::class.java)"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lco/uk/getmondo/api/PaymentsApi;

    return-object v0
.end method

.method public final c()Lokhttp3/OkHttpClient;
    .locals 2

    .prologue
    .line 102
    new-instance v0, Lokhttp3/OkHttpClient$Builder;

    invoke-direct {v0}, Lokhttp3/OkHttpClient$Builder;-><init>()V

    invoke-virtual {v0}, Lokhttp3/OkHttpClient$Builder;->build()Lokhttp3/OkHttpClient;

    move-result-object v0

    const-string v1, "OkHttpClient.Builder().build()"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public final d(Lokhttp3/OkHttpClient;Lcom/squareup/moshi/v;Ljava/lang/String;)Lco/uk/getmondo/api/SignupApi;
    .locals 2

    .prologue
    const-string v0, "okHttpClient"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "moshi"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "apiUrl"

    invoke-static {p3, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 232
    invoke-static {p2}, Lretrofit2/converter/moshi/MoshiConverterFactory;->create(Lcom/squareup/moshi/v;)Lretrofit2/converter/moshi/MoshiConverterFactory;

    move-result-object v0

    const-string v1, "MoshiConverterFactory.create(moshi)"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lretrofit2/Converter$Factory;

    invoke-direct {p0, p3, p1, v0}, Lco/uk/getmondo/api/c;->a(Ljava/lang/String;Lokhttp3/OkHttpClient;Lretrofit2/Converter$Factory;)Lretrofit2/Retrofit;

    move-result-object v0

    .line 233
    const-class v1, Lco/uk/getmondo/api/SignupApi;

    invoke-virtual {v0, v1}, Lretrofit2/Retrofit;->create(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    const-string v1, "buildRetrofit(apiUrl, ok\u2026te(SignupApi::class.java)"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lco/uk/getmondo/api/SignupApi;

    return-object v0
.end method

.method public final d()Lokhttp3/logging/HttpLoggingInterceptor;
    .locals 2

    .prologue
    .line 325
    new-instance v0, Lokhttp3/logging/HttpLoggingInterceptor;

    invoke-direct {v0}, Lokhttp3/logging/HttpLoggingInterceptor;-><init>()V

    .line 326
    sget-object v1, Lokhttp3/logging/HttpLoggingInterceptor$Level;->NONE:Lokhttp3/logging/HttpLoggingInterceptor$Level;

    invoke-virtual {v0, v1}, Lokhttp3/logging/HttpLoggingInterceptor;->setLevel(Lokhttp3/logging/HttpLoggingInterceptor$Level;)Lokhttp3/logging/HttpLoggingInterceptor;

    .line 327
    return-object v0
.end method

.method public final e(Lokhttp3/OkHttpClient;Lcom/squareup/moshi/v;Ljava/lang/String;)Lco/uk/getmondo/api/MigrationApi;
    .locals 2

    .prologue
    const-string v0, "okHttpClient"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "moshi"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "apiUrl"

    invoke-static {p3, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 242
    invoke-static {p2}, Lretrofit2/converter/moshi/MoshiConverterFactory;->create(Lcom/squareup/moshi/v;)Lretrofit2/converter/moshi/MoshiConverterFactory;

    move-result-object v0

    const-string v1, "MoshiConverterFactory.create(moshi)"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lretrofit2/Converter$Factory;

    invoke-direct {p0, p3, p1, v0}, Lco/uk/getmondo/api/c;->a(Ljava/lang/String;Lokhttp3/OkHttpClient;Lretrofit2/Converter$Factory;)Lretrofit2/Retrofit;

    move-result-object v0

    .line 243
    const-class v1, Lco/uk/getmondo/api/MigrationApi;

    invoke-virtual {v0, v1}, Lretrofit2/Retrofit;->create(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    const-string v1, "buildRetrofit(apiUrl, ok\u2026MigrationApi::class.java)"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lco/uk/getmondo/api/MigrationApi;

    return-object v0
.end method

.method public final f(Lokhttp3/OkHttpClient;Lcom/squareup/moshi/v;Ljava/lang/String;)Lco/uk/getmondo/api/OverdraftApi;
    .locals 2

    .prologue
    const-string v0, "okHttpClient"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "moshi"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "apiUrl"

    invoke-static {p3, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 252
    invoke-static {p2}, Lretrofit2/converter/moshi/MoshiConverterFactory;->create(Lcom/squareup/moshi/v;)Lretrofit2/converter/moshi/MoshiConverterFactory;

    move-result-object v0

    const-string v1, "MoshiConverterFactory.create(moshi)"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lretrofit2/Converter$Factory;

    invoke-direct {p0, p3, p1, v0}, Lco/uk/getmondo/api/c;->a(Ljava/lang/String;Lokhttp3/OkHttpClient;Lretrofit2/Converter$Factory;)Lretrofit2/Retrofit;

    move-result-object v0

    .line 253
    const-class v1, Lco/uk/getmondo/api/OverdraftApi;

    invoke-virtual {v0, v1}, Lretrofit2/Retrofit;->create(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    const-string v1, "buildRetrofit(apiUrl, ok\u2026OverdraftApi::class.java)"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lco/uk/getmondo/api/OverdraftApi;

    return-object v0
.end method

.method public final g(Lokhttp3/OkHttpClient;Lcom/squareup/moshi/v;Ljava/lang/String;)Lco/uk/getmondo/api/PaymentLimitsApi;
    .locals 2

    .prologue
    const-string v0, "okHttpClient"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "moshi"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "apiUrl"

    invoke-static {p3, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 262
    invoke-static {p2}, Lretrofit2/converter/moshi/MoshiConverterFactory;->create(Lcom/squareup/moshi/v;)Lretrofit2/converter/moshi/MoshiConverterFactory;

    move-result-object v0

    const-string v1, "MoshiConverterFactory.create(moshi)"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lretrofit2/Converter$Factory;

    invoke-direct {p0, p3, p1, v0}, Lco/uk/getmondo/api/c;->a(Ljava/lang/String;Lokhttp3/OkHttpClient;Lretrofit2/Converter$Factory;)Lretrofit2/Retrofit;

    move-result-object v0

    .line 263
    const-class v1, Lco/uk/getmondo/api/PaymentLimitsApi;

    invoke-virtual {v0, v1}, Lretrofit2/Retrofit;->create(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    const-string v1, "buildRetrofit(apiUrl, ok\u2026entLimitsApi::class.java)"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lco/uk/getmondo/api/PaymentLimitsApi;

    return-object v0
.end method
