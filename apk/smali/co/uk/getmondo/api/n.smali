.class public final Lco/uk/getmondo/api/n;
.super Ljava/lang/Object;
.source "ApiModule_ProvideMonzoAnalyticsApi$app_monzoPrepaidReleaseFactory.java"

# interfaces
.implements Lb/a/b;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lb/a/b",
        "<",
        "Lco/uk/getmondo/api/AnalyticsApi;",
        ">;"
    }
.end annotation


# static fields
.field static final synthetic a:Z


# instance fields
.field private final b:Lco/uk/getmondo/api/c;

.field private final c:Ljavax/a/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/a/a",
            "<",
            "Lokhttp3/OkHttpClient;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Ljavax/a/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/a/a",
            "<",
            "Lcom/google/gson/f;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Ljavax/a/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/api/authentication/b;",
            ">;"
        }
    .end annotation
.end field

.field private final f:Ljavax/a/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/api/ac;",
            ">;"
        }
    .end annotation
.end field

.field private final g:Ljavax/a/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/api/authentication/k;",
            ">;"
        }
    .end annotation
.end field

.field private final h:Ljavax/a/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/a/a",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 12
    const-class v0, Lco/uk/getmondo/api/n;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lco/uk/getmondo/api/n;->a:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lco/uk/getmondo/api/c;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lco/uk/getmondo/api/c;",
            "Ljavax/a/a",
            "<",
            "Lokhttp3/OkHttpClient;",
            ">;",
            "Ljavax/a/a",
            "<",
            "Lcom/google/gson/f;",
            ">;",
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/api/authentication/b;",
            ">;",
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/api/ac;",
            ">;",
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/api/authentication/k;",
            ">;",
            "Ljavax/a/a",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    sget-boolean v0, Lco/uk/getmondo/api/n;->a:Z

    if-nez v0, :cond_0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 41
    :cond_0
    iput-object p1, p0, Lco/uk/getmondo/api/n;->b:Lco/uk/getmondo/api/c;

    .line 42
    sget-boolean v0, Lco/uk/getmondo/api/n;->a:Z

    if-nez v0, :cond_1

    if-nez p2, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 43
    :cond_1
    iput-object p2, p0, Lco/uk/getmondo/api/n;->c:Ljavax/a/a;

    .line 44
    sget-boolean v0, Lco/uk/getmondo/api/n;->a:Z

    if-nez v0, :cond_2

    if-nez p3, :cond_2

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 45
    :cond_2
    iput-object p3, p0, Lco/uk/getmondo/api/n;->d:Ljavax/a/a;

    .line 46
    sget-boolean v0, Lco/uk/getmondo/api/n;->a:Z

    if-nez v0, :cond_3

    if-nez p4, :cond_3

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 47
    :cond_3
    iput-object p4, p0, Lco/uk/getmondo/api/n;->e:Ljavax/a/a;

    .line 48
    sget-boolean v0, Lco/uk/getmondo/api/n;->a:Z

    if-nez v0, :cond_4

    if-nez p5, :cond_4

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 49
    :cond_4
    iput-object p5, p0, Lco/uk/getmondo/api/n;->f:Ljavax/a/a;

    .line 50
    sget-boolean v0, Lco/uk/getmondo/api/n;->a:Z

    if-nez v0, :cond_5

    if-nez p6, :cond_5

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 51
    :cond_5
    iput-object p6, p0, Lco/uk/getmondo/api/n;->g:Ljavax/a/a;

    .line 52
    sget-boolean v0, Lco/uk/getmondo/api/n;->a:Z

    if-nez v0, :cond_6

    if-nez p7, :cond_6

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 53
    :cond_6
    iput-object p7, p0, Lco/uk/getmondo/api/n;->h:Ljavax/a/a;

    .line 54
    return-void
.end method

.method public static a(Lco/uk/getmondo/api/c;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;)Lb/a/b;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lco/uk/getmondo/api/c;",
            "Ljavax/a/a",
            "<",
            "Lokhttp3/OkHttpClient;",
            ">;",
            "Ljavax/a/a",
            "<",
            "Lcom/google/gson/f;",
            ">;",
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/api/authentication/b;",
            ">;",
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/api/ac;",
            ">;",
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/api/authentication/k;",
            ">;",
            "Ljavax/a/a",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lb/a/b",
            "<",
            "Lco/uk/getmondo/api/AnalyticsApi;",
            ">;"
        }
    .end annotation

    .prologue
    .line 77
    new-instance v0, Lco/uk/getmondo/api/n;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object v7, p6

    invoke-direct/range {v0 .. v7}, Lco/uk/getmondo/api/n;-><init>(Lco/uk/getmondo/api/c;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;)V

    return-object v0
.end method


# virtual methods
.method public a()Lco/uk/getmondo/api/AnalyticsApi;
    .locals 7

    .prologue
    .line 58
    iget-object v0, p0, Lco/uk/getmondo/api/n;->b:Lco/uk/getmondo/api/c;

    iget-object v1, p0, Lco/uk/getmondo/api/n;->c:Ljavax/a/a;

    .line 60
    invoke-interface {v1}, Ljavax/a/a;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lokhttp3/OkHttpClient;

    iget-object v2, p0, Lco/uk/getmondo/api/n;->d:Ljavax/a/a;

    .line 61
    invoke-interface {v2}, Ljavax/a/a;->b()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/gson/f;

    iget-object v3, p0, Lco/uk/getmondo/api/n;->e:Ljavax/a/a;

    .line 62
    invoke-interface {v3}, Ljavax/a/a;->b()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lco/uk/getmondo/api/authentication/b;

    iget-object v4, p0, Lco/uk/getmondo/api/n;->f:Ljavax/a/a;

    .line 63
    invoke-interface {v4}, Ljavax/a/a;->b()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lco/uk/getmondo/api/ac;

    iget-object v5, p0, Lco/uk/getmondo/api/n;->g:Ljavax/a/a;

    .line 64
    invoke-interface {v5}, Ljavax/a/a;->b()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lco/uk/getmondo/api/authentication/k;

    iget-object v6, p0, Lco/uk/getmondo/api/n;->h:Ljavax/a/a;

    .line 65
    invoke-interface {v6}, Ljavax/a/a;->b()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    .line 59
    invoke-virtual/range {v0 .. v6}, Lco/uk/getmondo/api/c;->a(Lokhttp3/OkHttpClient;Lcom/google/gson/f;Lco/uk/getmondo/api/authentication/b;Lco/uk/getmondo/api/ac;Lco/uk/getmondo/api/authentication/k;Ljava/lang/String;)Lco/uk/getmondo/api/AnalyticsApi;

    move-result-object v0

    const-string v1, "Cannot return null from a non-@Nullable @Provides method"

    .line 58
    invoke-static {v0, v1}, Lb/a/d;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/api/AnalyticsApi;

    return-object v0
.end method

.method public synthetic b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 12
    invoke-virtual {p0}, Lco/uk/getmondo/api/n;->a()Lco/uk/getmondo/api/AnalyticsApi;

    move-result-object v0

    return-object v0
.end method
