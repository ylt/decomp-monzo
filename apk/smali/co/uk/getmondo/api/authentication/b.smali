.class public Lco/uk/getmondo/api/authentication/b;
.super Ljava/lang/Object;
.source "BearerAuthInterceptor.java"

# interfaces
.implements Lokhttp3/Interceptor;


# instance fields
.field private final a:Lco/uk/getmondo/api/authentication/d;

.field private final b:Lco/uk/getmondo/common/accounts/o;


# direct methods
.method constructor <init>(Lco/uk/getmondo/api/authentication/d;Lco/uk/getmondo/common/accounts/o;)V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    iput-object p1, p0, Lco/uk/getmondo/api/authentication/b;->a:Lco/uk/getmondo/api/authentication/d;

    .line 24
    iput-object p2, p0, Lco/uk/getmondo/api/authentication/b;->b:Lco/uk/getmondo/common/accounts/o;

    .line 25
    return-void
.end method

.method private a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lco/uk/getmondo/api/authentication/b;->b:Lco/uk/getmondo/common/accounts/o;

    invoke-virtual {v0}, Lco/uk/getmondo/common/accounts/o;->a()Ljava/lang/String;

    move-result-object v0

    .line 46
    if-eqz v0, :cond_0

    .line 52
    :goto_0
    return-object v0

    .line 51
    :cond_0
    iget-object v0, p0, Lco/uk/getmondo/api/authentication/b;->a:Lco/uk/getmondo/api/authentication/d;

    invoke-virtual {v0}, Lco/uk/getmondo/api/authentication/d;->a()Lio/reactivex/v;

    move-result-object v0

    invoke-virtual {v0}, Lio/reactivex/v;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/api/model/ApiToken;

    .line 52
    invoke-virtual {v0}, Lco/uk/getmondo/api/model/ApiToken;->a()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public intercept(Lokhttp3/Interceptor$Chain;)Lokhttp3/Response;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 29
    invoke-interface {p1}, Lokhttp3/Interceptor$Chain;->request()Lokhttp3/Request;

    move-result-object v0

    .line 32
    invoke-direct {p0}, Lco/uk/getmondo/api/authentication/b;->a()Ljava/lang/String;

    move-result-object v1

    .line 33
    if-nez v1, :cond_0

    .line 34
    invoke-interface {p1, v0}, Lokhttp3/Interceptor$Chain;->proceed(Lokhttp3/Request;)Lokhttp3/Response;

    move-result-object v0

    .line 40
    :goto_0
    return-object v0

    .line 37
    :cond_0
    invoke-virtual {v0}, Lokhttp3/Request;->newBuilder()Lokhttp3/Request$Builder;

    move-result-object v0

    const-string v2, "Authorization"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Bearer "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 38
    invoke-virtual {v0, v2, v1}, Lokhttp3/Request$Builder;->header(Ljava/lang/String;Ljava/lang/String;)Lokhttp3/Request$Builder;

    move-result-object v0

    .line 39
    invoke-virtual {v0}, Lokhttp3/Request$Builder;->build()Lokhttp3/Request;

    move-result-object v0

    .line 40
    invoke-interface {p1, v0}, Lokhttp3/Interceptor$Chain;->proceed(Lokhttp3/Request;)Lokhttp3/Response;

    move-result-object v0

    goto :goto_0
.end method
