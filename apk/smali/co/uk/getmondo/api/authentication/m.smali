.class public Lco/uk/getmondo/api/authentication/m;
.super Ljava/lang/Object;
.source "UserTokenRefresher.java"


# instance fields
.field private final a:Lco/uk/getmondo/api/authentication/MonzoOAuthApi;

.field private final b:Lco/uk/getmondo/common/accounts/o;


# direct methods
.method constructor <init>(Lco/uk/getmondo/api/authentication/MonzoOAuthApi;Lco/uk/getmondo/common/accounts/o;)V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    iput-object p1, p0, Lco/uk/getmondo/api/authentication/m;->a:Lco/uk/getmondo/api/authentication/MonzoOAuthApi;

    .line 26
    iput-object p2, p0, Lco/uk/getmondo/api/authentication/m;->b:Lco/uk/getmondo/common/accounts/o;

    .line 27
    return-void
.end method

.method public static a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 48
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Ljava/lang/Integer;Ljava/lang/Throwable;)Z
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 33
    invoke-virtual {p0}, Ljava/lang/Integer;->intValue()I

    move-result v2

    const/4 v3, 0x3

    if-lt v2, v3, :cond_1

    move v0, v1

    .line 41
    :cond_0
    :goto_0
    return v0

    .line 37
    :cond_1
    instance-of v2, p1, Lco/uk/getmondo/api/ApiException;

    if-eqz v2, :cond_0

    .line 38
    check-cast p1, Lco/uk/getmondo/api/ApiException;

    invoke-virtual {p1}, Lco/uk/getmondo/api/ApiException;->b()I

    move-result v2

    .line 39
    const/16 v3, 0x191

    if-eq v2, v3, :cond_2

    const/16 v3, 0x193

    if-ne v2, v3, :cond_0

    :cond_2
    move v0, v1

    goto :goto_0
.end method


# virtual methods
.method a(Lco/uk/getmondo/d/ai;)Lio/reactivex/v;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lco/uk/getmondo/d/ai;",
            ")",
            "Lio/reactivex/v",
            "<",
            "Lco/uk/getmondo/d/ai;",
            ">;"
        }
    .end annotation

    .prologue
    .line 30
    iget-object v0, p0, Lco/uk/getmondo/api/authentication/m;->a:Lco/uk/getmondo/api/authentication/MonzoOAuthApi;

    sget-object v1, Lco/uk/getmondo/api/authentication/MonzoOAuthApi;->a:Ljava/lang/String;

    const-string v2, "refresh_token"

    invoke-virtual {p1}, Lco/uk/getmondo/d/ai;->c()Lco/uk/getmondo/d/ai$a;

    move-result-object v3

    invoke-virtual {v3}, Lco/uk/getmondo/d/ai$a;->a()Ljava/lang/String;

    move-result-object v3

    .line 31
    invoke-virtual {p1}, Lco/uk/getmondo/d/ai;->c()Lco/uk/getmondo/d/ai$a;

    move-result-object v4

    invoke-virtual {v4}, Lco/uk/getmondo/d/ai$a;->b()Ljava/lang/String;

    move-result-object v4

    .line 30
    invoke-interface {v0, v1, v2, v3, v4}, Lco/uk/getmondo/api/authentication/MonzoOAuthApi;->refreshToken(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lio/reactivex/v;

    move-result-object v0

    invoke-static {}, Lco/uk/getmondo/api/authentication/n;->a()Lio/reactivex/c/d;

    move-result-object v1

    .line 32
    invoke-virtual {v0, v1}, Lio/reactivex/v;->a(Lio/reactivex/c/d;)Lio/reactivex/v;

    move-result-object v0

    new-instance v1, Lco/uk/getmondo/d/a/s;

    invoke-direct {v1}, Lco/uk/getmondo/d/a/s;-><init>()V

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    invoke-static {v1}, Lco/uk/getmondo/api/authentication/o;->a(Lco/uk/getmondo/d/a/s;)Lio/reactivex/c/h;

    move-result-object v1

    .line 43
    invoke-virtual {v0, v1}, Lio/reactivex/v;->d(Lio/reactivex/c/h;)Lio/reactivex/v;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/api/authentication/m;->b:Lco/uk/getmondo/common/accounts/o;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    invoke-static {v1}, Lco/uk/getmondo/api/authentication/p;->a(Lco/uk/getmondo/common/accounts/o;)Lio/reactivex/c/g;

    move-result-object v1

    .line 44
    invoke-virtual {v0, v1}, Lio/reactivex/v;->c(Lio/reactivex/c/g;)Lio/reactivex/v;

    move-result-object v0

    .line 30
    return-object v0
.end method
