.class public Lco/uk/getmondo/api/authentication/i;
.super Ljava/lang/Object;
.source "ClientTokenStorage.java"


# instance fields
.field private final a:Lcom/google/gson/f;

.field private b:Landroid/content/SharedPreferences;


# direct methods
.method constructor <init>(Landroid/content/Context;Lcom/google/gson/f;)V
    .locals 2

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    iput-object p2, p0, Lco/uk/getmondo/api/authentication/i;->a:Lcom/google/gson/f;

    .line 29
    const-string v0, "client-storage"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/api/authentication/i;->b:Landroid/content/SharedPreferences;

    .line 30
    return-void
.end method


# virtual methods
.method a()Lco/uk/getmondo/api/model/ApiToken;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 38
    iget-object v1, p0, Lco/uk/getmondo/api/authentication/i;->b:Landroid/content/SharedPreferences;

    const-string v2, "KEY_TOKEN"

    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 39
    if-eqz v1, :cond_0

    .line 40
    iget-object v0, p0, Lco/uk/getmondo/api/authentication/i;->a:Lcom/google/gson/f;

    const-class v2, Lco/uk/getmondo/api/model/ApiToken;

    invoke-virtual {v0, v1, v2}, Lcom/google/gson/f;->a(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/api/model/ApiToken;

    .line 42
    :cond_0
    return-object v0
.end method

.method a(Lco/uk/getmondo/api/model/ApiToken;)V
    .locals 3

    .prologue
    .line 33
    iget-object v0, p0, Lco/uk/getmondo/api/authentication/i;->b:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "KEY_TOKEN"

    iget-object v2, p0, Lco/uk/getmondo/api/authentication/i;->a:Lcom/google/gson/f;

    invoke-virtual {v2, p1}, Lcom/google/gson/f;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 34
    return-void
.end method
