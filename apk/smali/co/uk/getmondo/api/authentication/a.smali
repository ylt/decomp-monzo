.class public final enum Lco/uk/getmondo/api/authentication/a;
.super Ljava/lang/Enum;
.source "AuthError.java"

# interfaces
.implements Lco/uk/getmondo/common/e/f;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lco/uk/getmondo/api/authentication/a;",
        ">;",
        "Lco/uk/getmondo/common/e/f;"
    }
.end annotation


# static fields
.field public static final enum a:Lco/uk/getmondo/api/authentication/a;

.field public static final enum b:Lco/uk/getmondo/api/authentication/a;

.field public static final enum c:Lco/uk/getmondo/api/authentication/a;

.field public static final enum d:Lco/uk/getmondo/api/authentication/a;

.field public static final enum e:Lco/uk/getmondo/api/authentication/a;

.field public static final enum f:Lco/uk/getmondo/api/authentication/a;

.field public static final enum g:Lco/uk/getmondo/api/authentication/a;

.field private static final synthetic i:[Lco/uk/getmondo/api/authentication/a;


# instance fields
.field private final h:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 13
    new-instance v0, Lco/uk/getmondo/api/authentication/a;

    const-string v1, "BAD_ACCESS_TOKEN_EVICTED"

    const-string v2, "unauthorized.bad_access_token.evicted"

    invoke-direct {v0, v1, v4, v2}, Lco/uk/getmondo/api/authentication/a;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lco/uk/getmondo/api/authentication/a;->a:Lco/uk/getmondo/api/authentication/a;

    .line 14
    new-instance v0, Lco/uk/getmondo/api/authentication/a;

    const-string v1, "BAD_ACCESS_TOKEN_EXPIRED"

    const-string v2, "unauthorized.bad_access_token.expired"

    invoke-direct {v0, v1, v5, v2}, Lco/uk/getmondo/api/authentication/a;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lco/uk/getmondo/api/authentication/a;->b:Lco/uk/getmondo/api/authentication/a;

    .line 15
    new-instance v0, Lco/uk/getmondo/api/authentication/a;

    const-string v1, "BAD_ACCESS_TOKEN"

    const-string v2, "unauthorized.bad_access_token"

    invoke-direct {v0, v1, v6, v2}, Lco/uk/getmondo/api/authentication/a;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lco/uk/getmondo/api/authentication/a;->c:Lco/uk/getmondo/api/authentication/a;

    .line 16
    new-instance v0, Lco/uk/getmondo/api/authentication/a;

    const-string v1, "BAD_REFRESH_TOKEN"

    const-string v2, "unauthorized.bad_refresh_token"

    invoke-direct {v0, v1, v7, v2}, Lco/uk/getmondo/api/authentication/a;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lco/uk/getmondo/api/authentication/a;->d:Lco/uk/getmondo/api/authentication/a;

    .line 17
    new-instance v0, Lco/uk/getmondo/api/authentication/a;

    const-string v1, "BAD_AUTHORIZATION_CODE"

    const-string v2, "unauthorized.bad_authorization_code"

    invoke-direct {v0, v1, v8, v2}, Lco/uk/getmondo/api/authentication/a;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lco/uk/getmondo/api/authentication/a;->e:Lco/uk/getmondo/api/authentication/a;

    .line 18
    new-instance v0, Lco/uk/getmondo/api/authentication/a;

    const-string v1, "UNAUTHORIZED"

    const/4 v2, 0x5

    const-string v3, "unauthorized"

    invoke-direct {v0, v1, v2, v3}, Lco/uk/getmondo/api/authentication/a;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lco/uk/getmondo/api/authentication/a;->f:Lco/uk/getmondo/api/authentication/a;

    .line 19
    new-instance v0, Lco/uk/getmondo/api/authentication/a;

    const-string v1, "FORBIDDEN"

    const/4 v2, 0x6

    const-string v3, "forbidden"

    invoke-direct {v0, v1, v2, v3}, Lco/uk/getmondo/api/authentication/a;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lco/uk/getmondo/api/authentication/a;->g:Lco/uk/getmondo/api/authentication/a;

    .line 11
    const/4 v0, 0x7

    new-array v0, v0, [Lco/uk/getmondo/api/authentication/a;

    sget-object v1, Lco/uk/getmondo/api/authentication/a;->a:Lco/uk/getmondo/api/authentication/a;

    aput-object v1, v0, v4

    sget-object v1, Lco/uk/getmondo/api/authentication/a;->b:Lco/uk/getmondo/api/authentication/a;

    aput-object v1, v0, v5

    sget-object v1, Lco/uk/getmondo/api/authentication/a;->c:Lco/uk/getmondo/api/authentication/a;

    aput-object v1, v0, v6

    sget-object v1, Lco/uk/getmondo/api/authentication/a;->d:Lco/uk/getmondo/api/authentication/a;

    aput-object v1, v0, v7

    sget-object v1, Lco/uk/getmondo/api/authentication/a;->e:Lco/uk/getmondo/api/authentication/a;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lco/uk/getmondo/api/authentication/a;->f:Lco/uk/getmondo/api/authentication/a;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lco/uk/getmondo/api/authentication/a;->g:Lco/uk/getmondo/api/authentication/a;

    aput-object v2, v0, v1

    sput-object v0, Lco/uk/getmondo/api/authentication/a;->i:[Lco/uk/getmondo/api/authentication/a;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 23
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 24
    iput-object p3, p0, Lco/uk/getmondo/api/authentication/a;->h:Ljava/lang/String;

    .line 25
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lco/uk/getmondo/api/authentication/a;
    .locals 1

    .prologue
    .line 11
    const-class v0, Lco/uk/getmondo/api/authentication/a;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/api/authentication/a;

    return-object v0
.end method

.method public static values()[Lco/uk/getmondo/api/authentication/a;
    .locals 1

    .prologue
    .line 11
    sget-object v0, Lco/uk/getmondo/api/authentication/a;->i:[Lco/uk/getmondo/api/authentication/a;

    invoke-virtual {v0}, [Lco/uk/getmondo/api/authentication/a;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lco/uk/getmondo/api/authentication/a;

    return-object v0
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lco/uk/getmondo/api/authentication/a;->h:Ljava/lang/String;

    return-object v0
.end method

.method public b()Z
    .locals 1

    .prologue
    .line 34
    sget-object v0, Lco/uk/getmondo/api/authentication/a;->a:Lco/uk/getmondo/api/authentication/a;

    if-eq p0, v0, :cond_0

    sget-object v0, Lco/uk/getmondo/api/authentication/a;->d:Lco/uk/getmondo/api/authentication/a;

    if-ne p0, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
