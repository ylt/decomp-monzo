.class public Lco/uk/getmondo/api/authentication/k;
.super Ljava/lang/Object;
.source "MonzoAuthenticator.java"

# interfaces
.implements Lokhttp3/Authenticator;


# static fields
.field static final a:J


# instance fields
.field private final b:Lco/uk/getmondo/common/accounts/o;

.field private final c:Lco/uk/getmondo/api/authentication/d;

.field private final d:Lcom/google/gson/f;

.field private final e:Lco/uk/getmondo/api/authentication/m;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 32
    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x1e

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Lco/uk/getmondo/api/authentication/k;->a:J

    return-void
.end method

.method constructor <init>(Lco/uk/getmondo/common/accounts/o;Lco/uk/getmondo/api/authentication/d;Lcom/google/gson/f;Lco/uk/getmondo/api/authentication/m;)V
    .locals 0

    .prologue
    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    iput-object p1, p0, Lco/uk/getmondo/api/authentication/k;->b:Lco/uk/getmondo/common/accounts/o;

    .line 46
    iput-object p2, p0, Lco/uk/getmondo/api/authentication/k;->c:Lco/uk/getmondo/api/authentication/d;

    .line 47
    iput-object p3, p0, Lco/uk/getmondo/api/authentication/k;->d:Lcom/google/gson/f;

    .line 48
    iput-object p4, p0, Lco/uk/getmondo/api/authentication/k;->e:Lco/uk/getmondo/api/authentication/m;

    .line 49
    return-void
.end method

.method private a(Lokhttp3/Request;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 159
    const-string v0, "Authorization"

    invoke-virtual {p1, v0}, Lokhttp3/Request;->header(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 160
    if-eqz v0, :cond_0

    const-string v1, "Bearer "

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 161
    const-string v1, "Bearer "

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    .line 163
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Lokhttp3/Response;Ljava/lang/String;)Lokhttp3/Request;
    .locals 4

    .prologue
    .line 113
    iget-object v0, p0, Lco/uk/getmondo/api/authentication/k;->c:Lco/uk/getmondo/api/authentication/d;

    invoke-virtual {v0}, Lco/uk/getmondo/api/authentication/d;->a()Lio/reactivex/v;

    move-result-object v0

    invoke-virtual {v0}, Lio/reactivex/v;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/api/model/ApiToken;

    .line 114
    invoke-virtual {v0}, Lco/uk/getmondo/api/model/ApiToken;->a()Ljava/lang/String;

    move-result-object v1

    .line 117
    invoke-virtual {v0}, Lco/uk/getmondo/api/model/ApiToken;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 118
    iget-object v0, p0, Lco/uk/getmondo/api/authentication/k;->c:Lco/uk/getmondo/api/authentication/d;

    invoke-virtual {v0}, Lco/uk/getmondo/api/authentication/d;->b()Lio/reactivex/v;

    move-result-object v0

    invoke-virtual {v0}, Lio/reactivex/v;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/api/model/ApiToken;

    invoke-virtual {v0}, Lco/uk/getmondo/api/model/ApiToken;->a()Ljava/lang/String;

    move-result-object v0

    .line 121
    :goto_0
    const-string v1, "Proceeding with new client token %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object v0, v2, v3

    invoke-static {v1, v2}, Ld/a/a;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 122
    invoke-direct {p0, p1, v0}, Lco/uk/getmondo/api/authentication/k;->b(Lokhttp3/Response;Ljava/lang/String;)Lokhttp3/Request;

    move-result-object v0

    return-object v0

    :cond_0
    move-object v0, v1

    goto :goto_0
.end method

.method private a(Lokhttp3/Response;Ljava/lang/String;Lco/uk/getmondo/d/ai;)Lokhttp3/Request;
    .locals 8

    .prologue
    const/4 v0, 0x0

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 126
    if-nez p3, :cond_0

    .line 127
    const-string v1, "Attempted to refresh user token, but there isn\'t an existing user token saved"

    new-array v2, v6, [Ljava/lang/Object;

    invoke-static {v1, v2}, Ld/a/a;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 148
    :goto_0
    return-object v0

    .line 132
    :cond_0
    invoke-virtual {p3}, Lco/uk/getmondo/d/ai;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 134
    invoke-virtual {p3}, Lco/uk/getmondo/d/ai;->b()Ljava/lang/String;

    move-result-object v0

    .line 147
    :goto_1
    const-string v1, "Proceeding with new user token %s"

    new-array v2, v7, [Ljava/lang/Object;

    aput-object v0, v2, v6

    invoke-static {v1, v2}, Ld/a/a;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 148
    invoke-direct {p0, p1, v0}, Lco/uk/getmondo/api/authentication/k;->b(Lokhttp3/Response;Ljava/lang/String;)Lokhttp3/Request;

    move-result-object v0

    goto :goto_0

    .line 137
    :cond_1
    invoke-virtual {p3}, Lco/uk/getmondo/d/ai;->a()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    .line 138
    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    sget-wide v4, Lco/uk/getmondo/api/authentication/k;->a:J

    cmp-long v1, v2, v4

    if-gez v1, :cond_2

    .line 139
    const-string v1, "Illegal refresh. User token has attempted to be refreshed within less than %d seconds of its creation"

    new-array v2, v7, [Ljava/lang/Object;

    sget-object v3, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    sget-wide v4, Lco/uk/getmondo/api/authentication/k;->a:J

    .line 140
    invoke-virtual {v3, v4, v5}, Ljava/util/concurrent/TimeUnit;->toSeconds(J)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v2, v6

    .line 139
    invoke-static {v1, v2}, Ld/a/a;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 144
    :cond_2
    iget-object v0, p0, Lco/uk/getmondo/api/authentication/k;->e:Lco/uk/getmondo/api/authentication/m;

    invoke-virtual {v0, p3}, Lco/uk/getmondo/api/authentication/m;->a(Lco/uk/getmondo/d/ai;)Lio/reactivex/v;

    move-result-object v0

    invoke-virtual {v0}, Lio/reactivex/v;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/d/ai;

    invoke-virtual {v0}, Lco/uk/getmondo/d/ai;->b()Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method

.method private b(Lokhttp3/Response;Ljava/lang/String;)Lokhttp3/Request;
    .locals 4

    .prologue
    .line 152
    invoke-virtual {p1}, Lokhttp3/Response;->request()Lokhttp3/Request;

    move-result-object v0

    invoke-virtual {v0}, Lokhttp3/Request;->newBuilder()Lokhttp3/Request$Builder;

    move-result-object v0

    const-string v1, "Authorization"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Bearer "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 153
    invoke-virtual {v0, v1, v2}, Lokhttp3/Request$Builder;->header(Ljava/lang/String;Ljava/lang/String;)Lokhttp3/Request$Builder;

    move-result-object v0

    .line 154
    invoke-virtual {v0}, Lokhttp3/Request$Builder;->build()Lokhttp3/Request;

    move-result-object v0

    .line 152
    return-object v0
.end method


# virtual methods
.method public declared-synchronized authenticate(Lokhttp3/Route;Lokhttp3/Response;)Lokhttp3/Request;
    .locals 11
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v0, 0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 54
    monitor-enter p0

    :try_start_0
    iget-object v3, p0, Lco/uk/getmondo/api/authentication/k;->b:Lco/uk/getmondo/common/accounts/o;

    invoke-virtual {v3}, Lco/uk/getmondo/common/accounts/o;->b()Lco/uk/getmondo/d/ai;

    move-result-object v6

    .line 55
    invoke-virtual {p2}, Lokhttp3/Response;->request()Lokhttp3/Request;

    move-result-object v3

    invoke-direct {p0, v3}, Lco/uk/getmondo/api/authentication/k;->a(Lokhttp3/Request;)Ljava/lang/String;

    move-result-object v7

    .line 56
    if-eqz v7, :cond_1

    iget-object v3, p0, Lco/uk/getmondo/api/authentication/k;->c:Lco/uk/getmondo/api/authentication/d;

    invoke-virtual {v3, v7}, Lco/uk/getmondo/api/authentication/d;->a(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    move v5, v0

    .line 57
    :goto_0
    if-eqz v7, :cond_2

    if-nez v5, :cond_2

    move v4, v0

    .line 59
    :goto_1
    const-string v1, "Request failed with 401 - tokenFromRequest:%s requestWasMadeWithClientToken:%s existingUserToken:%s"

    const/4 v0, 0x3

    new-array v3, v0, [Ljava/lang/Object;

    const/4 v0, 0x0

    aput-object v7, v3, v0

    const/4 v0, 0x1

    .line 60
    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v8

    aput-object v8, v3, v0

    const/4 v8, 0x2

    if-eqz v6, :cond_3

    invoke-virtual {v6}, Lco/uk/getmondo/d/ai;->b()Ljava/lang/String;

    move-result-object v0

    :goto_2
    aput-object v0, v3, v8

    .line 59
    invoke-static {v1, v3}, Ld/a/a;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 62
    if-eqz v6, :cond_4

    invoke-virtual {v6}, Lco/uk/getmondo/d/ai;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 64
    invoke-virtual {v6}, Lco/uk/getmondo/d/ai;->b()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p2, v0}, Lco/uk/getmondo/api/authentication/k;->b(Lokhttp3/Response;Ljava/lang/String;)Lokhttp3/Request;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 108
    :cond_0
    :goto_3
    monitor-exit p0

    return-object v0

    :cond_1
    move v5, v1

    .line 56
    goto :goto_0

    :cond_2
    move v4, v1

    .line 57
    goto :goto_1

    :cond_3
    move-object v0, v2

    .line 60
    goto :goto_2

    .line 65
    :cond_4
    if-nez v7, :cond_5

    .line 67
    const/4 v0, 0x0

    :try_start_1
    invoke-direct {p0, p2, v0}, Lco/uk/getmondo/api/authentication/k;->a(Lokhttp3/Response;Ljava/lang/String;)Lokhttp3/Request;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    goto :goto_3

    .line 74
    :cond_5
    const-wide/32 v0, 0x100000

    :try_start_2
    invoke-virtual {p2, v0, v1}, Lokhttp3/Response;->peekBody(J)Lokhttp3/ResponseBody;
    :try_end_2
    .catch Lcom/google/gson/JsonIOException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Lcom/google/gson/JsonSyntaxException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v3

    .line 75
    :try_start_3
    iget-object v0, p0, Lco/uk/getmondo/api/authentication/k;->d:Lcom/google/gson/f;

    invoke-virtual {v3}, Lokhttp3/ResponseBody;->charStream()Ljava/io/Reader;

    move-result-object v1

    const-class v8, Lco/uk/getmondo/api/model/b;

    invoke-virtual {v0, v1, v8}, Lcom/google/gson/f;->a(Ljava/io/Reader;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/api/model/b;

    .line 76
    invoke-static {}, Lco/uk/getmondo/api/authentication/a;->values()[Lco/uk/getmondo/api/authentication/a;

    move-result-object v1

    invoke-virtual {v0}, Lco/uk/getmondo/api/model/b;->a()Ljava/lang/String;

    move-result-object v8

    invoke-static {v1, v8}, Lco/uk/getmondo/common/e/d;->a([Lco/uk/getmondo/common/e/f;Ljava/lang/String;)Lco/uk/getmondo/common/e/f;

    move-result-object v1

    check-cast v1, Lco/uk/getmondo/api/authentication/a;

    .line 77
    if-nez v1, :cond_7

    .line 78
    const-string v1, "Unknown auth error code in 401 response, code=%s"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-virtual {v0}, Lco/uk/getmondo/api/model/b;->a()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v5

    invoke-static {v1, v4}, Ld/a/a;->d(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_3
    .catch Lcom/google/gson/JsonIOException; {:try_start_3 .. :try_end_3} :catch_2
    .catch Lcom/google/gson/JsonSyntaxException; {:try_start_3 .. :try_end_3} :catch_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    .line 106
    if-eqz v3, :cond_6

    :try_start_4
    invoke-virtual {v3}, Lokhttp3/ResponseBody;->close()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :cond_6
    move-object v0, v2

    .line 79
    goto :goto_3

    .line 82
    :cond_7
    :try_start_5
    const-string v0, "401 Error code is %s"

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    invoke-virtual {v1}, Lco/uk/getmondo/api/authentication/a;->a()Ljava/lang/String;

    move-result-object v10

    aput-object v10, v8, v9

    invoke-static {v0, v8}, Ld/a/a;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 83
    sget-object v0, Lco/uk/getmondo/api/authentication/k$1;->a:[I

    invoke-virtual {v1}, Lco/uk/getmondo/api/authentication/a;->ordinal()I

    move-result v1

    aget v0, v0, v1
    :try_end_5
    .catch Lcom/google/gson/JsonIOException; {:try_start_5 .. :try_end_5} :catch_2
    .catch Lcom/google/gson/JsonSyntaxException; {:try_start_5 .. :try_end_5} :catch_3
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    packed-switch v0, :pswitch_data_0

    .line 106
    if-eqz v3, :cond_8

    :try_start_6
    invoke-virtual {v3}, Lokhttp3/ResponseBody;->close()V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    :cond_8
    :goto_4
    move-object v0, v2

    .line 108
    goto :goto_3

    .line 85
    :pswitch_0
    if-eqz v5, :cond_9

    .line 87
    :try_start_7
    invoke-direct {p0, p2, v7}, Lco/uk/getmondo/api/authentication/k;->a(Lokhttp3/Response;Ljava/lang/String;)Lokhttp3/Request;
    :try_end_7
    .catch Lcom/google/gson/JsonIOException; {:try_start_7 .. :try_end_7} :catch_2
    .catch Lcom/google/gson/JsonSyntaxException; {:try_start_7 .. :try_end_7} :catch_3
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    move-result-object v0

    .line 106
    if-eqz v3, :cond_0

    :try_start_8
    invoke-virtual {v3}, Lokhttp3/ResponseBody;->close()V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    goto :goto_3

    .line 54
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 106
    :cond_9
    if-eqz v3, :cond_a

    :try_start_9
    invoke-virtual {v3}, Lokhttp3/ResponseBody;->close()V
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    :cond_a
    move-object v0, v2

    .line 91
    goto/16 :goto_3

    .line 94
    :pswitch_1
    if-eqz v4, :cond_b

    .line 96
    :try_start_a
    invoke-direct {p0, p2, v7, v6}, Lco/uk/getmondo/api/authentication/k;->a(Lokhttp3/Response;Ljava/lang/String;Lco/uk/getmondo/d/ai;)Lokhttp3/Request;
    :try_end_a
    .catch Lcom/google/gson/JsonIOException; {:try_start_a .. :try_end_a} :catch_2
    .catch Lcom/google/gson/JsonSyntaxException; {:try_start_a .. :try_end_a} :catch_3
    .catchall {:try_start_a .. :try_end_a} :catchall_2

    move-result-object v0

    .line 106
    if-eqz v3, :cond_0

    :try_start_b
    invoke-virtual {v3}, Lokhttp3/ResponseBody;->close()V
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    goto/16 :goto_3

    .line 99
    :cond_b
    :try_start_c
    invoke-direct {p0, p2, v7}, Lco/uk/getmondo/api/authentication/k;->a(Lokhttp3/Response;Ljava/lang/String;)Lokhttp3/Request;
    :try_end_c
    .catch Lcom/google/gson/JsonIOException; {:try_start_c .. :try_end_c} :catch_2
    .catch Lcom/google/gson/JsonSyntaxException; {:try_start_c .. :try_end_c} :catch_3
    .catchall {:try_start_c .. :try_end_c} :catchall_2

    move-result-object v0

    .line 106
    if-eqz v3, :cond_0

    :try_start_d
    invoke-virtual {v3}, Lokhttp3/ResponseBody;->close()V
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_0

    goto/16 :goto_3

    .line 102
    :catch_0
    move-exception v0

    move-object v1, v2

    :goto_5
    move-object v3, v1

    .line 104
    :goto_6
    :try_start_e
    const-string v1, "Error parsing auth error response"

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v0, v1, v4}, Ld/a/a;->a(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_e
    .catchall {:try_start_e .. :try_end_e} :catchall_2

    .line 106
    if-eqz v3, :cond_8

    :try_start_f
    invoke-virtual {v3}, Lokhttp3/ResponseBody;->close()V

    goto :goto_4

    :catchall_1
    move-exception v0

    move-object v3, v2

    :goto_7
    if-eqz v3, :cond_c

    invoke-virtual {v3}, Lokhttp3/ResponseBody;->close()V

    :cond_c
    throw v0
    :try_end_f
    .catchall {:try_start_f .. :try_end_f} :catchall_0

    :catchall_2
    move-exception v0

    goto :goto_7

    .line 102
    :catch_1
    move-exception v0

    move-object v3, v2

    goto :goto_6

    :catch_2
    move-exception v0

    goto :goto_6

    :catch_3
    move-exception v0

    move-object v1, v3

    goto :goto_5

    .line 83
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method
