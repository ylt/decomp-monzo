.class public Lco/uk/getmondo/api/authentication/d;
.super Ljava/lang/Object;
.source "ClientTokenManager.java"


# instance fields
.field private final a:Lco/uk/getmondo/api/authentication/i;

.field private final b:Lco/uk/getmondo/api/authentication/MonzoOAuthApi;

.field private final c:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lco/uk/getmondo/api/authentication/i;Lco/uk/getmondo/api/authentication/MonzoOAuthApi;)V
    .locals 1

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    invoke-static {v0}, Ljava/util/Collections;->synchronizedSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/api/authentication/d;->c:Ljava/util/Set;

    .line 26
    iput-object p1, p0, Lco/uk/getmondo/api/authentication/d;->a:Lco/uk/getmondo/api/authentication/i;

    .line 27
    iput-object p2, p0, Lco/uk/getmondo/api/authentication/d;->b:Lco/uk/getmondo/api/authentication/MonzoOAuthApi;

    .line 28
    return-void
.end method

.method static synthetic a(Lco/uk/getmondo/api/authentication/d;)Lio/reactivex/z;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 35
    iget-object v0, p0, Lco/uk/getmondo/api/authentication/d;->a:Lco/uk/getmondo/api/authentication/i;

    invoke-virtual {v0}, Lco/uk/getmondo/api/authentication/i;->a()Lco/uk/getmondo/api/model/ApiToken;

    move-result-object v0

    .line 36
    if-nez v0, :cond_0

    .line 37
    invoke-virtual {p0}, Lco/uk/getmondo/api/authentication/d;->b()Lio/reactivex/v;

    move-result-object v0

    .line 39
    :goto_0
    return-object v0

    :cond_0
    invoke-static {v0}, Lio/reactivex/v;->a(Ljava/lang/Object;)Lio/reactivex/v;

    move-result-object v0

    goto :goto_0
.end method

.method static synthetic a(Lco/uk/getmondo/api/authentication/d;Lco/uk/getmondo/api/model/ApiToken;)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 49
    iget-object v0, p0, Lco/uk/getmondo/api/authentication/d;->a:Lco/uk/getmondo/api/authentication/i;

    invoke-virtual {v0, p1}, Lco/uk/getmondo/api/authentication/i;->a(Lco/uk/getmondo/api/model/ApiToken;)V

    .line 50
    const-string v0, "New client token created %s"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-virtual {p1}, Lco/uk/getmondo/api/model/ApiToken;->a()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ld/a/a;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 51
    return-void
.end method

.method static synthetic b(Lco/uk/getmondo/api/authentication/d;Lco/uk/getmondo/api/model/ApiToken;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 40
    iget-object v0, p0, Lco/uk/getmondo/api/authentication/d;->c:Ljava/util/Set;

    invoke-virtual {p1}, Lco/uk/getmondo/api/model/ApiToken;->a()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    return-void
.end method


# virtual methods
.method a()Lio/reactivex/v;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/v",
            "<",
            "Lco/uk/getmondo/api/model/ApiToken;",
            ">;"
        }
    .end annotation

    .prologue
    .line 34
    invoke-static {p0}, Lco/uk/getmondo/api/authentication/e;->a(Lco/uk/getmondo/api/authentication/d;)Ljava/util/concurrent/Callable;

    move-result-object v0

    invoke-static {v0}, Lio/reactivex/v;->a(Ljava/util/concurrent/Callable;)Lio/reactivex/v;

    move-result-object v0

    invoke-static {p0}, Lco/uk/getmondo/api/authentication/f;->a(Lco/uk/getmondo/api/authentication/d;)Lio/reactivex/c/g;

    move-result-object v1

    .line 40
    invoke-virtual {v0, v1}, Lio/reactivex/v;->c(Lio/reactivex/c/g;)Lio/reactivex/v;

    move-result-object v0

    .line 34
    return-object v0
.end method

.method a(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lco/uk/getmondo/api/authentication/d;->c:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method b()Lio/reactivex/v;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/v",
            "<",
            "Lco/uk/getmondo/api/model/ApiToken;",
            ">;"
        }
    .end annotation

    .prologue
    .line 47
    iget-object v0, p0, Lco/uk/getmondo/api/authentication/d;->b:Lco/uk/getmondo/api/authentication/MonzoOAuthApi;

    sget-object v1, Lco/uk/getmondo/api/authentication/MonzoOAuthApi;->a:Ljava/lang/String;

    const-string v2, "client_credentials"

    invoke-interface {v0, v1, v2}, Lco/uk/getmondo/api/authentication/MonzoOAuthApi;->requestClientToken(Ljava/lang/String;Ljava/lang/String;)Lio/reactivex/v;

    move-result-object v0

    invoke-static {p0}, Lco/uk/getmondo/api/authentication/g;->a(Lco/uk/getmondo/api/authentication/d;)Lio/reactivex/c/g;

    move-result-object v1

    .line 48
    invoke-virtual {v0, v1}, Lio/reactivex/v;->c(Lio/reactivex/c/g;)Lio/reactivex/v;

    move-result-object v0

    .line 47
    return-object v0
.end method
