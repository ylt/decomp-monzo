.class public final Lco/uk/getmondo/api/SignupApi$DefaultImpls;
.super Ljava/lang/Object;
.source "SignupApi.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lco/uk/getmondo/api/SignupApi;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "DefaultImpls"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# direct methods
.method public static synthetic submitProfile$default(Lco/uk/getmondo/api/SignupApi;Ljava/lang/String;Ljava/lang/String;Lorg/threeten/bp/LocalDate;ILjava/lang/Object;)Lio/reactivex/b;
    .locals 2
    .annotation runtime Lretrofit2/http/FormUrlEncoded;
    .end annotation

    .annotation runtime Lretrofit2/http/PUT;
        value = "signup/profile"
    .end annotation

    .prologue
    if-eqz p5, :cond_0

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Super calls with default arguments not supported in this target, function: submitProfile"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    and-int/lit8 v0, p4, 0x2

    if-eqz v0, :cond_1

    .line 43
    const/4 v0, 0x0

    check-cast v0, Ljava/lang/String;

    :goto_0
    invoke-interface {p0, p1, v0, p3}, Lco/uk/getmondo/api/SignupApi;->submitProfile(Ljava/lang/String;Ljava/lang/String;Lorg/threeten/bp/LocalDate;)Lio/reactivex/b;

    move-result-object v0

    return-object v0

    :cond_1
    move-object v0, p2

    goto :goto_0
.end method

.method public static synthetic submitProfileAddress$default(Lco/uk/getmondo/api/SignupApi;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/Object;)Lio/reactivex/b;
    .locals 6
    .annotation runtime Lretrofit2/http/FormUrlEncoded;
    .end annotation

    .annotation runtime Lretrofit2/http/PUT;
        value = "signup/profile/address"
    .end annotation

    .prologue
    if-eqz p7, :cond_0

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Super calls with default arguments not supported in this target, function: submitProfileAddress"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    and-int/lit8 v0, p6, 0x1

    if-eqz v0, :cond_1

    .line 50
    const/4 v0, 0x0

    check-cast v0, Ljava/lang/String;

    move-object v1, v0

    :goto_0
    move-object v0, p0

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-interface/range {v0 .. v5}, Lco/uk/getmondo/api/SignupApi;->submitProfileAddress(Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lio/reactivex/b;

    move-result-object v0

    return-object v0

    :cond_1
    move-object v1, p1

    goto :goto_0
.end method
