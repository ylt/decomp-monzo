.class public interface abstract Lco/uk/getmondo/api/AnalyticsApi;
.super Ljava/lang/Object;
.source "AnalyticsApi.java"


# virtual methods
.method public abstract trackEvent(Lco/uk/getmondo/api/model/tracking/Impression;)Lio/reactivex/b;
    .param p1    # Lco/uk/getmondo/api/model/tracking/Impression;
        .annotation runtime Lretrofit2/http/Body;
        .end annotation
    .end param
    .annotation runtime Lretrofit2/http/Headers;
        value = {
            "Content-Type: application/json"
        }
    .end annotation

    .annotation runtime Lretrofit2/http/POST;
        value = "analytics/track"
    .end annotation
.end method
