.class final synthetic Lco/uk/getmondo/api/af;
.super Ljava/lang/Object;

# interfaces
.implements Lio/reactivex/c/a;


# instance fields
.field private final a:Lco/uk/getmondo/api/ae;

.field private final b:Ljava/io/InputStream;

.field private final c:J

.field private final d:Ljava/lang/String;

.field private final e:Ljava/lang/String;


# direct methods
.method private constructor <init>(Lco/uk/getmondo/api/ae;Ljava/io/InputStream;JLjava/lang/String;Ljava/lang/String;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lco/uk/getmondo/api/af;->a:Lco/uk/getmondo/api/ae;

    iput-object p2, p0, Lco/uk/getmondo/api/af;->b:Ljava/io/InputStream;

    iput-wide p3, p0, Lco/uk/getmondo/api/af;->c:J

    iput-object p5, p0, Lco/uk/getmondo/api/af;->d:Ljava/lang/String;

    iput-object p6, p0, Lco/uk/getmondo/api/af;->e:Ljava/lang/String;

    return-void
.end method

.method public static a(Lco/uk/getmondo/api/ae;Ljava/io/InputStream;JLjava/lang/String;Ljava/lang/String;)Lio/reactivex/c/a;
    .locals 8

    new-instance v1, Lco/uk/getmondo/api/af;

    move-object v2, p0

    move-object v3, p1

    move-wide v4, p2

    move-object v6, p4

    move-object v7, p5

    invoke-direct/range {v1 .. v7}, Lco/uk/getmondo/api/af;-><init>(Lco/uk/getmondo/api/ae;Ljava/io/InputStream;JLjava/lang/String;Ljava/lang/String;)V

    return-object v1
.end method


# virtual methods
.method public a()V
    .locals 6

    iget-object v0, p0, Lco/uk/getmondo/api/af;->a:Lco/uk/getmondo/api/ae;

    iget-object v1, p0, Lco/uk/getmondo/api/af;->b:Ljava/io/InputStream;

    iget-wide v2, p0, Lco/uk/getmondo/api/af;->c:J

    iget-object v4, p0, Lco/uk/getmondo/api/af;->d:Ljava/lang/String;

    iget-object v5, p0, Lco/uk/getmondo/api/af;->e:Ljava/lang/String;

    invoke-static/range {v0 .. v5}, Lco/uk/getmondo/api/ae;->a(Lco/uk/getmondo/api/ae;Ljava/io/InputStream;JLjava/lang/String;Ljava/lang/String;)V

    return-void
.end method
