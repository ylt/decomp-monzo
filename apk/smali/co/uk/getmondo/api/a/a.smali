.class public Lco/uk/getmondo/api/a/a;
.super Ljava/lang/Object;
.source "WaitlistFieldMapHelper.java"


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Ljava/lang/String;

.field private final c:Ljava/lang/String;

.field private final d:Ljava/lang/String;

.field private final e:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    iput-object p1, p0, Lco/uk/getmondo/api/a/a;->a:Ljava/lang/String;

    .line 18
    iput-object p2, p0, Lco/uk/getmondo/api/a/a;->b:Ljava/lang/String;

    .line 19
    iput-object p3, p0, Lco/uk/getmondo/api/a/a;->c:Ljava/lang/String;

    .line 20
    iput-object p4, p0, Lco/uk/getmondo/api/a/a;->d:Ljava/lang/String;

    .line 21
    iput-object p5, p0, Lco/uk/getmondo/api/a/a;->e:Ljava/lang/String;

    .line 22
    return-void
.end method


# virtual methods
.method public a()Ljava/util/Map;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 25
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 26
    const-string v1, "email"

    iget-object v2, p0, Lco/uk/getmondo/api/a/a;->a:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 27
    const-string v1, "name"

    iget-object v2, p0, Lco/uk/getmondo/api/a/a;->b:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 28
    const-string v1, "country"

    iget-object v2, p0, Lco/uk/getmondo/api/a/a;->c:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 29
    const-string v1, "date_of_birth"

    iget-object v2, p0, Lco/uk/getmondo/api/a/a;->d:Ljava/lang/String;

    invoke-static {v2}, Lco/uk/getmondo/create_account/b;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 30
    iget-object v1, p0, Lco/uk/getmondo/api/a/a;->e:Ljava/lang/String;

    invoke-static {v1}, Lco/uk/getmondo/common/k/p;->c(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 31
    const-string v1, "postal_code"

    iget-object v2, p0, Lco/uk/getmondo/api/a/a;->e:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 33
    :cond_0
    const-string v1, "current_account"

    sget-object v2, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 34
    return-object v0
.end method
