.class public final Lco/uk/getmondo/api/l;
.super Ljava/lang/Object;
.source "ApiModule_ProvideIdentityVerificationApi$app_monzoPrepaidReleaseFactory.java"

# interfaces
.implements Lb/a/b;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lb/a/b",
        "<",
        "Lco/uk/getmondo/api/IdentityVerificationApi;",
        ">;"
    }
.end annotation


# static fields
.field static final synthetic a:Z


# instance fields
.field private final b:Lco/uk/getmondo/api/c;

.field private final c:Ljavax/a/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/a/a",
            "<",
            "Lretrofit2/Retrofit;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 9
    const-class v0, Lco/uk/getmondo/api/l;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lco/uk/getmondo/api/l;->a:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lco/uk/getmondo/api/c;Ljavax/a/a;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lco/uk/getmondo/api/c;",
            "Ljavax/a/a",
            "<",
            "Lretrofit2/Retrofit;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    sget-boolean v0, Lco/uk/getmondo/api/l;->a:Z

    if-nez v0, :cond_0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 22
    :cond_0
    iput-object p1, p0, Lco/uk/getmondo/api/l;->b:Lco/uk/getmondo/api/c;

    .line 23
    sget-boolean v0, Lco/uk/getmondo/api/l;->a:Z

    if-nez v0, :cond_1

    if-nez p2, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 24
    :cond_1
    iput-object p2, p0, Lco/uk/getmondo/api/l;->c:Ljavax/a/a;

    .line 25
    return-void
.end method

.method public static a(Lco/uk/getmondo/api/c;Ljavax/a/a;)Lb/a/b;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lco/uk/getmondo/api/c;",
            "Ljavax/a/a",
            "<",
            "Lretrofit2/Retrofit;",
            ">;)",
            "Lb/a/b",
            "<",
            "Lco/uk/getmondo/api/IdentityVerificationApi;",
            ">;"
        }
    .end annotation

    .prologue
    .line 36
    new-instance v0, Lco/uk/getmondo/api/l;

    invoke-direct {v0, p0, p1}, Lco/uk/getmondo/api/l;-><init>(Lco/uk/getmondo/api/c;Ljavax/a/a;)V

    return-object v0
.end method


# virtual methods
.method public a()Lco/uk/getmondo/api/IdentityVerificationApi;
    .locals 2

    .prologue
    .line 29
    iget-object v1, p0, Lco/uk/getmondo/api/l;->b:Lco/uk/getmondo/api/c;

    iget-object v0, p0, Lco/uk/getmondo/api/l;->c:Ljavax/a/a;

    .line 30
    invoke-interface {v0}, Ljavax/a/a;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lretrofit2/Retrofit;

    invoke-virtual {v1, v0}, Lco/uk/getmondo/api/c;->b(Lretrofit2/Retrofit;)Lco/uk/getmondo/api/IdentityVerificationApi;

    move-result-object v0

    const-string v1, "Cannot return null from a non-@Nullable @Provides method"

    .line 29
    invoke-static {v0, v1}, Lb/a/d;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/api/IdentityVerificationApi;

    return-object v0
.end method

.method public synthetic b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 9
    invoke-virtual {p0}, Lco/uk/getmondo/api/l;->a()Lco/uk/getmondo/api/IdentityVerificationApi;

    move-result-object v0

    return-object v0
.end method
