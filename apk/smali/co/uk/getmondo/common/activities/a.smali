.class public final Lco/uk/getmondo/common/activities/a;
.super Ljava/lang/Object;
.source "ActivityExt.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000\u001e\n\u0000\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u000b\n\u0000\u001a&\u0010\u0000\u001a\u00020\u0001*\u00020\u00022\u0006\u0010\u0003\u001a\u00020\u00042\u0008\u0008\u0003\u0010\u0005\u001a\u00020\u00062\u0008\u0008\u0002\u0010\u0007\u001a\u00020\u0008\u00a8\u0006\t"
    }
    d2 = {
        "launchCustomTab",
        "",
        "Landroid/app/Activity;",
        "url",
        "",
        "background",
        "",
        "showTitle",
        "",
        "app_monzoPrepaidRelease"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# direct methods
.method public static final a(Landroid/app/Activity;Ljava/lang/String;IZ)V
    .locals 2

    .prologue
    const-string v0, "$receiver"

    invoke-static {p0, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "url"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 16
    new-instance v1, Landroid/support/b/a$a;

    invoke-direct {v1}, Landroid/support/b/a$a;-><init>()V

    move-object v0, p0

    .line 17
    check-cast v0, Landroid/content/Context;

    invoke-static {v0, p2}, Landroid/support/v4/content/a;->c(Landroid/content/Context;I)I

    move-result v0

    invoke-virtual {v1, v0}, Landroid/support/b/a$a;->a(I)Landroid/support/b/a$a;

    move-result-object v0

    .line 18
    invoke-virtual {v0, p3}, Landroid/support/b/a$a;->a(Z)Landroid/support/b/a$a;

    move-result-object v0

    .line 19
    invoke-virtual {v0}, Landroid/support/b/a$a;->a()Landroid/support/b/a;

    move-result-object v0

    .line 20
    check-cast p0, Landroid/content/Context;

    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, p0, v1}, Landroid/support/b/a;->a(Landroid/content/Context;Landroid/net/Uri;)V

    .line 21
    return-void
.end method

.method public static bridge synthetic a(Landroid/app/Activity;Ljava/lang/String;IZILjava/lang/Object;)V
    .locals 1

    .prologue
    and-int/lit8 v0, p4, 0x2

    if-eqz v0, :cond_0

    .line 13
    const p2, 0x7f0f000f

    :cond_0
    and-int/lit8 v0, p4, 0x4

    if-eqz v0, :cond_1

    .line 14
    const/4 p3, 0x0

    :cond_1
    invoke-static {p0, p1, p2, p3}, Lco/uk/getmondo/common/activities/a;->a(Landroid/app/Activity;Ljava/lang/String;IZ)V

    return-void
.end method
