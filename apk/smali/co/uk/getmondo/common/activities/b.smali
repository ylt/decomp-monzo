.class public abstract Lco/uk/getmondo/common/activities/b;
.super Landroid/support/v7/app/e;
.source "BaseActivity.java"

# interfaces
.implements Lco/uk/getmondo/common/e/a$a;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lco/uk/getmondo/common/activities/b$a;
    }
.end annotation


# instance fields
.field private a:Lco/uk/getmondo/common/h/b/b;

.field private b:Landroid/support/v7/widget/Toolbar;

.field protected d:Lcom/c/b/b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/c/b/b",
            "<",
            "Lco/uk/getmondo/common/f/c;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 28
    invoke-direct {p0}, Landroid/support/v7/app/e;-><init>()V

    .line 32
    invoke-static {}, Lcom/c/b/b;->c()Lcom/c/b/b;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/common/activities/b;->d:Lcom/c/b/b;

    return-void
.end method

.method static synthetic a(Ljava/lang/String;Lco/uk/getmondo/common/f/c;)V
    .locals 0

    .prologue
    .line 148
    invoke-virtual {p1, p0}, Lco/uk/getmondo/common/f/c;->c(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic b(Ljava/lang/String;Lco/uk/getmondo/common/f/c;)V
    .locals 0

    .prologue
    .line 125
    invoke-virtual {p1, p0}, Lco/uk/getmondo/common/f/c;->b(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method protected a(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 140
    if-nez p1, :cond_0

    .line 145
    :goto_0
    return-void

    .line 143
    :cond_0
    const-string v0, "input_method"

    invoke-virtual {p0, v0}, Lco/uk/getmondo/common/activities/b;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 144
    invoke-virtual {p1}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x2

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    goto :goto_0
.end method

.method public b(I)V
    .locals 1

    .prologue
    .line 119
    invoke-virtual {p0, p1}, Lco/uk/getmondo/common/activities/b;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lco/uk/getmondo/common/activities/b;->b(Ljava/lang/String;)V

    .line 120
    return-void
.end method

.method public b(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 124
    invoke-virtual {p0}, Lco/uk/getmondo/common/activities/b;->n()Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0, v0}, Lco/uk/getmondo/common/activities/b;->a(Landroid/view/View;)V

    .line 125
    iget-object v0, p0, Lco/uk/getmondo/common/activities/b;->d:Lcom/c/b/b;

    invoke-static {p1}, Lco/uk/getmondo/common/activities/c;->a(Ljava/lang/String;)Lcom/c/a/a;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/c/b/b;->a(Lcom/c/a/a;)Lcom/c/b/b;

    .line 126
    return-void
.end method

.method public c(I)V
    .locals 1

    .prologue
    .line 166
    invoke-virtual {p0, p1}, Lco/uk/getmondo/common/activities/b;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lco/uk/getmondo/splash/SplashActivity;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lco/uk/getmondo/common/activities/b;->startActivity(Landroid/content/Intent;)V

    .line 167
    return-void
.end method

.method public c(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 148
    iget-object v0, p0, Lco/uk/getmondo/common/activities/b;->d:Lcom/c/b/b;

    invoke-static {p1}, Lco/uk/getmondo/common/activities/d;->a(Ljava/lang/String;)Lcom/c/a/a;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/c/b/b;->a(Lcom/c/a/a;)Lcom/c/b/b;

    .line 149
    return-void
.end method

.method protected l()Lco/uk/getmondo/common/h/b/b;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lco/uk/getmondo/common/activities/b;->a:Lco/uk/getmondo/common/h/b/b;

    return-object v0
.end method

.method public m()Landroid/view/View;
    .locals 1

    .prologue
    .line 63
    invoke-virtual {p0}, Lco/uk/getmondo/common/activities/b;->n()Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public n()Landroid/view/View;
    .locals 2

    .prologue
    .line 67
    invoke-virtual {p0}, Lco/uk/getmondo/common/activities/b;->getCurrentFocus()Landroid/view/View;

    move-result-object v0

    if-nez v0, :cond_0

    .line 68
    invoke-virtual {p0}, Lco/uk/getmondo/common/activities/b;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    const v1, 0x1020002

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 70
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Lco/uk/getmondo/common/activities/b;->getCurrentFocus()Landroid/view/View;

    move-result-object v0

    goto :goto_0
.end method

.method protected o()V
    .locals 1

    .prologue
    .line 75
    :try_start_0
    invoke-virtual {p0}, Lco/uk/getmondo/common/activities/b;->onBackPressed()V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 79
    :goto_0
    return-void

    .line 76
    :catch_0
    move-exception v0

    .line 77
    invoke-static {v0}, Lcom/crashlytics/android/Crashlytics;->logException(Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 36
    invoke-super {p0, p1}, Landroid/support/v7/app/e;->onCreate(Landroid/os/Bundle;)V

    .line 38
    invoke-static {}, Lco/uk/getmondo/common/h/b/a;->a()Lco/uk/getmondo/common/h/b/a$d;

    move-result-object v0

    invoke-static {p0}, Lco/uk/getmondo/MonzoApplication;->a(Landroid/content/Context;)Lco/uk/getmondo/MonzoApplication;

    move-result-object v1

    invoke-virtual {v1}, Lco/uk/getmondo/MonzoApplication;->b()Lco/uk/getmondo/common/h/a/a;

    move-result-object v1

    invoke-virtual {v0, v1}, Lco/uk/getmondo/common/h/b/a$d;->a(Lco/uk/getmondo/common/h/a/a;)Lco/uk/getmondo/common/h/b/a$d;

    move-result-object v0

    new-instance v1, Lco/uk/getmondo/common/h/b/c;

    invoke-direct {v1, p0}, Lco/uk/getmondo/common/h/b/c;-><init>(Landroid/app/Activity;)V

    .line 39
    invoke-virtual {v0, v1}, Lco/uk/getmondo/common/h/b/a$d;->a(Lco/uk/getmondo/common/h/b/c;)Lco/uk/getmondo/common/h/b/a$d;

    move-result-object v0

    invoke-virtual {v0}, Lco/uk/getmondo/common/h/b/a$d;->a()Lco/uk/getmondo/common/h/b/b;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/common/activities/b;->a:Lco/uk/getmondo/common/h/b/b;

    .line 40
    return-void
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 44
    invoke-static {}, Lcom/c/b/b;->c()Lcom/c/b/b;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/common/activities/b;->d:Lcom/c/b/b;

    .line 45
    invoke-super {p0}, Landroid/support/v7/app/e;->onDestroy()V

    .line 46
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1

    .prologue
    .line 50
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 55
    invoke-super {p0, p1}, Landroid/support/v7/app/e;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    :goto_0
    return v0

    .line 52
    :pswitch_0
    invoke-virtual {p0}, Lco/uk/getmondo/common/activities/b;->o()V

    .line 53
    const/4 v0, 0x1

    goto :goto_0

    .line 50
    nop

    :pswitch_data_0
    .packed-switch 0x102002c
        :pswitch_0
    .end packed-switch
.end method

.method protected p()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 102
    invoke-virtual {p0}, Lco/uk/getmondo/common/activities/b;->n()Landroid/view/View;

    move-result-object v0

    const v1, 0x7f1100fe

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 103
    if-eqz v0, :cond_0

    instance-of v1, v0, Landroid/support/v7/widget/Toolbar;

    if-eqz v1, :cond_0

    .line 104
    check-cast v0, Landroid/support/v7/widget/Toolbar;

    iput-object v0, p0, Lco/uk/getmondo/common/activities/b;->b:Landroid/support/v7/widget/Toolbar;

    .line 108
    iget-object v0, p0, Lco/uk/getmondo/common/activities/b;->b:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {p0, v0}, Lco/uk/getmondo/common/activities/b;->setSupportActionBar(Landroid/support/v7/widget/Toolbar;)V

    .line 109
    invoke-virtual {p0}, Lco/uk/getmondo/common/activities/b;->getSupportActionBar()Landroid/support/v7/app/a;

    move-result-object v0

    .line 110
    if-nez v0, :cond_1

    .line 115
    :cond_0
    :goto_0
    return-void

    .line 113
    :cond_1
    invoke-virtual {v0, v2}, Landroid/support/v7/app/a;->b(Z)V

    .line 114
    invoke-virtual {v0, v2}, Landroid/support/v7/app/a;->d(Z)V

    goto :goto_0
.end method

.method public q()V
    .locals 1

    .prologue
    .line 130
    invoke-virtual {p0}, Lco/uk/getmondo/common/activities/b;->n()Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0, v0}, Lco/uk/getmondo/common/activities/b;->a(Landroid/view/View;)V

    .line 131
    invoke-static {p0}, Lco/uk/getmondo/force_upgrade/ForceUpgradeActivity;->a(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lco/uk/getmondo/common/activities/b;->startActivity(Landroid/content/Intent;)V

    .line 132
    return-void
.end method

.method protected r()Landroid/support/v7/widget/Toolbar;
    .locals 1

    .prologue
    .line 136
    iget-object v0, p0, Lco/uk/getmondo/common/activities/b;->b:Landroid/support/v7/widget/Toolbar;

    return-object v0
.end method

.method public s()V
    .locals 2

    .prologue
    .line 152
    iget-object v0, p0, Lco/uk/getmondo/common/activities/b;->d:Lcom/c/b/b;

    invoke-static {}, Lco/uk/getmondo/common/activities/e;->a()Lcom/c/a/a;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/c/b/b;->a(Lcom/c/a/a;)Lcom/c/b/b;

    .line 153
    return-void
.end method

.method public setContentView(I)V
    .locals 2

    .prologue
    .line 83
    invoke-super {p0, p1}, Landroid/support/v7/app/e;->setContentView(I)V

    .line 84
    invoke-virtual {p0}, Lco/uk/getmondo/common/activities/b;->p()V

    .line 85
    new-instance v0, Lco/uk/getmondo/common/f/c;

    invoke-virtual {p0}, Lco/uk/getmondo/common/activities/b;->m()Landroid/view/View;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lco/uk/getmondo/common/f/c;-><init>(Landroid/content/Context;Landroid/view/View;)V

    invoke-static {v0}, Lcom/c/b/b;->b(Ljava/lang/Object;)Lcom/c/b/b;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/common/activities/b;->d:Lcom/c/b/b;

    .line 86
    return-void
.end method

.method public setContentView(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 90
    invoke-super {p0, p1}, Landroid/support/v7/app/e;->setContentView(Landroid/view/View;)V

    .line 91
    invoke-virtual {p0}, Lco/uk/getmondo/common/activities/b;->p()V

    .line 92
    new-instance v0, Lco/uk/getmondo/common/f/c;

    invoke-virtual {p0}, Lco/uk/getmondo/common/activities/b;->m()Landroid/view/View;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lco/uk/getmondo/common/f/c;-><init>(Landroid/content/Context;Landroid/view/View;)V

    invoke-static {v0}, Lcom/c/b/b;->b(Ljava/lang/Object;)Lcom/c/b/b;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/common/activities/b;->d:Lcom/c/b/b;

    .line 93
    return-void
.end method

.method public setContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
    .locals 0

    .prologue
    .line 97
    invoke-super {p0, p1, p2}, Landroid/support/v7/app/e;->setContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 98
    invoke-virtual {p0}, Lco/uk/getmondo/common/activities/b;->p()V

    .line 99
    return-void
.end method

.method public t()V
    .locals 2

    .prologue
    .line 156
    iget-object v0, p0, Lco/uk/getmondo/common/activities/b;->d:Lcom/c/b/b;

    invoke-static {}, Lco/uk/getmondo/common/activities/f;->a()Lcom/c/a/a;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/c/b/b;->a(Lcom/c/a/a;)Lcom/c/b/b;

    .line 157
    return-void
.end method

.method public u()V
    .locals 0

    .prologue
    .line 161
    invoke-static {p0}, Lco/uk/getmondo/splash/SplashActivity;->a(Landroid/content/Context;)V

    .line 162
    return-void
.end method
