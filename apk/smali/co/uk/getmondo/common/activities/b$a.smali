.class public final enum Lco/uk/getmondo/common/activities/b$a;
.super Ljava/lang/Enum;
.source "BaseActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lco/uk/getmondo/common/activities/b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lco/uk/getmondo/common/activities/b$a;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lco/uk/getmondo/common/activities/b$a;

.field public static final enum b:Lco/uk/getmondo/common/activities/b$a;

.field private static final synthetic c:[Lco/uk/getmondo/common/activities/b$a;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 170
    new-instance v0, Lco/uk/getmondo/common/activities/b$a;

    const-string v1, "DARK"

    invoke-direct {v0, v1, v2}, Lco/uk/getmondo/common/activities/b$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lco/uk/getmondo/common/activities/b$a;->a:Lco/uk/getmondo/common/activities/b$a;

    .line 171
    new-instance v0, Lco/uk/getmondo/common/activities/b$a;

    const-string v1, "LIGHT"

    invoke-direct {v0, v1, v3}, Lco/uk/getmondo/common/activities/b$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lco/uk/getmondo/common/activities/b$a;->b:Lco/uk/getmondo/common/activities/b$a;

    .line 169
    const/4 v0, 0x2

    new-array v0, v0, [Lco/uk/getmondo/common/activities/b$a;

    sget-object v1, Lco/uk/getmondo/common/activities/b$a;->a:Lco/uk/getmondo/common/activities/b$a;

    aput-object v1, v0, v2

    sget-object v1, Lco/uk/getmondo/common/activities/b$a;->b:Lco/uk/getmondo/common/activities/b$a;

    aput-object v1, v0, v3

    sput-object v0, Lco/uk/getmondo/common/activities/b$a;->c:[Lco/uk/getmondo/common/activities/b$a;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 169
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lco/uk/getmondo/common/activities/b$a;
    .locals 1

    .prologue
    .line 169
    const-class v0, Lco/uk/getmondo/common/activities/b$a;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/common/activities/b$a;

    return-object v0
.end method

.method public static values()[Lco/uk/getmondo/common/activities/b$a;
    .locals 1

    .prologue
    .line 169
    sget-object v0, Lco/uk/getmondo/common/activities/b$a;->c:[Lco/uk/getmondo/common/activities/b$a;

    invoke-virtual {v0}, [Lco/uk/getmondo/common/activities/b$a;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lco/uk/getmondo/common/activities/b$a;

    return-object v0
.end method
