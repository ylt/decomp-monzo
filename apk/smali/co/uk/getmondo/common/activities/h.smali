.class public abstract Lco/uk/getmondo/common/activities/h;
.super Lco/uk/getmondo/common/activities/b;
.source "ModalActivity.java"


# instance fields
.field private a:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 12
    invoke-direct {p0}, Lco/uk/getmondo/common/activities/b;-><init>()V

    .line 14
    const/4 v0, 0x1

    iput-boolean v0, p0, Lco/uk/getmondo/common/activities/h;->a:Z

    return-void
.end method

.method private b()V
    .locals 3

    .prologue
    .line 47
    invoke-virtual {p0}, Lco/uk/getmondo/common/activities/h;->r()Landroid/support/v7/widget/Toolbar;

    move-result-object v0

    .line 48
    if-eqz v0, :cond_0

    iget-boolean v1, p0, Lco/uk/getmondo/common/activities/h;->a:Z

    if-eqz v1, :cond_0

    .line 49
    invoke-virtual {v0}, Landroid/support/v7/widget/Toolbar;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f02014c

    invoke-static {v1, v2}, Landroid/support/v4/content/a;->a(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->mutate()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 50
    invoke-virtual {v0, v1}, Landroid/support/v7/widget/Toolbar;->setNavigationIcon(Landroid/graphics/drawable/Drawable;)V

    .line 52
    :cond_0
    return-void
.end method


# virtual methods
.method protected a()V
    .locals 1

    .prologue
    .line 55
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lco/uk/getmondo/common/activities/h;->setResult(I)V

    .line 56
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1

    .prologue
    .line 18
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 25
    :cond_0
    :goto_0
    invoke-super {p0, p1}, Lco/uk/getmondo/common/activities/b;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    return v0

    .line 20
    :pswitch_0
    iget-boolean v0, p0, Lco/uk/getmondo/common/activities/h;->a:Z

    if-eqz v0, :cond_0

    .line 21
    invoke-virtual {p0}, Lco/uk/getmondo/common/activities/h;->a()V

    goto :goto_0

    .line 18
    :pswitch_data_0
    .packed-switch 0x102002c
        :pswitch_0
    .end packed-switch
.end method

.method public setContentView(I)V
    .locals 0

    .prologue
    .line 30
    invoke-super {p0, p1}, Lco/uk/getmondo/common/activities/b;->setContentView(I)V

    .line 31
    invoke-direct {p0}, Lco/uk/getmondo/common/activities/h;->b()V

    .line 32
    return-void
.end method

.method public setContentView(Landroid/view/View;)V
    .locals 0

    .prologue
    .line 36
    invoke-super {p0, p1}, Lco/uk/getmondo/common/activities/b;->setContentView(Landroid/view/View;)V

    .line 37
    invoke-direct {p0}, Lco/uk/getmondo/common/activities/h;->b()V

    .line 38
    return-void
.end method

.method public setContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
    .locals 0

    .prologue
    .line 42
    invoke-super {p0, p1, p2}, Lco/uk/getmondo/common/activities/b;->setContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 43
    invoke-direct {p0}, Lco/uk/getmondo/common/activities/h;->b()V

    .line 44
    return-void
.end method
