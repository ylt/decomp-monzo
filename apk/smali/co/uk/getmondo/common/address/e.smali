.class Lco/uk/getmondo/common/address/e;
.super Lco/uk/getmondo/common/ui/b;
.source "LegacyEnterAddressPresenter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lco/uk/getmondo/common/address/e$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lco/uk/getmondo/common/ui/b",
        "<",
        "Lco/uk/getmondo/common/address/e$a;",
        ">;"
    }
.end annotation


# instance fields
.field private final c:Lco/uk/getmondo/common/a;

.field private d:Lco/uk/getmondo/d/s;


# direct methods
.method constructor <init>(Lco/uk/getmondo/common/a;)V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, Lco/uk/getmondo/common/ui/b;-><init>()V

    .line 24
    iput-object p1, p0, Lco/uk/getmondo/common/address/e;->c:Lco/uk/getmondo/common/a;

    .line 25
    return-void
.end method

.method static synthetic a(Lco/uk/getmondo/common/address/e$a;Lco/uk/getmondo/common/address/a/a/a;)V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 51
    invoke-virtual {p1}, Lco/uk/getmondo/common/address/a/a/a;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    .line 52
    invoke-virtual {p1}, Lco/uk/getmondo/common/address/a/a/a;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    .line 53
    invoke-virtual {p1}, Lco/uk/getmondo/common/address/a/a/a;->c()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v3

    .line 54
    invoke-static {v2}, Lco/uk/getmondo/common/k/p;->d(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 55
    invoke-interface {p0}, Lco/uk/getmondo/common/address/e$a;->b()V

    .line 68
    :goto_0
    return-void

    .line 56
    :cond_0
    invoke-static {v1}, Lco/uk/getmondo/common/k/p;->d(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 57
    invoke-interface {p0}, Lco/uk/getmondo/common/address/e$a;->c()V

    goto :goto_0

    .line 58
    :cond_1
    invoke-static {v3}, Lco/uk/getmondo/common/k/p;->d(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 59
    invoke-interface {p0}, Lco/uk/getmondo/common/address/e$a;->d()V

    goto :goto_0

    .line 61
    :cond_2
    new-instance v0, Lco/uk/getmondo/d/s;

    .line 62
    invoke-static {v2}, Lco/uk/getmondo/common/k/a;->a(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 64
    invoke-static {}, Lco/uk/getmondo/d/i;->i()Lco/uk/getmondo/d/i;

    move-result-object v4

    invoke-virtual {v4}, Lco/uk/getmondo/d/i;->e()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-direct/range {v0 .. v5}, Lco/uk/getmondo/d/s;-><init>(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 66
    invoke-interface {p0, v0}, Lco/uk/getmondo/common/address/e$a;->a(Lco/uk/getmondo/d/s;)V

    goto :goto_0
.end method

.method static synthetic b(Lco/uk/getmondo/common/address/e$a;Lco/uk/getmondo/common/address/a/a/a;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 49
    invoke-interface {p0}, Lco/uk/getmondo/common/address/e$a;->e()V

    return-void
.end method


# virtual methods
.method public a(Lco/uk/getmondo/common/address/e$a;)V
    .locals 2

    .prologue
    .line 33
    invoke-super {p0, p1}, Lco/uk/getmondo/common/ui/b;->a(Lco/uk/getmondo/common/ui/f;)V

    .line 34
    iget-object v0, p0, Lco/uk/getmondo/common/address/e;->c:Lco/uk/getmondo/common/a;

    invoke-static {}, Lco/uk/getmondo/api/model/tracking/Impression;->r()Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v1

    invoke-virtual {v0, v1}, Lco/uk/getmondo/common/a;->a(Lco/uk/getmondo/api/model/tracking/Impression;)V

    .line 36
    iget-object v0, p0, Lco/uk/getmondo/common/address/e;->d:Lco/uk/getmondo/d/s;

    if-eqz v0, :cond_1

    .line 37
    iget-object v0, p0, Lco/uk/getmondo/common/address/e;->d:Lco/uk/getmondo/d/s;

    invoke-virtual {v0}, Lco/uk/getmondo/d/s;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 38
    iget-object v0, p0, Lco/uk/getmondo/common/address/e;->d:Lco/uk/getmondo/d/s;

    invoke-virtual {v0}, Lco/uk/getmondo/d/s;->h()[Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lco/uk/getmondo/common/k/a;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Lco/uk/getmondo/common/address/e$a;->a(Ljava/lang/String;)V

    .line 40
    :cond_0
    iget-object v0, p0, Lco/uk/getmondo/common/address/e;->d:Lco/uk/getmondo/d/s;

    invoke-virtual {v0}, Lco/uk/getmondo/d/s;->c()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Lco/uk/getmondo/common/address/e$a;->d(Ljava/lang/String;)V

    .line 41
    iget-object v0, p0, Lco/uk/getmondo/common/address/e;->d:Lco/uk/getmondo/d/s;

    invoke-virtual {v0}, Lco/uk/getmondo/d/s;->g()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 42
    iget-object v0, p0, Lco/uk/getmondo/common/address/e;->d:Lco/uk/getmondo/d/s;

    invoke-virtual {v0}, Lco/uk/getmondo/d/s;->i()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Lco/uk/getmondo/common/address/e$a;->e(Ljava/lang/String;)V

    .line 46
    :cond_1
    invoke-static {}, Lco/uk/getmondo/d/i;->i()Lco/uk/getmondo/d/i;

    move-result-object v0

    invoke-virtual {v0}, Lco/uk/getmondo/d/i;->b()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Lco/uk/getmondo/common/address/e$a;->f(Ljava/lang/String;)V

    .line 48
    invoke-interface {p1}, Lco/uk/getmondo/common/address/e$a;->a()Lio/reactivex/n;

    move-result-object v0

    invoke-static {p1}, Lco/uk/getmondo/common/address/f;->a(Lco/uk/getmondo/common/address/e$a;)Lio/reactivex/c/g;

    move-result-object v1

    .line 49
    invoke-virtual {v0, v1}, Lio/reactivex/n;->doOnNext(Lio/reactivex/c/g;)Lio/reactivex/n;

    move-result-object v0

    invoke-static {p1}, Lco/uk/getmondo/common/address/g;->a(Lco/uk/getmondo/common/address/e$a;)Lio/reactivex/c/g;

    move-result-object v1

    .line 50
    invoke-virtual {v0, v1}, Lio/reactivex/n;->subscribe(Lio/reactivex/c/g;)Lio/reactivex/b/b;

    move-result-object v0

    .line 48
    invoke-virtual {p0, v0}, Lco/uk/getmondo/common/address/e;->a(Lio/reactivex/b/b;)V

    .line 69
    return-void
.end method

.method public bridge synthetic a(Lco/uk/getmondo/common/ui/f;)V
    .locals 0

    .prologue
    .line 17
    check-cast p1, Lco/uk/getmondo/common/address/e$a;

    invoke-virtual {p0, p1}, Lco/uk/getmondo/common/address/e;->a(Lco/uk/getmondo/common/address/e$a;)V

    return-void
.end method

.method public a(Lco/uk/getmondo/d/s;)V
    .locals 0

    .prologue
    .line 28
    iput-object p1, p0, Lco/uk/getmondo/common/address/e;->d:Lco/uk/getmondo/d/s;

    .line 29
    return-void
.end method
