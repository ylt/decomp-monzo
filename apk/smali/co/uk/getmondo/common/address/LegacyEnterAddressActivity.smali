.class public Lco/uk/getmondo/common/address/LegacyEnterAddressActivity;
.super Lco/uk/getmondo/common/activities/b;
.source "LegacyEnterAddressActivity.java"

# interfaces
.implements Lco/uk/getmondo/common/address/e$a;


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# instance fields
.field a:Lco/uk/getmondo/common/address/e;

.field cityView:Landroid/widget/EditText;
    .annotation build Lbutterknife/BindView;
        value = 0x7f110164
    .end annotation
.end field

.field cityWrapper:Landroid/support/design/widget/TextInputLayout;
    .annotation build Lbutterknife/BindView;
        value = 0x7f110163
    .end annotation
.end field

.field countryView:Landroid/widget/EditText;
    .annotation build Lbutterknife/BindView;
        value = 0x7f110165
    .end annotation
.end field

.field postalCodeView:Landroid/widget/EditText;
    .annotation build Lbutterknife/BindView;
        value = 0x7f110162
    .end annotation
.end field

.field postalCodeWrapper:Landroid/support/design/widget/TextInputLayout;
    .annotation build Lbutterknife/BindView;
        value = 0x7f110161
    .end annotation
.end field

.field streetAddressView:Landroid/widget/EditText;
    .annotation build Lbutterknife/BindView;
        value = 0x7f110160
    .end annotation
.end field

.field streetAddressWrapper:Landroid/support/design/widget/TextInputLayout;
    .annotation build Lbutterknife/BindView;
        value = 0x7f11015f
    .end annotation
.end field

.field submitButton:Landroid/widget/Button;
    .annotation build Lbutterknife/BindView;
        value = 0x7f110166
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Lco/uk/getmondo/common/activities/b;-><init>()V

    return-void
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;Lco/uk/getmondo/common/activities/b$a;)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 42
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lco/uk/getmondo/common/address/LegacyEnterAddressActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 43
    const-string v1, "EXTRA_SUBMIT_TEXT"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 44
    const-string v1, "EXTRA_THEME"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 45
    return-object v0
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;Lco/uk/getmondo/common/activities/b$a;Lco/uk/getmondo/d/s;)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 49
    invoke-static {p0, p1, p2}, Lco/uk/getmondo/common/address/LegacyEnterAddressActivity;->a(Landroid/content/Context;Ljava/lang/String;Lco/uk/getmondo/common/activities/b$a;)Landroid/content/Intent;

    move-result-object v0

    .line 50
    if-eqz p3, :cond_0

    .line 51
    const-string v1, "EXTRA_ADDRESS"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 53
    :cond_0
    return-object v0
.end method

.method static synthetic a(Lco/uk/getmondo/common/address/LegacyEnterAddressActivity;Ljava/lang/Object;)Lco/uk/getmondo/common/address/a/a/a;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 92
    new-instance v0, Lco/uk/getmondo/common/address/a/a/a;

    iget-object v1, p0, Lco/uk/getmondo/common/address/LegacyEnterAddressActivity;->streetAddressView:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lco/uk/getmondo/common/address/LegacyEnterAddressActivity;->postalCodeView:Landroid/widget/EditText;

    .line 93
    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lco/uk/getmondo/common/address/LegacyEnterAddressActivity;->cityView:Landroid/widget/EditText;

    .line 94
    invoke-virtual {v3}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lco/uk/getmondo/common/address/a/a/a;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 92
    return-object v0
.end method

.method public static a(Landroid/content/Intent;)Lco/uk/getmondo/d/s;
    .locals 1

    .prologue
    .line 57
    const-string v0, "EXTRA_ADDRESS"

    invoke-virtual {p0, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/d/s;

    return-object v0
.end method


# virtual methods
.method public a()Lio/reactivex/n;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Lco/uk/getmondo/common/address/a/a/a;",
            ">;"
        }
    .end annotation

    .prologue
    .line 91
    iget-object v0, p0, Lco/uk/getmondo/common/address/LegacyEnterAddressActivity;->submitButton:Landroid/widget/Button;

    invoke-static {v0}, Lcom/b/a/c/c;->a(Landroid/view/View;)Lio/reactivex/n;

    move-result-object v0

    invoke-static {p0}, Lco/uk/getmondo/common/address/c;->a(Lco/uk/getmondo/common/address/LegacyEnterAddressActivity;)Lio/reactivex/c/h;

    move-result-object v1

    .line 92
    invoke-virtual {v0, v1}, Lio/reactivex/n;->map(Lio/reactivex/c/h;)Lio/reactivex/n;

    move-result-object v0

    .line 91
    return-object v0
.end method

.method public a(Lco/uk/getmondo/d/s;)V
    .locals 2

    .prologue
    .line 99
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 100
    const-string v1, "EXTRA_ADDRESS"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 101
    const/4 v1, -0x1

    invoke-virtual {p0, v1, v0}, Lco/uk/getmondo/common/address/LegacyEnterAddressActivity;->setResult(ILandroid/content/Intent;)V

    .line 102
    invoke-virtual {p0}, Lco/uk/getmondo/common/address/LegacyEnterAddressActivity;->finish()V

    .line 103
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 107
    iget-object v0, p0, Lco/uk/getmondo/common/address/LegacyEnterAddressActivity;->streetAddressView:Landroid/widget/EditText;

    invoke-virtual {v0, p1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 108
    return-void
.end method

.method public b()V
    .locals 2

    .prologue
    .line 127
    iget-object v0, p0, Lco/uk/getmondo/common/address/LegacyEnterAddressActivity;->streetAddressWrapper:Landroid/support/design/widget/TextInputLayout;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/support/design/widget/TextInputLayout;->setErrorEnabled(Z)V

    .line 128
    iget-object v0, p0, Lco/uk/getmondo/common/address/LegacyEnterAddressActivity;->streetAddressWrapper:Landroid/support/design/widget/TextInputLayout;

    const v1, 0x7f0a0312

    invoke-virtual {p0, v1}, Lco/uk/getmondo/common/address/LegacyEnterAddressActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/design/widget/TextInputLayout;->setError(Ljava/lang/CharSequence;)V

    .line 129
    return-void
.end method

.method public c()V
    .locals 2

    .prologue
    .line 133
    iget-object v0, p0, Lco/uk/getmondo/common/address/LegacyEnterAddressActivity;->postalCodeWrapper:Landroid/support/design/widget/TextInputLayout;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/support/design/widget/TextInputLayout;->setErrorEnabled(Z)V

    .line 134
    iget-object v0, p0, Lco/uk/getmondo/common/address/LegacyEnterAddressActivity;->postalCodeWrapper:Landroid/support/design/widget/TextInputLayout;

    const v1, 0x7f0a0311

    invoke-virtual {p0, v1}, Lco/uk/getmondo/common/address/LegacyEnterAddressActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/design/widget/TextInputLayout;->setError(Ljava/lang/CharSequence;)V

    .line 135
    return-void
.end method

.method public d()V
    .locals 2

    .prologue
    .line 139
    iget-object v0, p0, Lco/uk/getmondo/common/address/LegacyEnterAddressActivity;->cityWrapper:Landroid/support/design/widget/TextInputLayout;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/support/design/widget/TextInputLayout;->setErrorEnabled(Z)V

    .line 140
    iget-object v0, p0, Lco/uk/getmondo/common/address/LegacyEnterAddressActivity;->cityWrapper:Landroid/support/design/widget/TextInputLayout;

    const v1, 0x7f0a030f

    invoke-virtual {p0, v1}, Lco/uk/getmondo/common/address/LegacyEnterAddressActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/design/widget/TextInputLayout;->setError(Ljava/lang/CharSequence;)V

    .line 141
    return-void
.end method

.method public d(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 112
    iget-object v0, p0, Lco/uk/getmondo/common/address/LegacyEnterAddressActivity;->postalCodeView:Landroid/widget/EditText;

    invoke-virtual {v0, p1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 113
    return-void
.end method

.method public e()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 145
    iget-object v0, p0, Lco/uk/getmondo/common/address/LegacyEnterAddressActivity;->streetAddressWrapper:Landroid/support/design/widget/TextInputLayout;

    invoke-virtual {v0, v1}, Landroid/support/design/widget/TextInputLayout;->setErrorEnabled(Z)V

    .line 146
    iget-object v0, p0, Lco/uk/getmondo/common/address/LegacyEnterAddressActivity;->postalCodeWrapper:Landroid/support/design/widget/TextInputLayout;

    invoke-virtual {v0, v1}, Landroid/support/design/widget/TextInputLayout;->setErrorEnabled(Z)V

    .line 147
    iget-object v0, p0, Lco/uk/getmondo/common/address/LegacyEnterAddressActivity;->cityWrapper:Landroid/support/design/widget/TextInputLayout;

    invoke-virtual {v0, v1}, Landroid/support/design/widget/TextInputLayout;->setErrorEnabled(Z)V

    .line 148
    return-void
.end method

.method public e(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 117
    iget-object v0, p0, Lco/uk/getmondo/common/address/LegacyEnterAddressActivity;->cityView:Landroid/widget/EditText;

    invoke-virtual {v0, p1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 118
    return-void
.end method

.method public f(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 122
    iget-object v0, p0, Lco/uk/getmondo/common/address/LegacyEnterAddressActivity;->countryView:Landroid/widget/EditText;

    invoke-virtual {v0, p1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 123
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 62
    invoke-virtual {p0}, Lco/uk/getmondo/common/address/LegacyEnterAddressActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "EXTRA_THEME"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/common/activities/b$a;

    .line 63
    if-eqz v0, :cond_0

    .line 64
    sget-object v1, Lco/uk/getmondo/common/activities/b$a;->b:Lco/uk/getmondo/common/activities/b$a;

    if-ne v0, v1, :cond_1

    const v0, 0x7f0c0128

    .line 65
    :goto_0
    invoke-virtual {p0, v0}, Lco/uk/getmondo/common/address/LegacyEnterAddressActivity;->setTheme(I)V

    .line 68
    :cond_0
    invoke-super {p0, p1}, Lco/uk/getmondo/common/activities/b;->onCreate(Landroid/os/Bundle;)V

    .line 69
    const v0, 0x7f050036

    invoke-virtual {p0, v0}, Lco/uk/getmondo/common/address/LegacyEnterAddressActivity;->setContentView(I)V

    .line 70
    invoke-static {p0}, Lbutterknife/ButterKnife;->bind(Landroid/app/Activity;)Lbutterknife/Unbinder;

    .line 71
    invoke-virtual {p0}, Lco/uk/getmondo/common/address/LegacyEnterAddressActivity;->l()Lco/uk/getmondo/common/h/b/b;

    move-result-object v0

    invoke-interface {v0, p0}, Lco/uk/getmondo/common/h/b/b;->a(Lco/uk/getmondo/common/address/LegacyEnterAddressActivity;)V

    .line 73
    invoke-virtual {p0}, Lco/uk/getmondo/common/address/LegacyEnterAddressActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "EXTRA_SUBMIT_TEXT"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 74
    iget-object v1, p0, Lco/uk/getmondo/common/address/LegacyEnterAddressActivity;->submitButton:Landroid/widget/Button;

    invoke-virtual {v1, v0}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 77
    invoke-virtual {p0}, Lco/uk/getmondo/common/address/LegacyEnterAddressActivity;->getSupportActionBar()Landroid/support/v7/app/a;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/support/v7/app/a;->b(Z)V

    .line 79
    iget-object v1, p0, Lco/uk/getmondo/common/address/LegacyEnterAddressActivity;->a:Lco/uk/getmondo/common/address/e;

    invoke-virtual {p0}, Lco/uk/getmondo/common/address/LegacyEnterAddressActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v2, "EXTRA_ADDRESS"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/d/s;

    invoke-virtual {v1, v0}, Lco/uk/getmondo/common/address/e;->a(Lco/uk/getmondo/d/s;)V

    .line 80
    iget-object v0, p0, Lco/uk/getmondo/common/address/LegacyEnterAddressActivity;->a:Lco/uk/getmondo/common/address/e;

    invoke-virtual {v0, p0}, Lco/uk/getmondo/common/address/e;->a(Lco/uk/getmondo/common/address/e$a;)V

    .line 81
    return-void

    .line 64
    :cond_1
    const v0, 0x7f0c0125

    goto :goto_0
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lco/uk/getmondo/common/address/LegacyEnterAddressActivity;->a:Lco/uk/getmondo/common/address/e;

    invoke-virtual {v0}, Lco/uk/getmondo/common/address/e;->b()V

    .line 86
    invoke-super {p0}, Lco/uk/getmondo/common/activities/b;->onDestroy()V

    .line 87
    return-void
.end method
