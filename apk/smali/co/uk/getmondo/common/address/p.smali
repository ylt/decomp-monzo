.class public Lco/uk/getmondo/common/address/p;
.super Lco/uk/getmondo/common/ui/b;
.source "SelectAddressPresenter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lco/uk/getmondo/common/address/p$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lco/uk/getmondo/common/ui/b",
        "<",
        "Lco/uk/getmondo/common/address/p$a;",
        ">;"
    }
.end annotation


# instance fields
.field private final c:Lio/reactivex/u;

.field private final d:Lio/reactivex/u;

.field private final e:Lco/uk/getmondo/common/accounts/d;

.field private final f:Lco/uk/getmondo/common/e/a;

.field private final g:Lco/uk/getmondo/api/MonzoApi;

.field private final h:Lco/uk/getmondo/common/a;

.field private final i:Lco/uk/getmondo/d/a/i;

.field private j:Z


# direct methods
.method constructor <init>(Lio/reactivex/u;Lio/reactivex/u;Lco/uk/getmondo/common/accounts/d;Lco/uk/getmondo/common/e/a;Lco/uk/getmondo/api/MonzoApi;Lco/uk/getmondo/common/a;Z)V
    .locals 1

    .prologue
    .line 38
    invoke-direct {p0}, Lco/uk/getmondo/common/ui/b;-><init>()V

    .line 33
    new-instance v0, Lco/uk/getmondo/d/a/i;

    invoke-direct {v0}, Lco/uk/getmondo/d/a/i;-><init>()V

    iput-object v0, p0, Lco/uk/getmondo/common/address/p;->i:Lco/uk/getmondo/d/a/i;

    .line 39
    iput-object p1, p0, Lco/uk/getmondo/common/address/p;->c:Lio/reactivex/u;

    .line 40
    iput-object p2, p0, Lco/uk/getmondo/common/address/p;->d:Lio/reactivex/u;

    .line 41
    iput-object p3, p0, Lco/uk/getmondo/common/address/p;->e:Lco/uk/getmondo/common/accounts/d;

    .line 42
    iput-object p4, p0, Lco/uk/getmondo/common/address/p;->f:Lco/uk/getmondo/common/e/a;

    .line 43
    iput-object p5, p0, Lco/uk/getmondo/common/address/p;->g:Lco/uk/getmondo/api/MonzoApi;

    .line 44
    iput-object p6, p0, Lco/uk/getmondo/common/address/p;->h:Lco/uk/getmondo/common/a;

    .line 45
    iput-boolean p7, p0, Lco/uk/getmondo/common/address/p;->j:Z

    .line 46
    return-void
.end method

.method static synthetic a(Lco/uk/getmondo/api/model/ApiPostcodeResponse;)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 90
    invoke-virtual {p0}, Lco/uk/getmondo/api/model/ApiPostcodeResponse;->a()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lco/uk/getmondo/common/address/p;Ljava/lang/Throwable;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 104
    iget-object v1, p0, Lco/uk/getmondo/common/address/p;->f:Lco/uk/getmondo/common/e/a;

    iget-object v0, p0, Lco/uk/getmondo/common/address/p;->a:Lco/uk/getmondo/common/ui/f;

    check-cast v0, Lco/uk/getmondo/common/e/a$a;

    invoke-virtual {v1, p1, v0}, Lco/uk/getmondo/common/e/a;->a(Ljava/lang/Throwable;Lco/uk/getmondo/common/e/a$a;)Z

    return-void
.end method

.method static synthetic a(Lco/uk/getmondo/common/address/p;Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 96
    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 97
    iget-object v0, p0, Lco/uk/getmondo/common/address/p;->a:Lco/uk/getmondo/common/ui/f;

    check-cast v0, Lco/uk/getmondo/common/address/p$a;

    invoke-interface {v0}, Lco/uk/getmondo/common/address/p$a;->a()V

    .line 98
    iget-object v0, p0, Lco/uk/getmondo/common/address/p;->a:Lco/uk/getmondo/common/ui/f;

    check-cast v0, Lco/uk/getmondo/common/address/p$a;

    invoke-interface {v0}, Lco/uk/getmondo/common/address/p$a;->c()V

    .line 104
    :goto_0
    return-void

    .line 100
    :cond_0
    iget-object v0, p0, Lco/uk/getmondo/common/address/p;->a:Lco/uk/getmondo/common/ui/f;

    check-cast v0, Lco/uk/getmondo/common/address/p$a;

    invoke-interface {v0}, Lco/uk/getmondo/common/address/p$a;->b()V

    .line 101
    iget-object v0, p0, Lco/uk/getmondo/common/address/p;->a:Lco/uk/getmondo/common/ui/f;

    check-cast v0, Lco/uk/getmondo/common/address/p$a;

    invoke-interface {v0}, Lco/uk/getmondo/common/address/p$a;->d()V

    .line 102
    iget-object v0, p0, Lco/uk/getmondo/common/address/p;->a:Lco/uk/getmondo/common/ui/f;

    check-cast v0, Lco/uk/getmondo/common/address/p$a;

    invoke-interface {v0, p1}, Lco/uk/getmondo/common/address/p$a;->a(Ljava/util/List;)V

    goto :goto_0
.end method

.method static synthetic a(Lco/uk/getmondo/common/address/p;Ljava/util/List;Ljava/lang/Throwable;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 94
    iget-object v0, p0, Lco/uk/getmondo/common/address/p;->a:Lco/uk/getmondo/common/ui/f;

    check-cast v0, Lco/uk/getmondo/common/address/p$a;

    invoke-interface {v0}, Lco/uk/getmondo/common/address/p$a;->t()V

    return-void
.end method

.method static synthetic b(Lco/uk/getmondo/common/address/p;Ljava/util/List;)Lio/reactivex/z;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 91
    invoke-static {p1}, Lio/reactivex/n;->fromIterable(Ljava/lang/Iterable;)Lio/reactivex/n;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/common/address/p;->i:Lco/uk/getmondo/d/a/i;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    invoke-static {v1}, Lco/uk/getmondo/common/address/v;->a(Lco/uk/getmondo/d/a/i;)Lio/reactivex/c/h;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/n;->map(Lio/reactivex/c/h;)Lio/reactivex/n;

    move-result-object v0

    invoke-virtual {v0}, Lio/reactivex/n;->toList()Lio/reactivex/v;

    move-result-object v0

    return-object v0
.end method

.method private b(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 86
    iget-object v0, p0, Lco/uk/getmondo/common/address/p;->a:Lco/uk/getmondo/common/ui/f;

    check-cast v0, Lco/uk/getmondo/common/address/p$a;

    invoke-interface {v0}, Lco/uk/getmondo/common/address/p$a;->s()V

    .line 87
    iget-object v0, p0, Lco/uk/getmondo/common/address/p;->a:Lco/uk/getmondo/common/ui/f;

    check-cast v0, Lco/uk/getmondo/common/address/p$a;

    invoke-interface {v0}, Lco/uk/getmondo/common/address/p$a;->g()V

    .line 89
    iget-object v0, p0, Lco/uk/getmondo/common/address/p;->g:Lco/uk/getmondo/api/MonzoApi;

    invoke-static {}, Lco/uk/getmondo/d/i;->i()Lco/uk/getmondo/d/i;

    move-result-object v1

    invoke-virtual {v1}, Lco/uk/getmondo/d/i;->e()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Lco/uk/getmondo/api/MonzoApi;->lookupPostcode(Ljava/lang/String;Ljava/lang/String;)Lio/reactivex/v;

    move-result-object v0

    invoke-static {}, Lco/uk/getmondo/common/address/q;->a()Lio/reactivex/c/h;

    move-result-object v1

    .line 90
    invoke-virtual {v0, v1}, Lio/reactivex/v;->d(Lio/reactivex/c/h;)Lio/reactivex/v;

    move-result-object v0

    invoke-static {p0}, Lco/uk/getmondo/common/address/r;->a(Lco/uk/getmondo/common/address/p;)Lio/reactivex/c/h;

    move-result-object v1

    .line 91
    invoke-virtual {v0, v1}, Lio/reactivex/v;->a(Lio/reactivex/c/h;)Lio/reactivex/v;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/common/address/p;->d:Lio/reactivex/u;

    .line 92
    invoke-virtual {v0, v1}, Lio/reactivex/v;->b(Lio/reactivex/u;)Lio/reactivex/v;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/common/address/p;->c:Lio/reactivex/u;

    .line 93
    invoke-virtual {v0, v1}, Lio/reactivex/v;->a(Lio/reactivex/u;)Lio/reactivex/v;

    move-result-object v0

    invoke-static {p0}, Lco/uk/getmondo/common/address/s;->a(Lco/uk/getmondo/common/address/p;)Lio/reactivex/c/b;

    move-result-object v1

    .line 94
    invoke-virtual {v0, v1}, Lio/reactivex/v;->a(Lio/reactivex/c/b;)Lio/reactivex/v;

    move-result-object v0

    invoke-static {p0}, Lco/uk/getmondo/common/address/t;->a(Lco/uk/getmondo/common/address/p;)Lio/reactivex/c/g;

    move-result-object v1

    invoke-static {p0}, Lco/uk/getmondo/common/address/u;->a(Lco/uk/getmondo/common/address/p;)Lio/reactivex/c/g;

    move-result-object v2

    .line 95
    invoke-virtual {v0, v1, v2}, Lio/reactivex/v;->a(Lio/reactivex/c/g;Lio/reactivex/c/g;)Lio/reactivex/b/b;

    move-result-object v0

    .line 89
    invoke-virtual {p0, v0}, Lco/uk/getmondo/common/address/p;->a(Lio/reactivex/b/b;)V

    .line 105
    return-void
.end method


# virtual methods
.method a()V
    .locals 1

    .prologue
    .line 74
    iget-object v0, p0, Lco/uk/getmondo/common/address/p;->a:Lco/uk/getmondo/common/ui/f;

    check-cast v0, Lco/uk/getmondo/common/address/p$a;

    invoke-interface {v0}, Lco/uk/getmondo/common/address/p$a;->f()V

    .line 75
    return-void
.end method

.method public a(Lco/uk/getmondo/common/address/p$a;)V
    .locals 2

    .prologue
    .line 50
    invoke-super {p0, p1}, Lco/uk/getmondo/common/ui/b;->a(Lco/uk/getmondo/common/ui/f;)V

    .line 51
    iget-object v1, p0, Lco/uk/getmondo/common/address/p;->h:Lco/uk/getmondo/common/a;

    iget-boolean v0, p0, Lco/uk/getmondo/common/address/p;->j:Z

    if-eqz v0, :cond_0

    invoke-static {}, Lco/uk/getmondo/api/model/tracking/Impression;->q()Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v0}, Lco/uk/getmondo/common/a;->a(Lco/uk/getmondo/api/model/tracking/Impression;)V

    .line 53
    iget-boolean v0, p0, Lco/uk/getmondo/common/address/p;->j:Z

    if-eqz v0, :cond_1

    .line 54
    iget-object v0, p0, Lco/uk/getmondo/common/address/p;->e:Lco/uk/getmondo/common/accounts/d;

    invoke-virtual {v0}, Lco/uk/getmondo/common/accounts/d;->b()Lco/uk/getmondo/d/ak;

    move-result-object v0

    invoke-virtual {v0}, Lco/uk/getmondo/d/ak;->d()Lco/uk/getmondo/d/ac;

    move-result-object v0

    invoke-virtual {v0}, Lco/uk/getmondo/d/ac;->h()Lco/uk/getmondo/d/s;

    move-result-object v0

    invoke-virtual {v0}, Lco/uk/getmondo/d/s;->c()Ljava/lang/String;

    move-result-object v0

    .line 55
    invoke-interface {p1, v0}, Lco/uk/getmondo/common/address/p$a;->a(Ljava/lang/String;)V

    .line 56
    invoke-direct {p0, v0}, Lco/uk/getmondo/common/address/p;->b(Ljava/lang/String;)V

    .line 60
    :goto_1
    return-void

    .line 51
    :cond_0
    invoke-static {}, Lco/uk/getmondo/api/model/tracking/Impression;->F()Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    goto :goto_0

    .line 58
    :cond_1
    invoke-interface {p1}, Lco/uk/getmondo/common/address/p$a;->a()V

    goto :goto_1
.end method

.method public bridge synthetic a(Lco/uk/getmondo/common/ui/f;)V
    .locals 0

    .prologue
    .line 24
    check-cast p1, Lco/uk/getmondo/common/address/p$a;

    invoke-virtual {p0, p1}, Lco/uk/getmondo/common/address/p;->a(Lco/uk/getmondo/common/address/p$a;)V

    return-void
.end method

.method a(Lco/uk/getmondo/d/s;)V
    .locals 2

    .prologue
    .line 63
    iget-boolean v0, p0, Lco/uk/getmondo/common/address/p;->j:Z

    if-eqz v0, :cond_0

    .line 64
    iget-object v0, p0, Lco/uk/getmondo/common/address/p;->e:Lco/uk/getmondo/common/accounts/d;

    invoke-virtual {v0}, Lco/uk/getmondo/common/accounts/d;->b()Lco/uk/getmondo/d/ak;

    move-result-object v0

    .line 65
    iget-object v1, p0, Lco/uk/getmondo/common/address/p;->e:Lco/uk/getmondo/common/accounts/d;

    invoke-virtual {v0}, Lco/uk/getmondo/d/ak;->d()Lco/uk/getmondo/d/ac;

    move-result-object v0

    invoke-virtual {v0, p1}, Lco/uk/getmondo/d/ac;->a(Lco/uk/getmondo/d/s;)Lco/uk/getmondo/d/ac;

    move-result-object v0

    invoke-virtual {v1, v0}, Lco/uk/getmondo/common/accounts/d;->a(Lco/uk/getmondo/d/ac;)V

    .line 66
    iget-object v0, p0, Lco/uk/getmondo/common/address/p;->a:Lco/uk/getmondo/common/ui/f;

    check-cast v0, Lco/uk/getmondo/common/address/p$a;

    invoke-interface {v0}, Lco/uk/getmondo/common/address/p$a;->e()V

    .line 71
    :goto_0
    return-void

    .line 68
    :cond_0
    iget-object v0, p0, Lco/uk/getmondo/common/address/p;->h:Lco/uk/getmondo/common/a;

    invoke-static {}, Lco/uk/getmondo/api/model/tracking/Impression;->G()Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v1

    invoke-virtual {v0, v1}, Lco/uk/getmondo/common/a;->a(Lco/uk/getmondo/api/model/tracking/Impression;)V

    .line 69
    iget-object v0, p0, Lco/uk/getmondo/common/address/p;->a:Lco/uk/getmondo/common/ui/f;

    check-cast v0, Lco/uk/getmondo/common/address/p$a;

    invoke-interface {v0, p1}, Lco/uk/getmondo/common/address/p$a;->a(Lco/uk/getmondo/d/s;)V

    goto :goto_0
.end method

.method a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 78
    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    .line 79
    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 83
    :goto_0
    return-void

    .line 82
    :cond_0
    invoke-direct {p0, v0}, Lco/uk/getmondo/common/address/p;->b(Ljava/lang/String;)V

    goto :goto_0
.end method
