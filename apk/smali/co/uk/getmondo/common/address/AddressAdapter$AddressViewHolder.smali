.class Lco/uk/getmondo/common/address/AddressAdapter$AddressViewHolder;
.super Landroid/support/v7/widget/RecyclerView$w;
.source "AddressAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lco/uk/getmondo/common/address/AddressAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "AddressViewHolder"
.end annotation


# instance fields
.field final synthetic a:Lco/uk/getmondo/common/address/AddressAdapter;

.field addressLine1:Landroid/widget/TextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f11040f
    .end annotation
.end field

.field addressLine2:Landroid/widget/TextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f110410
    .end annotation
.end field

.field private final b:Lco/uk/getmondo/common/address/AddressAdapter$a;


# direct methods
.method public constructor <init>(Lco/uk/getmondo/common/address/AddressAdapter;Landroid/view/View;Lco/uk/getmondo/common/address/AddressAdapter$a;)V
    .locals 0

    .prologue
    .line 112
    iput-object p1, p0, Lco/uk/getmondo/common/address/AddressAdapter$AddressViewHolder;->a:Lco/uk/getmondo/common/address/AddressAdapter;

    .line 113
    invoke-direct {p0, p2}, Landroid/support/v7/widget/RecyclerView$w;-><init>(Landroid/view/View;)V

    .line 114
    invoke-static {p0, p2}, Lbutterknife/ButterKnife;->bind(Ljava/lang/Object;Landroid/view/View;)Lbutterknife/Unbinder;

    .line 115
    iput-object p3, p0, Lco/uk/getmondo/common/address/AddressAdapter$AddressViewHolder;->b:Lco/uk/getmondo/common/address/AddressAdapter$a;

    .line 116
    return-void
.end method

.method static synthetic a(Lco/uk/getmondo/common/address/AddressAdapter$AddressViewHolder;Lco/uk/getmondo/d/s;Landroid/view/View;)V
    .locals 1

    .prologue
    .line 123
    iget-object v0, p0, Lco/uk/getmondo/common/address/AddressAdapter$AddressViewHolder;->b:Lco/uk/getmondo/common/address/AddressAdapter$a;

    if-nez v0, :cond_0

    .line 127
    :goto_0
    return-void

    .line 126
    :cond_0
    iget-object v0, p0, Lco/uk/getmondo/common/address/AddressAdapter$AddressViewHolder;->b:Lco/uk/getmondo/common/address/AddressAdapter$a;

    invoke-interface {v0, p1}, Lco/uk/getmondo/common/address/AddressAdapter$a;->a(Lco/uk/getmondo/d/s;)V

    goto :goto_0
.end method


# virtual methods
.method public a(Lco/uk/getmondo/d/s;)V
    .locals 6

    .prologue
    .line 119
    invoke-virtual {p1}, Lco/uk/getmondo/d/s;->h()[Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lco/uk/getmondo/common/k/a;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 120
    iget-object v1, p0, Lco/uk/getmondo/common/address/AddressAdapter$AddressViewHolder;->addressLine1:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 121
    iget-object v0, p0, Lco/uk/getmondo/common/address/AddressAdapter$AddressViewHolder;->addressLine2:Landroid/widget/TextView;

    iget-object v1, p0, Lco/uk/getmondo/common/address/AddressAdapter$AddressViewHolder;->itemView:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a00cb

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-virtual {p1}, Lco/uk/getmondo/d/s;->c()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    invoke-virtual {p1}, Lco/uk/getmondo/d/s;->i()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 122
    iget-object v0, p0, Lco/uk/getmondo/common/address/AddressAdapter$AddressViewHolder;->itemView:Landroid/view/View;

    invoke-static {p0, p1}, Lco/uk/getmondo/common/address/a;->a(Lco/uk/getmondo/common/address/AddressAdapter$AddressViewHolder;Lco/uk/getmondo/d/s;)Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 128
    return-void
.end method
