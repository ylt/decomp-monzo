.class Lco/uk/getmondo/common/address/AddressAdapter$EnterAddressViewHolder;
.super Landroid/support/v7/widget/RecyclerView$w;
.source "AddressAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lco/uk/getmondo/common/address/AddressAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "EnterAddressViewHolder"
.end annotation


# instance fields
.field final synthetic a:Lco/uk/getmondo/common/address/AddressAdapter;

.field addressNotInListText:Landroid/widget/TextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f110411
    .end annotation
.end field

.field private final b:Lco/uk/getmondo/common/address/AddressAdapter$b;


# direct methods
.method public constructor <init>(Lco/uk/getmondo/common/address/AddressAdapter;Landroid/view/View;Lco/uk/getmondo/common/address/AddressAdapter$b;)V
    .locals 0

    .prologue
    .line 136
    iput-object p1, p0, Lco/uk/getmondo/common/address/AddressAdapter$EnterAddressViewHolder;->a:Lco/uk/getmondo/common/address/AddressAdapter;

    .line 137
    invoke-direct {p0, p2}, Landroid/support/v7/widget/RecyclerView$w;-><init>(Landroid/view/View;)V

    .line 138
    iput-object p3, p0, Lco/uk/getmondo/common/address/AddressAdapter$EnterAddressViewHolder;->b:Lco/uk/getmondo/common/address/AddressAdapter$b;

    .line 139
    invoke-static {p0, p2}, Lbutterknife/ButterKnife;->bind(Ljava/lang/Object;Landroid/view/View;)Lbutterknife/Unbinder;

    .line 140
    return-void
.end method

.method static synthetic a(Lco/uk/getmondo/common/address/AddressAdapter$EnterAddressViewHolder;Landroid/view/View;)V
    .locals 1

    .prologue
    .line 145
    iget-object v0, p0, Lco/uk/getmondo/common/address/AddressAdapter$EnterAddressViewHolder;->b:Lco/uk/getmondo/common/address/AddressAdapter$b;

    if-nez v0, :cond_0

    .line 149
    :goto_0
    return-void

    .line 148
    :cond_0
    iget-object v0, p0, Lco/uk/getmondo/common/address/AddressAdapter$EnterAddressViewHolder;->b:Lco/uk/getmondo/common/address/AddressAdapter$b;

    invoke-interface {v0}, Lco/uk/getmondo/common/address/AddressAdapter$b;->a()V

    goto :goto_0
.end method


# virtual methods
.method public a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 143
    iget-object v0, p0, Lco/uk/getmondo/common/address/AddressAdapter$EnterAddressViewHolder;->addressNotInListText:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 144
    iget-object v0, p0, Lco/uk/getmondo/common/address/AddressAdapter$EnterAddressViewHolder;->itemView:Landroid/view/View;

    invoke-static {p0}, Lco/uk/getmondo/common/address/b;->a(Lco/uk/getmondo/common/address/AddressAdapter$EnterAddressViewHolder;)Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 150
    return-void
.end method
