.class public Lco/uk/getmondo/common/address/AddressAdapter;
.super Landroid/support/v7/widget/RecyclerView$a;
.source "AddressAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lco/uk/getmondo/common/address/AddressAdapter$EnterAddressViewHolder;,
        Lco/uk/getmondo/common/address/AddressAdapter$AddressViewHolder;,
        Lco/uk/getmondo/common/address/AddressAdapter$b;,
        Lco/uk/getmondo/common/address/AddressAdapter$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/support/v7/widget/RecyclerView$a",
        "<",
        "Landroid/support/v7/widget/RecyclerView$w;",
        ">;"
    }
.end annotation

.annotation runtime Ljava/lang/Deprecated;
.end annotation


# instance fields
.field private final a:Lco/uk/getmondo/common/address/AddressAdapter$a;

.field private final b:Lco/uk/getmondo/common/address/AddressAdapter$b;

.field private final c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lco/uk/getmondo/d/s;",
            ">;"
        }
    .end annotation
.end field

.field private d:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lco/uk/getmondo/common/address/AddressAdapter$a;Lco/uk/getmondo/common/address/AddressAdapter$b;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 29
    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView$a;-><init>()V

    .line 25
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lco/uk/getmondo/common/address/AddressAdapter;->c:Ljava/util/List;

    .line 30
    iput-object p1, p0, Lco/uk/getmondo/common/address/AddressAdapter;->a:Lco/uk/getmondo/common/address/AddressAdapter$a;

    .line 31
    iput-object p2, p0, Lco/uk/getmondo/common/address/AddressAdapter;->b:Lco/uk/getmondo/common/address/AddressAdapter$b;

    .line 32
    iput-object p3, p0, Lco/uk/getmondo/common/address/AddressAdapter;->d:Ljava/lang/String;

    .line 33
    return-void
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    .line 49
    iget-object v0, p0, Lco/uk/getmondo/common/address/AddressAdapter;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    .line 50
    iget-object v1, p0, Lco/uk/getmondo/common/address/AddressAdapter;->c:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 51
    const/4 v1, 0x0

    invoke-virtual {p0, v1, v0}, Lco/uk/getmondo/common/address/AddressAdapter;->notifyItemRangeRemoved(II)V

    .line 52
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 36
    iput-object p1, p0, Lco/uk/getmondo/common/address/AddressAdapter;->d:Ljava/lang/String;

    .line 37
    invoke-virtual {p0}, Lco/uk/getmondo/common/address/AddressAdapter;->getItemCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {p0, v0}, Lco/uk/getmondo/common/address/AddressAdapter;->notifyItemChanged(I)V

    .line 38
    return-void
.end method

.method public a(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lco/uk/getmondo/d/s;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 41
    iget-object v0, p0, Lco/uk/getmondo/common/address/AddressAdapter;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    .line 42
    iget-object v1, p0, Lco/uk/getmondo/common/address/AddressAdapter;->c:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 43
    invoke-virtual {p0, v2, v0}, Lco/uk/getmondo/common/address/AddressAdapter;->notifyItemRangeRemoved(II)V

    .line 44
    iget-object v0, p0, Lco/uk/getmondo/common/address/AddressAdapter;->c:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 45
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    invoke-virtual {p0, v2, v0}, Lco/uk/getmondo/common/address/AddressAdapter;->notifyItemRangeInserted(II)V

    .line 46
    return-void
.end method

.method public getItemCount()I
    .locals 1

    .prologue
    .line 93
    iget-object v0, p0, Lco/uk/getmondo/common/address/AddressAdapter;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    return v0
.end method

.method public getItemViewType(I)I
    .locals 1

    .prologue
    .line 83
    iget-object v0, p0, Lco/uk/getmondo/common/address/AddressAdapter;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ne p1, v0, :cond_0

    .line 84
    const/4 v0, 0x1

    .line 88
    :goto_0
    return v0

    .line 85
    :cond_0
    iget-object v0, p0, Lco/uk/getmondo/common/address/AddressAdapter;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge p1, v0, :cond_1

    .line 86
    const/4 v0, 0x0

    goto :goto_0

    .line 88
    :cond_1
    invoke-super {p0, p1}, Landroid/support/v7/widget/RecyclerView$a;->getItemViewType(I)I

    move-result v0

    goto :goto_0
.end method

.method public onBindViewHolder(Landroid/support/v7/widget/RecyclerView$w;I)V
    .locals 2

    .prologue
    .line 70
    instance-of v0, p1, Lco/uk/getmondo/common/address/AddressAdapter$AddressViewHolder;

    if-eqz v0, :cond_0

    .line 71
    check-cast p1, Lco/uk/getmondo/common/address/AddressAdapter$AddressViewHolder;

    .line 72
    iget-object v0, p0, Lco/uk/getmondo/common/address/AddressAdapter;->c:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/d/s;

    invoke-virtual {p1, v0}, Lco/uk/getmondo/common/address/AddressAdapter$AddressViewHolder;->a(Lco/uk/getmondo/d/s;)V

    .line 79
    :goto_0
    return-void

    .line 73
    :cond_0
    instance-of v0, p1, Lco/uk/getmondo/common/address/AddressAdapter$EnterAddressViewHolder;

    if-eqz v0, :cond_1

    .line 74
    check-cast p1, Lco/uk/getmondo/common/address/AddressAdapter$EnterAddressViewHolder;

    .line 75
    iget-object v0, p0, Lco/uk/getmondo/common/address/AddressAdapter;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lco/uk/getmondo/common/address/AddressAdapter$EnterAddressViewHolder;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 77
    :cond_1
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Unknown view holder type"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public onCreateViewHolder(Landroid/view/ViewGroup;I)Landroid/support/v7/widget/RecyclerView$w;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 56
    packed-switch p2, :pswitch_data_0

    .line 64
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Unknown view type"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 58
    :pswitch_0
    new-instance v0, Lco/uk/getmondo/common/address/AddressAdapter$AddressViewHolder;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f0500f8

    invoke-virtual {v1, v2, p1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    iget-object v2, p0, Lco/uk/getmondo/common/address/AddressAdapter;->a:Lco/uk/getmondo/common/address/AddressAdapter$a;

    invoke-direct {v0, p0, v1, v2}, Lco/uk/getmondo/common/address/AddressAdapter$AddressViewHolder;-><init>(Lco/uk/getmondo/common/address/AddressAdapter;Landroid/view/View;Lco/uk/getmondo/common/address/AddressAdapter$a;)V

    .line 61
    :goto_0
    return-object v0

    :pswitch_1
    new-instance v0, Lco/uk/getmondo/common/address/AddressAdapter$EnterAddressViewHolder;

    .line 62
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f0500f9

    invoke-virtual {v1, v2, p1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    iget-object v2, p0, Lco/uk/getmondo/common/address/AddressAdapter;->b:Lco/uk/getmondo/common/address/AddressAdapter$b;

    invoke-direct {v0, p0, v1, v2}, Lco/uk/getmondo/common/address/AddressAdapter$EnterAddressViewHolder;-><init>(Lco/uk/getmondo/common/address/AddressAdapter;Landroid/view/View;Lco/uk/getmondo/common/address/AddressAdapter$b;)V

    goto :goto_0

    .line 56
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
