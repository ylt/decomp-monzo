.class public Lco/uk/getmondo/common/address/SelectAddressActivity;
.super Lco/uk/getmondo/common/activities/b;
.source "SelectAddressActivity.java"

# interfaces
.implements Lco/uk/getmondo/common/address/p$a;


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# instance fields
.field a:Lco/uk/getmondo/common/address/p;

.field addressesRecyclerView:Landroid/support/v7/widget/RecyclerView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f1101fe
    .end annotation
.end field

.field private b:Lco/uk/getmondo/common/address/AddressAdapter;

.field private c:Lco/uk/getmondo/common/activities/b$a;

.field postalCode:Landroid/widget/EditText;
    .annotation build Lbutterknife/BindView;
        value = 0x7f110146
    .end annotation
.end field

.field postalCodeWrapper:Landroid/support/design/widget/TextInputLayout;
    .annotation build Lbutterknife/BindView;
        value = 0x7f110145
    .end annotation
.end field

.field selectAddressLabel:Landroid/widget/TextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f1101fd
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, Lco/uk/getmondo/common/activities/b;-><init>()V

    return-void
.end method

.method public static a(Landroid/content/Intent;)Lco/uk/getmondo/d/s;
    .locals 1

    .prologue
    .line 55
    const-string v0, "RESULT_EXTRA_ADDRESS"

    invoke-virtual {p0, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/d/s;

    return-object v0
.end method

.method public static a(Landroid/content/Context;Lco/uk/getmondo/common/activities/b$a;Z)V
    .locals 1

    .prologue
    .line 44
    invoke-static {p0, p1, p2}, Lco/uk/getmondo/common/address/SelectAddressActivity;->b(Landroid/content/Context;Lco/uk/getmondo/common/activities/b$a;Z)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 45
    return-void
.end method

.method static synthetic a(Lco/uk/getmondo/common/address/SelectAddressActivity;Landroid/widget/TextView;ILandroid/view/KeyEvent;)Z
    .locals 2

    .prologue
    .line 79
    const/4 v0, 0x3

    if-ne p2, v0, :cond_0

    .line 80
    invoke-virtual {p0, p1}, Lco/uk/getmondo/common/address/SelectAddressActivity;->a(Landroid/view/View;)V

    .line 81
    invoke-virtual {p1}, Landroid/widget/TextView;->clearFocus()V

    .line 82
    iget-object v0, p0, Lco/uk/getmondo/common/address/SelectAddressActivity;->a:Lco/uk/getmondo/common/address/p;

    iget-object v1, p0, Lco/uk/getmondo/common/address/SelectAddressActivity;->postalCode:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lco/uk/getmondo/common/address/p;->a(Ljava/lang/String;)V

    .line 83
    const/4 v0, 0x1

    .line 85
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(Landroid/content/Context;Lco/uk/getmondo/common/activities/b$a;Z)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 48
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lco/uk/getmondo/common/address/SelectAddressActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 49
    const-string v1, "EXTRA_THEME"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 50
    const-string v1, "EXTRA_IS_SIGN_UP_FLOW"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 51
    return-object v0
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    .line 106
    iget-object v0, p0, Lco/uk/getmondo/common/address/SelectAddressActivity;->selectAddressLabel:Landroid/widget/TextView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 107
    return-void
.end method

.method public a(Lco/uk/getmondo/d/s;)V
    .locals 2

    .prologue
    .line 160
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 161
    const-string v1, "RESULT_EXTRA_ADDRESS"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 162
    const/4 v1, -0x1

    invoke-virtual {p0, v1, v0}, Lco/uk/getmondo/common/address/SelectAddressActivity;->setResult(ILandroid/content/Intent;)V

    .line 163
    invoke-virtual {p0}, Lco/uk/getmondo/common/address/SelectAddressActivity;->finish()V

    .line 164
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 139
    iget-object v0, p0, Lco/uk/getmondo/common/address/SelectAddressActivity;->postalCode:Landroid/widget/EditText;

    invoke-virtual {v0, p1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 140
    return-void
.end method

.method public a(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lco/uk/getmondo/d/s;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 131
    iget-object v0, p0, Lco/uk/getmondo/common/address/SelectAddressActivity;->selectAddressLabel:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 132
    iget-object v0, p0, Lco/uk/getmondo/common/address/SelectAddressActivity;->addressesRecyclerView:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setVisibility(I)V

    .line 133
    iget-object v0, p0, Lco/uk/getmondo/common/address/SelectAddressActivity;->b:Lco/uk/getmondo/common/address/AddressAdapter;

    invoke-virtual {v0, p1}, Lco/uk/getmondo/common/address/AddressAdapter;->a(Ljava/util/List;)V

    .line 134
    iget-object v0, p0, Lco/uk/getmondo/common/address/SelectAddressActivity;->addressesRecyclerView:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView;->getLayoutManager()Landroid/support/v7/widget/RecyclerView$h;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView$h;->e(I)V

    .line 135
    return-void
.end method

.method public b()V
    .locals 2

    .prologue
    .line 111
    iget-object v0, p0, Lco/uk/getmondo/common/address/SelectAddressActivity;->selectAddressLabel:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 112
    return-void
.end method

.method public c()V
    .locals 2

    .prologue
    .line 116
    iget-object v0, p0, Lco/uk/getmondo/common/address/SelectAddressActivity;->postalCodeWrapper:Landroid/support/design/widget/TextInputLayout;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/support/design/widget/TextInputLayout;->setErrorEnabled(Z)V

    .line 117
    iget-object v0, p0, Lco/uk/getmondo/common/address/SelectAddressActivity;->postalCodeWrapper:Landroid/support/design/widget/TextInputLayout;

    const v1, 0x7f0a02bf

    invoke-virtual {p0, v1}, Lco/uk/getmondo/common/address/SelectAddressActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/design/widget/TextInputLayout;->setError(Ljava/lang/CharSequence;)V

    .line 118
    iget-object v0, p0, Lco/uk/getmondo/common/address/SelectAddressActivity;->addressesRecyclerView:Landroid/support/v7/widget/RecyclerView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setVisibility(I)V

    .line 119
    iget-object v0, p0, Lco/uk/getmondo/common/address/SelectAddressActivity;->b:Lco/uk/getmondo/common/address/AddressAdapter;

    const v1, 0x7f0a0179

    invoke-virtual {p0, v1}, Lco/uk/getmondo/common/address/SelectAddressActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lco/uk/getmondo/common/address/AddressAdapter;->a(Ljava/lang/String;)V

    .line 120
    return-void
.end method

.method public d()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 124
    iget-object v0, p0, Lco/uk/getmondo/common/address/SelectAddressActivity;->postalCodeWrapper:Landroid/support/design/widget/TextInputLayout;

    invoke-virtual {v0, v1}, Landroid/support/design/widget/TextInputLayout;->setErrorEnabled(Z)V

    .line 125
    iget-object v0, p0, Lco/uk/getmondo/common/address/SelectAddressActivity;->addressesRecyclerView:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setVisibility(I)V

    .line 126
    iget-object v0, p0, Lco/uk/getmondo/common/address/SelectAddressActivity;->b:Lco/uk/getmondo/common/address/AddressAdapter;

    const v1, 0x7f0a00ce

    invoke-virtual {p0, v1}, Lco/uk/getmondo/common/address/SelectAddressActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lco/uk/getmondo/common/address/AddressAdapter;->a(Ljava/lang/String;)V

    .line 127
    return-void
.end method

.method public e()V
    .locals 0

    .prologue
    .line 144
    invoke-static {p0}, Lco/uk/getmondo/create_account/phone_number/EnterPhoneNumberActivity;->a(Landroid/content/Context;)V

    .line 145
    return-void
.end method

.method public f()V
    .locals 2

    .prologue
    .line 149
    const v0, 0x7f0a02be

    invoke-virtual {p0, v0}, Lco/uk/getmondo/common/address/SelectAddressActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/common/address/SelectAddressActivity;->c:Lco/uk/getmondo/common/activities/b$a;

    invoke-static {p0, v0, v1}, Lco/uk/getmondo/common/address/LegacyEnterAddressActivity;->a(Landroid/content/Context;Ljava/lang/String;Lco/uk/getmondo/common/activities/b$a;)Landroid/content/Intent;

    move-result-object v0

    .line 150
    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lco/uk/getmondo/common/address/SelectAddressActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 151
    return-void
.end method

.method public g()V
    .locals 1

    .prologue
    .line 155
    iget-object v0, p0, Lco/uk/getmondo/common/address/SelectAddressActivity;->b:Lco/uk/getmondo/common/address/AddressAdapter;

    invoke-virtual {v0}, Lco/uk/getmondo/common/address/AddressAdapter;->a()V

    .line 156
    return-void
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 2

    .prologue
    .line 168
    invoke-super {p0, p1, p2, p3}, Lco/uk/getmondo/common/activities/b;->onActivityResult(IILandroid/content/Intent;)V

    .line 169
    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    .line 170
    invoke-static {p3}, Lco/uk/getmondo/common/address/LegacyEnterAddressActivity;->a(Landroid/content/Intent;)Lco/uk/getmondo/d/s;

    move-result-object v0

    .line 171
    iget-object v1, p0, Lco/uk/getmondo/common/address/SelectAddressActivity;->a:Lco/uk/getmondo/common/address/p;

    invoke-virtual {v1, v0}, Lco/uk/getmondo/common/address/p;->a(Lco/uk/getmondo/d/s;)V

    .line 173
    :cond_0
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 60
    invoke-virtual {p0}, Lco/uk/getmondo/common/address/SelectAddressActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "EXTRA_THEME"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/common/activities/b$a;

    iput-object v0, p0, Lco/uk/getmondo/common/address/SelectAddressActivity;->c:Lco/uk/getmondo/common/activities/b$a;

    .line 61
    iget-object v0, p0, Lco/uk/getmondo/common/address/SelectAddressActivity;->c:Lco/uk/getmondo/common/activities/b$a;

    if-eqz v0, :cond_0

    .line 62
    iget-object v0, p0, Lco/uk/getmondo/common/address/SelectAddressActivity;->c:Lco/uk/getmondo/common/activities/b$a;

    sget-object v1, Lco/uk/getmondo/common/activities/b$a;->b:Lco/uk/getmondo/common/activities/b$a;

    if-ne v0, v1, :cond_2

    const v0, 0x7f0c0128

    .line 63
    :goto_0
    invoke-virtual {p0, v0}, Lco/uk/getmondo/common/address/SelectAddressActivity;->setTheme(I)V

    .line 66
    :cond_0
    invoke-super {p0, p1}, Lco/uk/getmondo/common/activities/b;->onCreate(Landroid/os/Bundle;)V

    .line 67
    const v0, 0x7f05005c

    invoke-virtual {p0, v0}, Lco/uk/getmondo/common/address/SelectAddressActivity;->setContentView(I)V

    .line 68
    invoke-static {p0}, Lbutterknife/ButterKnife;->bind(Landroid/app/Activity;)Lbutterknife/Unbinder;

    .line 70
    invoke-virtual {p0}, Lco/uk/getmondo/common/address/SelectAddressActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "EXTRA_IS_SIGN_UP_FLOW"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    .line 71
    invoke-virtual {p0}, Lco/uk/getmondo/common/address/SelectAddressActivity;->l()Lco/uk/getmondo/common/h/b/b;

    move-result-object v1

    new-instance v2, Lco/uk/getmondo/common/address/n;

    invoke-direct {v2, v0}, Lco/uk/getmondo/common/address/n;-><init>(Z)V

    invoke-interface {v1, v2}, Lco/uk/getmondo/common/h/b/b;->a(Lco/uk/getmondo/common/address/n;)Lco/uk/getmondo/common/address/m;

    move-result-object v1

    invoke-interface {v1, p0}, Lco/uk/getmondo/common/address/m;->a(Lco/uk/getmondo/common/address/SelectAddressActivity;)V

    .line 73
    if-nez v0, :cond_1

    .line 74
    invoke-virtual {p0}, Lco/uk/getmondo/common/address/SelectAddressActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/Window;->setSoftInputMode(I)V

    .line 75
    iget-object v0, p0, Lco/uk/getmondo/common/address/SelectAddressActivity;->postalCode:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->requestFocus()Z

    .line 78
    :cond_1
    iget-object v0, p0, Lco/uk/getmondo/common/address/SelectAddressActivity;->postalCode:Landroid/widget/EditText;

    invoke-static {p0}, Lco/uk/getmondo/common/address/i;->a(Lco/uk/getmondo/common/address/SelectAddressActivity;)Landroid/widget/TextView$OnEditorActionListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 88
    new-instance v0, Lco/uk/getmondo/common/address/AddressAdapter;

    iget-object v1, p0, Lco/uk/getmondo/common/address/SelectAddressActivity;->a:Lco/uk/getmondo/common/address/p;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    invoke-static {v1}, Lco/uk/getmondo/common/address/j;->a(Lco/uk/getmondo/common/address/p;)Lco/uk/getmondo/common/address/AddressAdapter$a;

    move-result-object v1

    iget-object v2, p0, Lco/uk/getmondo/common/address/SelectAddressActivity;->a:Lco/uk/getmondo/common/address/p;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    invoke-static {v2}, Lco/uk/getmondo/common/address/k;->a(Lco/uk/getmondo/common/address/p;)Lco/uk/getmondo/common/address/AddressAdapter$b;

    move-result-object v2

    const v3, 0x7f0a00ce

    .line 89
    invoke-virtual {p0, v3}, Lco/uk/getmondo/common/address/SelectAddressActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lco/uk/getmondo/common/address/AddressAdapter;-><init>(Lco/uk/getmondo/common/address/AddressAdapter$a;Lco/uk/getmondo/common/address/AddressAdapter$b;Ljava/lang/String;)V

    iput-object v0, p0, Lco/uk/getmondo/common/address/SelectAddressActivity;->b:Lco/uk/getmondo/common/address/AddressAdapter;

    .line 90
    iget-object v0, p0, Lco/uk/getmondo/common/address/SelectAddressActivity;->addressesRecyclerView:Landroid/support/v7/widget/RecyclerView;

    new-instance v1, Landroid/support/v7/widget/LinearLayoutManager;

    invoke-direct {v1, p0}, Landroid/support/v7/widget/LinearLayoutManager;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(Landroid/support/v7/widget/RecyclerView$h;)V

    .line 91
    iget-object v0, p0, Lco/uk/getmondo/common/address/SelectAddressActivity;->addressesRecyclerView:Landroid/support/v7/widget/RecyclerView;

    iget-object v1, p0, Lco/uk/getmondo/common/address/SelectAddressActivity;->b:Lco/uk/getmondo/common/address/AddressAdapter;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setAdapter(Landroid/support/v7/widget/RecyclerView$a;)V

    .line 93
    iget-object v0, p0, Lco/uk/getmondo/common/address/SelectAddressActivity;->a:Lco/uk/getmondo/common/address/p;

    invoke-virtual {v0, p0}, Lco/uk/getmondo/common/address/p;->a(Lco/uk/getmondo/common/address/p$a;)V

    .line 95
    const v0, 0x7f0a03d7

    invoke-virtual {p0, v0}, Lco/uk/getmondo/common/address/SelectAddressActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lco/uk/getmondo/common/address/SelectAddressActivity;->setTitle(Ljava/lang/CharSequence;)V

    .line 96
    return-void

    .line 62
    :cond_2
    const v0, 0x7f0c0125

    goto/16 :goto_0
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 100
    iget-object v0, p0, Lco/uk/getmondo/common/address/SelectAddressActivity;->a:Lco/uk/getmondo/common/address/p;

    invoke-virtual {v0}, Lco/uk/getmondo/common/address/p;->b()V

    .line 101
    invoke-super {p0}, Lco/uk/getmondo/common/activities/b;->onDestroy()V

    .line 102
    return-void
.end method
