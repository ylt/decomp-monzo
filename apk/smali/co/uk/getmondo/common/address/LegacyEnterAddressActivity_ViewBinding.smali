.class public Lco/uk/getmondo/common/address/LegacyEnterAddressActivity_ViewBinding;
.super Ljava/lang/Object;
.source "LegacyEnterAddressActivity_ViewBinding.java"

# interfaces
.implements Lbutterknife/Unbinder;


# instance fields
.field private a:Lco/uk/getmondo/common/address/LegacyEnterAddressActivity;


# direct methods
.method public constructor <init>(Lco/uk/getmondo/common/address/LegacyEnterAddressActivity;Landroid/view/View;)V
    .locals 3

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    iput-object p1, p0, Lco/uk/getmondo/common/address/LegacyEnterAddressActivity_ViewBinding;->a:Lco/uk/getmondo/common/address/LegacyEnterAddressActivity;

    .line 28
    const v0, 0x7f110161

    const-string v1, "field \'postalCodeWrapper\'"

    const-class v2, Landroid/support/design/widget/TextInputLayout;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/design/widget/TextInputLayout;

    iput-object v0, p1, Lco/uk/getmondo/common/address/LegacyEnterAddressActivity;->postalCodeWrapper:Landroid/support/design/widget/TextInputLayout;

    .line 29
    const v0, 0x7f110162

    const-string v1, "field \'postalCodeView\'"

    const-class v2, Landroid/widget/EditText;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p1, Lco/uk/getmondo/common/address/LegacyEnterAddressActivity;->postalCodeView:Landroid/widget/EditText;

    .line 30
    const v0, 0x7f110160

    const-string v1, "field \'streetAddressView\'"

    const-class v2, Landroid/widget/EditText;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p1, Lco/uk/getmondo/common/address/LegacyEnterAddressActivity;->streetAddressView:Landroid/widget/EditText;

    .line 31
    const v0, 0x7f11015f

    const-string v1, "field \'streetAddressWrapper\'"

    const-class v2, Landroid/support/design/widget/TextInputLayout;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/design/widget/TextInputLayout;

    iput-object v0, p1, Lco/uk/getmondo/common/address/LegacyEnterAddressActivity;->streetAddressWrapper:Landroid/support/design/widget/TextInputLayout;

    .line 32
    const v0, 0x7f110163

    const-string v1, "field \'cityWrapper\'"

    const-class v2, Landroid/support/design/widget/TextInputLayout;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/design/widget/TextInputLayout;

    iput-object v0, p1, Lco/uk/getmondo/common/address/LegacyEnterAddressActivity;->cityWrapper:Landroid/support/design/widget/TextInputLayout;

    .line 33
    const v0, 0x7f110164

    const-string v1, "field \'cityView\'"

    const-class v2, Landroid/widget/EditText;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p1, Lco/uk/getmondo/common/address/LegacyEnterAddressActivity;->cityView:Landroid/widget/EditText;

    .line 34
    const v0, 0x7f110165

    const-string v1, "field \'countryView\'"

    const-class v2, Landroid/widget/EditText;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p1, Lco/uk/getmondo/common/address/LegacyEnterAddressActivity;->countryView:Landroid/widget/EditText;

    .line 35
    const v0, 0x7f110166

    const-string v1, "field \'submitButton\'"

    const-class v2, Landroid/widget/Button;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p1, Lco/uk/getmondo/common/address/LegacyEnterAddressActivity;->submitButton:Landroid/widget/Button;

    .line 36
    return-void
.end method


# virtual methods
.method public unbind()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 41
    iget-object v0, p0, Lco/uk/getmondo/common/address/LegacyEnterAddressActivity_ViewBinding;->a:Lco/uk/getmondo/common/address/LegacyEnterAddressActivity;

    .line 42
    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Bindings already cleared."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 43
    :cond_0
    iput-object v1, p0, Lco/uk/getmondo/common/address/LegacyEnterAddressActivity_ViewBinding;->a:Lco/uk/getmondo/common/address/LegacyEnterAddressActivity;

    .line 45
    iput-object v1, v0, Lco/uk/getmondo/common/address/LegacyEnterAddressActivity;->postalCodeWrapper:Landroid/support/design/widget/TextInputLayout;

    .line 46
    iput-object v1, v0, Lco/uk/getmondo/common/address/LegacyEnterAddressActivity;->postalCodeView:Landroid/widget/EditText;

    .line 47
    iput-object v1, v0, Lco/uk/getmondo/common/address/LegacyEnterAddressActivity;->streetAddressView:Landroid/widget/EditText;

    .line 48
    iput-object v1, v0, Lco/uk/getmondo/common/address/LegacyEnterAddressActivity;->streetAddressWrapper:Landroid/support/design/widget/TextInputLayout;

    .line 49
    iput-object v1, v0, Lco/uk/getmondo/common/address/LegacyEnterAddressActivity;->cityWrapper:Landroid/support/design/widget/TextInputLayout;

    .line 50
    iput-object v1, v0, Lco/uk/getmondo/common/address/LegacyEnterAddressActivity;->cityView:Landroid/widget/EditText;

    .line 51
    iput-object v1, v0, Lco/uk/getmondo/common/address/LegacyEnterAddressActivity;->countryView:Landroid/widget/EditText;

    .line 52
    iput-object v1, v0, Lco/uk/getmondo/common/address/LegacyEnterAddressActivity;->submitButton:Landroid/widget/Button;

    .line 53
    return-void
.end method
