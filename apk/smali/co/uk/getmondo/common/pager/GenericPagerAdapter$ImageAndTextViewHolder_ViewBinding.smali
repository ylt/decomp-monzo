.class public Lco/uk/getmondo/common/pager/GenericPagerAdapter$ImageAndTextViewHolder_ViewBinding;
.super Ljava/lang/Object;
.source "GenericPagerAdapter$ImageAndTextViewHolder_ViewBinding.java"

# interfaces
.implements Lbutterknife/Unbinder;


# instance fields
.field private a:Lco/uk/getmondo/common/pager/GenericPagerAdapter$ImageAndTextViewHolder;


# direct methods
.method public constructor <init>(Lco/uk/getmondo/common/pager/GenericPagerAdapter$ImageAndTextViewHolder;Landroid/view/View;)V
    .locals 3

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    iput-object p1, p0, Lco/uk/getmondo/common/pager/GenericPagerAdapter$ImageAndTextViewHolder_ViewBinding;->a:Lco/uk/getmondo/common/pager/GenericPagerAdapter$ImageAndTextViewHolder;

    .line 22
    const v0, 0x7f1100ce

    const-string v1, "field \'imageView\'"

    const-class v2, Landroid/widget/ImageView;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p1, Lco/uk/getmondo/common/pager/GenericPagerAdapter$ImageAndTextViewHolder;->imageView:Landroid/widget/ImageView;

    .line 23
    const v0, 0x7f1101e2

    const-string v1, "field \'titleTextView\'"

    const-class v2, Landroid/widget/TextView;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p1, Lco/uk/getmondo/common/pager/GenericPagerAdapter$ImageAndTextViewHolder;->titleTextView:Landroid/widget/TextView;

    .line 24
    const v0, 0x7f11023f

    const-string v1, "field \'contentTextView\'"

    const-class v2, Landroid/widget/TextView;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p1, Lco/uk/getmondo/common/pager/GenericPagerAdapter$ImageAndTextViewHolder;->contentTextView:Landroid/widget/TextView;

    .line 25
    const v0, 0x7f110461

    const-string v1, "field \'footerTextView\'"

    const-class v2, Landroid/widget/TextView;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p1, Lco/uk/getmondo/common/pager/GenericPagerAdapter$ImageAndTextViewHolder;->footerTextView:Landroid/widget/TextView;

    .line 26
    const v0, 0x7f1100d3

    const-string v1, "field \'spacerView\'"

    invoke-static {p2, v0, v1}, Lbutterknife/internal/Utils;->findRequiredView(Landroid/view/View;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p1, Lco/uk/getmondo/common/pager/GenericPagerAdapter$ImageAndTextViewHolder;->spacerView:Landroid/view/View;

    .line 27
    return-void
.end method


# virtual methods
.method public unbind()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 32
    iget-object v0, p0, Lco/uk/getmondo/common/pager/GenericPagerAdapter$ImageAndTextViewHolder_ViewBinding;->a:Lco/uk/getmondo/common/pager/GenericPagerAdapter$ImageAndTextViewHolder;

    .line 33
    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Bindings already cleared."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 34
    :cond_0
    iput-object v1, p0, Lco/uk/getmondo/common/pager/GenericPagerAdapter$ImageAndTextViewHolder_ViewBinding;->a:Lco/uk/getmondo/common/pager/GenericPagerAdapter$ImageAndTextViewHolder;

    .line 36
    iput-object v1, v0, Lco/uk/getmondo/common/pager/GenericPagerAdapter$ImageAndTextViewHolder;->imageView:Landroid/widget/ImageView;

    .line 37
    iput-object v1, v0, Lco/uk/getmondo/common/pager/GenericPagerAdapter$ImageAndTextViewHolder;->titleTextView:Landroid/widget/TextView;

    .line 38
    iput-object v1, v0, Lco/uk/getmondo/common/pager/GenericPagerAdapter$ImageAndTextViewHolder;->contentTextView:Landroid/widget/TextView;

    .line 39
    iput-object v1, v0, Lco/uk/getmondo/common/pager/GenericPagerAdapter$ImageAndTextViewHolder;->footerTextView:Landroid/widget/TextView;

    .line 40
    iput-object v1, v0, Lco/uk/getmondo/common/pager/GenericPagerAdapter$ImageAndTextViewHolder;->spacerView:Landroid/view/View;

    .line 41
    return-void
.end method
