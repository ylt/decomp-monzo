.class Lco/uk/getmondo/common/pager/GenericPagerAdapter$TitleAndImageViewHolder;
.super Lco/uk/getmondo/common/pager/g;
.source "GenericPagerAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lco/uk/getmondo/common/pager/GenericPagerAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "TitleAndImageViewHolder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lco/uk/getmondo/common/pager/g",
        "<",
        "Lco/uk/getmondo/common/pager/f$e;",
        ">;"
    }
.end annotation


# instance fields
.field imageView:Landroid/widget/ImageView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f1100ce
    .end annotation
.end field

.field titleTextView:Landroid/widget/TextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f1101e2
    .end annotation
.end field


# direct methods
.method constructor <init>(Landroid/view/View;)V
    .locals 0

    .prologue
    .line 120
    invoke-direct {p0, p1}, Lco/uk/getmondo/common/pager/g;-><init>(Landroid/view/View;)V

    .line 121
    invoke-static {p0, p1}, Lbutterknife/ButterKnife;->bind(Ljava/lang/Object;Landroid/view/View;)Lbutterknife/Unbinder;

    .line 122
    return-void
.end method


# virtual methods
.method a(Lco/uk/getmondo/common/pager/f$e;)Landroid/view/View;
    .locals 2

    .prologue
    .line 126
    iget-object v0, p0, Lco/uk/getmondo/common/pager/GenericPagerAdapter$TitleAndImageViewHolder;->imageView:Landroid/widget/ImageView;

    invoke-interface {p1}, Lco/uk/getmondo/common/pager/f$e;->c()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 127
    iget-object v0, p0, Lco/uk/getmondo/common/pager/GenericPagerAdapter$TitleAndImageViewHolder;->titleTextView:Landroid/widget/TextView;

    invoke-interface {p1}, Lco/uk/getmondo/common/pager/f$e;->b()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 128
    iget-object v0, p0, Lco/uk/getmondo/common/pager/GenericPagerAdapter$TitleAndImageViewHolder;->a:Landroid/view/View;

    return-object v0
.end method
