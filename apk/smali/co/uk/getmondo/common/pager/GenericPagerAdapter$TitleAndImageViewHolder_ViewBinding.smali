.class public Lco/uk/getmondo/common/pager/GenericPagerAdapter$TitleAndImageViewHolder_ViewBinding;
.super Ljava/lang/Object;
.source "GenericPagerAdapter$TitleAndImageViewHolder_ViewBinding.java"

# interfaces
.implements Lbutterknife/Unbinder;


# instance fields
.field private a:Lco/uk/getmondo/common/pager/GenericPagerAdapter$TitleAndImageViewHolder;


# direct methods
.method public constructor <init>(Lco/uk/getmondo/common/pager/GenericPagerAdapter$TitleAndImageViewHolder;Landroid/view/View;)V
    .locals 3

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    iput-object p1, p0, Lco/uk/getmondo/common/pager/GenericPagerAdapter$TitleAndImageViewHolder_ViewBinding;->a:Lco/uk/getmondo/common/pager/GenericPagerAdapter$TitleAndImageViewHolder;

    .line 22
    const v0, 0x7f1101e2

    const-string v1, "field \'titleTextView\'"

    const-class v2, Landroid/widget/TextView;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p1, Lco/uk/getmondo/common/pager/GenericPagerAdapter$TitleAndImageViewHolder;->titleTextView:Landroid/widget/TextView;

    .line 23
    const v0, 0x7f1100ce

    const-string v1, "field \'imageView\'"

    const-class v2, Landroid/widget/ImageView;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p1, Lco/uk/getmondo/common/pager/GenericPagerAdapter$TitleAndImageViewHolder;->imageView:Landroid/widget/ImageView;

    .line 24
    return-void
.end method


# virtual methods
.method public unbind()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 29
    iget-object v0, p0, Lco/uk/getmondo/common/pager/GenericPagerAdapter$TitleAndImageViewHolder_ViewBinding;->a:Lco/uk/getmondo/common/pager/GenericPagerAdapter$TitleAndImageViewHolder;

    .line 30
    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Bindings already cleared."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 31
    :cond_0
    iput-object v1, p0, Lco/uk/getmondo/common/pager/GenericPagerAdapter$TitleAndImageViewHolder_ViewBinding;->a:Lco/uk/getmondo/common/pager/GenericPagerAdapter$TitleAndImageViewHolder;

    .line 33
    iput-object v1, v0, Lco/uk/getmondo/common/pager/GenericPagerAdapter$TitleAndImageViewHolder;->titleTextView:Landroid/widget/TextView;

    .line 34
    iput-object v1, v0, Lco/uk/getmondo/common/pager/GenericPagerAdapter$TitleAndImageViewHolder;->imageView:Landroid/widget/ImageView;

    .line 35
    return-void
.end method
