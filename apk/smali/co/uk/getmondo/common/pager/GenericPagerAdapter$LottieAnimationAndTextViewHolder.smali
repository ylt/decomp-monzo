.class Lco/uk/getmondo/common/pager/GenericPagerAdapter$LottieAnimationAndTextViewHolder;
.super Lco/uk/getmondo/common/pager/g;
.source "GenericPagerAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lco/uk/getmondo/common/pager/GenericPagerAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "LottieAnimationAndTextViewHolder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lco/uk/getmondo/common/pager/g",
        "<",
        "Lco/uk/getmondo/common/pager/f$d;",
        ">;"
    }
.end annotation


# instance fields
.field contentTextView:Landroid/widget/TextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f11023f
    .end annotation
.end field

.field lottieAnimationView:Lcom/airbnb/lottie/LottieAnimationView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f110462
    .end annotation
.end field

.field titleTextView:Landroid/widget/TextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f1101e2
    .end annotation
.end field


# direct methods
.method constructor <init>(Landroid/view/View;)V
    .locals 0

    .prologue
    .line 138
    invoke-direct {p0, p1}, Lco/uk/getmondo/common/pager/g;-><init>(Landroid/view/View;)V

    .line 139
    invoke-static {p0, p1}, Lbutterknife/ButterKnife;->bind(Ljava/lang/Object;Landroid/view/View;)Lbutterknife/Unbinder;

    .line 140
    return-void
.end method


# virtual methods
.method a(Lco/uk/getmondo/common/pager/f$d;)Landroid/view/View;
    .locals 2

    .prologue
    .line 144
    iget-object v0, p0, Lco/uk/getmondo/common/pager/GenericPagerAdapter$LottieAnimationAndTextViewHolder;->lottieAnimationView:Lcom/airbnb/lottie/LottieAnimationView;

    invoke-interface {p1}, Lco/uk/getmondo/common/pager/f$d;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/airbnb/lottie/LottieAnimationView;->setAnimation(Ljava/lang/String;)V

    .line 145
    iget-object v0, p0, Lco/uk/getmondo/common/pager/GenericPagerAdapter$LottieAnimationAndTextViewHolder;->titleTextView:Landroid/widget/TextView;

    invoke-interface {p1}, Lco/uk/getmondo/common/pager/f$d;->c()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 146
    iget-object v0, p0, Lco/uk/getmondo/common/pager/GenericPagerAdapter$LottieAnimationAndTextViewHolder;->contentTextView:Landroid/widget/TextView;

    invoke-interface {p1}, Lco/uk/getmondo/common/pager/f$d;->d()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 147
    iget-object v0, p0, Lco/uk/getmondo/common/pager/GenericPagerAdapter$LottieAnimationAndTextViewHolder;->a:Landroid/view/View;

    return-object v0
.end method
