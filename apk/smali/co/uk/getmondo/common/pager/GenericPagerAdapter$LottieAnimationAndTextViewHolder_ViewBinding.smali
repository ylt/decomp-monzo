.class public Lco/uk/getmondo/common/pager/GenericPagerAdapter$LottieAnimationAndTextViewHolder_ViewBinding;
.super Ljava/lang/Object;
.source "GenericPagerAdapter$LottieAnimationAndTextViewHolder_ViewBinding.java"

# interfaces
.implements Lbutterknife/Unbinder;


# instance fields
.field private a:Lco/uk/getmondo/common/pager/GenericPagerAdapter$LottieAnimationAndTextViewHolder;


# direct methods
.method public constructor <init>(Lco/uk/getmondo/common/pager/GenericPagerAdapter$LottieAnimationAndTextViewHolder;Landroid/view/View;)V
    .locals 3

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    iput-object p1, p0, Lco/uk/getmondo/common/pager/GenericPagerAdapter$LottieAnimationAndTextViewHolder_ViewBinding;->a:Lco/uk/getmondo/common/pager/GenericPagerAdapter$LottieAnimationAndTextViewHolder;

    .line 22
    const v0, 0x7f1101e2

    const-string v1, "field \'titleTextView\'"

    const-class v2, Landroid/widget/TextView;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p1, Lco/uk/getmondo/common/pager/GenericPagerAdapter$LottieAnimationAndTextViewHolder;->titleTextView:Landroid/widget/TextView;

    .line 23
    const v0, 0x7f11023f

    const-string v1, "field \'contentTextView\'"

    const-class v2, Landroid/widget/TextView;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p1, Lco/uk/getmondo/common/pager/GenericPagerAdapter$LottieAnimationAndTextViewHolder;->contentTextView:Landroid/widget/TextView;

    .line 24
    const v0, 0x7f110462

    const-string v1, "field \'lottieAnimationView\'"

    const-class v2, Lcom/airbnb/lottie/LottieAnimationView;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/airbnb/lottie/LottieAnimationView;

    iput-object v0, p1, Lco/uk/getmondo/common/pager/GenericPagerAdapter$LottieAnimationAndTextViewHolder;->lottieAnimationView:Lcom/airbnb/lottie/LottieAnimationView;

    .line 25
    return-void
.end method


# virtual methods
.method public unbind()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 30
    iget-object v0, p0, Lco/uk/getmondo/common/pager/GenericPagerAdapter$LottieAnimationAndTextViewHolder_ViewBinding;->a:Lco/uk/getmondo/common/pager/GenericPagerAdapter$LottieAnimationAndTextViewHolder;

    .line 31
    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Bindings already cleared."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 32
    :cond_0
    iput-object v1, p0, Lco/uk/getmondo/common/pager/GenericPagerAdapter$LottieAnimationAndTextViewHolder_ViewBinding;->a:Lco/uk/getmondo/common/pager/GenericPagerAdapter$LottieAnimationAndTextViewHolder;

    .line 34
    iput-object v1, v0, Lco/uk/getmondo/common/pager/GenericPagerAdapter$LottieAnimationAndTextViewHolder;->titleTextView:Landroid/widget/TextView;

    .line 35
    iput-object v1, v0, Lco/uk/getmondo/common/pager/GenericPagerAdapter$LottieAnimationAndTextViewHolder;->contentTextView:Landroid/widget/TextView;

    .line 36
    iput-object v1, v0, Lco/uk/getmondo/common/pager/GenericPagerAdapter$LottieAnimationAndTextViewHolder;->lottieAnimationView:Lcom/airbnb/lottie/LottieAnimationView;

    .line 37
    return-void
.end method
