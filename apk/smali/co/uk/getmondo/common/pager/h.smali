.class public Lco/uk/getmondo/common/pager/h;
.super Ljava/lang/Object;
.source "PageViewTracker.java"


# instance fields
.field private final a:Lco/uk/getmondo/common/a;

.field private b:Landroid/support/v4/view/ViewPager;

.field private final c:Landroid/support/v4/view/ViewPager$f;


# direct methods
.method public constructor <init>(Lco/uk/getmondo/common/a;)V
    .locals 1

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    new-instance v0, Lco/uk/getmondo/common/pager/h$1;

    invoke-direct {v0, p0}, Lco/uk/getmondo/common/pager/h$1;-><init>(Lco/uk/getmondo/common/pager/h;)V

    iput-object v0, p0, Lco/uk/getmondo/common/pager/h;->c:Landroid/support/v4/view/ViewPager$f;

    .line 27
    iput-object p1, p0, Lco/uk/getmondo/common/pager/h;->a:Lco/uk/getmondo/common/a;

    .line 28
    return-void
.end method

.method private a(I)V
    .locals 3

    .prologue
    .line 47
    if-gez p1, :cond_1

    .line 67
    :cond_0
    :goto_0
    return-void

    .line 51
    :cond_1
    iget-object v0, p0, Lco/uk/getmondo/common/pager/h;->b:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0}, Landroid/support/v4/view/ViewPager;->getAdapter()Landroid/support/v4/view/p;

    move-result-object v0

    .line 52
    if-nez v0, :cond_2

    .line 53
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "The ViewPager must have an adapter"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 54
    :cond_2
    instance-of v1, v0, Lco/uk/getmondo/common/pager/GenericPagerAdapter;

    if-nez v1, :cond_3

    .line 55
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "PageViewTracker requires an adapter of type "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-class v2, Lco/uk/getmondo/common/pager/GenericPagerAdapter;

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 58
    :cond_3
    check-cast v0, Lco/uk/getmondo/common/pager/GenericPagerAdapter;

    .line 59
    invoke-virtual {v0}, Lco/uk/getmondo/common/pager/GenericPagerAdapter;->b()I

    move-result v1

    if-ge p1, v1, :cond_0

    .line 63
    iget-object v0, v0, Lco/uk/getmondo/common/pager/GenericPagerAdapter;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/common/pager/f;

    .line 64
    invoke-interface {v0}, Lco/uk/getmondo/common/pager/f;->a()Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 65
    iget-object v1, p0, Lco/uk/getmondo/common/pager/h;->a:Lco/uk/getmondo/common/a;

    invoke-interface {v0}, Lco/uk/getmondo/common/pager/f;->a()Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    invoke-virtual {v1, v0}, Lco/uk/getmondo/common/a;->a(Lco/uk/getmondo/api/model/tracking/Impression;)V

    goto :goto_0
.end method

.method static synthetic a(Lco/uk/getmondo/common/pager/h;I)V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0, p1}, Lco/uk/getmondo/common/pager/h;->a(I)V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    .line 38
    iget-object v0, p0, Lco/uk/getmondo/common/pager/h;->b:Landroid/support/v4/view/ViewPager;

    if-nez v0, :cond_0

    .line 44
    :goto_0
    return-void

    .line 42
    :cond_0
    iget-object v0, p0, Lco/uk/getmondo/common/pager/h;->b:Landroid/support/v4/view/ViewPager;

    iget-object v1, p0, Lco/uk/getmondo/common/pager/h;->c:Landroid/support/v4/view/ViewPager$f;

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->b(Landroid/support/v4/view/ViewPager$f;)V

    .line 43
    const/4 v0, 0x0

    iput-object v0, p0, Lco/uk/getmondo/common/pager/h;->b:Landroid/support/v4/view/ViewPager;

    goto :goto_0
.end method

.method public a(Landroid/support/v4/view/ViewPager;)V
    .locals 1

    .prologue
    .line 31
    invoke-virtual {p0}, Lco/uk/getmondo/common/pager/h;->a()V

    .line 32
    iput-object p1, p0, Lco/uk/getmondo/common/pager/h;->b:Landroid/support/v4/view/ViewPager;

    .line 33
    invoke-virtual {p1}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v0

    invoke-direct {p0, v0}, Lco/uk/getmondo/common/pager/h;->a(I)V

    .line 34
    iget-object v0, p0, Lco/uk/getmondo/common/pager/h;->c:Landroid/support/v4/view/ViewPager$f;

    invoke-virtual {p1, v0}, Landroid/support/v4/view/ViewPager;->a(Landroid/support/v4/view/ViewPager$f;)V

    .line 35
    return-void
.end method
