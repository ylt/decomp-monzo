.class Lco/uk/getmondo/common/pager/GenericPagerAdapter$ImageAndTextViewHolder;
.super Lco/uk/getmondo/common/pager/g;
.source "GenericPagerAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lco/uk/getmondo/common/pager/GenericPagerAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "ImageAndTextViewHolder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lco/uk/getmondo/common/pager/g",
        "<",
        "Lco/uk/getmondo/common/pager/f$c;",
        ">;"
    }
.end annotation


# instance fields
.field contentTextView:Landroid/widget/TextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f11023f
    .end annotation
.end field

.field footerTextView:Landroid/widget/TextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f110461
    .end annotation
.end field

.field imageView:Landroid/widget/ImageView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f1100ce
    .end annotation
.end field

.field spacerView:Landroid/view/View;
    .annotation build Lbutterknife/BindView;
        value = 0x7f1100d3
    .end annotation
.end field

.field titleTextView:Landroid/widget/TextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f1101e2
    .end annotation
.end field


# direct methods
.method constructor <init>(Landroid/view/View;)V
    .locals 0

    .prologue
    .line 93
    invoke-direct {p0, p1}, Lco/uk/getmondo/common/pager/g;-><init>(Landroid/view/View;)V

    .line 94
    invoke-static {p0, p1}, Lbutterknife/ButterKnife;->bind(Ljava/lang/Object;Landroid/view/View;)Lbutterknife/Unbinder;

    .line 95
    return-void
.end method


# virtual methods
.method a(Lco/uk/getmondo/common/pager/f$c;)Landroid/view/View;
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/4 v2, 0x0

    .line 99
    iget-object v0, p0, Lco/uk/getmondo/common/pager/GenericPagerAdapter$ImageAndTextViewHolder;->imageView:Landroid/widget/ImageView;

    invoke-interface {p1}, Lco/uk/getmondo/common/pager/f$c;->b()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 100
    iget-object v0, p0, Lco/uk/getmondo/common/pager/GenericPagerAdapter$ImageAndTextViewHolder;->titleTextView:Landroid/widget/TextView;

    invoke-interface {p1}, Lco/uk/getmondo/common/pager/f$c;->c()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 101
    iget-object v0, p0, Lco/uk/getmondo/common/pager/GenericPagerAdapter$ImageAndTextViewHolder;->contentTextView:Landroid/widget/TextView;

    invoke-interface {p1}, Lco/uk/getmondo/common/pager/f$c;->d()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 102
    invoke-interface {p1}, Lco/uk/getmondo/common/pager/f$c;->e()Ljava/lang/Integer;

    move-result-object v0

    if-nez v0, :cond_0

    .line 103
    iget-object v0, p0, Lco/uk/getmondo/common/pager/GenericPagerAdapter$ImageAndTextViewHolder;->footerTextView:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 104
    iget-object v0, p0, Lco/uk/getmondo/common/pager/GenericPagerAdapter$ImageAndTextViewHolder;->spacerView:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 111
    :goto_0
    iget-object v0, p0, Lco/uk/getmondo/common/pager/GenericPagerAdapter$ImageAndTextViewHolder;->a:Landroid/view/View;

    return-object v0

    .line 106
    :cond_0
    iget-object v0, p0, Lco/uk/getmondo/common/pager/GenericPagerAdapter$ImageAndTextViewHolder;->footerTextView:Landroid/widget/TextView;

    invoke-interface {p1}, Lco/uk/getmondo/common/pager/f$c;->e()Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 107
    iget-object v0, p0, Lco/uk/getmondo/common/pager/GenericPagerAdapter$ImageAndTextViewHolder;->footerTextView:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 108
    iget-object v0, p0, Lco/uk/getmondo/common/pager/GenericPagerAdapter$ImageAndTextViewHolder;->spacerView:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method
