.class public Lco/uk/getmondo/common/pager/GenericPagerAdapter;
.super Landroid/support/v4/view/p;
.source "GenericPagerAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lco/uk/getmondo/common/pager/GenericPagerAdapter$LottieAnimationAndTextViewHolder;,
        Lco/uk/getmondo/common/pager/GenericPagerAdapter$TitleAndImageViewHolder;,
        Lco/uk/getmondo/common/pager/GenericPagerAdapter$ImageAndTextViewHolder;
    }
.end annotation


# instance fields
.field final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lco/uk/getmondo/common/pager/f;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lco/uk/getmondo/common/pager/f;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 22
    invoke-direct {p0}, Landroid/support/v4/view/p;-><init>()V

    .line 23
    iput-object p1, p0, Lco/uk/getmondo/common/pager/GenericPagerAdapter;->a:Ljava/util/List;

    .line 24
    return-void
.end method

.method public varargs constructor <init>([Lco/uk/getmondo/common/pager/f;)V
    .locals 1

    .prologue
    .line 26
    invoke-direct {p0}, Landroid/support/v4/view/p;-><init>()V

    .line 27
    invoke-static {p1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/common/pager/GenericPagerAdapter;->a:Ljava/util/List;

    .line 28
    return-void
.end method


# virtual methods
.method public a(Landroid/view/ViewGroup;I)Ljava/lang/Object;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 33
    iget-object v0, p0, Lco/uk/getmondo/common/pager/GenericPagerAdapter;->a:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/common/pager/f;

    .line 34
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    .line 37
    instance-of v2, v0, Lco/uk/getmondo/common/pager/f$a;

    if-eqz v2, :cond_0

    .line 38
    check-cast v0, Lco/uk/getmondo/common/pager/f$a;

    invoke-interface {v0}, Lco/uk/getmondo/common/pager/f$a;->b()I

    move-result v0

    invoke-virtual {v1, v0, p1, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 54
    :goto_0
    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 55
    return-object v0

    .line 39
    :cond_0
    instance-of v2, v0, Lco/uk/getmondo/common/pager/f$c;

    if-eqz v2, :cond_1

    .line 40
    new-instance v2, Lco/uk/getmondo/common/pager/GenericPagerAdapter$ImageAndTextViewHolder;

    const v3, 0x7f050126

    invoke-virtual {v1, v3, p1, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    invoke-direct {v2, v1}, Lco/uk/getmondo/common/pager/GenericPagerAdapter$ImageAndTextViewHolder;-><init>(Landroid/view/View;)V

    check-cast v0, Lco/uk/getmondo/common/pager/f$c;

    .line 41
    invoke-virtual {v2, v0}, Lco/uk/getmondo/common/pager/GenericPagerAdapter$ImageAndTextViewHolder;->a(Lco/uk/getmondo/common/pager/f$c;)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    .line 42
    :cond_1
    instance-of v2, v0, Lco/uk/getmondo/common/pager/f$e;

    if-eqz v2, :cond_2

    .line 43
    new-instance v2, Lco/uk/getmondo/common/pager/GenericPagerAdapter$TitleAndImageViewHolder;

    const v3, 0x7f05012a

    invoke-virtual {v1, v3, p1, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    invoke-direct {v2, v1}, Lco/uk/getmondo/common/pager/GenericPagerAdapter$TitleAndImageViewHolder;-><init>(Landroid/view/View;)V

    check-cast v0, Lco/uk/getmondo/common/pager/f$e;

    .line 44
    invoke-virtual {v2, v0}, Lco/uk/getmondo/common/pager/GenericPagerAdapter$TitleAndImageViewHolder;->a(Lco/uk/getmondo/common/pager/f$e;)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    .line 45
    :cond_2
    instance-of v2, v0, Lco/uk/getmondo/common/pager/f$b;

    if-eqz v2, :cond_3

    .line 46
    check-cast v0, Lco/uk/getmondo/common/pager/f$b;

    invoke-interface {v0}, Lco/uk/getmondo/common/pager/f$b;->c()Landroid/view/View;

    move-result-object v0

    goto :goto_0

    .line 47
    :cond_3
    instance-of v2, v0, Lco/uk/getmondo/common/pager/f$d;

    if-eqz v2, :cond_4

    .line 48
    new-instance v2, Lco/uk/getmondo/common/pager/GenericPagerAdapter$LottieAnimationAndTextViewHolder;

    const v3, 0x7f050127

    invoke-virtual {v1, v3, p1, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    invoke-direct {v2, v1}, Lco/uk/getmondo/common/pager/GenericPagerAdapter$LottieAnimationAndTextViewHolder;-><init>(Landroid/view/View;)V

    check-cast v0, Lco/uk/getmondo/common/pager/f$d;

    .line 49
    invoke-virtual {v2, v0}, Lco/uk/getmondo/common/pager/GenericPagerAdapter$LottieAnimationAndTextViewHolder;->a(Lco/uk/getmondo/common/pager/f$d;)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    .line 51
    :cond_4
    new-instance v1, Ljava/lang/RuntimeException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unsupported page type: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public a(Landroid/view/ViewGroup;ILjava/lang/Object;)V
    .locals 2

    .prologue
    .line 68
    iget-object v0, p0, Lco/uk/getmondo/common/pager/GenericPagerAdapter;->a:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/common/pager/f;

    .line 69
    instance-of v1, v0, Lco/uk/getmondo/common/pager/f$b;

    if-eqz v1, :cond_0

    .line 70
    check-cast v0, Lco/uk/getmondo/common/pager/f$b;

    invoke-interface {v0}, Lco/uk/getmondo/common/pager/f$b;->b()V

    .line 72
    :cond_0
    check-cast p3, Landroid/view/View;

    invoke-virtual {p1, p3}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 73
    return-void
.end method

.method public a(Landroid/view/View;Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 82
    if-ne p1, p2, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()I
    .locals 1

    .prologue
    .line 77
    iget-object v0, p0, Lco/uk/getmondo/common/pager/GenericPagerAdapter;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public b(Landroid/view/ViewGroup;ILjava/lang/Object;)V
    .locals 2

    .prologue
    .line 59
    invoke-super {p0, p1, p2, p3}, Landroid/support/v4/view/p;->b(Landroid/view/ViewGroup;ILjava/lang/Object;)V

    .line 60
    check-cast p3, Landroid/view/View;

    const v0, 0x7f110462

    invoke-virtual {p3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/airbnb/lottie/LottieAnimationView;

    .line 61
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/airbnb/lottie/LottieAnimationView;->b()Z

    move-result v1

    if-nez v1, :cond_0

    .line 62
    invoke-virtual {v0}, Lcom/airbnb/lottie/LottieAnimationView;->c()V

    .line 64
    :cond_0
    return-void
.end method
