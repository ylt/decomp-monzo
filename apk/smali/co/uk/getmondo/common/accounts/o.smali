.class public Lco/uk/getmondo/common/accounts/o;
.super Ljava/lang/Object;
.source "UserAccessTokenManager.java"


# instance fields
.field private final a:Lco/uk/getmondo/common/accounts/k;


# direct methods
.method public constructor <init>(Lco/uk/getmondo/common/accounts/k;)V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    iput-object p1, p0, Lco/uk/getmondo/common/accounts/o;->a:Lco/uk/getmondo/common/accounts/k;

    .line 18
    return-void
.end method


# virtual methods
.method public declared-synchronized a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 26
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lco/uk/getmondo/common/accounts/o;->a:Lco/uk/getmondo/common/accounts/k;

    invoke-virtual {v0}, Lco/uk/getmondo/common/accounts/k;->b()Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public a(Lco/uk/getmondo/d/ai;)V
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lco/uk/getmondo/common/accounts/o;->a:Lco/uk/getmondo/common/accounts/k;

    invoke-virtual {v0, p1}, Lco/uk/getmondo/common/accounts/k;->a(Lco/uk/getmondo/d/ai;)V

    .line 40
    return-void
.end method

.method public declared-synchronized b()Lco/uk/getmondo/d/ai;
    .locals 1

    .prologue
    .line 35
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lco/uk/getmondo/common/accounts/o;->a:Lco/uk/getmondo/common/accounts/k;

    invoke-virtual {v0}, Lco/uk/getmondo/common/accounts/k;->a()Lco/uk/getmondo/d/ai;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
