.class public Lco/uk/getmondo/common/accounts/d;
.super Ljava/lang/Object;
.source "AccountService.java"


# instance fields
.field private final a:Lco/uk/getmondo/common/accounts/k;

.field private final b:Lio/reactivex/u;

.field private final c:Lco/uk/getmondo/common/q;

.field private final d:Lco/uk/getmondo/waitlist/i;

.field private final e:Lco/uk/getmondo/payments/send/payment_category/b$a;

.field private final f:Lco/uk/getmondo/settings/k;

.field private final g:Lco/uk/getmondo/signup/identity_verification/a/h;

.field private final h:Lco/uk/getmondo/signup/status/g;

.field private final i:Lco/uk/getmondo/migration/d;


# direct methods
.method public constructor <init>(Lco/uk/getmondo/common/accounts/k;Lio/reactivex/u;Lco/uk/getmondo/waitlist/i;Lco/uk/getmondo/common/q;Lco/uk/getmondo/payments/send/payment_category/b$a;Lco/uk/getmondo/settings/k;Lco/uk/getmondo/signup/identity_verification/a/h;Lco/uk/getmondo/signup/status/g;Lco/uk/getmondo/migration/d;)V
    .locals 0

    .prologue
    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    iput-object p1, p0, Lco/uk/getmondo/common/accounts/d;->a:Lco/uk/getmondo/common/accounts/k;

    .line 44
    iput-object p2, p0, Lco/uk/getmondo/common/accounts/d;->b:Lio/reactivex/u;

    .line 45
    iput-object p4, p0, Lco/uk/getmondo/common/accounts/d;->c:Lco/uk/getmondo/common/q;

    .line 46
    iput-object p3, p0, Lco/uk/getmondo/common/accounts/d;->d:Lco/uk/getmondo/waitlist/i;

    .line 47
    iput-object p5, p0, Lco/uk/getmondo/common/accounts/d;->e:Lco/uk/getmondo/payments/send/payment_category/b$a;

    .line 48
    iput-object p6, p0, Lco/uk/getmondo/common/accounts/d;->f:Lco/uk/getmondo/settings/k;

    .line 49
    iput-object p7, p0, Lco/uk/getmondo/common/accounts/d;->g:Lco/uk/getmondo/signup/identity_verification/a/h;

    .line 50
    iput-object p8, p0, Lco/uk/getmondo/common/accounts/d;->h:Lco/uk/getmondo/signup/status/g;

    .line 51
    iput-object p9, p0, Lco/uk/getmondo/common/accounts/d;->i:Lco/uk/getmondo/migration/d;

    .line 52
    return-void
.end method

.method static synthetic a(Lio/realm/av;)V
    .locals 0

    .prologue
    .line 140
    invoke-virtual {p0}, Lio/realm/av;->l()V

    return-void
.end method

.method static synthetic f()V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 139
    invoke-static {}, Lio/realm/av;->n()Lio/realm/av;

    move-result-object v2

    const/4 v1, 0x0

    .line 140
    :try_start_0
    invoke-static {}, Lco/uk/getmondo/common/accounts/i;->a()Lio/realm/av$a;

    move-result-object v0

    invoke-virtual {v2, v0}, Lio/realm/av;->a(Lio/realm/av$a;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 141
    if-eqz v2, :cond_0

    if-eqz v1, :cond_1

    :try_start_1
    invoke-virtual {v2}, Lio/realm/av;->close()V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_1

    .line 142
    :cond_0
    :goto_0
    return-void

    .line 141
    :cond_1
    invoke-virtual {v2}, Lio/realm/av;->close()V

    goto :goto_0

    .line 139
    :catch_0
    move-exception v0

    :try_start_2
    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 141
    :catchall_0
    move-exception v1

    move-object v3, v1

    move-object v1, v0

    move-object v0, v3

    :goto_1
    if-eqz v2, :cond_2

    if-eqz v1, :cond_3

    :try_start_3
    invoke-virtual {v2}, Lio/realm/av;->close()V
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_2

    :cond_2
    :goto_2
    throw v0

    :cond_3
    invoke-virtual {v2}, Lio/realm/av;->close()V

    goto :goto_2

    :catch_1
    move-exception v0

    goto :goto_0

    :catch_2
    move-exception v1

    goto :goto_2

    :catchall_1
    move-exception v0

    goto :goto_1
.end method

.method private g()V
    .locals 2

    .prologue
    .line 138
    invoke-static {}, Lco/uk/getmondo/common/accounts/h;->b()Lio/reactivex/c/a;

    move-result-object v0

    invoke-static {v0}, Lio/reactivex/b;->a(Lio/reactivex/c/a;)Lio/reactivex/b;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/common/accounts/d;->b:Lio/reactivex/u;

    .line 142
    invoke-virtual {v0, v1}, Lio/reactivex/b;->b(Lio/reactivex/u;)Lio/reactivex/b;

    move-result-object v0

    invoke-virtual {v0}, Lio/reactivex/b;->c()Lio/reactivex/b/b;

    .line 143
    return-void
.end method


# virtual methods
.method public declared-synchronized a(Lco/uk/getmondo/common/accounts/l;)V
    .locals 2

    .prologue
    .line 112
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lco/uk/getmondo/common/accounts/d;->a:Lco/uk/getmondo/common/accounts/k;

    invoke-virtual {v0}, Lco/uk/getmondo/common/accounts/k;->c()Lco/uk/getmondo/d/ak;

    move-result-object v0

    .line 113
    iget-object v1, p0, Lco/uk/getmondo/common/accounts/d;->a:Lco/uk/getmondo/common/accounts/k;

    invoke-virtual {v0, p1}, Lco/uk/getmondo/d/ak;->a(Lco/uk/getmondo/common/accounts/l;)Lco/uk/getmondo/d/ak;

    move-result-object v0

    invoke-virtual {v1, v0}, Lco/uk/getmondo/common/accounts/k;->a(Lco/uk/getmondo/d/ak;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 114
    monitor-exit p0

    return-void

    .line 112
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized a(Lco/uk/getmondo/d/a;)V
    .locals 2

    .prologue
    .line 88
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lco/uk/getmondo/common/accounts/d;->a:Lco/uk/getmondo/common/accounts/k;

    invoke-virtual {v0}, Lco/uk/getmondo/common/accounts/k;->c()Lco/uk/getmondo/d/ak;

    move-result-object v0

    .line 89
    iget-object v1, p0, Lco/uk/getmondo/common/accounts/d;->a:Lco/uk/getmondo/common/accounts/k;

    invoke-virtual {v0, p1}, Lco/uk/getmondo/d/ak;->a(Lco/uk/getmondo/d/a;)Lco/uk/getmondo/d/ak;

    move-result-object v0

    invoke-virtual {v1, v0}, Lco/uk/getmondo/common/accounts/k;->a(Lco/uk/getmondo/d/ak;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 90
    monitor-exit p0

    return-void

    .line 88
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized a(Lco/uk/getmondo/d/ac;)V
    .locals 2

    .prologue
    .line 96
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lco/uk/getmondo/common/accounts/d;->a:Lco/uk/getmondo/common/accounts/k;

    invoke-virtual {v0}, Lco/uk/getmondo/common/accounts/k;->c()Lco/uk/getmondo/d/ak;

    move-result-object v0

    .line 97
    iget-object v1, p0, Lco/uk/getmondo/common/accounts/d;->a:Lco/uk/getmondo/common/accounts/k;

    invoke-virtual {v0, p1}, Lco/uk/getmondo/d/ak;->a(Lco/uk/getmondo/d/ac;)Lco/uk/getmondo/d/ak;

    move-result-object v0

    invoke-virtual {v1, v0}, Lco/uk/getmondo/common/accounts/k;->a(Lco/uk/getmondo/d/ak;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 98
    monitor-exit p0

    return-void

    .line 96
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized a(Lco/uk/getmondo/d/ac;Lco/uk/getmondo/d/an;)V
    .locals 2

    .prologue
    .line 121
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lco/uk/getmondo/common/accounts/d;->a:Lco/uk/getmondo/common/accounts/k;

    invoke-virtual {v0}, Lco/uk/getmondo/common/accounts/k;->c()Lco/uk/getmondo/d/ak;

    move-result-object v0

    .line 122
    iget-object v1, p0, Lco/uk/getmondo/common/accounts/d;->a:Lco/uk/getmondo/common/accounts/k;

    invoke-virtual {v0, p1, p2}, Lco/uk/getmondo/d/ak;->a(Lco/uk/getmondo/d/ac;Lco/uk/getmondo/d/an;)Lco/uk/getmondo/d/ak;

    move-result-object v0

    invoke-virtual {v1, v0}, Lco/uk/getmondo/common/accounts/k;->a(Lco/uk/getmondo/d/ak;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 123
    monitor-exit p0

    return-void

    .line 121
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized a(Lco/uk/getmondo/d/ai;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 75
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lco/uk/getmondo/common/accounts/d;->a:Lco/uk/getmondo/common/accounts/k;

    invoke-virtual {v0, p1, p2}, Lco/uk/getmondo/common/accounts/k;->a(Lco/uk/getmondo/d/ai;Ljava/lang/String;)V

    .line 76
    iget-object v0, p0, Lco/uk/getmondo/common/accounts/d;->a:Lco/uk/getmondo/common/accounts/k;

    new-instance v1, Lco/uk/getmondo/d/ak;

    invoke-direct {v1, p3}, Lco/uk/getmondo/d/ak;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lco/uk/getmondo/common/accounts/k;->a(Lco/uk/getmondo/d/ak;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 77
    monitor-exit p0

    return-void

    .line 75
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized a(Lco/uk/getmondo/d/an;)V
    .locals 2

    .prologue
    .line 104
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lco/uk/getmondo/common/accounts/d;->a:Lco/uk/getmondo/common/accounts/k;

    invoke-virtual {v0}, Lco/uk/getmondo/common/accounts/k;->c()Lco/uk/getmondo/d/ak;

    move-result-object v0

    .line 105
    iget-object v1, p0, Lco/uk/getmondo/common/accounts/d;->a:Lco/uk/getmondo/common/accounts/k;

    invoke-virtual {v0, p1}, Lco/uk/getmondo/d/ak;->a(Lco/uk/getmondo/d/an;)Lco/uk/getmondo/d/ak;

    move-result-object v0

    invoke-virtual {v1, v0}, Lco/uk/getmondo/common/accounts/k;->a(Lco/uk/getmondo/d/ak;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 106
    monitor-exit p0

    return-void

    .line 104
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized a()Z
    .locals 1

    .prologue
    .line 55
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lco/uk/getmondo/common/accounts/d;->a:Lco/uk/getmondo/common/accounts/k;

    invoke-virtual {v0}, Lco/uk/getmondo/common/accounts/k;->e()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public b()Lco/uk/getmondo/d/ak;
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lco/uk/getmondo/common/accounts/d;->a:Lco/uk/getmondo/common/accounts/k;

    invoke-virtual {v0}, Lco/uk/getmondo/common/accounts/k;->c()Lco/uk/getmondo/d/ak;

    move-result-object v0

    return-object v0
.end method

.method public c()Lio/reactivex/v;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/v",
            "<",
            "Lco/uk/getmondo/d/ak;",
            ">;"
        }
    .end annotation

    .prologue
    .line 64
    iget-object v0, p0, Lco/uk/getmondo/common/accounts/d;->a:Lco/uk/getmondo/common/accounts/k;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    invoke-static {v0}, Lco/uk/getmondo/common/accounts/e;->a(Lco/uk/getmondo/common/accounts/k;)Ljava/util/concurrent/Callable;

    move-result-object v0

    invoke-static {v0}, Lio/reactivex/v;->c(Ljava/util/concurrent/Callable;)Lio/reactivex/v;

    move-result-object v0

    return-object v0
.end method

.method public d()Lio/reactivex/v;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/v",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 69
    invoke-virtual {p0}, Lco/uk/getmondo/common/accounts/d;->c()Lio/reactivex/v;

    move-result-object v0

    invoke-static {}, Lco/uk/getmondo/common/accounts/f;->a()Lio/reactivex/c/h;

    move-result-object v1

    .line 70
    invoke-virtual {v0, v1}, Lio/reactivex/v;->d(Lio/reactivex/c/h;)Lio/reactivex/v;

    move-result-object v0

    invoke-static {}, Lco/uk/getmondo/common/accounts/g;->a()Lio/reactivex/c/h;

    move-result-object v1

    .line 71
    invoke-virtual {v0, v1}, Lio/reactivex/v;->d(Lio/reactivex/c/h;)Lio/reactivex/v;

    move-result-object v0

    .line 69
    return-object v0
.end method

.method public declared-synchronized e()V
    .locals 1

    .prologue
    .line 126
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lco/uk/getmondo/common/accounts/d;->a:Lco/uk/getmondo/common/accounts/k;

    invoke-virtual {v0}, Lco/uk/getmondo/common/accounts/k;->d()V

    .line 127
    iget-object v0, p0, Lco/uk/getmondo/common/accounts/d;->d:Lco/uk/getmondo/waitlist/i;

    invoke-virtual {v0}, Lco/uk/getmondo/waitlist/i;->c()V

    .line 128
    iget-object v0, p0, Lco/uk/getmondo/common/accounts/d;->f:Lco/uk/getmondo/settings/k;

    invoke-virtual {v0}, Lco/uk/getmondo/settings/k;->e()V

    .line 129
    invoke-direct {p0}, Lco/uk/getmondo/common/accounts/d;->g()V

    .line 130
    iget-object v0, p0, Lco/uk/getmondo/common/accounts/d;->c:Lco/uk/getmondo/common/q;

    invoke-virtual {v0}, Lco/uk/getmondo/common/q;->b()V

    .line 131
    iget-object v0, p0, Lco/uk/getmondo/common/accounts/d;->e:Lco/uk/getmondo/payments/send/payment_category/b$a;

    invoke-virtual {v0}, Lco/uk/getmondo/payments/send/payment_category/b$a;->b()V

    .line 132
    iget-object v0, p0, Lco/uk/getmondo/common/accounts/d;->g:Lco/uk/getmondo/signup/identity_verification/a/h;

    invoke-virtual {v0}, Lco/uk/getmondo/signup/identity_verification/a/h;->i()V

    .line 133
    iget-object v0, p0, Lco/uk/getmondo/common/accounts/d;->h:Lco/uk/getmondo/signup/status/g;

    invoke-virtual {v0}, Lco/uk/getmondo/signup/status/g;->c()V

    .line 134
    iget-object v0, p0, Lco/uk/getmondo/common/accounts/d;->i:Lco/uk/getmondo/migration/d;

    invoke-virtual {v0}, Lco/uk/getmondo/migration/d;->c()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 135
    monitor-exit p0

    return-void

    .line 126
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
