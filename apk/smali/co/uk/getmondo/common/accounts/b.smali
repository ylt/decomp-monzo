.class public final Lco/uk/getmondo/common/accounts/b;
.super Ljava/lang/Object;
.source "AccountManager.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u00000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u000e\n\u0002\u0008\u0003\n\u0002\u0010\u000b\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u000f\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u000c\u0010\t\u001a\u0008\u0012\u0004\u0012\u00020\n0\u0012R\u0013\u0010\u0005\u001a\u0004\u0018\u00010\u00068F\u00a2\u0006\u0006\u001a\u0004\u0008\u0007\u0010\u0008R\u0013\u0010\t\u001a\u0004\u0018\u00010\n8F\u00a2\u0006\u0006\u001a\u0004\u0008\u000b\u0010\u000cR\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0011\u0010\r\u001a\u00020\u000e8F\u00a2\u0006\u0006\u001a\u0004\u0008\r\u0010\u000fR\u0013\u0010\u0010\u001a\u0004\u0018\u00010\n8F\u00a2\u0006\u0006\u001a\u0004\u0008\u0011\u0010\u000c\u00a8\u0006\u0013"
    }
    d2 = {
        "Lco/uk/getmondo/common/accounts/AccountManager;",
        "",
        "androidAccountManager",
        "Lco/uk/getmondo/common/accounts/AndroidAccountManager;",
        "(Lco/uk/getmondo/common/accounts/AndroidAccountManager;)V",
        "account",
        "Lco/uk/getmondo/model/Account;",
        "getAccount",
        "()Lco/uk/getmondo/model/Account;",
        "accountId",
        "",
        "getAccountId",
        "()Ljava/lang/String;",
        "isRetailAccount",
        "",
        "()Z",
        "secondaryAccountId",
        "getSecondaryAccountId",
        "Lio/reactivex/Observable;",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field private final a:Lco/uk/getmondo/common/accounts/k;


# direct methods
.method public constructor <init>(Lco/uk/getmondo/common/accounts/k;)V
    .locals 1

    .prologue
    const-string v0, "androidAccountManager"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lco/uk/getmondo/common/accounts/b;->a:Lco/uk/getmondo/common/accounts/k;

    return-void
.end method


# virtual methods
.method public final a()Lco/uk/getmondo/d/a;
    .locals 1

    .prologue
    .line 13
    iget-object v0, p0, Lco/uk/getmondo/common/accounts/b;->a:Lco/uk/getmondo/common/accounts/k;

    invoke-virtual {v0}, Lco/uk/getmondo/common/accounts/k;->c()Lco/uk/getmondo/d/ak;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lco/uk/getmondo/d/ak;->c()Lco/uk/getmondo/d/a;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 16
    invoke-virtual {p0}, Lco/uk/getmondo/common/accounts/b;->a()Lco/uk/getmondo/d/a;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lco/uk/getmondo/d/a;->e()Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 19
    invoke-virtual {p0}, Lco/uk/getmondo/common/accounts/b;->a()Lco/uk/getmondo/d/a;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lco/uk/getmondo/d/a;->a()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 22
    iget-object v0, p0, Lco/uk/getmondo/common/accounts/b;->a:Lco/uk/getmondo/common/accounts/k;

    invoke-virtual {v0}, Lco/uk/getmondo/common/accounts/k;->c()Lco/uk/getmondo/d/ak;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lco/uk/getmondo/d/ak;->f()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final e()Lio/reactivex/n;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 24
    new-instance v0, Lco/uk/getmondo/common/accounts/b$a;

    invoke-direct {v0, p0}, Lco/uk/getmondo/common/accounts/b$a;-><init>(Lco/uk/getmondo/common/accounts/b;)V

    check-cast v0, Ljava/util/concurrent/Callable;

    invoke-static {v0}, Lio/reactivex/n;->fromCallable(Ljava/util/concurrent/Callable;)Lio/reactivex/n;

    move-result-object v0

    const-string v1, "Observable.fromCallable { accountId }"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method
