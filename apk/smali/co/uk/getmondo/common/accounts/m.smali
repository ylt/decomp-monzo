.class public final Lco/uk/getmondo/common/accounts/m;
.super Ljava/lang/Object;
.source "ProfileAndAccountInfoZipper.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000,\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\u0018\u00002\u00020\u0001B\u0007\u0008\u0007\u00a2\u0006\u0002\u0010\u0002J*\u0010\u0003\u001a\u00020\u00042\u000c\u0010\u0005\u001a\u0008\u0012\u0004\u0012\u00020\u00070\u00062\u000c\u0010\u0008\u001a\u0008\u0012\u0004\u0012\u00020\n0\t2\u0006\u0010\u000b\u001a\u00020\u000c\u00a8\u0006\r"
    }
    d2 = {
        "Lco/uk/getmondo/common/accounts/ProfileAndAccountInfoZipper;",
        "",
        "()V",
        "zip",
        "Lco/uk/getmondo/common/accounts/ProfileAndAccountInfo;",
        "accounts",
        "",
        "Lco/uk/getmondo/model/Account;",
        "profileOptional",
        "Lcom/memoizrlabs/poweroptional/Optional;",
        "Lco/uk/getmondo/model/Profile;",
        "isPrepaidAccountMigrated",
        "",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/util/List;Lcom/c/b/b;Z)Lco/uk/getmondo/common/accounts/l;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<+",
            "Lco/uk/getmondo/d/a;",
            ">;",
            "Lcom/c/b/b",
            "<",
            "Lco/uk/getmondo/d/ac;",
            ">;Z)",
            "Lco/uk/getmondo/common/accounts/l;"
        }
    .end annotation

    .prologue
    const/4 v5, 0x1

    const/4 v1, 0x0

    const-string v0, "accounts"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "profileOptional"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v0, v1

    .line 20
    check-cast v0, Lco/uk/getmondo/d/a;

    move-object v2, v1

    .line 21
    check-cast v2, Ljava/lang/String;

    .line 22
    sget-object v3, Lco/uk/getmondo/a;->c:Ljava/lang/Boolean;

    const-string v4, "BuildConfig.BANK"

    invoke-static {v3, v4}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 24
    check-cast p1, Ljava/lang/Iterable;

    .line 39
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    move-object v0, v3

    check-cast v0, Lco/uk/getmondo/d/a;

    .line 24
    invoke-interface {v0}, Lco/uk/getmondo/d/a;->d()Lco/uk/getmondo/d/a$b;

    move-result-object v0

    sget-object v5, Lco/uk/getmondo/d/a$b;->RETAIL:Lco/uk/getmondo/d/a$b;

    invoke-static {v0, v5}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    move-object v0, v3

    .line 40
    :goto_0
    check-cast v0, Lco/uk/getmondo/d/a;

    move-object v3, v0

    .line 33
    :goto_1
    invoke-virtual {p2}, Lcom/c/b/b;->d()Z

    move-result v0

    if-eqz v0, :cond_c

    .line 36
    :goto_2
    new-instance v0, Lco/uk/getmondo/common/accounts/l;

    invoke-direct {v0, v1, v3, v2}, Lco/uk/getmondo/common/accounts/l;-><init>(Lco/uk/getmondo/d/ac;Lco/uk/getmondo/d/a;Ljava/lang/String;)V

    return-object v0

    :cond_1
    move-object v0, v1

    .line 40
    goto :goto_0

    .line 25
    :cond_2
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v3

    if-ne v3, v5, :cond_3

    .line 26
    const/4 v0, 0x0

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/d/a;

    move-object v3, v0

    goto :goto_1

    .line 27
    :cond_3
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v3

    if-le v3, v5, :cond_d

    .line 28
    if-eqz p3, :cond_9

    move-object v0, p1

    .line 29
    check-cast v0, Ljava/lang/Iterable;

    .line 41
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_4
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    move-object v0, v2

    check-cast v0, Lco/uk/getmondo/d/a;

    .line 29
    invoke-interface {v0}, Lco/uk/getmondo/d/a;->d()Lco/uk/getmondo/d/a$b;

    move-result-object v0

    sget-object v4, Lco/uk/getmondo/d/a$b;->RETAIL:Lco/uk/getmondo/d/a$b;

    invoke-static {v0, v4}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    move-object v0, v2

    .line 42
    :goto_3
    check-cast v0, Lco/uk/getmondo/d/a;

    .line 30
    check-cast p1, Ljava/lang/Iterable;

    .line 43
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_5
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_7

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    move-object v2, v3

    check-cast v2, Lco/uk/getmondo/d/a;

    .line 30
    invoke-interface {v2}, Lco/uk/getmondo/d/a;->d()Lco/uk/getmondo/d/a$b;

    move-result-object v2

    sget-object v5, Lco/uk/getmondo/d/a$b;->PREPAID:Lco/uk/getmondo/d/a$b;

    invoke-static {v2, v5}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    move-object v2, v3

    .line 44
    :goto_4
    check-cast v2, Lco/uk/getmondo/d/a;

    if-eqz v2, :cond_8

    invoke-interface {v2}, Lco/uk/getmondo/d/a;->a()Ljava/lang/String;

    move-result-object v2

    :goto_5
    move-object v3, v0

    goto :goto_1

    :cond_6
    move-object v0, v1

    .line 42
    goto :goto_3

    :cond_7
    move-object v2, v1

    .line 44
    goto :goto_4

    :cond_8
    move-object v2, v1

    goto :goto_5

    .line 32
    :cond_9
    check-cast p1, Ljava/lang/Iterable;

    .line 45
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_a
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_b

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    move-object v0, v3

    check-cast v0, Lco/uk/getmondo/d/a;

    .line 32
    invoke-interface {v0}, Lco/uk/getmondo/d/a;->d()Lco/uk/getmondo/d/a$b;

    move-result-object v0

    sget-object v5, Lco/uk/getmondo/d/a$b;->PREPAID:Lco/uk/getmondo/d/a$b;

    invoke-static {v0, v5}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_a

    move-object v0, v3

    .line 46
    :goto_6
    check-cast v0, Lco/uk/getmondo/d/a;

    move-object v3, v0

    goto/16 :goto_1

    :cond_b
    move-object v0, v1

    goto :goto_6

    .line 33
    :cond_c
    invoke-virtual {p2}, Lcom/c/b/b;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/d/ac;

    move-object v1, v0

    goto/16 :goto_2

    :cond_d
    move-object v3, v0

    goto/16 :goto_1
.end method
