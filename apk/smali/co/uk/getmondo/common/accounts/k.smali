.class public Lco/uk/getmondo/common/accounts/k;
.super Ljava/lang/Object;
.source "AndroidAccountManager.java"


# instance fields
.field private final a:Landroid/accounts/AccountManager;

.field private final b:Lcom/google/gson/f;

.field private final c:Landroid/os/Handler;

.field private d:Lco/uk/getmondo/d/ak;


# direct methods
.method public constructor <init>(Landroid/accounts/AccountManager;)V
    .locals 3

    .prologue
    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lco/uk/getmondo/common/accounts/k;->c:Landroid/os/Handler;

    .line 42
    iput-object p1, p0, Lco/uk/getmondo/common/accounts/k;->a:Landroid/accounts/AccountManager;

    .line 44
    const-class v0, Lco/uk/getmondo/d/a;

    const-string v1, "account"

    invoke-static {v0, v1}, Lco/uk/getmondo/common/g/b;->a(Ljava/lang/Class;Ljava/lang/String;)Lco/uk/getmondo/common/g/b;

    move-result-object v0

    const-class v1, Lco/uk/getmondo/d/ab;

    const-string v2, "prepaid"

    .line 45
    invoke-virtual {v0, v1, v2}, Lco/uk/getmondo/common/g/b;->b(Ljava/lang/Class;Ljava/lang/String;)Lco/uk/getmondo/common/g/b;

    move-result-object v0

    const-class v1, Lco/uk/getmondo/d/ad;

    const-string v2, "retail"

    .line 46
    invoke-virtual {v0, v1, v2}, Lco/uk/getmondo/common/g/b;->b(Ljava/lang/Class;Ljava/lang/String;)Lco/uk/getmondo/common/g/b;

    move-result-object v1

    sget-object v0, Lco/uk/getmondo/a;->c:Ljava/lang/Boolean;

    .line 47
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    const-class v0, Lco/uk/getmondo/d/ad;

    :goto_0
    invoke-virtual {v1, v0}, Lco/uk/getmondo/common/g/b;->a(Ljava/lang/Class;)Lco/uk/getmondo/common/g/b;

    move-result-object v0

    .line 50
    new-instance v1, Lcom/google/gson/g;

    invoke-direct {v1}, Lcom/google/gson/g;-><init>()V

    .line 51
    invoke-virtual {v1, v0}, Lcom/google/gson/g;->a(Lcom/google/gson/t;)Lcom/google/gson/g;

    move-result-object v0

    .line 52
    invoke-virtual {v0}, Lcom/google/gson/g;->a()Lcom/google/gson/f;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/common/accounts/k;->b:Lcom/google/gson/f;

    .line 53
    return-void

    .line 47
    :cond_0
    const-class v0, Lco/uk/getmondo/d/ab;

    goto :goto_0
.end method

.method private f()Landroid/accounts/Account;
    .locals 2

    .prologue
    .line 146
    iget-object v0, p0, Lco/uk/getmondo/common/accounts/k;->a:Landroid/accounts/AccountManager;

    const-string v1, "co.uk.getmondo"

    invoke-virtual {v0, v1}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v0

    .line 147
    array-length v1, v0

    if-gtz v1, :cond_0

    .line 148
    const/4 v0, 0x0

    .line 150
    :goto_0
    return-object v0

    :cond_0
    const/4 v1, 0x0

    aget-object v0, v0, v1

    goto :goto_0
.end method


# virtual methods
.method public a()Lco/uk/getmondo/d/ai;
    .locals 4

    .prologue
    .line 57
    invoke-direct {p0}, Lco/uk/getmondo/common/accounts/k;->f()Landroid/accounts/Account;

    move-result-object v0

    .line 58
    if-eqz v0, :cond_0

    .line 60
    :try_start_0
    iget-object v1, p0, Lco/uk/getmondo/common/accounts/k;->a:Landroid/accounts/AccountManager;

    const-string v2, "co.uk.getmondo_authTokenType"

    const/4 v3, 0x1

    invoke-virtual {v1, v0, v2, v3}, Landroid/accounts/AccountManager;->blockingGetAuthToken(Landroid/accounts/Account;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v2

    .line 61
    iget-object v1, p0, Lco/uk/getmondo/common/accounts/k;->a:Landroid/accounts/AccountManager;

    const-string v3, "co.uk.getmondo_refreshData"

    invoke-virtual {v1, v0, v3}, Landroid/accounts/AccountManager;->getUserData(Landroid/accounts/Account;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 62
    iget-object v1, p0, Lco/uk/getmondo/common/accounts/k;->b:Lcom/google/gson/f;

    const-class v3, Lco/uk/getmondo/d/ai$a;

    invoke-virtual {v1, v0, v3}, Lcom/google/gson/f;->a(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/d/ai$a;

    .line 63
    new-instance v1, Lco/uk/getmondo/d/ai;

    invoke-direct {v1, v2, v0}, Lco/uk/getmondo/d/ai;-><init>(Ljava/lang/String;Lco/uk/getmondo/d/ai$a;)V
    :try_end_0
    .catch Landroid/accounts/OperationCanceledException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Landroid/accounts/AuthenticatorException; {:try_start_0 .. :try_end_0} :catch_0

    move-object v0, v1

    .line 68
    :goto_0
    return-object v0

    .line 64
    :catch_0
    move-exception v0

    .line 65
    :goto_1
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "Failed to get auth token"

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    invoke-static {v1}, Ld/a/a;->a(Ljava/lang/Throwable;)V

    .line 68
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 64
    :catch_1
    move-exception v0

    goto :goto_1

    :catch_2
    move-exception v0

    goto :goto_1
.end method

.method public a(Landroid/accounts/OnAccountsUpdateListener;)V
    .locals 3

    .prologue
    .line 97
    iget-object v0, p0, Lco/uk/getmondo/common/accounts/k;->a:Landroid/accounts/AccountManager;

    iget-object v1, p0, Lco/uk/getmondo/common/accounts/k;->c:Landroid/os/Handler;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v1, v2}, Landroid/accounts/AccountManager;->addOnAccountsUpdatedListener(Landroid/accounts/OnAccountsUpdateListener;Landroid/os/Handler;Z)V

    .line 98
    return-void
.end method

.method public a(Lco/uk/getmondo/d/ai;)V
    .locals 5

    .prologue
    .line 111
    invoke-direct {p0}, Lco/uk/getmondo/common/accounts/k;->f()Landroid/accounts/Account;

    move-result-object v0

    .line 112
    iget-object v1, p0, Lco/uk/getmondo/common/accounts/k;->a:Landroid/accounts/AccountManager;

    const-string v2, "co.uk.getmondo_authTokenType"

    invoke-virtual {p1}, Lco/uk/getmondo/d/ai;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v0, v2, v3}, Landroid/accounts/AccountManager;->setAuthToken(Landroid/accounts/Account;Ljava/lang/String;Ljava/lang/String;)V

    .line 113
    iget-object v1, p0, Lco/uk/getmondo/common/accounts/k;->a:Landroid/accounts/AccountManager;

    const-string v2, "co.uk.getmondo_refreshData"

    iget-object v3, p0, Lco/uk/getmondo/common/accounts/k;->b:Lcom/google/gson/f;

    invoke-virtual {p1}, Lco/uk/getmondo/d/ai;->c()Lco/uk/getmondo/d/ai$a;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/google/gson/f;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v0, v2, v3}, Landroid/accounts/AccountManager;->setUserData(Landroid/accounts/Account;Ljava/lang/String;Ljava/lang/String;)V

    .line 114
    return-void
.end method

.method public a(Lco/uk/getmondo/d/ai;Ljava/lang/String;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 105
    new-instance v0, Landroid/accounts/Account;

    const-string v1, "co.uk.getmondo"

    invoke-direct {v0, p2, v1}, Landroid/accounts/Account;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 106
    iget-object v1, p0, Lco/uk/getmondo/common/accounts/k;->a:Landroid/accounts/AccountManager;

    invoke-virtual {v1, v0, v2, v2}, Landroid/accounts/AccountManager;->addAccountExplicitly(Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;)Z

    .line 107
    invoke-virtual {p0, p1}, Lco/uk/getmondo/common/accounts/k;->a(Lco/uk/getmondo/d/ai;)V

    .line 108
    return-void
.end method

.method public declared-synchronized a(Lco/uk/getmondo/d/ak;)V
    .locals 4

    .prologue
    .line 117
    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lco/uk/getmondo/common/accounts/k;->f()Landroid/accounts/Account;

    move-result-object v0

    .line 118
    if-eqz v0, :cond_0

    .line 119
    iget-object v1, p0, Lco/uk/getmondo/common/accounts/k;->b:Lcom/google/gson/f;

    const-class v2, Lco/uk/getmondo/d/ak;

    invoke-virtual {v1, p1, v2}, Lcom/google/gson/f;->a(Ljava/lang/Object;Ljava/lang/reflect/Type;)Ljava/lang/String;

    move-result-object v1

    .line 120
    iput-object p1, p0, Lco/uk/getmondo/common/accounts/k;->d:Lco/uk/getmondo/d/ak;

    .line 121
    iget-object v2, p0, Lco/uk/getmondo/common/accounts/k;->a:Landroid/accounts/AccountManager;

    const-string v3, "co.uk.getmondo_userKey"

    invoke-virtual {v2, v0, v3, v1}, Landroid/accounts/AccountManager;->setUserData(Landroid/accounts/Account;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 125
    monitor-exit p0

    return-void

    .line 123
    :cond_0
    :try_start_1
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Attempted to store a User when logged out"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 117
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public b()Ljava/lang/String;
    .locals 4

    .prologue
    .line 73
    invoke-direct {p0}, Lco/uk/getmondo/common/accounts/k;->f()Landroid/accounts/Account;

    move-result-object v0

    .line 74
    if-eqz v0, :cond_0

    .line 76
    :try_start_0
    iget-object v1, p0, Lco/uk/getmondo/common/accounts/k;->a:Landroid/accounts/AccountManager;

    const-string v2, "co.uk.getmondo_authTokenType"

    const/4 v3, 0x1

    invoke-virtual {v1, v0, v2, v3}, Landroid/accounts/AccountManager;->blockingGetAuthToken(Landroid/accounts/Account;Ljava/lang/String;Z)Ljava/lang/String;
    :try_end_0
    .catch Landroid/accounts/OperationCanceledException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Landroid/accounts/AuthenticatorException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 81
    :goto_0
    return-object v0

    .line 77
    :catch_0
    move-exception v0

    .line 78
    :goto_1
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "Failed to get auth token"

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    invoke-static {v1}, Ld/a/a;->a(Ljava/lang/Throwable;)V

    .line 81
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 77
    :catch_1
    move-exception v0

    goto :goto_1

    :catch_2
    move-exception v0

    goto :goto_1
.end method

.method public b(Landroid/accounts/OnAccountsUpdateListener;)V
    .locals 1

    .prologue
    .line 101
    iget-object v0, p0, Lco/uk/getmondo/common/accounts/k;->a:Landroid/accounts/AccountManager;

    invoke-virtual {v0, p1}, Landroid/accounts/AccountManager;->removeOnAccountsUpdatedListener(Landroid/accounts/OnAccountsUpdateListener;)V

    .line 102
    return-void
.end method

.method public declared-synchronized c()Lco/uk/getmondo/d/ak;
    .locals 3

    .prologue
    .line 86
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lco/uk/getmondo/common/accounts/k;->d:Lco/uk/getmondo/d/ak;

    if-nez v0, :cond_0

    .line 87
    invoke-direct {p0}, Lco/uk/getmondo/common/accounts/k;->f()Landroid/accounts/Account;

    move-result-object v0

    .line 88
    if-eqz v0, :cond_0

    .line 89
    iget-object v1, p0, Lco/uk/getmondo/common/accounts/k;->a:Landroid/accounts/AccountManager;

    const-string v2, "co.uk.getmondo_userKey"

    invoke-virtual {v1, v0, v2}, Landroid/accounts/AccountManager;->getUserData(Landroid/accounts/Account;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 90
    iget-object v1, p0, Lco/uk/getmondo/common/accounts/k;->b:Lcom/google/gson/f;

    const-class v2, Lco/uk/getmondo/d/ak;

    invoke-virtual {v1, v0, v2}, Lcom/google/gson/f;->a(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/d/ak;

    iput-object v0, p0, Lco/uk/getmondo/common/accounts/k;->d:Lco/uk/getmondo/d/ak;

    .line 93
    :cond_0
    iget-object v0, p0, Lco/uk/getmondo/common/accounts/k;->d:Lco/uk/getmondo/d/ak;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 86
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public d()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 128
    iput-object v3, p0, Lco/uk/getmondo/common/accounts/k;->d:Lco/uk/getmondo/d/ak;

    .line 129
    invoke-direct {p0}, Lco/uk/getmondo/common/accounts/k;->f()Landroid/accounts/Account;

    move-result-object v0

    .line 130
    if-eqz v0, :cond_0

    .line 131
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x16

    if-ge v1, v2, :cond_1

    .line 133
    iget-object v1, p0, Lco/uk/getmondo/common/accounts/k;->a:Landroid/accounts/AccountManager;

    invoke-virtual {v1, v0, v3, v3}, Landroid/accounts/AccountManager;->removeAccount(Landroid/accounts/Account;Landroid/accounts/AccountManagerCallback;Landroid/os/Handler;)Landroid/accounts/AccountManagerFuture;

    .line 138
    :cond_0
    :goto_0
    return-void

    .line 135
    :cond_1
    iget-object v1, p0, Lco/uk/getmondo/common/accounts/k;->a:Landroid/accounts/AccountManager;

    invoke-virtual {v1, v0}, Landroid/accounts/AccountManager;->removeAccountExplicitly(Landroid/accounts/Account;)Z

    goto :goto_0
.end method

.method public e()Z
    .locals 1

    .prologue
    .line 141
    invoke-direct {p0}, Lco/uk/getmondo/common/accounts/k;->f()Landroid/accounts/Account;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
