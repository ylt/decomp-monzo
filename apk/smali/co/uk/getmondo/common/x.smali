.class public Lco/uk/getmondo/common/x;
.super Ljava/lang/Object;
.source "SddMigrationStorage.java"


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14
    return-void
.end method

.method static synthetic a(Lco/uk/getmondo/common/j/g$a;)Lco/uk/getmondo/d/ag;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 30
    invoke-virtual {p0}, Lco/uk/getmondo/common/j/g$a;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 31
    new-instance v0, Lco/uk/getmondo/d/ag;

    invoke-direct {v0}, Lco/uk/getmondo/d/ag;-><init>()V

    .line 33
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Lco/uk/getmondo/common/j/g$a;->d()Lio/realm/bb;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/d/ag;

    goto :goto_0
.end method

.method static synthetic a(Lio/realm/av;)Lio/realm/bg;
    .locals 1

    .prologue
    .line 27
    const-class v0, Lco/uk/getmondo/d/ag;

    invoke-virtual {p0, v0}, Lio/realm/av;->a(Ljava/lang/Class;)Lio/realm/bf;

    move-result-object v0

    .line 28
    invoke-virtual {v0}, Lio/realm/bf;->g()Lio/realm/bg;

    move-result-object v0

    .line 27
    return-object v0
.end method

.method static synthetic a(Ljava/lang/String;Lio/realm/av;)V
    .locals 1

    .prologue
    .line 19
    const-class v0, Lco/uk/getmondo/d/ag;

    invoke-virtual {p1, v0}, Lio/realm/av;->b(Ljava/lang/Class;)V

    .line 20
    new-instance v0, Lco/uk/getmondo/d/ag;

    invoke-direct {v0, p0}, Lco/uk/getmondo/d/ag;-><init>(Ljava/lang/String;)V

    .line 21
    invoke-virtual {p1, v0}, Lio/realm/av;->c(Lio/realm/bb;)V

    .line 22
    return-void
.end method


# virtual methods
.method public a()Lio/reactivex/n;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Lco/uk/getmondo/d/ag;",
            ">;"
        }
    .end annotation

    .prologue
    .line 27
    invoke-static {}, Lco/uk/getmondo/common/z;->a()Lkotlin/d/a/b;

    move-result-object v0

    invoke-static {v0}, Lco/uk/getmondo/common/j/g;->a(Lkotlin/d/a/b;)Lio/reactivex/n;

    move-result-object v0

    invoke-static {}, Lco/uk/getmondo/common/aa;->a()Lio/reactivex/c/h;

    move-result-object v1

    .line 29
    invoke-virtual {v0, v1}, Lio/reactivex/n;->map(Lio/reactivex/c/h;)Lio/reactivex/n;

    move-result-object v0

    .line 27
    return-object v0
.end method

.method public a(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 17
    invoke-static {}, Lio/realm/av;->n()Lio/realm/av;

    move-result-object v2

    const/4 v1, 0x0

    .line 18
    :try_start_0
    invoke-static {p1}, Lco/uk/getmondo/common/y;->a(Ljava/lang/String;)Lio/realm/av$a;

    move-result-object v0

    invoke-virtual {v2, v0}, Lio/realm/av;->a(Lio/realm/av$a;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 23
    if-eqz v2, :cond_0

    if-eqz v1, :cond_1

    :try_start_1
    invoke-virtual {v2}, Lio/realm/av;->close()V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_1

    .line 24
    :cond_0
    :goto_0
    return-void

    .line 23
    :cond_1
    invoke-virtual {v2}, Lio/realm/av;->close()V

    goto :goto_0

    .line 17
    :catch_0
    move-exception v0

    :try_start_2
    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 23
    :catchall_0
    move-exception v1

    move-object v3, v1

    move-object v1, v0

    move-object v0, v3

    :goto_1
    if-eqz v2, :cond_2

    if-eqz v1, :cond_3

    :try_start_3
    invoke-virtual {v2}, Lio/realm/av;->close()V
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_2

    :cond_2
    :goto_2
    throw v0

    :cond_3
    invoke-virtual {v2}, Lio/realm/av;->close()V

    goto :goto_2

    :catch_1
    move-exception v0

    goto :goto_0

    :catch_2
    move-exception v1

    goto :goto_2

    :catchall_1
    move-exception v0

    goto :goto_1
.end method
