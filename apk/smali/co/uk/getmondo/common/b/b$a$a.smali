.class public final enum Lco/uk/getmondo/common/b/b$a$a;
.super Ljava/lang/Enum;
.source "QueryResults.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lco/uk/getmondo/common/b/b$a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lco/uk/getmondo/common/b/b$a$a;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000\u000c\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\u0008\u0005\u0008\u0086\u0001\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00000\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002j\u0002\u0008\u0003j\u0002\u0008\u0004j\u0002\u0008\u0005\u00a8\u0006\u0006"
    }
    d2 = {
        "Lco/uk/getmondo/common/data/QueryResults$Change$Type;",
        "",
        "(Ljava/lang/String;I)V",
        "INSERTION",
        "DELETION",
        "MODIFICATION",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# static fields
.field public static final enum a:Lco/uk/getmondo/common/b/b$a$a;

.field public static final enum b:Lco/uk/getmondo/common/b/b$a$a;

.field public static final enum c:Lco/uk/getmondo/common/b/b$a$a;

.field private static final synthetic d:[Lco/uk/getmondo/common/b/b$a$a;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    const/4 v0, 0x3

    new-array v0, v0, [Lco/uk/getmondo/common/b/b$a$a;

    new-instance v1, Lco/uk/getmondo/common/b/b$a$a;

    const-string v2, "INSERTION"

    invoke-direct {v1, v2, v3}, Lco/uk/getmondo/common/b/b$a$a;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lco/uk/getmondo/common/b/b$a$a;->a:Lco/uk/getmondo/common/b/b$a$a;

    aput-object v1, v0, v3

    new-instance v1, Lco/uk/getmondo/common/b/b$a$a;

    const-string v2, "DELETION"

    invoke-direct {v1, v2, v4}, Lco/uk/getmondo/common/b/b$a$a;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lco/uk/getmondo/common/b/b$a$a;->b:Lco/uk/getmondo/common/b/b$a$a;

    aput-object v1, v0, v4

    new-instance v1, Lco/uk/getmondo/common/b/b$a$a;

    const-string v2, "MODIFICATION"

    invoke-direct {v1, v2, v5}, Lco/uk/getmondo/common/b/b$a$a;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lco/uk/getmondo/common/b/b$a$a;->c:Lco/uk/getmondo/common/b/b$a$a;

    aput-object v1, v0, v5

    sput-object v0, Lco/uk/getmondo/common/b/b$a$a;->d:[Lco/uk/getmondo/common/b/b$a$a;

    return-void
.end method

.method protected constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 14
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lco/uk/getmondo/common/b/b$a$a;
    .locals 1

    const-class v0, Lco/uk/getmondo/common/b/b$a$a;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/common/b/b$a$a;

    return-object v0
.end method

.method public static values()[Lco/uk/getmondo/common/b/b$a$a;
    .locals 1

    sget-object v0, Lco/uk/getmondo/common/b/b$a$a;->d:[Lco/uk/getmondo/common/b/b$a$a;

    invoke-virtual {v0}, [Lco/uk/getmondo/common/b/b$a$a;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lco/uk/getmondo/common/b/b$a$a;

    return-object v0
.end method
