.class public final Lco/uk/getmondo/common/e/c;
.super Ljava/lang/Object;
.source "ErrorExt.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000$\n\u0000\n\u0002\u0010\u000e\n\u0002\u0010\u0003\n\u0002\u0008\u0003\n\u0002\u0010\u0008\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0011\n\u0002\u0008\u0002\u001a)\u0010\t\u001a\u0004\u0018\u0001H\n\"\u0008\u0008\u0000\u0010\n*\u00020\u000b*\u00020\u00022\u000c\u0010\u000c\u001a\u0008\u0012\u0004\u0012\u0002H\n0\r\u00a2\u0006\u0002\u0010\u000e\"\u0015\u0010\u0000\u001a\u00020\u0001*\u00020\u00028F\u00a2\u0006\u0006\u001a\u0004\u0008\u0003\u0010\u0004\"\u0017\u0010\u0005\u001a\u0004\u0018\u00010\u0006*\u00020\u00028F\u00a2\u0006\u0006\u001a\u0004\u0008\u0007\u0010\u0008\u00a8\u0006\u000f"
    }
    d2 = {
        "apiErrorCode",
        "",
        "",
        "getApiErrorCode",
        "(Ljava/lang/Throwable;)Ljava/lang/String;",
        "httpErrorCode",
        "",
        "getHttpErrorCode",
        "(Ljava/lang/Throwable;)Ljava/lang/Integer;",
        "matches",
        "E",
        "Lco/uk/getmondo/common/errors/MatchableError;",
        "errors",
        "",
        "(Ljava/lang/Throwable;[Lco/uk/getmondo/common/errors/MatchableError;)Lco/uk/getmondo/common/errors/MatchableError;",
        "app_monzoPrepaidRelease"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# direct methods
.method public static final a(Ljava/lang/Throwable;[Lco/uk/getmondo/common/e/f;)Lco/uk/getmondo/common/e/f;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E::",
            "Lco/uk/getmondo/common/e/f;",
            ">(",
            "Ljava/lang/Throwable;",
            "[TE;)TE;"
        }
    .end annotation

    .prologue
    const-string v0, "$receiver"

    invoke-static {p0, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "errors"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 21
    invoke-static {p0}, Lco/uk/getmondo/common/e/c;->a(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lco/uk/getmondo/common/e/d;->a([Lco/uk/getmondo/common/e/f;Ljava/lang/String;)Lco/uk/getmondo/common/e/f;

    move-result-object v0

    return-object v0
.end method

.method public static final a(Ljava/lang/Throwable;)Ljava/lang/String;
    .locals 1

    .prologue
    const-string v0, "$receiver"

    invoke-static {p0, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 7
    instance-of v0, p0, Lco/uk/getmondo/api/ApiException;

    if-eqz v0, :cond_1

    .line 8
    check-cast p0, Lco/uk/getmondo/api/ApiException;

    invoke-virtual {p0}, Lco/uk/getmondo/api/ApiException;->e()Lco/uk/getmondo/api/model/b;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lco/uk/getmondo/api/model/b;->a()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 10
    :goto_0
    return-object v0

    .line 8
    :cond_0
    const-string v0, ""

    goto :goto_0

    .line 10
    :cond_1
    const-string v0, ""

    goto :goto_0
.end method

.method public static final b(Ljava/lang/Throwable;)Ljava/lang/Integer;
    .locals 1

    .prologue
    const-string v0, "$receiver"

    invoke-static {p0, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 15
    instance-of v0, p0, Lco/uk/getmondo/api/ApiException;

    if-eqz v0, :cond_0

    .line 16
    check-cast p0, Lco/uk/getmondo/api/ApiException;

    invoke-virtual {p0}, Lco/uk/getmondo/api/ApiException;->b()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 18
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
