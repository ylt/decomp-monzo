.class public final Lco/uk/getmondo/common/e/a;
.super Ljava/lang/Object;
.source "ApiErrorHandler.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lco/uk/getmondo/common/e/a$a;,
        Lco/uk/getmondo/common/e/a$b;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000P\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0003\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0003\u0018\u0000 \u001c2\u00020\u0001:\u0002\u001b\u001cB\u0017\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J,\u0010\u0007\u001a\u00020\u00082\u0006\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\u000c2\u0012\u0010\r\u001a\u000e\u0012\u0004\u0012\u00020\u000f\u0012\u0004\u0012\u00020\u00080\u000eH\u0002J\u0016\u0010\u0010\u001a\u00020\u00112\u0006\u0010\u0012\u001a\u00020\u00132\u0006\u0010\u000b\u001a\u00020\u000cJ*\u0010\u0010\u001a\u00020\u00112\u0006\u0010\u0012\u001a\u00020\u00132\u0006\u0010\u000b\u001a\u00020\u000c2\u0012\u0010\r\u001a\u000e\u0012\u0004\u0012\u00020\u000f\u0012\u0004\u0012\u00020\u00080\u000eJ\u0012\u0010\u0014\u001a\u00020\u00112\u0008\u0010\u0015\u001a\u0004\u0018\u00010\u0013H\u0002J \u0010\u0016\u001a\u00020\u00082\u0006\u0010\u0017\u001a\u00020\u00182\u0006\u0010\u0019\u001a\u00020\u001a2\u0006\u0010\u000b\u001a\u00020\u000cH\u0002R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001d"
    }
    d2 = {
        "Lco/uk/getmondo/common/errors/ApiErrorHandler;",
        "",
        "accountService",
        "Lco/uk/getmondo/common/accounts/AccountService;",
        "analyticsService",
        "Lco/uk/getmondo/common/AnalyticsService;",
        "(Lco/uk/getmondo/common/accounts/AccountService;Lco/uk/getmondo/common/AnalyticsService;)V",
        "handleApiException",
        "",
        "apiException",
        "Lco/uk/getmondo/api/ApiException;",
        "view",
        "Lco/uk/getmondo/common/errors/ApiErrorHandler$ApiErrorView;",
        "showError",
        "Lkotlin/Function1;",
        "",
        "handleError",
        "",
        "throwable",
        "",
        "isNetworkException",
        "t",
        "performLogOut",
        "errorCause",
        "Lco/uk/getmondo/api/authentication/AuthError;",
        "logoutReason",
        "",
        "ApiErrorView",
        "Companion",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# static fields
.field public static final a:Lco/uk/getmondo/common/e/a$b;


# instance fields
.field private final b:Lco/uk/getmondo/common/accounts/d;

.field private final c:Lco/uk/getmondo/common/a;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lco/uk/getmondo/common/e/a$b;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lco/uk/getmondo/common/e/a$b;-><init>(Lkotlin/d/b/i;)V

    sput-object v0, Lco/uk/getmondo/common/e/a;->a:Lco/uk/getmondo/common/e/a$b;

    return-void
.end method

.method public constructor <init>(Lco/uk/getmondo/common/accounts/d;Lco/uk/getmondo/common/a;)V
    .locals 1

    .prologue
    const-string v0, "accountService"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "analyticsService"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lco/uk/getmondo/common/e/a;->b:Lco/uk/getmondo/common/accounts/d;

    iput-object p2, p0, Lco/uk/getmondo/common/e/a;->c:Lco/uk/getmondo/common/a;

    return-void
.end method

.method private final a(Lco/uk/getmondo/api/ApiException;Lco/uk/getmondo/common/e/a$a;Lkotlin/d/a/b;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lco/uk/getmondo/api/ApiException;",
            "Lco/uk/getmondo/common/e/a$a;",
            "Lkotlin/d/a/b",
            "<-",
            "Ljava/lang/Integer;",
            "Lkotlin/n;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 59
    new-instance v1, Ljava/lang/Exception;

    const-string v2, "Handling api error"

    move-object v0, p1

    check-cast v0, Ljava/lang/Throwable;

    invoke-direct {v1, v2, v0}, Ljava/lang/Exception;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    move-object v0, v1

    check-cast v0, Ljava/lang/Throwable;

    const-string v1, "Api Error with view %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Ld/a/a;->a(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 61
    iget-object v0, p0, Lco/uk/getmondo/common/e/a;->c:Lco/uk/getmondo/common/a;

    sget-object v1, Lco/uk/getmondo/api/model/tracking/Impression;->Companion:Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    invoke-virtual {p1}, Lco/uk/getmondo/api/ApiException;->b()I

    move-result v2

    invoke-virtual {p1}, Lco/uk/getmondo/api/ApiException;->c()Ljava/lang/String;

    move-result-object v3

    const-string v4, "apiException.path"

    invoke-static {v3, v4}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 62
    invoke-virtual {p1}, Lco/uk/getmondo/api/ApiException;->d()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1}, Lco/uk/getmondo/api/ApiException;->e()Lco/uk/getmondo/api/model/b;

    move-result-object v5

    .line 61
    invoke-virtual {v1, v2, v3, v4, v5}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->b(ILjava/lang/String;Ljava/lang/String;Lco/uk/getmondo/api/model/b;)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v1

    invoke-virtual {v0, v1}, Lco/uk/getmondo/common/a;->a(Lco/uk/getmondo/api/model/tracking/Impression;)V

    .line 64
    invoke-virtual {p1}, Lco/uk/getmondo/api/ApiException;->b()I

    move-result v1

    .line 65
    invoke-virtual {p1}, Lco/uk/getmondo/api/ApiException;->e()Lco/uk/getmondo/api/model/b;

    move-result-object v2

    .line 68
    const/16 v0, 0x1f3

    const/16 v3, 0x190

    if-le v3, v1, :cond_1

    .line 76
    :cond_0
    const/16 v0, 0x1f4

    if-lt v1, v0, :cond_3

    .line 80
    const v0, 0x7f0a0197

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {p3, v0}, Lkotlin/d/a/b;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 84
    :goto_0
    return-void

    .line 68
    :cond_1
    if-lt v0, v1, :cond_0

    if-eqz v2, :cond_0

    .line 69
    invoke-static {}, Lco/uk/getmondo/api/authentication/a;->values()[Lco/uk/getmondo/api/authentication/a;

    move-result-object v0

    check-cast v0, [Lco/uk/getmondo/common/e/f;

    invoke-virtual {v2}, Lco/uk/getmondo/api/model/b;->a()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lco/uk/getmondo/common/e/d;->a([Lco/uk/getmondo/common/e/f;Ljava/lang/String;)Lco/uk/getmondo/common/e/f;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/api/authentication/a;

    .line 70
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lco/uk/getmondo/api/authentication/a;->b()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 71
    invoke-virtual {v2}, Lco/uk/getmondo/api/model/b;->b()Ljava/lang/String;

    move-result-object v1

    const-string v2, "apiError.message"

    invoke-static {v1, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, v0, v1, p2}, Lco/uk/getmondo/common/e/a;->a(Lco/uk/getmondo/api/authentication/a;Ljava/lang/String;Lco/uk/getmondo/common/e/a$a;)V

    goto :goto_0

    .line 73
    :cond_2
    invoke-virtual {v2}, Lco/uk/getmondo/api/model/b;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 74
    invoke-interface {p2}, Lco/uk/getmondo/common/e/a$a;->q()V

    goto :goto_0

    .line 83
    :cond_3
    const v0, 0x7f0a0196

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {p3, v0}, Lkotlin/d/a/b;->a(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method private final a(Lco/uk/getmondo/api/authentication/a;Ljava/lang/String;Lco/uk/getmondo/common/e/a$a;)V
    .locals 3

    .prologue
    .line 88
    iget-object v0, p0, Lco/uk/getmondo/common/e/a;->b:Lco/uk/getmondo/common/accounts/d;

    invoke-virtual {v0}, Lco/uk/getmondo/common/accounts/d;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 89
    iget-object v0, p0, Lco/uk/getmondo/common/e/a;->b:Lco/uk/getmondo/common/accounts/d;

    invoke-virtual {v0}, Lco/uk/getmondo/common/accounts/d;->b()Lco/uk/getmondo/d/ak;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lco/uk/getmondo/d/ak;->d()Lco/uk/getmondo/d/ac;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lco/uk/getmondo/d/ac;->b()Ljava/lang/String;

    move-result-object v0

    :goto_0
    if-nez v0, :cond_0

    invoke-static {}, Lkotlin/d/b/l;->a()V

    .line 90
    :cond_0
    iget-object v1, p0, Lco/uk/getmondo/common/e/a;->c:Lco/uk/getmondo/common/a;

    sget-object v2, Lco/uk/getmondo/api/model/tracking/Impression;->Companion:Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    invoke-virtual {v2, p2, v0}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->a(Ljava/lang/String;Ljava/lang/String;)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    invoke-virtual {v1, v0}, Lco/uk/getmondo/common/a;->a(Lco/uk/getmondo/api/model/tracking/Impression;)V

    .line 92
    iget-object v0, p0, Lco/uk/getmondo/common/e/a;->b:Lco/uk/getmondo/common/accounts/d;

    invoke-virtual {v0}, Lco/uk/getmondo/common/accounts/d;->e()V

    .line 93
    sget-object v0, Lco/uk/getmondo/api/authentication/a;->a:Lco/uk/getmondo/api/authentication/a;

    invoke-static {p1, v0}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 94
    const v0, 0x7f0a0430

    invoke-interface {p3, v0}, Lco/uk/getmondo/common/e/a$a;->c(I)V

    .line 97
    :cond_1
    :goto_1
    return-void

    .line 89
    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    .line 96
    :cond_3
    invoke-interface {p3}, Lco/uk/getmondo/common/e/a$a;->u()V

    goto :goto_1
.end method

.method public static final a(Ljava/lang/Throwable;)Z
    .locals 1

    sget-object v0, Lco/uk/getmondo/common/e/a;->a:Lco/uk/getmondo/common/e/a$b;

    invoke-virtual {v0, p0}, Lco/uk/getmondo/common/e/a$b;->a(Ljava/lang/Throwable;)Z

    move-result v0

    return v0
.end method

.method private final b(Ljava/lang/Throwable;)Z
    .locals 1

    .prologue
    .line 101
    if-eqz p1, :cond_0

    instance-of v0, p1, Ljava/io/IOException;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Ljava/lang/Throwable;Lco/uk/getmondo/common/e/a$a;)Z
    .locals 1

    .prologue
    const-string v0, "throwable"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "view"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 25
    new-instance v0, Lco/uk/getmondo/common/e/a$c;

    invoke-direct {v0, p2}, Lco/uk/getmondo/common/e/a$c;-><init>(Lco/uk/getmondo/common/e/a$a;)V

    check-cast v0, Lkotlin/d/a/b;

    invoke-virtual {p0, p1, p2, v0}, Lco/uk/getmondo/common/e/a;->a(Ljava/lang/Throwable;Lco/uk/getmondo/common/e/a$a;Lkotlin/d/a/b;)Z

    move-result v0

    return v0
.end method

.method public final a(Ljava/lang/Throwable;Lco/uk/getmondo/common/e/a$a;Lkotlin/d/a/b;)Z
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Throwable;",
            "Lco/uk/getmondo/common/e/a$a;",
            "Lkotlin/d/a/b",
            "<-",
            "Ljava/lang/Integer;",
            "Lkotlin/n;",
            ">;)Z"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    const-string v0, "throwable"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "view"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "showError"

    invoke-static {p3, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 33
    sget-object v0, Lco/uk/getmondo/common/e/a;->a:Lco/uk/getmondo/common/e/a$b;

    invoke-virtual {v0, p1}, Lco/uk/getmondo/common/e/a$b;->a(Ljava/lang/Throwable;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lco/uk/getmondo/common/e/a;->a:Lco/uk/getmondo/common/e/a$b;

    invoke-virtual {p1}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v3

    invoke-virtual {v0, v3}, Lco/uk/getmondo/common/e/a$b;->a(Ljava/lang/Throwable;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 34
    :cond_0
    const v0, 0x7f0a0183

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {p3, v0}, Lkotlin/d/a/b;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 35
    new-instance v0, Ljava/lang/Exception;

    const-string v3, "Wrong certificate"

    invoke-direct {v0, v3, p1}, Ljava/lang/Exception;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    check-cast v0, Ljava/lang/Throwable;

    const-string v3, "Wrong certificate with view %s"

    new-array v4, v1, [Ljava/lang/Object;

    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v2

    invoke-static {v0, v3, v4}, Ld/a/a;->a(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    move v0, v1

    .line 54
    :goto_0
    return v0

    .line 39
    :cond_1
    instance-of v0, p1, Lco/uk/getmondo/api/ApiException;

    if-eqz v0, :cond_2

    .line 40
    check-cast p1, Lco/uk/getmondo/api/ApiException;

    invoke-direct {p0, p1, p2, p3}, Lco/uk/getmondo/common/e/a;->a(Lco/uk/getmondo/api/ApiException;Lco/uk/getmondo/common/e/a$a;Lkotlin/d/a/b;)V

    move v0, v1

    .line 41
    goto :goto_0

    .line 42
    :cond_2
    invoke-virtual {p1}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    if-eqz v0, :cond_4

    invoke-virtual {p1}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    instance-of v0, v0, Lco/uk/getmondo/api/ApiException;

    if-eqz v0, :cond_4

    .line 43
    invoke-virtual {p1}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    if-nez v0, :cond_3

    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type co.uk.getmondo.api.ApiException"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_3
    check-cast v0, Lco/uk/getmondo/api/ApiException;

    invoke-direct {p0, v0, p2, p3}, Lco/uk/getmondo/common/e/a;->a(Lco/uk/getmondo/api/ApiException;Lco/uk/getmondo/common/e/a$a;Lkotlin/d/a/b;)V

    move v0, v1

    .line 44
    goto :goto_0

    .line 45
    :cond_4
    invoke-direct {p0, p1}, Lco/uk/getmondo/common/e/a;->b(Ljava/lang/Throwable;)Z

    move-result v0

    if-nez v0, :cond_5

    invoke-virtual {p1}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    invoke-direct {p0, v0}, Lco/uk/getmondo/common/e/a;->b(Ljava/lang/Throwable;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 48
    :cond_5
    const v0, 0x7f0a01c7

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {p3, v0}, Lkotlin/d/a/b;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 49
    new-instance v0, Ljava/lang/Exception;

    const-string v3, "Network error"

    invoke-direct {v0, v3, p1}, Ljava/lang/Exception;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    check-cast v0, Ljava/lang/Throwable;

    const-string v3, "Network failure with view %s"

    new-array v4, v1, [Ljava/lang/Object;

    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v2

    invoke-static {v0, v3, v4}, Ld/a/a;->a(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    move v0, v1

    .line 50
    goto :goto_0

    .line 53
    :cond_6
    new-instance v0, Ljava/lang/Exception;

    const-string v3, "Failed to handle error"

    invoke-direct {v0, v3, p1}, Ljava/lang/Exception;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    check-cast v0, Ljava/lang/Throwable;

    const-string v3, "Failed to handle error with view %s"

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v1, v2

    invoke-static {v0, v3, v1}, Ld/a/a;->a(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    move v0, v2

    .line 54
    goto :goto_0
.end method
