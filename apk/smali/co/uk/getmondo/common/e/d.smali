.class public Lco/uk/getmondo/common/e/d;
.super Ljava/lang/Object;
.source "ErrorMatcher.java"


# direct methods
.method public static a([Lco/uk/getmondo/common/e/f;Ljava/lang/String;)Lco/uk/getmondo/common/e/f;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E::",
            "Lco/uk/getmondo/common/e/f;",
            ">([TE;",
            "Ljava/lang/String;",
            ")TE;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 14
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    if-eqz p0, :cond_0

    array-length v2, p0

    if-nez v2, :cond_1

    .line 30
    :cond_0
    :goto_0
    return-object v1

    .line 20
    :cond_1
    array-length v4, p0

    move v3, v0

    move-object v2, v1

    :goto_1
    if-ge v3, v4, :cond_2

    aget-object v1, p0, v3

    .line 21
    invoke-interface {v1}, Lco/uk/getmondo/common/e/f;->a()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 25
    invoke-interface {v1}, Lco/uk/getmondo/common/e/f;->a()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_3

    invoke-interface {v1}, Lco/uk/getmondo/common/e/f;->a()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    if-le v5, v0, :cond_3

    .line 26
    invoke-interface {v1}, Lco/uk/getmondo/common/e/f;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    .line 20
    :goto_2
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    move-object v2, v1

    goto :goto_1

    :cond_2
    move-object v1, v2

    .line 30
    goto :goto_0

    :cond_3
    move-object v1, v2

    goto :goto_2
.end method
