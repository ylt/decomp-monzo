.class public final Lco/uk/getmondo/common/h/a/o$a;
.super Ljava/lang/Object;
.source "DaggerApplicationComponent.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lco/uk/getmondo/common/h/a/o;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation


# instance fields
.field private a:Lco/uk/getmondo/common/h/a/b;

.field private b:Lco/uk/getmondo/api/c;

.field private c:Lco/uk/getmondo/api/y;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 1143
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lco/uk/getmondo/common/h/a/o$1;)V
    .locals 0

    .prologue
    .line 1136
    invoke-direct {p0}, Lco/uk/getmondo/common/h/a/o$a;-><init>()V

    return-void
.end method

.method static synthetic a(Lco/uk/getmondo/common/h/a/o$a;)Lco/uk/getmondo/common/h/a/b;
    .locals 1

    .prologue
    .line 1136
    iget-object v0, p0, Lco/uk/getmondo/common/h/a/o$a;->a:Lco/uk/getmondo/common/h/a/b;

    return-object v0
.end method

.method static synthetic b(Lco/uk/getmondo/common/h/a/o$a;)Lco/uk/getmondo/api/c;
    .locals 1

    .prologue
    .line 1136
    iget-object v0, p0, Lco/uk/getmondo/common/h/a/o$a;->b:Lco/uk/getmondo/api/c;

    return-object v0
.end method

.method static synthetic c(Lco/uk/getmondo/common/h/a/o$a;)Lco/uk/getmondo/api/y;
    .locals 1

    .prologue
    .line 1136
    iget-object v0, p0, Lco/uk/getmondo/common/h/a/o$a;->c:Lco/uk/getmondo/api/y;

    return-object v0
.end method


# virtual methods
.method public a()Lco/uk/getmondo/common/h/a/a;
    .locals 3

    .prologue
    .line 1146
    iget-object v0, p0, Lco/uk/getmondo/common/h/a/o$a;->a:Lco/uk/getmondo/common/h/a/b;

    if-nez v0, :cond_0

    .line 1147
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-class v2, Lco/uk/getmondo/common/h/a/b;

    .line 1148
    invoke-virtual {v2}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " must be set"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1150
    :cond_0
    iget-object v0, p0, Lco/uk/getmondo/common/h/a/o$a;->b:Lco/uk/getmondo/api/c;

    if-nez v0, :cond_1

    .line 1151
    new-instance v0, Lco/uk/getmondo/api/c;

    invoke-direct {v0}, Lco/uk/getmondo/api/c;-><init>()V

    iput-object v0, p0, Lco/uk/getmondo/common/h/a/o$a;->b:Lco/uk/getmondo/api/c;

    .line 1153
    :cond_1
    iget-object v0, p0, Lco/uk/getmondo/common/h/a/o$a;->c:Lco/uk/getmondo/api/y;

    if-nez v0, :cond_2

    .line 1154
    new-instance v0, Lco/uk/getmondo/api/y;

    invoke-direct {v0}, Lco/uk/getmondo/api/y;-><init>()V

    iput-object v0, p0, Lco/uk/getmondo/common/h/a/o$a;->c:Lco/uk/getmondo/api/y;

    .line 1156
    :cond_2
    new-instance v0, Lco/uk/getmondo/common/h/a/o;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lco/uk/getmondo/common/h/a/o;-><init>(Lco/uk/getmondo/common/h/a/o$a;Lco/uk/getmondo/common/h/a/o$1;)V

    return-object v0
.end method

.method public a(Lco/uk/getmondo/common/h/a/b;)Lco/uk/getmondo/common/h/a/o$a;
    .locals 1

    .prologue
    .line 1160
    invoke-static {p1}, Lb/a/d;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/common/h/a/b;

    iput-object v0, p0, Lco/uk/getmondo/common/h/a/o$a;->a:Lco/uk/getmondo/common/h/a/b;

    .line 1161
    return-object p0
.end method
