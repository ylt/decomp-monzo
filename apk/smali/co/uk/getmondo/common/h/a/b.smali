.class public Lco/uk/getmondo/common/h/a/b;
.super Ljava/lang/Object;
.source "ApplicationModule.java"


# instance fields
.field protected final a:Landroid/app/Application;


# direct methods
.method public constructor <init>(Landroid/app/Application;)V
    .locals 0

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    iput-object p1, p0, Lco/uk/getmondo/common/h/a/b;->a:Landroid/app/Application;

    .line 39
    return-void
.end method


# virtual methods
.method a()Landroid/app/Application;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lco/uk/getmondo/common/h/a/b;->a:Landroid/app/Application;

    return-object v0
.end method

.method a(Landroid/content/Context;Lco/uk/getmondo/payments/send/data/p;Lco/uk/getmondo/payments/send/data/h;Lco/uk/getmondo/common/accounts/d;Lco/uk/getmondo/common/accounts/k;Lco/uk/getmondo/common/a;)Lco/uk/getmondo/common/a/c;
    .locals 7

    .prologue
    .line 103
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x19

    if-lt v0, v1, :cond_0

    .line 104
    new-instance v0, Lco/uk/getmondo/common/a/a;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    invoke-direct/range {v0 .. v6}, Lco/uk/getmondo/common/a/a;-><init>(Landroid/content/Context;Lco/uk/getmondo/payments/send/data/p;Lco/uk/getmondo/payments/send/data/h;Lco/uk/getmondo/common/accounts/d;Lco/uk/getmondo/common/accounts/k;Lco/uk/getmondo/common/a;)V

    .line 106
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lco/uk/getmondo/common/a/d;

    invoke-direct {v0}, Lco/uk/getmondo/common/a/d;-><init>()V

    goto :goto_0
.end method

.method a(Landroid/content/Context;)Lco/uk/getmondo/common/s;
    .locals 1

    .prologue
    .line 67
    new-instance v0, Lco/uk/getmondo/common/u;

    invoke-direct {v0, p1}, Lco/uk/getmondo/common/u;-><init>(Landroid/content/Context;)V

    return-object v0
.end method

.method b()Landroid/content/Context;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lco/uk/getmondo/common/h/a/b;->a:Landroid/app/Application;

    return-object v0
.end method

.method b(Landroid/content/Context;)Lio/michaelrocks/libphonenumber/android/h;
    .locals 1

    .prologue
    .line 95
    invoke-static {p1}, Lio/michaelrocks/libphonenumber/android/h;->a(Landroid/content/Context;)Lio/michaelrocks/libphonenumber/android/h;

    move-result-object v0

    return-object v0
.end method

.method c()Landroid/content/res/Resources;
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lco/uk/getmondo/common/h/a/b;->a:Landroid/app/Application;

    invoke-virtual {v0}, Landroid/app/Application;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    return-object v0
.end method

.method c(Landroid/content/Context;)Landroid/support/v4/b/b/a;
    .locals 1

    .prologue
    .line 111
    invoke-static {p1}, Landroid/support/v4/b/b/a;->a(Landroid/content/Context;)Landroid/support/v4/b/b/a;

    move-result-object v0

    return-object v0
.end method

.method d()Lco/uk/getmondo/common/accounts/k;
    .locals 2

    .prologue
    .line 61
    new-instance v0, Lco/uk/getmondo/common/accounts/k;

    iget-object v1, p0, Lco/uk/getmondo/common/h/a/b;->a:Landroid/app/Application;

    invoke-static {v1}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v1

    invoke-direct {v0, v1}, Lco/uk/getmondo/common/accounts/k;-><init>(Landroid/accounts/AccountManager;)V

    return-object v0
.end method

.method e()Lio/reactivex/u;
    .locals 1

    .prologue
    .line 73
    invoke-static {}, Lio/reactivex/h/a;->b()Lio/reactivex/u;

    move-result-object v0

    return-object v0
.end method

.method f()Lio/reactivex/u;
    .locals 1

    .prologue
    .line 79
    invoke-static {}, Lio/reactivex/a/b/a;->a()Lio/reactivex/u;

    move-result-object v0

    return-object v0
.end method

.method g()Lio/reactivex/u;
    .locals 1

    .prologue
    .line 85
    invoke-static {}, Lio/reactivex/h/a;->a()Lio/reactivex/u;

    move-result-object v0

    return-object v0
.end method

.method h()Lio/intercom/android/sdk/push/IntercomPushClient;
    .locals 1

    .prologue
    .line 90
    new-instance v0, Lio/intercom/android/sdk/push/IntercomPushClient;

    invoke-direct {v0}, Lio/intercom/android/sdk/push/IntercomPushClient;-><init>()V

    return-object v0
.end method
