.class public final Lco/uk/getmondo/common/h/a/i;
.super Ljava/lang/Object;
.source "ApplicationModule_ProvideIntercomPushClientFactory.java"

# interfaces
.implements Lb/a/b;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lb/a/b",
        "<",
        "Lio/intercom/android/sdk/push/IntercomPushClient;",
        ">;"
    }
.end annotation


# static fields
.field static final synthetic a:Z


# instance fields
.field private final b:Lco/uk/getmondo/common/h/a/b;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 8
    const-class v0, Lco/uk/getmondo/common/h/a/i;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lco/uk/getmondo/common/h/a/i;->a:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lco/uk/getmondo/common/h/a/b;)V
    .locals 1

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    sget-boolean v0, Lco/uk/getmondo/common/h/a/i;->a:Z

    if-nez v0, :cond_0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 18
    :cond_0
    iput-object p1, p0, Lco/uk/getmondo/common/h/a/i;->b:Lco/uk/getmondo/common/h/a/b;

    .line 19
    return-void
.end method

.method public static a(Lco/uk/getmondo/common/h/a/b;)Lb/a/b;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lco/uk/getmondo/common/h/a/b;",
            ")",
            "Lb/a/b",
            "<",
            "Lio/intercom/android/sdk/push/IntercomPushClient;",
            ">;"
        }
    .end annotation

    .prologue
    .line 29
    new-instance v0, Lco/uk/getmondo/common/h/a/i;

    invoke-direct {v0, p0}, Lco/uk/getmondo/common/h/a/i;-><init>(Lco/uk/getmondo/common/h/a/b;)V

    return-object v0
.end method


# virtual methods
.method public a()Lio/intercom/android/sdk/push/IntercomPushClient;
    .locals 2

    .prologue
    .line 23
    iget-object v0, p0, Lco/uk/getmondo/common/h/a/i;->b:Lco/uk/getmondo/common/h/a/b;

    .line 24
    invoke-virtual {v0}, Lco/uk/getmondo/common/h/a/b;->h()Lio/intercom/android/sdk/push/IntercomPushClient;

    move-result-object v0

    const-string v1, "Cannot return null from a non-@Nullable @Provides method"

    .line 23
    invoke-static {v0, v1}, Lb/a/d;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/intercom/android/sdk/push/IntercomPushClient;

    return-object v0
.end method

.method public synthetic b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 8
    invoke-virtual {p0}, Lco/uk/getmondo/common/h/a/i;->a()Lio/intercom/android/sdk/push/IntercomPushClient;

    move-result-object v0

    return-object v0
.end method
