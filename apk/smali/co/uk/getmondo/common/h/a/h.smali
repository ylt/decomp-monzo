.class public final Lco/uk/getmondo/common/h/a/h;
.super Ljava/lang/Object;
.source "ApplicationModule_ProvideFingerprintManagerCompatFactory.java"

# interfaces
.implements Lb/a/b;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lb/a/b",
        "<",
        "Landroid/support/v4/b/b/a;",
        ">;"
    }
.end annotation


# static fields
.field static final synthetic a:Z


# instance fields
.field private final b:Lco/uk/getmondo/common/h/a/b;

.field private final c:Ljavax/a/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/a/a",
            "<",
            "Landroid/content/Context;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 10
    const-class v0, Lco/uk/getmondo/common/h/a/h;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lco/uk/getmondo/common/h/a/h;->a:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lco/uk/getmondo/common/h/a/b;Ljavax/a/a;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lco/uk/getmondo/common/h/a/b;",
            "Ljavax/a/a",
            "<",
            "Landroid/content/Context;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    sget-boolean v0, Lco/uk/getmondo/common/h/a/h;->a:Z

    if-nez v0, :cond_0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 23
    :cond_0
    iput-object p1, p0, Lco/uk/getmondo/common/h/a/h;->b:Lco/uk/getmondo/common/h/a/b;

    .line 24
    sget-boolean v0, Lco/uk/getmondo/common/h/a/h;->a:Z

    if-nez v0, :cond_1

    if-nez p2, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 25
    :cond_1
    iput-object p2, p0, Lco/uk/getmondo/common/h/a/h;->c:Ljavax/a/a;

    .line 26
    return-void
.end method

.method public static a(Lco/uk/getmondo/common/h/a/b;Ljavax/a/a;)Lb/a/b;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lco/uk/getmondo/common/h/a/b;",
            "Ljavax/a/a",
            "<",
            "Landroid/content/Context;",
            ">;)",
            "Lb/a/b",
            "<",
            "Landroid/support/v4/b/b/a;",
            ">;"
        }
    .end annotation

    .prologue
    .line 37
    new-instance v0, Lco/uk/getmondo/common/h/a/h;

    invoke-direct {v0, p0, p1}, Lco/uk/getmondo/common/h/a/h;-><init>(Lco/uk/getmondo/common/h/a/b;Ljavax/a/a;)V

    return-object v0
.end method


# virtual methods
.method public a()Landroid/support/v4/b/b/a;
    .locals 2

    .prologue
    .line 30
    iget-object v1, p0, Lco/uk/getmondo/common/h/a/h;->b:Lco/uk/getmondo/common/h/a/b;

    iget-object v0, p0, Lco/uk/getmondo/common/h/a/h;->c:Ljavax/a/a;

    .line 31
    invoke-interface {v0}, Ljavax/a/a;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-virtual {v1, v0}, Lco/uk/getmondo/common/h/a/b;->c(Landroid/content/Context;)Landroid/support/v4/b/b/a;

    move-result-object v0

    const-string v1, "Cannot return null from a non-@Nullable @Provides method"

    .line 30
    invoke-static {v0, v1}, Lb/a/d;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/b/b/a;

    return-object v0
.end method

.method public synthetic b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 10
    invoke-virtual {p0}, Lco/uk/getmondo/common/h/a/h;->a()Landroid/support/v4/b/b/a;

    move-result-object v0

    return-object v0
.end method
