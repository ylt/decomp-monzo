.class final Lco/uk/getmondo/common/h/b/a$f;
.super Ljava/lang/Object;
.source "DaggerViewComponent.java"

# interfaces
.implements Lco/uk/getmondo/golden_ticket/c;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lco/uk/getmondo/common/h/b/a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "f"
.end annotation


# instance fields
.field final synthetic a:Lco/uk/getmondo/common/h/b/a;

.field private final b:Lco/uk/getmondo/golden_ticket/d;

.field private c:Ljavax/a/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/a/a",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private d:Ljavax/a/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/a/a",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private e:Ljavax/a/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/golden_ticket/g;",
            ">;"
        }
    .end annotation
.end field

.field private f:Lb/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lb/a",
            "<",
            "Lco/uk/getmondo/golden_ticket/GoldenTicketActivity;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Lco/uk/getmondo/common/h/b/a;Lco/uk/getmondo/golden_ticket/d;)V
    .locals 1

    .prologue
    .line 3938
    iput-object p1, p0, Lco/uk/getmondo/common/h/b/a$f;->a:Lco/uk/getmondo/common/h/b/a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 3939
    invoke-static {p2}, Lb/a/d;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/golden_ticket/d;

    iput-object v0, p0, Lco/uk/getmondo/common/h/b/a$f;->b:Lco/uk/getmondo/golden_ticket/d;

    .line 3940
    invoke-direct {p0}, Lco/uk/getmondo/common/h/b/a$f;->a()V

    .line 3941
    return-void
.end method

.method synthetic constructor <init>(Lco/uk/getmondo/common/h/b/a;Lco/uk/getmondo/golden_ticket/d;Lco/uk/getmondo/common/h/b/a$1;)V
    .locals 0

    .prologue
    .line 3927
    invoke-direct {p0, p1, p2}, Lco/uk/getmondo/common/h/b/a$f;-><init>(Lco/uk/getmondo/common/h/b/a;Lco/uk/getmondo/golden_ticket/d;)V

    return-void
.end method

.method private a()V
    .locals 8

    .prologue
    .line 3946
    iget-object v0, p0, Lco/uk/getmondo/common/h/b/a$f;->b:Lco/uk/getmondo/golden_ticket/d;

    .line 3947
    invoke-static {v0}, Lco/uk/getmondo/golden_ticket/f;->a(Lco/uk/getmondo/golden_ticket/d;)Lb/a/b;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/common/h/b/a$f;->c:Ljavax/a/a;

    .line 3949
    iget-object v0, p0, Lco/uk/getmondo/common/h/b/a$f;->b:Lco/uk/getmondo/golden_ticket/d;

    .line 3950
    invoke-static {v0}, Lco/uk/getmondo/golden_ticket/e;->a(Lco/uk/getmondo/golden_ticket/d;)Lb/a/b;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/common/h/b/a$f;->d:Ljavax/a/a;

    .line 3955
    invoke-static {}, Lb/a/c;->a()Lb/a;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/common/h/b/a$f;->a:Lco/uk/getmondo/common/h/b/a;

    .line 3956
    invoke-static {v1}, Lco/uk/getmondo/common/h/b/a;->b(Lco/uk/getmondo/common/h/b/a;)Ljavax/a/a;

    move-result-object v1

    iget-object v2, p0, Lco/uk/getmondo/common/h/b/a$f;->a:Lco/uk/getmondo/common/h/b/a;

    .line 3957
    invoke-static {v2}, Lco/uk/getmondo/common/h/b/a;->a(Lco/uk/getmondo/common/h/b/a;)Ljavax/a/a;

    move-result-object v2

    iget-object v3, p0, Lco/uk/getmondo/common/h/b/a$f;->c:Ljavax/a/a;

    iget-object v4, p0, Lco/uk/getmondo/common/h/b/a$f;->d:Ljavax/a/a;

    iget-object v5, p0, Lco/uk/getmondo/common/h/b/a$f;->a:Lco/uk/getmondo/common/h/b/a;

    .line 3960
    invoke-static {v5}, Lco/uk/getmondo/common/h/b/a;->n(Lco/uk/getmondo/common/h/b/a;)Ljavax/a/a;

    move-result-object v5

    iget-object v6, p0, Lco/uk/getmondo/common/h/b/a$f;->a:Lco/uk/getmondo/common/h/b/a;

    .line 3961
    invoke-static {v6}, Lco/uk/getmondo/common/h/b/a;->c(Lco/uk/getmondo/common/h/b/a;)Ljavax/a/a;

    move-result-object v6

    iget-object v7, p0, Lco/uk/getmondo/common/h/b/a$f;->a:Lco/uk/getmondo/common/h/b/a;

    .line 3962
    invoke-static {v7}, Lco/uk/getmondo/common/h/b/a;->f(Lco/uk/getmondo/common/h/b/a;)Ljavax/a/a;

    move-result-object v7

    .line 3954
    invoke-static/range {v0 .. v7}, Lco/uk/getmondo/golden_ticket/n;->a(Lb/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;)Lb/a/b;

    move-result-object v0

    .line 3953
    invoke-static {v0}, Lb/a/a;->a(Ljavax/a/a;)Ljavax/a/a;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/common/h/b/a$f;->e:Ljavax/a/a;

    .line 3964
    iget-object v0, p0, Lco/uk/getmondo/common/h/b/a$f;->e:Ljavax/a/a;

    .line 3965
    invoke-static {v0}, Lco/uk/getmondo/golden_ticket/b;->a(Ljavax/a/a;)Lb/a;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/common/h/b/a$f;->f:Lb/a;

    .line 3966
    return-void
.end method


# virtual methods
.method public a(Lco/uk/getmondo/golden_ticket/GoldenTicketActivity;)V
    .locals 1

    .prologue
    .line 3975
    iget-object v0, p0, Lco/uk/getmondo/common/h/b/a$f;->f:Lb/a;

    invoke-interface {v0, p1}, Lb/a;->a(Ljava/lang/Object;)V

    .line 3976
    return-void
.end method
