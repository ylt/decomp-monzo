.class final Lco/uk/getmondo/common/h/b/a$b;
.super Ljava/lang/Object;
.source "DaggerViewComponent.java"

# interfaces
.implements Lco/uk/getmondo/profile/address/b;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lco/uk/getmondo/common/h/b/a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "b"
.end annotation


# instance fields
.field final synthetic a:Lco/uk/getmondo/common/h/b/a;

.field private final b:Lco/uk/getmondo/profile/address/c;

.field private c:Ljavax/a/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/api/model/Address;",
            ">;"
        }
    .end annotation
.end field

.field private d:Ljavax/a/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/profile/address/g;",
            ">;"
        }
    .end annotation
.end field

.field private e:Lb/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lb/a",
            "<",
            "Lco/uk/getmondo/profile/address/EnterAddressActivity;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Lco/uk/getmondo/common/h/b/a;Lco/uk/getmondo/profile/address/c;)V
    .locals 1

    .prologue
    .line 4451
    iput-object p1, p0, Lco/uk/getmondo/common/h/b/a$b;->a:Lco/uk/getmondo/common/h/b/a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 4452
    invoke-static {p2}, Lb/a/d;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/profile/address/c;

    iput-object v0, p0, Lco/uk/getmondo/common/h/b/a$b;->b:Lco/uk/getmondo/profile/address/c;

    .line 4453
    invoke-direct {p0}, Lco/uk/getmondo/common/h/b/a$b;->a()V

    .line 4454
    return-void
.end method

.method synthetic constructor <init>(Lco/uk/getmondo/common/h/b/a;Lco/uk/getmondo/profile/address/c;Lco/uk/getmondo/common/h/b/a$1;)V
    .locals 0

    .prologue
    .line 4442
    invoke-direct {p0, p1, p2}, Lco/uk/getmondo/common/h/b/a$b;-><init>(Lco/uk/getmondo/common/h/b/a;Lco/uk/getmondo/profile/address/c;)V

    return-void
.end method

.method private a()V
    .locals 3

    .prologue
    .line 4459
    iget-object v0, p0, Lco/uk/getmondo/common/h/b/a$b;->b:Lco/uk/getmondo/profile/address/c;

    .line 4460
    invoke-static {v0}, Lco/uk/getmondo/profile/address/d;->a(Lco/uk/getmondo/profile/address/c;)Lb/a/b;

    move-result-object v0

    invoke-static {v0}, Lb/a/a;->a(Ljavax/a/a;)Ljavax/a/a;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/common/h/b/a$b;->c:Ljavax/a/a;

    .line 4465
    invoke-static {}, Lb/a/c;->a()Lb/a;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/common/h/b/a$b;->c:Ljavax/a/a;

    iget-object v2, p0, Lco/uk/getmondo/common/h/b/a$b;->a:Lco/uk/getmondo/common/h/b/a;

    .line 4467
    invoke-static {v2}, Lco/uk/getmondo/common/h/b/a;->f(Lco/uk/getmondo/common/h/b/a;)Ljavax/a/a;

    move-result-object v2

    .line 4464
    invoke-static {v0, v1, v2}, Lco/uk/getmondo/profile/address/i;->a(Lb/a;Ljavax/a/a;Ljavax/a/a;)Lb/a/b;

    move-result-object v0

    .line 4463
    invoke-static {v0}, Lb/a/a;->a(Ljavax/a/a;)Ljavax/a/a;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/common/h/b/a$b;->d:Ljavax/a/a;

    .line 4469
    iget-object v0, p0, Lco/uk/getmondo/common/h/b/a$b;->d:Ljavax/a/a;

    .line 4470
    invoke-static {v0}, Lco/uk/getmondo/profile/address/f;->a(Ljavax/a/a;)Lb/a;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/common/h/b/a$b;->e:Lb/a;

    .line 4471
    return-void
.end method


# virtual methods
.method public a(Lco/uk/getmondo/profile/address/EnterAddressActivity;)V
    .locals 1

    .prologue
    .line 4475
    iget-object v0, p0, Lco/uk/getmondo/common/h/b/a$b;->e:Lb/a;

    invoke-interface {v0, p1}, Lb/a;->a(Ljava/lang/Object;)V

    .line 4476
    return-void
.end method
