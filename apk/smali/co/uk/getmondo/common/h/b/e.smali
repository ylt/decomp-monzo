.class public final Lco/uk/getmondo/common/h/b/e;
.super Ljava/lang/Object;
.source "ViewModule_ProvideStripeFactory.java"

# interfaces
.implements Lb/a/b;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lb/a/b",
        "<",
        "Lcom/stripe/android/Stripe;",
        ">;"
    }
.end annotation


# static fields
.field static final synthetic a:Z


# instance fields
.field private final b:Lco/uk/getmondo/common/h/b/c;

.field private final c:Ljavax/a/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/a/a",
            "<",
            "Landroid/content/Context;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 10
    const-class v0, Lco/uk/getmondo/common/h/b/e;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lco/uk/getmondo/common/h/b/e;->a:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lco/uk/getmondo/common/h/b/c;Ljavax/a/a;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lco/uk/getmondo/common/h/b/c;",
            "Ljavax/a/a",
            "<",
            "Landroid/content/Context;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    sget-boolean v0, Lco/uk/getmondo/common/h/b/e;->a:Z

    if-nez v0, :cond_0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 21
    :cond_0
    iput-object p1, p0, Lco/uk/getmondo/common/h/b/e;->b:Lco/uk/getmondo/common/h/b/c;

    .line 22
    sget-boolean v0, Lco/uk/getmondo/common/h/b/e;->a:Z

    if-nez v0, :cond_1

    if-nez p2, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 23
    :cond_1
    iput-object p2, p0, Lco/uk/getmondo/common/h/b/e;->c:Ljavax/a/a;

    .line 24
    return-void
.end method

.method public static a(Lco/uk/getmondo/common/h/b/c;Ljavax/a/a;)Lb/a/b;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lco/uk/getmondo/common/h/b/c;",
            "Ljavax/a/a",
            "<",
            "Landroid/content/Context;",
            ">;)",
            "Lb/a/b",
            "<",
            "Lcom/stripe/android/Stripe;",
            ">;"
        }
    .end annotation

    .prologue
    .line 34
    new-instance v0, Lco/uk/getmondo/common/h/b/e;

    invoke-direct {v0, p0, p1}, Lco/uk/getmondo/common/h/b/e;-><init>(Lco/uk/getmondo/common/h/b/c;Ljavax/a/a;)V

    return-object v0
.end method


# virtual methods
.method public a()Lcom/stripe/android/Stripe;
    .locals 2

    .prologue
    .line 28
    iget-object v1, p0, Lco/uk/getmondo/common/h/b/e;->b:Lco/uk/getmondo/common/h/b/c;

    iget-object v0, p0, Lco/uk/getmondo/common/h/b/e;->c:Ljavax/a/a;

    .line 29
    invoke-interface {v0}, Ljavax/a/a;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-virtual {v1, v0}, Lco/uk/getmondo/common/h/b/c;->a(Landroid/content/Context;)Lcom/stripe/android/Stripe;

    move-result-object v0

    const-string v1, "Cannot return null from a non-@Nullable @Provides method"

    .line 28
    invoke-static {v0, v1}, Lb/a/d;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/stripe/android/Stripe;

    return-object v0
.end method

.method public synthetic b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 10
    invoke-virtual {p0}, Lco/uk/getmondo/common/h/b/e;->a()Lcom/stripe/android/Stripe;

    move-result-object v0

    return-object v0
.end method
