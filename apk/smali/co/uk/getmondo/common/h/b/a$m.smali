.class final Lco/uk/getmondo/common/h/b/a$m;
.super Ljava/lang/Object;
.source "DaggerViewComponent.java"

# interfaces
.implements Lco/uk/getmondo/payments/send/authentication/e;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lco/uk/getmondo/common/h/b/a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "m"
.end annotation


# instance fields
.field final synthetic a:Lco/uk/getmondo/common/h/b/a;

.field private final b:Lco/uk/getmondo/payments/send/authentication/f;

.field private c:Ljavax/a/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/payments/send/data/a/f;",
            ">;"
        }
    .end annotation
.end field

.field private d:Ljavax/a/a;

.field private e:Ljavax/a/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/payments/send/a;",
            ">;"
        }
    .end annotation
.end field

.field private f:Lb/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lb/a",
            "<",
            "Lco/uk/getmondo/payments/send/authentication/PaymentAuthenticationActivity;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Lco/uk/getmondo/common/h/b/a;Lco/uk/getmondo/payments/send/authentication/f;)V
    .locals 1

    .prologue
    .line 3506
    iput-object p1, p0, Lco/uk/getmondo/common/h/b/a$m;->a:Lco/uk/getmondo/common/h/b/a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 3507
    invoke-static {p2}, Lb/a/d;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/payments/send/authentication/f;

    iput-object v0, p0, Lco/uk/getmondo/common/h/b/a$m;->b:Lco/uk/getmondo/payments/send/authentication/f;

    .line 3508
    invoke-direct {p0}, Lco/uk/getmondo/common/h/b/a$m;->a()V

    .line 3509
    return-void
.end method

.method synthetic constructor <init>(Lco/uk/getmondo/common/h/b/a;Lco/uk/getmondo/payments/send/authentication/f;Lco/uk/getmondo/common/h/b/a$1;)V
    .locals 0

    .prologue
    .line 3492
    invoke-direct {p0, p1, p2}, Lco/uk/getmondo/common/h/b/a$m;-><init>(Lco/uk/getmondo/common/h/b/a;Lco/uk/getmondo/payments/send/authentication/f;)V

    return-void
.end method

.method private a()V
    .locals 8

    .prologue
    .line 3514
    iget-object v0, p0, Lco/uk/getmondo/common/h/b/a$m;->b:Lco/uk/getmondo/payments/send/authentication/f;

    .line 3515
    invoke-static {v0}, Lco/uk/getmondo/payments/send/authentication/g;->a(Lco/uk/getmondo/payments/send/authentication/f;)Lb/a/b;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/common/h/b/a$m;->c:Ljavax/a/a;

    .line 3520
    invoke-static {}, Lb/a/c;->a()Lb/a;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/common/h/b/a$m;->a:Lco/uk/getmondo/common/h/b/a;

    .line 3521
    invoke-static {v1}, Lco/uk/getmondo/common/h/b/a;->a(Lco/uk/getmondo/common/h/b/a;)Ljavax/a/a;

    move-result-object v1

    iget-object v2, p0, Lco/uk/getmondo/common/h/b/a$m;->a:Lco/uk/getmondo/common/h/b/a;

    .line 3522
    invoke-static {v2}, Lco/uk/getmondo/common/h/b/a;->b(Lco/uk/getmondo/common/h/b/a;)Ljavax/a/a;

    move-result-object v2

    iget-object v3, p0, Lco/uk/getmondo/common/h/b/a$m;->a:Lco/uk/getmondo/common/h/b/a;

    .line 3523
    invoke-static {v3}, Lco/uk/getmondo/common/h/b/a;->c(Lco/uk/getmondo/common/h/b/a;)Ljavax/a/a;

    move-result-object v3

    iget-object v4, p0, Lco/uk/getmondo/common/h/b/a$m;->a:Lco/uk/getmondo/common/h/b/a;

    .line 3524
    invoke-static {v4}, Lco/uk/getmondo/common/h/b/a;->g(Lco/uk/getmondo/common/h/b/a;)Ljavax/a/a;

    move-result-object v4

    iget-object v5, p0, Lco/uk/getmondo/common/h/b/a$m;->a:Lco/uk/getmondo/common/h/b/a;

    .line 3525
    invoke-static {v5}, Lco/uk/getmondo/common/h/b/a;->f(Lco/uk/getmondo/common/h/b/a;)Ljavax/a/a;

    move-result-object v5

    iget-object v6, p0, Lco/uk/getmondo/common/h/b/a$m;->c:Ljavax/a/a;

    iget-object v7, p0, Lco/uk/getmondo/common/h/b/a$m;->a:Lco/uk/getmondo/common/h/b/a;

    .line 3527
    invoke-static {v7}, Lco/uk/getmondo/common/h/b/a;->h(Lco/uk/getmondo/common/h/b/a;)Ljavax/a/a;

    move-result-object v7

    .line 3519
    invoke-static/range {v0 .. v7}, Lco/uk/getmondo/payments/send/authentication/o;->a(Lb/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;)Lb/a/b;

    move-result-object v0

    .line 3518
    invoke-static {v0}, Lb/a/a;->a(Ljavax/a/a;)Ljavax/a/a;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/common/h/b/a$m;->d:Ljavax/a/a;

    .line 3529
    iget-object v0, p0, Lco/uk/getmondo/common/h/b/a$m;->a:Lco/uk/getmondo/common/h/b/a;

    .line 3532
    invoke-static {v0}, Lco/uk/getmondo/common/h/b/a;->i(Lco/uk/getmondo/common/h/b/a;)Ljavax/a/a;

    move-result-object v0

    .line 3531
    invoke-static {v0}, Lco/uk/getmondo/payments/send/c;->a(Ljavax/a/a;)Lb/a/b;

    move-result-object v0

    .line 3530
    invoke-static {v0}, Lb/a/a;->a(Ljavax/a/a;)Ljavax/a/a;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/common/h/b/a$m;->e:Ljavax/a/a;

    .line 3534
    iget-object v0, p0, Lco/uk/getmondo/common/h/b/a$m;->d:Ljavax/a/a;

    iget-object v1, p0, Lco/uk/getmondo/common/h/b/a$m;->e:Ljavax/a/a;

    .line 3535
    invoke-static {v0, v1}, Lco/uk/getmondo/payments/send/authentication/d;->a(Ljavax/a/a;Ljavax/a/a;)Lb/a;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/common/h/b/a$m;->f:Lb/a;

    .line 3537
    return-void
.end method


# virtual methods
.method public a(Lco/uk/getmondo/payments/send/authentication/PaymentAuthenticationActivity;)V
    .locals 1

    .prologue
    .line 3541
    iget-object v0, p0, Lco/uk/getmondo/common/h/b/a$m;->f:Lb/a;

    invoke-interface {v0, p1}, Lb/a;->a(Ljava/lang/Object;)V

    .line 3542
    return-void
.end method
