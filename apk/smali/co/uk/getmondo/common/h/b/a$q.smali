.class final Lco/uk/getmondo/common/h/b/a$q;
.super Ljava/lang/Object;
.source "DaggerViewComponent.java"

# interfaces
.implements Lco/uk/getmondo/payments/recurring_cancellation/e;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lco/uk/getmondo/common/h/b/a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "q"
.end annotation


# instance fields
.field final synthetic a:Lco/uk/getmondo/common/h/b/a;

.field private final b:Lco/uk/getmondo/payments/recurring_cancellation/f;

.field private c:Ljavax/a/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/payments/a/a/f;",
            ">;"
        }
    .end annotation
.end field

.field private d:Ljavax/a/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/payments/recurring_cancellation/b;",
            ">;"
        }
    .end annotation
.end field

.field private e:Lb/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lb/a",
            "<",
            "Lco/uk/getmondo/payments/recurring_cancellation/RecurringPaymentCancelActivity;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Lco/uk/getmondo/common/h/b/a;Lco/uk/getmondo/payments/recurring_cancellation/f;)V
    .locals 1

    .prologue
    .line 4667
    iput-object p1, p0, Lco/uk/getmondo/common/h/b/a$q;->a:Lco/uk/getmondo/common/h/b/a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 4668
    invoke-static {p2}, Lb/a/d;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/payments/recurring_cancellation/f;

    iput-object v0, p0, Lco/uk/getmondo/common/h/b/a$q;->b:Lco/uk/getmondo/payments/recurring_cancellation/f;

    .line 4669
    invoke-direct {p0}, Lco/uk/getmondo/common/h/b/a$q;->a()V

    .line 4670
    return-void
.end method

.method synthetic constructor <init>(Lco/uk/getmondo/common/h/b/a;Lco/uk/getmondo/payments/recurring_cancellation/f;Lco/uk/getmondo/common/h/b/a$1;)V
    .locals 0

    .prologue
    .line 4657
    invoke-direct {p0, p1, p2}, Lco/uk/getmondo/common/h/b/a$q;-><init>(Lco/uk/getmondo/common/h/b/a;Lco/uk/getmondo/payments/recurring_cancellation/f;)V

    return-void
.end method

.method private a()V
    .locals 6

    .prologue
    .line 4675
    iget-object v0, p0, Lco/uk/getmondo/common/h/b/a$q;->b:Lco/uk/getmondo/payments/recurring_cancellation/f;

    .line 4676
    invoke-static {v0}, Lco/uk/getmondo/payments/recurring_cancellation/g;->a(Lco/uk/getmondo/payments/recurring_cancellation/f;)Lb/a/b;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/common/h/b/a$q;->c:Ljavax/a/a;

    .line 4681
    invoke-static {}, Lb/a/c;->a()Lb/a;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/common/h/b/a$q;->a:Lco/uk/getmondo/common/h/b/a;

    .line 4682
    invoke-static {v1}, Lco/uk/getmondo/common/h/b/a;->b(Lco/uk/getmondo/common/h/b/a;)Ljavax/a/a;

    move-result-object v1

    iget-object v2, p0, Lco/uk/getmondo/common/h/b/a$q;->a:Lco/uk/getmondo/common/h/b/a;

    .line 4683
    invoke-static {v2}, Lco/uk/getmondo/common/h/b/a;->a(Lco/uk/getmondo/common/h/b/a;)Ljavax/a/a;

    move-result-object v2

    iget-object v3, p0, Lco/uk/getmondo/common/h/b/a$q;->a:Lco/uk/getmondo/common/h/b/a;

    .line 4684
    invoke-static {v3}, Lco/uk/getmondo/common/h/b/a;->D(Lco/uk/getmondo/common/h/b/a;)Ljavax/a/a;

    move-result-object v3

    iget-object v4, p0, Lco/uk/getmondo/common/h/b/a$q;->a:Lco/uk/getmondo/common/h/b/a;

    .line 4685
    invoke-static {v4}, Lco/uk/getmondo/common/h/b/a;->c(Lco/uk/getmondo/common/h/b/a;)Ljavax/a/a;

    move-result-object v4

    iget-object v5, p0, Lco/uk/getmondo/common/h/b/a$q;->c:Ljavax/a/a;

    .line 4680
    invoke-static/range {v0 .. v5}, Lco/uk/getmondo/payments/recurring_cancellation/d;->a(Lb/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;)Lb/a/b;

    move-result-object v0

    .line 4679
    invoke-static {v0}, Lb/a/a;->a(Ljavax/a/a;)Ljavax/a/a;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/common/h/b/a$q;->d:Ljavax/a/a;

    .line 4688
    iget-object v0, p0, Lco/uk/getmondo/common/h/b/a$q;->d:Ljavax/a/a;

    .line 4689
    invoke-static {v0}, Lco/uk/getmondo/payments/recurring_cancellation/a;->a(Ljavax/a/a;)Lb/a;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/common/h/b/a$q;->e:Lb/a;

    .line 4691
    return-void
.end method


# virtual methods
.method public a(Lco/uk/getmondo/payments/recurring_cancellation/RecurringPaymentCancelActivity;)V
    .locals 1

    .prologue
    .line 4695
    iget-object v0, p0, Lco/uk/getmondo/common/h/b/a$q;->e:Lb/a;

    invoke-interface {v0, p1}, Lb/a;->a(Ljava/lang/Object;)V

    .line 4696
    return-void
.end method
