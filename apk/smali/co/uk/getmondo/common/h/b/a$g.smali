.class final Lco/uk/getmondo/common/h/b/a$g;
.super Ljava/lang/Object;
.source "DaggerViewComponent.java"

# interfaces
.implements Lco/uk/getmondo/help/b/a;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lco/uk/getmondo/common/h/b/a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "g"
.end annotation


# instance fields
.field final synthetic a:Lco/uk/getmondo/common/h/b/a;

.field private final b:Lco/uk/getmondo/help/b/b;

.field private c:Ljavax/a/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/a/a",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private d:Ljavax/a/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/a/a",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private e:Ljavax/a/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/help/c;",
            ">;"
        }
    .end annotation
.end field

.field private f:Lb/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lb/a",
            "<",
            "Lco/uk/getmondo/help/HelpCategoryActivity;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Lco/uk/getmondo/common/h/b/a;Lco/uk/getmondo/help/b/b;)V
    .locals 1

    .prologue
    .line 4531
    iput-object p1, p0, Lco/uk/getmondo/common/h/b/a$g;->a:Lco/uk/getmondo/common/h/b/a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 4532
    invoke-static {p2}, Lb/a/d;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/help/b/b;

    iput-object v0, p0, Lco/uk/getmondo/common/h/b/a$g;->b:Lco/uk/getmondo/help/b/b;

    .line 4533
    invoke-direct {p0}, Lco/uk/getmondo/common/h/b/a$g;->a()V

    .line 4534
    return-void
.end method

.method synthetic constructor <init>(Lco/uk/getmondo/common/h/b/a;Lco/uk/getmondo/help/b/b;Lco/uk/getmondo/common/h/b/a$1;)V
    .locals 0

    .prologue
    .line 4520
    invoke-direct {p0, p1, p2}, Lco/uk/getmondo/common/h/b/a$g;-><init>(Lco/uk/getmondo/common/h/b/a;Lco/uk/getmondo/help/b/b;)V

    return-void
.end method

.method private a()V
    .locals 8

    .prologue
    .line 4539
    iget-object v0, p0, Lco/uk/getmondo/common/h/b/a$g;->b:Lco/uk/getmondo/help/b/b;

    .line 4541
    invoke-static {v0}, Lco/uk/getmondo/help/b/c;->a(Lco/uk/getmondo/help/b/b;)Lb/a/b;

    move-result-object v0

    .line 4540
    invoke-static {v0}, Lb/a/a;->a(Ljavax/a/a;)Ljavax/a/a;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/common/h/b/a$g;->c:Ljavax/a/a;

    .line 4543
    iget-object v0, p0, Lco/uk/getmondo/common/h/b/a$g;->b:Lco/uk/getmondo/help/b/b;

    iget-object v1, p0, Lco/uk/getmondo/common/h/b/a$g;->a:Lco/uk/getmondo/common/h/b/a;

    .line 4546
    invoke-static {v1}, Lco/uk/getmondo/common/h/b/a;->i(Lco/uk/getmondo/common/h/b/a;)Ljavax/a/a;

    move-result-object v1

    .line 4545
    invoke-static {v0, v1}, Lco/uk/getmondo/help/b/d;->a(Lco/uk/getmondo/help/b/b;Ljavax/a/a;)Lb/a/b;

    move-result-object v0

    .line 4544
    invoke-static {v0}, Lb/a/a;->a(Ljavax/a/a;)Ljavax/a/a;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/common/h/b/a$g;->d:Ljavax/a/a;

    .line 4551
    invoke-static {}, Lb/a/c;->a()Lb/a;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/common/h/b/a$g;->a:Lco/uk/getmondo/common/h/b/a;

    .line 4552
    invoke-static {v1}, Lco/uk/getmondo/common/h/b/a;->b(Lco/uk/getmondo/common/h/b/a;)Ljavax/a/a;

    move-result-object v1

    iget-object v2, p0, Lco/uk/getmondo/common/h/b/a$g;->a:Lco/uk/getmondo/common/h/b/a;

    .line 4553
    invoke-static {v2}, Lco/uk/getmondo/common/h/b/a;->a(Lco/uk/getmondo/common/h/b/a;)Ljavax/a/a;

    move-result-object v2

    iget-object v3, p0, Lco/uk/getmondo/common/h/b/a$g;->a:Lco/uk/getmondo/common/h/b/a;

    .line 4554
    invoke-static {v3}, Lco/uk/getmondo/common/h/b/a;->C(Lco/uk/getmondo/common/h/b/a;)Ljavax/a/a;

    move-result-object v3

    iget-object v4, p0, Lco/uk/getmondo/common/h/b/a$g;->a:Lco/uk/getmondo/common/h/b/a;

    .line 4555
    invoke-static {v4}, Lco/uk/getmondo/common/h/b/a;->c(Lco/uk/getmondo/common/h/b/a;)Ljavax/a/a;

    move-result-object v4

    iget-object v5, p0, Lco/uk/getmondo/common/h/b/a$g;->c:Ljavax/a/a;

    iget-object v6, p0, Lco/uk/getmondo/common/h/b/a$g;->d:Ljavax/a/a;

    iget-object v7, p0, Lco/uk/getmondo/common/h/b/a$g;->a:Lco/uk/getmondo/common/h/b/a;

    .line 4558
    invoke-static {v7}, Lco/uk/getmondo/common/h/b/a;->f(Lco/uk/getmondo/common/h/b/a;)Ljavax/a/a;

    move-result-object v7

    .line 4550
    invoke-static/range {v0 .. v7}, Lco/uk/getmondo/help/e;->a(Lb/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;)Lb/a/b;

    move-result-object v0

    .line 4549
    invoke-static {v0}, Lb/a/a;->a(Ljavax/a/a;)Ljavax/a/a;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/common/h/b/a$g;->e:Ljavax/a/a;

    .line 4560
    iget-object v0, p0, Lco/uk/getmondo/common/h/b/a$g;->e:Ljavax/a/a;

    iget-object v1, p0, Lco/uk/getmondo/common/h/b/a$g;->a:Lco/uk/getmondo/common/h/b/a;

    .line 4562
    invoke-static {v1}, Lco/uk/getmondo/common/h/b/a;->A(Lco/uk/getmondo/common/h/b/a;)Ljavax/a/a;

    move-result-object v1

    .line 4561
    invoke-static {v0, v1}, Lco/uk/getmondo/help/b;->a(Ljavax/a/a;Ljavax/a/a;)Lb/a;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/common/h/b/a$g;->f:Lb/a;

    .line 4563
    return-void
.end method


# virtual methods
.method public a(Lco/uk/getmondo/help/HelpCategoryActivity;)V
    .locals 1

    .prologue
    .line 4567
    iget-object v0, p0, Lco/uk/getmondo/common/h/b/a$g;->f:Lb/a;

    invoke-interface {v0, p1}, Lb/a;->a(Ljava/lang/Object;)V

    .line 4568
    return-void
.end method
