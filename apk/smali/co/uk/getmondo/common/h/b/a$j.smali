.class final Lco/uk/getmondo/common/h/b/a$j;
.super Ljava/lang/Object;
.source "DaggerViewComponent.java"

# interfaces
.implements Lco/uk/getmondo/signup/identity_verification/sdd/b;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lco/uk/getmondo/common/h/b/a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "j"
.end annotation


# instance fields
.field final synthetic a:Lco/uk/getmondo/common/h/b/a;

.field private final b:Lco/uk/getmondo/signup/identity_verification/sdd/c;

.field private c:Ljavax/a/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/signup/identity_verification/sdd/j;",
            ">;"
        }
    .end annotation
.end field

.field private d:Ljavax/a/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/a/a",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private e:Ljavax/a/a;

.field private f:Lb/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lb/a",
            "<",
            "Lco/uk/getmondo/signup/identity_verification/sdd/IdentityVerificationSddActivity;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Lco/uk/getmondo/common/h/b/a;Lco/uk/getmondo/signup/identity_verification/sdd/c;)V
    .locals 1

    .prologue
    .line 4406
    iput-object p1, p0, Lco/uk/getmondo/common/h/b/a$j;->a:Lco/uk/getmondo/common/h/b/a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 4408
    invoke-static {p2}, Lb/a/d;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/signup/identity_verification/sdd/c;

    iput-object v0, p0, Lco/uk/getmondo/common/h/b/a$j;->b:Lco/uk/getmondo/signup/identity_verification/sdd/c;

    .line 4409
    invoke-direct {p0}, Lco/uk/getmondo/common/h/b/a$j;->a()V

    .line 4410
    return-void
.end method

.method synthetic constructor <init>(Lco/uk/getmondo/common/h/b/a;Lco/uk/getmondo/signup/identity_verification/sdd/c;Lco/uk/getmondo/common/h/b/a$1;)V
    .locals 0

    .prologue
    .line 4391
    invoke-direct {p0, p1, p2}, Lco/uk/getmondo/common/h/b/a$j;-><init>(Lco/uk/getmondo/common/h/b/a;Lco/uk/getmondo/signup/identity_verification/sdd/c;)V

    return-void
.end method

.method private a()V
    .locals 4

    .prologue
    .line 4415
    iget-object v0, p0, Lco/uk/getmondo/common/h/b/a$j;->b:Lco/uk/getmondo/signup/identity_verification/sdd/c;

    .line 4416
    invoke-static {v0}, Lco/uk/getmondo/signup/identity_verification/sdd/e;->a(Lco/uk/getmondo/signup/identity_verification/sdd/c;)Lb/a/b;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/common/h/b/a$j;->c:Ljavax/a/a;

    .line 4419
    iget-object v0, p0, Lco/uk/getmondo/common/h/b/a$j;->b:Lco/uk/getmondo/signup/identity_verification/sdd/c;

    .line 4420
    invoke-static {v0}, Lco/uk/getmondo/signup/identity_verification/sdd/d;->a(Lco/uk/getmondo/signup/identity_verification/sdd/c;)Lb/a/b;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/common/h/b/a$j;->d:Ljavax/a/a;

    .line 4426
    invoke-static {}, Lb/a/c;->a()Lb/a;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/common/h/b/a$j;->c:Ljavax/a/a;

    iget-object v2, p0, Lco/uk/getmondo/common/h/b/a$j;->a:Lco/uk/getmondo/common/h/b/a;

    .line 4428
    invoke-static {v2}, Lco/uk/getmondo/common/h/b/a;->f(Lco/uk/getmondo/common/h/b/a;)Ljavax/a/a;

    move-result-object v2

    iget-object v3, p0, Lco/uk/getmondo/common/h/b/a$j;->d:Ljavax/a/a;

    .line 4425
    invoke-static {v0, v1, v2, v3}, Lco/uk/getmondo/signup/identity_verification/sdd/i;->a(Lb/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;)Lb/a/b;

    move-result-object v0

    .line 4424
    invoke-static {v0}, Lb/a/a;->a(Ljavax/a/a;)Ljavax/a/a;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/common/h/b/a$j;->e:Ljavax/a/a;

    .line 4431
    iget-object v0, p0, Lco/uk/getmondo/common/h/b/a$j;->e:Ljavax/a/a;

    .line 4432
    invoke-static {v0}, Lco/uk/getmondo/signup/identity_verification/sdd/a;->a(Ljavax/a/a;)Lb/a;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/common/h/b/a$j;->f:Lb/a;

    .line 4434
    return-void
.end method


# virtual methods
.method public a(Lco/uk/getmondo/signup/identity_verification/sdd/IdentityVerificationSddActivity;)V
    .locals 1

    .prologue
    .line 4438
    iget-object v0, p0, Lco/uk/getmondo/common/h/b/a$j;->f:Lb/a;

    invoke-interface {v0, p1}, Lb/a;->a(Ljava/lang/Object;)V

    .line 4439
    return-void
.end method
