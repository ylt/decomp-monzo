.class final Lco/uk/getmondo/common/h/b/a$c;
.super Ljava/lang/Object;
.source "DaggerViewComponent.java"

# interfaces
.implements Lco/uk/getmondo/payments/send/bank/payment/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lco/uk/getmondo/common/h/b/a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "c"
.end annotation


# instance fields
.field final synthetic a:Lco/uk/getmondo/common/h/b/a;

.field private final b:Lco/uk/getmondo/payments/send/bank/payment/i;

.field private c:Ljavax/a/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/payments/send/data/a/b;",
            ">;"
        }
    .end annotation
.end field

.field private d:Ljavax/a/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/payments/send/bank/payment/k;",
            ">;"
        }
    .end annotation
.end field

.field private e:Lb/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lb/a",
            "<",
            "Lco/uk/getmondo/payments/send/bank/payment/BankPaymentDetailsActivity;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Lco/uk/getmondo/common/h/b/a;Lco/uk/getmondo/payments/send/bank/payment/i;)V
    .locals 1

    .prologue
    .line 4619
    iput-object p1, p0, Lco/uk/getmondo/common/h/b/a$c;->a:Lco/uk/getmondo/common/h/b/a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 4620
    invoke-static {p2}, Lb/a/d;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/payments/send/bank/payment/i;

    iput-object v0, p0, Lco/uk/getmondo/common/h/b/a$c;->b:Lco/uk/getmondo/payments/send/bank/payment/i;

    .line 4621
    invoke-direct {p0}, Lco/uk/getmondo/common/h/b/a$c;->a()V

    .line 4622
    return-void
.end method

.method synthetic constructor <init>(Lco/uk/getmondo/common/h/b/a;Lco/uk/getmondo/payments/send/bank/payment/i;Lco/uk/getmondo/common/h/b/a$1;)V
    .locals 0

    .prologue
    .line 4610
    invoke-direct {p0, p1, p2}, Lco/uk/getmondo/common/h/b/a$c;-><init>(Lco/uk/getmondo/common/h/b/a;Lco/uk/getmondo/payments/send/bank/payment/i;)V

    return-void
.end method

.method private a()V
    .locals 7

    .prologue
    .line 4627
    iget-object v0, p0, Lco/uk/getmondo/common/h/b/a$c;->b:Lco/uk/getmondo/payments/send/bank/payment/i;

    .line 4628
    invoke-static {v0}, Lco/uk/getmondo/payments/send/bank/payment/j;->a(Lco/uk/getmondo/payments/send/bank/payment/i;)Lb/a/b;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/common/h/b/a$c;->c:Ljavax/a/a;

    .line 4634
    invoke-static {}, Lb/a/c;->a()Lb/a;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/common/h/b/a$c;->a:Lco/uk/getmondo/common/h/b/a;

    .line 4635
    invoke-static {v1}, Lco/uk/getmondo/common/h/b/a;->a(Lco/uk/getmondo/common/h/b/a;)Ljavax/a/a;

    move-result-object v1

    iget-object v2, p0, Lco/uk/getmondo/common/h/b/a$c;->a:Lco/uk/getmondo/common/h/b/a;

    .line 4636
    invoke-static {v2}, Lco/uk/getmondo/common/h/b/a;->b(Lco/uk/getmondo/common/h/b/a;)Ljavax/a/a;

    move-result-object v2

    iget-object v3, p0, Lco/uk/getmondo/common/h/b/a$c;->a:Lco/uk/getmondo/common/h/b/a;

    .line 4637
    invoke-static {v3}, Lco/uk/getmondo/common/h/b/a;->e(Lco/uk/getmondo/common/h/b/a;)Ljavax/a/a;

    move-result-object v3

    iget-object v4, p0, Lco/uk/getmondo/common/h/b/a$c;->a:Lco/uk/getmondo/common/h/b/a;

    .line 4638
    invoke-static {v4}, Lco/uk/getmondo/common/h/b/a;->d(Lco/uk/getmondo/common/h/b/a;)Ljavax/a/a;

    move-result-object v4

    iget-object v5, p0, Lco/uk/getmondo/common/h/b/a$c;->a:Lco/uk/getmondo/common/h/b/a;

    .line 4639
    invoke-static {v5}, Lco/uk/getmondo/common/h/b/a;->c(Lco/uk/getmondo/common/h/b/a;)Ljavax/a/a;

    move-result-object v5

    iget-object v6, p0, Lco/uk/getmondo/common/h/b/a$c;->c:Ljavax/a/a;

    .line 4633
    invoke-static/range {v0 .. v6}, Lco/uk/getmondo/payments/send/bank/payment/l;->a(Lb/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;)Lb/a/b;

    move-result-object v0

    .line 4632
    invoke-static {v0}, Lb/a/a;->a(Ljavax/a/a;)Ljavax/a/a;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/common/h/b/a$c;->d:Ljavax/a/a;

    .line 4642
    iget-object v0, p0, Lco/uk/getmondo/common/h/b/a$c;->d:Ljavax/a/a;

    .line 4643
    invoke-static {v0}, Lco/uk/getmondo/payments/send/bank/payment/g;->a(Ljavax/a/a;)Lb/a;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/common/h/b/a$c;->e:Lb/a;

    .line 4644
    return-void
.end method


# virtual methods
.method public a(Lco/uk/getmondo/payments/send/bank/payment/BankPaymentDetailsActivity;)V
    .locals 1

    .prologue
    .line 4653
    iget-object v0, p0, Lco/uk/getmondo/common/h/b/a$c;->e:Lb/a;

    invoke-interface {v0, p1}, Lb/a;->a(Ljava/lang/Object;)V

    .line 4654
    return-void
.end method
