.class final Lco/uk/getmondo/common/h/b/a$i$b;
.super Ljava/lang/Object;
.source "DaggerViewComponent.java"

# interfaces
.implements Lco/uk/getmondo/signup/identity_verification/i;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lco/uk/getmondo/common/h/b/a$i;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "b"
.end annotation


# instance fields
.field final synthetic a:Lco/uk/getmondo/common/h/b/a$i;

.field private final b:Lco/uk/getmondo/signup/identity_verification/j;

.field private c:Ljavax/a/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/signup/identity_verification/v;",
            ">;"
        }
    .end annotation
.end field

.field private d:Lb/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lb/a",
            "<",
            "Lco/uk/getmondo/signup/identity_verification/IdentityVerificationActivity;",
            ">;"
        }
    .end annotation
.end field

.field private e:Ljavax/a/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/signup/identity_verification/a/c;",
            ">;"
        }
    .end annotation
.end field

.field private f:Ljavax/a/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/api/model/tracking/Impression$KycFrom;",
            ">;"
        }
    .end annotation
.end field

.field private g:Ljavax/a/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/a/a",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private h:Ljavax/a/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/a/a",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private i:Ljavax/a/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/signup/identity_verification/g;",
            ">;"
        }
    .end annotation
.end field

.field private j:Lb/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lb/a",
            "<",
            "Lco/uk/getmondo/signup/identity_verification/e;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Lco/uk/getmondo/common/h/b/a$i;Lco/uk/getmondo/signup/identity_verification/j;)V
    .locals 1

    .prologue
    .line 4278
    iput-object p1, p0, Lco/uk/getmondo/common/h/b/a$i$b;->a:Lco/uk/getmondo/common/h/b/a$i;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 4280
    invoke-static {p2}, Lb/a/d;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/signup/identity_verification/j;

    iput-object v0, p0, Lco/uk/getmondo/common/h/b/a$i$b;->b:Lco/uk/getmondo/signup/identity_verification/j;

    .line 4281
    invoke-direct {p0}, Lco/uk/getmondo/common/h/b/a$i$b;->a()V

    .line 4282
    return-void
.end method

.method synthetic constructor <init>(Lco/uk/getmondo/common/h/b/a$i;Lco/uk/getmondo/signup/identity_verification/j;Lco/uk/getmondo/common/h/b/a$1;)V
    .locals 0

    .prologue
    .line 4256
    invoke-direct {p0, p1, p2}, Lco/uk/getmondo/common/h/b/a$i$b;-><init>(Lco/uk/getmondo/common/h/b/a$i;Lco/uk/getmondo/signup/identity_verification/j;)V

    return-void
.end method

.method private a()V
    .locals 11

    .prologue
    .line 4287
    .line 4290
    invoke-static {}, Lb/a/c;->a()Lb/a;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/common/h/b/a$i$b;->a:Lco/uk/getmondo/common/h/b/a$i;

    iget-object v1, v1, Lco/uk/getmondo/common/h/b/a$i;->a:Lco/uk/getmondo/common/h/b/a;

    .line 4291
    invoke-static {v1}, Lco/uk/getmondo/common/h/b/a;->b(Lco/uk/getmondo/common/h/b/a;)Ljavax/a/a;

    move-result-object v1

    iget-object v2, p0, Lco/uk/getmondo/common/h/b/a$i$b;->a:Lco/uk/getmondo/common/h/b/a$i;

    iget-object v2, v2, Lco/uk/getmondo/common/h/b/a$i;->a:Lco/uk/getmondo/common/h/b/a;

    .line 4292
    invoke-static {v2}, Lco/uk/getmondo/common/h/b/a;->a(Lco/uk/getmondo/common/h/b/a;)Ljavax/a/a;

    move-result-object v2

    iget-object v3, p0, Lco/uk/getmondo/common/h/b/a$i$b;->a:Lco/uk/getmondo/common/h/b/a$i;

    iget-object v3, v3, Lco/uk/getmondo/common/h/b/a$i;->a:Lco/uk/getmondo/common/h/b/a;

    .line 4293
    invoke-static {v3}, Lco/uk/getmondo/common/h/b/a;->c(Lco/uk/getmondo/common/h/b/a;)Ljavax/a/a;

    move-result-object v3

    iget-object v4, p0, Lco/uk/getmondo/common/h/b/a$i$b;->a:Lco/uk/getmondo/common/h/b/a$i;

    .line 4296
    invoke-static {v4}, Lco/uk/getmondo/common/h/b/a$i;->a(Lco/uk/getmondo/common/h/b/a$i;)Ljavax/a/a;

    move-result-object v4

    iget-object v5, p0, Lco/uk/getmondo/common/h/b/a$i$b;->a:Lco/uk/getmondo/common/h/b/a$i;

    iget-object v5, v5, Lco/uk/getmondo/common/h/b/a$i;->a:Lco/uk/getmondo/common/h/b/a;

    .line 4297
    invoke-static {v5}, Lco/uk/getmondo/common/h/b/a;->B(Lco/uk/getmondo/common/h/b/a;)Ljavax/a/a;

    move-result-object v5

    .line 4289
    invoke-static/range {v0 .. v5}, Lco/uk/getmondo/signup/identity_verification/x;->a(Lb/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;)Lb/a/b;

    move-result-object v0

    .line 4288
    invoke-static {v0}, Lb/a/a;->a(Ljavax/a/a;)Ljavax/a/a;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/common/h/b/a$i$b;->c:Ljavax/a/a;

    .line 4299
    iget-object v0, p0, Lco/uk/getmondo/common/h/b/a$i$b;->a:Lco/uk/getmondo/common/h/b/a$i;

    iget-object v0, v0, Lco/uk/getmondo/common/h/b/a$i;->a:Lco/uk/getmondo/common/h/b/a;

    .line 4301
    invoke-static {v0}, Lco/uk/getmondo/common/h/b/a;->A(Lco/uk/getmondo/common/h/b/a;)Ljavax/a/a;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/common/h/b/a$i$b;->c:Ljavax/a/a;

    .line 4300
    invoke-static {v0, v1}, Lco/uk/getmondo/signup/identity_verification/n;->a(Ljavax/a/a;Ljavax/a/a;)Lb/a;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/common/h/b/a$i$b;->d:Lb/a;

    .line 4304
    invoke-static {}, Lco/uk/getmondo/signup/identity_verification/a/d;->c()Lb/a/b;

    move-result-object v0

    invoke-static {v0}, Lb/a/a;->a(Ljavax/a/a;)Ljavax/a/a;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/common/h/b/a$i$b;->e:Ljavax/a/a;

    .line 4306
    iget-object v0, p0, Lco/uk/getmondo/common/h/b/a$i$b;->b:Lco/uk/getmondo/signup/identity_verification/j;

    .line 4307
    invoke-static {v0}, Lco/uk/getmondo/signup/identity_verification/l;->a(Lco/uk/getmondo/signup/identity_verification/j;)Lb/a/b;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/common/h/b/a$i$b;->f:Ljavax/a/a;

    .line 4310
    iget-object v0, p0, Lco/uk/getmondo/common/h/b/a$i$b;->b:Lco/uk/getmondo/signup/identity_verification/j;

    .line 4311
    invoke-static {v0}, Lco/uk/getmondo/signup/identity_verification/m;->a(Lco/uk/getmondo/signup/identity_verification/j;)Lb/a/b;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/common/h/b/a$i$b;->g:Ljavax/a/a;

    .line 4314
    iget-object v0, p0, Lco/uk/getmondo/common/h/b/a$i$b;->b:Lco/uk/getmondo/signup/identity_verification/j;

    .line 4315
    invoke-static {v0}, Lco/uk/getmondo/signup/identity_verification/k;->a(Lco/uk/getmondo/signup/identity_verification/j;)Lb/a/b;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/common/h/b/a$i$b;->h:Ljavax/a/a;

    .line 4321
    invoke-static {}, Lb/a/c;->a()Lb/a;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/common/h/b/a$i$b;->a:Lco/uk/getmondo/common/h/b/a$i;

    iget-object v1, v1, Lco/uk/getmondo/common/h/b/a$i;->a:Lco/uk/getmondo/common/h/b/a;

    .line 4322
    invoke-static {v1}, Lco/uk/getmondo/common/h/b/a;->b(Lco/uk/getmondo/common/h/b/a;)Ljavax/a/a;

    move-result-object v1

    iget-object v2, p0, Lco/uk/getmondo/common/h/b/a$i$b;->a:Lco/uk/getmondo/common/h/b/a$i;

    iget-object v2, v2, Lco/uk/getmondo/common/h/b/a$i;->a:Lco/uk/getmondo/common/h/b/a;

    .line 4323
    invoke-static {v2}, Lco/uk/getmondo/common/h/b/a;->a(Lco/uk/getmondo/common/h/b/a;)Ljavax/a/a;

    move-result-object v2

    iget-object v3, p0, Lco/uk/getmondo/common/h/b/a$i$b;->a:Lco/uk/getmondo/common/h/b/a$i;

    iget-object v3, v3, Lco/uk/getmondo/common/h/b/a$i;->a:Lco/uk/getmondo/common/h/b/a;

    .line 4324
    invoke-static {v3}, Lco/uk/getmondo/common/h/b/a;->c(Lco/uk/getmondo/common/h/b/a;)Ljavax/a/a;

    move-result-object v3

    iget-object v4, p0, Lco/uk/getmondo/common/h/b/a$i$b;->a:Lco/uk/getmondo/common/h/b/a$i;

    .line 4327
    invoke-static {v4}, Lco/uk/getmondo/common/h/b/a$i;->a(Lco/uk/getmondo/common/h/b/a$i;)Ljavax/a/a;

    move-result-object v4

    iget-object v5, p0, Lco/uk/getmondo/common/h/b/a$i$b;->e:Ljavax/a/a;

    iget-object v6, p0, Lco/uk/getmondo/common/h/b/a$i$b;->a:Lco/uk/getmondo/common/h/b/a$i;

    iget-object v6, v6, Lco/uk/getmondo/common/h/b/a$i;->a:Lco/uk/getmondo/common/h/b/a;

    .line 4329
    invoke-static {v6}, Lco/uk/getmondo/common/h/b/a;->f(Lco/uk/getmondo/common/h/b/a;)Ljavax/a/a;

    move-result-object v6

    iget-object v7, p0, Lco/uk/getmondo/common/h/b/a$i$b;->a:Lco/uk/getmondo/common/h/b/a$i;

    .line 4332
    invoke-static {v7}, Lco/uk/getmondo/common/h/b/a$i;->b(Lco/uk/getmondo/common/h/b/a$i;)Ljavax/a/a;

    move-result-object v7

    iget-object v8, p0, Lco/uk/getmondo/common/h/b/a$i$b;->f:Ljavax/a/a;

    iget-object v9, p0, Lco/uk/getmondo/common/h/b/a$i$b;->g:Ljavax/a/a;

    iget-object v10, p0, Lco/uk/getmondo/common/h/b/a$i$b;->h:Ljavax/a/a;

    .line 4320
    invoke-static/range {v0 .. v10}, Lco/uk/getmondo/signup/identity_verification/h;->a(Lb/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;)Lb/a/b;

    move-result-object v0

    .line 4319
    invoke-static {v0}, Lb/a/a;->a(Ljavax/a/a;)Ljavax/a/a;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/common/h/b/a$i$b;->i:Ljavax/a/a;

    .line 4337
    iget-object v0, p0, Lco/uk/getmondo/common/h/b/a$i$b;->i:Ljavax/a/a;

    .line 4338
    invoke-static {v0}, Lco/uk/getmondo/signup/identity_verification/f;->a(Ljavax/a/a;)Lb/a;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/common/h/b/a$i$b;->j:Lb/a;

    .line 4339
    return-void
.end method


# virtual methods
.method public a(Lco/uk/getmondo/signup/identity_verification/IdentityVerificationActivity;)V
    .locals 1

    .prologue
    .line 4343
    iget-object v0, p0, Lco/uk/getmondo/common/h/b/a$i$b;->d:Lb/a;

    invoke-interface {v0, p1}, Lb/a;->a(Ljava/lang/Object;)V

    .line 4344
    return-void
.end method

.method public a(Lco/uk/getmondo/signup/identity_verification/e;)V
    .locals 1

    .prologue
    .line 4348
    iget-object v0, p0, Lco/uk/getmondo/common/h/b/a$i$b;->j:Lb/a;

    invoke-interface {v0, p1}, Lb/a;->a(Ljava/lang/Object;)V

    .line 4349
    return-void
.end method
