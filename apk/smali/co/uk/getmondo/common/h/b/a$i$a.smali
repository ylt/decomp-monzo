.class final Lco/uk/getmondo/common/h/b/a$i$a;
.super Ljava/lang/Object;
.source "DaggerViewComponent.java"

# interfaces
.implements Lco/uk/getmondo/signup/identity_verification/id_picture/e;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lco/uk/getmondo/common/h/b/a$i;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "a"
.end annotation


# instance fields
.field final synthetic a:Lco/uk/getmondo/common/h/b/a$i;

.field private final b:Lco/uk/getmondo/signup/identity_verification/id_picture/f;

.field private c:Ljavax/a/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/api/model/identity_verification/IdentityDocumentType;",
            ">;"
        }
    .end annotation
.end field

.field private d:Ljavax/a/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/signup/identity_verification/id_picture/s;",
            ">;"
        }
    .end annotation
.end field

.field private e:Ljavax/a/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/d/i;",
            ">;"
        }
    .end annotation
.end field

.field private f:Ljavax/a/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/a/a",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private g:Ljavax/a/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/signup/identity_verification/id_picture/j;",
            ">;"
        }
    .end annotation
.end field

.field private h:Lb/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lb/a",
            "<",
            "Lco/uk/getmondo/signup/identity_verification/id_picture/DocumentCameraActivity;",
            ">;"
        }
    .end annotation
.end field

.field private i:Ljavax/a/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/signup/identity_verification/fallback/e;",
            ">;"
        }
    .end annotation
.end field

.field private j:Ljavax/a/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/signup/identity_verification/fallback/b;",
            ">;"
        }
    .end annotation
.end field

.field private k:Lb/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lb/a",
            "<",
            "Lco/uk/getmondo/signup/identity_verification/fallback/DocumentFallbackActivity;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Lco/uk/getmondo/common/h/b/a$i;Lco/uk/getmondo/signup/identity_verification/id_picture/f;)V
    .locals 1

    .prologue
    .line 4174
    iput-object p1, p0, Lco/uk/getmondo/common/h/b/a$i$a;->a:Lco/uk/getmondo/common/h/b/a$i;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 4175
    invoke-static {p2}, Lb/a/d;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/signup/identity_verification/id_picture/f;

    iput-object v0, p0, Lco/uk/getmondo/common/h/b/a$i$a;->b:Lco/uk/getmondo/signup/identity_verification/id_picture/f;

    .line 4176
    invoke-direct {p0}, Lco/uk/getmondo/common/h/b/a$i$a;->a()V

    .line 4177
    return-void
.end method

.method synthetic constructor <init>(Lco/uk/getmondo/common/h/b/a$i;Lco/uk/getmondo/signup/identity_verification/id_picture/f;Lco/uk/getmondo/common/h/b/a$1;)V
    .locals 0

    .prologue
    .line 4153
    invoke-direct {p0, p1, p2}, Lco/uk/getmondo/common/h/b/a$i$a;-><init>(Lco/uk/getmondo/common/h/b/a$i;Lco/uk/getmondo/signup/identity_verification/id_picture/f;)V

    return-void
.end method

.method private a()V
    .locals 13

    .prologue
    .line 4182
    iget-object v0, p0, Lco/uk/getmondo/common/h/b/a$i$a;->b:Lco/uk/getmondo/signup/identity_verification/id_picture/f;

    .line 4183
    invoke-static {v0}, Lco/uk/getmondo/signup/identity_verification/id_picture/i;->a(Lco/uk/getmondo/signup/identity_verification/id_picture/f;)Lb/a/b;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/common/h/b/a$i$a;->c:Ljavax/a/a;

    .line 4185
    iget-object v0, p0, Lco/uk/getmondo/common/h/b/a$i$a;->a:Lco/uk/getmondo/common/h/b/a$i;

    iget-object v0, v0, Lco/uk/getmondo/common/h/b/a$i;->a:Lco/uk/getmondo/common/h/b/a;

    .line 4187
    invoke-static {v0}, Lco/uk/getmondo/common/h/b/a;->y(Lco/uk/getmondo/common/h/b/a;)Ljavax/a/a;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/common/h/b/a$i$a;->c:Ljavax/a/a;

    .line 4186
    invoke-static {v0, v1}, Lco/uk/getmondo/signup/identity_verification/id_picture/t;->a(Ljavax/a/a;Ljavax/a/a;)Lb/a/b;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/common/h/b/a$i$a;->d:Ljavax/a/a;

    .line 4189
    iget-object v0, p0, Lco/uk/getmondo/common/h/b/a$i$a;->b:Lco/uk/getmondo/signup/identity_verification/id_picture/f;

    .line 4190
    invoke-static {v0}, Lco/uk/getmondo/signup/identity_verification/id_picture/g;->a(Lco/uk/getmondo/signup/identity_verification/id_picture/f;)Lb/a/b;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/common/h/b/a$i$a;->e:Ljavax/a/a;

    .line 4192
    iget-object v0, p0, Lco/uk/getmondo/common/h/b/a$i$a;->b:Lco/uk/getmondo/signup/identity_verification/id_picture/f;

    .line 4193
    invoke-static {v0}, Lco/uk/getmondo/signup/identity_verification/id_picture/h;->a(Lco/uk/getmondo/signup/identity_verification/id_picture/f;)Lb/a/b;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/common/h/b/a$i$a;->f:Ljavax/a/a;

    .line 4198
    invoke-static {}, Lb/a/c;->a()Lb/a;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/common/h/b/a$i$a;->a:Lco/uk/getmondo/common/h/b/a$i;

    iget-object v1, v1, Lco/uk/getmondo/common/h/b/a$i;->a:Lco/uk/getmondo/common/h/b/a;

    .line 4199
    invoke-static {v1}, Lco/uk/getmondo/common/h/b/a;->a(Lco/uk/getmondo/common/h/b/a;)Ljavax/a/a;

    move-result-object v1

    iget-object v2, p0, Lco/uk/getmondo/common/h/b/a$i$a;->a:Lco/uk/getmondo/common/h/b/a$i;

    iget-object v2, v2, Lco/uk/getmondo/common/h/b/a$i;->a:Lco/uk/getmondo/common/h/b/a;

    .line 4200
    invoke-static {v2}, Lco/uk/getmondo/common/h/b/a;->b(Lco/uk/getmondo/common/h/b/a;)Ljavax/a/a;

    move-result-object v2

    iget-object v3, p0, Lco/uk/getmondo/common/h/b/a$i$a;->a:Lco/uk/getmondo/common/h/b/a$i;

    iget-object v3, v3, Lco/uk/getmondo/common/h/b/a$i;->a:Lco/uk/getmondo/common/h/b/a;

    .line 4201
    invoke-static {v3}, Lco/uk/getmondo/common/h/b/a;->z(Lco/uk/getmondo/common/h/b/a;)Ljavax/a/a;

    move-result-object v3

    iget-object v4, p0, Lco/uk/getmondo/common/h/b/a$i$a;->a:Lco/uk/getmondo/common/h/b/a$i;

    .line 4204
    invoke-static {v4}, Lco/uk/getmondo/common/h/b/a$i;->a(Lco/uk/getmondo/common/h/b/a$i;)Ljavax/a/a;

    move-result-object v4

    iget-object v5, p0, Lco/uk/getmondo/common/h/b/a$i$a;->d:Ljavax/a/a;

    iget-object v6, p0, Lco/uk/getmondo/common/h/b/a$i$a;->a:Lco/uk/getmondo/common/h/b/a$i;

    iget-object v6, v6, Lco/uk/getmondo/common/h/b/a$i;->a:Lco/uk/getmondo/common/h/b/a;

    .line 4206
    invoke-static {v6}, Lco/uk/getmondo/common/h/b/a;->f(Lco/uk/getmondo/common/h/b/a;)Ljavax/a/a;

    move-result-object v6

    .line 4207
    invoke-static {}, Lco/uk/getmondo/signup/identity_verification/id_picture/v;->c()Lb/a/b;

    move-result-object v7

    iget-object v8, p0, Lco/uk/getmondo/common/h/b/a$i$a;->a:Lco/uk/getmondo/common/h/b/a$i;

    .line 4210
    invoke-static {v8}, Lco/uk/getmondo/common/h/b/a$i;->b(Lco/uk/getmondo/common/h/b/a$i;)Ljavax/a/a;

    move-result-object v8

    iget-object v9, p0, Lco/uk/getmondo/common/h/b/a$i$a;->c:Ljavax/a/a;

    iget-object v10, p0, Lco/uk/getmondo/common/h/b/a$i$a;->e:Ljavax/a/a;

    iget-object v11, p0, Lco/uk/getmondo/common/h/b/a$i$a;->a:Lco/uk/getmondo/common/h/b/a$i;

    .line 4213
    invoke-static {v11}, Lco/uk/getmondo/common/h/b/a$i;->c(Lco/uk/getmondo/common/h/b/a$i;)Ljavax/a/a;

    move-result-object v11

    iget-object v12, p0, Lco/uk/getmondo/common/h/b/a$i$a;->f:Ljavax/a/a;

    .line 4197
    invoke-static/range {v0 .. v12}, Lco/uk/getmondo/signup/identity_verification/id_picture/r;->a(Lb/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;)Lb/a/b;

    move-result-object v0

    .line 4196
    invoke-static {v0}, Lb/a/a;->a(Ljavax/a/a;)Ljavax/a/a;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/common/h/b/a$i$a;->g:Ljavax/a/a;

    .line 4216
    iget-object v0, p0, Lco/uk/getmondo/common/h/b/a$i$a;->g:Ljavax/a/a;

    iget-object v1, p0, Lco/uk/getmondo/common/h/b/a$i$a;->a:Lco/uk/getmondo/common/h/b/a$i;

    .line 4219
    invoke-static {v1}, Lco/uk/getmondo/common/h/b/a$i;->d(Lco/uk/getmondo/common/h/b/a$i;)Ljavax/a/a;

    move-result-object v1

    .line 4217
    invoke-static {v0, v1}, Lco/uk/getmondo/signup/identity_verification/id_picture/d;->a(Ljavax/a/a;Ljavax/a/a;)Lb/a;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/common/h/b/a$i$a;->h:Lb/a;

    .line 4221
    iget-object v0, p0, Lco/uk/getmondo/common/h/b/a$i$a;->a:Lco/uk/getmondo/common/h/b/a$i;

    iget-object v0, v0, Lco/uk/getmondo/common/h/b/a$i;->a:Lco/uk/getmondo/common/h/b/a;

    .line 4222
    invoke-static {v0}, Lco/uk/getmondo/common/h/b/a;->y(Lco/uk/getmondo/common/h/b/a;)Ljavax/a/a;

    move-result-object v0

    invoke-static {v0}, Lco/uk/getmondo/signup/identity_verification/fallback/f;->a(Ljavax/a/a;)Lb/a/b;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/common/h/b/a$i$a;->i:Ljavax/a/a;

    .line 4227
    invoke-static {}, Lb/a/c;->a()Lb/a;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/common/h/b/a$i$a;->a:Lco/uk/getmondo/common/h/b/a$i;

    iget-object v1, v1, Lco/uk/getmondo/common/h/b/a$i;->a:Lco/uk/getmondo/common/h/b/a;

    .line 4228
    invoke-static {v1}, Lco/uk/getmondo/common/h/b/a;->z(Lco/uk/getmondo/common/h/b/a;)Ljavax/a/a;

    move-result-object v1

    iget-object v2, p0, Lco/uk/getmondo/common/h/b/a$i$a;->a:Lco/uk/getmondo/common/h/b/a$i;

    iget-object v2, v2, Lco/uk/getmondo/common/h/b/a$i;->a:Lco/uk/getmondo/common/h/b/a;

    .line 4229
    invoke-static {v2}, Lco/uk/getmondo/common/h/b/a;->a(Lco/uk/getmondo/common/h/b/a;)Ljavax/a/a;

    move-result-object v2

    iget-object v3, p0, Lco/uk/getmondo/common/h/b/a$i$a;->e:Ljavax/a/a;

    iget-object v4, p0, Lco/uk/getmondo/common/h/b/a$i$a;->c:Ljavax/a/a;

    iget-object v5, p0, Lco/uk/getmondo/common/h/b/a$i$a;->a:Lco/uk/getmondo/common/h/b/a$i;

    .line 4234
    invoke-static {v5}, Lco/uk/getmondo/common/h/b/a$i;->a(Lco/uk/getmondo/common/h/b/a$i;)Ljavax/a/a;

    move-result-object v5

    .line 4235
    invoke-static {}, Lco/uk/getmondo/signup/identity_verification/id_picture/v;->c()Lb/a/b;

    move-result-object v6

    iget-object v7, p0, Lco/uk/getmondo/common/h/b/a$i$a;->i:Ljavax/a/a;

    iget-object v8, p0, Lco/uk/getmondo/common/h/b/a$i$a;->a:Lco/uk/getmondo/common/h/b/a$i;

    iget-object v8, v8, Lco/uk/getmondo/common/h/b/a$i;->a:Lco/uk/getmondo/common/h/b/a;

    .line 4237
    invoke-static {v8}, Lco/uk/getmondo/common/h/b/a;->f(Lco/uk/getmondo/common/h/b/a;)Ljavax/a/a;

    move-result-object v8

    .line 4226
    invoke-static/range {v0 .. v8}, Lco/uk/getmondo/signup/identity_verification/fallback/d;->a(Lb/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;)Lb/a/b;

    move-result-object v0

    .line 4225
    invoke-static {v0}, Lb/a/a;->a(Ljavax/a/a;)Ljavax/a/a;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/common/h/b/a$i$a;->j:Ljavax/a/a;

    .line 4239
    iget-object v0, p0, Lco/uk/getmondo/common/h/b/a$i$a;->j:Ljavax/a/a;

    iget-object v1, p0, Lco/uk/getmondo/common/h/b/a$i$a;->a:Lco/uk/getmondo/common/h/b/a$i;

    .line 4242
    invoke-static {v1}, Lco/uk/getmondo/common/h/b/a$i;->d(Lco/uk/getmondo/common/h/b/a$i;)Ljavax/a/a;

    move-result-object v1

    .line 4240
    invoke-static {v0, v1}, Lco/uk/getmondo/signup/identity_verification/fallback/a;->a(Ljavax/a/a;Ljavax/a/a;)Lb/a;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/common/h/b/a$i$a;->k:Lb/a;

    .line 4243
    return-void
.end method


# virtual methods
.method public a(Lco/uk/getmondo/signup/identity_verification/fallback/DocumentFallbackActivity;)V
    .locals 1

    .prologue
    .line 4252
    iget-object v0, p0, Lco/uk/getmondo/common/h/b/a$i$a;->k:Lb/a;

    invoke-interface {v0, p1}, Lb/a;->a(Ljava/lang/Object;)V

    .line 4253
    return-void
.end method

.method public a(Lco/uk/getmondo/signup/identity_verification/id_picture/DocumentCameraActivity;)V
    .locals 1

    .prologue
    .line 4247
    iget-object v0, p0, Lco/uk/getmondo/common/h/b/a$i$a;->h:Lb/a;

    invoke-interface {v0, p1}, Lb/a;->a(Ljava/lang/Object;)V

    .line 4248
    return-void
.end method
