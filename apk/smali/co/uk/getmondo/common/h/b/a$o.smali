.class final Lco/uk/getmondo/common/h/b/a$o;
.super Ljava/lang/Object;
.source "DaggerViewComponent.java"

# interfaces
.implements Lco/uk/getmondo/payments/send/peer/e;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lco/uk/getmondo/common/h/b/a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "o"
.end annotation


# instance fields
.field final synthetic a:Lco/uk/getmondo/common/h/b/a;

.field private final b:Lco/uk/getmondo/payments/send/peer/f;

.field private c:Ljavax/a/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/d/aa;",
            ">;"
        }
    .end annotation
.end field

.field private d:Ljavax/a/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/d/c;",
            ">;"
        }
    .end annotation
.end field

.field private e:Ljavax/a/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/a/a",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private f:Ljavax/a/a;

.field private g:Lb/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lb/a",
            "<",
            "Lco/uk/getmondo/payments/send/peer/PeerPaymentActivity;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Lco/uk/getmondo/common/h/b/a;Lco/uk/getmondo/payments/send/peer/f;)V
    .locals 1

    .prologue
    .line 3449
    iput-object p1, p0, Lco/uk/getmondo/common/h/b/a$o;->a:Lco/uk/getmondo/common/h/b/a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 3450
    invoke-static {p2}, Lb/a/d;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/payments/send/peer/f;

    iput-object v0, p0, Lco/uk/getmondo/common/h/b/a$o;->b:Lco/uk/getmondo/payments/send/peer/f;

    .line 3451
    invoke-direct {p0}, Lco/uk/getmondo/common/h/b/a$o;->a()V

    .line 3452
    return-void
.end method

.method synthetic constructor <init>(Lco/uk/getmondo/common/h/b/a;Lco/uk/getmondo/payments/send/peer/f;Lco/uk/getmondo/common/h/b/a$1;)V
    .locals 0

    .prologue
    .line 3435
    invoke-direct {p0, p1, p2}, Lco/uk/getmondo/common/h/b/a$o;-><init>(Lco/uk/getmondo/common/h/b/a;Lco/uk/getmondo/payments/send/peer/f;)V

    return-void
.end method

.method private a()V
    .locals 9

    .prologue
    .line 3457
    iget-object v0, p0, Lco/uk/getmondo/common/h/b/a$o;->b:Lco/uk/getmondo/payments/send/peer/f;

    invoke-static {v0}, Lco/uk/getmondo/payments/send/peer/i;->a(Lco/uk/getmondo/payments/send/peer/f;)Lb/a/b;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/common/h/b/a$o;->c:Ljavax/a/a;

    .line 3459
    iget-object v0, p0, Lco/uk/getmondo/common/h/b/a$o;->b:Lco/uk/getmondo/payments/send/peer/f;

    invoke-static {v0}, Lco/uk/getmondo/payments/send/peer/g;->a(Lco/uk/getmondo/payments/send/peer/f;)Lb/a/b;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/common/h/b/a$o;->d:Ljavax/a/a;

    .line 3461
    iget-object v0, p0, Lco/uk/getmondo/common/h/b/a$o;->b:Lco/uk/getmondo/payments/send/peer/f;

    invoke-static {v0}, Lco/uk/getmondo/payments/send/peer/h;->a(Lco/uk/getmondo/payments/send/peer/f;)Lb/a/b;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/common/h/b/a$o;->e:Ljavax/a/a;

    .line 3466
    invoke-static {}, Lb/a/c;->a()Lb/a;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/common/h/b/a$o;->a:Lco/uk/getmondo/common/h/b/a;

    .line 3467
    invoke-static {v1}, Lco/uk/getmondo/common/h/b/a;->a(Lco/uk/getmondo/common/h/b/a;)Ljavax/a/a;

    move-result-object v1

    iget-object v2, p0, Lco/uk/getmondo/common/h/b/a$o;->a:Lco/uk/getmondo/common/h/b/a;

    .line 3468
    invoke-static {v2}, Lco/uk/getmondo/common/h/b/a;->b(Lco/uk/getmondo/common/h/b/a;)Ljavax/a/a;

    move-result-object v2

    iget-object v3, p0, Lco/uk/getmondo/common/h/b/a$o;->a:Lco/uk/getmondo/common/h/b/a;

    .line 3469
    invoke-static {v3}, Lco/uk/getmondo/common/h/b/a;->c(Lco/uk/getmondo/common/h/b/a;)Ljavax/a/a;

    move-result-object v3

    iget-object v4, p0, Lco/uk/getmondo/common/h/b/a$o;->a:Lco/uk/getmondo/common/h/b/a;

    .line 3470
    invoke-static {v4}, Lco/uk/getmondo/common/h/b/a;->d(Lco/uk/getmondo/common/h/b/a;)Ljavax/a/a;

    move-result-object v4

    iget-object v5, p0, Lco/uk/getmondo/common/h/b/a$o;->c:Ljavax/a/a;

    iget-object v6, p0, Lco/uk/getmondo/common/h/b/a$o;->d:Ljavax/a/a;

    iget-object v7, p0, Lco/uk/getmondo/common/h/b/a$o;->e:Ljavax/a/a;

    iget-object v8, p0, Lco/uk/getmondo/common/h/b/a$o;->a:Lco/uk/getmondo/common/h/b/a;

    .line 3474
    invoke-static {v8}, Lco/uk/getmondo/common/h/b/a;->e(Lco/uk/getmondo/common/h/b/a;)Ljavax/a/a;

    move-result-object v8

    .line 3465
    invoke-static/range {v0 .. v8}, Lco/uk/getmondo/payments/send/peer/r;->a(Lb/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;)Lb/a/b;

    move-result-object v0

    .line 3464
    invoke-static {v0}, Lb/a/a;->a(Ljavax/a/a;)Ljavax/a/a;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/common/h/b/a$o;->f:Ljavax/a/a;

    .line 3476
    iget-object v0, p0, Lco/uk/getmondo/common/h/b/a$o;->f:Ljavax/a/a;

    iget-object v1, p0, Lco/uk/getmondo/common/h/b/a$o;->a:Lco/uk/getmondo/common/h/b/a;

    .line 3478
    invoke-static {v1}, Lco/uk/getmondo/common/h/b/a;->f(Lco/uk/getmondo/common/h/b/a;)Ljavax/a/a;

    move-result-object v1

    .line 3477
    invoke-static {v0, v1}, Lco/uk/getmondo/payments/send/peer/d;->a(Ljavax/a/a;Ljavax/a/a;)Lb/a;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/common/h/b/a$o;->g:Lb/a;

    .line 3479
    return-void
.end method


# virtual methods
.method public a(Lco/uk/getmondo/payments/send/peer/PeerPaymentActivity;)V
    .locals 1

    .prologue
    .line 3488
    iget-object v0, p0, Lco/uk/getmondo/common/h/b/a$o;->g:Lb/a;

    invoke-interface {v0, p1}, Lb/a;->a(Ljava/lang/Object;)V

    .line 3489
    return-void
.end method
