.class final Lco/uk/getmondo/common/h/b/a$e;
.super Ljava/lang/Object;
.source "DaggerViewComponent.java"

# interfaces
.implements Lco/uk/getmondo/monzo/me/customise/d;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lco/uk/getmondo/common/h/b/a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "e"
.end annotation


# instance fields
.field final synthetic a:Lco/uk/getmondo/common/h/b/a;

.field private final b:Lco/uk/getmondo/monzo/me/customise/e;

.field private c:Ljavax/a/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/d/c;",
            ">;"
        }
    .end annotation
.end field

.field private d:Ljavax/a/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/a/a",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private e:Ljavax/a/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/api/model/tracking/Impression$CustomiseMonzoMeLinkFrom;",
            ">;"
        }
    .end annotation
.end field

.field private f:Ljavax/a/a;

.field private g:Lb/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lb/a",
            "<",
            "Lco/uk/getmondo/monzo/me/customise/CustomiseMonzoMeLinkActivity;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Lco/uk/getmondo/common/h/b/a;Lco/uk/getmondo/monzo/me/customise/e;)V
    .locals 1

    .prologue
    .line 3887
    iput-object p1, p0, Lco/uk/getmondo/common/h/b/a$e;->a:Lco/uk/getmondo/common/h/b/a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 3888
    invoke-static {p2}, Lb/a/d;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/monzo/me/customise/e;

    iput-object v0, p0, Lco/uk/getmondo/common/h/b/a$e;->b:Lco/uk/getmondo/monzo/me/customise/e;

    .line 3889
    invoke-direct {p0}, Lco/uk/getmondo/common/h/b/a$e;->a()V

    .line 3890
    return-void
.end method

.method synthetic constructor <init>(Lco/uk/getmondo/common/h/b/a;Lco/uk/getmondo/monzo/me/customise/e;Lco/uk/getmondo/common/h/b/a$1;)V
    .locals 0

    .prologue
    .line 3871
    invoke-direct {p0, p1, p2}, Lco/uk/getmondo/common/h/b/a$e;-><init>(Lco/uk/getmondo/common/h/b/a;Lco/uk/getmondo/monzo/me/customise/e;)V

    return-void
.end method

.method private a()V
    .locals 8

    .prologue
    .line 3895
    iget-object v0, p0, Lco/uk/getmondo/common/h/b/a$e;->b:Lco/uk/getmondo/monzo/me/customise/e;

    .line 3896
    invoke-static {v0}, Lco/uk/getmondo/monzo/me/customise/f;->a(Lco/uk/getmondo/monzo/me/customise/e;)Lb/a/b;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/common/h/b/a$e;->c:Ljavax/a/a;

    .line 3898
    iget-object v0, p0, Lco/uk/getmondo/common/h/b/a$e;->b:Lco/uk/getmondo/monzo/me/customise/e;

    .line 3899
    invoke-static {v0}, Lco/uk/getmondo/monzo/me/customise/h;->a(Lco/uk/getmondo/monzo/me/customise/e;)Lb/a/b;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/common/h/b/a$e;->d:Ljavax/a/a;

    .line 3901
    iget-object v0, p0, Lco/uk/getmondo/common/h/b/a$e;->b:Lco/uk/getmondo/monzo/me/customise/e;

    .line 3902
    invoke-static {v0}, Lco/uk/getmondo/monzo/me/customise/g;->a(Lco/uk/getmondo/monzo/me/customise/e;)Lb/a/b;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/common/h/b/a$e;->e:Ljavax/a/a;

    .line 3907
    invoke-static {}, Lb/a/c;->a()Lb/a;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/common/h/b/a$e;->a:Lco/uk/getmondo/common/h/b/a;

    .line 3908
    invoke-static {v1}, Lco/uk/getmondo/common/h/b/a;->f(Lco/uk/getmondo/common/h/b/a;)Ljavax/a/a;

    move-result-object v1

    iget-object v2, p0, Lco/uk/getmondo/common/h/b/a$e;->a:Lco/uk/getmondo/common/h/b/a;

    .line 3909
    invoke-static {v2}, Lco/uk/getmondo/common/h/b/a;->j(Lco/uk/getmondo/common/h/b/a;)Ljavax/a/a;

    move-result-object v2

    iget-object v3, p0, Lco/uk/getmondo/common/h/b/a$e;->c:Ljavax/a/a;

    iget-object v4, p0, Lco/uk/getmondo/common/h/b/a$e;->d:Ljavax/a/a;

    .line 3912
    invoke-static {}, Lco/uk/getmondo/monzo/me/b;->c()Lb/a/b;

    move-result-object v5

    iget-object v6, p0, Lco/uk/getmondo/common/h/b/a$e;->e:Ljavax/a/a;

    iget-object v7, p0, Lco/uk/getmondo/common/h/b/a$e;->a:Lco/uk/getmondo/common/h/b/a;

    .line 3914
    invoke-static {v7}, Lco/uk/getmondo/common/h/b/a;->m(Lco/uk/getmondo/common/h/b/a;)Ljavax/a/a;

    move-result-object v7

    .line 3906
    invoke-static/range {v0 .. v7}, Lco/uk/getmondo/monzo/me/customise/o;->a(Lb/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;)Lb/a/b;

    move-result-object v0

    .line 3905
    invoke-static {v0}, Lb/a/a;->a(Ljavax/a/a;)Ljavax/a/a;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/common/h/b/a$e;->f:Ljavax/a/a;

    .line 3916
    iget-object v0, p0, Lco/uk/getmondo/common/h/b/a$e;->f:Ljavax/a/a;

    .line 3917
    invoke-static {v0}, Lco/uk/getmondo/monzo/me/customise/c;->a(Ljavax/a/a;)Lb/a;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/common/h/b/a$e;->g:Lb/a;

    .line 3919
    return-void
.end method


# virtual methods
.method public a(Lco/uk/getmondo/monzo/me/customise/CustomiseMonzoMeLinkActivity;)V
    .locals 1

    .prologue
    .line 3923
    iget-object v0, p0, Lco/uk/getmondo/common/h/b/a$e;->g:Lb/a;

    invoke-interface {v0, p1}, Lb/a;->a(Ljava/lang/Object;)V

    .line 3924
    return-void
.end method
