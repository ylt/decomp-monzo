.class final Lco/uk/getmondo/common/h/b/a$s;
.super Ljava/lang/Object;
.source "DaggerViewComponent.java"

# interfaces
.implements Lco/uk/getmondo/spending/merchant/a;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lco/uk/getmondo/common/h/b/a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "s"
.end annotation


# instance fields
.field final synthetic a:Lco/uk/getmondo/common/h/b/a;

.field private final b:Lco/uk/getmondo/spending/merchant/f;

.field private c:Ljavax/a/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/spending/merchant/h;",
            ">;"
        }
    .end annotation
.end field

.field private d:Lb/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lb/a",
            "<",
            "Lco/uk/getmondo/spending/merchant/SpendingByMerchantFragment;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Lco/uk/getmondo/common/h/b/a;Lco/uk/getmondo/spending/merchant/f;)V
    .locals 1

    .prologue
    .line 3756
    iput-object p1, p0, Lco/uk/getmondo/common/h/b/a$s;->a:Lco/uk/getmondo/common/h/b/a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 3757
    invoke-static {p2}, Lb/a/d;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/spending/merchant/f;

    iput-object v0, p0, Lco/uk/getmondo/common/h/b/a$s;->b:Lco/uk/getmondo/spending/merchant/f;

    .line 3758
    invoke-direct {p0}, Lco/uk/getmondo/common/h/b/a$s;->a()V

    .line 3759
    return-void
.end method

.method synthetic constructor <init>(Lco/uk/getmondo/common/h/b/a;Lco/uk/getmondo/spending/merchant/f;Lco/uk/getmondo/common/h/b/a$1;)V
    .locals 0

    .prologue
    .line 3749
    invoke-direct {p0, p1, p2}, Lco/uk/getmondo/common/h/b/a$s;-><init>(Lco/uk/getmondo/common/h/b/a;Lco/uk/getmondo/spending/merchant/f;)V

    return-void
.end method

.method private a()V
    .locals 5

    .prologue
    .line 3764
    iget-object v0, p0, Lco/uk/getmondo/common/h/b/a$s;->b:Lco/uk/getmondo/spending/merchant/f;

    iget-object v1, p0, Lco/uk/getmondo/common/h/b/a$s;->a:Lco/uk/getmondo/common/h/b/a;

    .line 3768
    invoke-static {v1}, Lco/uk/getmondo/common/h/b/a;->a(Lco/uk/getmondo/common/h/b/a;)Ljavax/a/a;

    move-result-object v1

    iget-object v2, p0, Lco/uk/getmondo/common/h/b/a$s;->a:Lco/uk/getmondo/common/h/b/a;

    .line 3769
    invoke-static {v2}, Lco/uk/getmondo/common/h/b/a;->q(Lco/uk/getmondo/common/h/b/a;)Ljavax/a/a;

    move-result-object v2

    .line 3770
    invoke-static {}, Lco/uk/getmondo/spending/a/f;->c()Lb/a/b;

    move-result-object v3

    iget-object v4, p0, Lco/uk/getmondo/common/h/b/a$s;->a:Lco/uk/getmondo/common/h/b/a;

    .line 3771
    invoke-static {v4}, Lco/uk/getmondo/common/h/b/a;->f(Lco/uk/getmondo/common/h/b/a;)Ljavax/a/a;

    move-result-object v4

    .line 3766
    invoke-static {v0, v1, v2, v3, v4}, Lco/uk/getmondo/spending/merchant/g;->a(Lco/uk/getmondo/spending/merchant/f;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;)Lb/a/b;

    move-result-object v0

    .line 3765
    invoke-static {v0}, Lb/a/a;->a(Ljavax/a/a;)Ljavax/a/a;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/common/h/b/a$s;->c:Ljavax/a/a;

    .line 3773
    iget-object v0, p0, Lco/uk/getmondo/common/h/b/a$s;->c:Ljavax/a/a;

    .line 3774
    invoke-static {v0}, Lco/uk/getmondo/spending/merchant/e;->a(Ljavax/a/a;)Lb/a;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/common/h/b/a$s;->d:Lb/a;

    .line 3775
    return-void
.end method


# virtual methods
.method public a(Lco/uk/getmondo/spending/merchant/SpendingByMerchantFragment;)V
    .locals 1

    .prologue
    .line 3784
    iget-object v0, p0, Lco/uk/getmondo/common/h/b/a$s;->d:Lb/a;

    invoke-interface {v0, p1}, Lb/a;->a(Ljava/lang/Object;)V

    .line 3785
    return-void
.end method
