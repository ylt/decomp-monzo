.class final Lco/uk/getmondo/common/h/b/a$n;
.super Ljava/lang/Object;
.source "DaggerViewComponent.java"

# interfaces
.implements Lco/uk/getmondo/payments/send/payment_category/l;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lco/uk/getmondo/common/h/b/a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "n"
.end annotation


# instance fields
.field final synthetic a:Lco/uk/getmondo/common/h/b/a;

.field private final b:Lco/uk/getmondo/payments/send/payment_category/m;

.field private c:Ljavax/a/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/payments/send/data/a/f;",
            ">;"
        }
    .end annotation
.end field

.field private d:Ljavax/a/a;

.field private e:Lb/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lb/a",
            "<",
            "Lco/uk/getmondo/payments/send/payment_category/PaymentCategoryActivity;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Lco/uk/getmondo/common/h/b/a;Lco/uk/getmondo/payments/send/payment_category/m;)V
    .locals 1

    .prologue
    .line 3798
    iput-object p1, p0, Lco/uk/getmondo/common/h/b/a$n;->a:Lco/uk/getmondo/common/h/b/a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 3799
    invoke-static {p2}, Lb/a/d;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/payments/send/payment_category/m;

    iput-object v0, p0, Lco/uk/getmondo/common/h/b/a$n;->b:Lco/uk/getmondo/payments/send/payment_category/m;

    .line 3800
    invoke-direct {p0}, Lco/uk/getmondo/common/h/b/a$n;->a()V

    .line 3801
    return-void
.end method

.method synthetic constructor <init>(Lco/uk/getmondo/common/h/b/a;Lco/uk/getmondo/payments/send/payment_category/m;Lco/uk/getmondo/common/h/b/a$1;)V
    .locals 0

    .prologue
    .line 3788
    invoke-direct {p0, p1, p2}, Lco/uk/getmondo/common/h/b/a$n;-><init>(Lco/uk/getmondo/common/h/b/a;Lco/uk/getmondo/payments/send/payment_category/m;)V

    return-void
.end method

.method private a()V
    .locals 5

    .prologue
    .line 3806
    iget-object v0, p0, Lco/uk/getmondo/common/h/b/a$n;->b:Lco/uk/getmondo/payments/send/payment_category/m;

    .line 3808
    invoke-static {v0}, Lco/uk/getmondo/payments/send/payment_category/n;->a(Lco/uk/getmondo/payments/send/payment_category/m;)Lb/a/b;

    move-result-object v0

    .line 3807
    invoke-static {v0}, Lb/a/a;->a(Ljavax/a/a;)Ljavax/a/a;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/common/h/b/a$n;->c:Ljavax/a/a;

    .line 3813
    invoke-static {}, Lb/a/c;->a()Lb/a;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/common/h/b/a$n;->a:Lco/uk/getmondo/common/h/b/a;

    .line 3814
    invoke-static {v1}, Lco/uk/getmondo/common/h/b/a;->b(Lco/uk/getmondo/common/h/b/a;)Ljavax/a/a;

    move-result-object v1

    iget-object v2, p0, Lco/uk/getmondo/common/h/b/a$n;->a:Lco/uk/getmondo/common/h/b/a;

    .line 3815
    invoke-static {v2}, Lco/uk/getmondo/common/h/b/a;->s(Lco/uk/getmondo/common/h/b/a;)Ljavax/a/a;

    move-result-object v2

    iget-object v3, p0, Lco/uk/getmondo/common/h/b/a$n;->a:Lco/uk/getmondo/common/h/b/a;

    .line 3816
    invoke-static {v3}, Lco/uk/getmondo/common/h/b/a;->f(Lco/uk/getmondo/common/h/b/a;)Ljavax/a/a;

    move-result-object v3

    iget-object v4, p0, Lco/uk/getmondo/common/h/b/a$n;->c:Ljavax/a/a;

    .line 3812
    invoke-static {v0, v1, v2, v3, v4}, Lco/uk/getmondo/payments/send/payment_category/t;->a(Lb/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;)Lb/a/b;

    move-result-object v0

    .line 3811
    invoke-static {v0}, Lb/a/a;->a(Ljavax/a/a;)Ljavax/a/a;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/common/h/b/a$n;->d:Ljavax/a/a;

    .line 3819
    iget-object v0, p0, Lco/uk/getmondo/common/h/b/a$n;->d:Ljavax/a/a;

    .line 3820
    invoke-static {v0}, Lco/uk/getmondo/payments/send/payment_category/k;->a(Ljavax/a/a;)Lb/a;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/common/h/b/a$n;->e:Lb/a;

    .line 3821
    return-void
.end method


# virtual methods
.method public a(Lco/uk/getmondo/payments/send/payment_category/PaymentCategoryActivity;)V
    .locals 1

    .prologue
    .line 3825
    iget-object v0, p0, Lco/uk/getmondo/common/h/b/a$n;->e:Lb/a;

    invoke-interface {v0, p1}, Lb/a;->a(Ljava/lang/Object;)V

    .line 3826
    return-void
.end method
