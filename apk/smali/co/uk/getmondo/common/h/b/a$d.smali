.class public final Lco/uk/getmondo/common/h/b/a$d;
.super Ljava/lang/Object;
.source "DaggerViewComponent.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lco/uk/getmondo/common/h/b/a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "d"
.end annotation


# instance fields
.field private a:Lco/uk/getmondo/common/h/b/c;

.field private b:Lco/uk/getmondo/common/h/a/a;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 3411
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lco/uk/getmondo/common/h/b/a$1;)V
    .locals 0

    .prologue
    .line 3406
    invoke-direct {p0}, Lco/uk/getmondo/common/h/b/a$d;-><init>()V

    return-void
.end method

.method static synthetic a(Lco/uk/getmondo/common/h/b/a$d;)Lco/uk/getmondo/common/h/b/c;
    .locals 1

    .prologue
    .line 3406
    iget-object v0, p0, Lco/uk/getmondo/common/h/b/a$d;->a:Lco/uk/getmondo/common/h/b/c;

    return-object v0
.end method

.method static synthetic b(Lco/uk/getmondo/common/h/b/a$d;)Lco/uk/getmondo/common/h/a/a;
    .locals 1

    .prologue
    .line 3406
    iget-object v0, p0, Lco/uk/getmondo/common/h/b/a$d;->b:Lco/uk/getmondo/common/h/a/a;

    return-object v0
.end method


# virtual methods
.method public a(Lco/uk/getmondo/common/h/a/a;)Lco/uk/getmondo/common/h/b/a$d;
    .locals 1

    .prologue
    .line 3430
    invoke-static {p1}, Lb/a/d;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/common/h/a/a;

    iput-object v0, p0, Lco/uk/getmondo/common/h/b/a$d;->b:Lco/uk/getmondo/common/h/a/a;

    .line 3431
    return-object p0
.end method

.method public a(Lco/uk/getmondo/common/h/b/c;)Lco/uk/getmondo/common/h/b/a$d;
    .locals 1

    .prologue
    .line 3425
    invoke-static {p1}, Lb/a/d;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/common/h/b/c;

    iput-object v0, p0, Lco/uk/getmondo/common/h/b/a$d;->a:Lco/uk/getmondo/common/h/b/c;

    .line 3426
    return-object p0
.end method

.method public a()Lco/uk/getmondo/common/h/b/b;
    .locals 3

    .prologue
    .line 3414
    iget-object v0, p0, Lco/uk/getmondo/common/h/b/a$d;->a:Lco/uk/getmondo/common/h/b/c;

    if-nez v0, :cond_0

    .line 3415
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-class v2, Lco/uk/getmondo/common/h/b/c;

    invoke-virtual {v2}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " must be set"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 3417
    :cond_0
    iget-object v0, p0, Lco/uk/getmondo/common/h/b/a$d;->b:Lco/uk/getmondo/common/h/a/a;

    if-nez v0, :cond_1

    .line 3418
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-class v2, Lco/uk/getmondo/common/h/a/a;

    .line 3419
    invoke-virtual {v2}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " must be set"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 3421
    :cond_1
    new-instance v0, Lco/uk/getmondo/common/h/b/a;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lco/uk/getmondo/common/h/b/a;-><init>(Lco/uk/getmondo/common/h/b/a$d;Lco/uk/getmondo/common/h/b/a$1;)V

    return-object v0
.end method
