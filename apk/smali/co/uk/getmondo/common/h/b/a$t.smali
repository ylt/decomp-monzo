.class final Lco/uk/getmondo/common/h/b/a$t;
.super Ljava/lang/Object;
.source "DaggerViewComponent.java"

# interfaces
.implements Lco/uk/getmondo/spending/transactions/b;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lco/uk/getmondo/common/h/b/a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "t"
.end annotation


# instance fields
.field final synthetic a:Lco/uk/getmondo/common/h/b/a;

.field private final b:Lco/uk/getmondo/spending/transactions/d;

.field private c:Ljavax/a/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/spending/transactions/f;",
            ">;"
        }
    .end annotation
.end field

.field private d:Ljavax/a/a;

.field private e:Lb/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lb/a",
            "<",
            "Lco/uk/getmondo/spending/transactions/SpendingTransactionsFragment;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Lco/uk/getmondo/common/h/b/a;Lco/uk/getmondo/spending/transactions/d;)V
    .locals 1

    .prologue
    .line 3713
    iput-object p1, p0, Lco/uk/getmondo/common/h/b/a$t;->a:Lco/uk/getmondo/common/h/b/a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 3714
    invoke-static {p2}, Lb/a/d;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/spending/transactions/d;

    iput-object v0, p0, Lco/uk/getmondo/common/h/b/a$t;->b:Lco/uk/getmondo/spending/transactions/d;

    .line 3715
    invoke-direct {p0}, Lco/uk/getmondo/common/h/b/a$t;->a()V

    .line 3716
    return-void
.end method

.method synthetic constructor <init>(Lco/uk/getmondo/common/h/b/a;Lco/uk/getmondo/spending/transactions/d;Lco/uk/getmondo/common/h/b/a$1;)V
    .locals 0

    .prologue
    .line 3701
    invoke-direct {p0, p1, p2}, Lco/uk/getmondo/common/h/b/a$t;-><init>(Lco/uk/getmondo/common/h/b/a;Lco/uk/getmondo/spending/transactions/d;)V

    return-void
.end method

.method private a()V
    .locals 4

    .prologue
    .line 3721
    iget-object v0, p0, Lco/uk/getmondo/common/h/b/a$t;->b:Lco/uk/getmondo/spending/transactions/d;

    iget-object v1, p0, Lco/uk/getmondo/common/h/b/a$t;->a:Lco/uk/getmondo/common/h/b/a;

    .line 3724
    invoke-static {v1}, Lco/uk/getmondo/common/h/b/a;->a(Lco/uk/getmondo/common/h/b/a;)Ljavax/a/a;

    move-result-object v1

    iget-object v2, p0, Lco/uk/getmondo/common/h/b/a$t;->a:Lco/uk/getmondo/common/h/b/a;

    .line 3725
    invoke-static {v2}, Lco/uk/getmondo/common/h/b/a;->q(Lco/uk/getmondo/common/h/b/a;)Ljavax/a/a;

    move-result-object v2

    iget-object v3, p0, Lco/uk/getmondo/common/h/b/a$t;->a:Lco/uk/getmondo/common/h/b/a;

    .line 3726
    invoke-static {v3}, Lco/uk/getmondo/common/h/b/a;->f(Lco/uk/getmondo/common/h/b/a;)Ljavax/a/a;

    move-result-object v3

    .line 3722
    invoke-static {v0, v1, v2, v3}, Lco/uk/getmondo/spending/transactions/e;->a(Lco/uk/getmondo/spending/transactions/d;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;)Lb/a/b;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/common/h/b/a$t;->c:Ljavax/a/a;

    .line 3730
    invoke-static {}, Lb/a/c;->a()Lb/a;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/common/h/b/a$t;->a:Lco/uk/getmondo/common/h/b/a;

    .line 3731
    invoke-static {v1}, Lco/uk/getmondo/common/h/b/a;->r(Lco/uk/getmondo/common/h/b/a;)Ljavax/a/a;

    move-result-object v1

    .line 3729
    invoke-static {v0, v1}, Lco/uk/getmondo/spending/transactions/a;->a(Lb/a;Ljavax/a/a;)Lb/a/b;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/common/h/b/a$t;->d:Ljavax/a/a;

    .line 3733
    iget-object v0, p0, Lco/uk/getmondo/common/h/b/a$t;->c:Ljavax/a/a;

    iget-object v1, p0, Lco/uk/getmondo/common/h/b/a$t;->d:Ljavax/a/a;

    .line 3734
    invoke-static {v0, v1}, Lco/uk/getmondo/spending/transactions/c;->a(Ljavax/a/a;Ljavax/a/a;)Lb/a;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/common/h/b/a$t;->e:Lb/a;

    .line 3736
    return-void
.end method


# virtual methods
.method public a(Lco/uk/getmondo/spending/transactions/SpendingTransactionsFragment;)V
    .locals 1

    .prologue
    .line 3745
    iget-object v0, p0, Lco/uk/getmondo/common/h/b/a$t;->e:Lb/a;

    invoke-interface {v0, p1}, Lb/a;->a(Ljava/lang/Object;)V

    .line 3746
    return-void
.end method
