.class final Lco/uk/getmondo/common/h/b/a$p;
.super Ljava/lang/Object;
.source "DaggerViewComponent.java"

# interfaces
.implements Lco/uk/getmondo/common/pin/a;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lco/uk/getmondo/common/h/b/a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "p"
.end annotation


# instance fields
.field final synthetic a:Lco/uk/getmondo/common/h/b/a;

.field private final b:Lco/uk/getmondo/common/pin/g;

.field private c:Ljavax/a/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/common/pin/a/a/b;",
            ">;"
        }
    .end annotation
.end field

.field private d:Ljavax/a/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/common/pin/c;",
            ">;"
        }
    .end annotation
.end field

.field private e:Lb/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lb/a",
            "<",
            "Lco/uk/getmondo/common/pin/PinEntryActivity;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Lco/uk/getmondo/common/h/b/a;Lco/uk/getmondo/common/pin/g;)V
    .locals 1

    .prologue
    .line 4488
    iput-object p1, p0, Lco/uk/getmondo/common/h/b/a$p;->a:Lco/uk/getmondo/common/h/b/a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 4489
    invoke-static {p2}, Lb/a/d;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/common/pin/g;

    iput-object v0, p0, Lco/uk/getmondo/common/h/b/a$p;->b:Lco/uk/getmondo/common/pin/g;

    .line 4490
    invoke-direct {p0}, Lco/uk/getmondo/common/h/b/a$p;->a()V

    .line 4491
    return-void
.end method

.method synthetic constructor <init>(Lco/uk/getmondo/common/h/b/a;Lco/uk/getmondo/common/pin/g;Lco/uk/getmondo/common/h/b/a$1;)V
    .locals 0

    .prologue
    .line 4479
    invoke-direct {p0, p1, p2}, Lco/uk/getmondo/common/h/b/a$p;-><init>(Lco/uk/getmondo/common/h/b/a;Lco/uk/getmondo/common/pin/g;)V

    return-void
.end method

.method private a()V
    .locals 7

    .prologue
    .line 4496
    iget-object v0, p0, Lco/uk/getmondo/common/h/b/a$p;->b:Lco/uk/getmondo/common/pin/g;

    .line 4497
    invoke-static {v0}, Lco/uk/getmondo/common/pin/h;->a(Lco/uk/getmondo/common/pin/g;)Lb/a/b;

    move-result-object v0

    invoke-static {v0}, Lb/a/a;->a(Ljavax/a/a;)Ljavax/a/a;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/common/h/b/a$p;->c:Ljavax/a/a;

    .line 4502
    invoke-static {}, Lb/a/c;->a()Lb/a;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/common/h/b/a$p;->a:Lco/uk/getmondo/common/h/b/a;

    .line 4503
    invoke-static {v1}, Lco/uk/getmondo/common/h/b/a;->b(Lco/uk/getmondo/common/h/b/a;)Ljavax/a/a;

    move-result-object v1

    iget-object v2, p0, Lco/uk/getmondo/common/h/b/a$p;->a:Lco/uk/getmondo/common/h/b/a;

    .line 4504
    invoke-static {v2}, Lco/uk/getmondo/common/h/b/a;->a(Lco/uk/getmondo/common/h/b/a;)Ljavax/a/a;

    move-result-object v2

    iget-object v3, p0, Lco/uk/getmondo/common/h/b/a$p;->a:Lco/uk/getmondo/common/h/b/a;

    .line 4505
    invoke-static {v3}, Lco/uk/getmondo/common/h/b/a;->c(Lco/uk/getmondo/common/h/b/a;)Ljavax/a/a;

    move-result-object v3

    iget-object v4, p0, Lco/uk/getmondo/common/h/b/a$p;->c:Ljavax/a/a;

    iget-object v5, p0, Lco/uk/getmondo/common/h/b/a$p;->a:Lco/uk/getmondo/common/h/b/a;

    .line 4507
    invoke-static {v5}, Lco/uk/getmondo/common/h/b/a;->B(Lco/uk/getmondo/common/h/b/a;)Ljavax/a/a;

    move-result-object v5

    iget-object v6, p0, Lco/uk/getmondo/common/h/b/a$p;->a:Lco/uk/getmondo/common/h/b/a;

    .line 4508
    invoke-static {v6}, Lco/uk/getmondo/common/h/b/a;->f(Lco/uk/getmondo/common/h/b/a;)Ljavax/a/a;

    move-result-object v6

    .line 4501
    invoke-static/range {v0 .. v6}, Lco/uk/getmondo/common/pin/f;->a(Lb/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;)Lb/a/b;

    move-result-object v0

    .line 4500
    invoke-static {v0}, Lb/a/a;->a(Ljavax/a/a;)Ljavax/a/a;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/common/h/b/a$p;->d:Ljavax/a/a;

    .line 4510
    iget-object v0, p0, Lco/uk/getmondo/common/h/b/a$p;->d:Ljavax/a/a;

    .line 4511
    invoke-static {v0}, Lco/uk/getmondo/common/pin/b;->a(Ljavax/a/a;)Lb/a;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/common/h/b/a$p;->e:Lb/a;

    .line 4512
    return-void
.end method


# virtual methods
.method public a(Lco/uk/getmondo/common/pin/PinEntryActivity;)V
    .locals 1

    .prologue
    .line 4516
    iget-object v0, p0, Lco/uk/getmondo/common/h/b/a$p;->e:Lb/a;

    invoke-interface {v0, p1}, Lb/a;->a(Ljava/lang/Object;)V

    .line 4517
    return-void
.end method
