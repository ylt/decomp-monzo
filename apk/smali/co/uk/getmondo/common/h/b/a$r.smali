.class final Lco/uk/getmondo/common/h/b/a$r;
.super Ljava/lang/Object;
.source "DaggerViewComponent.java"

# interfaces
.implements Lco/uk/getmondo/common/address/m;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lco/uk/getmondo/common/h/b/a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "r"
.end annotation


# instance fields
.field final synthetic a:Lco/uk/getmondo/common/h/b/a;

.field private final b:Lco/uk/getmondo/common/address/n;

.field private c:Ljavax/a/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/a/a",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private d:Ljavax/a/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/common/address/p;",
            ">;"
        }
    .end annotation
.end field

.field private e:Lb/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lb/a",
            "<",
            "Lco/uk/getmondo/common/address/SelectAddressActivity;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Lco/uk/getmondo/common/h/b/a;Lco/uk/getmondo/common/address/n;)V
    .locals 1

    .prologue
    .line 3611
    iput-object p1, p0, Lco/uk/getmondo/common/h/b/a$r;->a:Lco/uk/getmondo/common/h/b/a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 3612
    invoke-static {p2}, Lb/a/d;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/common/address/n;

    iput-object v0, p0, Lco/uk/getmondo/common/h/b/a$r;->b:Lco/uk/getmondo/common/address/n;

    .line 3613
    invoke-direct {p0}, Lco/uk/getmondo/common/h/b/a$r;->a()V

    .line 3614
    return-void
.end method

.method synthetic constructor <init>(Lco/uk/getmondo/common/h/b/a;Lco/uk/getmondo/common/address/n;Lco/uk/getmondo/common/h/b/a$1;)V
    .locals 0

    .prologue
    .line 3600
    invoke-direct {p0, p1, p2}, Lco/uk/getmondo/common/h/b/a$r;-><init>(Lco/uk/getmondo/common/h/b/a;Lco/uk/getmondo/common/address/n;)V

    return-void
.end method

.method private a()V
    .locals 8

    .prologue
    .line 3619
    iget-object v0, p0, Lco/uk/getmondo/common/h/b/a$r;->b:Lco/uk/getmondo/common/address/n;

    .line 3620
    invoke-static {v0}, Lco/uk/getmondo/common/address/o;->a(Lco/uk/getmondo/common/address/n;)Lb/a/b;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/common/h/b/a$r;->c:Ljavax/a/a;

    .line 3625
    invoke-static {}, Lb/a/c;->a()Lb/a;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/common/h/b/a$r;->a:Lco/uk/getmondo/common/h/b/a;

    .line 3626
    invoke-static {v1}, Lco/uk/getmondo/common/h/b/a;->a(Lco/uk/getmondo/common/h/b/a;)Ljavax/a/a;

    move-result-object v1

    iget-object v2, p0, Lco/uk/getmondo/common/h/b/a$r;->a:Lco/uk/getmondo/common/h/b/a;

    .line 3627
    invoke-static {v2}, Lco/uk/getmondo/common/h/b/a;->b(Lco/uk/getmondo/common/h/b/a;)Ljavax/a/a;

    move-result-object v2

    iget-object v3, p0, Lco/uk/getmondo/common/h/b/a$r;->a:Lco/uk/getmondo/common/h/b/a;

    .line 3628
    invoke-static {v3}, Lco/uk/getmondo/common/h/b/a;->d(Lco/uk/getmondo/common/h/b/a;)Ljavax/a/a;

    move-result-object v3

    iget-object v4, p0, Lco/uk/getmondo/common/h/b/a$r;->a:Lco/uk/getmondo/common/h/b/a;

    .line 3629
    invoke-static {v4}, Lco/uk/getmondo/common/h/b/a;->c(Lco/uk/getmondo/common/h/b/a;)Ljavax/a/a;

    move-result-object v4

    iget-object v5, p0, Lco/uk/getmondo/common/h/b/a$r;->a:Lco/uk/getmondo/common/h/b/a;

    .line 3630
    invoke-static {v5}, Lco/uk/getmondo/common/h/b/a;->n(Lco/uk/getmondo/common/h/b/a;)Ljavax/a/a;

    move-result-object v5

    iget-object v6, p0, Lco/uk/getmondo/common/h/b/a$r;->a:Lco/uk/getmondo/common/h/b/a;

    .line 3631
    invoke-static {v6}, Lco/uk/getmondo/common/h/b/a;->f(Lco/uk/getmondo/common/h/b/a;)Ljavax/a/a;

    move-result-object v6

    iget-object v7, p0, Lco/uk/getmondo/common/h/b/a$r;->c:Ljavax/a/a;

    .line 3624
    invoke-static/range {v0 .. v7}, Lco/uk/getmondo/common/address/w;->a(Lb/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;)Lb/a/b;

    move-result-object v0

    .line 3623
    invoke-static {v0}, Lb/a/a;->a(Ljavax/a/a;)Ljavax/a/a;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/common/h/b/a$r;->d:Ljavax/a/a;

    .line 3634
    iget-object v0, p0, Lco/uk/getmondo/common/h/b/a$r;->d:Ljavax/a/a;

    .line 3635
    invoke-static {v0}, Lco/uk/getmondo/common/address/l;->a(Ljavax/a/a;)Lb/a;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/common/h/b/a$r;->e:Lb/a;

    .line 3637
    return-void
.end method


# virtual methods
.method public a(Lco/uk/getmondo/common/address/SelectAddressActivity;)V
    .locals 1

    .prologue
    .line 3646
    iget-object v0, p0, Lco/uk/getmondo/common/h/b/a$r;->e:Lb/a;

    invoke-interface {v0, p1}, Lb/a;->a(Ljava/lang/Object;)V

    .line 3647
    return-void
.end method
