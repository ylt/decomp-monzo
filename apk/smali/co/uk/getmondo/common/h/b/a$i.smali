.class final Lco/uk/getmondo/common/h/b/a$i;
.super Ljava/lang/Object;
.source "DaggerViewComponent.java"

# interfaces
.implements Lco/uk/getmondo/signup/identity_verification/o;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lco/uk/getmondo/common/h/b/a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "i"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lco/uk/getmondo/common/h/b/a$i$b;,
        Lco/uk/getmondo/common/h/b/a$i$a;
    }
.end annotation


# instance fields
.field final synthetic a:Lco/uk/getmondo/common/h/b/a;

.field private final b:Lco/uk/getmondo/signup/identity_verification/p;

.field private c:Ljavax/a/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/api/model/signup/SignupSource;",
            ">;"
        }
    .end annotation
.end field

.field private d:Ljavax/a/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/signup/identity_verification/a/e;",
            ">;"
        }
    .end annotation
.end field

.field private e:Ljavax/a/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/signup/identity_verification/video/a;",
            ">;"
        }
    .end annotation
.end field

.field private f:Ljavax/a/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/signup/identity_verification/a/a;",
            ">;"
        }
    .end annotation
.end field

.field private g:Ljavax/a/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/signup/identity_verification/video/ab;",
            ">;"
        }
    .end annotation
.end field

.field private h:Ljavax/a/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/signup/identity_verification/video/n;",
            ">;"
        }
    .end annotation
.end field

.field private i:Lb/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lb/a",
            "<",
            "Lco/uk/getmondo/signup/identity_verification/video/VideoRecordingActivity;",
            ">;"
        }
    .end annotation
.end field

.field private j:Ljavax/a/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/signup/identity_verification/aa;",
            ">;"
        }
    .end annotation
.end field

.field private k:Lb/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lb/a",
            "<",
            "Lco/uk/getmondo/signup/identity_verification/VerificationPendingActivity;",
            ">;"
        }
    .end annotation
.end field

.field private l:Ljavax/a/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/signup/identity_verification/fallback/h;",
            ">;"
        }
    .end annotation
.end field

.field private m:Lb/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lb/a",
            "<",
            "Lco/uk/getmondo/signup/identity_verification/fallback/VideoFallbackActivity;",
            ">;"
        }
    .end annotation
.end field

.field private n:Ljavax/a/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/signup/identity_verification/a/j;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Lco/uk/getmondo/common/h/b/a;Lco/uk/getmondo/signup/identity_verification/p;)V
    .locals 1

    .prologue
    .line 4049
    iput-object p1, p0, Lco/uk/getmondo/common/h/b/a$i;->a:Lco/uk/getmondo/common/h/b/a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 4050
    invoke-static {p2}, Lb/a/d;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/signup/identity_verification/p;

    iput-object v0, p0, Lco/uk/getmondo/common/h/b/a$i;->b:Lco/uk/getmondo/signup/identity_verification/p;

    .line 4051
    invoke-direct {p0}, Lco/uk/getmondo/common/h/b/a$i;->a()V

    .line 4052
    return-void
.end method

.method synthetic constructor <init>(Lco/uk/getmondo/common/h/b/a;Lco/uk/getmondo/signup/identity_verification/p;Lco/uk/getmondo/common/h/b/a$1;)V
    .locals 0

    .prologue
    .line 4021
    invoke-direct {p0, p1, p2}, Lco/uk/getmondo/common/h/b/a$i;-><init>(Lco/uk/getmondo/common/h/b/a;Lco/uk/getmondo/signup/identity_verification/p;)V

    return-void
.end method

.method static synthetic a(Lco/uk/getmondo/common/h/b/a$i;)Ljavax/a/a;
    .locals 1

    .prologue
    .line 4021
    iget-object v0, p0, Lco/uk/getmondo/common/h/b/a$i;->d:Ljavax/a/a;

    return-object v0
.end method

.method private a()V
    .locals 9

    .prologue
    .line 4057
    iget-object v0, p0, Lco/uk/getmondo/common/h/b/a$i;->b:Lco/uk/getmondo/signup/identity_verification/p;

    .line 4058
    invoke-static {v0}, Lco/uk/getmondo/signup/identity_verification/s;->a(Lco/uk/getmondo/signup/identity_verification/p;)Lb/a/b;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/common/h/b/a$i;->c:Ljavax/a/a;

    .line 4060
    iget-object v0, p0, Lco/uk/getmondo/common/h/b/a$i;->b:Lco/uk/getmondo/signup/identity_verification/p;

    iget-object v1, p0, Lco/uk/getmondo/common/h/b/a$i;->a:Lco/uk/getmondo/common/h/b/a;

    .line 4064
    invoke-static {v1}, Lco/uk/getmondo/common/h/b/a;->u(Lco/uk/getmondo/common/h/b/a;)Ljavax/a/a;

    move-result-object v1

    iget-object v2, p0, Lco/uk/getmondo/common/h/b/a$i;->a:Lco/uk/getmondo/common/h/b/a;

    .line 4065
    invoke-static {v2}, Lco/uk/getmondo/common/h/b/a;->v(Lco/uk/getmondo/common/h/b/a;)Ljavax/a/a;

    move-result-object v2

    iget-object v3, p0, Lco/uk/getmondo/common/h/b/a$i;->a:Lco/uk/getmondo/common/h/b/a;

    .line 4066
    invoke-static {v3}, Lco/uk/getmondo/common/h/b/a;->w(Lco/uk/getmondo/common/h/b/a;)Ljavax/a/a;

    move-result-object v3

    iget-object v4, p0, Lco/uk/getmondo/common/h/b/a$i;->a:Lco/uk/getmondo/common/h/b/a;

    .line 4067
    invoke-static {v4}, Lco/uk/getmondo/common/h/b/a;->x(Lco/uk/getmondo/common/h/b/a;)Ljavax/a/a;

    move-result-object v4

    iget-object v5, p0, Lco/uk/getmondo/common/h/b/a$i;->c:Ljavax/a/a;

    .line 4062
    invoke-static/range {v0 .. v5}, Lco/uk/getmondo/signup/identity_verification/q;->a(Lco/uk/getmondo/signup/identity_verification/p;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;)Lb/a/b;

    move-result-object v0

    .line 4061
    invoke-static {v0}, Lb/a/a;->a(Ljavax/a/a;)Ljavax/a/a;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/common/h/b/a$i;->d:Ljavax/a/a;

    .line 4070
    iget-object v0, p0, Lco/uk/getmondo/common/h/b/a$i;->a:Lco/uk/getmondo/common/h/b/a;

    .line 4071
    invoke-static {v0}, Lco/uk/getmondo/common/h/b/a;->f(Lco/uk/getmondo/common/h/b/a;)Ljavax/a/a;

    move-result-object v0

    invoke-static {v0}, Lco/uk/getmondo/signup/identity_verification/video/c;->a(Ljavax/a/a;)Lb/a/b;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/common/h/b/a$i;->e:Ljavax/a/a;

    .line 4073
    iget-object v0, p0, Lco/uk/getmondo/common/h/b/a$i;->a:Lco/uk/getmondo/common/h/b/a;

    .line 4074
    invoke-static {v0}, Lco/uk/getmondo/common/h/b/a;->y(Lco/uk/getmondo/common/h/b/a;)Ljavax/a/a;

    move-result-object v0

    invoke-static {v0}, Lco/uk/getmondo/signup/identity_verification/a/b;->a(Ljavax/a/a;)Lb/a/b;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/common/h/b/a$i;->f:Ljavax/a/a;

    .line 4076
    iget-object v0, p0, Lco/uk/getmondo/common/h/b/a$i;->a:Lco/uk/getmondo/common/h/b/a;

    .line 4078
    invoke-static {v0}, Lco/uk/getmondo/common/h/b/a;->y(Lco/uk/getmondo/common/h/b/a;)Ljavax/a/a;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/common/h/b/a$i;->a:Lco/uk/getmondo/common/h/b/a;

    .line 4079
    invoke-static {v1}, Lco/uk/getmondo/common/h/b/a;->d(Lco/uk/getmondo/common/h/b/a;)Ljavax/a/a;

    move-result-object v1

    .line 4077
    invoke-static {v0, v1}, Lco/uk/getmondo/signup/identity_verification/video/ac;->a(Ljavax/a/a;Ljavax/a/a;)Lb/a/b;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/common/h/b/a$i;->g:Ljavax/a/a;

    .line 4084
    invoke-static {}, Lb/a/c;->a()Lb/a;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/common/h/b/a$i;->a:Lco/uk/getmondo/common/h/b/a;

    .line 4085
    invoke-static {v1}, Lco/uk/getmondo/common/h/b/a;->a(Lco/uk/getmondo/common/h/b/a;)Ljavax/a/a;

    move-result-object v1

    iget-object v2, p0, Lco/uk/getmondo/common/h/b/a$i;->a:Lco/uk/getmondo/common/h/b/a;

    .line 4086
    invoke-static {v2}, Lco/uk/getmondo/common/h/b/a;->z(Lco/uk/getmondo/common/h/b/a;)Ljavax/a/a;

    move-result-object v2

    iget-object v3, p0, Lco/uk/getmondo/common/h/b/a$i;->d:Ljavax/a/a;

    iget-object v4, p0, Lco/uk/getmondo/common/h/b/a$i;->a:Lco/uk/getmondo/common/h/b/a;

    .line 4088
    invoke-static {v4}, Lco/uk/getmondo/common/h/b/a;->f(Lco/uk/getmondo/common/h/b/a;)Ljavax/a/a;

    move-result-object v4

    iget-object v5, p0, Lco/uk/getmondo/common/h/b/a$i;->a:Lco/uk/getmondo/common/h/b/a;

    .line 4089
    invoke-static {v5}, Lco/uk/getmondo/common/h/b/a;->A(Lco/uk/getmondo/common/h/b/a;)Ljavax/a/a;

    move-result-object v5

    iget-object v6, p0, Lco/uk/getmondo/common/h/b/a$i;->e:Ljavax/a/a;

    iget-object v7, p0, Lco/uk/getmondo/common/h/b/a$i;->f:Ljavax/a/a;

    iget-object v8, p0, Lco/uk/getmondo/common/h/b/a$i;->g:Ljavax/a/a;

    .line 4083
    invoke-static/range {v0 .. v8}, Lco/uk/getmondo/signup/identity_verification/video/aa;->a(Lb/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;)Lb/a/b;

    move-result-object v0

    .line 4082
    invoke-static {v0}, Lb/a/a;->a(Ljavax/a/a;)Ljavax/a/a;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/common/h/b/a$i;->h:Ljavax/a/a;

    .line 4094
    iget-object v0, p0, Lco/uk/getmondo/common/h/b/a$i;->h:Ljavax/a/a;

    .line 4095
    invoke-static {v0}, Lco/uk/getmondo/signup/identity_verification/video/m;->a(Ljavax/a/a;)Lb/a;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/common/h/b/a$i;->i:Lb/a;

    .line 4100
    invoke-static {}, Lb/a/c;->a()Lb/a;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/common/h/b/a$i;->a:Lco/uk/getmondo/common/h/b/a;

    .line 4101
    invoke-static {v1}, Lco/uk/getmondo/common/h/b/a;->b(Lco/uk/getmondo/common/h/b/a;)Ljavax/a/a;

    move-result-object v1

    iget-object v2, p0, Lco/uk/getmondo/common/h/b/a$i;->a:Lco/uk/getmondo/common/h/b/a;

    .line 4102
    invoke-static {v2}, Lco/uk/getmondo/common/h/b/a;->a(Lco/uk/getmondo/common/h/b/a;)Ljavax/a/a;

    move-result-object v2

    iget-object v3, p0, Lco/uk/getmondo/common/h/b/a$i;->d:Ljavax/a/a;

    iget-object v4, p0, Lco/uk/getmondo/common/h/b/a$i;->a:Lco/uk/getmondo/common/h/b/a;

    .line 4104
    invoke-static {v4}, Lco/uk/getmondo/common/h/b/a;->c(Lco/uk/getmondo/common/h/b/a;)Ljavax/a/a;

    move-result-object v4

    iget-object v5, p0, Lco/uk/getmondo/common/h/b/a$i;->a:Lco/uk/getmondo/common/h/b/a;

    .line 4105
    invoke-static {v5}, Lco/uk/getmondo/common/h/b/a;->d(Lco/uk/getmondo/common/h/b/a;)Ljavax/a/a;

    move-result-object v5

    .line 4099
    invoke-static/range {v0 .. v5}, Lco/uk/getmondo/signup/identity_verification/ad;->a(Lb/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;)Lb/a/b;

    move-result-object v0

    .line 4098
    invoke-static {v0}, Lb/a/a;->a(Ljavax/a/a;)Ljavax/a/a;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/common/h/b/a$i;->j:Ljavax/a/a;

    .line 4107
    iget-object v0, p0, Lco/uk/getmondo/common/h/b/a$i;->j:Ljavax/a/a;

    .line 4108
    invoke-static {v0}, Lco/uk/getmondo/signup/identity_verification/z;->a(Ljavax/a/a;)Lb/a;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/common/h/b/a$i;->k:Lb/a;

    .line 4113
    invoke-static {}, Lb/a/c;->a()Lb/a;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/common/h/b/a$i;->d:Ljavax/a/a;

    iget-object v2, p0, Lco/uk/getmondo/common/h/b/a$i;->g:Ljavax/a/a;

    iget-object v3, p0, Lco/uk/getmondo/common/h/b/a$i;->a:Lco/uk/getmondo/common/h/b/a;

    .line 4116
    invoke-static {v3}, Lco/uk/getmondo/common/h/b/a;->f(Lco/uk/getmondo/common/h/b/a;)Ljavax/a/a;

    move-result-object v3

    .line 4112
    invoke-static {v0, v1, v2, v3}, Lco/uk/getmondo/signup/identity_verification/fallback/j;->a(Lb/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;)Lb/a/b;

    move-result-object v0

    .line 4111
    invoke-static {v0}, Lb/a/a;->a(Ljavax/a/a;)Ljavax/a/a;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/common/h/b/a$i;->l:Ljavax/a/a;

    .line 4118
    iget-object v0, p0, Lco/uk/getmondo/common/h/b/a$i;->l:Ljavax/a/a;

    iget-object v1, p0, Lco/uk/getmondo/common/h/b/a$i;->f:Ljavax/a/a;

    .line 4119
    invoke-static {v0, v1}, Lco/uk/getmondo/signup/identity_verification/fallback/g;->a(Ljavax/a/a;Ljavax/a/a;)Lb/a;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/common/h/b/a$i;->m:Lb/a;

    .line 4122
    iget-object v0, p0, Lco/uk/getmondo/common/h/b/a$i;->b:Lco/uk/getmondo/signup/identity_verification/p;

    .line 4123
    invoke-static {v0}, Lco/uk/getmondo/signup/identity_verification/r;->a(Lco/uk/getmondo/signup/identity_verification/p;)Lb/a/b;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/common/h/b/a$i;->n:Ljavax/a/a;

    .line 4125
    return-void
.end method

.method static synthetic b(Lco/uk/getmondo/common/h/b/a$i;)Ljavax/a/a;
    .locals 1

    .prologue
    .line 4021
    iget-object v0, p0, Lco/uk/getmondo/common/h/b/a$i;->n:Ljavax/a/a;

    return-object v0
.end method

.method static synthetic c(Lco/uk/getmondo/common/h/b/a$i;)Ljavax/a/a;
    .locals 1

    .prologue
    .line 4021
    iget-object v0, p0, Lco/uk/getmondo/common/h/b/a$i;->c:Ljavax/a/a;

    return-object v0
.end method

.method static synthetic d(Lco/uk/getmondo/common/h/b/a$i;)Ljavax/a/a;
    .locals 1

    .prologue
    .line 4021
    iget-object v0, p0, Lco/uk/getmondo/common/h/b/a$i;->f:Ljavax/a/a;

    return-object v0
.end method


# virtual methods
.method public a(Lco/uk/getmondo/signup/identity_verification/j;)Lco/uk/getmondo/signup/identity_verification/i;
    .locals 2

    .prologue
    .line 4150
    new-instance v0, Lco/uk/getmondo/common/h/b/a$i$b;

    const/4 v1, 0x0

    invoke-direct {v0, p0, p1, v1}, Lco/uk/getmondo/common/h/b/a$i$b;-><init>(Lco/uk/getmondo/common/h/b/a$i;Lco/uk/getmondo/signup/identity_verification/j;Lco/uk/getmondo/common/h/b/a$1;)V

    return-object v0
.end method

.method public a(Lco/uk/getmondo/signup/identity_verification/id_picture/f;)Lco/uk/getmondo/signup/identity_verification/id_picture/e;
    .locals 2

    .prologue
    .line 4144
    new-instance v0, Lco/uk/getmondo/common/h/b/a$i$a;

    const/4 v1, 0x0

    invoke-direct {v0, p0, p1, v1}, Lco/uk/getmondo/common/h/b/a$i$a;-><init>(Lco/uk/getmondo/common/h/b/a$i;Lco/uk/getmondo/signup/identity_verification/id_picture/f;Lco/uk/getmondo/common/h/b/a$1;)V

    return-object v0
.end method

.method public a(Lco/uk/getmondo/signup/identity_verification/VerificationPendingActivity;)V
    .locals 1

    .prologue
    .line 4134
    iget-object v0, p0, Lco/uk/getmondo/common/h/b/a$i;->k:Lb/a;

    invoke-interface {v0, p1}, Lb/a;->a(Ljava/lang/Object;)V

    .line 4135
    return-void
.end method

.method public a(Lco/uk/getmondo/signup/identity_verification/fallback/VideoFallbackActivity;)V
    .locals 1

    .prologue
    .line 4139
    iget-object v0, p0, Lco/uk/getmondo/common/h/b/a$i;->m:Lb/a;

    invoke-interface {v0, p1}, Lb/a;->a(Ljava/lang/Object;)V

    .line 4140
    return-void
.end method

.method public a(Lco/uk/getmondo/signup/identity_verification/video/VideoRecordingActivity;)V
    .locals 1

    .prologue
    .line 4129
    iget-object v0, p0, Lco/uk/getmondo/common/h/b/a$i;->i:Lb/a;

    invoke-interface {v0, p1}, Lb/a;->a(Ljava/lang/Object;)V

    .line 4130
    return-void
.end method
