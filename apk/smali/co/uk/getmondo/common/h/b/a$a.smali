.class final Lco/uk/getmondo/common/h/b/a$a;
.super Ljava/lang/Object;
.source "DaggerViewComponent.java"

# interfaces
.implements Lco/uk/getmondo/card/activate/b;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lco/uk/getmondo/common/h/b/a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "a"
.end annotation


# instance fields
.field final synthetic a:Lco/uk/getmondo/common/h/b/a;

.field private final b:Lco/uk/getmondo/card/activate/c;

.field private c:Ljavax/a/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/a/a",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private d:Ljavax/a/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/card/activate/e;",
            ">;"
        }
    .end annotation
.end field

.field private e:Lb/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lb/a",
            "<",
            "Lco/uk/getmondo/card/activate/ActivateCardActivity;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Lco/uk/getmondo/common/h/b/a;Lco/uk/getmondo/card/activate/c;)V
    .locals 1

    .prologue
    .line 3661
    iput-object p1, p0, Lco/uk/getmondo/common/h/b/a$a;->a:Lco/uk/getmondo/common/h/b/a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 3662
    invoke-static {p2}, Lb/a/d;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/card/activate/c;

    iput-object v0, p0, Lco/uk/getmondo/common/h/b/a$a;->b:Lco/uk/getmondo/card/activate/c;

    .line 3663
    invoke-direct {p0}, Lco/uk/getmondo/common/h/b/a$a;->a()V

    .line 3664
    return-void
.end method

.method synthetic constructor <init>(Lco/uk/getmondo/common/h/b/a;Lco/uk/getmondo/card/activate/c;Lco/uk/getmondo/common/h/b/a$1;)V
    .locals 0

    .prologue
    .line 3650
    invoke-direct {p0, p1, p2}, Lco/uk/getmondo/common/h/b/a$a;-><init>(Lco/uk/getmondo/common/h/b/a;Lco/uk/getmondo/card/activate/c;)V

    return-void
.end method

.method private a()V
    .locals 9

    .prologue
    .line 3669
    iget-object v0, p0, Lco/uk/getmondo/common/h/b/a$a;->b:Lco/uk/getmondo/card/activate/c;

    .line 3670
    invoke-static {v0}, Lco/uk/getmondo/card/activate/d;->a(Lco/uk/getmondo/card/activate/c;)Lb/a/b;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/common/h/b/a$a;->c:Ljavax/a/a;

    .line 3675
    invoke-static {}, Lb/a/c;->a()Lb/a;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/common/h/b/a$a;->a:Lco/uk/getmondo/common/h/b/a;

    .line 3676
    invoke-static {v1}, Lco/uk/getmondo/common/h/b/a;->a(Lco/uk/getmondo/common/h/b/a;)Ljavax/a/a;

    move-result-object v1

    iget-object v2, p0, Lco/uk/getmondo/common/h/b/a$a;->a:Lco/uk/getmondo/common/h/b/a;

    .line 3677
    invoke-static {v2}, Lco/uk/getmondo/common/h/b/a;->b(Lco/uk/getmondo/common/h/b/a;)Ljavax/a/a;

    move-result-object v2

    iget-object v3, p0, Lco/uk/getmondo/common/h/b/a$a;->a:Lco/uk/getmondo/common/h/b/a;

    .line 3678
    invoke-static {v3}, Lco/uk/getmondo/common/h/b/a;->d(Lco/uk/getmondo/common/h/b/a;)Ljavax/a/a;

    move-result-object v3

    iget-object v4, p0, Lco/uk/getmondo/common/h/b/a$a;->a:Lco/uk/getmondo/common/h/b/a;

    .line 3679
    invoke-static {v4}, Lco/uk/getmondo/common/h/b/a;->c(Lco/uk/getmondo/common/h/b/a;)Ljavax/a/a;

    move-result-object v4

    iget-object v5, p0, Lco/uk/getmondo/common/h/b/a$a;->a:Lco/uk/getmondo/common/h/b/a;

    .line 3680
    invoke-static {v5}, Lco/uk/getmondo/common/h/b/a;->o(Lco/uk/getmondo/common/h/b/a;)Ljavax/a/a;

    move-result-object v5

    iget-object v6, p0, Lco/uk/getmondo/common/h/b/a$a;->a:Lco/uk/getmondo/common/h/b/a;

    .line 3681
    invoke-static {v6}, Lco/uk/getmondo/common/h/b/a;->f(Lco/uk/getmondo/common/h/b/a;)Ljavax/a/a;

    move-result-object v6

    iget-object v7, p0, Lco/uk/getmondo/common/h/b/a$a;->a:Lco/uk/getmondo/common/h/b/a;

    .line 3682
    invoke-static {v7}, Lco/uk/getmondo/common/h/b/a;->p(Lco/uk/getmondo/common/h/b/a;)Ljavax/a/a;

    move-result-object v7

    iget-object v8, p0, Lco/uk/getmondo/common/h/b/a$a;->c:Ljavax/a/a;

    .line 3674
    invoke-static/range {v0 .. v8}, Lco/uk/getmondo/card/activate/o;->a(Lb/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;)Lb/a/b;

    move-result-object v0

    .line 3673
    invoke-static {v0}, Lb/a/a;->a(Ljavax/a/a;)Ljavax/a/a;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/common/h/b/a$a;->d:Ljavax/a/a;

    .line 3685
    iget-object v0, p0, Lco/uk/getmondo/common/h/b/a$a;->d:Ljavax/a/a;

    .line 3686
    invoke-static {v0}, Lco/uk/getmondo/card/activate/a;->a(Ljavax/a/a;)Lb/a;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/common/h/b/a$a;->e:Lb/a;

    .line 3688
    return-void
.end method


# virtual methods
.method public a(Lco/uk/getmondo/card/activate/ActivateCardActivity;)V
    .locals 1

    .prologue
    .line 3697
    iget-object v0, p0, Lco/uk/getmondo/common/h/b/a$a;->e:Lb/a;

    invoke-interface {v0, p1}, Lb/a;->a(Ljava/lang/Object;)V

    .line 3698
    return-void
.end method
