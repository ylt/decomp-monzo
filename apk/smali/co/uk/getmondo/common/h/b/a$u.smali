.class final Lco/uk/getmondo/common/h/b/a$u;
.super Ljava/lang/Object;
.source "DaggerViewComponent.java"

# interfaces
.implements Lco/uk/getmondo/transaction/c;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lco/uk/getmondo/common/h/b/a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "u"
.end annotation


# instance fields
.field final synthetic a:Lco/uk/getmondo/common/h/b/a;

.field private final b:Lco/uk/getmondo/transaction/d;

.field private c:Ljavax/a/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/a/a",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private d:Ljavax/a/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/transaction/a;",
            ">;"
        }
    .end annotation
.end field

.field private e:Ljavax/a/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/transaction/details/b;",
            ">;"
        }
    .end annotation
.end field

.field private f:Lb/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lb/a",
            "<",
            "Lco/uk/getmondo/transaction/details/TransactionDetailsActivity;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Lco/uk/getmondo/common/h/b/a;Lco/uk/getmondo/transaction/d;)V
    .locals 1

    .prologue
    .line 3556
    iput-object p1, p0, Lco/uk/getmondo/common/h/b/a$u;->a:Lco/uk/getmondo/common/h/b/a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 3557
    invoke-static {p2}, Lb/a/d;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/transaction/d;

    iput-object v0, p0, Lco/uk/getmondo/common/h/b/a$u;->b:Lco/uk/getmondo/transaction/d;

    .line 3558
    invoke-direct {p0}, Lco/uk/getmondo/common/h/b/a$u;->a()V

    .line 3559
    return-void
.end method

.method synthetic constructor <init>(Lco/uk/getmondo/common/h/b/a;Lco/uk/getmondo/transaction/d;Lco/uk/getmondo/common/h/b/a$1;)V
    .locals 0

    .prologue
    .line 3545
    invoke-direct {p0, p1, p2}, Lco/uk/getmondo/common/h/b/a$u;-><init>(Lco/uk/getmondo/common/h/b/a;Lco/uk/getmondo/transaction/d;)V

    return-void
.end method

.method private a()V
    .locals 11

    .prologue
    .line 3564
    iget-object v0, p0, Lco/uk/getmondo/common/h/b/a$u;->b:Lco/uk/getmondo/transaction/d;

    .line 3565
    invoke-static {v0}, Lco/uk/getmondo/transaction/e;->a(Lco/uk/getmondo/transaction/d;)Lb/a/b;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/common/h/b/a$u;->c:Ljavax/a/a;

    .line 3567
    iget-object v0, p0, Lco/uk/getmondo/common/h/b/a$u;->a:Lco/uk/getmondo/common/h/b/a;

    .line 3568
    invoke-static {v0}, Lco/uk/getmondo/common/h/b/a;->j(Lco/uk/getmondo/common/h/b/a;)Ljavax/a/a;

    move-result-object v0

    invoke-static {v0}, Lco/uk/getmondo/transaction/b;->a(Ljavax/a/a;)Lb/a/b;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/common/h/b/a$u;->d:Ljavax/a/a;

    .line 3573
    invoke-static {}, Lb/a/c;->a()Lb/a;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/common/h/b/a$u;->a:Lco/uk/getmondo/common/h/b/a;

    .line 3574
    invoke-static {v1}, Lco/uk/getmondo/common/h/b/a;->a(Lco/uk/getmondo/common/h/b/a;)Ljavax/a/a;

    move-result-object v1

    iget-object v2, p0, Lco/uk/getmondo/common/h/b/a$u;->a:Lco/uk/getmondo/common/h/b/a;

    .line 3575
    invoke-static {v2}, Lco/uk/getmondo/common/h/b/a;->b(Lco/uk/getmondo/common/h/b/a;)Ljavax/a/a;

    move-result-object v2

    iget-object v3, p0, Lco/uk/getmondo/common/h/b/a$u;->c:Ljavax/a/a;

    iget-object v4, p0, Lco/uk/getmondo/common/h/b/a$u;->a:Lco/uk/getmondo/common/h/b/a;

    .line 3577
    invoke-static {v4}, Lco/uk/getmondo/common/h/b/a;->k(Lco/uk/getmondo/common/h/b/a;)Ljavax/a/a;

    move-result-object v4

    iget-object v5, p0, Lco/uk/getmondo/common/h/b/a$u;->a:Lco/uk/getmondo/common/h/b/a;

    .line 3578
    invoke-static {v5}, Lco/uk/getmondo/common/h/b/a;->l(Lco/uk/getmondo/common/h/b/a;)Ljavax/a/a;

    move-result-object v5

    iget-object v6, p0, Lco/uk/getmondo/common/h/b/a$u;->a:Lco/uk/getmondo/common/h/b/a;

    .line 3579
    invoke-static {v6}, Lco/uk/getmondo/common/h/b/a;->c(Lco/uk/getmondo/common/h/b/a;)Ljavax/a/a;

    move-result-object v6

    iget-object v7, p0, Lco/uk/getmondo/common/h/b/a$u;->d:Ljavax/a/a;

    iget-object v8, p0, Lco/uk/getmondo/common/h/b/a$u;->a:Lco/uk/getmondo/common/h/b/a;

    .line 3581
    invoke-static {v8}, Lco/uk/getmondo/common/h/b/a;->m(Lco/uk/getmondo/common/h/b/a;)Ljavax/a/a;

    move-result-object v8

    iget-object v9, p0, Lco/uk/getmondo/common/h/b/a$u;->a:Lco/uk/getmondo/common/h/b/a;

    .line 3582
    invoke-static {v9}, Lco/uk/getmondo/common/h/b/a;->f(Lco/uk/getmondo/common/h/b/a;)Ljavax/a/a;

    move-result-object v9

    iget-object v10, p0, Lco/uk/getmondo/common/h/b/a$u;->a:Lco/uk/getmondo/common/h/b/a;

    .line 3583
    invoke-static {v10}, Lco/uk/getmondo/common/h/b/a;->h(Lco/uk/getmondo/common/h/b/a;)Ljavax/a/a;

    move-result-object v10

    .line 3572
    invoke-static/range {v0 .. v10}, Lco/uk/getmondo/transaction/details/d;->a(Lb/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;)Lb/a/b;

    move-result-object v0

    .line 3571
    invoke-static {v0}, Lb/a/a;->a(Ljavax/a/a;)Ljavax/a/a;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/common/h/b/a$u;->e:Ljavax/a/a;

    .line 3585
    iget-object v0, p0, Lco/uk/getmondo/common/h/b/a$u;->e:Ljavax/a/a;

    .line 3586
    invoke-static {v0}, Lco/uk/getmondo/transaction/details/a;->a(Ljavax/a/a;)Lb/a;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/common/h/b/a$u;->f:Lb/a;

    .line 3587
    return-void
.end method


# virtual methods
.method public a(Lco/uk/getmondo/transaction/details/TransactionDetailsActivity;)V
    .locals 1

    .prologue
    .line 3596
    iget-object v0, p0, Lco/uk/getmondo/common/h/b/a$u;->f:Lb/a;

    invoke-interface {v0, p1}, Lb/a;->a(Ljava/lang/Object;)V

    .line 3597
    return-void
.end method
