.class final Lco/uk/getmondo/common/h/b/a$k;
.super Ljava/lang/Object;
.source "DaggerViewComponent.java"

# interfaces
.implements Lco/uk/getmondo/monzo/me/deeplink/b;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lco/uk/getmondo/common/h/b/a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "k"
.end annotation


# instance fields
.field final synthetic a:Lco/uk/getmondo/common/h/b/a;

.field private final b:Lco/uk/getmondo/monzo/me/deeplink/c;

.field private c:Ljavax/a/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/a/a",
            "<",
            "Landroid/net/Uri;",
            ">;"
        }
    .end annotation
.end field

.field private d:Ljavax/a/a;

.field private e:Lb/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lb/a",
            "<",
            "Lco/uk/getmondo/monzo/me/deeplink/MonzoMeActivity;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Lco/uk/getmondo/common/h/b/a;Lco/uk/getmondo/monzo/me/deeplink/c;)V
    .locals 1

    .prologue
    .line 3839
    iput-object p1, p0, Lco/uk/getmondo/common/h/b/a$k;->a:Lco/uk/getmondo/common/h/b/a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 3840
    invoke-static {p2}, Lb/a/d;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/monzo/me/deeplink/c;

    iput-object v0, p0, Lco/uk/getmondo/common/h/b/a$k;->b:Lco/uk/getmondo/monzo/me/deeplink/c;

    .line 3841
    invoke-direct {p0}, Lco/uk/getmondo/common/h/b/a$k;->a()V

    .line 3842
    return-void
.end method

.method synthetic constructor <init>(Lco/uk/getmondo/common/h/b/a;Lco/uk/getmondo/monzo/me/deeplink/c;Lco/uk/getmondo/common/h/b/a$1;)V
    .locals 0

    .prologue
    .line 3829
    invoke-direct {p0, p1, p2}, Lco/uk/getmondo/common/h/b/a$k;-><init>(Lco/uk/getmondo/common/h/b/a;Lco/uk/getmondo/monzo/me/deeplink/c;)V

    return-void
.end method

.method private a()V
    .locals 3

    .prologue
    .line 3847
    iget-object v0, p0, Lco/uk/getmondo/common/h/b/a$k;->b:Lco/uk/getmondo/monzo/me/deeplink/c;

    invoke-static {v0}, Lco/uk/getmondo/monzo/me/deeplink/e;->a(Lco/uk/getmondo/monzo/me/deeplink/c;)Lb/a/b;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/common/h/b/a$k;->c:Ljavax/a/a;

    .line 3849
    iget-object v0, p0, Lco/uk/getmondo/common/h/b/a$k;->b:Lco/uk/getmondo/monzo/me/deeplink/c;

    iget-object v1, p0, Lco/uk/getmondo/common/h/b/a$k;->a:Lco/uk/getmondo/common/h/b/a;

    .line 3853
    invoke-static {v1}, Lco/uk/getmondo/common/h/b/a;->d(Lco/uk/getmondo/common/h/b/a;)Ljavax/a/a;

    move-result-object v1

    iget-object v2, p0, Lco/uk/getmondo/common/h/b/a$k;->a:Lco/uk/getmondo/common/h/b/a;

    .line 3854
    invoke-static {v2}, Lco/uk/getmondo/common/h/b/a;->m(Lco/uk/getmondo/common/h/b/a;)Ljavax/a/a;

    move-result-object v2

    .line 3851
    invoke-static {v0, v1, v2}, Lco/uk/getmondo/monzo/me/deeplink/d;->a(Lco/uk/getmondo/monzo/me/deeplink/c;Ljavax/a/a;Ljavax/a/a;)Lb/a/b;

    move-result-object v0

    .line 3850
    invoke-static {v0}, Lb/a/a;->a(Ljavax/a/a;)Ljavax/a/a;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/common/h/b/a$k;->d:Ljavax/a/a;

    .line 3856
    iget-object v0, p0, Lco/uk/getmondo/common/h/b/a$k;->d:Ljavax/a/a;

    .line 3857
    invoke-static {v0}, Lco/uk/getmondo/monzo/me/deeplink/a;->a(Ljavax/a/a;)Lb/a;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/common/h/b/a$k;->e:Lb/a;

    .line 3858
    return-void
.end method


# virtual methods
.method public a(Lco/uk/getmondo/monzo/me/deeplink/MonzoMeActivity;)V
    .locals 1

    .prologue
    .line 3867
    iget-object v0, p0, Lco/uk/getmondo/common/h/b/a$k;->e:Lb/a;

    invoke-interface {v0, p1}, Lb/a;->a(Ljava/lang/Object;)V

    .line 3868
    return-void
.end method
