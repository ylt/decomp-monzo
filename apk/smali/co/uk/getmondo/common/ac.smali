.class public final Lco/uk/getmondo/common/ac;
.super Ljava/lang/Object;
.source "SharedPeferencesExt.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000\u0016\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\u0008\u0002\u001a(\u0010\u0000\u001a\u0014\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00040\u00020\u0001*\u00020\u00032\n\u0008\u0002\u0010\u0005\u001a\u0004\u0018\u00010\u0004\u00a8\u0006\u0006"
    }
    d2 = {
        "changes",
        "Lio/reactivex/Observable;",
        "Lkotlin/Pair;",
        "Landroid/content/SharedPreferences;",
        "",
        "filterKey",
        "app_monzoPrepaidRelease"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# direct methods
.method public static final a(Landroid/content/SharedPreferences;Ljava/lang/String;)Lio/reactivex/n;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/SharedPreferences;",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/n",
            "<",
            "Lkotlin/h",
            "<",
            "Landroid/content/SharedPreferences;",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation

    .prologue
    const-string v0, "$receiver"

    invoke-static {p0, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 7
    new-instance v0, Lco/uk/getmondo/common/ac$a;

    invoke-direct {v0, p0, p1}, Lco/uk/getmondo/common/ac$a;-><init>(Landroid/content/SharedPreferences;Ljava/lang/String;)V

    check-cast v0, Lio/reactivex/p;

    invoke-static {v0}, Lio/reactivex/n;->create(Lio/reactivex/p;)Lio/reactivex/n;

    move-result-object v0

    const-string v1, "Observable.create { emit\u2026istener(listener) }\n    }"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method
