.class public Lco/uk/getmondo/common/m;
.super Ljava/lang/Object;
.source "FacebookService.java"


# instance fields
.field private final a:Landroid/app/Application;

.field private b:Lcom/facebook/appevents/AppEventsLogger;


# direct methods
.method constructor <init>(Landroid/app/Application;)V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    iput-object p1, p0, Lco/uk/getmondo/common/m;->a:Landroid/app/Application;

    .line 18
    return-void
.end method

.method private c()Lcom/facebook/appevents/AppEventsLogger;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lco/uk/getmondo/common/m;->b:Lcom/facebook/appevents/AppEventsLogger;

    if-nez v0, :cond_0

    .line 34
    iget-object v0, p0, Lco/uk/getmondo/common/m;->a:Landroid/app/Application;

    invoke-static {v0}, Lcom/facebook/appevents/AppEventsLogger;->newLogger(Landroid/content/Context;)Lcom/facebook/appevents/AppEventsLogger;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/common/m;->b:Lcom/facebook/appevents/AppEventsLogger;

    .line 36
    :cond_0
    iget-object v0, p0, Lco/uk/getmondo/common/m;->b:Lcom/facebook/appevents/AppEventsLogger;

    return-object v0
.end method


# virtual methods
.method public a()V
    .locals 3

    .prologue
    .line 21
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 22
    const-string v1, "fb_registration_method"

    const-string v2, "Email"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 23
    invoke-direct {p0}, Lco/uk/getmondo/common/m;->c()Lcom/facebook/appevents/AppEventsLogger;

    move-result-object v1

    const-string v2, "fb_mobile_complete_registration"

    invoke-virtual {v1, v2, v0}, Lcom/facebook/appevents/AppEventsLogger;->logEvent(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 24
    return-void
.end method

.method public b()V
    .locals 3

    .prologue
    .line 27
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 28
    const-string v1, "fb_success"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 29
    invoke-direct {p0}, Lco/uk/getmondo/common/m;->c()Lcom/facebook/appevents/AppEventsLogger;

    move-result-object v1

    const-string v2, "fb_mobile_add_payment_info"

    invoke-virtual {v1, v2, v0}, Lcom/facebook/appevents/AppEventsLogger;->logEvent(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 30
    return-void
.end method
