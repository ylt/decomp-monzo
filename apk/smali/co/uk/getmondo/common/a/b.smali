.class public abstract enum Lco/uk/getmondo/common/a/b;
.super Ljava/lang/Enum;
.source "AppShortcut.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lco/uk/getmondo/common/a/b$b;,
        Lco/uk/getmondo/common/a/b$d;,
        Lco/uk/getmondo/common/a/b$c;,
        Lco/uk/getmondo/common/a/b$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lco/uk/getmondo/common/a/b;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000&\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\n\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0005\u0008\u0086\u0001\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00000\u0001B+\u0008\u0002\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0008\u0008\u0001\u0010\u0004\u001a\u00020\u0005\u0012\u0008\u0008\u0001\u0010\u0006\u001a\u00020\u0005\u0012\u0006\u0010\u0007\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0008J\u0010\u0010\u000f\u001a\u00020\u00102\u0006\u0010\u0011\u001a\u00020\u0012H&R\u0011\u0010\u0007\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\t\u0010\nR\u0011\u0010\u0006\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000b\u0010\u000cR\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\r\u0010\nR\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000e\u0010\u000cj\u0002\u0008\u0013j\u0002\u0008\u0014j\u0002\u0008\u0015j\u0002\u0008\u0016\u00a8\u0006\u0017"
    }
    d2 = {
        "Lco/uk/getmondo/common/app_shortcuts/AppShortcut;",
        "",
        "id",
        "",
        "shortLabel",
        "",
        "icon",
        "analyticEvent",
        "(Ljava/lang/String;ILjava/lang/String;IILjava/lang/String;)V",
        "getAnalyticEvent",
        "()Ljava/lang/String;",
        "getIcon",
        "()I",
        "getId",
        "getShortLabel",
        "getIntent",
        "Landroid/content/Intent;",
        "context",
        "Landroid/content/Context;",
        "SEND_MONEY",
        "TOP_UP",
        "SPENDING",
        "CHAT",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# static fields
.field public static final enum a:Lco/uk/getmondo/common/a/b;

.field public static final enum b:Lco/uk/getmondo/common/a/b;

.field public static final enum c:Lco/uk/getmondo/common/a/b;

.field public static final enum d:Lco/uk/getmondo/common/a/b;

.field private static final synthetic e:[Lco/uk/getmondo/common/a/b;


# instance fields
.field private final f:Ljava/lang/String;

.field private final g:I

.field private final h:I

.field private final i:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    const/4 v0, 0x4

    new-array v0, v0, [Lco/uk/getmondo/common/a/b;

    new-instance v1, Lco/uk/getmondo/common/a/b$b;

    const-string v2, "SEND_MONEY"

    invoke-direct {v1, v2, v3}, Lco/uk/getmondo/common/a/b$b;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lco/uk/getmondo/common/a/b;->a:Lco/uk/getmondo/common/a/b;

    aput-object v1, v0, v3

    new-instance v1, Lco/uk/getmondo/common/a/b$d;

    const-string v2, "TOP_UP"

    invoke-direct {v1, v2, v4}, Lco/uk/getmondo/common/a/b$d;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lco/uk/getmondo/common/a/b;->b:Lco/uk/getmondo/common/a/b;

    aput-object v1, v0, v4

    new-instance v1, Lco/uk/getmondo/common/a/b$c;

    const-string v2, "SPENDING"

    invoke-direct {v1, v2, v5}, Lco/uk/getmondo/common/a/b$c;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lco/uk/getmondo/common/a/b;->c:Lco/uk/getmondo/common/a/b;

    aput-object v1, v0, v5

    new-instance v1, Lco/uk/getmondo/common/a/b$a;

    const-string v2, "CHAT"

    invoke-direct {v1, v2, v6}, Lco/uk/getmondo/common/a/b$a;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lco/uk/getmondo/common/a/b;->d:Lco/uk/getmondo/common/a/b;

    aput-object v1, v0, v6

    sput-object v0, Lco/uk/getmondo/common/a/b;->e:[Lco/uk/getmondo/common/a/b;

    return-void
.end method

.method protected constructor <init>(Ljava/lang/String;ILjava/lang/String;IILjava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "II",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    const-string v0, "id"

    invoke-static {p3, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "analyticEvent"

    invoke-static {p6, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 15
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput-object p3, p0, Lco/uk/getmondo/common/a/b;->f:Ljava/lang/String;

    iput p4, p0, Lco/uk/getmondo/common/a/b;->g:I

    iput p5, p0, Lco/uk/getmondo/common/a/b;->h:I

    iput-object p6, p0, Lco/uk/getmondo/common/a/b;->i:Ljava/lang/String;

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lco/uk/getmondo/common/a/b;
    .locals 1

    const-class v0, Lco/uk/getmondo/common/a/b;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/common/a/b;

    return-object v0
.end method

.method public static values()[Lco/uk/getmondo/common/a/b;
    .locals 1

    sget-object v0, Lco/uk/getmondo/common/a/b;->e:[Lco/uk/getmondo/common/a/b;

    invoke-virtual {v0}, [Lco/uk/getmondo/common/a/b;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lco/uk/getmondo/common/a/b;

    return-object v0
.end method


# virtual methods
.method public abstract a(Landroid/content/Context;)Landroid/content/Intent;
.end method

.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 16
    iget-object v0, p0, Lco/uk/getmondo/common/a/b;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 17
    iget v0, p0, Lco/uk/getmondo/common/a/b;->g:I

    return v0
.end method

.method public final c()I
    .locals 1

    .prologue
    .line 18
    iget v0, p0, Lco/uk/getmondo/common/a/b;->h:I

    return v0
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 19
    iget-object v0, p0, Lco/uk/getmondo/common/a/b;->i:Ljava/lang/String;

    return-object v0
.end method
