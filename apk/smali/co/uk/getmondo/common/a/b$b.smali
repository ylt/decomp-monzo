.class public final Lco/uk/getmondo/common/a/b$b;
.super Lco/uk/getmondo/common/a/b;
.source "AppShortcut.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lco/uk/getmondo/common/a/b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "b"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0008\u00c6\u0001\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u0010\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006H\u0016\u00a8\u0006\u0007"
    }
    d2 = {
        "Lco/uk/getmondo/common/app_shortcuts/AppShortcut$SEND_MONEY;",
        "Lco/uk/getmondo/common/app_shortcuts/AppShortcut;",
        "(Ljava/lang/String;I)V",
        "getIntent",
        "Landroid/content/Intent;",
        "context",
        "Landroid/content/Context;",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# direct methods
.method constructor <init>(Ljava/lang/String;I)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 21
    const-string v3, "id_send"

    sget-object v0, Lco/uk/getmondo/a;->c:Ljava/lang/Boolean;

    const-string v1, "BuildConfig.BANK"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    const v4, 0x7f0a00ed

    :goto_0
    const v5, 0x7f020054

    const-string v6, "send_money"

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    invoke-direct/range {v0 .. v6}, Lco/uk/getmondo/common/a/b;-><init>(Ljava/lang/String;ILjava/lang/String;IILjava/lang/String;)V

    return-void

    :cond_0
    const v4, 0x7f0a02f4

    goto :goto_0
.end method


# virtual methods
.method public a(Landroid/content/Context;)Landroid/content/Intent;
    .locals 3

    .prologue
    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 23
    sget-object v0, Lco/uk/getmondo/main/HomeActivity;->f:Lco/uk/getmondo/main/HomeActivity$a;

    sget-object v1, Lco/uk/getmondo/main/g;->d:Lco/uk/getmondo/main/g;

    sget-object v2, Lco/uk/getmondo/common/a/b;->a:Lco/uk/getmondo/common/a/b;

    invoke-virtual {v0, p1, v1, v2}, Lco/uk/getmondo/main/HomeActivity$a;->a(Landroid/content/Context;Lco/uk/getmondo/main/g;Lco/uk/getmondo/common/a/b;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method
