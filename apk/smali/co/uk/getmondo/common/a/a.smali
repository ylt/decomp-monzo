.class public final Lco/uk/getmondo/common/a/a;
.super Ljava/lang/Object;
.source "Api25AppShortcuts.kt"

# interfaces
.implements Lco/uk/getmondo/common/a/c;


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x19
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000\\\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u000e\n\u0002\u0008\u0002\u0008\u0007\u0018\u00002\u00020\u0001B5\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b\u0012\u0006\u0010\u000c\u001a\u00020\r\u00a2\u0006\u0002\u0010\u000eJ\u0018\u0010\u0012\u001a\u00020\u00132\u0006\u0010\u0014\u001a\u00020\u00152\u0006\u0010\u0002\u001a\u00020\u0003H\u0002J\u0008\u0010\u0016\u001a\u00020\u0017H\u0016J\u0012\u0010\u0018\u001a\u00020\u00172\u0008\u0010\u0019\u001a\u0004\u0018\u00010\u001aH\u0002J\u0010\u0010\u001b\u001a\u00020\u00172\u0006\u0010\u001c\u001a\u00020\u0015H\u0016J\u0018\u0010\u001b\u001a\u00020\u00172\u0006\u0010\u001d\u001a\u00020\u001e2\u0006\u0010\u001f\u001a\u00020\u001eH\u0016R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000c\u001a\u00020\rX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0016\u0010\u000f\u001a\n \u0011*\u0004\u0018\u00010\u00100\u0010X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006 "
    }
    d2 = {
        "Lco/uk/getmondo/common/app_shortcuts/Api25AppShortcuts;",
        "Lco/uk/getmondo/common/app_shortcuts/AppShortcuts;",
        "context",
        "Landroid/content/Context;",
        "userSettingsStorage",
        "Lco/uk/getmondo/payments/send/data/UserSettingsStorage;",
        "userSettingsRepository",
        "Lco/uk/getmondo/payments/send/data/UserSettingsRepository;",
        "accountService",
        "Lco/uk/getmondo/common/accounts/AccountService;",
        "androidAccountManager",
        "Lco/uk/getmondo/common/accounts/AndroidAccountManager;",
        "analyticsService",
        "Lco/uk/getmondo/common/AnalyticsService;",
        "(Landroid/content/Context;Lco/uk/getmondo/payments/send/data/UserSettingsStorage;Lco/uk/getmondo/payments/send/data/UserSettingsRepository;Lco/uk/getmondo/common/accounts/AccountService;Lco/uk/getmondo/common/accounts/AndroidAccountManager;Lco/uk/getmondo/common/AnalyticsService;)V",
        "shortcutManager",
        "Landroid/content/pm/ShortcutManager;",
        "kotlin.jvm.PlatformType",
        "buildInfo",
        "Landroid/content/pm/ShortcutInfo;",
        "shortcut",
        "Lco/uk/getmondo/common/app_shortcuts/AppShortcut;",
        "initialise",
        "",
        "refresh",
        "user",
        "Lco/uk/getmondo/model/User;",
        "reportUsed",
        "appShortcut",
        "appShortcutId",
        "",
        "analyticEvent",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field private final a:Landroid/content/pm/ShortcutManager;

.field private final b:Landroid/content/Context;

.field private final c:Lco/uk/getmondo/payments/send/data/p;

.field private final d:Lco/uk/getmondo/payments/send/data/h;

.field private final e:Lco/uk/getmondo/common/accounts/d;

.field private final f:Lco/uk/getmondo/common/accounts/k;

.field private final g:Lco/uk/getmondo/common/a;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lco/uk/getmondo/payments/send/data/p;Lco/uk/getmondo/payments/send/data/h;Lco/uk/getmondo/common/accounts/d;Lco/uk/getmondo/common/accounts/k;Lco/uk/getmondo/common/a;)V
    .locals 2

    .prologue
    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "userSettingsStorage"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "userSettingsRepository"

    invoke-static {p3, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "accountService"

    invoke-static {p4, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "androidAccountManager"

    invoke-static {p5, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "analyticsService"

    invoke-static {p6, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lco/uk/getmondo/common/a/a;->b:Landroid/content/Context;

    iput-object p2, p0, Lco/uk/getmondo/common/a/a;->c:Lco/uk/getmondo/payments/send/data/p;

    iput-object p3, p0, Lco/uk/getmondo/common/a/a;->d:Lco/uk/getmondo/payments/send/data/h;

    iput-object p4, p0, Lco/uk/getmondo/common/a/a;->e:Lco/uk/getmondo/common/accounts/d;

    iput-object p5, p0, Lco/uk/getmondo/common/a/a;->f:Lco/uk/getmondo/common/accounts/k;

    iput-object p6, p0, Lco/uk/getmondo/common/a/a;->g:Lco/uk/getmondo/common/a;

    .line 33
    iget-object v0, p0, Lco/uk/getmondo/common/a/a;->b:Landroid/content/Context;

    const-class v1, Landroid/content/pm/ShortcutManager;

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/ShortcutManager;

    iput-object v0, p0, Lco/uk/getmondo/common/a/a;->a:Landroid/content/pm/ShortcutManager;

    return-void
.end method

.method private final a(Lco/uk/getmondo/common/a/b;Landroid/content/Context;)Landroid/content/pm/ShortcutInfo;
    .locals 2

    .prologue
    .line 78
    new-instance v1, Landroid/content/pm/ShortcutInfo$Builder;

    invoke-virtual {p1}, Lco/uk/getmondo/common/a/b;->a()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, p2, v0}, Landroid/content/pm/ShortcutInfo$Builder;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 79
    invoke-virtual {p1}, Lco/uk/getmondo/common/a/b;->b()I

    move-result v0

    invoke-virtual {p2, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-virtual {v1, v0}, Landroid/content/pm/ShortcutInfo$Builder;->setShortLabel(Ljava/lang/CharSequence;)Landroid/content/pm/ShortcutInfo$Builder;

    move-result-object v0

    .line 80
    invoke-virtual {p1}, Lco/uk/getmondo/common/a/b;->c()I

    move-result v1

    invoke-static {p2, v1}, Landroid/graphics/drawable/Icon;->createWithResource(Landroid/content/Context;I)Landroid/graphics/drawable/Icon;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/pm/ShortcutInfo$Builder;->setIcon(Landroid/graphics/drawable/Icon;)Landroid/content/pm/ShortcutInfo$Builder;

    move-result-object v0

    .line 81
    invoke-virtual {p1, p2}, Lco/uk/getmondo/common/a/b;->a(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/pm/ShortcutInfo$Builder;->setIntent(Landroid/content/Intent;)Landroid/content/pm/ShortcutInfo$Builder;

    move-result-object v0

    .line 82
    invoke-virtual {v0}, Landroid/content/pm/ShortcutInfo$Builder;->build()Landroid/content/pm/ShortcutInfo;

    move-result-object v0

    const-string v1, "ShortcutInfo.Builder(con\u2026\n                .build()"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public static final synthetic a(Lco/uk/getmondo/common/a/a;)Lco/uk/getmondo/common/accounts/k;
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, Lco/uk/getmondo/common/a/a;->f:Lco/uk/getmondo/common/accounts/k;

    return-object v0
.end method

.method public static final synthetic a(Lco/uk/getmondo/common/a/a;Lco/uk/getmondo/d/ak;)V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0, p1}, Lco/uk/getmondo/common/a/a;->a(Lco/uk/getmondo/d/ak;)V

    return-void
.end method

.method private final a(Lco/uk/getmondo/d/ak;)V
    .locals 10

    .prologue
    const/4 v5, 0x4

    const/4 v9, 0x3

    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 45
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lco/uk/getmondo/d/ak;->a()Lco/uk/getmondo/d/ak$a;

    move-result-object v0

    sget-object v1, Lco/uk/getmondo/d/ak$a;->HAS_ACCOUNT:Lco/uk/getmondo/d/ak$a;

    if-eq v0, v1, :cond_1

    .line 46
    :cond_0
    iget-object v0, p0, Lco/uk/getmondo/common/a/a;->a:Landroid/content/pm/ShortcutManager;

    invoke-virtual {v0}, Landroid/content/pm/ShortcutManager;->removeAllDynamicShortcuts()V

    .line 47
    iget-object v1, p0, Lco/uk/getmondo/common/a/a;->a:Landroid/content/pm/ShortcutManager;

    new-array v0, v5, [Ljava/lang/String;

    sget-object v2, Lco/uk/getmondo/common/a/b;->d:Lco/uk/getmondo/common/a/b;

    invoke-virtual {v2}, Lco/uk/getmondo/common/a/b;->a()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v6

    .line 48
    sget-object v2, Lco/uk/getmondo/common/a/b;->c:Lco/uk/getmondo/common/a/b;

    invoke-virtual {v2}, Lco/uk/getmondo/common/a/b;->a()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v7

    .line 49
    sget-object v2, Lco/uk/getmondo/common/a/b;->b:Lco/uk/getmondo/common/a/b;

    invoke-virtual {v2}, Lco/uk/getmondo/common/a/b;->a()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v8

    .line 50
    sget-object v2, Lco/uk/getmondo/common/a/b;->a:Lco/uk/getmondo/common/a/b;

    invoke-virtual {v2}, Lco/uk/getmondo/common/a/b;->a()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v9

    .line 47
    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    .line 51
    iget-object v0, p0, Lco/uk/getmondo/common/a/a;->b:Landroid/content/Context;

    const v3, 0x7f0a00df

    invoke-virtual {v0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    .line 47
    invoke-virtual {v1, v2, v0}, Landroid/content/pm/ShortcutManager;->disableShortcuts(Ljava/util/List;Ljava/lang/CharSequence;)V

    .line 74
    :goto_0
    return-void

    .line 53
    :cond_1
    const/4 v0, 0x0

    check-cast v0, Landroid/content/pm/ShortcutInfo;

    .line 54
    iget-object v1, p0, Lco/uk/getmondo/common/a/a;->c:Lco/uk/getmondo/payments/send/data/p;

    invoke-virtual {v1}, Lco/uk/getmondo/payments/send/data/p;->a()Lco/uk/getmondo/payments/send/data/a/d;

    move-result-object v1

    sget-object v2, Lco/uk/getmondo/payments/send/data/a/d;->a:Lco/uk/getmondo/payments/send/data/a/d;

    invoke-static {v1, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 55
    sget-object v0, Lco/uk/getmondo/common/a/b;->a:Lco/uk/getmondo/common/a/b;

    iget-object v1, p0, Lco/uk/getmondo/common/a/a;->b:Landroid/content/Context;

    invoke-direct {p0, v0, v1}, Lco/uk/getmondo/common/a/a;->a(Lco/uk/getmondo/common/a/b;Landroid/content/Context;)Landroid/content/pm/ShortcutInfo;

    move-result-object v0

    move-object v1, v0

    .line 64
    :goto_1
    const/4 v0, 0x0

    check-cast v0, Landroid/content/pm/ShortcutInfo;

    .line 66
    invoke-virtual {p1}, Lco/uk/getmondo/d/ak;->c()Lco/uk/getmondo/d/a;

    move-result-object v2

    if-eqz v2, :cond_2

    invoke-interface {v2}, Lco/uk/getmondo/d/a;->e()Z

    move-result v2

    if-nez v2, :cond_2

    .line 67
    sget-object v0, Lco/uk/getmondo/common/a/b;->b:Lco/uk/getmondo/common/a/b;

    iget-object v2, p0, Lco/uk/getmondo/common/a/a;->b:Landroid/content/Context;

    invoke-direct {p0, v0, v2}, Lco/uk/getmondo/common/a/a;->a(Lco/uk/getmondo/common/a/b;Landroid/content/Context;)Landroid/content/pm/ShortcutInfo;

    move-result-object v0

    .line 70
    :cond_2
    iget-object v2, p0, Lco/uk/getmondo/common/a/a;->a:Landroid/content/pm/ShortcutManager;

    new-array v3, v5, [Landroid/content/pm/ShortcutInfo;

    sget-object v4, Lco/uk/getmondo/common/a/b;->d:Lco/uk/getmondo/common/a/b;

    iget-object v5, p0, Lco/uk/getmondo/common/a/a;->b:Landroid/content/Context;

    invoke-direct {p0, v4, v5}, Lco/uk/getmondo/common/a/a;->a(Lco/uk/getmondo/common/a/b;Landroid/content/Context;)Landroid/content/pm/ShortcutInfo;

    move-result-object v4

    aput-object v4, v3, v6

    .line 71
    sget-object v4, Lco/uk/getmondo/common/a/b;->c:Lco/uk/getmondo/common/a/b;

    iget-object v5, p0, Lco/uk/getmondo/common/a/a;->b:Landroid/content/Context;

    invoke-direct {p0, v4, v5}, Lco/uk/getmondo/common/a/a;->a(Lco/uk/getmondo/common/a/b;Landroid/content/Context;)Landroid/content/pm/ShortcutInfo;

    move-result-object v4

    aput-object v4, v3, v7

    .line 72
    aput-object v0, v3, v8

    .line 73
    aput-object v1, v3, v9

    .line 70
    invoke-static {v3}, Lkotlin/a/m;->c([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/content/pm/ShortcutManager;->setDynamicShortcuts(Ljava/util/List;)Z

    goto :goto_0

    .line 57
    :cond_3
    iget-object v1, p0, Lco/uk/getmondo/common/a/a;->c:Lco/uk/getmondo/payments/send/data/p;

    invoke-virtual {v1}, Lco/uk/getmondo/payments/send/data/p;->a()Lco/uk/getmondo/payments/send/data/a/d;

    move-result-object v1

    sget-object v2, Lco/uk/getmondo/payments/send/data/a/d;->b:Lco/uk/getmondo/payments/send/data/a/d;

    invoke-static {v1, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 59
    const v1, 0x7f0a00de

    .line 62
    :goto_2
    iget-object v2, p0, Lco/uk/getmondo/common/a/a;->a:Landroid/content/pm/ShortcutManager;

    sget-object v3, Lco/uk/getmondo/common/a/b;->a:Lco/uk/getmondo/common/a/b;

    invoke-virtual {v3}, Lco/uk/getmondo/common/a/b;->a()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lkotlin/a/m;->a(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v3

    iget-object v4, p0, Lco/uk/getmondo/common/a/a;->b:Landroid/content/Context;

    invoke-virtual {v4, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {v2, v3, v1}, Landroid/content/pm/ShortcutManager;->disableShortcuts(Ljava/util/List;Ljava/lang/CharSequence;)V

    move-object v1, v0

    goto :goto_1

    .line 61
    :cond_4
    const v1, 0x7f0a00dd

    goto :goto_2
.end method

.method public static final synthetic b(Lco/uk/getmondo/common/a/a;)Lco/uk/getmondo/common/accounts/d;
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, Lco/uk/getmondo/common/a/a;->e:Lco/uk/getmondo/common/accounts/d;

    return-object v0
.end method


# virtual methods
.method public a()V
    .locals 3

    .prologue
    .line 36
    new-instance v0, Lco/uk/getmondo/common/a/a$a;

    invoke-direct {v0, p0}, Lco/uk/getmondo/common/a/a$a;-><init>(Lco/uk/getmondo/common/a/a;)V

    check-cast v0, Lio/reactivex/p;

    invoke-static {v0}, Lio/reactivex/n;->create(Lio/reactivex/p;)Lio/reactivex/n;

    move-result-object v1

    .line 40
    iget-object v0, p0, Lco/uk/getmondo/common/a/a;->d:Lco/uk/getmondo/payments/send/data/h;

    invoke-virtual {v0}, Lco/uk/getmondo/payments/send/data/h;->b()Lio/reactivex/n;

    move-result-object v0

    check-cast v0, Lio/reactivex/r;

    invoke-virtual {v1, v0}, Lio/reactivex/n;->mergeWith(Lio/reactivex/r;)Lio/reactivex/n;

    move-result-object v2

    .line 41
    new-instance v0, Lco/uk/getmondo/common/a/a$b;

    invoke-direct {v0, p0}, Lco/uk/getmondo/common/a/a$b;-><init>(Lco/uk/getmondo/common/a/a;)V

    check-cast v0, Lio/reactivex/c/g;

    sget-object v1, Lco/uk/getmondo/common/a/a$c;->a:Lco/uk/getmondo/common/a/a$c;

    check-cast v1, Lio/reactivex/c/g;

    invoke-virtual {v2, v0, v1}, Lio/reactivex/n;->subscribe(Lio/reactivex/c/g;Lio/reactivex/c/g;)Lio/reactivex/b/b;

    .line 42
    return-void
.end method

.method public a(Lco/uk/getmondo/common/a/b;)V
    .locals 2

    .prologue
    const-string v0, "appShortcut"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 86
    invoke-virtual {p1}, Lco/uk/getmondo/common/a/b;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lco/uk/getmondo/common/a/b;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lco/uk/getmondo/common/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 87
    return-void
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    const-string v0, "appShortcutId"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "analyticEvent"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 90
    iget-object v0, p0, Lco/uk/getmondo/common/a/a;->a:Landroid/content/pm/ShortcutManager;

    invoke-virtual {v0, p1}, Landroid/content/pm/ShortcutManager;->reportShortcutUsed(Ljava/lang/String;)V

    .line 92
    iget-object v0, p0, Lco/uk/getmondo/common/a/a;->g:Lco/uk/getmondo/common/a;

    sget-object v1, Lco/uk/getmondo/api/model/tracking/Impression;->Companion:Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    invoke-virtual {v1, p2}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->m(Ljava/lang/String;)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v1

    invoke-virtual {v0, v1}, Lco/uk/getmondo/common/a;->a(Lco/uk/getmondo/api/model/tracking/Impression;)V

    .line 93
    return-void
.end method
