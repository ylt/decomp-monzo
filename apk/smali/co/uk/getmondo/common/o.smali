.class public final Lco/uk/getmondo/common/o;
.super Ljava/lang/Object;
.source "FeatureFlagsStorage.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0007\u0008\u0007\u00a2\u0006\u0002\u0010\u0002J\u000c\u0010\u0003\u001a\u0008\u0012\u0004\u0012\u00020\u00050\u0004J\u0006\u0010\u0006\u001a\u00020\u0005J\u000e\u0010\u0007\u001a\u00020\u00082\u0006\u0010\u0003\u001a\u00020\u0005\u00a8\u0006\t"
    }
    d2 = {
        "Lco/uk/getmondo/common/FeatureFlagsStorage;",
        "",
        "()V",
        "featureFlags",
        "Lio/reactivex/Observable;",
        "Lco/uk/getmondo/model/FeatureFlags;",
        "getFeatureFlags",
        "save",
        "",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Lio/reactivex/n;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Lco/uk/getmondo/d/l;",
            ">;"
        }
    .end annotation

    .prologue
    .line 22
    sget-object v0, Lco/uk/getmondo/common/o$a;->a:Lco/uk/getmondo/common/o$a;

    check-cast v0, Lkotlin/d/a/b;

    invoke-static {v0}, Lco/uk/getmondo/common/j/g;->a(Lkotlin/d/a/b;)Lio/reactivex/n;

    move-result-object v1

    .line 23
    sget-object v0, Lco/uk/getmondo/common/o$b;->a:Lco/uk/getmondo/common/o$b;

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {v1, v0}, Lio/reactivex/n;->map(Lio/reactivex/c/h;)Lio/reactivex/n;

    move-result-object v0

    const-string v1, "RxRealm.asObservable { r\u2026      }\n                }"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public final a(Lco/uk/getmondo/d/l;)V
    .locals 5

    .prologue
    const-string v1, "featureFlags"

    invoke-static {p1, v1}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 13
    invoke-static {}, Lio/realm/av;->n()Lio/realm/av;

    move-result-object v1

    check-cast v1, Ljava/io/Closeable;

    const/4 v4, 0x0

    nop

    :try_start_0
    move-object v0, v1

    check-cast v0, Lio/realm/av;

    move-object v2, v0

    .line 14
    new-instance v3, Lco/uk/getmondo/common/o$c;

    invoke-direct {v3, p1}, Lco/uk/getmondo/common/o$c;-><init>(Lco/uk/getmondo/d/l;)V

    check-cast v3, Lio/realm/av$a;

    invoke-virtual {v2, v3}, Lio/realm/av;->a(Lio/realm/av$a;)V

    .line 18
    sget-object v2, Lkotlin/n;->a:Lkotlin/n;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 13
    if-eqz v1, :cond_0

    invoke-interface {v1}, Ljava/io/Closeable;->close()V

    .line 19
    :cond_0
    return-void

    .line 13
    :catch_0
    move-exception v2

    const/4 v3, 0x1

    nop

    if-eqz v1, :cond_1

    :try_start_1
    invoke-interface {v1}, Ljava/io/Closeable;->close()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_1
    :goto_0
    :try_start_2
    check-cast v2, Ljava/lang/Throwable;

    throw v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :catchall_0
    move-exception v2

    :goto_1
    if-nez v3, :cond_2

    if-eqz v1, :cond_2

    invoke-interface {v1}, Ljava/io/Closeable;->close()V

    :cond_2
    throw v2

    :catch_1
    move-exception v4

    goto :goto_0

    :catchall_1
    move-exception v2

    move v3, v4

    goto :goto_1
.end method

.method public final b()Lco/uk/getmondo/d/l;
    .locals 8

    .prologue
    const/4 v3, 0x0

    .line 34
    invoke-static {}, Lio/realm/av;->n()Lio/realm/av;

    move-result-object v1

    check-cast v1, Ljava/io/Closeable;

    .line 35
    nop

    :try_start_0
    move-object v0, v1

    check-cast v0, Lio/realm/av;

    move-object v2, v0

    .line 36
    const-class v4, Lco/uk/getmondo/d/l;

    invoke-virtual {v2, v4}, Lio/realm/av;->a(Ljava/lang/Class;)Lio/realm/bf;

    move-result-object v2

    invoke-virtual {v2}, Lio/realm/bf;->h()Lio/realm/bb;

    move-result-object v2

    check-cast v2, Lco/uk/getmondo/d/l;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 37
    if-eqz v2, :cond_1

    :goto_0
    if-eqz v1, :cond_0

    invoke-interface {v1}, Ljava/io/Closeable;->close()V

    :cond_0
    return-object v2

    :cond_1
    :try_start_1
    new-instance v2, Lco/uk/getmondo/d/l;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x3

    const/4 v7, 0x0

    invoke-direct {v2, v4, v5, v6, v7}, Lco/uk/getmondo/d/l;-><init>(ZZILkotlin/d/b/i;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 35
    :catch_0
    move-exception v2

    const/4 v3, 0x1

    nop

    if-eqz v1, :cond_2

    :try_start_2
    invoke-interface {v1}, Ljava/io/Closeable;->close()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :cond_2
    :goto_1
    :try_start_3
    check-cast v2, Ljava/lang/Throwable;

    throw v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :catchall_0
    move-exception v2

    if-nez v3, :cond_3

    if-eqz v1, :cond_3

    invoke-interface {v1}, Ljava/io/Closeable;->close()V

    :cond_3
    throw v2

    :catch_1
    move-exception v4

    goto :goto_1
.end method
