.class public final Lco/uk/getmondo/common/c/b;
.super Ljava/lang/Object;
.source "ThreeTenMoshiAdapter.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0012\u0010\u0003\u001a\u0004\u0018\u00010\u00042\u0006\u0010\u0005\u001a\u00020\u0006H\u0007J\u0012\u0010\u0007\u001a\u0004\u0018\u00010\u00082\u0006\u0010\u0005\u001a\u00020\u0006H\u0007J\u0010\u0010\t\u001a\u00020\u00062\u0006\u0010\n\u001a\u00020\u0008H\u0007J\u0010\u0010\u000b\u001a\u00020\u00062\u0006\u0010\u000c\u001a\u00020\u0004H\u0007J\u0012\u0010\r\u001a\u0004\u0018\u00010\u000e2\u0006\u0010\u0005\u001a\u00020\u0006H\u0007J\u0010\u0010\u000f\u001a\u00020\u00062\u0006\u0010\u0010\u001a\u00020\u000eH\u0007\u00a8\u0006\u0011"
    }
    d2 = {
        "Lco/uk/getmondo/common/date/ThreeTenMoshiAdapter;",
        "",
        "()V",
        "localDateFromJson",
        "Lorg/threeten/bp/LocalDate;",
        "date",
        "",
        "localDateTimeFromJson",
        "Lorg/threeten/bp/LocalDateTime;",
        "localDateTimeToJason",
        "localDateTime",
        "localDateToJason",
        "localDate",
        "zonedDateTimeFromJson",
        "Lorg/threeten/bp/ZonedDateTime;",
        "zonedDateTimeToJason",
        "zonedDateTime",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final localDateFromJson(Ljava/lang/String;)Lorg/threeten/bp/LocalDate;
    .locals 1
    .annotation runtime Lcom/squareup/moshi/g;
    .end annotation

    .prologue
    const-string v0, "date"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v0, p1

    .line 15
    check-cast v0, Ljava/lang/CharSequence;

    invoke-static {v0}, Lkotlin/h/j;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_1

    .line 16
    check-cast p1, Ljava/lang/CharSequence;

    invoke-static {p1}, Lorg/threeten/bp/LocalDate;->a(Ljava/lang/CharSequence;)Lorg/threeten/bp/LocalDate;

    move-result-object v0

    .line 15
    :goto_1
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 17
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final localDateTimeFromJson(Ljava/lang/String;)Lorg/threeten/bp/LocalDateTime;
    .locals 1
    .annotation runtime Lcom/squareup/moshi/g;
    .end annotation

    .prologue
    const-string v0, "date"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v0, p1

    .line 23
    check-cast v0, Ljava/lang/CharSequence;

    invoke-static {v0}, Lkotlin/h/j;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_1

    .line 24
    check-cast p1, Ljava/lang/CharSequence;

    invoke-static {p1}, Lorg/threeten/bp/ZonedDateTime;->a(Ljava/lang/CharSequence;)Lorg/threeten/bp/ZonedDateTime;

    move-result-object v0

    invoke-virtual {v0}, Lorg/threeten/bp/ZonedDateTime;->d()Lorg/threeten/bp/LocalDateTime;

    move-result-object v0

    .line 23
    :goto_1
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 25
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final localDateTimeToJason(Lorg/threeten/bp/LocalDateTime;)Ljava/lang/String;
    .locals 2
    .annotation runtime Lcom/squareup/moshi/x;
    .end annotation

    .prologue
    const-string v0, "localDateTime"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 21
    sget-object v0, Lorg/threeten/bp/format/DateTimeFormatter;->g:Lorg/threeten/bp/format/DateTimeFormatter;

    invoke-virtual {p1, v0}, Lorg/threeten/bp/LocalDateTime;->a(Lorg/threeten/bp/format/DateTimeFormatter;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "localDateTime.format(Dat\u2026tter.ISO_LOCAL_DATE_TIME)"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public final localDateToJason(Lorg/threeten/bp/LocalDate;)Ljava/lang/String;
    .locals 2
    .annotation runtime Lcom/squareup/moshi/x;
    .end annotation

    .prologue
    const-string v0, "localDate"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 13
    sget-object v0, Lorg/threeten/bp/format/DateTimeFormatter;->a:Lorg/threeten/bp/format/DateTimeFormatter;

    invoke-virtual {p1, v0}, Lorg/threeten/bp/LocalDate;->a(Lorg/threeten/bp/format/DateTimeFormatter;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "localDate.format(DateTimeFormatter.ISO_LOCAL_DATE)"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public final zonedDateTimeFromJson(Ljava/lang/String;)Lorg/threeten/bp/ZonedDateTime;
    .locals 1
    .annotation runtime Lcom/squareup/moshi/g;
    .end annotation

    .prologue
    const-string v0, "date"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v0, p1

    .line 31
    check-cast v0, Ljava/lang/CharSequence;

    invoke-static {v0}, Lkotlin/h/j;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_1

    .line 32
    check-cast p1, Ljava/lang/CharSequence;

    invoke-static {p1}, Lorg/threeten/bp/ZonedDateTime;->a(Ljava/lang/CharSequence;)Lorg/threeten/bp/ZonedDateTime;

    move-result-object v0

    .line 31
    :goto_1
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 33
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final zonedDateTimeToJason(Lorg/threeten/bp/ZonedDateTime;)Ljava/lang/String;
    .locals 2
    .annotation runtime Lcom/squareup/moshi/x;
    .end annotation

    .prologue
    const-string v0, "zonedDateTime"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 29
    sget-object v0, Lorg/threeten/bp/format/DateTimeFormatter;->i:Lorg/threeten/bp/format/DateTimeFormatter;

    invoke-virtual {p1, v0}, Lorg/threeten/bp/ZonedDateTime;->a(Lorg/threeten/bp/format/DateTimeFormatter;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "zonedDateTime.format(Dat\u2026tter.ISO_ZONED_DATE_TIME)"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method
