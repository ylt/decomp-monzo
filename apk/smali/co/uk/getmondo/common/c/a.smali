.class public final Lco/uk/getmondo/common/c/a;
.super Ljava/lang/Object;
.source "DateUtils.java"


# static fields
.field private static final a:Ljava/util/Calendar;

.field private static final b:Ljava/text/SimpleDateFormat;

.field private static final c:Ljava/text/SimpleDateFormat;

.field private static final d:Ljava/text/SimpleDateFormat;

.field private static final e:Ljava/text/SimpleDateFormat;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 21
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    sput-object v0, Lco/uk/getmondo/common/c/a;->a:Ljava/util/Calendar;

    .line 29
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "EEEE"

    sget-object v2, Ljava/util/Locale;->UK:Ljava/util/Locale;

    invoke-direct {v0, v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    sput-object v0, Lco/uk/getmondo/common/c/a;->b:Ljava/text/SimpleDateFormat;

    .line 30
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "MMMM"

    sget-object v2, Ljava/util/Locale;->UK:Ljava/util/Locale;

    invoke-direct {v0, v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    sput-object v0, Lco/uk/getmondo/common/c/a;->c:Ljava/text/SimpleDateFormat;

    .line 31
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "HH:mm"

    sget-object v2, Ljava/util/Locale;->UK:Ljava/util/Locale;

    invoke-direct {v0, v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    sput-object v0, Lco/uk/getmondo/common/c/a;->d:Ljava/text/SimpleDateFormat;

    .line 32
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "EEEE d MMMM yyyy HH:mm:ss"

    sget-object v2, Ljava/util/Locale;->UK:Ljava/util/Locale;

    invoke-direct {v0, v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    sput-object v0, Lco/uk/getmondo/common/c/a;->e:Ljava/text/SimpleDateFormat;

    return-void
.end method

.method public static a(J)I
    .locals 2

    .prologue
    .line 39
    sget-object v0, Lco/uk/getmondo/common/c/a;->a:Ljava/util/Calendar;

    invoke-virtual {v0, p0, p1}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 40
    sget-object v0, Lco/uk/getmondo/common/c/a;->a:Ljava/util/Calendar;

    const/4 v1, 0x6

    invoke-virtual {v0, v1}, Ljava/util/Calendar;->get(I)I

    move-result v0

    return v0
.end method

.method public static a(I)Ljava/lang/String;
    .locals 3

    .prologue
    .line 84
    const/4 v0, 0x1

    if-lt p0, v0, :cond_0

    const/16 v0, 0x1f

    if-le p0, v0, :cond_1

    .line 85
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Illegal day of month: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 87
    :cond_1
    const/16 v0, 0xb

    if-lt p0, v0, :cond_2

    const/16 v0, 0xd

    if-gt p0, v0, :cond_2

    .line 88
    const-string v0, "th"

    .line 98
    :goto_0
    return-object v0

    .line 90
    :cond_2
    rem-int/lit8 v0, p0, 0xa

    packed-switch v0, :pswitch_data_0

    .line 98
    const-string v0, "th"

    goto :goto_0

    .line 92
    :pswitch_0
    const-string v0, "st"

    goto :goto_0

    .line 94
    :pswitch_1
    const-string v0, "nd"

    goto :goto_0

    .line 96
    :pswitch_2
    const-string v0, "rd"

    goto :goto_0

    .line 90
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static a(JZ)Ljava/lang/String;
    .locals 12

    .prologue
    const/4 v10, 0x0

    const/4 v5, 0x6

    const/4 v6, 0x1

    .line 53
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0, p0, p1}, Ljava/util/Date;-><init>(J)V

    .line 54
    sget-object v1, Lco/uk/getmondo/common/c/a;->a:Ljava/util/Calendar;

    invoke-virtual {v1, p0, p1}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 55
    sget-object v1, Lco/uk/getmondo/common/c/a;->a:Ljava/util/Calendar;

    const/4 v2, 0x5

    invoke-virtual {v1, v2}, Ljava/util/Calendar;->get(I)I

    move-result v1

    .line 57
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v2

    .line 58
    invoke-virtual {v2, v6}, Ljava/util/Calendar;->get(I)I

    move-result v3

    sget-object v4, Lco/uk/getmondo/common/c/a;->a:Ljava/util/Calendar;

    invoke-virtual {v4, v6}, Ljava/util/Calendar;->get(I)I

    move-result v4

    if-ne v3, v4, :cond_0

    .line 59
    invoke-virtual {v2, v5}, Ljava/util/Calendar;->get(I)I

    move-result v3

    sget-object v4, Lco/uk/getmondo/common/c/a;->a:Ljava/util/Calendar;

    invoke-virtual {v4, v5}, Ljava/util/Calendar;->get(I)I

    move-result v4

    if-ne v3, v4, :cond_0

    .line 60
    const-string v0, "Today"

    .line 73
    :goto_0
    return-object v0

    .line 63
    :cond_0
    if-eqz p2, :cond_2

    invoke-virtual {v2, v6}, Ljava/util/Calendar;->get(I)I

    move-result v3

    sget-object v4, Lco/uk/getmondo/common/c/a;->a:Ljava/util/Calendar;

    invoke-virtual {v4, v6}, Ljava/util/Calendar;->get(I)I

    move-result v4

    if-lt v3, v4, :cond_1

    .line 64
    invoke-virtual {v2, v5}, Ljava/util/Calendar;->get(I)I

    move-result v3

    sget-object v4, Lco/uk/getmondo/common/c/a;->a:Ljava/util/Calendar;

    invoke-virtual {v4, v5}, Ljava/util/Calendar;->get(I)I

    move-result v4

    if-ge v3, v4, :cond_2

    .line 65
    :cond_1
    sget-object v0, Ljava/util/Locale;->UK:Ljava/util/Locale;

    const-string v1, "%1$s days from now"

    new-array v3, v6, [Ljava/lang/Object;

    sget-object v4, Ljava/util/concurrent/TimeUnit;->DAYS:Ljava/util/concurrent/TimeUnit;

    sget-object v5, Lco/uk/getmondo/common/c/a;->a:Ljava/util/Calendar;

    invoke-virtual {v5}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v6

    invoke-virtual {v2}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v8

    sub-long/2addr v6, v8

    sget-object v2, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v4, v6, v7, v2}, Ljava/util/concurrent/TimeUnit;->convert(JLjava/util/concurrent/TimeUnit;)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v3, v10

    invoke-static {v0, v1, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 68
    :cond_2
    invoke-static {v1}, Lco/uk/getmondo/common/c/a;->a(I)Ljava/lang/String;

    move-result-object v2

    .line 69
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 70
    sget-object v2, Lco/uk/getmondo/common/c/a;->b:Ljava/text/SimpleDateFormat;

    invoke-virtual {v2, v0}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v2

    .line 71
    sget-object v3, Lco/uk/getmondo/common/c/a;->c:Ljava/text/SimpleDateFormat;

    invoke-virtual {v3, v0}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    .line 73
    sget-object v3, Ljava/util/Locale;->UK:Ljava/util/Locale;

    const-string v4, "%1$s %2$s %3$s"

    const/4 v5, 0x3

    new-array v5, v5, [Ljava/lang/Object;

    aput-object v2, v5, v10

    aput-object v1, v5, v6

    const/4 v1, 0x2

    aput-object v0, v5, v1

    invoke-static {v3, v4, v5}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Ljava/util/Date;)Ljava/lang/String;
    .locals 8

    .prologue
    .line 77
    sget-object v0, Lco/uk/getmondo/common/c/a;->a:Ljava/util/Calendar;

    invoke-virtual {v0, p0}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    .line 78
    sget-object v0, Lco/uk/getmondo/common/c/a;->d:Ljava/text/SimpleDateFormat;

    invoke-virtual {v0, p0}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    .line 80
    sget-object v1, Ljava/util/Locale;->UK:Ljava/util/Locale;

    const-string v2, "%1$s, %2$s"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-virtual {p0}, Ljava/util/Date;->getTime()J

    move-result-wide v6

    invoke-static {v6, v7}, Lco/uk/getmondo/common/c/a;->b(J)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    aput-object v0, v3, v4

    invoke-static {v1, v2, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static a()Ljava/util/Date;
    .locals 4

    .prologue
    .line 103
    sget-object v0, Lco/uk/getmondo/common/c/a;->a:Ljava/util/Calendar;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 104
    sget-object v0, Lco/uk/getmondo/common/c/a;->a:Ljava/util/Calendar;

    const/4 v1, 0x6

    const/4 v2, -0x7

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->add(II)V

    .line 105
    sget-object v0, Lco/uk/getmondo/common/c/a;->a:Ljava/util/Calendar;

    invoke-virtual {v0}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/util/Date;Ljava/util/Date;)Ljava/util/Date;
    .locals 4

    .prologue
    .line 109
    invoke-virtual {p0}, Ljava/util/Date;->getTime()J

    move-result-wide v0

    invoke-virtual {p1}, Ljava/util/Date;->getTime()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-gez v0, :cond_0

    .line 112
    :goto_0
    return-object p0

    :cond_0
    move-object p0, p1

    goto :goto_0
.end method

.method public static a(Lorg/threeten/bp/LocalDateTime;)Ljava/util/Date;
    .locals 7

    .prologue
    .line 129
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 130
    invoke-virtual {p0}, Lorg/threeten/bp/LocalDateTime;->b()I

    move-result v1

    invoke-virtual {p0}, Lorg/threeten/bp/LocalDateTime;->c()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {p0}, Lorg/threeten/bp/LocalDateTime;->e()I

    move-result v3

    invoke-virtual {p0}, Lorg/threeten/bp/LocalDateTime;->f()I

    move-result v4

    invoke-virtual {p0}, Lorg/threeten/bp/LocalDateTime;->g()I

    move-result v5

    invoke-virtual {p0}, Lorg/threeten/bp/LocalDateTime;->h()I

    move-result v6

    invoke-virtual/range {v0 .. v6}, Ljava/util/Calendar;->set(IIIIII)V

    .line 131
    invoke-virtual {v0}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/util/Date;Ljava/util/TimeZone;)Lorg/threeten/bp/LocalDateTime;
    .locals 2

    .prologue
    .line 135
    invoke-virtual {p0}, Ljava/util/Date;->getTime()J

    move-result-wide v0

    invoke-static {v0, v1}, Lorg/threeten/bp/Instant;->b(J)Lorg/threeten/bp/Instant;

    move-result-object v0

    invoke-static {p1}, Lorg/threeten/bp/DateTimeUtils;->a(Ljava/util/TimeZone;)Lorg/threeten/bp/ZoneId;

    move-result-object v1

    invoke-static {v0, v1}, Lorg/threeten/bp/LocalDateTime;->a(Lorg/threeten/bp/Instant;Lorg/threeten/bp/ZoneId;)Lorg/threeten/bp/LocalDateTime;

    move-result-object v0

    return-object v0
.end method

.method public static b(J)Ljava/lang/String;
    .locals 2

    .prologue
    .line 44
    const/4 v0, 0x0

    invoke-static {p0, p1, v0}, Lco/uk/getmondo/common/c/a;->a(JZ)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static b(Ljava/util/Date;)Ljava/util/Date;
    .locals 3

    .prologue
    .line 117
    sget-object v0, Lco/uk/getmondo/common/c/a;->a:Ljava/util/Calendar;

    invoke-virtual {v0, p0}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    .line 118
    sget-object v0, Lco/uk/getmondo/common/c/a;->a:Ljava/util/Calendar;

    const/4 v1, 0x6

    const/4 v2, -0x1

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->add(II)V

    .line 119
    sget-object v0, Lco/uk/getmondo/common/c/a;->a:Ljava/util/Calendar;

    invoke-virtual {v0}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v0

    return-object v0
.end method

.method public static b(Ljava/util/Date;Ljava/util/TimeZone;)Lorg/threeten/bp/YearMonth;
    .locals 2

    .prologue
    .line 139
    invoke-static {p0, p1}, Lco/uk/getmondo/common/c/a;->a(Ljava/util/Date;Ljava/util/TimeZone;)Lorg/threeten/bp/LocalDateTime;

    move-result-object v0

    .line 140
    invoke-virtual {v0}, Lorg/threeten/bp/LocalDateTime;->b()I

    move-result v1

    invoke-virtual {v0}, Lorg/threeten/bp/LocalDateTime;->d()Lorg/threeten/bp/Month;

    move-result-object v0

    invoke-static {v1, v0}, Lorg/threeten/bp/YearMonth;->a(ILorg/threeten/bp/Month;)Lorg/threeten/bp/YearMonth;

    move-result-object v0

    return-object v0
.end method

.method public static c(Ljava/util/Date;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 123
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "yyyy-MM-dd\'T\'HH:mm:ss.SSS\'Z\'"

    sget-object v2, Ljava/util/Locale;->UK:Ljava/util/Locale;

    invoke-direct {v0, v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 124
    const-string v1, "UTC"

    invoke-static {v1}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/text/SimpleDateFormat;->setTimeZone(Ljava/util/TimeZone;)V

    .line 125
    invoke-virtual {v0, p0}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static d(Ljava/util/Date;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 144
    sget-object v0, Lco/uk/getmondo/common/c/a;->e:Ljava/text/SimpleDateFormat;

    invoke-virtual {v0, p0}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
