.class public Lco/uk/getmondo/common/k/l;
.super Ljava/lang/Object;
.source "PhotoFileProvider.java"


# instance fields
.field private final a:Landroid/content/Context;


# direct methods
.method constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    iput-object p1, p0, Lco/uk/getmondo/common/k/l;->a:Landroid/content/Context;

    .line 23
    return-void
.end method


# virtual methods
.method public a()Ljava/io/File;
    .locals 4

    .prologue
    .line 27
    const/4 v0, 0x0

    .line 29
    :try_start_0
    iget-object v1, p0, Lco/uk/getmondo/common/k/l;->a:Landroid/content/Context;

    invoke-static {v1}, Lco/uk/getmondo/common/k/h;->a(Landroid/content/Context;)Ljava/io/File;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 33
    :goto_0
    return-object v0

    .line 30
    :catch_0
    move-exception v1

    .line 31
    const-string v2, "Failed to get photo file from file"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, Ld/a/a;->a(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public a(Ljava/lang/String;)Ljava/io/InputStream;
    .locals 4

    .prologue
    .line 37
    const/4 v0, 0x0

    .line 39
    :try_start_0
    iget-object v1, p0, Lco/uk/getmondo/common/k/l;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/content/ContentResolver;->openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 43
    :goto_0
    return-object v0

    .line 40
    :catch_0
    move-exception v1

    .line 41
    const-string v2, "Failed to get photo file from input stream"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, Ld/a/a;->a(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public b(Ljava/lang/String;)J
    .locals 3

    .prologue
    .line 48
    :try_start_0
    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/common/k/l;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-static {v0, v1}, Lco/uk/getmondo/common/k/h;->a(Landroid/net/Uri;Landroid/content/ContentResolver;)J
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v0

    .line 52
    :goto_0
    return-wide v0

    .line 49
    :catch_0
    move-exception v0

    .line 50
    const-string v1, "Failed to get photo file size"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Ld/a/a;->a(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 52
    const-wide/16 v0, 0x0

    goto :goto_0
.end method
