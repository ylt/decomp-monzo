.class public final Lco/uk/getmondo/common/k/d;
.super Ljava/lang/Object;
.source "DateInputFormatter.kt"

# interfaces
.implements Landroid/text/TextWatcher;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0002\u0008\u0002\n\u0002\u0010\u000c\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\r\n\u0002\u0008\u0004\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0010\u0010\u0008\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\u000bH\u0016J(\u0010\u000c\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\r2\u0006\u0010\u000e\u001a\u00020\u00042\u0006\u0010\u000f\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0004H\u0016J(\u0010\u0010\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\r2\u0006\u0010\u000e\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u000f\u001a\u00020\u0004H\u0016R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082D\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0011"
    }
    d2 = {
        "Lco/uk/getmondo/common/utils/DateInputFormatter;",
        "Landroid/text/TextWatcher;",
        "()V",
        "after",
        "",
        "before",
        "divider",
        "",
        "afterTextChanged",
        "",
        "text",
        "Landroid/text/Editable;",
        "beforeTextChanged",
        "",
        "start",
        "count",
        "onTextChanged",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field private final a:C

.field private b:I

.field private c:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 13
    const/16 v0, 0x2f

    iput-char v0, p0, Lco/uk/getmondo/common/k/d;->a:C

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 9

    .prologue
    const/4 v8, 0x5

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v1, 0x1

    const/4 v2, 0x0

    const-string v0, "text"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 18
    iget v0, p0, Lco/uk/getmondo/common/k/d;->c:I

    iget v3, p0, Lco/uk/getmondo/common/k/d;->b:I

    if-ge v0, v3, :cond_1

    move v0, v1

    .line 19
    :goto_0
    if-nez v0, :cond_0

    move-object v0, p1

    check-cast v0, Ljava/lang/CharSequence;

    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    if-eqz v0, :cond_3

    .line 35
    :cond_0
    :goto_2
    return-void

    :cond_1
    move v0, v2

    .line 18
    goto :goto_0

    :cond_2
    move v0, v2

    .line 19
    goto :goto_1

    :cond_3
    move-object v0, p1

    .line 21
    check-cast v0, Ljava/lang/CharSequence;

    invoke-static {v0}, Lkotlin/h/j;->e(Ljava/lang/CharSequence;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/text/Editable;->charAt(I)C

    move-result v0

    iget-char v3, p0, Lco/uk/getmondo/common/k/d;->a:C

    if-ne v0, v3, :cond_6

    move v0, v1

    .line 22
    :goto_3
    invoke-interface {p1}, Landroid/text/Editable;->length()I

    move-result v3

    if-eq v3, v6, :cond_4

    invoke-interface {p1}, Landroid/text/Editable;->length()I

    move-result v3

    if-ne v3, v8, :cond_7

    :cond_4
    move v3, v1

    .line 23
    :goto_4
    invoke-interface {p1}, Landroid/text/Editable;->length()I

    move-result v4

    if-eq v4, v7, :cond_5

    invoke-interface {p1}, Landroid/text/Editable;->length()I

    move-result v4

    const/4 v5, 0x6

    if-ne v4, v5, :cond_8

    .line 25
    :cond_5
    :goto_5
    if-eqz v0, :cond_9

    invoke-interface {p1}, Landroid/text/Editable;->length()I

    move-result v4

    if-ne v4, v6, :cond_9

    .line 26
    const-string v0, "0"

    check-cast v0, Ljava/lang/CharSequence;

    invoke-interface {p1, v2, v0}, Landroid/text/Editable;->insert(ILjava/lang/CharSequence;)Landroid/text/Editable;

    goto :goto_2

    :cond_6
    move v0, v2

    .line 21
    goto :goto_3

    :cond_7
    move v3, v2

    .line 22
    goto :goto_4

    :cond_8
    move v1, v2

    .line 23
    goto :goto_5

    .line 27
    :cond_9
    if-eqz v0, :cond_a

    invoke-interface {p1, v6}, Landroid/text/Editable;->charAt(I)C

    move-result v2

    iget-char v4, p0, Lco/uk/getmondo/common/k/d;->a:C

    if-ne v2, v4, :cond_a

    invoke-interface {p1}, Landroid/text/Editable;->length()I

    move-result v2

    if-ne v2, v8, :cond_a

    .line 28
    const-string v0, "0"

    check-cast v0, Ljava/lang/CharSequence;

    invoke-interface {p1, v7, v0}, Landroid/text/Editable;->insert(ILjava/lang/CharSequence;)Landroid/text/Editable;

    goto :goto_2

    .line 29
    :cond_a
    if-eqz v3, :cond_b

    .line 30
    iget-char v0, p0, Lco/uk/getmondo/common/k/d;->a:C

    invoke-interface {p1, v0}, Landroid/text/Editable;->append(C)Landroid/text/Editable;

    goto :goto_2

    .line 31
    :cond_b
    if-eqz v1, :cond_c

    if-nez v0, :cond_c

    move-object v0, p1

    .line 32
    check-cast v0, Ljava/lang/CharSequence;

    invoke-static {v0}, Lkotlin/h/j;->e(Ljava/lang/CharSequence;)I

    move-result v1

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, ""

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-char v2, p0, Lco/uk/getmondo/common/k/d;->a:C

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-interface {p1, v1, v0}, Landroid/text/Editable;->insert(ILjava/lang/CharSequence;)Landroid/text/Editable;

    goto/16 :goto_2

    .line 33
    :cond_c
    if-nez v1, :cond_0

    if-eqz v0, :cond_0

    move-object v0, p1

    .line 34
    check-cast v0, Ljava/lang/CharSequence;

    invoke-static {v0}, Lkotlin/h/j;->e(Ljava/lang/CharSequence;)I

    move-result v1

    move-object v0, p1

    check-cast v0, Ljava/lang/CharSequence;

    invoke-static {v0}, Lkotlin/h/j;->e(Ljava/lang/CharSequence;)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    invoke-interface {p1, v1, v0}, Landroid/text/Editable;->delete(II)Landroid/text/Editable;

    goto/16 :goto_2
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 1

    .prologue
    const-string v0, "text"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 39
    iput p4, p0, Lco/uk/getmondo/common/k/d;->c:I

    .line 40
    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 1

    .prologue
    const-string v0, "text"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 43
    iput p3, p0, Lco/uk/getmondo/common/k/d;->b:I

    .line 44
    return-void
.end method
