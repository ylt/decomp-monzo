.class public Lco/uk/getmondo/common/k/c;
.super Ljava/lang/Object;
.source "ColorUtil.java"


# direct methods
.method public static a(I)I
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v0, 0x0

    .line 12
    const/4 v1, 0x3

    new-array v2, v1, [F

    .line 13
    invoke-static {p0, v2}, Landroid/graphics/Color;->colorToHSV(I[F)V

    .line 14
    aget v1, v2, v4

    .line 15
    const/high16 v3, 0x3e800000    # 0.25f

    sub-float/2addr v1, v3

    .line 16
    cmpg-float v3, v1, v0

    if-gez v3, :cond_0

    .line 19
    :goto_0
    aput v0, v2, v4

    .line 20
    invoke-static {v2}, Landroid/graphics/Color;->HSVToColor([F)I

    move-result v0

    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method public static a(IF)I
    .locals 4

    .prologue
    .line 24
    invoke-static {p0}, Landroid/graphics/Color;->alpha(I)I

    move-result v0

    int-to-float v0, v0

    mul-float/2addr v0, p1

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    .line 25
    invoke-static {p0}, Landroid/graphics/Color;->red(I)I

    move-result v1

    .line 26
    invoke-static {p0}, Landroid/graphics/Color;->green(I)I

    move-result v2

    .line 27
    invoke-static {p0}, Landroid/graphics/Color;->blue(I)I

    move-result v3

    .line 28
    invoke-static {v0, v1, v2, v3}, Landroid/graphics/Color;->argb(IIII)I

    move-result v0

    return v0
.end method
