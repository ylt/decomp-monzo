.class Lco/uk/getmondo/common/k/i$1;
.super Ljava/lang/Object;
.source "InputFormatter.java"

# interfaces
.implements Landroid/text/TextWatcher;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lco/uk/getmondo/common/k/i;->a()Landroid/text/TextWatcher;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lco/uk/getmondo/common/k/i;


# direct methods
.method constructor <init>(Lco/uk/getmondo/common/k/i;)V
    .locals 0

    .prologue
    .line 34
    iput-object p1, p0, Lco/uk/getmondo/common/k/i$1;->a:Lco/uk/getmondo/common/k/i;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 3

    .prologue
    .line 47
    const/4 v0, 0x0

    :goto_0
    invoke-interface {p1}, Landroid/text/Editable;->length()I

    move-result v1

    if-ge v0, v1, :cond_1

    .line 48
    iget-object v1, p0, Lco/uk/getmondo/common/k/i$1;->a:Lco/uk/getmondo/common/k/i;

    invoke-static {v1}, Lco/uk/getmondo/common/k/i;->a(Lco/uk/getmondo/common/k/i;)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    rem-int v1, v0, v1

    iget-object v2, p0, Lco/uk/getmondo/common/k/i$1;->a:Lco/uk/getmondo/common/k/i;

    invoke-static {v2}, Lco/uk/getmondo/common/k/i;->a(Lco/uk/getmondo/common/k/i;)I

    move-result v2

    if-ne v1, v2, :cond_0

    invoke-interface {p1, v0}, Landroid/text/Editable;->charAt(I)C

    move-result v1

    iget-object v2, p0, Lco/uk/getmondo/common/k/i$1;->a:Lco/uk/getmondo/common/k/i;

    invoke-static {v2}, Lco/uk/getmondo/common/k/i;->b(Lco/uk/getmondo/common/k/i;)C

    move-result v2

    if-eq v1, v2, :cond_0

    .line 49
    iget-object v1, p0, Lco/uk/getmondo/common/k/i$1;->a:Lco/uk/getmondo/common/k/i;

    invoke-static {v1}, Lco/uk/getmondo/common/k/i;->b(Lco/uk/getmondo/common/k/i;)C

    move-result v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(C)Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v0, v1}, Landroid/text/Editable;->insert(ILjava/lang/CharSequence;)Landroid/text/Editable;

    .line 47
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 52
    :cond_1
    return-void
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 38
    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 43
    return-void
.end method
