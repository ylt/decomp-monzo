.class final Lco/uk/getmondo/common/o$b;
.super Ljava/lang/Object;
.source "FeatureFlagsStorage.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lco/uk/getmondo/common/o;->a()Lio/reactivex/n;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h",
        "<TT;TR;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u0004\u0018\u00010\u00012\u0014\u0010\u0002\u001a\u0010\u0012\u000c\u0012\n \u0004*\u0004\u0018\u00010\u00010\u00010\u0003H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "Lco/uk/getmondo/model/FeatureFlags;",
        "results",
        "Lco/uk/getmondo/common/rx/RxRealm$Result;",
        "kotlin.jvm.PlatformType",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# static fields
.field public static final a:Lco/uk/getmondo/common/o$b;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lco/uk/getmondo/common/o$b;

    invoke-direct {v0}, Lco/uk/getmondo/common/o$b;-><init>()V

    sput-object v0, Lco/uk/getmondo/common/o$b;->a:Lco/uk/getmondo/common/o$b;

    return-void
.end method

.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lco/uk/getmondo/common/j/g$a;)Lco/uk/getmondo/d/l;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lco/uk/getmondo/common/j/g$a",
            "<+",
            "Lco/uk/getmondo/d/l;",
            ">;)",
            "Lco/uk/getmondo/d/l;"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    const-string v0, "results"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 24
    invoke-virtual {p1}, Lco/uk/getmondo/common/j/g$a;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 26
    new-instance v0, Lco/uk/getmondo/d/l;

    const/4 v1, 0x3

    const/4 v2, 0x0

    invoke-direct {v0, v3, v3, v1, v2}, Lco/uk/getmondo/d/l;-><init>(ZZILkotlin/d/b/i;)V

    .line 24
    :goto_0
    return-object v0

    .line 28
    :cond_0
    invoke-virtual {p1}, Lco/uk/getmondo/common/j/g$a;->d()Lio/realm/bb;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/d/l;

    goto :goto_0
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 9
    check-cast p1, Lco/uk/getmondo/common/j/g$a;

    invoke-virtual {p0, p1}, Lco/uk/getmondo/common/o$b;->a(Lco/uk/getmondo/common/j/g$a;)Lco/uk/getmondo/d/l;

    move-result-object v0

    return-object v0
.end method
