.class public final Lco/uk/getmondo/common/j/g;
.super Ljava/lang/Object;
.source "RxRealm.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lco/uk/getmondo/common/j/g$a;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000H\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\u00c6\u0002\u0018\u00002\u00020\u0001:\u0001\u0015B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u0010\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006H\u0007J8\u0010\u0007\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u0002H\n0\t0\u0008\"\u0008\u0008\u0000\u0010\n*\u00020\u000b2\u0018\u0010\u000c\u001a\u0014\u0012\u0004\u0012\u00020\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u0002H\n0\u000f0\rH\u0007J\u001a\u0010\u0010\u001a\n\u0012\u0004\u0012\u00020\u0012\u0018\u00010\u00112\u0008\u0010\u0013\u001a\u0004\u0018\u00010\u0014H\u0002\u00a8\u0006\u0016"
    }
    d2 = {
        "Lco/uk/getmondo/common/rx/RxRealm;",
        "",
        "()V",
        "asCompletable",
        "Lio/reactivex/Completable;",
        "realmTransaction",
        "Lio/realm/Realm$Transaction;",
        "asObservable",
        "Lio/reactivex/Observable;",
        "Lco/uk/getmondo/common/rx/RxRealm$Result;",
        "T",
        "Lio/realm/RealmModel;",
        "query",
        "Lkotlin/Function1;",
        "Lio/realm/Realm;",
        "Lio/realm/RealmResults;",
        "calculateDiff",
        "",
        "Lco/uk/getmondo/common/data/QueryResults$Change;",
        "realmChangeSet",
        "Lio/realm/OrderedCollectionChangeSet;",
        "Result",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# static fields
.field public static final a:Lco/uk/getmondo/common/j/g;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 9
    new-instance v0, Lco/uk/getmondo/common/j/g;

    invoke-direct {v0}, Lco/uk/getmondo/common/j/g;-><init>()V

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    check-cast p0, Lco/uk/getmondo/common/j/g;

    sput-object p0, Lco/uk/getmondo/common/j/g;->a:Lco/uk/getmondo/common/j/g;

    return-void
.end method

.method public static final a(Lio/realm/av$a;)Lio/reactivex/b;
    .locals 2

    .prologue
    const-string v0, "realmTransaction"

    invoke-static {p0, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 44
    new-instance v0, Lco/uk/getmondo/common/j/g$b;

    invoke-direct {v0, p0}, Lco/uk/getmondo/common/j/g$b;-><init>(Lio/realm/av$a;)V

    check-cast v0, Lio/reactivex/c/a;

    invoke-static {v0}, Lio/reactivex/b;->a(Lio/reactivex/c/a;)Lio/reactivex/b;

    move-result-object v0

    const-string v1, "Completable.fromAction {\u2026)\n            }\n        }"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public static final a(Lkotlin/d/a/b;)Lio/reactivex/n;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Lio/realm/bb;",
            ">(",
            "Lkotlin/d/a/b",
            "<-",
            "Lio/realm/av;",
            "+",
            "Lio/realm/bg",
            "<TT;>;>;)",
            "Lio/reactivex/n",
            "<",
            "Lco/uk/getmondo/common/j/g$a",
            "<TT;>;>;"
        }
    .end annotation

    .prologue
    const-string v0, "query"

    invoke-static {p0, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 21
    new-instance v0, Lco/uk/getmondo/common/j/g$c;

    invoke-direct {v0, p0}, Lco/uk/getmondo/common/j/g$c;-><init>(Lkotlin/d/a/b;)V

    check-cast v0, Lio/reactivex/p;

    invoke-static {v0}, Lio/reactivex/n;->create(Lio/reactivex/p;)Lio/reactivex/n;

    move-result-object v0

    const-string v1, "Observable.create<Result\u2026)\n            }\n        }"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public static final synthetic a(Lco/uk/getmondo/common/j/g;Lio/realm/al;)Ljava/util/List;
    .locals 1

    .prologue
    .line 9
    invoke-direct {p0, p1}, Lco/uk/getmondo/common/j/g;->a(Lio/realm/al;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method private final a(Lio/realm/al;)Ljava/util/List;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/realm/al;",
            ")",
            "Ljava/util/List",
            "<",
            "Lco/uk/getmondo/common/b/b$a;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 52
    if-nez p1, :cond_1

    .line 53
    const/4 v0, 0x0

    .line 68
    :cond_0
    return-object v0

    .line 56
    :cond_1
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    check-cast v0, Ljava/util/List;

    .line 59
    invoke-interface {p1}, Lio/realm/al;->a()[Lio/realm/al$a;

    move-result-object v1

    if-eqz v1, :cond_2

    check-cast v1, [Ljava/lang/Object;

    invoke-static {v1}, Lkotlin/a/g;->f([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    if-eqz v1, :cond_2

    check-cast v1, Ljava/lang/Iterable;

    .line 95
    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lio/realm/al$a;

    .line 60
    new-instance v3, Lco/uk/getmondo/common/b/b$a;

    sget-object v5, Lco/uk/getmondo/common/b/b$a$a;->b:Lco/uk/getmondo/common/b/b$a$a;

    iget v6, v1, Lio/realm/al$a;->a:I

    iget v1, v1, Lio/realm/al$a;->b:I

    invoke-direct {v3, v5, v6, v1}, Lco/uk/getmondo/common/b/b$a;-><init>(Lco/uk/getmondo/common/b/b$a$a;II)V

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 61
    nop

    goto :goto_0

    .line 62
    :cond_2
    invoke-interface {p1}, Lio/realm/al;->b()[Lio/realm/al$a;

    move-result-object v1

    if-eqz v1, :cond_3

    check-cast v1, [Ljava/lang/Object;

    move v3, v4

    .line 97
    :goto_1
    array-length v2, v1

    if-ge v3, v2, :cond_3

    aget-object v2, v1, v3

    check-cast v2, Lio/realm/al$a;

    .line 63
    new-instance v5, Lco/uk/getmondo/common/b/b$a;

    sget-object v6, Lco/uk/getmondo/common/b/b$a$a;->a:Lco/uk/getmondo/common/b/b$a$a;

    iget v7, v2, Lio/realm/al$a;->a:I

    iget v2, v2, Lio/realm/al$a;->b:I

    invoke-direct {v5, v6, v7, v2}, Lco/uk/getmondo/common/b/b$a;-><init>(Lco/uk/getmondo/common/b/b$a$a;II)V

    invoke-interface {v0, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 64
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_1

    .line 65
    :cond_3
    invoke-interface {p1}, Lio/realm/al;->c()[Lio/realm/al$a;

    move-result-object v1

    if-eqz v1, :cond_0

    check-cast v1, [Ljava/lang/Object;

    .line 99
    :goto_2
    array-length v2, v1

    if-ge v4, v2, :cond_0

    aget-object v2, v1, v4

    check-cast v2, Lio/realm/al$a;

    .line 66
    new-instance v3, Lco/uk/getmondo/common/b/b$a;

    sget-object v5, Lco/uk/getmondo/common/b/b$a$a;->c:Lco/uk/getmondo/common/b/b$a$a;

    iget v6, v2, Lio/realm/al$a;->a:I

    iget v2, v2, Lio/realm/al$a;->b:I

    invoke-direct {v3, v5, v6, v2}, Lco/uk/getmondo/common/b/b$a;-><init>(Lco/uk/getmondo/common/b/b$a$a;II)V

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 67
    add-int/lit8 v4, v4, 0x1

    goto :goto_2
.end method
