.class public final Lco/uk/getmondo/common/j/e;
.super Ljava/lang/Object;
.source "RxDialog.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000*\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\u0008\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002JC\u0010\u0003\u001a\u0008\u0012\u0004\u0012\u0002H\u00050\u0004\"\u0004\u0008\u0000\u0010\u00052-\u0010\u0006\u001a)\u0012\u001f\u0012\u001d\u0012\u0004\u0012\u0002H\u0005\u0012\u0004\u0012\u00020\u00080\u0007\u00a2\u0006\u000c\u0008\t\u0012\u0008\u0008\n\u0012\u0004\u0008\u0008(\u000b\u0012\u0004\u0012\u00020\u000c0\u0007H\u0007\u00a8\u0006\r"
    }
    d2 = {
        "Lco/uk/getmondo/common/rx/RxDialog;",
        "",
        "()V",
        "create",
        "Lio/reactivex/Maybe;",
        "T",
        "createDialog",
        "Lkotlin/Function1;",
        "",
        "Lkotlin/ParameterName;",
        "name",
        "setResult",
        "Landroid/app/Dialog;",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# static fields
.field public static final a:Lco/uk/getmondo/common/j/e;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 7
    new-instance v0, Lco/uk/getmondo/common/j/e;

    invoke-direct {v0}, Lco/uk/getmondo/common/j/e;-><init>()V

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    check-cast p0, Lco/uk/getmondo/common/j/e;

    sput-object p0, Lco/uk/getmondo/common/j/e;->a:Lco/uk/getmondo/common/j/e;

    return-void
.end method

.method public static final a(Lkotlin/d/a/b;)Lio/reactivex/h;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lkotlin/d/a/b",
            "<-",
            "Lkotlin/d/a/b",
            "<-TT;",
            "Lkotlin/n;",
            ">;+",
            "Landroid/app/Dialog;",
            ">;)",
            "Lio/reactivex/h",
            "<TT;>;"
        }
    .end annotation

    .prologue
    const-string v0, "createDialog"

    invoke-static {p0, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 18
    new-instance v0, Lco/uk/getmondo/common/j/e$a;

    invoke-direct {v0, p0}, Lco/uk/getmondo/common/j/e$a;-><init>(Lkotlin/d/a/b;)V

    check-cast v0, Lio/reactivex/k;

    invoke-static {v0}, Lio/reactivex/h;->a(Lio/reactivex/k;)Lio/reactivex/h;

    move-result-object v0

    const-string v1, "Maybe.create { emitter -\u2026  dialog.show()\n        }"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method
