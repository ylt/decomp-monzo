.class public final Lco/uk/getmondo/common/j/g$a;
.super Ljava/lang/Object;
.source "RxRealm.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lco/uk/getmondo/common/j/g;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T::",
        "Lio/realm/bb;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000:\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0002\u0008\n\n\u0002\u0010 \n\u0002\u0008\u0003\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u000e\n\u0000\u0008\u0086\u0008\u0018\u0000*\n\u0008\u0000\u0010\u0001 \u0001*\u00020\u00022\u00020\u0003B\u001b\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u000c\u0010\u0006\u001a\u0008\u0012\u0004\u0012\u00028\u00000\u0007\u00a2\u0006\u0002\u0010\u0008J\t\u0010\u000f\u001a\u00020\u0005H\u00c2\u0003J\u000f\u0010\u0010\u001a\u0008\u0012\u0004\u0012\u00028\u00000\u0007H\u00c6\u0003J)\u0010\u0011\u001a\u0008\u0012\u0004\u0012\u00028\u00000\u00002\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u00052\u000e\u0008\u0002\u0010\u0006\u001a\u0008\u0012\u0004\u0012\u00028\u00000\u0007H\u00c6\u0001J\r\u0010\u0012\u001a\u0004\u0018\u00018\u0000\u00a2\u0006\u0002\u0010\u0013J\u000c\u0010\u0014\u001a\u0008\u0012\u0004\u0012\u00028\u00000\u0015J\u0013\u0010\u0016\u001a\u00020\n2\u0008\u0010\u0017\u001a\u0004\u0018\u00010\u0003H\u00d6\u0003J\t\u0010\u0018\u001a\u00020\u0019H\u00d6\u0001J\t\u0010\u001a\u001a\u00020\u001bH\u00d6\u0001R\u0011\u0010\t\u001a\u00020\n8F\u00a2\u0006\u0006\u001a\u0004\u0008\t\u0010\u000bR\u0011\u0010\u000c\u001a\u00020\n8F\u00a2\u0006\u0006\u001a\u0004\u0008\u000c\u0010\u000bR\u0017\u0010\u0006\u001a\u0008\u0012\u0004\u0012\u00028\u00000\u0007\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\r\u0010\u000eR\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001c"
    }
    d2 = {
        "Lco/uk/getmondo/common/rx/RxRealm$Result;",
        "T",
        "Lio/realm/RealmModel;",
        "",
        "realm",
        "Lio/realm/Realm;",
        "queryResults",
        "Lco/uk/getmondo/common/data/QueryResults;",
        "(Lio/realm/Realm;Lco/uk/getmondo/common/data/QueryResults;)V",
        "isEmpty",
        "",
        "()Z",
        "isNotEmpty",
        "getQueryResults",
        "()Lco/uk/getmondo/common/data/QueryResults;",
        "component1",
        "component2",
        "copy",
        "copyFirstFromRealm",
        "()Lio/realm/RealmModel;",
        "copyFromRealm",
        "",
        "equals",
        "other",
        "hashCode",
        "",
        "toString",
        "",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field private final a:Lio/realm/av;

.field private final b:Lco/uk/getmondo/common/b/b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lco/uk/getmondo/common/b/b",
            "<TT;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lio/realm/av;Lco/uk/getmondo/common/b/b;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/realm/av;",
            "Lco/uk/getmondo/common/b/b",
            "<+TT;>;)V"
        }
    .end annotation

    .prologue
    const-string v0, "realm"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "queryResults"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 71
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lco/uk/getmondo/common/j/g$a;->a:Lio/realm/av;

    iput-object p2, p0, Lco/uk/getmondo/common/j/g$a;->b:Lco/uk/getmondo/common/b/b;

    return-void
.end method


# virtual methods
.method public final a()Z
    .locals 1

    .prologue
    .line 76
    iget-object v0, p0, Lco/uk/getmondo/common/j/g$a;->b:Lco/uk/getmondo/common/b/b;

    invoke-virtual {v0}, Lco/uk/getmondo/common/b/b;->a()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 78
    iget-object v0, p0, Lco/uk/getmondo/common/j/g$a;->b:Lco/uk/getmondo/common/b/b;

    invoke-virtual {v0}, Lco/uk/getmondo/common/b/b;->a()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    return v0
.end method

.method public final c()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 81
    iget-object v1, p0, Lco/uk/getmondo/common/j/g$a;->a:Lio/realm/av;

    iget-object v0, p0, Lco/uk/getmondo/common/j/g$a;->b:Lco/uk/getmondo/common/b/b;

    invoke-virtual {v0}, Lco/uk/getmondo/common/b/b;->a()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    invoke-virtual {v1, v0}, Lio/realm/av;->a(Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object v0

    const-string v1, "realm.copyFromRealm(queryResults.results)"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public final d()Lio/realm/bb;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .prologue
    .line 85
    iget-object v0, p0, Lco/uk/getmondo/common/j/g$a;->b:Lco/uk/getmondo/common/b/b;

    invoke-virtual {v0}, Lco/uk/getmondo/common/b/b;->a()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 86
    const/4 v0, 0x0

    .line 89
    :goto_0
    return-object v0

    :cond_0
    iget-object v1, p0, Lco/uk/getmondo/common/j/g$a;->a:Lio/realm/av;

    iget-object v0, p0, Lco/uk/getmondo/common/j/g$a;->b:Lco/uk/getmondo/common/b/b;

    invoke-virtual {v0}, Lco/uk/getmondo/common/b/b;->a()Ljava/util/List;

    move-result-object v0

    const/4 v2, 0x0

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/realm/bb;

    invoke-virtual {v1, v0}, Lio/realm/av;->e(Lio/realm/bb;)Lio/realm/bb;

    move-result-object v0

    goto :goto_0
.end method

.method public final e()Lco/uk/getmondo/common/b/b;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lco/uk/getmondo/common/b/b",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 73
    iget-object v0, p0, Lco/uk/getmondo/common/j/g$a;->b:Lco/uk/getmondo/common/b/b;

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_0

    instance-of v0, p1, Lco/uk/getmondo/common/j/g$a;

    if-eqz v0, :cond_1

    check-cast p1, Lco/uk/getmondo/common/j/g$a;

    iget-object v0, p0, Lco/uk/getmondo/common/j/g$a;->a:Lio/realm/av;

    iget-object v1, p1, Lco/uk/getmondo/common/j/g$a;->a:Lio/realm/av;

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lco/uk/getmondo/common/j/g$a;->b:Lco/uk/getmondo/common/b/b;

    iget-object v1, p1, Lco/uk/getmondo/common/j/g$a;->b:Lco/uk/getmondo/common/b/b;

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    const/4 v1, 0x0

    iget-object v0, p0, Lco/uk/getmondo/common/j/g$a;->a:Lio/realm/av;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lco/uk/getmondo/common/j/g$a;->b:Lco/uk/getmondo/common/b/b;

    if-eqz v2, :cond_0

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_0
    add-int/2addr v0, v1

    return v0

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Result(realm="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/common/j/g$a;->a:Lio/realm/av;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", queryResults="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/common/j/g$a;->b:Lco/uk/getmondo/common/b/b;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
