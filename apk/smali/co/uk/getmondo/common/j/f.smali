.class public final Lco/uk/getmondo/common/j/f;
.super Ljava/lang/Object;
.source "RxExt.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000@\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u001a@\u0010\u0000\u001a\u0014\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u0002H\u0003\u0012\u0004\u0012\u0002H\u00040\u00020\u0001\"\u0004\u0008\u0000\u0010\u0003\"\u0004\u0008\u0001\u0010\u00042\u000c\u0010\u0005\u001a\u0008\u0012\u0004\u0012\u0002H\u00030\u00012\u000c\u0010\u0006\u001a\u0008\u0012\u0004\u0012\u0002H\u00040\u0001\u001a\u0015\u0010\u0007\u001a\u00020\u0008*\u00020\u00082\u0006\u0010\t\u001a\u00020\nH\u0086\u0002\u001a#\u0010\u000b\u001a\u0008\u0012\u0004\u0012\u0002H\u00030\u000c\"\u0004\u0008\u0000\u0010\u0003*\u00020\r2\u0006\u0010\u000e\u001a\u0002H\u0003\u00a2\u0006\u0002\u0010\u000f\u001a\u001c\u0010\u0010\u001a\u0008\u0012\u0004\u0012\u0002H\u00030\u000c\"\u0004\u0008\u0000\u0010\u0003*\u0008\u0012\u0004\u0012\u0002H\u00030\u0011\u001a\"\u0010\u0012\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u0002H\u00030\u00130\u0011\"\u0004\u0008\u0000\u0010\u0003*\u0008\u0012\u0004\u0012\u0002H\u00030\u000c\u001a<\u0010\u0014\u001a\u0014\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u0002H\u0003\u0012\u0004\u0012\u0002H\u00040\u00020\u0001\"\u0004\u0008\u0000\u0010\u0003\"\u0004\u0008\u0001\u0010\u0004*\u0008\u0012\u0004\u0012\u0002H\u00030\u00012\u000c\u0010\u0015\u001a\u0008\u0012\u0004\u0012\u0002H\u00040\u0016\u001a<\u0010\u0017\u001a\u0014\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u0002H\u0003\u0012\u0004\u0012\u0002H\u00040\u00020\u0011\"\u0004\u0008\u0000\u0010\u0003\"\u0004\u0008\u0001\u0010\u0004*\u0008\u0012\u0004\u0012\u0002H\u00030\u00112\u000c\u0010\u0015\u001a\u0008\u0012\u0004\u0012\u0002H\u00040\u0018\u00a8\u0006\u0019"
    }
    d2 = {
        "combineLatest",
        "Lio/reactivex/Observable;",
        "Lkotlin/Pair;",
        "T",
        "U",
        "source1",
        "source2",
        "plus",
        "Lio/reactivex/disposables/CompositeDisposable;",
        "disposable",
        "Lio/reactivex/disposables/Disposable;",
        "toMaybeDefaultOnErrorComplete",
        "Lio/reactivex/Maybe;",
        "Lio/reactivex/Completable;",
        "completionValue",
        "(Lio/reactivex/Completable;Ljava/lang/Object;)Lio/reactivex/Maybe;",
        "toMaybeOnErrorComplete",
        "Lio/reactivex/Single;",
        "toOptionalSingle",
        "Lcom/memoizrlabs/poweroptional/Optional;",
        "withLatestFrom",
        "other",
        "Lio/reactivex/ObservableSource;",
        "zipWith",
        "Lio/reactivex/SingleSource;",
        "app_monzoPrepaidRelease"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# direct methods
.method public static final a(Lio/reactivex/b/a;Lio/reactivex/b/b;)Lio/reactivex/b/a;
    .locals 1

    .prologue
    const-string v0, "$receiver"

    invoke-static {p0, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "disposable"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 50
    invoke-virtual {p0, p1}, Lio/reactivex/b/a;->a(Lio/reactivex/b/b;)Z

    .line 51
    return-object p0
.end method

.method public static final a(Lio/reactivex/b;Ljava/lang/Object;)Lio/reactivex/h;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lio/reactivex/b;",
            "TT;)",
            "Lio/reactivex/h",
            "<TT;>;"
        }
    .end annotation

    .prologue
    const-string v0, "$receiver"

    invoke-static {p0, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 28
    invoke-virtual {p0, p1}, Lio/reactivex/b;->a(Ljava/lang/Object;)Lio/reactivex/v;

    move-result-object v0

    invoke-virtual {v0}, Lio/reactivex/v;->e()Lio/reactivex/h;

    move-result-object v0

    invoke-virtual {v0}, Lio/reactivex/h;->f()Lio/reactivex/h;

    move-result-object v0

    const-string v1, "toSingleDefault(completi\u2026Maybe().onErrorComplete()"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public static final a(Lio/reactivex/v;)Lio/reactivex/h;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lio/reactivex/v",
            "<TT;>;)",
            "Lio/reactivex/h",
            "<TT;>;"
        }
    .end annotation

    .prologue
    const-string v0, "$receiver"

    invoke-static {p0, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 22
    invoke-virtual {p0}, Lio/reactivex/v;->e()Lio/reactivex/h;

    move-result-object v0

    invoke-virtual {v0}, Lio/reactivex/h;->f()Lio/reactivex/h;

    move-result-object v0

    const-string v1, "toMaybe().onErrorComplete()"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public static final a(Lio/reactivex/n;Lio/reactivex/n;)Lio/reactivex/n;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            "U:",
            "Ljava/lang/Object;",
            ">(",
            "Lio/reactivex/n",
            "<TT;>;",
            "Lio/reactivex/n",
            "<TU;>;)",
            "Lio/reactivex/n",
            "<",
            "Lkotlin/h",
            "<TT;TU;>;>;"
        }
    .end annotation

    .prologue
    const-string v0, "source1"

    invoke-static {p0, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "source2"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 44
    check-cast p0, Lio/reactivex/r;

    check-cast p1, Lio/reactivex/r;

    .line 45
    sget-object v0, Lco/uk/getmondo/common/j/f$a;->a:Lco/uk/getmondo/common/j/f$a;

    check-cast v0, Lio/reactivex/c/c;

    .line 44
    invoke-static {p0, p1, v0}, Lio/reactivex/n;->combineLatest(Lio/reactivex/r;Lio/reactivex/r;Lio/reactivex/c/c;)Lio/reactivex/n;

    move-result-object v0

    const-string v1, "Observable.combineLatest\u2026> { t, u -> Pair(t, u) })"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 45
    return-object v0
.end method

.method public static final a(Lio/reactivex/n;Lio/reactivex/r;)Lio/reactivex/n;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            "U:",
            "Ljava/lang/Object;",
            ">(",
            "Lio/reactivex/n",
            "<TT;>;",
            "Lio/reactivex/r",
            "<TU;>;)",
            "Lio/reactivex/n",
            "<",
            "Lkotlin/h",
            "<TT;TU;>;>;"
        }
    .end annotation

    .prologue
    const-string v0, "$receiver"

    invoke-static {p0, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "other"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 41
    sget-object v0, Lco/uk/getmondo/common/j/f$b;->a:Lco/uk/getmondo/common/j/f$b;

    check-cast v0, Lio/reactivex/c/c;

    invoke-virtual {p0, p1, v0}, Lio/reactivex/n;->withLatestFrom(Lio/reactivex/r;Lio/reactivex/c/c;)Lio/reactivex/n;

    move-result-object v0

    const-string v1, "withLatestFrom(other, Bi\u2026n { t, u -> Pair(t, u) })"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public static final a(Lio/reactivex/v;Lio/reactivex/z;)Lio/reactivex/v;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            "U:",
            "Ljava/lang/Object;",
            ">(",
            "Lio/reactivex/v",
            "<TT;>;",
            "Lio/reactivex/z",
            "<TU;>;)",
            "Lio/reactivex/v",
            "<",
            "Lkotlin/h",
            "<TT;TU;>;>;"
        }
    .end annotation

    .prologue
    const-string v0, "$receiver"

    invoke-static {p0, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "other"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 19
    sget-object v0, Lco/uk/getmondo/common/j/f$c;->a:Lco/uk/getmondo/common/j/f$c;

    check-cast v0, Lio/reactivex/c/c;

    invoke-virtual {p0, p1, v0}, Lio/reactivex/v;->a(Lio/reactivex/z;Lio/reactivex/c/c;)Lio/reactivex/v;

    move-result-object v0

    const-string v1, "zipWith(other, BiFunction { t, u -> Pair(t, u) })"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method
