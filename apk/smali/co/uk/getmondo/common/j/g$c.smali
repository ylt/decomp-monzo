.class final Lco/uk/getmondo/common/j/g$c;
.super Ljava/lang/Object;
.source "RxRealm.kt"

# interfaces
.implements Lio/reactivex/p;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lco/uk/getmondo/common/j/g;->a(Lkotlin/d/a/b;)Lio/reactivex/n;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/p",
        "<TT;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000\u001a\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u0001\"\u0008\u0008\u0000\u0010\u0002*\u00020\u00032 \u0010\u0004\u001a\u001c\u0012\u0018\u0012\u0016\u0012\u0004\u0012\u0002H\u0002 \u0007*\n\u0012\u0004\u0012\u0002H\u0002\u0018\u00010\u00060\u00060\u0005H\n\u00a2\u0006\u0002\u0008\u0008"
    }
    d2 = {
        "<anonymous>",
        "",
        "T",
        "Lio/realm/RealmModel;",
        "emitter",
        "Lio/reactivex/ObservableEmitter;",
        "Lco/uk/getmondo/common/rx/RxRealm$Result;",
        "kotlin.jvm.PlatformType",
        "subscribe"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field final synthetic a:Lkotlin/d/a/b;


# direct methods
.method constructor <init>(Lkotlin/d/a/b;)V
    .locals 0

    iput-object p1, p0, Lco/uk/getmondo/common/j/g$c;->a:Lkotlin/d/a/b;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lio/reactivex/o;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/o",
            "<",
            "Lco/uk/getmondo/common/j/g$a",
            "<TT;>;>;)V"
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    const-string v0, "emitter"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 22
    invoke-static {}, Lio/realm/av;->n()Lio/realm/av;

    move-result-object v3

    .line 23
    new-instance v0, Lco/uk/getmondo/common/j/g$c$a;

    invoke-direct {v0, p1, v3}, Lco/uk/getmondo/common/j/g$c$a;-><init>(Lio/reactivex/o;Lio/realm/av;)V

    check-cast v0, Lio/realm/am;

    .line 28
    iget-object v1, p0, Lco/uk/getmondo/common/j/g$c;->a:Lkotlin/d/a/b;

    const-string v2, "realm"

    invoke-static {v3, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v1, v3}, Lkotlin/d/a/b;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lio/realm/bg;

    .line 29
    invoke-virtual {v1, v0}, Lio/realm/bg;->a(Lio/realm/am;)V

    .line 30
    new-instance v2, Lco/uk/getmondo/common/j/g$c$1;

    invoke-direct {v2, v1, v0, v3}, Lco/uk/getmondo/common/j/g$c$1;-><init>(Lio/realm/bg;Lio/realm/am;Lio/realm/av;)V

    move-object v0, v2

    check-cast v0, Lio/reactivex/c/f;

    invoke-interface {p1, v0}, Lio/reactivex/o;->a(Lio/reactivex/c/f;)V

    .line 36
    invoke-virtual {v1}, Lio/realm/bg;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {v1}, Lio/realm/bg;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 37
    new-instance v0, Lco/uk/getmondo/common/j/g$a;

    new-instance v2, Lco/uk/getmondo/common/b/b;

    check-cast v1, Ljava/util/List;

    const/4 v4, 0x2

    invoke-direct {v2, v1, v5, v4, v5}, Lco/uk/getmondo/common/b/b;-><init>(Ljava/util/List;Ljava/util/List;ILkotlin/d/b/i;)V

    invoke-direct {v0, v3, v2}, Lco/uk/getmondo/common/j/g$a;-><init>(Lio/realm/av;Lco/uk/getmondo/common/b/b;)V

    invoke-interface {p1, v0}, Lio/reactivex/o;->a(Ljava/lang/Object;)V

    .line 39
    :cond_0
    return-void
.end method
