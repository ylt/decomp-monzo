.class final Lco/uk/getmondo/common/j/e$a;
.super Ljava/lang/Object;
.source "RxDialog.kt"

# interfaces
.implements Lio/reactivex/k;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lco/uk/getmondo/common/j/e;->a(Lkotlin/d/a/b;)Lio/reactivex/h;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/k",
        "<TT;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000\u0012\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u0001\"\u0004\u0008\u0000\u0010\u00022\u0014\u0010\u0003\u001a\u0010\u0012\u000c\u0012\n \u0005*\u0004\u0018\u0001H\u0002H\u00020\u0004H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "",
        "T",
        "emitter",
        "Lio/reactivex/MaybeEmitter;",
        "kotlin.jvm.PlatformType",
        "subscribe"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field final synthetic a:Lkotlin/d/a/b;


# direct methods
.method constructor <init>(Lkotlin/d/a/b;)V
    .locals 0

    iput-object p1, p0, Lco/uk/getmondo/common/j/e$a;->a:Lkotlin/d/a/b;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lio/reactivex/i;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/i",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    const-string v0, "emitter"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 19
    iget-object v0, p0, Lco/uk/getmondo/common/j/e$a;->a:Lkotlin/d/a/b;

    new-instance v1, Lco/uk/getmondo/common/j/e$a$a;

    invoke-direct {v1, p1}, Lco/uk/getmondo/common/j/e$a$a;-><init>(Lio/reactivex/i;)V

    invoke-interface {v0, v1}, Lkotlin/d/a/b;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Dialog;

    .line 20
    new-instance v1, Lco/uk/getmondo/common/j/e$a$1;

    invoke-direct {v1, p1}, Lco/uk/getmondo/common/j/e$a$1;-><init>(Lio/reactivex/i;)V

    check-cast v1, Landroid/content/DialogInterface$OnDismissListener;

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 25
    new-instance v1, Lco/uk/getmondo/common/j/e$a$2;

    invoke-direct {v1, v0}, Lco/uk/getmondo/common/j/e$a$2;-><init>(Landroid/app/Dialog;)V

    check-cast v1, Lio/reactivex/c/f;

    invoke-interface {p1, v1}, Lio/reactivex/i;->a(Lio/reactivex/c/f;)V

    .line 29
    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    .line 30
    return-void
.end method
