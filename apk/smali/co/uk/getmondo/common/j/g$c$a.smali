.class final Lco/uk/getmondo/common/j/g$c$a;
.super Ljava/lang/Object;
.source "RxRealm.kt"

# interfaces
.implements Lio/realm/am;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lco/uk/getmondo/common/j/g$c;->a(Lio/reactivex/o;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/realm/am",
        "<",
        "Lio/realm/bg",
        "<TT;>;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000\u001c\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u0001\"\u0008\u0008\u0000\u0010\u0002*\u00020\u00032\u001a\u0010\u0004\u001a\u0016\u0012\u0004\u0012\u0002H\u0002 \u0006*\n\u0012\u0004\u0012\u0002H\u0002\u0018\u00010\u00050\u00052\u000e\u0010\u0007\u001a\n \u0006*\u0004\u0018\u00010\u00080\u0008H\n\u00a2\u0006\u0002\u0008\t"
    }
    d2 = {
        "<anonymous>",
        "",
        "T",
        "Lio/realm/RealmModel;",
        "realmResults",
        "Lio/realm/RealmResults;",
        "kotlin.jvm.PlatformType",
        "changeSet",
        "Lio/realm/OrderedCollectionChangeSet;",
        "onChange"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field final synthetic a:Lio/reactivex/o;

.field final synthetic b:Lio/realm/av;


# direct methods
.method constructor <init>(Lio/reactivex/o;Lio/realm/av;)V
    .locals 0

    iput-object p1, p0, Lco/uk/getmondo/common/j/g$c$a;->a:Lio/reactivex/o;

    iput-object p2, p0, Lco/uk/getmondo/common/j/g$c$a;->b:Lio/realm/av;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lio/realm/bg;Lio/realm/al;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/realm/bg",
            "<TT;>;",
            "Lio/realm/al;",
            ")V"
        }
    .end annotation

    .prologue
    .line 24
    invoke-virtual {p1}, Lio/realm/bg;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lio/realm/bg;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 25
    iget-object v0, p0, Lco/uk/getmondo/common/j/g$c$a;->a:Lio/reactivex/o;

    new-instance v1, Lco/uk/getmondo/common/j/g$a;

    iget-object v2, p0, Lco/uk/getmondo/common/j/g$c$a;->b:Lio/realm/av;

    const-string v3, "realm"

    invoke-static {v2, v3}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v3, Lco/uk/getmondo/common/b/b;

    const-string v4, "realmResults"

    invoke-static {p1, v4}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Ljava/util/List;

    sget-object v4, Lco/uk/getmondo/common/j/g;->a:Lco/uk/getmondo/common/j/g;

    invoke-static {v4, p2}, Lco/uk/getmondo/common/j/g;->a(Lco/uk/getmondo/common/j/g;Lio/realm/al;)Ljava/util/List;

    move-result-object v4

    invoke-direct {v3, p1, v4}, Lco/uk/getmondo/common/b/b;-><init>(Ljava/util/List;Ljava/util/List;)V

    invoke-direct {v1, v2, v3}, Lco/uk/getmondo/common/j/g$a;-><init>(Lio/realm/av;Lco/uk/getmondo/common/b/b;)V

    invoke-interface {v0, v1}, Lio/reactivex/o;->a(Ljava/lang/Object;)V

    .line 27
    :cond_0
    return-void
.end method

.method public bridge synthetic a(Ljava/lang/Object;Lio/realm/al;)V
    .locals 0

    .prologue
    .line 9
    check-cast p1, Lio/realm/bg;

    invoke-virtual {p0, p1, p2}, Lco/uk/getmondo/common/j/g$c$a;->a(Lio/realm/bg;Lio/realm/al;)V

    return-void
.end method
