.class public Lco/uk/getmondo/common/a;
.super Ljava/lang/Object;
.source "AnalyticsService.java"


# instance fields
.field private final a:Lco/uk/getmondo/api/AnalyticsApi;

.field private final b:Lio/reactivex/u;


# direct methods
.method public constructor <init>(Lco/uk/getmondo/api/AnalyticsApi;Lio/reactivex/u;)V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    iput-object p1, p0, Lco/uk/getmondo/common/a;->a:Lco/uk/getmondo/api/AnalyticsApi;

    .line 20
    iput-object p2, p0, Lco/uk/getmondo/common/a;->b:Lio/reactivex/u;

    .line 21
    return-void
.end method

.method static synthetic b(Lco/uk/getmondo/api/model/tracking/Impression;)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 26
    const-string v0, "Impression tracked: %s"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-virtual {p0}, Lco/uk/getmondo/api/model/tracking/Impression;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ld/a/a;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public a(Lco/uk/getmondo/api/model/tracking/Impression;)V
    .locals 3

    .prologue
    .line 24
    iget-object v0, p0, Lco/uk/getmondo/common/a;->a:Lco/uk/getmondo/api/AnalyticsApi;

    invoke-interface {v0, p1}, Lco/uk/getmondo/api/AnalyticsApi;->trackEvent(Lco/uk/getmondo/api/model/tracking/Impression;)Lio/reactivex/b;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/common/a;->b:Lio/reactivex/u;

    .line 25
    invoke-virtual {v0, v1}, Lio/reactivex/b;->b(Lio/reactivex/u;)Lio/reactivex/b;

    move-result-object v0

    invoke-static {p1}, Lco/uk/getmondo/common/b;->a(Lco/uk/getmondo/api/model/tracking/Impression;)Lio/reactivex/c/a;

    move-result-object v1

    invoke-static {}, Lco/uk/getmondo/common/c;->a()Lio/reactivex/c/g;

    move-result-object v2

    .line 26
    invoke-virtual {v0, v1, v2}, Lio/reactivex/b;->a(Lio/reactivex/c/a;Lio/reactivex/c/g;)Lio/reactivex/b/b;

    .line 27
    return-void
.end method
