.class public final enum Lco/uk/getmondo/common/t;
.super Ljava/lang/Enum;
.source "NavigationFlow.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lco/uk/getmondo/common/t;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lco/uk/getmondo/common/t;

.field public static final enum b:Lco/uk/getmondo/common/t;

.field public static final enum c:Lco/uk/getmondo/common/t;

.field private static final synthetic d:[Lco/uk/getmondo/common/t;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 5
    new-instance v0, Lco/uk/getmondo/common/t;

    const-string v1, "MONZO_ME_DEEP_LINK"

    invoke-direct {v0, v1, v2}, Lco/uk/getmondo/common/t;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lco/uk/getmondo/common/t;->a:Lco/uk/getmondo/common/t;

    .line 6
    new-instance v0, Lco/uk/getmondo/common/t;

    const-string v1, "CONTACTS"

    invoke-direct {v0, v1, v3}, Lco/uk/getmondo/common/t;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lco/uk/getmondo/common/t;->b:Lco/uk/getmondo/common/t;

    .line 7
    new-instance v0, Lco/uk/getmondo/common/t;

    const-string v1, "TRANSACTION_SPLIT"

    invoke-direct {v0, v1, v4}, Lco/uk/getmondo/common/t;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lco/uk/getmondo/common/t;->c:Lco/uk/getmondo/common/t;

    .line 4
    const/4 v0, 0x3

    new-array v0, v0, [Lco/uk/getmondo/common/t;

    sget-object v1, Lco/uk/getmondo/common/t;->a:Lco/uk/getmondo/common/t;

    aput-object v1, v0, v2

    sget-object v1, Lco/uk/getmondo/common/t;->b:Lco/uk/getmondo/common/t;

    aput-object v1, v0, v3

    sget-object v1, Lco/uk/getmondo/common/t;->c:Lco/uk/getmondo/common/t;

    aput-object v1, v0, v4

    sput-object v0, Lco/uk/getmondo/common/t;->d:[Lco/uk/getmondo/common/t;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 4
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lco/uk/getmondo/common/t;
    .locals 1

    .prologue
    .line 4
    const-class v0, Lco/uk/getmondo/common/t;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/common/t;

    return-object v0
.end method

.method public static values()[Lco/uk/getmondo/common/t;
    .locals 1

    .prologue
    .line 4
    sget-object v0, Lco/uk/getmondo/common/t;->d:[Lco/uk/getmondo/common/t;

    invoke-virtual {v0}, [Lco/uk/getmondo/common/t;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lco/uk/getmondo/common/t;

    return-object v0
.end method
