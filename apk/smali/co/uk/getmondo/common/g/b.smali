.class public final Lco/uk/getmondo/common/g/b;
.super Ljava/lang/Object;
.source "RuntimeTypeAdapterFactory.java"

# interfaces
.implements Lcom/google/gson/t;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/google/gson/t;"
    }
.end annotation


# instance fields
.field private final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private final b:Ljava/lang/String;

.field private final c:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Class",
            "<*>;>;"
        }
    .end annotation
.end field

.field private final d:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Class",
            "<*>;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private e:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Ljava/lang/Class;Ljava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 131
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 127
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v0, p0, Lco/uk/getmondo/common/g/b;->c:Ljava/util/Map;

    .line 128
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v0, p0, Lco/uk/getmondo/common/g/b;->d:Ljava/util/Map;

    .line 132
    if-eqz p2, :cond_0

    if-nez p1, :cond_1

    .line 133
    :cond_0
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 135
    :cond_1
    iput-object p1, p0, Lco/uk/getmondo/common/g/b;->a:Ljava/lang/Class;

    .line 136
    iput-object p2, p0, Lco/uk/getmondo/common/g/b;->b:Ljava/lang/String;

    .line 137
    return-void
.end method

.method public static a(Ljava/lang/Class;Ljava/lang/String;)Lco/uk/getmondo/common/g/b;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;",
            "Ljava/lang/String;",
            ")",
            "Lco/uk/getmondo/common/g/b",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 144
    new-instance v0, Lco/uk/getmondo/common/g/b;

    invoke-direct {v0, p0, p1}, Lco/uk/getmondo/common/g/b;-><init>(Ljava/lang/Class;Ljava/lang/String;)V

    return-object v0
.end method

.method static synthetic a(Lco/uk/getmondo/common/g/b;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 124
    iget-object v0, p0, Lco/uk/getmondo/common/g/b;->b:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic b(Lco/uk/getmondo/common/g/b;)Ljava/lang/Class;
    .locals 1

    .prologue
    .line 124
    iget-object v0, p0, Lco/uk/getmondo/common/g/b;->e:Ljava/lang/Class;

    return-object v0
.end method

.method static synthetic c(Lco/uk/getmondo/common/g/b;)Ljava/lang/Class;
    .locals 1

    .prologue
    .line 124
    iget-object v0, p0, Lco/uk/getmondo/common/g/b;->a:Ljava/lang/Class;

    return-object v0
.end method

.method static synthetic d(Lco/uk/getmondo/common/g/b;)Ljava/util/Map;
    .locals 1

    .prologue
    .line 124
    iget-object v0, p0, Lco/uk/getmondo/common/g/b;->d:Ljava/util/Map;

    return-object v0
.end method


# virtual methods
.method public a(Ljava/lang/Class;)Lco/uk/getmondo/common/g/b;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+TT;>;)",
            "Lco/uk/getmondo/common/g/b",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 192
    if-nez p1, :cond_0

    .line 193
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 195
    :cond_0
    iget-object v0, p0, Lco/uk/getmondo/common/g/b;->e:Ljava/lang/Class;

    if-ne p1, v0, :cond_1

    .line 196
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "type must be unique"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 198
    :cond_1
    iput-object p1, p0, Lco/uk/getmondo/common/g/b;->e:Ljava/lang/Class;

    .line 199
    return-object p0
.end method

.method public a(Lcom/google/gson/f;Lcom/google/gson/c/a;)Lcom/google/gson/s;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<R:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/google/gson/f;",
            "Lcom/google/gson/c/a",
            "<TR;>;)",
            "Lcom/google/gson/s",
            "<TR;>;"
        }
    .end annotation

    .prologue
    .line 203
    invoke-virtual {p2}, Lcom/google/gson/c/a;->a()Ljava/lang/Class;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/common/g/b;->a:Ljava/lang/Class;

    if-eq v0, v1, :cond_0

    .line 204
    const/4 v0, 0x0

    .line 217
    :goto_0
    return-object v0

    .line 207
    :cond_0
    new-instance v2, Ljava/util/LinkedHashMap;

    invoke-direct {v2}, Ljava/util/LinkedHashMap;-><init>()V

    .line 209
    new-instance v3, Ljava/util/LinkedHashMap;

    invoke-direct {v3}, Ljava/util/LinkedHashMap;-><init>()V

    .line 211
    iget-object v0, p0, Lco/uk/getmondo/common/g/b;->c:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 212
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Class;

    invoke-static {v1}, Lcom/google/gson/c/a;->b(Ljava/lang/Class;)Lcom/google/gson/c/a;

    move-result-object v1

    invoke-virtual {p1, p0, v1}, Lcom/google/gson/f;->a(Lcom/google/gson/t;Lcom/google/gson/c/a;)Lcom/google/gson/s;

    move-result-object v1

    .line 213
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v5

    invoke-interface {v2, v5, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 214
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    invoke-interface {v3, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 217
    :cond_1
    new-instance v0, Lco/uk/getmondo/common/g/b$1;

    invoke-direct {v0, p0, p1, v2, v3}, Lco/uk/getmondo/common/g/b$1;-><init>(Lco/uk/getmondo/common/g/b;Lcom/google/gson/f;Ljava/util/Map;Ljava/util/Map;)V

    .line 265
    invoke-virtual {v0}, Lco/uk/getmondo/common/g/b$1;->a()Lcom/google/gson/s;

    move-result-object v0

    goto :goto_0
.end method

.method public b(Ljava/lang/Class;Ljava/lang/String;)Lco/uk/getmondo/common/g/b;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+TT;>;",
            "Ljava/lang/String;",
            ")",
            "Lco/uk/getmondo/common/g/b",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 163
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 164
    :cond_0
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 166
    :cond_1
    iget-object v0, p0, Lco/uk/getmondo/common/g/b;->d:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lco/uk/getmondo/common/g/b;->c:Ljava/util/Map;

    invoke-interface {v0, p2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 167
    :cond_2
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "types and labels must be unique"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 169
    :cond_3
    iget-object v0, p0, Lco/uk/getmondo/common/g/b;->c:Ljava/util/Map;

    invoke-interface {v0, p2, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 170
    iget-object v0, p0, Lco/uk/getmondo/common/g/b;->d:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 171
    return-object p0
.end method
