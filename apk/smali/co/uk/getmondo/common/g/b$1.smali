.class Lco/uk/getmondo/common/g/b$1;
.super Lcom/google/gson/s;
.source "RuntimeTypeAdapterFactory.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lco/uk/getmondo/common/g/b;->a(Lcom/google/gson/f;Lcom/google/gson/c/a;)Lcom/google/gson/s;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/gson/s",
        "<TR;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/google/gson/f;

.field final synthetic b:Ljava/util/Map;

.field final synthetic c:Ljava/util/Map;

.field final synthetic d:Lco/uk/getmondo/common/g/b;


# direct methods
.method constructor <init>(Lco/uk/getmondo/common/g/b;Lcom/google/gson/f;Ljava/util/Map;Ljava/util/Map;)V
    .locals 0

    .prologue
    .line 217
    iput-object p1, p0, Lco/uk/getmondo/common/g/b$1;->d:Lco/uk/getmondo/common/g/b;

    iput-object p2, p0, Lco/uk/getmondo/common/g/b$1;->a:Lcom/google/gson/f;

    iput-object p3, p0, Lco/uk/getmondo/common/g/b$1;->b:Ljava/util/Map;

    iput-object p4, p0, Lco/uk/getmondo/common/g/b$1;->c:Ljava/util/Map;

    invoke-direct {p0}, Lcom/google/gson/s;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/gson/stream/JsonReader;",
            ")TR;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 220
    invoke-static {p1}, Lcom/google/gson/b/j;->a(Lcom/google/gson/stream/JsonReader;)Lcom/google/gson/l;

    move-result-object v1

    .line 221
    invoke-virtual {v1}, Lcom/google/gson/l;->k()Lcom/google/gson/n;

    move-result-object v0

    iget-object v2, p0, Lco/uk/getmondo/common/g/b$1;->d:Lco/uk/getmondo/common/g/b;

    invoke-static {v2}, Lco/uk/getmondo/common/g/b;->a(Lco/uk/getmondo/common/g/b;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/gson/n;->a(Ljava/lang/String;)Lcom/google/gson/l;

    move-result-object v0

    .line 222
    if-nez v0, :cond_0

    iget-object v2, p0, Lco/uk/getmondo/common/g/b$1;->d:Lco/uk/getmondo/common/g/b;

    invoke-static {v2}, Lco/uk/getmondo/common/g/b;->b(Lco/uk/getmondo/common/g/b;)Ljava/lang/Class;

    move-result-object v2

    if-nez v2, :cond_0

    .line 223
    new-instance v0, Lcom/google/gson/JsonParseException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "cannot deserialize "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lco/uk/getmondo/common/g/b$1;->d:Lco/uk/getmondo/common/g/b;

    invoke-static {v2}, Lco/uk/getmondo/common/g/b;->c(Lco/uk/getmondo/common/g/b;)Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " because it does not define a field named "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lco/uk/getmondo/common/g/b$1;->d:Lco/uk/getmondo/common/g/b;

    .line 224
    invoke-static {v2}, Lco/uk/getmondo/common/g/b;->a(Lco/uk/getmondo/common/g/b;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " and no read fallback type is registered"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/gson/JsonParseException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 227
    :cond_0
    if-nez v0, :cond_1

    .line 229
    iget-object v0, p0, Lco/uk/getmondo/common/g/b$1;->a:Lcom/google/gson/f;

    iget-object v2, p0, Lco/uk/getmondo/common/g/b$1;->d:Lco/uk/getmondo/common/g/b;

    iget-object v3, p0, Lco/uk/getmondo/common/g/b$1;->d:Lco/uk/getmondo/common/g/b;

    .line 230
    invoke-static {v3}, Lco/uk/getmondo/common/g/b;->b(Lco/uk/getmondo/common/g/b;)Ljava/lang/Class;

    move-result-object v3

    invoke-static {v3}, Lcom/google/gson/c/a;->b(Ljava/lang/Class;)Lcom/google/gson/c/a;

    move-result-object v3

    .line 229
    invoke-virtual {v0, v2, v3}, Lcom/google/gson/f;->a(Lcom/google/gson/t;Lcom/google/gson/c/a;)Lcom/google/gson/s;

    move-result-object v0

    .line 231
    invoke-virtual {v0, v1}, Lcom/google/gson/s;->a(Lcom/google/gson/l;)Ljava/lang/Object;

    move-result-object v0

    .line 240
    :goto_0
    return-object v0

    .line 233
    :cond_1
    invoke-virtual {v0}, Lcom/google/gson/l;->b()Ljava/lang/String;

    move-result-object v2

    .line 235
    iget-object v0, p0, Lco/uk/getmondo/common/g/b$1;->b:Ljava/util/Map;

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/gson/s;

    .line 236
    if-nez v0, :cond_2

    .line 237
    new-instance v0, Lcom/google/gson/JsonParseException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "cannot deserialize "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v3, p0, Lco/uk/getmondo/common/g/b$1;->d:Lco/uk/getmondo/common/g/b;

    invoke-static {v3}, Lco/uk/getmondo/common/g/b;->c(Lco/uk/getmondo/common/g/b;)Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " subtype named "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "; did you forget to register a subtype?"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/gson/JsonParseException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 240
    :cond_2
    invoke-virtual {v0, v1}, Lcom/google/gson/s;->a(Lcom/google/gson/l;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method public a(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/gson/stream/JsonWriter;",
            "TR;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 245
    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    .line 246
    iget-object v0, p0, Lco/uk/getmondo/common/g/b$1;->d:Lco/uk/getmondo/common/g/b;

    invoke-static {v0}, Lco/uk/getmondo/common/g/b;->d(Lco/uk/getmondo/common/g/b;)Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 248
    iget-object v1, p0, Lco/uk/getmondo/common/g/b$1;->c:Ljava/util/Map;

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/gson/s;

    .line 249
    if-nez v1, :cond_0

    .line 250
    new-instance v0, Lcom/google/gson/JsonParseException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "cannot serialize "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "; did you forget to register a subtype?"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/gson/JsonParseException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 253
    :cond_0
    invoke-virtual {v1, p2}, Lcom/google/gson/s;->a(Ljava/lang/Object;)Lcom/google/gson/l;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/gson/l;->k()Lcom/google/gson/n;

    move-result-object v1

    .line 254
    iget-object v3, p0, Lco/uk/getmondo/common/g/b$1;->d:Lco/uk/getmondo/common/g/b;

    invoke-static {v3}, Lco/uk/getmondo/common/g/b;->a(Lco/uk/getmondo/common/g/b;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/google/gson/n;->b(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 255
    new-instance v0, Lcom/google/gson/JsonParseException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "cannot serialize "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " because it already defines a field named "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lco/uk/getmondo/common/g/b$1;->d:Lco/uk/getmondo/common/g/b;

    .line 256
    invoke-static {v2}, Lco/uk/getmondo/common/g/b;->a(Lco/uk/getmondo/common/g/b;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/gson/JsonParseException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 258
    :cond_1
    new-instance v2, Lcom/google/gson/n;

    invoke-direct {v2}, Lcom/google/gson/n;-><init>()V

    .line 259
    iget-object v3, p0, Lco/uk/getmondo/common/g/b$1;->d:Lco/uk/getmondo/common/g/b;

    invoke-static {v3}, Lco/uk/getmondo/common/g/b;->a(Lco/uk/getmondo/common/g/b;)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Lcom/google/gson/o;

    invoke-direct {v4, v0}, Lcom/google/gson/o;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v3, v4}, Lcom/google/gson/n;->a(Ljava/lang/String;Lcom/google/gson/l;)V

    .line 260
    invoke-virtual {v1}, Lcom/google/gson/n;->o()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 261
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/gson/l;

    invoke-virtual {v2, v1, v0}, Lcom/google/gson/n;->a(Ljava/lang/String;Lcom/google/gson/l;)V

    goto :goto_0

    .line 263
    :cond_2
    invoke-static {v2, p1}, Lcom/google/gson/b/j;->a(Lcom/google/gson/l;Lcom/google/gson/stream/JsonWriter;)V

    .line 264
    return-void
.end method
