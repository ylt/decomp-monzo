.class public Lco/uk/getmondo/common/i/a;
.super Ljava/lang/Object;
.source "AmountAggregator.java"


# instance fields
.field private final a:Lco/uk/getmondo/common/i/c;

.field private b:J

.field private c:I


# direct methods
.method public constructor <init>(Lco/uk/getmondo/common/i/c;)V
    .locals 2

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    iput-object p1, p0, Lco/uk/getmondo/common/i/a;->a:Lco/uk/getmondo/common/i/c;

    .line 17
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lco/uk/getmondo/common/i/a;->b:J

    .line 18
    const/4 v0, 0x0

    iput v0, p0, Lco/uk/getmondo/common/i/a;->c:I

    .line 19
    return-void
.end method


# virtual methods
.method public a()Lco/uk/getmondo/d/c;
    .locals 4

    .prologue
    .line 35
    new-instance v0, Lco/uk/getmondo/d/c;

    iget-wide v2, p0, Lco/uk/getmondo/common/i/a;->b:J

    iget-object v1, p0, Lco/uk/getmondo/common/i/a;->a:Lco/uk/getmondo/common/i/c;

    invoke-direct {v0, v2, v3, v1}, Lco/uk/getmondo/d/c;-><init>(JLco/uk/getmondo/common/i/c;)V

    return-object v0
.end method

.method public a(Lco/uk/getmondo/d/c;)V
    .locals 4

    .prologue
    .line 22
    if-nez p1, :cond_0

    .line 32
    :goto_0
    return-void

    .line 26
    :cond_0
    iget-object v0, p0, Lco/uk/getmondo/common/i/a;->a:Lco/uk/getmondo/common/i/c;

    invoke-virtual {v0}, Lco/uk/getmondo/common/i/c;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lco/uk/getmondo/d/c;->l()Lco/uk/getmondo/common/i/c;

    move-result-object v1

    invoke-virtual {v1}, Lco/uk/getmondo/common/i/c;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 27
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "This amount aggregator only supports amounts with currency "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lco/uk/getmondo/common/i/a;->a:Lco/uk/getmondo/common/i/c;

    invoke-virtual {v2}, Lco/uk/getmondo/common/i/c;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 30
    :cond_1
    iget-wide v0, p0, Lco/uk/getmondo/common/i/a;->b:J

    invoke-virtual {p1}, Lco/uk/getmondo/d/c;->k()J

    move-result-wide v2

    add-long/2addr v0, v2

    iput-wide v0, p0, Lco/uk/getmondo/common/i/a;->b:J

    .line 31
    iget v0, p0, Lco/uk/getmondo/common/i/a;->c:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lco/uk/getmondo/common/i/a;->c:I

    goto :goto_0
.end method

.method public b()I
    .locals 1

    .prologue
    .line 39
    iget v0, p0, Lco/uk/getmondo/common/i/a;->c:I

    return v0
.end method

.method public c()V
    .locals 2

    .prologue
    .line 43
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lco/uk/getmondo/common/i/a;->b:J

    .line 44
    const/4 v0, 0x0

    iput v0, p0, Lco/uk/getmondo/common/i/a;->c:I

    .line 45
    return-void
.end method
