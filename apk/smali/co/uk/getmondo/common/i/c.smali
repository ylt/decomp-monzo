.class public Lco/uk/getmondo/common/i/c;
.super Ljava/lang/Object;
.source "Currency.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lco/uk/getmondo/common/i/c;",
            ">;"
        }
    .end annotation
.end field

.field public static final a:Lco/uk/getmondo/common/i/c;


# instance fields
.field private b:Lorg/joda/money/CurrencyUnit;

.field private c:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 21
    new-instance v0, Lco/uk/getmondo/common/i/c;

    const-string v1, "GBP"

    invoke-direct {v0, v1}, Lco/uk/getmondo/common/i/c;-><init>(Ljava/lang/String;)V

    sput-object v0, Lco/uk/getmondo/common/i/c;->a:Lco/uk/getmondo/common/i/c;

    .line 94
    new-instance v0, Lco/uk/getmondo/common/i/c$1;

    invoke-direct {v0}, Lco/uk/getmondo/common/i/c$1;-><init>()V

    sput-object v0, Lco/uk/getmondo/common/i/c;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method protected constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 83
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 84
    invoke-virtual {p1}, Landroid/os/Parcel;->readSerializable()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lorg/joda/money/CurrencyUnit;

    iput-object v0, p0, Lco/uk/getmondo/common/i/c;->b:Lorg/joda/money/CurrencyUnit;

    .line 85
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/common/i/c;->c:Ljava/lang/String;

    .line 86
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    :try_start_0
    invoke-static {p1}, Lorg/joda/money/CurrencyUnit;->of(Ljava/lang/String;)Lorg/joda/money/CurrencyUnit;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/common/i/c;->b:Lorg/joda/money/CurrencyUnit;
    :try_end_0
    .catch Lorg/joda/money/IllegalCurrencyException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    .line 38
    :goto_0
    return-void

    .line 29
    :catch_0
    move-exception v0

    .line 34
    :goto_1
    sget-object v1, Lorg/joda/money/CurrencyUnit;->GBP:Lorg/joda/money/CurrencyUnit;

    iput-object v1, p0, Lco/uk/getmondo/common/i/c;->b:Lorg/joda/money/CurrencyUnit;

    .line 35
    if-nez p1, :cond_0

    const-string p1, ""

    :cond_0
    iput-object p1, p0, Lco/uk/getmondo/common/i/c;->c:Ljava/lang/String;

    .line 36
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "Invalid currency code"

    invoke-direct {v1, v2, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    invoke-static {v1}, Ld/a/a;->a(Ljava/lang/Throwable;)V

    goto :goto_0

    .line 29
    :catch_1
    move-exception v0

    goto :goto_1
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lco/uk/getmondo/common/i/c;->b:Lorg/joda/money/CurrencyUnit;

    invoke-virtual {v0}, Lorg/joda/money/CurrencyUnit;->getDecimalPlaces()I

    move-result v0

    return v0
.end method

.method public a(Ljava/util/Locale;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lco/uk/getmondo/common/i/c;->c:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 54
    iget-object v0, p0, Lco/uk/getmondo/common/i/c;->c:Ljava/lang/String;

    .line 57
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lco/uk/getmondo/common/i/c;->b:Lorg/joda/money/CurrencyUnit;

    invoke-virtual {v0, p1}, Lorg/joda/money/CurrencyUnit;->getSymbol(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lco/uk/getmondo/common/i/c;->c:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 46
    iget-object v0, p0, Lco/uk/getmondo/common/i/c;->c:Ljava/lang/String;

    .line 49
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lco/uk/getmondo/common/i/c;->b:Lorg/joda/money/CurrencyUnit;

    invoke-virtual {v0}, Lorg/joda/money/CurrencyUnit;->getCurrencyCode()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 62
    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 67
    if-ne p0, p1, :cond_1

    .line 73
    :cond_0
    :goto_0
    return v0

    .line 68
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    goto :goto_0

    .line 70
    :cond_3
    check-cast p1, Lco/uk/getmondo/common/i/c;

    .line 72
    iget-object v2, p0, Lco/uk/getmondo/common/i/c;->b:Lorg/joda/money/CurrencyUnit;

    if-eqz v2, :cond_5

    iget-object v2, p0, Lco/uk/getmondo/common/i/c;->b:Lorg/joda/money/CurrencyUnit;

    iget-object v3, p1, Lco/uk/getmondo/common/i/c;->b:Lorg/joda/money/CurrencyUnit;

    invoke-virtual {v2, v3}, Lorg/joda/money/CurrencyUnit;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    goto :goto_0

    :cond_5
    iget-object v2, p1, Lco/uk/getmondo/common/i/c;->b:Lorg/joda/money/CurrencyUnit;

    if-nez v2, :cond_4

    .line 73
    :cond_6
    iget-object v2, p0, Lco/uk/getmondo/common/i/c;->c:Ljava/lang/String;

    if-eqz v2, :cond_7

    iget-object v0, p0, Lco/uk/getmondo/common/i/c;->c:Ljava/lang/String;

    iget-object v1, p1, Lco/uk/getmondo/common/i/c;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0

    :cond_7
    iget-object v2, p1, Lco/uk/getmondo/common/i/c;->c:Ljava/lang/String;

    if-eqz v2, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 78
    iget-object v0, p0, Lco/uk/getmondo/common/i/c;->b:Lorg/joda/money/CurrencyUnit;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lco/uk/getmondo/common/i/c;->b:Lorg/joda/money/CurrencyUnit;

    invoke-virtual {v0}, Lorg/joda/money/CurrencyUnit;->hashCode()I

    move-result v0

    .line 79
    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lco/uk/getmondo/common/i/c;->c:Ljava/lang/String;

    if-eqz v2, :cond_0

    iget-object v1, p0, Lco/uk/getmondo/common/i/c;->c:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    :cond_0
    add-int/2addr v0, v1

    .line 80
    return v0

    :cond_1
    move v0, v1

    .line 78
    goto :goto_0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 90
    iget-object v0, p0, Lco/uk/getmondo/common/i/c;->b:Lorg/joda/money/CurrencyUnit;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    .line 91
    iget-object v0, p0, Lco/uk/getmondo/common/i/c;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 92
    return-void
.end method
