.class public Lco/uk/getmondo/common/i/d;
.super Ljava/lang/Object;
.source "MoneyUtils.java"


# direct methods
.method public static a(JI)I
    .locals 6

    .prologue
    .line 8
    if-gtz p2, :cond_0

    .line 9
    const/4 v0, 0x0

    .line 12
    :goto_0
    return v0

    :cond_0
    invoke-static {p0, p1}, Ljava/lang/Math;->abs(J)J

    move-result-wide v0

    long-to-int v0, v0

    const-wide/high16 v2, 0x4024000000000000L    # 10.0

    int-to-double v4, p2

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v2

    double-to-int v1, v2

    rem-int/2addr v0, v1

    goto :goto_0
.end method

.method public static a(II)Ljava/lang/String;
    .locals 5

    .prologue
    .line 68
    if-gtz p1, :cond_0

    .line 69
    const-string v0, ""

    .line 73
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Ljava/util/Locale;->UK:Ljava/util/Locale;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, ".%0"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "d"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static b(JI)J
    .locals 6

    .prologue
    .line 16
    if-gtz p2, :cond_0

    .line 17
    invoke-static {p0, p1}, Ljava/lang/Math;->abs(J)J

    move-result-wide v0

    .line 20
    :goto_0
    return-wide v0

    :cond_0
    invoke-static {p0, p1}, Ljava/lang/Math;->abs(J)J

    move-result-wide v0

    const-wide/high16 v2, 0x4024000000000000L    # 10.0

    int-to-double v4, p2

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v2

    double-to-int v2, v2

    int-to-long v2, v2

    div-long/2addr v0, v2

    goto :goto_0
.end method

.method public static c(JI)D
    .locals 6

    .prologue
    .line 24
    if-gtz p2, :cond_0

    .line 25
    long-to-double v0, p0

    .line 28
    :goto_0
    return-wide v0

    :cond_0
    long-to-double v0, p0

    const-wide/high16 v2, 0x4024000000000000L    # 10.0

    int-to-double v4, p2

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v2

    div-double/2addr v0, v2

    goto :goto_0
.end method

.method public static d(JI)J
    .locals 8

    .prologue
    const-wide/16 v6, 0x0

    .line 32
    cmp-long v0, p0, v6

    if-eqz v0, :cond_0

    if-gtz p2, :cond_2

    :cond_0
    move-wide v0, p0

    .line 49
    :cond_1
    :goto_0
    return-wide v0

    .line 37
    :cond_2
    invoke-static {p0, p1}, Ljava/lang/Math;->abs(J)J

    move-result-wide v0

    .line 38
    const-wide/high16 v2, 0x4024000000000000L    # 10.0

    int-to-double v4, p2

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v2

    double-to-int v2, v2

    .line 39
    long-to-int v3, v0

    rem-int/2addr v3, v2

    .line 42
    div-int/lit8 v4, v2, 0x2

    if-gt v3, v4, :cond_3

    .line 43
    int-to-long v2, v3

    sub-long/2addr v0, v2

    .line 49
    :goto_1
    cmp-long v2, p0, v6

    if-gez v2, :cond_1

    const-wide/16 v2, -0x1

    mul-long/2addr v0, v2

    goto :goto_0

    .line 45
    :cond_3
    int-to-long v4, v3

    sub-long/2addr v0, v4

    int-to-long v2, v2

    add-long/2addr v0, v2

    goto :goto_1
.end method

.method public static e(JI)J
    .locals 8

    .prologue
    const-wide/16 v6, 0x0

    .line 53
    cmp-long v0, p0, v6

    if-eqz v0, :cond_0

    if-gtz p2, :cond_2

    :cond_0
    move-wide v0, p0

    .line 64
    :cond_1
    :goto_0
    return-wide v0

    .line 58
    :cond_2
    invoke-static {p0, p1}, Ljava/lang/Math;->abs(J)J

    move-result-wide v0

    .line 59
    const-wide/high16 v2, 0x4024000000000000L    # 10.0

    int-to-double v4, p2

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v2

    double-to-int v2, v2

    .line 60
    long-to-int v3, v0

    rem-int/2addr v3, v2

    .line 61
    if-lez v3, :cond_3

    int-to-long v4, v3

    sub-long/2addr v0, v4

    int-to-long v2, v2

    add-long/2addr v0, v2

    .line 64
    :cond_3
    cmp-long v2, p0, v6

    if-gez v2, :cond_1

    const-wide/16 v2, -0x1

    mul-long/2addr v0, v2

    goto :goto_0
.end method
