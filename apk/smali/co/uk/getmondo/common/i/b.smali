.class public final Lco/uk/getmondo/common/i/b;
.super Ljava/lang/Object;
.source "AmountFormatter.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lco/uk/getmondo/common/i/b$a;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0008\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u0000 \u000f2\u00020\u0001:\u0001\u000fB%\u0008\u0007\u0012\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u0003\u0012\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u0003\u0012\u0008\u0008\u0002\u0010\u0005\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0006J\u000e\u0010\u000b\u001a\u00020\u000c2\u0006\u0010\r\u001a\u00020\u000eR\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0007\u0010\u0008R\u0011\u0010\u0005\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\t\u0010\u0008R\u0011\u0010\u0004\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\n\u0010\u0008\u00a8\u0006\u0010"
    }
    d2 = {
        "Lco/uk/getmondo/common/money/AmountFormatter;",
        "",
        "alwaysPrintFractionalPart",
        "",
        "showNegative",
        "hideCurrency",
        "(ZZZ)V",
        "getAlwaysPrintFractionalPart",
        "()Z",
        "getHideCurrency",
        "getShowNegative",
        "format",
        "",
        "amount",
        "Lco/uk/getmondo/model/Amount;",
        "Companion",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# static fields
.field public static final a:Lco/uk/getmondo/common/i/b$a;


# instance fields
.field private final b:Z

.field private final c:Z

.field private final d:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lco/uk/getmondo/common/i/b$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lco/uk/getmondo/common/i/b$a;-><init>(Lkotlin/d/b/i;)V

    sput-object v0, Lco/uk/getmondo/common/i/b;->a:Lco/uk/getmondo/common/i/b$a;

    return-void
.end method

.method public constructor <init>()V
    .locals 6

    const/4 v1, 0x0

    const/4 v4, 0x7

    const/4 v5, 0x0

    move-object v0, p0

    move v2, v1

    move v3, v1

    invoke-direct/range {v0 .. v5}, Lco/uk/getmondo/common/i/b;-><init>(ZZZILkotlin/d/b/i;)V

    return-void
.end method

.method public constructor <init>(ZZZ)V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean p1, p0, Lco/uk/getmondo/common/i/b;->b:Z

    iput-boolean p2, p0, Lco/uk/getmondo/common/i/b;->c:Z

    iput-boolean p3, p0, Lco/uk/getmondo/common/i/b;->d:Z

    return-void
.end method

.method public synthetic constructor <init>(ZZZILkotlin/d/b/i;)V
    .locals 2

    .prologue
    const/4 v0, 0x0

    and-int/lit8 v1, p4, 0x1

    if-eqz v1, :cond_0

    move p1, v0

    .line 8
    :cond_0
    and-int/lit8 v1, p4, 0x2

    if-eqz v1, :cond_1

    move p2, v0

    .line 9
    :cond_1
    and-int/lit8 v1, p4, 0x4

    if-eqz v1, :cond_2

    move p3, v0

    .line 10
    :cond_2
    invoke-direct {p0, p1, p2, p3}, Lco/uk/getmondo/common/i/b;-><init>(ZZZ)V

    return-void
.end method


# virtual methods
.method public final a(Lco/uk/getmondo/d/c;)Ljava/lang/String;
    .locals 6

    .prologue
    const-string v0, "amount"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 13
    iget-boolean v0, p0, Lco/uk/getmondo/common/i/b;->d:Z

    if-eqz v0, :cond_1

    const-string v0, ""

    .line 14
    :goto_0
    iget-boolean v1, p0, Lco/uk/getmondo/common/i/b;->c:Z

    if-eqz v1, :cond_2

    invoke-virtual {p1}, Lco/uk/getmondo/d/c;->a()Z

    move-result v1

    if-eqz v1, :cond_2

    const-string v1, "-"

    .line 15
    :goto_1
    sget-object v2, Lco/uk/getmondo/common/i/b;->a:Lco/uk/getmondo/common/i/b$a;

    invoke-virtual {p1}, Lco/uk/getmondo/d/c;->c()J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Lco/uk/getmondo/common/i/b$a;->a(J)Ljava/lang/String;

    move-result-object v3

    .line 17
    invoke-virtual {p1}, Lco/uk/getmondo/d/c;->d()I

    move-result v4

    .line 18
    invoke-virtual {p1}, Lco/uk/getmondo/d/c;->l()Lco/uk/getmondo/common/i/c;

    move-result-object v2

    invoke-virtual {v2}, Lco/uk/getmondo/common/i/c;->a()I

    move-result v5

    .line 19
    iget-boolean v2, p0, Lco/uk/getmondo/common/i/b;->b:Z

    if-nez v2, :cond_0

    if-lez v4, :cond_3

    :cond_0
    const/4 v2, 0x1

    .line 20
    :goto_2
    if-eqz v2, :cond_4

    invoke-static {v4, v5}, Lco/uk/getmondo/common/i/d;->a(II)Ljava/lang/String;

    move-result-object v2

    .line 21
    :goto_3
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 13
    :cond_1
    invoke-virtual {p1}, Lco/uk/getmondo/d/c;->g()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 14
    :cond_2
    const-string v1, ""

    goto :goto_1

    .line 19
    :cond_3
    const/4 v2, 0x0

    goto :goto_2

    .line 20
    :cond_4
    const-string v2, ""

    goto :goto_3
.end method
