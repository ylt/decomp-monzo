.class public Lco/uk/getmondo/common/d/e;
.super Landroid/app/DialogFragment;
.source "PinBlockedDialogFragment.java"


# instance fields
.field private a:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    return-void
.end method

.method public static a()Lco/uk/getmondo/common/d/e;
    .locals 1

    .prologue
    .line 29
    const/4 v0, 0x0

    invoke-static {v0}, Lco/uk/getmondo/common/d/e;->a(Z)Lco/uk/getmondo/common/d/e;

    move-result-object v0

    return-object v0
.end method

.method public static a(Z)Lco/uk/getmondo/common/d/e;
    .locals 3

    .prologue
    .line 21
    new-instance v0, Lco/uk/getmondo/common/d/e;

    invoke-direct {v0}, Lco/uk/getmondo/common/d/e;-><init>()V

    .line 22
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 23
    const-string v2, "ARGUMENT_FINISH_ACTIVITY"

    invoke-virtual {v1, v2, p0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 24
    invoke-virtual {v0, v1}, Lco/uk/getmondo/common/d/e;->setArguments(Landroid/os/Bundle;)V

    .line 25
    return-object v0
.end method

.method static synthetic a(Landroid/app/Activity;Landroid/content/DialogInterface;I)V
    .locals 1

    .prologue
    .line 46
    invoke-static {p0}, Lco/uk/getmondo/MonzoApplication;->a(Landroid/content/Context;)Lco/uk/getmondo/MonzoApplication;

    move-result-object v0

    invoke-virtual {v0}, Lco/uk/getmondo/MonzoApplication;->b()Lco/uk/getmondo/common/h/a/a;

    move-result-object v0

    .line 47
    invoke-interface {v0}, Lco/uk/getmondo/common/h/a/a;->p()Lco/uk/getmondo/common/q;

    move-result-object v0

    invoke-virtual {v0}, Lco/uk/getmondo/common/q;->a()V

    .line 48
    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 34
    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onCreate(Landroid/os/Bundle;)V

    .line 35
    invoke-virtual {p0}, Lco/uk/getmondo/common/d/e;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 36
    if-eqz v0, :cond_0

    const-string v1, "ARGUMENT_FINISH_ACTIVITY"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lco/uk/getmondo/common/d/e;->a:Z

    .line 37
    return-void

    .line 36
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 3

    .prologue
    .line 41
    invoke-virtual {p0}, Lco/uk/getmondo/common/d/e;->getActivity()Landroid/app/Activity;

    move-result-object v0

    .line 42
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, v0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v2, 0x7f0a0307

    .line 43
    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f0a0302

    .line 44
    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f0a0125

    .line 45
    invoke-virtual {p0, v2}, Lco/uk/getmondo/common/d/e;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0}, Lco/uk/getmondo/common/d/f;->a(Landroid/app/Activity;)Landroid/content/DialogInterface$OnClickListener;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const/4 v1, 0x0

    .line 49
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0a02c3

    .line 50
    invoke-virtual {p0, v1}, Lco/uk/getmondo/common/d/e;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 51
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 42
    return-object v0
.end method

.method public onDismiss(Landroid/content/DialogInterface;)V
    .locals 1

    .prologue
    .line 56
    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onDismiss(Landroid/content/DialogInterface;)V

    .line 57
    iget-boolean v0, p0, Lco/uk/getmondo/common/d/e;->a:Z

    if-eqz v0, :cond_0

    .line 58
    invoke-virtual {p0}, Lco/uk/getmondo/common/d/e;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 60
    :cond_0
    return-void
.end method
