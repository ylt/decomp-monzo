.class public final Lco/uk/getmondo/common/d/c;
.super Landroid/support/v4/app/i;
.source "ForgotPinDialogFragment.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0012\u0010\t\u001a\u00020\n2\u0008\u0010\u000b\u001a\u0004\u0018\u00010\u000cH\u0016J\u0012\u0010\r\u001a\u00020\u000e2\u0008\u0010\u000b\u001a\u0004\u0018\u00010\u000cH\u0016R\u001e\u0010\u0003\u001a\u00020\u00048\u0006@\u0006X\u0087.\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u0005\u0010\u0006\"\u0004\u0008\u0007\u0010\u0008\u00a8\u0006\u000f"
    }
    d2 = {
        "Lco/uk/getmondo/common/dialog/ForgotPinDialogFragment;",
        "Landroid/support/v4/app/DialogFragment;",
        "()V",
        "intercomService",
        "Lco/uk/getmondo/common/IntercomService;",
        "getIntercomService",
        "()Lco/uk/getmondo/common/IntercomService;",
        "setIntercomService",
        "(Lco/uk/getmondo/common/IntercomService;)V",
        "onCreate",
        "",
        "savedInstanceState",
        "Landroid/os/Bundle;",
        "onCreateDialog",
        "Landroid/app/Dialog;",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field public a:Lco/uk/getmondo/common/q;

.field private b:Ljava/util/HashMap;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Landroid/support/v4/app/i;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Lco/uk/getmondo/common/q;
    .locals 2

    .prologue
    .line 15
    iget-object v0, p0, Lco/uk/getmondo/common/d/c;->a:Lco/uk/getmondo/common/q;

    if-nez v0, :cond_0

    const-string v1, "intercomService"

    invoke-static {v1}, Lkotlin/d/b/l;->b(Ljava/lang/String;)V

    :cond_0
    return-object v0
.end method

.method public b()V
    .locals 1

    iget-object v0, p0, Lco/uk/getmondo/common/d/c;->b:Ljava/util/HashMap;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lco/uk/getmondo/common/d/c;->b:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    :cond_0
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 18
    invoke-super {p0, p1}, Landroid/support/v4/app/i;->onCreate(Landroid/os/Bundle;)V

    .line 20
    invoke-static {}, Lco/uk/getmondo/common/h/b/a;->a()Lco/uk/getmondo/common/h/b/a$d;

    move-result-object v1

    .line 21
    invoke-virtual {p0}, Lco/uk/getmondo/common/d/c;->getActivity()Landroid/support/v4/app/j;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-static {v0}, Lco/uk/getmondo/MonzoApplication;->a(Landroid/content/Context;)Lco/uk/getmondo/MonzoApplication;

    move-result-object v0

    invoke-virtual {v0}, Lco/uk/getmondo/MonzoApplication;->b()Lco/uk/getmondo/common/h/a/a;

    move-result-object v0

    invoke-virtual {v1, v0}, Lco/uk/getmondo/common/h/b/a$d;->a(Lco/uk/getmondo/common/h/a/a;)Lco/uk/getmondo/common/h/b/a$d;

    move-result-object v1

    .line 22
    new-instance v2, Lco/uk/getmondo/common/h/b/c;

    invoke-virtual {p0}, Lco/uk/getmondo/common/d/c;->getActivity()Landroid/support/v4/app/j;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    invoke-direct {v2, v0}, Lco/uk/getmondo/common/h/b/c;-><init>(Landroid/app/Activity;)V

    invoke-virtual {v1, v2}, Lco/uk/getmondo/common/h/b/a$d;->a(Lco/uk/getmondo/common/h/b/c;)Lco/uk/getmondo/common/h/b/a$d;

    move-result-object v0

    .line 23
    invoke-virtual {v0}, Lco/uk/getmondo/common/h/b/a$d;->a()Lco/uk/getmondo/common/h/b/b;

    move-result-object v0

    .line 24
    invoke-interface {v0, p0}, Lco/uk/getmondo/common/h/b/b;->a(Lco/uk/getmondo/common/d/c;)V

    .line 25
    return-void
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 3

    .prologue
    .line 28
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lco/uk/getmondo/common/d/c;->getActivity()Landroid/support/v4/app/j;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-direct {v1, v0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 29
    const v0, 0x7f0a0191

    invoke-virtual {v1, v0}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 30
    const v1, 0x7f0a0192

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    .line 31
    const v0, 0x7f0a012b

    invoke-virtual {p0, v0}, Lco/uk/getmondo/common/d/c;->getString(I)Ljava/lang/String;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    new-instance v1, Lco/uk/getmondo/common/d/c$a;

    invoke-direct {v1, p0}, Lco/uk/getmondo/common/d/c$a;-><init>(Lco/uk/getmondo/common/d/c;)V

    check-cast v1, Landroid/content/DialogInterface$OnClickListener;

    invoke-virtual {v2, v0, v1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    .line 34
    const v0, 0x7f0a02c3

    invoke-virtual {p0, v0}, Lco/uk/getmondo/common/d/c;->getString(I)Ljava/lang/String;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 35
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    const-string v1, "AlertDialog.Builder(acti\u2026                .create()"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/app/Dialog;

    return-object v0
.end method

.method public synthetic onDestroyView()V
    .locals 0

    invoke-super {p0}, Landroid/support/v4/app/i;->onDestroyView()V

    invoke-virtual {p0}, Lco/uk/getmondo/common/d/c;->b()V

    return-void
.end method
