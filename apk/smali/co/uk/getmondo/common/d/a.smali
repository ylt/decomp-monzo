.class public Lco/uk/getmondo/common/d/a;
.super Landroid/support/v4/app/i;
.source "ErrorDialogFragment.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Landroid/support/v4/app/i;-><init>()V

    return-void
.end method

.method public static a(Ljava/lang/String;)Lco/uk/getmondo/common/d/a;
    .locals 1

    .prologue
    .line 42
    const-string v0, ""

    invoke-static {v0, p0}, Lco/uk/getmondo/common/d/a;->a(Ljava/lang/String;Ljava/lang/String;)Lco/uk/getmondo/common/d/a;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;)Lco/uk/getmondo/common/d/a;
    .locals 1

    .prologue
    .line 38
    const/4 v0, 0x1

    invoke-static {p0, p1, v0}, Lco/uk/getmondo/common/d/a;->a(Ljava/lang/String;Ljava/lang/String;Z)Lco/uk/getmondo/common/d/a;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;IZ)Lco/uk/getmondo/common/d/a;
    .locals 3

    .prologue
    .line 23
    new-instance v0, Lco/uk/getmondo/common/d/a;

    invoke-direct {v0}, Lco/uk/getmondo/common/d/a;-><init>()V

    .line 24
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 25
    const-string v2, "ARGUMENT_TITLE"

    invoke-virtual {v1, v2, p0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 26
    const-string v2, "ARGUMENT_MESSAGE"

    invoke-virtual {v1, v2, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 27
    const-string v2, "ARGUMENT_BUTTON_TEXT"

    invoke-virtual {v1, v2, p2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 28
    const-string v2, "ARGUMENT_FINISH_ACTIVITY"

    invoke-virtual {v1, v2, p3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 29
    invoke-virtual {v0, v1}, Lco/uk/getmondo/common/d/a;->setArguments(Landroid/os/Bundle;)V

    .line 30
    return-object v0
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;Z)Lco/uk/getmondo/common/d/a;
    .locals 1

    .prologue
    .line 34
    const v0, 0x104000a

    invoke-static {p0, p1, v0, p2}, Lco/uk/getmondo/common/d/a;->a(Ljava/lang/String;Ljava/lang/String;IZ)Lco/uk/getmondo/common/d/a;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lco/uk/getmondo/common/d/a;Landroid/app/Activity;Landroid/content/DialogInterface;I)V
    .locals 2

    .prologue
    .line 52
    invoke-virtual {p0}, Lco/uk/getmondo/common/d/a;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "ARGUMENT_FINISH_ACTIVITY"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 53
    invoke-virtual {p1}, Landroid/app/Activity;->finish()V

    .line 55
    :cond_0
    return-void
.end method


# virtual methods
.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 4

    .prologue
    .line 47
    invoke-virtual {p0}, Lco/uk/getmondo/common/d/a;->getActivity()Landroid/support/v4/app/j;

    move-result-object v0

    .line 48
    new-instance v1, Landroid/support/v7/app/d$a;

    const v2, 0x7f0c0112

    invoke-direct {v1, v0, v2}, Landroid/support/v7/app/d$a;-><init>(Landroid/content/Context;I)V

    .line 49
    invoke-virtual {p0}, Lco/uk/getmondo/common/d/a;->getArguments()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "ARGUMENT_TITLE"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/support/v7/app/d$a;->a(Ljava/lang/CharSequence;)Landroid/support/v7/app/d$a;

    move-result-object v1

    .line 50
    invoke-virtual {p0}, Lco/uk/getmondo/common/d/a;->getArguments()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "ARGUMENT_MESSAGE"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/support/v7/app/d$a;->b(Ljava/lang/CharSequence;)Landroid/support/v7/app/d$a;

    move-result-object v1

    .line 51
    invoke-virtual {p0}, Lco/uk/getmondo/common/d/a;->getArguments()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "ARGUMENT_BUTTON_TEXT"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    invoke-static {p0, v0}, Lco/uk/getmondo/common/d/b;->a(Lco/uk/getmondo/common/d/a;Landroid/app/Activity;)Landroid/content/DialogInterface$OnClickListener;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Landroid/support/v7/app/d$a;->a(ILandroid/content/DialogInterface$OnClickListener;)Landroid/support/v7/app/d$a;

    move-result-object v0

    .line 56
    invoke-virtual {v0}, Landroid/support/v7/app/d$a;->b()Landroid/support/v7/app/d;

    move-result-object v0

    .line 48
    return-object v0
.end method
