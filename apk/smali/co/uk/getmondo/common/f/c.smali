.class public Lco/uk/getmondo/common/f/c;
.super Ljava/lang/Object;
.source "UiMessageHelper.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lco/uk/getmondo/common/f/c$a;
    }
.end annotation


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Landroid/view/View;

.field private c:Landroid/app/ProgressDialog;

.field private d:Landroid/support/design/widget/Snackbar;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/view/View;)V
    .locals 2

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    iput-object p1, p0, Lco/uk/getmondo/common/f/c;->a:Landroid/content/Context;

    .line 24
    iput-object p2, p0, Lco/uk/getmondo/common/f/c;->b:Landroid/view/View;

    .line 27
    if-nez p2, :cond_0

    .line 28
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "The view passed into the UiMessageHelper cannot be null"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 30
    :cond_0
    return-void
.end method

.method static synthetic a(Lco/uk/getmondo/common/f/c$a;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 51
    invoke-interface {p0}, Lco/uk/getmondo/common/f/c$a;->a()V

    return-void
.end method

.method static synthetic b(Lco/uk/getmondo/common/f/c$a;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 38
    invoke-interface {p0}, Lco/uk/getmondo/common/f/c$a;->a()V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    .line 73
    iget-object v0, p0, Lco/uk/getmondo/common/f/c;->a:Landroid/content/Context;

    const v1, 0x7f0a0262

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lco/uk/getmondo/common/f/c;->c(Ljava/lang/String;)V

    .line 74
    return-void
.end method

.method public a(ILjava/lang/String;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 69
    iget-object v0, p0, Lco/uk/getmondo/common/f/c;->a:Landroid/content/Context;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p2, v1, v2

    invoke-virtual {v0, p1, v1}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0, v3, v3}, Lco/uk/getmondo/common/f/c;->b(Ljava/lang/String;Ljava/lang/String;Lco/uk/getmondo/common/f/c$a;)Landroid/support/design/widget/Snackbar;

    .line 70
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 33
    iget-object v0, p0, Lco/uk/getmondo/common/f/c;->b:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/common/f/c;->b:Landroid/view/View;

    invoke-static {v0, v1, p1, v2, v2}, Lco/uk/getmondo/common/ui/i;->a(Landroid/content/Context;Landroid/view/View;Ljava/lang/String;IZ)Landroid/support/design/widget/Snackbar;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/design/widget/Snackbar;->c()V

    .line 34
    return-void
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;Lco/uk/getmondo/common/f/c$a;)V
    .locals 4

    .prologue
    .line 37
    iget-object v0, p0, Lco/uk/getmondo/common/f/c;->b:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/common/f/c;->b:Landroid/view/View;

    const/4 v2, -0x2

    const/4 v3, 0x0

    invoke-static {v0, v1, p1, v2, v3}, Lco/uk/getmondo/common/ui/i;->a(Landroid/content/Context;Landroid/view/View;Ljava/lang/String;IZ)Landroid/support/design/widget/Snackbar;

    move-result-object v0

    .line 38
    invoke-static {p3}, Lco/uk/getmondo/common/f/d;->a(Lco/uk/getmondo/common/f/c$a;)Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, p2, v1}, Landroid/support/design/widget/Snackbar;->a(Ljava/lang/CharSequence;Landroid/view/View$OnClickListener;)Landroid/support/design/widget/Snackbar;

    .line 39
    invoke-virtual {v0}, Landroid/support/design/widget/Snackbar;->c()V

    .line 40
    return-void
.end method

.method public b(Ljava/lang/String;Ljava/lang/String;Lco/uk/getmondo/common/f/c$a;)Landroid/support/design/widget/Snackbar;
    .locals 4

    .prologue
    .line 44
    iget-object v0, p0, Lco/uk/getmondo/common/f/c;->d:Landroid/support/design/widget/Snackbar;

    if-nez v0, :cond_2

    .line 45
    iget-object v0, p0, Lco/uk/getmondo/common/f/c;->b:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/common/f/c;->b:Landroid/view/View;

    const/4 v2, -0x2

    const/4 v3, 0x0

    invoke-static {v0, v1, p1, v2, v3}, Lco/uk/getmondo/common/ui/i;->a(Landroid/content/Context;Landroid/view/View;Ljava/lang/String;IZ)Landroid/support/design/widget/Snackbar;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/common/f/c;->d:Landroid/support/design/widget/Snackbar;

    .line 50
    :goto_0
    if-eqz p2, :cond_0

    if-eqz p3, :cond_0

    .line 51
    iget-object v0, p0, Lco/uk/getmondo/common/f/c;->d:Landroid/support/design/widget/Snackbar;

    invoke-static {p3}, Lco/uk/getmondo/common/f/e;->a(Lco/uk/getmondo/common/f/c$a;)Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, p2, v1}, Landroid/support/design/widget/Snackbar;->a(Ljava/lang/CharSequence;Landroid/view/View$OnClickListener;)Landroid/support/design/widget/Snackbar;

    .line 54
    :cond_0
    iget-object v0, p0, Lco/uk/getmondo/common/f/c;->d:Landroid/support/design/widget/Snackbar;

    invoke-virtual {v0}, Landroid/support/design/widget/Snackbar;->e()Z

    move-result v0

    if-nez v0, :cond_1

    .line 55
    iget-object v0, p0, Lco/uk/getmondo/common/f/c;->d:Landroid/support/design/widget/Snackbar;

    invoke-virtual {v0}, Landroid/support/design/widget/Snackbar;->c()V

    .line 57
    :cond_1
    iget-object v0, p0, Lco/uk/getmondo/common/f/c;->d:Landroid/support/design/widget/Snackbar;

    return-object v0

    .line 47
    :cond_2
    iget-object v0, p0, Lco/uk/getmondo/common/f/c;->d:Landroid/support/design/widget/Snackbar;

    invoke-virtual {v0, p1}, Landroid/support/design/widget/Snackbar;->a(Ljava/lang/CharSequence;)Landroid/support/design/widget/Snackbar;

    goto :goto_0
.end method

.method public b()V
    .locals 1

    .prologue
    .line 91
    iget-object v0, p0, Lco/uk/getmondo/common/f/c;->c:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_0

    .line 92
    iget-object v0, p0, Lco/uk/getmondo/common/f/c;->c:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 94
    :cond_0
    return-void
.end method

.method public b(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 61
    invoke-virtual {p0, p1}, Lco/uk/getmondo/common/f/c;->a(Ljava/lang/String;)V

    .line 62
    return-void
.end method

.method public c(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 78
    :try_start_0
    iget-object v0, p0, Lco/uk/getmondo/common/f/c;->c:Landroid/app/ProgressDialog;

    if-nez v0, :cond_0

    .line 79
    new-instance v0, Landroid/app/ProgressDialog;

    new-instance v1, Landroid/view/ContextThemeWrapper;

    iget-object v2, p0, Lco/uk/getmondo/common/f/c;->a:Landroid/content/Context;

    const v3, 0x7f0c010e

    invoke-direct {v1, v2, v3}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    invoke-direct {v0, v1}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lco/uk/getmondo/common/f/c;->c:Landroid/app/ProgressDialog;

    .line 81
    :cond_0
    iget-object v0, p0, Lco/uk/getmondo/common/f/c;->c:Landroid/app/ProgressDialog;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setTitle(Ljava/lang/CharSequence;)V

    .line 82
    iget-object v0, p0, Lco/uk/getmondo/common/f/c;->c:Landroid/app/ProgressDialog;

    invoke-virtual {v0, p1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 83
    iget-object v0, p0, Lco/uk/getmondo/common/f/c;->c:Landroid/app/ProgressDialog;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setIndeterminate(Z)V

    .line 84
    iget-object v0, p0, Lco/uk/getmondo/common/f/c;->c:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 88
    :goto_0
    return-void

    .line 85
    :catch_0
    move-exception v0

    .line 86
    const-string v1, "Problem showing progress dialog"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Ld/a/a;->a(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method
