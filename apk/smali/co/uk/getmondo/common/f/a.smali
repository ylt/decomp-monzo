.class public Lco/uk/getmondo/common/f/a;
.super Landroid/support/v4/app/Fragment;
.source "BaseFragment.java"

# interfaces
.implements Lco/uk/getmondo/common/e/a$a;


# instance fields
.field private a:Lco/uk/getmondo/common/h/b/b;

.field protected b:Lcom/c/b/b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/c/b/b",
            "<",
            "Lco/uk/getmondo/common/f/c;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 19
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    .line 21
    invoke-static {}, Lcom/c/b/b;->c()Lcom/c/b/b;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/common/f/a;->b:Lcom/c/b/b;

    return-void
.end method

.method static synthetic a(Ljava/lang/String;Lco/uk/getmondo/common/f/c;)V
    .locals 0

    .prologue
    .line 54
    invoke-virtual {p1, p0}, Lco/uk/getmondo/common/f/c;->b(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method protected B()Lco/uk/getmondo/common/h/b/b;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lco/uk/getmondo/common/f/a;->a:Lco/uk/getmondo/common/h/b/b;

    return-object v0
.end method

.method public b(I)V
    .locals 1

    .prologue
    .line 49
    invoke-virtual {p0, p1}, Lco/uk/getmondo/common/f/a;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lco/uk/getmondo/common/f/a;->d(Ljava/lang/String;)V

    .line 50
    return-void
.end method

.method public c(I)V
    .locals 2

    .prologue
    .line 69
    invoke-virtual {p0}, Lco/uk/getmondo/common/f/a;->getActivity()Landroid/support/v4/app/j;

    move-result-object v0

    invoke-virtual {p0, p1}, Lco/uk/getmondo/common/f/a;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lco/uk/getmondo/splash/SplashActivity;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lco/uk/getmondo/common/f/a;->startActivity(Landroid/content/Intent;)V

    .line 70
    return-void
.end method

.method public d(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 54
    iget-object v0, p0, Lco/uk/getmondo/common/f/a;->b:Lcom/c/b/b;

    invoke-static {p1}, Lco/uk/getmondo/common/f/b;->a(Ljava/lang/String;)Lcom/c/a/a;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/c/b/b;->a(Lcom/c/a/a;)Lcom/c/b/b;

    .line 55
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 25
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 26
    invoke-static {}, Lco/uk/getmondo/common/h/b/a;->a()Lco/uk/getmondo/common/h/b/a$d;

    move-result-object v0

    .line 27
    invoke-virtual {p0}, Lco/uk/getmondo/common/f/a;->getActivity()Landroid/support/v4/app/j;

    move-result-object v1

    invoke-static {v1}, Lco/uk/getmondo/MonzoApplication;->a(Landroid/content/Context;)Lco/uk/getmondo/MonzoApplication;

    move-result-object v1

    invoke-virtual {v1}, Lco/uk/getmondo/MonzoApplication;->b()Lco/uk/getmondo/common/h/a/a;

    move-result-object v1

    invoke-virtual {v0, v1}, Lco/uk/getmondo/common/h/b/a$d;->a(Lco/uk/getmondo/common/h/a/a;)Lco/uk/getmondo/common/h/b/a$d;

    move-result-object v0

    new-instance v1, Lco/uk/getmondo/common/h/b/c;

    .line 28
    invoke-virtual {p0}, Lco/uk/getmondo/common/f/a;->getActivity()Landroid/support/v4/app/j;

    move-result-object v2

    invoke-direct {v1, v2}, Lco/uk/getmondo/common/h/b/c;-><init>(Landroid/app/Activity;)V

    invoke-virtual {v0, v1}, Lco/uk/getmondo/common/h/b/a$d;->a(Lco/uk/getmondo/common/h/b/c;)Lco/uk/getmondo/common/h/b/a$d;

    move-result-object v0

    invoke-virtual {v0}, Lco/uk/getmondo/common/h/b/a$d;->a()Lco/uk/getmondo/common/h/b/b;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/common/f/a;->a:Lco/uk/getmondo/common/h/b/b;

    .line 29
    return-void
.end method

.method public onDestroyView()V
    .locals 1

    .prologue
    .line 39
    invoke-static {}, Lcom/c/b/b;->c()Lcom/c/b/b;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/common/f/a;->b:Lcom/c/b/b;

    .line 40
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDestroyView()V

    .line 41
    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 33
    invoke-super {p0, p1, p2}, Landroid/support/v4/app/Fragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 34
    new-instance v0, Lco/uk/getmondo/common/f/c;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Lco/uk/getmondo/common/f/c;-><init>(Landroid/content/Context;Landroid/view/View;)V

    invoke-static {v0}, Lcom/c/b/b;->b(Ljava/lang/Object;)Lcom/c/b/b;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/common/f/a;->b:Lcom/c/b/b;

    .line 35
    return-void
.end method

.method public q()V
    .locals 1

    .prologue
    .line 59
    invoke-virtual {p0}, Lco/uk/getmondo/common/f/a;->getActivity()Landroid/support/v4/app/j;

    move-result-object v0

    invoke-static {v0}, Lco/uk/getmondo/force_upgrade/ForceUpgradeActivity;->a(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lco/uk/getmondo/common/f/a;->startActivity(Landroid/content/Intent;)V

    .line 60
    return-void
.end method

.method public u()V
    .locals 1

    .prologue
    .line 64
    invoke-virtual {p0}, Lco/uk/getmondo/common/f/a;->getActivity()Landroid/support/v4/app/j;

    move-result-object v0

    invoke-static {v0}, Lco/uk/getmondo/splash/SplashActivity;->a(Landroid/content/Context;)V

    .line 65
    return-void
.end method
