.class public Lco/uk/getmondo/common/ThirdPartyShareReceiver;
.super Landroid/content/BroadcastReceiver;
.source "ThirdPartyShareReceiver.java"


# instance fields
.field a:Lco/uk/getmondo/common/a;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 3
    .annotation build Landroid/annotation/TargetApi;
        value = 0x16
    .end annotation

    .prologue
    .line 23
    invoke-static {p1}, Lco/uk/getmondo/MonzoApplication;->a(Landroid/content/Context;)Lco/uk/getmondo/MonzoApplication;

    move-result-object v0

    invoke-virtual {v0}, Lco/uk/getmondo/MonzoApplication;->b()Lco/uk/getmondo/common/h/a/a;

    move-result-object v0

    invoke-interface {v0, p0}, Lco/uk/getmondo/common/h/a/a;->a(Lco/uk/getmondo/common/ThirdPartyShareReceiver;)V

    .line 25
    const-string v0, "android.intent.extra.CHOSEN_COMPONENT"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/content/ComponentName;

    .line 27
    const-string v1, "KEY_ANALYTIC_NAME"

    invoke-virtual {p2, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 28
    invoke-virtual {v0}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v0

    .line 29
    iget-object v2, p0, Lco/uk/getmondo/common/ThirdPartyShareReceiver;->a:Lco/uk/getmondo/common/a;

    invoke-static {v1, v0}, Lco/uk/getmondo/api/model/tracking/Impression;->b(Ljava/lang/String;Ljava/lang/String;)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    invoke-virtual {v2, v0}, Lco/uk/getmondo/common/a;->a(Lco/uk/getmondo/api/model/tracking/Impression;)V

    .line 30
    return-void
.end method
