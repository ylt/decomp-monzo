.class public Lco/uk/getmondo/common/u;
.super Ljava/lang/Object;
.source "PreferenceCache.java"

# interfaces
.implements Lco/uk/getmondo/common/s;


# instance fields
.field private final a:Landroid/content/SharedPreferences;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    const-string v0, "co.uk.getmondo"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/common/u;->a:Landroid/content/SharedPreferences;

    .line 23
    return-void
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 43
    const-string v0, "EMAIL_KEY"

    invoke-virtual {p0, v0}, Lco/uk/getmondo/common/u;->f(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 48
    const-string v0, "EMAIL_KEY"

    invoke-virtual {p0, p1, v0}, Lco/uk/getmondo/common/u;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 49
    return-void
.end method

.method protected a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lco/uk/getmondo/common/u;->a:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 32
    if-nez p1, :cond_0

    .line 33
    invoke-interface {v0, p2}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 37
    :goto_0
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 38
    return-void

    .line 35
    :cond_0
    invoke-interface {v0, p2, p1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    goto :goto_0
.end method

.method public a(Z)V
    .locals 2

    .prologue
    .line 91
    iget-object v0, p0, Lco/uk/getmondo/common/u;->a:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "FIRST_TIME_WAITING_LIST_KEY"

    .line 92
    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 93
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 94
    return-void
.end method

.method public b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 54
    const-string v0, "OAUTH_STATE_KEY"

    invoke-virtual {p0, v0}, Lco/uk/getmondo/common/u;->f(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public b(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 59
    const-string v0, "OAUTH_STATE_KEY"

    invoke-virtual {p0, p1, v0}, Lco/uk/getmondo/common/u;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 60
    return-void
.end method

.method public b(Z)V
    .locals 2

    .prologue
    .line 118
    iget-object v0, p0, Lco/uk/getmondo/common/u;->a:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "NOTIFY_WHEN_READY_KEY"

    .line 119
    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 120
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 121
    return-void
.end method

.method public c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 65
    const-string v0, "SESSION_KEY"

    invoke-virtual {p0, v0}, Lco/uk/getmondo/common/u;->f(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public c(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 70
    const-string v0, "SESSION_KEY"

    invoke-virtual {p0, p1, v0}, Lco/uk/getmondo/common/u;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 71
    return-void
.end method

.method public d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 76
    const-string v0, "FINGER_PRINT_KEY"

    invoke-virtual {p0, v0}, Lco/uk/getmondo/common/u;->f(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public d(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 81
    const-string v0, "FINGER_PRINT_KEY"

    invoke-virtual {p0, p1, v0}, Lco/uk/getmondo/common/u;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 82
    return-void
.end method

.method public e(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 108
    const-string v0, "READ_EVENTS_KEY"

    invoke-virtual {p0, p1, v0}, Lco/uk/getmondo/common/u;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 109
    return-void
.end method

.method public e()Z
    .locals 3

    .prologue
    .line 86
    iget-object v0, p0, Lco/uk/getmondo/common/u;->a:Landroid/content/SharedPreferences;

    const-string v1, "FIRST_TIME_WAITING_LIST_KEY"

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public f()Ljava/lang/String;
    .locals 1

    .prologue
    .line 99
    const-string v0, "READ_EVENTS_KEY"

    invoke-virtual {p0, v0}, Lco/uk/getmondo/common/u;->f(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 100
    if-nez v0, :cond_0

    .line 101
    const-string v0, ""

    .line 103
    :cond_0
    return-object v0
.end method

.method protected f(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 27
    iget-object v0, p0, Lco/uk/getmondo/common/u;->a:Landroid/content/SharedPreferences;

    const/4 v1, 0x0

    invoke-interface {v0, p1, v1}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public g()Z
    .locals 3

    .prologue
    .line 113
    iget-object v0, p0, Lco/uk/getmondo/common/u;->a:Landroid/content/SharedPreferences;

    const-string v1, "NOTIFY_WHEN_READY_KEY"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method
