.class public final Lco/uk/getmondo/common/ui/LoadingErrorView;
.super Landroid/support/constraint/ConstraintLayout;
.source "LoadingErrorView.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000H\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0007\n\u0002\u0010\u000b\n\u0002\u0008\u0008\n\u0002\u0010\u000e\n\u0002\u0008\u0011\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u000f\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B%\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\n\u0008\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u0012\u0008\u0008\u0002\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\u0008J,\u00109\u001a\u00020.2\u0006\u0010\u001f\u001a\u00020\u001b2\u0008\u0008\u0002\u0010:\u001a\u00020\u00122\u0008\u0008\u0002\u00106\u001a\u00020\u001b2\u0008\u0008\u0003\u0010\u000c\u001a\u00020\u0007J\u001a\u0010;\u001a\u00020.2\u0006\u0010<\u001a\u00020\u00122\n\u0008\u0002\u0010\u001f\u001a\u0004\u0018\u00010\u001bJ\u000c\u0010=\u001a\u0008\u0012\u0004\u0012\u00020.0>R\u000e\u0010\t\u001a\u00020\nX\u0082\u0004\u00a2\u0006\u0002\n\u0000R&\u0010\u000c\u001a\u00020\u00072\u0006\u0010\u000b\u001a\u00020\u00078\u0006@FX\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\r\u0010\u000e\"\u0004\u0008\u000f\u0010\u0010R\u0012\u0010\u0011\u001a\u00020\u00078\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R$\u0010\u0013\u001a\u00020\u00122\u0006\u0010\u000b\u001a\u00020\u0012@FX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u0014\u0010\u0015\"\u0004\u0008\u0016\u0010\u0017R&\u0010\u0018\u001a\u00020\u00072\u0006\u0010\u000b\u001a\u00020\u00078\u0006@FX\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u0019\u0010\u000e\"\u0004\u0008\u001a\u0010\u0010R\u001e\u0010\u001c\u001a\u00020\u001b2\u0006\u0010\u000b\u001a\u00020\u001b@BX\u0082\u000e\u00a2\u0006\u0008\n\u0000\"\u0004\u0008\u001d\u0010\u001eR(\u0010\u001f\u001a\u0004\u0018\u00010\u001b2\u0008\u0010\u000b\u001a\u0004\u0018\u00010\u001b8F@FX\u0086\u000e\u00a2\u0006\u000c\u001a\u0004\u0008 \u0010!\"\u0004\u0008\"\u0010\u001eR$\u0010#\u001a\u00020\u00072\u0006\u0010\u000b\u001a\u00020\u0007@FX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008$\u0010\u000e\"\u0004\u0008%\u0010\u0010R&\u0010&\u001a\u00020\u00072\u0006\u0010\u000b\u001a\u00020\u00078\u0006@FX\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\'\u0010\u000e\"\u0004\u0008(\u0010\u0010R$\u0010)\u001a\u00020\u00122\u0006\u0010\u000b\u001a\u00020\u0012@FX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008*\u0010\u0015\"\u0004\u0008+\u0010\u0017R\"\u0010,\u001a\n\u0012\u0004\u0012\u00020.\u0018\u00010-X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008/\u00100\"\u0004\u00081\u00102R$\u00103\u001a\u00020\u00122\u0006\u0010\u000b\u001a\u00020\u0012@FX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u00084\u0010\u0015\"\u0004\u00085\u0010\u0017R$\u00106\u001a\u00020\u001b2\u0006\u0010\u000b\u001a\u00020\u001b8F@FX\u0086\u000e\u00a2\u0006\u000c\u001a\u0004\u00087\u0010!\"\u0004\u00088\u0010\u001e\u00a8\u0006?"
    }
    d2 = {
        "Lco/uk/getmondo/common/ui/LoadingErrorView;",
        "Landroid/support/constraint/ConstraintLayout;",
        "context",
        "Landroid/content/Context;",
        "attrs",
        "Landroid/util/AttributeSet;",
        "defStyle",
        "",
        "(Landroid/content/Context;Landroid/util/AttributeSet;I)V",
        "circleBackground",
        "Landroid/graphics/drawable/ShapeDrawable;",
        "value",
        "iconRes",
        "getIconRes",
        "()I",
        "setIconRes",
        "(I)V",
        "iconTint",
        "",
        "loading",
        "getLoading",
        "()Z",
        "setLoading",
        "(Z)V",
        "loadingIconBackgroundColor",
        "getLoadingIconBackgroundColor",
        "setLoadingIconBackgroundColor",
        "",
        "lottieAnimationFileName",
        "setLottieAnimationFileName",
        "(Ljava/lang/String;)V",
        "message",
        "getMessage",
        "()Ljava/lang/String;",
        "setMessage",
        "messageTextColor",
        "getMessageTextColor",
        "setMessageTextColor",
        "overlayBackgroundColor",
        "getOverlayBackgroundColor",
        "setOverlayBackgroundColor",
        "overlayEnabled",
        "getOverlayEnabled",
        "setOverlayEnabled",
        "retryClickListener",
        "Lkotlin/Function0;",
        "",
        "getRetryClickListener",
        "()Lkotlin/jvm/functions/Function0;",
        "setRetryClickListener",
        "(Lkotlin/jvm/functions/Function0;)V",
        "retryEnabled",
        "getRetryEnabled",
        "setRetryEnabled",
        "retryLabel",
        "getRetryLabel",
        "setRetryLabel",
        "error",
        "shouldRetry",
        "load",
        "isLoading",
        "retryClicks",
        "Lio/reactivex/Observable;",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field private final c:Landroid/graphics/drawable/ShapeDrawable;

.field private d:Z

.field private e:I

.field private f:Z

.field private g:I

.field private h:I

.field private i:Z

.field private j:I

.field private k:Lkotlin/d/a/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/d/a/a",
            "<",
            "Lkotlin/n;",
            ">;"
        }
    .end annotation
.end field

.field private l:I

.field private m:Ljava/lang/String;

.field private n:Ljava/util/HashMap;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 6

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x6

    move-object v0, p0

    move-object v1, p1

    move-object v5, v2

    invoke-direct/range {v0 .. v5}, Lco/uk/getmondo/common/ui/LoadingErrorView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/d/b/i;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 6

    const/4 v3, 0x0

    const/4 v4, 0x4

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    invoke-direct/range {v0 .. v5}, Lco/uk/getmondo/common/ui/LoadingErrorView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/d/b/i;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 6

    .prologue
    const v5, 0x7f0f0058

    const v4, 0x7f0f0056

    const/4 v3, 0x0

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 28
    invoke-direct {p0, p1, p2, p3}, Landroid/support/constraint/ConstraintLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 30
    new-instance v1, Landroid/graphics/drawable/ShapeDrawable;

    new-instance v0, Landroid/graphics/drawable/shapes/OvalShape;

    invoke-direct {v0}, Landroid/graphics/drawable/shapes/OvalShape;-><init>()V

    check-cast v0, Landroid/graphics/drawable/shapes/Shape;

    invoke-direct {v1, v0}, Landroid/graphics/drawable/ShapeDrawable;-><init>(Landroid/graphics/drawable/shapes/Shape;)V

    iput-object v1, p0, Lco/uk/getmondo/common/ui/LoadingErrorView;->c:Landroid/graphics/drawable/ShapeDrawable;

    .line 44
    iput v4, p0, Lco/uk/getmondo/common/ui/LoadingErrorView;->e:I

    .line 77
    const v0, 0x7f0f0075

    iput v0, p0, Lco/uk/getmondo/common/ui/LoadingErrorView;->h:I

    .line 109
    iput v5, p0, Lco/uk/getmondo/common/ui/LoadingErrorView;->j:I

    .line 119
    const-string v0, "lottie/loading_spinner_colored.json"

    iput-object v0, p0, Lco/uk/getmondo/common/ui/LoadingErrorView;->m:Ljava/lang/String;

    .line 126
    const v1, 0x7f050142

    move-object v0, p0

    check-cast v0, Landroid/view/ViewGroup;

    invoke-static {p1, v1, v0}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 128
    sget v0, Lco/uk/getmondo/c$a;->progressLayout:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/common/ui/LoadingErrorView;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    .line 195
    invoke-virtual {v0}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v2

    new-instance v1, Lco/uk/getmondo/common/ui/LoadingErrorView$a;

    invoke-direct {v1, v0, p0}, Lco/uk/getmondo/common/ui/LoadingErrorView$a;-><init>(Landroid/view/View;Lco/uk/getmondo/common/ui/LoadingErrorView;)V

    move-object v0, v1

    check-cast v0, Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    invoke-virtual {v2, v0}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 201
    nop

    .line 135
    if-eqz p2, :cond_0

    .line 136
    sget-object v0, Lco/uk/getmondo/c$b;->LoadingErrorView:[I

    invoke-virtual {p1, p2, v0, p3, v3}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v1

    .line 137
    invoke-virtual {v1, v3, v3}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v0

    invoke-virtual {p0, v0}, Lco/uk/getmondo/common/ui/LoadingErrorView;->setOverlayEnabled(Z)V

    .line 138
    const/4 v0, 0x1

    invoke-virtual {v1, v0, v4}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v0

    invoke-virtual {p0, v0}, Lco/uk/getmondo/common/ui/LoadingErrorView;->setOverlayBackgroundColor(I)V

    .line 139
    const/4 v0, 0x5

    invoke-virtual {v1, v0, v3}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v0

    invoke-virtual {p0, v0}, Lco/uk/getmondo/common/ui/LoadingErrorView;->setLoadingIconBackgroundColor(I)V

    .line 140
    const/4 v0, 0x2

    invoke-virtual {v1, v0, v3}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v0

    invoke-virtual {p0, v0}, Lco/uk/getmondo/common/ui/LoadingErrorView;->setLoading(Z)V

    .line 141
    const/4 v0, 0x4

    invoke-virtual {v1, v0, v3}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v0

    iput v0, p0, Lco/uk/getmondo/common/ui/LoadingErrorView;->l:I

    .line 142
    const/4 v0, 0x3

    invoke-virtual {v1, v0, v3}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v0

    invoke-virtual {p0, v0}, Lco/uk/getmondo/common/ui/LoadingErrorView;->setIconRes(I)V

    .line 143
    const/4 v0, 0x6

    invoke-virtual {v1, v0}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lco/uk/getmondo/common/ui/LoadingErrorView;->setMessage(Ljava/lang/String;)V

    .line 144
    const/16 v0, 0x8

    invoke-virtual {v1, v0, v3}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v0

    invoke-virtual {p0, v0}, Lco/uk/getmondo/common/ui/LoadingErrorView;->setRetryEnabled(Z)V

    .line 145
    const/16 v0, 0x9

    invoke-virtual {v1, v0}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    :goto_0
    invoke-virtual {p0, v0}, Lco/uk/getmondo/common/ui/LoadingErrorView;->setRetryLabel(Ljava/lang/String;)V

    .line 146
    const/16 v0, 0xa

    invoke-virtual {v1, v0}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    :goto_1
    invoke-direct {p0, v0}, Lco/uk/getmondo/common/ui/LoadingErrorView;->setLottieAnimationFileName(Ljava/lang/String;)V

    .line 147
    const/4 v0, 0x7

    invoke-virtual {v1, v0, v5}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v0

    invoke-virtual {p0, v0}, Lco/uk/getmondo/common/ui/LoadingErrorView;->setMessageTextColor(I)V

    .line 149
    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    .line 150
    nop

    nop

    .line 152
    :cond_0
    sget v0, Lco/uk/getmondo/c$a;->retryButton:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/common/ui/LoadingErrorView;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    new-instance v1, Lco/uk/getmondo/common/ui/LoadingErrorView$1;

    invoke-direct {v1, p0}, Lco/uk/getmondo/common/ui/LoadingErrorView$1;-><init>(Lco/uk/getmondo/common/ui/LoadingErrorView;)V

    check-cast v1, Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void

    .line 145
    :cond_1
    const v0, 0x7f0a0265

    invoke-static {p0, v0}, Lco/uk/getmondo/common/ae;->a(Landroid/view/View;I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 146
    :cond_2
    const-string v0, "lottie/loading_spinner_colored.json"

    goto :goto_1
.end method

.method public synthetic constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/d/b/i;)V
    .locals 2

    .prologue
    and-int/lit8 v0, p4, 0x2

    if-eqz v0, :cond_1

    .line 26
    const/4 v0, 0x0

    check-cast v0, Landroid/util/AttributeSet;

    :goto_0
    and-int/lit8 v1, p4, 0x4

    if-eqz v1, :cond_0

    .line 27
    const p3, 0x7f010194

    :cond_0
    invoke-direct {p0, p1, v0, p3}, Lco/uk/getmondo/common/ui/LoadingErrorView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void

    :cond_1
    move-object v0, p2

    goto :goto_0
.end method

.method public static final synthetic a(Lco/uk/getmondo/common/ui/LoadingErrorView;)Landroid/graphics/drawable/ShapeDrawable;
    .locals 1

    .prologue
    .line 23
    iget-object v0, p0, Lco/uk/getmondo/common/ui/LoadingErrorView;->c:Landroid/graphics/drawable/ShapeDrawable;

    return-object v0
.end method

.method public static synthetic a(Lco/uk/getmondo/common/ui/LoadingErrorView;Ljava/lang/String;ZLjava/lang/String;IILjava/lang/Object;)V
    .locals 1

    .prologue
    and-int/lit8 v0, p5, 0x2

    if-eqz v0, :cond_0

    .line 175
    iget-boolean p2, p0, Lco/uk/getmondo/common/ui/LoadingErrorView;->i:Z

    :cond_0
    and-int/lit8 v0, p5, 0x4

    if-eqz v0, :cond_1

    .line 176
    invoke-virtual {p0}, Lco/uk/getmondo/common/ui/LoadingErrorView;->getRetryLabel()Ljava/lang/String;

    move-result-object p3

    :cond_1
    and-int/lit8 v0, p5, 0x8

    if-eqz v0, :cond_2

    .line 177
    iget p4, p0, Lco/uk/getmondo/common/ui/LoadingErrorView;->g:I

    :cond_2
    invoke-virtual {p0, p1, p2, p3, p4}, Lco/uk/getmondo/common/ui/LoadingErrorView;->a(Ljava/lang/String;ZLjava/lang/String;I)V

    return-void
.end method

.method private final setLottieAnimationFileName(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 121
    iput-object p1, p0, Lco/uk/getmondo/common/ui/LoadingErrorView;->m:Ljava/lang/String;

    .line 122
    sget v0, Lco/uk/getmondo/c$a;->loadingProgress:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/common/ui/LoadingErrorView;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/airbnb/lottie/LottieAnimationView;

    iget-object v1, p0, Lco/uk/getmondo/common/ui/LoadingErrorView;->m:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/airbnb/lottie/LottieAnimationView;->setAnimation(Ljava/lang/String;)V

    .line 123
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;ZLjava/lang/String;I)V
    .locals 1

    .prologue
    const-string v0, "message"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "retryLabel"

    invoke-static {p3, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 179
    invoke-virtual {p0, p1}, Lco/uk/getmondo/common/ui/LoadingErrorView;->setMessage(Ljava/lang/String;)V

    .line 180
    invoke-virtual {p0, p2}, Lco/uk/getmondo/common/ui/LoadingErrorView;->setRetryEnabled(Z)V

    .line 181
    invoke-virtual {p0, p3}, Lco/uk/getmondo/common/ui/LoadingErrorView;->setRetryLabel(Ljava/lang/String;)V

    .line 182
    invoke-virtual {p0, p4}, Lco/uk/getmondo/common/ui/LoadingErrorView;->setIconRes(I)V

    .line 183
    return-void
.end method

.method public final a(ZLjava/lang/String;)V
    .locals 1

    .prologue
    .line 160
    invoke-virtual {p0, p1}, Lco/uk/getmondo/common/ui/LoadingErrorView;->setLoading(Z)V

    .line 161
    invoke-virtual {p0, p2}, Lco/uk/getmondo/common/ui/LoadingErrorView;->setMessage(Ljava/lang/String;)V

    .line 163
    sget v0, Lco/uk/getmondo/c$a;->iconLayout:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/common/ui/LoadingErrorView;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    invoke-static {v0}, Lco/uk/getmondo/common/ae;->b(Landroid/view/View;)V

    .line 164
    sget v0, Lco/uk/getmondo/c$a;->retryButton:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/common/ui/LoadingErrorView;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    invoke-static {v0}, Lco/uk/getmondo/common/ae;->b(Landroid/view/View;)V

    .line 165
    return-void
.end method

.method public b(I)Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lco/uk/getmondo/common/ui/LoadingErrorView;->n:Ljava/util/HashMap;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lco/uk/getmondo/common/ui/LoadingErrorView;->n:Ljava/util/HashMap;

    :cond_0
    iget-object v0, p0, Lco/uk/getmondo/common/ui/LoadingErrorView;->n:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    if-nez v0, :cond_1

    invoke-virtual {p0, p1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/common/ui/LoadingErrorView;->n:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    return-object v0
.end method

.method public final c()Lio/reactivex/n;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Lkotlin/n;",
            ">;"
        }
    .end annotation

    .prologue
    .line 189
    new-instance v0, Lco/uk/getmondo/common/ui/LoadingErrorView$c;

    invoke-direct {v0, p0}, Lco/uk/getmondo/common/ui/LoadingErrorView$c;-><init>(Lco/uk/getmondo/common/ui/LoadingErrorView;)V

    check-cast v0, Lio/reactivex/p;

    invoke-static {v0}, Lio/reactivex/n;->create(Lio/reactivex/p;)Lio/reactivex/n;

    move-result-object v0

    const-string v1, "Observable.create { emit\u2026stener = null }\n        }"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public final getIconRes()I
    .locals 1

    .prologue
    .line 62
    iget v0, p0, Lco/uk/getmondo/common/ui/LoadingErrorView;->g:I

    return v0
.end method

.method public final getLoading()Z
    .locals 1

    .prologue
    .line 50
    iget-boolean v0, p0, Lco/uk/getmondo/common/ui/LoadingErrorView;->f:Z

    return v0
.end method

.method public final getLoadingIconBackgroundColor()I
    .locals 1

    .prologue
    .line 77
    iget v0, p0, Lco/uk/getmondo/common/ui/LoadingErrorView;->h:I

    return v0
.end method

.method public final getMessage()Ljava/lang/String;
    .locals 1

    .prologue
    .line 91
    sget v0, Lco/uk/getmondo/c$a;->messageText:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/common/ui/LoadingErrorView;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final getMessageTextColor()I
    .locals 1

    .prologue
    .line 109
    iget v0, p0, Lco/uk/getmondo/common/ui/LoadingErrorView;->j:I

    return v0
.end method

.method public final getOverlayBackgroundColor()I
    .locals 1

    .prologue
    .line 44
    iget v0, p0, Lco/uk/getmondo/common/ui/LoadingErrorView;->e:I

    return v0
.end method

.method public final getOverlayEnabled()Z
    .locals 1

    .prologue
    .line 32
    iget-boolean v0, p0, Lco/uk/getmondo/common/ui/LoadingErrorView;->d:Z

    return v0
.end method

.method public final getRetryClickListener()Lkotlin/d/a/a;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/d/a/a",
            "<",
            "Lkotlin/n;",
            ">;"
        }
    .end annotation

    .prologue
    .line 115
    iget-object v0, p0, Lco/uk/getmondo/common/ui/LoadingErrorView;->k:Lkotlin/d/a/a;

    return-object v0
.end method

.method public final getRetryEnabled()Z
    .locals 1

    .prologue
    .line 97
    iget-boolean v0, p0, Lco/uk/getmondo/common/ui/LoadingErrorView;->i:Z

    return v0
.end method

.method public final getRetryLabel()Ljava/lang/String;
    .locals 1

    .prologue
    .line 104
    sget v0, Lco/uk/getmondo/c$a;->retryButton:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/common/ui/LoadingErrorView;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    invoke-virtual {v0}, Landroid/widget/Button;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final setIconRes(I)V
    .locals 3

    .prologue
    .line 64
    iput p1, p0, Lco/uk/getmondo/common/ui/LoadingErrorView;->g:I

    .line 65
    if-eqz p1, :cond_1

    .line 66
    invoke-virtual {p0}, Lco/uk/getmondo/common/ui/LoadingErrorView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, p1}, Landroid/support/v4/content/a;->a(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 67
    iget v0, p0, Lco/uk/getmondo/common/ui/LoadingErrorView;->l:I

    if-eqz v0, :cond_0

    .line 68
    invoke-virtual {p0}, Lco/uk/getmondo/common/ui/LoadingErrorView;->getContext()Landroid/content/Context;

    move-result-object v0

    iget v2, p0, Lco/uk/getmondo/common/ui/LoadingErrorView;->l:I

    invoke-static {v0, v2}, Landroid/support/v4/content/a;->c(Landroid/content/Context;I)I

    move-result v0

    invoke-static {v1, v0}, Landroid/support/v4/a/a/a;->a(Landroid/graphics/drawable/Drawable;I)V

    .line 70
    :cond_0
    sget v0, Lco/uk/getmondo/c$a;->iconImageView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/common/ui/LoadingErrorView;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 71
    sget v0, Lco/uk/getmondo/c$a;->iconLayout:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/common/ui/LoadingErrorView;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    invoke-static {v0}, Lco/uk/getmondo/common/ae;->a(Landroid/view/View;)V

    .line 74
    :goto_0
    return-void

    .line 73
    :cond_1
    sget v0, Lco/uk/getmondo/c$a;->iconLayout:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/common/ui/LoadingErrorView;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    invoke-static {v0}, Lco/uk/getmondo/common/ae;->b(Landroid/view/View;)V

    goto :goto_0
.end method

.method public final setLoading(Z)V
    .locals 1

    .prologue
    .line 52
    iput-boolean p1, p0, Lco/uk/getmondo/common/ui/LoadingErrorView;->f:Z

    .line 53
    if-eqz p1, :cond_0

    .line 54
    sget v0, Lco/uk/getmondo/c$a;->loadingProgress:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/common/ui/LoadingErrorView;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/airbnb/lottie/LottieAnimationView;

    invoke-virtual {v0}, Lcom/airbnb/lottie/LottieAnimationView;->c()V

    .line 55
    sget v0, Lco/uk/getmondo/c$a;->progressLayout:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/common/ui/LoadingErrorView;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    invoke-static {v0}, Lco/uk/getmondo/common/ae;->a(Landroid/view/View;)V

    .line 59
    :goto_0
    return-void

    .line 57
    :cond_0
    sget v0, Lco/uk/getmondo/c$a;->progressLayout:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/common/ui/LoadingErrorView;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    invoke-static {v0}, Lco/uk/getmondo/common/ae;->b(Landroid/view/View;)V

    .line 58
    sget v0, Lco/uk/getmondo/c$a;->loadingProgress:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/common/ui/LoadingErrorView;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/airbnb/lottie/LottieAnimationView;

    invoke-virtual {v0}, Lcom/airbnb/lottie/LottieAnimationView;->d()V

    goto :goto_0
.end method

.method public final setLoadingIconBackgroundColor(I)V
    .locals 3

    .prologue
    .line 79
    iput p1, p0, Lco/uk/getmondo/common/ui/LoadingErrorView;->h:I

    .line 80
    packed-switch p1, :pswitch_data_0

    .line 82
    iget-object v1, p0, Lco/uk/getmondo/common/ui/LoadingErrorView;->c:Landroid/graphics/drawable/ShapeDrawable;

    invoke-virtual {v1}, Landroid/graphics/drawable/ShapeDrawable;->getPaint()Landroid/graphics/Paint;

    move-result-object v0

    invoke-virtual {p0}, Lco/uk/getmondo/common/ui/LoadingErrorView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2, p1}, Landroid/support/v4/content/a;->c(Landroid/content/Context;I)I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 84
    new-instance v0, Lco/uk/getmondo/common/ui/LoadingErrorView$b;

    invoke-direct {v0, p0, v1}, Lco/uk/getmondo/common/ui/LoadingErrorView$b;-><init>(Lco/uk/getmondo/common/ui/LoadingErrorView;Landroid/graphics/drawable/ShapeDrawable;)V

    check-cast v0, Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Lco/uk/getmondo/common/ui/LoadingErrorView;->post(Ljava/lang/Runnable;)Z

    .line 88
    :pswitch_0
    return-void

    .line 80
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method public final setMessage(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 93
    sget v0, Lco/uk/getmondo/c$a;->messageText:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/common/ui/LoadingErrorView;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    move-object v1, p1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 94
    if-nez p1, :cond_0

    sget v0, Lco/uk/getmondo/c$a;->messageText:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/common/ui/LoadingErrorView;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-static {v0}, Lco/uk/getmondo/common/ae;->b(Landroid/view/View;)V

    .line 95
    :goto_0
    return-void

    .line 94
    :cond_0
    sget v0, Lco/uk/getmondo/c$a;->messageText:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/common/ui/LoadingErrorView;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-static {v0}, Lco/uk/getmondo/common/ae;->a(Landroid/view/View;)V

    goto :goto_0
.end method

.method public final setMessageTextColor(I)V
    .locals 2

    .prologue
    .line 111
    iput p1, p0, Lco/uk/getmondo/common/ui/LoadingErrorView;->j:I

    .line 112
    sget v0, Lco/uk/getmondo/c$a;->messageText:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/common/ui/LoadingErrorView;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {p0}, Lco/uk/getmondo/common/ui/LoadingErrorView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, p1}, Landroid/support/v4/content/a;->c(Landroid/content/Context;I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 113
    return-void
.end method

.method public final setOverlayBackgroundColor(I)V
    .locals 1

    .prologue
    .line 46
    iput p1, p0, Lco/uk/getmondo/common/ui/LoadingErrorView;->e:I

    .line 47
    sget v0, Lco/uk/getmondo/c$a;->loadingOverlay:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/common/ui/LoadingErrorView;->b(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/view/View;->setBackgroundResource(I)V

    .line 48
    return-void
.end method

.method public final setOverlayEnabled(Z)V
    .locals 3

    .prologue
    const/4 v2, 0x2

    .line 34
    iput-boolean p1, p0, Lco/uk/getmondo/common/ui/LoadingErrorView;->d:Z

    .line 35
    if-eqz p1, :cond_0

    .line 36
    sget v0, Lco/uk/getmondo/c$a;->loadingOverlay:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/common/ui/LoadingErrorView;->b(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Lco/uk/getmondo/common/ae;->a(Landroid/view/View;)V

    .line 37
    sget v0, Lco/uk/getmondo/c$a;->messageText:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/common/ui/LoadingErrorView;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const/high16 v1, 0x41800000    # 16.0f

    invoke-virtual {v0, v2, v1}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 41
    :goto_0
    return-void

    .line 39
    :cond_0
    sget v0, Lco/uk/getmondo/c$a;->loadingOverlay:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/common/ui/LoadingErrorView;->b(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Lco/uk/getmondo/common/ae;->b(Landroid/view/View;)V

    .line 40
    sget v0, Lco/uk/getmondo/c$a;->messageText:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/common/ui/LoadingErrorView;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const/high16 v1, 0x41600000    # 14.0f

    invoke-virtual {v0, v2, v1}, Landroid/widget/TextView;->setTextSize(IF)V

    goto :goto_0
.end method

.method public final setRetryClickListener(Lkotlin/d/a/a;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/d/a/a",
            "<",
            "Lkotlin/n;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 115
    iput-object p1, p0, Lco/uk/getmondo/common/ui/LoadingErrorView;->k:Lkotlin/d/a/a;

    return-void
.end method

.method public final setRetryEnabled(Z)V
    .locals 1

    .prologue
    .line 99
    iput-boolean p1, p0, Lco/uk/getmondo/common/ui/LoadingErrorView;->i:Z

    .line 100
    if-eqz p1, :cond_0

    sget v0, Lco/uk/getmondo/c$a;->retryButton:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/common/ui/LoadingErrorView;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    invoke-static {v0}, Lco/uk/getmondo/common/ae;->a(Landroid/view/View;)V

    .line 101
    :goto_0
    return-void

    .line 100
    :cond_0
    sget v0, Lco/uk/getmondo/c$a;->retryButton:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/common/ui/LoadingErrorView;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    invoke-static {v0}, Lco/uk/getmondo/common/ae;->b(Landroid/view/View;)V

    goto :goto_0
.end method

.method public final setRetryLabel(Ljava/lang/String;)V
    .locals 1

    .prologue
    const-string v0, "value"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 106
    sget v0, Lco/uk/getmondo/c$a;->retryButton:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/common/ui/LoadingErrorView;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    check-cast p1, Ljava/lang/CharSequence;

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 107
    return-void
.end method
