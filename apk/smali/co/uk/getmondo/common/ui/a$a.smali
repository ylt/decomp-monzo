.class public final Lco/uk/getmondo/common/ui/a$a;
.super Ljava/lang/Object;
.source "AvatarGenerator.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lco/uk/getmondo/common/ui/a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0008\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u0010\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006H\u0007\u00a8\u0006\u0007"
    }
    d2 = {
        "Lco/uk/getmondo/common/ui/AvatarGenerator$Companion;",
        "",
        "()V",
        "ofCategories",
        "Lco/uk/getmondo/common/ui/AvatarGenerator;",
        "context",
        "Landroid/content/Context;",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 56
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/d/b/i;)V
    .locals 0

    .prologue
    .line 56
    invoke-direct {p0}, Lco/uk/getmondo/common/ui/a$a;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;)Lco/uk/getmondo/common/ui/a;
    .locals 4

    .prologue
    const/4 v2, 0x0

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 59
    const/16 v0, 0x8

    new-array v0, v0, [Lco/uk/getmondo/d/h;

    .line 60
    sget-object v1, Lco/uk/getmondo/d/h;->GROCERIES:Lco/uk/getmondo/d/h;

    aput-object v1, v0, v2

    .line 61
    const/4 v1, 0x1

    sget-object v3, Lco/uk/getmondo/d/h;->ENTERTAINMENT:Lco/uk/getmondo/d/h;

    aput-object v3, v0, v1

    .line 62
    const/4 v1, 0x2

    sget-object v3, Lco/uk/getmondo/d/h;->EATING_OUT:Lco/uk/getmondo/d/h;

    aput-object v3, v0, v1

    .line 63
    const/4 v1, 0x3

    sget-object v3, Lco/uk/getmondo/d/h;->SHOPPING:Lco/uk/getmondo/d/h;

    aput-object v3, v0, v1

    .line 64
    const/4 v1, 0x4

    sget-object v3, Lco/uk/getmondo/d/h;->HOLIDAYS:Lco/uk/getmondo/d/h;

    aput-object v3, v0, v1

    .line 65
    const/4 v1, 0x5

    sget-object v3, Lco/uk/getmondo/d/h;->EXPENSES:Lco/uk/getmondo/d/h;

    aput-object v3, v0, v1

    .line 66
    const/4 v1, 0x6

    sget-object v3, Lco/uk/getmondo/d/h;->TRANSPORT:Lco/uk/getmondo/d/h;

    aput-object v3, v0, v1

    .line 67
    const/4 v1, 0x7

    sget-object v3, Lco/uk/getmondo/d/h;->CASH:Lco/uk/getmondo/d/h;

    aput-object v3, v0, v1

    check-cast v0, [Ljava/lang/Object;

    .line 59
    nop

    .line 68
    nop

    .line 76
    new-instance v1, Ljava/util/ArrayList;

    array-length v3, v0

    invoke-direct {v1, v3}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v1, Ljava/util/Collection;

    move v3, v2

    .line 77
    :goto_0
    array-length v2, v0

    if-ge v3, v2, :cond_0

    aget-object v2, v0, v3

    .line 78
    check-cast v2, Lco/uk/getmondo/d/h;

    .line 68
    invoke-virtual {v2}, Lco/uk/getmondo/d/h;->b()I

    move-result v2

    invoke-static {p1, v2}, Landroid/support/v4/content/a;->c(Landroid/content/Context;I)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 77
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_0

    .line 79
    :cond_0
    check-cast v1, Ljava/util/List;

    .line 69
    new-instance v0, Lco/uk/getmondo/common/ui/a;

    invoke-direct {v0, v1}, Lco/uk/getmondo/common/ui/a;-><init>(Ljava/util/List;)V

    return-object v0
.end method
