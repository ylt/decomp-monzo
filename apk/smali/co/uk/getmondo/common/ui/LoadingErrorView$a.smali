.class public final Lco/uk/getmondo/common/ui/LoadingErrorView$a;
.super Ljava/lang/Object;
.source "ViewExt.kt"

# interfaces
.implements Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lco/uk/getmondo/common/ui/LoadingErrorView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000\u0013\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0000*\u0001\u0000\u0008\n\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0008\u0010\u0003\u001a\u00020\u0004H\u0016\u00a8\u0006\u0005"
    }
    d2 = {
        "co/uk/getmondo/common/ViewExtKt$afterLayout$1",
        "Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;",
        "(Landroid/view/View;Lkotlin/jvm/functions/Function1;)V",
        "onGlobalLayout",
        "",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field final synthetic a:Landroid/view/View;

.field final synthetic b:Lco/uk/getmondo/common/ui/LoadingErrorView;


# direct methods
.method public constructor <init>(Landroid/view/View;Lco/uk/getmondo/common/ui/LoadingErrorView;)V
    .locals 0

    .prologue
    iput-object p1, p0, Lco/uk/getmondo/common/ui/LoadingErrorView$a;->a:Landroid/view/View;

    iput-object p2, p0, Lco/uk/getmondo/common/ui/LoadingErrorView$a;->b:Lco/uk/getmondo/common/ui/LoadingErrorView;

    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onGlobalLayout()V
    .locals 4

    .prologue
    .line 38
    iget-object v0, p0, Lco/uk/getmondo/common/ui/LoadingErrorView$a;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v1

    move-object v0, p0

    check-cast v0, Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    invoke-virtual {v1, v0}, Landroid/view/ViewTreeObserver;->removeOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 39
    iget-object v0, p0, Lco/uk/getmondo/common/ui/LoadingErrorView$a;->a:Landroid/view/View;

    check-cast v0, Landroid/widget/FrameLayout;

    .line 49
    iget-object v1, p0, Lco/uk/getmondo/common/ui/LoadingErrorView$a;->b:Lco/uk/getmondo/common/ui/LoadingErrorView;

    invoke-static {v1}, Lco/uk/getmondo/common/ui/LoadingErrorView;->a(Lco/uk/getmondo/common/ui/LoadingErrorView;)Landroid/graphics/drawable/ShapeDrawable;

    move-result-object v2

    move-object v1, v0

    .line 50
    check-cast v1, Landroid/view/View;

    sget v3, Lco/uk/getmondo/c$a;->progressLayout:I

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/FrameLayout;

    invoke-virtual {v1}, Landroid/widget/FrameLayout;->getMeasuredWidth()I

    move-result v1

    invoke-virtual {v2, v1}, Landroid/graphics/drawable/ShapeDrawable;->setIntrinsicWidth(I)V

    .line 51
    check-cast v0, Landroid/view/View;

    sget v1, Lco/uk/getmondo/c$a;->progressLayout:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->getMeasuredHeight()I

    move-result v0

    invoke-virtual {v2, v0}, Landroid/graphics/drawable/ShapeDrawable;->setIntrinsicHeight(I)V

    .line 52
    nop

    .line 49
    nop

    .line 53
    nop

    .line 40
    return-void
.end method
