.class public final Lco/uk/getmondo/common/ui/j;
.super Ljava/lang/Object;
.source "SpannableStringBuilder.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000<\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\r\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0002\u0008\u0006\u0018\u00002\u00020\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0005J\u0018\u0010\n\u001a\u00020\u000b2\u0006\u0010\u000c\u001a\u00020\u00012\u0006\u0010\r\u001a\u00020\u000eH\u0002J\u0006\u0010\u000f\u001a\u00020\u0010J\u0018\u0010\u0011\u001a\u00020\u00002\u0006\u0010\u0012\u001a\u00020\u00132\u0008\u0008\u0002\u0010\r\u001a\u00020\u000eJ\u0018\u0010\u0014\u001a\u00020\u00002\u0006\u0010\u0015\u001a\u00020\u00132\u0008\u0008\u0002\u0010\r\u001a\u00020\u000eJ \u0010\u0016\u001a\u00020\u00002\u0006\u0010\u0017\u001a\u00020\u00032\u0006\u0010\u0018\u001a\u00020\u00132\u0008\u0008\u0002\u0010\r\u001a\u00020\u000eR\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0019"
    }
    d2 = {
        "Lco/uk/getmondo/common/ui/SpannableBuilder;",
        "",
        "text",
        "",
        "textToFormat",
        "(Ljava/lang/String;Ljava/lang/String;)V",
        "matcher",
        "Ljava/util/regex/Matcher;",
        "spannableString",
        "Landroid/text/SpannableString;",
        "apply",
        "",
        "span",
        "applyOnlyToSubstring",
        "",
        "build",
        "",
        "setColour",
        "colour",
        "",
        "setSize",
        "size",
        "setTypeface",
        "fontFamily",
        "style",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field private final a:Ljava/util/regex/Matcher;

.field private final b:Landroid/text/SpannableString;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    const-string v0, "text"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "textToFormat"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 12
    invoke-static {p2}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v1

    move-object v0, p1

    check-cast v0, Ljava/lang/CharSequence;

    invoke-virtual {v1, v0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    const-string v1, "Pattern.compile(textToFormat).matcher(text)"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v0, p0, Lco/uk/getmondo/common/ui/j;->a:Ljava/util/regex/Matcher;

    .line 13
    new-instance v0, Landroid/text/SpannableString;

    check-cast p1, Ljava/lang/CharSequence;

    invoke-direct {v0, p1}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    iput-object v0, p0, Lco/uk/getmondo/common/ui/j;->b:Landroid/text/SpannableString;

    return-void
.end method

.method private final a(Ljava/lang/Object;Z)V
    .locals 4

    .prologue
    const/16 v3, 0x21

    .line 31
    if-eqz p2, :cond_1

    .line 32
    :goto_0
    iget-object v0, p0, Lco/uk/getmondo/common/ui/j;->a:Ljava/util/regex/Matcher;

    invoke-virtual {v0}, Ljava/util/regex/Matcher;->find()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 33
    iget-object v0, p0, Lco/uk/getmondo/common/ui/j;->b:Landroid/text/SpannableString;

    iget-object v1, p0, Lco/uk/getmondo/common/ui/j;->a:Ljava/util/regex/Matcher;

    invoke-virtual {v1}, Ljava/util/regex/Matcher;->start()I

    move-result v1

    iget-object v2, p0, Lco/uk/getmondo/common/ui/j;->a:Ljava/util/regex/Matcher;

    invoke-virtual {v2}, Ljava/util/regex/Matcher;->end()I

    move-result v2

    invoke-virtual {v0, p1, v1, v2, v3}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    goto :goto_0

    .line 35
    :cond_0
    iget-object v0, p0, Lco/uk/getmondo/common/ui/j;->a:Ljava/util/regex/Matcher;

    invoke-virtual {v0}, Ljava/util/regex/Matcher;->reset()Ljava/util/regex/Matcher;

    .line 38
    :goto_1
    return-void

    .line 37
    :cond_1
    iget-object v0, p0, Lco/uk/getmondo/common/ui/j;->b:Landroid/text/SpannableString;

    const/4 v1, 0x0

    iget-object v2, p0, Lco/uk/getmondo/common/ui/j;->b:Landroid/text/SpannableString;

    invoke-virtual {v2}, Landroid/text/SpannableString;->length()I

    move-result v2

    invoke-virtual {v0, p1, v1, v2, v3}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    goto :goto_1
.end method


# virtual methods
.method public final a(IZ)Lco/uk/getmondo/common/ui/j;
    .locals 1

    .prologue
    .line 21
    new-instance v0, Landroid/text/style/ForegroundColorSpan;

    invoke-direct {v0, p1}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    invoke-direct {p0, v0, p2}, Lco/uk/getmondo/common/ui/j;->a(Ljava/lang/Object;Z)V

    .line 22
    return-object p0
.end method

.method public final a(Ljava/lang/String;IZ)Lco/uk/getmondo/common/ui/j;
    .locals 3

    .prologue
    const-string v0, "fontFamily"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 16
    new-instance v0, Lco/uk/getmondo/common/ui/k;

    invoke-static {p1, p2}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v1

    const-string v2, "Typeface.create(fontFamily, style)"

    invoke-static {v1, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v0, v1}, Lco/uk/getmondo/common/ui/k;-><init>(Landroid/graphics/Typeface;)V

    invoke-direct {p0, v0, p3}, Lco/uk/getmondo/common/ui/j;->a(Ljava/lang/Object;Z)V

    .line 17
    return-object p0
.end method

.method public final a()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lco/uk/getmondo/common/ui/j;->b:Landroid/text/SpannableString;

    check-cast v0, Ljava/lang/CharSequence;

    return-object v0
.end method
