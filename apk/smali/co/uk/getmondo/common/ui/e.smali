.class public final Lco/uk/getmondo/common/ui/e;
.super Landroid/support/v7/widget/al;
.source "InsetDividerItemDecoration.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0006\u0018\u00002\u00020\u0001BI\u0012\u0008\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0008\u0008\u0002\u0010\u0006\u001a\u00020\u0007\u0012\u0008\u0008\u0002\u0010\u0008\u001a\u00020\u0005\u0012\u0008\u0008\u0002\u0010\t\u001a\u00020\u0005\u0012\u0008\u0008\u0002\u0010\n\u001a\u00020\u0005\u0012\u0008\u0008\u0002\u0010\u000b\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u000c\u00a8\u0006\r"
    }
    d2 = {
        "Lco/uk/getmondo/common/ui/InsetDividerItemDecoration;",
        "Landroid/support/v7/widget/DividerItemDecoration;",
        "context",
        "Landroid/content/Context;",
        "orientation",
        "",
        "dividerDrawable",
        "Landroid/graphics/drawable/Drawable;",
        "insetLeft",
        "insetTop",
        "insetRight",
        "insetBottom",
        "(Landroid/content/Context;ILandroid/graphics/drawable/Drawable;IIII)V",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# direct methods
.method public constructor <init>(Landroid/content/Context;ILandroid/graphics/drawable/Drawable;IIII)V
    .locals 6

    .prologue
    const-string v0, "dividerDrawable"

    invoke-static {p3, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 19
    invoke-direct {p0, p1, p2}, Landroid/support/v7/widget/al;-><init>(Landroid/content/Context;I)V

    .line 22
    new-instance v0, Landroid/graphics/drawable/InsetDrawable;

    move-object v1, p3

    move v2, p4

    move v3, p5

    move v4, p6

    move v5, p7

    invoke-direct/range {v0 .. v5}, Landroid/graphics/drawable/InsetDrawable;-><init>(Landroid/graphics/drawable/Drawable;IIII)V

    check-cast v0, Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0, v0}, Lco/uk/getmondo/common/ui/e;->a(Landroid/graphics/drawable/Drawable;)V

    return-void
.end method

.method public synthetic constructor <init>(Landroid/content/Context;ILandroid/graphics/drawable/Drawable;IIIIILkotlin/d/b/i;)V
    .locals 8

    .prologue
    and-int/lit8 v0, p8, 0x4

    if-eqz v0, :cond_4

    .line 14
    const v0, 0x7f0201fc

    invoke-static {p1, v0}, Landroid/support/v4/content/a;->a(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    const-string v0, "ContextCompat.getDrawabl\u2026.drawable.simple_divider)"

    invoke-static {v3, v0}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    :goto_0
    and-int/lit8 v0, p8, 0x8

    if-eqz v0, :cond_3

    .line 15
    const/4 v4, 0x0

    :goto_1
    and-int/lit8 v0, p8, 0x10

    if-eqz v0, :cond_2

    .line 16
    const/4 v5, 0x0

    :goto_2
    and-int/lit8 v0, p8, 0x20

    if-eqz v0, :cond_1

    .line 17
    const/4 v6, 0x0

    :goto_3
    and-int/lit8 v0, p8, 0x40

    if-eqz v0, :cond_0

    .line 18
    const/4 v7, 0x0

    :goto_4
    move-object v0, p0

    move-object v1, p1

    move v2, p2

    invoke-direct/range {v0 .. v7}, Lco/uk/getmondo/common/ui/e;-><init>(Landroid/content/Context;ILandroid/graphics/drawable/Drawable;IIII)V

    return-void

    :cond_0
    move v7, p7

    goto :goto_4

    :cond_1
    move v6, p6

    goto :goto_3

    :cond_2
    move v5, p5

    goto :goto_2

    :cond_3
    move v4, p4

    goto :goto_1

    :cond_4
    move-object v3, p3

    goto :goto_0
.end method
