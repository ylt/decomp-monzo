.class Lco/uk/getmondo/common/ui/PinEntryView$1;
.super Ljava/lang/Object;
.source "PinEntryView.java"

# interfaces
.implements Landroid/text/TextWatcher;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lco/uk/getmondo/common/ui/PinEntryView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lco/uk/getmondo/common/ui/PinEntryView;


# direct methods
.method constructor <init>(Lco/uk/getmondo/common/ui/PinEntryView;)V
    .locals 0

    .prologue
    .line 108
    iput-object p1, p0, Lco/uk/getmondo/common/ui/PinEntryView$1;->a:Lco/uk/getmondo/common/ui/PinEntryView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 2

    .prologue
    .line 119
    iget-object v0, p0, Lco/uk/getmondo/common/ui/PinEntryView$1;->a:Lco/uk/getmondo/common/ui/PinEntryView;

    invoke-virtual {v0}, Lco/uk/getmondo/common/ui/PinEntryView;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 120
    iget-object v0, p0, Lco/uk/getmondo/common/ui/PinEntryView$1;->a:Lco/uk/getmondo/common/ui/PinEntryView;

    invoke-interface {p1}, Landroid/text/Editable;->length()I

    move-result v1

    invoke-static {v0, v1}, Lco/uk/getmondo/common/ui/PinEntryView;->a(Lco/uk/getmondo/common/ui/PinEntryView;I)I

    .line 121
    iget-object v0, p0, Lco/uk/getmondo/common/ui/PinEntryView$1;->a:Lco/uk/getmondo/common/ui/PinEntryView;

    invoke-static {v0}, Lco/uk/getmondo/common/ui/PinEntryView;->a(Lco/uk/getmondo/common/ui/PinEntryView;)I

    move-result v0

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lco/uk/getmondo/common/ui/PinEntryView$1;->a:Lco/uk/getmondo/common/ui/PinEntryView;

    invoke-static {v0}, Lco/uk/getmondo/common/ui/PinEntryView;->b(Lco/uk/getmondo/common/ui/PinEntryView;)Lco/uk/getmondo/common/ui/PinEntryView$a;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 122
    iget-object v0, p0, Lco/uk/getmondo/common/ui/PinEntryView$1;->a:Lco/uk/getmondo/common/ui/PinEntryView;

    invoke-static {v0}, Lco/uk/getmondo/common/ui/PinEntryView;->b(Lco/uk/getmondo/common/ui/PinEntryView;)Lco/uk/getmondo/common/ui/PinEntryView$a;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lco/uk/getmondo/common/ui/PinEntryView$a;->a(Ljava/lang/String;)V

    .line 124
    :cond_0
    iget-object v0, p0, Lco/uk/getmondo/common/ui/PinEntryView$1;->a:Lco/uk/getmondo/common/ui/PinEntryView;

    invoke-virtual {v0}, Lco/uk/getmondo/common/ui/PinEntryView;->invalidate()V

    .line 126
    :cond_1
    return-void
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 111
    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 115
    return-void
.end method
