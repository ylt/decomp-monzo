.class public final Lco/uk/getmondo/common/ui/i;
.super Ljava/lang/Object;
.source "SnackbarUtils.java"


# direct methods
.method public static a(Landroid/content/Context;Landroid/view/View;Ljava/lang/String;IZ)Landroid/support/design/widget/Snackbar;
    .locals 4

    .prologue
    .line 18
    if-eqz p4, :cond_0

    .line 19
    const v0, 0x7f0f00d0

    .line 23
    :goto_0
    invoke-static {p0, v0}, Landroid/support/v4/content/a;->c(Landroid/content/Context;I)I

    move-result v0

    .line 24
    const v1, 0x7f0f00ea

    invoke-static {p0, v1}, Landroid/support/v4/content/a;->c(Landroid/content/Context;I)I

    move-result v1

    .line 26
    invoke-static {p1, p2, p3}, Landroid/support/design/widget/Snackbar;->a(Landroid/view/View;Ljava/lang/CharSequence;I)Landroid/support/design/widget/Snackbar;

    move-result-object v2

    .line 27
    invoke-virtual {v2}, Landroid/support/design/widget/Snackbar;->b()Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, v0}, Landroid/view/View;->setBackgroundColor(I)V

    .line 28
    invoke-virtual {v2, v1}, Landroid/support/design/widget/Snackbar;->e(I)Landroid/support/design/widget/Snackbar;

    .line 29
    invoke-virtual {v2}, Landroid/support/design/widget/Snackbar;->b()Landroid/view/View;

    move-result-object v0

    const v3, 0x7f1102b2

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 30
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 31
    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setMaxLines(I)V

    .line 32
    return-object v2

    .line 21
    :cond_0
    const v0, 0x7f0f00bf

    goto :goto_0
.end method
