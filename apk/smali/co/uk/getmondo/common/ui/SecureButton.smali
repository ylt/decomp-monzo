.class public Lco/uk/getmondo/common/ui/SecureButton;
.super Landroid/support/v7/widget/i;
.source "SecureButton.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 21
    invoke-direct {p0, p1}, Landroid/support/v7/widget/i;-><init>(Landroid/content/Context;)V

    .line 22
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lco/uk/getmondo/common/ui/SecureButton;->setFilterTouchesWhenObscured(Z)V

    .line 23
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 26
    invoke-direct {p0, p1, p2}, Landroid/support/v7/widget/i;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 27
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lco/uk/getmondo/common/ui/SecureButton;->setFilterTouchesWhenObscured(Z)V

    .line 28
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 31
    invoke-direct {p0, p1, p2, p3}, Landroid/support/v7/widget/i;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 32
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lco/uk/getmondo/common/ui/SecureButton;->setFilterTouchesWhenObscured(Z)V

    .line 33
    return-void
.end method


# virtual methods
.method public onFilterTouchEventForSecurity(Landroid/view/MotionEvent;)Z
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 43
    invoke-super {p0, p1}, Landroid/support/v7/widget/i;->onFilterTouchEventForSecurity(Landroid/view/MotionEvent;)Z

    move-result v0

    if-nez v0, :cond_1

    move v3, v1

    .line 44
    :goto_0
    if-eqz v3, :cond_0

    invoke-virtual {p0}, Lco/uk/getmondo/common/ui/SecureButton;->getContext()Landroid/content/Context;

    move-result-object v0

    instance-of v0, v0, Landroid/support/v4/app/j;

    if-eqz v0, :cond_0

    .line 45
    invoke-virtual {p0}, Lco/uk/getmondo/common/ui/SecureButton;->getContext()Landroid/content/Context;

    move-result-object v0

    const v4, 0x7f0a0341

    invoke-virtual {v0, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 46
    invoke-virtual {p0}, Lco/uk/getmondo/common/ui/SecureButton;->getContext()Landroid/content/Context;

    move-result-object v4

    const v5, 0x7f0a0340

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 48
    invoke-static {v0, v4, v2}, Lco/uk/getmondo/common/d/a;->a(Ljava/lang/String;Ljava/lang/String;Z)Lco/uk/getmondo/common/d/a;

    move-result-object v4

    .line 49
    invoke-virtual {p0}, Lco/uk/getmondo/common/ui/SecureButton;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/j;

    invoke-virtual {v0}, Landroid/support/v4/app/j;->getSupportFragmentManager()Landroid/support/v4/app/n;

    move-result-object v0

    const-string v5, "MESSAGE_FRAGMENT_TAG"

    invoke-virtual {v4, v0, v5}, Lco/uk/getmondo/common/d/a;->show(Landroid/support/v4/app/n;Ljava/lang/String;)V

    .line 51
    :cond_0
    if-nez v3, :cond_2

    move v0, v1

    :goto_1
    return v0

    :cond_1
    move v3, v2

    .line 43
    goto :goto_0

    :cond_2
    move v0, v2

    .line 51
    goto :goto_1
.end method

.method public setFilterTouchesWhenObscured(Z)V
    .locals 1

    .prologue
    .line 38
    const/4 v0, 0x1

    invoke-super {p0, v0}, Landroid/support/v7/widget/i;->setFilterTouchesWhenObscured(Z)V

    .line 39
    return-void
.end method
