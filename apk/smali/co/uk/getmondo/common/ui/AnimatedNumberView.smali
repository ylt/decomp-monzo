.class public Lco/uk/getmondo/common/ui/AnimatedNumberView;
.super Landroid/widget/TextView;
.source "AnimatedNumberView.java"


# instance fields
.field private a:Landroid/animation/ObjectAnimator;

.field private b:I

.field private c:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 23
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lco/uk/getmondo/common/ui/AnimatedNumberView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 24
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 27
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lco/uk/getmondo/common/ui/AnimatedNumberView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 28
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3

    .prologue
    const/16 v2, 0xbb8

    .line 31
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/TextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 33
    if-eqz p2, :cond_0

    .line 34
    invoke-virtual {p0}, Lco/uk/getmondo/common/ui/AnimatedNumberView;->getContext()Landroid/content/Context;

    move-result-object v0

    sget-object v1, Lco/uk/getmondo/c$b;->AnimatedNumberView:[I

    invoke-virtual {v0, p2, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 35
    const/4 v1, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v1

    iput v1, p0, Lco/uk/getmondo/common/ui/AnimatedNumberView;->c:I

    .line 36
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 40
    :goto_0
    return-void

    .line 38
    :cond_0
    iput v2, p0, Lco/uk/getmondo/common/ui/AnimatedNumberView;->c:I

    goto :goto_0
.end method

.method private setNumberAsString(I)V
    .locals 4

    .prologue
    .line 68
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-static {v0}, Ljava/text/NumberFormat;->getNumberInstance(Ljava/util/Locale;)Ljava/text/NumberFormat;

    move-result-object v0

    int-to-long v2, p1

    invoke-virtual {v0, v2, v3}, Ljava/text/NumberFormat;->format(J)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lco/uk/getmondo/common/ui/AnimatedNumberView;->setText(Ljava/lang/CharSequence;)V

    .line 69
    return-void
.end method


# virtual methods
.method public a(I)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 72
    iget-object v0, p0, Lco/uk/getmondo/common/ui/AnimatedNumberView;->a:Landroid/animation/ObjectAnimator;

    if-eqz v0, :cond_0

    .line 73
    iget-object v0, p0, Lco/uk/getmondo/common/ui/AnimatedNumberView;->a:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->cancel()V

    .line 75
    :cond_0
    const-string v0, "numberInterpolator"

    const/4 v1, 0x2

    new-array v1, v1, [I

    iget v2, p0, Lco/uk/getmondo/common/ui/AnimatedNumberView;->b:I

    sub-int/2addr v2, p1

    invoke-static {v2, v3}, Ljava/lang/Math;->max(II)I

    move-result v2

    aput v2, v1, v3

    const/4 v2, 0x1

    iget v3, p0, Lco/uk/getmondo/common/ui/AnimatedNumberView;->b:I

    aput v3, v1, v2

    invoke-static {p0, v0, v1}, Landroid/animation/ObjectAnimator;->ofInt(Ljava/lang/Object;Ljava/lang/String;[I)Landroid/animation/ObjectAnimator;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/common/ui/AnimatedNumberView;->a:Landroid/animation/ObjectAnimator;

    .line 76
    iget-object v0, p0, Lco/uk/getmondo/common/ui/AnimatedNumberView;->a:Landroid/animation/ObjectAnimator;

    new-instance v1, Landroid/view/animation/DecelerateInterpolator;

    const/high16 v2, 0x40400000    # 3.0f

    invoke-direct {v1, v2}, Landroid/view/animation/DecelerateInterpolator;-><init>(F)V

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 77
    iget-object v0, p0, Lco/uk/getmondo/common/ui/AnimatedNumberView;->a:Landroid/animation/ObjectAnimator;

    iget v1, p0, Lco/uk/getmondo/common/ui/AnimatedNumberView;->c:I

    int-to-long v2, v1

    invoke-virtual {v0, v2, v3}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 78
    iget-object v0, p0, Lco/uk/getmondo/common/ui/AnimatedNumberView;->a:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    .line 79
    return-void
.end method

.method public setNumber(I)V
    .locals 4

    .prologue
    .line 43
    if-nez p1, :cond_0

    .line 44
    const v0, 0x7f0a02c1

    invoke-virtual {p0, v0}, Lco/uk/getmondo/common/ui/AnimatedNumberView;->setText(I)V

    .line 48
    :goto_0
    iget v0, p0, Lco/uk/getmondo/common/ui/AnimatedNumberView;->c:I

    if-nez v0, :cond_1

    .line 49
    invoke-direct {p0, p1}, Lco/uk/getmondo/common/ui/AnimatedNumberView;->setNumberAsString(I)V

    .line 59
    :goto_1
    return-void

    .line 46
    :cond_0
    invoke-direct {p0, p1}, Lco/uk/getmondo/common/ui/AnimatedNumberView;->setNumberAsString(I)V

    goto :goto_0

    .line 52
    :cond_1
    iget-object v0, p0, Lco/uk/getmondo/common/ui/AnimatedNumberView;->a:Landroid/animation/ObjectAnimator;

    if-eqz v0, :cond_2

    .line 53
    iget-object v0, p0, Lco/uk/getmondo/common/ui/AnimatedNumberView;->a:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->cancel()V

    .line 55
    :cond_2
    const-string v0, "numberInterpolator"

    const/4 v1, 0x2

    new-array v1, v1, [I

    const/4 v2, 0x0

    iget v3, p0, Lco/uk/getmondo/common/ui/AnimatedNumberView;->b:I

    aput v3, v1, v2

    const/4 v2, 0x1

    aput p1, v1, v2

    invoke-static {p0, v0, v1}, Landroid/animation/ObjectAnimator;->ofInt(Ljava/lang/Object;Ljava/lang/String;[I)Landroid/animation/ObjectAnimator;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/common/ui/AnimatedNumberView;->a:Landroid/animation/ObjectAnimator;

    .line 56
    iget-object v0, p0, Lco/uk/getmondo/common/ui/AnimatedNumberView;->a:Landroid/animation/ObjectAnimator;

    new-instance v1, Landroid/view/animation/DecelerateInterpolator;

    const/high16 v2, 0x40400000    # 3.0f

    invoke-direct {v1, v2}, Landroid/view/animation/DecelerateInterpolator;-><init>(F)V

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 57
    iget-object v0, p0, Lco/uk/getmondo/common/ui/AnimatedNumberView;->a:Landroid/animation/ObjectAnimator;

    iget v1, p0, Lco/uk/getmondo/common/ui/AnimatedNumberView;->c:I

    int-to-long v2, v1

    invoke-virtual {v0, v2, v3}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 58
    iget-object v0, p0, Lco/uk/getmondo/common/ui/AnimatedNumberView;->a:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    goto :goto_1
.end method

.method public setNumberInterpolator(I)V
    .locals 0

    .prologue
    .line 63
    iput p1, p0, Lco/uk/getmondo/common/ui/AnimatedNumberView;->b:I

    .line 64
    invoke-direct {p0, p1}, Lco/uk/getmondo/common/ui/AnimatedNumberView;->setNumberAsString(I)V

    .line 65
    return-void
.end method
