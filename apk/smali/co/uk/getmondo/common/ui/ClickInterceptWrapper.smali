.class public Lco/uk/getmondo/common/ui/ClickInterceptWrapper;
.super Landroid/widget/FrameLayout;
.source "ClickInterceptWrapper.java"


# instance fields
.field private a:Landroid/view/View$OnClickListener;

.field private b:Landroid/view/GestureDetector;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 28
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 12
    new-instance v0, Landroid/view/GestureDetector;

    invoke-virtual {p0}, Lco/uk/getmondo/common/ui/ClickInterceptWrapper;->getContext()Landroid/content/Context;

    move-result-object v1

    new-instance v2, Lco/uk/getmondo/common/ui/ClickInterceptWrapper$1;

    invoke-direct {v2, p0}, Lco/uk/getmondo/common/ui/ClickInterceptWrapper$1;-><init>(Lco/uk/getmondo/common/ui/ClickInterceptWrapper;)V

    invoke-direct {v0, v1, v2}, Landroid/view/GestureDetector;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    iput-object v0, p0, Lco/uk/getmondo/common/ui/ClickInterceptWrapper;->b:Landroid/view/GestureDetector;

    .line 29
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3

    .prologue
    .line 32
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 12
    new-instance v0, Landroid/view/GestureDetector;

    invoke-virtual {p0}, Lco/uk/getmondo/common/ui/ClickInterceptWrapper;->getContext()Landroid/content/Context;

    move-result-object v1

    new-instance v2, Lco/uk/getmondo/common/ui/ClickInterceptWrapper$1;

    invoke-direct {v2, p0}, Lco/uk/getmondo/common/ui/ClickInterceptWrapper$1;-><init>(Lco/uk/getmondo/common/ui/ClickInterceptWrapper;)V

    invoke-direct {v0, v1, v2}, Landroid/view/GestureDetector;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    iput-object v0, p0, Lco/uk/getmondo/common/ui/ClickInterceptWrapper;->b:Landroid/view/GestureDetector;

    .line 33
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3

    .prologue
    .line 36
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 12
    new-instance v0, Landroid/view/GestureDetector;

    invoke-virtual {p0}, Lco/uk/getmondo/common/ui/ClickInterceptWrapper;->getContext()Landroid/content/Context;

    move-result-object v1

    new-instance v2, Lco/uk/getmondo/common/ui/ClickInterceptWrapper$1;

    invoke-direct {v2, p0}, Lco/uk/getmondo/common/ui/ClickInterceptWrapper$1;-><init>(Lco/uk/getmondo/common/ui/ClickInterceptWrapper;)V

    invoke-direct {v0, v1, v2}, Landroid/view/GestureDetector;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    iput-object v0, p0, Lco/uk/getmondo/common/ui/ClickInterceptWrapper;->b:Landroid/view/GestureDetector;

    .line 37
    return-void
.end method

.method static synthetic a(Lco/uk/getmondo/common/ui/ClickInterceptWrapper;)Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 9
    iget-object v0, p0, Lco/uk/getmondo/common/ui/ClickInterceptWrapper;->a:Landroid/view/View$OnClickListener;

    return-object v0
.end method


# virtual methods
.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lco/uk/getmondo/common/ui/ClickInterceptWrapper;->b:Landroid/view/GestureDetector;

    invoke-virtual {v0, p1}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 42
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->onInterceptTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method

.method public setOnClickListener(Landroid/view/View$OnClickListener;)V
    .locals 0

    .prologue
    .line 47
    iput-object p1, p0, Lco/uk/getmondo/common/ui/ClickInterceptWrapper;->a:Landroid/view/View$OnClickListener;

    .line 48
    return-void
.end method
