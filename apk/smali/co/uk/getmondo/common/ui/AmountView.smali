.class public final Lco/uk/getmondo/common/ui/AmountView;
.super Landroid/support/v7/widget/aa;
.source "AmountView.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000L\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0002\u0008\u0003\n\u0002\u0010\u000b\n\u0002\u0008\n\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\t\n\u0002\u0008\t\u0018\u00002\u00020\u0001B%\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\n\u0008\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u0012\u0008\u0008\u0002\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\u0008J\u0010\u0010\u0018\u001a\u00020\u00192\u0006\u0010\u001a\u001a\u00020\u001bH\u0002J\u0010\u0010\u001c\u001a\u00020\u00192\u0006\u0010\u001d\u001a\u00020\u001eH\u0002J0\u0010\u001c\u001a\u00020\u00192\u0006\u0010\u001f\u001a\u00020\n2\u0006\u0010 \u001a\u00020!2\u0006\u0010\"\u001a\u00020!2\u0006\u0010#\u001a\u00020\u00072\u0006\u0010$\u001a\u00020\u0007H\u0002J\u0010\u0010%\u001a\u00020\u00072\u0006\u0010&\u001a\u00020\u0007H\u0002J\u000e\u0010\'\u001a\u00020\u00192\u0006\u0010\u001d\u001a\u00020\u001eJ\u0018\u0010(\u001a\u00020\u00192\u0008\u0008\u0001\u0010)\u001a\u00020\u00072\u0006\u0010\u001d\u001a\u00020\u001eR\u0010\u0010\t\u001a\u0004\u0018\u00010\nX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0012\u0010\u000b\u001a\u00020\u00078\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u000c\u001a\u0004\u0018\u00010\nX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u000eX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0012\u0010\u000f\u001a\u00020\u00078\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0010\u001a\u0004\u0018\u00010\nX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0012\u0010\u0011\u001a\u00020\u00078\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0012\u0010\u0012\u001a\u00020\u00078\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0012\u0010\u0013\u001a\u00020\u00078\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0014\u001a\u00020\u000eX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0015\u001a\u00020\u000eX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0016\u001a\u00020\u000eX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0017\u001a\u00020\u000eX\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006*"
    }
    d2 = {
        "Lco/uk/getmondo/common/ui/AmountView;",
        "Landroid/support/v7/widget/AppCompatTextView;",
        "context",
        "Landroid/content/Context;",
        "attrs",
        "Landroid/util/AttributeSet;",
        "defStyleAttr",
        "",
        "(Landroid/content/Context;Landroid/util/AttributeSet;I)V",
        "currencyFont",
        "",
        "currencyTextSize",
        "fractionalFont",
        "fractionalSemiTransparent",
        "",
        "fractionalTextSize",
        "integerFont",
        "integerTextSize",
        "negativeAmountColor",
        "positiveAmountColor",
        "showCurrency",
        "showFractionalPart",
        "showNegativeIfDebit",
        "showPlusIfCredit",
        "applyAttributes",
        "",
        "a",
        "Landroid/content/res/TypedArray;",
        "bind",
        "amount",
        "Lco/uk/getmondo/model/Amount;",
        "currencySymbol",
        "amountValue",
        "",
        "integerPartAbs",
        "fractionalPartAbs",
        "fractionalDigits",
        "convertToSemiTransparent",
        "originalColor",
        "setAmount",
        "setAmountWithStyle",
        "style",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field private a:Z

.field private b:Z

.field private c:Z

.field private d:Z

.field private e:Z

.field private f:I

.field private g:I

.field private h:I

.field private i:I

.field private j:I

.field private k:Ljava/lang/String;

.field private l:Ljava/lang/String;

.field private m:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 6

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x6

    move-object v0, p0

    move-object v1, p1

    move-object v5, v2

    invoke-direct/range {v0 .. v5}, Lco/uk/getmondo/common/ui/AmountView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/d/b/i;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 6

    const/4 v3, 0x0

    const/4 v4, 0x4

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    invoke-direct/range {v0 .. v5}, Lco/uk/getmondo/common/ui/AmountView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/d/b/i;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 8

    .prologue
    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 26
    invoke-direct {p0, p1, p2, p3}, Landroid/support/v7/widget/aa;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 43
    if-eqz p2, :cond_0

    .line 44
    invoke-virtual {p1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v0

    sget-object v1, Lco/uk/getmondo/c$b;->AmountView:[I

    const v2, 0x7f0c00b1

    invoke-virtual {v0, p2, v1, p3, v2}, Landroid/content/res/Resources$Theme;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v1

    .line 45
    nop

    .line 46
    :try_start_0
    const-string v0, "attributes"

    invoke-static {v1, v0}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, v1}, Lco/uk/getmondo/common/ui/AmountView;->a(Landroid/content/res/TypedArray;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 48
    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    .line 50
    nop

    nop

    .line 51
    :cond_0
    invoke-virtual {p0}, Lco/uk/getmondo/common/ui/AmountView;->isInEditMode()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 52
    const-string v1, "\u00a3"

    const-wide/16 v2, 0x2f12

    const-wide/16 v4, 0x78

    const/16 v6, 0x32

    const/4 v7, 0x2

    move-object v0, p0

    invoke-direct/range {v0 .. v7}, Lco/uk/getmondo/common/ui/AmountView;->a(Ljava/lang/String;JJII)V

    :cond_1
    return-void

    .line 48
    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    throw v0
.end method

.method public synthetic constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/d/b/i;)V
    .locals 2

    .prologue
    and-int/lit8 v0, p4, 0x2

    if-eqz v0, :cond_1

    .line 24
    const/4 v0, 0x0

    check-cast v0, Landroid/util/AttributeSet;

    :goto_0
    and-int/lit8 v1, p4, 0x4

    if-eqz v1, :cond_0

    .line 25
    const/4 p3, 0x0

    :cond_0
    invoke-direct {p0, p1, v0, p3}, Lco/uk/getmondo/common/ui/AmountView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void

    :cond_1
    move-object v0, p2

    goto :goto_0
.end method

.method private final a(I)I
    .locals 4

    .prologue
    .line 160
    shr-int/lit8 v0, p1, 0x10

    and-int/lit16 v0, v0, 0xff

    .line 161
    shr-int/lit8 v1, p1, 0x8

    and-int/lit16 v1, v1, 0xff

    .line 162
    and-int/lit16 v2, p1, 0xff

    .line 164
    const/16 v3, 0x66

    invoke-static {v3, v0, v1, v2}, Landroid/graphics/Color;->argb(IIII)I

    move-result v0

    return v0
.end method

.method private final a(Landroid/content/res/TypedArray;)V
    .locals 8

    .prologue
    const/16 v7, 0xc

    const/16 v6, 0xb

    const/16 v5, 0xa

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 80
    const/16 v0, 0x9

    invoke-virtual {p1, v0, v4}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v0

    iput-boolean v0, p0, Lco/uk/getmondo/common/ui/AmountView;->a:Z

    .line 81
    const/4 v0, 0x7

    invoke-virtual {p1, v0, v3}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v0

    iput-boolean v0, p0, Lco/uk/getmondo/common/ui/AmountView;->b:Z

    .line 82
    const/16 v0, 0x8

    invoke-virtual {p1, v0, v3}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v0

    iput-boolean v0, p0, Lco/uk/getmondo/common/ui/AmountView;->c:Z

    .line 83
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v4}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v0

    iput-boolean v0, p0, Lco/uk/getmondo/common/ui/AmountView;->d:Z

    .line 84
    const/4 v0, 0x3

    invoke-virtual {p0}, Lco/uk/getmondo/common/ui/AmountView;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0f00ea

    invoke-static {v1, v2}, Landroid/support/v4/content/a;->c(Landroid/content/Context;I)I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v0

    iput v0, p0, Lco/uk/getmondo/common/ui/AmountView;->f:I

    .line 85
    const/4 v0, 0x4

    iget v1, p0, Lco/uk/getmondo/common/ui/AmountView;->f:I

    invoke-virtual {p1, v0, v1}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v0

    iput v0, p0, Lco/uk/getmondo/common/ui/AmountView;->g:I

    .line 86
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v3}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v0

    iput-boolean v0, p0, Lco/uk/getmondo/common/ui/AmountView;->e:Z

    .line 87
    invoke-virtual {p0}, Lco/uk/getmondo/common/ui/AmountView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b009a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    invoke-virtual {p1, v3, v0}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v0

    iput v0, p0, Lco/uk/getmondo/common/ui/AmountView;->h:I

    .line 88
    invoke-virtual {p0}, Lco/uk/getmondo/common/ui/AmountView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0067

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    invoke-virtual {p1, v4, v0}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v0

    iput v0, p0, Lco/uk/getmondo/common/ui/AmountView;->i:I

    .line 89
    const/4 v0, 0x2

    invoke-virtual {p0}, Lco/uk/getmondo/common/ui/AmountView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0063

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v0

    iput v0, p0, Lco/uk/getmondo/common/ui/AmountView;->j:I

    .line 90
    invoke-virtual {p1, v7}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1, v7}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    iput-object v0, p0, Lco/uk/getmondo/common/ui/AmountView;->k:Ljava/lang/String;

    .line 91
    invoke-virtual {p1, v5}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p1, v5}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_1
    iput-object v0, p0, Lco/uk/getmondo/common/ui/AmountView;->l:Ljava/lang/String;

    .line 92
    invoke-virtual {p1, v6}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p1, v6}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_2
    iput-object v0, p0, Lco/uk/getmondo/common/ui/AmountView;->m:Ljava/lang/String;

    .line 93
    return-void

    .line 90
    :cond_0
    const-string v0, "sans-serif-light"

    goto :goto_0

    .line 91
    :cond_1
    const-string v0, "sans-serif-light"

    goto :goto_1

    .line 92
    :cond_2
    const-string v0, "sans-serif"

    goto :goto_2
.end method

.method private final a(Lco/uk/getmondo/d/c;)V
    .locals 8

    .prologue
    .line 72
    invoke-virtual {p1}, Lco/uk/getmondo/d/c;->g()Ljava/lang/String;

    move-result-object v1

    .line 73
    invoke-virtual {p1}, Lco/uk/getmondo/d/c;->k()J

    move-result-wide v2

    .line 74
    invoke-virtual {p1}, Lco/uk/getmondo/d/c;->c()J

    move-result-wide v4

    .line 75
    invoke-virtual {p1}, Lco/uk/getmondo/d/c;->d()I

    move-result v6

    .line 76
    invoke-virtual {p1}, Lco/uk/getmondo/d/c;->l()Lco/uk/getmondo/common/i/c;

    move-result-object v0

    invoke-virtual {v0}, Lco/uk/getmondo/common/i/c;->a()I

    move-result v7

    move-object v0, p0

    .line 72
    invoke-direct/range {v0 .. v7}, Lco/uk/getmondo/common/ui/AmountView;->a(Ljava/lang/String;JJII)V

    .line 77
    return-void
.end method

.method private final a(Ljava/lang/String;JJII)V
    .locals 14

    .prologue
    .line 98
    .line 99
    const/4 v2, 0x0

    check-cast v2, Ljava/lang/CharSequence;

    invoke-virtual {p0, v2}, Lco/uk/getmondo/common/ui/AmountView;->setText(Ljava/lang/CharSequence;)V

    .line 101
    iget-boolean v2, p0, Lco/uk/getmondo/common/ui/AmountView;->b:Z

    if-eqz v2, :cond_5

    const-wide/16 v2, 0x0

    cmp-long v2, p2, v2

    if-lez v2, :cond_5

    const-string v3, "+"

    .line 107
    :goto_0
    iget-boolean v2, p0, Lco/uk/getmondo/common/ui/AmountView;->d:Z

    if-nez v2, :cond_f

    .line 108
    const-string v4, ""

    .line 111
    :goto_1
    sget-object v2, Lco/uk/getmondo/common/i/b;->a:Lco/uk/getmondo/common/i/b$a;

    move-wide/from16 v0, p4

    invoke-virtual {v2, v0, v1}, Lco/uk/getmondo/common/i/b$a;->a(J)Ljava/lang/String;

    move-result-object v5

    .line 112
    iget-boolean v2, p0, Lco/uk/getmondo/common/ui/AmountView;->a:Z

    if-nez v2, :cond_7

    .line 113
    const-string v6, ""

    .line 117
    :goto_2
    new-instance v7, Landroid/text/SpannableString;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    check-cast v2, Ljava/lang/CharSequence;

    invoke-direct {v7, v2}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 119
    const/16 v10, 0x21

    .line 120
    const-wide/16 v8, 0x0

    cmp-long v2, p2, v8

    if-gez v2, :cond_8

    iget v2, p0, Lco/uk/getmondo/common/ui/AmountView;->g:I

    move v8, v2

    :goto_3
    move-object v2, v3

    .line 121
    check-cast v2, Ljava/lang/CharSequence;

    invoke-interface {v2}, Ljava/lang/CharSequence;->length()I

    move-result v2

    if-lez v2, :cond_9

    const/4 v2, 0x1

    :goto_4
    if-eqz v2, :cond_0

    .line 122
    const/4 v11, 0x0

    .line 123
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v12

    .line 124
    iget-boolean v2, p0, Lco/uk/getmondo/common/ui/AmountView;->d:Z

    if-eqz v2, :cond_a

    iget v2, p0, Lco/uk/getmondo/common/ui/AmountView;->h:I

    .line 125
    :goto_5
    iget-boolean v9, p0, Lco/uk/getmondo/common/ui/AmountView;->d:Z

    if-eqz v9, :cond_b

    iget-object v9, p0, Lco/uk/getmondo/common/ui/AmountView;->k:Ljava/lang/String;

    .line 126
    :goto_6
    new-instance v13, Landroid/text/style/ForegroundColorSpan;

    invoke-direct {v13, v8}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    invoke-virtual {v7, v13, v11, v12, v10}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 127
    new-instance v13, Landroid/text/style/AbsoluteSizeSpan;

    invoke-direct {v13, v2}, Landroid/text/style/AbsoluteSizeSpan;-><init>(I)V

    invoke-virtual {v7, v13, v11, v12, v10}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 128
    new-instance v2, Lco/uk/getmondo/common/ui/k;

    const/4 v13, 0x0

    invoke-static {v9, v13}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v9

    const-string v13, "Typeface.create(prefixFont, Typeface.NORMAL)"

    invoke-static {v9, v13}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v2, v9}, Lco/uk/getmondo/common/ui/k;-><init>(Landroid/graphics/Typeface;)V

    invoke-virtual {v7, v2, v11, v12, v10}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    :cond_0
    move-object v2, v4

    .line 131
    check-cast v2, Ljava/lang/CharSequence;

    invoke-interface {v2}, Ljava/lang/CharSequence;->length()I

    move-result v2

    if-lez v2, :cond_c

    const/4 v2, 0x1

    :goto_7
    if-eqz v2, :cond_1

    .line 132
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v2

    .line 133
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v9

    add-int/2addr v9, v2

    .line 134
    new-instance v11, Landroid/text/style/ForegroundColorSpan;

    invoke-direct {v11, v8}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    invoke-virtual {v7, v11, v2, v9, v10}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 135
    new-instance v11, Landroid/text/style/AbsoluteSizeSpan;

    iget v12, p0, Lco/uk/getmondo/common/ui/AmountView;->h:I

    invoke-direct {v11, v12}, Landroid/text/style/AbsoluteSizeSpan;-><init>(I)V

    invoke-virtual {v7, v11, v2, v9, v10}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 136
    new-instance v11, Lco/uk/getmondo/common/ui/k;

    iget-object v12, p0, Lco/uk/getmondo/common/ui/AmountView;->k:Ljava/lang/String;

    const/4 v13, 0x0

    invoke-static {v12, v13}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v12

    const-string v13, "Typeface.create(currencyFont, Typeface.NORMAL)"

    invoke-static {v12, v13}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v11, v12}, Lco/uk/getmondo/common/ui/k;-><init>(Landroid/graphics/Typeface;)V

    invoke-virtual {v7, v11, v2, v9, v10}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    :cond_1
    move-object v2, v5

    .line 139
    check-cast v2, Ljava/lang/CharSequence;

    invoke-interface {v2}, Ljava/lang/CharSequence;->length()I

    move-result v2

    if-lez v2, :cond_d

    const/4 v2, 0x1

    :goto_8
    if-eqz v2, :cond_2

    .line 140
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v2

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v9

    add-int/2addr v2, v9

    .line 141
    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v9

    add-int/2addr v9, v2

    .line 142
    new-instance v11, Landroid/text/style/ForegroundColorSpan;

    invoke-direct {v11, v8}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    invoke-virtual {v7, v11, v2, v9, v10}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 143
    new-instance v11, Landroid/text/style/AbsoluteSizeSpan;

    iget v12, p0, Lco/uk/getmondo/common/ui/AmountView;->i:I

    invoke-direct {v11, v12}, Landroid/text/style/AbsoluteSizeSpan;-><init>(I)V

    invoke-virtual {v7, v11, v2, v9, v10}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 144
    new-instance v11, Lco/uk/getmondo/common/ui/k;

    iget-object v12, p0, Lco/uk/getmondo/common/ui/AmountView;->l:Ljava/lang/String;

    const/4 v13, 0x0

    invoke-static {v12, v13}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v12

    const-string v13, "Typeface.create(integerFont, Typeface.NORMAL)"

    invoke-static {v12, v13}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v11, v12}, Lco/uk/getmondo/common/ui/k;-><init>(Landroid/graphics/Typeface;)V

    invoke-virtual {v7, v11, v2, v9, v10}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    :cond_2
    move-object v2, v6

    .line 147
    check-cast v2, Ljava/lang/CharSequence;

    invoke-interface {v2}, Ljava/lang/CharSequence;->length()I

    move-result v2

    if-lez v2, :cond_e

    const/4 v2, 0x1

    :goto_9
    if-eqz v2, :cond_4

    .line 148
    iget-boolean v2, p0, Lco/uk/getmondo/common/ui/AmountView;->e:Z

    if-eqz v2, :cond_3

    invoke-direct {p0, v8}, Lco/uk/getmondo/common/ui/AmountView;->a(I)I

    move-result v8

    .line 149
    :cond_3
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v2

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v3

    add-int/2addr v2, v3

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v3

    add-int/2addr v2, v3

    .line 150
    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v3

    add-int/2addr v3, v2

    .line 151
    new-instance v4, Landroid/text/style/ForegroundColorSpan;

    invoke-direct {v4, v8}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    invoke-virtual {v7, v4, v2, v3, v10}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 152
    new-instance v4, Landroid/text/style/AbsoluteSizeSpan;

    iget v5, p0, Lco/uk/getmondo/common/ui/AmountView;->j:I

    invoke-direct {v4, v5}, Landroid/text/style/AbsoluteSizeSpan;-><init>(I)V

    invoke-virtual {v7, v4, v2, v3, v10}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 153
    new-instance v4, Lco/uk/getmondo/common/ui/k;

    iget-object v5, p0, Lco/uk/getmondo/common/ui/AmountView;->m:Ljava/lang/String;

    const/4 v6, 0x0

    invoke-static {v5, v6}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v5

    const-string v6, "Typeface.create(fractionalFont, Typeface.NORMAL)"

    invoke-static {v5, v6}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v4, v5}, Lco/uk/getmondo/common/ui/k;-><init>(Landroid/graphics/Typeface;)V

    invoke-virtual {v7, v4, v2, v3, v10}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    :cond_4
    move-object v2, v7

    .line 156
    check-cast v2, Ljava/lang/CharSequence;

    invoke-virtual {p0, v2}, Lco/uk/getmondo/common/ui/AmountView;->setText(Ljava/lang/CharSequence;)V

    .line 157
    return-void

    .line 103
    :cond_5
    iget-boolean v2, p0, Lco/uk/getmondo/common/ui/AmountView;->c:Z

    if-eqz v2, :cond_6

    const-wide/16 v2, 0x0

    cmp-long v2, p2, v2

    if-gez v2, :cond_6

    const-string v3, "-"

    goto/16 :goto_0

    .line 104
    :cond_6
    const-string v3, ""

    goto/16 :goto_0

    .line 115
    :cond_7
    invoke-static/range {p6 .. p7}, Lco/uk/getmondo/common/i/d;->a(II)Ljava/lang/String;

    move-result-object v6

    goto/16 :goto_2

    .line 120
    :cond_8
    iget v2, p0, Lco/uk/getmondo/common/ui/AmountView;->f:I

    move v8, v2

    goto/16 :goto_3

    .line 121
    :cond_9
    const/4 v2, 0x0

    goto/16 :goto_4

    .line 124
    :cond_a
    iget v2, p0, Lco/uk/getmondo/common/ui/AmountView;->i:I

    goto/16 :goto_5

    .line 125
    :cond_b
    iget-object v9, p0, Lco/uk/getmondo/common/ui/AmountView;->l:Ljava/lang/String;

    goto/16 :goto_6

    .line 131
    :cond_c
    const/4 v2, 0x0

    goto/16 :goto_7

    .line 139
    :cond_d
    const/4 v2, 0x0

    goto/16 :goto_8

    .line 147
    :cond_e
    const/4 v2, 0x0

    goto :goto_9

    :cond_f
    move-object v4, p1

    goto/16 :goto_1
.end method


# virtual methods
.method public final a(ILco/uk/getmondo/d/c;)V
    .locals 2

    .prologue
    const-string v0, "amount"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 58
    invoke-virtual {p0}, Lco/uk/getmondo/common/ui/AmountView;->getContext()Landroid/content/Context;

    move-result-object v0

    sget-object v1, Lco/uk/getmondo/c$b;->AmountView:[I

    invoke-virtual {v0, p1, v1}, Landroid/content/Context;->obtainStyledAttributes(I[I)Landroid/content/res/TypedArray;

    move-result-object v1

    .line 59
    nop

    .line 60
    :try_start_0
    const-string v0, "attributes"

    invoke-static {v1, v0}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, v1}, Lco/uk/getmondo/common/ui/AmountView;->a(Landroid/content/res/TypedArray;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 62
    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    .line 63
    invoke-direct {p0, p2}, Lco/uk/getmondo/common/ui/AmountView;->a(Lco/uk/getmondo/d/c;)V

    .line 65
    return-void

    .line 62
    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    throw v0
.end method

.method public final setAmount(Lco/uk/getmondo/d/c;)V
    .locals 1

    .prologue
    const-string v0, "amount"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 68
    invoke-direct {p0, p1}, Lco/uk/getmondo/common/ui/AmountView;->a(Lco/uk/getmondo/d/c;)V

    .line 69
    return-void
.end method
