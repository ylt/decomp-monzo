.class public final Lco/uk/getmondo/common/ui/d;
.super Landroid/support/v7/widget/RecyclerView$g;
.source "GridSpacingItemDecoration.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u00006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0017\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J(\u0010\u0007\u001a\u00020\u00082\u0006\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\u000c2\u0006\u0010\r\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u0010H\u0016R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0011"
    }
    d2 = {
        "Lco/uk/getmondo/common/ui/GridSpacingItemDecoration;",
        "Landroid/support/v7/widget/RecyclerView$ItemDecoration;",
        "spacing",
        "",
        "includeEdge",
        "",
        "(IZ)V",
        "getItemOffsets",
        "",
        "outRect",
        "Landroid/graphics/Rect;",
        "view",
        "Landroid/view/View;",
        "parent",
        "Landroid/support/v7/widget/RecyclerView;",
        "state",
        "Landroid/support/v7/widget/RecyclerView$State;",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field private final a:I

.field private final b:Z


# direct methods
.method public constructor <init>(IZ)V
    .locals 0

    .prologue
    .line 10
    .line 13
    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView$g;-><init>()V

    iput p1, p0, Lco/uk/getmondo/common/ui/d;->a:I

    iput-boolean p2, p0, Lco/uk/getmondo/common/ui/d;->b:Z

    return-void
.end method

.method public synthetic constructor <init>(IZILkotlin/d/b/i;)V
    .locals 1

    .prologue
    and-int/lit8 v0, p3, 0x2

    if-eqz v0, :cond_0

    .line 12
    const/4 p2, 0x0

    :cond_0
    invoke-direct {p0, p1, p2}, Lco/uk/getmondo/common/ui/d;-><init>(IZ)V

    return-void
.end method


# virtual methods
.method public getItemOffsets(Landroid/graphics/Rect;Landroid/view/View;Landroid/support/v7/widget/RecyclerView;Landroid/support/v7/widget/RecyclerView$t;)V
    .locals 5

    .prologue
    const/4 v3, 0x0

    const-string v0, "outRect"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "view"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "parent"

    invoke-static {p3, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "state"

    invoke-static {p4, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 16
    invoke-virtual {p3, p2}, Landroid/support/v7/widget/RecyclerView;->f(Landroid/view/View;)I

    move-result v1

    .line 17
    invoke-virtual {p3}, Landroid/support/v7/widget/RecyclerView;->getLayoutManager()Landroid/support/v7/widget/RecyclerView$h;

    move-result-object v0

    if-nez v0, :cond_0

    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type android.support.v7.widget.GridLayoutManager"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    check-cast v0, Landroid/support/v7/widget/GridLayoutManager;

    .line 18
    invoke-virtual {v0}, Landroid/support/v7/widget/GridLayoutManager;->c()I

    move-result v2

    .line 19
    invoke-virtual {v0}, Landroid/support/v7/widget/GridLayoutManager;->b()Landroid/support/v7/widget/GridLayoutManager$c;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/GridLayoutManager$c;->a(I)I

    move-result v0

    .line 21
    if-ltz v1, :cond_5

    .line 22
    rem-int v3, v1, v2

    .line 24
    iget-boolean v4, p0, Lco/uk/getmondo/common/ui/d;->b:Z

    if-eqz v4, :cond_3

    .line 25
    iget v0, p0, Lco/uk/getmondo/common/ui/d;->a:I

    iget v4, p0, Lco/uk/getmondo/common/ui/d;->a:I

    mul-int/2addr v4, v3

    div-int/2addr v4, v2

    sub-int/2addr v0, v4

    iput v0, p1, Landroid/graphics/Rect;->left:I

    .line 26
    add-int/lit8 v0, v3, 0x1

    iget v3, p0, Lco/uk/getmondo/common/ui/d;->a:I

    mul-int/2addr v0, v3

    div-int/2addr v0, v2

    iput v0, p1, Landroid/graphics/Rect;->right:I

    .line 28
    if-ge v1, v2, :cond_1

    .line 29
    iget v0, p0, Lco/uk/getmondo/common/ui/d;->a:I

    iput v0, p1, Landroid/graphics/Rect;->top:I

    .line 31
    :cond_1
    iget v0, p0, Lco/uk/getmondo/common/ui/d;->a:I

    iput v0, p1, Landroid/graphics/Rect;->bottom:I

    .line 46
    :cond_2
    :goto_0
    return-void

    .line 33
    :cond_3
    iget v4, p0, Lco/uk/getmondo/common/ui/d;->a:I

    mul-int/2addr v4, v3

    div-int/2addr v4, v2

    iput v4, p1, Landroid/graphics/Rect;->left:I

    .line 34
    if-ge v0, v2, :cond_4

    .line 35
    iget v0, p0, Lco/uk/getmondo/common/ui/d;->a:I

    add-int/lit8 v3, v3, 0x1

    iget v4, p0, Lco/uk/getmondo/common/ui/d;->a:I

    mul-int/2addr v3, v4

    div-int/2addr v3, v2

    sub-int/2addr v0, v3

    iput v0, p1, Landroid/graphics/Rect;->right:I

    .line 37
    :cond_4
    if-lt v1, v2, :cond_2

    .line 38
    iget v0, p0, Lco/uk/getmondo/common/ui/d;->a:I

    iput v0, p1, Landroid/graphics/Rect;->top:I

    goto :goto_0

    .line 42
    :cond_5
    iput v3, p1, Landroid/graphics/Rect;->left:I

    .line 43
    iput v3, p1, Landroid/graphics/Rect;->right:I

    .line 44
    iput v3, p1, Landroid/graphics/Rect;->top:I

    .line 45
    iput v3, p1, Landroid/graphics/Rect;->bottom:I

    goto :goto_0
.end method
