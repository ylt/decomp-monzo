.class public Lco/uk/getmondo/common/ui/c;
.super Lcom/bumptech/glide/g/b/b;
.source "CircularViewTarget.java"


# direct methods
.method public constructor <init>(Landroid/widget/ImageView;)V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0, p1}, Lcom/bumptech/glide/g/b/b;-><init>(Landroid/widget/ImageView;)V

    .line 15
    return-void
.end method


# virtual methods
.method protected a(Landroid/graphics/Bitmap;)V
    .locals 2

    .prologue
    .line 19
    iget-object v0, p0, Lco/uk/getmondo/common/ui/c;->c:Landroid/view/View;

    check-cast v0, Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-static {v0, p1}, Landroid/support/v4/a/a/h;->a(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)Landroid/support/v4/a/a/f;

    move-result-object v1

    .line 20
    const/4 v0, 0x1

    invoke-virtual {v1, v0}, Landroid/support/v4/a/a/f;->a(Z)V

    .line 21
    iget-object v0, p0, Lco/uk/getmondo/common/ui/c;->c:Landroid/view/View;

    check-cast v0, Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 22
    return-void
.end method

.method protected bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 11
    check-cast p1, Landroid/graphics/Bitmap;

    invoke-virtual {p0, p1}, Lco/uk/getmondo/common/ui/c;->a(Landroid/graphics/Bitmap;)V

    return-void
.end method
