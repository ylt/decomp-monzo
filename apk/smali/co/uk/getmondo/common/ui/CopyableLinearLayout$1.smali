.class final Lco/uk/getmondo/common/ui/CopyableLinearLayout$1;
.super Ljava/lang/Object;
.source "CopyableLinearLayout.kt"

# interfaces
.implements Landroid/view/View$OnLongClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lco/uk/getmondo/common/ui/CopyableLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "",
        "it",
        "Landroid/view/View;",
        "kotlin.jvm.PlatformType",
        "onLongClick"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field final synthetic a:Lco/uk/getmondo/common/ui/CopyableLinearLayout;

.field final synthetic b:Lkotlin/d/b/x$c;

.field final synthetic c:Landroid/content/Context;


# direct methods
.method constructor <init>(Lco/uk/getmondo/common/ui/CopyableLinearLayout;Lkotlin/d/b/x$c;Landroid/content/Context;)V
    .locals 0

    iput-object p1, p0, Lco/uk/getmondo/common/ui/CopyableLinearLayout$1;->a:Lco/uk/getmondo/common/ui/CopyableLinearLayout;

    iput-object p2, p0, Lco/uk/getmondo/common/ui/CopyableLinearLayout$1;->b:Lkotlin/d/b/x$c;

    iput-object p3, p0, Lco/uk/getmondo/common/ui/CopyableLinearLayout$1;->c:Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onLongClick(Landroid/view/View;)Z
    .locals 11

    .prologue
    const/16 v7, 0x1e

    const/4 v9, 0x1

    const/4 v4, 0x0

    const/4 v2, 0x0

    .line 30
    iget-object v0, p0, Lco/uk/getmondo/common/ui/CopyableLinearLayout$1;->a:Lco/uk/getmondo/common/ui/CopyableLinearLayout;

    invoke-static {v0}, Lco/uk/getmondo/common/ui/m;->a(Landroid/view/ViewGroup;)Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 67
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    check-cast v1, Ljava/util/Collection;

    .line 68
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_0
    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    move-object v0, v3

    check-cast v0, Landroid/view/View;

    .line 30
    instance-of v6, v0, Landroid/widget/TextView;

    if-eqz v6, :cond_2

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    if-nez v0, :cond_1

    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type co.uk.getmondo.common.ui.CopyableLinearLayout.LayoutParams"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    check-cast v0, Lco/uk/getmondo/common/ui/CopyableLinearLayout$a;

    invoke-virtual {v0}, Lco/uk/getmondo/common/ui/CopyableLinearLayout$a;->a()I

    move-result v0

    iget-object v6, p0, Lco/uk/getmondo/common/ui/CopyableLinearLayout$1;->a:Lco/uk/getmondo/common/ui/CopyableLinearLayout;

    invoke-static {v6}, Lco/uk/getmondo/common/ui/CopyableLinearLayout;->a(Lco/uk/getmondo/common/ui/CopyableLinearLayout;)I

    move-result v6

    if-ne v0, v6, :cond_2

    move v0, v9

    :goto_1
    if-eqz v0, :cond_0

    invoke-interface {v1, v3}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_2
    move v0, v4

    goto :goto_1

    .line 69
    :cond_3
    check-cast v1, Ljava/util/List;

    move-object v0, v1

    check-cast v0, Ljava/lang/Iterable;

    .line 30
    iget-object v1, p0, Lco/uk/getmondo/common/ui/CopyableLinearLayout$1;->b:Lkotlin/d/b/x$c;

    iget-object v1, v1, Lkotlin/d/b/x$c;->a:Ljava/lang/Object;

    check-cast v1, Ljava/lang/String;

    const-string v3, "separator"

    invoke-static {v1, v3}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v1, Ljava/lang/CharSequence;

    sget-object v6, Lco/uk/getmondo/common/ui/CopyableLinearLayout$1$b;->a:Lco/uk/getmondo/common/ui/CopyableLinearLayout$1$b;

    check-cast v6, Lkotlin/d/a/b;

    move-object v3, v2

    move-object v5, v2

    move-object v8, v2

    invoke-static/range {v0 .. v8}, Lkotlin/a/m;->a(Ljava/lang/Iterable;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;ILjava/lang/CharSequence;Lkotlin/d/a/b;ILjava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    .line 31
    iget-object v0, p0, Lco/uk/getmondo/common/ui/CopyableLinearLayout$1;->a:Lco/uk/getmondo/common/ui/CopyableLinearLayout;

    invoke-static {v0}, Lco/uk/getmondo/common/ui/m;->a(Landroid/view/ViewGroup;)Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 70
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    check-cast v1, Ljava/util/Collection;

    .line 71
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_4
    :goto_2
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    move-object v0, v3

    check-cast v0, Landroid/view/View;

    .line 31
    instance-of v6, v0, Landroid/widget/TextView;

    if-eqz v6, :cond_6

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    if-nez v0, :cond_5

    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type co.uk.getmondo.common.ui.CopyableLinearLayout.LayoutParams"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_5
    check-cast v0, Lco/uk/getmondo/common/ui/CopyableLinearLayout$a;

    invoke-virtual {v0}, Lco/uk/getmondo/common/ui/CopyableLinearLayout$a;->a()I

    move-result v0

    iget-object v6, p0, Lco/uk/getmondo/common/ui/CopyableLinearLayout$1;->a:Lco/uk/getmondo/common/ui/CopyableLinearLayout;

    invoke-static {v6}, Lco/uk/getmondo/common/ui/CopyableLinearLayout;->b(Lco/uk/getmondo/common/ui/CopyableLinearLayout;)I

    move-result v6

    if-ne v0, v6, :cond_6

    move v0, v9

    :goto_3
    if-eqz v0, :cond_4

    invoke-interface {v1, v3}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_6
    move v0, v4

    goto :goto_3

    .line 72
    :cond_7
    check-cast v1, Ljava/util/List;

    move-object v0, v1

    check-cast v0, Ljava/lang/Iterable;

    .line 31
    iget-object v1, p0, Lco/uk/getmondo/common/ui/CopyableLinearLayout$1;->b:Lkotlin/d/b/x$c;

    iget-object v1, v1, Lkotlin/d/b/x$c;->a:Ljava/lang/Object;

    check-cast v1, Ljava/lang/String;

    const-string v3, "separator"

    invoke-static {v1, v3}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v1, Ljava/lang/CharSequence;

    sget-object v6, Lco/uk/getmondo/common/ui/CopyableLinearLayout$1$a;->a:Lco/uk/getmondo/common/ui/CopyableLinearLayout$1$a;

    check-cast v6, Lkotlin/d/a/b;

    move-object v3, v2

    move-object v5, v2

    move-object v8, v2

    invoke-static/range {v0 .. v8}, Lkotlin/a/m;->a(Ljava/lang/Iterable;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;ILjava/lang/CharSequence;Lkotlin/d/a/b;ILjava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 32
    iget-object v1, p0, Lco/uk/getmondo/common/ui/CopyableLinearLayout$1;->c:Landroid/content/Context;

    invoke-static {v1, v10, v0}, Lco/uk/getmondo/common/e;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 33
    return v9
.end method
