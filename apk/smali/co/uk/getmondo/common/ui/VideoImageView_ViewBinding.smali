.class public Lco/uk/getmondo/common/ui/VideoImageView_ViewBinding;
.super Ljava/lang/Object;
.source "VideoImageView_ViewBinding.java"

# interfaces
.implements Lbutterknife/Unbinder;


# instance fields
.field private a:Lco/uk/getmondo/common/ui/VideoImageView;


# direct methods
.method public constructor <init>(Lco/uk/getmondo/common/ui/VideoImageView;Landroid/view/View;)V
    .locals 3

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    iput-object p1, p0, Lco/uk/getmondo/common/ui/VideoImageView_ViewBinding;->a:Lco/uk/getmondo/common/ui/VideoImageView;

    .line 27
    const v0, 0x7f11048f

    const-string v1, "field \'exoPlayerView\'"

    const-class v2, Lcom/google/android/exoplayer2/ui/SimpleExoPlayerView;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/exoplayer2/ui/SimpleExoPlayerView;

    iput-object v0, p1, Lco/uk/getmondo/common/ui/VideoImageView;->exoPlayerView:Lcom/google/android/exoplayer2/ui/SimpleExoPlayerView;

    .line 28
    const v0, 0x7f110490

    const-string v1, "field \'placeholderImageView\'"

    const-class v2, Landroid/widget/ImageView;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p1, Lco/uk/getmondo/common/ui/VideoImageView;->placeholderImageView:Landroid/widget/ImageView;

    .line 29
    return-void
.end method


# virtual methods
.method public unbind()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 34
    iget-object v0, p0, Lco/uk/getmondo/common/ui/VideoImageView_ViewBinding;->a:Lco/uk/getmondo/common/ui/VideoImageView;

    .line 35
    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Bindings already cleared."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 36
    :cond_0
    iput-object v1, p0, Lco/uk/getmondo/common/ui/VideoImageView_ViewBinding;->a:Lco/uk/getmondo/common/ui/VideoImageView;

    .line 38
    iput-object v1, v0, Lco/uk/getmondo/common/ui/VideoImageView;->exoPlayerView:Lcom/google/android/exoplayer2/ui/SimpleExoPlayerView;

    .line 39
    iput-object v1, v0, Lco/uk/getmondo/common/ui/VideoImageView;->placeholderImageView:Landroid/widget/ImageView;

    .line 40
    return-void
.end method
