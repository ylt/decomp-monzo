.class public Lco/uk/getmondo/common/ui/AmountInputView;
.super Landroid/widget/LinearLayout;
.source "AmountInputView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lco/uk/getmondo/common/ui/AmountInputView$a;
    }
.end annotation


# instance fields
.field private final a:Lco/uk/getmondo/common/i/b;

.field private b:Lco/uk/getmondo/common/i/c;

.field private c:I

.field private d:Lco/uk/getmondo/d/c;

.field editText:Landroid/widget/EditText;
    .annotation build Lbutterknife/BindView;
        value = 0x7f1104a2
    .end annotation
.end field

.field textInputLayout:Landroid/support/design/widget/TextInputLayout;
    .annotation build Lbutterknife/BindView;
        value = 0x7f1104a1
    .end annotation
.end field

.field titleTextView:Landroid/widget/TextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f1104a0
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 56
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 45
    new-instance v0, Lco/uk/getmondo/common/i/b;

    const/4 v1, 0x1

    invoke-direct {v0, v2, v2, v1}, Lco/uk/getmondo/common/i/b;-><init>(ZZZ)V

    iput-object v0, p0, Lco/uk/getmondo/common/ui/AmountInputView;->a:Lco/uk/getmondo/common/i/b;

    .line 51
    sget-object v0, Lco/uk/getmondo/common/i/c;->a:Lco/uk/getmondo/common/i/c;

    iput-object v0, p0, Lco/uk/getmondo/common/ui/AmountInputView;->b:Lco/uk/getmondo/common/i/c;

    .line 52
    const/4 v0, 0x6

    iput v0, p0, Lco/uk/getmondo/common/ui/AmountInputView;->c:I

    .line 57
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lco/uk/getmondo/common/ui/AmountInputView;->a(Landroid/util/AttributeSet;)V

    .line 58
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 61
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 45
    new-instance v0, Lco/uk/getmondo/common/i/b;

    const/4 v1, 0x1

    invoke-direct {v0, v2, v2, v1}, Lco/uk/getmondo/common/i/b;-><init>(ZZZ)V

    iput-object v0, p0, Lco/uk/getmondo/common/ui/AmountInputView;->a:Lco/uk/getmondo/common/i/b;

    .line 51
    sget-object v0, Lco/uk/getmondo/common/i/c;->a:Lco/uk/getmondo/common/i/c;

    iput-object v0, p0, Lco/uk/getmondo/common/ui/AmountInputView;->b:Lco/uk/getmondo/common/i/c;

    .line 52
    const/4 v0, 0x6

    iput v0, p0, Lco/uk/getmondo/common/ui/AmountInputView;->c:I

    .line 62
    invoke-direct {p0, p2}, Lco/uk/getmondo/common/ui/AmountInputView;->a(Landroid/util/AttributeSet;)V

    .line 63
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 66
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 45
    new-instance v0, Lco/uk/getmondo/common/i/b;

    const/4 v1, 0x1

    invoke-direct {v0, v2, v2, v1}, Lco/uk/getmondo/common/i/b;-><init>(ZZZ)V

    iput-object v0, p0, Lco/uk/getmondo/common/ui/AmountInputView;->a:Lco/uk/getmondo/common/i/b;

    .line 51
    sget-object v0, Lco/uk/getmondo/common/i/c;->a:Lco/uk/getmondo/common/i/c;

    iput-object v0, p0, Lco/uk/getmondo/common/ui/AmountInputView;->b:Lco/uk/getmondo/common/i/c;

    .line 52
    const/4 v0, 0x6

    iput v0, p0, Lco/uk/getmondo/common/ui/AmountInputView;->c:I

    .line 67
    invoke-direct {p0, p2}, Lco/uk/getmondo/common/ui/AmountInputView;->a(Landroid/util/AttributeSet;)V

    .line 68
    return-void
.end method

.method private a(Landroid/util/AttributeSet;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v0, 0x0

    .line 71
    invoke-virtual {p0, v3}, Lco/uk/getmondo/common/ui/AmountInputView;->setOrientation(I)V

    .line 72
    invoke-virtual {p0}, Lco/uk/getmondo/common/ui/AmountInputView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f05013d

    invoke-virtual {v1, v2, p0, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 73
    invoke-static {p0, v1}, Lbutterknife/ButterKnife;->bind(Ljava/lang/Object;Landroid/view/View;)Lbutterknife/Unbinder;

    .line 76
    if-eqz p1, :cond_1

    .line 77
    invoke-virtual {p0}, Lco/uk/getmondo/common/ui/AmountInputView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v1

    sget-object v2, Lco/uk/getmondo/c$b;->AmountInputView:[I

    invoke-virtual {v1, p1, v2, v0, v0}, Landroid/content/res/Resources$Theme;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v1

    .line 79
    const/4 v0, 0x0

    :try_start_0
    invoke-virtual {v1, v0}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 80
    invoke-static {v0}, Lco/uk/getmondo/common/k/p;->c(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 81
    new-instance v2, Lco/uk/getmondo/common/i/c;

    invoke-direct {v2, v0}, Lco/uk/getmondo/common/i/c;-><init>(Ljava/lang/String;)V

    iput-object v2, p0, Lco/uk/getmondo/common/ui/AmountInputView;->b:Lco/uk/getmondo/common/i/c;

    .line 83
    :cond_0
    const/4 v0, 0x1

    const/4 v2, 0x6

    invoke-virtual {v1, v0, v2}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v0

    iput v0, p0, Lco/uk/getmondo/common/ui/AmountInputView;->c:I

    .line 84
    const/4 v0, 0x2

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 86
    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    .line 90
    :cond_1
    iget-object v1, p0, Lco/uk/getmondo/common/ui/AmountInputView;->textInputLayout:Landroid/support/design/widget/TextInputLayout;

    invoke-virtual {v1, v0}, Landroid/support/design/widget/TextInputLayout;->setErrorEnabled(Z)V

    .line 91
    invoke-direct {p0}, Lco/uk/getmondo/common/ui/AmountInputView;->b()V

    .line 92
    invoke-direct {p0}, Lco/uk/getmondo/common/ui/AmountInputView;->c()V

    .line 93
    invoke-direct {p0}, Lco/uk/getmondo/common/ui/AmountInputView;->d()V

    .line 94
    return-void

    .line 86
    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    throw v0
.end method

.method private b()V
    .locals 6

    .prologue
    .line 129
    iget-object v0, p0, Lco/uk/getmondo/common/ui/AmountInputView;->editText:Landroid/widget/EditText;

    if-nez v0, :cond_0

    .line 133
    :goto_0
    return-void

    .line 132
    :cond_0
    iget-object v0, p0, Lco/uk/getmondo/common/ui/AmountInputView;->editText:Landroid/widget/EditText;

    const/4 v1, 0x1

    new-array v1, v1, [Landroid/text/InputFilter;

    const/4 v2, 0x0

    new-instance v3, Lco/uk/getmondo/common/ui/AmountInputView$a;

    iget v4, p0, Lco/uk/getmondo/common/ui/AmountInputView;->c:I

    iget-object v5, p0, Lco/uk/getmondo/common/ui/AmountInputView;->b:Lco/uk/getmondo/common/i/c;

    invoke-virtual {v5}, Lco/uk/getmondo/common/i/c;->a()I

    move-result v5

    invoke-direct {v3, v4, v5}, Lco/uk/getmondo/common/ui/AmountInputView$a;-><init>(II)V

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setFilters([Landroid/text/InputFilter;)V

    goto :goto_0
.end method

.method private c()V
    .locals 7

    .prologue
    .line 136
    iget-object v0, p0, Lco/uk/getmondo/common/ui/AmountInputView;->titleTextView:Landroid/widget/TextView;

    if-nez v0, :cond_0

    .line 140
    :goto_0
    return-void

    .line 139
    :cond_0
    iget-object v0, p0, Lco/uk/getmondo/common/ui/AmountInputView;->titleTextView:Landroid/widget/TextView;

    invoke-virtual {p0}, Lco/uk/getmondo/common/ui/AmountInputView;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0a00da

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, Lco/uk/getmondo/common/ui/AmountInputView;->b:Lco/uk/getmondo/common/i/c;

    sget-object v6, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {v5, v6}, Lco/uk/getmondo/common/i/c;->a(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method private d()V
    .locals 3

    .prologue
    .line 143
    iget-object v0, p0, Lco/uk/getmondo/common/ui/AmountInputView;->editText:Landroid/widget/EditText;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lco/uk/getmondo/common/ui/AmountInputView;->d:Lco/uk/getmondo/d/c;

    if-nez v0, :cond_1

    .line 148
    :cond_0
    :goto_0
    return-void

    .line 147
    :cond_1
    iget-object v0, p0, Lco/uk/getmondo/common/ui/AmountInputView;->editText:Landroid/widget/EditText;

    iget-object v1, p0, Lco/uk/getmondo/common/ui/AmountInputView;->a:Lco/uk/getmondo/common/i/b;

    iget-object v2, p0, Lco/uk/getmondo/common/ui/AmountInputView;->d:Lco/uk/getmondo/d/c;

    invoke-virtual {v1, v2}, Lco/uk/getmondo/common/i/b;->a(Lco/uk/getmondo/d/c;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method


# virtual methods
.method public a()Lio/reactivex/n;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Ljava/lang/CharSequence;",
            ">;"
        }
    .end annotation

    .prologue
    .line 117
    iget-object v0, p0, Lco/uk/getmondo/common/ui/AmountInputView;->editText:Landroid/widget/EditText;

    invoke-static {v0}, Lcom/b/a/d/e;->c(Landroid/widget/TextView;)Lcom/b/a/a;

    move-result-object v0

    return-object v0
.end method

.method public getAmountText()Landroid/text/Editable;
    .locals 1

    .prologue
    .line 121
    iget-object v0, p0, Lco/uk/getmondo/common/ui/AmountInputView;->editText:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    return-object v0
.end method

.method public setCurrency(Lco/uk/getmondo/common/i/c;)V
    .locals 1

    .prologue
    .line 97
    iget-object v0, p0, Lco/uk/getmondo/common/ui/AmountInputView;->b:Lco/uk/getmondo/common/i/c;

    invoke-virtual {v0, p1}, Lco/uk/getmondo/common/i/c;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 104
    :goto_0
    return-void

    .line 101
    :cond_0
    iput-object p1, p0, Lco/uk/getmondo/common/ui/AmountInputView;->b:Lco/uk/getmondo/common/i/c;

    .line 102
    invoke-direct {p0}, Lco/uk/getmondo/common/ui/AmountInputView;->b()V

    .line 103
    invoke-direct {p0}, Lco/uk/getmondo/common/ui/AmountInputView;->c()V

    goto :goto_0
.end method

.method public setDefaultAmount(Lco/uk/getmondo/d/c;)V
    .locals 2

    .prologue
    .line 107
    iput-object p1, p0, Lco/uk/getmondo/common/ui/AmountInputView;->d:Lco/uk/getmondo/d/c;

    .line 108
    invoke-direct {p0}, Lco/uk/getmondo/common/ui/AmountInputView;->d()V

    .line 110
    iget-object v0, p0, Lco/uk/getmondo/common/ui/AmountInputView;->b:Lco/uk/getmondo/common/i/c;

    invoke-virtual {p1}, Lco/uk/getmondo/d/c;->l()Lco/uk/getmondo/common/i/c;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 111
    invoke-virtual {p1}, Lco/uk/getmondo/d/c;->l()Lco/uk/getmondo/common/i/c;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/common/ui/AmountInputView;->b:Lco/uk/getmondo/common/i/c;

    .line 112
    invoke-direct {p0}, Lco/uk/getmondo/common/ui/AmountInputView;->c()V

    .line 114
    :cond_0
    return-void
.end method

.method public setError(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 125
    iget-object v0, p0, Lco/uk/getmondo/common/ui/AmountInputView;->textInputLayout:Landroid/support/design/widget/TextInputLayout;

    invoke-virtual {v0, p1}, Landroid/support/design/widget/TextInputLayout;->setError(Ljava/lang/CharSequence;)V

    .line 126
    return-void
.end method
