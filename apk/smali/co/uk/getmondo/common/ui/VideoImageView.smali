.class public Lco/uk/getmondo/common/ui/VideoImageView;
.super Landroid/widget/FrameLayout;
.source "VideoImageView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lco/uk/getmondo/common/ui/VideoImageView$a;
    }
.end annotation


# instance fields
.field exoPlayerView:Lcom/google/android/exoplayer2/ui/SimpleExoPlayerView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f11048f
    .end annotation
.end field

.field placeholderImageView:Landroid/widget/ImageView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f110490
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 47
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lco/uk/getmondo/common/ui/VideoImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 48
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 51
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lco/uk/getmondo/common/ui/VideoImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 52
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 55
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 57
    invoke-virtual {p0}, Lco/uk/getmondo/common/ui/VideoImageView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f05013a

    invoke-virtual {v0, v1, p0, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 58
    invoke-static {p0}, Lbutterknife/ButterKnife;->bind(Landroid/view/View;)Lbutterknife/Unbinder;

    .line 60
    invoke-virtual {p0}, Lco/uk/getmondo/common/ui/VideoImageView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v0

    sget-object v1, Lco/uk/getmondo/c$b;->VideoImageView:[I

    invoke-virtual {v0, p2, v1, v2, v2}, Landroid/content/res/Resources$Theme;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v1

    .line 63
    const/4 v0, 0x0

    const/4 v2, 0x0

    :try_start_0
    invoke-virtual {v1, v0, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v0

    .line 64
    iget-object v2, p0, Lco/uk/getmondo/common/ui/VideoImageView;->placeholderImageView:Landroid/widget/ImageView;

    invoke-virtual {v2, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 66
    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v2

    .line 67
    iget-object v3, p0, Lco/uk/getmondo/common/ui/VideoImageView;->placeholderImageView:Landroid/widget/ImageView;

    invoke-virtual {v3, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 69
    const/4 v0, 0x2

    const/4 v3, 0x0

    invoke-virtual {v1, v0, v3}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v0

    .line 70
    invoke-static {v0}, Lco/uk/getmondo/common/ui/VideoImageView$a;->a(I)Lco/uk/getmondo/common/ui/VideoImageView$a;

    move-result-object v0

    .line 71
    iget-object v3, p0, Lco/uk/getmondo/common/ui/VideoImageView;->exoPlayerView:Lcom/google/android/exoplayer2/ui/SimpleExoPlayerView;

    invoke-static {v0}, Lco/uk/getmondo/common/ui/VideoImageView$a;->a(Lco/uk/getmondo/common/ui/VideoImageView$a;)I

    move-result v0

    invoke-virtual {v3, v0}, Lcom/google/android/exoplayer2/ui/SimpleExoPlayerView;->setResizeMode(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 73
    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    .line 76
    invoke-direct {p0, v2}, Lco/uk/getmondo/common/ui/VideoImageView;->a(I)V

    .line 77
    return-void

    .line 73
    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    throw v0
.end method

.method static synthetic a(Lcom/google/android/exoplayer2/upstream/RawResourceDataSource;)Lcom/google/android/exoplayer2/upstream/c;
    .locals 0

    .prologue
    .line 87
    return-object p0
.end method

.method private a(I)V
    .locals 7

    .prologue
    .line 81
    :try_start_0
    invoke-virtual {p0}, Lco/uk/getmondo/common/ui/VideoImageView;->getContext()Landroid/content/Context;

    move-result-object v0

    new-instance v1, Lcom/google/android/exoplayer2/b/b;

    invoke-direct {v1}, Lcom/google/android/exoplayer2/b/b;-><init>()V

    new-instance v2, Lcom/google/android/exoplayer2/c;

    invoke-direct {v2}, Lcom/google/android/exoplayer2/c;-><init>()V

    invoke-static {v0, v1, v2}, Lcom/google/android/exoplayer2/f;->a(Landroid/content/Context;Lcom/google/android/exoplayer2/b/g;Lcom/google/android/exoplayer2/l;)Lcom/google/android/exoplayer2/t;

    move-result-object v6

    .line 82
    iget-object v0, p0, Lco/uk/getmondo/common/ui/VideoImageView;->exoPlayerView:Lcom/google/android/exoplayer2/ui/SimpleExoPlayerView;

    invoke-virtual {v0, v6}, Lcom/google/android/exoplayer2/ui/SimpleExoPlayerView;->setPlayer(Lcom/google/android/exoplayer2/t;)V

    .line 84
    new-instance v2, Lcom/google/android/exoplayer2/upstream/RawResourceDataSource;

    invoke-virtual {p0}, Lco/uk/getmondo/common/ui/VideoImageView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {v2, v0}, Lcom/google/android/exoplayer2/upstream/RawResourceDataSource;-><init>(Landroid/content/Context;)V

    .line 85
    new-instance v0, Lcom/google/android/exoplayer2/upstream/e;

    invoke-static {p1}, Lcom/google/android/exoplayer2/upstream/RawResourceDataSource;->a(I)Landroid/net/Uri;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/exoplayer2/upstream/e;-><init>(Landroid/net/Uri;)V

    invoke-virtual {v2, v0}, Lcom/google/android/exoplayer2/upstream/RawResourceDataSource;->a(Lcom/google/android/exoplayer2/upstream/e;)J

    .line 87
    new-instance v0, Lcom/google/android/exoplayer2/source/f;

    invoke-virtual {v2}, Lcom/google/android/exoplayer2/upstream/RawResourceDataSource;->a()Landroid/net/Uri;

    move-result-object v1

    invoke-static {v2}, Lco/uk/getmondo/common/ui/l;->a(Lcom/google/android/exoplayer2/upstream/RawResourceDataSource;)Lcom/google/android/exoplayer2/upstream/c$a;

    move-result-object v2

    new-instance v3, Lcom/google/android/exoplayer2/extractor/c;

    invoke-direct {v3}, Lcom/google/android/exoplayer2/extractor/c;-><init>()V

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/exoplayer2/source/f;-><init>(Landroid/net/Uri;Lcom/google/android/exoplayer2/upstream/c$a;Lcom/google/android/exoplayer2/extractor/h;Landroid/os/Handler;Lcom/google/android/exoplayer2/source/f$a;)V

    .line 89
    new-instance v1, Lcom/google/android/exoplayer2/source/g;

    invoke-direct {v1, v0}, Lcom/google/android/exoplayer2/source/g;-><init>(Lcom/google/android/exoplayer2/source/i;)V

    invoke-virtual {v6, v1}, Lcom/google/android/exoplayer2/t;->a(Lcom/google/android/exoplayer2/source/i;)V

    .line 90
    const/4 v0, 0x1

    invoke-virtual {v6, v0}, Lcom/google/android/exoplayer2/t;->a(Z)V

    .line 91
    new-instance v0, Lco/uk/getmondo/common/ui/VideoImageView$1;

    invoke-direct {v0, p0}, Lco/uk/getmondo/common/ui/VideoImageView$1;-><init>(Lco/uk/getmondo/common/ui/VideoImageView;)V

    invoke-virtual {v6, v0}, Lcom/google/android/exoplayer2/t;->a(Lcom/google/android/exoplayer2/o$a;)V
    :try_end_0
    .catch Lcom/google/android/exoplayer2/upstream/RawResourceDataSource$RawResourceDataSourceException; {:try_start_0 .. :try_end_0} :catch_0

    .line 139
    :goto_0
    return-void

    .line 136
    :catch_0
    move-exception v0

    .line 137
    const-string v1, "Failed to play video"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Ld/a/a;->a(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 142
    iget-object v0, p0, Lco/uk/getmondo/common/ui/VideoImageView;->exoPlayerView:Lcom/google/android/exoplayer2/ui/SimpleExoPlayerView;

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/ui/SimpleExoPlayerView;->getPlayer()Lcom/google/android/exoplayer2/t;

    move-result-object v0

    .line 143
    if-eqz v0, :cond_0

    .line 144
    invoke-virtual {v0}, Lcom/google/android/exoplayer2/t;->d()V

    .line 146
    :cond_0
    return-void
.end method
