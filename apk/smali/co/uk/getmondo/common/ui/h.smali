.class public Lco/uk/getmondo/common/ui/h;
.super Landroid/support/v7/widget/RecyclerView$g;
.source "SimpleDividerItemDecoration.java"


# instance fields
.field private final a:I

.field private final b:Landroid/graphics/drawable/Drawable;

.field private final c:La/a/a/a/a/a;

.field private final d:I

.field private final e:I


# direct methods
.method public constructor <init>(Landroid/content/Context;La/a/a/a/a/a;I)V
    .locals 2

    .prologue
    .line 21
    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView$g;-><init>()V

    .line 22
    iput p3, p0, Lco/uk/getmondo/common/ui/h;->a:I

    .line 23
    const v0, 0x7f0201cf

    invoke-static {p1, v0}, Landroid/support/v4/content/a;->a(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/common/ui/h;->b:Landroid/graphics/drawable/Drawable;

    .line 24
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0115

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lco/uk/getmondo/common/ui/h;->d:I

    .line 25
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0113

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lco/uk/getmondo/common/ui/h;->e:I

    .line 26
    iput-object p2, p0, Lco/uk/getmondo/common/ui/h;->c:La/a/a/a/a/a;

    .line 27
    return-void
.end method


# virtual methods
.method public getItemOffsets(Landroid/graphics/Rect;Landroid/view/View;Landroid/support/v7/widget/RecyclerView;Landroid/support/v7/widget/RecyclerView$t;)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 57
    invoke-virtual {p3, p2}, Landroid/support/v7/widget/RecyclerView;->f(Landroid/view/View;)I

    move-result v0

    .line 58
    invoke-virtual {p3}, Landroid/support/v7/widget/RecyclerView;->s()Z

    move-result v2

    if-nez v2, :cond_0

    const/4 v2, -0x1

    if-ne v0, v2, :cond_1

    .line 70
    :cond_0
    :goto_0
    return-void

    .line 62
    :cond_1
    if-nez v0, :cond_2

    .line 63
    iget v0, p0, Lco/uk/getmondo/common/ui/h;->d:I

    .line 69
    :goto_1
    invoke-virtual {p1, v1, v0, v1, v1}, Landroid/graphics/Rect;->set(IIII)V

    goto :goto_0

    .line 64
    :cond_2
    iget-object v2, p0, Lco/uk/getmondo/common/ui/h;->c:La/a/a/a/a/a;

    invoke-interface {v2, v0}, La/a/a/a/a/a;->a(I)J

    move-result-wide v2

    iget-object v4, p0, Lco/uk/getmondo/common/ui/h;->c:La/a/a/a/a/a;

    add-int/lit8 v0, v0, -0x1

    invoke-interface {v4, v0}, La/a/a/a/a/a;->a(I)J

    move-result-wide v4

    cmp-long v0, v2, v4

    if-eqz v0, :cond_3

    .line 65
    iget v0, p0, Lco/uk/getmondo/common/ui/h;->e:I

    goto :goto_1

    :cond_3
    move v0, v1

    .line 67
    goto :goto_1
.end method

.method public onDrawOver(Landroid/graphics/Canvas;Landroid/support/v7/widget/RecyclerView;Landroid/support/v7/widget/RecyclerView$t;)V
    .locals 10

    .prologue
    .line 31
    invoke-virtual {p2}, Landroid/support/v7/widget/RecyclerView;->s()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 53
    :cond_0
    return-void

    .line 34
    :cond_1
    invoke-virtual {p2}, Landroid/support/v7/widget/RecyclerView;->getPaddingLeft()I

    move-result v0

    iget v1, p0, Lco/uk/getmondo/common/ui/h;->a:I

    add-int v2, v0, v1

    .line 35
    invoke-virtual {p2}, Landroid/support/v7/widget/RecyclerView;->getWidth()I

    move-result v0

    invoke-virtual {p2}, Landroid/support/v7/widget/RecyclerView;->getPaddingRight()I

    move-result v1

    sub-int v3, v0, v1

    .line 37
    invoke-virtual {p2}, Landroid/support/v7/widget/RecyclerView;->getChildCount()I

    move-result v4

    .line 38
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    add-int/lit8 v0, v4, -0x1

    if-ge v1, v0, :cond_0

    .line 39
    invoke-virtual {p2, v1}, Landroid/support/v7/widget/RecyclerView;->getChildAt(I)Landroid/view/View;

    move-result-object v5

    .line 40
    invoke-virtual {p2, v5}, Landroid/support/v7/widget/RecyclerView;->f(Landroid/view/View;)I

    move-result v0

    .line 42
    const/4 v6, -0x1

    if-eq v0, v6, :cond_2

    iget-object v6, p0, Lco/uk/getmondo/common/ui/h;->c:La/a/a/a/a/a;

    invoke-interface {v6, v0}, La/a/a/a/a/a;->a(I)J

    move-result-wide v6

    iget-object v8, p0, Lco/uk/getmondo/common/ui/h;->c:La/a/a/a/a/a;

    add-int/lit8 v0, v0, 0x1

    invoke-interface {v8, v0}, La/a/a/a/a/a;->a(I)J

    move-result-wide v8

    cmp-long v0, v6, v8

    if-nez v0, :cond_2

    .line 44
    invoke-virtual {v5}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView$i;

    .line 46
    invoke-virtual {v5}, Landroid/view/View;->getBottom()I

    move-result v5

    iget v0, v0, Landroid/support/v7/widget/RecyclerView$i;->bottomMargin:I

    add-int/2addr v0, v5

    .line 47
    iget-object v5, p0, Lco/uk/getmondo/common/ui/h;->b:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v5}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v5

    add-int/2addr v5, v0

    .line 49
    iget-object v6, p0, Lco/uk/getmondo/common/ui/h;->b:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v6, v2, v0, v3, v5}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 50
    iget-object v0, p0, Lco/uk/getmondo/common/ui/h;->b:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 38
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0
.end method
