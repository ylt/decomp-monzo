.class public final Lco/uk/getmondo/common/ui/CopyableLinearLayout;
.super Landroid/support/v7/widget/at;
.source "CopyableLinearLayout.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lco/uk/getmondo/common/ui/CopyableLinearLayout$a;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u00002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0004\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0018\u00002\u00020\u0001:\u0001\u0012B%\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\n\u0008\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u0012\u0008\u0008\u0002\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\u0008J\u0012\u0010\u000b\u001a\u00020\u000c2\u0008\u0010\r\u001a\u0004\u0018\u00010\u000eH\u0014J\u0008\u0010\u000f\u001a\u00020\u0010H\u0014J\u0010\u0010\u0011\u001a\u00020\u00102\u0006\u0010\u0004\u001a\u00020\u0005H\u0016J\u0012\u0010\u0011\u001a\u00020\u00102\u0008\u0010\r\u001a\u0004\u0018\u00010\u000eH\u0014R\u000e\u0010\t\u001a\u00020\u0007X\u0082D\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u0007X\u0082D\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0013"
    }
    d2 = {
        "Lco/uk/getmondo/common/ui/CopyableLinearLayout;",
        "Landroid/support/v7/widget/LinearLayoutCompat;",
        "context",
        "Landroid/content/Context;",
        "attrs",
        "Landroid/util/AttributeSet;",
        "defStyle",
        "",
        "(Landroid/content/Context;Landroid/util/AttributeSet;I)V",
        "copyAsContent",
        "copyAsLabel",
        "checkLayoutParams",
        "",
        "p",
        "Landroid/view/ViewGroup$LayoutParams;",
        "generateDefaultLayoutParams",
        "Landroid/support/v7/widget/LinearLayoutCompat$LayoutParams;",
        "generateLayoutParams",
        "LayoutParams",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field private final a:I

.field private final b:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 6

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x6

    move-object v0, p0

    move-object v1, p1

    move-object v5, v2

    invoke-direct/range {v0 .. v5}, Lco/uk/getmondo/common/ui/CopyableLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/d/b/i;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 6

    const/4 v3, 0x0

    const/4 v4, 0x4

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    invoke-direct/range {v0 .. v5}, Lco/uk/getmondo/common/ui/CopyableLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/d/b/i;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3

    .prologue
    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 17
    invoke-direct {p0, p1, p2, p3}, Landroid/support/v7/widget/at;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 19
    const/4 v0, 0x1

    iput v0, p0, Lco/uk/getmondo/common/ui/CopyableLinearLayout;->a:I

    .line 20
    const/4 v0, 0x2

    iput v0, p0, Lco/uk/getmondo/common/ui/CopyableLinearLayout;->b:I

    .line 23
    sget-object v0, Lco/uk/getmondo/c$b;->CopyableLinearLayout:[I

    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v1

    .line 24
    new-instance v2, Lkotlin/d/b/x$c;

    invoke-direct {v2}, Lkotlin/d/b/x$c;-><init>()V

    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v2, Lkotlin/d/b/x$c;->a:Ljava/lang/Object;

    .line 25
    iget-object v0, v2, Lkotlin/d/b/x$c;->a:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    if-nez v0, :cond_0

    .line 26
    const-string v0, " "

    iput-object v0, v2, Lkotlin/d/b/x$c;->a:Ljava/lang/Object;

    .line 28
    :cond_0
    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    .line 29
    new-instance v0, Lco/uk/getmondo/common/ui/CopyableLinearLayout$1;

    invoke-direct {v0, p0, v2, p1}, Lco/uk/getmondo/common/ui/CopyableLinearLayout$1;-><init>(Lco/uk/getmondo/common/ui/CopyableLinearLayout;Lkotlin/d/b/x$c;Landroid/content/Context;)V

    check-cast v0, Landroid/view/View$OnLongClickListener;

    invoke-virtual {p0, v0}, Lco/uk/getmondo/common/ui/CopyableLinearLayout;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    return-void
.end method

.method public synthetic constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/d/b/i;)V
    .locals 2

    .prologue
    and-int/lit8 v0, p4, 0x2

    if-eqz v0, :cond_1

    .line 15
    const/4 v0, 0x0

    check-cast v0, Landroid/util/AttributeSet;

    :goto_0
    and-int/lit8 v1, p4, 0x4

    if-eqz v1, :cond_0

    .line 16
    const/4 p3, 0x0

    :cond_0
    invoke-direct {p0, p1, v0, p3}, Lco/uk/getmondo/common/ui/CopyableLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void

    :cond_1
    move-object v0, p2

    goto :goto_0
.end method

.method public static final synthetic a(Lco/uk/getmondo/common/ui/CopyableLinearLayout;)I
    .locals 1

    .prologue
    .line 13
    iget v0, p0, Lco/uk/getmondo/common/ui/CopyableLinearLayout;->a:I

    return v0
.end method

.method public static final synthetic b(Lco/uk/getmondo/common/ui/CopyableLinearLayout;)I
    .locals 1

    .prologue
    .line 13
    iget v0, p0, Lco/uk/getmondo/common/ui/CopyableLinearLayout;->b:I

    return v0
.end method


# virtual methods
.method public b(Landroid/util/AttributeSet;)Landroid/support/v7/widget/at$a;
    .locals 3

    .prologue
    const-string v0, "attrs"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 38
    new-instance v0, Lco/uk/getmondo/common/ui/CopyableLinearLayout$a;

    invoke-virtual {p0}, Lco/uk/getmondo/common/ui/CopyableLinearLayout;->getContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "context"

    invoke-static {v1, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v0, v1, p1}, Lco/uk/getmondo/common/ui/CopyableLinearLayout$a;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    check-cast v0, Landroid/support/v7/widget/at$a;

    return-object v0
.end method

.method protected b(Landroid/view/ViewGroup$LayoutParams;)Landroid/support/v7/widget/at$a;
    .locals 1

    .prologue
    .line 42
    new-instance v0, Landroid/support/v7/widget/at$a;

    invoke-direct {v0, p1}, Landroid/support/v7/widget/at$a;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    return-object v0
.end method

.method protected checkLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Z
    .locals 1

    .prologue
    .line 50
    instance-of v0, p1, Lco/uk/getmondo/common/ui/CopyableLinearLayout$a;

    return v0
.end method

.method public synthetic generateDefaultLayoutParams()Landroid/view/ViewGroup$LayoutParams;
    .locals 1

    .prologue
    .line 13
    invoke-virtual {p0}, Lco/uk/getmondo/common/ui/CopyableLinearLayout;->j()Landroid/support/v7/widget/at$a;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$LayoutParams;

    return-object v0
.end method

.method public synthetic generateLayoutParams(Landroid/util/AttributeSet;)Landroid/view/ViewGroup$LayoutParams;
    .locals 1

    .prologue
    .line 13
    invoke-virtual {p0, p1}, Lco/uk/getmondo/common/ui/CopyableLinearLayout;->b(Landroid/util/AttributeSet;)Landroid/support/v7/widget/at$a;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$LayoutParams;

    return-object v0
.end method

.method public synthetic generateLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Landroid/view/ViewGroup$LayoutParams;
    .locals 1

    .prologue
    .line 13
    invoke-virtual {p0, p1}, Lco/uk/getmondo/common/ui/CopyableLinearLayout;->b(Landroid/view/ViewGroup$LayoutParams;)Landroid/support/v7/widget/at$a;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$LayoutParams;

    return-object v0
.end method

.method protected j()Landroid/support/v7/widget/at$a;
    .locals 3

    .prologue
    .line 46
    new-instance v0, Landroid/support/v7/widget/at$a;

    invoke-virtual {p0}, Lco/uk/getmondo/common/ui/CopyableLinearLayout;->getContext()Landroid/content/Context;

    move-result-object v1

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Landroid/support/v7/widget/at$a;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-object v0
.end method
