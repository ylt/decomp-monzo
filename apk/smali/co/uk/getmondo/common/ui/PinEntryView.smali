.class public Lco/uk/getmondo/common/ui/PinEntryView;
.super Landroid/support/v7/widget/n;
.source "PinEntryView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lco/uk/getmondo/common/ui/PinEntryView$a;
    }
.end annotation


# instance fields
.field private final a:Landroid/graphics/Paint;

.field private final b:Landroid/graphics/Paint;

.field private final c:Landroid/graphics/Paint;

.field private final d:Landroid/graphics/Paint;

.field private final e:Landroid/graphics/Paint;

.field private final f:I

.field private final g:I

.field private final h:I

.field private final i:I

.field private final j:I

.field private k:Lco/uk/getmondo/common/ui/PinEntryView$a;

.field private l:I

.field private m:Z

.field private n:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 48
    const/4 v0, 0x0

    const v1, 0x7f0100f4

    invoke-direct {p0, p1, v0, v1}, Lco/uk/getmondo/common/ui/PinEntryView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 49
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 52
    const v0, 0x7f0100f4

    invoke-direct {p0, p1, p2, v0}, Lco/uk/getmondo/common/ui/PinEntryView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 53
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 10

    .prologue
    const v9, 0x106000d

    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 56
    invoke-direct {p0, p1, p2, p3}, Landroid/support/v7/widget/n;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 27
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0, v7}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lco/uk/getmondo/common/ui/PinEntryView;->a:Landroid/graphics/Paint;

    .line 28
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0, v7}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lco/uk/getmondo/common/ui/PinEntryView;->b:Landroid/graphics/Paint;

    .line 29
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0, v7}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lco/uk/getmondo/common/ui/PinEntryView;->c:Landroid/graphics/Paint;

    .line 30
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0, v7}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lco/uk/getmondo/common/ui/PinEntryView;->d:Landroid/graphics/Paint;

    .line 31
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0, v7}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lco/uk/getmondo/common/ui/PinEntryView;->e:Landroid/graphics/Paint;

    .line 43
    iput v6, p0, Lco/uk/getmondo/common/ui/PinEntryView;->l:I

    .line 45
    iput-boolean v6, p0, Lco/uk/getmondo/common/ui/PinEntryView;->n:Z

    .line 58
    invoke-virtual {p0}, Lco/uk/getmondo/common/ui/PinEntryView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    .line 60
    const v0, 0x7f0f006e

    invoke-static {p1, v0}, Landroid/support/v4/content/a;->c(Landroid/content/Context;I)I

    move-result v2

    .line 62
    invoke-virtual {p1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v0

    sget-object v3, Lco/uk/getmondo/c$b;->PinEntryView:[I

    invoke-virtual {v0, p2, v3, v6, v6}, Landroid/content/res/Resources$Theme;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v3

    .line 64
    const/4 v0, 0x1

    :try_start_0
    invoke-virtual {v3, v0, v2}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v0

    .line 65
    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v4

    iput-boolean v4, p0, Lco/uk/getmondo/common/ui/PinEntryView;->n:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 67
    invoke-virtual {v3}, Landroid/content/res/TypedArray;->recycle()V

    .line 70
    iget-object v3, p0, Lco/uk/getmondo/common/ui/PinEntryView;->a:Landroid/graphics/Paint;

    invoke-virtual {v3, v0}, Landroid/graphics/Paint;->setColor(I)V

    .line 71
    const/high16 v0, 0x40c00000    # 6.0f

    invoke-static {v8, v0, v1}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lco/uk/getmondo/common/ui/PinEntryView;->i:I

    .line 73
    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    .line 74
    invoke-virtual {p1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v3

    const v4, 0x1010435

    invoke-virtual {v3, v4, v0, v7}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    .line 75
    iget v3, v0, Landroid/util/TypedValue;->resourceId:I

    if-lez v3, :cond_0

    iget v0, v0, Landroid/util/TypedValue;->resourceId:I

    invoke-static {p1, v0}, Landroid/support/v4/content/a;->c(Landroid/content/Context;I)I

    move-result v0

    .line 77
    :goto_0
    iget-object v3, p0, Lco/uk/getmondo/common/ui/PinEntryView;->c:Landroid/graphics/Paint;

    invoke-virtual {v3, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 78
    iget-object v2, p0, Lco/uk/getmondo/common/ui/PinEntryView;->c:Landroid/graphics/Paint;

    const/high16 v3, 0x40000000    # 2.0f

    invoke-static {v8, v3, v1}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v1

    invoke-virtual {v2, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 80
    iget-object v1, p0, Lco/uk/getmondo/common/ui/PinEntryView;->d:Landroid/graphics/Paint;

    invoke-virtual {v1, v0}, Landroid/graphics/Paint;->setColor(I)V

    .line 81
    iget-object v0, p0, Lco/uk/getmondo/common/ui/PinEntryView;->d:Landroid/graphics/Paint;

    iget-object v1, p0, Lco/uk/getmondo/common/ui/PinEntryView;->c:Landroid/graphics/Paint;

    invoke-virtual {v1}, Landroid/graphics/Paint;->getStrokeWidth()F

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 83
    iget-object v0, p0, Lco/uk/getmondo/common/ui/PinEntryView;->e:Landroid/graphics/Paint;

    const v1, 0x7f0f00d0

    invoke-static {p1, v1}, Landroid/support/v4/content/a;->c(Landroid/content/Context;I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 84
    iget-object v0, p0, Lco/uk/getmondo/common/ui/PinEntryView;->e:Landroid/graphics/Paint;

    iget-object v1, p0, Lco/uk/getmondo/common/ui/PinEntryView;->c:Landroid/graphics/Paint;

    invoke-virtual {v1}, Landroid/graphics/Paint;->getStrokeWidth()F

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 86
    iget-object v0, p0, Lco/uk/getmondo/common/ui/PinEntryView;->b:Landroid/graphics/Paint;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 87
    iget-object v0, p0, Lco/uk/getmondo/common/ui/PinEntryView;->b:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 88
    iget-object v0, p0, Lco/uk/getmondo/common/ui/PinEntryView;->b:Landroid/graphics/Paint;

    const/high16 v1, 0x42400000    # 48.0f

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 89
    iget-object v0, p0, Lco/uk/getmondo/common/ui/PinEntryView;->b:Landroid/graphics/Paint;

    const/high16 v1, 0x7f040000

    invoke-static {p1, v1}, Landroid/support/v4/content/a/b;->a(Landroid/content/Context;I)Landroid/graphics/Typeface;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 91
    invoke-virtual {p0}, Lco/uk/getmondo/common/ui/PinEntryView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0113

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    iput v0, p0, Lco/uk/getmondo/common/ui/PinEntryView;->j:I

    .line 92
    invoke-virtual {p0}, Lco/uk/getmondo/common/ui/PinEntryView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0115

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lco/uk/getmondo/common/ui/PinEntryView;->h:I

    .line 93
    invoke-virtual {p0}, Lco/uk/getmondo/common/ui/PinEntryView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0028

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lco/uk/getmondo/common/ui/PinEntryView;->f:I

    .line 94
    invoke-virtual {p0}, Lco/uk/getmondo/common/ui/PinEntryView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0027

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lco/uk/getmondo/common/ui/PinEntryView;->g:I

    .line 97
    invoke-virtual {p0}, Lco/uk/getmondo/common/ui/PinEntryView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, v9}, Landroid/support/v4/content/a;->c(Landroid/content/Context;I)I

    move-result v0

    invoke-virtual {p0, v0}, Lco/uk/getmondo/common/ui/PinEntryView;->setBackgroundColor(I)V

    .line 98
    invoke-virtual {p0}, Lco/uk/getmondo/common/ui/PinEntryView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, v9}, Landroid/support/v4/content/a;->c(Landroid/content/Context;I)I

    move-result v0

    invoke-virtual {p0, v0}, Lco/uk/getmondo/common/ui/PinEntryView;->setTextColor(I)V

    .line 99
    invoke-virtual {p0, v6}, Lco/uk/getmondo/common/ui/PinEntryView;->setCursorVisible(Z)V

    .line 100
    new-array v0, v7, [Landroid/text/InputFilter;

    new-instance v1, Landroid/text/InputFilter$LengthFilter;

    const/4 v2, 0x4

    invoke-direct {v1, v2}, Landroid/text/InputFilter$LengthFilter;-><init>(I)V

    aput-object v1, v0, v6

    invoke-virtual {p0, v0}, Lco/uk/getmondo/common/ui/PinEntryView;->setFilters([Landroid/text/InputFilter;)V

    .line 101
    const/16 v0, 0x12

    invoke-virtual {p0, v0}, Lco/uk/getmondo/common/ui/PinEntryView;->setInputType(I)V

    .line 102
    const/high16 v0, 0x10000000

    invoke-virtual {p0, v0}, Lco/uk/getmondo/common/ui/PinEntryView;->setImeOptions(I)V

    .line 103
    invoke-static {p0}, Lco/uk/getmondo/common/ui/g;->a(Lco/uk/getmondo/common/ui/PinEntryView;)Landroid/view/View$OnFocusChangeListener;

    move-result-object v0

    invoke-virtual {p0, v0}, Lco/uk/getmondo/common/ui/PinEntryView;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 108
    new-instance v0, Lco/uk/getmondo/common/ui/PinEntryView$1;

    invoke-direct {v0, p0}, Lco/uk/getmondo/common/ui/PinEntryView$1;-><init>(Lco/uk/getmondo/common/ui/PinEntryView;)V

    invoke-virtual {p0, v0}, Lco/uk/getmondo/common/ui/PinEntryView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 128
    return-void

    .line 67
    :catchall_0
    move-exception v0

    invoke-virtual {v3}, Landroid/content/res/TypedArray;->recycle()V

    throw v0

    .line 75
    :cond_0
    iget v0, v0, Landroid/util/TypedValue;->data:I

    goto/16 :goto_0
.end method

.method static synthetic a(Lco/uk/getmondo/common/ui/PinEntryView;)I
    .locals 1

    .prologue
    .line 24
    iget v0, p0, Lco/uk/getmondo/common/ui/PinEntryView;->l:I

    return v0
.end method

.method static synthetic a(Lco/uk/getmondo/common/ui/PinEntryView;I)I
    .locals 0

    .prologue
    .line 24
    iput p1, p0, Lco/uk/getmondo/common/ui/PinEntryView;->l:I

    return p1
.end method

.method static synthetic a(Lco/uk/getmondo/common/ui/PinEntryView;Landroid/view/View;Z)V
    .locals 1

    .prologue
    .line 105
    invoke-virtual {p0}, Lco/uk/getmondo/common/ui/PinEntryView;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->length()I

    move-result v0

    .line 106
    invoke-virtual {p0, v0}, Lco/uk/getmondo/common/ui/PinEntryView;->setSelection(I)V

    .line 107
    return-void
.end method

.method static synthetic b(Lco/uk/getmondo/common/ui/PinEntryView;)Lco/uk/getmondo/common/ui/PinEntryView$a;
    .locals 1

    .prologue
    .line 24
    iget-object v0, p0, Lco/uk/getmondo/common/ui/PinEntryView;->k:Lco/uk/getmondo/common/ui/PinEntryView$a;

    return-object v0
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 135
    const/4 v0, 0x0

    iput v0, p0, Lco/uk/getmondo/common/ui/PinEntryView;->l:I

    .line 136
    const-string v0, ""

    invoke-virtual {p0, v0}, Lco/uk/getmondo/common/ui/PinEntryView;->setText(Ljava/lang/CharSequence;)V

    .line 137
    return-void
.end method

.method public a(Z)V
    .locals 4

    .prologue
    .line 148
    const/4 v0, -0x1

    iput v0, p0, Lco/uk/getmondo/common/ui/PinEntryView;->l:I

    .line 149
    const/4 v0, 0x1

    iput-boolean v0, p0, Lco/uk/getmondo/common/ui/PinEntryView;->m:Z

    .line 150
    invoke-virtual {p0}, Lco/uk/getmondo/common/ui/PinEntryView;->invalidate()V

    .line 152
    if-eqz p1, :cond_0

    .line 153
    const-string v0, "translationX"

    const/16 v1, 0xa

    new-array v1, v1, [F

    fill-array-data v1, :array_0

    .line 154
    invoke-static {p0, v0, v1}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    const-wide/16 v2, 0x12c

    .line 155
    invoke-virtual {v0, v2, v3}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    move-result-object v0

    .line 156
    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    .line 158
    :cond_0
    return-void

    .line 153
    :array_0
    .array-data 4
        0x0
        0x41c80000    # 25.0f
        -0x3e380000    # -25.0f
        0x41c80000    # 25.0f
        -0x3e380000    # -25.0f
        0x41700000    # 15.0f
        -0x3e900000    # -15.0f
        0x40c00000    # 6.0f
        -0x3f400000    # -6.0f
        0x0
    .end array-data
.end method

.method public b()V
    .locals 1

    .prologue
    .line 140
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lco/uk/getmondo/common/ui/PinEntryView;->setEnabled(Z)V

    .line 141
    return-void
.end method

.method public c()V
    .locals 1

    .prologue
    .line 144
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lco/uk/getmondo/common/ui/PinEntryView;->setEnabled(Z)V

    .line 145
    return-void
.end method

.method public d()V
    .locals 1

    .prologue
    .line 161
    invoke-virtual {p0}, Lco/uk/getmondo/common/ui/PinEntryView;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->length()I

    move-result v0

    iput v0, p0, Lco/uk/getmondo/common/ui/PinEntryView;->l:I

    .line 162
    const/4 v0, 0x0

    iput-boolean v0, p0, Lco/uk/getmondo/common/ui/PinEntryView;->m:Z

    .line 163
    invoke-virtual {p0}, Lco/uk/getmondo/common/ui/PinEntryView;->invalidate()V

    .line 164
    return-void
.end method

.method protected dispatchDraw(Landroid/graphics/Canvas;)V
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 168
    invoke-super {p0, p1}, Landroid/support/v7/widget/n;->dispatchDraw(Landroid/graphics/Canvas;)V

    move v0, v1

    .line 170
    :goto_0
    iget v2, p0, Lco/uk/getmondo/common/ui/PinEntryView;->l:I

    if-ge v0, v2, :cond_1

    .line 171
    iget v2, p0, Lco/uk/getmondo/common/ui/PinEntryView;->f:I

    mul-int/2addr v2, v0

    iget v3, p0, Lco/uk/getmondo/common/ui/PinEntryView;->h:I

    mul-int/2addr v3, v0

    add-int/2addr v2, v3

    iget v3, p0, Lco/uk/getmondo/common/ui/PinEntryView;->i:I

    div-int/lit8 v3, v3, 0x2

    add-int/2addr v2, v3

    iget v3, p0, Lco/uk/getmondo/common/ui/PinEntryView;->f:I

    div-int/lit8 v3, v3, 0x2

    add-int/2addr v2, v3

    .line 172
    iget v3, p0, Lco/uk/getmondo/common/ui/PinEntryView;->g:I

    div-int/lit8 v3, v3, 0x2

    iget v4, p0, Lco/uk/getmondo/common/ui/PinEntryView;->i:I

    div-int/lit8 v4, v4, 0x2

    add-int/2addr v3, v4

    .line 173
    int-to-float v4, v2

    int-to-float v5, v3

    iget v6, p0, Lco/uk/getmondo/common/ui/PinEntryView;->i:I

    int-to-float v6, v6

    iget-object v7, p0, Lco/uk/getmondo/common/ui/PinEntryView;->a:Landroid/graphics/Paint;

    invoke-virtual {p1, v4, v5, v6, v7}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 175
    iget-boolean v4, p0, Lco/uk/getmondo/common/ui/PinEntryView;->n:Z

    if-eqz v4, :cond_0

    .line 176
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lco/uk/getmondo/common/ui/PinEntryView;->getText()Landroid/text/Editable;

    move-result-object v5

    invoke-interface {v5, v0}, Landroid/text/Editable;->charAt(I)C

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ""

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 177
    iget-object v5, p0, Lco/uk/getmondo/common/ui/PinEntryView;->b:Landroid/graphics/Paint;

    invoke-virtual {v5, v4}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    move-result v5

    .line 178
    int-to-float v2, v2

    const/high16 v6, 0x40000000    # 2.0f

    div-float/2addr v5, v6

    sub-float/2addr v2, v5

    iget v5, p0, Lco/uk/getmondo/common/ui/PinEntryView;->j:I

    sub-int/2addr v3, v5

    int-to-float v3, v3

    iget-object v5, p0, Lco/uk/getmondo/common/ui/PinEntryView;->b:Landroid/graphics/Paint;

    invoke-virtual {p1, v4, v2, v3, v5}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 170
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    move v6, v1

    .line 182
    :goto_1
    const/4 v0, 0x4

    if-ge v6, v0, :cond_4

    .line 183
    iget v0, p0, Lco/uk/getmondo/common/ui/PinEntryView;->f:I

    mul-int/2addr v0, v6

    iget v1, p0, Lco/uk/getmondo/common/ui/PinEntryView;->h:I

    mul-int/2addr v1, v6

    add-int/2addr v0, v1

    .line 185
    iget v1, p0, Lco/uk/getmondo/common/ui/PinEntryView;->l:I

    if-ne v6, v1, :cond_2

    .line 186
    iget-object v5, p0, Lco/uk/getmondo/common/ui/PinEntryView;->d:Landroid/graphics/Paint;

    .line 192
    :goto_2
    int-to-float v1, v0

    iget v2, p0, Lco/uk/getmondo/common/ui/PinEntryView;->g:I

    int-to-float v2, v2

    iget v3, p0, Lco/uk/getmondo/common/ui/PinEntryView;->f:I

    add-int/2addr v0, v3

    int-to-float v3, v0

    iget v0, p0, Lco/uk/getmondo/common/ui/PinEntryView;->g:I

    int-to-float v4, v0

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 182
    add-int/lit8 v1, v6, 0x1

    move v6, v1

    goto :goto_1

    .line 187
    :cond_2
    iget-boolean v1, p0, Lco/uk/getmondo/common/ui/PinEntryView;->m:Z

    if-eqz v1, :cond_3

    .line 188
    iget-object v5, p0, Lco/uk/getmondo/common/ui/PinEntryView;->e:Landroid/graphics/Paint;

    goto :goto_2

    .line 190
    :cond_3
    iget-object v5, p0, Lco/uk/getmondo/common/ui/PinEntryView;->c:Landroid/graphics/Paint;

    goto :goto_2

    .line 194
    :cond_4
    return-void
.end method

.method protected onMeasure(II)V
    .locals 3

    .prologue
    .line 198
    iget v0, p0, Lco/uk/getmondo/common/ui/PinEntryView;->f:I

    mul-int/lit8 v0, v0, 0x4

    iget v1, p0, Lco/uk/getmondo/common/ui/PinEntryView;->h:I

    mul-int/lit8 v1, v1, 0x3

    add-int/2addr v0, v1

    .line 199
    invoke-virtual {p0}, Lco/uk/getmondo/common/ui/PinEntryView;->getPaddingLeft()I

    move-result v1

    add-int/2addr v0, v1

    invoke-virtual {p0}, Lco/uk/getmondo/common/ui/PinEntryView;->getPaddingRight()I

    move-result v1

    add-int/2addr v0, v1

    iget v1, p0, Lco/uk/getmondo/common/ui/PinEntryView;->g:I

    invoke-virtual {p0}, Lco/uk/getmondo/common/ui/PinEntryView;->getPaddingTop()I

    move-result v2

    add-int/2addr v1, v2

    invoke-virtual {p0}, Lco/uk/getmondo/common/ui/PinEntryView;->getPaddingBottom()I

    move-result v2

    add-int/2addr v1, v2

    invoke-virtual {p0, v0, v1}, Lco/uk/getmondo/common/ui/PinEntryView;->setMeasuredDimension(II)V

    .line 200
    return-void
.end method

.method public setOnPinEnteredListener(Lco/uk/getmondo/common/ui/PinEntryView$a;)V
    .locals 0

    .prologue
    .line 131
    iput-object p1, p0, Lco/uk/getmondo/common/ui/PinEntryView;->k:Lco/uk/getmondo/common/ui/PinEntryView$a;

    .line 132
    return-void
.end method
