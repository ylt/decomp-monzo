.class Lco/uk/getmondo/common/ui/AmountInputView$a;
.super Ljava/lang/Object;
.source "AmountInputView.java"

# interfaces
.implements Landroid/text/InputFilter;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lco/uk/getmondo/common/ui/AmountInputView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "a"
.end annotation


# instance fields
.field private a:Ljava/util/regex/Pattern;

.field private final b:I

.field private final c:I


# direct methods
.method constructor <init>(II)V
    .locals 1

    .prologue
    .line 162
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 163
    if-lez p2, :cond_0

    const-string v0, "[0-9.]*"

    :goto_0
    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/common/ui/AmountInputView$a;->a:Ljava/util/regex/Pattern;

    .line 164
    iput p1, p0, Lco/uk/getmondo/common/ui/AmountInputView$a;->c:I

    .line 165
    iput p2, p0, Lco/uk/getmondo/common/ui/AmountInputView$a;->b:I

    .line 166
    return-void

    .line 163
    :cond_0
    const-string v0, "[0-9]*"

    goto :goto_0
.end method


# virtual methods
.method public filter(Ljava/lang/CharSequence;IILandroid/text/Spanned;II)Ljava/lang/CharSequence;
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 170
    invoke-interface {p1, p2, p3}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v0

    .line 172
    iget-object v1, p0, Lco/uk/getmondo/common/ui/AmountInputView$a;->a:Ljava/util/regex/Pattern;

    invoke-virtual {v1, v0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/regex/Matcher;->matches()Z

    move-result v1

    if-nez v1, :cond_0

    .line 173
    const-string v0, ""

    .line 191
    :goto_0
    return-object v0

    .line 176
    :cond_0
    new-instance v1, Landroid/text/SpannableStringBuilder;

    invoke-direct {v1, p4}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 177
    invoke-virtual {v1, p5, p6, v0}, Landroid/text/SpannableStringBuilder;->replace(IILjava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 178
    invoke-virtual {v1}, Landroid/text/SpannableStringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 179
    const-string v1, "\\."

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 180
    array-length v2, v1

    const/4 v3, 0x2

    if-le v2, v3, :cond_1

    .line 181
    const-string v0, ""

    goto :goto_0

    .line 184
    :cond_1
    array-length v2, v1

    if-lez v2, :cond_2

    const/4 v0, 0x0

    aget-object v0, v1, v0

    .line 185
    :cond_2
    array-length v2, v1

    if-le v2, v4, :cond_4

    aget-object v1, v1, v4

    .line 186
    :goto_1
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    iget v2, p0, Lco/uk/getmondo/common/ui/AmountInputView$a;->c:I

    if-gt v0, v2, :cond_3

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v0

    iget v1, p0, Lco/uk/getmondo/common/ui/AmountInputView$a;->b:I

    if-le v0, v1, :cond_5

    .line 187
    :cond_3
    const-string v0, ""

    goto :goto_0

    .line 185
    :cond_4
    const-string v1, ""

    goto :goto_1

    .line 191
    :cond_5
    const/4 v0, 0x0

    goto :goto_0
.end method
