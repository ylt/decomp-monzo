.class final Lco/uk/getmondo/common/ui/LoadingErrorView$b;
.super Ljava/lang/Object;
.source "LoadingErrorView.kt"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lco/uk/getmondo/common/ui/LoadingErrorView;->setLoadingIconBackgroundColor(I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000\u0008\n\u0000\n\u0002\u0010\u0002\n\u0000\u0010\u0000\u001a\u00020\u0001H\n\u00a2\u0006\u0002\u0008\u0002"
    }
    d2 = {
        "<anonymous>",
        "",
        "run"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field final synthetic a:Lco/uk/getmondo/common/ui/LoadingErrorView;

.field final synthetic b:Landroid/graphics/drawable/ShapeDrawable;


# direct methods
.method constructor <init>(Lco/uk/getmondo/common/ui/LoadingErrorView;Landroid/graphics/drawable/ShapeDrawable;)V
    .locals 0

    iput-object p1, p0, Lco/uk/getmondo/common/ui/LoadingErrorView$b;->a:Lco/uk/getmondo/common/ui/LoadingErrorView;

    iput-object p2, p0, Lco/uk/getmondo/common/ui/LoadingErrorView$b;->b:Landroid/graphics/drawable/ShapeDrawable;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 2

    .prologue
    .line 85
    iget-object v0, p0, Lco/uk/getmondo/common/ui/LoadingErrorView$b;->a:Lco/uk/getmondo/common/ui/LoadingErrorView;

    sget v1, Lco/uk/getmondo/c$a;->progressBackgroundView:I

    invoke-virtual {v0, v1}, Lco/uk/getmondo/common/ui/LoadingErrorView;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iget-object v1, p0, Lco/uk/getmondo/common/ui/LoadingErrorView$b;->b:Landroid/graphics/drawable/ShapeDrawable;

    check-cast v1, Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 86
    iget-object v0, p0, Lco/uk/getmondo/common/ui/LoadingErrorView$b;->a:Lco/uk/getmondo/common/ui/LoadingErrorView;

    sget v1, Lco/uk/getmondo/c$a;->iconBackgroundView:I

    invoke-virtual {v0, v1}, Lco/uk/getmondo/common/ui/LoadingErrorView;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iget-object v1, p0, Lco/uk/getmondo/common/ui/LoadingErrorView$b;->b:Landroid/graphics/drawable/ShapeDrawable;

    check-cast v1, Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 87
    return-void
.end method
