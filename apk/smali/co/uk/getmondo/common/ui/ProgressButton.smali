.class public final Lco/uk/getmondo/common/ui/ProgressButton;
.super Landroid/support/v7/widget/i;
.source "ProgressButton.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000H\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\r\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\r\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B%\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\n\u0008\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u0012\u0008\u0008\u0002\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\u0008J\u0010\u0010\u0014\u001a\u00020\u00152\u0006\u0010\u0016\u001a\u00020\u0017H\u0014J0\u0010\u0018\u001a\u00020\u00152\u0006\u0010\u0019\u001a\u00020\n2\u0006\u0010\u001a\u001a\u00020\u00072\u0006\u0010\u001b\u001a\u00020\u00072\u0006\u0010\u001c\u001a\u00020\u00072\u0006\u0010\u001d\u001a\u00020\u0007H\u0014J(\u0010\u001e\u001a\u00020\u00152\u0006\u0010\u001f\u001a\u00020\u00072\u0006\u0010 \u001a\u00020\u00072\u0006\u0010!\u001a\u00020\u00072\u0006\u0010\"\u001a\u00020\u0007H\u0014J\u0012\u0010#\u001a\u00020\n2\u0008\u0010$\u001a\u0004\u0018\u00010%H\u0014R$\u0010\u000b\u001a\u00020\n2\u0006\u0010\t\u001a\u00020\n@FX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u000b\u0010\u000c\"\u0004\u0008\r\u0010\u000eR\u000e\u0010\u000f\u001a\u00020\u0010X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0011\u001a\u00020\nX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0012\u001a\u00020\u0013X\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006&"
    }
    d2 = {
        "Lco/uk/getmondo/common/ui/ProgressButton;",
        "Landroid/support/v7/widget/AppCompatButton;",
        "context",
        "Landroid/content/Context;",
        "attrs",
        "Landroid/util/AttributeSet;",
        "defStyle",
        "",
        "(Landroid/content/Context;Landroid/util/AttributeSet;I)V",
        "value",
        "",
        "isLoading",
        "()Z",
        "setLoading",
        "(Z)V",
        "progressDrawable",
        "Landroid/graphics/drawable/AnimatedVectorDrawable;",
        "progressDrawableBoundsChanged",
        "textCopy",
        "",
        "onDraw",
        "",
        "canvas",
        "Landroid/graphics/Canvas;",
        "onLayout",
        "changed",
        "left",
        "top",
        "right",
        "bottom",
        "onSizeChanged",
        "w",
        "h",
        "oldw",
        "oldh",
        "verifyDrawable",
        "who",
        "Landroid/graphics/drawable/Drawable;",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field private final a:Landroid/graphics/drawable/AnimatedVectorDrawable;

.field private b:Ljava/lang/CharSequence;

.field private c:Z

.field private d:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 6

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x6

    move-object v0, p0

    move-object v1, p1

    move-object v5, v2

    invoke-direct/range {v0 .. v5}, Lco/uk/getmondo/common/ui/ProgressButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/d/b/i;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 6

    const/4 v3, 0x0

    const/4 v4, 0x4

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    invoke-direct/range {v0 .. v5}, Lco/uk/getmondo/common/ui/ProgressButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/d/b/i;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    .prologue
    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 20
    invoke-direct {p0, p1, p2, p3}, Landroid/support/v7/widget/i;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 22
    const v0, 0x7f020058

    invoke-virtual {p1, v0}, Landroid/content/Context;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-nez v0, :cond_0

    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type android.graphics.drawable.AnimatedVectorDrawable"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    check-cast v0, Landroid/graphics/drawable/AnimatedVectorDrawable;

    iput-object v0, p0, Lco/uk/getmondo/common/ui/ProgressButton;->a:Landroid/graphics/drawable/AnimatedVectorDrawable;

    .line 23
    const-string v0, ""

    check-cast v0, Ljava/lang/CharSequence;

    iput-object v0, p0, Lco/uk/getmondo/common/ui/ProgressButton;->b:Ljava/lang/CharSequence;

    .line 27
    iget-object v0, p0, Lco/uk/getmondo/common/ui/ProgressButton;->a:Landroid/graphics/drawable/AnimatedVectorDrawable;

    check-cast p0, Landroid/graphics/drawable/Drawable$Callback;

    invoke-virtual {v0, p0}, Landroid/graphics/drawable/AnimatedVectorDrawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    return-void
.end method

.method public synthetic constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/d/b/i;)V
    .locals 2

    .prologue
    and-int/lit8 v0, p4, 0x2

    if-eqz v0, :cond_1

    .line 18
    const/4 v0, 0x0

    check-cast v0, Landroid/util/AttributeSet;

    :goto_0
    and-int/lit8 v1, p4, 0x4

    if-eqz v1, :cond_0

    .line 19
    const p3, 0x7f0100f0

    :cond_0
    invoke-direct {p0, p1, v0, p3}, Lco/uk/getmondo/common/ui/ProgressButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void

    :cond_1
    move-object v0, p2

    goto :goto_0
.end method


# virtual methods
.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 5

    .prologue
    const-string v0, "canvas"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 64
    invoke-super {p0, p1}, Landroid/support/v7/widget/i;->onDraw(Landroid/graphics/Canvas;)V

    .line 65
    iget-boolean v0, p0, Lco/uk/getmondo/common/ui/ProgressButton;->d:Z

    if-eqz v0, :cond_1

    .line 66
    iget-boolean v0, p0, Lco/uk/getmondo/common/ui/ProgressButton;->c:Z

    if-eqz v0, :cond_0

    .line 67
    const/4 v0, 0x0

    iput-boolean v0, p0, Lco/uk/getmondo/common/ui/ProgressButton;->c:Z

    .line 70
    invoke-virtual {p0}, Lco/uk/getmondo/common/ui/ProgressButton;->getWidth()I

    move-result v0

    invoke-virtual {p0}, Lco/uk/getmondo/common/ui/ProgressButton;->getHeight()I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    int-to-double v0, v0

    const-wide v2, 0x3fe6666666666666L    # 0.7

    mul-double/2addr v0, v2

    double-to-int v0, v0

    .line 72
    invoke-virtual {p0}, Lco/uk/getmondo/common/ui/ProgressButton;->getWidth()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    div-int/lit8 v2, v0, 0x2

    sub-int/2addr v1, v2

    .line 73
    invoke-virtual {p0}, Lco/uk/getmondo/common/ui/ProgressButton;->getHeight()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    div-int/lit8 v3, v0, 0x2

    sub-int/2addr v2, v3

    .line 74
    iget-object v3, p0, Lco/uk/getmondo/common/ui/ProgressButton;->a:Landroid/graphics/drawable/AnimatedVectorDrawable;

    add-int v4, v1, v0

    add-int/2addr v0, v2

    invoke-virtual {v3, v1, v2, v4, v0}, Landroid/graphics/drawable/AnimatedVectorDrawable;->setBounds(IIII)V

    .line 76
    :cond_0
    iget-object v0, p0, Lco/uk/getmondo/common/ui/ProgressButton;->a:Landroid/graphics/drawable/AnimatedVectorDrawable;

    invoke-virtual {p0}, Lco/uk/getmondo/common/ui/ProgressButton;->getCurrentTextColor()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/AnimatedVectorDrawable;->setTint(I)V

    .line 77
    iget-object v0, p0, Lco/uk/getmondo/common/ui/ProgressButton;->a:Landroid/graphics/drawable/AnimatedVectorDrawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/AnimatedVectorDrawable;->draw(Landroid/graphics/Canvas;)V

    .line 79
    :cond_1
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 1

    .prologue
    .line 52
    invoke-super/range {p0 .. p5}, Landroid/support/v7/widget/i;->onLayout(ZIIII)V

    .line 53
    iget-boolean v0, p0, Lco/uk/getmondo/common/ui/ProgressButton;->c:Z

    or-int/2addr v0, p1

    iput-boolean v0, p0, Lco/uk/getmondo/common/ui/ProgressButton;->c:Z

    .line 54
    return-void
.end method

.method protected onSizeChanged(IIII)V
    .locals 1

    .prologue
    .line 57
    invoke-super {p0, p1, p2, p3, p4}, Landroid/support/v7/widget/i;->onSizeChanged(IIII)V

    .line 58
    const/4 v0, 0x1

    iput-boolean v0, p0, Lco/uk/getmondo/common/ui/ProgressButton;->c:Z

    .line 59
    return-void
.end method

.method public final setLoading(Z)V
    .locals 2

    .prologue
    .line 32
    iget-boolean v0, p0, Lco/uk/getmondo/common/ui/ProgressButton;->d:Z

    if-ne v0, p1, :cond_0

    .line 49
    :goto_0
    return-void

    .line 36
    :cond_0
    iput-boolean p1, p0, Lco/uk/getmondo/common/ui/ProgressButton;->d:Z

    .line 37
    if-eqz p1, :cond_1

    .line 38
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lco/uk/getmondo/common/ui/ProgressButton;->setClickable(Z)V

    .line 39
    invoke-virtual {p0}, Lco/uk/getmondo/common/ui/ProgressButton;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    const-string v1, "text"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v0, p0, Lco/uk/getmondo/common/ui/ProgressButton;->b:Ljava/lang/CharSequence;

    .line 40
    const-string v0, ""

    check-cast v0, Ljava/lang/CharSequence;

    invoke-virtual {p0, v0}, Lco/uk/getmondo/common/ui/ProgressButton;->setText(Ljava/lang/CharSequence;)V

    .line 41
    iget-object v0, p0, Lco/uk/getmondo/common/ui/ProgressButton;->a:Landroid/graphics/drawable/AnimatedVectorDrawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/AnimatedVectorDrawable;->start()V

    .line 47
    :goto_1
    invoke-virtual {p0}, Lco/uk/getmondo/common/ui/ProgressButton;->invalidate()V

    goto :goto_0

    .line 43
    :cond_1
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lco/uk/getmondo/common/ui/ProgressButton;->setClickable(Z)V

    .line 44
    iget-object v0, p0, Lco/uk/getmondo/common/ui/ProgressButton;->b:Ljava/lang/CharSequence;

    invoke-virtual {p0, v0}, Lco/uk/getmondo/common/ui/ProgressButton;->setText(Ljava/lang/CharSequence;)V

    .line 45
    const-string v0, ""

    check-cast v0, Ljava/lang/CharSequence;

    iput-object v0, p0, Lco/uk/getmondo/common/ui/ProgressButton;->b:Ljava/lang/CharSequence;

    .line 46
    iget-object v0, p0, Lco/uk/getmondo/common/ui/ProgressButton;->a:Landroid/graphics/drawable/AnimatedVectorDrawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/AnimatedVectorDrawable;->stop()V

    goto :goto_1
.end method

.method protected verifyDrawable(Landroid/graphics/drawable/Drawable;)Z
    .locals 1

    .prologue
    .line 61
    invoke-super {p0, p1}, Landroid/support/v7/widget/i;->verifyDrawable(Landroid/graphics/drawable/Drawable;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lco/uk/getmondo/common/ui/ProgressButton;->a:Landroid/graphics/drawable/AnimatedVectorDrawable;

    if-ne p1, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
