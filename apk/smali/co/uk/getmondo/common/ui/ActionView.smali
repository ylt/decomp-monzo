.class public final Lco/uk/getmondo/common/ui/ActionView;
.super Landroid/support/constraint/ConstraintLayout;
.source "ActionView.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0002\u0008\u0014\u0018\u00002\u00020\u0001B%\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\n\u0008\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u0012\u0008\u0008\u0002\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\u0008R(\u0010\u000b\u001a\u0004\u0018\u00010\n2\u0008\u0010\t\u001a\u0004\u0018\u00010\n@FX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u000c\u0010\r\"\u0004\u0008\u000e\u0010\u000fR(\u0010\u0010\u001a\u0004\u0018\u00010\n2\u0008\u0010\t\u001a\u0004\u0018\u00010\n@FX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u0011\u0010\r\"\u0004\u0008\u0012\u0010\u000fR&\u0010\u0013\u001a\u00020\u00072\u0006\u0010\t\u001a\u00020\u00078\u0006@FX\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u0014\u0010\u0015\"\u0004\u0008\u0016\u0010\u0017R(\u0010\u0018\u001a\u0004\u0018\u00010\n2\u0008\u0010\t\u001a\u0004\u0018\u00010\n@FX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u0019\u0010\r\"\u0004\u0008\u001a\u0010\u000fR&\u0010\u001b\u001a\u00020\u00072\u0006\u0010\t\u001a\u00020\u00078\u0006@FX\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u001c\u0010\u0015\"\u0004\u0008\u001d\u0010\u0017\u00a8\u0006\u001e"
    }
    d2 = {
        "Lco/uk/getmondo/common/ui/ActionView;",
        "Landroid/support/constraint/ConstraintLayout;",
        "context",
        "Landroid/content/Context;",
        "attrs",
        "Landroid/util/AttributeSet;",
        "defStyle",
        "",
        "(Landroid/content/Context;Landroid/util/AttributeSet;I)V",
        "value",
        "",
        "actionSubtitle",
        "getActionSubtitle",
        "()Ljava/lang/String;",
        "setActionSubtitle",
        "(Ljava/lang/String;)V",
        "actionTitle",
        "getActionTitle",
        "setActionTitle",
        "actionTitleColor",
        "getActionTitleColor",
        "()I",
        "setActionTitleColor",
        "(I)V",
        "iconUrl",
        "getIconUrl",
        "setIconUrl",
        "placeholderIcon",
        "getPlaceholderIcon",
        "setPlaceholderIcon",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field private c:Ljava/lang/String;

.field private d:I

.field private e:Ljava/lang/String;

.field private f:I

.field private g:Ljava/lang/String;

.field private h:Ljava/util/HashMap;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 6

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x6

    move-object v0, p0

    move-object v1, p1

    move-object v5, v2

    invoke-direct/range {v0 .. v5}, Lco/uk/getmondo/common/ui/ActionView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/d/b/i;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 6

    const/4 v3, 0x0

    const/4 v4, 0x4

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    invoke-direct/range {v0 .. v5}, Lco/uk/getmondo/common/ui/ActionView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/d/b/i;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 23
    invoke-direct {p0, p1, p2, p3}, Landroid/support/constraint/ConstraintLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 26
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f050133

    move-object v0, p0

    check-cast v0, Landroid/view/ViewGroup;

    invoke-virtual {v1, v2, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 27
    sget v0, Lco/uk/getmondo/c$a;->transactionActionIconImageView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/common/ui/ActionView;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setClipToOutline(Z)V

    .line 29
    if-eqz p2, :cond_0

    .line 30
    sget-object v0, Lco/uk/getmondo/c$b;->ActionView:[I

    invoke-virtual {p1, p2, v0, p3, v3}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 31
    invoke-virtual {v0, v3}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lco/uk/getmondo/common/ui/ActionView;->setActionTitle(Ljava/lang/String;)V

    .line 32
    invoke-virtual {v0, v4, v3}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v1

    invoke-virtual {p0, v1}, Lco/uk/getmondo/common/ui/ActionView;->setActionTitleColor(I)V

    .line 33
    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lco/uk/getmondo/common/ui/ActionView;->setActionSubtitle(Ljava/lang/String;)V

    .line 34
    const/4 v1, 0x3

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    invoke-virtual {p0, v1}, Lco/uk/getmondo/common/ui/ActionView;->setPlaceholderIcon(I)V

    .line 36
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 37
    nop

    nop

    .line 29
    :cond_0
    return-void
.end method

.method public synthetic constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/d/b/i;)V
    .locals 2

    .prologue
    and-int/lit8 v0, p4, 0x2

    if-eqz v0, :cond_1

    .line 21
    const/4 v0, 0x0

    check-cast v0, Landroid/util/AttributeSet;

    :goto_0
    and-int/lit8 v1, p4, 0x4

    if-eqz v1, :cond_0

    .line 22
    const/4 p3, 0x0

    :cond_0
    invoke-direct {p0, p1, v0, p3}, Lco/uk/getmondo/common/ui/ActionView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void

    :cond_1
    move-object v0, p2

    goto :goto_0
.end method


# virtual methods
.method public b(I)Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lco/uk/getmondo/common/ui/ActionView;->h:Ljava/util/HashMap;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lco/uk/getmondo/common/ui/ActionView;->h:Ljava/util/HashMap;

    :cond_0
    iget-object v0, p0, Lco/uk/getmondo/common/ui/ActionView;->h:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    if-nez v0, :cond_1

    invoke-virtual {p0, p1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/common/ui/ActionView;->h:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    return-object v0
.end method

.method public final getActionSubtitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lco/uk/getmondo/common/ui/ActionView;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final getActionTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lco/uk/getmondo/common/ui/ActionView;->c:Ljava/lang/String;

    return-object v0
.end method

.method public final getActionTitleColor()I
    .locals 1

    .prologue
    .line 46
    iget v0, p0, Lco/uk/getmondo/common/ui/ActionView;->d:I

    return v0
.end method

.method public final getIconUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lco/uk/getmondo/common/ui/ActionView;->g:Ljava/lang/String;

    return-object v0
.end method

.method public final getPlaceholderIcon()I
    .locals 1

    .prologue
    .line 63
    iget v0, p0, Lco/uk/getmondo/common/ui/ActionView;->f:I

    return v0
.end method

.method public final setActionSubtitle(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 54
    iput-object p1, p0, Lco/uk/getmondo/common/ui/ActionView;->e:Ljava/lang/String;

    .line 55
    if-eqz p1, :cond_0

    .line 56
    sget v0, Lco/uk/getmondo/c$a;->transactionActionSubtitleTextView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/common/ui/ActionView;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    check-cast p1, Ljava/lang/CharSequence;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 57
    sget v0, Lco/uk/getmondo/c$a;->transactionActionSubtitleTextView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/common/ui/ActionView;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-static {v0}, Lco/uk/getmondo/common/ae;->a(Landroid/view/View;)V

    .line 60
    :goto_0
    return-void

    .line 59
    :cond_0
    sget v0, Lco/uk/getmondo/c$a;->transactionActionSubtitleTextView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/common/ui/ActionView;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-static {v0}, Lco/uk/getmondo/common/ae;->b(Landroid/view/View;)V

    goto :goto_0
.end method

.method public final setActionTitle(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 42
    iput-object p1, p0, Lco/uk/getmondo/common/ui/ActionView;->c:Ljava/lang/String;

    .line 43
    sget v0, Lco/uk/getmondo/c$a;->transactionActionTitleTextView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/common/ui/ActionView;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    check-cast p1, Ljava/lang/CharSequence;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 44
    return-void
.end method

.method public final setActionTitleColor(I)V
    .locals 4

    .prologue
    .line 48
    if-eqz p1, :cond_0

    :goto_0
    iput p1, p0, Lco/uk/getmondo/common/ui/ActionView;->d:I

    .line 49
    sget v0, Lco/uk/getmondo/c$a;->transactionActionTitleTextView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/common/ui/ActionView;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {p0}, Lco/uk/getmondo/common/ui/ActionView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    iget v2, p0, Lco/uk/getmondo/common/ui/ActionView;->d:I

    invoke-virtual {p0}, Lco/uk/getmondo/common/ui/ActionView;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v3

    invoke-static {v1, v2, v3}, Landroid/support/v4/content/a/b;->b(Landroid/content/res/Resources;ILandroid/content/res/Resources$Theme;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 50
    return-void

    .line 48
    :cond_0
    const p1, 0x7f0f000f

    goto :goto_0
.end method

.method public final setIconUrl(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 71
    iput-object p1, p0, Lco/uk/getmondo/common/ui/ActionView;->g:Ljava/lang/String;

    .line 72
    invoke-virtual {p0}, Lco/uk/getmondo/common/ui/ActionView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/bumptech/glide/g;->b(Landroid/content/Context;)Lcom/bumptech/glide/j;

    move-result-object v0

    .line 73
    iget-object v1, p0, Lco/uk/getmondo/common/ui/ActionView;->g:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/bumptech/glide/j;->a(Ljava/lang/String;)Lcom/bumptech/glide/d;

    move-result-object v0

    .line 74
    iget v1, p0, Lco/uk/getmondo/common/ui/ActionView;->f:I

    invoke-virtual {v0, v1}, Lcom/bumptech/glide/d;->a(I)Lcom/bumptech/glide/c;

    move-result-object v0

    .line 75
    const v1, 0x3dcccccd    # 0.1f

    invoke-virtual {v0, v1}, Lcom/bumptech/glide/c;->a(F)Lcom/bumptech/glide/c;

    move-result-object v1

    .line 76
    sget v0, Lco/uk/getmondo/c$a;->transactionActionIconImageView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/common/ui/ActionView;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Lcom/bumptech/glide/c;->a(Landroid/widget/ImageView;)Lcom/bumptech/glide/g/b/j;

    .line 77
    return-void
.end method

.method public final setPlaceholderIcon(I)V
    .locals 2

    .prologue
    .line 65
    iput p1, p0, Lco/uk/getmondo/common/ui/ActionView;->f:I

    .line 66
    sget v0, Lco/uk/getmondo/c$a;->transactionActionIconImageView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/common/ui/ActionView;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iget v1, p0, Lco/uk/getmondo/common/ui/ActionView;->f:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 67
    return-void
.end method
