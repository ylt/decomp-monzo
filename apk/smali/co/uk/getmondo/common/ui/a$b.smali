.class public final Lco/uk/getmondo/common/ui/a$b;
.super Ljava/lang/Object;
.source "AvatarGenerator.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lco/uk/getmondo/common/ui/a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "b"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000,\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0006\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\u0018\u00002\u00020\u0001B\u0017\u0008\u0000\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J&\u0010\u000b\u001a\u00020\u000c2\u0008\u0008\u0002\u0010\r\u001a\u00020\u00052\u0008\u0008\u0002\u0010\u000e\u001a\u00020\u000f2\u0008\u0008\u0002\u0010\u0010\u001a\u00020\u0011H\u0007R\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0007\u0010\u0008R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\t\u0010\n\u00a8\u0006\u0012"
    }
    d2 = {
        "Lco/uk/getmondo/common/ui/AvatarGenerator$Creator;",
        "",
        "text",
        "",
        "color",
        "",
        "(Ljava/lang/String;I)V",
        "getColor",
        "()I",
        "getText",
        "()Ljava/lang/String;",
        "create",
        "Landroid/graphics/drawable/Drawable;",
        "fontSize",
        "typeface",
        "Landroid/graphics/Typeface;",
        "isSquare",
        "",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:I


# direct methods
.method public constructor <init>(Ljava/lang/String;I)V
    .locals 1

    .prologue
    const-string v0, "text"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lco/uk/getmondo/common/ui/a$b;->a:Ljava/lang/String;

    iput p2, p0, Lco/uk/getmondo/common/ui/a$b;->b:I

    return-void
.end method

.method public static synthetic a(Lco/uk/getmondo/common/ui/a$b;ILandroid/graphics/Typeface;ZILjava/lang/Object;)Landroid/graphics/drawable/Drawable;
    .locals 2

    .prologue
    const/4 v0, 0x0

    and-int/lit8 v1, p4, 0x1

    if-eqz v1, :cond_0

    .line 34
    const/16 v1, 0x14

    invoke-static {v1}, Lco/uk/getmondo/common/k/n;->b(I)I

    move-result p1

    :cond_0
    and-int/lit8 v1, p4, 0x2

    if-eqz v1, :cond_1

    .line 35
    const-string v1, "sans-serif-medium"

    invoke-static {v1, v0}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object p2

    const-string v1, "Typeface.create(\"sans-se\u2026medium\", Typeface.NORMAL)"

    invoke-static {p2, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    :cond_1
    and-int/lit8 v1, p4, 0x4

    if-eqz v1, :cond_2

    move p3, v0

    .line 36
    :cond_2
    invoke-virtual {p0, p1, p2, p3}, Lco/uk/getmondo/common/ui/a$b;->a(ILandroid/graphics/Typeface;Z)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(I)Landroid/graphics/drawable/Drawable;
    .locals 6

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x6

    move-object v0, p0

    move v1, p1

    move-object v5, v2

    invoke-static/range {v0 .. v5}, Lco/uk/getmondo/common/ui/a$b;->a(Lco/uk/getmondo/common/ui/a$b;ILandroid/graphics/Typeface;ZILjava/lang/Object;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    return-object v0
.end method

.method public final a(ILandroid/graphics/Typeface;Z)Landroid/graphics/drawable/Drawable;
    .locals 3

    .prologue
    const-string v0, "typeface"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 38
    iget v0, p0, Lco/uk/getmondo/common/ui/a$b;->b:I

    const v1, 0x3e4ccccd    # 0.2f

    invoke-static {v0, v1}, Lco/uk/getmondo/common/k/c;->a(IF)I

    move-result v0

    .line 39
    invoke-static {}, Lcom/a/a/a;->a()Lcom/a/a/a$d;

    move-result-object v1

    .line 40
    invoke-interface {v1}, Lcom/a/a/a$d;->b()Lcom/a/a/a$c;

    move-result-object v1

    .line 41
    invoke-interface {v1, p1}, Lcom/a/a/a$c;->b(I)Lcom/a/a/a$c;

    move-result-object v1

    .line 42
    invoke-interface {v1, p2}, Lcom/a/a/a$c;->a(Landroid/graphics/Typeface;)Lcom/a/a/a$c;

    move-result-object v1

    .line 43
    iget v2, p0, Lco/uk/getmondo/common/ui/a$b;->b:I

    invoke-interface {v1, v2}, Lcom/a/a/a$c;->a(I)Lcom/a/a/a$c;

    move-result-object v1

    .line 44
    invoke-interface {v1}, Lcom/a/a/a$c;->a()Lcom/a/a/a$c;

    move-result-object v1

    .line 45
    invoke-interface {v1}, Lcom/a/a/a$c;->c()Lcom/a/a/a$d;

    move-result-object v1

    .line 47
    if-eqz p3, :cond_0

    .line 49
    iget-object v2, p0, Lco/uk/getmondo/common/ui/a$b;->a:Ljava/lang/String;

    invoke-interface {v1, v2, v0}, Lcom/a/a/a$d;->a(Ljava/lang/String;I)Lcom/a/a/a;

    move-result-object v0

    const-string v1, "builder\n                \u2026ct(text, backgroundColor)"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/graphics/drawable/Drawable;

    .line 52
    :goto_0
    return-object v0

    :cond_0
    iget-object v2, p0, Lco/uk/getmondo/common/ui/a$b;->a:Ljava/lang/String;

    invoke-interface {v1, v2, v0}, Lcom/a/a/a$d;->b(Ljava/lang/String;I)Lcom/a/a/a;

    move-result-object v0

    const-string v1, "builder\n                \u2026nd(text, backgroundColor)"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/graphics/drawable/Drawable;

    goto :goto_0
.end method
