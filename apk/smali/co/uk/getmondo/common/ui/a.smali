.class public final Lco/uk/getmondo/common/ui/a;
.super Ljava/lang/Object;
.source "AvatarGenerator.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lco/uk/getmondo/common/ui/a$b;,
        Lco/uk/getmondo/common/ui/a$a;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000$\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010 \n\u0002\u0010\u0008\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0004\u0018\u0000 \u000b2\u00020\u0001:\u0002\u000b\u000cB\u0013\u0012\u000c\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003\u00a2\u0006\u0002\u0010\u0005J\u000e\u0010\u0006\u001a\u00020\u00072\u0006\u0010\u0008\u001a\u00020\tJ\u000e\u0010\n\u001a\u00020\u00042\u0006\u0010\u0008\u001a\u00020\tR\u0014\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\r"
    }
    d2 = {
        "Lco/uk/getmondo/common/ui/AvatarGenerator;",
        "",
        "colors",
        "",
        "",
        "(Ljava/util/List;)V",
        "from",
        "Lco/uk/getmondo/common/ui/AvatarGenerator$Creator;",
        "text",
        "",
        "generateColor",
        "Companion",
        "Creator",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# static fields
.field public static final a:Lco/uk/getmondo/common/ui/a$a;


# instance fields
.field private final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lco/uk/getmondo/common/ui/a$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lco/uk/getmondo/common/ui/a$a;-><init>(Lkotlin/d/b/i;)V

    sput-object v0, Lco/uk/getmondo/common/ui/a;->a:Lco/uk/getmondo/common/ui/a$a;

    return-void
.end method

.method public constructor <init>(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .prologue
    const-string v0, "colors"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lco/uk/getmondo/common/ui/a;->b:Ljava/util/List;

    return-void
.end method

.method public static final a(Landroid/content/Context;)Lco/uk/getmondo/common/ui/a;
    .locals 1

    const-string v0, "context"

    invoke-static {p0, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v0, Lco/uk/getmondo/common/ui/a;->a:Lco/uk/getmondo/common/ui/a$a;

    invoke-virtual {v0, p0}, Lco/uk/getmondo/common/ui/a$a;->a(Landroid/content/Context;)Lco/uk/getmondo/common/ui/a;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Lco/uk/getmondo/common/ui/a$b;
    .locals 3

    .prologue
    const-string v0, "text"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v0, p1

    .line 15
    check-cast v0, Ljava/lang/CharSequence;

    invoke-static {v0}, Lkotlin/h/j;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 16
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "\'Input is blank"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    :cond_0
    move-object v0, p1

    .line 18
    check-cast v0, Ljava/lang/CharSequence;

    invoke-static {v0}, Lkotlin/h/j;->f(Ljava/lang/CharSequence;)C

    move-result v0

    const/16 v1, 0x2b

    if-eq v0, v1, :cond_1

    move-object v0, p1

    check-cast v0, Ljava/lang/CharSequence;

    invoke-static {v0}, Lkotlin/h/j;->f(Ljava/lang/CharSequence;)C

    move-result v0

    invoke-static {v0}, Ljava/lang/Character;->isDigit(C)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    .line 19
    :goto_0
    new-instance v1, Lco/uk/getmondo/common/ui/a$b;

    if-eqz v0, :cond_3

    const-string v0, "#"

    :goto_1
    invoke-virtual {p0, p1}, Lco/uk/getmondo/common/ui/a;->b(Ljava/lang/String;)I

    move-result v2

    invoke-direct {v1, v0, v2}, Lco/uk/getmondo/common/ui/a$b;-><init>(Ljava/lang/String;I)V

    return-object v1

    .line 18
    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    :cond_3
    move-object v0, p1

    .line 19
    check-cast v0, Ljava/lang/CharSequence;

    invoke-static {v0}, Lkotlin/h/j;->f(Ljava/lang/CharSequence;)C

    move-result v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(C)Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method

.method public final b(Ljava/lang/String;)I
    .locals 4

    .prologue
    const/4 v1, 0x0

    const-string v0, "text"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 24
    check-cast p1, Ljava/lang/CharSequence;

    move v0, v1

    move v2, v1

    .line 76
    :goto_0
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v3

    if-ge v0, v3, :cond_0

    invoke-interface {p1, v0}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v3

    .line 24
    add-int/2addr v2, v3

    .line 76
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 25
    :cond_0
    iget-object v0, p0, Lco/uk/getmondo/common/ui/a;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    rem-int v0, v2, v0

    .line 26
    iget-object v2, p0, Lco/uk/getmondo/common/ui/a;->b:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ge v0, v2, :cond_1

    iget-object v1, p0, Lco/uk/getmondo/common/ui/a;->b:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Number;

    invoke-virtual {v0}, Ljava/lang/Number;->intValue()I

    move-result v0

    :goto_1
    return v0

    :cond_1
    iget-object v0, p0, Lco/uk/getmondo/common/ui/a;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Number;

    invoke-virtual {v0}, Ljava/lang/Number;->intValue()I

    move-result v0

    goto :goto_1
.end method
