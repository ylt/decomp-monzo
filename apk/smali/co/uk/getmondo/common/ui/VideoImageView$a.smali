.class final enum Lco/uk/getmondo/common/ui/VideoImageView$a;
.super Ljava/lang/Enum;
.source "VideoImageView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lco/uk/getmondo/common/ui/VideoImageView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lco/uk/getmondo/common/ui/VideoImageView$a;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lco/uk/getmondo/common/ui/VideoImageView$a;

.field public static final enum b:Lco/uk/getmondo/common/ui/VideoImageView$a;

.field public static final enum c:Lco/uk/getmondo/common/ui/VideoImageView$a;

.field public static final enum d:Lco/uk/getmondo/common/ui/VideoImageView$a;

.field private static final synthetic g:[Lco/uk/getmondo/common/ui/VideoImageView$a;


# instance fields
.field private final e:I

.field private final f:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 149
    new-instance v0, Lco/uk/getmondo/common/ui/VideoImageView$a;

    const-string v1, "FILL"

    invoke-direct {v0, v1, v2, v2, v5}, Lco/uk/getmondo/common/ui/VideoImageView$a;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lco/uk/getmondo/common/ui/VideoImageView$a;->a:Lco/uk/getmondo/common/ui/VideoImageView$a;

    .line 150
    new-instance v0, Lco/uk/getmondo/common/ui/VideoImageView$a;

    const-string v1, "FIT"

    invoke-direct {v0, v1, v3, v3, v2}, Lco/uk/getmondo/common/ui/VideoImageView$a;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lco/uk/getmondo/common/ui/VideoImageView$a;->b:Lco/uk/getmondo/common/ui/VideoImageView$a;

    .line 151
    new-instance v0, Lco/uk/getmondo/common/ui/VideoImageView$a;

    const-string v1, "FIXED_HEIGHT"

    invoke-direct {v0, v1, v4, v4, v4}, Lco/uk/getmondo/common/ui/VideoImageView$a;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lco/uk/getmondo/common/ui/VideoImageView$a;->c:Lco/uk/getmondo/common/ui/VideoImageView$a;

    .line 152
    new-instance v0, Lco/uk/getmondo/common/ui/VideoImageView$a;

    const-string v1, "FIXED_WIDTH"

    invoke-direct {v0, v1, v5, v5, v3}, Lco/uk/getmondo/common/ui/VideoImageView$a;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lco/uk/getmondo/common/ui/VideoImageView$a;->d:Lco/uk/getmondo/common/ui/VideoImageView$a;

    .line 148
    const/4 v0, 0x4

    new-array v0, v0, [Lco/uk/getmondo/common/ui/VideoImageView$a;

    sget-object v1, Lco/uk/getmondo/common/ui/VideoImageView$a;->a:Lco/uk/getmondo/common/ui/VideoImageView$a;

    aput-object v1, v0, v2

    sget-object v1, Lco/uk/getmondo/common/ui/VideoImageView$a;->b:Lco/uk/getmondo/common/ui/VideoImageView$a;

    aput-object v1, v0, v3

    sget-object v1, Lco/uk/getmondo/common/ui/VideoImageView$a;->c:Lco/uk/getmondo/common/ui/VideoImageView$a;

    aput-object v1, v0, v4

    sget-object v1, Lco/uk/getmondo/common/ui/VideoImageView$a;->d:Lco/uk/getmondo/common/ui/VideoImageView$a;

    aput-object v1, v0, v5

    sput-object v0, Lco/uk/getmondo/common/ui/VideoImageView$a;->g:[Lco/uk/getmondo/common/ui/VideoImageView$a;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;III)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 157
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 158
    iput p3, p0, Lco/uk/getmondo/common/ui/VideoImageView$a;->e:I

    .line 159
    iput p4, p0, Lco/uk/getmondo/common/ui/VideoImageView$a;->f:I

    .line 160
    return-void
.end method

.method static synthetic a(Lco/uk/getmondo/common/ui/VideoImageView$a;)I
    .locals 1

    .prologue
    .line 148
    iget v0, p0, Lco/uk/getmondo/common/ui/VideoImageView$a;->f:I

    return v0
.end method

.method static a(I)Lco/uk/getmondo/common/ui/VideoImageView$a;
    .locals 5

    .prologue
    .line 163
    invoke-static {}, Lco/uk/getmondo/common/ui/VideoImageView$a;->values()[Lco/uk/getmondo/common/ui/VideoImageView$a;

    move-result-object v1

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 164
    iget v4, v3, Lco/uk/getmondo/common/ui/VideoImageView$a;->e:I

    if-ne v4, p0, :cond_0

    .line 165
    return-object v3

    .line 163
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 168
    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unexpected ResizeMode id: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static valueOf(Ljava/lang/String;)Lco/uk/getmondo/common/ui/VideoImageView$a;
    .locals 1

    .prologue
    .line 148
    const-class v0, Lco/uk/getmondo/common/ui/VideoImageView$a;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/common/ui/VideoImageView$a;

    return-object v0
.end method

.method public static values()[Lco/uk/getmondo/common/ui/VideoImageView$a;
    .locals 1

    .prologue
    .line 148
    sget-object v0, Lco/uk/getmondo/common/ui/VideoImageView$a;->g:[Lco/uk/getmondo/common/ui/VideoImageView$a;

    invoke-virtual {v0}, [Lco/uk/getmondo/common/ui/VideoImageView$a;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lco/uk/getmondo/common/ui/VideoImageView$a;

    return-object v0
.end method
