.class public final Lco/uk/getmondo/common/q;
.super Ljava/lang/Object;
.source "IntercomService.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000:\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0008\u0007\u0018\u00002\u00020\u0001B!\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0008\u0008\u0001\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\u0008J\u0006\u0010\t\u001a\u00020\nJ\u000e\u0010\u000b\u001a\u00020\n2\u0006\u0010\u000c\u001a\u00020\rJ\u0006\u0010\u000e\u001a\u00020\nJ\u000e\u0010\u000f\u001a\u00020\n2\u0006\u0010\u0010\u001a\u00020\rJ\u0018\u0010\u0011\u001a\u00020\n2\u0006\u0010\u0012\u001a\u00020\u00132\u0008\u0010\u0014\u001a\u0004\u0018\u00010\u0015R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0016"
    }
    d2 = {
        "Lco/uk/getmondo/common/IntercomService;",
        "",
        "application",
        "Landroid/app/Application;",
        "monzoApi",
        "Lco/uk/getmondo/api/MonzoApi;",
        "ioScheduler",
        "Lio/reactivex/Scheduler;",
        "(Landroid/app/Application;Lco/uk/getmondo/api/MonzoApi;Lio/reactivex/Scheduler;)V",
        "clearRegisteredUser",
        "",
        "composeMessage",
        "message",
        "",
        "openIntercom",
        "registerUser",
        "userId",
        "updateUser",
        "profile",
        "Lco/uk/getmondo/model/Profile;",
        "account",
        "Lco/uk/getmondo/model/Account;",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field private final a:Lco/uk/getmondo/api/MonzoApi;

.field private final b:Lio/reactivex/u;


# direct methods
.method public constructor <init>(Landroid/app/Application;Lco/uk/getmondo/api/MonzoApi;Lio/reactivex/u;)V
    .locals 2

    .prologue
    const-string v0, "application"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "monzoApi"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "ioScheduler"

    invoke-static {p3, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lco/uk/getmondo/common/q;->a:Lco/uk/getmondo/api/MonzoApi;

    iput-object p3, p0, Lco/uk/getmondo/common/q;->b:Lio/reactivex/u;

    .line 26
    const-string v0, "android_sdk-33cefb6b165cfd2bc9843a6e33d8bb1201552534"

    const-string v1, "n3exvq9z"

    invoke-static {p1, v0, v1}, Lio/intercom/android/sdk/Intercom;->initialize(Landroid/app/Application;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 65
    invoke-static {}, Lio/intercom/android/sdk/Intercom;->client()Lio/intercom/android/sdk/Intercom;

    move-result-object v0

    invoke-virtual {v0}, Lio/intercom/android/sdk/Intercom;->displayConversationsList()V

    .line 66
    return-void
.end method

.method public final a(Lco/uk/getmondo/d/ac;Lco/uk/getmondo/d/a;)V
    .locals 4

    .prologue
    const-string v0, "profile"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 49
    sget-object v0, Lco/uk/getmondo/a;->b:Ljava/lang/Boolean;

    const-string v1, "BuildConfig.GHOST"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 62
    :goto_0
    return-void

    .line 53
    :cond_0
    new-instance v0, Lio/intercom/android/sdk/UserAttributes$Builder;

    invoke-direct {v0}, Lio/intercom/android/sdk/UserAttributes$Builder;-><init>()V

    .line 54
    invoke-virtual {p1}, Lco/uk/getmondo/d/ac;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/intercom/android/sdk/UserAttributes$Builder;->withName(Ljava/lang/String;)Lio/intercom/android/sdk/UserAttributes$Builder;

    move-result-object v0

    .line 55
    invoke-virtual {p1}, Lco/uk/getmondo/d/ac;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/intercom/android/sdk/UserAttributes$Builder;->withEmail(Ljava/lang/String;)Lio/intercom/android/sdk/UserAttributes$Builder;

    move-result-object v0

    .line 56
    if-eqz p2, :cond_1

    .line 57
    const-string v1, "account_id"

    invoke-interface {p2}, Lco/uk/getmondo/d/a;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lio/intercom/android/sdk/UserAttributes$Builder;->withCustomAttribute(Ljava/lang/String;Ljava/lang/Object;)Lio/intercom/android/sdk/UserAttributes$Builder;

    .line 60
    :cond_1
    invoke-static {}, Lio/intercom/android/sdk/Intercom;->client()Lio/intercom/android/sdk/Intercom;

    move-result-object v1

    invoke-virtual {v0}, Lio/intercom/android/sdk/UserAttributes$Builder;->build()Lio/intercom/android/sdk/UserAttributes;

    move-result-object v0

    invoke-virtual {v1, v0}, Lio/intercom/android/sdk/Intercom;->updateUser(Lio/intercom/android/sdk/UserAttributes;)V

    .line 61
    const-string v1, "Intercom user updated | name: %s email: %s accountID: %s"

    const/4 v0, 0x3

    new-array v2, v0, [Ljava/lang/Object;

    const/4 v0, 0x0

    invoke-virtual {p1}, Lco/uk/getmondo/d/ac;->c()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    const/4 v0, 0x1

    invoke-virtual {p1}, Lco/uk/getmondo/d/ac;->d()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    const/4 v3, 0x2

    if-eqz p2, :cond_2

    invoke-interface {p2}, Lco/uk/getmondo/d/a;->a()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    :goto_1
    aput-object v0, v2, v3

    invoke-static {v1, v2}, Ld/a/a;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    :cond_2
    const-string v0, ""

    goto :goto_1
.end method

.method public final a(Ljava/lang/String;)V
    .locals 4

    .prologue
    const-string v0, "userId"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 30
    sget-object v0, Lco/uk/getmondo/a;->b:Ljava/lang/Boolean;

    const-string v1, "BuildConfig.GHOST"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 46
    :goto_0
    return-void

    .line 34
    :cond_0
    invoke-static {}, Lio/intercom/android/sdk/identity/Registration;->create()Lio/intercom/android/sdk/identity/Registration;

    move-result-object v0

    invoke-virtual {v0, p1}, Lio/intercom/android/sdk/identity/Registration;->withUserId(Ljava/lang/String;)Lio/intercom/android/sdk/identity/Registration;

    move-result-object v0

    .line 35
    invoke-static {}, Lio/intercom/android/sdk/Intercom;->client()Lio/intercom/android/sdk/Intercom;

    move-result-object v1

    invoke-virtual {v1, v0}, Lio/intercom/android/sdk/Intercom;->registerIdentifiedUser(Lio/intercom/android/sdk/identity/Registration;)V

    .line 36
    const-string v1, "User registered with Intercom | userId: %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {v0}, Lio/intercom/android/sdk/identity/Registration;->getUserId()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v3

    invoke-static {v1, v2}, Ld/a/a;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 38
    iget-object v0, p0, Lco/uk/getmondo/common/q;->a:Lco/uk/getmondo/api/MonzoApi;

    const-string v1, "n3exvq9z"

    invoke-interface {v0, v1}, Lco/uk/getmondo/api/MonzoApi;->intercomUserHash(Ljava/lang/String;)Lio/reactivex/v;

    move-result-object v0

    .line 39
    const-wide/16 v2, 0x3

    invoke-virtual {v0, v2, v3}, Lio/reactivex/v;->a(J)Lio/reactivex/v;

    move-result-object v1

    .line 40
    sget-object v0, Lco/uk/getmondo/common/q$a;->a:Lco/uk/getmondo/common/q$a;

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {v1, v0}, Lio/reactivex/v;->d(Lio/reactivex/c/h;)Lio/reactivex/v;

    move-result-object v0

    .line 41
    iget-object v1, p0, Lco/uk/getmondo/common/q;->b:Lio/reactivex/u;

    invoke-virtual {v0, v1}, Lio/reactivex/v;->b(Lio/reactivex/u;)Lio/reactivex/v;

    move-result-object v2

    .line 42
    sget-object v0, Lco/uk/getmondo/common/q$b;->a:Lco/uk/getmondo/common/q$b;

    check-cast v0, Lio/reactivex/c/g;

    .line 45
    sget-object v1, Lco/uk/getmondo/common/q$c;->a:Lco/uk/getmondo/common/q$c;

    check-cast v1, Lio/reactivex/c/g;

    .line 42
    invoke-virtual {v2, v0, v1}, Lio/reactivex/v;->a(Lio/reactivex/c/g;Lio/reactivex/c/g;)Lio/reactivex/b/b;

    goto :goto_0
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 73
    invoke-static {}, Lio/intercom/android/sdk/Intercom;->client()Lio/intercom/android/sdk/Intercom;

    move-result-object v0

    invoke-virtual {v0}, Lio/intercom/android/sdk/Intercom;->reset()V

    .line 74
    return-void
.end method

.method public final b(Ljava/lang/String;)V
    .locals 1

    .prologue
    const-string v0, "message"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 69
    invoke-static {}, Lio/intercom/android/sdk/Intercom;->client()Lio/intercom/android/sdk/Intercom;

    move-result-object v0

    invoke-virtual {v0, p1}, Lio/intercom/android/sdk/Intercom;->displayMessageComposer(Ljava/lang/String;)V

    .line 70
    return-void
.end method
