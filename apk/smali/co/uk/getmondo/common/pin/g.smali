.class public final Lco/uk/getmondo/common/pin/g;
.super Ljava/lang/Object;
.source "PinModule.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0008\u0007\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0008\u0010\u0005\u001a\u00020\u0003H\u0007R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0006"
    }
    d2 = {
        "Lco/uk/getmondo/common/pin/PinModule;",
        "",
        "operation",
        "Lco/uk/getmondo/common/pin/data/model/PinOperation;",
        "(Lco/uk/getmondo/common/pin/data/model/PinOperation;)V",
        "providePinOperation",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field private final a:Lco/uk/getmondo/common/pin/a/a/b;


# direct methods
.method public constructor <init>(Lco/uk/getmondo/common/pin/a/a/b;)V
    .locals 1

    .prologue
    const-string v0, "operation"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lco/uk/getmondo/common/pin/g;->a:Lco/uk/getmondo/common/pin/a/a/b;

    return-void
.end method


# virtual methods
.method public final a()Lco/uk/getmondo/common/pin/a/a/b;
    .locals 1

    .prologue
    .line 14
    iget-object v0, p0, Lco/uk/getmondo/common/pin/g;->a:Lco/uk/getmondo/common/pin/a/a/b;

    return-object v0
.end method
