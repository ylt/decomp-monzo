.class public final Lco/uk/getmondo/common/pin/PinEntryActivity;
.super Lco/uk/getmondo/common/activities/b;
.source "PinEntryActivity.kt"

# interfaces
.implements Lco/uk/getmondo/common/pin/c$a;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lco/uk/getmondo/common/pin/PinEntryActivity$a;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000N\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0006\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0010\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u0008\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0002\u0008\u0008\u0018\u0000 (2\u00020\u00012\u00020\u0002:\u0001(B\u0005\u00a2\u0006\u0002\u0010\u0003J\u0008\u0010\u0011\u001a\u00020\u0012H\u0016J\u0008\u0010\u0013\u001a\u00020\u0012H\u0016J\"\u0010\u0014\u001a\u00020\u00122\u0006\u0010\u0015\u001a\u00020\u00162\u0006\u0010\u0017\u001a\u00020\u00162\u0008\u0010\u0018\u001a\u0004\u0018\u00010\u0019H\u0014J\u0012\u0010\u001a\u001a\u00020\u00122\u0008\u0010\u001b\u001a\u0004\u0018\u00010\u001cH\u0014J\u000e\u0010\u001d\u001a\u0008\u0012\u0004\u0012\u00020\u00120\u001eH\u0016J\u000e\u0010\u001f\u001a\u0008\u0012\u0004\u0012\u00020\u00120\u001eH\u0016J\u000e\u0010 \u001a\u0008\u0012\u0004\u0012\u00020!0\u001eH\u0016J\u0010\u0010\"\u001a\u00020\u00122\u0006\u0010\u0004\u001a\u00020\u0005H\u0016J\u0008\u0010#\u001a\u00020\u0012H\u0016J\u0008\u0010$\u001a\u00020\u0012H\u0016J\u0008\u0010%\u001a\u00020\u0012H\u0016J\u0008\u0010&\u001a\u00020\u0012H\u0016J\u0008\u0010\'\u001a\u00020\u0012H\u0016R#\u0010\u0004\u001a\n \u0006*\u0004\u0018\u00010\u00050\u00058BX\u0082\u0084\u0002\u00a2\u0006\u000c\n\u0004\u0008\t\u0010\n\u001a\u0004\u0008\u0007\u0010\u0008R\u001e\u0010\u000b\u001a\u00020\u000c8\u0006@\u0006X\u0087.\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\r\u0010\u000e\"\u0004\u0008\u000f\u0010\u0010\u00a8\u0006)"
    }
    d2 = {
        "Lco/uk/getmondo/common/pin/PinEntryActivity;",
        "Lco/uk/getmondo/common/activities/BaseActivity;",
        "Lco/uk/getmondo/common/pin/PinEntryPresenter$View;",
        "()V",
        "operation",
        "Lco/uk/getmondo/common/pin/data/model/PinOperation;",
        "kotlin.jvm.PlatformType",
        "getOperation",
        "()Lco/uk/getmondo/common/pin/data/model/PinOperation;",
        "operation$delegate",
        "Lkotlin/Lazy;",
        "presenter",
        "Lco/uk/getmondo/common/pin/PinEntryPresenter;",
        "getPresenter",
        "()Lco/uk/getmondo/common/pin/PinEntryPresenter;",
        "setPresenter",
        "(Lco/uk/getmondo/common/pin/PinEntryPresenter;)V",
        "hideLoading",
        "",
        "hidePinError",
        "onActivityResult",
        "requestCode",
        "",
        "resultCode",
        "data",
        "Landroid/content/Intent;",
        "onCreate",
        "savedInstanceState",
        "Landroid/os/Bundle;",
        "onForgotPin",
        "Lio/reactivex/Observable;",
        "onPinClicked",
        "onPinEntered",
        "",
        "openConfirmation",
        "showForgotPin",
        "showGenericError",
        "showLoading",
        "showPinBlocked",
        "showPinError",
        "Companion",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# static fields
.field static final synthetic a:[Lkotlin/reflect/l;

.field public static final c:Lco/uk/getmondo/common/pin/PinEntryActivity$a;


# instance fields
.field public b:Lco/uk/getmondo/common/pin/c;

.field private final e:Lkotlin/c;

.field private f:Ljava/util/HashMap;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const/4 v0, 0x1

    new-array v1, v0, [Lkotlin/reflect/l;

    const/4 v2, 0x0

    new-instance v0, Lkotlin/d/b/w;

    const-class v3, Lco/uk/getmondo/common/pin/PinEntryActivity;

    invoke-static {v3}, Lkotlin/d/b/y;->a(Ljava/lang/Class;)Lkotlin/reflect/c;

    move-result-object v3

    const-string v4, "operation"

    const-string v5, "getOperation()Lco/uk/getmondo/common/pin/data/model/PinOperation;"

    invoke-direct {v0, v3, v4, v5}, Lkotlin/d/b/w;-><init>(Lkotlin/reflect/e;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v0}, Lkotlin/d/b/y;->a(Lkotlin/d/b/v;)Lkotlin/reflect/n;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/l;

    aput-object v0, v1, v2

    sput-object v1, Lco/uk/getmondo/common/pin/PinEntryActivity;->a:[Lkotlin/reflect/l;

    new-instance v0, Lco/uk/getmondo/common/pin/PinEntryActivity$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lco/uk/getmondo/common/pin/PinEntryActivity$a;-><init>(Lkotlin/d/b/i;)V

    sput-object v0, Lco/uk/getmondo/common/pin/PinEntryActivity;->c:Lco/uk/getmondo/common/pin/PinEntryActivity$a;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 20
    invoke-direct {p0}, Lco/uk/getmondo/common/activities/b;-><init>()V

    .line 24
    new-instance v0, Lco/uk/getmondo/common/pin/PinEntryActivity$c;

    invoke-direct {v0, p0}, Lco/uk/getmondo/common/pin/PinEntryActivity$c;-><init>(Lco/uk/getmondo/common/pin/PinEntryActivity;)V

    check-cast v0, Lkotlin/d/a/a;

    invoke-static {v0}, Lkotlin/d;->a(Lkotlin/d/a/a;)Lkotlin/c;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/common/pin/PinEntryActivity;->e:Lkotlin/c;

    return-void
.end method

.method private final k()Lco/uk/getmondo/common/pin/a/a/b;
    .locals 3

    iget-object v0, p0, Lco/uk/getmondo/common/pin/PinEntryActivity;->e:Lkotlin/c;

    sget-object v1, Lco/uk/getmondo/common/pin/PinEntryActivity;->a:[Lkotlin/reflect/l;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-interface {v0}, Lkotlin/c;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/common/pin/a/a/b;

    return-object v0
.end method


# virtual methods
.method public a(I)Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lco/uk/getmondo/common/pin/PinEntryActivity;->f:Ljava/util/HashMap;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lco/uk/getmondo/common/pin/PinEntryActivity;->f:Ljava/util/HashMap;

    :cond_0
    iget-object v0, p0, Lco/uk/getmondo/common/pin/PinEntryActivity;->f:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    if-nez v0, :cond_1

    invoke-virtual {p0, p1}, Landroid/support/v4/app/j;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/common/pin/PinEntryActivity;->f:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    return-object v0
.end method

.method public a()Lio/reactivex/n;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Lkotlin/n;",
            ">;"
        }
    .end annotation

    .prologue
    .line 47
    sget v0, Lco/uk/getmondo/c$a;->pinEntry:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/common/pin/PinEntryActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/common/ui/PinEntryView;

    .line 117
    invoke-static {v0}, Lcom/b/a/c/c;->a(Landroid/view/View;)Lio/reactivex/n;

    move-result-object v1

    sget-object v0, Lcom/b/a/a/d;->a:Lcom/b/a/a/d;

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {v1, v0}, Lio/reactivex/n;->map(Lio/reactivex/c/h;)Lio/reactivex/n;

    move-result-object v0

    const-string v1, "RxView.clicks(this).map(VoidToUnit)"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public a(Lco/uk/getmondo/common/pin/a/a/b;)V
    .locals 4

    .prologue
    const-string v0, "operation"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 98
    instance-of v0, p1, Lco/uk/getmondo/common/pin/a/a/b$a;

    if-eqz v0, :cond_0

    move-object v0, p0

    .line 99
    check-cast v0, Landroid/content/Context;

    .line 100
    const v1, 0x7f0a00d7

    invoke-virtual {p0, v1}, Lco/uk/getmondo/common/pin/PinEntryActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    const v2, 0x7f0a00d6

    invoke-virtual {p0, v2}, Lco/uk/getmondo/common/pin/PinEntryActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 101
    invoke-virtual {p1}, Lco/uk/getmondo/common/pin/a/a/b;->a()Landroid/content/Intent;

    move-result-object v3

    .line 99
    invoke-static {v0, v1, v2, v3}, Lco/uk/getmondo/common/activities/ConfirmationActivity;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Landroid/content/Intent;)V

    .line 103
    :cond_0
    return-void
.end method

.method public b()Lio/reactivex/n;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 51
    new-instance v0, Lco/uk/getmondo/common/pin/PinEntryActivity$b;

    invoke-direct {v0, p0}, Lco/uk/getmondo/common/pin/PinEntryActivity$b;-><init>(Lco/uk/getmondo/common/pin/PinEntryActivity;)V

    check-cast v0, Lio/reactivex/p;

    invoke-static {v0}, Lio/reactivex/n;->create(Lio/reactivex/p;)Lio/reactivex/n;

    move-result-object v0

    const-string v1, "Observable.create { emit\u2026istener(null) }\n        }"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public c()Lio/reactivex/n;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Lkotlin/n;",
            ">;"
        }
    .end annotation

    .prologue
    .line 58
    sget v0, Lco/uk/getmondo/c$a;->pinActionView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/common/pin/PinEntryActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 118
    invoke-static {v0}, Lcom/b/a/c/c;->a(Landroid/view/View;)Lio/reactivex/n;

    move-result-object v1

    sget-object v0, Lcom/b/a/a/d;->a:Lcom/b/a/a/d;

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {v1, v0}, Lio/reactivex/n;->map(Lio/reactivex/c/h;)Lio/reactivex/n;

    move-result-object v0

    const-string v1, "RxView.clicks(this).map(VoidToUnit)"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public d()V
    .locals 2

    .prologue
    .line 62
    const v0, 0x7f0a0182

    invoke-virtual {p0, v0}, Lco/uk/getmondo/common/pin/PinEntryActivity;->b(I)V

    .line 63
    sget v0, Lco/uk/getmondo/c$a;->pinEntry:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/common/pin/PinEntryActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/common/ui/PinEntryView;

    invoke-virtual {v0}, Lco/uk/getmondo/common/ui/PinEntryView;->a()V

    .line 64
    sget v0, Lco/uk/getmondo/c$a;->pinEntry:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/common/pin/PinEntryActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/common/ui/PinEntryView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lco/uk/getmondo/common/ui/PinEntryView;->a(Z)V

    .line 65
    return-void
.end method

.method public e()V
    .locals 1

    .prologue
    .line 68
    sget v0, Lco/uk/getmondo/c$a;->pinEntry:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/common/pin/PinEntryActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/common/ui/PinEntryView;

    invoke-virtual {v0}, Lco/uk/getmondo/common/ui/PinEntryView;->d()V

    .line 69
    return-void
.end method

.method public f()V
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 72
    sget v0, Lco/uk/getmondo/c$a;->pinEntry:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/common/pin/PinEntryActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/common/ui/PinEntryView;

    invoke-virtual {v0, v1}, Lco/uk/getmondo/common/ui/PinEntryView;->a(Z)V

    .line 73
    invoke-static {v1}, Lco/uk/getmondo/common/d/e;->a(Z)Lco/uk/getmondo/common/d/e;

    move-result-object v0

    .line 74
    invoke-virtual {p0}, Lco/uk/getmondo/common/pin/PinEntryActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const-class v2, Lco/uk/getmondo/common/d/e;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lco/uk/getmondo/common/d/e;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    .line 75
    return-void
.end method

.method public g()V
    .locals 1

    .prologue
    .line 78
    const v0, 0x7f0a0196

    invoke-virtual {p0, v0}, Lco/uk/getmondo/common/pin/PinEntryActivity;->b(I)V

    .line 79
    return-void
.end method

.method public h()V
    .locals 3

    .prologue
    .line 82
    new-instance v1, Landroid/content/Intent;

    move-object v0, p0

    check-cast v0, Landroid/content/Context;

    const-class v2, Lco/uk/getmondo/pin/ForgotPinActivity;

    invoke-direct {v1, v0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/4 v0, 0x1

    invoke-virtual {p0, v1, v0}, Lco/uk/getmondo/common/pin/PinEntryActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 83
    return-void
.end method

.method public i()V
    .locals 1

    .prologue
    .line 86
    sget v0, Lco/uk/getmondo/c$a;->pinEntry:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/common/pin/PinEntryActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/common/ui/PinEntryView;

    invoke-static {v0}, Lco/uk/getmondo/common/ae;->c(Landroid/view/View;)V

    .line 87
    sget v0, Lco/uk/getmondo/c$a;->progressOverlay:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/common/pin/PinEntryActivity;->a(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Lco/uk/getmondo/common/ae;->a(Landroid/view/View;)V

    .line 88
    sget v0, Lco/uk/getmondo/c$a;->progressBar:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/common/pin/PinEntryActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    invoke-static {v0}, Lco/uk/getmondo/common/ae;->a(Landroid/view/View;)V

    .line 89
    return-void
.end method

.method public j()V
    .locals 1

    .prologue
    .line 92
    sget v0, Lco/uk/getmondo/c$a;->progressBar:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/common/pin/PinEntryActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    invoke-static {v0}, Lco/uk/getmondo/common/ae;->b(Landroid/view/View;)V

    .line 93
    sget v0, Lco/uk/getmondo/c$a;->progressOverlay:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/common/pin/PinEntryActivity;->a(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Lco/uk/getmondo/common/ae;->b(Landroid/view/View;)V

    .line 94
    return-void
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 1

    .prologue
    .line 37
    const/4 v0, 0x1

    if-ne p1, v0, :cond_1

    .line 38
    if-eqz p3, :cond_0

    const-string v0, "pin_error_msg"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 39
    const-string v0, "pin_error_msg"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lco/uk/getmondo/common/pin/PinEntryActivity;->b(Ljava/lang/String;)V

    .line 44
    :cond_0
    :goto_0
    return-void

    .line 43
    :cond_1
    invoke-super {p0, p1, p2, p3}, Lco/uk/getmondo/common/activities/b;->onActivityResult(IILandroid/content/Intent;)V

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 27
    invoke-super {p0, p1}, Lco/uk/getmondo/common/activities/b;->onCreate(Landroid/os/Bundle;)V

    .line 28
    const v0, 0x7f050056

    invoke-virtual {p0, v0}, Lco/uk/getmondo/common/pin/PinEntryActivity;->setContentView(I)V

    .line 30
    invoke-virtual {p0}, Lco/uk/getmondo/common/pin/PinEntryActivity;->l()Lco/uk/getmondo/common/h/b/b;

    move-result-object v0

    new-instance v1, Lco/uk/getmondo/common/pin/g;

    invoke-direct {p0}, Lco/uk/getmondo/common/pin/PinEntryActivity;->k()Lco/uk/getmondo/common/pin/a/a/b;

    move-result-object v2

    const-string v3, "operation"

    invoke-static {v2, v3}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v1, v2}, Lco/uk/getmondo/common/pin/g;-><init>(Lco/uk/getmondo/common/pin/a/a/b;)V

    invoke-interface {v0, v1}, Lco/uk/getmondo/common/h/b/b;->a(Lco/uk/getmondo/common/pin/g;)Lco/uk/getmondo/common/pin/a;

    move-result-object v0

    .line 31
    invoke-interface {v0, p0}, Lco/uk/getmondo/common/pin/a;->a(Lco/uk/getmondo/common/pin/PinEntryActivity;)V

    .line 33
    iget-object v0, p0, Lco/uk/getmondo/common/pin/PinEntryActivity;->b:Lco/uk/getmondo/common/pin/c;

    if-nez v0, :cond_0

    const-string v1, "presenter"

    invoke-static {v1}, Lkotlin/d/b/l;->b(Ljava/lang/String;)V

    :cond_0
    check-cast p0, Lco/uk/getmondo/common/pin/c$a;

    invoke-virtual {v0, p0}, Lco/uk/getmondo/common/pin/c;->a(Lco/uk/getmondo/common/pin/c$a;)V

    .line 34
    return-void
.end method
