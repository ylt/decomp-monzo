.class final Lco/uk/getmondo/common/pin/c$d;
.super Ljava/lang/Object;
.source "PinEntryPresenter.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lco/uk/getmondo/common/pin/c;->a(Lco/uk/getmondo/common/pin/c$a;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h",
        "<TT;",
        "Lio/reactivex/l",
        "<+TR;>;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000\u0012\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\u0010\u0000\u001a\u0008\u0012\u0004\u0012\u00020\u00020\u00012\u0006\u0010\u0003\u001a\u00020\u0004H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "Lio/reactivex/Maybe;",
        "",
        "it",
        "",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field final synthetic a:Lco/uk/getmondo/common/pin/c;

.field final synthetic b:Lco/uk/getmondo/common/pin/c$a;


# direct methods
.method constructor <init>(Lco/uk/getmondo/common/pin/c;Lco/uk/getmondo/common/pin/c$a;)V
    .locals 0

    iput-object p1, p0, Lco/uk/getmondo/common/pin/c$d;->a:Lco/uk/getmondo/common/pin/c;

    iput-object p2, p0, Lco/uk/getmondo/common/pin/c$d;->b:Lco/uk/getmondo/common/pin/c$a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Lio/reactivex/h;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/h",
            "<",
            "Lkotlin/n;",
            ">;"
        }
    .end annotation

    .prologue
    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 46
    iget-object v0, p0, Lco/uk/getmondo/common/pin/c$d;->a:Lco/uk/getmondo/common/pin/c;

    invoke-static {v0}, Lco/uk/getmondo/common/pin/c;->a(Lco/uk/getmondo/common/pin/c;)Lco/uk/getmondo/common/pin/a/a/b;

    move-result-object v0

    .line 47
    instance-of v0, v0, Lco/uk/getmondo/common/pin/a/a/b$a;

    if-eqz v0, :cond_0

    .line 48
    iget-object v0, p0, Lco/uk/getmondo/common/pin/c$d;->a:Lco/uk/getmondo/common/pin/c;

    invoke-static {v0}, Lco/uk/getmondo/common/pin/c;->b(Lco/uk/getmondo/common/pin/c;)Lco/uk/getmondo/profile/data/a;

    move-result-object v1

    iget-object v0, p0, Lco/uk/getmondo/common/pin/c$d;->a:Lco/uk/getmondo/common/pin/c;

    invoke-static {v0}, Lco/uk/getmondo/common/pin/c;->a(Lco/uk/getmondo/common/pin/c;)Lco/uk/getmondo/common/pin/a/a/b;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/common/pin/a/a/b$a;

    invoke-virtual {v0}, Lco/uk/getmondo/common/pin/a/a/b$a;->c()Lco/uk/getmondo/d/s;

    move-result-object v0

    sget-object v2, Lco/uk/getmondo/common/pin/a/a/a;->a:Lco/uk/getmondo/common/pin/a/a/a$b;

    invoke-virtual {v2, p1}, Lco/uk/getmondo/common/pin/a/a/a$b;->a(Ljava/lang/String;)Lco/uk/getmondo/common/pin/a/a/a;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lco/uk/getmondo/profile/data/a;->a(Lco/uk/getmondo/d/s;Lco/uk/getmondo/common/pin/a/a/a;)Lio/reactivex/b;

    move-result-object v0

    .line 49
    iget-object v1, p0, Lco/uk/getmondo/common/pin/c$d;->a:Lco/uk/getmondo/common/pin/c;

    invoke-static {v1}, Lco/uk/getmondo/common/pin/c;->c(Lco/uk/getmondo/common/pin/c;)Lio/reactivex/u;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/b;->b(Lio/reactivex/u;)Lio/reactivex/b;

    move-result-object v0

    .line 50
    iget-object v1, p0, Lco/uk/getmondo/common/pin/c$d;->a:Lco/uk/getmondo/common/pin/c;

    invoke-static {v1}, Lco/uk/getmondo/common/pin/c;->d(Lco/uk/getmondo/common/pin/c;)Lio/reactivex/u;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/b;->a(Lio/reactivex/u;)Lio/reactivex/b;

    move-result-object v1

    .line 51
    new-instance v0, Lco/uk/getmondo/common/pin/c$d$1;

    invoke-direct {v0, p0}, Lco/uk/getmondo/common/pin/c$d$1;-><init>(Lco/uk/getmondo/common/pin/c$d;)V

    check-cast v0, Lio/reactivex/c/g;

    invoke-virtual {v1, v0}, Lio/reactivex/b;->c(Lio/reactivex/c/g;)Lio/reactivex/b;

    move-result-object v1

    .line 52
    new-instance v0, Lco/uk/getmondo/common/pin/c$d$2;

    invoke-direct {v0, p0}, Lco/uk/getmondo/common/pin/c$d$2;-><init>(Lco/uk/getmondo/common/pin/c$d;)V

    check-cast v0, Lio/reactivex/c/g;

    invoke-virtual {v1, v0}, Lio/reactivex/b;->b(Lio/reactivex/c/g;)Lio/reactivex/b;

    move-result-object v1

    .line 53
    new-instance v0, Lco/uk/getmondo/common/pin/c$d$3;

    invoke-direct {v0, p0}, Lco/uk/getmondo/common/pin/c$d$3;-><init>(Lco/uk/getmondo/common/pin/c$d;)V

    check-cast v0, Lio/reactivex/c/a;

    invoke-virtual {v1, v0}, Lio/reactivex/b;->b(Lio/reactivex/c/a;)Lio/reactivex/b;

    move-result-object v1

    .line 56
    new-instance v0, Lco/uk/getmondo/common/pin/c$d$4;

    invoke-direct {v0, p0}, Lco/uk/getmondo/common/pin/c$d$4;-><init>(Lco/uk/getmondo/common/pin/c$d;)V

    check-cast v0, Lio/reactivex/c/g;

    invoke-virtual {v1, v0}, Lio/reactivex/b;->a(Lio/reactivex/c/g;)Lio/reactivex/b;

    move-result-object v0

    .line 60
    sget-object v1, Lkotlin/n;->a:Lkotlin/n;

    invoke-static {v0, v1}, Lco/uk/getmondo/common/j/f;->a(Lio/reactivex/b;Ljava/lang/Object;)Lio/reactivex/h;

    move-result-object v0

    .line 46
    return-object v0

    .line 60
    :cond_0
    new-instance v0, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {v0}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw v0
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 26
    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1}, Lco/uk/getmondo/common/pin/c$d;->a(Ljava/lang/String;)Lio/reactivex/h;

    move-result-object v0

    return-object v0
.end method
