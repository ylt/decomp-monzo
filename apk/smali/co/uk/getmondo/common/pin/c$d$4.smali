.class final Lco/uk/getmondo/common/pin/c$d$4;
.super Ljava/lang/Object;
.source "PinEntryPresenter.kt"

# interfaces
.implements Lio/reactivex/c/g;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lco/uk/getmondo/common/pin/c$d;->a(Ljava/lang/String;)Lio/reactivex/h;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/g",
        "<",
        "Ljava/lang/Throwable;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u0003\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "",
        "it",
        "",
        "kotlin.jvm.PlatformType",
        "accept"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field final synthetic a:Lco/uk/getmondo/common/pin/c$d;


# direct methods
.method constructor <init>(Lco/uk/getmondo/common/pin/c$d;)V
    .locals 0

    iput-object p1, p0, Lco/uk/getmondo/common/pin/c$d$4;->a:Lco/uk/getmondo/common/pin/c$d;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 26
    check-cast p1, Ljava/lang/Throwable;

    invoke-virtual {p0, p1}, Lco/uk/getmondo/common/pin/c$d$4;->a(Ljava/lang/Throwable;)V

    return-void
.end method

.method public final a(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 57
    iget-object v0, p0, Lco/uk/getmondo/common/pin/c$d$4;->a:Lco/uk/getmondo/common/pin/c$d;

    iget-object v0, v0, Lco/uk/getmondo/common/pin/c$d;->a:Lco/uk/getmondo/common/pin/c;

    invoke-static {v0}, Lco/uk/getmondo/common/pin/c;->e(Lco/uk/getmondo/common/pin/c;)Lco/uk/getmondo/common/a;

    move-result-object v0

    sget-object v1, Lco/uk/getmondo/api/model/tracking/Impression;->Companion:Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->m(Z)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v1

    invoke-virtual {v0, v1}, Lco/uk/getmondo/common/a;->a(Lco/uk/getmondo/api/model/tracking/Impression;)V

    .line 58
    iget-object v0, p0, Lco/uk/getmondo/common/pin/c$d$4;->a:Lco/uk/getmondo/common/pin/c$d;

    iget-object v0, v0, Lco/uk/getmondo/common/pin/c$d;->a:Lco/uk/getmondo/common/pin/c;

    const-string v1, "it"

    invoke-static {p1, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v1, p0, Lco/uk/getmondo/common/pin/c$d$4;->a:Lco/uk/getmondo/common/pin/c$d;

    iget-object v1, v1, Lco/uk/getmondo/common/pin/c$d;->b:Lco/uk/getmondo/common/pin/c$a;

    invoke-static {v0, p1, v1}, Lco/uk/getmondo/common/pin/c;->a(Lco/uk/getmondo/common/pin/c;Ljava/lang/Throwable;Lco/uk/getmondo/common/pin/c$a;)V

    .line 59
    return-void
.end method
