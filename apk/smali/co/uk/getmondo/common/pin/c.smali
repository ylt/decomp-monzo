.class public final Lco/uk/getmondo/common/pin/c;
.super Lco/uk/getmondo/common/ui/b;
.source "PinEntryPresenter.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lco/uk/getmondo/common/pin/c$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lco/uk/getmondo/common/ui/b",
        "<",
        "Lco/uk/getmondo/common/pin/c$a;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000>\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u0003\n\u0002\u0008\u0004\u0008\u0007\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00020\u0001:\u0001\u0015B;\u0008\u0007\u0012\u0008\u0008\u0001\u0010\u0003\u001a\u00020\u0004\u0012\u0008\u0008\u0001\u0010\u0005\u001a\u00020\u0004\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b\u0012\u0006\u0010\u000c\u001a\u00020\r\u00a2\u0006\u0002\u0010\u000eJ\u0018\u0010\u000f\u001a\u00020\u00102\u0006\u0010\u0011\u001a\u00020\u00122\u0006\u0010\u0013\u001a\u00020\u0002H\u0002J\u0010\u0010\u0014\u001a\u00020\u00102\u0006\u0010\u0013\u001a\u00020\u0002H\u0016R\u000e\u0010\u000c\u001a\u00020\rX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0016"
    }
    d2 = {
        "Lco/uk/getmondo/common/pin/PinEntryPresenter;",
        "Lco/uk/getmondo/common/ui/BasePresenter;",
        "Lco/uk/getmondo/common/pin/PinEntryPresenter$View;",
        "ioScheduler",
        "Lio/reactivex/Scheduler;",
        "uiScheduler",
        "apiErrorHandler",
        "Lco/uk/getmondo/common/errors/ApiErrorHandler;",
        "operation",
        "Lco/uk/getmondo/common/pin/data/model/PinOperation;",
        "profileManager",
        "Lco/uk/getmondo/profile/data/ProfileManager;",
        "analytics",
        "Lco/uk/getmondo/common/AnalyticsService;",
        "(Lio/reactivex/Scheduler;Lio/reactivex/Scheduler;Lco/uk/getmondo/common/errors/ApiErrorHandler;Lco/uk/getmondo/common/pin/data/model/PinOperation;Lco/uk/getmondo/profile/data/ProfileManager;Lco/uk/getmondo/common/AnalyticsService;)V",
        "handleError",
        "",
        "throwable",
        "",
        "view",
        "register",
        "View",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field private final c:Lio/reactivex/u;

.field private final d:Lio/reactivex/u;

.field private final e:Lco/uk/getmondo/common/e/a;

.field private final f:Lco/uk/getmondo/common/pin/a/a/b;

.field private final g:Lco/uk/getmondo/profile/data/a;

.field private final h:Lco/uk/getmondo/common/a;


# direct methods
.method public constructor <init>(Lio/reactivex/u;Lio/reactivex/u;Lco/uk/getmondo/common/e/a;Lco/uk/getmondo/common/pin/a/a/b;Lco/uk/getmondo/profile/data/a;Lco/uk/getmondo/common/a;)V
    .locals 1

    .prologue
    const-string v0, "ioScheduler"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "uiScheduler"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "apiErrorHandler"

    invoke-static {p3, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "operation"

    invoke-static {p4, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "profileManager"

    invoke-static {p5, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "analytics"

    invoke-static {p6, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 34
    invoke-direct {p0}, Lco/uk/getmondo/common/ui/b;-><init>()V

    iput-object p1, p0, Lco/uk/getmondo/common/pin/c;->c:Lio/reactivex/u;

    iput-object p2, p0, Lco/uk/getmondo/common/pin/c;->d:Lio/reactivex/u;

    iput-object p3, p0, Lco/uk/getmondo/common/pin/c;->e:Lco/uk/getmondo/common/e/a;

    iput-object p4, p0, Lco/uk/getmondo/common/pin/c;->f:Lco/uk/getmondo/common/pin/a/a/b;

    iput-object p5, p0, Lco/uk/getmondo/common/pin/c;->g:Lco/uk/getmondo/profile/data/a;

    iput-object p6, p0, Lco/uk/getmondo/common/pin/c;->h:Lco/uk/getmondo/common/a;

    return-void
.end method

.method public static final synthetic a(Lco/uk/getmondo/common/pin/c;)Lco/uk/getmondo/common/pin/a/a/b;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lco/uk/getmondo/common/pin/c;->f:Lco/uk/getmondo/common/pin/a/a/b;

    return-object v0
.end method

.method public static final synthetic a(Lco/uk/getmondo/common/pin/c;Ljava/lang/Throwable;Lco/uk/getmondo/common/pin/c$a;)V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0, p1, p2}, Lco/uk/getmondo/common/pin/c;->a(Ljava/lang/Throwable;Lco/uk/getmondo/common/pin/c$a;)V

    return-void
.end method

.method private final a(Ljava/lang/Throwable;Lco/uk/getmondo/common/pin/c$a;)V
    .locals 2

    .prologue
    .line 76
    instance-of v0, p1, Lco/uk/getmondo/api/ApiException;

    if-eqz v0, :cond_2

    .line 77
    invoke-static {}, Lco/uk/getmondo/payments/a/h;->values()[Lco/uk/getmondo/payments/a/h;

    move-result-object v0

    check-cast v0, [Lco/uk/getmondo/common/e/f;

    move-object v1, p1

    check-cast v1, Lco/uk/getmondo/api/ApiException;

    invoke-virtual {v1}, Lco/uk/getmondo/api/ApiException;->e()Lco/uk/getmondo/api/model/b;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lco/uk/getmondo/api/model/b;->a()Ljava/lang/String;

    move-result-object v1

    :goto_0
    invoke-static {v0, v1}, Lco/uk/getmondo/common/e/d;->a([Lco/uk/getmondo/common/e/f;Ljava/lang/String;)Lco/uk/getmondo/common/e/f;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/payments/a/h;

    .line 78
    if-eqz v0, :cond_2

    .line 79
    sget-object v1, Lco/uk/getmondo/common/pin/d;->a:[I

    invoke-virtual {v0}, Lco/uk/getmondo/payments/a/h;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 89
    :cond_0
    :goto_1
    return-void

    .line 77
    :cond_1
    const/4 v1, 0x0

    goto :goto_0

    .line 80
    :pswitch_0
    invoke-interface {p2}, Lco/uk/getmondo/common/pin/c$a;->f()V

    goto :goto_1

    .line 81
    :pswitch_1
    invoke-interface {p2}, Lco/uk/getmondo/common/pin/c$a;->d()V

    goto :goto_1

    .line 86
    :cond_2
    iget-object v1, p0, Lco/uk/getmondo/common/pin/c;->e:Lco/uk/getmondo/common/e/a;

    move-object v0, p2

    check-cast v0, Lco/uk/getmondo/common/e/a$a;

    invoke-virtual {v1, p1, v0}, Lco/uk/getmondo/common/e/a;->a(Ljava/lang/Throwable;Lco/uk/getmondo/common/e/a$a;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 87
    invoke-interface {p2}, Lco/uk/getmondo/common/pin/c$a;->g()V

    goto :goto_1

    .line 79
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static final synthetic b(Lco/uk/getmondo/common/pin/c;)Lco/uk/getmondo/profile/data/a;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lco/uk/getmondo/common/pin/c;->g:Lco/uk/getmondo/profile/data/a;

    return-object v0
.end method

.method public static final synthetic c(Lco/uk/getmondo/common/pin/c;)Lio/reactivex/u;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lco/uk/getmondo/common/pin/c;->c:Lio/reactivex/u;

    return-object v0
.end method

.method public static final synthetic d(Lco/uk/getmondo/common/pin/c;)Lio/reactivex/u;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lco/uk/getmondo/common/pin/c;->d:Lio/reactivex/u;

    return-object v0
.end method

.method public static final synthetic e(Lco/uk/getmondo/common/pin/c;)Lco/uk/getmondo/common/a;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lco/uk/getmondo/common/pin/c;->h:Lco/uk/getmondo/common/a;

    return-object v0
.end method


# virtual methods
.method public a(Lco/uk/getmondo/common/pin/c$a;)V
    .locals 5

    .prologue
    const-string v0, "view"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v0, p1

    .line 37
    check-cast v0, Lco/uk/getmondo/common/ui/f;

    invoke-super {p0, v0}, Lco/uk/getmondo/common/ui/b;->a(Lco/uk/getmondo/common/ui/f;)V

    .line 39
    iget-object v0, p0, Lco/uk/getmondo/common/pin/c;->h:Lco/uk/getmondo/common/a;

    sget-object v1, Lco/uk/getmondo/api/model/tracking/Impression;->Companion:Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    iget-object v2, p0, Lco/uk/getmondo/common/pin/c;->f:Lco/uk/getmondo/common/pin/a/a/b;

    invoke-virtual {v2}, Lco/uk/getmondo/common/pin/a/a/b;->b()Lco/uk/getmondo/api/model/tracking/Impression$PinFrom;

    move-result-object v2

    invoke-virtual {v1, v2}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->b(Lco/uk/getmondo/api/model/tracking/Impression$PinFrom;)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v1

    invoke-virtual {v0, v1}, Lco/uk/getmondo/common/a;->a(Lco/uk/getmondo/api/model/tracking/Impression;)V

    .line 41
    iget-object v3, p0, Lco/uk/getmondo/common/pin/c;->b:Lio/reactivex/b/a;

    .line 42
    invoke-interface {p1}, Lco/uk/getmondo/common/pin/c$a;->a()Lio/reactivex/n;

    move-result-object v4

    new-instance v0, Lco/uk/getmondo/common/pin/c$b;

    invoke-direct {v0, p1}, Lco/uk/getmondo/common/pin/c$b;-><init>(Lco/uk/getmondo/common/pin/c$a;)V

    check-cast v0, Lio/reactivex/c/g;

    sget-object v1, Lco/uk/getmondo/common/pin/c$c;->a:Lco/uk/getmondo/common/pin/c$c;

    check-cast v1, Lkotlin/d/a/b;

    if-eqz v1, :cond_0

    new-instance v2, Lco/uk/getmondo/common/pin/e;

    invoke-direct {v2, v1}, Lco/uk/getmondo/common/pin/e;-><init>(Lkotlin/d/a/b;)V

    move-object v1, v2

    :cond_0
    check-cast v1, Lio/reactivex/c/g;

    invoke-virtual {v4, v0, v1}, Lio/reactivex/n;->subscribe(Lio/reactivex/c/g;Lio/reactivex/c/g;)Lio/reactivex/b/b;

    move-result-object v0

    const-string v1, "view.onPinClicked()\n    \u2026ePinError() }, Timber::e)"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 41
    invoke-static {v3, v0}, Lco/uk/getmondo/common/j/f;->a(Lio/reactivex/b/a;Lio/reactivex/b/b;)Lio/reactivex/b/a;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/common/pin/c;->b:Lio/reactivex/b/a;

    .line 44
    iget-object v3, p0, Lco/uk/getmondo/common/pin/c;->b:Lio/reactivex/b/a;

    .line 64
    invoke-interface {p1}, Lco/uk/getmondo/common/pin/c$a;->b()Lio/reactivex/n;

    move-result-object v1

    .line 45
    new-instance v0, Lco/uk/getmondo/common/pin/c$d;

    invoke-direct {v0, p0, p1}, Lco/uk/getmondo/common/pin/c$d;-><init>(Lco/uk/getmondo/common/pin/c;Lco/uk/getmondo/common/pin/c$a;)V

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {v1, v0}, Lio/reactivex/n;->flatMapMaybe(Lio/reactivex/c/h;)Lio/reactivex/n;

    move-result-object v4

    .line 64
    new-instance v0, Lco/uk/getmondo/common/pin/c$e;

    invoke-direct {v0, p0, p1}, Lco/uk/getmondo/common/pin/c$e;-><init>(Lco/uk/getmondo/common/pin/c;Lco/uk/getmondo/common/pin/c$a;)V

    check-cast v0, Lio/reactivex/c/g;

    .line 66
    sget-object v1, Lco/uk/getmondo/common/pin/c$f;->a:Lco/uk/getmondo/common/pin/c$f;

    check-cast v1, Lkotlin/d/a/b;

    if-eqz v1, :cond_1

    new-instance v2, Lco/uk/getmondo/common/pin/e;

    invoke-direct {v2, v1}, Lco/uk/getmondo/common/pin/e;-><init>(Lkotlin/d/a/b;)V

    move-object v1, v2

    :cond_1
    check-cast v1, Lio/reactivex/c/g;

    .line 64
    invoke-virtual {v4, v0, v1}, Lio/reactivex/n;->subscribe(Lio/reactivex/c/g;Lio/reactivex/c/g;)Lio/reactivex/b/b;

    move-result-object v0

    const-string v1, "view.onPinEntered()\n    \u2026            }, Timber::e)"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 44
    invoke-static {v3, v0}, Lco/uk/getmondo/common/j/f;->a(Lio/reactivex/b/a;Lio/reactivex/b/b;)Lio/reactivex/b/a;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/common/pin/c;->b:Lio/reactivex/b/a;

    .line 68
    iget-object v3, p0, Lco/uk/getmondo/common/pin/c;->b:Lio/reactivex/b/a;

    .line 69
    invoke-interface {p1}, Lco/uk/getmondo/common/pin/c$a;->c()Lio/reactivex/n;

    move-result-object v4

    new-instance v0, Lco/uk/getmondo/common/pin/c$g;

    invoke-direct {v0, p0, p1}, Lco/uk/getmondo/common/pin/c$g;-><init>(Lco/uk/getmondo/common/pin/c;Lco/uk/getmondo/common/pin/c$a;)V

    check-cast v0, Lio/reactivex/c/g;

    .line 72
    sget-object v1, Lco/uk/getmondo/common/pin/c$h;->a:Lco/uk/getmondo/common/pin/c$h;

    check-cast v1, Lkotlin/d/a/b;

    if-eqz v1, :cond_2

    new-instance v2, Lco/uk/getmondo/common/pin/e;

    invoke-direct {v2, v1}, Lco/uk/getmondo/common/pin/e;-><init>(Lkotlin/d/a/b;)V

    move-object v1, v2

    :cond_2
    check-cast v1, Lio/reactivex/c/g;

    .line 69
    invoke-virtual {v4, v0, v1}, Lio/reactivex/n;->subscribe(Lio/reactivex/c/g;Lio/reactivex/c/g;)Lio/reactivex/b/b;

    move-result-object v0

    const-string v1, "view.onForgotPin()\n     \u2026            }, Timber::e)"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 68
    invoke-static {v3, v0}, Lco/uk/getmondo/common/j/f;->a(Lio/reactivex/b/a;Lio/reactivex/b/b;)Lio/reactivex/b/a;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/common/pin/c;->b:Lio/reactivex/b/a;

    .line 73
    return-void
.end method

.method public bridge synthetic a(Lco/uk/getmondo/common/ui/f;)V
    .locals 0

    .prologue
    .line 26
    check-cast p1, Lco/uk/getmondo/common/pin/c$a;

    invoke-virtual {p0, p1}, Lco/uk/getmondo/common/pin/c;->a(Lco/uk/getmondo/common/pin/c$a;)V

    return-void
.end method
