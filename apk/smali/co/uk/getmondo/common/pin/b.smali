.class public final Lco/uk/getmondo/common/pin/b;
.super Ljava/lang/Object;
.source "PinEntryActivity_MembersInjector.java"

# interfaces
.implements Lb/a;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lb/a",
        "<",
        "Lco/uk/getmondo/common/pin/PinEntryActivity;",
        ">;"
    }
.end annotation


# static fields
.field static final synthetic a:Z


# instance fields
.field private final b:Ljavax/a/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/common/pin/c;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 7
    const-class v0, Lco/uk/getmondo/common/pin/b;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lco/uk/getmondo/common/pin/b;->a:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Ljavax/a/a;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/common/pin/c;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 15
    sget-boolean v0, Lco/uk/getmondo/common/pin/b;->a:Z

    if-nez v0, :cond_0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 16
    :cond_0
    iput-object p1, p0, Lco/uk/getmondo/common/pin/b;->b:Ljavax/a/a;

    .line 17
    return-void
.end method

.method public static a(Ljavax/a/a;)Lb/a;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/common/pin/c;",
            ">;)",
            "Lb/a",
            "<",
            "Lco/uk/getmondo/common/pin/PinEntryActivity;",
            ">;"
        }
    .end annotation

    .prologue
    .line 21
    new-instance v0, Lco/uk/getmondo/common/pin/b;

    invoke-direct {v0, p0}, Lco/uk/getmondo/common/pin/b;-><init>(Ljavax/a/a;)V

    return-object v0
.end method


# virtual methods
.method public a(Lco/uk/getmondo/common/pin/PinEntryActivity;)V
    .locals 2

    .prologue
    .line 26
    if-nez p1, :cond_0

    .line 27
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Cannot inject members into a null reference"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 29
    :cond_0
    iget-object v0, p0, Lco/uk/getmondo/common/pin/b;->b:Ljavax/a/a;

    invoke-interface {v0}, Ljavax/a/a;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/common/pin/c;

    iput-object v0, p1, Lco/uk/getmondo/common/pin/PinEntryActivity;->b:Lco/uk/getmondo/common/pin/c;

    .line 30
    return-void
.end method

.method public bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 7
    check-cast p1, Lco/uk/getmondo/common/pin/PinEntryActivity;

    invoke-virtual {p0, p1}, Lco/uk/getmondo/common/pin/b;->a(Lco/uk/getmondo/common/pin/PinEntryActivity;)V

    return-void
.end method
