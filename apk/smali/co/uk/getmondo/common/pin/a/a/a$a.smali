.class public final enum Lco/uk/getmondo/common/pin/a/a/a$a;
.super Ljava/lang/Enum;
.source "Challenge.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lco/uk/getmondo/common/pin/a/a/a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lco/uk/getmondo/common/pin/a/a/a$a;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0006\u0008\u0086\u0001\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00000\u0001B\u000f\u0008\u0002\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0005\u0010\u0006j\u0002\u0008\u0007j\u0002\u0008\u0008\u00a8\u0006\t"
    }
    d2 = {
        "Lco/uk/getmondo/common/pin/data/model/Challenge$ChallengeType;",
        "",
        "type",
        "",
        "(Ljava/lang/String;ILjava/lang/String;)V",
        "getType",
        "()Ljava/lang/String;",
        "PIN",
        "SECURE_TOKEN",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# static fields
.field public static final enum a:Lco/uk/getmondo/common/pin/a/a/a$a;

.field public static final enum b:Lco/uk/getmondo/common/pin/a/a/a$a;

.field private static final synthetic c:[Lco/uk/getmondo/common/pin/a/a/a$a;


# instance fields
.field private final d:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    const/4 v0, 0x2

    new-array v0, v0, [Lco/uk/getmondo/common/pin/a/a/a$a;

    new-instance v1, Lco/uk/getmondo/common/pin/a/a/a$a;

    const-string v2, "PIN"

    .line 7
    const-string v3, "pin"

    invoke-direct {v1, v2, v4, v3}, Lco/uk/getmondo/common/pin/a/a/a$a;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lco/uk/getmondo/common/pin/a/a/a$a;->a:Lco/uk/getmondo/common/pin/a/a/a$a;

    aput-object v1, v0, v4

    new-instance v1, Lco/uk/getmondo/common/pin/a/a/a$a;

    const-string v2, "SECURE_TOKEN"

    .line 8
    const-string v3, "secure_token"

    invoke-direct {v1, v2, v5, v3}, Lco/uk/getmondo/common/pin/a/a/a$a;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lco/uk/getmondo/common/pin/a/a/a$a;->b:Lco/uk/getmondo/common/pin/a/a/a$a;

    aput-object v1, v0, v5

    sput-object v0, Lco/uk/getmondo/common/pin/a/a/a$a;->c:[Lco/uk/getmondo/common/pin/a/a/a$a;

    return-void
.end method

.method protected constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    const-string v0, "type"

    invoke-static {p3, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 6
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput-object p3, p0, Lco/uk/getmondo/common/pin/a/a/a$a;->d:Ljava/lang/String;

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lco/uk/getmondo/common/pin/a/a/a$a;
    .locals 1

    const-class v0, Lco/uk/getmondo/common/pin/a/a/a$a;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/common/pin/a/a/a$a;

    return-object v0
.end method

.method public static values()[Lco/uk/getmondo/common/pin/a/a/a$a;
    .locals 1

    sget-object v0, Lco/uk/getmondo/common/pin/a/a/a$a;->c:[Lco/uk/getmondo/common/pin/a/a/a$a;

    invoke-virtual {v0}, [Lco/uk/getmondo/common/pin/a/a/a$a;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lco/uk/getmondo/common/pin/a/a/a$a;

    return-object v0
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 6
    iget-object v0, p0, Lco/uk/getmondo/common/pin/a/a/a$a;->d:Ljava/lang/String;

    return-object v0
.end method
