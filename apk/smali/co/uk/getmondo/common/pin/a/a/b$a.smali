.class public final Lco/uk/getmondo/common/pin/a/a/b$a;
.super Lco/uk/getmondo/common/pin/a/a/b;
.source "PinOperation.kt"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lco/uk/getmondo/common/pin/a/a/b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lco/uk/getmondo/common/pin/a/a/b$a$a;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000L\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u000c\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0004\u0008\u0086\u0008\u0018\u0000 $2\u00020\u00012\u00020\u0002:\u0001$B\u000f\u0008\u0016\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u00a2\u0006\u0002\u0010\u0005B\u001f\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u0012\u0008\u0008\u0002\u0010\n\u001a\u00020\u000b\u00a2\u0006\u0002\u0010\u000cJ\t\u0010\u0013\u001a\u00020\u0007H\u00c6\u0003J\t\u0010\u0014\u001a\u00020\tH\u00c6\u0003J\t\u0010\u0015\u001a\u00020\u000bH\u00c6\u0003J\'\u0010\u0016\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0006\u001a\u00020\u00072\u0008\u0008\u0002\u0010\u0008\u001a\u00020\t2\u0008\u0008\u0002\u0010\n\u001a\u00020\u000bH\u00c6\u0001J\u0008\u0010\u0017\u001a\u00020\u0018H\u0016J\u0013\u0010\u0019\u001a\u00020\u001a2\u0008\u0010\u001b\u001a\u0004\u0018\u00010\u001cH\u00d6\u0003J\t\u0010\u001d\u001a\u00020\u0018H\u00d6\u0001J\t\u0010\u001e\u001a\u00020\u001fH\u00d6\u0001J\u0018\u0010 \u001a\u00020!2\u0006\u0010\"\u001a\u00020\u00042\u0006\u0010#\u001a\u00020\u0018H\u0016R\u0011\u0010\u0008\u001a\u00020\t\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\r\u0010\u000eR\u0014\u0010\n\u001a\u00020\u000bX\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000f\u0010\u0010R\u0014\u0010\u0006\u001a\u00020\u0007X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0011\u0010\u0012\u00a8\u0006%"
    }
    d2 = {
        "Lco/uk/getmondo/common/pin/data/model/PinOperation$UpdateAddress;",
        "Lco/uk/getmondo/common/pin/data/model/PinOperation;",
        "Landroid/os/Parcelable;",
        "source",
        "Landroid/os/Parcel;",
        "(Landroid/os/Parcel;)V",
        "intent",
        "Landroid/content/Intent;",
        "address",
        "Lco/uk/getmondo/model/LegacyAddress;",
        "from",
        "Lco/uk/getmondo/api/model/tracking/Impression$PinFrom;",
        "(Landroid/content/Intent;Lco/uk/getmondo/model/LegacyAddress;Lco/uk/getmondo/api/model/tracking/Impression$PinFrom;)V",
        "getAddress",
        "()Lco/uk/getmondo/model/LegacyAddress;",
        "getFrom",
        "()Lco/uk/getmondo/api/model/tracking/Impression$PinFrom;",
        "getIntent",
        "()Landroid/content/Intent;",
        "component1",
        "component2",
        "component3",
        "copy",
        "describeContents",
        "",
        "equals",
        "",
        "other",
        "",
        "hashCode",
        "toString",
        "",
        "writeToParcel",
        "",
        "dest",
        "flags",
        "Companion",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lco/uk/getmondo/common/pin/a/a/b$a;",
            ">;"
        }
    .end annotation
.end field

.field public static final a:Lco/uk/getmondo/common/pin/a/a/b$a$a;


# instance fields
.field private final b:Landroid/content/Intent;

.field private final c:Lco/uk/getmondo/d/s;

.field private final d:Lco/uk/getmondo/api/model/tracking/Impression$PinFrom;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    new-instance v0, Lco/uk/getmondo/common/pin/a/a/b$a$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lco/uk/getmondo/common/pin/a/a/b$a$a;-><init>(Lkotlin/d/b/i;)V

    sput-object v0, Lco/uk/getmondo/common/pin/a/a/b$a;->a:Lco/uk/getmondo/common/pin/a/a/b$a$a;

    .line 36
    new-instance v0, Lco/uk/getmondo/common/pin/a/a/b$a$b;

    invoke-direct {v0}, Lco/uk/getmondo/common/pin/a/a/b$a$b;-><init>()V

    check-cast v0, Landroid/os/Parcelable$Creator;

    sput-object v0, Lco/uk/getmondo/common/pin/a/a/b$a;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/content/Intent;Lco/uk/getmondo/d/s;Lco/uk/getmondo/api/model/tracking/Impression$PinFrom;)V
    .locals 1

    .prologue
    const-string v0, "intent"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "address"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "from"

    invoke-static {p3, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 15
    const/4 v0, 0x0

    .line 19
    invoke-direct {p0, v0}, Lco/uk/getmondo/common/pin/a/a/b;-><init>(Lkotlin/d/b/i;)V

    iput-object p1, p0, Lco/uk/getmondo/common/pin/a/a/b$a;->b:Landroid/content/Intent;

    iput-object p2, p0, Lco/uk/getmondo/common/pin/a/a/b$a;->c:Lco/uk/getmondo/d/s;

    iput-object p3, p0, Lco/uk/getmondo/common/pin/a/a/b$a;->d:Lco/uk/getmondo/api/model/tracking/Impression$PinFrom;

    return-void
.end method

.method public synthetic constructor <init>(Landroid/content/Intent;Lco/uk/getmondo/d/s;Lco/uk/getmondo/api/model/tracking/Impression$PinFrom;ILkotlin/d/b/i;)V
    .locals 1

    .prologue
    and-int/lit8 v0, p4, 0x4

    if-eqz v0, :cond_0

    .line 18
    sget-object p3, Lco/uk/getmondo/api/model/tracking/Impression$PinFrom;->ADDRESS_CHANGE:Lco/uk/getmondo/api/model/tracking/Impression$PinFrom;

    :cond_0
    invoke-direct {p0, p1, p2, p3}, Lco/uk/getmondo/common/pin/a/a/b$a;-><init>(Landroid/content/Intent;Lco/uk/getmondo/d/s;Lco/uk/getmondo/api/model/tracking/Impression$PinFrom;)V

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 4

    .prologue
    const-string v0, "source"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 22
    const-class v0, Landroid/content/Intent;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    const-string v1, "source.readParcelable<In\u2026::class.java.classLoader)"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/content/Intent;

    .line 23
    const-class v1, Lco/uk/getmondo/d/s;

    invoke-virtual {v1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v1

    const-string v2, "source.readParcelable<Le\u2026::class.java.classLoader)"

    invoke-static {v1, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v1, Lco/uk/getmondo/d/s;

    .line 24
    invoke-static {}, Lco/uk/getmondo/api/model/tracking/Impression$PinFrom;->values()[Lco/uk/getmondo/api/model/tracking/Impression$PinFrom;

    move-result-object v2

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v3

    aget-object v2, v2, v3

    .line 21
    invoke-direct {p0, v0, v1, v2}, Lco/uk/getmondo/common/pin/a/a/b$a;-><init>(Landroid/content/Intent;Lco/uk/getmondo/d/s;Lco/uk/getmondo/api/model/tracking/Impression$PinFrom;)V

    return-void
.end method


# virtual methods
.method public a()Landroid/content/Intent;
    .locals 1

    .prologue
    .line 16
    iget-object v0, p0, Lco/uk/getmondo/common/pin/a/a/b$a;->b:Landroid/content/Intent;

    return-object v0
.end method

.method public b()Lco/uk/getmondo/api/model/tracking/Impression$PinFrom;
    .locals 1

    .prologue
    .line 18
    iget-object v0, p0, Lco/uk/getmondo/common/pin/a/a/b$a;->d:Lco/uk/getmondo/api/model/tracking/Impression$PinFrom;

    return-object v0
.end method

.method public final c()Lco/uk/getmondo/d/s;
    .locals 1

    .prologue
    .line 17
    iget-object v0, p0, Lco/uk/getmondo/common/pin/a/a/b$a;->c:Lco/uk/getmondo/d/s;

    return-object v0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 27
    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_0

    instance-of v0, p1, Lco/uk/getmondo/common/pin/a/a/b$a;

    if-eqz v0, :cond_1

    check-cast p1, Lco/uk/getmondo/common/pin/a/a/b$a;

    invoke-virtual {p0}, Lco/uk/getmondo/common/pin/a/a/b$a;->a()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p1}, Lco/uk/getmondo/common/pin/a/a/b$a;->a()Landroid/content/Intent;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lco/uk/getmondo/common/pin/a/a/b$a;->c:Lco/uk/getmondo/d/s;

    iget-object v1, p1, Lco/uk/getmondo/common/pin/a/a/b$a;->c:Lco/uk/getmondo/d/s;

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lco/uk/getmondo/common/pin/a/a/b$a;->b()Lco/uk/getmondo/api/model/tracking/Impression$PinFrom;

    move-result-object v0

    invoke-virtual {p1}, Lco/uk/getmondo/common/pin/a/a/b$a;->b()Lco/uk/getmondo/api/model/tracking/Impression$PinFrom;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    const/4 v1, 0x0

    invoke-virtual {p0}, Lco/uk/getmondo/common/pin/a/a/b$a;->a()Landroid/content/Intent;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    :goto_0
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lco/uk/getmondo/common/pin/a/a/b$a;->c:Lco/uk/getmondo/d/s;

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lco/uk/getmondo/common/pin/a/a/b$a;->b()Lco/uk/getmondo/api/model/tracking/Impression$PinFrom;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_0
    add-int/2addr v0, v1

    return v0

    :cond_1
    move v0, v1

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_1
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "UpdateAddress(intent="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lco/uk/getmondo/common/pin/a/a/b$a;->a()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", address="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/common/pin/a/a/b$a;->c:Lco/uk/getmondo/d/s;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", from="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lco/uk/getmondo/common/pin/a/a/b$a;->b()Lco/uk/getmondo/api/model/tracking/Impression$PinFrom;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const-string v0, "dest"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 30
    invoke-virtual {p0}, Lco/uk/getmondo/common/pin/a/a/b$a;->a()Landroid/content/Intent;

    move-result-object v0

    check-cast v0, Landroid/os/Parcelable;

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 31
    iget-object v0, p0, Lco/uk/getmondo/common/pin/a/a/b$a;->c:Lco/uk/getmondo/d/s;

    check-cast v0, Landroid/os/Parcelable;

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 32
    invoke-virtual {p0}, Lco/uk/getmondo/common/pin/a/a/b$a;->b()Lco/uk/getmondo/api/model/tracking/Impression$PinFrom;

    move-result-object v0

    invoke-virtual {v0}, Lco/uk/getmondo/api/model/tracking/Impression$PinFrom;->ordinal()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 33
    nop

    .line 29
    nop

    .line 33
    return-void
.end method
