.class public Lco/uk/getmondo/waitlist/ui/ParallaxLinearLayout_ViewBinding;
.super Ljava/lang/Object;
.source "ParallaxLinearLayout_ViewBinding.java"

# interfaces
.implements Lbutterknife/Unbinder;


# instance fields
.field private a:Lco/uk/getmondo/waitlist/ui/ParallaxLinearLayout;


# direct methods
.method public constructor <init>(Lco/uk/getmondo/waitlist/ui/ParallaxLinearLayout;Landroid/view/View;)V
    .locals 3

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    iput-object p1, p0, Lco/uk/getmondo/waitlist/ui/ParallaxLinearLayout_ViewBinding;->a:Lco/uk/getmondo/waitlist/ui/ParallaxLinearLayout;

    .line 27
    const v0, 0x7f11040b

    const-string v1, "field \'imageView\'"

    const-class v2, Landroid/widget/ImageView;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p1, Lco/uk/getmondo/waitlist/ui/ParallaxLinearLayout;->imageView:Landroid/widget/ImageView;

    .line 28
    const v0, 0x7f11040c

    const-string v1, "field \'ahead\'"

    const-class v2, Lco/uk/getmondo/common/ui/AnimatedNumberView;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/common/ui/AnimatedNumberView;

    iput-object v0, p1, Lco/uk/getmondo/waitlist/ui/ParallaxLinearLayout;->ahead:Lco/uk/getmondo/common/ui/AnimatedNumberView;

    .line 29
    const v0, 0x7f11040d

    const-string v1, "field \'behind\'"

    const-class v2, Lco/uk/getmondo/common/ui/AnimatedNumberView;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/common/ui/AnimatedNumberView;

    iput-object v0, p1, Lco/uk/getmondo/waitlist/ui/ParallaxLinearLayout;->behind:Lco/uk/getmondo/common/ui/AnimatedNumberView;

    .line 30
    return-void
.end method


# virtual methods
.method public unbind()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 35
    iget-object v0, p0, Lco/uk/getmondo/waitlist/ui/ParallaxLinearLayout_ViewBinding;->a:Lco/uk/getmondo/waitlist/ui/ParallaxLinearLayout;

    .line 36
    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Bindings already cleared."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 37
    :cond_0
    iput-object v1, p0, Lco/uk/getmondo/waitlist/ui/ParallaxLinearLayout_ViewBinding;->a:Lco/uk/getmondo/waitlist/ui/ParallaxLinearLayout;

    .line 39
    iput-object v1, v0, Lco/uk/getmondo/waitlist/ui/ParallaxLinearLayout;->imageView:Landroid/widget/ImageView;

    .line 40
    iput-object v1, v0, Lco/uk/getmondo/waitlist/ui/ParallaxLinearLayout;->ahead:Lco/uk/getmondo/common/ui/AnimatedNumberView;

    .line 41
    iput-object v1, v0, Lco/uk/getmondo/waitlist/ui/ParallaxLinearLayout;->behind:Lco/uk/getmondo/common/ui/AnimatedNumberView;

    .line 42
    return-void
.end method
