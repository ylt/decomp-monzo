.class public Lco/uk/getmondo/waitlist/ui/ParallaxSwipeToRefresh;
.super Landroid/support/v4/widget/SwipeRefreshLayout;
.source "ParallaxSwipeToRefresh.java"


# instance fields
.field private m:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 13
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lco/uk/getmondo/waitlist/ui/ParallaxSwipeToRefresh;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 14
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0, p1, p2}, Landroid/support/v4/widget/SwipeRefreshLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 18
    return-void
.end method


# virtual methods
.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 22
    iget-boolean v0, p0, Lco/uk/getmondo/waitlist/ui/ParallaxSwipeToRefresh;->m:Z

    if-nez v0, :cond_0

    .line 23
    invoke-virtual {p0, p1}, Lco/uk/getmondo/waitlist/ui/ParallaxSwipeToRefresh;->onInterceptTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    iput-boolean v0, p0, Lco/uk/getmondo/waitlist/ui/ParallaxSwipeToRefresh;->m:Z

    .line 25
    :cond_0
    iget-boolean v0, p0, Lco/uk/getmondo/waitlist/ui/ParallaxSwipeToRefresh;->m:Z

    if-eqz v0, :cond_1

    .line 26
    invoke-super {p0, p1}, Landroid/support/v4/widget/SwipeRefreshLayout;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    .line 28
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setRefreshing(Z)V
    .locals 1

    .prologue
    .line 33
    invoke-super {p0, p1}, Landroid/support/v4/widget/SwipeRefreshLayout;->setRefreshing(Z)V

    .line 34
    const/4 v0, 0x0

    iput-boolean v0, p0, Lco/uk/getmondo/waitlist/ui/ParallaxSwipeToRefresh;->m:Z

    .line 35
    return-void
.end method
