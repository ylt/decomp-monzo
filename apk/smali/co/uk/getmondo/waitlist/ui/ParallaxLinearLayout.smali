.class public Lco/uk/getmondo/waitlist/ui/ParallaxLinearLayout;
.super Landroid/widget/LinearLayout;
.source "ParallaxLinearLayout.java"


# instance fields
.field private a:I

.field ahead:Lco/uk/getmondo/common/ui/AnimatedNumberView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f11040c
    .end annotation
.end field

.field private b:I

.field behind:Lco/uk/getmondo/common/ui/AnimatedNumberView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f11040d
    .end annotation
.end field

.field imageView:Landroid/widget/ImageView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f11040b
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 27
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lco/uk/getmondo/waitlist/ui/ParallaxLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 28
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 31
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lco/uk/getmondo/waitlist/ui/ParallaxLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 32
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 35
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, Lco/uk/getmondo/waitlist/ui/ParallaxLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V

    .line 36
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 39
    invoke-direct {p0, p1, p2, p3, p4}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V

    .line 22
    const/4 v0, -0x1

    iput v0, p0, Lco/uk/getmondo/waitlist/ui/ParallaxLinearLayout;->a:I

    .line 40
    invoke-static {p1}, Lco/uk/getmondo/MonzoApplication;->a(Landroid/content/Context;)Lco/uk/getmondo/MonzoApplication;

    move-result-object v0

    invoke-virtual {v0}, Lco/uk/getmondo/MonzoApplication;->b()Lco/uk/getmondo/common/h/a/a;

    move-result-object v0

    invoke-interface {v0, p0}, Lco/uk/getmondo/common/h/a/a;->a(Lco/uk/getmondo/waitlist/ui/ParallaxLinearLayout;)V

    .line 41
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lco/uk/getmondo/waitlist/ui/ParallaxLinearLayout;->setOrientation(I)V

    .line 42
    if-nez p2, :cond_0

    .line 51
    :goto_0
    return-void

    .line 45
    :cond_0
    invoke-virtual {p1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v0

    sget-object v1, Lco/uk/getmondo/c$b;->ParallaxLinearLayout:[I

    invoke-virtual {v0, p2, v1, v2, v2}, Landroid/content/res/Resources$Theme;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v1

    .line 47
    const/4 v0, 0x0

    const/4 v2, 0x0

    :try_start_0
    invoke-virtual {v1, v0, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v0

    iput v0, p0, Lco/uk/getmondo/waitlist/ui/ParallaxLinearLayout;->b:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 49
    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    throw v0
.end method


# virtual methods
.method public a(I)V
    .locals 2

    .prologue
    .line 66
    const/4 v0, 0x0

    int-to-float v1, p1

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Float;->intValue()I

    move-result v1

    invoke-virtual {p0, v0, v1}, Lco/uk/getmondo/waitlist/ui/ParallaxLinearLayout;->scrollTo(II)V

    .line 67
    return-void
.end method

.method public a(Lco/uk/getmondo/d/an;)V
    .locals 5

    .prologue
    .line 70
    if-nez p1, :cond_1

    .line 84
    :cond_0
    :goto_0
    return-void

    .line 74
    :cond_1
    iget-object v0, p0, Lco/uk/getmondo/waitlist/ui/ParallaxLinearLayout;->ahead:Lco/uk/getmondo/common/ui/AnimatedNumberView;

    invoke-virtual {p1}, Lco/uk/getmondo/d/an;->d()I

    move-result v1

    invoke-virtual {v0, v1}, Lco/uk/getmondo/common/ui/AnimatedNumberView;->setNumber(I)V

    .line 75
    iget-object v0, p0, Lco/uk/getmondo/waitlist/ui/ParallaxLinearLayout;->behind:Lco/uk/getmondo/common/ui/AnimatedNumberView;

    invoke-virtual {p1}, Lco/uk/getmondo/d/an;->e()I

    move-result v1

    invoke-virtual {v0, v1}, Lco/uk/getmondo/common/ui/AnimatedNumberView;->setNumber(I)V

    .line 77
    invoke-virtual {p1}, Lco/uk/getmondo/d/an;->b()I

    move-result v0

    .line 78
    iget v1, p0, Lco/uk/getmondo/waitlist/ui/ParallaxLinearLayout;->a:I

    if-eq v1, v0, :cond_0

    .line 79
    iput v0, p0, Lco/uk/getmondo/waitlist/ui/ParallaxLinearLayout;->a:I

    .line 81
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "waiting_list_"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 82
    iget-object v1, p0, Lco/uk/getmondo/waitlist/ui/ParallaxLinearLayout;->imageView:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lco/uk/getmondo/waitlist/ui/ParallaxLinearLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const-string v3, "drawable"

    invoke-virtual {p0}, Lco/uk/getmondo/waitlist/ui/ParallaxLinearLayout;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v0, v3, v4}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0
.end method

.method public b(I)V
    .locals 1

    .prologue
    .line 87
    iget-object v0, p0, Lco/uk/getmondo/waitlist/ui/ParallaxLinearLayout;->ahead:Lco/uk/getmondo/common/ui/AnimatedNumberView;

    invoke-virtual {v0, p1}, Lco/uk/getmondo/common/ui/AnimatedNumberView;->a(I)V

    .line 88
    iget-object v0, p0, Lco/uk/getmondo/waitlist/ui/ParallaxLinearLayout;->behind:Lco/uk/getmondo/common/ui/AnimatedNumberView;

    invoke-virtual {v0, p1}, Lco/uk/getmondo/common/ui/AnimatedNumberView;->a(I)V

    .line 89
    return-void
.end method

.method public canScrollVertically(I)Z
    .locals 2

    .prologue
    .line 55
    invoke-virtual {p0}, Lco/uk/getmondo/waitlist/ui/ParallaxLinearLayout;->getScrollY()I

    move-result v0

    iget v1, p0, Lco/uk/getmondo/waitlist/ui/ParallaxLinearLayout;->b:I

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onFinishInflate()V
    .locals 0

    .prologue
    .line 60
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    .line 62
    invoke-static {p0}, Lbutterknife/ButterKnife;->bind(Landroid/view/View;)Lbutterknife/Unbinder;

    .line 63
    return-void
.end method
