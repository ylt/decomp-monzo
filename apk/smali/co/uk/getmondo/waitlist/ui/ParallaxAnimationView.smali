.class public Lco/uk/getmondo/waitlist/ui/ParallaxAnimationView;
.super Landroid/widget/FrameLayout;
.source "ParallaxAnimationView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lco/uk/getmondo/waitlist/ui/ParallaxAnimationView$a;
    }
.end annotation


# instance fields
.field private a:Lco/uk/getmondo/waitlist/ui/ParallaxImageView;

.field private b:Lco/uk/getmondo/waitlist/ui/ParallaxImageView;

.field private c:Lco/uk/getmondo/waitlist/ui/ParallaxImageView;

.field private d:Lco/uk/getmondo/waitlist/ui/ParallaxImageView;

.field private e:Lco/uk/getmondo/waitlist/ui/ParallaxImageView;

.field private f:Lco/uk/getmondo/waitlist/ui/ParallaxImageView;

.field private g:Lco/uk/getmondo/waitlist/ui/ParallaxLinearLayout;

.field private h:Lco/uk/getmondo/waitlist/ui/ParallaxImageView;

.field private i:Lco/uk/getmondo/waitlist/ui/ParallaxSwipeToRefresh;

.field private j:Lco/uk/getmondo/waitlist/ui/ParallaxTouchInterceptorView;

.field private k:Lcom/c/b/b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/c/b/b",
            "<",
            "Lco/uk/getmondo/waitlist/ui/ParallaxAnimationView$a;",
            ">;"
        }
    .end annotation
.end field

.field private l:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 30
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lco/uk/getmondo/waitlist/ui/ParallaxAnimationView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 31
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 34
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lco/uk/getmondo/waitlist/ui/ParallaxAnimationView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 35
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3

    .prologue
    .line 38
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 26
    invoke-static {}, Lcom/c/b/b;->c()Lcom/c/b/b;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/waitlist/ui/ParallaxAnimationView;->k:Lcom/c/b/b;

    .line 40
    invoke-virtual {p0}, Lco/uk/getmondo/waitlist/ui/ParallaxAnimationView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0500f7

    const/4 v2, 0x1

    invoke-virtual {v0, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 41
    return-void
.end method

.method private a(F)V
    .locals 2

    .prologue
    .line 70
    iget-object v0, p0, Lco/uk/getmondo/waitlist/ui/ParallaxAnimationView;->h:Lco/uk/getmondo/waitlist/ui/ParallaxImageView;

    invoke-virtual {v0, p1}, Lco/uk/getmondo/waitlist/ui/ParallaxImageView;->a(F)V

    .line 71
    iget-object v0, p0, Lco/uk/getmondo/waitlist/ui/ParallaxAnimationView;->a:Lco/uk/getmondo/waitlist/ui/ParallaxImageView;

    invoke-virtual {v0, p1}, Lco/uk/getmondo/waitlist/ui/ParallaxImageView;->a(F)V

    .line 72
    iget-object v0, p0, Lco/uk/getmondo/waitlist/ui/ParallaxAnimationView;->b:Lco/uk/getmondo/waitlist/ui/ParallaxImageView;

    invoke-virtual {v0, p1}, Lco/uk/getmondo/waitlist/ui/ParallaxImageView;->a(F)V

    .line 73
    iget-object v0, p0, Lco/uk/getmondo/waitlist/ui/ParallaxAnimationView;->c:Lco/uk/getmondo/waitlist/ui/ParallaxImageView;

    invoke-virtual {v0, p1}, Lco/uk/getmondo/waitlist/ui/ParallaxImageView;->a(F)V

    .line 74
    iget-object v0, p0, Lco/uk/getmondo/waitlist/ui/ParallaxAnimationView;->d:Lco/uk/getmondo/waitlist/ui/ParallaxImageView;

    invoke-virtual {v0, p1}, Lco/uk/getmondo/waitlist/ui/ParallaxImageView;->a(F)V

    .line 75
    iget-object v0, p0, Lco/uk/getmondo/waitlist/ui/ParallaxAnimationView;->e:Lco/uk/getmondo/waitlist/ui/ParallaxImageView;

    invoke-virtual {v0, p1}, Lco/uk/getmondo/waitlist/ui/ParallaxImageView;->a(F)V

    .line 76
    iget-object v0, p0, Lco/uk/getmondo/waitlist/ui/ParallaxAnimationView;->f:Lco/uk/getmondo/waitlist/ui/ParallaxImageView;

    invoke-virtual {v0, p1}, Lco/uk/getmondo/waitlist/ui/ParallaxImageView;->a(F)V

    .line 77
    iget-object v0, p0, Lco/uk/getmondo/waitlist/ui/ParallaxAnimationView;->g:Lco/uk/getmondo/waitlist/ui/ParallaxLinearLayout;

    float-to-int v1, p1

    invoke-virtual {v0, v1}, Lco/uk/getmondo/waitlist/ui/ParallaxLinearLayout;->a(I)V

    .line 78
    return-void
.end method

.method static synthetic a(Lco/uk/getmondo/waitlist/ui/ParallaxAnimationView;)V
    .locals 1

    .prologue
    .line 62
    iget-boolean v0, p0, Lco/uk/getmondo/waitlist/ui/ParallaxAnimationView;->l:Z

    if-eqz v0, :cond_0

    .line 63
    const/4 v0, 0x0

    iput-boolean v0, p0, Lco/uk/getmondo/waitlist/ui/ParallaxAnimationView;->l:Z

    .line 64
    iget-object v0, p0, Lco/uk/getmondo/waitlist/ui/ParallaxAnimationView;->j:Lco/uk/getmondo/waitlist/ui/ParallaxTouchInterceptorView;

    invoke-virtual {v0}, Lco/uk/getmondo/waitlist/ui/ParallaxTouchInterceptorView;->b()V

    .line 66
    :cond_0
    return-void
.end method

.method static synthetic a(Lco/uk/getmondo/waitlist/ui/ParallaxAnimationView;F)V
    .locals 0

    invoke-direct {p0, p1}, Lco/uk/getmondo/waitlist/ui/ParallaxAnimationView;->a(F)V

    return-void
.end method

.method private b()V
    .locals 2

    .prologue
    .line 95
    iget-object v0, p0, Lco/uk/getmondo/waitlist/ui/ParallaxAnimationView;->k:Lcom/c/b/b;

    invoke-static {}, Lco/uk/getmondo/waitlist/ui/d;->a()Lcom/c/a/a;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/c/b/b;->a(Lcom/c/a/a;)Lcom/c/b/b;

    .line 96
    return-void
.end method

.method static synthetic b(Lco/uk/getmondo/waitlist/ui/ParallaxAnimationView;)V
    .locals 0

    invoke-direct {p0}, Lco/uk/getmondo/waitlist/ui/ParallaxAnimationView;->b()V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    .line 109
    iget-object v0, p0, Lco/uk/getmondo/waitlist/ui/ParallaxAnimationView;->j:Lco/uk/getmondo/waitlist/ui/ParallaxTouchInterceptorView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lco/uk/getmondo/waitlist/ui/ParallaxTouchInterceptorView;->setInitialPosition(Z)V

    .line 110
    return-void
.end method

.method public a(I)V
    .locals 1

    .prologue
    .line 99
    iget-object v0, p0, Lco/uk/getmondo/waitlist/ui/ParallaxAnimationView;->g:Lco/uk/getmondo/waitlist/ui/ParallaxLinearLayout;

    invoke-virtual {v0, p1}, Lco/uk/getmondo/waitlist/ui/ParallaxLinearLayout;->b(I)V

    .line 100
    return-void
.end method

.method public a(Lco/uk/getmondo/d/an;)V
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lco/uk/getmondo/waitlist/ui/ParallaxAnimationView;->g:Lco/uk/getmondo/waitlist/ui/ParallaxLinearLayout;

    invoke-virtual {v0, p1}, Lco/uk/getmondo/waitlist/ui/ParallaxLinearLayout;->a(Lco/uk/getmondo/d/an;)V

    .line 86
    return-void
.end method

.method public a(Lco/uk/getmondo/d/an;Z)V
    .locals 1

    .prologue
    .line 103
    iput-boolean p2, p0, Lco/uk/getmondo/waitlist/ui/ParallaxAnimationView;->l:Z

    .line 104
    iget-object v0, p0, Lco/uk/getmondo/waitlist/ui/ParallaxAnimationView;->g:Lco/uk/getmondo/waitlist/ui/ParallaxLinearLayout;

    invoke-virtual {v0, p1}, Lco/uk/getmondo/waitlist/ui/ParallaxLinearLayout;->a(Lco/uk/getmondo/d/an;)V

    .line 105
    iget-object v0, p0, Lco/uk/getmondo/waitlist/ui/ParallaxAnimationView;->j:Lco/uk/getmondo/waitlist/ui/ParallaxTouchInterceptorView;

    invoke-virtual {v0, p2}, Lco/uk/getmondo/waitlist/ui/ParallaxTouchInterceptorView;->setInitialPosition(Z)V

    .line 106
    return-void
.end method

.method protected onFinishInflate()V
    .locals 4

    .prologue
    .line 45
    invoke-super {p0}, Landroid/widget/FrameLayout;->onFinishInflate()V

    .line 46
    const v0, 0x7f110403

    invoke-virtual {p0, v0}, Lco/uk/getmondo/waitlist/ui/ParallaxAnimationView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/waitlist/ui/ParallaxImageView;

    iput-object v0, p0, Lco/uk/getmondo/waitlist/ui/ParallaxAnimationView;->a:Lco/uk/getmondo/waitlist/ui/ParallaxImageView;

    .line 47
    const v0, 0x7f110404

    invoke-virtual {p0, v0}, Lco/uk/getmondo/waitlist/ui/ParallaxAnimationView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/waitlist/ui/ParallaxImageView;

    iput-object v0, p0, Lco/uk/getmondo/waitlist/ui/ParallaxAnimationView;->b:Lco/uk/getmondo/waitlist/ui/ParallaxImageView;

    .line 48
    const v0, 0x7f110405

    invoke-virtual {p0, v0}, Lco/uk/getmondo/waitlist/ui/ParallaxAnimationView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/waitlist/ui/ParallaxImageView;

    iput-object v0, p0, Lco/uk/getmondo/waitlist/ui/ParallaxAnimationView;->c:Lco/uk/getmondo/waitlist/ui/ParallaxImageView;

    .line 49
    const v0, 0x7f110406

    invoke-virtual {p0, v0}, Lco/uk/getmondo/waitlist/ui/ParallaxAnimationView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/waitlist/ui/ParallaxImageView;

    iput-object v0, p0, Lco/uk/getmondo/waitlist/ui/ParallaxAnimationView;->d:Lco/uk/getmondo/waitlist/ui/ParallaxImageView;

    .line 50
    const v0, 0x7f110407

    invoke-virtual {p0, v0}, Lco/uk/getmondo/waitlist/ui/ParallaxAnimationView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/waitlist/ui/ParallaxImageView;

    iput-object v0, p0, Lco/uk/getmondo/waitlist/ui/ParallaxAnimationView;->e:Lco/uk/getmondo/waitlist/ui/ParallaxImageView;

    .line 51
    const v0, 0x7f110408

    invoke-virtual {p0, v0}, Lco/uk/getmondo/waitlist/ui/ParallaxAnimationView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/waitlist/ui/ParallaxImageView;

    iput-object v0, p0, Lco/uk/getmondo/waitlist/ui/ParallaxAnimationView;->f:Lco/uk/getmondo/waitlist/ui/ParallaxImageView;

    .line 52
    const v0, 0x7f11040a

    invoke-virtual {p0, v0}, Lco/uk/getmondo/waitlist/ui/ParallaxAnimationView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/waitlist/ui/ParallaxLinearLayout;

    iput-object v0, p0, Lco/uk/getmondo/waitlist/ui/ParallaxAnimationView;->g:Lco/uk/getmondo/waitlist/ui/ParallaxLinearLayout;

    .line 53
    const v0, 0x7f110402

    invoke-virtual {p0, v0}, Lco/uk/getmondo/waitlist/ui/ParallaxAnimationView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/waitlist/ui/ParallaxImageView;

    iput-object v0, p0, Lco/uk/getmondo/waitlist/ui/ParallaxAnimationView;->h:Lco/uk/getmondo/waitlist/ui/ParallaxImageView;

    .line 54
    const v0, 0x7f110409

    invoke-virtual {p0, v0}, Lco/uk/getmondo/waitlist/ui/ParallaxAnimationView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/waitlist/ui/ParallaxSwipeToRefresh;

    iput-object v0, p0, Lco/uk/getmondo/waitlist/ui/ParallaxAnimationView;->i:Lco/uk/getmondo/waitlist/ui/ParallaxSwipeToRefresh;

    .line 55
    const v0, 0x7f11040e

    invoke-virtual {p0, v0}, Lco/uk/getmondo/waitlist/ui/ParallaxAnimationView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/waitlist/ui/ParallaxTouchInterceptorView;

    iput-object v0, p0, Lco/uk/getmondo/waitlist/ui/ParallaxAnimationView;->j:Lco/uk/getmondo/waitlist/ui/ParallaxTouchInterceptorView;

    .line 57
    iget-object v0, p0, Lco/uk/getmondo/waitlist/ui/ParallaxAnimationView;->i:Lco/uk/getmondo/waitlist/ui/ParallaxSwipeToRefresh;

    const/4 v1, 0x1

    new-array v1, v1, [I

    const/4 v2, 0x0

    const v3, 0x7f0f0005

    aput v3, v1, v2

    invoke-virtual {v0, v1}, Lco/uk/getmondo/waitlist/ui/ParallaxSwipeToRefresh;->setColorSchemeResources([I)V

    .line 58
    iget-object v0, p0, Lco/uk/getmondo/waitlist/ui/ParallaxAnimationView;->j:Lco/uk/getmondo/waitlist/ui/ParallaxTouchInterceptorView;

    invoke-static {p0}, Lco/uk/getmondo/waitlist/ui/a;->a(Lco/uk/getmondo/waitlist/ui/ParallaxAnimationView;)Lco/uk/getmondo/waitlist/ui/ParallaxTouchInterceptorView$a;

    move-result-object v1

    invoke-virtual {v0, v1}, Lco/uk/getmondo/waitlist/ui/ParallaxTouchInterceptorView;->setOnYAxisScrollChangeListener(Lco/uk/getmondo/waitlist/ui/ParallaxTouchInterceptorView$a;)V

    .line 59
    iget-object v0, p0, Lco/uk/getmondo/waitlist/ui/ParallaxAnimationView;->j:Lco/uk/getmondo/waitlist/ui/ParallaxTouchInterceptorView;

    iget-object v1, p0, Lco/uk/getmondo/waitlist/ui/ParallaxAnimationView;->i:Lco/uk/getmondo/waitlist/ui/ParallaxSwipeToRefresh;

    invoke-virtual {v0, v1}, Lco/uk/getmondo/waitlist/ui/ParallaxTouchInterceptorView;->a(Lco/uk/getmondo/waitlist/ui/ParallaxSwipeToRefresh;)V

    .line 60
    iget-object v0, p0, Lco/uk/getmondo/waitlist/ui/ParallaxAnimationView;->i:Lco/uk/getmondo/waitlist/ui/ParallaxSwipeToRefresh;

    invoke-static {p0}, Lco/uk/getmondo/waitlist/ui/b;->a(Lco/uk/getmondo/waitlist/ui/ParallaxAnimationView;)Landroid/support/v4/widget/SwipeRefreshLayout$b;

    move-result-object v1

    invoke-virtual {v0, v1}, Lco/uk/getmondo/waitlist/ui/ParallaxSwipeToRefresh;->setOnRefreshListener(Landroid/support/v4/widget/SwipeRefreshLayout$b;)V

    .line 61
    invoke-static {p0}, Lco/uk/getmondo/waitlist/ui/c;->a(Lco/uk/getmondo/waitlist/ui/ParallaxAnimationView;)Ljava/lang/Runnable;

    move-result-object v0

    const-wide/16 v2, 0x96

    invoke-virtual {p0, v0, v2, v3}, Lco/uk/getmondo/waitlist/ui/ParallaxAnimationView;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 67
    return-void
.end method

.method public setParallaxAnimationListener(Lco/uk/getmondo/waitlist/ui/ParallaxAnimationView$a;)V
    .locals 4

    .prologue
    .line 89
    iget-object v0, p0, Lco/uk/getmondo/waitlist/ui/ParallaxAnimationView;->i:Lco/uk/getmondo/waitlist/ui/ParallaxSwipeToRefresh;

    const/4 v1, 0x1

    new-array v1, v1, [I

    const/4 v2, 0x0

    const v3, 0x7f0f0005

    aput v3, v1, v2

    invoke-virtual {v0, v1}, Lco/uk/getmondo/waitlist/ui/ParallaxSwipeToRefresh;->setColorSchemeResources([I)V

    .line 91
    invoke-static {p1}, Lcom/c/b/b;->b(Ljava/lang/Object;)Lcom/c/b/b;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/waitlist/ui/ParallaxAnimationView;->k:Lcom/c/b/b;

    .line 92
    return-void
.end method

.method public setRefreshing(Z)V
    .locals 1

    .prologue
    .line 81
    iget-object v0, p0, Lco/uk/getmondo/waitlist/ui/ParallaxAnimationView;->i:Lco/uk/getmondo/waitlist/ui/ParallaxSwipeToRefresh;

    invoke-virtual {v0, p1}, Lco/uk/getmondo/waitlist/ui/ParallaxSwipeToRefresh;->setRefreshing(Z)V

    .line 82
    return-void
.end method
