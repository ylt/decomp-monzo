.class public Lco/uk/getmondo/waitlist/ui/ParallaxTouchInterceptorView;
.super Landroid/widget/FrameLayout;
.source "ParallaxTouchInterceptorView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lco/uk/getmondo/waitlist/ui/ParallaxTouchInterceptorView$a;
    }
.end annotation


# instance fields
.field private a:I

.field private b:Landroid/animation/ObjectAnimator;

.field private c:Lco/uk/getmondo/waitlist/ui/ParallaxTouchInterceptorView$a;

.field private d:F

.field private e:Lco/uk/getmondo/waitlist/ui/ParallaxSwipeToRefresh;

.field private f:I

.field private g:I

.field private h:I

.field private i:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 26
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 27
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lco/uk/getmondo/waitlist/ui/ParallaxTouchInterceptorView;->a(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 28
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 32
    invoke-direct {p0, p1, p2}, Lco/uk/getmondo/waitlist/ui/ParallaxTouchInterceptorView;->a(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 33
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 36
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 37
    invoke-direct {p0, p1, p2}, Lco/uk/getmondo/waitlist/ui/ParallaxTouchInterceptorView;->a(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 38
    return-void
.end method

.method private a(F)I
    .locals 8

    .prologue
    const-wide/high16 v6, 0x3ff8000000000000L    # 1.5

    const/high16 v4, 0x3fc00000    # 1.5f

    .line 106
    const/4 v0, 0x0

    cmpl-float v0, p1, v0

    if-lez v0, :cond_1

    .line 107
    float-to-double v0, p1

    iget v2, p0, Lco/uk/getmondo/waitlist/ui/ParallaxTouchInterceptorView;->i:I

    int-to-double v2, v2

    mul-double/2addr v2, v6

    cmpl-double v0, v0, v2

    if-lez v0, :cond_0

    .line 108
    iget v0, p0, Lco/uk/getmondo/waitlist/ui/ParallaxTouchInterceptorView;->i:I

    int-to-float v0, v0

    mul-float p1, v0, v4

    .line 110
    :cond_0
    iget v0, p0, Lco/uk/getmondo/waitlist/ui/ParallaxTouchInterceptorView;->i:I

    int-to-float v0, v0

    mul-float/2addr v0, v4

    div-float v0, p1, v0

    float-to-double v0, v0

    const-wide/high16 v2, 0x3fc0000000000000L    # 0.125

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v0

    sub-double v0, v6, v0

    float-to-double v2, p1

    mul-double/2addr v0, v2

    double-to-int v0, v0

    .line 115
    :goto_0
    return v0

    .line 112
    :cond_1
    iget v0, p0, Lco/uk/getmondo/waitlist/ui/ParallaxTouchInterceptorView;->h:I

    int-to-float v0, v0

    mul-float/2addr v0, v4

    cmpg-float v0, p1, v0

    if-gez v0, :cond_2

    .line 113
    iget v0, p0, Lco/uk/getmondo/waitlist/ui/ParallaxTouchInterceptorView;->h:I

    int-to-float v0, v0

    mul-float p1, v0, v4

    .line 115
    :cond_2
    iget v0, p0, Lco/uk/getmondo/waitlist/ui/ParallaxTouchInterceptorView;->h:I

    int-to-float v0, v0

    mul-float/2addr v0, v4

    div-float v0, p1, v0

    float-to-double v0, v0

    const-wide v2, 0x3f899999a0000000L    # 0.012500000186264515

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v0

    sub-double v0, v6, v0

    float-to-double v2, p1

    mul-double/2addr v0, v2

    double-to-int v0, v0

    goto :goto_0
.end method

.method private a(III)V
    .locals 4

    .prologue
    .line 124
    iget-object v0, p0, Lco/uk/getmondo/waitlist/ui/ParallaxTouchInterceptorView;->b:Landroid/animation/ObjectAnimator;

    if-eqz v0, :cond_0

    .line 125
    iget-object v0, p0, Lco/uk/getmondo/waitlist/ui/ParallaxTouchInterceptorView;->b:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->cancel()V

    .line 127
    :cond_0
    const-string v0, "animatedPosition"

    const/4 v1, 0x2

    new-array v1, v1, [I

    const/4 v2, 0x0

    aput p1, v1, v2

    const/4 v2, 0x1

    aput p2, v1, v2

    invoke-static {p0, v0, v1}, Landroid/animation/ObjectAnimator;->ofInt(Ljava/lang/Object;Ljava/lang/String;[I)Landroid/animation/ObjectAnimator;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/waitlist/ui/ParallaxTouchInterceptorView;->b:Landroid/animation/ObjectAnimator;

    .line 128
    iget-object v0, p0, Lco/uk/getmondo/waitlist/ui/ParallaxTouchInterceptorView;->b:Landroid/animation/ObjectAnimator;

    new-instance v1, Landroid/view/animation/AccelerateDecelerateInterpolator;

    invoke-direct {v1}, Landroid/view/animation/AccelerateDecelerateInterpolator;-><init>()V

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 129
    iget-object v0, p0, Lco/uk/getmondo/waitlist/ui/ParallaxTouchInterceptorView;->b:Landroid/animation/ObjectAnimator;

    int-to-long v2, p3

    invoke-virtual {v0, v2, v3}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 130
    iget-object v0, p0, Lco/uk/getmondo/waitlist/ui/ParallaxTouchInterceptorView;->b:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    .line 131
    return-void
.end method

.method private a(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 41
    if-nez p2, :cond_0

    .line 53
    :goto_0
    return-void

    .line 44
    :cond_0
    invoke-virtual {p1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v0

    sget-object v1, Lco/uk/getmondo/c$b;->ParallaxTouchInterceptorView:[I

    invoke-virtual {v0, p2, v1, v2, v2}, Landroid/content/res/Resources$Theme;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v1

    .line 46
    const/4 v0, 0x2

    const/4 v2, 0x0

    :try_start_0
    invoke-virtual {v1, v0, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v0

    iput v0, p0, Lco/uk/getmondo/waitlist/ui/ParallaxTouchInterceptorView;->f:I

    .line 47
    const/4 v0, 0x3

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v0

    iput v0, p0, Lco/uk/getmondo/waitlist/ui/ParallaxTouchInterceptorView;->g:I

    .line 48
    const/4 v0, 0x0

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v0

    iput v0, p0, Lco/uk/getmondo/waitlist/ui/ParallaxTouchInterceptorView;->h:I

    .line 49
    const/4 v0, 0x1

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v0

    iput v0, p0, Lco/uk/getmondo/waitlist/ui/ParallaxTouchInterceptorView;->i:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 51
    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    throw v0
.end method


# virtual methods
.method public a()V
    .locals 3

    .prologue
    .line 81
    iget v0, p0, Lco/uk/getmondo/waitlist/ui/ParallaxTouchInterceptorView;->a:I

    iget v1, p0, Lco/uk/getmondo/waitlist/ui/ParallaxTouchInterceptorView;->f:I

    const/16 v2, 0xfa

    invoke-direct {p0, v0, v1, v2}, Lco/uk/getmondo/waitlist/ui/ParallaxTouchInterceptorView;->a(III)V

    .line 82
    return-void
.end method

.method public a(Lco/uk/getmondo/waitlist/ui/ParallaxSwipeToRefresh;)V
    .locals 0

    .prologue
    .line 85
    iput-object p1, p0, Lco/uk/getmondo/waitlist/ui/ParallaxTouchInterceptorView;->e:Lco/uk/getmondo/waitlist/ui/ParallaxSwipeToRefresh;

    .line 86
    return-void
.end method

.method public b()V
    .locals 3

    .prologue
    .line 134
    iget v0, p0, Lco/uk/getmondo/waitlist/ui/ParallaxTouchInterceptorView;->g:I

    iget v1, p0, Lco/uk/getmondo/waitlist/ui/ParallaxTouchInterceptorView;->f:I

    const/16 v2, 0xdac

    invoke-direct {p0, v0, v1, v2}, Lco/uk/getmondo/waitlist/ui/ParallaxTouchInterceptorView;->a(III)V

    .line 135
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/high16 v1, -0x40800000    # -1.0f

    .line 57
    iget-object v0, p0, Lco/uk/getmondo/waitlist/ui/ParallaxTouchInterceptorView;->e:Lco/uk/getmondo/waitlist/ui/ParallaxSwipeToRefresh;

    invoke-virtual {v0, p1}, Lco/uk/getmondo/waitlist/ui/ParallaxSwipeToRefresh;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 58
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 77
    :cond_0
    :goto_0
    return v2

    .line 60
    :pswitch_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    iput v0, p0, Lco/uk/getmondo/waitlist/ui/ParallaxTouchInterceptorView;->d:F

    goto :goto_0

    .line 63
    :pswitch_1
    iput v1, p0, Lco/uk/getmondo/waitlist/ui/ParallaxTouchInterceptorView;->d:F

    .line 64
    invoke-virtual {p0}, Lco/uk/getmondo/waitlist/ui/ParallaxTouchInterceptorView;->a()V

    goto :goto_0

    .line 67
    :pswitch_2
    iput v1, p0, Lco/uk/getmondo/waitlist/ui/ParallaxTouchInterceptorView;->d:F

    .line 68
    invoke-virtual {p0}, Lco/uk/getmondo/waitlist/ui/ParallaxTouchInterceptorView;->a()V

    goto :goto_0

    .line 71
    :pswitch_3
    iget v0, p0, Lco/uk/getmondo/waitlist/ui/ParallaxTouchInterceptorView;->d:F

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_0

    .line 74
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    iget v1, p0, Lco/uk/getmondo/waitlist/ui/ParallaxTouchInterceptorView;->d:F

    sub-float/2addr v0, v1

    neg-float v0, v0

    invoke-virtual {p0, v0}, Lco/uk/getmondo/waitlist/ui/ParallaxTouchInterceptorView;->setPosition(F)V

    goto :goto_0

    .line 58
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_3
        :pswitch_2
    .end packed-switch
.end method

.method public setAnimatedPosition(I)V
    .locals 2

    .prologue
    .line 89
    iput p1, p0, Lco/uk/getmondo/waitlist/ui/ParallaxTouchInterceptorView;->a:I

    .line 90
    iget-object v0, p0, Lco/uk/getmondo/waitlist/ui/ParallaxTouchInterceptorView;->c:Lco/uk/getmondo/waitlist/ui/ParallaxTouchInterceptorView$a;

    int-to-float v1, p1

    invoke-interface {v0, v1}, Lco/uk/getmondo/waitlist/ui/ParallaxTouchInterceptorView$a;->a(F)V

    .line 91
    return-void
.end method

.method public setInitialPosition(Z)V
    .locals 1

    .prologue
    .line 138
    if-eqz p1, :cond_0

    .line 139
    iget v0, p0, Lco/uk/getmondo/waitlist/ui/ParallaxTouchInterceptorView;->g:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/waitlist/ui/ParallaxTouchInterceptorView;->setAnimatedPosition(I)V

    .line 143
    :goto_0
    return-void

    .line 141
    :cond_0
    iget v0, p0, Lco/uk/getmondo/waitlist/ui/ParallaxTouchInterceptorView;->f:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/waitlist/ui/ParallaxTouchInterceptorView;->setAnimatedPosition(I)V

    goto :goto_0
.end method

.method public setOnYAxisScrollChangeListener(Lco/uk/getmondo/waitlist/ui/ParallaxTouchInterceptorView$a;)V
    .locals 0

    .prologue
    .line 120
    iput-object p1, p0, Lco/uk/getmondo/waitlist/ui/ParallaxTouchInterceptorView;->c:Lco/uk/getmondo/waitlist/ui/ParallaxTouchInterceptorView$a;

    .line 121
    return-void
.end method

.method public setPosition(F)V
    .locals 2

    .prologue
    .line 94
    invoke-direct {p0, p1}, Lco/uk/getmondo/waitlist/ui/ParallaxTouchInterceptorView;->a(F)I

    move-result v0

    int-to-float v0, v0

    .line 95
    iget v1, p0, Lco/uk/getmondo/waitlist/ui/ParallaxTouchInterceptorView;->h:I

    int-to-float v1, v1

    cmpg-float v1, v0, v1

    if-gez v1, :cond_0

    .line 96
    iget v0, p0, Lco/uk/getmondo/waitlist/ui/ParallaxTouchInterceptorView;->h:I

    int-to-float v0, v0

    .line 98
    :cond_0
    iget v1, p0, Lco/uk/getmondo/waitlist/ui/ParallaxTouchInterceptorView;->i:I

    int-to-float v1, v1

    cmpl-float v1, v0, v1

    if-lez v1, :cond_1

    .line 99
    iget v0, p0, Lco/uk/getmondo/waitlist/ui/ParallaxTouchInterceptorView;->i:I

    int-to-float v0, v0

    .line 101
    :cond_1
    iget v1, p0, Lco/uk/getmondo/waitlist/ui/ParallaxTouchInterceptorView;->f:I

    float-to-int v0, v0

    add-int/2addr v0, v1

    iput v0, p0, Lco/uk/getmondo/waitlist/ui/ParallaxTouchInterceptorView;->a:I

    .line 102
    iget-object v0, p0, Lco/uk/getmondo/waitlist/ui/ParallaxTouchInterceptorView;->c:Lco/uk/getmondo/waitlist/ui/ParallaxTouchInterceptorView$a;

    iget v1, p0, Lco/uk/getmondo/waitlist/ui/ParallaxTouchInterceptorView;->a:I

    int-to-float v1, v1

    invoke-interface {v0, v1}, Lco/uk/getmondo/waitlist/ui/ParallaxTouchInterceptorView$a;->a(F)V

    .line 103
    return-void
.end method
