.class public Lco/uk/getmondo/waitlist/ShareActivity_ViewBinding;
.super Ljava/lang/Object;
.source "ShareActivity_ViewBinding.java"

# interfaces
.implements Lbutterknife/Unbinder;


# instance fields
.field private a:Lco/uk/getmondo/waitlist/ShareActivity;

.field private b:Landroid/view/View;


# direct methods
.method public constructor <init>(Lco/uk/getmondo/waitlist/ShareActivity;Landroid/view/View;)V
    .locals 3

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    iput-object p1, p0, Lco/uk/getmondo/waitlist/ShareActivity_ViewBinding;->a:Lco/uk/getmondo/waitlist/ShareActivity;

    .line 30
    const v0, 0x7f110218

    const-string v1, "field \'link\'"

    const-class v2, Landroid/widget/TextView;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p1, Lco/uk/getmondo/waitlist/ShareActivity;->link:Landroid/widget/TextView;

    .line 31
    const v0, 0x7f110219

    const-string v1, "field \'description\'"

    const-class v2, Landroid/widget/TextView;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p1, Lco/uk/getmondo/waitlist/ShareActivity;->description:Landroid/widget/TextView;

    .line 32
    const v0, 0x7f11021a

    const-string v1, "method \'share\'"

    invoke-static {p2, v0, v1}, Lbutterknife/internal/Utils;->findRequiredView(Landroid/view/View;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 33
    iput-object v0, p0, Lco/uk/getmondo/waitlist/ShareActivity_ViewBinding;->b:Landroid/view/View;

    .line 34
    new-instance v1, Lco/uk/getmondo/waitlist/ShareActivity_ViewBinding$1;

    invoke-direct {v1, p0, p1}, Lco/uk/getmondo/waitlist/ShareActivity_ViewBinding$1;-><init>(Lco/uk/getmondo/waitlist/ShareActivity_ViewBinding;Lco/uk/getmondo/waitlist/ShareActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 40
    return-void
.end method


# virtual methods
.method public unbind()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 45
    iget-object v0, p0, Lco/uk/getmondo/waitlist/ShareActivity_ViewBinding;->a:Lco/uk/getmondo/waitlist/ShareActivity;

    .line 46
    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Bindings already cleared."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 47
    :cond_0
    iput-object v1, p0, Lco/uk/getmondo/waitlist/ShareActivity_ViewBinding;->a:Lco/uk/getmondo/waitlist/ShareActivity;

    .line 49
    iput-object v1, v0, Lco/uk/getmondo/waitlist/ShareActivity;->link:Landroid/widget/TextView;

    .line 50
    iput-object v1, v0, Lco/uk/getmondo/waitlist/ShareActivity;->description:Landroid/widget/TextView;

    .line 52
    iget-object v0, p0, Lco/uk/getmondo/waitlist/ShareActivity_ViewBinding;->b:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 53
    iput-object v1, p0, Lco/uk/getmondo/waitlist/ShareActivity_ViewBinding;->b:Landroid/view/View;

    .line 54
    return-void
.end method
