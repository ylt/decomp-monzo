.class public Lco/uk/getmondo/waitlist/WaitlistActivity_ViewBinding;
.super Ljava/lang/Object;
.source "WaitlistActivity_ViewBinding.java"

# interfaces
.implements Lbutterknife/Unbinder;


# instance fields
.field private a:Lco/uk/getmondo/waitlist/WaitlistActivity;


# direct methods
.method public constructor <init>(Lco/uk/getmondo/waitlist/WaitlistActivity;Landroid/view/View;)V
    .locals 3

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    iput-object p1, p0, Lco/uk/getmondo/waitlist/WaitlistActivity_ViewBinding;->a:Lco/uk/getmondo/waitlist/WaitlistActivity;

    .line 27
    const v0, 0x7f110277

    const-string v1, "field \'parallaxAnimationView\'"

    const-class v2, Lco/uk/getmondo/waitlist/ui/ParallaxAnimationView;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/waitlist/ui/ParallaxAnimationView;

    iput-object v0, p1, Lco/uk/getmondo/waitlist/WaitlistActivity;->parallaxAnimationView:Lco/uk/getmondo/waitlist/ui/ParallaxAnimationView;

    .line 28
    const v0, 0x7f110276

    const-string v1, "field \'threeFingerView\'"

    const-class v2, Lco/uk/getmondo/waitlist/TouchInterceptingRelativeLayout;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/waitlist/TouchInterceptingRelativeLayout;

    iput-object v0, p1, Lco/uk/getmondo/waitlist/WaitlistActivity;->threeFingerView:Lco/uk/getmondo/waitlist/TouchInterceptingRelativeLayout;

    .line 29
    const v0, 0x7f110279

    const-string v1, "field \'inviteFriendsTextView\'"

    const-class v2, Landroid/widget/TextView;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p1, Lco/uk/getmondo/waitlist/WaitlistActivity;->inviteFriendsTextView:Landroid/widget/TextView;

    .line 30
    const v0, 0x7f11027a

    const-string v1, "field \'bumpButton\'"

    invoke-static {p2, v0, v1}, Lbutterknife/internal/Utils;->findRequiredView(Landroid/view/View;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p1, Lco/uk/getmondo/waitlist/WaitlistActivity;->bumpButton:Landroid/view/View;

    .line 31
    return-void
.end method


# virtual methods
.method public unbind()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 36
    iget-object v0, p0, Lco/uk/getmondo/waitlist/WaitlistActivity_ViewBinding;->a:Lco/uk/getmondo/waitlist/WaitlistActivity;

    .line 37
    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Bindings already cleared."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 38
    :cond_0
    iput-object v1, p0, Lco/uk/getmondo/waitlist/WaitlistActivity_ViewBinding;->a:Lco/uk/getmondo/waitlist/WaitlistActivity;

    .line 40
    iput-object v1, v0, Lco/uk/getmondo/waitlist/WaitlistActivity;->parallaxAnimationView:Lco/uk/getmondo/waitlist/ui/ParallaxAnimationView;

    .line 41
    iput-object v1, v0, Lco/uk/getmondo/waitlist/WaitlistActivity;->threeFingerView:Lco/uk/getmondo/waitlist/TouchInterceptingRelativeLayout;

    .line 42
    iput-object v1, v0, Lco/uk/getmondo/waitlist/WaitlistActivity;->inviteFriendsTextView:Landroid/widget/TextView;

    .line 43
    iput-object v1, v0, Lco/uk/getmondo/waitlist/WaitlistActivity;->bumpButton:Landroid/view/View;

    .line 44
    return-void
.end method
