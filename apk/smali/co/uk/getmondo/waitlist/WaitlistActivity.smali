.class public Lco/uk/getmondo/waitlist/WaitlistActivity;
.super Lco/uk/getmondo/common/activities/b;
.source "WaitlistActivity.java"

# interfaces
.implements Landroid/support/v4/widget/SwipeRefreshLayout$b;
.implements Lco/uk/getmondo/waitlist/c$a;
.implements Lco/uk/getmondo/waitlist/ui/ParallaxAnimationView$a;


# instance fields
.field a:Lco/uk/getmondo/waitlist/c;

.field bumpButton:Landroid/view/View;
    .annotation build Lbutterknife/BindView;
        value = 0x7f11027a
    .end annotation
.end field

.field inviteFriendsTextView:Landroid/widget/TextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f110279
    .end annotation
.end field

.field parallaxAnimationView:Lco/uk/getmondo/waitlist/ui/ParallaxAnimationView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f110277
    .end annotation
.end field

.field threeFingerView:Lco/uk/getmondo/waitlist/TouchInterceptingRelativeLayout;
    .annotation build Lbutterknife/BindView;
        value = 0x7f110276
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Lco/uk/getmondo/common/activities/b;-><init>()V

    return-void
.end method

.method public static a(Landroid/content/Context;)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 46
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lco/uk/getmondo/waitlist/WaitlistActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    return-object v0
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lco/uk/getmondo/waitlist/WaitlistActivity;->a:Lco/uk/getmondo/waitlist/c;

    invoke-virtual {v0}, Lco/uk/getmondo/waitlist/c;->a()V

    .line 83
    return-void
.end method

.method public a(I)V
    .locals 1

    .prologue
    .line 143
    iget-object v0, p0, Lco/uk/getmondo/waitlist/WaitlistActivity;->inviteFriendsTextView:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(I)V

    .line 144
    return-void
.end method

.method public a(Lco/uk/getmondo/d/an;)V
    .locals 1

    .prologue
    .line 87
    iget-object v0, p0, Lco/uk/getmondo/waitlist/WaitlistActivity;->parallaxAnimationView:Lco/uk/getmondo/waitlist/ui/ParallaxAnimationView;

    invoke-virtual {v0, p1}, Lco/uk/getmondo/waitlist/ui/ParallaxAnimationView;->a(Lco/uk/getmondo/d/an;)V

    .line 88
    return-void
.end method

.method public a(Lco/uk/getmondo/d/an;Z)V
    .locals 1

    .prologue
    .line 92
    iget-object v0, p0, Lco/uk/getmondo/waitlist/WaitlistActivity;->parallaxAnimationView:Lco/uk/getmondo/waitlist/ui/ParallaxAnimationView;

    invoke-virtual {v0, p1, p2}, Lco/uk/getmondo/waitlist/ui/ParallaxAnimationView;->a(Lco/uk/getmondo/d/an;Z)V

    .line 93
    return-void
.end method

.method public a(Lco/uk/getmondo/d/k;)V
    .locals 1

    .prologue
    .line 98
    invoke-virtual {p1}, Lco/uk/getmondo/d/k;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 99
    invoke-static {p0, p1}, Lco/uk/getmondo/bump_up/WebEventActivity;->a(Landroid/app/Activity;Lco/uk/getmondo/d/k;)V

    .line 103
    :goto_0
    return-void

    .line 101
    :cond_0
    invoke-static {p0, p1}, Lco/uk/getmondo/bump_up/BumpUpActivity;->a(Landroid/app/Activity;Lco/uk/getmondo/d/k;)V

    goto :goto_0
.end method

.method public a(Z)V
    .locals 2

    .prologue
    .line 116
    iget-object v1, p0, Lco/uk/getmondo/waitlist/WaitlistActivity;->bumpButton:Landroid/view/View;

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 117
    return-void

    .line 116
    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public b()V
    .locals 1

    .prologue
    .line 107
    iget-object v0, p0, Lco/uk/getmondo/waitlist/WaitlistActivity;->parallaxAnimationView:Lco/uk/getmondo/waitlist/ui/ParallaxAnimationView;

    invoke-virtual {v0}, Lco/uk/getmondo/waitlist/ui/ParallaxAnimationView;->a()V

    .line 108
    return-void
.end method

.method public c()V
    .locals 0

    .prologue
    .line 112
    invoke-static {p0}, Lco/uk/getmondo/top/NotInCountryActivity;->b(Landroid/content/Context;)V

    .line 113
    return-void
.end method

.method public d()Lio/reactivex/n;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 130
    iget-object v0, p0, Lco/uk/getmondo/waitlist/WaitlistActivity;->bumpButton:Landroid/view/View;

    invoke-static {v0}, Lcom/b/a/c/c;->a(Landroid/view/View;)Lio/reactivex/n;

    move-result-object v0

    return-object v0
.end method

.method public e()V
    .locals 0

    .prologue
    .line 134
    invoke-static {p0}, Lco/uk/getmondo/waitlist/ShareActivity;->a(Landroid/app/Activity;)V

    .line 135
    return-void
.end method

.method public f()V
    .locals 1

    .prologue
    .line 138
    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lco/uk/getmondo/waitlist/WaitlistActivity;->setResult(I)V

    .line 139
    invoke-virtual {p0}, Lco/uk/getmondo/waitlist/WaitlistActivity;->finish()V

    .line 140
    return-void
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 2

    .prologue
    .line 51
    invoke-super {p0, p1, p2, p3}, Lco/uk/getmondo/common/activities/b;->onActivityResult(IILandroid/content/Intent;)V

    .line 52
    invoke-static {p1}, Lco/uk/getmondo/bump_up/BumpUpActivity;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 53
    iget-object v0, p0, Lco/uk/getmondo/waitlist/WaitlistActivity;->a:Lco/uk/getmondo/waitlist/c;

    invoke-virtual {v0}, Lco/uk/getmondo/waitlist/c;->d()V

    .line 54
    iget-object v0, p0, Lco/uk/getmondo/waitlist/WaitlistActivity;->parallaxAnimationView:Lco/uk/getmondo/waitlist/ui/ParallaxAnimationView;

    invoke-static {p3}, Lco/uk/getmondo/bump_up/BumpUpActivity;->a(Landroid/content/Intent;)I

    move-result v1

    invoke-virtual {v0, v1}, Lco/uk/getmondo/waitlist/ui/ParallaxAnimationView;->a(I)V

    .line 56
    :cond_0
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 60
    invoke-super {p0, p1}, Lco/uk/getmondo/common/activities/b;->onCreate(Landroid/os/Bundle;)V

    .line 61
    const v0, 0x7f050075

    invoke-virtual {p0, v0}, Lco/uk/getmondo/waitlist/WaitlistActivity;->setContentView(I)V

    .line 62
    invoke-static {p0}, Lbutterknife/ButterKnife;->bind(Landroid/app/Activity;)Lbutterknife/Unbinder;

    .line 63
    invoke-virtual {p0}, Lco/uk/getmondo/waitlist/WaitlistActivity;->l()Lco/uk/getmondo/common/h/b/b;

    move-result-object v0

    invoke-interface {v0, p0}, Lco/uk/getmondo/common/h/b/b;->a(Lco/uk/getmondo/waitlist/WaitlistActivity;)V

    .line 64
    iget-object v0, p0, Lco/uk/getmondo/waitlist/WaitlistActivity;->a:Lco/uk/getmondo/waitlist/c;

    invoke-virtual {v0, p0}, Lco/uk/getmondo/waitlist/c;->a(Lco/uk/getmondo/waitlist/c$a;)V

    .line 65
    iget-object v0, p0, Lco/uk/getmondo/waitlist/WaitlistActivity;->parallaxAnimationView:Lco/uk/getmondo/waitlist/ui/ParallaxAnimationView;

    invoke-virtual {v0, p0}, Lco/uk/getmondo/waitlist/ui/ParallaxAnimationView;->setParallaxAnimationListener(Lco/uk/getmondo/waitlist/ui/ParallaxAnimationView$a;)V

    .line 66
    return-void
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 70
    iget-object v0, p0, Lco/uk/getmondo/waitlist/WaitlistActivity;->a:Lco/uk/getmondo/waitlist/c;

    invoke-virtual {v0}, Lco/uk/getmondo/waitlist/c;->b()V

    .line 71
    invoke-super {p0}, Lco/uk/getmondo/common/activities/b;->onDestroy()V

    .line 72
    return-void
.end method

.method protected onResume()V
    .locals 1

    .prologue
    .line 76
    invoke-super {p0}, Lco/uk/getmondo/common/activities/b;->onResume()V

    .line 77
    iget-object v0, p0, Lco/uk/getmondo/waitlist/WaitlistActivity;->a:Lco/uk/getmondo/waitlist/c;

    invoke-virtual {v0}, Lco/uk/getmondo/waitlist/c;->c()V

    .line 78
    return-void
.end method

.method public s()V
    .locals 2

    .prologue
    .line 121
    iget-object v0, p0, Lco/uk/getmondo/waitlist/WaitlistActivity;->parallaxAnimationView:Lco/uk/getmondo/waitlist/ui/ParallaxAnimationView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lco/uk/getmondo/waitlist/ui/ParallaxAnimationView;->setRefreshing(Z)V

    .line 122
    return-void
.end method

.method public t()V
    .locals 2

    .prologue
    .line 126
    iget-object v0, p0, Lco/uk/getmondo/waitlist/WaitlistActivity;->parallaxAnimationView:Lco/uk/getmondo/waitlist/ui/ParallaxAnimationView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lco/uk/getmondo/waitlist/ui/ParallaxAnimationView;->setRefreshing(Z)V

    .line 127
    return-void
.end method
