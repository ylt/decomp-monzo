.class public Lco/uk/getmondo/waitlist/c;
.super Lco/uk/getmondo/common/ui/b;
.source "WaitlistPresenter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lco/uk/getmondo/waitlist/c$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lco/uk/getmondo/common/ui/b",
        "<",
        "Lco/uk/getmondo/waitlist/c$a;",
        ">;"
    }
.end annotation


# instance fields
.field private final c:Lio/reactivex/u;

.field private final d:Lio/reactivex/u;

.field private final e:Lco/uk/getmondo/common/e/a;

.field private final f:Lco/uk/getmondo/common/accounts/d;

.field private final g:Lco/uk/getmondo/api/b/a;

.field private final h:Lco/uk/getmondo/common/a;

.field private final i:Lco/uk/getmondo/common/s;

.field private final j:Lco/uk/getmondo/waitlist/i;

.field private k:Z


# direct methods
.method constructor <init>(Lio/reactivex/u;Lio/reactivex/u;Lco/uk/getmondo/common/e/a;Lco/uk/getmondo/common/accounts/d;Lco/uk/getmondo/api/b/a;Lco/uk/getmondo/common/s;Lco/uk/getmondo/waitlist/i;Lco/uk/getmondo/common/a;)V
    .locals 1

    .prologue
    .line 42
    invoke-direct {p0}, Lco/uk/getmondo/common/ui/b;-><init>()V

    .line 37
    const/4 v0, 0x0

    iput-boolean v0, p0, Lco/uk/getmondo/waitlist/c;->k:Z

    .line 43
    iput-object p1, p0, Lco/uk/getmondo/waitlist/c;->c:Lio/reactivex/u;

    .line 44
    iput-object p2, p0, Lco/uk/getmondo/waitlist/c;->d:Lio/reactivex/u;

    .line 45
    iput-object p3, p0, Lco/uk/getmondo/waitlist/c;->e:Lco/uk/getmondo/common/e/a;

    .line 46
    iput-object p4, p0, Lco/uk/getmondo/waitlist/c;->f:Lco/uk/getmondo/common/accounts/d;

    .line 47
    iput-object p5, p0, Lco/uk/getmondo/waitlist/c;->g:Lco/uk/getmondo/api/b/a;

    .line 48
    iput-object p6, p0, Lco/uk/getmondo/waitlist/c;->i:Lco/uk/getmondo/common/s;

    .line 49
    iput-object p7, p0, Lco/uk/getmondo/waitlist/c;->j:Lco/uk/getmondo/waitlist/i;

    .line 50
    iput-object p8, p0, Lco/uk/getmondo/waitlist/c;->h:Lco/uk/getmondo/common/a;

    .line 51
    return-void
.end method

.method static synthetic a(Lco/uk/getmondo/waitlist/c;Lco/uk/getmondo/d/an;)Lco/uk/getmondo/d/ak;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 85
    iget-object v0, p0, Lco/uk/getmondo/waitlist/c;->f:Lco/uk/getmondo/common/accounts/d;

    invoke-virtual {v0}, Lco/uk/getmondo/common/accounts/d;->b()Lco/uk/getmondo/d/ak;

    move-result-object v0

    return-object v0
.end method

.method private a(Lco/uk/getmondo/d/ak;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 102
    if-nez p1, :cond_1

    .line 103
    iget-object v0, p0, Lco/uk/getmondo/waitlist/c;->a:Lco/uk/getmondo/common/ui/f;

    check-cast v0, Lco/uk/getmondo/waitlist/c$a;

    const v1, 0x7f0a00c1

    invoke-interface {v0, v1}, Lco/uk/getmondo/waitlist/c$a;->b(I)V

    .line 144
    :cond_0
    :goto_0
    return-void

    .line 107
    :cond_1
    invoke-virtual {p1}, Lco/uk/getmondo/d/ak;->e()Lco/uk/getmondo/d/an;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 112
    invoke-virtual {p1}, Lco/uk/getmondo/d/ak;->e()Lco/uk/getmondo/d/an;

    move-result-object v0

    invoke-virtual {v0}, Lco/uk/getmondo/d/an;->f()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 113
    iget-object v0, p0, Lco/uk/getmondo/waitlist/c;->a:Lco/uk/getmondo/common/ui/f;

    check-cast v0, Lco/uk/getmondo/waitlist/c$a;

    invoke-interface {v0}, Lco/uk/getmondo/waitlist/c$a;->f()V

    goto :goto_0

    .line 117
    :cond_2
    invoke-virtual {p1}, Lco/uk/getmondo/d/ak;->e()Lco/uk/getmondo/d/an;

    move-result-object v0

    invoke-virtual {v0}, Lco/uk/getmondo/d/an;->a()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 118
    iget-object v0, p0, Lco/uk/getmondo/waitlist/c;->a:Lco/uk/getmondo/common/ui/f;

    check-cast v0, Lco/uk/getmondo/waitlist/c$a;

    invoke-interface {v0}, Lco/uk/getmondo/waitlist/c$a;->c()V

    goto :goto_0

    .line 122
    :cond_3
    invoke-virtual {p1}, Lco/uk/getmondo/d/ak;->e()Lco/uk/getmondo/d/an;

    move-result-object v0

    invoke-virtual {v0}, Lco/uk/getmondo/d/an;->i()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 123
    iget-object v0, p0, Lco/uk/getmondo/waitlist/c;->a:Lco/uk/getmondo/common/ui/f;

    check-cast v0, Lco/uk/getmondo/waitlist/c$a;

    invoke-interface {v0, v3}, Lco/uk/getmondo/waitlist/c$a;->a(Z)V

    .line 124
    iget-object v0, p0, Lco/uk/getmondo/waitlist/c;->a:Lco/uk/getmondo/common/ui/f;

    check-cast v0, Lco/uk/getmondo/waitlist/c$a;

    const v1, 0x7f0a0147

    invoke-interface {v0, v1}, Lco/uk/getmondo/waitlist/c$a;->a(I)V

    .line 133
    :goto_1
    iget-boolean v0, p0, Lco/uk/getmondo/waitlist/c;->k:Z

    if-eqz v0, :cond_6

    invoke-virtual {p1}, Lco/uk/getmondo/d/ak;->e()Lco/uk/getmondo/d/an;

    move-result-object v0

    invoke-direct {p0, v0}, Lco/uk/getmondo/waitlist/c;->a(Lco/uk/getmondo/d/an;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 134
    iget-object v0, p0, Lco/uk/getmondo/waitlist/c;->a:Lco/uk/getmondo/common/ui/f;

    check-cast v0, Lco/uk/getmondo/waitlist/c$a;

    invoke-interface {v0}, Lco/uk/getmondo/waitlist/c$a;->b()V

    goto :goto_0

    .line 125
    :cond_4
    invoke-virtual {p1}, Lco/uk/getmondo/d/ak;->e()Lco/uk/getmondo/d/an;

    move-result-object v0

    invoke-virtual {v0}, Lco/uk/getmondo/d/an;->d()I

    move-result v0

    if-nez v0, :cond_5

    .line 126
    iget-object v0, p0, Lco/uk/getmondo/waitlist/c;->a:Lco/uk/getmondo/common/ui/f;

    check-cast v0, Lco/uk/getmondo/waitlist/c$a;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lco/uk/getmondo/waitlist/c$a;->a(Z)V

    .line 127
    iget-object v0, p0, Lco/uk/getmondo/waitlist/c;->a:Lco/uk/getmondo/common/ui/f;

    check-cast v0, Lco/uk/getmondo/waitlist/c$a;

    const v1, 0x7f0a0455

    invoke-interface {v0, v1}, Lco/uk/getmondo/waitlist/c$a;->a(I)V

    goto :goto_1

    .line 129
    :cond_5
    iget-object v0, p0, Lco/uk/getmondo/waitlist/c;->a:Lco/uk/getmondo/common/ui/f;

    check-cast v0, Lco/uk/getmondo/waitlist/c$a;

    invoke-interface {v0, v3}, Lco/uk/getmondo/waitlist/c$a;->a(Z)V

    .line 130
    iget-object v0, p0, Lco/uk/getmondo/waitlist/c;->a:Lco/uk/getmondo/common/ui/f;

    check-cast v0, Lco/uk/getmondo/waitlist/c$a;

    const v1, 0x7f0a0454

    invoke-interface {v0, v1}, Lco/uk/getmondo/waitlist/c$a;->a(I)V

    goto :goto_1

    .line 138
    :cond_6
    iget-boolean v0, p0, Lco/uk/getmondo/waitlist/c;->k:Z

    if-nez v0, :cond_7

    .line 139
    iget-object v0, p0, Lco/uk/getmondo/waitlist/c;->a:Lco/uk/getmondo/common/ui/f;

    check-cast v0, Lco/uk/getmondo/waitlist/c$a;

    invoke-virtual {p1}, Lco/uk/getmondo/d/ak;->e()Lco/uk/getmondo/d/an;

    move-result-object v1

    iget-object v2, p0, Lco/uk/getmondo/waitlist/c;->i:Lco/uk/getmondo/common/s;

    invoke-interface {v2}, Lco/uk/getmondo/common/s;->e()Z

    move-result v2

    invoke-interface {v0, v1, v2}, Lco/uk/getmondo/waitlist/c$a;->a(Lco/uk/getmondo/d/an;Z)V

    .line 140
    iput-boolean v3, p0, Lco/uk/getmondo/waitlist/c;->k:Z

    goto/16 :goto_0

    .line 142
    :cond_7
    iget-object v0, p0, Lco/uk/getmondo/waitlist/c;->a:Lco/uk/getmondo/common/ui/f;

    check-cast v0, Lco/uk/getmondo/waitlist/c$a;

    invoke-virtual {p1}, Lco/uk/getmondo/d/ak;->e()Lco/uk/getmondo/d/an;

    move-result-object v1

    invoke-interface {v0, v1}, Lco/uk/getmondo/waitlist/c$a;->a(Lco/uk/getmondo/d/an;)V

    goto/16 :goto_0
.end method

.method static synthetic a(Lco/uk/getmondo/waitlist/c$a;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 58
    invoke-interface {p0}, Lco/uk/getmondo/waitlist/c$a;->e()V

    return-void
.end method

.method static synthetic a(Lco/uk/getmondo/waitlist/c;Ljava/lang/Throwable;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 93
    iget-object v1, p0, Lco/uk/getmondo/waitlist/c;->e:Lco/uk/getmondo/common/e/a;

    iget-object v0, p0, Lco/uk/getmondo/waitlist/c;->a:Lco/uk/getmondo/common/ui/f;

    check-cast v0, Lco/uk/getmondo/common/e/a$a;

    invoke-virtual {v1, p1, v0}, Lco/uk/getmondo/common/e/a;->a(Ljava/lang/Throwable;Lco/uk/getmondo/common/e/a$a;)Z

    return-void
.end method

.method static synthetic a(Lco/uk/getmondo/waitlist/c;ZLco/uk/getmondo/d/ak;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 89
    if-eqz p1, :cond_0

    .line 90
    iget-object v0, p0, Lco/uk/getmondo/waitlist/c;->a:Lco/uk/getmondo/common/ui/f;

    check-cast v0, Lco/uk/getmondo/waitlist/c$a;

    invoke-interface {v0}, Lco/uk/getmondo/waitlist/c$a;->t()V

    .line 92
    :cond_0
    invoke-direct {p0, p2}, Lco/uk/getmondo/waitlist/c;->a(Lco/uk/getmondo/d/ak;)V

    .line 93
    return-void
.end method

.method private a(Z)V
    .locals 3

    .prologue
    .line 80
    if-eqz p1, :cond_0

    .line 81
    iget-object v0, p0, Lco/uk/getmondo/waitlist/c;->a:Lco/uk/getmondo/common/ui/f;

    check-cast v0, Lco/uk/getmondo/waitlist/c$a;

    invoke-interface {v0}, Lco/uk/getmondo/waitlist/c$a;->s()V

    .line 84
    :cond_0
    iget-object v0, p0, Lco/uk/getmondo/waitlist/c;->g:Lco/uk/getmondo/api/b/a;

    invoke-virtual {v0}, Lco/uk/getmondo/api/b/a;->b()Lio/reactivex/v;

    move-result-object v0

    invoke-static {p0}, Lco/uk/getmondo/waitlist/e;->a(Lco/uk/getmondo/waitlist/c;)Lio/reactivex/c/h;

    move-result-object v1

    .line 85
    invoke-virtual {v0, v1}, Lio/reactivex/v;->d(Lio/reactivex/c/h;)Lio/reactivex/v;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/waitlist/c;->d:Lio/reactivex/u;

    .line 86
    invoke-virtual {v0, v1}, Lio/reactivex/v;->b(Lio/reactivex/u;)Lio/reactivex/v;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/waitlist/c;->c:Lio/reactivex/u;

    .line 87
    invoke-virtual {v0, v1}, Lio/reactivex/v;->a(Lio/reactivex/u;)Lio/reactivex/v;

    move-result-object v0

    invoke-static {p0, p1}, Lco/uk/getmondo/waitlist/f;->a(Lco/uk/getmondo/waitlist/c;Z)Lio/reactivex/c/g;

    move-result-object v1

    invoke-static {p0}, Lco/uk/getmondo/waitlist/g;->a(Lco/uk/getmondo/waitlist/c;)Lio/reactivex/c/g;

    move-result-object v2

    .line 88
    invoke-virtual {v0, v1, v2}, Lio/reactivex/v;->a(Lio/reactivex/c/g;Lio/reactivex/c/g;)Lio/reactivex/b/b;

    move-result-object v0

    .line 84
    invoke-virtual {p0, v0}, Lco/uk/getmondo/waitlist/c;->a(Lio/reactivex/b/b;)V

    .line 94
    return-void
.end method

.method private a(Lco/uk/getmondo/d/an;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 147
    if-nez p1, :cond_1

    .line 158
    :cond_0
    :goto_0
    return v0

    .line 150
    :cond_1
    iget-object v1, p0, Lco/uk/getmondo/waitlist/c;->i:Lco/uk/getmondo/common/s;

    invoke-interface {v1}, Lco/uk/getmondo/common/s;->f()Ljava/lang/String;

    move-result-object v1

    .line 151
    invoke-virtual {p1, v1}, Lco/uk/getmondo/d/an;->a(Ljava/lang/String;)Lco/uk/getmondo/d/k;

    move-result-object v2

    .line 152
    if-eqz v2, :cond_0

    .line 155
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "|"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v2}, Lco/uk/getmondo/d/k;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 156
    iget-object v1, p0, Lco/uk/getmondo/waitlist/c;->i:Lco/uk/getmondo/common/s;

    invoke-interface {v1, v0}, Lco/uk/getmondo/common/s;->e(Ljava/lang/String;)V

    .line 157
    iget-object v0, p0, Lco/uk/getmondo/waitlist/c;->a:Lco/uk/getmondo/common/ui/f;

    check-cast v0, Lco/uk/getmondo/waitlist/c$a;

    invoke-interface {v0, v2}, Lco/uk/getmondo/waitlist/c$a;->a(Lco/uk/getmondo/d/k;)V

    .line 158
    const/4 v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method a()V
    .locals 1

    .prologue
    .line 72
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lco/uk/getmondo/waitlist/c;->a(Z)V

    .line 73
    return-void
.end method

.method public bridge synthetic a(Lco/uk/getmondo/common/ui/f;)V
    .locals 0

    .prologue
    .line 26
    check-cast p1, Lco/uk/getmondo/waitlist/c$a;

    invoke-virtual {p0, p1}, Lco/uk/getmondo/waitlist/c;->a(Lco/uk/getmondo/waitlist/c$a;)V

    return-void
.end method

.method public a(Lco/uk/getmondo/waitlist/c$a;)V
    .locals 2

    .prologue
    .line 55
    invoke-super {p0, p1}, Lco/uk/getmondo/common/ui/b;->a(Lco/uk/getmondo/common/ui/f;)V

    .line 57
    invoke-interface {p1}, Lco/uk/getmondo/waitlist/c$a;->d()Lio/reactivex/n;

    move-result-object v0

    invoke-static {p1}, Lco/uk/getmondo/waitlist/d;->a(Lco/uk/getmondo/waitlist/c$a;)Lio/reactivex/c/g;

    move-result-object v1

    .line 58
    invoke-virtual {v0, v1}, Lio/reactivex/n;->subscribe(Lio/reactivex/c/g;)Lio/reactivex/b/b;

    move-result-object v0

    .line 57
    invoke-virtual {p0, v0}, Lco/uk/getmondo/waitlist/c;->a(Lio/reactivex/b/b;)V

    .line 60
    iget-object v0, p0, Lco/uk/getmondo/waitlist/c;->i:Lco/uk/getmondo/common/s;

    invoke-interface {v0}, Lco/uk/getmondo/common/s;->e()Z

    move-result v0

    .line 61
    if-eqz v0, :cond_0

    .line 62
    iget-object v0, p0, Lco/uk/getmondo/waitlist/c;->h:Lco/uk/getmondo/common/a;

    invoke-static {}, Lco/uk/getmondo/api/model/tracking/Impression;->d()Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v1

    invoke-virtual {v0, v1}, Lco/uk/getmondo/common/a;->a(Lco/uk/getmondo/api/model/tracking/Impression;)V

    .line 63
    iget-object v0, p0, Lco/uk/getmondo/waitlist/c;->i:Lco/uk/getmondo/common/s;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lco/uk/getmondo/common/s;->a(Z)V

    .line 67
    :goto_0
    iget-object v0, p0, Lco/uk/getmondo/waitlist/c;->f:Lco/uk/getmondo/common/accounts/d;

    invoke-virtual {v0}, Lco/uk/getmondo/common/accounts/d;->b()Lco/uk/getmondo/d/ak;

    move-result-object v0

    .line 68
    invoke-direct {p0, v0}, Lco/uk/getmondo/waitlist/c;->a(Lco/uk/getmondo/d/ak;)V

    .line 69
    return-void

    .line 65
    :cond_0
    iget-object v0, p0, Lco/uk/getmondo/waitlist/c;->h:Lco/uk/getmondo/common/a;

    invoke-static {}, Lco/uk/getmondo/api/model/tracking/Impression;->e()Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v1

    invoke-virtual {v0, v1}, Lco/uk/getmondo/common/a;->a(Lco/uk/getmondo/api/model/tracking/Impression;)V

    goto :goto_0
.end method

.method c()V
    .locals 1

    .prologue
    .line 76
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lco/uk/getmondo/waitlist/c;->a(Z)V

    .line 77
    return-void
.end method

.method d()V
    .locals 1

    .prologue
    .line 97
    iget-object v0, p0, Lco/uk/getmondo/waitlist/c;->f:Lco/uk/getmondo/common/accounts/d;

    invoke-virtual {v0}, Lco/uk/getmondo/common/accounts/d;->b()Lco/uk/getmondo/d/ak;

    move-result-object v0

    .line 98
    invoke-virtual {v0}, Lco/uk/getmondo/d/ak;->e()Lco/uk/getmondo/d/an;

    move-result-object v0

    invoke-direct {p0, v0}, Lco/uk/getmondo/waitlist/c;->a(Lco/uk/getmondo/d/an;)Z

    .line 99
    return-void
.end method
