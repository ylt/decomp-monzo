.class public final Lco/uk/getmondo/waitlist/referral/a;
.super Ljava/lang/Object;
.source "InstallReferrer.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lco/uk/getmondo/waitlist/referral/a$a;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000\"\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u0006\n\u0002\u0010\u000e\n\u0002\u0008\u0004\u0018\u0000 \u00102\u00020\u0001:\u0001\u0010B\u000f\u0008\u0002\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004R\u0011\u0010\u0005\u001a\u00020\u00068F\u00a2\u0006\u0006\u001a\u0004\u0008\u0007\u0010\u0008R\u0011\u0010\t\u001a\u00020\u00068F\u00a2\u0006\u0006\u001a\u0004\u0008\n\u0010\u0008R\u0011\u0010\u000b\u001a\u00020\u00068F\u00a2\u0006\u0006\u001a\u0004\u0008\u000b\u0010\u0008R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0013\u0010\u000c\u001a\u0004\u0018\u00010\r8F\u00a2\u0006\u0006\u001a\u0004\u0008\u000e\u0010\u000f\u00a8\u0006\u0011"
    }
    d2 = {
        "Lco/uk/getmondo/waitlist/referral/InstallReferrer;",
        "",
        "referrerUrl",
        "Lokhttp3/HttpUrl;",
        "(Lokhttp3/HttpUrl;)V",
        "hasUtmContent",
        "",
        "getHasUtmContent",
        "()Z",
        "hasUtmSource",
        "getHasUtmSource",
        "isPlayStoreRedirect",
        "utmContent",
        "",
        "getUtmContent",
        "()Ljava/lang/String;",
        "Companion",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# static fields
.field public static final a:Lco/uk/getmondo/waitlist/referral/a$a;


# instance fields
.field private final b:Lokhttp3/HttpUrl;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lco/uk/getmondo/waitlist/referral/a$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lco/uk/getmondo/waitlist/referral/a$a;-><init>(Lkotlin/d/b/i;)V

    sput-object v0, Lco/uk/getmondo/waitlist/referral/a;->a:Lco/uk/getmondo/waitlist/referral/a$a;

    return-void
.end method

.method private constructor <init>(Lokhttp3/HttpUrl;)V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lco/uk/getmondo/waitlist/referral/a;->b:Lokhttp3/HttpUrl;

    return-void
.end method

.method public synthetic constructor <init>(Lokhttp3/HttpUrl;Lkotlin/d/b/i;)V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0, p1}, Lco/uk/getmondo/waitlist/referral/a;-><init>(Lokhttp3/HttpUrl;)V

    return-void
.end method

.method public static final a(Ljava/lang/String;)Lco/uk/getmondo/waitlist/referral/a;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;,
            Ljava/io/UnsupportedEncodingException;
        }
    .end annotation

    sget-object v0, Lco/uk/getmondo/waitlist/referral/a;->a:Lco/uk/getmondo/waitlist/referral/a$a;

    invoke-virtual {v0, p0}, Lco/uk/getmondo/waitlist/referral/a$a;->a(Ljava/lang/String;)Lco/uk/getmondo/waitlist/referral/a;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a()Z
    .locals 2

    .prologue
    .line 10
    iget-object v0, p0, Lco/uk/getmondo/waitlist/referral/a;->b:Lokhttp3/HttpUrl;

    invoke-virtual {v0}, Lokhttp3/HttpUrl;->queryParameterNames()Ljava/util/Set;

    move-result-object v0

    const-string v1, "utm_source"

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final b()Z
    .locals 3

    .prologue
    .line 13
    invoke-virtual {p0}, Lco/uk/getmondo/waitlist/referral/a;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "play_store_redirect"

    iget-object v1, p0, Lco/uk/getmondo/waitlist/referral/a;->b:Lokhttp3/HttpUrl;

    const-string v2, "utm_source"

    invoke-virtual {v1, v2}, Lokhttp3/HttpUrl;->queryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c()Z
    .locals 2

    .prologue
    .line 16
    iget-object v0, p0, Lco/uk/getmondo/waitlist/referral/a;->b:Lokhttp3/HttpUrl;

    invoke-virtual {v0}, Lokhttp3/HttpUrl;->queryParameterNames()Ljava/util/Set;

    move-result-object v0

    const-string v1, "utm_content"

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final d()Ljava/lang/String;
    .locals 2

    .prologue
    .line 19
    iget-object v0, p0, Lco/uk/getmondo/waitlist/referral/a;->b:Lokhttp3/HttpUrl;

    const-string v1, "utm_content"

    invoke-virtual {v0, v1}, Lokhttp3/HttpUrl;->queryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
