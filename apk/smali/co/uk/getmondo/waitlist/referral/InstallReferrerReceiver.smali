.class public Lco/uk/getmondo/waitlist/referral/InstallReferrerReceiver;
.super Landroid/content/BroadcastReceiver;
.source "InstallReferrerReceiver.java"


# instance fields
.field a:Lco/uk/getmondo/common/a;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 3

    .prologue
    .line 23
    invoke-static {p1}, Lco/uk/getmondo/MonzoApplication;->a(Landroid/content/Context;)Lco/uk/getmondo/MonzoApplication;

    move-result-object v0

    invoke-virtual {v0}, Lco/uk/getmondo/MonzoApplication;->b()Lco/uk/getmondo/common/h/a/a;

    move-result-object v0

    invoke-interface {v0, p0}, Lco/uk/getmondo/common/h/a/a;->a(Lco/uk/getmondo/waitlist/referral/InstallReferrerReceiver;)V

    .line 25
    const-string v0, "referrer"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 26
    const-string v0, "referrer"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 29
    :try_start_0
    invoke-static {v0}, Lco/uk/getmondo/waitlist/referral/a;->a(Ljava/lang/String;)Lco/uk/getmondo/waitlist/referral/a;

    move-result-object v1

    .line 31
    invoke-virtual {v1}, Lco/uk/getmondo/waitlist/referral/a;->b()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {v1}, Lco/uk/getmondo/waitlist/referral/a;->c()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 32
    iget-object v0, p0, Lco/uk/getmondo/waitlist/referral/InstallReferrerReceiver;->a:Lco/uk/getmondo/common/a;

    invoke-virtual {v1}, Lco/uk/getmondo/waitlist/referral/a;->d()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lco/uk/getmondo/api/model/tracking/Impression;->g(Ljava/lang/String;)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v1

    invoke-virtual {v0, v1}, Lco/uk/getmondo/common/a;->a(Lco/uk/getmondo/api/model/tracking/Impression;)V

    .line 40
    :cond_0
    :goto_0
    return-void

    .line 34
    :cond_1
    iget-object v1, p0, Lco/uk/getmondo/waitlist/referral/InstallReferrerReceiver;->a:Lco/uk/getmondo/common/a;

    invoke-static {v0}, Lco/uk/getmondo/api/model/tracking/Impression;->f(Ljava/lang/String;)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    invoke-virtual {v1, v0}, Lco/uk/getmondo/common/a;->a(Lco/uk/getmondo/api/model/tracking/Impression;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 36
    :catch_0
    move-exception v0

    goto :goto_0

    :catch_1
    move-exception v0

    goto :goto_0
.end method
