.class public Lco/uk/getmondo/waitlist/ShareActivity;
.super Lco/uk/getmondo/common/activities/h;
.source "ShareActivity.java"


# instance fields
.field a:Lco/uk/getmondo/common/a;

.field b:Lco/uk/getmondo/common/accounts/d;

.field description:Landroid/widget/TextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f110219
    .end annotation
.end field

.field link:Landroid/widget/TextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f110218
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, Lco/uk/getmondo/common/activities/h;-><init>()V

    return-void
.end method

.method public static a(Landroid/app/Activity;)V
    .locals 2

    .prologue
    .line 31
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lco/uk/getmondo/waitlist/ShareActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 32
    invoke-virtual {p0, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    .line 33
    return-void
.end method

.method private a(Lco/uk/getmondo/d/ak;)V
    .locals 7

    .prologue
    .line 53
    iget-object v0, p0, Lco/uk/getmondo/waitlist/ShareActivity;->link:Landroid/widget/TextView;

    invoke-virtual {p1}, Lco/uk/getmondo/d/ak;->e()Lco/uk/getmondo/d/an;

    move-result-object v1

    invoke-virtual {v1}, Lco/uk/getmondo/d/an;->g()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 54
    invoke-virtual {p1}, Lco/uk/getmondo/d/ak;->e()Lco/uk/getmondo/d/an;

    move-result-object v0

    invoke-virtual {v0}, Lco/uk/getmondo/d/an;->h()I

    move-result v0

    .line 55
    iget-object v1, p0, Lco/uk/getmondo/waitlist/ShareActivity;->description:Landroid/widget/TextView;

    const v2, 0x7f0a0186

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    invoke-virtual {p0}, Lco/uk/getmondo/waitlist/ShareActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f120001

    invoke-virtual {v5, v6, v0}, Landroid/content/res/Resources;->getQuantityString(II)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v3, v4

    invoke-virtual {p0, v2, v3}, Lco/uk/getmondo/waitlist/ShareActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 56
    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 44
    invoke-super {p0, p1}, Lco/uk/getmondo/common/activities/h;->onCreate(Landroid/os/Bundle;)V

    .line 45
    const v0, 0x7f05005e

    invoke-virtual {p0, v0}, Lco/uk/getmondo/waitlist/ShareActivity;->setContentView(I)V

    .line 46
    invoke-static {p0}, Lbutterknife/ButterKnife;->bind(Landroid/app/Activity;)Lbutterknife/Unbinder;

    .line 47
    invoke-virtual {p0}, Lco/uk/getmondo/waitlist/ShareActivity;->l()Lco/uk/getmondo/common/h/b/b;

    move-result-object v0

    invoke-interface {v0, p0}, Lco/uk/getmondo/common/h/b/b;->a(Lco/uk/getmondo/waitlist/ShareActivity;)V

    .line 48
    iget-object v0, p0, Lco/uk/getmondo/waitlist/ShareActivity;->a:Lco/uk/getmondo/common/a;

    invoke-static {}, Lco/uk/getmondo/api/model/tracking/Impression;->g()Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v1

    invoke-virtual {v0, v1}, Lco/uk/getmondo/common/a;->a(Lco/uk/getmondo/api/model/tracking/Impression;)V

    .line 49
    iget-object v0, p0, Lco/uk/getmondo/waitlist/ShareActivity;->b:Lco/uk/getmondo/common/accounts/d;

    invoke-virtual {v0}, Lco/uk/getmondo/common/accounts/d;->b()Lco/uk/getmondo/d/ak;

    move-result-object v0

    invoke-direct {p0, v0}, Lco/uk/getmondo/waitlist/ShareActivity;->a(Lco/uk/getmondo/d/ak;)V

    .line 50
    return-void
.end method

.method share()V
    .locals 3
    .annotation build Lbutterknife/OnClick;
        value = {
            0x7f11021a
        }
    .end annotation

    .prologue
    .line 37
    iget-object v0, p0, Lco/uk/getmondo/waitlist/ShareActivity;->a:Lco/uk/getmondo/common/a;

    invoke-static {}, Lco/uk/getmondo/api/model/tracking/Impression;->h()Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v1

    invoke-virtual {v0, v1}, Lco/uk/getmondo/common/a;->a(Lco/uk/getmondo/api/model/tracking/Impression;)V

    .line 38
    const v0, 0x7f0a038c

    invoke-virtual {p0, v0}, Lco/uk/getmondo/waitlist/ShareActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 39
    iget-object v1, p0, Lco/uk/getmondo/waitlist/ShareActivity;->link:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lco/uk/getmondo/api/model/tracking/a;->WAITING_LIST_SHARE:Lco/uk/getmondo/api/model/tracking/a;

    invoke-static {p0, v0, v1, v2}, Lco/uk/getmondo/common/k/j;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lco/uk/getmondo/api/model/tracking/a;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lco/uk/getmondo/waitlist/ShareActivity;->startActivity(Landroid/content/Intent;)V

    .line 40
    return-void
.end method
