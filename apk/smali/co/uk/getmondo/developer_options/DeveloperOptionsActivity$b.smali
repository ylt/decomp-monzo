.class public final Lco/uk/getmondo/developer_options/DeveloperOptionsActivity$b;
.super Landroid/preference/PreferenceFragment;
.source "DeveloperOptionsActivity.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lco/uk/getmondo/developer_options/DeveloperOptionsActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "b"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0008\u0010\u0003\u001a\u00020\u0004H\u0002J\u0012\u0010\u0005\u001a\u00020\u00042\u0008\u0010\u0006\u001a\u0004\u0018\u00010\u0007H\u0016\u00a8\u0006\u0008"
    }
    d2 = {
        "Lco/uk/getmondo/developer_options/DeveloperOptionsActivity$DeveloperOptionsPreferenceFragment;",
        "Landroid/preference/PreferenceFragment;",
        "()V",
        "initDeviceInfo",
        "",
        "onCreate",
        "savedInstanceState",
        "Landroid/os/Bundle;",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field private a:Ljava/util/HashMap;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Landroid/preference/PreferenceFragment;-><init>()V

    return-void
.end method

.method private final b()V
    .locals 2

    .prologue
    .line 40
    const v0, 0x7f0a0158

    invoke-virtual {p0, v0}, Lco/uk/getmondo/developer_options/DeveloperOptionsActivity$b;->getString(I)Ljava/lang/String;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-virtual {p0, v0}, Lco/uk/getmondo/developer_options/DeveloperOptionsActivity$b;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    .line 41
    invoke-virtual {p0}, Lco/uk/getmondo/developer_options/DeveloperOptionsActivity$b;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->densityDpi:I

    sparse-switch v0, :sswitch_data_0

    .line 49
    invoke-virtual {p0}, Lco/uk/getmondo/developer_options/DeveloperOptionsActivity$b;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->densityDpi:I

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    .line 41
    :goto_0
    invoke-virtual {v1, v0}, Landroid/preference/Preference;->setTitle(Ljava/lang/CharSequence;)V

    .line 52
    const v0, 0x7f0a0159

    invoke-virtual {p0, v0}, Lco/uk/getmondo/developer_options/DeveloperOptionsActivity$b;->getString(I)Ljava/lang/String;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-virtual {p0, v0}, Lco/uk/getmondo/developer_options/DeveloperOptionsActivity$b;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    .line 53
    const-string v0, "http.agent"

    invoke-static {v0}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lco/uk/getmondo/common/k/p;->g(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-virtual {v1, v0}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 54
    new-instance v0, Lco/uk/getmondo/developer_options/DeveloperOptionsActivity$b$a;

    invoke-direct {v0, p0, v1}, Lco/uk/getmondo/developer_options/DeveloperOptionsActivity$b$a;-><init>(Lco/uk/getmondo/developer_options/DeveloperOptionsActivity$b;Landroid/preference/Preference;)V

    check-cast v0, Landroid/preference/Preference$OnPreferenceClickListener;

    invoke-virtual {v1, v0}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 61
    return-void

    .line 42
    :sswitch_0
    const-string v0, "ldpi"

    check-cast v0, Ljava/lang/CharSequence;

    goto :goto_0

    .line 43
    :sswitch_1
    const-string v0, "mdpi"

    check-cast v0, Ljava/lang/CharSequence;

    goto :goto_0

    .line 44
    :sswitch_2
    const-string v0, "hdpi"

    check-cast v0, Ljava/lang/CharSequence;

    goto :goto_0

    .line 45
    :sswitch_3
    const-string v0, "xhdpi"

    check-cast v0, Ljava/lang/CharSequence;

    goto :goto_0

    .line 46
    :sswitch_4
    const-string v0, "xxhdpi"

    check-cast v0, Ljava/lang/CharSequence;

    goto :goto_0

    .line 47
    :sswitch_5
    const-string v0, "xxxhdpi"

    check-cast v0, Ljava/lang/CharSequence;

    goto :goto_0

    .line 48
    :sswitch_6
    const-string v0, "tvdpi"

    check-cast v0, Ljava/lang/CharSequence;

    goto :goto_0

    .line 41
    nop

    :sswitch_data_0
    .sparse-switch
        0x78 -> :sswitch_0
        0xa0 -> :sswitch_1
        0xd5 -> :sswitch_6
        0xf0 -> :sswitch_2
        0x140 -> :sswitch_3
        0x1a4 -> :sswitch_4
        0x1e0 -> :sswitch_4
        0x280 -> :sswitch_5
    .end sparse-switch
.end method


# virtual methods
.method public a()V
    .locals 1

    iget-object v0, p0, Lco/uk/getmondo/developer_options/DeveloperOptionsActivity$b;->a:Ljava/util/HashMap;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lco/uk/getmondo/developer_options/DeveloperOptionsActivity$b;->a:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    :cond_0
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 28
    invoke-super {p0, p1}, Landroid/preference/PreferenceFragment;->onCreate(Landroid/os/Bundle;)V

    .line 30
    invoke-virtual {p0}, Lco/uk/getmondo/developer_options/DeveloperOptionsActivity$b;->getPreferenceManager()Landroid/preference/PreferenceManager;

    move-result-object v0

    const-string v1, "developer_options"

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceManager;->setSharedPreferencesName(Ljava/lang/String;)V

    .line 31
    const v0, 0x7f080001

    invoke-virtual {p0, v0}, Lco/uk/getmondo/developer_options/DeveloperOptionsActivity$b;->addPreferencesFromResource(I)V

    .line 33
    const v0, 0x7f0a0157

    invoke-virtual {p0, v0}, Lco/uk/getmondo/developer_options/DeveloperOptionsActivity$b;->getString(I)Ljava/lang/String;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-virtual {p0, v0}, Lco/uk/getmondo/developer_options/DeveloperOptionsActivity$b;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/preference/Preference;->setEnabled(Z)V

    .line 34
    const v0, 0x7f0a0156

    invoke-virtual {p0, v0}, Lco/uk/getmondo/developer_options/DeveloperOptionsActivity$b;->getString(I)Ljava/lang/String;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-virtual {p0, v0}, Lco/uk/getmondo/developer_options/DeveloperOptionsActivity$b;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/preference/Preference;->setEnabled(Z)V

    .line 36
    invoke-direct {p0}, Lco/uk/getmondo/developer_options/DeveloperOptionsActivity$b;->b()V

    .line 37
    return-void
.end method

.method public synthetic onDestroyView()V
    .locals 0

    invoke-super {p0}, Landroid/preference/PreferenceFragment;->onDestroyView()V

    invoke-virtual {p0}, Lco/uk/getmondo/developer_options/DeveloperOptionsActivity$b;->a()V

    return-void
.end method
