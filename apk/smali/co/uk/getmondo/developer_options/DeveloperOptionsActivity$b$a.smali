.class final Lco/uk/getmondo/developer_options/DeveloperOptionsActivity$b$a;
.super Ljava/lang/Object;
.source "DeveloperOptionsActivity.kt"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lco/uk/getmondo/developer_options/DeveloperOptionsActivity$b;->b()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "",
        "it",
        "Landroid/preference/Preference;",
        "kotlin.jvm.PlatformType",
        "onPreferenceClick"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field final synthetic a:Lco/uk/getmondo/developer_options/DeveloperOptionsActivity$b;

.field final synthetic b:Landroid/preference/Preference;


# direct methods
.method constructor <init>(Lco/uk/getmondo/developer_options/DeveloperOptionsActivity$b;Landroid/preference/Preference;)V
    .locals 0

    iput-object p1, p0, Lco/uk/getmondo/developer_options/DeveloperOptionsActivity$b$a;->a:Lco/uk/getmondo/developer_options/DeveloperOptionsActivity$b;

    iput-object p2, p0, Lco/uk/getmondo/developer_options/DeveloperOptionsActivity$b$a;->b:Landroid/preference/Preference;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onPreferenceClick(Landroid/preference/Preference;)Z
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 55
    iget-object v0, p0, Lco/uk/getmondo/developer_options/DeveloperOptionsActivity$b$a;->a:Lco/uk/getmondo/developer_options/DeveloperOptionsActivity$b;

    invoke-virtual {v0}, Lco/uk/getmondo/developer_options/DeveloperOptionsActivity$b;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const-string v1, "clipboard"

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type android.content.ClipboardManager"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    check-cast v0, Landroid/content/ClipboardManager;

    .line 56
    const/4 v1, 0x0

    iget-object v2, p0, Lco/uk/getmondo/developer_options/DeveloperOptionsActivity$b$a;->b:Landroid/preference/Preference;

    invoke-virtual {v2}, Landroid/preference/Preference;->getSummary()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/content/ClipData;->newPlainText(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Landroid/content/ClipData;

    move-result-object v1

    .line 57
    invoke-virtual {v0, v1}, Landroid/content/ClipboardManager;->setPrimaryClip(Landroid/content/ClipData;)V

    .line 58
    iget-object v0, p0, Lco/uk/getmondo/developer_options/DeveloperOptionsActivity$b$a;->a:Lco/uk/getmondo/developer_options/DeveloperOptionsActivity$b;

    invoke-virtual {v0}, Lco/uk/getmondo/developer_options/DeveloperOptionsActivity$b;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    const v1, 0x7f0a03cc

    invoke-static {v0, v1, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 59
    return v3
.end method
