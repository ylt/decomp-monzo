.class public final Lco/uk/getmondo/developer_options/DeveloperOptionsActivity;
.super Lco/uk/getmondo/common/activities/b;
.source "DeveloperOptionsActivity.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lco/uk/getmondo/developer_options/DeveloperOptionsActivity$b;,
        Lco/uk/getmondo/developer_options/DeveloperOptionsActivity$a;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0018\u0000 \u00072\u00020\u0001:\u0002\u0007\u0008B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0012\u0010\u0003\u001a\u00020\u00042\u0008\u0010\u0005\u001a\u0004\u0018\u00010\u0006H\u0014\u00a8\u0006\t"
    }
    d2 = {
        "Lco/uk/getmondo/developer_options/DeveloperOptionsActivity;",
        "Lco/uk/getmondo/common/activities/BaseActivity;",
        "()V",
        "onCreate",
        "",
        "savedInstanceState",
        "Landroid/os/Bundle;",
        "Companion",
        "DeveloperOptionsPreferenceFragment",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# static fields
.field public static final a:Lco/uk/getmondo/developer_options/DeveloperOptionsActivity$a;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lco/uk/getmondo/developer_options/DeveloperOptionsActivity$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lco/uk/getmondo/developer_options/DeveloperOptionsActivity$a;-><init>(Lkotlin/d/b/i;)V

    sput-object v0, Lco/uk/getmondo/developer_options/DeveloperOptionsActivity;->a:Lco/uk/getmondo/developer_options/DeveloperOptionsActivity$a;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Lco/uk/getmondo/common/activities/b;-><init>()V

    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 19
    invoke-super {p0, p1}, Lco/uk/getmondo/common/activities/b;->onCreate(Landroid/os/Bundle;)V

    .line 21
    invoke-virtual {p0}, Lco/uk/getmondo/developer_options/DeveloperOptionsActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v1

    .line 22
    const v2, 0x1020002

    new-instance v0, Lco/uk/getmondo/developer_options/DeveloperOptionsActivity$b;

    invoke-direct {v0}, Lco/uk/getmondo/developer_options/DeveloperOptionsActivity$b;-><init>()V

    check-cast v0, Landroid/app/Fragment;

    invoke-virtual {v1, v2, v0}, Landroid/app/FragmentTransaction;->replace(ILandroid/app/Fragment;)Landroid/app/FragmentTransaction;

    move-result-object v0

    .line 23
    invoke-virtual {v0}, Landroid/app/FragmentTransaction;->commit()I

    .line 24
    return-void
.end method
