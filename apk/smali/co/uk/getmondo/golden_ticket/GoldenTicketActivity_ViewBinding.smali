.class public Lco/uk/getmondo/golden_ticket/GoldenTicketActivity_ViewBinding;
.super Ljava/lang/Object;
.source "GoldenTicketActivity_ViewBinding.java"

# interfaces
.implements Lbutterknife/Unbinder;


# instance fields
.field private a:Lco/uk/getmondo/golden_ticket/GoldenTicketActivity;


# direct methods
.method public constructor <init>(Lco/uk/getmondo/golden_ticket/GoldenTicketActivity;Landroid/view/View;)V
    .locals 3

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    iput-object p1, p0, Lco/uk/getmondo/golden_ticket/GoldenTicketActivity_ViewBinding;->a:Lco/uk/getmondo/golden_ticket/GoldenTicketActivity;

    .line 30
    const v0, 0x7f110170

    const-string v1, "field \'progressBar\'"

    const-class v2, Landroid/widget/ProgressBar;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p1, Lco/uk/getmondo/golden_ticket/GoldenTicketActivity;->progressBar:Landroid/widget/ProgressBar;

    .line 31
    const v0, 0x7f110172

    const-string v1, "field \'titleTextView\'"

    const-class v2, Landroid/widget/TextView;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p1, Lco/uk/getmondo/golden_ticket/GoldenTicketActivity;->titleTextView:Landroid/widget/TextView;

    .line 32
    const v0, 0x7f110175

    const-string v1, "field \'bodyTextView\'"

    const-class v2, Landroid/widget/TextView;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p1, Lco/uk/getmondo/golden_ticket/GoldenTicketActivity;->bodyTextView:Landroid/widget/TextView;

    .line 33
    const v0, 0x7f110174

    const-string v1, "field \'imageView\'"

    const-class v2, Landroid/widget/ImageView;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p1, Lco/uk/getmondo/golden_ticket/GoldenTicketActivity;->imageView:Landroid/widget/ImageView;

    .line 34
    const v0, 0x7f110173

    const-string v1, "field \'videoImageView\'"

    const-class v2, Lco/uk/getmondo/common/ui/VideoImageView;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/common/ui/VideoImageView;

    iput-object v0, p1, Lco/uk/getmondo/golden_ticket/GoldenTicketActivity;->videoImageView:Lco/uk/getmondo/common/ui/VideoImageView;

    .line 35
    const v0, 0x7f110176

    const-string v1, "field \'shareLinkButton\'"

    const-class v2, Landroid/widget/Button;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p1, Lco/uk/getmondo/golden_ticket/GoldenTicketActivity;->shareLinkButton:Landroid/widget/Button;

    .line 36
    return-void
.end method


# virtual methods
.method public unbind()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 41
    iget-object v0, p0, Lco/uk/getmondo/golden_ticket/GoldenTicketActivity_ViewBinding;->a:Lco/uk/getmondo/golden_ticket/GoldenTicketActivity;

    .line 42
    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Bindings already cleared."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 43
    :cond_0
    iput-object v1, p0, Lco/uk/getmondo/golden_ticket/GoldenTicketActivity_ViewBinding;->a:Lco/uk/getmondo/golden_ticket/GoldenTicketActivity;

    .line 45
    iput-object v1, v0, Lco/uk/getmondo/golden_ticket/GoldenTicketActivity;->progressBar:Landroid/widget/ProgressBar;

    .line 46
    iput-object v1, v0, Lco/uk/getmondo/golden_ticket/GoldenTicketActivity;->titleTextView:Landroid/widget/TextView;

    .line 47
    iput-object v1, v0, Lco/uk/getmondo/golden_ticket/GoldenTicketActivity;->bodyTextView:Landroid/widget/TextView;

    .line 48
    iput-object v1, v0, Lco/uk/getmondo/golden_ticket/GoldenTicketActivity;->imageView:Landroid/widget/ImageView;

    .line 49
    iput-object v1, v0, Lco/uk/getmondo/golden_ticket/GoldenTicketActivity;->videoImageView:Lco/uk/getmondo/common/ui/VideoImageView;

    .line 50
    iput-object v1, v0, Lco/uk/getmondo/golden_ticket/GoldenTicketActivity;->shareLinkButton:Landroid/widget/Button;

    .line 51
    return-void
.end method
