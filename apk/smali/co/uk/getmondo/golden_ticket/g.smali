.class public Lco/uk/getmondo/golden_ticket/g;
.super Lco/uk/getmondo/common/ui/b;
.source "GoldenTicketPresenter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lco/uk/getmondo/golden_ticket/g$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lco/uk/getmondo/common/ui/b",
        "<",
        "Lco/uk/getmondo/golden_ticket/g$a;",
        ">;"
    }
.end annotation


# instance fields
.field private final c:Lio/reactivex/u;

.field private final d:Lio/reactivex/u;

.field private final e:Ljava/lang/String;

.field private final f:Z

.field private final g:Lco/uk/getmondo/api/MonzoApi;

.field private final h:Lco/uk/getmondo/common/e/a;

.field private final i:Lco/uk/getmondo/common/a;

.field private j:Ljava/lang/String;


# direct methods
.method constructor <init>(Lio/reactivex/u;Lio/reactivex/u;Ljava/lang/String;ZLco/uk/getmondo/api/MonzoApi;Lco/uk/getmondo/common/e/a;Lco/uk/getmondo/common/a;)V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0}, Lco/uk/getmondo/common/ui/b;-><init>()V

    .line 35
    iput-object p1, p0, Lco/uk/getmondo/golden_ticket/g;->c:Lio/reactivex/u;

    .line 36
    iput-object p2, p0, Lco/uk/getmondo/golden_ticket/g;->d:Lio/reactivex/u;

    .line 37
    iput-object p3, p0, Lco/uk/getmondo/golden_ticket/g;->e:Ljava/lang/String;

    .line 38
    iput-boolean p4, p0, Lco/uk/getmondo/golden_ticket/g;->f:Z

    .line 39
    iput-object p5, p0, Lco/uk/getmondo/golden_ticket/g;->g:Lco/uk/getmondo/api/MonzoApi;

    .line 40
    iput-object p6, p0, Lco/uk/getmondo/golden_ticket/g;->h:Lco/uk/getmondo/common/e/a;

    .line 41
    iput-object p7, p0, Lco/uk/getmondo/golden_ticket/g;->i:Lco/uk/getmondo/common/a;

    .line 42
    return-void
.end method

.method static synthetic a(Lco/uk/getmondo/golden_ticket/g$a;Lio/reactivex/b/b;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 53
    invoke-interface {p0}, Lco/uk/getmondo/golden_ticket/g$a;->a()V

    return-void
.end method

.method static synthetic a(Lco/uk/getmondo/golden_ticket/g;Lco/uk/getmondo/common/b/a;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 69
    iget-object v0, p0, Lco/uk/getmondo/golden_ticket/g;->i:Lco/uk/getmondo/common/a;

    iget-boolean v1, p0, Lco/uk/getmondo/golden_ticket/g;->f:Z

    invoke-static {v1}, Lco/uk/getmondo/api/model/tracking/Impression;->j(Z)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v1

    invoke-virtual {v0, v1}, Lco/uk/getmondo/common/a;->a(Lco/uk/getmondo/api/model/tracking/Impression;)V

    return-void
.end method

.method static synthetic a(Lco/uk/getmondo/golden_ticket/g;Lco/uk/getmondo/golden_ticket/g$a;Lco/uk/getmondo/api/model/ApiGoldenTicket;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 56
    invoke-virtual {p2}, Lco/uk/getmondo/api/model/ApiGoldenTicket;->b()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/golden_ticket/g;->j:Ljava/lang/String;

    .line 57
    invoke-virtual {p2}, Lco/uk/getmondo/api/model/ApiGoldenTicket;->a()Lco/uk/getmondo/api/model/ApiGoldenTicket$GoldenTicketStatus;

    move-result-object v0

    sget-object v1, Lco/uk/getmondo/api/model/ApiGoldenTicket$GoldenTicketStatus;->ACTIVE:Lco/uk/getmondo/api/model/ApiGoldenTicket$GoldenTicketStatus;

    if-ne v0, v1, :cond_1

    .line 58
    iget-boolean v0, p0, Lco/uk/getmondo/golden_ticket/g;->f:Z

    if-eqz v0, :cond_0

    .line 59
    invoke-interface {p1}, Lco/uk/getmondo/golden_ticket/g$a;->c()V

    .line 66
    :goto_0
    return-void

    .line 61
    :cond_0
    invoke-interface {p1}, Lco/uk/getmondo/golden_ticket/g$a;->d()V

    goto :goto_0

    .line 64
    :cond_1
    invoke-interface {p1}, Lco/uk/getmondo/golden_ticket/g$a;->e()V

    goto :goto_0
.end method

.method static synthetic a(Lco/uk/getmondo/golden_ticket/g;Lco/uk/getmondo/golden_ticket/g$a;Lco/uk/getmondo/common/b/a;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 70
    iget-object v0, p0, Lco/uk/getmondo/golden_ticket/g;->j:Ljava/lang/String;

    invoke-interface {p1, v0}, Lco/uk/getmondo/golden_ticket/g$a;->a(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic a(Lco/uk/getmondo/golden_ticket/g;Lco/uk/getmondo/golden_ticket/g$a;Ljava/lang/Throwable;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 66
    iget-object v0, p0, Lco/uk/getmondo/golden_ticket/g;->h:Lco/uk/getmondo/common/e/a;

    invoke-virtual {v0, p2, p1}, Lco/uk/getmondo/common/e/a;->a(Ljava/lang/Throwable;Lco/uk/getmondo/common/e/a$a;)Z

    return-void
.end method


# virtual methods
.method public bridge synthetic a(Lco/uk/getmondo/common/ui/f;)V
    .locals 0

    .prologue
    .line 19
    check-cast p1, Lco/uk/getmondo/golden_ticket/g$a;

    invoke-virtual {p0, p1}, Lco/uk/getmondo/golden_ticket/g;->a(Lco/uk/getmondo/golden_ticket/g$a;)V

    return-void
.end method

.method public a(Lco/uk/getmondo/golden_ticket/g$a;)V
    .locals 3

    .prologue
    .line 46
    invoke-super {p0, p1}, Lco/uk/getmondo/common/ui/b;->a(Lco/uk/getmondo/common/ui/f;)V

    .line 48
    iget-object v0, p0, Lco/uk/getmondo/golden_ticket/g;->i:Lco/uk/getmondo/common/a;

    iget-boolean v1, p0, Lco/uk/getmondo/golden_ticket/g;->f:Z

    invoke-static {v1}, Lco/uk/getmondo/api/model/tracking/Impression;->i(Z)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v1

    invoke-virtual {v0, v1}, Lco/uk/getmondo/common/a;->a(Lco/uk/getmondo/api/model/tracking/Impression;)V

    .line 50
    iget-object v0, p0, Lco/uk/getmondo/golden_ticket/g;->g:Lco/uk/getmondo/api/MonzoApi;

    iget-object v1, p0, Lco/uk/getmondo/golden_ticket/g;->e:Ljava/lang/String;

    invoke-interface {v0, v1}, Lco/uk/getmondo/api/MonzoApi;->goldenTicketStatus(Ljava/lang/String;)Lio/reactivex/v;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/golden_ticket/g;->c:Lio/reactivex/u;

    .line 51
    invoke-virtual {v0, v1}, Lio/reactivex/v;->b(Lio/reactivex/u;)Lio/reactivex/v;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/golden_ticket/g;->d:Lio/reactivex/u;

    .line 52
    invoke-virtual {v0, v1}, Lio/reactivex/v;->a(Lio/reactivex/u;)Lio/reactivex/v;

    move-result-object v0

    invoke-static {p1}, Lco/uk/getmondo/golden_ticket/h;->a(Lco/uk/getmondo/golden_ticket/g$a;)Lio/reactivex/c/g;

    move-result-object v1

    .line 53
    invoke-virtual {v0, v1}, Lio/reactivex/v;->b(Lio/reactivex/c/g;)Lio/reactivex/v;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    invoke-static {p1}, Lco/uk/getmondo/golden_ticket/i;->a(Lco/uk/getmondo/golden_ticket/g$a;)Lio/reactivex/c/a;

    move-result-object v1

    .line 54
    invoke-virtual {v0, v1}, Lio/reactivex/v;->a(Lio/reactivex/c/a;)Lio/reactivex/v;

    move-result-object v0

    invoke-static {p0, p1}, Lco/uk/getmondo/golden_ticket/j;->a(Lco/uk/getmondo/golden_ticket/g;Lco/uk/getmondo/golden_ticket/g$a;)Lio/reactivex/c/g;

    move-result-object v1

    invoke-static {p0, p1}, Lco/uk/getmondo/golden_ticket/k;->a(Lco/uk/getmondo/golden_ticket/g;Lco/uk/getmondo/golden_ticket/g$a;)Lio/reactivex/c/g;

    move-result-object v2

    .line 55
    invoke-virtual {v0, v1, v2}, Lio/reactivex/v;->a(Lio/reactivex/c/g;Lio/reactivex/c/g;)Lio/reactivex/b/b;

    move-result-object v0

    .line 50
    invoke-virtual {p0, v0}, Lco/uk/getmondo/golden_ticket/g;->a(Lio/reactivex/b/b;)V

    .line 68
    invoke-interface {p1}, Lco/uk/getmondo/golden_ticket/g$a;->f()Lio/reactivex/n;

    move-result-object v0

    invoke-static {p0}, Lco/uk/getmondo/golden_ticket/l;->a(Lco/uk/getmondo/golden_ticket/g;)Lio/reactivex/c/g;

    move-result-object v1

    .line 69
    invoke-virtual {v0, v1}, Lio/reactivex/n;->doOnNext(Lio/reactivex/c/g;)Lio/reactivex/n;

    move-result-object v0

    invoke-static {p0, p1}, Lco/uk/getmondo/golden_ticket/m;->a(Lco/uk/getmondo/golden_ticket/g;Lco/uk/getmondo/golden_ticket/g$a;)Lio/reactivex/c/g;

    move-result-object v1

    .line 70
    invoke-virtual {v0, v1}, Lio/reactivex/n;->subscribe(Lio/reactivex/c/g;)Lio/reactivex/b/b;

    move-result-object v0

    .line 68
    invoke-virtual {p0, v0}, Lco/uk/getmondo/golden_ticket/g;->a(Lio/reactivex/b/b;)V

    .line 71
    return-void
.end method
