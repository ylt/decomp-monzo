.class public Lco/uk/getmondo/golden_ticket/GoldenTicketActivity;
.super Lco/uk/getmondo/common/activities/b;
.source "GoldenTicketActivity.java"

# interfaces
.implements Lco/uk/getmondo/golden_ticket/g$a;


# instance fields
.field a:Lco/uk/getmondo/golden_ticket/g;

.field bodyTextView:Landroid/widget/TextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f110175
    .end annotation
.end field

.field imageView:Landroid/widget/ImageView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f110174
    .end annotation
.end field

.field progressBar:Landroid/widget/ProgressBar;
    .annotation build Lbutterknife/BindView;
        value = 0x7f110170
    .end annotation
.end field

.field shareLinkButton:Landroid/widget/Button;
    .annotation build Lbutterknife/BindView;
        value = 0x7f110176
    .end annotation
.end field

.field titleTextView:Landroid/widget/TextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f110172
    .end annotation
.end field

.field videoImageView:Lco/uk/getmondo/common/ui/VideoImageView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f110173
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, Lco/uk/getmondo/common/activities/b;-><init>()V

    return-void
.end method

.method static synthetic a(Ljava/lang/Object;)Lco/uk/getmondo/common/b/a;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 113
    sget-object v0, Lco/uk/getmondo/common/b/a;->a:Lco/uk/getmondo/common/b/a;

    return-object v0
.end method

.method public static a(Landroid/content/Context;Lco/uk/getmondo/d/o;Z)V
    .locals 3

    .prologue
    .line 42
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lco/uk/getmondo/golden_ticket/GoldenTicketActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 43
    const-string v1, "KEY_GOLDEN_TICKET_ID"

    invoke-virtual {p1}, Lco/uk/getmondo/d/o;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 44
    const-string v1, "KEY_IS_FIRST_GOLDEN_TICKET"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 45
    invoke-virtual {p0, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 46
    return-void
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    .line 72
    iget-object v0, p0, Lco/uk/getmondo/golden_ticket/GoldenTicketActivity;->progressBar:Landroid/widget/ProgressBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 73
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 106
    const v0, 0x7f0a01a0

    invoke-virtual {p0, v0}, Lco/uk/getmondo/golden_ticket/GoldenTicketActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 107
    sget-object v1, Lco/uk/getmondo/api/model/tracking/a;->GOLDEN_TICKET:Lco/uk/getmondo/api/model/tracking/a;

    invoke-static {p0, v0, p1, v1}, Lco/uk/getmondo/common/k/j;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lco/uk/getmondo/api/model/tracking/a;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lco/uk/getmondo/golden_ticket/GoldenTicketActivity;->startActivity(Landroid/content/Intent;)V

    .line 108
    return-void
.end method

.method public b()V
    .locals 2

    .prologue
    .line 77
    iget-object v0, p0, Lco/uk/getmondo/golden_ticket/GoldenTicketActivity;->progressBar:Landroid/widget/ProgressBar;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 78
    return-void
.end method

.method public c()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 82
    iget-object v0, p0, Lco/uk/getmondo/golden_ticket/GoldenTicketActivity;->titleTextView:Landroid/widget/TextView;

    const v1, 0x7f0a019e

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 83
    iget-object v0, p0, Lco/uk/getmondo/golden_ticket/GoldenTicketActivity;->bodyTextView:Landroid/widget/TextView;

    const v1, 0x7f0a019d

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 84
    iget-object v0, p0, Lco/uk/getmondo/golden_ticket/GoldenTicketActivity;->shareLinkButton:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setVisibility(I)V

    .line 85
    iget-object v0, p0, Lco/uk/getmondo/golden_ticket/GoldenTicketActivity;->videoImageView:Lco/uk/getmondo/common/ui/VideoImageView;

    invoke-virtual {v0, v2}, Lco/uk/getmondo/common/ui/VideoImageView;->setVisibility(I)V

    .line 86
    return-void
.end method

.method public d()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 90
    iget-object v0, p0, Lco/uk/getmondo/golden_ticket/GoldenTicketActivity;->titleTextView:Landroid/widget/TextView;

    const v1, 0x7f0a01a2

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 91
    iget-object v0, p0, Lco/uk/getmondo/golden_ticket/GoldenTicketActivity;->bodyTextView:Landroid/widget/TextView;

    const v1, 0x7f0a01a1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 92
    iget-object v0, p0, Lco/uk/getmondo/golden_ticket/GoldenTicketActivity;->shareLinkButton:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setVisibility(I)V

    .line 93
    iget-object v0, p0, Lco/uk/getmondo/golden_ticket/GoldenTicketActivity;->videoImageView:Lco/uk/getmondo/common/ui/VideoImageView;

    invoke-virtual {v0, v2}, Lco/uk/getmondo/common/ui/VideoImageView;->setVisibility(I)V

    .line 94
    return-void
.end method

.method public e()V
    .locals 2

    .prologue
    .line 98
    iget-object v0, p0, Lco/uk/getmondo/golden_ticket/GoldenTicketActivity;->titleTextView:Landroid/widget/TextView;

    const v1, 0x7f0a01a4

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 99
    iget-object v0, p0, Lco/uk/getmondo/golden_ticket/GoldenTicketActivity;->bodyTextView:Landroid/widget/TextView;

    const v1, 0x7f0a01a3

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 100
    iget-object v0, p0, Lco/uk/getmondo/golden_ticket/GoldenTicketActivity;->shareLinkButton:Landroid/widget/Button;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 101
    iget-object v0, p0, Lco/uk/getmondo/golden_ticket/GoldenTicketActivity;->imageView:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 102
    return-void
.end method

.method public f()Lio/reactivex/n;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Lco/uk/getmondo/common/b/a;",
            ">;"
        }
    .end annotation

    .prologue
    .line 112
    iget-object v0, p0, Lco/uk/getmondo/golden_ticket/GoldenTicketActivity;->shareLinkButton:Landroid/widget/Button;

    invoke-static {v0}, Lcom/b/a/c/c;->a(Landroid/view/View;)Lio/reactivex/n;

    move-result-object v0

    invoke-static {}, Lco/uk/getmondo/golden_ticket/a;->a()Lio/reactivex/c/h;

    move-result-object v1

    .line 113
    invoke-virtual {v0, v1}, Lio/reactivex/n;->map(Lio/reactivex/c/h;)Lio/reactivex/n;

    move-result-object v0

    .line 112
    return-object v0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 6

    .prologue
    .line 50
    invoke-super {p0, p1}, Lco/uk/getmondo/common/activities/b;->onCreate(Landroid/os/Bundle;)V

    .line 52
    const v0, 0x7f05003c

    invoke-virtual {p0, v0}, Lco/uk/getmondo/golden_ticket/GoldenTicketActivity;->setContentView(I)V

    .line 53
    invoke-static {p0}, Lbutterknife/ButterKnife;->bind(Landroid/app/Activity;)Lbutterknife/Unbinder;

    .line 55
    invoke-virtual {p0}, Lco/uk/getmondo/golden_ticket/GoldenTicketActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 56
    invoke-virtual {p0}, Lco/uk/getmondo/golden_ticket/GoldenTicketActivity;->l()Lco/uk/getmondo/common/h/b/b;

    move-result-object v1

    new-instance v2, Lco/uk/getmondo/golden_ticket/d;

    const-string v3, "KEY_GOLDEN_TICKET_ID"

    invoke-virtual {v0, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "KEY_IS_FIRST_GOLDEN_TICKET"

    const/4 v5, 0x0

    .line 57
    invoke-virtual {v0, v4, v5}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    invoke-direct {v2, v3, v0}, Lco/uk/getmondo/golden_ticket/d;-><init>(Ljava/lang/String;Z)V

    .line 56
    invoke-interface {v1, v2}, Lco/uk/getmondo/common/h/b/b;->a(Lco/uk/getmondo/golden_ticket/d;)Lco/uk/getmondo/golden_ticket/c;

    move-result-object v0

    .line 57
    invoke-interface {v0, p0}, Lco/uk/getmondo/golden_ticket/c;->a(Lco/uk/getmondo/golden_ticket/GoldenTicketActivity;)V

    .line 59
    iget-object v0, p0, Lco/uk/getmondo/golden_ticket/GoldenTicketActivity;->a:Lco/uk/getmondo/golden_ticket/g;

    invoke-virtual {v0, p0}, Lco/uk/getmondo/golden_ticket/g;->a(Lco/uk/getmondo/golden_ticket/g$a;)V

    .line 60
    return-void
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lco/uk/getmondo/golden_ticket/GoldenTicketActivity;->videoImageView:Lco/uk/getmondo/common/ui/VideoImageView;

    invoke-virtual {v0}, Lco/uk/getmondo/common/ui/VideoImageView;->a()V

    .line 66
    iget-object v0, p0, Lco/uk/getmondo/golden_ticket/GoldenTicketActivity;->a:Lco/uk/getmondo/golden_ticket/g;

    invoke-virtual {v0}, Lco/uk/getmondo/golden_ticket/g;->b()V

    .line 67
    invoke-super {p0}, Lco/uk/getmondo/common/activities/b;->onDestroy()V

    .line 68
    return-void
.end method
