.class public Lco/uk/getmondo/signup/EmailActivity;
.super Lco/uk/getmondo/common/activities/b;
.source "EmailActivity.java"

# interfaces
.implements Lco/uk/getmondo/signup/f$a;


# instance fields
.field a:Lco/uk/getmondo/common/k;

.field b:Lco/uk/getmondo/common/s;

.field c:Lco/uk/getmondo/signup/f;

.field content:Landroid/widget/TextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f11006e
    .end annotation
.end field

.field private final e:Lcom/b/b/b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/b/b/b",
            "<",
            "Landroid/support/v4/g/j",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 29
    invoke-direct {p0}, Lco/uk/getmondo/common/activities/b;-><init>()V

    .line 33
    invoke-static {}, Lcom/b/b/b;->a()Lcom/b/b/b;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/signup/EmailActivity;->e:Lcom/b/b/b;

    return-void
.end method

.method static synthetic a(ILjava/lang/String;Lco/uk/getmondo/common/f/c;)V
    .locals 0

    .prologue
    .line 110
    invoke-virtual {p2, p0, p1}, Lco/uk/getmondo/common/f/c;->a(ILjava/lang/String;)V

    return-void
.end method

.method public static a(Landroid/app/Activity;)V
    .locals 2

    .prologue
    .line 42
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lco/uk/getmondo/signup/EmailActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 43
    invoke-virtual {p0, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    .line 44
    return-void
.end method

.method private a(Landroid/content/Intent;)V
    .locals 3

    .prologue
    .line 76
    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 77
    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    const-string v1, "code"

    invoke-virtual {v0, v1}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 78
    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v1

    const-string v2, "state"

    invoke-virtual {v1, v2}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 79
    invoke-static {v0}, Lco/uk/getmondo/common/k/p;->c(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-static {v1}, Lco/uk/getmondo/common/k/p;->c(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 80
    iget-object v2, p0, Lco/uk/getmondo/signup/EmailActivity;->e:Lcom/b/b/b;

    invoke-static {v0, v1}, Landroid/support/v4/g/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Landroid/support/v4/g/j;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/b/b/b;->a(Ljava/lang/Object;)V

    .line 83
    :cond_0
    return-void
.end method

.method private a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 98
    invoke-virtual {p0}, Lco/uk/getmondo/signup/EmailActivity;->n()Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/EmailActivity;->a(Landroid/view/View;)V

    .line 99
    iget-object v0, p0, Lco/uk/getmondo/signup/EmailActivity;->d:Lcom/c/b/b;

    invoke-static {p1}, Lco/uk/getmondo/signup/c;->a(Ljava/lang/String;)Lcom/c/a/a;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/c/b/b;->a(Lcom/c/a/a;)Lcom/c/b/b;

    .line 100
    return-void
.end method

.method static synthetic c(Ljava/lang/String;Lco/uk/getmondo/common/f/c;)V
    .locals 0

    .prologue
    .line 99
    invoke-virtual {p1, p0}, Lco/uk/getmondo/common/f/c;->a(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 115
    sget-object v0, Lco/uk/getmondo/signup/j;->b:Lco/uk/getmondo/signup/j;

    invoke-static {p0, v0}, Lco/uk/getmondo/signup/status/SignupStatusActivity;->a(Landroid/content/Context;Lco/uk/getmondo/signup/j;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/EmailActivity;->startActivity(Landroid/content/Intent;)V

    .line 116
    invoke-virtual {p0}, Lco/uk/getmondo/signup/EmailActivity;->finishAffinity()V

    .line 117
    return-void
.end method

.method public a(ILjava/lang/String;)V
    .locals 2

    .prologue
    .line 109
    invoke-virtual {p0}, Lco/uk/getmondo/signup/EmailActivity;->n()Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/EmailActivity;->a(Landroid/view/View;)V

    .line 110
    iget-object v0, p0, Lco/uk/getmondo/signup/EmailActivity;->d:Lcom/c/b/b;

    invoke-static {p1, p2}, Lco/uk/getmondo/signup/d;->a(ILjava/lang/String;)Lcom/c/a/a;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/c/b/b;->a(Lcom/c/a/a;)Lcom/c/b/b;

    .line 111
    return-void
.end method

.method public b()Lio/reactivex/n;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Landroid/support/v4/g/j",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 121
    iget-object v0, p0, Lco/uk/getmondo/signup/EmailActivity;->e:Lcom/b/b/b;

    return-object v0
.end method

.method public c()V
    .locals 1

    .prologue
    .line 126
    invoke-static {p0}, Lco/uk/getmondo/main/HomeActivity;->b(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/EmailActivity;->startActivity(Landroid/content/Intent;)V

    .line 127
    invoke-virtual {p0}, Lco/uk/getmondo/signup/EmailActivity;->finishAffinity()V

    .line 128
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    .line 48
    invoke-super {p0, p1}, Lco/uk/getmondo/common/activities/b;->onCreate(Landroid/os/Bundle;)V

    .line 50
    invoke-virtual {p0}, Lco/uk/getmondo/signup/EmailActivity;->l()Lco/uk/getmondo/common/h/b/b;

    move-result-object v0

    invoke-interface {v0, p0}, Lco/uk/getmondo/common/h/b/b;->a(Lco/uk/getmondo/signup/EmailActivity;)V

    .line 51
    invoke-virtual {p0}, Lco/uk/getmondo/signup/EmailActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-direct {p0, v0}, Lco/uk/getmondo/signup/EmailActivity;->a(Landroid/content/Intent;)V

    .line 53
    const v0, 0x7f050034

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/EmailActivity;->setContentView(I)V

    .line 54
    invoke-static {p0}, Lbutterknife/ButterKnife;->bind(Landroid/app/Activity;)Lbutterknife/Unbinder;

    .line 56
    const v0, 0x7f0a0394

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/EmailActivity;->setTitle(I)V

    .line 57
    iget-object v0, p0, Lco/uk/getmondo/signup/EmailActivity;->content:Landroid/widget/TextView;

    const v1, 0x7f0a0436

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lco/uk/getmondo/signup/EmailActivity;->b:Lco/uk/getmondo/common/s;

    invoke-interface {v4}, Lco/uk/getmondo/common/s;->a()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {p0, v1, v2}, Lco/uk/getmondo/signup/EmailActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 59
    iget-object v0, p0, Lco/uk/getmondo/signup/EmailActivity;->c:Lco/uk/getmondo/signup/f;

    invoke-virtual {v0, p0}, Lco/uk/getmondo/signup/f;->a(Lco/uk/getmondo/signup/f$a;)V

    .line 60
    return-void
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lco/uk/getmondo/signup/EmailActivity;->c:Lco/uk/getmondo/signup/f;

    invoke-virtual {v0}, Lco/uk/getmondo/signup/f;->b()V

    .line 65
    invoke-super {p0}, Lco/uk/getmondo/common/activities/b;->onDestroy()V

    .line 66
    return-void
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 1

    .prologue
    .line 70
    invoke-super {p0, p1}, Lco/uk/getmondo/common/activities/b;->onNewIntent(Landroid/content/Intent;)V

    .line 71
    invoke-virtual {p0, p1}, Lco/uk/getmondo/signup/EmailActivity;->setIntent(Landroid/content/Intent;)V

    .line 72
    invoke-virtual {p0}, Lco/uk/getmondo/signup/EmailActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-direct {p0, v0}, Lco/uk/getmondo/signup/EmailActivity;->a(Landroid/content/Intent;)V

    .line 73
    return-void
.end method

.method onNoEmailClick()V
    .locals 1
    .annotation build Lbutterknife/OnClick;
        value = {
            0x7f11015b
        }
    .end annotation

    .prologue
    .line 104
    iget-object v0, p0, Lco/uk/getmondo/signup/EmailActivity;->c:Lco/uk/getmondo/signup/f;

    invoke-virtual {v0, p0}, Lco/uk/getmondo/signup/f;->a(Landroid/content/Context;)V

    .line 105
    return-void
.end method

.method openMail()V
    .locals 1
    .annotation build Lbutterknife/OnClick;
        value = {
            0x7f11015a
        }
    .end annotation

    .prologue
    .line 88
    :try_start_0
    iget-object v0, p0, Lco/uk/getmondo/signup/EmailActivity;->a:Lco/uk/getmondo/common/k;

    invoke-virtual {v0, p0}, Lco/uk/getmondo/common/k;->a(Landroid/content/Context;)V

    .line 89
    iget-object v0, p0, Lco/uk/getmondo/signup/EmailActivity;->c:Lco/uk/getmondo/signup/f;

    invoke-virtual {v0}, Lco/uk/getmondo/signup/f;->a()V
    :try_end_0
    .catch Ljava/util/NoSuchElementException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 95
    :goto_0
    return-void

    .line 90
    :catch_0
    move-exception v0

    .line 91
    invoke-virtual {v0}, Ljava/util/NoSuchElementException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lco/uk/getmondo/signup/EmailActivity;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 92
    :catch_1
    move-exception v0

    .line 93
    const v0, 0x7f0a0181

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/EmailActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lco/uk/getmondo/signup/EmailActivity;->a(Ljava/lang/String;)V

    goto :goto_0
.end method
