.class public Lco/uk/getmondo/signup/EmailActivity_ViewBinding;
.super Ljava/lang/Object;
.source "EmailActivity_ViewBinding.java"

# interfaces
.implements Lbutterknife/Unbinder;


# instance fields
.field private a:Lco/uk/getmondo/signup/EmailActivity;

.field private b:Landroid/view/View;

.field private c:Landroid/view/View;


# direct methods
.method public constructor <init>(Lco/uk/getmondo/signup/EmailActivity;Landroid/view/View;)V
    .locals 3

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    iput-object p1, p0, Lco/uk/getmondo/signup/EmailActivity_ViewBinding;->a:Lco/uk/getmondo/signup/EmailActivity;

    .line 32
    const v0, 0x7f11006e

    const-string v1, "field \'content\'"

    const-class v2, Landroid/widget/TextView;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p1, Lco/uk/getmondo/signup/EmailActivity;->content:Landroid/widget/TextView;

    .line 33
    const v0, 0x7f11015a

    const-string v1, "method \'openMail\'"

    invoke-static {p2, v0, v1}, Lbutterknife/internal/Utils;->findRequiredView(Landroid/view/View;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 34
    iput-object v0, p0, Lco/uk/getmondo/signup/EmailActivity_ViewBinding;->b:Landroid/view/View;

    .line 35
    new-instance v1, Lco/uk/getmondo/signup/EmailActivity_ViewBinding$1;

    invoke-direct {v1, p0, p1}, Lco/uk/getmondo/signup/EmailActivity_ViewBinding$1;-><init>(Lco/uk/getmondo/signup/EmailActivity_ViewBinding;Lco/uk/getmondo/signup/EmailActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 41
    const v0, 0x7f11015b

    const-string v1, "method \'onNoEmailClick\'"

    invoke-static {p2, v0, v1}, Lbutterknife/internal/Utils;->findRequiredView(Landroid/view/View;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 42
    iput-object v0, p0, Lco/uk/getmondo/signup/EmailActivity_ViewBinding;->c:Landroid/view/View;

    .line 43
    new-instance v1, Lco/uk/getmondo/signup/EmailActivity_ViewBinding$2;

    invoke-direct {v1, p0, p1}, Lco/uk/getmondo/signup/EmailActivity_ViewBinding$2;-><init>(Lco/uk/getmondo/signup/EmailActivity_ViewBinding;Lco/uk/getmondo/signup/EmailActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 49
    return-void
.end method


# virtual methods
.method public unbind()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 54
    iget-object v0, p0, Lco/uk/getmondo/signup/EmailActivity_ViewBinding;->a:Lco/uk/getmondo/signup/EmailActivity;

    .line 55
    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Bindings already cleared."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 56
    :cond_0
    iput-object v1, p0, Lco/uk/getmondo/signup/EmailActivity_ViewBinding;->a:Lco/uk/getmondo/signup/EmailActivity;

    .line 58
    iput-object v1, v0, Lco/uk/getmondo/signup/EmailActivity;->content:Landroid/widget/TextView;

    .line 60
    iget-object v0, p0, Lco/uk/getmondo/signup/EmailActivity_ViewBinding;->b:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 61
    iput-object v1, p0, Lco/uk/getmondo/signup/EmailActivity_ViewBinding;->b:Landroid/view/View;

    .line 62
    iget-object v0, p0, Lco/uk/getmondo/signup/EmailActivity_ViewBinding;->c:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 63
    iput-object v1, p0, Lco/uk/getmondo/signup/EmailActivity_ViewBinding;->c:Landroid/view/View;

    .line 64
    return-void
.end method
