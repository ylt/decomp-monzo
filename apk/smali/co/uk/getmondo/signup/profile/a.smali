.class public final Lco/uk/getmondo/signup/profile/a;
.super Ljava/lang/Object;
.source "AddressConfirmationActivity_MembersInjector.java"

# interfaces
.implements Lb/a;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lb/a",
        "<",
        "Lco/uk/getmondo/signup/profile/AddressConfirmationActivity;",
        ">;"
    }
.end annotation


# static fields
.field static final synthetic a:Z


# instance fields
.field private final b:Ljavax/a/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/common/q;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Ljavax/a/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/signup/profile/b;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 9
    const-class v0, Lco/uk/getmondo/signup/profile/a;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lco/uk/getmondo/signup/profile/a;->a:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Ljavax/a/a;Ljavax/a/a;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/common/q;",
            ">;",
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/signup/profile/b;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    sget-boolean v0, Lco/uk/getmondo/signup/profile/a;->a:Z

    if-nez v0, :cond_0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 23
    :cond_0
    iput-object p1, p0, Lco/uk/getmondo/signup/profile/a;->b:Ljavax/a/a;

    .line 24
    sget-boolean v0, Lco/uk/getmondo/signup/profile/a;->a:Z

    if-nez v0, :cond_1

    if-nez p2, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 25
    :cond_1
    iput-object p2, p0, Lco/uk/getmondo/signup/profile/a;->c:Ljavax/a/a;

    .line 26
    return-void
.end method

.method public static a(Ljavax/a/a;Ljavax/a/a;)Lb/a;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/common/q;",
            ">;",
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/signup/profile/b;",
            ">;)",
            "Lb/a",
            "<",
            "Lco/uk/getmondo/signup/profile/AddressConfirmationActivity;",
            ">;"
        }
    .end annotation

    .prologue
    .line 31
    new-instance v0, Lco/uk/getmondo/signup/profile/a;

    invoke-direct {v0, p0, p1}, Lco/uk/getmondo/signup/profile/a;-><init>(Ljavax/a/a;Ljavax/a/a;)V

    return-object v0
.end method


# virtual methods
.method public a(Lco/uk/getmondo/signup/profile/AddressConfirmationActivity;)V
    .locals 2

    .prologue
    .line 37
    if-nez p1, :cond_0

    .line 38
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Cannot inject members into a null reference"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 40
    :cond_0
    iget-object v0, p0, Lco/uk/getmondo/signup/profile/a;->b:Ljavax/a/a;

    invoke-interface {v0}, Ljavax/a/a;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/common/q;

    iput-object v0, p1, Lco/uk/getmondo/signup/a;->e:Lco/uk/getmondo/common/q;

    .line 41
    iget-object v0, p0, Lco/uk/getmondo/signup/profile/a;->c:Ljavax/a/a;

    invoke-interface {v0}, Ljavax/a/a;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/signup/profile/b;

    iput-object v0, p1, Lco/uk/getmondo/signup/profile/AddressConfirmationActivity;->b:Lco/uk/getmondo/signup/profile/b;

    .line 42
    return-void
.end method

.method public bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 9
    check-cast p1, Lco/uk/getmondo/signup/profile/AddressConfirmationActivity;

    invoke-virtual {p0, p1}, Lco/uk/getmondo/signup/profile/a;->a(Lco/uk/getmondo/signup/profile/AddressConfirmationActivity;)V

    return-void
.end method
