.class public final Lco/uk/getmondo/signup/profile/l;
.super Lco/uk/getmondo/common/f/a;
.source "ProfileDetailsFragment.kt"

# interfaces
.implements Lco/uk/getmondo/signup/profile/n$a;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lco/uk/getmondo/signup/profile/l$a;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000^\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\r\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0006\n\u0002\u0010\u000b\n\u0002\u0008\u000b\u0018\u00002\u00020\u00012\u00020\u0002:\u00010B\u0005\u00a2\u0006\u0002\u0010\u0003J\u0008\u0010\n\u001a\u00020\u000bH\u0016J\u000e\u0010\u000c\u001a\u0008\u0012\u0004\u0012\u00020\u000b0\rH\u0016J\u0010\u0010\u000e\u001a\u00020\u000b2\u0006\u0010\u000f\u001a\u00020\u0010H\u0016J\u0012\u0010\u0011\u001a\u00020\u000b2\u0008\u0010\u0012\u001a\u0004\u0018\u00010\u0013H\u0016J$\u0010\u0014\u001a\u00020\u00152\u0006\u0010\u0016\u001a\u00020\u00172\u0008\u0010\u0018\u001a\u0004\u0018\u00010\u00192\u0008\u0010\u0012\u001a\u0004\u0018\u00010\u0013H\u0016J\u000e\u0010\u001a\u001a\u0008\u0012\u0004\u0012\u00020\u001b0\rH\u0016J\u0008\u0010\u001c\u001a\u00020\u000bH\u0016J\u000e\u0010\u001d\u001a\u0008\u0012\u0004\u0012\u00020\u001b0\rH\u0016J\u000e\u0010\u001e\u001a\u0008\u0012\u0004\u0012\u00020\u001f0\rH\u0016J\u001a\u0010 \u001a\u00020\u000b2\u0006\u0010!\u001a\u00020\u00152\u0008\u0010\u0012\u001a\u0004\u0018\u00010\u0013H\u0016J\u0008\u0010\"\u001a\u00020\u000bH\u0016J\u0008\u0010#\u001a\u00020\u000bH\u0016J\u0010\u0010$\u001a\u00020\u000b2\u0006\u0010%\u001a\u00020&H\u0016J\u0010\u0010\'\u001a\u00020\u000b2\u0006\u0010(\u001a\u00020&H\u0016J\u0012\u0010)\u001a\u00020\u000b2\u0008\u0010*\u001a\u0004\u0018\u00010\u001fH\u0016J\u0008\u0010+\u001a\u00020\u000bH\u0016J\u0008\u0010,\u001a\u00020\u000bH\u0016J\u0008\u0010-\u001a\u00020\u000bH\u0016J\u0008\u0010.\u001a\u00020\u000bH\u0016J\u0008\u0010/\u001a\u00020\u000bH\u0016R\u001e\u0010\u0004\u001a\u00020\u00058\u0006@\u0006X\u0087.\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u0006\u0010\u0007\"\u0004\u0008\u0008\u0010\t\u00a8\u00061"
    }
    d2 = {
        "Lco/uk/getmondo/signup/profile/ProfileDetailsFragment;",
        "Lco/uk/getmondo/common/fragments/BaseFragment;",
        "Lco/uk/getmondo/signup/profile/ProfileDetailsPresenter$View;",
        "()V",
        "presenter",
        "Lco/uk/getmondo/signup/profile/ProfileDetailsPresenter;",
        "getPresenter",
        "()Lco/uk/getmondo/signup/profile/ProfileDetailsPresenter;",
        "setPresenter",
        "(Lco/uk/getmondo/signup/profile/ProfileDetailsPresenter;)V",
        "clearDateOfBirthError",
        "",
        "onAddPreferredNameClicked",
        "Lio/reactivex/Observable;",
        "onAttach",
        "context",
        "Landroid/content/Context;",
        "onCreate",
        "savedInstanceState",
        "Landroid/os/Bundle;",
        "onCreateView",
        "Landroid/view/View;",
        "inflater",
        "Landroid/view/LayoutInflater;",
        "container",
        "Landroid/view/ViewGroup;",
        "onDateOfBirthTextChange",
        "",
        "onDestroyView",
        "onLegalNameTextChange",
        "onNextClicked",
        "Lco/uk/getmondo/signup/profile/ProfileDetailsFormData;",
        "onViewCreated",
        "view",
        "profileDetailsSubmitted",
        "reloadSignupStatus",
        "setNextButtonEnabled",
        "enabled",
        "",
        "setNextButtonLoading",
        "loading",
        "showForm",
        "profileData",
        "showInvalidDateOfBirthError",
        "showLegalNameUnsupportedCharError",
        "showPreferredNameField",
        "showPreferredNameUnsupportedCharError",
        "showUnderAgeError",
        "StepListener",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field public a:Lco/uk/getmondo/signup/profile/n;

.field private c:Ljava/util/HashMap;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Lco/uk/getmondo/common/f/a;-><init>()V

    return-void
.end method


# virtual methods
.method public a(I)Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lco/uk/getmondo/signup/profile/l;->c:Ljava/util/HashMap;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lco/uk/getmondo/signup/profile/l;->c:Ljava/util/HashMap;

    :cond_0
    iget-object v0, p0, Lco/uk/getmondo/signup/profile/l;->c:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    if-nez v0, :cond_1

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getView()Landroid/view/View;

    move-result-object v0

    if-nez v0, :cond_2

    const/4 v0, 0x0

    :cond_1
    :goto_0
    return-object v0

    :cond_2
    invoke-virtual {v0, p1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/signup/profile/l;->c:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method public a()Lio/reactivex/n;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Lkotlin/n;",
            ">;"
        }
    .end annotation

    .prologue
    .line 54
    sget v0, Lco/uk/getmondo/c$a;->addPreferredNameButton:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/profile/l;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 142
    invoke-static {v0}, Lcom/b/a/c/c;->a(Landroid/view/View;)Lio/reactivex/n;

    move-result-object v1

    sget-object v0, Lcom/b/a/a/d;->a:Lcom/b/a/a/d;

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {v1, v0}, Lio/reactivex/n;->map(Lio/reactivex/c/h;)Lio/reactivex/n;

    move-result-object v0

    const-string v1, "RxView.clicks(this).map(VoidToUnit)"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 54
    return-object v0
.end method

.method public a(Lco/uk/getmondo/signup/profile/k;)V
    .locals 2

    .prologue
    .line 116
    if-eqz p1, :cond_0

    .line 117
    sget v0, Lco/uk/getmondo/c$a;->legalNameEditText:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/profile/l;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    invoke-virtual {p1}, Lco/uk/getmondo/signup/profile/k;->a()Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 118
    sget v0, Lco/uk/getmondo/c$a;->dateOfBirthEditText:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/profile/l;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    invoke-virtual {p1}, Lco/uk/getmondo/signup/profile/k;->c()Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 119
    invoke-virtual {p1}, Lco/uk/getmondo/signup/profile/k;->b()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 120
    sget v0, Lco/uk/getmondo/c$a;->preferredNameEditText:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/profile/l;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    invoke-virtual {p1}, Lco/uk/getmondo/signup/profile/k;->b()Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 121
    invoke-virtual {p0}, Lco/uk/getmondo/signup/profile/l;->f()V

    .line 125
    :cond_0
    sget v0, Lco/uk/getmondo/c$a;->legalNameEditText:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/profile/l;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->requestFocus()Z

    .line 126
    sget v0, Lco/uk/getmondo/c$a;->profileDetailsFormContainer:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/profile/l;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ScrollView;

    invoke-static {v0}, Lco/uk/getmondo/common/ae;->a(Landroid/view/View;)V

    .line 127
    sget v0, Lco/uk/getmondo/c$a;->profileDetailsProgress:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/profile/l;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    invoke-static {v0}, Lco/uk/getmondo/common/ae;->b(Landroid/view/View;)V

    .line 128
    return-void
.end method

.method public a(Z)V
    .locals 1

    .prologue
    .line 79
    sget v0, Lco/uk/getmondo/c$a;->profileDetailsNextButton:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/profile/l;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/common/ui/ProgressButton;

    invoke-virtual {v0, p1}, Lco/uk/getmondo/common/ui/ProgressButton;->setEnabled(Z)V

    .line 80
    return-void
.end method

.method public b()V
    .locals 1

    .prologue
    .line 131
    invoke-virtual {p0}, Lco/uk/getmondo/signup/profile/l;->getActivity()Landroid/support/v4/app/j;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/signup/i$a;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lco/uk/getmondo/signup/i$a;->b()V

    .line 132
    :cond_0
    return-void
.end method

.method public b(Z)V
    .locals 1

    .prologue
    .line 96
    sget v0, Lco/uk/getmondo/c$a;->profileDetailsNextButton:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/profile/l;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/common/ui/ProgressButton;

    invoke-virtual {v0, p1}, Lco/uk/getmondo/common/ui/ProgressButton;->setLoading(Z)V

    .line 97
    return-void
.end method

.method public c()Lio/reactivex/n;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Ljava/lang/CharSequence;",
            ">;"
        }
    .end annotation

    .prologue
    .line 56
    sget v0, Lco/uk/getmondo/c$a;->dateOfBirthEditText:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/profile/l;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    .line 143
    invoke-static {v0}, Lcom/b/a/d/e;->c(Landroid/widget/TextView;)Lcom/b/a/a;

    move-result-object v0

    const-string v1, "RxTextView.textChanges(this)"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lio/reactivex/n;

    .line 56
    return-object v0
.end method

.method public d()Lio/reactivex/n;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Ljava/lang/CharSequence;",
            ">;"
        }
    .end annotation

    .prologue
    .line 58
    sget v0, Lco/uk/getmondo/c$a;->legalNameEditText:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/profile/l;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    .line 144
    invoke-static {v0}, Lcom/b/a/d/e;->c(Landroid/widget/TextView;)Lcom/b/a/a;

    move-result-object v0

    const-string v1, "RxTextView.textChanges(this)"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lio/reactivex/n;

    .line 58
    return-object v0
.end method

.method public e()Lio/reactivex/n;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Lco/uk/getmondo/signup/profile/k;",
            ">;"
        }
    .end annotation

    .prologue
    .line 61
    sget v0, Lco/uk/getmondo/c$a;->profileDetailsNextButton:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/profile/l;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/common/ui/ProgressButton;

    .line 145
    invoke-static {v0}, Lcom/b/a/c/c;->a(Landroid/view/View;)Lio/reactivex/n;

    move-result-object v1

    sget-object v0, Lcom/b/a/a/d;->a:Lcom/b/a/a/d;

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {v1, v0}, Lio/reactivex/n;->map(Lio/reactivex/c/h;)Lio/reactivex/n;

    move-result-object v0

    const-string v1, "RxView.clicks(this).map(VoidToUnit)"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lio/reactivex/r;

    .line 61
    sget v1, Lco/uk/getmondo/c$a;->dateOfBirthEditText:I

    invoke-virtual {p0, v1}, Lco/uk/getmondo/signup/profile/l;->a(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/EditText;

    .line 146
    invoke-static {v1}, Lcom/b/a/d/e;->a(Landroid/widget/TextView;)Lio/reactivex/n;

    move-result-object v1

    const-string v2, "RxTextView.editorActions(this)"

    invoke-static {v1, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v1, Lio/reactivex/r;

    .line 61
    invoke-static {v0, v1}, Lio/reactivex/n;->merge(Lio/reactivex/r;Lio/reactivex/r;)Lio/reactivex/n;

    move-result-object v1

    .line 62
    new-instance v0, Lco/uk/getmondo/signup/profile/l$b;

    invoke-direct {v0, p0}, Lco/uk/getmondo/signup/profile/l$b;-><init>(Lco/uk/getmondo/signup/profile/l;)V

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {v1, v0}, Lio/reactivex/n;->map(Lio/reactivex/c/h;)Lio/reactivex/n;

    move-result-object v0

    const-string v1, "Observable.merge(profile\u2026      )\n                }"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public f()V
    .locals 1

    .prologue
    .line 73
    sget v0, Lco/uk/getmondo/c$a;->addPreferredNameButton:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/profile/l;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    invoke-static {v0}, Lco/uk/getmondo/common/ae;->b(Landroid/view/View;)V

    .line 74
    sget v0, Lco/uk/getmondo/c$a;->preferredNameInputLayout:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/profile/l;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/design/widget/TextInputLayout;

    invoke-static {v0}, Lco/uk/getmondo/common/ae;->a(Landroid/view/View;)V

    .line 75
    sget v0, Lco/uk/getmondo/c$a;->preferredNameInputLayout:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/profile/l;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/design/widget/TextInputLayout;

    invoke-virtual {v0}, Landroid/support/design/widget/TextInputLayout;->getEditText()Landroid/widget/EditText;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/widget/EditText;->requestFocus()Z

    .line 76
    :cond_0
    return-void
.end method

.method public g()V
    .locals 2

    .prologue
    .line 83
    sget v0, Lco/uk/getmondo/c$a;->dateOfBirthInputLayout:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/profile/l;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/design/widget/TextInputLayout;

    const v1, 0x7f0a0323

    invoke-virtual {p0, v1}, Lco/uk/getmondo/signup/profile/l;->getString(I)Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Landroid/support/design/widget/TextInputLayout;->setError(Ljava/lang/CharSequence;)V

    .line 84
    return-void
.end method

.method public h()V
    .locals 2

    .prologue
    .line 87
    sget v0, Lco/uk/getmondo/c$a;->dateOfBirthInputLayout:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/profile/l;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/design/widget/TextInputLayout;

    const-string v1, ""

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Landroid/support/design/widget/TextInputLayout;->setError(Ljava/lang/CharSequence;)V

    .line 88
    sget v0, Lco/uk/getmondo/c$a;->dateOfBirthInputLayout:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/profile/l;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/design/widget/TextInputLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/design/widget/TextInputLayout;->setErrorEnabled(Z)V

    .line 89
    return-void
.end method

.method public i()V
    .locals 2

    .prologue
    .line 92
    sget v0, Lco/uk/getmondo/c$a;->dateOfBirthInputLayout:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/profile/l;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/design/widget/TextInputLayout;

    const v1, 0x7f0a032a

    invoke-virtual {p0, v1}, Lco/uk/getmondo/signup/profile/l;->getString(I)Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Landroid/support/design/widget/TextInputLayout;->setError(Ljava/lang/CharSequence;)V

    .line 93
    return-void
.end method

.method public j()V
    .locals 3

    .prologue
    .line 100
    const v0, 0x7f0a0324

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/profile/l;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 102
    const v1, 0x7f0a032b

    invoke-virtual {p0, v1}, Lco/uk/getmondo/signup/profile/l;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 103
    const/4 v2, 0x0

    .line 100
    invoke-static {v0, v1, v2}, Lco/uk/getmondo/common/d/a;->a(Ljava/lang/String;Ljava/lang/String;Z)Lco/uk/getmondo/common/d/a;

    move-result-object v0

    .line 104
    invoke-virtual {p0}, Lco/uk/getmondo/signup/profile/l;->getFragmentManager()Landroid/support/v4/app/n;

    move-result-object v1

    const-class v2, Lco/uk/getmondo/common/d/a;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lco/uk/getmondo/common/d/a;->show(Landroid/support/v4/app/n;Ljava/lang/String;)V

    .line 105
    return-void
.end method

.method public k()V
    .locals 3

    .prologue
    .line 108
    const v0, 0x7f0a0328

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/profile/l;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 110
    const v1, 0x7f0a032b

    invoke-virtual {p0, v1}, Lco/uk/getmondo/signup/profile/l;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 111
    const/4 v2, 0x0

    .line 108
    invoke-static {v0, v1, v2}, Lco/uk/getmondo/common/d/a;->a(Ljava/lang/String;Ljava/lang/String;Z)Lco/uk/getmondo/common/d/a;

    move-result-object v0

    .line 112
    invoke-virtual {p0}, Lco/uk/getmondo/signup/profile/l;->getFragmentManager()Landroid/support/v4/app/n;

    move-result-object v1

    const-class v2, Lco/uk/getmondo/common/d/a;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lco/uk/getmondo/common/d/a;->show(Landroid/support/v4/app/n;Ljava/lang/String;)V

    .line 113
    return-void
.end method

.method public l()V
    .locals 2

    .prologue
    .line 135
    invoke-virtual {p0}, Lco/uk/getmondo/signup/profile/l;->getActivity()Landroid/support/v4/app/j;

    move-result-object v0

    if-nez v0, :cond_0

    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type co.uk.getmondo.signup.profile.ProfileDetailsFragment.StepListener"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    check-cast v0, Lco/uk/getmondo/signup/profile/l$a;

    invoke-interface {v0}, Lco/uk/getmondo/signup/profile/l$a;->c()V

    .line 136
    return-void
.end method

.method public m()V
    .locals 1

    iget-object v0, p0, Lco/uk/getmondo/signup/profile/l;->c:Ljava/util/HashMap;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lco/uk/getmondo/signup/profile/l;->c:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    :cond_0
    return-void
.end method

.method public onAttach(Landroid/content/Context;)V
    .locals 2

    .prologue
    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 42
    invoke-super {p0, p1}, Lco/uk/getmondo/common/f/a;->onAttach(Landroid/content/Context;)V

    .line 43
    instance-of v0, p1, Lco/uk/getmondo/signup/profile/l$a;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Activity must implement "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-class v1, Lco/uk/getmondo/signup/profile/l$a;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    .line 44
    :cond_0
    instance-of v0, p1, Lco/uk/getmondo/signup/i$a;

    if-nez v0, :cond_1

    .line 45
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Activity must implement "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-class v1, Lco/uk/getmondo/signup/i$a;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 44
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    .line 47
    :cond_1
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 27
    invoke-super {p0, p1}, Lco/uk/getmondo/common/f/a;->onCreate(Landroid/os/Bundle;)V

    .line 28
    invoke-virtual {p0}, Lco/uk/getmondo/signup/profile/l;->B()Lco/uk/getmondo/common/h/b/b;

    move-result-object v0

    invoke-interface {v0, p0}, Lco/uk/getmondo/common/h/b/b;->a(Lco/uk/getmondo/signup/profile/l;)V

    .line 29
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2

    .prologue
    const-string v0, "inflater"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 32
    const v0, 0x7f0500a4

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    const-string v1, "inflater.inflate(R.layou\u2026etails, container, false)"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public onDestroyView()V
    .locals 2

    .prologue
    .line 50
    iget-object v0, p0, Lco/uk/getmondo/signup/profile/l;->a:Lco/uk/getmondo/signup/profile/n;

    if-nez v0, :cond_0

    const-string v1, "presenter"

    invoke-static {v1}, Lkotlin/d/b/l;->b(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0}, Lco/uk/getmondo/signup/profile/n;->b()V

    .line 51
    invoke-super {p0}, Lco/uk/getmondo/common/f/a;->onDestroyView()V

    .line 52
    invoke-virtual {p0}, Lco/uk/getmondo/signup/profile/l;->m()V

    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 2

    .prologue
    const-string v0, "view"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 35
    invoke-super {p0, p1, p2}, Lco/uk/getmondo/common/f/a;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 36
    sget v0, Lco/uk/getmondo/c$a;->dateOfBirthEditText:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/profile/l;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    new-instance v1, Lco/uk/getmondo/common/k/d;

    invoke-direct {v1}, Lco/uk/getmondo/common/k/d;-><init>()V

    check-cast v1, Landroid/text/TextWatcher;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 38
    iget-object v0, p0, Lco/uk/getmondo/signup/profile/l;->a:Lco/uk/getmondo/signup/profile/n;

    if-nez v0, :cond_0

    const-string v1, "presenter"

    invoke-static {v1}, Lkotlin/d/b/l;->b(Ljava/lang/String;)V

    :cond_0
    check-cast p0, Lco/uk/getmondo/signup/profile/n$a;

    invoke-virtual {v0, p0}, Lco/uk/getmondo/signup/profile/n;->a(Lco/uk/getmondo/signup/profile/n$a;)V

    .line 39
    return-void
.end method
