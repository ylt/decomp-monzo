.class final Lco/uk/getmondo/signup/profile/n$h;
.super Ljava/lang/Object;
.source "ProfileDetailsPresenter.kt"

# interfaces
.implements Lio/reactivex/c/g;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lco/uk/getmondo/signup/profile/n;->a(Lco/uk/getmondo/signup/profile/n$a;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/g",
        "<",
        "Ljava/lang/CharSequence;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\r\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "",
        "dateText",
        "",
        "kotlin.jvm.PlatformType",
        "accept"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field final synthetic a:Lco/uk/getmondo/signup/profile/n;

.field final synthetic b:Lco/uk/getmondo/signup/profile/n$a;


# direct methods
.method constructor <init>(Lco/uk/getmondo/signup/profile/n;Lco/uk/getmondo/signup/profile/n$a;)V
    .locals 0

    iput-object p1, p0, Lco/uk/getmondo/signup/profile/n$h;->a:Lco/uk/getmondo/signup/profile/n;

    iput-object p2, p0, Lco/uk/getmondo/signup/profile/n$h;->b:Lco/uk/getmondo/signup/profile/n$a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/CharSequence;)V
    .locals 4

    .prologue
    .line 75
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v0

    iget-object v1, p0, Lco/uk/getmondo/signup/profile/n$h;->a:Lco/uk/getmondo/signup/profile/n;

    invoke-static {v1}, Lco/uk/getmondo/signup/profile/n;->c(Lco/uk/getmondo/signup/profile/n;)I

    move-result v1

    if-eq v0, v1, :cond_1

    .line 76
    iget-object v0, p0, Lco/uk/getmondo/signup/profile/n$h;->b:Lco/uk/getmondo/signup/profile/n$a;

    invoke-interface {v0}, Lco/uk/getmondo/signup/profile/n$a;->h()V

    .line 83
    :cond_0
    :goto_0
    return-void

    .line 78
    :cond_1
    iget-object v0, p0, Lco/uk/getmondo/signup/profile/n$h;->a:Lco/uk/getmondo/signup/profile/n;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lco/uk/getmondo/signup/profile/n;->a(Lco/uk/getmondo/signup/profile/n;Ljava/lang/String;)Lorg/threeten/bp/LocalDate;

    move-result-object v0

    .line 79
    if-nez v0, :cond_2

    .line 80
    iget-object v0, p0, Lco/uk/getmondo/signup/profile/n$h;->b:Lco/uk/getmondo/signup/profile/n$a;

    invoke-interface {v0}, Lco/uk/getmondo/signup/profile/n$a;->g()V

    goto :goto_0

    .line 81
    :cond_2
    const-wide/16 v2, 0x12

    invoke-static {v0, v2, v3}, Lco/uk/getmondo/common/c/c;->a(Lorg/threeten/bp/LocalDate;J)Z

    move-result v0

    if-nez v0, :cond_0

    .line 82
    iget-object v0, p0, Lco/uk/getmondo/signup/profile/n$h;->b:Lco/uk/getmondo/signup/profile/n$a;

    invoke-interface {v0}, Lco/uk/getmondo/signup/profile/n$a;->i()V

    goto :goto_0
.end method

.method public bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 25
    check-cast p1, Ljava/lang/CharSequence;

    invoke-virtual {p0, p1}, Lco/uk/getmondo/signup/profile/n$h;->a(Ljava/lang/CharSequence;)V

    return-void
.end method
