.class public final Lco/uk/getmondo/signup/profile/ProfileCreationActivity;
.super Lco/uk/getmondo/signup/a;
.source "ProfileCreationActivity.kt"

# interfaces
.implements Lco/uk/getmondo/signup/profile/e$b;
.implements Lco/uk/getmondo/signup/profile/l$a;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lco/uk/getmondo/signup/profile/ProfileCreationActivity$a;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000\"\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\u0018\u0000 \u000b2\u00020\u00012\u00020\u00022\u00020\u0003:\u0001\u000bB\u0005\u00a2\u0006\u0002\u0010\u0004J\u0012\u0010\u0005\u001a\u00020\u00062\u0008\u0010\u0007\u001a\u0004\u0018\u00010\u0008H\u0014J\u0008\u0010\t\u001a\u00020\u0006H\u0016J\u0008\u0010\n\u001a\u00020\u0006H\u0016\u00a8\u0006\u000c"
    }
    d2 = {
        "Lco/uk/getmondo/signup/profile/ProfileCreationActivity;",
        "Lco/uk/getmondo/signup/BaseSignupActivity;",
        "Lco/uk/getmondo/signup/profile/ProfileDetailsFragment$StepListener;",
        "Lco/uk/getmondo/signup/profile/ProfileAddressFragment$StepListener;",
        "()V",
        "onCreate",
        "",
        "savedInstanceState",
        "Landroid/os/Bundle;",
        "onProfileDetailsSubmitted",
        "onProfileReady",
        "Companion",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# static fields
.field public static final a:Lco/uk/getmondo/signup/profile/ProfileCreationActivity$a;


# instance fields
.field private b:Ljava/util/HashMap;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lco/uk/getmondo/signup/profile/ProfileCreationActivity$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lco/uk/getmondo/signup/profile/ProfileCreationActivity$a;-><init>(Lkotlin/d/b/i;)V

    sput-object v0, Lco/uk/getmondo/signup/profile/ProfileCreationActivity;->a:Lco/uk/getmondo/signup/profile/ProfileCreationActivity$a;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0}, Lco/uk/getmondo/signup/a;-><init>()V

    return-void
.end method


# virtual methods
.method public a(I)Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lco/uk/getmondo/signup/profile/ProfileCreationActivity;->b:Ljava/util/HashMap;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lco/uk/getmondo/signup/profile/ProfileCreationActivity;->b:Ljava/util/HashMap;

    :cond_0
    iget-object v0, p0, Lco/uk/getmondo/signup/profile/ProfileCreationActivity;->b:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    if-nez v0, :cond_1

    invoke-virtual {p0, p1}, Landroid/support/v4/app/j;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/signup/profile/ProfileCreationActivity;->b:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    return-object v0
.end method

.method public c()V
    .locals 4

    .prologue
    .line 32
    invoke-virtual {p0}, Lco/uk/getmondo/signup/profile/ProfileCreationActivity;->getSupportFragmentManager()Landroid/support/v4/app/n;

    move-result-object v0

    .line 33
    invoke-virtual {v0}, Landroid/support/v4/app/n;->a()Landroid/support/v4/app/t;

    move-result-object v1

    .line 34
    const v2, 0x7f1101ec

    sget-object v0, Lco/uk/getmondo/signup/profile/e;->d:Lco/uk/getmondo/signup/profile/e$a;

    invoke-virtual {p0}, Lco/uk/getmondo/signup/profile/ProfileCreationActivity;->a()Lco/uk/getmondo/signup/j;

    move-result-object v3

    invoke-virtual {v0, v3}, Lco/uk/getmondo/signup/profile/e$a;->a(Lco/uk/getmondo/signup/j;)Lco/uk/getmondo/signup/profile/e;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/Fragment;

    invoke-virtual {v1, v2, v0}, Landroid/support/v4/app/t;->b(ILandroid/support/v4/app/Fragment;)Landroid/support/v4/app/t;

    move-result-object v0

    .line 35
    const/16 v1, 0x1001

    invoke-virtual {v0, v1}, Landroid/support/v4/app/t;->a(I)Landroid/support/v4/app/t;

    move-result-object v0

    .line 36
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v4/app/t;->a(Ljava/lang/String;)Landroid/support/v4/app/t;

    move-result-object v0

    .line 37
    invoke-virtual {v0}, Landroid/support/v4/app/t;->c()I

    .line 38
    return-void
.end method

.method public f_()V
    .locals 1

    .prologue
    .line 41
    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/profile/ProfileCreationActivity;->setResult(I)V

    .line 42
    invoke-virtual {p0}, Lco/uk/getmondo/signup/profile/ProfileCreationActivity;->finish()V

    .line 43
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 16
    invoke-super {p0, p1}, Lco/uk/getmondo/signup/a;->onCreate(Landroid/os/Bundle;)V

    .line 17
    const v0, 0x7f050057

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/profile/ProfileCreationActivity;->setContentView(I)V

    .line 19
    invoke-virtual {p0}, Lco/uk/getmondo/signup/profile/ProfileCreationActivity;->l()Lco/uk/getmondo/common/h/b/b;

    move-result-object v0

    invoke-interface {v0, p0}, Lco/uk/getmondo/common/h/b/b;->a(Lco/uk/getmondo/signup/profile/ProfileCreationActivity;)V

    .line 20
    invoke-virtual {p0}, Lco/uk/getmondo/signup/profile/ProfileCreationActivity;->getSupportActionBar()Landroid/support/v7/app/a;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0, v1}, Landroid/support/v7/app/a;->b(Z)V

    .line 21
    :cond_0
    invoke-virtual {p0}, Lco/uk/getmondo/signup/profile/ProfileCreationActivity;->getSupportActionBar()Landroid/support/v7/app/a;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v0, v1}, Landroid/support/v7/app/a;->d(Z)V

    .line 23
    :cond_1
    if-nez p1, :cond_2

    .line 24
    invoke-virtual {p0}, Lco/uk/getmondo/signup/profile/ProfileCreationActivity;->getSupportFragmentManager()Landroid/support/v4/app/n;

    move-result-object v0

    .line 25
    invoke-virtual {v0}, Landroid/support/v4/app/n;->a()Landroid/support/v4/app/t;

    move-result-object v1

    .line 26
    const v2, 0x7f1101ec

    new-instance v0, Lco/uk/getmondo/signup/profile/l;

    invoke-direct {v0}, Lco/uk/getmondo/signup/profile/l;-><init>()V

    check-cast v0, Landroid/support/v4/app/Fragment;

    invoke-virtual {v1, v2, v0}, Landroid/support/v4/app/t;->a(ILandroid/support/v4/app/Fragment;)Landroid/support/v4/app/t;

    move-result-object v0

    .line 27
    invoke-virtual {v0}, Landroid/support/v4/app/t;->c()I

    .line 29
    :cond_2
    return-void
.end method
