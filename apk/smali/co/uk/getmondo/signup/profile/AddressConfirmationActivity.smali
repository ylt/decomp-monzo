.class public final Lco/uk/getmondo/signup/profile/AddressConfirmationActivity;
.super Lco/uk/getmondo/signup/a;
.source "AddressConfirmationActivity.kt"

# interfaces
.implements Lco/uk/getmondo/signup/profile/b$a;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lco/uk/getmondo/signup/profile/AddressConfirmationActivity$a;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000<\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0006\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0010\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0006\u0018\u0000 !2\u00020\u00012\u00020\u0002:\u0001!B\u0005\u00a2\u0006\u0002\u0010\u0003J\u0008\u0010\u0018\u001a\u00020\u0019H\u0016J\u0012\u0010\u001a\u001a\u00020\u00192\u0008\u0010\u001b\u001a\u0004\u0018\u00010\u001cH\u0014J\u0010\u0010\u001d\u001a\u00020\u00192\u0006\u0010\u001e\u001a\u00020\rH\u0016J\u0010\u0010\u001f\u001a\u00020\u00192\u0006\u0010 \u001a\u00020\rH\u0016R#\u0010\u0004\u001a\n \u0006*\u0004\u0018\u00010\u00050\u00058BX\u0082\u0084\u0002\u00a2\u0006\u000c\n\u0004\u0008\t\u0010\n\u001a\u0004\u0008\u0007\u0010\u0008R\u001a\u0010\u000b\u001a\u0008\u0012\u0004\u0012\u00020\r0\u000c8VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u000e\u0010\u000fR\u001a\u0010\u0010\u001a\u0008\u0012\u0004\u0012\u00020\u00050\u000c8VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0011\u0010\u000fR\u001e\u0010\u0012\u001a\u00020\u00138\u0006@\u0006X\u0087.\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u0014\u0010\u0015\"\u0004\u0008\u0016\u0010\u0017\u00a8\u0006\""
    }
    d2 = {
        "Lco/uk/getmondo/signup/profile/AddressConfirmationActivity;",
        "Lco/uk/getmondo/signup/BaseSignupActivity;",
        "Lco/uk/getmondo/signup/profile/AddressConfirmationPresenter$View;",
        "()V",
        "address",
        "Lco/uk/getmondo/api/model/Address;",
        "kotlin.jvm.PlatformType",
        "getAddress",
        "()Lco/uk/getmondo/api/model/Address;",
        "address$delegate",
        "Lkotlin/Lazy;",
        "addressInputValid",
        "Lio/reactivex/Observable;",
        "",
        "getAddressInputValid",
        "()Lio/reactivex/Observable;",
        "confirmAddressClicked",
        "getConfirmAddressClicked",
        "presenter",
        "Lco/uk/getmondo/signup/profile/AddressConfirmationPresenter;",
        "getPresenter",
        "()Lco/uk/getmondo/signup/profile/AddressConfirmationPresenter;",
        "setPresenter",
        "(Lco/uk/getmondo/signup/profile/AddressConfirmationPresenter;)V",
        "finishProfileStage",
        "",
        "onCreate",
        "savedInstanceState",
        "Landroid/os/Bundle;",
        "setConfirmButtonEnabled",
        "enabled",
        "setConfirmButtonLoading",
        "loading",
        "Companion",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# static fields
.field static final synthetic a:[Lkotlin/reflect/l;

.field public static final g:Lco/uk/getmondo/signup/profile/AddressConfirmationActivity$a;


# instance fields
.field public b:Lco/uk/getmondo/signup/profile/b;

.field private final h:Lkotlin/c;

.field private i:Ljava/util/HashMap;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const/4 v0, 0x1

    new-array v1, v0, [Lkotlin/reflect/l;

    const/4 v2, 0x0

    new-instance v0, Lkotlin/d/b/w;

    const-class v3, Lco/uk/getmondo/signup/profile/AddressConfirmationActivity;

    invoke-static {v3}, Lkotlin/d/b/y;->a(Ljava/lang/Class;)Lkotlin/reflect/c;

    move-result-object v3

    const-string v4, "address"

    const-string v5, "getAddress()Lco/uk/getmondo/api/model/Address;"

    invoke-direct {v0, v3, v4, v5}, Lkotlin/d/b/w;-><init>(Lkotlin/reflect/e;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v0}, Lkotlin/d/b/y;->a(Lkotlin/d/b/v;)Lkotlin/reflect/n;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/l;

    aput-object v0, v1, v2

    sput-object v1, Lco/uk/getmondo/signup/profile/AddressConfirmationActivity;->a:[Lkotlin/reflect/l;

    new-instance v0, Lco/uk/getmondo/signup/profile/AddressConfirmationActivity$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lco/uk/getmondo/signup/profile/AddressConfirmationActivity$a;-><init>(Lkotlin/d/b/i;)V

    sput-object v0, Lco/uk/getmondo/signup/profile/AddressConfirmationActivity;->g:Lco/uk/getmondo/signup/profile/AddressConfirmationActivity$a;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 16
    invoke-direct {p0}, Lco/uk/getmondo/signup/a;-><init>()V

    .line 19
    new-instance v0, Lco/uk/getmondo/signup/profile/AddressConfirmationActivity$b;

    invoke-direct {v0, p0}, Lco/uk/getmondo/signup/profile/AddressConfirmationActivity$b;-><init>(Lco/uk/getmondo/signup/profile/AddressConfirmationActivity;)V

    check-cast v0, Lkotlin/d/a/a;

    invoke-static {v0}, Lkotlin/d;->a(Lkotlin/d/a/a;)Lkotlin/c;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/signup/profile/AddressConfirmationActivity;->h:Lkotlin/c;

    return-void
.end method

.method private final f()Lco/uk/getmondo/api/model/Address;
    .locals 3

    iget-object v0, p0, Lco/uk/getmondo/signup/profile/AddressConfirmationActivity;->h:Lkotlin/c;

    sget-object v1, Lco/uk/getmondo/signup/profile/AddressConfirmationActivity;->a:[Lkotlin/reflect/l;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-interface {v0}, Lkotlin/c;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/api/model/Address;

    return-object v0
.end method


# virtual methods
.method public a(I)Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lco/uk/getmondo/signup/profile/AddressConfirmationActivity;->i:Ljava/util/HashMap;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lco/uk/getmondo/signup/profile/AddressConfirmationActivity;->i:Ljava/util/HashMap;

    :cond_0
    iget-object v0, p0, Lco/uk/getmondo/signup/profile/AddressConfirmationActivity;->i:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    if-nez v0, :cond_1

    invoke-virtual {p0, p1}, Landroid/support/v4/app/j;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/signup/profile/AddressConfirmationActivity;->i:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    return-object v0
.end method

.method public a(Z)V
    .locals 1

    .prologue
    .line 40
    sget v0, Lco/uk/getmondo/c$a;->signupAddressConfirmButton:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/profile/AddressConfirmationActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/common/ui/ProgressButton;

    invoke-virtual {v0, p1}, Lco/uk/getmondo/common/ui/ProgressButton;->setEnabled(Z)V

    .line 41
    return-void
.end method

.method public b(Z)V
    .locals 1

    .prologue
    .line 44
    sget v0, Lco/uk/getmondo/c$a;->signupAddressConfirmButton:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/profile/AddressConfirmationActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/common/ui/ProgressButton;

    invoke-virtual {v0, p1}, Lco/uk/getmondo/common/ui/ProgressButton;->setLoading(Z)V

    .line 45
    return-void
.end method

.method public c()Lio/reactivex/n;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .prologue
    .line 33
    sget v0, Lco/uk/getmondo/c$a;->signupAddressInputView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/profile/AddressConfirmationActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/profile/address/AddressInputView;

    invoke-virtual {v0}, Lco/uk/getmondo/profile/address/AddressInputView;->getAddressInputValid()Lio/reactivex/n;

    move-result-object v0

    return-object v0
.end method

.method public d()Lio/reactivex/n;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Lco/uk/getmondo/api/model/Address;",
            ">;"
        }
    .end annotation

    .prologue
    .line 36
    sget v0, Lco/uk/getmondo/c$a;->signupAddressConfirmButton:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/profile/AddressConfirmationActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/common/ui/ProgressButton;

    .line 63
    invoke-static {v0}, Lcom/b/a/c/c;->a(Landroid/view/View;)Lio/reactivex/n;

    move-result-object v1

    sget-object v0, Lcom/b/a/a/d;->a:Lcom/b/a/a/d;

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {v1, v0}, Lio/reactivex/n;->map(Lio/reactivex/c/h;)Lio/reactivex/n;

    move-result-object v1

    const-string v0, "RxView.clicks(this).map(VoidToUnit)"

    invoke-static {v1, v0}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 37
    new-instance v0, Lco/uk/getmondo/signup/profile/AddressConfirmationActivity$c;

    invoke-direct {v0, p0}, Lco/uk/getmondo/signup/profile/AddressConfirmationActivity$c;-><init>(Lco/uk/getmondo/signup/profile/AddressConfirmationActivity;)V

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {v1, v0}, Lio/reactivex/n;->map(Lio/reactivex/c/h;)Lio/reactivex/n;

    move-result-object v0

    const-string v1, "signupAddressConfirmButt\u2026ddressInputView.address }"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public e()V
    .locals 1

    .prologue
    .line 48
    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/profile/AddressConfirmationActivity;->setResult(I)V

    .line 49
    invoke-virtual {p0}, Lco/uk/getmondo/signup/profile/AddressConfirmationActivity;->finish()V

    .line 50
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 22
    invoke-super {p0, p1}, Lco/uk/getmondo/signup/a;->onCreate(Landroid/os/Bundle;)V

    .line 23
    const v0, 0x7f05001e

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/profile/AddressConfirmationActivity;->setContentView(I)V

    .line 25
    invoke-virtual {p0}, Lco/uk/getmondo/signup/profile/AddressConfirmationActivity;->l()Lco/uk/getmondo/common/h/b/b;

    move-result-object v0

    invoke-interface {v0, p0}, Lco/uk/getmondo/common/h/b/b;->a(Lco/uk/getmondo/signup/profile/AddressConfirmationActivity;)V

    .line 27
    sget v0, Lco/uk/getmondo/c$a;->signupAddressInputView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/profile/AddressConfirmationActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/profile/address/AddressInputView;

    invoke-direct {p0}, Lco/uk/getmondo/signup/profile/AddressConfirmationActivity;->f()Lco/uk/getmondo/api/model/Address;

    move-result-object v1

    invoke-virtual {v0, v1}, Lco/uk/getmondo/profile/address/AddressInputView;->setPreFilledAddress(Lco/uk/getmondo/api/model/Address;)V

    .line 29
    iget-object v0, p0, Lco/uk/getmondo/signup/profile/AddressConfirmationActivity;->b:Lco/uk/getmondo/signup/profile/b;

    if-nez v0, :cond_0

    const-string v1, "presenter"

    invoke-static {v1}, Lkotlin/d/b/l;->b(Ljava/lang/String;)V

    :cond_0
    check-cast p0, Lco/uk/getmondo/signup/profile/b$a;

    invoke-virtual {v0, p0}, Lco/uk/getmondo/signup/profile/b;->a(Lco/uk/getmondo/signup/profile/b$a;)V

    .line 30
    return-void
.end method
