.class final Lco/uk/getmondo/signup/profile/b$d;
.super Ljava/lang/Object;
.source "AddressConfirmationPresenter.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lco/uk/getmondo/signup/profile/b;->a(Lco/uk/getmondo/signup/profile/b$a;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h",
        "<TT;",
        "Lio/reactivex/l",
        "<+TR;>;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000\u0012\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u0008\u0012\u0004\u0012\u00020\u00020\u00012\u0006\u0010\u0003\u001a\u00020\u0004H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "Lio/reactivex/Maybe;",
        "",
        "it",
        "Lco/uk/getmondo/api/model/Address;",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field final synthetic a:Lco/uk/getmondo/signup/profile/b;

.field final synthetic b:Lco/uk/getmondo/signup/profile/b$a;


# direct methods
.method constructor <init>(Lco/uk/getmondo/signup/profile/b;Lco/uk/getmondo/signup/profile/b$a;)V
    .locals 0

    iput-object p1, p0, Lco/uk/getmondo/signup/profile/b$d;->a:Lco/uk/getmondo/signup/profile/b;

    iput-object p2, p0, Lco/uk/getmondo/signup/profile/b$d;->b:Lco/uk/getmondo/signup/profile/b$a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lco/uk/getmondo/api/model/Address;)Lio/reactivex/h;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lco/uk/getmondo/api/model/Address;",
            ")",
            "Lio/reactivex/h",
            "<",
            "Lkotlin/n;",
            ">;"
        }
    .end annotation

    .prologue
    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 40
    iget-object v0, p0, Lco/uk/getmondo/signup/profile/b$d;->a:Lco/uk/getmondo/signup/profile/b;

    invoke-static {v0}, Lco/uk/getmondo/signup/profile/b;->a(Lco/uk/getmondo/signup/profile/b;)Lco/uk/getmondo/signup/profile/q;

    move-result-object v0

    invoke-virtual {v0, p1}, Lco/uk/getmondo/signup/profile/q;->a(Lco/uk/getmondo/api/model/Address;)Lio/reactivex/b;

    move-result-object v0

    .line 41
    iget-object v1, p0, Lco/uk/getmondo/signup/profile/b$d;->a:Lco/uk/getmondo/signup/profile/b;

    invoke-static {v1}, Lco/uk/getmondo/signup/profile/b;->b(Lco/uk/getmondo/signup/profile/b;)Lio/reactivex/u;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/b;->b(Lio/reactivex/u;)Lio/reactivex/b;

    move-result-object v0

    .line 42
    iget-object v1, p0, Lco/uk/getmondo/signup/profile/b$d;->a:Lco/uk/getmondo/signup/profile/b;

    invoke-static {v1}, Lco/uk/getmondo/signup/profile/b;->c(Lco/uk/getmondo/signup/profile/b;)Lio/reactivex/u;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/b;->a(Lio/reactivex/u;)Lio/reactivex/b;

    move-result-object v1

    .line 43
    new-instance v0, Lco/uk/getmondo/signup/profile/b$d$1;

    invoke-direct {v0, p0}, Lco/uk/getmondo/signup/profile/b$d$1;-><init>(Lco/uk/getmondo/signup/profile/b$d;)V

    check-cast v0, Lio/reactivex/c/g;

    invoke-virtual {v1, v0}, Lio/reactivex/b;->c(Lio/reactivex/c/g;)Lio/reactivex/b;

    move-result-object v1

    .line 44
    new-instance v0, Lco/uk/getmondo/signup/profile/b$d$2;

    invoke-direct {v0, p0}, Lco/uk/getmondo/signup/profile/b$d$2;-><init>(Lco/uk/getmondo/signup/profile/b$d;)V

    check-cast v0, Lio/reactivex/c/g;

    invoke-virtual {v1, v0}, Lio/reactivex/b;->a(Lio/reactivex/c/g;)Lio/reactivex/b;

    move-result-object v0

    .line 45
    sget-object v1, Lkotlin/n;->a:Lkotlin/n;

    invoke-static {v0, v1}, Lco/uk/getmondo/common/j/f;->a(Lio/reactivex/b;Ljava/lang/Object;)Lio/reactivex/h;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 22
    check-cast p1, Lco/uk/getmondo/api/model/Address;

    invoke-virtual {p0, p1}, Lco/uk/getmondo/signup/profile/b$d;->a(Lco/uk/getmondo/api/model/Address;)Lio/reactivex/h;

    move-result-object v0

    return-object v0
.end method
