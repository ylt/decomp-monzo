.class final Lco/uk/getmondo/signup/profile/n$d;
.super Ljava/lang/Object;
.source "ProfileDetailsPresenter.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lco/uk/getmondo/signup/profile/n;->a(Lco/uk/getmondo/signup/profile/n$a;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h",
        "<TT;TR;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n\u00a2\u0006\u0002\u0008\u0004"
    }
    d2 = {
        "<anonymous>",
        "Lco/uk/getmondo/signup/profile/ProfileDetailsFormData;",
        "it",
        "Lco/uk/getmondo/api/model/signup/SignUpProfile;",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field final synthetic a:Lco/uk/getmondo/signup/profile/n;


# direct methods
.method constructor <init>(Lco/uk/getmondo/signup/profile/n;)V
    .locals 0

    iput-object p1, p0, Lco/uk/getmondo/signup/profile/n$d;->a:Lco/uk/getmondo/signup/profile/n;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lco/uk/getmondo/api/model/signup/SignUpProfile;)Lco/uk/getmondo/signup/profile/k;
    .locals 5

    .prologue
    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 49
    new-instance v2, Lco/uk/getmondo/signup/profile/k;

    .line 50
    invoke-virtual {p1}, Lco/uk/getmondo/api/model/signup/SignUpProfile;->a()Ljava/lang/String;

    move-result-object v3

    .line 51
    invoke-virtual {p1}, Lco/uk/getmondo/api/model/signup/SignUpProfile;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    .line 52
    :goto_0
    invoke-virtual {p1}, Lco/uk/getmondo/api/model/signup/SignUpProfile;->d()Lorg/threeten/bp/LocalDate;

    move-result-object v1

    if-eqz v1, :cond_1

    iget-object v4, p0, Lco/uk/getmondo/signup/profile/n$d;->a:Lco/uk/getmondo/signup/profile/n;

    invoke-static {v4}, Lco/uk/getmondo/signup/profile/n;->a(Lco/uk/getmondo/signup/profile/n;)Lorg/threeten/bp/format/DateTimeFormatter;

    move-result-object v4

    invoke-virtual {v1, v4}, Lorg/threeten/bp/LocalDate;->a(Lorg/threeten/bp/format/DateTimeFormatter;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 49
    :goto_1
    invoke-direct {v2, v3, v0, v1}, Lco/uk/getmondo/signup/profile/k;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 53
    return-object v2

    .line 51
    :cond_0
    invoke-virtual {p1}, Lco/uk/getmondo/api/model/signup/SignUpProfile;->b()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 52
    :cond_1
    const-string v1, ""

    goto :goto_1
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 25
    check-cast p1, Lco/uk/getmondo/api/model/signup/SignUpProfile;

    invoke-virtual {p0, p1}, Lco/uk/getmondo/signup/profile/n$d;->a(Lco/uk/getmondo/api/model/signup/SignUpProfile;)Lco/uk/getmondo/signup/profile/k;

    move-result-object v0

    return-object v0
.end method
