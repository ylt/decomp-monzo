.class final Lco/uk/getmondo/signup/profile/n$i;
.super Ljava/lang/Object;
.source "ProfileDetailsPresenter.kt"

# interfaces
.implements Lio/reactivex/c/q;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lco/uk/getmondo/signup/profile/n;->a(Lco/uk/getmondo/signup/profile/n$a;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/q",
        "<",
        "Lco/uk/getmondo/signup/profile/k;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n\u00a2\u0006\u0002\u0008\u0004"
    }
    d2 = {
        "<anonymous>",
        "",
        "it",
        "Lco/uk/getmondo/signup/profile/ProfileDetailsFormData;",
        "test"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field final synthetic a:Lco/uk/getmondo/signup/profile/n;


# direct methods
.method constructor <init>(Lco/uk/getmondo/signup/profile/n;)V
    .locals 0

    iput-object p1, p0, Lco/uk/getmondo/signup/profile/n$i;->a:Lco/uk/getmondo/signup/profile/n;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lco/uk/getmondo/signup/profile/k;)Z
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 88
    invoke-virtual {p1}, Lco/uk/getmondo/signup/profile/k;->a()Ljava/lang/String;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-static {v0}, Lkotlin/h/j;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    if-eqz v0, :cond_2

    invoke-virtual {p1}, Lco/uk/getmondo/signup/profile/k;->c()Ljava/lang/String;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-static {v0}, Lkotlin/h/j;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    if-eqz v0, :cond_2

    iget-object v0, p0, Lco/uk/getmondo/signup/profile/n$i;->a:Lco/uk/getmondo/signup/profile/n;

    invoke-virtual {p1}, Lco/uk/getmondo/signup/profile/k;->c()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lco/uk/getmondo/signup/profile/n;->a(Lco/uk/getmondo/signup/profile/n;Ljava/lang/String;)Lorg/threeten/bp/LocalDate;

    move-result-object v0

    const-wide/16 v4, 0x12

    invoke-static {v0, v4, v5}, Lco/uk/getmondo/common/c/c;->a(Lorg/threeten/bp/LocalDate;J)Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v1

    :goto_2
    return v0

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    move v0, v2

    goto :goto_1

    :cond_2
    move v0, v2

    goto :goto_2
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 25
    check-cast p1, Lco/uk/getmondo/signup/profile/k;

    invoke-virtual {p0, p1}, Lco/uk/getmondo/signup/profile/n$i;->a(Lco/uk/getmondo/signup/profile/k;)Z

    move-result v0

    return v0
.end method
