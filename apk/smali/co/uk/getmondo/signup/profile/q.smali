.class public final Lco/uk/getmondo/signup/profile/q;
.super Ljava/lang/Object;
.source "SignupProfileManager.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000>\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0018\u00002\u00020\u0001B\u000f\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u000c\u0010\u0005\u001a\u0008\u0012\u0004\u0012\u00020\u00070\u0006J$\u0010\u0008\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\n0\t0\u00062\u0008\u0008\u0002\u0010\u000b\u001a\u00020\u000c2\u0006\u0010\r\u001a\u00020\u000cJ\"\u0010\u000e\u001a\u00020\u000f2\u0006\u0010\u0010\u001a\u00020\u000c2\n\u0008\u0002\u0010\u0011\u001a\u0004\u0018\u00010\u000c2\u0006\u0010\u0012\u001a\u00020\u0013J\u000e\u0010\u0014\u001a\u00020\u000f2\u0006\u0010\u0015\u001a\u00020\nR\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0016"
    }
    d2 = {
        "Lco/uk/getmondo/signup/profile/SignupProfileManager;",
        "",
        "signupApi",
        "Lco/uk/getmondo/api/SignupApi;",
        "(Lco/uk/getmondo/api/SignupApi;)V",
        "existingProfile",
        "Lio/reactivex/Single;",
        "Lco/uk/getmondo/api/model/signup/SignUpProfile;",
        "searchAddress",
        "",
        "Lco/uk/getmondo/api/model/Address;",
        "countryCode",
        "",
        "postalCode",
        "submitProfile",
        "Lio/reactivex/Completable;",
        "legalName",
        "preferredName",
        "dateOfBirth",
        "Lorg/threeten/bp/LocalDate;",
        "submitProfileAddressAndCommit",
        "address",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field private final a:Lco/uk/getmondo/api/SignupApi;


# direct methods
.method public constructor <init>(Lco/uk/getmondo/api/SignupApi;)V
    .locals 1

    .prologue
    const-string v0, "signupApi"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lco/uk/getmondo/signup/profile/q;->a:Lco/uk/getmondo/api/SignupApi;

    return-void
.end method

.method public static bridge synthetic a(Lco/uk/getmondo/signup/profile/q;Ljava/lang/String;Ljava/lang/String;ILjava/lang/Object;)Lio/reactivex/v;
    .locals 1

    .prologue
    and-int/lit8 v0, p3, 0x1

    if-eqz v0, :cond_0

    .line 25
    const-string p1, "GBR"

    :cond_0
    invoke-virtual {p0, p1, p2}, Lco/uk/getmondo/signup/profile/q;->a(Ljava/lang/String;Ljava/lang/String;)Lio/reactivex/v;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(Lco/uk/getmondo/api/model/Address;)Lio/reactivex/b;
    .locals 6

    .prologue
    const/4 v2, 0x0

    const-string v0, "address"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 30
    iget-object v0, p0, Lco/uk/getmondo/signup/profile/q;->a:Lco/uk/getmondo/api/SignupApi;

    .line 31
    instance-of v1, p1, Lco/uk/getmondo/api/model/ApiAddress;

    if-nez v1, :cond_3

    move-object v1, v2

    :goto_0
    check-cast v1, Lco/uk/getmondo/api/model/ApiAddress;

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lco/uk/getmondo/api/model/ApiAddress;->f()Ljava/lang/String;

    move-result-object v1

    .line 32
    :goto_1
    invoke-interface {p1}, Lco/uk/getmondo/api/model/Address;->a()Ljava/util/List;

    move-result-object v2

    .line 33
    invoke-interface {p1}, Lco/uk/getmondo/api/model/Address;->b()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_1

    .line 34
    :goto_2
    invoke-interface {p1}, Lco/uk/getmondo/api/model/Address;->c()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_2

    .line 35
    :goto_3
    invoke-interface {p1}, Lco/uk/getmondo/api/model/Address;->d()Ljava/lang/String;

    move-result-object v5

    .line 30
    invoke-interface/range {v0 .. v5}, Lco/uk/getmondo/api/SignupApi;->submitProfileAddress(Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lio/reactivex/b;

    move-result-object v1

    .line 36
    iget-object v0, p0, Lco/uk/getmondo/signup/profile/q;->a:Lco/uk/getmondo/api/SignupApi;

    invoke-interface {v0}, Lco/uk/getmondo/api/SignupApi;->commitProfile()Lio/reactivex/b;

    move-result-object v0

    check-cast v0, Lio/reactivex/d;

    invoke-virtual {v1, v0}, Lio/reactivex/b;->b(Lio/reactivex/d;)Lio/reactivex/b;

    move-result-object v0

    const-string v1, "signupApi.submitProfileA\u2026ignupApi.commitProfile())"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0

    :cond_0
    move-object v1, v2

    .line 31
    goto :goto_1

    .line 33
    :cond_1
    const-string v3, ""

    goto :goto_2

    .line 34
    :cond_2
    const-string v4, ""

    goto :goto_3

    :cond_3
    move-object v1, p1

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Lorg/threeten/bp/LocalDate;)Lio/reactivex/b;
    .locals 1

    .prologue
    const-string v0, "legalName"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "dateOfBirth"

    invoke-static {p3, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 19
    iget-object v0, p0, Lco/uk/getmondo/signup/profile/q;->a:Lco/uk/getmondo/api/SignupApi;

    invoke-interface {v0, p1, p2, p3}, Lco/uk/getmondo/api/SignupApi;->submitProfile(Ljava/lang/String;Ljava/lang/String;Lorg/threeten/bp/LocalDate;)Lio/reactivex/b;

    move-result-object v0

    return-object v0
.end method

.method public final a()Lio/reactivex/v;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/v",
            "<",
            "Lco/uk/getmondo/api/model/signup/SignUpProfile;",
            ">;"
        }
    .end annotation

    .prologue
    .line 22
    iget-object v0, p0, Lco/uk/getmondo/signup/profile/q;->a:Lco/uk/getmondo/api/SignupApi;

    invoke-interface {v0}, Lco/uk/getmondo/api/SignupApi;->profile()Lio/reactivex/v;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)Lio/reactivex/v;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/v",
            "<",
            "Ljava/util/List",
            "<",
            "Lco/uk/getmondo/api/model/Address;",
            ">;>;"
        }
    .end annotation

    .prologue
    const-string v0, "countryCode"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "postalCode"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 27
    iget-object v0, p0, Lco/uk/getmondo/signup/profile/q;->a:Lco/uk/getmondo/api/SignupApi;

    invoke-interface {v0, p1, p2}, Lco/uk/getmondo/api/SignupApi;->searchAddress(Ljava/lang/String;Ljava/lang/String;)Lio/reactivex/v;

    move-result-object v1

    sget-object v0, Lco/uk/getmondo/signup/profile/q$a;->a:Lco/uk/getmondo/signup/profile/q$a;

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {v1, v0}, Lio/reactivex/v;->d(Lio/reactivex/c/h;)Lio/reactivex/v;

    move-result-object v0

    const-string v1, "signupApi.searchAddress(\u2026ode).map { it.addresses }"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method
