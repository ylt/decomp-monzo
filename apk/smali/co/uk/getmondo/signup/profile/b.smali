.class public final Lco/uk/getmondo/signup/profile/b;
.super Lco/uk/getmondo/common/ui/b;
.source "AddressConfirmationPresenter.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lco/uk/getmondo/signup/profile/b$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lco/uk/getmondo/common/ui/b",
        "<",
        "Lco/uk/getmondo/signup/profile/b$a;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u00008\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u0003\n\u0002\u0008\u0004\u0008\u0007\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00020\u0001:\u0001\u0013B3\u0008\u0007\u0012\u0008\u0008\u0001\u0010\u0003\u001a\u00020\u0004\u0012\u0008\u0008\u0001\u0010\u0005\u001a\u00020\u0004\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b\u00a2\u0006\u0002\u0010\u000cJ\u0018\u0010\r\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u00102\u0006\u0010\u0011\u001a\u00020\u0002H\u0002J\u0010\u0010\u0012\u001a\u00020\u000e2\u0006\u0010\u0011\u001a\u00020\u0002H\u0016R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0014"
    }
    d2 = {
        "Lco/uk/getmondo/signup/profile/AddressConfirmationPresenter;",
        "Lco/uk/getmondo/common/ui/BasePresenter;",
        "Lco/uk/getmondo/signup/profile/AddressConfirmationPresenter$View;",
        "ioScheduler",
        "Lio/reactivex/Scheduler;",
        "uiScheduler",
        "apiErrorHandler",
        "Lco/uk/getmondo/common/errors/ApiErrorHandler;",
        "signupProfileManager",
        "Lco/uk/getmondo/signup/profile/SignupProfileManager;",
        "analyticsService",
        "Lco/uk/getmondo/common/AnalyticsService;",
        "(Lio/reactivex/Scheduler;Lio/reactivex/Scheduler;Lco/uk/getmondo/common/errors/ApiErrorHandler;Lco/uk/getmondo/signup/profile/SignupProfileManager;Lco/uk/getmondo/common/AnalyticsService;)V",
        "handleError",
        "",
        "throwable",
        "",
        "view",
        "register",
        "View",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field private final c:Lio/reactivex/u;

.field private final d:Lio/reactivex/u;

.field private final e:Lco/uk/getmondo/common/e/a;

.field private final f:Lco/uk/getmondo/signup/profile/q;

.field private final g:Lco/uk/getmondo/common/a;


# direct methods
.method public constructor <init>(Lio/reactivex/u;Lio/reactivex/u;Lco/uk/getmondo/common/e/a;Lco/uk/getmondo/signup/profile/q;Lco/uk/getmondo/common/a;)V
    .locals 1

    .prologue
    const-string v0, "ioScheduler"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "uiScheduler"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "apiErrorHandler"

    invoke-static {p3, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "signupProfileManager"

    invoke-static {p4, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "analyticsService"

    invoke-static {p5, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 29
    invoke-direct {p0}, Lco/uk/getmondo/common/ui/b;-><init>()V

    iput-object p1, p0, Lco/uk/getmondo/signup/profile/b;->c:Lio/reactivex/u;

    iput-object p2, p0, Lco/uk/getmondo/signup/profile/b;->d:Lio/reactivex/u;

    iput-object p3, p0, Lco/uk/getmondo/signup/profile/b;->e:Lco/uk/getmondo/common/e/a;

    iput-object p4, p0, Lco/uk/getmondo/signup/profile/b;->f:Lco/uk/getmondo/signup/profile/q;

    iput-object p5, p0, Lco/uk/getmondo/signup/profile/b;->g:Lco/uk/getmondo/common/a;

    return-void
.end method

.method public static final synthetic a(Lco/uk/getmondo/signup/profile/b;)Lco/uk/getmondo/signup/profile/q;
    .locals 1

    .prologue
    .line 22
    iget-object v0, p0, Lco/uk/getmondo/signup/profile/b;->f:Lco/uk/getmondo/signup/profile/q;

    return-object v0
.end method

.method public static final synthetic a(Lco/uk/getmondo/signup/profile/b;Ljava/lang/Throwable;Lco/uk/getmondo/signup/profile/b$a;)V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0, p1, p2}, Lco/uk/getmondo/signup/profile/b;->a(Ljava/lang/Throwable;Lco/uk/getmondo/signup/profile/b$a;)V

    return-void
.end method

.method private final a(Ljava/lang/Throwable;Lco/uk/getmondo/signup/profile/b$a;)V
    .locals 2

    .prologue
    .line 51
    const/4 v0, 0x0

    invoke-interface {p2, v0}, Lco/uk/getmondo/signup/profile/b$a;->b(Z)V

    .line 53
    sget-object v1, Lco/uk/getmondo/signup/i;->a:Lco/uk/getmondo/signup/i;

    move-object v0, p2

    check-cast v0, Lco/uk/getmondo/signup/i$a;

    invoke-virtual {v1, p1, v0}, Lco/uk/getmondo/signup/i;->a(Ljava/lang/Throwable;Lco/uk/getmondo/signup/i$a;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 59
    :cond_0
    :goto_0
    return-void

    .line 56
    :cond_1
    iget-object v1, p0, Lco/uk/getmondo/signup/profile/b;->e:Lco/uk/getmondo/common/e/a;

    move-object v0, p2

    check-cast v0, Lco/uk/getmondo/common/e/a$a;

    invoke-virtual {v1, p1, v0}, Lco/uk/getmondo/common/e/a;->a(Ljava/lang/Throwable;Lco/uk/getmondo/common/e/a$a;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 57
    const v0, 0x7f0a0196

    invoke-interface {p2, v0}, Lco/uk/getmondo/signup/profile/b$a;->b(I)V

    goto :goto_0
.end method

.method public static final synthetic b(Lco/uk/getmondo/signup/profile/b;)Lio/reactivex/u;
    .locals 1

    .prologue
    .line 22
    iget-object v0, p0, Lco/uk/getmondo/signup/profile/b;->c:Lio/reactivex/u;

    return-object v0
.end method

.method public static final synthetic c(Lco/uk/getmondo/signup/profile/b;)Lio/reactivex/u;
    .locals 1

    .prologue
    .line 22
    iget-object v0, p0, Lco/uk/getmondo/signup/profile/b;->d:Lio/reactivex/u;

    return-object v0
.end method


# virtual methods
.method public bridge synthetic a(Lco/uk/getmondo/common/ui/f;)V
    .locals 0

    .prologue
    .line 22
    check-cast p1, Lco/uk/getmondo/signup/profile/b$a;

    invoke-virtual {p0, p1}, Lco/uk/getmondo/signup/profile/b;->a(Lco/uk/getmondo/signup/profile/b$a;)V

    return-void
.end method

.method public a(Lco/uk/getmondo/signup/profile/b$a;)V
    .locals 5

    .prologue
    const-string v0, "view"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v0, p1

    .line 32
    check-cast v0, Lco/uk/getmondo/common/ui/f;

    invoke-super {p0, v0}, Lco/uk/getmondo/common/ui/b;->a(Lco/uk/getmondo/common/ui/f;)V

    .line 33
    iget-object v0, p0, Lco/uk/getmondo/signup/profile/b;->g:Lco/uk/getmondo/common/a;

    sget-object v1, Lco/uk/getmondo/api/model/tracking/Impression;->Companion:Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    invoke-virtual {v1}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->z()Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v1

    invoke-virtual {v0, v1}, Lco/uk/getmondo/common/a;->a(Lco/uk/getmondo/api/model/tracking/Impression;)V

    .line 35
    iget-object v3, p0, Lco/uk/getmondo/signup/profile/b;->b:Lio/reactivex/b/a;

    .line 36
    invoke-interface {p1}, Lco/uk/getmondo/signup/profile/b$a;->c()Lio/reactivex/n;

    move-result-object v4

    new-instance v0, Lco/uk/getmondo/signup/profile/b$b;

    invoke-direct {v0, p1}, Lco/uk/getmondo/signup/profile/b$b;-><init>(Lco/uk/getmondo/signup/profile/b$a;)V

    check-cast v0, Lio/reactivex/c/g;

    sget-object v1, Lco/uk/getmondo/signup/profile/b$c;->a:Lco/uk/getmondo/signup/profile/b$c;

    check-cast v1, Lkotlin/d/a/b;

    if-eqz v1, :cond_0

    new-instance v2, Lco/uk/getmondo/signup/profile/c;

    invoke-direct {v2, v1}, Lco/uk/getmondo/signup/profile/c;-><init>(Lkotlin/d/a/b;)V

    move-object v1, v2

    :cond_0
    check-cast v1, Lio/reactivex/c/g;

    invoke-virtual {v4, v0, v1}, Lio/reactivex/n;->subscribe(Lio/reactivex/c/g;Lio/reactivex/c/g;)Lio/reactivex/b/b;

    move-result-object v0

    const-string v1, "view.addressInputValid\n \u2026Enabled(it) }, Timber::e)"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 35
    invoke-static {v3, v0}, Lco/uk/getmondo/common/j/f;->a(Lio/reactivex/b/a;Lio/reactivex/b/b;)Lio/reactivex/b/a;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/signup/profile/b;->b:Lio/reactivex/b/a;

    .line 38
    iget-object v1, p0, Lco/uk/getmondo/signup/profile/b;->b:Lio/reactivex/b/a;

    .line 47
    invoke-interface {p1}, Lco/uk/getmondo/signup/profile/b$a;->d()Lio/reactivex/n;

    move-result-object v2

    .line 39
    new-instance v0, Lco/uk/getmondo/signup/profile/b$d;

    invoke-direct {v0, p0, p1}, Lco/uk/getmondo/signup/profile/b$d;-><init>(Lco/uk/getmondo/signup/profile/b;Lco/uk/getmondo/signup/profile/b$a;)V

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {v2, v0}, Lio/reactivex/n;->flatMapMaybe(Lio/reactivex/c/h;)Lio/reactivex/n;

    move-result-object v2

    .line 47
    new-instance v0, Lco/uk/getmondo/signup/profile/b$e;

    invoke-direct {v0, p1}, Lco/uk/getmondo/signup/profile/b$e;-><init>(Lco/uk/getmondo/signup/profile/b$a;)V

    check-cast v0, Lio/reactivex/c/g;

    invoke-virtual {v2, v0}, Lio/reactivex/n;->subscribe(Lio/reactivex/c/g;)Lio/reactivex/b/b;

    move-result-object v0

    const-string v2, "view.confirmAddressClick\u2026ew.finishProfileStage() }"

    invoke-static {v0, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 38
    invoke-static {v1, v0}, Lco/uk/getmondo/common/j/f;->a(Lio/reactivex/b/a;Lio/reactivex/b/b;)Lio/reactivex/b/a;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/signup/profile/b;->b:Lio/reactivex/b/a;

    .line 48
    return-void
.end method
