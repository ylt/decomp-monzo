.class final Lco/uk/getmondo/signup/profile/n$l;
.super Ljava/lang/Object;
.source "ProfileDetailsPresenter.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lco/uk/getmondo/signup/profile/n;->a(Lco/uk/getmondo/signup/profile/n$a;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h",
        "<TT;TR;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\r\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n\u00a2\u0006\u0002\u0008\u0004"
    }
    d2 = {
        "<anonymous>",
        "",
        "dateText",
        "",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field final synthetic a:Lco/uk/getmondo/signup/profile/n;


# direct methods
.method constructor <init>(Lco/uk/getmondo/signup/profile/n;)V
    .locals 0

    iput-object p1, p0, Lco/uk/getmondo/signup/profile/n$l;->a:Lco/uk/getmondo/signup/profile/n;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 25
    check-cast p1, Ljava/lang/CharSequence;

    invoke-virtual {p0, p1}, Lco/uk/getmondo/signup/profile/n$l;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/CharSequence;)Z
    .locals 4

    .prologue
    const-string v0, "dateText"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 66
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v0

    iget-object v1, p0, Lco/uk/getmondo/signup/profile/n$l;->a:Lco/uk/getmondo/signup/profile/n;

    invoke-static {v1}, Lco/uk/getmondo/signup/profile/n;->c(Lco/uk/getmondo/signup/profile/n;)I

    move-result v1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lco/uk/getmondo/signup/profile/n$l;->a:Lco/uk/getmondo/signup/profile/n;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lco/uk/getmondo/signup/profile/n;->a(Lco/uk/getmondo/signup/profile/n;Ljava/lang/String;)Lorg/threeten/bp/LocalDate;

    move-result-object v0

    const-wide/16 v2, 0x12

    invoke-static {v0, v2, v3}, Lco/uk/getmondo/common/c/c;->a(Lorg/threeten/bp/LocalDate;J)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
