.class public final Lco/uk/getmondo/signup/profile/e$a;
.super Ljava/lang/Object;
.source "ProfileAddressFragment.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lco/uk/getmondo/signup/profile/e;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0008\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u000e\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006\u00a8\u0006\u0007"
    }
    d2 = {
        "Lco/uk/getmondo/signup/profile/ProfileAddressFragment$Companion;",
        "",
        "()V",
        "newInstance",
        "Lco/uk/getmondo/signup/profile/ProfileAddressFragment;",
        "signupEntryPoint",
        "Lco/uk/getmondo/signup/SignupEntryPoint;",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 125
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/d/b/i;)V
    .locals 0

    .prologue
    .line 125
    invoke-direct {p0}, Lco/uk/getmondo/signup/profile/e$a;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lco/uk/getmondo/signup/j;)Lco/uk/getmondo/signup/profile/e;
    .locals 3

    .prologue
    const-string v0, "signupEntryPoint"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 127
    new-instance v0, Lco/uk/getmondo/signup/profile/e;

    invoke-direct {v0}, Lco/uk/getmondo/signup/profile/e;-><init>()V

    .line 128
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 129
    const-string v2, "KEY_SIGNUP_ENTRY_POINT"

    check-cast p1, Ljava/io/Serializable;

    invoke-virtual {v1, v2, p1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 130
    nop

    .line 128
    invoke-virtual {v0, v1}, Lco/uk/getmondo/signup/profile/e;->setArguments(Landroid/os/Bundle;)V

    .line 131
    return-object v0
.end method
