.class public final Lco/uk/getmondo/signup/profile/n;
.super Lco/uk/getmondo/common/ui/b;
.source "ProfileDetailsPresenter.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lco/uk/getmondo/signup/profile/n$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lco/uk/getmondo/common/ui/b",
        "<",
        "Lco/uk/getmondo/signup/profile/n$a;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000T\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u0003\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0003\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00020\u0001:\u0001\u001fB3\u0008\u0007\u0012\u0008\u0008\u0001\u0010\u0003\u001a\u00020\u0004\u0012\u0008\u0008\u0001\u0010\u0005\u001a\u00020\u0004\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b\u00a2\u0006\u0002\u0010\u000cJ\u0018\u0010\u0015\u001a\u00020\u00162\u0006\u0010\u0017\u001a\u00020\u00182\u0006\u0010\u0019\u001a\u00020\u0002H\u0002J\u0012\u0010\u001a\u001a\u0004\u0018\u00010\u001b2\u0006\u0010\u001c\u001a\u00020\u001dH\u0002J\u0010\u0010\u001e\u001a\u00020\u00162\u0006\u0010\u0019\u001a\u00020\u0002H\u0016R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001b\u0010\r\u001a\u00020\u000e8BX\u0082\u0084\u0002\u00a2\u0006\u000c\n\u0004\u0008\u0011\u0010\u0012\u001a\u0004\u0008\u000f\u0010\u0010R\u000e\u0010\u0013\u001a\u00020\u0014X\u0082D\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006 "
    }
    d2 = {
        "Lco/uk/getmondo/signup/profile/ProfileDetailsPresenter;",
        "Lco/uk/getmondo/common/ui/BasePresenter;",
        "Lco/uk/getmondo/signup/profile/ProfileDetailsPresenter$View;",
        "uiScheduler",
        "Lio/reactivex/Scheduler;",
        "ioScheduler",
        "signupProfileManager",
        "Lco/uk/getmondo/signup/profile/SignupProfileManager;",
        "apiErrorHandler",
        "Lco/uk/getmondo/common/errors/ApiErrorHandler;",
        "analyticsService",
        "Lco/uk/getmondo/common/AnalyticsService;",
        "(Lio/reactivex/Scheduler;Lio/reactivex/Scheduler;Lco/uk/getmondo/signup/profile/SignupProfileManager;Lco/uk/getmondo/common/errors/ApiErrorHandler;Lco/uk/getmondo/common/AnalyticsService;)V",
        "dateFormatter",
        "Lorg/threeten/bp/format/DateTimeFormatter;",
        "getDateFormatter",
        "()Lorg/threeten/bp/format/DateTimeFormatter;",
        "dateFormatter$delegate",
        "Lkotlin/Lazy;",
        "dateLength",
        "",
        "handleError",
        "",
        "throwable",
        "",
        "view",
        "parseDate",
        "Lorg/threeten/bp/LocalDate;",
        "date",
        "",
        "register",
        "View",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# static fields
.field static final synthetic c:[Lkotlin/reflect/l;


# instance fields
.field private final d:Lkotlin/c;

.field private final e:I

.field private final f:Lio/reactivex/u;

.field private final g:Lio/reactivex/u;

.field private final h:Lco/uk/getmondo/signup/profile/q;

.field private final i:Lco/uk/getmondo/common/e/a;

.field private final j:Lco/uk/getmondo/common/a;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const/4 v0, 0x1

    new-array v1, v0, [Lkotlin/reflect/l;

    const/4 v2, 0x0

    new-instance v0, Lkotlin/d/b/w;

    const-class v3, Lco/uk/getmondo/signup/profile/n;

    invoke-static {v3}, Lkotlin/d/b/y;->a(Ljava/lang/Class;)Lkotlin/reflect/c;

    move-result-object v3

    const-string v4, "dateFormatter"

    const-string v5, "getDateFormatter()Lorg/threeten/bp/format/DateTimeFormatter;"

    invoke-direct {v0, v3, v4, v5}, Lkotlin/d/b/w;-><init>(Lkotlin/reflect/e;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v0}, Lkotlin/d/b/y;->a(Lkotlin/d/b/v;)Lkotlin/reflect/n;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/l;

    aput-object v0, v1, v2

    sput-object v1, Lco/uk/getmondo/signup/profile/n;->c:[Lkotlin/reflect/l;

    return-void
.end method

.method public constructor <init>(Lio/reactivex/u;Lio/reactivex/u;Lco/uk/getmondo/signup/profile/q;Lco/uk/getmondo/common/e/a;Lco/uk/getmondo/common/a;)V
    .locals 1

    .prologue
    const-string v0, "uiScheduler"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "ioScheduler"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "signupProfileManager"

    invoke-static {p3, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "apiErrorHandler"

    invoke-static {p4, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "analyticsService"

    invoke-static {p5, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 32
    invoke-direct {p0}, Lco/uk/getmondo/common/ui/b;-><init>()V

    iput-object p1, p0, Lco/uk/getmondo/signup/profile/n;->f:Lio/reactivex/u;

    iput-object p2, p0, Lco/uk/getmondo/signup/profile/n;->g:Lio/reactivex/u;

    iput-object p3, p0, Lco/uk/getmondo/signup/profile/n;->h:Lco/uk/getmondo/signup/profile/q;

    iput-object p4, p0, Lco/uk/getmondo/signup/profile/n;->i:Lco/uk/getmondo/common/e/a;

    iput-object p5, p0, Lco/uk/getmondo/signup/profile/n;->j:Lco/uk/getmondo/common/a;

    .line 34
    sget-object v0, Lco/uk/getmondo/signup/profile/n$b;->a:Lco/uk/getmondo/signup/profile/n$b;

    check-cast v0, Lkotlin/d/a/a;

    invoke-static {v0}, Lkotlin/d;->a(Lkotlin/d/a/a;)Lkotlin/c;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/signup/profile/n;->d:Lkotlin/c;

    .line 35
    const/16 v0, 0xa

    iput v0, p0, Lco/uk/getmondo/signup/profile/n;->e:I

    return-void
.end method

.method public static final synthetic a(Lco/uk/getmondo/signup/profile/n;Ljava/lang/String;)Lorg/threeten/bp/LocalDate;
    .locals 1

    .prologue
    .line 25
    invoke-direct {p0, p1}, Lco/uk/getmondo/signup/profile/n;->a(Ljava/lang/String;)Lorg/threeten/bp/LocalDate;

    move-result-object v0

    return-object v0
.end method

.method private final a(Ljava/lang/String;)Lorg/threeten/bp/LocalDate;
    .locals 1

    .prologue
    .line 127
    nop

    .line 128
    :try_start_0
    check-cast p1, Ljava/lang/CharSequence;

    invoke-direct {p0}, Lco/uk/getmondo/signup/profile/n;->a()Lorg/threeten/bp/format/DateTimeFormatter;

    move-result-object v0

    invoke-static {p1, v0}, Lorg/threeten/bp/LocalDate;->a(Ljava/lang/CharSequence;Lorg/threeten/bp/format/DateTimeFormatter;)Lorg/threeten/bp/LocalDate;
    :try_end_0
    .catch Lorg/threeten/bp/format/DateTimeParseException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 127
    :goto_0
    return-object v0

    .line 129
    :catch_0
    move-exception v0

    .line 130
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private final a()Lorg/threeten/bp/format/DateTimeFormatter;
    .locals 3

    iget-object v0, p0, Lco/uk/getmondo/signup/profile/n;->d:Lkotlin/c;

    sget-object v1, Lco/uk/getmondo/signup/profile/n;->c:[Lkotlin/reflect/l;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-interface {v0}, Lkotlin/c;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/threeten/bp/format/DateTimeFormatter;

    return-object v0
.end method

.method public static final synthetic a(Lco/uk/getmondo/signup/profile/n;)Lorg/threeten/bp/format/DateTimeFormatter;
    .locals 1

    .prologue
    .line 25
    invoke-direct {p0}, Lco/uk/getmondo/signup/profile/n;->a()Lorg/threeten/bp/format/DateTimeFormatter;

    move-result-object v0

    return-object v0
.end method

.method public static final synthetic a(Lco/uk/getmondo/signup/profile/n;Ljava/lang/Throwable;Lco/uk/getmondo/signup/profile/n$a;)V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0, p1, p2}, Lco/uk/getmondo/signup/profile/n;->a(Ljava/lang/Throwable;Lco/uk/getmondo/signup/profile/n$a;)V

    return-void
.end method

.method private final a(Ljava/lang/Throwable;Lco/uk/getmondo/signup/profile/n$a;)V
    .locals 2

    .prologue
    .line 104
    const/4 v0, 0x0

    invoke-interface {p2, v0}, Lco/uk/getmondo/signup/profile/n$a;->b(Z)V

    .line 106
    invoke-static {}, Lco/uk/getmondo/signup/profile/j;->values()[Lco/uk/getmondo/signup/profile/j;

    move-result-object v0

    check-cast v0, [Lco/uk/getmondo/common/e/f;

    invoke-static {p1, v0}, Lco/uk/getmondo/common/e/c;->a(Ljava/lang/Throwable;[Lco/uk/getmondo/common/e/f;)Lco/uk/getmondo/common/e/f;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/signup/profile/j;

    .line 107
    if-eqz v0, :cond_1

    .line 108
    sget-object v1, Lco/uk/getmondo/signup/profile/o;->a:[I

    invoke-virtual {v0}, Lco/uk/getmondo/signup/profile/j;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 124
    :cond_0
    :goto_0
    return-void

    .line 109
    :pswitch_0
    invoke-interface {p2}, Lco/uk/getmondo/signup/profile/n$a;->j()V

    goto :goto_0

    .line 110
    :pswitch_1
    invoke-interface {p2}, Lco/uk/getmondo/signup/profile/n$a;->k()V

    goto :goto_0

    .line 111
    :pswitch_2
    invoke-interface {p2}, Lco/uk/getmondo/signup/profile/n$a;->g()V

    goto :goto_0

    .line 116
    :cond_1
    sget-object v1, Lco/uk/getmondo/signup/i;->a:Lco/uk/getmondo/signup/i;

    move-object v0, p2

    check-cast v0, Lco/uk/getmondo/signup/i$a;

    invoke-virtual {v1, p1, v0}, Lco/uk/getmondo/signup/i;->a(Ljava/lang/Throwable;Lco/uk/getmondo/signup/i$a;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 119
    iget-object v1, p0, Lco/uk/getmondo/signup/profile/n;->i:Lco/uk/getmondo/common/e/a;

    move-object v0, p2

    check-cast v0, Lco/uk/getmondo/common/e/a$a;

    invoke-virtual {v1, p1, v0}, Lco/uk/getmondo/common/e/a;->a(Ljava/lang/Throwable;Lco/uk/getmondo/common/e/a$a;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 123
    const v0, 0x7f0a0196

    invoke-interface {p2, v0}, Lco/uk/getmondo/signup/profile/n$a;->b(I)V

    goto :goto_0

    .line 108
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static final synthetic b(Lco/uk/getmondo/signup/profile/n;)Lco/uk/getmondo/common/e/a;
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, Lco/uk/getmondo/signup/profile/n;->i:Lco/uk/getmondo/common/e/a;

    return-object v0
.end method

.method public static final synthetic c(Lco/uk/getmondo/signup/profile/n;)I
    .locals 1

    .prologue
    .line 25
    iget v0, p0, Lco/uk/getmondo/signup/profile/n;->e:I

    return v0
.end method

.method public static final synthetic d(Lco/uk/getmondo/signup/profile/n;)Lco/uk/getmondo/signup/profile/q;
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, Lco/uk/getmondo/signup/profile/n;->h:Lco/uk/getmondo/signup/profile/q;

    return-object v0
.end method

.method public static final synthetic e(Lco/uk/getmondo/signup/profile/n;)Lio/reactivex/u;
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, Lco/uk/getmondo/signup/profile/n;->g:Lio/reactivex/u;

    return-object v0
.end method

.method public static final synthetic f(Lco/uk/getmondo/signup/profile/n;)Lio/reactivex/u;
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, Lco/uk/getmondo/signup/profile/n;->f:Lio/reactivex/u;

    return-object v0
.end method


# virtual methods
.method public bridge synthetic a(Lco/uk/getmondo/common/ui/f;)V
    .locals 0

    .prologue
    .line 25
    check-cast p1, Lco/uk/getmondo/signup/profile/n$a;

    invoke-virtual {p0, p1}, Lco/uk/getmondo/signup/profile/n;->a(Lco/uk/getmondo/signup/profile/n$a;)V

    return-void
.end method

.method public a(Lco/uk/getmondo/signup/profile/n$a;)V
    .locals 4

    .prologue
    const-string v0, "view"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v0, p1

    .line 38
    check-cast v0, Lco/uk/getmondo/common/ui/f;

    invoke-super {p0, v0}, Lco/uk/getmondo/common/ui/b;->a(Lco/uk/getmondo/common/ui/f;)V

    .line 40
    iget-object v0, p0, Lco/uk/getmondo/signup/profile/n;->j:Lco/uk/getmondo/common/a;

    sget-object v1, Lco/uk/getmondo/api/model/tracking/Impression;->Companion:Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    invoke-virtual {v1}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->f()Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v1

    invoke-virtual {v0, v1}, Lco/uk/getmondo/common/a;->a(Lco/uk/getmondo/api/model/tracking/Impression;)V

    .line 42
    iget-object v1, p0, Lco/uk/getmondo/signup/profile/n;->b:Lio/reactivex/b/a;

    .line 43
    invoke-interface {p1}, Lco/uk/getmondo/signup/profile/n$a;->a()Lio/reactivex/n;

    move-result-object v2

    new-instance v0, Lco/uk/getmondo/signup/profile/n$c;

    invoke-direct {v0, p1}, Lco/uk/getmondo/signup/profile/n$c;-><init>(Lco/uk/getmondo/signup/profile/n$a;)V

    check-cast v0, Lio/reactivex/c/g;

    invoke-virtual {v2, v0}, Lio/reactivex/n;->subscribe(Lio/reactivex/c/g;)Lio/reactivex/b/b;

    move-result-object v0

    const-string v2, "view.onAddPreferredNameC\u2026howPreferredNameField() }"

    invoke-static {v0, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 42
    invoke-static {v1, v0}, Lco/uk/getmondo/common/j/f;->a(Lio/reactivex/b/a;Lio/reactivex/b/b;)Lio/reactivex/b/a;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/signup/profile/n;->b:Lio/reactivex/b/a;

    .line 45
    iget-object v2, p0, Lco/uk/getmondo/signup/profile/n;->b:Lio/reactivex/b/a;

    .line 55
    iget-object v0, p0, Lco/uk/getmondo/signup/profile/n;->h:Lco/uk/getmondo/signup/profile/q;

    invoke-virtual {v0}, Lco/uk/getmondo/signup/profile/q;->a()Lio/reactivex/v;

    move-result-object v0

    .line 46
    iget-object v1, p0, Lco/uk/getmondo/signup/profile/n;->g:Lio/reactivex/u;

    invoke-virtual {v0, v1}, Lio/reactivex/v;->b(Lio/reactivex/u;)Lio/reactivex/v;

    move-result-object v0

    .line 47
    iget-object v1, p0, Lco/uk/getmondo/signup/profile/n;->f:Lio/reactivex/u;

    invoke-virtual {v0, v1}, Lio/reactivex/v;->a(Lio/reactivex/u;)Lio/reactivex/v;

    move-result-object v1

    .line 48
    new-instance v0, Lco/uk/getmondo/signup/profile/n$d;

    invoke-direct {v0, p0}, Lco/uk/getmondo/signup/profile/n$d;-><init>(Lco/uk/getmondo/signup/profile/n;)V

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {v1, v0}, Lio/reactivex/v;->d(Lio/reactivex/c/h;)Lio/reactivex/v;

    move-result-object v3

    .line 56
    new-instance v0, Lco/uk/getmondo/signup/profile/n$e;

    invoke-direct {v0, p1}, Lco/uk/getmondo/signup/profile/n$e;-><init>(Lco/uk/getmondo/signup/profile/n$a;)V

    check-cast v0, Lio/reactivex/c/g;

    .line 57
    new-instance v1, Lco/uk/getmondo/signup/profile/n$f;

    invoke-direct {v1, p0, p1}, Lco/uk/getmondo/signup/profile/n$f;-><init>(Lco/uk/getmondo/signup/profile/n;Lco/uk/getmondo/signup/profile/n$a;)V

    check-cast v1, Lio/reactivex/c/g;

    .line 55
    invoke-virtual {v3, v0, v1}, Lio/reactivex/v;->a(Lio/reactivex/c/g;Lio/reactivex/c/g;)Lio/reactivex/b/b;

    move-result-object v0

    const-string v1, "signupProfileManager.exi\u2026      }\n                )"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 45
    invoke-static {v2, v0}, Lco/uk/getmondo/common/j/f;->a(Lio/reactivex/b/a;Lio/reactivex/b/b;)Lio/reactivex/b/a;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/signup/profile/n;->b:Lio/reactivex/b/a;

    .line 63
    invoke-interface {p1}, Lco/uk/getmondo/signup/profile/n$a;->d()Lio/reactivex/n;

    move-result-object v1

    sget-object v0, Lco/uk/getmondo/signup/profile/n$m;->a:Lco/uk/getmondo/signup/profile/n$m;

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {v1, v0}, Lio/reactivex/n;->map(Lio/reactivex/c/h;)Lio/reactivex/n;

    move-result-object v1

    .line 64
    invoke-interface {p1}, Lco/uk/getmondo/signup/profile/n$a;->c()Lio/reactivex/n;

    move-result-object v2

    .line 65
    new-instance v0, Lco/uk/getmondo/signup/profile/n$l;

    invoke-direct {v0, p0}, Lco/uk/getmondo/signup/profile/n$l;-><init>(Lco/uk/getmondo/signup/profile/n;)V

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {v2, v0}, Lio/reactivex/n;->map(Lio/reactivex/c/h;)Lio/reactivex/n;

    move-result-object v0

    .line 68
    iget-object v2, p0, Lco/uk/getmondo/signup/profile/n;->b:Lio/reactivex/b/a;

    .line 69
    const-string v3, "legalNameValid"

    invoke-static {v1, v3}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v3, "dateOfBirthValid"

    invoke-static {v0, v3}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v1, v0}, Lco/uk/getmondo/common/j/f;->a(Lio/reactivex/n;Lio/reactivex/n;)Lio/reactivex/n;

    move-result-object v1

    new-instance v0, Lco/uk/getmondo/signup/profile/n$g;

    invoke-direct {v0, p1}, Lco/uk/getmondo/signup/profile/n$g;-><init>(Lco/uk/getmondo/signup/profile/n$a;)V

    check-cast v0, Lio/reactivex/c/g;

    invoke-virtual {v1, v0}, Lio/reactivex/n;->subscribe(Lio/reactivex/c/g;)Lio/reactivex/b/b;

    move-result-object v0

    const-string v1, "combineLatest(legalNameV\u2026hValid)\n                }"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 68
    invoke-static {v2, v0}, Lco/uk/getmondo/common/j/f;->a(Lio/reactivex/b/a;Lio/reactivex/b/b;)Lio/reactivex/b/a;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/signup/profile/n;->b:Lio/reactivex/b/a;

    .line 73
    iget-object v1, p0, Lco/uk/getmondo/signup/profile/n;->b:Lio/reactivex/b/a;

    .line 74
    invoke-interface {p1}, Lco/uk/getmondo/signup/profile/n$a;->c()Lio/reactivex/n;

    move-result-object v2

    new-instance v0, Lco/uk/getmondo/signup/profile/n$h;

    invoke-direct {v0, p0, p1}, Lco/uk/getmondo/signup/profile/n$h;-><init>(Lco/uk/getmondo/signup/profile/n;Lco/uk/getmondo/signup/profile/n$a;)V

    check-cast v0, Lio/reactivex/c/g;

    invoke-virtual {v2, v0}, Lio/reactivex/n;->subscribe(Lio/reactivex/c/g;)Lio/reactivex/b/b;

    move-result-object v0

    const-string v2, "view.onDateOfBirthTextCh\u2026      }\n                }"

    invoke-static {v0, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 73
    invoke-static {v1, v0}, Lco/uk/getmondo/common/j/f;->a(Lio/reactivex/b/a;Lio/reactivex/b/b;)Lio/reactivex/b/a;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/signup/profile/n;->b:Lio/reactivex/b/a;

    .line 87
    iget-object v1, p0, Lco/uk/getmondo/signup/profile/n;->b:Lio/reactivex/b/a;

    .line 98
    invoke-interface {p1}, Lco/uk/getmondo/signup/profile/n$a;->e()Lio/reactivex/n;

    move-result-object v2

    .line 88
    new-instance v0, Lco/uk/getmondo/signup/profile/n$i;

    invoke-direct {v0, p0}, Lco/uk/getmondo/signup/profile/n$i;-><init>(Lco/uk/getmondo/signup/profile/n;)V

    check-cast v0, Lio/reactivex/c/q;

    invoke-virtual {v2, v0}, Lio/reactivex/n;->filter(Lio/reactivex/c/q;)Lio/reactivex/n;

    move-result-object v2

    .line 89
    new-instance v0, Lco/uk/getmondo/signup/profile/n$j;

    invoke-direct {v0, p0, p1}, Lco/uk/getmondo/signup/profile/n$j;-><init>(Lco/uk/getmondo/signup/profile/n;Lco/uk/getmondo/signup/profile/n$a;)V

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {v2, v0}, Lio/reactivex/n;->flatMapMaybe(Lio/reactivex/c/h;)Lio/reactivex/n;

    move-result-object v2

    .line 98
    new-instance v0, Lco/uk/getmondo/signup/profile/n$k;

    invoke-direct {v0, p1}, Lco/uk/getmondo/signup/profile/n$k;-><init>(Lco/uk/getmondo/signup/profile/n$a;)V

    check-cast v0, Lio/reactivex/c/g;

    invoke-virtual {v2, v0}, Lio/reactivex/n;->subscribe(Lio/reactivex/c/g;)Lio/reactivex/b/b;

    move-result-object v0

    const-string v2, "view.onNextClicked()\n   \u2026itted()\n                }"

    invoke-static {v0, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 87
    invoke-static {v1, v0}, Lco/uk/getmondo/common/j/f;->a(Lio/reactivex/b/a;Lio/reactivex/b/b;)Lio/reactivex/b/a;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/signup/profile/n;->b:Lio/reactivex/b/a;

    .line 101
    return-void
.end method
