.class public final Lco/uk/getmondo/signup/profile/e;
.super Lco/uk/getmondo/common/f/a;
.source "ProfileAddressFragment.kt"

# interfaces
.implements Lco/uk/getmondo/signup/profile/g$a;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lco/uk/getmondo/signup/profile/e$b;,
        Lco/uk/getmondo/signup/profile/e$a;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000|\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0010\u000e\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u000b\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0008\n\u0002\u0010\u000b\n\u0002\u0008\u0006\n\u0002\u0010 \n\u0002\u0008\u0005\u0018\u0000 C2\u00020\u00012\u00020\u0002:\u0002CDB\u0005\u00a2\u0006\u0002\u0010\u0003J\u0008\u0010\u001f\u001a\u00020\u0008H\u0016J\"\u0010 \u001a\u00020\u00082\u0006\u0010!\u001a\u00020\u00052\u0006\u0010\"\u001a\u00020\u00052\u0008\u0010#\u001a\u0004\u0018\u00010$H\u0016J\u0010\u0010%\u001a\u00020\u00082\u0006\u0010&\u001a\u00020\'H\u0016J\u0012\u0010(\u001a\u00020\u00082\u0008\u0010)\u001a\u0004\u0018\u00010*H\u0016J$\u0010+\u001a\u00020,2\u0006\u0010-\u001a\u00020.2\u0008\u0010/\u001a\u0004\u0018\u0001002\u0008\u0010)\u001a\u0004\u0018\u00010*H\u0016J\u0008\u00101\u001a\u00020\u0008H\u0016J\u001a\u00102\u001a\u00020\u00082\u0006\u00103\u001a\u00020,2\u0008\u0010)\u001a\u0004\u0018\u00010*H\u0016J\u0012\u00104\u001a\u00020\u00082\u0008\u00105\u001a\u0004\u0018\u00010\u000cH\u0016J\u0008\u00106\u001a\u00020\u0008H\u0016J\u0010\u00107\u001a\u00020\u00082\u0006\u00108\u001a\u000209H\u0016J\u0010\u0010:\u001a\u00020\u00082\u0006\u0010;\u001a\u000209H\u0016J\u0008\u0010<\u001a\u00020\u0008H\u0016J\u0008\u0010=\u001a\u00020\u0008H\u0016J\u0016\u0010>\u001a\u00020\u00082\u000c\u0010?\u001a\u0008\u0012\u0004\u0012\u00020\u000c0@H\u0016J\u0008\u0010A\u001a\u00020\u0008H\u0016J\u0008\u0010B\u001a\u00020\u0008H\u0016R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082D\u00a2\u0006\u0002\n\u0000R\u001a\u0010\u0006\u001a\u0008\u0012\u0004\u0012\u00020\u00080\u00078VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\t\u0010\nR\u001a\u0010\u000b\u001a\u0008\u0012\u0004\u0012\u00020\u000c0\u00078VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\r\u0010\nR\u001b\u0010\u000e\u001a\u00020\u000f8BX\u0082\u0084\u0002\u00a2\u0006\u000c\n\u0004\u0008\u0012\u0010\u0013\u001a\u0004\u0008\u0010\u0010\u0011R\u001a\u0010\u0014\u001a\u0008\u0012\u0004\u0012\u00020\u00150\u00078VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0016\u0010\nR\u001e\u0010\u0017\u001a\u00020\u00188\u0006@\u0006X\u0087.\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u0019\u0010\u001a\"\u0004\u0008\u001b\u0010\u001cR\u001a\u0010\u001d\u001a\u0008\u0012\u0004\u0012\u00020\u00080\u00078VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u001e\u0010\n\u00a8\u0006E"
    }
    d2 = {
        "Lco/uk/getmondo/signup/profile/ProfileAddressFragment;",
        "Lco/uk/getmondo/common/fragments/BaseFragment;",
        "Lco/uk/getmondo/signup/profile/ProfileAddressPresenter$View;",
        "()V",
        "REQUEST_CONFIRM_ADDRESS",
        "",
        "addressNotInListClicked",
        "Lio/reactivex/Observable;",
        "",
        "getAddressNotInListClicked",
        "()Lio/reactivex/Observable;",
        "addressSelected",
        "Lco/uk/getmondo/api/model/Address;",
        "getAddressSelected",
        "entryPoint",
        "Lco/uk/getmondo/signup/SignupEntryPoint;",
        "getEntryPoint",
        "()Lco/uk/getmondo/signup/SignupEntryPoint;",
        "entryPoint$delegate",
        "Lkotlin/Lazy;",
        "postcodeReady",
        "",
        "getPostcodeReady",
        "presenter",
        "Lco/uk/getmondo/signup/profile/ProfileAddressPresenter;",
        "getPresenter",
        "()Lco/uk/getmondo/signup/profile/ProfileAddressPresenter;",
        "setPresenter",
        "(Lco/uk/getmondo/signup/profile/ProfileAddressPresenter;)V",
        "residencyClicked",
        "getResidencyClicked",
        "hideContinue",
        "onActivityResult",
        "requestCode",
        "resultCode",
        "data",
        "Landroid/content/Intent;",
        "onAttach",
        "context",
        "Landroid/content/Context;",
        "onCreate",
        "savedInstanceState",
        "Landroid/os/Bundle;",
        "onCreateView",
        "Landroid/view/View;",
        "inflater",
        "Landroid/view/LayoutInflater;",
        "container",
        "Landroid/view/ViewGroup;",
        "onDestroyView",
        "onViewCreated",
        "view",
        "openAddressConfirmation",
        "address",
        "reloadSignupStatus",
        "setAddressLoading",
        "loading",
        "",
        "setContinueEnabled",
        "enabled",
        "showAddressNotFound",
        "showAddressNotInListButton",
        "showAddresses",
        "addresses",
        "",
        "showInvalidPostcodeError",
        "showUkOnly",
        "Companion",
        "StepListener",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# static fields
.field static final synthetic a:[Lkotlin/reflect/l;

.field public static final d:Lco/uk/getmondo/signup/profile/e$a;


# instance fields
.field public c:Lco/uk/getmondo/signup/profile/g;

.field private final e:Lkotlin/c;

.field private final f:I

.field private g:Ljava/util/HashMap;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const/4 v0, 0x1

    new-array v1, v0, [Lkotlin/reflect/l;

    const/4 v2, 0x0

    new-instance v0, Lkotlin/d/b/w;

    const-class v3, Lco/uk/getmondo/signup/profile/e;

    invoke-static {v3}, Lkotlin/d/b/y;->a(Ljava/lang/Class;)Lkotlin/reflect/c;

    move-result-object v3

    const-string v4, "entryPoint"

    const-string v5, "getEntryPoint()Lco/uk/getmondo/signup/SignupEntryPoint;"

    invoke-direct {v0, v3, v4, v5}, Lkotlin/d/b/w;-><init>(Lkotlin/reflect/e;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v0}, Lkotlin/d/b/y;->a(Lkotlin/d/b/v;)Lkotlin/reflect/n;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/l;

    aput-object v0, v1, v2

    sput-object v1, Lco/uk/getmondo/signup/profile/e;->a:[Lkotlin/reflect/l;

    new-instance v0, Lco/uk/getmondo/signup/profile/e$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lco/uk/getmondo/signup/profile/e$a;-><init>(Lkotlin/d/b/i;)V

    sput-object v0, Lco/uk/getmondo/signup/profile/e;->d:Lco/uk/getmondo/signup/profile/e$a;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 21
    invoke-direct {p0}, Lco/uk/getmondo/common/f/a;-><init>()V

    .line 22
    new-instance v0, Lco/uk/getmondo/signup/profile/e$c;

    invoke-direct {v0, p0}, Lco/uk/getmondo/signup/profile/e$c;-><init>(Lco/uk/getmondo/signup/profile/e;)V

    check-cast v0, Lkotlin/d/a/a;

    invoke-static {v0}, Lkotlin/d;->a(Lkotlin/d/a/a;)Lkotlin/c;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/signup/profile/e;->e:Lkotlin/c;

    .line 26
    const/4 v0, 0x1

    iput v0, p0, Lco/uk/getmondo/signup/profile/e;->f:I

    return-void
.end method

.method private final l()Lco/uk/getmondo/signup/j;
    .locals 3

    iget-object v0, p0, Lco/uk/getmondo/signup/profile/e;->e:Lkotlin/c;

    sget-object v1, Lco/uk/getmondo/signup/profile/e;->a:[Lkotlin/reflect/l;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-interface {v0}, Lkotlin/c;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/signup/j;

    return-object v0
.end method


# virtual methods
.method public a(I)Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lco/uk/getmondo/signup/profile/e;->g:Ljava/util/HashMap;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lco/uk/getmondo/signup/profile/e;->g:Ljava/util/HashMap;

    :cond_0
    iget-object v0, p0, Lco/uk/getmondo/signup/profile/e;->g:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    if-nez v0, :cond_1

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getView()Landroid/view/View;

    move-result-object v0

    if-nez v0, :cond_2

    const/4 v0, 0x0

    :cond_1
    :goto_0
    return-object v0

    :cond_2
    invoke-virtual {v0, p1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/signup/profile/e;->g:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method public a()Lio/reactivex/n;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Lkotlin/n;",
            ">;"
        }
    .end annotation

    .prologue
    .line 70
    sget v0, Lco/uk/getmondo/c$a;->signupAddressSelectionView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/profile/e;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/profile/address/AddressSelectionView;

    invoke-virtual {v0}, Lco/uk/getmondo/profile/address/AddressSelectionView;->g()Lio/reactivex/n;

    move-result-object v0

    return-object v0
.end method

.method public a(Lco/uk/getmondo/api/model/Address;)V
    .locals 3

    .prologue
    .line 118
    sget-object v1, Lco/uk/getmondo/signup/profile/AddressConfirmationActivity;->g:Lco/uk/getmondo/signup/profile/AddressConfirmationActivity$a;

    invoke-virtual {p0}, Lco/uk/getmondo/signup/profile/e;->getActivity()Landroid/support/v4/app/j;

    move-result-object v0

    const-string v2, "activity"

    invoke-static {v0, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/content/Context;

    invoke-direct {p0}, Lco/uk/getmondo/signup/profile/e;->l()Lco/uk/getmondo/signup/j;

    move-result-object v2

    invoke-virtual {v1, v0, p1, v2}, Lco/uk/getmondo/signup/profile/AddressConfirmationActivity$a;->a(Landroid/content/Context;Lco/uk/getmondo/api/model/Address;Lco/uk/getmondo/signup/j;)Landroid/content/Intent;

    move-result-object v0

    iget v1, p0, Lco/uk/getmondo/signup/profile/e;->f:I

    invoke-virtual {p0, v0, v1}, Lco/uk/getmondo/signup/profile/e;->startActivityForResult(Landroid/content/Intent;I)V

    .line 119
    return-void
.end method

.method public a(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<+",
            "Lco/uk/getmondo/api/model/Address;",
            ">;)V"
        }
    .end annotation

    .prologue
    const-string v0, "addresses"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 94
    sget v0, Lco/uk/getmondo/c$a;->signupAddressSelectionView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/profile/e;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/profile/address/AddressSelectionView;

    invoke-virtual {v0, p1}, Lco/uk/getmondo/profile/address/AddressSelectionView;->a(Ljava/util/List;)V

    .line 95
    return-void
.end method

.method public a(Z)V
    .locals 1

    .prologue
    .line 98
    sget v0, Lco/uk/getmondo/c$a;->signupAddressSelectionView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/profile/e;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/profile/address/AddressSelectionView;

    invoke-virtual {v0, p1}, Lco/uk/getmondo/profile/address/AddressSelectionView;->setAddressLoading(Z)V

    .line 99
    return-void
.end method

.method public b()V
    .locals 1

    .prologue
    .line 66
    invoke-virtual {p0}, Lco/uk/getmondo/signup/profile/e;->getActivity()Landroid/support/v4/app/j;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/signup/i$a;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lco/uk/getmondo/signup/i$a;->b()V

    .line 67
    :cond_0
    return-void
.end method

.method public c()Lio/reactivex/n;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 73
    sget v0, Lco/uk/getmondo/c$a;->signupAddressSelectionView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/profile/e;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/profile/address/AddressSelectionView;

    invoke-virtual {v0}, Lco/uk/getmondo/profile/address/AddressSelectionView;->i()Lio/reactivex/n;

    move-result-object v0

    return-object v0
.end method

.method public d()Lio/reactivex/n;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Lkotlin/n;",
            ">;"
        }
    .end annotation

    .prologue
    .line 76
    sget v0, Lco/uk/getmondo/c$a;->signupAddressSelectionView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/profile/e;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/profile/address/AddressSelectionView;

    invoke-virtual {v0}, Lco/uk/getmondo/profile/address/AddressSelectionView;->j()Lio/reactivex/n;

    move-result-object v0

    return-object v0
.end method

.method public e()Lio/reactivex/n;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Lco/uk/getmondo/api/model/Address;",
            ">;"
        }
    .end annotation

    .prologue
    .line 79
    sget v0, Lco/uk/getmondo/c$a;->signupAddressSelectionView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/profile/e;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/profile/address/AddressSelectionView;

    invoke-virtual {v0}, Lco/uk/getmondo/profile/address/AddressSelectionView;->k()Lio/reactivex/n;

    move-result-object v0

    return-object v0
.end method

.method public f()V
    .locals 3

    .prologue
    .line 86
    const v0, 0x7f0a0327

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/profile/e;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 88
    const v1, 0x7f0a0326

    invoke-virtual {p0, v1}, Lco/uk/getmondo/signup/profile/e;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 89
    const/4 v2, 0x0

    .line 86
    invoke-static {v0, v1, v2}, Lco/uk/getmondo/common/d/a;->a(Ljava/lang/String;Ljava/lang/String;Z)Lco/uk/getmondo/common/d/a;

    move-result-object v0

    .line 90
    invoke-virtual {p0}, Lco/uk/getmondo/signup/profile/e;->getFragmentManager()Landroid/support/v4/app/n;

    move-result-object v1

    const-class v2, Lco/uk/getmondo/common/d/a;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lco/uk/getmondo/common/d/a;->show(Landroid/support/v4/app/n;Ljava/lang/String;)V

    .line 91
    return-void
.end method

.method public g()V
    .locals 2

    .prologue
    .line 102
    sget v0, Lco/uk/getmondo/c$a;->signupAddressSelectionView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/profile/e;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/profile/address/AddressSelectionView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lco/uk/getmondo/profile/address/AddressSelectionView;->setPrimaryButtonVisible(Z)V

    .line 103
    return-void
.end method

.method public h()V
    .locals 1

    .prologue
    .line 106
    sget v0, Lco/uk/getmondo/c$a;->signupAddressSelectionView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/profile/e;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/profile/address/AddressSelectionView;

    invoke-virtual {v0}, Lco/uk/getmondo/profile/address/AddressSelectionView;->c()V

    .line 107
    return-void
.end method

.method public i()V
    .locals 1

    .prologue
    .line 110
    sget v0, Lco/uk/getmondo/c$a;->signupAddressSelectionView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/profile/e;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/profile/address/AddressSelectionView;

    invoke-virtual {v0}, Lco/uk/getmondo/profile/address/AddressSelectionView;->l()V

    .line 111
    return-void
.end method

.method public j()V
    .locals 1

    .prologue
    .line 114
    sget v0, Lco/uk/getmondo/c$a;->signupAddressSelectionView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/profile/e;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/profile/address/AddressSelectionView;

    invoke-virtual {v0}, Lco/uk/getmondo/profile/address/AddressSelectionView;->m()V

    .line 115
    return-void
.end method

.method public k()V
    .locals 1

    iget-object v0, p0, Lco/uk/getmondo/signup/profile/e;->g:Ljava/util/HashMap;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lco/uk/getmondo/signup/profile/e;->g:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    :cond_0
    return-void
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 2

    .prologue
    .line 55
    .line 56
    iget v0, p0, Lco/uk/getmondo/signup/profile/e;->f:I

    if-ne p1, v0, :cond_2

    .line 57
    const/4 v0, -0x1

    if-ne p2, v0, :cond_1

    .line 58
    invoke-virtual {p0}, Lco/uk/getmondo/signup/profile/e;->getActivity()Landroid/support/v4/app/j;

    move-result-object v0

    if-nez v0, :cond_0

    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type co.uk.getmondo.signup.profile.ProfileAddressFragment.StepListener"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    check-cast v0, Lco/uk/getmondo/signup/profile/e$b;

    invoke-interface {v0}, Lco/uk/getmondo/signup/profile/e$b;->f_()V

    .line 62
    :cond_1
    :goto_0
    return-void

    .line 61
    :cond_2
    invoke-super {p0, p1, p2, p3}, Lco/uk/getmondo/common/f/a;->onActivityResult(IILandroid/content/Intent;)V

    goto :goto_0
.end method

.method public onAttach(Landroid/content/Context;)V
    .locals 2

    .prologue
    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 42
    invoke-super {p0, p1}, Lco/uk/getmondo/common/f/a;->onAttach(Landroid/content/Context;)V

    .line 43
    instance-of v0, p1, Lco/uk/getmondo/signup/profile/e$b;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Activity must implement "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-class v1, Lco/uk/getmondo/signup/profile/e$b;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    .line 44
    :cond_0
    instance-of v0, p1, Lco/uk/getmondo/signup/a;

    if-nez v0, :cond_1

    .line 45
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Activity must extend "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-class v1, Lco/uk/getmondo/signup/a;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 44
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    .line 47
    :cond_1
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 29
    invoke-super {p0, p1}, Lco/uk/getmondo/common/f/a;->onCreate(Landroid/os/Bundle;)V

    .line 30
    invoke-virtual {p0}, Lco/uk/getmondo/signup/profile/e;->B()Lco/uk/getmondo/common/h/b/b;

    move-result-object v0

    invoke-interface {v0, p0}, Lco/uk/getmondo/common/h/b/b;->a(Lco/uk/getmondo/signup/profile/e;)V

    .line 31
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2

    .prologue
    const-string v0, "inflater"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 34
    const v0, 0x7f0500a3

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    const-string v1, "inflater.inflate(R.layou\u2026ddress, container, false)"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public onDestroyView()V
    .locals 2

    .prologue
    .line 50
    iget-object v0, p0, Lco/uk/getmondo/signup/profile/e;->c:Lco/uk/getmondo/signup/profile/g;

    if-nez v0, :cond_0

    const-string v1, "presenter"

    invoke-static {v1}, Lkotlin/d/b/l;->b(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0}, Lco/uk/getmondo/signup/profile/g;->b()V

    .line 51
    invoke-super {p0}, Lco/uk/getmondo/common/f/a;->onDestroyView()V

    .line 52
    invoke-virtual {p0}, Lco/uk/getmondo/signup/profile/e;->k()V

    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 2

    .prologue
    const-string v0, "view"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 37
    invoke-super {p0, p1, p2}, Lco/uk/getmondo/common/f/a;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 38
    iget-object v0, p0, Lco/uk/getmondo/signup/profile/e;->c:Lco/uk/getmondo/signup/profile/g;

    if-nez v0, :cond_0

    const-string v1, "presenter"

    invoke-static {v1}, Lkotlin/d/b/l;->b(Ljava/lang/String;)V

    :cond_0
    check-cast p0, Lco/uk/getmondo/signup/profile/g$a;

    invoke-virtual {v0, p0}, Lco/uk/getmondo/signup/profile/g;->a(Lco/uk/getmondo/signup/profile/g$a;)V

    .line 39
    return-void
.end method
