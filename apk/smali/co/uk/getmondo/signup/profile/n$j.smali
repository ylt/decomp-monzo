.class final Lco/uk/getmondo/signup/profile/n$j;
.super Ljava/lang/Object;
.source "ProfileDetailsPresenter.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lco/uk/getmondo/signup/profile/n;->a(Lco/uk/getmondo/signup/profile/n$a;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h",
        "<TT;",
        "Lio/reactivex/l",
        "<+TR;>;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000\u0012\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u0008\u0012\u0004\u0012\u00020\u00020\u00012\u0006\u0010\u0003\u001a\u00020\u0004H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "Lio/reactivex/Maybe;",
        "",
        "it",
        "Lco/uk/getmondo/signup/profile/ProfileDetailsFormData;",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field final synthetic a:Lco/uk/getmondo/signup/profile/n;

.field final synthetic b:Lco/uk/getmondo/signup/profile/n$a;


# direct methods
.method constructor <init>(Lco/uk/getmondo/signup/profile/n;Lco/uk/getmondo/signup/profile/n$a;)V
    .locals 0

    iput-object p1, p0, Lco/uk/getmondo/signup/profile/n$j;->a:Lco/uk/getmondo/signup/profile/n;

    iput-object p2, p0, Lco/uk/getmondo/signup/profile/n$j;->b:Lco/uk/getmondo/signup/profile/n$a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lco/uk/getmondo/signup/profile/k;)Lio/reactivex/h;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lco/uk/getmondo/signup/profile/k;",
            ")",
            "Lio/reactivex/h",
            "<",
            "Lkotlin/n;",
            ">;"
        }
    .end annotation

    .prologue
    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 90
    iget-object v0, p0, Lco/uk/getmondo/signup/profile/n$j;->a:Lco/uk/getmondo/signup/profile/n;

    invoke-virtual {p1}, Lco/uk/getmondo/signup/profile/k;->c()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lco/uk/getmondo/signup/profile/n;->a(Lco/uk/getmondo/signup/profile/n;Ljava/lang/String;)Lorg/threeten/bp/LocalDate;

    move-result-object v3

    if-nez v3, :cond_0

    invoke-static {}, Lkotlin/d/b/l;->a()V

    .line 91
    :cond_0
    iget-object v0, p0, Lco/uk/getmondo/signup/profile/n$j;->a:Lco/uk/getmondo/signup/profile/n;

    invoke-static {v0}, Lco/uk/getmondo/signup/profile/n;->d(Lco/uk/getmondo/signup/profile/n;)Lco/uk/getmondo/signup/profile/q;

    move-result-object v2

    invoke-virtual {p1}, Lco/uk/getmondo/signup/profile/k;->a()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type kotlin.CharSequence"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    check-cast v0, Ljava/lang/CharSequence;

    invoke-static {v0}, Lkotlin/h/j;->b(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lco/uk/getmondo/signup/profile/k;->b()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_3

    if-nez v0, :cond_2

    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type kotlin.CharSequence"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    check-cast v0, Ljava/lang/CharSequence;

    invoke-static {v0}, Lkotlin/h/j;->b(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v2, v1, v0, v3}, Lco/uk/getmondo/signup/profile/q;->a(Ljava/lang/String;Ljava/lang/String;Lorg/threeten/bp/LocalDate;)Lio/reactivex/b;

    move-result-object v0

    .line 92
    iget-object v1, p0, Lco/uk/getmondo/signup/profile/n$j;->a:Lco/uk/getmondo/signup/profile/n;

    invoke-static {v1}, Lco/uk/getmondo/signup/profile/n;->e(Lco/uk/getmondo/signup/profile/n;)Lio/reactivex/u;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/b;->b(Lio/reactivex/u;)Lio/reactivex/b;

    move-result-object v0

    .line 93
    iget-object v1, p0, Lco/uk/getmondo/signup/profile/n$j;->a:Lco/uk/getmondo/signup/profile/n;

    invoke-static {v1}, Lco/uk/getmondo/signup/profile/n;->f(Lco/uk/getmondo/signup/profile/n;)Lio/reactivex/u;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/b;->a(Lio/reactivex/u;)Lio/reactivex/b;

    move-result-object v1

    .line 94
    new-instance v0, Lco/uk/getmondo/signup/profile/n$j$1;

    invoke-direct {v0, p0}, Lco/uk/getmondo/signup/profile/n$j$1;-><init>(Lco/uk/getmondo/signup/profile/n$j;)V

    check-cast v0, Lio/reactivex/c/g;

    invoke-virtual {v1, v0}, Lio/reactivex/b;->c(Lio/reactivex/c/g;)Lio/reactivex/b;

    move-result-object v1

    .line 95
    new-instance v0, Lco/uk/getmondo/signup/profile/n$j$2;

    invoke-direct {v0, p0}, Lco/uk/getmondo/signup/profile/n$j$2;-><init>(Lco/uk/getmondo/signup/profile/n$j;)V

    check-cast v0, Lio/reactivex/c/g;

    invoke-virtual {v1, v0}, Lio/reactivex/b;->a(Lio/reactivex/c/g;)Lio/reactivex/b;

    move-result-object v0

    .line 96
    sget-object v1, Lkotlin/n;->a:Lkotlin/n;

    invoke-static {v0, v1}, Lco/uk/getmondo/common/j/f;->a(Lio/reactivex/b;Ljava/lang/Object;)Lio/reactivex/h;

    move-result-object v0

    return-object v0

    .line 91
    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 25
    check-cast p1, Lco/uk/getmondo/signup/profile/k;

    invoke-virtual {p0, p1}, Lco/uk/getmondo/signup/profile/n$j;->a(Lco/uk/getmondo/signup/profile/k;)Lio/reactivex/h;

    move-result-object v0

    return-object v0
.end method
