.class final Lco/uk/getmondo/signup/profile/AddressConfirmationActivity$c;
.super Ljava/lang/Object;
.source "AddressConfirmationActivity.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lco/uk/getmondo/signup/profile/AddressConfirmationActivity;->d()Lio/reactivex/n;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h",
        "<TT;TR;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n\u00a2\u0006\u0004\u0008\u0004\u0010\u0005"
    }
    d2 = {
        "<anonymous>",
        "Lco/uk/getmondo/api/model/Address;",
        "it",
        "",
        "apply",
        "(Lkotlin/Unit;)Lco/uk/getmondo/api/model/Address;"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field final synthetic a:Lco/uk/getmondo/signup/profile/AddressConfirmationActivity;


# direct methods
.method constructor <init>(Lco/uk/getmondo/signup/profile/AddressConfirmationActivity;)V
    .locals 0

    iput-object p1, p0, Lco/uk/getmondo/signup/profile/AddressConfirmationActivity$c;->a:Lco/uk/getmondo/signup/profile/AddressConfirmationActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lkotlin/n;)Lco/uk/getmondo/api/model/Address;
    .locals 2

    .prologue
    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 37
    iget-object v0, p0, Lco/uk/getmondo/signup/profile/AddressConfirmationActivity$c;->a:Lco/uk/getmondo/signup/profile/AddressConfirmationActivity;

    sget v1, Lco/uk/getmondo/c$a;->signupAddressInputView:I

    invoke-virtual {v0, v1}, Lco/uk/getmondo/signup/profile/AddressConfirmationActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/profile/address/AddressInputView;

    invoke-virtual {v0}, Lco/uk/getmondo/profile/address/AddressInputView;->getAddress()Lco/uk/getmondo/api/model/Address;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 16
    check-cast p1, Lkotlin/n;

    invoke-virtual {p0, p1}, Lco/uk/getmondo/signup/profile/AddressConfirmationActivity$c;->a(Lkotlin/n;)Lco/uk/getmondo/api/model/Address;

    move-result-object v0

    return-object v0
.end method
