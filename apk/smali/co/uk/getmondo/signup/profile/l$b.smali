.class final Lco/uk/getmondo/signup/profile/l$b;
.super Ljava/lang/Object;
.source "ProfileDetailsFragment.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lco/uk/getmondo/signup/profile/l;->e()Lio/reactivex/n;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h",
        "<TT;TR;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n\u00a2\u0006\u0002\u0008\u0004"
    }
    d2 = {
        "<anonymous>",
        "Lco/uk/getmondo/signup/profile/ProfileDetailsFormData;",
        "it",
        "",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field final synthetic a:Lco/uk/getmondo/signup/profile/l;


# direct methods
.method constructor <init>(Lco/uk/getmondo/signup/profile/l;)V
    .locals 0

    iput-object p1, p0, Lco/uk/getmondo/signup/profile/l$b;->a:Lco/uk/getmondo/signup/profile/l;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 22
    invoke-virtual {p0, p1}, Lco/uk/getmondo/signup/profile/l$b;->b(Ljava/lang/Object;)Lco/uk/getmondo/signup/profile/k;

    move-result-object v0

    return-object v0
.end method

.method public final b(Ljava/lang/Object;)Lco/uk/getmondo/signup/profile/k;
    .locals 5

    .prologue
    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 63
    new-instance v1, Lco/uk/getmondo/signup/profile/k;

    .line 64
    iget-object v0, p0, Lco/uk/getmondo/signup/profile/l$b;->a:Lco/uk/getmondo/signup/profile/l;

    sget v2, Lco/uk/getmondo/c$a;->legalNameEditText:I

    invoke-virtual {v0, v2}, Lco/uk/getmondo/signup/profile/l;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    .line 65
    iget-object v0, p0, Lco/uk/getmondo/signup/profile/l$b;->a:Lco/uk/getmondo/signup/profile/l;

    sget v3, Lco/uk/getmondo/c$a;->preferredNameEditText:I

    invoke-virtual {v0, v3}, Lco/uk/getmondo/signup/profile/l;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    .line 66
    iget-object v0, p0, Lco/uk/getmondo/signup/profile/l$b;->a:Lco/uk/getmondo/signup/profile/l;

    sget v4, Lco/uk/getmondo/c$a;->dateOfBirthEditText:I

    invoke-virtual {v0, v4}, Lco/uk/getmondo/signup/profile/l;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 63
    invoke-direct {v1, v2, v3, v0}, Lco/uk/getmondo/signup/profile/k;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 67
    return-object v1
.end method
