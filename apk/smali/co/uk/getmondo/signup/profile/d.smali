.class public final Lco/uk/getmondo/signup/profile/d;
.super Ljava/lang/Object;
.source "AddressConfirmationPresenter_Factory.java"

# interfaces
.implements Lb/a/b;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lb/a/b",
        "<",
        "Lco/uk/getmondo/signup/profile/b;",
        ">;"
    }
.end annotation


# static fields
.field static final synthetic a:Z


# instance fields
.field private final b:Lb/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lb/a",
            "<",
            "Lco/uk/getmondo/signup/profile/b;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Ljavax/a/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/a/a",
            "<",
            "Lio/reactivex/u;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Ljavax/a/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/a/a",
            "<",
            "Lio/reactivex/u;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Ljavax/a/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/common/e/a;",
            ">;"
        }
    .end annotation
.end field

.field private final f:Ljavax/a/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/signup/profile/q;",
            ">;"
        }
    .end annotation
.end field

.field private final g:Ljavax/a/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/common/a;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 12
    const-class v0, Lco/uk/getmondo/signup/profile/d;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lco/uk/getmondo/signup/profile/d;->a:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lb/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lb/a",
            "<",
            "Lco/uk/getmondo/signup/profile/b;",
            ">;",
            "Ljavax/a/a",
            "<",
            "Lio/reactivex/u;",
            ">;",
            "Ljavax/a/a",
            "<",
            "Lio/reactivex/u;",
            ">;",
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/common/e/a;",
            ">;",
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/signup/profile/q;",
            ">;",
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/common/a;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    sget-boolean v0, Lco/uk/getmondo/signup/profile/d;->a:Z

    if-nez v0, :cond_0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 39
    :cond_0
    iput-object p1, p0, Lco/uk/getmondo/signup/profile/d;->b:Lb/a;

    .line 40
    sget-boolean v0, Lco/uk/getmondo/signup/profile/d;->a:Z

    if-nez v0, :cond_1

    if-nez p2, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 41
    :cond_1
    iput-object p2, p0, Lco/uk/getmondo/signup/profile/d;->c:Ljavax/a/a;

    .line 42
    sget-boolean v0, Lco/uk/getmondo/signup/profile/d;->a:Z

    if-nez v0, :cond_2

    if-nez p3, :cond_2

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 43
    :cond_2
    iput-object p3, p0, Lco/uk/getmondo/signup/profile/d;->d:Ljavax/a/a;

    .line 44
    sget-boolean v0, Lco/uk/getmondo/signup/profile/d;->a:Z

    if-nez v0, :cond_3

    if-nez p4, :cond_3

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 45
    :cond_3
    iput-object p4, p0, Lco/uk/getmondo/signup/profile/d;->e:Ljavax/a/a;

    .line 46
    sget-boolean v0, Lco/uk/getmondo/signup/profile/d;->a:Z

    if-nez v0, :cond_4

    if-nez p5, :cond_4

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 47
    :cond_4
    iput-object p5, p0, Lco/uk/getmondo/signup/profile/d;->f:Ljavax/a/a;

    .line 48
    sget-boolean v0, Lco/uk/getmondo/signup/profile/d;->a:Z

    if-nez v0, :cond_5

    if-nez p6, :cond_5

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 49
    :cond_5
    iput-object p6, p0, Lco/uk/getmondo/signup/profile/d;->g:Ljavax/a/a;

    .line 50
    return-void
.end method

.method public static a(Lb/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;)Lb/a/b;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lb/a",
            "<",
            "Lco/uk/getmondo/signup/profile/b;",
            ">;",
            "Ljavax/a/a",
            "<",
            "Lio/reactivex/u;",
            ">;",
            "Ljavax/a/a",
            "<",
            "Lio/reactivex/u;",
            ">;",
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/common/e/a;",
            ">;",
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/signup/profile/q;",
            ">;",
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/common/a;",
            ">;)",
            "Lb/a/b",
            "<",
            "Lco/uk/getmondo/signup/profile/b;",
            ">;"
        }
    .end annotation

    .prologue
    .line 71
    new-instance v0, Lco/uk/getmondo/signup/profile/d;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lco/uk/getmondo/signup/profile/d;-><init>(Lb/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;)V

    return-object v0
.end method


# virtual methods
.method public a()Lco/uk/getmondo/signup/profile/b;
    .locals 7

    .prologue
    .line 54
    iget-object v6, p0, Lco/uk/getmondo/signup/profile/d;->b:Lb/a;

    new-instance v0, Lco/uk/getmondo/signup/profile/b;

    iget-object v1, p0, Lco/uk/getmondo/signup/profile/d;->c:Ljavax/a/a;

    .line 57
    invoke-interface {v1}, Ljavax/a/a;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lio/reactivex/u;

    iget-object v2, p0, Lco/uk/getmondo/signup/profile/d;->d:Ljavax/a/a;

    .line 58
    invoke-interface {v2}, Ljavax/a/a;->b()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lio/reactivex/u;

    iget-object v3, p0, Lco/uk/getmondo/signup/profile/d;->e:Ljavax/a/a;

    .line 59
    invoke-interface {v3}, Ljavax/a/a;->b()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lco/uk/getmondo/common/e/a;

    iget-object v4, p0, Lco/uk/getmondo/signup/profile/d;->f:Ljavax/a/a;

    .line 60
    invoke-interface {v4}, Ljavax/a/a;->b()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lco/uk/getmondo/signup/profile/q;

    iget-object v5, p0, Lco/uk/getmondo/signup/profile/d;->g:Ljavax/a/a;

    .line 61
    invoke-interface {v5}, Ljavax/a/a;->b()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lco/uk/getmondo/common/a;

    invoke-direct/range {v0 .. v5}, Lco/uk/getmondo/signup/profile/b;-><init>(Lio/reactivex/u;Lio/reactivex/u;Lco/uk/getmondo/common/e/a;Lco/uk/getmondo/signup/profile/q;Lco/uk/getmondo/common/a;)V

    .line 54
    invoke-static {v6, v0}, Lb/a/c;->a(Lb/a;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/signup/profile/b;

    return-object v0
.end method

.method public synthetic b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 12
    invoke-virtual {p0}, Lco/uk/getmondo/signup/profile/d;->a()Lco/uk/getmondo/signup/profile/b;

    move-result-object v0

    return-object v0
.end method
