.class public final Lco/uk/getmondo/signup/rejected/a;
.super Ljava/lang/Object;
.source "SignupRejectedActivity_MembersInjector.java"

# interfaces
.implements Lb/a;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lb/a",
        "<",
        "Lco/uk/getmondo/signup/rejected/SignupRejectedActivity;",
        ">;"
    }
.end annotation


# static fields
.field static final synthetic a:Z


# instance fields
.field private final b:Ljavax/a/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/common/a;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 8
    const-class v0, Lco/uk/getmondo/signup/rejected/a;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lco/uk/getmondo/signup/rejected/a;->a:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Ljavax/a/a;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/common/a;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    sget-boolean v0, Lco/uk/getmondo/signup/rejected/a;->a:Z

    if-nez v0, :cond_0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 19
    :cond_0
    iput-object p1, p0, Lco/uk/getmondo/signup/rejected/a;->b:Ljavax/a/a;

    .line 20
    return-void
.end method

.method public static a(Ljavax/a/a;)Lb/a;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/common/a;",
            ">;)",
            "Lb/a",
            "<",
            "Lco/uk/getmondo/signup/rejected/SignupRejectedActivity;",
            ">;"
        }
    .end annotation

    .prologue
    .line 24
    new-instance v0, Lco/uk/getmondo/signup/rejected/a;

    invoke-direct {v0, p0}, Lco/uk/getmondo/signup/rejected/a;-><init>(Ljavax/a/a;)V

    return-object v0
.end method


# virtual methods
.method public a(Lco/uk/getmondo/signup/rejected/SignupRejectedActivity;)V
    .locals 2

    .prologue
    .line 29
    if-nez p1, :cond_0

    .line 30
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Cannot inject members into a null reference"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 32
    :cond_0
    iget-object v0, p0, Lco/uk/getmondo/signup/rejected/a;->b:Ljavax/a/a;

    invoke-interface {v0}, Ljavax/a/a;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/common/a;

    iput-object v0, p1, Lco/uk/getmondo/signup/rejected/SignupRejectedActivity;->a:Lco/uk/getmondo/common/a;

    .line 33
    return-void
.end method

.method public bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 8
    check-cast p1, Lco/uk/getmondo/signup/rejected/SignupRejectedActivity;

    invoke-virtual {p0, p1}, Lco/uk/getmondo/signup/rejected/a;->a(Lco/uk/getmondo/signup/rejected/SignupRejectedActivity;)V

    return-void
.end method
