.class public final Lco/uk/getmondo/signup/marketing_opt_in/b;
.super Ljava/lang/Object;
.source "MarketingOptInManager.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\u0018\u00002\u00020\u0001B\u000f\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u000e\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u0008R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\t"
    }
    d2 = {
        "Lco/uk/getmondo/signup/marketing_opt_in/MarketingOptInManager;",
        "",
        "signupApi",
        "Lco/uk/getmondo/api/SignupApi;",
        "(Lco/uk/getmondo/api/SignupApi;)V",
        "subscribeToMarketing",
        "Lio/reactivex/Completable;",
        "optIn",
        "",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field private final a:Lco/uk/getmondo/api/SignupApi;


# direct methods
.method public constructor <init>(Lco/uk/getmondo/api/SignupApi;)V
    .locals 1

    .prologue
    const-string v0, "signupApi"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lco/uk/getmondo/signup/marketing_opt_in/b;->a:Lco/uk/getmondo/api/SignupApi;

    return-void
.end method


# virtual methods
.method public final a(Z)Lio/reactivex/b;
    .locals 2

    .prologue
    .line 8
    iget-object v1, p0, Lco/uk/getmondo/signup/marketing_opt_in/b;->a:Lco/uk/getmondo/api/SignupApi;

    if-nez p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-interface {v1, p1, v0}, Lco/uk/getmondo/api/SignupApi;->subscribeToMarketing(ZZ)Lio/reactivex/b;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
