.class public final Lco/uk/getmondo/signup/marketing_opt_in/f;
.super Lco/uk/getmondo/common/ui/b;
.source "NewsAndUpdatesPresenter.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lco/uk/getmondo/signup/marketing_opt_in/f$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lco/uk/getmondo/common/ui/b",
        "<",
        "Lco/uk/getmondo/signup/marketing_opt_in/f$a;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000@\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u0003\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u0004\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00020\u0001:\u0001\u0016B3\u0008\u0007\u0012\u0008\u0008\u0001\u0010\u0003\u001a\u00020\u0004\u0012\u0008\u0008\u0001\u0010\u0005\u001a\u00020\u0004\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b\u00a2\u0006\u0002\u0010\u000cJ\u0010\u0010\r\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u0010H\u0002J\u0010\u0010\u0011\u001a\u00020\u000e2\u0006\u0010\u0012\u001a\u00020\u0013H\u0002J\u0010\u0010\u0014\u001a\u00020\u000e2\u0006\u0010\u0015\u001a\u00020\u0002H\u0016R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0017"
    }
    d2 = {
        "Lco/uk/getmondo/signup/marketing_opt_in/NewsAndUpdatesPresenter;",
        "Lco/uk/getmondo/common/ui/BasePresenter;",
        "Lco/uk/getmondo/signup/marketing_opt_in/NewsAndUpdatesPresenter$View;",
        "uiScheduler",
        "Lio/reactivex/Scheduler;",
        "ioScheduler",
        "analyticsService",
        "Lco/uk/getmondo/common/AnalyticsService;",
        "marketingOptInManager",
        "Lco/uk/getmondo/signup/marketing_opt_in/MarketingOptInManager;",
        "apiErrorHandler",
        "Lco/uk/getmondo/common/errors/ApiErrorHandler;",
        "(Lio/reactivex/Scheduler;Lio/reactivex/Scheduler;Lco/uk/getmondo/common/AnalyticsService;Lco/uk/getmondo/signup/marketing_opt_in/MarketingOptInManager;Lco/uk/getmondo/common/errors/ApiErrorHandler;)V",
        "handleError",
        "",
        "throwable",
        "",
        "handleSubscribe",
        "optIn",
        "",
        "register",
        "view",
        "View",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field private final c:Lio/reactivex/u;

.field private final d:Lio/reactivex/u;

.field private final e:Lco/uk/getmondo/common/a;

.field private final f:Lco/uk/getmondo/signup/marketing_opt_in/b;

.field private final g:Lco/uk/getmondo/common/e/a;


# direct methods
.method public constructor <init>(Lio/reactivex/u;Lio/reactivex/u;Lco/uk/getmondo/common/a;Lco/uk/getmondo/signup/marketing_opt_in/b;Lco/uk/getmondo/common/e/a;)V
    .locals 1

    .prologue
    const-string v0, "uiScheduler"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "ioScheduler"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "analyticsService"

    invoke-static {p3, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "marketingOptInManager"

    invoke-static {p4, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "apiErrorHandler"

    invoke-static {p5, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 26
    invoke-direct {p0}, Lco/uk/getmondo/common/ui/b;-><init>()V

    iput-object p1, p0, Lco/uk/getmondo/signup/marketing_opt_in/f;->c:Lio/reactivex/u;

    iput-object p2, p0, Lco/uk/getmondo/signup/marketing_opt_in/f;->d:Lio/reactivex/u;

    iput-object p3, p0, Lco/uk/getmondo/signup/marketing_opt_in/f;->e:Lco/uk/getmondo/common/a;

    iput-object p4, p0, Lco/uk/getmondo/signup/marketing_opt_in/f;->f:Lco/uk/getmondo/signup/marketing_opt_in/b;

    iput-object p5, p0, Lco/uk/getmondo/signup/marketing_opt_in/f;->g:Lco/uk/getmondo/common/e/a;

    return-void
.end method

.method public static final synthetic a(Lco/uk/getmondo/signup/marketing_opt_in/f;)Lco/uk/getmondo/common/a;
    .locals 1

    .prologue
    .line 19
    iget-object v0, p0, Lco/uk/getmondo/signup/marketing_opt_in/f;->e:Lco/uk/getmondo/common/a;

    return-object v0
.end method

.method public static final synthetic a(Lco/uk/getmondo/signup/marketing_opt_in/f;Ljava/lang/Throwable;)V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0, p1}, Lco/uk/getmondo/signup/marketing_opt_in/f;->a(Ljava/lang/Throwable;)V

    return-void
.end method

.method public static final synthetic a(Lco/uk/getmondo/signup/marketing_opt_in/f;Z)V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0, p1}, Lco/uk/getmondo/signup/marketing_opt_in/f;->a(Z)V

    return-void
.end method

.method private final a(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 63
    iget-object v0, p0, Lco/uk/getmondo/signup/marketing_opt_in/f;->a:Lco/uk/getmondo/common/ui/f;

    check-cast v0, Lco/uk/getmondo/signup/marketing_opt_in/f$a;

    invoke-interface {v0}, Lco/uk/getmondo/signup/marketing_opt_in/f$a;->e()V

    .line 64
    iget-object v0, p0, Lco/uk/getmondo/signup/marketing_opt_in/f;->a:Lco/uk/getmondo/common/ui/f;

    check-cast v0, Lco/uk/getmondo/signup/marketing_opt_in/f$a;

    invoke-interface {v0}, Lco/uk/getmondo/signup/marketing_opt_in/f$a;->g()V

    .line 65
    iget-object v0, p0, Lco/uk/getmondo/signup/marketing_opt_in/f;->a:Lco/uk/getmondo/common/ui/f;

    check-cast v0, Lco/uk/getmondo/signup/marketing_opt_in/f$a;

    invoke-interface {v0, v1}, Lco/uk/getmondo/signup/marketing_opt_in/f$a;->a(Z)V

    .line 66
    iget-object v0, p0, Lco/uk/getmondo/signup/marketing_opt_in/f;->a:Lco/uk/getmondo/common/ui/f;

    check-cast v0, Lco/uk/getmondo/signup/marketing_opt_in/f$a;

    invoke-interface {v0, v1}, Lco/uk/getmondo/signup/marketing_opt_in/f$a;->b(Z)V

    .line 67
    sget-object v1, Lco/uk/getmondo/signup/i;->a:Lco/uk/getmondo/signup/i;

    iget-object v0, p0, Lco/uk/getmondo/signup/marketing_opt_in/f;->a:Lco/uk/getmondo/common/ui/f;

    const-string v2, "view"

    invoke-static {v0, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lco/uk/getmondo/signup/i$a;

    invoke-virtual {v1, p1, v0}, Lco/uk/getmondo/signup/i;->a(Ljava/lang/Throwable;Lco/uk/getmondo/signup/i$a;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v1, p0, Lco/uk/getmondo/signup/marketing_opt_in/f;->g:Lco/uk/getmondo/common/e/a;

    iget-object v0, p0, Lco/uk/getmondo/signup/marketing_opt_in/f;->a:Lco/uk/getmondo/common/ui/f;

    const-string v2, "view"

    invoke-static {v0, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lco/uk/getmondo/common/e/a$a;

    invoke-virtual {v1, p1, v0}, Lco/uk/getmondo/common/e/a;->a(Ljava/lang/Throwable;Lco/uk/getmondo/common/e/a$a;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 68
    iget-object v0, p0, Lco/uk/getmondo/signup/marketing_opt_in/f;->a:Lco/uk/getmondo/common/ui/f;

    check-cast v0, Lco/uk/getmondo/signup/marketing_opt_in/f$a;

    const v1, 0x7f0a0196

    invoke-interface {v0, v1}, Lco/uk/getmondo/signup/marketing_opt_in/f$a;->b(I)V

    .line 70
    :cond_0
    return-void
.end method

.method private final a(Z)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 53
    if-eqz p1, :cond_0

    .line 54
    iget-object v0, p0, Lco/uk/getmondo/signup/marketing_opt_in/f;->a:Lco/uk/getmondo/common/ui/f;

    check-cast v0, Lco/uk/getmondo/signup/marketing_opt_in/f$a;

    invoke-interface {v0}, Lco/uk/getmondo/signup/marketing_opt_in/f$a;->d()V

    .line 57
    :goto_0
    iget-object v0, p0, Lco/uk/getmondo/signup/marketing_opt_in/f;->a:Lco/uk/getmondo/common/ui/f;

    check-cast v0, Lco/uk/getmondo/signup/marketing_opt_in/f$a;

    invoke-interface {v0, v1}, Lco/uk/getmondo/signup/marketing_opt_in/f$a;->a(Z)V

    .line 59
    iget-object v0, p0, Lco/uk/getmondo/signup/marketing_opt_in/f;->a:Lco/uk/getmondo/common/ui/f;

    check-cast v0, Lco/uk/getmondo/signup/marketing_opt_in/f$a;

    invoke-interface {v0, v1}, Lco/uk/getmondo/signup/marketing_opt_in/f$a;->b(Z)V

    .line 60
    return-void

    .line 56
    :cond_0
    iget-object v0, p0, Lco/uk/getmondo/signup/marketing_opt_in/f;->a:Lco/uk/getmondo/common/ui/f;

    check-cast v0, Lco/uk/getmondo/signup/marketing_opt_in/f$a;

    invoke-interface {v0}, Lco/uk/getmondo/signup/marketing_opt_in/f$a;->f()V

    goto :goto_0
.end method

.method public static final synthetic b(Lco/uk/getmondo/signup/marketing_opt_in/f;)Lco/uk/getmondo/signup/marketing_opt_in/b;
    .locals 1

    .prologue
    .line 19
    iget-object v0, p0, Lco/uk/getmondo/signup/marketing_opt_in/f;->f:Lco/uk/getmondo/signup/marketing_opt_in/b;

    return-object v0
.end method

.method public static final synthetic c(Lco/uk/getmondo/signup/marketing_opt_in/f;)Lio/reactivex/u;
    .locals 1

    .prologue
    .line 19
    iget-object v0, p0, Lco/uk/getmondo/signup/marketing_opt_in/f;->d:Lio/reactivex/u;

    return-object v0
.end method

.method public static final synthetic d(Lco/uk/getmondo/signup/marketing_opt_in/f;)Lio/reactivex/u;
    .locals 1

    .prologue
    .line 19
    iget-object v0, p0, Lco/uk/getmondo/signup/marketing_opt_in/f;->c:Lio/reactivex/u;

    return-object v0
.end method


# virtual methods
.method public bridge synthetic a(Lco/uk/getmondo/common/ui/f;)V
    .locals 0

    .prologue
    .line 19
    check-cast p1, Lco/uk/getmondo/signup/marketing_opt_in/f$a;

    invoke-virtual {p0, p1}, Lco/uk/getmondo/signup/marketing_opt_in/f;->a(Lco/uk/getmondo/signup/marketing_opt_in/f$a;)V

    return-void
.end method

.method public a(Lco/uk/getmondo/signup/marketing_opt_in/f$a;)V
    .locals 6

    .prologue
    const-string v0, "view"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v0, p1

    .line 29
    check-cast v0, Lco/uk/getmondo/common/ui/f;

    invoke-super {p0, v0}, Lco/uk/getmondo/common/ui/b;->a(Lco/uk/getmondo/common/ui/f;)V

    .line 31
    iget-object v0, p0, Lco/uk/getmondo/signup/marketing_opt_in/f;->e:Lco/uk/getmondo/common/a;

    sget-object v1, Lco/uk/getmondo/api/model/tracking/Impression;->Companion:Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    invoke-virtual {v1}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->aH()Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v1

    invoke-virtual {v0, v1}, Lco/uk/getmondo/common/a;->a(Lco/uk/getmondo/api/model/tracking/Impression;)V

    .line 33
    invoke-interface {p1}, Lco/uk/getmondo/signup/marketing_opt_in/f$a;->a()Lio/reactivex/n;

    move-result-object v1

    sget-object v0, Lco/uk/getmondo/signup/marketing_opt_in/f$e;->a:Lco/uk/getmondo/signup/marketing_opt_in/f$e;

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {v1, v0}, Lio/reactivex/n;->map(Lio/reactivex/c/h;)Lio/reactivex/n;

    move-result-object v1

    .line 34
    invoke-interface {p1}, Lco/uk/getmondo/signup/marketing_opt_in/f$a;->c()Lio/reactivex/n;

    move-result-object v2

    sget-object v0, Lco/uk/getmondo/signup/marketing_opt_in/f$f;->a:Lco/uk/getmondo/signup/marketing_opt_in/f$f;

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {v2, v0}, Lio/reactivex/n;->map(Lio/reactivex/c/h;)Lio/reactivex/n;

    move-result-object v0

    .line 36
    iget-object v2, p0, Lco/uk/getmondo/signup/marketing_opt_in/f;->b:Lio/reactivex/b/a;

    .line 37
    check-cast v0, Lio/reactivex/r;

    invoke-virtual {v1, v0}, Lio/reactivex/n;->mergeWith(Lio/reactivex/r;)Lio/reactivex/n;

    move-result-object v0

    .line 39
    const-wide/16 v4, 0x64

    sget-object v1, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v4, v5, v1}, Lio/reactivex/n;->throttleFirst(JLjava/util/concurrent/TimeUnit;)Lio/reactivex/n;

    move-result-object v1

    .line 40
    new-instance v0, Lco/uk/getmondo/signup/marketing_opt_in/f$b;

    invoke-direct {v0, p0}, Lco/uk/getmondo/signup/marketing_opt_in/f$b;-><init>(Lco/uk/getmondo/signup/marketing_opt_in/f;)V

    check-cast v0, Lio/reactivex/c/g;

    invoke-virtual {v1, v0}, Lio/reactivex/n;->doOnNext(Lio/reactivex/c/g;)Lio/reactivex/n;

    move-result-object v1

    .line 41
    new-instance v0, Lco/uk/getmondo/signup/marketing_opt_in/f$c;

    invoke-direct {v0, p0}, Lco/uk/getmondo/signup/marketing_opt_in/f$c;-><init>(Lco/uk/getmondo/signup/marketing_opt_in/f;)V

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {v1, v0}, Lio/reactivex/n;->flatMapMaybe(Lio/reactivex/c/h;)Lio/reactivex/n;

    move-result-object v1

    .line 49
    new-instance v0, Lco/uk/getmondo/signup/marketing_opt_in/f$d;

    invoke-direct {v0, p1}, Lco/uk/getmondo/signup/marketing_opt_in/f$d;-><init>(Lco/uk/getmondo/signup/marketing_opt_in/f$a;)V

    check-cast v0, Lio/reactivex/c/g;

    invoke-virtual {v1, v0}, Lio/reactivex/n;->subscribe(Lio/reactivex/c/g;)Lio/reactivex/b/b;

    move-result-object v0

    const-string v1, "optInClicks\n            \u2026 { view.completeStage() }"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 36
    invoke-static {v2, v0}, Lco/uk/getmondo/common/j/f;->a(Lio/reactivex/b/a;Lio/reactivex/b/b;)Lio/reactivex/b/a;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/signup/marketing_opt_in/f;->b:Lio/reactivex/b/a;

    .line 50
    return-void
.end method
