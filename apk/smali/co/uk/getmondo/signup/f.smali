.class public final Lco/uk/getmondo/signup/f;
.super Lco/uk/getmondo/common/ui/b;
.source "EmailPresenter.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lco/uk/getmondo/signup/f$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lco/uk/getmondo/common/ui/b",
        "<",
        "Lco/uk/getmondo/signup/f$a;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000D\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0005\u0008\u0007\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00020\u0001:\u0001\u0018BC\u0008\u0007\u0012\u0008\u0008\u0001\u0010\u0003\u001a\u00020\u0004\u0012\u0008\u0008\u0001\u0010\u0005\u001a\u00020\u0004\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b\u0012\u0006\u0010\u000c\u001a\u00020\r\u0012\u0006\u0010\u000e\u001a\u00020\u000f\u00a2\u0006\u0002\u0010\u0010J\u000e\u0010\u0011\u001a\u00020\u00122\u0006\u0010\u0013\u001a\u00020\u0014J\u0006\u0010\u0015\u001a\u00020\u0012J\u0010\u0010\u0016\u001a\u00020\u00122\u0006\u0010\u0017\u001a\u00020\u0002H\u0016R\u000e\u0010\u000c\u001a\u00020\rX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u000fX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0019"
    }
    d2 = {
        "Lco/uk/getmondo/signup/EmailPresenter;",
        "Lco/uk/getmondo/common/ui/BasePresenter;",
        "Lco/uk/getmondo/signup/EmailPresenter$EmailView;",
        "uiScheduler",
        "Lio/reactivex/Scheduler;",
        "ioScheduler",
        "apiErrorHandler",
        "Lco/uk/getmondo/common/errors/ApiErrorHandler;",
        "intentBuilder",
        "Lco/uk/getmondo/common/ExternalIntentBuilder;",
        "userInteractor",
        "Lco/uk/getmondo/api/interactors/UserInteractor;",
        "analyticsService",
        "Lco/uk/getmondo/common/AnalyticsService;",
        "pushNotificationRegistration",
        "Lco/uk/getmondo/fcm/PushNotificationRegistration;",
        "(Lio/reactivex/Scheduler;Lio/reactivex/Scheduler;Lco/uk/getmondo/common/errors/ApiErrorHandler;Lco/uk/getmondo/common/ExternalIntentBuilder;Lco/uk/getmondo/api/interactors/UserInteractor;Lco/uk/getmondo/common/AnalyticsService;Lco/uk/getmondo/fcm/PushNotificationRegistration;)V",
        "onNoEmail",
        "",
        "context",
        "Landroid/content/Context;",
        "onOpenEmailApp",
        "register",
        "view",
        "EmailView",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field private final c:Lio/reactivex/u;

.field private final d:Lio/reactivex/u;

.field private final e:Lco/uk/getmondo/common/e/a;

.field private final f:Lco/uk/getmondo/common/k;

.field private final g:Lco/uk/getmondo/api/b/a;

.field private final h:Lco/uk/getmondo/common/a;

.field private final i:Lco/uk/getmondo/fcm/e;


# direct methods
.method public constructor <init>(Lio/reactivex/u;Lio/reactivex/u;Lco/uk/getmondo/common/e/a;Lco/uk/getmondo/common/k;Lco/uk/getmondo/api/b/a;Lco/uk/getmondo/common/a;Lco/uk/getmondo/fcm/e;)V
    .locals 1

    .prologue
    const-string v0, "uiScheduler"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "ioScheduler"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "apiErrorHandler"

    invoke-static {p3, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "intentBuilder"

    invoke-static {p4, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "userInteractor"

    invoke-static {p5, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "analyticsService"

    invoke-static {p6, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "pushNotificationRegistration"

    invoke-static {p7, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 39
    invoke-direct {p0}, Lco/uk/getmondo/common/ui/b;-><init>()V

    iput-object p1, p0, Lco/uk/getmondo/signup/f;->c:Lio/reactivex/u;

    iput-object p2, p0, Lco/uk/getmondo/signup/f;->d:Lio/reactivex/u;

    iput-object p3, p0, Lco/uk/getmondo/signup/f;->e:Lco/uk/getmondo/common/e/a;

    iput-object p4, p0, Lco/uk/getmondo/signup/f;->f:Lco/uk/getmondo/common/k;

    iput-object p5, p0, Lco/uk/getmondo/signup/f;->g:Lco/uk/getmondo/api/b/a;

    iput-object p6, p0, Lco/uk/getmondo/signup/f;->h:Lco/uk/getmondo/common/a;

    iput-object p7, p0, Lco/uk/getmondo/signup/f;->i:Lco/uk/getmondo/fcm/e;

    return-void
.end method

.method public static final synthetic a(Lco/uk/getmondo/signup/f;)Lco/uk/getmondo/api/b/a;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lco/uk/getmondo/signup/f;->g:Lco/uk/getmondo/api/b/a;

    return-object v0
.end method

.method public static final synthetic b(Lco/uk/getmondo/signup/f;)Lio/reactivex/u;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lco/uk/getmondo/signup/f;->c:Lio/reactivex/u;

    return-object v0
.end method

.method public static final synthetic c(Lco/uk/getmondo/signup/f;)Lio/reactivex/u;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lco/uk/getmondo/signup/f;->d:Lio/reactivex/u;

    return-object v0
.end method

.method public static final synthetic d(Lco/uk/getmondo/signup/f;)Lco/uk/getmondo/common/e/a;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lco/uk/getmondo/signup/f;->e:Lco/uk/getmondo/common/e/a;

    return-object v0
.end method

.method public static final synthetic e(Lco/uk/getmondo/signup/f;)Lco/uk/getmondo/fcm/e;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lco/uk/getmondo/signup/f;->i:Lco/uk/getmondo/fcm/e;

    return-object v0
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 85
    iget-object v0, p0, Lco/uk/getmondo/signup/f;->h:Lco/uk/getmondo/common/a;

    sget-object v1, Lco/uk/getmondo/api/model/tracking/Impression;->Companion:Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    invoke-virtual {v1}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->h()Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v1

    invoke-virtual {v0, v1}, Lco/uk/getmondo/common/a;->a(Lco/uk/getmondo/api/model/tracking/Impression;)V

    .line 86
    return-void
.end method

.method public final a(Landroid/content/Context;)V
    .locals 2

    .prologue
    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 89
    iget-object v0, p0, Lco/uk/getmondo/signup/f;->h:Lco/uk/getmondo/common/a;

    sget-object v1, Lco/uk/getmondo/api/model/tracking/Impression;->Companion:Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    invoke-virtual {v1}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->i()Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v1

    invoke-virtual {v0, v1}, Lco/uk/getmondo/common/a;->a(Lco/uk/getmondo/api/model/tracking/Impression;)V

    .line 90
    iget-object v0, p0, Lco/uk/getmondo/signup/f;->f:Lco/uk/getmondo/common/k;

    const-string v1, "https://monzo.com/faq/no-email/"

    invoke-virtual {v0, p1, v1}, Lco/uk/getmondo/common/k;->a(Landroid/content/Context;Ljava/lang/String;)V

    .line 91
    return-void
.end method

.method public bridge synthetic a(Lco/uk/getmondo/common/ui/f;)V
    .locals 0

    .prologue
    .line 30
    check-cast p1, Lco/uk/getmondo/signup/f$a;

    invoke-virtual {p0, p1}, Lco/uk/getmondo/signup/f;->a(Lco/uk/getmondo/signup/f$a;)V

    return-void
.end method

.method public a(Lco/uk/getmondo/signup/f$a;)V
    .locals 5

    .prologue
    const-string v0, "view"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v0, p1

    .line 42
    check-cast v0, Lco/uk/getmondo/common/ui/f;

    invoke-super {p0, v0}, Lco/uk/getmondo/common/ui/b;->a(Lco/uk/getmondo/common/ui/f;)V

    .line 44
    iget-object v0, p0, Lco/uk/getmondo/signup/f;->h:Lco/uk/getmondo/common/a;

    sget-object v1, Lco/uk/getmondo/api/model/tracking/Impression;->Companion:Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    invoke-virtual {v1}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->g()Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v1

    invoke-virtual {v0, v1}, Lco/uk/getmondo/common/a;->a(Lco/uk/getmondo/api/model/tracking/Impression;)V

    .line 46
    iget-object v3, p0, Lco/uk/getmondo/signup/f;->b:Lio/reactivex/b/a;

    .line 71
    invoke-interface {p1}, Lco/uk/getmondo/signup/f$a;->b()Lio/reactivex/n;

    move-result-object v1

    .line 47
    new-instance v0, Lco/uk/getmondo/signup/f$b;

    invoke-direct {v0, p0, p1}, Lco/uk/getmondo/signup/f$b;-><init>(Lco/uk/getmondo/signup/f;Lco/uk/getmondo/signup/f$a;)V

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {v1, v0}, Lio/reactivex/n;->flatMapMaybe(Lio/reactivex/c/h;)Lio/reactivex/n;

    move-result-object v0

    .line 70
    iget-object v1, p0, Lco/uk/getmondo/signup/f;->c:Lio/reactivex/u;

    invoke-virtual {v0, v1}, Lio/reactivex/n;->observeOn(Lio/reactivex/u;)Lio/reactivex/n;

    move-result-object v4

    .line 71
    new-instance v0, Lco/uk/getmondo/signup/f$c;

    invoke-direct {v0, p0, p1}, Lco/uk/getmondo/signup/f$c;-><init>(Lco/uk/getmondo/signup/f;Lco/uk/getmondo/signup/f$a;)V

    check-cast v0, Lio/reactivex/c/g;

    .line 81
    sget-object v1, Lco/uk/getmondo/signup/f$d;->a:Lco/uk/getmondo/signup/f$d;

    check-cast v1, Lkotlin/d/a/b;

    if-eqz v1, :cond_0

    new-instance v2, Lco/uk/getmondo/signup/g;

    invoke-direct {v2, v1}, Lco/uk/getmondo/signup/g;-><init>(Lkotlin/d/a/b;)V

    move-object v1, v2

    :cond_0
    check-cast v1, Lio/reactivex/c/g;

    .line 71
    invoke-virtual {v4, v0, v1}, Lio/reactivex/n;->subscribe(Lio/reactivex/c/g;Lio/reactivex/c/g;)Lio/reactivex/b/b;

    move-result-object v0

    const-string v1, "view.onMagicLinkChanged(\u2026            }, Timber::e)"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 46
    invoke-static {v3, v0}, Lio/reactivex/rxkotlin/a;->a(Lio/reactivex/b/a;Lio/reactivex/b/b;)V

    .line 82
    return-void
.end method
