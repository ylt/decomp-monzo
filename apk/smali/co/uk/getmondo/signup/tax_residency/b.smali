.class public final Lco/uk/getmondo/signup/tax_residency/b;
.super Lco/uk/getmondo/common/ui/b;
.source "TaxResidencyPresenter.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lco/uk/getmondo/signup/tax_residency/b$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lco/uk/getmondo/common/ui/b",
        "<",
        "Lco/uk/getmondo/signup/tax_residency/b$a;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u00002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u0003\n\u0002\u0008\u0004\u0008\u0007\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00020\u0001:\u0001\u0011B+\u0008\u0007\u0012\u0008\u0008\u0001\u0010\u0003\u001a\u00020\u0004\u0012\u0008\u0008\u0001\u0010\u0005\u001a\u00020\u0004\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u00a2\u0006\u0002\u0010\nJ\u0018\u0010\u000b\u001a\u00020\u000c2\u0006\u0010\r\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u0002H\u0002J\u0010\u0010\u0010\u001a\u00020\u000c2\u0006\u0010\u000f\u001a\u00020\u0002H\u0016R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0012"
    }
    d2 = {
        "Lco/uk/getmondo/signup/tax_residency/TaxResidencyPresenter;",
        "Lco/uk/getmondo/common/ui/BasePresenter;",
        "Lco/uk/getmondo/signup/tax_residency/TaxResidencyPresenter$View;",
        "ioScheduler",
        "Lio/reactivex/Scheduler;",
        "uiScheduler",
        "taxResidencyManager",
        "Lco/uk/getmondo/signup/tax_residency/data/TaxResidencyManager;",
        "apiErrorHandler",
        "Lco/uk/getmondo/common/errors/ApiErrorHandler;",
        "(Lio/reactivex/Scheduler;Lio/reactivex/Scheduler;Lco/uk/getmondo/signup/tax_residency/data/TaxResidencyManager;Lco/uk/getmondo/common/errors/ApiErrorHandler;)V",
        "handleStatusError",
        "",
        "it",
        "",
        "view",
        "register",
        "View",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field private final c:Lio/reactivex/u;

.field private final d:Lio/reactivex/u;

.field private final e:Lco/uk/getmondo/signup/tax_residency/a/c;

.field private final f:Lco/uk/getmondo/common/e/a;


# direct methods
.method public constructor <init>(Lio/reactivex/u;Lio/reactivex/u;Lco/uk/getmondo/signup/tax_residency/a/c;Lco/uk/getmondo/common/e/a;)V
    .locals 1

    .prologue
    const-string v0, "ioScheduler"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "uiScheduler"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "taxResidencyManager"

    invoke-static {p3, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "apiErrorHandler"

    invoke-static {p4, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 28
    invoke-direct {p0}, Lco/uk/getmondo/common/ui/b;-><init>()V

    iput-object p1, p0, Lco/uk/getmondo/signup/tax_residency/b;->c:Lio/reactivex/u;

    iput-object p2, p0, Lco/uk/getmondo/signup/tax_residency/b;->d:Lio/reactivex/u;

    iput-object p3, p0, Lco/uk/getmondo/signup/tax_residency/b;->e:Lco/uk/getmondo/signup/tax_residency/a/c;

    iput-object p4, p0, Lco/uk/getmondo/signup/tax_residency/b;->f:Lco/uk/getmondo/common/e/a;

    return-void
.end method

.method public static final synthetic a(Lco/uk/getmondo/signup/tax_residency/b;)Lco/uk/getmondo/signup/tax_residency/a/c;
    .locals 1

    .prologue
    .line 22
    iget-object v0, p0, Lco/uk/getmondo/signup/tax_residency/b;->e:Lco/uk/getmondo/signup/tax_residency/a/c;

    return-object v0
.end method

.method public static final synthetic a(Lco/uk/getmondo/signup/tax_residency/b;Ljava/lang/Throwable;Lco/uk/getmondo/signup/tax_residency/b$a;)V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0, p1, p2}, Lco/uk/getmondo/signup/tax_residency/b;->a(Ljava/lang/Throwable;Lco/uk/getmondo/signup/tax_residency/b$a;)V

    return-void
.end method

.method private final a(Ljava/lang/Throwable;Lco/uk/getmondo/signup/tax_residency/b$a;)V
    .locals 3

    .prologue
    .line 61
    sget-object v1, Lco/uk/getmondo/signup/i;->a:Lco/uk/getmondo/signup/i;

    move-object v0, p2

    check-cast v0, Lco/uk/getmondo/signup/i$a;

    invoke-virtual {v1, p1, v0}, Lco/uk/getmondo/signup/i;->a(Ljava/lang/Throwable;Lco/uk/getmondo/signup/i$a;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 62
    iget-object v2, p0, Lco/uk/getmondo/signup/tax_residency/b;->f:Lco/uk/getmondo/common/e/a;

    move-object v0, p2

    check-cast v0, Lco/uk/getmondo/common/e/a$a;

    new-instance v1, Lco/uk/getmondo/signup/tax_residency/b$b;

    invoke-direct {v1, p2}, Lco/uk/getmondo/signup/tax_residency/b$b;-><init>(Lco/uk/getmondo/signup/tax_residency/b$a;)V

    check-cast v1, Lkotlin/d/a/b;

    invoke-virtual {v2, p1, v0, v1}, Lco/uk/getmondo/common/e/a;->a(Ljava/lang/Throwable;Lco/uk/getmondo/common/e/a$a;Lkotlin/d/a/b;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 63
    const v0, 0x7f0a0196

    invoke-interface {p2, v0}, Lco/uk/getmondo/signup/tax_residency/b$a;->b(I)V

    .line 65
    :cond_0
    return-void
.end method

.method public static final synthetic b(Lco/uk/getmondo/signup/tax_residency/b;)Lio/reactivex/u;
    .locals 1

    .prologue
    .line 22
    iget-object v0, p0, Lco/uk/getmondo/signup/tax_residency/b;->c:Lio/reactivex/u;

    return-object v0
.end method

.method public static final synthetic c(Lco/uk/getmondo/signup/tax_residency/b;)Lio/reactivex/u;
    .locals 1

    .prologue
    .line 22
    iget-object v0, p0, Lco/uk/getmondo/signup/tax_residency/b;->d:Lio/reactivex/u;

    return-object v0
.end method


# virtual methods
.method public bridge synthetic a(Lco/uk/getmondo/common/ui/f;)V
    .locals 0

    .prologue
    .line 22
    check-cast p1, Lco/uk/getmondo/signup/tax_residency/b$a;

    invoke-virtual {p0, p1}, Lco/uk/getmondo/signup/tax_residency/b;->a(Lco/uk/getmondo/signup/tax_residency/b$a;)V

    return-void
.end method

.method public a(Lco/uk/getmondo/signup/tax_residency/b$a;)V
    .locals 5

    .prologue
    const-string v0, "view"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v0, p1

    .line 31
    check-cast v0, Lco/uk/getmondo/common/ui/f;

    invoke-super {p0, v0}, Lco/uk/getmondo/common/ui/b;->a(Lco/uk/getmondo/common/ui/f;)V

    .line 34
    iget-object v3, p0, Lco/uk/getmondo/signup/tax_residency/b;->b:Lio/reactivex/b/a;

    .line 45
    invoke-interface {p1}, Lco/uk/getmondo/signup/tax_residency/b$a;->c()Lio/reactivex/n;

    move-result-object v0

    .line 35
    sget-object v1, Lkotlin/n;->a:Lkotlin/n;

    invoke-virtual {v0, v1}, Lio/reactivex/n;->startWith(Ljava/lang/Object;)Lio/reactivex/n;

    move-result-object v1

    .line 36
    new-instance v0, Lco/uk/getmondo/signup/tax_residency/b$c;

    invoke-direct {v0, p0, p1}, Lco/uk/getmondo/signup/tax_residency/b$c;-><init>(Lco/uk/getmondo/signup/tax_residency/b;Lco/uk/getmondo/signup/tax_residency/b$a;)V

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {v1, v0}, Lio/reactivex/n;->flatMapMaybe(Lio/reactivex/c/h;)Lio/reactivex/n;

    move-result-object v4

    .line 45
    new-instance v0, Lco/uk/getmondo/signup/tax_residency/b$d;

    invoke-direct {v0, p1}, Lco/uk/getmondo/signup/tax_residency/b$d;-><init>(Lco/uk/getmondo/signup/tax_residency/b$a;)V

    check-cast v0, Lio/reactivex/c/g;

    .line 56
    sget-object v1, Lco/uk/getmondo/signup/tax_residency/b$e;->a:Lco/uk/getmondo/signup/tax_residency/b$e;

    check-cast v1, Lkotlin/d/a/b;

    if-eqz v1, :cond_0

    new-instance v2, Lco/uk/getmondo/signup/tax_residency/d;

    invoke-direct {v2, v1}, Lco/uk/getmondo/signup/tax_residency/d;-><init>(Lkotlin/d/a/b;)V

    move-object v1, v2

    :cond_0
    check-cast v1, Lio/reactivex/c/g;

    .line 45
    invoke-virtual {v4, v0, v1}, Lio/reactivex/n;->subscribe(Lio/reactivex/c/g;Lio/reactivex/c/g;)Lio/reactivex/b/b;

    move-result-object v0

    const-string v1, "view.onRetryClicked\n    \u2026            }, Timber::e)"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 34
    invoke-static {v3, v0}, Lco/uk/getmondo/common/j/f;->a(Lio/reactivex/b/a;Lio/reactivex/b/b;)Lio/reactivex/b/a;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/signup/tax_residency/b;->b:Lio/reactivex/b/a;

    .line 58
    return-void
.end method
