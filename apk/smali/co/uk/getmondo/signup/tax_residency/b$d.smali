.class final Lco/uk/getmondo/signup/tax_residency/b$d;
.super Ljava/lang/Object;
.source "TaxResidencyPresenter.kt"

# interfaces
.implements Lio/reactivex/c/g;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lco/uk/getmondo/signup/tax_residency/b;->a(Lco/uk/getmondo/signup/tax_residency/b$a;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/g",
        "<",
        "Lco/uk/getmondo/api/model/tax_residency/TaxResidencyInfo;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "",
        "it",
        "Lco/uk/getmondo/api/model/tax_residency/TaxResidencyInfo;",
        "kotlin.jvm.PlatformType",
        "accept"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field final synthetic a:Lco/uk/getmondo/signup/tax_residency/b$a;


# direct methods
.method constructor <init>(Lco/uk/getmondo/signup/tax_residency/b$a;)V
    .locals 0

    iput-object p1, p0, Lco/uk/getmondo/signup/tax_residency/b$d;->a:Lco/uk/getmondo/signup/tax_residency/b$a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lco/uk/getmondo/api/model/tax_residency/TaxResidencyInfo;)V
    .locals 2

    .prologue
    .line 46
    invoke-virtual {p1}, Lco/uk/getmondo/api/model/tax_residency/TaxResidencyInfo;->a()Lco/uk/getmondo/api/model/tax_residency/TaxResidencyInfo$Status;

    move-result-object v0

    sget-object v1, Lco/uk/getmondo/signup/tax_residency/c;->a:[I

    invoke-virtual {v0}, Lco/uk/getmondo/api/model/tax_residency/TaxResidencyInfo$Status;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 55
    :goto_0
    return-void

    .line 48
    :pswitch_0
    invoke-virtual {p1}, Lco/uk/getmondo/api/model/tax_residency/TaxResidencyInfo;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 49
    iget-object v0, p0, Lco/uk/getmondo/signup/tax_residency/b$d;->a:Lco/uk/getmondo/signup/tax_residency/b$a;

    invoke-interface {v0}, Lco/uk/getmondo/signup/tax_residency/b$a;->f()V

    goto :goto_0

    .line 51
    :cond_0
    iget-object v0, p0, Lco/uk/getmondo/signup/tax_residency/b$d;->a:Lco/uk/getmondo/signup/tax_residency/b$a;

    invoke-interface {v0}, Lco/uk/getmondo/signup/tax_residency/b$a;->g()V

    goto :goto_0

    .line 54
    :pswitch_1
    iget-object v0, p0, Lco/uk/getmondo/signup/tax_residency/b$d;->a:Lco/uk/getmondo/signup/tax_residency/b$a;

    invoke-interface {v0}, Lco/uk/getmondo/signup/tax_residency/b$a;->h()V

    goto :goto_0

    .line 46
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 22
    check-cast p1, Lco/uk/getmondo/api/model/tax_residency/TaxResidencyInfo;

    invoke-virtual {p0, p1}, Lco/uk/getmondo/signup/tax_residency/b$d;->a(Lco/uk/getmondo/api/model/tax_residency/TaxResidencyInfo;)V

    return-void
.end method
