.class public final Lco/uk/getmondo/signup/tax_residency/b/a;
.super Landroid/support/v7/widget/RecyclerView$a;
.source "TaxResidencySelectionAdapter.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lco/uk/getmondo/signup/tax_residency/b/a$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/support/v7/widget/RecyclerView$a",
        "<",
        "Lco/uk/getmondo/signup/tax_residency/b/a$a;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000<\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010!\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u0008\n\u0002\u0010\u0008\n\u0002\u0008\u0006\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010 \n\u0002\u0008\u0004\u0018\u00002\u000c\u0012\u0008\u0012\u00060\u0002R\u00020\u00000\u0001:\u0001\u001fB-\u0012\u000e\u0008\u0002\u0010\u0003\u001a\u0008\u0012\u0004\u0012\u00020\u00050\u0004\u0012\u0016\u0008\u0002\u0010\u0006\u001a\u0010\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u0008\u0018\u00010\u0007\u00a2\u0006\u0002\u0010\tJ\u0008\u0010\u0010\u001a\u00020\u0011H\u0016J\u0010\u0010\u0012\u001a\u00020\u00112\u0006\u0010\u0013\u001a\u00020\u0011H\u0016J\u001c\u0010\u0014\u001a\u00020\u00082\n\u0010\u0015\u001a\u00060\u0002R\u00020\u00002\u0006\u0010\u0013\u001a\u00020\u0011H\u0016J\u001c\u0010\u0016\u001a\u00060\u0002R\u00020\u00002\u0006\u0010\u0017\u001a\u00020\u00182\u0006\u0010\u0019\u001a\u00020\u0011H\u0016J\u0014\u0010\u001a\u001a\u00020\u00082\u000c\u0010\u001b\u001a\u0008\u0012\u0004\u0012\u00020\u00050\u001cJ\u000e\u0010\u001d\u001a\u00020\u00082\u0006\u0010\u001e\u001a\u00020\u0005R(\u0010\u0006\u001a\u0010\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u0008\u0018\u00010\u0007X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\n\u0010\u000b\"\u0004\u0008\u000c\u0010\rR\u0017\u0010\u0003\u001a\u0008\u0012\u0004\u0012\u00020\u00050\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000e\u0010\u000f\u00a8\u0006 "
    }
    d2 = {
        "Lco/uk/getmondo/signup/tax_residency/step/TaxResidencySelectionAdapter;",
        "Landroid/support/v7/widget/RecyclerView$Adapter;",
        "Lco/uk/getmondo/signup/tax_residency/step/TaxResidencySelectionAdapter$TaxCountryViewHolder;",
        "taxCountries",
        "",
        "Lco/uk/getmondo/signup/tax_residency/data/TaxCountry;",
        "clickListener",
        "Lkotlin/Function1;",
        "",
        "(Ljava/util/List;Lkotlin/jvm/functions/Function1;)V",
        "getClickListener",
        "()Lkotlin/jvm/functions/Function1;",
        "setClickListener",
        "(Lkotlin/jvm/functions/Function1;)V",
        "getTaxCountries",
        "()Ljava/util/List;",
        "getItemCount",
        "",
        "getItemViewType",
        "position",
        "onBindViewHolder",
        "holder",
        "onCreateViewHolder",
        "parent",
        "Landroid/view/ViewGroup;",
        "viewType",
        "setCountries",
        "countries",
        "",
        "updateCountry",
        "taxCountry",
        "TaxCountryViewHolder",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field private final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lco/uk/getmondo/signup/tax_residency/a/a;",
            ">;"
        }
    .end annotation
.end field

.field private b:Lkotlin/d/a/b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/d/a/b",
            "<-",
            "Lco/uk/getmondo/signup/tax_residency/a/a;",
            "Lkotlin/n;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    const/4 v0, 0x3

    invoke-direct {p0, v1, v1, v0, v1}, Lco/uk/getmondo/signup/tax_residency/b/a;-><init>(Ljava/util/List;Lkotlin/d/a/b;ILkotlin/d/b/i;)V

    return-void
.end method

.method public constructor <init>(Ljava/util/List;Lkotlin/d/a/b;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lco/uk/getmondo/signup/tax_residency/a/a;",
            ">;",
            "Lkotlin/d/a/b",
            "<-",
            "Lco/uk/getmondo/signup/tax_residency/a/a;",
            "Lkotlin/n;",
            ">;)V"
        }
    .end annotation

    .prologue
    const-string v0, "taxCountries"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 14
    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView$a;-><init>()V

    iput-object p1, p0, Lco/uk/getmondo/signup/tax_residency/b/a;->a:Ljava/util/List;

    iput-object p2, p0, Lco/uk/getmondo/signup/tax_residency/b/a;->b:Lkotlin/d/a/b;

    return-void
.end method

.method public synthetic constructor <init>(Ljava/util/List;Lkotlin/d/a/b;ILkotlin/d/b/i;)V
    .locals 1

    .prologue
    and-int/lit8 v0, p3, 0x1

    if-eqz v0, :cond_0

    .line 12
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    check-cast v0, Ljava/util/List;

    move-object p1, v0

    :cond_0
    and-int/lit8 v0, p3, 0x2

    if-eqz v0, :cond_1

    .line 13
    const/4 v0, 0x0

    check-cast v0, Lkotlin/d/a/b;

    :goto_0
    invoke-direct {p0, p1, v0}, Lco/uk/getmondo/signup/tax_residency/b/a;-><init>(Ljava/util/List;Lkotlin/d/a/b;)V

    return-void

    :cond_1
    move-object v0, p2

    goto :goto_0
.end method


# virtual methods
.method public a(Landroid/view/ViewGroup;I)Lco/uk/getmondo/signup/tax_residency/b/a$a;
    .locals 3

    .prologue
    const-string v0, "parent"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 17
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, p2, p1, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 18
    new-instance v1, Lco/uk/getmondo/signup/tax_residency/b/a$a;

    const-string v2, "view"

    invoke-static {v0, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v1, p0, v0}, Lco/uk/getmondo/signup/tax_residency/b/a$a;-><init>(Lco/uk/getmondo/signup/tax_residency/b/a;Landroid/view/View;)V

    return-object v1
.end method

.method public final a()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lco/uk/getmondo/signup/tax_residency/a/a;",
            ">;"
        }
    .end annotation

    .prologue
    .line 12
    iget-object v0, p0, Lco/uk/getmondo/signup/tax_residency/b/a;->a:Ljava/util/List;

    return-object v0
.end method

.method public final a(Lco/uk/getmondo/signup/tax_residency/a/a;)V
    .locals 4

    .prologue
    const-string v0, "taxCountry"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 43
    iget-object v1, p0, Lco/uk/getmondo/signup/tax_residency/b/a;->a:Ljava/util/List;

    .line 65
    const/4 v0, 0x0

    .line 66
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .line 67
    check-cast v0, Lco/uk/getmondo/signup/tax_residency/a/a;

    .line 43
    invoke-virtual {v0}, Lco/uk/getmondo/signup/tax_residency/a/a;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lco/uk/getmondo/signup/tax_residency/a/a;->b()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 44
    :goto_1
    iget-object v0, p0, Lco/uk/getmondo/signup/tax_residency/b/a;->a:Ljava/util/List;

    invoke-interface {v0, v1, p1}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 45
    invoke-virtual {p0, v1}, Lco/uk/getmondo/signup/tax_residency/b/a;->notifyItemChanged(I)V

    .line 46
    return-void

    .line 69
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    .line 66
    goto :goto_0

    .line 71
    :cond_1
    const/4 v1, -0x1

    goto :goto_1
.end method

.method public a(Lco/uk/getmondo/signup/tax_residency/b/a$a;I)V
    .locals 1

    .prologue
    const-string v0, "holder"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 22
    iget-object v0, p0, Lco/uk/getmondo/signup/tax_residency/b/a;->a:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/signup/tax_residency/a/a;

    invoke-virtual {p1, v0}, Lco/uk/getmondo/signup/tax_residency/b/a$a;->a(Lco/uk/getmondo/signup/tax_residency/a/a;)V

    .line 23
    return-void
.end method

.method public final a(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lco/uk/getmondo/signup/tax_residency/a/a;",
            ">;)V"
        }
    .end annotation

    .prologue
    const-string v0, "countries"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 37
    iget-object v0, p0, Lco/uk/getmondo/signup/tax_residency/b/a;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 38
    iget-object v0, p0, Lco/uk/getmondo/signup/tax_residency/b/a;->a:Ljava/util/List;

    check-cast p1, Ljava/util/Collection;

    invoke-interface {v0, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 39
    invoke-virtual {p0}, Lco/uk/getmondo/signup/tax_residency/b/a;->notifyDataSetChanged()V

    .line 40
    return-void
.end method

.method public final a(Lkotlin/d/a/b;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/d/a/b",
            "<-",
            "Lco/uk/getmondo/signup/tax_residency/a/a;",
            "Lkotlin/n;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 13
    iput-object p1, p0, Lco/uk/getmondo/signup/tax_residency/b/a;->b:Lkotlin/d/a/b;

    return-void
.end method

.method public final b()Lkotlin/d/a/b;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/d/a/b",
            "<",
            "Lco/uk/getmondo/signup/tax_residency/a/a;",
            "Lkotlin/n;",
            ">;"
        }
    .end annotation

    .prologue
    .line 13
    iget-object v0, p0, Lco/uk/getmondo/signup/tax_residency/b/a;->b:Lkotlin/d/a/b;

    return-object v0
.end method

.method public getItemCount()I
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lco/uk/getmondo/signup/tax_residency/b/a;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getItemViewType(I)I
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lco/uk/getmondo/signup/tax_residency/b/a;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/signup/tax_residency/a/a;

    invoke-virtual {v0}, Lco/uk/getmondo/signup/tax_residency/a/a;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 31
    const v0, 0x7f05010b

    .line 33
    :goto_0
    return v0

    :cond_0
    const v0, 0x7f050101

    goto :goto_0
.end method

.method public synthetic onBindViewHolder(Landroid/support/v7/widget/RecyclerView$w;I)V
    .locals 0

    .prologue
    .line 11
    check-cast p1, Lco/uk/getmondo/signup/tax_residency/b/a$a;

    invoke-virtual {p0, p1, p2}, Lco/uk/getmondo/signup/tax_residency/b/a;->a(Lco/uk/getmondo/signup/tax_residency/b/a$a;I)V

    return-void
.end method

.method public synthetic onCreateViewHolder(Landroid/view/ViewGroup;I)Landroid/support/v7/widget/RecyclerView$w;
    .locals 1

    .prologue
    .line 11
    invoke-virtual {p0, p1, p2}, Lco/uk/getmondo/signup/tax_residency/b/a;->a(Landroid/view/ViewGroup;I)Lco/uk/getmondo/signup/tax_residency/b/a$a;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView$w;

    return-object v0
.end method
