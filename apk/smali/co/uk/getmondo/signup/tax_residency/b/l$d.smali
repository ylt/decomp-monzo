.class final Lco/uk/getmondo/signup/tax_residency/b/l$d;
.super Ljava/lang/Object;
.source "TaxResidencyTinEntryFragment.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lco/uk/getmondo/signup/tax_residency/b/l;->d()Lio/reactivex/n;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h",
        "<TT;TR;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n\u00a2\u0006\u0004\u0008\u0004\u0010\u0005"
    }
    d2 = {
        "<anonymous>",
        "",
        "it",
        "",
        "apply",
        "(Lkotlin/Unit;)Ljava/lang/String;"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field final synthetic a:Lco/uk/getmondo/signup/tax_residency/b/l;


# direct methods
.method constructor <init>(Lco/uk/getmondo/signup/tax_residency/b/l;)V
    .locals 0

    iput-object p1, p0, Lco/uk/getmondo/signup/tax_residency/b/l$d;->a:Lco/uk/getmondo/signup/tax_residency/b/l;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 21
    check-cast p1, Lkotlin/n;

    invoke-virtual {p0, p1}, Lco/uk/getmondo/signup/tax_residency/b/l$d;->a(Lkotlin/n;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lkotlin/n;)Ljava/lang/String;
    .locals 2

    .prologue
    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 67
    iget-object v0, p0, Lco/uk/getmondo/signup/tax_residency/b/l$d;->a:Lco/uk/getmondo/signup/tax_residency/b/l;

    sget v1, Lco/uk/getmondo/c$a;->taxNumberView:I

    invoke-virtual {v0, v1}, Lco/uk/getmondo/signup/tax_residency/b/l;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/signup/tax_residency/ui/TaxResidencyNumberView;

    sget v1, Lco/uk/getmondo/c$a;->taxNumberEditText:I

    invoke-virtual {v0, v1}, Lco/uk/getmondo/signup/tax_residency/ui/TaxResidencyNumberView;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/design/widget/TextInputEditText;

    invoke-virtual {v0}, Landroid/support/design/widget/TextInputEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
