.class public interface abstract Lco/uk/getmondo/signup/tax_residency/b/d$a;
.super Ljava/lang/Object;
.source "TaxResidencySelectionPresenter.kt"

# interfaces
.implements Lco/uk/getmondo/common/e/a$a;
.implements Lco/uk/getmondo/common/ui/f;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lco/uk/getmondo/signup/tax_residency/b/d;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "a"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000:\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010 \n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0008\u0005\u0008f\u0018\u00002\u00020\u00012\u00020\u0002J\u0008\u0010\u0012\u001a\u00020\u0008H&J\u0010\u0010\u0013\u001a\u00020\u00082\u0006\u0010\u0014\u001a\u00020\u0015H&J\u0010\u0010\u0016\u001a\u00020\u00082\u0006\u0010\u0017\u001a\u00020\u000cH&J\u0010\u0010\u0018\u001a\u00020\u00082\u0006\u0010\u0019\u001a\u00020\u0004H&R\u0012\u0010\u0003\u001a\u00020\u0004X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0003\u0010\u0005R\u0018\u0010\u0006\u001a\u0008\u0012\u0004\u0012\u00020\u00080\u0007X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\t\u0010\nR\u0018\u0010\u000b\u001a\u0008\u0012\u0004\u0012\u00020\u000c0\u0007X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\r\u0010\nR\u0018\u0010\u000e\u001a\u0008\u0012\u0004\u0012\u00020\u000c0\u000fX\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0010\u0010\u0011\u00a8\u0006\u001a"
    }
    d2 = {
        "Lco/uk/getmondo/signup/tax_residency/step/TaxResidencySelectionPresenter$View;",
        "Lco/uk/getmondo/common/ui/MvpView;",
        "Lco/uk/getmondo/common/errors/ApiErrorHandler$ApiErrorView;",
        "isUsTaxResident",
        "",
        "()Z",
        "onContinue",
        "Lio/reactivex/Observable;",
        "",
        "getOnContinue",
        "()Lio/reactivex/Observable;",
        "onCountryClicked",
        "Lco/uk/getmondo/signup/tax_residency/data/TaxCountry;",
        "getOnCountryClicked",
        "taxCountries",
        "",
        "getTaxCountries",
        "()Ljava/util/List;",
        "completeStage",
        "goToNextStep",
        "jurisdiction",
        "Lco/uk/getmondo/api/model/tax_residency/Jurisdiction;",
        "setSelection",
        "taxCountry",
        "showLoading",
        "loading",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# virtual methods
.method public abstract a()Lio/reactivex/n;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Lco/uk/getmondo/signup/tax_residency/a/a;",
            ">;"
        }
    .end annotation
.end method

.method public abstract a(Lco/uk/getmondo/api/model/tax_residency/Jurisdiction;)V
.end method

.method public abstract a(Lco/uk/getmondo/signup/tax_residency/a/a;)V
.end method

.method public abstract a(Z)V
.end method

.method public abstract b()Lio/reactivex/n;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Lkotlin/n;",
            ">;"
        }
    .end annotation
.end method

.method public abstract c()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lco/uk/getmondo/signup/tax_residency/a/a;",
            ">;"
        }
    .end annotation
.end method

.method public abstract d()Z
.end method

.method public abstract e()V
.end method
