.class final Lco/uk/getmondo/signup/tax_residency/b/d$f;
.super Ljava/lang/Object;
.source "TaxResidencySelectionPresenter.kt"

# interfaces
.implements Lio/reactivex/c/g;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lco/uk/getmondo/signup/tax_residency/b/d;->a(Lco/uk/getmondo/signup/tax_residency/b/d$a;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/g",
        "<",
        "Lcom/c/b/b",
        "<",
        "Lco/uk/getmondo/api/model/tax_residency/Jurisdiction;",
        ">;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000\u0014\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u001a\u0010\u0002\u001a\u0016\u0012\u0004\u0012\u00020\u0004 \u0005*\n\u0012\u0004\u0012\u00020\u0004\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "",
        "jurisdiction",
        "Lcom/memoizrlabs/poweroptional/Optional;",
        "Lco/uk/getmondo/api/model/tax_residency/Jurisdiction;",
        "kotlin.jvm.PlatformType",
        "accept"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field final synthetic a:Lco/uk/getmondo/signup/tax_residency/b/d$a;


# direct methods
.method constructor <init>(Lco/uk/getmondo/signup/tax_residency/b/d$a;)V
    .locals 0

    iput-object p1, p0, Lco/uk/getmondo/signup/tax_residency/b/d$f;->a:Lco/uk/getmondo/signup/tax_residency/b/d$a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/c/b/b;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/c/b/b",
            "<",
            "Lco/uk/getmondo/api/model/tax_residency/Jurisdiction;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 57
    invoke-virtual {p1}, Lcom/c/b/b;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 58
    iget-object v0, p0, Lco/uk/getmondo/signup/tax_residency/b/d$f;->a:Lco/uk/getmondo/signup/tax_residency/b/d$a;

    invoke-interface {v0}, Lco/uk/getmondo/signup/tax_residency/b/d$a;->e()V

    .line 61
    :goto_0
    return-void

    .line 60
    :cond_0
    iget-object v1, p0, Lco/uk/getmondo/signup/tax_residency/b/d$f;->a:Lco/uk/getmondo/signup/tax_residency/b/d$a;

    invoke-virtual {p1}, Lcom/c/b/b;->a()Ljava/lang/Object;

    move-result-object v0

    const-string v2, "jurisdiction.get()"

    invoke-static {v0, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lco/uk/getmondo/api/model/tax_residency/Jurisdiction;

    invoke-interface {v1, v0}, Lco/uk/getmondo/signup/tax_residency/b/d$a;->a(Lco/uk/getmondo/api/model/tax_residency/Jurisdiction;)V

    goto :goto_0
.end method

.method public bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 23
    check-cast p1, Lcom/c/b/b;

    invoke-virtual {p0, p1}, Lco/uk/getmondo/signup/tax_residency/b/d$f;->a(Lcom/c/b/b;)V

    return-void
.end method
