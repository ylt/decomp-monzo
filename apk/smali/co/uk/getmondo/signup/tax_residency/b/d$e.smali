.class final Lco/uk/getmondo/signup/tax_residency/b/d$e;
.super Ljava/lang/Object;
.source "TaxResidencySelectionPresenter.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lco/uk/getmondo/signup/tax_residency/b/d;->a(Lco/uk/getmondo/signup/tax_residency/b/d$a;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h",
        "<TT;",
        "Lio/reactivex/l",
        "<+TR;>;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000\u001a\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u001c\u0012\u0018\u0012\u0016\u0012\u0004\u0012\u00020\u0003 \u0004*\n\u0012\u0004\u0012\u00020\u0003\u0018\u00010\u00020\u00020\u00012\u0006\u0010\u0005\u001a\u00020\u0006H\n\u00a2\u0006\u0004\u0008\u0007\u0010\u0008"
    }
    d2 = {
        "<anonymous>",
        "Lio/reactivex/Maybe;",
        "Lcom/memoizrlabs/poweroptional/Optional;",
        "Lco/uk/getmondo/api/model/tax_residency/Jurisdiction;",
        "kotlin.jvm.PlatformType",
        "it",
        "",
        "apply",
        "(Lkotlin/Unit;)Lio/reactivex/Maybe;"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field final synthetic a:Lco/uk/getmondo/signup/tax_residency/b/d;

.field final synthetic b:Lco/uk/getmondo/signup/tax_residency/b/d$a;


# direct methods
.method constructor <init>(Lco/uk/getmondo/signup/tax_residency/b/d;Lco/uk/getmondo/signup/tax_residency/b/d$a;)V
    .locals 0

    iput-object p1, p0, Lco/uk/getmondo/signup/tax_residency/b/d$e;->a:Lco/uk/getmondo/signup/tax_residency/b/d;

    iput-object p2, p0, Lco/uk/getmondo/signup/tax_residency/b/d$e;->b:Lco/uk/getmondo/signup/tax_residency/b/d$a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lkotlin/n;)Lio/reactivex/h;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/n;",
            ")",
            "Lio/reactivex/h",
            "<",
            "Lcom/c/b/b",
            "<",
            "Lco/uk/getmondo/api/model/tax_residency/Jurisdiction;",
            ">;>;"
        }
    .end annotation

    .prologue
    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 43
    iget-object v0, p0, Lco/uk/getmondo/signup/tax_residency/b/d$e;->a:Lco/uk/getmondo/signup/tax_residency/b/d;

    invoke-static {v0}, Lco/uk/getmondo/signup/tax_residency/b/d;->a(Lco/uk/getmondo/signup/tax_residency/b/d;)Lco/uk/getmondo/signup/tax_residency/a/c;

    move-result-object v3

    iget-object v0, p0, Lco/uk/getmondo/signup/tax_residency/b/d$e;->b:Lco/uk/getmondo/signup/tax_residency/b/d$a;

    invoke-interface {v0}, Lco/uk/getmondo/signup/tax_residency/b/d$a;->d()Z

    move-result v4

    iget-object v0, p0, Lco/uk/getmondo/signup/tax_residency/b/d$e;->b:Lco/uk/getmondo/signup/tax_residency/b/d$a;

    invoke-interface {v0}, Lco/uk/getmondo/signup/tax_residency/b/d$a;->c()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 80
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    check-cast v1, Ljava/util/Collection;

    .line 81
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_0
    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    move-object v0, v2

    check-cast v0, Lco/uk/getmondo/signup/tax_residency/a/a;

    .line 43
    invoke-virtual {v0}, Lco/uk/getmondo/signup/tax_residency/a/a;->e()Z

    move-result v6

    if-nez v6, :cond_1

    invoke-virtual {v0}, Lco/uk/getmondo/signup/tax_residency/a/a;->d()Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    :goto_1
    if-eqz v0, :cond_0

    invoke-interface {v1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    .line 82
    :cond_3
    check-cast v1, Ljava/util/List;

    .line 43
    invoke-virtual {v3, v4, v1}, Lco/uk/getmondo/signup/tax_residency/a/c;->a(ZLjava/util/List;)Lio/reactivex/b;

    move-result-object v1

    .line 44
    iget-object v0, p0, Lco/uk/getmondo/signup/tax_residency/b/d$e;->a:Lco/uk/getmondo/signup/tax_residency/b/d;

    invoke-static {v0}, Lco/uk/getmondo/signup/tax_residency/b/d;->a(Lco/uk/getmondo/signup/tax_residency/b/d;)Lco/uk/getmondo/signup/tax_residency/a/c;

    move-result-object v0

    invoke-virtual {v0}, Lco/uk/getmondo/signup/tax_residency/a/c;->b()Lio/reactivex/v;

    move-result-object v0

    check-cast v0, Lio/reactivex/z;

    invoke-virtual {v1, v0}, Lio/reactivex/b;->a(Lio/reactivex/z;)Lio/reactivex/v;

    move-result-object v0

    .line 45
    iget-object v1, p0, Lco/uk/getmondo/signup/tax_residency/b/d$e;->a:Lco/uk/getmondo/signup/tax_residency/b/d;

    invoke-static {v1}, Lco/uk/getmondo/signup/tax_residency/b/d;->b(Lco/uk/getmondo/signup/tax_residency/b/d;)Lio/reactivex/u;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/v;->b(Lio/reactivex/u;)Lio/reactivex/v;

    move-result-object v0

    .line 46
    iget-object v1, p0, Lco/uk/getmondo/signup/tax_residency/b/d$e;->a:Lco/uk/getmondo/signup/tax_residency/b/d;

    invoke-static {v1}, Lco/uk/getmondo/signup/tax_residency/b/d;->c(Lco/uk/getmondo/signup/tax_residency/b/d;)Lio/reactivex/u;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/v;->a(Lio/reactivex/u;)Lio/reactivex/v;

    move-result-object v1

    .line 47
    new-instance v0, Lco/uk/getmondo/signup/tax_residency/b/d$e$1;

    invoke-direct {v0, p0}, Lco/uk/getmondo/signup/tax_residency/b/d$e$1;-><init>(Lco/uk/getmondo/signup/tax_residency/b/d$e;)V

    check-cast v0, Lio/reactivex/c/g;

    invoke-virtual {v1, v0}, Lio/reactivex/v;->b(Lio/reactivex/c/g;)Lio/reactivex/v;

    move-result-object v1

    .line 48
    new-instance v0, Lco/uk/getmondo/signup/tax_residency/b/d$e$2;

    invoke-direct {v0, p0}, Lco/uk/getmondo/signup/tax_residency/b/d$e$2;-><init>(Lco/uk/getmondo/signup/tax_residency/b/d$e;)V

    check-cast v0, Lio/reactivex/c/g;

    invoke-virtual {v1, v0}, Lio/reactivex/v;->d(Lio/reactivex/c/g;)Lio/reactivex/v;

    move-result-object v0

    .line 54
    invoke-static {v0}, Lco/uk/getmondo/common/j/f;->a(Lio/reactivex/v;)Lio/reactivex/h;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 23
    check-cast p1, Lkotlin/n;

    invoke-virtual {p0, p1}, Lco/uk/getmondo/signup/tax_residency/b/d$e;->a(Lkotlin/n;)Lio/reactivex/h;

    move-result-object v0

    return-object v0
.end method
