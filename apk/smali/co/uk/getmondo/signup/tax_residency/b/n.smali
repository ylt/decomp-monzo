.class public final Lco/uk/getmondo/signup/tax_residency/b/n;
.super Lco/uk/getmondo/common/ui/b;
.source "TaxResidencyTinEntryPresenter.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lco/uk/getmondo/signup/tax_residency/b/n$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lco/uk/getmondo/common/ui/b",
        "<",
        "Lco/uk/getmondo/signup/tax_residency/b/n$a;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000>\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0007\u0008\u0007\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00020\u0001:\u0001\u0018B3\u0008\u0007\u0012\u0008\u0008\u0001\u0010\u0003\u001a\u00020\u0004\u0012\u0008\u0008\u0001\u0010\u0005\u001a\u00020\u0004\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b\u00a2\u0006\u0002\u0010\u000cJ\u0018\u0010\u000f\u001a\u00020\u00102\u0006\u0010\u0011\u001a\u00020\u00122\u0006\u0010\u0013\u001a\u00020\u0002H\u0002J\u001a\u0010\u0014\u001a\u0004\u0018\u00010\u000e2\u0006\u0010\u0015\u001a\u00020\u000e2\u0006\u0010\u0011\u001a\u00020\u0012H\u0002J\u0010\u0010\u0016\u001a\u00020\u00102\u0006\u0010\u0013\u001a\u00020\u0002H\u0016J \u0010\u0017\u001a\u00020\u00102\u0006\u0010\u0015\u001a\u00020\u000e2\u0006\u0010\u0011\u001a\u00020\u00122\u0006\u0010\u0013\u001a\u00020\u0002H\u0002R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u000eX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0019"
    }
    d2 = {
        "Lco/uk/getmondo/signup/tax_residency/step/TaxResidencyTinEntryPresenter;",
        "Lco/uk/getmondo/common/ui/BasePresenter;",
        "Lco/uk/getmondo/signup/tax_residency/step/TaxResidencyTinEntryPresenter$View;",
        "ioScheduler",
        "Lio/reactivex/Scheduler;",
        "uiScheduler",
        "taxResidencyManager",
        "Lco/uk/getmondo/signup/tax_residency/data/TaxResidencyManager;",
        "apiErrorHandler",
        "Lco/uk/getmondo/common/errors/ApiErrorHandler;",
        "analyticsService",
        "Lco/uk/getmondo/common/AnalyticsService;",
        "(Lio/reactivex/Scheduler;Lio/reactivex/Scheduler;Lco/uk/getmondo/signup/tax_residency/data/TaxResidencyManager;Lco/uk/getmondo/common/errors/ApiErrorHandler;Lco/uk/getmondo/common/AnalyticsService;)V",
        "currentTinType",
        "Lco/uk/getmondo/api/model/tax_residency/Jurisdiction$TinType;",
        "displayJurisdiction",
        "",
        "jurisdiction",
        "Lco/uk/getmondo/api/model/tax_residency/Jurisdiction;",
        "view",
        "getAltTinType",
        "tinType",
        "register",
        "updateTinTypes",
        "View",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field private c:Lco/uk/getmondo/api/model/tax_residency/Jurisdiction$TinType;

.field private final d:Lio/reactivex/u;

.field private final e:Lio/reactivex/u;

.field private final f:Lco/uk/getmondo/signup/tax_residency/a/c;

.field private final g:Lco/uk/getmondo/common/e/a;

.field private final h:Lco/uk/getmondo/common/a;


# direct methods
.method public constructor <init>(Lio/reactivex/u;Lio/reactivex/u;Lco/uk/getmondo/signup/tax_residency/a/c;Lco/uk/getmondo/common/e/a;Lco/uk/getmondo/common/a;)V
    .locals 1

    .prologue
    const-string v0, "ioScheduler"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "uiScheduler"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "taxResidencyManager"

    invoke-static {p3, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "apiErrorHandler"

    invoke-static {p4, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "analyticsService"

    invoke-static {p5, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 31
    invoke-direct {p0}, Lco/uk/getmondo/common/ui/b;-><init>()V

    iput-object p1, p0, Lco/uk/getmondo/signup/tax_residency/b/n;->d:Lio/reactivex/u;

    iput-object p2, p0, Lco/uk/getmondo/signup/tax_residency/b/n;->e:Lio/reactivex/u;

    iput-object p3, p0, Lco/uk/getmondo/signup/tax_residency/b/n;->f:Lco/uk/getmondo/signup/tax_residency/a/c;

    iput-object p4, p0, Lco/uk/getmondo/signup/tax_residency/b/n;->g:Lco/uk/getmondo/common/e/a;

    iput-object p5, p0, Lco/uk/getmondo/signup/tax_residency/b/n;->h:Lco/uk/getmondo/common/a;

    return-void
.end method

.method private final a(Lco/uk/getmondo/api/model/tax_residency/Jurisdiction$TinType;Lco/uk/getmondo/api/model/tax_residency/Jurisdiction;)Lco/uk/getmondo/api/model/tax_residency/Jurisdiction$TinType;
    .locals 1

    .prologue
    .line 105
    invoke-virtual {p2}, Lco/uk/getmondo/api/model/tax_residency/Jurisdiction;->c()Lco/uk/getmondo/api/model/tax_residency/Jurisdiction$TinType;

    move-result-object v0

    invoke-static {p1, v0}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p2}, Lco/uk/getmondo/api/model/tax_residency/Jurisdiction;->d()Lco/uk/getmondo/api/model/tax_residency/Jurisdiction$TinType;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p2}, Lco/uk/getmondo/api/model/tax_residency/Jurisdiction;->c()Lco/uk/getmondo/api/model/tax_residency/Jurisdiction$TinType;

    move-result-object v0

    goto :goto_0
.end method

.method public static final synthetic a(Lco/uk/getmondo/signup/tax_residency/b/n;)Lco/uk/getmondo/api/model/tax_residency/Jurisdiction$TinType;
    .locals 2

    .prologue
    .line 24
    iget-object v0, p0, Lco/uk/getmondo/signup/tax_residency/b/n;->c:Lco/uk/getmondo/api/model/tax_residency/Jurisdiction$TinType;

    if-nez v0, :cond_0

    const-string v1, "currentTinType"

    invoke-static {v1}, Lkotlin/d/b/l;->b(Ljava/lang/String;)V

    :cond_0
    return-object v0
.end method

.method public static final synthetic a(Lco/uk/getmondo/signup/tax_residency/b/n;Lco/uk/getmondo/api/model/tax_residency/Jurisdiction$TinType;Lco/uk/getmondo/api/model/tax_residency/Jurisdiction;)Lco/uk/getmondo/api/model/tax_residency/Jurisdiction$TinType;
    .locals 1

    .prologue
    .line 24
    invoke-direct {p0, p1, p2}, Lco/uk/getmondo/signup/tax_residency/b/n;->a(Lco/uk/getmondo/api/model/tax_residency/Jurisdiction$TinType;Lco/uk/getmondo/api/model/tax_residency/Jurisdiction;)Lco/uk/getmondo/api/model/tax_residency/Jurisdiction$TinType;

    move-result-object v0

    return-object v0
.end method

.method private final a(Lco/uk/getmondo/api/model/tax_residency/Jurisdiction$TinType;Lco/uk/getmondo/api/model/tax_residency/Jurisdiction;Lco/uk/getmondo/signup/tax_residency/b/n$a;)V
    .locals 1

    .prologue
    .line 113
    invoke-interface {p3, p1}, Lco/uk/getmondo/signup/tax_residency/b/n$a;->a(Lco/uk/getmondo/api/model/tax_residency/Jurisdiction$TinType;)V

    .line 114
    invoke-direct {p0, p1, p2}, Lco/uk/getmondo/signup/tax_residency/b/n;->a(Lco/uk/getmondo/api/model/tax_residency/Jurisdiction$TinType;Lco/uk/getmondo/api/model/tax_residency/Jurisdiction;)Lco/uk/getmondo/api/model/tax_residency/Jurisdiction$TinType;

    move-result-object v0

    .line 115
    if-eqz v0, :cond_0

    invoke-interface {p3, v0}, Lco/uk/getmondo/signup/tax_residency/b/n$a;->b(Lco/uk/getmondo/api/model/tax_residency/Jurisdiction$TinType;)V

    .line 116
    :goto_0
    return-void

    .line 115
    :cond_0
    invoke-interface {p3}, Lco/uk/getmondo/signup/tax_residency/b/n$a;->f()V

    goto :goto_0
.end method

.method private final a(Lco/uk/getmondo/api/model/tax_residency/Jurisdiction;Lco/uk/getmondo/signup/tax_residency/b/n$a;)V
    .locals 2

    .prologue
    .line 90
    invoke-virtual {p1}, Lco/uk/getmondo/api/model/tax_residency/Jurisdiction;->c()Lco/uk/getmondo/api/model/tax_residency/Jurisdiction$TinType;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/signup/tax_residency/b/n;->c:Lco/uk/getmondo/api/model/tax_residency/Jurisdiction$TinType;

    .line 92
    iget-object v0, p0, Lco/uk/getmondo/signup/tax_residency/b/n;->c:Lco/uk/getmondo/api/model/tax_residency/Jurisdiction$TinType;

    if-nez v0, :cond_0

    const-string v1, "currentTinType"

    invoke-static {v1}, Lkotlin/d/b/l;->b(Ljava/lang/String;)V

    :cond_0
    invoke-direct {p0, v0, p1, p2}, Lco/uk/getmondo/signup/tax_residency/b/n;->a(Lco/uk/getmondo/api/model/tax_residency/Jurisdiction$TinType;Lco/uk/getmondo/api/model/tax_residency/Jurisdiction;Lco/uk/getmondo/signup/tax_residency/b/n$a;)V

    .line 94
    invoke-virtual {p1}, Lco/uk/getmondo/api/model/tax_residency/Jurisdiction;->h()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 95
    invoke-virtual {p1}, Lco/uk/getmondo/api/model/tax_residency/Jurisdiction;->b()Z

    move-result v0

    invoke-interface {p2, v0}, Lco/uk/getmondo/signup/tax_residency/b/n$a;->a(Z)V

    .line 98
    :goto_0
    return-void

    .line 97
    :cond_1
    invoke-interface {p2}, Lco/uk/getmondo/signup/tax_residency/b/n$a;->g()V

    goto :goto_0
.end method

.method public static final synthetic a(Lco/uk/getmondo/signup/tax_residency/b/n;Lco/uk/getmondo/api/model/tax_residency/Jurisdiction$TinType;)V
    .locals 0

    .prologue
    .line 24
    iput-object p1, p0, Lco/uk/getmondo/signup/tax_residency/b/n;->c:Lco/uk/getmondo/api/model/tax_residency/Jurisdiction$TinType;

    return-void
.end method

.method public static final synthetic a(Lco/uk/getmondo/signup/tax_residency/b/n;Lco/uk/getmondo/api/model/tax_residency/Jurisdiction$TinType;Lco/uk/getmondo/api/model/tax_residency/Jurisdiction;Lco/uk/getmondo/signup/tax_residency/b/n$a;)V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0, p1, p2, p3}, Lco/uk/getmondo/signup/tax_residency/b/n;->a(Lco/uk/getmondo/api/model/tax_residency/Jurisdiction$TinType;Lco/uk/getmondo/api/model/tax_residency/Jurisdiction;Lco/uk/getmondo/signup/tax_residency/b/n$a;)V

    return-void
.end method

.method public static final synthetic b(Lco/uk/getmondo/signup/tax_residency/b/n;)Lco/uk/getmondo/signup/tax_residency/a/c;
    .locals 1

    .prologue
    .line 24
    iget-object v0, p0, Lco/uk/getmondo/signup/tax_residency/b/n;->f:Lco/uk/getmondo/signup/tax_residency/a/c;

    return-object v0
.end method

.method public static final synthetic c(Lco/uk/getmondo/signup/tax_residency/b/n;)Lio/reactivex/u;
    .locals 1

    .prologue
    .line 24
    iget-object v0, p0, Lco/uk/getmondo/signup/tax_residency/b/n;->d:Lio/reactivex/u;

    return-object v0
.end method

.method public static final synthetic d(Lco/uk/getmondo/signup/tax_residency/b/n;)Lio/reactivex/u;
    .locals 1

    .prologue
    .line 24
    iget-object v0, p0, Lco/uk/getmondo/signup/tax_residency/b/n;->e:Lio/reactivex/u;

    return-object v0
.end method

.method public static final synthetic e(Lco/uk/getmondo/signup/tax_residency/b/n;)Lco/uk/getmondo/common/e/a;
    .locals 1

    .prologue
    .line 24
    iget-object v0, p0, Lco/uk/getmondo/signup/tax_residency/b/n;->g:Lco/uk/getmondo/common/e/a;

    return-object v0
.end method


# virtual methods
.method public bridge synthetic a(Lco/uk/getmondo/common/ui/f;)V
    .locals 0

    .prologue
    .line 24
    check-cast p1, Lco/uk/getmondo/signup/tax_residency/b/n$a;

    invoke-virtual {p0, p1}, Lco/uk/getmondo/signup/tax_residency/b/n;->a(Lco/uk/getmondo/signup/tax_residency/b/n$a;)V

    return-void
.end method

.method public a(Lco/uk/getmondo/signup/tax_residency/b/n$a;)V
    .locals 6

    .prologue
    const-string v0, "view"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v0, p1

    .line 36
    check-cast v0, Lco/uk/getmondo/common/ui/f;

    invoke-super {p0, v0}, Lco/uk/getmondo/common/ui/b;->a(Lco/uk/getmondo/common/ui/f;)V

    .line 38
    iget-object v0, p0, Lco/uk/getmondo/signup/tax_residency/b/n;->h:Lco/uk/getmondo/common/a;

    sget-object v1, Lco/uk/getmondo/api/model/tracking/Impression;->Companion:Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    invoke-virtual {v1}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->aG()Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v1

    invoke-virtual {v0, v1}, Lco/uk/getmondo/common/a;->a(Lco/uk/getmondo/api/model/tracking/Impression;)V

    .line 39
    invoke-interface {p1}, Lco/uk/getmondo/signup/tax_residency/b/n$a;->a()Lco/uk/getmondo/api/model/tax_residency/Jurisdiction;

    move-result-object v3

    .line 40
    invoke-direct {p0, v3, p1}, Lco/uk/getmondo/signup/tax_residency/b/n;->a(Lco/uk/getmondo/api/model/tax_residency/Jurisdiction;Lco/uk/getmondo/signup/tax_residency/b/n$a;)V

    .line 42
    iget-object v4, p0, Lco/uk/getmondo/signup/tax_residency/b/n;->b:Lio/reactivex/b/a;

    .line 43
    invoke-interface {p1}, Lco/uk/getmondo/signup/tax_residency/b/n$a;->c()Lio/reactivex/n;

    move-result-object v5

    new-instance v0, Lco/uk/getmondo/signup/tax_residency/b/n$b;

    invoke-direct {v0, p1}, Lco/uk/getmondo/signup/tax_residency/b/n$b;-><init>(Lco/uk/getmondo/signup/tax_residency/b/n$a;)V

    check-cast v0, Lio/reactivex/c/g;

    .line 46
    sget-object v1, Lco/uk/getmondo/signup/tax_residency/b/n$e;->a:Lco/uk/getmondo/signup/tax_residency/b/n$e;

    check-cast v1, Lkotlin/d/a/b;

    if-eqz v1, :cond_0

    new-instance v2, Lco/uk/getmondo/signup/tax_residency/b/o;

    invoke-direct {v2, v1}, Lco/uk/getmondo/signup/tax_residency/b/o;-><init>(Lkotlin/d/a/b;)V

    move-object v1, v2

    :cond_0
    check-cast v1, Lio/reactivex/c/g;

    .line 43
    invoke-virtual {v5, v0, v1}, Lio/reactivex/n;->subscribe(Lio/reactivex/c/g;Lio/reactivex/c/g;)Lio/reactivex/b/b;

    move-result-object v0

    const-string v1, "view.onTinChanges\n      \u2026            }, Timber::e)"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 42
    invoke-static {v4, v0}, Lco/uk/getmondo/common/j/f;->a(Lio/reactivex/b/a;Lio/reactivex/b/b;)Lio/reactivex/b/a;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/signup/tax_residency/b/n;->b:Lio/reactivex/b/a;

    .line 48
    iget-object v4, p0, Lco/uk/getmondo/signup/tax_residency/b/n;->b:Lio/reactivex/b/a;

    .line 50
    invoke-interface {p1}, Lco/uk/getmondo/signup/tax_residency/b/n$a;->b()Lio/reactivex/n;

    move-result-object v1

    .line 49
    new-instance v0, Lco/uk/getmondo/signup/tax_residency/b/n$f;

    invoke-direct {v0, p0, v3}, Lco/uk/getmondo/signup/tax_residency/b/n$f;-><init>(Lco/uk/getmondo/signup/tax_residency/b/n;Lco/uk/getmondo/api/model/tax_residency/Jurisdiction;)V

    check-cast v0, Lio/reactivex/c/g;

    invoke-virtual {v1, v0}, Lio/reactivex/n;->doOnNext(Lio/reactivex/c/g;)Lio/reactivex/n;

    move-result-object v5

    .line 50
    new-instance v0, Lco/uk/getmondo/signup/tax_residency/b/n$g;

    invoke-direct {v0, p0, v3, p1}, Lco/uk/getmondo/signup/tax_residency/b/n$g;-><init>(Lco/uk/getmondo/signup/tax_residency/b/n;Lco/uk/getmondo/api/model/tax_residency/Jurisdiction;Lco/uk/getmondo/signup/tax_residency/b/n$a;)V

    check-cast v0, Lio/reactivex/c/g;

    sget-object v1, Lco/uk/getmondo/signup/tax_residency/b/n$h;->a:Lco/uk/getmondo/signup/tax_residency/b/n$h;

    check-cast v1, Lkotlin/d/a/b;

    if-eqz v1, :cond_1

    new-instance v2, Lco/uk/getmondo/signup/tax_residency/b/o;

    invoke-direct {v2, v1}, Lco/uk/getmondo/signup/tax_residency/b/o;-><init>(Lkotlin/d/a/b;)V

    move-object v1, v2

    :cond_1
    check-cast v1, Lio/reactivex/c/g;

    invoke-virtual {v5, v0, v1}, Lio/reactivex/n;->subscribe(Lio/reactivex/c/g;Lio/reactivex/c/g;)Lio/reactivex/b/b;

    move-result-object v0

    const-string v1, "view.onAltTinClicked\n   \u2026tion, view) }, Timber::e)"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 48
    invoke-static {v4, v0}, Lco/uk/getmondo/common/j/f;->a(Lio/reactivex/b/a;Lio/reactivex/b/b;)Lio/reactivex/b/a;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/signup/tax_residency/b/n;->b:Lio/reactivex/b/a;

    .line 52
    iget-object v4, p0, Lco/uk/getmondo/signup/tax_residency/b/n;->b:Lio/reactivex/b/a;

    .line 80
    invoke-interface {p1}, Lco/uk/getmondo/signup/tax_residency/b/n$a;->d()Lio/reactivex/n;

    move-result-object v1

    .line 53
    invoke-interface {p1}, Lco/uk/getmondo/signup/tax_residency/b/n$a;->e()Lio/reactivex/n;

    move-result-object v2

    sget-object v0, Lco/uk/getmondo/signup/tax_residency/b/n$i;->a:Lco/uk/getmondo/signup/tax_residency/b/n$i;

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {v2, v0}, Lio/reactivex/n;->map(Lio/reactivex/c/h;)Lio/reactivex/n;

    move-result-object v0

    check-cast v0, Lio/reactivex/r;

    invoke-virtual {v1, v0}, Lio/reactivex/n;->mergeWith(Lio/reactivex/r;)Lio/reactivex/n;

    move-result-object v1

    .line 54
    sget-object v0, Lco/uk/getmondo/signup/tax_residency/b/n$j;->a:Lco/uk/getmondo/signup/tax_residency/b/n$j;

    check-cast v0, Lio/reactivex/c/q;

    invoke-virtual {v1, v0}, Lio/reactivex/n;->filter(Lio/reactivex/c/q;)Lio/reactivex/n;

    move-result-object v1

    .line 55
    sget-object v0, Lco/uk/getmondo/signup/tax_residency/b/n$k;->a:Lco/uk/getmondo/signup/tax_residency/b/n$k;

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {v1, v0}, Lio/reactivex/n;->map(Lio/reactivex/c/h;)Lio/reactivex/n;

    move-result-object v1

    .line 56
    new-instance v0, Lco/uk/getmondo/signup/tax_residency/b/n$l;

    invoke-direct {v0, p0, v3, p1}, Lco/uk/getmondo/signup/tax_residency/b/n$l;-><init>(Lco/uk/getmondo/signup/tax_residency/b/n;Lco/uk/getmondo/api/model/tax_residency/Jurisdiction;Lco/uk/getmondo/signup/tax_residency/b/n$a;)V

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {v1, v0}, Lio/reactivex/n;->flatMapMaybe(Lio/reactivex/c/h;)Lio/reactivex/n;

    move-result-object v3

    .line 80
    new-instance v0, Lco/uk/getmondo/signup/tax_residency/b/n$c;

    invoke-direct {v0, p1}, Lco/uk/getmondo/signup/tax_residency/b/n$c;-><init>(Lco/uk/getmondo/signup/tax_residency/b/n$a;)V

    check-cast v0, Lio/reactivex/c/g;

    .line 86
    sget-object v1, Lco/uk/getmondo/signup/tax_residency/b/n$d;->a:Lco/uk/getmondo/signup/tax_residency/b/n$d;

    check-cast v1, Lkotlin/d/a/b;

    if-eqz v1, :cond_2

    new-instance v2, Lco/uk/getmondo/signup/tax_residency/b/o;

    invoke-direct {v2, v1}, Lco/uk/getmondo/signup/tax_residency/b/o;-><init>(Lkotlin/d/a/b;)V

    move-object v1, v2

    :cond_2
    check-cast v1, Lio/reactivex/c/g;

    .line 80
    invoke-virtual {v3, v0, v1}, Lio/reactivex/n;->subscribe(Lio/reactivex/c/g;Lio/reactivex/c/g;)Lio/reactivex/b/b;

    move-result-object v0

    const-string v1, "view.onContinueClicked\n \u2026            }, Timber::e)"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 52
    invoke-static {v4, v0}, Lco/uk/getmondo/common/j/f;->a(Lio/reactivex/b/a;Lio/reactivex/b/b;)Lio/reactivex/b/a;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/signup/tax_residency/b/n;->b:Lio/reactivex/b/a;

    .line 88
    return-void
.end method
