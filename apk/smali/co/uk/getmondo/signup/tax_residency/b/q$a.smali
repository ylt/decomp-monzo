.class public interface abstract Lco/uk/getmondo/signup/tax_residency/b/q$a;
.super Ljava/lang/Object;
.source "TaxResidencyUsResidentFragment.kt"

# interfaces
.implements Lco/uk/getmondo/signup/tax_residency/f;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lco/uk/getmondo/signup/tax_residency/b/q;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "a"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0008f\u0018\u00002\u00020\u0001J(\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u00052\u0016\u0010\u0006\u001a\u0012\u0012\u0004\u0012\u00020\u00080\u0007j\u0008\u0012\u0004\u0012\u00020\u0008`\tH&\u00a8\u0006\n"
    }
    d2 = {
        "Lco/uk/getmondo/signup/tax_residency/step/TaxResidencyUsResidentFragment$StepListener;",
        "Lco/uk/getmondo/signup/tax_residency/TaxResidencyStageListener;",
        "onUsResidentStepCompleted",
        "",
        "isUsTaxResident",
        "",
        "taxCountries",
        "Ljava/util/ArrayList;",
        "Lco/uk/getmondo/signup/tax_residency/data/TaxCountry;",
        "Lkotlin/collections/ArrayList;",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# virtual methods
.method public abstract a(ZLjava/util/ArrayList;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "Ljava/util/ArrayList",
            "<",
            "Lco/uk/getmondo/signup/tax_residency/a/a;",
            ">;)V"
        }
    .end annotation
.end method
