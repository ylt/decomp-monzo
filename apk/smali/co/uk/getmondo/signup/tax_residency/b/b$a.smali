.class public final Lco/uk/getmondo/signup/tax_residency/b/b$a;
.super Ljava/lang/Object;
.source "TaxResidencySelectionFragment.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lco/uk/getmondo/signup/tax_residency/b/b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000*\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0008\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u001c\u0010\u0006\u001a\u00020\u00072\u0006\u0010\u0008\u001a\u00020\t2\u000c\u0010\n\u001a\u0008\u0012\u0004\u0012\u00020\u000c0\u000bR\u000e\u0010\u0003\u001a\u00020\u0004X\u0082T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0082T\u00a2\u0006\u0002\n\u0000\u00a8\u0006\r"
    }
    d2 = {
        "Lco/uk/getmondo/signup/tax_residency/step/TaxResidencySelectionFragment$Companion;",
        "",
        "()V",
        "ARG_TAX_COUNTRIES",
        "",
        "ARG_US_TAX_RESIDENT",
        "newInstance",
        "Lco/uk/getmondo/signup/tax_residency/step/TaxResidencySelectionFragment;",
        "isUsTaxResident",
        "",
        "taxCountries",
        "Ljava/util/ArrayList;",
        "Lco/uk/getmondo/signup/tax_residency/data/TaxCountry;",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 96
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/d/b/i;)V
    .locals 0

    .prologue
    .line 96
    invoke-direct {p0}, Lco/uk/getmondo/signup/tax_residency/b/b$a;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(ZLjava/util/ArrayList;)Lco/uk/getmondo/signup/tax_residency/b/b;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "Ljava/util/ArrayList",
            "<",
            "Lco/uk/getmondo/signup/tax_residency/a/a;",
            ">;)",
            "Lco/uk/getmondo/signup/tax_residency/b/b;"
        }
    .end annotation

    .prologue
    const-string v0, "taxCountries"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 102
    new-instance v0, Lco/uk/getmondo/signup/tax_residency/b/b;

    invoke-direct {v0}, Lco/uk/getmondo/signup/tax_residency/b/b;-><init>()V

    .line 103
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 104
    const-string v2, "ARG_US_TAX_RESIDENT"

    invoke-virtual {v1, v2, p1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 105
    const-string v2, "ARG_TAX_COUNTRIES"

    invoke-virtual {v1, v2, p2}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 106
    invoke-virtual {v0, v1}, Lco/uk/getmondo/signup/tax_residency/b/b;->setArguments(Landroid/os/Bundle;)V

    .line 107
    return-object v0
.end method
