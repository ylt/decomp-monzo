.class final Lco/uk/getmondo/signup/tax_residency/b/n$l$2;
.super Ljava/lang/Object;
.source "TaxResidencyTinEntryPresenter.kt"

# interfaces
.implements Lio/reactivex/c/g;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lco/uk/getmondo/signup/tax_residency/b/n$l;->a(Ljava/lang/String;)Lio/reactivex/h;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/g",
        "<",
        "Ljava/lang/Throwable;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u0003\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "",
        "it",
        "",
        "kotlin.jvm.PlatformType",
        "accept"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field final synthetic a:Lco/uk/getmondo/signup/tax_residency/b/n$l;


# direct methods
.method constructor <init>(Lco/uk/getmondo/signup/tax_residency/b/n$l;)V
    .locals 0

    iput-object p1, p0, Lco/uk/getmondo/signup/tax_residency/b/n$l$2;->a:Lco/uk/getmondo/signup/tax_residency/b/n$l;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 24
    check-cast p1, Ljava/lang/Throwable;

    invoke-virtual {p0, p1}, Lco/uk/getmondo/signup/tax_residency/b/n$l$2;->a(Ljava/lang/Throwable;)V

    return-void
.end method

.method public final a(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 68
    iget-object v0, p0, Lco/uk/getmondo/signup/tax_residency/b/n$l$2;->a:Lco/uk/getmondo/signup/tax_residency/b/n$l;

    iget-object v0, v0, Lco/uk/getmondo/signup/tax_residency/b/n$l;->c:Lco/uk/getmondo/signup/tax_residency/b/n$a;

    invoke-interface {v0, v1}, Lco/uk/getmondo/signup/tax_residency/b/n$a;->c(Z)V

    .line 69
    iget-object v0, p0, Lco/uk/getmondo/signup/tax_residency/b/n$l$2;->a:Lco/uk/getmondo/signup/tax_residency/b/n$l;

    iget-object v0, v0, Lco/uk/getmondo/signup/tax_residency/b/n$l;->c:Lco/uk/getmondo/signup/tax_residency/b/n$a;

    invoke-interface {v0, v1}, Lco/uk/getmondo/signup/tax_residency/b/n$a;->d(Z)V

    .line 70
    instance-of v0, p1, Lco/uk/getmondo/api/ApiException;

    if-eqz v0, :cond_1

    .line 71
    invoke-static {}, Lco/uk/getmondo/signup/tax_residency/a/b;->values()[Lco/uk/getmondo/signup/tax_residency/a/b;

    move-result-object v0

    check-cast v0, [Lco/uk/getmondo/common/e/f;

    move-object v1, p1

    check-cast v1, Lco/uk/getmondo/api/ApiException;

    invoke-virtual {v1}, Lco/uk/getmondo/api/ApiException;->e()Lco/uk/getmondo/api/model/b;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lco/uk/getmondo/api/model/b;->a()Ljava/lang/String;

    move-result-object v1

    :goto_0
    invoke-static {v0, v1}, Lco/uk/getmondo/common/e/d;->a([Lco/uk/getmondo/common/e/f;Ljava/lang/String;)Lco/uk/getmondo/common/e/f;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/signup/tax_residency/a/b;

    .line 72
    sget-object v1, Lco/uk/getmondo/signup/tax_residency/a/b;->a:Lco/uk/getmondo/signup/tax_residency/a/b;

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 73
    iget-object v0, p0, Lco/uk/getmondo/signup/tax_residency/b/n$l$2;->a:Lco/uk/getmondo/signup/tax_residency/b/n$l;

    iget-object v0, v0, Lco/uk/getmondo/signup/tax_residency/b/n$l;->c:Lco/uk/getmondo/signup/tax_residency/b/n$a;

    iget-object v1, p0, Lco/uk/getmondo/signup/tax_residency/b/n$l$2;->a:Lco/uk/getmondo/signup/tax_residency/b/n$l;

    iget-object v1, v1, Lco/uk/getmondo/signup/tax_residency/b/n$l;->a:Lco/uk/getmondo/signup/tax_residency/b/n;

    invoke-static {v1}, Lco/uk/getmondo/signup/tax_residency/b/n;->a(Lco/uk/getmondo/signup/tax_residency/b/n;)Lco/uk/getmondo/api/model/tax_residency/Jurisdiction$TinType;

    move-result-object v1

    invoke-interface {v0, v1}, Lco/uk/getmondo/signup/tax_residency/b/n$a;->c(Lco/uk/getmondo/api/model/tax_residency/Jurisdiction$TinType;)V

    .line 78
    :goto_1
    return-void

    .line 71
    :cond_0
    const/4 v1, 0x0

    goto :goto_0

    .line 77
    :cond_1
    iget-object v0, p0, Lco/uk/getmondo/signup/tax_residency/b/n$l$2;->a:Lco/uk/getmondo/signup/tax_residency/b/n$l;

    iget-object v0, v0, Lco/uk/getmondo/signup/tax_residency/b/n$l;->a:Lco/uk/getmondo/signup/tax_residency/b/n;

    invoke-static {v0}, Lco/uk/getmondo/signup/tax_residency/b/n;->e(Lco/uk/getmondo/signup/tax_residency/b/n;)Lco/uk/getmondo/common/e/a;

    move-result-object v1

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lco/uk/getmondo/signup/tax_residency/b/n$l$2;->a:Lco/uk/getmondo/signup/tax_residency/b/n$l;

    iget-object v0, v0, Lco/uk/getmondo/signup/tax_residency/b/n$l;->c:Lco/uk/getmondo/signup/tax_residency/b/n$a;

    check-cast v0, Lco/uk/getmondo/common/e/a$a;

    invoke-virtual {v1, p1, v0}, Lco/uk/getmondo/common/e/a;->a(Ljava/lang/Throwable;Lco/uk/getmondo/common/e/a$a;)Z

    goto :goto_1
.end method
