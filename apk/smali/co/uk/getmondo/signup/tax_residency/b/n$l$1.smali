.class final Lco/uk/getmondo/signup/tax_residency/b/n$l$1;
.super Ljava/lang/Object;
.source "TaxResidencyTinEntryPresenter.kt"

# interfaces
.implements Lio/reactivex/c/g;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lco/uk/getmondo/signup/tax_residency/b/n$l;->a(Ljava/lang/String;)Lio/reactivex/h;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/g",
        "<",
        "Lio/reactivex/b/b;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "",
        "it",
        "Lio/reactivex/disposables/Disposable;",
        "kotlin.jvm.PlatformType",
        "accept"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field final synthetic a:Lco/uk/getmondo/signup/tax_residency/b/n$l;

.field final synthetic b:Ljava/lang/String;


# direct methods
.method constructor <init>(Lco/uk/getmondo/signup/tax_residency/b/n$l;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lco/uk/getmondo/signup/tax_residency/b/n$l$1;->a:Lco/uk/getmondo/signup/tax_residency/b/n$l;

    iput-object p2, p0, Lco/uk/getmondo/signup/tax_residency/b/n$l$1;->b:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lio/reactivex/b/b;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 62
    iget-object v0, p0, Lco/uk/getmondo/signup/tax_residency/b/n$l$1;->b:Ljava/lang/String;

    const-string v1, "NO_TIN"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    iget-object v0, p0, Lco/uk/getmondo/signup/tax_residency/b/n$l$1;->a:Lco/uk/getmondo/signup/tax_residency/b/n$l;

    iget-object v0, v0, Lco/uk/getmondo/signup/tax_residency/b/n$l;->c:Lco/uk/getmondo/signup/tax_residency/b/n$a;

    invoke-interface {v0, v2}, Lco/uk/getmondo/signup/tax_residency/b/n$a;->c(Z)V

    .line 65
    :goto_0
    return-void

    .line 64
    :cond_0
    iget-object v0, p0, Lco/uk/getmondo/signup/tax_residency/b/n$l$1;->a:Lco/uk/getmondo/signup/tax_residency/b/n$l;

    iget-object v0, v0, Lco/uk/getmondo/signup/tax_residency/b/n$l;->c:Lco/uk/getmondo/signup/tax_residency/b/n$a;

    invoke-interface {v0, v2}, Lco/uk/getmondo/signup/tax_residency/b/n$a;->d(Z)V

    goto :goto_0
.end method

.method public bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 24
    check-cast p1, Lio/reactivex/b/b;

    invoke-virtual {p0, p1}, Lco/uk/getmondo/signup/tax_residency/b/n$l$1;->a(Lio/reactivex/b/b;)V

    return-void
.end method
