.class final Lco/uk/getmondo/signup/tax_residency/b/s$b;
.super Ljava/lang/Object;
.source "TaxResidencyUsResidentPresenter.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lco/uk/getmondo/signup/tax_residency/b/s;->a(Lco/uk/getmondo/signup/tax_residency/b/s$a;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h",
        "<TT;",
        "Lio/reactivex/l",
        "<+TR;>;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000\u001e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0004\u0010\u0000\u001aH\u0012D\u0012B\u0012\u0004\u0012\u00020\u0003\u0012\u0014\u0012\u0012\u0012\u0004\u0012\u00020\u00050\u0004j\u0008\u0012\u0004\u0012\u00020\u0005`\u0006 \u0007* \u0012\u0004\u0012\u00020\u0003\u0012\u0014\u0012\u0012\u0012\u0004\u0012\u00020\u00050\u0004j\u0008\u0012\u0004\u0012\u00020\u0005`\u0006\u0018\u00010\u00020\u00020\u00012\u0006\u0010\u0008\u001a\u00020\u0003H\n\u00a2\u0006\u0004\u0008\t\u0010\n"
    }
    d2 = {
        "<anonymous>",
        "Lio/reactivex/Maybe;",
        "Lkotlin/Pair;",
        "",
        "Ljava/util/ArrayList;",
        "Lco/uk/getmondo/signup/tax_residency/data/TaxCountry;",
        "Lkotlin/collections/ArrayList;",
        "kotlin.jvm.PlatformType",
        "isUsTaxResident",
        "apply",
        "(Ljava/lang/Boolean;)Lio/reactivex/Maybe;"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field final synthetic a:Lco/uk/getmondo/signup/tax_residency/b/s;

.field final synthetic b:Lco/uk/getmondo/signup/tax_residency/b/s$a;


# direct methods
.method constructor <init>(Lco/uk/getmondo/signup/tax_residency/b/s;Lco/uk/getmondo/signup/tax_residency/b/s$a;)V
    .locals 0

    iput-object p1, p0, Lco/uk/getmondo/signup/tax_residency/b/s$b;->a:Lco/uk/getmondo/signup/tax_residency/b/s;

    iput-object p2, p0, Lco/uk/getmondo/signup/tax_residency/b/s$b;->b:Lco/uk/getmondo/signup/tax_residency/b/s$a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Boolean;)Lio/reactivex/h;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Boolean;",
            ")",
            "Lio/reactivex/h",
            "<",
            "Lkotlin/h",
            "<",
            "Ljava/lang/Boolean;",
            "Ljava/util/ArrayList",
            "<",
            "Lco/uk/getmondo/signup/tax_residency/a/a;",
            ">;>;>;"
        }
    .end annotation

    .prologue
    const-string v0, "isUsTaxResident"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 39
    iget-object v0, p0, Lco/uk/getmondo/signup/tax_residency/b/s$b;->a:Lco/uk/getmondo/signup/tax_residency/b/s;

    invoke-static {v0}, Lco/uk/getmondo/signup/tax_residency/b/s;->a(Lco/uk/getmondo/signup/tax_residency/b/s;)Lco/uk/getmondo/signup/tax_residency/a/c;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {v0, v1}, Lco/uk/getmondo/signup/tax_residency/a/c;->a(Z)Lio/reactivex/v;

    move-result-object v1

    .line 40
    new-instance v0, Lco/uk/getmondo/signup/tax_residency/b/s$b$1;

    invoke-direct {v0, p1}, Lco/uk/getmondo/signup/tax_residency/b/s$b$1;-><init>(Ljava/lang/Boolean;)V

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {v1, v0}, Lio/reactivex/v;->d(Lio/reactivex/c/h;)Lio/reactivex/v;

    move-result-object v0

    .line 41
    iget-object v1, p0, Lco/uk/getmondo/signup/tax_residency/b/s$b;->a:Lco/uk/getmondo/signup/tax_residency/b/s;

    invoke-static {v1}, Lco/uk/getmondo/signup/tax_residency/b/s;->b(Lco/uk/getmondo/signup/tax_residency/b/s;)Lio/reactivex/u;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/v;->b(Lio/reactivex/u;)Lio/reactivex/v;

    move-result-object v0

    .line 42
    iget-object v1, p0, Lco/uk/getmondo/signup/tax_residency/b/s$b;->a:Lco/uk/getmondo/signup/tax_residency/b/s;

    invoke-static {v1}, Lco/uk/getmondo/signup/tax_residency/b/s;->c(Lco/uk/getmondo/signup/tax_residency/b/s;)Lio/reactivex/u;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/v;->a(Lio/reactivex/u;)Lio/reactivex/v;

    move-result-object v1

    .line 43
    new-instance v0, Lco/uk/getmondo/signup/tax_residency/b/s$b$2;

    invoke-direct {v0, p0, p1}, Lco/uk/getmondo/signup/tax_residency/b/s$b$2;-><init>(Lco/uk/getmondo/signup/tax_residency/b/s$b;Ljava/lang/Boolean;)V

    check-cast v0, Lio/reactivex/c/g;

    invoke-virtual {v1, v0}, Lio/reactivex/v;->b(Lio/reactivex/c/g;)Lio/reactivex/v;

    move-result-object v1

    .line 44
    new-instance v0, Lco/uk/getmondo/signup/tax_residency/b/s$b$3;

    invoke-direct {v0, p0, p1}, Lco/uk/getmondo/signup/tax_residency/b/s$b$3;-><init>(Lco/uk/getmondo/signup/tax_residency/b/s$b;Ljava/lang/Boolean;)V

    check-cast v0, Lio/reactivex/c/g;

    invoke-virtual {v1, v0}, Lio/reactivex/v;->d(Lio/reactivex/c/g;)Lio/reactivex/v;

    move-result-object v0

    .line 50
    invoke-static {v0}, Lco/uk/getmondo/common/j/f;->a(Lio/reactivex/v;)Lio/reactivex/h;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 22
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Lco/uk/getmondo/signup/tax_residency/b/s$b;->a(Ljava/lang/Boolean;)Lio/reactivex/h;

    move-result-object v0

    return-object v0
.end method
