.class final Lco/uk/getmondo/signup/tax_residency/b/n$l;
.super Ljava/lang/Object;
.source "TaxResidencyTinEntryPresenter.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lco/uk/getmondo/signup/tax_residency/b/n;->a(Lco/uk/getmondo/signup/tax_residency/b/n$a;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h",
        "<TT;",
        "Lio/reactivex/l",
        "<+TR;>;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000\u0018\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0000\u0010\u0000\u001a\u001c\u0012\u0018\u0012\u0016\u0012\u0004\u0012\u00020\u0003 \u0004*\n\u0012\u0004\u0012\u00020\u0003\u0018\u00010\u00020\u00020\u00012\u0006\u0010\u0005\u001a\u00020\u0006H\n\u00a2\u0006\u0002\u0008\u0007"
    }
    d2 = {
        "<anonymous>",
        "Lio/reactivex/Maybe;",
        "Lcom/memoizrlabs/poweroptional/Optional;",
        "Lco/uk/getmondo/api/model/tax_residency/Jurisdiction;",
        "kotlin.jvm.PlatformType",
        "tinValue",
        "",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field final synthetic a:Lco/uk/getmondo/signup/tax_residency/b/n;

.field final synthetic b:Lco/uk/getmondo/api/model/tax_residency/Jurisdiction;

.field final synthetic c:Lco/uk/getmondo/signup/tax_residency/b/n$a;


# direct methods
.method constructor <init>(Lco/uk/getmondo/signup/tax_residency/b/n;Lco/uk/getmondo/api/model/tax_residency/Jurisdiction;Lco/uk/getmondo/signup/tax_residency/b/n$a;)V
    .locals 0

    iput-object p1, p0, Lco/uk/getmondo/signup/tax_residency/b/n$l;->a:Lco/uk/getmondo/signup/tax_residency/b/n;

    iput-object p2, p0, Lco/uk/getmondo/signup/tax_residency/b/n$l;->b:Lco/uk/getmondo/api/model/tax_residency/Jurisdiction;

    iput-object p3, p0, Lco/uk/getmondo/signup/tax_residency/b/n$l;->c:Lco/uk/getmondo/signup/tax_residency/b/n$a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Lio/reactivex/h;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/h",
            "<",
            "Lcom/c/b/b",
            "<",
            "Lco/uk/getmondo/api/model/tax_residency/Jurisdiction;",
            ">;>;"
        }
    .end annotation

    .prologue
    const-string v0, "tinValue"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 57
    iget-object v0, p0, Lco/uk/getmondo/signup/tax_residency/b/n$l;->a:Lco/uk/getmondo/signup/tax_residency/b/n;

    invoke-static {v0}, Lco/uk/getmondo/signup/tax_residency/b/n;->b(Lco/uk/getmondo/signup/tax_residency/b/n;)Lco/uk/getmondo/signup/tax_residency/a/c;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/signup/tax_residency/b/n$l;->b:Lco/uk/getmondo/api/model/tax_residency/Jurisdiction;

    iget-object v2, p0, Lco/uk/getmondo/signup/tax_residency/b/n$l;->a:Lco/uk/getmondo/signup/tax_residency/b/n;

    invoke-static {v2}, Lco/uk/getmondo/signup/tax_residency/b/n;->a(Lco/uk/getmondo/signup/tax_residency/b/n;)Lco/uk/getmondo/api/model/tax_residency/Jurisdiction$TinType;

    move-result-object v2

    invoke-virtual {v2}, Lco/uk/getmondo/api/model/tax_residency/Jurisdiction$TinType;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2, p1}, Lco/uk/getmondo/signup/tax_residency/a/c;->a(Lco/uk/getmondo/api/model/tax_residency/Jurisdiction;Ljava/lang/String;Ljava/lang/String;)Lio/reactivex/b;

    move-result-object v1

    .line 58
    iget-object v0, p0, Lco/uk/getmondo/signup/tax_residency/b/n$l;->a:Lco/uk/getmondo/signup/tax_residency/b/n;

    invoke-static {v0}, Lco/uk/getmondo/signup/tax_residency/b/n;->b(Lco/uk/getmondo/signup/tax_residency/b/n;)Lco/uk/getmondo/signup/tax_residency/a/c;

    move-result-object v0

    invoke-virtual {v0}, Lco/uk/getmondo/signup/tax_residency/a/c;->b()Lio/reactivex/v;

    move-result-object v0

    check-cast v0, Lio/reactivex/z;

    invoke-virtual {v1, v0}, Lio/reactivex/b;->a(Lio/reactivex/z;)Lio/reactivex/v;

    move-result-object v0

    .line 59
    iget-object v1, p0, Lco/uk/getmondo/signup/tax_residency/b/n$l;->a:Lco/uk/getmondo/signup/tax_residency/b/n;

    invoke-static {v1}, Lco/uk/getmondo/signup/tax_residency/b/n;->c(Lco/uk/getmondo/signup/tax_residency/b/n;)Lio/reactivex/u;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/v;->b(Lio/reactivex/u;)Lio/reactivex/v;

    move-result-object v0

    .line 60
    iget-object v1, p0, Lco/uk/getmondo/signup/tax_residency/b/n$l;->a:Lco/uk/getmondo/signup/tax_residency/b/n;

    invoke-static {v1}, Lco/uk/getmondo/signup/tax_residency/b/n;->d(Lco/uk/getmondo/signup/tax_residency/b/n;)Lio/reactivex/u;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/v;->a(Lio/reactivex/u;)Lio/reactivex/v;

    move-result-object v1

    .line 61
    new-instance v0, Lco/uk/getmondo/signup/tax_residency/b/n$l$1;

    invoke-direct {v0, p0, p1}, Lco/uk/getmondo/signup/tax_residency/b/n$l$1;-><init>(Lco/uk/getmondo/signup/tax_residency/b/n$l;Ljava/lang/String;)V

    check-cast v0, Lio/reactivex/c/g;

    invoke-virtual {v1, v0}, Lio/reactivex/v;->b(Lio/reactivex/c/g;)Lio/reactivex/v;

    move-result-object v1

    .line 67
    new-instance v0, Lco/uk/getmondo/signup/tax_residency/b/n$l$2;

    invoke-direct {v0, p0}, Lco/uk/getmondo/signup/tax_residency/b/n$l$2;-><init>(Lco/uk/getmondo/signup/tax_residency/b/n$l;)V

    check-cast v0, Lio/reactivex/c/g;

    invoke-virtual {v1, v0}, Lio/reactivex/v;->d(Lio/reactivex/c/g;)Lio/reactivex/v;

    move-result-object v0

    .line 78
    invoke-static {v0}, Lco/uk/getmondo/common/j/f;->a(Lio/reactivex/v;)Lio/reactivex/h;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 24
    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1}, Lco/uk/getmondo/signup/tax_residency/b/n$l;->a(Ljava/lang/String;)Lio/reactivex/h;

    move-result-object v0

    return-object v0
.end method
