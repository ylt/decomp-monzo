.class public interface abstract Lco/uk/getmondo/signup/tax_residency/b/n$a;
.super Ljava/lang/Object;
.source "TaxResidencyTinEntryPresenter.kt"

# interfaces
.implements Lco/uk/getmondo/common/e/a$a;
.implements Lco/uk/getmondo/common/ui/f;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lco/uk/getmondo/signup/tax_residency/b/n;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "a"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000B\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u000e\n\u0002\u0008\u0004\n\u0002\u0010\r\n\u0002\u0008\t\n\u0002\u0010\u000b\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0008\u0006\u0008f\u0018\u00002\u00020\u00012\u00020\u0002J\u0008\u0010\u0014\u001a\u00020\tH&J\u0010\u0010\u0015\u001a\u00020\t2\u0006\u0010\u0016\u001a\u00020\u0004H&J\u0008\u0010\u0017\u001a\u00020\tH&J\u0008\u0010\u0018\u001a\u00020\tH&J\u0008\u0010\u0019\u001a\u00020\tH&J\u0010\u0010\u001a\u001a\u00020\t2\u0006\u0010\u001b\u001a\u00020\u001cH&J\u0010\u0010\u001d\u001a\u00020\t2\u0006\u0010\u001e\u001a\u00020\u001cH&J\u0010\u0010\u001f\u001a\u00020\t2\u0006\u0010\u001e\u001a\u00020\u001cH&J\u0010\u0010 \u001a\u00020\t2\u0006\u0010!\u001a\u00020\"H&J\u0010\u0010#\u001a\u00020\t2\u0006\u0010$\u001a\u00020\"H&J\u0010\u0010%\u001a\u00020\t2\u0006\u0010&\u001a\u00020\u001cH&J\u0010\u0010\'\u001a\u00020\t2\u0006\u0010$\u001a\u00020\"H&R\u0012\u0010\u0003\u001a\u00020\u0004X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0005\u0010\u0006R\u0018\u0010\u0007\u001a\u0008\u0012\u0004\u0012\u00020\t0\u0008X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\n\u0010\u000bR\u0018\u0010\u000c\u001a\u0008\u0012\u0004\u0012\u00020\r0\u0008X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u000e\u0010\u000bR\u0018\u0010\u000f\u001a\u0008\u0012\u0004\u0012\u00020\t0\u0008X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0010\u0010\u000bR\u0018\u0010\u0011\u001a\u0008\u0012\u0004\u0012\u00020\u00120\u0008X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0013\u0010\u000b\u00a8\u0006("
    }
    d2 = {
        "Lco/uk/getmondo/signup/tax_residency/step/TaxResidencyTinEntryPresenter$View;",
        "Lco/uk/getmondo/common/ui/MvpView;",
        "Lco/uk/getmondo/common/errors/ApiErrorHandler$ApiErrorView;",
        "jurisdiction",
        "Lco/uk/getmondo/api/model/tax_residency/Jurisdiction;",
        "getJurisdiction",
        "()Lco/uk/getmondo/api/model/tax_residency/Jurisdiction;",
        "onAltTinClicked",
        "Lio/reactivex/Observable;",
        "",
        "getOnAltTinClicked",
        "()Lio/reactivex/Observable;",
        "onContinueClicked",
        "",
        "getOnContinueClicked",
        "onNoTinClicked",
        "getOnNoTinClicked",
        "onTinChanges",
        "",
        "getOnTinChanges",
        "completeStage",
        "goToNextStep",
        "nextJurisdiction",
        "hideAltTinType",
        "hideInvalidTinError",
        "hideNoTin",
        "setContinueButtonEnabled",
        "enabled",
        "",
        "setContinueButtonLoading",
        "loading",
        "setNoTinButtonLoading",
        "showAltTinType",
        "altTinType",
        "Lco/uk/getmondo/api/model/tax_residency/Jurisdiction$TinType;",
        "showInvalidTinError",
        "tinType",
        "showNoTin",
        "isPlural",
        "showTinType",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# virtual methods
.method public abstract a()Lco/uk/getmondo/api/model/tax_residency/Jurisdiction;
.end method

.method public abstract a(Lco/uk/getmondo/api/model/tax_residency/Jurisdiction$TinType;)V
.end method

.method public abstract a(Lco/uk/getmondo/api/model/tax_residency/Jurisdiction;)V
.end method

.method public abstract a(Z)V
.end method

.method public abstract b()Lio/reactivex/n;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Lkotlin/n;",
            ">;"
        }
    .end annotation
.end method

.method public abstract b(Lco/uk/getmondo/api/model/tax_residency/Jurisdiction$TinType;)V
.end method

.method public abstract b(Z)V
.end method

.method public abstract c()Lio/reactivex/n;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Ljava/lang/CharSequence;",
            ">;"
        }
    .end annotation
.end method

.method public abstract c(Lco/uk/getmondo/api/model/tax_residency/Jurisdiction$TinType;)V
.end method

.method public abstract c(Z)V
.end method

.method public abstract d()Lio/reactivex/n;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end method

.method public abstract d(Z)V
.end method

.method public abstract e()Lio/reactivex/n;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Lkotlin/n;",
            ">;"
        }
    .end annotation
.end method

.method public abstract f()V
.end method

.method public abstract g()V
.end method

.method public abstract h()V
.end method

.method public abstract i()V
.end method
