.class final Lco/uk/getmondo/signup/tax_residency/b/n$f;
.super Ljava/lang/Object;
.source "TaxResidencyTinEntryPresenter.kt"

# interfaces
.implements Lio/reactivex/c/g;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lco/uk/getmondo/signup/tax_residency/b/n;->a(Lco/uk/getmondo/signup/tax_residency/b/n$a;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/g",
        "<",
        "Lkotlin/n;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000\n\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0004\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0003*\u0004\u0018\u00010\u00010\u0001H\n\u00a2\u0006\u0004\u0008\u0004\u0010\u0005"
    }
    d2 = {
        "<anonymous>",
        "",
        "it",
        "kotlin.jvm.PlatformType",
        "accept",
        "(Lkotlin/Unit;)V"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field final synthetic a:Lco/uk/getmondo/signup/tax_residency/b/n;

.field final synthetic b:Lco/uk/getmondo/api/model/tax_residency/Jurisdiction;


# direct methods
.method constructor <init>(Lco/uk/getmondo/signup/tax_residency/b/n;Lco/uk/getmondo/api/model/tax_residency/Jurisdiction;)V
    .locals 0

    iput-object p1, p0, Lco/uk/getmondo/signup/tax_residency/b/n$f;->a:Lco/uk/getmondo/signup/tax_residency/b/n;

    iput-object p2, p0, Lco/uk/getmondo/signup/tax_residency/b/n$f;->b:Lco/uk/getmondo/api/model/tax_residency/Jurisdiction;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 24
    check-cast p1, Lkotlin/n;

    invoke-virtual {p0, p1}, Lco/uk/getmondo/signup/tax_residency/b/n$f;->a(Lkotlin/n;)V

    return-void
.end method

.method public final a(Lkotlin/n;)V
    .locals 4

    .prologue
    .line 49
    iget-object v0, p0, Lco/uk/getmondo/signup/tax_residency/b/n$f;->a:Lco/uk/getmondo/signup/tax_residency/b/n;

    iget-object v1, p0, Lco/uk/getmondo/signup/tax_residency/b/n$f;->a:Lco/uk/getmondo/signup/tax_residency/b/n;

    iget-object v2, p0, Lco/uk/getmondo/signup/tax_residency/b/n$f;->a:Lco/uk/getmondo/signup/tax_residency/b/n;

    invoke-static {v2}, Lco/uk/getmondo/signup/tax_residency/b/n;->a(Lco/uk/getmondo/signup/tax_residency/b/n;)Lco/uk/getmondo/api/model/tax_residency/Jurisdiction$TinType;

    move-result-object v2

    iget-object v3, p0, Lco/uk/getmondo/signup/tax_residency/b/n$f;->b:Lco/uk/getmondo/api/model/tax_residency/Jurisdiction;

    invoke-static {v1, v2, v3}, Lco/uk/getmondo/signup/tax_residency/b/n;->a(Lco/uk/getmondo/signup/tax_residency/b/n;Lco/uk/getmondo/api/model/tax_residency/Jurisdiction$TinType;Lco/uk/getmondo/api/model/tax_residency/Jurisdiction;)Lco/uk/getmondo/api/model/tax_residency/Jurisdiction$TinType;

    move-result-object v1

    if-nez v1, :cond_0

    invoke-static {}, Lkotlin/d/b/l;->a()V

    :cond_0
    invoke-static {v0, v1}, Lco/uk/getmondo/signup/tax_residency/b/n;->a(Lco/uk/getmondo/signup/tax_residency/b/n;Lco/uk/getmondo/api/model/tax_residency/Jurisdiction$TinType;)V

    return-void
.end method
