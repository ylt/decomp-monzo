.class final Lco/uk/getmondo/signup/tax_residency/b/s$c;
.super Ljava/lang/Object;
.source "TaxResidencyUsResidentPresenter.kt"

# interfaces
.implements Lio/reactivex/c/g;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lco/uk/getmondo/signup/tax_residency/b/s;->a(Lco/uk/getmondo/signup/tax_residency/b/s$a;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/g",
        "<",
        "Lkotlin/h",
        "<+",
        "Ljava/lang/Boolean;",
        "+",
        "Ljava/util/ArrayList",
        "<",
        "Lco/uk/getmondo/signup/tax_residency/a/a;",
        ">;>;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000 \n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012F\u0010\u0002\u001aB\u0012\u0004\u0012\u00020\u0004\u0012\u0014\u0012\u0012\u0012\u0004\u0012\u00020\u00060\u0005j\u0008\u0012\u0004\u0012\u00020\u0006`\u0007 \u0008* \u0012\u0004\u0012\u00020\u0004\u0012\u0014\u0012\u0012\u0012\u0004\u0012\u00020\u00060\u0005j\u0008\u0012\u0004\u0012\u00020\u0006`\u0007\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\t"
    }
    d2 = {
        "<anonymous>",
        "",
        "<name for destructuring parameter 0>",
        "Lkotlin/Pair;",
        "",
        "Ljava/util/ArrayList;",
        "Lco/uk/getmondo/signup/tax_residency/data/TaxCountry;",
        "Lkotlin/collections/ArrayList;",
        "kotlin.jvm.PlatformType",
        "accept"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field final synthetic a:Lco/uk/getmondo/signup/tax_residency/b/s$a;


# direct methods
.method constructor <init>(Lco/uk/getmondo/signup/tax_residency/b/s$a;)V
    .locals 0

    iput-object p1, p0, Lco/uk/getmondo/signup/tax_residency/b/s$c;->a:Lco/uk/getmondo/signup/tax_residency/b/s$a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 22
    check-cast p1, Lkotlin/h;

    invoke-virtual {p0, p1}, Lco/uk/getmondo/signup/tax_residency/b/s$c;->a(Lkotlin/h;)V

    return-void
.end method

.method public final a(Lkotlin/h;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/h",
            "<",
            "Ljava/lang/Boolean;",
            "+",
            "Ljava/util/ArrayList",
            "<",
            "Lco/uk/getmondo/signup/tax_residency/a/a;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    invoke-virtual {p1}, Lkotlin/h;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {p1}, Lkotlin/h;->d()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/ArrayList;

    .line 52
    iget-object v2, p0, Lco/uk/getmondo/signup/tax_residency/b/s$c;->a:Lco/uk/getmondo/signup/tax_residency/b/s$a;

    const-string v3, "isUsTaxResident"

    invoke-static {v0, v3}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    const-string v3, "taxCountries"

    invoke-static {v1, v3}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v2, v0, v1}, Lco/uk/getmondo/signup/tax_residency/b/s$a;->a(ZLjava/util/ArrayList;)V

    return-void
.end method
