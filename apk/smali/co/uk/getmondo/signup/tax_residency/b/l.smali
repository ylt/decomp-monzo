.class public final Lco/uk/getmondo/signup/tax_residency/b/l;
.super Lco/uk/getmondo/common/f/a;
.source "TaxResidencyTinEntryFragment.kt"

# interfaces
.implements Lco/uk/getmondo/signup/tax_residency/b/n$a;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lco/uk/getmondo/signup/tax_residency/b/l$b;,
        Lco/uk/getmondo/signup/tax_residency/b/l$a;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000t\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u000e\n\u0002\u0008\u0004\n\u0002\u0010\r\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u000c\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0010\u000b\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0008\u0008\u0018\u0000 @2\u00020\u00012\u00020\u0002:\u0002@AB\u0005\u00a2\u0006\u0002\u0010\u0003J\u0008\u0010\u001d\u001a\u00020\u000cH\u0016J\u0010\u0010\u001e\u001a\u00020\u000c2\u0006\u0010\u001f\u001a\u00020\u0005H\u0016J\u0008\u0010 \u001a\u00020\u000cH\u0016J\u0008\u0010!\u001a\u00020\u000cH\u0016J\u0008\u0010\"\u001a\u00020\u000cH\u0016J\u0010\u0010#\u001a\u00020\u000c2\u0006\u0010$\u001a\u00020%H\u0016J\u0012\u0010&\u001a\u00020\u000c2\u0008\u0010\'\u001a\u0004\u0018\u00010(H\u0016J&\u0010)\u001a\u0004\u0018\u00010*2\u0006\u0010+\u001a\u00020,2\u0008\u0010-\u001a\u0004\u0018\u00010.2\u0008\u0010\'\u001a\u0004\u0018\u00010(H\u0016J\u0008\u0010/\u001a\u00020\u000cH\u0016J\u001c\u00100\u001a\u00020\u000c2\u0008\u00101\u001a\u0004\u0018\u00010*2\u0008\u0010\'\u001a\u0004\u0018\u00010(H\u0016J\u0010\u00102\u001a\u00020\u000c2\u0006\u00103\u001a\u000204H\u0016J\u0010\u00105\u001a\u00020\u000c2\u0006\u00106\u001a\u000204H\u0016J\u0010\u00107\u001a\u00020\u000c2\u0006\u00106\u001a\u000204H\u0016J\u0010\u00108\u001a\u00020\u000c2\u0006\u00109\u001a\u00020:H\u0016J\u0010\u0010;\u001a\u00020\u000c2\u0006\u0010<\u001a\u00020:H\u0016J\u0010\u0010=\u001a\u00020\u000c2\u0006\u0010>\u001a\u000204H\u0016J\u0010\u0010?\u001a\u00020\u000c2\u0006\u0010<\u001a\u00020:H\u0016R\u0014\u0010\u0004\u001a\u00020\u00058VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0006\u0010\u0007R\u000e\u0010\u0008\u001a\u00020\tX\u0082.\u00a2\u0006\u0002\n\u0000R\u001a\u0010\n\u001a\u0008\u0012\u0004\u0012\u00020\u000c0\u000b8VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\r\u0010\u000eR\u001a\u0010\u000f\u001a\u0008\u0012\u0004\u0012\u00020\u00100\u000b8VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0011\u0010\u000eR\u001a\u0010\u0012\u001a\u0008\u0012\u0004\u0012\u00020\u000c0\u000b8VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0013\u0010\u000eR\u001a\u0010\u0014\u001a\u0008\u0012\u0004\u0012\u00020\u00150\u000b8VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0016\u0010\u000eR\u001e\u0010\u0017\u001a\u00020\u00188\u0006@\u0006X\u0087.\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u0019\u0010\u001a\"\u0004\u0008\u001b\u0010\u001c\u00a8\u0006B"
    }
    d2 = {
        "Lco/uk/getmondo/signup/tax_residency/step/TaxResidencyTinEntryFragment;",
        "Lco/uk/getmondo/common/fragments/BaseFragment;",
        "Lco/uk/getmondo/signup/tax_residency/step/TaxResidencyTinEntryPresenter$View;",
        "()V",
        "jurisdiction",
        "Lco/uk/getmondo/api/model/tax_residency/Jurisdiction;",
        "getJurisdiction",
        "()Lco/uk/getmondo/api/model/tax_residency/Jurisdiction;",
        "listener",
        "Lco/uk/getmondo/signup/tax_residency/step/TaxResidencyTinEntryFragment$StepListener;",
        "onAltTinClicked",
        "Lio/reactivex/Observable;",
        "",
        "getOnAltTinClicked",
        "()Lio/reactivex/Observable;",
        "onContinueClicked",
        "",
        "getOnContinueClicked",
        "onNoTinClicked",
        "getOnNoTinClicked",
        "onTinChanges",
        "",
        "getOnTinChanges",
        "presenter",
        "Lco/uk/getmondo/signup/tax_residency/step/TaxResidencyTinEntryPresenter;",
        "getPresenter",
        "()Lco/uk/getmondo/signup/tax_residency/step/TaxResidencyTinEntryPresenter;",
        "setPresenter",
        "(Lco/uk/getmondo/signup/tax_residency/step/TaxResidencyTinEntryPresenter;)V",
        "completeStage",
        "goToNextStep",
        "nextJurisdiction",
        "hideAltTinType",
        "hideInvalidTinError",
        "hideNoTin",
        "onAttach",
        "context",
        "Landroid/content/Context;",
        "onCreate",
        "savedInstanceState",
        "Landroid/os/Bundle;",
        "onCreateView",
        "Landroid/view/View;",
        "inflater",
        "Landroid/view/LayoutInflater;",
        "container",
        "Landroid/view/ViewGroup;",
        "onDestroyView",
        "onViewCreated",
        "view",
        "setContinueButtonEnabled",
        "enabled",
        "",
        "setContinueButtonLoading",
        "loading",
        "setNoTinButtonLoading",
        "showAltTinType",
        "altTinType",
        "Lco/uk/getmondo/api/model/tax_residency/Jurisdiction$TinType;",
        "showInvalidTinError",
        "tinType",
        "showNoTin",
        "isPlural",
        "showTinType",
        "Companion",
        "StepListener",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# static fields
.field public static final c:Lco/uk/getmondo/signup/tax_residency/b/l$a;


# instance fields
.field public a:Lco/uk/getmondo/signup/tax_residency/b/n;

.field private d:Lco/uk/getmondo/signup/tax_residency/b/l$b;

.field private e:Ljava/util/HashMap;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lco/uk/getmondo/signup/tax_residency/b/l$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lco/uk/getmondo/signup/tax_residency/b/l$a;-><init>(Lkotlin/d/b/i;)V

    sput-object v0, Lco/uk/getmondo/signup/tax_residency/b/l;->c:Lco/uk/getmondo/signup/tax_residency/b/l$a;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Lco/uk/getmondo/common/f/a;-><init>()V

    return-void
.end method


# virtual methods
.method public a(I)Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lco/uk/getmondo/signup/tax_residency/b/l;->e:Ljava/util/HashMap;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lco/uk/getmondo/signup/tax_residency/b/l;->e:Ljava/util/HashMap;

    :cond_0
    iget-object v0, p0, Lco/uk/getmondo/signup/tax_residency/b/l;->e:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    if-nez v0, :cond_1

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getView()Landroid/view/View;

    move-result-object v0

    if-nez v0, :cond_2

    const/4 v0, 0x0

    :cond_1
    :goto_0
    return-object v0

    :cond_2
    invoke-virtual {v0, p1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/signup/tax_residency/b/l;->e:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method public a()Lco/uk/getmondo/api/model/tax_residency/Jurisdiction;
    .locals 2

    .prologue
    .line 56
    invoke-virtual {p0}, Lco/uk/getmondo/signup/tax_residency/b/l;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "ARG_JURISDICTION"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    const-string v1, "arguments.getParcelable(ARG_JURISDICTION)"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lco/uk/getmondo/api/model/tax_residency/Jurisdiction;

    return-object v0
.end method

.method public a(Lco/uk/getmondo/api/model/tax_residency/Jurisdiction$TinType;)V
    .locals 2

    .prologue
    const-string v0, "tinType"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 73
    sget v0, Lco/uk/getmondo/c$a;->taxNumberView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/tax_residency/b/l;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/signup/tax_residency/ui/TaxResidencyNumberView;

    invoke-virtual {p1}, Lco/uk/getmondo/api/model/tax_residency/Jurisdiction$TinType;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lco/uk/getmondo/signup/tax_residency/ui/TaxResidencyNumberView;->setLabel(Ljava/lang/String;)V

    .line 74
    sget v0, Lco/uk/getmondo/c$a;->taxNumberView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/tax_residency/b/l;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/signup/tax_residency/ui/TaxResidencyNumberView;

    sget v1, Lco/uk/getmondo/c$a;->taxNumberEditText:I

    invoke-virtual {v0, v1}, Lco/uk/getmondo/signup/tax_residency/ui/TaxResidencyNumberView;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/design/widget/TextInputEditText;

    const-string v1, ""

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Landroid/support/design/widget/TextInputEditText;->setText(Ljava/lang/CharSequence;)V

    .line 75
    return-void
.end method

.method public a(Lco/uk/getmondo/api/model/tax_residency/Jurisdiction;)V
    .locals 2

    .prologue
    const-string v0, "nextJurisdiction"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 122
    iget-object v0, p0, Lco/uk/getmondo/signup/tax_residency/b/l;->d:Lco/uk/getmondo/signup/tax_residency/b/l$b;

    if-nez v0, :cond_0

    const-string v1, "listener"

    invoke-static {v1}, Lkotlin/d/b/l;->b(Ljava/lang/String;)V

    :cond_0
    invoke-interface {v0, p1}, Lco/uk/getmondo/signup/tax_residency/b/l$b;->b(Lco/uk/getmondo/api/model/tax_residency/Jurisdiction;)V

    .line 123
    return-void
.end method

.method public a(Z)V
    .locals 4

    .prologue
    .line 86
    if-eqz p1, :cond_0

    const/4 v0, 0x2

    move v1, v0

    .line 87
    :goto_0
    sget v0, Lco/uk/getmondo/c$a;->noTinButton:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/tax_residency/b/l;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/common/ui/ProgressButton;

    invoke-virtual {p0}, Lco/uk/getmondo/signup/tax_residency/b/l;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const/high16 v3, 0x7f120000

    invoke-virtual {v2, v3, v1}, Landroid/content/res/Resources;->getQuantityString(II)Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Lco/uk/getmondo/common/ui/ProgressButton;->setText(Ljava/lang/CharSequence;)V

    .line 88
    sget v0, Lco/uk/getmondo/c$a;->noTinButton:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/tax_residency/b/l;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/common/ui/ProgressButton;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lco/uk/getmondo/common/ui/ProgressButton;->setVisibility(I)V

    .line 89
    return-void

    .line 86
    :cond_0
    const/4 v0, 0x1

    move v1, v0

    goto :goto_0
.end method

.method public b()Lio/reactivex/n;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Lkotlin/n;",
            ">;"
        }
    .end annotation

    .prologue
    .line 59
    sget v0, Lco/uk/getmondo/c$a;->altTinTypeText:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/tax_residency/b/l;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 143
    invoke-static {v0}, Lcom/b/a/c/c;->a(Landroid/view/View;)Lio/reactivex/n;

    move-result-object v1

    sget-object v0, Lcom/b/a/a/d;->a:Lcom/b/a/a/d;

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {v1, v0}, Lio/reactivex/n;->map(Lio/reactivex/c/h;)Lio/reactivex/n;

    move-result-object v0

    const-string v1, "RxView.clicks(this).map(VoidToUnit)"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 59
    return-object v0
.end method

.method public b(Lco/uk/getmondo/api/model/tax_residency/Jurisdiction$TinType;)V
    .locals 2

    .prologue
    const-string v0, "altTinType"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 78
    sget v0, Lco/uk/getmondo/c$a;->taxNumberView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/tax_residency/b/l;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/signup/tax_residency/ui/TaxResidencyNumberView;

    invoke-virtual {p1}, Lco/uk/getmondo/api/model/tax_residency/Jurisdiction$TinType;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lco/uk/getmondo/signup/tax_residency/ui/TaxResidencyNumberView;->setAltTinType(Ljava/lang/String;)V

    .line 79
    return-void
.end method

.method public b(Z)V
    .locals 1

    .prologue
    .line 106
    sget v0, Lco/uk/getmondo/c$a;->continueButton:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/tax_residency/b/l;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/common/ui/ProgressButton;

    invoke-virtual {v0, p1}, Lco/uk/getmondo/common/ui/ProgressButton;->setEnabled(Z)V

    .line 107
    return-void
.end method

.method public c()Lio/reactivex/n;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Ljava/lang/CharSequence;",
            ">;"
        }
    .end annotation

    .prologue
    .line 62
    sget v0, Lco/uk/getmondo/c$a;->taxNumberEditText:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/tax_residency/b/l;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/design/widget/TextInputEditText;

    .line 144
    invoke-static {v0}, Lcom/b/a/d/e;->c(Landroid/widget/TextView;)Lcom/b/a/a;

    move-result-object v0

    const-string v1, "RxTextView.textChanges(this)"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lio/reactivex/n;

    .line 62
    return-object v0
.end method

.method public c(Lco/uk/getmondo/api/model/tax_residency/Jurisdiction$TinType;)V
    .locals 6

    .prologue
    const-string v0, "tinType"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 96
    sget v0, Lco/uk/getmondo/c$a;->taxNumberInputLayout:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/tax_residency/b/l;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/design/widget/TextInputLayout;

    const v1, 0x7f0a03c4

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    .line 97
    const/4 v3, 0x0

    invoke-virtual {p1}, Lco/uk/getmondo/api/model/tax_residency/Jurisdiction$TinType;->b()Ljava/lang/String;

    move-result-object v4

    if-nez v4, :cond_0

    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type java.lang.String"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    invoke-virtual {v4}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v4

    const-string v5, "(this as java.lang.String).toLowerCase()"

    invoke-static {v4, v5}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    aput-object v4, v2, v3

    const/4 v3, 0x1

    invoke-virtual {p1}, Lco/uk/getmondo/api/model/tax_residency/Jurisdiction$TinType;->c()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    .line 96
    invoke-virtual {p0, v1, v2}, Lco/uk/getmondo/signup/tax_residency/b/l;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Landroid/support/design/widget/TextInputLayout;->setError(Ljava/lang/CharSequence;)V

    .line 98
    return-void
.end method

.method public c(Z)V
    .locals 1

    .prologue
    .line 110
    sget v0, Lco/uk/getmondo/c$a;->continueButton:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/tax_residency/b/l;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/common/ui/ProgressButton;

    invoke-virtual {v0, p1}, Lco/uk/getmondo/common/ui/ProgressButton;->setLoading(Z)V

    .line 111
    return-void
.end method

.method public d()Lio/reactivex/n;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 65
    sget v0, Lco/uk/getmondo/c$a;->continueButton:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/tax_residency/b/l;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/common/ui/ProgressButton;

    .line 145
    invoke-static {v0}, Lcom/b/a/c/c;->a(Landroid/view/View;)Lio/reactivex/n;

    move-result-object v1

    sget-object v0, Lcom/b/a/a/d;->a:Lcom/b/a/a/d;

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {v1, v0}, Lio/reactivex/n;->map(Lio/reactivex/c/h;)Lio/reactivex/n;

    move-result-object v1

    const-string v0, "RxView.clicks(this).map(VoidToUnit)"

    invoke-static {v1, v0}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 66
    sget v0, Lco/uk/getmondo/c$a;->taxNumberEditText:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/tax_residency/b/l;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/design/widget/TextInputEditText;

    .line 146
    invoke-static {v0}, Lcom/b/a/d/e;->a(Landroid/widget/TextView;)Lio/reactivex/n;

    move-result-object v2

    const-string v0, "RxTextView.editorActions(this)"

    invoke-static {v2, v0}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 66
    sget-object v0, Lco/uk/getmondo/signup/tax_residency/b/l$c;->a:Lco/uk/getmondo/signup/tax_residency/b/l$c;

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {v2, v0}, Lio/reactivex/n;->map(Lio/reactivex/c/h;)Lio/reactivex/n;

    move-result-object v0

    check-cast v0, Lio/reactivex/r;

    invoke-virtual {v1, v0}, Lio/reactivex/n;->mergeWith(Lio/reactivex/r;)Lio/reactivex/n;

    move-result-object v1

    .line 67
    new-instance v0, Lco/uk/getmondo/signup/tax_residency/b/l$d;

    invoke-direct {v0, p0}, Lco/uk/getmondo/signup/tax_residency/b/l$d;-><init>(Lco/uk/getmondo/signup/tax_residency/b/l;)V

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {v1, v0}, Lio/reactivex/n;->map(Lio/reactivex/c/h;)Lio/reactivex/n;

    move-result-object v0

    const-string v1, "continueButton.clicks()\n\u2026ditText.text.toString() }"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public d(Z)V
    .locals 1

    .prologue
    .line 114
    sget v0, Lco/uk/getmondo/c$a;->noTinButton:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/tax_residency/b/l;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/common/ui/ProgressButton;

    invoke-virtual {v0, p1}, Lco/uk/getmondo/common/ui/ProgressButton;->setLoading(Z)V

    .line 115
    return-void
.end method

.method public e()Lio/reactivex/n;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Lkotlin/n;",
            ">;"
        }
    .end annotation

    .prologue
    .line 70
    sget v0, Lco/uk/getmondo/c$a;->noTinButton:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/tax_residency/b/l;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/common/ui/ProgressButton;

    .line 147
    invoke-static {v0}, Lcom/b/a/c/c;->a(Landroid/view/View;)Lio/reactivex/n;

    move-result-object v1

    sget-object v0, Lcom/b/a/a/d;->a:Lcom/b/a/a/d;

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {v1, v0}, Lio/reactivex/n;->map(Lio/reactivex/c/h;)Lio/reactivex/n;

    move-result-object v0

    const-string v1, "RxView.clicks(this).map(VoidToUnit)"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 70
    return-object v0
.end method

.method public f()V
    .locals 2

    .prologue
    .line 82
    sget v0, Lco/uk/getmondo/c$a;->taxNumberView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/tax_residency/b/l;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/signup/tax_residency/ui/TaxResidencyNumberView;

    sget v1, Lco/uk/getmondo/c$a;->altTinTypeText:I

    invoke-virtual {v0, v1}, Lco/uk/getmondo/signup/tax_residency/ui/TaxResidencyNumberView;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 83
    return-void
.end method

.method public g()V
    .locals 2

    .prologue
    .line 92
    sget v0, Lco/uk/getmondo/c$a;->noTinButton:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/tax_residency/b/l;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/common/ui/ProgressButton;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lco/uk/getmondo/common/ui/ProgressButton;->setVisibility(I)V

    .line 93
    return-void
.end method

.method public h()V
    .locals 2

    .prologue
    .line 101
    sget v0, Lco/uk/getmondo/c$a;->taxNumberInputLayout:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/tax_residency/b/l;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/design/widget/TextInputLayout;

    const-string v1, ""

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Landroid/support/design/widget/TextInputLayout;->setError(Ljava/lang/CharSequence;)V

    .line 102
    sget v0, Lco/uk/getmondo/c$a;->taxNumberInputLayout:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/tax_residency/b/l;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/design/widget/TextInputLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/design/widget/TextInputLayout;->setErrorEnabled(Z)V

    .line 103
    return-void
.end method

.method public i()V
    .locals 2

    .prologue
    .line 118
    iget-object v0, p0, Lco/uk/getmondo/signup/tax_residency/b/l;->d:Lco/uk/getmondo/signup/tax_residency/b/l$b;

    if-nez v0, :cond_0

    const-string v1, "listener"

    invoke-static {v1}, Lkotlin/d/b/l;->b(Ljava/lang/String;)V

    :cond_0
    invoke-interface {v0}, Lco/uk/getmondo/signup/tax_residency/b/l$b;->j()V

    .line 119
    return-void
.end method

.method public j()V
    .locals 1

    iget-object v0, p0, Lco/uk/getmondo/signup/tax_residency/b/l;->e:Ljava/util/HashMap;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lco/uk/getmondo/signup/tax_residency/b/l;->e:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    :cond_0
    return-void
.end method

.method public onAttach(Landroid/content/Context;)V
    .locals 2

    .prologue
    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 27
    invoke-super {p0, p1}, Lco/uk/getmondo/common/f/a;->onAttach(Landroid/content/Context;)V

    .line 28
    instance-of v0, p1, Lco/uk/getmondo/signup/tax_residency/b/l$b;

    if-eqz v0, :cond_0

    .line 29
    check-cast p1, Lco/uk/getmondo/signup/tax_residency/b/l$b;

    iput-object p1, p0, Lco/uk/getmondo/signup/tax_residency/b/l;->d:Lco/uk/getmondo/signup/tax_residency/b/l$b;

    .line 32
    return-void

    .line 31
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Activity must implement TaxResidencyTinEntryFragment.StepListener"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 36
    invoke-super {p0, p1}, Lco/uk/getmondo/common/f/a;->onCreate(Landroid/os/Bundle;)V

    .line 37
    invoke-virtual {p0}, Lco/uk/getmondo/signup/tax_residency/b/l;->B()Lco/uk/getmondo/common/h/b/b;

    move-result-object v0

    invoke-interface {v0, p0}, Lco/uk/getmondo/common/h/b/b;->a(Lco/uk/getmondo/signup/tax_residency/b/l;)V

    .line 38
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2

    .prologue
    const-string v0, "inflater"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 41
    const v0, 0x7f0500ae

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public onDestroyView()V
    .locals 2

    .prologue
    .line 51
    invoke-super {p0}, Lco/uk/getmondo/common/f/a;->onDestroyView()V

    .line 52
    iget-object v0, p0, Lco/uk/getmondo/signup/tax_residency/b/l;->a:Lco/uk/getmondo/signup/tax_residency/b/n;

    if-nez v0, :cond_0

    const-string v1, "presenter"

    invoke-static {v1}, Lkotlin/d/b/l;->b(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0}, Lco/uk/getmondo/signup/tax_residency/b/n;->b()V

    .line 53
    invoke-virtual {p0}, Lco/uk/getmondo/signup/tax_residency/b/l;->j()V

    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 45
    invoke-super {p0, p1, p2}, Lco/uk/getmondo/common/f/a;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 46
    sget v0, Lco/uk/getmondo/c$a;->taxNumberView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/tax_residency/b/l;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/signup/tax_residency/ui/TaxResidencyNumberView;

    invoke-virtual {p0}, Lco/uk/getmondo/signup/tax_residency/b/l;->a()Lco/uk/getmondo/api/model/tax_residency/Jurisdiction;

    move-result-object v1

    invoke-virtual {v1}, Lco/uk/getmondo/api/model/tax_residency/Jurisdiction;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lco/uk/getmondo/signup/tax_residency/ui/TaxResidencyNumberView;->setFlag(Ljava/lang/String;)V

    .line 47
    iget-object v0, p0, Lco/uk/getmondo/signup/tax_residency/b/l;->a:Lco/uk/getmondo/signup/tax_residency/b/n;

    if-nez v0, :cond_0

    const-string v1, "presenter"

    invoke-static {v1}, Lkotlin/d/b/l;->b(Ljava/lang/String;)V

    :cond_0
    check-cast p0, Lco/uk/getmondo/signup/tax_residency/b/n$a;

    invoke-virtual {v0, p0}, Lco/uk/getmondo/signup/tax_residency/b/n;->a(Lco/uk/getmondo/signup/tax_residency/b/n$a;)V

    .line 48
    return-void
.end method
