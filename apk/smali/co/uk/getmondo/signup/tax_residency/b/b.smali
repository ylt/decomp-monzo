.class public final Lco/uk/getmondo/signup/tax_residency/b/b;
.super Lco/uk/getmondo/common/f/a;
.source "TaxResidencySelectionFragment.kt"

# interfaces
.implements Lco/uk/getmondo/signup/tax_residency/b/d$a;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lco/uk/getmondo/signup/tax_residency/b/b$b;,
        Lco/uk/getmondo/signup/tax_residency/b/b$a;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000r\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0010 \n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\n\u0018\u0000 42\u00020\u00012\u00020\u0002:\u000245B\u0005\u00a2\u0006\u0002\u0010\u0003J\u0008\u0010\u001d\u001a\u00020\rH\u0016J\u0010\u0010\u001e\u001a\u00020\r2\u0006\u0010\u001f\u001a\u00020 H\u0016J\u0010\u0010!\u001a\u00020\r2\u0006\u0010\"\u001a\u00020#H\u0016J\u0012\u0010$\u001a\u00020\r2\u0008\u0010%\u001a\u0004\u0018\u00010&H\u0016J&\u0010\'\u001a\u0004\u0018\u00010(2\u0006\u0010)\u001a\u00020*2\u0008\u0010+\u001a\u0004\u0018\u00010,2\u0008\u0010%\u001a\u0004\u0018\u00010&H\u0016J\u0008\u0010-\u001a\u00020\rH\u0016J\u001c\u0010.\u001a\u00020\r2\u0008\u0010/\u001a\u0004\u0018\u00010(2\u0008\u0010%\u001a\u0004\u0018\u00010&H\u0016J\u0010\u00100\u001a\u00020\r2\u0006\u00101\u001a\u00020\u0011H\u0016J\u0010\u00102\u001a\u00020\r2\u0006\u00103\u001a\u00020\u0007H\u0016R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0006\u001a\u00020\u00078VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0006\u0010\u0008R\u000e\u0010\t\u001a\u00020\nX\u0082.\u00a2\u0006\u0002\n\u0000R\u001a\u0010\u000b\u001a\u0008\u0012\u0004\u0012\u00020\r0\u000c8VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u000e\u0010\u000fR\u001a\u0010\u0010\u001a\u0008\u0012\u0004\u0012\u00020\u00110\u000c8VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0012\u0010\u000fR\u001e\u0010\u0013\u001a\u00020\u00148\u0006@\u0006X\u0087.\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u0015\u0010\u0016\"\u0004\u0008\u0017\u0010\u0018R\u001a\u0010\u0019\u001a\u0008\u0012\u0004\u0012\u00020\u00110\u001a8VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u001b\u0010\u001c\u00a8\u00066"
    }
    d2 = {
        "Lco/uk/getmondo/signup/tax_residency/step/TaxResidencySelectionFragment;",
        "Lco/uk/getmondo/common/fragments/BaseFragment;",
        "Lco/uk/getmondo/signup/tax_residency/step/TaxResidencySelectionPresenter$View;",
        "()V",
        "adapter",
        "Lco/uk/getmondo/signup/tax_residency/step/TaxResidencySelectionAdapter;",
        "isUsTaxResident",
        "",
        "()Z",
        "listener",
        "Lco/uk/getmondo/signup/tax_residency/step/TaxResidencySelectionFragment$StepListener;",
        "onContinue",
        "Lio/reactivex/Observable;",
        "",
        "getOnContinue",
        "()Lio/reactivex/Observable;",
        "onCountryClicked",
        "Lco/uk/getmondo/signup/tax_residency/data/TaxCountry;",
        "getOnCountryClicked",
        "presenter",
        "Lco/uk/getmondo/signup/tax_residency/step/TaxResidencySelectionPresenter;",
        "getPresenter",
        "()Lco/uk/getmondo/signup/tax_residency/step/TaxResidencySelectionPresenter;",
        "setPresenter",
        "(Lco/uk/getmondo/signup/tax_residency/step/TaxResidencySelectionPresenter;)V",
        "taxCountries",
        "",
        "getTaxCountries",
        "()Ljava/util/List;",
        "completeStage",
        "goToNextStep",
        "jurisdiction",
        "Lco/uk/getmondo/api/model/tax_residency/Jurisdiction;",
        "onAttach",
        "context",
        "Landroid/content/Context;",
        "onCreate",
        "savedInstanceState",
        "Landroid/os/Bundle;",
        "onCreateView",
        "Landroid/view/View;",
        "inflater",
        "Landroid/view/LayoutInflater;",
        "container",
        "Landroid/view/ViewGroup;",
        "onDestroyView",
        "onViewCreated",
        "view",
        "setSelection",
        "taxCountry",
        "showLoading",
        "loading",
        "Companion",
        "StepListener",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# static fields
.field public static final c:Lco/uk/getmondo/signup/tax_residency/b/b$a;


# instance fields
.field public a:Lco/uk/getmondo/signup/tax_residency/b/d;

.field private d:Lco/uk/getmondo/signup/tax_residency/b/b$b;

.field private final e:Lco/uk/getmondo/signup/tax_residency/b/a;

.field private f:Ljava/util/HashMap;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lco/uk/getmondo/signup/tax_residency/b/b$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lco/uk/getmondo/signup/tax_residency/b/b$a;-><init>(Lkotlin/d/b/i;)V

    sput-object v0, Lco/uk/getmondo/signup/tax_residency/b/b;->c:Lco/uk/getmondo/signup/tax_residency/b/b$a;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 22
    invoke-direct {p0}, Lco/uk/getmondo/common/f/a;-><init>()V

    .line 26
    new-instance v0, Lco/uk/getmondo/signup/tax_residency/b/a;

    const/4 v1, 0x3

    invoke-direct {v0, v2, v2, v1, v2}, Lco/uk/getmondo/signup/tax_residency/b/a;-><init>(Ljava/util/List;Lkotlin/d/a/b;ILkotlin/d/b/i;)V

    iput-object v0, p0, Lco/uk/getmondo/signup/tax_residency/b/b;->e:Lco/uk/getmondo/signup/tax_residency/b/a;

    return-void
.end method

.method public static final synthetic a(Lco/uk/getmondo/signup/tax_residency/b/b;)Lco/uk/getmondo/signup/tax_residency/b/a;
    .locals 1

    .prologue
    .line 22
    iget-object v0, p0, Lco/uk/getmondo/signup/tax_residency/b/b;->e:Lco/uk/getmondo/signup/tax_residency/b/a;

    return-object v0
.end method


# virtual methods
.method public a(I)Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lco/uk/getmondo/signup/tax_residency/b/b;->f:Ljava/util/HashMap;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lco/uk/getmondo/signup/tax_residency/b/b;->f:Ljava/util/HashMap;

    :cond_0
    iget-object v0, p0, Lco/uk/getmondo/signup/tax_residency/b/b;->f:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    if-nez v0, :cond_1

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getView()Landroid/view/View;

    move-result-object v0

    if-nez v0, :cond_2

    const/4 v0, 0x0

    :cond_1
    :goto_0
    return-object v0

    :cond_2
    invoke-virtual {v0, p1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/signup/tax_residency/b/b;->f:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method public a()Lio/reactivex/n;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Lco/uk/getmondo/signup/tax_residency/a/a;",
            ">;"
        }
    .end annotation

    .prologue
    .line 66
    new-instance v0, Lco/uk/getmondo/signup/tax_residency/b/b$c;

    invoke-direct {v0, p0}, Lco/uk/getmondo/signup/tax_residency/b/b$c;-><init>(Lco/uk/getmondo/signup/tax_residency/b/b;)V

    check-cast v0, Lio/reactivex/p;

    invoke-static {v0}, Lio/reactivex/n;->create(Lio/reactivex/p;)Lio/reactivex/n;

    move-result-object v0

    const-string v1, "Observable.create { emit\u2026stener = null }\n        }"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 69
    return-object v0
.end method

.method public a(Lco/uk/getmondo/api/model/tax_residency/Jurisdiction;)V
    .locals 2

    .prologue
    const-string v0, "jurisdiction"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 93
    iget-object v0, p0, Lco/uk/getmondo/signup/tax_residency/b/b;->d:Lco/uk/getmondo/signup/tax_residency/b/b$b;

    if-nez v0, :cond_0

    const-string v1, "listener"

    invoke-static {v1}, Lkotlin/d/b/l;->b(Ljava/lang/String;)V

    :cond_0
    invoke-interface {v0, p1}, Lco/uk/getmondo/signup/tax_residency/b/b$b;->a(Lco/uk/getmondo/api/model/tax_residency/Jurisdiction;)V

    .line 94
    return-void
.end method

.method public a(Lco/uk/getmondo/signup/tax_residency/a/a;)V
    .locals 1

    .prologue
    const-string v0, "taxCountry"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 81
    iget-object v0, p0, Lco/uk/getmondo/signup/tax_residency/b/b;->e:Lco/uk/getmondo/signup/tax_residency/b/a;

    invoke-virtual {v0, p1}, Lco/uk/getmondo/signup/tax_residency/b/a;->a(Lco/uk/getmondo/signup/tax_residency/a/a;)V

    .line 82
    return-void
.end method

.method public a(Z)V
    .locals 1

    .prologue
    .line 85
    sget v0, Lco/uk/getmondo/c$a;->continueButton:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/tax_residency/b/b;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/common/ui/ProgressButton;

    invoke-virtual {v0, p1}, Lco/uk/getmondo/common/ui/ProgressButton;->setLoading(Z)V

    .line 86
    return-void
.end method

.method public b()Lio/reactivex/n;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Lkotlin/n;",
            ">;"
        }
    .end annotation

    .prologue
    .line 72
    sget v0, Lco/uk/getmondo/c$a;->continueButton:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/tax_residency/b/b;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/common/ui/ProgressButton;

    .line 116
    invoke-static {v0}, Lcom/b/a/c/c;->a(Landroid/view/View;)Lio/reactivex/n;

    move-result-object v1

    sget-object v0, Lcom/b/a/a/d;->a:Lcom/b/a/a/d;

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {v1, v0}, Lio/reactivex/n;->map(Lio/reactivex/c/h;)Lio/reactivex/n;

    move-result-object v0

    const-string v1, "RxView.clicks(this).map(VoidToUnit)"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 72
    return-object v0
.end method

.method public c()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lco/uk/getmondo/signup/tax_residency/a/a;",
            ">;"
        }
    .end annotation

    .prologue
    .line 75
    iget-object v0, p0, Lco/uk/getmondo/signup/tax_residency/b/b;->e:Lco/uk/getmondo/signup/tax_residency/b/a;

    invoke-virtual {v0}, Lco/uk/getmondo/signup/tax_residency/b/a;->a()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public d()Z
    .locals 2

    .prologue
    .line 78
    invoke-virtual {p0}, Lco/uk/getmondo/signup/tax_residency/b/b;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "ARG_US_TAX_RESIDENT"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public e()V
    .locals 2

    .prologue
    .line 89
    iget-object v0, p0, Lco/uk/getmondo/signup/tax_residency/b/b;->d:Lco/uk/getmondo/signup/tax_residency/b/b$b;

    if-nez v0, :cond_0

    const-string v1, "listener"

    invoke-static {v1}, Lkotlin/d/b/l;->b(Ljava/lang/String;)V

    :cond_0
    invoke-interface {v0}, Lco/uk/getmondo/signup/tax_residency/b/b$b;->j()V

    .line 90
    return-void
.end method

.method public f()V
    .locals 1

    iget-object v0, p0, Lco/uk/getmondo/signup/tax_residency/b/b;->f:Ljava/util/HashMap;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lco/uk/getmondo/signup/tax_residency/b/b;->f:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    :cond_0
    return-void
.end method

.method public onAttach(Landroid/content/Context;)V
    .locals 2

    .prologue
    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 29
    invoke-super {p0, p1}, Lco/uk/getmondo/common/f/a;->onAttach(Landroid/content/Context;)V

    .line 30
    instance-of v0, p1, Lco/uk/getmondo/signup/tax_residency/b/b$b;

    if-eqz v0, :cond_0

    .line 31
    check-cast p1, Lco/uk/getmondo/signup/tax_residency/b/b$b;

    iput-object p1, p0, Lco/uk/getmondo/signup/tax_residency/b/b;->d:Lco/uk/getmondo/signup/tax_residency/b/b$b;

    .line 34
    return-void

    .line 33
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Activity must implement TaxResidencySelectionFragment.StepListener"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 38
    invoke-super {p0, p1}, Lco/uk/getmondo/common/f/a;->onCreate(Landroid/os/Bundle;)V

    .line 39
    invoke-virtual {p0}, Lco/uk/getmondo/signup/tax_residency/b/b;->B()Lco/uk/getmondo/common/h/b/b;

    move-result-object v0

    invoke-interface {v0, p0}, Lco/uk/getmondo/common/h/b/b;->a(Lco/uk/getmondo/signup/tax_residency/b/b;)V

    .line 40
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2

    .prologue
    const-string v0, "inflater"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 43
    const v0, 0x7f0500ac

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public onDestroyView()V
    .locals 2

    .prologue
    .line 61
    iget-object v0, p0, Lco/uk/getmondo/signup/tax_residency/b/b;->a:Lco/uk/getmondo/signup/tax_residency/b/d;

    if-nez v0, :cond_0

    const-string v1, "presenter"

    invoke-static {v1}, Lkotlin/d/b/l;->b(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0}, Lco/uk/getmondo/signup/tax_residency/b/d;->b()V

    .line 62
    invoke-super {p0}, Lco/uk/getmondo/common/f/a;->onDestroyView()V

    .line 63
    invoke-virtual {p0}, Lco/uk/getmondo/signup/tax_residency/b/b;->f()V

    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 11

    .prologue
    const/4 v3, 0x0

    const/4 v4, 0x0

    .line 47
    invoke-super {p0, p1, p2}, Lco/uk/getmondo/common/f/a;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 49
    new-instance v2, Landroid/support/v7/widget/LinearLayoutManager;

    invoke-virtual {p0}, Lco/uk/getmondo/signup/tax_residency/b/b;->getActivity()Landroid/support/v4/app/j;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-direct {v2, v0}, Landroid/support/v7/widget/LinearLayoutManager;-><init>(Landroid/content/Context;)V

    .line 50
    sget v0, Lco/uk/getmondo/c$a;->residencyCountriesRecyclerView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/tax_residency/b/b;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView;

    move-object v1, v2

    check-cast v1, Landroid/support/v7/widget/RecyclerView$h;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(Landroid/support/v7/widget/RecyclerView$h;)V

    .line 51
    sget v0, Lco/uk/getmondo/c$a;->residencyCountriesRecyclerView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/tax_residency/b/b;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView;

    iget-object v1, p0, Lco/uk/getmondo/signup/tax_residency/b/b;->e:Lco/uk/getmondo/signup/tax_residency/b/a;

    check-cast v1, Landroid/support/v7/widget/RecyclerView$a;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setAdapter(Landroid/support/v7/widget/RecyclerView$a;)V

    .line 52
    sget v0, Lco/uk/getmondo/c$a;->residencyCountriesRecyclerView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/tax_residency/b/b;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setHasFixedSize(Z)V

    .line 53
    sget v0, Lco/uk/getmondo/c$a;->residencyCountriesRecyclerView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/tax_residency/b/b;->a(I)Landroid/view/View;

    move-result-object v0

    move-object v10, v0

    check-cast v10, Landroid/support/v7/widget/RecyclerView;

    new-instance v0, Lco/uk/getmondo/common/ui/e;

    invoke-virtual {p0}, Lco/uk/getmondo/signup/tax_residency/b/b;->getActivity()Landroid/support/v4/app/j;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    invoke-virtual {v2}, Landroid/support/v7/widget/LinearLayoutManager;->h()I

    move-result v2

    const/16 v8, 0x7c

    move v5, v4

    move v6, v4

    move v7, v4

    move-object v9, v3

    invoke-direct/range {v0 .. v9}, Lco/uk/getmondo/common/ui/e;-><init>(Landroid/content/Context;ILandroid/graphics/drawable/Drawable;IIIIILkotlin/d/b/i;)V

    check-cast v0, Landroid/support/v7/widget/RecyclerView$g;

    invoke-virtual {v10, v0}, Landroid/support/v7/widget/RecyclerView;->a(Landroid/support/v7/widget/RecyclerView$g;)V

    .line 54
    sget v0, Lco/uk/getmondo/c$a;->residencyCountriesRecyclerView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/tax_residency/b/b;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView;->getItemAnimator()Landroid/support/v7/widget/RecyclerView$e;

    move-result-object v0

    if-nez v0, :cond_0

    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type android.support.v7.widget.DefaultItemAnimator"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    check-cast v0, Landroid/support/v7/widget/ak;

    invoke-virtual {v0, v4}, Landroid/support/v7/widget/ak;->a(Z)V

    .line 55
    iget-object v1, p0, Lco/uk/getmondo/signup/tax_residency/b/b;->e:Lco/uk/getmondo/signup/tax_residency/b/a;

    invoke-virtual {p0}, Lco/uk/getmondo/signup/tax_residency/b/b;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v2, "ARG_TAX_COUNTRIES"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    const-string v2, "arguments.getParcelableA\u2026ayList(ARG_TAX_COUNTRIES)"

    invoke-static {v0, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Ljava/util/List;

    invoke-virtual {v1, v0}, Lco/uk/getmondo/signup/tax_residency/b/a;->a(Ljava/util/List;)V

    .line 57
    iget-object v0, p0, Lco/uk/getmondo/signup/tax_residency/b/b;->a:Lco/uk/getmondo/signup/tax_residency/b/d;

    if-nez v0, :cond_1

    const-string v1, "presenter"

    invoke-static {v1}, Lkotlin/d/b/l;->b(Ljava/lang/String;)V

    :cond_1
    check-cast p0, Lco/uk/getmondo/signup/tax_residency/b/d$a;

    invoke-virtual {v0, p0}, Lco/uk/getmondo/signup/tax_residency/b/d;->a(Lco/uk/getmondo/signup/tax_residency/b/d$a;)V

    .line 58
    return-void
.end method
