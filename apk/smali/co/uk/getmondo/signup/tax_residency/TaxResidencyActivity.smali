.class public final Lco/uk/getmondo/signup/tax_residency/TaxResidencyActivity;
.super Lco/uk/getmondo/signup/a;
.source "TaxResidencyActivity.kt"

# interfaces
.implements Lco/uk/getmondo/signup/tax_residency/b$a;
.implements Lco/uk/getmondo/signup/tax_residency/b/b$b;
.implements Lco/uk/getmondo/signup/tax_residency/b/g$a;
.implements Lco/uk/getmondo/signup/tax_residency/b/l$b;
.implements Lco/uk/getmondo/signup/tax_residency/b/q$a;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lco/uk/getmondo/signup/tax_residency/TaxResidencyActivity$a;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0008\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0007\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u0008\n\u0002\u0008\u0005\u0018\u0000 02\u00020\u00012\u00020\u00022\u00020\u00032\u00020\u00042\u00020\u00052\u00020\u0006:\u00010B\u0005\u00a2\u0006\u0002\u0010\u0007J\u0008\u0010\u0013\u001a\u00020\nH\u0016J\u0008\u0010\u0014\u001a\u00020\nH\u0016J\u0010\u0010\u0015\u001a\u00020\n2\u0006\u0010\u0016\u001a\u00020\u0017H\u0016J\u0012\u0010\u0018\u001a\u00020\n2\u0008\u0010\u0019\u001a\u0004\u0018\u00010\u001aH\u0014J\u0008\u0010\u001b\u001a\u00020\nH\u0014J\u0008\u0010\u001c\u001a\u00020\nH\u0016J\u0008\u0010\u001d\u001a\u00020\nH\u0016J\u0010\u0010\u001e\u001a\u00020\n2\u0006\u0010\u001f\u001a\u00020\u0017H\u0016J\u001e\u0010 \u001a\u00020\n2\u0006\u0010!\u001a\u00020\"2\u000c\u0010#\u001a\u0008\u0012\u0004\u0012\u00020%0$H\u0016J\u001a\u0010&\u001a\u00020\n2\u0006\u0010\'\u001a\u00020(2\u0008\u0008\u0002\u0010)\u001a\u00020\"H\u0002J\u0010\u0010*\u001a\u00020\n2\u0006\u0010+\u001a\u00020,H\u0016J\u0008\u0010-\u001a\u00020\nH\u0016J\u0008\u0010.\u001a\u00020\nH\u0016J\u0008\u0010/\u001a\u00020\nH\u0016R\u001a\u0010\u0008\u001a\u0008\u0012\u0004\u0012\u00020\n0\t8VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u000b\u0010\u000cR\u001e\u0010\r\u001a\u00020\u000e8\u0006@\u0006X\u0087.\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u000f\u0010\u0010\"\u0004\u0008\u0011\u0010\u0012\u00a8\u00061"
    }
    d2 = {
        "Lco/uk/getmondo/signup/tax_residency/TaxResidencyActivity;",
        "Lco/uk/getmondo/signup/BaseSignupActivity;",
        "Lco/uk/getmondo/signup/tax_residency/TaxResidencyPresenter$View;",
        "Lco/uk/getmondo/signup/tax_residency/step/TaxResidencySummaryFragment$StepListener;",
        "Lco/uk/getmondo/signup/tax_residency/step/TaxResidencyUsResidentFragment$StepListener;",
        "Lco/uk/getmondo/signup/tax_residency/step/TaxResidencySelectionFragment$StepListener;",
        "Lco/uk/getmondo/signup/tax_residency/step/TaxResidencyTinEntryFragment$StepListener;",
        "()V",
        "onRetryClicked",
        "Lio/reactivex/Observable;",
        "",
        "getOnRetryClicked",
        "()Lio/reactivex/Observable;",
        "presenter",
        "Lco/uk/getmondo/signup/tax_residency/TaxResidencyPresenter;",
        "getPresenter",
        "()Lco/uk/getmondo/signup/tax_residency/TaxResidencyPresenter;",
        "setPresenter",
        "(Lco/uk/getmondo/signup/tax_residency/TaxResidencyPresenter;)V",
        "finishStage",
        "hideLoading",
        "onCountrySelectionComplete",
        "firstJurisdiction",
        "Lco/uk/getmondo/api/model/tax_residency/Jurisdiction;",
        "onCreate",
        "savedInstanceState",
        "Landroid/os/Bundle;",
        "onDestroy",
        "onStageComplete",
        "onSummaryStepComplete",
        "onTinEntryComplete",
        "nextJurisdiction",
        "onUsResidentStepCompleted",
        "isUsTaxResident",
        "",
        "taxCountries",
        "Ljava/util/ArrayList;",
        "Lco/uk/getmondo/signup/tax_residency/data/TaxCountry;",
        "showFragment",
        "fragment",
        "Landroid/support/v4/app/Fragment;",
        "addToBackStack",
        "showFullScreenError",
        "errorMessage",
        "",
        "showLoading",
        "showSummaryScreen",
        "showUsTaxResidentScreen",
        "Companion",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# static fields
.field public static final b:Lco/uk/getmondo/signup/tax_residency/TaxResidencyActivity$a;


# instance fields
.field public a:Lco/uk/getmondo/signup/tax_residency/b;

.field private g:Ljava/util/HashMap;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lco/uk/getmondo/signup/tax_residency/TaxResidencyActivity$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lco/uk/getmondo/signup/tax_residency/TaxResidencyActivity$a;-><init>(Lkotlin/d/b/i;)V

    sput-object v0, Lco/uk/getmondo/signup/tax_residency/TaxResidencyActivity;->b:Lco/uk/getmondo/signup/tax_residency/TaxResidencyActivity$a;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Lco/uk/getmondo/signup/a;-><init>()V

    return-void
.end method

.method private final a(Landroid/support/v4/app/Fragment;Z)V
    .locals 3

    .prologue
    .line 107
    invoke-virtual {p0}, Lco/uk/getmondo/signup/tax_residency/TaxResidencyActivity;->getSupportFragmentManager()Landroid/support/v4/app/n;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/n;->a()Landroid/support/v4/app/t;

    move-result-object v0

    .line 108
    const v1, 0x7f110238

    invoke-virtual {v0, v1, p1}, Landroid/support/v4/app/t;->b(ILandroid/support/v4/app/Fragment;)Landroid/support/v4/app/t;

    move-result-object v0

    .line 109
    if-eqz p2, :cond_0

    .line 111
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v4/app/t;->a(Ljava/lang/String;)Landroid/support/v4/app/t;

    move-result-object v1

    .line 112
    const/16 v2, 0x1001

    invoke-virtual {v1, v2}, Landroid/support/v4/app/t;->a(I)Landroid/support/v4/app/t;

    .line 114
    :cond_0
    invoke-virtual {v0}, Landroid/support/v4/app/t;->d()I

    .line 115
    return-void
.end method

.method static bridge synthetic a(Lco/uk/getmondo/signup/tax_residency/TaxResidencyActivity;Landroid/support/v4/app/Fragment;ZILjava/lang/Object;)V
    .locals 1

    .prologue
    and-int/lit8 v0, p3, 0x2

    if-eqz v0, :cond_0

    .line 106
    const/4 p2, 0x1

    :cond_0
    invoke-direct {p0, p1, p2}, Lco/uk/getmondo/signup/tax_residency/TaxResidencyActivity;->a(Landroid/support/v4/app/Fragment;Z)V

    return-void
.end method


# virtual methods
.method public a(I)Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lco/uk/getmondo/signup/tax_residency/TaxResidencyActivity;->g:Ljava/util/HashMap;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lco/uk/getmondo/signup/tax_residency/TaxResidencyActivity;->g:Ljava/util/HashMap;

    :cond_0
    iget-object v0, p0, Lco/uk/getmondo/signup/tax_residency/TaxResidencyActivity;->g:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    if-nez v0, :cond_1

    invoke-virtual {p0, p1}, Landroid/support/v4/app/j;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/signup/tax_residency/TaxResidencyActivity;->g:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    return-object v0
.end method

.method public a(Lco/uk/getmondo/api/model/tax_residency/Jurisdiction;)V
    .locals 4

    .prologue
    const-string v0, "firstJurisdiction"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 94
    sget-object v0, Lco/uk/getmondo/signup/tax_residency/b/l;->c:Lco/uk/getmondo/signup/tax_residency/b/l$a;

    invoke-virtual {v0, p1}, Lco/uk/getmondo/signup/tax_residency/b/l$a;->a(Lco/uk/getmondo/api/model/tax_residency/Jurisdiction;)Lco/uk/getmondo/signup/tax_residency/b/l;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/Fragment;

    const/4 v1, 0x0

    const/4 v2, 0x2

    const/4 v3, 0x0

    invoke-static {p0, v0, v1, v2, v3}, Lco/uk/getmondo/signup/tax_residency/TaxResidencyActivity;->a(Lco/uk/getmondo/signup/tax_residency/TaxResidencyActivity;Landroid/support/v4/app/Fragment;ZILjava/lang/Object;)V

    .line 95
    return-void
.end method

.method public a(ZLjava/util/ArrayList;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "Ljava/util/ArrayList",
            "<",
            "Lco/uk/getmondo/signup/tax_residency/a/a;",
            ">;)V"
        }
    .end annotation

    .prologue
    const-string v0, "taxCountries"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 90
    sget-object v0, Lco/uk/getmondo/signup/tax_residency/b/b;->c:Lco/uk/getmondo/signup/tax_residency/b/b$a;

    invoke-virtual {v0, p1, p2}, Lco/uk/getmondo/signup/tax_residency/b/b$a;->a(ZLjava/util/ArrayList;)Lco/uk/getmondo/signup/tax_residency/b/b;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/Fragment;

    const/4 v1, 0x0

    const/4 v2, 0x2

    const/4 v3, 0x0

    invoke-static {p0, v0, v1, v2, v3}, Lco/uk/getmondo/signup/tax_residency/TaxResidencyActivity;->a(Lco/uk/getmondo/signup/tax_residency/TaxResidencyActivity;Landroid/support/v4/app/Fragment;ZILjava/lang/Object;)V

    .line 91
    return-void
.end method

.method public b(Lco/uk/getmondo/api/model/tax_residency/Jurisdiction;)V
    .locals 4

    .prologue
    const-string v0, "nextJurisdiction"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 98
    sget-object v0, Lco/uk/getmondo/signup/tax_residency/b/l;->c:Lco/uk/getmondo/signup/tax_residency/b/l$a;

    invoke-virtual {v0, p1}, Lco/uk/getmondo/signup/tax_residency/b/l$a;->a(Lco/uk/getmondo/api/model/tax_residency/Jurisdiction;)Lco/uk/getmondo/signup/tax_residency/b/l;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/Fragment;

    const/4 v1, 0x0

    const/4 v2, 0x2

    const/4 v3, 0x0

    invoke-static {p0, v0, v1, v2, v3}, Lco/uk/getmondo/signup/tax_residency/TaxResidencyActivity;->a(Lco/uk/getmondo/signup/tax_residency/TaxResidencyActivity;Landroid/support/v4/app/Fragment;ZILjava/lang/Object;)V

    .line 99
    return-void
.end method

.method public c()Lio/reactivex/n;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Lkotlin/n;",
            ">;"
        }
    .end annotation

    .prologue
    .line 50
    sget v0, Lco/uk/getmondo/c$a;->taxResidencyErrorView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/tax_residency/TaxResidencyActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/common/ui/LoadingErrorView;

    invoke-virtual {v0}, Lco/uk/getmondo/common/ui/LoadingErrorView;->c()Lio/reactivex/n;

    move-result-object v0

    return-object v0
.end method

.method public d()V
    .locals 1

    .prologue
    .line 53
    sget v0, Lco/uk/getmondo/c$a;->taxResidencyProgress:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/tax_residency/TaxResidencyActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    invoke-static {v0}, Lco/uk/getmondo/common/ae;->a(Landroid/view/View;)V

    .line 54
    sget v0, Lco/uk/getmondo/c$a;->taxResidencyErrorView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/tax_residency/TaxResidencyActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/common/ui/LoadingErrorView;

    invoke-static {v0}, Lco/uk/getmondo/common/ae;->b(Landroid/view/View;)V

    .line 55
    sget v0, Lco/uk/getmondo/c$a;->taxResidencyFragmentContainer:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/tax_residency/TaxResidencyActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    invoke-static {v0}, Lco/uk/getmondo/common/ae;->b(Landroid/view/View;)V

    .line 56
    return-void
.end method

.method public d(I)V
    .locals 2

    .prologue
    .line 64
    sget v0, Lco/uk/getmondo/c$a;->taxResidencyErrorView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/tax_residency/TaxResidencyActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/common/ui/LoadingErrorView;

    invoke-virtual {p0, p1}, Lco/uk/getmondo/signup/tax_residency/TaxResidencyActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lco/uk/getmondo/common/ui/LoadingErrorView;->setMessage(Ljava/lang/String;)V

    .line 65
    sget v0, Lco/uk/getmondo/c$a;->taxResidencyErrorView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/tax_residency/TaxResidencyActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/common/ui/LoadingErrorView;

    invoke-static {v0}, Lco/uk/getmondo/common/ae;->a(Landroid/view/View;)V

    .line 66
    sget v0, Lco/uk/getmondo/c$a;->taxResidencyFragmentContainer:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/tax_residency/TaxResidencyActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    invoke-static {v0}, Lco/uk/getmondo/common/ae;->b(Landroid/view/View;)V

    .line 67
    return-void
.end method

.method public e()V
    .locals 1

    .prologue
    .line 59
    sget v0, Lco/uk/getmondo/c$a;->taxResidencyProgress:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/tax_residency/TaxResidencyActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    invoke-static {v0}, Lco/uk/getmondo/common/ae;->b(Landroid/view/View;)V

    .line 60
    sget v0, Lco/uk/getmondo/c$a;->taxResidencyFragmentContainer:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/tax_residency/TaxResidencyActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    invoke-static {v0}, Lco/uk/getmondo/common/ae;->a(Landroid/view/View;)V

    .line 61
    return-void
.end method

.method public f()V
    .locals 2

    .prologue
    .line 70
    new-instance v0, Lco/uk/getmondo/signup/tax_residency/b/g;

    invoke-direct {v0}, Lco/uk/getmondo/signup/tax_residency/b/g;-><init>()V

    check-cast v0, Landroid/support/v4/app/Fragment;

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lco/uk/getmondo/signup/tax_residency/TaxResidencyActivity;->a(Landroid/support/v4/app/Fragment;Z)V

    .line 71
    return-void
.end method

.method public g()V
    .locals 2

    .prologue
    .line 74
    new-instance v0, Lco/uk/getmondo/signup/tax_residency/b/q;

    invoke-direct {v0}, Lco/uk/getmondo/signup/tax_residency/b/q;-><init>()V

    check-cast v0, Landroid/support/v4/app/Fragment;

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lco/uk/getmondo/signup/tax_residency/TaxResidencyActivity;->a(Landroid/support/v4/app/Fragment;Z)V

    .line 75
    return-void
.end method

.method public h()V
    .locals 1

    .prologue
    .line 78
    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/tax_residency/TaxResidencyActivity;->setResult(I)V

    .line 79
    invoke-virtual {p0}, Lco/uk/getmondo/signup/tax_residency/TaxResidencyActivity;->finish()V

    .line 80
    return-void
.end method

.method public i()V
    .locals 2

    .prologue
    .line 86
    new-instance v0, Lco/uk/getmondo/signup/tax_residency/b/q;

    invoke-direct {v0}, Lco/uk/getmondo/signup/tax_residency/b/q;-><init>()V

    check-cast v0, Landroid/support/v4/app/Fragment;

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lco/uk/getmondo/signup/tax_residency/TaxResidencyActivity;->a(Landroid/support/v4/app/Fragment;Z)V

    .line 87
    return-void
.end method

.method public j()V
    .locals 0

    .prologue
    .line 102
    invoke-virtual {p0}, Lco/uk/getmondo/signup/tax_residency/TaxResidencyActivity;->h()V

    .line 103
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 34
    invoke-super {p0, p1}, Lco/uk/getmondo/signup/a;->onCreate(Landroid/os/Bundle;)V

    .line 35
    const v0, 0x7f050067

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/tax_residency/TaxResidencyActivity;->setContentView(I)V

    .line 37
    invoke-virtual {p0}, Lco/uk/getmondo/signup/tax_residency/TaxResidencyActivity;->l()Lco/uk/getmondo/common/h/b/b;

    move-result-object v0

    invoke-interface {v0, p0}, Lco/uk/getmondo/common/h/b/b;->a(Lco/uk/getmondo/signup/tax_residency/TaxResidencyActivity;)V

    .line 38
    invoke-virtual {p0}, Lco/uk/getmondo/signup/tax_residency/TaxResidencyActivity;->getSupportActionBar()Landroid/support/v7/app/a;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v7/app/a;->b(Z)V

    .line 40
    :cond_0
    iget-object v0, p0, Lco/uk/getmondo/signup/tax_residency/TaxResidencyActivity;->a:Lco/uk/getmondo/signup/tax_residency/b;

    if-nez v0, :cond_1

    const-string v1, "presenter"

    invoke-static {v1}, Lkotlin/d/b/l;->b(Ljava/lang/String;)V

    :cond_1
    check-cast p0, Lco/uk/getmondo/signup/tax_residency/b$a;

    invoke-virtual {v0, p0}, Lco/uk/getmondo/signup/tax_residency/b;->a(Lco/uk/getmondo/signup/tax_residency/b$a;)V

    .line 41
    return-void
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    .line 44
    iget-object v0, p0, Lco/uk/getmondo/signup/tax_residency/TaxResidencyActivity;->a:Lco/uk/getmondo/signup/tax_residency/b;

    if-nez v0, :cond_0

    const-string v1, "presenter"

    invoke-static {v1}, Lkotlin/d/b/l;->b(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0}, Lco/uk/getmondo/signup/tax_residency/b;->b()V

    .line 45
    invoke-super {p0}, Lco/uk/getmondo/signup/a;->onDestroy()V

    .line 46
    return-void
.end method
