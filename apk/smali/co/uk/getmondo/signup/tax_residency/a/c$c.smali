.class final Lco/uk/getmondo/signup/tax_residency/a/c$c;
.super Ljava/lang/Object;
.source "TaxResidencyManager.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lco/uk/getmondo/signup/tax_residency/a/c;->b()Lio/reactivex/v;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h",
        "<TT;TR;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000\u0012\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u0008\u0012\u0004\u0012\u00020\u00020\u00012\u0006\u0010\u0003\u001a\u00020\u0004H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "",
        "Lco/uk/getmondo/api/model/tax_residency/Jurisdiction;",
        "it",
        "Lco/uk/getmondo/api/model/tax_residency/TaxResidencyInfo;",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# static fields
.field public static final a:Lco/uk/getmondo/signup/tax_residency/a/c$c;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lco/uk/getmondo/signup/tax_residency/a/c$c;

    invoke-direct {v0}, Lco/uk/getmondo/signup/tax_residency/a/c$c;-><init>()V

    sput-object v0, Lco/uk/getmondo/signup/tax_residency/a/c$c;->a:Lco/uk/getmondo/signup/tax_residency/a/c$c;

    return-void
.end method

.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 12
    check-cast p1, Lco/uk/getmondo/api/model/tax_residency/TaxResidencyInfo;

    invoke-virtual {p0, p1}, Lco/uk/getmondo/signup/tax_residency/a/c$c;->a(Lco/uk/getmondo/api/model/tax_residency/TaxResidencyInfo;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lco/uk/getmondo/api/model/tax_residency/TaxResidencyInfo;)Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lco/uk/getmondo/api/model/tax_residency/TaxResidencyInfo;",
            ")",
            "Ljava/util/List",
            "<",
            "Lco/uk/getmondo/api/model/tax_residency/Jurisdiction;",
            ">;"
        }
    .end annotation

    .prologue
    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 35
    invoke-virtual {p1}, Lco/uk/getmondo/api/model/tax_residency/TaxResidencyInfo;->d()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_3

    check-cast v0, Ljava/lang/Iterable;

    .line 79
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    check-cast v1, Ljava/util/Collection;

    .line 80
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    move-object v0, v2

    check-cast v0, Lco/uk/getmondo/api/model/tax_residency/Jurisdiction;

    .line 35
    invoke-virtual {v0}, Lco/uk/getmondo/api/model/tax_residency/Jurisdiction;->f()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-virtual {v0}, Lco/uk/getmondo/api/model/tax_residency/Jurisdiction;->g()Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_1
    if-eqz v0, :cond_0

    invoke-interface {v1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    .line 81
    :cond_2
    check-cast v1, Ljava/util/List;

    .line 35
    :goto_2
    return-object v1

    :cond_3
    invoke-static {}, Lkotlin/a/m;->a()Ljava/util/List;

    move-result-object v1

    goto :goto_2
.end method
