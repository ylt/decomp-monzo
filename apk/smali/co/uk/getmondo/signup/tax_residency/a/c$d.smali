.class final Lco/uk/getmondo/signup/tax_residency/a/c$d;
.super Ljava/lang/Object;
.source "TaxResidencyManager.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lco/uk/getmondo/signup/tax_residency/a/c;->b()Lio/reactivex/v;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h",
        "<TT;",
        "Lio/reactivex/z",
        "<+TR;>;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000\u0016\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0000\u0010\u0000\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00030\u00020\u00012\u000c\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00020\u00030\u0005H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "Lio/reactivex/Single;",
        "Lcom/memoizrlabs/poweroptional/Optional;",
        "Lco/uk/getmondo/api/model/tax_residency/Jurisdiction;",
        "it",
        "",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field final synthetic a:Lco/uk/getmondo/signup/tax_residency/a/c;


# direct methods
.method constructor <init>(Lco/uk/getmondo/signup/tax_residency/a/c;)V
    .locals 0

    iput-object p1, p0, Lco/uk/getmondo/signup/tax_residency/a/c$d;->a:Lco/uk/getmondo/signup/tax_residency/a/c;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/util/List;)Lio/reactivex/v;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lco/uk/getmondo/api/model/tax_residency/Jurisdiction;",
            ">;)",
            "Lio/reactivex/v",
            "<",
            "Lcom/c/b/b",
            "<",
            "Lco/uk/getmondo/api/model/tax_residency/Jurisdiction;",
            ">;>;"
        }
    .end annotation

    .prologue
    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 36
    iget-object v0, p0, Lco/uk/getmondo/signup/tax_residency/a/c$d;->a:Lco/uk/getmondo/signup/tax_residency/a/c;

    invoke-static {v0, p1}, Lco/uk/getmondo/signup/tax_residency/a/c;->a(Lco/uk/getmondo/signup/tax_residency/a/c;Ljava/util/List;)Lio/reactivex/v;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 12
    check-cast p1, Ljava/util/List;

    invoke-virtual {p0, p1}, Lco/uk/getmondo/signup/tax_residency/a/c$d;->a(Ljava/util/List;)Lio/reactivex/v;

    move-result-object v0

    return-object v0
.end method
