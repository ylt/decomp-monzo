.class public final Lco/uk/getmondo/signup/tax_residency/a/c;
.super Ljava/lang/Object;
.source "TaxResidencyManager.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lco/uk/getmondo/signup/tax_residency/a/c$a;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000R\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0002\u0008\u0006\u0018\u0000 \u001f2\u00020\u0001:\u0001\u001fB\u000f\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\"\u0010\u0005\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00080\u00070\u00062\u000c\u0010\t\u001a\u0008\u0012\u0004\u0012\u00020\u00080\nH\u0002J(\u0010\u000b\u001a\u0012\u0012\u0004\u0012\u00020\r0\u000cj\u0008\u0012\u0004\u0012\u00020\r`\u000e2\u0006\u0010\u000f\u001a\u00020\u00102\u0006\u0010\u0011\u001a\u00020\u0012H\u0002J\u000c\u0010\u0013\u001a\u0008\u0012\u0004\u0012\u00020\u00100\u0006J$\u0010\u0014\u001a\u0018\u0012\u0014\u0012\u0012\u0012\u0004\u0012\u00020\r0\u000cj\u0008\u0012\u0004\u0012\u00020\r`\u000e0\u00062\u0006\u0010\u0011\u001a\u00020\u0012J\u0012\u0010\u0015\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00080\u00070\u0006J\u001e\u0010\u0016\u001a\u00020\u00172\u0006\u0010\u0018\u001a\u00020\u00082\u0006\u0010\u0019\u001a\u00020\u001a2\u0006\u0010\u001b\u001a\u00020\u001aJ\u0006\u0010\u001c\u001a\u00020\u0017J\u001c\u0010\u001d\u001a\u00020\u00172\u0006\u0010\u0011\u001a\u00020\u00122\u000c\u0010\u001e\u001a\u0008\u0012\u0004\u0012\u00020\r0\nR\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006 "
    }
    d2 = {
        "Lco/uk/getmondo/signup/tax_residency/data/TaxResidencyManager;",
        "",
        "api",
        "Lco/uk/getmondo/api/TaxResidencyApi;",
        "(Lco/uk/getmondo/api/TaxResidencyApi;)V",
        "checkIsLastJurisdiction",
        "Lio/reactivex/Single;",
        "Lcom/memoizrlabs/poweroptional/Optional;",
        "Lco/uk/getmondo/api/model/tax_residency/Jurisdiction;",
        "jurisdictions",
        "",
        "generateTaxCountriesList",
        "Ljava/util/ArrayList;",
        "Lco/uk/getmondo/signup/tax_residency/data/TaxCountry;",
        "Lkotlin/collections/ArrayList;",
        "taxResidency",
        "Lco/uk/getmondo/api/model/tax_residency/TaxResidencyInfo;",
        "isUsTaxResident",
        "",
        "getStatus",
        "getTaxCountries",
        "nextJurisdictionOrSubmit",
        "reportTin",
        "Lio/reactivex/Completable;",
        "jurisdiction",
        "tinId",
        "",
        "tinValue",
        "submitUkTaxResidentOnly",
        "updateSelfCertification",
        "countries",
        "Companion",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# static fields
.field public static final a:Lco/uk/getmondo/signup/tax_residency/a/c$a;


# instance fields
.field private final b:Lco/uk/getmondo/api/TaxResidencyApi;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lco/uk/getmondo/signup/tax_residency/a/c$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lco/uk/getmondo/signup/tax_residency/a/c$a;-><init>(Lkotlin/d/b/i;)V

    sput-object v0, Lco/uk/getmondo/signup/tax_residency/a/c;->a:Lco/uk/getmondo/signup/tax_residency/a/c$a;

    return-void
.end method

.method public constructor <init>(Lco/uk/getmondo/api/TaxResidencyApi;)V
    .locals 1

    .prologue
    const-string v0, "api"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lco/uk/getmondo/signup/tax_residency/a/c;->b:Lco/uk/getmondo/api/TaxResidencyApi;

    return-void
.end method

.method public static final synthetic a(Lco/uk/getmondo/signup/tax_residency/a/c;Ljava/util/List;)Lio/reactivex/v;
    .locals 1

    .prologue
    .line 12
    invoke-direct {p0, p1}, Lco/uk/getmondo/signup/tax_residency/a/c;->a(Ljava/util/List;)Lio/reactivex/v;

    move-result-object v0

    return-object v0
.end method

.method private final a(Ljava/util/List;)Lio/reactivex/v;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lco/uk/getmondo/api/model/tax_residency/Jurisdiction;",
            ">;)",
            "Lio/reactivex/v",
            "<",
            "Lcom/c/b/b",
            "<",
            "Lco/uk/getmondo/api/model/tax_residency/Jurisdiction;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 53
    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 54
    iget-object v0, p0, Lco/uk/getmondo/signup/tax_residency/a/c;->b:Lco/uk/getmondo/api/TaxResidencyApi;

    invoke-interface {v0}, Lco/uk/getmondo/api/TaxResidencyApi;->submit()Lio/reactivex/b;

    move-result-object v0

    .line 55
    invoke-static {}, Lcom/c/b/b;->c()Lcom/c/b/b;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/b;->a(Ljava/lang/Object;)Lio/reactivex/v;

    move-result-object v0

    const-string v1, "api.submit()\n           \u2026Default(Optional.empty())"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 53
    :goto_0
    return-object v0

    .line 57
    :cond_0
    const/4 v0, 0x0

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/c/b/b;->b(Ljava/lang/Object;)Lcom/c/b/b;

    move-result-object v0

    invoke-static {v0}, Lio/reactivex/v;->a(Ljava/lang/Object;)Lio/reactivex/v;

    move-result-object v0

    const-string v1, "Single.just(Optional.optionOf(jurisdictions[0]))"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private final a(Lco/uk/getmondo/api/model/tax_residency/TaxResidencyInfo;Z)Ljava/util/ArrayList;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lco/uk/getmondo/api/model/tax_residency/TaxResidencyInfo;",
            "Z)",
            "Ljava/util/ArrayList",
            "<",
            "Lco/uk/getmondo/signup/tax_residency/a/a;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 62
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 64
    sget-object v0, Lco/uk/getmondo/d/i;->Companion:Lco/uk/getmondo/d/i$a;

    invoke-virtual {v0}, Lco/uk/getmondo/d/i$a;->b()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v10

    move v6, v7

    :goto_0
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/d/i;

    invoke-virtual {v0}, Lco/uk/getmondo/d/i;->f()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Lco/uk/getmondo/d/i;->g()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Lco/uk/getmondo/d/i;->h()Ljava/lang/String;

    move-result-object v3

    .line 65
    sget-object v0, Lco/uk/getmondo/d/i;->UNITED_KINGDOM:Lco/uk/getmondo/d/i;

    invoke-virtual {v0}, Lco/uk/getmondo/d/i;->e()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lco/uk/getmondo/d/i;->UNITED_STATES:Lco/uk/getmondo/d/i;

    invoke-virtual {v0}, Lco/uk/getmondo/d/i;->e()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    if-eqz p2, :cond_1

    :cond_0
    move v4, v8

    .line 66
    :goto_1
    invoke-virtual {p1}, Lco/uk/getmondo/api/model/tax_residency/TaxResidencyInfo;->d()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_5

    check-cast v0, Ljava/lang/Iterable;

    .line 86
    instance-of v5, v0, Ljava/util/Collection;

    if-eqz v5, :cond_2

    move-object v5, v0

    check-cast v5, Ljava/util/Collection;

    invoke-interface {v5}, Ljava/util/Collection;->isEmpty()Z

    move-result v5

    if-eqz v5, :cond_2

    move v0, v7

    :goto_2
    move v5, v0

    .line 67
    :goto_3
    new-instance v0, Lco/uk/getmondo/signup/tax_residency/a/a;

    invoke-direct/range {v0 .. v5}, Lco/uk/getmondo/signup/tax_residency/a/a;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)V

    .line 68
    invoke-virtual {p1}, Lco/uk/getmondo/api/model/tax_residency/TaxResidencyInfo;->c()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1, v3}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 69
    add-int/lit8 v1, v6, 0x1

    invoke-virtual {v9, v6, v0}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    move v0, v1

    :goto_4
    move v6, v0

    .line 72
    goto :goto_0

    :cond_1
    move v4, v7

    .line 65
    goto :goto_1

    .line 87
    :cond_2
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_3
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/api/model/tax_residency/Jurisdiction;

    .line 66
    invoke-virtual {v0}, Lco/uk/getmondo/api/model/tax_residency/Jurisdiction;->e()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v3}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    move v0, v8

    goto :goto_2

    :cond_4
    move v0, v7

    .line 88
    goto :goto_2

    :cond_5
    move v5, v7

    .line 66
    goto :goto_3

    .line 71
    :cond_6
    invoke-virtual {v9, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move v0, v6

    goto :goto_4

    .line 74
    :cond_7
    return-object v9
.end method

.method public static final synthetic a(Lco/uk/getmondo/signup/tax_residency/a/c;Lco/uk/getmondo/api/model/tax_residency/TaxResidencyInfo;Z)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 12
    invoke-direct {p0, p1, p2}, Lco/uk/getmondo/signup/tax_residency/a/c;->a(Lco/uk/getmondo/api/model/tax_residency/TaxResidencyInfo;Z)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(Lco/uk/getmondo/api/model/tax_residency/Jurisdiction;Ljava/lang/String;Ljava/lang/String;)Lio/reactivex/b;
    .locals 2

    .prologue
    const-string v0, "jurisdiction"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "tinId"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "tinValue"

    invoke-static {p3, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 40
    const-string v0, "NO_TIN"

    invoke-static {p3, v0}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lco/uk/getmondo/api/model/tax_residency/Jurisdiction;->h()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 41
    iget-object v0, p0, Lco/uk/getmondo/signup/tax_residency/a/c;->b:Lco/uk/getmondo/api/TaxResidencyApi;

    invoke-virtual {p1}, Lco/uk/getmondo/api/model/tax_residency/Jurisdiction;->e()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lco/uk/getmondo/api/TaxResidencyApi;->noTin(Ljava/lang/String;)Lio/reactivex/b;

    move-result-object v0

    .line 40
    :goto_0
    return-object v0

    .line 43
    :cond_0
    iget-object v0, p0, Lco/uk/getmondo/signup/tax_residency/a/c;->b:Lco/uk/getmondo/api/TaxResidencyApi;

    invoke-virtual {p1}, Lco/uk/getmondo/api/model/tax_residency/Jurisdiction;->e()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, p2, p3}, Lco/uk/getmondo/api/TaxResidencyApi;->updateTin(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lio/reactivex/b;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(ZLjava/util/List;)Lio/reactivex/b;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "Ljava/util/List",
            "<",
            "Lco/uk/getmondo/signup/tax_residency/a/a;",
            ">;)",
            "Lio/reactivex/b;"
        }
    .end annotation

    .prologue
    const-string v0, "countries"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 29
    check-cast p2, Ljava/lang/Iterable;

    .line 79
    new-instance v0, Ljava/util/ArrayList;

    const/16 v1, 0xa

    invoke-static {p2, v1}, Lkotlin/a/m;->a(Ljava/lang/Iterable;I)I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v0, Ljava/util/Collection;

    .line 80
    invoke-interface {p2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 81
    check-cast v1, Lco/uk/getmondo/signup/tax_residency/a/a;

    .line 29
    invoke-virtual {v1}, Lco/uk/getmondo/signup/tax_residency/a/a;->c()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 82
    :cond_0
    check-cast v0, Ljava/util/List;

    check-cast v0, Ljava/util/Collection;

    .line 29
    nop

    .line 84
    invoke-interface {v0}, Ljava/util/Collection;->size()I

    move-result v1

    new-array v1, v1, [Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Collection;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_1

    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type kotlin.Array<T>"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 29
    :cond_1
    check-cast v0, [Ljava/lang/String;

    .line 30
    iget-object v1, p0, Lco/uk/getmondo/signup/tax_residency/a/c;->b:Lco/uk/getmondo/api/TaxResidencyApi;

    const/4 v2, 0x0

    invoke-interface {v1, v2, p1, v0}, Lco/uk/getmondo/api/TaxResidencyApi;->updateSelfCertification(ZZ[Ljava/lang/String;)Lio/reactivex/b;

    move-result-object v0

    return-object v0
.end method

.method public final a()Lio/reactivex/v;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/v",
            "<",
            "Lco/uk/getmondo/api/model/tax_residency/TaxResidencyInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 26
    iget-object v0, p0, Lco/uk/getmondo/signup/tax_residency/a/c;->b:Lco/uk/getmondo/api/TaxResidencyApi;

    invoke-interface {v0}, Lco/uk/getmondo/api/TaxResidencyApi;->status()Lio/reactivex/v;

    move-result-object v0

    return-object v0
.end method

.method public final a(Z)Lio/reactivex/v;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)",
            "Lio/reactivex/v",
            "<",
            "Ljava/util/ArrayList",
            "<",
            "Lco/uk/getmondo/signup/tax_residency/a/a;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 22
    invoke-virtual {p0}, Lco/uk/getmondo/signup/tax_residency/a/c;->a()Lio/reactivex/v;

    move-result-object v1

    .line 23
    new-instance v0, Lco/uk/getmondo/signup/tax_residency/a/c$b;

    invoke-direct {v0, p0, p1}, Lco/uk/getmondo/signup/tax_residency/a/c$b;-><init>(Lco/uk/getmondo/signup/tax_residency/a/c;Z)V

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {v1, v0}, Lio/reactivex/v;->d(Lio/reactivex/c/h;)Lio/reactivex/v;

    move-result-object v0

    const-string v1, "getStatus()\n            \u2026yInfo, isUsTaxResident) }"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public final b()Lio/reactivex/v;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/v",
            "<",
            "Lcom/c/b/b",
            "<",
            "Lco/uk/getmondo/api/model/tax_residency/Jurisdiction;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 34
    invoke-virtual {p0}, Lco/uk/getmondo/signup/tax_residency/a/c;->a()Lio/reactivex/v;

    move-result-object v1

    .line 35
    sget-object v0, Lco/uk/getmondo/signup/tax_residency/a/c$c;->a:Lco/uk/getmondo/signup/tax_residency/a/c$c;

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {v1, v0}, Lio/reactivex/v;->d(Lio/reactivex/c/h;)Lio/reactivex/v;

    move-result-object v1

    .line 36
    new-instance v0, Lco/uk/getmondo/signup/tax_residency/a/c$d;

    invoke-direct {v0, p0}, Lco/uk/getmondo/signup/tax_residency/a/c$d;-><init>(Lco/uk/getmondo/signup/tax_residency/a/c;)V

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {v1, v0}, Lio/reactivex/v;->a(Lio/reactivex/c/h;)Lio/reactivex/v;

    move-result-object v0

    const-string v1, "getStatus()\n            \u2026kIsLastJurisdiction(it) }"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public final c()Lio/reactivex/b;
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 48
    iget-object v1, p0, Lco/uk/getmondo/signup/tax_residency/a/c;->b:Lco/uk/getmondo/api/TaxResidencyApi;

    new-array v0, v4, [Ljava/lang/String;

    sget-object v2, Lco/uk/getmondo/d/i;->UNITED_KINGDOM:Lco/uk/getmondo/d/i;

    invoke-virtual {v2}, Lco/uk/getmondo/d/i;->e()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v3

    check-cast v0, [Ljava/lang/Object;

    .line 85
    check-cast v0, [Ljava/lang/String;

    .line 48
    invoke-interface {v1, v4, v3, v0}, Lco/uk/getmondo/api/TaxResidencyApi;->updateSelfCertification(ZZ[Ljava/lang/String;)Lio/reactivex/b;

    move-result-object v1

    .line 49
    iget-object v0, p0, Lco/uk/getmondo/signup/tax_residency/a/c;->b:Lco/uk/getmondo/api/TaxResidencyApi;

    invoke-interface {v0}, Lco/uk/getmondo/api/TaxResidencyApi;->submit()Lio/reactivex/b;

    move-result-object v0

    check-cast v0, Lio/reactivex/d;

    invoke-virtual {v1, v0}, Lio/reactivex/b;->b(Lio/reactivex/d;)Lio/reactivex/b;

    move-result-object v0

    const-string v1, "api.updateSelfCertificat\u2026   .andThen(api.submit())"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method
