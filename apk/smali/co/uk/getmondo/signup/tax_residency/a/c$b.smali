.class final Lco/uk/getmondo/signup/tax_residency/a/c$b;
.super Ljava/lang/Object;
.source "TaxResidencyManager.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lco/uk/getmondo/signup/tax_residency/a/c;->a(Z)Lio/reactivex/v;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h",
        "<TT;TR;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000\u0016\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u0012\u0012\u0004\u0012\u00020\u00020\u0001j\u0008\u0012\u0004\u0012\u00020\u0002`\u00032\u0006\u0010\u0004\u001a\u00020\u0005H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "Ljava/util/ArrayList;",
        "Lco/uk/getmondo/signup/tax_residency/data/TaxCountry;",
        "Lkotlin/collections/ArrayList;",
        "taxResidencyInfo",
        "Lco/uk/getmondo/api/model/tax_residency/TaxResidencyInfo;",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field final synthetic a:Lco/uk/getmondo/signup/tax_residency/a/c;

.field final synthetic b:Z


# direct methods
.method constructor <init>(Lco/uk/getmondo/signup/tax_residency/a/c;Z)V
    .locals 0

    iput-object p1, p0, Lco/uk/getmondo/signup/tax_residency/a/c$b;->a:Lco/uk/getmondo/signup/tax_residency/a/c;

    iput-boolean p2, p0, Lco/uk/getmondo/signup/tax_residency/a/c$b;->b:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 12
    check-cast p1, Lco/uk/getmondo/api/model/tax_residency/TaxResidencyInfo;

    invoke-virtual {p0, p1}, Lco/uk/getmondo/signup/tax_residency/a/c$b;->a(Lco/uk/getmondo/api/model/tax_residency/TaxResidencyInfo;)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lco/uk/getmondo/api/model/tax_residency/TaxResidencyInfo;)Ljava/util/ArrayList;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lco/uk/getmondo/api/model/tax_residency/TaxResidencyInfo;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lco/uk/getmondo/signup/tax_residency/a/a;",
            ">;"
        }
    .end annotation

    .prologue
    const-string v0, "taxResidencyInfo"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 23
    iget-object v0, p0, Lco/uk/getmondo/signup/tax_residency/a/c$b;->a:Lco/uk/getmondo/signup/tax_residency/a/c;

    iget-boolean v1, p0, Lco/uk/getmondo/signup/tax_residency/a/c$b;->b:Z

    invoke-static {v0, p1, v1}, Lco/uk/getmondo/signup/tax_residency/a/c;->a(Lco/uk/getmondo/signup/tax_residency/a/c;Lco/uk/getmondo/api/model/tax_residency/TaxResidencyInfo;Z)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method
