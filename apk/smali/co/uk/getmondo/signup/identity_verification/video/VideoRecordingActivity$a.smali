.class public final Lco/uk/getmondo/signup/identity_verification/video/VideoRecordingActivity$a;
.super Ljava/lang/Object;
.source "VideoRecordingActivity.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lco/uk/getmondo/signup/identity_verification/video/VideoRecordingActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000<\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0002\u0008\u0002\n\u0002\u0010\t\n\u0002\u0008\u0003\n\u0002\u0010\u0008\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0008\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J \u0010\u000f\u001a\u00020\u00102\u0006\u0010\u0011\u001a\u00020\u00122\u0006\u0010\u0013\u001a\u00020\u00142\u0006\u0010\u0015\u001a\u00020\u0016H\u0007R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0082T\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0008\u0010\tR\u000e\u0010\n\u001a\u00020\u000bX\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000c\u001a\u00020\u000bX\u0082T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u000bX\u0082T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u0004X\u0082T\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0017"
    }
    d2 = {
        "Lco/uk/getmondo/signup/identity_verification/video/VideoRecordingActivity$Companion;",
        "",
        "()V",
        "KEY_SIGNUP_SOURCE",
        "",
        "KEY_VERSION",
        "MAX_RECORDING_TIME",
        "",
        "getMAX_RECORDING_TIME",
        "()J",
        "MAX_RECORDING_TIME_SECONDS",
        "",
        "REQUEST_CAMERA_RECORD_AUDIO_PERMISSION",
        "REQUEST_CODE_VIDEO_PLAYBACK",
        "TAG_ERROR_DIALOG_FRAGMENT",
        "buildIntent",
        "Landroid/content/Intent;",
        "context",
        "Landroid/content/Context;",
        "version",
        "Lco/uk/getmondo/signup/identity_verification/data/IdentityVerificationVersion;",
        "signupSource",
        "Lco/uk/getmondo/api/model/signup/SignupSource;",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 219
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/d/b/i;)V
    .locals 0

    .prologue
    .line 219
    invoke-direct {p0}, Lco/uk/getmondo/signup/identity_verification/video/VideoRecordingActivity$a;-><init>()V

    return-void
.end method

.method private final a()J
    .locals 2

    .prologue
    .line 228
    invoke-static {}, Lco/uk/getmondo/signup/identity_verification/video/VideoRecordingActivity;->H()J

    move-result-wide v0

    return-wide v0
.end method

.method public static final synthetic a(Lco/uk/getmondo/signup/identity_verification/video/VideoRecordingActivity$a;)J
    .locals 2

    .prologue
    .line 219
    invoke-direct {p0}, Lco/uk/getmondo/signup/identity_verification/video/VideoRecordingActivity$a;->a()J

    move-result-wide v0

    return-wide v0
.end method


# virtual methods
.method public final a(Landroid/content/Context;Lco/uk/getmondo/signup/identity_verification/a/j;Lco/uk/getmondo/api/model/signup/SignupSource;)Landroid/content/Intent;
    .locals 2

    .prologue
    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "version"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "signupSource"

    invoke-static {p3, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 235
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lco/uk/getmondo/signup/identity_verification/video/VideoRecordingActivity;

    invoke-direct {v0, p1, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 236
    const-string v1, "KEY_VERSION"

    check-cast p2, Ljava/io/Serializable;

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    move-result-object v0

    .line 237
    const-string v1, "KEY_SIGNUP_SOURCE"

    check-cast p3, Ljava/io/Serializable;

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "Intent(context, VideoRec\u2026NUP_SOURCE, signupSource)"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method
