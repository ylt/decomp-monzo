.class public final Lco/uk/getmondo/signup/identity_verification/video/ab;
.super Ljava/lang/Object;
.source "VideoStringProvider.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0002\u0008\u0003\u0018\u00002\u00020\u0001B\u0019\u0008\u0007\u0012\u0008\u0008\u0001\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\u0006\u0010\u0007\u001a\u00020\u0008J\u0006\u0010\t\u001a\u00020\u0008J\u0006\u0010\n\u001a\u00020\u0008R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000b"
    }
    d2 = {
        "Lco/uk/getmondo/signup/identity_verification/video/VideoStringProvider;",
        "",
        "context",
        "Landroid/content/Context;",
        "accountService",
        "Lco/uk/getmondo/common/accounts/AccountService;",
        "(Landroid/content/Context;Lco/uk/getmondo/common/accounts/AccountService;)V",
        "selfieConfirmation",
        "",
        "selfieInstructions",
        "selfieTextToRead",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Lco/uk/getmondo/common/accounts/d;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lco/uk/getmondo/common/accounts/d;)V
    .locals 1

    .prologue
    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "accountService"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lco/uk/getmondo/signup/identity_verification/video/ab;->a:Landroid/content/Context;

    iput-object p2, p0, Lco/uk/getmondo/signup/identity_verification/video/ab;->b:Lco/uk/getmondo/common/accounts/d;

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 5

    .prologue
    .line 17
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/video/ab;->b:Lco/uk/getmondo/common/accounts/d;

    invoke-virtual {v0}, Lco/uk/getmondo/common/accounts/d;->b()Lco/uk/getmondo/d/ak;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lco/uk/getmondo/d/ak;->d()Lco/uk/getmondo/d/ac;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lco/uk/getmondo/d/ac;->c()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    :goto_0
    invoke-static {v0}, Lco/uk/getmondo/common/k/p;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 18
    sget-object v0, Lco/uk/getmondo/a;->c:Ljava/lang/Boolean;

    const-string v2, "BuildConfig.BANK"

    invoke-static {v0, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    const v0, 0x7f0a0216

    .line 19
    :goto_1
    iget-object v2, p0, Lco/uk/getmondo/signup/identity_verification/video/ab;->a:Landroid/content/Context;

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v1, v3, v4

    invoke-virtual {v2, v0, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "context.getString(textResource, name)"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0

    .line 17
    :cond_0
    const-string v0, ""

    goto :goto_0

    .line 18
    :cond_1
    const v0, 0x7f0a0217

    goto :goto_1
.end method

.method public final b()Ljava/lang/String;
    .locals 2

    .prologue
    .line 23
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/video/ab;->a:Landroid/content/Context;

    const v1, 0x7f0a0221

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "context.getString(R.stri\u2026ra_fallback_instructions)"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 2

    .prologue
    .line 27
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/video/ab;->a:Landroid/content/Context;

    const v1, 0x7f0a0220

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "context.getString(R.stri\u2026ra_fallback_confirmation)"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method
