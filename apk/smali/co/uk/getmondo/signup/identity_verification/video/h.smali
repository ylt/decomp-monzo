.class Lco/uk/getmondo/signup/identity_verification/video/h;
.super Lco/uk/getmondo/common/ui/b;
.source "VideoPlaybackPresenter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lco/uk/getmondo/signup/identity_verification/video/h$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lco/uk/getmondo/common/ui/b",
        "<",
        "Lco/uk/getmondo/signup/identity_verification/video/h$a;",
        ">;"
    }
.end annotation


# instance fields
.field private final c:Lco/uk/getmondo/common/a;

.field private final d:Ljava/lang/String;


# direct methods
.method constructor <init>(Lco/uk/getmondo/common/a;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Lco/uk/getmondo/common/ui/b;-><init>()V

    .line 20
    iput-object p1, p0, Lco/uk/getmondo/signup/identity_verification/video/h;->c:Lco/uk/getmondo/common/a;

    .line 21
    iput-object p2, p0, Lco/uk/getmondo/signup/identity_verification/video/h;->d:Ljava/lang/String;

    .line 22
    return-void
.end method

.method static synthetic a(Lco/uk/getmondo/signup/identity_verification/video/h$a;Ljava/lang/Boolean;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 35
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 36
    invoke-interface {p0}, Lco/uk/getmondo/signup/identity_verification/video/h$a;->g()V

    .line 40
    :goto_0
    return-void

    .line 38
    :cond_0
    invoke-interface {p0}, Lco/uk/getmondo/signup/identity_verification/video/h$a;->f()V

    goto :goto_0
.end method

.method static synthetic a(Lco/uk/getmondo/signup/identity_verification/video/h$a;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 31
    invoke-interface {p0}, Lco/uk/getmondo/signup/identity_verification/video/h$a;->d()V

    return-void
.end method

.method static synthetic a(Lco/uk/getmondo/signup/identity_verification/video/h;Lco/uk/getmondo/signup/identity_verification/video/h$a;Ljava/lang/Object;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 44
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/video/h;->c:Lco/uk/getmondo/common/a;

    invoke-static {}, Lco/uk/getmondo/api/model/tracking/Impression;->D()Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v1

    invoke-virtual {v0, v1}, Lco/uk/getmondo/common/a;->a(Lco/uk/getmondo/api/model/tracking/Impression;)V

    .line 45
    invoke-interface {p1}, Lco/uk/getmondo/signup/identity_verification/video/h$a;->e()V

    .line 46
    return-void
.end method


# virtual methods
.method public bridge synthetic a(Lco/uk/getmondo/common/ui/f;)V
    .locals 0

    .prologue
    .line 12
    check-cast p1, Lco/uk/getmondo/signup/identity_verification/video/h$a;

    invoke-virtual {p0, p1}, Lco/uk/getmondo/signup/identity_verification/video/h;->a(Lco/uk/getmondo/signup/identity_verification/video/h$a;)V

    return-void
.end method

.method public a(Lco/uk/getmondo/signup/identity_verification/video/h$a;)V
    .locals 2

    .prologue
    .line 26
    invoke-super {p0, p1}, Lco/uk/getmondo/common/ui/b;->a(Lco/uk/getmondo/common/ui/f;)V

    .line 28
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/video/h;->d:Ljava/lang/String;

    invoke-interface {p1, v0}, Lco/uk/getmondo/signup/identity_verification/video/h$a;->a(Ljava/lang/String;)V

    .line 30
    invoke-interface {p1}, Lco/uk/getmondo/signup/identity_verification/video/h$a;->a()Lio/reactivex/n;

    move-result-object v0

    invoke-static {p1}, Lco/uk/getmondo/signup/identity_verification/video/i;->a(Lco/uk/getmondo/signup/identity_verification/video/h$a;)Lio/reactivex/c/g;

    move-result-object v1

    .line 31
    invoke-virtual {v0, v1}, Lio/reactivex/n;->subscribe(Lio/reactivex/c/g;)Lio/reactivex/b/b;

    move-result-object v0

    .line 30
    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/identity_verification/video/h;->a(Lio/reactivex/b/b;)V

    .line 33
    invoke-interface {p1}, Lco/uk/getmondo/signup/identity_verification/video/h$a;->c()Lio/reactivex/n;

    move-result-object v0

    invoke-static {p1}, Lco/uk/getmondo/signup/identity_verification/video/j;->a(Lco/uk/getmondo/signup/identity_verification/video/h$a;)Lio/reactivex/c/g;

    move-result-object v1

    .line 34
    invoke-virtual {v0, v1}, Lio/reactivex/n;->subscribe(Lio/reactivex/c/g;)Lio/reactivex/b/b;

    move-result-object v0

    .line 33
    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/identity_verification/video/h;->a(Lio/reactivex/b/b;)V

    .line 42
    invoke-interface {p1}, Lco/uk/getmondo/signup/identity_verification/video/h$a;->b()Lio/reactivex/n;

    move-result-object v0

    invoke-static {p0, p1}, Lco/uk/getmondo/signup/identity_verification/video/k;->a(Lco/uk/getmondo/signup/identity_verification/video/h;Lco/uk/getmondo/signup/identity_verification/video/h$a;)Lio/reactivex/c/g;

    move-result-object v1

    .line 43
    invoke-virtual {v0, v1}, Lio/reactivex/n;->subscribe(Lio/reactivex/c/g;)Lio/reactivex/b/b;

    move-result-object v0

    .line 42
    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/identity_verification/video/h;->a(Lio/reactivex/b/b;)V

    .line 47
    return-void
.end method
