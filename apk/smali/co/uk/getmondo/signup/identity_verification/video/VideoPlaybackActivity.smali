.class public final Lco/uk/getmondo/signup/identity_verification/video/VideoPlaybackActivity;
.super Lco/uk/getmondo/common/activities/b;
.source "VideoPlaybackActivity.kt"

# interfaces
.implements Lco/uk/getmondo/signup/identity_verification/video/h$a;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lco/uk/getmondo/signup/identity_verification/video/VideoPlaybackActivity$a;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000H\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0010\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0002\u0008\u0004\n\u0002\u0010\u000e\n\u0002\u0008\u0004\u0018\u0000 \u001e2\u00020\u00012\u00020\u0002:\u0001\u001eB\u0005\u00a2\u0006\u0002\u0010\u0003J\u0008\u0010\u000c\u001a\u00020\rH\u0016J\u0012\u0010\u000e\u001a\u00020\r2\u0008\u0010\u000f\u001a\u0004\u0018\u00010\u0010H\u0014J\u0008\u0010\u0011\u001a\u00020\rH\u0014J\u000e\u0010\u0012\u001a\u0008\u0012\u0004\u0012\u00020\u00140\u0013H\u0016J\u000e\u0010\u0015\u001a\u0008\u0012\u0004\u0012\u00020\u00160\u0013H\u0016J\u000e\u0010\u0017\u001a\u0008\u0012\u0004\u0012\u00020\u00160\u0013H\u0016J\u0008\u0010\u0018\u001a\u00020\rH\u0016J\u0010\u0010\u0019\u001a\u00020\r2\u0006\u0010\u001a\u001a\u00020\u001bH\u0016J\u0008\u0010\u001c\u001a\u00020\rH\u0016J\u0008\u0010\u001d\u001a\u00020\rH\u0016R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082.\u00a2\u0006\u0002\n\u0000R\u001e\u0010\u0006\u001a\u00020\u00078\u0000@\u0000X\u0081.\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u0008\u0010\t\"\u0004\u0008\n\u0010\u000b\u00a8\u0006\u001f"
    }
    d2 = {
        "Lco/uk/getmondo/signup/identity_verification/video/VideoPlaybackActivity;",
        "Lco/uk/getmondo/common/activities/BaseActivity;",
        "Lco/uk/getmondo/signup/identity_verification/video/VideoPlaybackPresenter$View;",
        "()V",
        "exoPlayer",
        "Lcom/google/android/exoplayer2/SimpleExoPlayer;",
        "presenter",
        "Lco/uk/getmondo/signup/identity_verification/video/VideoPlaybackPresenter;",
        "getPresenter$app_monzoPrepaidRelease",
        "()Lco/uk/getmondo/signup/identity_verification/video/VideoPlaybackPresenter;",
        "setPresenter$app_monzoPrepaidRelease",
        "(Lco/uk/getmondo/signup/identity_verification/video/VideoPlaybackPresenter;)V",
        "mute",
        "",
        "onCreate",
        "savedInstanceState",
        "Landroid/os/Bundle;",
        "onDestroy",
        "onMuteClicked",
        "Lio/reactivex/Observable;",
        "",
        "onUseVideoClicked",
        "",
        "onVideoPlayerTouched",
        "restartVideo",
        "startVideo",
        "videoPath",
        "",
        "unmute",
        "useVideo",
        "Companion",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# static fields
.field public static final b:Lco/uk/getmondo/signup/identity_verification/video/VideoPlaybackActivity$a;


# instance fields
.field public a:Lco/uk/getmondo/signup/identity_verification/video/h;

.field private c:Lcom/google/android/exoplayer2/t;

.field private e:Ljava/util/HashMap;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lco/uk/getmondo/signup/identity_verification/video/VideoPlaybackActivity$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lco/uk/getmondo/signup/identity_verification/video/VideoPlaybackActivity$a;-><init>(Lkotlin/d/b/i;)V

    sput-object v0, Lco/uk/getmondo/signup/identity_verification/video/VideoPlaybackActivity;->b:Lco/uk/getmondo/signup/identity_verification/video/VideoPlaybackActivity$a;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Lco/uk/getmondo/common/activities/b;-><init>()V

    return-void
.end method


# virtual methods
.method public a(I)Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/video/VideoPlaybackActivity;->e:Ljava/util/HashMap;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lco/uk/getmondo/signup/identity_verification/video/VideoPlaybackActivity;->e:Ljava/util/HashMap;

    :cond_0
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/video/VideoPlaybackActivity;->e:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    if-nez v0, :cond_1

    invoke-virtual {p0, p1}, Landroid/support/v4/app/j;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/signup/identity_verification/video/VideoPlaybackActivity;->e:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    return-object v0
.end method

.method public a()Lio/reactivex/n;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 53
    sget v0, Lco/uk/getmondo/c$a;->exoPlayerView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/identity_verification/video/VideoPlaybackActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/exoplayer2/ui/SimpleExoPlayerView;

    invoke-static {v0}, Lcom/b/a/c/c;->c(Landroid/view/View;)Lio/reactivex/n;

    move-result-object v1

    .line 54
    sget-object v0, Lco/uk/getmondo/signup/identity_verification/video/VideoPlaybackActivity$c;->a:Lco/uk/getmondo/signup/identity_verification/video/VideoPlaybackActivity$c;

    check-cast v0, Lio/reactivex/c/q;

    invoke-virtual {v1, v0}, Lio/reactivex/n;->filter(Lio/reactivex/c/q;)Lio/reactivex/n;

    move-result-object v1

    .line 55
    sget-object v0, Lco/uk/getmondo/signup/identity_verification/video/VideoPlaybackActivity$d;->a:Lco/uk/getmondo/signup/identity_verification/video/VideoPlaybackActivity$d;

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {v1, v0}, Lio/reactivex/n;->map(Lio/reactivex/c/h;)Lio/reactivex/n;

    move-result-object v0

    const-string v1, "RxView.touches(exoPlayer\u2026    .map { Event.IGNORE }"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public a(Ljava/lang/String;)V
    .locals 6

    .prologue
    const/4 v4, 0x0

    const-string v0, "videoPath"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 68
    new-instance v2, Lcom/google/android/exoplayer2/upstream/h;

    move-object v0, p0

    check-cast v0, Landroid/content/Context;

    move-object v1, p0

    check-cast v1, Landroid/content/Context;

    const-string v3, "monzo"

    invoke-static {v1, v3}, Lcom/google/android/exoplayer2/util/s;->a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v0, v1, v4}, Lcom/google/android/exoplayer2/upstream/h;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/exoplayer2/upstream/l;)V

    .line 69
    new-instance v0, Lcom/google/android/exoplayer2/source/f;

    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    check-cast v2, Lcom/google/android/exoplayer2/upstream/c$a;

    .line 70
    new-instance v3, Lcom/google/android/exoplayer2/extractor/c;

    invoke-direct {v3}, Lcom/google/android/exoplayer2/extractor/c;-><init>()V

    check-cast v3, Lcom/google/android/exoplayer2/extractor/h;

    move-object v5, v4

    .line 69
    invoke-direct/range {v0 .. v5}, Lcom/google/android/exoplayer2/source/f;-><init>(Landroid/net/Uri;Lcom/google/android/exoplayer2/upstream/c$a;Lcom/google/android/exoplayer2/extractor/h;Landroid/os/Handler;Lcom/google/android/exoplayer2/source/f$a;)V

    .line 71
    iget-object v1, p0, Lco/uk/getmondo/signup/identity_verification/video/VideoPlaybackActivity;->c:Lcom/google/android/exoplayer2/t;

    if-nez v1, :cond_0

    const-string v2, "exoPlayer"

    invoke-static {v2}, Lkotlin/d/b/l;->b(Ljava/lang/String;)V

    :cond_0
    check-cast v0, Lcom/google/android/exoplayer2/source/i;

    invoke-virtual {v1, v0}, Lcom/google/android/exoplayer2/t;->a(Lcom/google/android/exoplayer2/source/i;)V

    .line 72
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/video/VideoPlaybackActivity;->c:Lcom/google/android/exoplayer2/t;

    if-nez v0, :cond_1

    const-string v1, "exoPlayer"

    invoke-static {v1}, Lkotlin/d/b/l;->b(Ljava/lang/String;)V

    :cond_1
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/exoplayer2/t;->a(Z)V

    .line 73
    sget v0, Lco/uk/getmondo/c$a;->muteButton:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/identity_verification/video/VideoPlaybackActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/signup/identity_verification/video/MuteButton;

    invoke-virtual {v0}, Lco/uk/getmondo/signup/identity_verification/video/MuteButton;->a()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 74
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/video/VideoPlaybackActivity;->c:Lcom/google/android/exoplayer2/t;

    if-nez v0, :cond_2

    const-string v1, "exoPlayer"

    invoke-static {v1}, Lkotlin/d/b/l;->b(Ljava/lang/String;)V

    :cond_2
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/exoplayer2/t;->a(F)V

    .line 76
    :cond_3
    return-void
.end method

.method public b()Lio/reactivex/n;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 59
    sget v0, Lco/uk/getmondo/c$a;->useThisVideoButton:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/identity_verification/video/VideoPlaybackActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    invoke-static {v0}, Lcom/b/a/c/c;->a(Landroid/view/View;)Lio/reactivex/n;

    move-result-object v0

    const-string v1, "RxView.clicks(useThisVideoButton)"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public c()Lio/reactivex/n;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .prologue
    .line 63
    sget v0, Lco/uk/getmondo/c$a;->muteButton:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/identity_verification/video/VideoPlaybackActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/signup/identity_verification/video/MuteButton;

    invoke-static {v0}, Lcom/b/a/c/c;->a(Landroid/view/View;)Lio/reactivex/n;

    move-result-object v1

    .line 64
    new-instance v0, Lco/uk/getmondo/signup/identity_verification/video/VideoPlaybackActivity$b;

    invoke-direct {v0, p0}, Lco/uk/getmondo/signup/identity_verification/video/VideoPlaybackActivity$b;-><init>(Lco/uk/getmondo/signup/identity_verification/video/VideoPlaybackActivity;)V

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {v1, v0}, Lio/reactivex/n;->map(Lio/reactivex/c/h;)Lio/reactivex/n;

    move-result-object v0

    const-string v1, "RxView.clicks(muteButton\u2026ap { muteButton.isMuted }"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public d()V
    .locals 4

    .prologue
    .line 80
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/video/VideoPlaybackActivity;->c:Lcom/google/android/exoplayer2/t;

    if-nez v0, :cond_0

    const-string v1, "exoPlayer"

    invoke-static {v1}, Lkotlin/d/b/l;->b(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0}, Lcom/google/android/exoplayer2/t;->a()I

    move-result v0

    const/4 v1, 0x3

    if-ne v0, v1, :cond_3

    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/video/VideoPlaybackActivity;->c:Lcom/google/android/exoplayer2/t;

    if-nez v0, :cond_1

    const-string v1, "exoPlayer"

    invoke-static {v1}, Lkotlin/d/b/l;->b(Ljava/lang/String;)V

    :cond_1
    invoke-virtual {v0}, Lcom/google/android/exoplayer2/t;->b()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 87
    :cond_2
    :goto_0
    return-void

    .line 84
    :cond_3
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/video/VideoPlaybackActivity;->c:Lcom/google/android/exoplayer2/t;

    if-nez v0, :cond_4

    const-string v1, "exoPlayer"

    invoke-static {v1}, Lkotlin/d/b/l;->b(Ljava/lang/String;)V

    :cond_4
    invoke-virtual {v0}, Lcom/google/android/exoplayer2/t;->a()I

    move-result v0

    const/4 v1, 0x4

    if-ne v0, v1, :cond_2

    .line 85
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/video/VideoPlaybackActivity;->c:Lcom/google/android/exoplayer2/t;

    if-nez v0, :cond_5

    const-string v1, "exoPlayer"

    invoke-static {v1}, Lkotlin/d/b/l;->b(Ljava/lang/String;)V

    :cond_5
    const-wide/16 v2, 0x0

    invoke-virtual {v0, v2, v3}, Lcom/google/android/exoplayer2/t;->a(J)V

    goto :goto_0
.end method

.method public e()V
    .locals 1

    .prologue
    .line 90
    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/identity_verification/video/VideoPlaybackActivity;->setResult(I)V

    .line 91
    invoke-virtual {p0}, Lco/uk/getmondo/signup/identity_verification/video/VideoPlaybackActivity;->finish()V

    .line 92
    return-void
.end method

.method public f()V
    .locals 2

    .prologue
    .line 95
    sget v0, Lco/uk/getmondo/c$a;->muteButton:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/identity_verification/video/VideoPlaybackActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/signup/identity_verification/video/MuteButton;

    invoke-virtual {v0}, Lco/uk/getmondo/signup/identity_verification/video/MuteButton;->b()V

    .line 96
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/video/VideoPlaybackActivity;->c:Lcom/google/android/exoplayer2/t;

    if-nez v0, :cond_0

    const-string v1, "exoPlayer"

    invoke-static {v1}, Lkotlin/d/b/l;->b(Ljava/lang/String;)V

    :cond_0
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/exoplayer2/t;->a(F)V

    .line 97
    return-void
.end method

.method public g()V
    .locals 2

    .prologue
    .line 100
    sget v0, Lco/uk/getmondo/c$a;->muteButton:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/identity_verification/video/VideoPlaybackActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/signup/identity_verification/video/MuteButton;

    invoke-virtual {v0}, Lco/uk/getmondo/signup/identity_verification/video/MuteButton;->b()V

    .line 101
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/video/VideoPlaybackActivity;->c:Lcom/google/android/exoplayer2/t;

    if-nez v0, :cond_0

    const-string v1, "exoPlayer"

    invoke-static {v1}, Lkotlin/d/b/l;->b(Ljava/lang/String;)V

    :cond_0
    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Lcom/google/android/exoplayer2/t;->a(F)V

    .line 102
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 33
    invoke-super {p0, p1}, Lco/uk/getmondo/common/activities/b;->onCreate(Landroid/os/Bundle;)V

    .line 34
    const v0, 0x7f050046

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/identity_verification/video/VideoPlaybackActivity;->setContentView(I)V

    .line 36
    invoke-virtual {p0}, Lco/uk/getmondo/signup/identity_verification/video/VideoPlaybackActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "KEY_VIDEO_PATH"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 38
    invoke-virtual {p0}, Lco/uk/getmondo/signup/identity_verification/video/VideoPlaybackActivity;->l()Lco/uk/getmondo/common/h/b/b;

    move-result-object v1

    new-instance v2, Lco/uk/getmondo/signup/identity_verification/video/f;

    invoke-direct {v2, v0}, Lco/uk/getmondo/signup/identity_verification/video/f;-><init>(Ljava/lang/String;)V

    invoke-interface {v1, v2}, Lco/uk/getmondo/common/h/b/b;->a(Lco/uk/getmondo/signup/identity_verification/video/f;)Lco/uk/getmondo/signup/identity_verification/video/e;

    move-result-object v0

    invoke-interface {v0, p0}, Lco/uk/getmondo/signup/identity_verification/video/e;->a(Lco/uk/getmondo/signup/identity_verification/video/VideoPlaybackActivity;)V

    move-object v0, p0

    .line 40
    check-cast v0, Landroid/content/Context;

    new-instance v1, Lcom/google/android/exoplayer2/b/b;

    invoke-direct {v1}, Lcom/google/android/exoplayer2/b/b;-><init>()V

    check-cast v1, Lcom/google/android/exoplayer2/b/g;

    new-instance v2, Lcom/google/android/exoplayer2/c;

    invoke-direct {v2}, Lcom/google/android/exoplayer2/c;-><init>()V

    check-cast v2, Lcom/google/android/exoplayer2/l;

    invoke-static {v0, v1, v2}, Lcom/google/android/exoplayer2/f;->a(Landroid/content/Context;Lcom/google/android/exoplayer2/b/g;Lcom/google/android/exoplayer2/l;)Lcom/google/android/exoplayer2/t;

    move-result-object v0

    const-string v1, "ExoPlayerFactory.newSimp\u2026(), DefaultLoadControl())"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v0, p0, Lco/uk/getmondo/signup/identity_verification/video/VideoPlaybackActivity;->c:Lcom/google/android/exoplayer2/t;

    .line 41
    sget v0, Lco/uk/getmondo/c$a;->exoPlayerView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/identity_verification/video/VideoPlaybackActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/exoplayer2/ui/SimpleExoPlayerView;

    iget-object v1, p0, Lco/uk/getmondo/signup/identity_verification/video/VideoPlaybackActivity;->c:Lcom/google/android/exoplayer2/t;

    if-nez v1, :cond_0

    const-string v2, "exoPlayer"

    invoke-static {v2}, Lkotlin/d/b/l;->b(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0, v1}, Lcom/google/android/exoplayer2/ui/SimpleExoPlayerView;->setPlayer(Lcom/google/android/exoplayer2/t;)V

    .line 43
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/video/VideoPlaybackActivity;->a:Lco/uk/getmondo/signup/identity_verification/video/h;

    if-nez v0, :cond_1

    const-string v1, "presenter"

    invoke-static {v1}, Lkotlin/d/b/l;->b(Ljava/lang/String;)V

    :cond_1
    check-cast p0, Lco/uk/getmondo/signup/identity_verification/video/h$a;

    invoke-virtual {v0, p0}, Lco/uk/getmondo/signup/identity_verification/video/h;->a(Lco/uk/getmondo/signup/identity_verification/video/h$a;)V

    .line 44
    return-void

    .line 36
    :cond_2
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "A path to the video file is required"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    .line 47
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/video/VideoPlaybackActivity;->a:Lco/uk/getmondo/signup/identity_verification/video/h;

    if-nez v0, :cond_0

    const-string v1, "presenter"

    invoke-static {v1}, Lkotlin/d/b/l;->b(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0}, Lco/uk/getmondo/signup/identity_verification/video/h;->b()V

    .line 48
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/video/VideoPlaybackActivity;->c:Lcom/google/android/exoplayer2/t;

    if-nez v0, :cond_1

    const-string v1, "exoPlayer"

    invoke-static {v1}, Lkotlin/d/b/l;->b(Ljava/lang/String;)V

    :cond_1
    invoke-virtual {v0}, Lcom/google/android/exoplayer2/t;->d()V

    .line 49
    invoke-super {p0}, Lco/uk/getmondo/common/activities/b;->onDestroy()V

    .line 50
    return-void
.end method
