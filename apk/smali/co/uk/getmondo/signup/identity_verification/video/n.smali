.class public Lco/uk/getmondo/signup/identity_verification/video/n;
.super Lco/uk/getmondo/common/ui/b;
.source "VideoRecordingPresenter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lco/uk/getmondo/signup/identity_verification/video/n$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lco/uk/getmondo/common/ui/b",
        "<",
        "Lco/uk/getmondo/signup/identity_verification/video/n$a;",
        ">;"
    }
.end annotation


# instance fields
.field private final c:Lio/reactivex/u;

.field private final d:Lio/reactivex/u;

.field private final e:Lco/uk/getmondo/signup/identity_verification/a/e;

.field private final f:Lco/uk/getmondo/common/a;

.field private final g:Lco/uk/getmondo/common/q;

.field private final h:Lco/uk/getmondo/signup/identity_verification/video/a;

.field private final i:Ljava/lang/String;

.field private final j:Lco/uk/getmondo/signup/identity_verification/video/ab;


# direct methods
.method constructor <init>(Lio/reactivex/u;Lio/reactivex/u;Lco/uk/getmondo/signup/identity_verification/a/e;Lco/uk/getmondo/common/a;Lco/uk/getmondo/common/q;Lco/uk/getmondo/signup/identity_verification/video/a;Lco/uk/getmondo/signup/identity_verification/a/a;Lco/uk/getmondo/signup/identity_verification/video/ab;)V
    .locals 1

    .prologue
    .line 37
    invoke-direct {p0}, Lco/uk/getmondo/common/ui/b;-><init>()V

    .line 38
    iput-object p1, p0, Lco/uk/getmondo/signup/identity_verification/video/n;->c:Lio/reactivex/u;

    .line 39
    iput-object p2, p0, Lco/uk/getmondo/signup/identity_verification/video/n;->d:Lio/reactivex/u;

    .line 40
    iput-object p3, p0, Lco/uk/getmondo/signup/identity_verification/video/n;->e:Lco/uk/getmondo/signup/identity_verification/a/e;

    .line 41
    iput-object p4, p0, Lco/uk/getmondo/signup/identity_verification/video/n;->f:Lco/uk/getmondo/common/a;

    .line 42
    iput-object p5, p0, Lco/uk/getmondo/signup/identity_verification/video/n;->g:Lco/uk/getmondo/common/q;

    .line 43
    iput-object p6, p0, Lco/uk/getmondo/signup/identity_verification/video/n;->h:Lco/uk/getmondo/signup/identity_verification/video/a;

    .line 44
    invoke-virtual {p7}, Lco/uk/getmondo/signup/identity_verification/a/a;->a()Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/signup/identity_verification/video/n;->i:Ljava/lang/String;

    .line 45
    iput-object p8, p0, Lco/uk/getmondo/signup/identity_verification/video/n;->j:Lco/uk/getmondo/signup/identity_verification/video/ab;

    .line 46
    return-void
.end method

.method static synthetic a(Lco/uk/getmondo/signup/identity_verification/video/n;Ljava/lang/Object;)Lio/reactivex/z;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 88
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/video/n;->h:Lco/uk/getmondo/signup/identity_verification/video/a;

    iget-object v1, p0, Lco/uk/getmondo/signup/identity_verification/video/n;->i:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lco/uk/getmondo/signup/identity_verification/video/a;->a(Ljava/lang/String;)Lio/reactivex/v;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/signup/identity_verification/video/n;->d:Lio/reactivex/u;

    .line 89
    invoke-virtual {v0, v1}, Lio/reactivex/v;->b(Lio/reactivex/u;)Lio/reactivex/v;

    move-result-object v0

    invoke-static {p0}, Lco/uk/getmondo/signup/identity_verification/video/r;->a(Lco/uk/getmondo/signup/identity_verification/video/n;)Lio/reactivex/c/h;

    move-result-object v1

    .line 90
    invoke-virtual {v0, v1}, Lio/reactivex/v;->f(Lio/reactivex/c/h;)Lio/reactivex/v;

    move-result-object v0

    .line 88
    return-object v0
.end method

.method static synthetic a(Lco/uk/getmondo/signup/identity_verification/video/n;Ljava/lang/Throwable;)Lio/reactivex/z;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 92
    instance-of v0, p1, Ljava/lang/OutOfMemoryError;

    .line 93
    iget-object v1, p0, Lco/uk/getmondo/signup/identity_verification/video/n;->f:Lco/uk/getmondo/common/a;

    invoke-virtual {p1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, v0}, Lco/uk/getmondo/api/model/tracking/Impression;->b(Ljava/lang/String;Z)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    invoke-virtual {v1, v0}, Lco/uk/getmondo/common/a;->a(Lco/uk/getmondo/api/model/tracking/Impression;)V

    .line 94
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/video/n;->i:Ljava/lang/String;

    invoke-static {v0}, Lio/reactivex/v;->a(Ljava/lang/Object;)Lio/reactivex/v;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lco/uk/getmondo/signup/identity_verification/video/n$a;Lio/reactivex/m;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 97
    invoke-interface {p0}, Lco/uk/getmondo/signup/identity_verification/video/n$a;->C()V

    return-void
.end method

.method static synthetic a(Lco/uk/getmondo/signup/identity_verification/video/n$a;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 108
    invoke-interface {p0}, Lco/uk/getmondo/signup/identity_verification/video/n$a;->finish()V

    return-void
.end method

.method static synthetic a(Lco/uk/getmondo/signup/identity_verification/video/n$a;Ljava/lang/Throwable;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 103
    invoke-static {p1}, Ld/a/a;->a(Ljava/lang/Throwable;)V

    .line 104
    invoke-interface {p0}, Lco/uk/getmondo/signup/identity_verification/video/n$a;->G()V

    .line 105
    return-void
.end method

.method static synthetic a(Lco/uk/getmondo/signup/identity_verification/video/n;Lco/uk/getmondo/signup/identity_verification/video/n$a;Ljava/lang/Boolean;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 112
    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 113
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/video/n;->e:Lco/uk/getmondo/signup/identity_verification/a/e;

    iget-object v1, p0, Lco/uk/getmondo/signup/identity_verification/video/n;->i:Ljava/lang/String;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lco/uk/getmondo/signup/identity_verification/a/e;->a(Ljava/lang/String;Z)V

    .line 114
    invoke-interface {p1}, Lco/uk/getmondo/signup/identity_verification/video/n$a;->finish()V

    .line 118
    :goto_0
    return-void

    .line 116
    :cond_0
    invoke-direct {p0, p1}, Lco/uk/getmondo/signup/identity_verification/video/n;->b(Lco/uk/getmondo/signup/identity_verification/video/n$a;)V

    goto :goto_0
.end method

.method static synthetic a(Lco/uk/getmondo/signup/identity_verification/video/n;Lco/uk/getmondo/signup/identity_verification/video/n$a;Ljava/lang/Object;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 65
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/video/n;->j:Lco/uk/getmondo/signup/identity_verification/video/ab;

    invoke-virtual {v0}, Lco/uk/getmondo/signup/identity_verification/video/ab;->a()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Lco/uk/getmondo/signup/identity_verification/video/n$a;->e(Ljava/lang/String;)V

    .line 66
    invoke-interface {p1}, Lco/uk/getmondo/signup/identity_verification/video/n$a;->w()V

    .line 67
    invoke-interface {p1}, Lco/uk/getmondo/signup/identity_verification/video/n$a;->y()V

    .line 68
    invoke-interface {p1}, Lco/uk/getmondo/signup/identity_verification/video/n$a;->k()V

    .line 69
    invoke-interface {p1}, Lco/uk/getmondo/signup/identity_verification/video/n$a;->c()V

    .line 70
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/video/n;->f:Lco/uk/getmondo/common/a;

    invoke-static {}, Lco/uk/getmondo/api/model/tracking/Impression;->C()Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v1

    invoke-virtual {v0, v1}, Lco/uk/getmondo/common/a;->a(Lco/uk/getmondo/api/model/tracking/Impression;)V

    .line 71
    return-void
.end method

.method static synthetic a(Lco/uk/getmondo/signup/identity_verification/video/n;Lco/uk/getmondo/signup/identity_verification/video/n$a;Ljava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 99
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/video/n;->i:Ljava/lang/String;

    invoke-interface {p1, v0}, Lco/uk/getmondo/signup/identity_verification/video/n$a;->d(Ljava/lang/String;)V

    .line 100
    invoke-interface {p1}, Lco/uk/getmondo/signup/identity_verification/video/n$a;->v()V

    .line 101
    return-void
.end method

.method private b(Lco/uk/getmondo/signup/identity_verification/video/n$a;)V
    .locals 0

    .prologue
    .line 122
    invoke-interface {p1}, Lco/uk/getmondo/signup/identity_verification/video/n$a;->F()V

    .line 123
    invoke-interface {p1}, Lco/uk/getmondo/signup/identity_verification/video/n$a;->z()V

    .line 124
    invoke-interface {p1}, Lco/uk/getmondo/signup/identity_verification/video/n$a;->w()V

    .line 125
    return-void
.end method

.method static synthetic b(Lco/uk/getmondo/signup/identity_verification/video/n$a;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 79
    invoke-interface {p0}, Lco/uk/getmondo/signup/identity_verification/video/n$a;->d()V

    .line 80
    invoke-interface {p0}, Lco/uk/getmondo/signup/identity_verification/video/n$a;->j()V

    .line 81
    invoke-interface {p0}, Lco/uk/getmondo/signup/identity_verification/video/n$a;->b()V

    .line 82
    invoke-interface {p0}, Lco/uk/getmondo/signup/identity_verification/video/n$a;->D()V

    .line 83
    invoke-interface {p0}, Lco/uk/getmondo/signup/identity_verification/video/n$a;->B()V

    .line 84
    return-void
.end method

.method static synthetic b(Lco/uk/getmondo/signup/identity_verification/video/n$a;Ljava/lang/Throwable;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 72
    invoke-static {p1}, Ld/a/a;->a(Ljava/lang/Throwable;)V

    .line 73
    invoke-interface {p0}, Lco/uk/getmondo/signup/identity_verification/video/n$a;->G()V

    .line 74
    return-void
.end method

.method static synthetic b(Lco/uk/getmondo/signup/identity_verification/video/n;Lco/uk/getmondo/signup/identity_verification/video/n$a;Ljava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 56
    invoke-interface {p1}, Lco/uk/getmondo/signup/identity_verification/video/n$a;->A()V

    .line 57
    invoke-interface {p1}, Lco/uk/getmondo/signup/identity_verification/video/n$a;->j()V

    .line 58
    invoke-interface {p1}, Lco/uk/getmondo/signup/identity_verification/video/n$a;->E()V

    .line 59
    invoke-interface {p1}, Lco/uk/getmondo/signup/identity_verification/video/n$a;->x()V

    .line 60
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/video/n;->i:Ljava/lang/String;

    invoke-interface {p1, v0}, Lco/uk/getmondo/signup/identity_verification/video/n$a;->a(Ljava/lang/String;)V

    .line 61
    return-void
.end method


# virtual methods
.method a()V
    .locals 1

    .prologue
    .line 128
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/video/n;->g:Lco/uk/getmondo/common/q;

    invoke-virtual {v0}, Lco/uk/getmondo/common/q;->a()V

    .line 129
    return-void
.end method

.method public bridge synthetic a(Lco/uk/getmondo/common/ui/f;)V
    .locals 0

    .prologue
    .line 22
    check-cast p1, Lco/uk/getmondo/signup/identity_verification/video/n$a;

    invoke-virtual {p0, p1}, Lco/uk/getmondo/signup/identity_verification/video/n;->a(Lco/uk/getmondo/signup/identity_verification/video/n$a;)V

    return-void
.end method

.method public a(Lco/uk/getmondo/signup/identity_verification/video/n$a;)V
    .locals 5

    .prologue
    .line 50
    invoke-super {p0, p1}, Lco/uk/getmondo/common/ui/b;->a(Lco/uk/getmondo/common/ui/f;)V

    .line 52
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/video/n;->f:Lco/uk/getmondo/common/a;

    const/4 v1, 0x0

    invoke-static {v1}, Lco/uk/getmondo/api/model/tracking/Impression;->f(Z)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v1

    invoke-virtual {v0, v1}, Lco/uk/getmondo/common/a;->a(Lco/uk/getmondo/api/model/tracking/Impression;)V

    .line 54
    invoke-interface {p1}, Lco/uk/getmondo/signup/identity_verification/video/n$a;->e()Lio/reactivex/n;

    move-result-object v0

    invoke-static {p0, p1}, Lco/uk/getmondo/signup/identity_verification/video/o;->a(Lco/uk/getmondo/signup/identity_verification/video/n;Lco/uk/getmondo/signup/identity_verification/video/n$a;)Lio/reactivex/c/g;

    move-result-object v1

    .line 55
    invoke-virtual {v0, v1}, Lio/reactivex/n;->doOnNext(Lio/reactivex/c/g;)Lio/reactivex/n;

    move-result-object v0

    const-wide/16 v2, 0x1

    sget-object v1, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    iget-object v4, p0, Lco/uk/getmondo/signup/identity_verification/video/n;->d:Lio/reactivex/u;

    .line 62
    invoke-virtual {v0, v2, v3, v1, v4}, Lio/reactivex/n;->delay(JLjava/util/concurrent/TimeUnit;Lio/reactivex/u;)Lio/reactivex/n;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/signup/identity_verification/video/n;->c:Lio/reactivex/u;

    .line 63
    invoke-virtual {v0, v1}, Lio/reactivex/n;->observeOn(Lio/reactivex/u;)Lio/reactivex/n;

    move-result-object v0

    invoke-static {p0, p1}, Lco/uk/getmondo/signup/identity_verification/video/s;->a(Lco/uk/getmondo/signup/identity_verification/video/n;Lco/uk/getmondo/signup/identity_verification/video/n$a;)Lio/reactivex/c/g;

    move-result-object v1

    invoke-static {p1}, Lco/uk/getmondo/signup/identity_verification/video/t;->a(Lco/uk/getmondo/signup/identity_verification/video/n$a;)Lio/reactivex/c/g;

    move-result-object v2

    .line 64
    invoke-virtual {v0, v1, v2}, Lio/reactivex/n;->subscribe(Lio/reactivex/c/g;Lio/reactivex/c/g;)Lio/reactivex/b/b;

    move-result-object v0

    .line 54
    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/identity_verification/video/n;->a(Lio/reactivex/b/b;)V

    .line 76
    invoke-interface {p1}, Lco/uk/getmondo/signup/identity_verification/video/n$a;->f()Lio/reactivex/n;

    move-result-object v0

    invoke-interface {p1}, Lco/uk/getmondo/signup/identity_verification/video/n$a;->i()Lio/reactivex/n;

    move-result-object v1

    invoke-static {v0, v1}, Lio/reactivex/n;->merge(Lio/reactivex/r;Lio/reactivex/r;)Lio/reactivex/n;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/signup/identity_verification/video/n;->c:Lio/reactivex/u;

    .line 77
    invoke-virtual {v0, v1}, Lio/reactivex/n;->observeOn(Lio/reactivex/u;)Lio/reactivex/n;

    move-result-object v0

    invoke-static {p1}, Lco/uk/getmondo/signup/identity_verification/video/u;->a(Lco/uk/getmondo/signup/identity_verification/video/n$a;)Lio/reactivex/c/g;

    move-result-object v1

    .line 78
    invoke-virtual {v0, v1}, Lio/reactivex/n;->doOnNext(Lio/reactivex/c/g;)Lio/reactivex/n;

    move-result-object v0

    const-wide/16 v2, 0x2

    sget-object v1, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    iget-object v4, p0, Lco/uk/getmondo/signup/identity_verification/video/n;->d:Lio/reactivex/u;

    .line 87
    invoke-virtual {v0, v2, v3, v1, v4}, Lio/reactivex/n;->delay(JLjava/util/concurrent/TimeUnit;Lio/reactivex/u;)Lio/reactivex/n;

    move-result-object v0

    invoke-static {p0}, Lco/uk/getmondo/signup/identity_verification/video/v;->a(Lco/uk/getmondo/signup/identity_verification/video/n;)Lio/reactivex/c/h;

    move-result-object v1

    .line 88
    invoke-virtual {v0, v1}, Lio/reactivex/n;->flatMapSingle(Lio/reactivex/c/h;)Lio/reactivex/n;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/signup/identity_verification/video/n;->c:Lio/reactivex/u;

    .line 96
    invoke-virtual {v0, v1}, Lio/reactivex/n;->observeOn(Lio/reactivex/u;)Lio/reactivex/n;

    move-result-object v0

    invoke-static {p1}, Lco/uk/getmondo/signup/identity_verification/video/w;->a(Lco/uk/getmondo/signup/identity_verification/video/n$a;)Lio/reactivex/c/g;

    move-result-object v1

    .line 97
    invoke-virtual {v0, v1}, Lio/reactivex/n;->doOnEach(Lio/reactivex/c/g;)Lio/reactivex/n;

    move-result-object v0

    invoke-static {p0, p1}, Lco/uk/getmondo/signup/identity_verification/video/x;->a(Lco/uk/getmondo/signup/identity_verification/video/n;Lco/uk/getmondo/signup/identity_verification/video/n$a;)Lio/reactivex/c/g;

    move-result-object v1

    invoke-static {p1}, Lco/uk/getmondo/signup/identity_verification/video/y;->a(Lco/uk/getmondo/signup/identity_verification/video/n$a;)Lio/reactivex/c/g;

    move-result-object v2

    .line 98
    invoke-virtual {v0, v1, v2}, Lio/reactivex/n;->subscribe(Lio/reactivex/c/g;Lio/reactivex/c/g;)Lio/reactivex/b/b;

    move-result-object v0

    .line 76
    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/identity_verification/video/n;->a(Lio/reactivex/b/b;)V

    .line 107
    invoke-interface {p1}, Lco/uk/getmondo/signup/identity_verification/video/n$a;->g()Lio/reactivex/n;

    move-result-object v0

    invoke-static {p1}, Lco/uk/getmondo/signup/identity_verification/video/z;->a(Lco/uk/getmondo/signup/identity_verification/video/n$a;)Lio/reactivex/c/g;

    move-result-object v1

    .line 108
    invoke-virtual {v0, v1}, Lio/reactivex/n;->subscribe(Lio/reactivex/c/g;)Lio/reactivex/b/b;

    move-result-object v0

    .line 107
    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/identity_verification/video/n;->a(Lio/reactivex/b/b;)V

    .line 110
    invoke-interface {p1}, Lco/uk/getmondo/signup/identity_verification/video/n$a;->h()Lio/reactivex/n;

    move-result-object v0

    invoke-static {p0, p1}, Lco/uk/getmondo/signup/identity_verification/video/p;->a(Lco/uk/getmondo/signup/identity_verification/video/n;Lco/uk/getmondo/signup/identity_verification/video/n$a;)Lio/reactivex/c/g;

    move-result-object v1

    invoke-static {}, Lco/uk/getmondo/signup/identity_verification/video/q;->a()Lio/reactivex/c/g;

    move-result-object v2

    .line 111
    invoke-virtual {v0, v1, v2}, Lio/reactivex/n;->subscribe(Lio/reactivex/c/g;Lio/reactivex/c/g;)Lio/reactivex/b/b;

    move-result-object v0

    .line 110
    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/identity_verification/video/n;->a(Lio/reactivex/b/b;)V

    .line 119
    return-void
.end method
