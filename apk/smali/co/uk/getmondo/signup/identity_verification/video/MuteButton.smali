.class public Lco/uk/getmondo/signup/identity_verification/video/MuteButton;
.super Landroid/support/v7/widget/o;
.source "MuteButton.java"


# static fields
.field private static final a:[I


# instance fields
.field private b:Z


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 11
    const/4 v0, 0x1

    new-array v0, v0, [I

    const/4 v1, 0x0

    const v2, 0x7f010268

    aput v2, v0, v1

    sput-object v0, Lco/uk/getmondo/signup/identity_verification/video/MuteButton;->a:[I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 16
    invoke-direct {p0, p1}, Landroid/support/v7/widget/o;-><init>(Landroid/content/Context;)V

    .line 13
    const/4 v0, 0x1

    iput-boolean v0, p0, Lco/uk/getmondo/signup/identity_verification/video/MuteButton;->b:Z

    .line 17
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 20
    invoke-direct {p0, p1, p2}, Landroid/support/v7/widget/o;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 13
    const/4 v0, 0x1

    iput-boolean v0, p0, Lco/uk/getmondo/signup/identity_verification/video/MuteButton;->b:Z

    .line 21
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 24
    invoke-direct {p0, p1, p2, p3}, Landroid/support/v7/widget/o;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 13
    const/4 v0, 0x1

    iput-boolean v0, p0, Lco/uk/getmondo/signup/identity_verification/video/MuteButton;->b:Z

    .line 25
    return-void
.end method


# virtual methods
.method public a()Z
    .locals 1

    .prologue
    .line 37
    iget-boolean v0, p0, Lco/uk/getmondo/signup/identity_verification/video/MuteButton;->b:Z

    return v0
.end method

.method public b()V
    .locals 1

    .prologue
    .line 41
    iget-boolean v0, p0, Lco/uk/getmondo/signup/identity_verification/video/MuteButton;->b:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lco/uk/getmondo/signup/identity_verification/video/MuteButton;->b:Z

    .line 42
    return-void

    .line 41
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onCreateDrawableState(I)[I
    .locals 2

    .prologue
    .line 29
    add-int/lit8 v0, p1, 0x1

    invoke-super {p0, v0}, Landroid/support/v7/widget/o;->onCreateDrawableState(I)[I

    move-result-object v0

    .line 30
    iget-boolean v1, p0, Lco/uk/getmondo/signup/identity_verification/video/MuteButton;->b:Z

    if-eqz v1, :cond_0

    .line 31
    sget-object v1, Lco/uk/getmondo/signup/identity_verification/video/MuteButton;->a:[I

    invoke-static {v0, v1}, Lco/uk/getmondo/signup/identity_verification/video/MuteButton;->mergeDrawableStates([I[I)[I

    .line 33
    :cond_0
    return-object v0
.end method
