.class public final Lco/uk/getmondo/signup/identity_verification/video/VideoRecordingActivity;
.super Lco/uk/getmondo/common/activities/b;
.source "VideoRecordingActivity.kt"

# interfaces
.implements Lco/uk/getmondo/signup/identity_verification/video/n$a;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lco/uk/getmondo/signup/identity_verification/video/VideoRecordingActivity$a;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000n\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0008\n\u0002\u0010\u0008\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0010\u0011\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0015\n\u0002\u0008\u001a\u0018\u0000 I2\u00020\u00012\u00020\u0002:\u0001IB\u0005\u00a2\u0006\u0002\u0010\u0003J\u0008\u0010\u0017\u001a\u00020\u0018H\u0016J\u0008\u0010\u0019\u001a\u00020\u0018H\u0016J\u0008\u0010\u001a\u001a\u00020\u0008H\u0002J\u0008\u0010\u001b\u001a\u00020\u0018H\u0016J\u0008\u0010\u001c\u001a\u00020\u0018H\u0016J\u0008\u0010\u001d\u001a\u00020\u0018H\u0016J\u0008\u0010\u001e\u001a\u00020\u0018H\u0016J\"\u0010\u001f\u001a\u00020\u00182\u0006\u0010 \u001a\u00020!2\u0006\u0010\"\u001a\u00020!2\u0008\u0010#\u001a\u0004\u0018\u00010$H\u0014J\u0012\u0010%\u001a\u00020\u00182\u0008\u0010&\u001a\u0004\u0018\u00010\'H\u0014J\u0008\u0010(\u001a\u00020\u0018H\u0014J\u0008\u0010)\u001a\u00020\u0018H\u0014J\u000e\u0010*\u001a\u0008\u0012\u0004\u0012\u00020\u00120\u0011H\u0016J+\u0010+\u001a\u00020\u00182\u0006\u0010 \u001a\u00020!2\u000c\u0010,\u001a\u0008\u0012\u0004\u0012\u00020.0-2\u0006\u0010/\u001a\u000200H\u0016\u00a2\u0006\u0002\u00101J\u0008\u00102\u001a\u00020\u0018H\u0014J\u0008\u00103\u001a\u00020\u0018H\u0014J\u000e\u00104\u001a\u0008\u0012\u0004\u0012\u00020\u00120\u0011H\u0016J\u000e\u00105\u001a\u0008\u0012\u0004\u0012\u00020\u00120\u0011H\u0016J\u000e\u00106\u001a\u0008\u0012\u0004\u0012\u00020\u00120\u0011H\u0016J\u000e\u00107\u001a\u0008\u0012\u0004\u0012\u00020\u00080\u0011H\u0016J\u0010\u00108\u001a\u00020\u00182\u0006\u00109\u001a\u00020.H\u0016J\u0008\u0010:\u001a\u00020\u0018H\u0016J\u0008\u0010;\u001a\u00020\u0018H\u0016J\u0008\u0010<\u001a\u00020\u0018H\u0016J\u0008\u0010=\u001a\u00020\u0018H\u0016J\u0008\u0010>\u001a\u00020\u0018H\u0016J\u0008\u0010?\u001a\u00020\u0018H\u0016J\u0008\u0010@\u001a\u00020\u0018H\u0016J\u0010\u0010A\u001a\u00020\u00182\u0006\u0010B\u001a\u00020.H\u0016J\u0008\u0010C\u001a\u00020\u0018H\u0016J\u0008\u0010D\u001a\u00020\u0018H\u0016J\u0010\u0010E\u001a\u00020\u00182\u0006\u0010F\u001a\u00020.H\u0016J\u0008\u0010G\u001a\u00020\u0018H\u0016J\u0008\u0010H\u001a\u00020\u0018H\u0016R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R2\u0010\u0006\u001a&\u0012\u000c\u0012\n \t*\u0004\u0018\u00010\u00080\u0008 \t*\u0012\u0012\u000c\u0012\n \t*\u0004\u0018\u00010\u00080\u0008\u0018\u00010\u00070\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001e\u0010\n\u001a\u00020\u000b8\u0000@\u0000X\u0081.\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u000c\u0010\r\"\u0004\u0008\u000e\u0010\u000fR\u0014\u0010\u0010\u001a\u0008\u0012\u0004\u0012\u00020\u00120\u0011X\u0082.\u00a2\u0006\u0002\n\u0000R2\u0010\u0013\u001a&\u0012\u000c\u0012\n \t*\u0004\u0018\u00010\u00120\u0012 \t*\u0012\u0012\u000c\u0012\n \t*\u0004\u0018\u00010\u00120\u0012\u0018\u00010\u00070\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0014\u001a\u00020\u0008X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0015\u001a\u00020\u0016X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006J"
    }
    d2 = {
        "Lco/uk/getmondo/signup/identity_verification/video/VideoRecordingActivity;",
        "Lco/uk/getmondo/common/activities/BaseActivity;",
        "Lco/uk/getmondo/signup/identity_verification/video/VideoRecordingPresenter$View;",
        "()V",
        "handler",
        "Landroid/os/Handler;",
        "playbackResultRelay",
        "Lcom/jakewharton/rxrelay2/PublishRelay;",
        "",
        "kotlin.jvm.PlatformType",
        "presenter",
        "Lco/uk/getmondo/signup/identity_verification/video/VideoRecordingPresenter;",
        "getPresenter$app_monzoPrepaidRelease",
        "()Lco/uk/getmondo/signup/identity_verification/video/VideoRecordingPresenter;",
        "setPresenter$app_monzoPrepaidRelease",
        "(Lco/uk/getmondo/signup/identity_verification/video/VideoRecordingPresenter;)V",
        "recordButtonClicked",
        "Lio/reactivex/Observable;",
        "",
        "recordingTimedOutRelay",
        "shouldDisplayPermissionDeniedDialog",
        "signalRecordingTimedOut",
        "Ljava/lang/Runnable;",
        "disableVideoButton",
        "",
        "enableVideoButton",
        "hasCameraAndRecordAudioPermissions",
        "hideCameraOverlay",
        "hideSavingVideo",
        "hideTextToRead",
        "hideWarmingUpCamera",
        "onActivityResult",
        "requestCode",
        "",
        "resultCode",
        "data",
        "Landroid/content/Intent;",
        "onCreate",
        "savedInstanceState",
        "Landroid/os/Bundle;",
        "onDestroy",
        "onPause",
        "onRecordingTimedOut",
        "onRequestPermissionsResult",
        "permissions",
        "",
        "",
        "grantResults",
        "",
        "(I[Ljava/lang/String;[I)V",
        "onResume",
        "onResumeFragments",
        "onStartRecordingClicked",
        "onStopRecordingClicked",
        "onTakeLaterClicked",
        "onVideoPlaybackResult",
        "playVideo",
        "videoPath",
        "showCameraOverlay",
        "showReadTextBelowText",
        "showRecordAction",
        "showRecordingError",
        "showRecordingInstructions",
        "showSavingVideo",
        "showStopAction",
        "showTextToRead",
        "text",
        "showWarmingUpCamera",
        "startRecordingTimer",
        "startRecordingVideo",
        "videoFilePath",
        "stopRecordingTimer",
        "stopRecordingVideo",
        "Companion",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# static fields
.field public static final b:Lco/uk/getmondo/signup/identity_verification/video/VideoRecordingActivity$a;

.field private static final j:J


# instance fields
.field public a:Lco/uk/getmondo/signup/identity_verification/video/n;

.field private c:Lio/reactivex/n;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/n",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Lcom/b/b/c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/b/b/c",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final f:Lcom/b/b/c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/b/b/c",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private final g:Landroid/os/Handler;

.field private final h:Ljava/lang/Runnable;

.field private i:Z

.field private k:Ljava/util/HashMap;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    new-instance v0, Lco/uk/getmondo/signup/identity_verification/video/VideoRecordingActivity$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lco/uk/getmondo/signup/identity_verification/video/VideoRecordingActivity$a;-><init>(Lkotlin/d/b/i;)V

    sput-object v0, Lco/uk/getmondo/signup/identity_verification/video/VideoRecordingActivity;->b:Lco/uk/getmondo/signup/identity_verification/video/VideoRecordingActivity$a;

    .line 228
    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const/16 v1, 0xf

    int-to-long v2, v1

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Lco/uk/getmondo/signup/identity_verification/video/VideoRecordingActivity;->j:J

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 28
    invoke-direct {p0}, Lco/uk/getmondo/common/activities/b;-><init>()V

    .line 32
    invoke-static {}, Lcom/b/b/c;->a()Lcom/b/b/c;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/signup/identity_verification/video/VideoRecordingActivity;->e:Lcom/b/b/c;

    .line 33
    invoke-static {}, Lcom/b/b/c;->a()Lcom/b/b/c;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/signup/identity_verification/video/VideoRecordingActivity;->f:Lcom/b/b/c;

    .line 34
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lco/uk/getmondo/signup/identity_verification/video/VideoRecordingActivity;->g:Landroid/os/Handler;

    .line 35
    new-instance v0, Lco/uk/getmondo/signup/identity_verification/video/VideoRecordingActivity$e;

    invoke-direct {v0, p0}, Lco/uk/getmondo/signup/identity_verification/video/VideoRecordingActivity$e;-><init>(Lco/uk/getmondo/signup/identity_verification/video/VideoRecordingActivity;)V

    check-cast v0, Ljava/lang/Runnable;

    iput-object v0, p0, Lco/uk/getmondo/signup/identity_verification/video/VideoRecordingActivity;->h:Ljava/lang/Runnable;

    return-void
.end method

.method public static final synthetic H()J
    .locals 2

    .prologue
    .line 28
    sget-wide v0, Lco/uk/getmondo/signup/identity_verification/video/VideoRecordingActivity;->j:J

    return-wide v0
.end method

.method private final I()Z
    .locals 2

    .prologue
    .line 215
    move-object v0, p0

    check-cast v0, Landroid/content/Context;

    const-string v1, "android.permission.CAMERA"

    invoke-static {v0, v1}, Landroid/support/v4/content/a;->b(Landroid/content/Context;Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_0

    .line 216
    check-cast p0, Landroid/content/Context;

    const-string v0, "android.permission.RECORD_AUDIO"

    invoke-static {p0, v0}, Landroid/support/v4/content/a;->b(Landroid/content/Context;Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static final synthetic a(Lco/uk/getmondo/signup/identity_verification/video/VideoRecordingActivity;)Lcom/b/b/c;
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/video/VideoRecordingActivity;->f:Lcom/b/b/c;

    return-object v0
.end method


# virtual methods
.method public A()V
    .locals 1

    .prologue
    .line 158
    sget v0, Lco/uk/getmondo/c$a;->maskImageView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/identity_verification/video/VideoRecordingActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    invoke-static {v0}, Lco/uk/getmondo/common/ae;->b(Landroid/view/View;)V

    .line 159
    return-void
.end method

.method public B()V
    .locals 1

    .prologue
    .line 168
    sget v0, Lco/uk/getmondo/c$a;->savingTextView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/identity_verification/video/VideoRecordingActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-static {v0}, Lco/uk/getmondo/common/ae;->a(Landroid/view/View;)V

    .line 169
    return-void
.end method

.method public C()V
    .locals 1

    .prologue
    .line 172
    sget v0, Lco/uk/getmondo/c$a;->savingTextView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/identity_verification/video/VideoRecordingActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-static {v0}, Lco/uk/getmondo/common/ae;->b(Landroid/view/View;)V

    .line 173
    return-void
.end method

.method public D()V
    .locals 1

    .prologue
    .line 182
    sget v0, Lco/uk/getmondo/c$a;->textToReadTextView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/identity_verification/video/VideoRecordingActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-static {v0}, Lco/uk/getmondo/common/ae;->b(Landroid/view/View;)V

    .line 183
    sget v0, Lco/uk/getmondo/c$a;->cameraOverlayView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/identity_verification/video/VideoRecordingActivity;->a(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Lco/uk/getmondo/common/ae;->b(Landroid/view/View;)V

    .line 184
    return-void
.end method

.method public E()V
    .locals 2

    .prologue
    .line 187
    sget v0, Lco/uk/getmondo/c$a;->descriptionTextView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/identity_verification/video/VideoRecordingActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const v1, 0x7f0a0214

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 188
    return-void
.end method

.method public F()V
    .locals 2

    .prologue
    .line 191
    sget v0, Lco/uk/getmondo/c$a;->descriptionTextView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/identity_verification/video/VideoRecordingActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const v1, 0x7f0a0214

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 192
    return-void
.end method

.method public G()V
    .locals 2

    .prologue
    .line 195
    invoke-virtual {p0}, Lco/uk/getmondo/signup/identity_verification/video/VideoRecordingActivity;->n()Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/identity_verification/video/VideoRecordingActivity;->a(Landroid/view/View;)V

    .line 196
    iget-object v1, p0, Lco/uk/getmondo/signup/identity_verification/video/VideoRecordingActivity;->d:Lcom/c/b/b;

    new-instance v0, Lco/uk/getmondo/signup/identity_verification/video/VideoRecordingActivity$d;

    invoke-direct {v0, p0}, Lco/uk/getmondo/signup/identity_verification/video/VideoRecordingActivity$d;-><init>(Lco/uk/getmondo/signup/identity_verification/video/VideoRecordingActivity;)V

    check-cast v0, Lcom/c/a/a;

    invoke-virtual {v1, v0}, Lcom/c/b/b;->a(Lcom/c/a/a;)Lcom/c/b/b;

    .line 201
    return-void
.end method

.method public a(I)Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/video/VideoRecordingActivity;->k:Ljava/util/HashMap;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lco/uk/getmondo/signup/identity_verification/video/VideoRecordingActivity;->k:Ljava/util/HashMap;

    :cond_0
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/video/VideoRecordingActivity;->k:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    if-nez v0, :cond_1

    invoke-virtual {p0, p1}, Landroid/support/v4/app/j;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/signup/identity_verification/video/VideoRecordingActivity;->k:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    return-object v0
.end method

.method public final a()Lco/uk/getmondo/signup/identity_verification/video/n;
    .locals 2

    .prologue
    .line 29
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/video/VideoRecordingActivity;->a:Lco/uk/getmondo/signup/identity_verification/video/n;

    if-nez v0, :cond_0

    const-string v1, "presenter"

    invoke-static {v1}, Lkotlin/d/b/l;->b(Ljava/lang/String;)V

    :cond_0
    return-object v0
.end method

.method public a(Ljava/lang/String;)V
    .locals 1

    .prologue
    const-string v0, "videoFilePath"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 93
    sget v0, Lco/uk/getmondo/c$a;->cameraView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/identity_verification/video/VideoRecordingActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/cameraview/CameraView;

    invoke-virtual {v0, p1}, Lcom/google/android/cameraview/CameraView;->a(Ljava/lang/String;)V

    .line 94
    return-void
.end method

.method public b()V
    .locals 1

    .prologue
    .line 97
    sget v0, Lco/uk/getmondo/c$a;->cameraView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/identity_verification/video/VideoRecordingActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/cameraview/CameraView;

    invoke-virtual {v0}, Lcom/google/android/cameraview/CameraView;->e()V

    .line 98
    return-void
.end method

.method public c()V
    .locals 4

    .prologue
    .line 101
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/video/VideoRecordingActivity;->g:Landroid/os/Handler;

    iget-object v1, p0, Lco/uk/getmondo/signup/identity_verification/video/VideoRecordingActivity;->h:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 102
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/video/VideoRecordingActivity;->g:Landroid/os/Handler;

    iget-object v1, p0, Lco/uk/getmondo/signup/identity_verification/video/VideoRecordingActivity;->h:Ljava/lang/Runnable;

    sget-object v2, Lco/uk/getmondo/signup/identity_verification/video/VideoRecordingActivity;->b:Lco/uk/getmondo/signup/identity_verification/video/VideoRecordingActivity$a;

    invoke-static {v2}, Lco/uk/getmondo/signup/identity_verification/video/VideoRecordingActivity$a;->a(Lco/uk/getmondo/signup/identity_verification/video/VideoRecordingActivity$a;)J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 103
    return-void
.end method

.method public d()V
    .locals 2

    .prologue
    .line 106
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/video/VideoRecordingActivity;->g:Landroid/os/Handler;

    iget-object v1, p0, Lco/uk/getmondo/signup/identity_verification/video/VideoRecordingActivity;->h:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 107
    return-void
.end method

.method public d(Ljava/lang/String;)V
    .locals 2

    .prologue
    const-string v0, "videoPath"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 162
    sget-object v1, Lco/uk/getmondo/signup/identity_verification/video/VideoPlaybackActivity;->b:Lco/uk/getmondo/signup/identity_verification/video/VideoPlaybackActivity$a;

    move-object v0, p0

    check-cast v0, Landroid/content/Context;

    invoke-virtual {v1, v0, p1}, Lco/uk/getmondo/signup/identity_verification/video/VideoPlaybackActivity$a;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 163
    const/high16 v1, 0x10000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 164
    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lco/uk/getmondo/signup/identity_verification/video/VideoRecordingActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 165
    return-void
.end method

.method public e()Lio/reactivex/n;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 110
    iget-object v1, p0, Lco/uk/getmondo/signup/identity_verification/video/VideoRecordingActivity;->c:Lio/reactivex/n;

    if-nez v1, :cond_0

    const-string v0, "recordButtonClicked"

    invoke-static {v0}, Lkotlin/d/b/l;->b(Ljava/lang/String;)V

    :cond_0
    new-instance v0, Lco/uk/getmondo/signup/identity_verification/video/VideoRecordingActivity$b;

    invoke-direct {v0, p0}, Lco/uk/getmondo/signup/identity_verification/video/VideoRecordingActivity$b;-><init>(Lco/uk/getmondo/signup/identity_verification/video/VideoRecordingActivity;)V

    check-cast v0, Lio/reactivex/c/q;

    invoke-virtual {v1, v0}, Lio/reactivex/n;->filter(Lio/reactivex/c/q;)Lio/reactivex/n;

    move-result-object v0

    const-string v1, "recordButtonClicked.filt\u2026raView.isRecordingVideo }"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public e(Ljava/lang/String;)V
    .locals 1

    .prologue
    const-string v0, "text"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 176
    sget v0, Lco/uk/getmondo/c$a;->textToReadTextView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/identity_verification/video/VideoRecordingActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-static {v0}, Lco/uk/getmondo/common/ae;->a(Landroid/view/View;)V

    .line 177
    sget v0, Lco/uk/getmondo/c$a;->textToReadTextView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/identity_verification/video/VideoRecordingActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    check-cast p1, Ljava/lang/CharSequence;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 178
    sget v0, Lco/uk/getmondo/c$a;->cameraOverlayView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/identity_verification/video/VideoRecordingActivity;->a(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Lco/uk/getmondo/common/ae;->a(Landroid/view/View;)V

    .line 179
    return-void
.end method

.method public f()Lio/reactivex/n;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 114
    iget-object v1, p0, Lco/uk/getmondo/signup/identity_verification/video/VideoRecordingActivity;->c:Lio/reactivex/n;

    if-nez v1, :cond_0

    const-string v0, "recordButtonClicked"

    invoke-static {v0}, Lkotlin/d/b/l;->b(Ljava/lang/String;)V

    :cond_0
    new-instance v0, Lco/uk/getmondo/signup/identity_verification/video/VideoRecordingActivity$c;

    invoke-direct {v0, p0}, Lco/uk/getmondo/signup/identity_verification/video/VideoRecordingActivity$c;-><init>(Lco/uk/getmondo/signup/identity_verification/video/VideoRecordingActivity;)V

    check-cast v0, Lio/reactivex/c/q;

    invoke-virtual {v1, v0}, Lio/reactivex/n;->filter(Lio/reactivex/c/q;)Lio/reactivex/n;

    move-result-object v0

    const-string v1, "recordButtonClicked.filt\u2026raView.isRecordingVideo }"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public g()Lio/reactivex/n;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 118
    sget v0, Lco/uk/getmondo/c$a;->takeLaterButton:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/identity_verification/video/VideoRecordingActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    invoke-static {v0}, Lcom/b/a/c/c;->a(Landroid/view/View;)Lio/reactivex/n;

    move-result-object v0

    const-string v1, "RxView.clicks(takeLaterButton)"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public h()Lio/reactivex/n;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .prologue
    .line 122
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/video/VideoRecordingActivity;->e:Lcom/b/b/c;

    const-string v1, "playbackResultRelay"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lio/reactivex/n;

    return-object v0
.end method

.method public i()Lio/reactivex/n;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 126
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/video/VideoRecordingActivity;->f:Lcom/b/b/c;

    const-string v1, "recordingTimedOutRelay"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lio/reactivex/n;

    return-object v0
.end method

.method public j()V
    .locals 2

    .prologue
    .line 130
    sget v0, Lco/uk/getmondo/c$a;->recordVideoButton:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/identity_verification/video/VideoRecordingActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 131
    return-void
.end method

.method public k()V
    .locals 2

    .prologue
    .line 134
    sget v0, Lco/uk/getmondo/c$a;->recordVideoButton:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/identity_verification/video/VideoRecordingActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    const v1, 0x7f020062

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 135
    return-void
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 2

    .prologue
    .line 85
    const/4 v0, 0x2

    if-ne p1, v0, :cond_1

    .line 86
    iget-object v1, p0, Lco/uk/getmondo/signup/identity_verification/video/VideoRecordingActivity;->e:Lcom/b/b/c;

    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/b/b/c;->a(Ljava/lang/Object;)V

    .line 89
    :goto_1
    return-void

    .line 86
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 88
    :cond_1
    invoke-super {p0, p1, p2, p3}, Lco/uk/getmondo/common/activities/b;->onActivityResult(IILandroid/content/Intent;)V

    goto :goto_1
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 39
    invoke-super {p0, p1}, Lco/uk/getmondo/common/activities/b;->onCreate(Landroid/os/Bundle;)V

    .line 40
    const v0, 0x7f050058

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/identity_verification/video/VideoRecordingActivity;->setContentView(I)V

    .line 42
    invoke-virtual {p0}, Lco/uk/getmondo/signup/identity_verification/video/VideoRecordingActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "KEY_VERSION"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    if-nez v0, :cond_0

    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type co.uk.getmondo.signup.identity_verification.data.IdentityVerificationVersion"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    check-cast v0, Lco/uk/getmondo/signup/identity_verification/a/j;

    .line 43
    invoke-virtual {p0}, Lco/uk/getmondo/signup/identity_verification/video/VideoRecordingActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "KEY_SIGNUP_SOURCE"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v1

    if-nez v1, :cond_1

    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type co.uk.getmondo.api.model.signup.SignupSource"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    check-cast v1, Lco/uk/getmondo/api/model/signup/SignupSource;

    .line 44
    invoke-virtual {p0}, Lco/uk/getmondo/signup/identity_verification/video/VideoRecordingActivity;->l()Lco/uk/getmondo/common/h/b/b;

    move-result-object v2

    new-instance v3, Lco/uk/getmondo/signup/identity_verification/p;

    invoke-direct {v3, v0, v1}, Lco/uk/getmondo/signup/identity_verification/p;-><init>(Lco/uk/getmondo/signup/identity_verification/a/j;Lco/uk/getmondo/api/model/signup/SignupSource;)V

    invoke-interface {v2, v3}, Lco/uk/getmondo/common/h/b/b;->a(Lco/uk/getmondo/signup/identity_verification/p;)Lco/uk/getmondo/signup/identity_verification/o;

    move-result-object v0

    invoke-interface {v0, p0}, Lco/uk/getmondo/signup/identity_verification/o;->a(Lco/uk/getmondo/signup/identity_verification/video/VideoRecordingActivity;)V

    .line 46
    sget v0, Lco/uk/getmondo/c$a;->recordVideoButton:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/identity_verification/video/VideoRecordingActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    invoke-static {v0}, Lcom/b/a/c/c;->a(Landroid/view/View;)Lio/reactivex/n;

    move-result-object v0

    invoke-virtual {v0}, Lio/reactivex/n;->share()Lio/reactivex/n;

    move-result-object v0

    const-string v1, "RxView.clicks(recordVideoButton).share()"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v0, p0, Lco/uk/getmondo/signup/identity_verification/video/VideoRecordingActivity;->c:Lio/reactivex/n;

    .line 47
    iget-object v1, p0, Lco/uk/getmondo/signup/identity_verification/video/VideoRecordingActivity;->a:Lco/uk/getmondo/signup/identity_verification/video/n;

    if-nez v1, :cond_2

    const-string v0, "presenter"

    invoke-static {v0}, Lkotlin/d/b/l;->b(Ljava/lang/String;)V

    :cond_2
    move-object v0, p0

    check-cast v0, Lco/uk/getmondo/signup/identity_verification/video/n$a;

    invoke-virtual {v1, v0}, Lco/uk/getmondo/signup/identity_verification/video/n;->a(Lco/uk/getmondo/signup/identity_verification/video/n$a;)V

    .line 49
    invoke-direct {p0}, Lco/uk/getmondo/signup/identity_verification/video/VideoRecordingActivity;->I()Z

    move-result v0

    if-nez v0, :cond_3

    .line 50
    check-cast p0, Landroid/app/Activity;

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "android.permission.CAMERA"

    aput-object v2, v0, v1

    const-string v1, "android.permission.RECORD_AUDIO"

    aput-object v1, v0, v4

    check-cast v0, [Ljava/lang/Object;

    .line 243
    check-cast v0, [Ljava/lang/String;

    .line 50
    invoke-static {p0, v0, v4}, Landroid/support/v4/app/a;->a(Landroid/app/Activity;[Ljava/lang/String;I)V

    .line 53
    :cond_3
    return-void
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    .line 79
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/video/VideoRecordingActivity;->g:Landroid/os/Handler;

    iget-object v1, p0, Lco/uk/getmondo/signup/identity_verification/video/VideoRecordingActivity;->h:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 80
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/video/VideoRecordingActivity;->a:Lco/uk/getmondo/signup/identity_verification/video/n;

    if-nez v0, :cond_0

    const-string v1, "presenter"

    invoke-static {v1}, Lkotlin/d/b/l;->b(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0}, Lco/uk/getmondo/signup/identity_verification/video/n;->b()V

    .line 81
    invoke-super {p0}, Lco/uk/getmondo/common/activities/b;->onDestroy()V

    .line 82
    return-void
.end method

.method protected onPause()V
    .locals 1

    .prologue
    .line 74
    sget v0, Lco/uk/getmondo/c$a;->cameraView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/identity_verification/video/VideoRecordingActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/cameraview/CameraView;

    invoke-virtual {v0}, Lcom/google/android/cameraview/CameraView;->c()V

    .line 75
    invoke-super {p0}, Lco/uk/getmondo/common/activities/b;->onPause()V

    .line 76
    return-void
.end method

.method public onRequestPermissionsResult(I[Ljava/lang/String;[I)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    const-string v0, "permissions"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "grantResults"

    invoke-static {p3, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 204
    if-ne p1, v2, :cond_2

    .line 205
    array-length v0, p3

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x0

    aget v0, p3, v0

    if-nez v0, :cond_0

    aget v0, p3, v2

    if-eqz v0, :cond_1

    .line 207
    :cond_0
    iput-boolean v2, p0, Lco/uk/getmondo/signup/identity_verification/video/VideoRecordingActivity;->i:Z

    .line 211
    :cond_1
    :goto_0
    return-void

    .line 210
    :cond_2
    invoke-super {p0, p1, p2, p3}, Lco/uk/getmondo/common/activities/b;->onRequestPermissionsResult(I[Ljava/lang/String;[I)V

    goto :goto_0
.end method

.method protected onResume()V
    .locals 1

    .prologue
    .line 56
    invoke-super {p0}, Lco/uk/getmondo/common/activities/b;->onResume()V

    .line 57
    invoke-direct {p0}, Lco/uk/getmondo/signup/identity_verification/video/VideoRecordingActivity;->I()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 58
    sget v0, Lco/uk/getmondo/c$a;->cameraView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/identity_verification/video/VideoRecordingActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/cameraview/CameraView;

    invoke-virtual {v0}, Lcom/google/android/cameraview/CameraView;->b()V

    .line 60
    :cond_0
    return-void
.end method

.method protected onResumeFragments()V
    .locals 3

    .prologue
    .line 63
    invoke-super {p0}, Lco/uk/getmondo/common/activities/b;->onResumeFragments()V

    .line 66
    iget-boolean v0, p0, Lco/uk/getmondo/signup/identity_verification/video/VideoRecordingActivity;->i:Z

    if-eqz v0, :cond_0

    .line 67
    const v0, 0x7f0a032d

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/identity_verification/video/VideoRecordingActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lco/uk/getmondo/common/d/a;->a(Ljava/lang/String;)Lco/uk/getmondo/common/d/a;

    move-result-object v0

    .line 68
    invoke-virtual {p0}, Lco/uk/getmondo/signup/identity_verification/video/VideoRecordingActivity;->getSupportFragmentManager()Landroid/support/v4/app/n;

    move-result-object v1

    const-string v2, "TAG_ERROR_DIALOG_FRAGMENT"

    invoke-virtual {v0, v1, v2}, Lco/uk/getmondo/common/d/a;->show(Landroid/support/v4/app/n;Ljava/lang/String;)V

    .line 69
    const/4 v0, 0x0

    iput-boolean v0, p0, Lco/uk/getmondo/signup/identity_verification/video/VideoRecordingActivity;->i:Z

    .line 71
    :cond_0
    return-void
.end method

.method public v()V
    .locals 2

    .prologue
    .line 138
    sget v0, Lco/uk/getmondo/c$a;->recordVideoButton:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/identity_verification/video/VideoRecordingActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    const v1, 0x7f02005f

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 139
    return-void
.end method

.method public w()V
    .locals 2

    .prologue
    .line 142
    sget v0, Lco/uk/getmondo/c$a;->recordVideoButton:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/identity_verification/video/VideoRecordingActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 143
    return-void
.end method

.method public x()V
    .locals 1

    .prologue
    .line 146
    sget v0, Lco/uk/getmondo/c$a;->warmingUpTextView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/identity_verification/video/VideoRecordingActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-static {v0}, Lco/uk/getmondo/common/ae;->a(Landroid/view/View;)V

    .line 147
    return-void
.end method

.method public y()V
    .locals 1

    .prologue
    .line 150
    sget v0, Lco/uk/getmondo/c$a;->warmingUpTextView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/identity_verification/video/VideoRecordingActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-static {v0}, Lco/uk/getmondo/common/ae;->b(Landroid/view/View;)V

    .line 151
    return-void
.end method

.method public z()V
    .locals 1

    .prologue
    .line 154
    sget v0, Lco/uk/getmondo/c$a;->maskImageView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/identity_verification/video/VideoRecordingActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    invoke-static {v0}, Lco/uk/getmondo/common/ae;->a(Landroid/view/View;)V

    .line 155
    return-void
.end method
