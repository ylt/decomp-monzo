.class public Lco/uk/getmondo/signup/identity_verification/video/a;
.super Ljava/lang/Object;
.source "Mp4Helper.java"


# instance fields
.field private final a:Lco/uk/getmondo/common/a;


# direct methods
.method public constructor <init>(Lco/uk/getmondo/common/a;)V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    iput-object p1, p0, Lco/uk/getmondo/signup/identity_verification/video/a;->a:Lco/uk/getmondo/common/a;

    .line 33
    return-void
.end method

.method static synthetic a(Lco/uk/getmondo/signup/identity_verification/video/a;Ljava/lang/String;)Lio/reactivex/z;
    .locals 14
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 46
    new-instance v6, Ljava/io/File;

    invoke-direct {v6, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 47
    const/4 v2, 0x0

    .line 48
    const/4 v1, 0x0

    .line 50
    :try_start_0
    new-instance v5, Lcom/googlecode/mp4parser/c;

    invoke-direct {v5, v6}, Lcom/googlecode/mp4parser/c;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_c
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 51
    :try_start_1
    new-instance v4, Lcom/coremedia/iso/d;

    invoke-direct {v4, v5}, Lcom/coremedia/iso/d;-><init>(Lcom/googlecode/mp4parser/b;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_d
    .catch Ljava/lang/OutOfMemoryError; {:try_start_1 .. :try_end_1} :catch_a
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 53
    :try_start_2
    invoke-virtual {v4}, Lcom/coremedia/iso/d;->a()Lcom/coremedia/iso/boxes/MovieBox;

    move-result-object v0

    const-class v1, Lcom/coremedia/iso/boxes/TrackBox;

    invoke-virtual {v0, v1}, Lcom/coremedia/iso/boxes/MovieBox;->getBoxes(Ljava/lang/Class;)Ljava/util/List;

    move-result-object v7

    .line 54
    const/4 v3, 0x0

    .line 55
    const/4 v2, 0x0

    .line 56
    const/4 v1, 0x0

    .line 57
    invoke-interface {v7}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_0
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/coremedia/iso/boxes/TrackBox;

    .line 58
    invoke-virtual {v0}, Lcom/coremedia/iso/boxes/TrackBox;->getMediaBox()Lcom/coremedia/iso/boxes/MediaBox;

    move-result-object v0

    invoke-virtual {v0}, Lcom/coremedia/iso/boxes/MediaBox;->getMediaInformationBox()Lcom/coremedia/iso/boxes/MediaInformationBox;

    move-result-object v0

    invoke-virtual {v0}, Lcom/coremedia/iso/boxes/MediaInformationBox;->getSampleTableBox()Lcom/coremedia/iso/boxes/SampleTableBox;

    move-result-object v0

    .line 59
    invoke-virtual {v0}, Lcom/coremedia/iso/boxes/SampleTableBox;->getTimeToSampleBox()Lcom/coremedia/iso/boxes/TimeToSampleBox;

    move-result-object v0

    invoke-virtual {v0}, Lcom/coremedia/iso/boxes/TimeToSampleBox;->getEntries()Ljava/util/List;

    move-result-object v9

    .line 64
    const/4 v0, 0x0

    invoke-interface {v9, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/coremedia/iso/boxes/TimeToSampleBox$a;

    .line 65
    invoke-virtual {v0}, Lcom/coremedia/iso/boxes/TimeToSampleBox$a;->b()J

    move-result-wide v10

    const-wide/16 v12, 0x2710

    cmp-long v10, v10, v12

    if-lez v10, :cond_0

    .line 66
    invoke-virtual {v0}, Lcom/coremedia/iso/boxes/TimeToSampleBox$a;->b()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    .line 67
    invoke-interface {v9}, Ljava/util/List;->size()I

    move-result v1

    const/4 v3, 0x1

    if-le v1, v3, :cond_1

    const/4 v1, 0x1

    invoke-interface {v9, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/coremedia/iso/boxes/TimeToSampleBox$a;

    invoke-virtual {v1}, Lcom/coremedia/iso/boxes/TimeToSampleBox$a;->b()J

    move-result-wide v10

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    .line 68
    :goto_1
    const/4 v3, 0x1

    .line 69
    const-wide/16 v10, 0xbb8

    invoke-virtual {v0, v10, v11}, Lcom/coremedia/iso/boxes/TimeToSampleBox$a;->b(J)V

    :cond_0
    move-object v0, v1

    move-object v1, v2

    move v2, v3

    move v3, v2

    move-object v2, v1

    move-object v1, v0

    .line 71
    goto :goto_0

    .line 67
    :cond_1
    const/4 v1, 0x0

    goto :goto_1

    .line 73
    :cond_2
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/video/a;->a:Lco/uk/getmondo/common/a;

    invoke-static {v3, v2, v1}, Lco/uk/getmondo/api/model/tracking/Impression;->a(ZLjava/lang/Long;Ljava/lang/Long;)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v1

    invoke-virtual {v0, v1}, Lco/uk/getmondo/common/a;->a(Lco/uk/getmondo/api/model/tracking/Impression;)V

    .line 75
    if-eqz v3, :cond_6

    .line 76
    new-instance v1, Lcom/googlecode/mp4parser/authoring/c;

    invoke-direct {v1}, Lcom/googlecode/mp4parser/authoring/c;-><init>()V

    .line 77
    invoke-interface {v7}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/coremedia/iso/boxes/TrackBox;

    .line 78
    new-instance v3, Lcom/googlecode/mp4parser/authoring/d;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    .line 79
    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "["

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v0}, Lcom/coremedia/iso/boxes/TrackBox;->getTrackHeaderBox()Lcom/coremedia/iso/boxes/TrackHeaderBox;

    move-result-object v8

    invoke-virtual {v8}, Lcom/coremedia/iso/boxes/TrackHeaderBox;->getTrackId()J

    move-result-wide v8

    invoke-virtual {v7, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "]"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x0

    new-array v8, v8, [Lcom/coremedia/iso/d;

    invoke-direct {v3, v7, v0, v8}, Lcom/googlecode/mp4parser/authoring/d;-><init>(Ljava/lang/String;Lcom/coremedia/iso/boxes/TrackBox;[Lcom/coremedia/iso/d;)V

    .line 78
    invoke-virtual {v1, v3}, Lcom/googlecode/mp4parser/authoring/c;->a(Lcom/googlecode/mp4parser/authoring/f;)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_2 .. :try_end_2} :catch_b
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    goto :goto_2

    .line 91
    :catch_0
    move-exception v0

    move-object v1, v4

    move-object v2, v5

    .line 92
    :goto_3
    :try_start_3
    invoke-static {v0}, Lio/reactivex/v;->a(Ljava/lang/Throwable;)Lio/reactivex/v;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_3

    move-result-object v0

    .line 99
    if-eqz v2, :cond_3

    :try_start_4
    invoke-interface {v2}, Lcom/googlecode/mp4parser/b;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_5

    .line 104
    :cond_3
    :goto_4
    if-eqz v1, :cond_4

    :try_start_5
    invoke-virtual {v1}, Lcom/coremedia/iso/d;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_6

    .line 109
    :cond_4
    :goto_5
    return-object v0

    .line 81
    :cond_5
    :try_start_6
    invoke-virtual {v4}, Lcom/coremedia/iso/d;->a()Lcom/coremedia/iso/boxes/MovieBox;

    move-result-object v0

    invoke-virtual {v0}, Lcom/coremedia/iso/boxes/MovieBox;->getMovieHeaderBox()Lcom/coremedia/iso/boxes/MovieHeaderBox;

    move-result-object v0

    invoke-virtual {v0}, Lcom/coremedia/iso/boxes/MovieHeaderBox;->getMatrix()Lcom/googlecode/mp4parser/c/h;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/googlecode/mp4parser/authoring/c;->a(Lcom/googlecode/mp4parser/c/h;)V

    .line 82
    new-instance v0, Lcom/googlecode/mp4parser/authoring/builder/DefaultMp4Builder;

    invoke-direct {v0}, Lcom/googlecode/mp4parser/authoring/builder/DefaultMp4Builder;-><init>()V

    invoke-virtual {v0, v1}, Lcom/googlecode/mp4parser/authoring/builder/DefaultMp4Builder;->a(Lcom/googlecode/mp4parser/authoring/c;)Lcom/coremedia/iso/boxes/b;

    move-result-object v0

    .line 86
    invoke-virtual {v6}, Ljava/io/File;->delete()Z

    .line 87
    new-instance v1, Ljava/io/RandomAccessFile;

    invoke-virtual {v6}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v2

    const-string v3, "rw"

    invoke-direct {v1, v2, v3}, Ljava/io/RandomAccessFile;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/io/RandomAccessFile;->getChannel()Ljava/nio/channels/FileChannel;

    move-result-object v1

    .line 88
    invoke-interface {v0, v1}, Lcom/coremedia/iso/boxes/b;->writeContainer(Ljava/nio/channels/WritableByteChannel;)V

    .line 89
    invoke-virtual {v1}, Ljava/nio/channels/FileChannel;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_6 .. :try_end_6} :catch_b
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    .line 99
    :cond_6
    if-eqz v5, :cond_7

    :try_start_7
    invoke-interface {v5}, Lcom/googlecode/mp4parser/b;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_3

    .line 104
    :cond_7
    :goto_6
    if-eqz v4, :cond_8

    :try_start_8
    invoke-virtual {v4}, Lcom/coremedia/iso/d;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_4

    .line 109
    :cond_8
    :goto_7
    invoke-static {p1}, Lio/reactivex/v;->a(Ljava/lang/Object;)Lio/reactivex/v;

    move-result-object v0

    goto :goto_5

    .line 93
    :catch_1
    move-exception v0

    move-object v4, v1

    move-object v5, v2

    .line 96
    :goto_8
    :try_start_9
    invoke-static {v0}, Lio/reactivex/v;->a(Ljava/lang/Throwable;)Lio/reactivex/v;
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_2

    move-result-object v0

    .line 99
    if-eqz v5, :cond_9

    :try_start_a
    invoke-interface {v5}, Lcom/googlecode/mp4parser/b;->close()V
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_7

    .line 104
    :cond_9
    :goto_9
    if-eqz v4, :cond_4

    :try_start_b
    invoke-virtual {v4}, Lcom/coremedia/iso/d;->close()V
    :try_end_b
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_2

    goto :goto_5

    .line 105
    :catch_2
    move-exception v1

    goto :goto_5

    .line 98
    :catchall_0
    move-exception v0

    move-object v4, v1

    move-object v5, v2

    .line 99
    :goto_a
    if-eqz v5, :cond_a

    :try_start_c
    invoke-interface {v5}, Lcom/googlecode/mp4parser/b;->close()V
    :try_end_c
    .catch Ljava/io/IOException; {:try_start_c .. :try_end_c} :catch_8

    .line 104
    :cond_a
    :goto_b
    if-eqz v4, :cond_b

    :try_start_d
    invoke-virtual {v4}, Lcom/coremedia/iso/d;->close()V
    :try_end_d
    .catch Ljava/io/IOException; {:try_start_d .. :try_end_d} :catch_9

    .line 107
    :cond_b
    :goto_c
    throw v0

    .line 100
    :catch_3
    move-exception v0

    goto :goto_6

    .line 105
    :catch_4
    move-exception v0

    goto :goto_7

    .line 100
    :catch_5
    move-exception v2

    goto :goto_4

    .line 105
    :catch_6
    move-exception v1

    goto :goto_5

    .line 100
    :catch_7
    move-exception v1

    goto :goto_9

    :catch_8
    move-exception v1

    goto :goto_b

    .line 105
    :catch_9
    move-exception v1

    goto :goto_c

    .line 98
    :catchall_1
    move-exception v0

    move-object v4, v1

    goto :goto_a

    :catchall_2
    move-exception v0

    goto :goto_a

    :catchall_3
    move-exception v0

    move-object v4, v1

    move-object v5, v2

    goto :goto_a

    .line 93
    :catch_a
    move-exception v0

    move-object v4, v1

    goto :goto_8

    :catch_b
    move-exception v0

    goto :goto_8

    .line 91
    :catch_c
    move-exception v0

    goto/16 :goto_3

    :catch_d
    move-exception v0

    move-object v2, v5

    goto/16 :goto_3
.end method


# virtual methods
.method a(Ljava/lang/String;)Lio/reactivex/v;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/v",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 45
    invoke-static {p0, p1}, Lco/uk/getmondo/signup/identity_verification/video/b;->a(Lco/uk/getmondo/signup/identity_verification/video/a;Ljava/lang/String;)Ljava/util/concurrent/Callable;

    move-result-object v0

    invoke-static {v0}, Lio/reactivex/v;->a(Ljava/util/concurrent/Callable;)Lio/reactivex/v;

    move-result-object v0

    return-object v0
.end method
