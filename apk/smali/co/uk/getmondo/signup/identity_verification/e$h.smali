.class final Lco/uk/getmondo/signup/identity_verification/e$h;
.super Lkotlin/d/b/m;
.source "IdentityDocumentsFragment.kt"

# interfaces
.implements Lkotlin/d/a/b;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lco/uk/getmondo/signup/identity_verification/e;->g()Lio/reactivex/h;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/d/b/m;",
        "Lkotlin/d/a/b",
        "<",
        "Lkotlin/d/a/b",
        "<-",
        "Ljava/lang/Boolean;",
        "+",
        "Lkotlin/n;",
        ">;",
        "Landroid/support/design/widget/c;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000\u0016\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0002\u0010\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u0012\u0010\u0002\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050\u0003H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "Landroid/support/design/widget/BottomSheetDialog;",
        "setResult",
        "Lkotlin/Function1;",
        "",
        "",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field final synthetic a:Lco/uk/getmondo/signup/identity_verification/e;


# direct methods
.method constructor <init>(Lco/uk/getmondo/signup/identity_verification/e;)V
    .locals 1

    iput-object p1, p0, Lco/uk/getmondo/signup/identity_verification/e$h;->a:Lco/uk/getmondo/signup/identity_verification/e;

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lkotlin/d/b/m;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final a(Lkotlin/d/a/b;)Landroid/support/design/widget/c;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/d/a/b",
            "<-",
            "Ljava/lang/Boolean;",
            "Lkotlin/n;",
            ">;)",
            "Landroid/support/design/widget/c;"
        }
    .end annotation

    .prologue
    const-string v0, "setResult"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 164
    new-instance v0, Landroid/view/ContextThemeWrapper;

    iget-object v1, p0, Lco/uk/getmondo/signup/identity_verification/e$h;->a:Lco/uk/getmondo/signup/identity_verification/e;

    invoke-virtual {v1}, Lco/uk/getmondo/signup/identity_verification/e;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0c010e

    invoke-direct {v0, v1, v2}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    .line 165
    iget-object v1, p0, Lco/uk/getmondo/signup/identity_verification/e$h;->a:Lco/uk/getmondo/signup/identity_verification/e;

    invoke-virtual {v1}, Lco/uk/getmondo/signup/identity_verification/e;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v1

    check-cast v0, Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/view/LayoutInflater;->cloneInContext(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 166
    const v1, 0x7f05007b

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 168
    new-instance v2, Landroid/support/design/widget/c;

    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/e$h;->a:Lco/uk/getmondo/signup/identity_verification/e;

    invoke-virtual {v0}, Lco/uk/getmondo/signup/identity_verification/e;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {v2, v0}, Landroid/support/design/widget/c;-><init>(Landroid/content/Context;)V

    .line 169
    invoke-virtual {v2, v1}, Landroid/support/design/widget/c;->setContentView(Landroid/view/View;)V

    .line 171
    const v0, 0x7f11028e

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    .line 172
    new-instance v0, Lco/uk/getmondo/signup/identity_verification/e$h$1;

    invoke-direct {v0, p1}, Lco/uk/getmondo/signup/identity_verification/e$h$1;-><init>(Lkotlin/d/a/b;)V

    check-cast v0, Landroid/view/View$OnClickListener;

    invoke-virtual {v3, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 173
    const v0, 0x7f11028f

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 174
    new-instance v0, Lco/uk/getmondo/signup/identity_verification/e$h$2;

    invoke-direct {v0, p1}, Lco/uk/getmondo/signup/identity_verification/e$h$2;-><init>(Lkotlin/d/a/b;)V

    check-cast v0, Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 176
    return-object v2
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 44
    check-cast p1, Lkotlin/d/a/b;

    invoke-virtual {p0, p1}, Lco/uk/getmondo/signup/identity_verification/e$h;->a(Lkotlin/d/a/b;)Landroid/support/design/widget/c;

    move-result-object v0

    return-object v0
.end method
