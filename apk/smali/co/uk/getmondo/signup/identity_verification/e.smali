.class public final Lco/uk/getmondo/signup/identity_verification/e;
.super Lco/uk/getmondo/common/f/a;
.source "IdentityDocumentsFragment.kt"

# interfaces
.implements Lco/uk/getmondo/signup/identity_verification/g$a;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lco/uk/getmondo/signup/identity_verification/e$b;,
        Lco/uk/getmondo/signup/identity_verification/e$a;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000\u00c4\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0010\u000e\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0010\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0010\u0000\n\u0002\u0008\u0011\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\r\u0018\u0000 p2\u00020\u00012\u00020\u0002:\u0002pqB\u0005\u00a2\u0006\u0002\u0010\u0003J\u0008\u0010/\u001a\u000200H\u0016J\u0008\u00101\u001a\u000200H\u0016J\u0008\u00102\u001a\u000200H\u0016J\u001a\u00103\u001a\u0002002\u0008\u00104\u001a\u0004\u0018\u00010$2\u0006\u00105\u001a\u000206H\u0002J\"\u00107\u001a\u0002002\u0006\u00108\u001a\u0002092\u0006\u0010:\u001a\u0002092\u0008\u0010;\u001a\u0004\u0018\u00010<H\u0016J\u0010\u0010=\u001a\u0002002\u0006\u0010>\u001a\u00020?H\u0016J\u0012\u0010@\u001a\u0002002\u0008\u0010A\u001a\u0004\u0018\u00010BH\u0016J$\u0010C\u001a\u00020D2\u0006\u0010E\u001a\u00020F2\u0008\u0010G\u001a\u0004\u0018\u00010H2\u0008\u0010A\u001a\u0004\u0018\u00010BH\u0016J\u0008\u0010I\u001a\u000200H\u0016J\u001a\u0010J\u001a\u0014\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00020\r\u0012\u0004\u0012\u00020\u000e0\u000c0KH\u0016J\u000e\u0010L\u001a\u0008\u0012\u0004\u0012\u00020\u000e0KH\u0016J\u0008\u0010M\u001a\u000200H\u0016J\u000e\u0010N\u001a\u0008\u0012\u0004\u0012\u0002000KH\u0016J\u000e\u0010O\u001a\u0008\u0012\u0004\u0012\u00020P0KH\u0016J\u000e\u0010Q\u001a\u0008\u0012\u0004\u0012\u0002000KH\u0016J\u000e\u0010R\u001a\u0008\u0012\u0004\u0012\u0002000KH\u0016J\u001a\u0010S\u001a\u0002002\u0006\u0010T\u001a\u00020D2\u0008\u0010A\u001a\u0004\u0018\u00010BH\u0016J\u0018\u0010U\u001a\u0002002\u0006\u0010V\u001a\u00020\u000e2\u0006\u0010W\u001a\u00020\u0005H\u0016J \u0010X\u001a\u0002002\u0006\u0010Y\u001a\u00020\u00112\u0006\u0010Z\u001a\u00020\u000e2\u0006\u0010[\u001a\u00020\rH\u0016J \u0010\\\u001a\u0002002\u0006\u0010Y\u001a\u00020\u00112\u0006\u0010V\u001a\u00020\u000e2\u0006\u0010[\u001a\u00020\rH\u0016J\u0010\u0010]\u001a\u0002002\u0006\u0010Y\u001a\u00020\u0011H\u0016J\u0018\u0010^\u001a\u0002002\u0006\u0010\u0010\u001a\u00020\u00112\u0006\u0010_\u001a\u00020\u0016H\u0016J\u0008\u0010`\u001a\u000200H\u0016J\u000e\u0010a\u001a\u0008\u0012\u0004\u0012\u00020\u00050bH\u0017J\u0010\u0010c\u001a\u0002002\u0006\u0010d\u001a\u00020eH\u0016J\u0008\u0010f\u001a\u000200H\u0016J\u0016\u0010g\u001a\u0008\u0012\u0004\u0012\u00020\u00050b2\u0006\u0010V\u001a\u00020\u000eH\u0016J\u000e\u0010h\u001a\u0008\u0012\u0004\u0012\u00020\u000e0bH\u0017J\u0008\u0010i\u001a\u000200H\u0016J\u000e\u0010j\u001a\u0008\u0012\u0004\u0012\u00020\u000e0bH\u0017J\u0010\u0010k\u001a\u0002002\u0006\u0010l\u001a\u00020$H\u0016J\u0010\u0010m\u001a\u0002002\u0006\u0010n\u001a\u00020$H\u0016J\u0008\u0010o\u001a\u000200H\u0016R\u001b\u0010\u0004\u001a\u00020\u00058BX\u0082\u0084\u0002\u00a2\u0006\u000c\n\u0004\u0008\u0008\u0010\t\u001a\u0004\u0008\u0006\u0010\u0007Rb\u0010\n\u001aV\u0012$\u0012\"\u0012\u0004\u0012\u00020\r\u0012\u0004\u0012\u00020\u000e \u000f*\u0010\u0012\u0004\u0012\u00020\r\u0012\u0004\u0012\u00020\u000e\u0018\u00010\u000c0\u000c \u000f**\u0012$\u0012\"\u0012\u0004\u0012\u00020\r\u0012\u0004\u0012\u00020\u000e \u000f*\u0010\u0012\u0004\u0012\u00020\r\u0012\u0004\u0012\u00020\u000e\u0018\u00010\u000c0\u000c\u0018\u00010\u000b0\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001b\u0010\u0010\u001a\u00020\u00118BX\u0082\u0084\u0002\u00a2\u0006\u000c\n\u0004\u0008\u0014\u0010\t\u001a\u0004\u0008\u0012\u0010\u0013R\u001b\u0010\u0015\u001a\u00020\u00168BX\u0082\u0084\u0002\u00a2\u0006\u000c\n\u0004\u0008\u0019\u0010\t\u001a\u0004\u0008\u0017\u0010\u0018R\u000e\u0010\u001a\u001a\u00020\u001bX\u0082.\u00a2\u0006\u0002\n\u0000R2\u0010\u001c\u001a&\u0012\u000c\u0012\n \u000f*\u0004\u0018\u00010\u000e0\u000e \u000f*\u0012\u0012\u000c\u0012\n \u000f*\u0004\u0018\u00010\u000e0\u000e\u0018\u00010\u000b0\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001e\u0010\u001d\u001a\u00020\u001e8\u0006@\u0006X\u0087.\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u001f\u0010 \"\u0004\u0008!\u0010\"R\u001d\u0010#\u001a\u0004\u0018\u00010$8BX\u0082\u0084\u0002\u00a2\u0006\u000c\n\u0004\u0008\'\u0010\t\u001a\u0004\u0008%\u0010&R\u0010\u0010(\u001a\u0004\u0018\u00010)X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u001b\u0010*\u001a\u00020+8BX\u0082\u0084\u0002\u00a2\u0006\u000c\n\u0004\u0008.\u0010\t\u001a\u0004\u0008,\u0010-\u00a8\u0006r"
    }
    d2 = {
        "Lco/uk/getmondo/signup/identity_verification/IdentityDocumentsFragment;",
        "Lco/uk/getmondo/common/fragments/BaseFragment;",
        "Lco/uk/getmondo/signup/identity_verification/IdentityDocumentsPresenter$View;",
        "()V",
        "allowSystemCamera",
        "",
        "getAllowSystemCamera",
        "()Z",
        "allowSystemCamera$delegate",
        "Lkotlin/Lazy;",
        "documentCountryRelay",
        "Lcom/jakewharton/rxrelay2/PublishRelay;",
        "Lkotlin/Pair;",
        "Lco/uk/getmondo/model/Country;",
        "Lco/uk/getmondo/api/model/identity_verification/IdentityDocumentType;",
        "kotlin.jvm.PlatformType",
        "identityVerificationVersion",
        "Lco/uk/getmondo/signup/identity_verification/data/IdentityVerificationVersion;",
        "getIdentityVerificationVersion",
        "()Lco/uk/getmondo/signup/identity_verification/data/IdentityVerificationVersion;",
        "identityVerificationVersion$delegate",
        "kycFrom",
        "Lco/uk/getmondo/api/model/tracking/Impression$KycFrom;",
        "getKycFrom",
        "()Lco/uk/getmondo/api/model/tracking/Impression$KycFrom;",
        "kycFrom$delegate",
        "listener",
        "Lco/uk/getmondo/signup/identity_verification/IdentityDocumentsFragment$StepListener;",
        "photoIdRelay",
        "presenter",
        "Lco/uk/getmondo/signup/identity_verification/IdentityDocumentsPresenter;",
        "getPresenter",
        "()Lco/uk/getmondo/signup/identity_verification/IdentityDocumentsPresenter;",
        "setPresenter",
        "(Lco/uk/getmondo/signup/identity_verification/IdentityDocumentsPresenter;)V",
        "rejectedReason",
        "",
        "getRejectedReason",
        "()Ljava/lang/String;",
        "rejectedReason$delegate",
        "rejectedReasonSnackbar",
        "Landroid/support/design/widget/Snackbar;",
        "signupSource",
        "Lco/uk/getmondo/api/model/signup/SignupSource;",
        "getSignupSource",
        "()Lco/uk/getmondo/api/model/signup/SignupSource;",
        "signupSource$delegate",
        "disableSubmitVerificationButton",
        "",
        "enableSubmitVerificationButton",
        "hideLoading",
        "loadThumbnail",
        "path",
        "imageView",
        "Landroid/widget/ImageView;",
        "onActivityResult",
        "requestCode",
        "",
        "resultCode",
        "data",
        "Landroid/content/Intent;",
        "onAttach",
        "context",
        "Landroid/content/Context;",
        "onCreate",
        "savedInstanceState",
        "Landroid/os/Bundle;",
        "onCreateView",
        "Landroid/view/View;",
        "inflater",
        "Landroid/view/LayoutInflater;",
        "container",
        "Landroid/view/ViewGroup;",
        "onDestroyView",
        "onDocumentCountrySelected",
        "Lio/reactivex/Observable;",
        "onPhotoIdTypeChosen",
        "onStop",
        "onSubmitVerificationClicked",
        "onSubmitVerificationConfirmationClicked",
        "",
        "onTakePhotoClicked",
        "onTakeVideoClicked",
        "onViewCreated",
        "view",
        "openCountrySelection",
        "documentType",
        "useSystemCamera",
        "openDocumentCamera",
        "version",
        "idDocumentType",
        "country",
        "openDocumentCameraFallback",
        "openTakeVideo",
        "openVerificationPending",
        "from",
        "openVideoFallback",
        "showDoYouHavePassport",
        "Lio/reactivex/Maybe;",
        "showIdentityDocComplete",
        "identityDocument",
        "Lco/uk/getmondo/signup/identity_verification/data/model/IdentityDocument;",
        "showIdentityDocIncomplete",
        "showIsDocumentUk",
        "showLegacyIdentityDocumentSelection",
        "showLoading",
        "showOtherIdentityDocumentsSelection",
        "showRejectedReason",
        "message",
        "showVideoComplete",
        "videoPath",
        "showVideoIncomplete",
        "Companion",
        "StepListener",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# static fields
.field static final synthetic a:[Lkotlin/reflect/l;

.field public static final d:Lco/uk/getmondo/signup/identity_verification/e$a;


# instance fields
.field public c:Lco/uk/getmondo/signup/identity_verification/g;

.field private final e:Lcom/b/b/c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/b/b/c",
            "<",
            "Lco/uk/getmondo/api/model/identity_verification/IdentityDocumentType;",
            ">;"
        }
    .end annotation
.end field

.field private final f:Lcom/b/b/c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/b/b/c",
            "<",
            "Lkotlin/h",
            "<",
            "Lco/uk/getmondo/d/i;",
            "Lco/uk/getmondo/api/model/identity_verification/IdentityDocumentType;",
            ">;>;"
        }
    .end annotation
.end field

.field private final g:Lkotlin/c;

.field private final h:Lkotlin/c;

.field private final i:Lkotlin/c;

.field private final j:Lkotlin/c;

.field private final k:Lkotlin/c;

.field private l:Lco/uk/getmondo/signup/identity_verification/e$b;

.field private m:Landroid/support/design/widget/Snackbar;

.field private n:Ljava/util/HashMap;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const/4 v0, 0x5

    new-array v1, v0, [Lkotlin/reflect/l;

    const/4 v2, 0x0

    new-instance v0, Lkotlin/d/b/w;

    const-class v3, Lco/uk/getmondo/signup/identity_verification/e;

    invoke-static {v3}, Lkotlin/d/b/y;->a(Ljava/lang/Class;)Lkotlin/reflect/c;

    move-result-object v3

    const-string v4, "identityVerificationVersion"

    const-string v5, "getIdentityVerificationVersion()Lco/uk/getmondo/signup/identity_verification/data/IdentityVerificationVersion;"

    invoke-direct {v0, v3, v4, v5}, Lkotlin/d/b/w;-><init>(Lkotlin/reflect/e;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v0}, Lkotlin/d/b/y;->a(Lkotlin/d/b/v;)Lkotlin/reflect/n;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/l;

    aput-object v0, v1, v2

    const/4 v2, 0x1

    new-instance v0, Lkotlin/d/b/w;

    const-class v3, Lco/uk/getmondo/signup/identity_verification/e;

    invoke-static {v3}, Lkotlin/d/b/y;->a(Ljava/lang/Class;)Lkotlin/reflect/c;

    move-result-object v3

    const-string v4, "kycFrom"

    const-string v5, "getKycFrom()Lco/uk/getmondo/api/model/tracking/Impression$KycFrom;"

    invoke-direct {v0, v3, v4, v5}, Lkotlin/d/b/w;-><init>(Lkotlin/reflect/e;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v0}, Lkotlin/d/b/y;->a(Lkotlin/d/b/v;)Lkotlin/reflect/n;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/l;

    aput-object v0, v1, v2

    const/4 v2, 0x2

    new-instance v0, Lkotlin/d/b/w;

    const-class v3, Lco/uk/getmondo/signup/identity_verification/e;

    invoke-static {v3}, Lkotlin/d/b/y;->a(Ljava/lang/Class;)Lkotlin/reflect/c;

    move-result-object v3

    const-string v4, "rejectedReason"

    const-string v5, "getRejectedReason()Ljava/lang/String;"

    invoke-direct {v0, v3, v4, v5}, Lkotlin/d/b/w;-><init>(Lkotlin/reflect/e;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v0}, Lkotlin/d/b/y;->a(Lkotlin/d/b/v;)Lkotlin/reflect/n;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/l;

    aput-object v0, v1, v2

    const/4 v2, 0x3

    new-instance v0, Lkotlin/d/b/w;

    const-class v3, Lco/uk/getmondo/signup/identity_verification/e;

    invoke-static {v3}, Lkotlin/d/b/y;->a(Ljava/lang/Class;)Lkotlin/reflect/c;

    move-result-object v3

    const-string v4, "allowSystemCamera"

    const-string v5, "getAllowSystemCamera()Z"

    invoke-direct {v0, v3, v4, v5}, Lkotlin/d/b/w;-><init>(Lkotlin/reflect/e;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v0}, Lkotlin/d/b/y;->a(Lkotlin/d/b/v;)Lkotlin/reflect/n;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/l;

    aput-object v0, v1, v2

    const/4 v2, 0x4

    new-instance v0, Lkotlin/d/b/w;

    const-class v3, Lco/uk/getmondo/signup/identity_verification/e;

    invoke-static {v3}, Lkotlin/d/b/y;->a(Ljava/lang/Class;)Lkotlin/reflect/c;

    move-result-object v3

    const-string v4, "signupSource"

    const-string v5, "getSignupSource()Lco/uk/getmondo/api/model/signup/SignupSource;"

    invoke-direct {v0, v3, v4, v5}, Lkotlin/d/b/w;-><init>(Lkotlin/reflect/e;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v0}, Lkotlin/d/b/y;->a(Lkotlin/d/b/v;)Lkotlin/reflect/n;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/l;

    aput-object v0, v1, v2

    sput-object v1, Lco/uk/getmondo/signup/identity_verification/e;->a:[Lkotlin/reflect/l;

    new-instance v0, Lco/uk/getmondo/signup/identity_verification/e$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lco/uk/getmondo/signup/identity_verification/e$a;-><init>(Lkotlin/d/b/i;)V

    sput-object v0, Lco/uk/getmondo/signup/identity_verification/e;->d:Lco/uk/getmondo/signup/identity_verification/e$a;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 44
    invoke-direct {p0}, Lco/uk/getmondo/common/f/a;-><init>()V

    .line 45
    invoke-static {}, Lcom/b/b/c;->a()Lcom/b/b/c;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/signup/identity_verification/e;->e:Lcom/b/b/c;

    .line 46
    invoke-static {}, Lcom/b/b/c;->a()Lcom/b/b/c;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/signup/identity_verification/e;->f:Lcom/b/b/c;

    .line 48
    new-instance v0, Lco/uk/getmondo/signup/identity_verification/e$d;

    invoke-direct {v0, p0}, Lco/uk/getmondo/signup/identity_verification/e$d;-><init>(Lco/uk/getmondo/signup/identity_verification/e;)V

    check-cast v0, Lkotlin/d/a/a;

    invoke-static {v0}, Lkotlin/d;->a(Lkotlin/d/a/a;)Lkotlin/c;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/signup/identity_verification/e;->g:Lkotlin/c;

    .line 49
    new-instance v0, Lco/uk/getmondo/signup/identity_verification/e$e;

    invoke-direct {v0, p0}, Lco/uk/getmondo/signup/identity_verification/e$e;-><init>(Lco/uk/getmondo/signup/identity_verification/e;)V

    check-cast v0, Lkotlin/d/a/a;

    invoke-static {v0}, Lkotlin/d;->a(Lkotlin/d/a/a;)Lkotlin/c;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/signup/identity_verification/e;->h:Lkotlin/c;

    .line 50
    new-instance v0, Lco/uk/getmondo/signup/identity_verification/e$g;

    invoke-direct {v0, p0}, Lco/uk/getmondo/signup/identity_verification/e$g;-><init>(Lco/uk/getmondo/signup/identity_verification/e;)V

    check-cast v0, Lkotlin/d/a/a;

    invoke-static {v0}, Lkotlin/d;->a(Lkotlin/d/a/a;)Lkotlin/c;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/signup/identity_verification/e;->i:Lkotlin/c;

    .line 51
    new-instance v0, Lco/uk/getmondo/signup/identity_verification/e$c;

    invoke-direct {v0, p0}, Lco/uk/getmondo/signup/identity_verification/e$c;-><init>(Lco/uk/getmondo/signup/identity_verification/e;)V

    check-cast v0, Lkotlin/d/a/a;

    invoke-static {v0}, Lkotlin/d;->a(Lkotlin/d/a/a;)Lkotlin/c;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/signup/identity_verification/e;->j:Lkotlin/c;

    .line 52
    new-instance v0, Lco/uk/getmondo/signup/identity_verification/e$m;

    invoke-direct {v0, p0}, Lco/uk/getmondo/signup/identity_verification/e$m;-><init>(Lco/uk/getmondo/signup/identity_verification/e;)V

    check-cast v0, Lkotlin/d/a/a;

    invoke-static {v0}, Lkotlin/d;->a(Lkotlin/d/a/a;)Lkotlin/c;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/signup/identity_verification/e;->k:Lkotlin/c;

    return-void
.end method

.method public static final synthetic a(Lco/uk/getmondo/signup/identity_verification/e;)Landroid/support/design/widget/Snackbar;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/e;->m:Landroid/support/design/widget/Snackbar;

    return-object v0
.end method

.method private final a(Ljava/lang/String;Landroid/widget/ImageView;)V
    .locals 6

    .prologue
    .line 285
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 286
    check-cast p0, Landroid/support/v4/app/Fragment;

    invoke-static {p0}, Lcom/bumptech/glide/g;->a(Landroid/support/v4/app/Fragment;)Lcom/bumptech/glide/j;

    move-result-object v0

    .line 287
    invoke-virtual {v0, v1}, Lcom/bumptech/glide/j;->a(Ljava/io/File;)Lcom/bumptech/glide/d;

    move-result-object v2

    .line 288
    new-instance v0, Lcom/bumptech/glide/h/b;

    invoke-virtual {v1}, Ljava/io/File;->lastModified()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/bumptech/glide/h/b;-><init>(Ljava/lang/String;)V

    check-cast v0, Lcom/bumptech/glide/load/c;

    invoke-virtual {v2, v0}, Lcom/bumptech/glide/d;->a(Lcom/bumptech/glide/load/c;)Lcom/bumptech/glide/c;

    move-result-object v0

    .line 289
    const v1, 0x3e4ccccd    # 0.2f

    invoke-virtual {v0, v1}, Lcom/bumptech/glide/c;->a(F)Lcom/bumptech/glide/c;

    move-result-object v0

    .line 290
    invoke-virtual {v0, p2}, Lcom/bumptech/glide/c;->a(Landroid/widget/ImageView;)Lcom/bumptech/glide/g/b/j;

    .line 291
    return-void
.end method

.method private final r()Lco/uk/getmondo/signup/identity_verification/a/j;
    .locals 3

    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/e;->g:Lkotlin/c;

    sget-object v1, Lco/uk/getmondo/signup/identity_verification/e;->a:[Lkotlin/reflect/l;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-interface {v0}, Lkotlin/c;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/signup/identity_verification/a/j;

    return-object v0
.end method

.method private final s()Lco/uk/getmondo/api/model/tracking/Impression$KycFrom;
    .locals 3

    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/e;->h:Lkotlin/c;

    sget-object v1, Lco/uk/getmondo/signup/identity_verification/e;->a:[Lkotlin/reflect/l;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-interface {v0}, Lkotlin/c;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/api/model/tracking/Impression$KycFrom;

    return-object v0
.end method

.method private final t()Ljava/lang/String;
    .locals 3

    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/e;->i:Lkotlin/c;

    sget-object v1, Lco/uk/getmondo/signup/identity_verification/e;->a:[Lkotlin/reflect/l;

    const/4 v2, 0x2

    aget-object v1, v1, v2

    invoke-interface {v0}, Lkotlin/c;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method private final v()Z
    .locals 3

    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/e;->j:Lkotlin/c;

    sget-object v1, Lco/uk/getmondo/signup/identity_verification/e;->a:[Lkotlin/reflect/l;

    const/4 v2, 0x3

    aget-object v1, v1, v2

    invoke-interface {v0}, Lkotlin/c;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method

.method private final w()Lco/uk/getmondo/api/model/signup/SignupSource;
    .locals 3

    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/e;->k:Lkotlin/c;

    sget-object v1, Lco/uk/getmondo/signup/identity_verification/e;->a:[Lkotlin/reflect/l;

    const/4 v2, 0x4

    aget-object v1, v1, v2

    invoke-interface {v0}, Lkotlin/c;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/api/model/signup/SignupSource;

    return-object v0
.end method


# virtual methods
.method public a(I)Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/e;->n:Ljava/util/HashMap;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lco/uk/getmondo/signup/identity_verification/e;->n:Ljava/util/HashMap;

    :cond_0
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/e;->n:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    if-nez v0, :cond_1

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getView()Landroid/view/View;

    move-result-object v0

    if-nez v0, :cond_2

    const/4 v0, 0x0

    :cond_1
    :goto_0
    return-object v0

    :cond_2
    invoke-virtual {v0, p1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/signup/identity_verification/e;->n:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method public a(Lco/uk/getmondo/api/model/identity_verification/IdentityDocumentType;)Lio/reactivex/h;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lco/uk/getmondo/api/model/identity_verification/IdentityDocumentType;",
            ")",
            "Lio/reactivex/h",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .prologue
    const-string v0, "documentType"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 200
    invoke-virtual {p1}, Lco/uk/getmondo/api/model/identity_verification/IdentityDocumentType;->c()I

    move-result v0

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/identity_verification/e;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 201
    new-instance v0, Lco/uk/getmondo/signup/identity_verification/e$i;

    invoke-direct {v0, p0, v1}, Lco/uk/getmondo/signup/identity_verification/e$i;-><init>(Lco/uk/getmondo/signup/identity_verification/e;Ljava/lang/String;)V

    check-cast v0, Lkotlin/d/a/b;

    invoke-static {v0}, Lco/uk/getmondo/common/j/e;->a(Lkotlin/d/a/b;)Lio/reactivex/h;

    move-result-object v0

    return-object v0
.end method

.method public a()Lio/reactivex/n;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Lkotlin/n;",
            ">;"
        }
    .end annotation

    .prologue
    .line 108
    sget v0, Lco/uk/getmondo/c$a;->takePhotoButton:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/identity_verification/e;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 338
    invoke-static {v0}, Lcom/b/a/c/c;->a(Landroid/view/View;)Lio/reactivex/n;

    move-result-object v1

    sget-object v0, Lcom/b/a/a/d;->a:Lcom/b/a/a/d;

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {v1, v0}, Lio/reactivex/n;->map(Lio/reactivex/c/h;)Lio/reactivex/n;

    move-result-object v0

    const-string v1, "RxView.clicks(this).map(VoidToUnit)"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 108
    return-object v0
.end method

.method public a(Lco/uk/getmondo/api/model/identity_verification/IdentityDocumentType;Z)V
    .locals 2

    .prologue
    const-string v0, "documentType"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 241
    invoke-virtual {p0}, Lco/uk/getmondo/signup/identity_verification/e;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, p1}, Lco/uk/getmondo/signup/identity_verification/CountrySelectionActivity;->a(Landroid/content/Context;Lco/uk/getmondo/api/model/identity_verification/IdentityDocumentType;)Landroid/content/Intent;

    move-result-object v0

    const/16 v1, 0x3e9

    invoke-virtual {p0, v0, v1}, Lco/uk/getmondo/signup/identity_verification/e;->startActivityForResult(Landroid/content/Intent;I)V

    .line 242
    return-void
.end method

.method public a(Lco/uk/getmondo/signup/identity_verification/a/a/b;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const-string v0, "identityDocument"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 245
    sget v0, Lco/uk/getmondo/c$a;->photoCompleteImageView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/identity_verification/e;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    const v1, 0x7f0201c2

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 246
    sget v0, Lco/uk/getmondo/c$a;->takePhotoButton:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/identity_verification/e;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    const v1, 0x7f0a01fa

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(I)V

    .line 247
    sget v0, Lco/uk/getmondo/c$a;->takePhotoButton:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/identity_verification/e;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setActivated(Z)V

    .line 248
    sget v0, Lco/uk/getmondo/c$a;->primaryDocumentThumbnail:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/identity_verification/e;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 249
    invoke-virtual {p1}, Lco/uk/getmondo/signup/identity_verification/a/a/b;->a()Ljava/lang/String;

    move-result-object v1

    sget v0, Lco/uk/getmondo/c$a;->primaryDocumentThumbnail:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/identity_verification/e;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    const-string v2, "primaryDocumentThumbnail"

    invoke-static {v0, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, v1, v0}, Lco/uk/getmondo/signup/identity_verification/e;->a(Ljava/lang/String;Landroid/widget/ImageView;)V

    .line 250
    invoke-virtual {p1}, Lco/uk/getmondo/signup/identity_verification/a/a/b;->d()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 251
    sget v0, Lco/uk/getmondo/c$a;->secondaryDocumentThumbnail:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/identity_verification/e;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 252
    invoke-virtual {p1}, Lco/uk/getmondo/signup/identity_verification/a/a/b;->d()Ljava/lang/String;

    move-result-object v1

    sget v0, Lco/uk/getmondo/c$a;->secondaryDocumentThumbnail:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/identity_verification/e;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    const-string v2, "secondaryDocumentThumbnail"

    invoke-static {v0, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, v1, v0}, Lco/uk/getmondo/signup/identity_verification/e;->a(Ljava/lang/String;Landroid/widget/ImageView;)V

    .line 255
    :goto_0
    return-void

    .line 254
    :cond_0
    sget v0, Lco/uk/getmondo/c$a;->secondaryDocumentThumbnail:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/identity_verification/e;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0
.end method

.method public a(Lco/uk/getmondo/signup/identity_verification/a/j;)V
    .locals 3

    .prologue
    const-string v0, "version"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 213
    sget-object v0, Lco/uk/getmondo/signup/identity_verification/video/VideoRecordingActivity;->b:Lco/uk/getmondo/signup/identity_verification/video/VideoRecordingActivity$a;

    invoke-virtual {p0}, Lco/uk/getmondo/signup/identity_verification/e;->getContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "context"

    invoke-static {v1, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Lco/uk/getmondo/signup/identity_verification/e;->w()Lco/uk/getmondo/api/model/signup/SignupSource;

    move-result-object v2

    invoke-virtual {v0, v1, p1, v2}, Lco/uk/getmondo/signup/identity_verification/video/VideoRecordingActivity$a;->a(Landroid/content/Context;Lco/uk/getmondo/signup/identity_verification/a/j;Lco/uk/getmondo/api/model/signup/SignupSource;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/identity_verification/e;->startActivity(Landroid/content/Intent;)V

    .line 214
    return-void
.end method

.method public a(Lco/uk/getmondo/signup/identity_verification/a/j;Lco/uk/getmondo/api/model/identity_verification/IdentityDocumentType;Lco/uk/getmondo/d/i;)V
    .locals 2

    .prologue
    const-string v0, "version"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "idDocumentType"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "country"

    invoke-static {p3, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 131
    invoke-virtual {p0}, Lco/uk/getmondo/signup/identity_verification/e;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {p0}, Lco/uk/getmondo/signup/identity_verification/e;->w()Lco/uk/getmondo/api/model/signup/SignupSource;

    move-result-object v1

    invoke-static {v0, p1, p2, p3, v1}, Lco/uk/getmondo/signup/identity_verification/id_picture/DocumentCameraActivity;->a(Landroid/content/Context;Lco/uk/getmondo/signup/identity_verification/a/j;Lco/uk/getmondo/api/model/identity_verification/IdentityDocumentType;Lco/uk/getmondo/d/i;Lco/uk/getmondo/api/model/signup/SignupSource;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/identity_verification/e;->startActivity(Landroid/content/Intent;)V

    .line 132
    return-void
.end method

.method public a(Lco/uk/getmondo/signup/identity_verification/a/j;Lco/uk/getmondo/api/model/tracking/Impression$KycFrom;)V
    .locals 2

    .prologue
    const-string v0, "identityVerificationVersion"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "from"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 237
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/e;->l:Lco/uk/getmondo/signup/identity_verification/e$b;

    if-nez v0, :cond_0

    const-string v1, "listener"

    invoke-static {v1}, Lkotlin/d/b/l;->b(Ljava/lang/String;)V

    :cond_0
    invoke-interface {v0}, Lco/uk/getmondo/signup/identity_verification/e$b;->e_()V

    .line 238
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 6

    .prologue
    const-string v0, "message"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 102
    invoke-virtual {p0}, Lco/uk/getmondo/signup/identity_verification/e;->getContext()Landroid/content/Context;

    move-result-object v1

    sget v0, Lco/uk/getmondo/c$a;->rootView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/identity_verification/e;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    sget-object v2, Ljava/util/concurrent/TimeUnit;->MINUTES:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v4, 0x1

    invoke-virtual {v2, v4, v5}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v2

    long-to-int v2, v2

    const/4 v3, 0x1

    invoke-static {v1, v0, p1, v2, v3}, Lco/uk/getmondo/common/ui/i;->a(Landroid/content/Context;Landroid/view/View;Ljava/lang/String;IZ)Landroid/support/design/widget/Snackbar;

    move-result-object v1

    .line 103
    const v2, 0x7f0a019a

    new-instance v0, Lco/uk/getmondo/signup/identity_verification/e$l;

    invoke-direct {v0, p0}, Lco/uk/getmondo/signup/identity_verification/e$l;-><init>(Lco/uk/getmondo/signup/identity_verification/e;)V

    check-cast v0, Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2, v0}, Landroid/support/design/widget/Snackbar;->a(ILandroid/view/View$OnClickListener;)Landroid/support/design/widget/Snackbar;

    move-result-object v0

    .line 104
    invoke-virtual {p0}, Lco/uk/getmondo/signup/identity_verification/e;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0f00ea

    invoke-static {v1, v2}, Landroid/support/v4/content/a;->c(Landroid/content/Context;I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/support/design/widget/Snackbar;->e(I)Landroid/support/design/widget/Snackbar;

    move-result-object v0

    .line 105
    invoke-virtual {v0}, Landroid/support/design/widget/Snackbar;->c()V

    iput-object v0, p0, Lco/uk/getmondo/signup/identity_verification/e;->m:Landroid/support/design/widget/Snackbar;

    .line 106
    return-void
.end method

.method public b()Lio/reactivex/n;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Lkotlin/n;",
            ">;"
        }
    .end annotation

    .prologue
    .line 112
    sget v0, Lco/uk/getmondo/c$a;->takeVideoButton:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/identity_verification/e;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 339
    invoke-static {v0}, Lcom/b/a/c/c;->a(Landroid/view/View;)Lio/reactivex/n;

    move-result-object v1

    sget-object v0, Lcom/b/a/a/d;->a:Lcom/b/a/a/d;

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {v1, v0}, Lio/reactivex/n;->map(Lio/reactivex/c/h;)Lio/reactivex/n;

    move-result-object v0

    const-string v1, "RxView.clicks(this).map(VoidToUnit)"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 112
    return-object v0
.end method

.method public b(Lco/uk/getmondo/signup/identity_verification/a/j;Lco/uk/getmondo/api/model/identity_verification/IdentityDocumentType;Lco/uk/getmondo/d/i;)V
    .locals 3

    .prologue
    const-string v0, "version"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "documentType"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "country"

    invoke-static {p3, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 135
    sget-object v0, Lco/uk/getmondo/signup/identity_verification/fallback/DocumentFallbackActivity;->c:Lco/uk/getmondo/signup/identity_verification/fallback/DocumentFallbackActivity$a;

    invoke-virtual {p0}, Lco/uk/getmondo/signup/identity_verification/e;->getContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "context"

    invoke-static {v1, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Lco/uk/getmondo/signup/identity_verification/e;->w()Lco/uk/getmondo/api/model/signup/SignupSource;

    move-result-object v2

    invoke-virtual {v0, v1, p2, p3, v2}, Lco/uk/getmondo/signup/identity_verification/fallback/DocumentFallbackActivity$a;->a(Landroid/content/Context;Lco/uk/getmondo/api/model/identity_verification/IdentityDocumentType;Lco/uk/getmondo/d/i;Lco/uk/getmondo/api/model/signup/SignupSource;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/identity_verification/e;->startActivity(Landroid/content/Intent;)V

    .line 136
    return-void
.end method

.method public b(Ljava/lang/String;)V
    .locals 2

    .prologue
    const-string v0, "videoPath"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 269
    sget v0, Lco/uk/getmondo/c$a;->videoCompleteImageView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/identity_verification/e;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    const v1, 0x7f0201c2

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 270
    sget v0, Lco/uk/getmondo/c$a;->takeVideoButton:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/identity_verification/e;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    const v1, 0x7f0a01fa

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(I)V

    .line 271
    sget v0, Lco/uk/getmondo/c$a;->takeVideoButton:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/identity_verification/e;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setActivated(Z)V

    .line 272
    sget v0, Lco/uk/getmondo/c$a;->videoThumbnail:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/identity_verification/e;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 273
    sget v0, Lco/uk/getmondo/c$a;->videoThumbnail:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/identity_verification/e;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    const-string v1, "videoThumbnail"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, p1, v0}, Lco/uk/getmondo/signup/identity_verification/e;->a(Ljava/lang/String;Landroid/widget/ImageView;)V

    .line 274
    return-void
.end method

.method public c()Lio/reactivex/n;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Lkotlin/n;",
            ">;"
        }
    .end annotation

    .prologue
    .line 114
    sget v0, Lco/uk/getmondo/c$a;->submitVerificationButton:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/identity_verification/e;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/common/ui/ProgressButton;

    .line 340
    invoke-static {v0}, Lcom/b/a/c/c;->a(Landroid/view/View;)Lio/reactivex/n;

    move-result-object v1

    sget-object v0, Lcom/b/a/a/d;->a:Lcom/b/a/a/d;

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {v1, v0}, Lio/reactivex/n;->map(Lio/reactivex/c/h;)Lio/reactivex/n;

    move-result-object v0

    const-string v1, "RxView.clicks(this).map(VoidToUnit)"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 114
    return-object v0
.end method

.method public d()Lio/reactivex/n;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 117
    new-instance v0, Lco/uk/getmondo/signup/identity_verification/e$f;

    invoke-direct {v0, p0}, Lco/uk/getmondo/signup/identity_verification/e$f;-><init>(Lco/uk/getmondo/signup/identity_verification/e;)V

    check-cast v0, Lio/reactivex/p;

    invoke-static {v0}, Lio/reactivex/n;->create(Lio/reactivex/p;)Lio/reactivex/n;

    move-result-object v0

    const-string v1, "Observable.create { emit\u2026)\n            }\n        }"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public e()Lio/reactivex/n;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Lkotlin/h",
            "<",
            "Lco/uk/getmondo/d/i;",
            "Lco/uk/getmondo/api/model/identity_verification/IdentityDocumentType;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 139
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/e;->f:Lcom/b/b/c;

    const-string v1, "documentCountryRelay"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lio/reactivex/n;

    return-object v0
.end method

.method public f()Lio/reactivex/h;
    .locals 1
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "InflateParams"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/h",
            "<",
            "Lco/uk/getmondo/api/model/identity_verification/IdentityDocumentType;",
            ">;"
        }
    .end annotation

    .prologue
    .line 144
    new-instance v0, Lco/uk/getmondo/signup/identity_verification/e$j;

    invoke-direct {v0, p0}, Lco/uk/getmondo/signup/identity_verification/e$j;-><init>(Lco/uk/getmondo/signup/identity_verification/e;)V

    check-cast v0, Lkotlin/d/a/b;

    invoke-static {v0}, Lco/uk/getmondo/common/j/e;->a(Lkotlin/d/a/b;)Lio/reactivex/h;

    move-result-object v0

    return-object v0
.end method

.method public g()Lio/reactivex/h;
    .locals 1
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "InflateParams"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/h",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .prologue
    .line 163
    new-instance v0, Lco/uk/getmondo/signup/identity_verification/e$h;

    invoke-direct {v0, p0}, Lco/uk/getmondo/signup/identity_verification/e$h;-><init>(Lco/uk/getmondo/signup/identity_verification/e;)V

    check-cast v0, Lkotlin/d/a/b;

    invoke-static {v0}, Lco/uk/getmondo/common/j/e;->a(Lkotlin/d/a/b;)Lio/reactivex/h;

    move-result-object v0

    return-object v0
.end method

.method public h()Lio/reactivex/h;
    .locals 1
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "InflateParams"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/h",
            "<",
            "Lco/uk/getmondo/api/model/identity_verification/IdentityDocumentType;",
            ">;"
        }
    .end annotation

    .prologue
    .line 182
    new-instance v0, Lco/uk/getmondo/signup/identity_verification/e$k;

    invoke-direct {v0, p0}, Lco/uk/getmondo/signup/identity_verification/e$k;-><init>(Lco/uk/getmondo/signup/identity_verification/e;)V

    check-cast v0, Lkotlin/d/a/b;

    invoke-static {v0}, Lco/uk/getmondo/common/j/e;->a(Lkotlin/d/a/b;)Lio/reactivex/h;

    move-result-object v0

    return-object v0
.end method

.method public i()V
    .locals 3

    .prologue
    .line 217
    sget-object v0, Lco/uk/getmondo/signup/identity_verification/fallback/VideoFallbackActivity;->e:Lco/uk/getmondo/signup/identity_verification/fallback/VideoFallbackActivity$a;

    invoke-virtual {p0}, Lco/uk/getmondo/signup/identity_verification/e;->getContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "context"

    invoke-static {v1, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Lco/uk/getmondo/signup/identity_verification/e;->w()Lco/uk/getmondo/api/model/signup/SignupSource;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lco/uk/getmondo/signup/identity_verification/fallback/VideoFallbackActivity$a;->a(Landroid/content/Context;Lco/uk/getmondo/api/model/signup/SignupSource;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/identity_verification/e;->startActivity(Landroid/content/Intent;)V

    .line 218
    return-void
.end method

.method public j()V
    .locals 2

    .prologue
    .line 221
    sget v0, Lco/uk/getmondo/c$a;->submitVerificationButton:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/identity_verification/e;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/common/ui/ProgressButton;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lco/uk/getmondo/common/ui/ProgressButton;->setLoading(Z)V

    .line 222
    return-void
.end method

.method public k()V
    .locals 2

    .prologue
    .line 225
    sget v0, Lco/uk/getmondo/c$a;->submitVerificationButton:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/identity_verification/e;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/common/ui/ProgressButton;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lco/uk/getmondo/common/ui/ProgressButton;->setLoading(Z)V

    .line 226
    return-void
.end method

.method public l()V
    .locals 2

    .prologue
    .line 229
    sget v0, Lco/uk/getmondo/c$a;->submitVerificationButton:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/identity_verification/e;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/common/ui/ProgressButton;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lco/uk/getmondo/common/ui/ProgressButton;->setEnabled(Z)V

    .line 230
    return-void
.end method

.method public m()V
    .locals 2

    .prologue
    .line 233
    sget v0, Lco/uk/getmondo/c$a;->submitVerificationButton:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/identity_verification/e;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/common/ui/ProgressButton;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lco/uk/getmondo/common/ui/ProgressButton;->setEnabled(Z)V

    .line 234
    return-void
.end method

.method public n()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/16 v2, 0x8

    .line 259
    sget v0, Lco/uk/getmondo/c$a;->photoCompleteImageView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/identity_verification/e;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    const v1, 0x7f02014a

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 260
    sget v0, Lco/uk/getmondo/c$a;->takePhotoButton:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/identity_verification/e;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    const v1, 0x7f0a020f

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(I)V

    .line 261
    sget v0, Lco/uk/getmondo/c$a;->takePhotoButton:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/identity_verification/e;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setActivated(Z)V

    .line 262
    sget v0, Lco/uk/getmondo/c$a;->primaryDocumentThumbnail:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/identity_verification/e;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 263
    sget v0, Lco/uk/getmondo/c$a;->primaryDocumentThumbnail:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/identity_verification/e;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 264
    sget v0, Lco/uk/getmondo/c$a;->secondaryDocumentThumbnail:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/identity_verification/e;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 265
    sget v0, Lco/uk/getmondo/c$a;->secondaryDocumentThumbnail:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/identity_verification/e;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 266
    return-void
.end method

.method public o()V
    .locals 2

    .prologue
    .line 277
    sget v0, Lco/uk/getmondo/c$a;->videoCompleteImageView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/identity_verification/e;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    const v1, 0x7f02014b

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 278
    sget v0, Lco/uk/getmondo/c$a;->takeVideoButton:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/identity_verification/e;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    const v1, 0x7f0a021c

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(I)V

    .line 279
    sget v0, Lco/uk/getmondo/c$a;->takeVideoButton:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/identity_verification/e;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setActivated(Z)V

    .line 280
    sget v0, Lco/uk/getmondo/c$a;->videoThumbnail:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/identity_verification/e;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 281
    sget v0, Lco/uk/getmondo/c$a;->videoThumbnail:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/identity_verification/e;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 282
    return-void
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 4

    .prologue
    .line 294
    const/16 v0, 0x3e9

    if-ne p1, v0, :cond_0

    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    .line 295
    invoke-static {p3}, Lco/uk/getmondo/signup/identity_verification/CountrySelectionActivity;->b(Landroid/content/Intent;)Lco/uk/getmondo/d/i;

    move-result-object v0

    .line 296
    invoke-static {p3}, Lco/uk/getmondo/signup/identity_verification/CountrySelectionActivity;->a(Landroid/content/Intent;)Lco/uk/getmondo/api/model/identity_verification/IdentityDocumentType;

    move-result-object v1

    .line 297
    iget-object v2, p0, Lco/uk/getmondo/signup/identity_verification/e;->f:Lcom/b/b/c;

    new-instance v3, Lkotlin/h;

    invoke-direct {v3, v0, v1}, Lkotlin/h;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-virtual {v2, v3}, Lcom/b/b/c;->a(Ljava/lang/Object;)V

    .line 300
    :goto_0
    return-void

    .line 299
    :cond_0
    invoke-super {p0, p1, p2, p3}, Lco/uk/getmondo/common/f/a;->onActivityResult(IILandroid/content/Intent;)V

    goto :goto_0
.end method

.method public onAttach(Landroid/content/Context;)V
    .locals 2

    .prologue
    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 82
    invoke-super {p0, p1}, Lco/uk/getmondo/common/f/a;->onAttach(Landroid/content/Context;)V

    .line 84
    instance-of v0, p1, Lco/uk/getmondo/signup/identity_verification/e$b;

    if-eqz v0, :cond_0

    .line 85
    check-cast p1, Lco/uk/getmondo/signup/identity_verification/e$b;

    iput-object p1, p0, Lco/uk/getmondo/signup/identity_verification/e;->l:Lco/uk/getmondo/signup/identity_verification/e$b;

    .line 88
    return-void

    .line 87
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Activity must implement StepListener"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    .line 60
    invoke-super {p0, p1}, Lco/uk/getmondo/common/f/a;->onCreate(Landroid/os/Bundle;)V

    .line 62
    invoke-virtual {p0}, Lco/uk/getmondo/signup/identity_verification/e;->B()Lco/uk/getmondo/common/h/b/b;

    move-result-object v0

    .line 63
    new-instance v1, Lco/uk/getmondo/signup/identity_verification/p;

    invoke-direct {p0}, Lco/uk/getmondo/signup/identity_verification/e;->r()Lco/uk/getmondo/signup/identity_verification/a/j;

    move-result-object v2

    invoke-direct {p0}, Lco/uk/getmondo/signup/identity_verification/e;->w()Lco/uk/getmondo/api/model/signup/SignupSource;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lco/uk/getmondo/signup/identity_verification/p;-><init>(Lco/uk/getmondo/signup/identity_verification/a/j;Lco/uk/getmondo/api/model/signup/SignupSource;)V

    invoke-interface {v0, v1}, Lco/uk/getmondo/common/h/b/b;->a(Lco/uk/getmondo/signup/identity_verification/p;)Lco/uk/getmondo/signup/identity_verification/o;

    move-result-object v0

    .line 64
    new-instance v1, Lco/uk/getmondo/signup/identity_verification/j;

    invoke-direct {p0}, Lco/uk/getmondo/signup/identity_verification/e;->s()Lco/uk/getmondo/api/model/tracking/Impression$KycFrom;

    move-result-object v2

    invoke-direct {p0}, Lco/uk/getmondo/signup/identity_verification/e;->t()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0}, Lco/uk/getmondo/signup/identity_verification/e;->v()Z

    move-result v4

    invoke-direct {v1, v2, v3, v4}, Lco/uk/getmondo/signup/identity_verification/j;-><init>(Lco/uk/getmondo/api/model/tracking/Impression$KycFrom;Ljava/lang/String;Z)V

    invoke-interface {v0, v1}, Lco/uk/getmondo/signup/identity_verification/o;->a(Lco/uk/getmondo/signup/identity_verification/j;)Lco/uk/getmondo/signup/identity_verification/i;

    move-result-object v0

    .line 65
    invoke-interface {v0, p0}, Lco/uk/getmondo/signup/identity_verification/i;->a(Lco/uk/getmondo/signup/identity_verification/e;)V

    .line 66
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2

    .prologue
    const-string v0, "inflater"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 69
    const v0, 0x7f05009f

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    const-string v1, "inflater.inflate(R.layou\u2026uments, container, false)"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public onDestroyView()V
    .locals 2

    .prologue
    .line 97
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/e;->c:Lco/uk/getmondo/signup/identity_verification/g;

    if-nez v0, :cond_0

    const-string v1, "presenter"

    invoke-static {v1}, Lkotlin/d/b/l;->b(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0}, Lco/uk/getmondo/signup/identity_verification/g;->b()V

    .line 98
    invoke-super {p0}, Lco/uk/getmondo/common/f/a;->onDestroyView()V

    .line 99
    invoke-virtual {p0}, Lco/uk/getmondo/signup/identity_verification/e;->p()V

    return-void
.end method

.method public onStop()V
    .locals 1

    .prologue
    .line 92
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/e;->m:Landroid/support/design/widget/Snackbar;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/support/design/widget/Snackbar;->d()V

    .line 93
    :cond_0
    invoke-super {p0}, Lco/uk/getmondo/common/f/a;->onStop()V

    .line 94
    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    const-string v0, "view"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 72
    invoke-super {p0, p1, p2}, Lco/uk/getmondo/common/f/a;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 74
    sget v0, Lco/uk/getmondo/c$a;->primaryDocumentThumbnail:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/identity_verification/e;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setClipToOutline(Z)V

    .line 75
    sget v0, Lco/uk/getmondo/c$a;->secondaryDocumentThumbnail:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/identity_verification/e;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setClipToOutline(Z)V

    .line 76
    sget v0, Lco/uk/getmondo/c$a;->videoThumbnail:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/identity_verification/e;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setClipToOutline(Z)V

    .line 78
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/e;->c:Lco/uk/getmondo/signup/identity_verification/g;

    if-nez v0, :cond_0

    const-string v1, "presenter"

    invoke-static {v1}, Lkotlin/d/b/l;->b(Ljava/lang/String;)V

    :cond_0
    check-cast p0, Lco/uk/getmondo/signup/identity_verification/g$a;

    invoke-virtual {v0, p0}, Lco/uk/getmondo/signup/identity_verification/g;->a(Lco/uk/getmondo/signup/identity_verification/g$a;)V

    .line 79
    return-void
.end method

.method public p()V
    .locals 1

    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/e;->n:Ljava/util/HashMap;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/e;->n:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    :cond_0
    return-void
.end method
