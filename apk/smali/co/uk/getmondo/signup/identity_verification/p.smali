.class public Lco/uk/getmondo/signup/identity_verification/p;
.super Ljava/lang/Object;
.source "IdentityVerificationModule.java"


# instance fields
.field private final a:Lco/uk/getmondo/signup/identity_verification/a/j;

.field private final b:Lco/uk/getmondo/api/model/signup/SignupSource;


# direct methods
.method public constructor <init>(Lco/uk/getmondo/signup/identity_verification/a/j;Lco/uk/getmondo/api/model/signup/SignupSource;)V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    iput-object p1, p0, Lco/uk/getmondo/signup/identity_verification/p;->a:Lco/uk/getmondo/signup/identity_verification/a/j;

    .line 25
    iput-object p2, p0, Lco/uk/getmondo/signup/identity_verification/p;->b:Lco/uk/getmondo/api/model/signup/SignupSource;

    .line 26
    return-void
.end method


# virtual methods
.method a(Lco/uk/getmondo/signup/identity_verification/a/h;Lco/uk/getmondo/api/IdentityVerificationApi;Lco/uk/getmondo/api/ae;Lco/uk/getmondo/common/i;Lco/uk/getmondo/api/model/signup/SignupSource;)Lco/uk/getmondo/signup/identity_verification/a/e;
    .locals 6

    .prologue
    .line 45
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/p;->a:Lco/uk/getmondo/signup/identity_verification/a/j;

    sget-object v1, Lco/uk/getmondo/signup/identity_verification/a/j;->a:Lco/uk/getmondo/signup/identity_verification/a/j;

    if-ne v0, v1, :cond_0

    .line 46
    new-instance v0, Lco/uk/getmondo/signup/identity_verification/a/k;

    invoke-direct {v0, p1, p2, p3, p4}, Lco/uk/getmondo/signup/identity_verification/a/k;-><init>(Lco/uk/getmondo/signup/identity_verification/a/h;Lco/uk/getmondo/api/IdentityVerificationApi;Lco/uk/getmondo/api/ae;Lco/uk/getmondo/common/i;)V

    .line 48
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lco/uk/getmondo/signup/identity_verification/a/l;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v5}, Lco/uk/getmondo/signup/identity_verification/a/l;-><init>(Lco/uk/getmondo/signup/identity_verification/a/h;Lco/uk/getmondo/api/IdentityVerificationApi;Lco/uk/getmondo/api/ae;Lco/uk/getmondo/common/i;Lco/uk/getmondo/api/model/signup/SignupSource;)V

    goto :goto_0
.end method

.method a()Lco/uk/getmondo/signup/identity_verification/a/j;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/p;->a:Lco/uk/getmondo/signup/identity_verification/a/j;

    return-object v0
.end method

.method b()Lco/uk/getmondo/api/model/signup/SignupSource;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/p;->b:Lco/uk/getmondo/api/model/signup/SignupSource;

    return-object v0
.end method
