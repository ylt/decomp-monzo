.class final Lco/uk/getmondo/signup/identity_verification/g$p;
.super Ljava/lang/Object;
.source "IdentityDocumentsPresenter.kt"

# interfaces
.implements Lio/reactivex/c/g;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lco/uk/getmondo/signup/identity_verification/g;->a(Lco/uk/getmondo/signup/identity_verification/g$a;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/g",
        "<",
        "Lkotlin/h",
        "<+",
        "Lco/uk/getmondo/d/i;",
        "+",
        "Lco/uk/getmondo/api/model/identity_verification/IdentityDocumentType;",
        ">;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000\u0018\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012&\u0010\u0002\u001a\"\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u0005 \u0006*\u0010\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u0005\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\u0007"
    }
    d2 = {
        "<anonymous>",
        "",
        "<name for destructuring parameter 0>",
        "Lkotlin/Pair;",
        "Lco/uk/getmondo/model/Country;",
        "Lco/uk/getmondo/api/model/identity_verification/IdentityDocumentType;",
        "kotlin.jvm.PlatformType",
        "accept"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field final synthetic a:Lco/uk/getmondo/signup/identity_verification/g;

.field final synthetic b:Lco/uk/getmondo/signup/identity_verification/g$a;


# direct methods
.method constructor <init>(Lco/uk/getmondo/signup/identity_verification/g;Lco/uk/getmondo/signup/identity_verification/g$a;)V
    .locals 0

    iput-object p1, p0, Lco/uk/getmondo/signup/identity_verification/g$p;->a:Lco/uk/getmondo/signup/identity_verification/g;

    iput-object p2, p0, Lco/uk/getmondo/signup/identity_verification/g$p;->b:Lco/uk/getmondo/signup/identity_verification/g$a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 26
    check-cast p1, Lkotlin/h;

    invoke-virtual {p0, p1}, Lco/uk/getmondo/signup/identity_verification/g$p;->a(Lkotlin/h;)V

    return-void
.end method

.method public final a(Lkotlin/h;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/h",
            "<",
            "Lco/uk/getmondo/d/i;",
            "+",
            "Lco/uk/getmondo/api/model/identity_verification/IdentityDocumentType;",
            ">;)V"
        }
    .end annotation

    .prologue
    invoke-virtual {p1}, Lkotlin/h;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/d/i;

    invoke-virtual {p1}, Lkotlin/h;->d()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lco/uk/getmondo/api/model/identity_verification/IdentityDocumentType;

    .line 81
    iget-object v2, p0, Lco/uk/getmondo/signup/identity_verification/g$p;->a:Lco/uk/getmondo/signup/identity_verification/g;

    invoke-static {v2}, Lco/uk/getmondo/signup/identity_verification/g;->a(Lco/uk/getmondo/signup/identity_verification/g;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 82
    iget-object v2, p0, Lco/uk/getmondo/signup/identity_verification/g$p;->b:Lco/uk/getmondo/signup/identity_verification/g$a;

    iget-object v3, p0, Lco/uk/getmondo/signup/identity_verification/g$p;->a:Lco/uk/getmondo/signup/identity_verification/g;

    invoke-static {v3}, Lco/uk/getmondo/signup/identity_verification/g;->b(Lco/uk/getmondo/signup/identity_verification/g;)Lco/uk/getmondo/signup/identity_verification/a/j;

    move-result-object v3

    invoke-interface {v2, v3, v1, v0}, Lco/uk/getmondo/signup/identity_verification/g$a;->b(Lco/uk/getmondo/signup/identity_verification/a/j;Lco/uk/getmondo/api/model/identity_verification/IdentityDocumentType;Lco/uk/getmondo/d/i;)V

    .line 85
    :goto_0
    return-void

    .line 84
    :cond_0
    iget-object v2, p0, Lco/uk/getmondo/signup/identity_verification/g$p;->b:Lco/uk/getmondo/signup/identity_verification/g$a;

    iget-object v3, p0, Lco/uk/getmondo/signup/identity_verification/g$p;->a:Lco/uk/getmondo/signup/identity_verification/g;

    invoke-static {v3}, Lco/uk/getmondo/signup/identity_verification/g;->b(Lco/uk/getmondo/signup/identity_verification/g;)Lco/uk/getmondo/signup/identity_verification/a/j;

    move-result-object v3

    invoke-interface {v2, v3, v1, v0}, Lco/uk/getmondo/signup/identity_verification/g$a;->a(Lco/uk/getmondo/signup/identity_verification/a/j;Lco/uk/getmondo/api/model/identity_verification/IdentityDocumentType;Lco/uk/getmondo/d/i;)V

    goto :goto_0
.end method
