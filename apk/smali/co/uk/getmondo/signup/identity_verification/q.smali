.class public final Lco/uk/getmondo/signup/identity_verification/q;
.super Ljava/lang/Object;
.source "IdentityVerificationModule_ProvideIdentityVerificationManagerFactory.java"

# interfaces
.implements Lb/a/b;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lb/a/b",
        "<",
        "Lco/uk/getmondo/signup/identity_verification/a/e;",
        ">;"
    }
.end annotation


# static fields
.field static final synthetic a:Z


# instance fields
.field private final b:Lco/uk/getmondo/signup/identity_verification/p;

.field private final c:Ljavax/a/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/signup/identity_verification/a/h;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Ljavax/a/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/api/IdentityVerificationApi;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Ljavax/a/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/api/ae;",
            ">;"
        }
    .end annotation
.end field

.field private final f:Ljavax/a/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/common/i;",
            ">;"
        }
    .end annotation
.end field

.field private final g:Ljavax/a/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/api/model/signup/SignupSource;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 14
    const-class v0, Lco/uk/getmondo/signup/identity_verification/q;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lco/uk/getmondo/signup/identity_verification/q;->a:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lco/uk/getmondo/signup/identity_verification/p;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lco/uk/getmondo/signup/identity_verification/p;",
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/signup/identity_verification/a/h;",
            ">;",
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/api/IdentityVerificationApi;",
            ">;",
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/api/ae;",
            ">;",
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/common/i;",
            ">;",
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/api/model/signup/SignupSource;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    sget-boolean v0, Lco/uk/getmondo/signup/identity_verification/q;->a:Z

    if-nez v0, :cond_0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 40
    :cond_0
    iput-object p1, p0, Lco/uk/getmondo/signup/identity_verification/q;->b:Lco/uk/getmondo/signup/identity_verification/p;

    .line 41
    sget-boolean v0, Lco/uk/getmondo/signup/identity_verification/q;->a:Z

    if-nez v0, :cond_1

    if-nez p2, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 42
    :cond_1
    iput-object p2, p0, Lco/uk/getmondo/signup/identity_verification/q;->c:Ljavax/a/a;

    .line 43
    sget-boolean v0, Lco/uk/getmondo/signup/identity_verification/q;->a:Z

    if-nez v0, :cond_2

    if-nez p3, :cond_2

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 44
    :cond_2
    iput-object p3, p0, Lco/uk/getmondo/signup/identity_verification/q;->d:Ljavax/a/a;

    .line 45
    sget-boolean v0, Lco/uk/getmondo/signup/identity_verification/q;->a:Z

    if-nez v0, :cond_3

    if-nez p4, :cond_3

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 46
    :cond_3
    iput-object p4, p0, Lco/uk/getmondo/signup/identity_verification/q;->e:Ljavax/a/a;

    .line 47
    sget-boolean v0, Lco/uk/getmondo/signup/identity_verification/q;->a:Z

    if-nez v0, :cond_4

    if-nez p5, :cond_4

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 48
    :cond_4
    iput-object p5, p0, Lco/uk/getmondo/signup/identity_verification/q;->f:Ljavax/a/a;

    .line 49
    sget-boolean v0, Lco/uk/getmondo/signup/identity_verification/q;->a:Z

    if-nez v0, :cond_5

    if-nez p6, :cond_5

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 50
    :cond_5
    iput-object p6, p0, Lco/uk/getmondo/signup/identity_verification/q;->g:Ljavax/a/a;

    .line 51
    return-void
.end method

.method public static a(Lco/uk/getmondo/signup/identity_verification/p;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;)Lb/a/b;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lco/uk/getmondo/signup/identity_verification/p;",
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/signup/identity_verification/a/h;",
            ">;",
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/api/IdentityVerificationApi;",
            ">;",
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/api/ae;",
            ">;",
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/common/i;",
            ">;",
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/api/model/signup/SignupSource;",
            ">;)",
            "Lb/a/b",
            "<",
            "Lco/uk/getmondo/signup/identity_verification/a/e;",
            ">;"
        }
    .end annotation

    .prologue
    .line 72
    new-instance v0, Lco/uk/getmondo/signup/identity_verification/q;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lco/uk/getmondo/signup/identity_verification/q;-><init>(Lco/uk/getmondo/signup/identity_verification/p;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;)V

    return-object v0
.end method


# virtual methods
.method public a()Lco/uk/getmondo/signup/identity_verification/a/e;
    .locals 6

    .prologue
    .line 55
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/q;->b:Lco/uk/getmondo/signup/identity_verification/p;

    iget-object v1, p0, Lco/uk/getmondo/signup/identity_verification/q;->c:Ljavax/a/a;

    .line 57
    invoke-interface {v1}, Ljavax/a/a;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lco/uk/getmondo/signup/identity_verification/a/h;

    iget-object v2, p0, Lco/uk/getmondo/signup/identity_verification/q;->d:Ljavax/a/a;

    .line 58
    invoke-interface {v2}, Ljavax/a/a;->b()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lco/uk/getmondo/api/IdentityVerificationApi;

    iget-object v3, p0, Lco/uk/getmondo/signup/identity_verification/q;->e:Ljavax/a/a;

    .line 59
    invoke-interface {v3}, Ljavax/a/a;->b()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lco/uk/getmondo/api/ae;

    iget-object v4, p0, Lco/uk/getmondo/signup/identity_verification/q;->f:Ljavax/a/a;

    .line 60
    invoke-interface {v4}, Ljavax/a/a;->b()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lco/uk/getmondo/common/i;

    iget-object v5, p0, Lco/uk/getmondo/signup/identity_verification/q;->g:Ljavax/a/a;

    .line 61
    invoke-interface {v5}, Ljavax/a/a;->b()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lco/uk/getmondo/api/model/signup/SignupSource;

    .line 56
    invoke-virtual/range {v0 .. v5}, Lco/uk/getmondo/signup/identity_verification/p;->a(Lco/uk/getmondo/signup/identity_verification/a/h;Lco/uk/getmondo/api/IdentityVerificationApi;Lco/uk/getmondo/api/ae;Lco/uk/getmondo/common/i;Lco/uk/getmondo/api/model/signup/SignupSource;)Lco/uk/getmondo/signup/identity_verification/a/e;

    move-result-object v0

    const-string v1, "Cannot return null from a non-@Nullable @Provides method"

    .line 55
    invoke-static {v0, v1}, Lb/a/d;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/signup/identity_verification/a/e;

    return-object v0
.end method

.method public synthetic b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 14
    invoke-virtual {p0}, Lco/uk/getmondo/signup/identity_verification/q;->a()Lco/uk/getmondo/signup/identity_verification/a/e;

    move-result-object v0

    return-object v0
.end method
