.class public interface abstract Lco/uk/getmondo/signup/identity_verification/g$a;
.super Ljava/lang/Object;
.source "IdentityDocumentsPresenter.kt"

# interfaces
.implements Lco/uk/getmondo/common/e/a$a;
.implements Lco/uk/getmondo/common/ui/f;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lco/uk/getmondo/signup/identity_verification/g;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "a"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000b\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u0000\n\u0002\u0008\u0005\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0007\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0007\n\u0002\u0010\u000e\n\u0002\u0008\u0004\u0008f\u0018\u00002\u00020\u00012\u00020\u0002J\u0008\u0010\u0003\u001a\u00020\u0004H&J\u0008\u0010\u0005\u001a\u00020\u0004H&J\u0008\u0010\u0006\u001a\u00020\u0004H&J\u001a\u0010\u0007\u001a\u0014\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00020\n\u0012\u0004\u0012\u00020\u000b0\t0\u0008H&J\u000e\u0010\u000c\u001a\u0008\u0012\u0004\u0012\u00020\u000b0\u0008H&J\u000e\u0010\r\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0008H&J\u000e\u0010\u000e\u001a\u0008\u0012\u0004\u0012\u00020\u000f0\u0008H&J\u000e\u0010\u0010\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0008H&J\u000e\u0010\u0011\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0008H&J\u0018\u0010\u0012\u001a\u00020\u00042\u0006\u0010\u0013\u001a\u00020\u000b2\u0006\u0010\u0014\u001a\u00020\u0015H&J \u0010\u0016\u001a\u00020\u00042\u0006\u0010\u0017\u001a\u00020\u00182\u0006\u0010\u0019\u001a\u00020\u000b2\u0006\u0010\u001a\u001a\u00020\nH&J \u0010\u001b\u001a\u00020\u00042\u0006\u0010\u0017\u001a\u00020\u00182\u0006\u0010\u0013\u001a\u00020\u000b2\u0006\u0010\u001a\u001a\u00020\nH&J\u0010\u0010\u001c\u001a\u00020\u00042\u0006\u0010\u0017\u001a\u00020\u0018H&J\u0018\u0010\u001d\u001a\u00020\u00042\u0006\u0010\u001e\u001a\u00020\u00182\u0006\u0010\u001f\u001a\u00020 H&J\u0008\u0010!\u001a\u00020\u0004H&J\u000e\u0010\"\u001a\u0008\u0012\u0004\u0012\u00020\u00150#H&J\u0010\u0010$\u001a\u00020\u00042\u0006\u0010%\u001a\u00020&H&J\u0008\u0010\'\u001a\u00020\u0004H&J\u0016\u0010(\u001a\u0008\u0012\u0004\u0012\u00020\u00150#2\u0006\u0010\u0013\u001a\u00020\u000bH&J\u000e\u0010)\u001a\u0008\u0012\u0004\u0012\u00020\u000b0#H&J\u0008\u0010*\u001a\u00020\u0004H&J\u000e\u0010+\u001a\u0008\u0012\u0004\u0012\u00020\u000b0#H&J\u0010\u0010,\u001a\u00020\u00042\u0006\u0010-\u001a\u00020.H&J\u0010\u0010/\u001a\u00020\u00042\u0006\u00100\u001a\u00020.H&J\u0008\u00101\u001a\u00020\u0004H&\u00a8\u00062"
    }
    d2 = {
        "Lco/uk/getmondo/signup/identity_verification/IdentityDocumentsPresenter$View;",
        "Lco/uk/getmondo/common/ui/MvpView;",
        "Lco/uk/getmondo/common/errors/ApiErrorHandler$ApiErrorView;",
        "disableSubmitVerificationButton",
        "",
        "enableSubmitVerificationButton",
        "hideLoading",
        "onDocumentCountrySelected",
        "Lio/reactivex/Observable;",
        "Lkotlin/Pair;",
        "Lco/uk/getmondo/model/Country;",
        "Lco/uk/getmondo/api/model/identity_verification/IdentityDocumentType;",
        "onPhotoIdTypeChosen",
        "onSubmitVerificationClicked",
        "onSubmitVerificationConfirmationClicked",
        "",
        "onTakePhotoClicked",
        "onTakeVideoClicked",
        "openCountrySelection",
        "documentType",
        "useSystemCamera",
        "",
        "openDocumentCamera",
        "version",
        "Lco/uk/getmondo/signup/identity_verification/data/IdentityVerificationVersion;",
        "idDocumentType",
        "country",
        "openDocumentCameraFallback",
        "openTakeVideo",
        "openVerificationPending",
        "identityVerificationVersion",
        "from",
        "Lco/uk/getmondo/api/model/tracking/Impression$KycFrom;",
        "openVideoFallback",
        "showDoYouHavePassport",
        "Lio/reactivex/Maybe;",
        "showIdentityDocComplete",
        "identityDocument",
        "Lco/uk/getmondo/signup/identity_verification/data/model/IdentityDocument;",
        "showIdentityDocIncomplete",
        "showIsDocumentUk",
        "showLegacyIdentityDocumentSelection",
        "showLoading",
        "showOtherIdentityDocumentsSelection",
        "showRejectedReason",
        "message",
        "",
        "showVideoComplete",
        "videoPath",
        "showVideoIncomplete",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# virtual methods
.method public abstract a(Lco/uk/getmondo/api/model/identity_verification/IdentityDocumentType;)Lio/reactivex/h;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lco/uk/getmondo/api/model/identity_verification/IdentityDocumentType;",
            ")",
            "Lio/reactivex/h",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end method

.method public abstract a()Lio/reactivex/n;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Lkotlin/n;",
            ">;"
        }
    .end annotation
.end method

.method public abstract a(Lco/uk/getmondo/api/model/identity_verification/IdentityDocumentType;Z)V
.end method

.method public abstract a(Lco/uk/getmondo/signup/identity_verification/a/a/b;)V
.end method

.method public abstract a(Lco/uk/getmondo/signup/identity_verification/a/j;)V
.end method

.method public abstract a(Lco/uk/getmondo/signup/identity_verification/a/j;Lco/uk/getmondo/api/model/identity_verification/IdentityDocumentType;Lco/uk/getmondo/d/i;)V
.end method

.method public abstract a(Lco/uk/getmondo/signup/identity_verification/a/j;Lco/uk/getmondo/api/model/tracking/Impression$KycFrom;)V
.end method

.method public abstract a(Ljava/lang/String;)V
.end method

.method public abstract b()Lio/reactivex/n;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Lkotlin/n;",
            ">;"
        }
    .end annotation
.end method

.method public abstract b(Lco/uk/getmondo/signup/identity_verification/a/j;Lco/uk/getmondo/api/model/identity_verification/IdentityDocumentType;Lco/uk/getmondo/d/i;)V
.end method

.method public abstract b(Ljava/lang/String;)V
.end method

.method public abstract c()Lio/reactivex/n;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Lkotlin/n;",
            ">;"
        }
    .end annotation
.end method

.method public abstract d()Lio/reactivex/n;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end method

.method public abstract e()Lio/reactivex/n;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Lkotlin/h",
            "<",
            "Lco/uk/getmondo/d/i;",
            "Lco/uk/getmondo/api/model/identity_verification/IdentityDocumentType;",
            ">;>;"
        }
    .end annotation
.end method

.method public abstract f()Lio/reactivex/h;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/h",
            "<",
            "Lco/uk/getmondo/api/model/identity_verification/IdentityDocumentType;",
            ">;"
        }
    .end annotation
.end method

.method public abstract g()Lio/reactivex/h;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/h",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end method

.method public abstract h()Lio/reactivex/h;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/h",
            "<",
            "Lco/uk/getmondo/api/model/identity_verification/IdentityDocumentType;",
            ">;"
        }
    .end annotation
.end method

.method public abstract i()V
.end method

.method public abstract j()V
.end method

.method public abstract k()V
.end method

.method public abstract l()V
.end method

.method public abstract m()V
.end method

.method public abstract n()V
.end method

.method public abstract o()V
.end method
