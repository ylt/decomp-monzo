.class final Lco/uk/getmondo/signup/identity_verification/g$e;
.super Ljava/lang/Object;
.source "IdentityDocumentsPresenter.kt"

# interfaces
.implements Lio/reactivex/c/g;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lco/uk/getmondo/signup/identity_verification/g;->a(Lco/uk/getmondo/signup/identity_verification/g$a;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/g",
        "<",
        "Lkotlin/h",
        "<+",
        "Lcom/c/b/b",
        "<",
        "Lco/uk/getmondo/signup/identity_verification/a/a/b;",
        ">;+",
        "Lcom/c/b/b",
        "<",
        "Ljava/lang/String;",
        ">;>;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000\u001c\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012>\u0010\u0002\u001a:\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00050\u0004\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00060\u0004 \u0007*\u001c\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00050\u0004\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00060\u0004\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\u0008"
    }
    d2 = {
        "<anonymous>",
        "",
        "<name for destructuring parameter 0>",
        "Lkotlin/Pair;",
        "Lcom/memoizrlabs/poweroptional/Optional;",
        "Lco/uk/getmondo/signup/identity_verification/data/model/IdentityDocument;",
        "",
        "kotlin.jvm.PlatformType",
        "accept"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field final synthetic a:Lco/uk/getmondo/signup/identity_verification/g;

.field final synthetic b:Lco/uk/getmondo/signup/identity_verification/g$a;

.field final synthetic c:Z


# direct methods
.method constructor <init>(Lco/uk/getmondo/signup/identity_verification/g;Lco/uk/getmondo/signup/identity_verification/g$a;Z)V
    .locals 0

    iput-object p1, p0, Lco/uk/getmondo/signup/identity_verification/g$e;->a:Lco/uk/getmondo/signup/identity_verification/g;

    iput-object p2, p0, Lco/uk/getmondo/signup/identity_verification/g$e;->b:Lco/uk/getmondo/signup/identity_verification/g$a;

    iput-boolean p3, p0, Lco/uk/getmondo/signup/identity_verification/g$e;->c:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 26
    check-cast p1, Lkotlin/h;

    invoke-virtual {p0, p1}, Lco/uk/getmondo/signup/identity_verification/g$e;->a(Lkotlin/h;)V

    return-void
.end method

.method public final a(Lkotlin/h;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/h",
            "<+",
            "Lcom/c/b/b",
            "<",
            "Lco/uk/getmondo/signup/identity_verification/a/a/b;",
            ">;+",
            "Lcom/c/b/b",
            "<",
            "Ljava/lang/String;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    invoke-virtual {p1}, Lkotlin/h;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/c/b/b;

    invoke-virtual {p1}, Lkotlin/h;->d()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/c/b/b;

    .line 107
    invoke-virtual {v0}, Lcom/c/b/b;->b()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {v0}, Lcom/c/b/b;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lco/uk/getmondo/signup/identity_verification/a/a/b;

    iget-object v3, p0, Lco/uk/getmondo/signup/identity_verification/g$e;->a:Lco/uk/getmondo/signup/identity_verification/g;

    invoke-static {v3}, Lco/uk/getmondo/signup/identity_verification/g;->c(Lco/uk/getmondo/signup/identity_verification/g;)Lco/uk/getmondo/signup/identity_verification/a/c;

    move-result-object v3

    iget-object v6, p0, Lco/uk/getmondo/signup/identity_verification/g$e;->a:Lco/uk/getmondo/signup/identity_verification/g;

    invoke-static {v6}, Lco/uk/getmondo/signup/identity_verification/g;->b(Lco/uk/getmondo/signup/identity_verification/g;)Lco/uk/getmondo/signup/identity_verification/a/j;

    move-result-object v6

    invoke-virtual {v2, v3, v6}, Lco/uk/getmondo/signup/identity_verification/a/a/b;->a(Lco/uk/getmondo/signup/identity_verification/a/c;Lco/uk/getmondo/signup/identity_verification/a/j;)Z

    move-result v2

    if-eqz v2, :cond_2

    move v3, v4

    .line 108
    :goto_0
    invoke-virtual {v1}, Lcom/c/b/b;->b()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lco/uk/getmondo/signup/identity_verification/g$e;->a:Lco/uk/getmondo/signup/identity_verification/g;

    invoke-static {v2}, Lco/uk/getmondo/signup/identity_verification/g;->c(Lco/uk/getmondo/signup/identity_verification/g;)Lco/uk/getmondo/signup/identity_verification/a/c;

    move-result-object v6

    invoke-virtual {v1}, Lcom/c/b/b;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-virtual {v6, v2}, Lco/uk/getmondo/signup/identity_verification/a/c;->a(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    move v5, v4

    .line 109
    :cond_0
    if-eqz v3, :cond_3

    if-eqz v5, :cond_3

    .line 110
    iget-object v2, p0, Lco/uk/getmondo/signup/identity_verification/g$e;->b:Lco/uk/getmondo/signup/identity_verification/g$a;

    invoke-interface {v2}, Lco/uk/getmondo/signup/identity_verification/g$a;->l()V

    .line 113
    :goto_1
    if-eqz v3, :cond_4

    .line 116
    iget-object v2, p0, Lco/uk/getmondo/signup/identity_verification/g$e;->b:Lco/uk/getmondo/signup/identity_verification/g$a;

    invoke-virtual {v0}, Lcom/c/b/b;->a()Ljava/lang/Object;

    move-result-object v0

    const-string v6, "identityDocument.get()"

    invoke-static {v0, v6}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lco/uk/getmondo/signup/identity_verification/a/a/b;

    invoke-interface {v2, v0}, Lco/uk/getmondo/signup/identity_verification/g$a;->a(Lco/uk/getmondo/signup/identity_verification/a/a/b;)V

    .line 119
    :goto_2
    if-eqz v5, :cond_5

    .line 121
    iget-object v2, p0, Lco/uk/getmondo/signup/identity_verification/g$e;->b:Lco/uk/getmondo/signup/identity_verification/g$a;

    invoke-virtual {v1}, Lcom/c/b/b;->a()Ljava/lang/Object;

    move-result-object v0

    const-string v1, "videoPath.get()"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Ljava/lang/String;

    invoke-interface {v2, v0}, Lco/uk/getmondo/signup/identity_verification/g$a;->b(Ljava/lang/String;)V

    .line 124
    :goto_3
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/g$e;->a:Lco/uk/getmondo/signup/identity_verification/g;

    invoke-static {v0}, Lco/uk/getmondo/signup/identity_verification/g;->d(Lco/uk/getmondo/signup/identity_verification/g;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 127
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/g$e;->a:Lco/uk/getmondo/signup/identity_verification/g;

    invoke-static {v0}, Lco/uk/getmondo/signup/identity_verification/g;->e(Lco/uk/getmondo/signup/identity_verification/g;)Lco/uk/getmondo/common/a;

    move-result-object v0

    sget-object v1, Lco/uk/getmondo/api/model/tracking/Impression;->Companion:Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    iget-boolean v2, p0, Lco/uk/getmondo/signup/identity_verification/g$e;->c:Z

    invoke-virtual {v1, v3, v5, v2}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->a(ZZZ)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v1

    invoke-virtual {v0, v1}, Lco/uk/getmondo/common/a;->a(Lco/uk/getmondo/api/model/tracking/Impression;)V

    .line 128
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/g$e;->a:Lco/uk/getmondo/signup/identity_verification/g;

    invoke-static {v0, v4}, Lco/uk/getmondo/signup/identity_verification/g;->a(Lco/uk/getmondo/signup/identity_verification/g;Z)V

    .line 130
    :cond_1
    return-void

    :cond_2
    move v3, v5

    .line 107
    goto :goto_0

    .line 112
    :cond_3
    iget-object v2, p0, Lco/uk/getmondo/signup/identity_verification/g$e;->b:Lco/uk/getmondo/signup/identity_verification/g$a;

    invoke-interface {v2}, Lco/uk/getmondo/signup/identity_verification/g$a;->m()V

    goto :goto_1

    .line 118
    :cond_4
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/g$e;->b:Lco/uk/getmondo/signup/identity_verification/g$a;

    invoke-interface {v0}, Lco/uk/getmondo/signup/identity_verification/g$a;->n()V

    goto :goto_2

    .line 123
    :cond_5
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/g$e;->b:Lco/uk/getmondo/signup/identity_verification/g$a;

    invoke-interface {v0}, Lco/uk/getmondo/signup/identity_verification/g$a;->o()V

    goto :goto_3
.end method
