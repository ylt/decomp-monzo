.class final Lco/uk/getmondo/signup/identity_verification/v$d;
.super Ljava/lang/Object;
.source "IdentityVerificationPresenter.kt"

# interfaces
.implements Lio/reactivex/c/g;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lco/uk/getmondo/signup/identity_verification/v;->a(Lco/uk/getmondo/signup/identity_verification/v$a;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/g",
        "<",
        "Lco/uk/getmondo/api/model/identity_verification/IdentityVerification;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "",
        "it",
        "Lco/uk/getmondo/api/model/identity_verification/IdentityVerification;",
        "kotlin.jvm.PlatformType",
        "accept"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field final synthetic a:Lco/uk/getmondo/signup/identity_verification/v;

.field final synthetic b:Lco/uk/getmondo/signup/identity_verification/v$a;


# direct methods
.method constructor <init>(Lco/uk/getmondo/signup/identity_verification/v;Lco/uk/getmondo/signup/identity_verification/v$a;)V
    .locals 0

    iput-object p1, p0, Lco/uk/getmondo/signup/identity_verification/v$d;->a:Lco/uk/getmondo/signup/identity_verification/v;

    iput-object p2, p0, Lco/uk/getmondo/signup/identity_verification/v$d;->b:Lco/uk/getmondo/signup/identity_verification/v$a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lco/uk/getmondo/api/model/identity_verification/IdentityVerification;)V
    .locals 3

    .prologue
    .line 59
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/v$d;->b:Lco/uk/getmondo/signup/identity_verification/v$a;

    invoke-interface {v0}, Lco/uk/getmondo/signup/identity_verification/v$a;->g()V

    .line 60
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/v$d;->a:Lco/uk/getmondo/signup/identity_verification/v;

    invoke-virtual {p1}, Lco/uk/getmondo/api/model/identity_verification/IdentityVerification;->c()Z

    move-result v1

    invoke-static {v0, v1}, Lco/uk/getmondo/signup/identity_verification/v;->a(Lco/uk/getmondo/signup/identity_verification/v;Z)V

    .line 62
    invoke-virtual {p1}, Lco/uk/getmondo/api/model/identity_verification/IdentityVerification;->b()Lco/uk/getmondo/api/model/identity_verification/IdentityVerification$Status;

    move-result-object v0

    if-nez v0, :cond_0

    .line 66
    :goto_0
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/v$d;->b:Lco/uk/getmondo/signup/identity_verification/v$a;

    invoke-interface {v0}, Lco/uk/getmondo/signup/identity_verification/v$a;->j()V

    .line 67
    :goto_1
    return-void

    .line 62
    :cond_0
    sget-object v1, Lco/uk/getmondo/signup/identity_verification/w;->a:[I

    invoke-virtual {v0}, Lco/uk/getmondo/api/model/identity_verification/IdentityVerification$Status;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 63
    :pswitch_0
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/v$d;->b:Lco/uk/getmondo/signup/identity_verification/v$a;

    invoke-interface {v0}, Lco/uk/getmondo/signup/identity_verification/v$a;->h()V

    goto :goto_1

    .line 64
    :pswitch_1
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/v$d;->b:Lco/uk/getmondo/signup/identity_verification/v$a;

    invoke-virtual {p1}, Lco/uk/getmondo/api/model/identity_verification/IdentityVerification;->c()Z

    move-result v1

    invoke-virtual {p1}, Lco/uk/getmondo/api/model/identity_verification/IdentityVerification;->a()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lco/uk/getmondo/signup/identity_verification/v$a;->a(ZLjava/lang/String;)V

    goto :goto_1

    .line 65
    :pswitch_2
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/v$d;->b:Lco/uk/getmondo/signup/identity_verification/v$a;

    invoke-interface {v0}, Lco/uk/getmondo/signup/identity_verification/v$a;->i()V

    goto :goto_1

    .line 62
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 22
    check-cast p1, Lco/uk/getmondo/api/model/identity_verification/IdentityVerification;

    invoke-virtual {p0, p1}, Lco/uk/getmondo/signup/identity_verification/v$d;->a(Lco/uk/getmondo/api/model/identity_verification/IdentityVerification;)V

    return-void
.end method
