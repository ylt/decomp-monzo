.class public final Lco/uk/getmondo/signup/identity_verification/VerificationPendingActivity;
.super Lco/uk/getmondo/common/activities/b;
.source "VerificationPendingActivity.kt"

# interfaces
.implements Lco/uk/getmondo/signup/identity_verification/aa$a;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lco/uk/getmondo/signup/identity_verification/VerificationPendingActivity$a;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000R\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0007\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0010\u000e\n\u0002\u0008\u0006\u0018\u0000 -2\u00020\u00012\u00020\u0002:\u0001-B\u0005\u00a2\u0006\u0002\u0010\u0003J\u0008\u0010\u001c\u001a\u00020\u0011H\u0016J\u0012\u0010\u001d\u001a\u00020\u00112\u0008\u0010\u001e\u001a\u0004\u0018\u00010\u001fH\u0014J\u0008\u0010 \u001a\u00020\u0011H\u0014J\u000e\u0010!\u001a\u0008\u0012\u0004\u0012\u00020\u00110\"H\u0016J\u0008\u0010#\u001a\u00020\u0011H\u0014J\u0008\u0010$\u001a\u00020\u0011H\u0016J\u0008\u0010%\u001a\u00020\u0011H\u0016J\u0012\u0010&\u001a\u00020\u00112\u0008\u0010\'\u001a\u0004\u0018\u00010(H\u0016J\u0008\u0010)\u001a\u00020\u0011H\u0016J\u0008\u0010*\u001a\u00020\u0011H\u0016J\u0008\u0010+\u001a\u00020\u0011H\u0016J\u0008\u0010,\u001a\u00020\u0011H\u0016R\u001b\u0010\u0004\u001a\u00020\u00058BX\u0082\u0084\u0002\u00a2\u0006\u000c\n\u0004\u0008\u0008\u0010\t\u001a\u0004\u0008\u0006\u0010\u0007R\u001b\u0010\n\u001a\u00020\u000b8BX\u0082\u0084\u0002\u00a2\u0006\u000c\n\u0004\u0008\u000e\u0010\t\u001a\u0004\u0008\u000c\u0010\rR\u0014\u0010\u000f\u001a\u0008\u0012\u0004\u0012\u00020\u00110\u0010X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0012\u001a\u00020\u00138BX\u0082\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0014\u0010\u0015R\u001e\u0010\u0016\u001a\u00020\u00178\u0006@\u0006X\u0087.\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u0018\u0010\u0019\"\u0004\u0008\u001a\u0010\u001b\u00a8\u0006."
    }
    d2 = {
        "Lco/uk/getmondo/signup/identity_verification/VerificationPendingActivity;",
        "Lco/uk/getmondo/common/activities/BaseActivity;",
        "Lco/uk/getmondo/signup/identity_verification/VerificationPendingPresenter$View;",
        "()V",
        "from",
        "Lco/uk/getmondo/api/model/tracking/Impression$KycFrom;",
        "getFrom",
        "()Lco/uk/getmondo/api/model/tracking/Impression$KycFrom;",
        "from$delegate",
        "Lkotlin/Lazy;",
        "identityVerificationVersion",
        "Lco/uk/getmondo/signup/identity_verification/data/IdentityVerificationVersion;",
        "getIdentityVerificationVersion",
        "()Lco/uk/getmondo/signup/identity_verification/data/IdentityVerificationVersion;",
        "identityVerificationVersion$delegate",
        "resumeRelay",
        "Lcom/jakewharton/rxrelay2/PublishRelay;",
        "",
        "signUpSource",
        "Lco/uk/getmondo/api/model/signup/SignupSource;",
        "getSignUpSource",
        "()Lco/uk/getmondo/api/model/signup/SignupSource;",
        "verificationPendingPresenter",
        "Lco/uk/getmondo/signup/identity_verification/VerificationPendingPresenter;",
        "getVerificationPendingPresenter",
        "()Lco/uk/getmondo/signup/identity_verification/VerificationPendingPresenter;",
        "setVerificationPendingPresenter",
        "(Lco/uk/getmondo/signup/identity_verification/VerificationPendingPresenter;)V",
        "hideLoading",
        "onCreate",
        "savedInstanceState",
        "Landroid/os/Bundle;",
        "onDestroy",
        "onRefresh",
        "Lio/reactivex/Observable;",
        "onResume",
        "openChat",
        "openHome",
        "openIdentityVerification",
        "rejectionNote",
        "",
        "openIdentityVerificationOnboarding",
        "openTopupInfo",
        "showLoading",
        "showVerifyingMessage",
        "Companion",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# static fields
.field static final synthetic a:[Lkotlin/reflect/l;

.field public static final c:Lco/uk/getmondo/signup/identity_verification/VerificationPendingActivity$a;


# instance fields
.field public b:Lco/uk/getmondo/signup/identity_verification/aa;

.field private final e:Lcom/b/b/c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/b/b/c",
            "<",
            "Lkotlin/n;",
            ">;"
        }
    .end annotation
.end field

.field private final f:Lkotlin/c;

.field private final g:Lkotlin/c;

.field private h:Ljava/util/HashMap;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const/4 v0, 0x2

    new-array v1, v0, [Lkotlin/reflect/l;

    const/4 v2, 0x0

    new-instance v0, Lkotlin/d/b/w;

    const-class v3, Lco/uk/getmondo/signup/identity_verification/VerificationPendingActivity;

    invoke-static {v3}, Lkotlin/d/b/y;->a(Ljava/lang/Class;)Lkotlin/reflect/c;

    move-result-object v3

    const-string v4, "identityVerificationVersion"

    const-string v5, "getIdentityVerificationVersion()Lco/uk/getmondo/signup/identity_verification/data/IdentityVerificationVersion;"

    invoke-direct {v0, v3, v4, v5}, Lkotlin/d/b/w;-><init>(Lkotlin/reflect/e;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v0}, Lkotlin/d/b/y;->a(Lkotlin/d/b/v;)Lkotlin/reflect/n;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/l;

    aput-object v0, v1, v2

    const/4 v2, 0x1

    new-instance v0, Lkotlin/d/b/w;

    const-class v3, Lco/uk/getmondo/signup/identity_verification/VerificationPendingActivity;

    invoke-static {v3}, Lkotlin/d/b/y;->a(Ljava/lang/Class;)Lkotlin/reflect/c;

    move-result-object v3

    const-string v4, "from"

    const-string v5, "getFrom()Lco/uk/getmondo/api/model/tracking/Impression$KycFrom;"

    invoke-direct {v0, v3, v4, v5}, Lkotlin/d/b/w;-><init>(Lkotlin/reflect/e;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v0}, Lkotlin/d/b/y;->a(Lkotlin/d/b/v;)Lkotlin/reflect/n;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/l;

    aput-object v0, v1, v2

    sput-object v1, Lco/uk/getmondo/signup/identity_verification/VerificationPendingActivity;->a:[Lkotlin/reflect/l;

    new-instance v0, Lco/uk/getmondo/signup/identity_verification/VerificationPendingActivity$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lco/uk/getmondo/signup/identity_verification/VerificationPendingActivity$a;-><init>(Lkotlin/d/b/i;)V

    sput-object v0, Lco/uk/getmondo/signup/identity_verification/VerificationPendingActivity;->c:Lco/uk/getmondo/signup/identity_verification/VerificationPendingActivity$a;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 21
    invoke-direct {p0}, Lco/uk/getmondo/common/activities/b;-><init>()V

    .line 22
    invoke-static {}, Lcom/b/b/c;->a()Lcom/b/b/c;

    move-result-object v0

    const-string v1, "PublishRelay.create()"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v0, p0, Lco/uk/getmondo/signup/identity_verification/VerificationPendingActivity;->e:Lcom/b/b/c;

    .line 25
    new-instance v0, Lco/uk/getmondo/signup/identity_verification/VerificationPendingActivity$c;

    invoke-direct {v0, p0}, Lco/uk/getmondo/signup/identity_verification/VerificationPendingActivity$c;-><init>(Lco/uk/getmondo/signup/identity_verification/VerificationPendingActivity;)V

    check-cast v0, Lkotlin/d/a/a;

    invoke-static {v0}, Lkotlin/d;->a(Lkotlin/d/a/a;)Lkotlin/c;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/signup/identity_verification/VerificationPendingActivity;->f:Lkotlin/c;

    .line 28
    new-instance v0, Lco/uk/getmondo/signup/identity_verification/VerificationPendingActivity$b;

    invoke-direct {v0, p0}, Lco/uk/getmondo/signup/identity_verification/VerificationPendingActivity$b;-><init>(Lco/uk/getmondo/signup/identity_verification/VerificationPendingActivity;)V

    check-cast v0, Lkotlin/d/a/a;

    invoke-static {v0}, Lkotlin/d;->a(Lkotlin/d/a/a;)Lkotlin/c;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/signup/identity_verification/VerificationPendingActivity;->g:Lkotlin/c;

    return-void
.end method

.method public static final a(Landroid/content/Context;Lco/uk/getmondo/signup/identity_verification/a/j;Lco/uk/getmondo/api/model/tracking/Impression$KycFrom;)Landroid/content/Intent;
    .locals 1

    const-string v0, "context"

    invoke-static {p0, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "identityVerificationVersion"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "from"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v0, Lco/uk/getmondo/signup/identity_verification/VerificationPendingActivity;->c:Lco/uk/getmondo/signup/identity_verification/VerificationPendingActivity$a;

    invoke-virtual {v0, p0, p1, p2}, Lco/uk/getmondo/signup/identity_verification/VerificationPendingActivity$a;->a(Landroid/content/Context;Lco/uk/getmondo/signup/identity_verification/a/j;Lco/uk/getmondo/api/model/tracking/Impression$KycFrom;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method private final i()Lco/uk/getmondo/signup/identity_verification/a/j;
    .locals 3

    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/VerificationPendingActivity;->f:Lkotlin/c;

    sget-object v1, Lco/uk/getmondo/signup/identity_verification/VerificationPendingActivity;->a:[Lkotlin/reflect/l;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-interface {v0}, Lkotlin/c;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/signup/identity_verification/a/j;

    return-object v0
.end method

.method private final j()Lco/uk/getmondo/api/model/tracking/Impression$KycFrom;
    .locals 3

    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/VerificationPendingActivity;->g:Lkotlin/c;

    sget-object v1, Lco/uk/getmondo/signup/identity_verification/VerificationPendingActivity;->a:[Lkotlin/reflect/l;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-interface {v0}, Lkotlin/c;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/api/model/tracking/Impression$KycFrom;

    return-object v0
.end method

.method private final k()Lco/uk/getmondo/api/model/signup/SignupSource;
    .locals 1

    .prologue
    .line 31
    invoke-direct {p0}, Lco/uk/getmondo/signup/identity_verification/VerificationPendingActivity;->j()Lco/uk/getmondo/api/model/tracking/Impression$KycFrom;

    move-result-object v0

    invoke-virtual {v0}, Lco/uk/getmondo/api/model/tracking/Impression$KycFrom;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lco/uk/getmondo/api/model/signup/SignupSource;->SDD_MIGRATION:Lco/uk/getmondo/api/model/signup/SignupSource;

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lco/uk/getmondo/api/model/signup/SignupSource;->LEGACY_PREPAID:Lco/uk/getmondo/api/model/signup/SignupSource;

    goto :goto_0
.end method


# virtual methods
.method public a(I)Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/VerificationPendingActivity;->h:Ljava/util/HashMap;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lco/uk/getmondo/signup/identity_verification/VerificationPendingActivity;->h:Ljava/util/HashMap;

    :cond_0
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/VerificationPendingActivity;->h:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    if-nez v0, :cond_1

    invoke-virtual {p0, p1}, Landroid/support/v4/app/j;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/signup/identity_verification/VerificationPendingActivity;->h:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    return-object v0
.end method

.method public a()Lio/reactivex/n;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Lkotlin/n;",
            ">;"
        }
    .end annotation

    .prologue
    .line 56
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/VerificationPendingActivity;->e:Lcom/b/b/c;

    check-cast v0, Lio/reactivex/n;

    return-object v0
.end method

.method public a(Ljava/lang/String;)V
    .locals 7

    .prologue
    .line 85
    sget-object v0, Lco/uk/getmondo/signup/identity_verification/IdentityVerificationActivity;->g:Lco/uk/getmondo/signup/identity_verification/IdentityVerificationActivity$a;

    move-object v1, p0

    .line 86
    check-cast v1, Landroid/content/Context;

    invoke-direct {p0}, Lco/uk/getmondo/signup/identity_verification/VerificationPendingActivity;->i()Lco/uk/getmondo/signup/identity_verification/a/j;

    move-result-object v2

    invoke-direct {p0}, Lco/uk/getmondo/signup/identity_verification/VerificationPendingActivity;->j()Lco/uk/getmondo/api/model/tracking/Impression$KycFrom;

    move-result-object v3

    invoke-direct {p0}, Lco/uk/getmondo/signup/identity_verification/VerificationPendingActivity;->k()Lco/uk/getmondo/api/model/signup/SignupSource;

    move-result-object v4

    sget-object v5, Lco/uk/getmondo/signup/j;->b:Lco/uk/getmondo/signup/j;

    move-object v6, p1

    .line 85
    invoke-virtual/range {v0 .. v6}, Lco/uk/getmondo/signup/identity_verification/IdentityVerificationActivity$a;->a(Landroid/content/Context;Lco/uk/getmondo/signup/identity_verification/a/j;Lco/uk/getmondo/api/model/tracking/Impression$KycFrom;Lco/uk/getmondo/api/model/signup/SignupSource;Lco/uk/getmondo/signup/j;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/identity_verification/VerificationPendingActivity;->startActivity(Landroid/content/Intent;)V

    .line 87
    invoke-virtual {p0}, Lco/uk/getmondo/signup/identity_verification/VerificationPendingActivity;->finish()V

    .line 88
    return-void
.end method

.method public b()V
    .locals 2

    .prologue
    .line 60
    sget v0, Lco/uk/getmondo/c$a;->verificationPendingProgress:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/identity_verification/VerificationPendingActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 61
    return-void
.end method

.method public c()V
    .locals 2

    .prologue
    .line 64
    sget v0, Lco/uk/getmondo/c$a;->verificationPendingProgress:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/identity_verification/VerificationPendingActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 65
    return-void
.end method

.method public d()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 68
    sget v0, Lco/uk/getmondo/c$a;->verificationPendingImage:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/identity_verification/VerificationPendingActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 69
    sget v0, Lco/uk/getmondo/c$a;->verificationPendingTitle:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/identity_verification/VerificationPendingActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 70
    sget v0, Lco/uk/getmondo/c$a;->verificationPendingSubtitle:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/identity_verification/VerificationPendingActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 71
    return-void
.end method

.method public e()V
    .locals 3

    .prologue
    .line 74
    new-instance v1, Landroid/content/Intent;

    move-object v0, p0

    check-cast v0, Landroid/content/Context;

    const-class v2, Lco/uk/getmondo/create_account/topup/TopupInfoActivity;

    invoke-direct {v1, v0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v1}, Lco/uk/getmondo/signup/identity_verification/VerificationPendingActivity;->startActivity(Landroid/content/Intent;)V

    .line 75
    invoke-virtual {p0}, Lco/uk/getmondo/signup/identity_verification/VerificationPendingActivity;->finish()V

    .line 76
    return-void
.end method

.method public f()V
    .locals 9

    .prologue
    const/4 v6, 0x0

    .line 79
    sget-object v0, Lco/uk/getmondo/signup/identity_verification/IdentityVerificationActivity;->g:Lco/uk/getmondo/signup/identity_verification/IdentityVerificationActivity$a;

    move-object v1, p0

    .line 80
    check-cast v1, Landroid/content/Context;

    invoke-direct {p0}, Lco/uk/getmondo/signup/identity_verification/VerificationPendingActivity;->i()Lco/uk/getmondo/signup/identity_verification/a/j;

    move-result-object v2

    invoke-direct {p0}, Lco/uk/getmondo/signup/identity_verification/VerificationPendingActivity;->j()Lco/uk/getmondo/api/model/tracking/Impression$KycFrom;

    move-result-object v3

    invoke-direct {p0}, Lco/uk/getmondo/signup/identity_verification/VerificationPendingActivity;->k()Lco/uk/getmondo/api/model/signup/SignupSource;

    move-result-object v4

    sget-object v5, Lco/uk/getmondo/signup/j;->b:Lco/uk/getmondo/signup/j;

    const/16 v7, 0x20

    move-object v8, v6

    .line 79
    invoke-static/range {v0 .. v8}, Lco/uk/getmondo/signup/identity_verification/IdentityVerificationActivity$a;->a(Lco/uk/getmondo/signup/identity_verification/IdentityVerificationActivity$a;Landroid/content/Context;Lco/uk/getmondo/signup/identity_verification/a/j;Lco/uk/getmondo/api/model/tracking/Impression$KycFrom;Lco/uk/getmondo/api/model/signup/SignupSource;Lco/uk/getmondo/signup/j;Ljava/lang/String;ILjava/lang/Object;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/identity_verification/VerificationPendingActivity;->startActivity(Landroid/content/Intent;)V

    .line 81
    invoke-virtual {p0}, Lco/uk/getmondo/signup/identity_verification/VerificationPendingActivity;->finish()V

    .line 82
    return-void
.end method

.method public g()V
    .locals 2

    .prologue
    .line 91
    sget-object v1, Lco/uk/getmondo/signup/identity_verification/chat_with_us/ChatWithUsActivity;->b:Lco/uk/getmondo/signup/identity_verification/chat_with_us/ChatWithUsActivity$a;

    move-object v0, p0

    check-cast v0, Landroid/content/Context;

    invoke-virtual {v1, v0}, Lco/uk/getmondo/signup/identity_verification/chat_with_us/ChatWithUsActivity$a;->a(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/identity_verification/VerificationPendingActivity;->startActivity(Landroid/content/Intent;)V

    .line 92
    invoke-virtual {p0}, Lco/uk/getmondo/signup/identity_verification/VerificationPendingActivity;->finish()V

    .line 93
    return-void
.end method

.method public h()V
    .locals 3

    .prologue
    .line 96
    new-instance v1, Landroid/content/Intent;

    move-object v0, p0

    check-cast v0, Landroid/content/Context;

    const-class v2, Lco/uk/getmondo/main/HomeActivity;

    invoke-direct {v1, v0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v1}, Lco/uk/getmondo/signup/identity_verification/VerificationPendingActivity;->startActivity(Landroid/content/Intent;)V

    .line 97
    invoke-virtual {p0}, Lco/uk/getmondo/signup/identity_verification/VerificationPendingActivity;->finish()V

    .line 98
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 36
    invoke-super {p0, p1}, Lco/uk/getmondo/common/activities/b;->onCreate(Landroid/os/Bundle;)V

    .line 37
    const v0, 0x7f050072

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/identity_verification/VerificationPendingActivity;->setContentView(I)V

    .line 39
    invoke-virtual {p0}, Lco/uk/getmondo/signup/identity_verification/VerificationPendingActivity;->l()Lco/uk/getmondo/common/h/b/b;

    move-result-object v0

    new-instance v1, Lco/uk/getmondo/signup/identity_verification/p;

    invoke-direct {p0}, Lco/uk/getmondo/signup/identity_verification/VerificationPendingActivity;->i()Lco/uk/getmondo/signup/identity_verification/a/j;

    move-result-object v2

    invoke-direct {p0}, Lco/uk/getmondo/signup/identity_verification/VerificationPendingActivity;->k()Lco/uk/getmondo/api/model/signup/SignupSource;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lco/uk/getmondo/signup/identity_verification/p;-><init>(Lco/uk/getmondo/signup/identity_verification/a/j;Lco/uk/getmondo/api/model/signup/SignupSource;)V

    invoke-interface {v0, v1}, Lco/uk/getmondo/common/h/b/b;->a(Lco/uk/getmondo/signup/identity_verification/p;)Lco/uk/getmondo/signup/identity_verification/o;

    move-result-object v0

    .line 40
    invoke-interface {v0, p0}, Lco/uk/getmondo/signup/identity_verification/o;->a(Lco/uk/getmondo/signup/identity_verification/VerificationPendingActivity;)V

    .line 42
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/VerificationPendingActivity;->b:Lco/uk/getmondo/signup/identity_verification/aa;

    if-nez v0, :cond_0

    const-string v1, "verificationPendingPresenter"

    invoke-static {v1}, Lkotlin/d/b/l;->b(Ljava/lang/String;)V

    :cond_0
    check-cast p0, Lco/uk/getmondo/signup/identity_verification/aa$a;

    invoke-virtual {v0, p0}, Lco/uk/getmondo/signup/identity_verification/aa;->a(Lco/uk/getmondo/signup/identity_verification/aa$a;)V

    .line 43
    return-void
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    .line 51
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/VerificationPendingActivity;->b:Lco/uk/getmondo/signup/identity_verification/aa;

    if-nez v0, :cond_0

    const-string v1, "verificationPendingPresenter"

    invoke-static {v1}, Lkotlin/d/b/l;->b(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0}, Lco/uk/getmondo/signup/identity_verification/aa;->b()V

    .line 52
    invoke-super {p0}, Lco/uk/getmondo/common/activities/b;->onDestroy()V

    .line 53
    return-void
.end method

.method protected onResume()V
    .locals 2

    .prologue
    .line 46
    invoke-super {p0}, Lco/uk/getmondo/common/activities/b;->onResume()V

    .line 47
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/VerificationPendingActivity;->e:Lcom/b/b/c;

    sget-object v1, Lkotlin/n;->a:Lkotlin/n;

    invoke-virtual {v0, v1}, Lcom/b/b/c;->a(Ljava/lang/Object;)V

    .line 48
    return-void
.end method
