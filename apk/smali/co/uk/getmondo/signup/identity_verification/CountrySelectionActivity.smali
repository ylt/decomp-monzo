.class public Lco/uk/getmondo/signup/identity_verification/CountrySelectionActivity;
.super Lco/uk/getmondo/common/activities/b;
.source "CountrySelectionActivity.java"


# instance fields
.field a:Lco/uk/getmondo/common/a;

.field recyclerView:Landroid/support/v7/widget/RecyclerView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f11013e
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Lco/uk/getmondo/common/activities/b;-><init>()V

    return-void
.end method

.method public static a(Landroid/content/Context;Lco/uk/getmondo/api/model/identity_verification/IdentityDocumentType;)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 34
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lco/uk/getmondo/signup/identity_verification/CountrySelectionActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "KEY_DOCUMENT_TYPE"

    .line 35
    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    move-result-object v0

    .line 34
    return-object v0
.end method

.method public static a(Landroid/content/Intent;)Lco/uk/getmondo/api/model/identity_verification/IdentityDocumentType;
    .locals 1

    .prologue
    .line 39
    const-string v0, "KEY_DOCUMENT_TYPE"

    invoke-virtual {p0, v0}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/api/model/identity_verification/IdentityDocumentType;

    return-object v0
.end method

.method static synthetic a(Lco/uk/getmondo/signup/identity_verification/CountrySelectionActivity;Lco/uk/getmondo/api/model/identity_verification/IdentityDocumentType;Lco/uk/getmondo/d/i;)Lkotlin/n;
    .locals 2

    .prologue
    .line 68
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v1, "KEY_COUNTRY"

    .line 69
    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "KEY_DOCUMENT_TYPE"

    .line 70
    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    move-result-object v0

    .line 71
    const/4 v1, -0x1

    invoke-virtual {p0, v1, v0}, Lco/uk/getmondo/signup/identity_verification/CountrySelectionActivity;->setResult(ILandroid/content/Intent;)V

    .line 72
    invoke-virtual {p0}, Lco/uk/getmondo/signup/identity_verification/CountrySelectionActivity;->finish()V

    .line 73
    sget-object v0, Lkotlin/n;->a:Lkotlin/n;

    return-object v0
.end method

.method public static b(Landroid/content/Intent;)Lco/uk/getmondo/d/i;
    .locals 1

    .prologue
    .line 43
    const-string v0, "KEY_COUNTRY"

    invoke-virtual {p0, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/d/i;

    return-object v0
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 48
    invoke-super {p0, p1}, Lco/uk/getmondo/common/activities/b;->onCreate(Landroid/os/Bundle;)V

    .line 49
    const v0, 0x7f05002c

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/identity_verification/CountrySelectionActivity;->setContentView(I)V

    .line 50
    invoke-static {p0}, Lbutterknife/ButterKnife;->bind(Landroid/app/Activity;)Lbutterknife/Unbinder;

    .line 52
    invoke-virtual {p0}, Lco/uk/getmondo/signup/identity_verification/CountrySelectionActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "KEY_DOCUMENT_TYPE"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/api/model/identity_verification/IdentityDocumentType;

    .line 53
    if-nez v0, :cond_0

    .line 54
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Identity document type is required"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 57
    :cond_0
    invoke-virtual {p0}, Lco/uk/getmondo/signup/identity_verification/CountrySelectionActivity;->l()Lco/uk/getmondo/common/h/b/b;

    move-result-object v1

    invoke-interface {v1, p0}, Lco/uk/getmondo/common/h/b/b;->a(Lco/uk/getmondo/signup/identity_verification/CountrySelectionActivity;)V

    .line 59
    const v1, 0x7f0a01d1

    new-array v2, v5, [Ljava/lang/Object;

    const/4 v3, 0x0

    .line 60
    invoke-virtual {v0}, Lco/uk/getmondo/api/model/identity_verification/IdentityDocumentType;->c()I

    move-result v4

    invoke-virtual {p0, v4}, Lco/uk/getmondo/signup/identity_verification/CountrySelectionActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lco/uk/getmondo/common/k/p;->i(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    .line 59
    invoke-virtual {p0, v1, v2}, Lco/uk/getmondo/signup/identity_verification/CountrySelectionActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lco/uk/getmondo/signup/identity_verification/CountrySelectionActivity;->setTitle(Ljava/lang/CharSequence;)V

    .line 62
    new-instance v1, Lco/uk/getmondo/signup_old/a;

    invoke-static {}, Lco/uk/getmondo/d/i;->j()Ljava/util/List;

    move-result-object v2

    invoke-direct {v1, v2}, Lco/uk/getmondo/signup_old/a;-><init>(Ljava/util/List;)V

    .line 63
    iget-object v2, p0, Lco/uk/getmondo/signup/identity_verification/CountrySelectionActivity;->recyclerView:Landroid/support/v7/widget/RecyclerView;

    new-instance v3, Landroid/support/v7/widget/LinearLayoutManager;

    invoke-direct {v3, p0}, Landroid/support/v7/widget/LinearLayoutManager;-><init>(Landroid/content/Context;)V

    invoke-virtual {v2, v3}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(Landroid/support/v7/widget/RecyclerView$h;)V

    .line 64
    iget-object v2, p0, Lco/uk/getmondo/signup/identity_verification/CountrySelectionActivity;->recyclerView:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v2, v1}, Landroid/support/v7/widget/RecyclerView;->setAdapter(Landroid/support/v7/widget/RecyclerView$a;)V

    .line 65
    iget-object v2, p0, Lco/uk/getmondo/signup/identity_verification/CountrySelectionActivity;->recyclerView:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v2, v5}, Landroid/support/v7/widget/RecyclerView;->setHasFixedSize(Z)V

    .line 67
    invoke-static {p0, v0}, Lco/uk/getmondo/signup/identity_verification/b;->a(Lco/uk/getmondo/signup/identity_verification/CountrySelectionActivity;Lco/uk/getmondo/api/model/identity_verification/IdentityDocumentType;)Lkotlin/d/a/b;

    move-result-object v2

    invoke-virtual {v1, v2}, Lco/uk/getmondo/signup_old/a;->a(Lkotlin/d/a/b;)V

    .line 76
    iget-object v1, p0, Lco/uk/getmondo/signup/identity_verification/CountrySelectionActivity;->a:Lco/uk/getmondo/common/a;

    invoke-virtual {v0}, Lco/uk/getmondo/api/model/identity_verification/IdentityDocumentType;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lco/uk/getmondo/api/model/tracking/Impression;->i(Ljava/lang/String;)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    invoke-virtual {v1, v0}, Lco/uk/getmondo/common/a;->a(Lco/uk/getmondo/api/model/tracking/Impression;)V

    .line 77
    return-void
.end method
