.class final Lco/uk/getmondo/signup/identity_verification/v$c$1;
.super Ljava/lang/Object;
.source "IdentityVerificationPresenter.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lco/uk/getmondo/signup/identity_verification/v$c;->a(Lkotlin/n;)Lio/reactivex/h;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h",
        "<TT;",
        "Lio/reactivex/z",
        "<+TR;>;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0010\u0000\u001a&\u0012\u000c\u0012\n \u0003*\u0004\u0018\u00010\u00020\u0002 \u0003*\u0012\u0012\u000c\u0012\n \u0003*\u0004\u0018\u00010\u00020\u0002\u0018\u00010\u00010\u00012\u0006\u0010\u0004\u001a\u00020\u0002H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "Lio/reactivex/Single;",
        "Lco/uk/getmondo/api/model/identity_verification/IdentityVerification;",
        "kotlin.jvm.PlatformType",
        "it",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field final synthetic a:Lco/uk/getmondo/signup/identity_verification/v$c;


# direct methods
.method constructor <init>(Lco/uk/getmondo/signup/identity_verification/v$c;)V
    .locals 0

    iput-object p1, p0, Lco/uk/getmondo/signup/identity_verification/v$c$1;->a:Lco/uk/getmondo/signup/identity_verification/v$c;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lco/uk/getmondo/api/model/identity_verification/IdentityVerification;)Lio/reactivex/v;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lco/uk/getmondo/api/model/identity_verification/IdentityVerification;",
            ")",
            "Lio/reactivex/v",
            "<",
            "Lco/uk/getmondo/api/model/identity_verification/IdentityVerification;",
            ">;"
        }
    .end annotation

    .prologue
    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 42
    invoke-virtual {p1}, Lco/uk/getmondo/api/model/identity_verification/IdentityVerification;->b()Lco/uk/getmondo/api/model/identity_verification/IdentityVerification$Status;

    move-result-object v0

    sget-object v1, Lco/uk/getmondo/api/model/identity_verification/IdentityVerification$Status;->NOT_STARTED:Lco/uk/getmondo/api/model/identity_verification/IdentityVerification$Status;

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 43
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/v$c$1;->a:Lco/uk/getmondo/signup/identity_verification/v$c;

    iget-object v0, v0, Lco/uk/getmondo/signup/identity_verification/v$c;->a:Lco/uk/getmondo/signup/identity_verification/v;

    invoke-static {v0}, Lco/uk/getmondo/signup/identity_verification/v;->b(Lco/uk/getmondo/signup/identity_verification/v;)Lco/uk/getmondo/profile/data/a;

    move-result-object v0

    invoke-virtual {v0}, Lco/uk/getmondo/profile/data/a;->a()Lio/reactivex/b;

    move-result-object v0

    invoke-virtual {v0, p1}, Lio/reactivex/b;->a(Ljava/lang/Object;)Lio/reactivex/v;

    move-result-object v0

    .line 42
    :goto_0
    return-object v0

    .line 45
    :cond_0
    invoke-static {p1}, Lio/reactivex/v;->a(Ljava/lang/Object;)Lio/reactivex/v;

    move-result-object v0

    goto :goto_0
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 22
    check-cast p1, Lco/uk/getmondo/api/model/identity_verification/IdentityVerification;

    invoke-virtual {p0, p1}, Lco/uk/getmondo/signup/identity_verification/v$c$1;->a(Lco/uk/getmondo/api/model/identity_verification/IdentityVerification;)Lio/reactivex/v;

    move-result-object v0

    return-object v0
.end method
