.class public final Lco/uk/getmondo/signup/identity_verification/t$a;
.super Ljava/lang/Object;
.source "IdentityVerificationOnboardingFragment.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lco/uk/getmondo/signup/identity_verification/t;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000&\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0008\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u0016\u0010\u0006\u001a\u00020\u00072\u0006\u0010\u0008\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\u000bR\u000e\u0010\u0003\u001a\u00020\u0004X\u0082T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0082T\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000c"
    }
    d2 = {
        "Lco/uk/getmondo/signup/identity_verification/IdentityVerificationOnboardingFragment$Companion;",
        "",
        "()V",
        "KEY_ENTRY_POINT",
        "",
        "KEY_VERSION",
        "newInstance",
        "Landroid/support/v4/app/Fragment;",
        "version",
        "Lco/uk/getmondo/signup/identity_verification/data/IdentityVerificationVersion;",
        "from",
        "Lco/uk/getmondo/api/model/tracking/Impression$KycFrom;",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 98
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/d/b/i;)V
    .locals 0

    .prologue
    .line 98
    invoke-direct {p0}, Lco/uk/getmondo/signup/identity_verification/t$a;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lco/uk/getmondo/signup/identity_verification/a/j;Lco/uk/getmondo/api/model/tracking/Impression$KycFrom;)Landroid/support/v4/app/Fragment;
    .locals 3

    .prologue
    const-string v0, "version"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "from"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 103
    new-instance v0, Lco/uk/getmondo/signup/identity_verification/t;

    invoke-direct {v0}, Lco/uk/getmondo/signup/identity_verification/t;-><init>()V

    .line 104
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 105
    const-string v2, "KEY_VERSION"

    check-cast p1, Ljava/io/Serializable;

    invoke-virtual {v1, v2, p1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 106
    const-string v2, "KEY_ENTRY_POINT"

    check-cast p2, Ljava/io/Serializable;

    invoke-virtual {v1, v2, p2}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 107
    invoke-virtual {v0, v1}, Lco/uk/getmondo/signup/identity_verification/t;->setArguments(Landroid/os/Bundle;)V

    .line 108
    check-cast v0, Landroid/support/v4/app/Fragment;

    return-object v0
.end method
