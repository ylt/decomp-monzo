.class public final synthetic Lco/uk/getmondo/signup/identity_verification/ab;
.super Ljava/lang/Object;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# static fields
.field public static final synthetic a:[I

.field public static final synthetic b:[I


# direct methods
.method static synthetic constructor <clinit>()V
    .locals 4

    const/4 v3, 0x2

    const/4 v2, 0x1

    invoke-static {}, Lco/uk/getmondo/d/ak$a;->values()[Lco/uk/getmondo/d/ak$a;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lco/uk/getmondo/signup/identity_verification/ab;->a:[I

    sget-object v0, Lco/uk/getmondo/signup/identity_verification/ab;->a:[I

    sget-object v1, Lco/uk/getmondo/d/ak$a;->HAS_ACCOUNT:Lco/uk/getmondo/d/ak$a;

    invoke-virtual {v1}, Lco/uk/getmondo/d/ak$a;->ordinal()I

    move-result v1

    aput v2, v0, v1

    sget-object v0, Lco/uk/getmondo/signup/identity_verification/ab;->a:[I

    sget-object v1, Lco/uk/getmondo/d/ak$a;->INITIAL_TOPUP_REQUIRED:Lco/uk/getmondo/d/ak$a;

    invoke-virtual {v1}, Lco/uk/getmondo/d/ak$a;->ordinal()I

    move-result v1

    aput v3, v0, v1

    invoke-static {}, Lco/uk/getmondo/api/model/identity_verification/IdentityVerification$Status;->values()[Lco/uk/getmondo/api/model/identity_verification/IdentityVerification$Status;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lco/uk/getmondo/signup/identity_verification/ab;->b:[I

    sget-object v0, Lco/uk/getmondo/signup/identity_verification/ab;->b:[I

    sget-object v1, Lco/uk/getmondo/api/model/identity_verification/IdentityVerification$Status;->NOT_STARTED:Lco/uk/getmondo/api/model/identity_verification/IdentityVerification$Status;

    invoke-virtual {v1}, Lco/uk/getmondo/api/model/identity_verification/IdentityVerification$Status;->ordinal()I

    move-result v1

    aput v2, v0, v1

    sget-object v0, Lco/uk/getmondo/signup/identity_verification/ab;->b:[I

    sget-object v1, Lco/uk/getmondo/api/model/identity_verification/IdentityVerification$Status;->PENDING_SUBMISSION:Lco/uk/getmondo/api/model/identity_verification/IdentityVerification$Status;

    invoke-virtual {v1}, Lco/uk/getmondo/api/model/identity_verification/IdentityVerification$Status;->ordinal()I

    move-result v1

    aput v3, v0, v1

    sget-object v0, Lco/uk/getmondo/signup/identity_verification/ab;->b:[I

    sget-object v1, Lco/uk/getmondo/api/model/identity_verification/IdentityVerification$Status;->APPROVED:Lco/uk/getmondo/api/model/identity_verification/IdentityVerification$Status;

    invoke-virtual {v1}, Lco/uk/getmondo/api/model/identity_verification/IdentityVerification$Status;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1

    sget-object v0, Lco/uk/getmondo/signup/identity_verification/ab;->b:[I

    sget-object v1, Lco/uk/getmondo/api/model/identity_verification/IdentityVerification$Status;->NOT_REQUIRED:Lco/uk/getmondo/api/model/identity_verification/IdentityVerification$Status;

    invoke-virtual {v1}, Lco/uk/getmondo/api/model/identity_verification/IdentityVerification$Status;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1

    sget-object v0, Lco/uk/getmondo/signup/identity_verification/ab;->b:[I

    sget-object v1, Lco/uk/getmondo/api/model/identity_verification/IdentityVerification$Status;->BLOCKED:Lco/uk/getmondo/api/model/identity_verification/IdentityVerification$Status;

    invoke-virtual {v1}, Lco/uk/getmondo/api/model/identity_verification/IdentityVerification$Status;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1

    sget-object v0, Lco/uk/getmondo/signup/identity_verification/ab;->b:[I

    sget-object v1, Lco/uk/getmondo/api/model/identity_verification/IdentityVerification$Status;->PENDING_REVIEW:Lco/uk/getmondo/api/model/identity_verification/IdentityVerification$Status;

    invoke-virtual {v1}, Lco/uk/getmondo/api/model/identity_verification/IdentityVerification$Status;->ordinal()I

    move-result v1

    const/4 v2, 0x6

    aput v2, v0, v1

    return-void
.end method
