.class public final Lco/uk/getmondo/signup/identity_verification/s;
.super Ljava/lang/Object;
.source "IdentityVerificationModule_ProvideSignUpSourceFactory.java"

# interfaces
.implements Lb/a/b;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lb/a/b",
        "<",
        "Lco/uk/getmondo/api/model/signup/SignupSource;",
        ">;"
    }
.end annotation


# static fields
.field static final synthetic a:Z


# instance fields
.field private final b:Lco/uk/getmondo/signup/identity_verification/p;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 8
    const-class v0, Lco/uk/getmondo/signup/identity_verification/s;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lco/uk/getmondo/signup/identity_verification/s;->a:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lco/uk/getmondo/signup/identity_verification/p;)V
    .locals 1

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    sget-boolean v0, Lco/uk/getmondo/signup/identity_verification/s;->a:Z

    if-nez v0, :cond_0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 18
    :cond_0
    iput-object p1, p0, Lco/uk/getmondo/signup/identity_verification/s;->b:Lco/uk/getmondo/signup/identity_verification/p;

    .line 19
    return-void
.end method

.method public static a(Lco/uk/getmondo/signup/identity_verification/p;)Lb/a/b;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lco/uk/getmondo/signup/identity_verification/p;",
            ")",
            "Lb/a/b",
            "<",
            "Lco/uk/getmondo/api/model/signup/SignupSource;",
            ">;"
        }
    .end annotation

    .prologue
    .line 28
    new-instance v0, Lco/uk/getmondo/signup/identity_verification/s;

    invoke-direct {v0, p0}, Lco/uk/getmondo/signup/identity_verification/s;-><init>(Lco/uk/getmondo/signup/identity_verification/p;)V

    return-object v0
.end method


# virtual methods
.method public a()Lco/uk/getmondo/api/model/signup/SignupSource;
    .locals 2

    .prologue
    .line 23
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/s;->b:Lco/uk/getmondo/signup/identity_verification/p;

    .line 24
    invoke-virtual {v0}, Lco/uk/getmondo/signup/identity_verification/p;->b()Lco/uk/getmondo/api/model/signup/SignupSource;

    move-result-object v0

    const-string v1, "Cannot return null from a non-@Nullable @Provides method"

    .line 23
    invoke-static {v0, v1}, Lb/a/d;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/api/model/signup/SignupSource;

    return-object v0
.end method

.method public synthetic b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 8
    invoke-virtual {p0}, Lco/uk/getmondo/signup/identity_verification/s;->a()Lco/uk/getmondo/api/model/signup/SignupSource;

    move-result-object v0

    return-object v0
.end method
