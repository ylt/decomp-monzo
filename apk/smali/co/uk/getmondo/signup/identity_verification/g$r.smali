.class final Lco/uk/getmondo/signup/identity_verification/g$r;
.super Ljava/lang/Object;
.source "IdentityDocumentsPresenter.kt"

# interfaces
.implements Lio/reactivex/c/g;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lco/uk/getmondo/signup/identity_verification/g;->a(Lco/uk/getmondo/signup/identity_verification/g$a;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/g",
        "<",
        "Lco/uk/getmondo/api/model/identity_verification/IdentityDocumentType;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "",
        "idDocumentType",
        "Lco/uk/getmondo/api/model/identity_verification/IdentityDocumentType;",
        "kotlin.jvm.PlatformType",
        "accept"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field final synthetic a:Lco/uk/getmondo/signup/identity_verification/g;

.field final synthetic b:Lco/uk/getmondo/signup/identity_verification/g$a;


# direct methods
.method constructor <init>(Lco/uk/getmondo/signup/identity_verification/g;Lco/uk/getmondo/signup/identity_verification/g$a;)V
    .locals 0

    iput-object p1, p0, Lco/uk/getmondo/signup/identity_verification/g$r;->a:Lco/uk/getmondo/signup/identity_verification/g;

    iput-object p2, p0, Lco/uk/getmondo/signup/identity_verification/g$r;->b:Lco/uk/getmondo/signup/identity_verification/g$a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lco/uk/getmondo/api/model/identity_verification/IdentityDocumentType;)V
    .locals 3

    .prologue
    .line 91
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/g$r;->b:Lco/uk/getmondo/signup/identity_verification/g$a;

    iget-object v1, p0, Lco/uk/getmondo/signup/identity_verification/g$r;->a:Lco/uk/getmondo/signup/identity_verification/g;

    invoke-static {v1}, Lco/uk/getmondo/signup/identity_verification/g;->b(Lco/uk/getmondo/signup/identity_verification/g;)Lco/uk/getmondo/signup/identity_verification/a/j;

    move-result-object v1

    const-string v2, "idDocumentType"

    invoke-static {p1, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v2, Lco/uk/getmondo/d/i;->UNITED_KINGDOM:Lco/uk/getmondo/d/i;

    invoke-interface {v0, v1, p1, v2}, Lco/uk/getmondo/signup/identity_verification/g$a;->a(Lco/uk/getmondo/signup/identity_verification/a/j;Lco/uk/getmondo/api/model/identity_verification/IdentityDocumentType;Lco/uk/getmondo/d/i;)V

    .line 92
    return-void
.end method

.method public bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 26
    check-cast p1, Lco/uk/getmondo/api/model/identity_verification/IdentityDocumentType;

    invoke-virtual {p0, p1}, Lco/uk/getmondo/signup/identity_verification/g$r;->a(Lco/uk/getmondo/api/model/identity_verification/IdentityDocumentType;)V

    return-void
.end method
