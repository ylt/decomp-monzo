.class final Lco/uk/getmondo/signup/identity_verification/fallback/DocumentFallbackActivity$c;
.super Ljava/lang/Object;
.source "DocumentFallbackActivity.kt"

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lco/uk/getmondo/signup/identity_verification/fallback/DocumentFallbackActivity;->a(Landroid/graphics/Bitmap;Z)Lio/reactivex/n;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<TT;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000\n\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\n \u0002*\u0004\u0018\u00010\u00010\u0001H\n\u00a2\u0006\u0002\u0008\u0003"
    }
    d2 = {
        "<anonymous>",
        "Landroid/net/Uri;",
        "kotlin.jvm.PlatformType",
        "call"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field final synthetic a:Lco/uk/getmondo/signup/identity_verification/fallback/DocumentFallbackActivity;

.field final synthetic b:Z

.field final synthetic c:Landroid/graphics/Bitmap;


# direct methods
.method constructor <init>(Lco/uk/getmondo/signup/identity_verification/fallback/DocumentFallbackActivity;ZLandroid/graphics/Bitmap;)V
    .locals 0

    iput-object p1, p0, Lco/uk/getmondo/signup/identity_verification/fallback/DocumentFallbackActivity$c;->a:Lco/uk/getmondo/signup/identity_verification/fallback/DocumentFallbackActivity;

    iput-boolean p2, p0, Lco/uk/getmondo/signup/identity_verification/fallback/DocumentFallbackActivity$c;->b:Z

    iput-object p3, p0, Lco/uk/getmondo/signup/identity_verification/fallback/DocumentFallbackActivity$c;->c:Landroid/graphics/Bitmap;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Landroid/net/Uri;
    .locals 8

    .prologue
    .line 92
    iget-boolean v1, p0, Lco/uk/getmondo/signup/identity_verification/fallback/DocumentFallbackActivity$c;->b:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lco/uk/getmondo/signup/identity_verification/fallback/DocumentFallbackActivity$c;->a:Lco/uk/getmondo/signup/identity_verification/fallback/DocumentFallbackActivity;

    invoke-virtual {v1}, Lco/uk/getmondo/signup/identity_verification/fallback/DocumentFallbackActivity;->a()Lco/uk/getmondo/signup/identity_verification/a/a;

    move-result-object v1

    invoke-virtual {v1}, Lco/uk/getmondo/signup/identity_verification/a/a;->b()Ljava/io/File;

    move-result-object v1

    move-object v3, v1

    .line 93
    :goto_0
    new-instance v1, Ljava/io/FileOutputStream;

    invoke-direct {v1, v3}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    check-cast v1, Ljava/io/Closeable;

    const/4 v4, 0x0

    nop

    :try_start_0
    move-object v0, v1

    check-cast v0, Ljava/io/FileOutputStream;

    move-object v2, v0

    iget-object v5, p0, Lco/uk/getmondo/signup/identity_verification/fallback/DocumentFallbackActivity$c;->c:Landroid/graphics/Bitmap;

    sget-object v6, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v7, 0x5a

    check-cast v2, Ljava/io/OutputStream;

    invoke-virtual {v5, v6, v7, v2}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    invoke-interface {v1}, Ljava/io/Closeable;->close()V

    .line 94
    invoke-static {v3}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v1

    return-object v1

    .line 92
    :cond_0
    iget-object v1, p0, Lco/uk/getmondo/signup/identity_verification/fallback/DocumentFallbackActivity$c;->a:Lco/uk/getmondo/signup/identity_verification/fallback/DocumentFallbackActivity;

    invoke-virtual {v1}, Lco/uk/getmondo/signup/identity_verification/fallback/DocumentFallbackActivity;->a()Lco/uk/getmondo/signup/identity_verification/a/a;

    move-result-object v1

    invoke-virtual {v1}, Lco/uk/getmondo/signup/identity_verification/a/a;->c()Ljava/io/File;

    move-result-object v1

    move-object v3, v1

    goto :goto_0

    .line 93
    :catch_0
    move-exception v2

    const/4 v3, 0x1

    nop

    :try_start_1
    invoke-interface {v1}, Ljava/io/Closeable;->close()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :goto_1
    :try_start_2
    check-cast v2, Ljava/lang/Throwable;

    throw v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :catchall_0
    move-exception v2

    :goto_2
    if-nez v3, :cond_1

    invoke-interface {v1}, Ljava/io/Closeable;->close()V

    :cond_1
    throw v2

    :catch_1
    move-exception v4

    goto :goto_1

    :catchall_1
    move-exception v2

    move v3, v4

    goto :goto_2
.end method

.method public synthetic call()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 38
    invoke-virtual {p0}, Lco/uk/getmondo/signup/identity_verification/fallback/DocumentFallbackActivity$c;->a()Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method
