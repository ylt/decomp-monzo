.class public final Lco/uk/getmondo/signup/identity_verification/fallback/h;
.super Lco/uk/getmondo/common/ui/b;
.source "VideoFallbackPresenter.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lco/uk/getmondo/signup/identity_verification/fallback/h$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lco/uk/getmondo/common/ui/b",
        "<",
        "Lco/uk/getmondo/signup/identity_verification/fallback/h$a;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u00000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0003\u0008\u0007\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00020\u0001:\u0001\u000fB\u001f\u0008\u0007\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u0006\u0010\u0007\u001a\u00020\u0008\u00a2\u0006\u0002\u0010\tJ\u0010\u0010\u000c\u001a\u00020\r2\u0006\u0010\u000e\u001a\u00020\u0002H\u0016R\u000e\u0010\u0007\u001a\u00020\u0008X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\n\u001a\u0004\u0018\u00010\u000bX\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0010"
    }
    d2 = {
        "Lco/uk/getmondo/signup/identity_verification/fallback/VideoFallbackPresenter;",
        "Lco/uk/getmondo/common/ui/BasePresenter;",
        "Lco/uk/getmondo/signup/identity_verification/fallback/VideoFallbackPresenter$View;",
        "verificationManager",
        "Lco/uk/getmondo/signup/identity_verification/data/IdentityVerificationManager;",
        "stringProvider",
        "Lco/uk/getmondo/signup/identity_verification/video/VideoStringProvider;",
        "analyticsService",
        "Lco/uk/getmondo/common/AnalyticsService;",
        "(Lco/uk/getmondo/signup/identity_verification/data/IdentityVerificationManager;Lco/uk/getmondo/signup/identity_verification/video/VideoStringProvider;Lco/uk/getmondo/common/AnalyticsService;)V",
        "videoPath",
        "",
        "register",
        "",
        "view",
        "View",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field private c:Ljava/lang/String;

.field private final d:Lco/uk/getmondo/signup/identity_verification/a/e;

.field private final e:Lco/uk/getmondo/signup/identity_verification/video/ab;

.field private final f:Lco/uk/getmondo/common/a;


# direct methods
.method public constructor <init>(Lco/uk/getmondo/signup/identity_verification/a/e;Lco/uk/getmondo/signup/identity_verification/video/ab;Lco/uk/getmondo/common/a;)V
    .locals 1

    .prologue
    const-string v0, "verificationManager"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "stringProvider"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "analyticsService"

    invoke-static {p3, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 24
    invoke-direct {p0}, Lco/uk/getmondo/common/ui/b;-><init>()V

    iput-object p1, p0, Lco/uk/getmondo/signup/identity_verification/fallback/h;->d:Lco/uk/getmondo/signup/identity_verification/a/e;

    iput-object p2, p0, Lco/uk/getmondo/signup/identity_verification/fallback/h;->e:Lco/uk/getmondo/signup/identity_verification/video/ab;

    iput-object p3, p0, Lco/uk/getmondo/signup/identity_verification/fallback/h;->f:Lco/uk/getmondo/common/a;

    return-void
.end method

.method public static final synthetic a(Lco/uk/getmondo/signup/identity_verification/fallback/h;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 19
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/fallback/h;->c:Ljava/lang/String;

    return-object v0
.end method

.method public static final synthetic a(Lco/uk/getmondo/signup/identity_verification/fallback/h;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 19
    iput-object p1, p0, Lco/uk/getmondo/signup/identity_verification/fallback/h;->c:Ljava/lang/String;

    return-void
.end method

.method public static final synthetic b(Lco/uk/getmondo/signup/identity_verification/fallback/h;)Lco/uk/getmondo/signup/identity_verification/a/e;
    .locals 1

    .prologue
    .line 19
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/fallback/h;->d:Lco/uk/getmondo/signup/identity_verification/a/e;

    return-object v0
.end method

.method public static final synthetic c(Lco/uk/getmondo/signup/identity_verification/fallback/h;)Lco/uk/getmondo/signup/identity_verification/video/ab;
    .locals 1

    .prologue
    .line 19
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/fallback/h;->e:Lco/uk/getmondo/signup/identity_verification/video/ab;

    return-object v0
.end method


# virtual methods
.method public bridge synthetic a(Lco/uk/getmondo/common/ui/f;)V
    .locals 0

    .prologue
    .line 19
    check-cast p1, Lco/uk/getmondo/signup/identity_verification/fallback/h$a;

    invoke-virtual {p0, p1}, Lco/uk/getmondo/signup/identity_verification/fallback/h;->a(Lco/uk/getmondo/signup/identity_verification/fallback/h$a;)V

    return-void
.end method

.method public a(Lco/uk/getmondo/signup/identity_verification/fallback/h$a;)V
    .locals 5

    .prologue
    const-string v0, "view"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v0, p1

    .line 28
    check-cast v0, Lco/uk/getmondo/common/ui/f;

    invoke-super {p0, v0}, Lco/uk/getmondo/common/ui/b;->a(Lco/uk/getmondo/common/ui/f;)V

    .line 30
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/fallback/h;->f:Lco/uk/getmondo/common/a;

    sget-object v1, Lco/uk/getmondo/api/model/tracking/Impression;->Companion:Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->f(Z)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v1

    invoke-virtual {v0, v1}, Lco/uk/getmondo/common/a;->a(Lco/uk/getmondo/api/model/tracking/Impression;)V

    .line 31
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/fallback/h;->e:Lco/uk/getmondo/signup/identity_verification/video/ab;

    invoke-virtual {v0}, Lco/uk/getmondo/signup/identity_verification/video/ab;->b()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/signup/identity_verification/fallback/h;->e:Lco/uk/getmondo/signup/identity_verification/video/ab;

    invoke-virtual {v1}, Lco/uk/getmondo/signup/identity_verification/video/ab;->a()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v0, v1}, Lco/uk/getmondo/signup/identity_verification/fallback/h$a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 33
    iget-object v3, p0, Lco/uk/getmondo/signup/identity_verification/fallback/h;->b:Lio/reactivex/b/a;

    .line 34
    invoke-interface {p1}, Lco/uk/getmondo/signup/identity_verification/fallback/h$a;->b()Lio/reactivex/n;

    move-result-object v4

    new-instance v0, Lco/uk/getmondo/signup/identity_verification/fallback/h$b;

    invoke-direct {v0, p0, p1}, Lco/uk/getmondo/signup/identity_verification/fallback/h$b;-><init>(Lco/uk/getmondo/signup/identity_verification/fallback/h;Lco/uk/getmondo/signup/identity_verification/fallback/h$a;)V

    check-cast v0, Lio/reactivex/c/g;

    .line 47
    sget-object v1, Lco/uk/getmondo/signup/identity_verification/fallback/h$c;->a:Lco/uk/getmondo/signup/identity_verification/fallback/h$c;

    check-cast v1, Lkotlin/d/a/b;

    if-eqz v1, :cond_0

    new-instance v2, Lco/uk/getmondo/signup/identity_verification/fallback/i;

    invoke-direct {v2, v1}, Lco/uk/getmondo/signup/identity_verification/fallback/i;-><init>(Lkotlin/d/a/b;)V

    move-object v1, v2

    :cond_0
    check-cast v1, Lio/reactivex/c/g;

    .line 34
    invoke-virtual {v4, v0, v1}, Lio/reactivex/n;->subscribe(Lio/reactivex/c/g;Lio/reactivex/c/g;)Lio/reactivex/b/b;

    move-result-object v0

    const-string v1, "view.onMainActionClicked\u2026            }, Timber::e)"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 33
    invoke-static {v3, v0}, Lco/uk/getmondo/common/j/f;->a(Lio/reactivex/b/a;Lio/reactivex/b/b;)Lio/reactivex/b/a;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/signup/identity_verification/fallback/h;->b:Lio/reactivex/b/a;

    .line 49
    iget-object v1, p0, Lco/uk/getmondo/signup/identity_verification/fallback/h;->b:Lio/reactivex/b/a;

    .line 50
    invoke-interface {p1}, Lco/uk/getmondo/signup/identity_verification/fallback/h$a;->c()Lio/reactivex/n;

    move-result-object v2

    new-instance v0, Lco/uk/getmondo/signup/identity_verification/fallback/h$d;

    invoke-direct {v0, p0, p1}, Lco/uk/getmondo/signup/identity_verification/fallback/h$d;-><init>(Lco/uk/getmondo/signup/identity_verification/fallback/h;Lco/uk/getmondo/signup/identity_verification/fallback/h$a;)V

    check-cast v0, Lio/reactivex/c/g;

    invoke-virtual {v2, v0}, Lio/reactivex/n;->subscribe(Lio/reactivex/c/g;)Lio/reactivex/b/b;

    move-result-object v0

    const-string v2, "view.onVideoTaken()\n    \u2026      }\n                }"

    invoke-static {v0, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 49
    invoke-static {v1, v0}, Lco/uk/getmondo/common/j/f;->a(Lio/reactivex/b/a;Lio/reactivex/b/b;)Lio/reactivex/b/a;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/signup/identity_verification/fallback/h;->b:Lio/reactivex/b/a;

    .line 59
    iget-object v1, p0, Lco/uk/getmondo/signup/identity_verification/fallback/h;->b:Lio/reactivex/b/a;

    .line 60
    invoke-interface {p1}, Lco/uk/getmondo/signup/identity_verification/fallback/h$a;->e()Lio/reactivex/n;

    move-result-object v2

    new-instance v0, Lco/uk/getmondo/signup/identity_verification/fallback/h$e;

    invoke-direct {v0, p1}, Lco/uk/getmondo/signup/identity_verification/fallback/h$e;-><init>(Lco/uk/getmondo/signup/identity_verification/fallback/h$a;)V

    check-cast v0, Lio/reactivex/c/g;

    invoke-virtual {v2, v0}, Lio/reactivex/n;->subscribe(Lio/reactivex/c/g;)Lio/reactivex/b/b;

    move-result-object v0

    const-string v2, "view.onMuteClicked()\n   \u2026      }\n                }"

    invoke-static {v0, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 59
    invoke-static {v1, v0}, Lco/uk/getmondo/common/j/f;->a(Lio/reactivex/b/a;Lio/reactivex/b/b;)Lio/reactivex/b/a;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/signup/identity_verification/fallback/h;->b:Lio/reactivex/b/a;

    .line 68
    iget-object v3, p0, Lco/uk/getmondo/signup/identity_verification/fallback/h;->b:Lio/reactivex/b/a;

    .line 69
    invoke-interface {p1}, Lco/uk/getmondo/signup/identity_verification/fallback/h$a;->d()Lio/reactivex/n;

    move-result-object v4

    new-instance v0, Lco/uk/getmondo/signup/identity_verification/fallback/h$f;

    invoke-direct {v0, p0, p1}, Lco/uk/getmondo/signup/identity_verification/fallback/h$f;-><init>(Lco/uk/getmondo/signup/identity_verification/fallback/h;Lco/uk/getmondo/signup/identity_verification/fallback/h$a;)V

    check-cast v0, Lio/reactivex/c/g;

    .line 73
    sget-object v1, Lco/uk/getmondo/signup/identity_verification/fallback/h$g;->a:Lco/uk/getmondo/signup/identity_verification/fallback/h$g;

    check-cast v1, Lkotlin/d/a/b;

    if-eqz v1, :cond_1

    new-instance v2, Lco/uk/getmondo/signup/identity_verification/fallback/i;

    invoke-direct {v2, v1}, Lco/uk/getmondo/signup/identity_verification/fallback/i;-><init>(Lkotlin/d/a/b;)V

    move-object v1, v2

    :cond_1
    check-cast v1, Lio/reactivex/c/g;

    .line 69
    invoke-virtual {v4, v0, v1}, Lio/reactivex/n;->subscribe(Lio/reactivex/c/g;Lio/reactivex/c/g;)Lio/reactivex/b/b;

    move-result-object v0

    const-string v1, "view.onRetakeClicked()\n \u2026            }, Timber::e)"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 68
    invoke-static {v3, v0}, Lco/uk/getmondo/common/j/f;->a(Lio/reactivex/b/a;Lio/reactivex/b/b;)Lio/reactivex/b/a;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/signup/identity_verification/fallback/h;->b:Lio/reactivex/b/a;

    .line 74
    return-void
.end method
