.class final Lco/uk/getmondo/signup/identity_verification/fallback/b$b;
.super Ljava/lang/Object;
.source "DocumentFallbackPresenter.kt"

# interfaces
.implements Lio/reactivex/c/g;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lco/uk/getmondo/signup/identity_verification/fallback/b;->a(Lco/uk/getmondo/signup/identity_verification/fallback/b$a;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/g",
        "<",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0003\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0004\u0008\u0005\u0010\u0006"
    }
    d2 = {
        "<anonymous>",
        "",
        "hasCameraPermission",
        "",
        "kotlin.jvm.PlatformType",
        "accept",
        "(Ljava/lang/Boolean;)V"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field final synthetic a:Lco/uk/getmondo/signup/identity_verification/fallback/b;

.field final synthetic b:Lco/uk/getmondo/signup/identity_verification/fallback/b$a;


# direct methods
.method constructor <init>(Lco/uk/getmondo/signup/identity_verification/fallback/b;Lco/uk/getmondo/signup/identity_verification/fallback/b$a;)V
    .locals 0

    iput-object p1, p0, Lco/uk/getmondo/signup/identity_verification/fallback/b$b;->a:Lco/uk/getmondo/signup/identity_verification/fallback/b;

    iput-object p2, p0, Lco/uk/getmondo/signup/identity_verification/fallback/b$b;->b:Lco/uk/getmondo/signup/identity_verification/fallback/b$a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Boolean;)V
    .locals 6

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 51
    const-string v0, "hasCameraPermission"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 52
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/fallback/b$b;->a:Lco/uk/getmondo/signup/identity_verification/fallback/b;

    invoke-static {v0}, Lco/uk/getmondo/signup/identity_verification/fallback/b;->a(Lco/uk/getmondo/signup/identity_verification/fallback/b;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_4

    move v0, v1

    .line 53
    :goto_0
    iget-object v3, p0, Lco/uk/getmondo/signup/identity_verification/fallback/b$b;->a:Lco/uk/getmondo/signup/identity_verification/fallback/b;

    invoke-static {v3}, Lco/uk/getmondo/signup/identity_verification/fallback/b;->b(Lco/uk/getmondo/signup/identity_verification/fallback/b;)Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_0

    move v2, v1

    .line 55
    :cond_0
    iget-object v3, p0, Lco/uk/getmondo/signup/identity_verification/fallback/b$b;->a:Lco/uk/getmondo/signup/identity_verification/fallback/b;

    invoke-static {v3}, Lco/uk/getmondo/signup/identity_verification/fallback/b;->c(Lco/uk/getmondo/signup/identity_verification/fallback/b;)Lco/uk/getmondo/api/model/identity_verification/IdentityDocumentType;

    move-result-object v3

    invoke-virtual {v3}, Lco/uk/getmondo/api/model/identity_verification/IdentityDocumentType;->b()Z

    move-result v3

    .line 56
    if-eqz v0, :cond_1

    if-nez v2, :cond_2

    :cond_1
    if-eqz v0, :cond_5

    if-nez v3, :cond_5

    .line 57
    :cond_2
    new-instance v0, Lco/uk/getmondo/signup/identity_verification/a/a/b;

    iget-object v2, p0, Lco/uk/getmondo/signup/identity_verification/fallback/b$b;->a:Lco/uk/getmondo/signup/identity_verification/fallback/b;

    invoke-static {v2}, Lco/uk/getmondo/signup/identity_verification/fallback/b;->a(Lco/uk/getmondo/signup/identity_verification/fallback/b;)Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_3

    invoke-static {}, Lkotlin/d/b/l;->a()V

    :cond_3
    iget-object v3, p0, Lco/uk/getmondo/signup/identity_verification/fallback/b$b;->a:Lco/uk/getmondo/signup/identity_verification/fallback/b;

    invoke-static {v3}, Lco/uk/getmondo/signup/identity_verification/fallback/b;->c(Lco/uk/getmondo/signup/identity_verification/fallback/b;)Lco/uk/getmondo/api/model/identity_verification/IdentityDocumentType;

    move-result-object v3

    iget-object v4, p0, Lco/uk/getmondo/signup/identity_verification/fallback/b$b;->a:Lco/uk/getmondo/signup/identity_verification/fallback/b;

    invoke-static {v4}, Lco/uk/getmondo/signup/identity_verification/fallback/b;->d(Lco/uk/getmondo/signup/identity_verification/fallback/b;)Lco/uk/getmondo/d/i;

    move-result-object v4

    iget-object v5, p0, Lco/uk/getmondo/signup/identity_verification/fallback/b$b;->a:Lco/uk/getmondo/signup/identity_verification/fallback/b;

    invoke-static {v5}, Lco/uk/getmondo/signup/identity_verification/fallback/b;->b(Lco/uk/getmondo/signup/identity_verification/fallback/b;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v0, v2, v3, v4, v5}, Lco/uk/getmondo/signup/identity_verification/a/a/b;-><init>(Ljava/lang/String;Lco/uk/getmondo/api/model/identity_verification/IdentityDocumentType;Lco/uk/getmondo/d/i;Ljava/lang/String;)V

    .line 58
    iget-object v2, p0, Lco/uk/getmondo/signup/identity_verification/fallback/b$b;->a:Lco/uk/getmondo/signup/identity_verification/fallback/b;

    invoke-static {v2}, Lco/uk/getmondo/signup/identity_verification/fallback/b;->e(Lco/uk/getmondo/signup/identity_verification/fallback/b;)Lco/uk/getmondo/signup/identity_verification/a/e;

    move-result-object v2

    invoke-interface {v2, v0, v1}, Lco/uk/getmondo/signup/identity_verification/a/e;->a(Lco/uk/getmondo/signup/identity_verification/a/a/b;Z)V

    .line 59
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/fallback/b$b;->b:Lco/uk/getmondo/signup/identity_verification/fallback/b$a;

    invoke-interface {v0}, Lco/uk/getmondo/signup/identity_verification/fallback/b$a;->finish()V

    .line 69
    :goto_1
    return-void

    :cond_4
    move v0, v2

    .line 52
    goto :goto_0

    .line 60
    :cond_5
    if-eqz v0, :cond_6

    if-eqz v3, :cond_6

    if-nez v2, :cond_6

    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/fallback/b$b;->a:Lco/uk/getmondo/signup/identity_verification/fallback/b;

    invoke-static {v0}, Lco/uk/getmondo/signup/identity_verification/fallback/b;->f(Lco/uk/getmondo/signup/identity_verification/fallback/b;)Z

    move-result v0

    if-nez v0, :cond_6

    .line 61
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/fallback/b$b;->a:Lco/uk/getmondo/signup/identity_verification/fallback/b;

    iget-object v2, p0, Lco/uk/getmondo/signup/identity_verification/fallback/b$b;->b:Lco/uk/getmondo/signup/identity_verification/fallback/b$a;

    invoke-static {v0, v2}, Lco/uk/getmondo/signup/identity_verification/fallback/b;->a(Lco/uk/getmondo/signup/identity_verification/fallback/b;Lco/uk/getmondo/signup/identity_verification/fallback/b$a;)V

    .line 62
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/fallback/b$b;->b:Lco/uk/getmondo/signup/identity_verification/fallback/b$a;

    invoke-interface {v0}, Lco/uk/getmondo/signup/identity_verification/fallback/b$a;->g()V

    .line 63
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/fallback/b$b;->a:Lco/uk/getmondo/signup/identity_verification/fallback/b;

    invoke-static {v0, v1}, Lco/uk/getmondo/signup/identity_verification/fallback/b;->a(Lco/uk/getmondo/signup/identity_verification/fallback/b;Z)V

    goto :goto_1

    .line 65
    :cond_6
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/fallback/b$b;->b:Lco/uk/getmondo/signup/identity_verification/fallback/b$a;

    invoke-interface {v0}, Lco/uk/getmondo/signup/identity_verification/fallback/b$a;->f()V

    goto :goto_1

    .line 68
    :cond_7
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/fallback/b$b;->b:Lco/uk/getmondo/signup/identity_verification/fallback/b$a;

    invoke-interface {v0}, Lco/uk/getmondo/signup/identity_verification/fallback/b$a;->e()V

    goto :goto_1
.end method

.method public bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 27
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Lco/uk/getmondo/signup/identity_verification/fallback/b$b;->a(Ljava/lang/Boolean;)V

    return-void
.end method
