.class final Lco/uk/getmondo/signup/identity_verification/fallback/b$e;
.super Ljava/lang/Object;
.source "DocumentFallbackPresenter.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lco/uk/getmondo/signup/identity_verification/fallback/b;->a(Lco/uk/getmondo/signup/identity_verification/fallback/b$a;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h",
        "<TT;",
        "Lio/reactivex/r",
        "<+TR;>;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000\u0014\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a&\u0012\u000c\u0012\n \u0003*\u0004\u0018\u00010\u00020\u0002 \u0003*\u0012\u0012\u000c\u0012\n \u0003*\u0004\u0018\u00010\u00020\u0002\u0018\u00010\u00010\u00012\u0006\u0010\u0004\u001a\u00020\u0005H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "Lio/reactivex/Observable;",
        "Landroid/net/Uri;",
        "kotlin.jvm.PlatformType",
        "bitmap",
        "Landroid/graphics/Bitmap;",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field final synthetic a:Lco/uk/getmondo/signup/identity_verification/fallback/b;

.field final synthetic b:Lco/uk/getmondo/signup/identity_verification/fallback/b$a;


# direct methods
.method constructor <init>(Lco/uk/getmondo/signup/identity_verification/fallback/b;Lco/uk/getmondo/signup/identity_verification/fallback/b$a;)V
    .locals 0

    iput-object p1, p0, Lco/uk/getmondo/signup/identity_verification/fallback/b$e;->a:Lco/uk/getmondo/signup/identity_verification/fallback/b;

    iput-object p2, p0, Lco/uk/getmondo/signup/identity_verification/fallback/b$e;->b:Lco/uk/getmondo/signup/identity_verification/fallback/b$a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/graphics/Bitmap;)Lio/reactivex/n;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/graphics/Bitmap;",
            ")",
            "Lio/reactivex/n",
            "<",
            "Landroid/net/Uri;",
            ">;"
        }
    .end annotation

    .prologue
    const-string v0, "bitmap"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 74
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/fallback/b$e;->b:Lco/uk/getmondo/signup/identity_verification/fallback/b$a;

    iget-object v1, p0, Lco/uk/getmondo/signup/identity_verification/fallback/b$e;->a:Lco/uk/getmondo/signup/identity_verification/fallback/b;

    invoke-static {v1}, Lco/uk/getmondo/signup/identity_verification/fallback/b;->i(Lco/uk/getmondo/signup/identity_verification/fallback/b;)Z

    move-result v1

    invoke-interface {v0, p1, v1}, Lco/uk/getmondo/signup/identity_verification/fallback/b$a;->a(Landroid/graphics/Bitmap;Z)Lio/reactivex/n;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/signup/identity_verification/fallback/b$e;->a:Lco/uk/getmondo/signup/identity_verification/fallback/b;

    invoke-static {v1}, Lco/uk/getmondo/signup/identity_verification/fallback/b;->h(Lco/uk/getmondo/signup/identity_verification/fallback/b;)Lio/reactivex/u;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/n;->subscribeOn(Lio/reactivex/u;)Lio/reactivex/n;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 27
    check-cast p1, Landroid/graphics/Bitmap;

    invoke-virtual {p0, p1}, Lco/uk/getmondo/signup/identity_verification/fallback/b$e;->a(Landroid/graphics/Bitmap;)Lio/reactivex/n;

    move-result-object v0

    return-object v0
.end method
