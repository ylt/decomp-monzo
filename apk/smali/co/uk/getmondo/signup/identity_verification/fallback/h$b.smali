.class final Lco/uk/getmondo/signup/identity_verification/fallback/h$b;
.super Ljava/lang/Object;
.source "VideoFallbackPresenter.kt"

# interfaces
.implements Lio/reactivex/c/g;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lco/uk/getmondo/signup/identity_verification/fallback/h;->a(Lco/uk/getmondo/signup/identity_verification/fallback/h$a;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/g",
        "<",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0003\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0004\u0008\u0005\u0010\u0006"
    }
    d2 = {
        "<anonymous>",
        "",
        "hasCameraPermission",
        "",
        "kotlin.jvm.PlatformType",
        "accept",
        "(Ljava/lang/Boolean;)V"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field final synthetic a:Lco/uk/getmondo/signup/identity_verification/fallback/h;

.field final synthetic b:Lco/uk/getmondo/signup/identity_verification/fallback/h$a;


# direct methods
.method constructor <init>(Lco/uk/getmondo/signup/identity_verification/fallback/h;Lco/uk/getmondo/signup/identity_verification/fallback/h$a;)V
    .locals 0

    iput-object p1, p0, Lco/uk/getmondo/signup/identity_verification/fallback/h$b;->a:Lco/uk/getmondo/signup/identity_verification/fallback/h;

    iput-object p2, p0, Lco/uk/getmondo/signup/identity_verification/fallback/h$b;->b:Lco/uk/getmondo/signup/identity_verification/fallback/h$a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Boolean;)V
    .locals 3

    .prologue
    .line 35
    const-string v0, "hasCameraPermission"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 36
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/fallback/h$b;->a:Lco/uk/getmondo/signup/identity_verification/fallback/h;

    invoke-static {v0}, Lco/uk/getmondo/signup/identity_verification/fallback/h;->a(Lco/uk/getmondo/signup/identity_verification/fallback/h;)Ljava/lang/String;

    move-result-object v0

    .line 37
    if-eqz v0, :cond_0

    .line 38
    iget-object v1, p0, Lco/uk/getmondo/signup/identity_verification/fallback/h$b;->a:Lco/uk/getmondo/signup/identity_verification/fallback/h;

    invoke-static {v1}, Lco/uk/getmondo/signup/identity_verification/fallback/h;->b(Lco/uk/getmondo/signup/identity_verification/fallback/h;)Lco/uk/getmondo/signup/identity_verification/a/e;

    move-result-object v1

    const/4 v2, 0x1

    invoke-interface {v1, v0, v2}, Lco/uk/getmondo/signup/identity_verification/a/e;->a(Ljava/lang/String;Z)V

    .line 39
    iget-object v1, p0, Lco/uk/getmondo/signup/identity_verification/fallback/h$b;->b:Lco/uk/getmondo/signup/identity_verification/fallback/h$a;

    invoke-interface {v1, v0}, Lco/uk/getmondo/signup/identity_verification/fallback/h$a;->a(Ljava/lang/String;)V

    .line 40
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/fallback/h$b;->b:Lco/uk/getmondo/signup/identity_verification/fallback/h$a;

    invoke-interface {v0}, Lco/uk/getmondo/signup/identity_verification/fallback/h$a;->finish()V

    .line 46
    :goto_0
    return-void

    .line 42
    :cond_0
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/fallback/h$b;->b:Lco/uk/getmondo/signup/identity_verification/fallback/h$a;

    invoke-interface {v0}, Lco/uk/getmondo/signup/identity_verification/fallback/h$a;->g()V

    goto :goto_0

    .line 45
    :cond_1
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/fallback/h$b;->b:Lco/uk/getmondo/signup/identity_verification/fallback/h$a;

    invoke-interface {v0}, Lco/uk/getmondo/signup/identity_verification/fallback/h$a;->f()V

    goto :goto_0
.end method

.method public bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 19
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Lco/uk/getmondo/signup/identity_verification/fallback/h$b;->a(Ljava/lang/Boolean;)V

    return-void
.end method
