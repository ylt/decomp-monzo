.class public final Lco/uk/getmondo/signup/identity_verification/fallback/d;
.super Ljava/lang/Object;
.source "DocumentFallbackPresenter_Factory.java"

# interfaces
.implements Lb/a/b;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lb/a/b",
        "<",
        "Lco/uk/getmondo/signup/identity_verification/fallback/b;",
        ">;"
    }
.end annotation


# static fields
.field static final synthetic a:Z


# instance fields
.field private final b:Lb/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lb/a",
            "<",
            "Lco/uk/getmondo/signup/identity_verification/fallback/b;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Ljavax/a/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/a/a",
            "<",
            "Lio/reactivex/u;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Ljavax/a/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/a/a",
            "<",
            "Lio/reactivex/u;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Ljavax/a/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/d/i;",
            ">;"
        }
    .end annotation
.end field

.field private final f:Ljavax/a/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/api/model/identity_verification/IdentityDocumentType;",
            ">;"
        }
    .end annotation
.end field

.field private final g:Ljavax/a/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/signup/identity_verification/a/e;",
            ">;"
        }
    .end annotation
.end field

.field private final h:Ljavax/a/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/signup/identity_verification/id_picture/u;",
            ">;"
        }
    .end annotation
.end field

.field private final i:Ljavax/a/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/signup/identity_verification/fallback/e;",
            ">;"
        }
    .end annotation
.end field

.field private final j:Ljavax/a/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/common/a;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 15
    const-class v0, Lco/uk/getmondo/signup/identity_verification/fallback/d;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lco/uk/getmondo/signup/identity_verification/fallback/d;->a:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lb/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lb/a",
            "<",
            "Lco/uk/getmondo/signup/identity_verification/fallback/b;",
            ">;",
            "Ljavax/a/a",
            "<",
            "Lio/reactivex/u;",
            ">;",
            "Ljavax/a/a",
            "<",
            "Lio/reactivex/u;",
            ">;",
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/d/i;",
            ">;",
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/api/model/identity_verification/IdentityDocumentType;",
            ">;",
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/signup/identity_verification/a/e;",
            ">;",
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/signup/identity_verification/id_picture/u;",
            ">;",
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/signup/identity_verification/fallback/e;",
            ">;",
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/common/a;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    sget-boolean v0, Lco/uk/getmondo/signup/identity_verification/fallback/d;->a:Z

    if-nez v0, :cond_0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 49
    :cond_0
    iput-object p1, p0, Lco/uk/getmondo/signup/identity_verification/fallback/d;->b:Lb/a;

    .line 50
    sget-boolean v0, Lco/uk/getmondo/signup/identity_verification/fallback/d;->a:Z

    if-nez v0, :cond_1

    if-nez p2, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 51
    :cond_1
    iput-object p2, p0, Lco/uk/getmondo/signup/identity_verification/fallback/d;->c:Ljavax/a/a;

    .line 52
    sget-boolean v0, Lco/uk/getmondo/signup/identity_verification/fallback/d;->a:Z

    if-nez v0, :cond_2

    if-nez p3, :cond_2

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 53
    :cond_2
    iput-object p3, p0, Lco/uk/getmondo/signup/identity_verification/fallback/d;->d:Ljavax/a/a;

    .line 54
    sget-boolean v0, Lco/uk/getmondo/signup/identity_verification/fallback/d;->a:Z

    if-nez v0, :cond_3

    if-nez p4, :cond_3

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 55
    :cond_3
    iput-object p4, p0, Lco/uk/getmondo/signup/identity_verification/fallback/d;->e:Ljavax/a/a;

    .line 56
    sget-boolean v0, Lco/uk/getmondo/signup/identity_verification/fallback/d;->a:Z

    if-nez v0, :cond_4

    if-nez p5, :cond_4

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 57
    :cond_4
    iput-object p5, p0, Lco/uk/getmondo/signup/identity_verification/fallback/d;->f:Ljavax/a/a;

    .line 58
    sget-boolean v0, Lco/uk/getmondo/signup/identity_verification/fallback/d;->a:Z

    if-nez v0, :cond_5

    if-nez p6, :cond_5

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 59
    :cond_5
    iput-object p6, p0, Lco/uk/getmondo/signup/identity_verification/fallback/d;->g:Ljavax/a/a;

    .line 60
    sget-boolean v0, Lco/uk/getmondo/signup/identity_verification/fallback/d;->a:Z

    if-nez v0, :cond_6

    if-nez p7, :cond_6

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 61
    :cond_6
    iput-object p7, p0, Lco/uk/getmondo/signup/identity_verification/fallback/d;->h:Ljavax/a/a;

    .line 62
    sget-boolean v0, Lco/uk/getmondo/signup/identity_verification/fallback/d;->a:Z

    if-nez v0, :cond_7

    if-nez p8, :cond_7

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 63
    :cond_7
    iput-object p8, p0, Lco/uk/getmondo/signup/identity_verification/fallback/d;->i:Ljavax/a/a;

    .line 64
    sget-boolean v0, Lco/uk/getmondo/signup/identity_verification/fallback/d;->a:Z

    if-nez v0, :cond_8

    if-nez p9, :cond_8

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 65
    :cond_8
    iput-object p9, p0, Lco/uk/getmondo/signup/identity_verification/fallback/d;->j:Ljavax/a/a;

    .line 66
    return-void
.end method

.method public static a(Lb/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;)Lb/a/b;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lb/a",
            "<",
            "Lco/uk/getmondo/signup/identity_verification/fallback/b;",
            ">;",
            "Ljavax/a/a",
            "<",
            "Lio/reactivex/u;",
            ">;",
            "Ljavax/a/a",
            "<",
            "Lio/reactivex/u;",
            ">;",
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/d/i;",
            ">;",
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/api/model/identity_verification/IdentityDocumentType;",
            ">;",
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/signup/identity_verification/a/e;",
            ">;",
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/signup/identity_verification/id_picture/u;",
            ">;",
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/signup/identity_verification/fallback/e;",
            ">;",
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/common/a;",
            ">;)",
            "Lb/a/b",
            "<",
            "Lco/uk/getmondo/signup/identity_verification/fallback/b;",
            ">;"
        }
    .end annotation

    .prologue
    .line 93
    new-instance v0, Lco/uk/getmondo/signup/identity_verification/fallback/d;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    invoke-direct/range {v0 .. v9}, Lco/uk/getmondo/signup/identity_verification/fallback/d;-><init>(Lb/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;)V

    return-object v0
.end method


# virtual methods
.method public a()Lco/uk/getmondo/signup/identity_verification/fallback/b;
    .locals 10

    .prologue
    .line 70
    iget-object v9, p0, Lco/uk/getmondo/signup/identity_verification/fallback/d;->b:Lb/a;

    new-instance v0, Lco/uk/getmondo/signup/identity_verification/fallback/b;

    iget-object v1, p0, Lco/uk/getmondo/signup/identity_verification/fallback/d;->c:Ljavax/a/a;

    .line 73
    invoke-interface {v1}, Ljavax/a/a;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lio/reactivex/u;

    iget-object v2, p0, Lco/uk/getmondo/signup/identity_verification/fallback/d;->d:Ljavax/a/a;

    .line 74
    invoke-interface {v2}, Ljavax/a/a;->b()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lio/reactivex/u;

    iget-object v3, p0, Lco/uk/getmondo/signup/identity_verification/fallback/d;->e:Ljavax/a/a;

    .line 75
    invoke-interface {v3}, Ljavax/a/a;->b()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lco/uk/getmondo/d/i;

    iget-object v4, p0, Lco/uk/getmondo/signup/identity_verification/fallback/d;->f:Ljavax/a/a;

    .line 76
    invoke-interface {v4}, Ljavax/a/a;->b()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lco/uk/getmondo/api/model/identity_verification/IdentityDocumentType;

    iget-object v5, p0, Lco/uk/getmondo/signup/identity_verification/fallback/d;->g:Ljavax/a/a;

    .line 77
    invoke-interface {v5}, Ljavax/a/a;->b()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lco/uk/getmondo/signup/identity_verification/a/e;

    iget-object v6, p0, Lco/uk/getmondo/signup/identity_verification/fallback/d;->h:Ljavax/a/a;

    .line 78
    invoke-interface {v6}, Ljavax/a/a;->b()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lco/uk/getmondo/signup/identity_verification/id_picture/u;

    iget-object v7, p0, Lco/uk/getmondo/signup/identity_verification/fallback/d;->i:Ljavax/a/a;

    .line 79
    invoke-interface {v7}, Ljavax/a/a;->b()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lco/uk/getmondo/signup/identity_verification/fallback/e;

    iget-object v8, p0, Lco/uk/getmondo/signup/identity_verification/fallback/d;->j:Ljavax/a/a;

    .line 80
    invoke-interface {v8}, Ljavax/a/a;->b()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lco/uk/getmondo/common/a;

    invoke-direct/range {v0 .. v8}, Lco/uk/getmondo/signup/identity_verification/fallback/b;-><init>(Lio/reactivex/u;Lio/reactivex/u;Lco/uk/getmondo/d/i;Lco/uk/getmondo/api/model/identity_verification/IdentityDocumentType;Lco/uk/getmondo/signup/identity_verification/a/e;Lco/uk/getmondo/signup/identity_verification/id_picture/u;Lco/uk/getmondo/signup/identity_verification/fallback/e;Lco/uk/getmondo/common/a;)V

    .line 70
    invoke-static {v9, v0}, Lb/a/c;->a(Lb/a;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/signup/identity_verification/fallback/b;

    return-object v0
.end method

.method public synthetic b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 15
    invoke-virtual {p0}, Lco/uk/getmondo/signup/identity_verification/fallback/d;->a()Lco/uk/getmondo/signup/identity_verification/fallback/b;

    move-result-object v0

    return-object v0
.end method
