.class public final Lco/uk/getmondo/signup/identity_verification/fallback/e;
.super Ljava/lang/Object;
.source "DocumentFallbackStringProvider.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u00000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0011\u0008\u0007\u0012\u0008\u0008\u0001\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0016\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u00082\u0006\u0010\t\u001a\u00020\nJ\u0006\u0010\u000b\u001a\u00020\u0006J\"\u0010\u000c\u001a\u000e\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\u000e0\r2\u0006\u0010\u0007\u001a\u00020\u00082\u0006\u0010\t\u001a\u00020\nR\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000f"
    }
    d2 = {
        "Lco/uk/getmondo/signup/identity_verification/fallback/DocumentFallbackStringProvider;",
        "",
        "context",
        "Landroid/content/Context;",
        "(Landroid/content/Context;)V",
        "confirmationId",
        "",
        "documentType",
        "Lco/uk/getmondo/api/model/identity_verification/IdentityDocumentType;",
        "isFront",
        "",
        "confirmationPassport",
        "getInstructions",
        "Lkotlin/Pair;",
        "Landroid/graphics/drawable/Drawable;",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field private final a:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lco/uk/getmondo/signup/identity_verification/fallback/e;->a:Landroid/content/Context;

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 2

    .prologue
    .line 27
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/fallback/e;->a:Landroid/content/Context;

    const v1, 0x7f0a0227

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "context.getString(R.stri\u2026allback_confirm_passport)"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public final a(Lco/uk/getmondo/api/model/identity_verification/IdentityDocumentType;Z)Lkotlin/h;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lco/uk/getmondo/api/model/identity_verification/IdentityDocumentType;",
            "Z)",
            "Lkotlin/h",
            "<",
            "Ljava/lang/String;",
            "Landroid/graphics/drawable/Drawable;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    const-string v0, "documentType"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 15
    sget-object v0, Lco/uk/getmondo/api/model/identity_verification/IdentityDocumentType;->PASSPORT:Lco/uk/getmondo/api/model/identity_verification/IdentityDocumentType;

    invoke-static {p1, v0}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 16
    new-instance v0, Lkotlin/h;

    iget-object v1, p0, Lco/uk/getmondo/signup/identity_verification/fallback/e;->a:Landroid/content/Context;

    const v2, 0x7f0a022a

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lco/uk/getmondo/signup/identity_verification/fallback/e;->a:Landroid/content/Context;

    const v3, 0x7f0201c9

    invoke-virtual {v2, v3}, Landroid/content/Context;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lkotlin/h;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 23
    :goto_0
    return-object v0

    .line 19
    :cond_0
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/fallback/e;->a:Landroid/content/Context;

    invoke-virtual {p1}, Lco/uk/getmondo/api/model/identity_verification/IdentityDocumentType;->c()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 20
    if-eqz p2, :cond_1

    .line 21
    new-instance v0, Lkotlin/h;

    iget-object v2, p0, Lco/uk/getmondo/signup/identity_verification/fallback/e;->a:Landroid/content/Context;

    const v3, 0x7f0a0229

    new-array v4, v4, [Ljava/lang/Object;

    aput-object v1, v4, v5

    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lco/uk/getmondo/signup/identity_verification/fallback/e;->a:Landroid/content/Context;

    const v3, 0x7f0201c8

    invoke-virtual {v2, v3}, Landroid/content/Context;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lkotlin/h;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_0

    .line 23
    :cond_1
    new-instance v0, Lkotlin/h;

    iget-object v2, p0, Lco/uk/getmondo/signup/identity_verification/fallback/e;->a:Landroid/content/Context;

    const v3, 0x7f0a0228

    new-array v4, v4, [Ljava/lang/Object;

    aput-object v1, v4, v5

    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lco/uk/getmondo/signup/identity_verification/fallback/e;->a:Landroid/content/Context;

    const v3, 0x7f0201c7

    invoke-virtual {v2, v3}, Landroid/content/Context;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lkotlin/h;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public final b(Lco/uk/getmondo/api/model/identity_verification/IdentityDocumentType;Z)Ljava/lang/String;
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    const-string v0, "documentType"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 31
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/fallback/e;->a:Landroid/content/Context;

    invoke-virtual {p1}, Lco/uk/getmondo/api/model/identity_verification/IdentityDocumentType;->c()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 32
    if-eqz p2, :cond_0

    .line 33
    iget-object v1, p0, Lco/uk/getmondo/signup/identity_verification/fallback/e;->a:Landroid/content/Context;

    const v2, 0x7f0a0226

    new-array v3, v3, [Ljava/lang/Object;

    aput-object v0, v3, v4

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "context.getString(R.stri\u2026_id_front, idDisplayName)"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 35
    :goto_0
    return-object v0

    :cond_0
    iget-object v1, p0, Lco/uk/getmondo/signup/identity_verification/fallback/e;->a:Landroid/content/Context;

    const v2, 0x7f0a0225

    new-array v3, v3, [Ljava/lang/Object;

    aput-object v0, v3, v4

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "context.getString(R.stri\u2026m_id_back, idDisplayName)"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0
.end method
