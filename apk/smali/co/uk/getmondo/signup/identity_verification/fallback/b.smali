.class public final Lco/uk/getmondo/signup/identity_verification/fallback/b;
.super Lco/uk/getmondo/common/ui/b;
.source "DocumentFallbackPresenter.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lco/uk/getmondo/signup/identity_verification/fallback/b$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lco/uk/getmondo/common/ui/b",
        "<",
        "Lco/uk/getmondo/signup/identity_verification/fallback/b$a;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000T\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u0004\u0008\u0007\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00020\u0001:\u0001\u001dBK\u0008\u0007\u0012\u0008\u0008\u0001\u0010\u0003\u001a\u00020\u0004\u0012\u0008\u0008\u0001\u0010\u0005\u001a\u00020\u0004\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b\u0012\u0006\u0010\u000c\u001a\u00020\r\u0012\u0006\u0010\u000e\u001a\u00020\u000f\u0012\u0006\u0010\u0010\u001a\u00020\u0011\u00a2\u0006\u0002\u0010\u0012J\u0008\u0010\u0018\u001a\u00020\u0017H\u0002J\u0010\u0010\u0019\u001a\u00020\u001a2\u0006\u0010\u001b\u001a\u00020\u0002H\u0016J\u0010\u0010\u001c\u001a\u00020\u001a2\u0006\u0010\u001b\u001a\u00020\u0002H\u0002R\u000e\u0010\u0010\u001a\u00020\u0011X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0013\u001a\u0004\u0018\u00010\u0014X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0015\u001a\u0004\u0018\u00010\u0014X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000c\u001a\u00020\rX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0016\u001a\u00020\u0017X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u000fX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001e"
    }
    d2 = {
        "Lco/uk/getmondo/signup/identity_verification/fallback/DocumentFallbackPresenter;",
        "Lco/uk/getmondo/common/ui/BasePresenter;",
        "Lco/uk/getmondo/signup/identity_verification/fallback/DocumentFallbackPresenter$View;",
        "computationScheduler",
        "Lio/reactivex/Scheduler;",
        "uiScheduler",
        "country",
        "Lco/uk/getmondo/model/Country;",
        "documentType",
        "Lco/uk/getmondo/api/model/identity_verification/IdentityDocumentType;",
        "verificationManager",
        "Lco/uk/getmondo/signup/identity_verification/data/IdentityVerificationManager;",
        "imageResizer",
        "Lco/uk/getmondo/signup/identity_verification/id_picture/DocumentImageResizer;",
        "stringProvider",
        "Lco/uk/getmondo/signup/identity_verification/fallback/DocumentFallbackStringProvider;",
        "analyticsService",
        "Lco/uk/getmondo/common/AnalyticsService;",
        "(Lio/reactivex/Scheduler;Lio/reactivex/Scheduler;Lco/uk/getmondo/model/Country;Lco/uk/getmondo/api/model/identity_verification/IdentityDocumentType;Lco/uk/getmondo/signup/identity_verification/data/IdentityVerificationManager;Lco/uk/getmondo/signup/identity_verification/id_picture/DocumentImageResizer;Lco/uk/getmondo/signup/identity_verification/fallback/DocumentFallbackStringProvider;Lco/uk/getmondo/common/AnalyticsService;)V",
        "backPhotoPath",
        "",
        "frontPhotoPath",
        "shownBackInstructions",
        "",
        "isFront",
        "register",
        "",
        "view",
        "showInstructions",
        "View",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field private c:Ljava/lang/String;

.field private d:Ljava/lang/String;

.field private e:Z

.field private final f:Lio/reactivex/u;

.field private final g:Lio/reactivex/u;

.field private final h:Lco/uk/getmondo/d/i;

.field private final i:Lco/uk/getmondo/api/model/identity_verification/IdentityDocumentType;

.field private final j:Lco/uk/getmondo/signup/identity_verification/a/e;

.field private final k:Lco/uk/getmondo/signup/identity_verification/id_picture/u;

.field private final l:Lco/uk/getmondo/signup/identity_verification/fallback/e;

.field private final m:Lco/uk/getmondo/common/a;


# direct methods
.method public constructor <init>(Lio/reactivex/u;Lio/reactivex/u;Lco/uk/getmondo/d/i;Lco/uk/getmondo/api/model/identity_verification/IdentityDocumentType;Lco/uk/getmondo/signup/identity_verification/a/e;Lco/uk/getmondo/signup/identity_verification/id_picture/u;Lco/uk/getmondo/signup/identity_verification/fallback/e;Lco/uk/getmondo/common/a;)V
    .locals 1

    .prologue
    const-string v0, "computationScheduler"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "uiScheduler"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "country"

    invoke-static {p3, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "documentType"

    invoke-static {p4, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "verificationManager"

    invoke-static {p5, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "imageResizer"

    invoke-static {p6, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "stringProvider"

    invoke-static {p7, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "analyticsService"

    invoke-static {p8, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 37
    invoke-direct {p0}, Lco/uk/getmondo/common/ui/b;-><init>()V

    iput-object p1, p0, Lco/uk/getmondo/signup/identity_verification/fallback/b;->f:Lio/reactivex/u;

    iput-object p2, p0, Lco/uk/getmondo/signup/identity_verification/fallback/b;->g:Lio/reactivex/u;

    iput-object p3, p0, Lco/uk/getmondo/signup/identity_verification/fallback/b;->h:Lco/uk/getmondo/d/i;

    iput-object p4, p0, Lco/uk/getmondo/signup/identity_verification/fallback/b;->i:Lco/uk/getmondo/api/model/identity_verification/IdentityDocumentType;

    iput-object p5, p0, Lco/uk/getmondo/signup/identity_verification/fallback/b;->j:Lco/uk/getmondo/signup/identity_verification/a/e;

    iput-object p6, p0, Lco/uk/getmondo/signup/identity_verification/fallback/b;->k:Lco/uk/getmondo/signup/identity_verification/id_picture/u;

    iput-object p7, p0, Lco/uk/getmondo/signup/identity_verification/fallback/b;->l:Lco/uk/getmondo/signup/identity_verification/fallback/e;

    iput-object p8, p0, Lco/uk/getmondo/signup/identity_verification/fallback/b;->m:Lco/uk/getmondo/common/a;

    return-void
.end method

.method public static final synthetic a(Lco/uk/getmondo/signup/identity_verification/fallback/b;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/fallback/b;->c:Ljava/lang/String;

    return-object v0
.end method

.method public static final synthetic a(Lco/uk/getmondo/signup/identity_verification/fallback/b;Lco/uk/getmondo/signup/identity_verification/fallback/b$a;)V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0, p1}, Lco/uk/getmondo/signup/identity_verification/fallback/b;->b(Lco/uk/getmondo/signup/identity_verification/fallback/b$a;)V

    return-void
.end method

.method public static final synthetic a(Lco/uk/getmondo/signup/identity_verification/fallback/b;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 27
    iput-object p1, p0, Lco/uk/getmondo/signup/identity_verification/fallback/b;->c:Ljava/lang/String;

    return-void
.end method

.method public static final synthetic a(Lco/uk/getmondo/signup/identity_verification/fallback/b;Z)V
    .locals 0

    .prologue
    .line 27
    iput-boolean p1, p0, Lco/uk/getmondo/signup/identity_verification/fallback/b;->e:Z

    return-void
.end method

.method private final a()Z
    .locals 1

    .prologue
    .line 116
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/fallback/b;->i:Lco/uk/getmondo/api/model/identity_verification/IdentityDocumentType;

    invoke-virtual {v0}, Lco/uk/getmondo/api/model/identity_verification/IdentityDocumentType;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/fallback/b;->c:Ljava/lang/String;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static final synthetic b(Lco/uk/getmondo/signup/identity_verification/fallback/b;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/fallback/b;->d:Ljava/lang/String;

    return-object v0
.end method

.method private final b(Lco/uk/getmondo/signup/identity_verification/fallback/b$a;)V
    .locals 3

    .prologue
    .line 112
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/fallback/b;->l:Lco/uk/getmondo/signup/identity_verification/fallback/e;

    iget-object v1, p0, Lco/uk/getmondo/signup/identity_verification/fallback/b;->i:Lco/uk/getmondo/api/model/identity_verification/IdentityDocumentType;

    invoke-direct {p0}, Lco/uk/getmondo/signup/identity_verification/fallback/b;->a()Z

    move-result v2

    invoke-virtual {v0, v1, v2}, Lco/uk/getmondo/signup/identity_verification/fallback/e;->a(Lco/uk/getmondo/api/model/identity_verification/IdentityDocumentType;Z)Lkotlin/h;

    move-result-object v1

    invoke-virtual {v1}, Lkotlin/h;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1}, Lkotlin/h;->d()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/graphics/drawable/Drawable;

    .line 113
    invoke-interface {p1, v0, v1}, Lco/uk/getmondo/signup/identity_verification/fallback/b$a;->a(Ljava/lang/String;Landroid/graphics/drawable/Drawable;)V

    .line 114
    return-void
.end method

.method public static final synthetic b(Lco/uk/getmondo/signup/identity_verification/fallback/b;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 27
    iput-object p1, p0, Lco/uk/getmondo/signup/identity_verification/fallback/b;->d:Ljava/lang/String;

    return-void
.end method

.method public static final synthetic c(Lco/uk/getmondo/signup/identity_verification/fallback/b;)Lco/uk/getmondo/api/model/identity_verification/IdentityDocumentType;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/fallback/b;->i:Lco/uk/getmondo/api/model/identity_verification/IdentityDocumentType;

    return-object v0
.end method

.method public static final synthetic d(Lco/uk/getmondo/signup/identity_verification/fallback/b;)Lco/uk/getmondo/d/i;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/fallback/b;->h:Lco/uk/getmondo/d/i;

    return-object v0
.end method

.method public static final synthetic e(Lco/uk/getmondo/signup/identity_verification/fallback/b;)Lco/uk/getmondo/signup/identity_verification/a/e;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/fallback/b;->j:Lco/uk/getmondo/signup/identity_verification/a/e;

    return-object v0
.end method

.method public static final synthetic f(Lco/uk/getmondo/signup/identity_verification/fallback/b;)Z
    .locals 1

    .prologue
    .line 27
    iget-boolean v0, p0, Lco/uk/getmondo/signup/identity_verification/fallback/b;->e:Z

    return v0
.end method

.method public static final synthetic g(Lco/uk/getmondo/signup/identity_verification/fallback/b;)Lco/uk/getmondo/signup/identity_verification/id_picture/u;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/fallback/b;->k:Lco/uk/getmondo/signup/identity_verification/id_picture/u;

    return-object v0
.end method

.method public static final synthetic h(Lco/uk/getmondo/signup/identity_verification/fallback/b;)Lio/reactivex/u;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/fallback/b;->f:Lio/reactivex/u;

    return-object v0
.end method

.method public static final synthetic i(Lco/uk/getmondo/signup/identity_verification/fallback/b;)Z
    .locals 1

    .prologue
    .line 27
    invoke-direct {p0}, Lco/uk/getmondo/signup/identity_verification/fallback/b;->a()Z

    move-result v0

    return v0
.end method

.method public static final synthetic j(Lco/uk/getmondo/signup/identity_verification/fallback/b;)Lco/uk/getmondo/signup/identity_verification/fallback/e;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/fallback/b;->l:Lco/uk/getmondo/signup/identity_verification/fallback/e;

    return-object v0
.end method


# virtual methods
.method public bridge synthetic a(Lco/uk/getmondo/common/ui/f;)V
    .locals 0

    .prologue
    .line 27
    check-cast p1, Lco/uk/getmondo/signup/identity_verification/fallback/b$a;

    invoke-virtual {p0, p1}, Lco/uk/getmondo/signup/identity_verification/fallback/b;->a(Lco/uk/getmondo/signup/identity_verification/fallback/b$a;)V

    return-void
.end method

.method public a(Lco/uk/getmondo/signup/identity_verification/fallback/b$a;)V
    .locals 5

    .prologue
    const-string v0, "view"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v0, p1

    .line 44
    check-cast v0, Lco/uk/getmondo/common/ui/f;

    invoke-super {p0, v0}, Lco/uk/getmondo/common/ui/b;->a(Lco/uk/getmondo/common/ui/f;)V

    .line 46
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/fallback/b;->m:Lco/uk/getmondo/common/a;

    sget-object v1, Lco/uk/getmondo/api/model/tracking/Impression;->Companion:Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    iget-object v2, p0, Lco/uk/getmondo/signup/identity_verification/fallback/b;->i:Lco/uk/getmondo/api/model/identity_verification/IdentityDocumentType;

    invoke-virtual {v2}, Lco/uk/getmondo/api/model/identity_verification/IdentityDocumentType;->a()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->a(Ljava/lang/String;Z)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v1

    invoke-virtual {v0, v1}, Lco/uk/getmondo/common/a;->a(Lco/uk/getmondo/api/model/tracking/Impression;)V

    .line 47
    invoke-direct {p0, p1}, Lco/uk/getmondo/signup/identity_verification/fallback/b;->b(Lco/uk/getmondo/signup/identity_verification/fallback/b$a;)V

    .line 49
    iget-object v3, p0, Lco/uk/getmondo/signup/identity_verification/fallback/b;->b:Lio/reactivex/b/a;

    .line 50
    invoke-interface {p1}, Lco/uk/getmondo/signup/identity_verification/fallback/b$a;->b()Lio/reactivex/n;

    move-result-object v4

    new-instance v0, Lco/uk/getmondo/signup/identity_verification/fallback/b$b;

    invoke-direct {v0, p0, p1}, Lco/uk/getmondo/signup/identity_verification/fallback/b$b;-><init>(Lco/uk/getmondo/signup/identity_verification/fallback/b;Lco/uk/getmondo/signup/identity_verification/fallback/b$a;)V

    check-cast v0, Lio/reactivex/c/g;

    .line 70
    sget-object v1, Lco/uk/getmondo/signup/identity_verification/fallback/b$c;->a:Lco/uk/getmondo/signup/identity_verification/fallback/b$c;

    check-cast v1, Lkotlin/d/a/b;

    if-eqz v1, :cond_0

    new-instance v2, Lco/uk/getmondo/signup/identity_verification/fallback/c;

    invoke-direct {v2, v1}, Lco/uk/getmondo/signup/identity_verification/fallback/c;-><init>(Lkotlin/d/a/b;)V

    move-object v1, v2

    :cond_0
    check-cast v1, Lio/reactivex/c/g;

    .line 50
    invoke-virtual {v4, v0, v1}, Lio/reactivex/n;->subscribe(Lio/reactivex/c/g;Lio/reactivex/c/g;)Lio/reactivex/b/b;

    move-result-object v0

    const-string v1, "view.onMainActionClicked\u2026            }, Timber::e)"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 49
    invoke-static {v3, v0}, Lco/uk/getmondo/common/j/f;->a(Lio/reactivex/b/a;Lio/reactivex/b/b;)Lio/reactivex/b/a;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/signup/identity_verification/fallback/b;->b:Lio/reactivex/b/a;

    .line 72
    iget-object v3, p0, Lco/uk/getmondo/signup/identity_verification/fallback/b;->b:Lio/reactivex/b/a;

    .line 76
    invoke-interface {p1}, Lco/uk/getmondo/signup/identity_verification/fallback/b$a;->d()Lio/reactivex/n;

    move-result-object v1

    .line 73
    new-instance v0, Lco/uk/getmondo/signup/identity_verification/fallback/b$d;

    invoke-direct {v0, p0}, Lco/uk/getmondo/signup/identity_verification/fallback/b$d;-><init>(Lco/uk/getmondo/signup/identity_verification/fallback/b;)V

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {v1, v0}, Lio/reactivex/n;->concatMap(Lio/reactivex/c/h;)Lio/reactivex/n;

    move-result-object v1

    .line 74
    new-instance v0, Lco/uk/getmondo/signup/identity_verification/fallback/b$e;

    invoke-direct {v0, p0, p1}, Lco/uk/getmondo/signup/identity_verification/fallback/b$e;-><init>(Lco/uk/getmondo/signup/identity_verification/fallback/b;Lco/uk/getmondo/signup/identity_verification/fallback/b$a;)V

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {v1, v0}, Lio/reactivex/n;->concatMap(Lio/reactivex/c/h;)Lio/reactivex/n;

    move-result-object v0

    .line 75
    iget-object v1, p0, Lco/uk/getmondo/signup/identity_verification/fallback/b;->g:Lio/reactivex/u;

    invoke-virtual {v0, v1}, Lio/reactivex/n;->observeOn(Lio/reactivex/u;)Lio/reactivex/n;

    move-result-object v4

    .line 76
    new-instance v0, Lco/uk/getmondo/signup/identity_verification/fallback/b$f;

    invoke-direct {v0, p0, p1}, Lco/uk/getmondo/signup/identity_verification/fallback/b$f;-><init>(Lco/uk/getmondo/signup/identity_verification/fallback/b;Lco/uk/getmondo/signup/identity_verification/fallback/b$a;)V

    check-cast v0, Lio/reactivex/c/g;

    .line 92
    sget-object v1, Lco/uk/getmondo/signup/identity_verification/fallback/b$g;->a:Lco/uk/getmondo/signup/identity_verification/fallback/b$g;

    check-cast v1, Lkotlin/d/a/b;

    if-eqz v1, :cond_1

    new-instance v2, Lco/uk/getmondo/signup/identity_verification/fallback/c;

    invoke-direct {v2, v1}, Lco/uk/getmondo/signup/identity_verification/fallback/c;-><init>(Lkotlin/d/a/b;)V

    move-object v1, v2

    :cond_1
    check-cast v1, Lio/reactivex/c/g;

    .line 76
    invoke-virtual {v4, v0, v1}, Lio/reactivex/n;->subscribe(Lio/reactivex/c/g;Lio/reactivex/c/g;)Lio/reactivex/b/b;

    move-result-object v0

    const-string v1, "view.onPictureChanged()\n\u2026            }, Timber::e)"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 72
    invoke-static {v3, v0}, Lco/uk/getmondo/common/j/f;->a(Lio/reactivex/b/a;Lio/reactivex/b/b;)Lio/reactivex/b/a;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/signup/identity_verification/fallback/b;->b:Lio/reactivex/b/a;

    .line 94
    iget-object v3, p0, Lco/uk/getmondo/signup/identity_verification/fallback/b;->b:Lio/reactivex/b/a;

    .line 95
    invoke-interface {p1}, Lco/uk/getmondo/signup/identity_verification/fallback/b$a;->c()Lio/reactivex/n;

    move-result-object v4

    new-instance v0, Lco/uk/getmondo/signup/identity_verification/fallback/b$h;

    invoke-direct {v0, p0, p1}, Lco/uk/getmondo/signup/identity_verification/fallback/b$h;-><init>(Lco/uk/getmondo/signup/identity_verification/fallback/b;Lco/uk/getmondo/signup/identity_verification/fallback/b$a;)V

    check-cast v0, Lio/reactivex/c/g;

    .line 108
    sget-object v1, Lco/uk/getmondo/signup/identity_verification/fallback/b$i;->a:Lco/uk/getmondo/signup/identity_verification/fallback/b$i;

    check-cast v1, Lkotlin/d/a/b;

    if-eqz v1, :cond_2

    new-instance v2, Lco/uk/getmondo/signup/identity_verification/fallback/c;

    invoke-direct {v2, v1}, Lco/uk/getmondo/signup/identity_verification/fallback/c;-><init>(Lkotlin/d/a/b;)V

    move-object v1, v2

    :cond_2
    check-cast v1, Lio/reactivex/c/g;

    .line 95
    invoke-virtual {v4, v0, v1}, Lio/reactivex/n;->subscribe(Lio/reactivex/c/g;Lio/reactivex/c/g;)Lio/reactivex/b/b;

    move-result-object v0

    const-string v1, "view.onRetakeClicked()\n \u2026            }, Timber::e)"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 94
    invoke-static {v3, v0}, Lco/uk/getmondo/common/j/f;->a(Lio/reactivex/b/a;Lio/reactivex/b/b;)Lio/reactivex/b/a;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/signup/identity_verification/fallback/b;->b:Lio/reactivex/b/a;

    .line 109
    return-void
.end method
