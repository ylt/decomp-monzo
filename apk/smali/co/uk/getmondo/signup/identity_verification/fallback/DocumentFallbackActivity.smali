.class public final Lco/uk/getmondo/signup/identity_verification/fallback/DocumentFallbackActivity;
.super Lco/uk/getmondo/common/activities/b;
.source "DocumentFallbackActivity.kt"

# interfaces
.implements Lco/uk/getmondo/signup/identity_verification/fallback/b$a;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lco/uk/getmondo/signup/identity_verification/fallback/DocumentFallbackActivity$a;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000\u008c\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u0011\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0015\n\u0002\u0008\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0007\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u0000 >2\u00020\u00012\u00020\u0002:\u0001>B\u0005\u00a2\u0006\u0002\u0010\u0003J\u0008\u0010\u0017\u001a\u00020\u0018H\u0002J\"\u0010\u0019\u001a\u00020\u000f2\u0006\u0010\u001a\u001a\u00020\u001b2\u0006\u0010\u001c\u001a\u00020\u001b2\u0008\u0010\u001d\u001a\u0004\u0018\u00010\u001eH\u0014J\u0012\u0010\u001f\u001a\u00020\u000f2\u0008\u0010 \u001a\u0004\u0018\u00010!H\u0014J\u0008\u0010\"\u001a\u00020\u000fH\u0014J\u000e\u0010#\u001a\u0008\u0012\u0004\u0012\u00020\u00180$H\u0016J\u000e\u0010%\u001a\u0008\u0012\u0004\u0012\u00020\u000c0$H\u0016J+\u0010&\u001a\u00020\u000f2\u0006\u0010\u001a\u001a\u00020\u001b2\u000c\u0010\'\u001a\u0008\u0012\u0004\u0012\u00020)0(2\u0006\u0010*\u001a\u00020+H\u0016\u00a2\u0006\u0002\u0010,J\u000e\u0010-\u001a\u0008\u0012\u0004\u0012\u00020.0$H\u0016J\u0008\u0010/\u001a\u00020\u000fH\u0016J\u0008\u00100\u001a\u00020\u000fH\u0016J\u0008\u00101\u001a\u00020\u000fH\u0016J\u001e\u00102\u001a\u0008\u0012\u0004\u0012\u0002030$2\u0006\u00104\u001a\u0002052\u0006\u00106\u001a\u00020\u0018H\u0016J\u0018\u00107\u001a\u00020\u000f2\u0006\u00108\u001a\u00020)2\u0006\u00109\u001a\u000203H\u0016J\u0018\u0010:\u001a\u00020\u000f2\u0006\u0010;\u001a\u00020)2\u0006\u0010<\u001a\u00020=H\u0016R\u001e\u0010\u0004\u001a\u00020\u00058\u0006@\u0006X\u0087.\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u0006\u0010\u0007\"\u0004\u0008\u0008\u0010\tR2\u0010\n\u001a&\u0012\u000c\u0012\n \r*\u0004\u0018\u00010\u000c0\u000c \r*\u0012\u0012\u000c\u0012\n \r*\u0004\u0018\u00010\u000c0\u000c\u0018\u00010\u000b0\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R2\u0010\u000e\u001a&\u0012\u000c\u0012\n \r*\u0004\u0018\u00010\u000f0\u000f \r*\u0012\u0012\u000c\u0012\n \r*\u0004\u0018\u00010\u000f0\u000f\u0018\u00010\u000b0\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0010\u001a\u00020\u000cX\u0082.\u00a2\u0006\u0002\n\u0000R\u001e\u0010\u0011\u001a\u00020\u00128\u0006@\u0006X\u0087.\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u0013\u0010\u0014\"\u0004\u0008\u0015\u0010\u0016\u00a8\u0006?"
    }
    d2 = {
        "Lco/uk/getmondo/signup/identity_verification/fallback/DocumentFallbackActivity;",
        "Lco/uk/getmondo/common/activities/BaseActivity;",
        "Lco/uk/getmondo/signup/identity_verification/fallback/DocumentFallbackPresenter$View;",
        "()V",
        "fileGenerator",
        "Lco/uk/getmondo/signup/identity_verification/data/FileGenerator;",
        "getFileGenerator",
        "()Lco/uk/getmondo/signup/identity_verification/data/FileGenerator;",
        "setFileGenerator",
        "(Lco/uk/getmondo/signup/identity_verification/data/FileGenerator;)V",
        "imageChangedRelay",
        "Lcom/jakewharton/rxrelay2/PublishRelay;",
        "Ljava/io/File;",
        "kotlin.jvm.PlatformType",
        "permissionGrantedRelay",
        "",
        "photoFile",
        "presenter",
        "Lco/uk/getmondo/signup/identity_verification/fallback/DocumentFallbackPresenter;",
        "getPresenter",
        "()Lco/uk/getmondo/signup/identity_verification/fallback/DocumentFallbackPresenter;",
        "setPresenter",
        "(Lco/uk/getmondo/signup/identity_verification/fallback/DocumentFallbackPresenter;)V",
        "hasCameraPermission",
        "",
        "onActivityResult",
        "requestCode",
        "",
        "resultCode",
        "data",
        "Landroid/content/Intent;",
        "onCreate",
        "savedInstanceState",
        "Landroid/os/Bundle;",
        "onDestroy",
        "onMainActionClicked",
        "Lio/reactivex/Observable;",
        "onPictureChanged",
        "onRequestPermissionsResult",
        "permissions",
        "",
        "",
        "grantResults",
        "",
        "(I[Ljava/lang/String;[I)V",
        "onRetakeClicked",
        "",
        "openCamera",
        "requestCameraPermission",
        "resetActionButtons",
        "saveInFile",
        "Landroid/net/Uri;",
        "bitmap",
        "Landroid/graphics/Bitmap;",
        "isPrimaryFile",
        "showConfirmation",
        "title",
        "uri",
        "showInstructions",
        "text",
        "preview",
        "Landroid/graphics/drawable/Drawable;",
        "Companion",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# static fields
.field public static final c:Lco/uk/getmondo/signup/identity_verification/fallback/DocumentFallbackActivity$a;


# instance fields
.field public a:Lco/uk/getmondo/signup/identity_verification/fallback/b;

.field public b:Lco/uk/getmondo/signup/identity_verification/a/a;

.field private final e:Lcom/b/b/c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/b/b/c",
            "<",
            "Lkotlin/n;",
            ">;"
        }
    .end annotation
.end field

.field private final f:Lcom/b/b/c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/b/b/c",
            "<",
            "Ljava/io/File;",
            ">;"
        }
    .end annotation
.end field

.field private g:Ljava/io/File;

.field private h:Ljava/util/HashMap;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lco/uk/getmondo/signup/identity_verification/fallback/DocumentFallbackActivity$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lco/uk/getmondo/signup/identity_verification/fallback/DocumentFallbackActivity$a;-><init>(Lkotlin/d/b/i;)V

    sput-object v0, Lco/uk/getmondo/signup/identity_verification/fallback/DocumentFallbackActivity;->c:Lco/uk/getmondo/signup/identity_verification/fallback/DocumentFallbackActivity$a;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 38
    invoke-direct {p0}, Lco/uk/getmondo/common/activities/b;-><init>()V

    .line 39
    invoke-static {}, Lcom/b/b/c;->a()Lcom/b/b/c;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/signup/identity_verification/fallback/DocumentFallbackActivity;->e:Lcom/b/b/c;

    .line 40
    invoke-static {}, Lcom/b/b/c;->a()Lcom/b/b/c;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/signup/identity_verification/fallback/DocumentFallbackActivity;->f:Lcom/b/b/c;

    return-void
.end method

.method public static final synthetic a(Lco/uk/getmondo/signup/identity_verification/fallback/DocumentFallbackActivity;)Z
    .locals 1

    .prologue
    .line 38
    invoke-direct {p0}, Lco/uk/getmondo/signup/identity_verification/fallback/DocumentFallbackActivity;->h()Z

    move-result v0

    return v0
.end method

.method private final h()Z
    .locals 1

    .prologue
    .line 152
    check-cast p0, Landroid/content/Context;

    const-string v0, "android.permission.CAMERA"

    invoke-static {p0, v0}, Landroid/support/v4/content/a;->b(Landroid/content/Context;Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a(I)Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/fallback/DocumentFallbackActivity;->h:Ljava/util/HashMap;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lco/uk/getmondo/signup/identity_verification/fallback/DocumentFallbackActivity;->h:Ljava/util/HashMap;

    :cond_0
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/fallback/DocumentFallbackActivity;->h:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    if-nez v0, :cond_1

    invoke-virtual {p0, p1}, Landroid/support/v4/app/j;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/signup/identity_verification/fallback/DocumentFallbackActivity;->h:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    return-object v0
.end method

.method public final a()Lco/uk/getmondo/signup/identity_verification/a/a;
    .locals 2

    .prologue
    .line 43
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/fallback/DocumentFallbackActivity;->b:Lco/uk/getmondo/signup/identity_verification/a/a;

    if-nez v0, :cond_0

    const-string v1, "fileGenerator"

    invoke-static {v1}, Lkotlin/d/b/l;->b(Ljava/lang/String;)V

    :cond_0
    return-object v0
.end method

.method public a(Landroid/graphics/Bitmap;Z)Lio/reactivex/n;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/graphics/Bitmap;",
            "Z)",
            "Lio/reactivex/n",
            "<",
            "Landroid/net/Uri;",
            ">;"
        }
    .end annotation

    .prologue
    const-string v0, "bitmap"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 91
    new-instance v0, Lco/uk/getmondo/signup/identity_verification/fallback/DocumentFallbackActivity$c;

    invoke-direct {v0, p0, p2, p1}, Lco/uk/getmondo/signup/identity_verification/fallback/DocumentFallbackActivity$c;-><init>(Lco/uk/getmondo/signup/identity_verification/fallback/DocumentFallbackActivity;ZLandroid/graphics/Bitmap;)V

    check-cast v0, Ljava/util/concurrent/Callable;

    invoke-static {v0}, Lio/reactivex/n;->fromCallable(Ljava/util/concurrent/Callable;)Lio/reactivex/n;

    move-result-object v0

    const-string v1, "Observable.fromCallable \u2026.fromFile(file)\n        }"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public a(Ljava/lang/String;Landroid/graphics/drawable/Drawable;)V
    .locals 1

    .prologue
    const-string v0, "text"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "preview"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 114
    sget v0, Lco/uk/getmondo/c$a;->fallbackDocumentBodyTextView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/identity_verification/fallback/DocumentFallbackActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    check-cast p1, Ljava/lang/CharSequence;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 115
    sget v0, Lco/uk/getmondo/c$a;->fallbackDocumentPreviewImageView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/identity_verification/fallback/DocumentFallbackActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    invoke-virtual {v0, p2}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 116
    return-void
.end method

.method public a(Ljava/lang/String;Landroid/net/Uri;)V
    .locals 6

    .prologue
    const-string v0, "title"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "uri"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 119
    sget v0, Lco/uk/getmondo/c$a;->fallbackDocumentBodyTextView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/identity_verification/fallback/DocumentFallbackActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    check-cast p1, Ljava/lang/CharSequence;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 120
    sget v0, Lco/uk/getmondo/c$a;->mainActionButton:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/identity_verification/fallback/DocumentFallbackActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    const v1, 0x7f0a022f

    invoke-virtual {p0, v1}, Lco/uk/getmondo/signup/identity_verification/fallback/DocumentFallbackActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 121
    sget v0, Lco/uk/getmondo/c$a;->retakePhotoButton:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/identity_verification/fallback/DocumentFallbackActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 122
    new-instance v1, Ljava/io/File;

    invoke-virtual {p2}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    move-object v0, p0

    .line 123
    check-cast v0, Landroid/support/v4/app/j;

    invoke-static {v0}, Lcom/bumptech/glide/g;->a(Landroid/support/v4/app/j;)Lcom/bumptech/glide/j;

    move-result-object v0

    .line 124
    invoke-virtual {v0, v1}, Lcom/bumptech/glide/j;->a(Ljava/io/File;)Lcom/bumptech/glide/d;

    move-result-object v2

    .line 125
    new-instance v0, Lcom/bumptech/glide/h/b;

    invoke-virtual {v1}, Ljava/io/File;->lastModified()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/bumptech/glide/h/b;-><init>(Ljava/lang/String;)V

    check-cast v0, Lcom/bumptech/glide/load/c;

    invoke-virtual {v2, v0}, Lcom/bumptech/glide/d;->a(Lcom/bumptech/glide/load/c;)Lcom/bumptech/glide/c;

    move-result-object v1

    .line 126
    sget v0, Lco/uk/getmondo/c$a;->fallbackDocumentPreviewImageView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/identity_verification/fallback/DocumentFallbackActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Lcom/bumptech/glide/c;->a(Landroid/widget/ImageView;)Lcom/bumptech/glide/g/b/j;

    .line 127
    return-void
.end method

.method public b()Lio/reactivex/n;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .prologue
    .line 79
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/fallback/DocumentFallbackActivity;->e:Lcom/b/b/c;

    check-cast v0, Lio/reactivex/r;

    sget v1, Lco/uk/getmondo/c$a;->mainActionButton:I

    invoke-virtual {p0, v1}, Lco/uk/getmondo/signup/identity_verification/fallback/DocumentFallbackActivity;->a(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    invoke-static {v1}, Lcom/b/a/c/c;->a(Landroid/view/View;)Lio/reactivex/n;

    move-result-object v1

    check-cast v1, Lio/reactivex/r;

    invoke-static {v0, v1}, Lio/reactivex/n;->merge(Lio/reactivex/r;Lio/reactivex/r;)Lio/reactivex/n;

    move-result-object v1

    new-instance v0, Lco/uk/getmondo/signup/identity_verification/fallback/DocumentFallbackActivity$b;

    invoke-direct {v0, p0}, Lco/uk/getmondo/signup/identity_verification/fallback/DocumentFallbackActivity$b;-><init>(Lco/uk/getmondo/signup/identity_verification/fallback/DocumentFallbackActivity;)V

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {v1, v0}, Lio/reactivex/n;->map(Lio/reactivex/c/h;)Lio/reactivex/n;

    move-result-object v0

    const-string v1, "Observable.merge(permiss\u2026{ hasCameraPermission() }"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public c()Lio/reactivex/n;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 83
    sget v0, Lco/uk/getmondo/c$a;->retakePhotoButton:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/identity_verification/fallback/DocumentFallbackActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    invoke-static {v0}, Lcom/b/a/c/c;->a(Landroid/view/View;)Lio/reactivex/n;

    move-result-object v0

    const-string v1, "RxView.clicks(retakePhotoButton)"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public d()Lio/reactivex/n;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Ljava/io/File;",
            ">;"
        }
    .end annotation

    .prologue
    .line 87
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/fallback/DocumentFallbackActivity;->f:Lcom/b/b/c;

    const-string v1, "imageChangedRelay"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lio/reactivex/n;

    return-object v0
.end method

.method public e()V
    .locals 3

    .prologue
    .line 99
    check-cast p0, Landroid/app/Activity;

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "android.permission.CAMERA"

    aput-object v2, v0, v1

    check-cast v0, [Ljava/lang/Object;

    .line 175
    check-cast v0, [Ljava/lang/String;

    .line 99
    const/16 v1, 0x3e8

    invoke-static {p0, v0, v1}, Landroid/support/v4/app/a;->a(Landroid/app/Activity;[Ljava/lang/String;I)V

    .line 100
    return-void
.end method

.method public f()V
    .locals 5

    .prologue
    .line 103
    new-instance v1, Landroid/content/Intent;

    const-string v0, "android.media.action.IMAGE_CAPTURE"

    invoke-direct {v1, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 104
    invoke-virtual {p0}, Lco/uk/getmondo/signup/identity_verification/fallback/DocumentFallbackActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/content/Intent;->resolveActivity(Landroid/content/pm/PackageManager;)Landroid/content/ComponentName;

    move-result-object v0

    if-eqz v0, :cond_1

    move-object v0, p0

    .line 105
    check-cast v0, Landroid/content/Context;

    const v2, 0x7f0a018b

    invoke-virtual {p0, v2}, Lco/uk/getmondo/signup/identity_verification/fallback/DocumentFallbackActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lco/uk/getmondo/signup/identity_verification/fallback/DocumentFallbackActivity;->g:Ljava/io/File;

    if-nez v3, :cond_0

    const-string v4, "photoFile"

    invoke-static {v4}, Lkotlin/d/b/l;->b(Ljava/lang/String;)V

    :cond_0
    invoke-static {v0, v2, v3}, Landroid/support/v4/content/FileProvider;->a(Landroid/content/Context;Ljava/lang/String;Ljava/io/File;)Landroid/net/Uri;

    move-result-object v0

    .line 106
    const-string v2, "output"

    check-cast v0, Landroid/os/Parcelable;

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 107
    const/16 v0, 0x3e9

    invoke-virtual {p0, v1, v0}, Lco/uk/getmondo/signup/identity_verification/fallback/DocumentFallbackActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 110
    :goto_0
    return-void

    .line 109
    :cond_1
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Failed to find default camera to open"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    invoke-static {v0}, Ld/a/a;->a(Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public g()V
    .locals 2

    .prologue
    .line 130
    sget v0, Lco/uk/getmondo/c$a;->mainActionButton:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/identity_verification/fallback/DocumentFallbackActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    const v1, 0x7f0a022b

    invoke-virtual {p0, v1}, Lco/uk/getmondo/signup/identity_verification/fallback/DocumentFallbackActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 131
    sget v0, Lco/uk/getmondo/c$a;->retakePhotoButton:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/identity_verification/fallback/DocumentFallbackActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 132
    return-void
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 3

    .prologue
    .line 144
    const/16 v0, 0x3e9

    if-ne p1, v0, :cond_1

    const/4 v0, -0x1

    if-ne p2, v0, :cond_1

    .line 145
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/fallback/DocumentFallbackActivity;->f:Lcom/b/b/c;

    iget-object v1, p0, Lco/uk/getmondo/signup/identity_verification/fallback/DocumentFallbackActivity;->g:Ljava/io/File;

    if-nez v1, :cond_0

    const-string v2, "photoFile"

    invoke-static {v2}, Lkotlin/d/b/l;->b(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0, v1}, Lcom/b/b/c;->a(Ljava/lang/Object;)V

    .line 148
    :goto_0
    return-void

    .line 147
    :cond_1
    invoke-super {p0, p1, p2, p3}, Lco/uk/getmondo/common/activities/b;->onActivityResult(IILandroid/content/Intent;)V

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 6

    .prologue
    .line 47
    invoke-super {p0, p1}, Lco/uk/getmondo/common/activities/b;->onCreate(Landroid/os/Bundle;)V

    .line 49
    const v0, 0x7f050032

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/identity_verification/fallback/DocumentFallbackActivity;->setContentView(I)V

    .line 51
    sget-object v0, Lco/uk/getmondo/d/i;->Companion:Lco/uk/getmondo/d/i$a;

    invoke-virtual {p0}, Lco/uk/getmondo/signup/identity_verification/fallback/DocumentFallbackActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "KEY_COUNTRY_CODE"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lco/uk/getmondo/d/i$a;->a(Ljava/lang/String;)Lco/uk/getmondo/d/i;

    move-result-object v0

    .line 52
    if-nez v0, :cond_5

    .line 53
    sget-object v0, Lco/uk/getmondo/d/i;->UNITED_KINGDOM:Lco/uk/getmondo/d/i;

    move-object v1, v0

    .line 56
    :goto_0
    sget-object v0, Lco/uk/getmondo/api/model/identity_verification/IdentityDocumentType;->PASSPORT:Lco/uk/getmondo/api/model/identity_verification/IdentityDocumentType;

    .line 57
    invoke-virtual {p0}, Lco/uk/getmondo/signup/identity_verification/fallback/DocumentFallbackActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string v3, "KEY_ID_DOCUMENT_TYPE"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 58
    invoke-virtual {p0}, Lco/uk/getmondo/signup/identity_verification/fallback/DocumentFallbackActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v2, "KEY_ID_DOCUMENT_TYPE"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    if-nez v0, :cond_0

    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type co.uk.getmondo.api.model.identity_verification.IdentityDocumentType"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    check-cast v0, Lco/uk/getmondo/api/model/identity_verification/IdentityDocumentType;

    move-object v2, v0

    .line 60
    :goto_1
    invoke-virtual {p0}, Lco/uk/getmondo/signup/identity_verification/fallback/DocumentFallbackActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v3, "KEY_SIGNUP_SOURCE"

    invoke-virtual {v0, v3}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    if-nez v0, :cond_1

    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type co.uk.getmondo.api.model.signup.SignupSource"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    check-cast v0, Lco/uk/getmondo/api/model/signup/SignupSource;

    .line 61
    invoke-virtual {p0}, Lco/uk/getmondo/signup/identity_verification/fallback/DocumentFallbackActivity;->l()Lco/uk/getmondo/common/h/b/b;

    move-result-object v3

    .line 62
    new-instance v4, Lco/uk/getmondo/signup/identity_verification/p;

    sget-object v5, Lco/uk/getmondo/signup/identity_verification/a/j;->b:Lco/uk/getmondo/signup/identity_verification/a/j;

    invoke-direct {v4, v5, v0}, Lco/uk/getmondo/signup/identity_verification/p;-><init>(Lco/uk/getmondo/signup/identity_verification/a/j;Lco/uk/getmondo/api/model/signup/SignupSource;)V

    invoke-interface {v3, v4}, Lco/uk/getmondo/common/h/b/b;->a(Lco/uk/getmondo/signup/identity_verification/p;)Lco/uk/getmondo/signup/identity_verification/o;

    move-result-object v0

    .line 63
    new-instance v3, Lco/uk/getmondo/signup/identity_verification/id_picture/f;

    const/4 v4, 0x0

    invoke-direct {v3, v2, v1, v4}, Lco/uk/getmondo/signup/identity_verification/id_picture/f;-><init>(Lco/uk/getmondo/api/model/identity_verification/IdentityDocumentType;Lco/uk/getmondo/d/i;Ljava/lang/String;)V

    invoke-interface {v0, v3}, Lco/uk/getmondo/signup/identity_verification/o;->a(Lco/uk/getmondo/signup/identity_verification/id_picture/f;)Lco/uk/getmondo/signup/identity_verification/id_picture/e;

    move-result-object v0

    .line 64
    invoke-interface {v0, p0}, Lco/uk/getmondo/signup/identity_verification/id_picture/e;->a(Lco/uk/getmondo/signup/identity_verification/fallback/DocumentFallbackActivity;)V

    .line 65
    iget-object v1, p0, Lco/uk/getmondo/signup/identity_verification/fallback/DocumentFallbackActivity;->a:Lco/uk/getmondo/signup/identity_verification/fallback/b;

    if-nez v1, :cond_2

    const-string v0, "presenter"

    invoke-static {v0}, Lkotlin/d/b/l;->b(Ljava/lang/String;)V

    :cond_2
    move-object v0, p0

    check-cast v0, Lco/uk/getmondo/signup/identity_verification/fallback/b$a;

    invoke-virtual {v1, v0}, Lco/uk/getmondo/signup/identity_verification/fallback/b;->a(Lco/uk/getmondo/signup/identity_verification/fallback/b$a;)V

    .line 67
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/fallback/DocumentFallbackActivity;->b:Lco/uk/getmondo/signup/identity_verification/a/a;

    if-nez v0, :cond_3

    const-string v1, "fileGenerator"

    invoke-static {v1}, Lkotlin/d/b/l;->b(Ljava/lang/String;)V

    :cond_3
    invoke-virtual {v0}, Lco/uk/getmondo/signup/identity_verification/a/a;->e()Ljava/io/File;

    move-result-object v0

    const-string v1, "fileGenerator.tempFallbackIdentityDocFile"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v0, p0, Lco/uk/getmondo/signup/identity_verification/fallback/DocumentFallbackActivity;->g:Ljava/io/File;

    .line 68
    return-void

    :cond_4
    move-object v2, v0

    goto :goto_1

    :cond_5
    move-object v1, v0

    goto :goto_0
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    .line 71
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/fallback/DocumentFallbackActivity;->g:Ljava/io/File;

    if-nez v0, :cond_0

    const-string v1, "photoFile"

    invoke-static {v1}, Lkotlin/d/b/l;->b(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 72
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/fallback/DocumentFallbackActivity;->g:Ljava/io/File;

    if-nez v0, :cond_1

    const-string v1, "photoFile"

    invoke-static {v1}, Lkotlin/d/b/l;->b(Ljava/lang/String;)V

    :cond_1
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    .line 74
    :cond_2
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/fallback/DocumentFallbackActivity;->a:Lco/uk/getmondo/signup/identity_verification/fallback/b;

    if-nez v0, :cond_3

    const-string v1, "presenter"

    invoke-static {v1}, Lkotlin/d/b/l;->b(Ljava/lang/String;)V

    :cond_3
    invoke-virtual {v0}, Lco/uk/getmondo/signup/identity_verification/fallback/b;->b()V

    .line 75
    invoke-super {p0}, Lco/uk/getmondo/common/activities/b;->onDestroy()V

    .line 76
    return-void
.end method

.method public onRequestPermissionsResult(I[Ljava/lang/String;[I)V
    .locals 3

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    const-string v2, "permissions"

    invoke-static {p2, v2}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v2, "grantResults"

    invoke-static {p3, v2}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 135
    const/16 v2, 0x3e8

    if-ne p1, v2, :cond_2

    array-length v2, p3

    if-nez v2, :cond_0

    move v2, v0

    :goto_0
    if-nez v2, :cond_1

    :goto_1
    if-eqz v0, :cond_2

    aget v0, p3, v1

    if-nez v0, :cond_2

    .line 137
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/fallback/DocumentFallbackActivity;->e:Lcom/b/b/c;

    sget-object v1, Lkotlin/n;->a:Lkotlin/n;

    invoke-virtual {v0, v1}, Lcom/b/b/c;->a(Ljava/lang/Object;)V

    .line 140
    :goto_2
    return-void

    :cond_0
    move v2, v1

    .line 135
    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_1

    .line 139
    :cond_2
    invoke-super {p0, p1, p2, p3}, Lco/uk/getmondo/common/activities/b;->onRequestPermissionsResult(I[Ljava/lang/String;[I)V

    goto :goto_2
.end method
