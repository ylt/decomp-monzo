.class final Lco/uk/getmondo/signup/identity_verification/fallback/b$f;
.super Ljava/lang/Object;
.source "DocumentFallbackPresenter.kt"

# interfaces
.implements Lio/reactivex/c/g;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lco/uk/getmondo/signup/identity_verification/fallback/b;->a(Lco/uk/getmondo/signup/identity_verification/fallback/b$a;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/g",
        "<",
        "Landroid/net/Uri;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "",
        "uri",
        "Landroid/net/Uri;",
        "kotlin.jvm.PlatformType",
        "accept"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field final synthetic a:Lco/uk/getmondo/signup/identity_verification/fallback/b;

.field final synthetic b:Lco/uk/getmondo/signup/identity_verification/fallback/b$a;


# direct methods
.method constructor <init>(Lco/uk/getmondo/signup/identity_verification/fallback/b;Lco/uk/getmondo/signup/identity_verification/fallback/b$a;)V
    .locals 0

    iput-object p1, p0, Lco/uk/getmondo/signup/identity_verification/fallback/b$f;->a:Lco/uk/getmondo/signup/identity_verification/fallback/b;

    iput-object p2, p0, Lco/uk/getmondo/signup/identity_verification/fallback/b$f;->b:Lco/uk/getmondo/signup/identity_verification/fallback/b$a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/net/Uri;)V
    .locals 4

    .prologue
    .line 77
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/fallback/b$f;->a:Lco/uk/getmondo/signup/identity_verification/fallback/b;

    invoke-static {v0}, Lco/uk/getmondo/signup/identity_verification/fallback/b;->a(Lco/uk/getmondo/signup/identity_verification/fallback/b;)Ljava/lang/String;

    move-result-object v0

    .line 78
    if-nez v0, :cond_1

    .line 79
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/fallback/b$f;->a:Lco/uk/getmondo/signup/identity_verification/fallback/b;

    invoke-virtual {p1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lco/uk/getmondo/signup/identity_verification/fallback/b;->a(Lco/uk/getmondo/signup/identity_verification/fallback/b;Ljava/lang/String;)V

    .line 80
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/fallback/b$f;->a:Lco/uk/getmondo/signup/identity_verification/fallback/b;

    invoke-static {v0}, Lco/uk/getmondo/signup/identity_verification/fallback/b;->c(Lco/uk/getmondo/signup/identity_verification/fallback/b;)Lco/uk/getmondo/api/model/identity_verification/IdentityDocumentType;

    move-result-object v0

    invoke-virtual {v0}, Lco/uk/getmondo/api/model/identity_verification/IdentityDocumentType;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 82
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/fallback/b$f;->b:Lco/uk/getmondo/signup/identity_verification/fallback/b$a;

    iget-object v1, p0, Lco/uk/getmondo/signup/identity_verification/fallback/b$f;->a:Lco/uk/getmondo/signup/identity_verification/fallback/b;

    invoke-static {v1}, Lco/uk/getmondo/signup/identity_verification/fallback/b;->j(Lco/uk/getmondo/signup/identity_verification/fallback/b;)Lco/uk/getmondo/signup/identity_verification/fallback/e;

    move-result-object v1

    invoke-virtual {v1}, Lco/uk/getmondo/signup/identity_verification/fallback/e;->a()Ljava/lang/String;

    move-result-object v1

    const-string v2, "uri"

    invoke-static {p1, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v0, v1, p1}, Lco/uk/getmondo/signup/identity_verification/fallback/b$a;->a(Ljava/lang/String;Landroid/net/Uri;)V

    .line 91
    :goto_0
    return-void

    .line 85
    :cond_0
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/fallback/b$f;->b:Lco/uk/getmondo/signup/identity_verification/fallback/b$a;

    iget-object v1, p0, Lco/uk/getmondo/signup/identity_verification/fallback/b$f;->a:Lco/uk/getmondo/signup/identity_verification/fallback/b;

    invoke-static {v1}, Lco/uk/getmondo/signup/identity_verification/fallback/b;->j(Lco/uk/getmondo/signup/identity_verification/fallback/b;)Lco/uk/getmondo/signup/identity_verification/fallback/e;

    move-result-object v1

    iget-object v2, p0, Lco/uk/getmondo/signup/identity_verification/fallback/b$f;->a:Lco/uk/getmondo/signup/identity_verification/fallback/b;

    invoke-static {v2}, Lco/uk/getmondo/signup/identity_verification/fallback/b;->c(Lco/uk/getmondo/signup/identity_verification/fallback/b;)Lco/uk/getmondo/api/model/identity_verification/IdentityDocumentType;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Lco/uk/getmondo/signup/identity_verification/fallback/e;->b(Lco/uk/getmondo/api/model/identity_verification/IdentityDocumentType;Z)Ljava/lang/String;

    move-result-object v1

    const-string v2, "uri"

    invoke-static {p1, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v0, v1, p1}, Lco/uk/getmondo/signup/identity_verification/fallback/b$a;->a(Ljava/lang/String;Landroid/net/Uri;)V

    goto :goto_0

    .line 88
    :cond_1
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/fallback/b$f;->a:Lco/uk/getmondo/signup/identity_verification/fallback/b;

    invoke-virtual {p1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lco/uk/getmondo/signup/identity_verification/fallback/b;->b(Lco/uk/getmondo/signup/identity_verification/fallback/b;Ljava/lang/String;)V

    .line 90
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/fallback/b$f;->b:Lco/uk/getmondo/signup/identity_verification/fallback/b$a;

    iget-object v1, p0, Lco/uk/getmondo/signup/identity_verification/fallback/b$f;->a:Lco/uk/getmondo/signup/identity_verification/fallback/b;

    invoke-static {v1}, Lco/uk/getmondo/signup/identity_verification/fallback/b;->j(Lco/uk/getmondo/signup/identity_verification/fallback/b;)Lco/uk/getmondo/signup/identity_verification/fallback/e;

    move-result-object v1

    iget-object v2, p0, Lco/uk/getmondo/signup/identity_verification/fallback/b$f;->a:Lco/uk/getmondo/signup/identity_verification/fallback/b;

    invoke-static {v2}, Lco/uk/getmondo/signup/identity_verification/fallback/b;->c(Lco/uk/getmondo/signup/identity_verification/fallback/b;)Lco/uk/getmondo/api/model/identity_verification/IdentityDocumentType;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Lco/uk/getmondo/signup/identity_verification/fallback/e;->b(Lco/uk/getmondo/api/model/identity_verification/IdentityDocumentType;Z)Ljava/lang/String;

    move-result-object v1

    const-string v2, "uri"

    invoke-static {p1, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v0, v1, p1}, Lco/uk/getmondo/signup/identity_verification/fallback/b$a;->a(Ljava/lang/String;Landroid/net/Uri;)V

    goto :goto_0
.end method

.method public bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 27
    check-cast p1, Landroid/net/Uri;

    invoke-virtual {p0, p1}, Lco/uk/getmondo/signup/identity_verification/fallback/b$f;->a(Landroid/net/Uri;)V

    return-void
.end method
