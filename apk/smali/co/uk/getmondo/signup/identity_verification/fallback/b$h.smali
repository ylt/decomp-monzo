.class final Lco/uk/getmondo/signup/identity_verification/fallback/b$h;
.super Ljava/lang/Object;
.source "DocumentFallbackPresenter.kt"

# interfaces
.implements Lio/reactivex/c/g;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lco/uk/getmondo/signup/identity_verification/fallback/b;->a(Lco/uk/getmondo/signup/identity_verification/fallback/b$a;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/g",
        "<",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u0000\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "",
        "it",
        "",
        "kotlin.jvm.PlatformType",
        "accept"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field final synthetic a:Lco/uk/getmondo/signup/identity_verification/fallback/b;

.field final synthetic b:Lco/uk/getmondo/signup/identity_verification/fallback/b$a;


# direct methods
.method constructor <init>(Lco/uk/getmondo/signup/identity_verification/fallback/b;Lco/uk/getmondo/signup/identity_verification/fallback/b$a;)V
    .locals 0

    iput-object p1, p0, Lco/uk/getmondo/signup/identity_verification/fallback/b$h;->a:Lco/uk/getmondo/signup/identity_verification/fallback/b;

    iput-object p2, p0, Lco/uk/getmondo/signup/identity_verification/fallback/b$h;->b:Lco/uk/getmondo/signup/identity_verification/fallback/b$a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)V
    .locals 5

    .prologue
    const/4 v0, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 96
    iget-object v1, p0, Lco/uk/getmondo/signup/identity_verification/fallback/b$h;->a:Lco/uk/getmondo/signup/identity_verification/fallback/b;

    invoke-static {v1}, Lco/uk/getmondo/signup/identity_verification/fallback/b;->a(Lco/uk/getmondo/signup/identity_verification/fallback/b;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    move v1, v2

    .line 97
    :goto_0
    iget-object v4, p0, Lco/uk/getmondo/signup/identity_verification/fallback/b$h;->a:Lco/uk/getmondo/signup/identity_verification/fallback/b;

    invoke-static {v4}, Lco/uk/getmondo/signup/identity_verification/fallback/b;->b(Lco/uk/getmondo/signup/identity_verification/fallback/b;)Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_2

    .line 100
    :goto_1
    if-eqz v1, :cond_3

    if-nez v2, :cond_3

    .line 101
    iget-object v1, p0, Lco/uk/getmondo/signup/identity_verification/fallback/b$h;->a:Lco/uk/getmondo/signup/identity_verification/fallback/b;

    check-cast v0, Ljava/lang/String;

    invoke-static {v1, v0}, Lco/uk/getmondo/signup/identity_verification/fallback/b;->a(Lco/uk/getmondo/signup/identity_verification/fallback/b;Ljava/lang/String;)V

    .line 104
    :cond_0
    :goto_2
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/fallback/b$h;->a:Lco/uk/getmondo/signup/identity_verification/fallback/b;

    invoke-static {v0, v3}, Lco/uk/getmondo/signup/identity_verification/fallback/b;->a(Lco/uk/getmondo/signup/identity_verification/fallback/b;Z)V

    .line 106
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/fallback/b$h;->b:Lco/uk/getmondo/signup/identity_verification/fallback/b$a;

    invoke-interface {v0}, Lco/uk/getmondo/signup/identity_verification/fallback/b$a;->g()V

    .line 107
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/fallback/b$h;->a:Lco/uk/getmondo/signup/identity_verification/fallback/b;

    iget-object v1, p0, Lco/uk/getmondo/signup/identity_verification/fallback/b$h;->b:Lco/uk/getmondo/signup/identity_verification/fallback/b$a;

    invoke-static {v0, v1}, Lco/uk/getmondo/signup/identity_verification/fallback/b;->a(Lco/uk/getmondo/signup/identity_verification/fallback/b;Lco/uk/getmondo/signup/identity_verification/fallback/b$a;)V

    .line 108
    return-void

    :cond_1
    move v1, v3

    .line 96
    goto :goto_0

    :cond_2
    move v2, v3

    .line 97
    goto :goto_1

    .line 102
    :cond_3
    if-eqz v1, :cond_0

    if-eqz v2, :cond_0

    .line 103
    iget-object v1, p0, Lco/uk/getmondo/signup/identity_verification/fallback/b$h;->a:Lco/uk/getmondo/signup/identity_verification/fallback/b;

    check-cast v0, Ljava/lang/String;

    invoke-static {v1, v0}, Lco/uk/getmondo/signup/identity_verification/fallback/b;->b(Lco/uk/getmondo/signup/identity_verification/fallback/b;Ljava/lang/String;)V

    goto :goto_2
.end method
