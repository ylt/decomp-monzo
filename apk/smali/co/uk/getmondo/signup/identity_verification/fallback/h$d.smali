.class final Lco/uk/getmondo/signup/identity_verification/fallback/h$d;
.super Ljava/lang/Object;
.source "VideoFallbackPresenter.kt"

# interfaces
.implements Lio/reactivex/c/g;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lco/uk/getmondo/signup/identity_verification/fallback/h;->a(Lco/uk/getmondo/signup/identity_verification/fallback/h$a;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/g",
        "<",
        "Ljava/io/File;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "",
        "file",
        "Ljava/io/File;",
        "kotlin.jvm.PlatformType",
        "accept"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field final synthetic a:Lco/uk/getmondo/signup/identity_verification/fallback/h;

.field final synthetic b:Lco/uk/getmondo/signup/identity_verification/fallback/h$a;


# direct methods
.method constructor <init>(Lco/uk/getmondo/signup/identity_verification/fallback/h;Lco/uk/getmondo/signup/identity_verification/fallback/h$a;)V
    .locals 0

    iput-object p1, p0, Lco/uk/getmondo/signup/identity_verification/fallback/h$d;->a:Lco/uk/getmondo/signup/identity_verification/fallback/h;

    iput-object p2, p0, Lco/uk/getmondo/signup/identity_verification/fallback/h$d;->b:Lco/uk/getmondo/signup/identity_verification/fallback/h$a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/io/File;)V
    .locals 4

    .prologue
    .line 51
    invoke-virtual {p1}, Ljava/io/File;->length()J

    move-result-wide v0

    const/high16 v2, 0x1400000

    int-to-long v2, v2

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    .line 52
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/fallback/h$d;->b:Lco/uk/getmondo/signup/identity_verification/fallback/h$a;

    invoke-interface {v0}, Lco/uk/getmondo/signup/identity_verification/fallback/h$a;->i()V

    .line 56
    :goto_0
    return-void

    .line 54
    :cond_0
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/fallback/h$d;->a:Lco/uk/getmondo/signup/identity_verification/fallback/h;

    invoke-virtual {p1}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lco/uk/getmondo/signup/identity_verification/fallback/h;->a(Lco/uk/getmondo/signup/identity_verification/fallback/h;Ljava/lang/String;)V

    .line 55
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/fallback/h$d;->b:Lco/uk/getmondo/signup/identity_verification/fallback/h$a;

    const-string v1, "file"

    invoke-static {p1, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v1, p0, Lco/uk/getmondo/signup/identity_verification/fallback/h$d;->a:Lco/uk/getmondo/signup/identity_verification/fallback/h;

    invoke-static {v1}, Lco/uk/getmondo/signup/identity_verification/fallback/h;->c(Lco/uk/getmondo/signup/identity_verification/fallback/h;)Lco/uk/getmondo/signup/identity_verification/video/ab;

    move-result-object v1

    invoke-virtual {v1}, Lco/uk/getmondo/signup/identity_verification/video/ab;->c()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lco/uk/getmondo/signup/identity_verification/fallback/h$d;->a:Lco/uk/getmondo/signup/identity_verification/fallback/h;

    invoke-static {v2}, Lco/uk/getmondo/signup/identity_verification/fallback/h;->c(Lco/uk/getmondo/signup/identity_verification/fallback/h;)Lco/uk/getmondo/signup/identity_verification/video/ab;

    move-result-object v2

    invoke-virtual {v2}, Lco/uk/getmondo/signup/identity_verification/video/ab;->a()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, p1, v1, v2}, Lco/uk/getmondo/signup/identity_verification/fallback/h$a;->a(Ljava/io/File;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 19
    check-cast p1, Ljava/io/File;

    invoke-virtual {p0, p1}, Lco/uk/getmondo/signup/identity_verification/fallback/h$d;->a(Ljava/io/File;)V

    return-void
.end method
