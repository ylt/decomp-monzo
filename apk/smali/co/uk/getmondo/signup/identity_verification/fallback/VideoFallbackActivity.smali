.class public final Lco/uk/getmondo/signup/identity_verification/fallback/VideoFallbackActivity;
.super Lco/uk/getmondo/common/activities/b;
.source "VideoFallbackActivity.kt"

# interfaces
.implements Lco/uk/getmondo/signup/identity_verification/fallback/h$a;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lco/uk/getmondo/signup/identity_verification/fallback/VideoFallbackActivity$a;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000\u0090\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0008\u0007\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0003\n\u0002\u0010\u0008\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u0011\n\u0000\n\u0002\u0010\u0015\n\u0002\u0008\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\r\u0018\u0000 K2\u00020\u00012\u00020\u0002:\u0001KB\u0005\u00a2\u0006\u0002\u0010\u0003J\u0010\u0010 \u001a\u00020\u00112\u0006\u0010!\u001a\u00020\"H\u0016J\u0008\u0010#\u001a\u00020$H\u0002J\u0008\u0010%\u001a\u00020\u0011H\u0016J\"\u0010&\u001a\u00020\u00112\u0006\u0010\'\u001a\u00020(2\u0006\u0010)\u001a\u00020(2\u0008\u0010*\u001a\u0004\u0018\u00010+H\u0014J\u0012\u0010,\u001a\u00020\u00112\u0008\u0010-\u001a\u0004\u0018\u00010.H\u0014J\u0008\u0010/\u001a\u00020\u0011H\u0014J\u000e\u00100\u001a\u0008\u0012\u0004\u0012\u00020$01H\u0016J\u000e\u00102\u001a\u0008\u0012\u0004\u0012\u00020$01H\u0016J+\u00103\u001a\u00020\u00112\u0006\u0010\'\u001a\u00020(2\u000c\u00104\u001a\u0008\u0012\u0004\u0012\u00020\"052\u0006\u00106\u001a\u000207H\u0016\u00a2\u0006\u0002\u00108J\u000e\u00109\u001a\u0008\u0012\u0004\u0012\u00020:01H\u0016J\u000e\u0010;\u001a\u0008\u0012\u0004\u0012\u00020\u000c01H\u0016J\u0006\u0010<\u001a\u00020\u0011J\u0010\u0010=\u001a\u00020\u00112\u0006\u0010>\u001a\u00020?H\u0002J\u0008\u0010@\u001a\u00020\u0011H\u0016J\u0008\u0010A\u001a\u00020\u0011H\u0016J \u0010B\u001a\u00020\u00112\u0006\u0010C\u001a\u00020\u000c2\u0006\u0010D\u001a\u00020\"2\u0006\u0010E\u001a\u00020\"H\u0016J\u0018\u0010F\u001a\u00020\u00112\u0006\u0010D\u001a\u00020\"2\u0006\u0010E\u001a\u00020\"H\u0016J\u0018\u0010G\u001a\u00020\u00112\u0006\u0010D\u001a\u00020\"2\u0006\u0010E\u001a\u00020\"H\u0002J\u0008\u0010H\u001a\u00020\u0011H\u0016J\u0008\u0010I\u001a\u00020\u0011H\u0016J\u0008\u0010J\u001a\u00020\u0011H\u0016R\u001e\u0010\u0004\u001a\u00020\u00058\u0006@\u0006X\u0087.\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u0006\u0010\u0007\"\u0004\u0008\u0008\u0010\tR\u001e\u0010\n\u001a\u0012\u0012\u0004\u0012\u00020\u000c0\u000bj\u0008\u0012\u0004\u0012\u00020\u000c`\rX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u000e\u001a\u0004\u0018\u00010\u000cX\u0082\u000e\u00a2\u0006\u0002\n\u0000R2\u0010\u000f\u001a&\u0012\u000c\u0012\n \u0012*\u0004\u0018\u00010\u00110\u0011 \u0012*\u0012\u0012\u000c\u0012\n \u0012*\u0004\u0018\u00010\u00110\u0011\u0018\u00010\u00100\u0010X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001e\u0010\u0013\u001a\u00020\u00148\u0006@\u0006X\u0087.\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u0015\u0010\u0016\"\u0004\u0008\u0017\u0010\u0018R#\u0010\u0019\u001a\n \u0012*\u0004\u0018\u00010\u001a0\u001a8BX\u0082\u0084\u0002\u00a2\u0006\u000c\n\u0004\u0008\u001d\u0010\u001e\u001a\u0004\u0008\u001b\u0010\u001cR2\u0010\u001f\u001a&\u0012\u000c\u0012\n \u0012*\u0004\u0018\u00010\u000c0\u000c \u0012*\u0012\u0012\u000c\u0012\n \u0012*\u0004\u0018\u00010\u000c0\u000c\u0018\u00010\u00100\u0010X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006L"
    }
    d2 = {
        "Lco/uk/getmondo/signup/identity_verification/fallback/VideoFallbackActivity;",
        "Lco/uk/getmondo/common/activities/BaseActivity;",
        "Lco/uk/getmondo/signup/identity_verification/fallback/VideoFallbackPresenter$View;",
        "()V",
        "fileGenerator",
        "Lco/uk/getmondo/signup/identity_verification/data/FileGenerator;",
        "getFileGenerator",
        "()Lco/uk/getmondo/signup/identity_verification/data/FileGenerator;",
        "setFileGenerator",
        "(Lco/uk/getmondo/signup/identity_verification/data/FileGenerator;)V",
        "generatedVideoFiles",
        "Ljava/util/ArrayList;",
        "Ljava/io/File;",
        "Lkotlin/collections/ArrayList;",
        "latestVideoFile",
        "permissionGrantedRelay",
        "Lcom/jakewharton/rxrelay2/PublishRelay;",
        "",
        "kotlin.jvm.PlatformType",
        "presenter",
        "Lco/uk/getmondo/signup/identity_verification/fallback/VideoFallbackPresenter;",
        "getPresenter",
        "()Lco/uk/getmondo/signup/identity_verification/fallback/VideoFallbackPresenter;",
        "setPresenter",
        "(Lco/uk/getmondo/signup/identity_verification/fallback/VideoFallbackPresenter;)V",
        "simpleExoPlayer",
        "Lcom/google/android/exoplayer2/SimpleExoPlayer;",
        "getSimpleExoPlayer",
        "()Lcom/google/android/exoplayer2/SimpleExoPlayer;",
        "simpleExoPlayer$delegate",
        "Lkotlin/Lazy;",
        "videoTakenRelay",
        "deleteOldVideos",
        "usedSelfiePath",
        "",
        "hasPermissions",
        "",
        "mute",
        "onActivityResult",
        "requestCode",
        "",
        "resultCode",
        "data",
        "Landroid/content/Intent;",
        "onCreate",
        "savedInstanceState",
        "Landroid/os/Bundle;",
        "onDestroy",
        "onMainActionClicked",
        "Lio/reactivex/Observable;",
        "onMuteClicked",
        "onRequestPermissionsResult",
        "permissions",
        "",
        "grantResults",
        "",
        "(I[Ljava/lang/String;[I)V",
        "onRetakeClicked",
        "",
        "onVideoTaken",
        "playSampleVideo",
        "playVideoFromPath",
        "videoUri",
        "Landroid/net/Uri;",
        "requestCameraPermission",
        "resetToExample",
        "showConfirmation",
        "videoFile",
        "sentence",
        "textToRead",
        "showInitialInstructions",
        "showInstructions",
        "showVideoTooLongError",
        "takeVideo",
        "unmute",
        "Companion",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# static fields
.field static final synthetic a:[Lkotlin/reflect/l;

.field public static final e:Lco/uk/getmondo/signup/identity_verification/fallback/VideoFallbackActivity$a;


# instance fields
.field public b:Lco/uk/getmondo/signup/identity_verification/fallback/h;

.field public c:Lco/uk/getmondo/signup/identity_verification/a/a;

.field private final f:Lkotlin/c;

.field private final g:Lcom/b/b/c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/b/b/c",
            "<",
            "Lkotlin/n;",
            ">;"
        }
    .end annotation
.end field

.field private final h:Lcom/b/b/c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/b/b/c",
            "<",
            "Ljava/io/File;",
            ">;"
        }
    .end annotation
.end field

.field private final i:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/io/File;",
            ">;"
        }
    .end annotation
.end field

.field private j:Ljava/io/File;

.field private k:Ljava/util/HashMap;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const/4 v0, 0x1

    new-array v1, v0, [Lkotlin/reflect/l;

    const/4 v2, 0x0

    new-instance v0, Lkotlin/d/b/w;

    const-class v3, Lco/uk/getmondo/signup/identity_verification/fallback/VideoFallbackActivity;

    invoke-static {v3}, Lkotlin/d/b/y;->a(Ljava/lang/Class;)Lkotlin/reflect/c;

    move-result-object v3

    const-string v4, "simpleExoPlayer"

    const-string v5, "getSimpleExoPlayer()Lcom/google/android/exoplayer2/SimpleExoPlayer;"

    invoke-direct {v0, v3, v4, v5}, Lkotlin/d/b/w;-><init>(Lkotlin/reflect/e;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v0}, Lkotlin/d/b/y;->a(Lkotlin/d/b/v;)Lkotlin/reflect/n;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/l;

    aput-object v0, v1, v2

    sput-object v1, Lco/uk/getmondo/signup/identity_verification/fallback/VideoFallbackActivity;->a:[Lkotlin/reflect/l;

    new-instance v0, Lco/uk/getmondo/signup/identity_verification/fallback/VideoFallbackActivity$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lco/uk/getmondo/signup/identity_verification/fallback/VideoFallbackActivity$a;-><init>(Lkotlin/d/b/i;)V

    sput-object v0, Lco/uk/getmondo/signup/identity_verification/fallback/VideoFallbackActivity;->e:Lco/uk/getmondo/signup/identity_verification/fallback/VideoFallbackActivity$a;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 48
    invoke-direct {p0}, Lco/uk/getmondo/common/activities/b;-><init>()V

    .line 49
    new-instance v0, Lco/uk/getmondo/signup/identity_verification/fallback/VideoFallbackActivity$f;

    invoke-direct {v0, p0}, Lco/uk/getmondo/signup/identity_verification/fallback/VideoFallbackActivity$f;-><init>(Lco/uk/getmondo/signup/identity_verification/fallback/VideoFallbackActivity;)V

    check-cast v0, Lkotlin/d/a/a;

    invoke-static {v0}, Lkotlin/d;->a(Lkotlin/d/a/a;)Lkotlin/c;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/signup/identity_verification/fallback/VideoFallbackActivity;->f:Lkotlin/c;

    .line 52
    invoke-static {}, Lcom/b/b/c;->a()Lcom/b/b/c;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/signup/identity_verification/fallback/VideoFallbackActivity;->g:Lcom/b/b/c;

    .line 53
    invoke-static {}, Lcom/b/b/c;->a()Lcom/b/b/c;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/signup/identity_verification/fallback/VideoFallbackActivity;->h:Lcom/b/b/c;

    .line 54
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lco/uk/getmondo/signup/identity_verification/fallback/VideoFallbackActivity;->i:Ljava/util/ArrayList;

    return-void
.end method

.method private final a(Landroid/net/Uri;)V
    .locals 6

    .prologue
    const/4 v4, 0x0

    .line 180
    new-instance v2, Lcom/google/android/exoplayer2/upstream/h;

    move-object v0, p0

    check-cast v0, Landroid/content/Context;

    move-object v1, p0

    check-cast v1, Landroid/content/Context;

    const-string v3, "monzo"

    invoke-static {v1, v3}, Lcom/google/android/exoplayer2/util/s;->a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v0, v1, v4}, Lcom/google/android/exoplayer2/upstream/h;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/exoplayer2/upstream/l;)V

    .line 181
    new-instance v0, Lcom/google/android/exoplayer2/source/f;

    check-cast v2, Lcom/google/android/exoplayer2/upstream/c$a;

    .line 182
    new-instance v3, Lcom/google/android/exoplayer2/extractor/c;

    invoke-direct {v3}, Lcom/google/android/exoplayer2/extractor/c;-><init>()V

    check-cast v3, Lcom/google/android/exoplayer2/extractor/h;

    move-object v1, p1

    move-object v5, v4

    .line 181
    invoke-direct/range {v0 .. v5}, Lcom/google/android/exoplayer2/source/f;-><init>(Landroid/net/Uri;Lcom/google/android/exoplayer2/upstream/c$a;Lcom/google/android/exoplayer2/extractor/h;Landroid/os/Handler;Lcom/google/android/exoplayer2/source/f$a;)V

    .line 183
    invoke-direct {p0}, Lco/uk/getmondo/signup/identity_verification/fallback/VideoFallbackActivity;->v()Lcom/google/android/exoplayer2/t;

    move-result-object v2

    new-instance v1, Lcom/google/android/exoplayer2/source/g;

    check-cast v0, Lcom/google/android/exoplayer2/source/i;

    invoke-direct {v1, v0}, Lcom/google/android/exoplayer2/source/g;-><init>(Lcom/google/android/exoplayer2/source/i;)V

    move-object v0, v1

    check-cast v0, Lcom/google/android/exoplayer2/source/i;

    invoke-virtual {v2, v0}, Lcom/google/android/exoplayer2/t;->a(Lcom/google/android/exoplayer2/source/i;)V

    .line 184
    invoke-direct {p0}, Lco/uk/getmondo/signup/identity_verification/fallback/VideoFallbackActivity;->v()Lcom/google/android/exoplayer2/t;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/exoplayer2/t;->a(Z)V

    .line 185
    return-void
.end method

.method public static final synthetic a(Lco/uk/getmondo/signup/identity_verification/fallback/VideoFallbackActivity;)Z
    .locals 1

    .prologue
    .line 48
    invoke-direct {p0}, Lco/uk/getmondo/signup/identity_verification/fallback/VideoFallbackActivity;->w()Z

    move-result v0

    return v0
.end method

.method private final b(Ljava/lang/String;Ljava/lang/String;)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 172
    move-object v0, p0

    check-cast v0, Landroid/content/Context;

    const v1, 0x7f0f00f2

    invoke-static {v0, v1}, Landroid/support/v4/content/a;->c(Landroid/content/Context;I)I

    move-result v1

    .line 173
    sget v0, Lco/uk/getmondo/c$a;->fallbackVideoInstructionsTextView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/identity_verification/fallback/VideoFallbackActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    new-instance v2, Lco/uk/getmondo/common/ui/j;

    sget-object v3, Lkotlin/d/b/ab;->a:Lkotlin/d/b/ab;

    new-array v3, v5, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p2, v3, v4

    array-length v4, v3

    invoke-static {v3, v4}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v3

    invoke-static {p1, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "java.lang.String.format(format, *args)"

    invoke-static {v3, v4}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v2, v3, p2}, Lco/uk/getmondo/common/ui/j;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 174
    invoke-virtual {v2, v1, v5}, Lco/uk/getmondo/common/ui/j;->a(IZ)Lco/uk/getmondo/common/ui/j;

    move-result-object v1

    .line 175
    const-string v2, "sans-serif"

    invoke-virtual {v1, v2, v5, v5}, Lco/uk/getmondo/common/ui/j;->a(Ljava/lang/String;IZ)Lco/uk/getmondo/common/ui/j;

    move-result-object v1

    .line 176
    invoke-virtual {v1}, Lco/uk/getmondo/common/ui/j;->a()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 177
    return-void
.end method

.method private final v()Lcom/google/android/exoplayer2/t;
    .locals 3

    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/fallback/VideoFallbackActivity;->f:Lkotlin/c;

    sget-object v1, Lco/uk/getmondo/signup/identity_verification/fallback/VideoFallbackActivity;->a:[Lkotlin/reflect/l;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-interface {v0}, Lkotlin/c;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/exoplayer2/t;

    return-object v0
.end method

.method private final w()Z
    .locals 2

    .prologue
    .line 225
    move-object v0, p0

    check-cast v0, Landroid/content/Context;

    const-string v1, "android.permission.CAMERA"

    invoke-static {v0, v1}, Landroid/support/v4/content/a;->b(Landroid/content/Context;Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_0

    .line 226
    check-cast p0, Landroid/content/Context;

    const-string v0, "android.permission.RECORD_AUDIO"

    invoke-static {p0, v0}, Landroid/support/v4/content/a;->b(Landroid/content/Context;Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a(I)Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/fallback/VideoFallbackActivity;->k:Ljava/util/HashMap;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lco/uk/getmondo/signup/identity_verification/fallback/VideoFallbackActivity;->k:Ljava/util/HashMap;

    :cond_0
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/fallback/VideoFallbackActivity;->k:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    if-nez v0, :cond_1

    invoke-virtual {p0, p1}, Landroid/support/v4/app/j;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/signup/identity_verification/fallback/VideoFallbackActivity;->k:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    return-object v0
.end method

.method public final a()V
    .locals 6

    .prologue
    const/4 v4, 0x0

    .line 75
    sget v0, Lco/uk/getmondo/c$a;->fallbackVideoExoPlayer:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/identity_verification/fallback/VideoFallbackActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/exoplayer2/ui/SimpleExoPlayerView;

    invoke-direct {p0}, Lco/uk/getmondo/signup/identity_verification/fallback/VideoFallbackActivity;->v()Lcom/google/android/exoplayer2/t;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/exoplayer2/ui/SimpleExoPlayerView;->setPlayer(Lcom/google/android/exoplayer2/t;)V

    .line 77
    new-instance v3, Lcom/google/android/exoplayer2/upstream/RawResourceDataSource;

    move-object v0, p0

    check-cast v0, Landroid/content/Context;

    invoke-direct {v3, v0}, Lcom/google/android/exoplayer2/upstream/RawResourceDataSource;-><init>(Landroid/content/Context;)V

    .line 78
    new-instance v0, Lcom/google/android/exoplayer2/upstream/e;

    const v1, 0x7f090006

    invoke-static {v1}, Lcom/google/android/exoplayer2/upstream/RawResourceDataSource;->a(I)Landroid/net/Uri;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/exoplayer2/upstream/e;-><init>(Landroid/net/Uri;)V

    invoke-virtual {v3, v0}, Lcom/google/android/exoplayer2/upstream/RawResourceDataSource;->a(Lcom/google/android/exoplayer2/upstream/e;)J

    .line 80
    new-instance v0, Lcom/google/android/exoplayer2/source/f;

    invoke-virtual {v3}, Lcom/google/android/exoplayer2/upstream/RawResourceDataSource;->a()Landroid/net/Uri;

    move-result-object v1

    new-instance v2, Lco/uk/getmondo/signup/identity_verification/fallback/VideoFallbackActivity$d;

    invoke-direct {v2, v3}, Lco/uk/getmondo/signup/identity_verification/fallback/VideoFallbackActivity$d;-><init>(Lcom/google/android/exoplayer2/upstream/RawResourceDataSource;)V

    check-cast v2, Lcom/google/android/exoplayer2/upstream/c$a;

    .line 81
    new-instance v3, Lcom/google/android/exoplayer2/extractor/c;

    invoke-direct {v3}, Lcom/google/android/exoplayer2/extractor/c;-><init>()V

    check-cast v3, Lcom/google/android/exoplayer2/extractor/h;

    move-object v5, v4

    .line 80
    invoke-direct/range {v0 .. v5}, Lcom/google/android/exoplayer2/source/f;-><init>(Landroid/net/Uri;Lcom/google/android/exoplayer2/upstream/c$a;Lcom/google/android/exoplayer2/extractor/h;Landroid/os/Handler;Lcom/google/android/exoplayer2/source/f$a;)V

    .line 82
    invoke-direct {p0}, Lco/uk/getmondo/signup/identity_verification/fallback/VideoFallbackActivity;->v()Lcom/google/android/exoplayer2/t;

    move-result-object v2

    new-instance v1, Lcom/google/android/exoplayer2/source/g;

    check-cast v0, Lcom/google/android/exoplayer2/source/i;

    invoke-direct {v1, v0}, Lcom/google/android/exoplayer2/source/g;-><init>(Lcom/google/android/exoplayer2/source/i;)V

    move-object v0, v1

    check-cast v0, Lcom/google/android/exoplayer2/source/i;

    invoke-virtual {v2, v0}, Lcom/google/android/exoplayer2/t;->a(Lcom/google/android/exoplayer2/source/i;)V

    .line 83
    invoke-direct {p0}, Lco/uk/getmondo/signup/identity_verification/fallback/VideoFallbackActivity;->v()Lcom/google/android/exoplayer2/t;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/exoplayer2/t;->a(Z)V

    .line 84
    invoke-direct {p0}, Lco/uk/getmondo/signup/identity_verification/fallback/VideoFallbackActivity;->v()Lcom/google/android/exoplayer2/t;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/exoplayer2/t;->a(F)V

    .line 85
    return-void
.end method

.method public a(Ljava/io/File;Ljava/lang/String;Ljava/lang/String;)V
    .locals 6

    .prologue
    const/4 v2, 0x4

    const/4 v3, 0x0

    const-string v0, "videoFile"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "sentence"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "textToRead"

    invoke-static {p3, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 158
    invoke-direct {p0, p2, p3}, Lco/uk/getmondo/signup/identity_verification/fallback/VideoFallbackActivity;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 159
    sget v0, Lco/uk/getmondo/c$a;->mainActionButton:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/identity_verification/fallback/VideoFallbackActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    const v1, 0x7f0a0230

    invoke-virtual {p0, v1}, Lco/uk/getmondo/signup/identity_verification/fallback/VideoFallbackActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 160
    sget v0, Lco/uk/getmondo/c$a;->retakeVideoButton:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/identity_verification/fallback/VideoFallbackActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setVisibility(I)V

    .line 161
    sget v0, Lco/uk/getmondo/c$a;->fallbackVideoExoPlayer:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/identity_verification/fallback/VideoFallbackActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/exoplayer2/ui/SimpleExoPlayerView;

    const/4 v1, 0x0

    check-cast v1, Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1}, Lcom/google/android/exoplayer2/ui/SimpleExoPlayerView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 163
    new-instance v0, Landroid/support/constraint/b;

    invoke-direct {v0}, Landroid/support/constraint/b;-><init>()V

    .line 164
    sget v1, Lco/uk/getmondo/c$a;->fallbackVideoConstraintLayout:I

    invoke-virtual {p0, v1}, Lco/uk/getmondo/signup/identity_verification/fallback/VideoFallbackActivity;->a(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/support/constraint/ConstraintLayout;

    invoke-virtual {v0, v1}, Landroid/support/constraint/b;->a(Landroid/support/constraint/ConstraintLayout;)V

    .line 165
    sget v1, Lco/uk/getmondo/c$a;->fallbackVideoExoPlayer:I

    invoke-virtual {p0, v1}, Lco/uk/getmondo/signup/identity_verification/fallback/VideoFallbackActivity;->a(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/android/exoplayer2/ui/SimpleExoPlayerView;

    invoke-virtual {v1}, Lcom/google/android/exoplayer2/ui/SimpleExoPlayerView;->getId()I

    move-result v1

    move v4, v2

    move v5, v3

    invoke-virtual/range {v0 .. v5}, Landroid/support/constraint/b;->a(IIIII)V

    .line 166
    sget v1, Lco/uk/getmondo/c$a;->fallbackVideoConstraintLayout:I

    invoke-virtual {p0, v1}, Lco/uk/getmondo/signup/identity_verification/fallback/VideoFallbackActivity;->a(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/support/constraint/ConstraintLayout;

    invoke-virtual {v0, v1}, Landroid/support/constraint/b;->b(Landroid/support/constraint/ConstraintLayout;)V

    .line 168
    invoke-static {p1}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v0

    const-string v1, "Uri.fromFile(videoFile)"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lco/uk/getmondo/signup/identity_verification/fallback/VideoFallbackActivity;->a(Landroid/net/Uri;)V

    .line 169
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 3

    .prologue
    const-string v0, "usedSelfiePath"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 88
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/fallback/VideoFallbackActivity;->i:Ljava/util/ArrayList;

    check-cast v0, Ljava/lang/Iterable;

    .line 252
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/File;

    .line 89
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v0}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, p1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    xor-int/lit8 v2, v2, 0x1

    if-eqz v2, :cond_0

    .line 90
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    .line 92
    :cond_0
    nop

    goto :goto_0

    .line 253
    :cond_1
    nop

    .line 93
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/fallback/VideoFallbackActivity;->i:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 94
    return-void
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    const-string v0, "sentence"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "textToRead"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 154
    invoke-direct {p0, p1, p2}, Lco/uk/getmondo/signup/identity_verification/fallback/VideoFallbackActivity;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 155
    return-void
.end method

.method public b()Lio/reactivex/n;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .prologue
    .line 109
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/fallback/VideoFallbackActivity;->g:Lcom/b/b/c;

    check-cast v0, Lio/reactivex/r;

    sget v1, Lco/uk/getmondo/c$a;->mainActionButton:I

    invoke-virtual {p0, v1}, Lco/uk/getmondo/signup/identity_verification/fallback/VideoFallbackActivity;->a(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    invoke-static {v1}, Lcom/b/a/c/c;->a(Landroid/view/View;)Lio/reactivex/n;

    move-result-object v1

    check-cast v1, Lio/reactivex/r;

    invoke-static {v0, v1}, Lio/reactivex/n;->merge(Lio/reactivex/r;Lio/reactivex/r;)Lio/reactivex/n;

    move-result-object v1

    new-instance v0, Lco/uk/getmondo/signup/identity_verification/fallback/VideoFallbackActivity$b;

    invoke-direct {v0, p0}, Lco/uk/getmondo/signup/identity_verification/fallback/VideoFallbackActivity$b;-><init>(Lco/uk/getmondo/signup/identity_verification/fallback/VideoFallbackActivity;)V

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {v1, v0}, Lio/reactivex/n;->map(Lio/reactivex/c/h;)Lio/reactivex/n;

    move-result-object v0

    const-string v1, "Observable.merge(permiss\u2026.map { hasPermissions() }"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public c()Lio/reactivex/n;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Ljava/io/File;",
            ">;"
        }
    .end annotation

    .prologue
    .line 113
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/fallback/VideoFallbackActivity;->h:Lcom/b/b/c;

    const-string v1, "videoTakenRelay"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lio/reactivex/n;

    return-object v0
.end method

.method public d()Lio/reactivex/n;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 117
    sget v0, Lco/uk/getmondo/c$a;->retakeVideoButton:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/identity_verification/fallback/VideoFallbackActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    invoke-static {v0}, Lcom/b/a/c/c;->a(Landroid/view/View;)Lio/reactivex/n;

    move-result-object v0

    const-string v1, "RxView.clicks(retakeVideoButton)"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public e()Lio/reactivex/n;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .prologue
    .line 121
    sget v0, Lco/uk/getmondo/c$a;->fallbackVideoMuteButton:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/identity_verification/fallback/VideoFallbackActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/signup/identity_verification/video/MuteButton;

    invoke-static {v0}, Lcom/b/a/c/c;->a(Landroid/view/View;)Lio/reactivex/n;

    move-result-object v1

    new-instance v0, Lco/uk/getmondo/signup/identity_verification/fallback/VideoFallbackActivity$c;

    invoke-direct {v0, p0}, Lco/uk/getmondo/signup/identity_verification/fallback/VideoFallbackActivity$c;-><init>(Lco/uk/getmondo/signup/identity_verification/fallback/VideoFallbackActivity;)V

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {v1, v0}, Lio/reactivex/n;->map(Lio/reactivex/c/h;)Lio/reactivex/n;

    move-result-object v0

    const-string v1, "RxView.clicks(fallbackVi\u2026VideoMuteButton.isMuted }"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public f()V
    .locals 3

    .prologue
    .line 125
    check-cast p0, Landroid/app/Activity;

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "android.permission.CAMERA"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "android.permission.RECORD_AUDIO"

    aput-object v2, v0, v1

    check-cast v0, [Ljava/lang/Object;

    .line 256
    check-cast v0, [Ljava/lang/String;

    .line 126
    const/16 v1, 0x3e9

    .line 125
    invoke-static {p0, v0, v1}, Landroid/support/v4/app/a;->a(Landroid/app/Activity;[Ljava/lang/String;I)V

    .line 127
    return-void
.end method

.method public g()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 130
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/fallback/VideoFallbackActivity;->c:Lco/uk/getmondo/signup/identity_verification/a/a;

    if-nez v0, :cond_0

    const-string v1, "fileGenerator"

    invoke-static {v1}, Lkotlin/d/b/l;->b(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0}, Lco/uk/getmondo/signup/identity_verification/a/a;->d()Ljava/io/File;

    move-result-object v1

    .line 131
    iput-object v1, p0, Lco/uk/getmondo/signup/identity_verification/fallback/VideoFallbackActivity;->j:Ljava/io/File;

    .line 132
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/fallback/VideoFallbackActivity;->i:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move-object v0, p0

    .line 134
    check-cast v0, Landroid/content/Context;

    const v2, 0x7f0a018b

    invoke-virtual {p0, v2}, Lco/uk/getmondo/signup/identity_verification/fallback/VideoFallbackActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2, v1}, Landroid/support/v4/content/FileProvider;->a(Landroid/content/Context;Ljava/lang/String;Ljava/io/File;)Landroid/net/Uri;

    move-result-object v0

    .line 137
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.media.action.VIDEO_CAPTURE"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 138
    const-string v2, "android.intent.extra.durationLimit"

    const/16 v3, 0xf

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v1

    .line 139
    const-string v2, "android.intent.extra.sizeLimit"

    const/high16 v3, 0x1400000

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v1

    .line 140
    const-string v2, "android.intent.extra.screenOrientation"

    invoke-virtual {v1, v2, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v1

    .line 141
    const-string v2, "output"

    check-cast v0, Landroid/os/Parcelable;

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v0

    .line 142
    const-string v1, "android.intent.extra.videoQuality"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    .line 144
    const-string v1, "android.intent.extras.CAMERA_FACING"

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    .line 146
    invoke-virtual {p0}, Lco/uk/getmondo/signup/identity_verification/fallback/VideoFallbackActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->resolveActivity(Landroid/content/pm/PackageManager;)Landroid/content/ComponentName;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 147
    const/16 v1, 0x3ea

    invoke-virtual {p0, v0, v1}, Lco/uk/getmondo/signup/identity_verification/fallback/VideoFallbackActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 150
    :goto_0
    return-void

    .line 149
    :cond_1
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Failed to find default camera to open"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    invoke-static {v0}, Ld/a/a;->a(Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public h()V
    .locals 6

    .prologue
    .line 188
    sget v0, Lco/uk/getmondo/c$a;->mainActionButton:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/identity_verification/fallback/VideoFallbackActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    const v1, 0x7f0a022e

    invoke-virtual {p0, v1}, Lco/uk/getmondo/signup/identity_verification/fallback/VideoFallbackActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 189
    sget v0, Lco/uk/getmondo/c$a;->retakeVideoButton:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/identity_verification/fallback/VideoFallbackActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 190
    invoke-virtual {p0}, Lco/uk/getmondo/signup/identity_verification/fallback/VideoFallbackActivity;->a()V

    .line 192
    sget v0, Lco/uk/getmondo/c$a;->fallbackVideoExoPlayer:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/identity_verification/fallback/VideoFallbackActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/exoplayer2/ui/SimpleExoPlayerView;

    const v1, 0x7f020212

    invoke-virtual {v0, v1}, Lcom/google/android/exoplayer2/ui/SimpleExoPlayerView;->setBackgroundResource(I)V

    .line 193
    new-instance v0, Landroid/support/constraint/b;

    invoke-direct {v0}, Landroid/support/constraint/b;-><init>()V

    .line 194
    sget v1, Lco/uk/getmondo/c$a;->fallbackVideoConstraintLayout:I

    invoke-virtual {p0, v1}, Lco/uk/getmondo/signup/identity_verification/fallback/VideoFallbackActivity;->a(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/support/constraint/ConstraintLayout;

    invoke-virtual {v0, v1}, Landroid/support/constraint/b;->a(Landroid/support/constraint/ConstraintLayout;)V

    .line 195
    sget v1, Lco/uk/getmondo/c$a;->fallbackVideoExoPlayer:I

    invoke-virtual {p0, v1}, Lco/uk/getmondo/signup/identity_verification/fallback/VideoFallbackActivity;->a(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/android/exoplayer2/ui/SimpleExoPlayerView;

    invoke-virtual {v1}, Lcom/google/android/exoplayer2/ui/SimpleExoPlayerView;->getId()I

    move-result v1

    const/4 v2, 0x4

    sget v3, Lco/uk/getmondo/c$a;->mainActionButton:I

    invoke-virtual {p0, v3}, Lco/uk/getmondo/signup/identity_verification/fallback/VideoFallbackActivity;->a(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/Button;

    invoke-virtual {v3}, Landroid/widget/Button;->getId()I

    move-result v3

    const/4 v4, 0x3

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/support/constraint/b;->a(IIIII)V

    .line 196
    sget v1, Lco/uk/getmondo/c$a;->fallbackVideoConstraintLayout:I

    invoke-virtual {p0, v1}, Lco/uk/getmondo/signup/identity_verification/fallback/VideoFallbackActivity;->a(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/support/constraint/ConstraintLayout;

    invoke-virtual {v0, v1}, Landroid/support/constraint/b;->b(Landroid/support/constraint/ConstraintLayout;)V

    .line 197
    return-void
.end method

.method public i()V
    .locals 6

    .prologue
    .line 200
    const v0, 0x7f0a0231

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/identity_verification/fallback/VideoFallbackActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    move-object v0, p0

    .line 201
    check-cast v0, Landroid/content/Context;

    invoke-virtual {p0}, Lco/uk/getmondo/signup/identity_verification/fallback/VideoFallbackActivity;->m()Landroid/view/View;

    move-result-object v2

    sget-object v3, Ljava/util/concurrent/TimeUnit;->MINUTES:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v4, 0x1

    invoke-virtual {v3, v4, v5}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v4

    long-to-int v3, v4

    const/4 v4, 0x1

    invoke-static {v0, v2, v1, v3, v4}, Lco/uk/getmondo/common/ui/i;->a(Landroid/content/Context;Landroid/view/View;Ljava/lang/String;IZ)Landroid/support/design/widget/Snackbar;

    move-result-object v1

    .line 202
    const v2, 0x7f0a019a

    new-instance v0, Lco/uk/getmondo/signup/identity_verification/fallback/VideoFallbackActivity$e;

    invoke-direct {v0, v1}, Lco/uk/getmondo/signup/identity_verification/fallback/VideoFallbackActivity$e;-><init>(Landroid/support/design/widget/Snackbar;)V

    check-cast v0, Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2, v0}, Landroid/support/design/widget/Snackbar;->a(ILandroid/view/View$OnClickListener;)Landroid/support/design/widget/Snackbar;

    move-result-object v0

    .line 203
    check-cast p0, Landroid/content/Context;

    const v2, 0x7f0f00ea

    invoke-static {p0, v2}, Landroid/support/v4/content/a;->c(Landroid/content/Context;I)I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/support/design/widget/Snackbar;->e(I)Landroid/support/design/widget/Snackbar;

    .line 204
    invoke-virtual {v1}, Landroid/support/design/widget/Snackbar;->c()V

    .line 205
    return-void
.end method

.method public j()V
    .locals 2

    .prologue
    .line 230
    sget v0, Lco/uk/getmondo/c$a;->fallbackVideoMuteButton:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/identity_verification/fallback/VideoFallbackActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/signup/identity_verification/video/MuteButton;

    invoke-virtual {v0}, Lco/uk/getmondo/signup/identity_verification/video/MuteButton;->b()V

    .line 231
    invoke-direct {p0}, Lco/uk/getmondo/signup/identity_verification/fallback/VideoFallbackActivity;->v()Lcom/google/android/exoplayer2/t;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/exoplayer2/t;->a(F)V

    .line 232
    return-void
.end method

.method public k()V
    .locals 2

    .prologue
    .line 235
    sget v0, Lco/uk/getmondo/c$a;->fallbackVideoMuteButton:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/identity_verification/fallback/VideoFallbackActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/signup/identity_verification/video/MuteButton;

    invoke-virtual {v0}, Lco/uk/getmondo/signup/identity_verification/video/MuteButton;->b()V

    .line 236
    invoke-direct {p0}, Lco/uk/getmondo/signup/identity_verification/fallback/VideoFallbackActivity;->v()Lcom/google/android/exoplayer2/t;

    move-result-object v0

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Lcom/google/android/exoplayer2/t;->a(F)V

    .line 237
    return-void
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 2

    .prologue
    .line 217
    const/16 v0, 0x3ea

    if-ne p1, v0, :cond_1

    const/4 v0, -0x1

    if-ne p2, v0, :cond_1

    .line 218
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/fallback/VideoFallbackActivity;->h:Lcom/b/b/c;

    iget-object v1, p0, Lco/uk/getmondo/signup/identity_verification/fallback/VideoFallbackActivity;->j:Ljava/io/File;

    if-nez v1, :cond_0

    invoke-static {}, Lkotlin/d/b/l;->a()V

    :cond_0
    invoke-virtual {v0, v1}, Lcom/b/b/c;->a(Ljava/lang/Object;)V

    .line 221
    :goto_0
    return-void

    .line 220
    :cond_1
    invoke-super {p0, p1, p2, p3}, Lco/uk/getmondo/common/activities/b;->onActivityResult(IILandroid/content/Intent;)V

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 61
    invoke-super {p0, p1}, Lco/uk/getmondo/common/activities/b;->onCreate(Landroid/os/Bundle;)V

    .line 63
    const v0, 0x7f050074

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/identity_verification/fallback/VideoFallbackActivity;->setContentView(I)V

    .line 65
    invoke-virtual {p0}, Lco/uk/getmondo/signup/identity_verification/fallback/VideoFallbackActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "KEY_SIGNUP_SOURCE"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    if-nez v0, :cond_0

    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type co.uk.getmondo.api.model.signup.SignupSource"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    check-cast v0, Lco/uk/getmondo/api/model/signup/SignupSource;

    .line 66
    invoke-virtual {p0}, Lco/uk/getmondo/signup/identity_verification/fallback/VideoFallbackActivity;->l()Lco/uk/getmondo/common/h/b/b;

    move-result-object v1

    .line 67
    new-instance v2, Lco/uk/getmondo/signup/identity_verification/p;

    sget-object v3, Lco/uk/getmondo/signup/identity_verification/a/j;->b:Lco/uk/getmondo/signup/identity_verification/a/j;

    invoke-direct {v2, v3, v0}, Lco/uk/getmondo/signup/identity_verification/p;-><init>(Lco/uk/getmondo/signup/identity_verification/a/j;Lco/uk/getmondo/api/model/signup/SignupSource;)V

    invoke-interface {v1, v2}, Lco/uk/getmondo/common/h/b/b;->a(Lco/uk/getmondo/signup/identity_verification/p;)Lco/uk/getmondo/signup/identity_verification/o;

    move-result-object v0

    .line 68
    invoke-interface {v0, p0}, Lco/uk/getmondo/signup/identity_verification/o;->a(Lco/uk/getmondo/signup/identity_verification/fallback/VideoFallbackActivity;)V

    .line 69
    iget-object v1, p0, Lco/uk/getmondo/signup/identity_verification/fallback/VideoFallbackActivity;->b:Lco/uk/getmondo/signup/identity_verification/fallback/h;

    if-nez v1, :cond_1

    const-string v0, "presenter"

    invoke-static {v0}, Lkotlin/d/b/l;->b(Ljava/lang/String;)V

    :cond_1
    move-object v0, p0

    check-cast v0, Lco/uk/getmondo/signup/identity_verification/fallback/h$a;

    invoke-virtual {v1, v0}, Lco/uk/getmondo/signup/identity_verification/fallback/h;->a(Lco/uk/getmondo/signup/identity_verification/fallback/h$a;)V

    .line 71
    invoke-virtual {p0}, Lco/uk/getmondo/signup/identity_verification/fallback/VideoFallbackActivity;->a()V

    .line 72
    return-void
.end method

.method protected onDestroy()V
    .locals 3

    .prologue
    .line 97
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/fallback/VideoFallbackActivity;->i:Ljava/util/ArrayList;

    check-cast v0, Ljava/lang/Iterable;

    .line 254
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/File;

    .line 98
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 99
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    .line 101
    :cond_0
    nop

    goto :goto_0

    .line 255
    :cond_1
    nop

    .line 102
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/fallback/VideoFallbackActivity;->i:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 103
    invoke-direct {p0}, Lco/uk/getmondo/signup/identity_verification/fallback/VideoFallbackActivity;->v()Lcom/google/android/exoplayer2/t;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/t;->d()V

    .line 104
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/fallback/VideoFallbackActivity;->b:Lco/uk/getmondo/signup/identity_verification/fallback/h;

    if-nez v0, :cond_2

    const-string v1, "presenter"

    invoke-static {v1}, Lkotlin/d/b/l;->b(Ljava/lang/String;)V

    :cond_2
    invoke-virtual {v0}, Lco/uk/getmondo/signup/identity_verification/fallback/h;->b()V

    .line 105
    invoke-super {p0}, Lco/uk/getmondo/common/activities/b;->onDestroy()V

    .line 106
    return-void
.end method

.method public onRequestPermissionsResult(I[Ljava/lang/String;[I)V
    .locals 3

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    const-string v2, "permissions"

    invoke-static {p2, v2}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v2, "grantResults"

    invoke-static {p3, v2}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 208
    const/16 v2, 0x3e9

    if-ne p1, v2, :cond_2

    array-length v2, p3

    if-nez v2, :cond_0

    move v2, v0

    :goto_0
    if-nez v2, :cond_1

    :goto_1
    if-eqz v0, :cond_2

    aget v0, p3, v1

    if-nez v0, :cond_2

    .line 210
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/fallback/VideoFallbackActivity;->g:Lcom/b/b/c;

    sget-object v1, Lkotlin/n;->a:Lkotlin/n;

    invoke-virtual {v0, v1}, Lcom/b/b/c;->a(Ljava/lang/Object;)V

    .line 213
    :goto_2
    return-void

    :cond_0
    move v2, v1

    .line 208
    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_1

    .line 212
    :cond_2
    invoke-super {p0, p1, p2, p3}, Lco/uk/getmondo/common/activities/b;->onRequestPermissionsResult(I[Ljava/lang/String;[I)V

    goto :goto_2
.end method
