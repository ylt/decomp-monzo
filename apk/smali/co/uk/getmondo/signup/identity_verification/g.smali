.class public final Lco/uk/getmondo/signup/identity_verification/g;
.super Lco/uk/getmondo/common/ui/b;
.source "IdentityDocumentsPresenter.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lco/uk/getmondo/signup/identity_verification/g$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lco/uk/getmondo/common/ui/b",
        "<",
        "Lco/uk/getmondo/signup/identity_verification/g$a;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000P\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0003\n\u0002\u0010\u0002\n\u0002\u0008\u0003\u0008\u0007\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00020\u0001:\u0001\u001bB]\u0008\u0007\u0012\u0008\u0008\u0001\u0010\u0003\u001a\u00020\u0004\u0012\u0008\u0008\u0001\u0010\u0005\u001a\u00020\u0004\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b\u0012\u0006\u0010\u000c\u001a\u00020\r\u0012\u0006\u0010\u000e\u001a\u00020\u000f\u0012\u0006\u0010\u0010\u001a\u00020\u0011\u0012\u0008\u0010\u0012\u001a\u0004\u0018\u00010\u0013\u0012\u0006\u0010\u0014\u001a\u00020\u0015\u00a2\u0006\u0002\u0010\u0016J\u0010\u0010\u0018\u001a\u00020\u00192\u0006\u0010\u001a\u001a\u00020\u0002H\u0016R\u000e\u0010\u0014\u001a\u00020\u0015X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000c\u001a\u00020\rX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0010\u001a\u00020\u0011X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0017\u001a\u00020\u0015X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0012\u001a\u0004\u0018\u00010\u0013X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u000fX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001c"
    }
    d2 = {
        "Lco/uk/getmondo/signup/identity_verification/IdentityDocumentsPresenter;",
        "Lco/uk/getmondo/common/ui/BasePresenter;",
        "Lco/uk/getmondo/signup/identity_verification/IdentityDocumentsPresenter$View;",
        "ioScheduler",
        "Lio/reactivex/Scheduler;",
        "uiScheduler",
        "apiErrorHandler",
        "Lco/uk/getmondo/common/errors/ApiErrorHandler;",
        "verificationManager",
        "Lco/uk/getmondo/signup/identity_verification/data/IdentityVerificationManager;",
        "fileValidator",
        "Lco/uk/getmondo/signup/identity_verification/data/FileValidator;",
        "analyticsService",
        "Lco/uk/getmondo/common/AnalyticsService;",
        "version",
        "Lco/uk/getmondo/signup/identity_verification/data/IdentityVerificationVersion;",
        "from",
        "Lco/uk/getmondo/api/model/tracking/Impression$KycFrom;",
        "rejectionReason",
        "",
        "allowSystemCamera",
        "",
        "(Lio/reactivex/Scheduler;Lio/reactivex/Scheduler;Lco/uk/getmondo/common/errors/ApiErrorHandler;Lco/uk/getmondo/signup/identity_verification/data/IdentityVerificationManager;Lco/uk/getmondo/signup/identity_verification/data/FileValidator;Lco/uk/getmondo/common/AnalyticsService;Lco/uk/getmondo/signup/identity_verification/data/IdentityVerificationVersion;Lco/uk/getmondo/api/model/tracking/Impression$KycFrom;Ljava/lang/String;Z)V",
        "pageViewTracked",
        "register",
        "",
        "view",
        "View",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field private c:Z

.field private final d:Lio/reactivex/u;

.field private final e:Lio/reactivex/u;

.field private final f:Lco/uk/getmondo/common/e/a;

.field private final g:Lco/uk/getmondo/signup/identity_verification/a/e;

.field private final h:Lco/uk/getmondo/signup/identity_verification/a/c;

.field private final i:Lco/uk/getmondo/common/a;

.field private final j:Lco/uk/getmondo/signup/identity_verification/a/j;

.field private final k:Lco/uk/getmondo/api/model/tracking/Impression$KycFrom;

.field private final l:Ljava/lang/String;

.field private m:Z


# direct methods
.method public constructor <init>(Lio/reactivex/u;Lio/reactivex/u;Lco/uk/getmondo/common/e/a;Lco/uk/getmondo/signup/identity_verification/a/e;Lco/uk/getmondo/signup/identity_verification/a/c;Lco/uk/getmondo/common/a;Lco/uk/getmondo/signup/identity_verification/a/j;Lco/uk/getmondo/api/model/tracking/Impression$KycFrom;Ljava/lang/String;Z)V
    .locals 1

    .prologue
    const-string v0, "ioScheduler"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "uiScheduler"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "apiErrorHandler"

    invoke-static {p3, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "verificationManager"

    invoke-static {p4, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "fileValidator"

    invoke-static {p5, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "analyticsService"

    invoke-static {p6, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "version"

    invoke-static {p7, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "from"

    invoke-static {p8, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 38
    invoke-direct {p0}, Lco/uk/getmondo/common/ui/b;-><init>()V

    iput-object p1, p0, Lco/uk/getmondo/signup/identity_verification/g;->d:Lio/reactivex/u;

    iput-object p2, p0, Lco/uk/getmondo/signup/identity_verification/g;->e:Lio/reactivex/u;

    iput-object p3, p0, Lco/uk/getmondo/signup/identity_verification/g;->f:Lco/uk/getmondo/common/e/a;

    iput-object p4, p0, Lco/uk/getmondo/signup/identity_verification/g;->g:Lco/uk/getmondo/signup/identity_verification/a/e;

    iput-object p5, p0, Lco/uk/getmondo/signup/identity_verification/g;->h:Lco/uk/getmondo/signup/identity_verification/a/c;

    iput-object p6, p0, Lco/uk/getmondo/signup/identity_verification/g;->i:Lco/uk/getmondo/common/a;

    iput-object p7, p0, Lco/uk/getmondo/signup/identity_verification/g;->j:Lco/uk/getmondo/signup/identity_verification/a/j;

    iput-object p8, p0, Lco/uk/getmondo/signup/identity_verification/g;->k:Lco/uk/getmondo/api/model/tracking/Impression$KycFrom;

    iput-object p9, p0, Lco/uk/getmondo/signup/identity_verification/g;->l:Ljava/lang/String;

    iput-boolean p10, p0, Lco/uk/getmondo/signup/identity_verification/g;->m:Z

    return-void
.end method

.method public static final synthetic a(Lco/uk/getmondo/signup/identity_verification/g;Z)V
    .locals 0

    .prologue
    .line 26
    iput-boolean p1, p0, Lco/uk/getmondo/signup/identity_verification/g;->c:Z

    return-void
.end method

.method public static final synthetic a(Lco/uk/getmondo/signup/identity_verification/g;)Z
    .locals 1

    .prologue
    .line 26
    iget-boolean v0, p0, Lco/uk/getmondo/signup/identity_verification/g;->m:Z

    return v0
.end method

.method public static final synthetic b(Lco/uk/getmondo/signup/identity_verification/g;)Lco/uk/getmondo/signup/identity_verification/a/j;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/g;->j:Lco/uk/getmondo/signup/identity_verification/a/j;

    return-object v0
.end method

.method public static final synthetic c(Lco/uk/getmondo/signup/identity_verification/g;)Lco/uk/getmondo/signup/identity_verification/a/c;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/g;->h:Lco/uk/getmondo/signup/identity_verification/a/c;

    return-object v0
.end method

.method public static final synthetic d(Lco/uk/getmondo/signup/identity_verification/g;)Z
    .locals 1

    .prologue
    .line 26
    iget-boolean v0, p0, Lco/uk/getmondo/signup/identity_verification/g;->c:Z

    return v0
.end method

.method public static final synthetic e(Lco/uk/getmondo/signup/identity_verification/g;)Lco/uk/getmondo/common/a;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/g;->i:Lco/uk/getmondo/common/a;

    return-object v0
.end method

.method public static final synthetic f(Lco/uk/getmondo/signup/identity_verification/g;)Lco/uk/getmondo/signup/identity_verification/a/e;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/g;->g:Lco/uk/getmondo/signup/identity_verification/a/e;

    return-object v0
.end method

.method public static final synthetic g(Lco/uk/getmondo/signup/identity_verification/g;)Lio/reactivex/u;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/g;->d:Lio/reactivex/u;

    return-object v0
.end method

.method public static final synthetic h(Lco/uk/getmondo/signup/identity_verification/g;)Lio/reactivex/u;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/g;->e:Lio/reactivex/u;

    return-object v0
.end method

.method public static final synthetic i(Lco/uk/getmondo/signup/identity_verification/g;)Lco/uk/getmondo/common/e/a;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/g;->f:Lco/uk/getmondo/common/e/a;

    return-object v0
.end method

.method public static final synthetic j(Lco/uk/getmondo/signup/identity_verification/g;)Lco/uk/getmondo/api/model/tracking/Impression$KycFrom;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/g;->k:Lco/uk/getmondo/api/model/tracking/Impression$KycFrom;

    return-object v0
.end method


# virtual methods
.method public bridge synthetic a(Lco/uk/getmondo/common/ui/f;)V
    .locals 0

    .prologue
    .line 26
    check-cast p1, Lco/uk/getmondo/signup/identity_verification/g$a;

    invoke-virtual {p0, p1}, Lco/uk/getmondo/signup/identity_verification/g;->a(Lco/uk/getmondo/signup/identity_verification/g$a;)V

    return-void
.end method

.method public a(Lco/uk/getmondo/signup/identity_verification/g$a;)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    const-string v0, "view"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v0, p1

    .line 42
    check-cast v0, Lco/uk/getmondo/common/ui/f;

    invoke-super {p0, v0}, Lco/uk/getmondo/common/ui/b;->a(Lco/uk/getmondo/common/ui/f;)V

    .line 44
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/g;->l:Ljava/lang/String;

    check-cast v0, Ljava/lang/CharSequence;

    if-eqz v0, :cond_0

    invoke-static {v0}, Lkotlin/h/j;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_4

    :cond_0
    move v0, v1

    :goto_0
    if-nez v0, :cond_1

    move v2, v1

    .line 45
    :cond_1
    if-eqz v2, :cond_3

    .line 46
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/g;->l:Ljava/lang/String;

    if-nez v0, :cond_2

    invoke-static {}, Lkotlin/d/b/l;->a()V

    :cond_2
    invoke-interface {p1, v0}, Lco/uk/getmondo/signup/identity_verification/g$a;->a(Ljava/lang/String;)V

    .line 49
    :cond_3
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/g;->j:Lco/uk/getmondo/signup/identity_verification/a/j;

    sget-object v1, Lco/uk/getmondo/signup/identity_verification/a/j;->b:Lco/uk/getmondo/signup/identity_verification/a/j;

    if-ne v0, v1, :cond_5

    .line 50
    iget-object v3, p0, Lco/uk/getmondo/signup/identity_verification/g;->b:Lio/reactivex/b/a;

    .line 67
    invoke-interface {p1}, Lco/uk/getmondo/signup/identity_verification/g$a;->a()Lio/reactivex/n;

    move-result-object v1

    .line 51
    new-instance v0, Lco/uk/getmondo/signup/identity_verification/g$b;

    invoke-direct {v0, p1}, Lco/uk/getmondo/signup/identity_verification/g$b;-><init>(Lco/uk/getmondo/signup/identity_verification/g$a;)V

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {v1, v0}, Lio/reactivex/n;->flatMapMaybe(Lio/reactivex/c/h;)Lio/reactivex/n;

    move-result-object v1

    .line 52
    new-instance v0, Lco/uk/getmondo/signup/identity_verification/g$l;

    invoke-direct {v0, p1}, Lco/uk/getmondo/signup/identity_verification/g$l;-><init>(Lco/uk/getmondo/signup/identity_verification/g$a;)V

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {v1, v0}, Lio/reactivex/n;->flatMapMaybe(Lio/reactivex/c/h;)Lio/reactivex/n;

    move-result-object v1

    .line 59
    new-instance v0, Lco/uk/getmondo/signup/identity_verification/g$m;

    invoke-direct {v0, p1}, Lco/uk/getmondo/signup/identity_verification/g$m;-><init>(Lco/uk/getmondo/signup/identity_verification/g$a;)V

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {v1, v0}, Lio/reactivex/n;->flatMapMaybe(Lio/reactivex/c/h;)Lio/reactivex/n;

    move-result-object v4

    .line 67
    new-instance v0, Lco/uk/getmondo/signup/identity_verification/g$n;

    invoke-direct {v0, p0, p1}, Lco/uk/getmondo/signup/identity_verification/g$n;-><init>(Lco/uk/getmondo/signup/identity_verification/g;Lco/uk/getmondo/signup/identity_verification/g$a;)V

    check-cast v0, Lio/reactivex/c/g;

    .line 77
    sget-object v1, Lco/uk/getmondo/signup/identity_verification/g$o;->a:Lco/uk/getmondo/signup/identity_verification/g$o;

    check-cast v1, Lio/reactivex/c/g;

    .line 67
    invoke-virtual {v4, v0, v1}, Lio/reactivex/n;->subscribe(Lio/reactivex/c/g;Lio/reactivex/c/g;)Lio/reactivex/b/b;

    move-result-object v0

    const-string v1, "view.onTakePhotoClicked(\u2026     }, { Timber.e(it) })"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 50
    invoke-static {v3, v0}, Lco/uk/getmondo/common/j/f;->a(Lio/reactivex/b/a;Lio/reactivex/b/b;)Lio/reactivex/b/a;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/signup/identity_verification/g;->b:Lio/reactivex/b/a;

    .line 79
    iget-object v1, p0, Lco/uk/getmondo/signup/identity_verification/g;->b:Lio/reactivex/b/a;

    .line 80
    invoke-interface {p1}, Lco/uk/getmondo/signup/identity_verification/g$a;->e()Lio/reactivex/n;

    move-result-object v3

    new-instance v0, Lco/uk/getmondo/signup/identity_verification/g$p;

    invoke-direct {v0, p0, p1}, Lco/uk/getmondo/signup/identity_verification/g$p;-><init>(Lco/uk/getmondo/signup/identity_verification/g;Lco/uk/getmondo/signup/identity_verification/g$a;)V

    check-cast v0, Lio/reactivex/c/g;

    invoke-virtual {v3, v0}, Lio/reactivex/n;->subscribe(Lio/reactivex/c/g;)Lio/reactivex/b/b;

    move-result-object v0

    const-string v3, "view.onDocumentCountrySe\u2026  }\n                    }"

    invoke-static {v0, v3}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 79
    invoke-static {v1, v0}, Lco/uk/getmondo/common/j/f;->a(Lio/reactivex/b/a;Lio/reactivex/b/b;)Lio/reactivex/b/a;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/signup/identity_verification/g;->b:Lio/reactivex/b/a;

    .line 93
    :goto_1
    iget-object v3, p0, Lco/uk/getmondo/signup/identity_verification/g;->b:Lio/reactivex/b/a;

    .line 96
    invoke-interface {p1}, Lco/uk/getmondo/signup/identity_verification/g$a;->b()Lio/reactivex/n;

    move-result-object v4

    new-instance v0, Lco/uk/getmondo/signup/identity_verification/g$c;

    invoke-direct {v0, p0, p1}, Lco/uk/getmondo/signup/identity_verification/g$c;-><init>(Lco/uk/getmondo/signup/identity_verification/g;Lco/uk/getmondo/signup/identity_verification/g$a;)V

    check-cast v0, Lio/reactivex/c/g;

    .line 102
    sget-object v1, Lco/uk/getmondo/signup/identity_verification/g$d;->a:Lco/uk/getmondo/signup/identity_verification/g$d;

    check-cast v1, Lio/reactivex/c/g;

    .line 96
    invoke-virtual {v4, v0, v1}, Lio/reactivex/n;->subscribe(Lio/reactivex/c/g;Lio/reactivex/c/g;)Lio/reactivex/b/b;

    move-result-object v0

    const-string v1, "view.onTakeVideoClicked(\u2026en take video screen\") })"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 95
    invoke-static {v3, v0}, Lco/uk/getmondo/common/j/f;->a(Lio/reactivex/b/a;Lio/reactivex/b/b;)Lio/reactivex/b/a;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/signup/identity_verification/g;->b:Lio/reactivex/b/a;

    .line 104
    iget-object v3, p0, Lco/uk/getmondo/signup/identity_verification/g;->b:Lio/reactivex/b/a;

    .line 106
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/g;->g:Lco/uk/getmondo/signup/identity_verification/a/e;

    invoke-interface {v0}, Lco/uk/getmondo/signup/identity_verification/a/e;->b()Lio/reactivex/n;

    move-result-object v0

    .line 105
    iget-object v1, p0, Lco/uk/getmondo/signup/identity_verification/g;->g:Lco/uk/getmondo/signup/identity_verification/a/e;

    invoke-interface {v1}, Lco/uk/getmondo/signup/identity_verification/a/e;->c()Lio/reactivex/n;

    move-result-object v1

    .line 104
    invoke-static {v0, v1}, Lco/uk/getmondo/common/j/f;->a(Lio/reactivex/n;Lio/reactivex/n;)Lio/reactivex/n;

    move-result-object v4

    .line 106
    new-instance v0, Lco/uk/getmondo/signup/identity_verification/g$e;

    invoke-direct {v0, p0, p1, v2}, Lco/uk/getmondo/signup/identity_verification/g$e;-><init>(Lco/uk/getmondo/signup/identity_verification/g;Lco/uk/getmondo/signup/identity_verification/g$a;Z)V

    check-cast v0, Lio/reactivex/c/g;

    .line 130
    sget-object v1, Lco/uk/getmondo/signup/identity_verification/g$f;->a:Lco/uk/getmondo/signup/identity_verification/g$f;

    check-cast v1, Lio/reactivex/c/g;

    .line 106
    invoke-virtual {v4, v0, v1}, Lio/reactivex/n;->subscribe(Lio/reactivex/c/g;Lio/reactivex/c/g;)Lio/reactivex/b/b;

    move-result-object v0

    const-string v1, "combineLatest(verificati\u2026ate of submit button\") })"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 104
    invoke-static {v3, v0}, Lco/uk/getmondo/common/j/f;->a(Lio/reactivex/b/a;Lio/reactivex/b/b;)Lio/reactivex/b/a;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/signup/identity_verification/g;->b:Lio/reactivex/b/a;

    .line 132
    iget-object v2, p0, Lco/uk/getmondo/signup/identity_verification/g;->b:Lio/reactivex/b/a;

    .line 150
    invoke-interface {p1}, Lco/uk/getmondo/signup/identity_verification/g$a;->c()Lio/reactivex/n;

    move-result-object v1

    .line 133
    new-instance v0, Lco/uk/getmondo/signup/identity_verification/g$g;

    invoke-direct {v0, p1}, Lco/uk/getmondo/signup/identity_verification/g$g;-><init>(Lco/uk/getmondo/signup/identity_verification/g$a;)V

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {v1, v0}, Lio/reactivex/n;->flatMap(Lio/reactivex/c/h;)Lio/reactivex/n;

    move-result-object v1

    .line 134
    new-instance v0, Lco/uk/getmondo/signup/identity_verification/g$h;

    invoke-direct {v0, p0, p1}, Lco/uk/getmondo/signup/identity_verification/g$h;-><init>(Lco/uk/getmondo/signup/identity_verification/g;Lco/uk/getmondo/signup/identity_verification/g$a;)V

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {v1, v0}, Lio/reactivex/n;->flatMapMaybe(Lio/reactivex/c/h;)Lio/reactivex/n;

    move-result-object v1

    .line 149
    new-instance v0, Lco/uk/getmondo/signup/identity_verification/g$i;

    invoke-direct {v0, p0}, Lco/uk/getmondo/signup/identity_verification/g$i;-><init>(Lco/uk/getmondo/signup/identity_verification/g;)V

    check-cast v0, Lio/reactivex/c/g;

    invoke-virtual {v1, v0}, Lio/reactivex/n;->doOnNext(Lio/reactivex/c/g;)Lio/reactivex/n;

    move-result-object v3

    .line 150
    new-instance v0, Lco/uk/getmondo/signup/identity_verification/g$j;

    invoke-direct {v0, p0, p1}, Lco/uk/getmondo/signup/identity_verification/g$j;-><init>(Lco/uk/getmondo/signup/identity_verification/g;Lco/uk/getmondo/signup/identity_verification/g$a;)V

    check-cast v0, Lio/reactivex/c/g;

    .line 151
    sget-object v1, Lco/uk/getmondo/signup/identity_verification/g$k;->a:Lco/uk/getmondo/signup/identity_verification/g$k;

    check-cast v1, Lio/reactivex/c/g;

    .line 150
    invoke-virtual {v3, v0, v1}, Lio/reactivex/n;->subscribe(Lio/reactivex/c/g;Lio/reactivex/c/g;)Lio/reactivex/b/b;

    move-result-object v0

    const-string v1, "view.onSubmitVerificatio\u2026rification documents\") })"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 132
    invoke-static {v2, v0}, Lco/uk/getmondo/common/j/f;->a(Lio/reactivex/b/a;Lio/reactivex/b/b;)Lio/reactivex/b/a;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/signup/identity_verification/g;->b:Lio/reactivex/b/a;

    .line 152
    return-void

    :cond_4
    move v0, v2

    .line 44
    goto/16 :goto_0

    .line 88
    :cond_5
    iget-object v3, p0, Lco/uk/getmondo/signup/identity_verification/g;->b:Lio/reactivex/b/a;

    .line 90
    invoke-interface {p1}, Lco/uk/getmondo/signup/identity_verification/g$a;->a()Lio/reactivex/n;

    move-result-object v1

    .line 89
    new-instance v0, Lco/uk/getmondo/signup/identity_verification/g$q;

    invoke-direct {v0, p1}, Lco/uk/getmondo/signup/identity_verification/g$q;-><init>(Lco/uk/getmondo/signup/identity_verification/g$a;)V

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {v1, v0}, Lio/reactivex/n;->flatMapMaybe(Lio/reactivex/c/h;)Lio/reactivex/n;

    move-result-object v4

    .line 90
    new-instance v0, Lco/uk/getmondo/signup/identity_verification/g$r;

    invoke-direct {v0, p0, p1}, Lco/uk/getmondo/signup/identity_verification/g$r;-><init>(Lco/uk/getmondo/signup/identity_verification/g;Lco/uk/getmondo/signup/identity_verification/g$a;)V

    check-cast v0, Lio/reactivex/c/g;

    .line 92
    sget-object v1, Lco/uk/getmondo/signup/identity_verification/g$s;->a:Lco/uk/getmondo/signup/identity_verification/g$s;

    check-cast v1, Lio/reactivex/c/g;

    .line 90
    invoke-virtual {v4, v0, v1}, Lio/reactivex/n;->subscribe(Lio/reactivex/c/g;Lio/reactivex/c/g;)Lio/reactivex/b/b;

    move-result-object v0

    const-string v1, "view.onTakePhotoClicked(\u2026     }, { Timber.e(it) })"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 88
    invoke-static {v3, v0}, Lco/uk/getmondo/common/j/f;->a(Lio/reactivex/b/a;Lio/reactivex/b/b;)Lio/reactivex/b/a;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/signup/identity_verification/g;->b:Lio/reactivex/b/a;

    goto/16 :goto_1
.end method
