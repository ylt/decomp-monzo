.class final Lco/uk/getmondo/signup/identity_verification/aa$c;
.super Ljava/lang/Object;
.source "VerificationPendingPresenter.kt"

# interfaces
.implements Lio/reactivex/c/g;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lco/uk/getmondo/signup/identity_verification/aa;->a(Lco/uk/getmondo/signup/identity_verification/aa$a;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/g",
        "<",
        "Lco/uk/getmondo/api/model/identity_verification/IdentityVerification;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "",
        "identityVerification",
        "Lco/uk/getmondo/api/model/identity_verification/IdentityVerification;",
        "kotlin.jvm.PlatformType",
        "accept"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field final synthetic a:Lco/uk/getmondo/signup/identity_verification/aa;

.field final synthetic b:Lco/uk/getmondo/signup/identity_verification/aa$a;


# direct methods
.method constructor <init>(Lco/uk/getmondo/signup/identity_verification/aa;Lco/uk/getmondo/signup/identity_verification/aa$a;)V
    .locals 0

    iput-object p1, p0, Lco/uk/getmondo/signup/identity_verification/aa$c;->a:Lco/uk/getmondo/signup/identity_verification/aa;

    iput-object p2, p0, Lco/uk/getmondo/signup/identity_verification/aa$c;->b:Lco/uk/getmondo/signup/identity_verification/aa$a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lco/uk/getmondo/api/model/identity_verification/IdentityVerification;)V
    .locals 4

    .prologue
    .line 46
    invoke-virtual {p1}, Lco/uk/getmondo/api/model/identity_verification/IdentityVerification;->b()Lco/uk/getmondo/api/model/identity_verification/IdentityVerification$Status;

    move-result-object v0

    if-nez v0, :cond_0

    .line 60
    :goto_0
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unexpected identity verification: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    invoke-static {v0}, Ld/a/a;->a(Ljava/lang/Throwable;)V

    .line 61
    :goto_1
    return-void

    .line 46
    :cond_0
    sget-object v1, Lco/uk/getmondo/signup/identity_verification/ab;->b:[I

    invoke-virtual {v0}, Lco/uk/getmondo/api/model/identity_verification/IdentityVerification$Status;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 47
    :pswitch_0
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/aa$c;->b:Lco/uk/getmondo/signup/identity_verification/aa$a;

    invoke-interface {v0}, Lco/uk/getmondo/signup/identity_verification/aa$a;->f()V

    goto :goto_1

    .line 48
    :pswitch_1
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/aa$c;->b:Lco/uk/getmondo/signup/identity_verification/aa$a;

    invoke-virtual {p1}, Lco/uk/getmondo/api/model/identity_verification/IdentityVerification;->a()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lco/uk/getmondo/signup/identity_verification/aa$a;->a(Ljava/lang/String;)V

    goto :goto_1

    .line 50
    :pswitch_2
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/aa$c;->a:Lco/uk/getmondo/signup/identity_verification/aa;

    invoke-static {v0}, Lco/uk/getmondo/signup/identity_verification/aa;->e(Lco/uk/getmondo/signup/identity_verification/aa;)Lco/uk/getmondo/common/accounts/d;

    move-result-object v0

    invoke-virtual {v0}, Lco/uk/getmondo/common/accounts/d;->b()Lco/uk/getmondo/d/ak;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lco/uk/getmondo/d/ak;->a()Lco/uk/getmondo/d/ak$a;

    move-result-object v0

    move-object v1, v0

    .line 51
    :goto_2
    if-nez v1, :cond_2

    .line 54
    :goto_3
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unexpected user state: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    invoke-static {v0}, Ld/a/a;->a(Ljava/lang/Throwable;)V

    goto :goto_1

    .line 50
    :cond_1
    const/4 v0, 0x0

    move-object v1, v0

    goto :goto_2

    .line 51
    :cond_2
    sget-object v0, Lco/uk/getmondo/signup/identity_verification/ab;->a:[I

    invoke-virtual {v1}, Lco/uk/getmondo/d/ak$a;->ordinal()I

    move-result v2

    aget v0, v0, v2

    packed-switch v0, :pswitch_data_1

    goto :goto_3

    .line 52
    :pswitch_3
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/aa$c;->b:Lco/uk/getmondo/signup/identity_verification/aa$a;

    invoke-interface {v0}, Lco/uk/getmondo/signup/identity_verification/aa$a;->h()V

    goto :goto_1

    .line 53
    :pswitch_4
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/aa$c;->b:Lco/uk/getmondo/signup/identity_verification/aa$a;

    invoke-interface {v0}, Lco/uk/getmondo/signup/identity_verification/aa$a;->e()V

    goto :goto_1

    .line 58
    :pswitch_5
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/aa$c;->b:Lco/uk/getmondo/signup/identity_verification/aa$a;

    invoke-interface {v0}, Lco/uk/getmondo/signup/identity_verification/aa$a;->g()V

    goto :goto_1

    .line 59
    :pswitch_6
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/aa$c;->b:Lco/uk/getmondo/signup/identity_verification/aa$a;

    invoke-interface {v0}, Lco/uk/getmondo/signup/identity_verification/aa$a;->d()V

    goto :goto_1

    .line 46
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_2
        :pswitch_5
        :pswitch_6
    .end packed-switch

    .line 51
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 22
    check-cast p1, Lco/uk/getmondo/api/model/identity_verification/IdentityVerification;

    invoke-virtual {p0, p1}, Lco/uk/getmondo/signup/identity_verification/aa$c;->a(Lco/uk/getmondo/api/model/identity_verification/IdentityVerification;)V

    return-void
.end method
