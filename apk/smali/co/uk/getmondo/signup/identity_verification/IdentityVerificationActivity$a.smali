.class public final Lco/uk/getmondo/signup/identity_verification/IdentityVerificationActivity$a;
.super Ljava/lang/Object;
.source "IdentityVerificationActivity.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lco/uk/getmondo/signup/identity_verification/IdentityVerificationActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000:\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J>\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\u000c2\u0006\u0010\r\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u00102\u0006\u0010\u0011\u001a\u00020\u00122\u0008\u0008\u0002\u0010\u0013\u001a\u00020\u00142\n\u0008\u0002\u0010\u0015\u001a\u0004\u0018\u00010\u0004H\u0007R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0082T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0004X\u0082T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0004X\u0082T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0008\u001a\u00020\u0004X\u0082T\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0016"
    }
    d2 = {
        "Lco/uk/getmondo/signup/identity_verification/IdentityVerificationActivity$Companion;",
        "",
        "()V",
        "KEY_ENTRY_POINT",
        "",
        "KEY_REJECTION_REASON",
        "KEY_SIGNUP_ENTRY_POINT",
        "KEY_SIGNUP_SOURCE",
        "KEY_VERSION",
        "buildIntent",
        "Landroid/content/Intent;",
        "context",
        "Landroid/content/Context;",
        "version",
        "Lco/uk/getmondo/signup/identity_verification/data/IdentityVerificationVersion;",
        "from",
        "Lco/uk/getmondo/api/model/tracking/Impression$KycFrom;",
        "signupSource",
        "Lco/uk/getmondo/api/model/signup/SignupSource;",
        "signupEntryPoint",
        "Lco/uk/getmondo/signup/SignupEntryPoint;",
        "rejectedReason",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 130
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/d/b/i;)V
    .locals 0

    .prologue
    .line 130
    invoke-direct {p0}, Lco/uk/getmondo/signup/identity_verification/IdentityVerificationActivity$a;-><init>()V

    return-void
.end method

.method public static bridge synthetic a(Lco/uk/getmondo/signup/identity_verification/IdentityVerificationActivity$a;Landroid/content/Context;Lco/uk/getmondo/signup/identity_verification/a/j;Lco/uk/getmondo/api/model/tracking/Impression$KycFrom;Lco/uk/getmondo/api/model/signup/SignupSource;Lco/uk/getmondo/signup/j;Ljava/lang/String;ILjava/lang/Object;)Landroid/content/Intent;
    .locals 7

    .prologue
    and-int/lit8 v0, p7, 0x10

    if-eqz v0, :cond_1

    .line 147
    sget-object v5, Lco/uk/getmondo/signup/j;->b:Lco/uk/getmondo/signup/j;

    :goto_0
    and-int/lit8 v0, p7, 0x20

    if-eqz v0, :cond_0

    .line 148
    const/4 v0, 0x0

    check-cast v0, Ljava/lang/String;

    move-object v6, v0

    :goto_1
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-virtual/range {v0 .. v6}, Lco/uk/getmondo/signup/identity_verification/IdentityVerificationActivity$a;->a(Landroid/content/Context;Lco/uk/getmondo/signup/identity_verification/a/j;Lco/uk/getmondo/api/model/tracking/Impression$KycFrom;Lco/uk/getmondo/api/model/signup/SignupSource;Lco/uk/getmondo/signup/j;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    return-object v0

    :cond_0
    move-object v6, p6

    goto :goto_1

    :cond_1
    move-object v5, p5

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/content/Context;Lco/uk/getmondo/signup/identity_verification/a/j;Lco/uk/getmondo/api/model/tracking/Impression$KycFrom;Lco/uk/getmondo/api/model/signup/SignupSource;Lco/uk/getmondo/signup/j;Ljava/lang/String;)Landroid/content/Intent;
    .locals 2

    .prologue
    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "version"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "from"

    invoke-static {p3, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "signupSource"

    invoke-static {p4, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "signupEntryPoint"

    invoke-static {p5, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 150
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lco/uk/getmondo/signup/identity_verification/IdentityVerificationActivity;

    invoke-direct {v0, p1, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 151
    const-string v1, "KEY_VERSION"

    check-cast p2, Ljava/io/Serializable;

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    move-result-object v0

    .line 152
    const-string v1, "KEY_ENTRY_POINT"

    check-cast p3, Ljava/io/Serializable;

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    move-result-object v0

    .line 153
    const-string v1, "KEY_REJECTION_REASON"

    invoke-virtual {v0, v1, p6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 154
    const-string v1, "KEY_SIGNUP_SOURCE"

    check-cast p4, Ljava/io/Serializable;

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    move-result-object v0

    .line 155
    const-string v1, "KEY_SIGNUP_ENTRY_POINT"

    check-cast p5, Ljava/io/Serializable;

    invoke-virtual {v0, v1, p5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "Intent(context, Identity\u2026_POINT, signupEntryPoint)"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method
