.class public final Lco/uk/getmondo/signup/identity_verification/t;
.super Lco/uk/getmondo/common/f/a;
.source "IdentityVerificationOnboardingFragment.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lco/uk/getmondo/signup/identity_verification/t$b;,
        Lco/uk/getmondo/signup/identity_verification/t$a;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0010\u0011\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0006\u0018\u0000 -2\u00020\u0001:\u0002-.B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0013\u0010\u0016\u001a\u0008\u0012\u0004\u0012\u00020\u00180\u0017H\u0002\u00a2\u0006\u0002\u0010\u0019J\u0006\u0010\u001a\u001a\u00020\u001bJ\u0006\u0010\u001c\u001a\u00020\u001dJ\u0010\u0010\u001e\u001a\u00020\u001d2\u0006\u0010\u001f\u001a\u00020 H\u0016J\u0012\u0010!\u001a\u00020\u001d2\u0008\u0010\"\u001a\u0004\u0018\u00010#H\u0016J&\u0010$\u001a\u0004\u0018\u00010%2\u0006\u0010&\u001a\u00020\'2\u0008\u0010(\u001a\u0004\u0018\u00010)2\u0008\u0010\"\u001a\u0004\u0018\u00010#H\u0016J\u0008\u0010*\u001a\u00020\u001dH\u0016J\u001c\u0010+\u001a\u00020\u001d2\u0008\u0010,\u001a\u0004\u0018\u00010%2\u0008\u0010\"\u001a\u0004\u0018\u00010#H\u0016R\u001b\u0010\u0003\u001a\u00020\u00048BX\u0082\u0084\u0002\u00a2\u0006\u000c\n\u0004\u0008\u0007\u0010\u0008\u001a\u0004\u0008\u0005\u0010\u0006R\u001b\u0010\t\u001a\u00020\n8BX\u0082\u0084\u0002\u00a2\u0006\u000c\n\u0004\u0008\r\u0010\u0008\u001a\u0004\u0008\u000b\u0010\u000cR\u000e\u0010\u000e\u001a\u00020\u000fX\u0082.\u00a2\u0006\u0002\n\u0000R\u001e\u0010\u0010\u001a\u00020\u00118\u0006@\u0006X\u0087.\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u0012\u0010\u0013\"\u0004\u0008\u0014\u0010\u0015\u00a8\u0006/"
    }
    d2 = {
        "Lco/uk/getmondo/signup/identity_verification/IdentityVerificationOnboardingFragment;",
        "Lco/uk/getmondo/common/fragments/BaseFragment;",
        "()V",
        "from",
        "Lco/uk/getmondo/api/model/tracking/Impression$KycFrom;",
        "getFrom",
        "()Lco/uk/getmondo/api/model/tracking/Impression$KycFrom;",
        "from$delegate",
        "Lkotlin/Lazy;",
        "identityVerificationVersion",
        "Lco/uk/getmondo/signup/identity_verification/data/IdentityVerificationVersion;",
        "getIdentityVerificationVersion",
        "()Lco/uk/getmondo/signup/identity_verification/data/IdentityVerificationVersion;",
        "identityVerificationVersion$delegate",
        "listener",
        "Lco/uk/getmondo/signup/identity_verification/IdentityVerificationOnboardingFragment$StepListener;",
        "pageViewTracker",
        "Lco/uk/getmondo/common/pager/PageViewTracker;",
        "getPageViewTracker",
        "()Lco/uk/getmondo/common/pager/PageViewTracker;",
        "setPageViewTracker",
        "(Lco/uk/getmondo/common/pager/PageViewTracker;)V",
        "buildOnboardingPages",
        "",
        "Lco/uk/getmondo/common/pager/Page;",
        "()[Lco/uk/getmondo/common/pager/Page;",
        "canGoBack",
        "",
        "goBack",
        "",
        "onAttach",
        "context",
        "Landroid/content/Context;",
        "onCreate",
        "savedInstanceState",
        "Landroid/os/Bundle;",
        "onCreateView",
        "Landroid/view/View;",
        "inflater",
        "Landroid/view/LayoutInflater;",
        "container",
        "Landroid/view/ViewGroup;",
        "onDestroy",
        "onViewCreated",
        "view",
        "Companion",
        "StepListener",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# static fields
.field static final synthetic a:[Lkotlin/reflect/l;

.field public static final d:Lco/uk/getmondo/signup/identity_verification/t$a;


# instance fields
.field public c:Lco/uk/getmondo/common/pager/h;

.field private final e:Lkotlin/c;

.field private final f:Lkotlin/c;

.field private g:Lco/uk/getmondo/signup/identity_verification/t$b;

.field private h:Ljava/util/HashMap;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const/4 v0, 0x2

    new-array v1, v0, [Lkotlin/reflect/l;

    const/4 v2, 0x0

    new-instance v0, Lkotlin/d/b/w;

    const-class v3, Lco/uk/getmondo/signup/identity_verification/t;

    invoke-static {v3}, Lkotlin/d/b/y;->a(Ljava/lang/Class;)Lkotlin/reflect/c;

    move-result-object v3

    const-string v4, "identityVerificationVersion"

    const-string v5, "getIdentityVerificationVersion()Lco/uk/getmondo/signup/identity_verification/data/IdentityVerificationVersion;"

    invoke-direct {v0, v3, v4, v5}, Lkotlin/d/b/w;-><init>(Lkotlin/reflect/e;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v0}, Lkotlin/d/b/y;->a(Lkotlin/d/b/v;)Lkotlin/reflect/n;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/l;

    aput-object v0, v1, v2

    const/4 v2, 0x1

    new-instance v0, Lkotlin/d/b/w;

    const-class v3, Lco/uk/getmondo/signup/identity_verification/t;

    invoke-static {v3}, Lkotlin/d/b/y;->a(Ljava/lang/Class;)Lkotlin/reflect/c;

    move-result-object v3

    const-string v4, "from"

    const-string v5, "getFrom()Lco/uk/getmondo/api/model/tracking/Impression$KycFrom;"

    invoke-direct {v0, v3, v4, v5}, Lkotlin/d/b/w;-><init>(Lkotlin/reflect/e;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v0}, Lkotlin/d/b/y;->a(Lkotlin/d/b/v;)Lkotlin/reflect/n;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/l;

    aput-object v0, v1, v2

    sput-object v1, Lco/uk/getmondo/signup/identity_verification/t;->a:[Lkotlin/reflect/l;

    new-instance v0, Lco/uk/getmondo/signup/identity_verification/t$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lco/uk/getmondo/signup/identity_verification/t$a;-><init>(Lkotlin/d/b/i;)V

    sput-object v0, Lco/uk/getmondo/signup/identity_verification/t;->d:Lco/uk/getmondo/signup/identity_verification/t$a;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 20
    invoke-direct {p0}, Lco/uk/getmondo/common/f/a;-><init>()V

    .line 21
    new-instance v0, Lco/uk/getmondo/signup/identity_verification/t$d;

    invoke-direct {v0, p0}, Lco/uk/getmondo/signup/identity_verification/t$d;-><init>(Lco/uk/getmondo/signup/identity_verification/t;)V

    check-cast v0, Lkotlin/d/a/a;

    invoke-static {v0}, Lkotlin/d;->a(Lkotlin/d/a/a;)Lkotlin/c;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/signup/identity_verification/t;->e:Lkotlin/c;

    .line 22
    new-instance v0, Lco/uk/getmondo/signup/identity_verification/t$c;

    invoke-direct {v0, p0}, Lco/uk/getmondo/signup/identity_verification/t$c;-><init>(Lco/uk/getmondo/signup/identity_verification/t;)V

    check-cast v0, Lkotlin/d/a/a;

    invoke-static {v0}, Lkotlin/d;->a(Lkotlin/d/a/a;)Lkotlin/c;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/signup/identity_verification/t;->f:Lkotlin/c;

    return-void
.end method

.method public static final synthetic a(Lco/uk/getmondo/signup/identity_verification/t;)Lco/uk/getmondo/signup/identity_verification/t$b;
    .locals 2

    .prologue
    .line 20
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/t;->g:Lco/uk/getmondo/signup/identity_verification/t$b;

    if-nez v0, :cond_0

    const-string v1, "listener"

    invoke-static {v1}, Lkotlin/d/b/l;->b(Ljava/lang/String;)V

    :cond_0
    return-object v0
.end method

.method private final d()Lco/uk/getmondo/signup/identity_verification/a/j;
    .locals 3

    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/t;->e:Lkotlin/c;

    sget-object v1, Lco/uk/getmondo/signup/identity_verification/t;->a:[Lkotlin/reflect/l;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-interface {v0}, Lkotlin/c;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/signup/identity_verification/a/j;

    return-object v0
.end method

.method private final e()Lco/uk/getmondo/api/model/tracking/Impression$KycFrom;
    .locals 3

    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/t;->f:Lkotlin/c;

    sget-object v1, Lco/uk/getmondo/signup/identity_verification/t;->a:[Lkotlin/reflect/l;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-interface {v0}, Lkotlin/c;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/api/model/tracking/Impression$KycFrom;

    return-object v0
.end method

.method private final f()[Lco/uk/getmondo/common/pager/f;
    .locals 8

    .prologue
    .line 73
    invoke-direct {p0}, Lco/uk/getmondo/signup/identity_verification/t;->d()Lco/uk/getmondo/signup/identity_verification/a/j;

    move-result-object v0

    sget-object v1, Lco/uk/getmondo/signup/identity_verification/a/j;->a:Lco/uk/getmondo/signup/identity_verification/a/j;

    if-ne v0, v1, :cond_0

    .line 74
    const v0, 0x7f0a0207

    move v2, v0

    .line 78
    :goto_0
    const/4 v0, 0x3

    new-array v1, v0, [Lco/uk/getmondo/common/pager/f;

    .line 79
    const/4 v3, 0x0

    new-instance v0, Lco/uk/getmondo/common/pager/d;

    const v4, 0x7f0201c3

    .line 80
    const v5, 0x7f0a020a

    .line 82
    sget-object v6, Lco/uk/getmondo/api/model/tracking/Impression;->Companion:Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    invoke-direct {p0}, Lco/uk/getmondo/signup/identity_verification/t;->e()Lco/uk/getmondo/api/model/tracking/Impression$KycFrom;

    move-result-object v7

    invoke-virtual {v6, v7}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->a(Lco/uk/getmondo/api/model/tracking/Impression$KycFrom;)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v6

    .line 79
    invoke-direct {v0, v4, v5, v2, v6}, Lco/uk/getmondo/common/pager/d;-><init>(IIILco/uk/getmondo/api/model/tracking/Impression;)V

    check-cast v0, Lco/uk/getmondo/common/pager/f;

    aput-object v0, v1, v3

    .line 83
    const/4 v2, 0x1

    new-instance v0, Lco/uk/getmondo/common/pager/d;

    const v3, 0x7f0201c4

    .line 84
    const v4, 0x7f0a020b

    .line 85
    const v5, 0x7f0a0208

    .line 86
    sget-object v6, Lco/uk/getmondo/api/model/tracking/Impression;->Companion:Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    invoke-direct {p0}, Lco/uk/getmondo/signup/identity_verification/t;->e()Lco/uk/getmondo/api/model/tracking/Impression$KycFrom;

    move-result-object v7

    invoke-virtual {v6, v7}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->b(Lco/uk/getmondo/api/model/tracking/Impression$KycFrom;)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v6

    .line 83
    invoke-direct {v0, v3, v4, v5, v6}, Lco/uk/getmondo/common/pager/d;-><init>(IIILco/uk/getmondo/api/model/tracking/Impression;)V

    check-cast v0, Lco/uk/getmondo/common/pager/f;

    aput-object v0, v1, v2

    .line 87
    const/4 v2, 0x2

    new-instance v0, Lco/uk/getmondo/common/pager/d;

    const v3, 0x7f0201c5

    .line 88
    const v4, 0x7f0a020c

    .line 89
    const v5, 0x7f0a0209

    .line 90
    sget-object v6, Lco/uk/getmondo/api/model/tracking/Impression;->Companion:Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    invoke-direct {p0}, Lco/uk/getmondo/signup/identity_verification/t;->e()Lco/uk/getmondo/api/model/tracking/Impression$KycFrom;

    move-result-object v7

    invoke-virtual {v6, v7}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->c(Lco/uk/getmondo/api/model/tracking/Impression$KycFrom;)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v6

    .line 87
    invoke-direct {v0, v3, v4, v5, v6}, Lco/uk/getmondo/common/pager/d;-><init>(IIILco/uk/getmondo/api/model/tracking/Impression;)V

    check-cast v0, Lco/uk/getmondo/common/pager/f;

    aput-object v0, v1, v2

    move-object v0, v1

    check-cast v0, [Ljava/lang/Object;

    .line 78
    nop

    .line 117
    check-cast v0, [Lco/uk/getmondo/common/pager/f;

    return-object v0

    .line 76
    :cond_0
    const v0, 0x7f0a0206

    move v2, v0

    goto :goto_0
.end method


# virtual methods
.method public a(I)Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/t;->h:Ljava/util/HashMap;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lco/uk/getmondo/signup/identity_verification/t;->h:Ljava/util/HashMap;

    :cond_0
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/t;->h:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    if-nez v0, :cond_1

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getView()Landroid/view/View;

    move-result-object v0

    if-nez v0, :cond_2

    const/4 v0, 0x0

    :cond_1
    :goto_0
    return-object v0

    :cond_2
    invoke-virtual {v0, p1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/signup/identity_verification/t;->h:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 55
    sget v0, Lco/uk/getmondo/c$a;->idvOnboardingViewPager:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/identity_verification/t;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()V
    .locals 3

    .prologue
    .line 59
    sget v0, Lco/uk/getmondo/c$a;->idvOnboardingViewPager:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/identity_verification/t;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v4/view/ViewPager;

    sget v1, Lco/uk/getmondo/c$a;->idvOnboardingViewPager:I

    invoke-virtual {p0, v1}, Lco/uk/getmondo/signup/identity_verification/t;->a(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/support/v4/view/ViewPager;

    invoke-virtual {v1}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/view/ViewPager;->a(IZ)V

    .line 60
    return-void
.end method

.method public c()V
    .locals 1

    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/t;->h:Ljava/util/HashMap;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/t;->h:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    :cond_0
    return-void
.end method

.method public onAttach(Landroid/content/Context;)V
    .locals 2

    .prologue
    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 63
    invoke-super {p0, p1}, Lco/uk/getmondo/common/f/a;->onAttach(Landroid/content/Context;)V

    .line 65
    instance-of v0, p1, Lco/uk/getmondo/signup/identity_verification/t$b;

    if-eqz v0, :cond_0

    .line 66
    check-cast p1, Lco/uk/getmondo/signup/identity_verification/t$b;

    iput-object p1, p0, Lco/uk/getmondo/signup/identity_verification/t;->g:Lco/uk/getmondo/signup/identity_verification/t$b;

    .line 69
    return-void

    .line 68
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Activity must implement StepListener"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 28
    invoke-super {p0, p1}, Lco/uk/getmondo/common/f/a;->onCreate(Landroid/os/Bundle;)V

    .line 30
    invoke-virtual {p0}, Lco/uk/getmondo/signup/identity_verification/t;->B()Lco/uk/getmondo/common/h/b/b;

    move-result-object v0

    invoke-interface {v0, p0}, Lco/uk/getmondo/common/h/b/b;->a(Lco/uk/getmondo/signup/identity_verification/t;)V

    .line 31
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2

    .prologue
    const-string v0, "inflater"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 34
    const v0, 0x7f0500a0

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 94
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/t;->c:Lco/uk/getmondo/common/pager/h;

    if-nez v0, :cond_0

    const-string v1, "pageViewTracker"

    invoke-static {v1}, Lkotlin/d/b/l;->b(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0}, Lco/uk/getmondo/common/pager/h;->a()V

    .line 95
    invoke-super {p0}, Lco/uk/getmondo/common/f/a;->onDestroy()V

    .line 96
    return-void
.end method

.method public synthetic onDestroyView()V
    .locals 0

    invoke-super {p0}, Lco/uk/getmondo/common/f/a;->onDestroyView()V

    invoke-virtual {p0}, Lco/uk/getmondo/signup/identity_verification/t;->c()V

    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 38
    invoke-super {p0, p1, p2}, Lco/uk/getmondo/common/f/a;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 40
    sget v0, Lco/uk/getmondo/c$a;->idvOnboardingViewPager:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/identity_verification/t;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v4/view/ViewPager;

    new-instance v2, Lco/uk/getmondo/common/pager/GenericPagerAdapter;

    invoke-direct {p0}, Lco/uk/getmondo/signup/identity_verification/t;->f()[Lco/uk/getmondo/common/pager/f;

    move-result-object v1

    array-length v3, v1

    invoke-static {v1, v3}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Lco/uk/getmondo/common/pager/f;

    invoke-direct {v2, v1}, Lco/uk/getmondo/common/pager/GenericPagerAdapter;-><init>([Lco/uk/getmondo/common/pager/f;)V

    move-object v1, v2

    check-cast v1, Landroid/support/v4/view/p;

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setAdapter(Landroid/support/v4/view/p;)V

    .line 41
    iget-object v1, p0, Lco/uk/getmondo/signup/identity_verification/t;->c:Lco/uk/getmondo/common/pager/h;

    if-nez v1, :cond_0

    const-string v0, "pageViewTracker"

    invoke-static {v0}, Lkotlin/d/b/l;->b(Ljava/lang/String;)V

    :cond_0
    sget v0, Lco/uk/getmondo/c$a;->idvOnboardingViewPager:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/identity_verification/t;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v4/view/ViewPager;

    invoke-virtual {v1, v0}, Lco/uk/getmondo/common/pager/h;->a(Landroid/support/v4/view/ViewPager;)V

    .line 42
    sget v0, Lco/uk/getmondo/c$a;->idvOnboardingViewPagerIndicator:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/identity_verification/t;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lme/relex/circleindicator/CircleIndicator;

    sget v1, Lco/uk/getmondo/c$a;->idvOnboardingViewPager:I

    invoke-virtual {p0, v1}, Lco/uk/getmondo/signup/identity_verification/t;->a(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0, v1}, Lme/relex/circleindicator/CircleIndicator;->setViewPager(Landroid/support/v4/view/ViewPager;)V

    .line 44
    sget v0, Lco/uk/getmondo/c$a;->idvOnboardingNextButton:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/identity_verification/t;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    new-instance v1, Lco/uk/getmondo/signup/identity_verification/t$e;

    invoke-direct {v1, p0}, Lco/uk/getmondo/signup/identity_verification/t$e;-><init>(Lco/uk/getmondo/signup/identity_verification/t;)V

    check-cast v1, Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 52
    return-void
.end method
