.class public Lco/uk/getmondo/signup/identity_verification/id_picture/DocumentCameraActivity;
.super Lco/uk/getmondo/common/activities/b;
.source "DocumentCameraActivity.java"

# interfaces
.implements Lco/uk/getmondo/signup/identity_verification/id_picture/j$a;


# instance fields
.field a:Lco/uk/getmondo/signup/identity_verification/id_picture/j;

.field b:Lco/uk/getmondo/signup/identity_verification/a/a;

.field private c:Lcom/b/b/c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/b/b/c",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field cameraProgress:Landroid/widget/ProgressBar;
    .annotation build Lbutterknife/BindView;
        value = 0x7f1101b1
    .end annotation
.end field

.field cameraView:Lcom/google/android/cameraview/CameraView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f110231
    .end annotation
.end field

.field documentPositionTextView:Landroid/widget/TextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f110234
    .end annotation
.end field

.field private e:Z

.field frameOverlayView:Lco/uk/getmondo/signup/identity_verification/id_picture/FrameOverlayView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f110233
    .end annotation
.end field

.field instructionsView:Landroid/widget/TextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f110230
    .end annotation
.end field

.field previewImage:Landroid/widget/ImageView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f110232
    .end annotation
.end field

.field takePictureButton:Landroid/widget/ImageButton;
    .annotation build Lbutterknife/BindView;
        value = 0x7f110235
    .end annotation
.end field

.field thumbnailImageView:Landroid/widget/ImageView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f110237
    .end annotation
.end field

.field usePictureButton:Landroid/widget/Button;
    .annotation build Lbutterknife/BindView;
        value = 0x7f110236
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 50
    invoke-direct {p0}, Lco/uk/getmondo/common/activities/b;-><init>()V

    .line 72
    invoke-static {}, Lcom/b/b/c;->a()Lcom/b/b/c;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/signup/identity_verification/id_picture/DocumentCameraActivity;->c:Lcom/b/b/c;

    .line 73
    const/4 v0, 0x0

    iput-boolean v0, p0, Lco/uk/getmondo/signup/identity_verification/id_picture/DocumentCameraActivity;->e:Z

    return-void
.end method

.method public static a(Landroid/content/Context;Lco/uk/getmondo/signup/identity_verification/a/j;Lco/uk/getmondo/api/model/identity_verification/IdentityDocumentType;Lco/uk/getmondo/d/i;Lco/uk/getmondo/api/model/signup/SignupSource;)Landroid/content/Intent;
    .locals 3

    .prologue
    .line 77
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lco/uk/getmondo/signup/identity_verification/id_picture/DocumentCameraActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "KEY_VERSION"

    .line 78
    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "KEY_ID_DOCUMENT_TYPE"

    .line 79
    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "KEY_COUNTRY_CODE"

    .line 80
    invoke-virtual {p3}, Lco/uk/getmondo/d/i;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "KEY_SIGNUP_SOURCE"

    .line 81
    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    move-result-object v0

    .line 77
    return-object v0
.end method

.method public static a(Landroid/content/Context;Lco/uk/getmondo/signup/identity_verification/a/j;Lco/uk/getmondo/api/model/identity_verification/IdentityDocumentType;Lco/uk/getmondo/d/i;Lco/uk/getmondo/api/model/signup/SignupSource;Ljava/lang/String;)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 86
    invoke-static {p0, p1, p2, p3, p4}, Lco/uk/getmondo/signup/identity_verification/id_picture/DocumentCameraActivity;->a(Landroid/content/Context;Lco/uk/getmondo/signup/identity_verification/a/j;Lco/uk/getmondo/api/model/identity_verification/IdentityDocumentType;Lco/uk/getmondo/d/i;Lco/uk/getmondo/api/model/signup/SignupSource;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "KEY_FRONT_PHOTO_PATH"

    .line 87
    invoke-virtual {v0, v1, p5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 86
    return-object v0
.end method

.method static synthetic a(Lco/uk/getmondo/signup/identity_verification/id_picture/DocumentCameraActivity;ZLandroid/graphics/Bitmap;)Landroid/net/Uri;
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 222
    if-eqz p1, :cond_1

    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/id_picture/DocumentCameraActivity;->b:Lco/uk/getmondo/signup/identity_verification/a/a;

    invoke-virtual {v0}, Lco/uk/getmondo/signup/identity_verification/a/a;->b()Ljava/io/File;

    move-result-object v0

    .line 223
    :goto_0
    new-instance v2, Ljava/io/FileOutputStream;

    invoke-direct {v2, v0}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    const/4 v1, 0x0

    .line 224
    :try_start_0
    sget-object v3, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v4, 0x5a

    invoke-virtual {p2, v3, v4, v2}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 225
    if-eqz v2, :cond_0

    if-eqz v1, :cond_2

    :try_start_1
    invoke-virtual {v2}, Ljava/io/OutputStream;->close()V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_1

    .line 226
    :cond_0
    :goto_1
    invoke-static {v0}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v0

    return-object v0

    .line 222
    :cond_1
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/id_picture/DocumentCameraActivity;->b:Lco/uk/getmondo/signup/identity_verification/a/a;

    invoke-virtual {v0}, Lco/uk/getmondo/signup/identity_verification/a/a;->c()Ljava/io/File;

    move-result-object v0

    goto :goto_0

    .line 225
    :cond_2
    invoke-virtual {v2}, Ljava/io/OutputStream;->close()V

    goto :goto_1

    .line 223
    :catch_0
    move-exception v0

    :try_start_2
    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 225
    :catchall_0
    move-exception v1

    move-object v5, v1

    move-object v1, v0

    move-object v0, v5

    :goto_2
    if-eqz v2, :cond_3

    if-eqz v1, :cond_4

    :try_start_3
    invoke-virtual {v2}, Ljava/io/OutputStream;->close()V
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_2

    :cond_3
    :goto_3
    throw v0

    :cond_4
    invoke-virtual {v2}, Ljava/io/OutputStream;->close()V

    goto :goto_3

    :catch_1
    move-exception v1

    goto :goto_1

    :catch_2
    move-exception v1

    goto :goto_3

    :catchall_1
    move-exception v0

    goto :goto_2
.end method

.method static synthetic a(Lco/uk/getmondo/signup/identity_verification/id_picture/DocumentCameraActivity;Lcom/google/android/cameraview/CameraView$a;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 212
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/id_picture/DocumentCameraActivity;->cameraView:Lcom/google/android/cameraview/CameraView;

    invoke-virtual {v0, p1}, Lcom/google/android/cameraview/CameraView;->b(Lcom/google/android/cameraview/CameraView$a;)V

    return-void
.end method

.method static synthetic a(Lco/uk/getmondo/signup/identity_verification/id_picture/DocumentCameraActivity;Lio/reactivex/o;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 205
    new-instance v0, Lco/uk/getmondo/signup/identity_verification/id_picture/DocumentCameraActivity$1;

    invoke-direct {v0, p0, p1}, Lco/uk/getmondo/signup/identity_verification/id_picture/DocumentCameraActivity$1;-><init>(Lco/uk/getmondo/signup/identity_verification/id_picture/DocumentCameraActivity;Lio/reactivex/o;)V

    .line 211
    iget-object v1, p0, Lco/uk/getmondo/signup/identity_verification/id_picture/DocumentCameraActivity;->cameraView:Lcom/google/android/cameraview/CameraView;

    invoke-virtual {v1, v0}, Lcom/google/android/cameraview/CameraView;->a(Lcom/google/android/cameraview/CameraView$a;)V

    .line 212
    invoke-static {p0, v0}, Lco/uk/getmondo/signup/identity_verification/id_picture/c;->a(Lco/uk/getmondo/signup/identity_verification/id_picture/DocumentCameraActivity;Lcom/google/android/cameraview/CameraView$a;)Lio/reactivex/c/f;

    move-result-object v0

    invoke-interface {p1, v0}, Lio/reactivex/o;->a(Lio/reactivex/c/f;)V

    .line 213
    return-void
.end method

.method private v()Z
    .locals 1

    .prologue
    .line 184
    const-string v0, "android.permission.CAMERA"

    invoke-static {p0, v0}, Landroid/support/v4/content/a;->b(Landroid/content/Context;Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a()Lio/reactivex/n;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .prologue
    .line 189
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/id_picture/DocumentCameraActivity;->c:Lcom/b/b/c;

    return-object v0
.end method

.method public a(Landroid/graphics/Bitmap;Z)Lio/reactivex/n;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/graphics/Bitmap;",
            "Z)",
            "Lio/reactivex/n",
            "<",
            "Landroid/net/Uri;",
            ">;"
        }
    .end annotation

    .prologue
    .line 221
    invoke-static {p0, p2, p1}, Lco/uk/getmondo/signup/identity_verification/id_picture/b;->a(Lco/uk/getmondo/signup/identity_verification/id_picture/DocumentCameraActivity;ZLandroid/graphics/Bitmap;)Ljava/util/concurrent/Callable;

    move-result-object v0

    invoke-static {v0}, Lio/reactivex/n;->fromCallable(Ljava/util/concurrent/Callable;)Lio/reactivex/n;

    move-result-object v0

    return-object v0
.end method

.method public a(Landroid/graphics/Bitmap;)V
    .locals 2

    .prologue
    .line 273
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/id_picture/DocumentCameraActivity;->thumbnailImageView:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 274
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/id_picture/DocumentCameraActivity;->previewImage:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 275
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/id_picture/DocumentCameraActivity;->previewImage:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 276
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/id_picture/DocumentCameraActivity;->frameOverlayView:Lco/uk/getmondo/signup/identity_verification/id_picture/FrameOverlayView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lco/uk/getmondo/signup/identity_verification/id_picture/FrameOverlayView;->setOpaqueFrame(Z)V

    .line 277
    return-void
.end method

.method public a(Lco/uk/getmondo/signup/identity_verification/a/j;Lco/uk/getmondo/api/model/identity_verification/IdentityDocumentType;Lco/uk/getmondo/d/i;Lco/uk/getmondo/api/model/signup/SignupSource;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 312
    invoke-static/range {p0 .. p5}, Lco/uk/getmondo/signup/identity_verification/id_picture/DocumentCameraActivity;->a(Landroid/content/Context;Lco/uk/getmondo/signup/identity_verification/a/j;Lco/uk/getmondo/api/model/identity_verification/IdentityDocumentType;Lco/uk/getmondo/d/i;Lco/uk/getmondo/api/model/signup/SignupSource;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/identity_verification/id_picture/DocumentCameraActivity;->startActivity(Landroid/content/Intent;)V

    .line 313
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 232
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/id_picture/DocumentCameraActivity;->instructionsView:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 233
    return-void
.end method

.method public a(Z)V
    .locals 2

    .prologue
    .line 261
    iget-object v1, p0, Lco/uk/getmondo/signup/identity_verification/id_picture/DocumentCameraActivity;->documentPositionTextView:Landroid/widget/TextView;

    if-eqz p1, :cond_0

    const v0, 0x7f0a01d6

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(I)V

    .line 262
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/id_picture/DocumentCameraActivity;->documentPositionTextView:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 263
    return-void

    .line 261
    :cond_0
    const v0, 0x7f0a01d5

    goto :goto_0
.end method

.method public b()Lio/reactivex/n;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 194
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/id_picture/DocumentCameraActivity;->takePictureButton:Landroid/widget/ImageButton;

    invoke-static {v0}, Lcom/b/a/c/c;->a(Landroid/view/View;)Lio/reactivex/n;

    move-result-object v0

    return-object v0
.end method

.method public b(Z)V
    .locals 1

    .prologue
    .line 281
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/id_picture/DocumentCameraActivity;->takePictureButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, p1}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 282
    return-void
.end method

.method public c()Lio/reactivex/n;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 199
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/id_picture/DocumentCameraActivity;->usePictureButton:Landroid/widget/Button;

    invoke-static {v0}, Lcom/b/a/c/c;->a(Landroid/view/View;)Lio/reactivex/n;

    move-result-object v0

    return-object v0
.end method

.method public c(Z)V
    .locals 1

    .prologue
    .line 286
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/id_picture/DocumentCameraActivity;->usePictureButton:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 287
    return-void
.end method

.method public d()Lio/reactivex/n;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<[B>;"
        }
    .end annotation

    .prologue
    .line 204
    invoke-static {p0}, Lco/uk/getmondo/signup/identity_verification/id_picture/a;->a(Lco/uk/getmondo/signup/identity_verification/id_picture/DocumentCameraActivity;)Lio/reactivex/p;

    move-result-object v0

    invoke-static {v0}, Lio/reactivex/n;->create(Lio/reactivex/p;)Lio/reactivex/n;

    move-result-object v0

    return-object v0
.end method

.method public d(Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 249
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 250
    invoke-static {p0}, Lcom/bumptech/glide/g;->a(Landroid/support/v4/app/j;)Lcom/bumptech/glide/j;

    move-result-object v1

    .line 251
    invoke-virtual {v1, v0}, Lcom/bumptech/glide/j;->a(Ljava/io/File;)Lcom/bumptech/glide/d;

    move-result-object v1

    new-instance v2, Lcom/bumptech/glide/h/b;

    .line 252
    invoke-virtual {v0}, Ljava/io/File;->lastModified()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lcom/bumptech/glide/h/b;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Lcom/bumptech/glide/d;->a(Lcom/bumptech/glide/load/c;)Lcom/bumptech/glide/c;

    move-result-object v0

    const v1, 0x3e4ccccd    # 0.2f

    .line 253
    invoke-virtual {v0, v1}, Lcom/bumptech/glide/c;->a(F)Lcom/bumptech/glide/c;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/signup/identity_verification/id_picture/DocumentCameraActivity;->thumbnailImageView:Landroid/widget/ImageView;

    .line 254
    invoke-virtual {v0, v1}, Lcom/bumptech/glide/c;->a(Landroid/widget/ImageView;)Lcom/bumptech/glide/g/b/j;

    .line 256
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/id_picture/DocumentCameraActivity;->thumbnailImageView:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 257
    return-void
.end method

.method public e()V
    .locals 2

    .prologue
    .line 237
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/id_picture/DocumentCameraActivity;->usePictureButton:Landroid/widget/Button;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 238
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/id_picture/DocumentCameraActivity;->takePictureButton:Landroid/widget/ImageButton;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 239
    return-void
.end method

.method public f()V
    .locals 2

    .prologue
    .line 243
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/id_picture/DocumentCameraActivity;->usePictureButton:Landroid/widget/Button;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 244
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/id_picture/DocumentCameraActivity;->takePictureButton:Landroid/widget/ImageButton;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 245
    return-void
.end method

.method public g()V
    .locals 2

    .prologue
    .line 267
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/id_picture/DocumentCameraActivity;->previewImage:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 268
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/id_picture/DocumentCameraActivity;->frameOverlayView:Lco/uk/getmondo/signup/identity_verification/id_picture/FrameOverlayView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lco/uk/getmondo/signup/identity_verification/id_picture/FrameOverlayView;->setOpaqueFrame(Z)V

    .line 269
    return-void
.end method

.method public h()V
    .locals 2

    .prologue
    .line 291
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/id_picture/DocumentCameraActivity;->cameraProgress:Landroid/widget/ProgressBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 292
    return-void
.end method

.method public i()V
    .locals 2

    .prologue
    .line 296
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/id_picture/DocumentCameraActivity;->cameraProgress:Landroid/widget/ProgressBar;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 297
    return-void
.end method

.method public j()V
    .locals 1

    .prologue
    .line 301
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/id_picture/DocumentCameraActivity;->cameraView:Lcom/google/android/cameraview/CameraView;

    invoke-virtual {v0}, Lcom/google/android/cameraview/CameraView;->g()V

    .line 302
    return-void
.end method

.method public k()V
    .locals 0

    .prologue
    .line 306
    invoke-super {p0}, Lco/uk/getmondo/common/activities/b;->onBackPressed()V

    .line 307
    return-void
.end method

.method public onBackPressed()V
    .locals 2

    .prologue
    .line 168
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/id_picture/DocumentCameraActivity;->previewImage:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    .line 169
    :goto_0
    iget-object v1, p0, Lco/uk/getmondo/signup/identity_verification/id_picture/DocumentCameraActivity;->c:Lcom/b/b/c;

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/b/b/c;->a(Ljava/lang/Object;)V

    .line 170
    return-void

    .line 168
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 8

    .prologue
    const/4 v7, 0x1

    .line 92
    invoke-super {p0, p1}, Lco/uk/getmondo/common/activities/b;->onCreate(Landroid/os/Bundle;)V

    .line 93
    const v0, 0x7f050066

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/identity_verification/id_picture/DocumentCameraActivity;->setContentView(I)V

    .line 94
    invoke-static {p0}, Lbutterknife/ButterKnife;->bind(Landroid/app/Activity;)Lbutterknife/Unbinder;

    .line 96
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/id_picture/DocumentCameraActivity;->thumbnailImageView:Landroid/widget/ImageView;

    const v1, 0x7f020201

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 97
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/id_picture/DocumentCameraActivity;->thumbnailImageView:Landroid/widget/ImageView;

    invoke-virtual {v0, v7}, Landroid/widget/ImageView;->setClipToOutline(Z)V

    .line 99
    invoke-virtual {p0}, Lco/uk/getmondo/signup/identity_verification/id_picture/DocumentCameraActivity;->getSupportActionBar()Landroid/support/v7/app/a;

    move-result-object v0

    .line 100
    if-eqz v0, :cond_0

    .line 101
    invoke-virtual {v0, v7}, Landroid/support/v7/app/a;->b(Z)V

    .line 104
    :cond_0
    invoke-virtual {p0}, Lco/uk/getmondo/signup/identity_verification/id_picture/DocumentCameraActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "KEY_VERSION"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/signup/identity_verification/a/j;

    .line 105
    if-nez v0, :cond_1

    .line 106
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Identity verification flow version required"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 108
    :cond_1
    invoke-virtual {p0}, Lco/uk/getmondo/signup/identity_verification/id_picture/DocumentCameraActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "KEY_COUNTRY_CODE"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lco/uk/getmondo/d/i;->a(Ljava/lang/String;)Lco/uk/getmondo/d/i;

    move-result-object v1

    .line 109
    if-nez v1, :cond_4

    .line 110
    sget-object v1, Lco/uk/getmondo/d/i;->UNITED_KINGDOM:Lco/uk/getmondo/d/i;

    move-object v2, v1

    .line 113
    :goto_0
    sget-object v1, Lco/uk/getmondo/api/model/identity_verification/IdentityDocumentType;->PASSPORT:Lco/uk/getmondo/api/model/identity_verification/IdentityDocumentType;

    .line 114
    invoke-virtual {p0}, Lco/uk/getmondo/signup/identity_verification/id_picture/DocumentCameraActivity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    const-string v4, "KEY_ID_DOCUMENT_TYPE"

    invoke-virtual {v3, v4}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 115
    invoke-virtual {p0}, Lco/uk/getmondo/signup/identity_verification/id_picture/DocumentCameraActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v3, "KEY_ID_DOCUMENT_TYPE"

    invoke-virtual {v1, v3}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v1

    check-cast v1, Lco/uk/getmondo/api/model/identity_verification/IdentityDocumentType;

    move-object v3, v1

    .line 118
    :goto_1
    invoke-virtual {p0}, Lco/uk/getmondo/signup/identity_verification/id_picture/DocumentCameraActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v4, "KEY_SIGNUP_SOURCE"

    invoke-virtual {v1, v4}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v1

    check-cast v1, Lco/uk/getmondo/api/model/signup/SignupSource;

    .line 119
    invoke-virtual {p0}, Lco/uk/getmondo/signup/identity_verification/id_picture/DocumentCameraActivity;->getIntent()Landroid/content/Intent;

    move-result-object v4

    const-string v5, "KEY_FRONT_PHOTO_PATH"

    invoke-virtual {v4, v5}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 120
    invoke-virtual {p0}, Lco/uk/getmondo/signup/identity_verification/id_picture/DocumentCameraActivity;->l()Lco/uk/getmondo/common/h/b/b;

    move-result-object v5

    new-instance v6, Lco/uk/getmondo/signup/identity_verification/p;

    invoke-direct {v6, v0, v1}, Lco/uk/getmondo/signup/identity_verification/p;-><init>(Lco/uk/getmondo/signup/identity_verification/a/j;Lco/uk/getmondo/api/model/signup/SignupSource;)V

    .line 121
    invoke-interface {v5, v6}, Lco/uk/getmondo/common/h/b/b;->a(Lco/uk/getmondo/signup/identity_verification/p;)Lco/uk/getmondo/signup/identity_verification/o;

    move-result-object v0

    new-instance v1, Lco/uk/getmondo/signup/identity_verification/id_picture/f;

    invoke-direct {v1, v3, v2, v4}, Lco/uk/getmondo/signup/identity_verification/id_picture/f;-><init>(Lco/uk/getmondo/api/model/identity_verification/IdentityDocumentType;Lco/uk/getmondo/d/i;Ljava/lang/String;)V

    .line 122
    invoke-interface {v0, v1}, Lco/uk/getmondo/signup/identity_verification/o;->a(Lco/uk/getmondo/signup/identity_verification/id_picture/f;)Lco/uk/getmondo/signup/identity_verification/id_picture/e;

    move-result-object v0

    .line 123
    invoke-interface {v0, p0}, Lco/uk/getmondo/signup/identity_verification/id_picture/e;->a(Lco/uk/getmondo/signup/identity_verification/id_picture/DocumentCameraActivity;)V

    .line 125
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/id_picture/DocumentCameraActivity;->a:Lco/uk/getmondo/signup/identity_verification/id_picture/j;

    invoke-virtual {v0, p0}, Lco/uk/getmondo/signup/identity_verification/id_picture/j;->a(Lco/uk/getmondo/signup/identity_verification/id_picture/j$a;)V

    .line 126
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/id_picture/DocumentCameraActivity;->frameOverlayView:Lco/uk/getmondo/signup/identity_verification/id_picture/FrameOverlayView;

    invoke-virtual {v0, v3}, Lco/uk/getmondo/signup/identity_verification/id_picture/FrameOverlayView;->setFrameIdType(Lco/uk/getmondo/api/model/identity_verification/IdentityDocumentType;)V

    .line 128
    invoke-direct {p0}, Lco/uk/getmondo/signup/identity_verification/id_picture/DocumentCameraActivity;->v()Z

    move-result v0

    if-nez v0, :cond_2

    .line 129
    new-array v0, v7, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "android.permission.CAMERA"

    aput-object v2, v0, v1

    invoke-static {p0, v0, v7}, Landroid/support/v4/app/a;->a(Landroid/app/Activity;[Ljava/lang/String;I)V

    .line 131
    :cond_2
    return-void

    :cond_3
    move-object v3, v1

    goto :goto_1

    :cond_4
    move-object v2, v1

    goto :goto_0
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 161
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/id_picture/DocumentCameraActivity;->a:Lco/uk/getmondo/signup/identity_verification/id_picture/j;

    invoke-virtual {v0}, Lco/uk/getmondo/signup/identity_verification/id_picture/j;->b()V

    .line 162
    invoke-super {p0}, Lco/uk/getmondo/common/activities/b;->onDestroy()V

    .line 163
    return-void
.end method

.method public onPause()V
    .locals 1

    .prologue
    .line 155
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/id_picture/DocumentCameraActivity;->cameraView:Lcom/google/android/cameraview/CameraView;

    invoke-virtual {v0}, Lcom/google/android/cameraview/CameraView;->c()V

    .line 156
    invoke-super {p0}, Lco/uk/getmondo/common/activities/b;->onPause()V

    .line 157
    return-void
.end method

.method public onRequestPermissionsResult(I[Ljava/lang/String;[I)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 174
    if-ne p1, v1, :cond_2

    .line 175
    array-length v0, p3

    if-ne v0, v1, :cond_0

    const/4 v0, 0x0

    aget v0, p3, v0

    if-eqz v0, :cond_1

    .line 176
    :cond_0
    iput-boolean v1, p0, Lco/uk/getmondo/signup/identity_verification/id_picture/DocumentCameraActivity;->e:Z

    .line 181
    :cond_1
    :goto_0
    return-void

    .line 179
    :cond_2
    invoke-super {p0, p1, p2, p3}, Lco/uk/getmondo/common/activities/b;->onRequestPermissionsResult(I[Ljava/lang/String;[I)V

    goto :goto_0
.end method

.method public onResume()V
    .locals 1

    .prologue
    .line 135
    invoke-super {p0}, Lco/uk/getmondo/common/activities/b;->onResume()V

    .line 136
    invoke-direct {p0}, Lco/uk/getmondo/signup/identity_verification/id_picture/DocumentCameraActivity;->v()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 137
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/id_picture/DocumentCameraActivity;->cameraView:Lcom/google/android/cameraview/CameraView;

    invoke-virtual {v0}, Lcom/google/android/cameraview/CameraView;->a()V

    .line 139
    :cond_0
    return-void
.end method

.method protected onResumeFragments()V
    .locals 3

    .prologue
    .line 143
    invoke-super {p0}, Lco/uk/getmondo/common/activities/b;->onResumeFragments()V

    .line 146
    iget-boolean v0, p0, Lco/uk/getmondo/signup/identity_verification/id_picture/DocumentCameraActivity;->e:Z

    if-eqz v0, :cond_0

    .line 147
    const v0, 0x7f0a01fe

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/identity_verification/id_picture/DocumentCameraActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lco/uk/getmondo/common/d/a;->a(Ljava/lang/String;)Lco/uk/getmondo/common/d/a;

    move-result-object v0

    .line 148
    invoke-virtual {p0}, Lco/uk/getmondo/signup/identity_verification/id_picture/DocumentCameraActivity;->getSupportFragmentManager()Landroid/support/v4/app/n;

    move-result-object v1

    const-string v2, "TAG_ERROR_DIALOG_FRAGMENT"

    invoke-virtual {v0, v1, v2}, Lco/uk/getmondo/common/d/a;->show(Landroid/support/v4/app/n;Ljava/lang/String;)V

    .line 149
    const/4 v0, 0x0

    iput-boolean v0, p0, Lco/uk/getmondo/signup/identity_verification/id_picture/DocumentCameraActivity;->e:Z

    .line 151
    :cond_0
    return-void
.end method
