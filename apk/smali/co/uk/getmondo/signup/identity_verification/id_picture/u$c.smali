.class final Lco/uk/getmondo/signup/identity_verification/id_picture/u$c;
.super Ljava/lang/Object;
.source "DocumentImageResizer.kt"

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lco/uk/getmondo/signup/identity_verification/id_picture/u;->a([B)Lio/reactivex/n;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<TT;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000\u0008\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u0001H\n\u00a2\u0006\u0002\u0008\u0002"
    }
    d2 = {
        "<anonymous>",
        "Landroid/graphics/Bitmap;",
        "call"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field final synthetic a:Lco/uk/getmondo/signup/identity_verification/id_picture/u;

.field final synthetic b:[B


# direct methods
.method constructor <init>(Lco/uk/getmondo/signup/identity_verification/id_picture/u;[B)V
    .locals 0

    iput-object p1, p0, Lco/uk/getmondo/signup/identity_verification/id_picture/u$c;->a:Lco/uk/getmondo/signup/identity_verification/id_picture/u;

    iput-object p2, p0, Lco/uk/getmondo/signup/identity_verification/id_picture/u$c;->b:[B

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Landroid/graphics/Bitmap;
    .locals 7

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 23
    iget-object v1, p0, Lco/uk/getmondo/signup/identity_verification/id_picture/u$c;->a:Lco/uk/getmondo/signup/identity_verification/id_picture/u;

    iget-object v2, p0, Lco/uk/getmondo/signup/identity_verification/id_picture/u$c;->a:Lco/uk/getmondo/signup/identity_verification/id_picture/u;

    iget-object v5, p0, Lco/uk/getmondo/signup/identity_verification/id_picture/u$c;->b:[B

    invoke-static {v2, v5}, Lco/uk/getmondo/signup/identity_verification/id_picture/u;->a(Lco/uk/getmondo/signup/identity_verification/id_picture/u;[B)Landroid/util/SizeF;

    move-result-object v2

    invoke-static {v1, v2}, Lco/uk/getmondo/signup/identity_verification/id_picture/u;->a(Lco/uk/getmondo/signup/identity_verification/id_picture/u;Landroid/util/SizeF;)Landroid/graphics/BitmapFactory$Options;

    move-result-object v1

    .line 24
    iget-object v2, p0, Lco/uk/getmondo/signup/identity_verification/id_picture/u$c;->b:[B

    iget-object v5, p0, Lco/uk/getmondo/signup/identity_verification/id_picture/u$c;->b:[B

    array-length v5, v5

    invoke-static {v2, v3, v5, v1}, Landroid/graphics/BitmapFactory;->decodeByteArray([BIILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v5

    .line 26
    const/4 v1, 0x0

    check-cast v1, Landroid/graphics/Matrix;

    .line 27
    new-instance v1, Ljava/io/ByteArrayInputStream;

    iget-object v2, p0, Lco/uk/getmondo/signup/identity_verification/id_picture/u$c;->b:[B

    invoke-direct {v1, v2}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    check-cast v1, Ljava/io/Closeable;

    nop

    :try_start_0
    move-object v0, v1

    check-cast v0, Ljava/io/ByteArrayInputStream;

    move-object v2, v0

    .line 28
    new-instance v6, Landroid/support/d/a;

    check-cast v2, Ljava/io/InputStream;

    invoke-direct {v6, v2}, Landroid/support/d/a;-><init>(Ljava/io/InputStream;)V

    .line 29
    iget-object v2, p0, Lco/uk/getmondo/signup/identity_verification/id_picture/u$c;->a:Lco/uk/getmondo/signup/identity_verification/id_picture/u;

    invoke-static {v2, v6}, Lco/uk/getmondo/signup/identity_verification/id_picture/u;->a(Lco/uk/getmondo/signup/identity_verification/id_picture/u;Landroid/support/d/a;)Landroid/graphics/Matrix;

    move-result-object v2

    .line 30
    nop

    sget-object v3, Lkotlin/n;->a:Lkotlin/n;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 27
    invoke-interface {v1}, Ljava/io/Closeable;->close()V

    .line 33
    iget-object v1, p0, Lco/uk/getmondo/signup/identity_verification/id_picture/u$c;->a:Lco/uk/getmondo/signup/identity_verification/id_picture/u;

    const-string v3, "bitmap"

    invoke-static {v5, v3}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v1, v5, v4, v2}, Lco/uk/getmondo/signup/identity_verification/id_picture/u;->a(Lco/uk/getmondo/signup/identity_verification/id_picture/u;Landroid/graphics/Bitmap;ZLandroid/graphics/Matrix;)Landroid/graphics/Bitmap;

    move-result-object v1

    return-object v1

    .line 27
    :catch_0
    move-exception v2

    nop

    :try_start_1
    invoke-interface {v1}, Ljava/io/Closeable;->close()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :goto_0
    :try_start_2
    check-cast v2, Ljava/lang/Throwable;

    throw v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :catchall_0
    move-exception v2

    move v3, v4

    :goto_1
    if-nez v3, :cond_0

    invoke-interface {v1}, Ljava/io/Closeable;->close()V

    :cond_0
    throw v2

    :catch_1
    move-exception v3

    goto :goto_0

    :catchall_1
    move-exception v2

    goto :goto_1
.end method

.method public synthetic call()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 13
    invoke-virtual {p0}, Lco/uk/getmondo/signup/identity_verification/id_picture/u$c;->a()Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method
