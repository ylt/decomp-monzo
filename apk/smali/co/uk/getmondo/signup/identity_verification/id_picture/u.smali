.class public final Lco/uk/getmondo/signup/identity_verification/id_picture/u;
.super Ljava/lang/Object;
.source "DocumentImageResizer.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lco/uk/getmondo/signup/identity_verification/id_picture/u$a;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000J\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u0003\n\u0002\u0010\u0012\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0018\u0000 \u001a2\u00020\u0001:\u0001\u001aB\u0007\u0008\u0007\u00a2\u0006\u0002\u0010\u0002J\u0012\u0010\u0003\u001a\u0004\u0018\u00010\u00042\u0006\u0010\u0005\u001a\u00020\u0006H\u0002J\u0010\u0010\u0007\u001a\u00020\u00082\u0006\u0010\t\u001a\u00020\nH\u0002J\"\u0010\u000b\u001a\u00020\u000c2\u0006\u0010\r\u001a\u00020\u000c2\u0006\u0010\u000e\u001a\u00020\u000f2\u0008\u0010\u0010\u001a\u0004\u0018\u00010\u0004H\u0002J\u0010\u0010\u0011\u001a\u00020\n2\u0006\u0010\u0012\u001a\u00020\u0013H\u0002J\u0010\u0010\u0014\u001a\u00020\n2\u0006\u0010\u0015\u001a\u00020\u0016H\u0002J\u0014\u0010\u0017\u001a\u0008\u0012\u0004\u0012\u00020\u000c0\u00182\u0006\u0010\u0015\u001a\u00020\u0016J\u0014\u0010\u0019\u001a\u0008\u0012\u0004\u0012\u00020\u000c0\u00182\u0006\u0010\u0012\u001a\u00020\u0013\u00a8\u0006\u001b"
    }
    d2 = {
        "Lco/uk/getmondo/signup/identity_verification/id_picture/DocumentImageResizer;",
        "",
        "()V",
        "calculateRotationMatrix",
        "Landroid/graphics/Matrix;",
        "exifInterface",
        "Landroid/support/media/ExifInterface;",
        "calculateScalingOptions",
        "Landroid/graphics/BitmapFactory$Options;",
        "size",
        "Landroid/util/SizeF;",
        "cropAndRotate",
        "Landroid/graphics/Bitmap;",
        "bitmap",
        "shouldCrop",
        "",
        "rotationMatrix",
        "extractPictureSizeFromByteArray",
        "pictureData",
        "",
        "extractPictureSizeFromFile",
        "file",
        "Ljava/io/File;",
        "prepareFallbackImage",
        "Lio/reactivex/Observable;",
        "prepareImage",
        "Companion",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# static fields
.field public static final a:Lco/uk/getmondo/signup/identity_verification/id_picture/u$a;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lco/uk/getmondo/signup/identity_verification/id_picture/u$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lco/uk/getmondo/signup/identity_verification/id_picture/u$a;-><init>(Lkotlin/d/b/i;)V

    sput-object v0, Lco/uk/getmondo/signup/identity_verification/id_picture/u;->a:Lco/uk/getmondo/signup/identity_verification/id_picture/u$a;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private final a(Landroid/graphics/Bitmap;ZLandroid/graphics/Matrix;)Landroid/graphics/Bitmap;
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 96
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    .line 97
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    .line 98
    if-ne v4, v3, :cond_0

    if-eqz p3, :cond_2

    .line 99
    :cond_0
    if-eqz p2, :cond_1

    .line 100
    invoke-static {v3, v4}, Ljava/lang/Math;->min(II)I

    move-result v4

    move v3, v4

    .line 103
    :cond_1
    if-nez p3, :cond_3

    .line 104
    invoke-static {p1, v1, v1, v3, v4}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIII)Landroid/graphics/Bitmap;

    move-result-object p1

    const-string v0, "Bitmap.createBitmap(bitmap, 0, 0, width, height)"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 107
    :cond_2
    :goto_0
    return-object p1

    .line 106
    :cond_3
    const/4 v6, 0x1

    move-object v0, p1

    move v2, v1

    move-object v5, p3

    invoke-static/range {v0 .. v6}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;

    move-result-object p1

    const-string v0, "Bitmap.createBitmap(bitm\u2026ht, rotationMatrix, true)"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static final synthetic a(Lco/uk/getmondo/signup/identity_verification/id_picture/u;Landroid/graphics/Bitmap;ZLandroid/graphics/Matrix;)Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 13
    invoke-direct {p0, p1, p2, p3}, Lco/uk/getmondo/signup/identity_verification/id_picture/u;->a(Landroid/graphics/Bitmap;ZLandroid/graphics/Matrix;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method private final a(Landroid/util/SizeF;)Landroid/graphics/BitmapFactory$Options;
    .locals 9

    .prologue
    const/4 v8, 0x2

    .line 47
    const-wide v0, 0x415ab3f000000000L    # 7000000.0

    invoke-static {v0, v1}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v0

    const-wide/high16 v2, 0x4000000000000000L    # 2.0

    mul-double/2addr v0, v2

    invoke-virtual {p1}, Landroid/util/SizeF;->getWidth()F

    move-result v2

    float-to-double v2, v2

    mul-double/2addr v0, v2

    invoke-virtual {p1}, Landroid/util/SizeF;->getWidth()F

    move-result v2

    invoke-virtual {p1}, Landroid/util/SizeF;->getHeight()F

    move-result v3

    add-float/2addr v2, v3

    float-to-double v2, v2

    div-double/2addr v0, v2

    .line 48
    new-instance v2, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v2}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 49
    invoke-virtual {p1}, Landroid/util/SizeF;->getWidth()F

    move-result v3

    float-to-double v4, v3

    cmpl-double v3, v4, v0

    if-lez v3, :cond_0

    .line 50
    const/4 v3, 0x1

    iput-boolean v3, v2, Landroid/graphics/BitmapFactory$Options;->inScaled:Z

    .line 51
    invoke-virtual {p1}, Landroid/util/SizeF;->getWidth()F

    move-result v3

    float-to-int v3, v3

    iput v3, v2, Landroid/graphics/BitmapFactory$Options;->inDensity:I

    .line 52
    invoke-virtual {p1}, Landroid/util/SizeF;->getWidth()F

    move-result v3

    float-to-double v4, v3

    div-double/2addr v4, v0

    invoke-static {v4, v5}, Ljava/lang/Math;->floor(D)D

    move-result-wide v4

    int-to-double v6, v8

    cmpl-double v3, v4, v6

    if-ltz v3, :cond_1

    .line 54
    iput v8, v2, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    .line 55
    double-to-int v0, v0

    iget v1, v2, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    mul-int/2addr v0, v1

    iput v0, v2, Landroid/graphics/BitmapFactory$Options;->inTargetDensity:I

    .line 58
    :cond_0
    :goto_0
    return-object v2

    .line 57
    :cond_1
    double-to-int v0, v0

    iput v0, v2, Landroid/graphics/BitmapFactory$Options;->inTargetDensity:I

    goto :goto_0
.end method

.method public static final synthetic a(Lco/uk/getmondo/signup/identity_verification/id_picture/u;Landroid/util/SizeF;)Landroid/graphics/BitmapFactory$Options;
    .locals 1

    .prologue
    .line 13
    invoke-direct {p0, p1}, Lco/uk/getmondo/signup/identity_verification/id_picture/u;->a(Landroid/util/SizeF;)Landroid/graphics/BitmapFactory$Options;

    move-result-object v0

    return-object v0
.end method

.method private final a(Landroid/support/d/a;)Landroid/graphics/Matrix;
    .locals 2

    .prologue
    .line 79
    const-string v0, "Orientation"

    const/4 v1, 0x1

    invoke-virtual {p1, v0, v1}, Landroid/support/d/a;->a(Ljava/lang/String;I)I

    move-result v0

    .line 80
    packed-switch v0, :pswitch_data_0

    .line 84
    :pswitch_0
    const/4 v0, 0x0

    move v1, v0

    .line 87
    :goto_0
    if-lez v1, :cond_0

    .line 88
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    .line 89
    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->postRotate(F)Z

    .line 92
    :goto_1
    return-object v0

    .line 81
    :pswitch_1
    const/16 v0, 0x5a

    move v1, v0

    goto :goto_0

    .line 82
    :pswitch_2
    const/16 v0, 0xb4

    move v1, v0

    goto :goto_0

    .line 83
    :pswitch_3
    const/16 v0, 0x10e

    move v1, v0

    goto :goto_0

    .line 92
    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    .line 80
    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method public static final synthetic a(Lco/uk/getmondo/signup/identity_verification/id_picture/u;Landroid/support/d/a;)Landroid/graphics/Matrix;
    .locals 1

    .prologue
    .line 13
    invoke-direct {p0, p1}, Lco/uk/getmondo/signup/identity_verification/id_picture/u;->a(Landroid/support/d/a;)Landroid/graphics/Matrix;

    move-result-object v0

    return-object v0
.end method

.method public static final synthetic a(Lco/uk/getmondo/signup/identity_verification/id_picture/u;Ljava/io/File;)Landroid/util/SizeF;
    .locals 1

    .prologue
    .line 13
    invoke-direct {p0, p1}, Lco/uk/getmondo/signup/identity_verification/id_picture/u;->b(Ljava/io/File;)Landroid/util/SizeF;

    move-result-object v0

    return-object v0
.end method

.method public static final synthetic a(Lco/uk/getmondo/signup/identity_verification/id_picture/u;[B)Landroid/util/SizeF;
    .locals 1

    .prologue
    .line 13
    invoke-direct {p0, p1}, Lco/uk/getmondo/signup/identity_verification/id_picture/u;->b([B)Landroid/util/SizeF;

    move-result-object v0

    return-object v0
.end method

.method private final b(Ljava/io/File;)Landroid/util/SizeF;
    .locals 3

    .prologue
    .line 64
    new-instance v0, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v0}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 65
    const/4 v1, 0x1

    iput-boolean v1, v0, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 66
    invoke-virtual {p1}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v0}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    .line 67
    new-instance v1, Landroid/util/SizeF;

    iget v2, v0, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    int-to-float v2, v2

    iget v0, v0, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    int-to-float v0, v0

    invoke-direct {v1, v2, v0}, Landroid/util/SizeF;-><init>(FF)V

    return-object v1
.end method

.method private final b([B)Landroid/util/SizeF;
    .locals 3

    .prologue
    .line 72
    new-instance v0, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v0}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 73
    const/4 v1, 0x1

    iput-boolean v1, v0, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 74
    const/4 v1, 0x0

    array-length v2, p1

    invoke-static {p1, v1, v2, v0}, Landroid/graphics/BitmapFactory;->decodeByteArray([BIILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    .line 75
    new-instance v1, Landroid/util/SizeF;

    iget v2, v0, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    int-to-float v2, v2

    iget v0, v0, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    int-to-float v0, v0

    invoke-direct {v1, v2, v0}, Landroid/util/SizeF;-><init>(FF)V

    return-object v1
.end method


# virtual methods
.method public final a(Ljava/io/File;)Lio/reactivex/n;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/File;",
            ")",
            "Lio/reactivex/n",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation

    .prologue
    const-string v0, "file"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 38
    new-instance v0, Lco/uk/getmondo/signup/identity_verification/id_picture/u$b;

    invoke-direct {v0, p0, p1}, Lco/uk/getmondo/signup/identity_verification/id_picture/u$b;-><init>(Lco/uk/getmondo/signup/identity_verification/id_picture/u;Ljava/io/File;)V

    check-cast v0, Ljava/util/concurrent/Callable;

    invoke-static {v0}, Lio/reactivex/n;->fromCallable(Ljava/util/concurrent/Callable;)Lio/reactivex/n;

    move-result-object v0

    const-string v1, "Observable.fromCallable \u2026 false, matrix)\n        }"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public final a([B)Lio/reactivex/n;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([B)",
            "Lio/reactivex/n",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation

    .prologue
    const-string v0, "pictureData"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 20
    new-instance v0, Lco/uk/getmondo/signup/identity_verification/id_picture/u$c;

    invoke-direct {v0, p0, p1}, Lco/uk/getmondo/signup/identity_verification/id_picture/u$c;-><init>(Lco/uk/getmondo/signup/identity_verification/id_picture/u;[B)V

    check-cast v0, Ljava/util/concurrent/Callable;

    invoke-static {v0}, Lio/reactivex/n;->fromCallable(Ljava/util/concurrent/Callable;)Lio/reactivex/n;

    move-result-object v0

    const-string v1, "Observable.fromCallable \u2026rotationMatrix)\n        }"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method
