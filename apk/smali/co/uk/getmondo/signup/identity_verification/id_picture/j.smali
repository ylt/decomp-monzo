.class public Lco/uk/getmondo/signup/identity_verification/id_picture/j;
.super Lco/uk/getmondo/common/ui/b;
.source "DocumentCameraPresenter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lco/uk/getmondo/signup/identity_verification/id_picture/j$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lco/uk/getmondo/common/ui/b",
        "<",
        "Lco/uk/getmondo/signup/identity_verification/id_picture/j$a;",
        ">;"
    }
.end annotation


# instance fields
.field private c:Landroid/net/Uri;

.field private final d:Lio/reactivex/u;

.field private final e:Lio/reactivex/u;

.field private final f:Lco/uk/getmondo/signup/identity_verification/id_picture/s;

.field private final g:Lio/reactivex/u;

.field private final h:Lco/uk/getmondo/signup/identity_verification/a/e;

.field private final i:Lco/uk/getmondo/common/a;

.field private final j:Lco/uk/getmondo/signup/identity_verification/id_picture/u;

.field private final k:Lco/uk/getmondo/signup/identity_verification/a/j;

.field private final l:Lco/uk/getmondo/api/model/identity_verification/IdentityDocumentType;

.field private final m:Lco/uk/getmondo/api/model/signup/SignupSource;

.field private final n:Ljava/lang/String;

.field private final o:Lco/uk/getmondo/d/i;


# direct methods
.method constructor <init>(Lio/reactivex/u;Lio/reactivex/u;Lio/reactivex/u;Lco/uk/getmondo/signup/identity_verification/a/e;Lco/uk/getmondo/signup/identity_verification/id_picture/s;Lco/uk/getmondo/common/a;Lco/uk/getmondo/signup/identity_verification/id_picture/u;Lco/uk/getmondo/signup/identity_verification/a/j;Lco/uk/getmondo/api/model/identity_verification/IdentityDocumentType;Lco/uk/getmondo/d/i;Lco/uk/getmondo/api/model/signup/SignupSource;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 48
    invoke-direct {p0}, Lco/uk/getmondo/common/ui/b;-><init>()V

    .line 49
    iput-object p1, p0, Lco/uk/getmondo/signup/identity_verification/id_picture/j;->d:Lio/reactivex/u;

    .line 50
    iput-object p2, p0, Lco/uk/getmondo/signup/identity_verification/id_picture/j;->e:Lio/reactivex/u;

    .line 51
    iput-object p3, p0, Lco/uk/getmondo/signup/identity_verification/id_picture/j;->g:Lio/reactivex/u;

    .line 52
    iput-object p4, p0, Lco/uk/getmondo/signup/identity_verification/id_picture/j;->h:Lco/uk/getmondo/signup/identity_verification/a/e;

    .line 53
    iput-object p5, p0, Lco/uk/getmondo/signup/identity_verification/id_picture/j;->f:Lco/uk/getmondo/signup/identity_verification/id_picture/s;

    .line 54
    iput-object p6, p0, Lco/uk/getmondo/signup/identity_verification/id_picture/j;->i:Lco/uk/getmondo/common/a;

    .line 55
    iput-object p7, p0, Lco/uk/getmondo/signup/identity_verification/id_picture/j;->j:Lco/uk/getmondo/signup/identity_verification/id_picture/u;

    .line 56
    iput-object p8, p0, Lco/uk/getmondo/signup/identity_verification/id_picture/j;->k:Lco/uk/getmondo/signup/identity_verification/a/j;

    .line 57
    iput-object p9, p0, Lco/uk/getmondo/signup/identity_verification/id_picture/j;->l:Lco/uk/getmondo/api/model/identity_verification/IdentityDocumentType;

    .line 58
    iput-object p11, p0, Lco/uk/getmondo/signup/identity_verification/id_picture/j;->m:Lco/uk/getmondo/api/model/signup/SignupSource;

    .line 59
    iput-object p12, p0, Lco/uk/getmondo/signup/identity_verification/id_picture/j;->n:Ljava/lang/String;

    .line 60
    iput-object p10, p0, Lco/uk/getmondo/signup/identity_verification/id_picture/j;->o:Lco/uk/getmondo/d/i;

    .line 61
    return-void
.end method

.method static synthetic a(Lco/uk/getmondo/signup/identity_verification/id_picture/j;Lco/uk/getmondo/signup/identity_verification/id_picture/j$a;ZLandroid/graphics/Bitmap;)Lio/reactivex/r;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 97
    invoke-interface {p1, p3, p2}, Lco/uk/getmondo/signup/identity_verification/id_picture/j$a;->a(Landroid/graphics/Bitmap;Z)Lio/reactivex/n;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/signup/identity_verification/id_picture/j;->e:Lio/reactivex/u;

    invoke-virtual {v0, v1}, Lio/reactivex/n;->subscribeOn(Lio/reactivex/u;)Lio/reactivex/n;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lco/uk/getmondo/signup/identity_verification/id_picture/j;[B)Lio/reactivex/r;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 89
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/id_picture/j;->j:Lco/uk/getmondo/signup/identity_verification/id_picture/u;

    invoke-virtual {v0, p1}, Lco/uk/getmondo/signup/identity_verification/id_picture/u;->a([B)Lio/reactivex/n;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/signup/identity_verification/id_picture/j;->g:Lio/reactivex/u;

    invoke-virtual {v0, v1}, Lio/reactivex/n;->subscribeOn(Lio/reactivex/u;)Lio/reactivex/n;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lco/uk/getmondo/signup/identity_verification/id_picture/j$a;Ljava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 82
    invoke-interface {p0, v0}, Lco/uk/getmondo/signup/identity_verification/id_picture/j$a;->b(Z)V

    .line 83
    invoke-interface {p0, v0}, Lco/uk/getmondo/signup/identity_verification/id_picture/j$a;->c(Z)V

    .line 84
    invoke-interface {p0}, Lco/uk/getmondo/signup/identity_verification/id_picture/j$a;->h()V

    .line 85
    invoke-interface {p0}, Lco/uk/getmondo/signup/identity_verification/id_picture/j$a;->j()V

    .line 86
    return-void
.end method

.method static synthetic a(Lco/uk/getmondo/signup/identity_verification/id_picture/j;Lco/uk/getmondo/signup/identity_verification/id_picture/j$a;Landroid/graphics/Bitmap;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 92
    invoke-interface {p1}, Lco/uk/getmondo/signup/identity_verification/id_picture/j$a;->i()V

    .line 93
    invoke-interface {p1, p2}, Lco/uk/getmondo/signup/identity_verification/id_picture/j$a;->a(Landroid/graphics/Bitmap;)V

    .line 94
    invoke-interface {p1}, Lco/uk/getmondo/signup/identity_verification/id_picture/j$a;->e()V

    .line 95
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/id_picture/j;->f:Lco/uk/getmondo/signup/identity_verification/id_picture/s;

    invoke-virtual {v0}, Lco/uk/getmondo/signup/identity_verification/id_picture/s;->a()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Lco/uk/getmondo/signup/identity_verification/id_picture/j$a;->a(Ljava/lang/String;)V

    .line 96
    return-void
.end method

.method static synthetic a(Lco/uk/getmondo/signup/identity_verification/id_picture/j;Lco/uk/getmondo/signup/identity_verification/id_picture/j$a;Landroid/net/Uri;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 100
    iput-object p2, p0, Lco/uk/getmondo/signup/identity_verification/id_picture/j;->c:Landroid/net/Uri;

    .line 101
    const/4 v0, 0x1

    invoke-interface {p1, v0}, Lco/uk/getmondo/signup/identity_verification/id_picture/j$a;->c(Z)V

    .line 102
    return-void
.end method

.method static synthetic a(Lco/uk/getmondo/signup/identity_verification/id_picture/j;Lco/uk/getmondo/signup/identity_verification/id_picture/j$a;Ljava/lang/Object;)V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 106
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/id_picture/j;->k:Lco/uk/getmondo/signup/identity_verification/a/j;

    sget-object v1, Lco/uk/getmondo/signup/identity_verification/a/j;->b:Lco/uk/getmondo/signup/identity_verification/a/j;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/id_picture/j;->l:Lco/uk/getmondo/api/model/identity_verification/IdentityDocumentType;

    invoke-virtual {v0}, Lco/uk/getmondo/api/model/identity_verification/IdentityDocumentType;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/id_picture/j;->n:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 110
    iget-object v1, p0, Lco/uk/getmondo/signup/identity_verification/id_picture/j;->k:Lco/uk/getmondo/signup/identity_verification/a/j;

    iget-object v2, p0, Lco/uk/getmondo/signup/identity_verification/id_picture/j;->l:Lco/uk/getmondo/api/model/identity_verification/IdentityDocumentType;

    iget-object v3, p0, Lco/uk/getmondo/signup/identity_verification/id_picture/j;->o:Lco/uk/getmondo/d/i;

    iget-object v4, p0, Lco/uk/getmondo/signup/identity_verification/id_picture/j;->m:Lco/uk/getmondo/api/model/signup/SignupSource;

    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/id_picture/j;->c:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v5

    move-object v0, p1

    invoke-interface/range {v0 .. v5}, Lco/uk/getmondo/signup/identity_verification/id_picture/j$a;->a(Lco/uk/getmondo/signup/identity_verification/a/j;Lco/uk/getmondo/api/model/identity_verification/IdentityDocumentType;Lco/uk/getmondo/d/i;Lco/uk/getmondo/api/model/signup/SignupSource;Ljava/lang/String;)V

    .line 119
    :goto_0
    invoke-interface {p1}, Lco/uk/getmondo/signup/identity_verification/id_picture/j$a;->finish()V

    .line 120
    return-void

    .line 112
    :cond_0
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/id_picture/j;->n:Ljava/lang/String;

    if-nez v0, :cond_1

    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/id_picture/j;->c:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v0

    .line 113
    :goto_1
    iget-object v1, p0, Lco/uk/getmondo/signup/identity_verification/id_picture/j;->n:Ljava/lang/String;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lco/uk/getmondo/signup/identity_verification/id_picture/j;->c:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v1

    .line 114
    :goto_2
    iget-object v2, p0, Lco/uk/getmondo/signup/identity_verification/id_picture/j;->h:Lco/uk/getmondo/signup/identity_verification/a/e;

    new-instance v3, Lco/uk/getmondo/signup/identity_verification/a/a/b;

    iget-object v4, p0, Lco/uk/getmondo/signup/identity_verification/id_picture/j;->l:Lco/uk/getmondo/api/model/identity_verification/IdentityDocumentType;

    iget-object v5, p0, Lco/uk/getmondo/signup/identity_verification/id_picture/j;->o:Lco/uk/getmondo/d/i;

    invoke-direct {v3, v0, v4, v5, v1}, Lco/uk/getmondo/signup/identity_verification/a/a/b;-><init>(Ljava/lang/String;Lco/uk/getmondo/api/model/identity_verification/IdentityDocumentType;Lco/uk/getmondo/d/i;Ljava/lang/String;)V

    const/4 v0, 0x0

    invoke-interface {v2, v3, v0}, Lco/uk/getmondo/signup/identity_verification/a/e;->a(Lco/uk/getmondo/signup/identity_verification/a/a/b;Z)V

    .line 117
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/id_picture/j;->i:Lco/uk/getmondo/common/a;

    iget-object v1, p0, Lco/uk/getmondo/signup/identity_verification/id_picture/j;->l:Lco/uk/getmondo/api/model/identity_verification/IdentityDocumentType;

    invoke-virtual {v1}, Lco/uk/getmondo/api/model/identity_verification/IdentityDocumentType;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lco/uk/getmondo/api/model/tracking/Impression;->h(Ljava/lang/String;)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v1

    invoke-virtual {v0, v1}, Lco/uk/getmondo/common/a;->a(Lco/uk/getmondo/api/model/tracking/Impression;)V

    goto :goto_0

    .line 112
    :cond_1
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/id_picture/j;->n:Ljava/lang/String;

    goto :goto_1

    .line 113
    :cond_2
    const/4 v1, 0x0

    goto :goto_2
.end method

.method static synthetic a(Lco/uk/getmondo/signup/identity_verification/id_picture/j;Lco/uk/getmondo/signup/identity_verification/id_picture/j$a;ZLjava/lang/Boolean;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 124
    invoke-virtual {p3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 125
    invoke-interface {p1}, Lco/uk/getmondo/signup/identity_verification/id_picture/j$a;->g()V

    .line 126
    invoke-interface {p1}, Lco/uk/getmondo/signup/identity_verification/id_picture/j$a;->f()V

    .line 127
    const/4 v0, 0x1

    invoke-interface {p1, v0}, Lco/uk/getmondo/signup/identity_verification/id_picture/j$a;->b(Z)V

    .line 128
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/id_picture/j;->f:Lco/uk/getmondo/signup/identity_verification/id_picture/s;

    invoke-virtual {v0, p2}, Lco/uk/getmondo/signup/identity_verification/id_picture/s;->a(Z)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Lco/uk/getmondo/signup/identity_verification/id_picture/j$a;->a(Ljava/lang/String;)V

    .line 129
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/id_picture/j;->n:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 130
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/id_picture/j;->n:Ljava/lang/String;

    invoke-interface {p1, v0}, Lco/uk/getmondo/signup/identity_verification/id_picture/j$a;->d(Ljava/lang/String;)V

    .line 135
    :cond_0
    :goto_0
    return-void

    .line 133
    :cond_1
    invoke-interface {p1}, Lco/uk/getmondo/signup/identity_verification/id_picture/j$a;->k()V

    goto :goto_0
.end method


# virtual methods
.method public bridge synthetic a(Lco/uk/getmondo/common/ui/f;)V
    .locals 0

    .prologue
    .line 26
    check-cast p1, Lco/uk/getmondo/signup/identity_verification/id_picture/j$a;

    invoke-virtual {p0, p1}, Lco/uk/getmondo/signup/identity_verification/id_picture/j;->a(Lco/uk/getmondo/signup/identity_verification/id_picture/j$a;)V

    return-void
.end method

.method public a(Lco/uk/getmondo/signup/identity_verification/id_picture/j$a;)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 65
    invoke-super {p0, p1}, Lco/uk/getmondo/common/ui/b;->a(Lco/uk/getmondo/common/ui/f;)V

    .line 67
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/id_picture/j;->i:Lco/uk/getmondo/common/a;

    iget-object v3, p0, Lco/uk/getmondo/signup/identity_verification/id_picture/j;->l:Lco/uk/getmondo/api/model/identity_verification/IdentityDocumentType;

    invoke-virtual {v3}, Lco/uk/getmondo/api/model/identity_verification/IdentityDocumentType;->a()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3, v2}, Lco/uk/getmondo/api/model/tracking/Impression;->a(Ljava/lang/String;Z)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v3

    invoke-virtual {v0, v3}, Lco/uk/getmondo/common/a;->a(Lco/uk/getmondo/api/model/tracking/Impression;)V

    .line 69
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/id_picture/j;->n:Ljava/lang/String;

    if-nez v0, :cond_2

    move v0, v1

    .line 70
    :goto_0
    invoke-interface {p1}, Lco/uk/getmondo/signup/identity_verification/id_picture/j$a;->f()V

    .line 71
    iget-object v3, p0, Lco/uk/getmondo/signup/identity_verification/id_picture/j;->f:Lco/uk/getmondo/signup/identity_verification/id_picture/s;

    invoke-virtual {v3, v0}, Lco/uk/getmondo/signup/identity_verification/id_picture/s;->a(Z)Ljava/lang/String;

    move-result-object v3

    invoke-interface {p1, v3}, Lco/uk/getmondo/signup/identity_verification/id_picture/j$a;->a(Ljava/lang/String;)V

    .line 72
    if-nez v0, :cond_0

    .line 73
    iget-object v3, p0, Lco/uk/getmondo/signup/identity_verification/id_picture/j;->n:Ljava/lang/String;

    invoke-interface {p1, v3}, Lco/uk/getmondo/signup/identity_verification/id_picture/j$a;->d(Ljava/lang/String;)V

    .line 76
    :cond_0
    iget-object v3, p0, Lco/uk/getmondo/signup/identity_verification/id_picture/j;->k:Lco/uk/getmondo/signup/identity_verification/a/j;

    sget-object v4, Lco/uk/getmondo/signup/identity_verification/a/j;->b:Lco/uk/getmondo/signup/identity_verification/a/j;

    if-ne v3, v4, :cond_1

    iget-object v3, p0, Lco/uk/getmondo/signup/identity_verification/id_picture/j;->l:Lco/uk/getmondo/api/model/identity_verification/IdentityDocumentType;

    invoke-virtual {v3}, Lco/uk/getmondo/api/model/identity_verification/IdentityDocumentType;->b()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 77
    iget-object v3, p0, Lco/uk/getmondo/signup/identity_verification/id_picture/j;->n:Ljava/lang/String;

    if-nez v3, :cond_3

    :goto_1
    invoke-interface {p1, v1}, Lco/uk/getmondo/signup/identity_verification/id_picture/j$a;->a(Z)V

    .line 80
    :cond_1
    invoke-interface {p1}, Lco/uk/getmondo/signup/identity_verification/id_picture/j$a;->b()Lio/reactivex/n;

    move-result-object v1

    invoke-static {p1}, Lco/uk/getmondo/signup/identity_verification/id_picture/k;->a(Lco/uk/getmondo/signup/identity_verification/id_picture/j$a;)Lio/reactivex/c/g;

    move-result-object v2

    .line 81
    invoke-virtual {v1, v2}, Lio/reactivex/n;->subscribe(Lio/reactivex/c/g;)Lio/reactivex/b/b;

    move-result-object v1

    .line 80
    invoke-virtual {p0, v1}, Lco/uk/getmondo/signup/identity_verification/id_picture/j;->a(Lio/reactivex/b/b;)V

    .line 88
    invoke-interface {p1}, Lco/uk/getmondo/signup/identity_verification/id_picture/j$a;->d()Lio/reactivex/n;

    move-result-object v1

    invoke-static {p0}, Lco/uk/getmondo/signup/identity_verification/id_picture/l;->a(Lco/uk/getmondo/signup/identity_verification/id_picture/j;)Lio/reactivex/c/h;

    move-result-object v2

    .line 89
    invoke-virtual {v1, v2}, Lio/reactivex/n;->concatMap(Lio/reactivex/c/h;)Lio/reactivex/n;

    move-result-object v1

    iget-object v2, p0, Lco/uk/getmondo/signup/identity_verification/id_picture/j;->d:Lio/reactivex/u;

    .line 90
    invoke-virtual {v1, v2}, Lio/reactivex/n;->observeOn(Lio/reactivex/u;)Lio/reactivex/n;

    move-result-object v1

    invoke-static {p0, p1}, Lco/uk/getmondo/signup/identity_verification/id_picture/m;->a(Lco/uk/getmondo/signup/identity_verification/id_picture/j;Lco/uk/getmondo/signup/identity_verification/id_picture/j$a;)Lio/reactivex/c/g;

    move-result-object v2

    .line 91
    invoke-virtual {v1, v2}, Lio/reactivex/n;->doOnNext(Lio/reactivex/c/g;)Lio/reactivex/n;

    move-result-object v1

    invoke-static {p0, p1, v0}, Lco/uk/getmondo/signup/identity_verification/id_picture/n;->a(Lco/uk/getmondo/signup/identity_verification/id_picture/j;Lco/uk/getmondo/signup/identity_verification/id_picture/j$a;Z)Lio/reactivex/c/h;

    move-result-object v2

    .line 97
    invoke-virtual {v1, v2}, Lio/reactivex/n;->concatMap(Lio/reactivex/c/h;)Lio/reactivex/n;

    move-result-object v1

    iget-object v2, p0, Lco/uk/getmondo/signup/identity_verification/id_picture/j;->d:Lio/reactivex/u;

    .line 98
    invoke-virtual {v1, v2}, Lio/reactivex/n;->observeOn(Lio/reactivex/u;)Lio/reactivex/n;

    move-result-object v1

    invoke-static {p0, p1}, Lco/uk/getmondo/signup/identity_verification/id_picture/o;->a(Lco/uk/getmondo/signup/identity_verification/id_picture/j;Lco/uk/getmondo/signup/identity_verification/id_picture/j$a;)Lio/reactivex/c/g;

    move-result-object v2

    .line 99
    invoke-virtual {v1, v2}, Lio/reactivex/n;->subscribe(Lio/reactivex/c/g;)Lio/reactivex/b/b;

    move-result-object v1

    .line 88
    invoke-virtual {p0, v1}, Lco/uk/getmondo/signup/identity_verification/id_picture/j;->a(Lio/reactivex/b/b;)V

    .line 104
    invoke-interface {p1}, Lco/uk/getmondo/signup/identity_verification/id_picture/j$a;->c()Lio/reactivex/n;

    move-result-object v1

    invoke-static {p0, p1}, Lco/uk/getmondo/signup/identity_verification/id_picture/p;->a(Lco/uk/getmondo/signup/identity_verification/id_picture/j;Lco/uk/getmondo/signup/identity_verification/id_picture/j$a;)Lio/reactivex/c/g;

    move-result-object v2

    .line 105
    invoke-virtual {v1, v2}, Lio/reactivex/n;->subscribe(Lio/reactivex/c/g;)Lio/reactivex/b/b;

    move-result-object v1

    .line 104
    invoke-virtual {p0, v1}, Lco/uk/getmondo/signup/identity_verification/id_picture/j;->a(Lio/reactivex/b/b;)V

    .line 122
    invoke-interface {p1}, Lco/uk/getmondo/signup/identity_verification/id_picture/j$a;->a()Lio/reactivex/n;

    move-result-object v1

    invoke-static {p0, p1, v0}, Lco/uk/getmondo/signup/identity_verification/id_picture/q;->a(Lco/uk/getmondo/signup/identity_verification/id_picture/j;Lco/uk/getmondo/signup/identity_verification/id_picture/j$a;Z)Lio/reactivex/c/g;

    move-result-object v0

    .line 123
    invoke-virtual {v1, v0}, Lio/reactivex/n;->subscribe(Lio/reactivex/c/g;)Lio/reactivex/b/b;

    move-result-object v0

    .line 122
    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/identity_verification/id_picture/j;->a(Lio/reactivex/b/b;)V

    .line 136
    return-void

    :cond_2
    move v0, v2

    .line 69
    goto/16 :goto_0

    :cond_3
    move v1, v2

    .line 77
    goto :goto_1
.end method
