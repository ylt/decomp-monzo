.class final Lco/uk/getmondo/signup/identity_verification/id_picture/u$b;
.super Ljava/lang/Object;
.source "DocumentImageResizer.kt"

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lco/uk/getmondo/signup/identity_verification/id_picture/u;->a(Ljava/io/File;)Lio/reactivex/n;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<TT;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000\u0008\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u0001H\n\u00a2\u0006\u0002\u0008\u0002"
    }
    d2 = {
        "<anonymous>",
        "Landroid/graphics/Bitmap;",
        "call"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field final synthetic a:Lco/uk/getmondo/signup/identity_verification/id_picture/u;

.field final synthetic b:Ljava/io/File;


# direct methods
.method constructor <init>(Lco/uk/getmondo/signup/identity_verification/id_picture/u;Ljava/io/File;)V
    .locals 0

    iput-object p1, p0, Lco/uk/getmondo/signup/identity_verification/id_picture/u$b;->a:Lco/uk/getmondo/signup/identity_verification/id_picture/u;

    iput-object p2, p0, Lco/uk/getmondo/signup/identity_verification/id_picture/u$b;->b:Ljava/io/File;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Landroid/graphics/Bitmap;
    .locals 4

    .prologue
    .line 39
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/id_picture/u$b;->a:Lco/uk/getmondo/signup/identity_verification/id_picture/u;

    iget-object v1, p0, Lco/uk/getmondo/signup/identity_verification/id_picture/u$b;->b:Ljava/io/File;

    invoke-static {v0, v1}, Lco/uk/getmondo/signup/identity_verification/id_picture/u;->a(Lco/uk/getmondo/signup/identity_verification/id_picture/u;Ljava/io/File;)Landroid/util/SizeF;

    move-result-object v0

    .line 40
    iget-object v1, p0, Lco/uk/getmondo/signup/identity_verification/id_picture/u$b;->b:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lco/uk/getmondo/signup/identity_verification/id_picture/u$b;->a:Lco/uk/getmondo/signup/identity_verification/id_picture/u;

    invoke-static {v2, v0}, Lco/uk/getmondo/signup/identity_verification/id_picture/u;->a(Lco/uk/getmondo/signup/identity_verification/id_picture/u;Landroid/util/SizeF;)Landroid/graphics/BitmapFactory$Options;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 41
    iget-object v1, p0, Lco/uk/getmondo/signup/identity_verification/id_picture/u$b;->a:Lco/uk/getmondo/signup/identity_verification/id_picture/u;

    new-instance v2, Landroid/support/d/a;

    iget-object v3, p0, Lco/uk/getmondo/signup/identity_verification/id_picture/u$b;->b:Ljava/io/File;

    invoke-virtual {v3}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/support/d/a;-><init>(Ljava/lang/String;)V

    invoke-static {v1, v2}, Lco/uk/getmondo/signup/identity_verification/id_picture/u;->a(Lco/uk/getmondo/signup/identity_verification/id_picture/u;Landroid/support/d/a;)Landroid/graphics/Matrix;

    move-result-object v1

    .line 42
    iget-object v2, p0, Lco/uk/getmondo/signup/identity_verification/id_picture/u$b;->a:Lco/uk/getmondo/signup/identity_verification/id_picture/u;

    const-string v3, "bitmap"

    invoke-static {v0, v3}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v3, 0x0

    invoke-static {v2, v0, v3, v1}, Lco/uk/getmondo/signup/identity_verification/id_picture/u;->a(Lco/uk/getmondo/signup/identity_verification/id_picture/u;Landroid/graphics/Bitmap;ZLandroid/graphics/Matrix;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method public synthetic call()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 13
    invoke-virtual {p0}, Lco/uk/getmondo/signup/identity_verification/id_picture/u$b;->a()Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method
