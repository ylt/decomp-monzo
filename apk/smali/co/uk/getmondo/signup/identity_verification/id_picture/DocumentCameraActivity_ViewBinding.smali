.class public Lco/uk/getmondo/signup/identity_verification/id_picture/DocumentCameraActivity_ViewBinding;
.super Ljava/lang/Object;
.source "DocumentCameraActivity_ViewBinding.java"

# interfaces
.implements Lbutterknife/Unbinder;


# instance fields
.field private a:Lco/uk/getmondo/signup/identity_verification/id_picture/DocumentCameraActivity;


# direct methods
.method public constructor <init>(Lco/uk/getmondo/signup/identity_verification/id_picture/DocumentCameraActivity;Landroid/view/View;)V
    .locals 3

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    iput-object p1, p0, Lco/uk/getmondo/signup/identity_verification/id_picture/DocumentCameraActivity_ViewBinding;->a:Lco/uk/getmondo/signup/identity_verification/id_picture/DocumentCameraActivity;

    .line 31
    const v0, 0x7f110231

    const-string v1, "field \'cameraView\'"

    const-class v2, Lcom/google/android/cameraview/CameraView;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/cameraview/CameraView;

    iput-object v0, p1, Lco/uk/getmondo/signup/identity_verification/id_picture/DocumentCameraActivity;->cameraView:Lcom/google/android/cameraview/CameraView;

    .line 32
    const v0, 0x7f110232

    const-string v1, "field \'previewImage\'"

    const-class v2, Landroid/widget/ImageView;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p1, Lco/uk/getmondo/signup/identity_verification/id_picture/DocumentCameraActivity;->previewImage:Landroid/widget/ImageView;

    .line 33
    const v0, 0x7f110235

    const-string v1, "field \'takePictureButton\'"

    const-class v2, Landroid/widget/ImageButton;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p1, Lco/uk/getmondo/signup/identity_verification/id_picture/DocumentCameraActivity;->takePictureButton:Landroid/widget/ImageButton;

    .line 34
    const v0, 0x7f110230

    const-string v1, "field \'instructionsView\'"

    const-class v2, Landroid/widget/TextView;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p1, Lco/uk/getmondo/signup/identity_verification/id_picture/DocumentCameraActivity;->instructionsView:Landroid/widget/TextView;

    .line 35
    const v0, 0x7f110233

    const-string v1, "field \'frameOverlayView\'"

    const-class v2, Lco/uk/getmondo/signup/identity_verification/id_picture/FrameOverlayView;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/signup/identity_verification/id_picture/FrameOverlayView;

    iput-object v0, p1, Lco/uk/getmondo/signup/identity_verification/id_picture/DocumentCameraActivity;->frameOverlayView:Lco/uk/getmondo/signup/identity_verification/id_picture/FrameOverlayView;

    .line 36
    const v0, 0x7f110236

    const-string v1, "field \'usePictureButton\'"

    const-class v2, Landroid/widget/Button;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p1, Lco/uk/getmondo/signup/identity_verification/id_picture/DocumentCameraActivity;->usePictureButton:Landroid/widget/Button;

    .line 37
    const v0, 0x7f1101b1

    const-string v1, "field \'cameraProgress\'"

    const-class v2, Landroid/widget/ProgressBar;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p1, Lco/uk/getmondo/signup/identity_verification/id_picture/DocumentCameraActivity;->cameraProgress:Landroid/widget/ProgressBar;

    .line 38
    const v0, 0x7f110237

    const-string v1, "field \'thumbnailImageView\'"

    const-class v2, Landroid/widget/ImageView;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p1, Lco/uk/getmondo/signup/identity_verification/id_picture/DocumentCameraActivity;->thumbnailImageView:Landroid/widget/ImageView;

    .line 39
    const v0, 0x7f110234

    const-string v1, "field \'documentPositionTextView\'"

    const-class v2, Landroid/widget/TextView;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p1, Lco/uk/getmondo/signup/identity_verification/id_picture/DocumentCameraActivity;->documentPositionTextView:Landroid/widget/TextView;

    .line 40
    return-void
.end method


# virtual methods
.method public unbind()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 45
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/id_picture/DocumentCameraActivity_ViewBinding;->a:Lco/uk/getmondo/signup/identity_verification/id_picture/DocumentCameraActivity;

    .line 46
    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Bindings already cleared."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 47
    :cond_0
    iput-object v1, p0, Lco/uk/getmondo/signup/identity_verification/id_picture/DocumentCameraActivity_ViewBinding;->a:Lco/uk/getmondo/signup/identity_verification/id_picture/DocumentCameraActivity;

    .line 49
    iput-object v1, v0, Lco/uk/getmondo/signup/identity_verification/id_picture/DocumentCameraActivity;->cameraView:Lcom/google/android/cameraview/CameraView;

    .line 50
    iput-object v1, v0, Lco/uk/getmondo/signup/identity_verification/id_picture/DocumentCameraActivity;->previewImage:Landroid/widget/ImageView;

    .line 51
    iput-object v1, v0, Lco/uk/getmondo/signup/identity_verification/id_picture/DocumentCameraActivity;->takePictureButton:Landroid/widget/ImageButton;

    .line 52
    iput-object v1, v0, Lco/uk/getmondo/signup/identity_verification/id_picture/DocumentCameraActivity;->instructionsView:Landroid/widget/TextView;

    .line 53
    iput-object v1, v0, Lco/uk/getmondo/signup/identity_verification/id_picture/DocumentCameraActivity;->frameOverlayView:Lco/uk/getmondo/signup/identity_verification/id_picture/FrameOverlayView;

    .line 54
    iput-object v1, v0, Lco/uk/getmondo/signup/identity_verification/id_picture/DocumentCameraActivity;->usePictureButton:Landroid/widget/Button;

    .line 55
    iput-object v1, v0, Lco/uk/getmondo/signup/identity_verification/id_picture/DocumentCameraActivity;->cameraProgress:Landroid/widget/ProgressBar;

    .line 56
    iput-object v1, v0, Lco/uk/getmondo/signup/identity_verification/id_picture/DocumentCameraActivity;->thumbnailImageView:Landroid/widget/ImageView;

    .line 57
    iput-object v1, v0, Lco/uk/getmondo/signup/identity_verification/id_picture/DocumentCameraActivity;->documentPositionTextView:Landroid/widget/TextView;

    .line 58
    return-void
.end method
