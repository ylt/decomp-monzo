.class final synthetic Lco/uk/getmondo/signup/identity_verification/id_picture/b;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/util/concurrent/Callable;


# instance fields
.field private final a:Lco/uk/getmondo/signup/identity_verification/id_picture/DocumentCameraActivity;

.field private final b:Z

.field private final c:Landroid/graphics/Bitmap;


# direct methods
.method private constructor <init>(Lco/uk/getmondo/signup/identity_verification/id_picture/DocumentCameraActivity;ZLandroid/graphics/Bitmap;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lco/uk/getmondo/signup/identity_verification/id_picture/b;->a:Lco/uk/getmondo/signup/identity_verification/id_picture/DocumentCameraActivity;

    iput-boolean p2, p0, Lco/uk/getmondo/signup/identity_verification/id_picture/b;->b:Z

    iput-object p3, p0, Lco/uk/getmondo/signup/identity_verification/id_picture/b;->c:Landroid/graphics/Bitmap;

    return-void
.end method

.method public static a(Lco/uk/getmondo/signup/identity_verification/id_picture/DocumentCameraActivity;ZLandroid/graphics/Bitmap;)Ljava/util/concurrent/Callable;
    .locals 1

    new-instance v0, Lco/uk/getmondo/signup/identity_verification/id_picture/b;

    invoke-direct {v0, p0, p1, p2}, Lco/uk/getmondo/signup/identity_verification/id_picture/b;-><init>(Lco/uk/getmondo/signup/identity_verification/id_picture/DocumentCameraActivity;ZLandroid/graphics/Bitmap;)V

    return-object v0
.end method


# virtual methods
.method public call()Ljava/lang/Object;
    .locals 3

    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/id_picture/b;->a:Lco/uk/getmondo/signup/identity_verification/id_picture/DocumentCameraActivity;

    iget-boolean v1, p0, Lco/uk/getmondo/signup/identity_verification/id_picture/b;->b:Z

    iget-object v2, p0, Lco/uk/getmondo/signup/identity_verification/id_picture/b;->c:Landroid/graphics/Bitmap;

    invoke-static {v0, v1, v2}, Lco/uk/getmondo/signup/identity_verification/id_picture/DocumentCameraActivity;->a(Lco/uk/getmondo/signup/identity_verification/id_picture/DocumentCameraActivity;ZLandroid/graphics/Bitmap;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method
