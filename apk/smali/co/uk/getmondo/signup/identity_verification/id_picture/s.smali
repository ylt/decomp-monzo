.class public Lco/uk/getmondo/signup/identity_verification/id_picture/s;
.super Ljava/lang/Object;
.source "DocumentCameraStringProvider.java"


# instance fields
.field private a:Landroid/content/Context;

.field private final b:Lco/uk/getmondo/api/model/identity_verification/IdentityDocumentType;


# direct methods
.method constructor <init>(Landroid/content/Context;Lco/uk/getmondo/api/model/identity_verification/IdentityDocumentType;)V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    iput-object p1, p0, Lco/uk/getmondo/signup/identity_verification/id_picture/s;->a:Landroid/content/Context;

    .line 19
    iput-object p2, p0, Lco/uk/getmondo/signup/identity_verification/id_picture/s;->b:Lco/uk/getmondo/api/model/identity_verification/IdentityDocumentType;

    .line 20
    return-void
.end method

.method private b()Ljava/lang/String;
    .locals 2

    .prologue
    .line 37
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/id_picture/s;->a:Landroid/content/Context;

    iget-object v1, p0, Lco/uk/getmondo/signup/identity_verification/id_picture/s;->b:Lco/uk/getmondo/api/model/identity_verification/IdentityDocumentType;

    invoke-virtual {v1}, Lco/uk/getmondo/api/model/identity_verification/IdentityDocumentType;->c()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method a()Ljava/lang/String;
    .locals 5

    .prologue
    .line 33
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/id_picture/s;->a:Landroid/content/Context;

    const v1, 0x7f0a01d0

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-direct {p0}, Lco/uk/getmondo/signup/identity_verification/id_picture/s;->b()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method a(Z)Ljava/lang/String;
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 23
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/id_picture/s;->b:Lco/uk/getmondo/api/model/identity_verification/IdentityDocumentType;

    invoke-virtual {v0}, Lco/uk/getmondo/api/model/identity_verification/IdentityDocumentType;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 24
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/id_picture/s;->a:Landroid/content/Context;

    const v1, 0x7f0a01ce

    new-array v2, v5, [Ljava/lang/Object;

    invoke-direct {p0}, Lco/uk/getmondo/signup/identity_verification/id_picture/s;->b()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 29
    :goto_0
    return-object v0

    .line 26
    :cond_0
    iget-object v1, p0, Lco/uk/getmondo/signup/identity_verification/id_picture/s;->a:Landroid/content/Context;

    if-eqz p1, :cond_1

    const v0, 0x7f0a01d6

    :goto_1
    invoke-virtual {v1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 28
    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    .line 29
    iget-object v1, p0, Lco/uk/getmondo/signup/identity_verification/id_picture/s;->a:Landroid/content/Context;

    const v2, 0x7f0a01cf

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    aput-object v0, v3, v4

    invoke-direct {p0}, Lco/uk/getmondo/signup/identity_verification/id_picture/s;->b()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v3, v5

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 26
    :cond_1
    const v0, 0x7f0a01d5

    goto :goto_1
.end method
