.class public Lco/uk/getmondo/signup/identity_verification/id_picture/FrameOverlayView;
.super Landroid/view/View;
.source "FrameOverlayView.java"


# instance fields
.field private final a:I

.field private final b:I

.field private final c:I

.field private final d:I

.field private final e:I

.field private final f:I

.field private final g:Landroid/graphics/Paint;

.field private final h:Landroid/graphics/Paint;

.field private final i:Landroid/graphics/Paint;

.field private final j:Landroid/graphics/Path;

.field private k:Lco/uk/getmondo/api/model/identity_verification/IdentityDocumentType;

.field private l:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 39
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lco/uk/getmondo/signup/identity_verification/id_picture/FrameOverlayView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 40
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 43
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lco/uk/getmondo/signup/identity_verification/id_picture/FrameOverlayView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 44
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 5

    .prologue
    const/16 v1, 0x10

    const/4 v2, 0x2

    const/4 v4, 0x1

    .line 47
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 22
    invoke-static {v1}, Lco/uk/getmondo/common/k/n;->b(I)I

    move-result v0

    iput v0, p0, Lco/uk/getmondo/signup/identity_verification/id_picture/FrameOverlayView;->a:I

    .line 23
    const/16 v0, 0x3e

    invoke-static {v0}, Lco/uk/getmondo/common/k/n;->b(I)I

    move-result v0

    iput v0, p0, Lco/uk/getmondo/signup/identity_verification/id_picture/FrameOverlayView;->b:I

    .line 24
    invoke-static {v2}, Lco/uk/getmondo/common/k/n;->b(I)I

    move-result v0

    iput v0, p0, Lco/uk/getmondo/signup/identity_verification/id_picture/FrameOverlayView;->c:I

    .line 26
    const/16 v0, 0x4b

    invoke-static {v0}, Lco/uk/getmondo/common/k/n;->b(I)I

    move-result v0

    iput v0, p0, Lco/uk/getmondo/signup/identity_verification/id_picture/FrameOverlayView;->d:I

    .line 27
    invoke-static {v1}, Lco/uk/getmondo/common/k/n;->b(I)I

    move-result v0

    iput v0, p0, Lco/uk/getmondo/signup/identity_verification/id_picture/FrameOverlayView;->e:I

    .line 29
    const/4 v0, 0x3

    invoke-static {v0}, Lco/uk/getmondo/common/k/n;->b(I)I

    move-result v0

    iput v0, p0, Lco/uk/getmondo/signup/identity_verification/id_picture/FrameOverlayView;->f:I

    .line 34
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lco/uk/getmondo/signup/identity_verification/id_picture/FrameOverlayView;->j:Landroid/graphics/Path;

    .line 35
    sget-object v0, Lco/uk/getmondo/api/model/identity_verification/IdentityDocumentType;->PASSPORT:Lco/uk/getmondo/api/model/identity_verification/IdentityDocumentType;

    iput-object v0, p0, Lco/uk/getmondo/signup/identity_verification/id_picture/FrameOverlayView;->k:Lco/uk/getmondo/api/model/identity_verification/IdentityDocumentType;

    .line 36
    const/4 v0, 0x0

    iput-boolean v0, p0, Lco/uk/getmondo/signup/identity_verification/id_picture/FrameOverlayView;->l:Z

    .line 49
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0, v4}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lco/uk/getmondo/signup/identity_verification/id_picture/FrameOverlayView;->g:Landroid/graphics/Paint;

    .line 50
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/id_picture/FrameOverlayView;->g:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 51
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/id_picture/FrameOverlayView;->g:Landroid/graphics/Paint;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 52
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/id_picture/FrameOverlayView;->g:Landroid/graphics/Paint;

    iget v1, p0, Lco/uk/getmondo/signup/identity_verification/id_picture/FrameOverlayView;->c:I

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 53
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/id_picture/FrameOverlayView;->g:Landroid/graphics/Paint;

    new-instance v1, Landroid/graphics/DashPathEffect;

    new-array v2, v2, [F

    fill-array-data v2, :array_0

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3}, Landroid/graphics/DashPathEffect;-><init>([FF)V

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setPathEffect(Landroid/graphics/PathEffect;)Landroid/graphics/PathEffect;

    .line 55
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lco/uk/getmondo/signup/identity_verification/id_picture/FrameOverlayView;->i:Landroid/graphics/Paint;

    .line 56
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/id_picture/FrameOverlayView;->i:Landroid/graphics/Paint;

    invoke-virtual {v0, v4}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 57
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/id_picture/FrameOverlayView;->i:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 59
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lco/uk/getmondo/signup/identity_verification/id_picture/FrameOverlayView;->h:Landroid/graphics/Paint;

    .line 60
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/id_picture/FrameOverlayView;->h:Landroid/graphics/Paint;

    const v1, 0x7f0f0036

    invoke-static {p1, v1}, Landroid/support/v4/content/a;->c(Landroid/content/Context;I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 61
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/id_picture/FrameOverlayView;->h:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 62
    return-void

    .line 53
    :array_0
    .array-data 4
        0x41a00000    # 20.0f
        0x41200000    # 10.0f
    .end array-data
.end method

.method private a(Landroid/graphics/Canvas;)V
    .locals 2

    .prologue
    .line 102
    iget-boolean v0, p0, Lco/uk/getmondo/signup/identity_verification/id_picture/FrameOverlayView;->l:Z

    if-eqz v0, :cond_0

    .line 103
    iget v0, p0, Lco/uk/getmondo/signup/identity_verification/id_picture/FrameOverlayView;->b:I

    iget v1, p0, Lco/uk/getmondo/signup/identity_verification/id_picture/FrameOverlayView;->a:I

    invoke-direct {p0, p1, v0, v1}, Lco/uk/getmondo/signup/identity_verification/id_picture/FrameOverlayView;->a(Landroid/graphics/Canvas;II)V

    .line 107
    :cond_0
    iget v0, p0, Lco/uk/getmondo/signup/identity_verification/id_picture/FrameOverlayView;->b:I

    iget v1, p0, Lco/uk/getmondo/signup/identity_verification/id_picture/FrameOverlayView;->a:I

    invoke-direct {p0, p1, v0, v1}, Lco/uk/getmondo/signup/identity_verification/id_picture/FrameOverlayView;->b(Landroid/graphics/Canvas;II)V

    .line 110
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/id_picture/FrameOverlayView;->j:Landroid/graphics/Path;

    iget-object v1, p0, Lco/uk/getmondo/signup/identity_verification/id_picture/FrameOverlayView;->g:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 111
    return-void
.end method

.method private a(Landroid/graphics/Canvas;II)V
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 122
    iget v0, p0, Lco/uk/getmondo/signup/identity_verification/id_picture/FrameOverlayView;->c:I

    int-to-float v0, v0

    const/high16 v2, 0x40000000    # 2.0f

    div-float v6, v0, v2

    .line 124
    int-to-float v0, p2

    add-float v3, v0, v6

    invoke-virtual {p0}, Lco/uk/getmondo/signup/identity_verification/id_picture/FrameOverlayView;->getHeight()I

    move-result v0

    int-to-float v4, v0

    iget-object v5, p0, Lco/uk/getmondo/signup/identity_verification/id_picture/FrameOverlayView;->h:Landroid/graphics/Paint;

    move-object v0, p1

    move v2, v1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 126
    invoke-virtual {p0}, Lco/uk/getmondo/signup/identity_verification/id_picture/FrameOverlayView;->getWidth()I

    move-result v0

    sub-int/2addr v0, p2

    int-to-float v0, v0

    sub-float v3, v0, v6

    invoke-virtual {p0}, Lco/uk/getmondo/signup/identity_verification/id_picture/FrameOverlayView;->getWidth()I

    move-result v0

    int-to-float v5, v0

    invoke-virtual {p0}, Lco/uk/getmondo/signup/identity_verification/id_picture/FrameOverlayView;->getHeight()I

    move-result v0

    int-to-float v6, v0

    iget-object v7, p0, Lco/uk/getmondo/signup/identity_verification/id_picture/FrameOverlayView;->h:Landroid/graphics/Paint;

    move-object v2, p1

    move v4, v1

    invoke-virtual/range {v2 .. v7}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 128
    int-to-float v3, p2

    invoke-virtual {p0}, Lco/uk/getmondo/signup/identity_verification/id_picture/FrameOverlayView;->getWidth()I

    move-result v0

    sub-int/2addr v0, p2

    int-to-float v5, v0

    int-to-float v6, p3

    iget-object v7, p0, Lco/uk/getmondo/signup/identity_verification/id_picture/FrameOverlayView;->h:Landroid/graphics/Paint;

    move-object v2, p1

    move v4, v1

    invoke-virtual/range {v2 .. v7}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 130
    int-to-float v1, p2

    invoke-virtual {p0}, Lco/uk/getmondo/signup/identity_verification/id_picture/FrameOverlayView;->getHeight()I

    move-result v0

    sub-int/2addr v0, p3

    int-to-float v2, v0

    invoke-virtual {p0}, Lco/uk/getmondo/signup/identity_verification/id_picture/FrameOverlayView;->getWidth()I

    move-result v0

    sub-int/2addr v0, p2

    int-to-float v3, v0

    invoke-virtual {p0}, Lco/uk/getmondo/signup/identity_verification/id_picture/FrameOverlayView;->getHeight()I

    move-result v0

    int-to-float v4, v0

    iget-object v5, p0, Lco/uk/getmondo/signup/identity_verification/id_picture/FrameOverlayView;->h:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 131
    return-void
.end method

.method private b(Landroid/graphics/Canvas;)V
    .locals 2

    .prologue
    .line 114
    iget-boolean v0, p0, Lco/uk/getmondo/signup/identity_verification/id_picture/FrameOverlayView;->l:Z

    if-eqz v0, :cond_0

    .line 115
    iget v0, p0, Lco/uk/getmondo/signup/identity_verification/id_picture/FrameOverlayView;->e:I

    iget v1, p0, Lco/uk/getmondo/signup/identity_verification/id_picture/FrameOverlayView;->d:I

    invoke-direct {p0, p1, v0, v1}, Lco/uk/getmondo/signup/identity_verification/id_picture/FrameOverlayView;->a(Landroid/graphics/Canvas;II)V

    .line 118
    :cond_0
    iget v0, p0, Lco/uk/getmondo/signup/identity_verification/id_picture/FrameOverlayView;->e:I

    iget v1, p0, Lco/uk/getmondo/signup/identity_verification/id_picture/FrameOverlayView;->d:I

    invoke-direct {p0, p1, v0, v1}, Lco/uk/getmondo/signup/identity_verification/id_picture/FrameOverlayView;->b(Landroid/graphics/Canvas;II)V

    .line 119
    return-void
.end method

.method private b(Landroid/graphics/Canvas;II)V
    .locals 8

    .prologue
    .line 134
    int-to-float v1, p2

    int-to-float v2, p3

    invoke-virtual {p0}, Lco/uk/getmondo/signup/identity_verification/id_picture/FrameOverlayView;->getWidth()I

    move-result v0

    sub-int/2addr v0, p2

    int-to-float v3, v0

    invoke-virtual {p0}, Lco/uk/getmondo/signup/identity_verification/id_picture/FrameOverlayView;->getHeight()I

    move-result v0

    sub-int/2addr v0, p3

    int-to-float v4, v0

    iget v0, p0, Lco/uk/getmondo/signup/identity_verification/id_picture/FrameOverlayView;->f:I

    int-to-float v5, v0

    iget v0, p0, Lco/uk/getmondo/signup/identity_verification/id_picture/FrameOverlayView;->f:I

    int-to-float v6, v0

    iget-object v7, p0, Lco/uk/getmondo/signup/identity_verification/id_picture/FrameOverlayView;->i:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v7}, Landroid/graphics/Canvas;->drawRoundRect(FFFFFFLandroid/graphics/Paint;)V

    .line 136
    return-void
.end method


# virtual methods
.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 2

    .prologue
    .line 92
    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    .line 94
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/id_picture/FrameOverlayView;->k:Lco/uk/getmondo/api/model/identity_verification/IdentityDocumentType;

    sget-object v1, Lco/uk/getmondo/api/model/identity_verification/IdentityDocumentType;->PASSPORT:Lco/uk/getmondo/api/model/identity_verification/IdentityDocumentType;

    if-ne v0, v1, :cond_0

    .line 95
    invoke-direct {p0, p1}, Lco/uk/getmondo/signup/identity_verification/id_picture/FrameOverlayView;->a(Landroid/graphics/Canvas;)V

    .line 99
    :goto_0
    return-void

    .line 97
    :cond_0
    invoke-direct {p0, p1}, Lco/uk/getmondo/signup/identity_verification/id_picture/FrameOverlayView;->b(Landroid/graphics/Canvas;)V

    goto :goto_0
.end method

.method protected onLayout(ZIIII)V
    .locals 3

    .prologue
    .line 81
    invoke-super/range {p0 .. p5}, Landroid/view/View;->onLayout(ZIIII)V

    .line 83
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/id_picture/FrameOverlayView;->k:Lco/uk/getmondo/api/model/identity_verification/IdentityDocumentType;

    sget-object v1, Lco/uk/getmondo/api/model/identity_verification/IdentityDocumentType;->PASSPORT:Lco/uk/getmondo/api/model/identity_verification/IdentityDocumentType;

    if-ne v0, v1, :cond_0

    .line 84
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/id_picture/FrameOverlayView;->j:Landroid/graphics/Path;

    invoke-virtual {v0}, Landroid/graphics/Path;->reset()V

    .line 85
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/id_picture/FrameOverlayView;->j:Landroid/graphics/Path;

    iget v1, p0, Lco/uk/getmondo/signup/identity_verification/id_picture/FrameOverlayView;->b:I

    int-to-float v1, v1

    invoke-virtual {p0}, Lco/uk/getmondo/signup/identity_verification/id_picture/FrameOverlayView;->getHeight()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    int-to-float v2, v2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->moveTo(FF)V

    .line 86
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/id_picture/FrameOverlayView;->j:Landroid/graphics/Path;

    invoke-virtual {p0}, Lco/uk/getmondo/signup/identity_verification/id_picture/FrameOverlayView;->getWidth()I

    move-result v1

    iget v2, p0, Lco/uk/getmondo/signup/identity_verification/id_picture/FrameOverlayView;->b:I

    sub-int/2addr v1, v2

    int-to-float v1, v1

    invoke-virtual {p0}, Lco/uk/getmondo/signup/identity_verification/id_picture/FrameOverlayView;->getHeight()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    int-to-float v2, v2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 88
    :cond_0
    return-void
.end method

.method public setFrameIdType(Lco/uk/getmondo/api/model/identity_verification/IdentityDocumentType;)V
    .locals 2

    .prologue
    .line 65
    iput-object p1, p0, Lco/uk/getmondo/signup/identity_verification/id_picture/FrameOverlayView;->k:Lco/uk/getmondo/api/model/identity_verification/IdentityDocumentType;

    .line 66
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/id_picture/FrameOverlayView;->i:Landroid/graphics/Paint;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 67
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/id_picture/FrameOverlayView;->i:Landroid/graphics/Paint;

    iget v1, p0, Lco/uk/getmondo/signup/identity_verification/id_picture/FrameOverlayView;->c:I

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 68
    return-void
.end method

.method public setOpaqueFrame(Z)V
    .locals 1

    .prologue
    .line 71
    iget-boolean v0, p0, Lco/uk/getmondo/signup/identity_verification/id_picture/FrameOverlayView;->l:Z

    if-ne v0, p1, :cond_0

    .line 77
    :goto_0
    return-void

    .line 75
    :cond_0
    iput-boolean p1, p0, Lco/uk/getmondo/signup/identity_verification/id_picture/FrameOverlayView;->l:Z

    .line 76
    invoke-virtual {p0}, Lco/uk/getmondo/signup/identity_verification/id_picture/FrameOverlayView;->invalidate()V

    goto :goto_0
.end method
