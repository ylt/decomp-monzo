.class final Lco/uk/getmondo/signup/identity_verification/g$i;
.super Ljava/lang/Object;
.source "IdentityDocumentsPresenter.kt"

# interfaces
.implements Lio/reactivex/c/g;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lco/uk/getmondo/signup/identity_verification/g;->a(Lco/uk/getmondo/signup/identity_verification/g$a;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/g",
        "<",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0003\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0004\u0008\u0005\u0010\u0006"
    }
    d2 = {
        "<anonymous>",
        "",
        "usedSystemCamera",
        "",
        "kotlin.jvm.PlatformType",
        "accept",
        "(Ljava/lang/Boolean;)V"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field final synthetic a:Lco/uk/getmondo/signup/identity_verification/g;


# direct methods
.method constructor <init>(Lco/uk/getmondo/signup/identity_verification/g;)V
    .locals 0

    iput-object p1, p0, Lco/uk/getmondo/signup/identity_verification/g$i;->a:Lco/uk/getmondo/signup/identity_verification/g;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Boolean;)V
    .locals 4

    .prologue
    .line 149
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/g$i;->a:Lco/uk/getmondo/signup/identity_verification/g;

    invoke-static {v0}, Lco/uk/getmondo/signup/identity_verification/g;->e(Lco/uk/getmondo/signup/identity_verification/g;)Lco/uk/getmondo/common/a;

    move-result-object v0

    sget-object v1, Lco/uk/getmondo/api/model/tracking/Impression;->Companion:Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    iget-object v2, p0, Lco/uk/getmondo/signup/identity_verification/g$i;->a:Lco/uk/getmondo/signup/identity_verification/g;

    invoke-static {v2}, Lco/uk/getmondo/signup/identity_verification/g;->j(Lco/uk/getmondo/signup/identity_verification/g;)Lco/uk/getmondo/api/model/tracking/Impression$KycFrom;

    move-result-object v2

    if-nez p1, :cond_0

    invoke-static {}, Lkotlin/d/b/l;->a()V

    :cond_0
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    invoke-virtual {v1, v2, v3}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->a(Lco/uk/getmondo/api/model/tracking/Impression$KycFrom;Z)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v1

    invoke-virtual {v0, v1}, Lco/uk/getmondo/common/a;->a(Lco/uk/getmondo/api/model/tracking/Impression;)V

    return-void
.end method

.method public bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 26
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Lco/uk/getmondo/signup/identity_verification/g$i;->a(Ljava/lang/Boolean;)V

    return-void
.end method
