.class final Lco/uk/getmondo/signup/identity_verification/a/k$c;
.super Ljava/lang/Object;
.source "LegacyIdentityVerificationManager.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lco/uk/getmondo/signup/identity_verification/a/k;->d()Lio/reactivex/v;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h",
        "<TT;TR;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n\u00a2\u0006\u0002\u0008\u0004"
    }
    d2 = {
        "<anonymous>",
        "Lco/uk/getmondo/api/model/identity_verification/IdentityVerification;",
        "it",
        "Lco/uk/getmondo/api/model/identity_verification/LegacyKyc;",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# static fields
.field public static final a:Lco/uk/getmondo/signup/identity_verification/a/k$c;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lco/uk/getmondo/signup/identity_verification/a/k$c;

    invoke-direct {v0}, Lco/uk/getmondo/signup/identity_verification/a/k$c;-><init>()V

    sput-object v0, Lco/uk/getmondo/signup/identity_verification/a/k$c;->a:Lco/uk/getmondo/signup/identity_verification/a/k$c;

    return-void
.end method

.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lco/uk/getmondo/api/model/identity_verification/LegacyKyc;)Lco/uk/getmondo/api/model/identity_verification/IdentityVerification;
    .locals 1

    .prologue
    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 26
    invoke-virtual {p1}, Lco/uk/getmondo/api/model/identity_verification/LegacyKyc;->a()Lco/uk/getmondo/api/model/identity_verification/IdentityVerification;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 17
    check-cast p1, Lco/uk/getmondo/api/model/identity_verification/LegacyKyc;

    invoke-virtual {p0, p1}, Lco/uk/getmondo/signup/identity_verification/a/k$c;->a(Lco/uk/getmondo/api/model/identity_verification/LegacyKyc;)Lco/uk/getmondo/api/model/identity_verification/IdentityVerification;

    move-result-object v0

    return-object v0
.end method
