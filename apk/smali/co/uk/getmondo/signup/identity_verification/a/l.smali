.class public final Lco/uk/getmondo/signup/identity_verification/a/l;
.super Ljava/lang/Object;
.source "NewIdentityVerificationManager.kt"

# interfaces
.implements Lco/uk/getmondo/signup/identity_verification/a/e;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000l\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0006\u0018\u00002\u00020\u0001B-\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b\u00a2\u0006\u0002\u0010\u000cJ4\u0010\r\u001a&\u0012\u000c\u0012\n \u0010*\u0004\u0018\u00010\u000f0\u000f \u0010*\u0012\u0012\u000c\u0012\n \u0010*\u0004\u0018\u00010\u000f0\u000f\u0018\u00010\u000e0\u000e2\u0006\u0010\u0011\u001a\u00020\u0012H\u0002J\u0014\u0010\u0013\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00160\u00150\u0014H\u0016J\u000e\u0010\u0017\u001a\u0008\u0012\u0004\u0012\u00020\u00180\u000eH\u0016J\u000e\u0010\u0019\u001a\u0008\u0012\u0004\u0012\u00020\u001a0\u000eH\u0016J\u0008\u0010\u001b\u001a\u00020\u001cH\u0002J\u001e\u0010\u001d\u001a\u0008\u0012\u0004\u0012\u00020\u00120\u000e2\u0006\u0010\u001e\u001a\u00020\u00122\u0006\u0010\u001f\u001a\u00020 H\u0002J\u0018\u0010!\u001a\u00020\"2\u0006\u0010#\u001a\u00020\u00162\u0006\u0010$\u001a\u00020\u001aH\u0016J\u0018\u0010%\u001a\u00020\"2\u0006\u0010&\u001a\u00020\u00122\u0006\u0010$\u001a\u00020\u001aH\u0016J\u0014\u0010\'\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00120\u00150\u0014H\u0016R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006("
    }
    d2 = {
        "Lco/uk/getmondo/signup/identity_verification/data/NewIdentityVerificationManager;",
        "Lco/uk/getmondo/signup/identity_verification/data/IdentityVerificationManager;",
        "storage",
        "Lco/uk/getmondo/signup/identity_verification/data/IdentityVerificationStorage;",
        "api",
        "Lco/uk/getmondo/api/IdentityVerificationApi;",
        "fileUploader",
        "Lco/uk/getmondo/api/FileUploader;",
        "deleteFeedItemStorage",
        "Lco/uk/getmondo/common/DeleteFeedItemStorage;",
        "source",
        "Lco/uk/getmondo/api/model/signup/SignupSource;",
        "(Lco/uk/getmondo/signup/identity_verification/data/IdentityVerificationStorage;Lco/uk/getmondo/api/IdentityVerificationApi;Lco/uk/getmondo/api/FileUploader;Lco/uk/getmondo/common/DeleteFeedItemStorage;Lco/uk/getmondo/api/model/signup/SignupSource;)V",
        "fileWrapper",
        "Lio/reactivex/Single;",
        "Lco/uk/getmondo/signup/identity_verification/data/model/FileWrapper;",
        "kotlin.jvm.PlatformType",
        "path",
        "",
        "identityDocument",
        "Lio/reactivex/Observable;",
        "Lcom/memoizrlabs/poweroptional/Optional;",
        "Lco/uk/getmondo/signup/identity_verification/data/model/IdentityDocument;",
        "status",
        "Lco/uk/getmondo/api/model/identity_verification/IdentityVerification;",
        "submitEvidence",
        "",
        "uploadDocuments",
        "Lio/reactivex/Completable;",
        "uploadFile",
        "filePath",
        "fileType",
        "Lco/uk/getmondo/api/model/identity_verification/FileType;",
        "useIdentityDocument",
        "",
        "evidence",
        "usedSystemCamera",
        "useVideoSelfie",
        "videoPath",
        "videoSelfiePath",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field private final a:Lco/uk/getmondo/signup/identity_verification/a/h;

.field private final b:Lco/uk/getmondo/api/IdentityVerificationApi;

.field private final c:Lco/uk/getmondo/api/ae;

.field private final d:Lco/uk/getmondo/common/i;

.field private final e:Lco/uk/getmondo/api/model/signup/SignupSource;


# direct methods
.method public constructor <init>(Lco/uk/getmondo/signup/identity_verification/a/h;Lco/uk/getmondo/api/IdentityVerificationApi;Lco/uk/getmondo/api/ae;Lco/uk/getmondo/common/i;Lco/uk/getmondo/api/model/signup/SignupSource;)V
    .locals 1

    .prologue
    const-string v0, "storage"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "api"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "fileUploader"

    invoke-static {p3, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "deleteFeedItemStorage"

    invoke-static {p4, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "source"

    invoke-static {p5, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lco/uk/getmondo/signup/identity_verification/a/l;->a:Lco/uk/getmondo/signup/identity_verification/a/h;

    iput-object p2, p0, Lco/uk/getmondo/signup/identity_verification/a/l;->b:Lco/uk/getmondo/api/IdentityVerificationApi;

    iput-object p3, p0, Lco/uk/getmondo/signup/identity_verification/a/l;->c:Lco/uk/getmondo/api/ae;

    iput-object p4, p0, Lco/uk/getmondo/signup/identity_verification/a/l;->d:Lco/uk/getmondo/common/i;

    iput-object p5, p0, Lco/uk/getmondo/signup/identity_verification/a/l;->e:Lco/uk/getmondo/api/model/signup/SignupSource;

    return-void
.end method

.method public static final synthetic a(Lco/uk/getmondo/signup/identity_verification/a/l;)Lco/uk/getmondo/signup/identity_verification/a/h;
    .locals 1

    .prologue
    .line 17
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/a/l;->a:Lco/uk/getmondo/signup/identity_verification/a/h;

    return-object v0
.end method

.method private final a(Ljava/lang/String;)Lio/reactivex/v;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/v",
            "<",
            "Lco/uk/getmondo/signup/identity_verification/a/a/a;",
            ">;"
        }
    .end annotation

    .prologue
    .line 92
    new-instance v0, Lco/uk/getmondo/signup/identity_verification/a/l$a;

    invoke-direct {v0, p1}, Lco/uk/getmondo/signup/identity_verification/a/l$a;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/util/concurrent/Callable;

    invoke-static {v0}, Lio/reactivex/v;->c(Ljava/util/concurrent/Callable;)Lio/reactivex/v;

    move-result-object v0

    return-object v0
.end method

.method private final a(Ljava/lang/String;Lco/uk/getmondo/api/model/identity_verification/FileType;)Lio/reactivex/v;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lco/uk/getmondo/api/model/identity_verification/FileType;",
            ")",
            "Lio/reactivex/v",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 83
    invoke-direct {p0, p1}, Lco/uk/getmondo/signup/identity_verification/a/l;->a(Ljava/lang/String;)Lio/reactivex/v;

    move-result-object v1

    .line 84
    new-instance v0, Lco/uk/getmondo/signup/identity_verification/a/l$g;

    invoke-direct {v0, p0, p2}, Lco/uk/getmondo/signup/identity_verification/a/l$g;-><init>(Lco/uk/getmondo/signup/identity_verification/a/l;Lco/uk/getmondo/api/model/identity_verification/FileType;)V

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {v1, v0}, Lio/reactivex/v;->a(Lio/reactivex/c/h;)Lio/reactivex/v;

    move-result-object v0

    const-string v1, "fileWrapper(filePath)\n  \u2026      }\n                }"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public static final synthetic b(Lco/uk/getmondo/signup/identity_verification/a/l;)Lco/uk/getmondo/common/i;
    .locals 1

    .prologue
    .line 17
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/a/l;->d:Lco/uk/getmondo/common/i;

    return-object v0
.end method

.method public static final synthetic c(Lco/uk/getmondo/signup/identity_verification/a/l;)Lco/uk/getmondo/api/IdentityVerificationApi;
    .locals 1

    .prologue
    .line 17
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/a/l;->b:Lco/uk/getmondo/api/IdentityVerificationApi;

    return-object v0
.end method

.method public static final synthetic d(Lco/uk/getmondo/signup/identity_verification/a/l;)Lco/uk/getmondo/api/model/signup/SignupSource;
    .locals 1

    .prologue
    .line 17
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/a/l;->e:Lco/uk/getmondo/api/model/signup/SignupSource;

    return-object v0
.end method

.method public static final synthetic e(Lco/uk/getmondo/signup/identity_verification/a/l;)Lco/uk/getmondo/api/ae;
    .locals 1

    .prologue
    .line 17
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/a/l;->c:Lco/uk/getmondo/api/ae;

    return-object v0
.end method

.method private final e()Lio/reactivex/b;
    .locals 5

    .prologue
    .line 55
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/a/l;->a:Lco/uk/getmondo/signup/identity_verification/a/h;

    invoke-virtual {v0}, Lco/uk/getmondo/signup/identity_verification/a/h;->a()Lco/uk/getmondo/signup/identity_verification/a/a/b;

    move-result-object v1

    .line 56
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/a/l;->a:Lco/uk/getmondo/signup/identity_verification/a/h;

    invoke-virtual {v0}, Lco/uk/getmondo/signup/identity_verification/a/h;->c()Ljava/lang/String;

    move-result-object v2

    .line 57
    if-eqz v1, :cond_0

    if-nez v2, :cond_1

    .line 58
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Cannot submit evidence because ID evidence or video "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "path is missing | idEvidence: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 59
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " videoPath "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 58
    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    invoke-static {v0}, Lio/reactivex/b;->a(Ljava/lang/Throwable;)Lio/reactivex/b;

    move-result-object v0

    const-string v1, "Completable.error(Illega\u2026e videoPath $videoPath\"))"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 79
    :goto_0
    return-object v0

    .line 62
    :cond_1
    invoke-virtual {v1}, Lco/uk/getmondo/signup/identity_verification/a/a/b;->d()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_2

    .line 64
    invoke-virtual {v1}, Lco/uk/getmondo/signup/identity_verification/a/a/b;->a()Ljava/lang/String;

    move-result-object v0

    sget-object v3, Lco/uk/getmondo/api/model/identity_verification/FileType;->IDENTITY_DOCUMENT:Lco/uk/getmondo/api/model/identity_verification/FileType;

    invoke-direct {p0, v0, v3}, Lco/uk/getmondo/signup/identity_verification/a/l;->a(Ljava/lang/String;Lco/uk/getmondo/api/model/identity_verification/FileType;)Lio/reactivex/v;

    move-result-object v3

    new-instance v0, Lco/uk/getmondo/signup/identity_verification/a/l$d;

    invoke-direct {v0, p0, v1}, Lco/uk/getmondo/signup/identity_verification/a/l$d;-><init>(Lco/uk/getmondo/signup/identity_verification/a/l;Lco/uk/getmondo/signup/identity_verification/a/a/b;)V

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {v3, v0}, Lio/reactivex/v;->c(Lio/reactivex/c/h;)Lio/reactivex/b;

    move-result-object v0

    move-object v1, v0

    .line 76
    :goto_1
    sget-object v0, Lco/uk/getmondo/api/model/identity_verification/FileType;->SELFIE_VIDEO:Lco/uk/getmondo/api/model/identity_verification/FileType;

    invoke-direct {p0, v2, v0}, Lco/uk/getmondo/signup/identity_verification/a/l;->a(Ljava/lang/String;Lco/uk/getmondo/api/model/identity_verification/FileType;)Lio/reactivex/v;

    move-result-object v2

    .line 77
    new-instance v0, Lco/uk/getmondo/signup/identity_verification/a/l$f;

    invoke-direct {v0, p0}, Lco/uk/getmondo/signup/identity_verification/a/l$f;-><init>(Lco/uk/getmondo/signup/identity_verification/a/l;)V

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {v2, v0}, Lio/reactivex/v;->c(Lio/reactivex/c/h;)Lio/reactivex/b;

    move-result-object v0

    .line 79
    check-cast v0, Lio/reactivex/d;

    invoke-virtual {v1, v0}, Lio/reactivex/b;->d(Lio/reactivex/d;)Lio/reactivex/b;

    move-result-object v1

    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/a/l;->b:Lco/uk/getmondo/api/IdentityVerificationApi;

    iget-object v2, p0, Lco/uk/getmondo/signup/identity_verification/a/l;->e:Lco/uk/getmondo/api/model/signup/SignupSource;

    invoke-interface {v0, v2}, Lco/uk/getmondo/api/IdentityVerificationApi;->submit(Lco/uk/getmondo/api/model/signup/SignupSource;)Lio/reactivex/b;

    move-result-object v0

    check-cast v0, Lio/reactivex/d;

    invoke-virtual {v1, v0}, Lio/reactivex/b;->b(Lio/reactivex/d;)Lio/reactivex/b;

    move-result-object v0

    const-string v1, "idEvidenceUpload.mergeWi\u2026dThen(api.submit(source))"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    .line 68
    :cond_2
    invoke-virtual {v1}, Lco/uk/getmondo/signup/identity_verification/a/a/b;->a()Ljava/lang/String;

    move-result-object v0

    sget-object v3, Lco/uk/getmondo/api/model/identity_verification/FileType;->IDENTITY_DOCUMENT:Lco/uk/getmondo/api/model/identity_verification/FileType;

    invoke-direct {p0, v0, v3}, Lco/uk/getmondo/signup/identity_verification/a/l;->a(Ljava/lang/String;Lco/uk/getmondo/api/model/identity_verification/FileType;)Lio/reactivex/v;

    move-result-object v3

    .line 69
    invoke-virtual {v1}, Lco/uk/getmondo/signup/identity_verification/a/a/b;->d()Ljava/lang/String;

    move-result-object v0

    sget-object v4, Lco/uk/getmondo/api/model/identity_verification/FileType;->IDENTITY_DOCUMENT:Lco/uk/getmondo/api/model/identity_verification/FileType;

    invoke-direct {p0, v0, v4}, Lco/uk/getmondo/signup/identity_verification/a/l;->a(Ljava/lang/String;Lco/uk/getmondo/api/model/identity_verification/FileType;)Lio/reactivex/v;

    move-result-object v0

    check-cast v0, Lio/reactivex/z;

    invoke-static {v3, v0}, Lco/uk/getmondo/common/j/f;->a(Lio/reactivex/v;Lio/reactivex/z;)Lio/reactivex/v;

    move-result-object v3

    .line 70
    new-instance v0, Lco/uk/getmondo/signup/identity_verification/a/l$e;

    invoke-direct {v0, p0, v1}, Lco/uk/getmondo/signup/identity_verification/a/l$e;-><init>(Lco/uk/getmondo/signup/identity_verification/a/l;Lco/uk/getmondo/signup/identity_verification/a/a/b;)V

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {v3, v0}, Lio/reactivex/v;->c(Lio/reactivex/c/h;)Lio/reactivex/b;

    move-result-object v0

    move-object v1, v0

    goto :goto_1
.end method


# virtual methods
.method public a()Lio/reactivex/v;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/v",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .prologue
    .line 29
    invoke-direct {p0}, Lco/uk/getmondo/signup/identity_verification/a/l;->e()Lio/reactivex/b;

    move-result-object v1

    .line 30
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/a/l;->a:Lco/uk/getmondo/signup/identity_verification/a/h;

    invoke-virtual {v0}, Lco/uk/getmondo/signup/identity_verification/a/h;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/a/l;->a:Lco/uk/getmondo/signup/identity_verification/a/h;

    invoke-virtual {v0}, Lco/uk/getmondo/signup/identity_verification/a/h;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v1, v0}, Lio/reactivex/b;->a(Ljava/lang/Object;)Lio/reactivex/v;

    move-result-object v1

    .line 31
    new-instance v0, Lco/uk/getmondo/signup/identity_verification/a/l$c;

    invoke-direct {v0, p0}, Lco/uk/getmondo/signup/identity_verification/a/l$c;-><init>(Lco/uk/getmondo/signup/identity_verification/a/l;)V

    check-cast v0, Lio/reactivex/c/g;

    invoke-virtual {v1, v0}, Lio/reactivex/v;->c(Lio/reactivex/c/g;)Lio/reactivex/v;

    move-result-object v0

    const-string v1, "uploadDocuments()\n      \u2026tFlag()\n                }"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0

    .line 30
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Lco/uk/getmondo/signup/identity_verification/a/a/b;Z)V
    .locals 1

    .prologue
    const-string v0, "evidence"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 47
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/a/l;->a:Lco/uk/getmondo/signup/identity_verification/a/h;

    invoke-virtual {v0, p1, p2}, Lco/uk/getmondo/signup/identity_verification/a/h;->a(Lco/uk/getmondo/signup/identity_verification/a/a/b;Z)V

    .line 48
    return-void
.end method

.method public a(Ljava/lang/String;Z)V
    .locals 1

    .prologue
    const-string v0, "videoPath"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 51
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/a/l;->a:Lco/uk/getmondo/signup/identity_verification/a/h;

    invoke-virtual {v0, p1, p2}, Lco/uk/getmondo/signup/identity_verification/a/h;->a(Ljava/lang/String;Z)V

    .line 52
    return-void
.end method

.method public b()Lio/reactivex/n;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Lcom/c/b/b",
            "<",
            "Lco/uk/getmondo/signup/identity_verification/a/a/b;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 42
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/a/l;->a:Lco/uk/getmondo/signup/identity_verification/a/h;

    invoke-virtual {v0}, Lco/uk/getmondo/signup/identity_verification/a/h;->f()Lio/reactivex/n;

    move-result-object v0

    return-object v0
.end method

.method public c()Lio/reactivex/n;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Lcom/c/b/b",
            "<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 44
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/a/l;->a:Lco/uk/getmondo/signup/identity_verification/a/h;

    invoke-virtual {v0}, Lco/uk/getmondo/signup/identity_verification/a/h;->g()Lio/reactivex/n;

    move-result-object v0

    return-object v0
.end method

.method public d()Lio/reactivex/v;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/v",
            "<",
            "Lco/uk/getmondo/api/model/identity_verification/IdentityVerification;",
            ">;"
        }
    .end annotation

    .prologue
    .line 26
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/a/l;->b:Lco/uk/getmondo/api/IdentityVerificationApi;

    iget-object v1, p0, Lco/uk/getmondo/signup/identity_verification/a/l;->e:Lco/uk/getmondo/api/model/signup/SignupSource;

    invoke-interface {v0, v1}, Lco/uk/getmondo/api/IdentityVerificationApi;->status(Lco/uk/getmondo/api/model/signup/SignupSource;)Lio/reactivex/v;

    move-result-object v1

    new-instance v0, Lco/uk/getmondo/signup/identity_verification/a/l$b;

    invoke-direct {v0, p0}, Lco/uk/getmondo/signup/identity_verification/a/l$b;-><init>(Lco/uk/getmondo/signup/identity_verification/a/l;)V

    check-cast v0, Lio/reactivex/c/g;

    invoke-virtual {v1, v0}, Lio/reactivex/v;->c(Lio/reactivex/c/g;)Lio/reactivex/v;

    move-result-object v0

    const-string v1, "api.status(source).doOnS\u2026dentityVerification(it) }"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method
