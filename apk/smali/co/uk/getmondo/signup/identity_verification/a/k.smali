.class public final Lco/uk/getmondo/signup/identity_verification/a/k;
.super Ljava/lang/Object;
.source "LegacyIdentityVerificationManager.kt"

# interfaces
.implements Lco/uk/getmondo/signup/identity_verification/a/e;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lco/uk/getmondo/signup/identity_verification/a/k$a;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000^\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0006\n\u0002\u0010\u0002\n\u0002\u0008\u0007\u0018\u0000 &2\u00020\u0001:\u0001&B%\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u00a2\u0006\u0002\u0010\nJ\u0016\u0010\u000b\u001a\u0008\u0012\u0004\u0012\u00020\r0\u000c2\u0006\u0010\u000e\u001a\u00020\u000fH\u0002J\u0014\u0010\u0010\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00130\u00120\u0011H\u0016J\u000e\u0010\u0014\u001a\u0008\u0012\u0004\u0012\u00020\u00150\u000cH\u0016J\u000e\u0010\u0016\u001a\u0008\u0012\u0004\u0012\u00020\u00170\u000cH\u0016J\u0008\u0010\u0018\u001a\u00020\u0019H\u0002J(\u0010\u001a\u001a\u00020\u00192\u0006\u0010\u001b\u001a\u00020\r2\u0006\u0010\u001c\u001a\u00020\u000f2\u0006\u0010\u001d\u001a\u00020\u000f2\u0006\u0010\u001e\u001a\u00020\u000fH\u0002J\u0018\u0010\u001f\u001a\u00020 2\u0006\u0010!\u001a\u00020\u00132\u0006\u0010\"\u001a\u00020\u0017H\u0016J\u0018\u0010#\u001a\u00020 2\u0006\u0010$\u001a\u00020\u000f2\u0006\u0010\"\u001a\u00020\u0017H\u0016J\u0014\u0010%\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u000f0\u00120\u0011H\u0016R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\'"
    }
    d2 = {
        "Lco/uk/getmondo/signup/identity_verification/data/LegacyIdentityVerificationManager;",
        "Lco/uk/getmondo/signup/identity_verification/data/IdentityVerificationManager;",
        "storage",
        "Lco/uk/getmondo/signup/identity_verification/data/IdentityVerificationStorage;",
        "identityVerificationApi",
        "Lco/uk/getmondo/api/IdentityVerificationApi;",
        "fileUploader",
        "Lco/uk/getmondo/api/FileUploader;",
        "deleteFeedItemStorage",
        "Lco/uk/getmondo/common/DeleteFeedItemStorage;",
        "(Lco/uk/getmondo/signup/identity_verification/data/IdentityVerificationStorage;Lco/uk/getmondo/api/IdentityVerificationApi;Lco/uk/getmondo/api/FileUploader;Lco/uk/getmondo/common/DeleteFeedItemStorage;)V",
        "fileWrapper",
        "Lio/reactivex/Single;",
        "Lco/uk/getmondo/signup/identity_verification/data/model/FileWrapper;",
        "path",
        "",
        "identityDocument",
        "Lio/reactivex/Observable;",
        "Lcom/memoizrlabs/poweroptional/Optional;",
        "Lco/uk/getmondo/signup/identity_verification/data/model/IdentityDocument;",
        "status",
        "Lco/uk/getmondo/api/model/identity_verification/IdentityVerification;",
        "submitEvidence",
        "",
        "uploadDocuments",
        "Lio/reactivex/Completable;",
        "uploadEvidence",
        "wrapper",
        "groupId",
        "mediaType",
        "evidenceType",
        "useIdentityDocument",
        "",
        "evidence",
        "usedSystemCamera",
        "useVideoSelfie",
        "videoPath",
        "videoSelfiePath",
        "Companion",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# static fields
.field public static final a:Lco/uk/getmondo/signup/identity_verification/a/k$a;


# instance fields
.field private final b:Lco/uk/getmondo/signup/identity_verification/a/h;

.field private final c:Lco/uk/getmondo/api/IdentityVerificationApi;

.field private final d:Lco/uk/getmondo/api/ae;

.field private final e:Lco/uk/getmondo/common/i;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lco/uk/getmondo/signup/identity_verification/a/k$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lco/uk/getmondo/signup/identity_verification/a/k$a;-><init>(Lkotlin/d/b/i;)V

    sput-object v0, Lco/uk/getmondo/signup/identity_verification/a/k;->a:Lco/uk/getmondo/signup/identity_verification/a/k$a;

    return-void
.end method

.method public constructor <init>(Lco/uk/getmondo/signup/identity_verification/a/h;Lco/uk/getmondo/api/IdentityVerificationApi;Lco/uk/getmondo/api/ae;Lco/uk/getmondo/common/i;)V
    .locals 1

    .prologue
    const-string v0, "storage"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "identityVerificationApi"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "fileUploader"

    invoke-static {p3, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "deleteFeedItemStorage"

    invoke-static {p4, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lco/uk/getmondo/signup/identity_verification/a/k;->b:Lco/uk/getmondo/signup/identity_verification/a/h;

    iput-object p2, p0, Lco/uk/getmondo/signup/identity_verification/a/k;->c:Lco/uk/getmondo/api/IdentityVerificationApi;

    iput-object p3, p0, Lco/uk/getmondo/signup/identity_verification/a/k;->d:Lco/uk/getmondo/api/ae;

    iput-object p4, p0, Lco/uk/getmondo/signup/identity_verification/a/k;->e:Lco/uk/getmondo/common/i;

    return-void
.end method

.method public static final synthetic a(Lco/uk/getmondo/signup/identity_verification/a/k;)Lco/uk/getmondo/signup/identity_verification/a/h;
    .locals 1

    .prologue
    .line 17
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/a/k;->b:Lco/uk/getmondo/signup/identity_verification/a/h;

    return-object v0
.end method

.method private final a(Lco/uk/getmondo/signup/identity_verification/a/a/a;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lio/reactivex/b;
    .locals 7

    .prologue
    .line 82
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/a/k;->c:Lco/uk/getmondo/api/IdentityVerificationApi;

    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "UUID.randomUUID().toString()"

    invoke-static {v1, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v0, v1, p3}, Lco/uk/getmondo/api/IdentityVerificationApi;->createKYCUploadUrl(Ljava/lang/String;Ljava/lang/String;)Lio/reactivex/v;

    move-result-object v6

    .line 83
    new-instance v0, Lco/uk/getmondo/signup/identity_verification/a/k$h;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p3

    move-object v4, p2

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lco/uk/getmondo/signup/identity_verification/a/k$h;-><init>(Lco/uk/getmondo/signup/identity_verification/a/k;Lco/uk/getmondo/signup/identity_verification/a/a/a;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {v6, v0}, Lio/reactivex/v;->c(Lio/reactivex/c/h;)Lio/reactivex/b;

    move-result-object v0

    const-string v1, "identityVerificationApi.\u2026eType))\n                }"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public static final synthetic a(Lco/uk/getmondo/signup/identity_verification/a/k;Lco/uk/getmondo/signup/identity_verification/a/a/a;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lio/reactivex/b;
    .locals 1

    .prologue
    .line 17
    invoke-direct {p0, p1, p2, p3, p4}, Lco/uk/getmondo/signup/identity_verification/a/k;->a(Lco/uk/getmondo/signup/identity_verification/a/a/a;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lio/reactivex/b;

    move-result-object v0

    return-object v0
.end method

.method private final a(Ljava/lang/String;)Lio/reactivex/v;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/v",
            "<",
            "Lco/uk/getmondo/signup/identity_verification/a/a/a;",
            ">;"
        }
    .end annotation

    .prologue
    .line 90
    new-instance v0, Lco/uk/getmondo/signup/identity_verification/a/k$b;

    invoke-direct {v0, p1}, Lco/uk/getmondo/signup/identity_verification/a/k$b;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/util/concurrent/Callable;

    invoke-static {v0}, Lio/reactivex/v;->c(Ljava/util/concurrent/Callable;)Lio/reactivex/v;

    move-result-object v0

    const-string v1, "Single.fromCallable { Fi\u2026er.fromFile(File(path)) }"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public static final synthetic b(Lco/uk/getmondo/signup/identity_verification/a/k;)Lco/uk/getmondo/common/i;
    .locals 1

    .prologue
    .line 17
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/a/k;->e:Lco/uk/getmondo/common/i;

    return-object v0
.end method

.method public static final synthetic c(Lco/uk/getmondo/signup/identity_verification/a/k;)Lco/uk/getmondo/api/ae;
    .locals 1

    .prologue
    .line 17
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/a/k;->d:Lco/uk/getmondo/api/ae;

    return-object v0
.end method

.method public static final synthetic d(Lco/uk/getmondo/signup/identity_verification/a/k;)Lco/uk/getmondo/api/IdentityVerificationApi;
    .locals 1

    .prologue
    .line 17
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/a/k;->c:Lco/uk/getmondo/api/IdentityVerificationApi;

    return-object v0
.end method

.method private final e()Lio/reactivex/b;
    .locals 5

    .prologue
    .line 55
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/a/k;->b:Lco/uk/getmondo/signup/identity_verification/a/h;

    invoke-virtual {v0}, Lco/uk/getmondo/signup/identity_verification/a/h;->a()Lco/uk/getmondo/signup/identity_verification/a/a/b;

    move-result-object v1

    .line 56
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/a/k;->b:Lco/uk/getmondo/signup/identity_verification/a/h;

    invoke-virtual {v0}, Lco/uk/getmondo/signup/identity_verification/a/h;->c()Ljava/lang/String;

    move-result-object v2

    .line 57
    if-eqz v1, :cond_0

    if-nez v2, :cond_1

    .line 58
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Cannot submit evidence because identity doc or video "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "path are missing | idEvidence: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 59
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " videoPath "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 58
    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    invoke-static {v0}, Lio/reactivex/b;->a(Ljava/lang/Throwable;)Lio/reactivex/b;

    move-result-object v0

    const-string v1, "Completable.error(Illega\u2026e videoPath $videoPath\"))"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 74
    :goto_0
    return-object v0

    .line 62
    :cond_1
    invoke-virtual {v1}, Lco/uk/getmondo/signup/identity_verification/a/a/b;->a()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lco/uk/getmondo/signup/identity_verification/a/k;->a(Ljava/lang/String;)Lio/reactivex/v;

    move-result-object v3

    .line 63
    invoke-direct {p0, v2}, Lco/uk/getmondo/signup/identity_verification/a/k;->a(Ljava/lang/String;)Lio/reactivex/v;

    move-result-object v2

    .line 64
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v4

    .line 66
    new-instance v0, Lco/uk/getmondo/signup/identity_verification/a/k$f;

    invoke-direct {v0, p0, v4, v1}, Lco/uk/getmondo/signup/identity_verification/a/k$f;-><init>(Lco/uk/getmondo/signup/identity_verification/a/k;Ljava/lang/String;Lco/uk/getmondo/signup/identity_verification/a/a/b;)V

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {v3, v0}, Lio/reactivex/v;->c(Lio/reactivex/c/h;)Lio/reactivex/b;

    move-result-object v1

    .line 69
    new-instance v0, Lco/uk/getmondo/signup/identity_verification/a/k$g;

    invoke-direct {v0, p0, v4}, Lco/uk/getmondo/signup/identity_verification/a/k$g;-><init>(Lco/uk/getmondo/signup/identity_verification/a/k;Ljava/lang/String;)V

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {v2, v0}, Lio/reactivex/v;->c(Lio/reactivex/c/h;)Lio/reactivex/b;

    move-result-object v0

    .line 73
    check-cast v0, Lio/reactivex/d;

    invoke-virtual {v1, v0}, Lio/reactivex/b;->d(Lio/reactivex/d;)Lio/reactivex/b;

    move-result-object v1

    .line 74
    new-instance v0, Lco/uk/getmondo/signup/identity_verification/a/k$e;

    invoke-direct {v0, p0, v4}, Lco/uk/getmondo/signup/identity_verification/a/k$e;-><init>(Lco/uk/getmondo/signup/identity_verification/a/k;Ljava/lang/String;)V

    check-cast v0, Lio/reactivex/c/a;

    invoke-virtual {v1, v0}, Lio/reactivex/b;->b(Lio/reactivex/c/a;)Lio/reactivex/b;

    move-result-object v0

    const-string v1, "photoUpload.mergeWith(vi\u2026ge.saveGroupId(groupId) }"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method public a()Lio/reactivex/v;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/v",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .prologue
    .line 30
    invoke-direct {p0}, Lco/uk/getmondo/signup/identity_verification/a/k;->e()Lio/reactivex/b;

    move-result-object v0

    .line 31
    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/b;->a(Ljava/lang/Object;)Lio/reactivex/v;

    move-result-object v1

    .line 32
    new-instance v0, Lco/uk/getmondo/signup/identity_verification/a/k$d;

    invoke-direct {v0, p0}, Lco/uk/getmondo/signup/identity_verification/a/k$d;-><init>(Lco/uk/getmondo/signup/identity_verification/a/k;)V

    check-cast v0, Lio/reactivex/c/g;

    invoke-virtual {v1, v0}, Lio/reactivex/v;->c(Lio/reactivex/c/g;)Lio/reactivex/v;

    move-result-object v0

    const-string v1, "uploadDocuments()\n      \u2026tFlag()\n                }"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public a(Lco/uk/getmondo/signup/identity_verification/a/a/b;Z)V
    .locals 2

    .prologue
    const-string v0, "evidence"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 47
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/a/k;->b:Lco/uk/getmondo/signup/identity_verification/a/h;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Lco/uk/getmondo/signup/identity_verification/a/h;->a(Lco/uk/getmondo/signup/identity_verification/a/a/b;Z)V

    .line 48
    return-void
.end method

.method public a(Ljava/lang/String;Z)V
    .locals 2

    .prologue
    const-string v0, "videoPath"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 51
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/a/k;->b:Lco/uk/getmondo/signup/identity_verification/a/h;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Lco/uk/getmondo/signup/identity_verification/a/h;->a(Ljava/lang/String;Z)V

    .line 52
    return-void
.end method

.method public b()Lio/reactivex/n;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Lcom/c/b/b",
            "<",
            "Lco/uk/getmondo/signup/identity_verification/a/a/b;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 39
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/a/k;->b:Lco/uk/getmondo/signup/identity_verification/a/h;

    invoke-virtual {v0}, Lco/uk/getmondo/signup/identity_verification/a/h;->f()Lio/reactivex/n;

    move-result-object v0

    return-object v0
.end method

.method public c()Lio/reactivex/n;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Lcom/c/b/b",
            "<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 43
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/a/k;->b:Lco/uk/getmondo/signup/identity_verification/a/h;

    invoke-virtual {v0}, Lco/uk/getmondo/signup/identity_verification/a/h;->g()Lio/reactivex/n;

    move-result-object v0

    return-object v0
.end method

.method public d()Lio/reactivex/v;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/v",
            "<",
            "Lco/uk/getmondo/api/model/identity_verification/IdentityVerification;",
            ">;"
        }
    .end annotation

    .prologue
    .line 25
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/a/k;->c:Lco/uk/getmondo/api/IdentityVerificationApi;

    invoke-interface {v0}, Lco/uk/getmondo/api/IdentityVerificationApi;->kycStatus()Lio/reactivex/v;

    move-result-object v1

    .line 26
    sget-object v0, Lco/uk/getmondo/signup/identity_verification/a/k$c;->a:Lco/uk/getmondo/signup/identity_verification/a/k$c;

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {v1, v0}, Lio/reactivex/v;->d(Lio/reactivex/c/h;)Lio/reactivex/v;

    move-result-object v0

    const-string v1, "identityVerificationApi.\u2026oIdentityVerification() }"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method
