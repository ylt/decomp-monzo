.class final Lco/uk/getmondo/signup/identity_verification/a/l$f;
.super Ljava/lang/Object;
.source "NewIdentityVerificationManager.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lco/uk/getmondo/signup/identity_verification/a/l;->e()Lio/reactivex/b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h",
        "<",
        "Ljava/lang/String;",
        "Lio/reactivex/d;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n\u00a2\u0006\u0002\u0008\u0004"
    }
    d2 = {
        "<anonymous>",
        "Lio/reactivex/Completable;",
        "it",
        "",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field final synthetic a:Lco/uk/getmondo/signup/identity_verification/a/l;


# direct methods
.method constructor <init>(Lco/uk/getmondo/signup/identity_verification/a/l;)V
    .locals 0

    iput-object p1, p0, Lco/uk/getmondo/signup/identity_verification/a/l$f;->a:Lco/uk/getmondo/signup/identity_verification/a/l;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Lio/reactivex/b;
    .locals 3

    .prologue
    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 77
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/a/l$f;->a:Lco/uk/getmondo/signup/identity_verification/a/l;

    invoke-static {v0}, Lco/uk/getmondo/signup/identity_verification/a/l;->c(Lco/uk/getmondo/signup/identity_verification/a/l;)Lco/uk/getmondo/api/IdentityVerificationApi;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/signup/identity_verification/a/l$f;->a:Lco/uk/getmondo/signup/identity_verification/a/l;

    invoke-static {v1}, Lco/uk/getmondo/signup/identity_verification/a/l;->d(Lco/uk/getmondo/signup/identity_verification/a/l;)Lco/uk/getmondo/api/model/signup/SignupSource;

    move-result-object v1

    iget-object v2, p0, Lco/uk/getmondo/signup/identity_verification/a/l$f;->a:Lco/uk/getmondo/signup/identity_verification/a/l;

    invoke-static {v2}, Lco/uk/getmondo/signup/identity_verification/a/l;->a(Lco/uk/getmondo/signup/identity_verification/a/l;)Lco/uk/getmondo/signup/identity_verification/a/h;

    move-result-object v2

    invoke-virtual {v2}, Lco/uk/getmondo/signup/identity_verification/a/h;->d()Z

    move-result v2

    invoke-interface {v0, v1, p1, v2}, Lco/uk/getmondo/api/IdentityVerificationApi;->registerSelfieVideo(Lco/uk/getmondo/api/model/signup/SignupSource;Ljava/lang/String;Z)Lio/reactivex/b;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 17
    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1}, Lco/uk/getmondo/signup/identity_verification/a/l$f;->a(Ljava/lang/String;)Lio/reactivex/b;

    move-result-object v0

    return-object v0
.end method
