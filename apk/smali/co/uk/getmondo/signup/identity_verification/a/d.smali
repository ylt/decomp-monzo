.class public final enum Lco/uk/getmondo/signup/identity_verification/a/d;
.super Ljava/lang/Enum;
.source "FileValidator_Factory.java"

# interfaces
.implements Lb/a/b;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lco/uk/getmondo/signup/identity_verification/a/d;",
        ">;",
        "Lb/a/b",
        "<",
        "Lco/uk/getmondo/signup/identity_verification/a/c;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lco/uk/getmondo/signup/identity_verification/a/d;

.field private static final synthetic b:[Lco/uk/getmondo/signup/identity_verification/a/d;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 11
    new-instance v0, Lco/uk/getmondo/signup/identity_verification/a/d;

    const-string v1, "INSTANCE"

    invoke-direct {v0, v1, v2}, Lco/uk/getmondo/signup/identity_verification/a/d;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lco/uk/getmondo/signup/identity_verification/a/d;->a:Lco/uk/getmondo/signup/identity_verification/a/d;

    .line 6
    const/4 v0, 0x1

    new-array v0, v0, [Lco/uk/getmondo/signup/identity_verification/a/d;

    sget-object v1, Lco/uk/getmondo/signup/identity_verification/a/d;->a:Lco/uk/getmondo/signup/identity_verification/a/d;

    aput-object v1, v0, v2

    sput-object v0, Lco/uk/getmondo/signup/identity_verification/a/d;->b:[Lco/uk/getmondo/signup/identity_verification/a/d;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 10
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static c()Lb/a/b;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lb/a/b",
            "<",
            "Lco/uk/getmondo/signup/identity_verification/a/c;",
            ">;"
        }
    .end annotation

    .prologue
    .line 19
    sget-object v0, Lco/uk/getmondo/signup/identity_verification/a/d;->a:Lco/uk/getmondo/signup/identity_verification/a/d;

    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)Lco/uk/getmondo/signup/identity_verification/a/d;
    .locals 1

    .prologue
    .line 6
    const-class v0, Lco/uk/getmondo/signup/identity_verification/a/d;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/signup/identity_verification/a/d;

    return-object v0
.end method

.method public static values()[Lco/uk/getmondo/signup/identity_verification/a/d;
    .locals 1

    .prologue
    .line 6
    sget-object v0, Lco/uk/getmondo/signup/identity_verification/a/d;->b:[Lco/uk/getmondo/signup/identity_verification/a/d;

    invoke-virtual {v0}, [Lco/uk/getmondo/signup/identity_verification/a/d;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lco/uk/getmondo/signup/identity_verification/a/d;

    return-object v0
.end method


# virtual methods
.method public a()Lco/uk/getmondo/signup/identity_verification/a/c;
    .locals 1

    .prologue
    .line 15
    new-instance v0, Lco/uk/getmondo/signup/identity_verification/a/c;

    invoke-direct {v0}, Lco/uk/getmondo/signup/identity_verification/a/c;-><init>()V

    return-object v0
.end method

.method public synthetic b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 6
    invoke-virtual {p0}, Lco/uk/getmondo/signup/identity_verification/a/d;->a()Lco/uk/getmondo/signup/identity_verification/a/c;

    move-result-object v0

    return-object v0
.end method
