.class public final Lco/uk/getmondo/signup/identity_verification/a/a/a;
.super Ljava/lang/Object;
.source "FileWrapper.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lco/uk/getmondo/signup/identity_verification/a/a/a$a;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u00004\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\t\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u000f\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0002\u0008\u0003\u0008\u0086\u0008\u0018\u0000 \u001e2\u00020\u0001:\u0001\u001eB%\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u00a2\u0006\u0002\u0010\nJ\t\u0010\u0013\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0014\u001a\u00020\u0005H\u00c6\u0003J\t\u0010\u0015\u001a\u00020\u0007H\u00c6\u0003J\t\u0010\u0016\u001a\u00020\tH\u00c6\u0003J1\u0010\u0017\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u00052\u0008\u0008\u0002\u0010\u0006\u001a\u00020\u00072\u0008\u0008\u0002\u0010\u0008\u001a\u00020\tH\u00c6\u0001J\u0013\u0010\u0018\u001a\u00020\u00192\u0008\u0010\u001a\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u0010\u001b\u001a\u00020\u001cH\u00d6\u0001J\t\u0010\u001d\u001a\u00020\u0005H\u00d6\u0001R\u0011\u0010\u0008\u001a\u00020\t\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000b\u0010\u000cR\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\r\u0010\u000eR\u0011\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000f\u0010\u0010R\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0011\u0010\u0012\u00a8\u0006\u001f"
    }
    d2 = {
        "Lco/uk/getmondo/signup/identity_verification/data/model/FileWrapper;",
        "",
        "inputStream",
        "Ljava/io/FileInputStream;",
        "name",
        "",
        "length",
        "",
        "contentType",
        "Lco/uk/getmondo/api/model/identity_verification/ContentType;",
        "(Ljava/io/FileInputStream;Ljava/lang/String;JLco/uk/getmondo/api/model/identity_verification/ContentType;)V",
        "getContentType",
        "()Lco/uk/getmondo/api/model/identity_verification/ContentType;",
        "getInputStream",
        "()Ljava/io/FileInputStream;",
        "getLength",
        "()J",
        "getName",
        "()Ljava/lang/String;",
        "component1",
        "component2",
        "component3",
        "component4",
        "copy",
        "equals",
        "",
        "other",
        "hashCode",
        "",
        "toString",
        "Create",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# static fields
.field public static final a:Lco/uk/getmondo/signup/identity_verification/a/a/a$a;


# instance fields
.field private final b:Ljava/io/FileInputStream;

.field private final c:Ljava/lang/String;

.field private final d:J

.field private final e:Lco/uk/getmondo/api/model/identity_verification/ContentType;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lco/uk/getmondo/signup/identity_verification/a/a/a$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lco/uk/getmondo/signup/identity_verification/a/a/a$a;-><init>(Lkotlin/d/b/i;)V

    sput-object v0, Lco/uk/getmondo/signup/identity_verification/a/a/a;->a:Lco/uk/getmondo/signup/identity_verification/a/a/a$a;

    return-void
.end method

.method public constructor <init>(Ljava/io/FileInputStream;Ljava/lang/String;JLco/uk/getmondo/api/model/identity_verification/ContentType;)V
    .locals 1

    .prologue
    const-string v0, "inputStream"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "name"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "contentType"

    invoke-static {p5, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lco/uk/getmondo/signup/identity_verification/a/a/a;->b:Ljava/io/FileInputStream;

    iput-object p2, p0, Lco/uk/getmondo/signup/identity_verification/a/a/a;->c:Ljava/lang/String;

    iput-wide p3, p0, Lco/uk/getmondo/signup/identity_verification/a/a/a;->d:J

    iput-object p5, p0, Lco/uk/getmondo/signup/identity_verification/a/a/a;->e:Lco/uk/getmondo/api/model/identity_verification/ContentType;

    return-void
.end method


# virtual methods
.method public final a()Ljava/io/FileInputStream;
    .locals 1

    .prologue
    .line 9
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/a/a/a;->b:Ljava/io/FileInputStream;

    return-object v0
.end method

.method public final b()J
    .locals 2

    .prologue
    .line 11
    iget-wide v0, p0, Lco/uk/getmondo/signup/identity_verification/a/a/a;->d:J

    return-wide v0
.end method

.method public final c()Ljava/io/FileInputStream;
    .locals 1

    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/a/a/a;->b:Ljava/io/FileInputStream;

    return-object v0
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/a/a/a;->c:Ljava/lang/String;

    return-object v0
.end method

.method public final e()J
    .locals 2

    iget-wide v0, p0, Lco/uk/getmondo/signup/identity_verification/a/a/a;->d:J

    return-wide v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 6

    const/4 v1, 0x1

    const/4 v0, 0x0

    if-eq p0, p1, :cond_0

    instance-of v2, p1, Lco/uk/getmondo/signup/identity_verification/a/a/a;

    if-eqz v2, :cond_1

    check-cast p1, Lco/uk/getmondo/signup/identity_verification/a/a/a;

    iget-object v2, p0, Lco/uk/getmondo/signup/identity_verification/a/a/a;->b:Ljava/io/FileInputStream;

    iget-object v3, p1, Lco/uk/getmondo/signup/identity_verification/a/a/a;->b:Ljava/io/FileInputStream;

    invoke-static {v2, v3}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lco/uk/getmondo/signup/identity_verification/a/a/a;->c:Ljava/lang/String;

    iget-object v3, p1, Lco/uk/getmondo/signup/identity_verification/a/a/a;->c:Ljava/lang/String;

    invoke-static {v2, v3}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    iget-wide v2, p0, Lco/uk/getmondo/signup/identity_verification/a/a/a;->d:J

    iget-wide v4, p1, Lco/uk/getmondo/signup/identity_verification/a/a/a;->d:J

    cmp-long v2, v2, v4

    if-nez v2, :cond_2

    move v2, v1

    :goto_0
    if-eqz v2, :cond_1

    iget-object v2, p0, Lco/uk/getmondo/signup/identity_verification/a/a/a;->e:Lco/uk/getmondo/api/model/identity_verification/ContentType;

    iget-object v3, p1, Lco/uk/getmondo/signup/identity_verification/a/a/a;->e:Lco/uk/getmondo/api/model/identity_verification/ContentType;

    invoke-static {v2, v3}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    move v0, v1

    :cond_1
    return v0

    :cond_2
    move v2, v0

    goto :goto_0
.end method

.method public final f()Lco/uk/getmondo/api/model/identity_verification/ContentType;
    .locals 1

    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/a/a/a;->e:Lco/uk/getmondo/api/model/identity_verification/ContentType;

    return-object v0
.end method

.method public hashCode()I
    .locals 6

    const/4 v1, 0x0

    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/a/a/a;->b:Ljava/io/FileInputStream;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    :goto_0
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/a/a/a;->c:Ljava/lang/String;

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-wide v2, p0, Lco/uk/getmondo/signup/identity_verification/a/a/a;->d:J

    const/16 v4, 0x20

    ushr-long v4, v2, v4

    xor-long/2addr v2, v4

    long-to-int v2, v2

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lco/uk/getmondo/signup/identity_verification/a/a/a;->e:Lco/uk/getmondo/api/model/identity_verification/ContentType;

    if-eqz v2, :cond_0

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_0
    add-int/2addr v0, v1

    return v0

    :cond_1
    move v0, v1

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_1
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "FileWrapper(inputStream="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/signup/identity_verification/a/a/a;->b:Ljava/io/FileInputStream;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", name="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/signup/identity_verification/a/a/a;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", length="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lco/uk/getmondo/signup/identity_verification/a/a/a;->d:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", contentType="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/signup/identity_verification/a/a/a;->e:Lco/uk/getmondo/api/model/identity_verification/ContentType;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
