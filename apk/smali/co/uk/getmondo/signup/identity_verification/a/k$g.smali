.class final Lco/uk/getmondo/signup/identity_verification/a/k$g;
.super Ljava/lang/Object;
.source "LegacyIdentityVerificationManager.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lco/uk/getmondo/signup/identity_verification/a/k;->e()Lio/reactivex/b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h",
        "<",
        "Lco/uk/getmondo/signup/identity_verification/a/a/a;",
        "Lio/reactivex/d;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n\u00a2\u0006\u0002\u0008\u0004"
    }
    d2 = {
        "<anonymous>",
        "Lio/reactivex/Completable;",
        "fileWrapper",
        "Lco/uk/getmondo/signup/identity_verification/data/model/FileWrapper;",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field final synthetic a:Lco/uk/getmondo/signup/identity_verification/a/k;

.field final synthetic b:Ljava/lang/String;


# direct methods
.method constructor <init>(Lco/uk/getmondo/signup/identity_verification/a/k;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lco/uk/getmondo/signup/identity_verification/a/k$g;->a:Lco/uk/getmondo/signup/identity_verification/a/k;

    iput-object p2, p0, Lco/uk/getmondo/signup/identity_verification/a/k$g;->b:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lco/uk/getmondo/signup/identity_verification/a/a/a;)Lio/reactivex/b;
    .locals 4

    .prologue
    const-string v0, "fileWrapper"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 70
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/a/k$g;->a:Lco/uk/getmondo/signup/identity_verification/a/k;

    iget-object v1, p0, Lco/uk/getmondo/signup/identity_verification/a/k$g;->b:Ljava/lang/String;

    const-string v2, "groupId"

    invoke-static {v1, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v2, "video/mp4"

    const-string v3, "kyc_video_face"

    invoke-static {v0, p1, v1, v2, v3}, Lco/uk/getmondo/signup/identity_verification/a/k;->a(Lco/uk/getmondo/signup/identity_verification/a/k;Lco/uk/getmondo/signup/identity_verification/a/a/a;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lio/reactivex/b;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 17
    check-cast p1, Lco/uk/getmondo/signup/identity_verification/a/a/a;

    invoke-virtual {p0, p1}, Lco/uk/getmondo/signup/identity_verification/a/k$g;->a(Lco/uk/getmondo/signup/identity_verification/a/a/a;)Lio/reactivex/b;

    move-result-object v0

    return-object v0
.end method
