.class public interface abstract Lco/uk/getmondo/signup/identity_verification/a/e;
.super Ljava/lang/Object;
.source "IdentityVerificationManager.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u00008\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0004\n\u0002\u0010\u000e\n\u0002\u0008\u0002\u0008f\u0018\u00002\u00020\u0001J\u0014\u0010\u0002\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00050\u00040\u0003H&J\u000e\u0010\u0006\u001a\u0008\u0012\u0004\u0012\u00020\u00080\u0007H&J\u000e\u0010\t\u001a\u0008\u0012\u0004\u0012\u00020\n0\u0007H&J\u0018\u0010\u000b\u001a\u00020\u000c2\u0006\u0010\r\u001a\u00020\u00052\u0006\u0010\u000e\u001a\u00020\nH&J\u0018\u0010\u000f\u001a\u00020\u000c2\u0006\u0010\u0010\u001a\u00020\u00112\u0006\u0010\u000e\u001a\u00020\nH&J\u0014\u0010\u0012\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00110\u00040\u0003H&\u00a8\u0006\u0013"
    }
    d2 = {
        "Lco/uk/getmondo/signup/identity_verification/data/IdentityVerificationManager;",
        "",
        "identityDocument",
        "Lio/reactivex/Observable;",
        "Lcom/memoizrlabs/poweroptional/Optional;",
        "Lco/uk/getmondo/signup/identity_verification/data/model/IdentityDocument;",
        "status",
        "Lio/reactivex/Single;",
        "Lco/uk/getmondo/api/model/identity_verification/IdentityVerification;",
        "submitEvidence",
        "",
        "useIdentityDocument",
        "",
        "evidence",
        "usedSystemCamera",
        "useVideoSelfie",
        "videoPath",
        "",
        "videoSelfiePath",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# virtual methods
.method public abstract a()Lio/reactivex/v;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/v",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end method

.method public abstract a(Lco/uk/getmondo/signup/identity_verification/a/a/b;Z)V
.end method

.method public abstract a(Ljava/lang/String;Z)V
.end method

.method public abstract b()Lio/reactivex/n;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Lcom/c/b/b",
            "<",
            "Lco/uk/getmondo/signup/identity_verification/a/a/b;",
            ">;>;"
        }
    .end annotation
.end method

.method public abstract c()Lio/reactivex/n;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Lcom/c/b/b",
            "<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end method

.method public abstract d()Lio/reactivex/v;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/v",
            "<",
            "Lco/uk/getmondo/api/model/identity_verification/IdentityVerification;",
            ">;"
        }
    .end annotation
.end method
