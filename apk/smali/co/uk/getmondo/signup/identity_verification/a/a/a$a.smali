.class public final Lco/uk/getmondo/signup/identity_verification/a/a/a$a;
.super Ljava/lang/Object;
.source "FileWrapper.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lco/uk/getmondo/signup/identity_verification/a/a/a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0008\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u0010\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006H\u0007\u00a8\u0006\u0007"
    }
    d2 = {
        "Lco/uk/getmondo/signup/identity_verification/data/model/FileWrapper$Create;",
        "",
        "()V",
        "fromFile",
        "Lco/uk/getmondo/signup/identity_verification/data/model/FileWrapper;",
        "file",
        "Ljava/io/File;",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/d/b/i;)V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Lco/uk/getmondo/signup/identity_verification/a/a/a$a;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/io/File;)Lco/uk/getmondo/signup/identity_verification/a/a/a;
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;
        }
    .end annotation

    .prologue
    const-string v0, "file"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 19
    new-instance v1, Lco/uk/getmondo/signup/identity_verification/a/a/a;

    new-instance v2, Ljava/io/FileInputStream;

    invoke-direct {v2, p1}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    invoke-virtual {p1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v3

    const-string v0, "file.name"

    invoke-static {v3, v0}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/io/File;->length()J

    move-result-wide v4

    .line 20
    sget-object v0, Lco/uk/getmondo/api/model/identity_verification/ContentType;->Companion:Lco/uk/getmondo/api/model/identity_verification/ContentType$Companion;

    invoke-static {p1}, Lkotlin/io/b;->a(Ljava/io/File;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v6}, Lco/uk/getmondo/api/model/identity_verification/ContentType$Companion;->a(Ljava/lang/String;)Lco/uk/getmondo/api/model/identity_verification/ContentType;

    move-result-object v6

    .line 19
    invoke-direct/range {v1 .. v6}, Lco/uk/getmondo/signup/identity_verification/a/a/a;-><init>(Ljava/io/FileInputStream;Ljava/lang/String;JLco/uk/getmondo/api/model/identity_verification/ContentType;)V

    return-object v1
.end method
