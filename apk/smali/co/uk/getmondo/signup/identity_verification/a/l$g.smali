.class final Lco/uk/getmondo/signup/identity_verification/a/l$g;
.super Ljava/lang/Object;
.source "NewIdentityVerificationManager.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lco/uk/getmondo/signup/identity_verification/a/l;->a(Ljava/lang/String;Lco/uk/getmondo/api/model/identity_verification/FileType;)Lio/reactivex/v;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h",
        "<TT;",
        "Lio/reactivex/z",
        "<+TR;>;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000\u0014\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a&\u0012\u000c\u0012\n \u0003*\u0004\u0018\u00010\u00020\u0002 \u0003*\u0012\u0012\u000c\u0012\n \u0003*\u0004\u0018\u00010\u00020\u0002\u0018\u00010\u00010\u00012\u0006\u0010\u0004\u001a\u00020\u0005H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "Lio/reactivex/Single;",
        "",
        "kotlin.jvm.PlatformType",
        "<name for destructuring parameter 0>",
        "Lco/uk/getmondo/signup/identity_verification/data/model/FileWrapper;",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field final synthetic a:Lco/uk/getmondo/signup/identity_verification/a/l;

.field final synthetic b:Lco/uk/getmondo/api/model/identity_verification/FileType;


# direct methods
.method constructor <init>(Lco/uk/getmondo/signup/identity_verification/a/l;Lco/uk/getmondo/api/model/identity_verification/FileType;)V
    .locals 0

    iput-object p1, p0, Lco/uk/getmondo/signup/identity_verification/a/l$g;->a:Lco/uk/getmondo/signup/identity_verification/a/l;

    iput-object p2, p0, Lco/uk/getmondo/signup/identity_verification/a/l$g;->b:Lco/uk/getmondo/api/model/identity_verification/FileType;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lco/uk/getmondo/signup/identity_verification/a/a/a;)Lio/reactivex/v;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lco/uk/getmondo/signup/identity_verification/a/a/a;",
            ")",
            "Lio/reactivex/v",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    const-string v0, "<name for destructuring parameter 0>"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lco/uk/getmondo/signup/identity_verification/a/a/a;->c()Ljava/io/FileInputStream;

    move-result-object v8

    invoke-virtual {p1}, Lco/uk/getmondo/signup/identity_verification/a/a/a;->d()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1}, Lco/uk/getmondo/signup/identity_verification/a/a/a;->e()J

    move-result-wide v6

    invoke-virtual {p1}, Lco/uk/getmondo/signup/identity_verification/a/a/a;->f()Lco/uk/getmondo/api/model/identity_verification/ContentType;

    move-result-object v5

    .line 85
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/a/l$g;->a:Lco/uk/getmondo/signup/identity_verification/a/l;

    invoke-static {v0}, Lco/uk/getmondo/signup/identity_verification/a/l;->c(Lco/uk/getmondo/signup/identity_verification/a/l;)Lco/uk/getmondo/api/IdentityVerificationApi;

    move-result-object v1

    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/a/l$g;->a:Lco/uk/getmondo/signup/identity_verification/a/l;

    invoke-static {v0}, Lco/uk/getmondo/signup/identity_verification/a/l;->d(Lco/uk/getmondo/signup/identity_verification/a/l;)Lco/uk/getmondo/api/model/signup/SignupSource;

    move-result-object v2

    iget-object v3, p0, Lco/uk/getmondo/signup/identity_verification/a/l$g;->b:Lco/uk/getmondo/api/model/identity_verification/FileType;

    invoke-interface/range {v1 .. v7}, Lco/uk/getmondo/api/IdentityVerificationApi;->requestFileUpload(Lco/uk/getmondo/api/model/signup/SignupSource;Lco/uk/getmondo/api/model/identity_verification/FileType;Ljava/lang/String;Lco/uk/getmondo/api/model/identity_verification/ContentType;J)Lio/reactivex/v;

    move-result-object v9

    .line 86
    new-instance v0, Lco/uk/getmondo/signup/identity_verification/a/l$g$1;

    move-object v1, p0

    move-object v2, v8

    move-wide v3, v6

    invoke-direct/range {v0 .. v5}, Lco/uk/getmondo/signup/identity_verification/a/l$g$1;-><init>(Lco/uk/getmondo/signup/identity_verification/a/l$g;Ljava/io/FileInputStream;JLco/uk/getmondo/api/model/identity_verification/ContentType;)V

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {v9, v0}, Lio/reactivex/v;->a(Lio/reactivex/c/h;)Lio/reactivex/v;

    move-result-object v0

    .line 88
    return-object v0
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 17
    check-cast p1, Lco/uk/getmondo/signup/identity_verification/a/a/a;

    invoke-virtual {p0, p1}, Lco/uk/getmondo/signup/identity_verification/a/l$g;->a(Lco/uk/getmondo/signup/identity_verification/a/a/a;)Lio/reactivex/v;

    move-result-object v0

    return-object v0
.end method
