.class final Lco/uk/getmondo/signup/identity_verification/a/l$g$1;
.super Ljava/lang/Object;
.source "NewIdentityVerificationManager.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lco/uk/getmondo/signup/identity_verification/a/l$g;->a(Lco/uk/getmondo/signup/identity_verification/a/a/a;)Lio/reactivex/v;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h",
        "<TT;",
        "Lio/reactivex/z",
        "<+TR;>;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000\u0014\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a&\u0012\u000c\u0012\n \u0003*\u0004\u0018\u00010\u00020\u0002 \u0003*\u0012\u0012\u000c\u0012\n \u0003*\u0004\u0018\u00010\u00020\u0002\u0018\u00010\u00010\u00012\u0006\u0010\u0004\u001a\u00020\u0005H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "Lio/reactivex/Single;",
        "",
        "kotlin.jvm.PlatformType",
        "<name for destructuring parameter 0>",
        "Lco/uk/getmondo/api/model/identity_verification/FileUpload;",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field final synthetic a:Lco/uk/getmondo/signup/identity_verification/a/l$g;

.field final synthetic b:Ljava/io/FileInputStream;

.field final synthetic c:J

.field final synthetic d:Lco/uk/getmondo/api/model/identity_verification/ContentType;


# direct methods
.method constructor <init>(Lco/uk/getmondo/signup/identity_verification/a/l$g;Ljava/io/FileInputStream;JLco/uk/getmondo/api/model/identity_verification/ContentType;)V
    .locals 1

    iput-object p1, p0, Lco/uk/getmondo/signup/identity_verification/a/l$g$1;->a:Lco/uk/getmondo/signup/identity_verification/a/l$g;

    iput-object p2, p0, Lco/uk/getmondo/signup/identity_verification/a/l$g$1;->b:Ljava/io/FileInputStream;

    iput-wide p3, p0, Lco/uk/getmondo/signup/identity_verification/a/l$g$1;->c:J

    iput-object p5, p0, Lco/uk/getmondo/signup/identity_verification/a/l$g$1;->d:Lco/uk/getmondo/api/model/identity_verification/ContentType;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lco/uk/getmondo/api/model/identity_verification/FileUpload;)Lio/reactivex/v;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lco/uk/getmondo/api/model/identity_verification/FileUpload;",
            ")",
            "Lio/reactivex/v",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    const-string v0, "<name for destructuring parameter 0>"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lco/uk/getmondo/api/model/identity_verification/FileUpload;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lco/uk/getmondo/api/model/identity_verification/FileUpload;->b()Ljava/lang/String;

    move-result-object v2

    .line 87
    iget-object v1, p0, Lco/uk/getmondo/signup/identity_verification/a/l$g$1;->a:Lco/uk/getmondo/signup/identity_verification/a/l$g;

    iget-object v1, v1, Lco/uk/getmondo/signup/identity_verification/a/l$g;->a:Lco/uk/getmondo/signup/identity_verification/a/l;

    invoke-static {v1}, Lco/uk/getmondo/signup/identity_verification/a/l;->e(Lco/uk/getmondo/signup/identity_verification/a/l;)Lco/uk/getmondo/api/ae;

    move-result-object v1

    iget-object v3, p0, Lco/uk/getmondo/signup/identity_verification/a/l$g$1;->b:Ljava/io/FileInputStream;

    check-cast v3, Ljava/io/InputStream;

    iget-wide v4, p0, Lco/uk/getmondo/signup/identity_verification/a/l$g$1;->c:J

    iget-object v6, p0, Lco/uk/getmondo/signup/identity_verification/a/l$g$1;->d:Lco/uk/getmondo/api/model/identity_verification/ContentType;

    invoke-virtual {v6}, Lco/uk/getmondo/api/model/identity_verification/ContentType;->a()Ljava/lang/String;

    move-result-object v6

    invoke-virtual/range {v1 .. v6}, Lco/uk/getmondo/api/ae;->a(Ljava/lang/String;Ljava/io/InputStream;JLjava/lang/String;)Lio/reactivex/b;

    move-result-object v1

    invoke-virtual {v1, v0}, Lio/reactivex/b;->a(Ljava/lang/Object;)Lio/reactivex/v;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 17
    check-cast p1, Lco/uk/getmondo/api/model/identity_verification/FileUpload;

    invoke-virtual {p0, p1}, Lco/uk/getmondo/signup/identity_verification/a/l$g$1;->a(Lco/uk/getmondo/api/model/identity_verification/FileUpload;)Lio/reactivex/v;

    move-result-object v0

    return-object v0
.end method
