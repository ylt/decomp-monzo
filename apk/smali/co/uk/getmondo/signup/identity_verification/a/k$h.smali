.class final Lco/uk/getmondo/signup/identity_verification/a/k$h;
.super Ljava/lang/Object;
.source "LegacyIdentityVerificationManager.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lco/uk/getmondo/signup/identity_verification/a/k;->a(Lco/uk/getmondo/signup/identity_verification/a/a/a;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lio/reactivex/b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h",
        "<",
        "Lco/uk/getmondo/api/model/identity_verification/ApiUploadContainer;",
        "Lio/reactivex/d;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\n \u0002*\u0004\u0018\u00010\u00010\u00012\u0006\u0010\u0003\u001a\u00020\u0004H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "Lio/reactivex/Completable;",
        "kotlin.jvm.PlatformType",
        "upload",
        "Lco/uk/getmondo/api/model/identity_verification/ApiUploadContainer;",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field final synthetic a:Lco/uk/getmondo/signup/identity_verification/a/k;

.field final synthetic b:Lco/uk/getmondo/signup/identity_verification/a/a/a;

.field final synthetic c:Ljava/lang/String;

.field final synthetic d:Ljava/lang/String;

.field final synthetic e:Ljava/lang/String;


# direct methods
.method constructor <init>(Lco/uk/getmondo/signup/identity_verification/a/k;Lco/uk/getmondo/signup/identity_verification/a/a/a;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lco/uk/getmondo/signup/identity_verification/a/k$h;->a:Lco/uk/getmondo/signup/identity_verification/a/k;

    iput-object p2, p0, Lco/uk/getmondo/signup/identity_verification/a/k$h;->b:Lco/uk/getmondo/signup/identity_verification/a/a/a;

    iput-object p3, p0, Lco/uk/getmondo/signup/identity_verification/a/k$h;->c:Ljava/lang/String;

    iput-object p4, p0, Lco/uk/getmondo/signup/identity_verification/a/k$h;->d:Ljava/lang/String;

    iput-object p5, p0, Lco/uk/getmondo/signup/identity_verification/a/k$h;->e:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lco/uk/getmondo/api/model/identity_verification/ApiUploadContainer;)Lio/reactivex/b;
    .locals 7

    .prologue
    const-string v0, "upload"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 84
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/a/k$h;->a:Lco/uk/getmondo/signup/identity_verification/a/k;

    invoke-static {v0}, Lco/uk/getmondo/signup/identity_verification/a/k;->c(Lco/uk/getmondo/signup/identity_verification/a/k;)Lco/uk/getmondo/api/ae;

    move-result-object v1

    invoke-virtual {p1}, Lco/uk/getmondo/api/model/identity_verification/ApiUploadContainer;->b()Ljava/lang/String;

    move-result-object v2

    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/a/k$h;->b:Lco/uk/getmondo/signup/identity_verification/a/a/a;

    invoke-virtual {v0}, Lco/uk/getmondo/signup/identity_verification/a/a/a;->a()Ljava/io/FileInputStream;

    move-result-object v3

    check-cast v3, Ljava/io/InputStream;

    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/a/k$h;->b:Lco/uk/getmondo/signup/identity_verification/a/a/a;

    invoke-virtual {v0}, Lco/uk/getmondo/signup/identity_verification/a/a/a;->b()J

    move-result-wide v4

    iget-object v6, p0, Lco/uk/getmondo/signup/identity_verification/a/k$h;->c:Ljava/lang/String;

    invoke-virtual/range {v1 .. v6}, Lco/uk/getmondo/api/ae;->a(Ljava/lang/String;Ljava/io/InputStream;JLjava/lang/String;)Lio/reactivex/b;

    move-result-object v1

    .line 85
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/a/k$h;->a:Lco/uk/getmondo/signup/identity_verification/a/k;

    invoke-static {v0}, Lco/uk/getmondo/signup/identity_verification/a/k;->d(Lco/uk/getmondo/signup/identity_verification/a/k;)Lco/uk/getmondo/api/IdentityVerificationApi;

    move-result-object v0

    invoke-virtual {p1}, Lco/uk/getmondo/api/model/identity_verification/ApiUploadContainer;->a()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lco/uk/getmondo/signup/identity_verification/a/k$h;->d:Ljava/lang/String;

    iget-object v4, p0, Lco/uk/getmondo/signup/identity_verification/a/k$h;->e:Ljava/lang/String;

    invoke-interface {v0, v2, v3, v4}, Lco/uk/getmondo/api/IdentityVerificationApi;->registerKYCUrl(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lio/reactivex/b;

    move-result-object v0

    check-cast v0, Lio/reactivex/d;

    invoke-virtual {v1, v0}, Lio/reactivex/b;->b(Lio/reactivex/d;)Lio/reactivex/b;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 17
    check-cast p1, Lco/uk/getmondo/api/model/identity_verification/ApiUploadContainer;

    invoke-virtual {p0, p1}, Lco/uk/getmondo/signup/identity_verification/a/k$h;->a(Lco/uk/getmondo/api/model/identity_verification/ApiUploadContainer;)Lio/reactivex/b;

    move-result-object v0

    return-object v0
.end method
