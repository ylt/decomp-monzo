.class public final Lco/uk/getmondo/signup/identity_verification/a/a/b;
.super Ljava/lang/Object;
.source "IdentityDocument.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000<\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u000f\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\u0086\u0008\u0018\u00002\u00020\u0001B+\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0008\u0008\u0002\u0010\u0006\u001a\u00020\u0007\u0012\n\u0008\u0002\u0010\u0008\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\u0002\u0010\tJ\t\u0010\u0011\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0012\u001a\u00020\u0005H\u00c6\u0003J\t\u0010\u0013\u001a\u00020\u0007H\u00c6\u0003J\u000b\u0010\u0014\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003J3\u0010\u0015\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u00052\u0008\u0008\u0002\u0010\u0006\u001a\u00020\u00072\n\u0008\u0002\u0010\u0008\u001a\u0004\u0018\u00010\u0003H\u00c6\u0001J\u0013\u0010\u0016\u001a\u00020\u00172\u0008\u0010\u0018\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u0010\u0019\u001a\u00020\u001aH\u00d6\u0001J\u0016\u0010\u001b\u001a\u00020\u00172\u0006\u0010\u001c\u001a\u00020\u001d2\u0006\u0010\u001e\u001a\u00020\u001fJ\t\u0010 \u001a\u00020\u0003H\u00d6\u0001R\u0011\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\n\u0010\u000bR\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000c\u0010\rR\u0013\u0010\u0008\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000e\u0010\rR\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000f\u0010\u0010\u00a8\u0006!"
    }
    d2 = {
        "Lco/uk/getmondo/signup/identity_verification/data/model/IdentityDocument;",
        "",
        "primaryPhotoPath",
        "",
        "type",
        "Lco/uk/getmondo/api/model/identity_verification/IdentityDocumentType;",
        "country",
        "Lco/uk/getmondo/model/Country;",
        "secondaryPhotoPath",
        "(Ljava/lang/String;Lco/uk/getmondo/api/model/identity_verification/IdentityDocumentType;Lco/uk/getmondo/model/Country;Ljava/lang/String;)V",
        "getCountry",
        "()Lco/uk/getmondo/model/Country;",
        "getPrimaryPhotoPath",
        "()Ljava/lang/String;",
        "getSecondaryPhotoPath",
        "getType",
        "()Lco/uk/getmondo/api/model/identity_verification/IdentityDocumentType;",
        "component1",
        "component2",
        "component3",
        "component4",
        "copy",
        "equals",
        "",
        "other",
        "hashCode",
        "",
        "isValid",
        "fileValidator",
        "Lco/uk/getmondo/signup/identity_verification/data/FileValidator;",
        "version",
        "Lco/uk/getmondo/signup/identity_verification/data/IdentityVerificationVersion;",
        "toString",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Lco/uk/getmondo/api/model/identity_verification/IdentityDocumentType;

.field private final c:Lco/uk/getmondo/d/i;

.field private final d:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Lco/uk/getmondo/api/model/identity_verification/IdentityDocumentType;Lco/uk/getmondo/d/i;Ljava/lang/String;)V
    .locals 1

    .prologue
    const-string v0, "primaryPhotoPath"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "type"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "country"

    invoke-static {p3, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lco/uk/getmondo/signup/identity_verification/a/a/b;->a:Ljava/lang/String;

    iput-object p2, p0, Lco/uk/getmondo/signup/identity_verification/a/a/b;->b:Lco/uk/getmondo/api/model/identity_verification/IdentityDocumentType;

    iput-object p3, p0, Lco/uk/getmondo/signup/identity_verification/a/a/b;->c:Lco/uk/getmondo/d/i;

    iput-object p4, p0, Lco/uk/getmondo/signup/identity_verification/a/a/b;->d:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 9
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/a/a/b;->a:Ljava/lang/String;

    return-object v0
.end method

.method public final a(Lco/uk/getmondo/signup/identity_verification/a/c;Lco/uk/getmondo/signup/identity_verification/a/j;)Z
    .locals 2

    .prologue
    const-string v0, "fileValidator"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "version"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 16
    sget-object v0, Lco/uk/getmondo/signup/identity_verification/a/j;->a:Lco/uk/getmondo/signup/identity_verification/a/j;

    invoke-static {p2, v0}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/a/a/b;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lco/uk/getmondo/signup/identity_verification/a/c;->a(Ljava/lang/String;)Z

    move-result v0

    :goto_0
    return v0

    .line 18
    :cond_0
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/a/a/b;->b:Lco/uk/getmondo/api/model/identity_verification/IdentityDocumentType;

    sget-object v1, Lco/uk/getmondo/api/model/identity_verification/IdentityDocumentType;->PASSPORT:Lco/uk/getmondo/api/model/identity_verification/IdentityDocumentType;

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/a/a/b;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lco/uk/getmondo/signup/identity_verification/a/c;->a(Ljava/lang/String;)Z

    move-result v0

    goto :goto_0

    .line 19
    :cond_1
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/a/a/b;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lco/uk/getmondo/signup/identity_verification/a/c;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/a/a/b;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lco/uk/getmondo/signup/identity_verification/a/c;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()Lco/uk/getmondo/api/model/identity_verification/IdentityDocumentType;
    .locals 1

    .prologue
    .line 10
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/a/a/b;->b:Lco/uk/getmondo/api/model/identity_verification/IdentityDocumentType;

    return-object v0
.end method

.method public final c()Lco/uk/getmondo/d/i;
    .locals 1

    .prologue
    .line 11
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/a/a/b;->c:Lco/uk/getmondo/d/i;

    return-object v0
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 12
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/a/a/b;->d:Ljava/lang/String;

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_0

    instance-of v0, p1, Lco/uk/getmondo/signup/identity_verification/a/a/b;

    if-eqz v0, :cond_1

    check-cast p1, Lco/uk/getmondo/signup/identity_verification/a/a/b;

    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/a/a/b;->a:Ljava/lang/String;

    iget-object v1, p1, Lco/uk/getmondo/signup/identity_verification/a/a/b;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/a/a/b;->b:Lco/uk/getmondo/api/model/identity_verification/IdentityDocumentType;

    iget-object v1, p1, Lco/uk/getmondo/signup/identity_verification/a/a/b;->b:Lco/uk/getmondo/api/model/identity_verification/IdentityDocumentType;

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/a/a/b;->c:Lco/uk/getmondo/d/i;

    iget-object v1, p1, Lco/uk/getmondo/signup/identity_verification/a/a/b;->c:Lco/uk/getmondo/d/i;

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/a/a/b;->d:Ljava/lang/String;

    iget-object v1, p1, Lco/uk/getmondo/signup/identity_verification/a/a/b;->d:Ljava/lang/String;

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    const/4 v1, 0x0

    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/a/a/b;->a:Ljava/lang/String;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    :goto_0
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/a/a/b;->b:Lco/uk/getmondo/api/model/identity_verification/IdentityDocumentType;

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/a/a/b;->c:Lco/uk/getmondo/d/i;

    if-eqz v0, :cond_3

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    :goto_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lco/uk/getmondo/signup/identity_verification/a/a/b;->d:Ljava/lang/String;

    if-eqz v2, :cond_0

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_0
    add-int/2addr v0, v1

    return v0

    :cond_1
    move v0, v1

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_1

    :cond_3
    move v0, v1

    goto :goto_2
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "IdentityDocument(primaryPhotoPath="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/signup/identity_verification/a/a/b;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/signup/identity_verification/a/a/b;->b:Lco/uk/getmondo/api/model/identity_verification/IdentityDocumentType;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", country="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/signup/identity_verification/a/a/b;->c:Lco/uk/getmondo/d/i;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", secondaryPhotoPath="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/signup/identity_verification/a/a/b;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
