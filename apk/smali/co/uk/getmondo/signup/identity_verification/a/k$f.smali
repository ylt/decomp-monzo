.class final Lco/uk/getmondo/signup/identity_verification/a/k$f;
.super Ljava/lang/Object;
.source "LegacyIdentityVerificationManager.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lco/uk/getmondo/signup/identity_verification/a/k;->e()Lio/reactivex/b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h",
        "<",
        "Lco/uk/getmondo/signup/identity_verification/a/a/a;",
        "Lio/reactivex/d;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n\u00a2\u0006\u0002\u0008\u0004"
    }
    d2 = {
        "<anonymous>",
        "Lio/reactivex/Completable;",
        "fileWrapper",
        "Lco/uk/getmondo/signup/identity_verification/data/model/FileWrapper;",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field final synthetic a:Lco/uk/getmondo/signup/identity_verification/a/k;

.field final synthetic b:Ljava/lang/String;

.field final synthetic c:Lco/uk/getmondo/signup/identity_verification/a/a/b;


# direct methods
.method constructor <init>(Lco/uk/getmondo/signup/identity_verification/a/k;Ljava/lang/String;Lco/uk/getmondo/signup/identity_verification/a/a/b;)V
    .locals 0

    iput-object p1, p0, Lco/uk/getmondo/signup/identity_verification/a/k$f;->a:Lco/uk/getmondo/signup/identity_verification/a/k;

    iput-object p2, p0, Lco/uk/getmondo/signup/identity_verification/a/k$f;->b:Ljava/lang/String;

    iput-object p3, p0, Lco/uk/getmondo/signup/identity_verification/a/k$f;->c:Lco/uk/getmondo/signup/identity_verification/a/a/b;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lco/uk/getmondo/signup/identity_verification/a/a/a;)Lio/reactivex/b;
    .locals 5

    .prologue
    const-string v0, "fileWrapper"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 67
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/a/k$f;->a:Lco/uk/getmondo/signup/identity_verification/a/k;

    iget-object v1, p0, Lco/uk/getmondo/signup/identity_verification/a/k$f;->b:Ljava/lang/String;

    const-string v2, "groupId"

    invoke-static {v1, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v2, "image/jpeg"

    sget-object v3, Lco/uk/getmondo/api/model/identity_verification/LegacyIdType;->Companion:Lco/uk/getmondo/api/model/identity_verification/LegacyIdType$Companion;

    iget-object v4, p0, Lco/uk/getmondo/signup/identity_verification/a/k$f;->c:Lco/uk/getmondo/signup/identity_verification/a/a/b;

    invoke-virtual {v4}, Lco/uk/getmondo/signup/identity_verification/a/a/b;->b()Lco/uk/getmondo/api/model/identity_verification/IdentityDocumentType;

    move-result-object v4

    invoke-virtual {v3, v4}, Lco/uk/getmondo/api/model/identity_verification/LegacyIdType$Companion;->a(Lco/uk/getmondo/api/model/identity_verification/IdentityDocumentType;)Lco/uk/getmondo/api/model/identity_verification/LegacyIdType;

    move-result-object v3

    invoke-virtual {v3}, Lco/uk/getmondo/api/model/identity_verification/LegacyIdType;->a()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, p1, v1, v2, v3}, Lco/uk/getmondo/signup/identity_verification/a/k;->a(Lco/uk/getmondo/signup/identity_verification/a/k;Lco/uk/getmondo/signup/identity_verification/a/a/a;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lio/reactivex/b;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 17
    check-cast p1, Lco/uk/getmondo/signup/identity_verification/a/a/a;

    invoke-virtual {p0, p1}, Lco/uk/getmondo/signup/identity_verification/a/k$f;->a(Lco/uk/getmondo/signup/identity_verification/a/a/a;)Lio/reactivex/b;

    move-result-object v0

    return-object v0
.end method
