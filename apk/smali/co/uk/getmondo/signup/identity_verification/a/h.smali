.class public final Lco/uk/getmondo/signup/identity_verification/a/h;
.super Ljava/lang/Object;
.source "IdentityVerificationStorage.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lco/uk/getmondo/signup/identity_verification/a/h$a;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000\\\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0006\n\u0002\u0010\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\t\u0008\u0007\u0018\u0000 02\u00020\u0001:\u00010B\u0019\u0008\u0007\u0012\u0008\u0008\u0001\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\u0006\u0010\"\u001a\u00020#J\r\u0010$\u001a\u00020#H\u0000\u00a2\u0006\u0002\u0008%J\u0008\u0010&\u001a\u00020#H\u0002J\u0012\u0010\'\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u000c0\u00110(J\u000e\u0010)\u001a\u00020#2\u0006\u0010\u0007\u001a\u00020\u0008J\u0016\u0010*\u001a\u00020#2\u0006\u0010+\u001a\u00020\u000c2\u0006\u0010,\u001a\u00020\u0014J\u000e\u0010-\u001a\u00020#2\u0006\u0010\u0017\u001a\u00020\u0018J\u0016\u0010.\u001a\u00020#2\u0006\u0010/\u001a\u00020\u00082\u0006\u0010,\u001a\u00020\u0014J\u0012\u0010\u001d\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00080\u00110(R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0013\u0010\u0007\u001a\u0004\u0018\u00010\u00088F\u00a2\u0006\u0006\u001a\u0004\u0008\t\u0010\nR\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0013\u0010\u000b\u001a\u0004\u0018\u00010\u000c8F\u00a2\u0006\u0006\u001a\u0004\u0008\r\u0010\u000eRJ\u0010\u000f\u001a>\u0012\u0018\u0012\u0016\u0012\u0004\u0012\u00020\u000c \u0012*\n\u0012\u0004\u0012\u00020\u000c\u0018\u00010\u00110\u0011 \u0012*\u001e\u0012\u0018\u0012\u0016\u0012\u0004\u0012\u00020\u000c \u0012*\n\u0012\u0004\u0012\u00020\u000c\u0018\u00010\u00110\u0011\u0018\u00010\u00100\u0010X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0011\u0010\u0013\u001a\u00020\u00148F\u00a2\u0006\u0006\u001a\u0004\u0008\u0015\u0010\u0016R\u0013\u0010\u0017\u001a\u0004\u0018\u00010\u00188F\u00a2\u0006\u0006\u001a\u0004\u0008\u0019\u0010\u001aR\u000e\u0010\u001b\u001a\u00020\u001cX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0013\u0010\u001d\u001a\u0004\u0018\u00010\u00088F\u00a2\u0006\u0006\u001a\u0004\u0008\u001e\u0010\nRJ\u0010\u001f\u001a>\u0012\u0018\u0012\u0016\u0012\u0004\u0012\u00020\u0008 \u0012*\n\u0012\u0004\u0012\u00020\u0008\u0018\u00010\u00110\u0011 \u0012*\u001e\u0012\u0018\u0012\u0016\u0012\u0004\u0012\u00020\u0008 \u0012*\n\u0012\u0004\u0012\u00020\u0008\u0018\u00010\u00110\u0011\u0018\u00010\u00100\u0010X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0011\u0010 \u001a\u00020\u00148F\u00a2\u0006\u0006\u001a\u0004\u0008!\u0010\u0016\u00a8\u00061"
    }
    d2 = {
        "Lco/uk/getmondo/signup/identity_verification/data/IdentityVerificationStorage;",
        "",
        "context",
        "Landroid/content/Context;",
        "gson",
        "Lcom/google/gson/Gson;",
        "(Landroid/content/Context;Lcom/google/gson/Gson;)V",
        "groupId",
        "",
        "getGroupId",
        "()Ljava/lang/String;",
        "identityDocument",
        "Lco/uk/getmondo/signup/identity_verification/data/model/IdentityDocument;",
        "getIdentityDocument",
        "()Lco/uk/getmondo/signup/identity_verification/data/model/IdentityDocument;",
        "identityDocumentRelay",
        "Lcom/jakewharton/rxrelay2/BehaviorRelay;",
        "Lcom/memoizrlabs/poweroptional/Optional;",
        "kotlin.jvm.PlatformType",
        "identityDocumentUsedSystemCamera",
        "",
        "getIdentityDocumentUsedSystemCamera",
        "()Z",
        "identityVerification",
        "Lco/uk/getmondo/api/model/identity_verification/IdentityVerification;",
        "getIdentityVerification",
        "()Lco/uk/getmondo/api/model/identity_verification/IdentityVerification;",
        "sharedPreferences",
        "Landroid/content/SharedPreferences;",
        "videoPath",
        "getVideoPath",
        "videoPathRelay",
        "videoUsedSystemCamera",
        "getVideoUsedSystemCamera",
        "clearAll",
        "",
        "deleteDocuments",
        "deleteDocuments$app_monzoPrepaidRelease",
        "deleteFiles",
        "identityDocumentEvidence",
        "Lio/reactivex/Observable;",
        "saveGroupId",
        "saveIdentityDocumentEvidence",
        "evidence",
        "usedSystemCamera",
        "saveIdentityVerification",
        "saveVideoEvidence",
        "path",
        "Companion",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# static fields
.field public static final a:Lco/uk/getmondo/signup/identity_verification/a/h$a;


# instance fields
.field private final b:Lcom/b/b/b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/b/b/b",
            "<",
            "Lcom/c/b/b",
            "<",
            "Lco/uk/getmondo/signup/identity_verification/a/a/b;",
            ">;>;"
        }
    .end annotation
.end field

.field private final c:Lcom/b/b/b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/b/b/b",
            "<",
            "Lcom/c/b/b",
            "<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field private final d:Landroid/content/SharedPreferences;

.field private final e:Landroid/content/Context;

.field private final f:Lcom/google/gson/f;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lco/uk/getmondo/signup/identity_verification/a/h$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lco/uk/getmondo/signup/identity_verification/a/h$a;-><init>(Lkotlin/d/b/i;)V

    sput-object v0, Lco/uk/getmondo/signup/identity_verification/a/h;->a:Lco/uk/getmondo/signup/identity_verification/a/h$a;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/gson/f;)V
    .locals 3

    .prologue
    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "gson"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lco/uk/getmondo/signup/identity_verification/a/h;->e:Landroid/content/Context;

    iput-object p2, p0, Lco/uk/getmondo/signup/identity_verification/a/h;->f:Lcom/google/gson/f;

    .line 25
    invoke-static {}, Lcom/b/b/b;->a()Lcom/b/b/b;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/signup/identity_verification/a/h;->b:Lcom/b/b/b;

    .line 26
    invoke-static {}, Lcom/b/b/b;->a()Lcom/b/b/b;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/signup/identity_verification/a/h;->c:Lcom/b/b/b;

    .line 31
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/a/h;->e:Landroid/content/Context;

    const-string v1, "identity_verification"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "context.getSharedPrefere\u2026ME, Context.MODE_PRIVATE)"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v0, p0, Lco/uk/getmondo/signup/identity_verification/a/h;->d:Landroid/content/SharedPreferences;

    .line 32
    invoke-virtual {p0}, Lco/uk/getmondo/signup/identity_verification/a/h;->a()Lco/uk/getmondo/signup/identity_verification/a/a/b;

    move-result-object v0

    .line 33
    invoke-virtual {p0}, Lco/uk/getmondo/signup/identity_verification/a/h;->c()Ljava/lang/String;

    move-result-object v1

    .line 35
    iget-object v2, p0, Lco/uk/getmondo/signup/identity_verification/a/h;->b:Lcom/b/b/b;

    invoke-static {v0}, Lcom/c/b/b;->b(Ljava/lang/Object;)Lcom/c/b/b;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/b/b/b;->a(Ljava/lang/Object;)V

    .line 36
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/a/h;->c:Lcom/b/b/b;

    invoke-static {v1}, Lcom/c/b/b;->b(Ljava/lang/Object;)Lcom/c/b/b;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/b/b/b;->a(Ljava/lang/Object;)V

    return-void
.end method

.method private final j()V
    .locals 3

    .prologue
    .line 128
    invoke-virtual {p0}, Lco/uk/getmondo/signup/identity_verification/a/h;->a()Lco/uk/getmondo/signup/identity_verification/a/a/b;

    move-result-object v0

    .line 129
    if-eqz v0, :cond_0

    .line 130
    new-instance v1, Ljava/io/File;

    invoke-virtual {v0}, Lco/uk/getmondo/signup/identity_verification/a/a/b;->a()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    .line 131
    invoke-virtual {v0}, Lco/uk/getmondo/signup/identity_verification/a/a/b;->d()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 132
    new-instance v1, Ljava/io/File;

    invoke-virtual {v0}, Lco/uk/getmondo/signup/identity_verification/a/a/b;->d()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    .line 136
    :cond_0
    invoke-virtual {p0}, Lco/uk/getmondo/signup/identity_verification/a/h;->c()Ljava/lang/String;

    move-result-object v0

    .line 137
    if-eqz v0, :cond_1

    .line 138
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    .line 140
    :cond_1
    return-void
.end method


# virtual methods
.method public final a()Lco/uk/getmondo/signup/identity_verification/a/a/b;
    .locals 7

    .prologue
    const/4 v0, 0x0

    .line 42
    iget-object v1, p0, Lco/uk/getmondo/signup/identity_verification/a/h;->d:Landroid/content/SharedPreferences;

    const-string v2, "photo_id_type"

    const/4 v3, -0x1

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    .line 43
    if-ltz v1, :cond_0

    .line 44
    iget-object v2, p0, Lco/uk/getmondo/signup/identity_verification/a/h;->d:Landroid/content/SharedPreferences;

    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    .line 45
    const-string v3, "identity_document_type"

    invoke-static {}, Lco/uk/getmondo/api/model/identity_verification/IdentityDocumentType;->values()[Lco/uk/getmondo/api/model/identity_verification/IdentityDocumentType;

    move-result-object v4

    aget-object v1, v4, v1

    invoke-virtual {v1}, Lco/uk/getmondo/api/model/identity_verification/IdentityDocumentType;->a()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v2, v3, v1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    .line 46
    const-string v2, "photo_id_type"

    invoke-interface {v1, v2}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    .line 47
    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 50
    :cond_0
    iget-object v1, p0, Lco/uk/getmondo/signup/identity_verification/a/h;->d:Landroid/content/SharedPreferences;

    const-string v2, "photo_id_path"

    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 51
    sget-object v2, Lco/uk/getmondo/api/model/identity_verification/IdentityDocumentType;->Companion:Lco/uk/getmondo/api/model/identity_verification/IdentityDocumentType$Companion;

    iget-object v3, p0, Lco/uk/getmondo/signup/identity_verification/a/h;->d:Landroid/content/SharedPreferences;

    const-string v4, "identity_document_type"

    invoke-interface {v3, v4, v0}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lco/uk/getmondo/api/model/identity_verification/IdentityDocumentType$Companion;->a(Ljava/lang/String;)Lco/uk/getmondo/api/model/identity_verification/IdentityDocumentType;

    move-result-object v2

    .line 52
    iget-object v3, p0, Lco/uk/getmondo/signup/identity_verification/a/h;->d:Landroid/content/SharedPreferences;

    const-string v4, "secondary_photo_id_path"

    invoke-interface {v3, v4, v0}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 53
    sget-object v4, Lco/uk/getmondo/d/i;->Companion:Lco/uk/getmondo/d/i$a;

    iget-object v5, p0, Lco/uk/getmondo/signup/identity_verification/a/h;->d:Landroid/content/SharedPreferences;

    const-string v6, "identity_document_country"

    invoke-interface {v5, v6, v0}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lco/uk/getmondo/d/i$a;->a(Ljava/lang/String;)Lco/uk/getmondo/d/i;

    move-result-object v4

    .line 55
    if-eqz v1, :cond_1

    if-eqz v2, :cond_1

    if-nez v4, :cond_2

    .line 58
    :cond_1
    :goto_0
    return-object v0

    :cond_2
    new-instance v0, Lco/uk/getmondo/signup/identity_verification/a/a/b;

    invoke-direct {v0, v1, v2, v4, v3}, Lco/uk/getmondo/signup/identity_verification/a/a/b;-><init>(Ljava/lang/String;Lco/uk/getmondo/api/model/identity_verification/IdentityDocumentType;Lco/uk/getmondo/d/i;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final a(Lco/uk/getmondo/api/model/identity_verification/IdentityVerification;)V
    .locals 3

    .prologue
    const-string v0, "identityVerification"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 100
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/a/h;->d:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 101
    const-string v1, "identity_verification"

    iget-object v2, p0, Lco/uk/getmondo/signup/identity_verification/a/h;->f:Lcom/google/gson/f;

    invoke-virtual {v2, p1}, Lcom/google/gson/f;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 102
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 103
    return-void
.end method

.method public final a(Lco/uk/getmondo/signup/identity_verification/a/a/b;Z)V
    .locals 3

    .prologue
    const-string v0, "evidence"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 77
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/a/h;->d:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 78
    const-string v1, "photo_id_path"

    invoke-virtual {p1}, Lco/uk/getmondo/signup/identity_verification/a/a/b;->a()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 79
    const-string v1, "identity_document_country"

    invoke-virtual {p1}, Lco/uk/getmondo/signup/identity_verification/a/a/b;->c()Lco/uk/getmondo/d/i;

    move-result-object v2

    invoke-virtual {v2}, Lco/uk/getmondo/d/i;->e()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 80
    const-string v1, "identity_document_type"

    invoke-virtual {p1}, Lco/uk/getmondo/signup/identity_verification/a/a/b;->b()Lco/uk/getmondo/api/model/identity_verification/IdentityDocumentType;

    move-result-object v2

    invoke-virtual {v2}, Lco/uk/getmondo/api/model/identity_verification/IdentityDocumentType;->a()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 81
    const-string v1, "secondary_photo_id_path"

    invoke-virtual {p1}, Lco/uk/getmondo/signup/identity_verification/a/a/b;->d()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 82
    const-string v1, "id_used_system_camera"

    invoke-interface {v0, v1, p2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 83
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 84
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/a/h;->b:Lcom/b/b/b;

    invoke-static {p1}, Lcom/c/b/b;->b(Ljava/lang/Object;)Lcom/c/b/b;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/b/b/b;->a(Ljava/lang/Object;)V

    .line 85
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 2

    .prologue
    const-string v0, "groupId"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 96
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/a/h;->d:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "group_id"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 97
    return-void
.end method

.method public final a(Ljava/lang/String;Z)V
    .locals 2

    .prologue
    const-string v0, "path"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 88
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/a/h;->d:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 89
    const-string v1, "video_id_path"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 90
    const-string v1, "video_used_system_camera"

    invoke-interface {v0, v1, p2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 91
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 92
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/a/h;->c:Lcom/b/b/b;

    invoke-static {p1}, Lcom/c/b/b;->b(Ljava/lang/Object;)Lcom/c/b/b;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/b/b/b;->a(Ljava/lang/Object;)V

    .line 93
    return-void
.end method

.method public final b()Z
    .locals 3

    .prologue
    .line 62
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/a/h;->d:Landroid/content/SharedPreferences;

    const-string v1, "id_used_system_camera"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public final c()Ljava/lang/String;
    .locals 3

    .prologue
    .line 65
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/a/h;->d:Landroid/content/SharedPreferences;

    const-string v1, "video_id_path"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final d()Z
    .locals 3

    .prologue
    .line 68
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/a/h;->d:Landroid/content/SharedPreferences;

    const-string v1, "video_used_system_camera"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public final e()Ljava/lang/String;
    .locals 3

    .prologue
    .line 71
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/a/h;->d:Landroid/content/SharedPreferences;

    const-string v1, "group_id"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final f()Lio/reactivex/n;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Lcom/c/b/b",
            "<",
            "Lco/uk/getmondo/signup/identity_verification/a/a/b;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 106
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/a/h;->b:Lcom/b/b/b;

    const-string v1, "identityDocumentRelay"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lio/reactivex/n;

    return-object v0
.end method

.method public final g()Lio/reactivex/n;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Lcom/c/b/b",
            "<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 110
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/a/h;->c:Lcom/b/b/b;

    const-string v1, "videoPathRelay"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lio/reactivex/n;

    return-object v0
.end method

.method public final h()V
    .locals 2

    .prologue
    .line 114
    invoke-direct {p0}, Lco/uk/getmondo/signup/identity_verification/a/h;->j()V

    .line 115
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/a/h;->d:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 116
    const-string v1, "photo_id_path"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 117
    const-string v1, "secondary_photo_id_path"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 118
    const-string v1, "identity_document_type"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 119
    const-string v1, "identity_document_country"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 120
    const-string v1, "id_used_system_camera"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 121
    const-string v1, "video_id_path"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 122
    const-string v1, "video_used_system_camera"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 123
    const-string v1, "photo_id_type"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 124
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 125
    return-void
.end method

.method public final i()V
    .locals 1

    .prologue
    .line 143
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/a/h;->d:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 144
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->clear()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 145
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 146
    return-void
.end method
