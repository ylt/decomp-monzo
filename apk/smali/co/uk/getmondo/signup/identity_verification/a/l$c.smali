.class final Lco/uk/getmondo/signup/identity_verification/a/l$c;
.super Ljava/lang/Object;
.source "NewIdentityVerificationManager.kt"

# interfaces
.implements Lio/reactivex/c/g;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lco/uk/getmondo/signup/identity_verification/a/l;->a()Lio/reactivex/v;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/g",
        "<",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0003\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0004\u0008\u0005\u0010\u0006"
    }
    d2 = {
        "<anonymous>",
        "",
        "it",
        "",
        "kotlin.jvm.PlatformType",
        "accept",
        "(Ljava/lang/Boolean;)V"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field final synthetic a:Lco/uk/getmondo/signup/identity_verification/a/l;


# direct methods
.method constructor <init>(Lco/uk/getmondo/signup/identity_verification/a/l;)V
    .locals 0

    iput-object p1, p0, Lco/uk/getmondo/signup/identity_verification/a/l$c;->a:Lco/uk/getmondo/signup/identity_verification/a/l;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Boolean;)V
    .locals 3

    .prologue
    .line 36
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/a/l$c;->a:Lco/uk/getmondo/signup/identity_verification/a/l;

    invoke-static {v0}, Lco/uk/getmondo/signup/identity_verification/a/l;->a(Lco/uk/getmondo/signup/identity_verification/a/l;)Lco/uk/getmondo/signup/identity_verification/a/h;

    move-result-object v0

    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "UUID.randomUUID().toString()"

    invoke-static {v1, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lco/uk/getmondo/signup/identity_verification/a/h;->a(Ljava/lang/String;)V

    .line 37
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/a/l$c;->a:Lco/uk/getmondo/signup/identity_verification/a/l;

    invoke-static {v0}, Lco/uk/getmondo/signup/identity_verification/a/l;->a(Lco/uk/getmondo/signup/identity_verification/a/l;)Lco/uk/getmondo/signup/identity_verification/a/h;

    move-result-object v0

    invoke-virtual {v0}, Lco/uk/getmondo/signup/identity_verification/a/h;->h()V

    .line 38
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/a/l$c;->a:Lco/uk/getmondo/signup/identity_verification/a/l;

    invoke-static {v0}, Lco/uk/getmondo/signup/identity_verification/a/l;->b(Lco/uk/getmondo/signup/identity_verification/a/l;)Lco/uk/getmondo/common/i;

    move-result-object v0

    invoke-virtual {v0}, Lco/uk/getmondo/common/i;->a()V

    .line 39
    return-void
.end method

.method public bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 17
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Lco/uk/getmondo/signup/identity_verification/a/l$c;->a(Ljava/lang/Boolean;)V

    return-void
.end method
