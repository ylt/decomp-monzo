.class public Lco/uk/getmondo/signup/identity_verification/sdd/IdentityVerificationSddActivity_ViewBinding;
.super Ljava/lang/Object;
.source "IdentityVerificationSddActivity_ViewBinding.java"

# interfaces
.implements Lbutterknife/Unbinder;


# instance fields
.field private a:Lco/uk/getmondo/signup/identity_verification/sdd/IdentityVerificationSddActivity;


# direct methods
.method public constructor <init>(Lco/uk/getmondo/signup/identity_verification/sdd/IdentityVerificationSddActivity;Landroid/view/View;)V
    .locals 3

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    iput-object p1, p0, Lco/uk/getmondo/signup/identity_verification/sdd/IdentityVerificationSddActivity_ViewBinding;->a:Lco/uk/getmondo/signup/identity_verification/sdd/IdentityVerificationSddActivity;

    .line 28
    const v0, 0x7f1100fe

    const-string v1, "field \'toolbar\'"

    const-class v2, Landroid/support/v7/widget/Toolbar;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/Toolbar;

    iput-object v0, p1, Lco/uk/getmondo/signup/identity_verification/sdd/IdentityVerificationSddActivity;->toolbar:Landroid/support/v7/widget/Toolbar;

    .line 29
    const v0, 0x7f1101aa

    const-string v1, "field \'sddTitleTextView\'"

    const-class v2, Landroid/widget/TextView;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p1, Lco/uk/getmondo/signup/identity_verification/sdd/IdentityVerificationSddActivity;->sddTitleTextView:Landroid/widget/TextView;

    .line 30
    const v0, 0x7f1101ab

    const-string v1, "field \'sddBodyTextView\'"

    const-class v2, Landroid/widget/TextView;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p1, Lco/uk/getmondo/signup/identity_verification/sdd/IdentityVerificationSddActivity;->sddBodyTextView:Landroid/widget/TextView;

    .line 31
    const v0, 0x7f1101ac

    const-string v1, "field \'sddVerifyButton\'"

    const-class v2, Landroid/widget/Button;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p1, Lco/uk/getmondo/signup/identity_verification/sdd/IdentityVerificationSddActivity;->sddVerifyButton:Landroid/widget/Button;

    .line 32
    const v0, 0x7f1101ad

    const-string v1, "field \'sddNotNowButton\'"

    const-class v2, Landroid/widget/Button;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p1, Lco/uk/getmondo/signup/identity_verification/sdd/IdentityVerificationSddActivity;->sddNotNowButton:Landroid/widget/Button;

    .line 33
    return-void
.end method


# virtual methods
.method public unbind()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 38
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/sdd/IdentityVerificationSddActivity_ViewBinding;->a:Lco/uk/getmondo/signup/identity_verification/sdd/IdentityVerificationSddActivity;

    .line 39
    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Bindings already cleared."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 40
    :cond_0
    iput-object v1, p0, Lco/uk/getmondo/signup/identity_verification/sdd/IdentityVerificationSddActivity_ViewBinding;->a:Lco/uk/getmondo/signup/identity_verification/sdd/IdentityVerificationSddActivity;

    .line 42
    iput-object v1, v0, Lco/uk/getmondo/signup/identity_verification/sdd/IdentityVerificationSddActivity;->toolbar:Landroid/support/v7/widget/Toolbar;

    .line 43
    iput-object v1, v0, Lco/uk/getmondo/signup/identity_verification/sdd/IdentityVerificationSddActivity;->sddTitleTextView:Landroid/widget/TextView;

    .line 44
    iput-object v1, v0, Lco/uk/getmondo/signup/identity_verification/sdd/IdentityVerificationSddActivity;->sddBodyTextView:Landroid/widget/TextView;

    .line 45
    iput-object v1, v0, Lco/uk/getmondo/signup/identity_verification/sdd/IdentityVerificationSddActivity;->sddVerifyButton:Landroid/widget/Button;

    .line 46
    iput-object v1, v0, Lco/uk/getmondo/signup/identity_verification/sdd/IdentityVerificationSddActivity;->sddNotNowButton:Landroid/widget/Button;

    .line 47
    return-void
.end method
