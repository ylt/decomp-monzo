.class Lco/uk/getmondo/signup/identity_verification/sdd/f;
.super Lco/uk/getmondo/common/ui/b;
.source "IdentityVerificationSddPresenter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lco/uk/getmondo/signup/identity_verification/sdd/f$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lco/uk/getmondo/common/ui/b",
        "<",
        "Lco/uk/getmondo/signup/identity_verification/sdd/f$a;",
        ">;"
    }
.end annotation


# instance fields
.field private final c:Lco/uk/getmondo/signup/identity_verification/sdd/j;

.field private final d:Lco/uk/getmondo/common/a;

.field private final e:Ljava/lang/String;


# direct methods
.method constructor <init>(Lco/uk/getmondo/signup/identity_verification/sdd/j;Lco/uk/getmondo/common/a;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Lco/uk/getmondo/common/ui/b;-><init>()V

    .line 27
    iput-object p1, p0, Lco/uk/getmondo/signup/identity_verification/sdd/f;->c:Lco/uk/getmondo/signup/identity_verification/sdd/j;

    .line 28
    iput-object p2, p0, Lco/uk/getmondo/signup/identity_verification/sdd/f;->d:Lco/uk/getmondo/common/a;

    .line 29
    iput-object p3, p0, Lco/uk/getmondo/signup/identity_verification/sdd/f;->e:Ljava/lang/String;

    .line 30
    return-void
.end method

.method static synthetic a(Lco/uk/getmondo/signup/identity_verification/sdd/f;Lco/uk/getmondo/signup/identity_verification/sdd/f$a;Ljava/lang/Object;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 55
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/sdd/f;->d:Lco/uk/getmondo/common/a;

    iget-object v1, p0, Lco/uk/getmondo/signup/identity_verification/sdd/f;->c:Lco/uk/getmondo/signup/identity_verification/sdd/j;

    invoke-static {v1}, Lco/uk/getmondo/api/model/tracking/Impression;->a(Lco/uk/getmondo/signup/identity_verification/sdd/j;)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v1

    invoke-virtual {v0, v1}, Lco/uk/getmondo/common/a;->a(Lco/uk/getmondo/api/model/tracking/Impression;)V

    .line 56
    const-string v0, "https://monzo.com/-webviews/identity-upgrade"

    invoke-interface {p1, v0}, Lco/uk/getmondo/signup/identity_verification/sdd/f$a;->a(Ljava/lang/String;)V

    .line 57
    return-void
.end method

.method static synthetic b(Lco/uk/getmondo/signup/identity_verification/sdd/f;Lco/uk/getmondo/signup/identity_verification/sdd/f$a;Ljava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 46
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/sdd/f;->e:Ljava/lang/String;

    invoke-static {v0}, Lco/uk/getmondo/common/k/p;->d(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 47
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/sdd/f;->c:Lco/uk/getmondo/signup/identity_verification/sdd/j;

    invoke-virtual {v0}, Lco/uk/getmondo/signup/identity_verification/sdd/j;->c()Lco/uk/getmondo/api/model/tracking/Impression$KycFrom;

    move-result-object v0

    invoke-interface {p1, v0}, Lco/uk/getmondo/signup/identity_verification/sdd/f$a;->a(Lco/uk/getmondo/api/model/tracking/Impression$KycFrom;)V

    .line 51
    :goto_0
    return-void

    .line 49
    :cond_0
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/sdd/f;->c:Lco/uk/getmondo/signup/identity_verification/sdd/j;

    invoke-virtual {v0}, Lco/uk/getmondo/signup/identity_verification/sdd/j;->c()Lco/uk/getmondo/api/model/tracking/Impression$KycFrom;

    move-result-object v0

    invoke-interface {p1, v0}, Lco/uk/getmondo/signup/identity_verification/sdd/f$a;->b(Lco/uk/getmondo/api/model/tracking/Impression$KycFrom;)V

    goto :goto_0
.end method


# virtual methods
.method public bridge synthetic a(Lco/uk/getmondo/common/ui/f;)V
    .locals 0

    .prologue
    .line 16
    check-cast p1, Lco/uk/getmondo/signup/identity_verification/sdd/f$a;

    invoke-virtual {p0, p1}, Lco/uk/getmondo/signup/identity_verification/sdd/f;->a(Lco/uk/getmondo/signup/identity_verification/sdd/f$a;)V

    return-void
.end method

.method public a(Lco/uk/getmondo/signup/identity_verification/sdd/f$a;)V
    .locals 2

    .prologue
    .line 34
    invoke-super {p0, p1}, Lco/uk/getmondo/common/ui/b;->a(Lco/uk/getmondo/common/ui/f;)V

    .line 36
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/sdd/f;->d:Lco/uk/getmondo/common/a;

    iget-object v1, p0, Lco/uk/getmondo/signup/identity_verification/sdd/f;->c:Lco/uk/getmondo/signup/identity_verification/sdd/j;

    invoke-virtual {v1}, Lco/uk/getmondo/signup/identity_verification/sdd/j;->a()Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v1

    invoke-virtual {v0, v1}, Lco/uk/getmondo/common/a;->a(Lco/uk/getmondo/api/model/tracking/Impression;)V

    .line 38
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/sdd/f;->c:Lco/uk/getmondo/signup/identity_verification/sdd/j;

    sget-object v1, Lco/uk/getmondo/signup/identity_verification/sdd/j;->b:Lco/uk/getmondo/signup/identity_verification/sdd/j;

    if-ne v0, v1, :cond_1

    .line 39
    invoke-interface {p1}, Lco/uk/getmondo/signup/identity_verification/sdd/f$a;->c()V

    .line 44
    :cond_0
    :goto_0
    invoke-interface {p1}, Lco/uk/getmondo/signup/identity_verification/sdd/f$a;->a()Lio/reactivex/n;

    move-result-object v0

    invoke-static {p0, p1}, Lco/uk/getmondo/signup/identity_verification/sdd/g;->a(Lco/uk/getmondo/signup/identity_verification/sdd/f;Lco/uk/getmondo/signup/identity_verification/sdd/f$a;)Lio/reactivex/c/g;

    move-result-object v1

    .line 45
    invoke-virtual {v0, v1}, Lio/reactivex/n;->subscribe(Lio/reactivex/c/g;)Lio/reactivex/b/b;

    move-result-object v0

    .line 44
    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/identity_verification/sdd/f;->a(Lio/reactivex/b/b;)V

    .line 53
    invoke-interface {p1}, Lco/uk/getmondo/signup/identity_verification/sdd/f$a;->b()Lio/reactivex/n;

    move-result-object v0

    invoke-static {p0, p1}, Lco/uk/getmondo/signup/identity_verification/sdd/h;->a(Lco/uk/getmondo/signup/identity_verification/sdd/f;Lco/uk/getmondo/signup/identity_verification/sdd/f$a;)Lio/reactivex/c/g;

    move-result-object v1

    .line 54
    invoke-virtual {v0, v1}, Lio/reactivex/n;->subscribe(Lio/reactivex/c/g;)Lio/reactivex/b/b;

    move-result-object v0

    .line 53
    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/identity_verification/sdd/f;->a(Lio/reactivex/b/b;)V

    .line 58
    return-void

    .line 40
    :cond_1
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/sdd/f;->c:Lco/uk/getmondo/signup/identity_verification/sdd/j;

    sget-object v1, Lco/uk/getmondo/signup/identity_verification/sdd/j;->c:Lco/uk/getmondo/signup/identity_verification/sdd/j;

    if-ne v0, v1, :cond_0

    .line 41
    invoke-interface {p1}, Lco/uk/getmondo/signup/identity_verification/sdd/f$a;->d()V

    goto :goto_0
.end method
