.class public Lco/uk/getmondo/signup/identity_verification/sdd/IdentityVerificationSddActivity;
.super Lco/uk/getmondo/common/activities/b;
.source "IdentityVerificationSddActivity.java"

# interfaces
.implements Lco/uk/getmondo/signup/identity_verification/sdd/f$a;


# instance fields
.field a:Lco/uk/getmondo/signup/identity_verification/sdd/f;

.field sddBodyTextView:Landroid/widget/TextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f1101ab
    .end annotation
.end field

.field sddNotNowButton:Landroid/widget/Button;
    .annotation build Lbutterknife/BindView;
        value = 0x7f1101ad
    .end annotation
.end field

.field sddTitleTextView:Landroid/widget/TextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f1101aa
    .end annotation
.end field

.field sddVerifyButton:Landroid/widget/Button;
    .annotation build Lbutterknife/BindView;
        value = 0x7f1101ac
    .end annotation
.end field

.field toolbar:Landroid/support/v7/widget/Toolbar;
    .annotation build Lbutterknife/BindView;
        value = 0x7f1100fe
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Lco/uk/getmondo/common/activities/b;-><init>()V

    return-void
.end method

.method public static a(Landroid/content/Context;Lco/uk/getmondo/signup/identity_verification/sdd/j;)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 43
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lco/uk/getmondo/signup/identity_verification/sdd/IdentityVerificationSddActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "KEY_EXTRA_SDD_UPGRADE_TYPE"

    .line 44
    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    move-result-object v0

    .line 43
    return-object v0
.end method

.method public static a(Landroid/content/Context;Lco/uk/getmondo/signup/identity_verification/sdd/j;Ljava/lang/String;)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 48
    invoke-static {p0, p1}, Lco/uk/getmondo/signup/identity_verification/sdd/IdentityVerificationSddActivity;->a(Landroid/content/Context;Lco/uk/getmondo/signup/identity_verification/sdd/j;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "KEY_EXTRA_REJECTION_NOTE"

    .line 49
    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 48
    return-object v0
.end method


# virtual methods
.method public a()Lio/reactivex/n;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 78
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/sdd/IdentityVerificationSddActivity;->sddVerifyButton:Landroid/widget/Button;

    invoke-static {v0}, Lcom/b/a/c/c;->a(Landroid/view/View;)Lio/reactivex/n;

    move-result-object v0

    return-object v0
.end method

.method public a(Lco/uk/getmondo/api/model/tracking/Impression$KycFrom;)V
    .locals 2

    .prologue
    .line 100
    sget-object v0, Lco/uk/getmondo/signup/identity_verification/a/j;->b:Lco/uk/getmondo/signup/identity_verification/a/j;

    sget-object v1, Lco/uk/getmondo/api/model/signup/SignupSource;->SDD_MIGRATION:Lco/uk/getmondo/api/model/signup/SignupSource;

    invoke-static {p0, v0, p1, v1}, Lco/uk/getmondo/signup/identity_verification/IdentityVerificationActivity;->a(Landroid/content/Context;Lco/uk/getmondo/signup/identity_verification/a/j;Lco/uk/getmondo/api/model/tracking/Impression$KycFrom;Lco/uk/getmondo/api/model/signup/SignupSource;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/identity_verification/sdd/IdentityVerificationSddActivity;->startActivity(Landroid/content/Intent;)V

    .line 101
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 110
    new-instance v0, Landroid/support/b/a$a;

    invoke-direct {v0}, Landroid/support/b/a$a;-><init>()V

    const v1, 0x7f0f000f

    invoke-static {p0, v1}, Landroid/support/v4/content/a;->c(Landroid/content/Context;I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/support/b/a$a;->a(I)Landroid/support/b/a$a;

    move-result-object v0

    .line 111
    invoke-virtual {v0}, Landroid/support/b/a$a;->a()Landroid/support/b/a;

    move-result-object v0

    .line 112
    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, p0, v1}, Landroid/support/b/a;->a(Landroid/content/Context;Landroid/net/Uri;)V

    .line 113
    return-void
.end method

.method public b()Lio/reactivex/n;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 83
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/sdd/IdentityVerificationSddActivity;->sddNotNowButton:Landroid/widget/Button;

    invoke-static {v0}, Lcom/b/a/c/c;->a(Landroid/view/View;)Lio/reactivex/n;

    move-result-object v0

    return-object v0
.end method

.method public b(Lco/uk/getmondo/api/model/tracking/Impression$KycFrom;)V
    .locals 2

    .prologue
    .line 105
    sget-object v0, Lco/uk/getmondo/signup/identity_verification/a/j;->b:Lco/uk/getmondo/signup/identity_verification/a/j;

    sget-object v1, Lco/uk/getmondo/api/model/signup/SignupSource;->SDD_MIGRATION:Lco/uk/getmondo/api/model/signup/SignupSource;

    invoke-static {p0, v0, p1, v1}, Lco/uk/getmondo/signup/identity_verification/IdentityVerificationActivity;->a(Landroid/content/Context;Lco/uk/getmondo/signup/identity_verification/a/j;Lco/uk/getmondo/api/model/tracking/Impression$KycFrom;Lco/uk/getmondo/api/model/signup/SignupSource;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/identity_verification/sdd/IdentityVerificationSddActivity;->startActivity(Landroid/content/Intent;)V

    .line 106
    return-void
.end method

.method public c()V
    .locals 2

    .prologue
    .line 88
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/sdd/IdentityVerificationSddActivity;->sddBodyTextView:Landroid/widget/TextView;

    const v1, 0x7f0a01da

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 89
    return-void
.end method

.method public d()V
    .locals 2

    .prologue
    .line 93
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/sdd/IdentityVerificationSddActivity;->toolbar:Landroid/support/v7/widget/Toolbar;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/Toolbar;->setVisibility(I)V

    .line 94
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/sdd/IdentityVerificationSddActivity;->sddTitleTextView:Landroid/widget/TextView;

    const v1, 0x7f0a01de

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 95
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/sdd/IdentityVerificationSddActivity;->sddBodyTextView:Landroid/widget/TextView;

    const v1, 0x7f0a01db

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 96
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 54
    invoke-virtual {p0}, Lco/uk/getmondo/signup/identity_verification/sdd/IdentityVerificationSddActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "KEY_EXTRA_SDD_UPGRADE_TYPE"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/signup/identity_verification/sdd/j;

    .line 55
    sget-object v1, Lco/uk/getmondo/signup/identity_verification/sdd/j;->a:Lco/uk/getmondo/signup/identity_verification/sdd/j;

    if-ne v0, v1, :cond_0

    const v1, 0x7f0c0124

    .line 56
    :goto_0
    invoke-virtual {p0, v1}, Lco/uk/getmondo/signup/identity_verification/sdd/IdentityVerificationSddActivity;->setTheme(I)V

    .line 57
    invoke-super {p0, p1}, Lco/uk/getmondo/common/activities/b;->onCreate(Landroid/os/Bundle;)V

    .line 59
    const v1, 0x7f050044

    invoke-virtual {p0, v1}, Lco/uk/getmondo/signup/identity_verification/sdd/IdentityVerificationSddActivity;->setContentView(I)V

    .line 60
    invoke-static {p0}, Lbutterknife/ButterKnife;->bind(Landroid/app/Activity;)Lbutterknife/Unbinder;

    .line 62
    if-nez v0, :cond_1

    .line 63
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "IdentityVerificationSddActivity requires an SddUpgradeLevelType"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 55
    :cond_0
    const v1, 0x7f0c0127

    goto :goto_0

    .line 65
    :cond_1
    invoke-virtual {p0}, Lco/uk/getmondo/signup/identity_verification/sdd/IdentityVerificationSddActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "KEY_EXTRA_REJECTION_NOTE"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 66
    invoke-virtual {p0}, Lco/uk/getmondo/signup/identity_verification/sdd/IdentityVerificationSddActivity;->l()Lco/uk/getmondo/common/h/b/b;

    move-result-object v2

    new-instance v3, Lco/uk/getmondo/signup/identity_verification/sdd/c;

    invoke-direct {v3, v0, v1}, Lco/uk/getmondo/signup/identity_verification/sdd/c;-><init>(Lco/uk/getmondo/signup/identity_verification/sdd/j;Ljava/lang/String;)V

    invoke-interface {v2, v3}, Lco/uk/getmondo/common/h/b/b;->a(Lco/uk/getmondo/signup/identity_verification/sdd/c;)Lco/uk/getmondo/signup/identity_verification/sdd/b;

    move-result-object v0

    invoke-interface {v0, p0}, Lco/uk/getmondo/signup/identity_verification/sdd/b;->a(Lco/uk/getmondo/signup/identity_verification/sdd/IdentityVerificationSddActivity;)V

    .line 67
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/sdd/IdentityVerificationSddActivity;->a:Lco/uk/getmondo/signup/identity_verification/sdd/f;

    invoke-virtual {v0, p0}, Lco/uk/getmondo/signup/identity_verification/sdd/f;->a(Lco/uk/getmondo/signup/identity_verification/sdd/f$a;)V

    .line 68
    return-void
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/sdd/IdentityVerificationSddActivity;->a:Lco/uk/getmondo/signup/identity_verification/sdd/f;

    invoke-virtual {v0}, Lco/uk/getmondo/signup/identity_verification/sdd/f;->b()V

    .line 73
    invoke-super {p0}, Lco/uk/getmondo/common/activities/b;->onDestroy()V

    .line 74
    return-void
.end method
