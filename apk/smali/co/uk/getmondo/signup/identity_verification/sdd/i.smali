.class public final Lco/uk/getmondo/signup/identity_verification/sdd/i;
.super Ljava/lang/Object;
.source "IdentityVerificationSddPresenter_Factory.java"

# interfaces
.implements Lb/a/b;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lb/a/b",
        "<",
        "Lco/uk/getmondo/signup/identity_verification/sdd/f;",
        ">;"
    }
.end annotation


# static fields
.field static final synthetic a:Z


# instance fields
.field private final b:Lb/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lb/a",
            "<",
            "Lco/uk/getmondo/signup/identity_verification/sdd/f;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Ljavax/a/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/signup/identity_verification/sdd/j;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Ljavax/a/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/common/a;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Ljavax/a/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/a/a",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 10
    const-class v0, Lco/uk/getmondo/signup/identity_verification/sdd/i;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lco/uk/getmondo/signup/identity_verification/sdd/i;->a:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lb/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lb/a",
            "<",
            "Lco/uk/getmondo/signup/identity_verification/sdd/f;",
            ">;",
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/signup/identity_verification/sdd/j;",
            ">;",
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/common/a;",
            ">;",
            "Ljavax/a/a",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    sget-boolean v0, Lco/uk/getmondo/signup/identity_verification/sdd/i;->a:Z

    if-nez v0, :cond_0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 32
    :cond_0
    iput-object p1, p0, Lco/uk/getmondo/signup/identity_verification/sdd/i;->b:Lb/a;

    .line 34
    sget-boolean v0, Lco/uk/getmondo/signup/identity_verification/sdd/i;->a:Z

    if-nez v0, :cond_1

    if-nez p2, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 35
    :cond_1
    iput-object p2, p0, Lco/uk/getmondo/signup/identity_verification/sdd/i;->c:Ljavax/a/a;

    .line 36
    sget-boolean v0, Lco/uk/getmondo/signup/identity_verification/sdd/i;->a:Z

    if-nez v0, :cond_2

    if-nez p3, :cond_2

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 37
    :cond_2
    iput-object p3, p0, Lco/uk/getmondo/signup/identity_verification/sdd/i;->d:Ljavax/a/a;

    .line 38
    sget-boolean v0, Lco/uk/getmondo/signup/identity_verification/sdd/i;->a:Z

    if-nez v0, :cond_3

    if-nez p4, :cond_3

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 39
    :cond_3
    iput-object p4, p0, Lco/uk/getmondo/signup/identity_verification/sdd/i;->e:Ljavax/a/a;

    .line 40
    return-void
.end method

.method public static a(Lb/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;)Lb/a/b;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lb/a",
            "<",
            "Lco/uk/getmondo/signup/identity_verification/sdd/f;",
            ">;",
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/signup/identity_verification/sdd/j;",
            ">;",
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/common/a;",
            ">;",
            "Ljavax/a/a",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lb/a/b",
            "<",
            "Lco/uk/getmondo/signup/identity_verification/sdd/f;",
            ">;"
        }
    .end annotation

    .prologue
    .line 58
    new-instance v0, Lco/uk/getmondo/signup/identity_verification/sdd/i;

    invoke-direct {v0, p0, p1, p2, p3}, Lco/uk/getmondo/signup/identity_verification/sdd/i;-><init>(Lb/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;)V

    return-object v0
.end method


# virtual methods
.method public a()Lco/uk/getmondo/signup/identity_verification/sdd/f;
    .locals 5

    .prologue
    .line 44
    iget-object v3, p0, Lco/uk/getmondo/signup/identity_verification/sdd/i;->b:Lb/a;

    new-instance v4, Lco/uk/getmondo/signup/identity_verification/sdd/f;

    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/sdd/i;->c:Ljavax/a/a;

    .line 47
    invoke-interface {v0}, Ljavax/a/a;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/signup/identity_verification/sdd/j;

    iget-object v1, p0, Lco/uk/getmondo/signup/identity_verification/sdd/i;->d:Ljavax/a/a;

    .line 48
    invoke-interface {v1}, Ljavax/a/a;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lco/uk/getmondo/common/a;

    iget-object v2, p0, Lco/uk/getmondo/signup/identity_verification/sdd/i;->e:Ljavax/a/a;

    .line 49
    invoke-interface {v2}, Ljavax/a/a;->b()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-direct {v4, v0, v1, v2}, Lco/uk/getmondo/signup/identity_verification/sdd/f;-><init>(Lco/uk/getmondo/signup/identity_verification/sdd/j;Lco/uk/getmondo/common/a;Ljava/lang/String;)V

    .line 44
    invoke-static {v3, v4}, Lb/a/c;->a(Lb/a;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/signup/identity_verification/sdd/f;

    return-object v0
.end method

.method public synthetic b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 10
    invoke-virtual {p0}, Lco/uk/getmondo/signup/identity_verification/sdd/i;->a()Lco/uk/getmondo/signup/identity_verification/sdd/f;

    move-result-object v0

    return-object v0
.end method
