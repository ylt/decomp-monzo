.class public final enum Lco/uk/getmondo/signup/identity_verification/sdd/j;
.super Ljava/lang/Enum;
.source "SddUpgradeLevelType.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lco/uk/getmondo/signup/identity_verification/sdd/j;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u000b\u0008\u0086\u0001\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00000\u0001B\u001f\u0008\u0002\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\u0008R\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\t\u0010\nR\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000b\u0010\u000cR\u0011\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\r\u0010\u000ej\u0002\u0008\u000fj\u0002\u0008\u0010j\u0002\u0008\u0011\u00a8\u0006\u0012"
    }
    d2 = {
        "Lco/uk/getmondo/signup/identity_verification/sdd/SddUpgradeLevelType;",
        "",
        "impression",
        "Lco/uk/getmondo/api/model/tracking/Impression;",
        "from",
        "",
        "onboardingFrom",
        "Lco/uk/getmondo/api/model/tracking/Impression$KycFrom;",
        "(Ljava/lang/String;ILco/uk/getmondo/api/model/tracking/Impression;Ljava/lang/String;Lco/uk/getmondo/api/model/tracking/Impression$KycFrom;)V",
        "getFrom",
        "()Ljava/lang/String;",
        "getImpression",
        "()Lco/uk/getmondo/api/model/tracking/Impression;",
        "getOnboardingFrom",
        "()Lco/uk/getmondo/api/model/tracking/Impression$KycFrom;",
        "ENCOURAGE",
        "WARNING",
        "BLOCKED",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# static fields
.field public static final enum a:Lco/uk/getmondo/signup/identity_verification/sdd/j;

.field public static final enum b:Lco/uk/getmondo/signup/identity_verification/sdd/j;

.field public static final enum c:Lco/uk/getmondo/signup/identity_verification/sdd/j;

.field private static final synthetic d:[Lco/uk/getmondo/signup/identity_verification/sdd/j;


# instance fields
.field private final e:Lco/uk/getmondo/api/model/tracking/Impression;

.field private final f:Ljava/lang/String;

.field private final g:Lco/uk/getmondo/api/model/tracking/Impression$KycFrom;


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v2, 0x0

    const/4 v0, 0x3

    new-array v6, v0, [Lco/uk/getmondo/signup/identity_verification/sdd/j;

    new-instance v0, Lco/uk/getmondo/signup/identity_verification/sdd/j;

    const-string v1, "ENCOURAGE"

    .line 10
    sget-object v3, Lco/uk/getmondo/api/model/tracking/Impression;->Companion:Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    invoke-virtual {v3}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->az()Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v3

    const-string v4, "kyc-june26"

    sget-object v5, Lco/uk/getmondo/api/model/tracking/Impression$KycFrom;->KYC_SDD_ENCOURAGE:Lco/uk/getmondo/api/model/tracking/Impression$KycFrom;

    invoke-direct/range {v0 .. v5}, Lco/uk/getmondo/signup/identity_verification/sdd/j;-><init>(Ljava/lang/String;ILco/uk/getmondo/api/model/tracking/Impression;Ljava/lang/String;Lco/uk/getmondo/api/model/tracking/Impression$KycFrom;)V

    sput-object v0, Lco/uk/getmondo/signup/identity_verification/sdd/j;->a:Lco/uk/getmondo/signup/identity_verification/sdd/j;

    aput-object v0, v6, v2

    new-instance v0, Lco/uk/getmondo/signup/identity_verification/sdd/j;

    const-string v1, "WARNING"

    .line 12
    sget-object v2, Lco/uk/getmondo/api/model/tracking/Impression;->Companion:Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    invoke-virtual {v2}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->aA()Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v3

    const-string v4, "kyc-june26-warning"

    sget-object v5, Lco/uk/getmondo/api/model/tracking/Impression$KycFrom;->KYC_SDD_WARNING:Lco/uk/getmondo/api/model/tracking/Impression$KycFrom;

    move v2, v7

    invoke-direct/range {v0 .. v5}, Lco/uk/getmondo/signup/identity_verification/sdd/j;-><init>(Ljava/lang/String;ILco/uk/getmondo/api/model/tracking/Impression;Ljava/lang/String;Lco/uk/getmondo/api/model/tracking/Impression$KycFrom;)V

    sput-object v0, Lco/uk/getmondo/signup/identity_verification/sdd/j;->b:Lco/uk/getmondo/signup/identity_verification/sdd/j;

    aput-object v0, v6, v7

    new-instance v0, Lco/uk/getmondo/signup/identity_verification/sdd/j;

    const-string v1, "BLOCKED"

    .line 14
    sget-object v2, Lco/uk/getmondo/api/model/tracking/Impression;->Companion:Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    invoke-virtual {v2}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->aB()Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v3

    const-string v4, "kyc-june26-blocked"

    sget-object v5, Lco/uk/getmondo/api/model/tracking/Impression$KycFrom;->KYC_SDD_BLOCKED:Lco/uk/getmondo/api/model/tracking/Impression$KycFrom;

    move v2, v8

    invoke-direct/range {v0 .. v5}, Lco/uk/getmondo/signup/identity_verification/sdd/j;-><init>(Ljava/lang/String;ILco/uk/getmondo/api/model/tracking/Impression;Ljava/lang/String;Lco/uk/getmondo/api/model/tracking/Impression$KycFrom;)V

    sput-object v0, Lco/uk/getmondo/signup/identity_verification/sdd/j;->c:Lco/uk/getmondo/signup/identity_verification/sdd/j;

    aput-object v0, v6, v8

    sput-object v6, Lco/uk/getmondo/signup/identity_verification/sdd/j;->d:[Lco/uk/getmondo/signup/identity_verification/sdd/j;

    return-void
.end method

.method protected constructor <init>(Ljava/lang/String;ILco/uk/getmondo/api/model/tracking/Impression;Ljava/lang/String;Lco/uk/getmondo/api/model/tracking/Impression$KycFrom;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lco/uk/getmondo/api/model/tracking/Impression;",
            "Ljava/lang/String;",
            "Lco/uk/getmondo/api/model/tracking/Impression$KycFrom;",
            ")V"
        }
    .end annotation

    .prologue
    const-string v0, "impression"

    invoke-static {p3, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "from"

    invoke-static {p4, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onboardingFrom"

    invoke-static {p5, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 5
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput-object p3, p0, Lco/uk/getmondo/signup/identity_verification/sdd/j;->e:Lco/uk/getmondo/api/model/tracking/Impression;

    iput-object p4, p0, Lco/uk/getmondo/signup/identity_verification/sdd/j;->f:Ljava/lang/String;

    iput-object p5, p0, Lco/uk/getmondo/signup/identity_verification/sdd/j;->g:Lco/uk/getmondo/api/model/tracking/Impression$KycFrom;

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lco/uk/getmondo/signup/identity_verification/sdd/j;
    .locals 1

    const-class v0, Lco/uk/getmondo/signup/identity_verification/sdd/j;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/signup/identity_verification/sdd/j;

    return-object v0
.end method

.method public static values()[Lco/uk/getmondo/signup/identity_verification/sdd/j;
    .locals 1

    sget-object v0, Lco/uk/getmondo/signup/identity_verification/sdd/j;->d:[Lco/uk/getmondo/signup/identity_verification/sdd/j;

    invoke-virtual {v0}, [Lco/uk/getmondo/signup/identity_verification/sdd/j;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lco/uk/getmondo/signup/identity_verification/sdd/j;

    return-object v0
.end method


# virtual methods
.method public final a()Lco/uk/getmondo/api/model/tracking/Impression;
    .locals 1

    .prologue
    .line 6
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/sdd/j;->e:Lco/uk/getmondo/api/model/tracking/Impression;

    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 7
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/sdd/j;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final c()Lco/uk/getmondo/api/model/tracking/Impression$KycFrom;
    .locals 1

    .prologue
    .line 8
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/sdd/j;->g:Lco/uk/getmondo/api/model/tracking/Impression$KycFrom;

    return-object v0
.end method
