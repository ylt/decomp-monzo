.class final Lco/uk/getmondo/signup/identity_verification/v$c$2;
.super Ljava/lang/Object;
.source "IdentityVerificationPresenter.kt"

# interfaces
.implements Lio/reactivex/c/g;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lco/uk/getmondo/signup/identity_verification/v$c;->a(Lkotlin/n;)Lio/reactivex/h;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/g",
        "<",
        "Ljava/lang/Throwable;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u0003\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "",
        "it",
        "",
        "kotlin.jvm.PlatformType",
        "accept"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field final synthetic a:Lco/uk/getmondo/signup/identity_verification/v$c;


# direct methods
.method constructor <init>(Lco/uk/getmondo/signup/identity_verification/v$c;)V
    .locals 0

    iput-object p1, p0, Lco/uk/getmondo/signup/identity_verification/v$c$2;->a:Lco/uk/getmondo/signup/identity_verification/v$c;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 22
    check-cast p1, Ljava/lang/Throwable;

    invoke-virtual {p0, p1}, Lco/uk/getmondo/signup/identity_verification/v$c$2;->a(Ljava/lang/Throwable;)V

    return-void
.end method

.method public final a(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 51
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/v$c$2;->a:Lco/uk/getmondo/signup/identity_verification/v$c;

    iget-object v0, v0, Lco/uk/getmondo/signup/identity_verification/v$c;->b:Lco/uk/getmondo/signup/identity_verification/v$a;

    invoke-interface {v0}, Lco/uk/getmondo/signup/identity_verification/v$a;->g()V

    .line 52
    sget-object v1, Lco/uk/getmondo/signup/i;->a:Lco/uk/getmondo/signup/i;

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/v$c$2;->a:Lco/uk/getmondo/signup/identity_verification/v$c;

    iget-object v0, v0, Lco/uk/getmondo/signup/identity_verification/v$c;->b:Lco/uk/getmondo/signup/identity_verification/v$a;

    check-cast v0, Lco/uk/getmondo/signup/i$a;

    invoke-virtual {v1, p1, v0}, Lco/uk/getmondo/signup/i;->a(Ljava/lang/Throwable;Lco/uk/getmondo/signup/i$a;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/v$c$2;->a:Lco/uk/getmondo/signup/identity_verification/v$c;

    iget-object v0, v0, Lco/uk/getmondo/signup/identity_verification/v$c;->a:Lco/uk/getmondo/signup/identity_verification/v;

    invoke-static {v0}, Lco/uk/getmondo/signup/identity_verification/v;->e(Lco/uk/getmondo/signup/identity_verification/v;)Lco/uk/getmondo/common/e/a;

    move-result-object v1

    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/v$c$2;->a:Lco/uk/getmondo/signup/identity_verification/v$c;

    iget-object v0, v0, Lco/uk/getmondo/signup/identity_verification/v$c;->b:Lco/uk/getmondo/signup/identity_verification/v$a;

    check-cast v0, Lco/uk/getmondo/common/e/a$a;

    invoke-virtual {v1, p1, v0}, Lco/uk/getmondo/common/e/a;->a(Ljava/lang/Throwable;Lco/uk/getmondo/common/e/a$a;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 53
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/v$c$2;->a:Lco/uk/getmondo/signup/identity_verification/v$c;

    iget-object v0, v0, Lco/uk/getmondo/signup/identity_verification/v$c;->b:Lco/uk/getmondo/signup/identity_verification/v$a;

    const v1, 0x7f0a0196

    invoke-interface {v0, v1}, Lco/uk/getmondo/signup/identity_verification/v$a;->b(I)V

    .line 55
    :cond_0
    return-void
.end method
