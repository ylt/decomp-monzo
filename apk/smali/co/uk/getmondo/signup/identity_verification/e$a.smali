.class public final Lco/uk/getmondo/signup/identity_verification/e$a;
.super Ljava/lang/Object;
.source "IdentityDocumentsFragment.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lco/uk/getmondo/signup/identity_verification/e;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000<\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0002\u0008\u0003\n\u0002\u0010\u0008\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\u0008\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J0\u0010\u000c\u001a\u00020\r2\u0006\u0010\u000e\u001a\u00020\u000f2\u0006\u0010\u0010\u001a\u00020\u00112\u0008\u0010\u0012\u001a\u0004\u0018\u00010\u00042\u0006\u0010\u0013\u001a\u00020\u00142\u0006\u0010\u0015\u001a\u00020\u0016R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0082T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0004X\u0082T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0008X\u0082T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\u0004X\u0082T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u0004X\u0082T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u0004X\u0082T\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0017"
    }
    d2 = {
        "Lco/uk/getmondo/signup/identity_verification/IdentityDocumentsFragment$Companion;",
        "",
        "()V",
        "KEY_ALLOW_SYSTEM_CAMERA",
        "",
        "KEY_ENTRY_POINT",
        "KEY_REJECTION_REASON",
        "KEY_REQUEST_CODE_COUNTRY",
        "",
        "KEY_SIGNUP_SOURCE",
        "KEY_VERSION",
        "TAG_CONFIRM_VERIFY_IDENTITY",
        "newInstance",
        "Landroid/support/v4/app/Fragment;",
        "version",
        "Lco/uk/getmondo/signup/identity_verification/data/IdentityVerificationVersion;",
        "from",
        "Lco/uk/getmondo/api/model/tracking/Impression$KycFrom;",
        "rejectedReason",
        "allowSystemCamera",
        "",
        "signupSource",
        "Lco/uk/getmondo/api/model/signup/SignupSource;",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 303
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/d/b/i;)V
    .locals 0

    .prologue
    .line 303
    invoke-direct {p0}, Lco/uk/getmondo/signup/identity_verification/e$a;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lco/uk/getmondo/signup/identity_verification/a/j;Lco/uk/getmondo/api/model/tracking/Impression$KycFrom;Ljava/lang/String;ZLco/uk/getmondo/api/model/signup/SignupSource;)Landroid/support/v4/app/Fragment;
    .locals 3

    .prologue
    const-string v0, "version"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "from"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "signupSource"

    invoke-static {p5, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 321
    new-instance v0, Lco/uk/getmondo/signup/identity_verification/e;

    invoke-direct {v0}, Lco/uk/getmondo/signup/identity_verification/e;-><init>()V

    .line 322
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 323
    const-string v2, "KEY_VERSION"

    check-cast p1, Ljava/io/Serializable;

    invoke-virtual {v1, v2, p1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 324
    const-string v2, "KEY_ENTRY_POINT"

    check-cast p2, Ljava/io/Serializable;

    invoke-virtual {v1, v2, p2}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 325
    const-string v2, "KEY_REJECTION_REASON"

    invoke-virtual {v1, v2, p3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 326
    const-string v2, "KEY_ALLOW_SYSTEM_CAMERA"

    invoke-virtual {v1, v2, p4}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 327
    const-string v2, "KEY_SIGNUP_SOURCE"

    check-cast p5, Ljava/io/Serializable;

    invoke-virtual {v1, v2, p5}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 328
    nop

    .line 322
    invoke-virtual {v0, v1}, Lco/uk/getmondo/signup/identity_verification/e;->setArguments(Landroid/os/Bundle;)V

    .line 329
    check-cast v0, Landroid/support/v4/app/Fragment;

    return-object v0
.end method
