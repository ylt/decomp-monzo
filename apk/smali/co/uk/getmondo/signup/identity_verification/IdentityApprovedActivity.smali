.class public Lco/uk/getmondo/signup/identity_verification/IdentityApprovedActivity;
.super Lco/uk/getmondo/common/activities/b;
.source "IdentityApprovedActivity.java"


# instance fields
.field a:Lco/uk/getmondo/common/a;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Lco/uk/getmondo/common/activities/b;-><init>()V

    return-void
.end method

.method public static a(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 25
    invoke-static {p0}, Lco/uk/getmondo/signup/identity_verification/IdentityApprovedActivity;->b(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 26
    return-void
.end method

.method public static b(Landroid/content/Context;)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 29
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lco/uk/getmondo/signup/identity_verification/IdentityApprovedActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    return-object v0
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 34
    invoke-super {p0, p1}, Lco/uk/getmondo/common/activities/b;->onCreate(Landroid/os/Bundle;)V

    .line 35
    invoke-virtual {p0}, Lco/uk/getmondo/signup/identity_verification/IdentityApprovedActivity;->l()Lco/uk/getmondo/common/h/b/b;

    move-result-object v0

    invoke-interface {v0, p0}, Lco/uk/getmondo/common/h/b/b;->a(Lco/uk/getmondo/signup/identity_verification/IdentityApprovedActivity;)V

    .line 36
    const v0, 0x7f050042

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/identity_verification/IdentityApprovedActivity;->setContentView(I)V

    .line 37
    invoke-static {p0}, Lbutterknife/ButterKnife;->bind(Landroid/app/Activity;)Lbutterknife/Unbinder;

    .line 39
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/IdentityApprovedActivity;->a:Lco/uk/getmondo/common/a;

    invoke-static {}, Lco/uk/getmondo/api/model/tracking/Impression;->E()Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v1

    invoke-virtual {v0, v1}, Lco/uk/getmondo/common/a;->a(Lco/uk/getmondo/api/model/tracking/Impression;)V

    .line 40
    return-void
.end method

.method public onReturnToHomeClicked()V
    .locals 0
    .annotation build Lbutterknife/OnClick;
        value = {
            0x7f1101a5
        }
    .end annotation

    .prologue
    .line 44
    invoke-virtual {p0}, Lco/uk/getmondo/signup/identity_verification/IdentityApprovedActivity;->finish()V

    .line 45
    invoke-static {p0}, Lco/uk/getmondo/main/HomeActivity;->a(Landroid/content/Context;)V

    .line 46
    return-void
.end method
