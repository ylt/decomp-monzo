.class public final Lco/uk/getmondo/signup/identity_verification/v;
.super Lco/uk/getmondo/common/ui/b;
.source "IdentityVerificationPresenter.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lco/uk/getmondo/signup/identity_verification/v$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lco/uk/getmondo/common/ui/b",
        "<",
        "Lco/uk/getmondo/signup/identity_verification/v$a;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u00008\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0003\u0008\u0007\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00020\u0001:\u0001\u0012B3\u0008\u0007\u0012\u0008\u0008\u0001\u0010\u0003\u001a\u00020\u0004\u0012\u0008\u0008\u0001\u0010\u0005\u001a\u00020\u0004\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b\u00a2\u0006\u0002\u0010\u000cJ\u0010\u0010\u000f\u001a\u00020\u00102\u0006\u0010\u0011\u001a\u00020\u0002H\u0016R\u000e\u0010\r\u001a\u00020\u000eX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0013"
    }
    d2 = {
        "Lco/uk/getmondo/signup/identity_verification/IdentityVerificationPresenter;",
        "Lco/uk/getmondo/common/ui/BasePresenter;",
        "Lco/uk/getmondo/signup/identity_verification/IdentityVerificationPresenter$View;",
        "ioScheduler",
        "Lio/reactivex/Scheduler;",
        "uiScheduler",
        "apiErrorHandler",
        "Lco/uk/getmondo/common/errors/ApiErrorHandler;",
        "verificationManager",
        "Lco/uk/getmondo/signup/identity_verification/data/IdentityVerificationManager;",
        "profileManager",
        "Lco/uk/getmondo/profile/data/ProfileManager;",
        "(Lio/reactivex/Scheduler;Lio/reactivex/Scheduler;Lco/uk/getmondo/common/errors/ApiErrorHandler;Lco/uk/getmondo/signup/identity_verification/data/IdentityVerificationManager;Lco/uk/getmondo/profile/data/ProfileManager;)V",
        "allowSystemCamera",
        "",
        "register",
        "",
        "view",
        "View",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field private c:Z

.field private final d:Lio/reactivex/u;

.field private final e:Lio/reactivex/u;

.field private final f:Lco/uk/getmondo/common/e/a;

.field private final g:Lco/uk/getmondo/signup/identity_verification/a/e;

.field private final h:Lco/uk/getmondo/profile/data/a;


# direct methods
.method public constructor <init>(Lio/reactivex/u;Lio/reactivex/u;Lco/uk/getmondo/common/e/a;Lco/uk/getmondo/signup/identity_verification/a/e;Lco/uk/getmondo/profile/data/a;)V
    .locals 1

    .prologue
    const-string v0, "ioScheduler"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "uiScheduler"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "apiErrorHandler"

    invoke-static {p3, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "verificationManager"

    invoke-static {p4, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "profileManager"

    invoke-static {p5, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 28
    invoke-direct {p0}, Lco/uk/getmondo/common/ui/b;-><init>()V

    iput-object p1, p0, Lco/uk/getmondo/signup/identity_verification/v;->d:Lio/reactivex/u;

    iput-object p2, p0, Lco/uk/getmondo/signup/identity_verification/v;->e:Lio/reactivex/u;

    iput-object p3, p0, Lco/uk/getmondo/signup/identity_verification/v;->f:Lco/uk/getmondo/common/e/a;

    iput-object p4, p0, Lco/uk/getmondo/signup/identity_verification/v;->g:Lco/uk/getmondo/signup/identity_verification/a/e;

    iput-object p5, p0, Lco/uk/getmondo/signup/identity_verification/v;->h:Lco/uk/getmondo/profile/data/a;

    return-void
.end method

.method public static final synthetic a(Lco/uk/getmondo/signup/identity_verification/v;)Lco/uk/getmondo/signup/identity_verification/a/e;
    .locals 1

    .prologue
    .line 22
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/v;->g:Lco/uk/getmondo/signup/identity_verification/a/e;

    return-object v0
.end method

.method public static final synthetic a(Lco/uk/getmondo/signup/identity_verification/v;Z)V
    .locals 0

    .prologue
    .line 22
    iput-boolean p1, p0, Lco/uk/getmondo/signup/identity_verification/v;->c:Z

    return-void
.end method

.method public static final synthetic b(Lco/uk/getmondo/signup/identity_verification/v;)Lco/uk/getmondo/profile/data/a;
    .locals 1

    .prologue
    .line 22
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/v;->h:Lco/uk/getmondo/profile/data/a;

    return-object v0
.end method

.method public static final synthetic c(Lco/uk/getmondo/signup/identity_verification/v;)Lio/reactivex/u;
    .locals 1

    .prologue
    .line 22
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/v;->d:Lio/reactivex/u;

    return-object v0
.end method

.method public static final synthetic d(Lco/uk/getmondo/signup/identity_verification/v;)Lio/reactivex/u;
    .locals 1

    .prologue
    .line 22
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/v;->e:Lio/reactivex/u;

    return-object v0
.end method

.method public static final synthetic e(Lco/uk/getmondo/signup/identity_verification/v;)Lco/uk/getmondo/common/e/a;
    .locals 1

    .prologue
    .line 22
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/v;->f:Lco/uk/getmondo/common/e/a;

    return-object v0
.end method

.method public static final synthetic f(Lco/uk/getmondo/signup/identity_verification/v;)Z
    .locals 1

    .prologue
    .line 22
    iget-boolean v0, p0, Lco/uk/getmondo/signup/identity_verification/v;->c:Z

    return v0
.end method


# virtual methods
.method public bridge synthetic a(Lco/uk/getmondo/common/ui/f;)V
    .locals 0

    .prologue
    .line 22
    check-cast p1, Lco/uk/getmondo/signup/identity_verification/v$a;

    invoke-virtual {p0, p1}, Lco/uk/getmondo/signup/identity_verification/v;->a(Lco/uk/getmondo/signup/identity_verification/v$a;)V

    return-void
.end method

.method public a(Lco/uk/getmondo/signup/identity_verification/v$a;)V
    .locals 3

    .prologue
    const-string v0, "view"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v0, p1

    .line 32
    check-cast v0, Lco/uk/getmondo/common/ui/f;

    invoke-super {p0, v0}, Lco/uk/getmondo/common/ui/b;->a(Lco/uk/getmondo/common/ui/f;)V

    .line 34
    iget-object v1, p0, Lco/uk/getmondo/signup/identity_verification/v;->b:Lio/reactivex/b/a;

    .line 58
    invoke-interface {p1}, Lco/uk/getmondo/signup/identity_verification/v$a;->c()Lio/reactivex/n;

    move-result-object v0

    .line 35
    sget-object v2, Lkotlin/n;->a:Lkotlin/n;

    invoke-virtual {v0, v2}, Lio/reactivex/n;->startWith(Ljava/lang/Object;)Lio/reactivex/n;

    move-result-object v2

    .line 36
    new-instance v0, Lco/uk/getmondo/signup/identity_verification/v$b;

    invoke-direct {v0, p1}, Lco/uk/getmondo/signup/identity_verification/v$b;-><init>(Lco/uk/getmondo/signup/identity_verification/v$a;)V

    check-cast v0, Lio/reactivex/c/g;

    invoke-virtual {v2, v0}, Lio/reactivex/n;->doOnSubscribe(Lio/reactivex/c/g;)Lio/reactivex/n;

    move-result-object v2

    .line 37
    new-instance v0, Lco/uk/getmondo/signup/identity_verification/v$c;

    invoke-direct {v0, p0, p1}, Lco/uk/getmondo/signup/identity_verification/v$c;-><init>(Lco/uk/getmondo/signup/identity_verification/v;Lco/uk/getmondo/signup/identity_verification/v$a;)V

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {v2, v0}, Lio/reactivex/n;->flatMapMaybe(Lio/reactivex/c/h;)Lio/reactivex/n;

    move-result-object v2

    .line 58
    new-instance v0, Lco/uk/getmondo/signup/identity_verification/v$d;

    invoke-direct {v0, p0, p1}, Lco/uk/getmondo/signup/identity_verification/v$d;-><init>(Lco/uk/getmondo/signup/identity_verification/v;Lco/uk/getmondo/signup/identity_verification/v$a;)V

    check-cast v0, Lio/reactivex/c/g;

    invoke-virtual {v2, v0}, Lio/reactivex/n;->subscribe(Lio/reactivex/c/g;)Lio/reactivex/b/b;

    move-result-object v0

    const-string v2, "view.onRetryClicked\n    \u2026      }\n                }"

    invoke-static {v0, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 34
    invoke-static {v1, v0}, Lco/uk/getmondo/common/j/f;->a(Lio/reactivex/b/a;Lio/reactivex/b/b;)Lio/reactivex/b/a;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/signup/identity_verification/v;->b:Lio/reactivex/b/a;

    .line 70
    iget-object v1, p0, Lco/uk/getmondo/signup/identity_verification/v;->b:Lio/reactivex/b/a;

    .line 71
    invoke-interface {p1}, Lco/uk/getmondo/signup/identity_verification/v$a;->e()Lio/reactivex/n;

    move-result-object v2

    new-instance v0, Lco/uk/getmondo/signup/identity_verification/v$e;

    invoke-direct {v0, p0, p1}, Lco/uk/getmondo/signup/identity_verification/v$e;-><init>(Lco/uk/getmondo/signup/identity_verification/v;Lco/uk/getmondo/signup/identity_verification/v$a;)V

    check-cast v0, Lio/reactivex/c/g;

    invoke-virtual {v2, v0}, Lio/reactivex/n;->subscribe(Lio/reactivex/c/g;)Lio/reactivex/b/b;

    move-result-object v0

    const-string v2, "view.onOnboardingComplet\u2026llowSystemCamera, null) }"

    invoke-static {v0, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 70
    invoke-static {v1, v0}, Lco/uk/getmondo/common/j/f;->a(Lio/reactivex/b/a;Lio/reactivex/b/b;)Lio/reactivex/b/a;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/signup/identity_verification/v;->b:Lio/reactivex/b/a;

    .line 72
    return-void
.end method
