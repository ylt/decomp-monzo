.class final Lco/uk/getmondo/signup/identity_verification/g$m;
.super Ljava/lang/Object;
.source "IdentityDocumentsPresenter.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lco/uk/getmondo/signup/identity_verification/g;->a(Lco/uk/getmondo/signup/identity_verification/g$a;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h",
        "<TT;",
        "Lio/reactivex/l",
        "<+TR;>;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000\u0016\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u0003\u0010\u0000\u001aV\u0012$\u0012\"\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u0004 \u0005*\u0010\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u0004\u0018\u00010\u00020\u0002 \u0005**\u0012$\u0012\"\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u0004 \u0005*\u0010\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u0004\u0018\u00010\u00020\u0002\u0018\u00010\u00010\u00012\u0006\u0010\u0006\u001a\u00020\u0003H\n\u00a2\u0006\u0002\u0008\u0007"
    }
    d2 = {
        "<anonymous>",
        "Lio/reactivex/Maybe;",
        "Lkotlin/Pair;",
        "Lco/uk/getmondo/api/model/identity_verification/IdentityDocumentType;",
        "",
        "kotlin.jvm.PlatformType",
        "documentType",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field final synthetic a:Lco/uk/getmondo/signup/identity_verification/g$a;


# direct methods
.method constructor <init>(Lco/uk/getmondo/signup/identity_verification/g$a;)V
    .locals 0

    iput-object p1, p0, Lco/uk/getmondo/signup/identity_verification/g$m;->a:Lco/uk/getmondo/signup/identity_verification/g$a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lco/uk/getmondo/api/model/identity_verification/IdentityDocumentType;)Lio/reactivex/h;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lco/uk/getmondo/api/model/identity_verification/IdentityDocumentType;",
            ")",
            "Lio/reactivex/h",
            "<",
            "Lkotlin/h",
            "<",
            "Lco/uk/getmondo/api/model/identity_verification/IdentityDocumentType;",
            "Ljava/lang/Boolean;",
            ">;>;"
        }
    .end annotation

    .prologue
    const-string v0, "documentType"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 60
    sget-object v0, Lco/uk/getmondo/api/model/identity_verification/IdentityDocumentType;->NATIONAL_ID:Lco/uk/getmondo/api/model/identity_verification/IdentityDocumentType;

    if-ne p1, v0, :cond_0

    .line 62
    new-instance v0, Lkotlin/h;

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-direct {v0, p1, v1}, Lkotlin/h;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-static {v0}, Lio/reactivex/h;->a(Ljava/lang/Object;)Lio/reactivex/h;

    move-result-object v0

    .line 60
    :goto_0
    return-object v0

    .line 64
    :cond_0
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/g$m;->a:Lco/uk/getmondo/signup/identity_verification/g$a;

    invoke-interface {v0, p1}, Lco/uk/getmondo/signup/identity_verification/g$a;->a(Lco/uk/getmondo/api/model/identity_verification/IdentityDocumentType;)Lio/reactivex/h;

    move-result-object v1

    new-instance v0, Lco/uk/getmondo/signup/identity_verification/g$m$1;

    invoke-direct {v0, p1}, Lco/uk/getmondo/signup/identity_verification/g$m$1;-><init>(Lco/uk/getmondo/api/model/identity_verification/IdentityDocumentType;)V

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {v1, v0}, Lio/reactivex/h;->c(Lio/reactivex/c/h;)Lio/reactivex/h;

    move-result-object v0

    goto :goto_0
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 26
    check-cast p1, Lco/uk/getmondo/api/model/identity_verification/IdentityDocumentType;

    invoke-virtual {p0, p1}, Lco/uk/getmondo/signup/identity_verification/g$m;->a(Lco/uk/getmondo/api/model/identity_verification/IdentityDocumentType;)Lio/reactivex/h;

    move-result-object v0

    return-object v0
.end method
