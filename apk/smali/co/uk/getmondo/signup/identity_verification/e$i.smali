.class final Lco/uk/getmondo/signup/identity_verification/e$i;
.super Lkotlin/d/b/m;
.source "IdentityDocumentsFragment.kt"

# interfaces
.implements Lkotlin/d/a/b;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lco/uk/getmondo/signup/identity_verification/e;->a(Lco/uk/getmondo/api/model/identity_verification/IdentityDocumentType;)Lio/reactivex/h;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/d/b/m;",
        "Lkotlin/d/a/b",
        "<",
        "Lkotlin/d/a/b",
        "<-",
        "Ljava/lang/Boolean;",
        "+",
        "Lkotlin/n;",
        ">;",
        "Landroid/support/v7/app/d;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000\u0018\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0002\u0010\u0002\n\u0000\u0010\u0000\u001a\n \u0002*\u0004\u0018\u00010\u00010\u00012\u0012\u0010\u0003\u001a\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0004H\n\u00a2\u0006\u0002\u0008\u0007"
    }
    d2 = {
        "<anonymous>",
        "Landroid/support/v7/app/AlertDialog;",
        "kotlin.jvm.PlatformType",
        "setResult",
        "Lkotlin/Function1;",
        "",
        "",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field final synthetic a:Lco/uk/getmondo/signup/identity_verification/e;

.field final synthetic b:Ljava/lang/String;


# direct methods
.method constructor <init>(Lco/uk/getmondo/signup/identity_verification/e;Ljava/lang/String;)V
    .locals 1

    iput-object p1, p0, Lco/uk/getmondo/signup/identity_verification/e$i;->a:Lco/uk/getmondo/signup/identity_verification/e;

    iput-object p2, p0, Lco/uk/getmondo/signup/identity_verification/e$i;->b:Ljava/lang/String;

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lkotlin/d/b/m;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final a(Lkotlin/d/a/b;)Landroid/support/v7/app/d;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/d/a/b",
            "<-",
            "Ljava/lang/Boolean;",
            "Lkotlin/n;",
            ">;)",
            "Landroid/support/v7/app/d;"
        }
    .end annotation

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    const-string v0, "setResult"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 202
    new-instance v1, Landroid/support/v7/app/d$a;

    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/e$i;->a:Lco/uk/getmondo/signup/identity_verification/e;

    invoke-virtual {v0}, Lco/uk/getmondo/signup/identity_verification/e;->getContext()Landroid/content/Context;

    move-result-object v0

    const v2, 0x7f0c010f

    invoke-direct {v1, v0, v2}, Landroid/support/v7/app/d$a;-><init>(Landroid/content/Context;I)V

    .line 203
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/e$i;->a:Lco/uk/getmondo/signup/identity_verification/e;

    const v2, 0x7f0a0171

    new-array v3, v6, [Ljava/lang/Object;

    iget-object v4, p0, Lco/uk/getmondo/signup/identity_verification/e$i;->b:Ljava/lang/String;

    invoke-static {v4}, Lco/uk/getmondo/common/k/p;->i(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-virtual {v0, v2, v3}, Lco/uk/getmondo/signup/identity_verification/e;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-virtual {v1, v0}, Landroid/support/v7/app/d$a;->a(Ljava/lang/CharSequence;)Landroid/support/v7/app/d$a;

    move-result-object v1

    .line 204
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/e$i;->a:Lco/uk/getmondo/signup/identity_verification/e;

    const v2, 0x7f0a0170

    new-array v3, v6, [Ljava/lang/Object;

    iget-object v4, p0, Lco/uk/getmondo/signup/identity_verification/e$i;->b:Ljava/lang/String;

    aput-object v4, v3, v5

    invoke-virtual {v0, v2, v3}, Lco/uk/getmondo/signup/identity_verification/e;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-virtual {v1, v0}, Landroid/support/v7/app/d$a;->b(Ljava/lang/CharSequence;)Landroid/support/v7/app/d$a;

    move-result-object v1

    .line 205
    const v2, 0x7f0a016f

    new-instance v0, Lco/uk/getmondo/signup/identity_verification/e$i$1;

    invoke-direct {v0, p1}, Lco/uk/getmondo/signup/identity_verification/e$i$1;-><init>(Lkotlin/d/a/b;)V

    check-cast v0, Landroid/content/DialogInterface$OnClickListener;

    invoke-virtual {v1, v2, v0}, Landroid/support/v7/app/d$a;->a(ILandroid/content/DialogInterface$OnClickListener;)Landroid/support/v7/app/d$a;

    move-result-object v1

    .line 206
    const v2, 0x7f0a016e

    new-instance v0, Lco/uk/getmondo/signup/identity_verification/e$i$2;

    invoke-direct {v0, p1}, Lco/uk/getmondo/signup/identity_verification/e$i$2;-><init>(Lkotlin/d/a/b;)V

    check-cast v0, Landroid/content/DialogInterface$OnClickListener;

    invoke-virtual {v1, v2, v0}, Landroid/support/v7/app/d$a;->b(ILandroid/content/DialogInterface$OnClickListener;)Landroid/support/v7/app/d$a;

    move-result-object v0

    .line 207
    invoke-virtual {v0, v5}, Landroid/support/v7/app/d$a;->a(Z)Landroid/support/v7/app/d$a;

    move-result-object v0

    .line 208
    invoke-virtual {v0}, Landroid/support/v7/app/d$a;->b()Landroid/support/v7/app/d;

    move-result-object v0

    const-string v1, "AlertDialog.Builder(cont\u2026                .create()"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 44
    check-cast p1, Lkotlin/d/a/b;

    invoke-virtual {p0, p1}, Lco/uk/getmondo/signup/identity_verification/e$i;->a(Lkotlin/d/a/b;)Landroid/support/v7/app/d;

    move-result-object v0

    return-object v0
.end method
