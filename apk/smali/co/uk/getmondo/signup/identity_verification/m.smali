.class public final Lco/uk/getmondo/signup/identity_verification/m;
.super Ljava/lang/Object;
.source "IdentityVerificationActivityModule_ProvidesRejectionReasonFactory.java"

# interfaces
.implements Lb/a/b;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lb/a/b",
        "<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# static fields
.field static final synthetic a:Z


# instance fields
.field private final b:Lco/uk/getmondo/signup/identity_verification/j;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 7
    const-class v0, Lco/uk/getmondo/signup/identity_verification/m;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lco/uk/getmondo/signup/identity_verification/m;->a:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lco/uk/getmondo/signup/identity_verification/j;)V
    .locals 1

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    sget-boolean v0, Lco/uk/getmondo/signup/identity_verification/m;->a:Z

    if-nez v0, :cond_0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 18
    :cond_0
    iput-object p1, p0, Lco/uk/getmondo/signup/identity_verification/m;->b:Lco/uk/getmondo/signup/identity_verification/j;

    .line 19
    return-void
.end method

.method public static a(Lco/uk/getmondo/signup/identity_verification/j;)Lb/a/b;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lco/uk/getmondo/signup/identity_verification/j;",
            ")",
            "Lb/a/b",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 28
    new-instance v0, Lco/uk/getmondo/signup/identity_verification/m;

    invoke-direct {v0, p0}, Lco/uk/getmondo/signup/identity_verification/m;-><init>(Lco/uk/getmondo/signup/identity_verification/j;)V

    return-object v0
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 24
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/m;->b:Lco/uk/getmondo/signup/identity_verification/j;

    invoke-virtual {v0}, Lco/uk/getmondo/signup/identity_verification/j;->b()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public synthetic b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 7
    invoke-virtual {p0}, Lco/uk/getmondo/signup/identity_verification/m;->a()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
