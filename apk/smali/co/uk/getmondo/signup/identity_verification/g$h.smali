.class final Lco/uk/getmondo/signup/identity_verification/g$h;
.super Ljava/lang/Object;
.source "IdentityDocumentsPresenter.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lco/uk/getmondo/signup/identity_verification/g;->a(Lco/uk/getmondo/signup/identity_verification/g$a;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h",
        "<TT;",
        "Lio/reactivex/l",
        "<+TR;>;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000\u0014\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0010\u0000\n\u0000\u0010\u0000\u001a&\u0012\u000c\u0012\n \u0003*\u0004\u0018\u00010\u00020\u0002 \u0003*\u0012\u0012\u000c\u0012\n \u0003*\u0004\u0018\u00010\u00020\u0002\u0018\u00010\u00010\u00012\u0006\u0010\u0004\u001a\u00020\u0005H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "Lio/reactivex/Maybe;",
        "",
        "kotlin.jvm.PlatformType",
        "it",
        "",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field final synthetic a:Lco/uk/getmondo/signup/identity_verification/g;

.field final synthetic b:Lco/uk/getmondo/signup/identity_verification/g$a;


# direct methods
.method constructor <init>(Lco/uk/getmondo/signup/identity_verification/g;Lco/uk/getmondo/signup/identity_verification/g$a;)V
    .locals 0

    iput-object p1, p0, Lco/uk/getmondo/signup/identity_verification/g$h;->a:Lco/uk/getmondo/signup/identity_verification/g;

    iput-object p2, p0, Lco/uk/getmondo/signup/identity_verification/g$h;->b:Lco/uk/getmondo/signup/identity_verification/g$a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 26
    invoke-virtual {p0, p1}, Lco/uk/getmondo/signup/identity_verification/g$h;->b(Ljava/lang/Object;)Lio/reactivex/h;

    move-result-object v0

    return-object v0
.end method

.method public final b(Ljava/lang/Object;)Lio/reactivex/h;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            ")",
            "Lio/reactivex/h",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .prologue
    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 135
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/g$h;->a:Lco/uk/getmondo/signup/identity_verification/g;

    invoke-static {v0}, Lco/uk/getmondo/signup/identity_verification/g;->f(Lco/uk/getmondo/signup/identity_verification/g;)Lco/uk/getmondo/signup/identity_verification/a/e;

    move-result-object v0

    invoke-interface {v0}, Lco/uk/getmondo/signup/identity_verification/a/e;->a()Lio/reactivex/v;

    move-result-object v0

    .line 136
    invoke-virtual {v0}, Lio/reactivex/v;->e()Lio/reactivex/h;

    move-result-object v0

    .line 137
    iget-object v1, p0, Lco/uk/getmondo/signup/identity_verification/g$h;->a:Lco/uk/getmondo/signup/identity_verification/g;

    invoke-static {v1}, Lco/uk/getmondo/signup/identity_verification/g;->g(Lco/uk/getmondo/signup/identity_verification/g;)Lio/reactivex/u;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/h;->b(Lio/reactivex/u;)Lio/reactivex/h;

    move-result-object v0

    .line 138
    iget-object v1, p0, Lco/uk/getmondo/signup/identity_verification/g$h;->a:Lco/uk/getmondo/signup/identity_verification/g;

    invoke-static {v1}, Lco/uk/getmondo/signup/identity_verification/g;->h(Lco/uk/getmondo/signup/identity_verification/g;)Lio/reactivex/u;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/h;->a(Lio/reactivex/u;)Lio/reactivex/h;

    move-result-object v1

    .line 139
    new-instance v0, Lco/uk/getmondo/signup/identity_verification/g$h$1;

    invoke-direct {v0, p0}, Lco/uk/getmondo/signup/identity_verification/g$h$1;-><init>(Lco/uk/getmondo/signup/identity_verification/g$h;)V

    check-cast v0, Lio/reactivex/c/g;

    invoke-virtual {v1, v0}, Lio/reactivex/h;->b(Lio/reactivex/c/g;)Lio/reactivex/h;

    move-result-object v1

    .line 140
    new-instance v0, Lco/uk/getmondo/signup/identity_verification/g$h$2;

    invoke-direct {v0, p0}, Lco/uk/getmondo/signup/identity_verification/g$h$2;-><init>(Lco/uk/getmondo/signup/identity_verification/g$h;)V

    check-cast v0, Lio/reactivex/c/g;

    invoke-virtual {v1, v0}, Lio/reactivex/h;->a(Lio/reactivex/c/g;)Lio/reactivex/h;

    move-result-object v0

    .line 146
    invoke-virtual {v0}, Lio/reactivex/h;->f()Lio/reactivex/h;

    move-result-object v1

    .line 147
    new-instance v0, Lco/uk/getmondo/signup/identity_verification/g$h$3;

    invoke-direct {v0, p0}, Lco/uk/getmondo/signup/identity_verification/g$h$3;-><init>(Lco/uk/getmondo/signup/identity_verification/g$h;)V

    check-cast v0, Lio/reactivex/c/b;

    invoke-virtual {v1, v0}, Lio/reactivex/h;->a(Lio/reactivex/c/b;)Lio/reactivex/h;

    move-result-object v0

    return-object v0
.end method
