.class public final enum Lco/uk/getmondo/signup/identity_verification/y;
.super Ljava/lang/Enum;
.source "KycRejectedReason.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lco/uk/getmondo/signup/identity_verification/y$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lco/uk/getmondo/signup/identity_verification/y;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0013\u0008\u0086\u0001\u0018\u0000 \u00172\u0008\u0012\u0004\u0012\u00020\u00000\u0001:\u0001\u0017B\u0019\u0008\u0002\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0008\u0008\u0001\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0007\u0010\u0008R\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\t\u0010\nj\u0002\u0008\u000bj\u0002\u0008\u000cj\u0002\u0008\rj\u0002\u0008\u000ej\u0002\u0008\u000fj\u0002\u0008\u0010j\u0002\u0008\u0011j\u0002\u0008\u0012j\u0002\u0008\u0013j\u0002\u0008\u0014j\u0002\u0008\u0015j\u0002\u0008\u0016\u00a8\u0006\u0018"
    }
    d2 = {
        "Lco/uk/getmondo/signup/identity_verification/KycRejectedReason;",
        "",
        "apiValue",
        "",
        "rejectedReasonResId",
        "",
        "(Ljava/lang/String;ILjava/lang/String;I)V",
        "getApiValue",
        "()Ljava/lang/String;",
        "getRejectedReasonResId",
        "()I",
        "UNKNOWN",
        "DARK_PHOTO",
        "BLURRY_PHOTO",
        "PARTIAL_PHOTO",
        "REFLECTION_IN_PHOTO",
        "DARK_VIDEO",
        "NO_SOUND",
        "EXPIRED_DOCUMENT",
        "NOT_GOV_ID",
        "NO_DOB",
        "NO_EXPIRY_DATE",
        "OTHER",
        "Find",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# static fields
.field public static final enum a:Lco/uk/getmondo/signup/identity_verification/y;

.field public static final enum b:Lco/uk/getmondo/signup/identity_verification/y;

.field public static final enum c:Lco/uk/getmondo/signup/identity_verification/y;

.field public static final enum d:Lco/uk/getmondo/signup/identity_verification/y;

.field public static final enum e:Lco/uk/getmondo/signup/identity_verification/y;

.field public static final enum f:Lco/uk/getmondo/signup/identity_verification/y;

.field public static final enum g:Lco/uk/getmondo/signup/identity_verification/y;

.field public static final enum h:Lco/uk/getmondo/signup/identity_verification/y;

.field public static final enum i:Lco/uk/getmondo/signup/identity_verification/y;

.field public static final enum j:Lco/uk/getmondo/signup/identity_verification/y;

.field public static final enum k:Lco/uk/getmondo/signup/identity_verification/y;

.field public static final enum l:Lco/uk/getmondo/signup/identity_verification/y;

.field public static final m:Lco/uk/getmondo/signup/identity_verification/y$a;

.field private static final synthetic n:[Lco/uk/getmondo/signup/identity_verification/y;


# instance fields
.field private final o:Ljava/lang/String;

.field private final p:I


# direct methods
.method static constructor <clinit>()V
    .locals 10

    .prologue
    const/4 v9, 0x4

    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    const/16 v0, 0xc

    new-array v0, v0, [Lco/uk/getmondo/signup/identity_verification/y;

    new-instance v1, Lco/uk/getmondo/signup/identity_verification/y;

    const-string v2, "UNKNOWN"

    .line 7
    const-string v3, ""

    const v4, 0x7f0a023e

    invoke-direct {v1, v2, v5, v3, v4}, Lco/uk/getmondo/signup/identity_verification/y;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v1, Lco/uk/getmondo/signup/identity_verification/y;->a:Lco/uk/getmondo/signup/identity_verification/y;

    aput-object v1, v0, v5

    new-instance v1, Lco/uk/getmondo/signup/identity_verification/y;

    const-string v2, "DARK_PHOTO"

    .line 8
    const-string v3, "DARK_PHOTO"

    const v4, 0x7f0a0234

    invoke-direct {v1, v2, v6, v3, v4}, Lco/uk/getmondo/signup/identity_verification/y;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v1, Lco/uk/getmondo/signup/identity_verification/y;->b:Lco/uk/getmondo/signup/identity_verification/y;

    aput-object v1, v0, v6

    new-instance v1, Lco/uk/getmondo/signup/identity_verification/y;

    const-string v2, "BLURRY_PHOTO"

    .line 9
    const-string v3, "BLURRY_PHOTO"

    const v4, 0x7f0a0233

    invoke-direct {v1, v2, v7, v3, v4}, Lco/uk/getmondo/signup/identity_verification/y;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v1, Lco/uk/getmondo/signup/identity_verification/y;->c:Lco/uk/getmondo/signup/identity_verification/y;

    aput-object v1, v0, v7

    new-instance v1, Lco/uk/getmondo/signup/identity_verification/y;

    const-string v2, "PARTIAL_PHOTO"

    .line 10
    const-string v3, "PARTIAL_PHOTO"

    const v4, 0x7f0a023c

    invoke-direct {v1, v2, v8, v3, v4}, Lco/uk/getmondo/signup/identity_verification/y;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v1, Lco/uk/getmondo/signup/identity_verification/y;->d:Lco/uk/getmondo/signup/identity_verification/y;

    aput-object v1, v0, v8

    new-instance v1, Lco/uk/getmondo/signup/identity_verification/y;

    const-string v2, "REFLECTION_IN_PHOTO"

    .line 11
    const-string v3, "REFLECTION_IN_PHOTO"

    const v4, 0x7f0a023d

    invoke-direct {v1, v2, v9, v3, v4}, Lco/uk/getmondo/signup/identity_verification/y;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v1, Lco/uk/getmondo/signup/identity_verification/y;->e:Lco/uk/getmondo/signup/identity_verification/y;

    aput-object v1, v0, v9

    const/4 v1, 0x5

    new-instance v2, Lco/uk/getmondo/signup/identity_verification/y;

    const-string v3, "DARK_VIDEO"

    const/4 v4, 0x5

    .line 12
    const-string v5, "DARK_VIDEO"

    const v6, 0x7f0a0235

    invoke-direct {v2, v3, v4, v5, v6}, Lco/uk/getmondo/signup/identity_verification/y;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v2, Lco/uk/getmondo/signup/identity_verification/y;->f:Lco/uk/getmondo/signup/identity_verification/y;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    new-instance v2, Lco/uk/getmondo/signup/identity_verification/y;

    const-string v3, "NO_SOUND"

    const/4 v4, 0x6

    .line 13
    const-string v5, "NO_SOUND"

    const v6, 0x7f0a023a

    invoke-direct {v2, v3, v4, v5, v6}, Lco/uk/getmondo/signup/identity_verification/y;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v2, Lco/uk/getmondo/signup/identity_verification/y;->g:Lco/uk/getmondo/signup/identity_verification/y;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    new-instance v2, Lco/uk/getmondo/signup/identity_verification/y;

    const-string v3, "EXPIRED_DOCUMENT"

    const/4 v4, 0x7

    .line 14
    const-string v5, "EXPIRED_DOCUMENT"

    const v6, 0x7f0a0236

    invoke-direct {v2, v3, v4, v5, v6}, Lco/uk/getmondo/signup/identity_verification/y;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v2, Lco/uk/getmondo/signup/identity_verification/y;->h:Lco/uk/getmondo/signup/identity_verification/y;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    new-instance v2, Lco/uk/getmondo/signup/identity_verification/y;

    const-string v3, "NOT_GOV_ID"

    const/16 v4, 0x8

    .line 15
    const-string v5, "NOT_GOV_ID"

    const v6, 0x7f0a0239

    invoke-direct {v2, v3, v4, v5, v6}, Lco/uk/getmondo/signup/identity_verification/y;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v2, Lco/uk/getmondo/signup/identity_verification/y;->i:Lco/uk/getmondo/signup/identity_verification/y;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    new-instance v2, Lco/uk/getmondo/signup/identity_verification/y;

    const-string v3, "NO_DOB"

    const/16 v4, 0x9

    .line 16
    const-string v5, "NO_DOB"

    const v6, 0x7f0a0237

    invoke-direct {v2, v3, v4, v5, v6}, Lco/uk/getmondo/signup/identity_verification/y;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v2, Lco/uk/getmondo/signup/identity_verification/y;->j:Lco/uk/getmondo/signup/identity_verification/y;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    new-instance v2, Lco/uk/getmondo/signup/identity_verification/y;

    const-string v3, "NO_EXPIRY_DATE"

    const/16 v4, 0xa

    .line 17
    const-string v5, "NO_EXPIRY_DATE"

    const v6, 0x7f0a0238

    invoke-direct {v2, v3, v4, v5, v6}, Lco/uk/getmondo/signup/identity_verification/y;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v2, Lco/uk/getmondo/signup/identity_verification/y;->k:Lco/uk/getmondo/signup/identity_verification/y;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    new-instance v2, Lco/uk/getmondo/signup/identity_verification/y;

    const-string v3, "OTHER"

    const/16 v4, 0xb

    .line 18
    const-string v5, "OTHER"

    const v6, 0x7f0a023b

    invoke-direct {v2, v3, v4, v5, v6}, Lco/uk/getmondo/signup/identity_verification/y;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v2, Lco/uk/getmondo/signup/identity_verification/y;->l:Lco/uk/getmondo/signup/identity_verification/y;

    aput-object v2, v0, v1

    sput-object v0, Lco/uk/getmondo/signup/identity_verification/y;->n:[Lco/uk/getmondo/signup/identity_verification/y;

    new-instance v0, Lco/uk/getmondo/signup/identity_verification/y$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lco/uk/getmondo/signup/identity_verification/y$a;-><init>(Lkotlin/d/b/i;)V

    sput-object v0, Lco/uk/getmondo/signup/identity_verification/y;->m:Lco/uk/getmondo/signup/identity_verification/y$a;

    return-void
.end method

.method protected constructor <init>(Ljava/lang/String;ILjava/lang/String;I)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "I)V"
        }
    .end annotation

    .prologue
    const-string v0, "apiValue"

    invoke-static {p3, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 6
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput-object p3, p0, Lco/uk/getmondo/signup/identity_verification/y;->o:Ljava/lang/String;

    iput p4, p0, Lco/uk/getmondo/signup/identity_verification/y;->p:I

    return-void
.end method

.method public static final a(Ljava/lang/String;)Lco/uk/getmondo/signup/identity_verification/y;
    .locals 1

    const-string v0, "apiValue"

    invoke-static {p0, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v0, Lco/uk/getmondo/signup/identity_verification/y;->m:Lco/uk/getmondo/signup/identity_verification/y$a;

    invoke-virtual {v0, p0}, Lco/uk/getmondo/signup/identity_verification/y$a;->a(Ljava/lang/String;)Lco/uk/getmondo/signup/identity_verification/y;

    move-result-object v0

    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)Lco/uk/getmondo/signup/identity_verification/y;
    .locals 1

    const-class v0, Lco/uk/getmondo/signup/identity_verification/y;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/signup/identity_verification/y;

    return-object v0
.end method

.method public static values()[Lco/uk/getmondo/signup/identity_verification/y;
    .locals 1

    sget-object v0, Lco/uk/getmondo/signup/identity_verification/y;->n:[Lco/uk/getmondo/signup/identity_verification/y;

    invoke-virtual {v0}, [Lco/uk/getmondo/signup/identity_verification/y;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lco/uk/getmondo/signup/identity_verification/y;

    return-object v0
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 6
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/y;->o:Ljava/lang/String;

    return-object v0
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 6
    iget v0, p0, Lco/uk/getmondo/signup/identity_verification/y;->p:I

    return v0
.end method
