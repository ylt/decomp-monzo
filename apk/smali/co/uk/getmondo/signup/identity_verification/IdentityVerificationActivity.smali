.class public final Lco/uk/getmondo/signup/identity_verification/IdentityVerificationActivity;
.super Lco/uk/getmondo/signup/a;
.source "IdentityVerificationActivity.kt"

# interfaces
.implements Lco/uk/getmondo/signup/identity_verification/e$b;
.implements Lco/uk/getmondo/signup/identity_verification/t$b;
.implements Lco/uk/getmondo/signup/identity_verification/v$a;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lco/uk/getmondo/signup/identity_verification/IdentityVerificationActivity$a;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000d\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0010\u000e\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u0007\n\u0002\u0018\u0002\n\u0002\u0008\u0006\n\u0002\u0010\u000b\n\u0002\u0008\u0005\u0018\u0000 92\u00020\u00012\u00020\u00022\u00020\u00032\u00020\u0004:\u00019B\u0005\u00a2\u0006\u0002\u0010\u0005J\u0008\u0010*\u001a\u00020\u0013H\u0016J\u0008\u0010+\u001a\u00020\u0013H\u0016J\u0012\u0010,\u001a\u00020\u00132\u0008\u0010-\u001a\u0004\u0018\u00010.H\u0014J\u0008\u0010/\u001a\u00020\u0013H\u0014J\u0008\u00100\u001a\u00020\u0013H\u0016J\u0008\u0010\u0011\u001a\u00020\u0013H\u0016J\u0008\u00101\u001a\u00020\u0013H\u0016J\u0008\u00102\u001a\u00020\u0013H\u0016J\u001a\u00103\u001a\u00020\u00132\u0006\u00104\u001a\u0002052\u0008\u00106\u001a\u0004\u0018\u00010!H\u0016J\u0008\u00107\u001a\u00020\u0013H\u0016J\u0008\u00108\u001a\u00020\u0013H\u0016R\u001b\u0010\u0006\u001a\u00020\u00078BX\u0082\u0084\u0002\u00a2\u0006\u000c\n\u0004\u0008\n\u0010\u000b\u001a\u0004\u0008\u0008\u0010\tR\u001b\u0010\u000c\u001a\u00020\r8BX\u0082\u0084\u0002\u00a2\u0006\u000c\n\u0004\u0008\u0010\u0010\u000b\u001a\u0004\u0008\u000e\u0010\u000fR\u001a\u0010\u0011\u001a\u0008\u0012\u0004\u0012\u00020\u00130\u0012X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0014\u0010\u0015R\u001a\u0010\u0016\u001a\u0008\u0012\u0004\u0012\u00020\u00130\u00178VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0018\u0010\u0019R\u001e\u0010\u001a\u001a\u00020\u001b8\u0006@\u0006X\u0087.\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u001c\u0010\u001d\"\u0004\u0008\u001e\u0010\u001fR\u001d\u0010 \u001a\u0004\u0018\u00010!8BX\u0082\u0084\u0002\u00a2\u0006\u000c\n\u0004\u0008$\u0010\u000b\u001a\u0004\u0008\"\u0010#R\u001b\u0010%\u001a\u00020&8BX\u0082\u0084\u0002\u00a2\u0006\u000c\n\u0004\u0008)\u0010\u000b\u001a\u0004\u0008\'\u0010(\u00a8\u0006:"
    }
    d2 = {
        "Lco/uk/getmondo/signup/identity_verification/IdentityVerificationActivity;",
        "Lco/uk/getmondo/signup/BaseSignupActivity;",
        "Lco/uk/getmondo/signup/identity_verification/IdentityVerificationPresenter$View;",
        "Lco/uk/getmondo/signup/identity_verification/IdentityDocumentsFragment$StepListener;",
        "Lco/uk/getmondo/signup/identity_verification/IdentityVerificationOnboardingFragment$StepListener;",
        "()V",
        "entryPoint",
        "Lco/uk/getmondo/api/model/tracking/Impression$KycFrom;",
        "getEntryPoint",
        "()Lco/uk/getmondo/api/model/tracking/Impression$KycFrom;",
        "entryPoint$delegate",
        "Lkotlin/Lazy;",
        "identityVerificationVersion",
        "Lco/uk/getmondo/signup/identity_verification/data/IdentityVerificationVersion;",
        "getIdentityVerificationVersion",
        "()Lco/uk/getmondo/signup/identity_verification/data/IdentityVerificationVersion;",
        "identityVerificationVersion$delegate",
        "onOnboardingComplete",
        "Lcom/jakewharton/rxrelay2/PublishRelay;",
        "",
        "getOnOnboardingComplete",
        "()Lcom/jakewharton/rxrelay2/PublishRelay;",
        "onRetryClicked",
        "Lio/reactivex/Observable;",
        "getOnRetryClicked",
        "()Lio/reactivex/Observable;",
        "presenter",
        "Lco/uk/getmondo/signup/identity_verification/IdentityVerificationPresenter;",
        "getPresenter",
        "()Lco/uk/getmondo/signup/identity_verification/IdentityVerificationPresenter;",
        "setPresenter",
        "(Lco/uk/getmondo/signup/identity_verification/IdentityVerificationPresenter;)V",
        "rejectedReason",
        "",
        "getRejectedReason",
        "()Ljava/lang/String;",
        "rejectedReason$delegate",
        "signupSource",
        "Lco/uk/getmondo/api/model/signup/SignupSource;",
        "getSignupSource",
        "()Lco/uk/getmondo/api/model/signup/SignupSource;",
        "signupSource$delegate",
        "hideLoading",
        "onBackPressed",
        "onCreate",
        "savedInstanceState",
        "Landroid/os/Bundle;",
        "onDestroy",
        "onIdentityVerificationDocumentsSubmitted",
        "openBlocked",
        "openGenericError",
        "showIdentityDocuments",
        "allowSystemCamera",
        "",
        "rejectionNote",
        "showLoading",
        "showOnboarding",
        "Companion",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# static fields
.field static final synthetic a:[Lkotlin/reflect/l;

.field public static final g:Lco/uk/getmondo/signup/identity_verification/IdentityVerificationActivity$a;


# instance fields
.field public b:Lco/uk/getmondo/signup/identity_verification/v;

.field private final h:Lkotlin/c;

.field private final i:Lkotlin/c;

.field private final j:Lkotlin/c;

.field private final k:Lkotlin/c;

.field private final l:Lcom/b/b/c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/b/b/c",
            "<",
            "Lkotlin/n;",
            ">;"
        }
    .end annotation
.end field

.field private m:Ljava/util/HashMap;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const/4 v0, 0x4

    new-array v1, v0, [Lkotlin/reflect/l;

    const/4 v2, 0x0

    new-instance v0, Lkotlin/d/b/w;

    const-class v3, Lco/uk/getmondo/signup/identity_verification/IdentityVerificationActivity;

    invoke-static {v3}, Lkotlin/d/b/y;->a(Ljava/lang/Class;)Lkotlin/reflect/c;

    move-result-object v3

    const-string v4, "identityVerificationVersion"

    const-string v5, "getIdentityVerificationVersion()Lco/uk/getmondo/signup/identity_verification/data/IdentityVerificationVersion;"

    invoke-direct {v0, v3, v4, v5}, Lkotlin/d/b/w;-><init>(Lkotlin/reflect/e;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v0}, Lkotlin/d/b/y;->a(Lkotlin/d/b/v;)Lkotlin/reflect/n;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/l;

    aput-object v0, v1, v2

    const/4 v2, 0x1

    new-instance v0, Lkotlin/d/b/w;

    const-class v3, Lco/uk/getmondo/signup/identity_verification/IdentityVerificationActivity;

    invoke-static {v3}, Lkotlin/d/b/y;->a(Ljava/lang/Class;)Lkotlin/reflect/c;

    move-result-object v3

    const-string v4, "entryPoint"

    const-string v5, "getEntryPoint()Lco/uk/getmondo/api/model/tracking/Impression$KycFrom;"

    invoke-direct {v0, v3, v4, v5}, Lkotlin/d/b/w;-><init>(Lkotlin/reflect/e;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v0}, Lkotlin/d/b/y;->a(Lkotlin/d/b/v;)Lkotlin/reflect/n;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/l;

    aput-object v0, v1, v2

    const/4 v2, 0x2

    new-instance v0, Lkotlin/d/b/w;

    const-class v3, Lco/uk/getmondo/signup/identity_verification/IdentityVerificationActivity;

    invoke-static {v3}, Lkotlin/d/b/y;->a(Ljava/lang/Class;)Lkotlin/reflect/c;

    move-result-object v3

    const-string v4, "rejectedReason"

    const-string v5, "getRejectedReason()Ljava/lang/String;"

    invoke-direct {v0, v3, v4, v5}, Lkotlin/d/b/w;-><init>(Lkotlin/reflect/e;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v0}, Lkotlin/d/b/y;->a(Lkotlin/d/b/v;)Lkotlin/reflect/n;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/l;

    aput-object v0, v1, v2

    const/4 v2, 0x3

    new-instance v0, Lkotlin/d/b/w;

    const-class v3, Lco/uk/getmondo/signup/identity_verification/IdentityVerificationActivity;

    invoke-static {v3}, Lkotlin/d/b/y;->a(Ljava/lang/Class;)Lkotlin/reflect/c;

    move-result-object v3

    const-string v4, "signupSource"

    const-string v5, "getSignupSource()Lco/uk/getmondo/api/model/signup/SignupSource;"

    invoke-direct {v0, v3, v4, v5}, Lkotlin/d/b/w;-><init>(Lkotlin/reflect/e;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v0}, Lkotlin/d/b/y;->a(Lkotlin/d/b/v;)Lkotlin/reflect/n;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/l;

    aput-object v0, v1, v2

    sput-object v1, Lco/uk/getmondo/signup/identity_verification/IdentityVerificationActivity;->a:[Lkotlin/reflect/l;

    new-instance v0, Lco/uk/getmondo/signup/identity_verification/IdentityVerificationActivity$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lco/uk/getmondo/signup/identity_verification/IdentityVerificationActivity$a;-><init>(Lkotlin/d/b/i;)V

    sput-object v0, Lco/uk/getmondo/signup/identity_verification/IdentityVerificationActivity;->g:Lco/uk/getmondo/signup/identity_verification/IdentityVerificationActivity$a;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 23
    invoke-direct {p0}, Lco/uk/getmondo/signup/a;-><init>()V

    .line 25
    new-instance v0, Lco/uk/getmondo/signup/identity_verification/IdentityVerificationActivity$c;

    invoke-direct {v0, p0}, Lco/uk/getmondo/signup/identity_verification/IdentityVerificationActivity$c;-><init>(Lco/uk/getmondo/signup/identity_verification/IdentityVerificationActivity;)V

    check-cast v0, Lkotlin/d/a/a;

    invoke-static {v0}, Lkotlin/d;->a(Lkotlin/d/a/a;)Lkotlin/c;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/signup/identity_verification/IdentityVerificationActivity;->h:Lkotlin/c;

    .line 26
    new-instance v0, Lco/uk/getmondo/signup/identity_verification/IdentityVerificationActivity$b;

    invoke-direct {v0, p0}, Lco/uk/getmondo/signup/identity_verification/IdentityVerificationActivity$b;-><init>(Lco/uk/getmondo/signup/identity_verification/IdentityVerificationActivity;)V

    check-cast v0, Lkotlin/d/a/a;

    invoke-static {v0}, Lkotlin/d;->a(Lkotlin/d/a/a;)Lkotlin/c;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/signup/identity_verification/IdentityVerificationActivity;->i:Lkotlin/c;

    .line 27
    new-instance v0, Lco/uk/getmondo/signup/identity_verification/IdentityVerificationActivity$d;

    invoke-direct {v0, p0}, Lco/uk/getmondo/signup/identity_verification/IdentityVerificationActivity$d;-><init>(Lco/uk/getmondo/signup/identity_verification/IdentityVerificationActivity;)V

    check-cast v0, Lkotlin/d/a/a;

    invoke-static {v0}, Lkotlin/d;->a(Lkotlin/d/a/a;)Lkotlin/c;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/signup/identity_verification/IdentityVerificationActivity;->j:Lkotlin/c;

    .line 28
    new-instance v0, Lco/uk/getmondo/signup/identity_verification/IdentityVerificationActivity$e;

    invoke-direct {v0, p0}, Lco/uk/getmondo/signup/identity_verification/IdentityVerificationActivity$e;-><init>(Lco/uk/getmondo/signup/identity_verification/IdentityVerificationActivity;)V

    check-cast v0, Lkotlin/d/a/a;

    invoke-static {v0}, Lkotlin/d;->a(Lkotlin/d/a/a;)Lkotlin/c;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/signup/identity_verification/IdentityVerificationActivity;->k:Lkotlin/c;

    .line 63
    invoke-static {}, Lcom/b/b/c;->a()Lcom/b/b/c;

    move-result-object v0

    const-string v1, "PublishRelay.create()"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v0, p0, Lco/uk/getmondo/signup/identity_verification/IdentityVerificationActivity;->l:Lcom/b/b/c;

    return-void
.end method

.method public static final a(Landroid/content/Context;Lco/uk/getmondo/signup/identity_verification/a/j;Lco/uk/getmondo/api/model/tracking/Impression$KycFrom;Lco/uk/getmondo/api/model/signup/SignupSource;)Landroid/content/Intent;
    .locals 9

    const/4 v5, 0x0

    sget-object v0, Lco/uk/getmondo/signup/identity_verification/IdentityVerificationActivity;->g:Lco/uk/getmondo/signup/identity_verification/IdentityVerificationActivity$a;

    const/16 v7, 0x30

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v6, v5

    move-object v8, v5

    invoke-static/range {v0 .. v8}, Lco/uk/getmondo/signup/identity_verification/IdentityVerificationActivity$a;->a(Lco/uk/getmondo/signup/identity_verification/IdentityVerificationActivity$a;Landroid/content/Context;Lco/uk/getmondo/signup/identity_verification/a/j;Lco/uk/getmondo/api/model/tracking/Impression$KycFrom;Lco/uk/getmondo/api/model/signup/SignupSource;Lco/uk/getmondo/signup/j;Ljava/lang/String;ILjava/lang/Object;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public static final a(Landroid/content/Context;Lco/uk/getmondo/signup/identity_verification/a/j;Lco/uk/getmondo/api/model/tracking/Impression$KycFrom;Lco/uk/getmondo/api/model/signup/SignupSource;Lco/uk/getmondo/signup/j;Ljava/lang/String;)Landroid/content/Intent;
    .locals 7

    const-string v0, "context"

    invoke-static {p0, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "version"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "from"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "signupSource"

    invoke-static {p3, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "signupEntryPoint"

    invoke-static {p4, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v0, Lco/uk/getmondo/signup/identity_verification/IdentityVerificationActivity;->g:Lco/uk/getmondo/signup/identity_verification/IdentityVerificationActivity$a;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-virtual/range {v0 .. v6}, Lco/uk/getmondo/signup/identity_verification/IdentityVerificationActivity$a;->a(Landroid/content/Context;Lco/uk/getmondo/signup/identity_verification/a/j;Lco/uk/getmondo/api/model/tracking/Impression$KycFrom;Lco/uk/getmondo/api/model/signup/SignupSource;Lco/uk/getmondo/signup/j;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method private final v()Lco/uk/getmondo/signup/identity_verification/a/j;
    .locals 3

    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/IdentityVerificationActivity;->h:Lkotlin/c;

    sget-object v1, Lco/uk/getmondo/signup/identity_verification/IdentityVerificationActivity;->a:[Lkotlin/reflect/l;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-interface {v0}, Lkotlin/c;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/signup/identity_verification/a/j;

    return-object v0
.end method

.method private final w()Lco/uk/getmondo/api/model/tracking/Impression$KycFrom;
    .locals 3

    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/IdentityVerificationActivity;->i:Lkotlin/c;

    sget-object v1, Lco/uk/getmondo/signup/identity_verification/IdentityVerificationActivity;->a:[Lkotlin/reflect/l;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-interface {v0}, Lkotlin/c;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/api/model/tracking/Impression$KycFrom;

    return-object v0
.end method

.method private final x()Ljava/lang/String;
    .locals 3

    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/IdentityVerificationActivity;->j:Lkotlin/c;

    sget-object v1, Lco/uk/getmondo/signup/identity_verification/IdentityVerificationActivity;->a:[Lkotlin/reflect/l;

    const/4 v2, 0x2

    aget-object v1, v1, v2

    invoke-interface {v0}, Lkotlin/c;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method private final y()Lco/uk/getmondo/api/model/signup/SignupSource;
    .locals 3

    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/IdentityVerificationActivity;->k:Lkotlin/c;

    sget-object v1, Lco/uk/getmondo/signup/identity_verification/IdentityVerificationActivity;->a:[Lkotlin/reflect/l;

    const/4 v2, 0x3

    aget-object v1, v1, v2

    invoke-interface {v0}, Lkotlin/c;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/api/model/signup/SignupSource;

    return-object v0
.end method


# virtual methods
.method public a(I)Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/IdentityVerificationActivity;->m:Ljava/util/HashMap;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lco/uk/getmondo/signup/identity_verification/IdentityVerificationActivity;->m:Ljava/util/HashMap;

    :cond_0
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/IdentityVerificationActivity;->m:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    if-nez v0, :cond_1

    invoke-virtual {p0, p1}, Landroid/support/v4/app/j;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/signup/identity_verification/IdentityVerificationActivity;->m:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    return-object v0
.end method

.method public a(ZLjava/lang/String;)V
    .locals 8

    .prologue
    const v7, 0x7f1101a8

    .line 84
    invoke-virtual {p0}, Lco/uk/getmondo/signup/identity_verification/IdentityVerificationActivity;->getSupportFragmentManager()Landroid/support/v4/app/n;

    move-result-object v0

    invoke-virtual {v0, v7}, Landroid/support/v4/app/n;->a(I)Landroid/support/v4/app/Fragment;

    move-result-object v6

    .line 85
    instance-of v0, v6, Lco/uk/getmondo/signup/identity_verification/e;

    if-nez v0, :cond_1

    .line 86
    if-eqz p2, :cond_2

    move-object v3, p2

    .line 87
    :goto_0
    sget-object v0, Lco/uk/getmondo/signup/identity_verification/e;->d:Lco/uk/getmondo/signup/identity_verification/e$a;

    .line 88
    invoke-direct {p0}, Lco/uk/getmondo/signup/identity_verification/IdentityVerificationActivity;->v()Lco/uk/getmondo/signup/identity_verification/a/j;

    move-result-object v1

    .line 89
    invoke-direct {p0}, Lco/uk/getmondo/signup/identity_verification/IdentityVerificationActivity;->w()Lco/uk/getmondo/api/model/tracking/Impression$KycFrom;

    move-result-object v2

    .line 92
    invoke-direct {p0}, Lco/uk/getmondo/signup/identity_verification/IdentityVerificationActivity;->y()Lco/uk/getmondo/api/model/signup/SignupSource;

    move-result-object v5

    move v4, p1

    .line 87
    invoke-virtual/range {v0 .. v5}, Lco/uk/getmondo/signup/identity_verification/e$a;->a(Lco/uk/getmondo/signup/identity_verification/a/j;Lco/uk/getmondo/api/model/tracking/Impression$KycFrom;Ljava/lang/String;ZLco/uk/getmondo/api/model/signup/SignupSource;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    .line 94
    invoke-virtual {p0}, Lco/uk/getmondo/signup/identity_verification/IdentityVerificationActivity;->getSupportFragmentManager()Landroid/support/v4/app/n;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/n;->a()Landroid/support/v4/app/t;

    move-result-object v1

    .line 95
    invoke-virtual {v1, v7, v0}, Landroid/support/v4/app/t;->b(ILandroid/support/v4/app/Fragment;)Landroid/support/v4/app/t;

    move-result-object v0

    .line 96
    if-eqz v6, :cond_0

    .line 98
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v4/app/t;->a(Ljava/lang/String;)Landroid/support/v4/app/t;

    move-result-object v1

    .line 99
    const/16 v2, 0x1001

    invoke-virtual {v1, v2}, Landroid/support/v4/app/t;->a(I)Landroid/support/v4/app/t;

    .line 101
    :cond_0
    invoke-virtual {v0}, Landroid/support/v4/app/t;->d()I

    .line 103
    :cond_1
    return-void

    .line 86
    :cond_2
    invoke-direct {p0}, Lco/uk/getmondo/signup/identity_verification/IdentityVerificationActivity;->x()Ljava/lang/String;

    move-result-object v3

    goto :goto_0
.end method

.method public c()Lio/reactivex/n;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Lkotlin/n;",
            ">;"
        }
    .end annotation

    .prologue
    .line 61
    sget v0, Lco/uk/getmondo/c$a;->idvErrorView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/identity_verification/IdentityVerificationActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/common/ui/LoadingErrorView;

    .line 159
    invoke-static {v0}, Lcom/b/a/c/c;->a(Landroid/view/View;)Lio/reactivex/n;

    move-result-object v1

    sget-object v0, Lcom/b/a/a/d;->a:Lcom/b/a/a/d;

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {v1, v0}, Lio/reactivex/n;->map(Lio/reactivex/c/h;)Lio/reactivex/n;

    move-result-object v0

    const-string v1, "RxView.clicks(this).map(VoidToUnit)"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 61
    return-object v0
.end method

.method public d()Lcom/b/b/c;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/b/b/c",
            "<",
            "Lkotlin/n;",
            ">;"
        }
    .end annotation

    .prologue
    .line 63
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/IdentityVerificationActivity;->l:Lcom/b/b/c;

    return-object v0
.end method

.method public synthetic e()Lio/reactivex/n;
    .locals 1

    .prologue
    .line 23
    invoke-virtual {p0}, Lco/uk/getmondo/signup/identity_verification/IdentityVerificationActivity;->d()Lcom/b/b/c;

    move-result-object v0

    check-cast v0, Lio/reactivex/n;

    return-object v0
.end method

.method public e_()V
    .locals 4

    .prologue
    .line 122
    invoke-direct {p0}, Lco/uk/getmondo/signup/identity_verification/IdentityVerificationActivity;->y()Lco/uk/getmondo/api/model/signup/SignupSource;

    move-result-object v0

    sget-object v1, Lco/uk/getmondo/api/model/signup/SignupSource;->PERSONAL_ACCOUNT:Lco/uk/getmondo/api/model/signup/SignupSource;

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 123
    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/identity_verification/IdentityVerificationActivity;->setResult(I)V

    .line 124
    invoke-virtual {p0}, Lco/uk/getmondo/signup/identity_verification/IdentityVerificationActivity;->finish()V

    .line 127
    :goto_0
    return-void

    .line 126
    :cond_0
    sget-object v1, Lco/uk/getmondo/signup/identity_verification/VerificationPendingActivity;->c:Lco/uk/getmondo/signup/identity_verification/VerificationPendingActivity$a;

    move-object v0, p0

    check-cast v0, Landroid/content/Context;

    invoke-direct {p0}, Lco/uk/getmondo/signup/identity_verification/IdentityVerificationActivity;->v()Lco/uk/getmondo/signup/identity_verification/a/j;

    move-result-object v2

    invoke-direct {p0}, Lco/uk/getmondo/signup/identity_verification/IdentityVerificationActivity;->w()Lco/uk/getmondo/api/model/tracking/Impression$KycFrom;

    move-result-object v3

    invoke-virtual {v1, v0, v2, v3}, Lco/uk/getmondo/signup/identity_verification/VerificationPendingActivity$a;->a(Landroid/content/Context;Lco/uk/getmondo/signup/identity_verification/a/j;Lco/uk/getmondo/api/model/tracking/Impression$KycFrom;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/identity_verification/IdentityVerificationActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method public f()V
    .locals 1

    .prologue
    .line 66
    sget v0, Lco/uk/getmondo/c$a;->idvStatusProgressBar:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/identity_verification/IdentityVerificationActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    invoke-static {v0}, Lco/uk/getmondo/common/ae;->a(Landroid/view/View;)V

    .line 67
    return-void
.end method

.method public g()V
    .locals 1

    .prologue
    .line 70
    sget v0, Lco/uk/getmondo/c$a;->idvStatusProgressBar:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/identity_verification/IdentityVerificationActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    invoke-static {v0}, Lco/uk/getmondo/common/ae;->b(Landroid/view/View;)V

    .line 71
    return-void
.end method

.method public h()V
    .locals 4

    .prologue
    const v3, 0x7f1101a8

    .line 74
    invoke-virtual {p0}, Lco/uk/getmondo/signup/identity_verification/IdentityVerificationActivity;->getSupportFragmentManager()Landroid/support/v4/app/n;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/support/v4/app/n;->a(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    .line 75
    instance-of v0, v0, Lco/uk/getmondo/signup/identity_verification/t;

    if-nez v0, :cond_0

    .line 76
    sget-object v0, Lco/uk/getmondo/signup/identity_verification/t;->d:Lco/uk/getmondo/signup/identity_verification/t$a;

    invoke-direct {p0}, Lco/uk/getmondo/signup/identity_verification/IdentityVerificationActivity;->v()Lco/uk/getmondo/signup/identity_verification/a/j;

    move-result-object v1

    invoke-direct {p0}, Lco/uk/getmondo/signup/identity_verification/IdentityVerificationActivity;->w()Lco/uk/getmondo/api/model/tracking/Impression$KycFrom;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lco/uk/getmondo/signup/identity_verification/t$a;->a(Lco/uk/getmondo/signup/identity_verification/a/j;Lco/uk/getmondo/api/model/tracking/Impression$KycFrom;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    .line 77
    invoke-virtual {p0}, Lco/uk/getmondo/signup/identity_verification/IdentityVerificationActivity;->getSupportFragmentManager()Landroid/support/v4/app/n;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/n;->a()Landroid/support/v4/app/t;

    move-result-object v1

    .line 78
    invoke-virtual {v1, v3, v0}, Landroid/support/v4/app/t;->b(ILandroid/support/v4/app/Fragment;)Landroid/support/v4/app/t;

    move-result-object v0

    .line 79
    invoke-virtual {v0}, Landroid/support/v4/app/t;->d()I

    .line 81
    :cond_0
    return-void
.end method

.method public i()V
    .locals 2

    .prologue
    .line 106
    sget-object v1, Lco/uk/getmondo/signup/identity_verification/chat_with_us/ChatWithUsActivity;->b:Lco/uk/getmondo/signup/identity_verification/chat_with_us/ChatWithUsActivity$a;

    move-object v0, p0

    check-cast v0, Landroid/content/Context;

    invoke-virtual {v1, v0}, Lco/uk/getmondo/signup/identity_verification/chat_with_us/ChatWithUsActivity$a;->a(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/identity_verification/IdentityVerificationActivity;->startActivity(Landroid/content/Intent;)V

    .line 107
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/identity_verification/IdentityVerificationActivity;->setResult(I)V

    .line 108
    invoke-virtual {p0}, Lco/uk/getmondo/signup/identity_verification/IdentityVerificationActivity;->finish()V

    .line 109
    return-void
.end method

.method public j()V
    .locals 2

    .prologue
    .line 112
    sget-object v1, Lco/uk/getmondo/signup/identity_verification/chat_with_us/ChatWithUsActivity;->b:Lco/uk/getmondo/signup/identity_verification/chat_with_us/ChatWithUsActivity$a;

    move-object v0, p0

    check-cast v0, Landroid/content/Context;

    invoke-virtual {v1, v0}, Lco/uk/getmondo/signup/identity_verification/chat_with_us/ChatWithUsActivity$a;->a(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/identity_verification/IdentityVerificationActivity;->startActivity(Landroid/content/Intent;)V

    .line 113
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/identity_verification/IdentityVerificationActivity;->setResult(I)V

    .line 114
    invoke-virtual {p0}, Lco/uk/getmondo/signup/identity_verification/IdentityVerificationActivity;->finish()V

    .line 115
    return-void
.end method

.method public k()V
    .locals 2

    .prologue
    .line 118
    invoke-virtual {p0}, Lco/uk/getmondo/signup/identity_verification/IdentityVerificationActivity;->d()Lcom/b/b/c;

    move-result-object v0

    sget-object v1, Lkotlin/n;->a:Lkotlin/n;

    invoke-virtual {v0, v1}, Lcom/b/b/c;->a(Ljava/lang/Object;)V

    .line 119
    return-void
.end method

.method public onBackPressed()V
    .locals 2

    .prologue
    .line 47
    invoke-virtual {p0}, Lco/uk/getmondo/signup/identity_verification/IdentityVerificationActivity;->getSupportFragmentManager()Landroid/support/v4/app/n;

    move-result-object v0

    const v1, 0x7f1101a8

    invoke-virtual {v0, v1}, Landroid/support/v4/app/n;->a(I)Landroid/support/v4/app/Fragment;

    move-result-object v1

    .line 48
    instance-of v0, v1, Lco/uk/getmondo/signup/identity_verification/t;

    if-eqz v0, :cond_0

    move-object v0, v1

    check-cast v0, Lco/uk/getmondo/signup/identity_verification/t;

    invoke-virtual {v0}, Lco/uk/getmondo/signup/identity_verification/t;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 49
    check-cast v1, Lco/uk/getmondo/signup/identity_verification/t;

    invoke-virtual {v1}, Lco/uk/getmondo/signup/identity_verification/t;->b()V

    .line 52
    :goto_0
    return-void

    .line 51
    :cond_0
    invoke-super {p0}, Lco/uk/getmondo/signup/a;->onBackPressed()V

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 33
    invoke-super {p0, p1}, Lco/uk/getmondo/signup/a;->onCreate(Landroid/os/Bundle;)V

    .line 35
    const v0, 0x7f050043

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/identity_verification/IdentityVerificationActivity;->setContentView(I)V

    .line 36
    invoke-virtual {p0}, Lco/uk/getmondo/signup/identity_verification/IdentityVerificationActivity;->getSupportActionBar()Landroid/support/v7/app/a;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0, v4}, Landroid/support/v7/app/a;->b(Z)V

    .line 37
    :cond_0
    invoke-virtual {p0}, Lco/uk/getmondo/signup/identity_verification/IdentityVerificationActivity;->getSupportActionBar()Landroid/support/v7/app/a;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v0, v4}, Landroid/support/v7/app/a;->d(Z)V

    .line 38
    :cond_1
    invoke-virtual {p0}, Lco/uk/getmondo/signup/identity_verification/IdentityVerificationActivity;->l()Lco/uk/getmondo/common/h/b/b;

    move-result-object v0

    .line 39
    new-instance v1, Lco/uk/getmondo/signup/identity_verification/p;

    invoke-direct {p0}, Lco/uk/getmondo/signup/identity_verification/IdentityVerificationActivity;->v()Lco/uk/getmondo/signup/identity_verification/a/j;

    move-result-object v2

    invoke-direct {p0}, Lco/uk/getmondo/signup/identity_verification/IdentityVerificationActivity;->y()Lco/uk/getmondo/api/model/signup/SignupSource;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lco/uk/getmondo/signup/identity_verification/p;-><init>(Lco/uk/getmondo/signup/identity_verification/a/j;Lco/uk/getmondo/api/model/signup/SignupSource;)V

    invoke-interface {v0, v1}, Lco/uk/getmondo/common/h/b/b;->a(Lco/uk/getmondo/signup/identity_verification/p;)Lco/uk/getmondo/signup/identity_verification/o;

    move-result-object v0

    .line 40
    new-instance v1, Lco/uk/getmondo/signup/identity_verification/j;

    invoke-direct {p0}, Lco/uk/getmondo/signup/identity_verification/IdentityVerificationActivity;->w()Lco/uk/getmondo/api/model/tracking/Impression$KycFrom;

    move-result-object v2

    invoke-direct {p0}, Lco/uk/getmondo/signup/identity_verification/IdentityVerificationActivity;->x()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3, v4}, Lco/uk/getmondo/signup/identity_verification/j;-><init>(Lco/uk/getmondo/api/model/tracking/Impression$KycFrom;Ljava/lang/String;Z)V

    invoke-interface {v0, v1}, Lco/uk/getmondo/signup/identity_verification/o;->a(Lco/uk/getmondo/signup/identity_verification/j;)Lco/uk/getmondo/signup/identity_verification/i;

    move-result-object v0

    .line 41
    invoke-interface {v0, p0}, Lco/uk/getmondo/signup/identity_verification/i;->a(Lco/uk/getmondo/signup/identity_verification/IdentityVerificationActivity;)V

    .line 43
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/IdentityVerificationActivity;->b:Lco/uk/getmondo/signup/identity_verification/v;

    if-nez v0, :cond_2

    const-string v1, "presenter"

    invoke-static {v1}, Lkotlin/d/b/l;->b(Ljava/lang/String;)V

    :cond_2
    check-cast p0, Lco/uk/getmondo/signup/identity_verification/v$a;

    invoke-virtual {v0, p0}, Lco/uk/getmondo/signup/identity_verification/v;->a(Lco/uk/getmondo/signup/identity_verification/v$a;)V

    .line 44
    return-void
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    .line 56
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/IdentityVerificationActivity;->b:Lco/uk/getmondo/signup/identity_verification/v;

    if-nez v0, :cond_0

    const-string v1, "presenter"

    invoke-static {v1}, Lkotlin/d/b/l;->b(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0}, Lco/uk/getmondo/signup/identity_verification/v;->b()V

    .line 57
    invoke-super {p0}, Lco/uk/getmondo/signup/a;->onDestroy()V

    .line 58
    return-void
.end method
