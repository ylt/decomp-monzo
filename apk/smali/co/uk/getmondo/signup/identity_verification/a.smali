.class public final Lco/uk/getmondo/signup/identity_verification/a;
.super Landroid/support/v4/app/i;
.source "ConfirmIdentityVerificationDialogFragment.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lco/uk/getmondo/signup/identity_verification/a$a;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u0008\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u0000 \u00142\u00020\u0001:\u0001\u0014B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0012\u0010\r\u001a\u00020\u000e2\u0008\u0010\u000f\u001a\u0004\u0018\u00010\u0010H\u0016J\u0010\u0010\u0011\u001a\u00020\u00052\u0006\u0010\u0012\u001a\u00020\u0013H\u0016R\"\u0010\u0003\u001a\n\u0012\u0004\u0012\u00020\u0005\u0018\u00010\u0004X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u0006\u0010\u0007\"\u0004\u0008\u0008\u0010\tR\"\u0010\n\u001a\n\u0012\u0004\u0012\u00020\u0005\u0018\u00010\u0004X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u000b\u0010\u0007\"\u0004\u0008\u000c\u0010\t\u00a8\u0006\u0015"
    }
    d2 = {
        "Lco/uk/getmondo/signup/identity_verification/ConfirmIdentityVerificationDialogFragment;",
        "Landroid/support/v4/app/DialogFragment;",
        "()V",
        "closedListener",
        "Lkotlin/Function0;",
        "",
        "getClosedListener",
        "()Lkotlin/jvm/functions/Function0;",
        "setClosedListener",
        "(Lkotlin/jvm/functions/Function0;)V",
        "confirmListener",
        "getConfirmListener",
        "setConfirmListener",
        "onCreateDialog",
        "Landroid/app/Dialog;",
        "savedInstanceState",
        "Landroid/os/Bundle;",
        "onDismiss",
        "dialog",
        "Landroid/content/DialogInterface;",
        "Companion",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# static fields
.field public static final a:Lco/uk/getmondo/signup/identity_verification/a$a;


# instance fields
.field private b:Lkotlin/d/a/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/d/a/a",
            "<",
            "Lkotlin/n;",
            ">;"
        }
    .end annotation
.end field

.field private c:Lkotlin/d/a/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/d/a/a",
            "<",
            "Lkotlin/n;",
            ">;"
        }
    .end annotation
.end field

.field private d:Ljava/util/HashMap;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lco/uk/getmondo/signup/identity_verification/a$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lco/uk/getmondo/signup/identity_verification/a$a;-><init>(Lkotlin/d/b/i;)V

    sput-object v0, Lco/uk/getmondo/signup/identity_verification/a;->a:Lco/uk/getmondo/signup/identity_verification/a$a;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0}, Landroid/support/v4/app/i;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Lkotlin/d/a/a;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/d/a/a",
            "<",
            "Lkotlin/n;",
            ">;"
        }
    .end annotation

    .prologue
    .line 11
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/a;->b:Lkotlin/d/a/a;

    return-object v0
.end method

.method public final a(Lkotlin/d/a/a;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/d/a/a",
            "<",
            "Lkotlin/n;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 11
    iput-object p1, p0, Lco/uk/getmondo/signup/identity_verification/a;->b:Lkotlin/d/a/a;

    return-void
.end method

.method public b()V
    .locals 1

    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/a;->d:Ljava/util/HashMap;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/a;->d:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    :cond_0
    return-void
.end method

.method public final b(Lkotlin/d/a/a;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/d/a/a",
            "<",
            "Lkotlin/n;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 12
    iput-object p1, p0, Lco/uk/getmondo/signup/identity_verification/a;->c:Lkotlin/d/a/a;

    return-void
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 3

    .prologue
    .line 15
    invoke-virtual {p0}, Lco/uk/getmondo/signup/identity_verification/a;->getActivity()Landroid/support/v4/app/j;

    move-result-object v0

    .line 16
    new-instance v1, Landroid/support/v7/app/d$a;

    check-cast v0, Landroid/content/Context;

    const v2, 0x7f0c0112

    invoke-direct {v1, v0, v2}, Landroid/support/v7/app/d$a;-><init>(Landroid/content/Context;I)V

    .line 17
    const v0, 0x7f0a0202

    invoke-virtual {v1, v0}, Landroid/support/v7/app/d$a;->a(I)Landroid/support/v7/app/d$a;

    move-result-object v0

    .line 18
    const v1, 0x7f0a0201

    invoke-virtual {v0, v1}, Landroid/support/v7/app/d$a;->b(I)Landroid/support/v7/app/d$a;

    move-result-object v2

    .line 19
    const v0, 0x7f0a0200

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/identity_verification/a;->getString(I)Ljava/lang/String;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    new-instance v1, Lco/uk/getmondo/signup/identity_verification/a$b;

    invoke-direct {v1, p0}, Lco/uk/getmondo/signup/identity_verification/a$b;-><init>(Lco/uk/getmondo/signup/identity_verification/a;)V

    check-cast v1, Landroid/content/DialogInterface$OnClickListener;

    invoke-virtual {v2, v0, v1}, Landroid/support/v7/app/d$a;->a(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/support/v7/app/d$a;

    move-result-object v1

    .line 20
    const v0, 0x7f0a01ff

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/identity_verification/a;->getString(I)Ljava/lang/String;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Landroid/support/v7/app/d$a;->b(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/support/v7/app/d$a;

    move-result-object v0

    .line 21
    invoke-virtual {v0}, Landroid/support/v7/app/d$a;->b()Landroid/support/v7/app/d;

    move-result-object v0

    const-string v1, "AlertDialog.Builder(acti\u2026                .create()"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/app/Dialog;

    return-object v0
.end method

.method public synthetic onDestroyView()V
    .locals 0

    invoke-super {p0}, Landroid/support/v4/app/i;->onDestroyView()V

    invoke-virtual {p0}, Lco/uk/getmondo/signup/identity_verification/a;->b()V

    return-void
.end method

.method public onDismiss(Landroid/content/DialogInterface;)V
    .locals 1

    .prologue
    const-string v0, "dialog"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 25
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/a;->c:Lkotlin/d/a/a;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lkotlin/d/a/a;->v_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/n;

    .line 26
    :cond_0
    invoke-super {p0, p1}, Landroid/support/v4/app/i;->onDismiss(Landroid/content/DialogInterface;)V

    .line 27
    return-void
.end method
