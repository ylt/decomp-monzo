.class final Lco/uk/getmondo/signup/identity_verification/t$e;
.super Ljava/lang/Object;
.source "IdentityVerificationOnboardingFragment.kt"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lco/uk/getmondo/signup/identity_verification/t;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "",
        "it",
        "Landroid/view/View;",
        "kotlin.jvm.PlatformType",
        "onClick"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field final synthetic a:Lco/uk/getmondo/signup/identity_verification/t;


# direct methods
.method constructor <init>(Lco/uk/getmondo/signup/identity_verification/t;)V
    .locals 0

    iput-object p1, p0, Lco/uk/getmondo/signup/identity_verification/t$e;->a:Lco/uk/getmondo/signup/identity_verification/t;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 45
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/t$e;->a:Lco/uk/getmondo/signup/identity_verification/t;

    sget v1, Lco/uk/getmondo/c$a;->idvOnboardingViewPager:I

    invoke-virtual {v0, v1}, Lco/uk/getmondo/signup/identity_verification/t;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v0

    add-int/lit8 v1, v0, 0x1

    .line 46
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/t$e;->a:Lco/uk/getmondo/signup/identity_verification/t;

    sget v2, Lco/uk/getmondo/c$a;->idvOnboardingViewPager:I

    invoke-virtual {v0, v2}, Lco/uk/getmondo/signup/identity_verification/t;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0}, Landroid/support/v4/view/ViewPager;->getAdapter()Landroid/support/v4/view/p;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/view/p;->b()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 47
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/t$e;->a:Lco/uk/getmondo/signup/identity_verification/t;

    sget v2, Lco/uk/getmondo/c$a;->idvOnboardingViewPager:I

    invoke-virtual {v0, v2}, Lco/uk/getmondo/signup/identity_verification/t;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v4/view/ViewPager;

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/view/ViewPager;->a(IZ)V

    .line 50
    :goto_0
    return-void

    .line 49
    :cond_0
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/t$e;->a:Lco/uk/getmondo/signup/identity_verification/t;

    invoke-static {v0}, Lco/uk/getmondo/signup/identity_verification/t;->a(Lco/uk/getmondo/signup/identity_verification/t;)Lco/uk/getmondo/signup/identity_verification/t$b;

    move-result-object v0

    invoke-interface {v0}, Lco/uk/getmondo/signup/identity_verification/t$b;->k()V

    goto :goto_0
.end method
