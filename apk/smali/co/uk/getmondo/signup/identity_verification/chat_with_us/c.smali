.class public final Lco/uk/getmondo/signup/identity_verification/chat_with_us/c;
.super Ljava/lang/Object;
.source "ChatWithUsPresenter_Factory.java"

# interfaces
.implements Lb/a/b;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lb/a/b",
        "<",
        "Lco/uk/getmondo/signup/identity_verification/chat_with_us/b;",
        ">;"
    }
.end annotation


# static fields
.field static final synthetic a:Z


# instance fields
.field private final b:Lb/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lb/a",
            "<",
            "Lco/uk/getmondo/signup/identity_verification/chat_with_us/b;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Ljavax/a/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/common/q;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 10
    const-class v0, Lco/uk/getmondo/signup/identity_verification/chat_with_us/c;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lco/uk/getmondo/signup/identity_verification/chat_with_us/c;->a:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lb/a;Ljavax/a/a;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lb/a",
            "<",
            "Lco/uk/getmondo/signup/identity_verification/chat_with_us/b;",
            ">;",
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/common/q;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    sget-boolean v0, Lco/uk/getmondo/signup/identity_verification/chat_with_us/c;->a:Z

    if-nez v0, :cond_0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 23
    :cond_0
    iput-object p1, p0, Lco/uk/getmondo/signup/identity_verification/chat_with_us/c;->b:Lb/a;

    .line 24
    sget-boolean v0, Lco/uk/getmondo/signup/identity_verification/chat_with_us/c;->a:Z

    if-nez v0, :cond_1

    if-nez p2, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 25
    :cond_1
    iput-object p2, p0, Lco/uk/getmondo/signup/identity_verification/chat_with_us/c;->c:Ljavax/a/a;

    .line 26
    return-void
.end method

.method public static a(Lb/a;Ljavax/a/a;)Lb/a/b;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lb/a",
            "<",
            "Lco/uk/getmondo/signup/identity_verification/chat_with_us/b;",
            ">;",
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/common/q;",
            ">;)",
            "Lb/a/b",
            "<",
            "Lco/uk/getmondo/signup/identity_verification/chat_with_us/b;",
            ">;"
        }
    .end annotation

    .prologue
    .line 37
    new-instance v0, Lco/uk/getmondo/signup/identity_verification/chat_with_us/c;

    invoke-direct {v0, p0, p1}, Lco/uk/getmondo/signup/identity_verification/chat_with_us/c;-><init>(Lb/a;Ljavax/a/a;)V

    return-object v0
.end method


# virtual methods
.method public a()Lco/uk/getmondo/signup/identity_verification/chat_with_us/b;
    .locals 3

    .prologue
    .line 30
    iget-object v1, p0, Lco/uk/getmondo/signup/identity_verification/chat_with_us/c;->b:Lb/a;

    new-instance v2, Lco/uk/getmondo/signup/identity_verification/chat_with_us/b;

    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/chat_with_us/c;->c:Ljavax/a/a;

    .line 31
    invoke-interface {v0}, Ljavax/a/a;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/common/q;

    invoke-direct {v2, v0}, Lco/uk/getmondo/signup/identity_verification/chat_with_us/b;-><init>(Lco/uk/getmondo/common/q;)V

    .line 30
    invoke-static {v1, v2}, Lb/a/c;->a(Lb/a;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/signup/identity_verification/chat_with_us/b;

    return-object v0
.end method

.method public synthetic b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 10
    invoke-virtual {p0}, Lco/uk/getmondo/signup/identity_verification/chat_with_us/c;->a()Lco/uk/getmondo/signup/identity_verification/chat_with_us/b;

    move-result-object v0

    return-object v0
.end method
