.class public final Lco/uk/getmondo/signup/identity_verification/chat_with_us/ChatWithUsActivity;
.super Lco/uk/getmondo/common/activities/b;
.source "ChatWithUsActivity.kt"

# interfaces
.implements Lco/uk/getmondo/signup/identity_verification/chat_with_us/b$a;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lco/uk/getmondo/signup/identity_verification/chat_with_us/ChatWithUsActivity$a;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u00006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0005\u0018\u0000 \u00162\u00020\u00012\u00020\u0002:\u0001\u0016B\u0005\u00a2\u0006\u0002\u0010\u0003J\u000e\u0010\r\u001a\u0008\u0012\u0004\u0012\u00020\u000f0\u000eH\u0016J\u0012\u0010\u0010\u001a\u00020\u000c2\u0008\u0010\u0011\u001a\u0004\u0018\u00010\u0012H\u0014J\u0008\u0010\u0013\u001a\u00020\u000cH\u0014J\u000e\u0010\u0014\u001a\u0008\u0012\u0004\u0012\u00020\u000c0\u000eH\u0016J\u0008\u0010\u0015\u001a\u00020\u000cH\u0014R\u001e\u0010\u0004\u001a\u00020\u00058\u0006@\u0006X\u0087.\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u0006\u0010\u0007\"\u0004\u0008\u0008\u0010\tR\u0014\u0010\n\u001a\u0008\u0012\u0004\u0012\u00020\u000c0\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0017"
    }
    d2 = {
        "Lco/uk/getmondo/signup/identity_verification/chat_with_us/ChatWithUsActivity;",
        "Lco/uk/getmondo/common/activities/BaseActivity;",
        "Lco/uk/getmondo/signup/identity_verification/chat_with_us/ChatWithUsPresenter$View;",
        "()V",
        "presenter",
        "Lco/uk/getmondo/signup/identity_verification/chat_with_us/ChatWithUsPresenter;",
        "getPresenter",
        "()Lco/uk/getmondo/signup/identity_verification/chat_with_us/ChatWithUsPresenter;",
        "setPresenter",
        "(Lco/uk/getmondo/signup/identity_verification/chat_with_us/ChatWithUsPresenter;)V",
        "resumeRelay",
        "Lcom/jakewharton/rxrelay2/PublishRelay;",
        "",
        "onChatClicked",
        "Lio/reactivex/Observable;",
        "",
        "onCreate",
        "savedInstanceState",
        "Landroid/os/Bundle;",
        "onDestroy",
        "onRefresh",
        "onResume",
        "Companion",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# static fields
.field public static final b:Lco/uk/getmondo/signup/identity_verification/chat_with_us/ChatWithUsActivity$a;


# instance fields
.field public a:Lco/uk/getmondo/signup/identity_verification/chat_with_us/b;

.field private final c:Lcom/b/b/c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/b/b/c",
            "<",
            "Lkotlin/n;",
            ">;"
        }
    .end annotation
.end field

.field private e:Ljava/util/HashMap;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lco/uk/getmondo/signup/identity_verification/chat_with_us/ChatWithUsActivity$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lco/uk/getmondo/signup/identity_verification/chat_with_us/ChatWithUsActivity$a;-><init>(Lkotlin/d/b/i;)V

    sput-object v0, Lco/uk/getmondo/signup/identity_verification/chat_with_us/ChatWithUsActivity;->b:Lco/uk/getmondo/signup/identity_verification/chat_with_us/ChatWithUsActivity$a;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 14
    invoke-direct {p0}, Lco/uk/getmondo/common/activities/b;-><init>()V

    .line 16
    invoke-static {}, Lcom/b/b/c;->a()Lcom/b/b/c;

    move-result-object v0

    const-string v1, "PublishRelay.create()"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v0, p0, Lco/uk/getmondo/signup/identity_verification/chat_with_us/ChatWithUsActivity;->c:Lcom/b/b/c;

    return-void
.end method


# virtual methods
.method public a(I)Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/chat_with_us/ChatWithUsActivity;->e:Ljava/util/HashMap;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lco/uk/getmondo/signup/identity_verification/chat_with_us/ChatWithUsActivity;->e:Ljava/util/HashMap;

    :cond_0
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/chat_with_us/ChatWithUsActivity;->e:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    if-nez v0, :cond_1

    invoke-virtual {p0, p1}, Landroid/support/v4/app/j;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/signup/identity_verification/chat_with_us/ChatWithUsActivity;->e:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    return-object v0
.end method

.method public a()Lio/reactivex/n;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 50
    sget v0, Lco/uk/getmondo/c$a;->chatWithUsButton:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/identity_verification/chat_with_us/ChatWithUsActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    invoke-static {v0}, Lcom/b/a/c/c;->a(Landroid/view/View;)Lio/reactivex/n;

    move-result-object v0

    const-string v1, "RxView.clicks(chatWithUsButton)"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 27
    invoke-super {p0, p1}, Lco/uk/getmondo/common/activities/b;->onCreate(Landroid/os/Bundle;)V

    .line 29
    const v0, 0x7f050029

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/identity_verification/chat_with_us/ChatWithUsActivity;->setContentView(I)V

    .line 31
    invoke-virtual {p0}, Lco/uk/getmondo/signup/identity_verification/chat_with_us/ChatWithUsActivity;->l()Lco/uk/getmondo/common/h/b/b;

    move-result-object v0

    invoke-interface {v0, p0}, Lco/uk/getmondo/common/h/b/b;->a(Lco/uk/getmondo/signup/identity_verification/chat_with_us/ChatWithUsActivity;)V

    .line 32
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/chat_with_us/ChatWithUsActivity;->a:Lco/uk/getmondo/signup/identity_verification/chat_with_us/b;

    if-nez v0, :cond_0

    const-string v1, "presenter"

    invoke-static {v1}, Lkotlin/d/b/l;->b(Ljava/lang/String;)V

    :cond_0
    check-cast p0, Lco/uk/getmondo/signup/identity_verification/chat_with_us/b$a;

    invoke-virtual {v0, p0}, Lco/uk/getmondo/signup/identity_verification/chat_with_us/b;->a(Lco/uk/getmondo/signup/identity_verification/chat_with_us/b$a;)V

    .line 33
    return-void
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    .line 41
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/chat_with_us/ChatWithUsActivity;->a:Lco/uk/getmondo/signup/identity_verification/chat_with_us/b;

    if-nez v0, :cond_0

    const-string v1, "presenter"

    invoke-static {v1}, Lkotlin/d/b/l;->b(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0}, Lco/uk/getmondo/signup/identity_verification/chat_with_us/b;->b()V

    .line 42
    invoke-super {p0}, Lco/uk/getmondo/common/activities/b;->onDestroy()V

    .line 43
    return-void
.end method

.method protected onResume()V
    .locals 2

    .prologue
    .line 36
    invoke-super {p0}, Lco/uk/getmondo/common/activities/b;->onResume()V

    .line 37
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/chat_with_us/ChatWithUsActivity;->c:Lcom/b/b/c;

    sget-object v1, Lkotlin/n;->a:Lkotlin/n;

    invoke-virtual {v0, v1}, Lcom/b/b/c;->a(Ljava/lang/Object;)V

    .line 38
    return-void
.end method
