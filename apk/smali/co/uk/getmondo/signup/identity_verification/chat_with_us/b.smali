.class public final Lco/uk/getmondo/signup/identity_verification/chat_with_us/b;
.super Lco/uk/getmondo/common/ui/b;
.source "ChatWithUsPresenter.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lco/uk/getmondo/signup/identity_verification/chat_with_us/b$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lco/uk/getmondo/common/ui/b",
        "<",
        "Lco/uk/getmondo/signup/identity_verification/chat_with_us/b$a;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u0003\u0008\u0007\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00020\u0001:\u0001\tB\u000f\u0008\u0007\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u00a2\u0006\u0002\u0010\u0005J\u0010\u0010\u0006\u001a\u00020\u00072\u0006\u0010\u0008\u001a\u00020\u0002H\u0016R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\n"
    }
    d2 = {
        "Lco/uk/getmondo/signup/identity_verification/chat_with_us/ChatWithUsPresenter;",
        "Lco/uk/getmondo/common/ui/BasePresenter;",
        "Lco/uk/getmondo/signup/identity_verification/chat_with_us/ChatWithUsPresenter$View;",
        "intercomService",
        "Lco/uk/getmondo/common/IntercomService;",
        "(Lco/uk/getmondo/common/IntercomService;)V",
        "register",
        "",
        "view",
        "View",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field private final c:Lco/uk/getmondo/common/q;


# direct methods
.method public constructor <init>(Lco/uk/getmondo/common/q;)V
    .locals 1

    .prologue
    const-string v0, "intercomService"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 17
    invoke-direct {p0}, Lco/uk/getmondo/common/ui/b;-><init>()V

    iput-object p1, p0, Lco/uk/getmondo/signup/identity_verification/chat_with_us/b;->c:Lco/uk/getmondo/common/q;

    return-void
.end method

.method public static final synthetic a(Lco/uk/getmondo/signup/identity_verification/chat_with_us/b;)Lco/uk/getmondo/common/q;
    .locals 1

    .prologue
    .line 14
    iget-object v0, p0, Lco/uk/getmondo/signup/identity_verification/chat_with_us/b;->c:Lco/uk/getmondo/common/q;

    return-object v0
.end method


# virtual methods
.method public bridge synthetic a(Lco/uk/getmondo/common/ui/f;)V
    .locals 0

    .prologue
    .line 14
    check-cast p1, Lco/uk/getmondo/signup/identity_verification/chat_with_us/b$a;

    invoke-virtual {p0, p1}, Lco/uk/getmondo/signup/identity_verification/chat_with_us/b;->a(Lco/uk/getmondo/signup/identity_verification/chat_with_us/b$a;)V

    return-void
.end method

.method public a(Lco/uk/getmondo/signup/identity_verification/chat_with_us/b$a;)V
    .locals 4

    .prologue
    const-string v0, "view"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v0, p1

    .line 20
    check-cast v0, Lco/uk/getmondo/common/ui/f;

    invoke-super {p0, v0}, Lco/uk/getmondo/common/ui/b;->a(Lco/uk/getmondo/common/ui/f;)V

    .line 22
    iget-object v2, p0, Lco/uk/getmondo/signup/identity_verification/chat_with_us/b;->b:Lio/reactivex/b/a;

    .line 23
    invoke-interface {p1}, Lco/uk/getmondo/signup/identity_verification/chat_with_us/b$a;->a()Lio/reactivex/n;

    move-result-object v3

    new-instance v0, Lco/uk/getmondo/signup/identity_verification/chat_with_us/b$b;

    invoke-direct {v0, p0}, Lco/uk/getmondo/signup/identity_verification/chat_with_us/b$b;-><init>(Lco/uk/getmondo/signup/identity_verification/chat_with_us/b;)V

    check-cast v0, Lio/reactivex/c/g;

    sget-object v1, Lco/uk/getmondo/signup/identity_verification/chat_with_us/b$c;->a:Lco/uk/getmondo/signup/identity_verification/chat_with_us/b$c;

    check-cast v1, Lio/reactivex/c/g;

    invoke-virtual {v3, v0, v1}, Lio/reactivex/n;->subscribe(Lio/reactivex/c/g;Lio/reactivex/c/g;)Lio/reactivex/b/b;

    move-result-object v0

    const-string v1, "view.onChatClicked()\n   \u2026om() }, { Timber.e(it) })"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 22
    invoke-static {v2, v0}, Lco/uk/getmondo/common/j/f;->a(Lio/reactivex/b/a;Lio/reactivex/b/b;)Lio/reactivex/b/a;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/signup/identity_verification/chat_with_us/b;->b:Lio/reactivex/b/a;

    .line 24
    return-void
.end method
