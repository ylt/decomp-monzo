.class public final Lco/uk/getmondo/signup/status/a;
.super Ljava/lang/Object;
.source "SignupStatusActivity_MembersInjector.java"

# interfaces
.implements Lb/a;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lb/a",
        "<",
        "Lco/uk/getmondo/signup/status/SignupStatusActivity;",
        ">;"
    }
.end annotation


# static fields
.field static final synthetic a:Z


# instance fields
.field private final b:Ljavax/a/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/signup/status/d;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 7
    const-class v0, Lco/uk/getmondo/signup/status/a;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lco/uk/getmondo/signup/status/a;->a:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Ljavax/a/a;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/signup/status/d;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    sget-boolean v0, Lco/uk/getmondo/signup/status/a;->a:Z

    if-nez v0, :cond_0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 17
    :cond_0
    iput-object p1, p0, Lco/uk/getmondo/signup/status/a;->b:Ljavax/a/a;

    .line 18
    return-void
.end method

.method public static a(Ljavax/a/a;)Lb/a;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/signup/status/d;",
            ">;)",
            "Lb/a",
            "<",
            "Lco/uk/getmondo/signup/status/SignupStatusActivity;",
            ">;"
        }
    .end annotation

    .prologue
    .line 22
    new-instance v0, Lco/uk/getmondo/signup/status/a;

    invoke-direct {v0, p0}, Lco/uk/getmondo/signup/status/a;-><init>(Ljavax/a/a;)V

    return-object v0
.end method


# virtual methods
.method public a(Lco/uk/getmondo/signup/status/SignupStatusActivity;)V
    .locals 2

    .prologue
    .line 27
    if-nez p1, :cond_0

    .line 28
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Cannot inject members into a null reference"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 30
    :cond_0
    iget-object v0, p0, Lco/uk/getmondo/signup/status/a;->b:Ljavax/a/a;

    invoke-interface {v0}, Ljavax/a/a;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/signup/status/d;

    iput-object v0, p1, Lco/uk/getmondo/signup/status/SignupStatusActivity;->b:Lco/uk/getmondo/signup/status/d;

    .line 31
    return-void
.end method

.method public bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 7
    check-cast p1, Lco/uk/getmondo/signup/status/SignupStatusActivity;

    invoke-virtual {p0, p1}, Lco/uk/getmondo/signup/status/a;->a(Lco/uk/getmondo/signup/status/SignupStatusActivity;)V

    return-void
.end method
