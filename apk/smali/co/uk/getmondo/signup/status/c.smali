.class public final Lco/uk/getmondo/signup/status/c;
.super Ljava/lang/Object;
.source "SignupStatusManager_Factory.java"

# interfaces
.implements Lb/a/b;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lb/a/b",
        "<",
        "Lco/uk/getmondo/signup/status/b;",
        ">;"
    }
.end annotation


# static fields
.field static final synthetic a:Z


# instance fields
.field private final b:Ljavax/a/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/api/SignupApi;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Ljavax/a/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/api/MigrationApi;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Ljavax/a/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/api/b/a;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Ljavax/a/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/signup/status/g;",
            ">;"
        }
    .end annotation
.end field

.field private final f:Ljavax/a/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/migration/d;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 11
    const-class v0, Lco/uk/getmondo/signup/status/c;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lco/uk/getmondo/signup/status/c;->a:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/api/SignupApi;",
            ">;",
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/api/MigrationApi;",
            ">;",
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/api/b/a;",
            ">;",
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/signup/status/g;",
            ">;",
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/migration/d;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    sget-boolean v0, Lco/uk/getmondo/signup/status/c;->a:Z

    if-nez v0, :cond_0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 33
    :cond_0
    iput-object p1, p0, Lco/uk/getmondo/signup/status/c;->b:Ljavax/a/a;

    .line 34
    sget-boolean v0, Lco/uk/getmondo/signup/status/c;->a:Z

    if-nez v0, :cond_1

    if-nez p2, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 35
    :cond_1
    iput-object p2, p0, Lco/uk/getmondo/signup/status/c;->c:Ljavax/a/a;

    .line 36
    sget-boolean v0, Lco/uk/getmondo/signup/status/c;->a:Z

    if-nez v0, :cond_2

    if-nez p3, :cond_2

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 37
    :cond_2
    iput-object p3, p0, Lco/uk/getmondo/signup/status/c;->d:Ljavax/a/a;

    .line 38
    sget-boolean v0, Lco/uk/getmondo/signup/status/c;->a:Z

    if-nez v0, :cond_3

    if-nez p4, :cond_3

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 39
    :cond_3
    iput-object p4, p0, Lco/uk/getmondo/signup/status/c;->e:Ljavax/a/a;

    .line 40
    sget-boolean v0, Lco/uk/getmondo/signup/status/c;->a:Z

    if-nez v0, :cond_4

    if-nez p5, :cond_4

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 41
    :cond_4
    iput-object p5, p0, Lco/uk/getmondo/signup/status/c;->f:Ljavax/a/a;

    .line 42
    return-void
.end method

.method public static a(Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;)Lb/a/b;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/api/SignupApi;",
            ">;",
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/api/MigrationApi;",
            ">;",
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/api/b/a;",
            ">;",
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/signup/status/g;",
            ">;",
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/migration/d;",
            ">;)",
            "Lb/a/b",
            "<",
            "Lco/uk/getmondo/signup/status/b;",
            ">;"
        }
    .end annotation

    .prologue
    .line 60
    new-instance v0, Lco/uk/getmondo/signup/status/c;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lco/uk/getmondo/signup/status/c;-><init>(Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;)V

    return-object v0
.end method


# virtual methods
.method public a()Lco/uk/getmondo/signup/status/b;
    .locals 6

    .prologue
    .line 46
    new-instance v0, Lco/uk/getmondo/signup/status/b;

    iget-object v1, p0, Lco/uk/getmondo/signup/status/c;->b:Ljavax/a/a;

    .line 47
    invoke-interface {v1}, Ljavax/a/a;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lco/uk/getmondo/api/SignupApi;

    iget-object v2, p0, Lco/uk/getmondo/signup/status/c;->c:Ljavax/a/a;

    .line 48
    invoke-interface {v2}, Ljavax/a/a;->b()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lco/uk/getmondo/api/MigrationApi;

    iget-object v3, p0, Lco/uk/getmondo/signup/status/c;->d:Ljavax/a/a;

    .line 49
    invoke-interface {v3}, Ljavax/a/a;->b()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lco/uk/getmondo/api/b/a;

    iget-object v4, p0, Lco/uk/getmondo/signup/status/c;->e:Ljavax/a/a;

    .line 50
    invoke-interface {v4}, Ljavax/a/a;->b()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lco/uk/getmondo/signup/status/g;

    iget-object v5, p0, Lco/uk/getmondo/signup/status/c;->f:Ljavax/a/a;

    .line 51
    invoke-interface {v5}, Ljavax/a/a;->b()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lco/uk/getmondo/migration/d;

    invoke-direct/range {v0 .. v5}, Lco/uk/getmondo/signup/status/b;-><init>(Lco/uk/getmondo/api/SignupApi;Lco/uk/getmondo/api/MigrationApi;Lco/uk/getmondo/api/b/a;Lco/uk/getmondo/signup/status/g;Lco/uk/getmondo/migration/d;)V

    .line 46
    return-object v0
.end method

.method public synthetic b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 11
    invoke-virtual {p0}, Lco/uk/getmondo/signup/status/c;->a()Lco/uk/getmondo/signup/status/b;

    move-result-object v0

    return-object v0
.end method
