.class final Lco/uk/getmondo/signup/status/b$f;
.super Ljava/lang/Object;
.source "SignupStatusManager.kt"

# interfaces
.implements Lio/reactivex/c/g;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lco/uk/getmondo/signup/status/b;->a(Z)Lio/reactivex/v;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/g",
        "<",
        "Lco/uk/getmondo/api/model/signup/SignupInfo;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "",
        "it",
        "Lco/uk/getmondo/api/model/signup/SignupInfo;",
        "kotlin.jvm.PlatformType",
        "accept"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field final synthetic a:Lco/uk/getmondo/signup/status/b;


# direct methods
.method constructor <init>(Lco/uk/getmondo/signup/status/b;)V
    .locals 0

    iput-object p1, p0, Lco/uk/getmondo/signup/status/b$f;->a:Lco/uk/getmondo/signup/status/b;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lco/uk/getmondo/api/model/signup/SignupInfo;)V
    .locals 3

    .prologue
    .line 37
    iget-object v0, p0, Lco/uk/getmondo/signup/status/b$f;->a:Lco/uk/getmondo/signup/status/b;

    invoke-static {v0}, Lco/uk/getmondo/signup/status/b;->a(Lco/uk/getmondo/signup/status/b;)Lco/uk/getmondo/signup/status/g;

    move-result-object v0

    invoke-virtual {p1}, Lco/uk/getmondo/api/model/signup/SignupInfo;->a()Lco/uk/getmondo/api/model/signup/SignupInfo$Status;

    move-result-object v1

    sget-object v2, Lco/uk/getmondo/api/model/signup/SignupInfo$Status;->COMPLETED:Lco/uk/getmondo/api/model/signup/SignupInfo$Status;

    invoke-static {v1, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    invoke-virtual {v0, v1}, Lco/uk/getmondo/signup/status/g;->a(Z)V

    return-void
.end method

.method public bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 15
    check-cast p1, Lco/uk/getmondo/api/model/signup/SignupInfo;

    invoke-virtual {p0, p1}, Lco/uk/getmondo/signup/status/b$f;->a(Lco/uk/getmondo/api/model/signup/SignupInfo;)V

    return-void
.end method
