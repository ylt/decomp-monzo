.class public final synthetic Lco/uk/getmondo/signup/status/e;
.super Ljava/lang/Object;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# static fields
.field public static final synthetic a:[I

.field public static final synthetic b:[I

.field public static final synthetic c:[I


# direct methods
.method static synthetic constructor <clinit>()V
    .locals 7

    const/4 v6, 0x5

    const/4 v5, 0x4

    const/4 v4, 0x3

    const/4 v3, 0x2

    const/4 v2, 0x1

    invoke-static {}, Lco/uk/getmondo/api/model/signup/SignupInfo$Status;->values()[Lco/uk/getmondo/api/model/signup/SignupInfo$Status;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lco/uk/getmondo/signup/status/e;->a:[I

    sget-object v0, Lco/uk/getmondo/signup/status/e;->a:[I

    sget-object v1, Lco/uk/getmondo/api/model/signup/SignupInfo$Status;->COMPLETED:Lco/uk/getmondo/api/model/signup/SignupInfo$Status;

    invoke-virtual {v1}, Lco/uk/getmondo/api/model/signup/SignupInfo$Status;->ordinal()I

    move-result v1

    aput v2, v0, v1

    sget-object v0, Lco/uk/getmondo/signup/status/e;->a:[I

    sget-object v1, Lco/uk/getmondo/api/model/signup/SignupInfo$Status;->IN_PROGRESS:Lco/uk/getmondo/api/model/signup/SignupInfo$Status;

    invoke-virtual {v1}, Lco/uk/getmondo/api/model/signup/SignupInfo$Status;->ordinal()I

    move-result v1

    aput v3, v0, v1

    sget-object v0, Lco/uk/getmondo/signup/status/e;->a:[I

    sget-object v1, Lco/uk/getmondo/api/model/signup/SignupInfo$Status;->APPROVED:Lco/uk/getmondo/api/model/signup/SignupInfo$Status;

    invoke-virtual {v1}, Lco/uk/getmondo/api/model/signup/SignupInfo$Status;->ordinal()I

    move-result v1

    aput v4, v0, v1

    sget-object v0, Lco/uk/getmondo/signup/status/e;->a:[I

    sget-object v1, Lco/uk/getmondo/api/model/signup/SignupInfo$Status;->REJECTED:Lco/uk/getmondo/api/model/signup/SignupInfo$Status;

    invoke-virtual {v1}, Lco/uk/getmondo/api/model/signup/SignupInfo$Status;->ordinal()I

    move-result v1

    aput v5, v0, v1

    sget-object v0, Lco/uk/getmondo/signup/status/e;->a:[I

    sget-object v1, Lco/uk/getmondo/api/model/signup/SignupInfo$Status;->NOT_STARTED:Lco/uk/getmondo/api/model/signup/SignupInfo$Status;

    invoke-virtual {v1}, Lco/uk/getmondo/api/model/signup/SignupInfo$Status;->ordinal()I

    move-result v1

    aput v6, v0, v1

    invoke-static {}, Lco/uk/getmondo/api/model/signup/SignupInfo$Stage;->values()[Lco/uk/getmondo/api/model/signup/SignupInfo$Stage;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lco/uk/getmondo/signup/status/e;->b:[I

    sget-object v0, Lco/uk/getmondo/signup/status/e;->b:[I

    sget-object v1, Lco/uk/getmondo/api/model/signup/SignupInfo$Stage;->WAIT_LIST_SIGNUP:Lco/uk/getmondo/api/model/signup/SignupInfo$Stage;

    invoke-virtual {v1}, Lco/uk/getmondo/api/model/signup/SignupInfo$Stage;->ordinal()I

    move-result v1

    aput v2, v0, v1

    sget-object v0, Lco/uk/getmondo/signup/status/e;->b:[I

    sget-object v1, Lco/uk/getmondo/api/model/signup/SignupInfo$Stage;->WAIT_LIST:Lco/uk/getmondo/api/model/signup/SignupInfo$Stage;

    invoke-virtual {v1}, Lco/uk/getmondo/api/model/signup/SignupInfo$Stage;->ordinal()I

    move-result v1

    aput v3, v0, v1

    invoke-static {}, Lco/uk/getmondo/api/model/signup/SignupInfo$Stage;->values()[Lco/uk/getmondo/api/model/signup/SignupInfo$Stage;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lco/uk/getmondo/signup/status/e;->c:[I

    sget-object v0, Lco/uk/getmondo/signup/status/e;->c:[I

    sget-object v1, Lco/uk/getmondo/api/model/signup/SignupInfo$Stage;->PROFILE_DETAILS:Lco/uk/getmondo/api/model/signup/SignupInfo$Stage;

    invoke-virtual {v1}, Lco/uk/getmondo/api/model/signup/SignupInfo$Stage;->ordinal()I

    move-result v1

    aput v2, v0, v1

    sget-object v0, Lco/uk/getmondo/signup/status/e;->c:[I

    sget-object v1, Lco/uk/getmondo/api/model/signup/SignupInfo$Stage;->PHONE_VERIFICATION:Lco/uk/getmondo/api/model/signup/SignupInfo$Stage;

    invoke-virtual {v1}, Lco/uk/getmondo/api/model/signup/SignupInfo$Stage;->ordinal()I

    move-result v1

    aput v3, v0, v1

    sget-object v0, Lco/uk/getmondo/signup/status/e;->c:[I

    sget-object v1, Lco/uk/getmondo/api/model/signup/SignupInfo$Stage;->MARKETING_OPT_IN:Lco/uk/getmondo/api/model/signup/SignupInfo$Stage;

    invoke-virtual {v1}, Lco/uk/getmondo/api/model/signup/SignupInfo$Stage;->ordinal()I

    move-result v1

    aput v4, v0, v1

    sget-object v0, Lco/uk/getmondo/signup/status/e;->c:[I

    sget-object v1, Lco/uk/getmondo/api/model/signup/SignupInfo$Stage;->LEGAL_DOCUMENTS:Lco/uk/getmondo/api/model/signup/SignupInfo$Stage;

    invoke-virtual {v1}, Lco/uk/getmondo/api/model/signup/SignupInfo$Stage;->ordinal()I

    move-result v1

    aput v5, v0, v1

    sget-object v0, Lco/uk/getmondo/signup/status/e;->c:[I

    sget-object v1, Lco/uk/getmondo/api/model/signup/SignupInfo$Stage;->IDENTITY_VERIFICATION:Lco/uk/getmondo/api/model/signup/SignupInfo$Stage;

    invoke-virtual {v1}, Lco/uk/getmondo/api/model/signup/SignupInfo$Stage;->ordinal()I

    move-result v1

    aput v6, v0, v1

    sget-object v0, Lco/uk/getmondo/signup/status/e;->c:[I

    sget-object v1, Lco/uk/getmondo/api/model/signup/SignupInfo$Stage;->TAX_RESIDENCY:Lco/uk/getmondo/api/model/signup/SignupInfo$Stage;

    invoke-virtual {v1}, Lco/uk/getmondo/api/model/signup/SignupInfo$Stage;->ordinal()I

    move-result v1

    const/4 v2, 0x6

    aput v2, v0, v1

    sget-object v0, Lco/uk/getmondo/signup/status/e;->c:[I

    sget-object v1, Lco/uk/getmondo/api/model/signup/SignupInfo$Stage;->CARD_ORDER:Lco/uk/getmondo/api/model/signup/SignupInfo$Stage;

    invoke-virtual {v1}, Lco/uk/getmondo/api/model/signup/SignupInfo$Stage;->ordinal()I

    move-result v1

    const/4 v2, 0x7

    aput v2, v0, v1

    sget-object v0, Lco/uk/getmondo/signup/status/e;->c:[I

    sget-object v1, Lco/uk/getmondo/api/model/signup/SignupInfo$Stage;->CARD_ACTIVATION:Lco/uk/getmondo/api/model/signup/SignupInfo$Stage;

    invoke-virtual {v1}, Lco/uk/getmondo/api/model/signup/SignupInfo$Stage;->ordinal()I

    move-result v1

    const/16 v2, 0x8

    aput v2, v0, v1

    sget-object v0, Lco/uk/getmondo/signup/status/e;->c:[I

    sget-object v1, Lco/uk/getmondo/api/model/signup/SignupInfo$Stage;->PENDING:Lco/uk/getmondo/api/model/signup/SignupInfo$Stage;

    invoke-virtual {v1}, Lco/uk/getmondo/api/model/signup/SignupInfo$Stage;->ordinal()I

    move-result v1

    const/16 v2, 0x9

    aput v2, v0, v1

    sget-object v0, Lco/uk/getmondo/signup/status/e;->c:[I

    sget-object v1, Lco/uk/getmondo/api/model/signup/SignupInfo$Stage;->DEVICE_AUTHENTICATION_ENROLMENT:Lco/uk/getmondo/api/model/signup/SignupInfo$Stage;

    invoke-virtual {v1}, Lco/uk/getmondo/api/model/signup/SignupInfo$Stage;->ordinal()I

    move-result v1

    const/16 v2, 0xa

    aput v2, v0, v1

    sget-object v0, Lco/uk/getmondo/signup/status/e;->c:[I

    sget-object v1, Lco/uk/getmondo/api/model/signup/SignupInfo$Stage;->NONE:Lco/uk/getmondo/api/model/signup/SignupInfo$Stage;

    invoke-virtual {v1}, Lco/uk/getmondo/api/model/signup/SignupInfo$Stage;->ordinal()I

    move-result v1

    const/16 v2, 0xb

    aput v2, v0, v1

    sget-object v0, Lco/uk/getmondo/signup/status/e;->c:[I

    sget-object v1, Lco/uk/getmondo/api/model/signup/SignupInfo$Stage;->DONE:Lco/uk/getmondo/api/model/signup/SignupInfo$Stage;

    invoke-virtual {v1}, Lco/uk/getmondo/api/model/signup/SignupInfo$Stage;->ordinal()I

    move-result v1

    const/16 v2, 0xc

    aput v2, v0, v1

    sget-object v0, Lco/uk/getmondo/signup/status/e;->c:[I

    sget-object v1, Lco/uk/getmondo/api/model/signup/SignupInfo$Stage;->WAIT_LIST:Lco/uk/getmondo/api/model/signup/SignupInfo$Stage;

    invoke-virtual {v1}, Lco/uk/getmondo/api/model/signup/SignupInfo$Stage;->ordinal()I

    move-result v1

    const/16 v2, 0xd

    aput v2, v0, v1

    sget-object v0, Lco/uk/getmondo/signup/status/e;->c:[I

    sget-object v1, Lco/uk/getmondo/api/model/signup/SignupInfo$Stage;->WAIT_LIST_SIGNUP:Lco/uk/getmondo/api/model/signup/SignupInfo$Stage;

    invoke-virtual {v1}, Lco/uk/getmondo/api/model/signup/SignupInfo$Stage;->ordinal()I

    move-result v1

    const/16 v2, 0xe

    aput v2, v0, v1

    return-void
.end method
