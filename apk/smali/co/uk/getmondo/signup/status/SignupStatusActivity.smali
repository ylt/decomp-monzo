.class public final Lco/uk/getmondo/signup/status/SignupStatusActivity;
.super Lco/uk/getmondo/common/activities/b;
.source "SignupStatusActivity.kt"

# interfaces
.implements Lco/uk/getmondo/signup/status/d$a;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lco/uk/getmondo/signup/status/SignupStatusActivity$a;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000T\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u0004\n\u0002\u0010\u0008\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0010\n\u0002\u0010\u000e\n\u0002\u0008\u0003\u0018\u0000 32\u00020\u00012\u00020\u0002:\u00013B\u0005\u00a2\u0006\u0002\u0010\u0003J\u0008\u0010\u0014\u001a\u00020\u0012H\u0016J\"\u0010\u0015\u001a\u00020\u00122\u0006\u0010\u0016\u001a\u00020\u00172\u0006\u0010\u0018\u001a\u00020\u00172\u0008\u0010\u0019\u001a\u0004\u0018\u00010\u001aH\u0014J\u0012\u0010\u001b\u001a\u00020\u00122\u0008\u0010\u001c\u001a\u0004\u0018\u00010\u001dH\u0014J\u0008\u0010\u001e\u001a\u00020\u0012H\u0014J\u000e\u0010\u001f\u001a\u0008\u0012\u0004\u0012\u00020\u00120 H\u0016J\u000e\u0010!\u001a\u0008\u0012\u0004\u0012\u00020\u00120 H\u0016J\u0008\u0010\"\u001a\u00020\u0012H\u0016J\u0008\u0010#\u001a\u00020\u0012H\u0016J\u0008\u0010$\u001a\u00020\u0012H\u0016J\u0008\u0010%\u001a\u00020\u0012H\u0016J\u0008\u0010&\u001a\u00020\u0012H\u0016J\u0008\u0010\'\u001a\u00020\u0012H\u0016J\u0008\u0010(\u001a\u00020\u0012H\u0016J\u0008\u0010)\u001a\u00020\u0012H\u0016J\u0008\u0010*\u001a\u00020\u0012H\u0016J\u0008\u0010+\u001a\u00020\u0012H\u0016J\u0008\u0010,\u001a\u00020\u0012H\u0016J\u0008\u0010-\u001a\u00020\u0012H\u0016J\u0008\u0010.\u001a\u00020\u0012H\u0016J\u0010\u0010/\u001a\u00020\u00122\u0006\u00100\u001a\u000201H\u0016J\u0008\u00102\u001a\u00020\u0012H\u0016R\u001b\u0010\u0004\u001a\u00020\u00058VX\u0096\u0084\u0002\u00a2\u0006\u000c\n\u0004\u0008\u0008\u0010\t\u001a\u0004\u0008\u0006\u0010\u0007R\u001e\u0010\n\u001a\u00020\u000b8\u0006@\u0006X\u0087.\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u000c\u0010\r\"\u0004\u0008\u000e\u0010\u000fR2\u0010\u0010\u001a&\u0012\u000c\u0012\n \u0013*\u0004\u0018\u00010\u00120\u0012 \u0013*\u0012\u0012\u000c\u0012\n \u0013*\u0004\u0018\u00010\u00120\u0012\u0018\u00010\u00110\u0011X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u00064"
    }
    d2 = {
        "Lco/uk/getmondo/signup/status/SignupStatusActivity;",
        "Lco/uk/getmondo/common/activities/BaseActivity;",
        "Lco/uk/getmondo/signup/status/SignupStatusPresenter$View;",
        "()V",
        "entryPoint",
        "Lco/uk/getmondo/signup/SignupEntryPoint;",
        "getEntryPoint",
        "()Lco/uk/getmondo/signup/SignupEntryPoint;",
        "entryPoint$delegate",
        "Lkotlin/Lazy;",
        "presenter",
        "Lco/uk/getmondo/signup/status/SignupStatusPresenter;",
        "getPresenter",
        "()Lco/uk/getmondo/signup/status/SignupStatusPresenter;",
        "setPresenter",
        "(Lco/uk/getmondo/signup/status/SignupStatusPresenter;)V",
        "stageCompletedRelay",
        "Lcom/jakewharton/rxrelay2/PublishRelay;",
        "",
        "kotlin.jvm.PlatformType",
        "hideLoading",
        "onActivityResult",
        "requestCode",
        "",
        "resultCode",
        "data",
        "Landroid/content/Intent;",
        "onCreate",
        "savedInstanceState",
        "Landroid/os/Bundle;",
        "onDestroy",
        "onRetryClicked",
        "Lio/reactivex/Observable;",
        "onStageCompleted",
        "openCardActivation",
        "openCardOrdering",
        "openHome",
        "openIdentityVerification",
        "openLegalDocuments",
        "openMarketingOptIn",
        "openPhoneVerification",
        "openProfileCreation",
        "openSignUpPending",
        "openSignUpRejected",
        "openTaxResidency",
        "openWaitingList",
        "openWaitingListSignup",
        "showError",
        "message",
        "",
        "showLoading",
        "Companion",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# static fields
.field static final synthetic a:[Lkotlin/reflect/l;

.field public static final c:Lco/uk/getmondo/signup/status/SignupStatusActivity$a;


# instance fields
.field public b:Lco/uk/getmondo/signup/status/d;

.field private final e:Lcom/b/b/c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/b/b/c",
            "<",
            "Lkotlin/n;",
            ">;"
        }
    .end annotation
.end field

.field private final f:Lkotlin/c;

.field private g:Ljava/util/HashMap;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const/4 v0, 0x1

    new-array v1, v0, [Lkotlin/reflect/l;

    const/4 v2, 0x0

    new-instance v0, Lkotlin/d/b/w;

    const-class v3, Lco/uk/getmondo/signup/status/SignupStatusActivity;

    invoke-static {v3}, Lkotlin/d/b/y;->a(Ljava/lang/Class;)Lkotlin/reflect/c;

    move-result-object v3

    const-string v4, "entryPoint"

    const-string v5, "getEntryPoint()Lco/uk/getmondo/signup/SignupEntryPoint;"

    invoke-direct {v0, v3, v4, v5}, Lkotlin/d/b/w;-><init>(Lkotlin/reflect/e;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v0}, Lkotlin/d/b/y;->a(Lkotlin/d/b/v;)Lkotlin/reflect/n;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/l;

    aput-object v0, v1, v2

    sput-object v1, Lco/uk/getmondo/signup/status/SignupStatusActivity;->a:[Lkotlin/reflect/l;

    new-instance v0, Lco/uk/getmondo/signup/status/SignupStatusActivity$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lco/uk/getmondo/signup/status/SignupStatusActivity$a;-><init>(Lkotlin/d/b/i;)V

    sput-object v0, Lco/uk/getmondo/signup/status/SignupStatusActivity;->c:Lco/uk/getmondo/signup/status/SignupStatusActivity$a;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 33
    invoke-direct {p0}, Lco/uk/getmondo/common/activities/b;-><init>()V

    .line 34
    invoke-static {}, Lcom/b/b/c;->a()Lcom/b/b/c;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/signup/status/SignupStatusActivity;->e:Lcom/b/b/c;

    .line 62
    new-instance v0, Lco/uk/getmondo/signup/status/SignupStatusActivity$b;

    invoke-direct {v0, p0}, Lco/uk/getmondo/signup/status/SignupStatusActivity$b;-><init>(Lco/uk/getmondo/signup/status/SignupStatusActivity;)V

    check-cast v0, Lkotlin/d/a/a;

    invoke-static {v0}, Lkotlin/d;->a(Lkotlin/d/a/a;)Lkotlin/c;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/signup/status/SignupStatusActivity;->f:Lkotlin/c;

    return-void
.end method

.method public static final a(Landroid/content/Context;Lco/uk/getmondo/signup/j;)Landroid/content/Intent;
    .locals 1

    const-string v0, "context"

    invoke-static {p0, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "signupEntryPoint"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v0, Lco/uk/getmondo/signup/status/SignupStatusActivity;->c:Lco/uk/getmondo/signup/status/SignupStatusActivity$a;

    invoke-virtual {v0, p0, p1}, Lco/uk/getmondo/signup/status/SignupStatusActivity$a;->a(Landroid/content/Context;Lco/uk/getmondo/signup/j;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public A()V
    .locals 2

    .prologue
    .line 134
    move-object v0, p0

    check-cast v0, Landroid/content/Context;

    invoke-static {v0}, Lco/uk/getmondo/waitlist/WaitlistActivity;->a(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lco/uk/getmondo/signup/status/SignupStatusActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 135
    return-void
.end method

.method public B()V
    .locals 2

    .prologue
    .line 138
    move-object v0, p0

    check-cast v0, Landroid/content/Context;

    invoke-static {v0}, Lco/uk/getmondo/signup_old/CreateProfileActivity;->b(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lco/uk/getmondo/signup/status/SignupStatusActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 139
    return-void
.end method

.method public a(I)Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lco/uk/getmondo/signup/status/SignupStatusActivity;->g:Ljava/util/HashMap;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lco/uk/getmondo/signup/status/SignupStatusActivity;->g:Ljava/util/HashMap;

    :cond_0
    iget-object v0, p0, Lco/uk/getmondo/signup/status/SignupStatusActivity;->g:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    if-nez v0, :cond_1

    invoke-virtual {p0, p1}, Landroid/support/v4/app/j;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/signup/status/SignupStatusActivity;->g:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    return-object v0
.end method

.method public a()Lco/uk/getmondo/signup/j;
    .locals 3

    iget-object v0, p0, Lco/uk/getmondo/signup/status/SignupStatusActivity;->f:Lkotlin/c;

    sget-object v1, Lco/uk/getmondo/signup/status/SignupStatusActivity;->a:[Lkotlin/reflect/l;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-interface {v0}, Lkotlin/c;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/signup/j;

    return-object v0
.end method

.method public b()Lio/reactivex/n;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Lkotlin/n;",
            ">;"
        }
    .end annotation

    .prologue
    .line 64
    sget v0, Lco/uk/getmondo/c$a;->signupStatusErrorView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/status/SignupStatusActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/common/ui/LoadingErrorView;

    invoke-virtual {v0}, Lco/uk/getmondo/common/ui/LoadingErrorView;->c()Lio/reactivex/n;

    move-result-object v0

    return-object v0
.end method

.method public b(Ljava/lang/String;)V
    .locals 1

    .prologue
    const-string v0, "message"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 78
    sget v0, Lco/uk/getmondo/c$a;->signupStatusErrorView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/status/SignupStatusActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/common/ui/LoadingErrorView;

    invoke-virtual {v0, p1}, Lco/uk/getmondo/common/ui/LoadingErrorView;->setMessage(Ljava/lang/String;)V

    .line 79
    sget v0, Lco/uk/getmondo/c$a;->signupStatusErrorView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/status/SignupStatusActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/common/ui/LoadingErrorView;

    invoke-static {v0}, Lco/uk/getmondo/common/ae;->a(Landroid/view/View;)V

    .line 80
    return-void
.end method

.method public c()Lio/reactivex/n;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Lkotlin/n;",
            ">;"
        }
    .end annotation

    .prologue
    .line 66
    iget-object v0, p0, Lco/uk/getmondo/signup/status/SignupStatusActivity;->e:Lcom/b/b/c;

    const-string v1, "stageCompletedRelay"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lio/reactivex/n;

    return-object v0
.end method

.method public d()V
    .locals 1

    .prologue
    .line 69
    sget v0, Lco/uk/getmondo/c$a;->signupStatusProgress:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/status/SignupStatusActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    invoke-static {v0}, Lco/uk/getmondo/common/ae;->a(Landroid/view/View;)V

    .line 70
    sget v0, Lco/uk/getmondo/c$a;->signupStatusErrorView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/status/SignupStatusActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/common/ui/LoadingErrorView;

    invoke-static {v0}, Lco/uk/getmondo/common/ae;->b(Landroid/view/View;)V

    .line 71
    return-void
.end method

.method public e()V
    .locals 1

    .prologue
    .line 74
    sget v0, Lco/uk/getmondo/c$a;->signupStatusProgress:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/status/SignupStatusActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    invoke-static {v0}, Lco/uk/getmondo/common/ae;->b(Landroid/view/View;)V

    .line 75
    return-void
.end method

.method public f()V
    .locals 2

    .prologue
    .line 83
    sget-object v1, Lco/uk/getmondo/main/HomeActivity;->f:Lco/uk/getmondo/main/HomeActivity$a;

    move-object v0, p0

    check-cast v0, Landroid/content/Context;

    invoke-virtual {v1, v0}, Lco/uk/getmondo/main/HomeActivity$a;->b(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/status/SignupStatusActivity;->startActivity(Landroid/content/Intent;)V

    .line 84
    invoke-virtual {p0}, Lco/uk/getmondo/signup/status/SignupStatusActivity;->finishAffinity()V

    .line 85
    return-void
.end method

.method public g()V
    .locals 2

    .prologue
    .line 88
    sget-object v1, Lco/uk/getmondo/signup/rejected/SignupRejectedActivity;->b:Lco/uk/getmondo/signup/rejected/SignupRejectedActivity$a;

    move-object v0, p0

    check-cast v0, Landroid/content/Context;

    invoke-virtual {v1, v0}, Lco/uk/getmondo/signup/rejected/SignupRejectedActivity$a;->a(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lco/uk/getmondo/signup/status/SignupStatusActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 89
    return-void
.end method

.method public h()V
    .locals 3

    .prologue
    .line 92
    sget-object v1, Lco/uk/getmondo/signup/profile/ProfileCreationActivity;->a:Lco/uk/getmondo/signup/profile/ProfileCreationActivity$a;

    move-object v0, p0

    check-cast v0, Landroid/content/Context;

    invoke-virtual {p0}, Lco/uk/getmondo/signup/status/SignupStatusActivity;->a()Lco/uk/getmondo/signup/j;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lco/uk/getmondo/signup/profile/ProfileCreationActivity$a;->a(Landroid/content/Context;Lco/uk/getmondo/signup/j;)Landroid/content/Intent;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lco/uk/getmondo/signup/status/SignupStatusActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 93
    return-void
.end method

.method public i()V
    .locals 3

    .prologue
    .line 96
    sget-object v1, Lco/uk/getmondo/signup/phone_verification/PhoneVerificationActivity;->a:Lco/uk/getmondo/signup/phone_verification/PhoneVerificationActivity$a;

    move-object v0, p0

    check-cast v0, Landroid/content/Context;

    invoke-virtual {p0}, Lco/uk/getmondo/signup/status/SignupStatusActivity;->a()Lco/uk/getmondo/signup/j;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lco/uk/getmondo/signup/phone_verification/PhoneVerificationActivity$a;->a(Landroid/content/Context;Lco/uk/getmondo/signup/j;)Landroid/content/Intent;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lco/uk/getmondo/signup/status/SignupStatusActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 97
    return-void
.end method

.method public j()V
    .locals 3

    .prologue
    .line 100
    sget-object v1, Lco/uk/getmondo/signup/marketing_opt_in/MarketingOptInActivity;->a:Lco/uk/getmondo/signup/marketing_opt_in/MarketingOptInActivity$a;

    move-object v0, p0

    check-cast v0, Landroid/content/Context;

    invoke-virtual {p0}, Lco/uk/getmondo/signup/status/SignupStatusActivity;->a()Lco/uk/getmondo/signup/j;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lco/uk/getmondo/signup/marketing_opt_in/MarketingOptInActivity$a;->a(Landroid/content/Context;Lco/uk/getmondo/signup/j;)Landroid/content/Intent;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lco/uk/getmondo/signup/status/SignupStatusActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 101
    return-void
.end method

.method public k()V
    .locals 3

    .prologue
    .line 104
    sget-object v1, Lco/uk/getmondo/signup/documents/LegalDocumentsActivity;->a:Lco/uk/getmondo/signup/documents/LegalDocumentsActivity$a;

    move-object v0, p0

    check-cast v0, Landroid/content/Context;

    invoke-virtual {p0}, Lco/uk/getmondo/signup/status/SignupStatusActivity;->a()Lco/uk/getmondo/signup/j;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lco/uk/getmondo/signup/documents/LegalDocumentsActivity$a;->a(Landroid/content/Context;Lco/uk/getmondo/signup/j;)Landroid/content/Intent;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lco/uk/getmondo/signup/status/SignupStatusActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 105
    return-void
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 2

    .prologue
    .line 52
    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    .line 53
    packed-switch p2, :pswitch_data_0

    .line 59
    :goto_0
    return-void

    .line 54
    :pswitch_0
    invoke-virtual {p0}, Lco/uk/getmondo/signup/status/SignupStatusActivity;->finish()V

    goto :goto_0

    .line 55
    :pswitch_1
    iget-object v0, p0, Lco/uk/getmondo/signup/status/SignupStatusActivity;->e:Lcom/b/b/c;

    sget-object v1, Lkotlin/n;->a:Lkotlin/n;

    invoke-virtual {v0, v1}, Lcom/b/b/c;->a(Ljava/lang/Object;)V

    goto :goto_0

    .line 58
    :cond_0
    invoke-super {p0, p1, p2, p3}, Lco/uk/getmondo/common/activities/b;->onActivityResult(IILandroid/content/Intent;)V

    goto :goto_0

    .line 53
    nop

    :pswitch_data_0
    .packed-switch -0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 39
    invoke-super {p0, p1}, Lco/uk/getmondo/common/activities/b;->onCreate(Landroid/os/Bundle;)V

    .line 40
    const v0, 0x7f050062

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/status/SignupStatusActivity;->setContentView(I)V

    .line 42
    invoke-virtual {p0}, Lco/uk/getmondo/signup/status/SignupStatusActivity;->l()Lco/uk/getmondo/common/h/b/b;

    move-result-object v0

    invoke-interface {v0, p0}, Lco/uk/getmondo/common/h/b/b;->a(Lco/uk/getmondo/signup/status/SignupStatusActivity;)V

    .line 43
    iget-object v0, p0, Lco/uk/getmondo/signup/status/SignupStatusActivity;->b:Lco/uk/getmondo/signup/status/d;

    if-nez v0, :cond_0

    const-string v1, "presenter"

    invoke-static {v1}, Lkotlin/d/b/l;->b(Ljava/lang/String;)V

    :cond_0
    check-cast p0, Lco/uk/getmondo/signup/status/d$a;

    invoke-virtual {v0, p0}, Lco/uk/getmondo/signup/status/d;->a(Lco/uk/getmondo/signup/status/d$a;)V

    .line 44
    return-void
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    .line 47
    iget-object v0, p0, Lco/uk/getmondo/signup/status/SignupStatusActivity;->b:Lco/uk/getmondo/signup/status/d;

    if-nez v0, :cond_0

    const-string v1, "presenter"

    invoke-static {v1}, Lkotlin/d/b/l;->b(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0}, Lco/uk/getmondo/signup/status/d;->b()V

    .line 48
    invoke-super {p0}, Lco/uk/getmondo/common/activities/b;->onDestroy()V

    .line 49
    return-void
.end method

.method public v()V
    .locals 9

    .prologue
    const/4 v6, 0x0

    .line 108
    sget-object v0, Lco/uk/getmondo/signup/identity_verification/IdentityVerificationActivity;->g:Lco/uk/getmondo/signup/identity_verification/IdentityVerificationActivity$a;

    move-object v1, p0

    .line 109
    check-cast v1, Landroid/content/Context;

    .line 110
    sget-object v2, Lco/uk/getmondo/signup/identity_verification/a/j;->b:Lco/uk/getmondo/signup/identity_verification/a/j;

    .line 111
    sget-object v3, Lco/uk/getmondo/api/model/tracking/Impression$KycFrom;->SIGNUP:Lco/uk/getmondo/api/model/tracking/Impression$KycFrom;

    .line 112
    sget-object v4, Lco/uk/getmondo/api/model/signup/SignupSource;->PERSONAL_ACCOUNT:Lco/uk/getmondo/api/model/signup/SignupSource;

    .line 113
    invoke-virtual {p0}, Lco/uk/getmondo/signup/status/SignupStatusActivity;->a()Lco/uk/getmondo/signup/j;

    move-result-object v5

    const/16 v7, 0x20

    move-object v8, v6

    .line 108
    invoke-static/range {v0 .. v8}, Lco/uk/getmondo/signup/identity_verification/IdentityVerificationActivity$a;->a(Lco/uk/getmondo/signup/identity_verification/IdentityVerificationActivity$a;Landroid/content/Context;Lco/uk/getmondo/signup/identity_verification/a/j;Lco/uk/getmondo/api/model/tracking/Impression$KycFrom;Lco/uk/getmondo/api/model/signup/SignupSource;Lco/uk/getmondo/signup/j;Ljava/lang/String;ILjava/lang/Object;)Landroid/content/Intent;

    move-result-object v0

    .line 114
    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lco/uk/getmondo/signup/status/SignupStatusActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 115
    return-void
.end method

.method public w()V
    .locals 3

    .prologue
    .line 118
    sget-object v1, Lco/uk/getmondo/signup/tax_residency/TaxResidencyActivity;->b:Lco/uk/getmondo/signup/tax_residency/TaxResidencyActivity$a;

    move-object v0, p0

    check-cast v0, Landroid/content/Context;

    invoke-virtual {p0}, Lco/uk/getmondo/signup/status/SignupStatusActivity;->a()Lco/uk/getmondo/signup/j;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lco/uk/getmondo/signup/tax_residency/TaxResidencyActivity$a;->a(Landroid/content/Context;Lco/uk/getmondo/signup/j;)Landroid/content/Intent;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lco/uk/getmondo/signup/status/SignupStatusActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 119
    return-void
.end method

.method public x()V
    .locals 3

    .prologue
    .line 122
    sget-object v1, Lco/uk/getmondo/signup/card_ordering/OrderCardActivity;->a:Lco/uk/getmondo/signup/card_ordering/OrderCardActivity$a;

    move-object v0, p0

    check-cast v0, Landroid/content/Context;

    invoke-virtual {p0}, Lco/uk/getmondo/signup/status/SignupStatusActivity;->a()Lco/uk/getmondo/signup/j;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lco/uk/getmondo/signup/card_ordering/OrderCardActivity$a;->a(Landroid/content/Context;Lco/uk/getmondo/signup/j;)Landroid/content/Intent;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lco/uk/getmondo/signup/status/SignupStatusActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 123
    return-void
.end method

.method public y()V
    .locals 3

    .prologue
    .line 126
    sget-object v1, Lco/uk/getmondo/signup/card_activation/CardOnItsWayActivity;->g:Lco/uk/getmondo/signup/card_activation/CardOnItsWayActivity$a;

    move-object v0, p0

    check-cast v0, Landroid/content/Context;

    invoke-virtual {p0}, Lco/uk/getmondo/signup/status/SignupStatusActivity;->a()Lco/uk/getmondo/signup/j;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lco/uk/getmondo/signup/card_activation/CardOnItsWayActivity$a;->a(Landroid/content/Context;Lco/uk/getmondo/signup/j;)Landroid/content/Intent;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lco/uk/getmondo/signup/status/SignupStatusActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 127
    return-void
.end method

.method public z()V
    .locals 3

    .prologue
    .line 130
    sget-object v1, Lco/uk/getmondo/signup/pending/SignupPendingActivity;->b:Lco/uk/getmondo/signup/pending/SignupPendingActivity$a;

    move-object v0, p0

    check-cast v0, Landroid/content/Context;

    invoke-virtual {p0}, Lco/uk/getmondo/signup/status/SignupStatusActivity;->a()Lco/uk/getmondo/signup/j;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lco/uk/getmondo/signup/pending/SignupPendingActivity$a;->a(Landroid/content/Context;Lco/uk/getmondo/signup/j;)Landroid/content/Intent;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lco/uk/getmondo/signup/status/SignupStatusActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 131
    return-void
.end method
