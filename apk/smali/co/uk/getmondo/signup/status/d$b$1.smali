.class final Lco/uk/getmondo/signup/status/d$b$1;
.super Ljava/lang/Object;
.source "SignupStatusPresenter.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lco/uk/getmondo/signup/status/d$b;->a(Lkotlin/n;)Lio/reactivex/h;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h",
        "<TT;",
        "Lio/reactivex/z",
        "<+TR;>;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0010\u0000\u001a\u0016\u0012\u0004\u0012\u00020\u0002 \u0003*\n\u0012\u0004\u0012\u00020\u0002\u0018\u00010\u00010\u00012\u0006\u0010\u0004\u001a\u00020\u0002H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "Lio/reactivex/Single;",
        "Lco/uk/getmondo/api/model/signup/SignupInfo;",
        "kotlin.jvm.PlatformType",
        "it",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field final synthetic a:Lco/uk/getmondo/signup/status/d$b;


# direct methods
.method constructor <init>(Lco/uk/getmondo/signup/status/d$b;)V
    .locals 0

    iput-object p1, p0, Lco/uk/getmondo/signup/status/d$b$1;->a:Lco/uk/getmondo/signup/status/d$b;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lco/uk/getmondo/api/model/signup/SignupInfo;)Lio/reactivex/v;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lco/uk/getmondo/api/model/signup/SignupInfo;",
            ")",
            "Lio/reactivex/v",
            "<",
            "Lco/uk/getmondo/api/model/signup/SignupInfo;",
            ">;"
        }
    .end annotation

    .prologue
    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 38
    iget-object v0, p0, Lco/uk/getmondo/signup/status/d$b$1;->a:Lco/uk/getmondo/signup/status/d$b;

    iget-object v0, v0, Lco/uk/getmondo/signup/status/d$b;->a:Lco/uk/getmondo/signup/status/d;

    invoke-static {v0, p1}, Lco/uk/getmondo/signup/status/d;->a(Lco/uk/getmondo/signup/status/d;Lco/uk/getmondo/api/model/signup/SignupInfo;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lco/uk/getmondo/signup/status/d$b$1;->a:Lco/uk/getmondo/signup/status/d$b;

    iget-object v0, v0, Lco/uk/getmondo/signup/status/d$b;->a:Lco/uk/getmondo/signup/status/d;

    invoke-static {v0}, Lco/uk/getmondo/signup/status/d;->a(Lco/uk/getmondo/signup/status/d;)Lco/uk/getmondo/signup/status/b;

    move-result-object v0

    invoke-virtual {v0}, Lco/uk/getmondo/signup/status/b;->b()Lio/reactivex/v;

    move-result-object v0

    :goto_0
    return-object v0

    .line 40
    :cond_0
    invoke-virtual {p1}, Lco/uk/getmondo/api/model/signup/SignupInfo;->a()Lco/uk/getmondo/api/model/signup/SignupInfo$Status;

    move-result-object v0

    sget-object v1, Lco/uk/getmondo/api/model/signup/SignupInfo$Status;->IN_PROGRESS:Lco/uk/getmondo/api/model/signup/SignupInfo$Status;

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Lco/uk/getmondo/api/model/signup/SignupInfo;->b()Lco/uk/getmondo/api/model/signup/SignupInfo$Stage;

    move-result-object v0

    sget-object v1, Lco/uk/getmondo/api/model/signup/SignupInfo$Stage;->DEVICE_AUTHENTICATION_ENROLMENT:Lco/uk/getmondo/api/model/signup/SignupInfo$Stage;

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 42
    iget-object v0, p0, Lco/uk/getmondo/signup/status/d$b$1;->a:Lco/uk/getmondo/signup/status/d$b;

    iget-object v0, v0, Lco/uk/getmondo/signup/status/d$b;->a:Lco/uk/getmondo/signup/status/d;

    invoke-static {v0}, Lco/uk/getmondo/signup/status/d;->a(Lco/uk/getmondo/signup/status/d;)Lco/uk/getmondo/signup/status/b;

    move-result-object v0

    invoke-virtual {v0}, Lco/uk/getmondo/signup/status/b;->c()Lio/reactivex/v;

    move-result-object v0

    goto :goto_0

    .line 44
    :cond_1
    invoke-virtual {p1}, Lco/uk/getmondo/api/model/signup/SignupInfo;->a()Lco/uk/getmondo/api/model/signup/SignupInfo$Status;

    move-result-object v0

    sget-object v1, Lco/uk/getmondo/api/model/signup/SignupInfo$Status;->COMPLETED:Lco/uk/getmondo/api/model/signup/SignupInfo$Status;

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 46
    iget-object v0, p0, Lco/uk/getmondo/signup/status/d$b$1;->a:Lco/uk/getmondo/signup/status/d$b;

    iget-object v0, v0, Lco/uk/getmondo/signup/status/d$b;->a:Lco/uk/getmondo/signup/status/d;

    invoke-static {v0}, Lco/uk/getmondo/signup/status/d;->b(Lco/uk/getmondo/signup/status/d;)Lco/uk/getmondo/api/b/a;

    move-result-object v0

    invoke-virtual {v0}, Lco/uk/getmondo/api/b/a;->a()Lio/reactivex/v;

    move-result-object v1

    new-instance v0, Lco/uk/getmondo/signup/status/d$b$1$1;

    invoke-direct {v0, p1}, Lco/uk/getmondo/signup/status/d$b$1$1;-><init>(Lco/uk/getmondo/api/model/signup/SignupInfo;)V

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {v1, v0}, Lio/reactivex/v;->d(Lio/reactivex/c/h;)Lio/reactivex/v;

    move-result-object v0

    goto :goto_0

    .line 48
    :cond_2
    invoke-static {p1}, Lio/reactivex/v;->a(Ljava/lang/Object;)Lio/reactivex/v;

    move-result-object v0

    goto :goto_0
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 21
    check-cast p1, Lco/uk/getmondo/api/model/signup/SignupInfo;

    invoke-virtual {p0, p1}, Lco/uk/getmondo/signup/status/d$b$1;->a(Lco/uk/getmondo/api/model/signup/SignupInfo;)Lio/reactivex/v;

    move-result-object v0

    return-object v0
.end method
