.class final Lco/uk/getmondo/signup/status/b$d;
.super Ljava/lang/Object;
.source "SignupStatusManager.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lco/uk/getmondo/signup/status/b;->a(Lco/uk/getmondo/api/model/signup/SignupInfo;)Lio/reactivex/v;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h",
        "<",
        "Ljava/lang/Throwable;",
        "Lio/reactivex/z",
        "<+",
        "Lco/uk/getmondo/api/model/signup/SignupInfo;",
        ">;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000\u0014\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0003\n\u0000\u0010\u0000\u001a*\u0012\u000e\u0008\u0001\u0012\n \u0003*\u0004\u0018\u00010\u00020\u0002 \u0003*\u0014\u0012\u000e\u0008\u0001\u0012\n \u0003*\u0004\u0018\u00010\u00020\u0002\u0018\u00010\u00010\u00012\u0006\u0010\u0004\u001a\u00020\u0005H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "Lio/reactivex/SingleSource;",
        "Lco/uk/getmondo/api/model/signup/SignupInfo;",
        "kotlin.jvm.PlatformType",
        "it",
        "",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field final synthetic a:Lco/uk/getmondo/api/model/signup/SignupInfo;


# direct methods
.method constructor <init>(Lco/uk/getmondo/api/model/signup/SignupInfo;)V
    .locals 0

    iput-object p1, p0, Lco/uk/getmondo/signup/status/b$d;->a:Lco/uk/getmondo/api/model/signup/SignupInfo;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Throwable;)Lio/reactivex/z;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Throwable;",
            ")",
            "Lio/reactivex/z",
            "<+",
            "Lco/uk/getmondo/api/model/signup/SignupInfo;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 59
    invoke-static {p1}, Lco/uk/getmondo/common/e/c;->b(Ljava/lang/Throwable;)Ljava/lang/Integer;

    move-result-object v0

    if-nez v0, :cond_1

    .line 64
    :cond_0
    invoke-static {p1}, Lio/reactivex/v;->a(Ljava/lang/Throwable;)Lio/reactivex/v;

    move-result-object v0

    check-cast v0, Lio/reactivex/z;

    .line 59
    :goto_0
    return-object v0

    :cond_1
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/16 v1, 0x194

    if-ne v0, v1, :cond_0

    .line 62
    iget-object v0, p0, Lco/uk/getmondo/signup/status/b$d;->a:Lco/uk/getmondo/api/model/signup/SignupInfo;

    sget-object v1, Lco/uk/getmondo/api/model/signup/SignupInfo$Stage;->WAIT_LIST_SIGNUP:Lco/uk/getmondo/api/model/signup/SignupInfo$Stage;

    const/4 v2, 0x1

    invoke-static {v0, v3, v1, v2, v3}, Lco/uk/getmondo/api/model/signup/SignupInfo;->a(Lco/uk/getmondo/api/model/signup/SignupInfo;Lco/uk/getmondo/api/model/signup/SignupInfo$Status;Lco/uk/getmondo/api/model/signup/SignupInfo$Stage;ILjava/lang/Object;)Lco/uk/getmondo/api/model/signup/SignupInfo;

    move-result-object v0

    invoke-static {v0}, Lio/reactivex/v;->a(Ljava/lang/Object;)Lio/reactivex/v;

    move-result-object v0

    check-cast v0, Lio/reactivex/z;

    goto :goto_0
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 15
    check-cast p1, Ljava/lang/Throwable;

    invoke-virtual {p0, p1}, Lco/uk/getmondo/signup/status/b$d;->a(Ljava/lang/Throwable;)Lio/reactivex/z;

    move-result-object v0

    return-object v0
.end method
