.class final Lco/uk/getmondo/signup/status/d$b;
.super Ljava/lang/Object;
.source "SignupStatusPresenter.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lco/uk/getmondo/signup/status/d;->a(Lco/uk/getmondo/signup/status/d$a;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h",
        "<TT;",
        "Lio/reactivex/l",
        "<+TR;>;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000\u0016\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u0010\u0012\u000c\u0012\n \u0003*\u0004\u0018\u00010\u00020\u00020\u00012\u0006\u0010\u0004\u001a\u00020\u0005H\n\u00a2\u0006\u0004\u0008\u0006\u0010\u0007"
    }
    d2 = {
        "<anonymous>",
        "Lio/reactivex/Maybe;",
        "Lco/uk/getmondo/api/model/signup/SignupInfo;",
        "kotlin.jvm.PlatformType",
        "it",
        "",
        "apply",
        "(Lkotlin/Unit;)Lio/reactivex/Maybe;"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field final synthetic a:Lco/uk/getmondo/signup/status/d;

.field final synthetic b:Lco/uk/getmondo/signup/status/d$a;


# direct methods
.method constructor <init>(Lco/uk/getmondo/signup/status/d;Lco/uk/getmondo/signup/status/d$a;)V
    .locals 0

    iput-object p1, p0, Lco/uk/getmondo/signup/status/d$b;->a:Lco/uk/getmondo/signup/status/d;

    iput-object p2, p0, Lco/uk/getmondo/signup/status/d$b;->b:Lco/uk/getmondo/signup/status/d$a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lkotlin/n;)Lio/reactivex/h;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/n;",
            ")",
            "Lio/reactivex/h",
            "<",
            "Lco/uk/getmondo/api/model/signup/SignupInfo;",
            ">;"
        }
    .end annotation

    .prologue
    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 35
    iget-object v0, p0, Lco/uk/getmondo/signup/status/d$b;->b:Lco/uk/getmondo/signup/status/d$a;

    invoke-interface {v0}, Lco/uk/getmondo/signup/status/d$a;->a()Lco/uk/getmondo/signup/j;

    move-result-object v0

    sget-object v1, Lco/uk/getmondo/signup/j;->b:Lco/uk/getmondo/signup/j;

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    .line 36
    iget-object v1, p0, Lco/uk/getmondo/signup/status/d$b;->a:Lco/uk/getmondo/signup/status/d;

    invoke-static {v1}, Lco/uk/getmondo/signup/status/d;->a(Lco/uk/getmondo/signup/status/d;)Lco/uk/getmondo/signup/status/b;

    move-result-object v1

    invoke-virtual {v1, v0}, Lco/uk/getmondo/signup/status/b;->a(Z)Lio/reactivex/v;

    move-result-object v1

    .line 37
    new-instance v0, Lco/uk/getmondo/signup/status/d$b$1;

    invoke-direct {v0, p0}, Lco/uk/getmondo/signup/status/d$b$1;-><init>(Lco/uk/getmondo/signup/status/d$b;)V

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {v1, v0}, Lio/reactivex/v;->a(Lio/reactivex/c/h;)Lio/reactivex/v;

    move-result-object v0

    .line 51
    iget-object v1, p0, Lco/uk/getmondo/signup/status/d$b;->a:Lco/uk/getmondo/signup/status/d;

    invoke-static {v1}, Lco/uk/getmondo/signup/status/d;->c(Lco/uk/getmondo/signup/status/d;)Lio/reactivex/u;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/v;->b(Lio/reactivex/u;)Lio/reactivex/v;

    move-result-object v0

    .line 52
    iget-object v1, p0, Lco/uk/getmondo/signup/status/d$b;->a:Lco/uk/getmondo/signup/status/d;

    invoke-static {v1}, Lco/uk/getmondo/signup/status/d;->d(Lco/uk/getmondo/signup/status/d;)Lio/reactivex/u;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/v;->a(Lio/reactivex/u;)Lio/reactivex/v;

    move-result-object v1

    .line 53
    new-instance v0, Lco/uk/getmondo/signup/status/d$b$2;

    invoke-direct {v0, p0}, Lco/uk/getmondo/signup/status/d$b$2;-><init>(Lco/uk/getmondo/signup/status/d$b;)V

    check-cast v0, Lio/reactivex/c/g;

    invoke-virtual {v1, v0}, Lio/reactivex/v;->b(Lio/reactivex/c/g;)Lio/reactivex/v;

    move-result-object v1

    .line 54
    new-instance v0, Lco/uk/getmondo/signup/status/d$b$3;

    invoke-direct {v0, p0}, Lco/uk/getmondo/signup/status/d$b$3;-><init>(Lco/uk/getmondo/signup/status/d$b;)V

    check-cast v0, Lio/reactivex/c/b;

    invoke-virtual {v1, v0}, Lio/reactivex/v;->a(Lio/reactivex/c/b;)Lio/reactivex/v;

    move-result-object v1

    .line 55
    new-instance v0, Lco/uk/getmondo/signup/status/d$b$4;

    invoke-direct {v0, p0}, Lco/uk/getmondo/signup/status/d$b$4;-><init>(Lco/uk/getmondo/signup/status/d$b;)V

    check-cast v0, Lio/reactivex/c/g;

    invoke-virtual {v1, v0}, Lio/reactivex/v;->d(Lio/reactivex/c/g;)Lio/reactivex/v;

    move-result-object v0

    .line 60
    invoke-static {v0}, Lco/uk/getmondo/common/j/f;->a(Lio/reactivex/v;)Lio/reactivex/h;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 21
    check-cast p1, Lkotlin/n;

    invoke-virtual {p0, p1}, Lco/uk/getmondo/signup/status/d$b;->a(Lkotlin/n;)Lio/reactivex/h;

    move-result-object v0

    return-object v0
.end method
