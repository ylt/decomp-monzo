.class final Lco/uk/getmondo/signup/status/b$a;
.super Ljava/lang/Object;
.source "SignupStatusManager.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lco/uk/getmondo/signup/status/b;->d()Lio/reactivex/n;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h",
        "<TT;TR;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000\u0014\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0000\u0010\u0000\u001a\n \u0002*\u0004\u0018\u00010\u00010\u00012 \u0010\u0003\u001a\u001c\u0012\u000c\u0012\n \u0002*\u0004\u0018\u00010\u00010\u0001\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00050\u0004H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "Lco/uk/getmondo/api/model/sign_up/MigrationInfo;",
        "kotlin.jvm.PlatformType",
        "<name for destructuring parameter 0>",
        "Lkotlin/Triple;",
        "",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# static fields
.field public static final a:Lco/uk/getmondo/signup/status/b$a;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lco/uk/getmondo/signup/status/b$a;

    invoke-direct {v0}, Lco/uk/getmondo/signup/status/b$a;-><init>()V

    sput-object v0, Lco/uk/getmondo/signup/status/b$a;->a:Lco/uk/getmondo/signup/status/b$a;

    return-void
.end method

.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lkotlin/k;)Lco/uk/getmondo/api/model/sign_up/MigrationInfo;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/k",
            "<",
            "Lco/uk/getmondo/api/model/sign_up/MigrationInfo;",
            "Ljava/lang/Boolean;",
            "Ljava/lang/Boolean;",
            ">;)",
            "Lco/uk/getmondo/api/model/sign_up/MigrationInfo;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    const/4 v3, 0x0

    const-string v0, "<name for destructuring parameter 0>"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lkotlin/k;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/api/model/sign_up/MigrationInfo;

    invoke-virtual {p1}, Lkotlin/k;->b()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    invoke-virtual {p1}, Lkotlin/k;->c()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    .line 86
    if-nez v2, :cond_0

    if-eqz v4, :cond_1

    invoke-virtual {v0}, Lco/uk/getmondo/api/model/sign_up/MigrationInfo;->b()Z

    move-result v2

    if-nez v2, :cond_1

    .line 87
    :cond_0
    const/16 v7, 0x3e

    move v2, v1

    move-object v4, v3

    move-object v5, v3

    move-object v6, v3

    move-object v8, v3

    invoke-static/range {v0 .. v8}, Lco/uk/getmondo/api/model/sign_up/MigrationInfo;->a(Lco/uk/getmondo/api/model/sign_up/MigrationInfo;ZZLjava/lang/String;Ljava/lang/String;Lco/uk/getmondo/api/model/signup/SignupInfo$Status;Lco/uk/getmondo/api/model/signup/SignupInfo$Stage;ILjava/lang/Object;)Lco/uk/getmondo/api/model/sign_up/MigrationInfo;

    move-result-object v0

    .line 86
    :cond_1
    return-object v0
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 15
    check-cast p1, Lkotlin/k;

    invoke-virtual {p0, p1}, Lco/uk/getmondo/signup/status/b$a;->a(Lkotlin/k;)Lco/uk/getmondo/api/model/sign_up/MigrationInfo;

    move-result-object v0

    return-object v0
.end method
