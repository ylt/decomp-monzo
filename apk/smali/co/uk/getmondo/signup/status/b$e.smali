.class final Lco/uk/getmondo/signup/status/b$e;
.super Ljava/lang/Object;
.source "SignupStatusManager.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lco/uk/getmondo/signup/status/b;->a(Z)Lio/reactivex/v;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h",
        "<TT;",
        "Lio/reactivex/z",
        "<+TR;>;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0010\u0000\u001a\u0016\u0012\u0004\u0012\u00020\u0002 \u0003*\n\u0012\u0004\u0012\u00020\u0002\u0018\u00010\u00010\u00012\u0006\u0010\u0004\u001a\u00020\u0002H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "Lio/reactivex/Single;",
        "Lco/uk/getmondo/api/model/signup/SignupInfo;",
        "kotlin.jvm.PlatformType",
        "it",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field final synthetic a:Lco/uk/getmondo/signup/status/b;

.field final synthetic b:Z


# direct methods
.method constructor <init>(Lco/uk/getmondo/signup/status/b;Z)V
    .locals 0

    iput-object p1, p0, Lco/uk/getmondo/signup/status/b$e;->a:Lco/uk/getmondo/signup/status/b;

    iput-boolean p2, p0, Lco/uk/getmondo/signup/status/b$e;->b:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lco/uk/getmondo/api/model/signup/SignupInfo;)Lio/reactivex/v;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lco/uk/getmondo/api/model/signup/SignupInfo;",
            ")",
            "Lio/reactivex/v",
            "<",
            "Lco/uk/getmondo/api/model/signup/SignupInfo;",
            ">;"
        }
    .end annotation

    .prologue
    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 31
    iget-boolean v0, p0, Lco/uk/getmondo/signup/status/b$e;->b:Z

    if-eqz v0, :cond_0

    .line 32
    iget-object v0, p0, Lco/uk/getmondo/signup/status/b$e;->a:Lco/uk/getmondo/signup/status/b;

    invoke-static {v0, p1}, Lco/uk/getmondo/signup/status/b;->a(Lco/uk/getmondo/signup/status/b;Lco/uk/getmondo/api/model/signup/SignupInfo;)Lio/reactivex/v;

    move-result-object v0

    .line 31
    :goto_0
    return-object v0

    .line 34
    :cond_0
    invoke-static {p1}, Lio/reactivex/v;->a(Ljava/lang/Object;)Lio/reactivex/v;

    move-result-object v0

    goto :goto_0
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 15
    check-cast p1, Lco/uk/getmondo/api/model/signup/SignupInfo;

    invoke-virtual {p0, p1}, Lco/uk/getmondo/signup/status/b$e;->a(Lco/uk/getmondo/api/model/signup/SignupInfo;)Lio/reactivex/v;

    move-result-object v0

    return-object v0
.end method
