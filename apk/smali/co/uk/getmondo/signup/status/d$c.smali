.class final Lco/uk/getmondo/signup/status/d$c;
.super Ljava/lang/Object;
.source "SignupStatusPresenter.kt"

# interfaces
.implements Lio/reactivex/c/g;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lco/uk/getmondo/signup/status/d;->a(Lco/uk/getmondo/signup/status/d$a;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/g",
        "<",
        "Lco/uk/getmondo/api/model/signup/SignupInfo;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "",
        "it",
        "Lco/uk/getmondo/api/model/signup/SignupInfo;",
        "kotlin.jvm.PlatformType",
        "accept"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field final synthetic a:Lco/uk/getmondo/signup/status/d;

.field final synthetic b:Lco/uk/getmondo/signup/status/d$a;


# direct methods
.method constructor <init>(Lco/uk/getmondo/signup/status/d;Lco/uk/getmondo/signup/status/d$a;)V
    .locals 0

    iput-object p1, p0, Lco/uk/getmondo/signup/status/d$c;->a:Lco/uk/getmondo/signup/status/d;

    iput-object p2, p0, Lco/uk/getmondo/signup/status/d$c;->b:Lco/uk/getmondo/signup/status/d$a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lco/uk/getmondo/api/model/signup/SignupInfo;)V
    .locals 2

    .prologue
    .line 63
    invoke-virtual {p1}, Lco/uk/getmondo/api/model/signup/SignupInfo;->a()Lco/uk/getmondo/api/model/signup/SignupInfo$Status;

    move-result-object v0

    sget-object v1, Lco/uk/getmondo/signup/status/e;->a:[I

    invoke-virtual {v0}, Lco/uk/getmondo/api/model/signup/SignupInfo$Status;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 68
    :goto_0
    return-void

    .line 64
    :pswitch_0
    iget-object v0, p0, Lco/uk/getmondo/signup/status/d$c;->b:Lco/uk/getmondo/signup/status/d$a;

    invoke-interface {v0}, Lco/uk/getmondo/signup/status/d$a;->f()V

    goto :goto_0

    .line 65
    :pswitch_1
    iget-object v0, p0, Lco/uk/getmondo/signup/status/d$c;->a:Lco/uk/getmondo/signup/status/d;

    invoke-virtual {p1}, Lco/uk/getmondo/api/model/signup/SignupInfo;->b()Lco/uk/getmondo/api/model/signup/SignupInfo$Stage;

    move-result-object v1

    invoke-static {v0, v1}, Lco/uk/getmondo/signup/status/d;->a(Lco/uk/getmondo/signup/status/d;Lco/uk/getmondo/api/model/signup/SignupInfo$Stage;)V

    goto :goto_0

    .line 66
    :pswitch_2
    iget-object v0, p0, Lco/uk/getmondo/signup/status/d$c;->b:Lco/uk/getmondo/signup/status/d$a;

    invoke-interface {v0}, Lco/uk/getmondo/signup/status/d$a;->g()V

    goto :goto_0

    .line 67
    :pswitch_3
    iget-object v0, p0, Lco/uk/getmondo/signup/status/d$c;->a:Lco/uk/getmondo/signup/status/d;

    const-string v1, "it"

    invoke-static {p1, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v0, p1}, Lco/uk/getmondo/signup/status/d;->b(Lco/uk/getmondo/signup/status/d;Lco/uk/getmondo/api/model/signup/SignupInfo;)V

    goto :goto_0

    .line 63
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 21
    check-cast p1, Lco/uk/getmondo/api/model/signup/SignupInfo;

    invoke-virtual {p0, p1}, Lco/uk/getmondo/signup/status/d$c;->a(Lco/uk/getmondo/api/model/signup/SignupInfo;)V

    return-void
.end method
