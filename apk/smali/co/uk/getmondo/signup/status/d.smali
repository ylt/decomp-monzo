.class public final Lco/uk/getmondo/signup/status/d;
.super Lco/uk/getmondo/common/ui/b;
.source "SignupStatusPresenter.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lco/uk/getmondo/signup/status/d$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lco/uk/getmondo/common/ui/b",
        "<",
        "Lco/uk/getmondo/signup/status/d$a;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000D\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0006\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00020\u0001:\u0001\u0019B3\u0008\u0007\u0012\u0008\u0008\u0001\u0010\u0003\u001a\u00020\u0004\u0012\u0008\u0008\u0001\u0010\u0005\u001a\u00020\u0004\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b\u00a2\u0006\u0002\u0010\u000cJ\u0010\u0010\u0011\u001a\u00020\u00122\u0006\u0010\u0013\u001a\u00020\u0014H\u0002J\u0010\u0010\u0015\u001a\u00020\u00122\u0006\u0010\u0016\u001a\u00020\u000fH\u0002J\u0010\u0010\u0017\u001a\u00020\u00122\u0006\u0010\u0018\u001a\u00020\u0002H\u0016R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0018\u0010\r\u001a\u00020\u000e*\u00020\u000f8BX\u0082\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\r\u0010\u0010\u00a8\u0006\u001a"
    }
    d2 = {
        "Lco/uk/getmondo/signup/status/SignupStatusPresenter;",
        "Lco/uk/getmondo/common/ui/BasePresenter;",
        "Lco/uk/getmondo/signup/status/SignupStatusPresenter$View;",
        "uiScheduler",
        "Lio/reactivex/Scheduler;",
        "ioScheduler",
        "signupStatusManager",
        "Lco/uk/getmondo/signup/status/SignupStatusManager;",
        "apiErrorHandler",
        "Lco/uk/getmondo/common/errors/ApiErrorHandler;",
        "userInteractor",
        "Lco/uk/getmondo/api/interactors/UserInteractor;",
        "(Lio/reactivex/Scheduler;Lio/reactivex/Scheduler;Lco/uk/getmondo/signup/status/SignupStatusManager;Lco/uk/getmondo/common/errors/ApiErrorHandler;Lco/uk/getmondo/api/interactors/UserInteractor;)V",
        "isReadyToStart",
        "",
        "Lco/uk/getmondo/api/model/signup/SignupInfo;",
        "(Lco/uk/getmondo/api/model/signup/SignupInfo;)Z",
        "processStage",
        "",
        "stage",
        "Lco/uk/getmondo/api/model/signup/SignupInfo$Stage;",
        "processStageWhenStatusNotStarted",
        "signupInfo",
        "register",
        "view",
        "View",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field private final c:Lio/reactivex/u;

.field private final d:Lio/reactivex/u;

.field private final e:Lco/uk/getmondo/signup/status/b;

.field private final f:Lco/uk/getmondo/common/e/a;

.field private final g:Lco/uk/getmondo/api/b/a;


# direct methods
.method public constructor <init>(Lio/reactivex/u;Lio/reactivex/u;Lco/uk/getmondo/signup/status/b;Lco/uk/getmondo/common/e/a;Lco/uk/getmondo/api/b/a;)V
    .locals 1

    .prologue
    const-string v0, "uiScheduler"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "ioScheduler"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "signupStatusManager"

    invoke-static {p3, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "apiErrorHandler"

    invoke-static {p4, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "userInteractor"

    invoke-static {p5, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 28
    invoke-direct {p0}, Lco/uk/getmondo/common/ui/b;-><init>()V

    iput-object p1, p0, Lco/uk/getmondo/signup/status/d;->c:Lio/reactivex/u;

    iput-object p2, p0, Lco/uk/getmondo/signup/status/d;->d:Lio/reactivex/u;

    iput-object p3, p0, Lco/uk/getmondo/signup/status/d;->e:Lco/uk/getmondo/signup/status/b;

    iput-object p4, p0, Lco/uk/getmondo/signup/status/d;->f:Lco/uk/getmondo/common/e/a;

    iput-object p5, p0, Lco/uk/getmondo/signup/status/d;->g:Lco/uk/getmondo/api/b/a;

    return-void
.end method

.method public static final synthetic a(Lco/uk/getmondo/signup/status/d;)Lco/uk/getmondo/signup/status/b;
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lco/uk/getmondo/signup/status/d;->e:Lco/uk/getmondo/signup/status/b;

    return-object v0
.end method

.method private final a(Lco/uk/getmondo/api/model/signup/SignupInfo$Stage;)V
    .locals 3

    .prologue
    .line 81
    sget-object v0, Lco/uk/getmondo/signup/status/e;->c:[I

    invoke-virtual {p1}, Lco/uk/getmondo/api/model/signup/SignupInfo$Stage;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 95
    :goto_0
    return-void

    .line 82
    :pswitch_0
    iget-object v0, p0, Lco/uk/getmondo/signup/status/d;->a:Lco/uk/getmondo/common/ui/f;

    check-cast v0, Lco/uk/getmondo/signup/status/d$a;

    invoke-interface {v0}, Lco/uk/getmondo/signup/status/d$a;->h()V

    goto :goto_0

    .line 83
    :pswitch_1
    iget-object v0, p0, Lco/uk/getmondo/signup/status/d;->a:Lco/uk/getmondo/common/ui/f;

    check-cast v0, Lco/uk/getmondo/signup/status/d$a;

    invoke-interface {v0}, Lco/uk/getmondo/signup/status/d$a;->i()V

    goto :goto_0

    .line 84
    :pswitch_2
    iget-object v0, p0, Lco/uk/getmondo/signup/status/d;->a:Lco/uk/getmondo/common/ui/f;

    check-cast v0, Lco/uk/getmondo/signup/status/d$a;

    invoke-interface {v0}, Lco/uk/getmondo/signup/status/d$a;->j()V

    goto :goto_0

    .line 85
    :pswitch_3
    iget-object v0, p0, Lco/uk/getmondo/signup/status/d;->a:Lco/uk/getmondo/common/ui/f;

    check-cast v0, Lco/uk/getmondo/signup/status/d$a;

    invoke-interface {v0}, Lco/uk/getmondo/signup/status/d$a;->k()V

    goto :goto_0

    .line 86
    :pswitch_4
    iget-object v0, p0, Lco/uk/getmondo/signup/status/d;->a:Lco/uk/getmondo/common/ui/f;

    check-cast v0, Lco/uk/getmondo/signup/status/d$a;

    invoke-interface {v0}, Lco/uk/getmondo/signup/status/d$a;->v()V

    goto :goto_0

    .line 87
    :pswitch_5
    iget-object v0, p0, Lco/uk/getmondo/signup/status/d;->a:Lco/uk/getmondo/common/ui/f;

    check-cast v0, Lco/uk/getmondo/signup/status/d$a;

    invoke-interface {v0}, Lco/uk/getmondo/signup/status/d$a;->w()V

    goto :goto_0

    .line 88
    :pswitch_6
    iget-object v0, p0, Lco/uk/getmondo/signup/status/d;->a:Lco/uk/getmondo/common/ui/f;

    check-cast v0, Lco/uk/getmondo/signup/status/d$a;

    invoke-interface {v0}, Lco/uk/getmondo/signup/status/d$a;->x()V

    goto :goto_0

    .line 89
    :pswitch_7
    iget-object v0, p0, Lco/uk/getmondo/signup/status/d;->a:Lco/uk/getmondo/common/ui/f;

    check-cast v0, Lco/uk/getmondo/signup/status/d$a;

    invoke-interface {v0}, Lco/uk/getmondo/signup/status/d$a;->y()V

    goto :goto_0

    .line 90
    :pswitch_8
    iget-object v0, p0, Lco/uk/getmondo/signup/status/d;->a:Lco/uk/getmondo/common/ui/f;

    check-cast v0, Lco/uk/getmondo/signup/status/d$a;

    invoke-interface {v0}, Lco/uk/getmondo/signup/status/d$a;->z()V

    goto :goto_0

    .line 91
    :pswitch_9
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Unexpected stage! Should have been skipped"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    .line 92
    :pswitch_a
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Stage is missing when status is in-progress"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    .line 93
    :pswitch_b
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Unexpected stage! Should be Status.COMPLETED"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    .line 94
    :pswitch_c
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " stage is only allowed if status is NOT_STARTED"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    .line 81
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_c
    .end packed-switch
.end method

.method private final a(Lco/uk/getmondo/api/model/signup/SignupInfo;)V
    .locals 3

    .prologue
    .line 73
    invoke-virtual {p1}, Lco/uk/getmondo/api/model/signup/SignupInfo;->b()Lco/uk/getmondo/api/model/signup/SignupInfo$Stage;

    move-result-object v0

    sget-object v1, Lco/uk/getmondo/signup/status/e;->b:[I

    invoke-virtual {v0}, Lco/uk/getmondo/api/model/signup/SignupInfo$Stage;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 76
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unexpected stage "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lco/uk/getmondo/api/model/signup/SignupInfo;->b()Lco/uk/getmondo/api/model/signup/SignupInfo$Stage;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " when status is NOT_STARTED"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    .line 74
    :pswitch_0
    iget-object v0, p0, Lco/uk/getmondo/signup/status/d;->a:Lco/uk/getmondo/common/ui/f;

    check-cast v0, Lco/uk/getmondo/signup/status/d$a;

    invoke-interface {v0}, Lco/uk/getmondo/signup/status/d$a;->B()V

    .line 77
    :goto_0
    return-void

    .line 75
    :pswitch_1
    iget-object v0, p0, Lco/uk/getmondo/signup/status/d;->a:Lco/uk/getmondo/common/ui/f;

    check-cast v0, Lco/uk/getmondo/signup/status/d$a;

    invoke-interface {v0}, Lco/uk/getmondo/signup/status/d$a;->A()V

    goto :goto_0

    .line 73
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static final synthetic a(Lco/uk/getmondo/signup/status/d;Lco/uk/getmondo/api/model/signup/SignupInfo$Stage;)V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0, p1}, Lco/uk/getmondo/signup/status/d;->a(Lco/uk/getmondo/api/model/signup/SignupInfo$Stage;)V

    return-void
.end method

.method public static final synthetic a(Lco/uk/getmondo/signup/status/d;Lco/uk/getmondo/api/model/signup/SignupInfo;)Z
    .locals 1

    .prologue
    .line 21
    invoke-direct {p0, p1}, Lco/uk/getmondo/signup/status/d;->b(Lco/uk/getmondo/api/model/signup/SignupInfo;)Z

    move-result v0

    return v0
.end method

.method public static final synthetic b(Lco/uk/getmondo/signup/status/d;)Lco/uk/getmondo/api/b/a;
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lco/uk/getmondo/signup/status/d;->g:Lco/uk/getmondo/api/b/a;

    return-object v0
.end method

.method public static final synthetic b(Lco/uk/getmondo/signup/status/d;Lco/uk/getmondo/api/model/signup/SignupInfo;)V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0, p1}, Lco/uk/getmondo/signup/status/d;->a(Lco/uk/getmondo/api/model/signup/SignupInfo;)V

    return-void
.end method

.method private final b(Lco/uk/getmondo/api/model/signup/SignupInfo;)Z
    .locals 2

    .prologue
    .line 99
    invoke-virtual {p1}, Lco/uk/getmondo/api/model/signup/SignupInfo;->a()Lco/uk/getmondo/api/model/signup/SignupInfo$Status;

    move-result-object v0

    sget-object v1, Lco/uk/getmondo/api/model/signup/SignupInfo$Status;->NOT_STARTED:Lco/uk/getmondo/api/model/signup/SignupInfo$Status;

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lco/uk/getmondo/api/model/signup/SignupInfo;->b()Lco/uk/getmondo/api/model/signup/SignupInfo$Stage;

    move-result-object v0

    sget-object v1, Lco/uk/getmondo/api/model/signup/SignupInfo$Stage;->WAIT_LIST:Lco/uk/getmondo/api/model/signup/SignupInfo$Stage;

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lco/uk/getmondo/api/model/signup/SignupInfo;->b()Lco/uk/getmondo/api/model/signup/SignupInfo$Stage;

    move-result-object v0

    sget-object v1, Lco/uk/getmondo/api/model/signup/SignupInfo$Stage;->WAIT_LIST_SIGNUP:Lco/uk/getmondo/api/model/signup/SignupInfo$Stage;

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static final synthetic c(Lco/uk/getmondo/signup/status/d;)Lio/reactivex/u;
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lco/uk/getmondo/signup/status/d;->d:Lio/reactivex/u;

    return-object v0
.end method

.method public static final synthetic d(Lco/uk/getmondo/signup/status/d;)Lio/reactivex/u;
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lco/uk/getmondo/signup/status/d;->c:Lio/reactivex/u;

    return-object v0
.end method

.method public static final synthetic e(Lco/uk/getmondo/signup/status/d;)Lco/uk/getmondo/common/e/a;
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lco/uk/getmondo/signup/status/d;->f:Lco/uk/getmondo/common/e/a;

    return-object v0
.end method


# virtual methods
.method public bridge synthetic a(Lco/uk/getmondo/common/ui/f;)V
    .locals 0

    .prologue
    .line 21
    check-cast p1, Lco/uk/getmondo/signup/status/d$a;

    invoke-virtual {p0, p1}, Lco/uk/getmondo/signup/status/d;->a(Lco/uk/getmondo/signup/status/d$a;)V

    return-void
.end method

.method public a(Lco/uk/getmondo/signup/status/d$a;)V
    .locals 3

    .prologue
    const-string v0, "view"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v0, p1

    .line 31
    check-cast v0, Lco/uk/getmondo/common/ui/f;

    invoke-super {p0, v0}, Lco/uk/getmondo/common/ui/b;->a(Lco/uk/getmondo/common/ui/f;)V

    .line 32
    iget-object v2, p0, Lco/uk/getmondo/signup/status/d;->b:Lio/reactivex/b/a;

    .line 62
    invoke-interface {p1}, Lco/uk/getmondo/signup/status/d$a;->b()Lio/reactivex/n;

    move-result-object v0

    check-cast v0, Lio/reactivex/r;

    invoke-interface {p1}, Lco/uk/getmondo/signup/status/d$a;->c()Lio/reactivex/n;

    move-result-object v1

    check-cast v1, Lio/reactivex/r;

    invoke-static {v0, v1}, Lio/reactivex/n;->merge(Lio/reactivex/r;Lio/reactivex/r;)Lio/reactivex/n;

    move-result-object v0

    .line 33
    sget-object v1, Lkotlin/n;->a:Lkotlin/n;

    invoke-virtual {v0, v1}, Lio/reactivex/n;->startWith(Ljava/lang/Object;)Lio/reactivex/n;

    move-result-object v1

    .line 34
    new-instance v0, Lco/uk/getmondo/signup/status/d$b;

    invoke-direct {v0, p0, p1}, Lco/uk/getmondo/signup/status/d$b;-><init>(Lco/uk/getmondo/signup/status/d;Lco/uk/getmondo/signup/status/d$a;)V

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {v1, v0}, Lio/reactivex/n;->flatMapMaybe(Lio/reactivex/c/h;)Lio/reactivex/n;

    move-result-object v1

    .line 62
    new-instance v0, Lco/uk/getmondo/signup/status/d$c;

    invoke-direct {v0, p0, p1}, Lco/uk/getmondo/signup/status/d$c;-><init>(Lco/uk/getmondo/signup/status/d;Lco/uk/getmondo/signup/status/d$a;)V

    check-cast v0, Lio/reactivex/c/g;

    invoke-virtual {v1, v0}, Lio/reactivex/n;->subscribe(Lio/reactivex/c/g;)Lio/reactivex/b/b;

    move-result-object v0

    const-string v1, "Observable.merge(view.on\u2026      }\n                }"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 32
    invoke-static {v2, v0}, Lco/uk/getmondo/common/j/f;->a(Lio/reactivex/b/a;Lio/reactivex/b/b;)Lio/reactivex/b/a;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/signup/status/d;->b:Lio/reactivex/b/a;

    .line 70
    return-void
.end method
