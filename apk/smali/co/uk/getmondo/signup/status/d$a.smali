.class public interface abstract Lco/uk/getmondo/signup/status/d$a;
.super Ljava/lang/Object;
.source "SignupStatusPresenter.kt"

# interfaces
.implements Lco/uk/getmondo/common/e/a$a;
.implements Lco/uk/getmondo/common/ui/f;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lco/uk/getmondo/signup/status/d;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "a"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0010\u0008f\u0018\u00002\u00020\u00012\u00020\u0002J\u0008\u0010\u0007\u001a\u00020\u0008H&J\u000e\u0010\t\u001a\u0008\u0012\u0004\u0012\u00020\u00080\nH&J\u000e\u0010\u000b\u001a\u0008\u0012\u0004\u0012\u00020\u00080\nH&J\u0008\u0010\u000c\u001a\u00020\u0008H&J\u0008\u0010\r\u001a\u00020\u0008H&J\u0008\u0010\u000e\u001a\u00020\u0008H&J\u0008\u0010\u000f\u001a\u00020\u0008H&J\u0008\u0010\u0010\u001a\u00020\u0008H&J\u0008\u0010\u0011\u001a\u00020\u0008H&J\u0008\u0010\u0012\u001a\u00020\u0008H&J\u0008\u0010\u0013\u001a\u00020\u0008H&J\u0008\u0010\u0014\u001a\u00020\u0008H&J\u0008\u0010\u0015\u001a\u00020\u0008H&J\u0008\u0010\u0016\u001a\u00020\u0008H&J\u0008\u0010\u0017\u001a\u00020\u0008H&J\u0008\u0010\u0018\u001a\u00020\u0008H&J\u0008\u0010\u0019\u001a\u00020\u0008H&R\u0012\u0010\u0003\u001a\u00020\u0004X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0005\u0010\u0006\u00a8\u0006\u001a"
    }
    d2 = {
        "Lco/uk/getmondo/signup/status/SignupStatusPresenter$View;",
        "Lco/uk/getmondo/common/ui/MvpView;",
        "Lco/uk/getmondo/common/errors/ApiErrorHandler$ApiErrorView;",
        "entryPoint",
        "Lco/uk/getmondo/signup/SignupEntryPoint;",
        "getEntryPoint",
        "()Lco/uk/getmondo/signup/SignupEntryPoint;",
        "hideLoading",
        "",
        "onRetryClicked",
        "Lio/reactivex/Observable;",
        "onStageCompleted",
        "openCardActivation",
        "openCardOrdering",
        "openHome",
        "openIdentityVerification",
        "openLegalDocuments",
        "openMarketingOptIn",
        "openPhoneVerification",
        "openProfileCreation",
        "openSignUpPending",
        "openSignUpRejected",
        "openTaxResidency",
        "openWaitingList",
        "openWaitingListSignup",
        "showLoading",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# virtual methods
.method public abstract A()V
.end method

.method public abstract B()V
.end method

.method public abstract a()Lco/uk/getmondo/signup/j;
.end method

.method public abstract b()Lio/reactivex/n;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Lkotlin/n;",
            ">;"
        }
    .end annotation
.end method

.method public abstract c()Lio/reactivex/n;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Lkotlin/n;",
            ">;"
        }
    .end annotation
.end method

.method public abstract d()V
.end method

.method public abstract e()V
.end method

.method public abstract f()V
.end method

.method public abstract g()V
.end method

.method public abstract h()V
.end method

.method public abstract i()V
.end method

.method public abstract j()V
.end method

.method public abstract k()V
.end method

.method public abstract v()V
.end method

.method public abstract w()V
.end method

.method public abstract x()V
.end method

.method public abstract y()V
.end method

.method public abstract z()V
.end method
