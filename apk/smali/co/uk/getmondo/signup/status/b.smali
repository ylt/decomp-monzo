.class public final Lco/uk/getmondo/signup/status/b;
.super Ljava/lang/Object;
.source "SignupStatusManager.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000H\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0006\u0018\u00002\u00020\u0001B/\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b\u00a2\u0006\u0002\u0010\u000cJ\u000c\u0010\u0010\u001a\u0008\u0012\u0004\u0012\u00020\u00120\u0011J\u0016\u0010\u0013\u001a\u0008\u0012\u0004\u0012\u00020\u00150\u00142\u0006\u0010\u0016\u001a\u00020\u0015H\u0002J\u000c\u0010\u0017\u001a\u0008\u0012\u0004\u0012\u00020\u00150\u0014J\u000c\u0010\u0018\u001a\u0008\u0012\u0004\u0012\u00020\u00150\u0014J\u0016\u0010\u0019\u001a\u0008\u0012\u0004\u0012\u00020\u00150\u00142\u0008\u0008\u0002\u0010\u001a\u001a\u00020\u000eR\u0011\u0010\r\u001a\u00020\u000e8F\u00a2\u0006\u0006\u001a\u0004\u0008\r\u0010\u000fR\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001b"
    }
    d2 = {
        "Lco/uk/getmondo/signup/status/SignupStatusManager;",
        "",
        "signupApi",
        "Lco/uk/getmondo/api/SignupApi;",
        "migrationApi",
        "Lco/uk/getmondo/api/MigrationApi;",
        "userInteractor",
        "Lco/uk/getmondo/api/interactors/UserInteractor;",
        "statusStorage",
        "Lco/uk/getmondo/signup/status/SignupStatusStorage;",
        "migrationStorage",
        "Lco/uk/getmondo/migration/MigrationStorage;",
        "(Lco/uk/getmondo/api/SignupApi;Lco/uk/getmondo/api/MigrationApi;Lco/uk/getmondo/api/interactors/UserInteractor;Lco/uk/getmondo/signup/status/SignupStatusStorage;Lco/uk/getmondo/migration/MigrationStorage;)V",
        "isSignupCompleted",
        "",
        "()Z",
        "migrationInfo",
        "Lio/reactivex/Observable;",
        "Lco/uk/getmondo/api/model/sign_up/MigrationInfo;",
        "overrideStageIfInWaitingList",
        "Lio/reactivex/Single;",
        "Lco/uk/getmondo/api/model/signup/SignupInfo;",
        "signupInfo",
        "skipFingerprintEnrolment",
        "startSignUp",
        "status",
        "waitingListEnabled",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field private final a:Lco/uk/getmondo/api/SignupApi;

.field private final b:Lco/uk/getmondo/api/MigrationApi;

.field private final c:Lco/uk/getmondo/api/b/a;

.field private final d:Lco/uk/getmondo/signup/status/g;

.field private final e:Lco/uk/getmondo/migration/d;


# direct methods
.method public constructor <init>(Lco/uk/getmondo/api/SignupApi;Lco/uk/getmondo/api/MigrationApi;Lco/uk/getmondo/api/b/a;Lco/uk/getmondo/signup/status/g;Lco/uk/getmondo/migration/d;)V
    .locals 1

    .prologue
    const-string v0, "signupApi"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "migrationApi"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "userInteractor"

    invoke-static {p3, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "statusStorage"

    invoke-static {p4, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "migrationStorage"

    invoke-static {p5, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lco/uk/getmondo/signup/status/b;->a:Lco/uk/getmondo/api/SignupApi;

    iput-object p2, p0, Lco/uk/getmondo/signup/status/b;->b:Lco/uk/getmondo/api/MigrationApi;

    iput-object p3, p0, Lco/uk/getmondo/signup/status/b;->c:Lco/uk/getmondo/api/b/a;

    iput-object p4, p0, Lco/uk/getmondo/signup/status/b;->d:Lco/uk/getmondo/signup/status/g;

    iput-object p5, p0, Lco/uk/getmondo/signup/status/b;->e:Lco/uk/getmondo/migration/d;

    return-void
.end method

.method public static final synthetic a(Lco/uk/getmondo/signup/status/b;)Lco/uk/getmondo/signup/status/g;
    .locals 1

    .prologue
    .line 15
    iget-object v0, p0, Lco/uk/getmondo/signup/status/b;->d:Lco/uk/getmondo/signup/status/g;

    return-object v0
.end method

.method private final a(Lco/uk/getmondo/api/model/signup/SignupInfo;)Lio/reactivex/v;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lco/uk/getmondo/api/model/signup/SignupInfo;",
            ")",
            "Lio/reactivex/v",
            "<",
            "Lco/uk/getmondo/api/model/signup/SignupInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 48
    invoke-virtual {p1}, Lco/uk/getmondo/api/model/signup/SignupInfo;->a()Lco/uk/getmondo/api/model/signup/SignupInfo$Status;

    move-result-object v0

    sget-object v1, Lco/uk/getmondo/api/model/signup/SignupInfo$Status;->NOT_STARTED:Lco/uk/getmondo/api/model/signup/SignupInfo$Status;

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 49
    iget-object v0, p0, Lco/uk/getmondo/signup/status/b;->c:Lco/uk/getmondo/api/b/a;

    invoke-virtual {v0}, Lco/uk/getmondo/api/b/a;->b()Lio/reactivex/v;

    move-result-object v1

    .line 50
    sget-object v0, Lco/uk/getmondo/signup/status/b$b;->a:Lco/uk/getmondo/signup/status/b$b;

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {v1, v0}, Lio/reactivex/v;->d(Lio/reactivex/c/h;)Lio/reactivex/v;

    move-result-object v1

    .line 51
    new-instance v0, Lco/uk/getmondo/signup/status/b$c;

    invoke-direct {v0, p1}, Lco/uk/getmondo/signup/status/b$c;-><init>(Lco/uk/getmondo/api/model/signup/SignupInfo;)V

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {v1, v0}, Lio/reactivex/v;->d(Lio/reactivex/c/h;)Lio/reactivex/v;

    move-result-object v1

    .line 58
    new-instance v0, Lco/uk/getmondo/signup/status/b$d;

    invoke-direct {v0, p1}, Lco/uk/getmondo/signup/status/b$d;-><init>(Lco/uk/getmondo/api/model/signup/SignupInfo;)V

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {v1, v0}, Lio/reactivex/v;->f(Lio/reactivex/c/h;)Lio/reactivex/v;

    move-result-object v0

    const-string v1, "userInteractor.fetchWait\u2026  }\n                    }"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    :goto_0
    return-object v0

    .line 68
    :cond_0
    invoke-static {p1}, Lio/reactivex/v;->a(Ljava/lang/Object;)Lio/reactivex/v;

    move-result-object v0

    const-string v1, "Single.just(signupInfo)"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static final synthetic a(Lco/uk/getmondo/signup/status/b;Lco/uk/getmondo/api/model/signup/SignupInfo;)Lio/reactivex/v;
    .locals 1

    .prologue
    .line 15
    invoke-direct {p0, p1}, Lco/uk/getmondo/signup/status/b;->a(Lco/uk/getmondo/api/model/signup/SignupInfo;)Lio/reactivex/v;

    move-result-object v0

    return-object v0
.end method

.method public static bridge synthetic a(Lco/uk/getmondo/signup/status/b;ZILjava/lang/Object;)Lio/reactivex/v;
    .locals 1

    .prologue
    and-int/lit8 v0, p2, 0x1

    if-eqz v0, :cond_0

    .line 28
    const/4 p1, 0x1

    :cond_0
    invoke-virtual {p0, p1}, Lco/uk/getmondo/signup/status/b;->a(Z)Lio/reactivex/v;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(Z)Lio/reactivex/v;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)",
            "Lio/reactivex/v",
            "<",
            "Lco/uk/getmondo/api/model/signup/SignupInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 29
    iget-object v0, p0, Lco/uk/getmondo/signup/status/b;->a:Lco/uk/getmondo/api/SignupApi;

    invoke-interface {v0}, Lco/uk/getmondo/api/SignupApi;->signupStatus()Lio/reactivex/v;

    move-result-object v1

    .line 30
    new-instance v0, Lco/uk/getmondo/signup/status/b$e;

    invoke-direct {v0, p0, p1}, Lco/uk/getmondo/signup/status/b$e;-><init>(Lco/uk/getmondo/signup/status/b;Z)V

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {v1, v0}, Lio/reactivex/v;->a(Lio/reactivex/c/h;)Lio/reactivex/v;

    move-result-object v1

    .line 37
    new-instance v0, Lco/uk/getmondo/signup/status/b$f;

    invoke-direct {v0, p0}, Lco/uk/getmondo/signup/status/b$f;-><init>(Lco/uk/getmondo/signup/status/b;)V

    check-cast v0, Lio/reactivex/c/g;

    invoke-virtual {v1, v0}, Lio/reactivex/v;->a(Lio/reactivex/c/g;)Lio/reactivex/v;

    move-result-object v0

    const-string v1, "signupApi.signupStatus()\u2026upInfo.Status.COMPLETED }"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, Lco/uk/getmondo/signup/status/b;->d:Lco/uk/getmondo/signup/status/g;

    invoke-virtual {v0}, Lco/uk/getmondo/signup/status/g;->a()Z

    move-result v0

    return v0
.end method

.method public final b()Lio/reactivex/v;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/v",
            "<",
            "Lco/uk/getmondo/api/model/signup/SignupInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 72
    iget-object v0, p0, Lco/uk/getmondo/signup/status/b;->a:Lco/uk/getmondo/api/SignupApi;

    invoke-interface {v0}, Lco/uk/getmondo/api/SignupApi;->startSignUp()Lio/reactivex/b;

    move-result-object v1

    const/4 v0, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-static {p0, v0, v2, v3}, Lco/uk/getmondo/signup/status/b;->a(Lco/uk/getmondo/signup/status/b;ZILjava/lang/Object;)Lio/reactivex/v;

    move-result-object v0

    check-cast v0, Lio/reactivex/z;

    invoke-virtual {v1, v0}, Lio/reactivex/b;->a(Lio/reactivex/z;)Lio/reactivex/v;

    move-result-object v0

    const-string v1, "signupApi.startSignUp().andThen(status())"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public final c()Lio/reactivex/v;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/v",
            "<",
            "Lco/uk/getmondo/api/model/signup/SignupInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 74
    iget-object v0, p0, Lco/uk/getmondo/signup/status/b;->a:Lco/uk/getmondo/api/SignupApi;

    invoke-interface {v0}, Lco/uk/getmondo/api/SignupApi;->skipFingerprintEnrolment()Lio/reactivex/b;

    move-result-object v1

    const/4 v0, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-static {p0, v0, v2, v3}, Lco/uk/getmondo/signup/status/b;->a(Lco/uk/getmondo/signup/status/b;ZILjava/lang/Object;)Lio/reactivex/v;

    move-result-object v0

    check-cast v0, Lio/reactivex/z;

    invoke-virtual {v1, v0}, Lio/reactivex/b;->a(Lio/reactivex/z;)Lio/reactivex/v;

    move-result-object v0

    const-string v1, "signupApi.skipFingerprin\u2026lment().andThen(status())"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public final d()Lio/reactivex/n;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Lco/uk/getmondo/api/model/sign_up/MigrationInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 77
    sget-object v0, Lio/reactivex/rxkotlin/b;->a:Lio/reactivex/rxkotlin/b;

    .line 78
    iget-object v1, p0, Lco/uk/getmondo/signup/status/b;->b:Lco/uk/getmondo/api/MigrationApi;

    invoke-interface {v1}, Lco/uk/getmondo/api/MigrationApi;->migrationInfo()Lio/reactivex/v;

    move-result-object v1

    invoke-virtual {v1}, Lio/reactivex/v;->f()Lio/reactivex/n;

    move-result-object v1

    const-string v2, "migrationApi.migrationInfo().toObservable()"

    invoke-static {v1, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 79
    iget-object v2, p0, Lco/uk/getmondo/signup/status/b;->e:Lco/uk/getmondo/migration/d;

    invoke-virtual {v2}, Lco/uk/getmondo/migration/d;->b()Lio/reactivex/n;

    move-result-object v2

    .line 80
    iget-object v3, p0, Lco/uk/getmondo/signup/status/b;->d:Lco/uk/getmondo/signup/status/g;

    invoke-virtual {v3}, Lco/uk/getmondo/signup/status/g;->b()Lio/reactivex/n;

    move-result-object v3

    .line 77
    invoke-virtual {v0, v1, v2, v3}, Lio/reactivex/rxkotlin/b;->a(Lio/reactivex/n;Lio/reactivex/n;Lio/reactivex/n;)Lio/reactivex/n;

    move-result-object v1

    .line 81
    sget-object v0, Lco/uk/getmondo/signup/status/b$a;->a:Lco/uk/getmondo/signup/status/b$a;

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {v1, v0}, Lio/reactivex/n;->map(Lio/reactivex/c/h;)Lio/reactivex/n;

    move-result-object v0

    const-string v1, "Observables.combineLates\u2026o\n            }\n        }"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method
