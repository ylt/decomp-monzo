.class final Lco/uk/getmondo/signup/status/b$c;
.super Ljava/lang/Object;
.source "SignupStatusManager.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lco/uk/getmondo/signup/status/b;->a(Lco/uk/getmondo/api/model/signup/SignupInfo;)Lio/reactivex/v;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h",
        "<TT;TR;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n\u00a2\u0006\u0004\u0008\u0004\u0010\u0005"
    }
    d2 = {
        "<anonymous>",
        "Lco/uk/getmondo/api/model/signup/SignupInfo;",
        "finishedWaitingList",
        "",
        "apply",
        "(Ljava/lang/Boolean;)Lco/uk/getmondo/api/model/signup/SignupInfo;"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field final synthetic a:Lco/uk/getmondo/api/model/signup/SignupInfo;


# direct methods
.method constructor <init>(Lco/uk/getmondo/api/model/signup/SignupInfo;)V
    .locals 0

    iput-object p1, p0, Lco/uk/getmondo/signup/status/b$c;->a:Lco/uk/getmondo/api/model/signup/SignupInfo;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Boolean;)Lco/uk/getmondo/api/model/signup/SignupInfo;
    .locals 4

    .prologue
    const/4 v3, 0x0

    const-string v0, "finishedWaitingList"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 52
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 53
    iget-object v0, p0, Lco/uk/getmondo/signup/status/b$c;->a:Lco/uk/getmondo/api/model/signup/SignupInfo;

    .line 52
    :goto_0
    return-object v0

    .line 55
    :cond_0
    iget-object v0, p0, Lco/uk/getmondo/signup/status/b$c;->a:Lco/uk/getmondo/api/model/signup/SignupInfo;

    sget-object v1, Lco/uk/getmondo/api/model/signup/SignupInfo$Stage;->WAIT_LIST:Lco/uk/getmondo/api/model/signup/SignupInfo$Stage;

    const/4 v2, 0x1

    invoke-static {v0, v3, v1, v2, v3}, Lco/uk/getmondo/api/model/signup/SignupInfo;->a(Lco/uk/getmondo/api/model/signup/SignupInfo;Lco/uk/getmondo/api/model/signup/SignupInfo$Status;Lco/uk/getmondo/api/model/signup/SignupInfo$Stage;ILjava/lang/Object;)Lco/uk/getmondo/api/model/signup/SignupInfo;

    move-result-object v0

    goto :goto_0
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 15
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Lco/uk/getmondo/signup/status/b$c;->a(Ljava/lang/Boolean;)Lco/uk/getmondo/api/model/signup/SignupInfo;

    move-result-object v0

    return-object v0
.end method
