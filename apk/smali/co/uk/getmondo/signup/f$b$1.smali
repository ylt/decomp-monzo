.class final Lco/uk/getmondo/signup/f$b$1;
.super Ljava/lang/Object;
.source "EmailPresenter.kt"

# interfaces
.implements Lio/reactivex/c/g;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lco/uk/getmondo/signup/f$b;->a(Landroid/support/v4/g/j;)Lio/reactivex/h;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/g",
        "<",
        "Ljava/lang/Throwable;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u0003\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "",
        "error",
        "",
        "kotlin.jvm.PlatformType",
        "accept"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field final synthetic a:Lco/uk/getmondo/signup/f$b;


# direct methods
.method constructor <init>(Lco/uk/getmondo/signup/f$b;)V
    .locals 0

    iput-object p1, p0, Lco/uk/getmondo/signup/f$b$1;->a:Lco/uk/getmondo/signup/f$b;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 30
    check-cast p1, Ljava/lang/Throwable;

    invoke-virtual {p0, p1}, Lco/uk/getmondo/signup/f$b$1;->a(Ljava/lang/Throwable;)V

    return-void
.end method

.method public final a(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 53
    instance-of v0, p1, Lco/uk/getmondo/api/authentication/OAuthException;

    if-eqz v0, :cond_1

    .line 54
    iget-object v0, p0, Lco/uk/getmondo/signup/f$b$1;->a:Lco/uk/getmondo/signup/f$b;

    iget-object v0, v0, Lco/uk/getmondo/signup/f$b;->b:Lco/uk/getmondo/signup/f$a;

    const v1, 0x7f0a0180

    check-cast p1, Lco/uk/getmondo/api/authentication/OAuthException;

    invoke-virtual {p1}, Lco/uk/getmondo/api/authentication/OAuthException;->a()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lco/uk/getmondo/signup/f$a;->a(ILjava/lang/String;)V

    .line 67
    :cond_0
    :goto_0
    return-void

    .line 58
    :cond_1
    invoke-static {}, Lco/uk/getmondo/api/authentication/a;->values()[Lco/uk/getmondo/api/authentication/a;

    move-result-object v0

    check-cast v0, [Lco/uk/getmondo/common/e/f;

    invoke-static {p1, v0}, Lco/uk/getmondo/common/e/c;->a(Ljava/lang/Throwable;[Lco/uk/getmondo/common/e/f;)Lco/uk/getmondo/common/e/f;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/api/authentication/a;

    .line 59
    sget-object v1, Lco/uk/getmondo/api/authentication/a;->e:Lco/uk/getmondo/api/authentication/a;

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 60
    iget-object v0, p0, Lco/uk/getmondo/signup/f$b$1;->a:Lco/uk/getmondo/signup/f$b;

    iget-object v0, v0, Lco/uk/getmondo/signup/f$b;->b:Lco/uk/getmondo/signup/f$a;

    const v1, 0x7f0a017f

    invoke-interface {v0, v1}, Lco/uk/getmondo/signup/f$a;->b(I)V

    goto :goto_0

    .line 64
    :cond_2
    iget-object v0, p0, Lco/uk/getmondo/signup/f$b$1;->a:Lco/uk/getmondo/signup/f$b;

    iget-object v0, v0, Lco/uk/getmondo/signup/f$b;->a:Lco/uk/getmondo/signup/f;

    invoke-static {v0}, Lco/uk/getmondo/signup/f;->d(Lco/uk/getmondo/signup/f;)Lco/uk/getmondo/common/e/a;

    move-result-object v1

    const-string v0, "error"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lco/uk/getmondo/signup/f$b$1;->a:Lco/uk/getmondo/signup/f$b;

    iget-object v0, v0, Lco/uk/getmondo/signup/f$b;->b:Lco/uk/getmondo/signup/f$a;

    check-cast v0, Lco/uk/getmondo/common/e/a$a;

    invoke-virtual {v1, p1, v0}, Lco/uk/getmondo/common/e/a;->a(Ljava/lang/Throwable;Lco/uk/getmondo/common/e/a$a;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 65
    iget-object v0, p0, Lco/uk/getmondo/signup/f$b$1;->a:Lco/uk/getmondo/signup/f$b;

    iget-object v0, v0, Lco/uk/getmondo/signup/f$b;->b:Lco/uk/getmondo/signup/f$a;

    const v1, 0x7f0a0268

    invoke-interface {v0, v1}, Lco/uk/getmondo/signup/f$a;->b(I)V

    goto :goto_0
.end method
