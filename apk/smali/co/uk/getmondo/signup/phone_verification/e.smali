.class public final Lco/uk/getmondo/signup/phone_verification/e;
.super Lco/uk/getmondo/common/f/a;
.source "SmsSendFragment.kt"

# interfaces
.implements Lco/uk/getmondo/signup/phone_verification/g$a;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lco/uk/getmondo/signup/phone_verification/e$b;,
        Lco/uk/getmondo/signup/phone_verification/e$a;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000r\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u000e\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\t\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u0011\n\u0000\n\u0002\u0010\u0015\n\u0002\u0008\u000c\u0018\u0000 :2\u00020\u00012\u00020\u0002:\u0002:;B\u0005\u00a2\u0006\u0002\u0010\u0003J\u0008\u0010\u0018\u001a\u00020\rH\u0016J\u0008\u0010\u0019\u001a\u00020\u0006H\u0016J\u0008\u0010\u001a\u001a\u00020\u0006H\u0016J\u0010\u0010\u001b\u001a\u00020\u00062\u0006\u0010\u001c\u001a\u00020\u001dH\u0016J\u0012\u0010\u001e\u001a\u00020\u00062\u0008\u0010\u001f\u001a\u0004\u0018\u00010 H\u0016J$\u0010!\u001a\u00020\"2\u0006\u0010#\u001a\u00020$2\u0008\u0010%\u001a\u0004\u0018\u00010&2\u0008\u0010\u001f\u001a\u0004\u0018\u00010 H\u0016J\u0008\u0010\'\u001a\u00020\u0006H\u0016J\u0010\u0010(\u001a\u00020\u00062\u0006\u0010)\u001a\u00020\nH\u0016J+\u0010*\u001a\u00020\u00062\u0006\u0010+\u001a\u00020,2\u000c\u0010-\u001a\u0008\u0012\u0004\u0012\u00020\n0.2\u0006\u0010/\u001a\u000200H\u0016\u00a2\u0006\u0002\u00101J\u001a\u00102\u001a\u00020\u00062\u0006\u00103\u001a\u00020\"2\u0008\u0010\u001f\u001a\u0004\u0018\u00010 H\u0016J\u0008\u00104\u001a\u00020\u0006H\u0016J\u0008\u00105\u001a\u00020\u0006H\u0016J\u0010\u00106\u001a\u00020\u00062\u0006\u00107\u001a\u00020\rH\u0016J\u0008\u00108\u001a\u00020\u0006H\u0016J\u0008\u00109\u001a\u00020\u0006H\u0016R\u001a\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u00058VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0007\u0010\u0008R\u001a\u0010\t\u001a\u0008\u0012\u0004\u0012\u00020\n0\u00058VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u000b\u0010\u0008R\u001a\u0010\u000c\u001a\u0008\u0012\u0004\u0012\u00020\r0\u0005X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000e\u0010\u0008R2\u0010\u000f\u001a&\u0012\u000c\u0012\n \u0011*\u0004\u0018\u00010\r0\r \u0011*\u0012\u0012\u000c\u0012\n \u0011*\u0004\u0018\u00010\r0\r\u0018\u00010\u00100\u0010X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001e\u0010\u0012\u001a\u00020\u00138\u0006@\u0006X\u0087.\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u0014\u0010\u0015\"\u0004\u0008\u0016\u0010\u0017\u00a8\u0006<"
    }
    d2 = {
        "Lco/uk/getmondo/signup/phone_verification/SmsSendFragment;",
        "Lco/uk/getmondo/common/fragments/BaseFragment;",
        "Lco/uk/getmondo/signup/phone_verification/SmsSendPresenter$View;",
        "()V",
        "onContinueClicked",
        "Lio/reactivex/Observable;",
        "",
        "getOnContinueClicked",
        "()Lio/reactivex/Observable;",
        "onPhoneNumberChanged",
        "",
        "getOnPhoneNumberChanged",
        "onSmsPermissionsGranted",
        "",
        "getOnSmsPermissionsGranted",
        "permissionsResultRelay",
        "Lcom/jakewharton/rxrelay2/BehaviorRelay;",
        "kotlin.jvm.PlatformType",
        "presenter",
        "Lco/uk/getmondo/signup/phone_verification/SmsSendPresenter;",
        "getPresenter",
        "()Lco/uk/getmondo/signup/phone_verification/SmsSendPresenter;",
        "setPresenter",
        "(Lco/uk/getmondo/signup/phone_verification/SmsSendPresenter;)V",
        "checkSmsPermissions",
        "hideInvalidPhoneNumberError",
        "hideLoading",
        "onAttach",
        "context",
        "Landroid/content/Context;",
        "onCreate",
        "savedInstanceState",
        "Landroid/os/Bundle;",
        "onCreateView",
        "Landroid/view/View;",
        "inflater",
        "Landroid/view/LayoutInflater;",
        "container",
        "Landroid/view/ViewGroup;",
        "onDestroyView",
        "onPhoneNumberSent",
        "phoneNumber",
        "onRequestPermissionsResult",
        "requestCode",
        "",
        "permissions",
        "",
        "grantResults",
        "",
        "(I[Ljava/lang/String;[I)V",
        "onViewCreated",
        "view",
        "reloadSignupStatus",
        "requestSmsPermissions",
        "setContinueButtonEnabled",
        "enabled",
        "showInvalidPhoneNumberError",
        "showLoading",
        "Companion",
        "StepListener",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# static fields
.field public static final c:Lco/uk/getmondo/signup/phone_verification/e$a;


# instance fields
.field public a:Lco/uk/getmondo/signup/phone_verification/g;

.field private final d:Lcom/b/b/b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/b/b/b",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Lio/reactivex/n;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/n",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private f:Ljava/util/HashMap;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lco/uk/getmondo/signup/phone_verification/e$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lco/uk/getmondo/signup/phone_verification/e$a;-><init>(Lkotlin/d/b/i;)V

    sput-object v0, Lco/uk/getmondo/signup/phone_verification/e;->c:Lco/uk/getmondo/signup/phone_verification/e$a;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 23
    invoke-direct {p0}, Lco/uk/getmondo/common/f/a;-><init>()V

    .line 24
    invoke-static {}, Lcom/b/b/b;->a()Lcom/b/b/b;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/signup/phone_verification/e;->d:Lcom/b/b/b;

    .line 102
    iget-object v0, p0, Lco/uk/getmondo/signup/phone_verification/e;->d:Lcom/b/b/b;

    const-string v1, "permissionsResultRelay"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lio/reactivex/n;

    iput-object v0, p0, Lco/uk/getmondo/signup/phone_verification/e;->e:Lio/reactivex/n;

    return-void
.end method


# virtual methods
.method public a(I)Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lco/uk/getmondo/signup/phone_verification/e;->f:Ljava/util/HashMap;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lco/uk/getmondo/signup/phone_verification/e;->f:Ljava/util/HashMap;

    :cond_0
    iget-object v0, p0, Lco/uk/getmondo/signup/phone_verification/e;->f:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    if-nez v0, :cond_1

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getView()Landroid/view/View;

    move-result-object v0

    if-nez v0, :cond_2

    const/4 v0, 0x0

    :cond_1
    :goto_0
    return-object v0

    :cond_2
    invoke-virtual {v0, p1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/signup/phone_verification/e;->f:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method public a()Lio/reactivex/n;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Lkotlin/n;",
            ">;"
        }
    .end annotation

    .prologue
    .line 66
    sget v0, Lco/uk/getmondo/c$a;->phoneNumberEditText:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/phone_verification/e;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    .line 117
    invoke-static {v0}, Lcom/b/a/d/e;->a(Landroid/widget/TextView;)Lio/reactivex/n;

    move-result-object v0

    const-string v1, "RxTextView.editorActions(this)"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lio/reactivex/r;

    .line 66
    sget v1, Lco/uk/getmondo/c$a;->continueButton:I

    invoke-virtual {p0, v1}, Lco/uk/getmondo/signup/phone_verification/e;->a(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lco/uk/getmondo/common/ui/ProgressButton;

    .line 118
    invoke-static {v1}, Lcom/b/a/c/c;->a(Landroid/view/View;)Lio/reactivex/n;

    move-result-object v2

    sget-object v1, Lcom/b/a/a/d;->a:Lcom/b/a/a/d;

    check-cast v1, Lio/reactivex/c/h;

    invoke-virtual {v2, v1}, Lio/reactivex/n;->map(Lio/reactivex/c/h;)Lio/reactivex/n;

    move-result-object v1

    const-string v2, "RxView.clicks(this).map(VoidToUnit)"

    invoke-static {v1, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v1, Lio/reactivex/r;

    .line 66
    invoke-static {v0, v1}, Lio/reactivex/n;->merge(Lio/reactivex/r;Lio/reactivex/r;)Lio/reactivex/n;

    move-result-object v1

    sget-object v0, Lco/uk/getmondo/signup/phone_verification/e$c;->a:Lco/uk/getmondo/signup/phone_verification/e$c;

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {v1, v0}, Lio/reactivex/n;->map(Lio/reactivex/c/h;)Lio/reactivex/n;

    move-result-object v0

    const-string v1, "Observable.merge(phoneNu\u2026on.clicks()).map { Unit }"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public a(Ljava/lang/String;)V
    .locals 2

    .prologue
    const-string v0, "phoneNumber"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 93
    invoke-virtual {p0}, Lco/uk/getmondo/signup/phone_verification/e;->getActivity()Landroid/support/v4/app/j;

    move-result-object v0

    if-nez v0, :cond_0

    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type co.uk.getmondo.signup.phone_verification.SmsSendFragment.StepListener"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    check-cast v0, Lco/uk/getmondo/signup/phone_verification/e$b;

    invoke-interface {v0, p1}, Lco/uk/getmondo/signup/phone_verification/e$b;->a(Ljava/lang/String;)V

    .line 94
    return-void
.end method

.method public a(Z)V
    .locals 1

    .prologue
    .line 80
    sget v0, Lco/uk/getmondo/c$a;->continueButton:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/phone_verification/e;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/common/ui/ProgressButton;

    invoke-virtual {v0, p1}, Lco/uk/getmondo/common/ui/ProgressButton;->setEnabled(Z)V

    .line 81
    return-void
.end method

.method public b()V
    .locals 1

    .prologue
    .line 97
    invoke-virtual {p0}, Lco/uk/getmondo/signup/phone_verification/e;->getActivity()Landroid/support/v4/app/j;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/signup/i$a;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lco/uk/getmondo/signup/i$a;->b()V

    .line 98
    :cond_0
    return-void
.end method

.method public c()Lio/reactivex/n;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 69
    sget v0, Lco/uk/getmondo/c$a;->phoneNumberEditText:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/phone_verification/e;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    .line 119
    invoke-static {v0}, Lcom/b/a/d/e;->c(Landroid/widget/TextView;)Lcom/b/a/a;

    move-result-object v1

    const-string v0, "RxTextView.textChanges(this)"

    invoke-static {v1, v0}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 69
    sget-object v0, Lco/uk/getmondo/signup/phone_verification/e$d;->a:Lco/uk/getmondo/signup/phone_verification/e$d;

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {v1, v0}, Lcom/b/a/a;->map(Lio/reactivex/c/h;)Lio/reactivex/n;

    move-result-object v0

    const-string v1, "phoneNumberEditText.text\u2026s().map { it.toString() }"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public d()V
    .locals 2

    .prologue
    .line 72
    sget v0, Lco/uk/getmondo/c$a;->continueButton:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/phone_verification/e;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/common/ui/ProgressButton;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lco/uk/getmondo/common/ui/ProgressButton;->setLoading(Z)V

    .line 73
    return-void
.end method

.method public e()V
    .locals 2

    .prologue
    .line 76
    sget v0, Lco/uk/getmondo/c$a;->continueButton:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/phone_verification/e;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/common/ui/ProgressButton;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lco/uk/getmondo/common/ui/ProgressButton;->setLoading(Z)V

    .line 77
    return-void
.end method

.method public f()V
    .locals 2

    .prologue
    .line 84
    sget v0, Lco/uk/getmondo/c$a;->phoneNumberInputLayout:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/phone_verification/e;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/design/widget/TextInputLayout;

    const v1, 0x7f0a039a

    invoke-virtual {p0, v1}, Lco/uk/getmondo/signup/phone_verification/e;->getString(I)Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Landroid/support/design/widget/TextInputLayout;->setError(Ljava/lang/CharSequence;)V

    .line 85
    return-void
.end method

.method public g()V
    .locals 2

    .prologue
    .line 88
    sget v0, Lco/uk/getmondo/c$a;->phoneNumberInputLayout:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/phone_verification/e;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/design/widget/TextInputLayout;

    const/4 v1, 0x0

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Landroid/support/design/widget/TextInputLayout;->setError(Ljava/lang/CharSequence;)V

    .line 89
    sget v0, Lco/uk/getmondo/c$a;->phoneNumberInputLayout:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/phone_verification/e;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/design/widget/TextInputLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/design/widget/TextInputLayout;->setErrorEnabled(Z)V

    .line 90
    return-void
.end method

.method public h()Z
    .locals 3

    .prologue
    .line 100
    sget-object v1, Lco/uk/getmondo/common/k/k;->a:Lco/uk/getmondo/common/k/k$a;

    invoke-virtual {p0}, Lco/uk/getmondo/signup/phone_verification/e;->getActivity()Landroid/support/v4/app/j;

    move-result-object v0

    const-string v2, "activity"

    invoke-static {v0, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/content/Context;

    invoke-virtual {v1, v0}, Lco/uk/getmondo/common/k/k$a;->a(Landroid/content/Context;)Z

    move-result v0

    return v0
.end method

.method public i()Lio/reactivex/n;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .prologue
    .line 102
    iget-object v0, p0, Lco/uk/getmondo/signup/phone_verification/e;->e:Lio/reactivex/n;

    return-object v0
.end method

.method public j()V
    .locals 3

    .prologue
    .line 105
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "android.permission.RECEIVE_SMS"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "android.permission.READ_SMS"

    aput-object v2, v0, v1

    check-cast v0, [Ljava/lang/Object;

    .line 120
    check-cast v0, [Ljava/lang/String;

    .line 105
    const/16 v1, 0x3e9

    invoke-virtual {p0, v0, v1}, Lco/uk/getmondo/signup/phone_verification/e;->requestPermissions([Ljava/lang/String;I)V

    .line 106
    return-void
.end method

.method public k()V
    .locals 1

    iget-object v0, p0, Lco/uk/getmondo/signup/phone_verification/e;->f:Ljava/util/HashMap;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lco/uk/getmondo/signup/phone_verification/e;->f:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    :cond_0
    return-void
.end method

.method public onAttach(Landroid/content/Context;)V
    .locals 2

    .prologue
    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 34
    invoke-super {p0, p1}, Lco/uk/getmondo/common/f/a;->onAttach(Landroid/content/Context;)V

    .line 35
    instance-of v0, p1, Lco/uk/getmondo/signup/phone_verification/e$b;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Activity must implement "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-class v1, Lco/uk/getmondo/signup/phone_verification/e$b;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    .line 36
    :cond_0
    instance-of v0, p1, Lco/uk/getmondo/signup/i$a;

    if-nez v0, :cond_1

    .line 37
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Activity must implement "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-class v1, Lco/uk/getmondo/signup/i$a;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 36
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    .line 39
    :cond_1
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 29
    invoke-super {p0, p1}, Lco/uk/getmondo/common/f/a;->onCreate(Landroid/os/Bundle;)V

    .line 30
    invoke-virtual {p0}, Lco/uk/getmondo/signup/phone_verification/e;->B()Lco/uk/getmondo/common/h/b/b;

    move-result-object v0

    invoke-interface {v0, p0}, Lco/uk/getmondo/common/h/b/b;->a(Lco/uk/getmondo/signup/phone_verification/e;)V

    .line 31
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2

    .prologue
    const-string v0, "inflater"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 42
    const v0, 0x7f0500a7

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    const-string v1, "inflater.inflate(R.layou\u2026s_send, container, false)"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public onDestroyView()V
    .locals 2

    .prologue
    .line 50
    iget-object v0, p0, Lco/uk/getmondo/signup/phone_verification/e;->a:Lco/uk/getmondo/signup/phone_verification/g;

    if-nez v0, :cond_0

    const-string v1, "presenter"

    invoke-static {v1}, Lkotlin/d/b/l;->b(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0}, Lco/uk/getmondo/signup/phone_verification/g;->b()V

    .line 51
    invoke-super {p0}, Lco/uk/getmondo/common/f/a;->onDestroyView()V

    .line 52
    invoke-virtual {p0}, Lco/uk/getmondo/signup/phone_verification/e;->k()V

    return-void
.end method

.method public onRequestPermissionsResult(I[Ljava/lang/String;[I)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    const-string v2, "permissions"

    invoke-static {p2, v2}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v2, "grantResults"

    invoke-static {p3, v2}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 55
    const/16 v2, 0x3e9

    if-ne p1, v2, :cond_1

    .line 56
    array-length v2, p3

    if-le v2, v0, :cond_0

    aget v2, p3, v1

    if-nez v2, :cond_0

    aget v2, p3, v0

    if-nez v2, :cond_0

    .line 59
    :goto_0
    iget-object v1, p0, Lco/uk/getmondo/signup/phone_verification/e;->d:Lcom/b/b/b;

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/b/b/b;->a(Ljava/lang/Object;)V

    .line 63
    :goto_1
    return-void

    :cond_0
    move v0, v1

    .line 56
    goto :goto_0

    .line 62
    :cond_1
    invoke-super {p0, p1, p2, p3}, Lco/uk/getmondo/common/f/a;->onRequestPermissionsResult(I[Ljava/lang/String;[I)V

    goto :goto_1
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 2

    .prologue
    const-string v0, "view"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 45
    invoke-super {p0, p1, p2}, Lco/uk/getmondo/common/f/a;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 46
    iget-object v0, p0, Lco/uk/getmondo/signup/phone_verification/e;->a:Lco/uk/getmondo/signup/phone_verification/g;

    if-nez v0, :cond_0

    const-string v1, "presenter"

    invoke-static {v1}, Lkotlin/d/b/l;->b(Ljava/lang/String;)V

    :cond_0
    check-cast p0, Lco/uk/getmondo/signup/phone_verification/g$a;

    invoke-virtual {v0, p0}, Lco/uk/getmondo/signup/phone_verification/g;->a(Lco/uk/getmondo/signup/phone_verification/g$a;)V

    .line 47
    return-void
.end method
