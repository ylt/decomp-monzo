.class public final Lco/uk/getmondo/signup/phone_verification/j;
.super Lco/uk/getmondo/common/f/a;
.source "SmsVerificationFragment.kt"

# interfaces
.implements Lco/uk/getmondo/signup/phone_verification/l$a;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lco/uk/getmondo/signup/phone_verification/j$d;,
        Lco/uk/getmondo/signup/phone_verification/j$c;,
        Lco/uk/getmondo/signup/phone_verification/j$b;,
        Lco/uk/getmondo/signup/phone_verification/j$e;,
        Lco/uk/getmondo/signup/phone_verification/j$a;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000V\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\u0008\u0003\n\u0002\u0010\u0002\n\u0002\u0008\u0007\n\u0002\u0018\u0002\n\u0002\u0008\u000b\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0007\n\u0002\u0010\u000b\n\u0002\u0008\u000b\u0018\u0000 52\u00020\u00012\u00020\u0002:\u000556789B\u0005\u00a2\u0006\u0002\u0010\u0003J\u0010\u0010\u0017\u001a\u00020\n2\u0006\u0010\u0018\u001a\u00020\u0006H\u0016J\u0008\u0010\u0019\u001a\u00020\nH\u0016J\u0008\u0010\u001a\u001a\u00020\nH\u0016J\u0008\u0010\u001b\u001a\u00020\nH\u0016J\u0010\u0010\u001c\u001a\u00020\n2\u0006\u0010\u001d\u001a\u00020\u001eH\u0016J\u0012\u0010\u001f\u001a\u00020\n2\u0008\u0010 \u001a\u0004\u0018\u00010!H\u0016J$\u0010\"\u001a\u00020#2\u0006\u0010$\u001a\u00020%2\u0008\u0010&\u001a\u0004\u0018\u00010\'2\u0008\u0010 \u001a\u0004\u0018\u00010!H\u0016J\u0008\u0010(\u001a\u00020\nH\u0016J\u0008\u0010)\u001a\u00020\nH\u0016J\u001a\u0010*\u001a\u00020\n2\u0006\u0010+\u001a\u00020#2\u0008\u0010 \u001a\u0004\u0018\u00010!H\u0016J\u0008\u0010,\u001a\u00020\nH\u0016J\u0010\u0010-\u001a\u00020\n2\u0006\u0010.\u001a\u00020/H\u0016J\u0008\u00100\u001a\u00020\nH\u0016J\u0008\u00101\u001a\u00020\nH\u0016J\u0008\u00102\u001a\u00020\nH\u0016J\u0008\u00103\u001a\u00020\nH\u0016J\u0008\u00104\u001a\u00020\nH\u0016R\u001a\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u00058VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0007\u0010\u0008R\u001a\u0010\t\u001a\u0008\u0012\u0004\u0012\u00020\n0\u00058VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u000b\u0010\u0008R\u001a\u0010\u000c\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u00058VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\r\u0010\u0008R\u0014\u0010\u000e\u001a\u00020\u00068VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u000f\u0010\u0010R\u001e\u0010\u0011\u001a\u00020\u00128\u0006@\u0006X\u0087.\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u0013\u0010\u0014\"\u0004\u0008\u0015\u0010\u0016\u00a8\u0006:"
    }
    d2 = {
        "Lco/uk/getmondo/signup/phone_verification/SmsVerificationFragment;",
        "Lco/uk/getmondo/common/fragments/BaseFragment;",
        "Lco/uk/getmondo/signup/phone_verification/SmsVerificationPresenter$View;",
        "()V",
        "onCodeChanged",
        "Lio/reactivex/Observable;",
        "",
        "getOnCodeChanged",
        "()Lio/reactivex/Observable;",
        "onContinueClicked",
        "",
        "getOnContinueClicked",
        "onSmsCodeReceived",
        "getOnSmsCodeReceived",
        "phoneNumber",
        "getPhoneNumber",
        "()Ljava/lang/String;",
        "presenter",
        "Lco/uk/getmondo/signup/phone_verification/SmsVerificationPresenter;",
        "getPresenter",
        "()Lco/uk/getmondo/signup/phone_verification/SmsVerificationPresenter;",
        "setPresenter",
        "(Lco/uk/getmondo/signup/phone_verification/SmsVerificationPresenter;)V",
        "fillCode",
        "code",
        "hideIncorrectCodeError",
        "hideLoading",
        "navigateToSmsSendScreen",
        "onAttach",
        "context",
        "Landroid/content/Context;",
        "onCreate",
        "savedInstanceState",
        "Landroid/os/Bundle;",
        "onCreateView",
        "Landroid/view/View;",
        "inflater",
        "Landroid/view/LayoutInflater;",
        "container",
        "Landroid/view/ViewGroup;",
        "onDestroyView",
        "onPhoneNumberVerified",
        "onViewCreated",
        "view",
        "reloadSignupStatus",
        "setContinueButtonEnabled",
        "enabled",
        "",
        "showExpiredCodeError",
        "showIncorrectCodeError",
        "showLoading",
        "showPhoneNumberAlreadyInUseError",
        "showTooManyAttemptsError",
        "Companion",
        "ExpiredCodeDialog",
        "PhoneNumberAlreadyInUseDialog",
        "StepListener",
        "TooManyAttemptsDialog",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# static fields
.field public static final c:Lco/uk/getmondo/signup/phone_verification/j$a;


# instance fields
.field public a:Lco/uk/getmondo/signup/phone_verification/l;

.field private d:Ljava/util/HashMap;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lco/uk/getmondo/signup/phone_verification/j$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lco/uk/getmondo/signup/phone_verification/j$a;-><init>(Lkotlin/d/b/i;)V

    sput-object v0, Lco/uk/getmondo/signup/phone_verification/j;->c:Lco/uk/getmondo/signup/phone_verification/j$a;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Lco/uk/getmondo/common/f/a;-><init>()V

    return-void
.end method


# virtual methods
.method public a(I)Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lco/uk/getmondo/signup/phone_verification/j;->d:Ljava/util/HashMap;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lco/uk/getmondo/signup/phone_verification/j;->d:Ljava/util/HashMap;

    :cond_0
    iget-object v0, p0, Lco/uk/getmondo/signup/phone_verification/j;->d:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    if-nez v0, :cond_1

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getView()Landroid/view/View;

    move-result-object v0

    if-nez v0, :cond_2

    const/4 v0, 0x0

    :cond_1
    :goto_0
    return-object v0

    :cond_2
    invoke-virtual {v0, p1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/signup/phone_verification/j;->d:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method public final a()Lco/uk/getmondo/signup/phone_verification/l;
    .locals 2

    .prologue
    .line 27
    iget-object v0, p0, Lco/uk/getmondo/signup/phone_verification/j;->a:Lco/uk/getmondo/signup/phone_verification/l;

    if-nez v0, :cond_0

    const-string v1, "presenter"

    invoke-static {v1}, Lkotlin/d/b/l;->b(Ljava/lang/String;)V

    :cond_0
    return-object v0
.end method

.method public a(Ljava/lang/String;)V
    .locals 1

    .prologue
    const-string v0, "code"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 125
    sget v0, Lco/uk/getmondo/c$a;->verificationCodeEditText:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/phone_verification/j;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    check-cast p1, Ljava/lang/CharSequence;

    invoke-virtual {v0, p1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 126
    return-void
.end method

.method public a(Z)V
    .locals 1

    .prologue
    .line 80
    sget v0, Lco/uk/getmondo/c$a;->continueButton:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/phone_verification/j;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/common/ui/ProgressButton;

    invoke-virtual {v0, p1}, Lco/uk/getmondo/common/ui/ProgressButton;->setEnabled(Z)V

    .line 81
    return-void
.end method

.method public b()V
    .locals 1

    .prologue
    .line 91
    invoke-virtual {p0}, Lco/uk/getmondo/signup/phone_verification/j;->getActivity()Landroid/support/v4/app/j;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/signup/i$a;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lco/uk/getmondo/signup/i$a;->b()V

    .line 92
    :cond_0
    return-void
.end method

.method public c()Lio/reactivex/n;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 58
    new-instance v0, Lco/uk/getmondo/signup/phone_verification/j$h;

    invoke-direct {v0, p0}, Lco/uk/getmondo/signup/phone_verification/j$h;-><init>(Lco/uk/getmondo/signup/phone_verification/j;)V

    check-cast v0, Lio/reactivex/p;

    invoke-static {v0}, Lio/reactivex/n;->create(Lio/reactivex/p;)Lio/reactivex/n;

    move-result-object v0

    const-string v1, "Observable.create { emit\u2026CodeReceiver) }\n        }"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 63
    return-object v0
.end method

.method public d()Lio/reactivex/n;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Lkotlin/n;",
            ">;"
        }
    .end annotation

    .prologue
    .line 66
    sget v0, Lco/uk/getmondo/c$a;->verificationCodeEditText:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/phone_verification/j;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    .line 191
    invoke-static {v0}, Lcom/b/a/d/e;->a(Landroid/widget/TextView;)Lio/reactivex/n;

    move-result-object v0

    const-string v1, "RxTextView.editorActions(this)"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lio/reactivex/r;

    .line 66
    sget v1, Lco/uk/getmondo/c$a;->continueButton:I

    invoke-virtual {p0, v1}, Lco/uk/getmondo/signup/phone_verification/j;->a(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lco/uk/getmondo/common/ui/ProgressButton;

    .line 192
    invoke-static {v1}, Lcom/b/a/c/c;->a(Landroid/view/View;)Lio/reactivex/n;

    move-result-object v2

    sget-object v1, Lcom/b/a/a/d;->a:Lcom/b/a/a/d;

    check-cast v1, Lio/reactivex/c/h;

    invoke-virtual {v2, v1}, Lio/reactivex/n;->map(Lio/reactivex/c/h;)Lio/reactivex/n;

    move-result-object v1

    const-string v2, "RxView.clicks(this).map(VoidToUnit)"

    invoke-static {v1, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v1, Lio/reactivex/r;

    .line 66
    invoke-static {v0, v1}, Lio/reactivex/n;->merge(Lio/reactivex/r;Lio/reactivex/r;)Lio/reactivex/n;

    move-result-object v1

    sget-object v0, Lco/uk/getmondo/signup/phone_verification/j$g;->a:Lco/uk/getmondo/signup/phone_verification/j$g;

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {v1, v0}, Lio/reactivex/n;->map(Lio/reactivex/c/h;)Lio/reactivex/n;

    move-result-object v0

    const-string v1, "Observable.merge(verific\u2026on.clicks()).map { Unit }"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public e()Lio/reactivex/n;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 69
    sget v0, Lco/uk/getmondo/c$a;->verificationCodeEditText:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/phone_verification/j;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    .line 193
    invoke-static {v0}, Lcom/b/a/d/e;->c(Landroid/widget/TextView;)Lcom/b/a/a;

    move-result-object v1

    const-string v0, "RxTextView.textChanges(this)"

    invoke-static {v1, v0}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 69
    sget-object v0, Lco/uk/getmondo/signup/phone_verification/j$f;->a:Lco/uk/getmondo/signup/phone_verification/j$f;

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {v1, v0}, Lcom/b/a/a;->map(Lio/reactivex/c/h;)Lio/reactivex/n;

    move-result-object v0

    const-string v1, "verificationCodeEditText\u2026s().map { it.toString() }"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public f()V
    .locals 2

    .prologue
    .line 72
    sget v0, Lco/uk/getmondo/c$a;->continueButton:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/phone_verification/j;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/common/ui/ProgressButton;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lco/uk/getmondo/common/ui/ProgressButton;->setLoading(Z)V

    .line 73
    return-void
.end method

.method public g()V
    .locals 2

    .prologue
    .line 76
    sget v0, Lco/uk/getmondo/c$a;->continueButton:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/phone_verification/j;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/common/ui/ProgressButton;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lco/uk/getmondo/common/ui/ProgressButton;->setLoading(Z)V

    .line 77
    return-void
.end method

.method public h()Ljava/lang/String;
    .locals 2

    .prologue
    .line 84
    invoke-virtual {p0}, Lco/uk/getmondo/signup/phone_verification/j;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "phoneNumber"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "arguments.getString(EXTRA_PHONE_NUMBER)"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public i()V
    .locals 2

    .prologue
    .line 87
    invoke-virtual {p0}, Lco/uk/getmondo/signup/phone_verification/j;->getActivity()Landroid/support/v4/app/j;

    move-result-object v0

    if-nez v0, :cond_0

    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type co.uk.getmondo.signup.phone_verification.SmsVerificationFragment.StepListener"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    check-cast v0, Lco/uk/getmondo/signup/phone_verification/j$d;

    invoke-interface {v0}, Lco/uk/getmondo/signup/phone_verification/j$d;->d()V

    .line 88
    return-void
.end method

.method public j()V
    .locals 3

    .prologue
    .line 95
    new-instance v1, Lco/uk/getmondo/signup/phone_verification/j$b;

    invoke-direct {v1}, Lco/uk/getmondo/signup/phone_verification/j$b;-><init>()V

    move-object v0, p0

    .line 96
    check-cast v0, Landroid/support/v4/app/Fragment;

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Lco/uk/getmondo/signup/phone_verification/j$b;->setTargetFragment(Landroid/support/v4/app/Fragment;I)V

    .line 97
    invoke-virtual {p0}, Lco/uk/getmondo/signup/phone_verification/j;->getFragmentManager()Landroid/support/v4/app/n;

    move-result-object v0

    const-string v2, "errorDialog"

    invoke-virtual {v1, v0, v2}, Lco/uk/getmondo/signup/phone_verification/j$b;->show(Landroid/support/v4/app/n;Ljava/lang/String;)V

    .line 98
    return-void
.end method

.method public k()V
    .locals 2

    .prologue
    .line 101
    sget v0, Lco/uk/getmondo/c$a;->verificationCodeInputLayout:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/phone_verification/j;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/design/widget/TextInputLayout;

    const v1, 0x7f0a039f

    invoke-virtual {p0, v1}, Lco/uk/getmondo/signup/phone_verification/j;->getString(I)Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Landroid/support/design/widget/TextInputLayout;->setError(Ljava/lang/CharSequence;)V

    .line 102
    return-void
.end method

.method public l()V
    .locals 2

    .prologue
    .line 105
    sget v0, Lco/uk/getmondo/c$a;->verificationCodeInputLayout:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/phone_verification/j;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/design/widget/TextInputLayout;

    const/4 v1, 0x0

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Landroid/support/design/widget/TextInputLayout;->setError(Ljava/lang/CharSequence;)V

    .line 106
    sget v0, Lco/uk/getmondo/c$a;->verificationCodeInputLayout:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/phone_verification/j;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/design/widget/TextInputLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/design/widget/TextInputLayout;->setErrorEnabled(Z)V

    .line 107
    return-void
.end method

.method public m()V
    .locals 3

    .prologue
    .line 110
    new-instance v1, Lco/uk/getmondo/signup/phone_verification/j$e;

    invoke-direct {v1}, Lco/uk/getmondo/signup/phone_verification/j$e;-><init>()V

    move-object v0, p0

    .line 111
    check-cast v0, Landroid/support/v4/app/Fragment;

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Lco/uk/getmondo/signup/phone_verification/j$e;->setTargetFragment(Landroid/support/v4/app/Fragment;I)V

    .line 112
    invoke-virtual {p0}, Lco/uk/getmondo/signup/phone_verification/j;->getFragmentManager()Landroid/support/v4/app/n;

    move-result-object v0

    const-string v2, "errorDialog"

    invoke-virtual {v1, v0, v2}, Lco/uk/getmondo/signup/phone_verification/j$e;->show(Landroid/support/v4/app/n;Ljava/lang/String;)V

    .line 113
    return-void
.end method

.method public n()V
    .locals 3

    .prologue
    .line 116
    new-instance v0, Lco/uk/getmondo/signup/phone_verification/j$c;

    invoke-direct {v0}, Lco/uk/getmondo/signup/phone_verification/j$c;-><init>()V

    .line 117
    invoke-virtual {p0}, Lco/uk/getmondo/signup/phone_verification/j;->getFragmentManager()Landroid/support/v4/app/n;

    move-result-object v1

    const-string v2, "errorDialog"

    invoke-virtual {v0, v1, v2}, Lco/uk/getmondo/signup/phone_verification/j$c;->show(Landroid/support/v4/app/n;Ljava/lang/String;)V

    .line 118
    return-void
.end method

.method public o()V
    .locals 2

    .prologue
    .line 121
    invoke-virtual {p0}, Lco/uk/getmondo/signup/phone_verification/j;->getActivity()Landroid/support/v4/app/j;

    move-result-object v0

    if-nez v0, :cond_0

    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type co.uk.getmondo.signup.phone_verification.SmsVerificationFragment.StepListener"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    check-cast v0, Lco/uk/getmondo/signup/phone_verification/j$d;

    invoke-interface {v0}, Lco/uk/getmondo/signup/phone_verification/j$d;->c()V

    .line 122
    return-void
.end method

.method public onAttach(Landroid/content/Context;)V
    .locals 2

    .prologue
    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 36
    invoke-super {p0, p1}, Lco/uk/getmondo/common/f/a;->onAttach(Landroid/content/Context;)V

    .line 37
    instance-of v0, p1, Lco/uk/getmondo/signup/phone_verification/j$d;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Activity must implement "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-class v1, Lco/uk/getmondo/signup/phone_verification/j$d;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    .line 38
    :cond_0
    instance-of v0, p1, Lco/uk/getmondo/signup/i$a;

    if-nez v0, :cond_1

    .line 39
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Activity must implement "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-class v1, Lco/uk/getmondo/signup/i$a;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 38
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    .line 41
    :cond_1
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 30
    invoke-super {p0, p1}, Lco/uk/getmondo/common/f/a;->onCreate(Landroid/os/Bundle;)V

    .line 31
    invoke-virtual {p0}, Lco/uk/getmondo/signup/phone_verification/j;->h()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    const-string v1, "Required value was null."

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    .line 32
    :cond_0
    invoke-virtual {p0}, Lco/uk/getmondo/signup/phone_verification/j;->B()Lco/uk/getmondo/common/h/b/b;

    move-result-object v0

    invoke-interface {v0, p0}, Lco/uk/getmondo/common/h/b/b;->a(Lco/uk/getmondo/signup/phone_verification/j;)V

    .line 33
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2

    .prologue
    const-string v0, "inflater"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 44
    const v0, 0x7f0500a8

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    const-string v1, "inflater.inflate(R.layou\u2026cation, container, false)"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public onDestroyView()V
    .locals 2

    .prologue
    .line 53
    iget-object v0, p0, Lco/uk/getmondo/signup/phone_verification/j;->a:Lco/uk/getmondo/signup/phone_verification/l;

    if-nez v0, :cond_0

    const-string v1, "presenter"

    invoke-static {v1}, Lkotlin/d/b/l;->b(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0}, Lco/uk/getmondo/signup/phone_verification/l;->b()V

    .line 54
    invoke-super {p0}, Lco/uk/getmondo/common/f/a;->onDestroyView()V

    .line 55
    invoke-virtual {p0}, Lco/uk/getmondo/signup/phone_verification/j;->p()V

    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 2

    .prologue
    const-string v0, "view"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 47
    invoke-super {p0, p1, p2}, Lco/uk/getmondo/common/f/a;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 48
    iget-object v1, p0, Lco/uk/getmondo/signup/phone_verification/j;->a:Lco/uk/getmondo/signup/phone_verification/l;

    if-nez v1, :cond_0

    const-string v0, "presenter"

    invoke-static {v0}, Lkotlin/d/b/l;->b(Ljava/lang/String;)V

    :cond_0
    move-object v0, p0

    check-cast v0, Lco/uk/getmondo/signup/phone_verification/l$a;

    invoke-virtual {v1, v0}, Lco/uk/getmondo/signup/phone_verification/l;->a(Lco/uk/getmondo/signup/phone_verification/l$a;)V

    .line 49
    sget v0, Lco/uk/getmondo/c$a;->verificationCodeEditText:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/phone_verification/j;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    invoke-static {v0}, Lco/uk/getmondo/common/ae;->a(Landroid/widget/EditText;)V

    .line 50
    return-void
.end method

.method public p()V
    .locals 1

    iget-object v0, p0, Lco/uk/getmondo/signup/phone_verification/j;->d:Ljava/util/HashMap;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lco/uk/getmondo/signup/phone_verification/j;->d:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    :cond_0
    return-void
.end method
