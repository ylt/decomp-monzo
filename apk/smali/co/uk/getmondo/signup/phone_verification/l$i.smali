.class final Lco/uk/getmondo/signup/phone_verification/l$i;
.super Ljava/lang/Object;
.source "SmsVerificationPresenter.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lco/uk/getmondo/signup/phone_verification/l;->a(Lco/uk/getmondo/signup/phone_verification/l$a;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h",
        "<TT;",
        "Lio/reactivex/l",
        "<+TR;>;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000\u0012\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\u0010\u0000\u001a\u0008\u0012\u0004\u0012\u00020\u00020\u00012\u0006\u0010\u0003\u001a\u00020\u0004H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "Lio/reactivex/Maybe;",
        "",
        "code",
        "",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field final synthetic a:Lco/uk/getmondo/signup/phone_verification/l;

.field final synthetic b:Lco/uk/getmondo/signup/phone_verification/l$a;


# direct methods
.method constructor <init>(Lco/uk/getmondo/signup/phone_verification/l;Lco/uk/getmondo/signup/phone_verification/l$a;)V
    .locals 0

    iput-object p1, p0, Lco/uk/getmondo/signup/phone_verification/l$i;->a:Lco/uk/getmondo/signup/phone_verification/l;

    iput-object p2, p0, Lco/uk/getmondo/signup/phone_verification/l$i;->b:Lco/uk/getmondo/signup/phone_verification/l$a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Lio/reactivex/h;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/h",
            "<",
            "Lkotlin/n;",
            ">;"
        }
    .end annotation

    .prologue
    const-string v0, "code"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 56
    iget-object v0, p0, Lco/uk/getmondo/signup/phone_verification/l$i;->a:Lco/uk/getmondo/signup/phone_verification/l;

    invoke-static {v0}, Lco/uk/getmondo/signup/phone_verification/l;->a(Lco/uk/getmondo/signup/phone_verification/l;)Lco/uk/getmondo/signup/phone_verification/c;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/signup/phone_verification/l$i;->b:Lco/uk/getmondo/signup/phone_verification/l$a;

    invoke-interface {v1}, Lco/uk/getmondo/signup/phone_verification/l$a;->h()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, Lco/uk/getmondo/signup/phone_verification/c;->a(Ljava/lang/String;Ljava/lang/String;)Lio/reactivex/b;

    move-result-object v0

    .line 57
    iget-object v1, p0, Lco/uk/getmondo/signup/phone_verification/l$i;->a:Lco/uk/getmondo/signup/phone_verification/l;

    invoke-static {v1}, Lco/uk/getmondo/signup/phone_verification/l;->b(Lco/uk/getmondo/signup/phone_verification/l;)Lio/reactivex/u;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/b;->b(Lio/reactivex/u;)Lio/reactivex/b;

    move-result-object v0

    .line 58
    iget-object v1, p0, Lco/uk/getmondo/signup/phone_verification/l$i;->a:Lco/uk/getmondo/signup/phone_verification/l;

    invoke-static {v1}, Lco/uk/getmondo/signup/phone_verification/l;->c(Lco/uk/getmondo/signup/phone_verification/l;)Lio/reactivex/u;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/b;->a(Lio/reactivex/u;)Lio/reactivex/b;

    move-result-object v1

    .line 59
    new-instance v0, Lco/uk/getmondo/signup/phone_verification/l$i$1;

    invoke-direct {v0, p0}, Lco/uk/getmondo/signup/phone_verification/l$i$1;-><init>(Lco/uk/getmondo/signup/phone_verification/l$i;)V

    check-cast v0, Lio/reactivex/c/g;

    invoke-virtual {v1, v0}, Lio/reactivex/b;->c(Lio/reactivex/c/g;)Lio/reactivex/b;

    move-result-object v1

    .line 60
    new-instance v0, Lco/uk/getmondo/signup/phone_verification/l$i$2;

    invoke-direct {v0, p0}, Lco/uk/getmondo/signup/phone_verification/l$i$2;-><init>(Lco/uk/getmondo/signup/phone_verification/l$i;)V

    check-cast v0, Lio/reactivex/c/g;

    invoke-virtual {v1, v0}, Lio/reactivex/b;->a(Lio/reactivex/c/g;)Lio/reactivex/b;

    move-result-object v0

    .line 61
    sget-object v1, Lkotlin/n;->a:Lkotlin/n;

    invoke-static {v0, v1}, Lco/uk/getmondo/common/j/f;->a(Lio/reactivex/b;Ljava/lang/Object;)Lio/reactivex/h;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 21
    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1}, Lco/uk/getmondo/signup/phone_verification/l$i;->a(Ljava/lang/String;)Lio/reactivex/h;

    move-result-object v0

    return-object v0
.end method
