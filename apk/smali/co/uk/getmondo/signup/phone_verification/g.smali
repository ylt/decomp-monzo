.class public final Lco/uk/getmondo/signup/phone_verification/g;
.super Lco/uk/getmondo/common/ui/b;
.source "SmsSendPresenter.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lco/uk/getmondo/signup/phone_verification/g$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lco/uk/getmondo/common/ui/b",
        "<",
        "Lco/uk/getmondo/signup/phone_verification/g$a;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u00008\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u0003\n\u0002\u0008\u0004\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00020\u0001:\u0001\u0013B3\u0008\u0007\u0012\u0008\u0008\u0001\u0010\u0003\u001a\u00020\u0004\u0012\u0008\u0008\u0001\u0010\u0005\u001a\u00020\u0004\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b\u00a2\u0006\u0002\u0010\u000cJ\u0018\u0010\r\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u00102\u0006\u0010\u0011\u001a\u00020\u0002H\u0002J\u0010\u0010\u0012\u001a\u00020\u000e2\u0006\u0010\u0011\u001a\u00020\u0002H\u0016R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0014"
    }
    d2 = {
        "Lco/uk/getmondo/signup/phone_verification/SmsSendPresenter;",
        "Lco/uk/getmondo/common/ui/BasePresenter;",
        "Lco/uk/getmondo/signup/phone_verification/SmsSendPresenter$View;",
        "uiScheduler",
        "Lio/reactivex/Scheduler;",
        "ioScheduler",
        "phoneVerificationManager",
        "Lco/uk/getmondo/signup/phone_verification/PhoneVerificationManager;",
        "apiErrorHandler",
        "Lco/uk/getmondo/common/errors/ApiErrorHandler;",
        "analyticsService",
        "Lco/uk/getmondo/common/AnalyticsService;",
        "(Lio/reactivex/Scheduler;Lio/reactivex/Scheduler;Lco/uk/getmondo/signup/phone_verification/PhoneVerificationManager;Lco/uk/getmondo/common/errors/ApiErrorHandler;Lco/uk/getmondo/common/AnalyticsService;)V",
        "handleError",
        "",
        "throwable",
        "",
        "view",
        "register",
        "View",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field private final c:Lio/reactivex/u;

.field private final d:Lio/reactivex/u;

.field private final e:Lco/uk/getmondo/signup/phone_verification/c;

.field private final f:Lco/uk/getmondo/common/e/a;

.field private final g:Lco/uk/getmondo/common/a;


# direct methods
.method public constructor <init>(Lio/reactivex/u;Lio/reactivex/u;Lco/uk/getmondo/signup/phone_verification/c;Lco/uk/getmondo/common/e/a;Lco/uk/getmondo/common/a;)V
    .locals 1

    .prologue
    const-string v0, "uiScheduler"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "ioScheduler"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "phoneVerificationManager"

    invoke-static {p3, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "apiErrorHandler"

    invoke-static {p4, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "analyticsService"

    invoke-static {p5, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 28
    invoke-direct {p0}, Lco/uk/getmondo/common/ui/b;-><init>()V

    iput-object p1, p0, Lco/uk/getmondo/signup/phone_verification/g;->c:Lio/reactivex/u;

    iput-object p2, p0, Lco/uk/getmondo/signup/phone_verification/g;->d:Lio/reactivex/u;

    iput-object p3, p0, Lco/uk/getmondo/signup/phone_verification/g;->e:Lco/uk/getmondo/signup/phone_verification/c;

    iput-object p4, p0, Lco/uk/getmondo/signup/phone_verification/g;->f:Lco/uk/getmondo/common/e/a;

    iput-object p5, p0, Lco/uk/getmondo/signup/phone_verification/g;->g:Lco/uk/getmondo/common/a;

    return-void
.end method

.method public static final synthetic a(Lco/uk/getmondo/signup/phone_verification/g;)Lco/uk/getmondo/signup/phone_verification/c;
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lco/uk/getmondo/signup/phone_verification/g;->e:Lco/uk/getmondo/signup/phone_verification/c;

    return-object v0
.end method

.method public static final synthetic a(Lco/uk/getmondo/signup/phone_verification/g;Ljava/lang/Throwable;Lco/uk/getmondo/signup/phone_verification/g$a;)V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0, p1, p2}, Lco/uk/getmondo/signup/phone_verification/g;->a(Ljava/lang/Throwable;Lco/uk/getmondo/signup/phone_verification/g$a;)V

    return-void
.end method

.method private final a(Ljava/lang/Throwable;Lco/uk/getmondo/signup/phone_verification/g$a;)V
    .locals 2

    .prologue
    .line 79
    invoke-interface {p2}, Lco/uk/getmondo/signup/phone_verification/g$a;->e()V

    .line 81
    invoke-static {}, Lco/uk/getmondo/signup/phone_verification/b;->values()[Lco/uk/getmondo/signup/phone_verification/b;

    move-result-object v0

    check-cast v0, [Lco/uk/getmondo/common/e/f;

    invoke-static {p1, v0}, Lco/uk/getmondo/common/e/c;->a(Ljava/lang/Throwable;[Lco/uk/getmondo/common/e/f;)Lco/uk/getmondo/common/e/f;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/signup/phone_verification/b;

    if-nez v0, :cond_3

    .line 86
    :goto_0
    const/4 v0, 0x0

    .line 89
    :goto_1
    if-nez v0, :cond_0

    .line 90
    sget-object v1, Lco/uk/getmondo/signup/i;->a:Lco/uk/getmondo/signup/i;

    move-object v0, p2

    check-cast v0, Lco/uk/getmondo/signup/i$a;

    invoke-virtual {v1, p1, v0}, Lco/uk/getmondo/signup/i;->a(Ljava/lang/Throwable;Lco/uk/getmondo/signup/i$a;)Z

    move-result v0

    .line 93
    :cond_0
    if-nez v0, :cond_1

    .line 94
    iget-object v1, p0, Lco/uk/getmondo/signup/phone_verification/g;->f:Lco/uk/getmondo/common/e/a;

    move-object v0, p2

    check-cast v0, Lco/uk/getmondo/common/e/a$a;

    invoke-virtual {v1, p1, v0}, Lco/uk/getmondo/common/e/a;->a(Ljava/lang/Throwable;Lco/uk/getmondo/common/e/a$a;)Z

    move-result v0

    .line 97
    :cond_1
    if-nez v0, :cond_2

    .line 98
    const v0, 0x7f0a0196

    invoke-interface {p2, v0}, Lco/uk/getmondo/signup/phone_verification/g$a;->b(I)V

    .line 100
    :cond_2
    return-void

    .line 81
    :cond_3
    sget-object v1, Lco/uk/getmondo/signup/phone_verification/h;->a:[I

    invoke-virtual {v0}, Lco/uk/getmondo/signup/phone_verification/b;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 83
    :pswitch_0
    invoke-interface {p2}, Lco/uk/getmondo/signup/phone_verification/g$a;->f()V

    .line 84
    const/4 v0, 0x1

    goto :goto_1

    .line 81
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public static final synthetic b(Lco/uk/getmondo/signup/phone_verification/g;)Lio/reactivex/u;
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lco/uk/getmondo/signup/phone_verification/g;->d:Lio/reactivex/u;

    return-object v0
.end method

.method public static final synthetic c(Lco/uk/getmondo/signup/phone_verification/g;)Lio/reactivex/u;
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lco/uk/getmondo/signup/phone_verification/g;->c:Lio/reactivex/u;

    return-object v0
.end method


# virtual methods
.method public bridge synthetic a(Lco/uk/getmondo/common/ui/f;)V
    .locals 0

    .prologue
    .line 21
    check-cast p1, Lco/uk/getmondo/signup/phone_verification/g$a;

    invoke-virtual {p0, p1}, Lco/uk/getmondo/signup/phone_verification/g;->a(Lco/uk/getmondo/signup/phone_verification/g$a;)V

    return-void
.end method

.method public a(Lco/uk/getmondo/signup/phone_verification/g$a;)V
    .locals 5

    .prologue
    const-string v0, "view"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v0, p1

    .line 31
    check-cast v0, Lco/uk/getmondo/common/ui/f;

    invoke-super {p0, v0}, Lco/uk/getmondo/common/ui/b;->a(Lco/uk/getmondo/common/ui/f;)V

    .line 33
    invoke-interface {p1}, Lco/uk/getmondo/signup/phone_verification/g$a;->c()Lio/reactivex/n;

    move-result-object v0

    invoke-virtual {v0}, Lio/reactivex/n;->share()Lio/reactivex/n;

    move-result-object v1

    .line 34
    invoke-interface {p1}, Lco/uk/getmondo/signup/phone_verification/g$a;->a()Lio/reactivex/n;

    move-result-object v0

    invoke-virtual {v0}, Lio/reactivex/n;->share()Lio/reactivex/n;

    move-result-object v2

    .line 36
    iget-object v0, p0, Lco/uk/getmondo/signup/phone_verification/g;->g:Lco/uk/getmondo/common/a;

    sget-object v3, Lco/uk/getmondo/api/model/tracking/Impression;->Companion:Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    invoke-virtual {v3}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->A()Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v3

    invoke-virtual {v0, v3}, Lco/uk/getmondo/common/a;->a(Lco/uk/getmondo/api/model/tracking/Impression;)V

    .line 39
    iget-object v3, p0, Lco/uk/getmondo/signup/phone_verification/g;->b:Lio/reactivex/b/a;

    .line 40
    sget-object v0, Lco/uk/getmondo/signup/phone_verification/g$b;->a:Lco/uk/getmondo/signup/phone_verification/g$b;

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {v1, v0}, Lio/reactivex/n;->map(Lio/reactivex/c/h;)Lio/reactivex/n;

    move-result-object v4

    .line 41
    new-instance v0, Lco/uk/getmondo/signup/phone_verification/g$c;

    invoke-direct {v0, p1}, Lco/uk/getmondo/signup/phone_verification/g$c;-><init>(Lco/uk/getmondo/signup/phone_verification/g$a;)V

    check-cast v0, Lio/reactivex/c/g;

    invoke-virtual {v4, v0}, Lio/reactivex/n;->subscribe(Lio/reactivex/c/g;)Lio/reactivex/b/b;

    move-result-object v0

    const-string v4, "phoneNumberChanges\n     \u2026ntinueButtonEnabled(it) }"

    invoke-static {v0, v4}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 39
    invoke-static {v3, v0}, Lco/uk/getmondo/common/j/f;->a(Lio/reactivex/b/a;Lio/reactivex/b/b;)Lio/reactivex/b/a;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/signup/phone_verification/g;->b:Lio/reactivex/b/a;

    .line 44
    iget-object v3, p0, Lco/uk/getmondo/signup/phone_verification/g;->b:Lio/reactivex/b/a;

    .line 45
    new-instance v0, Lco/uk/getmondo/signup/phone_verification/g$d;

    invoke-direct {v0, p1}, Lco/uk/getmondo/signup/phone_verification/g$d;-><init>(Lco/uk/getmondo/signup/phone_verification/g$a;)V

    check-cast v0, Lio/reactivex/c/g;

    invoke-virtual {v2, v0}, Lio/reactivex/n;->subscribe(Lio/reactivex/c/g;)Lio/reactivex/b/b;

    move-result-object v0

    const-string v4, "continueClicks\n         \u2026      }\n                }"

    invoke-static {v0, v4}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 44
    invoke-static {v3, v0}, Lco/uk/getmondo/common/j/f;->a(Lio/reactivex/b/a;Lio/reactivex/b/b;)Lio/reactivex/b/a;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/signup/phone_verification/g;->b:Lio/reactivex/b/a;

    .line 52
    iget-object v3, p0, Lco/uk/getmondo/signup/phone_verification/g;->b:Lio/reactivex/b/a;

    .line 53
    new-instance v0, Lco/uk/getmondo/signup/phone_verification/g$e;

    invoke-direct {v0, p1}, Lco/uk/getmondo/signup/phone_verification/g$e;-><init>(Lco/uk/getmondo/signup/phone_verification/g$a;)V

    check-cast v0, Lio/reactivex/c/g;

    invoke-virtual {v1, v0}, Lio/reactivex/n;->subscribe(Lio/reactivex/c/g;)Lio/reactivex/b/b;

    move-result-object v0

    const-string v4, "phoneNumberChanges\n     \u2026validPhoneNumberError() }"

    invoke-static {v0, v4}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 52
    invoke-static {v3, v0}, Lco/uk/getmondo/common/j/f;->a(Lio/reactivex/b/a;Lio/reactivex/b/b;)Lio/reactivex/b/a;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/signup/phone_verification/g;->b:Lio/reactivex/b/a;

    .line 55
    invoke-interface {p1}, Lco/uk/getmondo/signup/phone_verification/g$a;->h()Z

    move-result v0

    if-nez v0, :cond_0

    .line 56
    invoke-interface {p1}, Lco/uk/getmondo/signup/phone_verification/g$a;->i()Lio/reactivex/n;

    move-result-object v0

    .line 63
    :goto_0
    const-string v3, "continueClicks"

    invoke-static {v2, v3}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v3, "onSmsPermissionGranted"

    invoke-static {v0, v3}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v2, v0}, Lco/uk/getmondo/common/j/f;->a(Lio/reactivex/n;Lio/reactivex/n;)Lio/reactivex/n;

    move-result-object v2

    .line 65
    iget-object v3, p0, Lco/uk/getmondo/signup/phone_verification/g;->b:Lio/reactivex/b/a;

    .line 66
    const-string v0, "phoneNumberChanges"

    invoke-static {v1, v0}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v0, v1

    check-cast v0, Lio/reactivex/r;

    invoke-static {v2, v0}, Lco/uk/getmondo/common/j/f;->a(Lio/reactivex/n;Lio/reactivex/r;)Lio/reactivex/n;

    move-result-object v1

    .line 67
    new-instance v0, Lco/uk/getmondo/signup/phone_verification/g$f;

    invoke-direct {v0, p0, p1}, Lco/uk/getmondo/signup/phone_verification/g$f;-><init>(Lco/uk/getmondo/signup/phone_verification/g;Lco/uk/getmondo/signup/phone_verification/g$a;)V

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {v1, v0}, Lio/reactivex/n;->flatMapMaybe(Lio/reactivex/c/h;)Lio/reactivex/n;

    move-result-object v1

    .line 75
    new-instance v0, Lco/uk/getmondo/signup/phone_verification/g$g;

    invoke-direct {v0, p1}, Lco/uk/getmondo/signup/phone_verification/g$g;-><init>(Lco/uk/getmondo/signup/phone_verification/g$a;)V

    check-cast v0, Lio/reactivex/c/g;

    invoke-virtual {v1, v0}, Lio/reactivex/n;->subscribe(Lio/reactivex/c/g;)Lio/reactivex/b/b;

    move-result-object v0

    const-string v1, "continueClicksWithPermis\u2026NumberSent(phoneNumber) }"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 65
    invoke-static {v3, v0}, Lco/uk/getmondo/common/j/f;->a(Lio/reactivex/b/a;Lio/reactivex/b/b;)Lio/reactivex/b/a;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/signup/phone_verification/g;->b:Lio/reactivex/b/a;

    .line 76
    return-void

    .line 58
    :cond_0
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v0}, Lio/reactivex/n;->just(Ljava/lang/Object;)Lio/reactivex/n;

    move-result-object v0

    goto :goto_0
.end method
