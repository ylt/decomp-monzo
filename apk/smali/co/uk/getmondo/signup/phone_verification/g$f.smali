.class final Lco/uk/getmondo/signup/phone_verification/g$f;
.super Ljava/lang/Object;
.source "SmsSendPresenter.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lco/uk/getmondo/signup/phone_verification/g;->a(Lco/uk/getmondo/signup/phone_verification/g$a;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h",
        "<TT;",
        "Lio/reactivex/l",
        "<+TR;>;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000\u001c\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0010\u000b\n\u0000\u0010\u0000\u001a\u0010\u0012\u000c\u0012\n \u0003*\u0004\u0018\u00010\u00020\u00020\u00012.\u0010\u0004\u001a*\u0012\u0018\u0012\u0016\u0012\u000c\u0012\n \u0003*\u0004\u0018\u00010\u00060\u0006\u0012\u0004\u0012\u00020\u00070\u0005\u0012\u000c\u0012\n \u0003*\u0004\u0018\u00010\u00020\u00020\u0005H\n\u00a2\u0006\u0002\u0008\u0008"
    }
    d2 = {
        "<anonymous>",
        "Lio/reactivex/Maybe;",
        "",
        "kotlin.jvm.PlatformType",
        "<name for destructuring parameter 0>",
        "Lkotlin/Pair;",
        "",
        "",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field final synthetic a:Lco/uk/getmondo/signup/phone_verification/g;

.field final synthetic b:Lco/uk/getmondo/signup/phone_verification/g$a;


# direct methods
.method constructor <init>(Lco/uk/getmondo/signup/phone_verification/g;Lco/uk/getmondo/signup/phone_verification/g$a;)V
    .locals 0

    iput-object p1, p0, Lco/uk/getmondo/signup/phone_verification/g$f;->a:Lco/uk/getmondo/signup/phone_verification/g;

    iput-object p2, p0, Lco/uk/getmondo/signup/phone_verification/g$f;->b:Lco/uk/getmondo/signup/phone_verification/g$a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lkotlin/h;)Lio/reactivex/h;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/h",
            "<",
            "Lkotlin/h",
            "<",
            "Lkotlin/n;",
            "Ljava/lang/Boolean;",
            ">;",
            "Ljava/lang/String;",
            ">;)",
            "Lio/reactivex/h",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    const-string v0, "<name for destructuring parameter 0>"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lkotlin/h;->d()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 68
    iget-object v1, p0, Lco/uk/getmondo/signup/phone_verification/g$f;->a:Lco/uk/getmondo/signup/phone_verification/g;

    invoke-static {v1}, Lco/uk/getmondo/signup/phone_verification/g;->a(Lco/uk/getmondo/signup/phone_verification/g;)Lco/uk/getmondo/signup/phone_verification/c;

    move-result-object v1

    const-string v2, "phoneNumber"

    invoke-static {v0, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Lco/uk/getmondo/signup/phone_verification/c;->a(Ljava/lang/String;)Lio/reactivex/b;

    move-result-object v1

    .line 69
    iget-object v2, p0, Lco/uk/getmondo/signup/phone_verification/g$f;->a:Lco/uk/getmondo/signup/phone_verification/g;

    invoke-static {v2}, Lco/uk/getmondo/signup/phone_verification/g;->b(Lco/uk/getmondo/signup/phone_verification/g;)Lio/reactivex/u;

    move-result-object v2

    invoke-virtual {v1, v2}, Lio/reactivex/b;->b(Lio/reactivex/u;)Lio/reactivex/b;

    move-result-object v1

    .line 70
    iget-object v2, p0, Lco/uk/getmondo/signup/phone_verification/g$f;->a:Lco/uk/getmondo/signup/phone_verification/g;

    invoke-static {v2}, Lco/uk/getmondo/signup/phone_verification/g;->c(Lco/uk/getmondo/signup/phone_verification/g;)Lio/reactivex/u;

    move-result-object v2

    invoke-virtual {v1, v2}, Lio/reactivex/b;->a(Lio/reactivex/u;)Lio/reactivex/b;

    move-result-object v2

    .line 71
    new-instance v1, Lco/uk/getmondo/signup/phone_verification/g$f$1;

    invoke-direct {v1, p0}, Lco/uk/getmondo/signup/phone_verification/g$f$1;-><init>(Lco/uk/getmondo/signup/phone_verification/g$f;)V

    check-cast v1, Lio/reactivex/c/g;

    invoke-virtual {v2, v1}, Lio/reactivex/b;->c(Lio/reactivex/c/g;)Lio/reactivex/b;

    move-result-object v2

    .line 72
    new-instance v1, Lco/uk/getmondo/signup/phone_verification/g$f$2;

    invoke-direct {v1, p0}, Lco/uk/getmondo/signup/phone_verification/g$f$2;-><init>(Lco/uk/getmondo/signup/phone_verification/g$f;)V

    check-cast v1, Lio/reactivex/c/g;

    invoke-virtual {v2, v1}, Lio/reactivex/b;->a(Lio/reactivex/c/g;)Lio/reactivex/b;

    move-result-object v1

    .line 73
    invoke-static {v1, v0}, Lco/uk/getmondo/common/j/f;->a(Lio/reactivex/b;Ljava/lang/Object;)Lio/reactivex/h;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 21
    check-cast p1, Lkotlin/h;

    invoke-virtual {p0, p1}, Lco/uk/getmondo/signup/phone_verification/g$f;->a(Lkotlin/h;)Lio/reactivex/h;

    move-result-object v0

    return-object v0
.end method
