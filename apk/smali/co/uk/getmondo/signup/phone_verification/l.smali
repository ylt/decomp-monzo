.class public final Lco/uk/getmondo/signup/phone_verification/l;
.super Lco/uk/getmondo/common/ui/b;
.source "SmsVerificationPresenter.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lco/uk/getmondo/signup/phone_verification/l$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lco/uk/getmondo/common/ui/b",
        "<",
        "Lco/uk/getmondo/signup/phone_verification/l$a;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000:\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0003\n\u0002\u0008\u0004\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00020\u0001:\u0001\u0014B3\u0008\u0007\u0012\u0008\u0008\u0001\u0010\u0003\u001a\u00020\u0004\u0012\u0008\u0008\u0001\u0010\u0005\u001a\u00020\u0004\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b\u00a2\u0006\u0002\u0010\u000cJ\u0006\u0010\r\u001a\u00020\u000eJ\u0018\u0010\u000f\u001a\u00020\u000e2\u0006\u0010\u0010\u001a\u00020\u00112\u0006\u0010\u0012\u001a\u00020\u0002H\u0002J\u0010\u0010\u0013\u001a\u00020\u000e2\u0006\u0010\u0012\u001a\u00020\u0002H\u0016R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0015"
    }
    d2 = {
        "Lco/uk/getmondo/signup/phone_verification/SmsVerificationPresenter;",
        "Lco/uk/getmondo/common/ui/BasePresenter;",
        "Lco/uk/getmondo/signup/phone_verification/SmsVerificationPresenter$View;",
        "uiScheduler",
        "Lio/reactivex/Scheduler;",
        "ioScheduler",
        "phoneVerificationManager",
        "Lco/uk/getmondo/signup/phone_verification/PhoneVerificationManager;",
        "apiErrorHandler",
        "Lco/uk/getmondo/common/errors/ApiErrorHandler;",
        "analyticsService",
        "Lco/uk/getmondo/common/AnalyticsService;",
        "(Lio/reactivex/Scheduler;Lio/reactivex/Scheduler;Lco/uk/getmondo/signup/phone_verification/PhoneVerificationManager;Lco/uk/getmondo/common/errors/ApiErrorHandler;Lco/uk/getmondo/common/AnalyticsService;)V",
        "getNewCode",
        "",
        "handleError",
        "throwable",
        "",
        "view",
        "register",
        "View",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field private final c:Lio/reactivex/u;

.field private final d:Lio/reactivex/u;

.field private final e:Lco/uk/getmondo/signup/phone_verification/c;

.field private final f:Lco/uk/getmondo/common/e/a;

.field private final g:Lco/uk/getmondo/common/a;


# direct methods
.method public constructor <init>(Lio/reactivex/u;Lio/reactivex/u;Lco/uk/getmondo/signup/phone_verification/c;Lco/uk/getmondo/common/e/a;Lco/uk/getmondo/common/a;)V
    .locals 1

    .prologue
    const-string v0, "uiScheduler"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "ioScheduler"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "phoneVerificationManager"

    invoke-static {p3, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "apiErrorHandler"

    invoke-static {p4, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "analyticsService"

    invoke-static {p5, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 28
    invoke-direct {p0}, Lco/uk/getmondo/common/ui/b;-><init>()V

    iput-object p1, p0, Lco/uk/getmondo/signup/phone_verification/l;->c:Lio/reactivex/u;

    iput-object p2, p0, Lco/uk/getmondo/signup/phone_verification/l;->d:Lio/reactivex/u;

    iput-object p3, p0, Lco/uk/getmondo/signup/phone_verification/l;->e:Lco/uk/getmondo/signup/phone_verification/c;

    iput-object p4, p0, Lco/uk/getmondo/signup/phone_verification/l;->f:Lco/uk/getmondo/common/e/a;

    iput-object p5, p0, Lco/uk/getmondo/signup/phone_verification/l;->g:Lco/uk/getmondo/common/a;

    return-void
.end method

.method public static final synthetic a(Lco/uk/getmondo/signup/phone_verification/l;)Lco/uk/getmondo/signup/phone_verification/c;
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lco/uk/getmondo/signup/phone_verification/l;->e:Lco/uk/getmondo/signup/phone_verification/c;

    return-object v0
.end method

.method public static final synthetic a(Lco/uk/getmondo/signup/phone_verification/l;Ljava/lang/Throwable;Lco/uk/getmondo/signup/phone_verification/l$a;)V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0, p1, p2}, Lco/uk/getmondo/signup/phone_verification/l;->a(Ljava/lang/Throwable;Lco/uk/getmondo/signup/phone_verification/l$a;)V

    return-void
.end method

.method private final a(Ljava/lang/Throwable;Lco/uk/getmondo/signup/phone_verification/l$a;)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 79
    invoke-interface {p2}, Lco/uk/getmondo/signup/phone_verification/l$a;->g()V

    .line 81
    invoke-static {}, Lco/uk/getmondo/signup/phone_verification/b;->values()[Lco/uk/getmondo/signup/phone_verification/b;

    move-result-object v0

    check-cast v0, [Lco/uk/getmondo/common/e/f;

    invoke-static {p1, v0}, Lco/uk/getmondo/common/e/c;->a(Ljava/lang/Throwable;[Lco/uk/getmondo/common/e/f;)Lco/uk/getmondo/common/e/f;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/signup/phone_verification/b;

    if-nez v0, :cond_3

    .line 99
    :goto_0
    const/4 v0, 0x0

    .line 102
    :goto_1
    if-nez v0, :cond_0

    .line 103
    sget-object v1, Lco/uk/getmondo/signup/i;->a:Lco/uk/getmondo/signup/i;

    move-object v0, p2

    check-cast v0, Lco/uk/getmondo/signup/i$a;

    invoke-virtual {v1, p1, v0}, Lco/uk/getmondo/signup/i;->a(Ljava/lang/Throwable;Lco/uk/getmondo/signup/i$a;)Z

    move-result v0

    .line 106
    :cond_0
    if-nez v0, :cond_1

    .line 107
    iget-object v1, p0, Lco/uk/getmondo/signup/phone_verification/l;->f:Lco/uk/getmondo/common/e/a;

    move-object v0, p2

    check-cast v0, Lco/uk/getmondo/common/e/a$a;

    invoke-virtual {v1, p1, v0}, Lco/uk/getmondo/common/e/a;->a(Ljava/lang/Throwable;Lco/uk/getmondo/common/e/a$a;)Z

    move-result v0

    .line 110
    :cond_1
    if-nez v0, :cond_2

    .line 111
    const v0, 0x7f0a0196

    invoke-interface {p2, v0}, Lco/uk/getmondo/signup/phone_verification/l$a;->b(I)V

    .line 113
    :cond_2
    return-void

    .line 81
    :cond_3
    sget-object v2, Lco/uk/getmondo/signup/phone_verification/m;->a:[I

    invoke-virtual {v0}, Lco/uk/getmondo/signup/phone_verification/b;->ordinal()I

    move-result v0

    aget v0, v2, v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 83
    :pswitch_0
    invoke-interface {p2}, Lco/uk/getmondo/signup/phone_verification/l$a;->j()V

    move v0, v1

    .line 84
    goto :goto_1

    .line 87
    :pswitch_1
    invoke-interface {p2}, Lco/uk/getmondo/signup/phone_verification/l$a;->k()V

    move v0, v1

    .line 88
    goto :goto_1

    .line 91
    :pswitch_2
    invoke-interface {p2}, Lco/uk/getmondo/signup/phone_verification/l$a;->m()V

    move v0, v1

    .line 92
    goto :goto_1

    .line 95
    :pswitch_3
    invoke-interface {p2}, Lco/uk/getmondo/signup/phone_verification/l$a;->n()V

    .line 96
    invoke-interface {p2}, Lco/uk/getmondo/signup/phone_verification/l$a;->o()V

    move v0, v1

    .line 97
    goto :goto_1

    .line 81
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static final synthetic b(Lco/uk/getmondo/signup/phone_verification/l;)Lio/reactivex/u;
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lco/uk/getmondo/signup/phone_verification/l;->d:Lio/reactivex/u;

    return-object v0
.end method

.method public static final synthetic c(Lco/uk/getmondo/signup/phone_verification/l;)Lio/reactivex/u;
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lco/uk/getmondo/signup/phone_verification/l;->c:Lio/reactivex/u;

    return-object v0
.end method

.method public static final synthetic d(Lco/uk/getmondo/signup/phone_verification/l;)Lco/uk/getmondo/signup/phone_verification/l$a;
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lco/uk/getmondo/signup/phone_verification/l;->a:Lco/uk/getmondo/common/ui/f;

    check-cast v0, Lco/uk/getmondo/signup/phone_verification/l$a;

    return-object v0
.end method


# virtual methods
.method public final a()V
    .locals 4

    .prologue
    .line 67
    iget-object v0, p0, Lco/uk/getmondo/signup/phone_verification/l;->a:Lco/uk/getmondo/common/ui/f;

    check-cast v0, Lco/uk/getmondo/signup/phone_verification/l$a;

    invoke-interface {v0}, Lco/uk/getmondo/signup/phone_verification/l$a;->f()V

    .line 69
    iget-object v2, p0, Lco/uk/getmondo/signup/phone_verification/l;->b:Lio/reactivex/b/a;

    .line 72
    iget-object v1, p0, Lco/uk/getmondo/signup/phone_verification/l;->e:Lco/uk/getmondo/signup/phone_verification/c;

    iget-object v0, p0, Lco/uk/getmondo/signup/phone_verification/l;->a:Lco/uk/getmondo/common/ui/f;

    check-cast v0, Lco/uk/getmondo/signup/phone_verification/l$a;

    invoke-interface {v0}, Lco/uk/getmondo/signup/phone_verification/l$a;->h()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lco/uk/getmondo/signup/phone_verification/c;->a(Ljava/lang/String;)Lio/reactivex/b;

    move-result-object v0

    .line 70
    iget-object v1, p0, Lco/uk/getmondo/signup/phone_verification/l;->d:Lio/reactivex/u;

    invoke-virtual {v0, v1}, Lio/reactivex/b;->b(Lio/reactivex/u;)Lio/reactivex/b;

    move-result-object v0

    .line 71
    iget-object v1, p0, Lco/uk/getmondo/signup/phone_verification/l;->c:Lio/reactivex/u;

    invoke-virtual {v0, v1}, Lio/reactivex/b;->a(Lio/reactivex/u;)Lio/reactivex/b;

    move-result-object v3

    .line 73
    new-instance v0, Lco/uk/getmondo/signup/phone_verification/l$b;

    invoke-direct {v0, p0}, Lco/uk/getmondo/signup/phone_verification/l$b;-><init>(Lco/uk/getmondo/signup/phone_verification/l;)V

    check-cast v0, Lio/reactivex/c/a;

    .line 74
    new-instance v1, Lco/uk/getmondo/signup/phone_verification/l$c;

    invoke-direct {v1, p0}, Lco/uk/getmondo/signup/phone_verification/l$c;-><init>(Lco/uk/getmondo/signup/phone_verification/l;)V

    check-cast v1, Lio/reactivex/c/g;

    .line 72
    invoke-virtual {v3, v0, v1}, Lio/reactivex/b;->a(Lio/reactivex/c/a;Lio/reactivex/c/g;)Lio/reactivex/b/b;

    move-result-object v0

    const-string v1, "phoneVerificationManager\u2026view) }\n                )"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 69
    invoke-static {v2, v0}, Lco/uk/getmondo/common/j/f;->a(Lio/reactivex/b/a;Lio/reactivex/b/b;)Lio/reactivex/b/a;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/signup/phone_verification/l;->b:Lio/reactivex/b/a;

    .line 76
    return-void
.end method

.method public bridge synthetic a(Lco/uk/getmondo/common/ui/f;)V
    .locals 0

    .prologue
    .line 21
    check-cast p1, Lco/uk/getmondo/signup/phone_verification/l$a;

    invoke-virtual {p0, p1}, Lco/uk/getmondo/signup/phone_verification/l;->a(Lco/uk/getmondo/signup/phone_verification/l$a;)V

    return-void
.end method

.method public a(Lco/uk/getmondo/signup/phone_verification/l$a;)V
    .locals 6

    .prologue
    const-string v0, "view"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v0, p1

    .line 31
    check-cast v0, Lco/uk/getmondo/common/ui/f;

    invoke-super {p0, v0}, Lco/uk/getmondo/common/ui/b;->a(Lco/uk/getmondo/common/ui/f;)V

    .line 33
    iget-object v0, p0, Lco/uk/getmondo/signup/phone_verification/l;->g:Lco/uk/getmondo/common/a;

    sget-object v1, Lco/uk/getmondo/api/model/tracking/Impression;->Companion:Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    invoke-virtual {v1}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->B()Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v1

    invoke-virtual {v0, v1}, Lco/uk/getmondo/common/a;->a(Lco/uk/getmondo/api/model/tracking/Impression;)V

    .line 35
    invoke-interface {p1}, Lco/uk/getmondo/signup/phone_verification/l$a;->e()Lio/reactivex/n;

    move-result-object v0

    invoke-virtual {v0}, Lio/reactivex/n;->share()Lio/reactivex/n;

    move-result-object v1

    .line 37
    iget-object v2, p0, Lco/uk/getmondo/signup/phone_verification/l;->b:Lio/reactivex/b/a;

    .line 38
    sget-object v0, Lco/uk/getmondo/signup/phone_verification/l$d;->a:Lco/uk/getmondo/signup/phone_verification/l$d;

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {v1, v0}, Lio/reactivex/n;->map(Lio/reactivex/c/h;)Lio/reactivex/n;

    move-result-object v3

    .line 39
    new-instance v0, Lco/uk/getmondo/signup/phone_verification/l$e;

    invoke-direct {v0, p1}, Lco/uk/getmondo/signup/phone_verification/l$e;-><init>(Lco/uk/getmondo/signup/phone_verification/l$a;)V

    check-cast v0, Lio/reactivex/c/g;

    invoke-virtual {v3, v0}, Lio/reactivex/n;->subscribe(Lio/reactivex/c/g;)Lio/reactivex/b/b;

    move-result-object v0

    const-string v3, "codeChanges\n            \u2026ntinueButtonEnabled(it) }"

    invoke-static {v0, v3}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 37
    invoke-static {v2, v0}, Lco/uk/getmondo/common/j/f;->a(Lio/reactivex/b/a;Lio/reactivex/b/b;)Lio/reactivex/b/a;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/signup/phone_verification/l;->b:Lio/reactivex/b/a;

    .line 42
    iget-object v2, p0, Lco/uk/getmondo/signup/phone_verification/l;->b:Lio/reactivex/b/a;

    .line 43
    new-instance v0, Lco/uk/getmondo/signup/phone_verification/l$f;

    invoke-direct {v0, p1}, Lco/uk/getmondo/signup/phone_verification/l$f;-><init>(Lco/uk/getmondo/signup/phone_verification/l$a;)V

    check-cast v0, Lio/reactivex/c/g;

    invoke-virtual {v1, v0}, Lio/reactivex/n;->subscribe(Lio/reactivex/c/g;)Lio/reactivex/b/b;

    move-result-object v0

    const-string v3, "codeChanges\n            \u2026ideIncorrectCodeError() }"

    invoke-static {v0, v3}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 42
    invoke-static {v2, v0}, Lco/uk/getmondo/common/j/f;->a(Lio/reactivex/b/a;Lio/reactivex/b/b;)Lio/reactivex/b/a;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/signup/phone_verification/l;->b:Lio/reactivex/b/a;

    .line 45
    invoke-interface {p1}, Lco/uk/getmondo/signup/phone_verification/l$a;->d()Lio/reactivex/n;

    move-result-object v2

    .line 46
    const-string v0, "codeChanges"

    invoke-static {v1, v0}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v0, v1

    check-cast v0, Lio/reactivex/r;

    invoke-static {v2, v0}, Lco/uk/getmondo/common/j/f;->a(Lio/reactivex/n;Lio/reactivex/r;)Lio/reactivex/n;

    move-result-object v1

    .line 47
    sget-object v0, Lco/uk/getmondo/signup/phone_verification/l$k;->a:Lco/uk/getmondo/signup/phone_verification/l$k;

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {v1, v0}, Lio/reactivex/n;->map(Lio/reactivex/c/h;)Lio/reactivex/n;

    move-result-object v2

    .line 48
    invoke-interface {p1}, Lco/uk/getmondo/signup/phone_verification/l$a;->c()Lio/reactivex/n;

    move-result-object v0

    invoke-virtual {v0}, Lio/reactivex/n;->share()Lio/reactivex/n;

    move-result-object v3

    .line 50
    iget-object v4, p0, Lco/uk/getmondo/signup/phone_verification/l;->b:Lio/reactivex/b/a;

    .line 51
    iget-object v0, p0, Lco/uk/getmondo/signup/phone_verification/l;->c:Lio/reactivex/u;

    invoke-virtual {v3, v0}, Lio/reactivex/n;->observeOn(Lio/reactivex/u;)Lio/reactivex/n;

    move-result-object v5

    .line 52
    new-instance v0, Lco/uk/getmondo/signup/phone_verification/l$g;

    invoke-direct {v0, p1}, Lco/uk/getmondo/signup/phone_verification/l$g;-><init>(Lco/uk/getmondo/signup/phone_verification/l$a;)V

    check-cast v0, Lio/reactivex/c/g;

    sget-object v1, Lco/uk/getmondo/signup/phone_verification/l$h;->a:Lco/uk/getmondo/signup/phone_verification/l$h;

    check-cast v1, Lio/reactivex/c/g;

    invoke-virtual {v5, v0, v1}, Lio/reactivex/n;->subscribe(Lio/reactivex/c/g;Lio/reactivex/c/g;)Lio/reactivex/b/b;

    move-result-object v0

    const-string v1, "smsCodeReceived\n        \u2026(it) }, { Timber.e(it) })"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 50
    invoke-static {v4, v0}, Lco/uk/getmondo/common/j/f;->a(Lio/reactivex/b/a;Lio/reactivex/b/b;)Lio/reactivex/b/a;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/signup/phone_verification/l;->b:Lio/reactivex/b/a;

    .line 54
    iget-object v4, p0, Lco/uk/getmondo/signup/phone_verification/l;->b:Lio/reactivex/b/a;

    move-object v0, v2

    .line 63
    check-cast v0, Lio/reactivex/r;

    move-object v1, v3

    check-cast v1, Lio/reactivex/r;

    invoke-static {v0, v1}, Lio/reactivex/n;->merge(Lio/reactivex/r;Lio/reactivex/r;)Lio/reactivex/n;

    move-result-object v1

    .line 55
    new-instance v0, Lco/uk/getmondo/signup/phone_verification/l$i;

    invoke-direct {v0, p0, p1}, Lco/uk/getmondo/signup/phone_verification/l$i;-><init>(Lco/uk/getmondo/signup/phone_verification/l;Lco/uk/getmondo/signup/phone_verification/l$a;)V

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {v1, v0}, Lio/reactivex/n;->flatMapMaybe(Lio/reactivex/c/h;)Lio/reactivex/n;

    move-result-object v1

    .line 63
    new-instance v0, Lco/uk/getmondo/signup/phone_verification/l$j;

    invoke-direct {v0, p1}, Lco/uk/getmondo/signup/phone_verification/l$j;-><init>(Lco/uk/getmondo/signup/phone_verification/l$a;)V

    check-cast v0, Lio/reactivex/c/g;

    invoke-virtual {v1, v0}, Lio/reactivex/n;->subscribe(Lio/reactivex/c/g;)Lio/reactivex/b/b;

    move-result-object v0

    const-string v1, "Observable.merge(manualS\u2026onPhoneNumberVerified() }"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 54
    invoke-static {v4, v0}, Lco/uk/getmondo/common/j/f;->a(Lio/reactivex/b/a;Lio/reactivex/b/b;)Lio/reactivex/b/a;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/signup/phone_verification/l;->b:Lio/reactivex/b/a;

    .line 64
    return-void
.end method
