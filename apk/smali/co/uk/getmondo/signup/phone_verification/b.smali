.class public final enum Lco/uk/getmondo/signup/phone_verification/b;
.super Ljava/lang/Enum;
.source "PhoneVerificationError.kt"

# interfaces
.implements Lco/uk/getmondo/common/e/f;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lco/uk/getmondo/signup/phone_verification/b;",
        ">;",
        "Lco/uk/getmondo/common/e/f;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\t\u0008\u0086\u0001\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00000\u00012\u00020\u0002B\u000f\u0008\u0002\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u00a2\u0006\u0002\u0010\u0005R\u0014\u0010\u0003\u001a\u00020\u0004X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0006\u0010\u0007j\u0002\u0008\u0008j\u0002\u0008\tj\u0002\u0008\nj\u0002\u0008\u000bj\u0002\u0008\u000c\u00a8\u0006\r"
    }
    d2 = {
        "Lco/uk/getmondo/signup/phone_verification/PhoneVerificationError;",
        "",
        "Lco/uk/getmondo/common/errors/MatchableError;",
        "prefix",
        "",
        "(Ljava/lang/String;ILjava/lang/String;)V",
        "getPrefix",
        "()Ljava/lang/String;",
        "INVALID_PHONE_NUMBER",
        "EXPIRED_CODE",
        "INCORRECT_CODE",
        "TOO_MANY_ATTEMPTS",
        "PHONE_NUMBER_ALREADY_IN_USE",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# static fields
.field public static final enum a:Lco/uk/getmondo/signup/phone_verification/b;

.field public static final enum b:Lco/uk/getmondo/signup/phone_verification/b;

.field public static final enum c:Lco/uk/getmondo/signup/phone_verification/b;

.field public static final enum d:Lco/uk/getmondo/signup/phone_verification/b;

.field public static final enum e:Lco/uk/getmondo/signup/phone_verification/b;

.field private static final synthetic f:[Lco/uk/getmondo/signup/phone_verification/b;


# instance fields
.field private final g:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    const/4 v0, 0x5

    new-array v0, v0, [Lco/uk/getmondo/signup/phone_verification/b;

    new-instance v1, Lco/uk/getmondo/signup/phone_verification/b;

    const-string v2, "INVALID_PHONE_NUMBER"

    .line 6
    const-string v3, "bad_request.bad_param.phone_number"

    invoke-direct {v1, v2, v4, v3}, Lco/uk/getmondo/signup/phone_verification/b;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lco/uk/getmondo/signup/phone_verification/b;->a:Lco/uk/getmondo/signup/phone_verification/b;

    aput-object v1, v0, v4

    new-instance v1, Lco/uk/getmondo/signup/phone_verification/b;

    const-string v2, "EXPIRED_CODE"

    .line 7
    const-string v3, "bad_request.code.expired"

    invoke-direct {v1, v2, v5, v3}, Lco/uk/getmondo/signup/phone_verification/b;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lco/uk/getmondo/signup/phone_verification/b;->b:Lco/uk/getmondo/signup/phone_verification/b;

    aput-object v1, v0, v5

    new-instance v1, Lco/uk/getmondo/signup/phone_verification/b;

    const-string v2, "INCORRECT_CODE"

    .line 8
    const-string v3, "bad_request.code.incorrect"

    invoke-direct {v1, v2, v6, v3}, Lco/uk/getmondo/signup/phone_verification/b;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lco/uk/getmondo/signup/phone_verification/b;->c:Lco/uk/getmondo/signup/phone_verification/b;

    aput-object v1, v0, v6

    new-instance v1, Lco/uk/getmondo/signup/phone_verification/b;

    const-string v2, "TOO_MANY_ATTEMPTS"

    .line 9
    const-string v3, "bad_request.too_many_attempts"

    invoke-direct {v1, v2, v7, v3}, Lco/uk/getmondo/signup/phone_verification/b;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lco/uk/getmondo/signup/phone_verification/b;->d:Lco/uk/getmondo/signup/phone_verification/b;

    aput-object v1, v0, v7

    new-instance v1, Lco/uk/getmondo/signup/phone_verification/b;

    const-string v2, "PHONE_NUMBER_ALREADY_IN_USE"

    .line 10
    const-string v3, "bad_request.phone_number.in_use"

    invoke-direct {v1, v2, v8, v3}, Lco/uk/getmondo/signup/phone_verification/b;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lco/uk/getmondo/signup/phone_verification/b;->e:Lco/uk/getmondo/signup/phone_verification/b;

    aput-object v1, v0, v8

    sput-object v0, Lco/uk/getmondo/signup/phone_verification/b;->f:[Lco/uk/getmondo/signup/phone_verification/b;

    return-void
.end method

.method protected constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    const-string v0, "prefix"

    invoke-static {p3, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 5
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput-object p3, p0, Lco/uk/getmondo/signup/phone_verification/b;->g:Ljava/lang/String;

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lco/uk/getmondo/signup/phone_verification/b;
    .locals 1

    const-class v0, Lco/uk/getmondo/signup/phone_verification/b;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/signup/phone_verification/b;

    return-object v0
.end method

.method public static values()[Lco/uk/getmondo/signup/phone_verification/b;
    .locals 1

    sget-object v0, Lco/uk/getmondo/signup/phone_verification/b;->f:[Lco/uk/getmondo/signup/phone_verification/b;

    invoke-virtual {v0}, [Lco/uk/getmondo/signup/phone_verification/b;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lco/uk/getmondo/signup/phone_verification/b;

    return-object v0
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 5
    iget-object v0, p0, Lco/uk/getmondo/signup/phone_verification/b;->g:Ljava/lang/String;

    return-object v0
.end method
