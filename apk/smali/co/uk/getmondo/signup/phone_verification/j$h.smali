.class final Lco/uk/getmondo/signup/phone_verification/j$h;
.super Ljava/lang/Object;
.source "SmsVerificationFragment.kt"

# interfaces
.implements Lio/reactivex/p;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lco/uk/getmondo/signup/phone_verification/j;->c()Lio/reactivex/n;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/p",
        "<TT;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000\u0014\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u0014\u0010\u0002\u001a\u0010\u0012\u000c\u0012\n \u0005*\u0004\u0018\u00010\u00040\u00040\u0003H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "",
        "emitter",
        "Lio/reactivex/ObservableEmitter;",
        "",
        "kotlin.jvm.PlatformType",
        "subscribe"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field final synthetic a:Lco/uk/getmondo/signup/phone_verification/j;


# direct methods
.method constructor <init>(Lco/uk/getmondo/signup/phone_verification/j;)V
    .locals 0

    iput-object p1, p0, Lco/uk/getmondo/signup/phone_verification/j$h;->a:Lco/uk/getmondo/signup/phone_verification/j;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lio/reactivex/o;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/o",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    const-string v0, "emitter"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 59
    new-instance v2, Lco/uk/getmondo/create_account/phone_number/ag;

    invoke-direct {v2}, Lco/uk/getmondo/create_account/phone_number/ag;-><init>()V

    .line 60
    new-instance v5, Lco/uk/getmondo/signup/phone_verification/j$h$a;

    invoke-direct {v5, p1}, Lco/uk/getmondo/signup/phone_verification/j$h$a;-><init>(Lio/reactivex/o;)V

    check-cast v5, Lco/uk/getmondo/create_account/phone_number/a;

    .line 61
    sget-object v0, Lco/uk/getmondo/create_account/phone_number/ag;->a:Lco/uk/getmondo/create_account/phone_number/ag$a;

    iget-object v1, p0, Lco/uk/getmondo/signup/phone_verification/j$h;->a:Lco/uk/getmondo/signup/phone_verification/j;

    invoke-virtual {v1}, Lco/uk/getmondo/signup/phone_verification/j;->getContext()Landroid/content/Context;

    move-result-object v1

    const-string v3, "context"

    invoke-static {v1, v3}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v3, "\\d{4,8}"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v6, 0x0

    const-string v7, "Monzo"

    aput-object v7, v4, v6

    check-cast v4, [Ljava/lang/Object;

    .line 191
    check-cast v4, [Ljava/lang/String;

    .line 61
    invoke-virtual/range {v0 .. v5}, Lco/uk/getmondo/create_account/phone_number/ag$a;->a(Landroid/content/Context;Lco/uk/getmondo/create_account/phone_number/ag;Ljava/lang/String;[Ljava/lang/String;Lco/uk/getmondo/create_account/phone_number/a;)V

    .line 62
    new-instance v0, Lco/uk/getmondo/signup/phone_verification/j$h$1;

    invoke-direct {v0, p0, v2}, Lco/uk/getmondo/signup/phone_verification/j$h$1;-><init>(Lco/uk/getmondo/signup/phone_verification/j$h;Lco/uk/getmondo/create_account/phone_number/ag;)V

    check-cast v0, Lio/reactivex/c/f;

    invoke-interface {p1, v0}, Lio/reactivex/o;->a(Lio/reactivex/c/f;)V

    .line 63
    return-void
.end method
