.class public final Lco/uk/getmondo/signup/pending/SignupPendingActivity;
.super Lco/uk/getmondo/signup/a;
.source "SignupPendingActivity.kt"

# interfaces
.implements Lco/uk/getmondo/signup/pending/b$a;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lco/uk/getmondo/signup/pending/SignupPendingActivity$a;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000(\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0010\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0004\u0018\u0000 \u00112\u00020\u00012\u00020\u0002:\u0001\u0011B\u0005\u00a2\u0006\u0002\u0010\u0003J\u0008\u0010\n\u001a\u00020\u000bH\u0016J\u0012\u0010\u000c\u001a\u00020\u000b2\u0008\u0010\r\u001a\u0004\u0018\u00010\u000eH\u0014J\u0008\u0010\u000f\u001a\u00020\u000bH\u0014J\u0008\u0010\u0010\u001a\u00020\u000bH\u0014R\u001e\u0010\u0004\u001a\u00020\u00058\u0006@\u0006X\u0087.\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u0006\u0010\u0007\"\u0004\u0008\u0008\u0010\t\u00a8\u0006\u0012"
    }
    d2 = {
        "Lco/uk/getmondo/signup/pending/SignupPendingActivity;",
        "Lco/uk/getmondo/signup/BaseSignupActivity;",
        "Lco/uk/getmondo/signup/pending/SignupPendingPresenter$View;",
        "()V",
        "presenter",
        "Lco/uk/getmondo/signup/pending/SignupPendingPresenter;",
        "getPresenter",
        "()Lco/uk/getmondo/signup/pending/SignupPendingPresenter;",
        "setPresenter",
        "(Lco/uk/getmondo/signup/pending/SignupPendingPresenter;)V",
        "finishStage",
        "",
        "onCreate",
        "savedInstanceState",
        "Landroid/os/Bundle;",
        "onResume",
        "onStop",
        "Companion",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# static fields
.field public static final b:Lco/uk/getmondo/signup/pending/SignupPendingActivity$a;


# instance fields
.field public a:Lco/uk/getmondo/signup/pending/b;

.field private g:Ljava/util/HashMap;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lco/uk/getmondo/signup/pending/SignupPendingActivity$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lco/uk/getmondo/signup/pending/SignupPendingActivity$a;-><init>(Lkotlin/d/b/i;)V

    sput-object v0, Lco/uk/getmondo/signup/pending/SignupPendingActivity;->b:Lco/uk/getmondo/signup/pending/SignupPendingActivity$a;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Lco/uk/getmondo/signup/a;-><init>()V

    return-void
.end method


# virtual methods
.method public a(I)Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lco/uk/getmondo/signup/pending/SignupPendingActivity;->g:Ljava/util/HashMap;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lco/uk/getmondo/signup/pending/SignupPendingActivity;->g:Ljava/util/HashMap;

    :cond_0
    iget-object v0, p0, Lco/uk/getmondo/signup/pending/SignupPendingActivity;->g:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    if-nez v0, :cond_1

    invoke-virtual {p0, p1}, Landroid/support/v4/app/j;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/signup/pending/SignupPendingActivity;->g:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    return-object v0
.end method

.method public c()V
    .locals 1

    .prologue
    .line 41
    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/pending/SignupPendingActivity;->setResult(I)V

    .line 42
    invoke-virtual {p0}, Lco/uk/getmondo/signup/pending/SignupPendingActivity;->finish()V

    .line 43
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 18
    invoke-super {p0, p1}, Lco/uk/getmondo/signup/a;->onCreate(Landroid/os/Bundle;)V

    .line 20
    const v0, 0x7f050060

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/pending/SignupPendingActivity;->setContentView(I)V

    .line 21
    invoke-virtual {p0}, Lco/uk/getmondo/signup/pending/SignupPendingActivity;->l()Lco/uk/getmondo/common/h/b/b;

    move-result-object v0

    invoke-interface {v0, p0}, Lco/uk/getmondo/common/h/b/b;->a(Lco/uk/getmondo/signup/pending/SignupPendingActivity;)V

    .line 23
    invoke-virtual {p0}, Lco/uk/getmondo/signup/pending/SignupPendingActivity;->a()Lco/uk/getmondo/signup/j;

    move-result-object v0

    sget-object v1, Lco/uk/getmondo/signup/j;->a:Lco/uk/getmondo/signup/j;

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 24
    sget v0, Lco/uk/getmondo/c$a;->signupPendingBackButton:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/pending/SignupPendingActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    invoke-static {v0}, Lco/uk/getmondo/common/ae;->a(Landroid/view/View;)V

    .line 25
    sget v0, Lco/uk/getmondo/c$a;->signupPendingBackButton:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/pending/SignupPendingActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    new-instance v1, Lco/uk/getmondo/signup/pending/SignupPendingActivity$b;

    invoke-direct {v1, p0}, Lco/uk/getmondo/signup/pending/SignupPendingActivity$b;-><init>(Lco/uk/getmondo/signup/pending/SignupPendingActivity;)V

    check-cast v1, Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 27
    :cond_0
    return-void
.end method

.method protected onResume()V
    .locals 2

    .prologue
    .line 30
    invoke-super {p0}, Lco/uk/getmondo/signup/a;->onResume()V

    .line 32
    iget-object v0, p0, Lco/uk/getmondo/signup/pending/SignupPendingActivity;->a:Lco/uk/getmondo/signup/pending/b;

    if-nez v0, :cond_0

    const-string v1, "presenter"

    invoke-static {v1}, Lkotlin/d/b/l;->b(Ljava/lang/String;)V

    :cond_0
    check-cast p0, Lco/uk/getmondo/signup/pending/b$a;

    invoke-virtual {v0, p0}, Lco/uk/getmondo/signup/pending/b;->a(Lco/uk/getmondo/signup/pending/b$a;)V

    .line 33
    return-void
.end method

.method protected onStop()V
    .locals 2

    .prologue
    .line 36
    iget-object v0, p0, Lco/uk/getmondo/signup/pending/SignupPendingActivity;->a:Lco/uk/getmondo/signup/pending/b;

    if-nez v0, :cond_0

    const-string v1, "presenter"

    invoke-static {v1}, Lkotlin/d/b/l;->b(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0}, Lco/uk/getmondo/signup/pending/b;->b()V

    .line 37
    invoke-super {p0}, Lco/uk/getmondo/signup/a;->onStop()V

    .line 38
    return-void
.end method
