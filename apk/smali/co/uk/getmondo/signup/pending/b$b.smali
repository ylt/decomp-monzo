.class final Lco/uk/getmondo/signup/pending/b$b;
.super Ljava/lang/Object;
.source "SignupPendingPresenter.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lco/uk/getmondo/signup/pending/b;->a(Lco/uk/getmondo/signup/pending/b$a;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h",
        "<TT;",
        "Lio/reactivex/l",
        "<+TR;>;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000\u0016\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\t\n\u0002\u0008\u0002\u0010\u0000\u001a\u0010\u0012\u000c\u0012\n \u0003*\u0004\u0018\u00010\u00020\u00020\u00012\u0006\u0010\u0004\u001a\u00020\u0005H\n\u00a2\u0006\u0004\u0008\u0006\u0010\u0007"
    }
    d2 = {
        "<anonymous>",
        "Lio/reactivex/Maybe;",
        "Lco/uk/getmondo/api/model/signup/SignupInfo;",
        "kotlin.jvm.PlatformType",
        "it",
        "",
        "apply",
        "(Ljava/lang/Long;)Lio/reactivex/Maybe;"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field final synthetic a:Lco/uk/getmondo/signup/pending/b;

.field final synthetic b:Lco/uk/getmondo/signup/pending/b$a;


# direct methods
.method constructor <init>(Lco/uk/getmondo/signup/pending/b;Lco/uk/getmondo/signup/pending/b$a;)V
    .locals 0

    iput-object p1, p0, Lco/uk/getmondo/signup/pending/b$b;->a:Lco/uk/getmondo/signup/pending/b;

    iput-object p2, p0, Lco/uk/getmondo/signup/pending/b$b;->b:Lco/uk/getmondo/signup/pending/b$a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Long;)Lio/reactivex/h;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Long;",
            ")",
            "Lio/reactivex/h",
            "<",
            "Lco/uk/getmondo/api/model/signup/SignupInfo;",
            ">;"
        }
    .end annotation

    .prologue
    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 40
    iget-object v0, p0, Lco/uk/getmondo/signup/pending/b$b;->a:Lco/uk/getmondo/signup/pending/b;

    invoke-static {v0}, Lco/uk/getmondo/signup/pending/b;->a(Lco/uk/getmondo/signup/pending/b;)Lco/uk/getmondo/signup/status/b;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-static {v0, v1, v2, v3}, Lco/uk/getmondo/signup/status/b;->a(Lco/uk/getmondo/signup/status/b;ZILjava/lang/Object;)Lio/reactivex/v;

    move-result-object v0

    .line 41
    iget-object v1, p0, Lco/uk/getmondo/signup/pending/b$b;->a:Lco/uk/getmondo/signup/pending/b;

    invoke-static {v1}, Lco/uk/getmondo/signup/pending/b;->b(Lco/uk/getmondo/signup/pending/b;)Lio/reactivex/u;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/v;->b(Lio/reactivex/u;)Lio/reactivex/v;

    move-result-object v0

    .line 42
    iget-object v1, p0, Lco/uk/getmondo/signup/pending/b$b;->a:Lco/uk/getmondo/signup/pending/b;

    invoke-static {v1}, Lco/uk/getmondo/signup/pending/b;->c(Lco/uk/getmondo/signup/pending/b;)Lio/reactivex/u;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/v;->a(Lio/reactivex/u;)Lio/reactivex/v;

    move-result-object v1

    .line 43
    new-instance v0, Lco/uk/getmondo/signup/pending/b$b$1;

    invoke-direct {v0, p0}, Lco/uk/getmondo/signup/pending/b$b$1;-><init>(Lco/uk/getmondo/signup/pending/b$b;)V

    check-cast v0, Lio/reactivex/c/g;

    invoke-virtual {v1, v0}, Lio/reactivex/v;->d(Lio/reactivex/c/g;)Lio/reactivex/v;

    move-result-object v0

    .line 44
    invoke-static {v0}, Lco/uk/getmondo/common/j/f;->a(Lio/reactivex/v;)Lio/reactivex/h;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 23
    check-cast p1, Ljava/lang/Long;

    invoke-virtual {p0, p1}, Lco/uk/getmondo/signup/pending/b$b;->a(Ljava/lang/Long;)Lio/reactivex/h;

    move-result-object v0

    return-object v0
.end method
