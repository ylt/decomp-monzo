.class public final Lco/uk/getmondo/signup/pending/b;
.super Lco/uk/getmondo/common/ui/b;
.source "SignupPendingPresenter.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lco/uk/getmondo/signup/pending/b$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lco/uk/getmondo/common/ui/b",
        "<",
        "Lco/uk/getmondo/signup/pending/b$a;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u00002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u0003\u0008\u0007\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00020\u0001:\u0001\u0010B3\u0008\u0007\u0012\u0008\u0008\u0001\u0010\u0003\u001a\u00020\u0004\u0012\u0008\u0008\u0001\u0010\u0005\u001a\u00020\u0004\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b\u00a2\u0006\u0002\u0010\u000cJ\u0010\u0010\r\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u0002H\u0016R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0011"
    }
    d2 = {
        "Lco/uk/getmondo/signup/pending/SignupPendingPresenter;",
        "Lco/uk/getmondo/common/ui/BasePresenter;",
        "Lco/uk/getmondo/signup/pending/SignupPendingPresenter$View;",
        "uiScheduler",
        "Lio/reactivex/Scheduler;",
        "ioScheduler",
        "signupStatusManager",
        "Lco/uk/getmondo/signup/status/SignupStatusManager;",
        "apiErrorHandler",
        "Lco/uk/getmondo/common/errors/ApiErrorHandler;",
        "analyticsService",
        "Lco/uk/getmondo/common/AnalyticsService;",
        "(Lio/reactivex/Scheduler;Lio/reactivex/Scheduler;Lco/uk/getmondo/signup/status/SignupStatusManager;Lco/uk/getmondo/common/errors/ApiErrorHandler;Lco/uk/getmondo/common/AnalyticsService;)V",
        "register",
        "",
        "view",
        "View",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field private final c:Lio/reactivex/u;

.field private final d:Lio/reactivex/u;

.field private final e:Lco/uk/getmondo/signup/status/b;

.field private final f:Lco/uk/getmondo/common/e/a;

.field private final g:Lco/uk/getmondo/common/a;


# direct methods
.method public constructor <init>(Lio/reactivex/u;Lio/reactivex/u;Lco/uk/getmondo/signup/status/b;Lco/uk/getmondo/common/e/a;Lco/uk/getmondo/common/a;)V
    .locals 1

    .prologue
    const-string v0, "uiScheduler"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "ioScheduler"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "signupStatusManager"

    invoke-static {p3, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "apiErrorHandler"

    invoke-static {p4, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "analyticsService"

    invoke-static {p5, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 30
    invoke-direct {p0}, Lco/uk/getmondo/common/ui/b;-><init>()V

    iput-object p1, p0, Lco/uk/getmondo/signup/pending/b;->c:Lio/reactivex/u;

    iput-object p2, p0, Lco/uk/getmondo/signup/pending/b;->d:Lio/reactivex/u;

    iput-object p3, p0, Lco/uk/getmondo/signup/pending/b;->e:Lco/uk/getmondo/signup/status/b;

    iput-object p4, p0, Lco/uk/getmondo/signup/pending/b;->f:Lco/uk/getmondo/common/e/a;

    iput-object p5, p0, Lco/uk/getmondo/signup/pending/b;->g:Lco/uk/getmondo/common/a;

    return-void
.end method

.method public static final synthetic a(Lco/uk/getmondo/signup/pending/b;)Lco/uk/getmondo/signup/status/b;
    .locals 1

    .prologue
    .line 23
    iget-object v0, p0, Lco/uk/getmondo/signup/pending/b;->e:Lco/uk/getmondo/signup/status/b;

    return-object v0
.end method

.method public static final synthetic b(Lco/uk/getmondo/signup/pending/b;)Lio/reactivex/u;
    .locals 1

    .prologue
    .line 23
    iget-object v0, p0, Lco/uk/getmondo/signup/pending/b;->d:Lio/reactivex/u;

    return-object v0
.end method

.method public static final synthetic c(Lco/uk/getmondo/signup/pending/b;)Lio/reactivex/u;
    .locals 1

    .prologue
    .line 23
    iget-object v0, p0, Lco/uk/getmondo/signup/pending/b;->c:Lio/reactivex/u;

    return-object v0
.end method

.method public static final synthetic d(Lco/uk/getmondo/signup/pending/b;)Lco/uk/getmondo/common/e/a;
    .locals 1

    .prologue
    .line 23
    iget-object v0, p0, Lco/uk/getmondo/signup/pending/b;->f:Lco/uk/getmondo/common/e/a;

    return-object v0
.end method


# virtual methods
.method public bridge synthetic a(Lco/uk/getmondo/common/ui/f;)V
    .locals 0

    .prologue
    .line 23
    check-cast p1, Lco/uk/getmondo/signup/pending/b$a;

    invoke-virtual {p0, p1}, Lco/uk/getmondo/signup/pending/b;->a(Lco/uk/getmondo/signup/pending/b$a;)V

    return-void
.end method

.method public a(Lco/uk/getmondo/signup/pending/b$a;)V
    .locals 6

    .prologue
    const-string v0, "view"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v0, p1

    .line 33
    check-cast v0, Lco/uk/getmondo/common/ui/f;

    invoke-super {p0, v0}, Lco/uk/getmondo/common/ui/b;->a(Lco/uk/getmondo/common/ui/f;)V

    .line 35
    iget-object v0, p0, Lco/uk/getmondo/signup/pending/b;->g:Lco/uk/getmondo/common/a;

    sget-object v1, Lco/uk/getmondo/api/model/tracking/Impression;->Companion:Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    invoke-virtual {v1}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->aV()Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v1

    invoke-virtual {v0, v1}, Lco/uk/getmondo/common/a;->a(Lco/uk/getmondo/api/model/tracking/Impression;)V

    .line 38
    iget-object v1, p0, Lco/uk/getmondo/signup/pending/b;->b:Lio/reactivex/b/a;

    .line 46
    const-wide/16 v2, 0x2

    const-wide/16 v4, 0xf

    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-static {v2, v3, v4, v5, v0}, Lio/reactivex/n;->interval(JJLjava/util/concurrent/TimeUnit;)Lio/reactivex/n;

    move-result-object v2

    .line 39
    new-instance v0, Lco/uk/getmondo/signup/pending/b$b;

    invoke-direct {v0, p0, p1}, Lco/uk/getmondo/signup/pending/b$b;-><init>(Lco/uk/getmondo/signup/pending/b;Lco/uk/getmondo/signup/pending/b$a;)V

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {v2, v0}, Lio/reactivex/n;->flatMapMaybe(Lio/reactivex/c/h;)Lio/reactivex/n;

    move-result-object v2

    .line 46
    new-instance v0, Lco/uk/getmondo/signup/pending/b$c;

    invoke-direct {v0, p1}, Lco/uk/getmondo/signup/pending/b$c;-><init>(Lco/uk/getmondo/signup/pending/b$a;)V

    check-cast v0, Lio/reactivex/c/g;

    invoke-virtual {v2, v0}, Lio/reactivex/n;->subscribe(Lio/reactivex/c/g;)Lio/reactivex/b/b;

    move-result-object v0

    const-string v2, "Observable.interval(2, 1\u2026      }\n                }"

    invoke-static {v0, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 38
    invoke-static {v1, v0}, Lco/uk/getmondo/common/j/f;->a(Lio/reactivex/b/a;Lio/reactivex/b/b;)Lio/reactivex/b/a;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/signup/pending/b;->b:Lio/reactivex/b/a;

    .line 51
    return-void
.end method
