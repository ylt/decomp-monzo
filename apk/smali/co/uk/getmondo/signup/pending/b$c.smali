.class final Lco/uk/getmondo/signup/pending/b$c;
.super Ljava/lang/Object;
.source "SignupPendingPresenter.kt"

# interfaces
.implements Lio/reactivex/c/g;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lco/uk/getmondo/signup/pending/b;->a(Lco/uk/getmondo/signup/pending/b$a;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/g",
        "<",
        "Lco/uk/getmondo/api/model/signup/SignupInfo;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "",
        "it",
        "Lco/uk/getmondo/api/model/signup/SignupInfo;",
        "kotlin.jvm.PlatformType",
        "accept"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field final synthetic a:Lco/uk/getmondo/signup/pending/b$a;


# direct methods
.method constructor <init>(Lco/uk/getmondo/signup/pending/b$a;)V
    .locals 0

    iput-object p1, p0, Lco/uk/getmondo/signup/pending/b$c;->a:Lco/uk/getmondo/signup/pending/b$a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lco/uk/getmondo/api/model/signup/SignupInfo;)V
    .locals 2

    .prologue
    .line 47
    invoke-virtual {p1}, Lco/uk/getmondo/api/model/signup/SignupInfo;->a()Lco/uk/getmondo/api/model/signup/SignupInfo$Status;

    move-result-object v0

    sget-object v1, Lco/uk/getmondo/api/model/signup/SignupInfo$Status;->IN_PROGRESS:Lco/uk/getmondo/api/model/signup/SignupInfo$Status;

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lco/uk/getmondo/api/model/signup/SignupInfo;->a()Lco/uk/getmondo/api/model/signup/SignupInfo$Status;

    move-result-object v0

    sget-object v1, Lco/uk/getmondo/api/model/signup/SignupInfo$Status;->APPROVED:Lco/uk/getmondo/api/model/signup/SignupInfo$Status;

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-nez v0, :cond_1

    :cond_0
    invoke-virtual {p1}, Lco/uk/getmondo/api/model/signup/SignupInfo;->b()Lco/uk/getmondo/api/model/signup/SignupInfo$Stage;

    move-result-object v0

    sget-object v1, Lco/uk/getmondo/api/model/signup/SignupInfo$Stage;->PENDING:Lco/uk/getmondo/api/model/signup/SignupInfo$Stage;

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_2

    .line 48
    :cond_1
    iget-object v0, p0, Lco/uk/getmondo/signup/pending/b$c;->a:Lco/uk/getmondo/signup/pending/b$a;

    invoke-interface {v0}, Lco/uk/getmondo/signup/pending/b$a;->c()V

    .line 50
    :cond_2
    return-void
.end method

.method public bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 23
    check-cast p1, Lco/uk/getmondo/api/model/signup/SignupInfo;

    invoke-virtual {p0, p1}, Lco/uk/getmondo/signup/pending/b$c;->a(Lco/uk/getmondo/api/model/signup/SignupInfo;)V

    return-void
.end method
