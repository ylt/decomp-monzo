.class final Lco/uk/getmondo/signup/f$c;
.super Ljava/lang/Object;
.source "EmailPresenter.kt"

# interfaces
.implements Lio/reactivex/c/g;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lco/uk/getmondo/signup/f;->a(Lco/uk/getmondo/signup/f$a;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/g",
        "<",
        "Lco/uk/getmondo/d/ak;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "",
        "user",
        "Lco/uk/getmondo/model/User;",
        "kotlin.jvm.PlatformType",
        "accept"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field final synthetic a:Lco/uk/getmondo/signup/f;

.field final synthetic b:Lco/uk/getmondo/signup/f$a;


# direct methods
.method constructor <init>(Lco/uk/getmondo/signup/f;Lco/uk/getmondo/signup/f$a;)V
    .locals 0

    iput-object p1, p0, Lco/uk/getmondo/signup/f$c;->a:Lco/uk/getmondo/signup/f;

    iput-object p2, p0, Lco/uk/getmondo/signup/f$c;->b:Lco/uk/getmondo/signup/f$a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lco/uk/getmondo/d/ak;)V
    .locals 2

    .prologue
    .line 72
    invoke-virtual {p1}, Lco/uk/getmondo/d/ak;->a()Lco/uk/getmondo/d/ak$a;

    move-result-object v0

    sget-object v1, Lco/uk/getmondo/d/ak$a;->NO_PROFILE:Lco/uk/getmondo/d/ak$a;

    if-eq v0, v1, :cond_0

    .line 73
    iget-object v0, p0, Lco/uk/getmondo/signup/f$c;->a:Lco/uk/getmondo/signup/f;

    invoke-static {v0}, Lco/uk/getmondo/signup/f;->e(Lco/uk/getmondo/signup/f;)Lco/uk/getmondo/fcm/e;

    move-result-object v0

    invoke-virtual {v0}, Lco/uk/getmondo/fcm/e;->a()V

    .line 76
    :cond_0
    sget-object v0, Lco/uk/getmondo/a;->c:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p1}, Lco/uk/getmondo/d/ak;->c()Lco/uk/getmondo/d/a;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-interface {v0}, Lco/uk/getmondo/d/a;->f()Z

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 77
    iget-object v0, p0, Lco/uk/getmondo/signup/f$c;->b:Lco/uk/getmondo/signup/f$a;

    invoke-interface {v0}, Lco/uk/getmondo/signup/f$a;->c()V

    .line 80
    :goto_0
    return-void

    .line 79
    :cond_1
    iget-object v0, p0, Lco/uk/getmondo/signup/f$c;->b:Lco/uk/getmondo/signup/f$a;

    invoke-interface {v0}, Lco/uk/getmondo/signup/f$a;->a()V

    goto :goto_0
.end method

.method public bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 30
    check-cast p1, Lco/uk/getmondo/d/ak;

    invoke-virtual {p0, p1}, Lco/uk/getmondo/signup/f$c;->a(Lco/uk/getmondo/d/ak;)V

    return-void
.end method
