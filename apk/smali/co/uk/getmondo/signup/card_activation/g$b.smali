.class final Lco/uk/getmondo/signup/card_activation/g$b;
.super Ljava/lang/Object;
.source "CardOnItsWayPresenter.kt"

# interfaces
.implements Lio/reactivex/c/g;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lco/uk/getmondo/signup/card_activation/g;->a(Lco/uk/getmondo/signup/card_activation/g$a;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/g",
        "<",
        "Lkotlin/n;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000\n\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0004\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0003*\u0004\u0018\u00010\u00010\u0001H\n\u00a2\u0006\u0004\u0008\u0004\u0010\u0005"
    }
    d2 = {
        "<anonymous>",
        "",
        "it",
        "kotlin.jvm.PlatformType",
        "accept",
        "(Lkotlin/Unit;)V"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field final synthetic a:Lco/uk/getmondo/signup/card_activation/g;

.field final synthetic b:Lco/uk/getmondo/signup/card_activation/g$a;


# direct methods
.method constructor <init>(Lco/uk/getmondo/signup/card_activation/g;Lco/uk/getmondo/signup/card_activation/g$a;)V
    .locals 0

    iput-object p1, p0, Lco/uk/getmondo/signup/card_activation/g$b;->a:Lco/uk/getmondo/signup/card_activation/g;

    iput-object p2, p0, Lco/uk/getmondo/signup/card_activation/g$b;->b:Lco/uk/getmondo/signup/card_activation/g$a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 15
    check-cast p1, Lkotlin/n;

    invoke-virtual {p0, p1}, Lco/uk/getmondo/signup/card_activation/g$b;->a(Lkotlin/n;)V

    return-void
.end method

.method public final a(Lkotlin/n;)V
    .locals 2

    .prologue
    .line 29
    iget-object v0, p0, Lco/uk/getmondo/signup/card_activation/g$b;->a:Lco/uk/getmondo/signup/card_activation/g;

    invoke-static {v0}, Lco/uk/getmondo/signup/card_activation/g;->a(Lco/uk/getmondo/signup/card_activation/g;)Lco/uk/getmondo/settings/k;

    move-result-object v0

    invoke-virtual {v0}, Lco/uk/getmondo/settings/k;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 30
    iget-object v0, p0, Lco/uk/getmondo/signup/card_activation/g$b;->b:Lco/uk/getmondo/signup/card_activation/g$a;

    invoke-interface {v0}, Lco/uk/getmondo/signup/card_activation/g$a;->f()V

    .line 36
    :goto_0
    return-void

    .line 32
    :cond_0
    iget-object v0, p0, Lco/uk/getmondo/signup/card_activation/g$b;->b:Lco/uk/getmondo/signup/card_activation/g$a;

    invoke-interface {v0}, Lco/uk/getmondo/signup/card_activation/g$a;->c()Lco/uk/getmondo/signup/j;

    move-result-object v0

    sget-object v1, Lco/uk/getmondo/signup/j;->a:Lco/uk/getmondo/signup/j;

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 33
    iget-object v0, p0, Lco/uk/getmondo/signup/card_activation/g$b;->b:Lco/uk/getmondo/signup/card_activation/g$a;

    invoke-interface {v0}, Lco/uk/getmondo/signup/card_activation/g$a;->g()V

    .line 35
    :cond_1
    iget-object v0, p0, Lco/uk/getmondo/signup/card_activation/g$b;->a:Lco/uk/getmondo/signup/card_activation/g;

    invoke-static {v0}, Lco/uk/getmondo/signup/card_activation/g;->a(Lco/uk/getmondo/signup/card_activation/g;)Lco/uk/getmondo/settings/k;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lco/uk/getmondo/settings/k;->c(Z)V

    goto :goto_0
.end method
