.class public final Lco/uk/getmondo/signup/card_activation/a;
.super Ljava/lang/Object;
.source "ActivateCardActivity_MembersInjector.java"

# interfaces
.implements Lb/a;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lb/a",
        "<",
        "Lco/uk/getmondo/signup/card_activation/ActivateCardActivity;",
        ">;"
    }
.end annotation


# static fields
.field static final synthetic a:Z


# instance fields
.field private final b:Ljavax/a/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/signup/card_activation/b;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Ljavax/a/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/common/q;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 8
    const-class v0, Lco/uk/getmondo/signup/card_activation/a;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lco/uk/getmondo/signup/card_activation/a;->a:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Ljavax/a/a;Ljavax/a/a;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/signup/card_activation/b;",
            ">;",
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/common/q;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    sget-boolean v0, Lco/uk/getmondo/signup/card_activation/a;->a:Z

    if-nez v0, :cond_0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 22
    :cond_0
    iput-object p1, p0, Lco/uk/getmondo/signup/card_activation/a;->b:Ljavax/a/a;

    .line 23
    sget-boolean v0, Lco/uk/getmondo/signup/card_activation/a;->a:Z

    if-nez v0, :cond_1

    if-nez p2, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 24
    :cond_1
    iput-object p2, p0, Lco/uk/getmondo/signup/card_activation/a;->c:Ljavax/a/a;

    .line 25
    return-void
.end method

.method public static a(Ljavax/a/a;Ljavax/a/a;)Lb/a;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/signup/card_activation/b;",
            ">;",
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/common/q;",
            ">;)",
            "Lb/a",
            "<",
            "Lco/uk/getmondo/signup/card_activation/ActivateCardActivity;",
            ">;"
        }
    .end annotation

    .prologue
    .line 30
    new-instance v0, Lco/uk/getmondo/signup/card_activation/a;

    invoke-direct {v0, p0, p1}, Lco/uk/getmondo/signup/card_activation/a;-><init>(Ljavax/a/a;Ljavax/a/a;)V

    return-object v0
.end method


# virtual methods
.method public a(Lco/uk/getmondo/signup/card_activation/ActivateCardActivity;)V
    .locals 2

    .prologue
    .line 35
    if-nez p1, :cond_0

    .line 36
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Cannot inject members into a null reference"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 38
    :cond_0
    iget-object v0, p0, Lco/uk/getmondo/signup/card_activation/a;->b:Ljavax/a/a;

    invoke-interface {v0}, Ljavax/a/a;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/signup/card_activation/b;

    iput-object v0, p1, Lco/uk/getmondo/signup/card_activation/ActivateCardActivity;->b:Lco/uk/getmondo/signup/card_activation/b;

    .line 39
    iget-object v0, p0, Lco/uk/getmondo/signup/card_activation/a;->c:Ljavax/a/a;

    invoke-interface {v0}, Ljavax/a/a;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/common/q;

    iput-object v0, p1, Lco/uk/getmondo/signup/card_activation/ActivateCardActivity;->c:Lco/uk/getmondo/common/q;

    .line 40
    return-void
.end method

.method public bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 8
    check-cast p1, Lco/uk/getmondo/signup/card_activation/ActivateCardActivity;

    invoke-virtual {p0, p1}, Lco/uk/getmondo/signup/card_activation/a;->a(Lco/uk/getmondo/signup/card_activation/ActivateCardActivity;)V

    return-void
.end method
