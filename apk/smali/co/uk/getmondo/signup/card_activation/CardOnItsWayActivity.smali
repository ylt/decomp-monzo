.class public final Lco/uk/getmondo/signup/card_activation/CardOnItsWayActivity;
.super Lco/uk/getmondo/signup/a;
.source "CardOnItsWayActivity.kt"

# interfaces
.implements Lco/uk/getmondo/signup/card_activation/g$a;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lco/uk/getmondo/signup/card_activation/CardOnItsWayActivity$a;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000L\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u0008\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0008\u0018\u0000 )2\u00020\u00012\u00020\u0002:\u0001)B\u0005\u00a2\u0006\u0002\u0010\u0003J\"\u0010\u001a\u001a\u00020\u000c2\u0006\u0010\u001b\u001a\u00020\u001c2\u0006\u0010\u001d\u001a\u00020\u001c2\u0008\u0010\u001e\u001a\u0004\u0018\u00010\u001fH\u0014J\u0012\u0010 \u001a\u00020\u000c2\u0008\u0010!\u001a\u0004\u0018\u00010\"H\u0014J\u0008\u0010#\u001a\u00020\u000cH\u0014J\u0008\u0010$\u001a\u00020\u000cH\u0014J\u0008\u0010%\u001a\u00020\u000cH\u0016J\u0008\u0010&\u001a\u00020\u000cH\u0016J\u0008\u0010\'\u001a\u00020\u000cH\u0016J\u0008\u0010(\u001a\u00020\u000cH\u0016R\u001b\u0010\u0004\u001a\u00020\u00058VX\u0096\u0084\u0002\u00a2\u0006\u000c\n\u0004\u0008\u0008\u0010\t\u001a\u0004\u0008\u0006\u0010\u0007R\u001a\u0010\n\u001a\u0008\u0012\u0004\u0012\u00020\u000c0\u000b8VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\r\u0010\u000eR\u001a\u0010\u000f\u001a\u0008\u0012\u0004\u0012\u00020\u000c0\u000b8VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0010\u0010\u000eR\u001e\u0010\u0011\u001a\u00020\u00128\u0006@\u0006X\u0087.\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u0013\u0010\u0014\"\u0004\u0008\u0015\u0010\u0016R2\u0010\u0017\u001a&\u0012\u000c\u0012\n \u0019*\u0004\u0018\u00010\u000c0\u000c \u0019*\u0012\u0012\u000c\u0012\n \u0019*\u0004\u0018\u00010\u000c0\u000c\u0018\u00010\u00180\u0018X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006*"
    }
    d2 = {
        "Lco/uk/getmondo/signup/card_activation/CardOnItsWayActivity;",
        "Lco/uk/getmondo/signup/BaseSignupActivity;",
        "Lco/uk/getmondo/signup/card_activation/CardOnItsWayPresenter$View;",
        "()V",
        "entryPoint",
        "Lco/uk/getmondo/signup/SignupEntryPoint;",
        "getEntryPoint",
        "()Lco/uk/getmondo/signup/SignupEntryPoint;",
        "entryPoint$delegate",
        "Lkotlin/Lazy;",
        "onMyCardArrivedClicked",
        "Lio/reactivex/Observable;",
        "",
        "getOnMyCardArrivedClicked",
        "()Lio/reactivex/Observable;",
        "onRefresh",
        "getOnRefresh",
        "presenter",
        "Lco/uk/getmondo/signup/card_activation/CardOnItsWayPresenter;",
        "getPresenter",
        "()Lco/uk/getmondo/signup/card_activation/CardOnItsWayPresenter;",
        "setPresenter",
        "(Lco/uk/getmondo/signup/card_activation/CardOnItsWayPresenter;)V",
        "resumeRelay",
        "Lcom/jakewharton/rxrelay2/PublishRelay;",
        "kotlin.jvm.PlatformType",
        "onActivityResult",
        "requestCode",
        "",
        "resultCode",
        "data",
        "Landroid/content/Intent;",
        "onCreate",
        "savedInstanceState",
        "Landroid/os/Bundle;",
        "onDestroy",
        "onResume",
        "openCardActivation",
        "openHome",
        "showBackToMonzoFeed",
        "showMyCardArrived",
        "Companion",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# static fields
.field static final synthetic a:[Lkotlin/reflect/l;

.field public static final g:Lco/uk/getmondo/signup/card_activation/CardOnItsWayActivity$a;


# instance fields
.field public b:Lco/uk/getmondo/signup/card_activation/g;

.field private final h:Lcom/b/b/c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/b/b/c",
            "<",
            "Lkotlin/n;",
            ">;"
        }
    .end annotation
.end field

.field private final i:Lkotlin/c;

.field private j:Ljava/util/HashMap;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const/4 v0, 0x1

    new-array v1, v0, [Lkotlin/reflect/l;

    const/4 v2, 0x0

    new-instance v0, Lkotlin/d/b/w;

    const-class v3, Lco/uk/getmondo/signup/card_activation/CardOnItsWayActivity;

    invoke-static {v3}, Lkotlin/d/b/y;->a(Ljava/lang/Class;)Lkotlin/reflect/c;

    move-result-object v3

    const-string v4, "entryPoint"

    const-string v5, "getEntryPoint()Lco/uk/getmondo/signup/SignupEntryPoint;"

    invoke-direct {v0, v3, v4, v5}, Lkotlin/d/b/w;-><init>(Lkotlin/reflect/e;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v0}, Lkotlin/d/b/y;->a(Lkotlin/d/b/v;)Lkotlin/reflect/n;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/l;

    aput-object v0, v1, v2

    sput-object v1, Lco/uk/getmondo/signup/card_activation/CardOnItsWayActivity;->a:[Lkotlin/reflect/l;

    new-instance v0, Lco/uk/getmondo/signup/card_activation/CardOnItsWayActivity$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lco/uk/getmondo/signup/card_activation/CardOnItsWayActivity$a;-><init>(Lkotlin/d/b/i;)V

    sput-object v0, Lco/uk/getmondo/signup/card_activation/CardOnItsWayActivity;->g:Lco/uk/getmondo/signup/card_activation/CardOnItsWayActivity$a;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 18
    invoke-direct {p0}, Lco/uk/getmondo/signup/a;-><init>()V

    .line 19
    invoke-static {}, Lcom/b/b/c;->a()Lcom/b/b/c;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/signup/card_activation/CardOnItsWayActivity;->h:Lcom/b/b/c;

    .line 20
    new-instance v0, Lco/uk/getmondo/signup/card_activation/CardOnItsWayActivity$b;

    invoke-direct {v0, p0}, Lco/uk/getmondo/signup/card_activation/CardOnItsWayActivity$b;-><init>(Lco/uk/getmondo/signup/card_activation/CardOnItsWayActivity;)V

    check-cast v0, Lkotlin/d/a/a;

    invoke-static {v0}, Lkotlin/d;->a(Lkotlin/d/a/a;)Lkotlin/c;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/signup/card_activation/CardOnItsWayActivity;->i:Lkotlin/c;

    return-void
.end method

.method public static final synthetic a(Lco/uk/getmondo/signup/card_activation/CardOnItsWayActivity;)Lco/uk/getmondo/signup/j;
    .locals 1

    .prologue
    .line 18
    invoke-virtual {p0}, Lco/uk/getmondo/signup/card_activation/CardOnItsWayActivity;->a()Lco/uk/getmondo/signup/j;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public a(I)Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lco/uk/getmondo/signup/card_activation/CardOnItsWayActivity;->j:Ljava/util/HashMap;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lco/uk/getmondo/signup/card_activation/CardOnItsWayActivity;->j:Ljava/util/HashMap;

    :cond_0
    iget-object v0, p0, Lco/uk/getmondo/signup/card_activation/CardOnItsWayActivity;->j:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    if-nez v0, :cond_1

    invoke-virtual {p0, p1}, Landroid/support/v4/app/j;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/signup/card_activation/CardOnItsWayActivity;->j:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    return-object v0
.end method

.method public c()Lco/uk/getmondo/signup/j;
    .locals 3

    iget-object v0, p0, Lco/uk/getmondo/signup/card_activation/CardOnItsWayActivity;->i:Lkotlin/c;

    sget-object v1, Lco/uk/getmondo/signup/card_activation/CardOnItsWayActivity;->a:[Lkotlin/reflect/l;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-interface {v0}, Lkotlin/c;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/signup/j;

    return-object v0
.end method

.method public d()Lio/reactivex/n;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Lkotlin/n;",
            ">;"
        }
    .end annotation

    .prologue
    .line 44
    iget-object v0, p0, Lco/uk/getmondo/signup/card_activation/CardOnItsWayActivity;->h:Lcom/b/b/c;

    const-string v1, "resumeRelay"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lio/reactivex/n;

    return-object v0
.end method

.method public e()Lio/reactivex/n;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Lkotlin/n;",
            ">;"
        }
    .end annotation

    .prologue
    .line 47
    sget v0, Lco/uk/getmondo/c$a;->cardOnItsWayButton:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/card_activation/CardOnItsWayActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 86
    invoke-static {v0}, Lcom/b/a/c/c;->a(Landroid/view/View;)Lio/reactivex/n;

    move-result-object v1

    sget-object v0, Lcom/b/a/a/d;->a:Lcom/b/a/a/d;

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {v1, v0}, Lio/reactivex/n;->map(Lio/reactivex/c/h;)Lio/reactivex/n;

    move-result-object v0

    const-string v1, "RxView.clicks(this).map(VoidToUnit)"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 47
    return-object v0
.end method

.method public f()V
    .locals 1

    .prologue
    .line 50
    sget v0, Lco/uk/getmondo/c$a;->cardOnItsWayButton:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/card_activation/CardOnItsWayActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    invoke-static {v0}, Lco/uk/getmondo/common/ae;->a(Landroid/view/View;)V

    .line 51
    return-void
.end method

.method public g()V
    .locals 2

    .prologue
    .line 54
    sget v0, Lco/uk/getmondo/c$a;->cardOnItsWayButton:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/card_activation/CardOnItsWayActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    const v1, 0x7f0a028a

    invoke-virtual {p0, v1}, Lco/uk/getmondo/signup/card_activation/CardOnItsWayActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 55
    sget v0, Lco/uk/getmondo/c$a;->cardOnItsWayButton:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/card_activation/CardOnItsWayActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    invoke-static {v0}, Lco/uk/getmondo/common/ae;->a(Landroid/view/View;)V

    .line 56
    return-void
.end method

.method public h()V
    .locals 2

    .prologue
    .line 59
    sget-object v1, Lco/uk/getmondo/signup/card_activation/ActivateCardActivity;->e:Lco/uk/getmondo/signup/card_activation/ActivateCardActivity$a;

    move-object v0, p0

    check-cast v0, Landroid/content/Context;

    invoke-virtual {v1, v0}, Lco/uk/getmondo/signup/card_activation/ActivateCardActivity$a;->a(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    const/16 v1, 0x3e9

    invoke-virtual {p0, v0, v1}, Lco/uk/getmondo/signup/card_activation/CardOnItsWayActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 60
    return-void
.end method

.method public i()V
    .locals 1

    .prologue
    .line 63
    sget-object v0, Lco/uk/getmondo/main/HomeActivity;->f:Lco/uk/getmondo/main/HomeActivity$a;

    check-cast p0, Landroid/content/Context;

    invoke-virtual {v0, p0}, Lco/uk/getmondo/main/HomeActivity$a;->a(Landroid/content/Context;)V

    .line 64
    return-void
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 67
    const/16 v0, 0x3e9

    if-ne p1, v0, :cond_1

    .line 68
    if-ne p2, v1, :cond_0

    .line 69
    invoke-virtual {p0, v1}, Lco/uk/getmondo/signup/card_activation/CardOnItsWayActivity;->setResult(I)V

    .line 70
    invoke-virtual {p0}, Lco/uk/getmondo/signup/card_activation/CardOnItsWayActivity;->finish()V

    .line 74
    :cond_0
    :goto_0
    return-void

    .line 73
    :cond_1
    invoke-super {p0, p1, p2, p3}, Lco/uk/getmondo/signup/a;->onActivityResult(IILandroid/content/Intent;)V

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 25
    invoke-super {p0, p1}, Lco/uk/getmondo/signup/a;->onCreate(Landroid/os/Bundle;)V

    .line 27
    const v0, 0x7f050025

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/card_activation/CardOnItsWayActivity;->setContentView(I)V

    .line 29
    invoke-virtual {p0}, Lco/uk/getmondo/signup/card_activation/CardOnItsWayActivity;->l()Lco/uk/getmondo/common/h/b/b;

    move-result-object v0

    invoke-interface {v0, p0}, Lco/uk/getmondo/common/h/b/b;->a(Lco/uk/getmondo/signup/card_activation/CardOnItsWayActivity;)V

    .line 30
    iget-object v0, p0, Lco/uk/getmondo/signup/card_activation/CardOnItsWayActivity;->b:Lco/uk/getmondo/signup/card_activation/g;

    if-nez v0, :cond_0

    const-string v1, "presenter"

    invoke-static {v1}, Lkotlin/d/b/l;->b(Ljava/lang/String;)V

    :cond_0
    check-cast p0, Lco/uk/getmondo/signup/card_activation/g$a;

    invoke-virtual {v0, p0}, Lco/uk/getmondo/signup/card_activation/g;->a(Lco/uk/getmondo/signup/card_activation/g$a;)V

    .line 31
    return-void
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    .line 39
    iget-object v0, p0, Lco/uk/getmondo/signup/card_activation/CardOnItsWayActivity;->b:Lco/uk/getmondo/signup/card_activation/g;

    if-nez v0, :cond_0

    const-string v1, "presenter"

    invoke-static {v1}, Lkotlin/d/b/l;->b(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0}, Lco/uk/getmondo/signup/card_activation/g;->b()V

    .line 40
    invoke-super {p0}, Lco/uk/getmondo/signup/a;->onDestroy()V

    .line 41
    return-void
.end method

.method protected onResume()V
    .locals 2

    .prologue
    .line 34
    invoke-super {p0}, Lco/uk/getmondo/signup/a;->onResume()V

    .line 35
    iget-object v0, p0, Lco/uk/getmondo/signup/card_activation/CardOnItsWayActivity;->h:Lcom/b/b/c;

    sget-object v1, Lkotlin/n;->a:Lkotlin/n;

    invoke-virtual {v0, v1}, Lcom/b/b/c;->a(Ljava/lang/Object;)V

    .line 36
    return-void
.end method
