.class public final Lco/uk/getmondo/signup/card_activation/ActivateCardActivity;
.super Lco/uk/getmondo/common/activities/b;
.source "ActivateCardActivity.kt"

# interfaces
.implements Lco/uk/getmondo/signup/card_activation/b$a;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lco/uk/getmondo/signup/card_activation/ActivateCardActivity$a;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000D\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0010\u0008\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0010\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0005\u0018\u0000 $2\u00020\u00012\u00020\u0002:\u0001$B\u0005\u00a2\u0006\u0002\u0010\u0003J\u0008\u0010\u001b\u001a\u00020\u001cH\u0016J\u0008\u0010\u001d\u001a\u00020\u001cH\u0016J\u0012\u0010\u001e\u001a\u00020\u001c2\u0008\u0010\u001f\u001a\u0004\u0018\u00010 H\u0014J\u0008\u0010!\u001a\u00020\u001cH\u0014J\u0008\u0010\"\u001a\u00020\u001cH\u0016J\u0008\u0010#\u001a\u00020\u001cH\u0016R\u001e\u0010\u0004\u001a\u00020\u00058\u0006@\u0006X\u0087.\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u0006\u0010\u0007\"\u0004\u0008\u0008\u0010\tR\u001b\u0010\n\u001a\u00020\u000b8BX\u0082\u0084\u0002\u00a2\u0006\u000c\n\u0004\u0008\u000e\u0010\u000f\u001a\u0004\u0008\u000c\u0010\rR\u001a\u0010\u0010\u001a\u0008\u0012\u0004\u0012\u00020\u00120\u00118VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0013\u0010\u0014R\u001e\u0010\u0015\u001a\u00020\u00168\u0000@\u0000X\u0081.\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u0017\u0010\u0018\"\u0004\u0008\u0019\u0010\u001a\u00a8\u0006%"
    }
    d2 = {
        "Lco/uk/getmondo/signup/card_activation/ActivateCardActivity;",
        "Lco/uk/getmondo/common/activities/BaseActivity;",
        "Lco/uk/getmondo/signup/card_activation/ActivateCardPresenter$View;",
        "()V",
        "intercomService",
        "Lco/uk/getmondo/common/IntercomService;",
        "getIntercomService",
        "()Lco/uk/getmondo/common/IntercomService;",
        "setIntercomService",
        "(Lco/uk/getmondo/common/IntercomService;)V",
        "maxLength",
        "",
        "getMaxLength",
        "()I",
        "maxLength$delegate",
        "Lkotlin/Lazy;",
        "onCardPanEntered",
        "Lio/reactivex/Observable;",
        "",
        "getOnCardPanEntered",
        "()Lio/reactivex/Observable;",
        "presenter",
        "Lco/uk/getmondo/signup/card_activation/ActivateCardPresenter;",
        "getPresenter$app_monzoPrepaidRelease",
        "()Lco/uk/getmondo/signup/card_activation/ActivateCardPresenter;",
        "setPresenter$app_monzoPrepaidRelease",
        "(Lco/uk/getmondo/signup/card_activation/ActivateCardPresenter;)V",
        "cardActivated",
        "",
        "hideLoading",
        "onCreate",
        "savedInstanceState",
        "Landroid/os/Bundle;",
        "onDestroy",
        "reloadSignupStatus",
        "showLoading",
        "Companion",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# static fields
.field static final synthetic a:[Lkotlin/reflect/l;

.field public static final e:Lco/uk/getmondo/signup/card_activation/ActivateCardActivity$a;


# instance fields
.field public b:Lco/uk/getmondo/signup/card_activation/b;

.field public c:Lco/uk/getmondo/common/q;

.field private final f:Lkotlin/c;

.field private g:Ljava/util/HashMap;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const/4 v0, 0x1

    new-array v1, v0, [Lkotlin/reflect/l;

    const/4 v2, 0x0

    new-instance v0, Lkotlin/d/b/w;

    const-class v3, Lco/uk/getmondo/signup/card_activation/ActivateCardActivity;

    invoke-static {v3}, Lkotlin/d/b/y;->a(Ljava/lang/Class;)Lkotlin/reflect/c;

    move-result-object v3

    const-string v4, "maxLength"

    const-string v5, "getMaxLength()I"

    invoke-direct {v0, v3, v4, v5}, Lkotlin/d/b/w;-><init>(Lkotlin/reflect/e;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v0}, Lkotlin/d/b/y;->a(Lkotlin/d/b/v;)Lkotlin/reflect/n;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/l;

    aput-object v0, v1, v2

    sput-object v1, Lco/uk/getmondo/signup/card_activation/ActivateCardActivity;->a:[Lkotlin/reflect/l;

    new-instance v0, Lco/uk/getmondo/signup/card_activation/ActivateCardActivity$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lco/uk/getmondo/signup/card_activation/ActivateCardActivity$a;-><init>(Lkotlin/d/b/i;)V

    sput-object v0, Lco/uk/getmondo/signup/card_activation/ActivateCardActivity;->e:Lco/uk/getmondo/signup/card_activation/ActivateCardActivity$a;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 23
    invoke-direct {p0}, Lco/uk/getmondo/common/activities/b;-><init>()V

    .line 24
    new-instance v0, Lco/uk/getmondo/signup/card_activation/ActivateCardActivity$b;

    invoke-direct {v0, p0}, Lco/uk/getmondo/signup/card_activation/ActivateCardActivity$b;-><init>(Lco/uk/getmondo/signup/card_activation/ActivateCardActivity;)V

    check-cast v0, Lkotlin/d/a/a;

    invoke-static {v0}, Lkotlin/d;->a(Lkotlin/d/a/a;)Lkotlin/c;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/signup/card_activation/ActivateCardActivity;->f:Lkotlin/c;

    return-void
.end method

.method public static final synthetic a(Lco/uk/getmondo/signup/card_activation/ActivateCardActivity;)I
    .locals 1

    .prologue
    .line 23
    invoke-direct {p0}, Lco/uk/getmondo/signup/card_activation/ActivateCardActivity;->f()I

    move-result v0

    return v0
.end method

.method private final f()I
    .locals 3

    iget-object v0, p0, Lco/uk/getmondo/signup/card_activation/ActivateCardActivity;->f:Lkotlin/c;

    sget-object v1, Lco/uk/getmondo/signup/card_activation/ActivateCardActivity;->a:[Lkotlin/reflect/l;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-interface {v0}, Lkotlin/c;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Number;

    invoke-virtual {v0}, Ljava/lang/Number;->intValue()I

    move-result v0

    return v0
.end method


# virtual methods
.method public a(I)Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lco/uk/getmondo/signup/card_activation/ActivateCardActivity;->g:Ljava/util/HashMap;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lco/uk/getmondo/signup/card_activation/ActivateCardActivity;->g:Ljava/util/HashMap;

    :cond_0
    iget-object v0, p0, Lco/uk/getmondo/signup/card_activation/ActivateCardActivity;->g:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    if-nez v0, :cond_1

    invoke-virtual {p0, p1}, Landroid/support/v4/app/j;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/signup/card_activation/ActivateCardActivity;->g:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    return-object v0
.end method

.method public a()Lio/reactivex/n;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 48
    sget v0, Lco/uk/getmondo/c$a;->activateCardEditText:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/card_activation/ActivateCardActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    .line 79
    invoke-static {v0}, Lcom/b/a/d/e;->d(Landroid/widget/TextView;)Lcom/b/a/a;

    move-result-object v1

    const-string v0, "RxTextView.afterTextChangeEvents(this)"

    invoke-static {v1, v0}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 49
    new-instance v0, Lco/uk/getmondo/signup/card_activation/ActivateCardActivity$c;

    invoke-direct {v0, p0}, Lco/uk/getmondo/signup/card_activation/ActivateCardActivity$c;-><init>(Lco/uk/getmondo/signup/card_activation/ActivateCardActivity;)V

    check-cast v0, Lio/reactivex/c/q;

    invoke-virtual {v1, v0}, Lcom/b/a/a;->filter(Lio/reactivex/c/q;)Lio/reactivex/n;

    move-result-object v1

    .line 50
    sget-object v0, Lco/uk/getmondo/signup/card_activation/ActivateCardActivity$d;->a:Lco/uk/getmondo/signup/card_activation/ActivateCardActivity$d;

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {v1, v0}, Lio/reactivex/n;->map(Lio/reactivex/c/h;)Lio/reactivex/n;

    move-result-object v0

    const-string v1, "activateCardEditText.aft\u2026t.editable().toString() }"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public b()V
    .locals 1

    .prologue
    .line 68
    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/card_activation/ActivateCardActivity;->setResult(I)V

    .line 69
    invoke-virtual {p0}, Lco/uk/getmondo/signup/card_activation/ActivateCardActivity;->finish()V

    .line 70
    return-void
.end method

.method public c()V
    .locals 1

    .prologue
    .line 53
    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/card_activation/ActivateCardActivity;->setResult(I)V

    .line 54
    invoke-virtual {p0}, Lco/uk/getmondo/signup/card_activation/ActivateCardActivity;->finish()V

    .line 55
    return-void
.end method

.method public d()V
    .locals 1

    .prologue
    .line 58
    sget v0, Lco/uk/getmondo/c$a;->activateCardProgressBar:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/card_activation/ActivateCardActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    invoke-static {v0}, Lco/uk/getmondo/common/ae;->a(Landroid/view/View;)V

    .line 59
    sget v0, Lco/uk/getmondo/c$a;->activateCardOverlayView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/card_activation/ActivateCardActivity;->a(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Lco/uk/getmondo/common/ae;->a(Landroid/view/View;)V

    .line 60
    return-void
.end method

.method public e()V
    .locals 1

    .prologue
    .line 63
    sget v0, Lco/uk/getmondo/c$a;->activateCardProgressBar:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/card_activation/ActivateCardActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    invoke-static {v0}, Lco/uk/getmondo/common/ae;->b(Landroid/view/View;)V

    .line 64
    sget v0, Lco/uk/getmondo/c$a;->activateCardOverlayView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/card_activation/ActivateCardActivity;->a(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Lco/uk/getmondo/common/ae;->b(Landroid/view/View;)V

    .line 65
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 30
    invoke-super {p0, p1}, Lco/uk/getmondo/common/activities/b;->onCreate(Landroid/os/Bundle;)V

    .line 32
    const v0, 0x7f05001c

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/card_activation/ActivateCardActivity;->setContentView(I)V

    .line 35
    const/16 v0, 0x20

    invoke-static {v0}, Lco/uk/getmondo/common/k/i;->a(C)Lco/uk/getmondo/common/k/i;

    move-result-object v0

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lco/uk/getmondo/common/k/i;->a(I)Lco/uk/getmondo/common/k/i;

    move-result-object v1

    .line 36
    sget v0, Lco/uk/getmondo/c$a;->activateCardEditText:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/card_activation/ActivateCardActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    invoke-virtual {v1}, Lco/uk/getmondo/common/k/i;->a()Landroid/text/TextWatcher;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 38
    invoke-virtual {p0}, Lco/uk/getmondo/signup/card_activation/ActivateCardActivity;->l()Lco/uk/getmondo/common/h/b/b;

    move-result-object v0

    invoke-interface {v0, p0}, Lco/uk/getmondo/common/h/b/b;->a(Lco/uk/getmondo/signup/card_activation/ActivateCardActivity;)V

    .line 39
    iget-object v0, p0, Lco/uk/getmondo/signup/card_activation/ActivateCardActivity;->b:Lco/uk/getmondo/signup/card_activation/b;

    if-nez v0, :cond_0

    const-string v1, "presenter"

    invoke-static {v1}, Lkotlin/d/b/l;->b(Ljava/lang/String;)V

    :cond_0
    check-cast p0, Lco/uk/getmondo/signup/card_activation/b$a;

    invoke-virtual {v0, p0}, Lco/uk/getmondo/signup/card_activation/b;->a(Lco/uk/getmondo/signup/card_activation/b$a;)V

    .line 40
    return-void
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    .line 43
    iget-object v0, p0, Lco/uk/getmondo/signup/card_activation/ActivateCardActivity;->b:Lco/uk/getmondo/signup/card_activation/b;

    if-nez v0, :cond_0

    const-string v1, "presenter"

    invoke-static {v1}, Lkotlin/d/b/l;->b(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0}, Lco/uk/getmondo/signup/card_activation/b;->b()V

    .line 44
    invoke-super {p0}, Lco/uk/getmondo/common/activities/b;->onDestroy()V

    .line 45
    return-void
.end method
