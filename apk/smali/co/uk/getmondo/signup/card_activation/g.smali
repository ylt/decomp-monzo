.class public final Lco/uk/getmondo/signup/card_activation/g;
.super Lco/uk/getmondo/common/ui/b;
.source "CardOnItsWayPresenter.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lco/uk/getmondo/signup/card_activation/g$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lco/uk/getmondo/common/ui/b",
        "<",
        "Lco/uk/getmondo/signup/card_activation/g$a;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u0003\u0008\u0007\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00020\u0001:\u0001\u000bB\u0017\u0008\u0007\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\u0002\u0010\u0007J\u0010\u0010\u0008\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\u0002H\u0016R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000c"
    }
    d2 = {
        "Lco/uk/getmondo/signup/card_activation/CardOnItsWayPresenter;",
        "Lco/uk/getmondo/common/ui/BasePresenter;",
        "Lco/uk/getmondo/signup/card_activation/CardOnItsWayPresenter$View;",
        "analyticsService",
        "Lco/uk/getmondo/common/AnalyticsService;",
        "localUserSettingStorage",
        "Lco/uk/getmondo/settings/LocalUserSettingStorage;",
        "(Lco/uk/getmondo/common/AnalyticsService;Lco/uk/getmondo/settings/LocalUserSettingStorage;)V",
        "register",
        "",
        "view",
        "View",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field private final c:Lco/uk/getmondo/common/a;

.field private final d:Lco/uk/getmondo/settings/k;


# direct methods
.method public constructor <init>(Lco/uk/getmondo/common/a;Lco/uk/getmondo/settings/k;)V
    .locals 1

    .prologue
    const-string v0, "analyticsService"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "localUserSettingStorage"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 19
    invoke-direct {p0}, Lco/uk/getmondo/common/ui/b;-><init>()V

    iput-object p1, p0, Lco/uk/getmondo/signup/card_activation/g;->c:Lco/uk/getmondo/common/a;

    iput-object p2, p0, Lco/uk/getmondo/signup/card_activation/g;->d:Lco/uk/getmondo/settings/k;

    return-void
.end method

.method public static final synthetic a(Lco/uk/getmondo/signup/card_activation/g;)Lco/uk/getmondo/settings/k;
    .locals 1

    .prologue
    .line 15
    iget-object v0, p0, Lco/uk/getmondo/signup/card_activation/g;->d:Lco/uk/getmondo/settings/k;

    return-object v0
.end method


# virtual methods
.method public bridge synthetic a(Lco/uk/getmondo/common/ui/f;)V
    .locals 0

    .prologue
    .line 15
    check-cast p1, Lco/uk/getmondo/signup/card_activation/g$a;

    invoke-virtual {p0, p1}, Lco/uk/getmondo/signup/card_activation/g;->a(Lco/uk/getmondo/signup/card_activation/g$a;)V

    return-void
.end method

.method public a(Lco/uk/getmondo/signup/card_activation/g$a;)V
    .locals 4

    .prologue
    const-string v0, "view"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v0, p1

    .line 22
    check-cast v0, Lco/uk/getmondo/common/ui/f;

    invoke-super {p0, v0}, Lco/uk/getmondo/common/ui/b;->a(Lco/uk/getmondo/common/ui/f;)V

    .line 24
    iget-object v0, p0, Lco/uk/getmondo/signup/card_activation/g;->c:Lco/uk/getmondo/common/a;

    sget-object v1, Lco/uk/getmondo/api/model/tracking/Impression;->Companion:Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    invoke-virtual {v1}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->aR()Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v1

    invoke-virtual {v0, v1}, Lco/uk/getmondo/common/a;->a(Lco/uk/getmondo/api/model/tracking/Impression;)V

    .line 26
    iget-object v0, p0, Lco/uk/getmondo/signup/card_activation/g;->d:Lco/uk/getmondo/settings/k;

    invoke-virtual {v0}, Lco/uk/getmondo/settings/k;->d()Z

    move-result v1

    .line 27
    iget-object v2, p0, Lco/uk/getmondo/signup/card_activation/g;->b:Lio/reactivex/b/a;

    .line 28
    invoke-interface {p1}, Lco/uk/getmondo/signup/card_activation/g$a;->d()Lio/reactivex/n;

    move-result-object v3

    new-instance v0, Lco/uk/getmondo/signup/card_activation/g$b;

    invoke-direct {v0, p0, p1}, Lco/uk/getmondo/signup/card_activation/g$b;-><init>(Lco/uk/getmondo/signup/card_activation/g;Lco/uk/getmondo/signup/card_activation/g$a;)V

    check-cast v0, Lio/reactivex/c/g;

    invoke-virtual {v3, v0}, Lio/reactivex/n;->subscribe(Lio/reactivex/c/g;)Lio/reactivex/b/b;

    move-result-object v0

    const-string v3, "view.onRefresh\n         \u2026      }\n                }"

    invoke-static {v0, v3}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 27
    invoke-static {v2, v0}, Lco/uk/getmondo/common/j/f;->a(Lio/reactivex/b/a;Lio/reactivex/b/b;)Lio/reactivex/b/a;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/signup/card_activation/g;->b:Lio/reactivex/b/a;

    .line 39
    iget-object v2, p0, Lco/uk/getmondo/signup/card_activation/g;->b:Lio/reactivex/b/a;

    .line 40
    invoke-interface {p1}, Lco/uk/getmondo/signup/card_activation/g$a;->e()Lio/reactivex/n;

    move-result-object v3

    new-instance v0, Lco/uk/getmondo/signup/card_activation/g$c;

    invoke-direct {v0, p1, v1}, Lco/uk/getmondo/signup/card_activation/g$c;-><init>(Lco/uk/getmondo/signup/card_activation/g$a;Z)V

    check-cast v0, Lio/reactivex/c/g;

    invoke-virtual {v3, v0}, Lio/reactivex/n;->subscribe(Lio/reactivex/c/g;)Lio/reactivex/b/b;

    move-result-object v0

    const-string v1, "view.onMyCardArrivedClic\u2026      }\n                }"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 39
    invoke-static {v2, v0}, Lco/uk/getmondo/common/j/f;->a(Lio/reactivex/b/a;Lio/reactivex/b/b;)Lio/reactivex/b/a;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/signup/card_activation/g;->b:Lio/reactivex/b/a;

    .line 49
    return-void
.end method
