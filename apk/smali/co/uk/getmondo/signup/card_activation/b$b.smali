.class final Lco/uk/getmondo/signup/card_activation/b$b;
.super Ljava/lang/Object;
.source "ActivateCardPresenter.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lco/uk/getmondo/signup/card_activation/b;->a(Lco/uk/getmondo/signup/card_activation/b$a;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h",
        "<TT;",
        "Lio/reactivex/l",
        "<+TR;>;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000\u0014\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0000\u0010\u0000\u001a&\u0012\u000c\u0012\n \u0003*\u0004\u0018\u00010\u00020\u0002 \u0003*\u0012\u0012\u000c\u0012\n \u0003*\u0004\u0018\u00010\u00020\u0002\u0018\u00010\u00010\u00012\u0006\u0010\u0004\u001a\u00020\u0005H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "Lio/reactivex/Maybe;",
        "Lco/uk/getmondo/common/data/Event;",
        "kotlin.jvm.PlatformType",
        "cardPan",
        "",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field final synthetic a:Lco/uk/getmondo/signup/card_activation/b;

.field final synthetic b:Lco/uk/getmondo/signup/card_activation/b$a;


# direct methods
.method constructor <init>(Lco/uk/getmondo/signup/card_activation/b;Lco/uk/getmondo/signup/card_activation/b$a;)V
    .locals 0

    iput-object p1, p0, Lco/uk/getmondo/signup/card_activation/b$b;->a:Lco/uk/getmondo/signup/card_activation/b;

    iput-object p2, p0, Lco/uk/getmondo/signup/card_activation/b$b;->b:Lco/uk/getmondo/signup/card_activation/b$a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Lio/reactivex/h;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/h",
            "<",
            "Lco/uk/getmondo/common/b/a;",
            ">;"
        }
    .end annotation

    .prologue
    const-string v0, "cardPan"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 36
    iget-object v0, p0, Lco/uk/getmondo/signup/card_activation/b$b;->a:Lco/uk/getmondo/signup/card_activation/b;

    invoke-static {v0}, Lco/uk/getmondo/signup/card_activation/b;->a(Lco/uk/getmondo/signup/card_activation/b;)Lco/uk/getmondo/signup/card_activation/d;

    move-result-object v6

    const-string v1, " "

    const-string v2, ""

    const/4 v3, 0x0

    const/4 v4, 0x4

    const/4 v5, 0x0

    move-object v0, p1

    invoke-static/range {v0 .. v5}, Lkotlin/h/j;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZILjava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v6, v0}, Lco/uk/getmondo/signup/card_activation/d;->a(Ljava/lang/String;)Lio/reactivex/b;

    move-result-object v0

    .line 37
    sget-object v1, Lco/uk/getmondo/common/b/a;->a:Lco/uk/getmondo/common/b/a;

    invoke-virtual {v0, v1}, Lio/reactivex/b;->a(Ljava/lang/Object;)Lio/reactivex/v;

    move-result-object v0

    .line 38
    invoke-virtual {v0}, Lio/reactivex/v;->e()Lio/reactivex/h;

    move-result-object v0

    .line 39
    iget-object v1, p0, Lco/uk/getmondo/signup/card_activation/b$b;->a:Lco/uk/getmondo/signup/card_activation/b;

    invoke-static {v1}, Lco/uk/getmondo/signup/card_activation/b;->b(Lco/uk/getmondo/signup/card_activation/b;)Lio/reactivex/u;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/h;->b(Lio/reactivex/u;)Lio/reactivex/h;

    move-result-object v0

    .line 40
    iget-object v1, p0, Lco/uk/getmondo/signup/card_activation/b$b;->a:Lco/uk/getmondo/signup/card_activation/b;

    invoke-static {v1}, Lco/uk/getmondo/signup/card_activation/b;->c(Lco/uk/getmondo/signup/card_activation/b;)Lio/reactivex/u;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/h;->a(Lio/reactivex/u;)Lio/reactivex/h;

    move-result-object v1

    .line 41
    new-instance v0, Lco/uk/getmondo/signup/card_activation/b$b$1;

    invoke-direct {v0, p0}, Lco/uk/getmondo/signup/card_activation/b$b$1;-><init>(Lco/uk/getmondo/signup/card_activation/b$b;)V

    check-cast v0, Lio/reactivex/c/g;

    invoke-virtual {v1, v0}, Lio/reactivex/h;->b(Lio/reactivex/c/g;)Lio/reactivex/h;

    move-result-object v1

    .line 42
    new-instance v0, Lco/uk/getmondo/signup/card_activation/b$b$2;

    invoke-direct {v0, p0}, Lco/uk/getmondo/signup/card_activation/b$b$2;-><init>(Lco/uk/getmondo/signup/card_activation/b$b;)V

    check-cast v0, Lio/reactivex/c/g;

    invoke-virtual {v1, v0}, Lio/reactivex/h;->a(Lio/reactivex/c/g;)Lio/reactivex/h;

    move-result-object v1

    .line 43
    new-instance v0, Lco/uk/getmondo/signup/card_activation/b$b$3;

    invoke-direct {v0, p0}, Lco/uk/getmondo/signup/card_activation/b$b$3;-><init>(Lco/uk/getmondo/signup/card_activation/b$b;)V

    check-cast v0, Lio/reactivex/c/b;

    invoke-virtual {v1, v0}, Lio/reactivex/h;->a(Lio/reactivex/c/b;)Lio/reactivex/h;

    move-result-object v0

    .line 44
    invoke-virtual {v0}, Lio/reactivex/h;->f()Lio/reactivex/h;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 20
    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1}, Lco/uk/getmondo/signup/card_activation/b$b;->a(Ljava/lang/String;)Lio/reactivex/h;

    move-result-object v0

    return-object v0
.end method
