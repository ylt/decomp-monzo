.class public final Lco/uk/getmondo/signup/card_activation/h;
.super Ljava/lang/Object;
.source "CardOnItsWayPresenter_Factory.java"

# interfaces
.implements Lb/a/b;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lb/a/b",
        "<",
        "Lco/uk/getmondo/signup/card_activation/g;",
        ">;"
    }
.end annotation


# static fields
.field static final synthetic a:Z


# instance fields
.field private final b:Lb/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lb/a",
            "<",
            "Lco/uk/getmondo/signup/card_activation/g;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Ljavax/a/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/common/a;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Ljavax/a/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/settings/k;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 11
    const-class v0, Lco/uk/getmondo/signup/card_activation/h;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lco/uk/getmondo/signup/card_activation/h;->a:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lb/a;Ljavax/a/a;Ljavax/a/a;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lb/a",
            "<",
            "Lco/uk/getmondo/signup/card_activation/g;",
            ">;",
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/common/a;",
            ">;",
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/settings/k;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    sget-boolean v0, Lco/uk/getmondo/signup/card_activation/h;->a:Z

    if-nez v0, :cond_0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 27
    :cond_0
    iput-object p1, p0, Lco/uk/getmondo/signup/card_activation/h;->b:Lb/a;

    .line 28
    sget-boolean v0, Lco/uk/getmondo/signup/card_activation/h;->a:Z

    if-nez v0, :cond_1

    if-nez p2, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 29
    :cond_1
    iput-object p2, p0, Lco/uk/getmondo/signup/card_activation/h;->c:Ljavax/a/a;

    .line 30
    sget-boolean v0, Lco/uk/getmondo/signup/card_activation/h;->a:Z

    if-nez v0, :cond_2

    if-nez p3, :cond_2

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 31
    :cond_2
    iput-object p3, p0, Lco/uk/getmondo/signup/card_activation/h;->d:Ljavax/a/a;

    .line 32
    return-void
.end method

.method public static a(Lb/a;Ljavax/a/a;Ljavax/a/a;)Lb/a/b;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lb/a",
            "<",
            "Lco/uk/getmondo/signup/card_activation/g;",
            ">;",
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/common/a;",
            ">;",
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/settings/k;",
            ">;)",
            "Lb/a/b",
            "<",
            "Lco/uk/getmondo/signup/card_activation/g;",
            ">;"
        }
    .end annotation

    .prologue
    .line 46
    new-instance v0, Lco/uk/getmondo/signup/card_activation/h;

    invoke-direct {v0, p0, p1, p2}, Lco/uk/getmondo/signup/card_activation/h;-><init>(Lb/a;Ljavax/a/a;Ljavax/a/a;)V

    return-object v0
.end method


# virtual methods
.method public a()Lco/uk/getmondo/signup/card_activation/g;
    .locals 4

    .prologue
    .line 36
    iget-object v2, p0, Lco/uk/getmondo/signup/card_activation/h;->b:Lb/a;

    new-instance v3, Lco/uk/getmondo/signup/card_activation/g;

    iget-object v0, p0, Lco/uk/getmondo/signup/card_activation/h;->c:Ljavax/a/a;

    .line 39
    invoke-interface {v0}, Ljavax/a/a;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/common/a;

    iget-object v1, p0, Lco/uk/getmondo/signup/card_activation/h;->d:Ljavax/a/a;

    invoke-interface {v1}, Ljavax/a/a;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lco/uk/getmondo/settings/k;

    invoke-direct {v3, v0, v1}, Lco/uk/getmondo/signup/card_activation/g;-><init>(Lco/uk/getmondo/common/a;Lco/uk/getmondo/settings/k;)V

    .line 36
    invoke-static {v2, v3}, Lb/a/c;->a(Lb/a;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/signup/card_activation/g;

    return-object v0
.end method

.method public synthetic b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 11
    invoke-virtual {p0}, Lco/uk/getmondo/signup/card_activation/h;->a()Lco/uk/getmondo/signup/card_activation/g;

    move-result-object v0

    return-object v0
.end method
