.class public final Lco/uk/getmondo/signup/documents/f;
.super Lco/uk/getmondo/common/ui/b;
.source "LegalDocumentsPresenter.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lco/uk/getmondo/signup/documents/f$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lco/uk/getmondo/common/ui/b",
        "<",
        "Lco/uk/getmondo/signup/documents/f$a;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u00008\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u0003\n\u0002\u0008\u0005\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00020\u0001:\u0001\u0014B3\u0008\u0007\u0012\u0008\u0008\u0001\u0010\u0003\u001a\u00020\u0004\u0012\u0008\u0008\u0001\u0010\u0005\u001a\u00020\u0004\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b\u00a2\u0006\u0002\u0010\u000cJ\u0010\u0010\r\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u0010H\u0002J\u0010\u0010\u0011\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u0010H\u0002J\u0010\u0010\u0012\u001a\u00020\u000e2\u0006\u0010\u0013\u001a\u00020\u0002H\u0016R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0015"
    }
    d2 = {
        "Lco/uk/getmondo/signup/documents/LegalDocumentsPresenter;",
        "Lco/uk/getmondo/common/ui/BasePresenter;",
        "Lco/uk/getmondo/signup/documents/LegalDocumentsPresenter$View;",
        "uiScheduler",
        "Lio/reactivex/Scheduler;",
        "ioScheduler",
        "analyticsService",
        "Lco/uk/getmondo/common/AnalyticsService;",
        "legalDocumentsManager",
        "Lco/uk/getmondo/signup/documents/LegalDocumentsManager;",
        "apiErrorHandler",
        "Lco/uk/getmondo/common/errors/ApiErrorHandler;",
        "(Lio/reactivex/Scheduler;Lio/reactivex/Scheduler;Lco/uk/getmondo/common/AnalyticsService;Lco/uk/getmondo/signup/documents/LegalDocumentsManager;Lco/uk/getmondo/common/errors/ApiErrorHandler;)V",
        "handleAcceptError",
        "",
        "throwable",
        "",
        "handleGetUrlsError",
        "register",
        "view",
        "View",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field private final c:Lio/reactivex/u;

.field private final d:Lio/reactivex/u;

.field private final e:Lco/uk/getmondo/common/a;

.field private final f:Lco/uk/getmondo/signup/documents/d;

.field private final g:Lco/uk/getmondo/common/e/a;


# direct methods
.method public constructor <init>(Lio/reactivex/u;Lio/reactivex/u;Lco/uk/getmondo/common/a;Lco/uk/getmondo/signup/documents/d;Lco/uk/getmondo/common/e/a;)V
    .locals 1

    .prologue
    const-string v0, "uiScheduler"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "ioScheduler"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "analyticsService"

    invoke-static {p3, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "legalDocumentsManager"

    invoke-static {p4, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "apiErrorHandler"

    invoke-static {p5, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 27
    invoke-direct {p0}, Lco/uk/getmondo/common/ui/b;-><init>()V

    iput-object p1, p0, Lco/uk/getmondo/signup/documents/f;->c:Lio/reactivex/u;

    iput-object p2, p0, Lco/uk/getmondo/signup/documents/f;->d:Lio/reactivex/u;

    iput-object p3, p0, Lco/uk/getmondo/signup/documents/f;->e:Lco/uk/getmondo/common/a;

    iput-object p4, p0, Lco/uk/getmondo/signup/documents/f;->f:Lco/uk/getmondo/signup/documents/d;

    iput-object p5, p0, Lco/uk/getmondo/signup/documents/f;->g:Lco/uk/getmondo/common/e/a;

    return-void
.end method

.method public static final synthetic a(Lco/uk/getmondo/signup/documents/f;)Lco/uk/getmondo/signup/documents/d;
    .locals 1

    .prologue
    .line 20
    iget-object v0, p0, Lco/uk/getmondo/signup/documents/f;->f:Lco/uk/getmondo/signup/documents/d;

    return-object v0
.end method

.method public static final synthetic a(Lco/uk/getmondo/signup/documents/f;Ljava/lang/Throwable;)V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0, p1}, Lco/uk/getmondo/signup/documents/f;->a(Ljava/lang/Throwable;)V

    return-void
.end method

.method private final a(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 82
    iget-object v0, p0, Lco/uk/getmondo/signup/documents/f;->a:Lco/uk/getmondo/common/ui/f;

    check-cast v0, Lco/uk/getmondo/signup/documents/f$a;

    invoke-interface {v0}, Lco/uk/getmondo/signup/documents/f$a;->i()V

    .line 83
    sget-object v1, Lco/uk/getmondo/signup/i;->a:Lco/uk/getmondo/signup/i;

    iget-object v0, p0, Lco/uk/getmondo/signup/documents/f;->a:Lco/uk/getmondo/common/ui/f;

    const-string v2, "view"

    invoke-static {v0, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lco/uk/getmondo/signup/i$a;

    invoke-virtual {v1, p1, v0}, Lco/uk/getmondo/signup/i;->a(Ljava/lang/Throwable;Lco/uk/getmondo/signup/i$a;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v1, p0, Lco/uk/getmondo/signup/documents/f;->g:Lco/uk/getmondo/common/e/a;

    iget-object v0, p0, Lco/uk/getmondo/signup/documents/f;->a:Lco/uk/getmondo/common/ui/f;

    const-string v2, "view"

    invoke-static {v0, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lco/uk/getmondo/common/e/a$a;

    invoke-virtual {v1, p1, v0}, Lco/uk/getmondo/common/e/a;->a(Ljava/lang/Throwable;Lco/uk/getmondo/common/e/a$a;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 84
    iget-object v0, p0, Lco/uk/getmondo/signup/documents/f;->a:Lco/uk/getmondo/common/ui/f;

    check-cast v0, Lco/uk/getmondo/signup/documents/f$a;

    const v1, 0x7f0a0196

    invoke-interface {v0, v1}, Lco/uk/getmondo/signup/documents/f$a;->b(I)V

    .line 86
    :cond_0
    return-void
.end method

.method public static final synthetic b(Lco/uk/getmondo/signup/documents/f;)Lio/reactivex/u;
    .locals 1

    .prologue
    .line 20
    iget-object v0, p0, Lco/uk/getmondo/signup/documents/f;->d:Lio/reactivex/u;

    return-object v0
.end method

.method public static final synthetic b(Lco/uk/getmondo/signup/documents/f;Ljava/lang/Throwable;)V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0, p1}, Lco/uk/getmondo/signup/documents/f;->b(Ljava/lang/Throwable;)V

    return-void
.end method

.method private final b(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 89
    iget-object v0, p0, Lco/uk/getmondo/signup/documents/f;->a:Lco/uk/getmondo/common/ui/f;

    check-cast v0, Lco/uk/getmondo/signup/documents/f$a;

    invoke-interface {v0}, Lco/uk/getmondo/signup/documents/f$a;->k()V

    .line 90
    sget-object v1, Lco/uk/getmondo/signup/i;->a:Lco/uk/getmondo/signup/i;

    iget-object v0, p0, Lco/uk/getmondo/signup/documents/f;->a:Lco/uk/getmondo/common/ui/f;

    const-string v2, "view"

    invoke-static {v0, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lco/uk/getmondo/signup/i$a;

    invoke-virtual {v1, p1, v0}, Lco/uk/getmondo/signup/i;->a(Ljava/lang/Throwable;Lco/uk/getmondo/signup/i$a;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v1, p0, Lco/uk/getmondo/signup/documents/f;->g:Lco/uk/getmondo/common/e/a;

    iget-object v0, p0, Lco/uk/getmondo/signup/documents/f;->a:Lco/uk/getmondo/common/ui/f;

    const-string v2, "view"

    invoke-static {v0, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lco/uk/getmondo/common/e/a$a;

    invoke-virtual {v1, p1, v0}, Lco/uk/getmondo/common/e/a;->a(Ljava/lang/Throwable;Lco/uk/getmondo/common/e/a$a;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 91
    iget-object v0, p0, Lco/uk/getmondo/signup/documents/f;->a:Lco/uk/getmondo/common/ui/f;

    check-cast v0, Lco/uk/getmondo/signup/documents/f$a;

    const v1, 0x7f0a0196

    invoke-interface {v0, v1}, Lco/uk/getmondo/signup/documents/f$a;->b(I)V

    .line 93
    :cond_0
    return-void
.end method

.method public static final synthetic c(Lco/uk/getmondo/signup/documents/f;)Lio/reactivex/u;
    .locals 1

    .prologue
    .line 20
    iget-object v0, p0, Lco/uk/getmondo/signup/documents/f;->c:Lio/reactivex/u;

    return-object v0
.end method

.method public static final synthetic d(Lco/uk/getmondo/signup/documents/f;)Lco/uk/getmondo/common/a;
    .locals 1

    .prologue
    .line 20
    iget-object v0, p0, Lco/uk/getmondo/signup/documents/f;->e:Lco/uk/getmondo/common/a;

    return-object v0
.end method


# virtual methods
.method public bridge synthetic a(Lco/uk/getmondo/common/ui/f;)V
    .locals 0

    .prologue
    .line 20
    check-cast p1, Lco/uk/getmondo/signup/documents/f$a;

    invoke-virtual {p0, p1}, Lco/uk/getmondo/signup/documents/f;->a(Lco/uk/getmondo/signup/documents/f$a;)V

    return-void
.end method

.method public a(Lco/uk/getmondo/signup/documents/f$a;)V
    .locals 3

    .prologue
    const-string v0, "view"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v0, p1

    .line 30
    check-cast v0, Lco/uk/getmondo/common/ui/f;

    invoke-super {p0, v0}, Lco/uk/getmondo/common/ui/b;->a(Lco/uk/getmondo/common/ui/f;)V

    .line 32
    iget-object v0, p0, Lco/uk/getmondo/signup/documents/f;->e:Lco/uk/getmondo/common/a;

    sget-object v1, Lco/uk/getmondo/api/model/tracking/Impression;->Companion:Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    invoke-virtual {v1}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->aI()Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v1

    invoke-virtual {v0, v1}, Lco/uk/getmondo/common/a;->a(Lco/uk/getmondo/api/model/tracking/Impression;)V

    .line 34
    iget-object v1, p0, Lco/uk/getmondo/signup/documents/f;->b:Lio/reactivex/b/a;

    .line 43
    invoke-interface {p1}, Lco/uk/getmondo/signup/documents/f$a;->a()Lio/reactivex/n;

    move-result-object v0

    .line 35
    sget-object v2, Lkotlin/n;->a:Lkotlin/n;

    invoke-virtual {v0, v2}, Lio/reactivex/n;->startWith(Ljava/lang/Object;)Lio/reactivex/n;

    move-result-object v2

    .line 36
    new-instance v0, Lco/uk/getmondo/signup/documents/f$b;

    invoke-direct {v0, p0, p1}, Lco/uk/getmondo/signup/documents/f$b;-><init>(Lco/uk/getmondo/signup/documents/f;Lco/uk/getmondo/signup/documents/f$a;)V

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {v2, v0}, Lio/reactivex/n;->flatMapMaybe(Lio/reactivex/c/h;)Lio/reactivex/n;

    move-result-object v2

    .line 43
    new-instance v0, Lco/uk/getmondo/signup/documents/f$c;

    invoke-direct {v0, p1}, Lco/uk/getmondo/signup/documents/f$c;-><init>(Lco/uk/getmondo/signup/documents/f$a;)V

    check-cast v0, Lio/reactivex/c/g;

    invoke-virtual {v2, v0}, Lio/reactivex/n;->subscribe(Lio/reactivex/c/g;)Lio/reactivex/b/b;

    move-result-object v0

    const-string v2, "view.onRefreshUrlsClicke\u2026 = urls\n                }"

    invoke-static {v0, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 34
    invoke-static {v1, v0}, Lco/uk/getmondo/common/j/f;->a(Lio/reactivex/b/a;Lio/reactivex/b/b;)Lio/reactivex/b/a;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/signup/documents/f;->b:Lio/reactivex/b/a;

    .line 48
    iget-object v1, p0, Lco/uk/getmondo/signup/documents/f;->b:Lio/reactivex/b/a;

    .line 49
    invoke-interface {p1}, Lco/uk/getmondo/signup/documents/f$a;->c()Lio/reactivex/n;

    move-result-object v2

    new-instance v0, Lco/uk/getmondo/signup/documents/f$d;

    invoke-direct {v0, p0, p1}, Lco/uk/getmondo/signup/documents/f$d;-><init>(Lco/uk/getmondo/signup/documents/f;Lco/uk/getmondo/signup/documents/f$a;)V

    check-cast v0, Lio/reactivex/c/g;

    invoke-virtual {v2, v0}, Lio/reactivex/n;->subscribe(Lio/reactivex/c/g;)Lio/reactivex/b/b;

    move-result-object v0

    const-string v2, "view.onTermsAndCondition\u2026tions()\n                }"

    invoke-static {v0, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 48
    invoke-static {v1, v0}, Lco/uk/getmondo/common/j/f;->a(Lio/reactivex/b/a;Lio/reactivex/b/b;)Lio/reactivex/b/a;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/signup/documents/f;->b:Lio/reactivex/b/a;

    .line 54
    iget-object v1, p0, Lco/uk/getmondo/signup/documents/f;->b:Lio/reactivex/b/a;

    .line 55
    invoke-interface {p1}, Lco/uk/getmondo/signup/documents/f$a;->d()Lio/reactivex/n;

    move-result-object v2

    new-instance v0, Lco/uk/getmondo/signup/documents/f$e;

    invoke-direct {v0, p0, p1}, Lco/uk/getmondo/signup/documents/f$e;-><init>(Lco/uk/getmondo/signup/documents/f;Lco/uk/getmondo/signup/documents/f$a;)V

    check-cast v0, Lio/reactivex/c/g;

    invoke-virtual {v2, v0}, Lio/reactivex/n;->subscribe(Lio/reactivex/c/g;)Lio/reactivex/b/b;

    move-result-object v0

    const-string v2, "view.onPrivacyPolicyClic\u2026olicy()\n                }"

    invoke-static {v0, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 54
    invoke-static {v1, v0}, Lco/uk/getmondo/common/j/f;->a(Lio/reactivex/b/a;Lio/reactivex/b/b;)Lio/reactivex/b/a;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/signup/documents/f;->b:Lio/reactivex/b/a;

    .line 60
    iget-object v1, p0, Lco/uk/getmondo/signup/documents/f;->b:Lio/reactivex/b/a;

    .line 61
    invoke-interface {p1}, Lco/uk/getmondo/signup/documents/f$a;->e()Lio/reactivex/n;

    move-result-object v2

    new-instance v0, Lco/uk/getmondo/signup/documents/f$f;

    invoke-direct {v0, p0, p1}, Lco/uk/getmondo/signup/documents/f$f;-><init>(Lco/uk/getmondo/signup/documents/f;Lco/uk/getmondo/signup/documents/f$a;)V

    check-cast v0, Lio/reactivex/c/g;

    invoke-virtual {v2, v0}, Lio/reactivex/n;->subscribe(Lio/reactivex/c/g;)Lio/reactivex/b/b;

    move-result-object v0

    const-string v2, "view.onFscsProtectionCli\u2026ction()\n                }"

    invoke-static {v0, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 60
    invoke-static {v1, v0}, Lco/uk/getmondo/common/j/f;->a(Lio/reactivex/b/a;Lio/reactivex/b/b;)Lio/reactivex/b/a;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/signup/documents/f;->b:Lio/reactivex/b/a;

    .line 66
    iget-object v1, p0, Lco/uk/getmondo/signup/documents/f;->b:Lio/reactivex/b/a;

    .line 78
    invoke-interface {p1}, Lco/uk/getmondo/signup/documents/f$a;->f()Lio/reactivex/n;

    move-result-object v2

    .line 67
    new-instance v0, Lco/uk/getmondo/signup/documents/f$g;

    invoke-direct {v0, p0, p1}, Lco/uk/getmondo/signup/documents/f$g;-><init>(Lco/uk/getmondo/signup/documents/f;Lco/uk/getmondo/signup/documents/f$a;)V

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {v2, v0}, Lio/reactivex/n;->flatMapMaybe(Lio/reactivex/c/h;)Lio/reactivex/n;

    move-result-object v2

    .line 78
    new-instance v0, Lco/uk/getmondo/signup/documents/f$h;

    invoke-direct {v0, p1}, Lco/uk/getmondo/signup/documents/f$h;-><init>(Lco/uk/getmondo/signup/documents/f$a;)V

    check-cast v0, Lio/reactivex/c/g;

    invoke-virtual {v2, v0}, Lio/reactivex/n;->subscribe(Lio/reactivex/c/g;)Lio/reactivex/b/b;

    move-result-object v0

    const-string v2, "view.onContinueClicked\n \u2026 { view.completeStage() }"

    invoke-static {v0, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 66
    invoke-static {v1, v0}, Lco/uk/getmondo/common/j/f;->a(Lio/reactivex/b/a;Lio/reactivex/b/b;)Lio/reactivex/b/a;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/signup/documents/f;->b:Lio/reactivex/b/a;

    .line 79
    return-void
.end method
