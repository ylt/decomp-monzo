.class public final Lco/uk/getmondo/signup/documents/d;
.super Ljava/lang/Object;
.source "LegalDocumentsManager.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000*\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u000f\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u001e\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u00082\u0006\u0010\t\u001a\u00020\u00082\u0006\u0010\n\u001a\u00020\u0008J\u000c\u0010\u000b\u001a\u0008\u0012\u0004\u0012\u00020\r0\u000cR\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000e"
    }
    d2 = {
        "Lco/uk/getmondo/signup/documents/LegalDocumentsManager;",
        "",
        "signupApi",
        "Lco/uk/getmondo/api/SignupApi;",
        "(Lco/uk/getmondo/api/SignupApi;)V",
        "acceptLegalDocuments",
        "Lio/reactivex/Completable;",
        "termsAndConditionsVersion",
        "",
        "privacyPolicyVersion",
        "fscsInformationSheetVersion",
        "legalDocuments",
        "Lio/reactivex/Single;",
        "Lco/uk/getmondo/api/model/signup/SignUpDocumentUrls;",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field private final a:Lco/uk/getmondo/api/SignupApi;


# direct methods
.method public constructor <init>(Lco/uk/getmondo/api/SignupApi;)V
    .locals 1

    .prologue
    const-string v0, "signupApi"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lco/uk/getmondo/signup/documents/d;->a:Lco/uk/getmondo/api/SignupApi;

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lio/reactivex/b;
    .locals 7

    .prologue
    const/4 v1, 0x1

    const-string v0, "termsAndConditionsVersion"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "privacyPolicyVersion"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "fscsInformationSheetVersion"

    invoke-static {p3, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 11
    iget-object v0, p0, Lco/uk/getmondo/signup/documents/d;->a:Lco/uk/getmondo/api/SignupApi;

    move-object v2, p1

    move v3, v1

    move-object v4, p2

    move v5, v1

    move-object v6, p3

    invoke-interface/range {v0 .. v6}, Lco/uk/getmondo/api/SignupApi;->acceptLegalDocuments(ZLjava/lang/String;ZLjava/lang/String;ZLjava/lang/String;)Lio/reactivex/b;

    move-result-object v0

    return-object v0
.end method

.method public final a()Lio/reactivex/v;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/v",
            "<",
            "Lco/uk/getmondo/api/model/signup/SignUpDocumentUrls;",
            ">;"
        }
    .end annotation

    .prologue
    .line 8
    iget-object v0, p0, Lco/uk/getmondo/signup/documents/d;->a:Lco/uk/getmondo/api/SignupApi;

    invoke-interface {v0}, Lco/uk/getmondo/api/SignupApi;->legalDocuments()Lio/reactivex/v;

    move-result-object v0

    return-object v0
.end method
