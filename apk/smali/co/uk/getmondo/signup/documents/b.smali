.class public final Lco/uk/getmondo/signup/documents/b;
.super Lco/uk/getmondo/common/f/a;
.source "LegalDocumentsFragment.kt"

# interfaces
.implements Lco/uk/getmondo/signup/documents/f$a;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lco/uk/getmondo/signup/documents/b$a;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000V\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u000b\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0008\n\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\n\n\u0002\u0010\u000e\n\u0002\u0008\u0003\u0018\u00002\u00020\u00012\u00020\u0002:\u00019B\u0005\u00a2\u0006\u0002\u0010\u0003J\u0008\u0010\u001e\u001a\u00020\u0006H\u0016J\u0008\u0010\u001f\u001a\u00020\u0006H\u0016J\u0008\u0010 \u001a\u00020\u0006H\u0016J\u0010\u0010!\u001a\u00020\u00062\u0006\u0010\"\u001a\u00020#H\u0016J\u0012\u0010$\u001a\u00020\u00062\u0008\u0010%\u001a\u0004\u0018\u00010&H\u0016J$\u0010\'\u001a\u00020(2\u0006\u0010)\u001a\u00020*2\u0008\u0010+\u001a\u0004\u0018\u00010,2\u0008\u0010%\u001a\u0004\u0018\u00010&H\u0016J\u0008\u0010-\u001a\u00020\u0006H\u0016J\u001c\u0010.\u001a\u00020\u00062\u0008\u0010/\u001a\u0004\u0018\u00010(2\u0008\u0010%\u001a\u0004\u0018\u00010&H\u0016J\u0008\u00100\u001a\u00020\u0006H\u0016J\u0008\u00101\u001a\u00020\u0006H\u0016J\u0008\u00102\u001a\u00020\u0006H\u0016J\u0008\u00103\u001a\u00020\u0006H\u0016J\u0008\u00104\u001a\u00020\u0006H\u0016J\u0012\u00105\u001a\u00020\u00062\u0008\u00106\u001a\u0004\u0018\u000107H\u0016J\u0008\u00108\u001a\u00020\u0006H\u0016R\u001a\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u00058VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0007\u0010\u0008R\u001a\u0010\t\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u00058VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\n\u0010\u0008R\u001a\u0010\u000b\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u00058VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u000c\u0010\u0008R\u001a\u0010\r\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u00058VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u000e\u0010\u0008R\u001a\u0010\u000f\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u00058VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0010\u0010\u0008R\u001e\u0010\u0011\u001a\u00020\u00128\u0006@\u0006X\u0087.\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u0013\u0010\u0014\"\u0004\u0008\u0015\u0010\u0016R(\u0010\u0019\u001a\u0004\u0018\u00010\u00182\u0008\u0010\u0017\u001a\u0004\u0018\u00010\u0018@VX\u0096\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u001a\u0010\u001b\"\u0004\u0008\u001c\u0010\u001d\u00a8\u0006:"
    }
    d2 = {
        "Lco/uk/getmondo/signup/documents/LegalDocumentsFragment;",
        "Lco/uk/getmondo/common/fragments/BaseFragment;",
        "Lco/uk/getmondo/signup/documents/LegalDocumentsPresenter$View;",
        "()V",
        "onContinueClicked",
        "Lio/reactivex/Observable;",
        "",
        "getOnContinueClicked",
        "()Lio/reactivex/Observable;",
        "onFscsProtectionClicked",
        "getOnFscsProtectionClicked",
        "onPrivacyPolicyClicked",
        "getOnPrivacyPolicyClicked",
        "onRefreshUrlsClicked",
        "getOnRefreshUrlsClicked",
        "onTermsAndConditionsClicked",
        "getOnTermsAndConditionsClicked",
        "presenter",
        "Lco/uk/getmondo/signup/documents/LegalDocumentsPresenter;",
        "getPresenter",
        "()Lco/uk/getmondo/signup/documents/LegalDocumentsPresenter;",
        "setPresenter",
        "(Lco/uk/getmondo/signup/documents/LegalDocumentsPresenter;)V",
        "value",
        "Lco/uk/getmondo/api/model/signup/SignUpDocumentUrls;",
        "urls",
        "getUrls",
        "()Lco/uk/getmondo/api/model/signup/SignUpDocumentUrls;",
        "setUrls",
        "(Lco/uk/getmondo/api/model/signup/SignUpDocumentUrls;)V",
        "completeStage",
        "hideAcceptLoading",
        "hideUrlsLoading",
        "onAttach",
        "context",
        "Landroid/content/Context;",
        "onCreate",
        "savedInstanceState",
        "Landroid/os/Bundle;",
        "onCreateView",
        "Landroid/view/View;",
        "inflater",
        "Landroid/view/LayoutInflater;",
        "container",
        "Landroid/view/ViewGroup;",
        "onDestroyView",
        "onViewCreated",
        "view",
        "openFscsProtection",
        "openPrivacyPolicy",
        "openTermsAndConditions",
        "reloadSignupStatus",
        "showAcceptLoading",
        "showError",
        "message",
        "",
        "showUrlsLoading",
        "StepListener",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field public a:Lco/uk/getmondo/signup/documents/f;

.field private c:Lco/uk/getmondo/api/model/signup/SignUpDocumentUrls;

.field private d:Ljava/util/HashMap;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Lco/uk/getmondo/common/f/a;-><init>()V

    return-void
.end method


# virtual methods
.method public a(I)Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lco/uk/getmondo/signup/documents/b;->d:Ljava/util/HashMap;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lco/uk/getmondo/signup/documents/b;->d:Ljava/util/HashMap;

    :cond_0
    iget-object v0, p0, Lco/uk/getmondo/signup/documents/b;->d:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    if-nez v0, :cond_1

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getView()Landroid/view/View;

    move-result-object v0

    if-nez v0, :cond_2

    const/4 v0, 0x0

    :cond_1
    :goto_0
    return-object v0

    :cond_2
    invoke-virtual {v0, p1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/signup/documents/b;->d:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method public a()Lio/reactivex/n;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Lkotlin/n;",
            ">;"
        }
    .end annotation

    .prologue
    .line 52
    sget v0, Lco/uk/getmondo/c$a;->monzoDocsErrorView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/documents/b;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/common/ui/LoadingErrorView;

    .line 132
    invoke-static {v0}, Lcom/b/a/c/c;->a(Landroid/view/View;)Lio/reactivex/n;

    move-result-object v1

    sget-object v0, Lcom/b/a/a/d;->a:Lcom/b/a/a/d;

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {v1, v0}, Lio/reactivex/n;->map(Lio/reactivex/c/h;)Lio/reactivex/n;

    move-result-object v0

    const-string v1, "RxView.clicks(this).map(VoidToUnit)"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 52
    return-object v0
.end method

.method public a(Lco/uk/getmondo/api/model/signup/SignUpDocumentUrls;)V
    .locals 1

    .prologue
    .line 68
    iput-object p1, p0, Lco/uk/getmondo/signup/documents/b;->c:Lco/uk/getmondo/api/model/signup/SignUpDocumentUrls;

    .line 69
    sget v0, Lco/uk/getmondo/c$a;->monzoDocsContent:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/documents/b;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/constraint/ConstraintLayout;

    invoke-static {v0}, Lco/uk/getmondo/common/ae;->a(Landroid/view/View;)V

    .line 70
    return-void
.end method

.method public b()V
    .locals 1

    .prologue
    .line 124
    invoke-virtual {p0}, Lco/uk/getmondo/signup/documents/b;->getActivity()Landroid/support/v4/app/j;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/signup/i$a;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lco/uk/getmondo/signup/i$a;->b()V

    .line 125
    :cond_0
    return-void
.end method

.method public c()Lio/reactivex/n;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Lkotlin/n;",
            ">;"
        }
    .end annotation

    .prologue
    .line 55
    sget v0, Lco/uk/getmondo/c$a;->docsTermsAndConditionsButton:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/documents/b;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 133
    invoke-static {v0}, Lcom/b/a/c/c;->a(Landroid/view/View;)Lio/reactivex/n;

    move-result-object v1

    sget-object v0, Lcom/b/a/a/d;->a:Lcom/b/a/a/d;

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {v1, v0}, Lio/reactivex/n;->map(Lio/reactivex/c/h;)Lio/reactivex/n;

    move-result-object v0

    const-string v1, "RxView.clicks(this).map(VoidToUnit)"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 55
    return-object v0
.end method

.method public d()Lio/reactivex/n;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Lkotlin/n;",
            ">;"
        }
    .end annotation

    .prologue
    .line 58
    sget v0, Lco/uk/getmondo/c$a;->docsPrivacyPolicyButton:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/documents/b;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 134
    invoke-static {v0}, Lcom/b/a/c/c;->a(Landroid/view/View;)Lio/reactivex/n;

    move-result-object v1

    sget-object v0, Lcom/b/a/a/d;->a:Lcom/b/a/a/d;

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {v1, v0}, Lio/reactivex/n;->map(Lio/reactivex/c/h;)Lio/reactivex/n;

    move-result-object v0

    const-string v1, "RxView.clicks(this).map(VoidToUnit)"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 58
    return-object v0
.end method

.method public d(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 93
    sget v0, Lco/uk/getmondo/c$a;->monzoDocsContent:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/documents/b;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/constraint/ConstraintLayout;

    invoke-virtual {v0}, Landroid/support/constraint/ConstraintLayout;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_0

    .line 94
    sget v0, Lco/uk/getmondo/c$a;->monzoDocsErrorView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/documents/b;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/common/ui/LoadingErrorView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lco/uk/getmondo/common/ui/LoadingErrorView;->setVisibility(I)V

    .line 95
    sget v0, Lco/uk/getmondo/c$a;->monzoDocsErrorView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/documents/b;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/common/ui/LoadingErrorView;

    invoke-virtual {v0, p1}, Lco/uk/getmondo/common/ui/LoadingErrorView;->setMessage(Ljava/lang/String;)V

    .line 98
    :goto_0
    return-void

    .line 97
    :cond_0
    invoke-super {p0, p1}, Lco/uk/getmondo/common/f/a;->d(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public e()Lio/reactivex/n;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Lkotlin/n;",
            ">;"
        }
    .end annotation

    .prologue
    .line 61
    sget v0, Lco/uk/getmondo/c$a;->docsFscsProtectionButton:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/documents/b;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 135
    invoke-static {v0}, Lcom/b/a/c/c;->a(Landroid/view/View;)Lio/reactivex/n;

    move-result-object v1

    sget-object v0, Lcom/b/a/a/d;->a:Lcom/b/a/a/d;

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {v1, v0}, Lio/reactivex/n;->map(Lio/reactivex/c/h;)Lio/reactivex/n;

    move-result-object v0

    const-string v1, "RxView.clicks(this).map(VoidToUnit)"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 61
    return-object v0
.end method

.method public f()Lio/reactivex/n;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Lkotlin/n;",
            ">;"
        }
    .end annotation

    .prologue
    .line 64
    sget v0, Lco/uk/getmondo/c$a;->docsContinueButton:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/documents/b;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/common/ui/ProgressButton;

    .line 136
    invoke-static {v0}, Lcom/b/a/c/c;->a(Landroid/view/View;)Lio/reactivex/n;

    move-result-object v1

    sget-object v0, Lcom/b/a/a/d;->a:Lcom/b/a/a/d;

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {v1, v0}, Lio/reactivex/n;->map(Lio/reactivex/c/h;)Lio/reactivex/n;

    move-result-object v0

    const-string v1, "RxView.clicks(this).map(VoidToUnit)"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 64
    return-object v0
.end method

.method public g()Lco/uk/getmondo/api/model/signup/SignUpDocumentUrls;
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lco/uk/getmondo/signup/documents/b;->c:Lco/uk/getmondo/api/model/signup/SignUpDocumentUrls;

    return-object v0
.end method

.method public h()V
    .locals 1

    .prologue
    .line 73
    sget v0, Lco/uk/getmondo/c$a;->monzoDocsProgress:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/documents/b;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    invoke-static {v0}, Lco/uk/getmondo/common/ae;->a(Landroid/view/View;)V

    .line 74
    sget v0, Lco/uk/getmondo/c$a;->monzoDocsErrorView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/documents/b;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/common/ui/LoadingErrorView;

    invoke-static {v0}, Lco/uk/getmondo/common/ae;->b(Landroid/view/View;)V

    .line 75
    sget v0, Lco/uk/getmondo/c$a;->monzoDocsContent:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/documents/b;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/constraint/ConstraintLayout;

    invoke-static {v0}, Lco/uk/getmondo/common/ae;->b(Landroid/view/View;)V

    .line 76
    return-void
.end method

.method public i()V
    .locals 1

    .prologue
    .line 79
    sget v0, Lco/uk/getmondo/c$a;->monzoDocsProgress:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/documents/b;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    invoke-static {v0}, Lco/uk/getmondo/common/ae;->b(Landroid/view/View;)V

    .line 80
    return-void
.end method

.method public j()V
    .locals 2

    .prologue
    .line 83
    sget v0, Lco/uk/getmondo/c$a;->docsContinueButton:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/documents/b;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/common/ui/ProgressButton;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lco/uk/getmondo/common/ui/ProgressButton;->setLoading(Z)V

    .line 84
    return-void
.end method

.method public k()V
    .locals 2

    .prologue
    .line 87
    sget v0, Lco/uk/getmondo/c$a;->docsContinueButton:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/documents/b;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/common/ui/ProgressButton;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lco/uk/getmondo/common/ui/ProgressButton;->setLoading(Z)V

    .line 88
    return-void
.end method

.method public l()V
    .locals 2

    .prologue
    .line 102
    invoke-virtual {p0}, Lco/uk/getmondo/signup/documents/b;->getActivity()Landroid/support/v4/app/j;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-virtual {p0}, Lco/uk/getmondo/signup/documents/b;->g()Lco/uk/getmondo/api/model/signup/SignUpDocumentUrls;

    move-result-object v1

    if-nez v1, :cond_0

    invoke-static {}, Lkotlin/d/b/l;->a()V

    :cond_0
    invoke-virtual {v1}, Lco/uk/getmondo/api/model/signup/SignUpDocumentUrls;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lco/uk/getmondo/terms_and_conditions/TermsAndConditionsActivity;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/documents/b;->startActivity(Landroid/content/Intent;)V

    .line 103
    return-void
.end method

.method public m()V
    .locals 3

    .prologue
    .line 106
    new-instance v1, Landroid/support/b/a$a;

    invoke-direct {v1}, Landroid/support/b/a$a;-><init>()V

    .line 107
    invoke-virtual {p0}, Lco/uk/getmondo/signup/documents/b;->getActivity()Landroid/support/v4/app/j;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    const v2, 0x7f0f000f

    invoke-static {v0, v2}, Landroid/support/v4/content/a;->c(Landroid/content/Context;I)I

    move-result v0

    invoke-virtual {v1, v0}, Landroid/support/b/a$a;->a(I)Landroid/support/b/a$a;

    move-result-object v0

    .line 108
    invoke-virtual {v0}, Landroid/support/b/a$a;->a()Landroid/support/b/a;

    move-result-object v1

    .line 109
    invoke-virtual {p0}, Lco/uk/getmondo/signup/documents/b;->getActivity()Landroid/support/v4/app/j;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-virtual {p0}, Lco/uk/getmondo/signup/documents/b;->g()Lco/uk/getmondo/api/model/signup/SignUpDocumentUrls;

    move-result-object v2

    if-nez v2, :cond_0

    invoke-static {}, Lkotlin/d/b/l;->a()V

    :cond_0
    invoke-virtual {v2}, Lco/uk/getmondo/api/model/signup/SignUpDocumentUrls;->c()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/support/b/a;->a(Landroid/content/Context;Landroid/net/Uri;)V

    .line 110
    return-void
.end method

.method public n()V
    .locals 3

    .prologue
    .line 113
    new-instance v1, Landroid/support/b/a$a;

    invoke-direct {v1}, Landroid/support/b/a$a;-><init>()V

    .line 114
    invoke-virtual {p0}, Lco/uk/getmondo/signup/documents/b;->getActivity()Landroid/support/v4/app/j;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    const v2, 0x7f0f000f

    invoke-static {v0, v2}, Landroid/support/v4/content/a;->c(Landroid/content/Context;I)I

    move-result v0

    invoke-virtual {v1, v0}, Landroid/support/b/a$a;->a(I)Landroid/support/b/a$a;

    move-result-object v0

    .line 115
    invoke-virtual {v0}, Landroid/support/b/a$a;->a()Landroid/support/b/a;

    move-result-object v1

    .line 116
    invoke-virtual {p0}, Lco/uk/getmondo/signup/documents/b;->getActivity()Landroid/support/v4/app/j;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-virtual {p0}, Lco/uk/getmondo/signup/documents/b;->g()Lco/uk/getmondo/api/model/signup/SignUpDocumentUrls;

    move-result-object v2

    if-nez v2, :cond_0

    invoke-static {}, Lkotlin/d/b/l;->a()V

    :cond_0
    invoke-virtual {v2}, Lco/uk/getmondo/api/model/signup/SignUpDocumentUrls;->e()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/support/b/a;->a(Landroid/content/Context;Landroid/net/Uri;)V

    .line 117
    return-void
.end method

.method public o()V
    .locals 2

    .prologue
    .line 120
    invoke-virtual {p0}, Lco/uk/getmondo/signup/documents/b;->getActivity()Landroid/support/v4/app/j;

    move-result-object v0

    if-nez v0, :cond_0

    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type co.uk.getmondo.signup.documents.LegalDocumentsFragment.StepListener"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    check-cast v0, Lco/uk/getmondo/signup/documents/b$a;

    invoke-interface {v0}, Lco/uk/getmondo/signup/documents/b$a;->c()V

    .line 121
    return-void
.end method

.method public onAttach(Landroid/content/Context;)V
    .locals 2

    .prologue
    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 31
    invoke-super {p0, p1}, Lco/uk/getmondo/common/f/a;->onAttach(Landroid/content/Context;)V

    .line 32
    instance-of v0, p1, Lco/uk/getmondo/signup/documents/b$a;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Activity must implement "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-class v1, Lco/uk/getmondo/signup/documents/b$a;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    .line 33
    :cond_0
    instance-of v0, p1, Lco/uk/getmondo/signup/i$a;

    if-nez v0, :cond_1

    .line 34
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Activity must implement "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-class v1, Lco/uk/getmondo/signup/i$a;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 33
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    .line 36
    :cond_1
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 26
    invoke-super {p0, p1}, Lco/uk/getmondo/common/f/a;->onCreate(Landroid/os/Bundle;)V

    .line 27
    invoke-virtual {p0}, Lco/uk/getmondo/signup/documents/b;->B()Lco/uk/getmondo/common/h/b/b;

    move-result-object v0

    invoke-interface {v0, p0}, Lco/uk/getmondo/common/h/b/b;->a(Lco/uk/getmondo/signup/documents/b;)V

    .line 28
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2

    .prologue
    const-string v0, "inflater"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 39
    const v0, 0x7f0500a1

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    const-string v1, "inflater.inflate(R.layou\u2026o_docs, container, false)"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public onDestroyView()V
    .locals 2

    .prologue
    .line 47
    iget-object v0, p0, Lco/uk/getmondo/signup/documents/b;->a:Lco/uk/getmondo/signup/documents/f;

    if-nez v0, :cond_0

    const-string v1, "presenter"

    invoke-static {v1}, Lkotlin/d/b/l;->b(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0}, Lco/uk/getmondo/signup/documents/f;->b()V

    .line 48
    invoke-super {p0}, Lco/uk/getmondo/common/f/a;->onDestroyView()V

    .line 49
    invoke-virtual {p0}, Lco/uk/getmondo/signup/documents/b;->p()V

    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 42
    invoke-super {p0, p1, p2}, Lco/uk/getmondo/common/f/a;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 43
    iget-object v0, p0, Lco/uk/getmondo/signup/documents/b;->a:Lco/uk/getmondo/signup/documents/f;

    if-nez v0, :cond_0

    const-string v1, "presenter"

    invoke-static {v1}, Lkotlin/d/b/l;->b(Ljava/lang/String;)V

    :cond_0
    check-cast p0, Lco/uk/getmondo/signup/documents/f$a;

    invoke-virtual {v0, p0}, Lco/uk/getmondo/signup/documents/f;->a(Lco/uk/getmondo/signup/documents/f$a;)V

    .line 44
    return-void
.end method

.method public p()V
    .locals 1

    iget-object v0, p0, Lco/uk/getmondo/signup/documents/b;->d:Ljava/util/HashMap;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lco/uk/getmondo/signup/documents/b;->d:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    :cond_0
    return-void
.end method
