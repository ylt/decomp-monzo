.class final Lco/uk/getmondo/signup/documents/f$g;
.super Ljava/lang/Object;
.source "LegalDocumentsPresenter.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lco/uk/getmondo/signup/documents/f;->a(Lco/uk/getmondo/signup/documents/f$a;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h",
        "<TT;",
        "Lio/reactivex/l",
        "<+TR;>;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u0003\u0010\u0000\u001a\u0008\u0012\u0004\u0012\u00020\u00020\u00012\u0006\u0010\u0003\u001a\u00020\u0002H\n\u00a2\u0006\u0004\u0008\u0004\u0010\u0005"
    }
    d2 = {
        "<anonymous>",
        "Lio/reactivex/Maybe;",
        "",
        "it",
        "apply",
        "(Lkotlin/Unit;)Lio/reactivex/Maybe;"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field final synthetic a:Lco/uk/getmondo/signup/documents/f;

.field final synthetic b:Lco/uk/getmondo/signup/documents/f$a;


# direct methods
.method constructor <init>(Lco/uk/getmondo/signup/documents/f;Lco/uk/getmondo/signup/documents/f$a;)V
    .locals 0

    iput-object p1, p0, Lco/uk/getmondo/signup/documents/f$g;->a:Lco/uk/getmondo/signup/documents/f;

    iput-object p2, p0, Lco/uk/getmondo/signup/documents/f$g;->b:Lco/uk/getmondo/signup/documents/f$a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lkotlin/n;)Lio/reactivex/h;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/n;",
            ")",
            "Lio/reactivex/h",
            "<",
            "Lkotlin/n;",
            ">;"
        }
    .end annotation

    .prologue
    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 68
    iget-object v0, p0, Lco/uk/getmondo/signup/documents/f$g;->b:Lco/uk/getmondo/signup/documents/f$a;

    invoke-interface {v0}, Lco/uk/getmondo/signup/documents/f$a;->g()Lco/uk/getmondo/api/model/signup/SignUpDocumentUrls;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-static {}, Lkotlin/d/b/l;->a()V

    :cond_0
    invoke-virtual {v0}, Lco/uk/getmondo/api/model/signup/SignUpDocumentUrls;->b()Ljava/lang/String;

    move-result-object v0

    .line 69
    iget-object v1, p0, Lco/uk/getmondo/signup/documents/f$g;->b:Lco/uk/getmondo/signup/documents/f$a;

    invoke-interface {v1}, Lco/uk/getmondo/signup/documents/f$a;->g()Lco/uk/getmondo/api/model/signup/SignUpDocumentUrls;

    move-result-object v1

    if-nez v1, :cond_1

    invoke-static {}, Lkotlin/d/b/l;->a()V

    :cond_1
    invoke-virtual {v1}, Lco/uk/getmondo/api/model/signup/SignUpDocumentUrls;->d()Ljava/lang/String;

    move-result-object v1

    .line 70
    iget-object v2, p0, Lco/uk/getmondo/signup/documents/f$g;->b:Lco/uk/getmondo/signup/documents/f$a;

    invoke-interface {v2}, Lco/uk/getmondo/signup/documents/f$a;->g()Lco/uk/getmondo/api/model/signup/SignUpDocumentUrls;

    move-result-object v2

    if-nez v2, :cond_2

    invoke-static {}, Lkotlin/d/b/l;->a()V

    :cond_2
    invoke-virtual {v2}, Lco/uk/getmondo/api/model/signup/SignUpDocumentUrls;->f()Ljava/lang/String;

    move-result-object v2

    .line 71
    iget-object v3, p0, Lco/uk/getmondo/signup/documents/f$g;->a:Lco/uk/getmondo/signup/documents/f;

    invoke-static {v3}, Lco/uk/getmondo/signup/documents/f;->a(Lco/uk/getmondo/signup/documents/f;)Lco/uk/getmondo/signup/documents/d;

    move-result-object v3

    invoke-virtual {v3, v0, v1, v2}, Lco/uk/getmondo/signup/documents/d;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lio/reactivex/b;

    move-result-object v0

    .line 72
    iget-object v1, p0, Lco/uk/getmondo/signup/documents/f$g;->a:Lco/uk/getmondo/signup/documents/f;

    invoke-static {v1}, Lco/uk/getmondo/signup/documents/f;->b(Lco/uk/getmondo/signup/documents/f;)Lio/reactivex/u;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/b;->b(Lio/reactivex/u;)Lio/reactivex/b;

    move-result-object v0

    .line 73
    iget-object v1, p0, Lco/uk/getmondo/signup/documents/f$g;->a:Lco/uk/getmondo/signup/documents/f;

    invoke-static {v1}, Lco/uk/getmondo/signup/documents/f;->c(Lco/uk/getmondo/signup/documents/f;)Lio/reactivex/u;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/b;->a(Lio/reactivex/u;)Lio/reactivex/b;

    move-result-object v1

    .line 74
    new-instance v0, Lco/uk/getmondo/signup/documents/f$g$1;

    invoke-direct {v0, p0}, Lco/uk/getmondo/signup/documents/f$g$1;-><init>(Lco/uk/getmondo/signup/documents/f$g;)V

    check-cast v0, Lio/reactivex/c/g;

    invoke-virtual {v1, v0}, Lio/reactivex/b;->c(Lio/reactivex/c/g;)Lio/reactivex/b;

    move-result-object v1

    .line 75
    new-instance v0, Lco/uk/getmondo/signup/documents/f$g$2;

    invoke-direct {v0, p0}, Lco/uk/getmondo/signup/documents/f$g$2;-><init>(Lco/uk/getmondo/signup/documents/f$g;)V

    check-cast v0, Lio/reactivex/c/g;

    invoke-virtual {v1, v0}, Lio/reactivex/b;->a(Lio/reactivex/c/g;)Lio/reactivex/b;

    move-result-object v0

    .line 76
    sget-object v1, Lkotlin/n;->a:Lkotlin/n;

    invoke-static {v0, v1}, Lco/uk/getmondo/common/j/f;->a(Lio/reactivex/b;Ljava/lang/Object;)Lio/reactivex/h;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 20
    check-cast p1, Lkotlin/n;

    invoke-virtual {p0, p1}, Lco/uk/getmondo/signup/documents/f$g;->a(Lkotlin/n;)Lio/reactivex/h;

    move-result-object v0

    return-object v0
.end method
