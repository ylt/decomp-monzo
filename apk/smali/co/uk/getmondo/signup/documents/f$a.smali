.class public interface abstract Lco/uk/getmondo/signup/documents/f$a;
.super Ljava/lang/Object;
.source "LegalDocumentsPresenter.kt"

# interfaces
.implements Lco/uk/getmondo/common/e/a$a;
.implements Lco/uk/getmondo/common/ui/f;
.implements Lco/uk/getmondo/signup/i$a;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lco/uk/getmondo/signup/documents/f;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "a"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u000b\n\u0002\u0018\u0002\n\u0002\u0008\r\u0008f\u0018\u00002\u00020\u00012\u00020\u00022\u00020\u0003J\u0008\u0010\u0017\u001a\u00020\u0006H&J\u0008\u0010\u0018\u001a\u00020\u0006H&J\u0008\u0010\u0019\u001a\u00020\u0006H&J\u0008\u0010\u001a\u001a\u00020\u0006H&J\u0008\u0010\u001b\u001a\u00020\u0006H&J\u0008\u0010\u001c\u001a\u00020\u0006H&J\u0008\u0010\u001d\u001a\u00020\u0006H&J\u0008\u0010\u001e\u001a\u00020\u0006H&R\u0018\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0005X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0007\u0010\u0008R\u0018\u0010\t\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0005X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\n\u0010\u0008R\u0018\u0010\u000b\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0005X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u000c\u0010\u0008R\u0018\u0010\r\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0005X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u000e\u0010\u0008R\u0018\u0010\u000f\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0005X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0010\u0010\u0008R\u001a\u0010\u0011\u001a\u0004\u0018\u00010\u0012X\u00a6\u000e\u00a2\u0006\u000c\u001a\u0004\u0008\u0013\u0010\u0014\"\u0004\u0008\u0015\u0010\u0016\u00a8\u0006\u001f"
    }
    d2 = {
        "Lco/uk/getmondo/signup/documents/LegalDocumentsPresenter$View;",
        "Lco/uk/getmondo/common/ui/MvpView;",
        "Lco/uk/getmondo/common/errors/ApiErrorHandler$ApiErrorView;",
        "Lco/uk/getmondo/signup/SignupApiErrorHandler$View;",
        "onContinueClicked",
        "Lio/reactivex/Observable;",
        "",
        "getOnContinueClicked",
        "()Lio/reactivex/Observable;",
        "onFscsProtectionClicked",
        "getOnFscsProtectionClicked",
        "onPrivacyPolicyClicked",
        "getOnPrivacyPolicyClicked",
        "onRefreshUrlsClicked",
        "getOnRefreshUrlsClicked",
        "onTermsAndConditionsClicked",
        "getOnTermsAndConditionsClicked",
        "urls",
        "Lco/uk/getmondo/api/model/signup/SignUpDocumentUrls;",
        "getUrls",
        "()Lco/uk/getmondo/api/model/signup/SignUpDocumentUrls;",
        "setUrls",
        "(Lco/uk/getmondo/api/model/signup/SignUpDocumentUrls;)V",
        "completeStage",
        "hideAcceptLoading",
        "hideUrlsLoading",
        "openFscsProtection",
        "openPrivacyPolicy",
        "openTermsAndConditions",
        "showAcceptLoading",
        "showUrlsLoading",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# virtual methods
.method public abstract a()Lio/reactivex/n;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Lkotlin/n;",
            ">;"
        }
    .end annotation
.end method

.method public abstract a(Lco/uk/getmondo/api/model/signup/SignUpDocumentUrls;)V
.end method

.method public abstract c()Lio/reactivex/n;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Lkotlin/n;",
            ">;"
        }
    .end annotation
.end method

.method public abstract d()Lio/reactivex/n;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Lkotlin/n;",
            ">;"
        }
    .end annotation
.end method

.method public abstract e()Lio/reactivex/n;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Lkotlin/n;",
            ">;"
        }
    .end annotation
.end method

.method public abstract f()Lio/reactivex/n;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Lkotlin/n;",
            ">;"
        }
    .end annotation
.end method

.method public abstract g()Lco/uk/getmondo/api/model/signup/SignUpDocumentUrls;
.end method

.method public abstract h()V
.end method

.method public abstract i()V
.end method

.method public abstract j()V
.end method

.method public abstract k()V
.end method

.method public abstract l()V
.end method

.method public abstract m()V
.end method

.method public abstract n()V
.end method

.method public abstract o()V
.end method
