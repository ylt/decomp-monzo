.class final Lco/uk/getmondo/signup/card_ordering/g$h;
.super Ljava/lang/Object;
.source "ConfirmCardDetailsPresenter.kt"

# interfaces
.implements Lio/reactivex/c/g;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lco/uk/getmondo/signup/card_ordering/g;->a(Lco/uk/getmondo/signup/card_ordering/g$a;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/g",
        "<",
        "Lkotlin/n;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000\n\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0004\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0003*\u0004\u0018\u00010\u00010\u0001H\n\u00a2\u0006\u0004\u0008\u0004\u0010\u0005"
    }
    d2 = {
        "<anonymous>",
        "",
        "it",
        "kotlin.jvm.PlatformType",
        "accept",
        "(Lkotlin/Unit;)V"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field final synthetic a:Lco/uk/getmondo/signup/card_ordering/g;

.field final synthetic b:Lco/uk/getmondo/signup/card_ordering/g$a;


# direct methods
.method constructor <init>(Lco/uk/getmondo/signup/card_ordering/g;Lco/uk/getmondo/signup/card_ordering/g$a;)V
    .locals 0

    iput-object p1, p0, Lco/uk/getmondo/signup/card_ordering/g$h;->a:Lco/uk/getmondo/signup/card_ordering/g;

    iput-object p2, p0, Lco/uk/getmondo/signup/card_ordering/g$h;->b:Lco/uk/getmondo/signup/card_ordering/g$a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 20
    check-cast p1, Lkotlin/n;

    invoke-virtual {p0, p1}, Lco/uk/getmondo/signup/card_ordering/g$h;->a(Lkotlin/n;)V

    return-void
.end method

.method public final a(Lkotlin/n;)V
    .locals 4

    .prologue
    .line 85
    iget-object v0, p0, Lco/uk/getmondo/signup/card_ordering/g$h;->a:Lco/uk/getmondo/signup/card_ordering/g;

    invoke-static {v0}, Lco/uk/getmondo/signup/card_ordering/g;->f(Lco/uk/getmondo/signup/card_ordering/g;)Lco/uk/getmondo/api/model/order_card/CardOrderName$Type;

    move-result-object v0

    sget-object v1, Lco/uk/getmondo/api/model/order_card/CardOrderName$Type;->LEGAL:Lco/uk/getmondo/api/model/order_card/CardOrderName$Type;

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lco/uk/getmondo/signup/card_ordering/g$h;->a:Lco/uk/getmondo/signup/card_ordering/g;

    invoke-static {v0}, Lco/uk/getmondo/signup/card_ordering/g;->e(Lco/uk/getmondo/signup/card_ordering/g;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-static {}, Lkotlin/d/b/l;->a()V

    .line 86
    :cond_0
    :goto_0
    iget-object v1, p0, Lco/uk/getmondo/signup/card_ordering/g$h;->a:Lco/uk/getmondo/signup/card_ordering/g;

    invoke-static {v1}, Lco/uk/getmondo/signup/card_ordering/g;->i(Lco/uk/getmondo/signup/card_ordering/g;)Lco/uk/getmondo/common/a;

    move-result-object v1

    sget-object v2, Lco/uk/getmondo/api/model/tracking/Impression;->Companion:Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    iget-object v3, p0, Lco/uk/getmondo/signup/card_ordering/g$h;->a:Lco/uk/getmondo/signup/card_ordering/g;

    invoke-static {v3}, Lco/uk/getmondo/signup/card_ordering/g;->f(Lco/uk/getmondo/signup/card_ordering/g;)Lco/uk/getmondo/api/model/order_card/CardOrderName$Type;

    move-result-object v3

    invoke-virtual {v3}, Lco/uk/getmondo/api/model/order_card/CardOrderName$Type;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->p(Ljava/lang/String;)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v2

    invoke-virtual {v1, v2}, Lco/uk/getmondo/common/a;->a(Lco/uk/getmondo/api/model/tracking/Impression;)V

    .line 87
    iget-object v1, p0, Lco/uk/getmondo/signup/card_ordering/g$h;->b:Lco/uk/getmondo/signup/card_ordering/g$a;

    iget-object v2, p0, Lco/uk/getmondo/signup/card_ordering/g$h;->a:Lco/uk/getmondo/signup/card_ordering/g;

    invoke-static {v2}, Lco/uk/getmondo/signup/card_ordering/g;->f(Lco/uk/getmondo/signup/card_ordering/g;)Lco/uk/getmondo/api/model/order_card/CardOrderName$Type;

    move-result-object v2

    invoke-interface {v1, v2, v0}, Lco/uk/getmondo/signup/card_ordering/g$a;->a(Lco/uk/getmondo/api/model/order_card/CardOrderName$Type;Ljava/lang/String;)V

    .line 88
    return-void

    .line 85
    :cond_1
    iget-object v0, p0, Lco/uk/getmondo/signup/card_ordering/g$h;->a:Lco/uk/getmondo/signup/card_ordering/g;

    invoke-static {v0}, Lco/uk/getmondo/signup/card_ordering/g;->g(Lco/uk/getmondo/signup/card_ordering/g;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-static {}, Lkotlin/d/b/l;->a()V

    goto :goto_0
.end method
