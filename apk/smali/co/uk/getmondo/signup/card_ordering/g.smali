.class public final Lco/uk/getmondo/signup/card_ordering/g;
.super Lco/uk/getmondo/common/ui/b;
.source "ConfirmCardDetailsPresenter.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lco/uk/getmondo/signup/card_ordering/g$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lco/uk/getmondo/common/ui/b",
        "<",
        "Lco/uk/getmondo/signup/card_ordering/g$a;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000F\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0005\u0008\u0007\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00020\u0001:\u0001\u0019B3\u0008\u0007\u0012\u0008\u0008\u0001\u0010\u0003\u001a\u00020\u0004\u0012\u0008\u0008\u0001\u0010\u0005\u001a\u00020\u0004\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b\u00a2\u0006\u0002\u0010\u000cJ\u0010\u0010\u0014\u001a\u00020\u00152\u0006\u0010\u0016\u001a\u00020\u0002H\u0016J\u0010\u0010\u0017\u001a\u00020\u00152\u0006\u0010\u0016\u001a\u00020\u0002H\u0002J\u0010\u0010\u0018\u001a\u00020\u00152\u0006\u0010\u0016\u001a\u00020\u0002H\u0002R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u000eX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u000f\u001a\u0004\u0018\u00010\u0010X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0011\u001a\u0004\u0018\u00010\u0010X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0012\u001a\u00020\u0013X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001a"
    }
    d2 = {
        "Lco/uk/getmondo/signup/card_ordering/ConfirmCardDetailsPresenter;",
        "Lco/uk/getmondo/common/ui/BasePresenter;",
        "Lco/uk/getmondo/signup/card_ordering/ConfirmCardDetailsPresenter$View;",
        "ioScheduler",
        "Lio/reactivex/Scheduler;",
        "uiScheduler",
        "orderCardManager",
        "Lco/uk/getmondo/signup/card_ordering/OrderCardManager;",
        "apiErrorHandler",
        "Lco/uk/getmondo/common/errors/ApiErrorHandler;",
        "analyticsService",
        "Lco/uk/getmondo/common/AnalyticsService;",
        "(Lio/reactivex/Scheduler;Lio/reactivex/Scheduler;Lco/uk/getmondo/signup/card_ordering/OrderCardManager;Lco/uk/getmondo/common/errors/ApiErrorHandler;Lco/uk/getmondo/common/AnalyticsService;)V",
        "canChooseName",
        "",
        "legalName",
        "",
        "preferredName",
        "selectedNameType",
        "Lco/uk/getmondo/api/model/order_card/CardOrderName$Type;",
        "register",
        "",
        "view",
        "setLegalNameSelected",
        "setPreferredNameSelected",
        "View",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field private c:Ljava/lang/String;

.field private d:Ljava/lang/String;

.field private e:Z

.field private f:Lco/uk/getmondo/api/model/order_card/CardOrderName$Type;

.field private final g:Lio/reactivex/u;

.field private final h:Lio/reactivex/u;

.field private final i:Lco/uk/getmondo/signup/card_ordering/k;

.field private final j:Lco/uk/getmondo/common/e/a;

.field private final k:Lco/uk/getmondo/common/a;


# direct methods
.method public constructor <init>(Lio/reactivex/u;Lio/reactivex/u;Lco/uk/getmondo/signup/card_ordering/k;Lco/uk/getmondo/common/e/a;Lco/uk/getmondo/common/a;)V
    .locals 1

    .prologue
    const-string v0, "ioScheduler"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "uiScheduler"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "orderCardManager"

    invoke-static {p3, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "apiErrorHandler"

    invoke-static {p4, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "analyticsService"

    invoke-static {p5, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 27
    invoke-direct {p0}, Lco/uk/getmondo/common/ui/b;-><init>()V

    iput-object p1, p0, Lco/uk/getmondo/signup/card_ordering/g;->g:Lio/reactivex/u;

    iput-object p2, p0, Lco/uk/getmondo/signup/card_ordering/g;->h:Lio/reactivex/u;

    iput-object p3, p0, Lco/uk/getmondo/signup/card_ordering/g;->i:Lco/uk/getmondo/signup/card_ordering/k;

    iput-object p4, p0, Lco/uk/getmondo/signup/card_ordering/g;->j:Lco/uk/getmondo/common/e/a;

    iput-object p5, p0, Lco/uk/getmondo/signup/card_ordering/g;->k:Lco/uk/getmondo/common/a;

    return-void
.end method

.method public static final synthetic a(Lco/uk/getmondo/signup/card_ordering/g;)Lco/uk/getmondo/signup/card_ordering/k;
    .locals 1

    .prologue
    .line 20
    iget-object v0, p0, Lco/uk/getmondo/signup/card_ordering/g;->i:Lco/uk/getmondo/signup/card_ordering/k;

    return-object v0
.end method

.method public static final synthetic a(Lco/uk/getmondo/signup/card_ordering/g;Lco/uk/getmondo/api/model/order_card/CardOrderName$Type;)V
    .locals 0

    .prologue
    .line 20
    iput-object p1, p0, Lco/uk/getmondo/signup/card_ordering/g;->f:Lco/uk/getmondo/api/model/order_card/CardOrderName$Type;

    return-void
.end method

.method public static final synthetic a(Lco/uk/getmondo/signup/card_ordering/g;Lco/uk/getmondo/signup/card_ordering/g$a;)V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0, p1}, Lco/uk/getmondo/signup/card_ordering/g;->c(Lco/uk/getmondo/signup/card_ordering/g$a;)V

    return-void
.end method

.method public static final synthetic a(Lco/uk/getmondo/signup/card_ordering/g;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 20
    iput-object p1, p0, Lco/uk/getmondo/signup/card_ordering/g;->c:Ljava/lang/String;

    return-void
.end method

.method public static final synthetic a(Lco/uk/getmondo/signup/card_ordering/g;Z)V
    .locals 0

    .prologue
    .line 20
    iput-boolean p1, p0, Lco/uk/getmondo/signup/card_ordering/g;->e:Z

    return-void
.end method

.method public static final synthetic b(Lco/uk/getmondo/signup/card_ordering/g;)Lio/reactivex/u;
    .locals 1

    .prologue
    .line 20
    iget-object v0, p0, Lco/uk/getmondo/signup/card_ordering/g;->g:Lio/reactivex/u;

    return-object v0
.end method

.method private final b(Lco/uk/getmondo/signup/card_ordering/g$a;)V
    .locals 1

    .prologue
    .line 92
    sget-object v0, Lco/uk/getmondo/api/model/order_card/CardOrderName$Type;->PREFERRED:Lco/uk/getmondo/api/model/order_card/CardOrderName$Type;

    iput-object v0, p0, Lco/uk/getmondo/signup/card_ordering/g;->f:Lco/uk/getmondo/api/model/order_card/CardOrderName$Type;

    .line 93
    iget-object v0, p0, Lco/uk/getmondo/signup/card_ordering/g;->d:Ljava/lang/String;

    if-nez v0, :cond_0

    invoke-static {}, Lkotlin/d/b/l;->a()V

    :cond_0
    invoke-interface {p1, v0}, Lco/uk/getmondo/signup/card_ordering/g$a;->f(Ljava/lang/String;)V

    .line 94
    return-void
.end method

.method public static final synthetic b(Lco/uk/getmondo/signup/card_ordering/g;Lco/uk/getmondo/signup/card_ordering/g$a;)V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0, p1}, Lco/uk/getmondo/signup/card_ordering/g;->b(Lco/uk/getmondo/signup/card_ordering/g$a;)V

    return-void
.end method

.method public static final synthetic b(Lco/uk/getmondo/signup/card_ordering/g;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 20
    iput-object p1, p0, Lco/uk/getmondo/signup/card_ordering/g;->d:Ljava/lang/String;

    return-void
.end method

.method public static final synthetic c(Lco/uk/getmondo/signup/card_ordering/g;)Lio/reactivex/u;
    .locals 1

    .prologue
    .line 20
    iget-object v0, p0, Lco/uk/getmondo/signup/card_ordering/g;->h:Lio/reactivex/u;

    return-object v0
.end method

.method private final c(Lco/uk/getmondo/signup/card_ordering/g$a;)V
    .locals 1

    .prologue
    .line 97
    sget-object v0, Lco/uk/getmondo/api/model/order_card/CardOrderName$Type;->LEGAL:Lco/uk/getmondo/api/model/order_card/CardOrderName$Type;

    iput-object v0, p0, Lco/uk/getmondo/signup/card_ordering/g;->f:Lco/uk/getmondo/api/model/order_card/CardOrderName$Type;

    .line 98
    iget-object v0, p0, Lco/uk/getmondo/signup/card_ordering/g;->c:Ljava/lang/String;

    if-nez v0, :cond_0

    invoke-static {}, Lkotlin/d/b/l;->a()V

    :cond_0
    invoke-interface {p1, v0}, Lco/uk/getmondo/signup/card_ordering/g$a;->e(Ljava/lang/String;)V

    .line 99
    return-void
.end method

.method public static final synthetic d(Lco/uk/getmondo/signup/card_ordering/g;)Lco/uk/getmondo/common/e/a;
    .locals 1

    .prologue
    .line 20
    iget-object v0, p0, Lco/uk/getmondo/signup/card_ordering/g;->j:Lco/uk/getmondo/common/e/a;

    return-object v0
.end method

.method public static final synthetic e(Lco/uk/getmondo/signup/card_ordering/g;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 20
    iget-object v0, p0, Lco/uk/getmondo/signup/card_ordering/g;->c:Ljava/lang/String;

    return-object v0
.end method

.method public static final synthetic f(Lco/uk/getmondo/signup/card_ordering/g;)Lco/uk/getmondo/api/model/order_card/CardOrderName$Type;
    .locals 2

    .prologue
    .line 20
    iget-object v0, p0, Lco/uk/getmondo/signup/card_ordering/g;->f:Lco/uk/getmondo/api/model/order_card/CardOrderName$Type;

    if-nez v0, :cond_0

    const-string v1, "selectedNameType"

    invoke-static {v1}, Lkotlin/d/b/l;->b(Ljava/lang/String;)V

    :cond_0
    return-object v0
.end method

.method public static final synthetic g(Lco/uk/getmondo/signup/card_ordering/g;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 20
    iget-object v0, p0, Lco/uk/getmondo/signup/card_ordering/g;->d:Ljava/lang/String;

    return-object v0
.end method

.method public static final synthetic h(Lco/uk/getmondo/signup/card_ordering/g;)Z
    .locals 1

    .prologue
    .line 20
    iget-boolean v0, p0, Lco/uk/getmondo/signup/card_ordering/g;->e:Z

    return v0
.end method

.method public static final synthetic i(Lco/uk/getmondo/signup/card_ordering/g;)Lco/uk/getmondo/common/a;
    .locals 1

    .prologue
    .line 20
    iget-object v0, p0, Lco/uk/getmondo/signup/card_ordering/g;->k:Lco/uk/getmondo/common/a;

    return-object v0
.end method


# virtual methods
.method public bridge synthetic a(Lco/uk/getmondo/common/ui/f;)V
    .locals 0

    .prologue
    .line 20
    check-cast p1, Lco/uk/getmondo/signup/card_ordering/g$a;

    invoke-virtual {p0, p1}, Lco/uk/getmondo/signup/card_ordering/g;->a(Lco/uk/getmondo/signup/card_ordering/g$a;)V

    return-void
.end method

.method public a(Lco/uk/getmondo/signup/card_ordering/g$a;)V
    .locals 3

    .prologue
    const-string v0, "view"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v0, p1

    .line 35
    check-cast v0, Lco/uk/getmondo/common/ui/f;

    invoke-super {p0, v0}, Lco/uk/getmondo/common/ui/b;->a(Lco/uk/getmondo/common/ui/f;)V

    .line 36
    iget-object v0, p0, Lco/uk/getmondo/signup/card_ordering/g;->k:Lco/uk/getmondo/common/a;

    sget-object v1, Lco/uk/getmondo/api/model/tracking/Impression;->Companion:Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    invoke-virtual {v1}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->aQ()Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v1

    invoke-virtual {v0, v1}, Lco/uk/getmondo/common/a;->a(Lco/uk/getmondo/api/model/tracking/Impression;)V

    .line 37
    iget-object v1, p0, Lco/uk/getmondo/signup/card_ordering/g;->b:Lio/reactivex/b/a;

    .line 52
    invoke-interface {p1}, Lco/uk/getmondo/signup/card_ordering/g$a;->a()Lio/reactivex/n;

    move-result-object v0

    .line 38
    sget-object v2, Lkotlin/n;->a:Lkotlin/n;

    invoke-virtual {v0, v2}, Lio/reactivex/n;->startWith(Ljava/lang/Object;)Lio/reactivex/n;

    move-result-object v2

    .line 39
    new-instance v0, Lco/uk/getmondo/signup/card_ordering/g$b;

    invoke-direct {v0, p0, p1}, Lco/uk/getmondo/signup/card_ordering/g$b;-><init>(Lco/uk/getmondo/signup/card_ordering/g;Lco/uk/getmondo/signup/card_ordering/g$a;)V

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {v2, v0}, Lio/reactivex/n;->flatMapMaybe(Lio/reactivex/c/h;)Lio/reactivex/n;

    move-result-object v2

    .line 52
    new-instance v0, Lco/uk/getmondo/signup/card_ordering/g$c;

    invoke-direct {v0, p0, p1}, Lco/uk/getmondo/signup/card_ordering/g$c;-><init>(Lco/uk/getmondo/signup/card_ordering/g;Lco/uk/getmondo/signup/card_ordering/g$a;)V

    check-cast v0, Lio/reactivex/c/g;

    invoke-virtual {v2, v0}, Lio/reactivex/n;->subscribe(Lio/reactivex/c/g;)Lio/reactivex/b/b;

    move-result-object v0

    const-string v2, "view.onRetryClicked\n    \u2026Line())\n                }"

    invoke-static {v0, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 37
    invoke-static {v1, v0}, Lco/uk/getmondo/common/j/f;->a(Lio/reactivex/b/a;Lio/reactivex/b/b;)Lio/reactivex/b/a;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/signup/card_ordering/g;->b:Lio/reactivex/b/a;

    .line 75
    iget-object v1, p0, Lco/uk/getmondo/signup/card_ordering/g;->b:Lio/reactivex/b/a;

    .line 77
    invoke-interface {p1}, Lco/uk/getmondo/signup/card_ordering/g$a;->d()Lio/reactivex/n;

    move-result-object v2

    .line 76
    new-instance v0, Lco/uk/getmondo/signup/card_ordering/g$d;

    invoke-direct {v0, p0}, Lco/uk/getmondo/signup/card_ordering/g$d;-><init>(Lco/uk/getmondo/signup/card_ordering/g;)V

    check-cast v0, Lio/reactivex/c/q;

    invoke-virtual {v2, v0}, Lio/reactivex/n;->skipWhile(Lio/reactivex/c/q;)Lio/reactivex/n;

    move-result-object v2

    .line 77
    new-instance v0, Lco/uk/getmondo/signup/card_ordering/g$e;

    invoke-direct {v0, p0, p1}, Lco/uk/getmondo/signup/card_ordering/g$e;-><init>(Lco/uk/getmondo/signup/card_ordering/g;Lco/uk/getmondo/signup/card_ordering/g$a;)V

    check-cast v0, Lio/reactivex/c/g;

    invoke-virtual {v2, v0}, Lio/reactivex/n;->subscribe(Lio/reactivex/c/g;)Lio/reactivex/b/b;

    move-result-object v0

    const-string v2, "view.onLegalNameSelected\u2026LegalNameSelected(view) }"

    invoke-static {v0, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 75
    invoke-static {v1, v0}, Lco/uk/getmondo/common/j/f;->a(Lio/reactivex/b/a;Lio/reactivex/b/b;)Lio/reactivex/b/a;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/signup/card_ordering/g;->b:Lio/reactivex/b/a;

    .line 79
    iget-object v1, p0, Lco/uk/getmondo/signup/card_ordering/g;->b:Lio/reactivex/b/a;

    .line 81
    invoke-interface {p1}, Lco/uk/getmondo/signup/card_ordering/g$a;->e()Lio/reactivex/n;

    move-result-object v2

    .line 80
    new-instance v0, Lco/uk/getmondo/signup/card_ordering/g$f;

    invoke-direct {v0, p0}, Lco/uk/getmondo/signup/card_ordering/g$f;-><init>(Lco/uk/getmondo/signup/card_ordering/g;)V

    check-cast v0, Lio/reactivex/c/q;

    invoke-virtual {v2, v0}, Lio/reactivex/n;->skipWhile(Lio/reactivex/c/q;)Lio/reactivex/n;

    move-result-object v2

    .line 81
    new-instance v0, Lco/uk/getmondo/signup/card_ordering/g$g;

    invoke-direct {v0, p0, p1}, Lco/uk/getmondo/signup/card_ordering/g$g;-><init>(Lco/uk/getmondo/signup/card_ordering/g;Lco/uk/getmondo/signup/card_ordering/g$a;)V

    check-cast v0, Lio/reactivex/c/g;

    invoke-virtual {v2, v0}, Lio/reactivex/n;->subscribe(Lio/reactivex/c/g;)Lio/reactivex/b/b;

    move-result-object v0

    const-string v2, "view.onPreferredNameSele\u2026erredNameSelected(view) }"

    invoke-static {v0, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 79
    invoke-static {v1, v0}, Lco/uk/getmondo/common/j/f;->a(Lio/reactivex/b/a;Lio/reactivex/b/b;)Lio/reactivex/b/a;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/signup/card_ordering/g;->b:Lio/reactivex/b/a;

    .line 83
    iget-object v1, p0, Lco/uk/getmondo/signup/card_ordering/g;->b:Lio/reactivex/b/a;

    .line 84
    invoke-interface {p1}, Lco/uk/getmondo/signup/card_ordering/g$a;->c()Lio/reactivex/n;

    move-result-object v2

    new-instance v0, Lco/uk/getmondo/signup/card_ordering/g$h;

    invoke-direct {v0, p0, p1}, Lco/uk/getmondo/signup/card_ordering/g$h;-><init>(Lco/uk/getmondo/signup/card_ordering/g;Lco/uk/getmondo/signup/card_ordering/g$a;)V

    check-cast v0, Lio/reactivex/c/g;

    invoke-virtual {v2, v0}, Lio/reactivex/n;->subscribe(Lio/reactivex/c/g;)Lio/reactivex/b/b;

    move-result-object v0

    const-string v2, "view.onSendCardClicked\n \u2026OnCard)\n                }"

    invoke-static {v0, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 83
    invoke-static {v1, v0}, Lco/uk/getmondo/common/j/f;->a(Lio/reactivex/b/a;Lio/reactivex/b/b;)Lio/reactivex/b/a;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/signup/card_ordering/g;->b:Lio/reactivex/b/a;

    .line 89
    return-void
.end method
