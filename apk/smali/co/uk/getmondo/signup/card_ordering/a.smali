.class public final Lco/uk/getmondo/signup/card_ordering/a;
.super Lco/uk/getmondo/common/f/a;
.source "ChoosePinFragment.kt"

# interfaces
.implements Lco/uk/getmondo/signup/card_ordering/c$a;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lco/uk/getmondo/signup/card_ordering/a$b;,
        Lco/uk/getmondo/signup/card_ordering/a$a;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000Z\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0002\u0008\u0006\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0008\n\u0002\u0010\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u000b\u0018\u0000 62\u00020\u00012\u00020\u0002:\u000267B\u0005\u00a2\u0006\u0002\u0010\u0003J\u0008\u0010\u001d\u001a\u00020\u001eH\u0016J\u0008\u0010\u001f\u001a\u00020\u001eH\u0016J\u0008\u0010 \u001a\u00020\u001eH\u0016J\u0008\u0010!\u001a\u00020\u001eH\u0016J\u0010\u0010\"\u001a\u00020\u001e2\u0006\u0010#\u001a\u00020$H\u0016J\u0012\u0010%\u001a\u00020\u001e2\u0008\u0010&\u001a\u0004\u0018\u00010\'H\u0016J$\u0010(\u001a\u00020)2\u0006\u0010*\u001a\u00020+2\u0008\u0010,\u001a\u0004\u0018\u00010-2\u0008\u0010&\u001a\u0004\u0018\u00010\'H\u0016J\u0008\u0010.\u001a\u00020\u001eH\u0016J\u0010\u0010/\u001a\u00020\u001e2\u0006\u00100\u001a\u00020\u0005H\u0016J\u001a\u00101\u001a\u00020\u001e2\u0006\u00102\u001a\u00020)2\u0008\u0010&\u001a\u0004\u0018\u00010\'H\u0016J\u0008\u00103\u001a\u00020\u001eH\u0016J\u0008\u00104\u001a\u00020\u001eH\u0016J\u0008\u00105\u001a\u00020\u001eH\u0016R#\u0010\u0004\u001a\n \u0006*\u0004\u0018\u00010\u00050\u00058BX\u0082\u0084\u0002\u00a2\u0006\u000c\n\u0004\u0008\t\u0010\n\u001a\u0004\u0008\u0007\u0010\u0008R\u001b\u0010\u000b\u001a\u00020\u000c8VX\u0096\u0084\u0002\u00a2\u0006\u000c\n\u0004\u0008\u000f\u0010\n\u001a\u0004\u0008\r\u0010\u000eR\u001a\u0010\u0010\u001a\u0008\u0012\u0004\u0012\u00020\u00050\u00118VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0012\u0010\u0013R\u001e\u0010\u0014\u001a\u00020\u00158\u0006@\u0006X\u0087.\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u0016\u0010\u0017\"\u0004\u0008\u0018\u0010\u0019R\u001d\u0010\u001a\u001a\u0004\u0018\u00010\u00058VX\u0096\u0084\u0002\u00a2\u0006\u000c\n\u0004\u0008\u001c\u0010\n\u001a\u0004\u0008\u001b\u0010\u0008\u00a8\u00068"
    }
    d2 = {
        "Lco/uk/getmondo/signup/card_ordering/ChoosePinFragment;",
        "Lco/uk/getmondo/common/fragments/BaseFragment;",
        "Lco/uk/getmondo/signup/card_ordering/ChoosePinPresenter$View;",
        "()V",
        "nameOnCard",
        "",
        "kotlin.jvm.PlatformType",
        "getNameOnCard",
        "()Ljava/lang/String;",
        "nameOnCard$delegate",
        "Lkotlin/Lazy;",
        "nameType",
        "Lco/uk/getmondo/api/model/order_card/CardOrderName$Type;",
        "getNameType",
        "()Lco/uk/getmondo/api/model/order_card/CardOrderName$Type;",
        "nameType$delegate",
        "onPinEntered",
        "Lio/reactivex/Observable;",
        "getOnPinEntered",
        "()Lio/reactivex/Observable;",
        "presenter",
        "Lco/uk/getmondo/signup/card_ordering/ChoosePinPresenter;",
        "getPresenter",
        "()Lco/uk/getmondo/signup/card_ordering/ChoosePinPresenter;",
        "setPresenter",
        "(Lco/uk/getmondo/signup/card_ordering/ChoosePinPresenter;)V",
        "previousPin",
        "getPreviousPin",
        "previousPin$delegate",
        "cardOrdered",
        "",
        "clearPin",
        "hideKeyboard",
        "hideLoading",
        "onAttach",
        "context",
        "Landroid/content/Context;",
        "onCreate",
        "savedInstanceState",
        "Landroid/os/Bundle;",
        "onCreateView",
        "Landroid/view/View;",
        "inflater",
        "Landroid/view/LayoutInflater;",
        "container",
        "Landroid/view/ViewGroup;",
        "onDestroyView",
        "onPinEnteredFirstTime",
        "pin",
        "onViewCreated",
        "view",
        "reloadSignupStatus",
        "showKeyboard",
        "showLoading",
        "Companion",
        "StepListener",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# static fields
.field static final synthetic a:[Lkotlin/reflect/l;

.field public static final d:Lco/uk/getmondo/signup/card_ordering/a$a;


# instance fields
.field public c:Lco/uk/getmondo/signup/card_ordering/c;

.field private final e:Lkotlin/c;

.field private final f:Lkotlin/c;

.field private final g:Lkotlin/c;

.field private h:Ljava/util/HashMap;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const/4 v0, 0x3

    new-array v1, v0, [Lkotlin/reflect/l;

    const/4 v2, 0x0

    new-instance v0, Lkotlin/d/b/w;

    const-class v3, Lco/uk/getmondo/signup/card_ordering/a;

    invoke-static {v3}, Lkotlin/d/b/y;->a(Ljava/lang/Class;)Lkotlin/reflect/c;

    move-result-object v3

    const-string v4, "nameOnCard"

    const-string v5, "getNameOnCard()Ljava/lang/String;"

    invoke-direct {v0, v3, v4, v5}, Lkotlin/d/b/w;-><init>(Lkotlin/reflect/e;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v0}, Lkotlin/d/b/y;->a(Lkotlin/d/b/v;)Lkotlin/reflect/n;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/l;

    aput-object v0, v1, v2

    const/4 v2, 0x1

    new-instance v0, Lkotlin/d/b/w;

    const-class v3, Lco/uk/getmondo/signup/card_ordering/a;

    invoke-static {v3}, Lkotlin/d/b/y;->a(Ljava/lang/Class;)Lkotlin/reflect/c;

    move-result-object v3

    const-string v4, "nameType"

    const-string v5, "getNameType()Lco/uk/getmondo/api/model/order_card/CardOrderName$Type;"

    invoke-direct {v0, v3, v4, v5}, Lkotlin/d/b/w;-><init>(Lkotlin/reflect/e;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v0}, Lkotlin/d/b/y;->a(Lkotlin/d/b/v;)Lkotlin/reflect/n;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/l;

    aput-object v0, v1, v2

    const/4 v2, 0x2

    new-instance v0, Lkotlin/d/b/w;

    const-class v3, Lco/uk/getmondo/signup/card_ordering/a;

    invoke-static {v3}, Lkotlin/d/b/y;->a(Ljava/lang/Class;)Lkotlin/reflect/c;

    move-result-object v3

    const-string v4, "previousPin"

    const-string v5, "getPreviousPin()Ljava/lang/String;"

    invoke-direct {v0, v3, v4, v5}, Lkotlin/d/b/w;-><init>(Lkotlin/reflect/e;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v0}, Lkotlin/d/b/y;->a(Lkotlin/d/b/v;)Lkotlin/reflect/n;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/l;

    aput-object v0, v1, v2

    sput-object v1, Lco/uk/getmondo/signup/card_ordering/a;->a:[Lkotlin/reflect/l;

    new-instance v0, Lco/uk/getmondo/signup/card_ordering/a$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lco/uk/getmondo/signup/card_ordering/a$a;-><init>(Lkotlin/d/b/i;)V

    sput-object v0, Lco/uk/getmondo/signup/card_ordering/a;->d:Lco/uk/getmondo/signup/card_ordering/a$a;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 23
    invoke-direct {p0}, Lco/uk/getmondo/common/f/a;-><init>()V

    .line 25
    new-instance v0, Lco/uk/getmondo/signup/card_ordering/a$c;

    invoke-direct {v0, p0}, Lco/uk/getmondo/signup/card_ordering/a$c;-><init>(Lco/uk/getmondo/signup/card_ordering/a;)V

    check-cast v0, Lkotlin/d/a/a;

    invoke-static {v0}, Lkotlin/d;->a(Lkotlin/d/a/a;)Lkotlin/c;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/signup/card_ordering/a;->e:Lkotlin/c;

    .line 26
    new-instance v0, Lco/uk/getmondo/signup/card_ordering/a$d;

    invoke-direct {v0, p0}, Lco/uk/getmondo/signup/card_ordering/a$d;-><init>(Lco/uk/getmondo/signup/card_ordering/a;)V

    check-cast v0, Lkotlin/d/a/a;

    invoke-static {v0}, Lkotlin/d;->a(Lkotlin/d/a/a;)Lkotlin/c;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/signup/card_ordering/a;->f:Lkotlin/c;

    .line 27
    new-instance v0, Lco/uk/getmondo/signup/card_ordering/a$g;

    invoke-direct {v0, p0}, Lco/uk/getmondo/signup/card_ordering/a$g;-><init>(Lco/uk/getmondo/signup/card_ordering/a;)V

    check-cast v0, Lkotlin/d/a/a;

    invoke-static {v0}, Lkotlin/d;->a(Lkotlin/d/a/a;)Lkotlin/c;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/signup/card_ordering/a;->g:Lkotlin/c;

    return-void
.end method

.method private final l()Ljava/lang/String;
    .locals 3

    iget-object v0, p0, Lco/uk/getmondo/signup/card_ordering/a;->e:Lkotlin/c;

    sget-object v1, Lco/uk/getmondo/signup/card_ordering/a;->a:[Lkotlin/reflect/l;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-interface {v0}, Lkotlin/c;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public a(I)Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lco/uk/getmondo/signup/card_ordering/a;->h:Ljava/util/HashMap;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lco/uk/getmondo/signup/card_ordering/a;->h:Ljava/util/HashMap;

    :cond_0
    iget-object v0, p0, Lco/uk/getmondo/signup/card_ordering/a;->h:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    if-nez v0, :cond_1

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getView()Landroid/view/View;

    move-result-object v0

    if-nez v0, :cond_2

    const/4 v0, 0x0

    :cond_1
    :goto_0
    return-object v0

    :cond_2
    invoke-virtual {v0, p1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/signup/card_ordering/a;->h:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method public a()Lco/uk/getmondo/api/model/order_card/CardOrderName$Type;
    .locals 3

    iget-object v0, p0, Lco/uk/getmondo/signup/card_ordering/a;->f:Lkotlin/c;

    sget-object v1, Lco/uk/getmondo/signup/card_ordering/a;->a:[Lkotlin/reflect/l;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-interface {v0}, Lkotlin/c;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/api/model/order_card/CardOrderName$Type;

    return-object v0
.end method

.method public a(Ljava/lang/String;)V
    .locals 4

    .prologue
    const-string v0, "pin"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 101
    invoke-virtual {p0}, Lco/uk/getmondo/signup/card_ordering/a;->getActivity()Landroid/support/v4/app/j;

    move-result-object v0

    if-nez v0, :cond_0

    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type co.uk.getmondo.signup.card_ordering.ChoosePinFragment.StepListener"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    check-cast v0, Lco/uk/getmondo/signup/card_ordering/a$b;

    invoke-virtual {p0}, Lco/uk/getmondo/signup/card_ordering/a;->a()Lco/uk/getmondo/api/model/order_card/CardOrderName$Type;

    move-result-object v1

    invoke-direct {p0}, Lco/uk/getmondo/signup/card_ordering/a;->l()Ljava/lang/String;

    move-result-object v2

    const-string v3, "nameOnCard"

    invoke-static {v2, v3}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v0, v1, v2, p1}, Lco/uk/getmondo/signup/card_ordering/a$b;->a(Lco/uk/getmondo/api/model/order_card/CardOrderName$Type;Ljava/lang/String;Ljava/lang/String;)V

    .line 102
    return-void
.end method

.method public b()V
    .locals 1

    .prologue
    .line 83
    invoke-virtual {p0}, Lco/uk/getmondo/signup/card_ordering/a;->getActivity()Landroid/support/v4/app/j;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/signup/i$a;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lco/uk/getmondo/signup/i$a;->b()V

    .line 84
    :cond_0
    return-void
.end method

.method public c()Ljava/lang/String;
    .locals 3

    iget-object v0, p0, Lco/uk/getmondo/signup/card_ordering/a;->g:Lkotlin/c;

    sget-object v1, Lco/uk/getmondo/signup/card_ordering/a;->a:[Lkotlin/reflect/l;

    const/4 v2, 0x2

    aget-object v1, v1, v2

    invoke-interface {v0}, Lkotlin/c;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public d()Lio/reactivex/n;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 68
    new-instance v0, Lco/uk/getmondo/signup/card_ordering/a$e;

    invoke-direct {v0, p0}, Lco/uk/getmondo/signup/card_ordering/a$e;-><init>(Lco/uk/getmondo/signup/card_ordering/a;)V

    check-cast v0, Lio/reactivex/p;

    invoke-static {v0}, Lio/reactivex/n;->create(Lio/reactivex/p;)Lio/reactivex/n;

    move-result-object v1

    .line 71
    new-instance v0, Lco/uk/getmondo/signup/card_ordering/a$f;

    invoke-direct {v0, p0}, Lco/uk/getmondo/signup/card_ordering/a$f;-><init>(Lco/uk/getmondo/signup/card_ordering/a;)V

    check-cast v0, Lio/reactivex/c/q;

    invoke-virtual {v1, v0}, Lio/reactivex/n;->filter(Lio/reactivex/c/q;)Lio/reactivex/n;

    move-result-object v0

    const-string v1, "Observable.create<String\u2026filter { _ -> isResumed }"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public e()V
    .locals 1

    .prologue
    .line 74
    sget v0, Lco/uk/getmondo/c$a;->choosePinEntryView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/card_ordering/a;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/common/ui/PinEntryView;

    invoke-virtual {v0}, Lco/uk/getmondo/common/ui/PinEntryView;->requestFocus()Z

    .line 75
    sget v0, Lco/uk/getmondo/c$a;->choosePinEntryView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/card_ordering/a;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/common/ui/PinEntryView;

    invoke-static {v0}, Lco/uk/getmondo/common/ae;->a(Landroid/widget/EditText;)V

    .line 76
    return-void
.end method

.method public f()V
    .locals 1

    .prologue
    .line 79
    sget v0, Lco/uk/getmondo/c$a;->choosePinEntryView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/card_ordering/a;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/common/ui/PinEntryView;

    invoke-static {v0}, Lco/uk/getmondo/common/ae;->c(Landroid/view/View;)V

    .line 80
    return-void
.end method

.method public g()V
    .locals 1

    .prologue
    .line 87
    sget v0, Lco/uk/getmondo/c$a;->choosePinProgressBar:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/card_ordering/a;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    invoke-static {v0}, Lco/uk/getmondo/common/ae;->a(Landroid/view/View;)V

    .line 88
    sget v0, Lco/uk/getmondo/c$a;->choosePinOverlayView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/card_ordering/a;->a(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Lco/uk/getmondo/common/ae;->a(Landroid/view/View;)V

    .line 89
    return-void
.end method

.method public h()V
    .locals 1

    .prologue
    .line 92
    sget v0, Lco/uk/getmondo/c$a;->choosePinOverlayView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/card_ordering/a;->a(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Lco/uk/getmondo/common/ae;->b(Landroid/view/View;)V

    .line 93
    sget v0, Lco/uk/getmondo/c$a;->choosePinProgressBar:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/card_ordering/a;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    invoke-static {v0}, Lco/uk/getmondo/common/ae;->b(Landroid/view/View;)V

    .line 94
    return-void
.end method

.method public i()V
    .locals 1

    .prologue
    .line 97
    sget v0, Lco/uk/getmondo/c$a;->choosePinEntryView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/card_ordering/a;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/common/ui/PinEntryView;

    invoke-virtual {v0}, Lco/uk/getmondo/common/ui/PinEntryView;->a()V

    .line 98
    return-void
.end method

.method public j()V
    .locals 2

    .prologue
    .line 105
    invoke-virtual {p0}, Lco/uk/getmondo/signup/card_ordering/a;->getActivity()Landroid/support/v4/app/j;

    move-result-object v0

    if-nez v0, :cond_0

    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type co.uk.getmondo.signup.card_ordering.ChoosePinFragment.StepListener"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    check-cast v0, Lco/uk/getmondo/signup/card_ordering/a$b;

    invoke-interface {v0}, Lco/uk/getmondo/signup/card_ordering/a$b;->d_()V

    .line 106
    return-void
.end method

.method public k()V
    .locals 1

    iget-object v0, p0, Lco/uk/getmondo/signup/card_ordering/a;->h:Ljava/util/HashMap;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lco/uk/getmondo/signup/card_ordering/a;->h:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    :cond_0
    return-void
.end method

.method public onAttach(Landroid/content/Context;)V
    .locals 2

    .prologue
    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 37
    invoke-super {p0, p1}, Lco/uk/getmondo/common/f/a;->onAttach(Landroid/content/Context;)V

    .line 38
    instance-of v0, p1, Lco/uk/getmondo/signup/card_ordering/a$b;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Activity must implement "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-class v1, Lco/uk/getmondo/signup/card_ordering/a$b;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    .line 39
    :cond_0
    instance-of v0, p1, Lco/uk/getmondo/signup/i$a;

    if-nez v0, :cond_1

    .line 40
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Activity must implement "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-class v1, Lco/uk/getmondo/signup/i$a;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 39
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    .line 42
    :cond_1
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 32
    invoke-super {p0, p1}, Lco/uk/getmondo/common/f/a;->onCreate(Landroid/os/Bundle;)V

    .line 33
    invoke-virtual {p0}, Lco/uk/getmondo/signup/card_ordering/a;->B()Lco/uk/getmondo/common/h/b/b;

    move-result-object v0

    invoke-interface {v0, p0}, Lco/uk/getmondo/common/h/b/b;->a(Lco/uk/getmondo/signup/card_ordering/a;)V

    .line 34
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2

    .prologue
    const-string v0, "inflater"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 45
    invoke-virtual {p0}, Lco/uk/getmondo/signup/card_ordering/a;->getActivity()Landroid/support/v4/app/j;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/j;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v1, 0x24

    invoke-virtual {v0, v1}, Landroid/view/Window;->setSoftInputMode(I)V

    .line 46
    const v0, 0x7f05009b

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    const-string v1, "inflater.inflate(R.layou\u2026se_pin, container, false)"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public onDestroyView()V
    .locals 2

    .prologue
    .line 62
    iget-object v0, p0, Lco/uk/getmondo/signup/card_ordering/a;->c:Lco/uk/getmondo/signup/card_ordering/c;

    if-nez v0, :cond_0

    const-string v1, "presenter"

    invoke-static {v1}, Lkotlin/d/b/l;->b(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0}, Lco/uk/getmondo/signup/card_ordering/c;->b()V

    .line 64
    invoke-super {p0}, Lco/uk/getmondo/common/f/a;->onDestroyView()V

    .line 65
    invoke-virtual {p0}, Lco/uk/getmondo/signup/card_ordering/a;->k()V

    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 2

    .prologue
    const-string v0, "view"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 50
    invoke-super {p0, p1, p2}, Lco/uk/getmondo/common/f/a;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 52
    iget-object v1, p0, Lco/uk/getmondo/signup/card_ordering/a;->c:Lco/uk/getmondo/signup/card_ordering/c;

    if-nez v1, :cond_0

    const-string v0, "presenter"

    invoke-static {v0}, Lkotlin/d/b/l;->b(Ljava/lang/String;)V

    :cond_0
    move-object v0, p0

    check-cast v0, Lco/uk/getmondo/signup/card_ordering/c$a;

    invoke-virtual {v1, v0}, Lco/uk/getmondo/signup/card_ordering/c;->a(Lco/uk/getmondo/signup/card_ordering/c$a;)V

    .line 53
    sget v0, Lco/uk/getmondo/c$a;->choosePinNameOnCardTextView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/card_ordering/a;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-direct {p0}, Lco/uk/getmondo/signup/card_ordering/a;->l()Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 55
    invoke-virtual {p0}, Lco/uk/getmondo/signup/card_ordering/a;->c()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 56
    sget v0, Lco/uk/getmondo/c$a;->choosePinTitleTextView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/card_ordering/a;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const v1, 0x7f0a02d6

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 57
    sget v0, Lco/uk/getmondo/c$a;->choosePinSubtitleTextView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/card_ordering/a;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const v1, 0x7f0a02d5

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 59
    :cond_1
    return-void
.end method
