.class public interface abstract Lco/uk/getmondo/signup/card_ordering/e$b;
.super Ljava/lang/Object;
.source "ConfirmCardDetailsFragment.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lco/uk/getmondo/signup/card_ordering/e;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "b"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\u0008f\u0018\u00002\u00020\u0001J\u0018\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u0007H&\u00a8\u0006\u0008"
    }
    d2 = {
        "Lco/uk/getmondo/signup/card_ordering/ConfirmCardDetailsFragment$StepListener;",
        "",
        "onCardDetailsConfirmed",
        "",
        "nameType",
        "Lco/uk/getmondo/api/model/order_card/CardOrderName$Type;",
        "nameOnCard",
        "",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# virtual methods
.method public abstract a(Lco/uk/getmondo/api/model/order_card/CardOrderName$Type;Ljava/lang/String;)V
.end method
