.class public final Lco/uk/getmondo/signup/card_ordering/k;
.super Ljava/lang/Object;
.source "OrderCardManager.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000.\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\u0018\u00002\u00020\u0001B\u000f\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u000c\u0010\u0005\u001a\u0008\u0012\u0004\u0012\u00020\u00070\u0006J\u0016\u0010\u0008\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\u000b2\u0006\u0010\u000c\u001a\u00020\rR\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000e"
    }
    d2 = {
        "Lco/uk/getmondo/signup/card_ordering/OrderCardManager;",
        "",
        "signupApi",
        "Lco/uk/getmondo/api/SignupApi;",
        "(Lco/uk/getmondo/api/SignupApi;)V",
        "cardOrderOptions",
        "Lio/reactivex/Single;",
        "Lco/uk/getmondo/api/model/order_card/CardOrderOptions;",
        "createCardOrder",
        "Lio/reactivex/Completable;",
        "nameType",
        "Lco/uk/getmondo/api/model/order_card/CardOrderName$Type;",
        "pin",
        "",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field private final a:Lco/uk/getmondo/api/SignupApi;


# direct methods
.method public constructor <init>(Lco/uk/getmondo/api/SignupApi;)V
    .locals 1

    .prologue
    const-string v0, "signupApi"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lco/uk/getmondo/signup/card_ordering/k;->a:Lco/uk/getmondo/api/SignupApi;

    return-void
.end method


# virtual methods
.method public final a(Lco/uk/getmondo/api/model/order_card/CardOrderName$Type;Ljava/lang/String;)Lio/reactivex/b;
    .locals 3

    .prologue
    const-string v0, "nameType"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "pin"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 13
    iget-object v0, p0, Lco/uk/getmondo/signup/card_ordering/k;->a:Lco/uk/getmondo/api/SignupApi;

    invoke-virtual {p1}, Lco/uk/getmondo/api/model/order_card/CardOrderName$Type;->a()Ljava/lang/String;

    move-result-object v1

    const-string v2, "specified"

    invoke-interface {v0, v1, v2, p2}, Lco/uk/getmondo/api/SignupApi;->createCardOrder(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lio/reactivex/b;

    move-result-object v0

    return-object v0
.end method

.method public final a()Lio/reactivex/v;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/v",
            "<",
            "Lco/uk/getmondo/api/model/order_card/CardOrderOptions;",
            ">;"
        }
    .end annotation

    .prologue
    .line 10
    iget-object v0, p0, Lco/uk/getmondo/signup/card_ordering/k;->a:Lco/uk/getmondo/api/SignupApi;

    invoke-interface {v0}, Lco/uk/getmondo/api/SignupApi;->cardOrderOptions()Lio/reactivex/v;

    move-result-object v0

    return-object v0
.end method
