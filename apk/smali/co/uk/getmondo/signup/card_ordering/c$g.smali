.class final Lco/uk/getmondo/signup/card_ordering/c$g;
.super Ljava/lang/Object;
.source "ChoosePinPresenter.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lco/uk/getmondo/signup/card_ordering/c;->a(Lco/uk/getmondo/signup/card_ordering/c$a;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h",
        "<TT;",
        "Lio/reactivex/l",
        "<+TR;>;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000\u0012\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\u0010\u0000\u001a\u0008\u0012\u0004\u0012\u00020\u00020\u00012\u0006\u0010\u0003\u001a\u00020\u0004H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "Lio/reactivex/Maybe;",
        "",
        "pin",
        "",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field final synthetic a:Lco/uk/getmondo/signup/card_ordering/c;

.field final synthetic b:Lco/uk/getmondo/signup/card_ordering/c$a;


# direct methods
.method constructor <init>(Lco/uk/getmondo/signup/card_ordering/c;Lco/uk/getmondo/signup/card_ordering/c$a;)V
    .locals 0

    iput-object p1, p0, Lco/uk/getmondo/signup/card_ordering/c$g;->a:Lco/uk/getmondo/signup/card_ordering/c;

    iput-object p2, p0, Lco/uk/getmondo/signup/card_ordering/c$g;->b:Lco/uk/getmondo/signup/card_ordering/c$a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Lio/reactivex/h;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/h",
            "<",
            "Lkotlin/n;",
            ">;"
        }
    .end annotation

    .prologue
    const-string v0, "pin"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 55
    iget-object v0, p0, Lco/uk/getmondo/signup/card_ordering/c$g;->a:Lco/uk/getmondo/signup/card_ordering/c;

    invoke-static {v0}, Lco/uk/getmondo/signup/card_ordering/c;->b(Lco/uk/getmondo/signup/card_ordering/c;)Lco/uk/getmondo/signup/card_ordering/k;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/signup/card_ordering/c$g;->b:Lco/uk/getmondo/signup/card_ordering/c$a;

    invoke-interface {v1}, Lco/uk/getmondo/signup/card_ordering/c$a;->a()Lco/uk/getmondo/api/model/order_card/CardOrderName$Type;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, Lco/uk/getmondo/signup/card_ordering/k;->a(Lco/uk/getmondo/api/model/order_card/CardOrderName$Type;Ljava/lang/String;)Lio/reactivex/b;

    move-result-object v0

    .line 56
    iget-object v1, p0, Lco/uk/getmondo/signup/card_ordering/c$g;->a:Lco/uk/getmondo/signup/card_ordering/c;

    invoke-static {v1}, Lco/uk/getmondo/signup/card_ordering/c;->c(Lco/uk/getmondo/signup/card_ordering/c;)Lio/reactivex/u;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/b;->b(Lio/reactivex/u;)Lio/reactivex/b;

    move-result-object v0

    .line 57
    iget-object v1, p0, Lco/uk/getmondo/signup/card_ordering/c$g;->a:Lco/uk/getmondo/signup/card_ordering/c;

    invoke-static {v1}, Lco/uk/getmondo/signup/card_ordering/c;->d(Lco/uk/getmondo/signup/card_ordering/c;)Lio/reactivex/u;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/b;->a(Lio/reactivex/u;)Lio/reactivex/b;

    move-result-object v1

    .line 58
    new-instance v0, Lco/uk/getmondo/signup/card_ordering/c$g$1;

    invoke-direct {v0, p0}, Lco/uk/getmondo/signup/card_ordering/c$g$1;-><init>(Lco/uk/getmondo/signup/card_ordering/c$g;)V

    check-cast v0, Lio/reactivex/c/g;

    invoke-virtual {v1, v0}, Lio/reactivex/b;->c(Lio/reactivex/c/g;)Lio/reactivex/b;

    move-result-object v1

    .line 59
    new-instance v0, Lco/uk/getmondo/signup/card_ordering/c$g$2;

    invoke-direct {v0, p0}, Lco/uk/getmondo/signup/card_ordering/c$g$2;-><init>(Lco/uk/getmondo/signup/card_ordering/c$g;)V

    check-cast v0, Lio/reactivex/c/g;

    invoke-virtual {v1, v0}, Lio/reactivex/b;->a(Lio/reactivex/c/g;)Lio/reactivex/b;

    move-result-object v0

    .line 60
    sget-object v1, Lkotlin/n;->a:Lkotlin/n;

    invoke-static {v0, v1}, Lco/uk/getmondo/common/j/f;->a(Lio/reactivex/b;Ljava/lang/Object;)Lio/reactivex/h;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 20
    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1}, Lco/uk/getmondo/signup/card_ordering/c$g;->a(Ljava/lang/String;)Lio/reactivex/h;

    move-result-object v0

    return-object v0
.end method
