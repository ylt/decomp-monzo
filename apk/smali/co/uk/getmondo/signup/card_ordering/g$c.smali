.class final Lco/uk/getmondo/signup/card_ordering/g$c;
.super Ljava/lang/Object;
.source "ConfirmCardDetailsPresenter.kt"

# interfaces
.implements Lio/reactivex/c/g;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lco/uk/getmondo/signup/card_ordering/g;->a(Lco/uk/getmondo/signup/card_ordering/g$a;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/g",
        "<",
        "Lco/uk/getmondo/api/model/order_card/CardOrderOptions;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "",
        "<name for destructuring parameter 0>",
        "Lco/uk/getmondo/api/model/order_card/CardOrderOptions;",
        "kotlin.jvm.PlatformType",
        "accept"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field final synthetic a:Lco/uk/getmondo/signup/card_ordering/g;

.field final synthetic b:Lco/uk/getmondo/signup/card_ordering/g$a;


# direct methods
.method constructor <init>(Lco/uk/getmondo/signup/card_ordering/g;Lco/uk/getmondo/signup/card_ordering/g$a;)V
    .locals 0

    iput-object p1, p0, Lco/uk/getmondo/signup/card_ordering/g$c;->a:Lco/uk/getmondo/signup/card_ordering/g;

    iput-object p2, p0, Lco/uk/getmondo/signup/card_ordering/g$c;->b:Lco/uk/getmondo/signup/card_ordering/g$a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lco/uk/getmondo/api/model/order_card/CardOrderOptions;)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    invoke-virtual {p1}, Lco/uk/getmondo/api/model/order_card/CardOrderOptions;->a()Ljava/util/List;

    move-result-object v1

    invoke-virtual {p1}, Lco/uk/getmondo/api/model/order_card/CardOrderOptions;->b()Lco/uk/getmondo/api/model/ApiAddress;

    move-result-object v2

    move-object v0, v1

    .line 53
    check-cast v0, Ljava/lang/Iterable;

    .line 122
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/api/model/order_card/CardOrderName;

    invoke-virtual {v0}, Lco/uk/getmondo/api/model/order_card/CardOrderName;->a()Lco/uk/getmondo/api/model/order_card/CardOrderName$Type;

    move-result-object v4

    invoke-virtual {v0}, Lco/uk/getmondo/api/model/order_card/CardOrderName;->b()Ljava/lang/String;

    move-result-object v0

    .line 54
    sget-object v5, Lco/uk/getmondo/signup/card_ordering/h;->a:[I

    invoke-virtual {v4}, Lco/uk/getmondo/api/model/order_card/CardOrderName$Type;->ordinal()I

    move-result v4

    aget v4, v5, v4

    packed-switch v4, :pswitch_data_0

    .line 66
    :goto_1
    nop

    goto :goto_0

    .line 56
    :pswitch_0
    iget-object v4, p0, Lco/uk/getmondo/signup/card_ordering/g$c;->a:Lco/uk/getmondo/signup/card_ordering/g;

    invoke-static {v4, v0}, Lco/uk/getmondo/signup/card_ordering/g;->a(Lco/uk/getmondo/signup/card_ordering/g;Ljava/lang/String;)V

    .line 57
    iget-object v4, p0, Lco/uk/getmondo/signup/card_ordering/g$c;->a:Lco/uk/getmondo/signup/card_ordering/g;

    sget-object v5, Lco/uk/getmondo/api/model/order_card/CardOrderName$Type;->LEGAL:Lco/uk/getmondo/api/model/order_card/CardOrderName$Type;

    invoke-static {v4, v5}, Lco/uk/getmondo/signup/card_ordering/g;->a(Lco/uk/getmondo/signup/card_ordering/g;Lco/uk/getmondo/api/model/order_card/CardOrderName$Type;)V

    .line 58
    iget-object v4, p0, Lco/uk/getmondo/signup/card_ordering/g$c;->b:Lco/uk/getmondo/signup/card_ordering/g$a;

    invoke-interface {v4, v0}, Lco/uk/getmondo/signup/card_ordering/g$a;->a(Ljava/lang/String;)V

    goto :goto_1

    .line 61
    :pswitch_1
    iget-object v4, p0, Lco/uk/getmondo/signup/card_ordering/g$c;->a:Lco/uk/getmondo/signup/card_ordering/g;

    invoke-static {v4, v0}, Lco/uk/getmondo/signup/card_ordering/g;->b(Lco/uk/getmondo/signup/card_ordering/g;Ljava/lang/String;)V

    .line 62
    iget-object v4, p0, Lco/uk/getmondo/signup/card_ordering/g$c;->a:Lco/uk/getmondo/signup/card_ordering/g;

    sget-object v5, Lco/uk/getmondo/api/model/order_card/CardOrderName$Type;->PREFERRED:Lco/uk/getmondo/api/model/order_card/CardOrderName$Type;

    invoke-static {v4, v5}, Lco/uk/getmondo/signup/card_ordering/g;->a(Lco/uk/getmondo/signup/card_ordering/g;Lco/uk/getmondo/api/model/order_card/CardOrderName$Type;)V

    .line 63
    iget-object v4, p0, Lco/uk/getmondo/signup/card_ordering/g$c;->b:Lco/uk/getmondo/signup/card_ordering/g$a;

    invoke-interface {v4, v0}, Lco/uk/getmondo/signup/card_ordering/g$a;->b(Ljava/lang/String;)V

    goto :goto_1

    .line 123
    :cond_0
    nop

    .line 67
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v0

    if-le v0, v6, :cond_1

    .line 68
    iget-object v0, p0, Lco/uk/getmondo/signup/card_ordering/g$c;->b:Lco/uk/getmondo/signup/card_ordering/g$a;

    invoke-interface {v0}, Lco/uk/getmondo/signup/card_ordering/g$a;->h()V

    .line 69
    iget-object v0, p0, Lco/uk/getmondo/signup/card_ordering/g$c;->a:Lco/uk/getmondo/signup/card_ordering/g;

    iget-object v1, p0, Lco/uk/getmondo/signup/card_ordering/g$c;->b:Lco/uk/getmondo/signup/card_ordering/g$a;

    invoke-static {v0, v1}, Lco/uk/getmondo/signup/card_ordering/g;->a(Lco/uk/getmondo/signup/card_ordering/g;Lco/uk/getmondo/signup/card_ordering/g$a;)V

    .line 70
    iget-object v0, p0, Lco/uk/getmondo/signup/card_ordering/g$c;->a:Lco/uk/getmondo/signup/card_ordering/g;

    invoke-static {v0, v6}, Lco/uk/getmondo/signup/card_ordering/g;->a(Lco/uk/getmondo/signup/card_ordering/g;Z)V

    .line 72
    :cond_1
    iget-object v0, p0, Lco/uk/getmondo/signup/card_ordering/g$c;->b:Lco/uk/getmondo/signup/card_ordering/g$a;

    invoke-virtual {v2}, Lco/uk/getmondo/api/model/ApiAddress;->e()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lco/uk/getmondo/signup/card_ordering/g$a;->c(Ljava/lang/String;)V

    .line 73
    return-void

    .line 54
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 20
    check-cast p1, Lco/uk/getmondo/api/model/order_card/CardOrderOptions;

    invoke-virtual {p0, p1}, Lco/uk/getmondo/signup/card_ordering/g$c;->a(Lco/uk/getmondo/api/model/order_card/CardOrderOptions;)V

    return-void
.end method
