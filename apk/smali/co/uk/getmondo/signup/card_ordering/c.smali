.class public final Lco/uk/getmondo/signup/card_ordering/c;
.super Lco/uk/getmondo/common/ui/b;
.source "ChoosePinPresenter.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lco/uk/getmondo/signup/card_ordering/c$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lco/uk/getmondo/common/ui/b",
        "<",
        "Lco/uk/getmondo/signup/card_ordering/c$a;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000:\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0003\n\u0002\u0008\u0003\u0008\u0007\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00020\u0001:\u0001\u0013B3\u0008\u0007\u0012\u0008\u0008\u0001\u0010\u0003\u001a\u00020\u0004\u0012\u0008\u0008\u0001\u0010\u0005\u001a\u00020\u0004\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b\u00a2\u0006\u0002\u0010\u000cJ\u0018\u0010\r\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u00022\u0006\u0010\u0010\u001a\u00020\u0011H\u0002J\u0010\u0010\u0012\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u0002H\u0016R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0014"
    }
    d2 = {
        "Lco/uk/getmondo/signup/card_ordering/ChoosePinPresenter;",
        "Lco/uk/getmondo/common/ui/BasePresenter;",
        "Lco/uk/getmondo/signup/card_ordering/ChoosePinPresenter$View;",
        "ioScheduler",
        "Lio/reactivex/Scheduler;",
        "uiScheduler",
        "orderCardManager",
        "Lco/uk/getmondo/signup/card_ordering/OrderCardManager;",
        "apiErrorHandler",
        "Lco/uk/getmondo/common/errors/ApiErrorHandler;",
        "analyticsService",
        "Lco/uk/getmondo/common/AnalyticsService;",
        "(Lio/reactivex/Scheduler;Lio/reactivex/Scheduler;Lco/uk/getmondo/signup/card_ordering/OrderCardManager;Lco/uk/getmondo/common/errors/ApiErrorHandler;Lco/uk/getmondo/common/AnalyticsService;)V",
        "handleError",
        "",
        "view",
        "it",
        "",
        "register",
        "View",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field private final c:Lio/reactivex/u;

.field private final d:Lio/reactivex/u;

.field private final e:Lco/uk/getmondo/signup/card_ordering/k;

.field private final f:Lco/uk/getmondo/common/e/a;

.field private final g:Lco/uk/getmondo/common/a;


# direct methods
.method public constructor <init>(Lio/reactivex/u;Lio/reactivex/u;Lco/uk/getmondo/signup/card_ordering/k;Lco/uk/getmondo/common/e/a;Lco/uk/getmondo/common/a;)V
    .locals 1

    .prologue
    const-string v0, "ioScheduler"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "uiScheduler"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "orderCardManager"

    invoke-static {p3, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "apiErrorHandler"

    invoke-static {p4, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "analyticsService"

    invoke-static {p5, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 27
    invoke-direct {p0}, Lco/uk/getmondo/common/ui/b;-><init>()V

    iput-object p1, p0, Lco/uk/getmondo/signup/card_ordering/c;->c:Lio/reactivex/u;

    iput-object p2, p0, Lco/uk/getmondo/signup/card_ordering/c;->d:Lio/reactivex/u;

    iput-object p3, p0, Lco/uk/getmondo/signup/card_ordering/c;->e:Lco/uk/getmondo/signup/card_ordering/k;

    iput-object p4, p0, Lco/uk/getmondo/signup/card_ordering/c;->f:Lco/uk/getmondo/common/e/a;

    iput-object p5, p0, Lco/uk/getmondo/signup/card_ordering/c;->g:Lco/uk/getmondo/common/a;

    return-void
.end method

.method public static final synthetic a(Lco/uk/getmondo/signup/card_ordering/c;)Lco/uk/getmondo/common/a;
    .locals 1

    .prologue
    .line 20
    iget-object v0, p0, Lco/uk/getmondo/signup/card_ordering/c;->g:Lco/uk/getmondo/common/a;

    return-object v0
.end method

.method private final a(Lco/uk/getmondo/signup/card_ordering/c$a;Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 66
    invoke-interface {p1}, Lco/uk/getmondo/signup/card_ordering/c$a;->h()V

    .line 67
    invoke-interface {p1}, Lco/uk/getmondo/signup/card_ordering/c$a;->f()V

    .line 68
    sget-object v1, Lco/uk/getmondo/signup/i;->a:Lco/uk/getmondo/signup/i;

    move-object v0, p1

    check-cast v0, Lco/uk/getmondo/signup/i$a;

    invoke-virtual {v1, p2, v0}, Lco/uk/getmondo/signup/i;->a(Ljava/lang/Throwable;Lco/uk/getmondo/signup/i$a;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v1, p0, Lco/uk/getmondo/signup/card_ordering/c;->f:Lco/uk/getmondo/common/e/a;

    move-object v0, p1

    check-cast v0, Lco/uk/getmondo/common/e/a$a;

    invoke-virtual {v1, p2, v0}, Lco/uk/getmondo/common/e/a;->a(Ljava/lang/Throwable;Lco/uk/getmondo/common/e/a$a;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 69
    const v0, 0x7f0a0196

    invoke-interface {p1, v0}, Lco/uk/getmondo/signup/card_ordering/c$a;->b(I)V

    .line 71
    :cond_0
    return-void
.end method

.method public static final synthetic a(Lco/uk/getmondo/signup/card_ordering/c;Lco/uk/getmondo/signup/card_ordering/c$a;Ljava/lang/Throwable;)V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0, p1, p2}, Lco/uk/getmondo/signup/card_ordering/c;->a(Lco/uk/getmondo/signup/card_ordering/c$a;Ljava/lang/Throwable;)V

    return-void
.end method

.method public static final synthetic b(Lco/uk/getmondo/signup/card_ordering/c;)Lco/uk/getmondo/signup/card_ordering/k;
    .locals 1

    .prologue
    .line 20
    iget-object v0, p0, Lco/uk/getmondo/signup/card_ordering/c;->e:Lco/uk/getmondo/signup/card_ordering/k;

    return-object v0
.end method

.method public static final synthetic c(Lco/uk/getmondo/signup/card_ordering/c;)Lio/reactivex/u;
    .locals 1

    .prologue
    .line 20
    iget-object v0, p0, Lco/uk/getmondo/signup/card_ordering/c;->c:Lio/reactivex/u;

    return-object v0
.end method

.method public static final synthetic d(Lco/uk/getmondo/signup/card_ordering/c;)Lio/reactivex/u;
    .locals 1

    .prologue
    .line 20
    iget-object v0, p0, Lco/uk/getmondo/signup/card_ordering/c;->d:Lio/reactivex/u;

    return-object v0
.end method


# virtual methods
.method public bridge synthetic a(Lco/uk/getmondo/common/ui/f;)V
    .locals 0

    .prologue
    .line 20
    check-cast p1, Lco/uk/getmondo/signup/card_ordering/c$a;

    invoke-virtual {p0, p1}, Lco/uk/getmondo/signup/card_ordering/c;->a(Lco/uk/getmondo/signup/card_ordering/c$a;)V

    return-void
.end method

.method public a(Lco/uk/getmondo/signup/card_ordering/c$a;)V
    .locals 4

    .prologue
    const-string v0, "view"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v0, p1

    .line 30
    check-cast v0, Lco/uk/getmondo/common/ui/f;

    invoke-super {p0, v0}, Lco/uk/getmondo/common/ui/b;->a(Lco/uk/getmondo/common/ui/f;)V

    .line 32
    invoke-interface {p1}, Lco/uk/getmondo/signup/card_ordering/c$a;->e()V

    .line 33
    invoke-interface {p1}, Lco/uk/getmondo/signup/card_ordering/c$a;->c()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    .line 34
    iget-object v0, p0, Lco/uk/getmondo/signup/card_ordering/c;->g:Lco/uk/getmondo/common/a;

    sget-object v1, Lco/uk/getmondo/api/model/tracking/Impression;->Companion:Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    invoke-virtual {v1}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->aS()Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v1

    invoke-virtual {v0, v1}, Lco/uk/getmondo/common/a;->a(Lco/uk/getmondo/api/model/tracking/Impression;)V

    .line 37
    :goto_0
    invoke-interface {p1}, Lco/uk/getmondo/signup/card_ordering/c$a;->d()Lio/reactivex/n;

    move-result-object v0

    invoke-virtual {v0}, Lio/reactivex/n;->share()Lio/reactivex/n;

    move-result-object v1

    .line 40
    iget-object v2, p0, Lco/uk/getmondo/signup/card_ordering/c;->b:Lio/reactivex/b/a;

    .line 41
    new-instance v0, Lco/uk/getmondo/signup/card_ordering/c$b;

    invoke-direct {v0, p1}, Lco/uk/getmondo/signup/card_ordering/c$b;-><init>(Lco/uk/getmondo/signup/card_ordering/c$a;)V

    check-cast v0, Lio/reactivex/c/q;

    invoke-virtual {v1, v0}, Lio/reactivex/n;->filter(Lio/reactivex/c/q;)Lio/reactivex/n;

    move-result-object v3

    .line 42
    new-instance v0, Lco/uk/getmondo/signup/card_ordering/c$c;

    invoke-direct {v0, p1}, Lco/uk/getmondo/signup/card_ordering/c$c;-><init>(Lco/uk/getmondo/signup/card_ordering/c$a;)V

    check-cast v0, Lio/reactivex/c/g;

    invoke-virtual {v3, v0}, Lio/reactivex/n;->subscribe(Lio/reactivex/c/g;)Lio/reactivex/b/b;

    move-result-object v0

    const-string v3, "onPinEntered\n           \u2026inEnteredFirstTime(pin) }"

    invoke-static {v0, v3}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 40
    invoke-static {v2, v0}, Lco/uk/getmondo/common/j/f;->a(Lio/reactivex/b/a;Lio/reactivex/b/b;)Lio/reactivex/b/a;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/signup/card_ordering/c;->b:Lio/reactivex/b/a;

    .line 44
    iget-object v2, p0, Lco/uk/getmondo/signup/card_ordering/c;->b:Lio/reactivex/b/a;

    .line 45
    new-instance v0, Lco/uk/getmondo/signup/card_ordering/c$d;

    invoke-direct {v0, p1}, Lco/uk/getmondo/signup/card_ordering/c$d;-><init>(Lco/uk/getmondo/signup/card_ordering/c$a;)V

    check-cast v0, Lio/reactivex/c/q;

    invoke-virtual {v1, v0}, Lio/reactivex/n;->filter(Lio/reactivex/c/q;)Lio/reactivex/n;

    move-result-object v3

    .line 46
    new-instance v0, Lco/uk/getmondo/signup/card_ordering/c$e;

    invoke-direct {v0, p0, p1}, Lco/uk/getmondo/signup/card_ordering/c$e;-><init>(Lco/uk/getmondo/signup/card_ordering/c;Lco/uk/getmondo/signup/card_ordering/c$a;)V

    check-cast v0, Lio/reactivex/c/g;

    invoke-virtual {v3, v0}, Lio/reactivex/n;->subscribe(Lio/reactivex/c/g;)Lio/reactivex/b/b;

    move-result-object v0

    const-string v3, "onPinEntered\n           \u2026ct_pin)\n                }"

    invoke-static {v0, v3}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 44
    invoke-static {v2, v0}, Lco/uk/getmondo/common/j/f;->a(Lio/reactivex/b/a;Lio/reactivex/b/b;)Lio/reactivex/b/a;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/signup/card_ordering/c;->b:Lio/reactivex/b/a;

    .line 52
    iget-object v2, p0, Lco/uk/getmondo/signup/card_ordering/c;->b:Lio/reactivex/b/a;

    .line 53
    new-instance v0, Lco/uk/getmondo/signup/card_ordering/c$f;

    invoke-direct {v0, p1}, Lco/uk/getmondo/signup/card_ordering/c$f;-><init>(Lco/uk/getmondo/signup/card_ordering/c$a;)V

    check-cast v0, Lio/reactivex/c/q;

    invoke-virtual {v1, v0}, Lio/reactivex/n;->filter(Lio/reactivex/c/q;)Lio/reactivex/n;

    move-result-object v1

    .line 54
    new-instance v0, Lco/uk/getmondo/signup/card_ordering/c$g;

    invoke-direct {v0, p0, p1}, Lco/uk/getmondo/signup/card_ordering/c$g;-><init>(Lco/uk/getmondo/signup/card_ordering/c;Lco/uk/getmondo/signup/card_ordering/c$a;)V

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {v1, v0}, Lio/reactivex/n;->flatMapMaybe(Lio/reactivex/c/h;)Lio/reactivex/n;

    move-result-object v1

    .line 62
    new-instance v0, Lco/uk/getmondo/signup/card_ordering/c$h;

    invoke-direct {v0, p1}, Lco/uk/getmondo/signup/card_ordering/c$h;-><init>(Lco/uk/getmondo/signup/card_ordering/c$a;)V

    check-cast v0, Lio/reactivex/c/g;

    invoke-virtual {v1, v0}, Lio/reactivex/n;->subscribe(Lio/reactivex/c/g;)Lio/reactivex/b/b;

    move-result-object v0

    const-string v1, "onPinEntered\n           \u2026be { view.cardOrdered() }"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 52
    invoke-static {v2, v0}, Lco/uk/getmondo/common/j/f;->a(Lio/reactivex/b/a;Lio/reactivex/b/b;)Lio/reactivex/b/a;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/signup/card_ordering/c;->b:Lio/reactivex/b/a;

    .line 63
    return-void

    .line 36
    :cond_0
    iget-object v0, p0, Lco/uk/getmondo/signup/card_ordering/c;->g:Lco/uk/getmondo/common/a;

    sget-object v1, Lco/uk/getmondo/api/model/tracking/Impression;->Companion:Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    invoke-virtual {v1}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->aT()Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v1

    invoke-virtual {v0, v1}, Lco/uk/getmondo/common/a;->a(Lco/uk/getmondo/api/model/tracking/Impression;)V

    goto/16 :goto_0
.end method
