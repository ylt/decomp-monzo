.class final Lco/uk/getmondo/signup/card_ordering/g$b;
.super Ljava/lang/Object;
.source "ConfirmCardDetailsPresenter.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lco/uk/getmondo/signup/card_ordering/g;->a(Lco/uk/getmondo/signup/card_ordering/g$a;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h",
        "<TT;",
        "Lio/reactivex/l",
        "<+TR;>;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000\u0016\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u0010\u0012\u000c\u0012\n \u0003*\u0004\u0018\u00010\u00020\u00020\u00012\u0006\u0010\u0004\u001a\u00020\u0005H\n\u00a2\u0006\u0004\u0008\u0006\u0010\u0007"
    }
    d2 = {
        "<anonymous>",
        "Lio/reactivex/Maybe;",
        "Lco/uk/getmondo/api/model/order_card/CardOrderOptions;",
        "kotlin.jvm.PlatformType",
        "it",
        "",
        "apply",
        "(Lkotlin/Unit;)Lio/reactivex/Maybe;"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field final synthetic a:Lco/uk/getmondo/signup/card_ordering/g;

.field final synthetic b:Lco/uk/getmondo/signup/card_ordering/g$a;


# direct methods
.method constructor <init>(Lco/uk/getmondo/signup/card_ordering/g;Lco/uk/getmondo/signup/card_ordering/g$a;)V
    .locals 0

    iput-object p1, p0, Lco/uk/getmondo/signup/card_ordering/g$b;->a:Lco/uk/getmondo/signup/card_ordering/g;

    iput-object p2, p0, Lco/uk/getmondo/signup/card_ordering/g$b;->b:Lco/uk/getmondo/signup/card_ordering/g$a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lkotlin/n;)Lio/reactivex/h;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/n;",
            ")",
            "Lio/reactivex/h",
            "<",
            "Lco/uk/getmondo/api/model/order_card/CardOrderOptions;",
            ">;"
        }
    .end annotation

    .prologue
    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 40
    iget-object v0, p0, Lco/uk/getmondo/signup/card_ordering/g$b;->a:Lco/uk/getmondo/signup/card_ordering/g;

    invoke-static {v0}, Lco/uk/getmondo/signup/card_ordering/g;->a(Lco/uk/getmondo/signup/card_ordering/g;)Lco/uk/getmondo/signup/card_ordering/k;

    move-result-object v0

    invoke-virtual {v0}, Lco/uk/getmondo/signup/card_ordering/k;->a()Lio/reactivex/v;

    move-result-object v0

    .line 41
    iget-object v1, p0, Lco/uk/getmondo/signup/card_ordering/g$b;->a:Lco/uk/getmondo/signup/card_ordering/g;

    invoke-static {v1}, Lco/uk/getmondo/signup/card_ordering/g;->b(Lco/uk/getmondo/signup/card_ordering/g;)Lio/reactivex/u;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/v;->b(Lio/reactivex/u;)Lio/reactivex/v;

    move-result-object v0

    .line 42
    iget-object v1, p0, Lco/uk/getmondo/signup/card_ordering/g$b;->a:Lco/uk/getmondo/signup/card_ordering/g;

    invoke-static {v1}, Lco/uk/getmondo/signup/card_ordering/g;->c(Lco/uk/getmondo/signup/card_ordering/g;)Lio/reactivex/u;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/v;->a(Lio/reactivex/u;)Lio/reactivex/v;

    move-result-object v1

    .line 43
    new-instance v0, Lco/uk/getmondo/signup/card_ordering/g$b$1;

    invoke-direct {v0, p0}, Lco/uk/getmondo/signup/card_ordering/g$b$1;-><init>(Lco/uk/getmondo/signup/card_ordering/g$b;)V

    check-cast v0, Lio/reactivex/c/g;

    invoke-virtual {v1, v0}, Lio/reactivex/v;->b(Lio/reactivex/c/g;)Lio/reactivex/v;

    move-result-object v1

    .line 44
    new-instance v0, Lco/uk/getmondo/signup/card_ordering/g$b$2;

    invoke-direct {v0, p0}, Lco/uk/getmondo/signup/card_ordering/g$b$2;-><init>(Lco/uk/getmondo/signup/card_ordering/g$b;)V

    check-cast v0, Lio/reactivex/c/b;

    invoke-virtual {v1, v0}, Lio/reactivex/v;->a(Lio/reactivex/c/b;)Lio/reactivex/v;

    move-result-object v1

    .line 45
    new-instance v0, Lco/uk/getmondo/signup/card_ordering/g$b$3;

    invoke-direct {v0, p0}, Lco/uk/getmondo/signup/card_ordering/g$b$3;-><init>(Lco/uk/getmondo/signup/card_ordering/g$b;)V

    check-cast v0, Lio/reactivex/c/g;

    invoke-virtual {v1, v0}, Lio/reactivex/v;->d(Lio/reactivex/c/g;)Lio/reactivex/v;

    move-result-object v0

    .line 50
    invoke-static {v0}, Lco/uk/getmondo/common/j/f;->a(Lio/reactivex/v;)Lio/reactivex/h;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 20
    check-cast p1, Lkotlin/n;

    invoke-virtual {p0, p1}, Lco/uk/getmondo/signup/card_ordering/g$b;->a(Lkotlin/n;)Lio/reactivex/h;

    move-result-object v0

    return-object v0
.end method
