.class public final Lco/uk/getmondo/signup/card_ordering/OrderCardActivity;
.super Lco/uk/getmondo/signup/a;
.source "OrderCardActivity.kt"

# interfaces
.implements Lco/uk/getmondo/signup/card_ordering/a$b;
.implements Lco/uk/getmondo/signup/card_ordering/e$b;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lco/uk/getmondo/signup/card_ordering/OrderCardActivity$a;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u00000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0005\u0018\u0000 \u00122\u00020\u00012\u00020\u00022\u00020\u0003:\u0001\u0012B\u0005\u00a2\u0006\u0002\u0010\u0004J\u0018\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u00082\u0006\u0010\t\u001a\u00020\nH\u0016J\u0008\u0010\u000b\u001a\u00020\u0006H\u0016J\u0012\u0010\u000c\u001a\u00020\u00062\u0008\u0010\r\u001a\u0004\u0018\u00010\u000eH\u0014J \u0010\u000f\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u00082\u0006\u0010\t\u001a\u00020\n2\u0006\u0010\u0010\u001a\u00020\nH\u0016J\"\u0010\u0011\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u00082\u0006\u0010\t\u001a\u00020\n2\u0008\u0010\u0010\u001a\u0004\u0018\u00010\nH\u0002\u00a8\u0006\u0013"
    }
    d2 = {
        "Lco/uk/getmondo/signup/card_ordering/OrderCardActivity;",
        "Lco/uk/getmondo/signup/BaseSignupActivity;",
        "Lco/uk/getmondo/signup/card_ordering/ConfirmCardDetailsFragment$StepListener;",
        "Lco/uk/getmondo/signup/card_ordering/ChoosePinFragment$StepListener;",
        "()V",
        "onCardDetailsConfirmed",
        "",
        "nameType",
        "Lco/uk/getmondo/api/model/order_card/CardOrderName$Type;",
        "nameOnCard",
        "",
        "onCardOrdered",
        "onCreate",
        "savedInstanceState",
        "Landroid/os/Bundle;",
        "onPinEntered",
        "pin",
        "showChoosePinFragment",
        "Companion",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# static fields
.field public static final a:Lco/uk/getmondo/signup/card_ordering/OrderCardActivity$a;


# instance fields
.field private b:Ljava/util/HashMap;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lco/uk/getmondo/signup/card_ordering/OrderCardActivity$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lco/uk/getmondo/signup/card_ordering/OrderCardActivity$a;-><init>(Lkotlin/d/b/i;)V

    sput-object v0, Lco/uk/getmondo/signup/card_ordering/OrderCardActivity;->a:Lco/uk/getmondo/signup/card_ordering/OrderCardActivity$a;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Lco/uk/getmondo/signup/a;-><init>()V

    return-void
.end method

.method private final b(Lco/uk/getmondo/api/model/order_card/CardOrderName$Type;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 42
    invoke-virtual {p0}, Lco/uk/getmondo/signup/card_ordering/OrderCardActivity;->getSupportFragmentManager()Landroid/support/v4/app/n;

    move-result-object v0

    .line 43
    invoke-virtual {v0}, Landroid/support/v4/app/n;->a()Landroid/support/v4/app/t;

    move-result-object v0

    .line 44
    const v1, 0x7f1101d1

    sget-object v2, Lco/uk/getmondo/signup/card_ordering/a;->d:Lco/uk/getmondo/signup/card_ordering/a$a;

    invoke-virtual {v2, p1, p2, p3}, Lco/uk/getmondo/signup/card_ordering/a$a;->a(Lco/uk/getmondo/api/model/order_card/CardOrderName$Type;Ljava/lang/String;Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/app/t;->b(ILandroid/support/v4/app/Fragment;)Landroid/support/v4/app/t;

    move-result-object v0

    .line 45
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v4/app/t;->a(Ljava/lang/String;)Landroid/support/v4/app/t;

    move-result-object v0

    .line 46
    const/16 v1, 0x1001

    invoke-virtual {v0, v1}, Landroid/support/v4/app/t;->a(I)Landroid/support/v4/app/t;

    move-result-object v0

    .line 47
    invoke-virtual {v0}, Landroid/support/v4/app/t;->c()I

    .line 48
    return-void
.end method


# virtual methods
.method public a(I)Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lco/uk/getmondo/signup/card_ordering/OrderCardActivity;->b:Ljava/util/HashMap;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lco/uk/getmondo/signup/card_ordering/OrderCardActivity;->b:Ljava/util/HashMap;

    :cond_0
    iget-object v0, p0, Lco/uk/getmondo/signup/card_ordering/OrderCardActivity;->b:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    if-nez v0, :cond_1

    invoke-virtual {p0, p1}, Landroid/support/v4/app/j;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/signup/card_ordering/OrderCardActivity;->b:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    return-object v0
.end method

.method public a(Lco/uk/getmondo/api/model/order_card/CardOrderName$Type;Ljava/lang/String;)V
    .locals 1

    .prologue
    const-string v0, "nameType"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "nameOnCard"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 34
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lco/uk/getmondo/signup/card_ordering/OrderCardActivity;->b(Lco/uk/getmondo/api/model/order_card/CardOrderName$Type;Ljava/lang/String;Ljava/lang/String;)V

    .line 35
    return-void
.end method

.method public a(Lco/uk/getmondo/api/model/order_card/CardOrderName$Type;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    const-string v0, "nameType"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "nameOnCard"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "pin"

    invoke-static {p3, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 38
    invoke-direct {p0, p1, p2, p3}, Lco/uk/getmondo/signup/card_ordering/OrderCardActivity;->b(Lco/uk/getmondo/api/model/order_card/CardOrderName$Type;Ljava/lang/String;Ljava/lang/String;)V

    .line 39
    return-void
.end method

.method public d_()V
    .locals 1

    .prologue
    .line 51
    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/card_ordering/OrderCardActivity;->setResult(I)V

    .line 52
    invoke-virtual {p0}, Lco/uk/getmondo/signup/card_ordering/OrderCardActivity;->finish()V

    .line 53
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 17
    invoke-super {p0, p1}, Lco/uk/getmondo/signup/a;->onCreate(Landroid/os/Bundle;)V

    .line 19
    const v0, 0x7f05004f

    invoke-virtual {p0, v0}, Lco/uk/getmondo/signup/card_ordering/OrderCardActivity;->setContentView(I)V

    .line 20
    invoke-virtual {p0}, Lco/uk/getmondo/signup/card_ordering/OrderCardActivity;->getSupportActionBar()Landroid/support/v7/app/a;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0, v1}, Landroid/support/v7/app/a;->b(Z)V

    .line 21
    :cond_0
    invoke-virtual {p0}, Lco/uk/getmondo/signup/card_ordering/OrderCardActivity;->getSupportActionBar()Landroid/support/v7/app/a;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v0, v1}, Landroid/support/v7/app/a;->d(Z)V

    .line 23
    :cond_1
    if-nez p1, :cond_2

    .line 24
    invoke-virtual {p0}, Lco/uk/getmondo/signup/card_ordering/OrderCardActivity;->getSupportFragmentManager()Landroid/support/v4/app/n;

    move-result-object v0

    .line 25
    invoke-virtual {v0}, Landroid/support/v4/app/n;->a()Landroid/support/v4/app/t;

    move-result-object v0

    .line 26
    const v1, 0x7f1101d1

    sget-object v2, Lco/uk/getmondo/signup/card_ordering/e;->c:Lco/uk/getmondo/signup/card_ordering/e$a;

    invoke-virtual {v2}, Lco/uk/getmondo/signup/card_ordering/e$a;->a()Landroid/support/v4/app/Fragment;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/app/t;->a(ILandroid/support/v4/app/Fragment;)Landroid/support/v4/app/t;

    move-result-object v0

    .line 27
    invoke-virtual {v0}, Landroid/support/v4/app/t;->c()I

    .line 30
    :cond_2
    invoke-virtual {p0}, Lco/uk/getmondo/signup/card_ordering/OrderCardActivity;->l()Lco/uk/getmondo/common/h/b/b;

    move-result-object v0

    invoke-interface {v0, p0}, Lco/uk/getmondo/common/h/b/b;->a(Lco/uk/getmondo/signup/card_ordering/OrderCardActivity;)V

    .line 31
    return-void
.end method
