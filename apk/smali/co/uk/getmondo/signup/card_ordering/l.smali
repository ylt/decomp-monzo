.class public final Lco/uk/getmondo/signup/card_ordering/l;
.super Ljava/lang/Object;
.source "OrderCardManager_Factory.java"

# interfaces
.implements Lb/a/b;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lb/a/b",
        "<",
        "Lco/uk/getmondo/signup/card_ordering/k;",
        ">;"
    }
.end annotation


# static fields
.field static final synthetic a:Z


# instance fields
.field private final b:Ljavax/a/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/api/SignupApi;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 8
    const-class v0, Lco/uk/getmondo/signup/card_ordering/l;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lco/uk/getmondo/signup/card_ordering/l;->a:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Ljavax/a/a;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/api/SignupApi;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    sget-boolean v0, Lco/uk/getmondo/signup/card_ordering/l;->a:Z

    if-nez v0, :cond_0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 17
    :cond_0
    iput-object p1, p0, Lco/uk/getmondo/signup/card_ordering/l;->b:Ljavax/a/a;

    .line 18
    return-void
.end method

.method public static a(Ljavax/a/a;)Lb/a/b;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/api/SignupApi;",
            ">;)",
            "Lb/a/b",
            "<",
            "Lco/uk/getmondo/signup/card_ordering/k;",
            ">;"
        }
    .end annotation

    .prologue
    .line 26
    new-instance v0, Lco/uk/getmondo/signup/card_ordering/l;

    invoke-direct {v0, p0}, Lco/uk/getmondo/signup/card_ordering/l;-><init>(Ljavax/a/a;)V

    return-object v0
.end method


# virtual methods
.method public a()Lco/uk/getmondo/signup/card_ordering/k;
    .locals 2

    .prologue
    .line 22
    new-instance v1, Lco/uk/getmondo/signup/card_ordering/k;

    iget-object v0, p0, Lco/uk/getmondo/signup/card_ordering/l;->b:Ljavax/a/a;

    invoke-interface {v0}, Ljavax/a/a;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/api/SignupApi;

    invoke-direct {v1, v0}, Lco/uk/getmondo/signup/card_ordering/k;-><init>(Lco/uk/getmondo/api/SignupApi;)V

    return-object v1
.end method

.method public synthetic b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 8
    invoke-virtual {p0}, Lco/uk/getmondo/signup/card_ordering/l;->a()Lco/uk/getmondo/signup/card_ordering/k;

    move-result-object v0

    return-object v0
.end method
