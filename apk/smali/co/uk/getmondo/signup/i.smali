.class public final Lco/uk/getmondo/signup/i;
.super Ljava/lang/Object;
.source "SignupApiErrorHandler.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lco/uk/getmondo/signup/i$a;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0003\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\u00c6\u0002\u0018\u00002\u00020\u0001:\u0001\tB\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u0016\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u0008\u00a8\u0006\n"
    }
    d2 = {
        "Lco/uk/getmondo/signup/SignupApiErrorHandler;",
        "",
        "()V",
        "handleError",
        "",
        "throwable",
        "",
        "view",
        "Lco/uk/getmondo/signup/SignupApiErrorHandler$View;",
        "View",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# static fields
.field public static final a:Lco/uk/getmondo/signup/i;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 7
    new-instance v0, Lco/uk/getmondo/signup/i;

    invoke-direct {v0}, Lco/uk/getmondo/signup/i;-><init>()V

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    check-cast p0, Lco/uk/getmondo/signup/i;

    sput-object p0, Lco/uk/getmondo/signup/i;->a:Lco/uk/getmondo/signup/i;

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Throwable;Lco/uk/getmondo/signup/i$a;)Z
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x2

    const/4 v0, 0x0

    const-string v1, "throwable"

    invoke-static {p1, v1}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "view"

    invoke-static {p2, v1}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 10
    invoke-static {p1}, Lco/uk/getmondo/common/e/c;->a(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v1

    .line 11
    const-string v2, "precondition_failed.signup.stage"

    invoke-static {v1, v2, v0, v3, v4}, Lkotlin/h/j;->a(Ljava/lang/String;Ljava/lang/String;ZILjava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 12
    const-string v2, "precondition_failed.signup.status"

    invoke-static {v1, v2, v0, v3, v4}, Lkotlin/h/j;->a(Ljava/lang/String;Ljava/lang/String;ZILjava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 13
    :cond_0
    invoke-interface {p2}, Lco/uk/getmondo/signup/i$a;->b()V

    .line 14
    const/4 v0, 0x1

    .line 16
    :cond_1
    return v0
.end method
