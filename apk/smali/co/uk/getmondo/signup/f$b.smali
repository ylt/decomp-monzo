.class final Lco/uk/getmondo/signup/f$b;
.super Ljava/lang/Object;
.source "EmailPresenter.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lco/uk/getmondo/signup/f;->a(Lco/uk/getmondo/signup/f$a;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h",
        "<TT;",
        "Lio/reactivex/l",
        "<+TR;>;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000\u0018\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0000\u0010\u0000\u001a&\u0012\u000c\u0012\n \u0003*\u0004\u0018\u00010\u00020\u0002 \u0003*\u0012\u0012\u000c\u0012\n \u0003*\u0004\u0018\u00010\u00020\u0002\u0018\u00010\u00010\u00012\u0012\u0010\u0004\u001a\u000e\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\u00060\u0005H\n\u00a2\u0006\u0002\u0008\u0007"
    }
    d2 = {
        "<anonymous>",
        "Lio/reactivex/Maybe;",
        "Lco/uk/getmondo/model/User;",
        "kotlin.jvm.PlatformType",
        "codeAndState",
        "Landroid/support/v4/util/Pair;",
        "",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field final synthetic a:Lco/uk/getmondo/signup/f;

.field final synthetic b:Lco/uk/getmondo/signup/f$a;


# direct methods
.method constructor <init>(Lco/uk/getmondo/signup/f;Lco/uk/getmondo/signup/f$a;)V
    .locals 0

    iput-object p1, p0, Lco/uk/getmondo/signup/f$b;->a:Lco/uk/getmondo/signup/f;

    iput-object p2, p0, Lco/uk/getmondo/signup/f$b;->b:Lco/uk/getmondo/signup/f$a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/support/v4/g/j;)Lio/reactivex/h;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/g/j",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)",
            "Lio/reactivex/h",
            "<",
            "Lco/uk/getmondo/d/ak;",
            ">;"
        }
    .end annotation

    .prologue
    const-string v0, "codeAndState"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 48
    iget-object v0, p0, Lco/uk/getmondo/signup/f$b;->a:Lco/uk/getmondo/signup/f;

    invoke-static {v0}, Lco/uk/getmondo/signup/f;->a(Lco/uk/getmondo/signup/f;)Lco/uk/getmondo/api/b/a;

    move-result-object v2

    iget-object v0, p1, Landroid/support/v4/g/j;->a:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    iget-object v1, p1, Landroid/support/v4/g/j;->b:Ljava/lang/Object;

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v2, v0, v1}, Lco/uk/getmondo/api/b/a;->a(Ljava/lang/String;Ljava/lang/String;)Lio/reactivex/v;

    move-result-object v0

    .line 49
    invoke-virtual {v0}, Lio/reactivex/v;->e()Lio/reactivex/h;

    move-result-object v0

    .line 50
    iget-object v1, p0, Lco/uk/getmondo/signup/f$b;->a:Lco/uk/getmondo/signup/f;

    invoke-static {v1}, Lco/uk/getmondo/signup/f;->b(Lco/uk/getmondo/signup/f;)Lio/reactivex/u;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/h;->a(Lio/reactivex/u;)Lio/reactivex/h;

    move-result-object v0

    .line 51
    iget-object v1, p0, Lco/uk/getmondo/signup/f$b;->a:Lco/uk/getmondo/signup/f;

    invoke-static {v1}, Lco/uk/getmondo/signup/f;->c(Lco/uk/getmondo/signup/f;)Lio/reactivex/u;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/h;->b(Lio/reactivex/u;)Lio/reactivex/h;

    move-result-object v1

    .line 52
    new-instance v0, Lco/uk/getmondo/signup/f$b$1;

    invoke-direct {v0, p0}, Lco/uk/getmondo/signup/f$b$1;-><init>(Lco/uk/getmondo/signup/f$b;)V

    check-cast v0, Lio/reactivex/c/g;

    invoke-virtual {v1, v0}, Lio/reactivex/h;->a(Lio/reactivex/c/g;)Lio/reactivex/h;

    move-result-object v1

    .line 68
    invoke-static {}, Lio/reactivex/h;->a()Lio/reactivex/h;

    move-result-object v0

    check-cast v0, Lio/reactivex/l;

    invoke-virtual {v1, v0}, Lio/reactivex/h;->a(Lio/reactivex/l;)Lio/reactivex/h;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 30
    check-cast p1, Landroid/support/v4/g/j;

    invoke-virtual {p0, p1}, Lco/uk/getmondo/signup/f$b;->a(Landroid/support/v4/g/j;)Lio/reactivex/h;

    move-result-object v0

    return-object v0
.end method
