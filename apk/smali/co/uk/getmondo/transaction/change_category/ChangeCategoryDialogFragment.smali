.class public Lco/uk/getmondo/transaction/change_category/ChangeCategoryDialogFragment;
.super Landroid/app/DialogFragment;
.source "ChangeCategoryDialogFragment.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lco/uk/getmondo/transaction/change_category/ChangeCategoryDialogFragment$b;,
        Lco/uk/getmondo/transaction/change_category/ChangeCategoryDialogFragment$a;
    }
.end annotation


# instance fields
.field private a:Lbutterknife/Unbinder;

.field private b:Lco/uk/getmondo/transaction/change_category/ChangeCategoryDialogFragment$a;

.field private c:Lco/uk/getmondo/transaction/change_category/ChangeCategoryDialogFragment$b;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    return-void
.end method

.method public static a(Ljava/lang/String;)Lco/uk/getmondo/transaction/change_category/ChangeCategoryDialogFragment;
    .locals 3

    .prologue
    .line 26
    new-instance v0, Lco/uk/getmondo/transaction/change_category/ChangeCategoryDialogFragment;

    invoke-direct {v0}, Lco/uk/getmondo/transaction/change_category/ChangeCategoryDialogFragment;-><init>()V

    .line 27
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 28
    const-string v2, "KEY_TRANSACTION_ID"

    invoke-virtual {v1, v2, p0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 29
    invoke-virtual {v0, v1}, Lco/uk/getmondo/transaction/change_category/ChangeCategoryDialogFragment;->setArguments(Landroid/os/Bundle;)V

    .line 30
    return-object v0
.end method


# virtual methods
.method public a(Lco/uk/getmondo/transaction/change_category/ChangeCategoryDialogFragment$a;)V
    .locals 0

    .prologue
    .line 34
    iput-object p1, p0, Lco/uk/getmondo/transaction/change_category/ChangeCategoryDialogFragment;->b:Lco/uk/getmondo/transaction/change_category/ChangeCategoryDialogFragment$a;

    .line 35
    return-void
.end method

.method public a(Lco/uk/getmondo/transaction/change_category/ChangeCategoryDialogFragment$b;)V
    .locals 0

    .prologue
    .line 101
    iput-object p1, p0, Lco/uk/getmondo/transaction/change_category/ChangeCategoryDialogFragment;->c:Lco/uk/getmondo/transaction/change_category/ChangeCategoryDialogFragment$b;

    .line 102
    return-void
.end method

.method onCategoryClicked(Landroid/view/ViewGroup;)V
    .locals 2
    .annotation build Lbutterknife/OnClick;
        value = {
            0x7f1102bd,
            0x7f1102c1,
            0x7f1102be,
            0x7f1102c2,
            0x7f1102bf,
            0x7f1102c3,
            0x7f1102bc,
            0x7f1102c4,
            0x7f1102c0,
            0x7f1102c5
        }
    .end annotation

    .prologue
    .line 57
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 89
    :goto_0
    invoke-virtual {p0}, Lco/uk/getmondo/transaction/change_category/ChangeCategoryDialogFragment;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    .line 90
    return-void

    .line 59
    :pswitch_0
    iget-object v0, p0, Lco/uk/getmondo/transaction/change_category/ChangeCategoryDialogFragment;->b:Lco/uk/getmondo/transaction/change_category/ChangeCategoryDialogFragment$a;

    sget-object v1, Lco/uk/getmondo/d/h;->TRANSPORT:Lco/uk/getmondo/d/h;

    invoke-interface {v0, v1}, Lco/uk/getmondo/transaction/change_category/ChangeCategoryDialogFragment$a;->a(Lco/uk/getmondo/d/h;)V

    goto :goto_0

    .line 62
    :pswitch_1
    iget-object v0, p0, Lco/uk/getmondo/transaction/change_category/ChangeCategoryDialogFragment;->b:Lco/uk/getmondo/transaction/change_category/ChangeCategoryDialogFragment$a;

    sget-object v1, Lco/uk/getmondo/d/h;->GROCERIES:Lco/uk/getmondo/d/h;

    invoke-interface {v0, v1}, Lco/uk/getmondo/transaction/change_category/ChangeCategoryDialogFragment$a;->a(Lco/uk/getmondo/d/h;)V

    goto :goto_0

    .line 65
    :pswitch_2
    iget-object v0, p0, Lco/uk/getmondo/transaction/change_category/ChangeCategoryDialogFragment;->b:Lco/uk/getmondo/transaction/change_category/ChangeCategoryDialogFragment$a;

    sget-object v1, Lco/uk/getmondo/d/h;->EATING_OUT:Lco/uk/getmondo/d/h;

    invoke-interface {v0, v1}, Lco/uk/getmondo/transaction/change_category/ChangeCategoryDialogFragment$a;->a(Lco/uk/getmondo/d/h;)V

    goto :goto_0

    .line 68
    :pswitch_3
    iget-object v0, p0, Lco/uk/getmondo/transaction/change_category/ChangeCategoryDialogFragment;->b:Lco/uk/getmondo/transaction/change_category/ChangeCategoryDialogFragment$a;

    sget-object v1, Lco/uk/getmondo/d/h;->CASH:Lco/uk/getmondo/d/h;

    invoke-interface {v0, v1}, Lco/uk/getmondo/transaction/change_category/ChangeCategoryDialogFragment$a;->a(Lco/uk/getmondo/d/h;)V

    goto :goto_0

    .line 71
    :pswitch_4
    iget-object v0, p0, Lco/uk/getmondo/transaction/change_category/ChangeCategoryDialogFragment;->b:Lco/uk/getmondo/transaction/change_category/ChangeCategoryDialogFragment$a;

    sget-object v1, Lco/uk/getmondo/d/h;->BILLS:Lco/uk/getmondo/d/h;

    invoke-interface {v0, v1}, Lco/uk/getmondo/transaction/change_category/ChangeCategoryDialogFragment$a;->a(Lco/uk/getmondo/d/h;)V

    goto :goto_0

    .line 74
    :pswitch_5
    iget-object v0, p0, Lco/uk/getmondo/transaction/change_category/ChangeCategoryDialogFragment;->b:Lco/uk/getmondo/transaction/change_category/ChangeCategoryDialogFragment$a;

    sget-object v1, Lco/uk/getmondo/d/h;->ENTERTAINMENT:Lco/uk/getmondo/d/h;

    invoke-interface {v0, v1}, Lco/uk/getmondo/transaction/change_category/ChangeCategoryDialogFragment$a;->a(Lco/uk/getmondo/d/h;)V

    goto :goto_0

    .line 77
    :pswitch_6
    iget-object v0, p0, Lco/uk/getmondo/transaction/change_category/ChangeCategoryDialogFragment;->b:Lco/uk/getmondo/transaction/change_category/ChangeCategoryDialogFragment$a;

    sget-object v1, Lco/uk/getmondo/d/h;->SHOPPING:Lco/uk/getmondo/d/h;

    invoke-interface {v0, v1}, Lco/uk/getmondo/transaction/change_category/ChangeCategoryDialogFragment$a;->a(Lco/uk/getmondo/d/h;)V

    goto :goto_0

    .line 80
    :pswitch_7
    iget-object v0, p0, Lco/uk/getmondo/transaction/change_category/ChangeCategoryDialogFragment;->b:Lco/uk/getmondo/transaction/change_category/ChangeCategoryDialogFragment$a;

    sget-object v1, Lco/uk/getmondo/d/h;->HOLIDAYS:Lco/uk/getmondo/d/h;

    invoke-interface {v0, v1}, Lco/uk/getmondo/transaction/change_category/ChangeCategoryDialogFragment$a;->a(Lco/uk/getmondo/d/h;)V

    goto :goto_0

    .line 83
    :pswitch_8
    iget-object v0, p0, Lco/uk/getmondo/transaction/change_category/ChangeCategoryDialogFragment;->b:Lco/uk/getmondo/transaction/change_category/ChangeCategoryDialogFragment$a;

    sget-object v1, Lco/uk/getmondo/d/h;->GENERAL:Lco/uk/getmondo/d/h;

    invoke-interface {v0, v1}, Lco/uk/getmondo/transaction/change_category/ChangeCategoryDialogFragment$a;->a(Lco/uk/getmondo/d/h;)V

    goto :goto_0

    .line 86
    :pswitch_9
    iget-object v0, p0, Lco/uk/getmondo/transaction/change_category/ChangeCategoryDialogFragment;->b:Lco/uk/getmondo/transaction/change_category/ChangeCategoryDialogFragment$a;

    sget-object v1, Lco/uk/getmondo/d/h;->EXPENSES:Lco/uk/getmondo/d/h;

    invoke-interface {v0, v1}, Lco/uk/getmondo/transaction/change_category/ChangeCategoryDialogFragment$a;->a(Lco/uk/getmondo/d/h;)V

    goto :goto_0

    .line 57
    nop

    :pswitch_data_0
    .packed-switch 0x7f1102bc
        :pswitch_6
        :pswitch_0
        :pswitch_2
        :pswitch_4
        :pswitch_8
        :pswitch_1
        :pswitch_3
        :pswitch_5
        :pswitch_7
        :pswitch_9
    .end packed-switch
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 4

    .prologue
    .line 39
    invoke-virtual {p0}, Lco/uk/getmondo/transaction/change_category/ChangeCategoryDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f050093

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 40
    invoke-static {p0, v0}, Lbutterknife/ButterKnife;->bind(Ljava/lang/Object;Landroid/view/View;)Lbutterknife/Unbinder;

    move-result-object v1

    iput-object v1, p0, Lco/uk/getmondo/transaction/change_category/ChangeCategoryDialogFragment;->a:Lbutterknife/Unbinder;

    .line 41
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lco/uk/getmondo/transaction/change_category/ChangeCategoryDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 42
    invoke-virtual {v1, v0}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 43
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 41
    return-object v0
.end method

.method public onDestroyView()V
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lco/uk/getmondo/transaction/change_category/ChangeCategoryDialogFragment;->a:Lbutterknife/Unbinder;

    invoke-interface {v0}, Lbutterknife/Unbinder;->unbind()V

    .line 49
    invoke-super {p0}, Landroid/app/DialogFragment;->onDestroyView()V

    .line 50
    return-void
.end method

.method public onDismiss(Landroid/content/DialogInterface;)V
    .locals 1

    .prologue
    .line 94
    iget-object v0, p0, Lco/uk/getmondo/transaction/change_category/ChangeCategoryDialogFragment;->c:Lco/uk/getmondo/transaction/change_category/ChangeCategoryDialogFragment$b;

    if-eqz v0, :cond_0

    .line 95
    iget-object v0, p0, Lco/uk/getmondo/transaction/change_category/ChangeCategoryDialogFragment;->c:Lco/uk/getmondo/transaction/change_category/ChangeCategoryDialogFragment$b;

    invoke-interface {v0}, Lco/uk/getmondo/transaction/change_category/ChangeCategoryDialogFragment$b;->a()V

    .line 97
    :cond_0
    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onDismiss(Landroid/content/DialogInterface;)V

    .line 98
    return-void
.end method
