.class Lco/uk/getmondo/transaction/a/a$a;
.super Ljava/lang/Object;
.source "TransactionManager.java"

# interfaces
.implements Lio/reactivex/c/c;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lco/uk/getmondo/transaction/a/a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lio/reactivex/c/c",
        "<",
        "Ljava/lang/Long;",
        "Ljava/lang/Long;",
        "Lco/uk/getmondo/transaction/details/c/a;",
        ">;"
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 155
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lco/uk/getmondo/transaction/a/a$1;)V
    .locals 0

    .prologue
    .line 155
    invoke-direct {p0}, Lco/uk/getmondo/transaction/a/a$a;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Ljava/lang/Long;Ljava/lang/Long;)Lco/uk/getmondo/transaction/details/c/a;
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    const-wide/16 v4, 0x0

    .line 158
    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    cmp-long v0, v0, v4

    if-nez v0, :cond_0

    .line 159
    new-instance v0, Lco/uk/getmondo/transaction/details/c/a;

    new-instance v1, Lco/uk/getmondo/d/c;

    sget-object v2, Lco/uk/getmondo/common/i/c;->a:Lco/uk/getmondo/common/i/c;

    invoke-direct {v1, v4, v5, v2}, Lco/uk/getmondo/d/c;-><init>(JLco/uk/getmondo/common/i/c;)V

    new-instance v2, Lco/uk/getmondo/d/c;

    sget-object v3, Lco/uk/getmondo/common/i/c;->a:Lco/uk/getmondo/common/i/c;

    invoke-direct {v2, v4, v5, v3}, Lco/uk/getmondo/d/c;-><init>(JLco/uk/getmondo/common/i/c;)V

    invoke-direct {v0, v4, v5, v1, v2}, Lco/uk/getmondo/transaction/details/c/a;-><init>(JLco/uk/getmondo/d/c;Lco/uk/getmondo/d/c;)V

    .line 162
    :goto_0
    return-object v0

    .line 161
    :cond_0
    invoke-virtual {p2}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    div-long v2, v0, v2

    .line 162
    new-instance v0, Lco/uk/getmondo/transaction/details/c/a;

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    new-instance v1, Lco/uk/getmondo/d/c;

    sget-object v6, Lco/uk/getmondo/common/i/c;->a:Lco/uk/getmondo/common/i/c;

    invoke-direct {v1, v2, v3, v6}, Lco/uk/getmondo/d/c;-><init>(JLco/uk/getmondo/common/i/c;)V

    new-instance v2, Lco/uk/getmondo/d/c;

    invoke-virtual {p2}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    sget-object v3, Lco/uk/getmondo/common/i/c;->a:Lco/uk/getmondo/common/i/c;

    invoke-direct {v2, v6, v7, v3}, Lco/uk/getmondo/d/c;-><init>(JLco/uk/getmondo/common/i/c;)V

    invoke-direct {v0, v4, v5, v1, v2}, Lco/uk/getmondo/transaction/details/c/a;-><init>(JLco/uk/getmondo/d/c;Lco/uk/getmondo/d/c;)V

    goto :goto_0
.end method

.method public bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 155
    check-cast p1, Ljava/lang/Long;

    check-cast p2, Ljava/lang/Long;

    invoke-virtual {p0, p1, p2}, Lco/uk/getmondo/transaction/a/a$a;->a(Ljava/lang/Long;Ljava/lang/Long;)Lco/uk/getmondo/transaction/details/c/a;

    move-result-object v0

    return-object v0
.end method
