.class final synthetic Lco/uk/getmondo/transaction/a/c;
.super Ljava/lang/Object;

# interfaces
.implements Lio/reactivex/c/h;


# instance fields
.field private final a:Lco/uk/getmondo/transaction/a/a;

.field private final b:Ljava/io/InputStream;

.field private final c:J


# direct methods
.method private constructor <init>(Lco/uk/getmondo/transaction/a/a;Ljava/io/InputStream;J)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lco/uk/getmondo/transaction/a/c;->a:Lco/uk/getmondo/transaction/a/a;

    iput-object p2, p0, Lco/uk/getmondo/transaction/a/c;->b:Ljava/io/InputStream;

    iput-wide p3, p0, Lco/uk/getmondo/transaction/a/c;->c:J

    return-void
.end method

.method public static a(Lco/uk/getmondo/transaction/a/a;Ljava/io/InputStream;J)Lio/reactivex/c/h;
    .locals 2

    new-instance v0, Lco/uk/getmondo/transaction/a/c;

    invoke-direct {v0, p0, p1, p2, p3}, Lco/uk/getmondo/transaction/a/c;-><init>(Lco/uk/getmondo/transaction/a/a;Ljava/io/InputStream;J)V

    return-object v0
.end method


# virtual methods
.method public a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4

    iget-object v0, p0, Lco/uk/getmondo/transaction/a/c;->a:Lco/uk/getmondo/transaction/a/a;

    iget-object v1, p0, Lco/uk/getmondo/transaction/a/c;->b:Ljava/io/InputStream;

    iget-wide v2, p0, Lco/uk/getmondo/transaction/a/c;->c:J

    check-cast p1, Lco/uk/getmondo/api/model/identity_verification/ApiUploadContainer;

    invoke-static {v0, v1, v2, v3, p1}, Lco/uk/getmondo/transaction/a/a;->a(Lco/uk/getmondo/transaction/a/a;Ljava/io/InputStream;JLco/uk/getmondo/api/model/identity_verification/ApiUploadContainer;)Lio/reactivex/z;

    move-result-object v0

    return-object v0
.end method
