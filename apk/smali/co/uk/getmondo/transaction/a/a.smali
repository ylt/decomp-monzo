.class public Lco/uk/getmondo/transaction/a/a;
.super Ljava/lang/Object;
.source "TransactionManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lco/uk/getmondo/transaction/a/a$b;,
        Lco/uk/getmondo/transaction/a/a$a;,
        Lco/uk/getmondo/transaction/a/a$c;,
        Lco/uk/getmondo/transaction/a/a$d;
    }
.end annotation


# instance fields
.field private final a:Lco/uk/getmondo/api/MonzoApi;

.field private final b:Lco/uk/getmondo/transaction/a/j;

.field private final c:Lco/uk/getmondo/api/ae;

.field private final d:Lco/uk/getmondo/d/a/b;


# direct methods
.method public constructor <init>(Lco/uk/getmondo/api/MonzoApi;Lco/uk/getmondo/transaction/a/j;Lco/uk/getmondo/api/ae;)V
    .locals 1

    .prologue
    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    new-instance v0, Lco/uk/getmondo/d/a/b;

    invoke-direct {v0}, Lco/uk/getmondo/d/a/b;-><init>()V

    iput-object v0, p0, Lco/uk/getmondo/transaction/a/a;->d:Lco/uk/getmondo/d/a/b;

    .line 45
    iput-object p1, p0, Lco/uk/getmondo/transaction/a/a;->a:Lco/uk/getmondo/api/MonzoApi;

    .line 46
    iput-object p2, p0, Lco/uk/getmondo/transaction/a/a;->b:Lco/uk/getmondo/transaction/a/j;

    .line 47
    iput-object p3, p0, Lco/uk/getmondo/transaction/a/a;->c:Lco/uk/getmondo/api/ae;

    .line 48
    return-void
.end method

.method static synthetic a(Lco/uk/getmondo/transaction/a/a;Lco/uk/getmondo/api/model/ApiAttachmentResponse;)Lco/uk/getmondo/d/d;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 78
    iget-object v0, p0, Lco/uk/getmondo/transaction/a/a;->d:Lco/uk/getmondo/d/a/b;

    invoke-virtual {p1}, Lco/uk/getmondo/api/model/ApiAttachmentResponse;->a()Lco/uk/getmondo/api/model/feed/ApiAttachment;

    move-result-object v1

    invoke-virtual {v0, v1}, Lco/uk/getmondo/d/a/b;->a(Lco/uk/getmondo/api/model/feed/ApiAttachment;)Lco/uk/getmondo/d/d;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lco/uk/getmondo/transaction/a/a;Ljava/lang/String;Lco/uk/getmondo/d/d;)Lio/reactivex/d;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 79
    iget-object v0, p0, Lco/uk/getmondo/transaction/a/a;->b:Lco/uk/getmondo/transaction/a/j;

    invoke-virtual {v0, p2, p1}, Lco/uk/getmondo/transaction/a/j;->a(Lco/uk/getmondo/d/d;Ljava/lang/String;)Lio/reactivex/b;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lco/uk/getmondo/transaction/a/a;Ljava/lang/String;Ljava/lang/String;Lco/uk/getmondo/api/model/ApiTransactionResponse;)Lio/reactivex/d;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 89
    iget-object v0, p0, Lco/uk/getmondo/transaction/a/a;->b:Lco/uk/getmondo/transaction/a/j;

    invoke-virtual {v0, p1, p2}, Lco/uk/getmondo/transaction/a/j;->a(Ljava/lang/String;Ljava/lang/String;)Lio/reactivex/b;

    move-result-object v0

    return-object v0
.end method

.method private a()Lio/reactivex/v;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/v",
            "<",
            "Lco/uk/getmondo/transaction/details/c/e;",
            ">;"
        }
    .end annotation

    .prologue
    .line 131
    iget-object v0, p0, Lco/uk/getmondo/transaction/a/a;->b:Lco/uk/getmondo/transaction/a/j;

    invoke-virtual {v0}, Lco/uk/getmondo/transaction/a/j;->a()Lio/reactivex/v;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/transaction/a/a;->b:Lco/uk/getmondo/transaction/a/j;

    .line 132
    invoke-virtual {v1}, Lco/uk/getmondo/transaction/a/j;->b()Lio/reactivex/v;

    move-result-object v1

    new-instance v2, Lco/uk/getmondo/transaction/a/a$c;

    const/4 v3, 0x0

    invoke-direct {v2, v3}, Lco/uk/getmondo/transaction/a/a$c;-><init>(Lco/uk/getmondo/transaction/a/a$1;)V

    .line 131
    invoke-static {v0, v1, v2}, Lio/reactivex/v;->a(Lio/reactivex/z;Lio/reactivex/z;Lio/reactivex/c/c;)Lio/reactivex/v;

    move-result-object v0

    return-object v0
.end method

.method private a(Lco/uk/getmondo/d/aa;)Lio/reactivex/v;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lco/uk/getmondo/d/aa;",
            ")",
            "Lio/reactivex/v",
            "<",
            "Lco/uk/getmondo/transaction/details/c/f;",
            ">;"
        }
    .end annotation

    .prologue
    .line 120
    iget-object v0, p0, Lco/uk/getmondo/transaction/a/a;->b:Lco/uk/getmondo/transaction/a/j;

    invoke-virtual {p1}, Lco/uk/getmondo/d/aa;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lco/uk/getmondo/transaction/a/j;->d(Ljava/lang/String;)Lio/reactivex/v;

    move-result-object v0

    new-instance v1, Lco/uk/getmondo/transaction/a/a$b;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lco/uk/getmondo/transaction/a/a$b;-><init>(Lco/uk/getmondo/transaction/a/a$1;)V

    .line 121
    invoke-virtual {v0, v1}, Lio/reactivex/v;->d(Lio/reactivex/c/h;)Lio/reactivex/v;

    move-result-object v0

    .line 120
    return-object v0
.end method

.method private a(Lco/uk/getmondo/payments/send/data/a/a;)Lio/reactivex/v;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lco/uk/getmondo/payments/send/data/a/a;",
            ")",
            "Lio/reactivex/v",
            "<",
            "Lco/uk/getmondo/transaction/details/c/f;",
            ">;"
        }
    .end annotation

    .prologue
    .line 115
    iget-object v0, p0, Lco/uk/getmondo/transaction/a/a;->b:Lco/uk/getmondo/transaction/a/j;

    invoke-virtual {p1}, Lco/uk/getmondo/payments/send/data/a/a;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lco/uk/getmondo/payments/send/data/a/a;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lco/uk/getmondo/transaction/a/j;->b(Ljava/lang/String;Ljava/lang/String;)Lio/reactivex/v;

    move-result-object v0

    new-instance v1, Lco/uk/getmondo/transaction/a/a$b;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lco/uk/getmondo/transaction/a/a$b;-><init>(Lco/uk/getmondo/transaction/a/a$1;)V

    .line 116
    invoke-virtual {v0, v1}, Lio/reactivex/v;->d(Lio/reactivex/c/h;)Lio/reactivex/v;

    move-result-object v0

    .line 115
    return-object v0
.end method

.method static synthetic a(Lco/uk/getmondo/transaction/a/a;Ljava/io/InputStream;JLco/uk/getmondo/api/model/identity_verification/ApiUploadContainer;)Lio/reactivex/z;
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 75
    iget-object v1, p0, Lco/uk/getmondo/transaction/a/a;->c:Lco/uk/getmondo/api/ae;

    invoke-virtual {p4}, Lco/uk/getmondo/api/model/identity_verification/ApiUploadContainer;->b()Ljava/lang/String;

    move-result-object v2

    const-string v6, "image/jpeg"

    move-object v3, p1

    move-wide v4, p2

    invoke-virtual/range {v1 .. v6}, Lco/uk/getmondo/api/ae;->a(Ljava/lang/String;Ljava/io/InputStream;JLjava/lang/String;)Lio/reactivex/b;

    move-result-object v0

    .line 76
    invoke-virtual {v0, p4}, Lio/reactivex/b;->a(Ljava/lang/Object;)Lio/reactivex/v;

    move-result-object v0

    .line 75
    return-object v0
.end method

.method static synthetic a(Lco/uk/getmondo/transaction/a/a;Ljava/lang/String;Ljava/lang/String;Lco/uk/getmondo/api/model/identity_verification/ApiUploadContainer;)Lio/reactivex/z;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 77
    iget-object v0, p0, Lco/uk/getmondo/transaction/a/a;->a:Lco/uk/getmondo/api/MonzoApi;

    invoke-virtual {p3}, Lco/uk/getmondo/api/model/identity_verification/ApiUploadContainer;->a()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, p1, p2, v1}, Lco/uk/getmondo/api/MonzoApi;->registerAttachment(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lio/reactivex/v;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Ljava/lang/String;Lco/uk/getmondo/d/h;Lio/realm/av;)V
    .locals 2

    .prologue
    .line 64
    const-class v0, Lco/uk/getmondo/d/aj;

    invoke-virtual {p2, v0}, Lio/realm/av;->a(Ljava/lang/Class;)Lio/realm/bf;

    move-result-object v0

    const-string v1, "id"

    .line 65
    invoke-virtual {v0, v1, p0}, Lio/realm/bf;->a(Ljava/lang/String;Ljava/lang/String;)Lio/realm/bf;

    move-result-object v0

    .line 66
    invoke-virtual {v0}, Lio/realm/bf;->h()Lio/realm/bb;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/d/aj;

    .line 67
    invoke-virtual {p1}, Lco/uk/getmondo/d/h;->f()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lco/uk/getmondo/d/aj;->a(Ljava/lang/String;)V

    .line 68
    return-void
.end method

.method static synthetic b(Ljava/lang/String;Lco/uk/getmondo/d/h;)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 62
    invoke-static {}, Lio/realm/av;->n()Lio/realm/av;

    move-result-object v2

    const/4 v1, 0x0

    .line 63
    :try_start_0
    invoke-static {p0, p1}, Lco/uk/getmondo/transaction/a/h;->a(Ljava/lang/String;Lco/uk/getmondo/d/h;)Lio/realm/av$a;

    move-result-object v0

    invoke-virtual {v2, v0}, Lio/realm/av;->a(Lio/realm/av$a;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 69
    if-eqz v2, :cond_0

    if-eqz v1, :cond_1

    :try_start_1
    invoke-virtual {v2}, Lio/realm/av;->close()V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_1

    .line 70
    :cond_0
    :goto_0
    return-void

    .line 69
    :cond_1
    invoke-virtual {v2}, Lio/realm/av;->close()V

    goto :goto_0

    .line 62
    :catch_0
    move-exception v0

    :try_start_2
    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 69
    :catchall_0
    move-exception v1

    move-object v3, v1

    move-object v1, v0

    move-object v0, v3

    :goto_1
    if-eqz v2, :cond_2

    if-eqz v1, :cond_3

    :try_start_3
    invoke-virtual {v2}, Lio/realm/av;->close()V
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_2

    :cond_2
    :goto_2
    throw v0

    :cond_3
    invoke-virtual {v2}, Lio/realm/av;->close()V

    goto :goto_2

    :catch_1
    move-exception v0

    goto :goto_0

    :catch_2
    move-exception v1

    goto :goto_2

    :catchall_1
    move-exception v0

    goto :goto_1
.end method

.method private d(Ljava/lang/String;)Lio/reactivex/v;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/v",
            "<",
            "Lco/uk/getmondo/transaction/details/c/f;",
            ">;"
        }
    .end annotation

    .prologue
    .line 109
    iget-object v0, p0, Lco/uk/getmondo/transaction/a/a;->b:Lco/uk/getmondo/transaction/a/j;

    invoke-virtual {v0, p1}, Lco/uk/getmondo/transaction/a/j;->e(Ljava/lang/String;)Lio/reactivex/v;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/transaction/a/a;->b:Lco/uk/getmondo/transaction/a/j;

    .line 110
    invoke-virtual {v1, p1}, Lco/uk/getmondo/transaction/a/j;->f(Ljava/lang/String;)Lio/reactivex/v;

    move-result-object v1

    new-instance v2, Lco/uk/getmondo/transaction/a/a$a;

    const/4 v3, 0x0

    invoke-direct {v2, v3}, Lco/uk/getmondo/transaction/a/a$a;-><init>(Lco/uk/getmondo/transaction/a/a$1;)V

    .line 109
    invoke-static {v0, v1, v2}, Lio/reactivex/v;->a(Lio/reactivex/z;Lio/reactivex/z;Lio/reactivex/c/c;)Lio/reactivex/v;

    move-result-object v0

    return-object v0
.end method

.method private e(Ljava/lang/String;)Lio/reactivex/v;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/v",
            "<",
            "Lco/uk/getmondo/transaction/details/c/e;",
            ">;"
        }
    .end annotation

    .prologue
    .line 125
    iget-object v0, p0, Lco/uk/getmondo/transaction/a/a;->b:Lco/uk/getmondo/transaction/a/j;

    invoke-virtual {v0, p1}, Lco/uk/getmondo/transaction/a/j;->g(Ljava/lang/String;)Lio/reactivex/v;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/transaction/a/a;->b:Lco/uk/getmondo/transaction/a/j;

    .line 126
    invoke-virtual {v1, p1}, Lco/uk/getmondo/transaction/a/j;->h(Ljava/lang/String;)Lio/reactivex/v;

    move-result-object v1

    new-instance v2, Lco/uk/getmondo/transaction/a/a$c;

    const/4 v3, 0x0

    invoke-direct {v2, v3}, Lco/uk/getmondo/transaction/a/a$c;-><init>(Lco/uk/getmondo/transaction/a/a$1;)V

    .line 125
    invoke-static {v0, v1, v2}, Lio/reactivex/v;->a(Lio/reactivex/z;Lio/reactivex/z;Lio/reactivex/c/c;)Lio/reactivex/v;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public a(Ljava/lang/String;)Lco/uk/getmondo/d/aj;
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lco/uk/getmondo/transaction/a/a;->b:Lco/uk/getmondo/transaction/a/j;

    invoke-virtual {v0, p1}, Lco/uk/getmondo/transaction/a/j;->a(Ljava/lang/String;)Lco/uk/getmondo/d/aj;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/lang/String;Lco/uk/getmondo/d/h;)Lio/reactivex/b;
    .locals 2

    .prologue
    .line 60
    iget-object v0, p0, Lco/uk/getmondo/transaction/a/a;->a:Lco/uk/getmondo/api/MonzoApi;

    invoke-virtual {p2}, Lco/uk/getmondo/d/h;->f()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Lco/uk/getmondo/api/MonzoApi;->updateTransactionCategory(Ljava/lang/String;Ljava/lang/String;)Lio/reactivex/b;

    move-result-object v0

    invoke-static {p1, p2}, Lco/uk/getmondo/transaction/a/b;->a(Ljava/lang/String;Lco/uk/getmondo/d/h;)Lio/reactivex/c/a;

    move-result-object v1

    .line 61
    invoke-static {v1}, Lio/reactivex/b;->a(Lio/reactivex/c/a;)Lio/reactivex/b;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/b;->b(Lio/reactivex/d;)Lio/reactivex/b;

    move-result-object v0

    .line 60
    return-object v0
.end method

.method public a(Ljava/lang/String;Ljava/io/InputStream;JLjava/lang/String;)Lio/reactivex/b;
    .locals 3

    .prologue
    .line 74
    iget-object v0, p0, Lco/uk/getmondo/transaction/a/a;->a:Lco/uk/getmondo/api/MonzoApi;

    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "image/jpeg"

    invoke-interface {v0, p1, v1, v2}, Lco/uk/getmondo/api/MonzoApi;->createAttachmentUploadUrl(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lio/reactivex/v;

    move-result-object v0

    invoke-static {p0, p2, p3, p4}, Lco/uk/getmondo/transaction/a/c;->a(Lco/uk/getmondo/transaction/a/a;Ljava/io/InputStream;J)Lio/reactivex/c/h;

    move-result-object v1

    .line 75
    invoke-virtual {v0, v1}, Lio/reactivex/v;->a(Lio/reactivex/c/h;)Lio/reactivex/v;

    move-result-object v0

    invoke-static {p0, p1, p5}, Lco/uk/getmondo/transaction/a/d;->a(Lco/uk/getmondo/transaction/a/a;Ljava/lang/String;Ljava/lang/String;)Lio/reactivex/c/h;

    move-result-object v1

    .line 77
    invoke-virtual {v0, v1}, Lio/reactivex/v;->a(Lio/reactivex/c/h;)Lio/reactivex/v;

    move-result-object v0

    invoke-static {p0}, Lco/uk/getmondo/transaction/a/e;->a(Lco/uk/getmondo/transaction/a/a;)Lio/reactivex/c/h;

    move-result-object v1

    .line 78
    invoke-virtual {v0, v1}, Lio/reactivex/v;->d(Lio/reactivex/c/h;)Lio/reactivex/v;

    move-result-object v0

    invoke-static {p0, p5}, Lco/uk/getmondo/transaction/a/f;->a(Lco/uk/getmondo/transaction/a/a;Ljava/lang/String;)Lio/reactivex/c/h;

    move-result-object v1

    .line 79
    invoke-virtual {v0, v1}, Lio/reactivex/v;->c(Lio/reactivex/c/h;)Lio/reactivex/b;

    move-result-object v0

    .line 74
    return-object v0
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;)Lio/reactivex/b;
    .locals 2

    .prologue
    .line 88
    iget-object v0, p0, Lco/uk/getmondo/transaction/a/a;->a:Lco/uk/getmondo/api/MonzoApi;

    invoke-interface {v0, p2, p1}, Lco/uk/getmondo/api/MonzoApi;->updateNotes(Ljava/lang/String;Ljava/lang/String;)Lio/reactivex/v;

    move-result-object v0

    invoke-static {p0, p2, p1}, Lco/uk/getmondo/transaction/a/g;->a(Lco/uk/getmondo/transaction/a/a;Ljava/lang/String;Ljava/lang/String;)Lio/reactivex/c/h;

    move-result-object v1

    .line 89
    invoke-virtual {v0, v1}, Lio/reactivex/v;->c(Lio/reactivex/c/h;)Lio/reactivex/b;

    move-result-object v0

    .line 88
    return-object v0
.end method

.method public a(Lco/uk/getmondo/d/aj;)Lio/reactivex/v;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lco/uk/getmondo/d/aj;",
            ")",
            "Lio/reactivex/v",
            "<",
            "Lco/uk/getmondo/transaction/details/c/f;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 93
    invoke-virtual {p1}, Lco/uk/getmondo/d/aj;->z()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 94
    invoke-direct {p0}, Lco/uk/getmondo/transaction/a/a;->a()Lio/reactivex/v;

    move-result-object v0

    new-instance v1, Lco/uk/getmondo/transaction/a/a$d;

    invoke-direct {v1, v2}, Lco/uk/getmondo/transaction/a/a$d;-><init>(Lco/uk/getmondo/transaction/a/a$1;)V

    invoke-virtual {v0, v1}, Lio/reactivex/v;->d(Lio/reactivex/c/h;)Lio/reactivex/v;

    move-result-object v0

    .line 104
    :goto_0
    return-object v0

    .line 95
    :cond_0
    invoke-virtual {p1}, Lco/uk/getmondo/d/aj;->r()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 96
    invoke-virtual {p1}, Lco/uk/getmondo/d/aj;->C()Lco/uk/getmondo/payments/send/data/a/a;

    move-result-object v0

    invoke-direct {p0, v0}, Lco/uk/getmondo/transaction/a/a;->a(Lco/uk/getmondo/payments/send/data/a/a;)Lio/reactivex/v;

    move-result-object v0

    goto :goto_0

    .line 97
    :cond_1
    invoke-virtual {p1}, Lco/uk/getmondo/d/aj;->u()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 98
    invoke-virtual {p1}, Lco/uk/getmondo/d/aj;->G()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lco/uk/getmondo/transaction/a/a;->d(Ljava/lang/String;)Lio/reactivex/v;

    move-result-object v0

    goto :goto_0

    .line 99
    :cond_2
    invoke-virtual {p1}, Lco/uk/getmondo/d/aj;->q()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 100
    invoke-virtual {p1}, Lco/uk/getmondo/d/aj;->B()Lco/uk/getmondo/d/aa;

    move-result-object v0

    invoke-direct {p0, v0}, Lco/uk/getmondo/transaction/a/a;->a(Lco/uk/getmondo/d/aa;)Lio/reactivex/v;

    move-result-object v0

    goto :goto_0

    .line 101
    :cond_3
    invoke-virtual {p1}, Lco/uk/getmondo/d/aj;->f()Lco/uk/getmondo/d/u;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 102
    invoke-virtual {p1}, Lco/uk/getmondo/d/aj;->f()Lco/uk/getmondo/d/u;

    move-result-object v0

    invoke-virtual {v0}, Lco/uk/getmondo/d/u;->h()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lco/uk/getmondo/transaction/a/a;->e(Ljava/lang/String;)Lio/reactivex/v;

    move-result-object v0

    new-instance v1, Lco/uk/getmondo/transaction/a/a$d;

    invoke-direct {v1, v2}, Lco/uk/getmondo/transaction/a/a$d;-><init>(Lco/uk/getmondo/transaction/a/a$1;)V

    invoke-virtual {v0, v1}, Lio/reactivex/v;->d(Lio/reactivex/c/h;)Lio/reactivex/v;

    move-result-object v0

    goto :goto_0

    .line 104
    :cond_4
    sget-object v0, Lco/uk/getmondo/transaction/details/c/d;->a:Lco/uk/getmondo/transaction/details/c/d;

    invoke-static {v0}, Lio/reactivex/v;->a(Ljava/lang/Object;)Lio/reactivex/v;

    move-result-object v0

    goto :goto_0
.end method

.method public b(Ljava/lang/String;)Lio/reactivex/n;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/n",
            "<",
            "Lco/uk/getmondo/d/aj;",
            ">;"
        }
    .end annotation

    .prologue
    .line 56
    iget-object v0, p0, Lco/uk/getmondo/transaction/a/a;->b:Lco/uk/getmondo/transaction/a/j;

    invoke-virtual {v0, p1}, Lco/uk/getmondo/transaction/a/j;->b(Ljava/lang/String;)Lio/reactivex/n;

    move-result-object v0

    return-object v0
.end method

.method public c(Ljava/lang/String;)Lio/reactivex/b;
    .locals 2

    .prologue
    .line 83
    iget-object v0, p0, Lco/uk/getmondo/transaction/a/a;->a:Lco/uk/getmondo/api/MonzoApi;

    invoke-interface {v0, p1}, Lco/uk/getmondo/api/MonzoApi;->deregisterAttachment(Ljava/lang/String;)Lio/reactivex/b;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/transaction/a/a;->b:Lco/uk/getmondo/transaction/a/j;

    .line 84
    invoke-virtual {v1, p1}, Lco/uk/getmondo/transaction/a/j;->c(Ljava/lang/String;)Lio/reactivex/b;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/b;->b(Lio/reactivex/d;)Lio/reactivex/b;

    move-result-object v0

    .line 83
    return-object v0
.end method
