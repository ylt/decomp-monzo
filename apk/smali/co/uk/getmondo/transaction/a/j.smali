.class public Lco/uk/getmondo/transaction/a/j;
.super Ljava/lang/Object;
.source "TransactionStorage.java"


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    return-void
.end method

.method static synthetic a(Ljava/lang/String;Lio/realm/av;)Lio/realm/bg;
    .locals 2

    .prologue
    .line 93
    const-class v0, Lco/uk/getmondo/d/aj;

    invoke-virtual {p1, v0}, Lio/realm/av;->a(Ljava/lang/Class;)Lio/realm/bf;

    move-result-object v0

    const-string v1, "peer.userId"

    .line 94
    invoke-virtual {v0, v1, p0}, Lio/realm/bf;->a(Ljava/lang/String;Ljava/lang/String;)Lio/realm/bf;

    move-result-object v0

    .line 95
    invoke-virtual {v0}, Lio/realm/bf;->g()Lio/realm/bg;

    move-result-object v0

    .line 93
    return-object v0
.end method

.method static synthetic a(Ljava/lang/String;Ljava/lang/String;Lio/realm/av;)Lio/realm/bg;
    .locals 2

    .prologue
    .line 83
    const-class v0, Lco/uk/getmondo/d/aj;

    invoke-virtual {p2, v0}, Lio/realm/av;->a(Ljava/lang/Class;)Lio/realm/bf;

    move-result-object v0

    const-string v1, "bankDetails"

    .line 84
    invoke-virtual {v0, v1}, Lio/realm/bf;->b(Ljava/lang/String;)Lio/realm/bf;

    move-result-object v0

    const-string v1, "bankDetails.sortCode"

    .line 85
    invoke-virtual {v0, v1, p0}, Lio/realm/bf;->a(Ljava/lang/String;Ljava/lang/String;)Lio/realm/bf;

    move-result-object v0

    const-string v1, "bankDetails.accountNumber"

    .line 86
    invoke-virtual {v0, v1, p1}, Lio/realm/bf;->a(Ljava/lang/String;Ljava/lang/String;)Lio/realm/bf;

    move-result-object v0

    .line 87
    invoke-virtual {v0}, Lio/realm/bf;->g()Lio/realm/bg;

    move-result-object v0

    .line 83
    return-object v0
.end method

.method static synthetic a(Ljava/util/Date;Ljava/util/Date;)Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 183
    invoke-static {}, Lio/realm/av;->n()Lio/realm/av;

    move-result-object v2

    const/4 v1, 0x0

    .line 184
    :try_start_0
    const-class v0, Lco/uk/getmondo/d/aj;

    invoke-virtual {v2, v0}, Lio/realm/av;->a(Ljava/lang/Class;)Lio/realm/bf;

    move-result-object v0

    const-string v3, "created"

    .line 185
    invoke-virtual {v0, v3, p0, p1}, Lio/realm/bf;->a(Ljava/lang/String;Ljava/util/Date;Ljava/util/Date;)Lio/realm/bf;

    move-result-object v0

    .line 186
    invoke-virtual {v0}, Lio/realm/bf;->f()Lio/realm/bg;

    move-result-object v0

    .line 187
    invoke-virtual {v2, v0}, Lio/realm/av;->a(Ljava/lang/Iterable;)Ljava/util/List;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v0

    .line 188
    if-eqz v2, :cond_0

    if-eqz v1, :cond_1

    :try_start_1
    invoke-virtual {v2}, Lio/realm/av;->close()V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_1

    .line 187
    :cond_0
    :goto_0
    return-object v0

    .line 188
    :cond_1
    invoke-virtual {v2}, Lio/realm/av;->close()V

    goto :goto_0

    .line 183
    :catch_0
    move-exception v0

    :try_start_2
    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 188
    :catchall_0
    move-exception v1

    move-object v4, v1

    move-object v1, v0

    move-object v0, v4

    :goto_1
    if-eqz v2, :cond_2

    if-eqz v1, :cond_3

    :try_start_3
    invoke-virtual {v2}, Lio/realm/av;->close()V
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_2

    :cond_2
    :goto_2
    throw v0

    :cond_3
    invoke-virtual {v2}, Lio/realm/av;->close()V

    goto :goto_2

    :catch_1
    move-exception v1

    goto :goto_0

    :catch_2
    move-exception v1

    goto :goto_2

    :catchall_1
    move-exception v0

    goto :goto_1
.end method

.method static synthetic a(Lco/uk/getmondo/d/d;Ljava/lang/String;Lio/realm/av;)V
    .locals 3

    .prologue
    .line 54
    invoke-virtual {p2, p0}, Lio/realm/av;->b(Lio/realm/bb;)Lio/realm/bb;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/d/d;

    .line 55
    const-class v1, Lco/uk/getmondo/d/aj;

    invoke-virtual {p2, v1}, Lio/realm/av;->a(Ljava/lang/Class;)Lio/realm/bf;

    move-result-object v1

    const-string v2, "id"

    .line 56
    invoke-virtual {v1, v2, p1}, Lio/realm/bf;->a(Ljava/lang/String;Ljava/lang/String;)Lio/realm/bf;

    move-result-object v1

    .line 57
    invoke-virtual {v1}, Lio/realm/bf;->h()Lio/realm/bb;

    move-result-object v1

    check-cast v1, Lco/uk/getmondo/d/aj;

    .line 58
    invoke-virtual {v1}, Lco/uk/getmondo/d/aj;->A()Lio/realm/az;

    move-result-object v1

    invoke-virtual {v1, v0}, Lio/realm/az;->a(Lio/realm/bb;)Z

    .line 59
    return-void
.end method

.method static synthetic b(Ljava/lang/String;Lio/realm/av;)V
    .locals 2

    .prologue
    .line 64
    const-class v0, Lco/uk/getmondo/d/d;

    invoke-virtual {p1, v0}, Lio/realm/av;->a(Ljava/lang/Class;)Lio/realm/bf;

    move-result-object v0

    const-string v1, "id"

    .line 65
    invoke-virtual {v0, v1, p0}, Lio/realm/bf;->a(Ljava/lang/String;Ljava/lang/String;)Lio/realm/bf;

    move-result-object v0

    .line 66
    invoke-virtual {v0}, Lio/realm/bf;->h()Lio/realm/bb;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/d/d;

    .line 67
    invoke-static {v0}, Lio/realm/bc;->b(Lio/realm/bb;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 68
    invoke-static {v0}, Lio/realm/bc;->a(Lio/realm/bb;)V

    .line 70
    :cond_0
    return-void
.end method

.method static synthetic b(Ljava/lang/String;Ljava/lang/String;Lio/realm/av;)V
    .locals 2

    .prologue
    .line 75
    const-class v0, Lco/uk/getmondo/d/aj;

    invoke-virtual {p2, v0}, Lio/realm/av;->a(Ljava/lang/Class;)Lio/realm/bf;

    move-result-object v0

    const-string v1, "id"

    .line 76
    invoke-virtual {v0, v1, p0}, Lio/realm/bf;->a(Ljava/lang/String;Ljava/lang/String;)Lio/realm/bf;

    move-result-object v0

    .line 77
    invoke-virtual {v0}, Lio/realm/bf;->h()Lio/realm/bb;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/d/aj;

    .line 78
    invoke-virtual {v0, p1}, Lco/uk/getmondo/d/aj;->b(Ljava/lang/String;)V

    .line 79
    return-void
.end method

.method static synthetic c(Ljava/lang/String;Lio/realm/av;)Lio/realm/bg;
    .locals 2

    .prologue
    .line 45
    const-class v0, Lco/uk/getmondo/d/aj;

    invoke-virtual {p1, v0}, Lio/realm/av;->a(Ljava/lang/Class;)Lio/realm/bf;

    move-result-object v0

    const-string v1, "id"

    .line 46
    invoke-virtual {v0, v1, p0}, Lio/realm/bf;->a(Ljava/lang/String;Ljava/lang/String;)Lio/realm/bf;

    move-result-object v0

    .line 47
    invoke-virtual {v0}, Lio/realm/bf;->g()Lio/realm/bg;

    move-result-object v0

    .line 45
    return-object v0
.end method

.method static synthetic d()Lco/uk/getmondo/d/aj;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 173
    invoke-static {}, Lio/realm/av;->n()Lio/realm/av;

    move-result-object v2

    const/4 v1, 0x0

    .line 174
    :try_start_0
    const-class v0, Lco/uk/getmondo/d/aj;

    invoke-virtual {v2, v0}, Lio/realm/av;->a(Ljava/lang/Class;)Lio/realm/bf;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/bf;->h()Lio/realm/bb;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/d/aj;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 175
    if-eqz v2, :cond_0

    if-eqz v1, :cond_1

    :try_start_1
    invoke-virtual {v2}, Lio/realm/av;->close()V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_1

    .line 174
    :cond_0
    :goto_0
    return-object v0

    .line 175
    :cond_1
    invoke-virtual {v2}, Lio/realm/av;->close()V

    goto :goto_0

    .line 173
    :catch_0
    move-exception v0

    :try_start_2
    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 175
    :catchall_0
    move-exception v1

    move-object v3, v1

    move-object v1, v0

    move-object v0, v3

    :goto_1
    if-eqz v2, :cond_2

    if-eqz v1, :cond_3

    :try_start_3
    invoke-virtual {v2}, Lio/realm/av;->close()V
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_2

    :cond_2
    :goto_2
    throw v0

    :cond_3
    invoke-virtual {v2}, Lio/realm/av;->close()V

    goto :goto_2

    :catch_1
    move-exception v1

    goto :goto_0

    :catch_2
    move-exception v1

    goto :goto_2

    :catchall_1
    move-exception v0

    goto :goto_1
.end method

.method static synthetic e()Ljava/lang/Long;
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 136
    invoke-static {}, Lio/realm/av;->n()Lio/realm/av;

    move-result-object v2

    const/4 v1, 0x0

    .line 137
    :try_start_0
    const-class v0, Lco/uk/getmondo/d/aj;

    invoke-virtual {v2, v0}, Lio/realm/av;->a(Ljava/lang/Class;)Lio/realm/bf;

    move-result-object v0

    const-string v3, "topUp"

    const/4 v4, 0x1

    .line 138
    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Lio/realm/bf;->a(Ljava/lang/String;Ljava/lang/Boolean;)Lio/realm/bf;

    move-result-object v0

    const-string v3, "declineReason"

    .line 139
    invoke-virtual {v0, v3}, Lio/realm/bf;->a(Ljava/lang/String;)Lio/realm/bf;

    move-result-object v0

    const-string v3, "amountValue"

    .line 140
    invoke-virtual {v0, v3}, Lio/realm/bf;->c(Ljava/lang/String;)Ljava/lang/Number;

    move-result-object v0

    .line 141
    invoke-virtual {v0}, Ljava/lang/Number;->longValue()J

    move-result-wide v4

    .line 137
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v0

    .line 142
    if-eqz v2, :cond_0

    if-eqz v1, :cond_1

    :try_start_1
    invoke-virtual {v2}, Lio/realm/av;->close()V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_1

    .line 137
    :cond_0
    :goto_0
    return-object v0

    .line 142
    :cond_1
    invoke-virtual {v2}, Lio/realm/av;->close()V

    goto :goto_0

    .line 136
    :catch_0
    move-exception v0

    :try_start_2
    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 142
    :catchall_0
    move-exception v1

    move-object v6, v1

    move-object v1, v0

    move-object v0, v6

    :goto_1
    if-eqz v2, :cond_2

    if-eqz v1, :cond_3

    :try_start_3
    invoke-virtual {v2}, Lio/realm/av;->close()V
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_2

    :cond_2
    :goto_2
    throw v0

    :cond_3
    invoke-virtual {v2}, Lio/realm/av;->close()V

    goto :goto_2

    :catch_1
    move-exception v1

    goto :goto_0

    :catch_2
    move-exception v1

    goto :goto_2

    :catchall_1
    move-exception v0

    goto :goto_1
.end method

.method static synthetic f()Ljava/lang/Long;
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 125
    invoke-static {}, Lio/realm/av;->n()Lio/realm/av;

    move-result-object v2

    const/4 v1, 0x0

    .line 126
    :try_start_0
    const-class v0, Lco/uk/getmondo/d/aj;

    invoke-virtual {v2, v0}, Lio/realm/av;->a(Ljava/lang/Class;)Lio/realm/bf;

    move-result-object v0

    const-string v3, "topUp"

    const/4 v4, 0x1

    .line 127
    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Lio/realm/bf;->a(Ljava/lang/String;Ljava/lang/Boolean;)Lio/realm/bf;

    move-result-object v0

    const-string v3, "declineReason"

    .line 128
    invoke-virtual {v0, v3}, Lio/realm/bf;->a(Ljava/lang/String;)Lio/realm/bf;

    move-result-object v0

    .line 129
    invoke-virtual {v0}, Lio/realm/bf;->e()J

    move-result-wide v4

    .line 126
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v0

    .line 130
    if-eqz v2, :cond_0

    if-eqz v1, :cond_1

    :try_start_1
    invoke-virtual {v2}, Lio/realm/av;->close()V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_1

    .line 126
    :cond_0
    :goto_0
    return-object v0

    .line 130
    :cond_1
    invoke-virtual {v2}, Lio/realm/av;->close()V

    goto :goto_0

    .line 125
    :catch_0
    move-exception v0

    :try_start_2
    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 130
    :catchall_0
    move-exception v1

    move-object v6, v1

    move-object v1, v0

    move-object v0, v6

    :goto_1
    if-eqz v2, :cond_2

    if-eqz v1, :cond_3

    :try_start_3
    invoke-virtual {v2}, Lio/realm/av;->close()V
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_2

    :cond_2
    :goto_2
    throw v0

    :cond_3
    invoke-virtual {v2}, Lio/realm/av;->close()V

    goto :goto_2

    :catch_1
    move-exception v1

    goto :goto_0

    :catch_2
    move-exception v1

    goto :goto_2

    :catchall_1
    move-exception v0

    goto :goto_1
.end method

.method static synthetic i(Ljava/lang/String;)Ljava/lang/Long;
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 160
    invoke-static {}, Lio/realm/av;->n()Lio/realm/av;

    move-result-object v2

    const/4 v1, 0x0

    .line 161
    :try_start_0
    const-class v0, Lco/uk/getmondo/d/aj;

    invoke-virtual {v2, v0}, Lio/realm/av;->a(Ljava/lang/Class;)Lio/realm/bf;

    move-result-object v0

    const-string v3, "merchant.groupId"

    .line 162
    invoke-virtual {v0, v3, p0}, Lio/realm/bf;->a(Ljava/lang/String;Ljava/lang/String;)Lio/realm/bf;

    move-result-object v0

    const-string v3, "amountValue"

    const-wide/16 v4, 0x0

    .line 163
    invoke-virtual {v0, v3, v4, v5}, Lio/realm/bf;->a(Ljava/lang/String;J)Lio/realm/bf;

    move-result-object v0

    const-string v3, "declineReason"

    .line 164
    invoke-virtual {v0, v3}, Lio/realm/bf;->a(Ljava/lang/String;)Lio/realm/bf;

    move-result-object v0

    const-string v3, "amountValue"

    .line 165
    invoke-virtual {v0, v3}, Lio/realm/bf;->c(Ljava/lang/String;)Ljava/lang/Number;

    move-result-object v0

    .line 166
    invoke-virtual {v0}, Ljava/lang/Number;->longValue()J

    move-result-wide v4

    .line 161
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v0

    .line 167
    if-eqz v2, :cond_0

    if-eqz v1, :cond_1

    :try_start_1
    invoke-virtual {v2}, Lio/realm/av;->close()V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_1

    .line 161
    :cond_0
    :goto_0
    return-object v0

    .line 167
    :cond_1
    invoke-virtual {v2}, Lio/realm/av;->close()V

    goto :goto_0

    .line 160
    :catch_0
    move-exception v0

    :try_start_2
    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 167
    :catchall_0
    move-exception v1

    move-object v6, v1

    move-object v1, v0

    move-object v0, v6

    :goto_1
    if-eqz v2, :cond_2

    if-eqz v1, :cond_3

    :try_start_3
    invoke-virtual {v2}, Lio/realm/av;->close()V
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_2

    :cond_2
    :goto_2
    throw v0

    :cond_3
    invoke-virtual {v2}, Lio/realm/av;->close()V

    goto :goto_2

    :catch_1
    move-exception v1

    goto :goto_0

    :catch_2
    move-exception v1

    goto :goto_2

    :catchall_1
    move-exception v0

    goto :goto_1
.end method

.method static synthetic j(Ljava/lang/String;)Ljava/lang/Long;
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 148
    invoke-static {}, Lio/realm/av;->n()Lio/realm/av;

    move-result-object v2

    const/4 v1, 0x0

    .line 149
    :try_start_0
    const-class v0, Lco/uk/getmondo/d/aj;

    invoke-virtual {v2, v0}, Lio/realm/av;->a(Ljava/lang/Class;)Lio/realm/bf;

    move-result-object v0

    const-string v3, "merchant.groupId"

    .line 150
    invoke-virtual {v0, v3, p0}, Lio/realm/bf;->a(Ljava/lang/String;Ljava/lang/String;)Lio/realm/bf;

    move-result-object v0

    const-string v3, "amountValue"

    const-wide/16 v4, 0x0

    .line 151
    invoke-virtual {v0, v3, v4, v5}, Lio/realm/bf;->a(Ljava/lang/String;J)Lio/realm/bf;

    move-result-object v0

    const-string v3, "declineReason"

    .line 152
    invoke-virtual {v0, v3}, Lio/realm/bf;->a(Ljava/lang/String;)Lio/realm/bf;

    move-result-object v0

    .line 153
    invoke-virtual {v0}, Lio/realm/bf;->e()J

    move-result-wide v4

    .line 149
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v0

    .line 154
    if-eqz v2, :cond_0

    if-eqz v1, :cond_1

    :try_start_1
    invoke-virtual {v2}, Lio/realm/av;->close()V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_1

    .line 149
    :cond_0
    :goto_0
    return-object v0

    .line 154
    :cond_1
    invoke-virtual {v2}, Lio/realm/av;->close()V

    goto :goto_0

    .line 148
    :catch_0
    move-exception v0

    :try_start_2
    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 154
    :catchall_0
    move-exception v1

    move-object v6, v1

    move-object v1, v0

    move-object v0, v6

    :goto_1
    if-eqz v2, :cond_2

    if-eqz v1, :cond_3

    :try_start_3
    invoke-virtual {v2}, Lio/realm/av;->close()V
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_2

    :cond_2
    :goto_2
    throw v0

    :cond_3
    invoke-virtual {v2}, Lio/realm/av;->close()V

    goto :goto_2

    :catch_1
    move-exception v1

    goto :goto_0

    :catch_2
    move-exception v1

    goto :goto_2

    :catchall_1
    move-exception v0

    goto :goto_1
.end method

.method static synthetic k(Ljava/lang/String;)Ljava/lang/Long;
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 113
    invoke-static {}, Lio/realm/av;->n()Lio/realm/av;

    move-result-object v2

    const/4 v1, 0x0

    .line 114
    :try_start_0
    const-class v0, Lco/uk/getmondo/d/aj;

    invoke-virtual {v2, v0}, Lio/realm/av;->a(Ljava/lang/Class;)Lio/realm/bf;

    move-result-object v0

    const-string v3, "declineReason"

    .line 115
    invoke-virtual {v0, v3}, Lio/realm/bf;->a(Ljava/lang/String;)Lio/realm/bf;

    move-result-object v0

    const-string v3, "bacsDirectDebitInstructionId"

    .line 116
    invoke-virtual {v0, v3, p0}, Lio/realm/bf;->a(Ljava/lang/String;Ljava/lang/String;)Lio/realm/bf;

    move-result-object v0

    const-string v3, "amountValue"

    .line 117
    invoke-virtual {v0, v3}, Lio/realm/bf;->c(Ljava/lang/String;)Ljava/lang/Number;

    move-result-object v0

    .line 118
    invoke-virtual {v0}, Ljava/lang/Number;->longValue()J

    move-result-wide v4

    .line 114
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v0

    .line 119
    if-eqz v2, :cond_0

    if-eqz v1, :cond_1

    :try_start_1
    invoke-virtual {v2}, Lio/realm/av;->close()V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_1

    .line 114
    :cond_0
    :goto_0
    return-object v0

    .line 119
    :cond_1
    invoke-virtual {v2}, Lio/realm/av;->close()V

    goto :goto_0

    .line 113
    :catch_0
    move-exception v0

    :try_start_2
    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 119
    :catchall_0
    move-exception v1

    move-object v6, v1

    move-object v1, v0

    move-object v0, v6

    :goto_1
    if-eqz v2, :cond_2

    if-eqz v1, :cond_3

    :try_start_3
    invoke-virtual {v2}, Lio/realm/av;->close()V
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_2

    :cond_2
    :goto_2
    throw v0

    :cond_3
    invoke-virtual {v2}, Lio/realm/av;->close()V

    goto :goto_2

    :catch_1
    move-exception v1

    goto :goto_0

    :catch_2
    move-exception v1

    goto :goto_2

    :catchall_1
    move-exception v0

    goto :goto_1
.end method

.method static synthetic l(Ljava/lang/String;)Ljava/lang/Long;
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 102
    invoke-static {}, Lio/realm/av;->n()Lio/realm/av;

    move-result-object v2

    const/4 v1, 0x0

    .line 103
    :try_start_0
    const-class v0, Lco/uk/getmondo/d/aj;

    invoke-virtual {v2, v0}, Lio/realm/av;->a(Ljava/lang/Class;)Lio/realm/bf;

    move-result-object v0

    const-string v3, "declineReason"

    .line 104
    invoke-virtual {v0, v3}, Lio/realm/bf;->a(Ljava/lang/String;)Lio/realm/bf;

    move-result-object v0

    const-string v3, "bacsDirectDebitInstructionId"

    .line 105
    invoke-virtual {v0, v3, p0}, Lio/realm/bf;->a(Ljava/lang/String;Ljava/lang/String;)Lio/realm/bf;

    move-result-object v0

    .line 106
    invoke-virtual {v0}, Lio/realm/bf;->e()J

    move-result-wide v4

    .line 103
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v0

    .line 107
    if-eqz v2, :cond_0

    if-eqz v1, :cond_1

    :try_start_1
    invoke-virtual {v2}, Lio/realm/av;->close()V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_1

    .line 103
    :cond_0
    :goto_0
    return-object v0

    .line 107
    :cond_1
    invoke-virtual {v2}, Lio/realm/av;->close()V

    goto :goto_0

    .line 102
    :catch_0
    move-exception v0

    :try_start_2
    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 107
    :catchall_0
    move-exception v1

    move-object v6, v1

    move-object v1, v0

    move-object v0, v6

    :goto_1
    if-eqz v2, :cond_2

    if-eqz v1, :cond_3

    :try_start_3
    invoke-virtual {v2}, Lio/realm/av;->close()V
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_2

    :cond_2
    :goto_2
    throw v0

    :cond_3
    invoke-virtual {v2}, Lio/realm/av;->close()V

    goto :goto_2

    :catch_1
    move-exception v1

    goto :goto_0

    :catch_2
    move-exception v1

    goto :goto_2

    :catchall_1
    move-exception v0

    goto :goto_1
.end method


# virtual methods
.method a(Ljava/lang/String;)Lco/uk/getmondo/d/aj;
    .locals 5

    .prologue
    .line 37
    invoke-static {}, Lio/realm/av;->n()Lio/realm/av;

    move-result-object v2

    const/4 v1, 0x0

    .line 38
    :try_start_0
    const-class v0, Lco/uk/getmondo/d/aj;

    invoke-virtual {v2, v0}, Lio/realm/av;->a(Ljava/lang/Class;)Lio/realm/bf;

    move-result-object v0

    const-string v3, "id"

    .line 39
    invoke-virtual {v0, v3, p1}, Lio/realm/bf;->a(Ljava/lang/String;Ljava/lang/String;)Lio/realm/bf;

    move-result-object v0

    .line 40
    invoke-virtual {v0}, Lio/realm/bf;->h()Lio/realm/bb;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/d/aj;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 41
    if-eqz v2, :cond_0

    if-eqz v1, :cond_1

    :try_start_1
    invoke-virtual {v2}, Lio/realm/av;->close()V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_1

    .line 38
    :cond_0
    :goto_0
    return-object v0

    .line 41
    :cond_1
    invoke-virtual {v2}, Lio/realm/av;->close()V

    goto :goto_0

    .line 37
    :catch_0
    move-exception v0

    :try_start_2
    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 41
    :catchall_0
    move-exception v1

    move-object v4, v1

    move-object v1, v0

    move-object v0, v4

    :goto_1
    if-eqz v2, :cond_2

    if-eqz v1, :cond_3

    :try_start_3
    invoke-virtual {v2}, Lio/realm/av;->close()V
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_2

    :cond_2
    :goto_2
    throw v0

    :cond_3
    invoke-virtual {v2}, Lio/realm/av;->close()V

    goto :goto_2

    :catch_1
    move-exception v1

    goto :goto_0

    :catch_2
    move-exception v1

    goto :goto_2

    :catchall_1
    move-exception v0

    goto :goto_1
.end method

.method a(Lco/uk/getmondo/d/d;Ljava/lang/String;)Lio/reactivex/b;
    .locals 1

    .prologue
    .line 53
    invoke-static {p1, p2}, Lco/uk/getmondo/transaction/a/w;->a(Lco/uk/getmondo/d/d;Ljava/lang/String;)Lio/realm/av$a;

    move-result-object v0

    invoke-static {v0}, Lco/uk/getmondo/common/j/g;->a(Lio/realm/av$a;)Lio/reactivex/b;

    move-result-object v0

    return-object v0
.end method

.method a(Ljava/lang/String;Ljava/lang/String;)Lio/reactivex/b;
    .locals 1

    .prologue
    .line 74
    invoke-static {p1, p2}, Lco/uk/getmondo/transaction/a/y;->a(Ljava/lang/String;Ljava/lang/String;)Lio/realm/av$a;

    move-result-object v0

    invoke-static {v0}, Lco/uk/getmondo/common/j/g;->a(Lio/realm/av$a;)Lio/reactivex/b;

    move-result-object v0

    return-object v0
.end method

.method a()Lio/reactivex/v;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/v",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .prologue
    .line 124
    invoke-static {}, Lco/uk/getmondo/transaction/a/o;->a()Ljava/util/concurrent/Callable;

    move-result-object v0

    invoke-static {v0}, Lio/reactivex/v;->c(Ljava/util/concurrent/Callable;)Lio/reactivex/v;

    move-result-object v0

    return-object v0
.end method

.method public a(Lorg/threeten/bp/LocalDateTime;Lorg/threeten/bp/LocalDateTime;)Lio/reactivex/v;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/threeten/bp/LocalDateTime;",
            "Lorg/threeten/bp/LocalDateTime;",
            ")",
            "Lio/reactivex/v",
            "<",
            "Ljava/util/List",
            "<",
            "Lco/uk/getmondo/d/aj;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 180
    invoke-static {}, Lorg/threeten/bp/ZoneId;->a()Lorg/threeten/bp/ZoneId;

    move-result-object v0

    invoke-virtual {p1, v0}, Lorg/threeten/bp/LocalDateTime;->a(Lorg/threeten/bp/ZoneId;)Lorg/threeten/bp/ZonedDateTime;

    move-result-object v0

    invoke-virtual {v0}, Lorg/threeten/bp/ZonedDateTime;->j()Lorg/threeten/bp/Instant;

    move-result-object v0

    invoke-static {v0}, Lorg/threeten/bp/DateTimeUtils;->a(Lorg/threeten/bp/Instant;)Ljava/util/Date;

    move-result-object v0

    .line 181
    invoke-static {}, Lorg/threeten/bp/ZoneId;->a()Lorg/threeten/bp/ZoneId;

    move-result-object v1

    invoke-virtual {p2, v1}, Lorg/threeten/bp/LocalDateTime;->a(Lorg/threeten/bp/ZoneId;)Lorg/threeten/bp/ZonedDateTime;

    move-result-object v1

    invoke-virtual {v1}, Lorg/threeten/bp/ZonedDateTime;->j()Lorg/threeten/bp/Instant;

    move-result-object v1

    invoke-static {v1}, Lorg/threeten/bp/DateTimeUtils;->a(Lorg/threeten/bp/Instant;)Ljava/util/Date;

    move-result-object v1

    .line 182
    invoke-static {v0, v1}, Lco/uk/getmondo/transaction/a/t;->a(Ljava/util/Date;Ljava/util/Date;)Ljava/util/concurrent/Callable;

    move-result-object v0

    invoke-static {v0}, Lio/reactivex/v;->c(Ljava/util/concurrent/Callable;)Lio/reactivex/v;

    move-result-object v0

    return-object v0
.end method

.method public b(Ljava/lang/String;)Lio/reactivex/n;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/n",
            "<",
            "Lco/uk/getmondo/d/aj;",
            ">;"
        }
    .end annotation

    .prologue
    .line 45
    invoke-static {p1}, Lco/uk/getmondo/transaction/a/k;->a(Ljava/lang/String;)Lkotlin/d/a/b;

    move-result-object v0

    invoke-static {v0}, Lco/uk/getmondo/common/j/g;->a(Lkotlin/d/a/b;)Lio/reactivex/n;

    move-result-object v0

    invoke-static {}, Lco/uk/getmondo/transaction/a/u;->a()Lio/reactivex/c/q;

    move-result-object v1

    .line 48
    invoke-virtual {v0, v1}, Lio/reactivex/n;->filter(Lio/reactivex/c/q;)Lio/reactivex/n;

    move-result-object v0

    invoke-static {}, Lco/uk/getmondo/transaction/a/v;->a()Lio/reactivex/c/h;

    move-result-object v1

    .line 49
    invoke-virtual {v0, v1}, Lio/reactivex/n;->map(Lio/reactivex/c/h;)Lio/reactivex/n;

    move-result-object v0

    .line 45
    return-object v0
.end method

.method b()Lio/reactivex/v;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/v",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .prologue
    .line 135
    invoke-static {}, Lco/uk/getmondo/transaction/a/p;->a()Ljava/util/concurrent/Callable;

    move-result-object v0

    invoke-static {v0}, Lio/reactivex/v;->c(Ljava/util/concurrent/Callable;)Lio/reactivex/v;

    move-result-object v0

    return-object v0
.end method

.method b(Ljava/lang/String;Ljava/lang/String;)Lio/reactivex/v;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/v",
            "<",
            "Ljava/util/List",
            "<",
            "Lco/uk/getmondo/d/aj;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 83
    invoke-static {p1, p2}, Lco/uk/getmondo/transaction/a/z;->a(Ljava/lang/String;Ljava/lang/String;)Lkotlin/d/a/b;

    move-result-object v0

    invoke-static {v0}, Lco/uk/getmondo/common/j/g;->a(Lkotlin/d/a/b;)Lio/reactivex/n;

    move-result-object v0

    invoke-static {}, Lco/uk/getmondo/transaction/a/aa;->a()Lio/reactivex/c/h;

    move-result-object v1

    .line 88
    invoke-virtual {v0, v1}, Lio/reactivex/n;->map(Lio/reactivex/c/h;)Lio/reactivex/n;

    move-result-object v0

    .line 89
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/n;->first(Ljava/lang/Object;)Lio/reactivex/v;

    move-result-object v0

    .line 83
    return-object v0
.end method

.method c(Ljava/lang/String;)Lio/reactivex/b;
    .locals 1

    .prologue
    .line 63
    invoke-static {p1}, Lco/uk/getmondo/transaction/a/x;->a(Ljava/lang/String;)Lio/realm/av$a;

    move-result-object v0

    invoke-static {v0}, Lco/uk/getmondo/common/j/g;->a(Lio/realm/av$a;)Lio/reactivex/b;

    move-result-object v0

    return-object v0
.end method

.method public c()Lio/reactivex/v;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/v",
            "<",
            "Lco/uk/getmondo/d/aj;",
            ">;"
        }
    .end annotation

    .prologue
    .line 172
    invoke-static {}, Lco/uk/getmondo/transaction/a/s;->a()Ljava/util/concurrent/Callable;

    move-result-object v0

    invoke-static {v0}, Lio/reactivex/v;->c(Ljava/util/concurrent/Callable;)Lio/reactivex/v;

    move-result-object v0

    return-object v0
.end method

.method d(Ljava/lang/String;)Lio/reactivex/v;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/v",
            "<",
            "Ljava/util/List",
            "<",
            "Lco/uk/getmondo/d/aj;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 93
    invoke-static {p1}, Lco/uk/getmondo/transaction/a/ab;->a(Ljava/lang/String;)Lkotlin/d/a/b;

    move-result-object v0

    invoke-static {v0}, Lco/uk/getmondo/common/j/g;->a(Lkotlin/d/a/b;)Lio/reactivex/n;

    move-result-object v0

    invoke-static {}, Lco/uk/getmondo/transaction/a/l;->a()Lio/reactivex/c/h;

    move-result-object v1

    .line 96
    invoke-virtual {v0, v1}, Lio/reactivex/n;->map(Lio/reactivex/c/h;)Lio/reactivex/n;

    move-result-object v0

    .line 97
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/n;->first(Ljava/lang/Object;)Lio/reactivex/v;

    move-result-object v0

    .line 93
    return-object v0
.end method

.method e(Ljava/lang/String;)Lio/reactivex/v;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/v",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .prologue
    .line 101
    invoke-static {p1}, Lco/uk/getmondo/transaction/a/m;->a(Ljava/lang/String;)Ljava/util/concurrent/Callable;

    move-result-object v0

    invoke-static {v0}, Lio/reactivex/v;->c(Ljava/util/concurrent/Callable;)Lio/reactivex/v;

    move-result-object v0

    return-object v0
.end method

.method f(Ljava/lang/String;)Lio/reactivex/v;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/v",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .prologue
    .line 112
    invoke-static {p1}, Lco/uk/getmondo/transaction/a/n;->a(Ljava/lang/String;)Ljava/util/concurrent/Callable;

    move-result-object v0

    invoke-static {v0}, Lio/reactivex/v;->c(Ljava/util/concurrent/Callable;)Lio/reactivex/v;

    move-result-object v0

    return-object v0
.end method

.method g(Ljava/lang/String;)Lio/reactivex/v;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/v",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .prologue
    .line 147
    invoke-static {p1}, Lco/uk/getmondo/transaction/a/q;->a(Ljava/lang/String;)Ljava/util/concurrent/Callable;

    move-result-object v0

    invoke-static {v0}, Lio/reactivex/v;->c(Ljava/util/concurrent/Callable;)Lio/reactivex/v;

    move-result-object v0

    return-object v0
.end method

.method h(Ljava/lang/String;)Lio/reactivex/v;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/v",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .prologue
    .line 159
    invoke-static {p1}, Lco/uk/getmondo/transaction/a/r;->a(Ljava/lang/String;)Ljava/util/concurrent/Callable;

    move-result-object v0

    invoke-static {v0}, Lio/reactivex/v;->c(Ljava/util/concurrent/Callable;)Lio/reactivex/v;

    move-result-object v0

    return-object v0
.end method
