.class Lco/uk/getmondo/transaction/a/a$b;
.super Ljava/lang/Object;
.source "TransactionManager.java"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lco/uk/getmondo/transaction/a/a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h",
        "<",
        "Ljava/util/List",
        "<",
        "Lco/uk/getmondo/d/aj;",
        ">;",
        "Lco/uk/getmondo/transaction/details/c/b;",
        ">;"
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 167
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lco/uk/getmondo/transaction/a/a$1;)V
    .locals 0

    .prologue
    .line 167
    invoke-direct {p0}, Lco/uk/getmondo/transaction/a/a$b;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Ljava/util/List;)Lco/uk/getmondo/transaction/details/c/b;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lco/uk/getmondo/d/aj;",
            ">;)",
            "Lco/uk/getmondo/transaction/details/c/b;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 170
    new-instance v3, Lco/uk/getmondo/common/i/a;

    sget-object v0, Lco/uk/getmondo/common/i/c;->a:Lco/uk/getmondo/common/i/c;

    invoke-direct {v3, v0}, Lco/uk/getmondo/common/i/a;-><init>(Lco/uk/getmondo/common/i/c;)V

    .line 171
    new-instance v6, Lco/uk/getmondo/common/i/a;

    sget-object v0, Lco/uk/getmondo/common/i/c;->a:Lco/uk/getmondo/common/i/c;

    invoke-direct {v6, v0}, Lco/uk/getmondo/common/i/a;-><init>(Lco/uk/getmondo/common/i/c;)V

    .line 172
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/d/aj;

    .line 173
    invoke-virtual {v0}, Lco/uk/getmondo/d/aj;->g()Lco/uk/getmondo/d/c;

    move-result-object v2

    invoke-virtual {v2}, Lco/uk/getmondo/d/c;->a()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 174
    invoke-virtual {v0}, Lco/uk/getmondo/d/aj;->g()Lco/uk/getmondo/d/c;

    move-result-object v0

    invoke-virtual {v3, v0}, Lco/uk/getmondo/common/i/a;->a(Lco/uk/getmondo/d/c;)V

    goto :goto_0

    .line 176
    :cond_0
    invoke-virtual {v0}, Lco/uk/getmondo/d/aj;->g()Lco/uk/getmondo/d/c;

    move-result-object v0

    invoke-virtual {v6, v0}, Lco/uk/getmondo/common/i/a;->a(Lco/uk/getmondo/d/c;)V

    goto :goto_0

    .line 179
    :cond_1
    new-instance v0, Lco/uk/getmondo/transaction/details/c/b;

    invoke-virtual {v3}, Lco/uk/getmondo/common/i/a;->b()I

    move-result v1

    int-to-long v1, v1

    invoke-virtual {v3}, Lco/uk/getmondo/common/i/a;->a()Lco/uk/getmondo/d/c;

    move-result-object v3

    .line 180
    invoke-virtual {v6}, Lco/uk/getmondo/common/i/a;->b()I

    move-result v4

    int-to-long v4, v4

    invoke-virtual {v6}, Lco/uk/getmondo/common/i/a;->a()Lco/uk/getmondo/d/c;

    move-result-object v6

    invoke-direct/range {v0 .. v6}, Lco/uk/getmondo/transaction/details/c/b;-><init>(JLco/uk/getmondo/d/c;JLco/uk/getmondo/d/c;)V

    .line 179
    return-object v0
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 167
    check-cast p1, Ljava/util/List;

    invoke-virtual {p0, p1}, Lco/uk/getmondo/transaction/a/a$b;->a(Ljava/util/List;)Lco/uk/getmondo/transaction/details/c/b;

    move-result-object v0

    return-object v0
.end method
