.class Lco/uk/getmondo/transaction/a/a$c;
.super Ljava/lang/Object;
.source "TransactionManager.java"

# interfaces
.implements Lio/reactivex/c/c;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lco/uk/getmondo/transaction/a/a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "c"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lio/reactivex/c/c",
        "<",
        "Ljava/lang/Long;",
        "Ljava/lang/Long;",
        "Lco/uk/getmondo/transaction/details/c/e;",
        ">;"
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 143
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lco/uk/getmondo/transaction/a/a$1;)V
    .locals 0

    .prologue
    .line 143
    invoke-direct {p0}, Lco/uk/getmondo/transaction/a/a$c;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Ljava/lang/Long;Ljava/lang/Long;)Lco/uk/getmondo/transaction/details/c/e;
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    const-wide/16 v4, 0x0

    .line 146
    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    cmp-long v0, v0, v4

    if-nez v0, :cond_0

    .line 147
    new-instance v0, Lco/uk/getmondo/transaction/details/c/e;

    new-instance v1, Lco/uk/getmondo/d/c;

    sget-object v2, Lco/uk/getmondo/common/i/c;->a:Lco/uk/getmondo/common/i/c;

    invoke-direct {v1, v4, v5, v2}, Lco/uk/getmondo/d/c;-><init>(JLco/uk/getmondo/common/i/c;)V

    new-instance v2, Lco/uk/getmondo/d/c;

    sget-object v3, Lco/uk/getmondo/common/i/c;->a:Lco/uk/getmondo/common/i/c;

    invoke-direct {v2, v4, v5, v3}, Lco/uk/getmondo/d/c;-><init>(JLco/uk/getmondo/common/i/c;)V

    invoke-direct {v0, v1, v2, v4, v5}, Lco/uk/getmondo/transaction/details/c/e;-><init>(Lco/uk/getmondo/d/c;Lco/uk/getmondo/d/c;J)V

    .line 150
    :goto_0
    return-object v0

    .line 149
    :cond_0
    invoke-virtual {p2}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    div-long v2, v0, v2

    .line 150
    new-instance v0, Lco/uk/getmondo/transaction/details/c/e;

    new-instance v1, Lco/uk/getmondo/d/c;

    invoke-virtual {p2}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    sget-object v6, Lco/uk/getmondo/common/i/c;->a:Lco/uk/getmondo/common/i/c;

    invoke-direct {v1, v4, v5, v6}, Lco/uk/getmondo/d/c;-><init>(JLco/uk/getmondo/common/i/c;)V

    new-instance v4, Lco/uk/getmondo/d/c;

    sget-object v5, Lco/uk/getmondo/common/i/c;->a:Lco/uk/getmondo/common/i/c;

    invoke-direct {v4, v2, v3, v5}, Lco/uk/getmondo/d/c;-><init>(JLco/uk/getmondo/common/i/c;)V

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-direct {v0, v1, v4, v2, v3}, Lco/uk/getmondo/transaction/details/c/e;-><init>(Lco/uk/getmondo/d/c;Lco/uk/getmondo/d/c;J)V

    goto :goto_0
.end method

.method public bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 143
    check-cast p1, Ljava/lang/Long;

    check-cast p2, Ljava/lang/Long;

    invoke-virtual {p0, p1, p2}, Lco/uk/getmondo/transaction/a/a$c;->a(Ljava/lang/Long;Ljava/lang/Long;)Lco/uk/getmondo/transaction/details/c/e;

    move-result-object v0

    return-object v0
.end method
