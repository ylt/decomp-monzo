.class public Lco/uk/getmondo/transaction/splitting/SplitBottomSheetFragment;
.super Landroid/support/design/widget/d;
.source "SplitBottomSheetFragment.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lco/uk/getmondo/transaction/splitting/SplitBottomSheetFragment$a;
    }
.end annotation


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private b:Lbutterknife/Unbinder;

.field private c:Lco/uk/getmondo/transaction/splitting/a/a;

.field private d:Lco/uk/getmondo/transaction/splitting/SplitBottomSheetFragment$a;

.field shareTextViews:Landroid/view/ViewGroup;
    .annotation build Lbutterknife/BindView;
        value = 0x7f110292
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 25
    const-class v0, Lco/uk/getmondo/transaction/splitting/SplitBottomSheetFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lco/uk/getmondo/transaction/splitting/SplitBottomSheetFragment;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Landroid/support/design/widget/d;-><init>()V

    return-void
.end method

.method public static a(Lco/uk/getmondo/transaction/splitting/a/a;)Lco/uk/getmondo/transaction/splitting/SplitBottomSheetFragment;
    .locals 2

    .prologue
    .line 35
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 36
    const-string v1, "view_model"

    invoke-virtual {v0, v1, p0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 37
    new-instance v1, Lco/uk/getmondo/transaction/splitting/SplitBottomSheetFragment;

    invoke-direct {v1}, Lco/uk/getmondo/transaction/splitting/SplitBottomSheetFragment;-><init>()V

    .line 38
    invoke-virtual {v1, v0}, Lco/uk/getmondo/transaction/splitting/SplitBottomSheetFragment;->setArguments(Landroid/os/Bundle;)V

    .line 39
    return-object v1
.end method

.method public static a(Landroid/support/v7/app/e;Lco/uk/getmondo/transaction/splitting/a/a;Lco/uk/getmondo/transaction/splitting/SplitBottomSheetFragment$a;)V
    .locals 3

    .prologue
    .line 43
    invoke-static {p1}, Lco/uk/getmondo/transaction/splitting/SplitBottomSheetFragment;->a(Lco/uk/getmondo/transaction/splitting/a/a;)Lco/uk/getmondo/transaction/splitting/SplitBottomSheetFragment;

    move-result-object v0

    .line 44
    invoke-virtual {v0, p2}, Lco/uk/getmondo/transaction/splitting/SplitBottomSheetFragment;->a(Lco/uk/getmondo/transaction/splitting/SplitBottomSheetFragment$a;)V

    .line 45
    invoke-virtual {p0}, Landroid/support/v7/app/e;->getSupportFragmentManager()Landroid/support/v4/app/n;

    move-result-object v1

    sget-object v2, Lco/uk/getmondo/transaction/splitting/SplitBottomSheetFragment;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lco/uk/getmondo/transaction/splitting/SplitBottomSheetFragment;->show(Landroid/support/v4/app/n;Ljava/lang/String;)V

    .line 46
    return-void
.end method

.method static synthetic a(Lco/uk/getmondo/transaction/splitting/SplitBottomSheetFragment;Landroid/app/Dialog;ILandroid/view/View;)V
    .locals 1

    .prologue
    .line 91
    iget-object v0, p0, Lco/uk/getmondo/transaction/splitting/SplitBottomSheetFragment;->d:Lco/uk/getmondo/transaction/splitting/SplitBottomSheetFragment$a;

    if-eqz v0, :cond_0

    .line 92
    iget-object v0, p0, Lco/uk/getmondo/transaction/splitting/SplitBottomSheetFragment;->d:Lco/uk/getmondo/transaction/splitting/SplitBottomSheetFragment$a;

    invoke-interface {v0, p1, p2}, Lco/uk/getmondo/transaction/splitting/SplitBottomSheetFragment$a;->a(Landroid/app/Dialog;I)V

    .line 94
    :cond_0
    return-void
.end method


# virtual methods
.method public a(Lco/uk/getmondo/transaction/splitting/SplitBottomSheetFragment$a;)V
    .locals 0

    .prologue
    .line 111
    iput-object p1, p0, Lco/uk/getmondo/transaction/splitting/SplitBottomSheetFragment;->d:Lco/uk/getmondo/transaction/splitting/SplitBottomSheetFragment$a;

    .line 112
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 57
    invoke-super {p0, p1}, Landroid/support/design/widget/d;->onCreate(Landroid/os/Bundle;)V

    .line 58
    invoke-virtual {p0}, Lco/uk/getmondo/transaction/splitting/SplitBottomSheetFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 59
    if-eqz v0, :cond_0

    const-string v1, "view_model"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 60
    const-string v1, "view_model"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/transaction/splitting/a/a;

    iput-object v0, p0, Lco/uk/getmondo/transaction/splitting/SplitBottomSheetFragment;->c:Lco/uk/getmondo/transaction/splitting/a/a;

    .line 62
    :cond_0
    return-void
.end method

.method public onCustomiseAmount()V
    .locals 2
    .annotation build Lbutterknife/OnClick;
        value = {
            0x7f110297
        }
    .end annotation

    .prologue
    .line 106
    iget-object v0, p0, Lco/uk/getmondo/transaction/splitting/SplitBottomSheetFragment;->d:Lco/uk/getmondo/transaction/splitting/SplitBottomSheetFragment$a;

    if-nez v0, :cond_0

    .line 108
    :goto_0
    return-void

    .line 107
    :cond_0
    iget-object v0, p0, Lco/uk/getmondo/transaction/splitting/SplitBottomSheetFragment;->d:Lco/uk/getmondo/transaction/splitting/SplitBottomSheetFragment$a;

    invoke-virtual {p0}, Lco/uk/getmondo/transaction/splitting/SplitBottomSheetFragment;->getDialog()Landroid/app/Dialog;

    move-result-object v1

    invoke-interface {v0, v1}, Lco/uk/getmondo/transaction/splitting/SplitBottomSheetFragment$a;->a(Landroid/app/Dialog;)V

    goto :goto_0
.end method

.method public onDestroyView()V
    .locals 1

    .prologue
    .line 100
    invoke-super {p0}, Landroid/support/design/widget/d;->onDestroyView()V

    .line 101
    iget-object v0, p0, Lco/uk/getmondo/transaction/splitting/SplitBottomSheetFragment;->b:Lbutterknife/Unbinder;

    invoke-interface {v0}, Lbutterknife/Unbinder;->unbind()V

    .line 102
    return-void
.end method

.method public setupDialog(Landroid/app/Dialog;I)V
    .locals 13

    .prologue
    const/4 v4, 0x1

    const/4 v2, 0x0

    .line 66
    invoke-super {p0, p1, p2}, Landroid/support/design/widget/d;->setupDialog(Landroid/app/Dialog;I)V

    .line 67
    invoke-virtual {p0}, Lco/uk/getmondo/transaction/splitting/SplitBottomSheetFragment;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f05007d

    const/4 v3, 0x0

    invoke-static {v0, v1, v3}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 68
    invoke-virtual {p1, v0}, Landroid/app/Dialog;->setContentView(Landroid/view/View;)V

    .line 69
    invoke-static {p0, v0}, Lbutterknife/ButterKnife;->bind(Ljava/lang/Object;Landroid/view/View;)Lbutterknife/Unbinder;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/transaction/splitting/SplitBottomSheetFragment;->b:Lbutterknife/Unbinder;

    .line 71
    iget-object v0, p0, Lco/uk/getmondo/transaction/splitting/SplitBottomSheetFragment;->c:Lco/uk/getmondo/transaction/splitting/a/a;

    if-nez v0, :cond_1

    .line 96
    :cond_0
    return-void

    .line 72
    :cond_1
    invoke-virtual {p0}, Lco/uk/getmondo/transaction/splitting/SplitBottomSheetFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    .line 73
    iget-object v0, p0, Lco/uk/getmondo/transaction/splitting/SplitBottomSheetFragment;->c:Lco/uk/getmondo/transaction/splitting/a/a;

    invoke-virtual {v0}, Lco/uk/getmondo/transaction/splitting/a/a;->a()[Lco/uk/getmondo/transaction/splitting/a/a$c;

    move-result-object v6

    move v1, v2

    .line 74
    :goto_0
    iget-object v0, p0, Lco/uk/getmondo/transaction/splitting/SplitBottomSheetFragment;->shareTextViews:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 75
    array-length v0, v6

    if-ge v1, v0, :cond_0

    .line 76
    iget-object v0, p0, Lco/uk/getmondo/transaction/splitting/SplitBottomSheetFragment;->shareTextViews:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 77
    aget-object v7, v6, v1

    .line 79
    invoke-virtual {v7}, Lco/uk/getmondo/transaction/splitting/a/a$c;->c()I

    move-result v3

    const/4 v8, -0x1

    if-ne v3, v8, :cond_2

    .line 80
    const v3, 0x7f0a0393

    new-array v8, v4, [Ljava/lang/Object;

    invoke-virtual {v7}, Lco/uk/getmondo/transaction/splitting/a/a$c;->d()Ljava/lang/String;

    move-result-object v9

    aput-object v9, v8, v2

    invoke-virtual {p0, v3, v8}, Lco/uk/getmondo/transaction/splitting/SplitBottomSheetFragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 86
    :goto_1
    invoke-virtual {v7}, Lco/uk/getmondo/transaction/splitting/a/a$c;->a()I

    move-result v8

    .line 87
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const v10, 0x7f120008

    new-array v11, v4, [Ljava/lang/Object;

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    aput-object v12, v11, v2

    invoke-virtual {v5, v10, v8, v11}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 88
    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 89
    invoke-virtual {v7}, Lco/uk/getmondo/transaction/splitting/a/a$c;->c()I

    move-result v3

    if-nez v3, :cond_4

    move v3, v4

    :goto_2
    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 90
    invoke-static {p0, p1, v8}, Lco/uk/getmondo/transaction/splitting/a;->a(Lco/uk/getmondo/transaction/splitting/SplitBottomSheetFragment;Landroid/app/Dialog;I)Landroid/view/View$OnClickListener;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 74
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 81
    :cond_2
    invoke-virtual {v7}, Lco/uk/getmondo/transaction/splitting/a/a$c;->c()I

    move-result v3

    if-ne v3, v4, :cond_3

    .line 82
    const v3, 0x7f0a038f

    new-array v8, v4, [Ljava/lang/Object;

    invoke-virtual {v7}, Lco/uk/getmondo/transaction/splitting/a/a$c;->d()Ljava/lang/String;

    move-result-object v9

    aput-object v9, v8, v2

    invoke-virtual {p0, v3, v8}, Lco/uk/getmondo/transaction/splitting/SplitBottomSheetFragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    goto :goto_1

    .line 84
    :cond_3
    const v3, 0x7f0a038b

    new-array v8, v4, [Ljava/lang/Object;

    invoke-virtual {v7}, Lco/uk/getmondo/transaction/splitting/a/a$c;->b()Ljava/lang/String;

    move-result-object v9

    aput-object v9, v8, v2

    invoke-virtual {p0, v3, v8}, Lco/uk/getmondo/transaction/splitting/SplitBottomSheetFragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    goto :goto_1

    :cond_4
    move v3, v2

    .line 89
    goto :goto_2
.end method
