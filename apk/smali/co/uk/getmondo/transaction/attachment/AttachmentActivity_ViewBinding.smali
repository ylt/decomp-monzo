.class public Lco/uk/getmondo/transaction/attachment/AttachmentActivity_ViewBinding;
.super Ljava/lang/Object;
.source "AttachmentActivity_ViewBinding.java"

# interfaces
.implements Lbutterknife/Unbinder;


# instance fields
.field private a:Lco/uk/getmondo/transaction/attachment/AttachmentActivity;

.field private b:Landroid/view/View;

.field private c:Landroid/view/View;


# direct methods
.method public constructor <init>(Lco/uk/getmondo/transaction/attachment/AttachmentActivity;Landroid/view/View;)V
    .locals 4

    .prologue
    const v3, 0x7f110102

    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    iput-object p1, p0, Lco/uk/getmondo/transaction/attachment/AttachmentActivity_ViewBinding;->a:Lco/uk/getmondo/transaction/attachment/AttachmentActivity;

    .line 35
    const-string v0, "field \'notesInput\' and method \'onNotesFocusChanged\'"

    invoke-static {p2, v3, v0}, Lbutterknife/internal/Utils;->findRequiredView(Landroid/view/View;ILjava/lang/String;)Landroid/view/View;

    move-result-object v1

    .line 36
    const-string v0, "field \'notesInput\'"

    const-class v2, Landroid/widget/EditText;

    invoke-static {v1, v3, v0, v2}, Lbutterknife/internal/Utils;->castView(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p1, Lco/uk/getmondo/transaction/attachment/AttachmentActivity;->notesInput:Landroid/widget/EditText;

    .line 37
    iput-object v1, p0, Lco/uk/getmondo/transaction/attachment/AttachmentActivity_ViewBinding;->b:Landroid/view/View;

    .line 38
    new-instance v0, Lco/uk/getmondo/transaction/attachment/AttachmentActivity_ViewBinding$1;

    invoke-direct {v0, p0, p1}, Lco/uk/getmondo/transaction/attachment/AttachmentActivity_ViewBinding$1;-><init>(Lco/uk/getmondo/transaction/attachment/AttachmentActivity_ViewBinding;Lco/uk/getmondo/transaction/attachment/AttachmentActivity;)V

    invoke-virtual {v1, v0}, Landroid/view/View;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 44
    const v0, 0x7f110103

    const-string v1, "field \'imagePager\'"

    const-class v2, Landroid/support/v4/view/ViewPager;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/view/ViewPager;

    iput-object v0, p1, Lco/uk/getmondo/transaction/attachment/AttachmentActivity;->imagePager:Landroid/support/v4/view/ViewPager;

    .line 45
    const v0, 0x7f110104

    const-string v1, "field \'viewpagerIndicator\'"

    const-class v2, Lme/relex/circleindicator/CircleIndicator;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lme/relex/circleindicator/CircleIndicator;

    iput-object v0, p1, Lco/uk/getmondo/transaction/attachment/AttachmentActivity;->viewpagerIndicator:Lme/relex/circleindicator/CircleIndicator;

    .line 46
    const v0, 0x7f1100fe

    const-string v1, "field \'toolbar\'"

    const-class v2, Landroid/support/v7/widget/Toolbar;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/Toolbar;

    iput-object v0, p1, Lco/uk/getmondo/transaction/attachment/AttachmentActivity;->toolbar:Landroid/support/v7/widget/Toolbar;

    .line 47
    const v0, 0x7f110105

    const-string v1, "method \'onSelectFromDeviceClick\'"

    invoke-static {p2, v0, v1}, Lbutterknife/internal/Utils;->findRequiredView(Landroid/view/View;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 48
    iput-object v0, p0, Lco/uk/getmondo/transaction/attachment/AttachmentActivity_ViewBinding;->c:Landroid/view/View;

    .line 49
    new-instance v1, Lco/uk/getmondo/transaction/attachment/AttachmentActivity_ViewBinding$2;

    invoke-direct {v1, p0, p1}, Lco/uk/getmondo/transaction/attachment/AttachmentActivity_ViewBinding$2;-><init>(Lco/uk/getmondo/transaction/attachment/AttachmentActivity_ViewBinding;Lco/uk/getmondo/transaction/attachment/AttachmentActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 55
    return-void
.end method


# virtual methods
.method public unbind()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 60
    iget-object v0, p0, Lco/uk/getmondo/transaction/attachment/AttachmentActivity_ViewBinding;->a:Lco/uk/getmondo/transaction/attachment/AttachmentActivity;

    .line 61
    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Bindings already cleared."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 62
    :cond_0
    iput-object v1, p0, Lco/uk/getmondo/transaction/attachment/AttachmentActivity_ViewBinding;->a:Lco/uk/getmondo/transaction/attachment/AttachmentActivity;

    .line 64
    iput-object v1, v0, Lco/uk/getmondo/transaction/attachment/AttachmentActivity;->notesInput:Landroid/widget/EditText;

    .line 65
    iput-object v1, v0, Lco/uk/getmondo/transaction/attachment/AttachmentActivity;->imagePager:Landroid/support/v4/view/ViewPager;

    .line 66
    iput-object v1, v0, Lco/uk/getmondo/transaction/attachment/AttachmentActivity;->viewpagerIndicator:Lme/relex/circleindicator/CircleIndicator;

    .line 67
    iput-object v1, v0, Lco/uk/getmondo/transaction/attachment/AttachmentActivity;->toolbar:Landroid/support/v7/widget/Toolbar;

    .line 69
    iget-object v0, p0, Lco/uk/getmondo/transaction/attachment/AttachmentActivity_ViewBinding;->b:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 70
    iput-object v1, p0, Lco/uk/getmondo/transaction/attachment/AttachmentActivity_ViewBinding;->b:Landroid/view/View;

    .line 71
    iget-object v0, p0, Lco/uk/getmondo/transaction/attachment/AttachmentActivity_ViewBinding;->c:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 72
    iput-object v1, p0, Lco/uk/getmondo/transaction/attachment/AttachmentActivity_ViewBinding;->c:Landroid/view/View;

    .line 73
    return-void
.end method
