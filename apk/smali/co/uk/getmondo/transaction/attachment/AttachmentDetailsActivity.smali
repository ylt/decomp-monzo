.class public Lco/uk/getmondo/transaction/attachment/AttachmentDetailsActivity;
.super Lco/uk/getmondo/common/activities/b;
.source "AttachmentDetailsActivity.java"

# interfaces
.implements Lco/uk/getmondo/transaction/attachment/e$a;


# instance fields
.field a:Lco/uk/getmondo/transaction/attachment/e;

.field private b:Ljava/lang/String;

.field private c:Le/a/a/a/d;

.field deleteButton:Landroid/widget/Button;
    .annotation build Lbutterknife/BindView;
        value = 0x7f11010f
    .end annotation
.end field

.field progress:Landroid/widget/ProgressBar;
    .annotation build Lbutterknife/BindView;
        value = 0x7f11010e
    .end annotation
.end field

.field zoomableImageView:Landroid/widget/ImageView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f11010d
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Lco/uk/getmondo/common/activities/b;-><init>()V

    return-void
.end method

.method static synthetic a(Lco/uk/getmondo/transaction/attachment/AttachmentDetailsActivity;)Le/a/a/a/d;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lco/uk/getmondo/transaction/attachment/AttachmentDetailsActivity;->c:Le/a/a/a/d;

    return-object v0
.end method

.method static synthetic a(Lco/uk/getmondo/transaction/attachment/AttachmentDetailsActivity;Ljava/lang/Object;)Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 78
    iget-object v0, p0, Lco/uk/getmondo/transaction/attachment/AttachmentDetailsActivity;->b:Ljava/lang/String;

    return-object v0
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 40
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lco/uk/getmondo/transaction/attachment/AttachmentDetailsActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 41
    const-string v1, "EXTRA_IMAGE_URL"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 42
    const-string v1, "EXTRA_ATTACHMENT_ID"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 43
    invoke-virtual {p0, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 44
    return-void
.end method


# virtual methods
.method public a()Lio/reactivex/n;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 77
    iget-object v0, p0, Lco/uk/getmondo/transaction/attachment/AttachmentDetailsActivity;->deleteButton:Landroid/widget/Button;

    invoke-static {v0}, Lcom/b/a/c/c;->a(Landroid/view/View;)Lio/reactivex/n;

    move-result-object v0

    invoke-static {p0}, Lco/uk/getmondo/transaction/attachment/c;->a(Lco/uk/getmondo/transaction/attachment/AttachmentDetailsActivity;)Lio/reactivex/c/h;

    move-result-object v1

    .line 78
    invoke-virtual {v0, v1}, Lio/reactivex/n;->map(Lio/reactivex/c/h;)Lio/reactivex/n;

    move-result-object v0

    .line 77
    return-object v0
.end method

.method public b()V
    .locals 2

    .prologue
    .line 83
    iget-object v0, p0, Lco/uk/getmondo/transaction/attachment/AttachmentDetailsActivity;->progress:Landroid/widget/ProgressBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 84
    return-void
.end method

.method public c()V
    .locals 2

    .prologue
    .line 88
    iget-object v0, p0, Lco/uk/getmondo/transaction/attachment/AttachmentDetailsActivity;->progress:Landroid/widget/ProgressBar;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 89
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 48
    invoke-super {p0, p1}, Lco/uk/getmondo/common/activities/b;->onCreate(Landroid/os/Bundle;)V

    .line 49
    const v0, 0x7f050021

    invoke-virtual {p0, v0}, Lco/uk/getmondo/transaction/attachment/AttachmentDetailsActivity;->setContentView(I)V

    .line 50
    invoke-static {p0}, Lbutterknife/ButterKnife;->bind(Landroid/app/Activity;)Lbutterknife/Unbinder;

    .line 51
    invoke-virtual {p0}, Lco/uk/getmondo/transaction/attachment/AttachmentDetailsActivity;->l()Lco/uk/getmondo/common/h/b/b;

    move-result-object v0

    invoke-interface {v0, p0}, Lco/uk/getmondo/common/h/b/b;->a(Lco/uk/getmondo/transaction/attachment/AttachmentDetailsActivity;)V

    .line 53
    invoke-virtual {p0}, Lco/uk/getmondo/transaction/attachment/AttachmentDetailsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "EXTRA_ATTACHMENT_ID"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/transaction/attachment/AttachmentDetailsActivity;->b:Ljava/lang/String;

    .line 54
    invoke-virtual {p0}, Lco/uk/getmondo/transaction/attachment/AttachmentDetailsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "EXTRA_IMAGE_URL"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 56
    new-instance v1, Le/a/a/a/d;

    iget-object v2, p0, Lco/uk/getmondo/transaction/attachment/AttachmentDetailsActivity;->zoomableImageView:Landroid/widget/ImageView;

    invoke-direct {v1, v2}, Le/a/a/a/d;-><init>(Landroid/widget/ImageView;)V

    iput-object v1, p0, Lco/uk/getmondo/transaction/attachment/AttachmentDetailsActivity;->c:Le/a/a/a/d;

    .line 57
    invoke-static {p0}, Lcom/bumptech/glide/g;->a(Landroid/support/v4/app/j;)Lcom/bumptech/glide/j;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/bumptech/glide/j;->a(Ljava/lang/String;)Lcom/bumptech/glide/d;

    move-result-object v0

    new-instance v1, Lco/uk/getmondo/transaction/attachment/AttachmentDetailsActivity$1;

    iget-object v2, p0, Lco/uk/getmondo/transaction/attachment/AttachmentDetailsActivity;->zoomableImageView:Landroid/widget/ImageView;

    invoke-direct {v1, p0, v2}, Lco/uk/getmondo/transaction/attachment/AttachmentDetailsActivity$1;-><init>(Lco/uk/getmondo/transaction/attachment/AttachmentDetailsActivity;Landroid/widget/ImageView;)V

    invoke-virtual {v0, v1}, Lcom/bumptech/glide/d;->a(Lcom/bumptech/glide/g/b/j;)Lcom/bumptech/glide/g/b/j;

    .line 64
    iget-object v0, p0, Lco/uk/getmondo/transaction/attachment/AttachmentDetailsActivity;->c:Le/a/a/a/d;

    sget-object v1, Landroid/widget/ImageView$ScaleType;->FIT_CENTER:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v0, v1}, Le/a/a/a/d;->a(Landroid/widget/ImageView$ScaleType;)V

    .line 66
    iget-object v0, p0, Lco/uk/getmondo/transaction/attachment/AttachmentDetailsActivity;->a:Lco/uk/getmondo/transaction/attachment/e;

    invoke-virtual {v0, p0}, Lco/uk/getmondo/transaction/attachment/e;->a(Lco/uk/getmondo/transaction/attachment/e$a;)V

    .line 67
    return-void
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lco/uk/getmondo/transaction/attachment/AttachmentDetailsActivity;->a:Lco/uk/getmondo/transaction/attachment/e;

    invoke-virtual {v0}, Lco/uk/getmondo/transaction/attachment/e;->b()V

    .line 72
    invoke-super {p0}, Lco/uk/getmondo/common/activities/b;->onDestroy()V

    .line 73
    return-void
.end method
