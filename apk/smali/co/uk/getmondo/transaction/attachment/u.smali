.class Lco/uk/getmondo/transaction/attachment/u;
.super Landroid/support/v4/view/p;
.source "ImagePagerAdapter.java"


# instance fields
.field private final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lco/uk/getmondo/d/d;",
            ">;"
        }
    .end annotation
.end field

.field private b:Landroid/view/View$OnClickListener;


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 22
    invoke-direct {p0}, Landroid/support/v4/view/p;-><init>()V

    .line 23
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lco/uk/getmondo/transaction/attachment/u;->a:Ljava/util/List;

    return-void
.end method

.method static synthetic a(Lco/uk/getmondo/transaction/attachment/u;Landroid/view/View;)V
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lco/uk/getmondo/transaction/attachment/u;->b:Landroid/view/View$OnClickListener;

    invoke-interface {v0, p1}, Landroid/view/View$OnClickListener;->onClick(Landroid/view/View;)V

    return-void
.end method

.method static synthetic a(Lco/uk/getmondo/transaction/attachment/u;Ljava/lang/String;ILandroid/view/View;)V
    .locals 2

    .prologue
    .line 82
    invoke-virtual {p3}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v0, p0, Lco/uk/getmondo/transaction/attachment/u;->a:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/d/d;

    invoke-virtual {v0}, Lco/uk/getmondo/d/d;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, p1, v0}, Lco/uk/getmondo/transaction/attachment/AttachmentDetailsActivity;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public a(Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 109
    const/4 v0, -0x2

    return v0
.end method

.method public a(Landroid/view/ViewGroup;I)Ljava/lang/Object;
    .locals 8

    .prologue
    const/16 v7, 0x8

    const/4 v6, 0x0

    .line 39
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f050078

    invoke-virtual {v0, v1, p1, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 40
    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 42
    const v1, 0x7f110288

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 43
    const v2, 0x7f110287

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    .line 44
    const v3, 0x7f110286

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/Button;

    .line 45
    const v4, 0x7f110289

    invoke-virtual {v0, v4}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ProgressBar;

    .line 47
    iget-object v5, p0, Lco/uk/getmondo/transaction/attachment/u;->a:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    if-ne p2, v5, :cond_1

    .line 48
    invoke-virtual {v1, v7}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 49
    invoke-virtual {v3, v6}, Landroid/widget/Button;->setVisibility(I)V

    .line 50
    invoke-virtual {v2, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 51
    invoke-static {p0}, Lco/uk/getmondo/transaction/attachment/v;->a(Lco/uk/getmondo/transaction/attachment/u;)Landroid/view/View$OnClickListener;

    move-result-object v2

    invoke-virtual {v3, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 52
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 53
    invoke-virtual {v4, v7}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 54
    invoke-virtual {v1, v6}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 55
    invoke-virtual {v1, v6}, Landroid/widget/ImageView;->setClipToOutline(Z)V

    .line 84
    :cond_0
    :goto_0
    return-object v0

    .line 56
    :cond_1
    iget-object v5, p0, Lco/uk/getmondo/transaction/attachment/u;->a:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    if-ge p2, v5, :cond_0

    .line 57
    invoke-virtual {v1, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 58
    invoke-virtual {v3, v7}, Landroid/widget/Button;->setVisibility(I)V

    .line 59
    invoke-virtual {v2, v7}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 60
    invoke-virtual {v4, v6}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 62
    iget-object v2, p0, Lco/uk/getmondo/transaction/attachment/u;->a:Ljava/util/List;

    invoke-interface {v2, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lco/uk/getmondo/d/d;

    invoke-virtual {v2}, Lco/uk/getmondo/d/d;->b()Ljava/lang/String;

    move-result-object v2

    .line 63
    const v3, 0x7f0201f8

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 64
    const/4 v3, 0x1

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setClipToOutline(Z)V

    .line 65
    invoke-virtual {v0}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/bumptech/glide/g;->b(Landroid/content/Context;)Lcom/bumptech/glide/j;

    move-result-object v3

    .line 66
    invoke-virtual {v3, v2}, Lcom/bumptech/glide/j;->a(Ljava/lang/String;)Lcom/bumptech/glide/d;

    move-result-object v3

    new-instance v5, Lco/uk/getmondo/transaction/attachment/u$1;

    invoke-direct {v5, p0, v4}, Lco/uk/getmondo/transaction/attachment/u$1;-><init>(Lco/uk/getmondo/transaction/attachment/u;Landroid/widget/ProgressBar;)V

    .line 67
    invoke-virtual {v3, v5}, Lcom/bumptech/glide/d;->a(Lcom/bumptech/glide/g/d;)Lcom/bumptech/glide/c;

    move-result-object v3

    .line 81
    invoke-virtual {v3, v1}, Lcom/bumptech/glide/c;->a(Landroid/widget/ImageView;)Lcom/bumptech/glide/g/b/j;

    .line 82
    invoke-static {p0, v2, p2}, Lco/uk/getmondo/transaction/attachment/w;->a(Lco/uk/getmondo/transaction/attachment/u;Ljava/lang/String;I)Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0
.end method

.method a(Landroid/view/View$OnClickListener;)V
    .locals 0

    .prologue
    .line 34
    iput-object p1, p0, Lco/uk/getmondo/transaction/attachment/u;->b:Landroid/view/View$OnClickListener;

    .line 35
    return-void
.end method

.method public a(Landroid/view/ViewGroup;ILjava/lang/Object;)V
    .locals 0

    .prologue
    .line 89
    check-cast p3, Landroid/view/View;

    invoke-virtual {p1, p3}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 90
    return-void
.end method

.method a(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lco/uk/getmondo/d/d;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 27
    iget-object v0, p0, Lco/uk/getmondo/transaction/attachment/u;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 28
    iget-object v0, p0, Lco/uk/getmondo/transaction/attachment/u;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 30
    invoke-virtual {p0}, Lco/uk/getmondo/transaction/attachment/u;->c()V

    .line 31
    return-void
.end method

.method public a(Landroid/view/View;Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 94
    if-ne p1, p2, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()I
    .locals 1

    .prologue
    .line 104
    iget-object v0, p0, Lco/uk/getmondo/transaction/attachment/u;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    return v0
.end method

.method public c(I)Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 99
    const-string v0, ""

    return-object v0
.end method
