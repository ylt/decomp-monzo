.class public Lco/uk/getmondo/transaction/attachment/AttachmentDetailsActivity_ViewBinding;
.super Ljava/lang/Object;
.source "AttachmentDetailsActivity_ViewBinding.java"

# interfaces
.implements Lbutterknife/Unbinder;


# instance fields
.field private a:Lco/uk/getmondo/transaction/attachment/AttachmentDetailsActivity;


# direct methods
.method public constructor <init>(Lco/uk/getmondo/transaction/attachment/AttachmentDetailsActivity;Landroid/view/View;)V
    .locals 3

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    iput-object p1, p0, Lco/uk/getmondo/transaction/attachment/AttachmentDetailsActivity_ViewBinding;->a:Lco/uk/getmondo/transaction/attachment/AttachmentDetailsActivity;

    .line 28
    const v0, 0x7f11010d

    const-string v1, "field \'zoomableImageView\'"

    const-class v2, Landroid/widget/ImageView;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p1, Lco/uk/getmondo/transaction/attachment/AttachmentDetailsActivity;->zoomableImageView:Landroid/widget/ImageView;

    .line 29
    const v0, 0x7f11010f

    const-string v1, "field \'deleteButton\'"

    const-class v2, Landroid/widget/Button;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p1, Lco/uk/getmondo/transaction/attachment/AttachmentDetailsActivity;->deleteButton:Landroid/widget/Button;

    .line 30
    const v0, 0x7f11010e

    const-string v1, "field \'progress\'"

    const-class v2, Landroid/widget/ProgressBar;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p1, Lco/uk/getmondo/transaction/attachment/AttachmentDetailsActivity;->progress:Landroid/widget/ProgressBar;

    .line 31
    return-void
.end method


# virtual methods
.method public unbind()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 36
    iget-object v0, p0, Lco/uk/getmondo/transaction/attachment/AttachmentDetailsActivity_ViewBinding;->a:Lco/uk/getmondo/transaction/attachment/AttachmentDetailsActivity;

    .line 37
    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Bindings already cleared."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 38
    :cond_0
    iput-object v1, p0, Lco/uk/getmondo/transaction/attachment/AttachmentDetailsActivity_ViewBinding;->a:Lco/uk/getmondo/transaction/attachment/AttachmentDetailsActivity;

    .line 40
    iput-object v1, v0, Lco/uk/getmondo/transaction/attachment/AttachmentDetailsActivity;->zoomableImageView:Landroid/widget/ImageView;

    .line 41
    iput-object v1, v0, Lco/uk/getmondo/transaction/attachment/AttachmentDetailsActivity;->deleteButton:Landroid/widget/Button;

    .line 42
    iput-object v1, v0, Lco/uk/getmondo/transaction/attachment/AttachmentDetailsActivity;->progress:Landroid/widget/ProgressBar;

    .line 43
    return-void
.end method
