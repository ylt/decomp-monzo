.class public Lco/uk/getmondo/transaction/attachment/AttachmentActivity;
.super Lco/uk/getmondo/common/activities/b;
.source "AttachmentActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lco/uk/getmondo/transaction/attachment/m$a;


# instance fields
.field a:Lco/uk/getmondo/transaction/attachment/m;

.field private b:Lco/uk/getmondo/transaction/attachment/u;

.field private final c:Landroid/os/Handler;

.field private final e:Ljava/lang/Runnable;

.field imagePager:Landroid/support/v4/view/ViewPager;
    .annotation build Lbutterknife/BindView;
        value = 0x7f110103
    .end annotation
.end field

.field notesInput:Landroid/widget/EditText;
    .annotation build Lbutterknife/BindView;
        value = 0x7f110102
    .end annotation
.end field

.field toolbar:Landroid/support/v7/widget/Toolbar;
    .annotation build Lbutterknife/BindView;
        value = 0x7f1100fe
    .end annotation
.end field

.field viewpagerIndicator:Lme/relex/circleindicator/CircleIndicator;
    .annotation build Lbutterknife/BindView;
        value = 0x7f110104
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 33
    invoke-direct {p0}, Lco/uk/getmondo/common/activities/b;-><init>()V

    .line 47
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lco/uk/getmondo/transaction/attachment/AttachmentActivity;->c:Landroid/os/Handler;

    .line 48
    invoke-static {p0}, Lco/uk/getmondo/transaction/attachment/a;->a(Lco/uk/getmondo/transaction/attachment/AttachmentActivity;)Ljava/lang/Runnable;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/transaction/attachment/AttachmentActivity;->e:Ljava/lang/Runnable;

    return-void
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 51
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lco/uk/getmondo/transaction/attachment/AttachmentActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 52
    const-string v1, "EXTRA_TRANSACTION_ID"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 53
    invoke-virtual {p0, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 54
    return-void
.end method

.method private b(Ljava/io/File;)V
    .locals 2

    .prologue
    .line 136
    invoke-static {p0, p1}, Lco/uk/getmondo/common/k;->a(Landroid/content/Context;Ljava/io/File;)Landroid/content/Intent;

    move-result-object v0

    .line 137
    if-eqz v0, :cond_0

    .line 138
    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lco/uk/getmondo/transaction/attachment/AttachmentActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 140
    :cond_0
    return-void
.end method

.method private f()Z
    .locals 1

    .prologue
    .line 132
    const-string v0, "android.permission.CAMERA"

    invoke-static {p0, v0}, Landroid/support/v4/content/a;->b(Landroid/content/Context;Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private g()V
    .locals 2

    .prologue
    .line 177
    invoke-static {}, Lco/uk/getmondo/common/k;->a()Landroid/content/Intent;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lco/uk/getmondo/transaction/attachment/AttachmentActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 178
    return-void
.end method


# virtual methods
.method public a()V
    .locals 0

    .prologue
    .line 157
    invoke-direct {p0}, Lco/uk/getmondo/transaction/attachment/AttachmentActivity;->g()V

    .line 158
    return-void
.end method

.method public a(Ljava/io/File;)V
    .locals 3

    .prologue
    .line 124
    invoke-direct {p0}, Lco/uk/getmondo/transaction/attachment/AttachmentActivity;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 125
    invoke-direct {p0, p1}, Lco/uk/getmondo/transaction/attachment/AttachmentActivity;->b(Ljava/io/File;)V

    .line 129
    :goto_0
    return-void

    .line 127
    :cond_0
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "android.permission.CAMERA"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    invoke-static {p0, v0, v1}, Landroid/support/v4/app/a;->a(Landroid/app/Activity;[Ljava/lang/String;I)V

    goto :goto_0
.end method

.method public a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 115
    iget-object v0, p0, Lco/uk/getmondo/transaction/attachment/AttachmentActivity;->toolbar:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {v0, p1}, Landroid/support/v7/widget/Toolbar;->setTitle(Ljava/lang/CharSequence;)V

    .line 116
    return-void
.end method

.method public a(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lco/uk/getmondo/d/d;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 193
    iget-object v0, p0, Lco/uk/getmondo/transaction/attachment/AttachmentActivity;->b:Lco/uk/getmondo/transaction/attachment/u;

    invoke-virtual {v0, p1}, Lco/uk/getmondo/transaction/attachment/u;->a(Ljava/util/List;)V

    .line 194
    return-void
.end method

.method public b()V
    .locals 1

    .prologue
    .line 162
    const v0, 0x7f0a00e4

    invoke-virtual {p0, v0}, Lco/uk/getmondo/transaction/attachment/AttachmentActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lco/uk/getmondo/transaction/attachment/AttachmentActivity;->c(Ljava/lang/String;)V

    .line 163
    return-void
.end method

.method public c()V
    .locals 4

    .prologue
    .line 167
    iget-object v0, p0, Lco/uk/getmondo/transaction/attachment/AttachmentActivity;->c:Landroid/os/Handler;

    iget-object v1, p0, Lco/uk/getmondo/transaction/attachment/AttachmentActivity;->e:Ljava/lang/Runnable;

    const-wide/16 v2, 0x12c

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 168
    return-void
.end method

.method public d()V
    .locals 2

    .prologue
    .line 172
    iget-object v0, p0, Lco/uk/getmondo/transaction/attachment/AttachmentActivity;->c:Landroid/os/Handler;

    iget-object v1, p0, Lco/uk/getmondo/transaction/attachment/AttachmentActivity;->e:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 173
    invoke-virtual {p0}, Lco/uk/getmondo/transaction/attachment/AttachmentActivity;->t()V

    .line 174
    return-void
.end method

.method public d(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 203
    iget-object v0, p0, Lco/uk/getmondo/transaction/attachment/AttachmentActivity;->notesInput:Landroid/widget/EditText;

    invoke-virtual {v0, p1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 204
    return-void
.end method

.method public e()V
    .locals 0

    .prologue
    .line 198
    invoke-virtual {p0}, Lco/uk/getmondo/transaction/attachment/AttachmentActivity;->finish()V

    .line 199
    return-void
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 182
    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    if-ne p2, v1, :cond_0

    .line 183
    iget-object v0, p0, Lco/uk/getmondo/transaction/attachment/AttachmentActivity;->a:Lco/uk/getmondo/transaction/attachment/m;

    invoke-virtual {v0}, Lco/uk/getmondo/transaction/attachment/m;->c()V

    .line 185
    :cond_0
    const/4 v0, 0x2

    if-ne p1, v0, :cond_1

    if-ne p2, v1, :cond_1

    if-eqz p3, :cond_1

    invoke-virtual {p3}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 186
    invoke-virtual {p3}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    .line 187
    iget-object v1, p0, Lco/uk/getmondo/transaction/attachment/AttachmentActivity;->a:Lco/uk/getmondo/transaction/attachment/m;

    invoke-virtual {v1, v0}, Lco/uk/getmondo/transaction/attachment/m;->b(Ljava/lang/String;)V

    .line 189
    :cond_1
    return-void
.end method

.method public onBackPressed()V
    .locals 2

    .prologue
    .line 100
    iget-object v0, p0, Lco/uk/getmondo/transaction/attachment/AttachmentActivity;->a:Lco/uk/getmondo/transaction/attachment/m;

    iget-object v1, p0, Lco/uk/getmondo/transaction/attachment/AttachmentActivity;->notesInput:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lco/uk/getmondo/transaction/attachment/m;->c(Ljava/lang/String;)V

    .line 101
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 105
    iget-object v0, p0, Lco/uk/getmondo/transaction/attachment/AttachmentActivity;->a:Lco/uk/getmondo/transaction/attachment/m;

    invoke-virtual {v0}, Lco/uk/getmondo/transaction/attachment/m;->a()V

    .line 106
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 58
    invoke-super {p0, p1}, Lco/uk/getmondo/common/activities/b;->onCreate(Landroid/os/Bundle;)V

    .line 59
    const v0, 0x7f05001d

    invoke-virtual {p0, v0}, Lco/uk/getmondo/transaction/attachment/AttachmentActivity;->setContentView(I)V

    .line 61
    invoke-static {p0}, Lbutterknife/ButterKnife;->bind(Landroid/app/Activity;)Lbutterknife/Unbinder;

    .line 62
    invoke-virtual {p0}, Lco/uk/getmondo/transaction/attachment/AttachmentActivity;->l()Lco/uk/getmondo/common/h/b/b;

    move-result-object v0

    invoke-interface {v0, p0}, Lco/uk/getmondo/common/h/b/b;->a(Lco/uk/getmondo/transaction/attachment/AttachmentActivity;)V

    .line 63
    iget-object v0, p0, Lco/uk/getmondo/transaction/attachment/AttachmentActivity;->toolbar:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {p0, v0}, Lco/uk/getmondo/transaction/attachment/AttachmentActivity;->setSupportActionBar(Landroid/support/v7/widget/Toolbar;)V

    .line 65
    new-instance v0, Lco/uk/getmondo/transaction/attachment/u;

    invoke-direct {v0}, Lco/uk/getmondo/transaction/attachment/u;-><init>()V

    iput-object v0, p0, Lco/uk/getmondo/transaction/attachment/AttachmentActivity;->b:Lco/uk/getmondo/transaction/attachment/u;

    .line 66
    iget-object v0, p0, Lco/uk/getmondo/transaction/attachment/AttachmentActivity;->b:Lco/uk/getmondo/transaction/attachment/u;

    invoke-virtual {v0, p0}, Lco/uk/getmondo/transaction/attachment/u;->a(Landroid/view/View$OnClickListener;)V

    .line 67
    iget-object v0, p0, Lco/uk/getmondo/transaction/attachment/AttachmentActivity;->imagePager:Landroid/support/v4/view/ViewPager;

    iget-object v1, p0, Lco/uk/getmondo/transaction/attachment/AttachmentActivity;->b:Lco/uk/getmondo/transaction/attachment/u;

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setAdapter(Landroid/support/v4/view/p;)V

    .line 68
    iget-object v0, p0, Lco/uk/getmondo/transaction/attachment/AttachmentActivity;->viewpagerIndicator:Lme/relex/circleindicator/CircleIndicator;

    iget-object v1, p0, Lco/uk/getmondo/transaction/attachment/AttachmentActivity;->imagePager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0, v1}, Lme/relex/circleindicator/CircleIndicator;->setViewPager(Landroid/support/v4/view/ViewPager;)V

    .line 69
    iget-object v0, p0, Lco/uk/getmondo/transaction/attachment/AttachmentActivity;->a:Lco/uk/getmondo/transaction/attachment/m;

    invoke-virtual {v0, p0}, Lco/uk/getmondo/transaction/attachment/m;->a(Lco/uk/getmondo/common/ui/f;)V

    .line 71
    invoke-virtual {p0}, Lco/uk/getmondo/transaction/attachment/AttachmentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "EXTRA_TRANSACTION_ID"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 72
    iget-object v1, p0, Lco/uk/getmondo/transaction/attachment/AttachmentActivity;->a:Lco/uk/getmondo/transaction/attachment/m;

    invoke-virtual {v1, v0}, Lco/uk/getmondo/transaction/attachment/m;->a(Ljava/lang/String;)V

    .line 73
    return-void
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 77
    iget-object v0, p0, Lco/uk/getmondo/transaction/attachment/AttachmentActivity;->a:Lco/uk/getmondo/transaction/attachment/m;

    invoke-virtual {v0}, Lco/uk/getmondo/transaction/attachment/m;->b()V

    .line 78
    invoke-super {p0}, Lco/uk/getmondo/common/activities/b;->onDestroy()V

    .line 79
    return-void
.end method

.method onNotesFocusChanged(Z)V
    .locals 1
    .annotation build Lbutterknife/OnFocusChange;
        value = {
            0x7f110102
        }
    .end annotation

    .prologue
    .line 93
    if-eqz p1, :cond_0

    .line 94
    iget-object v0, p0, Lco/uk/getmondo/transaction/attachment/AttachmentActivity;->a:Lco/uk/getmondo/transaction/attachment/m;

    invoke-virtual {v0}, Lco/uk/getmondo/transaction/attachment/m;->e()V

    .line 96
    :cond_0
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2

    .prologue
    .line 83
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 88
    invoke-super {p0, p1}, Lco/uk/getmondo/common/activities/b;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    :goto_0
    return v0

    .line 85
    :pswitch_0
    iget-object v0, p0, Lco/uk/getmondo/transaction/attachment/AttachmentActivity;->a:Lco/uk/getmondo/transaction/attachment/m;

    iget-object v1, p0, Lco/uk/getmondo/transaction/attachment/AttachmentActivity;->notesInput:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lco/uk/getmondo/transaction/attachment/m;->c(Ljava/lang/String;)V

    .line 86
    const/4 v0, 0x1

    goto :goto_0

    .line 83
    nop

    :pswitch_data_0
    .packed-switch 0x102002c
        :pswitch_0
    .end packed-switch
.end method

.method public onRequestPermissionsResult(I[Ljava/lang/String;[I)V
    .locals 2

    .prologue
    .line 144
    const/4 v0, 0x3

    if-ne p1, v0, :cond_2

    .line 145
    array-length v0, p3

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x0

    aget v0, p3, v0

    if-eqz v0, :cond_1

    .line 146
    :cond_0
    iget-object v0, p0, Lco/uk/getmondo/transaction/attachment/AttachmentActivity;->a:Lco/uk/getmondo/transaction/attachment/m;

    invoke-virtual {v0}, Lco/uk/getmondo/transaction/attachment/m;->g()V

    .line 153
    :goto_0
    return-void

    .line 148
    :cond_1
    iget-object v0, p0, Lco/uk/getmondo/transaction/attachment/AttachmentActivity;->a:Lco/uk/getmondo/transaction/attachment/m;

    invoke-virtual {v0}, Lco/uk/getmondo/transaction/attachment/m;->f()V

    goto :goto_0

    .line 151
    :cond_2
    invoke-super {p0, p1, p2, p3}, Lco/uk/getmondo/common/activities/b;->onRequestPermissionsResult(I[Ljava/lang/String;[I)V

    goto :goto_0
.end method

.method onSelectFromDeviceClick()V
    .locals 1
    .annotation build Lbutterknife/OnClick;
        value = {
            0x7f110105
        }
    .end annotation

    .prologue
    .line 110
    iget-object v0, p0, Lco/uk/getmondo/transaction/attachment/AttachmentActivity;->a:Lco/uk/getmondo/transaction/attachment/m;

    invoke-virtual {v0}, Lco/uk/getmondo/transaction/attachment/m;->d()V

    .line 111
    return-void
.end method
