.class Lco/uk/getmondo/transaction/attachment/e;
.super Lco/uk/getmondo/common/ui/b;
.source "AttachmentDetailsPresenter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lco/uk/getmondo/transaction/attachment/e$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lco/uk/getmondo/common/ui/b",
        "<",
        "Lco/uk/getmondo/transaction/attachment/e$a;",
        ">;"
    }
.end annotation


# instance fields
.field private final c:Lio/reactivex/u;

.field private final d:Lio/reactivex/u;

.field private final e:Lco/uk/getmondo/common/e/a;

.field private final f:Lco/uk/getmondo/transaction/a/a;


# direct methods
.method constructor <init>(Lio/reactivex/u;Lio/reactivex/u;Lco/uk/getmondo/common/e/a;Lco/uk/getmondo/transaction/a/a;)V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Lco/uk/getmondo/common/ui/b;-><init>()V

    .line 28
    iput-object p1, p0, Lco/uk/getmondo/transaction/attachment/e;->c:Lio/reactivex/u;

    .line 29
    iput-object p2, p0, Lco/uk/getmondo/transaction/attachment/e;->d:Lio/reactivex/u;

    .line 30
    iput-object p3, p0, Lco/uk/getmondo/transaction/attachment/e;->e:Lco/uk/getmondo/common/e/a;

    .line 31
    iput-object p4, p0, Lco/uk/getmondo/transaction/attachment/e;->f:Lco/uk/getmondo/transaction/a/a;

    .line 32
    return-void
.end method

.method static synthetic a(Lco/uk/getmondo/transaction/attachment/e;Lco/uk/getmondo/transaction/attachment/e$a;Ljava/lang/String;)Lio/reactivex/d;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 39
    iget-object v0, p0, Lco/uk/getmondo/transaction/attachment/e;->f:Lco/uk/getmondo/transaction/a/a;

    invoke-virtual {v0, p2}, Lco/uk/getmondo/transaction/a/a;->c(Ljava/lang/String;)Lio/reactivex/b;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/transaction/attachment/e;->d:Lio/reactivex/u;

    .line 40
    invoke-virtual {v0, v1}, Lio/reactivex/b;->b(Lio/reactivex/u;)Lio/reactivex/b;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/transaction/attachment/e;->c:Lio/reactivex/u;

    .line 41
    invoke-virtual {v0, v1}, Lio/reactivex/b;->a(Lio/reactivex/u;)Lio/reactivex/b;

    move-result-object v0

    invoke-static {p1}, Lco/uk/getmondo/transaction/attachment/h;->a(Lco/uk/getmondo/transaction/attachment/e$a;)Lio/reactivex/c/g;

    move-result-object v1

    .line 42
    invoke-virtual {v0, v1}, Lio/reactivex/b;->c(Lio/reactivex/c/g;)Lio/reactivex/b;

    move-result-object v0

    invoke-static {p1}, Lco/uk/getmondo/transaction/attachment/i;->a(Lco/uk/getmondo/transaction/attachment/e$a;)Lio/reactivex/c/g;

    move-result-object v1

    .line 43
    invoke-virtual {v0, v1}, Lio/reactivex/b;->b(Lio/reactivex/c/g;)Lio/reactivex/b;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    invoke-static {p1}, Lco/uk/getmondo/transaction/attachment/j;->a(Lco/uk/getmondo/transaction/attachment/e$a;)Lio/reactivex/c/a;

    move-result-object v1

    .line 44
    invoke-virtual {v0, v1}, Lio/reactivex/b;->b(Lio/reactivex/c/a;)Lio/reactivex/b;

    move-result-object v0

    invoke-static {p0, p1}, Lco/uk/getmondo/transaction/attachment/k;->a(Lco/uk/getmondo/transaction/attachment/e;Lco/uk/getmondo/transaction/attachment/e$a;)Lio/reactivex/c/g;

    move-result-object v1

    .line 45
    invoke-virtual {v0, v1}, Lio/reactivex/b;->a(Lio/reactivex/c/g;)Lio/reactivex/b;

    move-result-object v0

    .line 46
    invoke-virtual {v0}, Lio/reactivex/b;->b()Lio/reactivex/b;

    move-result-object v0

    .line 39
    return-object v0
.end method

.method static synthetic a(Lco/uk/getmondo/transaction/attachment/e$a;Lio/reactivex/b/b;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 42
    invoke-interface {p0}, Lco/uk/getmondo/transaction/attachment/e$a;->b()V

    return-void
.end method

.method static synthetic a(Lco/uk/getmondo/transaction/attachment/e$a;Ljava/lang/Throwable;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 43
    invoke-interface {p0}, Lco/uk/getmondo/transaction/attachment/e$a;->c()V

    return-void
.end method

.method static synthetic a(Lco/uk/getmondo/transaction/attachment/e;Lco/uk/getmondo/transaction/attachment/e$a;Ljava/lang/Throwable;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 45
    iget-object v0, p0, Lco/uk/getmondo/transaction/attachment/e;->e:Lco/uk/getmondo/common/e/a;

    invoke-virtual {v0, p2, p1}, Lco/uk/getmondo/common/e/a;->a(Ljava/lang/Throwable;Lco/uk/getmondo/common/e/a$a;)Z

    return-void
.end method


# virtual methods
.method public bridge synthetic a(Lco/uk/getmondo/common/ui/f;)V
    .locals 0

    .prologue
    .line 17
    check-cast p1, Lco/uk/getmondo/transaction/attachment/e$a;

    invoke-virtual {p0, p1}, Lco/uk/getmondo/transaction/attachment/e;->a(Lco/uk/getmondo/transaction/attachment/e$a;)V

    return-void
.end method

.method public a(Lco/uk/getmondo/transaction/attachment/e$a;)V
    .locals 3

    .prologue
    .line 36
    invoke-super {p0, p1}, Lco/uk/getmondo/common/ui/b;->a(Lco/uk/getmondo/common/ui/f;)V

    .line 38
    invoke-interface {p1}, Lco/uk/getmondo/transaction/attachment/e$a;->a()Lio/reactivex/n;

    move-result-object v0

    invoke-static {p0, p1}, Lco/uk/getmondo/transaction/attachment/f;->a(Lco/uk/getmondo/transaction/attachment/e;Lco/uk/getmondo/transaction/attachment/e$a;)Lio/reactivex/c/h;

    move-result-object v1

    .line 39
    invoke-virtual {v0, v1}, Lio/reactivex/n;->flatMapCompletable(Lio/reactivex/c/h;)Lio/reactivex/b;

    move-result-object v0

    .line 47
    invoke-static {}, Lco/uk/getmondo/common/j/a;->a()Lio/reactivex/c/a;

    move-result-object v1

    invoke-static {}, Lco/uk/getmondo/transaction/attachment/g;->a()Lio/reactivex/c/g;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lio/reactivex/b;->a(Lio/reactivex/c/a;Lio/reactivex/c/g;)Lio/reactivex/b/b;

    move-result-object v0

    .line 38
    invoke-virtual {p0, v0}, Lco/uk/getmondo/transaction/attachment/e;->a(Lio/reactivex/b/b;)V

    .line 48
    return-void
.end method
