.class Lco/uk/getmondo/transaction/attachment/m;
.super Lco/uk/getmondo/common/ui/b;
.source "AttachmentPresenter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lco/uk/getmondo/transaction/attachment/m$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lco/uk/getmondo/common/ui/b",
        "<",
        "Lco/uk/getmondo/transaction/attachment/m$a;",
        ">;"
    }
.end annotation


# instance fields
.field private final c:Lio/reactivex/u;

.field private final d:Lio/reactivex/u;

.field private final e:Lco/uk/getmondo/common/accounts/d;

.field private final f:Lco/uk/getmondo/transaction/a/a;

.field private final g:Lco/uk/getmondo/common/k/l;

.field private final h:Lco/uk/getmondo/common/a;

.field private final i:Lco/uk/getmondo/common/e/a;

.field private j:Ljava/lang/String;

.field private k:Ljava/lang/String;

.field private l:Ljava/io/File;


# direct methods
.method constructor <init>(Lio/reactivex/u;Lio/reactivex/u;Lco/uk/getmondo/common/accounts/d;Lco/uk/getmondo/transaction/a/a;Lco/uk/getmondo/common/k/l;Lco/uk/getmondo/common/a;Lco/uk/getmondo/common/e/a;)V
    .locals 0

    .prologue
    .line 47
    invoke-direct {p0}, Lco/uk/getmondo/common/ui/b;-><init>()V

    .line 48
    iput-object p1, p0, Lco/uk/getmondo/transaction/attachment/m;->c:Lio/reactivex/u;

    .line 49
    iput-object p2, p0, Lco/uk/getmondo/transaction/attachment/m;->d:Lio/reactivex/u;

    .line 50
    iput-object p3, p0, Lco/uk/getmondo/transaction/attachment/m;->e:Lco/uk/getmondo/common/accounts/d;

    .line 51
    iput-object p4, p0, Lco/uk/getmondo/transaction/attachment/m;->f:Lco/uk/getmondo/transaction/a/a;

    .line 52
    iput-object p5, p0, Lco/uk/getmondo/transaction/attachment/m;->g:Lco/uk/getmondo/common/k/l;

    .line 53
    iput-object p6, p0, Lco/uk/getmondo/transaction/attachment/m;->h:Lco/uk/getmondo/common/a;

    .line 54
    iput-object p7, p0, Lco/uk/getmondo/transaction/attachment/m;->i:Lco/uk/getmondo/common/e/a;

    .line 55
    return-void
.end method

.method static synthetic a(Lco/uk/getmondo/transaction/attachment/m;Ljava/io/InputStream;JLjava/lang/String;)Lio/reactivex/d;
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 100
    iget-object v1, p0, Lco/uk/getmondo/transaction/attachment/m;->f:Lco/uk/getmondo/transaction/a/a;

    iget-object v6, p0, Lco/uk/getmondo/transaction/attachment/m;->j:Ljava/lang/String;

    move-object v2, p4

    move-object v3, p1

    move-wide v4, p2

    invoke-virtual/range {v1 .. v6}, Lco/uk/getmondo/transaction/a/a;->a(Ljava/lang/String;Ljava/io/InputStream;JLjava/lang/String;)Lio/reactivex/b;

    move-result-object v0

    return-object v0
.end method

.method private a(Lco/uk/getmondo/d/aj;)V
    .locals 2

    .prologue
    .line 74
    invoke-virtual {p1}, Lco/uk/getmondo/d/aj;->e()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_2

    const-string v0, ""

    :goto_0
    iput-object v0, p0, Lco/uk/getmondo/transaction/attachment/m;->k:Ljava/lang/String;

    .line 75
    invoke-virtual {p1}, Lco/uk/getmondo/d/aj;->f()Lco/uk/getmondo/d/u;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 76
    iget-object v0, p0, Lco/uk/getmondo/transaction/attachment/m;->a:Lco/uk/getmondo/common/ui/f;

    check-cast v0, Lco/uk/getmondo/transaction/attachment/m$a;

    invoke-virtual {p1}, Lco/uk/getmondo/d/aj;->f()Lco/uk/getmondo/d/u;

    move-result-object v1

    invoke-virtual {v1}, Lco/uk/getmondo/d/u;->i()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lco/uk/getmondo/transaction/attachment/m$a;->a(Ljava/lang/String;)V

    .line 78
    :cond_0
    invoke-virtual {p1}, Lco/uk/getmondo/d/aj;->e()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 79
    iget-object v0, p0, Lco/uk/getmondo/transaction/attachment/m;->a:Lco/uk/getmondo/common/ui/f;

    check-cast v0, Lco/uk/getmondo/transaction/attachment/m$a;

    invoke-virtual {p1}, Lco/uk/getmondo/d/aj;->e()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lco/uk/getmondo/transaction/attachment/m$a;->d(Ljava/lang/String;)V

    .line 81
    :cond_1
    iget-object v0, p0, Lco/uk/getmondo/transaction/attachment/m;->a:Lco/uk/getmondo/common/ui/f;

    check-cast v0, Lco/uk/getmondo/transaction/attachment/m$a;

    invoke-virtual {p1}, Lco/uk/getmondo/d/aj;->A()Lio/realm/az;

    move-result-object v1

    invoke-interface {v0, v1}, Lco/uk/getmondo/transaction/attachment/m$a;->a(Ljava/util/List;)V

    .line 82
    return-void

    .line 74
    :cond_2
    invoke-virtual {p1}, Lco/uk/getmondo/d/aj;->e()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method static synthetic a(Lco/uk/getmondo/transaction/attachment/m;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 127
    iget-object v0, p0, Lco/uk/getmondo/transaction/attachment/m;->a:Lco/uk/getmondo/common/ui/f;

    check-cast v0, Lco/uk/getmondo/transaction/attachment/m$a;

    invoke-interface {v0}, Lco/uk/getmondo/transaction/attachment/m$a;->d()V

    .line 128
    iget-object v0, p0, Lco/uk/getmondo/transaction/attachment/m;->a:Lco/uk/getmondo/common/ui/f;

    check-cast v0, Lco/uk/getmondo/transaction/attachment/m$a;

    invoke-interface {v0}, Lco/uk/getmondo/transaction/attachment/m$a;->e()V

    .line 129
    return-void
.end method

.method static synthetic a(Lco/uk/getmondo/transaction/attachment/m;Lco/uk/getmondo/d/aj;)V
    .locals 0

    invoke-direct {p0, p1}, Lco/uk/getmondo/transaction/attachment/m;->a(Lco/uk/getmondo/d/aj;)V

    return-void
.end method

.method static synthetic a(Lco/uk/getmondo/transaction/attachment/m;Ljava/lang/Throwable;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 131
    iget-object v0, p0, Lco/uk/getmondo/transaction/attachment/m;->a:Lco/uk/getmondo/common/ui/f;

    check-cast v0, Lco/uk/getmondo/transaction/attachment/m$a;

    invoke-interface {v0}, Lco/uk/getmondo/transaction/attachment/m$a;->d()V

    .line 132
    iget-object v1, p0, Lco/uk/getmondo/transaction/attachment/m;->i:Lco/uk/getmondo/common/e/a;

    iget-object v0, p0, Lco/uk/getmondo/transaction/attachment/m;->a:Lco/uk/getmondo/common/ui/f;

    check-cast v0, Lco/uk/getmondo/common/e/a$a;

    invoke-virtual {v1, p1, v0}, Lco/uk/getmondo/common/e/a;->a(Ljava/lang/Throwable;Lco/uk/getmondo/common/e/a$a;)Z

    .line 133
    return-void
.end method

.method private a(Ljava/io/InputStream;J)V
    .locals 4

    .prologue
    .line 98
    iget-object v0, p0, Lco/uk/getmondo/transaction/attachment/m;->a:Lco/uk/getmondo/common/ui/f;

    check-cast v0, Lco/uk/getmondo/transaction/attachment/m$a;

    invoke-interface {v0}, Lco/uk/getmondo/transaction/attachment/m$a;->b()V

    .line 99
    iget-object v0, p0, Lco/uk/getmondo/transaction/attachment/m;->e:Lco/uk/getmondo/common/accounts/d;

    invoke-virtual {v0}, Lco/uk/getmondo/common/accounts/d;->d()Lio/reactivex/v;

    move-result-object v0

    invoke-static {p0, p1, p2, p3}, Lco/uk/getmondo/transaction/attachment/o;->a(Lco/uk/getmondo/transaction/attachment/m;Ljava/io/InputStream;J)Lio/reactivex/c/h;

    move-result-object v1

    .line 100
    invoke-virtual {v0, v1}, Lio/reactivex/v;->c(Lio/reactivex/c/h;)Lio/reactivex/b;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/transaction/attachment/m;->d:Lio/reactivex/u;

    .line 101
    invoke-virtual {v0, v1}, Lio/reactivex/b;->b(Lio/reactivex/u;)Lio/reactivex/b;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/transaction/attachment/m;->c:Lio/reactivex/u;

    .line 102
    invoke-virtual {v0, v1}, Lio/reactivex/b;->a(Lio/reactivex/u;)Lio/reactivex/b;

    move-result-object v0

    invoke-static {p0}, Lco/uk/getmondo/transaction/attachment/p;->a(Lco/uk/getmondo/transaction/attachment/m;)Lio/reactivex/c/a;

    move-result-object v1

    invoke-static {p0}, Lco/uk/getmondo/transaction/attachment/q;->a(Lco/uk/getmondo/transaction/attachment/m;)Lio/reactivex/c/g;

    move-result-object v2

    .line 103
    invoke-virtual {v0, v1, v2}, Lio/reactivex/b;->a(Lio/reactivex/c/a;Lio/reactivex/c/g;)Lio/reactivex/b/b;

    move-result-object v0

    .line 99
    invoke-virtual {p0, v0}, Lco/uk/getmondo/transaction/attachment/m;->a(Lio/reactivex/b/b;)V

    .line 110
    return-void
.end method

.method static synthetic b(Lco/uk/getmondo/transaction/attachment/m;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 103
    iget-object v0, p0, Lco/uk/getmondo/transaction/attachment/m;->a:Lco/uk/getmondo/common/ui/f;

    check-cast v0, Lco/uk/getmondo/transaction/attachment/m$a;

    invoke-interface {v0}, Lco/uk/getmondo/transaction/attachment/m$a;->d()V

    return-void
.end method

.method static synthetic b(Lco/uk/getmondo/transaction/attachment/m;Ljava/lang/Throwable;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 105
    iget-object v0, p0, Lco/uk/getmondo/transaction/attachment/m;->a:Lco/uk/getmondo/common/ui/f;

    check-cast v0, Lco/uk/getmondo/transaction/attachment/m$a;

    invoke-interface {v0}, Lco/uk/getmondo/transaction/attachment/m$a;->d()V

    .line 106
    iget-object v1, p0, Lco/uk/getmondo/transaction/attachment/m;->i:Lco/uk/getmondo/common/e/a;

    iget-object v0, p0, Lco/uk/getmondo/transaction/attachment/m;->a:Lco/uk/getmondo/common/ui/f;

    check-cast v0, Lco/uk/getmondo/common/e/a$a;

    invoke-virtual {v1, p1, v0}, Lco/uk/getmondo/common/e/a;->a(Ljava/lang/Throwable;Lco/uk/getmondo/common/e/a$a;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 107
    iget-object v0, p0, Lco/uk/getmondo/transaction/attachment/m;->a:Lco/uk/getmondo/common/ui/f;

    check-cast v0, Lco/uk/getmondo/transaction/attachment/m$a;

    const v1, 0x7f0a01e5

    invoke-interface {v0, v1}, Lco/uk/getmondo/transaction/attachment/m$a;->b(I)V

    .line 109
    :cond_0
    return-void
.end method


# virtual methods
.method a()V
    .locals 2

    .prologue
    .line 58
    iget-object v0, p0, Lco/uk/getmondo/transaction/attachment/m;->h:Lco/uk/getmondo/common/a;

    invoke-static {}, Lco/uk/getmondo/api/model/tracking/Impression;->o()Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v1

    invoke-virtual {v0, v1}, Lco/uk/getmondo/common/a;->a(Lco/uk/getmondo/api/model/tracking/Impression;)V

    .line 60
    iget-object v0, p0, Lco/uk/getmondo/transaction/attachment/m;->g:Lco/uk/getmondo/common/k/l;

    invoke-virtual {v0}, Lco/uk/getmondo/common/k/l;->a()Ljava/io/File;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/transaction/attachment/m;->l:Ljava/io/File;

    .line 61
    iget-object v0, p0, Lco/uk/getmondo/transaction/attachment/m;->l:Ljava/io/File;

    if-eqz v0, :cond_0

    .line 62
    iget-object v0, p0, Lco/uk/getmondo/transaction/attachment/m;->a:Lco/uk/getmondo/common/ui/f;

    check-cast v0, Lco/uk/getmondo/transaction/attachment/m$a;

    iget-object v1, p0, Lco/uk/getmondo/transaction/attachment/m;->l:Ljava/io/File;

    invoke-interface {v0, v1}, Lco/uk/getmondo/transaction/attachment/m$a;->a(Ljava/io/File;)V

    .line 64
    :cond_0
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 67
    iput-object p1, p0, Lco/uk/getmondo/transaction/attachment/m;->j:Ljava/lang/String;

    .line 68
    iget-object v0, p0, Lco/uk/getmondo/transaction/attachment/m;->f:Lco/uk/getmondo/transaction/a/a;

    invoke-virtual {v0, p1}, Lco/uk/getmondo/transaction/a/a;->b(Ljava/lang/String;)Lio/reactivex/n;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/transaction/attachment/m;->c:Lio/reactivex/u;

    .line 69
    invoke-virtual {v0, v1}, Lio/reactivex/n;->observeOn(Lio/reactivex/u;)Lio/reactivex/n;

    move-result-object v0

    invoke-static {p0}, Lco/uk/getmondo/transaction/attachment/n;->a(Lco/uk/getmondo/transaction/attachment/m;)Lio/reactivex/c/g;

    move-result-object v1

    .line 70
    invoke-virtual {v0, v1}, Lio/reactivex/n;->subscribe(Lio/reactivex/c/g;)Lio/reactivex/b/b;

    move-result-object v0

    .line 68
    invoke-virtual {p0, v0}, Lco/uk/getmondo/transaction/attachment/m;->a(Lio/reactivex/b/b;)V

    .line 71
    return-void
.end method

.method b(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 113
    iget-object v0, p0, Lco/uk/getmondo/transaction/attachment/m;->g:Lco/uk/getmondo/common/k/l;

    invoke-virtual {v0, p1}, Lco/uk/getmondo/common/k/l;->a(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v0

    .line 114
    iget-object v1, p0, Lco/uk/getmondo/transaction/attachment/m;->g:Lco/uk/getmondo/common/k/l;

    invoke-virtual {v1, p1}, Lco/uk/getmondo/common/k/l;->b(Ljava/lang/String;)J

    move-result-wide v2

    .line 115
    invoke-direct {p0, v0, v2, v3}, Lco/uk/getmondo/transaction/attachment/m;->a(Ljava/io/InputStream;J)V

    .line 116
    return-void
.end method

.method c()V
    .locals 4

    .prologue
    .line 85
    iget-object v0, p0, Lco/uk/getmondo/transaction/attachment/m;->l:Ljava/io/File;

    if-nez v0, :cond_0

    .line 95
    :goto_0
    return-void

    .line 89
    :cond_0
    :try_start_0
    new-instance v0, Ljava/io/FileInputStream;

    iget-object v1, p0, Lco/uk/getmondo/transaction/attachment/m;->l:Ljava/io/File;

    invoke-direct {v0, v1}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    .line 90
    iget-object v1, p0, Lco/uk/getmondo/transaction/attachment/m;->l:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->length()J

    move-result-wide v2

    .line 91
    invoke-direct {p0, v0, v2, v3}, Lco/uk/getmondo/transaction/attachment/m;->a(Ljava/io/InputStream;J)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 92
    :catch_0
    move-exception v0

    .line 93
    invoke-static {v0}, Ld/a/a;->a(Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method c(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 119
    iget-object v0, p0, Lco/uk/getmondo/transaction/attachment/m;->k:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 120
    iget-object v0, p0, Lco/uk/getmondo/transaction/attachment/m;->a:Lco/uk/getmondo/common/ui/f;

    check-cast v0, Lco/uk/getmondo/transaction/attachment/m$a;

    invoke-interface {v0}, Lco/uk/getmondo/transaction/attachment/m$a;->e()V

    .line 135
    :goto_0
    return-void

    .line 122
    :cond_0
    iget-object v0, p0, Lco/uk/getmondo/transaction/attachment/m;->a:Lco/uk/getmondo/common/ui/f;

    check-cast v0, Lco/uk/getmondo/transaction/attachment/m$a;

    invoke-interface {v0}, Lco/uk/getmondo/transaction/attachment/m$a;->c()V

    .line 123
    iget-object v0, p0, Lco/uk/getmondo/transaction/attachment/m;->f:Lco/uk/getmondo/transaction/a/a;

    iget-object v1, p0, Lco/uk/getmondo/transaction/attachment/m;->j:Ljava/lang/String;

    invoke-virtual {v0, p1, v1}, Lco/uk/getmondo/transaction/a/a;->a(Ljava/lang/String;Ljava/lang/String;)Lio/reactivex/b;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/transaction/attachment/m;->d:Lio/reactivex/u;

    .line 124
    invoke-virtual {v0, v1}, Lio/reactivex/b;->b(Lio/reactivex/u;)Lio/reactivex/b;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/transaction/attachment/m;->c:Lio/reactivex/u;

    .line 125
    invoke-virtual {v0, v1}, Lio/reactivex/b;->a(Lio/reactivex/u;)Lio/reactivex/b;

    move-result-object v0

    invoke-static {p0}, Lco/uk/getmondo/transaction/attachment/r;->a(Lco/uk/getmondo/transaction/attachment/m;)Lio/reactivex/c/a;

    move-result-object v1

    invoke-static {p0}, Lco/uk/getmondo/transaction/attachment/s;->a(Lco/uk/getmondo/transaction/attachment/m;)Lio/reactivex/c/g;

    move-result-object v2

    .line 126
    invoke-virtual {v0, v1, v2}, Lio/reactivex/b;->a(Lio/reactivex/c/a;Lio/reactivex/c/g;)Lio/reactivex/b/b;

    move-result-object v0

    .line 123
    invoke-virtual {p0, v0}, Lco/uk/getmondo/transaction/attachment/m;->a(Lio/reactivex/b/b;)V

    goto :goto_0
.end method

.method d()V
    .locals 2

    .prologue
    .line 138
    iget-object v0, p0, Lco/uk/getmondo/transaction/attachment/m;->h:Lco/uk/getmondo/common/a;

    invoke-static {}, Lco/uk/getmondo/api/model/tracking/Impression;->p()Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v1

    invoke-virtual {v0, v1}, Lco/uk/getmondo/common/a;->a(Lco/uk/getmondo/api/model/tracking/Impression;)V

    .line 139
    iget-object v0, p0, Lco/uk/getmondo/transaction/attachment/m;->a:Lco/uk/getmondo/common/ui/f;

    check-cast v0, Lco/uk/getmondo/transaction/attachment/m$a;

    invoke-interface {v0}, Lco/uk/getmondo/transaction/attachment/m$a;->a()V

    .line 140
    return-void
.end method

.method e()V
    .locals 2

    .prologue
    .line 143
    iget-object v0, p0, Lco/uk/getmondo/transaction/attachment/m;->h:Lco/uk/getmondo/common/a;

    invoke-static {}, Lco/uk/getmondo/api/model/tracking/Impression;->n()Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v1

    invoke-virtual {v0, v1}, Lco/uk/getmondo/common/a;->a(Lco/uk/getmondo/api/model/tracking/Impression;)V

    .line 144
    return-void
.end method

.method f()V
    .locals 2

    .prologue
    .line 147
    iget-object v0, p0, Lco/uk/getmondo/transaction/attachment/m;->l:Ljava/io/File;

    if-eqz v0, :cond_0

    .line 148
    iget-object v0, p0, Lco/uk/getmondo/transaction/attachment/m;->a:Lco/uk/getmondo/common/ui/f;

    check-cast v0, Lco/uk/getmondo/transaction/attachment/m$a;

    iget-object v1, p0, Lco/uk/getmondo/transaction/attachment/m;->l:Ljava/io/File;

    invoke-interface {v0, v1}, Lco/uk/getmondo/transaction/attachment/m$a;->a(Ljava/io/File;)V

    .line 150
    :cond_0
    return-void
.end method

.method g()V
    .locals 2

    .prologue
    .line 153
    iget-object v0, p0, Lco/uk/getmondo/transaction/attachment/m;->a:Lco/uk/getmondo/common/ui/f;

    check-cast v0, Lco/uk/getmondo/transaction/attachment/m$a;

    const v1, 0x7f0a00e3

    invoke-interface {v0, v1}, Lco/uk/getmondo/transaction/attachment/m$a;->b(I)V

    .line 154
    return-void
.end method
