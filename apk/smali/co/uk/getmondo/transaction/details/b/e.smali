.class public interface abstract Lco/uk/getmondo/transaction/details/b/e;
.super Ljava/lang/Object;
.source "Content.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lco/uk/getmondo/transaction/details/b/e$a;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0008f\u0018\u00002\u00020\u0001R\u0016\u0010\u0002\u001a\u0004\u0018\u00010\u00038VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0004\u0010\u0005R\u0016\u0010\u0006\u001a\u0004\u0018\u00010\u00078VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0008\u0010\t\u00a8\u0006\n"
    }
    d2 = {
        "Lco/uk/getmondo/transaction/details/base/Content;",
        "",
        "furtherInformationWebLink",
        "Lco/uk/getmondo/transaction/details/base/InformationWebLink;",
        "getFurtherInformationWebLink",
        "()Lco/uk/getmondo/transaction/details/base/InformationWebLink;",
        "yourHistory",
        "Lco/uk/getmondo/transaction/details/base/YourHistory;",
        "getYourHistory",
        "()Lco/uk/getmondo/transaction/details/base/YourHistory;",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# virtual methods
.method public abstract a()Lco/uk/getmondo/transaction/details/b/h;
.end method

.method public abstract b()Lco/uk/getmondo/transaction/details/b/n;
.end method
