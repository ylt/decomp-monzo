.class public abstract Lco/uk/getmondo/transaction/details/b/d;
.super Ljava/lang/Object;
.source "BaseHeader.kt"

# interfaces
.implements Lco/uk/getmondo/transaction/details/b/g;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u00000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0005\n\u0002\u0010\u000b\n\u0002\u0008\u0003\u0008&\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\n\u0010\u0014\u001a\u0004\u0018\u00010\u000cH\u0016R\u0016\u0010\u0005\u001a\u0004\u0018\u00010\u00068VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0007\u0010\u0008R\u000e\u0010\t\u001a\u00020\nX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u000b\u001a\u00020\u000c8VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\r\u0010\u000eR\u0016\u0010\u000f\u001a\u0004\u0018\u00010\u000c8VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0010\u0010\u000eR\u0014\u0010\u0011\u001a\u00020\u00128VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0011\u0010\u0013R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0015"
    }
    d2 = {
        "Lco/uk/getmondo/transaction/details/base/BaseHeader;",
        "Lco/uk/getmondo/transaction/details/base/Header;",
        "transaction",
        "Lco/uk/getmondo/model/Transaction;",
        "(Lco/uk/getmondo/model/Transaction;)V",
        "amount",
        "Lco/uk/getmondo/model/Amount;",
        "getAmount",
        "()Lco/uk/getmondo/model/Amount;",
        "amountFormatter",
        "Lco/uk/getmondo/common/money/AmountFormatter;",
        "date",
        "",
        "getDate",
        "()Ljava/lang/String;",
        "declinedReason",
        "getDeclinedReason",
        "isDeclined",
        "",
        "()Z",
        "localAmountText",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field private final a:Lco/uk/getmondo/common/i/b;

.field private final b:Lco/uk/getmondo/d/aj;


# direct methods
.method public constructor <init>(Lco/uk/getmondo/d/aj;)V
    .locals 6

    .prologue
    const/4 v2, 0x0

    const-string v0, "transaction"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lco/uk/getmondo/transaction/details/b/d;->b:Lco/uk/getmondo/d/aj;

    .line 9
    new-instance v0, Lco/uk/getmondo/common/i/b;

    const/4 v1, 0x1

    const/4 v4, 0x6

    const/4 v5, 0x0

    move v3, v2

    invoke-direct/range {v0 .. v5}, Lco/uk/getmondo/common/i/b;-><init>(ZZZILkotlin/d/b/i;)V

    iput-object v0, p0, Lco/uk/getmondo/transaction/details/b/d;->a:Lco/uk/getmondo/common/i/b;

    return-void
.end method


# virtual methods
.method public a(Landroid/content/Context;)Lco/uk/getmondo/transaction/details/b/j;
    .locals 1

    .prologue
    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 7
    invoke-static {p0, p1}, Lco/uk/getmondo/transaction/details/b/g$a;->a(Lco/uk/getmondo/transaction/details/b/g;Landroid/content/Context;)Lco/uk/getmondo/transaction/details/b/j;

    move-result-object v0

    return-object v0
.end method

.method public a()Ljava/lang/String;
    .locals 2

    .prologue
    .line 12
    iget-object v0, p0, Lco/uk/getmondo/transaction/details/b/d;->b:Lco/uk/getmondo/d/aj;

    invoke-virtual {v0}, Lco/uk/getmondo/d/aj;->o()Ljava/lang/String;

    move-result-object v0

    const-string v1, "transaction.createdAtDateTimeString"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public a(Landroid/content/res/Resources;)Ljava/lang/String;
    .locals 1

    .prologue
    const-string v0, "resources"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 7
    invoke-static {p0, p1}, Lco/uk/getmondo/transaction/details/b/g$a;->a(Lco/uk/getmondo/transaction/details/b/g;Landroid/content/res/Resources;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public b()Lco/uk/getmondo/d/c;
    .locals 1

    .prologue
    .line 15
    iget-object v0, p0, Lco/uk/getmondo/transaction/details/b/d;->b:Lco/uk/getmondo/d/aj;

    invoke-virtual {v0}, Lco/uk/getmondo/d/aj;->g()Lco/uk/getmondo/d/c;

    move-result-object v0

    return-object v0
.end method

.method public b(Landroid/content/Context;)Lco/uk/getmondo/transaction/details/b/j;
    .locals 1

    .prologue
    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 7
    invoke-static {p0, p1}, Lco/uk/getmondo/transaction/details/b/g$a;->b(Lco/uk/getmondo/transaction/details/b/g;Landroid/content/Context;)Lco/uk/getmondo/transaction/details/b/j;

    move-result-object v0

    return-object v0
.end method

.method public b(Landroid/content/res/Resources;)Ljava/lang/String;
    .locals 1

    .prologue
    const-string v0, "resources"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 7
    invoke-static {p0, p1}, Lco/uk/getmondo/transaction/details/b/g$a;->b(Lco/uk/getmondo/transaction/details/b/g;Landroid/content/res/Resources;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public c()Z
    .locals 1

    .prologue
    .line 18
    iget-object v0, p0, Lco/uk/getmondo/transaction/details/b/d;->b:Lco/uk/getmondo/d/aj;

    invoke-virtual {v0}, Lco/uk/getmondo/d/aj;->p()Z

    move-result v0

    return v0
.end method

.method public d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lco/uk/getmondo/transaction/details/b/d;->b:Lco/uk/getmondo/d/aj;

    invoke-virtual {v0}, Lco/uk/getmondo/d/aj;->p()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lco/uk/getmondo/transaction/details/b/d;->b:Lco/uk/getmondo/d/aj;

    invoke-virtual {v0}, Lco/uk/getmondo/d/aj;->d()Lcom/c/b/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/c/b/b;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public e()Ljava/lang/String;
    .locals 4

    .prologue
    .line 24
    iget-object v0, p0, Lco/uk/getmondo/transaction/details/b/d;->b:Lco/uk/getmondo/d/aj;

    invoke-virtual {v0}, Lco/uk/getmondo/d/aj;->i()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 26
    iget-object v0, p0, Lco/uk/getmondo/transaction/details/b/d;->a:Lco/uk/getmondo/common/i/b;

    iget-object v1, p0, Lco/uk/getmondo/transaction/details/b/d;->b:Lco/uk/getmondo/d/aj;

    invoke-virtual {v1}, Lco/uk/getmondo/d/aj;->h()Lco/uk/getmondo/d/c;

    move-result-object v1

    const-string v2, "transaction.localAmount"

    invoke-static {v1, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lco/uk/getmondo/common/i/b;->a(Lco/uk/getmondo/d/c;)Ljava/lang/String;

    move-result-object v0

    .line 27
    iget-object v1, p0, Lco/uk/getmondo/transaction/details/b/d;->b:Lco/uk/getmondo/d/aj;

    invoke-virtual {v1}, Lco/uk/getmondo/d/aj;->h()Lco/uk/getmondo/d/c;

    move-result-object v1

    invoke-virtual {v1}, Lco/uk/getmondo/d/c;->l()Lco/uk/getmondo/common/i/c;

    move-result-object v1

    invoke-virtual {v1}, Lco/uk/getmondo/common/i/c;->b()Ljava/lang/String;

    move-result-object v1

    .line 28
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v2, 0x20

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 24
    :goto_0
    return-object v0

    .line 30
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public f()I
    .locals 1

    .prologue
    .line 7
    invoke-static {p0}, Lco/uk/getmondo/transaction/details/b/g$a;->a(Lco/uk/getmondo/transaction/details/b/g;)I

    move-result v0

    return v0
.end method

.method public g()Z
    .locals 1

    .prologue
    .line 7
    invoke-static {p0}, Lco/uk/getmondo/transaction/details/b/g$a;->b(Lco/uk/getmondo/transaction/details/b/g;)Z

    move-result v0

    return v0
.end method

.method public h()Lco/uk/getmondo/transaction/details/c/c;
    .locals 1

    .prologue
    .line 7
    invoke-static {p0}, Lco/uk/getmondo/transaction/details/b/g$a;->c(Lco/uk/getmondo/transaction/details/b/g;)Lco/uk/getmondo/transaction/details/c/c;

    move-result-object v0

    return-object v0
.end method

.method public i()Z
    .locals 1

    .prologue
    .line 7
    invoke-static {p0}, Lco/uk/getmondo/transaction/details/b/g$a;->d(Lco/uk/getmondo/transaction/details/b/g;)Z

    move-result v0

    return v0
.end method
