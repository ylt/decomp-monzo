.class public interface abstract Lco/uk/getmondo/transaction/details/b/g;
.super Ljava/lang/Object;
.source "Header.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lco/uk/getmondo/transaction/details/b/g$a;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000P\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u0008\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u000e\n\u0002\u0008\u0005\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0008\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0008f\u0018\u00002\u00020\u0001J\u0012\u0010\u001f\u001a\u0004\u0018\u00010\u000f2\u0006\u0010 \u001a\u00020!H\u0016J\n\u0010\"\u001a\u0004\u0018\u00010\u000fH\u0016J\u0012\u0010#\u001a\u0004\u0018\u00010\u000f2\u0006\u0010 \u001a\u00020!H\u0016J\u0010\u0010$\u001a\u00020%2\u0006\u0010&\u001a\u00020\'H\u0016J\u0010\u0010(\u001a\u00020\u000f2\u0006\u0010 \u001a\u00020!H&J\u0010\u0010)\u001a\u00020%2\u0006\u0010&\u001a\u00020\'H\u0016R\u0014\u0010\u0002\u001a\u0004\u0018\u00010\u0003X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0004\u0010\u0005R\u0014\u0010\u0006\u001a\u00020\u00078WX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0008\u0010\tR\u0012\u0010\n\u001a\u00020\u000bX\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u000c\u0010\rR\u0012\u0010\u000e\u001a\u00020\u000fX\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0010\u0010\u0011R\u0014\u0010\u0012\u001a\u0004\u0018\u00010\u000fX\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0013\u0010\u0011R\u0012\u0010\u0014\u001a\u00020\u0015X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0014\u0010\u0016R\u0016\u0010\u0017\u001a\u0004\u0018\u00010\u00188VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0019\u0010\u001aR\u0014\u0010\u001b\u001a\u00020\u00158VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u001c\u0010\u0016R\u0014\u0010\u001d\u001a\u00020\u00158VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u001e\u0010\u0016\u00a8\u0006*"
    }
    d2 = {
        "Lco/uk/getmondo/transaction/details/base/Header;",
        "",
        "amount",
        "Lco/uk/getmondo/model/Amount;",
        "getAmount",
        "()Lco/uk/getmondo/model/Amount;",
        "amountDisplayStyle",
        "",
        "getAmountDisplayStyle",
        "()I",
        "avatar",
        "Lco/uk/getmondo/transaction/details/base/Avatar;",
        "getAvatar",
        "()Lco/uk/getmondo/transaction/details/base/Avatar;",
        "date",
        "",
        "getDate",
        "()Ljava/lang/String;",
        "declinedReason",
        "getDeclinedReason",
        "isDeclined",
        "",
        "()Z",
        "mapCoordinates",
        "Lco/uk/getmondo/transaction/details/model/MapCoordinates;",
        "getMapCoordinates",
        "()Lco/uk/getmondo/transaction/details/model/MapCoordinates;",
        "showCategoryChooser",
        "getShowCategoryChooser",
        "showSubtitleBeforeTitle",
        "getShowSubtitleBeforeTitle",
        "extraInformation",
        "resources",
        "Landroid/content/res/Resources;",
        "localAmountText",
        "subtitle",
        "subtitleTextAppearance",
        "Lco/uk/getmondo/transaction/details/base/TextAppearance;",
        "context",
        "Landroid/content/Context;",
        "title",
        "titleTextAppearance",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# virtual methods
.method public abstract c(Landroid/content/res/Resources;)Ljava/lang/String;
.end method

.method public abstract j()Lco/uk/getmondo/transaction/details/b/c;
.end method
