.class public interface abstract Lco/uk/getmondo/transaction/details/b/a;
.super Ljava/lang/Object;
.source "Action.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lco/uk/getmondo/transaction/details/b/a$a;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000.\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0003\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008f\u0018\u00002\u00020\u0001J\u0010\u0010\u0006\u001a\u00020\u00072\u0006\u0010\u0008\u001a\u00020\tH&J\u0008\u0010\n\u001a\u00020\u000bH&J\u0012\u0010\u000c\u001a\u0004\u0018\u00010\u00032\u0006\u0010\r\u001a\u00020\u000eH\u0016J\u0010\u0010\u000f\u001a\u00020\u00032\u0006\u0010\r\u001a\u00020\u000eH&R\u0016\u0010\u0002\u001a\u0004\u0018\u00010\u00038VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0004\u0010\u0005\u00a8\u0006\u0010"
    }
    d2 = {
        "Lco/uk/getmondo/transaction/details/base/Action;",
        "",
        "iconUrl",
        "",
        "getIconUrl",
        "()Ljava/lang/String;",
        "action",
        "",
        "activity",
        "Landroid/support/v7/app/AppCompatActivity;",
        "placeholderIcon",
        "",
        "subtitle",
        "resources",
        "Landroid/content/res/Resources;",
        "title",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# virtual methods
.method public abstract a()I
.end method

.method public abstract a(Landroid/content/res/Resources;)Ljava/lang/String;
.end method

.method public abstract a(Landroid/support/v7/app/e;)V
.end method

.method public abstract b()Ljava/lang/String;
.end method

.method public abstract b(Landroid/content/res/Resources;)Ljava/lang/String;
.end method
