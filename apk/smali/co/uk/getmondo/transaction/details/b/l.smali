.class public interface abstract Lco/uk/getmondo/transaction/details/b/l;
.super Ljava/lang/Object;
.source "TransactionViewModel.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lco/uk/getmondo/transaction/details/b/l$a;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000.\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0008f\u0018\u00002\u00020\u0001R\u0018\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0005\u0010\u0006R\u0012\u0010\u0007\u001a\u00020\u0008X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\t\u0010\nR\u0016\u0010\u000b\u001a\u0004\u0018\u00010\u000c8VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\r\u0010\u000eR\u0012\u0010\u000f\u001a\u00020\u0010X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0011\u0010\u0012\u00a8\u0006\u0013"
    }
    d2 = {
        "Lco/uk/getmondo/transaction/details/base/TransactionViewModel;",
        "",
        "actions",
        "",
        "Lco/uk/getmondo/transaction/details/base/Action;",
        "getActions",
        "()Ljava/util/List;",
        "content",
        "Lco/uk/getmondo/transaction/details/base/Content;",
        "getContent",
        "()Lco/uk/getmondo/transaction/details/base/Content;",
        "footer",
        "Lco/uk/getmondo/transaction/details/base/Footer;",
        "getFooter",
        "()Lco/uk/getmondo/transaction/details/base/Footer;",
        "header",
        "Lco/uk/getmondo/transaction/details/base/BaseHeader;",
        "getHeader",
        "()Lco/uk/getmondo/transaction/details/base/BaseHeader;",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# virtual methods
.method public abstract a()Lco/uk/getmondo/transaction/details/b/d;
.end method

.method public abstract b()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lco/uk/getmondo/transaction/details/b/a;",
            ">;"
        }
    .end annotation
.end method

.method public abstract c()Lco/uk/getmondo/transaction/details/b/e;
.end method

.method public abstract d()Lco/uk/getmondo/transaction/details/b/f;
.end method
