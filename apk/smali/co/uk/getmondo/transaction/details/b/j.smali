.class public abstract Lco/uk/getmondo/transaction/details/b/j;
.super Ljava/lang/Object;
.source "Header.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u00002\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0007\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0002\u0008\u0008\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u00086\u0018\u00002\u00020\u0001B+\u0008\u0002\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0005\u0012\n\u0008\u0002\u0010\u0007\u001a\u0004\u0018\u00010\u0008\u00a2\u0006\u0002\u0010\tR\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\n\u0010\u000bR\u0013\u0010\u0007\u001a\u0004\u0018\u00010\u0008\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000c\u0010\rR\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000e\u0010\u000fR\u0011\u0010\u0006\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0010\u0010\u000b\u0082\u0001\u0004\u0011\u0012\u0013\u0014\u00a8\u0006\u0015"
    }
    d2 = {
        "Lco/uk/getmondo/transaction/details/base/TextAppearance;",
        "",
        "size",
        "",
        "colour",
        "",
        "style",
        "fontFamily",
        "",
        "(FIILjava/lang/String;)V",
        "getColour",
        "()I",
        "getFontFamily",
        "()Ljava/lang/String;",
        "getSize",
        "()F",
        "getStyle",
        "Lco/uk/getmondo/transaction/details/base/TitleTextAppearance;",
        "Lco/uk/getmondo/transaction/details/base/AddressTextAppearance;",
        "Lco/uk/getmondo/transaction/details/base/SubTitleTextAppearance;",
        "Lco/uk/getmondo/transaction/details/base/UrlTextAppearance;",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field private final a:F

.field private final b:I

.field private final c:I

.field private final d:Ljava/lang/String;


# direct methods
.method private constructor <init>(FIILjava/lang/String;)V
    .locals 0

    .prologue
    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lco/uk/getmondo/transaction/details/b/j;->a:F

    iput p2, p0, Lco/uk/getmondo/transaction/details/b/j;->b:I

    iput p3, p0, Lco/uk/getmondo/transaction/details/b/j;->c:I

    iput-object p4, p0, Lco/uk/getmondo/transaction/details/b/j;->d:Ljava/lang/String;

    return-void
.end method

.method synthetic constructor <init>(FIILjava/lang/String;ILkotlin/d/b/i;)V
    .locals 1

    .prologue
    and-int/lit8 v0, p5, 0x8

    if-eqz v0, :cond_0

    .line 44
    const/4 v0, 0x0

    check-cast v0, Ljava/lang/String;

    :goto_0
    invoke-direct {p0, p1, p2, p3, v0}, Lco/uk/getmondo/transaction/details/b/j;-><init>(FIILjava/lang/String;)V

    return-void

    :cond_0
    move-object v0, p4

    goto :goto_0
.end method

.method public synthetic constructor <init>(FIILjava/lang/String;Lkotlin/d/b/i;)V
    .locals 0

    .prologue
    .line 40
    invoke-direct {p0, p1, p2, p3, p4}, Lco/uk/getmondo/transaction/details/b/j;-><init>(FIILjava/lang/String;)V

    return-void
.end method


# virtual methods
.method public final a()F
    .locals 1

    .prologue
    .line 41
    iget v0, p0, Lco/uk/getmondo/transaction/details/b/j;->a:F

    return v0
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 42
    iget v0, p0, Lco/uk/getmondo/transaction/details/b/j;->b:I

    return v0
.end method

.method public final c()I
    .locals 1

    .prologue
    .line 43
    iget v0, p0, Lco/uk/getmondo/transaction/details/b/j;->c:I

    return v0
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lco/uk/getmondo/transaction/details/b/j;->d:Ljava/lang/String;

    return-object v0
.end method
