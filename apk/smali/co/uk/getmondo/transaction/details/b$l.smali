.class final Lco/uk/getmondo/transaction/details/b$l;
.super Ljava/lang/Object;
.source "TransactionDetailsPresenter.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lco/uk/getmondo/transaction/details/b;->a(Lco/uk/getmondo/transaction/details/b$a;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h",
        "<",
        "Lkotlin/h",
        "<+",
        "Lco/uk/getmondo/d/h;",
        "+",
        "Lco/uk/getmondo/d/aj;",
        ">;",
        "Lio/reactivex/d;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000\u0018\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\n \u0002*\u0004\u0018\u00010\u00010\u00012\"\u0010\u0003\u001a\u001e\u0012\u000c\u0012\n \u0002*\u0004\u0018\u00010\u00050\u0005\u0012\u000c\u0012\n \u0002*\u0004\u0018\u00010\u00060\u00060\u0004H\n\u00a2\u0006\u0002\u0008\u0007"
    }
    d2 = {
        "<anonymous>",
        "Lio/reactivex/Completable;",
        "kotlin.jvm.PlatformType",
        "<name for destructuring parameter 0>",
        "Lkotlin/Pair;",
        "Lco/uk/getmondo/model/Category;",
        "Lco/uk/getmondo/model/Transaction;",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field final synthetic a:Lco/uk/getmondo/transaction/details/b;

.field final synthetic b:Lco/uk/getmondo/transaction/details/b$a;


# direct methods
.method constructor <init>(Lco/uk/getmondo/transaction/details/b;Lco/uk/getmondo/transaction/details/b$a;)V
    .locals 0

    iput-object p1, p0, Lco/uk/getmondo/transaction/details/b$l;->a:Lco/uk/getmondo/transaction/details/b;

    iput-object p2, p0, Lco/uk/getmondo/transaction/details/b$l;->b:Lco/uk/getmondo/transaction/details/b$a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lkotlin/h;)Lio/reactivex/b;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/h",
            "<+",
            "Lco/uk/getmondo/d/h;",
            "+",
            "Lco/uk/getmondo/d/aj;",
            ">;)",
            "Lio/reactivex/b;"
        }
    .end annotation

    .prologue
    const-string v0, "<name for destructuring parameter 0>"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lkotlin/h;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/d/h;

    .line 98
    iget-object v1, p0, Lco/uk/getmondo/transaction/details/b$l;->a:Lco/uk/getmondo/transaction/details/b;

    invoke-static {v1}, Lco/uk/getmondo/transaction/details/b;->b(Lco/uk/getmondo/transaction/details/b;)Lco/uk/getmondo/transaction/a/a;

    move-result-object v1

    iget-object v2, p0, Lco/uk/getmondo/transaction/details/b$l;->a:Lco/uk/getmondo/transaction/details/b;

    invoke-static {v2}, Lco/uk/getmondo/transaction/details/b;->g(Lco/uk/getmondo/transaction/details/b;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Lco/uk/getmondo/transaction/a/a;->a(Ljava/lang/String;Lco/uk/getmondo/d/h;)Lio/reactivex/b;

    move-result-object v0

    .line 99
    iget-object v1, p0, Lco/uk/getmondo/transaction/details/b$l;->a:Lco/uk/getmondo/transaction/details/b;

    invoke-static {v1}, Lco/uk/getmondo/transaction/details/b;->h(Lco/uk/getmondo/transaction/details/b;)Lio/reactivex/u;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/b;->b(Lio/reactivex/u;)Lio/reactivex/b;

    move-result-object v0

    .line 100
    iget-object v1, p0, Lco/uk/getmondo/transaction/details/b$l;->a:Lco/uk/getmondo/transaction/details/b;

    invoke-static {v1}, Lco/uk/getmondo/transaction/details/b;->i(Lco/uk/getmondo/transaction/details/b;)Lio/reactivex/u;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/b;->a(Lio/reactivex/u;)Lio/reactivex/b;

    move-result-object v1

    .line 101
    new-instance v0, Lco/uk/getmondo/transaction/details/b$l$1;

    invoke-direct {v0, p0}, Lco/uk/getmondo/transaction/details/b$l$1;-><init>(Lco/uk/getmondo/transaction/details/b$l;)V

    check-cast v0, Lio/reactivex/c/g;

    invoke-virtual {v1, v0}, Lio/reactivex/b;->a(Lio/reactivex/c/g;)Lio/reactivex/b;

    move-result-object v0

    .line 102
    invoke-virtual {v0}, Lio/reactivex/b;->b()Lio/reactivex/b;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 35
    check-cast p1, Lkotlin/h;

    invoke-virtual {p0, p1}, Lco/uk/getmondo/transaction/details/b$l;->a(Lkotlin/h;)Lio/reactivex/b;

    move-result-object v0

    return-object v0
.end method
