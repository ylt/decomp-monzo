.class public final Lco/uk/getmondo/transaction/details/d;
.super Ljava/lang/Object;
.source "TransactionDetailsPresenter_Factory.java"

# interfaces
.implements Lb/a/b;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lb/a/b",
        "<",
        "Lco/uk/getmondo/transaction/details/b;",
        ">;"
    }
.end annotation


# static fields
.field static final synthetic a:Z


# instance fields
.field private final b:Lb/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lb/a",
            "<",
            "Lco/uk/getmondo/transaction/details/b;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Ljavax/a/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/a/a",
            "<",
            "Lio/reactivex/u;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Ljavax/a/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/a/a",
            "<",
            "Lio/reactivex/u;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Ljavax/a/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/a/a",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final f:Ljavax/a/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/transaction/a/a;",
            ">;"
        }
    .end annotation
.end field

.field private final g:Ljavax/a/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/background_sync/d;",
            ">;"
        }
    .end annotation
.end field

.field private final h:Ljavax/a/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/common/e/a;",
            ">;"
        }
    .end annotation
.end field

.field private final i:Ljavax/a/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/transaction/a;",
            ">;"
        }
    .end annotation
.end field

.field private final j:Ljavax/a/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/payments/send/data/p;",
            ">;"
        }
    .end annotation
.end field

.field private final k:Ljavax/a/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/common/a;",
            ">;"
        }
    .end annotation
.end field

.field private final l:Ljavax/a/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/common/accounts/b;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 17
    const-class v0, Lco/uk/getmondo/transaction/details/d;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lco/uk/getmondo/transaction/details/d;->a:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lb/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lb/a",
            "<",
            "Lco/uk/getmondo/transaction/details/b;",
            ">;",
            "Ljavax/a/a",
            "<",
            "Lio/reactivex/u;",
            ">;",
            "Ljavax/a/a",
            "<",
            "Lio/reactivex/u;",
            ">;",
            "Ljavax/a/a",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/transaction/a/a;",
            ">;",
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/background_sync/d;",
            ">;",
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/common/e/a;",
            ">;",
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/transaction/a;",
            ">;",
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/payments/send/data/p;",
            ">;",
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/common/a;",
            ">;",
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/common/accounts/b;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 57
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 58
    sget-boolean v0, Lco/uk/getmondo/transaction/details/d;->a:Z

    if-nez v0, :cond_0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 59
    :cond_0
    iput-object p1, p0, Lco/uk/getmondo/transaction/details/d;->b:Lb/a;

    .line 60
    sget-boolean v0, Lco/uk/getmondo/transaction/details/d;->a:Z

    if-nez v0, :cond_1

    if-nez p2, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 61
    :cond_1
    iput-object p2, p0, Lco/uk/getmondo/transaction/details/d;->c:Ljavax/a/a;

    .line 62
    sget-boolean v0, Lco/uk/getmondo/transaction/details/d;->a:Z

    if-nez v0, :cond_2

    if-nez p3, :cond_2

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 63
    :cond_2
    iput-object p3, p0, Lco/uk/getmondo/transaction/details/d;->d:Ljavax/a/a;

    .line 64
    sget-boolean v0, Lco/uk/getmondo/transaction/details/d;->a:Z

    if-nez v0, :cond_3

    if-nez p4, :cond_3

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 65
    :cond_3
    iput-object p4, p0, Lco/uk/getmondo/transaction/details/d;->e:Ljavax/a/a;

    .line 66
    sget-boolean v0, Lco/uk/getmondo/transaction/details/d;->a:Z

    if-nez v0, :cond_4

    if-nez p5, :cond_4

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 67
    :cond_4
    iput-object p5, p0, Lco/uk/getmondo/transaction/details/d;->f:Ljavax/a/a;

    .line 68
    sget-boolean v0, Lco/uk/getmondo/transaction/details/d;->a:Z

    if-nez v0, :cond_5

    if-nez p6, :cond_5

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 69
    :cond_5
    iput-object p6, p0, Lco/uk/getmondo/transaction/details/d;->g:Ljavax/a/a;

    .line 70
    sget-boolean v0, Lco/uk/getmondo/transaction/details/d;->a:Z

    if-nez v0, :cond_6

    if-nez p7, :cond_6

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 71
    :cond_6
    iput-object p7, p0, Lco/uk/getmondo/transaction/details/d;->h:Ljavax/a/a;

    .line 72
    sget-boolean v0, Lco/uk/getmondo/transaction/details/d;->a:Z

    if-nez v0, :cond_7

    if-nez p8, :cond_7

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 73
    :cond_7
    iput-object p8, p0, Lco/uk/getmondo/transaction/details/d;->i:Ljavax/a/a;

    .line 74
    sget-boolean v0, Lco/uk/getmondo/transaction/details/d;->a:Z

    if-nez v0, :cond_8

    if-nez p9, :cond_8

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 75
    :cond_8
    iput-object p9, p0, Lco/uk/getmondo/transaction/details/d;->j:Ljavax/a/a;

    .line 76
    sget-boolean v0, Lco/uk/getmondo/transaction/details/d;->a:Z

    if-nez v0, :cond_9

    if-nez p10, :cond_9

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 77
    :cond_9
    iput-object p10, p0, Lco/uk/getmondo/transaction/details/d;->k:Ljavax/a/a;

    .line 78
    sget-boolean v0, Lco/uk/getmondo/transaction/details/d;->a:Z

    if-nez v0, :cond_a

    if-nez p11, :cond_a

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 79
    :cond_a
    iput-object p11, p0, Lco/uk/getmondo/transaction/details/d;->l:Ljavax/a/a;

    .line 80
    return-void
.end method

.method public static a(Lb/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;)Lb/a/b;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lb/a",
            "<",
            "Lco/uk/getmondo/transaction/details/b;",
            ">;",
            "Ljavax/a/a",
            "<",
            "Lio/reactivex/u;",
            ">;",
            "Ljavax/a/a",
            "<",
            "Lio/reactivex/u;",
            ">;",
            "Ljavax/a/a",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/transaction/a/a;",
            ">;",
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/background_sync/d;",
            ">;",
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/common/e/a;",
            ">;",
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/transaction/a;",
            ">;",
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/payments/send/data/p;",
            ">;",
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/common/a;",
            ">;",
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/common/accounts/b;",
            ">;)",
            "Lb/a/b",
            "<",
            "Lco/uk/getmondo/transaction/details/b;",
            ">;"
        }
    .end annotation

    .prologue
    .line 111
    new-instance v0, Lco/uk/getmondo/transaction/details/d;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    invoke-direct/range {v0 .. v11}, Lco/uk/getmondo/transaction/details/d;-><init>(Lb/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;)V

    return-object v0
.end method


# virtual methods
.method public a()Lco/uk/getmondo/transaction/details/b;
    .locals 12

    .prologue
    .line 84
    iget-object v11, p0, Lco/uk/getmondo/transaction/details/d;->b:Lb/a;

    new-instance v0, Lco/uk/getmondo/transaction/details/b;

    iget-object v1, p0, Lco/uk/getmondo/transaction/details/d;->c:Ljavax/a/a;

    .line 87
    invoke-interface {v1}, Ljavax/a/a;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lio/reactivex/u;

    iget-object v2, p0, Lco/uk/getmondo/transaction/details/d;->d:Ljavax/a/a;

    .line 88
    invoke-interface {v2}, Ljavax/a/a;->b()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lio/reactivex/u;

    iget-object v3, p0, Lco/uk/getmondo/transaction/details/d;->e:Ljavax/a/a;

    .line 89
    invoke-interface {v3}, Ljavax/a/a;->b()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    iget-object v4, p0, Lco/uk/getmondo/transaction/details/d;->f:Ljavax/a/a;

    .line 90
    invoke-interface {v4}, Ljavax/a/a;->b()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lco/uk/getmondo/transaction/a/a;

    iget-object v5, p0, Lco/uk/getmondo/transaction/details/d;->g:Ljavax/a/a;

    .line 91
    invoke-interface {v5}, Ljavax/a/a;->b()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lco/uk/getmondo/background_sync/d;

    iget-object v6, p0, Lco/uk/getmondo/transaction/details/d;->h:Ljavax/a/a;

    .line 92
    invoke-interface {v6}, Ljavax/a/a;->b()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lco/uk/getmondo/common/e/a;

    iget-object v7, p0, Lco/uk/getmondo/transaction/details/d;->i:Ljavax/a/a;

    .line 93
    invoke-interface {v7}, Ljavax/a/a;->b()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lco/uk/getmondo/transaction/a;

    iget-object v8, p0, Lco/uk/getmondo/transaction/details/d;->j:Ljavax/a/a;

    .line 94
    invoke-interface {v8}, Ljavax/a/a;->b()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lco/uk/getmondo/payments/send/data/p;

    iget-object v9, p0, Lco/uk/getmondo/transaction/details/d;->k:Ljavax/a/a;

    .line 95
    invoke-interface {v9}, Ljavax/a/a;->b()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lco/uk/getmondo/common/a;

    iget-object v10, p0, Lco/uk/getmondo/transaction/details/d;->l:Ljavax/a/a;

    .line 96
    invoke-interface {v10}, Ljavax/a/a;->b()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lco/uk/getmondo/common/accounts/b;

    invoke-direct/range {v0 .. v10}, Lco/uk/getmondo/transaction/details/b;-><init>(Lio/reactivex/u;Lio/reactivex/u;Ljava/lang/String;Lco/uk/getmondo/transaction/a/a;Lco/uk/getmondo/background_sync/d;Lco/uk/getmondo/common/e/a;Lco/uk/getmondo/transaction/a;Lco/uk/getmondo/payments/send/data/p;Lco/uk/getmondo/common/a;Lco/uk/getmondo/common/accounts/b;)V

    .line 84
    invoke-static {v11, v0}, Lb/a/c;->a(Lb/a;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/transaction/details/b;

    return-object v0
.end method

.method public synthetic b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 17
    invoke-virtual {p0}, Lco/uk/getmondo/transaction/details/d;->a()Lco/uk/getmondo/transaction/details/b;

    move-result-object v0

    return-object v0
.end method
