.class final Lco/uk/getmondo/transaction/details/b$g;
.super Ljava/lang/Object;
.source "TransactionDetailsPresenter.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lco/uk/getmondo/transaction/details/b;->a(Lco/uk/getmondo/transaction/details/b$a;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h",
        "<TT;",
        "Lio/reactivex/z",
        "<+TR;>;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000\u0016\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0010\u0000\u001aV\u0012$\u0012\"\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u0004 \u0005*\u0010\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u0004\u0018\u00010\u00020\u0002 \u0005**\u0012$\u0012\"\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u0004 \u0005*\u0010\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u0004\u0018\u00010\u00020\u0002\u0018\u00010\u00010\u00012\u0006\u0010\u0006\u001a\u00020\u0003H\n\u00a2\u0006\u0002\u0008\u0007"
    }
    d2 = {
        "<anonymous>",
        "Lio/reactivex/Single;",
        "Lkotlin/Pair;",
        "Lco/uk/getmondo/model/Transaction;",
        "Lco/uk/getmondo/transaction/details/model/TransactionHistory;",
        "kotlin.jvm.PlatformType",
        "transaction",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field final synthetic a:Lco/uk/getmondo/transaction/details/b;


# direct methods
.method constructor <init>(Lco/uk/getmondo/transaction/details/b;)V
    .locals 0

    iput-object p1, p0, Lco/uk/getmondo/transaction/details/b$g;->a:Lco/uk/getmondo/transaction/details/b;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lco/uk/getmondo/d/aj;)Lio/reactivex/v;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lco/uk/getmondo/d/aj;",
            ")",
            "Lio/reactivex/v",
            "<",
            "Lkotlin/h",
            "<",
            "Lco/uk/getmondo/d/aj;",
            "Lco/uk/getmondo/transaction/details/c/f;",
            ">;>;"
        }
    .end annotation

    .prologue
    const-string v0, "transaction"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 66
    iget-object v0, p0, Lco/uk/getmondo/transaction/details/b$g;->a:Lco/uk/getmondo/transaction/details/b;

    invoke-static {v0}, Lco/uk/getmondo/transaction/details/b;->b(Lco/uk/getmondo/transaction/details/b;)Lco/uk/getmondo/transaction/a/a;

    move-result-object v0

    invoke-virtual {v0, p1}, Lco/uk/getmondo/transaction/a/a;->a(Lco/uk/getmondo/d/aj;)Lio/reactivex/v;

    move-result-object v1

    .line 67
    new-instance v0, Lco/uk/getmondo/transaction/details/b$g$1;

    invoke-direct {v0, p1}, Lco/uk/getmondo/transaction/details/b$g$1;-><init>(Lco/uk/getmondo/d/aj;)V

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {v1, v0}, Lio/reactivex/v;->d(Lio/reactivex/c/h;)Lio/reactivex/v;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 35
    check-cast p1, Lco/uk/getmondo/d/aj;

    invoke-virtual {p0, p1}, Lco/uk/getmondo/transaction/details/b$g;->a(Lco/uk/getmondo/d/aj;)Lio/reactivex/v;

    move-result-object v0

    return-object v0
.end method
