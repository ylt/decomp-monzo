.class public final Lco/uk/getmondo/transaction/details/a/g$b;
.super Ljava/lang/Object;
.source "SplitWithMonzoMe.kt"

# interfaces
.implements Lco/uk/getmondo/transaction/splitting/SplitBottomSheetFragment$a;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lco/uk/getmondo/transaction/details/a/g;->a(Landroid/support/v7/app/e;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000!\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0000*\u0001\u0000\u0008\n\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0010\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006H\u0016J\u0018\u0010\u0007\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0008\u001a\u00020\tH\u0016\u00a8\u0006\n"
    }
    d2 = {
        "co/uk/getmondo/transaction/details/actions/SplitWithMonzoMe$action$1",
        "Lco/uk/getmondo/transaction/splitting/SplitBottomSheetFragment$SplitListener;",
        "(Lco/uk/getmondo/transaction/details/actions/SplitWithMonzoMe;Landroid/support/v7/app/AppCompatActivity;)V",
        "onCustomiseAmount",
        "",
        "dialog",
        "Landroid/app/Dialog;",
        "onShareWith",
        "numberPeople",
        "",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field final synthetic a:Lco/uk/getmondo/transaction/details/a/g;

.field final synthetic b:Landroid/support/v7/app/e;


# direct methods
.method constructor <init>(Lco/uk/getmondo/transaction/details/a/g;Landroid/support/v7/app/e;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v7/app/e;",
            ")V"
        }
    .end annotation

    .prologue
    .line 49
    iput-object p1, p0, Lco/uk/getmondo/transaction/details/a/g$b;->a:Lco/uk/getmondo/transaction/details/a/g;

    iput-object p2, p0, Lco/uk/getmondo/transaction/details/a/g$b;->b:Landroid/support/v7/app/e;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroid/app/Dialog;)V
    .locals 4

    .prologue
    const-string v0, "dialog"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 61
    iget-object v0, p0, Lco/uk/getmondo/transaction/details/a/g$b;->a:Lco/uk/getmondo/transaction/details/a/g;

    invoke-static {v0}, Lco/uk/getmondo/transaction/details/a/g;->a(Lco/uk/getmondo/transaction/details/a/g;)Lco/uk/getmondo/common/a;

    move-result-object v0

    sget-object v1, Lco/uk/getmondo/api/model/tracking/Impression;->Companion:Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    invoke-virtual {v1}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->U()Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v1

    invoke-virtual {v0, v1}, Lco/uk/getmondo/common/a;->a(Lco/uk/getmondo/api/model/tracking/Impression;)V

    .line 63
    iget-object v0, p0, Lco/uk/getmondo/transaction/details/a/g$b;->a:Lco/uk/getmondo/transaction/details/a/g;

    invoke-static {v0}, Lco/uk/getmondo/transaction/details/a/g;->b(Lco/uk/getmondo/transaction/details/a/g;)Lco/uk/getmondo/d/aj;

    move-result-object v0

    invoke-virtual {v0}, Lco/uk/getmondo/d/aj;->f()Lco/uk/getmondo/d/u;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lco/uk/getmondo/d/u;->f()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    move-object v1, v0

    .line 64
    :goto_0
    iget-object v0, p0, Lco/uk/getmondo/transaction/details/a/g$b;->b:Landroid/support/v7/app/e;

    check-cast v0, Landroid/content/Context;

    const/4 v2, 0x0

    sget-object v3, Lco/uk/getmondo/api/model/tracking/Impression$CustomiseMonzoMeLinkFrom;->SPLIT_COST:Lco/uk/getmondo/api/model/tracking/Impression$CustomiseMonzoMeLinkFrom;

    invoke-static {v0, v2, v1, v3}, Lco/uk/getmondo/monzo/me/customise/CustomiseMonzoMeLinkActivity;->a(Landroid/content/Context;Lco/uk/getmondo/d/c;Ljava/lang/String;Lco/uk/getmondo/api/model/tracking/Impression$CustomiseMonzoMeLinkFrom;)V

    .line 65
    invoke-virtual {p1}, Landroid/app/Dialog;->dismiss()V

    .line 66
    return-void

    .line 63
    :cond_0
    const-string v0, ""

    move-object v1, v0

    goto :goto_0
.end method

.method public a(Landroid/app/Dialog;I)V
    .locals 4

    .prologue
    const-string v0, "dialog"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 51
    iget-object v0, p0, Lco/uk/getmondo/transaction/details/a/g$b;->a:Lco/uk/getmondo/transaction/details/a/g;

    invoke-static {v0}, Lco/uk/getmondo/transaction/details/a/g;->a(Lco/uk/getmondo/transaction/details/a/g;)Lco/uk/getmondo/common/a;

    move-result-object v0

    sget-object v1, Lco/uk/getmondo/api/model/tracking/Impression;->Companion:Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    invoke-virtual {v1, p2}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->c(I)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v1

    invoke-virtual {v0, v1}, Lco/uk/getmondo/common/a;->a(Lco/uk/getmondo/api/model/tracking/Impression;)V

    .line 54
    iget-object v0, p0, Lco/uk/getmondo/transaction/details/a/g$b;->a:Lco/uk/getmondo/transaction/details/a/g;

    invoke-static {v0}, Lco/uk/getmondo/transaction/details/a/g;->b(Lco/uk/getmondo/transaction/details/a/g;)Lco/uk/getmondo/d/aj;

    move-result-object v0

    invoke-virtual {v0}, Lco/uk/getmondo/d/aj;->g()Lco/uk/getmondo/d/c;

    move-result-object v0

    add-int/lit8 v1, p2, 0x1

    invoke-virtual {v0, v1}, Lco/uk/getmondo/d/c;->a(I)Lco/uk/getmondo/d/c;

    move-result-object v2

    .line 55
    iget-object v0, p0, Lco/uk/getmondo/transaction/details/a/g$b;->a:Lco/uk/getmondo/transaction/details/a/g;

    invoke-static {v0}, Lco/uk/getmondo/transaction/details/a/g;->b(Lco/uk/getmondo/transaction/details/a/g;)Lco/uk/getmondo/d/aj;

    move-result-object v0

    invoke-virtual {v0}, Lco/uk/getmondo/d/aj;->f()Lco/uk/getmondo/d/u;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lco/uk/getmondo/d/u;->f()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    move-object v1, v0

    .line 56
    :goto_0
    iget-object v0, p0, Lco/uk/getmondo/transaction/details/a/g$b;->b:Landroid/support/v7/app/e;

    check-cast v0, Landroid/content/Context;

    invoke-virtual {v2}, Lco/uk/getmondo/d/c;->j()Lco/uk/getmondo/d/c;

    move-result-object v2

    sget-object v3, Lco/uk/getmondo/api/model/tracking/Impression$CustomiseMonzoMeLinkFrom;->SPLIT_COST:Lco/uk/getmondo/api/model/tracking/Impression$CustomiseMonzoMeLinkFrom;

    invoke-static {v0, v2, v1, v3}, Lco/uk/getmondo/monzo/me/customise/CustomiseMonzoMeLinkActivity;->a(Landroid/content/Context;Lco/uk/getmondo/d/c;Ljava/lang/String;Lco/uk/getmondo/api/model/tracking/Impression$CustomiseMonzoMeLinkFrom;)V

    .line 57
    invoke-virtual {p1}, Landroid/app/Dialog;->dismiss()V

    .line 58
    return-void

    .line 55
    :cond_0
    const-string v0, ""

    move-object v1, v0

    goto :goto_0
.end method
