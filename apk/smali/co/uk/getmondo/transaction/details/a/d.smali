.class public final Lco/uk/getmondo/transaction/details/a/d;
.super Ljava/lang/Object;
.source "ManageDirectDebits.kt"

# interfaces
.implements Lco/uk/getmondo/transaction/details/b/a;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000*\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0010\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006H\u0016J\u0008\u0010\u0007\u001a\u00020\u0008H\u0016J\u0010\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\u000cH\u0016\u00a8\u0006\r"
    }
    d2 = {
        "Lco/uk/getmondo/transaction/details/actions/ManageDirectDebits;",
        "Lco/uk/getmondo/transaction/details/base/Action;",
        "()V",
        "action",
        "",
        "activity",
        "Landroid/support/v7/app/AppCompatActivity;",
        "placeholderIcon",
        "",
        "title",
        "",
        "resources",
        "Landroid/content/res/Resources;",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 16
    const v0, 0x7f020103

    return v0
.end method

.method public a(Landroid/content/res/Resources;)Ljava/lang/String;
    .locals 2

    .prologue
    const-string v0, "resources"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 12
    const v0, 0x7f0a0409

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "resources.getString(R.st\u2026tion_manage_direct_debit)"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public a(Landroid/support/v7/app/e;)V
    .locals 3

    .prologue
    const-string v0, "activity"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 20
    new-instance v1, Landroid/content/Intent;

    move-object v0, p1

    check-cast v0, Landroid/content/Context;

    const-class v2, Lco/uk/getmondo/payments/recurring_list/RecurringPaymentsActivity;

    invoke-direct {v1, v0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p1, v1}, Landroid/support/v7/app/e;->startActivity(Landroid/content/Intent;)V

    .line 21
    return-void
.end method

.method public b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 10
    invoke-static {p0}, Lco/uk/getmondo/transaction/details/b/a$a;->a(Lco/uk/getmondo/transaction/details/b/a;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public b(Landroid/content/res/Resources;)Ljava/lang/String;
    .locals 1

    .prologue
    const-string v0, "resources"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 10
    invoke-static {p0, p1}, Lco/uk/getmondo/transaction/details/b/a$a;->a(Lco/uk/getmondo/transaction/details/b/a;Landroid/content/res/Resources;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
