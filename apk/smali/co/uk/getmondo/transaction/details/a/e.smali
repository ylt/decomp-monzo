.class public final Lco/uk/getmondo/transaction/details/a/e;
.super Ljava/lang/Object;
.source "SendMoneyFasterPaymentAction.kt"

# interfaces
.implements Lco/uk/getmondo/transaction/details/b/a;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u00008\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0010\u0010\u0007\u001a\u00020\u00082\u0006\u0010\t\u001a\u00020\nH\u0016J\u0008\u0010\u000b\u001a\u00020\u000cH\u0016J\u0012\u0010\r\u001a\u0004\u0018\u00010\u000e2\u0006\u0010\u000f\u001a\u00020\u0010H\u0016J\u0010\u0010\u0011\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u0010H\u0016R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0012"
    }
    d2 = {
        "Lco/uk/getmondo/transaction/details/actions/SendMoneyFasterPaymentAction;",
        "Lco/uk/getmondo/transaction/details/base/Action;",
        "transaction",
        "Lco/uk/getmondo/model/Transaction;",
        "(Lco/uk/getmondo/model/Transaction;)V",
        "bankDetails",
        "Lco/uk/getmondo/payments/send/data/model/BankDetails;",
        "action",
        "",
        "activity",
        "Landroid/support/v7/app/AppCompatActivity;",
        "placeholderIcon",
        "",
        "subtitle",
        "",
        "resources",
        "Landroid/content/res/Resources;",
        "title",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field private final a:Lco/uk/getmondo/payments/send/data/a/a;


# direct methods
.method public constructor <init>(Lco/uk/getmondo/d/aj;)V
    .locals 1

    .prologue
    const-string v0, "transaction"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14
    invoke-virtual {p1}, Lco/uk/getmondo/d/aj;->C()Lco/uk/getmondo/payments/send/data/a/a;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-static {}, Lkotlin/d/b/l;->a()V

    :cond_0
    iput-object v0, p0, Lco/uk/getmondo/transaction/details/a/e;->a:Lco/uk/getmondo/payments/send/data/a/a;

    return-void
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 29
    const v0, 0x7f020104

    return v0
.end method

.method public a(Landroid/content/res/Resources;)Ljava/lang/String;
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    const-string v0, "resources"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 17
    iget-object v0, p0, Lco/uk/getmondo/transaction/details/a/e;->a:Lco/uk/getmondo/payments/send/data/a/a;

    invoke-virtual {v0}, Lco/uk/getmondo/payments/send/data/a/a;->a()Ljava/lang/String;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    if-eqz v0, :cond_0

    invoke-static {v0}, Lkotlin/h/j;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    move v0, v2

    :goto_0
    if-eqz v0, :cond_2

    .line 18
    const v0, 0x7f0a040d

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "resources.getString(R.st\u2026g.transaction_send_money)"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 21
    :goto_1
    return-object v0

    :cond_1
    move v0, v1

    .line 17
    goto :goto_0

    .line 21
    :cond_2
    const v0, 0x7f0a040e

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v3, p0, Lco/uk/getmondo/transaction/details/a/e;->a:Lco/uk/getmondo/payments/send/data/a/a;

    invoke-virtual {v3}, Lco/uk/getmondo/payments/send/data/a/a;->a()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lco/uk/getmondo/common/k/p;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v1

    invoke-virtual {p1, v0, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "resources.getString(R.st\u2026stWord(bankDetails.name))"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_1
.end method

.method public a(Landroid/support/v7/app/e;)V
    .locals 4

    .prologue
    const-string v0, "activity"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 33
    new-instance v1, Lco/uk/getmondo/payments/send/data/a/b;

    iget-object v0, p0, Lco/uk/getmondo/transaction/details/a/e;->a:Lco/uk/getmondo/payments/send/data/a/a;

    invoke-virtual {v0}, Lco/uk/getmondo/payments/send/data/a/a;->a()Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lco/uk/getmondo/transaction/details/a/e;->a:Lco/uk/getmondo/payments/send/data/a/a;

    invoke-virtual {v2}, Lco/uk/getmondo/payments/send/data/a/a;->b()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lco/uk/getmondo/transaction/details/a/e;->a:Lco/uk/getmondo/payments/send/data/a/a;

    invoke-virtual {v3}, Lco/uk/getmondo/payments/send/data/a/a;->c()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v0, v2, v3}, Lco/uk/getmondo/payments/send/data/a/b;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move-object v0, p1

    .line 34
    check-cast v0, Landroid/content/Context;

    invoke-static {v0, v1}, Lco/uk/getmondo/payments/send/bank/payment/BankPaymentDetailsActivity;->a(Landroid/content/Context;Lco/uk/getmondo/payments/send/data/a/b;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/support/v7/app/e;->startActivity(Landroid/content/Intent;)V

    .line 35
    return-void
.end method

.method public b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 13
    invoke-static {p0}, Lco/uk/getmondo/transaction/details/b/a$a;->a(Lco/uk/getmondo/transaction/details/b/a;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public b(Landroid/content/res/Resources;)Ljava/lang/String;
    .locals 2

    .prologue
    const-string v0, "resources"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 25
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lco/uk/getmondo/transaction/details/a/e;->a:Lco/uk/getmondo/payments/send/data/a/a;

    invoke-virtual {v1}, Lco/uk/getmondo/payments/send/data/a/a;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lco/uk/getmondo/common/k/o;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\u30fb"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/transaction/details/a/e;->a:Lco/uk/getmondo/payments/send/data/a/a;

    invoke-virtual {v1}, Lco/uk/getmondo/payments/send/data/a/a;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
