.class public final Lco/uk/getmondo/transaction/details/a/g;
.super Ljava/lang/Object;
.source "SplitWithMonzoMe.kt"

# interfaces
.implements Lco/uk/getmondo/transaction/details/b/a;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lco/uk/getmondo/transaction/details/a/g$a;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000D\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u0000 \u00152\u00020\u0001:\u0001\u0015B%\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u00a2\u0006\u0002\u0010\nJ\u0010\u0010\u000b\u001a\u00020\u000c2\u0006\u0010\r\u001a\u00020\u000eH\u0016J\u0008\u0010\u000f\u001a\u00020\u0010H\u0016J\u0010\u0010\u0011\u001a\u00020\u00122\u0006\u0010\u0013\u001a\u00020\u0014H\u0016R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0016"
    }
    d2 = {
        "Lco/uk/getmondo/transaction/details/actions/SplitWithMonzoMe;",
        "Lco/uk/getmondo/transaction/details/base/Action;",
        "transaction",
        "Lco/uk/getmondo/model/Transaction;",
        "costSplitter",
        "Lco/uk/getmondo/transaction/CostSplitter;",
        "userSettingsStorage",
        "Lco/uk/getmondo/payments/send/data/UserSettingsStorage;",
        "analyticsService",
        "Lco/uk/getmondo/common/AnalyticsService;",
        "(Lco/uk/getmondo/model/Transaction;Lco/uk/getmondo/transaction/CostSplitter;Lco/uk/getmondo/payments/send/data/UserSettingsStorage;Lco/uk/getmondo/common/AnalyticsService;)V",
        "action",
        "",
        "activity",
        "Landroid/support/v7/app/AppCompatActivity;",
        "placeholderIcon",
        "",
        "title",
        "",
        "resources",
        "Landroid/content/res/Resources;",
        "Companion",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# static fields
.field public static final a:Lco/uk/getmondo/transaction/details/a/g$a;


# instance fields
.field private final b:Lco/uk/getmondo/d/aj;

.field private final c:Lco/uk/getmondo/transaction/a;

.field private final d:Lco/uk/getmondo/payments/send/data/p;

.field private final e:Lco/uk/getmondo/common/a;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lco/uk/getmondo/transaction/details/a/g$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lco/uk/getmondo/transaction/details/a/g$a;-><init>(Lkotlin/d/b/i;)V

    sput-object v0, Lco/uk/getmondo/transaction/details/a/g;->a:Lco/uk/getmondo/transaction/details/a/g$a;

    return-void
.end method

.method public constructor <init>(Lco/uk/getmondo/d/aj;Lco/uk/getmondo/transaction/a;Lco/uk/getmondo/payments/send/data/p;Lco/uk/getmondo/common/a;)V
    .locals 1

    .prologue
    const-string v0, "transaction"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "costSplitter"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "userSettingsStorage"

    invoke-static {p3, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "analyticsService"

    invoke-static {p4, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lco/uk/getmondo/transaction/details/a/g;->b:Lco/uk/getmondo/d/aj;

    iput-object p2, p0, Lco/uk/getmondo/transaction/details/a/g;->c:Lco/uk/getmondo/transaction/a;

    iput-object p3, p0, Lco/uk/getmondo/transaction/details/a/g;->d:Lco/uk/getmondo/payments/send/data/p;

    iput-object p4, p0, Lco/uk/getmondo/transaction/details/a/g;->e:Lco/uk/getmondo/common/a;

    return-void
.end method

.method public static final synthetic a(Lco/uk/getmondo/transaction/details/a/g;)Lco/uk/getmondo/common/a;
    .locals 1

    .prologue
    .line 20
    iget-object v0, p0, Lco/uk/getmondo/transaction/details/a/g;->e:Lco/uk/getmondo/common/a;

    return-object v0
.end method

.method public static final synthetic b(Lco/uk/getmondo/transaction/details/a/g;)Lco/uk/getmondo/d/aj;
    .locals 1

    .prologue
    .line 20
    iget-object v0, p0, Lco/uk/getmondo/transaction/details/a/g;->b:Lco/uk/getmondo/d/aj;

    return-object v0
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 31
    const v0, 0x7f020105

    return v0
.end method

.method public a(Landroid/content/res/Resources;)Ljava/lang/String;
    .locals 2

    .prologue
    const-string v0, "resources"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 27
    const v0, 0x7f0a03b6

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "resources.getString(R.string.split_cost_action)"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public a(Landroid/support/v7/app/e;)V
    .locals 5

    .prologue
    const-string v0, "activity"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 35
    iget-object v0, p0, Lco/uk/getmondo/transaction/details/a/g;->e:Lco/uk/getmondo/common/a;

    sget-object v1, Lco/uk/getmondo/api/model/tracking/Impression;->Companion:Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    invoke-virtual {v1}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->T()Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v1

    invoke-virtual {v0, v1}, Lco/uk/getmondo/common/a;->a(Lco/uk/getmondo/api/model/tracking/Impression;)V

    .line 38
    iget-object v0, p0, Lco/uk/getmondo/transaction/details/a/g;->d:Lco/uk/getmondo/payments/send/data/p;

    invoke-virtual {v0}, Lco/uk/getmondo/payments/send/data/p;->a()Lco/uk/getmondo/payments/send/data/a/d;

    move-result-object v0

    .line 39
    sget-object v1, Lco/uk/getmondo/payments/send/data/a/d;->b:Lco/uk/getmondo/payments/send/data/a/d;

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    move-object v0, p1

    .line 40
    check-cast v0, Landroid/content/Context;

    sget-object v1, Lco/uk/getmondo/common/t;->c:Lco/uk/getmondo/common/t;

    invoke-static {v0, v1}, Lco/uk/getmondo/payments/send/onboarding/PeerToPeerIntroActivity;->a(Landroid/content/Context;Lco/uk/getmondo/common/t;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/support/v7/app/e;->startActivity(Landroid/content/Intent;)V

    .line 68
    :goto_0
    return-void

    .line 42
    :cond_0
    sget-object v1, Lco/uk/getmondo/payments/send/data/a/d;->c:Lco/uk/getmondo/payments/send/data/a/d;

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 43
    invoke-static {}, Lco/uk/getmondo/common/d/e;->a()Lco/uk/getmondo/common/d/e;

    move-result-object v0

    invoke-virtual {p1}, Landroid/support/v7/app/e;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const-string v2, "TAG_ERROR_P2P_BLOCKED"

    invoke-virtual {v0, v1, v2}, Lco/uk/getmondo/common/d/e;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_0

    .line 45
    :cond_1
    iget-object v0, p0, Lco/uk/getmondo/transaction/details/a/g;->c:Lco/uk/getmondo/transaction/a;

    iget-object v1, p0, Lco/uk/getmondo/transaction/details/a/g;->b:Lco/uk/getmondo/d/aj;

    const/4 v2, 0x0

    const/4 v3, 0x2

    const/4 v4, 0x0

    invoke-static {v0, v1, v2, v3, v4}, Lco/uk/getmondo/transaction/a;->a(Lco/uk/getmondo/transaction/a;Lco/uk/getmondo/d/aj;IILjava/lang/Object;)Lco/uk/getmondo/transaction/splitting/a/a;

    move-result-object v1

    new-instance v0, Lco/uk/getmondo/transaction/details/a/g$b;

    invoke-direct {v0, p0, p1}, Lco/uk/getmondo/transaction/details/a/g$b;-><init>(Lco/uk/getmondo/transaction/details/a/g;Landroid/support/v7/app/e;)V

    check-cast v0, Lco/uk/getmondo/transaction/splitting/SplitBottomSheetFragment$a;

    invoke-static {p1, v1, v0}, Lco/uk/getmondo/transaction/splitting/SplitBottomSheetFragment;->a(Landroid/support/v7/app/e;Lco/uk/getmondo/transaction/splitting/a/a;Lco/uk/getmondo/transaction/splitting/SplitBottomSheetFragment$a;)V

    goto :goto_0
.end method

.method public b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 20
    invoke-static {p0}, Lco/uk/getmondo/transaction/details/b/a$a;->a(Lco/uk/getmondo/transaction/details/b/a;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public b(Landroid/content/res/Resources;)Ljava/lang/String;
    .locals 1

    .prologue
    const-string v0, "resources"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 20
    invoke-static {p0, p1}, Lco/uk/getmondo/transaction/details/b/a$a;->a(Lco/uk/getmondo/transaction/details/b/a;Landroid/content/res/Resources;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
