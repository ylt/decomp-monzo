.class public final Lco/uk/getmondo/transaction/details/a/b;
.super Ljava/lang/Object;
.source "AddNoteAction.kt"

# interfaces
.implements Lco/uk/getmondo/transaction/details/b/a;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u00000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0010\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u0008H\u0016J\u0008\u0010\t\u001a\u00020\nH\u0016J\u0010\u0010\u000b\u001a\u00020\u000c2\u0006\u0010\r\u001a\u00020\u000eH\u0016R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000f"
    }
    d2 = {
        "Lco/uk/getmondo/transaction/details/actions/AddNoteAction;",
        "Lco/uk/getmondo/transaction/details/base/Action;",
        "transaction",
        "Lco/uk/getmondo/model/Transaction;",
        "(Lco/uk/getmondo/model/Transaction;)V",
        "action",
        "",
        "activity",
        "Landroid/support/v7/app/AppCompatActivity;",
        "placeholderIcon",
        "",
        "title",
        "",
        "resources",
        "Landroid/content/res/Resources;",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field private final a:Lco/uk/getmondo/d/aj;


# direct methods
.method public constructor <init>(Lco/uk/getmondo/d/aj;)V
    .locals 1

    .prologue
    const-string v0, "transaction"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lco/uk/getmondo/transaction/details/a/b;->a:Lco/uk/getmondo/d/aj;

    return-void
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 17
    const v0, 0x7f020101

    return v0
.end method

.method public a(Landroid/content/res/Resources;)Ljava/lang/String;
    .locals 2

    .prologue
    const-string v0, "resources"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 12
    iget-object v0, p0, Lco/uk/getmondo/transaction/details/a/b;->a:Lco/uk/getmondo/d/aj;

    invoke-virtual {v0}, Lco/uk/getmondo/d/aj;->e()Ljava/lang/String;

    move-result-object v1

    move-object v0, v1

    .line 13
    check-cast v0, Ljava/lang/CharSequence;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_2

    const v0, 0x7f0a0406

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const-string v0, "resources.getString(R.st\u2026nsaction_add_note_action)"

    invoke-static {v1, v0}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    :goto_1
    return-object v1

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :cond_2
    if-nez v1, :cond_3

    invoke-static {}, Lkotlin/d/b/l;->a()V

    :cond_3
    const-string v0, "notes!!"

    invoke-static {v1, v0}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_1
.end method

.method public a(Landroid/support/v7/app/e;)V
    .locals 1

    .prologue
    const-string v0, "activity"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 21
    check-cast p1, Landroid/content/Context;

    iget-object v0, p0, Lco/uk/getmondo/transaction/details/a/b;->a:Lco/uk/getmondo/d/aj;

    invoke-virtual {v0}, Lco/uk/getmondo/d/aj;->w()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lco/uk/getmondo/transaction/attachment/AttachmentActivity;->a(Landroid/content/Context;Ljava/lang/String;)V

    .line 22
    return-void
.end method

.method public b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 10
    invoke-static {p0}, Lco/uk/getmondo/transaction/details/b/a$a;->a(Lco/uk/getmondo/transaction/details/b/a;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public b(Landroid/content/res/Resources;)Ljava/lang/String;
    .locals 1

    .prologue
    const-string v0, "resources"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 10
    invoke-static {p0, p1}, Lco/uk/getmondo/transaction/details/b/a$a;->a(Lco/uk/getmondo/transaction/details/b/a;Landroid/content/res/Resources;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
