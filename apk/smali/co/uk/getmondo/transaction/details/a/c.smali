.class public final Lco/uk/getmondo/transaction/details/a/c;
.super Ljava/lang/Object;
.source "AddReceiptAction.kt"

# interfaces
.implements Lco/uk/getmondo/transaction/details/b/a;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u00004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0002\u0008\u0003\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0010\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\u000cH\u0016J\u0008\u0010\r\u001a\u00020\u000eH\u0016J\u0010\u0010\u000f\u001a\u00020\u00062\u0006\u0010\u0010\u001a\u00020\u0011H\u0016R\u0016\u0010\u0005\u001a\u0004\u0018\u00010\u00068VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0007\u0010\u0008R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0012"
    }
    d2 = {
        "Lco/uk/getmondo/transaction/details/actions/AddReceiptAction;",
        "Lco/uk/getmondo/transaction/details/base/Action;",
        "transaction",
        "Lco/uk/getmondo/model/Transaction;",
        "(Lco/uk/getmondo/model/Transaction;)V",
        "iconUrl",
        "",
        "getIconUrl",
        "()Ljava/lang/String;",
        "action",
        "",
        "activity",
        "Landroid/support/v7/app/AppCompatActivity;",
        "placeholderIcon",
        "",
        "title",
        "resources",
        "Landroid/content/res/Resources;",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field private final a:Lco/uk/getmondo/d/aj;


# direct methods
.method public constructor <init>(Lco/uk/getmondo/d/aj;)V
    .locals 1

    .prologue
    const-string v0, "transaction"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lco/uk/getmondo/transaction/details/a/c;->a:Lco/uk/getmondo/d/aj;

    return-void
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 16
    const v0, 0x7f020102

    return v0
.end method

.method public a(Landroid/content/res/Resources;)Ljava/lang/String;
    .locals 2

    .prologue
    const-string v0, "resources"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 12
    const v0, 0x7f0a0407

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "resources.getString(R.st\u2026ction_add_receipt_action)"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public a(Landroid/support/v7/app/e;)V
    .locals 1

    .prologue
    const-string v0, "activity"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 23
    check-cast p1, Landroid/content/Context;

    iget-object v0, p0, Lco/uk/getmondo/transaction/details/a/c;->a:Lco/uk/getmondo/d/aj;

    invoke-virtual {v0}, Lco/uk/getmondo/d/aj;->w()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lco/uk/getmondo/transaction/attachment/AttachmentActivity;->a(Landroid/content/Context;Ljava/lang/String;)V

    .line 24
    return-void
.end method

.method public b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 20
    iget-object v0, p0, Lco/uk/getmondo/transaction/details/a/c;->a:Lco/uk/getmondo/d/aj;

    invoke-virtual {v0}, Lco/uk/getmondo/d/aj;->F()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public b(Landroid/content/res/Resources;)Ljava/lang/String;
    .locals 1

    .prologue
    const-string v0, "resources"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 10
    invoke-static {p0, p1}, Lco/uk/getmondo/transaction/details/b/a$a;->a(Lco/uk/getmondo/transaction/details/b/a;Landroid/content/res/Resources;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
