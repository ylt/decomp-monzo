.class public final Lco/uk/getmondo/transaction/details/TransactionDetailsActivity;
.super Lco/uk/getmondo/common/activities/b;
.source "TransactionDetailsActivity.kt"

# interfaces
.implements Lco/uk/getmondo/transaction/details/b$a;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lco/uk/getmondo/transaction/details/TransactionDetailsActivity$a;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000F\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\r\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u0000 %2\u00020\u00012\u00020\u0002:\u0001%B\u0005\u00a2\u0006\u0002\u0010\u0003J\u0016\u0010\u000e\u001a\u0008\u0012\u0004\u0012\u00020\u00100\u000f2\u0006\u0010\u0011\u001a\u00020\u0012H\u0016J\u000e\u0010\u0013\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u000fH\u0016J\u0012\u0010\u0014\u001a\u00020\u00062\u0008\u0010\u0015\u001a\u0004\u0018\u00010\u0016H\u0014J\u0008\u0010\u0017\u001a\u00020\u0006H\u0014J\u0008\u0010\u0018\u001a\u00020\u0006H\u0016J\u000e\u0010\u0019\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0005H\u0016J\u0008\u0010\u001a\u001a\u00020\u0006H\u0014J\u0008\u0010\u001b\u001a\u00020\u0006H\u0014J\u0010\u0010\u001c\u001a\u00020\u00062\u0006\u0010\u001d\u001a\u00020\u0016H\u0014J\u0008\u0010\u001e\u001a\u00020\u0006H\u0014J\u0008\u0010\u001f\u001a\u00020\u0006H\u0014J\u0010\u0010 \u001a\u00020\u00062\u0006\u0010!\u001a\u00020\u0010H\u0016J\u0010\u0010\"\u001a\u00020\u00062\u0006\u0010#\u001a\u00020$H\u0016R2\u0010\u0004\u001a&\u0012\u000c\u0012\n \u0007*\u0004\u0018\u00010\u00060\u0006 \u0007*\u0012\u0012\u000c\u0012\n \u0007*\u0004\u0018\u00010\u00060\u0006\u0018\u00010\u00050\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001e\u0010\u0008\u001a\u00020\t8\u0006@\u0006X\u0087.\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\n\u0010\u000b\"\u0004\u0008\u000c\u0010\r\u00a8\u0006&"
    }
    d2 = {
        "Lco/uk/getmondo/transaction/details/TransactionDetailsActivity;",
        "Lco/uk/getmondo/common/activities/BaseActivity;",
        "Lco/uk/getmondo/transaction/details/TransactionDetailsPresenter$View;",
        "()V",
        "mapClickRelay",
        "Lcom/jakewharton/rxrelay2/PublishRelay;",
        "",
        "kotlin.jvm.PlatformType",
        "presenter",
        "Lco/uk/getmondo/transaction/details/TransactionDetailsPresenter;",
        "getPresenter",
        "()Lco/uk/getmondo/transaction/details/TransactionDetailsPresenter;",
        "setPresenter",
        "(Lco/uk/getmondo/transaction/details/TransactionDetailsPresenter;)V",
        "onCategoryChanged",
        "Lio/reactivex/Observable;",
        "Lco/uk/getmondo/model/Category;",
        "transactionId",
        "",
        "onChangeCategoryClicked",
        "onCreate",
        "savedInstanceState",
        "Landroid/os/Bundle;",
        "onDestroy",
        "onLowMemory",
        "onMapClicked",
        "onPause",
        "onResume",
        "onSaveInstanceState",
        "outState",
        "onStart",
        "onStop",
        "setCategory",
        "category",
        "setTransaction",
        "transactionViewModel",
        "Lco/uk/getmondo/transaction/details/base/TransactionViewModel;",
        "Companion",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# static fields
.field public static final b:Lco/uk/getmondo/transaction/details/TransactionDetailsActivity$a;


# instance fields
.field public a:Lco/uk/getmondo/transaction/details/b;

.field private final c:Lcom/b/b/c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/b/b/c",
            "<",
            "Lkotlin/n;",
            ">;"
        }
    .end annotation
.end field

.field private e:Ljava/util/HashMap;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lco/uk/getmondo/transaction/details/TransactionDetailsActivity$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lco/uk/getmondo/transaction/details/TransactionDetailsActivity$a;-><init>(Lkotlin/d/b/i;)V

    sput-object v0, Lco/uk/getmondo/transaction/details/TransactionDetailsActivity;->b:Lco/uk/getmondo/transaction/details/TransactionDetailsActivity$a;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 29
    invoke-direct {p0}, Lco/uk/getmondo/common/activities/b;-><init>()V

    .line 32
    invoke-static {}, Lcom/b/b/c;->a()Lcom/b/b/c;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/transaction/details/TransactionDetailsActivity;->c:Lcom/b/b/c;

    return-void
.end method

.method public static final a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;
    .locals 1

    const-string v0, "context"

    invoke-static {p0, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "transactionId"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v0, Lco/uk/getmondo/transaction/details/TransactionDetailsActivity;->b:Lco/uk/getmondo/transaction/details/TransactionDetailsActivity$a;

    invoke-virtual {v0, p0, p1}, Lco/uk/getmondo/transaction/details/TransactionDetailsActivity$a;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public a(I)Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lco/uk/getmondo/transaction/details/TransactionDetailsActivity;->e:Ljava/util/HashMap;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lco/uk/getmondo/transaction/details/TransactionDetailsActivity;->e:Ljava/util/HashMap;

    :cond_0
    iget-object v0, p0, Lco/uk/getmondo/transaction/details/TransactionDetailsActivity;->e:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    if-nez v0, :cond_1

    invoke-virtual {p0, p1}, Landroid/support/v4/app/j;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/transaction/details/TransactionDetailsActivity;->e:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    return-object v0
.end method

.method public a()Lio/reactivex/n;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Lkotlin/n;",
            ">;"
        }
    .end annotation

    .prologue
    .line 126
    sget v0, Lco/uk/getmondo/c$a;->transactionActionFab:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/transaction/details/TransactionDetailsActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/transaction/details/views/VisibilityAwareFloatingActionButton;

    .line 156
    invoke-static {v0}, Lcom/b/a/c/c;->a(Landroid/view/View;)Lio/reactivex/n;

    move-result-object v1

    sget-object v0, Lcom/b/a/a/d;->a:Lcom/b/a/a/d;

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {v1, v0}, Lio/reactivex/n;->map(Lio/reactivex/c/h;)Lio/reactivex/n;

    move-result-object v0

    const-string v1, "RxView.clicks(this).map(VoidToUnit)"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 126
    return-object v0
.end method

.method public a(Ljava/lang/String;)Lio/reactivex/n;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/n",
            "<",
            "Lco/uk/getmondo/d/h;",
            ">;"
        }
    .end annotation

    .prologue
    const-string v0, "transactionId"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 131
    new-instance v0, Lco/uk/getmondo/transaction/details/TransactionDetailsActivity$b;

    invoke-direct {v0, p0, p1}, Lco/uk/getmondo/transaction/details/TransactionDetailsActivity$b;-><init>(Lco/uk/getmondo/transaction/details/TransactionDetailsActivity;Ljava/lang/String;)V

    check-cast v0, Lio/reactivex/p;

    invoke-static {v0}, Lio/reactivex/n;->create(Lio/reactivex/p;)Lio/reactivex/n;

    move-result-object v0

    const-string v1, "Observable.create<Catego\u2026)\n            }\n        }"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public a(Lco/uk/getmondo/d/h;)V
    .locals 2

    .prologue
    const-string v0, "category"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 105
    sget v0, Lco/uk/getmondo/c$a;->transactionActionFab:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/transaction/details/TransactionDetailsActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/transaction/details/views/VisibilityAwareFloatingActionButton;

    invoke-virtual {p1}, Lco/uk/getmondo/d/h;->a()I

    move-result v1

    invoke-virtual {p0, v1}, Lco/uk/getmondo/transaction/details/TransactionDetailsActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Lco/uk/getmondo/transaction/details/views/VisibilityAwareFloatingActionButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 106
    sget v0, Lco/uk/getmondo/c$a;->transactionActionFab:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/transaction/details/TransactionDetailsActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/transaction/details/views/VisibilityAwareFloatingActionButton;

    invoke-virtual {p1}, Lco/uk/getmondo/d/h;->d()I

    move-result v1

    invoke-virtual {v0, v1}, Lco/uk/getmondo/transaction/details/views/VisibilityAwareFloatingActionButton;->setImageResource(I)V

    .line 107
    sget v0, Lco/uk/getmondo/c$a;->transactionActionFab:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/transaction/details/TransactionDetailsActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/transaction/details/views/VisibilityAwareFloatingActionButton;

    .line 108
    check-cast p0, Landroid/content/Context;

    invoke-virtual {p1}, Lco/uk/getmondo/d/h;->b()I

    move-result v1

    invoke-static {p0, v1}, Landroid/support/v4/content/a;->c(Landroid/content/Context;I)I

    move-result v1

    .line 107
    invoke-static {v1}, Landroid/content/res/ColorStateList;->valueOf(I)Landroid/content/res/ColorStateList;

    move-result-object v1

    invoke-virtual {v0, v1}, Lco/uk/getmondo/transaction/details/views/VisibilityAwareFloatingActionButton;->setBackgroundTintList(Landroid/content/res/ColorStateList;)V

    .line 109
    return-void
.end method

.method public a(Lco/uk/getmondo/transaction/details/b/l;)V
    .locals 2

    .prologue
    const-string v0, "transactionViewModel"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 112
    sget v0, Lco/uk/getmondo/c$a;->transactionActionFab:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/transaction/details/TransactionDetailsActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/transaction/details/views/VisibilityAwareFloatingActionButton;

    invoke-interface {p1}, Lco/uk/getmondo/transaction/details/b/l;->a()Lco/uk/getmondo/transaction/details/b/d;

    move-result-object v1

    invoke-virtual {v1}, Lco/uk/getmondo/transaction/details/b/d;->i()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x0

    :goto_0
    invoke-virtual {v0, v1}, Lco/uk/getmondo/transaction/details/views/VisibilityAwareFloatingActionButton;->setVisibility(I)V

    .line 113
    sget v0, Lco/uk/getmondo/c$a;->transactionAppBarLayout:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/transaction/details/TransactionDetailsActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/transaction/details/views/TransactionAppBarLayout;

    invoke-interface {p1}, Lco/uk/getmondo/transaction/details/b/l;->a()Lco/uk/getmondo/transaction/details/b/d;

    move-result-object v1

    invoke-virtual {v0, v1}, Lco/uk/getmondo/transaction/details/views/TransactionAppBarLayout;->a(Lco/uk/getmondo/transaction/details/b/d;)V

    .line 114
    sget v0, Lco/uk/getmondo/c$a;->transactionAvatarView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/transaction/details/TransactionDetailsActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/transaction/details/views/AvatarView;

    invoke-interface {p1}, Lco/uk/getmondo/transaction/details/b/l;->a()Lco/uk/getmondo/transaction/details/b/d;

    move-result-object v1

    invoke-virtual {v0, v1}, Lco/uk/getmondo/transaction/details/views/AvatarView;->a(Lco/uk/getmondo/transaction/details/b/d;)V

    .line 115
    sget v0, Lco/uk/getmondo/c$a;->transactionHeaderView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/transaction/details/TransactionDetailsActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/transaction/details/views/HeaderView;

    invoke-interface {p1}, Lco/uk/getmondo/transaction/details/b/l;->a()Lco/uk/getmondo/transaction/details/b/d;

    move-result-object v1

    invoke-virtual {v0, v1}, Lco/uk/getmondo/transaction/details/views/HeaderView;->a(Lco/uk/getmondo/transaction/details/b/d;)V

    .line 116
    sget v0, Lco/uk/getmondo/c$a;->transactionActionsView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/transaction/details/TransactionDetailsActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/transaction/details/views/TransactionActionsView;

    invoke-interface {p1}, Lco/uk/getmondo/transaction/details/b/l;->b()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lco/uk/getmondo/transaction/details/views/TransactionActionsView;->a(Ljava/util/List;)V

    .line 117
    sget v0, Lco/uk/getmondo/c$a;->transactionContentView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/transaction/details/TransactionDetailsActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/transaction/details/views/ContentView;

    invoke-interface {p1}, Lco/uk/getmondo/transaction/details/b/l;->c()Lco/uk/getmondo/transaction/details/b/e;

    move-result-object v1

    invoke-virtual {v0, v1}, Lco/uk/getmondo/transaction/details/views/ContentView;->a(Lco/uk/getmondo/transaction/details/b/e;)V

    .line 118
    invoke-interface {p1}, Lco/uk/getmondo/transaction/details/b/l;->d()Lco/uk/getmondo/transaction/details/b/f;

    move-result-object v1

    .line 119
    if-eqz v1, :cond_1

    .line 120
    sget v0, Lco/uk/getmondo/c$a;->transactionFooterView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/transaction/details/TransactionDetailsActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/transaction/details/views/FooterView;

    invoke-virtual {v0, v1}, Lco/uk/getmondo/transaction/details/views/FooterView;->a(Lco/uk/getmondo/transaction/details/b/f;)V

    .line 123
    :goto_1
    return-void

    .line 112
    :cond_0
    const/16 v1, 0x8

    goto :goto_0

    .line 122
    :cond_1
    sget v0, Lco/uk/getmondo/c$a;->transactionFooterView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/transaction/details/TransactionDetailsActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/transaction/details/views/FooterView;

    invoke-static {v0}, Lco/uk/getmondo/common/ae;->b(Landroid/view/View;)V

    goto :goto_1
.end method

.method public b()Lcom/b/b/c;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/b/b/c",
            "<",
            "Lkotlin/n;",
            ">;"
        }
    .end annotation

    .prologue
    .line 128
    iget-object v0, p0, Lco/uk/getmondo/transaction/details/TransactionDetailsActivity;->c:Lcom/b/b/c;

    const-string v1, "mapClickRelay"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public synthetic c()Lio/reactivex/n;
    .locals 1

    .prologue
    .line 29
    invoke-virtual {p0}, Lco/uk/getmondo/transaction/details/TransactionDetailsActivity;->b()Lcom/b/b/c;

    move-result-object v0

    check-cast v0, Lio/reactivex/n;

    return-object v0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 35
    invoke-super {p0, p1}, Lco/uk/getmondo/common/activities/b;->onCreate(Landroid/os/Bundle;)V

    .line 37
    const v0, 0x7f050070

    invoke-virtual {p0, v0}, Lco/uk/getmondo/transaction/details/TransactionDetailsActivity;->setContentView(I)V

    .line 39
    invoke-virtual {p0}, Lco/uk/getmondo/transaction/details/TransactionDetailsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "KEY_TRANSACTION_ID"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 40
    invoke-virtual {p0}, Lco/uk/getmondo/transaction/details/TransactionDetailsActivity;->l()Lco/uk/getmondo/common/h/b/b;

    move-result-object v0

    new-instance v2, Lco/uk/getmondo/transaction/d;

    invoke-direct {v2, v1}, Lco/uk/getmondo/transaction/d;-><init>(Ljava/lang/String;)V

    invoke-interface {v0, v2}, Lco/uk/getmondo/common/h/b/b;->a(Lco/uk/getmondo/transaction/d;)Lco/uk/getmondo/transaction/c;

    move-result-object v0

    .line 41
    invoke-interface {v0, p0}, Lco/uk/getmondo/transaction/c;->a(Lco/uk/getmondo/transaction/details/TransactionDetailsActivity;)V

    .line 43
    invoke-virtual {p0}, Lco/uk/getmondo/transaction/details/TransactionDetailsActivity;->r()Landroid/support/v7/widget/Toolbar;

    move-result-object v0

    invoke-virtual {p0, v0}, Lco/uk/getmondo/transaction/details/TransactionDetailsActivity;->setSupportActionBar(Landroid/support/v7/widget/Toolbar;)V

    .line 45
    const-string v0, "notification"

    invoke-virtual {p0, v0}, Lco/uk/getmondo/transaction/details/TransactionDetailsActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type android.app.NotificationManager"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    check-cast v0, Landroid/app/NotificationManager;

    .line 46
    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/app/NotificationManager;->cancel(I)V

    .line 48
    iget-object v1, p0, Lco/uk/getmondo/transaction/details/TransactionDetailsActivity;->a:Lco/uk/getmondo/transaction/details/b;

    if-nez v1, :cond_1

    const-string v0, "presenter"

    invoke-static {v0}, Lkotlin/d/b/l;->b(Ljava/lang/String;)V

    :cond_1
    move-object v0, p0

    check-cast v0, Lco/uk/getmondo/transaction/details/b$a;

    invoke-virtual {v1, v0}, Lco/uk/getmondo/transaction/details/b;->a(Lco/uk/getmondo/transaction/details/b$a;)V

    .line 50
    sget v0, Lco/uk/getmondo/c$a;->transactionAvatarView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/transaction/details/TransactionDetailsActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/transaction/details/views/AvatarView;

    new-instance v1, Lco/uk/getmondo/transaction/details/TransactionDetailsActivity$c;

    invoke-direct {v1, p0}, Lco/uk/getmondo/transaction/details/TransactionDetailsActivity$c;-><init>(Lco/uk/getmondo/transaction/details/TransactionDetailsActivity;)V

    check-cast v1, Lkotlin/d/a/m;

    invoke-virtual {v0, v1}, Lco/uk/getmondo/transaction/details/views/AvatarView;->setBackgroundListener(Lkotlin/d/a/m;)V

    .line 55
    sget v0, Lco/uk/getmondo/c$a;->transactionAppBarLayout:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/transaction/details/TransactionDetailsActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/transaction/details/views/TransactionAppBarLayout;

    new-instance v1, Lco/uk/getmondo/transaction/details/TransactionDetailsActivity$d;

    iget-object v2, p0, Lco/uk/getmondo/transaction/details/TransactionDetailsActivity;->c:Lcom/b/b/c;

    invoke-direct {v1, v2}, Lco/uk/getmondo/transaction/details/TransactionDetailsActivity$d;-><init>(Lcom/b/b/c;)V

    check-cast v1, Lkotlin/d/a/b;

    invoke-virtual {v0, v1}, Lco/uk/getmondo/transaction/details/views/TransactionAppBarLayout;->setMapListener(Lkotlin/d/a/b;)V

    .line 56
    sget v0, Lco/uk/getmondo/c$a;->transactionAppBarLayout:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/transaction/details/TransactionDetailsActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/transaction/details/views/TransactionAppBarLayout;

    invoke-virtual {v0, p1}, Lco/uk/getmondo/transaction/details/views/TransactionAppBarLayout;->a(Landroid/os/Bundle;)V

    .line 58
    sget v0, Lco/uk/getmondo/c$a;->mainScrollContent:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/transaction/details/TransactionDetailsActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v4/widget/NestedScrollView;

    new-instance v1, Lco/uk/getmondo/transaction/details/TransactionDetailsActivity$e;

    invoke-direct {v1, p0}, Lco/uk/getmondo/transaction/details/TransactionDetailsActivity$e;-><init>(Lco/uk/getmondo/transaction/details/TransactionDetailsActivity;)V

    check-cast v1, Landroid/support/v4/widget/NestedScrollView$b;

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/NestedScrollView;->setOnScrollChangeListener(Landroid/support/v4/widget/NestedScrollView$b;)V

    .line 66
    return-void
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    .line 94
    iget-object v0, p0, Lco/uk/getmondo/transaction/details/TransactionDetailsActivity;->a:Lco/uk/getmondo/transaction/details/b;

    if-nez v0, :cond_0

    const-string v1, "presenter"

    invoke-static {v1}, Lkotlin/d/b/l;->b(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0}, Lco/uk/getmondo/transaction/details/b;->b()V

    .line 95
    sget v0, Lco/uk/getmondo/c$a;->transactionAppBarLayout:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/transaction/details/TransactionDetailsActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/transaction/details/views/TransactionAppBarLayout;

    invoke-virtual {v0}, Lco/uk/getmondo/transaction/details/views/TransactionAppBarLayout;->k()V

    .line 96
    invoke-super {p0}, Lco/uk/getmondo/common/activities/b;->onDestroy()V

    .line 97
    return-void
.end method

.method public onLowMemory()V
    .locals 1

    .prologue
    .line 100
    invoke-super {p0}, Lco/uk/getmondo/common/activities/b;->onLowMemory()V

    .line 101
    sget v0, Lco/uk/getmondo/c$a;->transactionAppBarLayout:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/transaction/details/TransactionDetailsActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/transaction/details/views/TransactionAppBarLayout;

    invoke-virtual {v0}, Lco/uk/getmondo/transaction/details/views/TransactionAppBarLayout;->l()V

    .line 102
    return-void
.end method

.method protected onPause()V
    .locals 1

    .prologue
    .line 84
    sget v0, Lco/uk/getmondo/c$a;->transactionAppBarLayout:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/transaction/details/TransactionDetailsActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/transaction/details/views/TransactionAppBarLayout;

    invoke-virtual {v0}, Lco/uk/getmondo/transaction/details/views/TransactionAppBarLayout;->i()V

    .line 85
    invoke-super {p0}, Lco/uk/getmondo/common/activities/b;->onPause()V

    .line 86
    return-void
.end method

.method protected onResume()V
    .locals 1

    .prologue
    .line 79
    invoke-super {p0}, Lco/uk/getmondo/common/activities/b;->onResume()V

    .line 80
    sget v0, Lco/uk/getmondo/c$a;->transactionAppBarLayout:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/transaction/details/TransactionDetailsActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/transaction/details/views/TransactionAppBarLayout;

    invoke-virtual {v0}, Lco/uk/getmondo/transaction/details/views/TransactionAppBarLayout;->h()V

    .line 81
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    const-string v0, "outState"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 69
    invoke-super {p0, p1}, Lco/uk/getmondo/common/activities/b;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 70
    sget v0, Lco/uk/getmondo/c$a;->transactionAppBarLayout:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/transaction/details/TransactionDetailsActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/transaction/details/views/TransactionAppBarLayout;

    invoke-virtual {v0, p1}, Lco/uk/getmondo/transaction/details/views/TransactionAppBarLayout;->b(Landroid/os/Bundle;)V

    .line 71
    return-void
.end method

.method protected onStart()V
    .locals 1

    .prologue
    .line 74
    invoke-super {p0}, Lco/uk/getmondo/common/activities/b;->onStart()V

    .line 75
    sget v0, Lco/uk/getmondo/c$a;->transactionAppBarLayout:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/transaction/details/TransactionDetailsActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/transaction/details/views/TransactionAppBarLayout;

    invoke-virtual {v0}, Lco/uk/getmondo/transaction/details/views/TransactionAppBarLayout;->g()V

    .line 76
    return-void
.end method

.method protected onStop()V
    .locals 1

    .prologue
    .line 89
    sget v0, Lco/uk/getmondo/c$a;->transactionAppBarLayout:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/transaction/details/TransactionDetailsActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/transaction/details/views/TransactionAppBarLayout;

    invoke-virtual {v0}, Lco/uk/getmondo/transaction/details/views/TransactionAppBarLayout;->j()V

    .line 90
    invoke-super {p0}, Lco/uk/getmondo/common/activities/b;->onStop()V

    .line 91
    return-void
.end method
