.class public final Lco/uk/getmondo/transaction/details/b;
.super Lco/uk/getmondo/common/ui/b;
.source "TransactionDetailsPresenter.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lco/uk/getmondo/transaction/details/b$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lco/uk/getmondo/common/ui/b",
        "<",
        "Lco/uk/getmondo/transaction/details/b$a;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000P\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u0003\u0008\u0007\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00020\u0001:\u0001\u001aB[\u0008\u0007\u0012\u0008\u0008\u0001\u0010\u0003\u001a\u00020\u0004\u0012\u0008\u0008\u0001\u0010\u0005\u001a\u00020\u0004\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b\u0012\u0006\u0010\u000c\u001a\u00020\r\u0012\u0006\u0010\u000e\u001a\u00020\u000f\u0012\u0006\u0010\u0010\u001a\u00020\u0011\u0012\u0006\u0010\u0012\u001a\u00020\u0013\u0012\u0006\u0010\u0014\u001a\u00020\u0015\u00a2\u0006\u0002\u0010\u0016J\u0010\u0010\u0017\u001a\u00020\u00182\u0006\u0010\u0019\u001a\u00020\u0002H\u0016R\u000e\u0010\u0014\u001a\u00020\u0015X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0012\u001a\u00020\u0013X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000c\u001a\u00020\rX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u000fX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0010\u001a\u00020\u0011X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001b"
    }
    d2 = {
        "Lco/uk/getmondo/transaction/details/TransactionDetailsPresenter;",
        "Lco/uk/getmondo/common/ui/BasePresenter;",
        "Lco/uk/getmondo/transaction/details/TransactionDetailsPresenter$View;",
        "uiScheduler",
        "Lio/reactivex/Scheduler;",
        "ioScheduler",
        "transactionId",
        "",
        "transactionManager",
        "Lco/uk/getmondo/transaction/data/TransactionManager;",
        "syncManager",
        "Lco/uk/getmondo/background_sync/SyncManager;",
        "apiErrorHandler",
        "Lco/uk/getmondo/common/errors/ApiErrorHandler;",
        "costSplitter",
        "Lco/uk/getmondo/transaction/CostSplitter;",
        "userSettingsStorage",
        "Lco/uk/getmondo/payments/send/data/UserSettingsStorage;",
        "analyticsService",
        "Lco/uk/getmondo/common/AnalyticsService;",
        "accountManager",
        "Lco/uk/getmondo/common/accounts/AccountManager;",
        "(Lio/reactivex/Scheduler;Lio/reactivex/Scheduler;Ljava/lang/String;Lco/uk/getmondo/transaction/data/TransactionManager;Lco/uk/getmondo/background_sync/SyncManager;Lco/uk/getmondo/common/errors/ApiErrorHandler;Lco/uk/getmondo/transaction/CostSplitter;Lco/uk/getmondo/payments/send/data/UserSettingsStorage;Lco/uk/getmondo/common/AnalyticsService;Lco/uk/getmondo/common/accounts/AccountManager;)V",
        "register",
        "",
        "view",
        "View",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field private final c:Lio/reactivex/u;

.field private final d:Lio/reactivex/u;

.field private final e:Ljava/lang/String;

.field private final f:Lco/uk/getmondo/transaction/a/a;

.field private final g:Lco/uk/getmondo/background_sync/d;

.field private final h:Lco/uk/getmondo/common/e/a;

.field private final i:Lco/uk/getmondo/transaction/a;

.field private final j:Lco/uk/getmondo/payments/send/data/p;

.field private final k:Lco/uk/getmondo/common/a;

.field private final l:Lco/uk/getmondo/common/accounts/b;


# direct methods
.method public constructor <init>(Lio/reactivex/u;Lio/reactivex/u;Ljava/lang/String;Lco/uk/getmondo/transaction/a/a;Lco/uk/getmondo/background_sync/d;Lco/uk/getmondo/common/e/a;Lco/uk/getmondo/transaction/a;Lco/uk/getmondo/payments/send/data/p;Lco/uk/getmondo/common/a;Lco/uk/getmondo/common/accounts/b;)V
    .locals 1

    .prologue
    const-string v0, "uiScheduler"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "ioScheduler"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "transactionId"

    invoke-static {p3, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "transactionManager"

    invoke-static {p4, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "syncManager"

    invoke-static {p5, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "apiErrorHandler"

    invoke-static {p6, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "costSplitter"

    invoke-static {p7, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "userSettingsStorage"

    invoke-static {p8, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "analyticsService"

    invoke-static {p9, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "accountManager"

    invoke-static {p10, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 47
    invoke-direct {p0}, Lco/uk/getmondo/common/ui/b;-><init>()V

    iput-object p1, p0, Lco/uk/getmondo/transaction/details/b;->c:Lio/reactivex/u;

    iput-object p2, p0, Lco/uk/getmondo/transaction/details/b;->d:Lio/reactivex/u;

    iput-object p3, p0, Lco/uk/getmondo/transaction/details/b;->e:Ljava/lang/String;

    iput-object p4, p0, Lco/uk/getmondo/transaction/details/b;->f:Lco/uk/getmondo/transaction/a/a;

    iput-object p5, p0, Lco/uk/getmondo/transaction/details/b;->g:Lco/uk/getmondo/background_sync/d;

    iput-object p6, p0, Lco/uk/getmondo/transaction/details/b;->h:Lco/uk/getmondo/common/e/a;

    iput-object p7, p0, Lco/uk/getmondo/transaction/details/b;->i:Lco/uk/getmondo/transaction/a;

    iput-object p8, p0, Lco/uk/getmondo/transaction/details/b;->j:Lco/uk/getmondo/payments/send/data/p;

    iput-object p9, p0, Lco/uk/getmondo/transaction/details/b;->k:Lco/uk/getmondo/common/a;

    iput-object p10, p0, Lco/uk/getmondo/transaction/details/b;->l:Lco/uk/getmondo/common/accounts/b;

    return-void
.end method

.method public static final synthetic a(Lco/uk/getmondo/transaction/details/b;)Lco/uk/getmondo/common/e/a;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lco/uk/getmondo/transaction/details/b;->h:Lco/uk/getmondo/common/e/a;

    return-object v0
.end method

.method public static final synthetic b(Lco/uk/getmondo/transaction/details/b;)Lco/uk/getmondo/transaction/a/a;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lco/uk/getmondo/transaction/details/b;->f:Lco/uk/getmondo/transaction/a/a;

    return-object v0
.end method

.method public static final synthetic c(Lco/uk/getmondo/transaction/details/b;)Lco/uk/getmondo/common/accounts/b;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lco/uk/getmondo/transaction/details/b;->l:Lco/uk/getmondo/common/accounts/b;

    return-object v0
.end method

.method public static final synthetic d(Lco/uk/getmondo/transaction/details/b;)Lco/uk/getmondo/transaction/a;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lco/uk/getmondo/transaction/details/b;->i:Lco/uk/getmondo/transaction/a;

    return-object v0
.end method

.method public static final synthetic e(Lco/uk/getmondo/transaction/details/b;)Lco/uk/getmondo/payments/send/data/p;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lco/uk/getmondo/transaction/details/b;->j:Lco/uk/getmondo/payments/send/data/p;

    return-object v0
.end method

.method public static final synthetic f(Lco/uk/getmondo/transaction/details/b;)Lco/uk/getmondo/common/a;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lco/uk/getmondo/transaction/details/b;->k:Lco/uk/getmondo/common/a;

    return-object v0
.end method

.method public static final synthetic g(Lco/uk/getmondo/transaction/details/b;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lco/uk/getmondo/transaction/details/b;->e:Ljava/lang/String;

    return-object v0
.end method

.method public static final synthetic h(Lco/uk/getmondo/transaction/details/b;)Lio/reactivex/u;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lco/uk/getmondo/transaction/details/b;->d:Lio/reactivex/u;

    return-object v0
.end method

.method public static final synthetic i(Lco/uk/getmondo/transaction/details/b;)Lio/reactivex/u;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lco/uk/getmondo/transaction/details/b;->c:Lio/reactivex/u;

    return-object v0
.end method


# virtual methods
.method public bridge synthetic a(Lco/uk/getmondo/common/ui/f;)V
    .locals 0

    .prologue
    .line 35
    check-cast p1, Lco/uk/getmondo/transaction/details/b$a;

    invoke-virtual {p0, p1}, Lco/uk/getmondo/transaction/details/b;->a(Lco/uk/getmondo/transaction/details/b$a;)V

    return-void
.end method

.method public a(Lco/uk/getmondo/transaction/details/b$a;)V
    .locals 6

    .prologue
    const-string v0, "view"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v0, p1

    .line 50
    check-cast v0, Lco/uk/getmondo/common/ui/f;

    invoke-super {p0, v0}, Lco/uk/getmondo/common/ui/b;->a(Lco/uk/getmondo/common/ui/f;)V

    .line 52
    iget-object v0, p0, Lco/uk/getmondo/transaction/details/b;->f:Lco/uk/getmondo/transaction/a/a;

    iget-object v1, p0, Lco/uk/getmondo/transaction/details/b;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lco/uk/getmondo/transaction/a/a;->a(Ljava/lang/String;)Lco/uk/getmondo/d/aj;

    move-result-object v0

    if-nez v0, :cond_0

    .line 54
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Transaction missing, syncing"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    invoke-static {v0}, Ld/a/a;->a(Ljava/lang/Throwable;)V

    .line 57
    iget-object v0, p0, Lco/uk/getmondo/transaction/details/b;->g:Lco/uk/getmondo/background_sync/d;

    invoke-virtual {v0}, Lco/uk/getmondo/background_sync/d;->a()Lio/reactivex/b;

    move-result-object v0

    .line 56
    iget-object v1, p0, Lco/uk/getmondo/transaction/details/b;->d:Lio/reactivex/u;

    invoke-virtual {v0, v1}, Lio/reactivex/b;->b(Lio/reactivex/u;)Lio/reactivex/b;

    move-result-object v2

    .line 57
    sget-object v0, Lco/uk/getmondo/transaction/details/b$b;->a:Lco/uk/getmondo/transaction/details/b$b;

    check-cast v0, Lio/reactivex/c/a;

    .line 58
    new-instance v1, Lco/uk/getmondo/transaction/details/b$f;

    invoke-direct {v1, p0, p1}, Lco/uk/getmondo/transaction/details/b$f;-><init>(Lco/uk/getmondo/transaction/details/b;Lco/uk/getmondo/transaction/details/b$a;)V

    check-cast v1, Lio/reactivex/c/g;

    .line 57
    invoke-virtual {v2, v0, v1}, Lio/reactivex/b;->a(Lio/reactivex/c/a;Lio/reactivex/c/g;)Lio/reactivex/b/b;

    move-result-object v0

    .line 55
    invoke-virtual {p0, v0}, Lco/uk/getmondo/transaction/details/b;->a(Lio/reactivex/b/b;)V

    .line 61
    :cond_0
    iget-object v0, p0, Lco/uk/getmondo/transaction/details/b;->k:Lco/uk/getmondo/common/a;

    sget-object v1, Lco/uk/getmondo/api/model/tracking/Impression;->Companion:Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    iget-object v2, p0, Lco/uk/getmondo/transaction/details/b;->e:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->f(Ljava/lang/String;)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v1

    invoke-virtual {v0, v1}, Lco/uk/getmondo/common/a;->a(Lco/uk/getmondo/api/model/tracking/Impression;)V

    .line 63
    iget-object v0, p0, Lco/uk/getmondo/transaction/details/b;->f:Lco/uk/getmondo/transaction/a/a;

    iget-object v1, p0, Lco/uk/getmondo/transaction/details/b;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lco/uk/getmondo/transaction/a/a;->b(Ljava/lang/String;)Lio/reactivex/n;

    move-result-object v2

    .line 64
    iget-object v4, p0, Lco/uk/getmondo/transaction/details/b;->b:Lio/reactivex/b/a;

    .line 65
    new-instance v0, Lco/uk/getmondo/transaction/details/b$g;

    invoke-direct {v0, p0}, Lco/uk/getmondo/transaction/details/b$g;-><init>(Lco/uk/getmondo/transaction/details/b;)V

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {v2, v0}, Lio/reactivex/n;->switchMapSingle(Lio/reactivex/c/h;)Lio/reactivex/n;

    move-result-object v0

    .line 69
    iget-object v1, p0, Lco/uk/getmondo/transaction/details/b;->c:Lio/reactivex/u;

    invoke-virtual {v0, v1}, Lio/reactivex/n;->observeOn(Lio/reactivex/u;)Lio/reactivex/n;

    move-result-object v5

    .line 70
    new-instance v0, Lco/uk/getmondo/transaction/details/b$h;

    invoke-direct {v0, p0, p1}, Lco/uk/getmondo/transaction/details/b$h;-><init>(Lco/uk/getmondo/transaction/details/b;Lco/uk/getmondo/transaction/details/b$a;)V

    check-cast v0, Lio/reactivex/c/g;

    .line 89
    sget-object v1, Lco/uk/getmondo/transaction/details/b$i;->a:Lco/uk/getmondo/transaction/details/b$i;

    check-cast v1, Lkotlin/d/a/b;

    if-eqz v1, :cond_1

    new-instance v3, Lco/uk/getmondo/transaction/details/c;

    invoke-direct {v3, v1}, Lco/uk/getmondo/transaction/details/c;-><init>(Lkotlin/d/a/b;)V

    move-object v1, v3

    :cond_1
    check-cast v1, Lio/reactivex/c/g;

    .line 70
    invoke-virtual {v5, v0, v1}, Lio/reactivex/n;->subscribe(Lio/reactivex/c/g;Lio/reactivex/c/g;)Lio/reactivex/b/b;

    move-result-object v0

    const-string v1, "transactionObservable\n  \u2026            }, Timber::e)"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 64
    invoke-static {v4, v0}, Lco/uk/getmondo/common/j/f;->a(Lio/reactivex/b/a;Lio/reactivex/b/b;)Lio/reactivex/b/a;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/transaction/details/b;->b:Lio/reactivex/b/a;

    .line 91
    iget-object v3, p0, Lco/uk/getmondo/transaction/details/b;->b:Lio/reactivex/b/a;

    .line 105
    invoke-interface {p1}, Lco/uk/getmondo/transaction/details/b$a;->a()Lio/reactivex/n;

    move-result-object v1

    .line 92
    new-instance v0, Lco/uk/getmondo/transaction/details/b$j;

    invoke-direct {v0, p0, p1}, Lco/uk/getmondo/transaction/details/b$j;-><init>(Lco/uk/getmondo/transaction/details/b;Lco/uk/getmondo/transaction/details/b$a;)V

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {v1, v0}, Lio/reactivex/n;->switchMap(Lio/reactivex/c/h;)Lio/reactivex/n;

    move-result-object v1

    .line 93
    const-string v0, "transactionObservable"

    invoke-static {v2, v0}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v0, v2

    check-cast v0, Lio/reactivex/r;

    invoke-static {v1, v0}, Lco/uk/getmondo/common/j/f;->a(Lio/reactivex/n;Lio/reactivex/r;)Lio/reactivex/n;

    move-result-object v1

    .line 94
    new-instance v0, Lco/uk/getmondo/transaction/details/b$k;

    invoke-direct {v0, p0}, Lco/uk/getmondo/transaction/details/b$k;-><init>(Lco/uk/getmondo/transaction/details/b;)V

    check-cast v0, Lio/reactivex/c/g;

    invoke-virtual {v1, v0}, Lio/reactivex/n;->doOnNext(Lio/reactivex/c/g;)Lio/reactivex/n;

    move-result-object v1

    .line 97
    new-instance v0, Lco/uk/getmondo/transaction/details/b$l;

    invoke-direct {v0, p0, p1}, Lco/uk/getmondo/transaction/details/b$l;-><init>(Lco/uk/getmondo/transaction/details/b;Lco/uk/getmondo/transaction/details/b$a;)V

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {v1, v0}, Lio/reactivex/n;->flatMapCompletable(Lio/reactivex/c/h;)Lio/reactivex/b;

    move-result-object v4

    .line 105
    sget-object v0, Lco/uk/getmondo/transaction/details/b$m;->a:Lco/uk/getmondo/transaction/details/b$m;

    check-cast v0, Lio/reactivex/c/a;

    sget-object v1, Lco/uk/getmondo/transaction/details/b$c;->a:Lco/uk/getmondo/transaction/details/b$c;

    check-cast v1, Lkotlin/d/a/b;

    if-eqz v1, :cond_2

    new-instance v2, Lco/uk/getmondo/transaction/details/c;

    invoke-direct {v2, v1}, Lco/uk/getmondo/transaction/details/c;-><init>(Lkotlin/d/a/b;)V

    move-object v1, v2

    :cond_2
    check-cast v1, Lio/reactivex/c/g;

    invoke-virtual {v4, v0, v1}, Lio/reactivex/b;->a(Lio/reactivex/c/a;Lio/reactivex/c/g;)Lio/reactivex/b/b;

    move-result-object v0

    const-string v1, "view.onChangeCategoryCli\u2026.subscribe({}, Timber::e)"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 91
    invoke-static {v3, v0}, Lco/uk/getmondo/common/j/f;->a(Lio/reactivex/b/a;Lio/reactivex/b/b;)Lio/reactivex/b/a;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/transaction/details/b;->b:Lio/reactivex/b/a;

    .line 107
    iget-object v3, p0, Lco/uk/getmondo/transaction/details/b;->b:Lio/reactivex/b/a;

    .line 108
    invoke-interface {p1}, Lco/uk/getmondo/transaction/details/b$a;->c()Lio/reactivex/n;

    move-result-object v4

    new-instance v0, Lco/uk/getmondo/transaction/details/b$d;

    invoke-direct {v0, p0}, Lco/uk/getmondo/transaction/details/b$d;-><init>(Lco/uk/getmondo/transaction/details/b;)V

    check-cast v0, Lio/reactivex/c/g;

    .line 110
    sget-object v1, Lco/uk/getmondo/transaction/details/b$e;->a:Lco/uk/getmondo/transaction/details/b$e;

    check-cast v1, Lkotlin/d/a/b;

    if-eqz v1, :cond_3

    new-instance v2, Lco/uk/getmondo/transaction/details/c;

    invoke-direct {v2, v1}, Lco/uk/getmondo/transaction/details/c;-><init>(Lkotlin/d/a/b;)V

    move-object v1, v2

    :cond_3
    check-cast v1, Lio/reactivex/c/g;

    .line 108
    invoke-virtual {v4, v0, v1}, Lio/reactivex/n;->subscribe(Lio/reactivex/c/g;Lio/reactivex/c/g;)Lio/reactivex/b/b;

    move-result-object v0

    const-string v1, "view.onMapClicked()\n    \u2026            }, Timber::e)"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 107
    invoke-static {v3, v0}, Lco/uk/getmondo/common/j/f;->a(Lio/reactivex/b/a;Lio/reactivex/b/b;)Lio/reactivex/b/a;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/transaction/details/b;->b:Lio/reactivex/b/a;

    .line 111
    return-void
.end method
