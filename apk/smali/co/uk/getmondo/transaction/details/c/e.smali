.class public final Lco/uk/getmondo/transaction/details/c/e;
.super Ljava/lang/Object;
.source "SpendingBehaviour.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000.\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\t\n\u0002\u0008\u000b\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u000e\n\u0000\u0008\u0086\u0008\u0018\u00002\u00020\u0001B\u001d\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\u0002\u0010\u0007J\t\u0010\r\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u000e\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u000f\u001a\u00020\u0006H\u00c6\u0003J\'\u0010\u0010\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0005\u001a\u00020\u0006H\u00c6\u0001J\u0013\u0010\u0011\u001a\u00020\u00122\u0008\u0010\u0013\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u0010\u0014\u001a\u00020\u0015H\u00d6\u0001J\t\u0010\u0016\u001a\u00020\u0017H\u00d6\u0001R\u0011\u0010\u0004\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0008\u0010\tR\u0011\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\n\u0010\u000bR\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000c\u0010\t\u00a8\u0006\u0018"
    }
    d2 = {
        "Lco/uk/getmondo/transaction/details/model/SpendingBehaviour;",
        "",
        "totalSpent",
        "Lco/uk/getmondo/model/Amount;",
        "averageSpend",
        "noOfTransactions",
        "",
        "(Lco/uk/getmondo/model/Amount;Lco/uk/getmondo/model/Amount;J)V",
        "getAverageSpend",
        "()Lco/uk/getmondo/model/Amount;",
        "getNoOfTransactions",
        "()J",
        "getTotalSpent",
        "component1",
        "component2",
        "component3",
        "copy",
        "equals",
        "",
        "other",
        "hashCode",
        "",
        "toString",
        "",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field private final a:Lco/uk/getmondo/d/c;

.field private final b:Lco/uk/getmondo/d/c;

.field private final c:J


# direct methods
.method public constructor <init>(Lco/uk/getmondo/d/c;Lco/uk/getmondo/d/c;J)V
    .locals 1

    .prologue
    const-string v0, "totalSpent"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "averageSpend"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 5
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lco/uk/getmondo/transaction/details/c/e;->a:Lco/uk/getmondo/d/c;

    iput-object p2, p0, Lco/uk/getmondo/transaction/details/c/e;->b:Lco/uk/getmondo/d/c;

    iput-wide p3, p0, Lco/uk/getmondo/transaction/details/c/e;->c:J

    return-void
.end method


# virtual methods
.method public final a()Lco/uk/getmondo/d/c;
    .locals 1

    .prologue
    .line 6
    iget-object v0, p0, Lco/uk/getmondo/transaction/details/c/e;->a:Lco/uk/getmondo/d/c;

    return-object v0
.end method

.method public final b()Lco/uk/getmondo/d/c;
    .locals 1

    .prologue
    .line 7
    iget-object v0, p0, Lco/uk/getmondo/transaction/details/c/e;->b:Lco/uk/getmondo/d/c;

    return-object v0
.end method

.method public final c()J
    .locals 2

    .prologue
    .line 8
    iget-wide v0, p0, Lco/uk/getmondo/transaction/details/c/e;->c:J

    return-wide v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 6

    const/4 v1, 0x1

    const/4 v0, 0x0

    if-eq p0, p1, :cond_0

    instance-of v2, p1, Lco/uk/getmondo/transaction/details/c/e;

    if-eqz v2, :cond_1

    check-cast p1, Lco/uk/getmondo/transaction/details/c/e;

    iget-object v2, p0, Lco/uk/getmondo/transaction/details/c/e;->a:Lco/uk/getmondo/d/c;

    iget-object v3, p1, Lco/uk/getmondo/transaction/details/c/e;->a:Lco/uk/getmondo/d/c;

    invoke-static {v2, v3}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lco/uk/getmondo/transaction/details/c/e;->b:Lco/uk/getmondo/d/c;

    iget-object v3, p1, Lco/uk/getmondo/transaction/details/c/e;->b:Lco/uk/getmondo/d/c;

    invoke-static {v2, v3}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    iget-wide v2, p0, Lco/uk/getmondo/transaction/details/c/e;->c:J

    iget-wide v4, p1, Lco/uk/getmondo/transaction/details/c/e;->c:J

    cmp-long v2, v2, v4

    if-nez v2, :cond_2

    move v2, v1

    :goto_0
    if-eqz v2, :cond_1

    :cond_0
    move v0, v1

    :cond_1
    return v0

    :cond_2
    move v2, v0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 6

    const/4 v1, 0x0

    iget-object v0, p0, Lco/uk/getmondo/transaction/details/c/e;->a:Lco/uk/getmondo/d/c;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lco/uk/getmondo/transaction/details/c/e;->b:Lco/uk/getmondo/d/c;

    if-eqz v2, :cond_0

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-wide v2, p0, Lco/uk/getmondo/transaction/details/c/e;->c:J

    const/16 v1, 0x20

    ushr-long v4, v2, v1

    xor-long/2addr v2, v4

    long-to-int v1, v2

    add-int/2addr v0, v1

    return v0

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "SpendingBehaviour(totalSpent="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/transaction/details/c/e;->a:Lco/uk/getmondo/d/c;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", averageSpend="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/transaction/details/c/e;->b:Lco/uk/getmondo/d/c;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", noOfTransactions="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lco/uk/getmondo/transaction/details/c/e;->c:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
