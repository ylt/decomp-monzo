.class public final Lco/uk/getmondo/transaction/details/c/b;
.super Lco/uk/getmondo/transaction/details/c/f;
.source "TransactionHistory.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u00000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u000f\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u000e\n\u0000\u0008\u0086\u0008\u0018\u00002\u00020\u0001B%\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0003\u0012\u0006\u0010\u0007\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0008J\t\u0010\u000f\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0010\u001a\u00020\u0005H\u00c6\u0003J\t\u0010\u0011\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0012\u001a\u00020\u0005H\u00c6\u0003J1\u0010\u0013\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u00052\u0008\u0008\u0002\u0010\u0006\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0007\u001a\u00020\u0005H\u00c6\u0001J\u0013\u0010\u0014\u001a\u00020\u00152\u0008\u0010\u0016\u001a\u0004\u0018\u00010\u0017H\u00d6\u0003J\t\u0010\u0018\u001a\u00020\u0019H\u00d6\u0001J\t\u0010\u001a\u001a\u00020\u001bH\u00d6\u0001R\u0011\u0010\u0006\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\t\u0010\nR\u0011\u0010\u0007\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000b\u0010\u000cR\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\r\u0010\nR\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000e\u0010\u000c\u00a8\u0006\u001c"
    }
    d2 = {
        "Lco/uk/getmondo/transaction/details/model/CashFlowHistory;",
        "Lco/uk/getmondo/transaction/details/model/TransactionHistory;",
        "totalOutgoing",
        "",
        "totalOutgoingAmount",
        "Lco/uk/getmondo/model/Amount;",
        "totalIncoming",
        "totalIncomingAmount",
        "(JLco/uk/getmondo/model/Amount;JLco/uk/getmondo/model/Amount;)V",
        "getTotalIncoming",
        "()J",
        "getTotalIncomingAmount",
        "()Lco/uk/getmondo/model/Amount;",
        "getTotalOutgoing",
        "getTotalOutgoingAmount",
        "component1",
        "component2",
        "component3",
        "component4",
        "copy",
        "equals",
        "",
        "other",
        "",
        "hashCode",
        "",
        "toString",
        "",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field private final a:J

.field private final b:Lco/uk/getmondo/d/c;

.field private final c:J

.field private final d:Lco/uk/getmondo/d/c;


# direct methods
.method public constructor <init>(JLco/uk/getmondo/d/c;JLco/uk/getmondo/d/c;)V
    .locals 1

    .prologue
    const-string v0, "totalOutgoingAmount"

    invoke-static {p3, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "totalIncomingAmount"

    invoke-static {p6, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 15
    const/4 v0, 0x0

    .line 20
    invoke-direct {p0, v0}, Lco/uk/getmondo/transaction/details/c/f;-><init>(Lkotlin/d/b/i;)V

    iput-wide p1, p0, Lco/uk/getmondo/transaction/details/c/b;->a:J

    iput-object p3, p0, Lco/uk/getmondo/transaction/details/c/b;->b:Lco/uk/getmondo/d/c;

    iput-wide p4, p0, Lco/uk/getmondo/transaction/details/c/b;->c:J

    iput-object p6, p0, Lco/uk/getmondo/transaction/details/c/b;->d:Lco/uk/getmondo/d/c;

    return-void
.end method


# virtual methods
.method public final a()J
    .locals 2

    .prologue
    .line 16
    iget-wide v0, p0, Lco/uk/getmondo/transaction/details/c/b;->a:J

    return-wide v0
.end method

.method public final b()Lco/uk/getmondo/d/c;
    .locals 1

    .prologue
    .line 17
    iget-object v0, p0, Lco/uk/getmondo/transaction/details/c/b;->b:Lco/uk/getmondo/d/c;

    return-object v0
.end method

.method public final c()J
    .locals 2

    .prologue
    .line 18
    iget-wide v0, p0, Lco/uk/getmondo/transaction/details/c/b;->c:J

    return-wide v0
.end method

.method public final d()Lco/uk/getmondo/d/c;
    .locals 1

    .prologue
    .line 19
    iget-object v0, p0, Lco/uk/getmondo/transaction/details/c/b;->d:Lco/uk/getmondo/d/c;

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 6

    const/4 v1, 0x1

    const/4 v0, 0x0

    if-eq p0, p1, :cond_0

    instance-of v2, p1, Lco/uk/getmondo/transaction/details/c/b;

    if-eqz v2, :cond_1

    check-cast p1, Lco/uk/getmondo/transaction/details/c/b;

    iget-wide v2, p0, Lco/uk/getmondo/transaction/details/c/b;->a:J

    iget-wide v4, p1, Lco/uk/getmondo/transaction/details/c/b;->a:J

    cmp-long v2, v2, v4

    if-nez v2, :cond_2

    move v2, v1

    :goto_0
    if-eqz v2, :cond_1

    iget-object v2, p0, Lco/uk/getmondo/transaction/details/c/b;->b:Lco/uk/getmondo/d/c;

    iget-object v3, p1, Lco/uk/getmondo/transaction/details/c/b;->b:Lco/uk/getmondo/d/c;

    invoke-static {v2, v3}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    iget-wide v2, p0, Lco/uk/getmondo/transaction/details/c/b;->c:J

    iget-wide v4, p1, Lco/uk/getmondo/transaction/details/c/b;->c:J

    cmp-long v2, v2, v4

    if-nez v2, :cond_3

    move v2, v1

    :goto_1
    if-eqz v2, :cond_1

    iget-object v2, p0, Lco/uk/getmondo/transaction/details/c/b;->d:Lco/uk/getmondo/d/c;

    iget-object v3, p1, Lco/uk/getmondo/transaction/details/c/b;->d:Lco/uk/getmondo/d/c;

    invoke-static {v2, v3}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    move v0, v1

    :cond_1
    return v0

    :cond_2
    move v2, v0

    goto :goto_0

    :cond_3
    move v2, v0

    goto :goto_1
.end method

.method public hashCode()I
    .locals 7

    const/16 v6, 0x20

    const/4 v1, 0x0

    iget-wide v2, p0, Lco/uk/getmondo/transaction/details/c/b;->a:J

    ushr-long v4, v2, v6

    xor-long/2addr v2, v4

    long-to-int v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lco/uk/getmondo/transaction/details/c/b;->b:Lco/uk/getmondo/d/c;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    :goto_0
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-wide v2, p0, Lco/uk/getmondo/transaction/details/c/b;->c:J

    ushr-long v4, v2, v6

    xor-long/2addr v2, v4

    long-to-int v2, v2

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lco/uk/getmondo/transaction/details/c/b;->d:Lco/uk/getmondo/d/c;

    if-eqz v2, :cond_0

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_0
    add-int/2addr v0, v1

    return v0

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "CashFlowHistory(totalOutgoing="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lco/uk/getmondo/transaction/details/c/b;->a:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", totalOutgoingAmount="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/transaction/details/c/b;->b:Lco/uk/getmondo/d/c;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", totalIncoming="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lco/uk/getmondo/transaction/details/c/b;->c:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", totalIncomingAmount="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/transaction/details/c/b;->d:Lco/uk/getmondo/d/c;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
