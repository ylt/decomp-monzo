.class final Lco/uk/getmondo/transaction/details/TransactionDetailsActivity$c;
.super Lkotlin/d/b/m;
.source "TransactionDetailsActivity.kt"

# interfaces
.implements Lkotlin/d/a/m;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lco/uk/getmondo/transaction/details/TransactionDetailsActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/d/b/m;",
        "Lkotlin/d/a/m",
        "<",
        "Landroid/graphics/drawable/Drawable;",
        "Ljava/lang/Boolean;",
        "Lkotlin/n;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000\u0014\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "",
        "background",
        "Landroid/graphics/drawable/Drawable;",
        "showOverlay",
        "",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field final synthetic a:Lco/uk/getmondo/transaction/details/TransactionDetailsActivity;


# direct methods
.method constructor <init>(Lco/uk/getmondo/transaction/details/TransactionDetailsActivity;)V
    .locals 1

    iput-object p1, p0, Lco/uk/getmondo/transaction/details/TransactionDetailsActivity$c;->a:Lco/uk/getmondo/transaction/details/TransactionDetailsActivity;

    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lkotlin/d/b/m;-><init>(I)V

    return-void
.end method


# virtual methods
.method public synthetic a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 29
    check-cast p1, Landroid/graphics/drawable/Drawable;

    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-virtual {p0, p1, v0}, Lco/uk/getmondo/transaction/details/TransactionDetailsActivity$c;->a(Landroid/graphics/drawable/Drawable;Z)V

    sget-object v0, Lkotlin/n;->a:Lkotlin/n;

    return-object v0
.end method

.method public final a(Landroid/graphics/drawable/Drawable;Z)V
    .locals 2

    .prologue
    const-string v0, "background"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 51
    if-eqz p2, :cond_0

    iget-object v0, p0, Lco/uk/getmondo/transaction/details/TransactionDetailsActivity$c;->a:Lco/uk/getmondo/transaction/details/TransactionDetailsActivity;

    sget v1, Lco/uk/getmondo/c$a;->backgroundImageOverlay:I

    invoke-virtual {v0, v1}, Lco/uk/getmondo/transaction/details/TransactionDetailsActivity;->a(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Lco/uk/getmondo/common/ae;->a(Landroid/view/View;)V

    .line 52
    :goto_0
    iget-object v0, p0, Lco/uk/getmondo/transaction/details/TransactionDetailsActivity$c;->a:Lco/uk/getmondo/transaction/details/TransactionDetailsActivity;

    sget v1, Lco/uk/getmondo/c$a;->transactionAppBarLayout:I

    invoke-virtual {v0, v1}, Lco/uk/getmondo/transaction/details/TransactionDetailsActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/transaction/details/views/TransactionAppBarLayout;

    sget v1, Lco/uk/getmondo/c$a;->backgroundImage:I

    invoke-virtual {v0, v1}, Lco/uk/getmondo/transaction/details/views/TransactionAppBarLayout;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 53
    return-void

    .line 51
    :cond_0
    iget-object v0, p0, Lco/uk/getmondo/transaction/details/TransactionDetailsActivity$c;->a:Lco/uk/getmondo/transaction/details/TransactionDetailsActivity;

    sget v1, Lco/uk/getmondo/c$a;->backgroundImageOverlay:I

    invoke-virtual {v0, v1}, Lco/uk/getmondo/transaction/details/TransactionDetailsActivity;->a(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Lco/uk/getmondo/common/ae;->b(Landroid/view/View;)V

    goto :goto_0
.end method
