.class final Lco/uk/getmondo/transaction/details/TransactionDetailsActivity$e;
.super Ljava/lang/Object;
.source "TransactionDetailsActivity.kt"

# interfaces
.implements Landroid/support/v4/widget/NestedScrollView$b;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lco/uk/getmondo/transaction/details/TransactionDetailsActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000\u0016\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0004\u0010\u0000\u001a\u00020\u00012\u0008\u0010\u0002\u001a\u0004\u0018\u00010\u00032\u0006\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u00052\u0006\u0010\u0007\u001a\u00020\u00052\u0006\u0010\u0008\u001a\u00020\u0005H\n\u00a2\u0006\u0002\u0008\t"
    }
    d2 = {
        "<anonymous>",
        "",
        "<anonymous parameter 0>",
        "Landroid/support/v4/widget/NestedScrollView;",
        "<anonymous parameter 1>",
        "",
        "scrollY",
        "<anonymous parameter 3>",
        "<anonymous parameter 4>",
        "onScrollChange"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field final synthetic a:Lco/uk/getmondo/transaction/details/TransactionDetailsActivity;


# direct methods
.method constructor <init>(Lco/uk/getmondo/transaction/details/TransactionDetailsActivity;)V
    .locals 0

    iput-object p1, p0, Lco/uk/getmondo/transaction/details/TransactionDetailsActivity$e;->a:Lco/uk/getmondo/transaction/details/TransactionDetailsActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/support/v4/widget/NestedScrollView;IIII)V
    .locals 2

    .prologue
    .line 59
    iget-object v0, p0, Lco/uk/getmondo/transaction/details/TransactionDetailsActivity$e;->a:Lco/uk/getmondo/transaction/details/TransactionDetailsActivity;

    sget v1, Lco/uk/getmondo/c$a;->transactionHeaderView:I

    invoke-virtual {v0, v1}, Lco/uk/getmondo/transaction/details/TransactionDetailsActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/transaction/details/views/HeaderView;

    invoke-virtual {v0}, Lco/uk/getmondo/transaction/details/views/HeaderView;->c()I

    move-result v0

    .line 60
    if-lt p3, v0, :cond_0

    .line 61
    iget-object v0, p0, Lco/uk/getmondo/transaction/details/TransactionDetailsActivity$e;->a:Lco/uk/getmondo/transaction/details/TransactionDetailsActivity;

    sget v1, Lco/uk/getmondo/c$a;->transactionAppBarLayout:I

    invoke-virtual {v0, v1}, Lco/uk/getmondo/transaction/details/TransactionDetailsActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/transaction/details/views/TransactionAppBarLayout;

    invoke-virtual {v0}, Lco/uk/getmondo/transaction/details/views/TransactionAppBarLayout;->e()V

    .line 64
    :goto_0
    return-void

    .line 63
    :cond_0
    iget-object v0, p0, Lco/uk/getmondo/transaction/details/TransactionDetailsActivity$e;->a:Lco/uk/getmondo/transaction/details/TransactionDetailsActivity;

    sget v1, Lco/uk/getmondo/c$a;->transactionAppBarLayout:I

    invoke-virtual {v0, v1}, Lco/uk/getmondo/transaction/details/TransactionDetailsActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/transaction/details/views/TransactionAppBarLayout;

    invoke-virtual {v0}, Lco/uk/getmondo/transaction/details/views/TransactionAppBarLayout;->f()V

    goto :goto_0
.end method
