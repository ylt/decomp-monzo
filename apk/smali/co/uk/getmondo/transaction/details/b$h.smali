.class final Lco/uk/getmondo/transaction/details/b$h;
.super Ljava/lang/Object;
.source "TransactionDetailsPresenter.kt"

# interfaces
.implements Lio/reactivex/c/g;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lco/uk/getmondo/transaction/details/b;->a(Lco/uk/getmondo/transaction/details/b$a;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/g",
        "<",
        "Lkotlin/h",
        "<+",
        "Lco/uk/getmondo/d/aj;",
        "+",
        "Lco/uk/getmondo/transaction/details/c/f;",
        ">;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000\u0018\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012&\u0010\u0002\u001a\"\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u0005 \u0006*\u0010\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u0005\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\u0007"
    }
    d2 = {
        "<anonymous>",
        "",
        "<name for destructuring parameter 0>",
        "Lkotlin/Pair;",
        "Lco/uk/getmondo/model/Transaction;",
        "Lco/uk/getmondo/transaction/details/model/TransactionHistory;",
        "kotlin.jvm.PlatformType",
        "accept"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field final synthetic a:Lco/uk/getmondo/transaction/details/b;

.field final synthetic b:Lco/uk/getmondo/transaction/details/b$a;


# direct methods
.method constructor <init>(Lco/uk/getmondo/transaction/details/b;Lco/uk/getmondo/transaction/details/b$a;)V
    .locals 0

    iput-object p1, p0, Lco/uk/getmondo/transaction/details/b$h;->a:Lco/uk/getmondo/transaction/details/b;

    iput-object p2, p0, Lco/uk/getmondo/transaction/details/b$h;->b:Lco/uk/getmondo/transaction/details/b$a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 35
    check-cast p1, Lkotlin/h;

    invoke-virtual {p0, p1}, Lco/uk/getmondo/transaction/details/b$h;->a(Lkotlin/h;)V

    return-void
.end method

.method public final a(Lkotlin/h;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/h",
            "<+",
            "Lco/uk/getmondo/d/aj;",
            "+",
            "Lco/uk/getmondo/transaction/details/c/f;",
            ">;)V"
        }
    .end annotation

    .prologue
    invoke-virtual {p1}, Lkotlin/h;->c()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lco/uk/getmondo/d/aj;

    invoke-virtual {p1}, Lkotlin/h;->d()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lco/uk/getmondo/transaction/details/c/f;

    .line 71
    iget-object v0, p0, Lco/uk/getmondo/transaction/details/b$h;->b:Lco/uk/getmondo/transaction/details/b$a;

    invoke-virtual {v1}, Lco/uk/getmondo/d/aj;->c()Lco/uk/getmondo/d/h;

    move-result-object v3

    const-string v4, "transaction.category"

    invoke-static {v3, v4}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v0, v3}, Lco/uk/getmondo/transaction/details/b$a;->a(Lco/uk/getmondo/d/h;)V

    .line 73
    invoke-virtual {v1}, Lco/uk/getmondo/d/aj;->r()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v3, p0, Lco/uk/getmondo/transaction/details/b$h;->b:Lco/uk/getmondo/transaction/details/b$a;

    new-instance v0, Lco/uk/getmondo/transaction/details/d/c/a;

    .line 75
    const-string v4, "transaction"

    invoke-static {v1, v4}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    if-nez v2, :cond_0

    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type co.uk.getmondo.transaction.details.model.CashFlowHistory"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    check-cast v2, Lco/uk/getmondo/transaction/details/c/b;

    .line 74
    invoke-direct {v0, v1, v2}, Lco/uk/getmondo/transaction/details/d/c/a;-><init>(Lco/uk/getmondo/d/aj;Lco/uk/getmondo/transaction/details/c/b;)V

    check-cast v0, Lco/uk/getmondo/transaction/details/b/l;

    invoke-interface {v3, v0}, Lco/uk/getmondo/transaction/details/b$a;->a(Lco/uk/getmondo/transaction/details/b/l;)V

    .line 88
    :goto_0
    return-void

    .line 76
    :cond_1
    invoke-virtual {v1}, Lco/uk/getmondo/d/aj;->s()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v3, p0, Lco/uk/getmondo/transaction/details/b$h;->b:Lco/uk/getmondo/transaction/details/b$a;

    new-instance v0, Lco/uk/getmondo/transaction/details/d/b/b;

    const-string v4, "transaction"

    invoke-static {v1, v4}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 77
    const-string v4, "transactionHistory"

    invoke-static {v2, v4}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 76
    invoke-direct {v0, v1, v2}, Lco/uk/getmondo/transaction/details/d/b/b;-><init>(Lco/uk/getmondo/d/aj;Lco/uk/getmondo/transaction/details/c/f;)V

    check-cast v0, Lco/uk/getmondo/transaction/details/b/l;

    invoke-interface {v3, v0}, Lco/uk/getmondo/transaction/details/b$a;->a(Lco/uk/getmondo/transaction/details/b/l;)V

    goto :goto_0

    .line 78
    :cond_2
    invoke-virtual {v1}, Lco/uk/getmondo/d/aj;->q()Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v3, p0, Lco/uk/getmondo/transaction/details/b$h;->b:Lco/uk/getmondo/transaction/details/b$a;

    new-instance v0, Lco/uk/getmondo/transaction/details/d/f/c;

    const-string v4, "transaction"

    invoke-static {v1, v4}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 79
    if-nez v2, :cond_3

    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type co.uk.getmondo.transaction.details.model.CashFlowHistory"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_3
    check-cast v2, Lco/uk/getmondo/transaction/details/c/b;

    .line 78
    invoke-direct {v0, v1, v2}, Lco/uk/getmondo/transaction/details/d/f/c;-><init>(Lco/uk/getmondo/d/aj;Lco/uk/getmondo/transaction/details/c/b;)V

    check-cast v0, Lco/uk/getmondo/transaction/details/b/l;

    invoke-interface {v3, v0}, Lco/uk/getmondo/transaction/details/b$a;->a(Lco/uk/getmondo/transaction/details/b/l;)V

    goto :goto_0

    .line 80
    :cond_4
    invoke-virtual {v1}, Lco/uk/getmondo/d/aj;->z()Z

    move-result v0

    if-eqz v0, :cond_6

    iget-object v3, p0, Lco/uk/getmondo/transaction/details/b$h;->b:Lco/uk/getmondo/transaction/details/b$a;

    new-instance v0, Lco/uk/getmondo/transaction/details/d/h/c;

    const-string v4, "transaction"

    invoke-static {v1, v4}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 81
    if-nez v2, :cond_5

    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type co.uk.getmondo.transaction.details.model.AverageSpendingHistory"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_5
    check-cast v2, Lco/uk/getmondo/transaction/details/c/a;

    iget-object v4, p0, Lco/uk/getmondo/transaction/details/b$h;->a:Lco/uk/getmondo/transaction/details/b;

    invoke-static {v4}, Lco/uk/getmondo/transaction/details/b;->c(Lco/uk/getmondo/transaction/details/b;)Lco/uk/getmondo/common/accounts/b;

    move-result-object v4

    .line 80
    invoke-direct {v0, v1, v2, v4}, Lco/uk/getmondo/transaction/details/d/h/c;-><init>(Lco/uk/getmondo/d/aj;Lco/uk/getmondo/transaction/details/c/a;Lco/uk/getmondo/common/accounts/b;)V

    check-cast v0, Lco/uk/getmondo/transaction/details/b/l;

    invoke-interface {v3, v0}, Lco/uk/getmondo/transaction/details/b$a;->a(Lco/uk/getmondo/transaction/details/b/l;)V

    goto :goto_0

    .line 82
    :cond_6
    invoke-virtual {v1}, Lco/uk/getmondo/d/aj;->a()Z

    move-result v0

    if-eqz v0, :cond_8

    iget-object v3, p0, Lco/uk/getmondo/transaction/details/b$h;->b:Lco/uk/getmondo/transaction/details/b$a;

    new-instance v0, Lco/uk/getmondo/transaction/details/d/g/b;

    const-string v4, "transaction"

    invoke-static {v1, v4}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 83
    if-nez v2, :cond_7

    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type co.uk.getmondo.transaction.details.model.AverageSpendingHistory"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_7
    check-cast v2, Lco/uk/getmondo/transaction/details/c/a;

    .line 82
    invoke-direct {v0, v1, v2}, Lco/uk/getmondo/transaction/details/d/g/b;-><init>(Lco/uk/getmondo/d/aj;Lco/uk/getmondo/transaction/details/c/a;)V

    check-cast v0, Lco/uk/getmondo/transaction/details/b/l;

    invoke-interface {v3, v0}, Lco/uk/getmondo/transaction/details/b$a;->a(Lco/uk/getmondo/transaction/details/b/l;)V

    goto/16 :goto_0

    .line 84
    :cond_8
    invoke-virtual {v1}, Lco/uk/getmondo/d/aj;->f()Lco/uk/getmondo/d/u;

    move-result-object v0

    if-eqz v0, :cond_a

    invoke-virtual {v0}, Lco/uk/getmondo/d/u;->g()Z

    move-result v0

    const/4 v3, 0x1

    if-ne v0, v3, :cond_a

    iget-object v3, p0, Lco/uk/getmondo/transaction/details/b$h;->b:Lco/uk/getmondo/transaction/details/b$a;

    new-instance v0, Lco/uk/getmondo/transaction/details/d/a/a;

    const-string v4, "transaction"

    invoke-static {v1, v4}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 85
    if-nez v2, :cond_9

    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type co.uk.getmondo.transaction.details.model.AverageSpendingHistory"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_9
    check-cast v2, Lco/uk/getmondo/transaction/details/c/a;

    .line 84
    invoke-direct {v0, v1, v2}, Lco/uk/getmondo/transaction/details/d/a/a;-><init>(Lco/uk/getmondo/d/aj;Lco/uk/getmondo/transaction/details/c/a;)V

    check-cast v0, Lco/uk/getmondo/transaction/details/b/l;

    invoke-interface {v3, v0}, Lco/uk/getmondo/transaction/details/b$a;->a(Lco/uk/getmondo/transaction/details/b/l;)V

    goto/16 :goto_0

    .line 86
    :cond_a
    iget-object v7, p0, Lco/uk/getmondo/transaction/details/b$h;->b:Lco/uk/getmondo/transaction/details/b$a;

    new-instance v0, Lco/uk/getmondo/transaction/details/d/d/d;

    const-string v3, "transaction"

    invoke-static {v1, v3}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v3, "transactionHistory"

    invoke-static {v2, v3}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v3, p0, Lco/uk/getmondo/transaction/details/b$h;->a:Lco/uk/getmondo/transaction/details/b;

    invoke-static {v3}, Lco/uk/getmondo/transaction/details/b;->d(Lco/uk/getmondo/transaction/details/b;)Lco/uk/getmondo/transaction/a;

    move-result-object v3

    .line 87
    iget-object v4, p0, Lco/uk/getmondo/transaction/details/b$h;->a:Lco/uk/getmondo/transaction/details/b;

    invoke-static {v4}, Lco/uk/getmondo/transaction/details/b;->e(Lco/uk/getmondo/transaction/details/b;)Lco/uk/getmondo/payments/send/data/p;

    move-result-object v4

    iget-object v5, p0, Lco/uk/getmondo/transaction/details/b$h;->a:Lco/uk/getmondo/transaction/details/b;

    invoke-static {v5}, Lco/uk/getmondo/transaction/details/b;->f(Lco/uk/getmondo/transaction/details/b;)Lco/uk/getmondo/common/a;

    move-result-object v5

    iget-object v6, p0, Lco/uk/getmondo/transaction/details/b$h;->a:Lco/uk/getmondo/transaction/details/b;

    invoke-static {v6}, Lco/uk/getmondo/transaction/details/b;->c(Lco/uk/getmondo/transaction/details/b;)Lco/uk/getmondo/common/accounts/b;

    move-result-object v6

    .line 86
    invoke-direct/range {v0 .. v6}, Lco/uk/getmondo/transaction/details/d/d/d;-><init>(Lco/uk/getmondo/d/aj;Lco/uk/getmondo/transaction/details/c/f;Lco/uk/getmondo/transaction/a;Lco/uk/getmondo/payments/send/data/p;Lco/uk/getmondo/common/a;Lco/uk/getmondo/common/accounts/b;)V

    check-cast v0, Lco/uk/getmondo/transaction/details/b/l;

    invoke-interface {v7, v0}, Lco/uk/getmondo/transaction/details/b$a;->a(Lco/uk/getmondo/transaction/details/b/l;)V

    goto/16 :goto_0
.end method
