.class public final Lco/uk/getmondo/transaction/details/views/AvatarView$b;
.super Lcom/bumptech/glide/g/b/b;
.source "AvatarView.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lco/uk/getmondo/transaction/details/views/AvatarView;->a(Lco/uk/getmondo/transaction/details/b/d;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000\u001f\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000*\u0001\u0000\u0008\n\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\"\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\u0010\u0010\u0007\u001a\u000c\u0012\u0006\u0008\u0000\u0012\u00020\u0006\u0018\u00010\u0008H\u0016\u00a8\u0006\t"
    }
    d2 = {
        "co/uk/getmondo/transaction/details/views/AvatarView$bind$1",
        "Lcom/bumptech/glide/request/target/BitmapImageViewTarget;",
        "(Lco/uk/getmondo/transaction/details/views/AvatarView;Ljava/lang/Integer;Landroid/widget/ImageView;)V",
        "onResourceReady",
        "",
        "bitmap",
        "Landroid/graphics/Bitmap;",
        "glideAnimation",
        "Lcom/bumptech/glide/request/animation/GlideAnimation;",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field final synthetic a:Lco/uk/getmondo/transaction/details/views/AvatarView;

.field final synthetic b:Ljava/lang/Integer;


# direct methods
.method constructor <init>(Lco/uk/getmondo/transaction/details/views/AvatarView;Ljava/lang/Integer;Landroid/widget/ImageView;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Integer;",
            "Landroid/widget/ImageView;",
            ")V"
        }
    .end annotation

    .prologue
    .line 68
    iput-object p1, p0, Lco/uk/getmondo/transaction/details/views/AvatarView$b;->a:Lco/uk/getmondo/transaction/details/views/AvatarView;

    iput-object p2, p0, Lco/uk/getmondo/transaction/details/views/AvatarView$b;->b:Ljava/lang/Integer;

    invoke-direct {p0, p3}, Lcom/bumptech/glide/g/b/b;-><init>(Landroid/widget/ImageView;)V

    return-void
.end method


# virtual methods
.method public a(Landroid/graphics/Bitmap;Lcom/bumptech/glide/g/a/c;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/graphics/Bitmap;",
            "Lcom/bumptech/glide/g/a/c",
            "<-",
            "Landroid/graphics/Bitmap;",
            ">;)V"
        }
    .end annotation

    .prologue
    const-string v0, "bitmap"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 70
    invoke-super {p0, p1, p2}, Lcom/bumptech/glide/g/b/b;->a(Ljava/lang/Object;Lcom/bumptech/glide/g/a/c;)V

    .line 71
    invoke-static {p1}, Landroid/support/v7/d/b;->a(Landroid/graphics/Bitmap;)Landroid/support/v7/d/b$a;

    move-result-object v1

    new-instance v0, Lco/uk/getmondo/transaction/details/views/AvatarView$b$a;

    invoke-direct {v0, p0}, Lco/uk/getmondo/transaction/details/views/AvatarView$b$a;-><init>(Lco/uk/getmondo/transaction/details/views/AvatarView$b;)V

    check-cast v0, Landroid/support/v7/d/b$c;

    invoke-virtual {v1, v0}, Landroid/support/v7/d/b$a;->a(Landroid/support/v7/d/b$c;)Landroid/os/AsyncTask;

    .line 80
    return-void
.end method

.method public bridge synthetic a(Ljava/lang/Object;Lcom/bumptech/glide/g/a/c;)V
    .locals 0

    .prologue
    .line 68
    check-cast p1, Landroid/graphics/Bitmap;

    invoke-virtual {p0, p1, p2}, Lco/uk/getmondo/transaction/details/views/AvatarView$b;->a(Landroid/graphics/Bitmap;Lcom/bumptech/glide/g/a/c;)V

    return-void
.end method
