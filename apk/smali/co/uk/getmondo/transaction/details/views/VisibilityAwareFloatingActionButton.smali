.class public final Lco/uk/getmondo/transaction/details/views/VisibilityAwareFloatingActionButton;
.super Landroid/support/design/widget/FloatingActionButton;
.source "VisibilityAwareFloatingActionButton.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0003\n\u0002\u0010\u0002\n\u0002\u0008\u0003\u0018\u00002\u00020\u0001B%\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\n\u0008\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u0012\u0008\u0008\u0002\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\u0008J\u0010\u0010\n\u001a\u00020\u000b2\u0006\u0010\u000c\u001a\u00020\u0007H\u0016J\u0008\u0010\r\u001a\u00020\u000bH\u0016R\u000e\u0010\t\u001a\u00020\u0007X\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000e"
    }
    d2 = {
        "Lco/uk/getmondo/transaction/details/views/VisibilityAwareFloatingActionButton;",
        "Landroid/support/design/widget/FloatingActionButton;",
        "context",
        "Landroid/content/Context;",
        "attrs",
        "Landroid/util/AttributeSet;",
        "defStyleAttr",
        "",
        "(Landroid/content/Context;Landroid/util/AttributeSet;I)V",
        "userSetVisibility",
        "setVisibility",
        "",
        "visibility",
        "show",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field private d:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 6

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x6

    move-object v0, p0

    move-object v1, p1

    move-object v5, v2

    invoke-direct/range {v0 .. v5}, Lco/uk/getmondo/transaction/details/views/VisibilityAwareFloatingActionButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/d/b/i;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 6

    const/4 v3, 0x0

    const/4 v4, 0x4

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    invoke-direct/range {v0 .. v5}, Lco/uk/getmondo/transaction/details/views/VisibilityAwareFloatingActionButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/d/b/i;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 12
    invoke-direct {p0, p1, p2, p3}, Landroid/support/design/widget/FloatingActionButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 16
    invoke-virtual {p0}, Lco/uk/getmondo/transaction/details/views/VisibilityAwareFloatingActionButton;->getVisibility()I

    move-result v0

    iput v0, p0, Lco/uk/getmondo/transaction/details/views/VisibilityAwareFloatingActionButton;->d:I

    return-void
.end method

.method public synthetic constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/d/b/i;)V
    .locals 2

    .prologue
    and-int/lit8 v0, p4, 0x2

    if-eqz v0, :cond_1

    .line 10
    const/4 v0, 0x0

    check-cast v0, Landroid/util/AttributeSet;

    :goto_0
    and-int/lit8 v1, p4, 0x4

    if-eqz v1, :cond_0

    .line 11
    const/4 p3, 0x0

    :cond_0
    invoke-direct {p0, p1, v0, p3}, Lco/uk/getmondo/transaction/details/views/VisibilityAwareFloatingActionButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void

    :cond_1
    move-object v0, p2

    goto :goto_0
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 28
    iget v0, p0, Lco/uk/getmondo/transaction/details/views/VisibilityAwareFloatingActionButton;->d:I

    if-nez v0, :cond_0

    .line 29
    invoke-super {p0}, Landroid/support/design/widget/FloatingActionButton;->a()V

    .line 31
    :cond_0
    return-void
.end method

.method public setVisibility(I)V
    .locals 0

    .prologue
    .line 20
    invoke-super {p0, p1}, Landroid/support/design/widget/FloatingActionButton;->setVisibility(I)V

    .line 21
    iput p1, p0, Lco/uk/getmondo/transaction/details/views/VisibilityAwareFloatingActionButton;->d:I

    .line 22
    return-void
.end method
