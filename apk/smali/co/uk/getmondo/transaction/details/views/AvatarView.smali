.class public final Lco/uk/getmondo/transaction/details/views/AvatarView;
.super Landroid/widget/FrameLayout;
.source "AvatarView.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000J\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\n\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B%\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\n\u0008\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u0012\u0008\u0008\u0002\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\u0008J\u000e\u0010 \u001a\u00020\u00172\u0006\u0010!\u001a\u00020\"R\u001b\u0010\t\u001a\u00020\n8BX\u0082\u0084\u0002\u00a2\u0006\u000c\n\u0004\u0008\r\u0010\u000e\u001a\u0004\u0008\u000b\u0010\u000cRL\u0010\u000f\u001a4\u0012\u0013\u0012\u00110\u0011\u00a2\u0006\u000c\u0008\u0012\u0012\u0008\u0008\u0013\u0012\u0004\u0008\u0008(\u0014\u0012\u0013\u0012\u00110\u0015\u00a2\u0006\u000c\u0008\u0012\u0012\u0008\u0008\u0013\u0012\u0004\u0008\u0008(\u0016\u0012\u0004\u0012\u00020\u0017\u0018\u00010\u0010X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u0018\u0010\u0019\"\u0004\u0008\u001a\u0010\u001bR\u001b\u0010\u001c\u001a\u00020\u00078BX\u0082\u0084\u0002\u00a2\u0006\u000c\n\u0004\u0008\u001f\u0010\u000e\u001a\u0004\u0008\u001d\u0010\u001e\u00a8\u0006#"
    }
    d2 = {
        "Lco/uk/getmondo/transaction/details/views/AvatarView;",
        "Landroid/widget/FrameLayout;",
        "context",
        "Landroid/content/Context;",
        "attrs",
        "Landroid/util/AttributeSet;",
        "defStyleAttr",
        "",
        "(Landroid/content/Context;Landroid/util/AttributeSet;I)V",
        "avatarGenerator",
        "Lco/uk/getmondo/common/ui/AvatarGenerator;",
        "getAvatarGenerator",
        "()Lco/uk/getmondo/common/ui/AvatarGenerator;",
        "avatarGenerator$delegate",
        "Lkotlin/Lazy;",
        "backgroundListener",
        "Lkotlin/Function2;",
        "Landroid/graphics/drawable/Drawable;",
        "Lkotlin/ParameterName;",
        "name",
        "background",
        "",
        "showOverlay",
        "",
        "getBackgroundListener",
        "()Lkotlin/jvm/functions/Function2;",
        "setBackgroundListener",
        "(Lkotlin/jvm/functions/Function2;)V",
        "p2pPlaceholderSize",
        "getP2pPlaceholderSize",
        "()I",
        "p2pPlaceholderSize$delegate",
        "bind",
        "header",
        "Lco/uk/getmondo/transaction/details/base/BaseHeader;",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# static fields
.field static final synthetic a:[Lkotlin/reflect/l;


# instance fields
.field private final b:Lkotlin/c;

.field private final c:Lkotlin/c;

.field private d:Lkotlin/d/a/m;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/d/a/m",
            "<-",
            "Landroid/graphics/drawable/Drawable;",
            "-",
            "Ljava/lang/Boolean;",
            "Lkotlin/n;",
            ">;"
        }
    .end annotation
.end field

.field private e:Ljava/util/HashMap;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const/4 v0, 0x2

    new-array v1, v0, [Lkotlin/reflect/l;

    const/4 v2, 0x0

    new-instance v0, Lkotlin/d/b/w;

    const-class v3, Lco/uk/getmondo/transaction/details/views/AvatarView;

    invoke-static {v3}, Lkotlin/d/b/y;->a(Ljava/lang/Class;)Lkotlin/reflect/c;

    move-result-object v3

    const-string v4, "avatarGenerator"

    const-string v5, "getAvatarGenerator()Lco/uk/getmondo/common/ui/AvatarGenerator;"

    invoke-direct {v0, v3, v4, v5}, Lkotlin/d/b/w;-><init>(Lkotlin/reflect/e;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v0}, Lkotlin/d/b/y;->a(Lkotlin/d/b/v;)Lkotlin/reflect/n;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/l;

    aput-object v0, v1, v2

    const/4 v2, 0x1

    new-instance v0, Lkotlin/d/b/w;

    const-class v3, Lco/uk/getmondo/transaction/details/views/AvatarView;

    invoke-static {v3}, Lkotlin/d/b/y;->a(Ljava/lang/Class;)Lkotlin/reflect/c;

    move-result-object v3

    const-string v4, "p2pPlaceholderSize"

    const-string v5, "getP2pPlaceholderSize()I"

    invoke-direct {v0, v3, v4, v5}, Lkotlin/d/b/w;-><init>(Lkotlin/reflect/e;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v0}, Lkotlin/d/b/y;->a(Lkotlin/d/b/v;)Lkotlin/reflect/n;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/l;

    aput-object v0, v1, v2

    sput-object v1, Lco/uk/getmondo/transaction/details/views/AvatarView;->a:[Lkotlin/reflect/l;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 6

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x6

    move-object v0, p0

    move-object v1, p1

    move-object v5, v2

    invoke-direct/range {v0 .. v5}, Lco/uk/getmondo/transaction/details/views/AvatarView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/d/b/i;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 6

    const/4 v3, 0x0

    const/4 v4, 0x4

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    invoke-direct/range {v0 .. v5}, Lco/uk/getmondo/transaction/details/views/AvatarView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/d/b/i;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    .prologue
    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 31
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 32
    new-instance v0, Lco/uk/getmondo/transaction/details/views/AvatarView$a;

    invoke-direct {v0, p1}, Lco/uk/getmondo/transaction/details/views/AvatarView$a;-><init>(Landroid/content/Context;)V

    check-cast v0, Lkotlin/d/a/a;

    invoke-static {v0}, Lkotlin/d;->a(Lkotlin/d/a/a;)Lkotlin/c;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/transaction/details/views/AvatarView;->b:Lkotlin/c;

    .line 33
    new-instance v0, Lco/uk/getmondo/transaction/details/views/AvatarView$d;

    invoke-direct {v0, p0}, Lco/uk/getmondo/transaction/details/views/AvatarView$d;-><init>(Lco/uk/getmondo/transaction/details/views/AvatarView;)V

    check-cast v0, Lkotlin/d/a/a;

    invoke-static {v0}, Lkotlin/d;->a(Lkotlin/d/a/a;)Lkotlin/c;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/transaction/details/views/AvatarView;->c:Lkotlin/c;

    .line 36
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f050135

    check-cast p0, Landroid/view/ViewGroup;

    invoke-virtual {v0, v1, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    return-void
.end method

.method public synthetic constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/d/b/i;)V
    .locals 2

    .prologue
    and-int/lit8 v0, p4, 0x2

    if-eqz v0, :cond_1

    .line 29
    const/4 v0, 0x0

    check-cast v0, Landroid/util/AttributeSet;

    :goto_0
    and-int/lit8 v1, p4, 0x4

    if-eqz v1, :cond_0

    .line 30
    const/4 p3, 0x0

    :cond_0
    invoke-direct {p0, p1, v0, p3}, Lco/uk/getmondo/transaction/details/views/AvatarView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void

    :cond_1
    move-object v0, p2

    goto :goto_0
.end method

.method private final getAvatarGenerator()Lco/uk/getmondo/common/ui/a;
    .locals 3

    iget-object v0, p0, Lco/uk/getmondo/transaction/details/views/AvatarView;->b:Lkotlin/c;

    sget-object v1, Lco/uk/getmondo/transaction/details/views/AvatarView;->a:[Lkotlin/reflect/l;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-interface {v0}, Lkotlin/c;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/common/ui/a;

    return-object v0
.end method

.method private final getP2pPlaceholderSize()I
    .locals 3

    iget-object v0, p0, Lco/uk/getmondo/transaction/details/views/AvatarView;->c:Lkotlin/c;

    sget-object v1, Lco/uk/getmondo/transaction/details/views/AvatarView;->a:[Lkotlin/reflect/l;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-interface {v0}, Lkotlin/c;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Number;

    invoke-virtual {v0}, Ljava/lang/Number;->intValue()I

    move-result v0

    return v0
.end method


# virtual methods
.method public a(I)Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lco/uk/getmondo/transaction/details/views/AvatarView;->e:Ljava/util/HashMap;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lco/uk/getmondo/transaction/details/views/AvatarView;->e:Ljava/util/HashMap;

    :cond_0
    iget-object v0, p0, Lco/uk/getmondo/transaction/details/views/AvatarView;->e:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    if-nez v0, :cond_1

    invoke-virtual {p0, p1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/transaction/details/views/AvatarView;->e:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    return-object v0
.end method

.method public final a(Lco/uk/getmondo/transaction/details/b/d;)V
    .locals 8

    .prologue
    const/4 v2, 0x0

    const/4 v7, 0x0

    const-string v0, "header"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 42
    invoke-virtual {p1}, Lco/uk/getmondo/transaction/details/b/d;->j()Lco/uk/getmondo/transaction/details/b/c;

    move-result-object v3

    .line 43
    invoke-interface {v3}, Lco/uk/getmondo/transaction/details/b/c;->d()Z

    move-result v0

    if-nez v0, :cond_1

    .line 44
    sget v0, Lco/uk/getmondo/c$a;->transactionLogoOutlineView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/transaction/details/views/AvatarView;->a(I)Landroid/view/View;

    move-result-object v0

    const v1, 0x7f0200b8

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundResource(I)V

    .line 47
    :goto_0
    invoke-virtual {p0}, Lco/uk/getmondo/transaction/details/views/AvatarView;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "context"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v3, v0}, Lco/uk/getmondo/transaction/details/b/c;->a(Landroid/content/Context;)Ljava/lang/Integer;

    move-result-object v4

    .line 50
    invoke-interface {v3}, Lco/uk/getmondo/transaction/details/b/c;->a()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_5

    .line 51
    invoke-interface {v3}, Lco/uk/getmondo/transaction/details/b/c;->c()Ljava/lang/String;

    move-result-object v0

    .line 52
    if-eqz v0, :cond_2

    .line 53
    invoke-direct {p0}, Lco/uk/getmondo/transaction/details/views/AvatarView;->getAvatarGenerator()Lco/uk/getmondo/common/ui/a;

    move-result-object v1

    invoke-virtual {v1, v0}, Lco/uk/getmondo/common/ui/a;->b(Ljava/lang/String;)I

    move-result v6

    .line 54
    invoke-direct {p0}, Lco/uk/getmondo/transaction/details/views/AvatarView;->getAvatarGenerator()Lco/uk/getmondo/common/ui/a;

    move-result-object v1

    invoke-virtual {v1, v0}, Lco/uk/getmondo/common/ui/a;->a(Ljava/lang/String;)Lco/uk/getmondo/common/ui/a$b;

    move-result-object v0

    .line 55
    invoke-direct {p0}, Lco/uk/getmondo/transaction/details/views/AvatarView;->getP2pPlaceholderSize()I

    move-result v1

    invoke-interface {v3}, Lco/uk/getmondo/transaction/details/b/c;->d()Z

    move-result v3

    const/4 v4, 0x2

    move-object v5, v2

    invoke-static/range {v0 .. v5}, Lco/uk/getmondo/common/ui/a$b;->a(Lco/uk/getmondo/common/ui/a$b;ILandroid/graphics/Typeface;ZILjava/lang/Object;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 56
    sget v0, Lco/uk/getmondo/c$a;->transactionAvatarImageView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/transaction/details/views/AvatarView;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 57
    iget-object v0, p0, Lco/uk/getmondo/transaction/details/views/AvatarView;->d:Lkotlin/d/a/m;

    if-eqz v0, :cond_0

    new-instance v1, Landroid/graphics/drawable/ColorDrawable;

    const/high16 v2, 0x3f000000    # 0.5f

    invoke-static {v6, v2}, Lco/uk/getmondo/common/k/c;->a(IF)I

    move-result v2

    invoke-direct {v1, v2}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lkotlin/d/a/m;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/n;

    .line 93
    :cond_0
    :goto_1
    return-void

    .line 46
    :cond_1
    sget v0, Lco/uk/getmondo/c$a;->transactionAvatarImageView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/transaction/details/views/AvatarView;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setClipToOutline(Z)V

    goto :goto_0

    .line 59
    :cond_2
    if-eqz v4, :cond_3

    iget-object v0, p0, Lco/uk/getmondo/transaction/details/views/AvatarView;->d:Lkotlin/d/a/m;

    if-eqz v0, :cond_3

    new-instance v1, Landroid/graphics/drawable/ColorDrawable;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-direct {v1, v2}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lkotlin/d/a/m;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/n;

    .line 60
    :cond_3
    sget v0, Lco/uk/getmondo/c$a;->transactionAvatarImageView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/transaction/details/views/AvatarView;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    invoke-interface {v3}, Lco/uk/getmondo/transaction/details/b/c;->b()Ljava/lang/Integer;

    move-result-object v1

    if-nez v1, :cond_4

    invoke-static {}, Lkotlin/d/b/l;->a()V

    :cond_4
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_1

    .line 63
    :cond_5
    invoke-virtual {p0}, Lco/uk/getmondo/transaction/details/views/AvatarView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/bumptech/glide/g;->b(Landroid/content/Context;)Lcom/bumptech/glide/j;

    move-result-object v0

    .line 64
    invoke-interface {v3}, Lco/uk/getmondo/transaction/details/b/c;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/bumptech/glide/j;->a(Landroid/net/Uri;)Lcom/bumptech/glide/d;

    move-result-object v0

    .line 65
    invoke-virtual {v0}, Lcom/bumptech/glide/d;->h()Lcom/bumptech/glide/b;

    move-result-object v0

    .line 66
    invoke-virtual {v0}, Lcom/bumptech/glide/b;->a()Lcom/bumptech/glide/a;

    move-result-object v2

    .line 67
    invoke-interface {v3}, Lco/uk/getmondo/transaction/details/b/c;->d()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 68
    new-instance v1, Lco/uk/getmondo/transaction/details/views/AvatarView$b;

    sget v0, Lco/uk/getmondo/c$a;->transactionAvatarImageView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/transaction/details/views/AvatarView;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    invoke-direct {v1, p0, v4, v0}, Lco/uk/getmondo/transaction/details/views/AvatarView$b;-><init>(Lco/uk/getmondo/transaction/details/views/AvatarView;Ljava/lang/Integer;Landroid/widget/ImageView;)V

    move-object v0, v1

    check-cast v0, Lcom/bumptech/glide/g/b/j;

    invoke-virtual {v2, v0}, Lcom/bumptech/glide/a;->a(Lcom/bumptech/glide/g/b/j;)Lcom/bumptech/glide/g/b/j;

    goto :goto_1

    .line 86
    :cond_6
    sget-object v0, Lcom/bumptech/glide/load/engine/b;->b:Lcom/bumptech/glide/load/engine/b;

    invoke-virtual {v2, v0}, Lcom/bumptech/glide/a;->a(Lcom/bumptech/glide/load/engine/b;)Lcom/bumptech/glide/a;

    move-result-object v2

    .line 87
    new-instance v1, Lco/uk/getmondo/transaction/details/views/AvatarView$c;

    sget v0, Lco/uk/getmondo/c$a;->transactionAvatarImageView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/transaction/details/views/AvatarView;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    invoke-direct {v1, p0, v0}, Lco/uk/getmondo/transaction/details/views/AvatarView$c;-><init>(Lco/uk/getmondo/transaction/details/views/AvatarView;Landroid/widget/ImageView;)V

    move-object v0, v1

    check-cast v0, Lcom/bumptech/glide/g/b/j;

    invoke-virtual {v2, v0}, Lcom/bumptech/glide/a;->a(Lcom/bumptech/glide/g/b/j;)Lcom/bumptech/glide/g/b/j;

    goto/16 :goto_1
.end method

.method public final getBackgroundListener()Lkotlin/d/a/m;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/d/a/m",
            "<",
            "Landroid/graphics/drawable/Drawable;",
            "Ljava/lang/Boolean;",
            "Lkotlin/n;",
            ">;"
        }
    .end annotation

    .prologue
    .line 39
    iget-object v0, p0, Lco/uk/getmondo/transaction/details/views/AvatarView;->d:Lkotlin/d/a/m;

    return-object v0
.end method

.method public final setBackgroundListener(Lkotlin/d/a/m;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/d/a/m",
            "<-",
            "Landroid/graphics/drawable/Drawable;",
            "-",
            "Ljava/lang/Boolean;",
            "Lkotlin/n;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 39
    iput-object p1, p0, Lco/uk/getmondo/transaction/details/views/AvatarView;->d:Lkotlin/d/a/m;

    return-void
.end method
