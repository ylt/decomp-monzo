.class public final Lco/uk/getmondo/transaction/details/views/TransactionAppBarLayout;
.super Landroid/support/design/widget/AppBarLayout;
.source "TransactionAppBarLayout.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lco/uk/getmondo/transaction/details/views/TransactionAppBarLayout$a;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000R\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\r\n\u0002\u0010\u0006\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0002\u0008\u0003\u0018\u0000 +2\u00020\u0001:\u0001+B\u001b\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\n\u0008\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u00a2\u0006\u0002\u0010\u0006J\u000e\u0010\u0012\u001a\u00020\t2\u0006\u0010\u0013\u001a\u00020\u0014J\u0006\u0010\u0015\u001a\u00020\tJ\u0010\u0010\u0016\u001a\u00020\t2\u0008\u0010\u0017\u001a\u0004\u0018\u00010\u0018J\u0006\u0010\u0019\u001a\u00020\tJ\u0006\u0010\u001a\u001a\u00020\tJ\u0018\u0010\u001b\u001a\u00020\t2\u0006\u0010\u001c\u001a\u00020\u00112\u0006\u0010\u001d\u001a\u00020\u0011H\u0014J\u0006\u0010\u001e\u001a\u00020\tJ\u0006\u0010\u001f\u001a\u00020\tJ\u0006\u0010 \u001a\u00020\tJ\u0006\u0010!\u001a\u00020\tJ\u000e\u0010\"\u001a\u00020\t2\u0006\u0010#\u001a\u00020\u0018J\"\u0010$\u001a\u00020\t2\u0006\u0010%\u001a\u00020&2\u0006\u0010\'\u001a\u00020&2\u0008\u0010(\u001a\u0004\u0018\u00010)H\u0002J\u0006\u0010*\u001a\u00020\tR(\u0010\u0007\u001a\u0010\u0012\u0004\u0012\u00020\t\u0012\u0004\u0012\u00020\t\u0018\u00010\u0008X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\n\u0010\u000b\"\u0004\u0008\u000c\u0010\rR\u000e\u0010\u000e\u001a\u00020\u000fX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0010\u001a\u00020\u0011X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006,"
    }
    d2 = {
        "Lco/uk/getmondo/transaction/details/views/TransactionAppBarLayout;",
        "Landroid/support/design/widget/AppBarLayout;",
        "context",
        "Landroid/content/Context;",
        "attrs",
        "Landroid/util/AttributeSet;",
        "(Landroid/content/Context;Landroid/util/AttributeSet;)V",
        "mapListener",
        "Lkotlin/Function1;",
        "",
        "getMapListener",
        "()Lkotlin/jvm/functions/Function1;",
        "setMapListener",
        "(Lkotlin/jvm/functions/Function1;)V",
        "rect",
        "Landroid/graphics/Rect;",
        "scrimVisibleHeightTriggerOffset",
        "",
        "bind",
        "header",
        "Lco/uk/getmondo/transaction/details/base/BaseHeader;",
        "hideTitle",
        "onCreate",
        "savedInstanceState",
        "Landroid/os/Bundle;",
        "onDestroy",
        "onLowMemory",
        "onMeasure",
        "widthMeasureSpec",
        "heightMeasureSpec",
        "onPause",
        "onResume",
        "onStart",
        "onStop",
        "saveInstanceState",
        "outState",
        "showCoordinates",
        "latitude",
        "",
        "longitude",
        "label",
        "",
        "showTitle",
        "Companion",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# static fields
.field public static final a:Lco/uk/getmondo/transaction/details/views/TransactionAppBarLayout$a;


# instance fields
.field private final b:Landroid/graphics/Rect;

.field private final c:I

.field private d:Lkotlin/d/a/b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/d/a/b",
            "<-",
            "Lkotlin/n;",
            "Lkotlin/n;",
            ">;"
        }
    .end annotation
.end field

.field private e:Ljava/util/HashMap;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lco/uk/getmondo/transaction/details/views/TransactionAppBarLayout$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lco/uk/getmondo/transaction/details/views/TransactionAppBarLayout$a;-><init>(Lkotlin/d/b/i;)V

    sput-object v0, Lco/uk/getmondo/transaction/details/views/TransactionAppBarLayout;->a:Lco/uk/getmondo/transaction/details/views/TransactionAppBarLayout$a;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    const/4 v1, 0x0

    const/4 v0, 0x2

    invoke-direct {p0, p1, v1, v0, v1}, Lco/uk/getmondo/transaction/details/views/TransactionAppBarLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;ILkotlin/d/b/i;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    .prologue
    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 24
    invoke-direct {p0, p1, p2}, Landroid/support/design/widget/AppBarLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 25
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lco/uk/getmondo/transaction/details/views/TransactionAppBarLayout;->b:Landroid/graphics/Rect;

    .line 26
    const/4 v0, 0x1

    invoke-static {v0}, Lco/uk/getmondo/common/k/n;->b(I)I

    move-result v0

    iput v0, p0, Lco/uk/getmondo/transaction/details/views/TransactionAppBarLayout;->c:I

    .line 29
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f050134

    check-cast p0, Landroid/view/ViewGroup;

    invoke-virtual {v0, v1, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    return-void
.end method

.method public synthetic constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;ILkotlin/d/b/i;)V
    .locals 1

    .prologue
    and-int/lit8 v0, p3, 0x2

    if-eqz v0, :cond_0

    .line 23
    const/4 v0, 0x0

    check-cast v0, Landroid/util/AttributeSet;

    :goto_0
    invoke-direct {p0, p1, v0}, Lco/uk/getmondo/transaction/details/views/TransactionAppBarLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void

    :cond_0
    move-object v0, p2

    goto :goto_0
.end method

.method private final a(DDLjava/lang/String;)V
    .locals 9

    .prologue
    .line 69
    sget v0, Lco/uk/getmondo/c$a;->map:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/transaction/details/views/TransactionAppBarLayout;->b(I)Landroid/view/View;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lcom/google/android/gms/maps/MapView;

    new-instance v0, Lco/uk/getmondo/transaction/details/views/TransactionAppBarLayout$c;

    move-object v1, p0

    move-wide v2, p1

    move-wide v4, p3

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lco/uk/getmondo/transaction/details/views/TransactionAppBarLayout$c;-><init>(Lco/uk/getmondo/transaction/details/views/TransactionAppBarLayout;DDLjava/lang/String;)V

    check-cast v0, Lcom/google/android/gms/maps/OnMapReadyCallback;

    invoke-virtual {v7, v0}, Lcom/google/android/gms/maps/MapView;->getMapAsync(Lcom/google/android/gms/maps/OnMapReadyCallback;)V

    .line 77
    sget v0, Lco/uk/getmondo/c$a;->collapsingToolbar:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/transaction/details/views/TransactionAppBarLayout;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/design/widget/CollapsingToolbarLayout;

    const v1, 0x7f0b011a

    invoke-static {v0, v1}, Lco/uk/getmondo/common/ae;->b(Landroid/view/View;I)V

    .line 78
    sget v0, Lco/uk/getmondo/c$a;->map:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/transaction/details/views/TransactionAppBarLayout;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/maps/MapView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/gms/maps/MapView;->setVisibility(I)V

    .line 79
    sget v0, Lco/uk/getmondo/c$a;->backgroundImage:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/transaction/details/views/TransactionAppBarLayout;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 80
    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 84
    if-eqz p1, :cond_0

    const-string v0, "KEY_MAP_VIEW_STATE"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    move-object v1, v0

    .line 85
    :goto_0
    sget v0, Lco/uk/getmondo/c$a;->map:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/transaction/details/views/TransactionAppBarLayout;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/maps/MapView;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/maps/MapView;->onCreate(Landroid/os/Bundle;)V

    .line 86
    return-void

    .line 84
    :cond_0
    const/4 v0, 0x0

    move-object v1, v0

    goto :goto_0
.end method

.method public final a(Lco/uk/getmondo/transaction/details/b/d;)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const-string v0, "header"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 35
    sget v0, Lco/uk/getmondo/c$a;->titleView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/transaction/details/views/TransactionAppBarLayout;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {p0}, Lco/uk/getmondo/transaction/details/views/TransactionAppBarLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const-string v2, "resources"

    invoke-static {v1, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1, v1}, Lco/uk/getmondo/transaction/details/b/d;->c(Landroid/content/res/Resources;)Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 37
    invoke-virtual {p1}, Lco/uk/getmondo/transaction/details/b/d;->h()Lco/uk/getmondo/transaction/details/c/c;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lco/uk/getmondo/transaction/details/c/c;->b()D

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    .line 38
    :goto_0
    invoke-virtual {p1}, Lco/uk/getmondo/transaction/details/b/d;->h()Lco/uk/getmondo/transaction/details/c/c;

    move-result-object v1

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Lco/uk/getmondo/transaction/details/c/c;->c()D

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v1

    .line 39
    :goto_1
    if-eqz v0, :cond_1

    if-eqz v1, :cond_1

    .line 40
    invoke-virtual {v0}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v2

    invoke-virtual {v1}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v4

    invoke-virtual {p1}, Lco/uk/getmondo/transaction/details/b/d;->h()Lco/uk/getmondo/transaction/details/c/c;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lco/uk/getmondo/transaction/details/c/c;->a()Ljava/lang/String;

    move-result-object v6

    :cond_0
    move-object v1, p0

    invoke-direct/range {v1 .. v6}, Lco/uk/getmondo/transaction/details/views/TransactionAppBarLayout;->a(DDLjava/lang/String;)V

    .line 42
    :cond_1
    sget v0, Lco/uk/getmondo/c$a;->mapTouchWrapper:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/transaction/details/views/TransactionAppBarLayout;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/common/ui/ClickInterceptWrapper;

    new-instance v1, Lco/uk/getmondo/transaction/details/views/TransactionAppBarLayout$b;

    invoke-direct {v1, p0}, Lco/uk/getmondo/transaction/details/views/TransactionAppBarLayout$b;-><init>(Lco/uk/getmondo/transaction/details/views/TransactionAppBarLayout;)V

    check-cast v1, Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Lco/uk/getmondo/common/ui/ClickInterceptWrapper;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 43
    return-void

    :cond_2
    move-object v0, v6

    .line 37
    goto :goto_0

    :cond_3
    move-object v1, v6

    .line 38
    goto :goto_1
.end method

.method public b(I)Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lco/uk/getmondo/transaction/details/views/TransactionAppBarLayout;->e:Ljava/util/HashMap;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lco/uk/getmondo/transaction/details/views/TransactionAppBarLayout;->e:Ljava/util/HashMap;

    :cond_0
    iget-object v0, p0, Lco/uk/getmondo/transaction/details/views/TransactionAppBarLayout;->e:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    if-nez v0, :cond_1

    invoke-virtual {p0, p1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/transaction/details/views/TransactionAppBarLayout;->e:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    return-object v0
.end method

.method public final b(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    const-string v0, "outState"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 90
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1, p1}, Landroid/os/Bundle;-><init>(Landroid/os/Bundle;)V

    .line 91
    sget v0, Lco/uk/getmondo/c$a;->map:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/transaction/details/views/TransactionAppBarLayout;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/maps/MapView;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/maps/MapView;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 92
    const-string v0, "KEY_MAP_VIEW_STATE"

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 93
    return-void
.end method

.method public final e()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 46
    sget v0, Lco/uk/getmondo/c$a;->titleView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/transaction/details/views/TransactionAppBarLayout;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getVisibility()I

    move-result v0

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    .line 47
    sget v0, Lco/uk/getmondo/c$a;->titleView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/transaction/details/views/TransactionAppBarLayout;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 48
    sget v0, Lco/uk/getmondo/c$a;->titleView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/transaction/details/views/TransactionAppBarLayout;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setAlpha(F)V

    .line 49
    sget v0, Lco/uk/getmondo/c$a;->titleView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/transaction/details/views/TransactionAppBarLayout;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    sget v1, Lco/uk/getmondo/c$a;->titleView:I

    invoke-virtual {p0, v1}, Lco/uk/getmondo/transaction/details/views/TransactionAppBarLayout;->b(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getHeight()I

    move-result v1

    int-to-float v1, v1

    const/4 v2, 0x2

    int-to-float v2, v2

    div-float/2addr v1, v2

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTranslationY(F)V

    .line 50
    sget v0, Lco/uk/getmondo/c$a;->titleView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/transaction/details/views/TransactionAppBarLayout;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/ViewPropertyAnimator;->translationY(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const-wide/16 v2, 0xc8

    invoke-virtual {v0, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    .line 52
    :cond_0
    return-void
.end method

.method public final f()V
    .locals 2

    .prologue
    .line 55
    sget v0, Lco/uk/getmondo/c$a;->titleView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/transaction/details/views/TransactionAppBarLayout;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    .line 56
    sget v0, Lco/uk/getmondo/c$a;->titleView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/transaction/details/views/TransactionAppBarLayout;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 58
    :cond_0
    return-void
.end method

.method public final g()V
    .locals 1

    .prologue
    .line 96
    sget v0, Lco/uk/getmondo/c$a;->map:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/transaction/details/views/TransactionAppBarLayout;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/maps/MapView;

    invoke-virtual {v0}, Lcom/google/android/gms/maps/MapView;->onStart()V

    .line 97
    return-void
.end method

.method public final getMapListener()Lkotlin/d/a/b;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/d/a/b",
            "<",
            "Lkotlin/n;",
            "Lkotlin/n;",
            ">;"
        }
    .end annotation

    .prologue
    .line 32
    iget-object v0, p0, Lco/uk/getmondo/transaction/details/views/TransactionAppBarLayout;->d:Lkotlin/d/a/b;

    return-object v0
.end method

.method public final h()V
    .locals 1

    .prologue
    .line 100
    sget v0, Lco/uk/getmondo/c$a;->map:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/transaction/details/views/TransactionAppBarLayout;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/maps/MapView;

    invoke-virtual {v0}, Lcom/google/android/gms/maps/MapView;->onResume()V

    .line 101
    return-void
.end method

.method public final i()V
    .locals 1

    .prologue
    .line 104
    sget v0, Lco/uk/getmondo/c$a;->map:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/transaction/details/views/TransactionAppBarLayout;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/maps/MapView;

    invoke-virtual {v0}, Lcom/google/android/gms/maps/MapView;->onPause()V

    .line 105
    return-void
.end method

.method public final j()V
    .locals 1

    .prologue
    .line 108
    sget v0, Lco/uk/getmondo/c$a;->map:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/transaction/details/views/TransactionAppBarLayout;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/maps/MapView;

    invoke-virtual {v0}, Lcom/google/android/gms/maps/MapView;->onStop()V

    .line 109
    return-void
.end method

.method public final k()V
    .locals 1

    .prologue
    .line 112
    sget v0, Lco/uk/getmondo/c$a;->map:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/transaction/details/views/TransactionAppBarLayout;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/maps/MapView;

    invoke-virtual {v0}, Lcom/google/android/gms/maps/MapView;->onDestroy()V

    .line 113
    return-void
.end method

.method public final l()V
    .locals 1

    .prologue
    .line 116
    sget v0, Lco/uk/getmondo/c$a;->map:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/transaction/details/views/TransactionAppBarLayout;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/maps/MapView;

    invoke-virtual {v0}, Lcom/google/android/gms/maps/MapView;->onLowMemory()V

    .line 117
    return-void
.end method

.method protected onMeasure(II)V
    .locals 3

    .prologue
    .line 61
    invoke-super {p0, p1, p2}, Landroid/support/design/widget/AppBarLayout;->onMeasure(II)V

    .line 63
    invoke-virtual {p0}, Lco/uk/getmondo/transaction/details/views/TransactionAppBarLayout;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-nez v0, :cond_0

    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type android.view.ViewGroup"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    check-cast v0, Landroid/view/ViewGroup;

    .line 64
    sget v1, Lco/uk/getmondo/c$a;->toolbar:I

    invoke-virtual {p0, v1}, Lco/uk/getmondo/transaction/details/views/TransactionAppBarLayout;->b(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/support/v7/widget/Toolbar;

    const-string v2, "toolbar"

    invoke-static {v1, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v1, Landroid/view/View;

    iget-object v2, p0, Lco/uk/getmondo/transaction/details/views/TransactionAppBarLayout;->b:Landroid/graphics/Rect;

    invoke-static {v0, v1, v2}, Lco/uk/getmondo/common/k/q;->a(Landroid/view/ViewGroup;Landroid/view/View;Landroid/graphics/Rect;)V

    .line 65
    sget v0, Lco/uk/getmondo/c$a;->collapsingToolbar:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/transaction/details/views/TransactionAppBarLayout;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/design/widget/CollapsingToolbarLayout;

    iget-object v1, p0, Lco/uk/getmondo/transaction/details/views/TransactionAppBarLayout;->b:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->bottom:I

    iget v2, p0, Lco/uk/getmondo/transaction/details/views/TransactionAppBarLayout;->c:I

    add-int/2addr v1, v2

    invoke-virtual {v0, v1}, Landroid/support/design/widget/CollapsingToolbarLayout;->setScrimVisibleHeightTrigger(I)V

    .line 66
    return-void
.end method

.method public final setMapListener(Lkotlin/d/a/b;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/d/a/b",
            "<-",
            "Lkotlin/n;",
            "Lkotlin/n;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 32
    iput-object p1, p0, Lco/uk/getmondo/transaction/details/views/TransactionAppBarLayout;->d:Lkotlin/d/a/b;

    return-void
.end method
