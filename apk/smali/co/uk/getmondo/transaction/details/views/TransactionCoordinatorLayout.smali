.class public final Lco/uk/getmondo/transaction/details/views/TransactionCoordinatorLayout;
.super Landroid/support/design/widget/CoordinatorLayout;
.source "TransactionCoordinatorLayout.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lco/uk/getmondo/transaction/details/views/TransactionCoordinatorLayout$b;,
        Lco/uk/getmondo/transaction/details/views/TransactionCoordinatorLayout$a;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000<\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u000b\u0018\u0000 \u001d2\u00020\u0001:\u0002\u001d\u001eB!\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0008\u0008\u0002\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\u0008J\u0018\u0010\u0011\u001a\u00020\u00122\u0006\u0010\u0013\u001a\u00020\u00142\u0006\u0010\u0015\u001a\u00020\nH\u0002J \u0010\u0016\u001a\u00020\u00122\u0006\u0010\u0013\u001a\u00020\u00142\u0006\u0010\u0017\u001a\u00020\u00072\u0006\u0010\u0015\u001a\u00020\nH\u0003J\u0010\u0010\u0018\u001a\u00020\u00122\u0006\u0010\u0013\u001a\u00020\u0014H\u0002J\u0008\u0010\u0019\u001a\u00020\u0012H\u0016J\u0008\u0010\u001a\u001a\u00020\u0012H\u0016J\u0008\u0010\u001b\u001a\u00020\u0012H\u0002J\u0008\u0010\u001c\u001a\u00020\u0012H\u0002R\u000e\u0010\t\u001a\u00020\nX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\nX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000c\u001a\u00020\nX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\nX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0012\u0010\u000e\u001a\u00060\u000fR\u00020\u0000X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0010\u001a\u00020\nX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001f"
    }
    d2 = {
        "Lco/uk/getmondo/transaction/details/views/TransactionCoordinatorLayout;",
        "Landroid/support/design/widget/CoordinatorLayout;",
        "context",
        "Landroid/content/Context;",
        "attrs",
        "Landroid/util/AttributeSet;",
        "defStyleAttr",
        "",
        "(Landroid/content/Context;Landroid/util/AttributeSet;I)V",
        "appBarLayoutRect",
        "Landroid/graphics/Rect;",
        "childRect",
        "desiredChildRect",
        "headerRect",
        "onPreDrawListener",
        "Lco/uk/getmondo/transaction/details/views/TransactionCoordinatorLayout$OnPreDrawListener;",
        "toolbarRect",
        "getChildRect",
        "",
        "child",
        "Landroid/view/View;",
        "out",
        "getDesiredChildRect",
        "gravity",
        "offsetChildIfNeeded",
        "onAttachedToWindow",
        "onDetachedFromWindow",
        "updateAvatar",
        "updateFab",
        "Companion",
        "OnPreDrawListener",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# static fields
.field public static final f:Lco/uk/getmondo/transaction/details/views/TransactionCoordinatorLayout$a;

.field private static final m:F


# instance fields
.field private final g:Lco/uk/getmondo/transaction/details/views/TransactionCoordinatorLayout$b;

.field private final h:Landroid/graphics/Rect;

.field private final i:Landroid/graphics/Rect;

.field private final j:Landroid/graphics/Rect;

.field private final k:Landroid/graphics/Rect;

.field private final l:Landroid/graphics/Rect;

.field private n:Ljava/util/HashMap;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    new-instance v0, Lco/uk/getmondo/transaction/details/views/TransactionCoordinatorLayout$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lco/uk/getmondo/transaction/details/views/TransactionCoordinatorLayout$a;-><init>(Lkotlin/d/b/i;)V

    sput-object v0, Lco/uk/getmondo/transaction/details/views/TransactionCoordinatorLayout;->f:Lco/uk/getmondo/transaction/details/views/TransactionCoordinatorLayout$a;

    .line 134
    const/16 v0, 0x48

    invoke-static {v0}, Lco/uk/getmondo/common/k/n;->b(I)I

    move-result v0

    int-to-float v0, v0

    neg-float v0, v0

    sput v0, Lco/uk/getmondo/transaction/details/views/TransactionCoordinatorLayout;->m:F

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 6

    const/4 v3, 0x0

    const/4 v4, 0x4

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    invoke-direct/range {v0 .. v5}, Lco/uk/getmondo/transaction/details/views/TransactionCoordinatorLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/d/b/i;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "attrs"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 21
    invoke-direct {p0, p1, p2, p3}, Landroid/support/design/widget/CoordinatorLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 22
    new-instance v0, Lco/uk/getmondo/transaction/details/views/TransactionCoordinatorLayout$b;

    invoke-direct {v0, p0}, Lco/uk/getmondo/transaction/details/views/TransactionCoordinatorLayout$b;-><init>(Lco/uk/getmondo/transaction/details/views/TransactionCoordinatorLayout;)V

    iput-object v0, p0, Lco/uk/getmondo/transaction/details/views/TransactionCoordinatorLayout;->g:Lco/uk/getmondo/transaction/details/views/TransactionCoordinatorLayout$b;

    .line 24
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lco/uk/getmondo/transaction/details/views/TransactionCoordinatorLayout;->h:Landroid/graphics/Rect;

    .line 25
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lco/uk/getmondo/transaction/details/views/TransactionCoordinatorLayout;->i:Landroid/graphics/Rect;

    .line 26
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lco/uk/getmondo/transaction/details/views/TransactionCoordinatorLayout;->j:Landroid/graphics/Rect;

    .line 28
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lco/uk/getmondo/transaction/details/views/TransactionCoordinatorLayout;->k:Landroid/graphics/Rect;

    .line 29
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lco/uk/getmondo/transaction/details/views/TransactionCoordinatorLayout;->l:Landroid/graphics/Rect;

    return-void
.end method

.method public synthetic constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/d/b/i;)V
    .locals 1

    .prologue
    and-int/lit8 v0, p4, 0x4

    if-eqz v0, :cond_0

    .line 20
    const/4 p3, 0x0

    :cond_0
    invoke-direct {p0, p1, p2, p3}, Lco/uk/getmondo/transaction/details/views/TransactionCoordinatorLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public static final synthetic a(Lco/uk/getmondo/transaction/details/views/TransactionCoordinatorLayout;)Landroid/graphics/Rect;
    .locals 1

    .prologue
    .line 17
    iget-object v0, p0, Lco/uk/getmondo/transaction/details/views/TransactionCoordinatorLayout;->h:Landroid/graphics/Rect;

    return-object v0
.end method

.method private final a(Landroid/view/View;ILandroid/graphics/Rect;)V
    .locals 8
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "RtlHardcoded"
        }
    .end annotation

    .prologue
    .line 79
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    if-nez v0, :cond_0

    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type android.support.design.widget.CoordinatorLayout.LayoutParams"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    check-cast v0, Landroid/support/design/widget/CoordinatorLayout$d;

    .line 80
    invoke-virtual {p0}, Lco/uk/getmondo/transaction/details/views/TransactionCoordinatorLayout;->getLayoutDirection()I

    move-result v1

    invoke-static {p2, v1}, Landroid/support/v4/view/d;->a(II)I

    move-result v1

    .line 82
    and-int/lit8 v2, v1, 0x7

    .line 83
    and-int/lit8 v3, v1, 0x70

    .line 85
    invoke-virtual {p1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v4

    .line 86
    invoke-virtual {p1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v5

    .line 88
    packed-switch v2, :pswitch_data_0

    .line 91
    :pswitch_0
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    .line 89
    :pswitch_1
    iget-object v1, p0, Lco/uk/getmondo/transaction/details/views/TransactionCoordinatorLayout;->h:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->right:I

    sub-int/2addr v1, v4

    move v2, v1

    .line 94
    :goto_0
    sparse-switch v3, :sswitch_data_0

    .line 97
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    .line 90
    :pswitch_2
    iget-object v1, p0, Lco/uk/getmondo/transaction/details/views/TransactionCoordinatorLayout;->h:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    move v2, v1

    goto :goto_0

    .line 95
    :sswitch_0
    iget-object v1, p0, Lco/uk/getmondo/transaction/details/views/TransactionCoordinatorLayout;->h:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->top:I

    div-int/lit8 v3, v5, 0x2

    sub-int/2addr v1, v3

    .line 101
    :goto_1
    iget-object v3, p0, Lco/uk/getmondo/transaction/details/views/TransactionCoordinatorLayout;->i:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->bottom:I

    div-int/lit8 v6, v5, 0x2

    sub-int/2addr v3, v6

    invoke-static {v1, v3}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 104
    invoke-virtual {p0}, Lco/uk/getmondo/transaction/details/views/TransactionCoordinatorLayout;->getPaddingLeft()I

    move-result v3

    iget v6, v0, Landroid/support/design/widget/CoordinatorLayout$d;->leftMargin:I

    add-int/2addr v3, v6

    invoke-virtual {p0}, Lco/uk/getmondo/transaction/details/views/TransactionCoordinatorLayout;->getWidth()I

    move-result v6

    invoke-virtual {p0}, Lco/uk/getmondo/transaction/details/views/TransactionCoordinatorLayout;->getPaddingRight()I

    move-result v7

    sub-int/2addr v6, v7

    sub-int/2addr v6, v4

    iget v7, v0, Landroid/support/design/widget/CoordinatorLayout$d;->rightMargin:I

    sub-int/2addr v6, v7

    invoke-static {v2, v6}, Ljava/lang/Math;->min(II)I

    move-result v2

    invoke-static {v3, v2}, Ljava/lang/Math;->max(II)I

    move-result v2

    .line 105
    invoke-virtual {p0}, Lco/uk/getmondo/transaction/details/views/TransactionCoordinatorLayout;->getPaddingTop()I

    move-result v3

    iget v6, v0, Landroid/support/design/widget/CoordinatorLayout$d;->topMargin:I

    add-int/2addr v3, v6

    invoke-virtual {p0}, Lco/uk/getmondo/transaction/details/views/TransactionCoordinatorLayout;->getHeight()I

    move-result v6

    invoke-virtual {p0}, Lco/uk/getmondo/transaction/details/views/TransactionCoordinatorLayout;->getPaddingBottom()I

    move-result v7

    sub-int/2addr v6, v7

    sub-int/2addr v6, v5

    iget v0, v0, Landroid/support/design/widget/CoordinatorLayout$d;->bottomMargin:I

    sub-int v0, v6, v0

    invoke-static {v1, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    invoke-static {v3, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 107
    add-int v1, v2, v4

    add-int v3, v0, v5

    invoke-virtual {p3, v2, v0, v1, v3}, Landroid/graphics/Rect;->set(IIII)V

    .line 108
    return-void

    .line 96
    :sswitch_1
    iget-object v1, p0, Lco/uk/getmondo/transaction/details/views/TransactionCoordinatorLayout;->h:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->bottom:I

    sub-int/2addr v1, v5

    div-int/lit8 v3, v5, 0x2

    add-int/2addr v1, v3

    goto :goto_1

    .line 88
    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_2
        :pswitch_0
        :pswitch_1
    .end packed-switch

    .line 94
    :sswitch_data_0
    .sparse-switch
        0x30 -> :sswitch_0
        0x50 -> :sswitch_1
    .end sparse-switch
.end method

.method public static final synthetic b(Lco/uk/getmondo/transaction/details/views/TransactionCoordinatorLayout;)Landroid/graphics/Rect;
    .locals 1

    .prologue
    .line 17
    iget-object v0, p0, Lco/uk/getmondo/transaction/details/views/TransactionCoordinatorLayout;->i:Landroid/graphics/Rect;

    return-object v0
.end method

.method public static final synthetic c(Lco/uk/getmondo/transaction/details/views/TransactionCoordinatorLayout;)Landroid/graphics/Rect;
    .locals 1

    .prologue
    .line 17
    iget-object v0, p0, Lco/uk/getmondo/transaction/details/views/TransactionCoordinatorLayout;->j:Landroid/graphics/Rect;

    return-object v0
.end method

.method private final d(Landroid/view/View;Landroid/graphics/Rect;)V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 70
    invoke-virtual {p1}, Landroid/view/View;->isLayoutRequested()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, Landroid/view/View;->getVisibility()I

    move-result v0

    const/16 v1, 0x8

    if-ne v0, v1, :cond_1

    .line 71
    :cond_0
    invoke-virtual {p2, v2, v2, v2, v2}, Landroid/graphics/Rect;->set(IIII)V

    .line 75
    :goto_0
    return-void

    .line 74
    :cond_1
    invoke-virtual {p1}, Landroid/view/View;->getLeft()I

    move-result v0

    invoke-virtual {p1}, Landroid/view/View;->getTop()I

    move-result v1

    invoke-virtual {p1}, Landroid/view/View;->getRight()I

    move-result v2

    invoke-virtual {p1}, Landroid/view/View;->getBottom()I

    move-result v3

    invoke-virtual {p2, v0, v1, v2, v3}, Landroid/graphics/Rect;->set(IIII)V

    goto :goto_0
.end method

.method public static final synthetic d(Lco/uk/getmondo/transaction/details/views/TransactionCoordinatorLayout;)V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Lco/uk/getmondo/transaction/details/views/TransactionCoordinatorLayout;->f()V

    return-void
.end method

.method public static final synthetic e()F
    .locals 1

    .prologue
    .line 17
    sget v0, Lco/uk/getmondo/transaction/details/views/TransactionCoordinatorLayout;->m:F

    return v0
.end method

.method private final e(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 58
    iget-object v0, p0, Lco/uk/getmondo/transaction/details/views/TransactionCoordinatorLayout;->l:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->left:I

    iget-object v1, p0, Lco/uk/getmondo/transaction/details/views/TransactionCoordinatorLayout;->k:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    sub-int/2addr v0, v1

    .line 59
    iget-object v1, p0, Lco/uk/getmondo/transaction/details/views/TransactionCoordinatorLayout;->l:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->top:I

    iget-object v2, p0, Lco/uk/getmondo/transaction/details/views/TransactionCoordinatorLayout;->k:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->top:I

    sub-int/2addr v1, v2

    .line 61
    if-eqz v0, :cond_0

    .line 62
    invoke-virtual {p1, v0}, Landroid/view/View;->offsetLeftAndRight(I)V

    .line 64
    :cond_0
    if-eqz v1, :cond_1

    .line 65
    invoke-virtual {p1, v1}, Landroid/view/View;->offsetTopAndBottom(I)V

    .line 67
    :cond_1
    return-void
.end method

.method public static final synthetic e(Lco/uk/getmondo/transaction/details/views/TransactionCoordinatorLayout;)V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Lco/uk/getmondo/transaction/details/views/TransactionCoordinatorLayout;->g()V

    return-void
.end method

.method private final f()V
    .locals 3

    .prologue
    .line 32
    sget v0, Lco/uk/getmondo/c$a;->transactionActionFab:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/transaction/details/views/TransactionCoordinatorLayout;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/transaction/details/views/VisibilityAwareFloatingActionButton;

    const-string v1, "transactionActionFab"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/view/View;

    iget-object v1, p0, Lco/uk/getmondo/transaction/details/views/TransactionCoordinatorLayout;->k:Landroid/graphics/Rect;

    invoke-direct {p0, v0, v1}, Lco/uk/getmondo/transaction/details/views/TransactionCoordinatorLayout;->d(Landroid/view/View;Landroid/graphics/Rect;)V

    .line 33
    sget v0, Lco/uk/getmondo/c$a;->transactionActionFab:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/transaction/details/views/TransactionCoordinatorLayout;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/transaction/details/views/VisibilityAwareFloatingActionButton;

    const-string v1, "transactionActionFab"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/view/View;

    const v1, 0x800055

    iget-object v2, p0, Lco/uk/getmondo/transaction/details/views/TransactionCoordinatorLayout;->l:Landroid/graphics/Rect;

    invoke-direct {p0, v0, v1, v2}, Lco/uk/getmondo/transaction/details/views/TransactionCoordinatorLayout;->a(Landroid/view/View;ILandroid/graphics/Rect;)V

    .line 34
    sget v0, Lco/uk/getmondo/c$a;->transactionActionFab:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/transaction/details/views/TransactionCoordinatorLayout;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/transaction/details/views/VisibilityAwareFloatingActionButton;

    const-string v1, "transactionActionFab"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/view/View;

    invoke-direct {p0, v0}, Lco/uk/getmondo/transaction/details/views/TransactionCoordinatorLayout;->e(Landroid/view/View;)V

    .line 37
    iget-object v0, p0, Lco/uk/getmondo/transaction/details/views/TransactionCoordinatorLayout;->l:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->top:I

    iget-object v1, p0, Lco/uk/getmondo/transaction/details/views/TransactionCoordinatorLayout;->i:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->bottom:I

    if-gt v0, v1, :cond_0

    .line 38
    sget v0, Lco/uk/getmondo/c$a;->transactionActionFab:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/transaction/details/views/TransactionCoordinatorLayout;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/transaction/details/views/VisibilityAwareFloatingActionButton;

    invoke-virtual {v0}, Lco/uk/getmondo/transaction/details/views/VisibilityAwareFloatingActionButton;->b()V

    .line 41
    :goto_0
    return-void

    .line 40
    :cond_0
    sget v0, Lco/uk/getmondo/c$a;->transactionActionFab:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/transaction/details/views/TransactionCoordinatorLayout;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/transaction/details/views/VisibilityAwareFloatingActionButton;

    invoke-virtual {v0}, Lco/uk/getmondo/transaction/details/views/VisibilityAwareFloatingActionButton;->a()V

    goto :goto_0
.end method

.method private final g()V
    .locals 4

    .prologue
    .line 45
    sget v0, Lco/uk/getmondo/c$a;->transactionAvatarView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/transaction/details/views/TransactionCoordinatorLayout;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/transaction/details/views/AvatarView;

    const-string v1, "transactionAvatarView"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/view/View;

    iget-object v1, p0, Lco/uk/getmondo/transaction/details/views/TransactionCoordinatorLayout;->k:Landroid/graphics/Rect;

    invoke-direct {p0, v0, v1}, Lco/uk/getmondo/transaction/details/views/TransactionCoordinatorLayout;->d(Landroid/view/View;Landroid/graphics/Rect;)V

    .line 46
    sget v0, Lco/uk/getmondo/c$a;->transactionAvatarView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/transaction/details/views/TransactionCoordinatorLayout;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/transaction/details/views/AvatarView;

    const-string v1, "transactionAvatarView"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/view/View;

    const v1, 0x800033

    iget-object v2, p0, Lco/uk/getmondo/transaction/details/views/TransactionCoordinatorLayout;->l:Landroid/graphics/Rect;

    invoke-direct {p0, v0, v1, v2}, Lco/uk/getmondo/transaction/details/views/TransactionCoordinatorLayout;->a(Landroid/view/View;ILandroid/graphics/Rect;)V

    .line 47
    sget v0, Lco/uk/getmondo/c$a;->transactionAvatarView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/transaction/details/views/TransactionCoordinatorLayout;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/transaction/details/views/AvatarView;

    const-string v1, "transactionAvatarView"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/view/View;

    invoke-direct {p0, v0}, Lco/uk/getmondo/transaction/details/views/TransactionCoordinatorLayout;->e(Landroid/view/View;)V

    .line 50
    sget v0, Lco/uk/getmondo/c$a;->transactionAppBarLayout:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/transaction/details/views/TransactionCoordinatorLayout;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/transaction/details/views/TransactionAppBarLayout;

    invoke-virtual {v0}, Lco/uk/getmondo/transaction/details/views/TransactionAppBarLayout;->getTotalScrollRange()I

    move-result v0

    int-to-float v0, v0

    neg-float v0, v0

    sget-object v1, Lco/uk/getmondo/transaction/details/views/TransactionCoordinatorLayout;->f:Lco/uk/getmondo/transaction/details/views/TransactionCoordinatorLayout$a;

    invoke-static {v1}, Lco/uk/getmondo/transaction/details/views/TransactionCoordinatorLayout$a;->a(Lco/uk/getmondo/transaction/details/views/TransactionCoordinatorLayout$a;)F

    move-result v1

    sub-float/2addr v0, v1

    const/4 v1, 0x0

    invoke-static {v0, v1}, Ljava/lang/Math;->min(FF)F

    move-result v1

    .line 51
    sget v0, Lco/uk/getmondo/c$a;->transactionAppBarLayout:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/transaction/details/views/TransactionCoordinatorLayout;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/transaction/details/views/TransactionAppBarLayout;

    invoke-virtual {v0}, Lco/uk/getmondo/transaction/details/views/TransactionAppBarLayout;->getTotalScrollRange()I

    move-result v0

    int-to-float v0, v0

    neg-float v0, v0

    .line 52
    iget-object v2, p0, Lco/uk/getmondo/transaction/details/views/TransactionCoordinatorLayout;->j:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->top:I

    int-to-float v2, v2

    .line 53
    const/high16 v3, 0x3f800000    # 1.0f

    sub-float/2addr v2, v0

    sub-float v0, v1, v0

    div-float v0, v2, v0

    invoke-static {v3, v0}, Ljava/lang/Math;->min(FF)F

    move-result v1

    .line 54
    sget v0, Lco/uk/getmondo/c$a;->transactionAvatarView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/transaction/details/views/TransactionCoordinatorLayout;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/transaction/details/views/AvatarView;

    invoke-virtual {v0, v1}, Lco/uk/getmondo/transaction/details/views/AvatarView;->setAlpha(F)V

    .line 55
    return-void
.end method


# virtual methods
.method public b(I)Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lco/uk/getmondo/transaction/details/views/TransactionCoordinatorLayout;->n:Ljava/util/HashMap;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lco/uk/getmondo/transaction/details/views/TransactionCoordinatorLayout;->n:Ljava/util/HashMap;

    :cond_0
    iget-object v0, p0, Lco/uk/getmondo/transaction/details/views/TransactionCoordinatorLayout;->n:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    if-nez v0, :cond_1

    invoke-virtual {p0, p1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/transaction/details/views/TransactionCoordinatorLayout;->n:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    return-object v0
.end method

.method public onAttachedToWindow()V
    .locals 2

    .prologue
    .line 111
    invoke-super {p0}, Landroid/support/design/widget/CoordinatorLayout;->onAttachedToWindow()V

    .line 112
    invoke-virtual {p0}, Lco/uk/getmondo/transaction/details/views/TransactionCoordinatorLayout;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v1

    iget-object v0, p0, Lco/uk/getmondo/transaction/details/views/TransactionCoordinatorLayout;->g:Lco/uk/getmondo/transaction/details/views/TransactionCoordinatorLayout$b;

    check-cast v0, Landroid/view/ViewTreeObserver$OnPreDrawListener;

    invoke-virtual {v1, v0}, Landroid/view/ViewTreeObserver;->addOnPreDrawListener(Landroid/view/ViewTreeObserver$OnPreDrawListener;)V

    .line 113
    return-void
.end method

.method public onDetachedFromWindow()V
    .locals 2

    .prologue
    .line 116
    invoke-super {p0}, Landroid/support/design/widget/CoordinatorLayout;->onDetachedFromWindow()V

    .line 117
    invoke-virtual {p0}, Lco/uk/getmondo/transaction/details/views/TransactionCoordinatorLayout;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v1

    iget-object v0, p0, Lco/uk/getmondo/transaction/details/views/TransactionCoordinatorLayout;->g:Lco/uk/getmondo/transaction/details/views/TransactionCoordinatorLayout$b;

    check-cast v0, Landroid/view/ViewTreeObserver$OnPreDrawListener;

    invoke-virtual {v1, v0}, Landroid/view/ViewTreeObserver;->removeOnPreDrawListener(Landroid/view/ViewTreeObserver$OnPreDrawListener;)V

    .line 118
    return-void
.end method
