.class final Lco/uk/getmondo/transaction/details/views/TransactionCoordinatorLayout$b;
.super Ljava/lang/Object;
.source "TransactionCoordinatorLayout.kt"

# interfaces
.implements Landroid/view/ViewTreeObserver$OnPreDrawListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lco/uk/getmondo/transaction/details/views/TransactionCoordinatorLayout;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "b"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0000\u0008\u0082\u0004\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0008\u0010\u0003\u001a\u00020\u0004H\u0016\u00a8\u0006\u0005"
    }
    d2 = {
        "Lco/uk/getmondo/transaction/details/views/TransactionCoordinatorLayout$OnPreDrawListener;",
        "Landroid/view/ViewTreeObserver$OnPreDrawListener;",
        "(Lco/uk/getmondo/transaction/details/views/TransactionCoordinatorLayout;)V",
        "onPreDraw",
        "",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field final synthetic a:Lco/uk/getmondo/transaction/details/views/TransactionCoordinatorLayout;


# direct methods
.method public constructor <init>(Lco/uk/getmondo/transaction/details/views/TransactionCoordinatorLayout;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 120
    iput-object p1, p0, Lco/uk/getmondo/transaction/details/views/TransactionCoordinatorLayout$b;->a:Lco/uk/getmondo/transaction/details/views/TransactionCoordinatorLayout;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPreDraw()Z
    .locals 3

    .prologue
    .line 122
    iget-object v1, p0, Lco/uk/getmondo/transaction/details/views/TransactionCoordinatorLayout$b;->a:Lco/uk/getmondo/transaction/details/views/TransactionCoordinatorLayout;

    iget-object v0, p0, Lco/uk/getmondo/transaction/details/views/TransactionCoordinatorLayout$b;->a:Lco/uk/getmondo/transaction/details/views/TransactionCoordinatorLayout;

    sget v2, Lco/uk/getmondo/c$a;->transactionHeaderView:I

    invoke-virtual {v0, v2}, Lco/uk/getmondo/transaction/details/views/TransactionCoordinatorLayout;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/transaction/details/views/HeaderView;

    const-string v2, "transactionHeaderView"

    invoke-static {v0, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/view/View;

    iget-object v2, p0, Lco/uk/getmondo/transaction/details/views/TransactionCoordinatorLayout$b;->a:Lco/uk/getmondo/transaction/details/views/TransactionCoordinatorLayout;

    invoke-static {v2}, Lco/uk/getmondo/transaction/details/views/TransactionCoordinatorLayout;->a(Lco/uk/getmondo/transaction/details/views/TransactionCoordinatorLayout;)Landroid/graphics/Rect;

    move-result-object v2

    invoke-static {v1, v0, v2}, Lco/uk/getmondo/common/k/q;->a(Landroid/view/ViewGroup;Landroid/view/View;Landroid/graphics/Rect;)V

    .line 123
    iget-object v1, p0, Lco/uk/getmondo/transaction/details/views/TransactionCoordinatorLayout$b;->a:Lco/uk/getmondo/transaction/details/views/TransactionCoordinatorLayout;

    iget-object v0, p0, Lco/uk/getmondo/transaction/details/views/TransactionCoordinatorLayout$b;->a:Lco/uk/getmondo/transaction/details/views/TransactionCoordinatorLayout;

    sget v2, Lco/uk/getmondo/c$a;->toolbar:I

    invoke-virtual {v0, v2}, Lco/uk/getmondo/transaction/details/views/TransactionCoordinatorLayout;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/Toolbar;

    const-string v2, "toolbar"

    invoke-static {v0, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/view/View;

    iget-object v2, p0, Lco/uk/getmondo/transaction/details/views/TransactionCoordinatorLayout$b;->a:Lco/uk/getmondo/transaction/details/views/TransactionCoordinatorLayout;

    invoke-static {v2}, Lco/uk/getmondo/transaction/details/views/TransactionCoordinatorLayout;->b(Lco/uk/getmondo/transaction/details/views/TransactionCoordinatorLayout;)Landroid/graphics/Rect;

    move-result-object v2

    invoke-static {v1, v0, v2}, Lco/uk/getmondo/common/k/q;->a(Landroid/view/ViewGroup;Landroid/view/View;Landroid/graphics/Rect;)V

    .line 124
    iget-object v1, p0, Lco/uk/getmondo/transaction/details/views/TransactionCoordinatorLayout$b;->a:Lco/uk/getmondo/transaction/details/views/TransactionCoordinatorLayout;

    iget-object v0, p0, Lco/uk/getmondo/transaction/details/views/TransactionCoordinatorLayout$b;->a:Lco/uk/getmondo/transaction/details/views/TransactionCoordinatorLayout;

    sget v2, Lco/uk/getmondo/c$a;->transactionAppBarLayout:I

    invoke-virtual {v0, v2}, Lco/uk/getmondo/transaction/details/views/TransactionCoordinatorLayout;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/transaction/details/views/TransactionAppBarLayout;

    const-string v2, "transactionAppBarLayout"

    invoke-static {v0, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/view/View;

    iget-object v2, p0, Lco/uk/getmondo/transaction/details/views/TransactionCoordinatorLayout$b;->a:Lco/uk/getmondo/transaction/details/views/TransactionCoordinatorLayout;

    invoke-static {v2}, Lco/uk/getmondo/transaction/details/views/TransactionCoordinatorLayout;->c(Lco/uk/getmondo/transaction/details/views/TransactionCoordinatorLayout;)Landroid/graphics/Rect;

    move-result-object v2

    invoke-static {v1, v0, v2}, Lco/uk/getmondo/common/k/q;->a(Landroid/view/ViewGroup;Landroid/view/View;Landroid/graphics/Rect;)V

    .line 126
    iget-object v0, p0, Lco/uk/getmondo/transaction/details/views/TransactionCoordinatorLayout$b;->a:Lco/uk/getmondo/transaction/details/views/TransactionCoordinatorLayout;

    invoke-static {v0}, Lco/uk/getmondo/transaction/details/views/TransactionCoordinatorLayout;->d(Lco/uk/getmondo/transaction/details/views/TransactionCoordinatorLayout;)V

    .line 127
    iget-object v0, p0, Lco/uk/getmondo/transaction/details/views/TransactionCoordinatorLayout$b;->a:Lco/uk/getmondo/transaction/details/views/TransactionCoordinatorLayout;

    invoke-static {v0}, Lco/uk/getmondo/transaction/details/views/TransactionCoordinatorLayout;->e(Lco/uk/getmondo/transaction/details/views/TransactionCoordinatorLayout;)V

    .line 129
    const/4 v0, 0x1

    return v0
.end method
