.class public final Lco/uk/getmondo/transaction/details/views/AvatarView$c;
.super Lco/uk/getmondo/common/ui/c;
.source "AvatarView.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lco/uk/getmondo/transaction/details/views/AvatarView;->a(Lco/uk/getmondo/transaction/details/b/d;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000\u001f\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000*\u0001\u0000\u0008\n\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\"\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\u0010\u0010\u0007\u001a\u000c\u0012\u0006\u0008\u0000\u0012\u00020\u0006\u0018\u00010\u0008H\u0016\u00a8\u0006\t"
    }
    d2 = {
        "co/uk/getmondo/transaction/details/views/AvatarView$bind$2",
        "Lco/uk/getmondo/common/ui/CircularViewTarget;",
        "(Lco/uk/getmondo/transaction/details/views/AvatarView;Landroid/widget/ImageView;)V",
        "onResourceReady",
        "",
        "bitmap",
        "Landroid/graphics/Bitmap;",
        "glideAnimation",
        "Lcom/bumptech/glide/request/animation/GlideAnimation;",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field final synthetic a:Lco/uk/getmondo/transaction/details/views/AvatarView;


# direct methods
.method constructor <init>(Lco/uk/getmondo/transaction/details/views/AvatarView;Landroid/widget/ImageView;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/ImageView;",
            ")V"
        }
    .end annotation

    .prologue
    .line 87
    iput-object p1, p0, Lco/uk/getmondo/transaction/details/views/AvatarView$c;->a:Lco/uk/getmondo/transaction/details/views/AvatarView;

    invoke-direct {p0, p2}, Lco/uk/getmondo/common/ui/c;-><init>(Landroid/widget/ImageView;)V

    return-void
.end method


# virtual methods
.method public a(Landroid/graphics/Bitmap;Lcom/bumptech/glide/g/a/c;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/graphics/Bitmap;",
            "Lcom/bumptech/glide/g/a/c",
            "<-",
            "Landroid/graphics/Bitmap;",
            ">;)V"
        }
    .end annotation

    .prologue
    const-string v0, "bitmap"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 89
    invoke-super {p0, p1, p2}, Lco/uk/getmondo/common/ui/c;->a(Ljava/lang/Object;Lcom/bumptech/glide/g/a/c;)V

    .line 90
    iget-object v0, p0, Lco/uk/getmondo/transaction/details/views/AvatarView$c;->a:Lco/uk/getmondo/transaction/details/views/AvatarView;

    invoke-virtual {v0}, Lco/uk/getmondo/transaction/details/views/AvatarView;->getBackgroundListener()Lkotlin/d/a/m;

    move-result-object v0

    if-eqz v0, :cond_0

    new-instance v1, Landroid/graphics/drawable/BitmapDrawable;

    iget-object v2, p0, Lco/uk/getmondo/transaction/details/views/AvatarView$c;->a:Lco/uk/getmondo/transaction/details/views/AvatarView;

    invoke-virtual {v2}, Lco/uk/getmondo/transaction/details/views/AvatarView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-direct {v1, v2, p1}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lkotlin/d/a/m;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/n;

    .line 91
    :cond_0
    return-void
.end method

.method public bridge synthetic a(Ljava/lang/Object;Lcom/bumptech/glide/g/a/c;)V
    .locals 0

    .prologue
    .line 87
    check-cast p1, Landroid/graphics/Bitmap;

    invoke-virtual {p0, p1, p2}, Lco/uk/getmondo/transaction/details/views/AvatarView$c;->a(Landroid/graphics/Bitmap;Lcom/bumptech/glide/g/a/c;)V

    return-void
.end method
