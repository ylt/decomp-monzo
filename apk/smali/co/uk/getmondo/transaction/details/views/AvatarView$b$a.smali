.class final Lco/uk/getmondo/transaction/details/views/AvatarView$b$a;
.super Ljava/lang/Object;
.source "AvatarView.kt"

# interfaces
.implements Landroid/support/v7/d/b$c;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lco/uk/getmondo/transaction/details/views/AvatarView$b;->a(Landroid/graphics/Bitmap;Lcom/bumptech/glide/g/a/c;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "",
        "palette",
        "Landroid/support/v7/graphics/Palette;",
        "kotlin.jvm.PlatformType",
        "onGenerated"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field final synthetic a:Lco/uk/getmondo/transaction/details/views/AvatarView$b;


# direct methods
.method constructor <init>(Lco/uk/getmondo/transaction/details/views/AvatarView$b;)V
    .locals 0

    iput-object p1, p0, Lco/uk/getmondo/transaction/details/views/AvatarView$b$a;->a:Lco/uk/getmondo/transaction/details/views/AvatarView$b;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/support/v7/d/b;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 72
    iget-object v0, p0, Lco/uk/getmondo/transaction/details/views/AvatarView$b$a;->a:Lco/uk/getmondo/transaction/details/views/AvatarView$b;

    iget-object v0, v0, Lco/uk/getmondo/transaction/details/views/AvatarView$b;->a:Lco/uk/getmondo/transaction/details/views/AvatarView;

    invoke-virtual {v0}, Lco/uk/getmondo/transaction/details/views/AvatarView;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0f006e

    invoke-static {v0, v1}, Landroid/support/v4/content/a;->c(Landroid/content/Context;I)I

    move-result v0

    .line 73
    invoke-virtual {p1, v0}, Landroid/support/v7/d/b;->a(I)I

    move-result v0

    .line 74
    iget-object v1, p0, Lco/uk/getmondo/transaction/details/views/AvatarView$b$a;->a:Lco/uk/getmondo/transaction/details/views/AvatarView$b;

    iget-object v1, v1, Lco/uk/getmondo/transaction/details/views/AvatarView$b;->b:Ljava/lang/Integer;

    if-eqz v1, :cond_1

    .line 75
    iget-object v0, p0, Lco/uk/getmondo/transaction/details/views/AvatarView$b$a;->a:Lco/uk/getmondo/transaction/details/views/AvatarView$b;

    iget-object v0, v0, Lco/uk/getmondo/transaction/details/views/AvatarView$b;->a:Lco/uk/getmondo/transaction/details/views/AvatarView;

    invoke-virtual {v0}, Lco/uk/getmondo/transaction/details/views/AvatarView;->getBackgroundListener()Lkotlin/d/a/m;

    move-result-object v0

    if-eqz v0, :cond_0

    new-instance v1, Landroid/graphics/drawable/ColorDrawable;

    iget-object v2, p0, Lco/uk/getmondo/transaction/details/views/AvatarView$b$a;->a:Lco/uk/getmondo/transaction/details/views/AvatarView$b;

    iget-object v2, v2, Lco/uk/getmondo/transaction/details/views/AvatarView$b;->b:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-direct {v1, v2}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lkotlin/d/a/m;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/n;

    .line 78
    :cond_0
    :goto_0
    return-void

    .line 77
    :cond_1
    iget-object v1, p0, Lco/uk/getmondo/transaction/details/views/AvatarView$b$a;->a:Lco/uk/getmondo/transaction/details/views/AvatarView$b;

    iget-object v1, v1, Lco/uk/getmondo/transaction/details/views/AvatarView$b;->a:Lco/uk/getmondo/transaction/details/views/AvatarView;

    invoke-virtual {v1}, Lco/uk/getmondo/transaction/details/views/AvatarView;->getBackgroundListener()Lkotlin/d/a/m;

    move-result-object v1

    if-eqz v1, :cond_0

    new-instance v2, Landroid/graphics/drawable/ColorDrawable;

    const/high16 v3, 0x3f000000    # 0.5f

    invoke-static {v0, v3}, Lco/uk/getmondo/common/k/c;->a(IF)I

    move-result v0

    invoke-direct {v2, v0}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-interface {v1, v2, v0}, Lkotlin/d/a/m;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/n;

    goto :goto_0
.end method
