.class public final Lco/uk/getmondo/transaction/details/views/HeaderView;
.super Landroid/support/constraint/ConstraintLayout;
.source "HeaderView.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000Z\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0010\u0002\n\u0002\u0008\u0004\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0004\u0018\u00002\u00020\u0001B%\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\n\u0008\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u0012\u0008\u0008\u0002\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\u0008J\u0008\u0010\u0012\u001a\u00020\u0013H\u0002J\u0010\u0010\u0014\u001a\u00020\u00132\u0006\u0010\t\u001a\u00020\nH\u0002J\u000e\u0010\u0015\u001a\u00020\u00132\u0006\u0010\t\u001a\u00020\nJ\u001a\u0010\u0016\u001a\u00020\u00132\u0008\u0010\u0017\u001a\u0004\u0018\u00010\u00182\u0006\u0010\u0019\u001a\u00020\u001aH\u0002J\u0006\u0010\u001b\u001a\u00020\u0007J \u0010\u001c\u001a\u00020\u0013*\u00020\u001d2\u0008\u0010\u001e\u001a\u0004\u0018\u00010\u001f2\u0008\u0008\u0001\u0010 \u001a\u00020\u0007H\u0002J\u001e\u0010!\u001a\u00020\u0013*\u00020\"2\u0008\u0010#\u001a\u0004\u0018\u00010\u00182\u0006\u0010$\u001a\u00020\u001aH\u0002J\u0016\u0010%\u001a\u00020\u0013*\u00020\"2\u0008\u0010#\u001a\u0004\u0018\u00010\u0018H\u0002R\u0010\u0010\t\u001a\u0004\u0018\u00010\nX\u0082\u000e\u00a2\u0006\u0002\n\u0000R(\u0010\u000c\u001a\u00020\u0007*\u00020\r2\u0006\u0010\u000b\u001a\u00020\u00078B@BX\u0082\u000e\u00a2\u0006\u000c\u001a\u0004\u0008\u000e\u0010\u000f\"\u0004\u0008\u0010\u0010\u0011\u00a8\u0006&"
    }
    d2 = {
        "Lco/uk/getmondo/transaction/details/views/HeaderView;",
        "Landroid/support/constraint/ConstraintLayout;",
        "context",
        "Landroid/content/Context;",
        "attrs",
        "Landroid/util/AttributeSet;",
        "defStyleAttr",
        "",
        "(Landroid/content/Context;Landroid/util/AttributeSet;I)V",
        "header",
        "Lco/uk/getmondo/transaction/details/base/BaseHeader;",
        "value",
        "topMargin",
        "Landroid/view/View;",
        "getTopMargin",
        "(Landroid/view/View;)I",
        "setTopMargin",
        "(Landroid/view/View;I)V",
        "adjustAmountInfoMargin",
        "",
        "adjustSubtitleMargin",
        "bind",
        "setSubtitleText",
        "subtitleText",
        "",
        "subtitleTextAppearance",
        "Lco/uk/getmondo/transaction/details/base/TextAppearance;",
        "titleScrollDistance",
        "setAmountAndStyleOrHide",
        "Lco/uk/getmondo/common/ui/AmountView;",
        "amount",
        "Lco/uk/getmondo/model/Amount;",
        "style",
        "setTextAndAppearanceOrHide",
        "Landroid/widget/TextView;",
        "text",
        "textAppearance",
        "setTextOrHide",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field private c:Lco/uk/getmondo/transaction/details/b/d;

.field private d:Ljava/util/HashMap;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 6

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x6

    move-object v0, p0

    move-object v1, p1

    move-object v5, v2

    invoke-direct/range {v0 .. v5}, Lco/uk/getmondo/transaction/details/views/HeaderView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/d/b/i;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 6

    const/4 v3, 0x0

    const/4 v4, 0x4

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    invoke-direct/range {v0 .. v5}, Lco/uk/getmondo/transaction/details/views/HeaderView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/d/b/i;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    .prologue
    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 31
    invoke-direct {p0, p1, p2, p3}, Landroid/support/constraint/ConstraintLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 35
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f050138

    check-cast p0, Landroid/view/ViewGroup;

    invoke-virtual {v0, v1, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    return-void
.end method

.method public synthetic constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/d/b/i;)V
    .locals 2

    .prologue
    and-int/lit8 v0, p4, 0x2

    if-eqz v0, :cond_1

    .line 29
    const/4 v0, 0x0

    check-cast v0, Landroid/util/AttributeSet;

    :goto_0
    and-int/lit8 v1, p4, 0x4

    if-eqz v1, :cond_0

    .line 30
    const/4 p3, 0x0

    :cond_0
    invoke-direct {p0, p1, v0, p3}, Lco/uk/getmondo/transaction/details/views/HeaderView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void

    :cond_1
    move-object v0, p2

    goto :goto_0
.end method

.method private final a(Landroid/view/View;I)V
    .locals 2

    .prologue
    .line 106
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    if-nez v0, :cond_0

    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type android.view.ViewGroup.MarginLayoutParams"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 107
    iput p2, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    .line 108
    check-cast v0, Landroid/view/ViewGroup$LayoutParams;

    invoke-virtual {p1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 109
    return-void
.end method

.method private final a(Landroid/widget/TextView;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 120
    if-nez p2, :cond_0

    .line 121
    check-cast p1, Landroid/view/View;

    invoke-static {p1}, Lco/uk/getmondo/common/ae;->b(Landroid/view/View;)V

    .line 124
    :goto_0
    return-void

    .line 123
    :cond_0
    check-cast p2, Ljava/lang/CharSequence;

    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method private final a(Landroid/widget/TextView;Ljava/lang/String;Lco/uk/getmondo/transaction/details/b/j;)V
    .locals 2

    .prologue
    .line 128
    invoke-direct {p0, p1, p2}, Lco/uk/getmondo/transaction/details/views/HeaderView;->a(Landroid/widget/TextView;Ljava/lang/String;)V

    .line 129
    invoke-virtual {p3}, Lco/uk/getmondo/transaction/details/b/j;->b()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setTextColor(I)V

    .line 130
    const/4 v0, 0x2

    invoke-virtual {p3}, Lco/uk/getmondo/transaction/details/b/j;->a()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 131
    invoke-virtual {p1}, Landroid/widget/TextView;->getTypeface()Landroid/graphics/Typeface;

    move-result-object v0

    invoke-virtual {p3}, Lco/uk/getmondo/transaction/details/b/j;->c()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;I)V

    .line 132
    return-void
.end method

.method private final a(Lco/uk/getmondo/common/ui/AmountView;Lco/uk/getmondo/d/c;I)V
    .locals 0

    .prologue
    .line 112
    if-nez p2, :cond_0

    .line 113
    check-cast p1, Landroid/view/View;

    invoke-static {p1}, Lco/uk/getmondo/common/ae;->b(Landroid/view/View;)V

    .line 116
    :goto_0
    return-void

    .line 115
    :cond_0
    invoke-virtual {p1, p3, p2}, Lco/uk/getmondo/common/ui/AmountView;->a(ILco/uk/getmondo/d/c;)V

    goto :goto_0
.end method

.method private final a(Ljava/lang/String;Lco/uk/getmondo/transaction/details/b/j;)V
    .locals 6

    .prologue
    .line 73
    sget v0, Lco/uk/getmondo/c$a;->transactionSubtitleTextView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/transaction/details/views/HeaderView;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-direct {p0, v0, p1, p2}, Lco/uk/getmondo/transaction/details/views/HeaderView;->a(Landroid/widget/TextView;Ljava/lang/String;Lco/uk/getmondo/transaction/details/b/j;)V

    .line 74
    sget v0, Lco/uk/getmondo/c$a;->transactionSubtitleTextView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/transaction/details/views/HeaderView;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Landroid/text/util/Linkify;->addLinks(Landroid/widget/TextView;I)Z

    move-result v0

    .line 75
    if-eqz v0, :cond_2

    .line 76
    sget v0, Lco/uk/getmondo/c$a;->transactionSubtitleTextView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/transaction/details/views/HeaderView;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    if-nez v0, :cond_0

    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type android.text.Spannable"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    check-cast v0, Landroid/text/Spannable;

    invoke-static {v0}, Lco/uk/getmondo/common/k/p;->a(Landroid/text/Spannable;)Landroid/text/Spannable;

    move-result-object v1

    .line 77
    if-nez p1, :cond_1

    invoke-static {}, Lkotlin/d/b/l;->a()V

    :cond_1
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    .line 78
    const/16 v2, 0x21

    .line 79
    invoke-virtual {p2}, Lco/uk/getmondo/transaction/details/b/j;->d()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p2}, Lco/uk/getmondo/transaction/details/b/j;->c()I

    move-result v4

    invoke-static {v3, v4}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v3

    .line 80
    new-instance v4, Lco/uk/getmondo/common/ui/k;

    const-string v5, "typeface"

    invoke-static {v3, v5}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v4, v3}, Lco/uk/getmondo/common/ui/k;-><init>(Landroid/graphics/Typeface;)V

    const/4 v3, 0x0

    invoke-interface {v1, v4, v3, v0, v2}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    .line 81
    sget v0, Lco/uk/getmondo/c$a;->transactionSubtitleTextView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/transaction/details/views/HeaderView;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 83
    :cond_2
    return-void
.end method

.method private final b(Lco/uk/getmondo/transaction/details/b/d;)V
    .locals 2

    .prologue
    .line 92
    instance-of v0, p1, Lco/uk/getmondo/transaction/details/d/f/a;

    if-eqz v0, :cond_0

    .line 93
    sget v0, Lco/uk/getmondo/c$a;->transactionSubtitleTextView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/transaction/details/views/HeaderView;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lco/uk/getmondo/transaction/details/views/HeaderView;->a(Landroid/view/View;I)V

    .line 95
    :cond_0
    return-void
.end method

.method private final d()V
    .locals 3

    .prologue
    .line 98
    sget v0, Lco/uk/getmondo/c$a;->transactionAmountView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/transaction/details/views/HeaderView;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/common/ui/AmountView;

    invoke-virtual {v0}, Lco/uk/getmondo/common/ui/AmountView;->getVisibility()I

    move-result v0

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    .line 99
    sget v0, Lco/uk/getmondo/c$a;->transactionLocalAmountTextView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/transaction/details/views/HeaderView;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {p0}, Lco/uk/getmondo/transaction/details/views/HeaderView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0113

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    invoke-direct {p0, v0, v1}, Lco/uk/getmondo/transaction/details/views/HeaderView;->a(Landroid/view/View;I)V

    .line 101
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(Lco/uk/getmondo/transaction/details/b/d;)V
    .locals 5

    .prologue
    const-string v0, "header"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 39
    iput-object p1, p0, Lco/uk/getmondo/transaction/details/views/HeaderView;->c:Lco/uk/getmondo/transaction/details/b/d;

    .line 41
    invoke-virtual {p0}, Lco/uk/getmondo/transaction/details/views/HeaderView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const-string v1, "resources"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1, v0}, Lco/uk/getmondo/transaction/details/b/d;->c(Landroid/content/res/Resources;)Ljava/lang/String;

    move-result-object v2

    .line 42
    invoke-virtual {p0}, Lco/uk/getmondo/transaction/details/views/HeaderView;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "context"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1, v0}, Lco/uk/getmondo/transaction/details/b/d;->a(Landroid/content/Context;)Lco/uk/getmondo/transaction/details/b/j;

    move-result-object v0

    .line 44
    invoke-virtual {p0}, Lco/uk/getmondo/transaction/details/views/HeaderView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const-string v3, "resources"

    invoke-static {v1, v3}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1, v1}, Lco/uk/getmondo/transaction/details/b/d;->a(Landroid/content/res/Resources;)Ljava/lang/String;

    move-result-object v3

    .line 45
    invoke-virtual {p0}, Lco/uk/getmondo/transaction/details/views/HeaderView;->getContext()Landroid/content/Context;

    move-result-object v1

    const-string v4, "context"

    invoke-static {v1, v4}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1, v1}, Lco/uk/getmondo/transaction/details/b/d;->b(Landroid/content/Context;)Lco/uk/getmondo/transaction/details/b/j;

    move-result-object v1

    .line 48
    invoke-virtual {p1}, Lco/uk/getmondo/transaction/details/b/d;->g()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 52
    nop

    .line 49
    nop

    .line 57
    nop

    .line 54
    nop

    move-object v4, v3

    move-object v3, v1

    move-object v1, v0

    .line 60
    :goto_0
    sget v0, Lco/uk/getmondo/c$a;->transactionTitleTextView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/transaction/details/views/HeaderView;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-direct {p0, v0, v4, v3}, Lco/uk/getmondo/transaction/details/views/HeaderView;->a(Landroid/widget/TextView;Ljava/lang/String;Lco/uk/getmondo/transaction/details/b/j;)V

    .line 61
    invoke-direct {p0, v2, v1}, Lco/uk/getmondo/transaction/details/views/HeaderView;->a(Ljava/lang/String;Lco/uk/getmondo/transaction/details/b/j;)V

    .line 62
    sget v0, Lco/uk/getmondo/c$a;->transactionDateTextView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/transaction/details/views/HeaderView;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {p1}, Lco/uk/getmondo/transaction/details/b/d;->a()Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 63
    sget v0, Lco/uk/getmondo/c$a;->transactionAmountView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/transaction/details/views/HeaderView;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/common/ui/AmountView;

    invoke-virtual {p1}, Lco/uk/getmondo/transaction/details/b/d;->b()Lco/uk/getmondo/d/c;

    move-result-object v1

    invoke-virtual {p1}, Lco/uk/getmondo/transaction/details/b/d;->f()I

    move-result v2

    invoke-direct {p0, v0, v1, v2}, Lco/uk/getmondo/transaction/details/views/HeaderView;->a(Lco/uk/getmondo/common/ui/AmountView;Lco/uk/getmondo/d/c;I)V

    .line 64
    sget v0, Lco/uk/getmondo/c$a;->transactionLocalAmountTextView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/transaction/details/views/HeaderView;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {p1}, Lco/uk/getmondo/transaction/details/b/d;->e()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lco/uk/getmondo/transaction/details/views/HeaderView;->a(Landroid/widget/TextView;Ljava/lang/String;)V

    .line 65
    invoke-virtual {p1}, Lco/uk/getmondo/transaction/details/b/d;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    sget v0, Lco/uk/getmondo/c$a;->transactionDeclinedTextView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/transaction/details/views/HeaderView;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-static {v0}, Lco/uk/getmondo/common/ae;->a(Landroid/view/View;)V

    .line 66
    :goto_1
    sget v0, Lco/uk/getmondo/c$a;->transactionExtraInformationTextView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/transaction/details/views/HeaderView;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {p0}, Lco/uk/getmondo/transaction/details/views/HeaderView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const-string v2, "resources"

    invoke-static {v1, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1, v1}, Lco/uk/getmondo/transaction/details/b/d;->b(Landroid/content/res/Resources;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lco/uk/getmondo/transaction/details/views/HeaderView;->a(Landroid/widget/TextView;Ljava/lang/String;)V

    .line 67
    sget v0, Lco/uk/getmondo/c$a;->transactionDeclinedExtraInformationTextView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/transaction/details/views/HeaderView;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {p1}, Lco/uk/getmondo/transaction/details/b/d;->d()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lco/uk/getmondo/transaction/details/views/HeaderView;->a(Landroid/widget/TextView;Ljava/lang/String;)V

    .line 68
    invoke-direct {p0, p1}, Lco/uk/getmondo/transaction/details/views/HeaderView;->b(Lco/uk/getmondo/transaction/details/b/d;)V

    .line 69
    invoke-direct {p0}, Lco/uk/getmondo/transaction/details/views/HeaderView;->d()V

    .line 70
    return-void

    .line 65
    :cond_0
    sget v0, Lco/uk/getmondo/c$a;->transactionDeclinedTextView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/transaction/details/views/HeaderView;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-static {v0}, Lco/uk/getmondo/common/ae;->b(Landroid/view/View;)V

    goto :goto_1

    :cond_1
    move-object v4, v2

    move-object v2, v3

    move-object v3, v0

    goto/16 :goto_0
.end method

.method public b(I)Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lco/uk/getmondo/transaction/details/views/HeaderView;->d:Ljava/util/HashMap;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lco/uk/getmondo/transaction/details/views/HeaderView;->d:Ljava/util/HashMap;

    :cond_0
    iget-object v0, p0, Lco/uk/getmondo/transaction/details/views/HeaderView;->d:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    if-nez v0, :cond_1

    invoke-virtual {p0, p1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/transaction/details/views/HeaderView;->d:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    return-object v0
.end method

.method public final c()I
    .locals 2

    .prologue
    .line 86
    iget-object v0, p0, Lco/uk/getmondo/transaction/details/views/HeaderView;->c:Lco/uk/getmondo/transaction/details/b/d;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lco/uk/getmondo/transaction/details/b/d;->g()Z

    move-result v1

    .line 87
    sget-object v0, Lco/uk/getmondo/transaction/details/views/HeaderView$a;->a:Lco/uk/getmondo/transaction/details/views/HeaderView$a;

    check-cast v0, Lkotlin/d/a/b;

    .line 88
    if-eqz v1, :cond_1

    sget v1, Lco/uk/getmondo/c$a;->transactionSubtitleTextView:I

    invoke-virtual {p0, v1}, Lco/uk/getmondo/transaction/details/views/HeaderView;->b(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    invoke-interface {v0, v1}, Lkotlin/d/a/b;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Number;

    invoke-virtual {v0}, Ljava/lang/Number;->intValue()I

    move-result v0

    :goto_0
    return v0

    .line 86
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 88
    :cond_1
    sget v1, Lco/uk/getmondo/c$a;->transactionTitleTextView:I

    invoke-virtual {p0, v1}, Lco/uk/getmondo/transaction/details/views/HeaderView;->b(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    invoke-interface {v0, v1}, Lkotlin/d/a/b;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Number;

    invoke-virtual {v0}, Ljava/lang/Number;->intValue()I

    move-result v0

    goto :goto_0
.end method
