.class final Lco/uk/getmondo/transaction/details/views/TransactionAppBarLayout$c;
.super Ljava/lang/Object;
.source "TransactionAppBarLayout.kt"

# interfaces
.implements Lcom/google/android/gms/maps/OnMapReadyCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lco/uk/getmondo/transaction/details/views/TransactionAppBarLayout;->a(DDLjava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "",
        "googleMap",
        "Lcom/google/android/gms/maps/GoogleMap;",
        "kotlin.jvm.PlatformType",
        "onMapReady"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field final synthetic a:Lco/uk/getmondo/transaction/details/views/TransactionAppBarLayout;

.field final synthetic b:D

.field final synthetic c:D

.field final synthetic d:Ljava/lang/String;


# direct methods
.method constructor <init>(Lco/uk/getmondo/transaction/details/views/TransactionAppBarLayout;DDLjava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lco/uk/getmondo/transaction/details/views/TransactionAppBarLayout$c;->a:Lco/uk/getmondo/transaction/details/views/TransactionAppBarLayout;

    iput-wide p2, p0, Lco/uk/getmondo/transaction/details/views/TransactionAppBarLayout$c;->b:D

    iput-wide p4, p0, Lco/uk/getmondo/transaction/details/views/TransactionAppBarLayout$c;->c:D

    iput-object p6, p0, Lco/uk/getmondo/transaction/details/views/TransactionAppBarLayout$c;->d:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onMapReady(Lcom/google/android/gms/maps/GoogleMap;)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 70
    new-instance v0, Lcom/google/android/gms/maps/model/LatLng;

    iget-wide v2, p0, Lco/uk/getmondo/transaction/details/views/TransactionAppBarLayout$c;->b:D

    iget-wide v4, p0, Lco/uk/getmondo/transaction/details/views/TransactionAppBarLayout$c;->c:D

    invoke-direct {v0, v2, v3, v4, v5}, Lcom/google/android/gms/maps/model/LatLng;-><init>(DD)V

    .line 71
    invoke-virtual {p1}, Lcom/google/android/gms/maps/GoogleMap;->getUiSettings()Lcom/google/android/gms/maps/UiSettings;

    move-result-object v1

    invoke-virtual {v1, v6}, Lcom/google/android/gms/maps/UiSettings;->setMapToolbarEnabled(Z)V

    .line 72
    invoke-virtual {p1, v6, v6, v6, v6}, Lcom/google/android/gms/maps/GoogleMap;->setPadding(IIII)V

    .line 73
    invoke-static {v0}, Lcom/google/android/gms/maps/CameraUpdateFactory;->newLatLng(Lcom/google/android/gms/maps/model/LatLng;)Lcom/google/android/gms/maps/CameraUpdate;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/google/android/gms/maps/GoogleMap;->moveCamera(Lcom/google/android/gms/maps/CameraUpdate;)V

    .line 74
    new-instance v1, Lcom/google/android/gms/maps/model/MarkerOptions;

    invoke-direct {v1}, Lcom/google/android/gms/maps/model/MarkerOptions;-><init>()V

    invoke-virtual {v1, v0}, Lcom/google/android/gms/maps/model/MarkerOptions;->position(Lcom/google/android/gms/maps/model/LatLng;)Lcom/google/android/gms/maps/model/MarkerOptions;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/transaction/details/views/TransactionAppBarLayout$c;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/maps/model/MarkerOptions;->title(Ljava/lang/String;)Lcom/google/android/gms/maps/model/MarkerOptions;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/google/android/gms/maps/GoogleMap;->addMarker(Lcom/google/android/gms/maps/model/MarkerOptions;)Lcom/google/android/gms/maps/model/Marker;

    .line 75
    iget-object v0, p0, Lco/uk/getmondo/transaction/details/views/TransactionAppBarLayout$c;->a:Lco/uk/getmondo/transaction/details/views/TransactionAppBarLayout;

    invoke-virtual {v0}, Lco/uk/getmondo/transaction/details/views/TransactionAppBarLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b011b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    invoke-virtual {p1, v0, v6, v6, v6}, Lcom/google/android/gms/maps/GoogleMap;->setPadding(IIII)V

    .line 76
    return-void
.end method
