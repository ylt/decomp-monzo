.class public final Lco/uk/getmondo/transaction/details/d/h/b;
.super Ljava/lang/Object;
.source "TopUpHistory.kt"

# interfaces
.implements Lco/uk/getmondo/transaction/details/b/n;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000,\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0012\u0010\n\u001a\u0004\u0018\u00010\u000b2\u0006\u0010\u000c\u001a\u00020\rH\u0016J\u0010\u0010\u000e\u001a\u00020\u000b2\u0006\u0010\u000c\u001a\u00020\rH\u0016R\u001a\u0010\u0005\u001a\u0008\u0012\u0004\u0012\u00020\u00070\u00068VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0008\u0010\tR\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000f"
    }
    d2 = {
        "Lco/uk/getmondo/transaction/details/types/topup/TopUpHistory;",
        "Lco/uk/getmondo/transaction/details/base/YourHistory;",
        "transactionHistory",
        "Lco/uk/getmondo/transaction/details/model/AverageSpendingHistory;",
        "(Lco/uk/getmondo/transaction/details/model/AverageSpendingHistory;)V",
        "items",
        "",
        "Lco/uk/getmondo/transaction/details/base/YourHistoryItem;",
        "getItems",
        "()Ljava/util/List;",
        "searchQuery",
        "",
        "resources",
        "Landroid/content/res/Resources;",
        "title",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field private final a:Lco/uk/getmondo/transaction/details/c/a;


# direct methods
.method public constructor <init>(Lco/uk/getmondo/transaction/details/c/a;)V
    .locals 1

    .prologue
    const-string v0, "transactionHistory"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lco/uk/getmondo/transaction/details/d/h/b;->a:Lco/uk/getmondo/transaction/details/c/a;

    return-void
.end method

.method public static final synthetic a(Lco/uk/getmondo/transaction/details/d/h/b;)Lco/uk/getmondo/transaction/details/c/a;
    .locals 1

    .prologue
    .line 9
    iget-object v0, p0, Lco/uk/getmondo/transaction/details/d/h/b;->a:Lco/uk/getmondo/transaction/details/c/a;

    return-object v0
.end method


# virtual methods
.method public a(Landroid/content/res/Resources;)Ljava/lang/String;
    .locals 2

    .prologue
    const-string v0, "resources"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 11
    const v0, 0x7f0a045a

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "resources.getString(R.string.your_topup_history)"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public a()Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lco/uk/getmondo/transaction/details/b/o;",
            ">;"
        }
    .end annotation

    .prologue
    .line 15
    iget-object v0, p0, Lco/uk/getmondo/transaction/details/d/h/b;->a:Lco/uk/getmondo/transaction/details/c/a;

    invoke-virtual {v0}, Lco/uk/getmondo/transaction/details/c/a;->a()J

    move-result-wide v0

    const-wide/16 v2, 0x1

    cmp-long v0, v0, v2

    if-gtz v0, :cond_0

    .line 16
    invoke-static {}, Lkotlin/a/m;->a()Ljava/util/List;

    move-result-object v0

    .line 55
    :goto_0
    return-object v0

    .line 19
    :cond_0
    new-instance v0, Lco/uk/getmondo/transaction/details/d/h/b$b;

    invoke-direct {v0, p0}, Lco/uk/getmondo/transaction/details/d/h/b$b;-><init>(Lco/uk/getmondo/transaction/details/d/h/b;)V

    .line 27
    new-instance v1, Lco/uk/getmondo/transaction/details/d/h/b$a;

    invoke-direct {v1, p0}, Lco/uk/getmondo/transaction/details/d/h/b$a;-><init>(Lco/uk/getmondo/transaction/details/d/h/b;)V

    .line 41
    new-instance v2, Lco/uk/getmondo/transaction/details/d/h/b$c;

    invoke-direct {v2, p0}, Lco/uk/getmondo/transaction/details/d/h/b$c;-><init>(Lco/uk/getmondo/transaction/details/d/h/b;)V

    .line 55
    const/4 v3, 0x3

    new-array v3, v3, [Lco/uk/getmondo/transaction/details/b/o;

    const/4 v4, 0x0

    check-cast v0, Lco/uk/getmondo/transaction/details/b/o;

    aput-object v0, v3, v4

    const/4 v4, 0x1

    move-object v0, v1

    check-cast v0, Lco/uk/getmondo/transaction/details/b/o;

    aput-object v0, v3, v4

    const/4 v1, 0x2

    move-object v0, v2

    check-cast v0, Lco/uk/getmondo/transaction/details/b/o;

    aput-object v0, v3, v1

    invoke-static {v3}, Lkotlin/a/m;->b([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    goto :goto_0
.end method

.method public b(Landroid/content/res/Resources;)Ljava/lang/String;
    .locals 1

    .prologue
    const-string v0, "resources"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 59
    const v0, 0x7f0a041c

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
