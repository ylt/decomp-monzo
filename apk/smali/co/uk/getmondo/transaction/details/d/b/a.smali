.class public final Lco/uk/getmondo/transaction/details/d/b/a;
.super Lco/uk/getmondo/transaction/details/b/d;
.source "BacsHeader.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0010\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\u000cH\u0016R\u0014\u0010\u0005\u001a\u00020\u00068VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0007\u0010\u0008R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\r"
    }
    d2 = {
        "Lco/uk/getmondo/transaction/details/types/bacs/BacsHeader;",
        "Lco/uk/getmondo/transaction/details/base/BaseHeader;",
        "transaction",
        "Lco/uk/getmondo/model/Transaction;",
        "(Lco/uk/getmondo/model/Transaction;)V",
        "avatar",
        "Lco/uk/getmondo/transaction/details/base/Avatar;",
        "getAvatar",
        "()Lco/uk/getmondo/transaction/details/base/Avatar;",
        "title",
        "",
        "resources",
        "Landroid/content/res/Resources;",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field private final a:Lco/uk/getmondo/d/aj;


# direct methods
.method public constructor <init>(Lco/uk/getmondo/d/aj;)V
    .locals 1

    .prologue
    const-string v0, "transaction"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 8
    invoke-direct {p0, p1}, Lco/uk/getmondo/transaction/details/b/d;-><init>(Lco/uk/getmondo/d/aj;)V

    iput-object p1, p0, Lco/uk/getmondo/transaction/details/d/b/a;->a:Lco/uk/getmondo/d/aj;

    return-void
.end method

.method public static final synthetic a(Lco/uk/getmondo/transaction/details/d/b/a;)Lco/uk/getmondo/d/aj;
    .locals 1

    .prologue
    .line 8
    iget-object v0, p0, Lco/uk/getmondo/transaction/details/d/b/a;->a:Lco/uk/getmondo/d/aj;

    return-object v0
.end method


# virtual methods
.method public c(Landroid/content/res/Resources;)Ljava/lang/String;
    .locals 2

    .prologue
    const-string v0, "resources"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 10
    iget-object v0, p0, Lco/uk/getmondo/transaction/details/d/b/a;->a:Lco/uk/getmondo/d/aj;

    invoke-virtual {v0}, Lco/uk/getmondo/d/aj;->C()Lco/uk/getmondo/payments/send/data/a/a;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lco/uk/getmondo/payments/send/data/a/a;->a()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lco/uk/getmondo/transaction/details/d/b/a;->a:Lco/uk/getmondo/d/aj;

    invoke-virtual {v0}, Lco/uk/getmondo/d/aj;->x()Ljava/lang/String;

    move-result-object v0

    const-string v1, "transaction.description"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public j()Lco/uk/getmondo/transaction/details/b/c;
    .locals 1

    .prologue
    .line 13
    new-instance v0, Lco/uk/getmondo/transaction/details/d/b/a$a;

    invoke-direct {v0, p0}, Lco/uk/getmondo/transaction/details/d/b/a$a;-><init>(Lco/uk/getmondo/transaction/details/d/b/a;)V

    check-cast v0, Lco/uk/getmondo/transaction/details/b/c;

    .line 22
    return-object v0
.end method
