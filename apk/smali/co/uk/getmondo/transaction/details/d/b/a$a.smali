.class public final Lco/uk/getmondo/transaction/details/d/b/a$a;
.super Ljava/lang/Object;
.source "BacsHeader.kt"

# interfaces
.implements Lco/uk/getmondo/transaction/details/b/c;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lco/uk/getmondo/transaction/details/d/b/a;->j()Lco/uk/getmondo/transaction/details/b/c;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000\u001d\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0002\u0008\u0003\n\u0002\u0010\u000b\n\u0002\u0008\u0004*\u0001\u0000\u0008\n\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002R\u0016\u0010\u0003\u001a\u0004\u0018\u00010\u00048VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0005\u0010\u0006R\u0014\u0010\u0007\u001a\u00020\u00088VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0007\u0010\tR\u0014\u0010\n\u001a\u00020\u00048VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u000b\u0010\u0006\u00a8\u0006\u000c"
    }
    d2 = {
        "co/uk/getmondo/transaction/details/types/bacs/BacsHeader$avatar$1",
        "Lco/uk/getmondo/transaction/details/base/Avatar;",
        "(Lco/uk/getmondo/transaction/details/types/bacs/BacsHeader;)V",
        "imageUrl",
        "",
        "getImageUrl",
        "()Ljava/lang/String;",
        "isSquare",
        "",
        "()Z",
        "name",
        "getName",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field final synthetic a:Lco/uk/getmondo/transaction/details/d/b/a;


# direct methods
.method constructor <init>(Lco/uk/getmondo/transaction/details/d/b/a;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 13
    iput-object p1, p0, Lco/uk/getmondo/transaction/details/d/b/a$a;->a:Lco/uk/getmondo/transaction/details/d/b/a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroid/content/Context;)Ljava/lang/Integer;
    .locals 1

    .prologue
    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 13
    invoke-static {p0, p1}, Lco/uk/getmondo/transaction/details/b/c$a;->a(Lco/uk/getmondo/transaction/details/b/c;Landroid/content/Context;)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 15
    iget-object v0, p0, Lco/uk/getmondo/transaction/details/d/b/a$a;->a:Lco/uk/getmondo/transaction/details/d/b/a;

    invoke-static {v0}, Lco/uk/getmondo/transaction/details/d/b/a;->a(Lco/uk/getmondo/transaction/details/d/b/a;)Lco/uk/getmondo/d/aj;

    move-result-object v0

    invoke-virtual {v0}, Lco/uk/getmondo/d/aj;->B()Lco/uk/getmondo/d/aa;

    move-result-object v0

    invoke-virtual {v0}, Lco/uk/getmondo/d/aa;->g()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public b()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 13
    invoke-static {p0}, Lco/uk/getmondo/transaction/details/b/c$a;->b(Lco/uk/getmondo/transaction/details/b/c;)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public c()Ljava/lang/String;
    .locals 2

    .prologue
    .line 18
    iget-object v0, p0, Lco/uk/getmondo/transaction/details/d/b/a$a;->a:Lco/uk/getmondo/transaction/details/d/b/a;

    invoke-static {v0}, Lco/uk/getmondo/transaction/details/d/b/a;->a(Lco/uk/getmondo/transaction/details/d/b/a;)Lco/uk/getmondo/d/aj;

    move-result-object v0

    invoke-virtual {v0}, Lco/uk/getmondo/d/aj;->B()Lco/uk/getmondo/d/aa;

    move-result-object v0

    invoke-virtual {v0}, Lco/uk/getmondo/d/aa;->b()Ljava/lang/String;

    move-result-object v0

    const-string v1, "transaction.peer.name"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public d()Z
    .locals 1

    .prologue
    .line 21
    const/4 v0, 0x1

    return v0
.end method
