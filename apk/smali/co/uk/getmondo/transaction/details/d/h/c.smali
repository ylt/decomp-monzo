.class public final Lco/uk/getmondo/transaction/details/d/h/c;
.super Ljava/lang/Object;
.source "TopUpTransaction.kt"

# interfaces
.implements Lco/uk/getmondo/transaction/details/b/l;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000R\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0007\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u000e\n\u0000\u0008\u0086\u0008\u0018\u00002\u00020\u0001B\u001d\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\u0008J\t\u0010\u0016\u001a\u00020\u0003H\u00c2\u0003J\t\u0010\u0017\u001a\u00020\u0005H\u00c2\u0003J\t\u0010\u0018\u001a\u00020\u0007H\u00c2\u0003J\'\u0010\u0019\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u00052\u0008\u0008\u0002\u0010\u0006\u001a\u00020\u0007H\u00c6\u0001J\u0013\u0010\u001a\u001a\u00020\u001b2\u0008\u0010\u001c\u001a\u0004\u0018\u00010\u001dH\u00d6\u0003J\t\u0010\u001e\u001a\u00020\u001fH\u00d6\u0001J\t\u0010 \u001a\u00020!H\u00d6\u0001R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001a\u0010\t\u001a\u0008\u0012\u0004\u0012\u00020\u000b0\n8VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u000c\u0010\rR\u0014\u0010\u000e\u001a\u00020\u000f8VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0010\u0010\u0011R\u0014\u0010\u0012\u001a\u00020\u00138VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0014\u0010\u0015R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\""
    }
    d2 = {
        "Lco/uk/getmondo/transaction/details/types/topup/TopUpTransaction;",
        "Lco/uk/getmondo/transaction/details/base/TransactionViewModel;",
        "transaction",
        "Lco/uk/getmondo/model/Transaction;",
        "transactionHistory",
        "Lco/uk/getmondo/transaction/details/model/AverageSpendingHistory;",
        "accountManager",
        "Lco/uk/getmondo/common/accounts/AccountManager;",
        "(Lco/uk/getmondo/model/Transaction;Lco/uk/getmondo/transaction/details/model/AverageSpendingHistory;Lco/uk/getmondo/common/accounts/AccountManager;)V",
        "actions",
        "",
        "Lco/uk/getmondo/transaction/details/base/Action;",
        "getActions",
        "()Ljava/util/List;",
        "content",
        "Lco/uk/getmondo/transaction/details/base/Content;",
        "getContent",
        "()Lco/uk/getmondo/transaction/details/base/Content;",
        "header",
        "Lco/uk/getmondo/transaction/details/base/BaseHeader;",
        "getHeader",
        "()Lco/uk/getmondo/transaction/details/base/BaseHeader;",
        "component1",
        "component2",
        "component3",
        "copy",
        "equals",
        "",
        "other",
        "",
        "hashCode",
        "",
        "toString",
        "",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field private final a:Lco/uk/getmondo/d/aj;

.field private final b:Lco/uk/getmondo/transaction/details/c/a;

.field private final c:Lco/uk/getmondo/common/accounts/b;


# direct methods
.method public constructor <init>(Lco/uk/getmondo/d/aj;Lco/uk/getmondo/transaction/details/c/a;Lco/uk/getmondo/common/accounts/b;)V
    .locals 1

    .prologue
    const-string v0, "transaction"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "transactionHistory"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "accountManager"

    invoke-static {p3, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lco/uk/getmondo/transaction/details/d/h/c;->a:Lco/uk/getmondo/d/aj;

    iput-object p2, p0, Lco/uk/getmondo/transaction/details/d/h/c;->b:Lco/uk/getmondo/transaction/details/c/a;

    iput-object p3, p0, Lco/uk/getmondo/transaction/details/d/h/c;->c:Lco/uk/getmondo/common/accounts/b;

    return-void
.end method

.method public static final synthetic a(Lco/uk/getmondo/transaction/details/d/h/c;)Lco/uk/getmondo/transaction/details/c/a;
    .locals 1

    .prologue
    .line 11
    iget-object v0, p0, Lco/uk/getmondo/transaction/details/d/h/c;->b:Lco/uk/getmondo/transaction/details/c/a;

    return-object v0
.end method


# virtual methods
.method public a()Lco/uk/getmondo/transaction/details/b/d;
    .locals 2

    .prologue
    .line 17
    new-instance v0, Lco/uk/getmondo/transaction/details/d/h/a;

    iget-object v1, p0, Lco/uk/getmondo/transaction/details/d/h/c;->a:Lco/uk/getmondo/d/aj;

    invoke-direct {v0, v1}, Lco/uk/getmondo/transaction/details/d/h/a;-><init>(Lco/uk/getmondo/d/aj;)V

    check-cast v0, Lco/uk/getmondo/transaction/details/b/d;

    return-object v0
.end method

.method public b()Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lco/uk/getmondo/transaction/details/b/a;",
            ">;"
        }
    .end annotation

    .prologue
    .line 20
    const/4 v0, 0x3

    new-array v1, v0, [Lco/uk/getmondo/transaction/details/b/a;

    const/4 v2, 0x0

    new-instance v0, Lco/uk/getmondo/transaction/details/a/h;

    iget-object v3, p0, Lco/uk/getmondo/transaction/details/d/h/c;->c:Lco/uk/getmondo/common/accounts/b;

    invoke-virtual {v3}, Lco/uk/getmondo/common/accounts/b;->b()Z

    move-result v3

    invoke-direct {v0, v3}, Lco/uk/getmondo/transaction/details/a/h;-><init>(Z)V

    check-cast v0, Lco/uk/getmondo/transaction/details/b/a;

    aput-object v0, v1, v2

    const/4 v2, 0x1

    new-instance v0, Lco/uk/getmondo/transaction/details/a/b;

    iget-object v3, p0, Lco/uk/getmondo/transaction/details/d/h/c;->a:Lco/uk/getmondo/d/aj;

    invoke-direct {v0, v3}, Lco/uk/getmondo/transaction/details/a/b;-><init>(Lco/uk/getmondo/d/aj;)V

    check-cast v0, Lco/uk/getmondo/transaction/details/b/a;

    aput-object v0, v1, v2

    const/4 v2, 0x2

    new-instance v0, Lco/uk/getmondo/transaction/details/a/c;

    iget-object v3, p0, Lco/uk/getmondo/transaction/details/d/h/c;->a:Lco/uk/getmondo/d/aj;

    invoke-direct {v0, v3}, Lco/uk/getmondo/transaction/details/a/c;-><init>(Lco/uk/getmondo/d/aj;)V

    check-cast v0, Lco/uk/getmondo/transaction/details/b/a;

    aput-object v0, v1, v2

    invoke-static {v1}, Lkotlin/a/m;->b([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public c()Lco/uk/getmondo/transaction/details/b/e;
    .locals 1

    .prologue
    .line 23
    new-instance v0, Lco/uk/getmondo/transaction/details/d/h/c$a;

    invoke-direct {v0, p0}, Lco/uk/getmondo/transaction/details/d/h/c$a;-><init>(Lco/uk/getmondo/transaction/details/d/h/c;)V

    check-cast v0, Lco/uk/getmondo/transaction/details/b/e;

    .line 26
    return-object v0
.end method

.method public d()Lco/uk/getmondo/transaction/details/b/f;
    .locals 1

    .prologue
    .line 11
    invoke-static {p0}, Lco/uk/getmondo/transaction/details/b/l$a;->a(Lco/uk/getmondo/transaction/details/b/l;)Lco/uk/getmondo/transaction/details/b/f;

    move-result-object v0

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_0

    instance-of v0, p1, Lco/uk/getmondo/transaction/details/d/h/c;

    if-eqz v0, :cond_1

    check-cast p1, Lco/uk/getmondo/transaction/details/d/h/c;

    iget-object v0, p0, Lco/uk/getmondo/transaction/details/d/h/c;->a:Lco/uk/getmondo/d/aj;

    iget-object v1, p1, Lco/uk/getmondo/transaction/details/d/h/c;->a:Lco/uk/getmondo/d/aj;

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lco/uk/getmondo/transaction/details/d/h/c;->b:Lco/uk/getmondo/transaction/details/c/a;

    iget-object v1, p1, Lco/uk/getmondo/transaction/details/d/h/c;->b:Lco/uk/getmondo/transaction/details/c/a;

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lco/uk/getmondo/transaction/details/d/h/c;->c:Lco/uk/getmondo/common/accounts/b;

    iget-object v1, p1, Lco/uk/getmondo/transaction/details/d/h/c;->c:Lco/uk/getmondo/common/accounts/b;

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    const/4 v1, 0x0

    iget-object v0, p0, Lco/uk/getmondo/transaction/details/d/h/c;->a:Lco/uk/getmondo/d/aj;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    :goto_0
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lco/uk/getmondo/transaction/details/d/h/c;->b:Lco/uk/getmondo/transaction/details/c/a;

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lco/uk/getmondo/transaction/details/d/h/c;->c:Lco/uk/getmondo/common/accounts/b;

    if-eqz v2, :cond_0

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_0
    add-int/2addr v0, v1

    return v0

    :cond_1
    move v0, v1

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_1
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "TopUpTransaction(transaction="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/transaction/details/d/h/c;->a:Lco/uk/getmondo/d/aj;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", transactionHistory="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/transaction/details/d/h/c;->b:Lco/uk/getmondo/transaction/details/c/a;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", accountManager="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/transaction/details/d/h/c;->c:Lco/uk/getmondo/common/accounts/b;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
