.class public final Lco/uk/getmondo/transaction/details/d/f/b$a;
.super Ljava/lang/Object;
.source "P2pHistory.kt"

# interfaces
.implements Lco/uk/getmondo/transaction/details/b/o;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lco/uk/getmondo/transaction/details/d/f/b;->a()Ljava/util/List;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000\u001b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003*\u0001\u0000\u0008\n\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0012\u0010\u0003\u001a\u0004\u0018\u00010\u00042\u0006\u0010\u0005\u001a\u00020\u0006H\u0016J\u0010\u0010\u0007\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006H\u0016J\u0010\u0010\u0008\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006H\u0016\u00a8\u0006\t"
    }
    d2 = {
        "co/uk/getmondo/transaction/details/types/p2p/P2pHistory$items$numberOfPayments$1",
        "Lco/uk/getmondo/transaction/details/base/YourHistoryItem;",
        "(Lco/uk/getmondo/transaction/details/types/p2p/P2pHistory;)V",
        "subtitle",
        "",
        "resources",
        "Landroid/content/res/Resources;",
        "title",
        "value",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field final synthetic a:Lco/uk/getmondo/transaction/details/d/f/b;


# direct methods
.method constructor <init>(Lco/uk/getmondo/transaction/details/d/f/b;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 25
    iput-object p1, p0, Lco/uk/getmondo/transaction/details/d/f/b$a;->a:Lco/uk/getmondo/transaction/details/d/f/b;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroid/content/res/Resources;)Ljava/lang/String;
    .locals 2

    .prologue
    const-string v0, "resources"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 26
    const v0, 0x7f0a0418

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "resources.getString(R.st\u2026ng.tx_p2p_no_of_payments)"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public b(Landroid/content/res/Resources;)Ljava/lang/String;
    .locals 1

    .prologue
    const-string v0, "resources"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 28
    const/4 v0, 0x0

    return-object v0
.end method

.method public c(Landroid/content/res/Resources;)Ljava/lang/String;
    .locals 4

    .prologue
    const-string v0, "resources"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 31
    iget-object v0, p0, Lco/uk/getmondo/transaction/details/d/f/b$a;->a:Lco/uk/getmondo/transaction/details/d/f/b;

    invoke-static {v0}, Lco/uk/getmondo/transaction/details/d/f/b;->a(Lco/uk/getmondo/transaction/details/d/f/b;)Lco/uk/getmondo/transaction/details/c/b;

    move-result-object v0

    invoke-virtual {v0}, Lco/uk/getmondo/transaction/details/c/b;->c()J

    move-result-wide v0

    iget-object v2, p0, Lco/uk/getmondo/transaction/details/d/f/b$a;->a:Lco/uk/getmondo/transaction/details/d/f/b;

    invoke-static {v2}, Lco/uk/getmondo/transaction/details/d/f/b;->a(Lco/uk/getmondo/transaction/details/d/f/b;)Lco/uk/getmondo/transaction/details/c/b;

    move-result-object v2

    invoke-virtual {v2}, Lco/uk/getmondo/transaction/details/c/b;->a()J

    move-result-wide v2

    add-long/2addr v0, v2

    .line 32
    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
