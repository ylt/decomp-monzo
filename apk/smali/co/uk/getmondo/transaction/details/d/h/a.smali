.class public final Lco/uk/getmondo/transaction/details/d/h/a;
.super Lco/uk/getmondo/transaction/details/b/d;
.source "TopUpHeader.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u000b\n\u0002\u0008\u0003\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0010\u0010\r\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u0010H\u0016R\u0014\u0010\u0005\u001a\u00020\u00068VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0007\u0010\u0008R\u0014\u0010\t\u001a\u00020\n8VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u000b\u0010\u000c\u00a8\u0006\u0011"
    }
    d2 = {
        "Lco/uk/getmondo/transaction/details/types/topup/TopUpHeader;",
        "Lco/uk/getmondo/transaction/details/base/BaseHeader;",
        "transaction",
        "Lco/uk/getmondo/model/Transaction;",
        "(Lco/uk/getmondo/model/Transaction;)V",
        "avatar",
        "Lco/uk/getmondo/transaction/details/base/Avatar;",
        "getAvatar",
        "()Lco/uk/getmondo/transaction/details/base/Avatar;",
        "showCategoryChooser",
        "",
        "getShowCategoryChooser",
        "()Z",
        "title",
        "",
        "resources",
        "Landroid/content/res/Resources;",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# direct methods
.method public constructor <init>(Lco/uk/getmondo/d/aj;)V
    .locals 1

    .prologue
    const-string v0, "transaction"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 11
    invoke-direct {p0, p1}, Lco/uk/getmondo/transaction/details/b/d;-><init>(Lco/uk/getmondo/d/aj;)V

    return-void
.end method


# virtual methods
.method public c(Landroid/content/res/Resources;)Ljava/lang/String;
    .locals 2

    .prologue
    const-string v0, "resources"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 12
    const v0, 0x7f0a041c

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "resources.getString(R.string.tx_title_topup)"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public i()Z
    .locals 1

    .prologue
    .line 26
    const/4 v0, 0x0

    return v0
.end method

.method public j()Lco/uk/getmondo/transaction/details/b/c;
    .locals 1

    .prologue
    .line 15
    new-instance v0, Lco/uk/getmondo/transaction/details/d/h/a$a;

    invoke-direct {v0}, Lco/uk/getmondo/transaction/details/d/h/a$a;-><init>()V

    check-cast v0, Lco/uk/getmondo/transaction/details/b/c;

    .line 23
    return-object v0
.end method
