.class public final Lco/uk/getmondo/transaction/details/d/g/a$a;
.super Ljava/lang/Object;
.source "TflHeader.kt"

# interfaces
.implements Lco/uk/getmondo/transaction/details/b/c;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lco/uk/getmondo/transaction/details/d/g/a;->j()Lco/uk/getmondo/transaction/details/b/c;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000+\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0002\u0008\u0003\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002*\u0001\u0000\u0008\n\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0017\u0010\n\u001a\u0004\u0018\u00010\u000b2\u0006\u0010\u000c\u001a\u00020\rH\u0016\u00a2\u0006\u0002\u0010\u000eR\u0016\u0010\u0003\u001a\u0004\u0018\u00010\u00048VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0005\u0010\u0006R\u0014\u0010\u0007\u001a\u00020\u00088VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0007\u0010\t\u00a8\u0006\u000f"
    }
    d2 = {
        "co/uk/getmondo/transaction/details/types/tfl/TflHeader$avatar$1",
        "Lco/uk/getmondo/transaction/details/base/Avatar;",
        "(Lco/uk/getmondo/transaction/details/types/tfl/TflHeader;)V",
        "imageUrl",
        "",
        "getImageUrl",
        "()Ljava/lang/String;",
        "isSquare",
        "",
        "()Z",
        "backgroundColour",
        "",
        "context",
        "Landroid/content/Context;",
        "(Landroid/content/Context;)Ljava/lang/Integer;",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field final synthetic a:Lco/uk/getmondo/transaction/details/d/g/a;


# direct methods
.method constructor <init>(Lco/uk/getmondo/transaction/details/d/g/a;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 23
    iput-object p1, p0, Lco/uk/getmondo/transaction/details/d/g/a$a;->a:Lco/uk/getmondo/transaction/details/d/g/a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroid/content/Context;)Ljava/lang/Integer;
    .locals 1

    .prologue
    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 30
    const v0, 0x7f0f00c7

    invoke-static {p1, v0}, Landroid/support/v4/content/a;->c(Landroid/content/Context;I)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, Lco/uk/getmondo/transaction/details/d/g/a$a;->a:Lco/uk/getmondo/transaction/details/d/g/a;

    invoke-static {v0}, Lco/uk/getmondo/transaction/details/d/g/a;->a(Lco/uk/getmondo/transaction/details/d/g/a;)Lco/uk/getmondo/d/u;

    move-result-object v0

    invoke-virtual {v0}, Lco/uk/getmondo/d/u;->j()Ljava/lang/String;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_2

    const/4 v0, 0x0

    :goto_1
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lco/uk/getmondo/transaction/details/d/g/a$a;->a:Lco/uk/getmondo/transaction/details/d/g/a;

    invoke-static {v0}, Lco/uk/getmondo/transaction/details/d/g/a;->a(Lco/uk/getmondo/transaction/details/d/g/a;)Lco/uk/getmondo/d/u;

    move-result-object v0

    invoke-virtual {v0}, Lco/uk/getmondo/d/u;->j()Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method

.method public b()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 23
    invoke-static {p0}, Lco/uk/getmondo/transaction/details/b/c$a;->b(Lco/uk/getmondo/transaction/details/b/c;)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 23
    invoke-static {p0}, Lco/uk/getmondo/transaction/details/b/c$a;->c(Lco/uk/getmondo/transaction/details/b/c;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public d()Z
    .locals 1

    .prologue
    .line 28
    const/4 v0, 0x1

    return v0
.end method
