.class public final Lco/uk/getmondo/transaction/details/d/a/a$a;
.super Ljava/lang/Object;
.source "AtmTransaction.kt"

# interfaces
.implements Lco/uk/getmondo/transaction/details/b/e;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lco/uk/getmondo/transaction/details/d/a/a;->c()Lco/uk/getmondo/transaction/details/b/e;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000\u001d\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0003*\u0001\u0000\u0008\n\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002R\u0016\u0010\u0003\u001a\u0004\u0018\u00010\u00048VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0005\u0010\u0006R\u0016\u0010\u0007\u001a\u0004\u0018\u00010\u00088VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\t\u0010\n\u00a8\u0006\u000b"
    }
    d2 = {
        "co/uk/getmondo/transaction/details/types/atm/AtmTransaction$content$1",
        "Lco/uk/getmondo/transaction/details/base/Content;",
        "(Lco/uk/getmondo/transaction/details/types/atm/AtmTransaction;)V",
        "furtherInformationWebLink",
        "Lco/uk/getmondo/transaction/details/base/InformationWebLink;",
        "getFurtherInformationWebLink",
        "()Lco/uk/getmondo/transaction/details/base/InformationWebLink;",
        "yourHistory",
        "Lco/uk/getmondo/transaction/details/base/YourHistory;",
        "getYourHistory",
        "()Lco/uk/getmondo/transaction/details/base/YourHistory;",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field final synthetic a:Lco/uk/getmondo/transaction/details/d/a/a;


# direct methods
.method constructor <init>(Lco/uk/getmondo/transaction/details/d/a/a;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 30
    iput-object p1, p0, Lco/uk/getmondo/transaction/details/d/a/a$a;->a:Lco/uk/getmondo/transaction/details/d/a/a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a()Lco/uk/getmondo/transaction/details/b/h;
    .locals 1

    .prologue
    .line 33
    new-instance v0, Lco/uk/getmondo/transaction/details/d/a/a$a$a;

    invoke-direct {v0}, Lco/uk/getmondo/transaction/details/d/a/a$a$a;-><init>()V

    check-cast v0, Lco/uk/getmondo/transaction/details/b/h;

    return-object v0
.end method

.method public b()Lco/uk/getmondo/transaction/details/b/n;
    .locals 3

    .prologue
    .line 44
    new-instance v0, Lco/uk/getmondo/transaction/details/d/e/a;

    iget-object v1, p0, Lco/uk/getmondo/transaction/details/d/a/a$a;->a:Lco/uk/getmondo/transaction/details/d/a/a;

    invoke-static {v1}, Lco/uk/getmondo/transaction/details/d/a/a;->a(Lco/uk/getmondo/transaction/details/d/a/a;)Lco/uk/getmondo/d/aj;

    move-result-object v1

    iget-object v2, p0, Lco/uk/getmondo/transaction/details/d/a/a$a;->a:Lco/uk/getmondo/transaction/details/d/a/a;

    invoke-static {v2}, Lco/uk/getmondo/transaction/details/d/a/a;->b(Lco/uk/getmondo/transaction/details/d/a/a;)Lco/uk/getmondo/transaction/details/c/a;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lco/uk/getmondo/transaction/details/d/e/a;-><init>(Lco/uk/getmondo/d/aj;Lco/uk/getmondo/transaction/details/c/a;)V

    check-cast v0, Lco/uk/getmondo/transaction/details/b/n;

    return-object v0
.end method
