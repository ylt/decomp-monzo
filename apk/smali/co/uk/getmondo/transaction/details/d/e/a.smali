.class public final Lco/uk/getmondo/transaction/details/d/e/a;
.super Ljava/lang/Object;
.source "MerchantHistory.kt"

# interfaces
.implements Lco/uk/getmondo/transaction/details/b/n;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u00002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u00002\u00020\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\u0012\u0010\u000c\u001a\u0004\u0018\u00010\r2\u0006\u0010\u000e\u001a\u00020\u000fH\u0016J\u0010\u0010\u0010\u001a\u00020\r2\u0006\u0010\u000e\u001a\u00020\u000fH\u0016R\u001a\u0010\u0007\u001a\u0008\u0012\u0004\u0012\u00020\t0\u00088VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\n\u0010\u000bR\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0011"
    }
    d2 = {
        "Lco/uk/getmondo/transaction/details/types/merchant/MerchantHistory;",
        "Lco/uk/getmondo/transaction/details/base/YourHistory;",
        "transaction",
        "Lco/uk/getmondo/model/Transaction;",
        "transactionHistory",
        "Lco/uk/getmondo/transaction/details/model/AverageSpendingHistory;",
        "(Lco/uk/getmondo/model/Transaction;Lco/uk/getmondo/transaction/details/model/AverageSpendingHistory;)V",
        "items",
        "",
        "Lco/uk/getmondo/transaction/details/base/YourHistoryItem;",
        "getItems",
        "()Ljava/util/List;",
        "searchQuery",
        "",
        "resources",
        "Landroid/content/res/Resources;",
        "title",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field private final a:Lco/uk/getmondo/d/aj;

.field private final b:Lco/uk/getmondo/transaction/details/c/a;


# direct methods
.method public constructor <init>(Lco/uk/getmondo/d/aj;Lco/uk/getmondo/transaction/details/c/a;)V
    .locals 1

    .prologue
    const-string v0, "transaction"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "transactionHistory"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lco/uk/getmondo/transaction/details/d/e/a;->a:Lco/uk/getmondo/d/aj;

    iput-object p2, p0, Lco/uk/getmondo/transaction/details/d/e/a;->b:Lco/uk/getmondo/transaction/details/c/a;

    return-void
.end method

.method public static final synthetic a(Lco/uk/getmondo/transaction/details/d/e/a;)Lco/uk/getmondo/d/aj;
    .locals 1

    .prologue
    .line 10
    iget-object v0, p0, Lco/uk/getmondo/transaction/details/d/e/a;->a:Lco/uk/getmondo/d/aj;

    return-object v0
.end method

.method public static final synthetic b(Lco/uk/getmondo/transaction/details/d/e/a;)Lco/uk/getmondo/transaction/details/c/a;
    .locals 1

    .prologue
    .line 10
    iget-object v0, p0, Lco/uk/getmondo/transaction/details/d/e/a;->b:Lco/uk/getmondo/transaction/details/c/a;

    return-object v0
.end method


# virtual methods
.method public a(Landroid/content/res/Resources;)Ljava/lang/String;
    .locals 4

    .prologue
    const-string v0, "resources"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 15
    iget-object v0, p0, Lco/uk/getmondo/transaction/details/d/e/a;->a:Lco/uk/getmondo/d/aj;

    invoke-virtual {v0}, Lco/uk/getmondo/d/aj;->u()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lco/uk/getmondo/transaction/details/d/e/a;->a:Lco/uk/getmondo/d/aj;

    invoke-virtual {v0}, Lco/uk/getmondo/d/aj;->x()Ljava/lang/String;

    move-result-object v0

    :goto_0
    if-eqz v0, :cond_2

    .line 16
    const v1, 0x7f0a0459

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object v0, v2, v3

    invoke-virtual {p1, v1, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "resources.getString(R.st\u2026r_merchant_history, name)"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    :goto_1
    return-object v0

    .line 15
    :cond_0
    iget-object v0, p0, Lco/uk/getmondo/transaction/details/d/e/a;->a:Lco/uk/getmondo/d/aj;

    invoke-virtual {v0}, Lco/uk/getmondo/d/aj;->f()Lco/uk/getmondo/d/u;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lco/uk/getmondo/d/u;->i()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :cond_2
    const-string v0, ""

    goto :goto_1
.end method

.method public a()Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lco/uk/getmondo/transaction/details/b/o;",
            ">;"
        }
    .end annotation

    .prologue
    .line 21
    iget-object v0, p0, Lco/uk/getmondo/transaction/details/d/e/a;->b:Lco/uk/getmondo/transaction/details/c/a;

    invoke-virtual {v0}, Lco/uk/getmondo/transaction/details/c/a;->a()J

    move-result-wide v0

    const-wide/16 v2, 0x1

    cmp-long v0, v0, v2

    if-gtz v0, :cond_0

    .line 22
    invoke-static {}, Lkotlin/a/m;->a()Ljava/util/List;

    move-result-object v0

    .line 67
    :goto_0
    return-object v0

    .line 25
    :cond_0
    new-instance v0, Lco/uk/getmondo/transaction/details/d/e/a$b;

    invoke-direct {v0, p0}, Lco/uk/getmondo/transaction/details/d/e/a$b;-><init>(Lco/uk/getmondo/transaction/details/d/e/a;)V

    .line 39
    new-instance v1, Lco/uk/getmondo/transaction/details/d/e/a$a;

    invoke-direct {v1, p0}, Lco/uk/getmondo/transaction/details/d/e/a$a;-><init>(Lco/uk/getmondo/transaction/details/d/e/a;)V

    .line 53
    new-instance v2, Lco/uk/getmondo/transaction/details/d/e/a$c;

    invoke-direct {v2, p0}, Lco/uk/getmondo/transaction/details/d/e/a$c;-><init>(Lco/uk/getmondo/transaction/details/d/e/a;)V

    .line 67
    const/4 v3, 0x3

    new-array v3, v3, [Lco/uk/getmondo/transaction/details/b/o;

    const/4 v4, 0x0

    check-cast v0, Lco/uk/getmondo/transaction/details/b/o;

    aput-object v0, v3, v4

    const/4 v4, 0x1

    move-object v0, v1

    check-cast v0, Lco/uk/getmondo/transaction/details/b/o;

    aput-object v0, v3, v4

    const/4 v1, 0x2

    move-object v0, v2

    check-cast v0, Lco/uk/getmondo/transaction/details/b/o;

    aput-object v0, v3, v1

    invoke-static {v3}, Lkotlin/a/m;->b([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    goto :goto_0
.end method

.method public b(Landroid/content/res/Resources;)Ljava/lang/String;
    .locals 1

    .prologue
    const-string v0, "resources"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 71
    iget-object v0, p0, Lco/uk/getmondo/transaction/details/d/e/a;->a:Lco/uk/getmondo/d/aj;

    invoke-virtual {v0}, Lco/uk/getmondo/d/aj;->f()Lco/uk/getmondo/d/u;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lco/uk/getmondo/d/u;->i()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
