.class public final Lco/uk/getmondo/transaction/details/d/g/a;
.super Lco/uk/getmondo/transaction/details/b/d;
.source "TflHeader.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000:\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0012\u0010\u000b\u001a\u0004\u0018\u00010\u000c2\u0006\u0010\r\u001a\u00020\u000eH\u0016J\u0010\u0010\u000f\u001a\u00020\u00102\u0006\u0010\u0011\u001a\u00020\u0012H\u0016J\u0010\u0010\u0013\u001a\u00020\u000c2\u0006\u0010\r\u001a\u00020\u000eH\u0016R\u0014\u0010\u0005\u001a\u00020\u00068VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0007\u0010\u0008R\u000e\u0010\t\u001a\u00020\nX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0014"
    }
    d2 = {
        "Lco/uk/getmondo/transaction/details/types/tfl/TflHeader;",
        "Lco/uk/getmondo/transaction/details/base/BaseHeader;",
        "transaction",
        "Lco/uk/getmondo/model/Transaction;",
        "(Lco/uk/getmondo/model/Transaction;)V",
        "avatar",
        "Lco/uk/getmondo/transaction/details/base/Avatar;",
        "getAvatar",
        "()Lco/uk/getmondo/transaction/details/base/Avatar;",
        "merchant",
        "Lco/uk/getmondo/model/Merchant;",
        "subtitle",
        "",
        "resources",
        "Landroid/content/res/Resources;",
        "subtitleTextAppearance",
        "Lco/uk/getmondo/transaction/details/base/UrlTextAppearance;",
        "context",
        "Landroid/content/Context;",
        "title",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field private final a:Lco/uk/getmondo/d/u;


# direct methods
.method public constructor <init>(Lco/uk/getmondo/d/aj;)V
    .locals 1

    .prologue
    const-string v0, "transaction"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 12
    invoke-direct {p0, p1}, Lco/uk/getmondo/transaction/details/b/d;-><init>(Lco/uk/getmondo/d/aj;)V

    .line 20
    invoke-virtual {p1}, Lco/uk/getmondo/d/aj;->f()Lco/uk/getmondo/d/u;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-static {}, Lkotlin/d/b/l;->a()V

    :cond_0
    iput-object v0, p0, Lco/uk/getmondo/transaction/details/d/g/a;->a:Lco/uk/getmondo/d/u;

    return-void
.end method

.method public static final synthetic a(Lco/uk/getmondo/transaction/details/d/g/a;)Lco/uk/getmondo/d/u;
    .locals 1

    .prologue
    .line 12
    iget-object v0, p0, Lco/uk/getmondo/transaction/details/d/g/a;->a:Lco/uk/getmondo/d/u;

    return-object v0
.end method


# virtual methods
.method public a(Landroid/content/res/Resources;)Ljava/lang/String;
    .locals 1

    .prologue
    const-string v0, "resources"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 16
    const v0, 0x7f0a040f

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public synthetic b(Landroid/content/Context;)Lco/uk/getmondo/transaction/details/b/j;
    .locals 1

    .prologue
    .line 12
    invoke-virtual {p0, p1}, Lco/uk/getmondo/transaction/details/d/g/a;->c(Landroid/content/Context;)Lco/uk/getmondo/transaction/details/b/m;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/transaction/details/b/j;

    return-object v0
.end method

.method public c(Landroid/content/Context;)Lco/uk/getmondo/transaction/details/b/m;
    .locals 1

    .prologue
    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 18
    new-instance v0, Lco/uk/getmondo/transaction/details/b/m;

    invoke-direct {v0, p1}, Lco/uk/getmondo/transaction/details/b/m;-><init>(Landroid/content/Context;)V

    return-object v0
.end method

.method public c(Landroid/content/res/Resources;)Ljava/lang/String;
    .locals 2

    .prologue
    const-string v0, "resources"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 14
    iget-object v0, p0, Lco/uk/getmondo/transaction/details/d/g/a;->a:Lco/uk/getmondo/d/u;

    invoke-virtual {v0}, Lco/uk/getmondo/d/u;->i()Ljava/lang/String;

    move-result-object v0

    const-string v1, "merchant.name"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public j()Lco/uk/getmondo/transaction/details/b/c;
    .locals 1

    .prologue
    .line 23
    new-instance v0, Lco/uk/getmondo/transaction/details/d/g/a$a;

    invoke-direct {v0, p0}, Lco/uk/getmondo/transaction/details/d/g/a$a;-><init>(Lco/uk/getmondo/transaction/details/d/g/a;)V

    check-cast v0, Lco/uk/getmondo/transaction/details/b/c;

    .line 31
    return-object v0
.end method
