.class public final Lco/uk/getmondo/transaction/details/d/d/d;
.super Ljava/lang/Object;
.source "GeneralTransaction.kt"

# interfaces
.implements Lco/uk/getmondo/transaction/details/b/l;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000l\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\n\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u000e\n\u0000\u0008\u0086\u0008\u0018\u00002\u00020\u0001B5\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b\u0012\u0006\u0010\u000c\u001a\u00020\r\u00a2\u0006\u0002\u0010\u000eJ\t\u0010 \u001a\u00020\u0003H\u00c2\u0003J\t\u0010!\u001a\u00020\u0005H\u00c2\u0003J\t\u0010\"\u001a\u00020\u0007H\u00c2\u0003J\t\u0010#\u001a\u00020\tH\u00c2\u0003J\t\u0010$\u001a\u00020\u000bH\u00c2\u0003J\t\u0010%\u001a\u00020\rH\u00c2\u0003JE\u0010&\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u00052\u0008\u0008\u0002\u0010\u0006\u001a\u00020\u00072\u0008\u0008\u0002\u0010\u0008\u001a\u00020\t2\u0008\u0008\u0002\u0010\n\u001a\u00020\u000b2\u0008\u0008\u0002\u0010\u000c\u001a\u00020\rH\u00c6\u0001J\u0013\u0010\'\u001a\u00020(2\u0008\u0010)\u001a\u0004\u0018\u00010*H\u00d6\u0003J\t\u0010+\u001a\u00020,H\u00d6\u0001J\t\u0010-\u001a\u00020.H\u00d6\u0001R\u000e\u0010\u000c\u001a\u00020\rX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001a\u0010\u000f\u001a\u0008\u0012\u0004\u0012\u00020\u00110\u00108VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0012\u0010\u0013R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0014\u001a\u00020\u00158VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0016\u0010\u0017R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0018\u001a\u00020\u00198VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u001a\u0010\u001bR\u0014\u0010\u001c\u001a\u00020\u001d8VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u001e\u0010\u001fR\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006/"
    }
    d2 = {
        "Lco/uk/getmondo/transaction/details/types/general/GeneralTransaction;",
        "Lco/uk/getmondo/transaction/details/base/TransactionViewModel;",
        "transaction",
        "Lco/uk/getmondo/model/Transaction;",
        "transactionHistory",
        "Lco/uk/getmondo/transaction/details/model/TransactionHistory;",
        "costSplitter",
        "Lco/uk/getmondo/transaction/CostSplitter;",
        "userSettingsStorage",
        "Lco/uk/getmondo/payments/send/data/UserSettingsStorage;",
        "analyticsService",
        "Lco/uk/getmondo/common/AnalyticsService;",
        "accountManager",
        "Lco/uk/getmondo/common/accounts/AccountManager;",
        "(Lco/uk/getmondo/model/Transaction;Lco/uk/getmondo/transaction/details/model/TransactionHistory;Lco/uk/getmondo/transaction/CostSplitter;Lco/uk/getmondo/payments/send/data/UserSettingsStorage;Lco/uk/getmondo/common/AnalyticsService;Lco/uk/getmondo/common/accounts/AccountManager;)V",
        "actions",
        "",
        "Lco/uk/getmondo/transaction/details/base/Action;",
        "getActions",
        "()Ljava/util/List;",
        "content",
        "Lco/uk/getmondo/transaction/details/base/Content;",
        "getContent",
        "()Lco/uk/getmondo/transaction/details/base/Content;",
        "footer",
        "Lco/uk/getmondo/transaction/details/base/Footer;",
        "getFooter",
        "()Lco/uk/getmondo/transaction/details/base/Footer;",
        "header",
        "Lco/uk/getmondo/transaction/details/base/BaseHeader;",
        "getHeader",
        "()Lco/uk/getmondo/transaction/details/base/BaseHeader;",
        "component1",
        "component2",
        "component3",
        "component4",
        "component5",
        "component6",
        "copy",
        "equals",
        "",
        "other",
        "",
        "hashCode",
        "",
        "toString",
        "",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field private final a:Lco/uk/getmondo/d/aj;

.field private final b:Lco/uk/getmondo/transaction/details/c/f;

.field private final c:Lco/uk/getmondo/transaction/a;

.field private final d:Lco/uk/getmondo/payments/send/data/p;

.field private final e:Lco/uk/getmondo/common/a;

.field private final f:Lco/uk/getmondo/common/accounts/b;


# direct methods
.method public constructor <init>(Lco/uk/getmondo/d/aj;Lco/uk/getmondo/transaction/details/c/f;Lco/uk/getmondo/transaction/a;Lco/uk/getmondo/payments/send/data/p;Lco/uk/getmondo/common/a;Lco/uk/getmondo/common/accounts/b;)V
    .locals 1

    .prologue
    const-string v0, "transaction"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "transactionHistory"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "costSplitter"

    invoke-static {p3, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "userSettingsStorage"

    invoke-static {p4, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "analyticsService"

    invoke-static {p5, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "accountManager"

    invoke-static {p6, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lco/uk/getmondo/transaction/details/d/d/d;->a:Lco/uk/getmondo/d/aj;

    iput-object p2, p0, Lco/uk/getmondo/transaction/details/d/d/d;->b:Lco/uk/getmondo/transaction/details/c/f;

    iput-object p3, p0, Lco/uk/getmondo/transaction/details/d/d/d;->c:Lco/uk/getmondo/transaction/a;

    iput-object p4, p0, Lco/uk/getmondo/transaction/details/d/d/d;->d:Lco/uk/getmondo/payments/send/data/p;

    iput-object p5, p0, Lco/uk/getmondo/transaction/details/d/d/d;->e:Lco/uk/getmondo/common/a;

    iput-object p6, p0, Lco/uk/getmondo/transaction/details/d/d/d;->f:Lco/uk/getmondo/common/accounts/b;

    return-void
.end method

.method public static final synthetic a(Lco/uk/getmondo/transaction/details/d/d/d;)Lco/uk/getmondo/d/aj;
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Lco/uk/getmondo/transaction/details/d/d/d;->a:Lco/uk/getmondo/d/aj;

    return-object v0
.end method

.method public static final synthetic b(Lco/uk/getmondo/transaction/details/d/d/d;)Lco/uk/getmondo/transaction/details/c/f;
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Lco/uk/getmondo/transaction/details/d/d/d;->b:Lco/uk/getmondo/transaction/details/c/f;

    return-object v0
.end method


# virtual methods
.method public a()Lco/uk/getmondo/transaction/details/b/d;
    .locals 2

    .prologue
    .line 37
    new-instance v0, Lco/uk/getmondo/transaction/details/d/d/c;

    iget-object v1, p0, Lco/uk/getmondo/transaction/details/d/d/d;->a:Lco/uk/getmondo/d/aj;

    invoke-direct {v0, v1}, Lco/uk/getmondo/transaction/details/d/d/c;-><init>(Lco/uk/getmondo/d/aj;)V

    check-cast v0, Lco/uk/getmondo/transaction/details/b/d;

    return-object v0
.end method

.method public b()Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lco/uk/getmondo/transaction/details/b/a;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 41
    iget-object v1, p0, Lco/uk/getmondo/transaction/details/d/d/d;->a:Lco/uk/getmondo/d/aj;

    invoke-virtual {v1}, Lco/uk/getmondo/d/aj;->g()Lco/uk/getmondo/d/c;

    move-result-object v1

    invoke-virtual {v1}, Lco/uk/getmondo/d/c;->k()J

    move-result-wide v2

    const/16 v1, -0xc8

    int-to-long v4, v1

    cmp-long v1, v2, v4

    if-gtz v1, :cond_0

    iget-object v1, p0, Lco/uk/getmondo/transaction/details/d/d/d;->a:Lco/uk/getmondo/d/aj;

    invoke-virtual {v1}, Lco/uk/getmondo/d/aj;->p()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lco/uk/getmondo/transaction/details/d/d/d;->f:Lco/uk/getmondo/common/accounts/b;

    invoke-virtual {v1}, Lco/uk/getmondo/common/accounts/b;->b()Z

    move-result v1

    if-nez v1, :cond_0

    .line 42
    new-instance v2, Lco/uk/getmondo/transaction/details/a/g;

    iget-object v1, p0, Lco/uk/getmondo/transaction/details/d/d/d;->a:Lco/uk/getmondo/d/aj;

    iget-object v3, p0, Lco/uk/getmondo/transaction/details/d/d/d;->c:Lco/uk/getmondo/transaction/a;

    iget-object v4, p0, Lco/uk/getmondo/transaction/details/d/d/d;->d:Lco/uk/getmondo/payments/send/data/p;

    iget-object v5, p0, Lco/uk/getmondo/transaction/details/d/d/d;->e:Lco/uk/getmondo/common/a;

    invoke-direct {v2, v1, v3, v4, v5}, Lco/uk/getmondo/transaction/details/a/g;-><init>(Lco/uk/getmondo/d/aj;Lco/uk/getmondo/transaction/a;Lco/uk/getmondo/payments/send/data/p;Lco/uk/getmondo/common/a;)V

    .line 44
    :goto_0
    iget-object v1, p0, Lco/uk/getmondo/transaction/details/d/d/d;->a:Lco/uk/getmondo/d/aj;

    invoke-virtual {v1}, Lco/uk/getmondo/d/aj;->p()Z

    move-result v1

    if-eqz v1, :cond_1

    move-object v1, v0

    .line 45
    :goto_1
    const/4 v0, 0x3

    new-array v3, v0, [Lco/uk/getmondo/transaction/details/b/a;

    const/4 v4, 0x0

    new-instance v0, Lco/uk/getmondo/transaction/details/a/b;

    iget-object v5, p0, Lco/uk/getmondo/transaction/details/d/d/d;->a:Lco/uk/getmondo/d/aj;

    invoke-direct {v0, v5}, Lco/uk/getmondo/transaction/details/a/b;-><init>(Lco/uk/getmondo/d/aj;)V

    check-cast v0, Lco/uk/getmondo/transaction/details/b/a;

    aput-object v0, v3, v4

    const/4 v4, 0x1

    move-object v0, v1

    check-cast v0, Lco/uk/getmondo/transaction/details/b/a;

    aput-object v0, v3, v4

    const/4 v1, 0x2

    move-object v0, v2

    check-cast v0, Lco/uk/getmondo/transaction/details/b/a;

    aput-object v0, v3, v1

    invoke-static {v3}, Lkotlin/a/m;->c([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0

    :cond_0
    move-object v2, v0

    .line 43
    goto :goto_0

    .line 44
    :cond_1
    new-instance v1, Lco/uk/getmondo/transaction/details/a/c;

    iget-object v0, p0, Lco/uk/getmondo/transaction/details/d/d/d;->a:Lco/uk/getmondo/d/aj;

    invoke-direct {v1, v0}, Lco/uk/getmondo/transaction/details/a/c;-><init>(Lco/uk/getmondo/d/aj;)V

    goto :goto_1
.end method

.method public c()Lco/uk/getmondo/transaction/details/b/e;
    .locals 1

    .prologue
    .line 49
    new-instance v0, Lco/uk/getmondo/transaction/details/d/d/d$a;

    invoke-direct {v0, p0}, Lco/uk/getmondo/transaction/details/d/d/d$a;-><init>(Lco/uk/getmondo/transaction/details/d/d/d;)V

    check-cast v0, Lco/uk/getmondo/transaction/details/b/e;

    .line 72
    return-object v0
.end method

.method public d()Lco/uk/getmondo/transaction/details/b/f;
    .locals 2

    .prologue
    .line 75
    new-instance v0, Lco/uk/getmondo/transaction/details/d/d/b;

    iget-object v1, p0, Lco/uk/getmondo/transaction/details/d/d/d;->a:Lco/uk/getmondo/d/aj;

    invoke-direct {v0, v1}, Lco/uk/getmondo/transaction/details/d/d/b;-><init>(Lco/uk/getmondo/d/aj;)V

    check-cast v0, Lco/uk/getmondo/transaction/details/b/f;

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_0

    instance-of v0, p1, Lco/uk/getmondo/transaction/details/d/d/d;

    if-eqz v0, :cond_1

    check-cast p1, Lco/uk/getmondo/transaction/details/d/d/d;

    iget-object v0, p0, Lco/uk/getmondo/transaction/details/d/d/d;->a:Lco/uk/getmondo/d/aj;

    iget-object v1, p1, Lco/uk/getmondo/transaction/details/d/d/d;->a:Lco/uk/getmondo/d/aj;

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lco/uk/getmondo/transaction/details/d/d/d;->b:Lco/uk/getmondo/transaction/details/c/f;

    iget-object v1, p1, Lco/uk/getmondo/transaction/details/d/d/d;->b:Lco/uk/getmondo/transaction/details/c/f;

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lco/uk/getmondo/transaction/details/d/d/d;->c:Lco/uk/getmondo/transaction/a;

    iget-object v1, p1, Lco/uk/getmondo/transaction/details/d/d/d;->c:Lco/uk/getmondo/transaction/a;

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lco/uk/getmondo/transaction/details/d/d/d;->d:Lco/uk/getmondo/payments/send/data/p;

    iget-object v1, p1, Lco/uk/getmondo/transaction/details/d/d/d;->d:Lco/uk/getmondo/payments/send/data/p;

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lco/uk/getmondo/transaction/details/d/d/d;->e:Lco/uk/getmondo/common/a;

    iget-object v1, p1, Lco/uk/getmondo/transaction/details/d/d/d;->e:Lco/uk/getmondo/common/a;

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lco/uk/getmondo/transaction/details/d/d/d;->f:Lco/uk/getmondo/common/accounts/b;

    iget-object v1, p1, Lco/uk/getmondo/transaction/details/d/d/d;->f:Lco/uk/getmondo/common/accounts/b;

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    const/4 v1, 0x0

    iget-object v0, p0, Lco/uk/getmondo/transaction/details/d/d/d;->a:Lco/uk/getmondo/d/aj;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    :goto_0
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lco/uk/getmondo/transaction/details/d/d/d;->b:Lco/uk/getmondo/transaction/details/c/f;

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lco/uk/getmondo/transaction/details/d/d/d;->c:Lco/uk/getmondo/transaction/a;

    if-eqz v0, :cond_3

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    :goto_2
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lco/uk/getmondo/transaction/details/d/d/d;->d:Lco/uk/getmondo/payments/send/data/p;

    if-eqz v0, :cond_4

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    :goto_3
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lco/uk/getmondo/transaction/details/d/d/d;->e:Lco/uk/getmondo/common/a;

    if-eqz v0, :cond_5

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    :goto_4
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lco/uk/getmondo/transaction/details/d/d/d;->f:Lco/uk/getmondo/common/accounts/b;

    if-eqz v2, :cond_0

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_0
    add-int/2addr v0, v1

    return v0

    :cond_1
    move v0, v1

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_1

    :cond_3
    move v0, v1

    goto :goto_2

    :cond_4
    move v0, v1

    goto :goto_3

    :cond_5
    move v0, v1

    goto :goto_4
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "GeneralTransaction(transaction="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/transaction/details/d/d/d;->a:Lco/uk/getmondo/d/aj;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", transactionHistory="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/transaction/details/d/d/d;->b:Lco/uk/getmondo/transaction/details/c/f;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", costSplitter="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/transaction/details/d/d/d;->c:Lco/uk/getmondo/transaction/a;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", userSettingsStorage="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/transaction/details/d/d/d;->d:Lco/uk/getmondo/payments/send/data/p;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", analyticsService="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/transaction/details/d/d/d;->e:Lco/uk/getmondo/common/a;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", accountManager="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/transaction/details/d/d/d;->f:Lco/uk/getmondo/common/accounts/b;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
