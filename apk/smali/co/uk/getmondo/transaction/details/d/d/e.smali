.class public final Lco/uk/getmondo/transaction/details/d/d/e;
.super Ljava/lang/Object;
.source "UnenrichedAvatar.kt"

# interfaces
.implements Lco/uk/getmondo/transaction/details/b/c;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000\"\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0002\u0008\u0003\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004R\u0014\u0010\u0005\u001a\u00020\u00068VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0005\u0010\u0007R\u0014\u0010\u0008\u001a\u00020\t8VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\n\u0010\u000bR\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000c"
    }
    d2 = {
        "Lco/uk/getmondo/transaction/details/types/general/UnenrichedAvatar;",
        "Lco/uk/getmondo/transaction/details/base/Avatar;",
        "transaction",
        "Lco/uk/getmondo/model/Transaction;",
        "(Lco/uk/getmondo/model/Transaction;)V",
        "isSquare",
        "",
        "()Z",
        "name",
        "",
        "getName",
        "()Ljava/lang/String;",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field private final a:Lco/uk/getmondo/d/aj;


# direct methods
.method public constructor <init>(Lco/uk/getmondo/d/aj;)V
    .locals 1

    .prologue
    const-string v0, "transaction"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lco/uk/getmondo/transaction/details/d/d/e;->a:Lco/uk/getmondo/d/aj;

    return-void
.end method


# virtual methods
.method public a(Landroid/content/Context;)Ljava/lang/Integer;
    .locals 1

    .prologue
    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 6
    invoke-static {p0, p1}, Lco/uk/getmondo/transaction/details/b/c$a;->a(Lco/uk/getmondo/transaction/details/b/c;Landroid/content/Context;)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 6
    invoke-static {p0}, Lco/uk/getmondo/transaction/details/b/c$a;->a(Lco/uk/getmondo/transaction/details/b/c;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public b()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 6
    invoke-static {p0}, Lco/uk/getmondo/transaction/details/b/c$a;->b(Lco/uk/getmondo/transaction/details/b/c;)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public c()Ljava/lang/String;
    .locals 2

    .prologue
    .line 8
    iget-object v0, p0, Lco/uk/getmondo/transaction/details/d/d/e;->a:Lco/uk/getmondo/d/aj;

    invoke-virtual {v0}, Lco/uk/getmondo/d/aj;->x()Ljava/lang/String;

    move-result-object v0

    const-string v1, "transaction.description"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public d()Z
    .locals 1

    .prologue
    .line 11
    const/4 v0, 0x1

    return v0
.end method
