.class public final Lco/uk/getmondo/transaction/details/d/f/a;
.super Lco/uk/getmondo/transaction/details/b/d;
.source "P2pHeader.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u00002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u000b\n\u0002\u0008\u0003\n\u0002\u0010\u000e\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0008\u0010\r\u001a\u00020\u000eH\u0002J\u0012\u0010\u000f\u001a\u0004\u0018\u00010\u000e2\u0006\u0010\u0010\u001a\u00020\u0011H\u0016J\u0010\u0010\u0012\u001a\u00020\u000e2\u0006\u0010\u0010\u001a\u00020\u0011H\u0016R\u0014\u0010\u0005\u001a\u00020\u00068VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0007\u0010\u0008R\u0014\u0010\t\u001a\u00020\n8VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u000b\u0010\u000cR\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0013"
    }
    d2 = {
        "Lco/uk/getmondo/transaction/details/types/p2p/P2pHeader;",
        "Lco/uk/getmondo/transaction/details/base/BaseHeader;",
        "transaction",
        "Lco/uk/getmondo/model/Transaction;",
        "(Lco/uk/getmondo/model/Transaction;)V",
        "avatar",
        "Lco/uk/getmondo/transaction/details/base/Avatar;",
        "getAvatar",
        "()Lco/uk/getmondo/transaction/details/base/Avatar;",
        "showSubtitleBeforeTitle",
        "",
        "getShowSubtitleBeforeTitle",
        "()Z",
        "getTitle",
        "",
        "subtitle",
        "resources",
        "Landroid/content/res/Resources;",
        "title",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field private final a:Lco/uk/getmondo/d/aj;


# direct methods
.method public constructor <init>(Lco/uk/getmondo/d/aj;)V
    .locals 1

    .prologue
    const-string v0, "transaction"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 10
    invoke-direct {p0, p1}, Lco/uk/getmondo/transaction/details/b/d;-><init>(Lco/uk/getmondo/d/aj;)V

    iput-object p1, p0, Lco/uk/getmondo/transaction/details/d/f/a;->a:Lco/uk/getmondo/d/aj;

    return-void
.end method

.method public static final synthetic a(Lco/uk/getmondo/transaction/details/d/f/a;)Lco/uk/getmondo/d/aj;
    .locals 1

    .prologue
    .line 10
    iget-object v0, p0, Lco/uk/getmondo/transaction/details/d/f/a;->a:Lco/uk/getmondo/d/aj;

    return-object v0
.end method

.method public static final synthetic b(Lco/uk/getmondo/transaction/details/d/f/a;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 10
    invoke-direct {p0}, Lco/uk/getmondo/transaction/details/d/f/a;->k()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private final k()Ljava/lang/String;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 15
    iget-object v0, p0, Lco/uk/getmondo/transaction/details/d/f/a;->a:Lco/uk/getmondo/d/aj;

    invoke-virtual {v0}, Lco/uk/getmondo/d/aj;->r()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lco/uk/getmondo/transaction/details/d/f/a;->a:Lco/uk/getmondo/d/aj;

    invoke-virtual {v0}, Lco/uk/getmondo/d/aj;->B()Lco/uk/getmondo/d/aa;

    move-result-object v0

    invoke-virtual {v0}, Lco/uk/getmondo/d/aa;->b()Ljava/lang/String;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-static {v0}, Lkotlin/h/j;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 16
    iget-object v0, p0, Lco/uk/getmondo/transaction/details/d/f/a;->a:Lco/uk/getmondo/d/aj;

    invoke-virtual {v0}, Lco/uk/getmondo/d/aj;->C()Lco/uk/getmondo/payments/send/data/a/a;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lco/uk/getmondo/payments/send/data/a/a;->b()Ljava/lang/String;

    move-result-object v0

    :goto_0
    if-nez v0, :cond_0

    invoke-static {}, Lkotlin/d/b/l;->a()V

    :cond_0
    invoke-static {v0}, Lco/uk/getmondo/common/k/o;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 17
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " \u2022 "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v0, p0, Lco/uk/getmondo/transaction/details/d/f/a;->a:Lco/uk/getmondo/d/aj;

    invoke-virtual {v0}, Lco/uk/getmondo/d/aj;->C()Lco/uk/getmondo/payments/send/data/a/a;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lco/uk/getmondo/payments/send/data/a/a;->c()Ljava/lang/String;

    move-result-object v0

    :goto_1
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 19
    :goto_2
    return-object v0

    :cond_1
    move-object v0, v1

    .line 16
    goto :goto_0

    :cond_2
    move-object v0, v1

    .line 17
    goto :goto_1

    .line 19
    :cond_3
    iget-object v0, p0, Lco/uk/getmondo/transaction/details/d/f/a;->a:Lco/uk/getmondo/d/aj;

    invoke-virtual {v0}, Lco/uk/getmondo/d/aj;->B()Lco/uk/getmondo/d/aa;

    move-result-object v0

    invoke-virtual {v0}, Lco/uk/getmondo/d/aa;->b()Ljava/lang/String;

    move-result-object v0

    const-string v1, "transaction.peer.name"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_2
.end method


# virtual methods
.method public a(Landroid/content/res/Resources;)Ljava/lang/String;
    .locals 1

    .prologue
    const-string v0, "resources"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 23
    iget-object v0, p0, Lco/uk/getmondo/transaction/details/d/f/a;->a:Lco/uk/getmondo/d/aj;

    invoke-virtual {v0}, Lco/uk/getmondo/d/aj;->g()Lco/uk/getmondo/d/c;

    move-result-object v0

    invoke-virtual {v0}, Lco/uk/getmondo/d/c;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    const v0, 0x7f0a040c

    :goto_0
    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 24
    return-object v0

    :cond_0
    const v0, 0x7f0a040b

    goto :goto_0
.end method

.method public c(Landroid/content/res/Resources;)Ljava/lang/String;
    .locals 1

    .prologue
    const-string v0, "resources"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 12
    invoke-direct {p0}, Lco/uk/getmondo/transaction/details/d/f/a;->k()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public g()Z
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lco/uk/getmondo/transaction/details/d/f/a;->a:Lco/uk/getmondo/d/aj;

    invoke-virtual {v0}, Lco/uk/getmondo/d/aj;->g()Lco/uk/getmondo/d/c;

    move-result-object v0

    invoke-virtual {v0}, Lco/uk/getmondo/d/c;->a()Z

    move-result v0

    return v0
.end method

.method public j()Lco/uk/getmondo/transaction/details/b/c;
    .locals 1

    .prologue
    .line 30
    new-instance v0, Lco/uk/getmondo/transaction/details/d/f/a$a;

    invoke-direct {v0, p0}, Lco/uk/getmondo/transaction/details/d/f/a$a;-><init>(Lco/uk/getmondo/transaction/details/d/f/a;)V

    check-cast v0, Lco/uk/getmondo/transaction/details/b/c;

    .line 39
    return-object v0
.end method
