.class public final Lco/uk/getmondo/transaction/details/d/f/b;
.super Ljava/lang/Object;
.source "P2pHistory.kt"

# interfaces
.implements Lco/uk/getmondo/transaction/details/b/n;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u00002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u00002\u00020\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\u0012\u0010\u000c\u001a\u0004\u0018\u00010\r2\u0006\u0010\u000e\u001a\u00020\u000fH\u0016J\u0010\u0010\u0010\u001a\u00020\r2\u0006\u0010\u000e\u001a\u00020\u000fH\u0016R\u001a\u0010\u0007\u001a\u0008\u0012\u0004\u0012\u00020\t0\u00088VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\n\u0010\u000bR\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0011"
    }
    d2 = {
        "Lco/uk/getmondo/transaction/details/types/p2p/P2pHistory;",
        "Lco/uk/getmondo/transaction/details/base/YourHistory;",
        "transaction",
        "Lco/uk/getmondo/model/Transaction;",
        "transactionHistory",
        "Lco/uk/getmondo/transaction/details/model/CashFlowHistory;",
        "(Lco/uk/getmondo/model/Transaction;Lco/uk/getmondo/transaction/details/model/CashFlowHistory;)V",
        "items",
        "",
        "Lco/uk/getmondo/transaction/details/base/YourHistoryItem;",
        "getItems",
        "()Ljava/util/List;",
        "searchQuery",
        "",
        "resources",
        "Landroid/content/res/Resources;",
        "title",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field private final a:Lco/uk/getmondo/d/aj;

.field private final b:Lco/uk/getmondo/transaction/details/c/b;


# direct methods
.method public constructor <init>(Lco/uk/getmondo/d/aj;Lco/uk/getmondo/transaction/details/c/b;)V
    .locals 1

    .prologue
    const-string v0, "transaction"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "transactionHistory"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lco/uk/getmondo/transaction/details/d/f/b;->a:Lco/uk/getmondo/d/aj;

    iput-object p2, p0, Lco/uk/getmondo/transaction/details/d/f/b;->b:Lco/uk/getmondo/transaction/details/c/b;

    return-void
.end method

.method public static final synthetic a(Lco/uk/getmondo/transaction/details/d/f/b;)Lco/uk/getmondo/transaction/details/c/b;
    .locals 1

    .prologue
    .line 10
    iget-object v0, p0, Lco/uk/getmondo/transaction/details/d/f/b;->b:Lco/uk/getmondo/transaction/details/c/b;

    return-object v0
.end method


# virtual methods
.method public a(Landroid/content/res/Resources;)Ljava/lang/String;
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    const-string v0, "resources"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 16
    iget-object v0, p0, Lco/uk/getmondo/transaction/details/d/f/b;->a:Lco/uk/getmondo/d/aj;

    invoke-virtual {v0}, Lco/uk/getmondo/d/aj;->B()Lco/uk/getmondo/d/aa;

    move-result-object v0

    invoke-virtual {v0}, Lco/uk/getmondo/d/aa;->b()Ljava/lang/String;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    if-eqz v0, :cond_0

    invoke-static {v0}, Lkotlin/h/j;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    move v0, v2

    :goto_0
    if-eqz v0, :cond_2

    .line 17
    const v0, 0x7f0a0353

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "resources.getString(R.string.send_money_history)"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 20
    :goto_1
    return-object v0

    :cond_1
    move v0, v1

    .line 16
    goto :goto_0

    .line 20
    :cond_2
    const v0, 0x7f0a0354

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v3, p0, Lco/uk/getmondo/transaction/details/d/f/b;->a:Lco/uk/getmondo/d/aj;

    invoke-virtual {v3}, Lco/uk/getmondo/d/aj;->B()Lco/uk/getmondo/d/aa;

    move-result-object v3

    invoke-virtual {v3}, Lco/uk/getmondo/d/aa;->b()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v1

    invoke-virtual {p1, v0, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "resources.getString(R.st\u2026t, transaction.peer.name)"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_1
.end method

.method public a()Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lco/uk/getmondo/transaction/details/b/o;",
            ">;"
        }
    .end annotation

    .prologue
    .line 25
    new-instance v0, Lco/uk/getmondo/transaction/details/d/f/b$a;

    invoke-direct {v0, p0}, Lco/uk/getmondo/transaction/details/d/f/b$a;-><init>(Lco/uk/getmondo/transaction/details/d/f/b;)V

    .line 36
    new-instance v1, Lco/uk/getmondo/transaction/details/d/f/b$c;

    invoke-direct {v1, p0}, Lco/uk/getmondo/transaction/details/d/f/b$c;-><init>(Lco/uk/getmondo/transaction/details/d/f/b;)V

    .line 50
    new-instance v2, Lco/uk/getmondo/transaction/details/d/f/b$b;

    invoke-direct {v2, p0}, Lco/uk/getmondo/transaction/details/d/f/b$b;-><init>(Lco/uk/getmondo/transaction/details/d/f/b;)V

    .line 64
    const/4 v3, 0x3

    new-array v3, v3, [Lco/uk/getmondo/transaction/details/b/o;

    const/4 v4, 0x0

    check-cast v0, Lco/uk/getmondo/transaction/details/b/o;

    aput-object v0, v3, v4

    const/4 v4, 0x1

    move-object v0, v1

    check-cast v0, Lco/uk/getmondo/transaction/details/b/o;

    aput-object v0, v3, v4

    const/4 v1, 0x2

    move-object v0, v2

    check-cast v0, Lco/uk/getmondo/transaction/details/b/o;

    aput-object v0, v3, v1

    invoke-static {v3}, Lkotlin/a/m;->b([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public b(Landroid/content/res/Resources;)Ljava/lang/String;
    .locals 1

    .prologue
    const-string v0, "resources"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 68
    iget-object v0, p0, Lco/uk/getmondo/transaction/details/d/f/b;->a:Lco/uk/getmondo/d/aj;

    invoke-virtual {v0}, Lco/uk/getmondo/d/aj;->B()Lco/uk/getmondo/d/aa;

    move-result-object v0

    invoke-virtual {v0}, Lco/uk/getmondo/d/aa;->b()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
