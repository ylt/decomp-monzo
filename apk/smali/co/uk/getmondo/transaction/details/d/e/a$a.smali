.class public final Lco/uk/getmondo/transaction/details/d/e/a$a;
.super Ljava/lang/Object;
.source "MerchantHistory.kt"

# interfaces
.implements Lco/uk/getmondo/transaction/details/b/o;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lco/uk/getmondo/transaction/details/d/e/a;->a()Ljava/util/List;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000\u001b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003*\u0001\u0000\u0008\n\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0012\u0010\u0003\u001a\u0004\u0018\u00010\u00042\u0006\u0010\u0005\u001a\u00020\u0006H\u0016J\u0010\u0010\u0007\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006H\u0016J\u0010\u0010\u0008\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006H\u0016\u00a8\u0006\t"
    }
    d2 = {
        "co/uk/getmondo/transaction/details/types/merchant/MerchantHistory$items$averageSpend$1",
        "Lco/uk/getmondo/transaction/details/base/YourHistoryItem;",
        "(Lco/uk/getmondo/transaction/details/types/merchant/MerchantHistory;)V",
        "subtitle",
        "",
        "resources",
        "Landroid/content/res/Resources;",
        "title",
        "value",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field final synthetic a:Lco/uk/getmondo/transaction/details/d/e/a;


# direct methods
.method constructor <init>(Lco/uk/getmondo/transaction/details/d/e/a;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 39
    iput-object p1, p0, Lco/uk/getmondo/transaction/details/d/e/a$a;->a:Lco/uk/getmondo/transaction/details/d/e/a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroid/content/res/Resources;)Ljava/lang/String;
    .locals 2

    .prologue
    const-string v0, "resources"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 40
    const v0, 0x7f0a0414

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "resources.getString(R.st\u2026x_merchant_average_spend)"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public b(Landroid/content/res/Resources;)Ljava/lang/String;
    .locals 5

    .prologue
    const-string v0, "resources"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 43
    iget-object v0, p0, Lco/uk/getmondo/transaction/details/d/e/a$a;->a:Lco/uk/getmondo/transaction/details/d/e/a;

    invoke-static {v0}, Lco/uk/getmondo/transaction/details/d/e/a;->b(Lco/uk/getmondo/transaction/details/d/e/a;)Lco/uk/getmondo/transaction/details/c/a;

    move-result-object v0

    invoke-virtual {v0}, Lco/uk/getmondo/transaction/details/c/a;->a()J

    move-result-wide v0

    long-to-int v0, v0

    .line 44
    const v1, 0x7f120005

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {p1, v1, v0, v2}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public c(Landroid/content/res/Resources;)Ljava/lang/String;
    .locals 5

    .prologue
    const-string v0, "resources"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 48
    iget-object v0, p0, Lco/uk/getmondo/transaction/details/d/e/a$a;->a:Lco/uk/getmondo/transaction/details/d/e/a;

    invoke-static {v0}, Lco/uk/getmondo/transaction/details/d/e/a;->b(Lco/uk/getmondo/transaction/details/d/e/a;)Lco/uk/getmondo/transaction/details/c/a;

    move-result-object v0

    invoke-virtual {v0}, Lco/uk/getmondo/transaction/details/c/a;->b()Lco/uk/getmondo/d/c;

    move-result-object v0

    invoke-virtual {v0}, Lco/uk/getmondo/d/c;->e()D

    move-result-wide v0

    .line 49
    const v2, 0x7f0a0193

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    aput-object v0, v3, v4

    invoke-virtual {p1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "resources.getString(R.st\u2026o_part_amount_gbp, value)"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method
