.class public final Lco/uk/getmondo/transaction/details/d/d/a;
.super Ljava/lang/Object;
.source "EnrichedAvatar.kt"

# interfaces
.implements Lco/uk/getmondo/transaction/details/b/c;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000*\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0002\u0008\u0003\n\u0002\u0010\u000b\n\u0002\u0008\u0004\n\u0002\u0010\u0008\n\u0002\u0008\u0003\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004R\u0016\u0010\u0005\u001a\u0004\u0018\u00010\u00068VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0007\u0010\u0008R\u0014\u0010\t\u001a\u00020\n8VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\t\u0010\u000bR\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0016\u0010\u000c\u001a\u0004\u0018\u00010\u00068VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\r\u0010\u0008R\u0016\u0010\u000e\u001a\u0004\u0018\u00010\u000f8VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0010\u0010\u0011\u00a8\u0006\u0012"
    }
    d2 = {
        "Lco/uk/getmondo/transaction/details/types/general/EnrichedAvatar;",
        "Lco/uk/getmondo/transaction/details/base/Avatar;",
        "merchant",
        "Lco/uk/getmondo/model/Merchant;",
        "(Lco/uk/getmondo/model/Merchant;)V",
        "imageUrl",
        "",
        "getImageUrl",
        "()Ljava/lang/String;",
        "isSquare",
        "",
        "()Z",
        "name",
        "getName",
        "resourceId",
        "",
        "getResourceId",
        "()Ljava/lang/Integer;",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field private final a:Lco/uk/getmondo/d/u;


# direct methods
.method public constructor <init>(Lco/uk/getmondo/d/u;)V
    .locals 1

    .prologue
    const-string v0, "merchant"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lco/uk/getmondo/transaction/details/d/d/a;->a:Lco/uk/getmondo/d/u;

    return-void
.end method


# virtual methods
.method public a(Landroid/content/Context;)Ljava/lang/Integer;
    .locals 1

    .prologue
    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 7
    invoke-static {p0, p1}, Lco/uk/getmondo/transaction/details/b/c$a;->a(Lco/uk/getmondo/transaction/details/b/c;Landroid/content/Context;)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 9
    iget-object v0, p0, Lco/uk/getmondo/transaction/details/d/d/a;->a:Lco/uk/getmondo/d/u;

    invoke-virtual {v0}, Lco/uk/getmondo/d/u;->j()Ljava/lang/String;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_2

    const/4 v0, 0x0

    :goto_1
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lco/uk/getmondo/transaction/details/d/d/a;->a:Lco/uk/getmondo/d/u;

    invoke-virtual {v0}, Lco/uk/getmondo/d/u;->j()Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method

.method public b()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 12
    iget-object v0, p0, Lco/uk/getmondo/transaction/details/d/d/a;->a:Lco/uk/getmondo/d/u;

    invoke-virtual {v0}, Lco/uk/getmondo/d/u;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    const v0, 0x7f02011d

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 15
    iget-object v0, p0, Lco/uk/getmondo/transaction/details/d/d/a;->a:Lco/uk/getmondo/d/u;

    invoke-virtual {v0}, Lco/uk/getmondo/d/u;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lco/uk/getmondo/transaction/details/d/d/a;->a:Lco/uk/getmondo/d/u;

    invoke-virtual {v0}, Lco/uk/getmondo/d/u;->i()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public d()Z
    .locals 1

    .prologue
    .line 18
    const/4 v0, 0x1

    return v0
.end method
