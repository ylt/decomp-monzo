.class public final Lco/uk/getmondo/transaction/details/d/d/c;
.super Lco/uk/getmondo/transaction/details/b/d;
.source "GeneralHeader.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000>\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0012\u0010\r\u001a\u0004\u0018\u00010\u000e2\u0006\u0010\u000f\u001a\u00020\u0010H\u0016J\u0012\u0010\u0011\u001a\u0004\u0018\u00010\u000e2\u0006\u0010\u000f\u001a\u00020\u0010H\u0016J\u0010\u0010\u0012\u001a\u00020\u00132\u0006\u0010\u0014\u001a\u00020\u0015H\u0016J\u0010\u0010\u0016\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u0010H\u0016R\u0014\u0010\u0005\u001a\u00020\u00068VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0007\u0010\u0008R\u0016\u0010\t\u001a\u0004\u0018\u00010\n8VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u000b\u0010\u000cR\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0017"
    }
    d2 = {
        "Lco/uk/getmondo/transaction/details/types/general/GeneralHeader;",
        "Lco/uk/getmondo/transaction/details/base/BaseHeader;",
        "transaction",
        "Lco/uk/getmondo/model/Transaction;",
        "(Lco/uk/getmondo/model/Transaction;)V",
        "avatar",
        "Lco/uk/getmondo/transaction/details/base/Avatar;",
        "getAvatar",
        "()Lco/uk/getmondo/transaction/details/base/Avatar;",
        "mapCoordinates",
        "Lco/uk/getmondo/transaction/details/model/MapCoordinates;",
        "getMapCoordinates",
        "()Lco/uk/getmondo/transaction/details/model/MapCoordinates;",
        "extraInformation",
        "",
        "resources",
        "Landroid/content/res/Resources;",
        "subtitle",
        "subtitleTextAppearance",
        "Lco/uk/getmondo/transaction/details/base/AddressTextAppearance;",
        "context",
        "Landroid/content/Context;",
        "title",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field private final a:Lco/uk/getmondo/d/aj;


# direct methods
.method public constructor <init>(Lco/uk/getmondo/d/aj;)V
    .locals 1

    .prologue
    const-string v0, "transaction"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 12
    invoke-direct {p0, p1}, Lco/uk/getmondo/transaction/details/b/d;-><init>(Lco/uk/getmondo/d/aj;)V

    iput-object p1, p0, Lco/uk/getmondo/transaction/details/d/d/c;->a:Lco/uk/getmondo/d/aj;

    return-void
.end method


# virtual methods
.method public a(Landroid/content/res/Resources;)Ljava/lang/String;
    .locals 3

    .prologue
    const/4 v0, 0x0

    const-string v1, "resources"

    invoke-static {p1, v1}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 20
    iget-object v1, p0, Lco/uk/getmondo/transaction/details/d/d/c;->a:Lco/uk/getmondo/d/aj;

    invoke-virtual {v1}, Lco/uk/getmondo/d/aj;->f()Lco/uk/getmondo/d/u;

    move-result-object v1

    .line 21
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lco/uk/getmondo/d/u;->k()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 22
    :cond_0
    :goto_0
    return-object v0

    :cond_1
    invoke-virtual {v1}, Lco/uk/getmondo/d/u;->c()Lcom/c/b/b;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v1, v0}, Lcom/c/b/b;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public synthetic b(Landroid/content/Context;)Lco/uk/getmondo/transaction/details/b/j;
    .locals 1

    .prologue
    .line 12
    invoke-virtual {p0, p1}, Lco/uk/getmondo/transaction/details/d/d/c;->c(Landroid/content/Context;)Lco/uk/getmondo/transaction/details/b/b;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/transaction/details/b/j;

    return-object v0
.end method

.method public b(Landroid/content/res/Resources;)Ljava/lang/String;
    .locals 9

    .prologue
    const/4 v5, 0x3

    const/4 v8, 0x2

    const/4 v7, 0x0

    const/4 v6, 0x1

    const-string v0, "resources"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 28
    iget-object v0, p0, Lco/uk/getmondo/transaction/details/d/d/c;->a:Lco/uk/getmondo/d/aj;

    invoke-virtual {v0}, Lco/uk/getmondo/d/aj;->i()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 30
    iget-object v0, p0, Lco/uk/getmondo/transaction/details/d/d/c;->a:Lco/uk/getmondo/d/aj;

    invoke-virtual {v0}, Lco/uk/getmondo/d/aj;->g()Lco/uk/getmondo/d/c;

    move-result-object v0

    invoke-virtual {v0}, Lco/uk/getmondo/d/c;->g()Ljava/lang/String;

    move-result-object v0

    .line 31
    iget-object v1, p0, Lco/uk/getmondo/transaction/details/d/d/c;->a:Lco/uk/getmondo/d/aj;

    invoke-virtual {v1}, Lco/uk/getmondo/d/aj;->h()Lco/uk/getmondo/d/c;

    move-result-object v1

    invoke-virtual {v1}, Lco/uk/getmondo/d/c;->g()Ljava/lang/String;

    move-result-object v1

    .line 32
    iget-object v2, p0, Lco/uk/getmondo/transaction/details/d/d/c;->a:Lco/uk/getmondo/d/aj;

    invoke-virtual {v2}, Lco/uk/getmondo/d/aj;->j()D

    move-result-wide v2

    .line 33
    iget-object v4, p0, Lco/uk/getmondo/transaction/details/d/d/c;->a:Lco/uk/getmondo/d/aj;

    invoke-virtual {v4}, Lco/uk/getmondo/d/aj;->k()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 34
    const v4, 0x7f0a02fa

    new-array v5, v5, [Ljava/lang/Object;

    aput-object v0, v5, v7

    aput-object v1, v5, v6

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    aput-object v0, v5, v8

    invoke-virtual {p1, v4, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 33
    :goto_0
    return-object v0

    .line 36
    :cond_0
    const v4, 0x7f0a018d

    new-array v5, v5, [Ljava/lang/Object;

    aput-object v0, v5, v7

    aput-object v1, v5, v6

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    aput-object v0, v5, v8

    invoke-virtual {p1, v4, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 39
    :cond_1
    iget-object v0, p0, Lco/uk/getmondo/transaction/details/d/d/c;->a:Lco/uk/getmondo/d/aj;

    invoke-virtual {v0}, Lco/uk/getmondo/d/aj;->f()Lco/uk/getmondo/d/u;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lco/uk/getmondo/d/u;->k()Z

    move-result v0

    if-ne v0, v6, :cond_2

    const v0, 0x7f0a040a

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 40
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public c(Landroid/content/Context;)Lco/uk/getmondo/transaction/details/b/b;
    .locals 1

    .prologue
    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 25
    new-instance v0, Lco/uk/getmondo/transaction/details/b/b;

    invoke-direct {v0, p1}, Lco/uk/getmondo/transaction/details/b/b;-><init>(Landroid/content/Context;)V

    return-object v0
.end method

.method public c(Landroid/content/res/Resources;)Ljava/lang/String;
    .locals 2

    .prologue
    const-string v0, "resources"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 15
    iget-object v0, p0, Lco/uk/getmondo/transaction/details/d/d/c;->a:Lco/uk/getmondo/d/aj;

    invoke-virtual {v0}, Lco/uk/getmondo/d/aj;->f()Lco/uk/getmondo/d/u;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 16
    invoke-virtual {v0}, Lco/uk/getmondo/d/u;->g()Z

    move-result v1

    if-eqz v1, :cond_1

    const v0, 0x7f0a041b

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "resources.getString(R.string.tx_title_atm)"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    :goto_0
    return-object v0

    .line 15
    :cond_0
    iget-object v0, p0, Lco/uk/getmondo/transaction/details/d/d/c;->a:Lco/uk/getmondo/d/aj;

    invoke-virtual {v0}, Lco/uk/getmondo/d/aj;->x()Ljava/lang/String;

    move-result-object v0

    const-string v1, "transaction.description"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    .line 16
    :cond_1
    invoke-virtual {v0}, Lco/uk/getmondo/d/u;->i()Ljava/lang/String;

    move-result-object v0

    const-string v1, "merchant.name"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public h()Lco/uk/getmondo/transaction/details/c/c;
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 52
    iget-object v1, p0, Lco/uk/getmondo/transaction/details/d/d/c;->a:Lco/uk/getmondo/d/aj;

    invoke-virtual {v1}, Lco/uk/getmondo/d/aj;->f()Lco/uk/getmondo/d/u;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 53
    invoke-virtual {v1}, Lco/uk/getmondo/d/u;->k()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 56
    :cond_0
    :goto_0
    return-object v0

    .line 54
    :cond_1
    invoke-virtual {v1}, Lco/uk/getmondo/d/u;->d()Ljava/lang/Double;

    move-result-object v2

    .line 55
    invoke-virtual {v1}, Lco/uk/getmondo/d/u;->e()Ljava/lang/Double;

    move-result-object v4

    .line 56
    if-eqz v2, :cond_0

    if-eqz v4, :cond_0

    new-instance v0, Lco/uk/getmondo/transaction/details/c/c;

    invoke-virtual {v1}, Lco/uk/getmondo/d/u;->i()Ljava/lang/String;

    move-result-object v1

    const-string v3, "merchant.name"

    invoke-static {v1, v3}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v2

    invoke-virtual {v4}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v4

    invoke-direct/range {v0 .. v5}, Lco/uk/getmondo/transaction/details/c/c;-><init>(Ljava/lang/String;DD)V

    goto :goto_0
.end method

.method public j()Lco/uk/getmondo/transaction/details/b/c;
    .locals 2

    .prologue
    .line 46
    iget-object v0, p0, Lco/uk/getmondo/transaction/details/d/d/c;->a:Lco/uk/getmondo/d/aj;

    invoke-virtual {v0}, Lco/uk/getmondo/d/aj;->f()Lco/uk/getmondo/d/u;

    move-result-object v1

    .line 47
    if-nez v1, :cond_0

    new-instance v0, Lco/uk/getmondo/transaction/details/d/d/e;

    iget-object v1, p0, Lco/uk/getmondo/transaction/details/d/d/c;->a:Lco/uk/getmondo/d/aj;

    invoke-direct {v0, v1}, Lco/uk/getmondo/transaction/details/d/d/e;-><init>(Lco/uk/getmondo/d/aj;)V

    check-cast v0, Lco/uk/getmondo/transaction/details/b/c;

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lco/uk/getmondo/transaction/details/d/d/a;

    invoke-direct {v0, v1}, Lco/uk/getmondo/transaction/details/d/d/a;-><init>(Lco/uk/getmondo/d/u;)V

    check-cast v0, Lco/uk/getmondo/transaction/details/b/c;

    goto :goto_0
.end method
