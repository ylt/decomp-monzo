.class public final Lco/uk/getmondo/transaction/a;
.super Ljava/lang/Object;
.source "CostSplitter.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000*\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0000\u0018\u00002\u00020\u0001B\u000f\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0018\u0010\u0007\u001a\u00020\u00082\u0006\u0010\t\u001a\u00020\n2\u0008\u0008\u0002\u0010\u000b\u001a\u00020\u000cR\u000e\u0010\u0005\u001a\u00020\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\r"
    }
    d2 = {
        "Lco/uk/getmondo/transaction/CostSplitter;",
        "",
        "userSettingsRepository",
        "Lco/uk/getmondo/payments/send/data/UserSettingsRepository;",
        "(Lco/uk/getmondo/payments/send/data/UserSettingsRepository;)V",
        "formatter",
        "Lco/uk/getmondo/common/money/AmountFormatter;",
        "splitCost",
        "Lco/uk/getmondo/transaction/splitting/model/Split;",
        "transaction",
        "Lco/uk/getmondo/model/Transaction;",
        "numberOfPeople",
        "",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field private final a:Lco/uk/getmondo/common/i/b;

.field private final b:Lco/uk/getmondo/payments/send/data/h;


# direct methods
.method public constructor <init>(Lco/uk/getmondo/payments/send/data/h;)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    const-string v0, "userSettingsRepository"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lco/uk/getmondo/transaction/a;->b:Lco/uk/getmondo/payments/send/data/h;

    .line 12
    new-instance v0, Lco/uk/getmondo/common/i/b;

    const/4 v4, 0x7

    const/4 v5, 0x0

    move v2, v1

    move v3, v1

    invoke-direct/range {v0 .. v5}, Lco/uk/getmondo/common/i/b;-><init>(ZZZILkotlin/d/b/i;)V

    iput-object v0, p0, Lco/uk/getmondo/transaction/a;->a:Lco/uk/getmondo/common/i/b;

    return-void
.end method

.method public static bridge synthetic a(Lco/uk/getmondo/transaction/a;Lco/uk/getmondo/d/aj;IILjava/lang/Object;)Lco/uk/getmondo/transaction/splitting/a/a;
    .locals 1

    .prologue
    and-int/lit8 v0, p3, 0x2

    if-eqz v0, :cond_0

    .line 14
    const/4 p2, 0x4

    :cond_0
    invoke-virtual {p0, p1, p2}, Lco/uk/getmondo/transaction/a;->a(Lco/uk/getmondo/d/aj;I)Lco/uk/getmondo/transaction/splitting/a/a;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(Lco/uk/getmondo/d/aj;I)Lco/uk/getmondo/transaction/splitting/a/a;
    .locals 10

    .prologue
    const-string v0, "transaction"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 15
    iget-object v0, p0, Lco/uk/getmondo/transaction/a;->b:Lco/uk/getmondo/payments/send/data/h;

    invoke-virtual {v0}, Lco/uk/getmondo/payments/send/data/h;->e()Lco/uk/getmondo/d/p;

    move-result-object v4

    .line 16
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    check-cast v0, Ljava/util/List;

    .line 17
    const/4 v1, 0x0

    move v3, v1

    :goto_0
    if-ge v3, p2, :cond_2

    .line 19
    invoke-virtual {p1}, Lco/uk/getmondo/d/aj;->g()Lco/uk/getmondo/d/c;

    move-result-object v1

    add-int/lit8 v2, v3, 0x2

    invoke-virtual {v1, v2}, Lco/uk/getmondo/d/c;->a(I)Lco/uk/getmondo/d/c;

    move-result-object v5

    .line 20
    sget v2, Lco/uk/getmondo/transaction/splitting/a/a$c;->a:I

    .line 21
    const-string v1, ""

    .line 22
    invoke-virtual {v5}, Lco/uk/getmondo/d/c;->e()D

    move-result-wide v6

    invoke-virtual {v4}, Lco/uk/getmondo/d/p;->a()I

    move-result v8

    int-to-double v8, v8

    cmpl-double v6, v6, v8

    if-lez v6, :cond_1

    .line 23
    sget v2, Lco/uk/getmondo/transaction/splitting/a/a$c;->c:I

    .line 24
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5}, Lco/uk/getmondo/d/c;->g()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v4}, Lco/uk/getmondo/d/p;->a()I

    move-result v6

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 28
    :cond_0
    :goto_1
    new-instance v6, Lco/uk/getmondo/transaction/splitting/a/a$c;

    add-int/lit8 v7, v3, 0x1

    iget-object v8, p0, Lco/uk/getmondo/transaction/a;->a:Lco/uk/getmondo/common/i/b;

    invoke-virtual {v8, v5}, Lco/uk/getmondo/common/i/b;->a(Lco/uk/getmondo/d/c;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v6, v7, v5, v2, v1}, Lco/uk/getmondo/transaction/splitting/a/a$c;-><init>(ILjava/lang/String;ILjava/lang/String;)V

    invoke-interface {v0, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 17
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_0

    .line 25
    :cond_1
    invoke-virtual {v5}, Lco/uk/getmondo/d/c;->e()D

    move-result-wide v6

    invoke-virtual {v4}, Lco/uk/getmondo/d/p;->b()I

    move-result v8

    int-to-double v8, v8

    cmpg-double v6, v6, v8

    if-gez v6, :cond_0

    .line 26
    sget v2, Lco/uk/getmondo/transaction/splitting/a/a$c;->b:I

    .line 27
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5}, Lco/uk/getmondo/d/c;->g()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v4}, Lco/uk/getmondo/d/p;->b()I

    move-result v6

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    .line 31
    :cond_2
    new-instance v1, Lco/uk/getmondo/transaction/splitting/a/a;

    check-cast v0, Ljava/util/Collection;

    .line 35
    invoke-interface {v0}, Ljava/util/Collection;->size()I

    move-result v2

    new-array v2, v2, [Lco/uk/getmondo/transaction/splitting/a/a$c;

    invoke-interface {v0, v2}, Ljava/util/Collection;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_3

    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type kotlin.Array<T>"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_3
    check-cast v0, [Lco/uk/getmondo/transaction/splitting/a/a$c;

    .line 31
    invoke-direct {v1, v0}, Lco/uk/getmondo/transaction/splitting/a/a;-><init>([Lco/uk/getmondo/transaction/splitting/a/a$c;)V

    return-object v1
.end method
