.class public final Lco/uk/getmondo/c/a$b;
.super Landroid/support/v4/app/r;
.source "ContactsFragment.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lco/uk/getmondo/c/a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "b"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000,\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0007\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\r\n\u0000\u0018\u00002\u00020\u0001B\u001d\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\u0008J\u0008\u0010\r\u001a\u00020\u0007H\u0016J\u0010\u0010\u000e\u001a\u00020\u000f2\u0006\u0010\u0010\u001a\u00020\u0007H\u0016J\u0010\u0010\u0011\u001a\u00020\u00122\u0006\u0010\u0010\u001a\u00020\u0007H\u0016R\u0011\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\t\u0010\nR\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000b\u0010\u000c\u00a8\u0006\u0013"
    }
    d2 = {
        "Lco/uk/getmondo/contacts/ContactsFragment$ContactsPagerAdapter;",
        "Landroid/support/v4/app/FragmentPagerAdapter;",
        "supportFragmentManager",
        "Landroid/support/v4/app/FragmentManager;",
        "resources",
        "Landroid/content/res/Resources;",
        "pageCount",
        "",
        "(Landroid/support/v4/app/FragmentManager;Landroid/content/res/Resources;I)V",
        "getPageCount",
        "()I",
        "getResources",
        "()Landroid/content/res/Resources;",
        "getCount",
        "getItem",
        "Landroid/support/v4/app/Fragment;",
        "position",
        "getPageTitle",
        "",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field private final a:Landroid/content/res/Resources;

.field private final b:I


# direct methods
.method public constructor <init>(Landroid/support/v4/app/n;Landroid/content/res/Resources;I)V
    .locals 1

    .prologue
    const-string v0, "supportFragmentManager"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "resources"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 99
    invoke-direct {p0, p1}, Landroid/support/v4/app/r;-><init>(Landroid/support/v4/app/n;)V

    iput-object p2, p0, Lco/uk/getmondo/c/a$b;->a:Landroid/content/res/Resources;

    iput p3, p0, Lco/uk/getmondo/c/a$b;->b:I

    return-void
.end method


# virtual methods
.method public a(I)Landroid/support/v4/app/Fragment;
    .locals 2

    .prologue
    .line 102
    .line 103
    sget-object v0, Lco/uk/getmondo/c/a;->c:Lco/uk/getmondo/c/a$a;

    invoke-static {v0}, Lco/uk/getmondo/c/a$a;->a(Lco/uk/getmondo/c/a$a;)I

    move-result v0

    if-ne p1, v0, :cond_0

    invoke-static {}, Lco/uk/getmondo/payments/send/SendMoneyFragment;->a()Landroid/support/v4/app/Fragment;

    move-result-object v0

    const-string v1, "SendMoneyFragment.newInstance()"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 102
    :goto_0
    return-object v0

    .line 104
    :cond_0
    sget-object v0, Lco/uk/getmondo/c/a;->c:Lco/uk/getmondo/c/a$a;

    invoke-static {v0}, Lco/uk/getmondo/c/a$a;->b(Lco/uk/getmondo/c/a$a;)I

    move-result v0

    if-ne p1, v0, :cond_1

    invoke-static {}, Lco/uk/getmondo/monzo/me/request/RequestMoneyFragment;->a()Landroid/support/v4/app/Fragment;

    move-result-object v0

    const-string v1, "RequestMoneyFragment.newInstance()"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    .line 105
    :cond_1
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Invalid position for contacts pager adapter"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method

.method public b()I
    .locals 1

    .prologue
    .line 118
    iget v0, p0, Lco/uk/getmondo/c/a$b;->b:I

    return v0
.end method

.method public c(I)Ljava/lang/CharSequence;
    .locals 2

    .prologue
    .line 110
    .line 111
    sget-object v0, Lco/uk/getmondo/c/a;->c:Lco/uk/getmondo/c/a$a;

    invoke-static {v0}, Lco/uk/getmondo/c/a$a;->a(Lco/uk/getmondo/c/a$a;)I

    move-result v0

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lco/uk/getmondo/c/a$b;->a:Landroid/content/res/Resources;

    const v1, 0x7f0a0127

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "resources.getString(R.st\u2026ontacts_title_send_money)"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Ljava/lang/CharSequence;

    .line 110
    :goto_0
    return-object v0

    .line 112
    :cond_0
    sget-object v0, Lco/uk/getmondo/c/a;->c:Lco/uk/getmondo/c/a$a;

    invoke-static {v0}, Lco/uk/getmondo/c/a$a;->b(Lco/uk/getmondo/c/a$a;)I

    move-result v0

    if-ne p1, v0, :cond_1

    iget-object v0, p0, Lco/uk/getmondo/c/a$b;->a:Landroid/content/res/Resources;

    const v1, 0x7f0a0126

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "resources.getString(R.st\u2026acts_title_request_money)"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Ljava/lang/CharSequence;

    goto :goto_0

    .line 113
    :cond_1
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Invalid position for contacts pager adapter"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method
