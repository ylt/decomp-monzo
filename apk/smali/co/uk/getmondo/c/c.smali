.class public final Lco/uk/getmondo/c/c;
.super Lco/uk/getmondo/common/ui/b;
.source "ContactsPresenter.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lco/uk/getmondo/c/c$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lco/uk/getmondo/common/ui/b",
        "<",
        "Lco/uk/getmondo/c/c$a;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000D\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u0003\u0008\u0007\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00020\u0001:\u0001\u0016BK\u0008\u0007\u0012\u0008\u0008\u0001\u0010\u0003\u001a\u00020\u0004\u0012\u0008\u0008\u0001\u0010\u0005\u001a\u00020\u0004\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b\u0012\u0006\u0010\u000c\u001a\u00020\r\u0012\u0006\u0010\u000e\u001a\u00020\u000f\u0012\u0006\u0010\u0010\u001a\u00020\u0011\u00a2\u0006\u0002\u0010\u0012J\u0010\u0010\u0013\u001a\u00020\u00142\u0006\u0010\u0015\u001a\u00020\u0002H\u0016R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000c\u001a\u00020\rX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u000fX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0010\u001a\u00020\u0011X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0017"
    }
    d2 = {
        "Lco/uk/getmondo/contacts/ContactsPresenter;",
        "Lco/uk/getmondo/common/ui/BasePresenter;",
        "Lco/uk/getmondo/contacts/ContactsPresenter$View;",
        "ioScheduler",
        "Lio/reactivex/Scheduler;",
        "uiScheduler",
        "analyticsService",
        "Lco/uk/getmondo/common/AnalyticsService;",
        "accountManager",
        "Lco/uk/getmondo/common/accounts/AccountManager;",
        "balanceRepository",
        "Lco/uk/getmondo/account_balance/BalanceRepository;",
        "apiErrorHandler",
        "Lco/uk/getmondo/common/errors/ApiErrorHandler;",
        "cardManager",
        "Lco/uk/getmondo/card/CardManager;",
        "featureFlagsStorage",
        "Lco/uk/getmondo/common/FeatureFlagsStorage;",
        "(Lio/reactivex/Scheduler;Lio/reactivex/Scheduler;Lco/uk/getmondo/common/AnalyticsService;Lco/uk/getmondo/common/accounts/AccountManager;Lco/uk/getmondo/account_balance/BalanceRepository;Lco/uk/getmondo/common/errors/ApiErrorHandler;Lco/uk/getmondo/card/CardManager;Lco/uk/getmondo/common/FeatureFlagsStorage;)V",
        "register",
        "",
        "view",
        "View",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field private final c:Lio/reactivex/u;

.field private final d:Lio/reactivex/u;

.field private final e:Lco/uk/getmondo/common/a;

.field private final f:Lco/uk/getmondo/common/accounts/b;

.field private final g:Lco/uk/getmondo/a/a;

.field private final h:Lco/uk/getmondo/common/e/a;

.field private final i:Lco/uk/getmondo/card/c;

.field private final j:Lco/uk/getmondo/common/o;


# direct methods
.method public constructor <init>(Lio/reactivex/u;Lio/reactivex/u;Lco/uk/getmondo/common/a;Lco/uk/getmondo/common/accounts/b;Lco/uk/getmondo/a/a;Lco/uk/getmondo/common/e/a;Lco/uk/getmondo/card/c;Lco/uk/getmondo/common/o;)V
    .locals 1

    .prologue
    const-string v0, "ioScheduler"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "uiScheduler"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "analyticsService"

    invoke-static {p3, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "accountManager"

    invoke-static {p4, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "balanceRepository"

    invoke-static {p5, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "apiErrorHandler"

    invoke-static {p6, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "cardManager"

    invoke-static {p7, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "featureFlagsStorage"

    invoke-static {p8, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 33
    invoke-direct {p0}, Lco/uk/getmondo/common/ui/b;-><init>()V

    iput-object p1, p0, Lco/uk/getmondo/c/c;->c:Lio/reactivex/u;

    iput-object p2, p0, Lco/uk/getmondo/c/c;->d:Lio/reactivex/u;

    iput-object p3, p0, Lco/uk/getmondo/c/c;->e:Lco/uk/getmondo/common/a;

    iput-object p4, p0, Lco/uk/getmondo/c/c;->f:Lco/uk/getmondo/common/accounts/b;

    iput-object p5, p0, Lco/uk/getmondo/c/c;->g:Lco/uk/getmondo/a/a;

    iput-object p6, p0, Lco/uk/getmondo/c/c;->h:Lco/uk/getmondo/common/e/a;

    iput-object p7, p0, Lco/uk/getmondo/c/c;->i:Lco/uk/getmondo/card/c;

    iput-object p8, p0, Lco/uk/getmondo/c/c;->j:Lco/uk/getmondo/common/o;

    return-void
.end method

.method public static final synthetic a(Lco/uk/getmondo/c/c;)Lco/uk/getmondo/common/e/a;
    .locals 1

    .prologue
    .line 23
    iget-object v0, p0, Lco/uk/getmondo/c/c;->h:Lco/uk/getmondo/common/e/a;

    return-object v0
.end method


# virtual methods
.method public a(Lco/uk/getmondo/c/c$a;)V
    .locals 4

    .prologue
    const-string v0, "view"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v0, p1

    .line 36
    check-cast v0, Lco/uk/getmondo/common/ui/f;

    invoke-super {p0, v0}, Lco/uk/getmondo/common/ui/b;->a(Lco/uk/getmondo/common/ui/f;)V

    .line 38
    iget-object v0, p0, Lco/uk/getmondo/c/c;->e:Lco/uk/getmondo/common/a;

    sget-object v1, Lco/uk/getmondo/api/model/tracking/Impression;->Companion:Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    invoke-virtual {v1}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->aC()Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v1

    invoke-virtual {v0, v1}, Lco/uk/getmondo/common/a;->a(Lco/uk/getmondo/api/model/tracking/Impression;)V

    .line 40
    iget-object v0, p0, Lco/uk/getmondo/c/c;->f:Lco/uk/getmondo/common/accounts/b;

    invoke-virtual {v0}, Lco/uk/getmondo/common/accounts/b;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 41
    invoke-interface {p1}, Lco/uk/getmondo/c/c$a;->b()V

    .line 43
    iget-object v1, p0, Lco/uk/getmondo/c/c;->b:Lio/reactivex/b/a;

    .line 46
    iget-object v0, p0, Lco/uk/getmondo/c/c;->j:Lco/uk/getmondo/common/o;

    invoke-virtual {v0}, Lco/uk/getmondo/common/o;->a()Lio/reactivex/n;

    move-result-object v2

    .line 44
    sget-object v0, Lco/uk/getmondo/c/c$b;->a:Lco/uk/getmondo/c/c$b;

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {v2, v0}, Lio/reactivex/n;->map(Lio/reactivex/c/h;)Lio/reactivex/n;

    move-result-object v0

    .line 45
    invoke-virtual {v0}, Lio/reactivex/n;->distinctUntilChanged()Lio/reactivex/n;

    move-result-object v2

    .line 46
    new-instance v0, Lco/uk/getmondo/c/c$c;

    invoke-direct {v0, p1}, Lco/uk/getmondo/c/c$c;-><init>(Lco/uk/getmondo/c/c$a;)V

    check-cast v0, Lio/reactivex/c/g;

    invoke-virtual {v2, v0}, Lio/reactivex/n;->subscribe(Lio/reactivex/c/g;)Lio/reactivex/b/b;

    move-result-object v0

    const-string v2, "featureFlagsStorage.feat\u2026tContactsFabVisible(it) }"

    invoke-static {v0, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 43
    invoke-static {v1, v0}, Lco/uk/getmondo/common/j/f;->a(Lio/reactivex/b/a;Lio/reactivex/b/b;)Lio/reactivex/b/a;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/c/c;->b:Lio/reactivex/b/a;

    .line 50
    :goto_0
    iget-object v2, p0, Lco/uk/getmondo/c/c;->b:Lio/reactivex/b/a;

    .line 55
    iget-object v0, p0, Lco/uk/getmondo/c/c;->g:Lco/uk/getmondo/a/a;

    iget-object v1, p0, Lco/uk/getmondo/c/c;->f:Lco/uk/getmondo/common/accounts/b;

    invoke-virtual {v1}, Lco/uk/getmondo/common/accounts/b;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lco/uk/getmondo/a/a;->b(Ljava/lang/String;)Lio/reactivex/b;

    move-result-object v0

    .line 53
    iget-object v1, p0, Lco/uk/getmondo/c/c;->c:Lio/reactivex/u;

    invoke-virtual {v0, v1}, Lio/reactivex/b;->b(Lio/reactivex/u;)Lio/reactivex/b;

    move-result-object v0

    .line 54
    iget-object v1, p0, Lco/uk/getmondo/c/c;->d:Lio/reactivex/u;

    invoke-virtual {v0, v1}, Lio/reactivex/b;->a(Lio/reactivex/u;)Lio/reactivex/b;

    move-result-object v3

    .line 55
    sget-object v0, Lco/uk/getmondo/c/c$d;->a:Lco/uk/getmondo/c/c$d;

    check-cast v0, Lio/reactivex/c/a;

    new-instance v1, Lco/uk/getmondo/c/c$e;

    invoke-direct {v1, p0, p1}, Lco/uk/getmondo/c/c$e;-><init>(Lco/uk/getmondo/c/c;Lco/uk/getmondo/c/c$a;)V

    check-cast v1, Lio/reactivex/c/g;

    invoke-virtual {v3, v0, v1}, Lio/reactivex/b;->a(Lio/reactivex/c/a;Lio/reactivex/c/g;)Lio/reactivex/b/b;

    move-result-object v0

    const-string v1, "balanceRepository.refres\u2026ndleError(error, view) })"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 52
    invoke-static {v2, v0}, Lco/uk/getmondo/common/j/f;->a(Lio/reactivex/b/a;Lio/reactivex/b/b;)Lio/reactivex/b/a;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/c/c;->b:Lio/reactivex/b/a;

    .line 57
    iget-object v2, p0, Lco/uk/getmondo/c/c;->b:Lio/reactivex/b/a;

    .line 61
    iget-object v0, p0, Lco/uk/getmondo/c/c;->g:Lco/uk/getmondo/a/a;

    iget-object v1, p0, Lco/uk/getmondo/c/c;->f:Lco/uk/getmondo/common/accounts/b;

    invoke-virtual {v1}, Lco/uk/getmondo/common/accounts/b;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lco/uk/getmondo/a/a;->a(Ljava/lang/String;)Lio/reactivex/n;

    move-result-object v0

    .line 58
    iget-object v1, p0, Lco/uk/getmondo/c/c;->d:Lio/reactivex/u;

    invoke-virtual {v0, v1}, Lio/reactivex/n;->observeOn(Lio/reactivex/u;)Lio/reactivex/n;

    move-result-object v1

    .line 59
    sget-object v0, Lco/uk/getmondo/c/c$f;->a:Lco/uk/getmondo/c/c$f;

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {v1, v0}, Lio/reactivex/n;->map(Lio/reactivex/c/h;)Lio/reactivex/n;

    move-result-object v1

    .line 60
    iget-object v0, p0, Lco/uk/getmondo/c/c;->i:Lco/uk/getmondo/card/c;

    invoke-virtual {v0}, Lco/uk/getmondo/card/c;->a()Lio/reactivex/n;

    move-result-object v0

    check-cast v0, Lio/reactivex/r;

    invoke-static {v1, v0}, Lco/uk/getmondo/common/j/f;->a(Lio/reactivex/n;Lio/reactivex/r;)Lio/reactivex/n;

    move-result-object v3

    .line 61
    new-instance v0, Lco/uk/getmondo/c/c$g;

    invoke-direct {v0, p1}, Lco/uk/getmondo/c/c$g;-><init>(Lco/uk/getmondo/c/c$a;)V

    check-cast v0, Lio/reactivex/c/g;

    .line 67
    sget-object v1, Lco/uk/getmondo/c/c$h;->a:Lco/uk/getmondo/c/c$h;

    check-cast v1, Lio/reactivex/c/g;

    .line 61
    invoke-virtual {v3, v0, v1}, Lio/reactivex/n;->subscribe(Lio/reactivex/c/g;Lio/reactivex/c/g;)Lio/reactivex/b/b;

    move-result-object v0

    const-string v1, "balanceRepository.balanc\u2026     }, { Timber.e(it) })"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 57
    invoke-static {v2, v0}, Lco/uk/getmondo/common/j/f;->a(Lio/reactivex/b/a;Lio/reactivex/b/b;)Lio/reactivex/b/a;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/c/c;->b:Lio/reactivex/b/a;

    .line 68
    return-void

    .line 48
    :cond_0
    invoke-interface {p1}, Lco/uk/getmondo/c/c$a;->a()V

    .line 49
    const/4 v0, 0x1

    invoke-interface {p1, v0}, Lco/uk/getmondo/c/c$a;->a(Z)V

    goto :goto_0
.end method

.method public bridge synthetic a(Lco/uk/getmondo/common/ui/f;)V
    .locals 0

    .prologue
    .line 23
    check-cast p1, Lco/uk/getmondo/c/c$a;

    invoke-virtual {p0, p1}, Lco/uk/getmondo/c/c;->a(Lco/uk/getmondo/c/c$a;)V

    return-void
.end method
