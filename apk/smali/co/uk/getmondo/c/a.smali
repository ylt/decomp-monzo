.class public final Lco/uk/getmondo/c/a;
.super Lco/uk/getmondo/common/f/a;
.source "ContactsFragment.kt"

# interfaces
.implements Lco/uk/getmondo/c/c$a;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lco/uk/getmondo/c/a$b;,
        Lco/uk/getmondo/c/a$a;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000H\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0006\u0018\u0000  2\u00020\u00012\u00020\u0002:\u0002 !B\u0005\u00a2\u0006\u0002\u0010\u0003J\u0012\u0010\n\u001a\u00020\u000b2\u0008\u0010\u000c\u001a\u0004\u0018\u00010\rH\u0016J&\u0010\u000e\u001a\u0004\u0018\u00010\u000f2\u0006\u0010\u0010\u001a\u00020\u00112\u0008\u0010\u0012\u001a\u0004\u0018\u00010\u00132\u0008\u0010\u000c\u001a\u0004\u0018\u00010\rH\u0016J\u0008\u0010\u0014\u001a\u00020\u000bH\u0016J\u001c\u0010\u0015\u001a\u00020\u000b2\u0008\u0010\u0016\u001a\u0004\u0018\u00010\u000f2\u0008\u0010\u000c\u001a\u0004\u0018\u00010\rH\u0016J\u0010\u0010\u0017\u001a\u00020\u000b2\u0006\u0010\u0018\u001a\u00020\u0019H\u0016J\u0010\u0010\u001a\u001a\u00020\u000b2\u0006\u0010\u001b\u001a\u00020\u001cH\u0016J\u0008\u0010\u001d\u001a\u00020\u000bH\u0016J\u0008\u0010\u001e\u001a\u00020\u000bH\u0016J\u0008\u0010\u001f\u001a\u00020\u000bH\u0016R\u001e\u0010\u0004\u001a\u00020\u00058\u0006@\u0006X\u0087.\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u0006\u0010\u0007\"\u0004\u0008\u0008\u0010\t\u00a8\u0006\""
    }
    d2 = {
        "Lco/uk/getmondo/contacts/ContactsFragment;",
        "Lco/uk/getmondo/common/fragments/BaseFragment;",
        "Lco/uk/getmondo/contacts/ContactsPresenter$View;",
        "()V",
        "presenter",
        "Lco/uk/getmondo/contacts/ContactsPresenter;",
        "getPresenter",
        "()Lco/uk/getmondo/contacts/ContactsPresenter;",
        "setPresenter",
        "(Lco/uk/getmondo/contacts/ContactsPresenter;)V",
        "onCreate",
        "",
        "savedInstanceState",
        "Landroid/os/Bundle;",
        "onCreateView",
        "Landroid/view/View;",
        "inflater",
        "Landroid/view/LayoutInflater;",
        "container",
        "Landroid/view/ViewGroup;",
        "onDestroyView",
        "onViewCreated",
        "view",
        "setContactsFabVisible",
        "visible",
        "",
        "showBalance",
        "balance",
        "Lco/uk/getmondo/model/Amount;",
        "showCardFrozen",
        "showPrepaidUi",
        "showRetailUi",
        "Companion",
        "ContactsPagerAdapter",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# static fields
.field public static final c:Lco/uk/getmondo/c/a$a;

.field private static final d:I = 0x0

# The value of this static final field might be set in the static constructor
.field private static final e:I = 0x1


# instance fields
.field public a:Lco/uk/getmondo/c/c;

.field private f:Ljava/util/HashMap;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    new-instance v0, Lco/uk/getmondo/c/a$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lco/uk/getmondo/c/a$a;-><init>(Lkotlin/d/b/i;)V

    sput-object v0, Lco/uk/getmondo/c/a;->c:Lco/uk/getmondo/c/a$a;

    .line 124
    const/4 v0, 0x1

    sput v0, Lco/uk/getmondo/c/a;->e:I

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Lco/uk/getmondo/common/f/a;-><init>()V

    return-void
.end method

.method public static final synthetic d()I
    .locals 1

    .prologue
    .line 25
    sget v0, Lco/uk/getmondo/c/a;->d:I

    return v0
.end method

.method public static final synthetic e()I
    .locals 1

    .prologue
    .line 25
    sget v0, Lco/uk/getmondo/c/a;->e:I

    return v0
.end method


# virtual methods
.method public a(I)Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lco/uk/getmondo/c/a;->f:Ljava/util/HashMap;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lco/uk/getmondo/c/a;->f:Ljava/util/HashMap;

    :cond_0
    iget-object v0, p0, Lco/uk/getmondo/c/a;->f:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    if-nez v0, :cond_1

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getView()Landroid/view/View;

    move-result-object v0

    if-nez v0, :cond_2

    const/4 v0, 0x0

    :cond_1
    :goto_0
    return-object v0

    :cond_2
    invoke-virtual {v0, p1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/c/a;->f:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method public a()V
    .locals 5

    .prologue
    .line 53
    sget v0, Lco/uk/getmondo/c$a;->contactsViewPager:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/c/a;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v4/view/ViewPager;

    new-instance v1, Lco/uk/getmondo/c/a$b;

    invoke-virtual {p0}, Lco/uk/getmondo/c/a;->getChildFragmentManager()Landroid/support/v4/app/n;

    move-result-object v2

    const-string v3, "childFragmentManager"

    invoke-static {v2, v3}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0}, Lco/uk/getmondo/c/a;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const-string v4, "resources"

    invoke-static {v3, v4}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v4, 0x2

    invoke-direct {v1, v2, v3, v4}, Lco/uk/getmondo/c/a$b;-><init>(Landroid/support/v4/app/n;Landroid/content/res/Resources;I)V

    check-cast v1, Landroid/support/v4/view/p;

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setAdapter(Landroid/support/v4/view/p;)V

    .line 54
    sget v0, Lco/uk/getmondo/c$a;->contactsViewPager:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/c/a;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v4/view/ViewPager;

    new-instance v1, Lco/uk/getmondo/c/a$c;

    invoke-direct {v1, p0}, Lco/uk/getmondo/c/a$c;-><init>(Lco/uk/getmondo/c/a;)V

    check-cast v1, Landroid/support/v4/view/ViewPager$f;

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->a(Landroid/support/v4/view/ViewPager$f;)V

    .line 63
    sget v0, Lco/uk/getmondo/c$a;->contactsTabLayout:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/c/a;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/design/widget/TabLayout;

    sget v1, Lco/uk/getmondo/c$a;->contactsViewPager:I

    invoke-virtual {p0, v1}, Lco/uk/getmondo/c/a;->a(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0, v1}, Landroid/support/design/widget/TabLayout;->setupWithViewPager(Landroid/support/v4/view/ViewPager;)V

    .line 64
    return-void
.end method

.method public a(Lco/uk/getmondo/d/c;)V
    .locals 5

    .prologue
    const-string v0, "balance"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 84
    sget v0, Lco/uk/getmondo/c$a;->toolbar:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/c/a;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/Toolbar;

    const v1, 0x7f0a02b4

    invoke-virtual {p0, v1}, Lco/uk/getmondo/c/a;->getString(I)Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/Toolbar;->setTitle(Ljava/lang/CharSequence;)V

    .line 85
    sget v0, Lco/uk/getmondo/c$a;->toolbar:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/c/a;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/Toolbar;

    sget-object v1, Lkotlin/d/b/ab;->a:Lkotlin/d/b/ab;

    const v1, 0x7f0a00e5

    invoke-virtual {p0, v1}, Lco/uk/getmondo/c/a;->getString(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, "getString(R.string.available_balance_format)"

    invoke-static {v1, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {p1}, Lco/uk/getmondo/d/c;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    array-length v3, v2

    invoke-static {v2, v3}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v2

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "java.lang.String.format(format, *args)"

    invoke-static {v1, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/Toolbar;->setSubtitle(Ljava/lang/CharSequence;)V

    .line 86
    sget v0, Lco/uk/getmondo/c$a;->toolbar:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/c/a;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/Toolbar;

    invoke-virtual {p0}, Lco/uk/getmondo/c/a;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x106000b

    invoke-static {v1, v2}, Landroid/support/v4/content/a;->c(Landroid/content/Context;I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/Toolbar;->setTitleTextColor(I)V

    .line 87
    return-void
.end method

.method public a(Z)V
    .locals 1

    .prologue
    .line 76
    if-eqz p1, :cond_0

    .line 77
    sget v0, Lco/uk/getmondo/c$a;->contactsFab:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/c/a;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/design/widget/FloatingActionButton;

    invoke-static {v0}, Lco/uk/getmondo/common/ae;->a(Landroid/view/View;)V

    .line 80
    :goto_0
    return-void

    .line 79
    :cond_0
    sget v0, Lco/uk/getmondo/c$a;->contactsFab:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/c/a;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/design/widget/FloatingActionButton;

    invoke-static {v0}, Lco/uk/getmondo/common/ae;->b(Landroid/view/View;)V

    goto :goto_0
.end method

.method public b()V
    .locals 5

    .prologue
    .line 67
    sget v0, Lco/uk/getmondo/c$a;->toolbar:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/c/a;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/Toolbar;

    const v1, 0x7f0a02b9

    invoke-virtual {p0, v1}, Lco/uk/getmondo/c/a;->getString(I)Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/Toolbar;->setTitle(Ljava/lang/CharSequence;)V

    .line 68
    sget v0, Lco/uk/getmondo/c$a;->contactsTabLayout:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/c/a;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/design/widget/TabLayout;

    invoke-static {v0}, Lco/uk/getmondo/common/ae;->b(Landroid/view/View;)V

    .line 69
    sget v0, Lco/uk/getmondo/c$a;->toolbar:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/c/a;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/Toolbar;

    invoke-virtual {v0}, Landroid/support/v7/widget/Toolbar;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    if-nez v0, :cond_0

    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type android.support.design.widget.AppBarLayout.LayoutParams"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    check-cast v0, Landroid/support/design/widget/AppBarLayout$a;

    .line 70
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/design/widget/AppBarLayout$a;->a(I)V

    .line 71
    sget v0, Lco/uk/getmondo/c$a;->contactsViewPager:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/c/a;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v4/view/ViewPager;

    new-instance v1, Lco/uk/getmondo/c/a$b;

    invoke-virtual {p0}, Lco/uk/getmondo/c/a;->getChildFragmentManager()Landroid/support/v4/app/n;

    move-result-object v2

    const-string v3, "childFragmentManager"

    invoke-static {v2, v3}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0}, Lco/uk/getmondo/c/a;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const-string v4, "resources"

    invoke-static {v3, v4}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v4, 0x1

    invoke-direct {v1, v2, v3, v4}, Lco/uk/getmondo/c/a$b;-><init>(Landroid/support/v4/app/n;Landroid/content/res/Resources;I)V

    check-cast v1, Landroid/support/v4/view/p;

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setAdapter(Landroid/support/v4/view/p;)V

    .line 72
    sget v0, Lco/uk/getmondo/c$a;->contactsTabLayout:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/c/a;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/design/widget/TabLayout;

    sget v1, Lco/uk/getmondo/c$a;->contactsViewPager:I

    invoke-virtual {p0, v1}, Lco/uk/getmondo/c/a;->a(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0, v1}, Landroid/support/design/widget/TabLayout;->setupWithViewPager(Landroid/support/v4/view/ViewPager;)V

    .line 73
    return-void
.end method

.method public c()V
    .locals 3

    .prologue
    .line 90
    sget v0, Lco/uk/getmondo/c$a;->toolbar:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/c/a;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/Toolbar;

    const v1, 0x7f0a02b1

    invoke-virtual {p0, v1}, Lco/uk/getmondo/c/a;->getString(I)Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/Toolbar;->setTitle(Ljava/lang/CharSequence;)V

    .line 91
    sget v0, Lco/uk/getmondo/c$a;->toolbar:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/c/a;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/Toolbar;

    const/4 v1, 0x0

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/Toolbar;->setSubtitle(Ljava/lang/CharSequence;)V

    .line 92
    sget v0, Lco/uk/getmondo/c$a;->toolbar:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/c/a;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/Toolbar;

    invoke-virtual {p0}, Lco/uk/getmondo/c/a;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0f0079

    invoke-static {v1, v2}, Landroid/support/v4/content/a;->c(Landroid/content/Context;I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/Toolbar;->setTitleTextColor(I)V

    .line 93
    return-void
.end method

.method public f()V
    .locals 1

    iget-object v0, p0, Lco/uk/getmondo/c/a;->f:Ljava/util/HashMap;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lco/uk/getmondo/c/a;->f:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    :cond_0
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 29
    invoke-super {p0, p1}, Lco/uk/getmondo/common/f/a;->onCreate(Landroid/os/Bundle;)V

    .line 30
    invoke-virtual {p0}, Lco/uk/getmondo/c/a;->B()Lco/uk/getmondo/common/h/b/b;

    move-result-object v0

    invoke-interface {v0, p0}, Lco/uk/getmondo/common/h/b/b;->a(Lco/uk/getmondo/c/a;)V

    .line 31
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2

    .prologue
    const-string v0, "inflater"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 34
    const v0, 0x7f05009d

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public onDestroyView()V
    .locals 2

    .prologue
    .line 48
    iget-object v0, p0, Lco/uk/getmondo/c/a;->a:Lco/uk/getmondo/c/c;

    if-nez v0, :cond_0

    const-string v1, "presenter"

    invoke-static {v1}, Lkotlin/d/b/l;->b(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0}, Lco/uk/getmondo/c/c;->b()V

    .line 49
    invoke-super {p0}, Lco/uk/getmondo/common/f/a;->onDestroyView()V

    .line 50
    invoke-virtual {p0}, Lco/uk/getmondo/c/a;->f()V

    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 38
    invoke-super {p0, p1, p2}, Lco/uk/getmondo/common/f/a;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 40
    iget-object v1, p0, Lco/uk/getmondo/c/a;->a:Lco/uk/getmondo/c/c;

    if-nez v1, :cond_0

    const-string v0, "presenter"

    invoke-static {v0}, Lkotlin/d/b/l;->b(Ljava/lang/String;)V

    :cond_0
    move-object v0, p0

    check-cast v0, Lco/uk/getmondo/c/c$a;

    invoke-virtual {v1, v0}, Lco/uk/getmondo/c/c;->a(Lco/uk/getmondo/c/c$a;)V

    .line 42
    invoke-virtual {p0}, Lco/uk/getmondo/c/a;->getActivity()Landroid/support/v4/app/j;

    move-result-object v0

    if-nez v0, :cond_1

    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type co.uk.getmondo.main.HomeActivity"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    check-cast v0, Lco/uk/getmondo/main/HomeActivity;

    .line 43
    sget v1, Lco/uk/getmondo/c$a;->toolbar:I

    invoke-virtual {p0, v1}, Lco/uk/getmondo/c/a;->a(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/support/v7/widget/Toolbar;

    invoke-virtual {v0, v1}, Lco/uk/getmondo/main/HomeActivity;->setSupportActionBar(Landroid/support/v7/widget/Toolbar;)V

    .line 44
    sget v1, Lco/uk/getmondo/c$a;->toolbar:I

    invoke-virtual {p0, v1}, Lco/uk/getmondo/c/a;->a(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/support/v7/widget/Toolbar;

    const-string v2, "toolbar"

    invoke-static {v1, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lco/uk/getmondo/main/HomeActivity;->a(Landroid/support/v7/widget/Toolbar;)V

    .line 45
    return-void
.end method
