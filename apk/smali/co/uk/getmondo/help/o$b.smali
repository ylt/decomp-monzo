.class final Lco/uk/getmondo/help/o$b;
.super Ljava/lang/Object;
.source "HelpTopicPresenter.kt"

# interfaces
.implements Lio/reactivex/c/g;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lco/uk/getmondo/help/o;->a(Lco/uk/getmondo/help/o$a;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/g",
        "<",
        "Lkotlin/n;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000\n\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0004\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0003*\u0004\u0018\u00010\u00010\u0001H\n\u00a2\u0006\u0004\u0008\u0004\u0010\u0005"
    }
    d2 = {
        "<anonymous>",
        "",
        "it",
        "kotlin.jvm.PlatformType",
        "accept",
        "(Lkotlin/Unit;)V"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field final synthetic a:Lco/uk/getmondo/help/o;

.field final synthetic b:Lco/uk/getmondo/help/o$a;


# direct methods
.method constructor <init>(Lco/uk/getmondo/help/o;Lco/uk/getmondo/help/o$a;)V
    .locals 0

    iput-object p1, p0, Lco/uk/getmondo/help/o$b;->a:Lco/uk/getmondo/help/o;

    iput-object p2, p0, Lco/uk/getmondo/help/o$b;->b:Lco/uk/getmondo/help/o$a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 16
    check-cast p1, Lkotlin/n;

    invoke-virtual {p0, p1}, Lco/uk/getmondo/help/o$b;->a(Lkotlin/n;)V

    return-void
.end method

.method public final a(Lkotlin/n;)V
    .locals 4

    .prologue
    .line 31
    iget-object v0, p0, Lco/uk/getmondo/help/o$b;->a:Lco/uk/getmondo/help/o;

    invoke-static {v0}, Lco/uk/getmondo/help/o;->a(Lco/uk/getmondo/help/o;)Lco/uk/getmondo/common/a;

    move-result-object v0

    sget-object v1, Lco/uk/getmondo/api/model/tracking/Impression;->Companion:Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    sget-object v2, Lco/uk/getmondo/api/model/tracking/Impression$HelpOutcome;->THUMBS_UP:Lco/uk/getmondo/api/model/tracking/Impression$HelpOutcome;

    iget-object v3, p0, Lco/uk/getmondo/help/o$b;->a:Lco/uk/getmondo/help/o;

    invoke-static {v3}, Lco/uk/getmondo/help/o;->b(Lco/uk/getmondo/help/o;)Lco/uk/getmondo/help/a/a/e;

    move-result-object v3

    invoke-virtual {v3}, Lco/uk/getmondo/help/a/a/e;->b()Lco/uk/getmondo/api/model/help/Topic;

    move-result-object v3

    invoke-virtual {v3}, Lco/uk/getmondo/api/model/help/Topic;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->a(Lco/uk/getmondo/api/model/tracking/Impression$HelpOutcome;Ljava/lang/String;)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v1

    invoke-virtual {v0, v1}, Lco/uk/getmondo/common/a;->a(Lco/uk/getmondo/api/model/tracking/Impression;)V

    .line 32
    iget-object v0, p0, Lco/uk/getmondo/help/o$b;->b:Lco/uk/getmondo/help/o$a;

    invoke-interface {v0}, Lco/uk/getmondo/help/o$a;->d()V

    .line 33
    return-void
.end method
