.class final Lco/uk/getmondo/help/c$g;
.super Ljava/lang/Object;
.source "HelpCategoryPresenter.kt"

# interfaces
.implements Lio/reactivex/c/g;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lco/uk/getmondo/help/c;->a(Lco/uk/getmondo/help/c$a;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/g",
        "<",
        "Lkotlin/n;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000\n\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0004\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0003*\u0004\u0018\u00010\u00010\u0001H\n\u00a2\u0006\u0004\u0008\u0004\u0010\u0005"
    }
    d2 = {
        "<anonymous>",
        "",
        "it",
        "kotlin.jvm.PlatformType",
        "accept",
        "(Lkotlin/Unit;)V"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field final synthetic a:Lco/uk/getmondo/help/c;

.field final synthetic b:Lco/uk/getmondo/help/c$a;


# direct methods
.method constructor <init>(Lco/uk/getmondo/help/c;Lco/uk/getmondo/help/c$a;)V
    .locals 0

    iput-object p1, p0, Lco/uk/getmondo/help/c$g;->a:Lco/uk/getmondo/help/c;

    iput-object p2, p0, Lco/uk/getmondo/help/c$g;->b:Lco/uk/getmondo/help/c$a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 27
    check-cast p1, Lkotlin/n;

    invoke-virtual {p0, p1}, Lco/uk/getmondo/help/c$g;->a(Lkotlin/n;)V

    return-void
.end method

.method public final a(Lkotlin/n;)V
    .locals 7

    .prologue
    const/4 v3, 0x0

    .line 69
    iget-object v0, p0, Lco/uk/getmondo/help/c$g;->a:Lco/uk/getmondo/help/c;

    invoke-static {v0}, Lco/uk/getmondo/help/c;->f(Lco/uk/getmondo/help/c;)Lco/uk/getmondo/common/a;

    move-result-object v6

    sget-object v0, Lco/uk/getmondo/api/model/tracking/Impression;->Companion:Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    sget-object v1, Lco/uk/getmondo/api/model/tracking/Impression$IntercomFrom;->HELP:Lco/uk/getmondo/api/model/tracking/Impression$IntercomFrom;

    iget-object v2, p0, Lco/uk/getmondo/help/c$g;->a:Lco/uk/getmondo/help/c;

    invoke-static {v2}, Lco/uk/getmondo/help/c;->b(Lco/uk/getmondo/help/c;)Ljava/lang/String;

    move-result-object v2

    const/4 v4, 0x4

    move-object v5, v3

    invoke-static/range {v0 .. v5}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->a(Lco/uk/getmondo/api/model/tracking/Impression$Companion;Lco/uk/getmondo/api/model/tracking/Impression$IntercomFrom;Ljava/lang/String;Ljava/lang/String;ILjava/lang/Object;)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    invoke-virtual {v6, v0}, Lco/uk/getmondo/common/a;->a(Lco/uk/getmondo/api/model/tracking/Impression;)V

    .line 70
    iget-object v0, p0, Lco/uk/getmondo/help/c$g;->b:Lco/uk/getmondo/help/c$a;

    invoke-interface {v0}, Lco/uk/getmondo/help/c$a;->e()V

    .line 71
    return-void
.end method
