.class final Lco/uk/getmondo/help/c$e;
.super Ljava/lang/Object;
.source "HelpCategoryPresenter.kt"

# interfaces
.implements Lio/reactivex/c/g;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lco/uk/getmondo/help/c;->a(Lco/uk/getmondo/help/c$a;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/g",
        "<",
        "Lco/uk/getmondo/help/a/a/d$c;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "",
        "it",
        "Lco/uk/getmondo/help/data/model/SectionItem$TopicItem;",
        "kotlin.jvm.PlatformType",
        "accept"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field final synthetic a:Lco/uk/getmondo/help/c$a;

.field final synthetic b:Ljava/lang/String;


# direct methods
.method constructor <init>(Lco/uk/getmondo/help/c$a;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lco/uk/getmondo/help/c$e;->a:Lco/uk/getmondo/help/c$a;

    iput-object p2, p0, Lco/uk/getmondo/help/c$e;->b:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lco/uk/getmondo/help/a/a/d$c;)V
    .locals 3

    .prologue
    .line 65
    iget-object v0, p0, Lco/uk/getmondo/help/c$e;->a:Lco/uk/getmondo/help/c$a;

    invoke-virtual {p1}, Lco/uk/getmondo/help/a/a/d$c;->c()Lco/uk/getmondo/api/model/help/Topic;

    move-result-object v1

    iget-object v2, p0, Lco/uk/getmondo/help/c$e;->b:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Lco/uk/getmondo/help/c$a;->a(Lco/uk/getmondo/api/model/help/Topic;Ljava/lang/String;)V

    return-void
.end method

.method public bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 27
    check-cast p1, Lco/uk/getmondo/help/a/a/d$c;

    invoke-virtual {p0, p1}, Lco/uk/getmondo/help/c$e;->a(Lco/uk/getmondo/help/a/a/d$c;)V

    return-void
.end method
