.class public final Lco/uk/getmondo/help/ui/HelpCategoryView;
.super Landroid/support/constraint/ConstraintLayout;
.source "HelpCategoryView.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000:\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0008\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u0004\n\u0002\u0010\u000e\n\u0002\u0008\n\u0018\u00002\u00020\u0001B%\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\n\u0008\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u0012\u0008\u0008\u0002\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\u0008J\u000e\u0010%\u001a\u00020\u00172\u0006\u0010\u000f\u001a\u00020\u0010R$\u0010\n\u001a\u00020\u00072\u0006\u0010\t\u001a\u00020\u0007@FX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u000b\u0010\u000c\"\u0004\u0008\r\u0010\u000eR\u001c\u0010\u000f\u001a\u0004\u0018\u00010\u0010X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u0011\u0010\u0012\"\u0004\u0008\u0013\u0010\u0014R*\u0010\u0015\u001a\u0012\u0012\u0006\u0012\u0004\u0018\u00010\u0010\u0012\u0004\u0012\u00020\u0017\u0018\u00010\u0016X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u0018\u0010\u0019\"\u0004\u0008\u001a\u0010\u001bR$\u0010\u001d\u001a\u00020\u001c2\u0006\u0010\t\u001a\u00020\u001c@FX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u001e\u0010\u001f\"\u0004\u0008 \u0010!R$\u0010\"\u001a\u00020\u001c2\u0006\u0010\t\u001a\u00020\u001c@FX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008#\u0010\u001f\"\u0004\u0008$\u0010!\u00a8\u0006&"
    }
    d2 = {
        "Lco/uk/getmondo/help/ui/HelpCategoryView;",
        "Landroid/support/constraint/ConstraintLayout;",
        "context",
        "Landroid/content/Context;",
        "attrs",
        "Landroid/util/AttributeSet;",
        "defStyleAttr",
        "",
        "(Landroid/content/Context;Landroid/util/AttributeSet;I)V",
        "value",
        "backgroundColorRes",
        "getBackgroundColorRes",
        "()I",
        "setBackgroundColorRes",
        "(I)V",
        "category",
        "Lco/uk/getmondo/help/data/model/Category;",
        "getCategory",
        "()Lco/uk/getmondo/help/data/model/Category;",
        "setCategory",
        "(Lco/uk/getmondo/help/data/model/Category;)V",
        "clickListener",
        "Lkotlin/Function1;",
        "",
        "getClickListener",
        "()Lkotlin/jvm/functions/Function1;",
        "setClickListener",
        "(Lkotlin/jvm/functions/Function1;)V",
        "",
        "emojiText",
        "getEmojiText",
        "()Ljava/lang/String;",
        "setEmojiText",
        "(Ljava/lang/String;)V",
        "label",
        "getLabel",
        "setLabel",
        "bind",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field private c:Lco/uk/getmondo/help/a/a/a;

.field private d:Ljava/lang/String;

.field private e:Ljava/lang/String;

.field private f:I

.field private g:Lkotlin/d/a/b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/d/a/b",
            "<-",
            "Lco/uk/getmondo/help/a/a/a;",
            "Lkotlin/n;",
            ">;"
        }
    .end annotation
.end field

.field private h:Ljava/util/HashMap;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 6

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x6

    move-object v0, p0

    move-object v1, p1

    move-object v5, v2

    invoke-direct/range {v0 .. v5}, Lco/uk/getmondo/help/ui/HelpCategoryView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/d/b/i;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 6

    const/4 v3, 0x0

    const/4 v4, 0x4

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    invoke-direct/range {v0 .. v5}, Lco/uk/getmondo/help/ui/HelpCategoryView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/d/b/i;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 21
    invoke-direct {p0, p1, p2, p3}, Landroid/support/constraint/ConstraintLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 25
    const-string v0, ""

    iput-object v0, p0, Lco/uk/getmondo/help/ui/HelpCategoryView;->d:Ljava/lang/String;

    .line 30
    const-string v0, ""

    iput-object v0, p0, Lco/uk/getmondo/help/ui/HelpCategoryView;->e:Ljava/lang/String;

    .line 46
    const v1, 0x7f05013f

    move-object v0, p0

    check-cast v0, Landroid/view/ViewGroup;

    invoke-static {p1, v1, v0}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 48
    if-eqz p2, :cond_0

    .line 49
    sget-object v0, Lco/uk/getmondo/c$b;->HelpCategoryView:[I

    invoke-virtual {p1, p2, v0, v2, v2}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v1

    .line 50
    invoke-virtual {v1, v2}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    :goto_0
    invoke-virtual {p0, v0}, Lco/uk/getmondo/help/ui/HelpCategoryView;->setLabel(Ljava/lang/String;)V

    .line 51
    const/4 v0, 0x2

    invoke-virtual {v1, v0}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    :goto_1
    invoke-virtual {p0, v0}, Lco/uk/getmondo/help/ui/HelpCategoryView;->setEmojiText(Ljava/lang/String;)V

    .line 52
    const/4 v0, 0x1

    invoke-virtual {v1, v0, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v0

    invoke-virtual {p0, v0}, Lco/uk/getmondo/help/ui/HelpCategoryView;->setBackgroundColorRes(I)V

    .line 54
    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    .line 55
    nop

    nop

    .line 57
    :cond_0
    new-instance v0, Lco/uk/getmondo/help/ui/HelpCategoryView$1;

    invoke-direct {v0, p0}, Lco/uk/getmondo/help/ui/HelpCategoryView$1;-><init>(Lco/uk/getmondo/help/ui/HelpCategoryView;)V

    check-cast v0, Landroid/view/View$OnClickListener;

    invoke-virtual {p0, v0}, Lco/uk/getmondo/help/ui/HelpCategoryView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void

    .line 50
    :cond_1
    const-string v0, ""

    goto :goto_0

    .line 51
    :cond_2
    const-string v0, ""

    goto :goto_1
.end method

.method public synthetic constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/d/b/i;)V
    .locals 2

    .prologue
    and-int/lit8 v0, p4, 0x2

    if-eqz v0, :cond_1

    .line 19
    const/4 v0, 0x0

    check-cast v0, Landroid/util/AttributeSet;

    :goto_0
    and-int/lit8 v1, p4, 0x4

    if-eqz v1, :cond_0

    .line 20
    const/4 p3, 0x0

    :cond_0
    invoke-direct {p0, p1, v0, p3}, Lco/uk/getmondo/help/ui/HelpCategoryView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void

    :cond_1
    move-object v0, p2

    goto :goto_0
.end method


# virtual methods
.method public final a(Lco/uk/getmondo/help/a/a/a;)V
    .locals 2

    .prologue
    const-string v0, "category"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 61
    iput-object p1, p0, Lco/uk/getmondo/help/ui/HelpCategoryView;->c:Lco/uk/getmondo/help/a/a/a;

    .line 62
    invoke-interface {p1}, Lco/uk/getmondo/help/a/a/a;->b()I

    move-result v0

    invoke-static {p0, v0}, Lco/uk/getmondo/common/ae;->a(Landroid/view/View;I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lco/uk/getmondo/help/ui/HelpCategoryView;->setLabel(Ljava/lang/String;)V

    .line 63
    invoke-interface {p1}, Lco/uk/getmondo/help/a/a/a;->c()I

    move-result v0

    invoke-static {v0}, Lco/uk/getmondo/common/k/e;->b(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "EmojiHelper.getEmojum(category.emojum)"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lco/uk/getmondo/help/ui/HelpCategoryView;->setEmojiText(Ljava/lang/String;)V

    .line 64
    return-void
.end method

.method public b(I)Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lco/uk/getmondo/help/ui/HelpCategoryView;->h:Ljava/util/HashMap;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lco/uk/getmondo/help/ui/HelpCategoryView;->h:Ljava/util/HashMap;

    :cond_0
    iget-object v0, p0, Lco/uk/getmondo/help/ui/HelpCategoryView;->h:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    if-nez v0, :cond_1

    invoke-virtual {p0, p1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/help/ui/HelpCategoryView;->h:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    return-object v0
.end method

.method public final getBackgroundColorRes()I
    .locals 1

    .prologue
    .line 35
    iget v0, p0, Lco/uk/getmondo/help/ui/HelpCategoryView;->f:I

    return v0
.end method

.method public final getCategory()Lco/uk/getmondo/help/a/a/a;
    .locals 1

    .prologue
    .line 23
    iget-object v0, p0, Lco/uk/getmondo/help/ui/HelpCategoryView;->c:Lco/uk/getmondo/help/a/a/a;

    return-object v0
.end method

.method public final getClickListener()Lkotlin/d/a/b;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/d/a/b",
            "<",
            "Lco/uk/getmondo/help/a/a/a;",
            "Lkotlin/n;",
            ">;"
        }
    .end annotation

    .prologue
    .line 43
    iget-object v0, p0, Lco/uk/getmondo/help/ui/HelpCategoryView;->g:Lkotlin/d/a/b;

    return-object v0
.end method

.method public final getEmojiText()Ljava/lang/String;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lco/uk/getmondo/help/ui/HelpCategoryView;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final getLabel()Ljava/lang/String;
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, Lco/uk/getmondo/help/ui/HelpCategoryView;->d:Ljava/lang/String;

    return-object v0
.end method

.method public final setBackgroundColorRes(I)V
    .locals 2

    .prologue
    .line 37
    if-eqz p1, :cond_1

    .line 38
    sget v0, Lco/uk/getmondo/c$a;->helpCategoryBackground:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/help/ui/HelpCategoryView;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->mutate()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-nez v0, :cond_0

    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type android.graphics.drawable.GradientDrawable"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    check-cast v0, Landroid/graphics/drawable/GradientDrawable;

    .line 39
    invoke-virtual {p0}, Lco/uk/getmondo/help/ui/HelpCategoryView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, p1}, Landroid/support/v4/content/a;->c(Landroid/content/Context;I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/GradientDrawable;->setColor(I)V

    .line 41
    :cond_1
    return-void
.end method

.method public final setCategory(Lco/uk/getmondo/help/a/a/a;)V
    .locals 0

    .prologue
    .line 23
    iput-object p1, p0, Lco/uk/getmondo/help/ui/HelpCategoryView;->c:Lco/uk/getmondo/help/a/a/a;

    return-void
.end method

.method public final setClickListener(Lkotlin/d/a/b;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/d/a/b",
            "<-",
            "Lco/uk/getmondo/help/a/a/a;",
            "Lkotlin/n;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 43
    iput-object p1, p0, Lco/uk/getmondo/help/ui/HelpCategoryView;->g:Lkotlin/d/a/b;

    return-void
.end method

.method public final setEmojiText(Ljava/lang/String;)V
    .locals 1

    .prologue
    const-string v0, "value"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 32
    sget v0, Lco/uk/getmondo/c$a;->helpCategoryIcon:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/help/ui/HelpCategoryView;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    check-cast p1, Ljava/lang/CharSequence;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 33
    return-void
.end method

.method public final setLabel(Ljava/lang/String;)V
    .locals 1

    .prologue
    const-string v0, "value"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 27
    sget v0, Lco/uk/getmondo/c$a;->helpCategoryLabel:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/help/ui/HelpCategoryView;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    check-cast p1, Ljava/lang/CharSequence;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 28
    return-void
.end method
