.class public final Lco/uk/getmondo/help/HelpTopicActivity;
.super Lco/uk/getmondo/common/activities/b;
.source "HelpTopicActivity.kt"

# interfaces
.implements Lco/uk/getmondo/help/o$a;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lco/uk/getmondo/help/HelpTopicActivity$a;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000L\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0008\u0007\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0010\u000e\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u0000 )2\u00020\u00012\u00020\u0002:\u0001)B\u0005\u00a2\u0006\u0002\u0010\u0003J\u0012\u0010\u001e\u001a\u00020\u00062\u0008\u0010\u001f\u001a\u0004\u0018\u00010 H\u0014J\u0008\u0010!\u001a\u00020\u0006H\u0016J\u0008\u0010\"\u001a\u00020\u0006H\u0016J\u0010\u0010#\u001a\u00020\u00062\u0006\u0010$\u001a\u00020%H\u0016J\u0010\u0010&\u001a\u00020\u00062\u0006\u0010\'\u001a\u00020(H\u0016R\u001a\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u00058VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0007\u0010\u0008R\u001a\u0010\t\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u00058VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\n\u0010\u0008R\u001e\u0010\u000b\u001a\u00020\u000c8\u0006@\u0006X\u0087.\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\r\u0010\u000e\"\u0004\u0008\u000f\u0010\u0010R\u001e\u0010\u0011\u001a\u00020\u00128\u0006@\u0006X\u0087.\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u0013\u0010\u0014\"\u0004\u0008\u0015\u0010\u0016R#\u0010\u0017\u001a\n \u0019*\u0004\u0018\u00010\u00180\u00188BX\u0082\u0084\u0002\u00a2\u0006\u000c\n\u0004\u0008\u001c\u0010\u001d\u001a\u0004\u0008\u001a\u0010\u001b\u00a8\u0006*"
    }
    d2 = {
        "Lco/uk/getmondo/help/HelpTopicActivity;",
        "Lco/uk/getmondo/common/activities/BaseActivity;",
        "Lco/uk/getmondo/help/HelpTopicPresenter$View;",
        "()V",
        "feedbackNegativeClicked",
        "Lio/reactivex/Observable;",
        "",
        "getFeedbackNegativeClicked",
        "()Lio/reactivex/Observable;",
        "feedbackPositiveClicked",
        "getFeedbackPositiveClicked",
        "intercomService",
        "Lco/uk/getmondo/common/IntercomService;",
        "getIntercomService",
        "()Lco/uk/getmondo/common/IntercomService;",
        "setIntercomService",
        "(Lco/uk/getmondo/common/IntercomService;)V",
        "presenter",
        "Lco/uk/getmondo/help/HelpTopicPresenter;",
        "getPresenter",
        "()Lco/uk/getmondo/help/HelpTopicPresenter;",
        "setPresenter",
        "(Lco/uk/getmondo/help/HelpTopicPresenter;)V",
        "topicViewModel",
        "Lco/uk/getmondo/help/data/model/TopicViewModel;",
        "kotlin.jvm.PlatformType",
        "getTopicViewModel",
        "()Lco/uk/getmondo/help/data/model/TopicViewModel;",
        "topicViewModel$delegate",
        "Lkotlin/Lazy;",
        "onCreate",
        "savedInstanceState",
        "Landroid/os/Bundle;",
        "openChat",
        "openHelpHome",
        "setTitle",
        "title",
        "",
        "showTopic",
        "topic",
        "Lco/uk/getmondo/api/model/help/Topic;",
        "Companion",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# static fields
.field static final synthetic a:[Lkotlin/reflect/l;

.field public static final e:Lco/uk/getmondo/help/HelpTopicActivity$a;


# instance fields
.field public b:Lco/uk/getmondo/help/o;

.field public c:Lco/uk/getmondo/common/q;

.field private final f:Lkotlin/c;

.field private g:Ljava/util/HashMap;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const/4 v0, 0x1

    new-array v1, v0, [Lkotlin/reflect/l;

    const/4 v2, 0x0

    new-instance v0, Lkotlin/d/b/w;

    const-class v3, Lco/uk/getmondo/help/HelpTopicActivity;

    invoke-static {v3}, Lkotlin/d/b/y;->a(Ljava/lang/Class;)Lkotlin/reflect/c;

    move-result-object v3

    const-string v4, "topicViewModel"

    const-string v5, "getTopicViewModel()Lco/uk/getmondo/help/data/model/TopicViewModel;"

    invoke-direct {v0, v3, v4, v5}, Lkotlin/d/b/w;-><init>(Lkotlin/reflect/e;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v0}, Lkotlin/d/b/y;->a(Lkotlin/d/b/v;)Lkotlin/reflect/n;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/l;

    aput-object v0, v1, v2

    sput-object v1, Lco/uk/getmondo/help/HelpTopicActivity;->a:[Lkotlin/reflect/l;

    new-instance v0, Lco/uk/getmondo/help/HelpTopicActivity$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lco/uk/getmondo/help/HelpTopicActivity$a;-><init>(Lkotlin/d/b/i;)V

    sput-object v0, Lco/uk/getmondo/help/HelpTopicActivity;->e:Lco/uk/getmondo/help/HelpTopicActivity$a;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 20
    invoke-direct {p0}, Lco/uk/getmondo/common/activities/b;-><init>()V

    .line 25
    new-instance v0, Lco/uk/getmondo/help/HelpTopicActivity$b;

    invoke-direct {v0, p0}, Lco/uk/getmondo/help/HelpTopicActivity$b;-><init>(Lco/uk/getmondo/help/HelpTopicActivity;)V

    check-cast v0, Lkotlin/d/a/a;

    invoke-static {v0}, Lkotlin/d;->a(Lkotlin/d/a/a;)Lkotlin/c;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/help/HelpTopicActivity;->f:Lkotlin/c;

    return-void
.end method

.method private final e()Lco/uk/getmondo/help/a/a/e;
    .locals 3

    iget-object v0, p0, Lco/uk/getmondo/help/HelpTopicActivity;->f:Lkotlin/c;

    sget-object v1, Lco/uk/getmondo/help/HelpTopicActivity;->a:[Lkotlin/reflect/l;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-interface {v0}, Lkotlin/c;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/help/a/a/e;

    return-object v0
.end method


# virtual methods
.method public a(I)Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lco/uk/getmondo/help/HelpTopicActivity;->g:Ljava/util/HashMap;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lco/uk/getmondo/help/HelpTopicActivity;->g:Ljava/util/HashMap;

    :cond_0
    iget-object v0, p0, Lco/uk/getmondo/help/HelpTopicActivity;->g:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    if-nez v0, :cond_1

    invoke-virtual {p0, p1}, Landroid/support/v4/app/j;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/help/HelpTopicActivity;->g:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    return-object v0
.end method

.method public a()Lio/reactivex/n;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Lkotlin/n;",
            ">;"
        }
    .end annotation

    .prologue
    .line 41
    sget v0, Lco/uk/getmondo/c$a;->topicFeedbackPositive:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/help/HelpTopicActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 74
    invoke-static {v0}, Lcom/b/a/c/c;->a(Landroid/view/View;)Lio/reactivex/n;

    move-result-object v1

    sget-object v0, Lcom/b/a/a/d;->a:Lcom/b/a/a/d;

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {v1, v0}, Lio/reactivex/n;->map(Lio/reactivex/c/h;)Lio/reactivex/n;

    move-result-object v0

    const-string v1, "RxView.clicks(this).map(VoidToUnit)"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 41
    return-object v0
.end method

.method public a(Lco/uk/getmondo/api/model/help/Topic;)V
    .locals 2

    .prologue
    const-string v0, "topic"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 51
    sget v0, Lco/uk/getmondo/c$a;->topicTitle:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/help/HelpTopicActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {p1}, Lco/uk/getmondo/api/model/help/Topic;->b()Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 52
    sget v0, Lco/uk/getmondo/c$a;->topicContent:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/help/HelpTopicActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {p1}, Lco/uk/getmondo/api/model/help/Topic;->c()Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 53
    sget v0, Lco/uk/getmondo/c$a;->topicContent:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/help/HelpTopicActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const/16 v1, 0xf

    invoke-static {v0, v1}, Landroid/text/util/Linkify;->addLinks(Landroid/widget/TextView;I)Z

    .line 54
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 1

    .prologue
    const-string v0, "title"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 47
    invoke-virtual {p0}, Lco/uk/getmondo/help/HelpTopicActivity;->getSupportActionBar()Landroid/support/v7/app/a;

    move-result-object v0

    if-eqz v0, :cond_0

    check-cast p1, Ljava/lang/CharSequence;

    invoke-virtual {v0, p1}, Landroid/support/v7/app/a;->a(Ljava/lang/CharSequence;)V

    .line 48
    :cond_0
    return-void
.end method

.method public b()Lio/reactivex/n;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Lkotlin/n;",
            ">;"
        }
    .end annotation

    .prologue
    .line 44
    sget v0, Lco/uk/getmondo/c$a;->topicFeedbackNegative:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/help/HelpTopicActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 75
    invoke-static {v0}, Lcom/b/a/c/c;->a(Landroid/view/View;)Lio/reactivex/n;

    move-result-object v1

    sget-object v0, Lcom/b/a/a/d;->a:Lcom/b/a/a/d;

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {v1, v0}, Lio/reactivex/n;->map(Lio/reactivex/c/h;)Lio/reactivex/n;

    move-result-object v0

    const-string v1, "RxView.clicks(this).map(VoidToUnit)"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 44
    return-object v0
.end method

.method public c()V
    .locals 3

    .prologue
    .line 57
    invoke-direct {p0}, Lco/uk/getmondo/help/HelpTopicActivity;->e()Lco/uk/getmondo/help/a/a/e;

    move-result-object v0

    invoke-virtual {v0}, Lco/uk/getmondo/help/a/a/e;->c()Ljava/lang/String;

    move-result-object v1

    move-object v0, v1

    .line 58
    check-cast v0, Ljava/lang/CharSequence;

    invoke-static {v0}, Lkotlin/h/j;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lco/uk/getmondo/help/HelpTopicActivity;->c:Lco/uk/getmondo/common/q;

    if-nez v0, :cond_0

    const-string v1, "intercomService"

    invoke-static {v1}, Lkotlin/d/b/l;->b(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0}, Lco/uk/getmondo/common/q;->a()V

    .line 59
    :goto_0
    return-void

    .line 58
    :cond_1
    iget-object v0, p0, Lco/uk/getmondo/help/HelpTopicActivity;->c:Lco/uk/getmondo/common/q;

    if-nez v0, :cond_2

    const-string v2, "intercomService"

    invoke-static {v2}, Lkotlin/d/b/l;->b(Ljava/lang/String;)V

    :cond_2
    invoke-virtual {v0, v1}, Lco/uk/getmondo/common/q;->b(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public d()V
    .locals 2

    .prologue
    .line 62
    sget-object v1, Lco/uk/getmondo/help/HelpActivity;->c:Lco/uk/getmondo/help/HelpActivity$a;

    move-object v0, p0

    check-cast v0, Landroid/content/Context;

    invoke-virtual {v1, v0}, Lco/uk/getmondo/help/HelpActivity$a;->a(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    const/high16 v1, 0x4000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lco/uk/getmondo/help/HelpTopicActivity;->startActivity(Landroid/content/Intent;)V

    .line 63
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 28
    invoke-super {p0, p1}, Lco/uk/getmondo/common/activities/b;->onCreate(Landroid/os/Bundle;)V

    .line 29
    const v0, 0x7f050040

    invoke-virtual {p0, v0}, Lco/uk/getmondo/help/HelpTopicActivity;->setContentView(I)V

    .line 31
    sget v0, Lco/uk/getmondo/c$a;->negativeFeedbackEmojum:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/help/HelpTopicActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-static {}, Lco/uk/getmondo/common/k/e;->h()Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 32
    sget v0, Lco/uk/getmondo/c$a;->positiveFeedbackEmojum:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/help/HelpTopicActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-static {}, Lco/uk/getmondo/common/k/e;->g()Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 34
    invoke-virtual {p0}, Lco/uk/getmondo/help/HelpTopicActivity;->l()Lco/uk/getmondo/common/h/b/b;

    move-result-object v0

    new-instance v1, Lco/uk/getmondo/help/b/f;

    invoke-direct {p0}, Lco/uk/getmondo/help/HelpTopicActivity;->e()Lco/uk/getmondo/help/a/a/e;

    move-result-object v2

    const-string v3, "topicViewModel"

    invoke-static {v2, v3}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v1, v2}, Lco/uk/getmondo/help/b/f;-><init>(Lco/uk/getmondo/help/a/a/e;)V

    invoke-interface {v0, v1}, Lco/uk/getmondo/common/h/b/b;->a(Lco/uk/getmondo/help/b/f;)Lco/uk/getmondo/help/b/e;

    move-result-object v0

    .line 35
    invoke-interface {v0, p0}, Lco/uk/getmondo/help/b/e;->a(Lco/uk/getmondo/help/HelpTopicActivity;)V

    .line 37
    iget-object v0, p0, Lco/uk/getmondo/help/HelpTopicActivity;->b:Lco/uk/getmondo/help/o;

    if-nez v0, :cond_0

    const-string v1, "presenter"

    invoke-static {v1}, Lkotlin/d/b/l;->b(Ljava/lang/String;)V

    :cond_0
    check-cast p0, Lco/uk/getmondo/help/o$a;

    invoke-virtual {v0, p0}, Lco/uk/getmondo/help/o;->a(Lco/uk/getmondo/help/o$a;)V

    .line 38
    return-void
.end method
