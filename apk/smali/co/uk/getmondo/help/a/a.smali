.class public final Lco/uk/getmondo/help/a/a;
.super Ljava/lang/Object;
.source "HelpManager.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u00008\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0018\u00002\u00020\u0001B\u0017\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\u001a\u0010\r\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00100\u000f0\u000e2\u0006\u0010\u0011\u001a\u00020\u0008J\u0012\u0010\u0012\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00130\u000f0\u000eJ\u001a\u0010\u0014\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00100\u000f0\u000e2\u0006\u0010\u0015\u001a\u00020\u0008R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001a\u0010\u0007\u001a\u00020\u0008X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\t\u0010\n\"\u0004\u0008\u000b\u0010\u000c\u00a8\u0006\u0016"
    }
    d2 = {
        "Lco/uk/getmondo/help/data/HelpManager;",
        "",
        "helpApi",
        "Lco/uk/getmondo/api/HelpApi;",
        "resourceProvider",
        "Lco/uk/getmondo/common/ResourceProvider;",
        "(Lco/uk/getmondo/api/HelpApi;Lco/uk/getmondo/common/ResourceProvider;)V",
        "trendingTitle",
        "",
        "getTrendingTitle",
        "()Ljava/lang/String;",
        "setTrendingTitle",
        "(Ljava/lang/String;)V",
        "loadCategoryItems",
        "Lio/reactivex/Single;",
        "",
        "Lco/uk/getmondo/help/data/model/SectionItem;",
        "id",
        "loadTrendingTopics",
        "Lco/uk/getmondo/api/model/help/Topic;",
        "search",
        "query",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field private a:Ljava/lang/String;

.field private final b:Lco/uk/getmondo/api/HelpApi;

.field private final c:Lco/uk/getmondo/common/v;


# direct methods
.method public constructor <init>(Lco/uk/getmondo/api/HelpApi;Lco/uk/getmondo/common/v;)V
    .locals 1

    .prologue
    const-string v0, "helpApi"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "resourceProvider"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lco/uk/getmondo/help/a/a;->b:Lco/uk/getmondo/api/HelpApi;

    iput-object p2, p0, Lco/uk/getmondo/help/a/a;->c:Lco/uk/getmondo/common/v;

    .line 16
    const-string v0, ""

    iput-object v0, p0, Lco/uk/getmondo/help/a/a;->a:Ljava/lang/String;

    return-void
.end method

.method public static final synthetic a(Lco/uk/getmondo/help/a/a;)Lco/uk/getmondo/common/v;
    .locals 1

    .prologue
    .line 12
    iget-object v0, p0, Lco/uk/getmondo/help/a/a;->c:Lco/uk/getmondo/common/v;

    return-object v0
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 16
    iget-object v0, p0, Lco/uk/getmondo/help/a/a;->a:Ljava/lang/String;

    return-object v0
.end method

.method public final a(Ljava/lang/String;)V
    .locals 1

    .prologue
    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 16
    iput-object p1, p0, Lco/uk/getmondo/help/a/a;->a:Ljava/lang/String;

    return-void
.end method

.method public final b()Lio/reactivex/v;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/v",
            "<",
            "Ljava/util/List",
            "<",
            "Lco/uk/getmondo/api/model/help/Topic;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 18
    iget-object v0, p0, Lco/uk/getmondo/help/a/a;->b:Lco/uk/getmondo/api/HelpApi;

    invoke-interface {v0}, Lco/uk/getmondo/api/HelpApi;->trending()Lio/reactivex/v;

    move-result-object v1

    .line 19
    new-instance v0, Lco/uk/getmondo/help/a/a$b;

    invoke-direct {v0, p0}, Lco/uk/getmondo/help/a/a$b;-><init>(Lco/uk/getmondo/help/a/a;)V

    check-cast v0, Lio/reactivex/c/g;

    invoke-virtual {v1, v0}, Lio/reactivex/v;->c(Lio/reactivex/c/g;)Lio/reactivex/v;

    move-result-object v1

    .line 20
    sget-object v0, Lco/uk/getmondo/help/a/a$c;->a:Lco/uk/getmondo/help/a/a$c;

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {v1, v0}, Lio/reactivex/v;->d(Lio/reactivex/c/h;)Lio/reactivex/v;

    move-result-object v0

    const-string v1, "helpApi.trending()\n     \u2026.first().topics.take(5) }"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public final b(Ljava/lang/String;)Lio/reactivex/v;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/v",
            "<",
            "Ljava/util/List",
            "<",
            "Lco/uk/getmondo/help/a/a/d;",
            ">;>;"
        }
    .end annotation

    .prologue
    const-string v0, "id"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 23
    iget-object v0, p0, Lco/uk/getmondo/help/a/a;->b:Lco/uk/getmondo/api/HelpApi;

    invoke-interface {v0, p1}, Lco/uk/getmondo/api/HelpApi;->categories(Ljava/lang/String;)Lio/reactivex/v;

    move-result-object v1

    .line 24
    new-instance v0, Lco/uk/getmondo/help/a/a$a;

    invoke-direct {v0, p0}, Lco/uk/getmondo/help/a/a$a;-><init>(Lco/uk/getmondo/help/a/a;)V

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {v1, v0}, Lio/reactivex/v;->d(Lio/reactivex/c/h;)Lio/reactivex/v;

    move-result-object v0

    const-string v1, "helpApi.categories(id)\n \u2026ryItems\n                }"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public final c(Ljava/lang/String;)Lio/reactivex/v;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/v",
            "<",
            "Ljava/util/List",
            "<",
            "Lco/uk/getmondo/help/a/a/d;",
            ">;>;"
        }
    .end annotation

    .prologue
    const-string v0, "query"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 36
    iget-object v0, p0, Lco/uk/getmondo/help/a/a;->b:Lco/uk/getmondo/api/HelpApi;

    invoke-interface {v0, p1}, Lco/uk/getmondo/api/HelpApi;->search(Ljava/lang/String;)Lio/reactivex/v;

    move-result-object v1

    .line 37
    new-instance v0, Lco/uk/getmondo/help/a/a$d;

    invoke-direct {v0, p0}, Lco/uk/getmondo/help/a/a$d;-><init>(Lco/uk/getmondo/help/a/a;)V

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {v1, v0}, Lio/reactivex/v;->d(Lio/reactivex/c/h;)Lio/reactivex/v;

    move-result-object v0

    const-string v1, "helpApi.search(query)\n  \u2026icItems\n                }"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method
