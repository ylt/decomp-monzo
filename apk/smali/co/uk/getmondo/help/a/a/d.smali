.class public abstract Lco/uk/getmondo/help/a/a/d;
.super Ljava/lang/Object;
.source "SectionItem.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lco/uk/getmondo/help/a/a/d$b;,
        Lco/uk/getmondo/help/a/a/d$c;,
        Lco/uk/getmondo/help/a/a/d$a;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000*\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0010\t\n\u0002\u0008\u0003\n\u0002\u0010\u0008\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u00086\u0018\u00002\u00020\u0001:\u0003\u000b\u000c\rB\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002R\u0012\u0010\u0003\u001a\u00020\u0004X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0005\u0010\u0006R\u0012\u0010\u0007\u001a\u00020\u0008X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\t\u0010\n\u0082\u0001\u0003\u000e\u000f\u0010\u00a8\u0006\u0011"
    }
    d2 = {
        "Lco/uk/getmondo/help/data/model/SectionItem;",
        "",
        "()V",
        "id",
        "",
        "getId",
        "()J",
        "layout",
        "",
        "getLayout",
        "()I",
        "HelpAction",
        "SectionHeader",
        "TopicItem",
        "Lco/uk/getmondo/help/data/model/SectionItem$SectionHeader;",
        "Lco/uk/getmondo/help/data/model/SectionItem$TopicItem;",
        "Lco/uk/getmondo/help/data/model/SectionItem$HelpAction;",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/d/b/i;)V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Lco/uk/getmondo/help/a/a/d;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract a()J
.end method

.method public abstract b()I
.end method
