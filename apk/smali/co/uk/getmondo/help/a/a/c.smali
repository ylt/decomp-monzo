.class public final enum Lco/uk/getmondo/help/a/a/c;
.super Ljava/lang/Enum;
.source "HelpCategory.kt"

# interfaces
.implements Lco/uk/getmondo/help/a/a/a;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lco/uk/getmondo/help/a/a/c$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lco/uk/getmondo/help/a/a/c;",
        ">;",
        "Lco/uk/getmondo/help/a/a/a;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000$\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0008\n\u0002\u0018\u0002\n\u0002\u0008\u000b\u0008\u0086\u0001\u0018\u0000 \u00192\u0008\u0012\u0004\u0012\u00020\u00000\u00012\u00020\u0002:\u0001\u0019B!\u0008\u0002\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u0008\u0008\u0001\u0010\u0005\u001a\u00020\u0006\u0012\u0006\u0010\u0007\u001a\u00020\u0006\u00a2\u0006\u0002\u0010\u0008R\u0014\u0010\u0007\u001a\u00020\u0006X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\t\u0010\nR\u0014\u0010\u0003\u001a\u00020\u0004X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000b\u0010\u000cR\u0014\u0010\u0005\u001a\u00020\u0006X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\r\u0010\nR\u0014\u0010\u000e\u001a\u00020\u000fX\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0010\u0010\u0011j\u0002\u0008\u0012j\u0002\u0008\u0013j\u0002\u0008\u0014j\u0002\u0008\u0015j\u0002\u0008\u0016j\u0002\u0008\u0017j\u0002\u0008\u0018\u00a8\u0006\u001a"
    }
    d2 = {
        "Lco/uk/getmondo/help/data/model/HelpCategory;",
        "",
        "Lco/uk/getmondo/help/data/model/Category;",
        "id",
        "",
        "titleRes",
        "",
        "emojum",
        "(Ljava/lang/String;ILjava/lang/String;II)V",
        "getEmojum",
        "()I",
        "getId",
        "()Ljava/lang/String;",
        "getTitleRes",
        "type",
        "Lco/uk/getmondo/help/data/model/Category$Type;",
        "getType",
        "()Lco/uk/getmondo/help/data/model/Category$Type;",
        "TRANSACTIONS_BALANCE",
        "CARD_PIN_TOP_UP",
        "LIMITS",
        "GOING_ABROAD",
        "PRODUCT_FEATURES",
        "PROFILE",
        "CONTACTS_PAYMENTS",
        "Companion",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# static fields
.field public static final enum a:Lco/uk/getmondo/help/a/a/c;

.field public static final enum b:Lco/uk/getmondo/help/a/a/c;

.field public static final enum c:Lco/uk/getmondo/help/a/a/c;

.field public static final enum d:Lco/uk/getmondo/help/a/a/c;

.field public static final enum e:Lco/uk/getmondo/help/a/a/c;

.field public static final enum f:Lco/uk/getmondo/help/a/a/c;

.field public static final enum g:Lco/uk/getmondo/help/a/a/c;

.field public static final h:Lco/uk/getmondo/help/a/a/c$a;

.field private static final synthetic i:[Lco/uk/getmondo/help/a/a/c;


# instance fields
.field private final j:Lco/uk/getmondo/help/a/a/a$a;

.field private final k:Ljava/lang/String;

.field private final l:I

.field private final m:I


# direct methods
.method static constructor <clinit>()V
    .locals 11

    .prologue
    const/4 v10, 0x4

    const/4 v9, 0x3

    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v2, 0x0

    const/4 v0, 0x7

    new-array v6, v0, [Lco/uk/getmondo/help/a/a/c;

    new-instance v0, Lco/uk/getmondo/help/a/a/c;

    const-string v1, "TRANSACTIONS_BALANCE"

    .line 13
    const-string v3, "transactions-and-balance"

    const v4, 0x7f0a01b6

    const v5, 0x1f4b7

    invoke-direct/range {v0 .. v5}, Lco/uk/getmondo/help/a/a/c;-><init>(Ljava/lang/String;ILjava/lang/String;II)V

    sput-object v0, Lco/uk/getmondo/help/a/a/c;->a:Lco/uk/getmondo/help/a/a/c;

    aput-object v0, v6, v2

    new-instance v0, Lco/uk/getmondo/help/a/a/c;

    const-string v1, "CARD_PIN_TOP_UP"

    .line 14
    const-string v3, "card-pin-and-top-up"

    const v4, 0x7f0a01b0

    const v5, 0x1f4b3

    move v2, v7

    invoke-direct/range {v0 .. v5}, Lco/uk/getmondo/help/a/a/c;-><init>(Ljava/lang/String;ILjava/lang/String;II)V

    sput-object v0, Lco/uk/getmondo/help/a/a/c;->b:Lco/uk/getmondo/help/a/a/c;

    aput-object v0, v6, v7

    new-instance v0, Lco/uk/getmondo/help/a/a/c;

    const-string v1, "LIMITS"

    .line 15
    const-string v3, "limits"

    const v4, 0x7f0a01b4

    const/16 v5, 0x270b

    move v2, v8

    invoke-direct/range {v0 .. v5}, Lco/uk/getmondo/help/a/a/c;-><init>(Ljava/lang/String;ILjava/lang/String;II)V

    sput-object v0, Lco/uk/getmondo/help/a/a/c;->c:Lco/uk/getmondo/help/a/a/c;

    aput-object v0, v6, v8

    new-instance v0, Lco/uk/getmondo/help/a/a/c;

    const-string v1, "GOING_ABROAD"

    .line 16
    const-string v3, "going-abroad"

    const v4, 0x7f0a01ae

    const/16 v5, 0x2708

    move v2, v9

    invoke-direct/range {v0 .. v5}, Lco/uk/getmondo/help/a/a/c;-><init>(Ljava/lang/String;ILjava/lang/String;II)V

    sput-object v0, Lco/uk/getmondo/help/a/a/c;->d:Lco/uk/getmondo/help/a/a/c;

    aput-object v0, v6, v9

    new-instance v0, Lco/uk/getmondo/help/a/a/c;

    const-string v1, "PRODUCT_FEATURES"

    .line 17
    const-string v3, "product-features"

    const v4, 0x7f0a01b3

    const v5, 0x1f4a1

    move v2, v10

    invoke-direct/range {v0 .. v5}, Lco/uk/getmondo/help/a/a/c;-><init>(Ljava/lang/String;ILjava/lang/String;II)V

    sput-object v0, Lco/uk/getmondo/help/a/a/c;->e:Lco/uk/getmondo/help/a/a/c;

    aput-object v0, v6, v10

    const/4 v7, 0x5

    new-instance v0, Lco/uk/getmondo/help/a/a/c;

    const-string v1, "PROFILE"

    const/4 v2, 0x5

    .line 18
    const-string v3, "profile"

    const v4, 0x7f0a01af

    const v5, 0x1f464

    invoke-direct/range {v0 .. v5}, Lco/uk/getmondo/help/a/a/c;-><init>(Ljava/lang/String;ILjava/lang/String;II)V

    sput-object v0, Lco/uk/getmondo/help/a/a/c;->f:Lco/uk/getmondo/help/a/a/c;

    aput-object v0, v6, v7

    const/4 v7, 0x6

    new-instance v0, Lco/uk/getmondo/help/a/a/c;

    const-string v1, "CONTACTS_PAYMENTS"

    const/4 v2, 0x6

    .line 19
    const-string v3, "contacts-and-payments"

    const v4, 0x7f0a01b1

    const v5, 0x1f48c

    invoke-direct/range {v0 .. v5}, Lco/uk/getmondo/help/a/a/c;-><init>(Ljava/lang/String;ILjava/lang/String;II)V

    sput-object v0, Lco/uk/getmondo/help/a/a/c;->g:Lco/uk/getmondo/help/a/a/c;

    aput-object v0, v6, v7

    sput-object v6, Lco/uk/getmondo/help/a/a/c;->i:[Lco/uk/getmondo/help/a/a/c;

    new-instance v0, Lco/uk/getmondo/help/a/a/c$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lco/uk/getmondo/help/a/a/c$a;-><init>(Lkotlin/d/b/i;)V

    sput-object v0, Lco/uk/getmondo/help/a/a/c;->h:Lco/uk/getmondo/help/a/a/c$a;

    return-void
.end method

.method protected constructor <init>(Ljava/lang/String;ILjava/lang/String;II)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "II)V"
        }
    .end annotation

    .prologue
    const-string v0, "id"

    invoke-static {p3, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 7
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput-object p3, p0, Lco/uk/getmondo/help/a/a/c;->k:Ljava/lang/String;

    iput p4, p0, Lco/uk/getmondo/help/a/a/c;->l:I

    iput p5, p0, Lco/uk/getmondo/help/a/a/c;->m:I

    .line 21
    sget-object v0, Lco/uk/getmondo/help/a/a/a$a;->a:Lco/uk/getmondo/help/a/a/a$a;

    iput-object v0, p0, Lco/uk/getmondo/help/a/a/c;->j:Lco/uk/getmondo/help/a/a/a$a;

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lco/uk/getmondo/help/a/a/c;
    .locals 1

    const-class v0, Lco/uk/getmondo/help/a/a/c;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/help/a/a/c;

    return-object v0
.end method

.method public static values()[Lco/uk/getmondo/help/a/a/c;
    .locals 1

    sget-object v0, Lco/uk/getmondo/help/a/a/c;->i:[Lco/uk/getmondo/help/a/a/c;

    invoke-virtual {v0}, [Lco/uk/getmondo/help/a/a/c;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lco/uk/getmondo/help/a/a/c;

    return-object v0
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 8
    iget-object v0, p0, Lco/uk/getmondo/help/a/a/c;->k:Ljava/lang/String;

    return-object v0
.end method

.method public b()I
    .locals 1

    .prologue
    .line 9
    iget v0, p0, Lco/uk/getmondo/help/a/a/c;->l:I

    return v0
.end method

.method public c()I
    .locals 1

    .prologue
    .line 10
    iget v0, p0, Lco/uk/getmondo/help/a/a/c;->m:I

    return v0
.end method

.method public d()Lco/uk/getmondo/help/a/a/a$a;
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lco/uk/getmondo/help/a/a/c;->j:Lco/uk/getmondo/help/a/a/a$a;

    return-object v0
.end method
