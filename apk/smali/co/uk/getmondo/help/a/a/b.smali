.class public final enum Lco/uk/getmondo/help/a/a/b;
.super Ljava/lang/Enum;
.source "CommunityCategory.kt"

# interfaces
.implements Lco/uk/getmondo/help/a/a/a;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lco/uk/getmondo/help/a/a/b$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lco/uk/getmondo/help/a/a/b;",
        ">;",
        "Lco/uk/getmondo/help/a/a/a;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000$\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\t\n\u0002\u0018\u0002\n\u0002\u0008\u0008\u0008\u0086\u0001\u0018\u0000 \u00172\u0008\u0012\u0004\u0012\u00020\u00000\u00012\u00020\u0002:\u0001\u0017B)\u0008\u0002\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u0008\u0008\u0001\u0010\u0005\u001a\u00020\u0006\u0012\u0006\u0010\u0007\u001a\u00020\u0006\u0012\u0006\u0010\u0008\u001a\u00020\u0004\u00a2\u0006\u0002\u0010\tR\u0014\u0010\u0007\u001a\u00020\u0006X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\n\u0010\u000bR\u0014\u0010\u0003\u001a\u00020\u0004X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000c\u0010\rR\u0014\u0010\u0005\u001a\u00020\u0006X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000e\u0010\u000bR\u0014\u0010\u000f\u001a\u00020\u0010X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0011\u0010\u0012R\u0011\u0010\u0008\u001a\u00020\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0013\u0010\rj\u0002\u0008\u0014j\u0002\u0008\u0015j\u0002\u0008\u0016\u00a8\u0006\u0018"
    }
    d2 = {
        "Lco/uk/getmondo/help/data/model/CommunityCategory;",
        "",
        "Lco/uk/getmondo/help/data/model/Category;",
        "id",
        "",
        "titleRes",
        "",
        "emojum",
        "url",
        "(Ljava/lang/String;ILjava/lang/String;IILjava/lang/String;)V",
        "getEmojum",
        "()I",
        "getId",
        "()Ljava/lang/String;",
        "getTitleRes",
        "type",
        "Lco/uk/getmondo/help/data/model/Category$Type;",
        "getType",
        "()Lco/uk/getmondo/help/data/model/Category$Type;",
        "getUrl",
        "SNEAK_PEEKS",
        "FEATURE_IDEAS",
        "COMMUNITY_HOME",
        "Companion",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# static fields
.field public static final enum a:Lco/uk/getmondo/help/a/a/b;

.field public static final enum b:Lco/uk/getmondo/help/a/a/b;

.field public static final enum c:Lco/uk/getmondo/help/a/a/b;

.field public static final d:Lco/uk/getmondo/help/a/a/b$a;

.field private static final synthetic e:[Lco/uk/getmondo/help/a/a/b;


# instance fields
.field private final f:Lco/uk/getmondo/help/a/a/a$a;

.field private final g:Ljava/lang/String;

.field private final h:I

.field private final i:I

.field private final j:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 10

    .prologue
    const/4 v9, 0x2

    const/4 v8, 0x1

    const/4 v2, 0x0

    const/4 v0, 0x3

    new-array v7, v0, [Lco/uk/getmondo/help/a/a/b;

    new-instance v0, Lco/uk/getmondo/help/a/a/b;

    const-string v1, "SNEAK_PEEKS"

    .line 14
    const-string v3, "sneak-peeks"

    const v4, 0x7f0a0124

    const v5, 0x1f440

    const-string v6, "https://community.monzo.com/c/sneak-peek"

    invoke-direct/range {v0 .. v6}, Lco/uk/getmondo/help/a/a/b;-><init>(Ljava/lang/String;ILjava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lco/uk/getmondo/help/a/a/b;->a:Lco/uk/getmondo/help/a/a/b;

    aput-object v0, v7, v2

    new-instance v0, Lco/uk/getmondo/help/a/a/b;

    const-string v1, "FEATURE_IDEAS"

    .line 15
    const-string v3, "feature-ideas"

    const v4, 0x7f0a0123

    const/16 v5, 0x26a1

    const-string v6, "https://community.monzo.com/c/ideas"

    move v2, v8

    invoke-direct/range {v0 .. v6}, Lco/uk/getmondo/help/a/a/b;-><init>(Ljava/lang/String;ILjava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lco/uk/getmondo/help/a/a/b;->b:Lco/uk/getmondo/help/a/a/b;

    aput-object v0, v7, v8

    new-instance v0, Lco/uk/getmondo/help/a/a/b;

    const-string v1, "COMMUNITY_HOME"

    .line 16
    const-string v3, "community-home"

    const v4, 0x7f0a0122

    const v5, 0x1f3e0

    const-string v6, "https://community.monzo.com/"

    move v2, v9

    invoke-direct/range {v0 .. v6}, Lco/uk/getmondo/help/a/a/b;-><init>(Ljava/lang/String;ILjava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lco/uk/getmondo/help/a/a/b;->c:Lco/uk/getmondo/help/a/a/b;

    aput-object v0, v7, v9

    sput-object v7, Lco/uk/getmondo/help/a/a/b;->e:[Lco/uk/getmondo/help/a/a/b;

    new-instance v0, Lco/uk/getmondo/help/a/a/b$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lco/uk/getmondo/help/a/a/b$a;-><init>(Lkotlin/d/b/i;)V

    sput-object v0, Lco/uk/getmondo/help/a/a/b;->d:Lco/uk/getmondo/help/a/a/b$a;

    return-void
.end method

.method protected constructor <init>(Ljava/lang/String;ILjava/lang/String;IILjava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "II",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    const-string v0, "id"

    invoke-static {p3, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "url"

    invoke-static {p6, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 7
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput-object p3, p0, Lco/uk/getmondo/help/a/a/b;->g:Ljava/lang/String;

    iput p4, p0, Lco/uk/getmondo/help/a/a/b;->h:I

    iput p5, p0, Lco/uk/getmondo/help/a/a/b;->i:I

    iput-object p6, p0, Lco/uk/getmondo/help/a/a/b;->j:Ljava/lang/String;

    .line 18
    sget-object v0, Lco/uk/getmondo/help/a/a/a$a;->b:Lco/uk/getmondo/help/a/a/a$a;

    iput-object v0, p0, Lco/uk/getmondo/help/a/a/b;->f:Lco/uk/getmondo/help/a/a/a$a;

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lco/uk/getmondo/help/a/a/b;
    .locals 1

    const-class v0, Lco/uk/getmondo/help/a/a/b;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/help/a/a/b;

    return-object v0
.end method

.method public static values()[Lco/uk/getmondo/help/a/a/b;
    .locals 1

    sget-object v0, Lco/uk/getmondo/help/a/a/b;->e:[Lco/uk/getmondo/help/a/a/b;

    invoke-virtual {v0}, [Lco/uk/getmondo/help/a/a/b;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lco/uk/getmondo/help/a/a/b;

    return-object v0
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 8
    iget-object v0, p0, Lco/uk/getmondo/help/a/a/b;->g:Ljava/lang/String;

    return-object v0
.end method

.method public b()I
    .locals 1

    .prologue
    .line 9
    iget v0, p0, Lco/uk/getmondo/help/a/a/b;->h:I

    return v0
.end method

.method public c()I
    .locals 1

    .prologue
    .line 10
    iget v0, p0, Lco/uk/getmondo/help/a/a/b;->i:I

    return v0
.end method

.method public d()Lco/uk/getmondo/help/a/a/a$a;
    .locals 1

    .prologue
    .line 18
    iget-object v0, p0, Lco/uk/getmondo/help/a/a/b;->f:Lco/uk/getmondo/help/a/a/a$a;

    return-object v0
.end method

.method public final e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 11
    iget-object v0, p0, Lco/uk/getmondo/help/a/a/b;->j:Ljava/lang/String;

    return-object v0
.end method
