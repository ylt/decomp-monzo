.class final Lco/uk/getmondo/help/a/a$a;
.super Ljava/lang/Object;
.source "HelpManager.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lco/uk/getmondo/help/a/a;->b(Ljava/lang/String;)Lio/reactivex/v;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h",
        "<TT;TR;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000\u0012\n\u0000\n\u0002\u0010!\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u0008\u0012\u0004\u0012\u00020\u00020\u00012\u0006\u0010\u0003\u001a\u00020\u0004H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "",
        "Lco/uk/getmondo/help/data/model/SectionItem;",
        "<name for destructuring parameter 0>",
        "Lco/uk/getmondo/api/model/help/Content;",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field final synthetic a:Lco/uk/getmondo/help/a/a;


# direct methods
.method constructor <init>(Lco/uk/getmondo/help/a/a;)V
    .locals 0

    iput-object p1, p0, Lco/uk/getmondo/help/a/a$a;->a:Lco/uk/getmondo/help/a/a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 12
    check-cast p1, Lco/uk/getmondo/api/model/help/Content;

    invoke-virtual {p0, p1}, Lco/uk/getmondo/help/a/a$a;->a(Lco/uk/getmondo/api/model/help/Content;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lco/uk/getmondo/api/model/help/Content;)Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lco/uk/getmondo/api/model/help/Content;",
            ")",
            "Ljava/util/List",
            "<",
            "Lco/uk/getmondo/help/a/a/d;",
            ">;"
        }
    .end annotation

    .prologue
    const-string v0, "<name for destructuring parameter 0>"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lco/uk/getmondo/api/model/help/Content;->b()Ljava/util/List;

    move-result-object v1

    .line 25
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    check-cast v0, Ljava/util/List;

    .line 26
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lco/uk/getmondo/api/model/help/Section;

    invoke-virtual {v1}, Lco/uk/getmondo/api/model/help/Section;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1}, Lco/uk/getmondo/api/model/help/Section;->d()Ljava/util/List;

    move-result-object v1

    .line 27
    new-instance v4, Lco/uk/getmondo/help/a/a/d$b;

    invoke-direct {v4, v2}, Lco/uk/getmondo/help/a/a/d$b;-><init>(Ljava/lang/String;)V

    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 28
    check-cast v1, Ljava/lang/Iterable;

    .line 46
    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    move-object v1, v0

    .line 47
    check-cast v1, Ljava/util/Collection;

    check-cast v2, Lco/uk/getmondo/api/model/help/Topic;

    .line 28
    new-instance v5, Lco/uk/getmondo/help/a/a/d$c;

    invoke-direct {v5, v2}, Lco/uk/getmondo/help/a/a/d$c;-><init>(Lco/uk/getmondo/api/model/help/Topic;)V

    invoke-interface {v1, v5}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_0
    move-object v1, v0

    .line 48
    check-cast v1, Ljava/util/Collection;

    nop

    .line 26
    goto :goto_0

    :cond_1
    move-object v1, v0

    .line 30
    check-cast v1, Ljava/util/Collection;

    new-instance v2, Lco/uk/getmondo/help/a/a/d$a;

    iget-object v3, p0, Lco/uk/getmondo/help/a/a$a;->a:Lco/uk/getmondo/help/a/a;

    invoke-static {v3}, Lco/uk/getmondo/help/a/a;->a(Lco/uk/getmondo/help/a/a;)Lco/uk/getmondo/common/v;

    move-result-object v3

    const v4, 0x7f0a01bf

    invoke-virtual {v3, v4}, Lco/uk/getmondo/common/v;->a(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Lco/uk/getmondo/help/a/a/d$a;-><init>(Ljava/lang/String;)V

    invoke-interface {v1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 31
    return-object v0
.end method
