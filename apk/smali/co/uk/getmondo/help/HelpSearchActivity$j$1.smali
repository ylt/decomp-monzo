.class final Lco/uk/getmondo/help/HelpSearchActivity$j$1;
.super Lkotlin/d/b/m;
.source "HelpSearchActivity.kt"

# interfaces
.implements Lkotlin/d/a/b;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lco/uk/getmondo/help/HelpSearchActivity$j;->a(Lio/reactivex/o;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/d/b/m;",
        "Lkotlin/d/a/b",
        "<",
        "Lco/uk/getmondo/help/a/a/d$c;",
        "Lkotlin/n;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n\u00a2\u0006\u0002\u0008\u0004"
    }
    d2 = {
        "<anonymous>",
        "",
        "it",
        "Lco/uk/getmondo/help/data/model/SectionItem$TopicItem;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field final synthetic a:Lco/uk/getmondo/help/HelpSearchActivity$j;

.field final synthetic b:Lio/reactivex/o;


# direct methods
.method constructor <init>(Lco/uk/getmondo/help/HelpSearchActivity$j;Lio/reactivex/o;)V
    .locals 1

    iput-object p1, p0, Lco/uk/getmondo/help/HelpSearchActivity$j$1;->a:Lco/uk/getmondo/help/HelpSearchActivity$j;

    iput-object p2, p0, Lco/uk/getmondo/help/HelpSearchActivity$j$1;->b:Lio/reactivex/o;

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lkotlin/d/b/m;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 26
    check-cast p1, Lco/uk/getmondo/help/a/a/d$c;

    invoke-virtual {p0, p1}, Lco/uk/getmondo/help/HelpSearchActivity$j$1;->a(Lco/uk/getmondo/help/a/a/d$c;)V

    sget-object v0, Lkotlin/n;->a:Lkotlin/n;

    return-object v0
.end method

.method public final a(Lco/uk/getmondo/help/a/a/d$c;)V
    .locals 5

    .prologue
    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 74
    iget-object v0, p0, Lco/uk/getmondo/help/HelpSearchActivity$j$1;->b:Lio/reactivex/o;

    new-instance v1, Lco/uk/getmondo/help/a/a/e;

    iget-object v2, p0, Lco/uk/getmondo/help/HelpSearchActivity$j$1;->a:Lco/uk/getmondo/help/HelpSearchActivity$j;

    iget-object v2, v2, Lco/uk/getmondo/help/HelpSearchActivity$j;->a:Lco/uk/getmondo/help/HelpSearchActivity;

    const v3, 0x7f0a01be

    invoke-virtual {v2, v3}, Lco/uk/getmondo/help/HelpSearchActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    const-string v3, "getString(R.string.help_search_title)"

    invoke-static {v2, v3}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lco/uk/getmondo/help/a/a/d$c;->c()Lco/uk/getmondo/api/model/help/Topic;

    move-result-object v3

    iget-object v4, p0, Lco/uk/getmondo/help/HelpSearchActivity$j$1;->a:Lco/uk/getmondo/help/HelpSearchActivity$j;

    iget-object v4, v4, Lco/uk/getmondo/help/HelpSearchActivity$j;->a:Lco/uk/getmondo/help/HelpSearchActivity;

    invoke-static {v4}, Lco/uk/getmondo/help/HelpSearchActivity;->b(Lco/uk/getmondo/help/HelpSearchActivity;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v1, v2, v3, v4}, Lco/uk/getmondo/help/a/a/e;-><init>(Ljava/lang/String;Lco/uk/getmondo/api/model/help/Topic;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Lio/reactivex/o;->a(Ljava/lang/Object;)V

    .line 75
    return-void
.end method
