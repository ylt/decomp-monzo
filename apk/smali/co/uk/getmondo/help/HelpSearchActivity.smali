.class public final Lco/uk/getmondo/help/HelpSearchActivity;
.super Lco/uk/getmondo/common/activities/b;
.source "HelpSearchActivity.kt"

# interfaces
.implements Lco/uk/getmondo/help/j$a;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lco/uk/getmondo/help/HelpSearchActivity$a;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000`\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0008\u000c\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0006\n\u0002\u0010\u000b\n\u0002\u0008\u0005\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u0000 72\u00020\u00012\u00020\u0002:\u00017B\u0005\u00a2\u0006\u0002\u0010\u0003J\u0008\u0010#\u001a\u00020$H\u0016J\u0008\u0010%\u001a\u00020$H\u0016J\u0012\u0010&\u001a\u00020$2\u0008\u0010\'\u001a\u0004\u0018\u00010(H\u0014J\u0010\u0010)\u001a\u00020$2\u0006\u0010*\u001a\u00020\u0006H\u0016J\u0010\u0010+\u001a\u00020$2\u0006\u0010,\u001a\u00020!H\u0016J\u0010\u0010-\u001a\u00020$2\u0006\u0010.\u001a\u00020/H\u0016J\u0010\u00100\u001a\u00020$2\u0006\u0010*\u001a\u00020\u0006H\u0016J\u0008\u00101\u001a\u00020$H\u0016J\u0008\u00102\u001a\u00020$H\u0016J\u0016\u00103\u001a\u00020$2\u000c\u00104\u001a\u0008\u0012\u0004\u0012\u00020605H\u0016R\u001a\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u00058VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0007\u0010\u0008R\u001e\u0010\t\u001a\u00020\n8\u0006@\u0006X\u0087.\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u000b\u0010\u000c\"\u0004\u0008\r\u0010\u000eR\u001e\u0010\u000f\u001a\u00020\u00108\u0006@\u0006X\u0087.\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u0011\u0010\u0012\"\u0004\u0008\u0013\u0010\u0014R\u001a\u0010\u0015\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u00058VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0016\u0010\u0008R\u0014\u0010\u0017\u001a\u00020\u00068BX\u0082\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0018\u0010\u0019R\u001a\u0010\u001a\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u00058VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u001b\u0010\u0008R\u000e\u0010\u001c\u001a\u00020\u001dX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001a\u0010\u001e\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u00058VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u001f\u0010\u0008R\u001a\u0010 \u001a\u0008\u0012\u0004\u0012\u00020!0\u00058VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\"\u0010\u0008\u00a8\u00068"
    }
    d2 = {
        "Lco/uk/getmondo/help/HelpSearchActivity;",
        "Lco/uk/getmondo/common/activities/BaseActivity;",
        "Lco/uk/getmondo/help/HelpSearchPresenter$View;",
        "()V",
        "helpClicked",
        "Lio/reactivex/Observable;",
        "",
        "getHelpClicked",
        "()Lio/reactivex/Observable;",
        "intercomService",
        "Lco/uk/getmondo/common/IntercomService;",
        "getIntercomService",
        "()Lco/uk/getmondo/common/IntercomService;",
        "setIntercomService",
        "(Lco/uk/getmondo/common/IntercomService;)V",
        "presenter",
        "Lco/uk/getmondo/help/HelpSearchPresenter;",
        "getPresenter",
        "()Lco/uk/getmondo/help/HelpSearchPresenter;",
        "setPresenter",
        "(Lco/uk/getmondo/help/HelpSearchPresenter;)V",
        "queryChanged",
        "getQueryChanged",
        "queryString",
        "getQueryString",
        "()Ljava/lang/String;",
        "retryClicked",
        "getRetryClicked",
        "sectionAdapter",
        "Lco/uk/getmondo/help/HelpSectionAdapter;",
        "suggestionClicked",
        "getSuggestionClicked",
        "topicClicked",
        "Lco/uk/getmondo/help/data/model/TopicViewModel;",
        "getTopicClicked",
        "hideSuggestions",
        "",
        "hideTopics",
        "onCreate",
        "savedInstanceState",
        "Landroid/os/Bundle;",
        "openChat",
        "query",
        "openTopic",
        "topicViewModel",
        "setLoading",
        "enabled",
        "",
        "setQuery",
        "showError",
        "showSuggestions",
        "showTopics",
        "topics",
        "",
        "Lco/uk/getmondo/help/data/model/SectionItem;",
        "Companion",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# static fields
.field public static final c:Lco/uk/getmondo/help/HelpSearchActivity$a;


# instance fields
.field public a:Lco/uk/getmondo/help/j;

.field public b:Lco/uk/getmondo/common/q;

.field private final e:Lco/uk/getmondo/help/m;

.field private f:Ljava/util/HashMap;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lco/uk/getmondo/help/HelpSearchActivity$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lco/uk/getmondo/help/HelpSearchActivity$a;-><init>(Lkotlin/d/b/i;)V

    sput-object v0, Lco/uk/getmondo/help/HelpSearchActivity;->c:Lco/uk/getmondo/help/HelpSearchActivity$a;

    return-void
.end method

.method public constructor <init>()V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 26
    invoke-direct {p0}, Lco/uk/getmondo/common/activities/b;-><init>()V

    .line 31
    new-instance v0, Lco/uk/getmondo/help/m;

    const/4 v4, 0x7

    move-object v2, v1

    move-object v3, v1

    move-object v5, v1

    invoke-direct/range {v0 .. v5}, Lco/uk/getmondo/help/m;-><init>(Ljava/util/List;Lkotlin/d/a/b;Lkotlin/d/a/a;ILkotlin/d/b/i;)V

    iput-object v0, p0, Lco/uk/getmondo/help/HelpSearchActivity;->e:Lco/uk/getmondo/help/m;

    return-void
.end method

.method public static final synthetic a(Lco/uk/getmondo/help/HelpSearchActivity;)Lco/uk/getmondo/help/m;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lco/uk/getmondo/help/HelpSearchActivity;->e:Lco/uk/getmondo/help/m;

    return-object v0
.end method

.method public static final synthetic b(Lco/uk/getmondo/help/HelpSearchActivity;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 26
    invoke-direct {p0}, Lco/uk/getmondo/help/HelpSearchActivity;->j()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private final j()Ljava/lang/String;
    .locals 1

    .prologue
    .line 33
    sget v0, Lco/uk/getmondo/c$a;->queryField:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/help/HelpSearchActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public a(I)Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lco/uk/getmondo/help/HelpSearchActivity;->f:Ljava/util/HashMap;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lco/uk/getmondo/help/HelpSearchActivity;->f:Ljava/util/HashMap;

    :cond_0
    iget-object v0, p0, Lco/uk/getmondo/help/HelpSearchActivity;->f:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    if-nez v0, :cond_1

    invoke-virtual {p0, p1}, Landroid/support/v4/app/j;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/help/HelpSearchActivity;->f:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    return-object v0
.end method

.method public a()Lio/reactivex/n;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 55
    sget v0, Lco/uk/getmondo/c$a;->helpSearchSuggestion1:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/help/HelpSearchActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 126
    invoke-static {v0}, Lcom/b/a/c/c;->a(Landroid/view/View;)Lio/reactivex/n;

    move-result-object v1

    sget-object v0, Lcom/b/a/a/d;->a:Lcom/b/a/a/d;

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {v1, v0}, Lio/reactivex/n;->map(Lio/reactivex/c/h;)Lio/reactivex/n;

    move-result-object v1

    const-string v0, "RxView.clicks(this).map(VoidToUnit)"

    invoke-static {v1, v0}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 56
    new-instance v0, Lco/uk/getmondo/help/HelpSearchActivity$f;

    invoke-direct {v0, p0}, Lco/uk/getmondo/help/HelpSearchActivity$f;-><init>(Lco/uk/getmondo/help/HelpSearchActivity;)V

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {v1, v0}, Lio/reactivex/n;->map(Lio/reactivex/c/h;)Lio/reactivex/n;

    move-result-object v0

    check-cast v0, Lio/reactivex/r;

    .line 57
    sget v1, Lco/uk/getmondo/c$a;->helpSearchSuggestion2:I

    invoke-virtual {p0, v1}, Lco/uk/getmondo/help/HelpSearchActivity;->a(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 127
    invoke-static {v1}, Lcom/b/a/c/c;->a(Landroid/view/View;)Lio/reactivex/n;

    move-result-object v2

    sget-object v1, Lcom/b/a/a/d;->a:Lcom/b/a/a/d;

    check-cast v1, Lio/reactivex/c/h;

    invoke-virtual {v2, v1}, Lio/reactivex/n;->map(Lio/reactivex/c/h;)Lio/reactivex/n;

    move-result-object v2

    const-string v1, "RxView.clicks(this).map(VoidToUnit)"

    invoke-static {v2, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 57
    new-instance v1, Lco/uk/getmondo/help/HelpSearchActivity$g;

    invoke-direct {v1, p0}, Lco/uk/getmondo/help/HelpSearchActivity$g;-><init>(Lco/uk/getmondo/help/HelpSearchActivity;)V

    check-cast v1, Lio/reactivex/c/h;

    invoke-virtual {v2, v1}, Lio/reactivex/n;->map(Lio/reactivex/c/h;)Lio/reactivex/n;

    move-result-object v1

    check-cast v1, Lio/reactivex/r;

    .line 58
    sget v2, Lco/uk/getmondo/c$a;->helpSearchSuggestion3:I

    invoke-virtual {p0, v2}, Lco/uk/getmondo/help/HelpSearchActivity;->a(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 128
    invoke-static {v2}, Lcom/b/a/c/c;->a(Landroid/view/View;)Lio/reactivex/n;

    move-result-object v3

    sget-object v2, Lcom/b/a/a/d;->a:Lcom/b/a/a/d;

    check-cast v2, Lio/reactivex/c/h;

    invoke-virtual {v3, v2}, Lio/reactivex/n;->map(Lio/reactivex/c/h;)Lio/reactivex/n;

    move-result-object v3

    const-string v2, "RxView.clicks(this).map(VoidToUnit)"

    invoke-static {v3, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 58
    new-instance v2, Lco/uk/getmondo/help/HelpSearchActivity$h;

    invoke-direct {v2, p0}, Lco/uk/getmondo/help/HelpSearchActivity$h;-><init>(Lco/uk/getmondo/help/HelpSearchActivity;)V

    check-cast v2, Lio/reactivex/c/h;

    invoke-virtual {v3, v2}, Lio/reactivex/n;->map(Lio/reactivex/c/h;)Lio/reactivex/n;

    move-result-object v2

    check-cast v2, Lio/reactivex/r;

    .line 55
    invoke-static {v0, v1, v2}, Lio/reactivex/n;->merge(Lio/reactivex/r;Lio/reactivex/r;Lio/reactivex/r;)Lio/reactivex/n;

    move-result-object v1

    .line 59
    sget-object v0, Lco/uk/getmondo/help/HelpSearchActivity$i;->a:Lco/uk/getmondo/help/HelpSearchActivity$i;

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {v1, v0}, Lio/reactivex/n;->map(Lio/reactivex/c/h;)Lio/reactivex/n;

    move-result-object v0

    const-string v1, "Observable.merge(\n      \u2026   .map { it.toString() }"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public a(Lco/uk/getmondo/help/a/a/e;)V
    .locals 2

    .prologue
    const-string v0, "topicViewModel"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 117
    sget-object v1, Lco/uk/getmondo/help/HelpTopicActivity;->e:Lco/uk/getmondo/help/HelpTopicActivity$a;

    move-object v0, p0

    check-cast v0, Landroid/content/Context;

    invoke-virtual {v1, v0, p1}, Lco/uk/getmondo/help/HelpTopicActivity$a;->a(Landroid/content/Context;Lco/uk/getmondo/help/a/a/e;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lco/uk/getmondo/help/HelpSearchActivity;->startActivity(Landroid/content/Intent;)V

    .line 118
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 2

    .prologue
    const-string v0, "query"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 89
    sget v0, Lco/uk/getmondo/c$a;->queryField:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/help/HelpSearchActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    const-string v1, ""

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 90
    sget v0, Lco/uk/getmondo/c$a;->queryField:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/help/HelpSearchActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    check-cast p1, Ljava/lang/CharSequence;

    invoke-virtual {v0, p1}, Landroid/widget/EditText;->append(Ljava/lang/CharSequence;)V

    .line 91
    return-void
.end method

.method public a(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<+",
            "Lco/uk/getmondo/help/a/a/d;",
            ">;)V"
        }
    .end annotation

    .prologue
    const-string v0, "topics"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 99
    iget-object v0, p0, Lco/uk/getmondo/help/HelpSearchActivity;->e:Lco/uk/getmondo/help/m;

    invoke-virtual {v0, p1}, Lco/uk/getmondo/help/m;->a(Ljava/util/List;)V

    .line 101
    sget v0, Lco/uk/getmondo/c$a;->helpSearchRecyclerView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/help/HelpSearchActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->a(I)V

    .line 102
    sget v0, Lco/uk/getmondo/c$a;->helpSearchRecyclerView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/help/HelpSearchActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView;

    invoke-static {v0}, Lco/uk/getmondo/common/ae;->a(Landroid/view/View;)V

    .line 103
    return-void
.end method

.method public a(Z)V
    .locals 2

    .prologue
    .line 94
    sget v0, Lco/uk/getmondo/c$a;->loadingErrorView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/help/HelpSearchActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/common/ui/LoadingErrorView;

    const v1, 0x7f0a01bc

    invoke-virtual {p0, v1}, Lco/uk/getmondo/help/HelpSearchActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lco/uk/getmondo/common/ui/LoadingErrorView;->a(ZLjava/lang/String;)V

    .line 95
    if-eqz p1, :cond_0

    sget v0, Lco/uk/getmondo/c$a;->loadingErrorView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/help/HelpSearchActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/common/ui/LoadingErrorView;

    invoke-static {v0}, Lco/uk/getmondo/common/ae;->a(Landroid/view/View;)V

    .line 96
    :goto_0
    return-void

    .line 95
    :cond_0
    sget v0, Lco/uk/getmondo/c$a;->loadingErrorView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/help/HelpSearchActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/common/ui/LoadingErrorView;

    invoke-static {v0}, Lco/uk/getmondo/common/ae;->b(Landroid/view/View;)V

    goto :goto_0
.end method

.method public b()Lio/reactivex/n;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 62
    sget v0, Lco/uk/getmondo/c$a;->queryField:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/help/HelpSearchActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    .line 129
    invoke-static {v0}, Lcom/b/a/d/e;->c(Landroid/widget/TextView;)Lcom/b/a/a;

    move-result-object v0

    const-string v1, "RxTextView.textChanges(this)"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 63
    invoke-virtual {v0}, Lcom/b/a/a;->b()Lio/reactivex/n;

    move-result-object v1

    .line 64
    sget-object v0, Lco/uk/getmondo/help/HelpSearchActivity$c;->a:Lco/uk/getmondo/help/HelpSearchActivity$c;

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {v1, v0}, Lio/reactivex/n;->debounce(Lio/reactivex/c/h;)Lio/reactivex/n;

    move-result-object v1

    .line 68
    sget-object v0, Lco/uk/getmondo/help/HelpSearchActivity$d;->a:Lco/uk/getmondo/help/HelpSearchActivity$d;

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {v1, v0}, Lio/reactivex/n;->map(Lio/reactivex/c/h;)Lio/reactivex/n;

    move-result-object v0

    .line 69
    invoke-static {}, Lio/reactivex/a/b/a;->a()Lio/reactivex/u;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/n;->observeOn(Lio/reactivex/u;)Lio/reactivex/n;

    move-result-object v0

    const-string v1, "queryField.textChanges()\u2026dSchedulers.mainThread())"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public c()Lio/reactivex/n;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Lco/uk/getmondo/help/a/a/e;",
            ">;"
        }
    .end annotation

    .prologue
    .line 72
    new-instance v0, Lco/uk/getmondo/help/HelpSearchActivity$j;

    invoke-direct {v0, p0}, Lco/uk/getmondo/help/HelpSearchActivity$j;-><init>(Lco/uk/getmondo/help/HelpSearchActivity;)V

    check-cast v0, Lio/reactivex/p;

    invoke-static {v0}, Lio/reactivex/n;->create(Lio/reactivex/p;)Lio/reactivex/n;

    move-result-object v0

    const-string v1, "Observable.create { emit\u2026stener = null }\n        }"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 77
    return-object v0
.end method

.method public d()Lio/reactivex/n;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 80
    new-instance v0, Lco/uk/getmondo/help/HelpSearchActivity$b;

    invoke-direct {v0, p0}, Lco/uk/getmondo/help/HelpSearchActivity$b;-><init>(Lco/uk/getmondo/help/HelpSearchActivity;)V

    check-cast v0, Lio/reactivex/p;

    invoke-static {v0}, Lio/reactivex/n;->create(Lio/reactivex/p;)Lio/reactivex/n;

    move-result-object v0

    const-string v1, "Observable.create { emit\u2026stener = null }\n        }"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 83
    return-object v0
.end method

.method public d(Ljava/lang/String;)V
    .locals 2

    .prologue
    const-string v0, "query"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 120
    iget-object v0, p0, Lco/uk/getmondo/help/HelpSearchActivity;->b:Lco/uk/getmondo/common/q;

    if-nez v0, :cond_0

    const-string v1, "intercomService"

    invoke-static {v1}, Lkotlin/d/b/l;->b(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0, p1}, Lco/uk/getmondo/common/q;->b(Ljava/lang/String;)V

    return-void
.end method

.method public e()Lio/reactivex/n;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 86
    sget v0, Lco/uk/getmondo/c$a;->loadingErrorView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/help/HelpSearchActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/common/ui/LoadingErrorView;

    invoke-virtual {v0}, Lco/uk/getmondo/common/ui/LoadingErrorView;->c()Lio/reactivex/n;

    move-result-object v1

    new-instance v0, Lco/uk/getmondo/help/HelpSearchActivity$e;

    invoke-direct {v0, p0}, Lco/uk/getmondo/help/HelpSearchActivity$e;-><init>(Lco/uk/getmondo/help/HelpSearchActivity;)V

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {v1, v0}, Lio/reactivex/n;->map(Lio/reactivex/c/h;)Lio/reactivex/n;

    move-result-object v0

    const-string v1, "loadingErrorView.retryClicks().map { queryString }"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public f()V
    .locals 1

    .prologue
    .line 105
    sget v0, Lco/uk/getmondo/c$a;->helpSearchSuggestions:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/help/HelpSearchActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/constraint/Group;

    invoke-static {v0}, Lco/uk/getmondo/common/ae;->a(Landroid/view/View;)V

    return-void
.end method

.method public g()V
    .locals 1

    .prologue
    .line 107
    sget v0, Lco/uk/getmondo/c$a;->helpSearchRecyclerView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/help/HelpSearchActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView;

    invoke-static {v0}, Lco/uk/getmondo/common/ae;->b(Landroid/view/View;)V

    return-void
.end method

.method public h()V
    .locals 1

    .prologue
    .line 109
    sget v0, Lco/uk/getmondo/c$a;->helpSearchSuggestions:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/help/HelpSearchActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/constraint/Group;

    invoke-static {v0}, Lco/uk/getmondo/common/ae;->b(Landroid/view/View;)V

    return-void
.end method

.method public i()V
    .locals 7

    .prologue
    const/4 v3, 0x0

    .line 112
    sget v0, Lco/uk/getmondo/c$a;->loadingErrorView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/help/HelpSearchActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/common/ui/LoadingErrorView;

    const v1, 0x7f0a01ba

    invoke-virtual {p0, v1}, Lco/uk/getmondo/help/HelpSearchActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, "getString(R.string.help_search_error)"

    invoke-static {v1, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v2, 0x0

    const v4, 0x7f02014f

    const/4 v5, 0x6

    move-object v6, v3

    invoke-static/range {v0 .. v6}, Lco/uk/getmondo/common/ui/LoadingErrorView;->a(Lco/uk/getmondo/common/ui/LoadingErrorView;Ljava/lang/String;ZLjava/lang/String;IILjava/lang/Object;)V

    .line 113
    sget v0, Lco/uk/getmondo/c$a;->loadingErrorView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/help/HelpSearchActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/common/ui/LoadingErrorView;

    invoke-static {v0}, Lco/uk/getmondo/common/ae;->a(Landroid/view/View;)V

    .line 114
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    .line 36
    invoke-super {p0, p1}, Lco/uk/getmondo/common/activities/b;->onCreate(Landroid/os/Bundle;)V

    .line 37
    const v0, 0x7f05003f

    invoke-virtual {p0, v0}, Lco/uk/getmondo/help/HelpSearchActivity;->setContentView(I)V

    .line 39
    invoke-virtual {p0}, Lco/uk/getmondo/help/HelpSearchActivity;->l()Lco/uk/getmondo/common/h/b/b;

    move-result-object v0

    invoke-interface {v0, p0}, Lco/uk/getmondo/common/h/b/b;->a(Lco/uk/getmondo/help/HelpSearchActivity;)V

    .line 41
    const v0, 0x7f0a01bb

    invoke-virtual {p0, v0}, Lco/uk/getmondo/help/HelpSearchActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 42
    new-instance v2, Landroid/text/SpannableString;

    move-object v0, v1

    check-cast v0, Ljava/lang/CharSequence;

    invoke-direct {v2, v0}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 43
    new-instance v0, Landroid/text/style/RelativeSizeSpan;

    const v3, 0x3f666666    # 0.9f

    invoke-direct {v0, v3}, Landroid/text/style/RelativeSizeSpan;-><init>(F)V

    const/4 v3, 0x0

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    const/16 v4, 0x21

    invoke-virtual {v2, v0, v3, v1, v4}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 44
    sget v0, Lco/uk/getmondo/c$a;->queryField:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/help/HelpSearchActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    move-object v1, v2

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setHint(Ljava/lang/CharSequence;)V

    .line 46
    new-instance v1, Landroid/support/v7/widget/LinearLayoutManager;

    move-object v0, p0

    check-cast v0, Landroid/content/Context;

    invoke-direct {v1, v0}, Landroid/support/v7/widget/LinearLayoutManager;-><init>(Landroid/content/Context;)V

    .line 47
    sget v0, Lco/uk/getmondo/c$a;->helpSearchRecyclerView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/help/HelpSearchActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView;

    check-cast v1, Landroid/support/v7/widget/RecyclerView$h;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(Landroid/support/v7/widget/RecyclerView$h;)V

    .line 48
    sget v0, Lco/uk/getmondo/c$a;->helpSearchRecyclerView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/help/HelpSearchActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView;

    iget-object v1, p0, Lco/uk/getmondo/help/HelpSearchActivity;->e:Lco/uk/getmondo/help/m;

    check-cast v1, Landroid/support/v7/widget/RecyclerView$a;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setAdapter(Landroid/support/v7/widget/RecyclerView$a;)V

    .line 49
    sget v0, Lco/uk/getmondo/c$a;->helpSearchRecyclerView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/help/HelpSearchActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setHasFixedSize(Z)V

    .line 51
    iget-object v0, p0, Lco/uk/getmondo/help/HelpSearchActivity;->a:Lco/uk/getmondo/help/j;

    if-nez v0, :cond_0

    const-string v1, "presenter"

    invoke-static {v1}, Lkotlin/d/b/l;->b(Ljava/lang/String;)V

    :cond_0
    check-cast p0, Lco/uk/getmondo/help/j$a;

    invoke-virtual {v0, p0}, Lco/uk/getmondo/help/j;->a(Lco/uk/getmondo/help/j$a;)V

    .line 52
    return-void
.end method
