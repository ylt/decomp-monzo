.class final Lco/uk/getmondo/help/f$g;
.super Ljava/lang/Object;
.source "HelpPresenter.kt"

# interfaces
.implements Lio/reactivex/c/g;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lco/uk/getmondo/help/f;->a(Lco/uk/getmondo/help/f$a;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/g",
        "<",
        "Lkotlin/n;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000\n\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0004\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0003*\u0004\u0018\u00010\u00010\u0001H\n\u00a2\u0006\u0004\u0008\u0004\u0010\u0005"
    }
    d2 = {
        "<anonymous>",
        "",
        "it",
        "kotlin.jvm.PlatformType",
        "accept",
        "(Lkotlin/Unit;)V"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field final synthetic a:Lco/uk/getmondo/help/f;

.field final synthetic b:Lco/uk/getmondo/help/f$a;


# direct methods
.method constructor <init>(Lco/uk/getmondo/help/f;Lco/uk/getmondo/help/f$a;)V
    .locals 0

    iput-object p1, p0, Lco/uk/getmondo/help/f$g;->a:Lco/uk/getmondo/help/f;

    iput-object p2, p0, Lco/uk/getmondo/help/f$g;->b:Lco/uk/getmondo/help/f$a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 23
    check-cast p1, Lkotlin/n;

    invoke-virtual {p0, p1}, Lco/uk/getmondo/help/f$g;->a(Lkotlin/n;)V

    return-void
.end method

.method public final a(Lkotlin/n;)V
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 63
    iget-object v0, p0, Lco/uk/getmondo/help/f$g;->a:Lco/uk/getmondo/help/f;

    invoke-static {v0}, Lco/uk/getmondo/help/f;->c(Lco/uk/getmondo/help/f;)Lco/uk/getmondo/common/a;

    move-result-object v6

    sget-object v0, Lco/uk/getmondo/api/model/tracking/Impression;->Companion:Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    sget-object v1, Lco/uk/getmondo/api/model/tracking/Impression$IntercomFrom;->HELP:Lco/uk/getmondo/api/model/tracking/Impression$IntercomFrom;

    const/4 v4, 0x6

    move-object v3, v2

    move-object v5, v2

    invoke-static/range {v0 .. v5}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->a(Lco/uk/getmondo/api/model/tracking/Impression$Companion;Lco/uk/getmondo/api/model/tracking/Impression$IntercomFrom;Ljava/lang/String;Ljava/lang/String;ILjava/lang/Object;)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    invoke-virtual {v6, v0}, Lco/uk/getmondo/common/a;->a(Lco/uk/getmondo/api/model/tracking/Impression;)V

    .line 64
    iget-object v0, p0, Lco/uk/getmondo/help/f$g;->b:Lco/uk/getmondo/help/f$a;

    invoke-interface {v0}, Lco/uk/getmondo/help/f$a;->g()V

    .line 65
    return-void
.end method
