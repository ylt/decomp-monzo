.class public final Lco/uk/getmondo/help/c;
.super Lco/uk/getmondo/common/ui/b;
.source "HelpCategoryPresenter.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lco/uk/getmondo/help/c$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lco/uk/getmondo/common/ui/b",
        "<",
        "Lco/uk/getmondo/help/c$a;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000:\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u0003\u0008\u0007\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00020\u0001:\u0001\u0013BG\u0008\u0007\u0012\u0008\u0008\u0001\u0010\u0003\u001a\u00020\u0004\u0012\u0008\u0008\u0001\u0010\u0005\u001a\u00020\u0004\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u0012\u0008\u0008\u0001\u0010\n\u001a\u00020\u000b\u0012\u0008\u0008\u0001\u0010\u000c\u001a\u00020\u000b\u0012\u0006\u0010\r\u001a\u00020\u000e\u00a2\u0006\u0002\u0010\u000fJ\u0010\u0010\u0010\u001a\u00020\u00112\u0006\u0010\u0012\u001a\u00020\u0002H\u0016R\u000e\u0010\r\u001a\u00020\u000eX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000c\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0014"
    }
    d2 = {
        "Lco/uk/getmondo/help/HelpCategoryPresenter;",
        "Lco/uk/getmondo/common/ui/BasePresenter;",
        "Lco/uk/getmondo/help/HelpCategoryPresenter$View;",
        "ioScheduler",
        "Lio/reactivex/Scheduler;",
        "uiScheduler",
        "helpManager",
        "Lco/uk/getmondo/help/data/HelpManager;",
        "apiErrorHandler",
        "Lco/uk/getmondo/common/errors/ApiErrorHandler;",
        "categoryId",
        "",
        "categoryTitle",
        "analyticsService",
        "Lco/uk/getmondo/common/AnalyticsService;",
        "(Lio/reactivex/Scheduler;Lio/reactivex/Scheduler;Lco/uk/getmondo/help/data/HelpManager;Lco/uk/getmondo/common/errors/ApiErrorHandler;Ljava/lang/String;Ljava/lang/String;Lco/uk/getmondo/common/AnalyticsService;)V",
        "register",
        "",
        "view",
        "View",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field private final c:Lio/reactivex/u;

.field private final d:Lio/reactivex/u;

.field private final e:Lco/uk/getmondo/help/a/a;

.field private final f:Lco/uk/getmondo/common/e/a;

.field private final g:Ljava/lang/String;

.field private final h:Ljava/lang/String;

.field private final i:Lco/uk/getmondo/common/a;


# direct methods
.method public constructor <init>(Lio/reactivex/u;Lio/reactivex/u;Lco/uk/getmondo/help/a/a;Lco/uk/getmondo/common/e/a;Ljava/lang/String;Ljava/lang/String;Lco/uk/getmondo/common/a;)V
    .locals 1

    .prologue
    const-string v0, "ioScheduler"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "uiScheduler"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "helpManager"

    invoke-static {p3, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "apiErrorHandler"

    invoke-static {p4, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "categoryId"

    invoke-static {p5, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "categoryTitle"

    invoke-static {p6, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "analyticsService"

    invoke-static {p7, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 36
    invoke-direct {p0}, Lco/uk/getmondo/common/ui/b;-><init>()V

    iput-object p1, p0, Lco/uk/getmondo/help/c;->c:Lio/reactivex/u;

    iput-object p2, p0, Lco/uk/getmondo/help/c;->d:Lio/reactivex/u;

    iput-object p3, p0, Lco/uk/getmondo/help/c;->e:Lco/uk/getmondo/help/a/a;

    iput-object p4, p0, Lco/uk/getmondo/help/c;->f:Lco/uk/getmondo/common/e/a;

    iput-object p5, p0, Lco/uk/getmondo/help/c;->g:Ljava/lang/String;

    iput-object p6, p0, Lco/uk/getmondo/help/c;->h:Ljava/lang/String;

    iput-object p7, p0, Lco/uk/getmondo/help/c;->i:Lco/uk/getmondo/common/a;

    return-void
.end method

.method public static final synthetic a(Lco/uk/getmondo/help/c;)Lco/uk/getmondo/help/a/a;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lco/uk/getmondo/help/c;->e:Lco/uk/getmondo/help/a/a;

    return-object v0
.end method

.method public static final synthetic b(Lco/uk/getmondo/help/c;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lco/uk/getmondo/help/c;->g:Ljava/lang/String;

    return-object v0
.end method

.method public static final synthetic c(Lco/uk/getmondo/help/c;)Lio/reactivex/u;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lco/uk/getmondo/help/c;->c:Lio/reactivex/u;

    return-object v0
.end method

.method public static final synthetic d(Lco/uk/getmondo/help/c;)Lio/reactivex/u;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lco/uk/getmondo/help/c;->d:Lio/reactivex/u;

    return-object v0
.end method

.method public static final synthetic e(Lco/uk/getmondo/help/c;)Lco/uk/getmondo/common/e/a;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lco/uk/getmondo/help/c;->f:Lco/uk/getmondo/common/e/a;

    return-object v0
.end method

.method public static final synthetic f(Lco/uk/getmondo/help/c;)Lco/uk/getmondo/common/a;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lco/uk/getmondo/help/c;->i:Lco/uk/getmondo/common/a;

    return-object v0
.end method


# virtual methods
.method public bridge synthetic a(Lco/uk/getmondo/common/ui/f;)V
    .locals 0

    .prologue
    .line 27
    check-cast p1, Lco/uk/getmondo/help/c$a;

    invoke-virtual {p0, p1}, Lco/uk/getmondo/help/c;->a(Lco/uk/getmondo/help/c$a;)V

    return-void
.end method

.method public a(Lco/uk/getmondo/help/c$a;)V
    .locals 6

    .prologue
    const-string v0, "view"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v0, p1

    .line 39
    check-cast v0, Lco/uk/getmondo/common/ui/f;

    invoke-super {p0, v0}, Lco/uk/getmondo/common/ui/b;->a(Lco/uk/getmondo/common/ui/f;)V

    .line 41
    iget-object v0, p0, Lco/uk/getmondo/help/c;->i:Lco/uk/getmondo/common/a;

    sget-object v1, Lco/uk/getmondo/api/model/tracking/Impression;->Companion:Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    iget-object v2, p0, Lco/uk/getmondo/help/c;->g:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->o(Ljava/lang/String;)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v1

    invoke-virtual {v0, v1}, Lco/uk/getmondo/common/a;->a(Lco/uk/getmondo/api/model/tracking/Impression;)V

    .line 43
    sget-object v0, Lco/uk/getmondo/help/a/a/c;->h:Lco/uk/getmondo/help/a/a/c$a;

    iget-object v1, p0, Lco/uk/getmondo/help/c;->g:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lco/uk/getmondo/help/a/a/c$a;->a(Ljava/lang/String;)Lco/uk/getmondo/help/a/a/c;

    move-result-object v0

    .line 44
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    if-eqz v0, :cond_3

    invoke-virtual {v0}, Lco/uk/getmondo/help/a/a/c;->c()I

    move-result v0

    invoke-static {v0}, Lco/uk/getmondo/common/k/e;->b(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/help/c;->h:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 46
    invoke-interface {p1, v3}, Lco/uk/getmondo/help/c$a;->a(Ljava/lang/String;)V

    .line 48
    iget-object v4, p0, Lco/uk/getmondo/help/c;->b:Lio/reactivex/b/a;

    .line 62
    sget-object v0, Lkotlin/n;->a:Lkotlin/n;

    invoke-static {v0}, Lio/reactivex/n;->just(Ljava/lang/Object;)Lio/reactivex/n;

    move-result-object v1

    .line 49
    invoke-interface {p1}, Lco/uk/getmondo/help/c$a;->a()Lio/reactivex/n;

    move-result-object v0

    check-cast v0, Lio/reactivex/r;

    invoke-virtual {v1, v0}, Lio/reactivex/n;->mergeWith(Lio/reactivex/r;)Lio/reactivex/n;

    move-result-object v1

    .line 50
    new-instance v0, Lco/uk/getmondo/help/c$b;

    invoke-direct {v0, p0, p1}, Lco/uk/getmondo/help/c$b;-><init>(Lco/uk/getmondo/help/c;Lco/uk/getmondo/help/c$a;)V

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {v1, v0}, Lio/reactivex/n;->flatMapMaybe(Lio/reactivex/c/h;)Lio/reactivex/n;

    move-result-object v5

    .line 62
    new-instance v0, Lco/uk/getmondo/help/c$c;

    invoke-direct {v0, p1}, Lco/uk/getmondo/help/c$c;-><init>(Lco/uk/getmondo/help/c$a;)V

    check-cast v0, Lio/reactivex/c/g;

    sget-object v1, Lco/uk/getmondo/help/c$d;->a:Lco/uk/getmondo/help/c$d;

    check-cast v1, Lkotlin/d/a/b;

    if-eqz v1, :cond_0

    new-instance v2, Lco/uk/getmondo/help/d;

    invoke-direct {v2, v1}, Lco/uk/getmondo/help/d;-><init>(Lkotlin/d/a/b;)V

    move-object v1, v2

    :cond_0
    check-cast v1, Lio/reactivex/c/g;

    invoke-virtual {v5, v0, v1}, Lio/reactivex/n;->subscribe(Lio/reactivex/c/g;Lio/reactivex/c/g;)Lio/reactivex/b/b;

    move-result-object v0

    const-string v1, "Observable.just(Unit) //\u2026Content(it) }, Timber::e)"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 48
    invoke-static {v4, v0}, Lco/uk/getmondo/common/j/f;->a(Lio/reactivex/b/a;Lio/reactivex/b/b;)Lio/reactivex/b/a;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/help/c;->b:Lio/reactivex/b/a;

    .line 64
    iget-object v4, p0, Lco/uk/getmondo/help/c;->b:Lio/reactivex/b/a;

    .line 65
    invoke-interface {p1}, Lco/uk/getmondo/help/c$a;->b()Lio/reactivex/n;

    move-result-object v5

    new-instance v0, Lco/uk/getmondo/help/c$e;

    invoke-direct {v0, p1, v3}, Lco/uk/getmondo/help/c$e;-><init>(Lco/uk/getmondo/help/c$a;Ljava/lang/String;)V

    check-cast v0, Lio/reactivex/c/g;

    sget-object v1, Lco/uk/getmondo/help/c$f;->a:Lco/uk/getmondo/help/c$f;

    check-cast v1, Lkotlin/d/a/b;

    if-eqz v1, :cond_1

    new-instance v2, Lco/uk/getmondo/help/d;

    invoke-direct {v2, v1}, Lco/uk/getmondo/help/d;-><init>(Lkotlin/d/a/b;)V

    move-object v1, v2

    :cond_1
    check-cast v1, Lio/reactivex/c/g;

    invoke-virtual {v5, v0, v1}, Lio/reactivex/n;->subscribe(Lio/reactivex/c/g;Lio/reactivex/c/g;)Lio/reactivex/b/b;

    move-result-object v0

    const-string v1, "view.topicClicked\n      \u2026pic, title) }, Timber::e)"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 64
    invoke-static {v4, v0}, Lco/uk/getmondo/common/j/f;->a(Lio/reactivex/b/a;Lio/reactivex/b/b;)Lio/reactivex/b/a;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/help/c;->b:Lio/reactivex/b/a;

    .line 67
    iget-object v3, p0, Lco/uk/getmondo/help/c;->b:Lio/reactivex/b/a;

    .line 68
    invoke-interface {p1}, Lco/uk/getmondo/help/c$a;->c()Lio/reactivex/n;

    move-result-object v4

    new-instance v0, Lco/uk/getmondo/help/c$g;

    invoke-direct {v0, p0, p1}, Lco/uk/getmondo/help/c$g;-><init>(Lco/uk/getmondo/help/c;Lco/uk/getmondo/help/c$a;)V

    check-cast v0, Lio/reactivex/c/g;

    .line 71
    sget-object v1, Lco/uk/getmondo/help/c$h;->a:Lco/uk/getmondo/help/c$h;

    check-cast v1, Lkotlin/d/a/b;

    if-eqz v1, :cond_2

    new-instance v2, Lco/uk/getmondo/help/d;

    invoke-direct {v2, v1}, Lco/uk/getmondo/help/d;-><init>(Lkotlin/d/a/b;)V

    move-object v1, v2

    :cond_2
    check-cast v1, Lio/reactivex/c/g;

    .line 68
    invoke-virtual {v4, v0, v1}, Lio/reactivex/n;->subscribe(Lio/reactivex/c/g;Lio/reactivex/c/g;)Lio/reactivex/b/b;

    move-result-object v0

    const-string v1, "view.helpClicked\n       \u2026            }, Timber::e)"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 67
    invoke-static {v3, v0}, Lco/uk/getmondo/common/j/f;->a(Lio/reactivex/b/a;Lio/reactivex/b/b;)Lio/reactivex/b/a;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/help/c;->b:Lio/reactivex/b/a;

    .line 72
    return-void

    .line 44
    :cond_3
    const/4 v0, 0x0

    goto/16 :goto_0
.end method
