.class public final Lco/uk/getmondo/help/m;
.super Landroid/support/v7/widget/RecyclerView$a;
.source "HelpSectionAdapter.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lco/uk/getmondo/help/m$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/support/v7/widget/RecyclerView$a",
        "<",
        "Lco/uk/getmondo/help/m$a;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000L\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010!\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\n\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\t\n\u0002\u0008\u0006\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010 \n\u0002\u0008\u0002\u0018\u00002\u000c\u0012\u0008\u0012\u00060\u0002R\u00020\u00000\u0001:\u0001#B?\u0012\u000e\u0008\u0002\u0010\u0003\u001a\u0008\u0012\u0004\u0012\u00020\u00050\u0004\u0012\u0016\u0008\u0002\u0010\u0006\u001a\u0010\u0012\u0004\u0012\u00020\u0008\u0012\u0004\u0012\u00020\t\u0018\u00010\u0007\u0012\u0010\u0008\u0002\u0010\n\u001a\n\u0012\u0004\u0012\u00020\t\u0018\u00010\u000b\u00a2\u0006\u0002\u0010\u000cJ\u0008\u0010\u0015\u001a\u00020\u0016H\u0016J\u0010\u0010\u0017\u001a\u00020\u00182\u0006\u0010\u0019\u001a\u00020\u0016H\u0016J\u0010\u0010\u001a\u001a\u00020\u00162\u0006\u0010\u0019\u001a\u00020\u0016H\u0016J\u001c\u0010\u001b\u001a\u00020\t2\n\u0010\u001c\u001a\u00060\u0002R\u00020\u00002\u0006\u0010\u0019\u001a\u00020\u0016H\u0016J\u001c\u0010\u001d\u001a\u00060\u0002R\u00020\u00002\u0006\u0010\u001e\u001a\u00020\u001f2\u0006\u0010 \u001a\u00020\u0016H\u0016J\u0014\u0010!\u001a\u00020\t2\u000c\u0010\u0003\u001a\u0008\u0012\u0004\u0012\u00020\u00050\"R\"\u0010\n\u001a\n\u0012\u0004\u0012\u00020\t\u0018\u00010\u000bX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\r\u0010\u000e\"\u0004\u0008\u000f\u0010\u0010R\u0014\u0010\u0003\u001a\u0008\u0012\u0004\u0012\u00020\u00050\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000R(\u0010\u0006\u001a\u0010\u0012\u0004\u0012\u00020\u0008\u0012\u0004\u0012\u00020\t\u0018\u00010\u0007X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u0011\u0010\u0012\"\u0004\u0008\u0013\u0010\u0014\u00a8\u0006$"
    }
    d2 = {
        "Lco/uk/getmondo/help/HelpSectionAdapter;",
        "Landroid/support/v7/widget/RecyclerView$Adapter;",
        "Lco/uk/getmondo/help/HelpSectionAdapter$ItemViewHolder;",
        "items",
        "",
        "Lco/uk/getmondo/help/data/model/SectionItem;",
        "topicClickListener",
        "Lkotlin/Function1;",
        "Lco/uk/getmondo/help/data/model/SectionItem$TopicItem;",
        "",
        "helpClickListener",
        "Lkotlin/Function0;",
        "(Ljava/util/List;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;)V",
        "getHelpClickListener",
        "()Lkotlin/jvm/functions/Function0;",
        "setHelpClickListener",
        "(Lkotlin/jvm/functions/Function0;)V",
        "getTopicClickListener",
        "()Lkotlin/jvm/functions/Function1;",
        "setTopicClickListener",
        "(Lkotlin/jvm/functions/Function1;)V",
        "getItemCount",
        "",
        "getItemId",
        "",
        "position",
        "getItemViewType",
        "onBindViewHolder",
        "holder",
        "onCreateViewHolder",
        "parent",
        "Landroid/view/ViewGroup;",
        "viewType",
        "setItems",
        "",
        "ItemViewHolder",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field private final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lco/uk/getmondo/help/a/a/d;",
            ">;"
        }
    .end annotation
.end field

.field private b:Lkotlin/d/a/b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/d/a/b",
            "<-",
            "Lco/uk/getmondo/help/a/a/d$c;",
            "Lkotlin/n;",
            ">;"
        }
    .end annotation
.end field

.field private c:Lkotlin/d/a/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/d/a/a",
            "<",
            "Lkotlin/n;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 6

    const/4 v1, 0x0

    const/4 v4, 0x7

    move-object v0, p0

    move-object v2, v1

    move-object v3, v1

    move-object v5, v1

    invoke-direct/range {v0 .. v5}, Lco/uk/getmondo/help/m;-><init>(Ljava/util/List;Lkotlin/d/a/b;Lkotlin/d/a/a;ILkotlin/d/b/i;)V

    return-void
.end method

.method public constructor <init>(Ljava/util/List;Lkotlin/d/a/b;Lkotlin/d/a/a;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lco/uk/getmondo/help/a/a/d;",
            ">;",
            "Lkotlin/d/a/b",
            "<-",
            "Lco/uk/getmondo/help/a/a/d$c;",
            "Lkotlin/n;",
            ">;",
            "Lkotlin/d/a/a",
            "<",
            "Lkotlin/n;",
            ">;)V"
        }
    .end annotation

    .prologue
    const-string v0, "items"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 19
    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView$a;-><init>()V

    iput-object p1, p0, Lco/uk/getmondo/help/m;->a:Ljava/util/List;

    iput-object p2, p0, Lco/uk/getmondo/help/m;->b:Lkotlin/d/a/b;

    iput-object p3, p0, Lco/uk/getmondo/help/m;->c:Lkotlin/d/a/a;

    .line 22
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lco/uk/getmondo/help/m;->setHasStableIds(Z)V

    return-void
.end method

.method public synthetic constructor <init>(Ljava/util/List;Lkotlin/d/a/b;Lkotlin/d/a/a;ILkotlin/d/b/i;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    and-int/lit8 v0, p4, 0x1

    if-eqz v0, :cond_0

    .line 16
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    check-cast v0, Ljava/util/List;

    move-object p1, v0

    :cond_0
    and-int/lit8 v0, p4, 0x2

    if-eqz v0, :cond_2

    move-object v0, v1

    .line 17
    check-cast v0, Lkotlin/d/a/b;

    :goto_0
    and-int/lit8 v2, p4, 0x4

    if-eqz v2, :cond_1

    .line 18
    check-cast v1, Lkotlin/d/a/a;

    :goto_1
    invoke-direct {p0, p1, v0, v1}, Lco/uk/getmondo/help/m;-><init>(Ljava/util/List;Lkotlin/d/a/b;Lkotlin/d/a/a;)V

    return-void

    :cond_1
    move-object v1, p3

    goto :goto_1

    :cond_2
    move-object v0, p2

    goto :goto_0
.end method

.method public static final synthetic a(Lco/uk/getmondo/help/m;)Ljava/util/List;
    .locals 1

    .prologue
    .line 15
    iget-object v0, p0, Lco/uk/getmondo/help/m;->a:Ljava/util/List;

    return-object v0
.end method


# virtual methods
.method public a(Landroid/view/ViewGroup;I)Lco/uk/getmondo/help/m$a;
    .locals 4

    .prologue
    const-string v0, "parent"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 26
    new-instance v0, Lco/uk/getmondo/help/m$a;

    const/4 v1, 0x0

    const/4 v2, 0x2

    const/4 v3, 0x0

    invoke-static {p1, p2, v1, v2, v3}, Lco/uk/getmondo/common/ui/m;->a(Landroid/view/ViewGroup;IZILjava/lang/Object;)Landroid/view/View;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lco/uk/getmondo/help/m$a;-><init>(Lco/uk/getmondo/help/m;Landroid/view/View;)V

    return-object v0
.end method

.method public final a()Lkotlin/d/a/b;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/d/a/b",
            "<",
            "Lco/uk/getmondo/help/a/a/d$c;",
            "Lkotlin/n;",
            ">;"
        }
    .end annotation

    .prologue
    .line 17
    iget-object v0, p0, Lco/uk/getmondo/help/m;->b:Lkotlin/d/a/b;

    return-object v0
.end method

.method public a(Lco/uk/getmondo/help/m$a;I)V
    .locals 1

    .prologue
    const-string v0, "holder"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 30
    iget-object v0, p0, Lco/uk/getmondo/help/m;->a:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/help/a/a/d;

    invoke-virtual {p1, v0}, Lco/uk/getmondo/help/m$a;->a(Lco/uk/getmondo/help/a/a/d;)V

    .line 31
    return-void
.end method

.method public final a(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<+",
            "Lco/uk/getmondo/help/a/a/d;",
            ">;)V"
        }
    .end annotation

    .prologue
    const-string v0, "items"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 40
    iget-object v0, p0, Lco/uk/getmondo/help/m;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 41
    iget-object v0, p0, Lco/uk/getmondo/help/m;->a:Ljava/util/List;

    check-cast p1, Ljava/util/Collection;

    invoke-interface {v0, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 42
    invoke-virtual {p0}, Lco/uk/getmondo/help/m;->notifyDataSetChanged()V

    .line 43
    return-void
.end method

.method public final a(Lkotlin/d/a/a;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/d/a/a",
            "<",
            "Lkotlin/n;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 18
    iput-object p1, p0, Lco/uk/getmondo/help/m;->c:Lkotlin/d/a/a;

    return-void
.end method

.method public final a(Lkotlin/d/a/b;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/d/a/b",
            "<-",
            "Lco/uk/getmondo/help/a/a/d$c;",
            "Lkotlin/n;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 17
    iput-object p1, p0, Lco/uk/getmondo/help/m;->b:Lkotlin/d/a/b;

    return-void
.end method

.method public final b()Lkotlin/d/a/a;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/d/a/a",
            "<",
            "Lkotlin/n;",
            ">;"
        }
    .end annotation

    .prologue
    .line 18
    iget-object v0, p0, Lco/uk/getmondo/help/m;->c:Lkotlin/d/a/a;

    return-object v0
.end method

.method public getItemCount()I
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lco/uk/getmondo/help/m;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getItemId(I)J
    .locals 2

    .prologue
    .line 37
    iget-object v0, p0, Lco/uk/getmondo/help/m;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/help/a/a/d;

    invoke-virtual {v0}, Lco/uk/getmondo/help/a/a/d;->a()J

    move-result-wide v0

    return-wide v0
.end method

.method public getItemViewType(I)I
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lco/uk/getmondo/help/m;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/help/a/a/d;

    invoke-virtual {v0}, Lco/uk/getmondo/help/a/a/d;->b()I

    move-result v0

    return v0
.end method

.method public synthetic onBindViewHolder(Landroid/support/v7/widget/RecyclerView$w;I)V
    .locals 0

    .prologue
    .line 15
    check-cast p1, Lco/uk/getmondo/help/m$a;

    invoke-virtual {p0, p1, p2}, Lco/uk/getmondo/help/m;->a(Lco/uk/getmondo/help/m$a;I)V

    return-void
.end method

.method public synthetic onCreateViewHolder(Landroid/view/ViewGroup;I)Landroid/support/v7/widget/RecyclerView$w;
    .locals 1

    .prologue
    .line 15
    invoke-virtual {p0, p1, p2}, Lco/uk/getmondo/help/m;->a(Landroid/view/ViewGroup;I)Lco/uk/getmondo/help/m$a;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView$w;

    return-object v0
.end method
