.class public final Lco/uk/getmondo/help/HelpActivity;
.super Lco/uk/getmondo/common/activities/b;
.source "HelpActivity.kt"

# interfaces
.implements Lco/uk/getmondo/help/f$a;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lco/uk/getmondo/help/HelpActivity$a;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000v\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0008\u0007\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0002\u0008\t\n\u0002\u0010\u000b\n\u0002\u0008\u0003\n\u0002\u0010 \n\u0002\u0008\u0002\u0018\u0000 :2\u00020\u00012\u00020\u0002:\u0001:B\u0005\u00a2\u0006\u0002\u0010\u0003J\u0012\u0010&\u001a\u00020\u00112\u0008\u0010\'\u001a\u0004\u0018\u00010(H\u0014J\u0010\u0010)\u001a\u00020\u00112\u0006\u0010*\u001a\u00020+H\u0016J\u0008\u0010,\u001a\u00020\u0011H\u0016J\u0010\u0010-\u001a\u00020\u00112\u0006\u0010.\u001a\u00020+H\u0016J\u0008\u0010/\u001a\u00020\u0011H\u0016J\u0018\u00100\u001a\u00020\u00112\u0006\u00101\u001a\u00020$2\u0006\u00102\u001a\u00020+H\u0016J\u0010\u00103\u001a\u00020\u00112\u0006\u00104\u001a\u000205H\u0016J\u0010\u00106\u001a\u00020\u00112\u0006\u00104\u001a\u000205H\u0016J\u0016\u00107\u001a\u00020\u00112\u000c\u00108\u001a\u0008\u0012\u0004\u0012\u00020$09H\u0016R2\u0010\u0004\u001a&\u0012\u000c\u0012\n \u0007*\u0004\u0018\u00010\u00060\u0006 \u0007*\u0012\u0012\u000c\u0012\n \u0007*\u0004\u0018\u00010\u00060\u0006\u0018\u00010\u00050\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001a\u0010\u0008\u001a\u0008\u0012\u0004\u0012\u00020\n0\t8VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u000b\u0010\u000cR\u001a\u0010\r\u001a\u0008\u0012\u0004\u0012\u00020\u000e0\t8VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u000f\u0010\u000cR\u001a\u0010\u0010\u001a\u0008\u0012\u0004\u0012\u00020\u00110\t8VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0012\u0010\u000cR\u001e\u0010\u0013\u001a\u00020\u00148\u0006@\u0006X\u0087.\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u0015\u0010\u0016\"\u0004\u0008\u0017\u0010\u0018R\u001e\u0010\u0019\u001a\u00020\u001a8\u0006@\u0006X\u0087.\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u001b\u0010\u001c\"\u0004\u0008\u001d\u0010\u001eR\u001a\u0010\u001f\u001a\u0008\u0012\u0004\u0012\u00020\u00110\t8VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008 \u0010\u000cR\u000e\u0010!\u001a\u00020\"X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001a\u0010#\u001a\u0008\u0012\u0004\u0012\u00020$0\t8VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008%\u0010\u000c\u00a8\u0006;"
    }
    d2 = {
        "Lco/uk/getmondo/help/HelpActivity;",
        "Lco/uk/getmondo/common/activities/BaseActivity;",
        "Lco/uk/getmondo/help/HelpPresenter$View;",
        "()V",
        "categoryClickRelay",
        "Lcom/jakewharton/rxrelay2/PublishRelay;",
        "Lco/uk/getmondo/help/data/model/Category;",
        "kotlin.jvm.PlatformType",
        "categoryClicked",
        "Lio/reactivex/Observable;",
        "Lco/uk/getmondo/help/data/model/HelpCategory;",
        "getCategoryClicked",
        "()Lio/reactivex/Observable;",
        "communityForumClicked",
        "Lco/uk/getmondo/help/data/model/CommunityCategory;",
        "getCommunityForumClicked",
        "helpClicked",
        "",
        "getHelpClicked",
        "intercomService",
        "Lco/uk/getmondo/common/IntercomService;",
        "getIntercomService",
        "()Lco/uk/getmondo/common/IntercomService;",
        "setIntercomService",
        "(Lco/uk/getmondo/common/IntercomService;)V",
        "presenter",
        "Lco/uk/getmondo/help/HelpPresenter;",
        "getPresenter",
        "()Lco/uk/getmondo/help/HelpPresenter;",
        "setPresenter",
        "(Lco/uk/getmondo/help/HelpPresenter;)V",
        "searchClicked",
        "getSearchClicked",
        "trendingAdapter",
        "Lco/uk/getmondo/help/TrendingAdapter;",
        "trendingTopicClicked",
        "Lco/uk/getmondo/api/model/help/Topic;",
        "getTrendingTopicClicked",
        "onCreate",
        "savedInstanceState",
        "Landroid/os/Bundle;",
        "openCategory",
        "categoryId",
        "",
        "openChat",
        "openCommunity",
        "url",
        "openSearch",
        "openTopic",
        "topic",
        "title",
        "setTrendingError",
        "isEnabled",
        "",
        "setTrendingLoading",
        "showTrendingTopics",
        "topics",
        "",
        "Companion",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# static fields
.field public static final c:Lco/uk/getmondo/help/HelpActivity$a;


# instance fields
.field public a:Lco/uk/getmondo/help/f;

.field public b:Lco/uk/getmondo/common/q;

.field private final e:Lco/uk/getmondo/help/r;

.field private final f:Lcom/b/b/c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/b/b/c",
            "<",
            "Lco/uk/getmondo/help/a/a/a;",
            ">;"
        }
    .end annotation
.end field

.field private g:Ljava/util/HashMap;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lco/uk/getmondo/help/HelpActivity$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lco/uk/getmondo/help/HelpActivity$a;-><init>(Lkotlin/d/b/i;)V

    sput-object v0, Lco/uk/getmondo/help/HelpActivity;->c:Lco/uk/getmondo/help/HelpActivity$a;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 28
    invoke-direct {p0}, Lco/uk/getmondo/common/activities/b;-><init>()V

    .line 33
    new-instance v0, Lco/uk/getmondo/help/r;

    const/4 v1, 0x3

    invoke-direct {v0, v2, v2, v1, v2}, Lco/uk/getmondo/help/r;-><init>(Ljava/util/List;Lkotlin/d/a/b;ILkotlin/d/b/i;)V

    iput-object v0, p0, Lco/uk/getmondo/help/HelpActivity;->e:Lco/uk/getmondo/help/r;

    .line 34
    invoke-static {}, Lcom/b/b/c;->a()Lcom/b/b/c;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/help/HelpActivity;->f:Lcom/b/b/c;

    return-void
.end method

.method public static final synthetic a(Lco/uk/getmondo/help/HelpActivity;)Lcom/b/b/c;
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Lco/uk/getmondo/help/HelpActivity;->f:Lcom/b/b/c;

    return-object v0
.end method

.method public static final synthetic b(Lco/uk/getmondo/help/HelpActivity;)Lco/uk/getmondo/help/r;
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Lco/uk/getmondo/help/HelpActivity;->e:Lco/uk/getmondo/help/r;

    return-object v0
.end method


# virtual methods
.method public a(I)Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lco/uk/getmondo/help/HelpActivity;->g:Ljava/util/HashMap;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lco/uk/getmondo/help/HelpActivity;->g:Ljava/util/HashMap;

    :cond_0
    iget-object v0, p0, Lco/uk/getmondo/help/HelpActivity;->g:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    if-nez v0, :cond_1

    invoke-virtual {p0, p1}, Landroid/support/v4/app/j;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/help/HelpActivity;->g:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    return-object v0
.end method

.method public a()Lio/reactivex/n;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Lco/uk/getmondo/help/a/a/c;",
            ">;"
        }
    .end annotation

    .prologue
    .line 69
    iget-object v1, p0, Lco/uk/getmondo/help/HelpActivity;->f:Lcom/b/b/c;

    .line 70
    sget-object v0, Lco/uk/getmondo/help/HelpActivity$b;->a:Lco/uk/getmondo/help/HelpActivity$b;

    check-cast v0, Lio/reactivex/c/q;

    invoke-virtual {v1, v0}, Lcom/b/b/c;->filter(Lio/reactivex/c/q;)Lio/reactivex/n;

    move-result-object v1

    .line 71
    sget-object v0, Lco/uk/getmondo/help/HelpActivity$c;->a:Lco/uk/getmondo/help/HelpActivity$c;

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {v1, v0}, Lio/reactivex/n;->map(Lio/reactivex/c/h;)Lio/reactivex/n;

    move-result-object v0

    const-string v1, "categoryClickRelay\n     \u2026elpCategory.from(it.id) }"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public a(Lco/uk/getmondo/api/model/help/Topic;Ljava/lang/String;)V
    .locals 8

    .prologue
    const/4 v3, 0x0

    const-string v0, "topic"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "title"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 109
    sget-object v7, Lco/uk/getmondo/help/HelpTopicActivity;->e:Lco/uk/getmondo/help/HelpTopicActivity$a;

    move-object v6, p0

    check-cast v6, Landroid/content/Context;

    new-instance v0, Lco/uk/getmondo/help/a/a/e;

    const/4 v4, 0x4

    move-object v1, p2

    move-object v2, p1

    move-object v5, v3

    invoke-direct/range {v0 .. v5}, Lco/uk/getmondo/help/a/a/e;-><init>(Ljava/lang/String;Lco/uk/getmondo/api/model/help/Topic;Ljava/lang/String;ILkotlin/d/b/i;)V

    invoke-virtual {v7, v6, v0}, Lco/uk/getmondo/help/HelpTopicActivity$a;->a(Landroid/content/Context;Lco/uk/getmondo/help/a/a/e;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lco/uk/getmondo/help/HelpActivity;->startActivity(Landroid/content/Intent;)V

    .line 110
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 2

    .prologue
    const-string v0, "categoryId"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 105
    sget-object v1, Lco/uk/getmondo/help/HelpCategoryActivity;->e:Lco/uk/getmondo/help/HelpCategoryActivity$a;

    move-object v0, p0

    check-cast v0, Landroid/content/Context;

    invoke-virtual {v1, v0, p1}, Lco/uk/getmondo/help/HelpCategoryActivity$a;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lco/uk/getmondo/help/HelpActivity;->startActivity(Landroid/content/Intent;)V

    .line 106
    return-void
.end method

.method public a(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lco/uk/getmondo/api/model/help/Topic;",
            ">;)V"
        }
    .end annotation

    .prologue
    const-string v0, "topics"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 89
    iget-object v0, p0, Lco/uk/getmondo/help/HelpActivity;->e:Lco/uk/getmondo/help/r;

    invoke-virtual {v0, p1}, Lco/uk/getmondo/help/r;->a(Ljava/util/List;)V

    .line 90
    return-void
.end method

.method public a(Z)V
    .locals 1

    .prologue
    .line 93
    if-eqz p1, :cond_0

    sget v0, Lco/uk/getmondo/c$a;->trendingProgress:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/help/HelpActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    invoke-static {v0}, Lco/uk/getmondo/common/ae;->a(Landroid/view/View;)V

    .line 94
    :goto_0
    return-void

    .line 93
    :cond_0
    sget v0, Lco/uk/getmondo/c$a;->trendingProgress:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/help/HelpActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    invoke-static {v0}, Lco/uk/getmondo/common/ae;->b(Landroid/view/View;)V

    goto :goto_0
.end method

.method public b()Lio/reactivex/n;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Lco/uk/getmondo/api/model/help/Topic;",
            ">;"
        }
    .end annotation

    .prologue
    .line 74
    new-instance v0, Lco/uk/getmondo/help/HelpActivity$h;

    invoke-direct {v0, p0}, Lco/uk/getmondo/help/HelpActivity$h;-><init>(Lco/uk/getmondo/help/HelpActivity;)V

    check-cast v0, Lio/reactivex/p;

    invoke-static {v0}, Lio/reactivex/n;->create(Lio/reactivex/p;)Lio/reactivex/n;

    move-result-object v0

    const-string v1, "Observable.create { emit\u2026stener = null }\n        }"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 77
    return-object v0
.end method

.method public b(Z)V
    .locals 1

    .prologue
    .line 97
    if-eqz p1, :cond_0

    sget v0, Lco/uk/getmondo/c$a;->trendingErrorText:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/help/HelpActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-static {v0}, Lco/uk/getmondo/common/ae;->a(Landroid/view/View;)V

    .line 98
    :goto_0
    return-void

    .line 97
    :cond_0
    sget v0, Lco/uk/getmondo/c$a;->trendingErrorText:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/help/HelpActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-static {v0}, Lco/uk/getmondo/common/ae;->b(Landroid/view/View;)V

    goto :goto_0
.end method

.method public c()Lio/reactivex/n;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Lco/uk/getmondo/help/a/a/b;",
            ">;"
        }
    .end annotation

    .prologue
    .line 80
    iget-object v1, p0, Lco/uk/getmondo/help/HelpActivity;->f:Lcom/b/b/c;

    .line 81
    sget-object v0, Lco/uk/getmondo/help/HelpActivity$d;->a:Lco/uk/getmondo/help/HelpActivity$d;

    check-cast v0, Lio/reactivex/c/q;

    invoke-virtual {v1, v0}, Lcom/b/b/c;->filter(Lio/reactivex/c/q;)Lio/reactivex/n;

    move-result-object v1

    .line 82
    sget-object v0, Lco/uk/getmondo/help/HelpActivity$e;->a:Lco/uk/getmondo/help/HelpActivity$e;

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {v1, v0}, Lio/reactivex/n;->map(Lio/reactivex/c/h;)Lio/reactivex/n;

    move-result-object v0

    const-string v1, "categoryClickRelay\n     \u2026ityCategory.from(it.id) }"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public d()Lio/reactivex/n;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Lkotlin/n;",
            ">;"
        }
    .end annotation

    .prologue
    .line 84
    sget v0, Lco/uk/getmondo/c$a;->helpButton:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/help/HelpActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 152
    invoke-static {v0}, Lcom/b/a/c/c;->a(Landroid/view/View;)Lio/reactivex/n;

    move-result-object v1

    sget-object v0, Lcom/b/a/a/d;->a:Lcom/b/a/a/d;

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {v1, v0}, Lio/reactivex/n;->map(Lio/reactivex/c/h;)Lio/reactivex/n;

    move-result-object v0

    const-string v1, "RxView.clicks(this).map(VoidToUnit)"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 84
    return-object v0
.end method

.method public d(Ljava/lang/String;)V
    .locals 2

    .prologue
    const-string v0, "url"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 113
    const v0, 0x7f0f000f

    const/4 v1, 0x1

    invoke-static {p0, p1, v0, v1}, Lco/uk/getmondo/common/activities/a;->a(Landroid/app/Activity;Ljava/lang/String;IZ)V

    .line 114
    return-void
.end method

.method public e()Lio/reactivex/n;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Lkotlin/n;",
            ">;"
        }
    .end annotation

    .prologue
    .line 86
    sget v0, Lco/uk/getmondo/c$a;->helpSearchLayout:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/help/HelpActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 153
    invoke-static {v0}, Lcom/b/a/c/c;->a(Landroid/view/View;)Lio/reactivex/n;

    move-result-object v1

    sget-object v0, Lcom/b/a/a/d;->a:Lcom/b/a/a/d;

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {v1, v0}, Lio/reactivex/n;->map(Lio/reactivex/c/h;)Lio/reactivex/n;

    move-result-object v0

    const-string v1, "RxView.clicks(this).map(VoidToUnit)"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 86
    return-object v0
.end method

.method public f()V
    .locals 2

    .prologue
    .line 101
    sget-object v1, Lco/uk/getmondo/help/HelpSearchActivity;->c:Lco/uk/getmondo/help/HelpSearchActivity$a;

    move-object v0, p0

    check-cast v0, Landroid/content/Context;

    invoke-virtual {v1, v0}, Lco/uk/getmondo/help/HelpSearchActivity$a;->a(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lco/uk/getmondo/help/HelpActivity;->startActivity(Landroid/content/Intent;)V

    .line 102
    return-void
.end method

.method public g()V
    .locals 2

    .prologue
    .line 117
    iget-object v0, p0, Lco/uk/getmondo/help/HelpActivity;->b:Lco/uk/getmondo/common/q;

    if-nez v0, :cond_0

    const-string v1, "intercomService"

    invoke-static {v1}, Lkotlin/d/b/l;->b(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0}, Lco/uk/getmondo/common/q;->a()V

    .line 118
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 37
    invoke-super {p0, p1}, Lco/uk/getmondo/common/activities/b;->onCreate(Landroid/os/Bundle;)V

    .line 38
    const v0, 0x7f05003d

    invoke-virtual {p0, v0}, Lco/uk/getmondo/help/HelpActivity;->setContentView(I)V

    .line 40
    invoke-virtual {p0}, Lco/uk/getmondo/help/HelpActivity;->l()Lco/uk/getmondo/common/h/b/b;

    move-result-object v0

    invoke-interface {v0, p0}, Lco/uk/getmondo/common/h/b/b;->a(Lco/uk/getmondo/help/HelpActivity;)V

    .line 43
    sget v0, Lco/uk/getmondo/c$a;->categoriesLayout:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/help/HelpActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/flexbox/FlexboxLayout;

    invoke-static {v0}, Lco/uk/getmondo/common/ui/m;->a(Landroid/view/ViewGroup;)Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 44
    nop

    .line 124
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    check-cast v1, Ljava/util/Collection;

    .line 131
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    instance-of v4, v2, Lco/uk/getmondo/help/ui/HelpCategoryView;

    if-eqz v4, :cond_0

    invoke-interface {v1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 132
    :cond_1
    check-cast v1, Ljava/util/List;

    check-cast v1, Ljava/lang/Iterable;

    .line 45
    nop

    .line 134
    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v5

    move v2, v3

    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    add-int/lit8 v4, v2, 0x1

    move-object v1, v0

    check-cast v1, Lco/uk/getmondo/help/ui/HelpCategoryView;

    .line 46
    invoke-static {}, Lco/uk/getmondo/help/a/a/c;->values()[Lco/uk/getmondo/help/a/a/c;

    move-result-object v0

    aget-object v0, v0, v2

    check-cast v0, Lco/uk/getmondo/help/a/a/a;

    invoke-virtual {v1, v0}, Lco/uk/getmondo/help/ui/HelpCategoryView;->a(Lco/uk/getmondo/help/a/a/a;)V

    .line 47
    new-instance v0, Lco/uk/getmondo/help/HelpActivity$f;

    invoke-direct {v0, p0}, Lco/uk/getmondo/help/HelpActivity$f;-><init>(Lco/uk/getmondo/help/HelpActivity;)V

    check-cast v0, Lkotlin/d/a/b;

    invoke-virtual {v1, v0}, Lco/uk/getmondo/help/ui/HelpCategoryView;->setClickListener(Lkotlin/d/a/b;)V

    .line 48
    nop

    move v2, v4

    goto :goto_1

    .line 135
    :cond_2
    nop

    .line 51
    new-instance v1, Landroid/support/v7/widget/LinearLayoutManager;

    move-object v0, p0

    check-cast v0, Landroid/content/Context;

    invoke-direct {v1, v0}, Landroid/support/v7/widget/LinearLayoutManager;-><init>(Landroid/content/Context;)V

    .line 52
    sget v0, Lco/uk/getmondo/c$a;->trendingRecyclerView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/help/HelpActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView;

    check-cast v1, Landroid/support/v7/widget/RecyclerView$h;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(Landroid/support/v7/widget/RecyclerView$h;)V

    .line 53
    sget v0, Lco/uk/getmondo/c$a;->trendingRecyclerView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/help/HelpActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView;

    iget-object v1, p0, Lco/uk/getmondo/help/HelpActivity;->e:Lco/uk/getmondo/help/r;

    check-cast v1, Landroid/support/v7/widget/RecyclerView$a;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setAdapter(Landroid/support/v7/widget/RecyclerView$a;)V

    .line 54
    sget v0, Lco/uk/getmondo/c$a;->trendingRecyclerView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/help/HelpActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setHasFixedSize(Z)V

    .line 57
    sget v0, Lco/uk/getmondo/c$a;->communityCategoryGroup:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/help/HelpActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/constraint/Group;

    invoke-virtual {v0}, Landroid/support/constraint/Group;->getReferencedIds()[I

    move-result-object v2

    .line 58
    nop

    .line 136
    new-instance v0, Ljava/util/ArrayList;

    array-length v1, v2

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v0, Ljava/util/Collection;

    move v1, v3

    .line 137
    :goto_2
    array-length v4, v2

    if-ge v1, v4, :cond_3

    aget v4, v2, v1

    .line 58
    invoke-virtual {p0, v4}, Lco/uk/getmondo/help/HelpActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-interface {v0, v4}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 137
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 139
    :cond_3
    check-cast v0, Ljava/util/List;

    check-cast v0, Ljava/lang/Iterable;

    .line 59
    nop

    .line 140
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    check-cast v1, Ljava/util/Collection;

    .line 147
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_4
    :goto_3
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    instance-of v4, v2, Lco/uk/getmondo/help/ui/HelpCategoryView;

    if-eqz v4, :cond_4

    invoke-interface {v1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 148
    :cond_5
    check-cast v1, Ljava/util/List;

    check-cast v1, Ljava/lang/Iterable;

    .line 60
    nop

    .line 150
    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_4
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    add-int/lit8 v2, v3, 0x1

    move-object v1, v0

    check-cast v1, Lco/uk/getmondo/help/ui/HelpCategoryView;

    .line 61
    invoke-static {}, Lco/uk/getmondo/help/a/a/b;->values()[Lco/uk/getmondo/help/a/a/b;

    move-result-object v0

    aget-object v0, v0, v3

    check-cast v0, Lco/uk/getmondo/help/a/a/a;

    invoke-virtual {v1, v0}, Lco/uk/getmondo/help/ui/HelpCategoryView;->a(Lco/uk/getmondo/help/a/a/a;)V

    .line 62
    new-instance v0, Lco/uk/getmondo/help/HelpActivity$g;

    invoke-direct {v0, p0}, Lco/uk/getmondo/help/HelpActivity$g;-><init>(Lco/uk/getmondo/help/HelpActivity;)V

    check-cast v0, Lkotlin/d/a/b;

    invoke-virtual {v1, v0}, Lco/uk/getmondo/help/ui/HelpCategoryView;->setClickListener(Lkotlin/d/a/b;)V

    .line 63
    nop

    move v3, v2

    goto :goto_4

    .line 151
    :cond_6
    nop

    .line 65
    iget-object v0, p0, Lco/uk/getmondo/help/HelpActivity;->a:Lco/uk/getmondo/help/f;

    if-nez v0, :cond_7

    const-string v1, "presenter"

    invoke-static {v1}, Lkotlin/d/b/l;->b(Ljava/lang/String;)V

    :cond_7
    check-cast p0, Lco/uk/getmondo/help/f$a;

    invoke-virtual {v0, p0}, Lco/uk/getmondo/help/f;->a(Lco/uk/getmondo/help/f$a;)V

    .line 66
    return-void
.end method
