.class final Lco/uk/getmondo/help/f$c;
.super Ljava/lang/Object;
.source "HelpPresenter.kt"

# interfaces
.implements Lio/reactivex/c/g;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lco/uk/getmondo/help/f;->a(Lco/uk/getmondo/help/f$a;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/g",
        "<",
        "Lco/uk/getmondo/api/model/help/Topic;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "",
        "it",
        "Lco/uk/getmondo/api/model/help/Topic;",
        "kotlin.jvm.PlatformType",
        "accept"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field final synthetic a:Lco/uk/getmondo/help/f;

.field final synthetic b:Lco/uk/getmondo/help/f$a;


# direct methods
.method constructor <init>(Lco/uk/getmondo/help/f;Lco/uk/getmondo/help/f$a;)V
    .locals 0

    iput-object p1, p0, Lco/uk/getmondo/help/f$c;->a:Lco/uk/getmondo/help/f;

    iput-object p2, p0, Lco/uk/getmondo/help/f$c;->b:Lco/uk/getmondo/help/f$a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lco/uk/getmondo/api/model/help/Topic;)V
    .locals 2

    .prologue
    .line 56
    iget-object v0, p0, Lco/uk/getmondo/help/f$c;->b:Lco/uk/getmondo/help/f$a;

    const-string v1, "it"

    invoke-static {p1, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v1, p0, Lco/uk/getmondo/help/f$c;->a:Lco/uk/getmondo/help/f;

    invoke-static {v1}, Lco/uk/getmondo/help/f;->b(Lco/uk/getmondo/help/f;)Lco/uk/getmondo/help/a/a;

    move-result-object v1

    invoke-virtual {v1}, Lco/uk/getmondo/help/a/a;->a()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Lco/uk/getmondo/help/f$a;->a(Lco/uk/getmondo/api/model/help/Topic;Ljava/lang/String;)V

    return-void
.end method

.method public bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 23
    check-cast p1, Lco/uk/getmondo/api/model/help/Topic;

    invoke-virtual {p0, p1}, Lco/uk/getmondo/help/f$c;->a(Lco/uk/getmondo/api/model/help/Topic;)V

    return-void
.end method
