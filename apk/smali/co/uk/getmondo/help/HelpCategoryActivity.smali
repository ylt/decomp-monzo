.class public final Lco/uk/getmondo/help/HelpCategoryActivity;
.super Lco/uk/getmondo/common/activities/b;
.source "HelpCategoryActivity.kt"

# interfaces
.implements Lco/uk/getmondo/help/c$a;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lco/uk/getmondo/help/HelpCategoryActivity$a;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0002\u0008\u0006\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0008\u0007\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u000b\n\u0002\u0008\u0003\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0018\u0000 42\u00020\u00012\u00020\u0002:\u00014B\u0005\u00a2\u0006\u0002\u0010\u0003J\u0012\u0010#\u001a\u00020\r2\u0008\u0010$\u001a\u0004\u0018\u00010%H\u0014J\u0008\u0010&\u001a\u00020\rH\u0016J\u0018\u0010\'\u001a\u00020\r2\u0006\u0010(\u001a\u00020)2\u0006\u0010*\u001a\u00020\u0005H\u0016J\u0010\u0010+\u001a\u00020\r2\u0006\u0010,\u001a\u00020-H\u0016J\u0010\u0010.\u001a\u00020\r2\u0006\u0010*\u001a\u00020\u0005H\u0016J\u0016\u0010/\u001a\u00020\r2\u000c\u00100\u001a\u0008\u0012\u0004\u0012\u00020201H\u0016J\u0008\u00103\u001a\u00020\rH\u0016R#\u0010\u0004\u001a\n \u0006*\u0004\u0018\u00010\u00050\u00058BX\u0082\u0084\u0002\u00a2\u0006\u000c\n\u0004\u0008\t\u0010\n\u001a\u0004\u0008\u0007\u0010\u0008R\u001a\u0010\u000b\u001a\u0008\u0012\u0004\u0012\u00020\r0\u000c8VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u000e\u0010\u000fR\u001e\u0010\u0010\u001a\u00020\u00118\u0006@\u0006X\u0087.\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u0012\u0010\u0013\"\u0004\u0008\u0014\u0010\u0015R\u001e\u0010\u0016\u001a\u00020\u00178\u0006@\u0006X\u0087.\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u0018\u0010\u0019\"\u0004\u0008\u001a\u0010\u001bR\u001a\u0010\u001c\u001a\u0008\u0012\u0004\u0012\u00020\r0\u000c8VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u001d\u0010\u000fR\u000e\u0010\u001e\u001a\u00020\u001fX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001a\u0010 \u001a\u0008\u0012\u0004\u0012\u00020!0\u000c8VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\"\u0010\u000f\u00a8\u00065"
    }
    d2 = {
        "Lco/uk/getmondo/help/HelpCategoryActivity;",
        "Lco/uk/getmondo/common/activities/BaseActivity;",
        "Lco/uk/getmondo/help/HelpCategoryPresenter$View;",
        "()V",
        "categoryId",
        "",
        "kotlin.jvm.PlatformType",
        "getCategoryId",
        "()Ljava/lang/String;",
        "categoryId$delegate",
        "Lkotlin/Lazy;",
        "helpClicked",
        "Lio/reactivex/Observable;",
        "",
        "getHelpClicked",
        "()Lio/reactivex/Observable;",
        "intercomService",
        "Lco/uk/getmondo/common/IntercomService;",
        "getIntercomService",
        "()Lco/uk/getmondo/common/IntercomService;",
        "setIntercomService",
        "(Lco/uk/getmondo/common/IntercomService;)V",
        "presenter",
        "Lco/uk/getmondo/help/HelpCategoryPresenter;",
        "getPresenter",
        "()Lco/uk/getmondo/help/HelpCategoryPresenter;",
        "setPresenter",
        "(Lco/uk/getmondo/help/HelpCategoryPresenter;)V",
        "retryClicked",
        "getRetryClicked",
        "sectionAdapter",
        "Lco/uk/getmondo/help/HelpSectionAdapter;",
        "topicClicked",
        "Lco/uk/getmondo/help/data/model/SectionItem$TopicItem;",
        "getTopicClicked",
        "onCreate",
        "savedInstanceState",
        "Landroid/os/Bundle;",
        "openChat",
        "openTopic",
        "topic",
        "Lco/uk/getmondo/api/model/help/Topic;",
        "title",
        "setLoading",
        "isEnabled",
        "",
        "setTitle",
        "showContent",
        "sectionItems",
        "",
        "Lco/uk/getmondo/help/data/model/SectionItem;",
        "showError",
        "Companion",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# static fields
.field static final synthetic a:[Lkotlin/reflect/l;

.field public static final e:Lco/uk/getmondo/help/HelpCategoryActivity$a;


# instance fields
.field public b:Lco/uk/getmondo/help/c;

.field public c:Lco/uk/getmondo/common/q;

.field private final f:Lkotlin/c;

.field private final g:Lco/uk/getmondo/help/m;

.field private h:Ljava/util/HashMap;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const/4 v0, 0x1

    new-array v1, v0, [Lkotlin/reflect/l;

    const/4 v2, 0x0

    new-instance v0, Lkotlin/d/b/w;

    const-class v3, Lco/uk/getmondo/help/HelpCategoryActivity;

    invoke-static {v3}, Lkotlin/d/b/y;->a(Ljava/lang/Class;)Lkotlin/reflect/c;

    move-result-object v3

    const-string v4, "categoryId"

    const-string v5, "getCategoryId()Ljava/lang/String;"

    invoke-direct {v0, v3, v4, v5}, Lkotlin/d/b/w;-><init>(Lkotlin/reflect/e;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v0}, Lkotlin/d/b/y;->a(Lkotlin/d/b/v;)Lkotlin/reflect/n;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/l;

    aput-object v0, v1, v2

    sput-object v1, Lco/uk/getmondo/help/HelpCategoryActivity;->a:[Lkotlin/reflect/l;

    new-instance v0, Lco/uk/getmondo/help/HelpCategoryActivity$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lco/uk/getmondo/help/HelpCategoryActivity$a;-><init>(Lkotlin/d/b/i;)V

    sput-object v0, Lco/uk/getmondo/help/HelpCategoryActivity;->e:Lco/uk/getmondo/help/HelpCategoryActivity$a;

    return-void
.end method

.method public constructor <init>()V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 21
    invoke-direct {p0}, Lco/uk/getmondo/common/activities/b;-><init>()V

    .line 26
    new-instance v0, Lco/uk/getmondo/help/HelpCategoryActivity$b;

    invoke-direct {v0, p0}, Lco/uk/getmondo/help/HelpCategoryActivity$b;-><init>(Lco/uk/getmondo/help/HelpCategoryActivity;)V

    check-cast v0, Lkotlin/d/a/a;

    invoke-static {v0}, Lkotlin/d;->a(Lkotlin/d/a/a;)Lkotlin/c;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/help/HelpCategoryActivity;->f:Lkotlin/c;

    .line 28
    new-instance v0, Lco/uk/getmondo/help/m;

    const/4 v4, 0x7

    move-object v2, v1

    move-object v3, v1

    move-object v5, v1

    invoke-direct/range {v0 .. v5}, Lco/uk/getmondo/help/m;-><init>(Ljava/util/List;Lkotlin/d/a/b;Lkotlin/d/a/a;ILkotlin/d/b/i;)V

    iput-object v0, p0, Lco/uk/getmondo/help/HelpCategoryActivity;->g:Lco/uk/getmondo/help/m;

    return-void
.end method

.method public static final synthetic a(Lco/uk/getmondo/help/HelpCategoryActivity;)Lco/uk/getmondo/help/m;
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lco/uk/getmondo/help/HelpCategoryActivity;->g:Lco/uk/getmondo/help/m;

    return-object v0
.end method

.method private final f()Ljava/lang/String;
    .locals 3

    iget-object v0, p0, Lco/uk/getmondo/help/HelpCategoryActivity;->f:Lkotlin/c;

    sget-object v1, Lco/uk/getmondo/help/HelpCategoryActivity;->a:[Lkotlin/reflect/l;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-interface {v0}, Lkotlin/c;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public a(I)Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lco/uk/getmondo/help/HelpCategoryActivity;->h:Ljava/util/HashMap;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lco/uk/getmondo/help/HelpCategoryActivity;->h:Ljava/util/HashMap;

    :cond_0
    iget-object v0, p0, Lco/uk/getmondo/help/HelpCategoryActivity;->h:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    if-nez v0, :cond_1

    invoke-virtual {p0, p1}, Landroid/support/v4/app/j;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/help/HelpCategoryActivity;->h:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    return-object v0
.end method

.method public a()Lio/reactivex/n;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Lkotlin/n;",
            ">;"
        }
    .end annotation

    .prologue
    .line 45
    sget v0, Lco/uk/getmondo/c$a;->loadingErrorView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/help/HelpCategoryActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/common/ui/LoadingErrorView;

    invoke-virtual {v0}, Lco/uk/getmondo/common/ui/LoadingErrorView;->c()Lio/reactivex/n;

    move-result-object v0

    return-object v0
.end method

.method public a(Lco/uk/getmondo/api/model/help/Topic;Ljava/lang/String;)V
    .locals 8

    .prologue
    const/4 v3, 0x0

    const-string v0, "topic"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "title"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 76
    sget-object v7, Lco/uk/getmondo/help/HelpTopicActivity;->e:Lco/uk/getmondo/help/HelpTopicActivity$a;

    move-object v6, p0

    check-cast v6, Landroid/content/Context;

    new-instance v0, Lco/uk/getmondo/help/a/a/e;

    const/4 v4, 0x4

    move-object v1, p2

    move-object v2, p1

    move-object v5, v3

    invoke-direct/range {v0 .. v5}, Lco/uk/getmondo/help/a/a/e;-><init>(Ljava/lang/String;Lco/uk/getmondo/api/model/help/Topic;Ljava/lang/String;ILkotlin/d/b/i;)V

    invoke-virtual {v7, v6, v0}, Lco/uk/getmondo/help/HelpTopicActivity$a;->a(Landroid/content/Context;Lco/uk/getmondo/help/a/a/e;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lco/uk/getmondo/help/HelpCategoryActivity;->startActivity(Landroid/content/Intent;)V

    .line 77
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 1

    .prologue
    const-string v0, "title"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 60
    invoke-virtual {p0}, Lco/uk/getmondo/help/HelpCategoryActivity;->getSupportActionBar()Landroid/support/v7/app/a;

    move-result-object v0

    if-eqz v0, :cond_0

    check-cast p1, Ljava/lang/CharSequence;

    invoke-virtual {v0, p1}, Landroid/support/v7/app/a;->a(Ljava/lang/CharSequence;)V

    .line 61
    :cond_0
    return-void
.end method

.method public a(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<+",
            "Lco/uk/getmondo/help/a/a/d;",
            ">;)V"
        }
    .end annotation

    .prologue
    const-string v0, "sectionItems"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 63
    iget-object v0, p0, Lco/uk/getmondo/help/HelpCategoryActivity;->g:Lco/uk/getmondo/help/m;

    invoke-virtual {v0, p1}, Lco/uk/getmondo/help/m;->a(Ljava/util/List;)V

    return-void
.end method

.method public a(Z)V
    .locals 2

    .prologue
    .line 71
    sget v0, Lco/uk/getmondo/c$a;->loadingErrorView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/help/HelpCategoryActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/common/ui/LoadingErrorView;

    const v1, 0x7f0a01b5

    invoke-virtual {p0, v1}, Lco/uk/getmondo/help/HelpCategoryActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lco/uk/getmondo/common/ui/LoadingErrorView;->a(ZLjava/lang/String;)V

    .line 72
    if-eqz p1, :cond_0

    sget v0, Lco/uk/getmondo/c$a;->loadingErrorView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/help/HelpCategoryActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/common/ui/LoadingErrorView;

    invoke-static {v0}, Lco/uk/getmondo/common/ae;->a(Landroid/view/View;)V

    .line 73
    :goto_0
    return-void

    .line 72
    :cond_0
    sget v0, Lco/uk/getmondo/c$a;->loadingErrorView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/help/HelpCategoryActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/common/ui/LoadingErrorView;

    invoke-static {v0}, Lco/uk/getmondo/common/ae;->b(Landroid/view/View;)V

    goto :goto_0
.end method

.method public b()Lio/reactivex/n;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Lco/uk/getmondo/help/a/a/d$c;",
            ">;"
        }
    .end annotation

    .prologue
    .line 48
    new-instance v0, Lco/uk/getmondo/help/HelpCategoryActivity$d;

    invoke-direct {v0, p0}, Lco/uk/getmondo/help/HelpCategoryActivity$d;-><init>(Lco/uk/getmondo/help/HelpCategoryActivity;)V

    check-cast v0, Lio/reactivex/p;

    invoke-static {v0}, Lio/reactivex/n;->create(Lio/reactivex/p;)Lio/reactivex/n;

    move-result-object v0

    const-string v1, "Observable.create { emit\u2026stener = null }\n        }"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 51
    return-object v0
.end method

.method public c()Lio/reactivex/n;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Lkotlin/n;",
            ">;"
        }
    .end annotation

    .prologue
    .line 54
    new-instance v0, Lco/uk/getmondo/help/HelpCategoryActivity$c;

    invoke-direct {v0, p0}, Lco/uk/getmondo/help/HelpCategoryActivity$c;-><init>(Lco/uk/getmondo/help/HelpCategoryActivity;)V

    check-cast v0, Lio/reactivex/p;

    invoke-static {v0}, Lio/reactivex/n;->create(Lio/reactivex/p;)Lio/reactivex/n;

    move-result-object v0

    const-string v1, "Observable.create { emit\u2026stener = null }\n        }"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 57
    return-object v0
.end method

.method public d()V
    .locals 7

    .prologue
    const/4 v3, 0x0

    .line 66
    sget v0, Lco/uk/getmondo/c$a;->loadingErrorView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/help/HelpCategoryActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/common/ui/LoadingErrorView;

    const v1, 0x7f0a01b2

    invoke-virtual {p0, v1}, Lco/uk/getmondo/help/HelpCategoryActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, "getString(R.string.help_category_error)"

    invoke-static {v1, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v2, 0x0

    const v4, 0x7f02014f

    const/4 v5, 0x6

    move-object v6, v3

    invoke-static/range {v0 .. v6}, Lco/uk/getmondo/common/ui/LoadingErrorView;->a(Lco/uk/getmondo/common/ui/LoadingErrorView;Ljava/lang/String;ZLjava/lang/String;IILjava/lang/Object;)V

    .line 67
    sget v0, Lco/uk/getmondo/c$a;->loadingErrorView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/help/HelpCategoryActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/common/ui/LoadingErrorView;

    invoke-static {v0}, Lco/uk/getmondo/common/ae;->a(Landroid/view/View;)V

    .line 68
    return-void
.end method

.method public e()V
    .locals 2

    .prologue
    .line 80
    iget-object v0, p0, Lco/uk/getmondo/help/HelpCategoryActivity;->c:Lco/uk/getmondo/common/q;

    if-nez v0, :cond_0

    const-string v1, "intercomService"

    invoke-static {v1}, Lkotlin/d/b/l;->b(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0}, Lco/uk/getmondo/common/q;->a()V

    .line 81
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 31
    invoke-super {p0, p1}, Lco/uk/getmondo/common/activities/b;->onCreate(Landroid/os/Bundle;)V

    .line 32
    const v0, 0x7f05003e

    invoke-virtual {p0, v0}, Lco/uk/getmondo/help/HelpCategoryActivity;->setContentView(I)V

    .line 34
    invoke-virtual {p0}, Lco/uk/getmondo/help/HelpCategoryActivity;->l()Lco/uk/getmondo/common/h/b/b;

    move-result-object v0

    new-instance v1, Lco/uk/getmondo/help/b/b;

    invoke-direct {p0}, Lco/uk/getmondo/help/HelpCategoryActivity;->f()Ljava/lang/String;

    move-result-object v2

    const-string v3, "categoryId"

    invoke-static {v2, v3}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v1, v2}, Lco/uk/getmondo/help/b/b;-><init>(Ljava/lang/String;)V

    invoke-interface {v0, v1}, Lco/uk/getmondo/common/h/b/b;->a(Lco/uk/getmondo/help/b/b;)Lco/uk/getmondo/help/b/a;

    move-result-object v0

    .line 35
    invoke-interface {v0, p0}, Lco/uk/getmondo/help/b/a;->a(Lco/uk/getmondo/help/HelpCategoryActivity;)V

    .line 37
    new-instance v1, Landroid/support/v7/widget/LinearLayoutManager;

    move-object v0, p0

    check-cast v0, Landroid/content/Context;

    invoke-direct {v1, v0}, Landroid/support/v7/widget/LinearLayoutManager;-><init>(Landroid/content/Context;)V

    .line 38
    sget v0, Lco/uk/getmondo/c$a;->categoryRecyclerView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/help/HelpCategoryActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView;

    check-cast v1, Landroid/support/v7/widget/RecyclerView$h;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(Landroid/support/v7/widget/RecyclerView$h;)V

    .line 39
    sget v0, Lco/uk/getmondo/c$a;->categoryRecyclerView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/help/HelpCategoryActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView;

    iget-object v1, p0, Lco/uk/getmondo/help/HelpCategoryActivity;->g:Lco/uk/getmondo/help/m;

    check-cast v1, Landroid/support/v7/widget/RecyclerView$a;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setAdapter(Landroid/support/v7/widget/RecyclerView$a;)V

    .line 40
    sget v0, Lco/uk/getmondo/c$a;->categoryRecyclerView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/help/HelpCategoryActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setHasFixedSize(Z)V

    .line 42
    iget-object v0, p0, Lco/uk/getmondo/help/HelpCategoryActivity;->b:Lco/uk/getmondo/help/c;

    if-nez v0, :cond_0

    const-string v1, "presenter"

    invoke-static {v1}, Lkotlin/d/b/l;->b(Ljava/lang/String;)V

    :cond_0
    check-cast p0, Lco/uk/getmondo/help/c$a;

    invoke-virtual {v0, p0}, Lco/uk/getmondo/help/c;->a(Lco/uk/getmondo/help/c$a;)V

    .line 43
    return-void
.end method
