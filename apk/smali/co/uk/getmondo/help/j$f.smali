.class final Lco/uk/getmondo/help/j$f;
.super Ljava/lang/Object;
.source "HelpSearchPresenter.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lco/uk/getmondo/help/j;->a(Lco/uk/getmondo/help/j$a;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h",
        "<TT;",
        "Lio/reactivex/r",
        "<+TR;>;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000\u0018\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0000\u0010\u0000\u001a>\u0012\u0018\u0012\u0016\u0012\u0004\u0012\u00020\u0003 \u0004*\n\u0012\u0004\u0012\u00020\u0003\u0018\u00010\u00020\u0002 \u0004*\u001e\u0012\u0018\u0012\u0016\u0012\u0004\u0012\u00020\u0003 \u0004*\n\u0012\u0004\u0012\u00020\u0003\u0018\u00010\u00020\u0002\u0018\u00010\u00010\u00012\u0006\u0010\u0005\u001a\u00020\u0006H\n\u00a2\u0006\u0002\u0008\u0007"
    }
    d2 = {
        "<anonymous>",
        "Lio/reactivex/Observable;",
        "",
        "Lco/uk/getmondo/help/data/model/SectionItem;",
        "kotlin.jvm.PlatformType",
        "it",
        "",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field final synthetic a:Lco/uk/getmondo/help/j;

.field final synthetic b:Lco/uk/getmondo/help/j$a;


# direct methods
.method constructor <init>(Lco/uk/getmondo/help/j;Lco/uk/getmondo/help/j$a;)V
    .locals 0

    iput-object p1, p0, Lco/uk/getmondo/help/j$f;->a:Lco/uk/getmondo/help/j;

    iput-object p2, p0, Lco/uk/getmondo/help/j$f;->b:Lco/uk/getmondo/help/j$a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Lio/reactivex/n;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/n",
            "<",
            "Ljava/util/List",
            "<",
            "Lco/uk/getmondo/help/a/a/d;",
            ">;>;"
        }
    .end annotation

    .prologue
    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v0, p1

    .line 39
    check-cast v0, Ljava/lang/CharSequence;

    invoke-static {v0}, Lkotlin/h/j;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 40
    invoke-static {}, Lkotlin/a/m;->a()Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lio/reactivex/n;->just(Ljava/lang/Object;)Lio/reactivex/n;

    move-result-object v0

    .line 39
    :goto_0
    return-object v0

    .line 42
    :cond_0
    iget-object v0, p0, Lco/uk/getmondo/help/j$f;->a:Lco/uk/getmondo/help/j;

    invoke-static {v0}, Lco/uk/getmondo/help/j;->a(Lco/uk/getmondo/help/j;)Lco/uk/getmondo/help/a/a;

    move-result-object v0

    check-cast p1, Ljava/lang/CharSequence;

    invoke-static {p1}, Lkotlin/h/j;->b(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lco/uk/getmondo/help/a/a;->c(Ljava/lang/String;)Lio/reactivex/v;

    move-result-object v0

    .line 43
    iget-object v1, p0, Lco/uk/getmondo/help/j$f;->a:Lco/uk/getmondo/help/j;

    invoke-static {v1}, Lco/uk/getmondo/help/j;->b(Lco/uk/getmondo/help/j;)Lio/reactivex/u;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/v;->b(Lio/reactivex/u;)Lio/reactivex/v;

    move-result-object v0

    .line 44
    iget-object v1, p0, Lco/uk/getmondo/help/j$f;->a:Lco/uk/getmondo/help/j;

    invoke-static {v1}, Lco/uk/getmondo/help/j;->c(Lco/uk/getmondo/help/j;)Lio/reactivex/u;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/v;->a(Lio/reactivex/u;)Lio/reactivex/v;

    move-result-object v1

    .line 45
    new-instance v0, Lco/uk/getmondo/help/j$f$1;

    invoke-direct {v0, p0}, Lco/uk/getmondo/help/j$f$1;-><init>(Lco/uk/getmondo/help/j$f;)V

    check-cast v0, Lio/reactivex/c/g;

    invoke-virtual {v1, v0}, Lio/reactivex/v;->b(Lio/reactivex/c/g;)Lio/reactivex/v;

    move-result-object v1

    .line 46
    new-instance v0, Lco/uk/getmondo/help/j$f$2;

    invoke-direct {v0, p0}, Lco/uk/getmondo/help/j$f$2;-><init>(Lco/uk/getmondo/help/j$f;)V

    check-cast v0, Lio/reactivex/c/b;

    invoke-virtual {v1, v0}, Lio/reactivex/v;->a(Lio/reactivex/c/b;)Lio/reactivex/v;

    move-result-object v1

    .line 47
    new-instance v0, Lco/uk/getmondo/help/j$f$3;

    invoke-direct {v0, p0}, Lco/uk/getmondo/help/j$f$3;-><init>(Lco/uk/getmondo/help/j$f;)V

    check-cast v0, Lio/reactivex/c/g;

    invoke-virtual {v1, v0}, Lio/reactivex/v;->d(Lio/reactivex/c/g;)Lio/reactivex/v;

    move-result-object v0

    .line 51
    invoke-virtual {v0}, Lio/reactivex/v;->f()Lio/reactivex/n;

    move-result-object v1

    .line 52
    invoke-static {}, Lio/reactivex/n;->empty()Lio/reactivex/n;

    move-result-object v0

    check-cast v0, Lio/reactivex/r;

    invoke-virtual {v1, v0}, Lio/reactivex/n;->onErrorResumeNext(Lio/reactivex/r;)Lio/reactivex/n;

    move-result-object v0

    goto :goto_0
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 18
    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1}, Lco/uk/getmondo/help/j$f;->a(Ljava/lang/String;)Lio/reactivex/n;

    move-result-object v0

    return-object v0
.end method
