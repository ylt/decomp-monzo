.class public final Lco/uk/getmondo/help/f;
.super Lco/uk/getmondo/common/ui/b;
.source "HelpPresenter.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lco/uk/getmondo/help/f$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lco/uk/getmondo/common/ui/b",
        "<",
        "Lco/uk/getmondo/help/f$a;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u00002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u0003\u0008\u0007\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00020\u0001:\u0001\u0010B3\u0008\u0007\u0012\u0008\u0008\u0001\u0010\u0003\u001a\u00020\u0004\u0012\u0008\u0008\u0001\u0010\u0005\u001a\u00020\u0004\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b\u00a2\u0006\u0002\u0010\u000cJ\u0010\u0010\r\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u0002H\u0016R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0011"
    }
    d2 = {
        "Lco/uk/getmondo/help/HelpPresenter;",
        "Lco/uk/getmondo/common/ui/BasePresenter;",
        "Lco/uk/getmondo/help/HelpPresenter$View;",
        "ioScheduler",
        "Lio/reactivex/Scheduler;",
        "uiScheduler",
        "helpManager",
        "Lco/uk/getmondo/help/data/HelpManager;",
        "apiErrorHandler",
        "Lco/uk/getmondo/common/errors/ApiErrorHandler;",
        "analyticsService",
        "Lco/uk/getmondo/common/AnalyticsService;",
        "(Lio/reactivex/Scheduler;Lio/reactivex/Scheduler;Lco/uk/getmondo/help/data/HelpManager;Lco/uk/getmondo/common/errors/ApiErrorHandler;Lco/uk/getmondo/common/AnalyticsService;)V",
        "register",
        "",
        "view",
        "View",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field private final c:Lio/reactivex/u;

.field private final d:Lio/reactivex/u;

.field private final e:Lco/uk/getmondo/help/a/a;

.field private final f:Lco/uk/getmondo/common/e/a;

.field private final g:Lco/uk/getmondo/common/a;


# direct methods
.method public constructor <init>(Lio/reactivex/u;Lio/reactivex/u;Lco/uk/getmondo/help/a/a;Lco/uk/getmondo/common/e/a;Lco/uk/getmondo/common/a;)V
    .locals 1

    .prologue
    const-string v0, "ioScheduler"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "uiScheduler"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "helpManager"

    invoke-static {p3, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "apiErrorHandler"

    invoke-static {p4, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "analyticsService"

    invoke-static {p5, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 30
    invoke-direct {p0}, Lco/uk/getmondo/common/ui/b;-><init>()V

    iput-object p1, p0, Lco/uk/getmondo/help/f;->c:Lio/reactivex/u;

    iput-object p2, p0, Lco/uk/getmondo/help/f;->d:Lio/reactivex/u;

    iput-object p3, p0, Lco/uk/getmondo/help/f;->e:Lco/uk/getmondo/help/a/a;

    iput-object p4, p0, Lco/uk/getmondo/help/f;->f:Lco/uk/getmondo/common/e/a;

    iput-object p5, p0, Lco/uk/getmondo/help/f;->g:Lco/uk/getmondo/common/a;

    return-void
.end method

.method public static final synthetic a(Lco/uk/getmondo/help/f;)Lco/uk/getmondo/common/e/a;
    .locals 1

    .prologue
    .line 23
    iget-object v0, p0, Lco/uk/getmondo/help/f;->f:Lco/uk/getmondo/common/e/a;

    return-object v0
.end method

.method public static final synthetic b(Lco/uk/getmondo/help/f;)Lco/uk/getmondo/help/a/a;
    .locals 1

    .prologue
    .line 23
    iget-object v0, p0, Lco/uk/getmondo/help/f;->e:Lco/uk/getmondo/help/a/a;

    return-object v0
.end method

.method public static final synthetic c(Lco/uk/getmondo/help/f;)Lco/uk/getmondo/common/a;
    .locals 1

    .prologue
    .line 23
    iget-object v0, p0, Lco/uk/getmondo/help/f;->g:Lco/uk/getmondo/common/a;

    return-object v0
.end method


# virtual methods
.method public bridge synthetic a(Lco/uk/getmondo/common/ui/f;)V
    .locals 0

    .prologue
    .line 23
    check-cast p1, Lco/uk/getmondo/help/f$a;

    invoke-virtual {p0, p1}, Lco/uk/getmondo/help/f;->a(Lco/uk/getmondo/help/f$a;)V

    return-void
.end method

.method public a(Lco/uk/getmondo/help/f$a;)V
    .locals 5

    .prologue
    const-string v0, "view"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v0, p1

    .line 33
    check-cast v0, Lco/uk/getmondo/common/ui/f;

    invoke-super {p0, v0}, Lco/uk/getmondo/common/ui/b;->a(Lco/uk/getmondo/common/ui/f;)V

    .line 35
    iget-object v3, p0, Lco/uk/getmondo/help/f;->b:Lio/reactivex/b/a;

    .line 36
    invoke-interface {p1}, Lco/uk/getmondo/help/f$a;->e()Lio/reactivex/n;

    move-result-object v4

    new-instance v0, Lco/uk/getmondo/help/f$b;

    invoke-direct {v0, p1}, Lco/uk/getmondo/help/f$b;-><init>(Lco/uk/getmondo/help/f$a;)V

    check-cast v0, Lio/reactivex/c/g;

    sget-object v1, Lco/uk/getmondo/help/f$i;->a:Lco/uk/getmondo/help/f$i;

    check-cast v1, Lkotlin/d/a/b;

    if-eqz v1, :cond_0

    new-instance v2, Lco/uk/getmondo/help/g;

    invoke-direct {v2, v1}, Lco/uk/getmondo/help/g;-><init>(Lkotlin/d/a/b;)V

    move-object v1, v2

    :cond_0
    check-cast v1, Lio/reactivex/c/g;

    invoke-virtual {v4, v0, v1}, Lio/reactivex/n;->subscribe(Lio/reactivex/c/g;Lio/reactivex/c/g;)Lio/reactivex/b/b;

    move-result-object v0

    const-string v1, "view.searchClicked\n     \u2026penSearch() }, Timber::e)"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 35
    invoke-static {v3, v0}, Lco/uk/getmondo/common/j/f;->a(Lio/reactivex/b/a;Lio/reactivex/b/b;)Lio/reactivex/b/a;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/help/f;->b:Lio/reactivex/b/a;

    .line 38
    iget-object v3, p0, Lco/uk/getmondo/help/f;->b:Lio/reactivex/b/a;

    .line 50
    iget-object v0, p0, Lco/uk/getmondo/help/f;->e:Lco/uk/getmondo/help/a/a;

    invoke-virtual {v0}, Lco/uk/getmondo/help/a/a;->b()Lio/reactivex/v;

    move-result-object v0

    .line 39
    iget-object v1, p0, Lco/uk/getmondo/help/f;->c:Lio/reactivex/u;

    invoke-virtual {v0, v1}, Lio/reactivex/v;->b(Lio/reactivex/u;)Lio/reactivex/v;

    move-result-object v0

    .line 40
    iget-object v1, p0, Lco/uk/getmondo/help/f;->d:Lio/reactivex/u;

    invoke-virtual {v0, v1}, Lio/reactivex/v;->a(Lio/reactivex/u;)Lio/reactivex/v;

    move-result-object v1

    .line 41
    new-instance v0, Lco/uk/getmondo/help/f$j;

    invoke-direct {v0, p1}, Lco/uk/getmondo/help/f$j;-><init>(Lco/uk/getmondo/help/f$a;)V

    check-cast v0, Lio/reactivex/c/g;

    invoke-virtual {v1, v0}, Lio/reactivex/v;->b(Lio/reactivex/c/g;)Lio/reactivex/v;

    move-result-object v1

    .line 45
    new-instance v0, Lco/uk/getmondo/help/f$k;

    invoke-direct {v0, p1}, Lco/uk/getmondo/help/f$k;-><init>(Lco/uk/getmondo/help/f$a;)V

    check-cast v0, Lio/reactivex/c/b;

    invoke-virtual {v1, v0}, Lio/reactivex/v;->a(Lio/reactivex/c/b;)Lio/reactivex/v;

    move-result-object v1

    .line 46
    new-instance v0, Lco/uk/getmondo/help/f$l;

    invoke-direct {v0, p0, p1}, Lco/uk/getmondo/help/f$l;-><init>(Lco/uk/getmondo/help/f;Lco/uk/getmondo/help/f$a;)V

    check-cast v0, Lio/reactivex/c/g;

    invoke-virtual {v1, v0}, Lio/reactivex/v;->d(Lio/reactivex/c/g;)Lio/reactivex/v;

    move-result-object v4

    .line 50
    new-instance v0, Lco/uk/getmondo/help/f$m;

    invoke-direct {v0, p1}, Lco/uk/getmondo/help/f$m;-><init>(Lco/uk/getmondo/help/f$a;)V

    check-cast v0, Lio/reactivex/c/g;

    sget-object v1, Lco/uk/getmondo/help/f$n;->a:Lco/uk/getmondo/help/f$n;

    check-cast v1, Lkotlin/d/a/b;

    if-eqz v1, :cond_1

    new-instance v2, Lco/uk/getmondo/help/g;

    invoke-direct {v2, v1}, Lco/uk/getmondo/help/g;-><init>(Lkotlin/d/a/b;)V

    move-object v1, v2

    :cond_1
    check-cast v1, Lio/reactivex/c/g;

    invoke-virtual {v4, v0, v1}, Lio/reactivex/v;->a(Lio/reactivex/c/g;Lio/reactivex/c/g;)Lio/reactivex/b/b;

    move-result-object v0

    const-string v1, "helpManager.loadTrending\u2026gTopics(it) }, Timber::e)"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 38
    invoke-static {v3, v0}, Lco/uk/getmondo/common/j/f;->a(Lio/reactivex/b/a;Lio/reactivex/b/b;)Lio/reactivex/b/a;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/help/f;->b:Lio/reactivex/b/a;

    .line 52
    iget-object v3, p0, Lco/uk/getmondo/help/f;->b:Lio/reactivex/b/a;

    .line 53
    invoke-interface {p1}, Lco/uk/getmondo/help/f$a;->a()Lio/reactivex/n;

    move-result-object v4

    new-instance v0, Lco/uk/getmondo/help/f$o;

    invoke-direct {v0, p1}, Lco/uk/getmondo/help/f$o;-><init>(Lco/uk/getmondo/help/f$a;)V

    check-cast v0, Lio/reactivex/c/g;

    sget-object v1, Lco/uk/getmondo/help/f$p;->a:Lco/uk/getmondo/help/f$p;

    check-cast v1, Lkotlin/d/a/b;

    if-eqz v1, :cond_2

    new-instance v2, Lco/uk/getmondo/help/g;

    invoke-direct {v2, v1}, Lco/uk/getmondo/help/g;-><init>(Lkotlin/d/a/b;)V

    move-object v1, v2

    :cond_2
    check-cast v1, Lio/reactivex/c/g;

    invoke-virtual {v4, v0, v1}, Lio/reactivex/n;->subscribe(Lio/reactivex/c/g;Lio/reactivex/c/g;)Lio/reactivex/b/b;

    move-result-object v0

    const-string v1, "view.categoryClicked\n   \u2026gory(it.id) }, Timber::e)"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 52
    invoke-static {v3, v0}, Lco/uk/getmondo/common/j/f;->a(Lio/reactivex/b/a;Lio/reactivex/b/b;)Lio/reactivex/b/a;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/help/f;->b:Lio/reactivex/b/a;

    .line 55
    iget-object v3, p0, Lco/uk/getmondo/help/f;->b:Lio/reactivex/b/a;

    .line 56
    invoke-interface {p1}, Lco/uk/getmondo/help/f$a;->b()Lio/reactivex/n;

    move-result-object v4

    new-instance v0, Lco/uk/getmondo/help/f$c;

    invoke-direct {v0, p0, p1}, Lco/uk/getmondo/help/f$c;-><init>(Lco/uk/getmondo/help/f;Lco/uk/getmondo/help/f$a;)V

    check-cast v0, Lio/reactivex/c/g;

    sget-object v1, Lco/uk/getmondo/help/f$d;->a:Lco/uk/getmondo/help/f$d;

    check-cast v1, Lkotlin/d/a/b;

    if-eqz v1, :cond_3

    new-instance v2, Lco/uk/getmondo/help/g;

    invoke-direct {v2, v1}, Lco/uk/getmondo/help/g;-><init>(Lkotlin/d/a/b;)V

    move-object v1, v2

    :cond_3
    check-cast v1, Lio/reactivex/c/g;

    invoke-virtual {v4, v0, v1}, Lio/reactivex/n;->subscribe(Lio/reactivex/c/g;Lio/reactivex/c/g;)Lio/reactivex/b/b;

    move-result-object v0

    const-string v1, "view.trendingTopicClicke\u2026ndingTitle) }, Timber::e)"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 55
    invoke-static {v3, v0}, Lco/uk/getmondo/common/j/f;->a(Lio/reactivex/b/a;Lio/reactivex/b/b;)Lio/reactivex/b/a;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/help/f;->b:Lio/reactivex/b/a;

    .line 58
    iget-object v3, p0, Lco/uk/getmondo/help/f;->b:Lio/reactivex/b/a;

    .line 59
    invoke-interface {p1}, Lco/uk/getmondo/help/f$a;->c()Lio/reactivex/n;

    move-result-object v4

    new-instance v0, Lco/uk/getmondo/help/f$e;

    invoke-direct {v0, p1}, Lco/uk/getmondo/help/f$e;-><init>(Lco/uk/getmondo/help/f$a;)V

    check-cast v0, Lio/reactivex/c/g;

    sget-object v1, Lco/uk/getmondo/help/f$f;->a:Lco/uk/getmondo/help/f$f;

    check-cast v1, Lkotlin/d/a/b;

    if-eqz v1, :cond_4

    new-instance v2, Lco/uk/getmondo/help/g;

    invoke-direct {v2, v1}, Lco/uk/getmondo/help/g;-><init>(Lkotlin/d/a/b;)V

    move-object v1, v2

    :cond_4
    check-cast v1, Lio/reactivex/c/g;

    invoke-virtual {v4, v0, v1}, Lio/reactivex/n;->subscribe(Lio/reactivex/c/g;Lio/reactivex/c/g;)Lio/reactivex/b/b;

    move-result-object v0

    const-string v1, "view.communityForumClick\u2026ity(it.url) }, Timber::e)"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 58
    invoke-static {v3, v0}, Lco/uk/getmondo/common/j/f;->a(Lio/reactivex/b/a;Lio/reactivex/b/b;)Lio/reactivex/b/a;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/help/f;->b:Lio/reactivex/b/a;

    .line 61
    iget-object v3, p0, Lco/uk/getmondo/help/f;->b:Lio/reactivex/b/a;

    .line 62
    invoke-interface {p1}, Lco/uk/getmondo/help/f$a;->d()Lio/reactivex/n;

    move-result-object v4

    new-instance v0, Lco/uk/getmondo/help/f$g;

    invoke-direct {v0, p0, p1}, Lco/uk/getmondo/help/f$g;-><init>(Lco/uk/getmondo/help/f;Lco/uk/getmondo/help/f$a;)V

    check-cast v0, Lio/reactivex/c/g;

    .line 65
    sget-object v1, Lco/uk/getmondo/help/f$h;->a:Lco/uk/getmondo/help/f$h;

    check-cast v1, Lkotlin/d/a/b;

    if-eqz v1, :cond_5

    new-instance v2, Lco/uk/getmondo/help/g;

    invoke-direct {v2, v1}, Lco/uk/getmondo/help/g;-><init>(Lkotlin/d/a/b;)V

    move-object v1, v2

    :cond_5
    check-cast v1, Lio/reactivex/c/g;

    .line 62
    invoke-virtual {v4, v0, v1}, Lio/reactivex/n;->subscribe(Lio/reactivex/c/g;Lio/reactivex/c/g;)Lio/reactivex/b/b;

    move-result-object v0

    const-string v1, "view.helpClicked\n       \u2026            }, Timber::e)"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 61
    invoke-static {v3, v0}, Lco/uk/getmondo/common/j/f;->a(Lio/reactivex/b/a;Lio/reactivex/b/b;)Lio/reactivex/b/a;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/help/f;->b:Lio/reactivex/b/a;

    .line 66
    return-void
.end method
