.class public final Lco/uk/getmondo/spending/a;
.super Landroid/support/v7/widget/RecyclerView$a;
.source "SpendingAdapter.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lco/uk/getmondo/spending/a$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/support/v7/widget/RecyclerView$a",
        "<",
        "Lco/uk/getmondo/spending/a$a;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000R\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010!\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0008\n\u0002\u0010\u0008\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010 \n\u0002\u0008\u0002\u0008\u0000\u0018\u00002\u000c\u0012\u0008\u0012\u00060\u0002R\u00020\u00000\u0001:\u0001\"B]\u0012\u001a\u0008\u0002\u0010\u0003\u001a\u0014\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\u00070\u00050\u0004\u0012:\u0008\u0002\u0010\u0008\u001a4\u0012\u0013\u0012\u00110\u0006\u00a2\u0006\u000c\u0008\n\u0012\u0008\u0008\u000b\u0012\u0004\u0008\u0008(\u000c\u0012\u0013\u0012\u00110\r\u00a2\u0006\u000c\u0008\n\u0012\u0008\u0008\u000b\u0012\u0004\u0008\u0008(\u000e\u0012\u0004\u0012\u00020\u000f\u0018\u00010\t\u00a2\u0006\u0002\u0010\u0010J\u0008\u0010\u0017\u001a\u00020\u0018H\u0016J\u001c\u0010\u0019\u001a\u00020\u000f2\n\u0010\u001a\u001a\u00060\u0002R\u00020\u00002\u0006\u0010\u001b\u001a\u00020\u0018H\u0016J\u001c\u0010\u001c\u001a\u00060\u0002R\u00020\u00002\u0006\u0010\u001d\u001a\u00020\u001e2\u0006\u0010\u001f\u001a\u00020\u0018H\u0016J \u0010 \u001a\u00020\u000f2\u0018\u0010\u0003\u001a\u0014\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\u00070\u00050!RL\u0010\u0008\u001a4\u0012\u0013\u0012\u00110\u0006\u00a2\u0006\u000c\u0008\n\u0012\u0008\u0008\u000b\u0012\u0004\u0008\u0008(\u000c\u0012\u0013\u0012\u00110\r\u00a2\u0006\u000c\u0008\n\u0012\u0008\u0008\u000b\u0012\u0004\u0008\u0008(\u000e\u0012\u0004\u0012\u00020\u000f\u0018\u00010\tX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u0011\u0010\u0012\"\u0004\u0008\u0013\u0010\u0014R#\u0010\u0003\u001a\u0014\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\u00070\u00050\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0015\u0010\u0016\u00a8\u0006#"
    }
    d2 = {
        "Lco/uk/getmondo/spending/SpendingAdapter;",
        "Landroid/support/v7/widget/RecyclerView$Adapter;",
        "Lco/uk/getmondo/spending/SpendingAdapter$ViewHolder;",
        "spendingData",
        "",
        "Landroid/support/v4/util/Pair;",
        "Lorg/threeten/bp/YearMonth;",
        "Lco/uk/getmondo/spending/data/SpendingData;",
        "clickListener",
        "Lkotlin/Function2;",
        "Lkotlin/ParameterName;",
        "name",
        "yearMonth",
        "Lco/uk/getmondo/spending/data/SpendingGroup;",
        "spendingGroup",
        "",
        "(Ljava/util/List;Lkotlin/jvm/functions/Function2;)V",
        "getClickListener",
        "()Lkotlin/jvm/functions/Function2;",
        "setClickListener",
        "(Lkotlin/jvm/functions/Function2;)V",
        "getSpendingData",
        "()Ljava/util/List;",
        "getItemCount",
        "",
        "onBindViewHolder",
        "holder",
        "position",
        "onCreateViewHolder",
        "parent",
        "Landroid/view/ViewGroup;",
        "viewType",
        "setData",
        "",
        "ViewHolder",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field private final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/support/v4/g/j",
            "<",
            "Lorg/threeten/bp/YearMonth;",
            "Lco/uk/getmondo/spending/a/g;",
            ">;>;"
        }
    .end annotation
.end field

.field private b:Lkotlin/d/a/m;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/d/a/m",
            "<-",
            "Lorg/threeten/bp/YearMonth;",
            "-",
            "Lco/uk/getmondo/spending/a/h;",
            "Lkotlin/n;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    const/4 v0, 0x3

    invoke-direct {p0, v1, v1, v0, v1}, Lco/uk/getmondo/spending/a;-><init>(Ljava/util/List;Lkotlin/d/a/m;ILkotlin/d/b/i;)V

    return-void
.end method

.method public constructor <init>(Ljava/util/List;Lkotlin/d/a/m;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/support/v4/g/j",
            "<",
            "Lorg/threeten/bp/YearMonth;",
            "Lco/uk/getmondo/spending/a/g;",
            ">;>;",
            "Lkotlin/d/a/m",
            "<-",
            "Lorg/threeten/bp/YearMonth;",
            "-",
            "Lco/uk/getmondo/spending/a/h;",
            "Lkotlin/n;",
            ">;)V"
        }
    .end annotation

    .prologue
    const-string v0, "spendingData"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 14
    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView$a;-><init>()V

    iput-object p1, p0, Lco/uk/getmondo/spending/a;->a:Ljava/util/List;

    iput-object p2, p0, Lco/uk/getmondo/spending/a;->b:Lkotlin/d/a/m;

    return-void
.end method

.method public synthetic constructor <init>(Ljava/util/List;Lkotlin/d/a/m;ILkotlin/d/b/i;)V
    .locals 1

    .prologue
    and-int/lit8 v0, p3, 0x1

    if-eqz v0, :cond_0

    .line 12
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    check-cast v0, Ljava/util/List;

    move-object p1, v0

    :cond_0
    and-int/lit8 v0, p3, 0x2

    if-eqz v0, :cond_1

    .line 13
    const/4 v0, 0x0

    check-cast v0, Lkotlin/d/a/m;

    :goto_0
    invoke-direct {p0, p1, v0}, Lco/uk/getmondo/spending/a;-><init>(Ljava/util/List;Lkotlin/d/a/m;)V

    return-void

    :cond_1
    move-object v0, p2

    goto :goto_0
.end method


# virtual methods
.method public a(Landroid/view/ViewGroup;I)Lco/uk/getmondo/spending/a$a;
    .locals 5

    .prologue
    const/4 v4, 0x0

    const-string v0, "parent"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 17
    new-instance v0, Lco/uk/getmondo/spending/MonthSpendingView;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lco/uk/getmondo/spending/MonthSpendingView;-><init>(Landroid/content/Context;)V

    .line 18
    new-instance v1, Landroid/view/ViewGroup$LayoutParams;

    const/4 v2, -0x1

    const/4 v3, -0x2

    invoke-direct {v1, v2, v3}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Lco/uk/getmondo/spending/MonthSpendingView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 19
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0113

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 20
    invoke-virtual {v0, v4, v1, v4, v1}, Lco/uk/getmondo/spending/MonthSpendingView;->setPaddingRelative(IIII)V

    .line 21
    new-instance v1, Lco/uk/getmondo/spending/a$a;

    invoke-direct {v1, p0, v0}, Lco/uk/getmondo/spending/a$a;-><init>(Lco/uk/getmondo/spending/a;Lco/uk/getmondo/spending/MonthSpendingView;)V

    return-object v1
.end method

.method public final a()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Landroid/support/v4/g/j",
            "<",
            "Lorg/threeten/bp/YearMonth;",
            "Lco/uk/getmondo/spending/a/g;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 12
    iget-object v0, p0, Lco/uk/getmondo/spending/a;->a:Ljava/util/List;

    return-object v0
.end method

.method public a(Lco/uk/getmondo/spending/a$a;I)V
    .locals 3

    .prologue
    const-string v0, "holder"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 25
    iget-object v0, p0, Lco/uk/getmondo/spending/a;->a:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/g/j;

    .line 26
    invoke-virtual {p1}, Lco/uk/getmondo/spending/a$a;->a()Lco/uk/getmondo/spending/MonthSpendingView;

    move-result-object v2

    iget-object v1, v0, Landroid/support/v4/g/j;->a:Ljava/lang/Object;

    check-cast v1, Lorg/threeten/bp/YearMonth;

    iget-object v0, v0, Landroid/support/v4/g/j;->b:Ljava/lang/Object;

    check-cast v0, Lco/uk/getmondo/spending/a/g;

    invoke-virtual {v2, v1, v0}, Lco/uk/getmondo/spending/MonthSpendingView;->a(Lorg/threeten/bp/YearMonth;Lco/uk/getmondo/spending/a/g;)V

    .line 27
    return-void
.end method

.method public final a(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<+",
            "Landroid/support/v4/g/j",
            "<",
            "Lorg/threeten/bp/YearMonth;",
            "Lco/uk/getmondo/spending/a/g;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    const-string v0, "spendingData"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 32
    iget-object v0, p0, Lco/uk/getmondo/spending/a;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 33
    iget-object v0, p0, Lco/uk/getmondo/spending/a;->a:Ljava/util/List;

    check-cast p1, Ljava/util/Collection;

    invoke-interface {v0, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 34
    invoke-virtual {p0}, Lco/uk/getmondo/spending/a;->notifyDataSetChanged()V

    .line 35
    return-void
.end method

.method public final a(Lkotlin/d/a/m;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/d/a/m",
            "<-",
            "Lorg/threeten/bp/YearMonth;",
            "-",
            "Lco/uk/getmondo/spending/a/h;",
            "Lkotlin/n;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 13
    iput-object p1, p0, Lco/uk/getmondo/spending/a;->b:Lkotlin/d/a/m;

    return-void
.end method

.method public final b()Lkotlin/d/a/m;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/d/a/m",
            "<",
            "Lorg/threeten/bp/YearMonth;",
            "Lco/uk/getmondo/spending/a/h;",
            "Lkotlin/n;",
            ">;"
        }
    .end annotation

    .prologue
    .line 13
    iget-object v0, p0, Lco/uk/getmondo/spending/a;->b:Lkotlin/d/a/m;

    return-object v0
.end method

.method public getItemCount()I
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, Lco/uk/getmondo/spending/a;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public synthetic onBindViewHolder(Landroid/support/v7/widget/RecyclerView$w;I)V
    .locals 0

    .prologue
    .line 11
    check-cast p1, Lco/uk/getmondo/spending/a$a;

    invoke-virtual {p0, p1, p2}, Lco/uk/getmondo/spending/a;->a(Lco/uk/getmondo/spending/a$a;I)V

    return-void
.end method

.method public synthetic onCreateViewHolder(Landroid/view/ViewGroup;I)Landroid/support/v7/widget/RecyclerView$w;
    .locals 1

    .prologue
    .line 11
    invoke-virtual {p0, p1, p2}, Lco/uk/getmondo/spending/a;->a(Landroid/view/ViewGroup;I)Lco/uk/getmondo/spending/a$a;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView$w;

    return-object v0
.end method
