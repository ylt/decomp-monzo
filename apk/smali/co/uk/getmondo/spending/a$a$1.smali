.class final Lco/uk/getmondo/spending/a$a$1;
.super Ljava/lang/Object;
.source "SpendingAdapter.kt"

# interfaces
.implements Lco/uk/getmondo/spending/MonthSpendingView$a;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lco/uk/getmondo/spending/a$a;-><init>(Lco/uk/getmondo/spending/a;Lco/uk/getmondo/spending/MonthSpendingView;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000\u0016\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u00032\u000e\u0010\u0005\u001a\n \u0004*\u0004\u0018\u00010\u00060\u0006H\n\u00a2\u0006\u0002\u0008\u0007"
    }
    d2 = {
        "<anonymous>",
        "",
        "yearMonth",
        "Lorg/threeten/bp/YearMonth;",
        "kotlin.jvm.PlatformType",
        "spendingGroup",
        "Lco/uk/getmondo/spending/data/SpendingGroup;",
        "onItemClicked"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field final synthetic a:Lco/uk/getmondo/spending/a$a;


# direct methods
.method constructor <init>(Lco/uk/getmondo/spending/a$a;)V
    .locals 0

    iput-object p1, p0, Lco/uk/getmondo/spending/a$a$1;->a:Lco/uk/getmondo/spending/a$a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lorg/threeten/bp/YearMonth;Lco/uk/getmondo/spending/a/h;)V
    .locals 2

    .prologue
    .line 41
    iget-object v0, p0, Lco/uk/getmondo/spending/a$a$1;->a:Lco/uk/getmondo/spending/a$a;

    iget-object v0, v0, Lco/uk/getmondo/spending/a$a;->a:Lco/uk/getmondo/spending/a;

    invoke-virtual {v0}, Lco/uk/getmondo/spending/a;->b()Lkotlin/d/a/m;

    move-result-object v0

    if-eqz v0, :cond_0

    const-string v1, "yearMonth"

    invoke-static {p1, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "spendingGroup"

    invoke-static {p2, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v0, p1, p2}, Lkotlin/d/a/m;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/n;

    .line 42
    :cond_0
    return-void
.end method
