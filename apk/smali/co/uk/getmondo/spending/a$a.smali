.class public final Lco/uk/getmondo/spending/a$a;
.super Landroid/support/v7/widget/RecyclerView$w;
.source "SpendingAdapter.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lco/uk/getmondo/spending/a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x11
    name = "a"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0006\u0008\u0080\u0004\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004R\u001a\u0010\u0002\u001a\u00020\u0003X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u0005\u0010\u0006\"\u0004\u0008\u0007\u0010\u0008\u00a8\u0006\t"
    }
    d2 = {
        "Lco/uk/getmondo/spending/SpendingAdapter$ViewHolder;",
        "Landroid/support/v7/widget/RecyclerView$ViewHolder;",
        "monthSpendingView",
        "Lco/uk/getmondo/spending/MonthSpendingView;",
        "(Lco/uk/getmondo/spending/SpendingAdapter;Lco/uk/getmondo/spending/MonthSpendingView;)V",
        "getMonthSpendingView",
        "()Lco/uk/getmondo/spending/MonthSpendingView;",
        "setMonthSpendingView",
        "(Lco/uk/getmondo/spending/MonthSpendingView;)V",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field final synthetic a:Lco/uk/getmondo/spending/a;

.field private b:Lco/uk/getmondo/spending/MonthSpendingView;


# direct methods
.method public constructor <init>(Lco/uk/getmondo/spending/a;Lco/uk/getmondo/spending/MonthSpendingView;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lco/uk/getmondo/spending/MonthSpendingView;",
            ")V"
        }
    .end annotation

    .prologue
    const-string v0, "monthSpendingView"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 37
    iput-object p1, p0, Lco/uk/getmondo/spending/a$a;->a:Lco/uk/getmondo/spending/a;

    move-object v0, p2

    check-cast v0, Landroid/view/View;

    invoke-direct {p0, v0}, Landroid/support/v7/widget/RecyclerView$w;-><init>(Landroid/view/View;)V

    iput-object p2, p0, Lco/uk/getmondo/spending/a$a;->b:Lco/uk/getmondo/spending/MonthSpendingView;

    .line 40
    iget-object v1, p0, Lco/uk/getmondo/spending/a$a;->b:Lco/uk/getmondo/spending/MonthSpendingView;

    new-instance v0, Lco/uk/getmondo/spending/a$a$1;

    invoke-direct {v0, p0}, Lco/uk/getmondo/spending/a$a$1;-><init>(Lco/uk/getmondo/spending/a$a;)V

    check-cast v0, Lco/uk/getmondo/spending/MonthSpendingView$a;

    invoke-virtual {v1, v0}, Lco/uk/getmondo/spending/MonthSpendingView;->setItemClickListener(Lco/uk/getmondo/spending/MonthSpendingView$a;)V

    return-void
.end method


# virtual methods
.method public final a()Lco/uk/getmondo/spending/MonthSpendingView;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lco/uk/getmondo/spending/a$a;->b:Lco/uk/getmondo/spending/MonthSpendingView;

    return-object v0
.end method
