.class Lco/uk/getmondo/spending/MonthSpendingView$ViewHolder;
.super Ljava/lang/Object;
.source "MonthSpendingView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lco/uk/getmondo/spending/MonthSpendingView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "ViewHolder"
.end annotation


# instance fields
.field final synthetic a:Lco/uk/getmondo/spending/MonthSpendingView;

.field amountView:Lco/uk/getmondo/common/ui/AmountView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f1101db
    .end annotation
.end field

.field private b:Lco/uk/getmondo/spending/a/h;

.field cardView:Landroid/support/v7/widget/CardView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f110431
    .end annotation
.end field

.field logoImageView:Landroid/widget/ImageView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f110432
    .end annotation
.end field

.field subtitleTextView:Landroid/widget/TextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f110433
    .end annotation
.end field

.field titleTextView:Landroid/widget/TextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f1101e2
    .end annotation
.end field


# direct methods
.method constructor <init>(Lco/uk/getmondo/spending/MonthSpendingView;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 129
    iput-object p1, p0, Lco/uk/getmondo/spending/MonthSpendingView$ViewHolder;->a:Lco/uk/getmondo/spending/MonthSpendingView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 130
    invoke-static {p0, p2}, Lbutterknife/ButterKnife;->bind(Ljava/lang/Object;Landroid/view/View;)Lbutterknife/Unbinder;

    .line 131
    return-void
.end method

.method private a(Lco/uk/getmondo/spending/a/h$a;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 153
    iget-object v0, p0, Lco/uk/getmondo/spending/MonthSpendingView$ViewHolder;->titleTextView:Landroid/widget/TextView;

    invoke-virtual {p1}, Lco/uk/getmondo/spending/a/h$a;->b()Lco/uk/getmondo/d/h;

    move-result-object v1

    invoke-virtual {v1}, Lco/uk/getmondo/d/h;->a()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 154
    iget-object v0, p0, Lco/uk/getmondo/spending/MonthSpendingView$ViewHolder;->logoImageView:Landroid/widget/ImageView;

    invoke-virtual {p1}, Lco/uk/getmondo/spending/a/h$a;->b()Lco/uk/getmondo/d/h;

    move-result-object v1

    invoke-virtual {v1}, Lco/uk/getmondo/d/h;->d()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 155
    iget-object v0, p0, Lco/uk/getmondo/spending/MonthSpendingView$ViewHolder;->logoImageView:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setClipToOutline(Z)V

    .line 156
    iget-object v0, p0, Lco/uk/getmondo/spending/MonthSpendingView$ViewHolder;->logoImageView:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 157
    return-void
.end method

.method private a(Lco/uk/getmondo/spending/a/h$b;)V
    .locals 2

    .prologue
    .line 160
    iget-object v0, p0, Lco/uk/getmondo/spending/MonthSpendingView$ViewHolder;->titleTextView:Landroid/widget/TextView;

    invoke-virtual {p1}, Lco/uk/getmondo/spending/a/h$b;->b()Lco/uk/getmondo/d/u;

    move-result-object v1

    invoke-virtual {v1}, Lco/uk/getmondo/d/u;->i()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 161
    iget-object v0, p0, Lco/uk/getmondo/spending/MonthSpendingView$ViewHolder;->logoImageView:Landroid/widget/ImageView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setClipToOutline(Z)V

    .line 162
    iget-object v0, p0, Lco/uk/getmondo/spending/MonthSpendingView$ViewHolder;->logoImageView:Landroid/widget/ImageView;

    const v1, 0x7f0201f8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 163
    invoke-virtual {p1}, Lco/uk/getmondo/spending/a/h$b;->b()Lco/uk/getmondo/d/u;

    move-result-object v0

    invoke-virtual {v0}, Lco/uk/getmondo/d/u;->j()Ljava/lang/String;

    move-result-object v0

    .line 164
    invoke-static {v0}, Lco/uk/getmondo/common/k/p;->d(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 165
    iget-object v0, p0, Lco/uk/getmondo/spending/MonthSpendingView$ViewHolder;->logoImageView:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/bumptech/glide/g;->b(Landroid/content/Context;)Lcom/bumptech/glide/j;

    move-result-object v0

    .line 166
    invoke-virtual {p1}, Lco/uk/getmondo/spending/a/h$b;->b()Lco/uk/getmondo/d/u;

    move-result-object v1

    invoke-virtual {v1}, Lco/uk/getmondo/d/u;->j()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/bumptech/glide/j;->a(Ljava/lang/String;)Lcom/bumptech/glide/d;

    move-result-object v0

    .line 167
    invoke-virtual {v0}, Lcom/bumptech/glide/d;->a()Lcom/bumptech/glide/c;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/spending/MonthSpendingView$ViewHolder;->logoImageView:Landroid/widget/ImageView;

    .line 168
    invoke-virtual {v0, v1}, Lcom/bumptech/glide/c;->a(Landroid/widget/ImageView;)Lcom/bumptech/glide/g/b/j;

    .line 172
    :goto_0
    return-void

    .line 170
    :cond_0
    iget-object v0, p0, Lco/uk/getmondo/spending/MonthSpendingView$ViewHolder;->logoImageView:Landroid/widget/ImageView;

    invoke-virtual {p1}, Lco/uk/getmondo/spending/a/h$b;->b()Lco/uk/getmondo/d/u;

    move-result-object v1

    invoke-virtual {v1}, Lco/uk/getmondo/d/u;->b()Lco/uk/getmondo/d/h;

    move-result-object v1

    invoke-virtual {v1}, Lco/uk/getmondo/d/h;->c()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0
.end method

.method private a(Lco/uk/getmondo/spending/a/h$c;)V
    .locals 3

    .prologue
    .line 175
    invoke-virtual {p1}, Lco/uk/getmondo/spending/a/h$c;->b()Lco/uk/getmondo/d/aa;

    move-result-object v0

    .line 176
    iget-object v1, p0, Lco/uk/getmondo/spending/MonthSpendingView$ViewHolder;->titleTextView:Landroid/widget/TextView;

    invoke-virtual {v0}, Lco/uk/getmondo/d/aa;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 177
    invoke-virtual {v0}, Lco/uk/getmondo/d/aa;->f()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 178
    iget-object v1, p0, Lco/uk/getmondo/spending/MonthSpendingView$ViewHolder;->logoImageView:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/bumptech/glide/g;->b(Landroid/content/Context;)Lcom/bumptech/glide/j;

    move-result-object v1

    .line 179
    invoke-virtual {v0}, Lco/uk/getmondo/d/aa;->g()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/bumptech/glide/j;->a(Ljava/lang/String;)Lcom/bumptech/glide/d;

    move-result-object v0

    .line 180
    invoke-virtual {v0}, Lcom/bumptech/glide/d;->h()Lcom/bumptech/glide/b;

    move-result-object v0

    .line 181
    invoke-virtual {v0}, Lcom/bumptech/glide/b;->a()Lcom/bumptech/glide/a;

    move-result-object v0

    new-instance v1, Lco/uk/getmondo/common/ui/c;

    iget-object v2, p0, Lco/uk/getmondo/spending/MonthSpendingView$ViewHolder;->logoImageView:Landroid/widget/ImageView;

    invoke-direct {v1, v2}, Lco/uk/getmondo/common/ui/c;-><init>(Landroid/widget/ImageView;)V

    .line 182
    invoke-virtual {v0, v1}, Lcom/bumptech/glide/a;->a(Lcom/bumptech/glide/g/b/j;)Lcom/bumptech/glide/g/b/j;

    .line 186
    :goto_0
    return-void

    .line 184
    :cond_0
    iget-object v1, p0, Lco/uk/getmondo/spending/MonthSpendingView$ViewHolder;->logoImageView:Landroid/widget/ImageView;

    iget-object v2, p0, Lco/uk/getmondo/spending/MonthSpendingView$ViewHolder;->a:Lco/uk/getmondo/spending/MonthSpendingView;

    invoke-static {v2}, Lco/uk/getmondo/spending/MonthSpendingView;->b(Lco/uk/getmondo/spending/MonthSpendingView;)Lco/uk/getmondo/common/ui/a;

    move-result-object v2

    invoke-virtual {v0}, Lco/uk/getmondo/d/aa;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lco/uk/getmondo/common/ui/a;->a(Ljava/lang/String;)Lco/uk/getmondo/common/ui/a$b;

    move-result-object v0

    iget-object v2, p0, Lco/uk/getmondo/spending/MonthSpendingView$ViewHolder;->a:Lco/uk/getmondo/spending/MonthSpendingView;

    invoke-static {v2}, Lco/uk/getmondo/spending/MonthSpendingView;->a(Lco/uk/getmondo/spending/MonthSpendingView;)I

    move-result v2

    invoke-virtual {v0, v2}, Lco/uk/getmondo/common/ui/a$b;->a(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0
.end method


# virtual methods
.method a(Lco/uk/getmondo/spending/a/h;)V
    .locals 7

    .prologue
    .line 134
    iput-object p1, p0, Lco/uk/getmondo/spending/MonthSpendingView$ViewHolder;->b:Lco/uk/getmondo/spending/a/h;

    .line 137
    invoke-virtual {p1}, Lco/uk/getmondo/spending/a/h;->a()Lco/uk/getmondo/spending/a/g;

    move-result-object v0

    invoke-virtual {v0}, Lco/uk/getmondo/spending/a/g;->a()Lco/uk/getmondo/d/c;

    move-result-object v0

    .line 138
    iget-object v1, p0, Lco/uk/getmondo/spending/MonthSpendingView$ViewHolder;->amountView:Lco/uk/getmondo/common/ui/AmountView;

    invoke-virtual {v0}, Lco/uk/getmondo/d/c;->e()D

    move-result-wide v2

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    cmpl-double v2, v2, v4

    if-ltz v2, :cond_1

    invoke-virtual {v0}, Lco/uk/getmondo/d/c;->h()Lco/uk/getmondo/d/c;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v0}, Lco/uk/getmondo/common/ui/AmountView;->setAmount(Lco/uk/getmondo/d/c;)V

    .line 140
    invoke-virtual {p1}, Lco/uk/getmondo/spending/a/h;->a()Lco/uk/getmondo/spending/a/g;

    move-result-object v0

    invoke-virtual {v0}, Lco/uk/getmondo/spending/a/g;->b()I

    move-result v0

    .line 141
    iget-object v1, p0, Lco/uk/getmondo/spending/MonthSpendingView$ViewHolder;->subtitleTextView:Landroid/widget/TextView;

    iget-object v2, p0, Lco/uk/getmondo/spending/MonthSpendingView$ViewHolder;->a:Lco/uk/getmondo/spending/MonthSpendingView;

    invoke-virtual {v2}, Lco/uk/getmondo/spending/MonthSpendingView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f120004

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-virtual {v2, v3, v0, v4}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 143
    instance-of v0, p1, Lco/uk/getmondo/spending/a/h$a;

    if-eqz v0, :cond_2

    .line 144
    check-cast p1, Lco/uk/getmondo/spending/a/h$a;

    invoke-direct {p0, p1}, Lco/uk/getmondo/spending/MonthSpendingView$ViewHolder;->a(Lco/uk/getmondo/spending/a/h$a;)V

    .line 150
    :cond_0
    :goto_1
    return-void

    .line 138
    :cond_1
    invoke-virtual {v0}, Lco/uk/getmondo/d/c;->i()Lco/uk/getmondo/d/c;

    move-result-object v0

    goto :goto_0

    .line 145
    :cond_2
    instance-of v0, p1, Lco/uk/getmondo/spending/a/h$b;

    if-eqz v0, :cond_3

    .line 146
    check-cast p1, Lco/uk/getmondo/spending/a/h$b;

    invoke-direct {p0, p1}, Lco/uk/getmondo/spending/MonthSpendingView$ViewHolder;->a(Lco/uk/getmondo/spending/a/h$b;)V

    goto :goto_1

    .line 147
    :cond_3
    instance-of v0, p1, Lco/uk/getmondo/spending/a/h$c;

    if-eqz v0, :cond_0

    .line 148
    check-cast p1, Lco/uk/getmondo/spending/a/h$c;

    invoke-direct {p0, p1}, Lco/uk/getmondo/spending/MonthSpendingView$ViewHolder;->a(Lco/uk/getmondo/spending/a/h$c;)V

    goto :goto_1
.end method

.method onCardClicked()V
    .locals 3
    .annotation build Lbutterknife/OnClick;
        value = {
            0x7f110431
        }
    .end annotation

    .prologue
    .line 190
    iget-object v0, p0, Lco/uk/getmondo/spending/MonthSpendingView$ViewHolder;->a:Lco/uk/getmondo/spending/MonthSpendingView;

    invoke-static {v0}, Lco/uk/getmondo/spending/MonthSpendingView;->c(Lco/uk/getmondo/spending/MonthSpendingView;)Lco/uk/getmondo/spending/MonthSpendingView$a;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 191
    iget-object v0, p0, Lco/uk/getmondo/spending/MonthSpendingView$ViewHolder;->a:Lco/uk/getmondo/spending/MonthSpendingView;

    invoke-static {v0}, Lco/uk/getmondo/spending/MonthSpendingView;->c(Lco/uk/getmondo/spending/MonthSpendingView;)Lco/uk/getmondo/spending/MonthSpendingView$a;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/spending/MonthSpendingView$ViewHolder;->a:Lco/uk/getmondo/spending/MonthSpendingView;

    invoke-static {v1}, Lco/uk/getmondo/spending/MonthSpendingView;->d(Lco/uk/getmondo/spending/MonthSpendingView;)Lorg/threeten/bp/YearMonth;

    move-result-object v1

    iget-object v2, p0, Lco/uk/getmondo/spending/MonthSpendingView$ViewHolder;->b:Lco/uk/getmondo/spending/a/h;

    invoke-interface {v0, v1, v2}, Lco/uk/getmondo/spending/MonthSpendingView$a;->a(Lorg/threeten/bp/YearMonth;Lco/uk/getmondo/spending/a/h;)V

    .line 193
    :cond_0
    return-void
.end method
