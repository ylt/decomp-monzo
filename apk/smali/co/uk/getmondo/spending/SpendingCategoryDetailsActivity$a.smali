.class Lco/uk/getmondo/spending/SpendingCategoryDetailsActivity$a;
.super Landroid/support/v4/app/r;
.source "SpendingCategoryDetailsActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lco/uk/getmondo/spending/SpendingCategoryDetailsActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "a"
.end annotation


# instance fields
.field final synthetic a:Lco/uk/getmondo/spending/SpendingCategoryDetailsActivity;


# direct methods
.method public constructor <init>(Lco/uk/getmondo/spending/SpendingCategoryDetailsActivity;Landroid/support/v4/app/n;)V
    .locals 0

    .prologue
    .line 75
    iput-object p1, p0, Lco/uk/getmondo/spending/SpendingCategoryDetailsActivity$a;->a:Lco/uk/getmondo/spending/SpendingCategoryDetailsActivity;

    .line 76
    invoke-direct {p0, p2}, Landroid/support/v4/app/r;-><init>(Landroid/support/v4/app/n;)V

    .line 77
    return-void
.end method


# virtual methods
.method public a(I)Landroid/support/v4/app/Fragment;
    .locals 2

    .prologue
    .line 81
    packed-switch p1, :pswitch_data_0

    .line 87
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Invalid position for spending pager adapter"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 83
    :pswitch_0
    iget-object v0, p0, Lco/uk/getmondo/spending/SpendingCategoryDetailsActivity$a;->a:Lco/uk/getmondo/spending/SpendingCategoryDetailsActivity;

    invoke-static {v0}, Lco/uk/getmondo/spending/SpendingCategoryDetailsActivity;->a(Lco/uk/getmondo/spending/SpendingCategoryDetailsActivity;)Lorg/threeten/bp/YearMonth;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/spending/SpendingCategoryDetailsActivity$a;->a:Lco/uk/getmondo/spending/SpendingCategoryDetailsActivity;

    invoke-static {v1}, Lco/uk/getmondo/spending/SpendingCategoryDetailsActivity;->b(Lco/uk/getmondo/spending/SpendingCategoryDetailsActivity;)Lco/uk/getmondo/d/h;

    move-result-object v1

    invoke-static {v0, v1}, Lco/uk/getmondo/spending/transactions/SpendingTransactionsFragment;->a(Lorg/threeten/bp/YearMonth;Lco/uk/getmondo/d/h;)Lco/uk/getmondo/spending/transactions/SpendingTransactionsFragment;

    move-result-object v0

    .line 85
    :goto_0
    return-object v0

    :pswitch_1
    iget-object v0, p0, Lco/uk/getmondo/spending/SpendingCategoryDetailsActivity$a;->a:Lco/uk/getmondo/spending/SpendingCategoryDetailsActivity;

    invoke-static {v0}, Lco/uk/getmondo/spending/SpendingCategoryDetailsActivity;->a(Lco/uk/getmondo/spending/SpendingCategoryDetailsActivity;)Lorg/threeten/bp/YearMonth;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/spending/SpendingCategoryDetailsActivity$a;->a:Lco/uk/getmondo/spending/SpendingCategoryDetailsActivity;

    invoke-static {v1}, Lco/uk/getmondo/spending/SpendingCategoryDetailsActivity;->b(Lco/uk/getmondo/spending/SpendingCategoryDetailsActivity;)Lco/uk/getmondo/d/h;

    move-result-object v1

    invoke-static {v0, v1}, Lco/uk/getmondo/spending/merchant/SpendingByMerchantFragment;->a(Lorg/threeten/bp/YearMonth;Lco/uk/getmondo/d/h;)Lco/uk/getmondo/spending/merchant/SpendingByMerchantFragment;

    move-result-object v0

    goto :goto_0

    .line 81
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public b()I
    .locals 1

    .prologue
    .line 93
    const/4 v0, 0x2

    return v0
.end method

.method public c(I)Ljava/lang/CharSequence;
    .locals 2

    .prologue
    .line 98
    packed-switch p1, :pswitch_data_0

    .line 104
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Invalid position for spending pager adapter"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 100
    :pswitch_0
    iget-object v0, p0, Lco/uk/getmondo/spending/SpendingCategoryDetailsActivity$a;->a:Lco/uk/getmondo/spending/SpendingCategoryDetailsActivity;

    const v1, 0x7f0a03b4

    invoke-virtual {v0, v1}, Lco/uk/getmondo/spending/SpendingCategoryDetailsActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 102
    :goto_0
    return-object v0

    :pswitch_1
    iget-object v0, p0, Lco/uk/getmondo/spending/SpendingCategoryDetailsActivity$a;->a:Lco/uk/getmondo/spending/SpendingCategoryDetailsActivity;

    const v1, 0x7f0a03b3

    invoke-virtual {v0, v1}, Lco/uk/getmondo/spending/SpendingCategoryDetailsActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 98
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
