.class public Lco/uk/getmondo/spending/MonthSpendingView$ViewHolder_ViewBinding;
.super Ljava/lang/Object;
.source "MonthSpendingView$ViewHolder_ViewBinding.java"

# interfaces
.implements Lbutterknife/Unbinder;


# instance fields
.field private a:Lco/uk/getmondo/spending/MonthSpendingView$ViewHolder;

.field private b:Landroid/view/View;


# direct methods
.method public constructor <init>(Lco/uk/getmondo/spending/MonthSpendingView$ViewHolder;Landroid/view/View;)V
    .locals 4

    .prologue
    const v3, 0x7f110431

    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    iput-object p1, p0, Lco/uk/getmondo/spending/MonthSpendingView$ViewHolder_ViewBinding;->a:Lco/uk/getmondo/spending/MonthSpendingView$ViewHolder;

    .line 28
    const-string v0, "field \'cardView\' and method \'onCardClicked\'"

    invoke-static {p2, v3, v0}, Lbutterknife/internal/Utils;->findRequiredView(Landroid/view/View;ILjava/lang/String;)Landroid/view/View;

    move-result-object v1

    .line 29
    const-string v0, "field \'cardView\'"

    const-class v2, Landroid/support/v7/widget/CardView;

    invoke-static {v1, v3, v0, v2}, Lbutterknife/internal/Utils;->castView(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/CardView;

    iput-object v0, p1, Lco/uk/getmondo/spending/MonthSpendingView$ViewHolder;->cardView:Landroid/support/v7/widget/CardView;

    .line 30
    iput-object v1, p0, Lco/uk/getmondo/spending/MonthSpendingView$ViewHolder_ViewBinding;->b:Landroid/view/View;

    .line 31
    new-instance v0, Lco/uk/getmondo/spending/MonthSpendingView$ViewHolder_ViewBinding$1;

    invoke-direct {v0, p0, p1}, Lco/uk/getmondo/spending/MonthSpendingView$ViewHolder_ViewBinding$1;-><init>(Lco/uk/getmondo/spending/MonthSpendingView$ViewHolder_ViewBinding;Lco/uk/getmondo/spending/MonthSpendingView$ViewHolder;)V

    invoke-virtual {v1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 37
    const v0, 0x7f110432

    const-string v1, "field \'logoImageView\'"

    const-class v2, Landroid/widget/ImageView;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p1, Lco/uk/getmondo/spending/MonthSpendingView$ViewHolder;->logoImageView:Landroid/widget/ImageView;

    .line 38
    const v0, 0x7f1101e2

    const-string v1, "field \'titleTextView\'"

    const-class v2, Landroid/widget/TextView;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p1, Lco/uk/getmondo/spending/MonthSpendingView$ViewHolder;->titleTextView:Landroid/widget/TextView;

    .line 39
    const v0, 0x7f110433

    const-string v1, "field \'subtitleTextView\'"

    const-class v2, Landroid/widget/TextView;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p1, Lco/uk/getmondo/spending/MonthSpendingView$ViewHolder;->subtitleTextView:Landroid/widget/TextView;

    .line 40
    const v0, 0x7f1101db

    const-string v1, "field \'amountView\'"

    const-class v2, Lco/uk/getmondo/common/ui/AmountView;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/common/ui/AmountView;

    iput-object v0, p1, Lco/uk/getmondo/spending/MonthSpendingView$ViewHolder;->amountView:Lco/uk/getmondo/common/ui/AmountView;

    .line 41
    return-void
.end method


# virtual methods
.method public unbind()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 46
    iget-object v0, p0, Lco/uk/getmondo/spending/MonthSpendingView$ViewHolder_ViewBinding;->a:Lco/uk/getmondo/spending/MonthSpendingView$ViewHolder;

    .line 47
    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Bindings already cleared."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 48
    :cond_0
    iput-object v1, p0, Lco/uk/getmondo/spending/MonthSpendingView$ViewHolder_ViewBinding;->a:Lco/uk/getmondo/spending/MonthSpendingView$ViewHolder;

    .line 50
    iput-object v1, v0, Lco/uk/getmondo/spending/MonthSpendingView$ViewHolder;->cardView:Landroid/support/v7/widget/CardView;

    .line 51
    iput-object v1, v0, Lco/uk/getmondo/spending/MonthSpendingView$ViewHolder;->logoImageView:Landroid/widget/ImageView;

    .line 52
    iput-object v1, v0, Lco/uk/getmondo/spending/MonthSpendingView$ViewHolder;->titleTextView:Landroid/widget/TextView;

    .line 53
    iput-object v1, v0, Lco/uk/getmondo/spending/MonthSpendingView$ViewHolder;->subtitleTextView:Landroid/widget/TextView;

    .line 54
    iput-object v1, v0, Lco/uk/getmondo/spending/MonthSpendingView$ViewHolder;->amountView:Lco/uk/getmondo/common/ui/AmountView;

    .line 56
    iget-object v0, p0, Lco/uk/getmondo/spending/MonthSpendingView$ViewHolder_ViewBinding;->b:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 57
    iput-object v1, p0, Lco/uk/getmondo/spending/MonthSpendingView$ViewHolder_ViewBinding;->b:Landroid/view/View;

    .line 58
    return-void
.end method
