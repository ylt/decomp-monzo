.class public Lco/uk/getmondo/spending/SpendingCategoryDetailsActivity;
.super Lco/uk/getmondo/common/activities/b;
.source "SpendingCategoryDetailsActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lco/uk/getmondo/spending/SpendingCategoryDetailsActivity$a;
    }
.end annotation


# instance fields
.field private a:Lorg/threeten/bp/YearMonth;

.field appBarLayout:Landroid/support/design/widget/AppBarLayout;
    .annotation build Lbutterknife/BindView;
        value = 0x7f110228
    .end annotation
.end field

.field private b:Lco/uk/getmondo/d/h;

.field tabLayout:Landroid/support/design/widget/TabLayout;
    .annotation build Lbutterknife/BindView;
        value = 0x7f110229
    .end annotation
.end field

.field toolbar:Landroid/support/v7/widget/Toolbar;
    .annotation build Lbutterknife/BindView;
        value = 0x7f1100fe
    .end annotation
.end field

.field viewPager:Landroid/support/v4/view/ViewPager;
    .annotation build Lbutterknife/BindView;
        value = 0x7f110227
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Lco/uk/getmondo/common/activities/b;-><init>()V

    return-void
.end method

.method static synthetic a(Lco/uk/getmondo/spending/SpendingCategoryDetailsActivity;)Lorg/threeten/bp/YearMonth;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lco/uk/getmondo/spending/SpendingCategoryDetailsActivity;->a:Lorg/threeten/bp/YearMonth;

    return-object v0
.end method

.method public static a(Landroid/content/Context;Lorg/threeten/bp/YearMonth;Lco/uk/getmondo/d/h;)V
    .locals 1

    .prologue
    .line 43
    invoke-static {p0, p1, p2}, Lco/uk/getmondo/spending/SpendingCategoryDetailsActivity;->b(Landroid/content/Context;Lorg/threeten/bp/YearMonth;Lco/uk/getmondo/d/h;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 44
    return-void
.end method

.method public static b(Landroid/content/Context;Lorg/threeten/bp/YearMonth;Lco/uk/getmondo/d/h;)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 47
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lco/uk/getmondo/spending/SpendingCategoryDetailsActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 48
    const-string v1, "year_month"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 49
    const-string v1, "category"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 50
    return-object v0
.end method

.method static synthetic b(Lco/uk/getmondo/spending/SpendingCategoryDetailsActivity;)Lco/uk/getmondo/d/h;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lco/uk/getmondo/spending/SpendingCategoryDetailsActivity;->b:Lco/uk/getmondo/d/h;

    return-object v0
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 55
    invoke-super {p0, p1}, Lco/uk/getmondo/common/activities/b;->onCreate(Landroid/os/Bundle;)V

    .line 56
    const v0, 0x7f050063

    invoke-virtual {p0, v0}, Lco/uk/getmondo/spending/SpendingCategoryDetailsActivity;->setContentView(I)V

    .line 57
    invoke-static {p0}, Lbutterknife/ButterKnife;->bind(Landroid/app/Activity;)Lbutterknife/Unbinder;

    .line 59
    invoke-virtual {p0}, Lco/uk/getmondo/spending/SpendingCategoryDetailsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "year_month"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lorg/threeten/bp/YearMonth;

    iput-object v0, p0, Lco/uk/getmondo/spending/SpendingCategoryDetailsActivity;->a:Lorg/threeten/bp/YearMonth;

    .line 60
    invoke-virtual {p0}, Lco/uk/getmondo/spending/SpendingCategoryDetailsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "category"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/d/h;

    iput-object v0, p0, Lco/uk/getmondo/spending/SpendingCategoryDetailsActivity;->b:Lco/uk/getmondo/d/h;

    .line 62
    iget-object v0, p0, Lco/uk/getmondo/spending/SpendingCategoryDetailsActivity;->toolbar:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {p0, v0}, Lco/uk/getmondo/spending/SpendingCategoryDetailsActivity;->setSupportActionBar(Landroid/support/v7/widget/Toolbar;)V

    .line 63
    iget-object v0, p0, Lco/uk/getmondo/spending/SpendingCategoryDetailsActivity;->viewPager:Landroid/support/v4/view/ViewPager;

    new-instance v1, Lco/uk/getmondo/spending/SpendingCategoryDetailsActivity$a;

    invoke-virtual {p0}, Lco/uk/getmondo/spending/SpendingCategoryDetailsActivity;->getSupportFragmentManager()Landroid/support/v4/app/n;

    move-result-object v2

    invoke-direct {v1, p0, v2}, Lco/uk/getmondo/spending/SpendingCategoryDetailsActivity$a;-><init>(Lco/uk/getmondo/spending/SpendingCategoryDetailsActivity;Landroid/support/v4/app/n;)V

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setAdapter(Landroid/support/v4/view/p;)V

    .line 64
    iget-object v0, p0, Lco/uk/getmondo/spending/SpendingCategoryDetailsActivity;->tabLayout:Landroid/support/design/widget/TabLayout;

    iget-object v1, p0, Lco/uk/getmondo/spending/SpendingCategoryDetailsActivity;->viewPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0, v1}, Landroid/support/design/widget/TabLayout;->setupWithViewPager(Landroid/support/v4/view/ViewPager;)V

    .line 66
    iget-object v0, p0, Lco/uk/getmondo/spending/SpendingCategoryDetailsActivity;->toolbar:Landroid/support/v7/widget/Toolbar;

    iget-object v1, p0, Lco/uk/getmondo/spending/SpendingCategoryDetailsActivity;->b:Lco/uk/getmondo/d/h;

    invoke-virtual {v1}, Lco/uk/getmondo/d/h;->a()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/Toolbar;->setTitle(I)V

    .line 68
    iget-object v0, p0, Lco/uk/getmondo/spending/SpendingCategoryDetailsActivity;->b:Lco/uk/getmondo/d/h;

    invoke-virtual {v0}, Lco/uk/getmondo/d/h;->b()I

    move-result v0

    invoke-static {p0, v0}, Landroid/support/v4/content/a;->c(Landroid/content/Context;I)I

    move-result v0

    .line 69
    iget-object v1, p0, Lco/uk/getmondo/spending/SpendingCategoryDetailsActivity;->appBarLayout:Landroid/support/design/widget/AppBarLayout;

    invoke-virtual {v1, v0}, Landroid/support/design/widget/AppBarLayout;->setBackgroundColor(I)V

    .line 70
    invoke-virtual {p0}, Lco/uk/getmondo/spending/SpendingCategoryDetailsActivity;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-static {v0}, Lco/uk/getmondo/common/k/c;->a(I)I

    move-result v0

    invoke-virtual {v1, v0}, Landroid/view/Window;->setStatusBarColor(I)V

    .line 71
    return-void
.end method
