.class public Lco/uk/getmondo/spending/MonthSpendingView;
.super Landroid/widget/FrameLayout;
.source "MonthSpendingView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lco/uk/getmondo/spending/MonthSpendingView$ViewHolder;,
        Lco/uk/getmondo/spending/MonthSpendingView$a;
    }
.end annotation


# instance fields
.field private final a:Lco/uk/getmondo/common/ui/a;

.field private b:Lorg/threeten/bp/YearMonth;

.field private c:Lco/uk/getmondo/spending/MonthSpendingView$a;

.field cardsContainer:Landroid/widget/LinearLayout;
    .annotation build Lbutterknife/BindView;
        value = 0x7f1104ba
    .end annotation
.end field

.field private d:I

.field monthAmountView:Lco/uk/getmondo/common/ui/AmountView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f1104b9
    .end annotation
.end field

.field monthNameTextView:Landroid/widget/TextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f110430
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 53
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lco/uk/getmondo/spending/MonthSpendingView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 54
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 57
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lco/uk/getmondo/spending/MonthSpendingView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 58
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 7

    .prologue
    .line 61
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 62
    invoke-static {p1}, Lco/uk/getmondo/common/ui/a;->a(Landroid/content/Context;)Lco/uk/getmondo/common/ui/a;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/spending/MonthSpendingView;->a:Lco/uk/getmondo/common/ui/a;

    .line 63
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f050143

    invoke-virtual {v0, v1, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 64
    invoke-static {p0}, Lbutterknife/ButterKnife;->bind(Landroid/view/View;)Lbutterknife/Unbinder;

    .line 66
    invoke-virtual {p0}, Lco/uk/getmondo/spending/MonthSpendingView;->isInEditMode()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 67
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 68
    new-instance v2, Lco/uk/getmondo/spending/a/g;

    new-instance v0, Lco/uk/getmondo/d/c;

    const-wide/16 v4, 0x7d0

    const-string v3, "GBP"

    invoke-direct {v0, v4, v5, v3}, Lco/uk/getmondo/d/c;-><init>(JLjava/lang/String;)V

    const/4 v3, 0x3

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    invoke-direct {v2, v0, v3, v4}, Lco/uk/getmondo/spending/a/g;-><init>(Lco/uk/getmondo/d/c;ILjava/util/List;)V

    .line 69
    invoke-static {}, Lco/uk/getmondo/d/h;->values()[Lco/uk/getmondo/d/h;

    move-result-object v3

    array-length v4, v3

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v4, :cond_0

    aget-object v5, v3, v0

    .line 70
    new-instance v6, Lco/uk/getmondo/spending/a/h$a;

    invoke-direct {v6, v2, v5}, Lco/uk/getmondo/spending/a/h$a;-><init>(Lco/uk/getmondo/spending/a/g;Lco/uk/getmondo/d/h;)V

    invoke-interface {v1, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 69
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 72
    :cond_0
    invoke-direct {p0, v1}, Lco/uk/getmondo/spending/MonthSpendingView;->setSpendingBreakdownData(Ljava/util/List;)V

    .line 75
    :cond_1
    const/4 v0, 0x2

    const/high16 v1, 0x41a00000    # 20.0f

    invoke-virtual {p0}, Lco/uk/getmondo/spending/MonthSpendingView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    invoke-static {v0, v1, v2}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lco/uk/getmondo/spending/MonthSpendingView;->d:I

    .line 76
    return-void
.end method

.method static synthetic a(Lco/uk/getmondo/spending/MonthSpendingView;)I
    .locals 1

    .prologue
    .line 41
    iget v0, p0, Lco/uk/getmondo/spending/MonthSpendingView;->d:I

    return v0
.end method

.method static synthetic b(Lco/uk/getmondo/spending/MonthSpendingView;)Lco/uk/getmondo/common/ui/a;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lco/uk/getmondo/spending/MonthSpendingView;->a:Lco/uk/getmondo/common/ui/a;

    return-object v0
.end method

.method static synthetic c(Lco/uk/getmondo/spending/MonthSpendingView;)Lco/uk/getmondo/spending/MonthSpendingView$a;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lco/uk/getmondo/spending/MonthSpendingView;->c:Lco/uk/getmondo/spending/MonthSpendingView$a;

    return-object v0
.end method

.method static synthetic d(Lco/uk/getmondo/spending/MonthSpendingView;)Lorg/threeten/bp/YearMonth;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lco/uk/getmondo/spending/MonthSpendingView;->b:Lorg/threeten/bp/YearMonth;

    return-object v0
.end method

.method private setSpendingBreakdownData(Ljava/util/List;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lco/uk/getmondo/spending/a/h;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 91
    move v2, v3

    :goto_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_1

    .line 92
    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/spending/a/h;

    .line 93
    iget-object v1, p0, Lco/uk/getmondo/spending/MonthSpendingView;->cardsContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 95
    if-eqz v1, :cond_0

    .line 96
    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    .line 97
    invoke-virtual {v1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lco/uk/getmondo/spending/MonthSpendingView$ViewHolder;

    .line 104
    :goto_1
    invoke-virtual {v1, v0}, Lco/uk/getmondo/spending/MonthSpendingView$ViewHolder;->a(Lco/uk/getmondo/spending/a/h;)V

    .line 91
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 99
    :cond_0
    invoke-virtual {p0}, Lco/uk/getmondo/spending/MonthSpendingView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const v4, 0x7f05010a

    iget-object v5, p0, Lco/uk/getmondo/spending/MonthSpendingView;->cardsContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v4, v5, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v4

    .line 100
    new-instance v1, Lco/uk/getmondo/spending/MonthSpendingView$ViewHolder;

    invoke-direct {v1, p0, v4}, Lco/uk/getmondo/spending/MonthSpendingView$ViewHolder;-><init>(Lco/uk/getmondo/spending/MonthSpendingView;Landroid/view/View;)V

    .line 101
    invoke-virtual {v4, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 102
    iget-object v5, p0, Lco/uk/getmondo/spending/MonthSpendingView;->cardsContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v5, v4}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    goto :goto_1

    .line 107
    :cond_1
    iget-object v0, p0, Lco/uk/getmondo/spending/MonthSpendingView;->cardsContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v0

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    if-le v0, v1, :cond_2

    .line 108
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    :goto_2
    iget-object v1, p0, Lco/uk/getmondo/spending/MonthSpendingView;->cardsContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v1

    if-ge v0, v1, :cond_2

    .line 109
    iget-object v1, p0, Lco/uk/getmondo/spending/MonthSpendingView;->cardsContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 108
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 112
    :cond_2
    return-void
.end method


# virtual methods
.method public a(Lorg/threeten/bp/YearMonth;Lco/uk/getmondo/spending/a/g;)V
    .locals 4

    .prologue
    .line 79
    iput-object p1, p0, Lco/uk/getmondo/spending/MonthSpendingView;->b:Lorg/threeten/bp/YearMonth;

    .line 80
    iget-object v0, p0, Lco/uk/getmondo/spending/MonthSpendingView;->monthAmountView:Lco/uk/getmondo/common/ui/AmountView;

    invoke-virtual {p2}, Lco/uk/getmondo/spending/a/g;->a()Lco/uk/getmondo/d/c;

    move-result-object v1

    invoke-virtual {v0, v1}, Lco/uk/getmondo/common/ui/AmountView;->setAmount(Lco/uk/getmondo/d/c;)V

    .line 81
    iget-object v0, p0, Lco/uk/getmondo/spending/MonthSpendingView;->monthNameTextView:Landroid/widget/TextView;

    invoke-virtual {p1}, Lorg/threeten/bp/YearMonth;->c()Lorg/threeten/bp/Month;

    move-result-object v1

    sget-object v2, Lorg/threeten/bp/format/TextStyle;->a:Lorg/threeten/bp/format/TextStyle;

    sget-object v3, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {v1, v2, v3}, Lorg/threeten/bp/Month;->a(Lorg/threeten/bp/format/TextStyle;Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 82
    invoke-virtual {p2}, Lco/uk/getmondo/spending/a/g;->c()Ljava/util/List;

    move-result-object v0

    invoke-direct {p0, v0}, Lco/uk/getmondo/spending/MonthSpendingView;->setSpendingBreakdownData(Ljava/util/List;)V

    .line 83
    return-void
.end method

.method public setItemClickListener(Lco/uk/getmondo/spending/MonthSpendingView$a;)V
    .locals 0

    .prologue
    .line 86
    iput-object p1, p0, Lco/uk/getmondo/spending/MonthSpendingView;->c:Lco/uk/getmondo/spending/MonthSpendingView$a;

    .line 87
    return-void
.end method
