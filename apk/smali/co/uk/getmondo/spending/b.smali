.class public final Lco/uk/getmondo/spending/b;
.super Lco/uk/getmondo/common/f/a;
.source "SpendingFragment.kt"

# interfaces
.implements Lco/uk/getmondo/spending/d$a;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lco/uk/getmondo/spending/b$a;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000\u0090\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u0012\u0018\u0000 F2\u00020\u00012\u00020\u0002:\u0001FB\u0005\u00a2\u0006\u0002\u0010\u0003J\"\u0010\u001f\u001a\u00020 2\u0018\u0010!\u001a\u0014\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00020\t\u0012\u0004\u0012\u00020#0\"0\u0008H\u0002J\u0008\u0010$\u001a\u00020%H\u0016J\u0012\u0010&\u001a\u00020%2\u0008\u0010\'\u001a\u0004\u0018\u00010(H\u0016J\u0018\u0010)\u001a\u00020%2\u0006\u0010*\u001a\u00020+2\u0006\u0010,\u001a\u00020-H\u0016J&\u0010.\u001a\u0004\u0018\u00010/2\u0006\u0010,\u001a\u0002002\u0008\u00101\u001a\u0004\u0018\u0001022\u0008\u0010\'\u001a\u0004\u0018\u00010(H\u0016J\u0008\u00103\u001a\u00020%H\u0016J\u0010\u00104\u001a\u0002052\u0006\u00106\u001a\u00020\u0005H\u0016J\u000e\u00107\u001a\u0008\u0012\u0004\u0012\u00020\u000e0\u0007H\u0016J\u001a\u00108\u001a\u00020%2\u0006\u00109\u001a\u00020/2\u0008\u0010\'\u001a\u0004\u0018\u00010(H\u0016J\u0008\u0010:\u001a\u00020%H\u0016J\u0010\u0010;\u001a\u00020%2\u0006\u0010<\u001a\u000205H\u0016J\"\u0010=\u001a\u00020%2\u0018\u0010!\u001a\u0014\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00020\t\u0012\u0004\u0012\u00020#0\"0\u0008H\u0016J\u0008\u0010>\u001a\u00020%H\u0016J\u0008\u0010?\u001a\u00020%H\u0016J\u0016\u0010@\u001a\u00020%2\u000c\u0010A\u001a\u0008\u0012\u0004\u0012\u00020\t0\u0008H\u0016J\u0018\u0010B\u001a\u00020%2\u0006\u0010C\u001a\u00020 2\u0006\u0010D\u001a\u00020 H\u0016J\u0008\u0010E\u001a\u00020%H\u0016R\u0010\u0010\u0004\u001a\u0004\u0018\u00010\u0005X\u0082\u000e\u00a2\u0006\u0002\n\u0000R \u0010\u0006\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\t0\u00080\u00078VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\n\u0010\u000bR2\u0010\u000c\u001a&\u0012\u000c\u0012\n \u000f*\u0004\u0018\u00010\u000e0\u000e \u000f*\u0012\u0012\u000c\u0012\n \u000f*\u0004\u0018\u00010\u000e0\u000e\u0018\u00010\r0\rX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001b\u0010\u0010\u001a\u00020\u00118BX\u0082\u0084\u0002\u00a2\u0006\u000c\n\u0004\u0008\u0014\u0010\u0015\u001a\u0004\u0008\u0012\u0010\u0013R\u001e\u0010\u0016\u001a\u00020\u00178\u0000@\u0000X\u0081.\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u0018\u0010\u0019\"\u0004\u0008\u001a\u0010\u001bR\u000e\u0010\u001c\u001a\u00020\u001dX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u001e\u001a\u0004\u0018\u00010\tX\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006G"
    }
    d2 = {
        "Lco/uk/getmondo/spending/SpendingFragment;",
        "Lco/uk/getmondo/common/fragments/BaseFragment;",
        "Lco/uk/getmondo/spending/SpendingPresenter$View;",
        "()V",
        "exportAction",
        "Landroid/view/MenuItem;",
        "exportClicks",
        "Lio/reactivex/Observable;",
        "",
        "Lorg/threeten/bp/YearMonth;",
        "getExportClicks",
        "()Lio/reactivex/Observable;",
        "exportRelay",
        "Lcom/jakewharton/rxrelay2/PublishRelay;",
        "",
        "kotlin.jvm.PlatformType",
        "linearLayoutManager",
        "Landroid/support/v7/widget/LinearLayoutManager;",
        "getLinearLayoutManager",
        "()Landroid/support/v7/widget/LinearLayoutManager;",
        "linearLayoutManager$delegate",
        "Lkotlin/Lazy;",
        "presenter",
        "Lco/uk/getmondo/spending/SpendingPresenter;",
        "getPresenter$app_monzoPrepaidRelease",
        "()Lco/uk/getmondo/spending/SpendingPresenter;",
        "setPresenter$app_monzoPrepaidRelease",
        "(Lco/uk/getmondo/spending/SpendingPresenter;)V",
        "spendingAdapter",
        "Lco/uk/getmondo/spending/SpendingAdapter;",
        "yearMonth",
        "findPositionByMonth",
        "",
        "spendingData",
        "Landroid/support/v4/util/Pair;",
        "Lco/uk/getmondo/spending/data/SpendingData;",
        "hideRecurringPaymentsBanner",
        "",
        "onCreate",
        "savedInstanceState",
        "Landroid/os/Bundle;",
        "onCreateOptionsMenu",
        "menu",
        "Landroid/view/Menu;",
        "inflater",
        "Landroid/view/MenuInflater;",
        "onCreateView",
        "Landroid/view/View;",
        "Landroid/view/LayoutInflater;",
        "container",
        "Landroid/view/ViewGroup;",
        "onDestroyView",
        "onOptionsItemSelected",
        "",
        "item",
        "onRecurringPaymentsClicked",
        "onViewCreated",
        "view",
        "openRecurringPayments",
        "setExportEnabled",
        "isEnabled",
        "setSpendingData",
        "showCardFrozen",
        "showEmptyNotice",
        "showExportData",
        "months",
        "showRecurringPaymentsBanner",
        "directDebitCount",
        "paymentSeriesCount",
        "showTitle",
        "Companion",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# static fields
.field static final synthetic a:[Lkotlin/reflect/l;

.field public static final d:Lco/uk/getmondo/spending/b$a;


# instance fields
.field public c:Lco/uk/getmondo/spending/d;

.field private final e:Lcom/b/b/c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/b/b/c",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private final f:Lco/uk/getmondo/spending/a;

.field private final g:Lkotlin/c;

.field private h:Lorg/threeten/bp/YearMonth;

.field private i:Landroid/view/MenuItem;

.field private j:Ljava/util/HashMap;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const/4 v0, 0x1

    new-array v1, v0, [Lkotlin/reflect/l;

    const/4 v2, 0x0

    new-instance v0, Lkotlin/d/b/w;

    const-class v3, Lco/uk/getmondo/spending/b;

    invoke-static {v3}, Lkotlin/d/b/y;->a(Ljava/lang/Class;)Lkotlin/reflect/c;

    move-result-object v3

    const-string v4, "linearLayoutManager"

    const-string v5, "getLinearLayoutManager()Landroid/support/v7/widget/LinearLayoutManager;"

    invoke-direct {v0, v3, v4, v5}, Lkotlin/d/b/w;-><init>(Lkotlin/reflect/e;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v0}, Lkotlin/d/b/y;->a(Lkotlin/d/b/v;)Lkotlin/reflect/n;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/l;

    aput-object v0, v1, v2

    sput-object v1, Lco/uk/getmondo/spending/b;->a:[Lkotlin/reflect/l;

    new-instance v0, Lco/uk/getmondo/spending/b$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lco/uk/getmondo/spending/b$a;-><init>(Lkotlin/d/b/i;)V

    sput-object v0, Lco/uk/getmondo/spending/b;->d:Lco/uk/getmondo/spending/b$a;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 25
    invoke-direct {p0}, Lco/uk/getmondo/common/f/a;-><init>()V

    .line 27
    invoke-static {}, Lcom/b/b/c;->a()Lcom/b/b/c;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/spending/b;->e:Lcom/b/b/c;

    .line 31
    new-instance v0, Lco/uk/getmondo/spending/a;

    const/4 v1, 0x3

    invoke-direct {v0, v2, v2, v1, v2}, Lco/uk/getmondo/spending/a;-><init>(Ljava/util/List;Lkotlin/d/a/m;ILkotlin/d/b/i;)V

    iput-object v0, p0, Lco/uk/getmondo/spending/b;->f:Lco/uk/getmondo/spending/a;

    .line 32
    new-instance v0, Lco/uk/getmondo/spending/b$c;

    invoke-direct {v0, p0}, Lco/uk/getmondo/spending/b$c;-><init>(Lco/uk/getmondo/spending/b;)V

    check-cast v0, Lkotlin/d/a/a;

    invoke-static {v0}, Lkotlin/d;->a(Lkotlin/d/a/a;)Lkotlin/c;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/spending/b;->g:Lkotlin/c;

    return-void
.end method

.method public static final synthetic a(Lco/uk/getmondo/spending/b;)Landroid/support/v7/widget/LinearLayoutManager;
    .locals 1

    .prologue
    .line 25
    invoke-direct {p0}, Lco/uk/getmondo/spending/b;->i()Landroid/support/v7/widget/LinearLayoutManager;

    move-result-object v0

    return-object v0
.end method

.method public static final synthetic b(Lco/uk/getmondo/spending/b;)Lco/uk/getmondo/spending/a;
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, Lco/uk/getmondo/spending/b;->f:Lco/uk/getmondo/spending/a;

    return-object v0
.end method

.method private final c(Ljava/util/List;)I
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<+",
            "Landroid/support/v4/g/j",
            "<",
            "Lorg/threeten/bp/YearMonth;",
            "Lco/uk/getmondo/spending/a/g;",
            ">;>;)I"
        }
    .end annotation

    .prologue
    .line 111
    move-object v0, p1

    check-cast v0, Ljava/util/Collection;

    invoke-static {v0}, Lkotlin/a/m;->a(Ljava/util/Collection;)Lkotlin/f/c;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 177
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    move-object v0, v1

    check-cast v0, Ljava/lang/Number;

    invoke-virtual {v0}, Ljava/lang/Number;->intValue()I

    move-result v0

    .line 111
    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/g/j;

    iget-object v0, v0, Landroid/support/v4/g/j;->a:Ljava/lang/Object;

    check-cast v0, Lorg/threeten/bp/YearMonth;

    iget-object v3, p0, Lco/uk/getmondo/spending/b;->h:Lorg/threeten/bp/YearMonth;

    invoke-static {v0, v3}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    move-object v0, v1

    .line 178
    :goto_0
    check-cast v0, Ljava/lang/Integer;

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 111
    :goto_1
    return v0

    .line 178
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 111
    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private final i()Landroid/support/v7/widget/LinearLayoutManager;
    .locals 3

    iget-object v0, p0, Lco/uk/getmondo/spending/b;->g:Lkotlin/c;

    sget-object v1, Lco/uk/getmondo/spending/b;->a:[Lkotlin/reflect/l;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-interface {v0}, Lkotlin/c;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/LinearLayoutManager;

    return-object v0
.end method


# virtual methods
.method public a(I)Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lco/uk/getmondo/spending/b;->j:Ljava/util/HashMap;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lco/uk/getmondo/spending/b;->j:Ljava/util/HashMap;

    :cond_0
    iget-object v0, p0, Lco/uk/getmondo/spending/b;->j:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    if-nez v0, :cond_1

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getView()Landroid/view/View;

    move-result-object v0

    if-nez v0, :cond_2

    const/4 v0, 0x0

    :cond_1
    :goto_0
    return-object v0

    :cond_2
    invoke-virtual {v0, p1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/spending/b;->j:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method public a()Lio/reactivex/n;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Ljava/util/List",
            "<",
            "Lorg/threeten/bp/YearMonth;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 92
    iget-object v1, p0, Lco/uk/getmondo/spending/b;->e:Lcom/b/b/c;

    new-instance v0, Lco/uk/getmondo/spending/b$b;

    invoke-direct {v0, p0}, Lco/uk/getmondo/spending/b$b;-><init>(Lco/uk/getmondo/spending/b;)V

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {v1, v0}, Lcom/b/b/c;->map(Lio/reactivex/c/h;)Lio/reactivex/n;

    move-result-object v0

    const-string v1, "exportRelay.map {\n      \u2026ap { it.first }\n        }"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 96
    return-object v0
.end method

.method public a(II)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 122
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 123
    if-lez p1, :cond_0

    .line 124
    invoke-virtual {p0}, Lco/uk/getmondo/spending/b;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f120002

    .line 125
    new-array v3, v6, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    .line 124
    invoke-virtual {v0, v2, p1, v3}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 128
    :cond_0
    if-lez p1, :cond_1

    if-lez p2, :cond_1

    .line 129
    const-string v0, ", "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 132
    :cond_1
    if-lez p2, :cond_2

    .line 133
    invoke-virtual {p0}, Lco/uk/getmondo/spending/b;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f120003

    .line 134
    new-array v3, v6, [Ljava/lang/Object;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    .line 133
    invoke-virtual {v0, v2, p2, v3}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 136
    :cond_2
    sget v0, Lco/uk/getmondo/c$a;->recurringBannerSubtitle:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/spending/b;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 137
    sget v0, Lco/uk/getmondo/c$a;->recurringPaymentsBanner:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/spending/b;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/constraint/ConstraintLayout;

    invoke-virtual {v0, v5}, Landroid/support/constraint/ConstraintLayout;->setVisibility(I)V

    .line 138
    sget v0, Lco/uk/getmondo/c$a;->topShadow:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/spending/b;->a(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/view/View;->setVisibility(I)V

    .line 139
    invoke-virtual {p0}, Lco/uk/getmondo/spending/b;->getActivity()Landroid/support/v4/app/j;

    move-result-object v0

    if-nez v0, :cond_3

    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type co.uk.getmondo.main.HomeActivity"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_3
    check-cast v0, Lco/uk/getmondo/main/HomeActivity;

    invoke-virtual {v0, v5}, Lco/uk/getmondo/main/HomeActivity;->a(Z)V

    .line 140
    return-void
.end method

.method public a(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<+",
            "Landroid/support/v4/g/j",
            "<",
            "Lorg/threeten/bp/YearMonth;",
            "Lco/uk/getmondo/spending/a/g;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    const-string v0, "spendingData"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 101
    iget-object v0, p0, Lco/uk/getmondo/spending/b;->f:Lco/uk/getmondo/spending/a;

    invoke-virtual {v0, p1}, Lco/uk/getmondo/spending/a;->a(Ljava/util/List;)V

    .line 102
    iget-object v0, p0, Lco/uk/getmondo/spending/b;->h:Lorg/threeten/bp/YearMonth;

    if-eqz v0, :cond_0

    .line 103
    sget v0, Lco/uk/getmondo/c$a;->spendingRecyclerView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/spending/b;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView;

    invoke-direct {p0, p1}, Lco/uk/getmondo/spending/b;->c(Ljava/util/List;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->a(I)V

    .line 104
    const/4 v0, 0x0

    check-cast v0, Lorg/threeten/bp/YearMonth;

    iput-object v0, p0, Lco/uk/getmondo/spending/b;->h:Lorg/threeten/bp/YearMonth;

    .line 106
    :cond_0
    sget v0, Lco/uk/getmondo/c$a;->spendingRecyclerView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/spending/b;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setVisibility(I)V

    .line 107
    sget v0, Lco/uk/getmondo/c$a;->spendingEmptyView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/spending/b;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 108
    return-void
.end method

.method public a(Z)V
    .locals 1

    .prologue
    .line 159
    iget-object v0, p0, Lco/uk/getmondo/spending/b;->i:Landroid/view/MenuItem;

    if-eqz v0, :cond_0

    invoke-interface {v0, p1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 160
    :cond_0
    return-void
.end method

.method public b()Lio/reactivex/n;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 98
    sget v0, Lco/uk/getmondo/c$a;->recurringPaymentsBanner:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/spending/b;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/constraint/ConstraintLayout;

    invoke-static {v0}, Lcom/b/a/c/c;->a(Landroid/view/View;)Lio/reactivex/n;

    move-result-object v0

    const-string v1, "RxView.clicks(recurringPaymentsBanner)"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public b(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lorg/threeten/bp/YearMonth;",
            ">;)V"
        }
    .end annotation

    .prologue
    const-string v0, "months"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 163
    new-instance v1, Lco/uk/getmondo/spending/b/a;

    invoke-virtual {p0}, Lco/uk/getmondo/spending/b;->getActivity()Landroid/support/v4/app/j;

    move-result-object v0

    const-string v2, "activity"

    invoke-static {v0, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/app/Activity;

    invoke-direct {v1, v0, p1}, Lco/uk/getmondo/spending/b/a;-><init>(Landroid/app/Activity;Ljava/util/List;)V

    invoke-virtual {v1}, Lco/uk/getmondo/spending/b/a;->show()V

    .line 164
    return-void
.end method

.method public c()V
    .locals 2

    .prologue
    .line 115
    sget v0, Lco/uk/getmondo/c$a;->spendingEmptyView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/spending/b;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 116
    sget v0, Lco/uk/getmondo/c$a;->spendingRecyclerView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/spending/b;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setVisibility(I)V

    .line 117
    return-void
.end method

.method public d()V
    .locals 3

    .prologue
    .line 119
    new-instance v1, Landroid/content/Intent;

    invoke-virtual {p0}, Lco/uk/getmondo/spending/b;->getActivity()Landroid/support/v4/app/j;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    const-class v2, Lco/uk/getmondo/payments/recurring_list/RecurringPaymentsActivity;

    invoke-direct {v1, v0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v1}, Lco/uk/getmondo/spending/b;->startActivity(Landroid/content/Intent;)V

    return-void
.end method

.method public e()V
    .locals 2

    .prologue
    const/16 v1, 0x8

    .line 143
    sget v0, Lco/uk/getmondo/c$a;->recurringPaymentsBanner:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/spending/b;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/constraint/ConstraintLayout;

    invoke-virtual {v0, v1}, Landroid/support/constraint/ConstraintLayout;->setVisibility(I)V

    .line 144
    sget v0, Lco/uk/getmondo/c$a;->topShadow:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/spending/b;->a(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 145
    invoke-virtual {p0}, Lco/uk/getmondo/spending/b;->getActivity()Landroid/support/v4/app/j;

    move-result-object v0

    if-nez v0, :cond_0

    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type co.uk.getmondo.main.HomeActivity"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    check-cast v0, Lco/uk/getmondo/main/HomeActivity;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lco/uk/getmondo/main/HomeActivity;->a(Z)V

    .line 146
    return-void
.end method

.method public f()V
    .locals 3

    .prologue
    .line 149
    sget v0, Lco/uk/getmondo/c$a;->toolbar:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/spending/b;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/Toolbar;

    const v1, 0x7f0a02ba

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/Toolbar;->setTitle(I)V

    .line 150
    sget v0, Lco/uk/getmondo/c$a;->toolbar:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/spending/b;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/Toolbar;

    invoke-virtual {p0}, Lco/uk/getmondo/spending/b;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x106000b

    invoke-static {v1, v2}, Landroid/support/v4/content/a;->c(Landroid/content/Context;I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/Toolbar;->setTitleTextColor(I)V

    .line 151
    return-void
.end method

.method public g()V
    .locals 3

    .prologue
    .line 154
    sget v0, Lco/uk/getmondo/c$a;->toolbar:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/spending/b;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/Toolbar;

    const v1, 0x7f0a02b1

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/Toolbar;->setTitle(I)V

    .line 155
    sget v0, Lco/uk/getmondo/c$a;->toolbar:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/spending/b;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/Toolbar;

    invoke-virtual {p0}, Lco/uk/getmondo/spending/b;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0f0079

    invoke-static {v1, v2}, Landroid/support/v4/content/a;->c(Landroid/content/Context;I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/Toolbar;->setTitleTextColor(I)V

    .line 156
    return-void
.end method

.method public h()V
    .locals 1

    iget-object v0, p0, Lco/uk/getmondo/spending/b;->j:Ljava/util/HashMap;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lco/uk/getmondo/spending/b;->j:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    :cond_0
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 39
    invoke-super {p0, p1}, Lco/uk/getmondo/common/f/a;->onCreate(Landroid/os/Bundle;)V

    .line 40
    invoke-virtual {p0}, Lco/uk/getmondo/spending/b;->B()Lco/uk/getmondo/common/h/b/b;

    move-result-object v0

    invoke-interface {v0, p0}, Lco/uk/getmondo/common/h/b/b;->a(Lco/uk/getmondo/spending/b;)V

    .line 42
    invoke-virtual {p0}, Lco/uk/getmondo/spending/b;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lco/uk/getmondo/spending/b;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "KEY_YEAR_MONTH"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 43
    invoke-virtual {p0}, Lco/uk/getmondo/spending/b;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "KEY_YEAR_MONTH"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    if-nez v0, :cond_0

    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type org.threeten.bp.YearMonth"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    check-cast v0, Lorg/threeten/bp/YearMonth;

    iput-object v0, p0, Lco/uk/getmondo/spending/b;->h:Lorg/threeten/bp/YearMonth;

    .line 45
    :cond_1
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V
    .locals 1

    .prologue
    const-string v0, "menu"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "inflater"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 77
    invoke-super {p0, p1, p2}, Lco/uk/getmondo/common/f/a;->onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V

    .line 78
    const v0, 0x7f130007

    invoke-virtual {p2, v0, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 79
    const v0, 0x7f1104d6

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/spending/b;->i:Landroid/view/MenuItem;

    .line 80
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2

    .prologue
    const-string v0, "inflater"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 48
    const v0, 0x7f0500a9

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public onDestroyView()V
    .locals 2

    .prologue
    .line 72
    iget-object v0, p0, Lco/uk/getmondo/spending/b;->c:Lco/uk/getmondo/spending/d;

    if-nez v0, :cond_0

    const-string v1, "presenter"

    invoke-static {v1}, Lkotlin/d/b/l;->b(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0}, Lco/uk/getmondo/spending/d;->b()V

    .line 73
    invoke-super {p0}, Lco/uk/getmondo/common/f/a;->onDestroyView()V

    .line 74
    invoke-virtual {p0}, Lco/uk/getmondo/spending/b;->h()V

    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2

    .prologue
    const-string v0, "item"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 83
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    .line 84
    const v1, 0x7f1104d6

    if-ne v0, v1, :cond_0

    .line 85
    iget-object v0, p0, Lco/uk/getmondo/spending/b;->e:Lcom/b/b/c;

    sget-object v1, Lkotlin/n;->a:Lkotlin/n;

    invoke-virtual {v0, v1}, Lcom/b/b/c;->a(Ljava/lang/Object;)V

    .line 86
    const/4 v0, 0x1

    .line 88
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Lco/uk/getmondo/common/f/a;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    goto :goto_0
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 3

    .prologue
    const-string v0, "view"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 52
    invoke-super {p0, p1, p2}, Lco/uk/getmondo/common/f/a;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 53
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lco/uk/getmondo/spending/b;->setHasOptionsMenu(Z)V

    .line 55
    iget-object v1, p0, Lco/uk/getmondo/spending/b;->f:Lco/uk/getmondo/spending/a;

    new-instance v0, Lco/uk/getmondo/spending/b$d;

    invoke-direct {v0, p0}, Lco/uk/getmondo/spending/b$d;-><init>(Lco/uk/getmondo/spending/b;)V

    check-cast v0, Lkotlin/d/a/m;

    invoke-virtual {v1, v0}, Lco/uk/getmondo/spending/a;->a(Lkotlin/d/a/m;)V

    .line 61
    sget v0, Lco/uk/getmondo/c$a;->spendingRecyclerView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/spending/b;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView;

    iget-object v1, p0, Lco/uk/getmondo/spending/b;->f:Lco/uk/getmondo/spending/a;

    check-cast v1, Landroid/support/v7/widget/RecyclerView$a;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setAdapter(Landroid/support/v7/widget/RecyclerView$a;)V

    .line 62
    sget v0, Lco/uk/getmondo/c$a;->spendingRecyclerView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/spending/b;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView;

    invoke-direct {p0}, Lco/uk/getmondo/spending/b;->i()Landroid/support/v7/widget/LinearLayoutManager;

    move-result-object v1

    check-cast v1, Landroid/support/v7/widget/RecyclerView$h;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(Landroid/support/v7/widget/RecyclerView$h;)V

    .line 64
    iget-object v1, p0, Lco/uk/getmondo/spending/b;->c:Lco/uk/getmondo/spending/d;

    if-nez v1, :cond_0

    const-string v0, "presenter"

    invoke-static {v0}, Lkotlin/d/b/l;->b(Ljava/lang/String;)V

    :cond_0
    move-object v0, p0

    check-cast v0, Lco/uk/getmondo/spending/d$a;

    invoke-virtual {v1, v0}, Lco/uk/getmondo/spending/d;->a(Lco/uk/getmondo/spending/d$a;)V

    .line 66
    invoke-virtual {p0}, Lco/uk/getmondo/spending/b;->getActivity()Landroid/support/v4/app/j;

    move-result-object v0

    if-nez v0, :cond_1

    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type co.uk.getmondo.main.HomeActivity"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    check-cast v0, Lco/uk/getmondo/main/HomeActivity;

    .line 67
    sget v1, Lco/uk/getmondo/c$a;->toolbar:I

    invoke-virtual {p0, v1}, Lco/uk/getmondo/spending/b;->a(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/support/v7/widget/Toolbar;

    invoke-virtual {v0, v1}, Lco/uk/getmondo/main/HomeActivity;->setSupportActionBar(Landroid/support/v7/widget/Toolbar;)V

    .line 68
    sget v1, Lco/uk/getmondo/c$a;->toolbar:I

    invoke-virtual {p0, v1}, Lco/uk/getmondo/spending/b;->a(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/support/v7/widget/Toolbar;

    const-string v2, "toolbar"

    invoke-static {v1, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lco/uk/getmondo/main/HomeActivity;->a(Landroid/support/v7/widget/Toolbar;)V

    .line 69
    return-void
.end method
