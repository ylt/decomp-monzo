.class final Lco/uk/getmondo/spending/d$d;
.super Ljava/lang/Object;
.source "SpendingPresenter.kt"

# interfaces
.implements Lio/reactivex/c/g;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lco/uk/getmondo/spending/d;->a(Lco/uk/getmondo/spending/d$a;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/g",
        "<",
        "Ljava/util/List",
        "<",
        "Landroid/support/v4/g/j",
        "<",
        "Lorg/threeten/bp/YearMonth;",
        "Lco/uk/getmondo/spending/a/g;",
        ">;>;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000 \n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010!\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0000\u0010\u0000\u001a\u00020\u00012\u009b\u0001\u0010\u0002\u001a\u0096\u0001\u0012D\u0012B\u0012\u000c\u0012\n \u0006*\u0004\u0018\u00010\u00050\u0005\u0012\u000c\u0012\n \u0006*\u0004\u0018\u00010\u00070\u0007 \u0006* \u0012\u000c\u0012\n \u0006*\u0004\u0018\u00010\u00050\u0005\u0012\u000c\u0012\n \u0006*\u0004\u0018\u00010\u00070\u0007\u0018\u00010\u00040\u0004 \u0006*J\u0012D\u0012B\u0012\u000c\u0012\n \u0006*\u0004\u0018\u00010\u00050\u0005\u0012\u000c\u0012\n \u0006*\u0004\u0018\u00010\u00070\u0007 \u0006* \u0012\u000c\u0012\n \u0006*\u0004\u0018\u00010\u00050\u0005\u0012\u000c\u0012\n \u0006*\u0004\u0018\u00010\u00070\u0007\u0018\u00010\u00040\u0004\u0018\u00010\u00080\u0003H\n\u00a2\u0006\u0002\u0008\t"
    }
    d2 = {
        "<anonymous>",
        "",
        "spendingData",
        "",
        "Landroid/support/v4/util/Pair;",
        "Lorg/threeten/bp/YearMonth;",
        "kotlin.jvm.PlatformType",
        "Lco/uk/getmondo/spending/data/SpendingData;",
        "",
        "accept"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field final synthetic a:Lco/uk/getmondo/spending/d$a;


# direct methods
.method constructor <init>(Lco/uk/getmondo/spending/d$a;)V
    .locals 0

    iput-object p1, p0, Lco/uk/getmondo/spending/d$d;->a:Lco/uk/getmondo/spending/d$a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 29
    check-cast p1, Ljava/util/List;

    invoke-virtual {p0, p1}, Lco/uk/getmondo/spending/d$d;->a(Ljava/util/List;)V

    return-void
.end method

.method public final a(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/support/v4/g/j",
            "<",
            "Lorg/threeten/bp/YearMonth;",
            "Lco/uk/getmondo/spending/a/g;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 56
    move-object v0, p1

    check-cast v0, Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    .line 57
    :goto_0
    if-nez v0, :cond_1

    .line 58
    iget-object v1, p0, Lco/uk/getmondo/spending/d$d;->a:Lco/uk/getmondo/spending/d$a;

    invoke-interface {v1}, Lco/uk/getmondo/spending/d$a;->c()V

    .line 61
    :goto_1
    iget-object v1, p0, Lco/uk/getmondo/spending/d$d;->a:Lco/uk/getmondo/spending/d$a;

    invoke-interface {v1, v0}, Lco/uk/getmondo/spending/d$a;->a(Z)V

    .line 63
    return-void

    .line 56
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 60
    :cond_1
    iget-object v1, p0, Lco/uk/getmondo/spending/d$d;->a:Lco/uk/getmondo/spending/d$a;

    const-string v2, "spendingData"

    invoke-static {p1, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v1, p1}, Lco/uk/getmondo/spending/d$a;->a(Ljava/util/List;)V

    goto :goto_1
.end method
