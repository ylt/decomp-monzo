.class final Lco/uk/getmondo/spending/d$b;
.super Ljava/lang/Object;
.source "SpendingPresenter.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lco/uk/getmondo/spending/d;->a(Lco/uk/getmondo/spending/d$a;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h",
        "<TT;",
        "Lio/reactivex/z",
        "<+TR;>;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000$\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010!\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00c3\u0002\u0012\u0099\u0001\u0012\u0096\u0001\u0012D\u0012B\u0012\u000c\u0012\n \u0005*\u0004\u0018\u00010\u00040\u0004\u0012\u000c\u0012\n \u0005*\u0004\u0018\u00010\u00060\u0006 \u0005* \u0012\u000c\u0012\n \u0005*\u0004\u0018\u00010\u00040\u0004\u0012\u000c\u0012\n \u0005*\u0004\u0018\u00010\u00060\u0006\u0018\u00010\u00030\u0003 \u0005*J\u0012D\u0012B\u0012\u000c\u0012\n \u0005*\u0004\u0018\u00010\u00040\u0004\u0012\u000c\u0012\n \u0005*\u0004\u0018\u00010\u00060\u0006 \u0005* \u0012\u000c\u0012\n \u0005*\u0004\u0018\u00010\u00040\u0004\u0012\u000c\u0012\n \u0005*\u0004\u0018\u00010\u00060\u0006\u0018\u00010\u00030\u0003\u0018\u00010\u00070\u0002 \u0005*\u00a0\u0001\u0012\u0099\u0001\u0012\u0096\u0001\u0012D\u0012B\u0012\u000c\u0012\n \u0005*\u0004\u0018\u00010\u00040\u0004\u0012\u000c\u0012\n \u0005*\u0004\u0018\u00010\u00060\u0006 \u0005* \u0012\u000c\u0012\n \u0005*\u0004\u0018\u00010\u00040\u0004\u0012\u000c\u0012\n \u0005*\u0004\u0018\u00010\u00060\u0006\u0018\u00010\u00030\u0003 \u0005*J\u0012D\u0012B\u0012\u000c\u0012\n \u0005*\u0004\u0018\u00010\u00040\u0004\u0012\u000c\u0012\n \u0005*\u0004\u0018\u00010\u00060\u0006 \u0005* \u0012\u000c\u0012\n \u0005*\u0004\u0018\u00010\u00040\u0004\u0012\u000c\u0012\n \u0005*\u0004\u0018\u00010\u00060\u0006\u0018\u00010\u00030\u0003\u0018\u00010\u00070\u0002\u0018\u00010\u00010\u00012(\u0010\u0008\u001a$\u0012\u000c\u0012\n \u0005*\u0004\u0018\u00010\t0\t \u0005*\u0010\u0012\u000c\u0012\n \u0005*\u0004\u0018\u00010\t0\t0\u00070\u0002H\n\u00a2\u0006\u0002\u0008\n"
    }
    d2 = {
        "<anonymous>",
        "Lio/reactivex/Single;",
        "",
        "Landroid/support/v4/util/Pair;",
        "Lorg/threeten/bp/YearMonth;",
        "kotlin.jvm.PlatformType",
        "Lco/uk/getmondo/spending/data/SpendingData;",
        "",
        "transactions",
        "Lco/uk/getmondo/model/Transaction;",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field final synthetic a:Lco/uk/getmondo/spending/d;


# direct methods
.method constructor <init>(Lco/uk/getmondo/spending/d;)V
    .locals 0

    iput-object p1, p0, Lco/uk/getmondo/spending/d$b;->a:Lco/uk/getmondo/spending/d;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/util/List;)Lio/reactivex/v;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lco/uk/getmondo/d/aj;",
            ">;)",
            "Lio/reactivex/v",
            "<",
            "Ljava/util/List",
            "<",
            "Landroid/support/v4/g/j",
            "<",
            "Lorg/threeten/bp/YearMonth;",
            "Lco/uk/getmondo/spending/a/g;",
            ">;>;>;"
        }
    .end annotation

    .prologue
    const-string v0, "transactions"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 50
    iget-object v0, p0, Lco/uk/getmondo/spending/d$b;->a:Lco/uk/getmondo/spending/d;

    invoke-static {v0}, Lco/uk/getmondo/spending/d;->a(Lco/uk/getmondo/spending/d;)Lco/uk/getmondo/spending/a/b;

    move-result-object v0

    sget-object v1, Lco/uk/getmondo/spending/a/a;->b:Lco/uk/getmondo/spending/a/a;

    invoke-virtual {v0, p1, v1}, Lco/uk/getmondo/spending/a/b;->a(Ljava/util/List;Lco/uk/getmondo/spending/a/a;)Lio/reactivex/n;

    move-result-object v0

    .line 51
    invoke-virtual {v0}, Lio/reactivex/n;->toList()Lio/reactivex/v;

    move-result-object v0

    .line 52
    iget-object v1, p0, Lco/uk/getmondo/spending/d$b;->a:Lco/uk/getmondo/spending/d;

    invoke-static {v1}, Lco/uk/getmondo/spending/d;->b(Lco/uk/getmondo/spending/d;)Lio/reactivex/u;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/v;->b(Lio/reactivex/u;)Lio/reactivex/v;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 29
    check-cast p1, Ljava/util/List;

    invoke-virtual {p0, p1}, Lco/uk/getmondo/spending/d$b;->a(Ljava/util/List;)Lio/reactivex/v;

    move-result-object v0

    return-object v0
.end method
