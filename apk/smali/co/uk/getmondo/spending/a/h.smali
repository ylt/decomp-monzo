.class public abstract Lco/uk/getmondo/spending/a/h;
.super Ljava/lang/Object;
.source "SpendingGroup.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lco/uk/getmondo/spending/a/h$b;,
        Lco/uk/getmondo/spending/a/h$c;,
        Lco/uk/getmondo/spending/a/h$a;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000\"\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u00086\u0018\u00002\u00020\u0001:\u0003\u0007\u0008\tB\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002R\u0012\u0010\u0003\u001a\u00020\u0004X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0005\u0010\u0006\u0082\u0001\u0003\n\u000b\u000c\u00a8\u0006\r"
    }
    d2 = {
        "Lco/uk/getmondo/spending/data/SpendingGroup;",
        "",
        "()V",
        "spendingData",
        "Lco/uk/getmondo/spending/data/SpendingData;",
        "getSpendingData",
        "()Lco/uk/getmondo/spending/data/SpendingData;",
        "OfCategory",
        "OfMerchant",
        "OfPeer",
        "Lco/uk/getmondo/spending/data/SpendingGroup$OfMerchant;",
        "Lco/uk/getmondo/spending/data/SpendingGroup$OfPeer;",
        "Lco/uk/getmondo/spending/data/SpendingGroup$OfCategory;",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/d/b/i;)V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0}, Lco/uk/getmondo/spending/a/h;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract a()Lco/uk/getmondo/spending/a/g;
.end method
