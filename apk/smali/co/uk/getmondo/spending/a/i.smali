.class public Lco/uk/getmondo/spending/a/i;
.super Ljava/lang/Object;
.source "SpendingRepository.java"


# instance fields
.field private a:Lco/uk/getmondo/transaction/a/j;


# direct methods
.method public constructor <init>(Lco/uk/getmondo/transaction/a/j;)V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    iput-object p1, p0, Lco/uk/getmondo/spending/a/i;->a:Lco/uk/getmondo/transaction/a/j;

    .line 34
    return-void
.end method

.method static synthetic a(Lorg/threeten/bp/YearMonth;Lorg/threeten/bp/YearMonth;)Landroid/util/Pair;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    const/16 v3, 0x3b

    .line 38
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lorg/threeten/bp/YearMonth;->c(I)Lorg/threeten/bp/LocalDate;

    move-result-object v0

    .line 39
    invoke-virtual {v0}, Lorg/threeten/bp/LocalDate;->m()Lorg/threeten/bp/LocalDateTime;

    move-result-object v0

    .line 40
    invoke-virtual {p1}, Lorg/threeten/bp/YearMonth;->f()Lorg/threeten/bp/LocalDate;

    move-result-object v1

    const/16 v2, 0x17

    .line 41
    invoke-virtual {v1, v2, v3}, Lorg/threeten/bp/LocalDate;->b(II)Lorg/threeten/bp/LocalDateTime;

    move-result-object v1

    .line 42
    invoke-virtual {v1, v3}, Lorg/threeten/bp/LocalDateTime;->a(I)Lorg/threeten/bp/LocalDateTime;

    move-result-object v1

    const v2, 0x3b9ac9ff

    .line 43
    invoke-virtual {v1, v2}, Lorg/threeten/bp/LocalDateTime;->b(I)Lorg/threeten/bp/LocalDateTime;

    move-result-object v1

    .line 44
    new-instance v2, Landroid/util/Pair;

    invoke-static {v0}, Lco/uk/getmondo/common/c/a;->a(Lorg/threeten/bp/LocalDateTime;)Ljava/util/Date;

    move-result-object v0

    invoke-static {v1}, Lco/uk/getmondo/common/c/a;->a(Lorg/threeten/bp/LocalDateTime;)Ljava/util/Date;

    move-result-object v1

    invoke-direct {v2, v0, v1}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    return-object v2
.end method

.method static synthetic a(Lco/uk/getmondo/d/h;Landroid/util/Pair;)Lio/reactivex/r;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 80
    invoke-static {p1, p0}, Lco/uk/getmondo/spending/a/s;->a(Landroid/util/Pair;Lco/uk/getmondo/d/h;)Lkotlin/d/a/b;

    move-result-object v0

    invoke-static {v0}, Lco/uk/getmondo/common/j/g;->a(Lkotlin/d/a/b;)Lio/reactivex/n;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lco/uk/getmondo/d/h;Ljava/lang/String;Ljava/lang/String;Landroid/util/Pair;)Lio/reactivex/r;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 51
    invoke-static {p3, p0, p1, p2}, Lco/uk/getmondo/spending/a/k;->a(Landroid/util/Pair;Lco/uk/getmondo/d/h;Ljava/lang/String;Ljava/lang/String;)Lkotlin/d/a/b;

    move-result-object v0

    invoke-static {v0}, Lco/uk/getmondo/common/j/g;->a(Lkotlin/d/a/b;)Lio/reactivex/n;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Landroid/util/Pair;Lco/uk/getmondo/d/h;Lio/realm/av;)Lio/realm/bg;
    .locals 3

    .prologue
    .line 80
    const-class v0, Lco/uk/getmondo/d/aj;

    invoke-virtual {p2, v0}, Lio/realm/av;->a(Ljava/lang/Class;)Lio/realm/bf;

    move-result-object v1

    const-string v2, "created"

    iget-object v0, p0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/util/Date;

    .line 81
    invoke-virtual {v1, v2, v0}, Lio/realm/bf;->a(Ljava/lang/String;Ljava/util/Date;)Lio/realm/bf;

    move-result-object v1

    const-string v2, "created"

    iget-object v0, p0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Ljava/util/Date;

    .line 82
    invoke-virtual {v1, v2, v0}, Lio/realm/bf;->b(Ljava/lang/String;Ljava/util/Date;)Lio/realm/bf;

    move-result-object v0

    const-string v1, "category"

    .line 83
    invoke-virtual {p1}, Lco/uk/getmondo/d/h;->f()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lio/realm/bf;->a(Ljava/lang/String;Ljava/lang/String;)Lio/realm/bf;

    move-result-object v0

    const-string v1, "includeInSpending"

    const/4 v2, 0x1

    .line 84
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lio/realm/bf;->a(Ljava/lang/String;Ljava/lang/Boolean;)Lio/realm/bf;

    move-result-object v0

    .line 85
    invoke-virtual {v0}, Lio/realm/bf;->g()Lio/realm/bg;

    move-result-object v0

    .line 80
    return-object v0
.end method

.method static synthetic a(Landroid/util/Pair;Lco/uk/getmondo/d/h;Ljava/lang/String;Ljava/lang/String;Lio/realm/av;)Lio/realm/bg;
    .locals 3

    .prologue
    .line 52
    const-class v0, Lco/uk/getmondo/d/m;

    invoke-virtual {p4, v0}, Lio/realm/av;->a(Ljava/lang/Class;)Lio/realm/bf;

    move-result-object v0

    const-string v1, "transaction"

    .line 53
    invoke-virtual {v0, v1}, Lio/realm/bf;->b(Ljava/lang/String;)Lio/realm/bf;

    move-result-object v1

    const-string v2, "transaction.created"

    iget-object v0, p0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/util/Date;

    .line 54
    invoke-virtual {v1, v2, v0}, Lio/realm/bf;->a(Ljava/lang/String;Ljava/util/Date;)Lio/realm/bf;

    move-result-object v1

    const-string v2, "transaction.created"

    iget-object v0, p0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Ljava/util/Date;

    .line 55
    invoke-virtual {v1, v2, v0}, Lio/realm/bf;->b(Ljava/lang/String;Ljava/util/Date;)Lio/realm/bf;

    move-result-object v0

    const-string v1, "transaction.includeInSpending"

    const/4 v2, 0x1

    .line 56
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lio/realm/bf;->a(Ljava/lang/String;Ljava/lang/Boolean;)Lio/realm/bf;

    move-result-object v0

    const-string v1, "transaction.category"

    .line 57
    invoke-virtual {p1}, Lco/uk/getmondo/d/h;->f()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lio/realm/bf;->a(Ljava/lang/String;Ljava/lang/String;)Lio/realm/bf;

    move-result-object v0

    .line 59
    if-eqz p2, :cond_0

    .line 60
    const-string v1, "transaction.merchant.groupId"

    invoke-virtual {v0, v1, p2}, Lio/realm/bf;->a(Ljava/lang/String;Ljava/lang/String;)Lio/realm/bf;

    move-result-object v0

    .line 62
    :cond_0
    if-eqz p3, :cond_1

    .line 63
    const-string v1, "transaction.peer.userId"

    invoke-virtual {v0, v1, p3}, Lio/realm/bf;->a(Ljava/lang/String;Ljava/lang/String;)Lio/realm/bf;

    move-result-object v0

    .line 65
    :cond_1
    const-string v1, "created"

    sget-object v2, Lio/realm/bl;->b:Lio/realm/bl;

    invoke-virtual {v0, v1, v2}, Lio/realm/bf;->a(Ljava/lang/String;Lio/realm/bl;)Lio/realm/bg;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lio/realm/av;)Lio/realm/bg;
    .locals 3

    .prologue
    .line 72
    const-class v0, Lco/uk/getmondo/d/aj;

    invoke-virtual {p0, v0}, Lio/realm/av;->a(Ljava/lang/Class;)Lio/realm/bf;

    move-result-object v0

    const-string v1, "includeInSpending"

    const/4 v2, 0x1

    .line 73
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lio/realm/bf;->a(Ljava/lang/String;Ljava/lang/Boolean;)Lio/realm/bf;

    move-result-object v0

    .line 74
    invoke-virtual {v0}, Lio/realm/bf;->g()Lio/realm/bg;

    move-result-object v0

    .line 72
    return-object v0
.end method

.method static synthetic a(Lco/uk/getmondo/d/aj;)Lorg/threeten/bp/LocalDate;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 91
    invoke-virtual {p0}, Lco/uk/getmondo/d/aj;->v()Ljava/util/Date;

    move-result-object v0

    const-string v1, "UTC"

    invoke-static {v1}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v1

    invoke-static {v0, v1}, Lco/uk/getmondo/common/c/a;->a(Ljava/util/Date;Ljava/util/TimeZone;)Lorg/threeten/bp/LocalDateTime;

    move-result-object v0

    invoke-virtual {v0}, Lorg/threeten/bp/LocalDateTime;->j()Lorg/threeten/bp/LocalDate;

    move-result-object v0

    return-object v0
.end method

.method private static b(Lorg/threeten/bp/YearMonth;Lorg/threeten/bp/YearMonth;)Lio/reactivex/v;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/threeten/bp/YearMonth;",
            "Lorg/threeten/bp/YearMonth;",
            ")",
            "Lio/reactivex/v",
            "<",
            "Landroid/util/Pair",
            "<",
            "Ljava/util/Date;",
            "Ljava/util/Date;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 37
    invoke-static {p0, p1}, Lco/uk/getmondo/spending/a/j;->a(Lorg/threeten/bp/YearMonth;Lorg/threeten/bp/YearMonth;)Ljava/util/concurrent/Callable;

    move-result-object v0

    invoke-static {v0}, Lio/reactivex/v;->c(Ljava/util/concurrent/Callable;)Lio/reactivex/v;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public a()Lio/reactivex/n;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Ljava/util/List",
            "<",
            "Lco/uk/getmondo/d/aj;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 72
    invoke-static {}, Lco/uk/getmondo/spending/a/n;->a()Lkotlin/d/a/b;

    move-result-object v0

    invoke-static {v0}, Lco/uk/getmondo/common/j/g;->a(Lkotlin/d/a/b;)Lio/reactivex/n;

    move-result-object v0

    invoke-static {}, Lco/uk/getmondo/spending/a/o;->a()Lio/reactivex/c/h;

    move-result-object v1

    .line 75
    invoke-virtual {v0, v1}, Lio/reactivex/n;->map(Lio/reactivex/c/h;)Lio/reactivex/n;

    move-result-object v0

    .line 72
    return-object v0
.end method

.method public a(Lorg/threeten/bp/YearMonth;Lco/uk/getmondo/d/h;)Lio/reactivex/n;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/threeten/bp/YearMonth;",
            "Lco/uk/getmondo/d/h;",
            ")",
            "Lio/reactivex/n",
            "<",
            "Ljava/util/List",
            "<",
            "Lco/uk/getmondo/d/aj;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 79
    invoke-static {p1, p1}, Lco/uk/getmondo/spending/a/i;->b(Lorg/threeten/bp/YearMonth;Lorg/threeten/bp/YearMonth;)Lio/reactivex/v;

    move-result-object v0

    invoke-static {p2}, Lco/uk/getmondo/spending/a/p;->a(Lco/uk/getmondo/d/h;)Lio/reactivex/c/h;

    move-result-object v1

    .line 80
    invoke-virtual {v0, v1}, Lio/reactivex/v;->b(Lio/reactivex/c/h;)Lio/reactivex/n;

    move-result-object v0

    invoke-static {}, Lco/uk/getmondo/spending/a/q;->a()Lio/reactivex/c/h;

    move-result-object v1

    .line 86
    invoke-virtual {v0, v1}, Lio/reactivex/n;->map(Lio/reactivex/c/h;)Lio/reactivex/n;

    move-result-object v0

    .line 79
    return-object v0
.end method

.method public a(Lorg/threeten/bp/YearMonth;Lco/uk/getmondo/d/h;Ljava/lang/String;Ljava/lang/String;)Lio/reactivex/n;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/threeten/bp/YearMonth;",
            "Lco/uk/getmondo/d/h;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/n",
            "<",
            "Ljava/util/List",
            "<",
            "Lco/uk/getmondo/d/m;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 50
    invoke-static {p1, p1}, Lco/uk/getmondo/spending/a/i;->b(Lorg/threeten/bp/YearMonth;Lorg/threeten/bp/YearMonth;)Lio/reactivex/v;

    move-result-object v0

    invoke-static {p2, p3, p4}, Lco/uk/getmondo/spending/a/l;->a(Lco/uk/getmondo/d/h;Ljava/lang/String;Ljava/lang/String;)Lio/reactivex/c/h;

    move-result-object v1

    .line 51
    invoke-virtual {v0, v1}, Lio/reactivex/v;->b(Lio/reactivex/c/h;)Lio/reactivex/n;

    move-result-object v0

    invoke-static {}, Lco/uk/getmondo/spending/a/m;->a()Lio/reactivex/c/h;

    move-result-object v1

    .line 68
    invoke-virtual {v0, v1}, Lio/reactivex/n;->map(Lio/reactivex/c/h;)Lio/reactivex/n;

    move-result-object v0

    .line 50
    return-object v0
.end method

.method public a(Lorg/threeten/bp/LocalDate;Lorg/threeten/bp/LocalDate;)Lio/reactivex/v;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/threeten/bp/LocalDate;",
            "Lorg/threeten/bp/LocalDate;",
            ")",
            "Lio/reactivex/v",
            "<",
            "Ljava/util/List",
            "<",
            "Lco/uk/getmondo/d/aj;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 95
    iget-object v0, p0, Lco/uk/getmondo/spending/a/i;->a:Lco/uk/getmondo/transaction/a/j;

    invoke-virtual {p1}, Lorg/threeten/bp/LocalDate;->m()Lorg/threeten/bp/LocalDateTime;

    move-result-object v1

    invoke-virtual {p2}, Lorg/threeten/bp/LocalDate;->m()Lorg/threeten/bp/LocalDateTime;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lco/uk/getmondo/transaction/a/j;->a(Lorg/threeten/bp/LocalDateTime;Lorg/threeten/bp/LocalDateTime;)Lio/reactivex/v;

    move-result-object v0

    return-object v0
.end method

.method public b()Lio/reactivex/v;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/v",
            "<",
            "Lorg/threeten/bp/LocalDate;",
            ">;"
        }
    .end annotation

    .prologue
    .line 90
    iget-object v0, p0, Lco/uk/getmondo/spending/a/i;->a:Lco/uk/getmondo/transaction/a/j;

    invoke-virtual {v0}, Lco/uk/getmondo/transaction/a/j;->c()Lio/reactivex/v;

    move-result-object v0

    invoke-static {}, Lco/uk/getmondo/spending/a/r;->a()Lio/reactivex/c/h;

    move-result-object v1

    .line 91
    invoke-virtual {v0, v1}, Lio/reactivex/v;->d(Lio/reactivex/c/h;)Lio/reactivex/v;

    move-result-object v0

    .line 90
    return-object v0
.end method
