.class final synthetic Lco/uk/getmondo/spending/a/j;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/util/concurrent/Callable;


# instance fields
.field private final a:Lorg/threeten/bp/YearMonth;

.field private final b:Lorg/threeten/bp/YearMonth;


# direct methods
.method private constructor <init>(Lorg/threeten/bp/YearMonth;Lorg/threeten/bp/YearMonth;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lco/uk/getmondo/spending/a/j;->a:Lorg/threeten/bp/YearMonth;

    iput-object p2, p0, Lco/uk/getmondo/spending/a/j;->b:Lorg/threeten/bp/YearMonth;

    return-void
.end method

.method public static a(Lorg/threeten/bp/YearMonth;Lorg/threeten/bp/YearMonth;)Ljava/util/concurrent/Callable;
    .locals 1

    new-instance v0, Lco/uk/getmondo/spending/a/j;

    invoke-direct {v0, p0, p1}, Lco/uk/getmondo/spending/a/j;-><init>(Lorg/threeten/bp/YearMonth;Lorg/threeten/bp/YearMonth;)V

    return-object v0
.end method


# virtual methods
.method public call()Ljava/lang/Object;
    .locals 2

    iget-object v0, p0, Lco/uk/getmondo/spending/a/j;->a:Lorg/threeten/bp/YearMonth;

    iget-object v1, p0, Lco/uk/getmondo/spending/a/j;->b:Lorg/threeten/bp/YearMonth;

    invoke-static {v0, v1}, Lco/uk/getmondo/spending/a/i;->a(Lorg/threeten/bp/YearMonth;Lorg/threeten/bp/YearMonth;)Landroid/util/Pair;

    move-result-object v0

    return-object v0
.end method
