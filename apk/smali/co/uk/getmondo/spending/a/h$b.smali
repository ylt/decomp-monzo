.class public final Lco/uk/getmondo/spending/a/h$b;
.super Lco/uk/getmondo/spending/a/h;
.source "SpendingGroup.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lco/uk/getmondo/spending/a/h;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "b"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u00000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\t\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u000e\n\u0000\u0008\u0086\u0008\u0018\u00002\u00020\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\t\u0010\u000b\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u000c\u001a\u00020\u0005H\u00c6\u0003J\u001d\u0010\r\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u0005H\u00c6\u0001J\u0013\u0010\u000e\u001a\u00020\u000f2\u0008\u0010\u0010\u001a\u0004\u0018\u00010\u0011H\u00d6\u0003J\t\u0010\u0012\u001a\u00020\u0013H\u00d6\u0001J\t\u0010\u0014\u001a\u00020\u0015H\u00d6\u0001R\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0007\u0010\u0008R\u0014\u0010\u0002\u001a\u00020\u0003X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\t\u0010\n\u00a8\u0006\u0016"
    }
    d2 = {
        "Lco/uk/getmondo/spending/data/SpendingGroup$OfMerchant;",
        "Lco/uk/getmondo/spending/data/SpendingGroup;",
        "spendingData",
        "Lco/uk/getmondo/spending/data/SpendingData;",
        "merchant",
        "Lco/uk/getmondo/model/Merchant;",
        "(Lco/uk/getmondo/spending/data/SpendingData;Lco/uk/getmondo/model/Merchant;)V",
        "getMerchant",
        "()Lco/uk/getmondo/model/Merchant;",
        "getSpendingData",
        "()Lco/uk/getmondo/spending/data/SpendingData;",
        "component1",
        "component2",
        "copy",
        "equals",
        "",
        "other",
        "",
        "hashCode",
        "",
        "toString",
        "",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field private final a:Lco/uk/getmondo/spending/a/g;

.field private final b:Lco/uk/getmondo/d/u;


# direct methods
.method public constructor <init>(Lco/uk/getmondo/spending/a/g;Lco/uk/getmondo/d/u;)V
    .locals 1

    .prologue
    const-string v0, "spendingData"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "merchant"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 20
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lco/uk/getmondo/spending/a/h;-><init>(Lkotlin/d/b/i;)V

    iput-object p1, p0, Lco/uk/getmondo/spending/a/h$b;->a:Lco/uk/getmondo/spending/a/g;

    iput-object p2, p0, Lco/uk/getmondo/spending/a/h$b;->b:Lco/uk/getmondo/d/u;

    return-void
.end method


# virtual methods
.method public a()Lco/uk/getmondo/spending/a/g;
    .locals 1

    .prologue
    .line 20
    iget-object v0, p0, Lco/uk/getmondo/spending/a/h$b;->a:Lco/uk/getmondo/spending/a/g;

    return-object v0
.end method

.method public final b()Lco/uk/getmondo/d/u;
    .locals 1

    .prologue
    .line 20
    iget-object v0, p0, Lco/uk/getmondo/spending/a/h$b;->b:Lco/uk/getmondo/d/u;

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_0

    instance-of v0, p1, Lco/uk/getmondo/spending/a/h$b;

    if-eqz v0, :cond_1

    check-cast p1, Lco/uk/getmondo/spending/a/h$b;

    invoke-virtual {p0}, Lco/uk/getmondo/spending/a/h$b;->a()Lco/uk/getmondo/spending/a/g;

    move-result-object v0

    invoke-virtual {p1}, Lco/uk/getmondo/spending/a/h$b;->a()Lco/uk/getmondo/spending/a/g;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lco/uk/getmondo/spending/a/h$b;->b:Lco/uk/getmondo/d/u;

    iget-object v1, p1, Lco/uk/getmondo/spending/a/h$b;->b:Lco/uk/getmondo/d/u;

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    const/4 v1, 0x0

    invoke-virtual {p0}, Lco/uk/getmondo/spending/a/h$b;->a()Lco/uk/getmondo/spending/a/g;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lco/uk/getmondo/spending/a/h$b;->b:Lco/uk/getmondo/d/u;

    if-eqz v2, :cond_0

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_0
    add-int/2addr v0, v1

    return v0

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "OfMerchant(spendingData="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lco/uk/getmondo/spending/a/h$b;->a()Lco/uk/getmondo/spending/a/g;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", merchant="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/spending/a/h$b;->b:Lco/uk/getmondo/d/u;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
