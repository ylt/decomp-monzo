.class public abstract enum Lco/uk/getmondo/spending/a/a;
.super Ljava/lang/Enum;
.source "GroupBy.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lco/uk/getmondo/spending/a/a;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lco/uk/getmondo/spending/a/a;

.field public static final enum b:Lco/uk/getmondo/spending/a/a;

.field private static final synthetic c:[Lco/uk/getmondo/spending/a/a;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 11
    new-instance v0, Lco/uk/getmondo/spending/a/a$1;

    const-string v1, "MERCHANT_OR_PEER_GROUP"

    invoke-direct {v0, v1, v2}, Lco/uk/getmondo/spending/a/a$1;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lco/uk/getmondo/spending/a/a;->a:Lco/uk/getmondo/spending/a/a;

    .line 32
    new-instance v0, Lco/uk/getmondo/spending/a/a$2;

    const-string v1, "CATEGORY"

    invoke-direct {v0, v1, v3}, Lco/uk/getmondo/spending/a/a$2;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lco/uk/getmondo/spending/a/a;->b:Lco/uk/getmondo/spending/a/a;

    .line 10
    const/4 v0, 0x2

    new-array v0, v0, [Lco/uk/getmondo/spending/a/a;

    sget-object v1, Lco/uk/getmondo/spending/a/a;->a:Lco/uk/getmondo/spending/a/a;

    aput-object v1, v0, v2

    sget-object v1, Lco/uk/getmondo/spending/a/a;->b:Lco/uk/getmondo/spending/a/a;

    aput-object v1, v0, v3

    sput-object v0, Lco/uk/getmondo/spending/a/a;->c:[Lco/uk/getmondo/spending/a/a;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 10
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;ILco/uk/getmondo/spending/a/a$1;)V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0, p1, p2}, Lco/uk/getmondo/spending/a/a;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lco/uk/getmondo/spending/a/a;
    .locals 1

    .prologue
    .line 10
    const-class v0, Lco/uk/getmondo/spending/a/a;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/spending/a/a;

    return-object v0
.end method

.method public static values()[Lco/uk/getmondo/spending/a/a;
    .locals 1

    .prologue
    .line 10
    sget-object v0, Lco/uk/getmondo/spending/a/a;->c:[Lco/uk/getmondo/spending/a/a;

    invoke-virtual {v0}, [Lco/uk/getmondo/spending/a/a;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lco/uk/getmondo/spending/a/a;

    return-object v0
.end method


# virtual methods
.method abstract a(Lco/uk/getmondo/spending/a/g;Lco/uk/getmondo/d/aj;)Lco/uk/getmondo/spending/a/h;
.end method

.method abstract a(Lco/uk/getmondo/d/aj;)Ljava/lang/Object;
.end method
