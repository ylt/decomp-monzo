.class public Lco/uk/getmondo/spending/a/b;
.super Ljava/lang/Object;
.source "SpendingCalculator.java"


# static fields
.field private static final a:Lco/uk/getmondo/common/i/c;

.field private static final b:Ljava/util/TimeZone;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 26
    sget-object v0, Lco/uk/getmondo/common/i/c;->a:Lco/uk/getmondo/common/i/c;

    sput-object v0, Lco/uk/getmondo/spending/a/b;->a:Lco/uk/getmondo/common/i/c;

    .line 27
    const-string v0, "UTC"

    invoke-static {v0}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v0

    sput-object v0, Lco/uk/getmondo/spending/a/b;->b:Ljava/util/TimeZone;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    return-void
.end method

.method static synthetic a(Lco/uk/getmondo/d/aj;Lco/uk/getmondo/d/aj;)I
    .locals 2

    .prologue
    .line 68
    invoke-virtual {p0}, Lco/uk/getmondo/d/aj;->v()Ljava/util/Date;

    move-result-object v0

    invoke-virtual {p1}, Lco/uk/getmondo/d/aj;->v()Ljava/util/Date;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/Date;->compareTo(Ljava/util/Date;)I

    move-result v0

    return v0
.end method

.method static synthetic a(Lco/uk/getmondo/spending/a/h;Lco/uk/getmondo/spending/a/h;)I
    .locals 4

    .prologue
    .line 47
    invoke-virtual {p0}, Lco/uk/getmondo/spending/a/h;->a()Lco/uk/getmondo/spending/a/g;

    move-result-object v0

    invoke-virtual {v0}, Lco/uk/getmondo/spending/a/g;->a()Lco/uk/getmondo/d/c;

    move-result-object v0

    invoke-virtual {v0}, Lco/uk/getmondo/d/c;->k()J

    move-result-wide v0

    .line 48
    invoke-virtual {p1}, Lco/uk/getmondo/spending/a/h;->a()Lco/uk/getmondo/spending/a/g;

    move-result-object v2

    invoke-virtual {v2}, Lco/uk/getmondo/spending/a/g;->a()Lco/uk/getmondo/d/c;

    move-result-object v2

    invoke-virtual {v2}, Lco/uk/getmondo/d/c;->k()J

    move-result-wide v2

    .line 50
    invoke-static {v2, v3, v0, v1}, Ljava/lang/Long;->compare(JJ)I

    move-result v0

    mul-int/lit8 v0, v0, -0x1

    return v0
.end method

.method private static a(Ljava/util/Map;Ljava/util/Map;Lco/uk/getmondo/spending/a/a;Lco/uk/getmondo/common/i/a;)Lco/uk/getmondo/spending/a/g;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Object;",
            "Lco/uk/getmondo/common/i/a;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Object;",
            "Lco/uk/getmondo/d/aj;",
            ">;",
            "Lco/uk/getmondo/spending/a/a;",
            "Lco/uk/getmondo/common/i/a;",
            ")",
            "Lco/uk/getmondo/spending/a/g;"
        }
    .end annotation

    .prologue
    .line 37
    new-instance v1, Ljava/util/ArrayList;

    invoke-interface {p0}, Ljava/util/Map;->size()I

    move-result v0

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 38
    invoke-interface {p0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    .line 39
    invoke-interface {p0, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/common/i/a;

    .line 41
    new-instance v4, Lco/uk/getmondo/spending/a/g;

    invoke-virtual {v0}, Lco/uk/getmondo/common/i/a;->a()Lco/uk/getmondo/d/c;

    move-result-object v5

    invoke-virtual {v0}, Lco/uk/getmondo/common/i/a;->b()I

    move-result v0

    invoke-direct {v4, v5, v0}, Lco/uk/getmondo/spending/a/g;-><init>(Lco/uk/getmondo/d/c;I)V

    .line 43
    invoke-interface {p1, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/d/aj;

    invoke-virtual {p2, v4, v0}, Lco/uk/getmondo/spending/a/a;->a(Lco/uk/getmondo/spending/a/g;Lco/uk/getmondo/d/aj;)Lco/uk/getmondo/spending/a/h;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 46
    :cond_0
    invoke-static {}, Lco/uk/getmondo/spending/a/c;->a()Ljava/util/Comparator;

    move-result-object v0

    invoke-static {v1, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 53
    new-instance v0, Lco/uk/getmondo/spending/a/g;

    invoke-virtual {p3}, Lco/uk/getmondo/common/i/a;->a()Lco/uk/getmondo/d/c;

    move-result-object v2

    invoke-virtual {p3}, Lco/uk/getmondo/common/i/a;->b()I

    move-result v3

    invoke-direct {v0, v2, v3, v1}, Lco/uk/getmondo/spending/a/g;-><init>(Lco/uk/getmondo/d/c;ILjava/util/List;)V

    return-object v0
.end method

.method static synthetic a(Ljava/util/List;Lco/uk/getmondo/spending/a/a;Lio/reactivex/o;)V
    .locals 10
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 62
    invoke-static {p0}, Lco/uk/getmondo/common/k/b;->a(Ljava/util/Collection;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 63
    invoke-interface {p2}, Lio/reactivex/o;->a()V

    .line 114
    :goto_0
    return-void

    .line 68
    :cond_0
    invoke-static {}, Lco/uk/getmondo/spending/a/e;->a()Ljava/util/Comparator;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Collections;->reverseOrder(Ljava/util/Comparator;)Ljava/util/Comparator;

    move-result-object v0

    invoke-static {p0, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 70
    new-instance v6, Lco/uk/getmondo/common/i/a;

    sget-object v0, Lco/uk/getmondo/spending/a/b;->a:Lco/uk/getmondo/common/i/c;

    invoke-direct {v6, v0}, Lco/uk/getmondo/common/i/a;-><init>(Lco/uk/getmondo/common/i/c;)V

    .line 71
    new-instance v7, Ljava/util/HashMap;

    invoke-direct {v7}, Ljava/util/HashMap;-><init>()V

    .line 72
    new-instance v8, Ljava/util/HashMap;

    invoke-direct {v8}, Ljava/util/HashMap;-><init>()V

    .line 75
    invoke-interface {p0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/d/aj;

    invoke-virtual {v0}, Lco/uk/getmondo/d/aj;->v()Ljava/util/Date;

    move-result-object v0

    sget-object v1, Lco/uk/getmondo/spending/a/b;->b:Ljava/util/TimeZone;

    invoke-static {v0, v1}, Lco/uk/getmondo/common/c/a;->b(Ljava/util/Date;Ljava/util/TimeZone;)Lorg/threeten/bp/YearMonth;

    move-result-object v0

    move v2, v3

    move-object v4, v0

    .line 76
    :goto_1
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_6

    .line 77
    invoke-interface {p0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/d/aj;

    .line 78
    invoke-virtual {p1, v0}, Lco/uk/getmondo/spending/a/a;->a(Lco/uk/getmondo/d/aj;)Ljava/lang/Object;

    move-result-object v5

    .line 82
    if-eqz v5, :cond_2

    .line 83
    invoke-interface {v7, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lco/uk/getmondo/common/i/a;

    .line 85
    if-nez v1, :cond_1

    .line 86
    new-instance v1, Lco/uk/getmondo/common/i/a;

    sget-object v9, Lco/uk/getmondo/spending/a/b;->a:Lco/uk/getmondo/common/i/c;

    invoke-direct {v1, v9}, Lco/uk/getmondo/common/i/a;-><init>(Lco/uk/getmondo/common/i/c;)V

    .line 87
    invoke-interface {v7, v5, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 90
    :cond_1
    invoke-virtual {v0}, Lco/uk/getmondo/d/aj;->g()Lco/uk/getmondo/d/c;

    move-result-object v9

    invoke-virtual {v6, v9}, Lco/uk/getmondo/common/i/a;->a(Lco/uk/getmondo/d/c;)V

    .line 91
    invoke-virtual {v0}, Lco/uk/getmondo/d/aj;->g()Lco/uk/getmondo/d/c;

    move-result-object v9

    invoke-virtual {v1, v9}, Lco/uk/getmondo/common/i/a;->a(Lco/uk/getmondo/d/c;)V

    .line 93
    invoke-interface {v8, v5}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 94
    invoke-interface {v8, v5, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 100
    :cond_2
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    if-ne v2, v0, :cond_4

    const/4 v0, 0x1

    move v5, v0

    .line 101
    :goto_2
    if-eqz v5, :cond_5

    const/4 v1, 0x0

    .line 102
    :goto_3
    if-eqz v1, :cond_3

    invoke-virtual {v4, v1}, Lorg/threeten/bp/YearMonth;->b(Lorg/threeten/bp/YearMonth;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 103
    :cond_3
    invoke-static {v7, v8, p1, v6}, Lco/uk/getmondo/spending/a/b;->a(Ljava/util/Map;Ljava/util/Map;Lco/uk/getmondo/spending/a/a;Lco/uk/getmondo/common/i/a;)Lco/uk/getmondo/spending/a/g;

    move-result-object v0

    .line 104
    new-instance v9, Landroid/support/v4/g/j;

    invoke-direct {v9, v4, v0}, Landroid/support/v4/g/j;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-interface {p2, v9}, Lio/reactivex/o;->a(Ljava/lang/Object;)V

    .line 105
    if-nez v5, :cond_7

    .line 106
    invoke-interface {v7}, Ljava/util/Map;->clear()V

    .line 107
    invoke-interface {v8}, Ljava/util/Map;->clear()V

    .line 108
    invoke-virtual {v6}, Lco/uk/getmondo/common/i/a;->c()V

    .line 76
    :goto_4
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    move-object v4, v1

    goto :goto_1

    :cond_4
    move v5, v3

    .line 100
    goto :goto_2

    .line 101
    :cond_5
    add-int/lit8 v0, v2, 0x1

    invoke-interface {p0, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/d/aj;

    invoke-virtual {v0}, Lco/uk/getmondo/d/aj;->v()Ljava/util/Date;

    move-result-object v0

    sget-object v1, Lco/uk/getmondo/spending/a/b;->b:Ljava/util/TimeZone;

    invoke-static {v0, v1}, Lco/uk/getmondo/common/c/a;->b(Ljava/util/Date;Ljava/util/TimeZone;)Lorg/threeten/bp/YearMonth;

    move-result-object v1

    goto :goto_3

    .line 113
    :cond_6
    invoke-interface {p2}, Lio/reactivex/o;->a()V

    goto/16 :goto_0

    :cond_7
    move-object v1, v4

    goto :goto_4
.end method


# virtual methods
.method public a(Ljava/util/List;Lco/uk/getmondo/spending/a/a;)Lio/reactivex/n;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lco/uk/getmondo/d/aj;",
            ">;",
            "Lco/uk/getmondo/spending/a/a;",
            ")",
            "Lio/reactivex/n",
            "<",
            "Landroid/support/v4/g/j",
            "<",
            "Lorg/threeten/bp/YearMonth;",
            "Lco/uk/getmondo/spending/a/g;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 61
    invoke-static {p1, p2}, Lco/uk/getmondo/spending/a/d;->a(Ljava/util/List;Lco/uk/getmondo/spending/a/a;)Lio/reactivex/p;

    move-result-object v0

    invoke-static {v0}, Lio/reactivex/n;->create(Lio/reactivex/p;)Lio/reactivex/n;

    move-result-object v0

    return-object v0
.end method
