.class final synthetic Lco/uk/getmondo/spending/a/k;
.super Ljava/lang/Object;

# interfaces
.implements Lkotlin/d/a/b;


# instance fields
.field private final a:Landroid/util/Pair;

.field private final b:Lco/uk/getmondo/d/h;

.field private final c:Ljava/lang/String;

.field private final d:Ljava/lang/String;


# direct methods
.method private constructor <init>(Landroid/util/Pair;Lco/uk/getmondo/d/h;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lco/uk/getmondo/spending/a/k;->a:Landroid/util/Pair;

    iput-object p2, p0, Lco/uk/getmondo/spending/a/k;->b:Lco/uk/getmondo/d/h;

    iput-object p3, p0, Lco/uk/getmondo/spending/a/k;->c:Ljava/lang/String;

    iput-object p4, p0, Lco/uk/getmondo/spending/a/k;->d:Ljava/lang/String;

    return-void
.end method

.method public static a(Landroid/util/Pair;Lco/uk/getmondo/d/h;Ljava/lang/String;Ljava/lang/String;)Lkotlin/d/a/b;
    .locals 1

    new-instance v0, Lco/uk/getmondo/spending/a/k;

    invoke-direct {v0, p0, p1, p2, p3}, Lco/uk/getmondo/spending/a/k;-><init>(Landroid/util/Pair;Lco/uk/getmondo/d/h;Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method public a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4

    iget-object v0, p0, Lco/uk/getmondo/spending/a/k;->a:Landroid/util/Pair;

    iget-object v1, p0, Lco/uk/getmondo/spending/a/k;->b:Lco/uk/getmondo/d/h;

    iget-object v2, p0, Lco/uk/getmondo/spending/a/k;->c:Ljava/lang/String;

    iget-object v3, p0, Lco/uk/getmondo/spending/a/k;->d:Ljava/lang/String;

    check-cast p1, Lio/realm/av;

    invoke-static {v0, v1, v2, v3, p1}, Lco/uk/getmondo/spending/a/i;->a(Landroid/util/Pair;Lco/uk/getmondo/d/h;Ljava/lang/String;Ljava/lang/String;Lio/realm/av;)Lio/realm/bg;

    move-result-object v0

    return-object v0
.end method
