.class public Lco/uk/getmondo/spending/merchant/SpendingByMerchantFragment;
.super Lco/uk/getmondo/common/f/a;
.source "SpendingByMerchantFragment.java"

# interfaces
.implements Lco/uk/getmondo/spending/merchant/h$a;


# instance fields
.field a:Lco/uk/getmondo/spending/merchant/h;

.field private final c:Lcom/b/b/c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/b/b/c",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private d:Lbutterknife/Unbinder;

.field monthSpendingView:Lco/uk/getmondo/spending/MonthSpendingView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f110340
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 30
    invoke-direct {p0}, Lco/uk/getmondo/common/f/a;-><init>()V

    .line 34
    invoke-static {}, Lcom/b/b/c;->a()Lcom/b/b/c;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/spending/merchant/SpendingByMerchantFragment;->c:Lcom/b/b/c;

    return-void
.end method

.method public static a(Lorg/threeten/bp/YearMonth;Lco/uk/getmondo/d/h;)Lco/uk/getmondo/spending/merchant/SpendingByMerchantFragment;
    .locals 2

    .prologue
    .line 41
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 42
    const-string v1, "KEY_YEAR_MONTH"

    invoke-virtual {v0, v1, p0}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 43
    const-string v1, "KEY_CATEGORY"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 44
    new-instance v1, Lco/uk/getmondo/spending/merchant/SpendingByMerchantFragment;

    invoke-direct {v1}, Lco/uk/getmondo/spending/merchant/SpendingByMerchantFragment;-><init>()V

    .line 45
    invoke-virtual {v1, v0}, Lco/uk/getmondo/spending/merchant/SpendingByMerchantFragment;->setArguments(Landroid/os/Bundle;)V

    .line 46
    return-object v1
.end method

.method static synthetic a(Lco/uk/getmondo/spending/merchant/SpendingByMerchantFragment;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 88
    iget-object v0, p0, Lco/uk/getmondo/spending/merchant/SpendingByMerchantFragment;->monthSpendingView:Lco/uk/getmondo/spending/MonthSpendingView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lco/uk/getmondo/spending/MonthSpendingView;->setItemClickListener(Lco/uk/getmondo/spending/MonthSpendingView$a;)V

    return-void
.end method

.method static synthetic a(Lco/uk/getmondo/spending/merchant/SpendingByMerchantFragment;Lio/reactivex/o;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 86
    iget-object v0, p0, Lco/uk/getmondo/spending/merchant/SpendingByMerchantFragment;->monthSpendingView:Lco/uk/getmondo/spending/MonthSpendingView;

    invoke-static {p1}, Lco/uk/getmondo/spending/merchant/c;->a(Lio/reactivex/o;)Lco/uk/getmondo/spending/MonthSpendingView$a;

    move-result-object v1

    invoke-virtual {v0, v1}, Lco/uk/getmondo/spending/MonthSpendingView;->setItemClickListener(Lco/uk/getmondo/spending/MonthSpendingView$a;)V

    .line 88
    invoke-static {p0}, Lco/uk/getmondo/spending/merchant/d;->a(Lco/uk/getmondo/spending/merchant/SpendingByMerchantFragment;)Lio/reactivex/c/f;

    move-result-object v0

    invoke-interface {p1, v0}, Lio/reactivex/o;->a(Lio/reactivex/c/f;)V

    .line 89
    return-void
.end method

.method static synthetic a(Lio/reactivex/o;Lorg/threeten/bp/YearMonth;Lco/uk/getmondo/spending/a/h;)V
    .locals 0

    .prologue
    .line 86
    invoke-interface {p0, p2}, Lio/reactivex/o;->a(Ljava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public a()Lio/reactivex/n;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Lco/uk/getmondo/spending/a/h;",
            ">;"
        }
    .end annotation

    .prologue
    .line 85
    invoke-static {p0}, Lco/uk/getmondo/spending/merchant/b;->a(Lco/uk/getmondo/spending/merchant/SpendingByMerchantFragment;)Lio/reactivex/p;

    move-result-object v0

    invoke-static {v0}, Lio/reactivex/n;->create(Lio/reactivex/p;)Lio/reactivex/n;

    move-result-object v0

    return-object v0
.end method

.method public a(Lorg/threeten/bp/YearMonth;Lco/uk/getmondo/d/h;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 104
    invoke-virtual {p0}, Lco/uk/getmondo/spending/merchant/SpendingByMerchantFragment;->getActivity()Landroid/support/v4/app/j;

    move-result-object v0

    invoke-static {v0, p1, p2, p3, p4}, Lco/uk/getmondo/spending/transactions/SpendingTransactionsActivity;->a(Landroid/content/Context;Lorg/threeten/bp/YearMonth;Lco/uk/getmondo/d/h;Ljava/lang/String;Ljava/lang/String;)V

    .line 105
    return-void
.end method

.method public a(Lorg/threeten/bp/YearMonth;Lco/uk/getmondo/spending/a/g;)V
    .locals 1

    .prologue
    .line 99
    iget-object v0, p0, Lco/uk/getmondo/spending/merchant/SpendingByMerchantFragment;->monthSpendingView:Lco/uk/getmondo/spending/MonthSpendingView;

    invoke-virtual {v0, p1, p2}, Lco/uk/getmondo/spending/MonthSpendingView;->a(Lorg/threeten/bp/YearMonth;Lco/uk/getmondo/spending/a/g;)V

    .line 100
    return-void
.end method

.method public b()Lio/reactivex/n;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .prologue
    .line 94
    iget-object v0, p0, Lco/uk/getmondo/spending/merchant/SpendingByMerchantFragment;->c:Lcom/b/b/c;

    return-object v0
.end method

.method public b(Lorg/threeten/bp/YearMonth;Lco/uk/getmondo/d/h;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 109
    invoke-virtual {p0}, Lco/uk/getmondo/spending/merchant/SpendingByMerchantFragment;->getActivity()Landroid/support/v4/app/j;

    move-result-object v0

    invoke-static {v0, p1, p2, p3, p4}, Lco/uk/getmondo/spending/transactions/SpendingTransactionsActivity;->c(Landroid/content/Context;Lorg/threeten/bp/YearMonth;Lco/uk/getmondo/d/h;Ljava/lang/String;Ljava/lang/String;)V

    .line 110
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 51
    invoke-super {p0, p1}, Lco/uk/getmondo/common/f/a;->onCreate(Landroid/os/Bundle;)V

    .line 52
    invoke-virtual {p0}, Lco/uk/getmondo/spending/merchant/SpendingByMerchantFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "KEY_YEAR_MONTH"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lorg/threeten/bp/YearMonth;

    .line 53
    invoke-virtual {p0}, Lco/uk/getmondo/spending/merchant/SpendingByMerchantFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "KEY_CATEGORY"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v1

    check-cast v1, Lco/uk/getmondo/d/h;

    .line 54
    invoke-virtual {p0}, Lco/uk/getmondo/spending/merchant/SpendingByMerchantFragment;->B()Lco/uk/getmondo/common/h/b/b;

    move-result-object v2

    new-instance v3, Lco/uk/getmondo/spending/merchant/f;

    invoke-direct {v3, v0, v1}, Lco/uk/getmondo/spending/merchant/f;-><init>(Lorg/threeten/bp/YearMonth;Lco/uk/getmondo/d/h;)V

    invoke-interface {v2, v3}, Lco/uk/getmondo/common/h/b/b;->a(Lco/uk/getmondo/spending/merchant/f;)Lco/uk/getmondo/spending/merchant/a;

    move-result-object v0

    invoke-interface {v0, p0}, Lco/uk/getmondo/spending/merchant/a;->a(Lco/uk/getmondo/spending/merchant/SpendingByMerchantFragment;)V

    .line 55
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2

    .prologue
    .line 60
    const v0, 0x7f0500aa

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public onDestroyView()V
    .locals 1

    .prologue
    .line 78
    iget-object v0, p0, Lco/uk/getmondo/spending/merchant/SpendingByMerchantFragment;->a:Lco/uk/getmondo/spending/merchant/h;

    invoke-virtual {v0}, Lco/uk/getmondo/spending/merchant/h;->b()V

    .line 79
    iget-object v0, p0, Lco/uk/getmondo/spending/merchant/SpendingByMerchantFragment;->d:Lbutterknife/Unbinder;

    invoke-interface {v0}, Lbutterknife/Unbinder;->unbind()V

    .line 80
    invoke-super {p0}, Lco/uk/getmondo/common/f/a;->onDestroyView()V

    .line 81
    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 65
    invoke-super {p0, p1, p2}, Lco/uk/getmondo/common/f/a;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 66
    invoke-static {p0, p1}, Lbutterknife/ButterKnife;->bind(Ljava/lang/Object;Landroid/view/View;)Lbutterknife/Unbinder;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/spending/merchant/SpendingByMerchantFragment;->d:Lbutterknife/Unbinder;

    .line 67
    iget-object v0, p0, Lco/uk/getmondo/spending/merchant/SpendingByMerchantFragment;->a:Lco/uk/getmondo/spending/merchant/h;

    invoke-virtual {v0, p0}, Lco/uk/getmondo/spending/merchant/h;->a(Lco/uk/getmondo/spending/merchant/h$a;)V

    .line 68
    return-void
.end method

.method public setUserVisibleHint(Z)V
    .locals 2

    .prologue
    .line 72
    invoke-super {p0, p1}, Lco/uk/getmondo/common/f/a;->setUserVisibleHint(Z)V

    .line 73
    iget-object v0, p0, Lco/uk/getmondo/spending/merchant/SpendingByMerchantFragment;->c:Lcom/b/b/c;

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/b/b/c;->a(Ljava/lang/Object;)V

    .line 74
    return-void
.end method
