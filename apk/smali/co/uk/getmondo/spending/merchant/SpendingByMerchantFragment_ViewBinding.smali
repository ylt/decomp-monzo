.class public Lco/uk/getmondo/spending/merchant/SpendingByMerchantFragment_ViewBinding;
.super Ljava/lang/Object;
.source "SpendingByMerchantFragment_ViewBinding.java"

# interfaces
.implements Lbutterknife/Unbinder;


# instance fields
.field private a:Lco/uk/getmondo/spending/merchant/SpendingByMerchantFragment;


# direct methods
.method public constructor <init>(Lco/uk/getmondo/spending/merchant/SpendingByMerchantFragment;Landroid/view/View;)V
    .locals 3

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    iput-object p1, p0, Lco/uk/getmondo/spending/merchant/SpendingByMerchantFragment_ViewBinding;->a:Lco/uk/getmondo/spending/merchant/SpendingByMerchantFragment;

    .line 21
    const v0, 0x7f110340

    const-string v1, "field \'monthSpendingView\'"

    const-class v2, Lco/uk/getmondo/spending/MonthSpendingView;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/spending/MonthSpendingView;

    iput-object v0, p1, Lco/uk/getmondo/spending/merchant/SpendingByMerchantFragment;->monthSpendingView:Lco/uk/getmondo/spending/MonthSpendingView;

    .line 22
    return-void
.end method


# virtual methods
.method public unbind()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 27
    iget-object v0, p0, Lco/uk/getmondo/spending/merchant/SpendingByMerchantFragment_ViewBinding;->a:Lco/uk/getmondo/spending/merchant/SpendingByMerchantFragment;

    .line 28
    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Bindings already cleared."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 29
    :cond_0
    iput-object v1, p0, Lco/uk/getmondo/spending/merchant/SpendingByMerchantFragment_ViewBinding;->a:Lco/uk/getmondo/spending/merchant/SpendingByMerchantFragment;

    .line 31
    iput-object v1, v0, Lco/uk/getmondo/spending/merchant/SpendingByMerchantFragment;->monthSpendingView:Lco/uk/getmondo/spending/MonthSpendingView;

    .line 32
    return-void
.end method
