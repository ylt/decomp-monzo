.class public final Lco/uk/getmondo/spending/merchant/g;
.super Ljava/lang/Object;
.source "SpendingByMerchantModule_ProvidePresenterFactory.java"

# interfaces
.implements Lb/a/b;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lb/a/b",
        "<",
        "Lco/uk/getmondo/spending/merchant/h;",
        ">;"
    }
.end annotation


# static fields
.field static final synthetic a:Z


# instance fields
.field private final b:Lco/uk/getmondo/spending/merchant/f;

.field private final c:Ljavax/a/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/a/a",
            "<",
            "Lio/reactivex/u;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Ljavax/a/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/spending/a/i;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Ljavax/a/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/spending/a/b;",
            ">;"
        }
    .end annotation
.end field

.field private final f:Ljavax/a/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/common/a;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 12
    const-class v0, Lco/uk/getmondo/spending/merchant/g;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lco/uk/getmondo/spending/merchant/g;->a:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lco/uk/getmondo/spending/merchant/f;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lco/uk/getmondo/spending/merchant/f;",
            "Ljavax/a/a",
            "<",
            "Lio/reactivex/u;",
            ">;",
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/spending/a/i;",
            ">;",
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/spending/a/b;",
            ">;",
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/common/a;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    sget-boolean v0, Lco/uk/getmondo/spending/merchant/g;->a:Z

    if-nez v0, :cond_0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 35
    :cond_0
    iput-object p1, p0, Lco/uk/getmondo/spending/merchant/g;->b:Lco/uk/getmondo/spending/merchant/f;

    .line 36
    sget-boolean v0, Lco/uk/getmondo/spending/merchant/g;->a:Z

    if-nez v0, :cond_1

    if-nez p2, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 37
    :cond_1
    iput-object p2, p0, Lco/uk/getmondo/spending/merchant/g;->c:Ljavax/a/a;

    .line 38
    sget-boolean v0, Lco/uk/getmondo/spending/merchant/g;->a:Z

    if-nez v0, :cond_2

    if-nez p3, :cond_2

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 39
    :cond_2
    iput-object p3, p0, Lco/uk/getmondo/spending/merchant/g;->d:Ljavax/a/a;

    .line 40
    sget-boolean v0, Lco/uk/getmondo/spending/merchant/g;->a:Z

    if-nez v0, :cond_3

    if-nez p4, :cond_3

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 41
    :cond_3
    iput-object p4, p0, Lco/uk/getmondo/spending/merchant/g;->e:Ljavax/a/a;

    .line 42
    sget-boolean v0, Lco/uk/getmondo/spending/merchant/g;->a:Z

    if-nez v0, :cond_4

    if-nez p5, :cond_4

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 43
    :cond_4
    iput-object p5, p0, Lco/uk/getmondo/spending/merchant/g;->f:Ljavax/a/a;

    .line 44
    return-void
.end method

.method public static a(Lco/uk/getmondo/spending/merchant/f;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;)Lb/a/b;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lco/uk/getmondo/spending/merchant/f;",
            "Ljavax/a/a",
            "<",
            "Lio/reactivex/u;",
            ">;",
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/spending/a/i;",
            ">;",
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/spending/a/b;",
            ">;",
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/common/a;",
            ">;)",
            "Lb/a/b",
            "<",
            "Lco/uk/getmondo/spending/merchant/h;",
            ">;"
        }
    .end annotation

    .prologue
    .line 63
    new-instance v0, Lco/uk/getmondo/spending/merchant/g;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lco/uk/getmondo/spending/merchant/g;-><init>(Lco/uk/getmondo/spending/merchant/f;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;)V

    return-object v0
.end method


# virtual methods
.method public a()Lco/uk/getmondo/spending/merchant/h;
    .locals 5

    .prologue
    .line 48
    iget-object v4, p0, Lco/uk/getmondo/spending/merchant/g;->b:Lco/uk/getmondo/spending/merchant/f;

    iget-object v0, p0, Lco/uk/getmondo/spending/merchant/g;->c:Ljavax/a/a;

    .line 50
    invoke-interface {v0}, Ljavax/a/a;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/reactivex/u;

    iget-object v1, p0, Lco/uk/getmondo/spending/merchant/g;->d:Ljavax/a/a;

    .line 51
    invoke-interface {v1}, Ljavax/a/a;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lco/uk/getmondo/spending/a/i;

    iget-object v2, p0, Lco/uk/getmondo/spending/merchant/g;->e:Ljavax/a/a;

    .line 52
    invoke-interface {v2}, Ljavax/a/a;->b()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lco/uk/getmondo/spending/a/b;

    iget-object v3, p0, Lco/uk/getmondo/spending/merchant/g;->f:Ljavax/a/a;

    .line 53
    invoke-interface {v3}, Ljavax/a/a;->b()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lco/uk/getmondo/common/a;

    .line 49
    invoke-virtual {v4, v0, v1, v2, v3}, Lco/uk/getmondo/spending/merchant/f;->a(Lio/reactivex/u;Lco/uk/getmondo/spending/a/i;Lco/uk/getmondo/spending/a/b;Lco/uk/getmondo/common/a;)Lco/uk/getmondo/spending/merchant/h;

    move-result-object v0

    const-string v1, "Cannot return null from a non-@Nullable @Provides method"

    .line 48
    invoke-static {v0, v1}, Lb/a/d;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/spending/merchant/h;

    return-object v0
.end method

.method public synthetic b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 12
    invoke-virtual {p0}, Lco/uk/getmondo/spending/merchant/g;->a()Lco/uk/getmondo/spending/merchant/h;

    move-result-object v0

    return-object v0
.end method
