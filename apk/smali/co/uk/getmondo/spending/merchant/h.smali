.class public Lco/uk/getmondo/spending/merchant/h;
.super Lco/uk/getmondo/common/ui/b;
.source "SpendingByMerchantPresenter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lco/uk/getmondo/spending/merchant/h$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lco/uk/getmondo/common/ui/b",
        "<",
        "Lco/uk/getmondo/spending/merchant/h$a;",
        ">;"
    }
.end annotation


# instance fields
.field private final c:Lio/reactivex/u;

.field private final d:Lco/uk/getmondo/spending/a/i;

.field private final e:Lco/uk/getmondo/spending/a/b;

.field private final f:Lorg/threeten/bp/YearMonth;

.field private final g:Lco/uk/getmondo/d/h;

.field private final h:Lco/uk/getmondo/common/a;


# direct methods
.method constructor <init>(Lio/reactivex/u;Lco/uk/getmondo/spending/a/i;Lco/uk/getmondo/spending/a/b;Lorg/threeten/bp/YearMonth;Lco/uk/getmondo/d/h;Lco/uk/getmondo/common/a;)V
    .locals 0

    .prologue
    .line 36
    invoke-direct {p0}, Lco/uk/getmondo/common/ui/b;-><init>()V

    .line 37
    iput-object p1, p0, Lco/uk/getmondo/spending/merchant/h;->c:Lio/reactivex/u;

    .line 38
    iput-object p2, p0, Lco/uk/getmondo/spending/merchant/h;->d:Lco/uk/getmondo/spending/a/i;

    .line 39
    iput-object p3, p0, Lco/uk/getmondo/spending/merchant/h;->e:Lco/uk/getmondo/spending/a/b;

    .line 40
    iput-object p4, p0, Lco/uk/getmondo/spending/merchant/h;->f:Lorg/threeten/bp/YearMonth;

    .line 41
    iput-object p5, p0, Lco/uk/getmondo/spending/merchant/h;->g:Lco/uk/getmondo/d/h;

    .line 42
    iput-object p6, p0, Lco/uk/getmondo/spending/merchant/h;->h:Lco/uk/getmondo/common/a;

    .line 43
    return-void
.end method

.method static synthetic a(Lco/uk/getmondo/spending/merchant/h;Ljava/util/List;)Lio/reactivex/r;
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 57
    iget-object v0, p0, Lco/uk/getmondo/spending/merchant/h;->e:Lco/uk/getmondo/spending/a/b;

    sget-object v1, Lco/uk/getmondo/spending/a/a;->a:Lco/uk/getmondo/spending/a/a;

    invoke-virtual {v0, p1, v1}, Lco/uk/getmondo/spending/a/b;->a(Ljava/util/List;Lco/uk/getmondo/spending/a/a;)Lio/reactivex/n;

    move-result-object v0

    new-instance v1, Landroid/support/v4/g/j;

    iget-object v2, p0, Lco/uk/getmondo/spending/merchant/h;->f:Lorg/threeten/bp/YearMonth;

    new-instance v3, Lco/uk/getmondo/spending/a/g;

    new-instance v4, Lco/uk/getmondo/d/c;

    const-wide/16 v6, 0x0

    sget-object v5, Lco/uk/getmondo/common/i/c;->a:Lco/uk/getmondo/common/i/c;

    invoke-direct {v4, v6, v7, v5}, Lco/uk/getmondo/d/c;-><init>(JLco/uk/getmondo/common/i/c;)V

    const/4 v5, 0x0

    invoke-direct {v3, v4, v5}, Lco/uk/getmondo/spending/a/g;-><init>(Lco/uk/getmondo/d/c;I)V

    invoke-direct {v1, v2, v3}, Landroid/support/v4/g/j;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 58
    invoke-virtual {v0, v1}, Lio/reactivex/n;->defaultIfEmpty(Ljava/lang/Object;)Lio/reactivex/n;

    move-result-object v0

    .line 57
    return-object v0
.end method

.method static synthetic a(Lco/uk/getmondo/spending/merchant/h$a;Landroid/support/v4/g/j;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 60
    iget-object v0, p1, Landroid/support/v4/g/j;->a:Ljava/lang/Object;

    check-cast v0, Lorg/threeten/bp/YearMonth;

    iget-object v1, p1, Landroid/support/v4/g/j;->b:Ljava/lang/Object;

    check-cast v1, Lco/uk/getmondo/spending/a/g;

    invoke-interface {p0, v0, v1}, Lco/uk/getmondo/spending/merchant/h$a;->a(Lorg/threeten/bp/YearMonth;Lco/uk/getmondo/spending/a/g;)V

    return-void
.end method

.method static synthetic a(Lco/uk/getmondo/spending/merchant/h;Lco/uk/getmondo/spending/merchant/h$a;Lco/uk/getmondo/spending/a/h;)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 65
    instance-of v0, p2, Lco/uk/getmondo/spending/a/h$b;

    if-eqz v0, :cond_1

    .line 66
    check-cast p2, Lco/uk/getmondo/spending/a/h$b;

    invoke-virtual {p2}, Lco/uk/getmondo/spending/a/h$b;->b()Lco/uk/getmondo/d/u;

    move-result-object v0

    .line 67
    iget-object v1, p0, Lco/uk/getmondo/spending/merchant/h;->f:Lorg/threeten/bp/YearMonth;

    iget-object v2, p0, Lco/uk/getmondo/spending/merchant/h;->g:Lco/uk/getmondo/d/h;

    invoke-virtual {v0}, Lco/uk/getmondo/d/u;->i()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0}, Lco/uk/getmondo/d/u;->h()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v1, v2, v3, v0}, Lco/uk/getmondo/spending/merchant/h$a;->a(Lorg/threeten/bp/YearMonth;Lco/uk/getmondo/d/h;Ljava/lang/String;Ljava/lang/String;)V

    .line 72
    :cond_0
    :goto_0
    return-void

    .line 68
    :cond_1
    instance-of v0, p2, Lco/uk/getmondo/spending/a/h$c;

    if-eqz v0, :cond_0

    .line 69
    check-cast p2, Lco/uk/getmondo/spending/a/h$c;

    invoke-virtual {p2}, Lco/uk/getmondo/spending/a/h$c;->b()Lco/uk/getmondo/d/aa;

    move-result-object v0

    .line 70
    iget-object v1, p0, Lco/uk/getmondo/spending/merchant/h;->f:Lorg/threeten/bp/YearMonth;

    iget-object v2, p0, Lco/uk/getmondo/spending/merchant/h;->g:Lco/uk/getmondo/d/h;

    invoke-virtual {v0}, Lco/uk/getmondo/d/aa;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0}, Lco/uk/getmondo/d/aa;->a()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v1, v2, v3, v0}, Lco/uk/getmondo/spending/merchant/h$a;->b(Lorg/threeten/bp/YearMonth;Lco/uk/getmondo/d/h;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method static synthetic a(Lco/uk/getmondo/spending/merchant/h;Ljava/lang/Boolean;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 52
    iget-object v0, p0, Lco/uk/getmondo/spending/merchant/h;->g:Lco/uk/getmondo/d/h;

    iget-object v1, p0, Lco/uk/getmondo/spending/merchant/h;->f:Lorg/threeten/bp/YearMonth;

    invoke-static {v0, v1}, Lco/uk/getmondo/api/model/tracking/Impression;->b(Lco/uk/getmondo/d/h;Lorg/threeten/bp/YearMonth;)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    .line 53
    iget-object v1, p0, Lco/uk/getmondo/spending/merchant/h;->h:Lco/uk/getmondo/common/a;

    invoke-virtual {v1, v0}, Lco/uk/getmondo/common/a;->a(Lco/uk/getmondo/api/model/tracking/Impression;)V

    .line 54
    return-void
.end method

.method static synthetic a(Ljava/lang/Boolean;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 50
    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method


# virtual methods
.method public bridge synthetic a(Lco/uk/getmondo/common/ui/f;)V
    .locals 0

    .prologue
    .line 26
    check-cast p1, Lco/uk/getmondo/spending/merchant/h$a;

    invoke-virtual {p0, p1}, Lco/uk/getmondo/spending/merchant/h;->a(Lco/uk/getmondo/spending/merchant/h$a;)V

    return-void
.end method

.method public a(Lco/uk/getmondo/spending/merchant/h$a;)V
    .locals 3

    .prologue
    .line 47
    invoke-super {p0, p1}, Lco/uk/getmondo/common/ui/b;->a(Lco/uk/getmondo/common/ui/f;)V

    .line 49
    invoke-interface {p1}, Lco/uk/getmondo/spending/merchant/h$a;->b()Lio/reactivex/n;

    move-result-object v0

    invoke-static {}, Lco/uk/getmondo/spending/merchant/i;->a()Lio/reactivex/c/q;

    move-result-object v1

    .line 50
    invoke-virtual {v0, v1}, Lio/reactivex/n;->filter(Lio/reactivex/c/q;)Lio/reactivex/n;

    move-result-object v0

    invoke-static {p0}, Lco/uk/getmondo/spending/merchant/j;->a(Lco/uk/getmondo/spending/merchant/h;)Lio/reactivex/c/g;

    move-result-object v1

    invoke-static {}, Lco/uk/getmondo/spending/merchant/k;->a()Lio/reactivex/c/g;

    move-result-object v2

    .line 51
    invoke-virtual {v0, v1, v2}, Lio/reactivex/n;->subscribe(Lio/reactivex/c/g;Lio/reactivex/c/g;)Lio/reactivex/b/b;

    move-result-object v0

    .line 49
    invoke-virtual {p0, v0}, Lco/uk/getmondo/spending/merchant/h;->a(Lio/reactivex/b/b;)V

    .line 56
    iget-object v0, p0, Lco/uk/getmondo/spending/merchant/h;->d:Lco/uk/getmondo/spending/a/i;

    iget-object v1, p0, Lco/uk/getmondo/spending/merchant/h;->f:Lorg/threeten/bp/YearMonth;

    iget-object v2, p0, Lco/uk/getmondo/spending/merchant/h;->g:Lco/uk/getmondo/d/h;

    invoke-virtual {v0, v1, v2}, Lco/uk/getmondo/spending/a/i;->a(Lorg/threeten/bp/YearMonth;Lco/uk/getmondo/d/h;)Lio/reactivex/n;

    move-result-object v0

    invoke-static {p0}, Lco/uk/getmondo/spending/merchant/l;->a(Lco/uk/getmondo/spending/merchant/h;)Lio/reactivex/c/h;

    move-result-object v1

    .line 57
    invoke-virtual {v0, v1}, Lio/reactivex/n;->flatMap(Lio/reactivex/c/h;)Lio/reactivex/n;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/spending/merchant/h;->c:Lio/reactivex/u;

    .line 59
    invoke-virtual {v0, v1}, Lio/reactivex/n;->observeOn(Lio/reactivex/u;)Lio/reactivex/n;

    move-result-object v0

    invoke-static {p1}, Lco/uk/getmondo/spending/merchant/m;->a(Lco/uk/getmondo/spending/merchant/h$a;)Lio/reactivex/c/g;

    move-result-object v1

    invoke-static {}, Lco/uk/getmondo/spending/merchant/n;->a()Lio/reactivex/c/g;

    move-result-object v2

    .line 60
    invoke-virtual {v0, v1, v2}, Lio/reactivex/n;->subscribe(Lio/reactivex/c/g;Lio/reactivex/c/g;)Lio/reactivex/b/b;

    move-result-object v0

    .line 56
    invoke-virtual {p0, v0}, Lco/uk/getmondo/spending/merchant/h;->a(Lio/reactivex/b/b;)V

    .line 63
    invoke-interface {p1}, Lco/uk/getmondo/spending/merchant/h$a;->a()Lio/reactivex/n;

    move-result-object v0

    invoke-static {p0, p1}, Lco/uk/getmondo/spending/merchant/o;->a(Lco/uk/getmondo/spending/merchant/h;Lco/uk/getmondo/spending/merchant/h$a;)Lio/reactivex/c/g;

    move-result-object v1

    .line 64
    invoke-virtual {v0, v1}, Lio/reactivex/n;->subscribe(Lio/reactivex/c/g;)Lio/reactivex/b/b;

    move-result-object v0

    .line 63
    invoke-virtual {p0, v0}, Lco/uk/getmondo/spending/merchant/h;->a(Lio/reactivex/b/b;)V

    .line 73
    return-void
.end method
