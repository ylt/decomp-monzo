.class final Lco/uk/getmondo/spending/b/c$j;
.super Ljava/lang/Object;
.source "ExportPresenter.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lco/uk/getmondo/spending/b/c;->a(Lco/uk/getmondo/spending/b/c$a;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h",
        "<TT;TR;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000\u0012\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u0008\u0012\u0004\u0012\u00020\u00020\u00012\u000c\u0010\u0003\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0001H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "",
        "Lco/uk/getmondo/spending/export/data/model/ExportPeriod;",
        "it",
        "Lorg/threeten/bp/YearMonth;",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field final synthetic a:Lco/uk/getmondo/spending/b/c;


# direct methods
.method constructor <init>(Lco/uk/getmondo/spending/b/c;)V
    .locals 0

    iput-object p1, p0, Lco/uk/getmondo/spending/b/c$j;->a:Lco/uk/getmondo/spending/b/c;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 33
    check-cast p1, Ljava/util/List;

    invoke-virtual {p0, p1}, Lco/uk/getmondo/spending/b/c$j;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/util/List;)Ljava/util/List;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lorg/threeten/bp/YearMonth;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lco/uk/getmondo/spending/b/a/a/c;",
            ">;"
        }
    .end annotation

    .prologue
    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 48
    check-cast p1, Ljava/lang/Iterable;

    .line 101
    new-instance v0, Ljava/util/ArrayList;

    const/16 v1, 0xa

    invoke-static {p1, v1}, Lkotlin/a/m;->a(Ljava/lang/Iterable;I)I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v0, Ljava/util/Collection;

    .line 102
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 103
    check-cast v1, Lorg/threeten/bp/YearMonth;

    .line 49
    iget-object v3, p0, Lco/uk/getmondo/spending/b/c$j;->a:Lco/uk/getmondo/spending/b/c;

    invoke-virtual {v1}, Lorg/threeten/bp/YearMonth;->b()I

    move-result v4

    invoke-virtual {v1}, Lorg/threeten/bp/YearMonth;->c()Lorg/threeten/bp/Month;

    move-result-object v5

    invoke-static {v4, v5}, Lorg/threeten/bp/YearMonth;->a(ILorg/threeten/bp/Month;)Lorg/threeten/bp/YearMonth;

    move-result-object v4

    const-string v5, "YearMonth.of(it.year, it.month)"

    invoke-static {v4, v5}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v3, v4}, Lco/uk/getmondo/spending/b/c;->a(Lco/uk/getmondo/spending/b/c;Lorg/threeten/bp/YearMonth;)Ljava/lang/String;

    move-result-object v3

    .line 50
    new-instance v4, Lco/uk/getmondo/spending/b/a/a/c;

    const/4 v5, 0x1

    invoke-virtual {v1, v5}, Lorg/threeten/bp/YearMonth;->c(I)Lorg/threeten/bp/LocalDate;

    move-result-object v5

    const-string v6, "it.atDay(1)"

    invoke-static {v5, v6}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v1}, Lorg/threeten/bp/YearMonth;->f()Lorg/threeten/bp/LocalDate;

    move-result-object v1

    const-string v6, "it.atEndOfMonth()"

    invoke-static {v1, v6}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v6, Lco/uk/getmondo/spending/b/a/a/c$a;->a:Lco/uk/getmondo/spending/b/a/a/c$a;

    invoke-direct {v4, v5, v1, v3, v6}, Lco/uk/getmondo/spending/b/a/a/c;-><init>(Lorg/threeten/bp/LocalDate;Lorg/threeten/bp/LocalDate;Ljava/lang/String;Lco/uk/getmondo/spending/b/a/a/c$a;)V

    nop

    invoke-interface {v0, v4}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 104
    :cond_0
    check-cast v0, Ljava/util/List;

    .line 51
    return-object v0
.end method
