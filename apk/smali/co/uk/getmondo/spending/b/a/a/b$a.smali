.class public final enum Lco/uk/getmondo/spending/b/a/a/b$a;
.super Ljava/lang/Enum;
.source "ExportData.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lco/uk/getmondo/spending/b/a/a/b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lco/uk/getmondo/spending/b/a/a/b$a;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0008\u0008\u0086\u0001\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00000\u0001B\u0019\u0008\u0002\u0012\u0008\u0008\u0001\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0007\u0010\u0008R\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\t\u0010\nj\u0002\u0008\u000bj\u0002\u0008\u000c\u00a8\u0006\r"
    }
    d2 = {
        "Lco/uk/getmondo/spending/export/data/model/ExportOutput$Format;",
        "",
        "displayTextRes",
        "",
        "extension",
        "",
        "(Ljava/lang/String;IILjava/lang/String;)V",
        "getDisplayTextRes",
        "()I",
        "getExtension",
        "()Ljava/lang/String;",
        "CSV",
        "QIF",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# static fields
.field public static final enum a:Lco/uk/getmondo/spending/b/a/a/b$a;

.field public static final enum b:Lco/uk/getmondo/spending/b/a/a/b$a;

.field private static final synthetic c:[Lco/uk/getmondo/spending/b/a/a/b$a;


# instance fields
.field private final d:I

.field private final e:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    const/4 v0, 0x2

    new-array v0, v0, [Lco/uk/getmondo/spending/b/a/a/b$a;

    new-instance v1, Lco/uk/getmondo/spending/b/a/a/b$a;

    const-string v2, "CSV"

    .line 22
    const v3, 0x7f0a03ad

    const-string v4, ".csv"

    invoke-direct {v1, v2, v5, v3, v4}, Lco/uk/getmondo/spending/b/a/a/b$a;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v1, Lco/uk/getmondo/spending/b/a/a/b$a;->a:Lco/uk/getmondo/spending/b/a/a/b$a;

    aput-object v1, v0, v5

    new-instance v1, Lco/uk/getmondo/spending/b/a/a/b$a;

    const-string v2, "QIF"

    .line 23
    const v3, 0x7f0a03ae

    const-string v4, ".qif"

    invoke-direct {v1, v2, v6, v3, v4}, Lco/uk/getmondo/spending/b/a/a/b$a;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v1, Lco/uk/getmondo/spending/b/a/a/b$a;->b:Lco/uk/getmondo/spending/b/a/a/b$a;

    aput-object v1, v0, v6

    sput-object v0, Lco/uk/getmondo/spending/b/a/a/b$a;->c:[Lco/uk/getmondo/spending/b/a/a/b$a;

    return-void
.end method

.method protected constructor <init>(Ljava/lang/String;IILjava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    const-string v0, "extension"

    invoke-static {p4, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 21
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput p3, p0, Lco/uk/getmondo/spending/b/a/a/b$a;->d:I

    iput-object p4, p0, Lco/uk/getmondo/spending/b/a/a/b$a;->e:Ljava/lang/String;

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lco/uk/getmondo/spending/b/a/a/b$a;
    .locals 1

    const-class v0, Lco/uk/getmondo/spending/b/a/a/b$a;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/spending/b/a/a/b$a;

    return-object v0
.end method

.method public static values()[Lco/uk/getmondo/spending/b/a/a/b$a;
    .locals 1

    sget-object v0, Lco/uk/getmondo/spending/b/a/a/b$a;->c:[Lco/uk/getmondo/spending/b/a/a/b$a;

    invoke-virtual {v0}, [Lco/uk/getmondo/spending/b/a/a/b$a;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lco/uk/getmondo/spending/b/a/a/b$a;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 21
    iget v0, p0, Lco/uk/getmondo/spending/b/a/a/b$a;->d:I

    return v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lco/uk/getmondo/spending/b/a/a/b$a;->e:Ljava/lang/String;

    return-object v0
.end method
