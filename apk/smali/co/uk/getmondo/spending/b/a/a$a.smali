.class final Lco/uk/getmondo/spending/b/a/a$a;
.super Ljava/lang/Object;
.source "TransactionExporter.kt"

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lco/uk/getmondo/spending/b/a/a;->a(Lco/uk/getmondo/spending/b/a/a/a;)Lio/reactivex/v;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<TT;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000\n\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0002\u0010\u0000\u001a\n \u0002*\u0004\u0018\u00010\u00010\u0001H\n\u00a2\u0006\u0002\u0008\u0003"
    }
    d2 = {
        "<anonymous>",
        "",
        "kotlin.jvm.PlatformType",
        "call"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field final synthetic a:Lco/uk/getmondo/spending/b/a/a;

.field final synthetic b:Lco/uk/getmondo/spending/b/a/a/a;


# direct methods
.method constructor <init>(Lco/uk/getmondo/spending/b/a/a;Lco/uk/getmondo/spending/b/a/a/a;)V
    .locals 0

    iput-object p1, p0, Lco/uk/getmondo/spending/b/a/a$a;->a:Lco/uk/getmondo/spending/b/a/a;

    iput-object p2, p0, Lco/uk/getmondo/spending/b/a/a$a;->b:Lco/uk/getmondo/spending/b/a/a/a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 6

    .prologue
    .line 27
    iget-object v0, p0, Lco/uk/getmondo/spending/b/a/a$a;->a:Lco/uk/getmondo/spending/b/a/a;

    .line 28
    iget-object v1, p0, Lco/uk/getmondo/spending/b/a/a$a;->b:Lco/uk/getmondo/spending/b/a/a/a;

    invoke-virtual {v1}, Lco/uk/getmondo/spending/b/a/a/a;->a()Lco/uk/getmondo/spending/b/a/a/b;

    move-result-object v1

    invoke-virtual {v1}, Lco/uk/getmondo/spending/b/a/a/b;->b()Ljava/lang/String;

    move-result-object v1

    .line 29
    iget-object v2, p0, Lco/uk/getmondo/spending/b/a/a$a;->b:Lco/uk/getmondo/spending/b/a/a/a;

    invoke-virtual {v2}, Lco/uk/getmondo/spending/b/a/a/a;->b()Ljava/lang/String;

    move-result-object v2

    .line 30
    iget-object v3, p0, Lco/uk/getmondo/spending/b/a/a$a;->b:Lco/uk/getmondo/spending/b/a/a/a;

    invoke-virtual {v3}, Lco/uk/getmondo/spending/b/a/a/a;->a()Lco/uk/getmondo/spending/b/a/a/b;

    move-result-object v3

    invoke-virtual {v3}, Lco/uk/getmondo/spending/b/a/a/b;->a()Lco/uk/getmondo/spending/b/a/a/b$a;

    move-result-object v3

    invoke-virtual {v3}, Lco/uk/getmondo/spending/b/a/a/b$a;->b()Ljava/lang/String;

    move-result-object v3

    .line 27
    invoke-static {v0, v1, v2, v3}, Lco/uk/getmondo/spending/b/a/a;->a(Lco/uk/getmondo/spending/b/a/a;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/io/File;

    move-result-object v2

    .line 32
    iget-object v0, p0, Lco/uk/getmondo/spending/b/a/a$a;->b:Lco/uk/getmondo/spending/b/a/a/a;

    invoke-virtual {v0}, Lco/uk/getmondo/spending/b/a/a/a;->a()Lco/uk/getmondo/spending/b/a/a/b;

    move-result-object v0

    invoke-virtual {v0}, Lco/uk/getmondo/spending/b/a/a/b;->a()Lco/uk/getmondo/spending/b/a/a/b$a;

    move-result-object v0

    sget-object v1, Lco/uk/getmondo/spending/b/a/b;->a:[I

    invoke-virtual {v0}, Lco/uk/getmondo/spending/b/a/a/b$a;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 35
    :goto_0
    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 33
    :pswitch_0
    iget-object v3, p0, Lco/uk/getmondo/spending/b/a/a$a;->a:Lco/uk/getmondo/spending/b/a/a;

    iget-object v0, p0, Lco/uk/getmondo/spending/b/a/a$a;->b:Lco/uk/getmondo/spending/b/a/a/a;

    invoke-virtual {v0}, Lco/uk/getmondo/spending/b/a/a/a;->c()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 92
    new-instance v1, Ljava/util/ArrayList;

    const/16 v4, 0xa

    invoke-static {v0, v4}, Lkotlin/a/m;->a(Ljava/lang/Iterable;I)I

    move-result v4

    invoke-direct {v1, v4}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v1, Ljava/util/Collection;

    .line 93
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .line 94
    check-cast v0, Lco/uk/getmondo/d/aj;

    .line 33
    sget-object v5, Lco/uk/getmondo/spending/b/a/a/e;->a:Lco/uk/getmondo/spending/b/a/a/e$a;

    invoke-virtual {v5, v0}, Lco/uk/getmondo/spending/b/a/a/e$a;->a(Lco/uk/getmondo/d/aj;)Lco/uk/getmondo/spending/b/a/a/e;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 95
    :cond_0
    check-cast v1, Ljava/util/List;

    .line 33
    invoke-static {v3, v1, v2}, Lco/uk/getmondo/spending/b/a/a;->a(Lco/uk/getmondo/spending/b/a/a;Ljava/util/List;Ljava/io/File;)V

    goto :goto_0

    .line 34
    :pswitch_1
    iget-object v0, p0, Lco/uk/getmondo/spending/b/a/a$a;->a:Lco/uk/getmondo/spending/b/a/a;

    iget-object v1, p0, Lco/uk/getmondo/spending/b/a/a$a;->b:Lco/uk/getmondo/spending/b/a/a/a;

    invoke-virtual {v1}, Lco/uk/getmondo/spending/b/a/a/a;->c()Ljava/util/List;

    move-result-object v1

    invoke-static {v0, v1, v2}, Lco/uk/getmondo/spending/b/a/a;->b(Lco/uk/getmondo/spending/b/a/a;Ljava/util/List;Ljava/io/File;)V

    goto :goto_0

    .line 32
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public synthetic call()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 20
    invoke-virtual {p0}, Lco/uk/getmondo/spending/b/a/a$a;->a()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
