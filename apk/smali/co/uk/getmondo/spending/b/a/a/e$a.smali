.class public final Lco/uk/getmondo/spending/b/a/a/e$a;
.super Ljava/lang/Object;
.source "TransactionRecordItem.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lco/uk/getmondo/spending/b/a/a/e;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000<\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0010\u0011\n\u0002\u0010\u000e\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u000e\u0010\u0011\u001a\u00020\u00052\u0006\u0010\u0012\u001a\u00020\u0013J\u000e\u0010\u0014\u001a\u00020\u00152\u0006\u0010\u0016\u001a\u00020\u0017J\u000e\u0010\u0018\u001a\u00020\u00052\u0006\u0010\u0016\u001a\u00020\u0017R\u0019\u0010\u0003\u001a\u0008\u0012\u0004\u0012\u00020\u00050\u0004\u00a2\u0006\n\n\u0002\u0010\u0008\u001a\u0004\u0008\u0006\u0010\u0007R\u0014\u0010\t\u001a\u00020\nX\u0082\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000b\u0010\u000cR\u0014\u0010\r\u001a\u00020\u000eX\u0082\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000f\u0010\u0010\u00a8\u0006\u0019"
    }
    d2 = {
        "Lco/uk/getmondo/spending/export/data/model/TransactionRecordItem$Companion;",
        "",
        "()V",
        "HEADERS",
        "",
        "",
        "getHEADERS",
        "()[Ljava/lang/String;",
        "[Ljava/lang/String;",
        "amountFormatter",
        "Lco/uk/getmondo/common/money/AmountFormatter;",
        "getAmountFormatter",
        "()Lco/uk/getmondo/common/money/AmountFormatter;",
        "dateFormatter",
        "Ljava/text/SimpleDateFormat;",
        "getDateFormatter",
        "()Ljava/text/SimpleDateFormat;",
        "formatAddress",
        "merchant",
        "Lco/uk/getmondo/model/Merchant;",
        "from",
        "Lco/uk/getmondo/spending/export/data/model/TransactionRecordItem;",
        "transaction",
        "Lco/uk/getmondo/model/Transaction;",
        "getDescription",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/d/b/i;)V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Lco/uk/getmondo/spending/b/a/a/e$a;-><init>()V

    return-void
.end method

.method private final b()Ljava/text/SimpleDateFormat;
    .locals 1

    .prologue
    .line 31
    invoke-static {}, Lco/uk/getmondo/spending/b/a/a/e;->b()Ljava/text/SimpleDateFormat;

    move-result-object v0

    return-object v0
.end method

.method private final c()Lco/uk/getmondo/common/i/b;
    .locals 1

    .prologue
    .line 32
    invoke-static {}, Lco/uk/getmondo/spending/b/a/a/e;->c()Lco/uk/getmondo/common/i/b;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(Lco/uk/getmondo/d/aj;)Lco/uk/getmondo/spending/b/a/a/e;
    .locals 21

    .prologue
    const-string v1, "transaction"

    move-object/from16 v0, p1

    invoke-static {v0, v1}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 38
    new-instance v10, Lco/uk/getmondo/spending/b/a/a/e;

    .line 39
    invoke-virtual/range {p1 .. p1}, Lco/uk/getmondo/d/aj;->w()Ljava/lang/String;

    move-result-object v9

    const-string v1, "transaction.id"

    invoke-static {v9, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    move-object/from16 v1, p0

    .line 40
    check-cast v1, Lco/uk/getmondo/spending/b/a/a/e$a;

    invoke-direct {v1}, Lco/uk/getmondo/spending/b/a/a/e$a;->b()Ljava/text/SimpleDateFormat;

    move-result-object v1

    invoke-virtual/range {p1 .. p1}, Lco/uk/getmondo/d/aj;->v()Ljava/util/Date;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v8

    const-string v1, "dateFormatter.format(transaction.created)"

    invoke-static {v8, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    move-object/from16 v1, p0

    .line 41
    check-cast v1, Lco/uk/getmondo/spending/b/a/a/e$a;

    invoke-direct {v1}, Lco/uk/getmondo/spending/b/a/a/e$a;->c()Lco/uk/getmondo/common/i/b;

    move-result-object v1

    invoke-virtual/range {p1 .. p1}, Lco/uk/getmondo/d/aj;->g()Lco/uk/getmondo/d/c;

    move-result-object v2

    const-string v3, "transaction.amount"

    invoke-static {v2, v3}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Lco/uk/getmondo/common/i/b;->a(Lco/uk/getmondo/d/c;)Ljava/lang/String;

    move-result-object v7

    .line 42
    invoke-virtual/range {p1 .. p1}, Lco/uk/getmondo/d/aj;->g()Lco/uk/getmondo/d/c;

    move-result-object v1

    invoke-virtual {v1}, Lco/uk/getmondo/d/c;->l()Lco/uk/getmondo/common/i/c;

    move-result-object v1

    invoke-virtual {v1}, Lco/uk/getmondo/common/i/c;->b()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    move-object v2, v1

    :goto_0
    move-object/from16 v1, p0

    .line 43
    check-cast v1, Lco/uk/getmondo/spending/b/a/a/e$a;

    invoke-direct {v1}, Lco/uk/getmondo/spending/b/a/a/e$a;->c()Lco/uk/getmondo/common/i/b;

    move-result-object v1

    invoke-virtual/range {p1 .. p1}, Lco/uk/getmondo/d/aj;->h()Lco/uk/getmondo/d/c;

    move-result-object v3

    const-string v4, "transaction.localAmount"

    invoke-static {v3, v4}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v1, v3}, Lco/uk/getmondo/common/i/b;->a(Lco/uk/getmondo/d/c;)Ljava/lang/String;

    move-result-object v6

    .line 44
    invoke-virtual/range {p1 .. p1}, Lco/uk/getmondo/d/aj;->h()Lco/uk/getmondo/d/c;

    move-result-object v1

    invoke-virtual {v1}, Lco/uk/getmondo/d/c;->l()Lco/uk/getmondo/common/i/c;

    move-result-object v1

    invoke-virtual {v1}, Lco/uk/getmondo/common/i/c;->b()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 45
    :goto_1
    invoke-virtual/range {p1 .. p1}, Lco/uk/getmondo/d/aj;->c()Lco/uk/getmondo/d/h;

    move-result-object v3

    invoke-virtual {v3}, Lco/uk/getmondo/d/h;->f()Ljava/lang/String;

    move-result-object v5

    const-string v3, "transaction.category.apiValue"

    invoke-static {v5, v3}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 46
    invoke-virtual/range {p1 .. p1}, Lco/uk/getmondo/d/aj;->f()Lco/uk/getmondo/d/u;

    move-result-object v3

    if-eqz v3, :cond_2

    invoke-virtual {v3}, Lco/uk/getmondo/d/u;->l()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_2

    .line 47
    :goto_2
    check-cast p0, Lco/uk/getmondo/spending/b/a/a/e$a;

    invoke-virtual/range {p0 .. p1}, Lco/uk/getmondo/spending/b/a/a/e$a;->b(Lco/uk/getmondo/d/aj;)Ljava/lang/String;

    move-result-object v4

    .line 48
    invoke-virtual/range {p1 .. p1}, Lco/uk/getmondo/d/aj;->f()Lco/uk/getmondo/d/u;

    move-result-object v11

    if-eqz v11, :cond_3

    sget-object v12, Lco/uk/getmondo/spending/b/a/a/e;->a:Lco/uk/getmondo/spending/b/a/a/e$a;

    const-string v13, "it"

    invoke-static {v11, v13}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v12, v11}, Lco/uk/getmondo/spending/b/a/a/e$a;->a(Lco/uk/getmondo/d/u;)Ljava/lang/String;

    move-result-object v11

    if-eqz v11, :cond_3

    move-object v15, v10

    move-object/from16 v17, v3

    move-object v3, v8

    move-object v8, v5

    move-object v5, v2

    move-object v2, v9

    move-object/from16 v9, v17

    move-object/from16 v18, v7

    move-object v7, v1

    move-object v1, v10

    move-object v10, v4

    move-object/from16 v4, v18

    .line 49
    :goto_3
    invoke-virtual/range {p1 .. p1}, Lco/uk/getmondo/d/aj;->e()Ljava/lang/String;

    move-result-object v12

    if-eqz v12, :cond_4

    .line 50
    :goto_4
    invoke-virtual/range {p1 .. p1}, Lco/uk/getmondo/d/aj;->A()Lio/realm/az;

    move-result-object v13

    check-cast v13, Ljava/lang/Iterable;

    .line 72
    new-instance v14, Ljava/util/ArrayList;

    const/16 v16, 0xa

    move/from16 v0, v16

    invoke-static {v13, v0}, Lkotlin/a/m;->a(Ljava/lang/Iterable;I)I

    move-result v16

    move/from16 v0, v16

    invoke-direct {v14, v0}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v14, Ljava/util/Collection;

    .line 73
    invoke-interface {v13}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v16

    :goto_5
    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->hasNext()Z

    move-result v13

    if-eqz v13, :cond_5

    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v13

    .line 74
    check-cast v13, Lco/uk/getmondo/d/d;

    .line 50
    invoke-virtual {v13}, Lco/uk/getmondo/d/d;->b()Ljava/lang/String;

    move-result-object v13

    invoke-interface {v14, v13}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_5

    .line 42
    :cond_0
    const-string v1, ""

    move-object v2, v1

    goto/16 :goto_0

    .line 44
    :cond_1
    const-string v1, ""

    goto :goto_1

    .line 46
    :cond_2
    const-string v3, ""

    goto :goto_2

    :cond_3
    move-object v11, v10

    move-object/from16 v17, v5

    move-object v5, v6

    move-object v6, v2

    move-object/from16 v2, v17

    move-object/from16 v18, v1

    move-object v1, v4

    move-object/from16 v4, v18

    .line 48
    const-string v12, ""

    move-object v15, v11

    move-object v11, v12

    move-object/from16 v17, v3

    move-object v3, v8

    move-object v8, v2

    move-object v2, v9

    move-object/from16 v9, v17

    move-object/from16 v18, v6

    move-object v6, v5

    move-object/from16 v5, v18

    move-object/from16 v19, v7

    move-object v7, v4

    move-object/from16 v4, v19

    move-object/from16 v20, v10

    move-object v10, v1

    move-object/from16 v1, v20

    goto :goto_3

    .line 49
    :cond_4
    const-string v12, ""

    goto :goto_4

    .line 75
    :cond_5
    check-cast v14, Ljava/util/List;

    .line 50
    invoke-interface {v14}, Ljava/util/List;->isEmpty()Z

    move-result v13

    if-eqz v13, :cond_6

    const-string v13, ""

    .line 38
    :goto_6
    invoke-direct/range {v1 .. v13}, Lco/uk/getmondo/spending/b/a/a/e;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object v15

    .line 50
    :cond_6
    invoke-virtual {v14}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v13

    goto :goto_6
.end method

.method public final a(Lco/uk/getmondo/d/u;)Ljava/lang/String;
    .locals 2

    .prologue
    const-string v0, "merchant"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 55
    invoke-virtual {p1}, Lco/uk/getmondo/d/u;->k()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "Online transaction"

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p1}, Lco/uk/getmondo/d/u;->c()Lcom/c/b/b;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v0, v1}, Lcom/c/b/b;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    const-string v1, "merchant.formattedAddress.orElse(\"\")"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public final a()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 34
    invoke-static {}, Lco/uk/getmondo/spending/b/a/a/e;->d()[Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final b(Lco/uk/getmondo/d/aj;)Ljava/lang/String;
    .locals 3

    .prologue
    const-string v0, "transaction"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 59
    invoke-virtual {p1}, Lco/uk/getmondo/d/aj;->f()Lco/uk/getmondo/d/u;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 60
    invoke-virtual {p1}, Lco/uk/getmondo/d/aj;->f()Lco/uk/getmondo/d/u;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-static {}, Lkotlin/d/b/l;->a()V

    :cond_0
    invoke-virtual {v0}, Lco/uk/getmondo/d/u;->i()Ljava/lang/String;

    move-result-object v0

    const-string v1, "transaction.merchant!!.name"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 59
    :goto_0
    return-object v0

    .line 61
    :cond_1
    invoke-virtual {p1}, Lco/uk/getmondo/d/aj;->B()Lco/uk/getmondo/d/aa;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 62
    invoke-virtual {p1}, Lco/uk/getmondo/d/aj;->B()Lco/uk/getmondo/d/aa;

    move-result-object v0

    invoke-virtual {v0}, Lco/uk/getmondo/d/aa;->b()Ljava/lang/String;

    move-result-object v0

    .line 63
    invoke-virtual {p1}, Lco/uk/getmondo/d/aj;->g()Lco/uk/getmondo/d/c;

    move-result-object v1

    invoke-virtual {v1}, Lco/uk/getmondo/d/c;->a()Z

    move-result v1

    if-eqz v1, :cond_2

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Payment to "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 64
    :goto_1
    const-string v1, "if (transaction.peer != \u2026         \"\"\n            }"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    .line 63
    :cond_2
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Payment from "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 64
    :cond_3
    invoke-virtual {p1}, Lco/uk/getmondo/d/aj;->x()Ljava/lang/String;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-static {v0}, Lkotlin/h/j;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_4

    const/4 v0, 0x1

    :goto_2
    if-eqz v0, :cond_5

    .line 65
    invoke-virtual {p1}, Lco/uk/getmondo/d/aj;->x()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 64
    :cond_4
    const/4 v0, 0x0

    goto :goto_2

    .line 67
    :cond_5
    const-string v0, ""

    goto :goto_1
.end method
