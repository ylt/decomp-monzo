.class public final Lco/uk/getmondo/spending/b/a/a;
.super Ljava/lang/Object;
.source "TransactionExporter.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000L\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0007\u0008\u0007\u00a2\u0006\u0002\u0010\u0002J \u0010\u0008\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\u000b2\u0006\u0010\u000c\u001a\u00020\u000b2\u0006\u0010\r\u001a\u00020\u000bH\u0002J\u0018\u0010\u000e\u001a\u00020\u000b2\u0006\u0010\u000c\u001a\u00020\u000b2\u0006\u0010\r\u001a\u00020\u000bH\u0002J\u0014\u0010\u000f\u001a\u0008\u0012\u0004\u0012\u00020\u000b0\u00102\u0006\u0010\u0011\u001a\u00020\u0012J\u001e\u0010\u0013\u001a\u00020\u00142\u000c\u0010\u0015\u001a\u0008\u0012\u0004\u0012\u00020\u00170\u00162\u0006\u0010\u0018\u001a\u00020\tH\u0002J\u001e\u0010\u0019\u001a\u00020\u00142\u000c\u0010\u001a\u001a\u0008\u0012\u0004\u0012\u00020\u001b0\u00162\u0006\u0010\u0018\u001a\u00020\tH\u0002R\u0016\u0010\u0003\u001a\n \u0005*\u0004\u0018\u00010\u00040\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001c"
    }
    d2 = {
        "Lco/uk/getmondo/spending/export/data/TransactionExporter;",
        "",
        "()V",
        "filenameDateTimeFormatter",
        "Lorg/threeten/bp/format/DateTimeFormatter;",
        "kotlin.jvm.PlatformType",
        "qifDateFormatter",
        "Ljava/text/SimpleDateFormat;",
        "createExportFile",
        "Ljava/io/File;",
        "directoryPathname",
        "",
        "description",
        "extension",
        "createFilename",
        "export",
        "Lio/reactivex/Single;",
        "exportData",
        "Lco/uk/getmondo/spending/export/data/model/ExportData;",
        "exportAsCsv",
        "",
        "transactionRecordItems",
        "",
        "Lco/uk/getmondo/spending/export/data/model/TransactionRecordItem;",
        "file",
        "exportAsQif",
        "transactions",
        "Lco/uk/getmondo/model/Transaction;",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field private final a:Lorg/threeten/bp/format/DateTimeFormatter;

.field private final b:Ljava/text/SimpleDateFormat;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    const-string v0, "yyyy-MM-dd_HHmmss"

    invoke-static {v0}, Lorg/threeten/bp/format/DateTimeFormatter;->a(Ljava/lang/String;)Lorg/threeten/bp/format/DateTimeFormatter;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/spending/b/a/a;->a:Lorg/threeten/bp/format/DateTimeFormatter;

    .line 23
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "dd/MM/yyyy"

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-direct {v0, v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    iput-object v0, p0, Lco/uk/getmondo/spending/b/a/a;->b:Ljava/text/SimpleDateFormat;

    return-void
.end method

.method public static final synthetic a(Lco/uk/getmondo/spending/b/a/a;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/io/File;
    .locals 1

    .prologue
    .line 20
    invoke-direct {p0, p1, p2, p3}, Lco/uk/getmondo/spending/b/a/a;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    return-object v0
.end method

.method private final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/io/File;
    .locals 3

    .prologue
    .line 87
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 88
    new-instance v1, Ljava/io/File;

    const-string v2, "export"

    invoke-direct {v1, v0, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/io/File;->mkdirs()Z

    .line 89
    new-instance v0, Ljava/io/File;

    invoke-direct {p0, p2, p3}, Lco/uk/getmondo/spending/b/a/a;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->createNewFile()Z

    return-object v0
.end method

.method private final a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 81
    check-cast p1, Ljava/lang/CharSequence;

    const-string v0, "\\s+"

    new-instance v1, Lkotlin/h/g;

    invoke-direct {v1, v0}, Lkotlin/h/g;-><init>(Ljava/lang/String;)V

    const-string v0, ""

    invoke-virtual {v1, p1, v0}, Lkotlin/h/g;->a(Ljava/lang/CharSequence;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 82
    invoke-static {}, Lorg/threeten/bp/LocalDateTime;->a()Lorg/threeten/bp/LocalDateTime;

    move-result-object v1

    iget-object v2, p0, Lco/uk/getmondo/spending/b/a/a;->a:Lorg/threeten/bp/format/DateTimeFormatter;

    invoke-virtual {v1, v2}, Lorg/threeten/bp/LocalDateTime;->a(Lorg/threeten/bp/format/DateTimeFormatter;)Ljava/lang/String;

    move-result-object v1

    .line 83
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "MonzoDataExport_"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v2, 0x5f

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static final synthetic a(Lco/uk/getmondo/spending/b/a/a;Ljava/util/List;Ljava/io/File;)V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0, p1, p2}, Lco/uk/getmondo/spending/b/a/a;->a(Ljava/util/List;Ljava/io/File;)V

    return-void
.end method

.method private final a(Ljava/util/List;Ljava/io/File;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lco/uk/getmondo/spending/b/a/a/e;",
            ">;",
            "Ljava/io/File;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 42
    new-instance v1, Ljava/io/FileWriter;

    invoke-direct {v1, p2}, Ljava/io/FileWriter;-><init>(Ljava/io/File;)V

    check-cast v1, Ljava/io/Closeable;

    nop

    :try_start_0
    move-object v0, v1

    check-cast v0, Ljava/io/FileWriter;

    move-object v2, v0

    .line 43
    new-instance v4, Lorg/apache/a/a/c;

    check-cast v2, Ljava/lang/Appendable;

    sget-object v5, Lorg/apache/a/a/b;->a:Lorg/apache/a/a/b;

    sget-object v3, Lco/uk/getmondo/spending/b/a/a/e;->a:Lco/uk/getmondo/spending/b/a/a/e$a;

    invoke-virtual {v3}, Lco/uk/getmondo/spending/b/a/a/e$a;->a()[Ljava/lang/String;

    move-result-object v3

    array-length v8, v3

    invoke-static {v3, v8}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [Ljava/lang/String;

    invoke-virtual {v5, v3}, Lorg/apache/a/a/b;->a([Ljava/lang/String;)Lorg/apache/a/a/b;

    move-result-object v3

    invoke-direct {v4, v2, v3}, Lorg/apache/a/a/c;-><init>(Ljava/lang/Appendable;Lorg/apache/a/a/b;)V

    .line 44
    check-cast p1, Ljava/lang/Iterable;

    .line 92
    new-instance v2, Ljava/util/ArrayList;

    const/16 v3, 0xa

    invoke-static {p1, v3}, Lkotlin/a/m;->a(Ljava/lang/Iterable;I)I

    move-result v3

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v2, Ljava/util/Collection;

    .line 93
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    .line 94
    check-cast v3, Lco/uk/getmondo/spending/b/a/a/e;

    .line 44
    invoke-virtual {v3}, Lco/uk/getmondo/spending/b/a/a/e;->a()[Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    goto :goto_0

    .line 42
    :catch_0
    move-exception v2

    nop

    :try_start_1
    invoke-interface {v1}, Ljava/io/Closeable;->close()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_3
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :goto_1
    :try_start_2
    check-cast v2, Ljava/lang/Throwable;

    throw v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :catchall_0
    move-exception v2

    move v6, v7

    :goto_2
    if-nez v6, :cond_0

    invoke-interface {v1}, Ljava/io/Closeable;->close()V

    :cond_0
    throw v2

    .line 95
    :cond_1
    :try_start_3
    check-cast v2, Ljava/util/List;

    .line 45
    move-object v0, v4

    check-cast v0, Ljava/io/Closeable;

    move-object v3, v0
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    nop

    :try_start_4
    move-object v0, v3

    check-cast v0, Lorg/apache/a/a/c;

    move-object v5, v0

    check-cast v2, Ljava/lang/Iterable;

    invoke-virtual {v4, v2}, Lorg/apache/a/a/c;->b(Ljava/lang/Iterable;)V

    sget-object v2, Lkotlin/n;->a:Lkotlin/n;
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_3

    :try_start_5
    invoke-interface {v3}, Ljava/io/Closeable;->close()V

    .line 46
    nop

    sget-object v2, Lkotlin/n;->a:Lkotlin/n;
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 42
    invoke-interface {v1}, Ljava/io/Closeable;->close()V

    .line 47
    return-void

    .line 45
    :catch_1
    move-exception v2

    nop

    :try_start_6
    invoke-interface {v3}, Ljava/io/Closeable;->close()V
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_2
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    :goto_3
    :try_start_7
    check-cast v2, Ljava/lang/Throwable;

    throw v2
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    :catchall_1
    move-exception v2

    move v4, v7

    :goto_4
    if-nez v4, :cond_2

    :try_start_8
    invoke-interface {v3}, Ljava/io/Closeable;->close()V

    :cond_2
    throw v2
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_0
    .catchall {:try_start_8 .. :try_end_8} :catchall_2

    .line 42
    :catchall_2
    move-exception v2

    goto :goto_2

    .line 45
    :catch_2
    move-exception v4

    goto :goto_3

    .line 42
    :catch_3
    move-exception v3

    goto :goto_1

    .line 45
    :catchall_3
    move-exception v2

    move v4, v6

    goto :goto_4
.end method

.method public static final synthetic b(Lco/uk/getmondo/spending/b/a/a;Ljava/util/List;Ljava/io/File;)V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0, p1, p2}, Lco/uk/getmondo/spending/b/a/a;->b(Ljava/util/List;Ljava/io/File;)V

    return-void
.end method

.method private final b(Ljava/util/List;Ljava/io/File;)V
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<+",
            "Lco/uk/getmondo/d/aj;",
            ">;",
            "Ljava/io/File;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 51
    const-string v5, "!Type:Bank\n"

    .line 53
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_0
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_e

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    move-object v4, v2

    check-cast v4, Lco/uk/getmondo/d/aj;

    .line 54
    iget-object v2, p0, Lco/uk/getmondo/spending/b/a/a;->b:Ljava/text/SimpleDateFormat;

    invoke-virtual {v4}, Lco/uk/getmondo/d/aj;->v()Ljava/util/Date;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v3

    move-object v2, v3

    .line 55
    check-cast v2, Ljava/lang/CharSequence;

    invoke-interface {v2}, Ljava/lang/CharSequence;->length()I

    move-result v2

    if-lez v2, :cond_6

    move v2, v7

    :goto_1
    if-eqz v2, :cond_0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v5, "D"

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    move-object v5, v2

    .line 57
    :cond_0
    invoke-virtual {v4}, Lco/uk/getmondo/d/aj;->g()Lco/uk/getmondo/d/c;

    move-result-object v2

    invoke-virtual {v2}, Lco/uk/getmondo/d/c;->f()D

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(D)Ljava/lang/String;

    move-result-object v3

    move-object v2, v3

    .line 58
    check-cast v2, Ljava/lang/CharSequence;

    invoke-interface {v2}, Ljava/lang/CharSequence;->length()I

    move-result v2

    if-lez v2, :cond_7

    move v2, v7

    :goto_2
    if-eqz v2, :cond_1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v5, "T"

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 60
    :cond_1
    sget-object v2, Lco/uk/getmondo/spending/b/a/a/e;->a:Lco/uk/getmondo/spending/b/a/a/e$a;

    invoke-virtual {v2, v4}, Lco/uk/getmondo/spending/b/a/a/e$a;->b(Lco/uk/getmondo/d/aj;)Ljava/lang/String;

    move-result-object v3

    move-object v2, v3

    .line 61
    check-cast v2, Ljava/lang/CharSequence;

    invoke-interface {v2}, Ljava/lang/CharSequence;->length()I

    move-result v2

    if-lez v2, :cond_8

    move v2, v7

    :goto_3
    if-eqz v2, :cond_2

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v5, "P"

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 63
    :cond_2
    invoke-virtual {v4}, Lco/uk/getmondo/d/aj;->c()Lco/uk/getmondo/d/h;

    move-result-object v2

    invoke-virtual {v2}, Lco/uk/getmondo/d/h;->f()Ljava/lang/String;

    move-result-object v3

    move-object v2, v3

    .line 64
    check-cast v2, Ljava/lang/CharSequence;

    invoke-interface {v2}, Ljava/lang/CharSequence;->length()I

    move-result v2

    if-lez v2, :cond_9

    move v2, v7

    :goto_4
    if-eqz v2, :cond_3

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v5, "L"

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 66
    :cond_3
    invoke-virtual {v4}, Lco/uk/getmondo/d/aj;->e()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_a

    :goto_5
    move-object v2, v3

    .line 67
    check-cast v2, Ljava/lang/CharSequence;

    invoke-interface {v2}, Ljava/lang/CharSequence;->length()I

    move-result v2

    if-lez v2, :cond_b

    move v2, v7

    :goto_6
    if-eqz v2, :cond_4

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v5, "M"

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 69
    :cond_4
    invoke-virtual {v4}, Lco/uk/getmondo/d/aj;->f()Lco/uk/getmondo/d/u;

    move-result-object v2

    if-eqz v2, :cond_c

    sget-object v3, Lco/uk/getmondo/spending/b/a/a/e;->a:Lco/uk/getmondo/spending/b/a/a/e$a;

    const-string v4, "it"

    invoke-static {v2, v4}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v3, v2}, Lco/uk/getmondo/spending/b/a/a/e$a;->a(Lco/uk/getmondo/d/u;)Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_c

    :goto_7
    move-object v2, v3

    .line 70
    check-cast v2, Ljava/lang/CharSequence;

    invoke-interface {v2}, Ljava/lang/CharSequence;->length()I

    move-result v2

    if-lez v2, :cond_d

    move v2, v7

    :goto_8
    if-eqz v2, :cond_5

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, "A"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 72
    :cond_5
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "^\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    goto/16 :goto_0

    :cond_6
    move v2, v6

    .line 55
    goto/16 :goto_1

    :cond_7
    move v2, v6

    .line 58
    goto/16 :goto_2

    :cond_8
    move v2, v6

    .line 61
    goto/16 :goto_3

    :cond_9
    move v2, v6

    .line 64
    goto/16 :goto_4

    .line 66
    :cond_a
    const-string v3, ""

    goto/16 :goto_5

    :cond_b
    move v2, v6

    .line 67
    goto/16 :goto_6

    .line 69
    :cond_c
    const-string v3, ""

    goto :goto_7

    :cond_d
    move v2, v6

    .line 70
    goto :goto_8

    .line 75
    :cond_e
    sget-object v3, Lkotlin/h/d;->e:Ljava/nio/charset/Charset;

    new-instance v2, Ljava/io/ByteArrayInputStream;

    if-nez v5, :cond_f

    new-instance v2, Lkotlin/TypeCastException;

    const-string v3, "null cannot be cast to non-null type java.lang.String"

    invoke-direct {v2, v3}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_f
    invoke-virtual {v5, v3}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object v3

    const-string v4, "(this as java.lang.String).getBytes(charset)"

    invoke-static {v3, v4}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v2, v3}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    check-cast v2, Ljava/io/Closeable;

    nop

    :try_start_0
    move-object v0, v2

    check-cast v0, Ljava/io/ByteArrayInputStream;

    move-object v3, v0

    .line 76
    new-instance v4, Ljava/io/FileOutputStream;

    invoke-direct {v4, p2}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    check-cast v4, Ljava/io/Closeable;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    nop

    :try_start_1
    move-object v0, v4

    check-cast v0, Ljava/io/FileOutputStream;

    move-object v5, v0

    check-cast v3, Ljava/io/InputStream;

    check-cast v5, Ljava/io/OutputStream;

    const/4 v8, 0x0

    const/4 v9, 0x2

    const/4 v10, 0x0

    invoke-static {v3, v5, v8, v9, v10}, Lkotlin/io/a;->a(Ljava/io/InputStream;Ljava/io/OutputStream;IILjava/lang/Object;)J
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_3

    :try_start_2
    invoke-interface {v4}, Ljava/io/Closeable;->close()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    nop

    .line 75
    invoke-interface {v2}, Ljava/io/Closeable;->close()V

    .line 78
    return-void

    .line 76
    :catch_0
    move-exception v3

    nop

    :try_start_3
    invoke-interface {v4}, Ljava/io/Closeable;->close()V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_2
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :goto_9
    :try_start_4
    check-cast v3, Ljava/lang/Throwable;

    throw v3
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :catchall_0
    move-exception v3

    move v5, v7

    :goto_a
    if-nez v5, :cond_10

    :try_start_5
    invoke-interface {v4}, Ljava/io/Closeable;->close()V

    :cond_10
    throw v3
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_1
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 75
    :catch_1
    move-exception v3

    nop

    :try_start_6
    invoke-interface {v2}, Ljava/io/Closeable;->close()V
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_3
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    :goto_b
    :try_start_7
    check-cast v3, Ljava/lang/Throwable;

    throw v3
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    :catchall_1
    move-exception v3

    move v6, v7

    :goto_c
    if-nez v6, :cond_11

    invoke-interface {v2}, Ljava/io/Closeable;->close()V

    :cond_11
    throw v3

    .line 76
    :catch_2
    move-exception v5

    goto :goto_9

    .line 75
    :catch_3
    move-exception v4

    goto :goto_b

    :catchall_2
    move-exception v3

    goto :goto_c

    .line 76
    :catchall_3
    move-exception v3

    move v5, v6

    goto :goto_a
.end method


# virtual methods
.method public final a(Lco/uk/getmondo/spending/b/a/a/a;)Lio/reactivex/v;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lco/uk/getmondo/spending/b/a/a/a;",
            ")",
            "Lio/reactivex/v",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    const-string v0, "exportData"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 26
    new-instance v0, Lco/uk/getmondo/spending/b/a/a$a;

    invoke-direct {v0, p0, p1}, Lco/uk/getmondo/spending/b/a/a$a;-><init>(Lco/uk/getmondo/spending/b/a/a;Lco/uk/getmondo/spending/b/a/a/a;)V

    check-cast v0, Ljava/util/concurrent/Callable;

    invoke-static {v0}, Lio/reactivex/v;->c(Ljava/util/concurrent/Callable;)Lio/reactivex/v;

    move-result-object v0

    const-string v1, "Single.fromCallable {\n  \u2026le.absolutePath\n        }"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method
