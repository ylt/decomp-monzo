.class final Lco/uk/getmondo/spending/b/c$f$3;
.super Ljava/lang/Object;
.source "ExportPresenter.kt"

# interfaces
.implements Lio/reactivex/c/g;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lco/uk/getmondo/spending/b/c$f;->a(Lkotlin/h;)Lio/reactivex/v;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/g",
        "<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "",
        "it",
        "",
        "kotlin.jvm.PlatformType",
        "accept"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field final synthetic a:Lco/uk/getmondo/spending/b/c$f;

.field final synthetic b:Lco/uk/getmondo/spending/b/a/a/b;


# direct methods
.method constructor <init>(Lco/uk/getmondo/spending/b/c$f;Lco/uk/getmondo/spending/b/a/a/b;)V
    .locals 0

    iput-object p1, p0, Lco/uk/getmondo/spending/b/c$f$3;->a:Lco/uk/getmondo/spending/b/c$f;

    iput-object p2, p0, Lco/uk/getmondo/spending/b/c$f$3;->b:Lco/uk/getmondo/spending/b/a/a/b;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 33
    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1}, Lco/uk/getmondo/spending/b/c$f$3;->a(Ljava/lang/String;)V

    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 74
    iget-object v0, p0, Lco/uk/getmondo/spending/b/c$f$3;->a:Lco/uk/getmondo/spending/b/c$f;

    iget-object v0, v0, Lco/uk/getmondo/spending/b/c$f;->a:Lco/uk/getmondo/spending/b/c;

    invoke-static {v0}, Lco/uk/getmondo/spending/b/c;->f(Lco/uk/getmondo/spending/b/c;)Lco/uk/getmondo/common/a;

    move-result-object v0

    .line 75
    sget-object v1, Lco/uk/getmondo/api/model/tracking/Impression;->Companion:Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    iget-object v2, p0, Lco/uk/getmondo/spending/b/c$f$3;->b:Lco/uk/getmondo/spending/b/a/a/b;

    invoke-virtual {v2}, Lco/uk/getmondo/spending/b/a/a/b;->a()Lco/uk/getmondo/spending/b/a/a/b$a;

    move-result-object v2

    invoke-virtual {v2}, Lco/uk/getmondo/spending/b/a/a/b$a;->name()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_0

    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type java.lang.String"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    invoke-virtual {v2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    const-string v3, "(this as java.lang.String).toLowerCase()"

    invoke-static {v2, v3}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v3, 0x1

    invoke-static {v1, v4, v2, v3, v4}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->a(Lco/uk/getmondo/api/model/tracking/Impression$Companion;Ljava/lang/String;Ljava/lang/String;ILjava/lang/Object;)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v1

    .line 74
    invoke-virtual {v0, v1}, Lco/uk/getmondo/common/a;->a(Lco/uk/getmondo/api/model/tracking/Impression;)V

    .line 75
    return-void
.end method
