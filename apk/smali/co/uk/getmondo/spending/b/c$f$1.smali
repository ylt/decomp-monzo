.class final Lco/uk/getmondo/spending/b/c$f$1;
.super Ljava/lang/Object;
.source "ExportPresenter.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lco/uk/getmondo/spending/b/c$f;->a(Lkotlin/h;)Lio/reactivex/v;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h",
        "<TT;TR;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000\u0018\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010!\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0000\u0010\u0000\u001a\u00020\u00012(\u0010\u0002\u001a$\u0012\u000c\u0012\n \u0005*\u0004\u0018\u00010\u00040\u0004 \u0005*\u0010\u0012\u000c\u0012\n \u0005*\u0004\u0018\u00010\u00040\u00040\u00060\u0003H\n\u00a2\u0006\u0002\u0008\u0007"
    }
    d2 = {
        "<anonymous>",
        "Lco/uk/getmondo/spending/export/data/model/ExportData;",
        "it",
        "",
        "Lco/uk/getmondo/model/Transaction;",
        "kotlin.jvm.PlatformType",
        "",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field final synthetic a:Lco/uk/getmondo/spending/b/c$f;

.field final synthetic b:Lco/uk/getmondo/spending/b/a/a/b;

.field final synthetic c:Lco/uk/getmondo/spending/b/a/a/c;


# direct methods
.method constructor <init>(Lco/uk/getmondo/spending/b/c$f;Lco/uk/getmondo/spending/b/a/a/b;Lco/uk/getmondo/spending/b/a/a/c;)V
    .locals 0

    iput-object p1, p0, Lco/uk/getmondo/spending/b/c$f$1;->a:Lco/uk/getmondo/spending/b/c$f;

    iput-object p2, p0, Lco/uk/getmondo/spending/b/c$f$1;->b:Lco/uk/getmondo/spending/b/a/a/b;

    iput-object p3, p0, Lco/uk/getmondo/spending/b/c$f$1;->c:Lco/uk/getmondo/spending/b/a/a/c;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/util/List;)Lco/uk/getmondo/spending/b/a/a/a;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lco/uk/getmondo/d/aj;",
            ">;)",
            "Lco/uk/getmondo/spending/b/a/a/a;"
        }
    .end annotation

    .prologue
    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 70
    new-instance v0, Lco/uk/getmondo/spending/b/a/a/a;

    iget-object v1, p0, Lco/uk/getmondo/spending/b/c$f$1;->b:Lco/uk/getmondo/spending/b/a/a/b;

    const-string v2, "exportOutput"

    invoke-static {v1, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v2, p0, Lco/uk/getmondo/spending/b/c$f$1;->c:Lco/uk/getmondo/spending/b/a/a/c;

    iget-object v3, p0, Lco/uk/getmondo/spending/b/c$f$1;->a:Lco/uk/getmondo/spending/b/c$f;

    iget-object v3, v3, Lco/uk/getmondo/spending/b/c$f;->a:Lco/uk/getmondo/spending/b/c;

    invoke-static {v3}, Lco/uk/getmondo/spending/b/c;->a(Lco/uk/getmondo/spending/b/c;)Lco/uk/getmondo/common/v;

    move-result-object v3

    invoke-virtual {v2, v3}, Lco/uk/getmondo/spending/b/a/a/c;->a(Lco/uk/getmondo/common/v;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2, p1}, Lco/uk/getmondo/spending/b/a/a/a;-><init>(Lco/uk/getmondo/spending/b/a/a/b;Ljava/lang/String;Ljava/util/List;)V

    return-object v0
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 33
    check-cast p1, Ljava/util/List;

    invoke-virtual {p0, p1}, Lco/uk/getmondo/spending/b/c$f$1;->a(Ljava/util/List;)Lco/uk/getmondo/spending/b/a/a/a;

    move-result-object v0

    return-object v0
.end method
