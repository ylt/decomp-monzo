.class final Lco/uk/getmondo/spending/b/c$i;
.super Ljava/lang/Object;
.source "ExportPresenter.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lco/uk/getmondo/spending/b/c;->a(Lco/uk/getmondo/spending/b/c$a;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h",
        "<TT;TR;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n\u00a2\u0006\u0002\u0008\u0004"
    }
    d2 = {
        "<anonymous>",
        "Lco/uk/getmondo/spending/export/data/model/ExportPeriod;",
        "it",
        "Lorg/threeten/bp/LocalDate;",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field final synthetic a:Lco/uk/getmondo/spending/b/c;


# direct methods
.method constructor <init>(Lco/uk/getmondo/spending/b/c;)V
    .locals 0

    iput-object p1, p0, Lco/uk/getmondo/spending/b/c$i;->a:Lco/uk/getmondo/spending/b/c;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lorg/threeten/bp/LocalDate;)Lco/uk/getmondo/spending/b/a/a/c;
    .locals 6

    .prologue
    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 56
    sget-object v0, Lorg/threeten/bp/format/FormatStyle;->d:Lorg/threeten/bp/format/FormatStyle;

    invoke-static {v0}, Lorg/threeten/bp/format/DateTimeFormatter;->a(Lorg/threeten/bp/format/FormatStyle;)Lorg/threeten/bp/format/DateTimeFormatter;

    move-result-object v1

    move-object v0, p1

    check-cast v0, Lorg/threeten/bp/temporal/TemporalAccessor;

    invoke-virtual {v1, v0}, Lorg/threeten/bp/format/DateTimeFormatter;->a(Lorg/threeten/bp/temporal/TemporalAccessor;)Ljava/lang/String;

    move-result-object v0

    .line 57
    iget-object v1, p0, Lco/uk/getmondo/spending/b/c$i;->a:Lco/uk/getmondo/spending/b/c;

    invoke-static {v1}, Lco/uk/getmondo/spending/b/c;->a(Lco/uk/getmondo/spending/b/c;)Lco/uk/getmondo/common/v;

    move-result-object v1

    const v2, 0x7f0a03b0

    invoke-virtual {v1, v2}, Lco/uk/getmondo/common/v;->a(I)Ljava/lang/String;

    move-result-object v1

    .line 58
    new-instance v2, Lco/uk/getmondo/spending/b/a/a/c;

    invoke-static {}, Lorg/threeten/bp/LocalDate;->a()Lorg/threeten/bp/LocalDate;

    move-result-object v3

    const-string v4, "LocalDate.now()"

    invoke-static {v3, v4}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v4, Lkotlin/d/b/ab;->a:Lkotlin/d/b/ab;

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v0, v4, v5

    array-length v0, v4

    invoke-static {v4, v0}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v0

    invoke-static {v1, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "java.lang.String.format(format, *args)"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v1, Lco/uk/getmondo/spending/b/a/a/c$a;->b:Lco/uk/getmondo/spending/b/a/a/c$a;

    invoke-direct {v2, p1, v3, v0, v1}, Lco/uk/getmondo/spending/b/a/a/c;-><init>(Lorg/threeten/bp/LocalDate;Lorg/threeten/bp/LocalDate;Ljava/lang/String;Lco/uk/getmondo/spending/b/a/a/c$a;)V

    return-object v2
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 33
    check-cast p1, Lorg/threeten/bp/LocalDate;

    invoke-virtual {p0, p1}, Lco/uk/getmondo/spending/b/c$i;->a(Lorg/threeten/bp/LocalDate;)Lco/uk/getmondo/spending/b/a/a/c;

    move-result-object v0

    return-object v0
.end method
