.class public final Lco/uk/getmondo/spending/b/c;
.super Lco/uk/getmondo/common/ui/b;
.source "ExportPresenter.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lco/uk/getmondo/spending/b/c$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lco/uk/getmondo/common/ui/b",
        "<",
        "Lco/uk/getmondo/spending/b/c$a;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000D\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0003\u0008\u0007\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00020\u0001:\u0001\u0016B;\u0008\u0007\u0012\u0008\u0008\u0001\u0010\u0003\u001a\u00020\u0004\u0012\u0008\u0008\u0001\u0010\u0005\u001a\u00020\u0004\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b\u0012\u0006\u0010\u000c\u001a\u00020\r\u00a2\u0006\u0002\u0010\u000eJ\u0010\u0010\u000f\u001a\u00020\u00102\u0006\u0010\u0011\u001a\u00020\u0012H\u0002J\u0010\u0010\u0013\u001a\u00020\u00142\u0006\u0010\u0015\u001a\u00020\u0002H\u0016R\u000e\u0010\u000c\u001a\u00020\rX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0017"
    }
    d2 = {
        "Lco/uk/getmondo/spending/export/ExportPresenter;",
        "Lco/uk/getmondo/common/ui/BasePresenter;",
        "Lco/uk/getmondo/spending/export/ExportPresenter$View;",
        "ioScheduler",
        "Lio/reactivex/Scheduler;",
        "uiScheduler",
        "spendingRepository",
        "Lco/uk/getmondo/spending/data/SpendingRepository;",
        "resourceProvider",
        "Lco/uk/getmondo/common/ResourceProvider;",
        "transactionExporter",
        "Lco/uk/getmondo/spending/export/data/TransactionExporter;",
        "analyticsService",
        "Lco/uk/getmondo/common/AnalyticsService;",
        "(Lio/reactivex/Scheduler;Lio/reactivex/Scheduler;Lco/uk/getmondo/spending/data/SpendingRepository;Lco/uk/getmondo/common/ResourceProvider;Lco/uk/getmondo/spending/export/data/TransactionExporter;Lco/uk/getmondo/common/AnalyticsService;)V",
        "getPeriodDisplayName",
        "",
        "yearMonth",
        "Lorg/threeten/bp/YearMonth;",
        "register",
        "",
        "view",
        "View",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field private final c:Lio/reactivex/u;

.field private final d:Lio/reactivex/u;

.field private final e:Lco/uk/getmondo/spending/a/i;

.field private final f:Lco/uk/getmondo/common/v;

.field private final g:Lco/uk/getmondo/spending/b/a/a;

.field private final h:Lco/uk/getmondo/common/a;


# direct methods
.method public constructor <init>(Lio/reactivex/u;Lio/reactivex/u;Lco/uk/getmondo/spending/a/i;Lco/uk/getmondo/common/v;Lco/uk/getmondo/spending/b/a/a;Lco/uk/getmondo/common/a;)V
    .locals 1

    .prologue
    const-string v0, "ioScheduler"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "uiScheduler"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "spendingRepository"

    invoke-static {p3, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "resourceProvider"

    invoke-static {p4, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "transactionExporter"

    invoke-static {p5, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "analyticsService"

    invoke-static {p6, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 41
    invoke-direct {p0}, Lco/uk/getmondo/common/ui/b;-><init>()V

    iput-object p1, p0, Lco/uk/getmondo/spending/b/c;->c:Lio/reactivex/u;

    iput-object p2, p0, Lco/uk/getmondo/spending/b/c;->d:Lio/reactivex/u;

    iput-object p3, p0, Lco/uk/getmondo/spending/b/c;->e:Lco/uk/getmondo/spending/a/i;

    iput-object p4, p0, Lco/uk/getmondo/spending/b/c;->f:Lco/uk/getmondo/common/v;

    iput-object p5, p0, Lco/uk/getmondo/spending/b/c;->g:Lco/uk/getmondo/spending/b/a/a;

    iput-object p6, p0, Lco/uk/getmondo/spending/b/c;->h:Lco/uk/getmondo/common/a;

    return-void
.end method

.method public static final synthetic a(Lco/uk/getmondo/spending/b/c;)Lco/uk/getmondo/common/v;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lco/uk/getmondo/spending/b/c;->f:Lco/uk/getmondo/common/v;

    return-object v0
.end method

.method public static final synthetic a(Lco/uk/getmondo/spending/b/c;Lorg/threeten/bp/YearMonth;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 33
    invoke-direct {p0, p1}, Lco/uk/getmondo/spending/b/c;->a(Lorg/threeten/bp/YearMonth;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private final a(Lorg/threeten/bp/YearMonth;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 86
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Lorg/threeten/bp/YearMonth;->c()Lorg/threeten/bp/Month;

    move-result-object v1

    sget-object v2, Lorg/threeten/bp/format/TextStyle;->a:Lorg/threeten/bp/format/TextStyle;

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lorg/threeten/bp/Month;->a(Lorg/threeten/bp/format/TextStyle;Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x20

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Lorg/threeten/bp/YearMonth;->b()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static final synthetic b(Lco/uk/getmondo/spending/b/c;)Lco/uk/getmondo/spending/a/i;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lco/uk/getmondo/spending/b/c;->e:Lco/uk/getmondo/spending/a/i;

    return-object v0
.end method

.method public static final synthetic c(Lco/uk/getmondo/spending/b/c;)Lco/uk/getmondo/spending/b/a/a;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lco/uk/getmondo/spending/b/c;->g:Lco/uk/getmondo/spending/b/a/a;

    return-object v0
.end method

.method public static final synthetic d(Lco/uk/getmondo/spending/b/c;)Lio/reactivex/u;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lco/uk/getmondo/spending/b/c;->c:Lio/reactivex/u;

    return-object v0
.end method

.method public static final synthetic e(Lco/uk/getmondo/spending/b/c;)Lio/reactivex/u;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lco/uk/getmondo/spending/b/c;->d:Lio/reactivex/u;

    return-object v0
.end method

.method public static final synthetic f(Lco/uk/getmondo/spending/b/c;)Lco/uk/getmondo/common/a;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lco/uk/getmondo/spending/b/c;->h:Lco/uk/getmondo/common/a;

    return-object v0
.end method


# virtual methods
.method public bridge synthetic a(Lco/uk/getmondo/common/ui/f;)V
    .locals 0

    .prologue
    .line 33
    check-cast p1, Lco/uk/getmondo/spending/b/c$a;

    invoke-virtual {p0, p1}, Lco/uk/getmondo/spending/b/c;->a(Lco/uk/getmondo/spending/b/c$a;)V

    return-void
.end method

.method public a(Lco/uk/getmondo/spending/b/c$a;)V
    .locals 4

    .prologue
    const-string v0, "view"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v0, p1

    .line 44
    check-cast v0, Lco/uk/getmondo/common/ui/f;

    invoke-super {p0, v0}, Lco/uk/getmondo/common/ui/b;->a(Lco/uk/getmondo/common/ui/f;)V

    .line 46
    invoke-interface {p1}, Lco/uk/getmondo/spending/b/c$a;->c()Lio/reactivex/v;

    move-result-object v1

    .line 47
    new-instance v0, Lco/uk/getmondo/spending/b/c$j;

    invoke-direct {v0, p0}, Lco/uk/getmondo/spending/b/c$j;-><init>(Lco/uk/getmondo/spending/b/c;)V

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {v1, v0}, Lio/reactivex/v;->d(Lio/reactivex/c/h;)Lio/reactivex/v;

    move-result-object v2

    .line 54
    iget-object v0, p0, Lco/uk/getmondo/spending/b/c;->e:Lco/uk/getmondo/spending/a/i;

    invoke-virtual {v0}, Lco/uk/getmondo/spending/a/i;->b()Lio/reactivex/v;

    move-result-object v1

    .line 55
    new-instance v0, Lco/uk/getmondo/spending/b/c$i;

    invoke-direct {v0, p0}, Lco/uk/getmondo/spending/b/c$i;-><init>(Lco/uk/getmondo/spending/b/c;)V

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {v1, v0}, Lio/reactivex/v;->d(Lio/reactivex/c/h;)Lio/reactivex/v;

    move-result-object v0

    .line 61
    iget-object v3, p0, Lco/uk/getmondo/spending/b/c;->b:Lio/reactivex/b/a;

    .line 62
    const-string v1, "allExportPeriod"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lio/reactivex/z;

    .line 101
    new-instance v1, Lco/uk/getmondo/spending/b/c$b;

    invoke-direct {v1}, Lco/uk/getmondo/spending/b/c$b;-><init>()V

    check-cast v1, Lio/reactivex/c/c;

    invoke-virtual {v2, v0, v1}, Lio/reactivex/v;->a(Lio/reactivex/z;Lio/reactivex/c/c;)Lio/reactivex/v;

    move-result-object v1

    const-string v0, "zipWith(other, BiFunctio\u2026-> zipper.invoke(t, u) })"

    invoke-static {v1, v0}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 63
    new-instance v0, Lco/uk/getmondo/spending/b/c$d;

    invoke-direct {v0, p1}, Lco/uk/getmondo/spending/b/c$d;-><init>(Lco/uk/getmondo/spending/b/c$a;)V

    check-cast v0, Lio/reactivex/c/g;

    invoke-virtual {v1, v0}, Lio/reactivex/v;->e(Lio/reactivex/c/g;)Lio/reactivex/b/b;

    move-result-object v0

    const-string v1, "monthExportPeriods\n     \u2026Periods(exportPeriods) })"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 61
    invoke-static {v3, v0}, Lco/uk/getmondo/common/j/f;->a(Lio/reactivex/b/a;Lio/reactivex/b/b;)Lio/reactivex/b/a;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/spending/b/c;->b:Lio/reactivex/b/a;

    .line 65
    iget-object v2, p0, Lco/uk/getmondo/spending/b/c;->b:Lio/reactivex/b/a;

    .line 77
    invoke-interface {p1}, Lco/uk/getmondo/spending/b/c$a;->d()Lio/reactivex/n;

    move-result-object v1

    .line 66
    new-instance v0, Lco/uk/getmondo/spending/b/c$e;

    invoke-direct {v0, p1}, Lco/uk/getmondo/spending/b/c$e;-><init>(Lco/uk/getmondo/spending/b/c$a;)V

    check-cast v0, Lio/reactivex/c/g;

    invoke-virtual {v1, v0}, Lio/reactivex/n;->doOnNext(Lio/reactivex/c/g;)Lio/reactivex/n;

    move-result-object v3

    .line 67
    invoke-interface {p1}, Lco/uk/getmondo/spending/b/c$a;->e()Lio/reactivex/n;

    move-result-object v0

    check-cast v0, Lio/reactivex/r;

    .line 102
    new-instance v1, Lco/uk/getmondo/spending/b/c$c;

    invoke-direct {v1}, Lco/uk/getmondo/spending/b/c$c;-><init>()V

    check-cast v1, Lio/reactivex/c/c;

    invoke-virtual {v3, v0, v1}, Lio/reactivex/n;->zipWith(Lio/reactivex/r;Lio/reactivex/c/c;)Lio/reactivex/n;

    move-result-object v1

    const-string v0, "zipWith(other, BiFunctio\u2026-> zipper.invoke(t, u) })"

    invoke-static {v1, v0}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 68
    new-instance v0, Lco/uk/getmondo/spending/b/c$f;

    invoke-direct {v0, p0}, Lco/uk/getmondo/spending/b/c$f;-><init>(Lco/uk/getmondo/spending/b/c;)V

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {v1, v0}, Lio/reactivex/n;->flatMapSingle(Lio/reactivex/c/h;)Lio/reactivex/n;

    move-result-object v3

    .line 78
    new-instance v0, Lco/uk/getmondo/spending/b/c$g;

    invoke-direct {v0, p1}, Lco/uk/getmondo/spending/b/c$g;-><init>(Lco/uk/getmondo/spending/b/c$a;)V

    check-cast v0, Lio/reactivex/c/g;

    .line 79
    new-instance v1, Lco/uk/getmondo/spending/b/c$h;

    invoke-direct {v1, p1}, Lco/uk/getmondo/spending/b/c$h;-><init>(Lco/uk/getmondo/spending/b/c$a;)V

    check-cast v1, Lio/reactivex/c/g;

    .line 77
    invoke-virtual {v3, v0, v1}, Lio/reactivex/n;->subscribe(Lio/reactivex/c/g;Lio/reactivex/c/g;)Lio/reactivex/b/b;

    move-result-object v0

    const-string v1, "view.exportPeriodClicks\n\u2026                       })"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 65
    invoke-static {v2, v0}, Lco/uk/getmondo/common/j/f;->a(Lio/reactivex/b/a;Lio/reactivex/b/b;)Lio/reactivex/b/a;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/spending/b/c;->b:Lio/reactivex/b/a;

    .line 83
    return-void
.end method
