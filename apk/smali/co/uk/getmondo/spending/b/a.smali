.class public final Lco/uk/getmondo/spending/b/a;
.super Landroid/support/design/widget/c;
.source "ExportBottomSheet.kt"

# interfaces
.implements Lco/uk/getmondo/spending/b/c$a;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000|\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0008\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0006\n\u0002\u0018\u0002\n\u0002\u0008\u0005\u0018\u00002\u00020\u00012\u00020\u0002B\u001d\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u000e\u0008\u0002\u0010\u0005\u001a\u0008\u0012\u0004\u0012\u00020\u00070\u0006\u00a2\u0006\u0002\u0010\u0008J2\u0010!\u001a\u00020\"2\u0006\u0010#\u001a\u00020$2\u000c\u0010%\u001a\u0008\u0012\u0004\u0012\u00020$0\u00062\u0012\u0010&\u001a\u000e\u0012\u0004\u0012\u00020(\u0012\u0004\u0012\u00020\"0\'H\u0002J\u0008\u0010)\u001a\u00020\"H\u0016J\u0012\u0010*\u001a\u00020\"2\u0008\u0010+\u001a\u0004\u0018\u00010,H\u0014J\u0008\u0010-\u001a\u00020\"H\u0016J\u0010\u0010.\u001a\u00020\"2\u0006\u0010/\u001a\u00020(H\u0016J\u0012\u0010.\u001a\u00020\"2\u0008\u00100\u001a\u0004\u0018\u00010$H\u0016J\u0016\u00101\u001a\u00020\"2\u000c\u00102\u001a\u0008\u0012\u0004\u0012\u0002030\u0006H\u0016J\u0016\u00104\u001a\u00020\"2\u000c\u00105\u001a\u0008\u0012\u0004\u0012\u00020\u00120\u0006H\u0016J\u0010\u00106\u001a\u00020\"2\u0006\u00107\u001a\u00020$H\u0016R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001a\u0010\t\u001a\u0008\u0012\u0004\u0012\u00020\u000b0\n8VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u000c\u0010\rR2\u0010\u000e\u001a&\u0012\u000c\u0012\n \u0010*\u0004\u0018\u00010\u000b0\u000b \u0010*\u0012\u0012\u000c\u0012\n \u0010*\u0004\u0018\u00010\u000b0\u000b\u0018\u00010\u000f0\u000fX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001a\u0010\u0011\u001a\u0008\u0012\u0004\u0012\u00020\u00120\n8VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0013\u0010\rR2\u0010\u0014\u001a&\u0012\u000c\u0012\n \u0010*\u0004\u0018\u00010\u00120\u0012 \u0010*\u0012\u0012\u000c\u0012\n \u0010*\u0004\u0018\u00010\u00120\u0012\u0018\u00010\u000f0\u000fX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0005\u001a\u0008\u0012\u0004\u0012\u00020\u00070\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000R \u0010\u0015\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00070\u00060\u00168VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0017\u0010\u0018R\u001e\u0010\u0019\u001a\u00020\u001a8\u0000@\u0000X\u0081.\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u001b\u0010\u001c\"\u0004\u0008\u001d\u0010\u001eR\u000e\u0010\u001f\u001a\u00020 X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u00068"
    }
    d2 = {
        "Lco/uk/getmondo/spending/export/ExportBottomSheet;",
        "Landroid/support/design/widget/BottomSheetDialog;",
        "Lco/uk/getmondo/spending/export/ExportPresenter$View;",
        "activity",
        "Landroid/app/Activity;",
        "initialMonths",
        "",
        "Lorg/threeten/bp/YearMonth;",
        "(Landroid/app/Activity;Ljava/util/List;)V",
        "exportOutputClicks",
        "Lio/reactivex/Observable;",
        "Lco/uk/getmondo/spending/export/data/model/ExportOutput;",
        "getExportOutputClicks",
        "()Lio/reactivex/Observable;",
        "exportOutputRelay",
        "Lcom/jakewharton/rxrelay2/PublishRelay;",
        "kotlin.jvm.PlatformType",
        "exportPeriodClicks",
        "Lco/uk/getmondo/spending/export/data/model/ExportPeriod;",
        "getExportPeriodClicks",
        "exportPeriodRelay",
        "months",
        "Lio/reactivex/Single;",
        "getMonths",
        "()Lio/reactivex/Single;",
        "presenter",
        "Lco/uk/getmondo/spending/export/ExportPresenter;",
        "getPresenter$app_monzoPrepaidRelease",
        "()Lco/uk/getmondo/spending/export/ExportPresenter;",
        "setPresenter$app_monzoPrepaidRelease",
        "(Lco/uk/getmondo/spending/export/ExportPresenter;)V",
        "sheetView",
        "Landroid/view/ViewGroup;",
        "bind",
        "",
        "title",
        "",
        "items",
        "clickListener",
        "Lkotlin/Function1;",
        "",
        "onAttachedToWindow",
        "onCreate",
        "savedInstanceState",
        "Landroid/os/Bundle;",
        "onDetachedFromWindow",
        "showError",
        "stringRes",
        "message",
        "showExportFormats",
        "formats",
        "Lco/uk/getmondo/spending/export/data/model/ExportOutput$Format;",
        "showExportPeriods",
        "periods",
        "showExportShare",
        "filePathname",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field public b:Lco/uk/getmondo/spending/b/c;

.field private final c:Lcom/b/b/c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/b/b/c",
            "<",
            "Lco/uk/getmondo/spending/b/a/a/c;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Lcom/b/b/c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/b/b/c",
            "<",
            "Lco/uk/getmondo/spending/b/a/a/b;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Landroid/view/ViewGroup;

.field private final f:Landroid/app/Activity;

.field private final g:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lorg/threeten/bp/YearMonth;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/app/Activity;Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Activity;",
            "Ljava/util/List",
            "<",
            "Lorg/threeten/bp/YearMonth;",
            ">;)V"
        }
    .end annotation

    .prologue
    const-string v0, "activity"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "initialMonths"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v0, p1

    .line 33
    check-cast v0, Landroid/content/Context;

    invoke-direct {p0, v0}, Landroid/support/design/widget/c;-><init>(Landroid/content/Context;)V

    iput-object p1, p0, Lco/uk/getmondo/spending/b/a;->f:Landroid/app/Activity;

    iput-object p2, p0, Lco/uk/getmondo/spending/b/a;->g:Ljava/util/List;

    .line 37
    invoke-static {}, Lcom/b/b/c;->a()Lcom/b/b/c;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/spending/b/a;->c:Lcom/b/b/c;

    .line 38
    invoke-static {}, Lcom/b/b/c;->a()Lcom/b/b/c;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/spending/b/a;->d:Lcom/b/b/c;

    .line 43
    invoke-virtual {p0}, Lco/uk/getmondo/spending/b/a;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f05007a

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    if-nez v0, :cond_0

    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type android.widget.LinearLayout"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    check-cast v0, Landroid/widget/LinearLayout;

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lco/uk/getmondo/spending/b/a;->e:Landroid/view/ViewGroup;

    .line 44
    iget-object v0, p0, Lco/uk/getmondo/spending/b/a;->e:Landroid/view/ViewGroup;

    check-cast v0, Landroid/view/View;

    invoke-virtual {p0, v0}, Lco/uk/getmondo/spending/b/a;->setContentView(Landroid/view/View;)V

    return-void
.end method

.method public static final synthetic a(Lco/uk/getmondo/spending/b/a;)Lcom/b/b/c;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lco/uk/getmondo/spending/b/a;->c:Lcom/b/b/c;

    return-object v0
.end method

.method private final a(Ljava/lang/String;Ljava/util/List;Lkotlin/d/a/b;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Lkotlin/d/a/b",
            "<-",
            "Ljava/lang/Integer;",
            "Lkotlin/n;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 110
    iget-object v0, p0, Lco/uk/getmondo/spending/b/a;->e:Landroid/view/ViewGroup;

    check-cast v0, Landroid/view/View;

    sget v1, Lco/uk/getmondo/c$a;->exportSheetTitleText:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    check-cast p1, Ljava/lang/CharSequence;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 112
    iget-object v0, p0, Lco/uk/getmondo/spending/b/a;->e:Landroid/view/ViewGroup;

    const/4 v1, 0x1

    iget-object v2, p0, Lco/uk/getmondo/spending/b/a;->e:Landroid/view/ViewGroup;

    invoke-virtual {v2}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v0, v1, v2}, Landroid/view/ViewGroup;->removeViews(II)V

    .line 114
    check-cast p2, Ljava/lang/Iterable;

    .line 133
    const/4 v0, 0x0

    .line 134
    invoke-interface {p2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move v2, v0

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    add-int/lit8 v3, v2, 0x1

    check-cast v0, Ljava/lang/String;

    .line 115
    iget-object v1, p0, Lco/uk/getmondo/spending/b/a;->e:Landroid/view/ViewGroup;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    const v5, 0x7f0500f5

    const/4 v6, 0x0

    invoke-static {v1, v5, v6}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    if-nez v1, :cond_0

    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type android.widget.TextView"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    check-cast v1, Landroid/widget/TextView;

    .line 116
    check-cast v0, Ljava/lang/CharSequence;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 117
    new-instance v0, Lco/uk/getmondo/spending/b/a$a;

    invoke-direct {v0, v2, p0, p3}, Lco/uk/getmondo/spending/b/a$a;-><init>(ILco/uk/getmondo/spending/b/a;Lkotlin/d/a/b;)V

    check-cast v0, Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 121
    iget-object v0, p0, Lco/uk/getmondo/spending/b/a;->e:Landroid/view/ViewGroup;

    check-cast v1, Landroid/view/View;

    add-int/lit8 v2, v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;I)V

    .line 122
    nop

    nop

    move v2, v3

    goto :goto_0

    .line 135
    :cond_1
    nop

    .line 123
    return-void
.end method

.method public static final synthetic b(Lco/uk/getmondo/spending/b/a;)Lcom/b/b/c;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lco/uk/getmondo/spending/b/a;->d:Lcom/b/b/c;

    return-object v0
.end method


# virtual methods
.method public a(Ljava/lang/String;)V
    .locals 4

    .prologue
    const-string v0, "filePathname"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 87
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 88
    invoke-virtual {p0}, Lco/uk/getmondo/spending/b/a;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {p0}, Lco/uk/getmondo/spending/b/a;->getContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f0a018b

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Landroid/support/v4/content/FileProvider;->a(Landroid/content/Context;Ljava/lang/String;Ljava/io/File;)Landroid/net/Uri;

    move-result-object v0

    .line 90
    iget-object v1, p0, Lco/uk/getmondo/spending/b/a;->f:Landroid/app/Activity;

    invoke-static {v1}, Landroid/support/v4/app/ao$a;->a(Landroid/app/Activity;)Landroid/support/v4/app/ao$a;

    move-result-object v1

    .line 91
    invoke-virtual {v1, v0}, Landroid/support/v4/app/ao$a;->a(Landroid/net/Uri;)Landroid/support/v4/app/ao$a;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/ao$a;->a()Landroid/content/Intent;

    move-result-object v1

    .line 94
    invoke-virtual {v1, v0}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 95
    const/4 v0, 0x1

    invoke-virtual {v1, v0}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 96
    nop

    .line 97
    invoke-virtual {p0}, Lco/uk/getmondo/spending/b/a;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 98
    invoke-virtual {p0}, Lco/uk/getmondo/spending/b/a;->dismiss()V

    .line 99
    return-void
.end method

.method public a(Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lco/uk/getmondo/spending/b/a/a/c;",
            ">;)V"
        }
    .end annotation

    .prologue
    const-string v0, "periods"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 74
    invoke-virtual {p0}, Lco/uk/getmondo/spending/b/a;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0a03b1

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    const-string v0, "context.getString(R.stri\u2026ding_export_period_title)"

    invoke-static {v2, v0}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v0, p1

    .line 75
    check-cast v0, Ljava/lang/Iterable;

    .line 125
    new-instance v1, Ljava/util/ArrayList;

    const/16 v3, 0xa

    invoke-static {v0, v3}, Lkotlin/a/m;->a(Ljava/lang/Iterable;I)I

    move-result v3

    invoke-direct {v1, v3}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v1, Ljava/util/Collection;

    .line 126
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .line 127
    check-cast v0, Lco/uk/getmondo/spending/b/a/a/c;

    .line 75
    invoke-virtual {v0}, Lco/uk/getmondo/spending/b/a/a/c;->c()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 128
    :cond_0
    check-cast v1, Ljava/util/List;

    .line 76
    new-instance v0, Lco/uk/getmondo/spending/b/a$d;

    invoke-direct {v0, p0, p1}, Lco/uk/getmondo/spending/b/a$d;-><init>(Lco/uk/getmondo/spending/b/a;Ljava/util/List;)V

    check-cast v0, Lkotlin/d/a/b;

    .line 74
    invoke-direct {p0, v2, v1, v0}, Lco/uk/getmondo/spending/b/a;->a(Ljava/lang/String;Ljava/util/List;Lkotlin/d/a/b;)V

    .line 77
    return-void
.end method

.method public b(I)V
    .locals 1

    .prologue
    .line 106
    invoke-virtual {p0}, Lco/uk/getmondo/spending/b/a;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lco/uk/getmondo/spending/b/a;->b(Ljava/lang/String;)V

    .line 107
    return-void
.end method

.method public b(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 102
    iget-object v0, p0, Lco/uk/getmondo/spending/b/a;->f:Landroid/app/Activity;

    instance-of v1, v0, Lco/uk/getmondo/common/activities/b;

    if-nez v1, :cond_0

    const/4 v0, 0x0

    :cond_0
    check-cast v0, Lco/uk/getmondo/common/activities/b;

    if-eqz v0, :cond_1

    invoke-virtual {v0, p1}, Lco/uk/getmondo/common/activities/b;->b(Ljava/lang/String;)V

    .line 103
    :cond_1
    return-void
.end method

.method public b(Ljava/util/List;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<+",
            "Lco/uk/getmondo/spending/b/a/a/b$a;",
            ">;)V"
        }
    .end annotation

    .prologue
    const-string v0, "formats"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 80
    invoke-virtual {p0}, Lco/uk/getmondo/spending/b/a;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0a03af

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    const-string v0, "context.getString(R.stri\u2026ding_export_format_title)"

    invoke-static {v2, v0}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v0, p1

    .line 81
    check-cast v0, Ljava/lang/Iterable;

    .line 129
    new-instance v1, Ljava/util/ArrayList;

    const/16 v3, 0xa

    invoke-static {v0, v3}, Lkotlin/a/m;->a(Ljava/lang/Iterable;I)I

    move-result v3

    invoke-direct {v1, v3}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v1, Ljava/util/Collection;

    .line 130
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .line 131
    check-cast v0, Lco/uk/getmondo/spending/b/a/a/b$a;

    .line 81
    invoke-virtual {p0}, Lco/uk/getmondo/spending/b/a;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v0}, Lco/uk/getmondo/spending/b/a/a/b$a;->a()I

    move-result v0

    invoke-virtual {v4, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 132
    :cond_0
    check-cast v1, Ljava/util/List;

    .line 82
    new-instance v0, Lco/uk/getmondo/spending/b/a$b;

    invoke-direct {v0, p0, p1}, Lco/uk/getmondo/spending/b/a$b;-><init>(Lco/uk/getmondo/spending/b/a;Ljava/util/List;)V

    check-cast v0, Lkotlin/d/a/b;

    .line 80
    invoke-direct {p0, v2, v1, v0}, Lco/uk/getmondo/spending/b/a;->a(Ljava/lang/String;Ljava/util/List;Lkotlin/d/a/b;)V

    .line 83
    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    new-instance v0, Lco/uk/getmondo/spending/b/a$c;

    invoke-direct {v0, p0}, Lco/uk/getmondo/spending/b/a$c;-><init>(Lco/uk/getmondo/spending/b/a;)V

    check-cast v0, Ljava/lang/Runnable;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 84
    return-void
.end method

.method public c()Lio/reactivex/v;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/v",
            "<",
            "Ljava/util/List",
            "<",
            "Lorg/threeten/bp/YearMonth;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 67
    iget-object v0, p0, Lco/uk/getmondo/spending/b/a;->g:Ljava/util/List;

    invoke-static {v0}, Lio/reactivex/v;->a(Ljava/lang/Object;)Lio/reactivex/v;

    move-result-object v0

    const-string v1, "Single.just(initialMonths)"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public d()Lio/reactivex/n;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Lco/uk/getmondo/spending/b/a/a/c;",
            ">;"
        }
    .end annotation

    .prologue
    .line 69
    iget-object v0, p0, Lco/uk/getmondo/spending/b/a;->c:Lcom/b/b/c;

    const-string v1, "exportPeriodRelay"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lio/reactivex/n;

    return-object v0
.end method

.method public e()Lio/reactivex/n;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Lco/uk/getmondo/spending/b/a/a/b;",
            ">;"
        }
    .end annotation

    .prologue
    .line 71
    iget-object v0, p0, Lco/uk/getmondo/spending/b/a;->d:Lcom/b/b/c;

    const-string v1, "exportOutputRelay"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lio/reactivex/n;

    return-object v0
.end method

.method public onAttachedToWindow()V
    .locals 2

    .prologue
    .line 58
    invoke-super {p0}, Landroid/support/design/widget/c;->onAttachedToWindow()V

    .line 59
    iget-object v0, p0, Lco/uk/getmondo/spending/b/a;->b:Lco/uk/getmondo/spending/b/c;

    if-nez v0, :cond_0

    const-string v1, "presenter"

    invoke-static {v1}, Lkotlin/d/b/l;->b(Ljava/lang/String;)V

    :cond_0
    check-cast p0, Lco/uk/getmondo/spending/b/c$a;

    invoke-virtual {v0, p0}, Lco/uk/getmondo/spending/b/c;->a(Lco/uk/getmondo/spending/b/c$a;)V

    .line 60
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 48
    invoke-super {p0, p1}, Landroid/support/design/widget/c;->onCreate(Landroid/os/Bundle;)V

    .line 50
    invoke-static {}, Lco/uk/getmondo/common/h/b/a;->a()Lco/uk/getmondo/common/h/b/a$d;

    move-result-object v0

    .line 51
    invoke-virtual {p0}, Lco/uk/getmondo/spending/b/a;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lco/uk/getmondo/MonzoApplication;->a(Landroid/content/Context;)Lco/uk/getmondo/MonzoApplication;

    move-result-object v1

    invoke-virtual {v1}, Lco/uk/getmondo/MonzoApplication;->b()Lco/uk/getmondo/common/h/a/a;

    move-result-object v1

    invoke-virtual {v0, v1}, Lco/uk/getmondo/common/h/b/a$d;->a(Lco/uk/getmondo/common/h/a/a;)Lco/uk/getmondo/common/h/b/a$d;

    move-result-object v0

    .line 52
    new-instance v1, Lco/uk/getmondo/common/h/b/c;

    iget-object v2, p0, Lco/uk/getmondo/spending/b/a;->f:Landroid/app/Activity;

    invoke-direct {v1, v2}, Lco/uk/getmondo/common/h/b/c;-><init>(Landroid/app/Activity;)V

    invoke-virtual {v0, v1}, Lco/uk/getmondo/common/h/b/a$d;->a(Lco/uk/getmondo/common/h/b/c;)Lco/uk/getmondo/common/h/b/a$d;

    move-result-object v0

    .line 53
    invoke-virtual {v0}, Lco/uk/getmondo/common/h/b/a$d;->a()Lco/uk/getmondo/common/h/b/b;

    move-result-object v0

    .line 54
    invoke-interface {v0, p0}, Lco/uk/getmondo/common/h/b/b;->a(Lco/uk/getmondo/spending/b/a;)V

    .line 55
    return-void
.end method

.method public onDetachedFromWindow()V
    .locals 2

    .prologue
    .line 63
    iget-object v0, p0, Lco/uk/getmondo/spending/b/a;->b:Lco/uk/getmondo/spending/b/c;

    if-nez v0, :cond_0

    const-string v1, "presenter"

    invoke-static {v1}, Lkotlin/d/b/l;->b(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0}, Lco/uk/getmondo/spending/b/c;->b()V

    .line 64
    invoke-super {p0}, Landroid/support/design/widget/c;->onDetachedFromWindow()V

    .line 65
    return-void
.end method
