.class final Lco/uk/getmondo/spending/b/c$f;
.super Ljava/lang/Object;
.source "ExportPresenter.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lco/uk/getmondo/spending/b/c;->a(Lco/uk/getmondo/spending/b/c$a;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h",
        "<TT;",
        "Lio/reactivex/z",
        "<+TR;>;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000\u001c\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a&\u0012\u000c\u0012\n \u0003*\u0004\u0018\u00010\u00020\u0002 \u0003*\u0012\u0012\u000c\u0012\n \u0003*\u0004\u0018\u00010\u00020\u0002\u0018\u00010\u00010\u00012\"\u0010\u0004\u001a\u001e\u0012\u000c\u0012\n \u0003*\u0004\u0018\u00010\u00060\u0006\u0012\u000c\u0012\n \u0003*\u0004\u0018\u00010\u00070\u00070\u0005H\n\u00a2\u0006\u0002\u0008\u0008"
    }
    d2 = {
        "<anonymous>",
        "Lio/reactivex/Single;",
        "",
        "kotlin.jvm.PlatformType",
        "<name for destructuring parameter 0>",
        "Lkotlin/Pair;",
        "Lco/uk/getmondo/spending/export/data/model/ExportPeriod;",
        "Lco/uk/getmondo/spending/export/data/model/ExportOutput;",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field final synthetic a:Lco/uk/getmondo/spending/b/c;


# direct methods
.method constructor <init>(Lco/uk/getmondo/spending/b/c;)V
    .locals 0

    iput-object p1, p0, Lco/uk/getmondo/spending/b/c$f;->a:Lco/uk/getmondo/spending/b/c;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lkotlin/h;)Lio/reactivex/v;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/h",
            "<",
            "Lco/uk/getmondo/spending/b/a/a/c;",
            "Lco/uk/getmondo/spending/b/a/a/b;",
            ">;)",
            "Lio/reactivex/v",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    const-string v0, "<name for destructuring parameter 0>"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lkotlin/h;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/spending/b/a/a/c;

    invoke-virtual {p1}, Lkotlin/h;->d()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lco/uk/getmondo/spending/b/a/a/b;

    .line 69
    iget-object v2, p0, Lco/uk/getmondo/spending/b/c$f;->a:Lco/uk/getmondo/spending/b/c;

    invoke-static {v2}, Lco/uk/getmondo/spending/b/c;->b(Lco/uk/getmondo/spending/b/c;)Lco/uk/getmondo/spending/a/i;

    move-result-object v2

    invoke-virtual {v0}, Lco/uk/getmondo/spending/b/a/a/c;->a()Lorg/threeten/bp/LocalDate;

    move-result-object v3

    invoke-virtual {v0}, Lco/uk/getmondo/spending/b/a/a/c;->b()Lorg/threeten/bp/LocalDate;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lco/uk/getmondo/spending/a/i;->a(Lorg/threeten/bp/LocalDate;Lorg/threeten/bp/LocalDate;)Lio/reactivex/v;

    move-result-object v3

    .line 70
    new-instance v2, Lco/uk/getmondo/spending/b/c$f$1;

    invoke-direct {v2, p0, v1, v0}, Lco/uk/getmondo/spending/b/c$f$1;-><init>(Lco/uk/getmondo/spending/b/c$f;Lco/uk/getmondo/spending/b/a/a/b;Lco/uk/getmondo/spending/b/a/a/c;)V

    move-object v0, v2

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {v3, v0}, Lio/reactivex/v;->d(Lio/reactivex/c/h;)Lio/reactivex/v;

    move-result-object v2

    .line 71
    new-instance v0, Lco/uk/getmondo/spending/b/c$f$2;

    invoke-direct {v0, p0}, Lco/uk/getmondo/spending/b/c$f$2;-><init>(Lco/uk/getmondo/spending/b/c$f;)V

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {v2, v0}, Lio/reactivex/v;->a(Lio/reactivex/c/h;)Lio/reactivex/v;

    move-result-object v0

    .line 72
    iget-object v2, p0, Lco/uk/getmondo/spending/b/c$f;->a:Lco/uk/getmondo/spending/b/c;

    invoke-static {v2}, Lco/uk/getmondo/spending/b/c;->d(Lco/uk/getmondo/spending/b/c;)Lio/reactivex/u;

    move-result-object v2

    invoke-virtual {v0, v2}, Lio/reactivex/v;->b(Lio/reactivex/u;)Lio/reactivex/v;

    move-result-object v0

    .line 73
    iget-object v2, p0, Lco/uk/getmondo/spending/b/c$f;->a:Lco/uk/getmondo/spending/b/c;

    invoke-static {v2}, Lco/uk/getmondo/spending/b/c;->e(Lco/uk/getmondo/spending/b/c;)Lio/reactivex/u;

    move-result-object v2

    invoke-virtual {v0, v2}, Lio/reactivex/v;->a(Lio/reactivex/u;)Lio/reactivex/v;

    move-result-object v2

    .line 74
    new-instance v0, Lco/uk/getmondo/spending/b/c$f$3;

    invoke-direct {v0, p0, v1}, Lco/uk/getmondo/spending/b/c$f$3;-><init>(Lco/uk/getmondo/spending/b/c$f;Lco/uk/getmondo/spending/b/a/a/b;)V

    check-cast v0, Lio/reactivex/c/g;

    invoke-virtual {v2, v0}, Lio/reactivex/v;->c(Lio/reactivex/c/g;)Lio/reactivex/v;

    move-result-object v0

    .line 75
    return-object v0
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 33
    check-cast p1, Lkotlin/h;

    invoke-virtual {p0, p1}, Lco/uk/getmondo/spending/b/c$f;->a(Lkotlin/h;)Lio/reactivex/v;

    move-result-object v0

    return-object v0
.end method
