.class public final Lco/uk/getmondo/spending/transactions/c;
.super Ljava/lang/Object;
.source "SpendingTransactionsFragment_MembersInjector.java"

# interfaces
.implements Lb/a;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lb/a",
        "<",
        "Lco/uk/getmondo/spending/transactions/SpendingTransactionsFragment;",
        ">;"
    }
.end annotation


# static fields
.field static final synthetic a:Z


# instance fields
.field private final b:Ljavax/a/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/spending/transactions/f;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Ljavax/a/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/spending/transactions/SpendingFeedAdapter;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 7
    const-class v0, Lco/uk/getmondo/spending/transactions/c;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lco/uk/getmondo/spending/transactions/c;->a:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Ljavax/a/a;Ljavax/a/a;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/spending/transactions/f;",
            ">;",
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/spending/transactions/SpendingFeedAdapter;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    sget-boolean v0, Lco/uk/getmondo/spending/transactions/c;->a:Z

    if-nez v0, :cond_0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 21
    :cond_0
    iput-object p1, p0, Lco/uk/getmondo/spending/transactions/c;->b:Ljavax/a/a;

    .line 22
    sget-boolean v0, Lco/uk/getmondo/spending/transactions/c;->a:Z

    if-nez v0, :cond_1

    if-nez p2, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 23
    :cond_1
    iput-object p2, p0, Lco/uk/getmondo/spending/transactions/c;->c:Ljavax/a/a;

    .line 24
    return-void
.end method

.method public static a(Ljavax/a/a;Ljavax/a/a;)Lb/a;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/spending/transactions/f;",
            ">;",
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/spending/transactions/SpendingFeedAdapter;",
            ">;)",
            "Lb/a",
            "<",
            "Lco/uk/getmondo/spending/transactions/SpendingTransactionsFragment;",
            ">;"
        }
    .end annotation

    .prologue
    .line 29
    new-instance v0, Lco/uk/getmondo/spending/transactions/c;

    invoke-direct {v0, p0, p1}, Lco/uk/getmondo/spending/transactions/c;-><init>(Ljavax/a/a;Ljavax/a/a;)V

    return-object v0
.end method


# virtual methods
.method public a(Lco/uk/getmondo/spending/transactions/SpendingTransactionsFragment;)V
    .locals 2

    .prologue
    .line 34
    if-nez p1, :cond_0

    .line 35
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Cannot inject members into a null reference"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 37
    :cond_0
    iget-object v0, p0, Lco/uk/getmondo/spending/transactions/c;->b:Ljavax/a/a;

    invoke-interface {v0}, Ljavax/a/a;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/spending/transactions/f;

    iput-object v0, p1, Lco/uk/getmondo/spending/transactions/SpendingTransactionsFragment;->a:Lco/uk/getmondo/spending/transactions/f;

    .line 38
    iget-object v0, p0, Lco/uk/getmondo/spending/transactions/c;->c:Ljavax/a/a;

    invoke-interface {v0}, Ljavax/a/a;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/spending/transactions/SpendingFeedAdapter;

    iput-object v0, p1, Lco/uk/getmondo/spending/transactions/SpendingTransactionsFragment;->c:Lco/uk/getmondo/spending/transactions/SpendingFeedAdapter;

    .line 39
    return-void
.end method

.method public bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 7
    check-cast p1, Lco/uk/getmondo/spending/transactions/SpendingTransactionsFragment;

    invoke-virtual {p0, p1}, Lco/uk/getmondo/spending/transactions/c;->a(Lco/uk/getmondo/spending/transactions/SpendingTransactionsFragment;)V

    return-void
.end method
