.class public Lco/uk/getmondo/spending/transactions/f;
.super Lco/uk/getmondo/common/ui/b;
.source "SpendingTransactionsPresenter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lco/uk/getmondo/spending/transactions/f$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lco/uk/getmondo/common/ui/b",
        "<",
        "Lco/uk/getmondo/spending/transactions/f$a;",
        ">;"
    }
.end annotation


# instance fields
.field private final c:Lio/reactivex/u;

.field private final d:Lco/uk/getmondo/spending/a/i;

.field private final e:Lco/uk/getmondo/common/a;

.field private final f:Lorg/threeten/bp/YearMonth;

.field private final g:Lco/uk/getmondo/d/h;

.field private final h:Ljava/lang/String;

.field private final i:Ljava/lang/String;


# direct methods
.method constructor <init>(Lio/reactivex/u;Lco/uk/getmondo/spending/a/i;Lco/uk/getmondo/common/a;Lorg/threeten/bp/YearMonth;Lco/uk/getmondo/d/h;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 38
    invoke-direct {p0}, Lco/uk/getmondo/common/ui/b;-><init>()V

    .line 39
    iput-object p1, p0, Lco/uk/getmondo/spending/transactions/f;->c:Lio/reactivex/u;

    .line 40
    iput-object p2, p0, Lco/uk/getmondo/spending/transactions/f;->d:Lco/uk/getmondo/spending/a/i;

    .line 41
    iput-object p3, p0, Lco/uk/getmondo/spending/transactions/f;->e:Lco/uk/getmondo/common/a;

    .line 42
    iput-object p4, p0, Lco/uk/getmondo/spending/transactions/f;->f:Lorg/threeten/bp/YearMonth;

    .line 43
    iput-object p5, p0, Lco/uk/getmondo/spending/transactions/f;->g:Lco/uk/getmondo/d/h;

    .line 44
    iput-object p6, p0, Lco/uk/getmondo/spending/transactions/f;->h:Ljava/lang/String;

    .line 45
    iput-object p7, p0, Lco/uk/getmondo/spending/transactions/f;->i:Ljava/lang/String;

    .line 46
    return-void
.end method

.method static synthetic a(Ljava/util/List;)Landroid/support/v4/g/j;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 60
    new-instance v1, Lco/uk/getmondo/common/i/a;

    sget-object v0, Lco/uk/getmondo/common/i/c;->a:Lco/uk/getmondo/common/i/c;

    invoke-direct {v1, v0}, Lco/uk/getmondo/common/i/a;-><init>(Lco/uk/getmondo/common/i/c;)V

    .line 61
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/d/m;

    .line 62
    invoke-virtual {v0}, Lco/uk/getmondo/d/m;->e()Lcom/c/b/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/c/b/b;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/d/aj;

    invoke-virtual {v0}, Lco/uk/getmondo/d/aj;->g()Lco/uk/getmondo/d/c;

    move-result-object v0

    invoke-virtual {v1, v0}, Lco/uk/getmondo/common/i/a;->a(Lco/uk/getmondo/d/c;)V

    goto :goto_0

    .line 64
    :cond_0
    invoke-virtual {v1}, Lco/uk/getmondo/common/i/a;->a()Lco/uk/getmondo/d/c;

    move-result-object v0

    invoke-static {p0, v0}, Landroid/support/v4/g/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Landroid/support/v4/g/j;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lco/uk/getmondo/spending/transactions/f;Lco/uk/getmondo/spending/transactions/f$a;Landroid/support/v4/g/j;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 66
    iget-object v0, p2, Landroid/support/v4/g/j;->a:Ljava/lang/Object;

    check-cast v0, Ljava/util/List;

    iget-object v1, p2, Landroid/support/v4/g/j;->b:Ljava/lang/Object;

    check-cast v1, Lco/uk/getmondo/d/c;

    iget-object v2, p0, Lco/uk/getmondo/spending/transactions/f;->f:Lorg/threeten/bp/YearMonth;

    invoke-interface {p1, v0, v1, v2}, Lco/uk/getmondo/spending/transactions/f$a;->a(Ljava/util/List;Lco/uk/getmondo/d/c;Lorg/threeten/bp/YearMonth;)V

    return-void
.end method


# virtual methods
.method public bridge synthetic a(Lco/uk/getmondo/common/ui/f;)V
    .locals 0

    .prologue
    .line 25
    check-cast p1, Lco/uk/getmondo/spending/transactions/f$a;

    invoke-virtual {p0, p1}, Lco/uk/getmondo/spending/transactions/f;->a(Lco/uk/getmondo/spending/transactions/f$a;)V

    return-void
.end method

.method public a(Lco/uk/getmondo/spending/transactions/f$a;)V
    .locals 5

    .prologue
    .line 50
    invoke-super {p0, p1}, Lco/uk/getmondo/common/ui/b;->a(Lco/uk/getmondo/common/ui/f;)V

    .line 52
    iget-object v1, p0, Lco/uk/getmondo/spending/transactions/f;->e:Lco/uk/getmondo/common/a;

    iget-object v0, p0, Lco/uk/getmondo/spending/transactions/f;->h:Ljava/lang/String;

    if-nez v0, :cond_0

    iget-object v0, p0, Lco/uk/getmondo/spending/transactions/f;->i:Ljava/lang/String;

    if-nez v0, :cond_0

    iget-object v0, p0, Lco/uk/getmondo/spending/transactions/f;->g:Lco/uk/getmondo/d/h;

    iget-object v2, p0, Lco/uk/getmondo/spending/transactions/f;->f:Lorg/threeten/bp/YearMonth;

    .line 53
    invoke-static {v0, v2}, Lco/uk/getmondo/api/model/tracking/Impression;->a(Lco/uk/getmondo/d/h;Lorg/threeten/bp/YearMonth;)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    .line 52
    :goto_0
    invoke-virtual {v1, v0}, Lco/uk/getmondo/common/a;->a(Lco/uk/getmondo/api/model/tracking/Impression;)V

    .line 56
    iget-object v0, p0, Lco/uk/getmondo/spending/transactions/f;->d:Lco/uk/getmondo/spending/a/i;

    iget-object v1, p0, Lco/uk/getmondo/spending/transactions/f;->f:Lorg/threeten/bp/YearMonth;

    iget-object v2, p0, Lco/uk/getmondo/spending/transactions/f;->g:Lco/uk/getmondo/d/h;

    iget-object v3, p0, Lco/uk/getmondo/spending/transactions/f;->h:Ljava/lang/String;

    iget-object v4, p0, Lco/uk/getmondo/spending/transactions/f;->i:Ljava/lang/String;

    invoke-virtual {v0, v1, v2, v3, v4}, Lco/uk/getmondo/spending/a/i;->a(Lorg/threeten/bp/YearMonth;Lco/uk/getmondo/d/h;Ljava/lang/String;Ljava/lang/String;)Lio/reactivex/n;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/spending/transactions/f;->c:Lio/reactivex/u;

    .line 57
    invoke-virtual {v0, v1}, Lio/reactivex/n;->observeOn(Lio/reactivex/u;)Lio/reactivex/n;

    move-result-object v0

    invoke-static {}, Lco/uk/getmondo/spending/transactions/g;->a()Lio/reactivex/c/h;

    move-result-object v1

    .line 58
    invoke-virtual {v0, v1}, Lio/reactivex/n;->map(Lio/reactivex/c/h;)Lio/reactivex/n;

    move-result-object v0

    invoke-static {p0, p1}, Lco/uk/getmondo/spending/transactions/h;->a(Lco/uk/getmondo/spending/transactions/f;Lco/uk/getmondo/spending/transactions/f$a;)Lio/reactivex/c/g;

    move-result-object v1

    invoke-static {}, Lco/uk/getmondo/spending/transactions/i;->a()Lio/reactivex/c/g;

    move-result-object v2

    .line 66
    invoke-virtual {v0, v1, v2}, Lio/reactivex/n;->subscribe(Lio/reactivex/c/g;Lio/reactivex/c/g;)Lio/reactivex/b/b;

    move-result-object v0

    .line 56
    invoke-virtual {p0, v0}, Lco/uk/getmondo/spending/transactions/f;->a(Lio/reactivex/b/b;)V

    .line 68
    return-void

    .line 53
    :cond_0
    iget-object v0, p0, Lco/uk/getmondo/spending/transactions/f;->g:Lco/uk/getmondo/d/h;

    iget-object v2, p0, Lco/uk/getmondo/spending/transactions/f;->f:Lorg/threeten/bp/YearMonth;

    .line 54
    invoke-static {v0, v2}, Lco/uk/getmondo/api/model/tracking/Impression;->c(Lco/uk/getmondo/d/h;Lorg/threeten/bp/YearMonth;)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    goto :goto_0
.end method
