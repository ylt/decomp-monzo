.class Lco/uk/getmondo/spending/transactions/SpendingFeedAdapter$SpendingHeaderViewHolder;
.super Landroid/support/v7/widget/RecyclerView$w;
.source "SpendingFeedAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lco/uk/getmondo/spending/transactions/SpendingFeedAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "SpendingHeaderViewHolder"
.end annotation


# instance fields
.field amountView:Lco/uk/getmondo/common/ui/AmountView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f11042f
    .end annotation
.end field

.field monthNameTextView:Landroid/widget/TextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f110430
    .end annotation
.end field


# direct methods
.method constructor <init>(Landroid/view/View;)V
    .locals 0

    .prologue
    .line 111
    invoke-direct {p0, p1}, Landroid/support/v7/widget/RecyclerView$w;-><init>(Landroid/view/View;)V

    .line 112
    invoke-static {p0, p1}, Lbutterknife/ButterKnife;->bind(Ljava/lang/Object;Landroid/view/View;)Lbutterknife/Unbinder;

    .line 113
    return-void
.end method


# virtual methods
.method a(Lorg/threeten/bp/YearMonth;Lco/uk/getmondo/d/c;)V
    .locals 4

    .prologue
    .line 116
    iget-object v0, p0, Lco/uk/getmondo/spending/transactions/SpendingFeedAdapter$SpendingHeaderViewHolder;->amountView:Lco/uk/getmondo/common/ui/AmountView;

    invoke-virtual {v0, p2}, Lco/uk/getmondo/common/ui/AmountView;->setAmount(Lco/uk/getmondo/d/c;)V

    .line 117
    iget-object v0, p0, Lco/uk/getmondo/spending/transactions/SpendingFeedAdapter$SpendingHeaderViewHolder;->monthNameTextView:Landroid/widget/TextView;

    invoke-virtual {p1}, Lorg/threeten/bp/YearMonth;->c()Lorg/threeten/bp/Month;

    move-result-object v1

    sget-object v2, Lorg/threeten/bp/format/TextStyle;->a:Lorg/threeten/bp/format/TextStyle;

    sget-object v3, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {v1, v2, v3}, Lorg/threeten/bp/Month;->a(Lorg/threeten/bp/format/TextStyle;Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 118
    return-void
.end method
