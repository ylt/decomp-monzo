.class public Lco/uk/getmondo/spending/transactions/SpendingTransactionsFragment;
.super Lco/uk/getmondo/common/f/a;
.source "SpendingTransactionsFragment.java"

# interfaces
.implements Lco/uk/getmondo/feed/adapter/a$a;
.implements Lco/uk/getmondo/spending/transactions/f$a;


# instance fields
.field a:Lco/uk/getmondo/spending/transactions/f;

.field c:Lco/uk/getmondo/spending/transactions/SpendingFeedAdapter;

.field private d:Lbutterknife/Unbinder;

.field recyclerView:Landroid/support/v7/widget/RecyclerView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f110341
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0}, Lco/uk/getmondo/common/f/a;-><init>()V

    return-void
.end method

.method public static a(Lorg/threeten/bp/YearMonth;Lco/uk/getmondo/d/h;)Lco/uk/getmondo/spending/transactions/SpendingTransactionsFragment;
    .locals 2

    .prologue
    .line 47
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 48
    const-string v1, "year_month"

    invoke-virtual {v0, v1, p0}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 49
    const-string v1, "category"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 51
    new-instance v1, Lco/uk/getmondo/spending/transactions/SpendingTransactionsFragment;

    invoke-direct {v1}, Lco/uk/getmondo/spending/transactions/SpendingTransactionsFragment;-><init>()V

    .line 52
    invoke-virtual {v1, v0}, Lco/uk/getmondo/spending/transactions/SpendingTransactionsFragment;->setArguments(Landroid/os/Bundle;)V

    .line 53
    return-object v1
.end method

.method public static a(Lorg/threeten/bp/YearMonth;Lco/uk/getmondo/d/h;Ljava/lang/String;)Lco/uk/getmondo/spending/transactions/SpendingTransactionsFragment;
    .locals 2

    .prologue
    .line 57
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 58
    const-string v1, "year_month"

    invoke-virtual {v0, v1, p0}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 59
    const-string v1, "merchant_group_id"

    invoke-virtual {v0, v1, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 60
    const-string v1, "category"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 62
    new-instance v1, Lco/uk/getmondo/spending/transactions/SpendingTransactionsFragment;

    invoke-direct {v1}, Lco/uk/getmondo/spending/transactions/SpendingTransactionsFragment;-><init>()V

    .line 63
    invoke-virtual {v1, v0}, Lco/uk/getmondo/spending/transactions/SpendingTransactionsFragment;->setArguments(Landroid/os/Bundle;)V

    .line 64
    return-object v1
.end method

.method public static b(Lorg/threeten/bp/YearMonth;Lco/uk/getmondo/d/h;Ljava/lang/String;)Lco/uk/getmondo/spending/transactions/SpendingTransactionsFragment;
    .locals 2

    .prologue
    .line 68
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 69
    const-string v1, "year_month"

    invoke-virtual {v0, v1, p0}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 70
    const-string v1, "peer_id"

    invoke-virtual {v0, v1, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 71
    const-string v1, "category"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 73
    new-instance v1, Lco/uk/getmondo/spending/transactions/SpendingTransactionsFragment;

    invoke-direct {v1}, Lco/uk/getmondo/spending/transactions/SpendingTransactionsFragment;-><init>()V

    .line 74
    invoke-virtual {v1, v0}, Lco/uk/getmondo/spending/transactions/SpendingTransactionsFragment;->setArguments(Landroid/os/Bundle;)V

    .line 75
    return-object v1
.end method


# virtual methods
.method public a(Lco/uk/getmondo/d/m;)V
    .locals 3

    .prologue
    .line 128
    invoke-virtual {p1}, Lco/uk/getmondo/d/m;->j()Lco/uk/getmondo/feed/a/a/a;

    move-result-object v0

    sget-object v1, Lco/uk/getmondo/feed/a/a/a;->a:Lco/uk/getmondo/feed/a/a/a;

    if-ne v0, v1, :cond_0

    .line 129
    invoke-virtual {p0}, Lco/uk/getmondo/spending/transactions/SpendingTransactionsFragment;->getActivity()Landroid/support/v4/app/j;

    move-result-object v1

    invoke-virtual {p0}, Lco/uk/getmondo/spending/transactions/SpendingTransactionsFragment;->getActivity()Landroid/support/v4/app/j;

    move-result-object v2

    invoke-virtual {p1}, Lco/uk/getmondo/d/m;->e()Lcom/c/b/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/c/b/b;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/d/aj;

    invoke-virtual {v0}, Lco/uk/getmondo/d/aj;->w()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lco/uk/getmondo/transaction/details/TransactionDetailsActivity;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/support/v4/app/j;->startActivity(Landroid/content/Intent;)V

    .line 131
    :cond_0
    return-void
.end method

.method public a(Ljava/util/List;Lco/uk/getmondo/d/c;Lorg/threeten/bp/YearMonth;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lco/uk/getmondo/d/m;",
            ">;",
            "Lco/uk/getmondo/d/c;",
            "Lorg/threeten/bp/YearMonth;",
            ")V"
        }
    .end annotation

    .prologue
    .line 123
    iget-object v0, p0, Lco/uk/getmondo/spending/transactions/SpendingTransactionsFragment;->c:Lco/uk/getmondo/spending/transactions/SpendingFeedAdapter;

    invoke-virtual {v0, p1, p2, p3}, Lco/uk/getmondo/spending/transactions/SpendingFeedAdapter;->a(Ljava/util/List;Lco/uk/getmondo/d/c;Lorg/threeten/bp/YearMonth;)V

    .line 124
    return-void
.end method

.method public b(Lco/uk/getmondo/d/m;)V
    .locals 0

    .prologue
    .line 136
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 6

    .prologue
    .line 80
    invoke-super {p0, p1}, Lco/uk/getmondo/common/f/a;->onCreate(Landroid/os/Bundle;)V

    .line 82
    invoke-virtual {p0}, Lco/uk/getmondo/spending/transactions/SpendingTransactionsFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "year_month"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/threeten/bp/YearMonth;

    .line 83
    invoke-virtual {p0}, Lco/uk/getmondo/spending/transactions/SpendingTransactionsFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "category"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lco/uk/getmondo/d/h;

    .line 84
    invoke-virtual {p0}, Lco/uk/getmondo/spending/transactions/SpendingTransactionsFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "merchant_group_id"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 85
    invoke-virtual {p0}, Lco/uk/getmondo/spending/transactions/SpendingTransactionsFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v3

    const-string v4, "peer_id"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 87
    invoke-virtual {p0}, Lco/uk/getmondo/spending/transactions/SpendingTransactionsFragment;->B()Lco/uk/getmondo/common/h/b/b;

    move-result-object v4

    new-instance v5, Lco/uk/getmondo/spending/transactions/d;

    invoke-direct {v5, v0, v1, v2, v3}, Lco/uk/getmondo/spending/transactions/d;-><init>(Lorg/threeten/bp/YearMonth;Lco/uk/getmondo/d/h;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v5}, Lco/uk/getmondo/common/h/b/b;->a(Lco/uk/getmondo/spending/transactions/d;)Lco/uk/getmondo/spending/transactions/b;

    move-result-object v0

    invoke-interface {v0, p0}, Lco/uk/getmondo/spending/transactions/b;->a(Lco/uk/getmondo/spending/transactions/SpendingTransactionsFragment;)V

    .line 88
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2

    .prologue
    .line 92
    const v0, 0x7f0500ab

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public onDestroyView()V
    .locals 1

    .prologue
    .line 116
    iget-object v0, p0, Lco/uk/getmondo/spending/transactions/SpendingTransactionsFragment;->a:Lco/uk/getmondo/spending/transactions/f;

    invoke-virtual {v0}, Lco/uk/getmondo/spending/transactions/f;->b()V

    .line 117
    iget-object v0, p0, Lco/uk/getmondo/spending/transactions/SpendingTransactionsFragment;->d:Lbutterknife/Unbinder;

    invoke-interface {v0}, Lbutterknife/Unbinder;->unbind()V

    .line 118
    invoke-super {p0}, Lco/uk/getmondo/common/f/a;->onDestroyView()V

    .line 119
    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 5

    .prologue
    .line 97
    invoke-super {p0, p1, p2}, Lco/uk/getmondo/common/f/a;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 99
    invoke-static {p0, p1}, Lbutterknife/ButterKnife;->bind(Ljava/lang/Object;Landroid/view/View;)Lbutterknife/Unbinder;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/spending/transactions/SpendingTransactionsFragment;->d:Lbutterknife/Unbinder;

    .line 101
    new-instance v0, Landroid/support/v7/widget/LinearLayoutManager;

    invoke-virtual {p0}, Lco/uk/getmondo/spending/transactions/SpendingTransactionsFragment;->getActivity()Landroid/support/v4/app/j;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/support/v7/widget/LinearLayoutManager;-><init>(Landroid/content/Context;)V

    .line 102
    iget-object v1, p0, Lco/uk/getmondo/spending/transactions/SpendingTransactionsFragment;->c:Lco/uk/getmondo/spending/transactions/SpendingFeedAdapter;

    invoke-virtual {v1, p0}, Lco/uk/getmondo/spending/transactions/SpendingFeedAdapter;->a(Lco/uk/getmondo/feed/adapter/a$a;)V

    .line 103
    iget-object v1, p0, Lco/uk/getmondo/spending/transactions/SpendingTransactionsFragment;->recyclerView:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v1, v0}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(Landroid/support/v7/widget/RecyclerView$h;)V

    .line 104
    iget-object v0, p0, Lco/uk/getmondo/spending/transactions/SpendingTransactionsFragment;->recyclerView:Landroid/support/v7/widget/RecyclerView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setHasFixedSize(Z)V

    .line 105
    iget-object v0, p0, Lco/uk/getmondo/spending/transactions/SpendingTransactionsFragment;->recyclerView:Landroid/support/v7/widget/RecyclerView;

    iget-object v1, p0, Lco/uk/getmondo/spending/transactions/SpendingTransactionsFragment;->c:Lco/uk/getmondo/spending/transactions/SpendingFeedAdapter;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setAdapter(Landroid/support/v7/widget/RecyclerView$a;)V

    .line 107
    invoke-virtual {p0}, Lco/uk/getmondo/spending/transactions/SpendingTransactionsFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0119

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 108
    iget-object v1, p0, Lco/uk/getmondo/spending/transactions/SpendingTransactionsFragment;->recyclerView:Landroid/support/v7/widget/RecyclerView;

    new-instance v2, Lco/uk/getmondo/common/ui/h;

    invoke-virtual {p0}, Lco/uk/getmondo/spending/transactions/SpendingTransactionsFragment;->getActivity()Landroid/support/v4/app/j;

    move-result-object v3

    iget-object v4, p0, Lco/uk/getmondo/spending/transactions/SpendingTransactionsFragment;->c:Lco/uk/getmondo/spending/transactions/SpendingFeedAdapter;

    invoke-direct {v2, v3, v4, v0}, Lco/uk/getmondo/common/ui/h;-><init>(Landroid/content/Context;La/a/a/a/a/a;I)V

    invoke-virtual {v1, v2}, Landroid/support/v7/widget/RecyclerView;->a(Landroid/support/v7/widget/RecyclerView$g;)V

    .line 109
    iget-object v0, p0, Lco/uk/getmondo/spending/transactions/SpendingTransactionsFragment;->recyclerView:Landroid/support/v7/widget/RecyclerView;

    new-instance v1, La/a/a/a/a/b;

    iget-object v2, p0, Lco/uk/getmondo/spending/transactions/SpendingTransactionsFragment;->c:Lco/uk/getmondo/spending/transactions/SpendingFeedAdapter;

    invoke-direct {v1, v2}, La/a/a/a/a/b;-><init>(La/a/a/a/a/a;)V

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->a(Landroid/support/v7/widget/RecyclerView$g;)V

    .line 111
    iget-object v0, p0, Lco/uk/getmondo/spending/transactions/SpendingTransactionsFragment;->a:Lco/uk/getmondo/spending/transactions/f;

    invoke-virtual {v0, p0}, Lco/uk/getmondo/spending/transactions/f;->a(Lco/uk/getmondo/spending/transactions/f$a;)V

    .line 112
    return-void
.end method
