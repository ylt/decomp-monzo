.class public final Lco/uk/getmondo/spending/transactions/e;
.super Ljava/lang/Object;
.source "SpendingTransactionsModule_ProvidePresenterFactory.java"

# interfaces
.implements Lb/a/b;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lb/a/b",
        "<",
        "Lco/uk/getmondo/spending/transactions/f;",
        ">;"
    }
.end annotation


# static fields
.field static final synthetic a:Z


# instance fields
.field private final b:Lco/uk/getmondo/spending/transactions/d;

.field private final c:Ljavax/a/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/a/a",
            "<",
            "Lio/reactivex/u;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Ljavax/a/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/spending/a/i;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Ljavax/a/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/common/a;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 11
    const-class v0, Lco/uk/getmondo/spending/transactions/e;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lco/uk/getmondo/spending/transactions/e;->a:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lco/uk/getmondo/spending/transactions/d;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lco/uk/getmondo/spending/transactions/d;",
            "Ljavax/a/a",
            "<",
            "Lio/reactivex/u;",
            ">;",
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/spending/a/i;",
            ">;",
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/common/a;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    sget-boolean v0, Lco/uk/getmondo/spending/transactions/e;->a:Z

    if-nez v0, :cond_0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 31
    :cond_0
    iput-object p1, p0, Lco/uk/getmondo/spending/transactions/e;->b:Lco/uk/getmondo/spending/transactions/d;

    .line 32
    sget-boolean v0, Lco/uk/getmondo/spending/transactions/e;->a:Z

    if-nez v0, :cond_1

    if-nez p2, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 33
    :cond_1
    iput-object p2, p0, Lco/uk/getmondo/spending/transactions/e;->c:Ljavax/a/a;

    .line 34
    sget-boolean v0, Lco/uk/getmondo/spending/transactions/e;->a:Z

    if-nez v0, :cond_2

    if-nez p3, :cond_2

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 35
    :cond_2
    iput-object p3, p0, Lco/uk/getmondo/spending/transactions/e;->d:Ljavax/a/a;

    .line 36
    sget-boolean v0, Lco/uk/getmondo/spending/transactions/e;->a:Z

    if-nez v0, :cond_3

    if-nez p4, :cond_3

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 37
    :cond_3
    iput-object p4, p0, Lco/uk/getmondo/spending/transactions/e;->e:Ljavax/a/a;

    .line 38
    return-void
.end method

.method public static a(Lco/uk/getmondo/spending/transactions/d;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;)Lb/a/b;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lco/uk/getmondo/spending/transactions/d;",
            "Ljavax/a/a",
            "<",
            "Lio/reactivex/u;",
            ">;",
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/spending/a/i;",
            ">;",
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/common/a;",
            ">;)",
            "Lb/a/b",
            "<",
            "Lco/uk/getmondo/spending/transactions/f;",
            ">;"
        }
    .end annotation

    .prologue
    .line 55
    new-instance v0, Lco/uk/getmondo/spending/transactions/e;

    invoke-direct {v0, p0, p1, p2, p3}, Lco/uk/getmondo/spending/transactions/e;-><init>(Lco/uk/getmondo/spending/transactions/d;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;)V

    return-object v0
.end method


# virtual methods
.method public a()Lco/uk/getmondo/spending/transactions/f;
    .locals 4

    .prologue
    .line 42
    iget-object v3, p0, Lco/uk/getmondo/spending/transactions/e;->b:Lco/uk/getmondo/spending/transactions/d;

    iget-object v0, p0, Lco/uk/getmondo/spending/transactions/e;->c:Ljavax/a/a;

    .line 44
    invoke-interface {v0}, Ljavax/a/a;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/reactivex/u;

    iget-object v1, p0, Lco/uk/getmondo/spending/transactions/e;->d:Ljavax/a/a;

    .line 45
    invoke-interface {v1}, Ljavax/a/a;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lco/uk/getmondo/spending/a/i;

    iget-object v2, p0, Lco/uk/getmondo/spending/transactions/e;->e:Ljavax/a/a;

    .line 46
    invoke-interface {v2}, Ljavax/a/a;->b()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lco/uk/getmondo/common/a;

    .line 43
    invoke-virtual {v3, v0, v1, v2}, Lco/uk/getmondo/spending/transactions/d;->a(Lio/reactivex/u;Lco/uk/getmondo/spending/a/i;Lco/uk/getmondo/common/a;)Lco/uk/getmondo/spending/transactions/f;

    move-result-object v0

    const-string v1, "Cannot return null from a non-@Nullable @Provides method"

    .line 42
    invoke-static {v0, v1}, Lb/a/d;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/spending/transactions/f;

    return-object v0
.end method

.method public synthetic b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 11
    invoke-virtual {p0}, Lco/uk/getmondo/spending/transactions/e;->a()Lco/uk/getmondo/spending/transactions/f;

    move-result-object v0

    return-object v0
.end method
