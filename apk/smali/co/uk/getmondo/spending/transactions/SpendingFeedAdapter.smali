.class Lco/uk/getmondo/spending/transactions/SpendingFeedAdapter;
.super Lco/uk/getmondo/feed/adapter/a;
.source "SpendingFeedAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lco/uk/getmondo/spending/transactions/SpendingFeedAdapter$SpendingHeaderViewHolder;
    }
.end annotation


# static fields
.field private static final b:I


# instance fields
.field private c:Landroid/support/v4/g/j;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/support/v4/g/j",
            "<",
            "Lorg/threeten/bp/YearMonth;",
            "Lco/uk/getmondo/d/c;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 35
    invoke-static {}, Lco/uk/getmondo/feed/a/a/a;->values()[Lco/uk/getmondo/feed/a/a/a;

    move-result-object v0

    array-length v0, v0

    sput v0, Lco/uk/getmondo/spending/transactions/SpendingFeedAdapter;->b:I

    return-void
.end method

.method constructor <init>(Lco/uk/getmondo/feed/a/a;)V
    .locals 0

    .prologue
    .line 41
    invoke-direct {p0, p1}, Lco/uk/getmondo/feed/adapter/a;-><init>(Lco/uk/getmondo/feed/a/a;)V

    .line 42
    return-void
.end method

.method private b(I)I
    .locals 1

    .prologue
    .line 100
    iget-object v0, p0, Lco/uk/getmondo/spending/transactions/SpendingFeedAdapter;->c:Landroid/support/v4/g/j;

    if-nez v0, :cond_0

    .line 103
    :goto_0
    return p1

    :cond_0
    add-int/lit8 p1, p1, -0x1

    goto :goto_0
.end method


# virtual methods
.method public a(I)J
    .locals 2

    .prologue
    .line 65
    iget-object v0, p0, Lco/uk/getmondo/spending/transactions/SpendingFeedAdapter;->c:Landroid/support/v4/g/j;

    if-eqz v0, :cond_0

    if-nez p1, :cond_0

    .line 66
    const-wide/16 v0, -0x1

    .line 68
    :goto_0
    return-wide v0

    :cond_0
    invoke-direct {p0, p1}, Lco/uk/getmondo/spending/transactions/SpendingFeedAdapter;->b(I)I

    move-result v0

    invoke-super {p0, v0}, Lco/uk/getmondo/feed/adapter/a;->a(I)J

    move-result-wide v0

    goto :goto_0
.end method

.method public a(Landroid/support/v7/widget/RecyclerView$w;I)V
    .locals 1

    .prologue
    .line 91
    invoke-direct {p0, p2}, Lco/uk/getmondo/spending/transactions/SpendingFeedAdapter;->b(I)I

    move-result v0

    invoke-super {p0, p1, v0}, Lco/uk/getmondo/feed/adapter/a;->a(Landroid/support/v7/widget/RecyclerView$w;I)V

    .line 92
    return-void
.end method

.method a(Ljava/util/List;Lco/uk/getmondo/d/c;Lorg/threeten/bp/YearMonth;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lco/uk/getmondo/d/m;",
            ">;",
            "Lco/uk/getmondo/d/c;",
            "Lorg/threeten/bp/YearMonth;",
            ")V"
        }
    .end annotation

    .prologue
    .line 45
    invoke-static {p3, p2}, Landroid/support/v4/g/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Landroid/support/v4/g/j;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/spending/transactions/SpendingFeedAdapter;->c:Landroid/support/v4/g/j;

    .line 46
    iput-object p1, p0, Lco/uk/getmondo/spending/transactions/SpendingFeedAdapter;->a:Ljava/util/List;

    .line 47
    invoke-virtual {p0}, Lco/uk/getmondo/spending/transactions/SpendingFeedAdapter;->notifyDataSetChanged()V

    .line 48
    return-void
.end method

.method public getItemCount()I
    .locals 2

    .prologue
    .line 96
    invoke-super {p0}, Lco/uk/getmondo/feed/adapter/a;->getItemCount()I

    move-result v1

    iget-object v0, p0, Lco/uk/getmondo/spending/transactions/SpendingFeedAdapter;->c:Landroid/support/v4/g/j;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    add-int/2addr v0, v1

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getItemId(I)J
    .locals 2

    .prologue
    .line 52
    invoke-direct {p0, p1}, Lco/uk/getmondo/spending/transactions/SpendingFeedAdapter;->b(I)I

    move-result v0

    invoke-super {p0, v0}, Lco/uk/getmondo/feed/adapter/a;->getItemId(I)J

    move-result-wide v0

    return-wide v0
.end method

.method public getItemViewType(I)I
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lco/uk/getmondo/spending/transactions/SpendingFeedAdapter;->c:Landroid/support/v4/g/j;

    if-eqz v0, :cond_0

    if-nez p1, :cond_0

    .line 58
    sget v0, Lco/uk/getmondo/spending/transactions/SpendingFeedAdapter;->b:I

    .line 60
    :goto_0
    return v0

    :cond_0
    invoke-direct {p0, p1}, Lco/uk/getmondo/spending/transactions/SpendingFeedAdapter;->b(I)I

    move-result v0

    invoke-super {p0, v0}, Lco/uk/getmondo/feed/adapter/a;->getItemViewType(I)I

    move-result v0

    goto :goto_0
.end method

.method public onBindViewHolder(Landroid/support/v7/widget/RecyclerView$w;I)V
    .locals 2

    .prologue
    .line 82
    instance-of v0, p1, Lco/uk/getmondo/spending/transactions/SpendingFeedAdapter$SpendingHeaderViewHolder;

    if-eqz v0, :cond_0

    .line 83
    check-cast p1, Lco/uk/getmondo/spending/transactions/SpendingFeedAdapter$SpendingHeaderViewHolder;

    iget-object v0, p0, Lco/uk/getmondo/spending/transactions/SpendingFeedAdapter;->c:Landroid/support/v4/g/j;

    iget-object v0, v0, Landroid/support/v4/g/j;->a:Ljava/lang/Object;

    check-cast v0, Lorg/threeten/bp/YearMonth;

    iget-object v1, p0, Lco/uk/getmondo/spending/transactions/SpendingFeedAdapter;->c:Landroid/support/v4/g/j;

    iget-object v1, v1, Landroid/support/v4/g/j;->b:Ljava/lang/Object;

    check-cast v1, Lco/uk/getmondo/d/c;

    invoke-virtual {p1, v0, v1}, Lco/uk/getmondo/spending/transactions/SpendingFeedAdapter$SpendingHeaderViewHolder;->a(Lorg/threeten/bp/YearMonth;Lco/uk/getmondo/d/c;)V

    .line 87
    :goto_0
    return-void

    .line 85
    :cond_0
    invoke-direct {p0, p2}, Lco/uk/getmondo/spending/transactions/SpendingFeedAdapter;->b(I)I

    move-result v0

    invoke-super {p0, p1, v0}, Lco/uk/getmondo/feed/adapter/a;->onBindViewHolder(Landroid/support/v7/widget/RecyclerView$w;I)V

    goto :goto_0
.end method

.method public onCreateViewHolder(Landroid/view/ViewGroup;I)Landroid/support/v7/widget/RecyclerView$w;
    .locals 3

    .prologue
    .line 73
    sget v0, Lco/uk/getmondo/spending/transactions/SpendingFeedAdapter;->b:I

    if-ne p2, v0, :cond_0

    .line 74
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f050109

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 75
    new-instance v0, Lco/uk/getmondo/spending/transactions/SpendingFeedAdapter$SpendingHeaderViewHolder;

    invoke-direct {v0, v1}, Lco/uk/getmondo/spending/transactions/SpendingFeedAdapter$SpendingHeaderViewHolder;-><init>(Landroid/view/View;)V

    .line 77
    :goto_0
    return-object v0

    :cond_0
    invoke-super {p0, p1, p2}, Lco/uk/getmondo/feed/adapter/a;->onCreateViewHolder(Landroid/view/ViewGroup;I)Landroid/support/v7/widget/RecyclerView$w;

    move-result-object v0

    goto :goto_0
.end method
