.class public Lco/uk/getmondo/spending/transactions/d;
.super Ljava/lang/Object;
.source "SpendingTransactionsModule.java"


# instance fields
.field private final a:Lorg/threeten/bp/YearMonth;

.field private final b:Lco/uk/getmondo/d/h;

.field private final c:Ljava/lang/String;

.field private final d:Ljava/lang/String;


# direct methods
.method constructor <init>(Lorg/threeten/bp/YearMonth;Lco/uk/getmondo/d/h;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    iput-object p1, p0, Lco/uk/getmondo/spending/transactions/d;->a:Lorg/threeten/bp/YearMonth;

    .line 23
    iput-object p2, p0, Lco/uk/getmondo/spending/transactions/d;->b:Lco/uk/getmondo/d/h;

    .line 24
    iput-object p3, p0, Lco/uk/getmondo/spending/transactions/d;->c:Ljava/lang/String;

    .line 25
    iput-object p4, p0, Lco/uk/getmondo/spending/transactions/d;->d:Ljava/lang/String;

    .line 26
    return-void
.end method


# virtual methods
.method a(Lio/reactivex/u;Lco/uk/getmondo/spending/a/i;Lco/uk/getmondo/common/a;)Lco/uk/getmondo/spending/transactions/f;
    .locals 8

    .prologue
    .line 31
    new-instance v0, Lco/uk/getmondo/spending/transactions/f;

    iget-object v4, p0, Lco/uk/getmondo/spending/transactions/d;->a:Lorg/threeten/bp/YearMonth;

    iget-object v5, p0, Lco/uk/getmondo/spending/transactions/d;->b:Lco/uk/getmondo/d/h;

    iget-object v6, p0, Lco/uk/getmondo/spending/transactions/d;->c:Ljava/lang/String;

    iget-object v7, p0, Lco/uk/getmondo/spending/transactions/d;->d:Ljava/lang/String;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    invoke-direct/range {v0 .. v7}, Lco/uk/getmondo/spending/transactions/f;-><init>(Lio/reactivex/u;Lco/uk/getmondo/spending/a/i;Lco/uk/getmondo/common/a;Lorg/threeten/bp/YearMonth;Lco/uk/getmondo/d/h;Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method
