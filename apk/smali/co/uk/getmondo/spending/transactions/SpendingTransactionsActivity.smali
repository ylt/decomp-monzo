.class public Lco/uk/getmondo/spending/transactions/SpendingTransactionsActivity;
.super Lco/uk/getmondo/common/activities/b;
.source "SpendingTransactionsActivity.java"


# instance fields
.field toolbar:Landroid/support/v7/widget/Toolbar;
    .annotation build Lbutterknife/BindView;
        value = 0x7f1100fe
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Lco/uk/getmondo/common/activities/b;-><init>()V

    return-void
.end method

.method public static a(Landroid/content/Context;Lorg/threeten/bp/YearMonth;Lco/uk/getmondo/d/h;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 33
    invoke-static {p0, p1, p2, p3, p4}, Lco/uk/getmondo/spending/transactions/SpendingTransactionsActivity;->b(Landroid/content/Context;Lorg/threeten/bp/YearMonth;Lco/uk/getmondo/d/h;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 34
    return-void
.end method

.method public static b(Landroid/content/Context;Lorg/threeten/bp/YearMonth;Lco/uk/getmondo/d/h;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 37
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lco/uk/getmondo/spending/transactions/SpendingTransactionsActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 38
    const-string v1, "EXTRA_YEAR_MONTH"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 39
    const-string v1, "EXTRA_CATEGORY"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 40
    const-string v1, "EXTRA_TITLE"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 41
    const-string v1, "EXTRA_MERCHANT_GROUP_ID"

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 42
    return-object v0
.end method

.method public static c(Landroid/content/Context;Lorg/threeten/bp/YearMonth;Lco/uk/getmondo/d/h;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 46
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lco/uk/getmondo/spending/transactions/SpendingTransactionsActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 47
    const-string v1, "EXTRA_YEAR_MONTH"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 48
    const-string v1, "EXTRA_CATEGORY"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 49
    const-string v1, "EXTRA_TITLE"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 50
    const-string v1, "EXTRA_PEER_ID"

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 51
    invoke-virtual {p0, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 52
    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    .line 56
    invoke-super {p0, p1}, Lco/uk/getmondo/common/activities/b;->onCreate(Landroid/os/Bundle;)V

    .line 57
    const v0, 0x7f050064

    invoke-virtual {p0, v0}, Lco/uk/getmondo/spending/transactions/SpendingTransactionsActivity;->setContentView(I)V

    .line 58
    invoke-static {p0}, Lbutterknife/ButterKnife;->bind(Landroid/app/Activity;)Lbutterknife/Unbinder;

    .line 59
    iget-object v0, p0, Lco/uk/getmondo/spending/transactions/SpendingTransactionsActivity;->toolbar:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {p0, v0}, Lco/uk/getmondo/spending/transactions/SpendingTransactionsActivity;->setSupportActionBar(Landroid/support/v7/widget/Toolbar;)V

    .line 61
    invoke-virtual {p0}, Lco/uk/getmondo/spending/transactions/SpendingTransactionsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "EXTRA_CATEGORY"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/d/h;

    .line 62
    invoke-virtual {p0}, Lco/uk/getmondo/spending/transactions/SpendingTransactionsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "EXTRA_TITLE"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 64
    iget-object v2, p0, Lco/uk/getmondo/spending/transactions/SpendingTransactionsActivity;->toolbar:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {v2, v1}, Landroid/support/v7/widget/Toolbar;->setTitle(Ljava/lang/CharSequence;)V

    .line 65
    invoke-virtual {v0}, Lco/uk/getmondo/d/h;->b()I

    move-result v1

    invoke-static {p0, v1}, Landroid/support/v4/content/a;->c(Landroid/content/Context;I)I

    move-result v1

    .line 66
    iget-object v2, p0, Lco/uk/getmondo/spending/transactions/SpendingTransactionsActivity;->toolbar:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {v2, v1}, Landroid/support/v7/widget/Toolbar;->setBackgroundColor(I)V

    .line 67
    invoke-virtual {p0}, Lco/uk/getmondo/spending/transactions/SpendingTransactionsActivity;->getWindow()Landroid/view/Window;

    move-result-object v2

    invoke-static {v1}, Lco/uk/getmondo/common/k/c;->a(I)I

    move-result v1

    invoke-virtual {v2, v1}, Landroid/view/Window;->setStatusBarColor(I)V

    .line 69
    if-nez p1, :cond_0

    .line 70
    invoke-virtual {p0}, Lco/uk/getmondo/spending/transactions/SpendingTransactionsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "EXTRA_MERCHANT_GROUP_ID"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 71
    invoke-virtual {p0}, Lco/uk/getmondo/spending/transactions/SpendingTransactionsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v3, "EXTRA_YEAR_MONTH"

    invoke-virtual {v1, v3}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v1

    check-cast v1, Lorg/threeten/bp/YearMonth;

    .line 72
    invoke-virtual {p0}, Lco/uk/getmondo/spending/transactions/SpendingTransactionsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    const-string v4, "EXTRA_PEER_ID"

    invoke-virtual {v3, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 74
    if-eqz v2, :cond_1

    .line 75
    invoke-static {v1, v0, v2}, Lco/uk/getmondo/spending/transactions/SpendingTransactionsFragment;->a(Lorg/threeten/bp/YearMonth;Lco/uk/getmondo/d/h;Ljava/lang/String;)Lco/uk/getmondo/spending/transactions/SpendingTransactionsFragment;

    move-result-object v0

    .line 80
    :goto_0
    invoke-virtual {p0}, Lco/uk/getmondo/spending/transactions/SpendingTransactionsActivity;->getSupportFragmentManager()Landroid/support/v4/app/n;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/n;->a()Landroid/support/v4/app/t;

    move-result-object v1

    const v2, 0x7f11022a

    .line 81
    invoke-virtual {v1, v2, v0}, Landroid/support/v4/app/t;->a(ILandroid/support/v4/app/Fragment;)Landroid/support/v4/app/t;

    move-result-object v0

    .line 82
    invoke-virtual {v0}, Landroid/support/v4/app/t;->c()I

    .line 84
    :cond_0
    return-void

    .line 77
    :cond_1
    invoke-static {v1, v0, v3}, Lco/uk/getmondo/spending/transactions/SpendingTransactionsFragment;->b(Lorg/threeten/bp/YearMonth;Lco/uk/getmondo/d/h;Ljava/lang/String;)Lco/uk/getmondo/spending/transactions/SpendingTransactionsFragment;

    move-result-object v0

    goto :goto_0
.end method
