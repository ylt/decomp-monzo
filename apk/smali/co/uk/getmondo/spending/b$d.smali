.class final Lco/uk/getmondo/spending/b$d;
.super Lkotlin/d/b/m;
.source "SpendingFragment.kt"

# interfaces
.implements Lkotlin/d/a/m;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lco/uk/getmondo/spending/b;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/d/b/m;",
        "Lkotlin/d/a/m",
        "<",
        "Lorg/threeten/bp/YearMonth;",
        "Lco/uk/getmondo/spending/a/h;",
        "Lkotlin/n;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000\u0014\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "",
        "yearMonth",
        "Lorg/threeten/bp/YearMonth;",
        "spendingGroup",
        "Lco/uk/getmondo/spending/data/SpendingGroup;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field final synthetic a:Lco/uk/getmondo/spending/b;


# direct methods
.method constructor <init>(Lco/uk/getmondo/spending/b;)V
    .locals 1

    iput-object p1, p0, Lco/uk/getmondo/spending/b$d;->a:Lco/uk/getmondo/spending/b;

    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lkotlin/d/b/m;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 25
    check-cast p1, Lorg/threeten/bp/YearMonth;

    check-cast p2, Lco/uk/getmondo/spending/a/h;

    invoke-virtual {p0, p1, p2}, Lco/uk/getmondo/spending/b$d;->a(Lorg/threeten/bp/YearMonth;Lco/uk/getmondo/spending/a/h;)V

    sget-object v0, Lkotlin/n;->a:Lkotlin/n;

    return-object v0
.end method

.method public final a(Lorg/threeten/bp/YearMonth;Lco/uk/getmondo/spending/a/h;)V
    .locals 2

    .prologue
    const-string v0, "yearMonth"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "spendingGroup"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 56
    instance-of v0, p2, Lco/uk/getmondo/spending/a/h$a;

    if-eqz v0, :cond_0

    .line 57
    check-cast p2, Lco/uk/getmondo/spending/a/h$a;

    invoke-virtual {p2}, Lco/uk/getmondo/spending/a/h$a;->b()Lco/uk/getmondo/d/h;

    move-result-object v1

    .line 58
    iget-object v0, p0, Lco/uk/getmondo/spending/b$d;->a:Lco/uk/getmondo/spending/b;

    invoke-virtual {v0}, Lco/uk/getmondo/spending/b;->getActivity()Landroid/support/v4/app/j;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-static {v0, p1, v1}, Lco/uk/getmondo/spending/SpendingCategoryDetailsActivity;->a(Landroid/content/Context;Lorg/threeten/bp/YearMonth;Lco/uk/getmondo/d/h;)V

    .line 60
    :cond_0
    return-void
.end method
