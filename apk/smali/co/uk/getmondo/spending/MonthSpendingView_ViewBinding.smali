.class public Lco/uk/getmondo/spending/MonthSpendingView_ViewBinding;
.super Ljava/lang/Object;
.source "MonthSpendingView_ViewBinding.java"

# interfaces
.implements Lbutterknife/Unbinder;


# instance fields
.field private a:Lco/uk/getmondo/spending/MonthSpendingView;


# direct methods
.method public constructor <init>(Lco/uk/getmondo/spending/MonthSpendingView;Landroid/view/View;)V
    .locals 3

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    iput-object p1, p0, Lco/uk/getmondo/spending/MonthSpendingView_ViewBinding;->a:Lco/uk/getmondo/spending/MonthSpendingView;

    .line 28
    const v0, 0x7f1104b9

    const-string v1, "field \'monthAmountView\'"

    const-class v2, Lco/uk/getmondo/common/ui/AmountView;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/common/ui/AmountView;

    iput-object v0, p1, Lco/uk/getmondo/spending/MonthSpendingView;->monthAmountView:Lco/uk/getmondo/common/ui/AmountView;

    .line 29
    const v0, 0x7f110430

    const-string v1, "field \'monthNameTextView\'"

    const-class v2, Landroid/widget/TextView;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p1, Lco/uk/getmondo/spending/MonthSpendingView;->monthNameTextView:Landroid/widget/TextView;

    .line 30
    const v0, 0x7f1104ba

    const-string v1, "field \'cardsContainer\'"

    const-class v2, Landroid/widget/LinearLayout;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p1, Lco/uk/getmondo/spending/MonthSpendingView;->cardsContainer:Landroid/widget/LinearLayout;

    .line 31
    return-void
.end method


# virtual methods
.method public unbind()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 36
    iget-object v0, p0, Lco/uk/getmondo/spending/MonthSpendingView_ViewBinding;->a:Lco/uk/getmondo/spending/MonthSpendingView;

    .line 37
    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Bindings already cleared."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 38
    :cond_0
    iput-object v1, p0, Lco/uk/getmondo/spending/MonthSpendingView_ViewBinding;->a:Lco/uk/getmondo/spending/MonthSpendingView;

    .line 40
    iput-object v1, v0, Lco/uk/getmondo/spending/MonthSpendingView;->monthAmountView:Lco/uk/getmondo/common/ui/AmountView;

    .line 41
    iput-object v1, v0, Lco/uk/getmondo/spending/MonthSpendingView;->monthNameTextView:Landroid/widget/TextView;

    .line 42
    iput-object v1, v0, Lco/uk/getmondo/spending/MonthSpendingView;->cardsContainer:Landroid/widget/LinearLayout;

    .line 43
    return-void
.end method
