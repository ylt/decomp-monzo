.class public Lco/uk/getmondo/spending/SpendingCategoryDetailsActivity_ViewBinding;
.super Ljava/lang/Object;
.source "SpendingCategoryDetailsActivity_ViewBinding.java"

# interfaces
.implements Lbutterknife/Unbinder;


# instance fields
.field private a:Lco/uk/getmondo/spending/SpendingCategoryDetailsActivity;


# direct methods
.method public constructor <init>(Lco/uk/getmondo/spending/SpendingCategoryDetailsActivity;Landroid/view/View;)V
    .locals 3

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    iput-object p1, p0, Lco/uk/getmondo/spending/SpendingCategoryDetailsActivity_ViewBinding;->a:Lco/uk/getmondo/spending/SpendingCategoryDetailsActivity;

    .line 29
    const v0, 0x7f110228

    const-string v1, "field \'appBarLayout\'"

    const-class v2, Landroid/support/design/widget/AppBarLayout;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/design/widget/AppBarLayout;

    iput-object v0, p1, Lco/uk/getmondo/spending/SpendingCategoryDetailsActivity;->appBarLayout:Landroid/support/design/widget/AppBarLayout;

    .line 30
    const v0, 0x7f1100fe

    const-string v1, "field \'toolbar\'"

    const-class v2, Landroid/support/v7/widget/Toolbar;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/Toolbar;

    iput-object v0, p1, Lco/uk/getmondo/spending/SpendingCategoryDetailsActivity;->toolbar:Landroid/support/v7/widget/Toolbar;

    .line 31
    const v0, 0x7f110229

    const-string v1, "field \'tabLayout\'"

    const-class v2, Landroid/support/design/widget/TabLayout;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/design/widget/TabLayout;

    iput-object v0, p1, Lco/uk/getmondo/spending/SpendingCategoryDetailsActivity;->tabLayout:Landroid/support/design/widget/TabLayout;

    .line 32
    const v0, 0x7f110227

    const-string v1, "field \'viewPager\'"

    const-class v2, Landroid/support/v4/view/ViewPager;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/view/ViewPager;

    iput-object v0, p1, Lco/uk/getmondo/spending/SpendingCategoryDetailsActivity;->viewPager:Landroid/support/v4/view/ViewPager;

    .line 33
    return-void
.end method


# virtual methods
.method public unbind()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 38
    iget-object v0, p0, Lco/uk/getmondo/spending/SpendingCategoryDetailsActivity_ViewBinding;->a:Lco/uk/getmondo/spending/SpendingCategoryDetailsActivity;

    .line 39
    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Bindings already cleared."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 40
    :cond_0
    iput-object v1, p0, Lco/uk/getmondo/spending/SpendingCategoryDetailsActivity_ViewBinding;->a:Lco/uk/getmondo/spending/SpendingCategoryDetailsActivity;

    .line 42
    iput-object v1, v0, Lco/uk/getmondo/spending/SpendingCategoryDetailsActivity;->appBarLayout:Landroid/support/design/widget/AppBarLayout;

    .line 43
    iput-object v1, v0, Lco/uk/getmondo/spending/SpendingCategoryDetailsActivity;->toolbar:Landroid/support/v7/widget/Toolbar;

    .line 44
    iput-object v1, v0, Lco/uk/getmondo/spending/SpendingCategoryDetailsActivity;->tabLayout:Landroid/support/design/widget/TabLayout;

    .line 45
    iput-object v1, v0, Lco/uk/getmondo/spending/SpendingCategoryDetailsActivity;->viewPager:Landroid/support/v4/view/ViewPager;

    .line 46
    return-void
.end method
