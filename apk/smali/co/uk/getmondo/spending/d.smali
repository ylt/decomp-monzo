.class public final Lco/uk/getmondo/spending/d;
.super Lco/uk/getmondo/common/ui/b;
.source "SpendingPresenter.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lco/uk/getmondo/spending/d$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lco/uk/getmondo/common/ui/b",
        "<",
        "Lco/uk/getmondo/spending/d$a;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000J\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u0003\u0008\u0007\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00020\u0001:\u0001\u0019B]\u0008\u0007\u0012\u0008\u0008\u0001\u0010\u0003\u001a\u00020\u0004\u0012\u0008\u0008\u0001\u0010\u0005\u001a\u00020\u0004\u0012\u0008\u0008\u0001\u0010\u0006\u001a\u00020\u0004\u0012\u0006\u0010\u0007\u001a\u00020\u0008\u0012\u0006\u0010\t\u001a\u00020\n\u0012\u0006\u0010\u000b\u001a\u00020\u000c\u0012\u0006\u0010\r\u001a\u00020\u000e\u0012\u0006\u0010\u000f\u001a\u00020\u0010\u0012\u0006\u0010\u0011\u001a\u00020\u0012\u0012\u0006\u0010\u0013\u001a\u00020\u0014\u00a2\u0006\u0002\u0010\u0015J\u0010\u0010\u0016\u001a\u00020\u00172\u0006\u0010\u0018\u001a\u00020\u0002H\u0016R\u000e\u0010\u0013\u001a\u00020\u0014X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u000cX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\u0010X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0011\u001a\u00020\u0012X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u000eX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\nX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0008X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001a"
    }
    d2 = {
        "Lco/uk/getmondo/spending/SpendingPresenter;",
        "Lco/uk/getmondo/common/ui/BasePresenter;",
        "Lco/uk/getmondo/spending/SpendingPresenter$View;",
        "ioScheduler",
        "Lio/reactivex/Scheduler;",
        "uiScheduler",
        "computation",
        "spendingRepository",
        "Lco/uk/getmondo/spending/data/SpendingRepository;",
        "spendingCalculator",
        "Lco/uk/getmondo/spending/data/SpendingCalculator;",
        "analyticsService",
        "Lco/uk/getmondo/common/AnalyticsService;",
        "recurringPaymentsManager",
        "Lco/uk/getmondo/payments/data/RecurringPaymentsManager;",
        "apiErrorHandler",
        "Lco/uk/getmondo/common/errors/ApiErrorHandler;",
        "cardManager",
        "Lco/uk/getmondo/card/CardManager;",
        "accountManager",
        "Lco/uk/getmondo/common/accounts/AccountManager;",
        "(Lio/reactivex/Scheduler;Lio/reactivex/Scheduler;Lio/reactivex/Scheduler;Lco/uk/getmondo/spending/data/SpendingRepository;Lco/uk/getmondo/spending/data/SpendingCalculator;Lco/uk/getmondo/common/AnalyticsService;Lco/uk/getmondo/payments/data/RecurringPaymentsManager;Lco/uk/getmondo/common/errors/ApiErrorHandler;Lco/uk/getmondo/card/CardManager;Lco/uk/getmondo/common/accounts/AccountManager;)V",
        "register",
        "",
        "view",
        "View",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field private final c:Lio/reactivex/u;

.field private final d:Lio/reactivex/u;

.field private final e:Lio/reactivex/u;

.field private final f:Lco/uk/getmondo/spending/a/i;

.field private final g:Lco/uk/getmondo/spending/a/b;

.field private final h:Lco/uk/getmondo/common/a;

.field private final i:Lco/uk/getmondo/payments/a/i;

.field private final j:Lco/uk/getmondo/common/e/a;

.field private final k:Lco/uk/getmondo/card/c;

.field private final l:Lco/uk/getmondo/common/accounts/b;


# direct methods
.method public constructor <init>(Lio/reactivex/u;Lio/reactivex/u;Lio/reactivex/u;Lco/uk/getmondo/spending/a/i;Lco/uk/getmondo/spending/a/b;Lco/uk/getmondo/common/a;Lco/uk/getmondo/payments/a/i;Lco/uk/getmondo/common/e/a;Lco/uk/getmondo/card/c;Lco/uk/getmondo/common/accounts/b;)V
    .locals 1

    .prologue
    const-string v0, "ioScheduler"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "uiScheduler"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "computation"

    invoke-static {p3, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "spendingRepository"

    invoke-static {p4, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "spendingCalculator"

    invoke-static {p5, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "analyticsService"

    invoke-static {p6, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "recurringPaymentsManager"

    invoke-static {p7, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "apiErrorHandler"

    invoke-static {p8, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "cardManager"

    invoke-static {p9, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "accountManager"

    invoke-static {p10, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 41
    invoke-direct {p0}, Lco/uk/getmondo/common/ui/b;-><init>()V

    iput-object p1, p0, Lco/uk/getmondo/spending/d;->c:Lio/reactivex/u;

    iput-object p2, p0, Lco/uk/getmondo/spending/d;->d:Lio/reactivex/u;

    iput-object p3, p0, Lco/uk/getmondo/spending/d;->e:Lio/reactivex/u;

    iput-object p4, p0, Lco/uk/getmondo/spending/d;->f:Lco/uk/getmondo/spending/a/i;

    iput-object p5, p0, Lco/uk/getmondo/spending/d;->g:Lco/uk/getmondo/spending/a/b;

    iput-object p6, p0, Lco/uk/getmondo/spending/d;->h:Lco/uk/getmondo/common/a;

    iput-object p7, p0, Lco/uk/getmondo/spending/d;->i:Lco/uk/getmondo/payments/a/i;

    iput-object p8, p0, Lco/uk/getmondo/spending/d;->j:Lco/uk/getmondo/common/e/a;

    iput-object p9, p0, Lco/uk/getmondo/spending/d;->k:Lco/uk/getmondo/card/c;

    iput-object p10, p0, Lco/uk/getmondo/spending/d;->l:Lco/uk/getmondo/common/accounts/b;

    return-void
.end method

.method public static final synthetic a(Lco/uk/getmondo/spending/d;)Lco/uk/getmondo/spending/a/b;
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, Lco/uk/getmondo/spending/d;->g:Lco/uk/getmondo/spending/a/b;

    return-object v0
.end method

.method public static final synthetic b(Lco/uk/getmondo/spending/d;)Lio/reactivex/u;
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, Lco/uk/getmondo/spending/d;->e:Lio/reactivex/u;

    return-object v0
.end method

.method public static final synthetic c(Lco/uk/getmondo/spending/d;)Lco/uk/getmondo/common/e/a;
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, Lco/uk/getmondo/spending/d;->j:Lco/uk/getmondo/common/e/a;

    return-object v0
.end method


# virtual methods
.method public bridge synthetic a(Lco/uk/getmondo/common/ui/f;)V
    .locals 0

    .prologue
    .line 29
    check-cast p1, Lco/uk/getmondo/spending/d$a;

    invoke-virtual {p0, p1}, Lco/uk/getmondo/spending/d;->a(Lco/uk/getmondo/spending/d$a;)V

    return-void
.end method

.method public a(Lco/uk/getmondo/spending/d$a;)V
    .locals 5

    .prologue
    const-string v0, "view"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v0, p1

    .line 44
    check-cast v0, Lco/uk/getmondo/common/ui/f;

    invoke-super {p0, v0}, Lco/uk/getmondo/common/ui/b;->a(Lco/uk/getmondo/common/ui/f;)V

    .line 46
    iget-object v0, p0, Lco/uk/getmondo/spending/d;->h:Lco/uk/getmondo/common/a;

    sget-object v1, Lco/uk/getmondo/api/model/tracking/Impression;->Companion:Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    invoke-virtual {v1}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->S()Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v1

    invoke-virtual {v0, v1}, Lco/uk/getmondo/common/a;->a(Lco/uk/getmondo/api/model/tracking/Impression;)V

    .line 48
    iget-object v3, p0, Lco/uk/getmondo/spending/d;->b:Lio/reactivex/b/a;

    .line 55
    iget-object v0, p0, Lco/uk/getmondo/spending/d;->f:Lco/uk/getmondo/spending/a/i;

    invoke-virtual {v0}, Lco/uk/getmondo/spending/a/i;->a()Lio/reactivex/n;

    move-result-object v1

    .line 49
    new-instance v0, Lco/uk/getmondo/spending/d$b;

    invoke-direct {v0, p0}, Lco/uk/getmondo/spending/d$b;-><init>(Lco/uk/getmondo/spending/d;)V

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {v1, v0}, Lio/reactivex/n;->flatMapSingle(Lio/reactivex/c/h;)Lio/reactivex/n;

    move-result-object v0

    .line 54
    iget-object v1, p0, Lco/uk/getmondo/spending/d;->d:Lio/reactivex/u;

    invoke-virtual {v0, v1}, Lio/reactivex/n;->observeOn(Lio/reactivex/u;)Lio/reactivex/n;

    move-result-object v4

    .line 55
    new-instance v0, Lco/uk/getmondo/spending/d$d;

    invoke-direct {v0, p1}, Lco/uk/getmondo/spending/d$d;-><init>(Lco/uk/getmondo/spending/d$a;)V

    check-cast v0, Lio/reactivex/c/g;

    .line 63
    sget-object v1, Lco/uk/getmondo/spending/d$e;->a:Lco/uk/getmondo/spending/d$e;

    check-cast v1, Lkotlin/d/a/b;

    if-eqz v1, :cond_0

    new-instance v2, Lco/uk/getmondo/spending/e;

    invoke-direct {v2, v1}, Lco/uk/getmondo/spending/e;-><init>(Lkotlin/d/a/b;)V

    move-object v1, v2

    :cond_0
    check-cast v1, Lio/reactivex/c/g;

    .line 55
    invoke-virtual {v4, v0, v1}, Lio/reactivex/n;->subscribe(Lio/reactivex/c/g;Lio/reactivex/c/g;)Lio/reactivex/b/b;

    move-result-object v0

    const-string v1, "spendingRepository.allTr\u2026            }, Timber::e)"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 48
    invoke-static {v3, v0}, Lco/uk/getmondo/common/j/f;->a(Lio/reactivex/b/a;Lio/reactivex/b/b;)Lio/reactivex/b/a;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/spending/d;->b:Lio/reactivex/b/a;

    .line 65
    iget-object v2, p0, Lco/uk/getmondo/spending/d;->b:Lio/reactivex/b/a;

    .line 67
    iget-object v0, p0, Lco/uk/getmondo/spending/d;->k:Lco/uk/getmondo/card/c;

    invoke-virtual {v0}, Lco/uk/getmondo/card/c;->a()Lio/reactivex/n;

    move-result-object v0

    .line 66
    iget-object v1, p0, Lco/uk/getmondo/spending/d;->d:Lio/reactivex/u;

    invoke-virtual {v0, v1}, Lio/reactivex/n;->observeOn(Lio/reactivex/u;)Lio/reactivex/n;

    move-result-object v3

    .line 68
    new-instance v0, Lco/uk/getmondo/spending/d$f;

    invoke-direct {v0, p1}, Lco/uk/getmondo/spending/d$f;-><init>(Lco/uk/getmondo/spending/d$a;)V

    check-cast v0, Lio/reactivex/c/g;

    .line 69
    sget-object v1, Lco/uk/getmondo/spending/d$g;->a:Lco/uk/getmondo/spending/d$g;

    check-cast v1, Lio/reactivex/c/g;

    .line 67
    invoke-virtual {v3, v0, v1}, Lio/reactivex/n;->subscribe(Lio/reactivex/c/g;Lio/reactivex/c/g;)Lio/reactivex/b/b;

    move-result-object v0

    const-string v1, "cardManager.card()\n     \u2026ed to get card state\") })"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 65
    invoke-static {v2, v0}, Lco/uk/getmondo/common/j/f;->a(Lio/reactivex/b/a;Lio/reactivex/b/b;)Lio/reactivex/b/a;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/spending/d;->b:Lio/reactivex/b/a;

    .line 71
    iget-object v1, p0, Lco/uk/getmondo/spending/d;->b:Lio/reactivex/b/a;

    .line 72
    invoke-interface {p1}, Lco/uk/getmondo/spending/d$a;->a()Lio/reactivex/n;

    move-result-object v2

    new-instance v0, Lco/uk/getmondo/spending/d$h;

    invoke-direct {v0, p1}, Lco/uk/getmondo/spending/d$h;-><init>(Lco/uk/getmondo/spending/d$a;)V

    check-cast v0, Lio/reactivex/c/g;

    invoke-virtual {v2, v0}, Lio/reactivex/n;->subscribe(Lio/reactivex/c/g;)Lio/reactivex/b/b;

    move-result-object v0

    const-string v2, "view.exportClicks\n      \u2026iew.showExportData(it) })"

    invoke-static {v0, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 71
    invoke-static {v1, v0}, Lco/uk/getmondo/common/j/f;->a(Lio/reactivex/b/a;Lio/reactivex/b/b;)Lio/reactivex/b/a;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/spending/d;->b:Lio/reactivex/b/a;

    .line 75
    iget-object v0, p0, Lco/uk/getmondo/spending/d;->l:Lco/uk/getmondo/common/accounts/b;

    invoke-virtual {v0}, Lco/uk/getmondo/common/accounts/b;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 76
    iget-object v2, p0, Lco/uk/getmondo/spending/d;->b:Lio/reactivex/b/a;

    .line 79
    iget-object v0, p0, Lco/uk/getmondo/spending/d;->i:Lco/uk/getmondo/payments/a/i;

    invoke-virtual {v0}, Lco/uk/getmondo/payments/a/i;->c()Lio/reactivex/b;

    move-result-object v0

    .line 77
    iget-object v1, p0, Lco/uk/getmondo/spending/d;->c:Lio/reactivex/u;

    invoke-virtual {v0, v1}, Lio/reactivex/b;->b(Lio/reactivex/u;)Lio/reactivex/b;

    move-result-object v0

    .line 78
    iget-object v1, p0, Lco/uk/getmondo/spending/d;->d:Lio/reactivex/u;

    invoke-virtual {v0, v1}, Lio/reactivex/b;->a(Lio/reactivex/u;)Lio/reactivex/b;

    move-result-object v3

    .line 80
    sget-object v0, Lco/uk/getmondo/spending/d$i;->a:Lco/uk/getmondo/spending/d$i;

    check-cast v0, Lio/reactivex/c/a;

    .line 81
    new-instance v1, Lco/uk/getmondo/spending/d$j;

    invoke-direct {v1, p0, p1}, Lco/uk/getmondo/spending/d$j;-><init>(Lco/uk/getmondo/spending/d;Lco/uk/getmondo/spending/d$a;)V

    check-cast v1, Lio/reactivex/c/g;

    .line 79
    invoke-virtual {v3, v0, v1}, Lio/reactivex/b;->a(Lio/reactivex/c/a;Lio/reactivex/c/g;)Lio/reactivex/b/b;

    move-result-object v0

    const-string v1, "recurringPaymentsManager\u2026  }\n                    )"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 76
    invoke-static {v2, v0}, Lco/uk/getmondo/common/j/f;->a(Lio/reactivex/b/a;Lio/reactivex/b/b;)Lio/reactivex/b/a;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/spending/d;->b:Lio/reactivex/b/a;

    .line 88
    iget-object v1, p0, Lco/uk/getmondo/spending/d;->b:Lio/reactivex/b/a;

    .line 90
    iget-object v0, p0, Lco/uk/getmondo/spending/d;->i:Lco/uk/getmondo/payments/a/i;

    invoke-virtual {v0}, Lco/uk/getmondo/payments/a/i;->g()Lio/reactivex/n;

    move-result-object v0

    .line 89
    iget-object v2, p0, Lco/uk/getmondo/spending/d;->d:Lio/reactivex/u;

    invoke-virtual {v0, v2}, Lio/reactivex/n;->observeOn(Lio/reactivex/u;)Lio/reactivex/n;

    move-result-object v2

    .line 90
    new-instance v0, Lco/uk/getmondo/spending/d$k;

    invoke-direct {v0, p1}, Lco/uk/getmondo/spending/d$k;-><init>(Lco/uk/getmondo/spending/d$a;)V

    check-cast v0, Lio/reactivex/c/g;

    invoke-virtual {v2, v0}, Lio/reactivex/n;->subscribe(Lio/reactivex/c/g;)Lio/reactivex/b/b;

    move-result-object v0

    const-string v2, "recurringPaymentsManager\u2026  }\n                    }"

    invoke-static {v0, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 88
    invoke-static {v1, v0}, Lco/uk/getmondo/common/j/f;->a(Lio/reactivex/b/a;Lio/reactivex/b/b;)Lio/reactivex/b/a;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/spending/d;->b:Lio/reactivex/b/a;

    .line 98
    iget-object v1, p0, Lco/uk/getmondo/spending/d;->b:Lio/reactivex/b/a;

    .line 99
    invoke-interface {p1}, Lco/uk/getmondo/spending/d$a;->b()Lio/reactivex/n;

    move-result-object v2

    new-instance v0, Lco/uk/getmondo/spending/d$c;

    invoke-direct {v0, p1}, Lco/uk/getmondo/spending/d$c;-><init>(Lco/uk/getmondo/spending/d$a;)V

    check-cast v0, Lio/reactivex/c/g;

    invoke-virtual {v2, v0}, Lio/reactivex/n;->subscribe(Lio/reactivex/c/g;)Lio/reactivex/b/b;

    move-result-object v0

    const-string v2, "view.onRecurringPayments\u2026openRecurringPayments() }"

    invoke-static {v0, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 98
    invoke-static {v1, v0}, Lco/uk/getmondo/common/j/f;->a(Lio/reactivex/b/a;Lio/reactivex/b/b;)Lio/reactivex/b/a;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/spending/d;->b:Lio/reactivex/b/a;

    .line 101
    :cond_1
    return-void
.end method
