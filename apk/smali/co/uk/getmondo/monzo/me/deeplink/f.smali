.class Lco/uk/getmondo/monzo/me/deeplink/f;
.super Lco/uk/getmondo/common/ui/b;
.source "MonzoMePresenter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lco/uk/getmondo/monzo/me/deeplink/f$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lco/uk/getmondo/common/ui/b",
        "<",
        "Lco/uk/getmondo/monzo/me/deeplink/f$a;",
        ">;"
    }
.end annotation


# instance fields
.field private final c:Lco/uk/getmondo/common/accounts/d;

.field private final d:Landroid/net/Uri;

.field private final e:Lco/uk/getmondo/payments/send/data/p;


# direct methods
.method constructor <init>(Lco/uk/getmondo/common/accounts/d;Landroid/net/Uri;Lco/uk/getmondo/payments/send/data/p;)V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, Lco/uk/getmondo/common/ui/b;-><init>()V

    .line 29
    iput-object p1, p0, Lco/uk/getmondo/monzo/me/deeplink/f;->c:Lco/uk/getmondo/common/accounts/d;

    .line 30
    iput-object p2, p0, Lco/uk/getmondo/monzo/me/deeplink/f;->d:Landroid/net/Uri;

    .line 31
    iput-object p3, p0, Lco/uk/getmondo/monzo/me/deeplink/f;->e:Lco/uk/getmondo/payments/send/data/p;

    .line 32
    return-void
.end method

.method private a(Ljava/lang/String;)Lco/uk/getmondo/d/c;
    .locals 4

    .prologue
    .line 58
    :try_start_0
    new-instance v0, Ljava/math/BigDecimal;

    invoke-direct {v0, p1}, Ljava/math/BigDecimal;-><init>(Ljava/lang/String;)V

    sget-object v1, Lco/uk/getmondo/common/i/c;->a:Lco/uk/getmondo/common/i/c;

    invoke-virtual {v1}, Lco/uk/getmondo/common/i/c;->a()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/math/BigDecimal;->scaleByPowerOfTen(I)Ljava/math/BigDecimal;

    move-result-object v1

    .line 59
    new-instance v0, Lco/uk/getmondo/d/c;

    invoke-virtual {v1}, Ljava/math/BigDecimal;->longValue()J

    move-result-wide v2

    sget-object v1, Lco/uk/getmondo/common/i/c;->a:Lco/uk/getmondo/common/i/c;

    invoke-direct {v0, v2, v3, v1}, Lco/uk/getmondo/d/c;-><init>(JLco/uk/getmondo/common/i/c;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 63
    :goto_0
    return-object v0

    .line 60
    :catch_0
    move-exception v0

    .line 61
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public bridge synthetic a(Lco/uk/getmondo/common/ui/f;)V
    .locals 0

    .prologue
    .line 21
    check-cast p1, Lco/uk/getmondo/monzo/me/deeplink/f$a;

    invoke-virtual {p0, p1}, Lco/uk/getmondo/monzo/me/deeplink/f;->a(Lco/uk/getmondo/monzo/me/deeplink/f$a;)V

    return-void
.end method

.method public a(Lco/uk/getmondo/monzo/me/deeplink/f$a;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 36
    invoke-super {p0, p1}, Lco/uk/getmondo/common/ui/b;->a(Lco/uk/getmondo/common/ui/f;)V

    .line 38
    iget-object v0, p0, Lco/uk/getmondo/monzo/me/deeplink/f;->c:Lco/uk/getmondo/common/accounts/d;

    invoke-virtual {v0}, Lco/uk/getmondo/common/accounts/d;->b()Lco/uk/getmondo/d/ak;

    move-result-object v0

    .line 39
    iget-object v1, p0, Lco/uk/getmondo/monzo/me/deeplink/f;->d:Landroid/net/Uri;

    if-eqz v1, :cond_2

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lco/uk/getmondo/d/ak;->a()Lco/uk/getmondo/d/ak$a;

    move-result-object v0

    sget-object v1, Lco/uk/getmondo/d/ak$a;->HAS_ACCOUNT:Lco/uk/getmondo/d/ak$a;

    if-ne v0, v1, :cond_2

    .line 40
    iget-object v0, p0, Lco/uk/getmondo/monzo/me/deeplink/f;->e:Lco/uk/getmondo/payments/send/data/p;

    invoke-virtual {v0}, Lco/uk/getmondo/payments/send/data/p;->a()Lco/uk/getmondo/payments/send/data/a/d;

    move-result-object v0

    sget-object v1, Lco/uk/getmondo/payments/send/data/a/d;->c:Lco/uk/getmondo/payments/send/data/a/d;

    if-ne v0, v1, :cond_0

    .line 41
    invoke-interface {p1}, Lco/uk/getmondo/monzo/me/deeplink/f$a;->a()V

    .line 52
    :goto_0
    return-void

    .line 43
    :cond_0
    iget-object v0, p0, Lco/uk/getmondo/monzo/me/deeplink/f;->d:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v1

    .line 44
    const/4 v0, 0x0

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 45
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v2

    if-le v2, v3, :cond_1

    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 46
    :goto_1
    iget-object v2, p0, Lco/uk/getmondo/monzo/me/deeplink/f;->d:Landroid/net/Uri;

    const-string v3, "d"

    invoke-virtual {v2, v3}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 47
    invoke-direct {p0, v1}, Lco/uk/getmondo/monzo/me/deeplink/f;->a(Ljava/lang/String;)Lco/uk/getmondo/d/c;

    move-result-object v1

    invoke-interface {p1, v0, v1, v2}, Lco/uk/getmondo/monzo/me/deeplink/f$a;->a(Ljava/lang/String;Lco/uk/getmondo/d/c;Ljava/lang/String;)V

    goto :goto_0

    .line 45
    :cond_1
    const-string v1, ""

    goto :goto_1

    .line 50
    :cond_2
    iget-object v0, p0, Lco/uk/getmondo/monzo/me/deeplink/f;->d:Landroid/net/Uri;

    invoke-interface {p1, v0}, Lco/uk/getmondo/monzo/me/deeplink/f$a;->a(Landroid/net/Uri;)V

    goto :goto_0
.end method
