.class public Lco/uk/getmondo/monzo/me/deeplink/MonzoMeActivity;
.super Lco/uk/getmondo/common/activities/b;
.source "MonzoMeActivity.java"

# interfaces
.implements Lco/uk/getmondo/monzo/me/deeplink/f$a;


# instance fields
.field a:Lco/uk/getmondo/monzo/me/deeplink/f;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Lco/uk/getmondo/common/activities/b;-><init>()V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 3

    .prologue
    .line 38
    const/4 v0, 0x1

    invoke-static {v0}, Lco/uk/getmondo/common/d/e;->a(Z)Lco/uk/getmondo/common/d/e;

    move-result-object v0

    invoke-virtual {p0}, Lco/uk/getmondo/monzo/me/deeplink/MonzoMeActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const-string v2, "tag_error_dialog"

    invoke-virtual {v0, v1, v2}, Lco/uk/getmondo/common/d/e;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    .line 39
    return-void
.end method

.method public a(Landroid/net/Uri;)V
    .locals 2

    .prologue
    .line 43
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-direct {v0, v1, p1}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 44
    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 45
    const-string v1, "com.android.chrome"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 47
    :try_start_0
    invoke-virtual {p0, v0}, Lco/uk/getmondo/monzo/me/deeplink/MonzoMeActivity;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 53
    :goto_0
    return-void

    .line 48
    :catch_0
    move-exception v1

    .line 50
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 51
    invoke-virtual {p0, v0}, Lco/uk/getmondo/monzo/me/deeplink/MonzoMeActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method public a(Ljava/lang/String;Lco/uk/getmondo/d/c;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 57
    invoke-static {p0, p1, p2, p3}, Lco/uk/getmondo/main/HomeActivity;->a(Landroid/content/Context;Ljava/lang/String;Lco/uk/getmondo/d/c;Ljava/lang/String;)V

    .line 58
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 23
    invoke-super {p0, p1}, Lco/uk/getmondo/common/activities/b;->onCreate(Landroid/os/Bundle;)V

    .line 25
    invoke-virtual {p0}, Lco/uk/getmondo/monzo/me/deeplink/MonzoMeActivity;->l()Lco/uk/getmondo/common/h/b/b;

    move-result-object v0

    new-instance v1, Lco/uk/getmondo/monzo/me/deeplink/c;

    invoke-virtual {p0}, Lco/uk/getmondo/monzo/me/deeplink/MonzoMeActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v2

    invoke-direct {v1, v2}, Lco/uk/getmondo/monzo/me/deeplink/c;-><init>(Landroid/net/Uri;)V

    invoke-interface {v0, v1}, Lco/uk/getmondo/common/h/b/b;->a(Lco/uk/getmondo/monzo/me/deeplink/c;)Lco/uk/getmondo/monzo/me/deeplink/b;

    move-result-object v0

    invoke-interface {v0, p0}, Lco/uk/getmondo/monzo/me/deeplink/b;->a(Lco/uk/getmondo/monzo/me/deeplink/MonzoMeActivity;)V

    .line 27
    iget-object v0, p0, Lco/uk/getmondo/monzo/me/deeplink/MonzoMeActivity;->a:Lco/uk/getmondo/monzo/me/deeplink/f;

    invoke-virtual {v0, p0}, Lco/uk/getmondo/monzo/me/deeplink/f;->a(Lco/uk/getmondo/monzo/me/deeplink/f$a;)V

    .line 28
    return-void
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lco/uk/getmondo/monzo/me/deeplink/MonzoMeActivity;->a:Lco/uk/getmondo/monzo/me/deeplink/f;

    invoke-virtual {v0}, Lco/uk/getmondo/monzo/me/deeplink/f;->b()V

    .line 33
    invoke-super {p0}, Lco/uk/getmondo/common/activities/b;->onDestroy()V

    .line 34
    return-void
.end method
