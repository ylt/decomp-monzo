.class Lco/uk/getmondo/monzo/me/customise/i;
.super Lco/uk/getmondo/common/ui/b;
.source "CustomiseMonzoMeLinkPresenter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lco/uk/getmondo/monzo/me/customise/i$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lco/uk/getmondo/common/ui/b",
        "<",
        "Lco/uk/getmondo/monzo/me/customise/i$a;",
        ">;"
    }
.end annotation


# instance fields
.field private final c:Lco/uk/getmondo/common/a;

.field private final d:Lco/uk/getmondo/payments/send/data/h;

.field private final e:Lco/uk/getmondo/d/c;

.field private final f:Ljava/lang/String;

.field private final g:Lco/uk/getmondo/monzo/me/a;

.field private final h:Lco/uk/getmondo/api/model/tracking/Impression$CustomiseMonzoMeLinkFrom;

.field private final i:Lco/uk/getmondo/payments/send/data/p;


# direct methods
.method constructor <init>(Lco/uk/getmondo/common/a;Lco/uk/getmondo/payments/send/data/h;Lco/uk/getmondo/d/c;Ljava/lang/String;Lco/uk/getmondo/monzo/me/a;Lco/uk/getmondo/api/model/tracking/Impression$CustomiseMonzoMeLinkFrom;Lco/uk/getmondo/payments/send/data/p;)V
    .locals 0

    .prologue
    .line 37
    invoke-direct {p0}, Lco/uk/getmondo/common/ui/b;-><init>()V

    .line 38
    iput-object p1, p0, Lco/uk/getmondo/monzo/me/customise/i;->c:Lco/uk/getmondo/common/a;

    .line 39
    iput-object p2, p0, Lco/uk/getmondo/monzo/me/customise/i;->d:Lco/uk/getmondo/payments/send/data/h;

    .line 40
    iput-object p3, p0, Lco/uk/getmondo/monzo/me/customise/i;->e:Lco/uk/getmondo/d/c;

    .line 41
    iput-object p4, p0, Lco/uk/getmondo/monzo/me/customise/i;->f:Ljava/lang/String;

    .line 42
    iput-object p5, p0, Lco/uk/getmondo/monzo/me/customise/i;->g:Lco/uk/getmondo/monzo/me/a;

    .line 43
    iput-object p6, p0, Lco/uk/getmondo/monzo/me/customise/i;->h:Lco/uk/getmondo/api/model/tracking/Impression$CustomiseMonzoMeLinkFrom;

    .line 44
    iput-object p7, p0, Lco/uk/getmondo/monzo/me/customise/i;->i:Lco/uk/getmondo/payments/send/data/p;

    .line 45
    return-void
.end method

.method static synthetic a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Landroid/support/v4/g/j;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 89
    invoke-static {p1, p2}, Landroid/support/v4/g/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Landroid/support/v4/g/j;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lco/uk/getmondo/monzo/me/customise/i;Lco/uk/getmondo/monzo/me/customise/i$a;Landroid/support/v4/g/j;)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 91
    iget-object v0, p0, Lco/uk/getmondo/monzo/me/customise/i;->c:Lco/uk/getmondo/common/a;

    invoke-static {}, Lco/uk/getmondo/api/model/tracking/Impression;->S()Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v1

    invoke-virtual {v0, v1}, Lco/uk/getmondo/common/a;->a(Lco/uk/getmondo/api/model/tracking/Impression;)V

    .line 93
    iget-object v0, p0, Lco/uk/getmondo/monzo/me/customise/i;->d:Lco/uk/getmondo/payments/send/data/h;

    invoke-virtual {v0}, Lco/uk/getmondo/payments/send/data/h;->c()Ljava/lang/String;

    move-result-object v2

    .line 94
    iget-object v3, p0, Lco/uk/getmondo/monzo/me/customise/i;->g:Lco/uk/getmondo/monzo/me/a;

    iget-object v0, p2, Landroid/support/v4/g/j;->a:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    iget-object v1, p2, Landroid/support/v4/g/j;->b:Ljava/lang/Object;

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v3, v2, v0, v1}, Lco/uk/getmondo/monzo/me/a;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Lco/uk/getmondo/monzo/me/customise/i$a;->a(Ljava/lang/String;)V

    .line 95
    return-void
.end method

.method static synthetic b(Lco/uk/getmondo/monzo/me/customise/i;Lco/uk/getmondo/monzo/me/customise/i$a;Landroid/support/v4/g/j;)V
    .locals 11
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    const/4 v10, 0x1

    .line 57
    const/4 v0, 0x0

    invoke-interface {p1, v0}, Lco/uk/getmondo/monzo/me/customise/i$a;->a(Z)V

    .line 59
    iget-object v0, p2, Landroid/support/v4/g/j;->a:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    .line 60
    iget-object v1, p2, Landroid/support/v4/g/j;->b:Ljava/lang/Object;

    check-cast v1, Ljava/lang/String;

    .line 62
    invoke-static {v0}, Lco/uk/getmondo/common/k/p;->d(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 63
    invoke-interface {p1}, Lco/uk/getmondo/monzo/me/customise/i$a;->e()V

    .line 64
    invoke-static {v1}, Lco/uk/getmondo/common/k/p;->c(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 65
    invoke-interface {p1, v10}, Lco/uk/getmondo/monzo/me/customise/i$a;->a(Z)V

    .line 85
    :cond_0
    :goto_0
    return-void

    .line 68
    :cond_1
    iget-object v1, p0, Lco/uk/getmondo/monzo/me/customise/i;->i:Lco/uk/getmondo/payments/send/data/p;

    invoke-virtual {v1}, Lco/uk/getmondo/payments/send/data/p;->b()Lco/uk/getmondo/d/p;

    move-result-object v1

    .line 69
    sget-object v2, Lco/uk/getmondo/common/i/c;->a:Lco/uk/getmondo/common/i/c;

    invoke-static {v0, v2}, Lco/uk/getmondo/d/c;->a(Ljava/lang/String;Lco/uk/getmondo/common/i/c;)Lco/uk/getmondo/d/c;

    move-result-object v0

    .line 70
    if-eqz v0, :cond_0

    .line 72
    invoke-virtual {v1}, Lco/uk/getmondo/d/p;->b()I

    move-result v2

    int-to-long v2, v2

    .line 73
    invoke-virtual {v1}, Lco/uk/getmondo/d/p;->a()I

    move-result v4

    int-to-long v4, v4

    .line 74
    invoke-virtual {v0}, Lco/uk/getmondo/d/c;->f()D

    move-result-wide v6

    .line 75
    const-wide/16 v8, 0x0

    cmp-long v0, v4, v8

    if-gtz v0, :cond_2

    .line 76
    invoke-interface {p1}, Lco/uk/getmondo/monzo/me/customise/i$a;->d()V

    goto :goto_0

    .line 77
    :cond_2
    long-to-double v2, v2

    cmpg-double v0, v6, v2

    if-ltz v0, :cond_3

    long-to-double v2, v4

    cmpl-double v0, v6, v2

    if-lez v0, :cond_4

    .line 78
    :cond_3
    invoke-virtual {v1}, Lco/uk/getmondo/d/p;->b()I

    move-result v0

    invoke-virtual {v1}, Lco/uk/getmondo/d/p;->a()I

    move-result v1

    invoke-interface {p1, v0, v1}, Lco/uk/getmondo/monzo/me/customise/i$a;->a(II)V

    goto :goto_0

    .line 80
    :cond_4
    invoke-interface {p1}, Lco/uk/getmondo/monzo/me/customise/i$a;->e()V

    .line 81
    invoke-interface {p1, v10}, Lco/uk/getmondo/monzo/me/customise/i$a;->a(Z)V

    goto :goto_0
.end method


# virtual methods
.method public bridge synthetic a(Lco/uk/getmondo/common/ui/f;)V
    .locals 0

    .prologue
    .line 24
    check-cast p1, Lco/uk/getmondo/monzo/me/customise/i$a;

    invoke-virtual {p0, p1}, Lco/uk/getmondo/monzo/me/customise/i;->a(Lco/uk/getmondo/monzo/me/customise/i$a;)V

    return-void
.end method

.method public a(Lco/uk/getmondo/monzo/me/customise/i$a;)V
    .locals 4

    .prologue
    .line 49
    invoke-super {p0, p1}, Lco/uk/getmondo/common/ui/b;->a(Lco/uk/getmondo/common/ui/f;)V

    .line 51
    iget-object v0, p0, Lco/uk/getmondo/monzo/me/customise/i;->c:Lco/uk/getmondo/common/a;

    iget-object v1, p0, Lco/uk/getmondo/monzo/me/customise/i;->h:Lco/uk/getmondo/api/model/tracking/Impression$CustomiseMonzoMeLinkFrom;

    invoke-static {v1}, Lco/uk/getmondo/api/model/tracking/Impression;->a(Lco/uk/getmondo/api/model/tracking/Impression$CustomiseMonzoMeLinkFrom;)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v1

    invoke-virtual {v0, v1}, Lco/uk/getmondo/common/a;->a(Lco/uk/getmondo/api/model/tracking/Impression;)V

    .line 53
    iget-object v0, p0, Lco/uk/getmondo/monzo/me/customise/i;->e:Lco/uk/getmondo/d/c;

    iget-object v1, p0, Lco/uk/getmondo/monzo/me/customise/i;->f:Ljava/lang/String;

    invoke-interface {p1, v0, v1}, Lco/uk/getmondo/monzo/me/customise/i$a;->a(Lco/uk/getmondo/d/c;Ljava/lang/String;)V

    .line 55
    invoke-interface {p1}, Lco/uk/getmondo/monzo/me/customise/i$a;->a()Lio/reactivex/n;

    move-result-object v0

    invoke-interface {p1}, Lco/uk/getmondo/monzo/me/customise/i$a;->b()Lio/reactivex/n;

    move-result-object v1

    invoke-static {}, Lco/uk/getmondo/monzo/me/customise/j;->a()Lio/reactivex/c/c;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lio/reactivex/n;->combineLatest(Lio/reactivex/r;Lio/reactivex/r;Lio/reactivex/c/c;)Lio/reactivex/n;

    move-result-object v0

    invoke-static {p0, p1}, Lco/uk/getmondo/monzo/me/customise/k;->a(Lco/uk/getmondo/monzo/me/customise/i;Lco/uk/getmondo/monzo/me/customise/i$a;)Lio/reactivex/c/g;

    move-result-object v1

    .line 56
    invoke-virtual {v0, v1}, Lio/reactivex/n;->subscribe(Lio/reactivex/c/g;)Lio/reactivex/b/b;

    move-result-object v0

    .line 55
    invoke-virtual {p0, v0}, Lco/uk/getmondo/monzo/me/customise/i;->a(Lio/reactivex/b/b;)V

    .line 87
    invoke-interface {p1}, Lco/uk/getmondo/monzo/me/customise/i$a;->c()Lio/reactivex/n;

    move-result-object v0

    .line 88
    invoke-interface {p1}, Lco/uk/getmondo/monzo/me/customise/i$a;->a()Lio/reactivex/n;

    move-result-object v1

    invoke-interface {p1}, Lco/uk/getmondo/monzo/me/customise/i$a;->b()Lio/reactivex/n;

    move-result-object v2

    invoke-static {}, Lco/uk/getmondo/monzo/me/customise/l;->a()Lio/reactivex/c/i;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Lio/reactivex/n;->withLatestFrom(Lio/reactivex/r;Lio/reactivex/r;Lio/reactivex/c/i;)Lio/reactivex/n;

    move-result-object v0

    invoke-static {p0, p1}, Lco/uk/getmondo/monzo/me/customise/m;->a(Lco/uk/getmondo/monzo/me/customise/i;Lco/uk/getmondo/monzo/me/customise/i$a;)Lio/reactivex/c/g;

    move-result-object v1

    invoke-static {}, Lco/uk/getmondo/monzo/me/customise/n;->a()Lio/reactivex/c/g;

    move-result-object v2

    .line 90
    invoke-virtual {v0, v1, v2}, Lio/reactivex/n;->subscribe(Lio/reactivex/c/g;Lio/reactivex/c/g;)Lio/reactivex/b/b;

    move-result-object v0

    .line 87
    invoke-virtual {p0, v0}, Lco/uk/getmondo/monzo/me/customise/i;->a(Lio/reactivex/b/b;)V

    .line 96
    return-void
.end method
