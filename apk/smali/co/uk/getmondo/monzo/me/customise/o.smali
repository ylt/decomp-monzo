.class public final Lco/uk/getmondo/monzo/me/customise/o;
.super Ljava/lang/Object;
.source "CustomiseMonzoMeLinkPresenter_Factory.java"

# interfaces
.implements Lb/a/b;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lb/a/b",
        "<",
        "Lco/uk/getmondo/monzo/me/customise/i;",
        ">;"
    }
.end annotation


# static fields
.field static final synthetic a:Z


# instance fields
.field private final b:Lb/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lb/a",
            "<",
            "Lco/uk/getmondo/monzo/me/customise/i;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Ljavax/a/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/common/a;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Ljavax/a/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/payments/send/data/h;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Ljavax/a/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/d/c;",
            ">;"
        }
    .end annotation
.end field

.field private final f:Ljavax/a/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/a/a",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final g:Ljavax/a/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/monzo/me/a;",
            ">;"
        }
    .end annotation
.end field

.field private final h:Ljavax/a/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/api/model/tracking/Impression$CustomiseMonzoMeLinkFrom;",
            ">;"
        }
    .end annotation
.end field

.field private final i:Ljavax/a/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/payments/send/data/p;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 15
    const-class v0, Lco/uk/getmondo/monzo/me/customise/o;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lco/uk/getmondo/monzo/me/customise/o;->a:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lb/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lb/a",
            "<",
            "Lco/uk/getmondo/monzo/me/customise/i;",
            ">;",
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/common/a;",
            ">;",
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/payments/send/data/h;",
            ">;",
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/d/c;",
            ">;",
            "Ljavax/a/a",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/monzo/me/a;",
            ">;",
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/api/model/tracking/Impression$CustomiseMonzoMeLinkFrom;",
            ">;",
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/payments/send/data/p;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 47
    sget-boolean v0, Lco/uk/getmondo/monzo/me/customise/o;->a:Z

    if-nez v0, :cond_0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 48
    :cond_0
    iput-object p1, p0, Lco/uk/getmondo/monzo/me/customise/o;->b:Lb/a;

    .line 50
    sget-boolean v0, Lco/uk/getmondo/monzo/me/customise/o;->a:Z

    if-nez v0, :cond_1

    if-nez p2, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 51
    :cond_1
    iput-object p2, p0, Lco/uk/getmondo/monzo/me/customise/o;->c:Ljavax/a/a;

    .line 52
    sget-boolean v0, Lco/uk/getmondo/monzo/me/customise/o;->a:Z

    if-nez v0, :cond_2

    if-nez p3, :cond_2

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 53
    :cond_2
    iput-object p3, p0, Lco/uk/getmondo/monzo/me/customise/o;->d:Ljavax/a/a;

    .line 54
    sget-boolean v0, Lco/uk/getmondo/monzo/me/customise/o;->a:Z

    if-nez v0, :cond_3

    if-nez p4, :cond_3

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 55
    :cond_3
    iput-object p4, p0, Lco/uk/getmondo/monzo/me/customise/o;->e:Ljavax/a/a;

    .line 56
    sget-boolean v0, Lco/uk/getmondo/monzo/me/customise/o;->a:Z

    if-nez v0, :cond_4

    if-nez p5, :cond_4

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 57
    :cond_4
    iput-object p5, p0, Lco/uk/getmondo/monzo/me/customise/o;->f:Ljavax/a/a;

    .line 58
    sget-boolean v0, Lco/uk/getmondo/monzo/me/customise/o;->a:Z

    if-nez v0, :cond_5

    if-nez p6, :cond_5

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 59
    :cond_5
    iput-object p6, p0, Lco/uk/getmondo/monzo/me/customise/o;->g:Ljavax/a/a;

    .line 60
    sget-boolean v0, Lco/uk/getmondo/monzo/me/customise/o;->a:Z

    if-nez v0, :cond_6

    if-nez p7, :cond_6

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 61
    :cond_6
    iput-object p7, p0, Lco/uk/getmondo/monzo/me/customise/o;->h:Ljavax/a/a;

    .line 62
    sget-boolean v0, Lco/uk/getmondo/monzo/me/customise/o;->a:Z

    if-nez v0, :cond_7

    if-nez p8, :cond_7

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 63
    :cond_7
    iput-object p8, p0, Lco/uk/getmondo/monzo/me/customise/o;->i:Ljavax/a/a;

    .line 64
    return-void
.end method

.method public static a(Lb/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;)Lb/a/b;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lb/a",
            "<",
            "Lco/uk/getmondo/monzo/me/customise/i;",
            ">;",
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/common/a;",
            ">;",
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/payments/send/data/h;",
            ">;",
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/d/c;",
            ">;",
            "Ljavax/a/a",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/monzo/me/a;",
            ">;",
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/api/model/tracking/Impression$CustomiseMonzoMeLinkFrom;",
            ">;",
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/payments/send/data/p;",
            ">;)",
            "Lb/a/b",
            "<",
            "Lco/uk/getmondo/monzo/me/customise/i;",
            ">;"
        }
    .end annotation

    .prologue
    .line 89
    new-instance v0, Lco/uk/getmondo/monzo/me/customise/o;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object v7, p6

    move-object/from16 v8, p7

    invoke-direct/range {v0 .. v8}, Lco/uk/getmondo/monzo/me/customise/o;-><init>(Lb/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;)V

    return-object v0
.end method


# virtual methods
.method public a()Lco/uk/getmondo/monzo/me/customise/i;
    .locals 9

    .prologue
    .line 68
    iget-object v8, p0, Lco/uk/getmondo/monzo/me/customise/o;->b:Lb/a;

    new-instance v0, Lco/uk/getmondo/monzo/me/customise/i;

    iget-object v1, p0, Lco/uk/getmondo/monzo/me/customise/o;->c:Ljavax/a/a;

    .line 71
    invoke-interface {v1}, Ljavax/a/a;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lco/uk/getmondo/common/a;

    iget-object v2, p0, Lco/uk/getmondo/monzo/me/customise/o;->d:Ljavax/a/a;

    .line 72
    invoke-interface {v2}, Ljavax/a/a;->b()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lco/uk/getmondo/payments/send/data/h;

    iget-object v3, p0, Lco/uk/getmondo/monzo/me/customise/o;->e:Ljavax/a/a;

    .line 73
    invoke-interface {v3}, Ljavax/a/a;->b()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lco/uk/getmondo/d/c;

    iget-object v4, p0, Lco/uk/getmondo/monzo/me/customise/o;->f:Ljavax/a/a;

    .line 74
    invoke-interface {v4}, Ljavax/a/a;->b()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    iget-object v5, p0, Lco/uk/getmondo/monzo/me/customise/o;->g:Ljavax/a/a;

    .line 75
    invoke-interface {v5}, Ljavax/a/a;->b()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lco/uk/getmondo/monzo/me/a;

    iget-object v6, p0, Lco/uk/getmondo/monzo/me/customise/o;->h:Ljavax/a/a;

    .line 76
    invoke-interface {v6}, Ljavax/a/a;->b()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lco/uk/getmondo/api/model/tracking/Impression$CustomiseMonzoMeLinkFrom;

    iget-object v7, p0, Lco/uk/getmondo/monzo/me/customise/o;->i:Ljavax/a/a;

    .line 77
    invoke-interface {v7}, Ljavax/a/a;->b()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lco/uk/getmondo/payments/send/data/p;

    invoke-direct/range {v0 .. v7}, Lco/uk/getmondo/monzo/me/customise/i;-><init>(Lco/uk/getmondo/common/a;Lco/uk/getmondo/payments/send/data/h;Lco/uk/getmondo/d/c;Ljava/lang/String;Lco/uk/getmondo/monzo/me/a;Lco/uk/getmondo/api/model/tracking/Impression$CustomiseMonzoMeLinkFrom;Lco/uk/getmondo/payments/send/data/p;)V

    .line 68
    invoke-static {v8, v0}, Lb/a/c;->a(Lb/a;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/monzo/me/customise/i;

    return-object v0
.end method

.method public synthetic b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 15
    invoke-virtual {p0}, Lco/uk/getmondo/monzo/me/customise/o;->a()Lco/uk/getmondo/monzo/me/customise/i;

    move-result-object v0

    return-object v0
.end method
