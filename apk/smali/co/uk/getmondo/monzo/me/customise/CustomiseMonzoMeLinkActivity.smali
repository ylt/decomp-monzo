.class public Lco/uk/getmondo/monzo/me/customise/CustomiseMonzoMeLinkActivity;
.super Lco/uk/getmondo/common/activities/b;
.source "CustomiseMonzoMeLinkActivity.java"

# interfaces
.implements Lco/uk/getmondo/monzo/me/customise/i$a;


# instance fields
.field a:Lco/uk/getmondo/monzo/me/customise/i;

.field amountInputView:Lco/uk/getmondo/common/ui/AmountInputView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f11014e
    .end annotation
.end field

.field notesEditText:Landroid/widget/EditText;
    .annotation build Lbutterknife/BindView;
        value = 0x7f11014f
    .end annotation
.end field

.field shareButton:Landroid/widget/Button;
    .annotation build Lbutterknife/BindView;
        value = 0x7f110150
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Lco/uk/getmondo/common/activities/b;-><init>()V

    return-void
.end method

.method public static a(Landroid/content/Context;Lco/uk/getmondo/d/c;Ljava/lang/String;Lco/uk/getmondo/api/model/tracking/Impression$CustomiseMonzoMeLinkFrom;)V
    .locals 1

    .prologue
    .line 39
    invoke-static {p0, p1, p2, p3}, Lco/uk/getmondo/monzo/me/customise/CustomiseMonzoMeLinkActivity;->b(Landroid/content/Context;Lco/uk/getmondo/d/c;Ljava/lang/String;Lco/uk/getmondo/api/model/tracking/Impression$CustomiseMonzoMeLinkFrom;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 40
    return-void
.end method

.method public static b(Landroid/content/Context;Lco/uk/getmondo/d/c;Ljava/lang/String;Lco/uk/getmondo/api/model/tracking/Impression$CustomiseMonzoMeLinkFrom;)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 43
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lco/uk/getmondo/monzo/me/customise/CustomiseMonzoMeLinkActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 44
    const-string v1, "key_notes"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 45
    const-string v1, "key_amount"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 46
    const-string v1, "key_entry_point"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 47
    return-object v0
.end method


# virtual methods
.method public a()Lio/reactivex/n;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 73
    iget-object v0, p0, Lco/uk/getmondo/monzo/me/customise/CustomiseMonzoMeLinkActivity;->amountInputView:Lco/uk/getmondo/common/ui/AmountInputView;

    invoke-virtual {v0}, Lco/uk/getmondo/common/ui/AmountInputView;->a()Lio/reactivex/n;

    move-result-object v0

    invoke-static {}, Lco/uk/getmondo/monzo/me/customise/a;->a()Lio/reactivex/c/h;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/n;->map(Lio/reactivex/c/h;)Lio/reactivex/n;

    move-result-object v0

    return-object v0
.end method

.method public a(II)V
    .locals 5

    .prologue
    .line 107
    iget-object v0, p0, Lco/uk/getmondo/monzo/me/customise/CustomiseMonzoMeLinkActivity;->amountInputView:Lco/uk/getmondo/common/ui/AmountInputView;

    const v1, 0x7f0a02a6

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {p0, v1, v2}, Lco/uk/getmondo/monzo/me/customise/CustomiseMonzoMeLinkActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lco/uk/getmondo/common/ui/AmountInputView;->setError(Ljava/lang/CharSequence;)V

    .line 108
    return-void
.end method

.method public a(Lco/uk/getmondo/d/c;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 93
    if-eqz p1, :cond_0

    .line 94
    iget-object v0, p0, Lco/uk/getmondo/monzo/me/customise/CustomiseMonzoMeLinkActivity;->amountInputView:Lco/uk/getmondo/common/ui/AmountInputView;

    invoke-virtual {v0, p1}, Lco/uk/getmondo/common/ui/AmountInputView;->setDefaultAmount(Lco/uk/getmondo/d/c;)V

    .line 96
    :cond_0
    iget-object v0, p0, Lco/uk/getmondo/monzo/me/customise/CustomiseMonzoMeLinkActivity;->notesEditText:Landroid/widget/EditText;

    invoke-virtual {v0, p2}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 97
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 101
    const v0, 0x7f0a0382

    invoke-virtual {p0, v0}, Lco/uk/getmondo/monzo/me/customise/CustomiseMonzoMeLinkActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 102
    sget-object v1, Lco/uk/getmondo/api/model/tracking/a;->REQUEST_MONEY_CUSTOMISE:Lco/uk/getmondo/api/model/tracking/a;

    invoke-static {p0, v0, p1, v1}, Lco/uk/getmondo/common/k/j;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lco/uk/getmondo/api/model/tracking/a;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lco/uk/getmondo/monzo/me/customise/CustomiseMonzoMeLinkActivity;->startActivity(Landroid/content/Intent;)V

    .line 103
    return-void
.end method

.method public a(Z)V
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lco/uk/getmondo/monzo/me/customise/CustomiseMonzoMeLinkActivity;->shareButton:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 89
    return-void
.end method

.method public b()Lio/reactivex/n;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 78
    iget-object v0, p0, Lco/uk/getmondo/monzo/me/customise/CustomiseMonzoMeLinkActivity;->notesEditText:Landroid/widget/EditText;

    invoke-static {v0}, Lcom/b/a/d/e;->c(Landroid/widget/TextView;)Lcom/b/a/a;

    move-result-object v0

    invoke-static {}, Lco/uk/getmondo/monzo/me/customise/b;->a()Lio/reactivex/c/h;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/b/a/a;->map(Lio/reactivex/c/h;)Lio/reactivex/n;

    move-result-object v0

    return-object v0
.end method

.method public c()Lio/reactivex/n;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 83
    iget-object v0, p0, Lco/uk/getmondo/monzo/me/customise/CustomiseMonzoMeLinkActivity;->shareButton:Landroid/widget/Button;

    invoke-static {v0}, Lcom/b/a/c/c;->a(Landroid/view/View;)Lio/reactivex/n;

    move-result-object v0

    return-object v0
.end method

.method public d()V
    .locals 2

    .prologue
    .line 112
    iget-object v0, p0, Lco/uk/getmondo/monzo/me/customise/CustomiseMonzoMeLinkActivity;->amountInputView:Lco/uk/getmondo/common/ui/AmountInputView;

    const v1, 0x7f0a02a7

    invoke-virtual {p0, v1}, Lco/uk/getmondo/monzo/me/customise/CustomiseMonzoMeLinkActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lco/uk/getmondo/common/ui/AmountInputView;->setError(Ljava/lang/CharSequence;)V

    .line 113
    return-void
.end method

.method public e()V
    .locals 2

    .prologue
    .line 117
    iget-object v0, p0, Lco/uk/getmondo/monzo/me/customise/CustomiseMonzoMeLinkActivity;->amountInputView:Lco/uk/getmondo/common/ui/AmountInputView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Lco/uk/getmondo/common/ui/AmountInputView;->setError(Ljava/lang/CharSequence;)V

    .line 118
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    .line 52
    invoke-super {p0, p1}, Lco/uk/getmondo/common/activities/b;->onCreate(Landroid/os/Bundle;)V

    .line 54
    const v0, 0x7f050030

    invoke-virtual {p0, v0}, Lco/uk/getmondo/monzo/me/customise/CustomiseMonzoMeLinkActivity;->setContentView(I)V

    .line 55
    invoke-static {p0}, Lbutterknife/ButterKnife;->bind(Landroid/app/Activity;)Lbutterknife/Unbinder;

    .line 57
    invoke-virtual {p0}, Lco/uk/getmondo/monzo/me/customise/CustomiseMonzoMeLinkActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "key_amount"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/d/c;

    .line 58
    invoke-virtual {p0}, Lco/uk/getmondo/monzo/me/customise/CustomiseMonzoMeLinkActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "key_notes"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 59
    invoke-virtual {p0}, Lco/uk/getmondo/monzo/me/customise/CustomiseMonzoMeLinkActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v3, "key_entry_point"

    invoke-virtual {v1, v3}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v1

    check-cast v1, Lco/uk/getmondo/api/model/tracking/Impression$CustomiseMonzoMeLinkFrom;

    .line 60
    invoke-virtual {p0}, Lco/uk/getmondo/monzo/me/customise/CustomiseMonzoMeLinkActivity;->l()Lco/uk/getmondo/common/h/b/b;

    move-result-object v3

    new-instance v4, Lco/uk/getmondo/monzo/me/customise/e;

    invoke-direct {v4, v0, v2, v1}, Lco/uk/getmondo/monzo/me/customise/e;-><init>(Lco/uk/getmondo/d/c;Ljava/lang/String;Lco/uk/getmondo/api/model/tracking/Impression$CustomiseMonzoMeLinkFrom;)V

    invoke-interface {v3, v4}, Lco/uk/getmondo/common/h/b/b;->a(Lco/uk/getmondo/monzo/me/customise/e;)Lco/uk/getmondo/monzo/me/customise/d;

    move-result-object v0

    invoke-interface {v0, p0}, Lco/uk/getmondo/monzo/me/customise/d;->a(Lco/uk/getmondo/monzo/me/customise/CustomiseMonzoMeLinkActivity;)V

    .line 61
    iget-object v0, p0, Lco/uk/getmondo/monzo/me/customise/CustomiseMonzoMeLinkActivity;->a:Lco/uk/getmondo/monzo/me/customise/i;

    invoke-virtual {v0, p0}, Lco/uk/getmondo/monzo/me/customise/i;->a(Lco/uk/getmondo/monzo/me/customise/i$a;)V

    .line 62
    return-void
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lco/uk/getmondo/monzo/me/customise/CustomiseMonzoMeLinkActivity;->a:Lco/uk/getmondo/monzo/me/customise/i;

    invoke-virtual {v0}, Lco/uk/getmondo/monzo/me/customise/i;->b()V

    .line 68
    invoke-super {p0}, Lco/uk/getmondo/common/activities/b;->onDestroy()V

    .line 69
    return-void
.end method
