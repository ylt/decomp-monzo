.class public Lco/uk/getmondo/monzo/me/request/RequestMoneyFragment;
.super Lco/uk/getmondo/common/f/a;
.source "RequestMoneyFragment.java"

# interfaces
.implements Lco/uk/getmondo/monzo/me/request/b$a;


# instance fields
.field a:Lco/uk/getmondo/monzo/me/request/b;

.field private final c:Lio/reactivex/i/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/i/a",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field customiseAmountButton:Landroid/widget/Button;
    .annotation build Lbutterknife/BindView;
        value = 0x7f110330
    .end annotation
.end field

.field private final d:Lio/reactivex/i/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/i/a",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private e:Lco/uk/getmondo/common/ui/a;

.field private f:Lbutterknife/Unbinder;

.field iconImageView:Landroid/widget/ImageView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f11032c
    .end annotation
.end field

.field linkTextView:Landroid/widget/TextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f11032e
    .end annotation
.end field

.field nameTextView:Landroid/widget/TextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f11032d
    .end annotation
.end field

.field shareButton:Landroid/widget/Button;
    .annotation build Lbutterknife/BindView;
        value = 0x7f11032f
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 36
    invoke-direct {p0}, Lco/uk/getmondo/common/f/a;-><init>()V

    .line 37
    invoke-static {}, Lio/reactivex/i/a;->a()Lio/reactivex/i/a;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/monzo/me/request/RequestMoneyFragment;->c:Lio/reactivex/i/a;

    .line 38
    invoke-static {}, Lio/reactivex/i/a;->a()Lio/reactivex/i/a;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/monzo/me/request/RequestMoneyFragment;->d:Lio/reactivex/i/a;

    return-void
.end method

.method public static a()Landroid/support/v4/app/Fragment;
    .locals 1

    .prologue
    .line 51
    new-instance v0, Lco/uk/getmondo/monzo/me/request/RequestMoneyFragment;

    invoke-direct {v0}, Lco/uk/getmondo/monzo/me/request/RequestMoneyFragment;-><init>()V

    return-object v0
.end method


# virtual methods
.method public a(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 133
    const v0, 0x7f0a0382

    invoke-virtual {p0, v0}, Lco/uk/getmondo/monzo/me/request/RequestMoneyFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 134
    invoke-virtual {p0}, Lco/uk/getmondo/monzo/me/request/RequestMoneyFragment;->getActivity()Landroid/support/v4/app/j;

    move-result-object v1

    sget-object v2, Lco/uk/getmondo/api/model/tracking/a;->REQUEST_MONEY:Lco/uk/getmondo/api/model/tracking/a;

    invoke-static {v1, v0, p1, v2}, Lco/uk/getmondo/common/k/j;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lco/uk/getmondo/api/model/tracking/a;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lco/uk/getmondo/monzo/me/request/RequestMoneyFragment;->startActivity(Landroid/content/Intent;)V

    .line 135
    return-void
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 125
    iget-object v0, p0, Lco/uk/getmondo/monzo/me/request/RequestMoneyFragment;->nameTextView:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 126
    iget-object v0, p0, Lco/uk/getmondo/monzo/me/request/RequestMoneyFragment;->linkTextView:Landroid/widget/TextView;

    invoke-virtual {v0, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 127
    const/4 v0, 0x2

    const/high16 v1, 0x41e00000    # 28.0f

    invoke-virtual {p0}, Lco/uk/getmondo/monzo/me/request/RequestMoneyFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    invoke-static {v0, v1, v2}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v0

    float-to-int v0, v0

    .line 128
    iget-object v1, p0, Lco/uk/getmondo/monzo/me/request/RequestMoneyFragment;->iconImageView:Landroid/widget/ImageView;

    iget-object v2, p0, Lco/uk/getmondo/monzo/me/request/RequestMoneyFragment;->e:Lco/uk/getmondo/common/ui/a;

    invoke-virtual {v2, p1}, Lco/uk/getmondo/common/ui/a;->a(Ljava/lang/String;)Lco/uk/getmondo/common/ui/a$b;

    move-result-object v2

    invoke-virtual {v2, v0}, Lco/uk/getmondo/common/ui/a$b;->a(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 129
    return-void
.end method

.method public b()Lio/reactivex/n;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 105
    iget-object v0, p0, Lco/uk/getmondo/monzo/me/request/RequestMoneyFragment;->d:Lio/reactivex/i/a;

    return-object v0
.end method

.method public c()Lio/reactivex/n;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 110
    iget-object v0, p0, Lco/uk/getmondo/monzo/me/request/RequestMoneyFragment;->shareButton:Landroid/widget/Button;

    invoke-static {v0}, Lcom/b/a/c/c;->a(Landroid/view/View;)Lio/reactivex/n;

    move-result-object v0

    return-object v0
.end method

.method public d()Lio/reactivex/n;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 115
    iget-object v0, p0, Lco/uk/getmondo/monzo/me/request/RequestMoneyFragment;->customiseAmountButton:Landroid/widget/Button;

    invoke-static {v0}, Lcom/b/a/c/c;->a(Landroid/view/View;)Lio/reactivex/n;

    move-result-object v0

    return-object v0
.end method

.method public e()Lio/reactivex/n;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .prologue
    .line 120
    iget-object v0, p0, Lco/uk/getmondo/monzo/me/request/RequestMoneyFragment;->c:Lio/reactivex/i/a;

    return-object v0
.end method

.method public f()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 139
    invoke-virtual {p0}, Lco/uk/getmondo/monzo/me/request/RequestMoneyFragment;->getActivity()Landroid/support/v4/app/j;

    move-result-object v0

    sget-object v1, Lco/uk/getmondo/api/model/tracking/Impression$CustomiseMonzoMeLinkFrom;->REQUEST_MONEY:Lco/uk/getmondo/api/model/tracking/Impression$CustomiseMonzoMeLinkFrom;

    invoke-static {v0, v2, v2, v1}, Lco/uk/getmondo/monzo/me/customise/CustomiseMonzoMeLinkActivity;->a(Landroid/content/Context;Lco/uk/getmondo/d/c;Ljava/lang/String;Lco/uk/getmondo/api/model/tracking/Impression$CustomiseMonzoMeLinkFrom;)V

    .line 140
    return-void
.end method

.method public g()V
    .locals 1

    .prologue
    .line 144
    invoke-virtual {p0}, Lco/uk/getmondo/monzo/me/request/RequestMoneyFragment;->getActivity()Landroid/support/v4/app/j;

    move-result-object v0

    invoke-static {v0}, Lco/uk/getmondo/monzo/me/onboarding/MonzoMeOnboardingActivity;->a(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lco/uk/getmondo/monzo/me/request/RequestMoneyFragment;->startActivity(Landroid/content/Intent;)V

    .line 145
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 56
    invoke-super {p0, p1}, Lco/uk/getmondo/common/f/a;->onCreate(Landroid/os/Bundle;)V

    .line 57
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lco/uk/getmondo/monzo/me/request/RequestMoneyFragment;->setHasOptionsMenu(Z)V

    .line 58
    invoke-virtual {p0}, Lco/uk/getmondo/monzo/me/request/RequestMoneyFragment;->getActivity()Landroid/support/v4/app/j;

    move-result-object v0

    invoke-static {v0}, Lco/uk/getmondo/common/ui/a;->a(Landroid/content/Context;)Lco/uk/getmondo/common/ui/a;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/monzo/me/request/RequestMoneyFragment;->e:Lco/uk/getmondo/common/ui/a;

    .line 59
    invoke-virtual {p0}, Lco/uk/getmondo/monzo/me/request/RequestMoneyFragment;->B()Lco/uk/getmondo/common/h/b/b;

    move-result-object v0

    invoke-interface {v0, p0}, Lco/uk/getmondo/common/h/b/b;->a(Lco/uk/getmondo/monzo/me/request/RequestMoneyFragment;)V

    .line 60
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V
    .locals 1

    .prologue
    .line 83
    const v0, 0x7f130005

    invoke-virtual {p2, v0, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 84
    invoke-super {p0, p1, p2}, Lco/uk/getmondo/common/f/a;->onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V

    .line 85
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2

    .prologue
    .line 64
    const v0, 0x7f0500a5

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public onDestroyView()V
    .locals 1

    .prologue
    .line 76
    iget-object v0, p0, Lco/uk/getmondo/monzo/me/request/RequestMoneyFragment;->a:Lco/uk/getmondo/monzo/me/request/b;

    invoke-virtual {v0}, Lco/uk/getmondo/monzo/me/request/b;->b()V

    .line 77
    iget-object v0, p0, Lco/uk/getmondo/monzo/me/request/RequestMoneyFragment;->f:Lbutterknife/Unbinder;

    invoke-interface {v0}, Lbutterknife/Unbinder;->unbind()V

    .line 78
    invoke-super {p0}, Lco/uk/getmondo/common/f/a;->onDestroyView()V

    .line 79
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2

    .prologue
    .line 89
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x7f1104d3

    if-ne v0, v1, :cond_0

    .line 90
    iget-object v0, p0, Lco/uk/getmondo/monzo/me/request/RequestMoneyFragment;->d:Lio/reactivex/i/a;

    sget-object v1, Lco/uk/getmondo/common/b/a;->a:Lco/uk/getmondo/common/b/a;

    invoke-virtual {v0, v1}, Lio/reactivex/i/a;->onNext(Ljava/lang/Object;)V

    .line 91
    const/4 v0, 0x1

    .line 94
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Lco/uk/getmondo/common/f/a;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    goto :goto_0
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 69
    invoke-super {p0, p1, p2}, Lco/uk/getmondo/common/f/a;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 70
    invoke-static {p0, p1}, Lbutterknife/ButterKnife;->bind(Ljava/lang/Object;Landroid/view/View;)Lbutterknife/Unbinder;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/monzo/me/request/RequestMoneyFragment;->f:Lbutterknife/Unbinder;

    .line 71
    iget-object v0, p0, Lco/uk/getmondo/monzo/me/request/RequestMoneyFragment;->a:Lco/uk/getmondo/monzo/me/request/b;

    invoke-virtual {v0, p0}, Lco/uk/getmondo/monzo/me/request/b;->a(Lco/uk/getmondo/monzo/me/request/b$a;)V

    .line 72
    return-void
.end method

.method public setUserVisibleHint(Z)V
    .locals 2

    .prologue
    .line 99
    invoke-super {p0, p1}, Lco/uk/getmondo/common/f/a;->setUserVisibleHint(Z)V

    .line 100
    iget-object v0, p0, Lco/uk/getmondo/monzo/me/request/RequestMoneyFragment;->c:Lio/reactivex/i/a;

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/i/a;->onNext(Ljava/lang/Object;)V

    .line 101
    return-void
.end method
