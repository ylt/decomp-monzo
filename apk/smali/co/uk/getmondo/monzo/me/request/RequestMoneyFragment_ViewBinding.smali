.class public Lco/uk/getmondo/monzo/me/request/RequestMoneyFragment_ViewBinding;
.super Ljava/lang/Object;
.source "RequestMoneyFragment_ViewBinding.java"

# interfaces
.implements Lbutterknife/Unbinder;


# instance fields
.field private a:Lco/uk/getmondo/monzo/me/request/RequestMoneyFragment;


# direct methods
.method public constructor <init>(Lco/uk/getmondo/monzo/me/request/RequestMoneyFragment;Landroid/view/View;)V
    .locals 3

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    iput-object p1, p0, Lco/uk/getmondo/monzo/me/request/RequestMoneyFragment_ViewBinding;->a:Lco/uk/getmondo/monzo/me/request/RequestMoneyFragment;

    .line 23
    const v0, 0x7f11032c

    const-string v1, "field \'iconImageView\'"

    const-class v2, Landroid/widget/ImageView;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p1, Lco/uk/getmondo/monzo/me/request/RequestMoneyFragment;->iconImageView:Landroid/widget/ImageView;

    .line 24
    const v0, 0x7f11032d

    const-string v1, "field \'nameTextView\'"

    const-class v2, Landroid/widget/TextView;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p1, Lco/uk/getmondo/monzo/me/request/RequestMoneyFragment;->nameTextView:Landroid/widget/TextView;

    .line 25
    const v0, 0x7f11032e

    const-string v1, "field \'linkTextView\'"

    const-class v2, Landroid/widget/TextView;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p1, Lco/uk/getmondo/monzo/me/request/RequestMoneyFragment;->linkTextView:Landroid/widget/TextView;

    .line 26
    const v0, 0x7f11032f

    const-string v1, "field \'shareButton\'"

    const-class v2, Landroid/widget/Button;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p1, Lco/uk/getmondo/monzo/me/request/RequestMoneyFragment;->shareButton:Landroid/widget/Button;

    .line 27
    const v0, 0x7f110330

    const-string v1, "field \'customiseAmountButton\'"

    const-class v2, Landroid/widget/Button;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p1, Lco/uk/getmondo/monzo/me/request/RequestMoneyFragment;->customiseAmountButton:Landroid/widget/Button;

    .line 28
    return-void
.end method


# virtual methods
.method public unbind()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 33
    iget-object v0, p0, Lco/uk/getmondo/monzo/me/request/RequestMoneyFragment_ViewBinding;->a:Lco/uk/getmondo/monzo/me/request/RequestMoneyFragment;

    .line 34
    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Bindings already cleared."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 35
    :cond_0
    iput-object v1, p0, Lco/uk/getmondo/monzo/me/request/RequestMoneyFragment_ViewBinding;->a:Lco/uk/getmondo/monzo/me/request/RequestMoneyFragment;

    .line 37
    iput-object v1, v0, Lco/uk/getmondo/monzo/me/request/RequestMoneyFragment;->iconImageView:Landroid/widget/ImageView;

    .line 38
    iput-object v1, v0, Lco/uk/getmondo/monzo/me/request/RequestMoneyFragment;->nameTextView:Landroid/widget/TextView;

    .line 39
    iput-object v1, v0, Lco/uk/getmondo/monzo/me/request/RequestMoneyFragment;->linkTextView:Landroid/widget/TextView;

    .line 40
    iput-object v1, v0, Lco/uk/getmondo/monzo/me/request/RequestMoneyFragment;->shareButton:Landroid/widget/Button;

    .line 41
    iput-object v1, v0, Lco/uk/getmondo/monzo/me/request/RequestMoneyFragment;->customiseAmountButton:Landroid/widget/Button;

    .line 42
    return-void
.end method
