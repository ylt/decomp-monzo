.class Lco/uk/getmondo/monzo/me/request/b;
.super Lco/uk/getmondo/common/ui/b;
.source "RequestMoneyPresenter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lco/uk/getmondo/monzo/me/request/b$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lco/uk/getmondo/common/ui/b",
        "<",
        "Lco/uk/getmondo/monzo/me/request/b$a;",
        ">;"
    }
.end annotation


# instance fields
.field private final c:Lco/uk/getmondo/common/a;

.field private final d:Lco/uk/getmondo/payments/send/data/h;

.field private final e:Lco/uk/getmondo/common/accounts/d;

.field private final f:Lco/uk/getmondo/monzo/me/a;

.field private final g:Lco/uk/getmondo/settings/k;


# direct methods
.method constructor <init>(Lco/uk/getmondo/common/a;Lco/uk/getmondo/payments/send/data/h;Lco/uk/getmondo/common/accounts/d;Lco/uk/getmondo/monzo/me/a;Lco/uk/getmondo/settings/k;)V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Lco/uk/getmondo/common/ui/b;-><init>()V

    .line 28
    iput-object p1, p0, Lco/uk/getmondo/monzo/me/request/b;->c:Lco/uk/getmondo/common/a;

    .line 29
    iput-object p2, p0, Lco/uk/getmondo/monzo/me/request/b;->d:Lco/uk/getmondo/payments/send/data/h;

    .line 30
    iput-object p3, p0, Lco/uk/getmondo/monzo/me/request/b;->e:Lco/uk/getmondo/common/accounts/d;

    .line 31
    iput-object p4, p0, Lco/uk/getmondo/monzo/me/request/b;->f:Lco/uk/getmondo/monzo/me/a;

    .line 32
    iput-object p5, p0, Lco/uk/getmondo/monzo/me/request/b;->g:Lco/uk/getmondo/settings/k;

    .line 33
    return-void
.end method

.method static synthetic a(Lco/uk/getmondo/monzo/me/request/b$a;Ljava/lang/Boolean;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 61
    invoke-interface {p0}, Lco/uk/getmondo/monzo/me/request/b$a;->g()V

    return-void
.end method

.method static synthetic a(Lco/uk/getmondo/monzo/me/request/b$a;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 57
    invoke-interface {p0}, Lco/uk/getmondo/monzo/me/request/b$a;->g()V

    return-void
.end method

.method static synthetic a(Lco/uk/getmondo/monzo/me/request/b;Lco/uk/getmondo/monzo/me/request/b$a;Ljava/lang/String;Ljava/lang/Object;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 49
    iget-object v0, p0, Lco/uk/getmondo/monzo/me/request/b;->c:Lco/uk/getmondo/common/a;

    invoke-static {}, Lco/uk/getmondo/api/model/tracking/Impression;->U()Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v1

    invoke-virtual {v0, v1}, Lco/uk/getmondo/common/a;->a(Lco/uk/getmondo/api/model/tracking/Impression;)V

    .line 50
    invoke-interface {p1, p2}, Lco/uk/getmondo/monzo/me/request/b$a;->a(Ljava/lang/String;)V

    .line 51
    return-void
.end method

.method static synthetic a(Lco/uk/getmondo/monzo/me/request/b;Ljava/lang/Boolean;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 60
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lco/uk/getmondo/monzo/me/request/b;->g:Lco/uk/getmondo/settings/k;

    invoke-virtual {v0}, Lco/uk/getmondo/settings/k;->c()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic b(Lco/uk/getmondo/monzo/me/request/b$a;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 54
    invoke-interface {p0}, Lco/uk/getmondo/monzo/me/request/b$a;->f()V

    return-void
.end method


# virtual methods
.method public bridge synthetic a(Lco/uk/getmondo/common/ui/f;)V
    .locals 0

    .prologue
    .line 17
    check-cast p1, Lco/uk/getmondo/monzo/me/request/b$a;

    invoke-virtual {p0, p1}, Lco/uk/getmondo/monzo/me/request/b;->a(Lco/uk/getmondo/monzo/me/request/b$a;)V

    return-void
.end method

.method public a(Lco/uk/getmondo/monzo/me/request/b$a;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 37
    invoke-super {p0, p1}, Lco/uk/getmondo/common/ui/b;->a(Lco/uk/getmondo/common/ui/f;)V

    .line 39
    iget-object v0, p0, Lco/uk/getmondo/monzo/me/request/b;->c:Lco/uk/getmondo/common/a;

    invoke-static {}, Lco/uk/getmondo/api/model/tracking/Impression;->T()Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v1

    invoke-virtual {v0, v1}, Lco/uk/getmondo/common/a;->a(Lco/uk/getmondo/api/model/tracking/Impression;)V

    .line 41
    iget-object v0, p0, Lco/uk/getmondo/monzo/me/request/b;->d:Lco/uk/getmondo/payments/send/data/h;

    invoke-virtual {v0}, Lco/uk/getmondo/payments/send/data/h;->c()Ljava/lang/String;

    move-result-object v0

    .line 42
    const-string v1, "%s/%s"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "monzo.me"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    aput-object v0, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 43
    iget-object v2, p0, Lco/uk/getmondo/monzo/me/request/b;->f:Lco/uk/getmondo/monzo/me/a;

    invoke-virtual {v2, v0, v5, v5}, Lco/uk/getmondo/monzo/me/a;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 45
    iget-object v2, p0, Lco/uk/getmondo/monzo/me/request/b;->e:Lco/uk/getmondo/common/accounts/d;

    invoke-virtual {v2}, Lco/uk/getmondo/common/accounts/d;->b()Lco/uk/getmondo/d/ak;

    move-result-object v2

    invoke-virtual {v2}, Lco/uk/getmondo/d/ak;->d()Lco/uk/getmondo/d/ac;

    move-result-object v2

    invoke-virtual {v2}, Lco/uk/getmondo/d/ac;->a()Ljava/lang/String;

    move-result-object v2

    invoke-interface {p1, v2, v1}, Lco/uk/getmondo/monzo/me/request/b$a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 47
    invoke-interface {p1}, Lco/uk/getmondo/monzo/me/request/b$a;->c()Lio/reactivex/n;

    move-result-object v1

    invoke-static {p0, p1, v0}, Lco/uk/getmondo/monzo/me/request/c;->a(Lco/uk/getmondo/monzo/me/request/b;Lco/uk/getmondo/monzo/me/request/b$a;Ljava/lang/String;)Lio/reactivex/c/g;

    move-result-object v0

    .line 48
    invoke-virtual {v1, v0}, Lio/reactivex/n;->subscribe(Lio/reactivex/c/g;)Lio/reactivex/b/b;

    move-result-object v0

    .line 47
    invoke-virtual {p0, v0}, Lco/uk/getmondo/monzo/me/request/b;->a(Lio/reactivex/b/b;)V

    .line 53
    invoke-interface {p1}, Lco/uk/getmondo/monzo/me/request/b$a;->d()Lio/reactivex/n;

    move-result-object v0

    invoke-static {p1}, Lco/uk/getmondo/monzo/me/request/d;->a(Lco/uk/getmondo/monzo/me/request/b$a;)Lio/reactivex/c/g;

    move-result-object v1

    .line 54
    invoke-virtual {v0, v1}, Lio/reactivex/n;->subscribe(Lio/reactivex/c/g;)Lio/reactivex/b/b;

    move-result-object v0

    .line 53
    invoke-virtual {p0, v0}, Lco/uk/getmondo/monzo/me/request/b;->a(Lio/reactivex/b/b;)V

    .line 56
    invoke-interface {p1}, Lco/uk/getmondo/monzo/me/request/b$a;->b()Lio/reactivex/n;

    move-result-object v0

    invoke-static {p1}, Lco/uk/getmondo/monzo/me/request/e;->a(Lco/uk/getmondo/monzo/me/request/b$a;)Lio/reactivex/c/g;

    move-result-object v1

    .line 57
    invoke-virtual {v0, v1}, Lio/reactivex/n;->subscribe(Lio/reactivex/c/g;)Lio/reactivex/b/b;

    move-result-object v0

    .line 56
    invoke-virtual {p0, v0}, Lco/uk/getmondo/monzo/me/request/b;->a(Lio/reactivex/b/b;)V

    .line 59
    invoke-interface {p1}, Lco/uk/getmondo/monzo/me/request/b$a;->e()Lio/reactivex/n;

    move-result-object v0

    invoke-static {p0}, Lco/uk/getmondo/monzo/me/request/f;->a(Lco/uk/getmondo/monzo/me/request/b;)Lio/reactivex/c/q;

    move-result-object v1

    .line 60
    invoke-virtual {v0, v1}, Lio/reactivex/n;->filter(Lio/reactivex/c/q;)Lio/reactivex/n;

    move-result-object v0

    invoke-static {p1}, Lco/uk/getmondo/monzo/me/request/g;->a(Lco/uk/getmondo/monzo/me/request/b$a;)Lio/reactivex/c/g;

    move-result-object v1

    .line 61
    invoke-virtual {v0, v1}, Lio/reactivex/n;->subscribe(Lio/reactivex/c/g;)Lio/reactivex/b/b;

    move-result-object v0

    .line 59
    invoke-virtual {p0, v0}, Lco/uk/getmondo/monzo/me/request/b;->a(Lio/reactivex/b/b;)V

    .line 62
    return-void
.end method
