.class Lco/uk/getmondo/monzo/me/onboarding/e;
.super Lco/uk/getmondo/common/ui/b;
.source "MonzoMeOnboardingPresenter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lco/uk/getmondo/monzo/me/onboarding/e$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lco/uk/getmondo/common/ui/b",
        "<",
        "Lco/uk/getmondo/monzo/me/onboarding/e$a;",
        ">;"
    }
.end annotation


# instance fields
.field private final c:Lco/uk/getmondo/settings/k;


# direct methods
.method constructor <init>(Lco/uk/getmondo/settings/k;)V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Lco/uk/getmondo/common/ui/b;-><init>()V

    .line 18
    iput-object p1, p0, Lco/uk/getmondo/monzo/me/onboarding/e;->c:Lco/uk/getmondo/settings/k;

    .line 19
    return-void
.end method

.method static synthetic a(Lco/uk/getmondo/monzo/me/onboarding/e$a;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 31
    invoke-interface {p0}, Lco/uk/getmondo/monzo/me/onboarding/e$a;->a()V

    return-void
.end method

.method static synthetic b(Lco/uk/getmondo/monzo/me/onboarding/e$a;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 28
    invoke-interface {p0}, Lco/uk/getmondo/monzo/me/onboarding/e$a;->b()V

    return-void
.end method


# virtual methods
.method public bridge synthetic a(Lco/uk/getmondo/common/ui/f;)V
    .locals 0

    .prologue
    .line 12
    check-cast p1, Lco/uk/getmondo/monzo/me/onboarding/e$a;

    invoke-virtual {p0, p1}, Lco/uk/getmondo/monzo/me/onboarding/e;->a(Lco/uk/getmondo/monzo/me/onboarding/e$a;)V

    return-void
.end method

.method public a(Lco/uk/getmondo/monzo/me/onboarding/e$a;)V
    .locals 2

    .prologue
    .line 23
    invoke-super {p0, p1}, Lco/uk/getmondo/common/ui/b;->a(Lco/uk/getmondo/common/ui/f;)V

    .line 25
    iget-object v0, p0, Lco/uk/getmondo/monzo/me/onboarding/e;->c:Lco/uk/getmondo/settings/k;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lco/uk/getmondo/settings/k;->b(Z)V

    .line 27
    invoke-interface {p1}, Lco/uk/getmondo/monzo/me/onboarding/e$a;->d()Lio/reactivex/n;

    move-result-object v0

    invoke-static {p1}, Lco/uk/getmondo/monzo/me/onboarding/f;->a(Lco/uk/getmondo/monzo/me/onboarding/e$a;)Lio/reactivex/c/g;

    move-result-object v1

    .line 28
    invoke-virtual {v0, v1}, Lio/reactivex/n;->subscribe(Lio/reactivex/c/g;)Lio/reactivex/b/b;

    move-result-object v0

    .line 27
    invoke-virtual {p0, v0}, Lco/uk/getmondo/monzo/me/onboarding/e;->a(Lio/reactivex/b/b;)V

    .line 30
    invoke-interface {p1}, Lco/uk/getmondo/monzo/me/onboarding/e$a;->c()Lio/reactivex/n;

    move-result-object v0

    invoke-static {p1}, Lco/uk/getmondo/monzo/me/onboarding/g;->a(Lco/uk/getmondo/monzo/me/onboarding/e$a;)Lio/reactivex/c/g;

    move-result-object v1

    .line 31
    invoke-virtual {v0, v1}, Lio/reactivex/n;->subscribe(Lio/reactivex/c/g;)Lio/reactivex/b/b;

    move-result-object v0

    .line 30
    invoke-virtual {p0, v0}, Lco/uk/getmondo/monzo/me/onboarding/e;->a(Lio/reactivex/b/b;)V

    .line 33
    invoke-interface {p1}, Lco/uk/getmondo/monzo/me/onboarding/e$a;->e()Lio/reactivex/n;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    invoke-static {p1}, Lco/uk/getmondo/monzo/me/onboarding/h;->a(Lco/uk/getmondo/monzo/me/onboarding/e$a;)Lio/reactivex/c/g;

    move-result-object v1

    .line 34
    invoke-virtual {v0, v1}, Lio/reactivex/n;->subscribe(Lio/reactivex/c/g;)Lio/reactivex/b/b;

    move-result-object v0

    .line 33
    invoke-virtual {p0, v0}, Lco/uk/getmondo/monzo/me/onboarding/e;->a(Lio/reactivex/b/b;)V

    .line 35
    return-void
.end method
