.class public final enum Lco/uk/getmondo/monzo/me/onboarding/e$a$a;
.super Ljava/lang/Enum;
.source "MonzoMeOnboardingPresenter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lco/uk/getmondo/monzo/me/onboarding/e$a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lco/uk/getmondo/monzo/me/onboarding/e$a$a;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lco/uk/getmondo/monzo/me/onboarding/e$a$a;

.field public static final enum b:Lco/uk/getmondo/monzo/me/onboarding/e$a$a;

.field private static final synthetic c:[Lco/uk/getmondo/monzo/me/onboarding/e$a$a;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 39
    new-instance v0, Lco/uk/getmondo/monzo/me/onboarding/e$a$a;

    const-string v1, "NEXT"

    invoke-direct {v0, v1, v2}, Lco/uk/getmondo/monzo/me/onboarding/e$a$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lco/uk/getmondo/monzo/me/onboarding/e$a$a;->a:Lco/uk/getmondo/monzo/me/onboarding/e$a$a;

    .line 40
    new-instance v0, Lco/uk/getmondo/monzo/me/onboarding/e$a$a;

    const-string v1, "GO"

    invoke-direct {v0, v1, v3}, Lco/uk/getmondo/monzo/me/onboarding/e$a$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lco/uk/getmondo/monzo/me/onboarding/e$a$a;->b:Lco/uk/getmondo/monzo/me/onboarding/e$a$a;

    .line 38
    const/4 v0, 0x2

    new-array v0, v0, [Lco/uk/getmondo/monzo/me/onboarding/e$a$a;

    sget-object v1, Lco/uk/getmondo/monzo/me/onboarding/e$a$a;->a:Lco/uk/getmondo/monzo/me/onboarding/e$a$a;

    aput-object v1, v0, v2

    sget-object v1, Lco/uk/getmondo/monzo/me/onboarding/e$a$a;->b:Lco/uk/getmondo/monzo/me/onboarding/e$a$a;

    aput-object v1, v0, v3

    sput-object v0, Lco/uk/getmondo/monzo/me/onboarding/e$a$a;->c:[Lco/uk/getmondo/monzo/me/onboarding/e$a$a;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 38
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lco/uk/getmondo/monzo/me/onboarding/e$a$a;
    .locals 1

    .prologue
    .line 38
    const-class v0, Lco/uk/getmondo/monzo/me/onboarding/e$a$a;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/monzo/me/onboarding/e$a$a;

    return-object v0
.end method

.method public static values()[Lco/uk/getmondo/monzo/me/onboarding/e$a$a;
    .locals 1

    .prologue
    .line 38
    sget-object v0, Lco/uk/getmondo/monzo/me/onboarding/e$a$a;->c:[Lco/uk/getmondo/monzo/me/onboarding/e$a$a;

    invoke-virtual {v0}, [Lco/uk/getmondo/monzo/me/onboarding/e$a$a;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lco/uk/getmondo/monzo/me/onboarding/e$a$a;

    return-object v0
.end method
