.class public Lco/uk/getmondo/monzo/me/onboarding/MonzoMeOnboardingActivity_ViewBinding;
.super Ljava/lang/Object;
.source "MonzoMeOnboardingActivity_ViewBinding.java"

# interfaces
.implements Lbutterknife/Unbinder;


# instance fields
.field private a:Lco/uk/getmondo/monzo/me/onboarding/MonzoMeOnboardingActivity;


# direct methods
.method public constructor <init>(Lco/uk/getmondo/monzo/me/onboarding/MonzoMeOnboardingActivity;Landroid/view/View;)V
    .locals 3

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    iput-object p1, p0, Lco/uk/getmondo/monzo/me/onboarding/MonzoMeOnboardingActivity_ViewBinding;->a:Lco/uk/getmondo/monzo/me/onboarding/MonzoMeOnboardingActivity;

    .line 30
    const v0, 0x7f1101c8

    const-string v1, "field \'viewPager\'"

    const-class v2, Landroid/support/v4/view/ViewPager;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/view/ViewPager;

    iput-object v0, p1, Lco/uk/getmondo/monzo/me/onboarding/MonzoMeOnboardingActivity;->viewPager:Landroid/support/v4/view/ViewPager;

    .line 31
    const v0, 0x7f1101c9

    const-string v1, "field \'viewPagerIndicator\'"

    const-class v2, Lme/relex/circleindicator/CircleIndicator;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lme/relex/circleindicator/CircleIndicator;

    iput-object v0, p1, Lco/uk/getmondo/monzo/me/onboarding/MonzoMeOnboardingActivity;->viewPagerIndicator:Lme/relex/circleindicator/CircleIndicator;

    .line 32
    const v0, 0x7f1101ca

    const-string v1, "field \'actionButton\'"

    const-class v2, Landroid/widget/Button;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p1, Lco/uk/getmondo/monzo/me/onboarding/MonzoMeOnboardingActivity;->actionButton:Landroid/widget/Button;

    .line 33
    const v0, 0x7f1101cb

    const-string v1, "field \'progress\'"

    const-class v2, Landroid/widget/ProgressBar;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p1, Lco/uk/getmondo/monzo/me/onboarding/MonzoMeOnboardingActivity;->progress:Landroid/widget/ProgressBar;

    .line 34
    const v0, 0x7f1100fe

    const-string v1, "field \'toolbar\'"

    const-class v2, Landroid/support/v7/widget/Toolbar;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/Toolbar;

    iput-object v0, p1, Lco/uk/getmondo/monzo/me/onboarding/MonzoMeOnboardingActivity;->toolbar:Landroid/support/v7/widget/Toolbar;

    .line 35
    return-void
.end method


# virtual methods
.method public unbind()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 40
    iget-object v0, p0, Lco/uk/getmondo/monzo/me/onboarding/MonzoMeOnboardingActivity_ViewBinding;->a:Lco/uk/getmondo/monzo/me/onboarding/MonzoMeOnboardingActivity;

    .line 41
    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Bindings already cleared."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 42
    :cond_0
    iput-object v1, p0, Lco/uk/getmondo/monzo/me/onboarding/MonzoMeOnboardingActivity_ViewBinding;->a:Lco/uk/getmondo/monzo/me/onboarding/MonzoMeOnboardingActivity;

    .line 44
    iput-object v1, v0, Lco/uk/getmondo/monzo/me/onboarding/MonzoMeOnboardingActivity;->viewPager:Landroid/support/v4/view/ViewPager;

    .line 45
    iput-object v1, v0, Lco/uk/getmondo/monzo/me/onboarding/MonzoMeOnboardingActivity;->viewPagerIndicator:Lme/relex/circleindicator/CircleIndicator;

    .line 46
    iput-object v1, v0, Lco/uk/getmondo/monzo/me/onboarding/MonzoMeOnboardingActivity;->actionButton:Landroid/widget/Button;

    .line 47
    iput-object v1, v0, Lco/uk/getmondo/monzo/me/onboarding/MonzoMeOnboardingActivity;->progress:Landroid/widget/ProgressBar;

    .line 48
    iput-object v1, v0, Lco/uk/getmondo/monzo/me/onboarding/MonzoMeOnboardingActivity;->toolbar:Landroid/support/v7/widget/Toolbar;

    .line 49
    return-void
.end method
