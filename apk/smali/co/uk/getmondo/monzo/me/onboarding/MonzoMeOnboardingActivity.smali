.class public Lco/uk/getmondo/monzo/me/onboarding/MonzoMeOnboardingActivity;
.super Lco/uk/getmondo/common/activities/b;
.source "MonzoMeOnboardingActivity.java"

# interfaces
.implements Lco/uk/getmondo/monzo/me/onboarding/e$a;


# static fields
.field private static final c:[Lco/uk/getmondo/common/pager/f;


# instance fields
.field a:Lco/uk/getmondo/monzo/me/onboarding/e;

.field actionButton:Landroid/widget/Button;
    .annotation build Lbutterknife/BindView;
        value = 0x7f1101ca
    .end annotation
.end field

.field b:Lco/uk/getmondo/common/pager/h;

.field private e:Lio/reactivex/n;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/n",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field progress:Landroid/widget/ProgressBar;
    .annotation build Lbutterknife/BindView;
        value = 0x7f1101cb
    .end annotation
.end field

.field toolbar:Landroid/support/v7/widget/Toolbar;
    .annotation build Lbutterknife/BindView;
        value = 0x7f1100fe
    .end annotation
.end field

.field viewPager:Landroid/support/v4/view/ViewPager;
    .annotation build Lbutterknife/BindView;
        value = 0x7f1101c8
    .end annotation
.end field

.field viewPagerIndicator:Lme/relex/circleindicator/CircleIndicator;
    .annotation build Lbutterknife/BindView;
        value = 0x7f1101c9
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 10

    .prologue
    const/4 v9, 0x3

    const/4 v8, 0x2

    const/4 v7, 0x1

    .line 38
    new-array v0, v9, [Lco/uk/getmondo/common/pager/f;

    const/4 v1, 0x0

    new-instance v2, Lco/uk/getmondo/common/pager/d;

    const v3, 0x7f02016c

    const v4, 0x7f0a02a9

    const v5, 0x7f0a02a8

    .line 40
    invoke-static {v7}, Lco/uk/getmondo/api/model/tracking/Impression;->c(I)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v6

    invoke-direct {v2, v3, v4, v5, v6}, Lco/uk/getmondo/common/pager/d;-><init>(IIILco/uk/getmondo/api/model/tracking/Impression;)V

    aput-object v2, v0, v1

    new-instance v1, Lco/uk/getmondo/common/pager/d;

    const v2, 0x7f02015b

    const v3, 0x7f0a02ab

    const v4, 0x7f0a02aa

    .line 42
    invoke-static {v8}, Lco/uk/getmondo/api/model/tracking/Impression;->c(I)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v5

    invoke-direct {v1, v2, v3, v4, v5}, Lco/uk/getmondo/common/pager/d;-><init>(IIILco/uk/getmondo/api/model/tracking/Impression;)V

    aput-object v1, v0, v7

    new-instance v1, Lco/uk/getmondo/common/pager/d;

    const v2, 0x7f02015c

    const v3, 0x7f0a02ad

    const v4, 0x7f0a02ac

    .line 44
    invoke-static {v9}, Lco/uk/getmondo/api/model/tracking/Impression;->c(I)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v5

    invoke-direct {v1, v2, v3, v4, v5}, Lco/uk/getmondo/common/pager/d;-><init>(IIILco/uk/getmondo/api/model/tracking/Impression;)V

    aput-object v1, v0, v8

    sput-object v0, Lco/uk/getmondo/monzo/me/onboarding/MonzoMeOnboardingActivity;->c:[Lco/uk/getmondo/common/pager/f;

    .line 38
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0}, Lco/uk/getmondo/common/activities/b;-><init>()V

    return-void
.end method

.method public static a(Landroid/content/Context;)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 35
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lco/uk/getmondo/monzo/me/onboarding/MonzoMeOnboardingActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    return-object v0
.end method

.method static synthetic a(Lco/uk/getmondo/monzo/me/onboarding/MonzoMeOnboardingActivity;Ljava/lang/Integer;)Lco/uk/getmondo/monzo/me/onboarding/e$a$a;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 121
    invoke-direct {p0}, Lco/uk/getmondo/monzo/me/onboarding/MonzoMeOnboardingActivity;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lco/uk/getmondo/monzo/me/onboarding/e$a$a;->b:Lco/uk/getmondo/monzo/me/onboarding/e$a$a;

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lco/uk/getmondo/monzo/me/onboarding/e$a$a;->a:Lco/uk/getmondo/monzo/me/onboarding/e$a$a;

    goto :goto_0
.end method

.method static synthetic a(Lco/uk/getmondo/monzo/me/onboarding/MonzoMeOnboardingActivity;Ljava/lang/Object;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 116
    invoke-direct {p0}, Lco/uk/getmondo/monzo/me/onboarding/MonzoMeOnboardingActivity;->f()Z

    move-result v0

    return v0
.end method

.method static synthetic b(Lco/uk/getmondo/monzo/me/onboarding/MonzoMeOnboardingActivity;Ljava/lang/Object;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 111
    invoke-direct {p0}, Lco/uk/getmondo/monzo/me/onboarding/MonzoMeOnboardingActivity;->f()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private f()Z
    .locals 2

    .prologue
    .line 125
    iget-object v0, p0, Lco/uk/getmondo/monzo/me/onboarding/MonzoMeOnboardingActivity;->viewPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v0

    iget-object v1, p0, Lco/uk/getmondo/monzo/me/onboarding/MonzoMeOnboardingActivity;->viewPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v1}, Landroid/support/v4/view/ViewPager;->getAdapter()Landroid/support/v4/view/p;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/view/p;->b()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a()V
    .locals 3

    .prologue
    .line 83
    iget-object v0, p0, Lco/uk/getmondo/monzo/me/onboarding/MonzoMeOnboardingActivity;->viewPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    .line 84
    iget-object v1, p0, Lco/uk/getmondo/monzo/me/onboarding/MonzoMeOnboardingActivity;->viewPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v1}, Landroid/support/v4/view/ViewPager;->getAdapter()Landroid/support/v4/view/p;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/view/p;->b()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 85
    iget-object v1, p0, Lco/uk/getmondo/monzo/me/onboarding/MonzoMeOnboardingActivity;->viewPager:Landroid/support/v4/view/ViewPager;

    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, Landroid/support/v4/view/ViewPager;->a(IZ)V

    .line 87
    :cond_0
    return-void
.end method

.method public a(Lco/uk/getmondo/monzo/me/onboarding/e$a$a;)V
    .locals 3

    .prologue
    .line 91
    const/4 v0, 0x0

    .line 92
    sget-object v1, Lco/uk/getmondo/monzo/me/onboarding/MonzoMeOnboardingActivity$1;->a:[I

    invoke-virtual {p1}, Lco/uk/getmondo/monzo/me/onboarding/e$a$a;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 100
    :goto_0
    iget-object v1, p0, Lco/uk/getmondo/monzo/me/onboarding/MonzoMeOnboardingActivity;->actionButton:Landroid/widget/Button;

    invoke-virtual {v1, v0}, Landroid/widget/Button;->setText(I)V

    .line 101
    return-void

    .line 94
    :pswitch_0
    const v0, 0x7f0a02bd

    .line 95
    goto :goto_0

    .line 97
    :pswitch_1
    const v0, 0x7f0a0246

    goto :goto_0

    .line 92
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public b()V
    .locals 1

    .prologue
    .line 105
    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lco/uk/getmondo/monzo/me/onboarding/MonzoMeOnboardingActivity;->setResult(I)V

    .line 106
    invoke-virtual {p0}, Lco/uk/getmondo/monzo/me/onboarding/MonzoMeOnboardingActivity;->finish()V

    .line 107
    return-void
.end method

.method public c()Lio/reactivex/n;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 111
    iget-object v0, p0, Lco/uk/getmondo/monzo/me/onboarding/MonzoMeOnboardingActivity;->e:Lio/reactivex/n;

    invoke-static {p0}, Lco/uk/getmondo/monzo/me/onboarding/a;->a(Lco/uk/getmondo/monzo/me/onboarding/MonzoMeOnboardingActivity;)Lio/reactivex/c/q;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/n;->filter(Lio/reactivex/c/q;)Lio/reactivex/n;

    move-result-object v0

    return-object v0
.end method

.method public d()Lio/reactivex/n;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 116
    iget-object v0, p0, Lco/uk/getmondo/monzo/me/onboarding/MonzoMeOnboardingActivity;->e:Lio/reactivex/n;

    invoke-static {p0}, Lco/uk/getmondo/monzo/me/onboarding/b;->a(Lco/uk/getmondo/monzo/me/onboarding/MonzoMeOnboardingActivity;)Lio/reactivex/c/q;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/n;->filter(Lio/reactivex/c/q;)Lio/reactivex/n;

    move-result-object v0

    return-object v0
.end method

.method public e()Lio/reactivex/n;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Lco/uk/getmondo/monzo/me/onboarding/e$a$a;",
            ">;"
        }
    .end annotation

    .prologue
    .line 121
    iget-object v0, p0, Lco/uk/getmondo/monzo/me/onboarding/MonzoMeOnboardingActivity;->viewPager:Landroid/support/v4/view/ViewPager;

    invoke-static {v0}, Lcom/b/a/b/b/a/a;->a(Landroid/support/v4/view/ViewPager;)Lcom/b/a/a;

    move-result-object v0

    invoke-static {p0}, Lco/uk/getmondo/monzo/me/onboarding/c;->a(Lco/uk/getmondo/monzo/me/onboarding/MonzoMeOnboardingActivity;)Lio/reactivex/c/h;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/b/a/a;->map(Lio/reactivex/c/h;)Lio/reactivex/n;

    move-result-object v0

    return-object v0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 59
    invoke-super {p0, p1}, Lco/uk/getmondo/common/activities/b;->onCreate(Landroid/os/Bundle;)V

    .line 60
    invoke-virtual {p0}, Lco/uk/getmondo/monzo/me/onboarding/MonzoMeOnboardingActivity;->l()Lco/uk/getmondo/common/h/b/b;

    move-result-object v0

    invoke-interface {v0, p0}, Lco/uk/getmondo/common/h/b/b;->a(Lco/uk/getmondo/monzo/me/onboarding/MonzoMeOnboardingActivity;)V

    .line 61
    const v0, 0x7f05004c

    invoke-virtual {p0, v0}, Lco/uk/getmondo/monzo/me/onboarding/MonzoMeOnboardingActivity;->setContentView(I)V

    .line 62
    invoke-static {p0}, Lbutterknife/ButterKnife;->bind(Landroid/app/Activity;)Lbutterknife/Unbinder;

    .line 64
    iget-object v0, p0, Lco/uk/getmondo/monzo/me/onboarding/MonzoMeOnboardingActivity;->actionButton:Landroid/widget/Button;

    invoke-static {v0}, Lcom/b/a/c/c;->a(Landroid/view/View;)Lio/reactivex/n;

    move-result-object v0

    invoke-virtual {v0}, Lio/reactivex/n;->share()Lio/reactivex/n;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/monzo/me/onboarding/MonzoMeOnboardingActivity;->e:Lio/reactivex/n;

    .line 65
    iget-object v0, p0, Lco/uk/getmondo/monzo/me/onboarding/MonzoMeOnboardingActivity;->toolbar:Landroid/support/v7/widget/Toolbar;

    const v1, 0x7f02014d

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/Toolbar;->setNavigationIcon(I)V

    .line 66
    const-string v0, ""

    invoke-virtual {p0, v0}, Lco/uk/getmondo/monzo/me/onboarding/MonzoMeOnboardingActivity;->setTitle(Ljava/lang/CharSequence;)V

    .line 67
    iget-object v0, p0, Lco/uk/getmondo/monzo/me/onboarding/MonzoMeOnboardingActivity;->viewPager:Landroid/support/v4/view/ViewPager;

    new-instance v1, Lco/uk/getmondo/common/pager/GenericPagerAdapter;

    sget-object v2, Lco/uk/getmondo/monzo/me/onboarding/MonzoMeOnboardingActivity;->c:[Lco/uk/getmondo/common/pager/f;

    invoke-direct {v1, v2}, Lco/uk/getmondo/common/pager/GenericPagerAdapter;-><init>([Lco/uk/getmondo/common/pager/f;)V

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setAdapter(Landroid/support/v4/view/p;)V

    .line 68
    iget-object v0, p0, Lco/uk/getmondo/monzo/me/onboarding/MonzoMeOnboardingActivity;->viewPagerIndicator:Lme/relex/circleindicator/CircleIndicator;

    iget-object v1, p0, Lco/uk/getmondo/monzo/me/onboarding/MonzoMeOnboardingActivity;->viewPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0, v1}, Lme/relex/circleindicator/CircleIndicator;->setViewPager(Landroid/support/v4/view/ViewPager;)V

    .line 69
    iget-object v0, p0, Lco/uk/getmondo/monzo/me/onboarding/MonzoMeOnboardingActivity;->b:Lco/uk/getmondo/common/pager/h;

    iget-object v1, p0, Lco/uk/getmondo/monzo/me/onboarding/MonzoMeOnboardingActivity;->viewPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0, v1}, Lco/uk/getmondo/common/pager/h;->a(Landroid/support/v4/view/ViewPager;)V

    .line 71
    iget-object v0, p0, Lco/uk/getmondo/monzo/me/onboarding/MonzoMeOnboardingActivity;->a:Lco/uk/getmondo/monzo/me/onboarding/e;

    invoke-virtual {v0, p0}, Lco/uk/getmondo/monzo/me/onboarding/e;->a(Lco/uk/getmondo/monzo/me/onboarding/e$a;)V

    .line 72
    return-void
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 76
    iget-object v0, p0, Lco/uk/getmondo/monzo/me/onboarding/MonzoMeOnboardingActivity;->a:Lco/uk/getmondo/monzo/me/onboarding/e;

    invoke-virtual {v0}, Lco/uk/getmondo/monzo/me/onboarding/e;->b()V

    .line 77
    iget-object v0, p0, Lco/uk/getmondo/monzo/me/onboarding/MonzoMeOnboardingActivity;->b:Lco/uk/getmondo/common/pager/h;

    invoke-virtual {v0}, Lco/uk/getmondo/common/pager/h;->a()V

    .line 78
    invoke-super {p0}, Lco/uk/getmondo/common/activities/b;->onDestroy()V

    .line 79
    return-void
.end method
