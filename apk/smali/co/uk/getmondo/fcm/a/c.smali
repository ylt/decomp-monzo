.class public Lco/uk/getmondo/fcm/a/c;
.super Lco/uk/getmondo/fcm/a/d;
.source "KycNotification.java"


# instance fields
.field private final d:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Lco/uk/getmondo/api/model/NotificationMessage;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 20
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v0

    invoke-direct {p0, v0, p2}, Lco/uk/getmondo/fcm/a/d;-><init>(ILco/uk/getmondo/api/model/NotificationMessage;)V

    .line 21
    iput-object p3, p0, Lco/uk/getmondo/fcm/a/c;->d:Ljava/lang/String;

    .line 22
    return-void
.end method


# virtual methods
.method public a(Landroid/content/Context;Lco/uk/getmondo/d/ak$a;)Landroid/app/PendingIntent;
    .locals 3

    .prologue
    .line 26
    sget-object v0, Lco/uk/getmondo/d/ak$a;->HAS_ACCOUNT:Lco/uk/getmondo/d/ak$a;

    if-eq p2, v0, :cond_0

    .line 27
    invoke-super {p0, p1, p2}, Lco/uk/getmondo/fcm/a/d;->a(Landroid/content/Context;Lco/uk/getmondo/d/ak$a;)Landroid/app/PendingIntent;

    move-result-object v0

    .line 39
    :goto_0
    return-object v0

    .line 31
    :cond_0
    iget-object v0, p0, Lco/uk/getmondo/fcm/a/c;->d:Ljava/lang/String;

    const-string v1, "kyc_request"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 32
    sget-object v0, Lco/uk/getmondo/signup/identity_verification/a/j;->a:Lco/uk/getmondo/signup/identity_verification/a/j;

    sget-object v1, Lco/uk/getmondo/api/model/tracking/Impression$KycFrom;->FEED:Lco/uk/getmondo/api/model/tracking/Impression$KycFrom;

    sget-object v2, Lco/uk/getmondo/api/model/signup/SignupSource;->LEGACY_PREPAID:Lco/uk/getmondo/api/model/signup/SignupSource;

    invoke-static {p1, v0, v1, v2}, Lco/uk/getmondo/signup/identity_verification/IdentityVerificationActivity;->a(Landroid/content/Context;Lco/uk/getmondo/signup/identity_verification/a/j;Lco/uk/getmondo/api/model/tracking/Impression$KycFrom;Lco/uk/getmondo/api/model/signup/SignupSource;)Landroid/content/Intent;

    move-result-object v0

    .line 39
    :goto_1
    invoke-static {p1}, Landroid/app/TaskStackBuilder;->create(Landroid/content/Context;)Landroid/app/TaskStackBuilder;

    move-result-object v1

    .line 40
    invoke-virtual {v1, v0}, Landroid/app/TaskStackBuilder;->addNextIntentWithParentStack(Landroid/content/Intent;)Landroid/app/TaskStackBuilder;

    move-result-object v0

    .line 41
    invoke-virtual {p0}, Lco/uk/getmondo/fcm/a/c;->b()I

    move-result v1

    const/high16 v2, 0x8000000

    invoke-virtual {v0, v1, v2}, Landroid/app/TaskStackBuilder;->getPendingIntent(II)Landroid/app/PendingIntent;

    move-result-object v0

    goto :goto_0

    .line 34
    :cond_1
    iget-object v0, p0, Lco/uk/getmondo/fcm/a/c;->d:Ljava/lang/String;

    const-string v1, "kyc_passed"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 35
    invoke-static {p1}, Lco/uk/getmondo/signup/identity_verification/IdentityApprovedActivity;->b(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    goto :goto_1

    .line 37
    :cond_2
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unexpected KYC subtype found: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lco/uk/getmondo/fcm/a/c;->d:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
