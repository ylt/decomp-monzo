.class public Lco/uk/getmondo/fcm/a/e;
.super Lco/uk/getmondo/fcm/a/d;
.source "TransactionNotification.java"


# static fields
.field private static final d:Landroid/net/Uri;

.field private static final e:Landroid/net/Uri;


# instance fields
.field private final f:Ljava/lang/String;

.field private final g:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 16
    const-string v0, "android.resource://co.uk.getmondo/2131296264"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lco/uk/getmondo/fcm/a/e;->d:Landroid/net/Uri;

    .line 17
    const-string v0, "android.resource://co.uk.getmondo/2131296263"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lco/uk/getmondo/fcm/a/e;->e:Landroid/net/Uri;

    return-void
.end method

.method public constructor <init>(Lco/uk/getmondo/api/model/NotificationMessage;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 23
    invoke-virtual {p2}, Ljava/lang/String;->hashCode()I

    move-result v0

    invoke-direct {p0, v0, p1}, Lco/uk/getmondo/fcm/a/d;-><init>(ILco/uk/getmondo/api/model/NotificationMessage;)V

    .line 24
    iput-object p2, p0, Lco/uk/getmondo/fcm/a/e;->f:Ljava/lang/String;

    .line 25
    iput-object p3, p0, Lco/uk/getmondo/fcm/a/e;->g:Ljava/lang/String;

    .line 26
    return-void
.end method


# virtual methods
.method public a(Landroid/content/Context;Lco/uk/getmondo/d/ak$a;)Landroid/app/PendingIntent;
    .locals 3

    .prologue
    .line 43
    sget-object v0, Lco/uk/getmondo/d/ak$a;->HAS_ACCOUNT:Lco/uk/getmondo/d/ak$a;

    if-eq p2, v0, :cond_0

    .line 44
    invoke-super {p0, p1, p2}, Lco/uk/getmondo/fcm/a/d;->a(Landroid/content/Context;Lco/uk/getmondo/d/ak$a;)Landroid/app/PendingIntent;

    move-result-object v0

    .line 47
    :goto_0
    return-object v0

    :cond_0
    invoke-static {p1}, Landroid/app/TaskStackBuilder;->create(Landroid/content/Context;)Landroid/app/TaskStackBuilder;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/fcm/a/e;->f:Ljava/lang/String;

    .line 48
    invoke-static {p1, v1}, Lco/uk/getmondo/transaction/details/TransactionDetailsActivity;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/TaskStackBuilder;->addNextIntentWithParentStack(Landroid/content/Intent;)Landroid/app/TaskStackBuilder;

    move-result-object v0

    .line 49
    invoke-virtual {p0}, Lco/uk/getmondo/fcm/a/e;->b()I

    move-result v1

    const/high16 v2, 0x8000000

    invoke-virtual {v0, v1, v2}, Landroid/app/TaskStackBuilder;->getPendingIntent(II)Landroid/app/PendingIntent;

    move-result-object v0

    goto :goto_0
.end method

.method public a()Landroid/net/Uri;
    .locals 2

    .prologue
    .line 31
    iget-object v0, p0, Lco/uk/getmondo/fcm/a/e;->g:Ljava/lang/String;

    const-string v1, "transaction_topup"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lco/uk/getmondo/fcm/a/e;->g:Ljava/lang/String;

    const-string v1, "transaction_credit"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 32
    :cond_0
    sget-object v0, Lco/uk/getmondo/fcm/a/e;->e:Landroid/net/Uri;

    .line 38
    :goto_0
    return-object v0

    .line 33
    :cond_1
    iget-object v0, p0, Lco/uk/getmondo/fcm/a/e;->g:Ljava/lang/String;

    const-string v1, "transaction_decline"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 34
    sget-object v0, Lco/uk/getmondo/fcm/a/e;->d:Landroid/net/Uri;

    goto :goto_0

    .line 36
    :cond_2
    sget-object v0, Lco/uk/getmondo/fcm/a/e;->a:Landroid/net/Uri;

    goto :goto_0
.end method
