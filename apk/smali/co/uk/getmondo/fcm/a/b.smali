.class public Lco/uk/getmondo/fcm/a/b;
.super Lco/uk/getmondo/fcm/a/d;
.source "GhostLoginNotification.java"


# instance fields
.field private final d:Ljava/lang/String;


# direct methods
.method public constructor <init>(ILco/uk/getmondo/api/model/NotificationMessage;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0, p1, p2}, Lco/uk/getmondo/fcm/a/d;-><init>(ILco/uk/getmondo/api/model/NotificationMessage;)V

    .line 19
    iput-object p3, p0, Lco/uk/getmondo/fcm/a/b;->d:Ljava/lang/String;

    .line 20
    return-void
.end method


# virtual methods
.method public a(Landroid/content/Context;Lco/uk/getmondo/d/ak$a;)Landroid/app/PendingIntent;
    .locals 4

    .prologue
    .line 24
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 25
    const-string v1, "co.uk.getmondo.ghost"

    .line 29
    new-instance v2, Landroid/content/ComponentName;

    const-string v3, "co.uk.getmondo.ghost.GhostLoginActivity"

    invoke-direct {v2, v1, v3}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 30
    const-string v1, "KEY_EXTRA_ACCESS_TOKEN"

    iget-object v2, p0, Lco/uk/getmondo/fcm/a/b;->d:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 33
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->resolveActivity(Landroid/content/pm/PackageManager;)Landroid/content/ComponentName;

    move-result-object v1

    if-nez v1, :cond_0

    .line 34
    invoke-super {p0, p1, p2}, Lco/uk/getmondo/fcm/a/d;->a(Landroid/content/Context;Lco/uk/getmondo/d/ak$a;)Landroid/app/PendingIntent;

    move-result-object v0

    .line 36
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Lco/uk/getmondo/fcm/a/b;->b()I

    move-result v1

    const/high16 v2, 0x8000000

    invoke-static {p1, v1, v0, v2}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    goto :goto_0
.end method
