.class public abstract Lco/uk/getmondo/fcm/a/d;
.super Ljava/lang/Object;
.source "PushNotification.java"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field static final a:Landroid/net/Uri;


# instance fields
.field protected final b:I

.field protected final c:Lco/uk/getmondo/api/model/NotificationMessage;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 17
    const/4 v0, 0x2

    invoke-static {v0}, Landroid/media/RingtoneManager;->getDefaultUri(I)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lco/uk/getmondo/fcm/a/d;->a:Landroid/net/Uri;

    return-void
.end method

.method constructor <init>(ILco/uk/getmondo/api/model/NotificationMessage;)V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    iput p1, p0, Lco/uk/getmondo/fcm/a/d;->b:I

    .line 24
    iput-object p2, p0, Lco/uk/getmondo/fcm/a/d;->c:Lco/uk/getmondo/api/model/NotificationMessage;

    .line 25
    return-void
.end method


# virtual methods
.method public a(Landroid/content/Context;Lco/uk/getmondo/d/ak$a;)Landroid/app/PendingIntent;
    .locals 3

    .prologue
    .line 29
    sget-object v0, Lco/uk/getmondo/d/ak$a;->HAS_ACCOUNT:Lco/uk/getmondo/d/ak$a;

    if-ne p2, v0, :cond_0

    .line 30
    invoke-static {p1}, Lco/uk/getmondo/main/HomeActivity;->c(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    .line 35
    :goto_0
    invoke-virtual {p0}, Lco/uk/getmondo/fcm/a/d;->b()I

    move-result v1

    const/high16 v2, 0x8000000

    invoke-static {p1, v1, v0, v2}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    return-object v0

    .line 32
    :cond_0
    invoke-static {p1}, Lco/uk/getmondo/splash/SplashActivity;->b(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    goto :goto_0
.end method

.method public a()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 39
    sget-object v0, Lco/uk/getmondo/fcm/a/d;->a:Landroid/net/Uri;

    return-object v0
.end method

.method public b()I
    .locals 1

    .prologue
    .line 43
    iget v0, p0, Lco/uk/getmondo/fcm/a/d;->b:I

    return v0
.end method

.method public c()Lco/uk/getmondo/api/model/NotificationMessage;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lco/uk/getmondo/fcm/a/d;->c:Lco/uk/getmondo/api/model/NotificationMessage;

    return-object v0
.end method
