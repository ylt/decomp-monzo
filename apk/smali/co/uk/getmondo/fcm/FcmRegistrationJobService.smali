.class public Lco/uk/getmondo/fcm/FcmRegistrationJobService;
.super Landroid/app/job/JobService;
.source "FcmRegistrationJobService.java"


# instance fields
.field a:Lio/reactivex/u;

.field b:Lco/uk/getmondo/api/MonzoApi;

.field private c:Lio/reactivex/b/b;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Landroid/app/job/JobService;-><init>()V

    .line 28
    return-void
.end method

.method static synthetic a(Lco/uk/getmondo/fcm/FcmRegistrationJobService;Landroid/app/job/JobParameters;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 42
    const-string v0, "Successfully registered for FCM messaging"

    new-array v1, v2, [Ljava/lang/Object;

    invoke-static {v0, v1}, Ld/a/a;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 43
    invoke-virtual {p0, p1, v2}, Lco/uk/getmondo/fcm/FcmRegistrationJobService;->jobFinished(Landroid/app/job/JobParameters;Z)V

    .line 44
    return-void
.end method

.method static synthetic a(Lco/uk/getmondo/fcm/FcmRegistrationJobService;Landroid/app/job/JobParameters;Ljava/lang/Throwable;)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 45
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v3, "Failure in FCM registration"

    invoke-direct {v0, v3, p2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    invoke-static {v0}, Ld/a/a;->a(Ljava/lang/Throwable;)V

    .line 47
    instance-of v0, p2, Ljava/io/IOException;

    if-eqz v0, :cond_2

    .line 48
    instance-of v0, p2, Lco/uk/getmondo/api/ApiException;

    if-eqz v0, :cond_0

    move-object v0, p2

    .line 49
    check-cast v0, Lco/uk/getmondo/api/ApiException;

    .line 50
    invoke-virtual {v0}, Lco/uk/getmondo/api/ApiException;->b()I

    move-result v0

    const/16 v3, 0x1f4

    if-lt v0, v3, :cond_2

    move v0, v1

    .line 57
    :goto_0
    if-eqz v0, :cond_1

    .line 58
    const-string v1, "Error with FCM registration, rescheduling"

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v1, v2}, Ld/a/a;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 62
    :goto_1
    invoke-virtual {p0, p1, v0}, Lco/uk/getmondo/fcm/FcmRegistrationJobService;->jobFinished(Landroid/app/job/JobParameters;Z)V

    .line 63
    return-void

    :cond_0
    move v0, v1

    .line 54
    goto :goto_0

    .line 60
    :cond_1
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "Giving up trying to register for FCM"

    invoke-direct {v1, v2, p2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    invoke-static {v1}, Ld/a/a;->a(Ljava/lang/Throwable;)V

    goto :goto_1

    :cond_2
    move v0, v2

    goto :goto_0
.end method


# virtual methods
.method public onCreate()V
    .locals 1

    .prologue
    .line 32
    invoke-super {p0}, Landroid/app/job/JobService;->onCreate()V

    .line 33
    invoke-static {p0}, Lco/uk/getmondo/MonzoApplication;->a(Landroid/content/Context;)Lco/uk/getmondo/MonzoApplication;

    move-result-object v0

    invoke-virtual {v0}, Lco/uk/getmondo/MonzoApplication;->b()Lco/uk/getmondo/common/h/a/a;

    move-result-object v0

    invoke-interface {v0, p0}, Lco/uk/getmondo/common/h/a/a;->a(Lco/uk/getmondo/fcm/FcmRegistrationJobService;)V

    .line 34
    return-void
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 78
    iget-object v0, p0, Lco/uk/getmondo/fcm/FcmRegistrationJobService;->c:Lio/reactivex/b/b;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lco/uk/getmondo/fcm/FcmRegistrationJobService;->c:Lio/reactivex/b/b;

    invoke-interface {v0}, Lio/reactivex/b/b;->isDisposed()Z

    move-result v0

    if-nez v0, :cond_0

    .line 79
    iget-object v0, p0, Lco/uk/getmondo/fcm/FcmRegistrationJobService;->c:Lio/reactivex/b/b;

    invoke-interface {v0}, Lio/reactivex/b/b;->dispose()V

    .line 81
    :cond_0
    invoke-super {p0}, Landroid/app/job/JobService;->onDestroy()V

    .line 82
    return-void
.end method

.method public onStartJob(Landroid/app/job/JobParameters;)Z
    .locals 3

    .prologue
    .line 38
    invoke-virtual {p1}, Landroid/app/job/JobParameters;->getExtras()Landroid/os/PersistableBundle;

    move-result-object v0

    const-string v1, "KEY_TOKEN"

    invoke-virtual {v0, v1}, Landroid/os/PersistableBundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 39
    iget-object v1, p0, Lco/uk/getmondo/fcm/FcmRegistrationJobService;->b:Lco/uk/getmondo/api/MonzoApi;

    const-string v2, "co.uk.getmondo"

    invoke-interface {v1, v0, v2}, Lco/uk/getmondo/api/MonzoApi;->registerForFcm(Ljava/lang/String;Ljava/lang/String;)Lio/reactivex/b;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/fcm/FcmRegistrationJobService;->a:Lio/reactivex/u;

    .line 40
    invoke-virtual {v0, v1}, Lio/reactivex/b;->b(Lio/reactivex/u;)Lio/reactivex/b;

    move-result-object v0

    invoke-static {p0, p1}, Lco/uk/getmondo/fcm/a;->a(Lco/uk/getmondo/fcm/FcmRegistrationJobService;Landroid/app/job/JobParameters;)Lio/reactivex/c/a;

    move-result-object v1

    invoke-static {p0, p1}, Lco/uk/getmondo/fcm/b;->a(Lco/uk/getmondo/fcm/FcmRegistrationJobService;Landroid/app/job/JobParameters;)Lio/reactivex/c/g;

    move-result-object v2

    .line 41
    invoke-virtual {v0, v1, v2}, Lio/reactivex/b;->a(Lio/reactivex/c/a;Lio/reactivex/c/g;)Lio/reactivex/b/b;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/fcm/FcmRegistrationJobService;->c:Lio/reactivex/b/b;

    .line 64
    const/4 v0, 0x1

    return v0
.end method

.method public onStopJob(Landroid/app/job/JobParameters;)Z
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lco/uk/getmondo/fcm/FcmRegistrationJobService;->c:Lio/reactivex/b/b;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lco/uk/getmondo/fcm/FcmRegistrationJobService;->c:Lio/reactivex/b/b;

    invoke-interface {v0}, Lio/reactivex/b/b;->isDisposed()Z

    move-result v0

    if-nez v0, :cond_0

    .line 70
    iget-object v0, p0, Lco/uk/getmondo/fcm/FcmRegistrationJobService;->c:Lio/reactivex/b/b;

    invoke-interface {v0}, Lio/reactivex/b/b;->dispose()V

    .line 73
    :cond_0
    const/4 v0, 0x1

    return v0
.end method
