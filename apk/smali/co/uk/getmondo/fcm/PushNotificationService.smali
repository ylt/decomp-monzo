.class public Lco/uk/getmondo/fcm/PushNotificationService;
.super Lcom/google/firebase/messaging/FirebaseMessagingService;
.source "PushNotificationService.java"


# instance fields
.field a:Lcom/google/gson/f;

.field b:Lco/uk/getmondo/common/accounts/d;

.field c:Lio/intercom/android/sdk/push/IntercomPushClient;

.field d:Lco/uk/getmondo/settings/k;

.field e:Lco/uk/getmondo/common/a;

.field private f:Landroid/app/NotificationManager;

.field private g:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 42
    invoke-direct {p0}, Lcom/google/firebase/messaging/FirebaseMessagingService;-><init>()V

    return-void
.end method

.method private a(Lcom/google/gson/n;Ljava/lang/String;Lco/uk/getmondo/api/model/NotificationMessage;)Lco/uk/getmondo/fcm/a/d;
    .locals 3

    .prologue
    const/4 v1, 0x2

    .line 96
    const/4 v0, -0x1

    invoke-virtual {p2}, Ljava/lang/String;->hashCode()I

    move-result v2

    sparse-switch v2, :sswitch_data_0

    :cond_0
    :goto_0
    packed-switch v0, :pswitch_data_0

    .line 117
    new-instance v0, Lco/uk/getmondo/fcm/a/a;

    iget v1, p0, Lco/uk/getmondo/fcm/PushNotificationService;->g:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lco/uk/getmondo/fcm/PushNotificationService;->g:I

    invoke-direct {v0, v1, p3}, Lco/uk/getmondo/fcm/a/a;-><init>(ILco/uk/getmondo/api/model/NotificationMessage;)V

    .line 121
    :goto_1
    return-object v0

    .line 96
    :sswitch_0
    const-string v2, "transaction"

    invoke-virtual {p2, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    :sswitch_1
    const-string v2, "kyc"

    invoke-virtual {p2, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :sswitch_2
    const-string v2, "ghost_login"

    invoke-virtual {p2, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    move v0, v1

    goto :goto_0

    .line 98
    :pswitch_0
    const-string v0, "transaction_id"

    invoke-virtual {p1, v0}, Lcom/google/gson/n;->c(Ljava/lang/String;)Lcom/google/gson/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/gson/l;->b()Ljava/lang/String;

    move-result-object v1

    .line 99
    const-string v0, "sub_type"

    invoke-virtual {p1, v0}, Lcom/google/gson/n;->c(Ljava/lang/String;)Lcom/google/gson/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/gson/l;->b()Ljava/lang/String;

    move-result-object v2

    .line 100
    new-instance v0, Lco/uk/getmondo/fcm/a/e;

    invoke-direct {v0, p3, v1, v2}, Lco/uk/getmondo/fcm/a/e;-><init>(Lco/uk/getmondo/api/model/NotificationMessage;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 104
    :pswitch_1
    const-string v0, "feed_item_id"

    invoke-virtual {p1, v0}, Lcom/google/gson/n;->c(Ljava/lang/String;)Lcom/google/gson/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/gson/l;->b()Ljava/lang/String;

    move-result-object v1

    .line 105
    const-string v0, "sub_type"

    invoke-virtual {p1, v0}, Lcom/google/gson/n;->c(Ljava/lang/String;)Lcom/google/gson/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/gson/l;->b()Ljava/lang/String;

    move-result-object v2

    .line 106
    new-instance v0, Lco/uk/getmondo/fcm/a/c;

    invoke-direct {v0, v1, p3, v2}, Lco/uk/getmondo/fcm/a/c;-><init>(Ljava/lang/String;Lco/uk/getmondo/api/model/NotificationMessage;Ljava/lang/String;)V

    goto :goto_1

    .line 110
    :pswitch_2
    const-string v0, "access_token"

    invoke-virtual {p1, v0}, Lcom/google/gson/n;->c(Ljava/lang/String;)Lcom/google/gson/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/gson/l;->b()Ljava/lang/String;

    move-result-object v2

    .line 111
    new-instance v0, Lco/uk/getmondo/fcm/a/b;

    invoke-direct {v0, v1, p3, v2}, Lco/uk/getmondo/fcm/a/b;-><init>(ILco/uk/getmondo/api/model/NotificationMessage;Ljava/lang/String;)V

    goto :goto_1

    .line 96
    :sswitch_data_0
    .sparse-switch
        -0x45bdaa47 -> :sswitch_2
        0x1a0b5 -> :sswitch_1
        0x7fa0d2de -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private a(Lco/uk/getmondo/fcm/a/d;Lco/uk/getmondo/d/ak$a;)V
    .locals 9

    .prologue
    const v8, 0x7f0f0036

    const v7, 0x7f02016f

    const/16 v6, 0x18

    const/4 v5, 0x1

    .line 175
    iget-object v0, p0, Lco/uk/getmondo/fcm/PushNotificationService;->d:Lco/uk/getmondo/settings/k;

    invoke-virtual {v0}, Lco/uk/getmondo/settings/k;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 176
    invoke-virtual {p1}, Lco/uk/getmondo/fcm/a/d;->c()Lco/uk/getmondo/api/model/NotificationMessage;

    move-result-object v0

    .line 177
    new-instance v1, Landroid/support/v4/app/ab$c;

    invoke-direct {v1, p0}, Landroid/support/v4/app/ab$c;-><init>(Landroid/content/Context;)V

    .line 178
    invoke-virtual {v1, v7}, Landroid/support/v4/app/ab$c;->a(I)Landroid/support/v4/app/ab$c;

    move-result-object v1

    .line 179
    invoke-static {p0, v8}, Landroid/support/v4/content/a;->c(Landroid/content/Context;I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/support/v4/app/ab$c;->e(I)Landroid/support/v4/app/ab$c;

    move-result-object v1

    .line 180
    invoke-virtual {p1, p0, p2}, Lco/uk/getmondo/fcm/a/d;->a(Landroid/content/Context;Lco/uk/getmondo/d/ak$a;)Landroid/app/PendingIntent;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/support/v4/app/ab$c;->a(Landroid/app/PendingIntent;)Landroid/support/v4/app/ab$c;

    move-result-object v1

    .line 181
    invoke-virtual {v1, v5}, Landroid/support/v4/app/ab$c;->a(Z)Landroid/support/v4/app/ab$c;

    move-result-object v1

    .line 182
    invoke-virtual {p1}, Lco/uk/getmondo/fcm/a/d;->a()Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/support/v4/app/ab$c;->a(Landroid/net/Uri;)Landroid/support/v4/app/ab$c;

    move-result-object v1

    .line 184
    invoke-virtual {v0}, Lco/uk/getmondo/api/model/NotificationMessage;->a()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lco/uk/getmondo/common/k/p;->d(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {v0}, Lco/uk/getmondo/api/model/NotificationMessage;->b()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lco/uk/getmondo/common/k/p;->d(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 185
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    if-ge v2, v6, :cond_0

    .line 186
    const v2, 0x7f0a00dc

    invoke-virtual {p0, v2}, Lco/uk/getmondo/fcm/PushNotificationService;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/support/v4/app/ab$c;->a(Ljava/lang/CharSequence;)Landroid/support/v4/app/ab$c;

    .line 188
    :cond_0
    invoke-virtual {v0}, Lco/uk/getmondo/api/model/NotificationMessage;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/support/v4/app/ab$c;->b(Ljava/lang/CharSequence;)Landroid/support/v4/app/ab$c;

    move-result-object v2

    new-instance v3, Landroid/support/v4/app/ab$b;

    invoke-direct {v3}, Landroid/support/v4/app/ab$b;-><init>()V

    .line 190
    invoke-virtual {v0}, Lco/uk/getmondo/api/model/NotificationMessage;->c()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Landroid/support/v4/app/ab$b;->a(Ljava/lang/CharSequence;)Landroid/support/v4/app/ab$b;

    move-result-object v0

    .line 189
    invoke-virtual {v2, v0}, Landroid/support/v4/app/ab$c;->a(Landroid/support/v4/app/ab$n;)Landroid/support/v4/app/ab$c;

    .line 205
    :goto_0
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v0, v6, :cond_4

    .line 206
    const-string v0, "summary_group"

    invoke-virtual {v1, v0}, Landroid/support/v4/app/ab$c;->b(Ljava/lang/String;)Landroid/support/v4/app/ab$c;

    .line 209
    iget-object v0, p0, Lco/uk/getmondo/fcm/PushNotificationService;->f:Landroid/app/NotificationManager;

    invoke-virtual {p1}, Lco/uk/getmondo/fcm/a/d;->b()I

    move-result v2

    invoke-virtual {v1}, Landroid/support/v4/app/ab$c;->a()Landroid/app/Notification;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    .line 212
    iget-object v0, p0, Lco/uk/getmondo/fcm/PushNotificationService;->f:Landroid/app/NotificationManager;

    invoke-virtual {v0}, Landroid/app/NotificationManager;->getActiveNotifications()[Landroid/service/notification/StatusBarNotification;

    move-result-object v0

    array-length v0, v0

    .line 213
    if-le v0, v5, :cond_1

    .line 214
    invoke-static {p0}, Lco/uk/getmondo/splash/SplashActivity;->b(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v1

    const/high16 v2, 0x8000000

    invoke-static {p0, v5, v1, v2}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    .line 216
    new-instance v2, Landroid/support/v4/app/ab$c;

    invoke-direct {v2, p0}, Landroid/support/v4/app/ab$c;-><init>(Landroid/content/Context;)V

    .line 217
    invoke-virtual {v2, v7}, Landroid/support/v4/app/ab$c;->a(I)Landroid/support/v4/app/ab$c;

    move-result-object v2

    const-string v3, "summary_group"

    .line 218
    invoke-virtual {v2, v3}, Landroid/support/v4/app/ab$c;->b(Ljava/lang/String;)Landroid/support/v4/app/ab$c;

    move-result-object v2

    .line 219
    invoke-virtual {v2, v0}, Landroid/support/v4/app/ab$c;->b(I)Landroid/support/v4/app/ab$c;

    move-result-object v0

    .line 220
    invoke-virtual {v0, v5}, Landroid/support/v4/app/ab$c;->a(Z)Landroid/support/v4/app/ab$c;

    move-result-object v0

    .line 221
    invoke-virtual {v0, v1}, Landroid/support/v4/app/ab$c;->a(Landroid/app/PendingIntent;)Landroid/support/v4/app/ab$c;

    move-result-object v0

    .line 222
    invoke-static {p0, v8}, Landroid/support/v4/content/a;->c(Landroid/content/Context;I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/support/v4/app/ab$c;->e(I)Landroid/support/v4/app/ab$c;

    move-result-object v0

    .line 223
    invoke-virtual {v0, v5}, Landroid/support/v4/app/ab$c;->c(Z)Landroid/support/v4/app/ab$c;

    move-result-object v0

    .line 224
    iget-object v1, p0, Lco/uk/getmondo/fcm/PushNotificationService;->f:Landroid/app/NotificationManager;

    invoke-virtual {v0}, Landroid/support/v4/app/ab$c;->a()Landroid/app/Notification;

    move-result-object v0

    invoke-virtual {v1, v5, v0}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    .line 230
    :cond_1
    :goto_1
    return-void

    .line 191
    :cond_2
    invoke-virtual {v0}, Lco/uk/getmondo/api/model/NotificationMessage;->c()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lco/uk/getmondo/common/k/p;->d(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 192
    invoke-virtual {v0}, Lco/uk/getmondo/api/model/NotificationMessage;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/support/v4/app/ab$c;->a(Ljava/lang/CharSequence;)Landroid/support/v4/app/ab$c;

    move-result-object v2

    .line 193
    invoke-virtual {v0}, Lco/uk/getmondo/api/model/NotificationMessage;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/support/v4/app/ab$c;->b(Ljava/lang/CharSequence;)Landroid/support/v4/app/ab$c;

    move-result-object v2

    new-instance v3, Landroid/support/v4/app/ab$b;

    invoke-direct {v3}, Landroid/support/v4/app/ab$b;-><init>()V

    .line 195
    invoke-virtual {v0}, Lco/uk/getmondo/api/model/NotificationMessage;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Landroid/support/v4/app/ab$b;->a(Ljava/lang/CharSequence;)Landroid/support/v4/app/ab$b;

    move-result-object v0

    .line 194
    invoke-virtual {v2, v0}, Landroid/support/v4/app/ab$c;->a(Landroid/support/v4/app/ab$n;)Landroid/support/v4/app/ab$c;

    goto/16 :goto_0

    .line 197
    :cond_3
    invoke-virtual {v0}, Lco/uk/getmondo/api/model/NotificationMessage;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/support/v4/app/ab$c;->a(Ljava/lang/CharSequence;)Landroid/support/v4/app/ab$c;

    move-result-object v2

    .line 198
    invoke-virtual {v0}, Lco/uk/getmondo/api/model/NotificationMessage;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/support/v4/app/ab$c;->b(Ljava/lang/CharSequence;)Landroid/support/v4/app/ab$c;

    move-result-object v2

    new-instance v3, Landroid/support/v4/app/ab$e;

    invoke-direct {v3}, Landroid/support/v4/app/ab$e;-><init>()V

    .line 200
    invoke-virtual {v0}, Lco/uk/getmondo/api/model/NotificationMessage;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/support/v4/app/ab$e;->a(Ljava/lang/CharSequence;)Landroid/support/v4/app/ab$e;

    move-result-object v3

    .line 201
    invoke-virtual {v0}, Lco/uk/getmondo/api/model/NotificationMessage;->b()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/support/v4/app/ab$e;->b(Ljava/lang/CharSequence;)Landroid/support/v4/app/ab$e;

    move-result-object v3

    .line 202
    invoke-virtual {v0}, Lco/uk/getmondo/api/model/NotificationMessage;->c()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Landroid/support/v4/app/ab$e;->b(Ljava/lang/CharSequence;)Landroid/support/v4/app/ab$e;

    move-result-object v0

    .line 199
    invoke-virtual {v2, v0}, Landroid/support/v4/app/ab$c;->a(Landroid/support/v4/app/ab$n;)Landroid/support/v4/app/ab$c;

    goto/16 :goto_0

    .line 227
    :cond_4
    iget-object v0, p0, Lco/uk/getmondo/fcm/PushNotificationService;->f:Landroid/app/NotificationManager;

    invoke-virtual {p1}, Lco/uk/getmondo/fcm/a/d;->b()I

    move-result v2

    invoke-virtual {v1}, Landroid/support/v4/app/ab$c;->a()Landroid/app/Notification;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    goto :goto_1
.end method

.method private b()Lco/uk/getmondo/api/model/NotificationMessage;
    .locals 4

    .prologue
    .line 152
    new-instance v0, Lco/uk/getmondo/api/model/NotificationMessage;

    const-string v1, ""

    const-string v2, ""

    const v3, 0x7f0a02c4

    invoke-virtual {p0, v3}, Lco/uk/getmondo/fcm/PushNotificationService;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lco/uk/getmondo/api/model/NotificationMessage;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method private b(Ljava/lang/String;)Lco/uk/getmondo/api/model/NotificationMessage;
    .locals 3

    .prologue
    .line 125
    invoke-static {p1}, Lco/uk/getmondo/common/k/p;->d(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 127
    invoke-direct {p0}, Lco/uk/getmondo/fcm/PushNotificationService;->b()Lco/uk/getmondo/api/model/NotificationMessage;

    move-result-object v0

    .line 147
    :cond_0
    :goto_0
    return-object v0

    .line 131
    :cond_1
    :try_start_0
    iget-object v0, p0, Lco/uk/getmondo/fcm/PushNotificationService;->a:Lcom/google/gson/f;

    const-class v1, Lco/uk/getmondo/api/model/NotificationMessage;

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/f;->a(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/api/model/NotificationMessage;

    .line 132
    invoke-virtual {v0}, Lco/uk/getmondo/api/model/NotificationMessage;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lco/uk/getmondo/common/k/p;->d(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {v0}, Lco/uk/getmondo/api/model/NotificationMessage;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lco/uk/getmondo/common/k/p;->d(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 133
    invoke-virtual {v0}, Lco/uk/getmondo/api/model/NotificationMessage;->c()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lco/uk/getmondo/common/k/p;->c(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 136
    invoke-virtual {v0}, Lco/uk/getmondo/api/model/NotificationMessage;->c()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lco/uk/getmondo/fcm/PushNotificationService;->c(Ljava/lang/String;)Lco/uk/getmondo/api/model/NotificationMessage;

    move-result-object v0

    goto :goto_0

    .line 137
    :cond_2
    invoke-virtual {v0}, Lco/uk/getmondo/api/model/NotificationMessage;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lco/uk/getmondo/common/k/p;->d(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lco/uk/getmondo/api/model/NotificationMessage;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lco/uk/getmondo/common/k/p;->d(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 138
    invoke-virtual {v0}, Lco/uk/getmondo/api/model/NotificationMessage;->c()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lco/uk/getmondo/common/k/p;->d(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 140
    invoke-direct {p0}, Lco/uk/getmondo/fcm/PushNotificationService;->b()Lco/uk/getmondo/api/model/NotificationMessage;
    :try_end_0
    .catch Lcom/google/gson/JsonSyntaxException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    .line 143
    :catch_0
    move-exception v0

    .line 144
    const-string v1, "Failed to parse notification"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Ld/a/a;->a(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 147
    invoke-direct {p0, p1}, Lco/uk/getmondo/fcm/PushNotificationService;->c(Ljava/lang/String;)Lco/uk/getmondo/api/model/NotificationMessage;

    move-result-object v0

    goto :goto_0
.end method

.method private c(Ljava/lang/String;)Lco/uk/getmondo/api/model/NotificationMessage;
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 157
    invoke-static {p1}, Lco/uk/getmondo/common/k/p;->c(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "\n"

    invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 158
    const-string v0, "\\r?\\n"

    invoke-virtual {p1, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 159
    array-length v0, v1

    if-le v0, v3, :cond_0

    .line 160
    new-instance v0, Lco/uk/getmondo/api/model/NotificationMessage;

    const/4 v2, 0x0

    aget-object v2, v1, v2

    invoke-static {v2}, Lco/uk/getmondo/fcm/PushNotificationService;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    aget-object v1, v1, v3

    invoke-static {v1}, Lco/uk/getmondo/fcm/PushNotificationService;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v3, ""

    invoke-direct {v0, v2, v1, v3}, Lco/uk/getmondo/api/model/NotificationMessage;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 163
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lco/uk/getmondo/api/model/NotificationMessage;

    const-string v1, ""

    const-string v2, ""

    invoke-direct {v0, v1, v2, p1}, Lco/uk/getmondo/api/model/NotificationMessage;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private static d(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 167
    const-string v0, "."

    invoke-virtual {p0, v0}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 168
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    .line 169
    const/4 v1, 0x0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {p0, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p0

    .line 171
    :cond_0
    return-object p0
.end method


# virtual methods
.method public a(Lcom/google/firebase/messaging/a;)V
    .locals 5

    .prologue
    .line 70
    const-string v0, "Push notification received"

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Ld/a/a;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 71
    invoke-virtual {p1}, Lcom/google/firebase/messaging/a;->a()Ljava/util/Map;

    move-result-object v1

    .line 72
    iget-object v0, p0, Lco/uk/getmondo/fcm/PushNotificationService;->c:Lio/intercom/android/sdk/push/IntercomPushClient;

    invoke-virtual {v0, v1}, Lio/intercom/android/sdk/push/IntercomPushClient;->isIntercomPush(Ljava/util/Map;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 73
    iget-object v0, p0, Lco/uk/getmondo/fcm/PushNotificationService;->c:Lio/intercom/android/sdk/push/IntercomPushClient;

    invoke-virtual {p0}, Lco/uk/getmondo/fcm/PushNotificationService;->getApplication()Landroid/app/Application;

    move-result-object v2

    invoke-virtual {v0, v2, v1}, Lio/intercom/android/sdk/push/IntercomPushClient;->handlePush(Landroid/app/Application;Ljava/util/Map;)V

    .line 92
    :cond_0
    :goto_0
    return-void

    .line 74
    :cond_1
    const-string v0, "payload"

    invoke-interface {v1, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 75
    iget-object v2, p0, Lco/uk/getmondo/fcm/PushNotificationService;->a:Lcom/google/gson/f;

    const-string v0, "payload"

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const-class v3, Lcom/google/gson/l;

    invoke-virtual {v2, v0, v3}, Lcom/google/gson/f;->a(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/gson/l;

    invoke-virtual {v0}, Lcom/google/gson/l;->k()Lcom/google/gson/n;

    move-result-object v2

    .line 76
    const-string v0, "type"

    invoke-virtual {v2, v0}, Lcom/google/gson/n;->c(Ljava/lang/String;)Lcom/google/gson/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/gson/l;->b()Ljava/lang/String;

    move-result-object v3

    .line 77
    const-string v0, "sub_type"

    invoke-virtual {v2, v0}, Lcom/google/gson/n;->c(Ljava/lang/String;)Lcom/google/gson/l;

    move-result-object v0

    if-nez v0, :cond_3

    const/4 v0, 0x0

    .line 78
    :goto_1
    iget-object v4, p0, Lco/uk/getmondo/fcm/PushNotificationService;->e:Lco/uk/getmondo/common/a;

    invoke-static {v3, v0}, Lco/uk/getmondo/api/model/tracking/Impression;->c(Ljava/lang/String;Ljava/lang/String;)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    invoke-virtual {v4, v0}, Lco/uk/getmondo/common/a;->a(Lco/uk/getmondo/api/model/tracking/Impression;)V

    .line 80
    const-string v0, "alert"

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-direct {p0, v0}, Lco/uk/getmondo/fcm/PushNotificationService;->b(Ljava/lang/String;)Lco/uk/getmondo/api/model/NotificationMessage;

    move-result-object v0

    .line 81
    invoke-direct {p0, v2, v3, v0}, Lco/uk/getmondo/fcm/PushNotificationService;->a(Lcom/google/gson/n;Ljava/lang/String;Lco/uk/getmondo/api/model/NotificationMessage;)Lco/uk/getmondo/fcm/a/d;

    move-result-object v1

    .line 83
    iget-object v0, p0, Lco/uk/getmondo/fcm/PushNotificationService;->b:Lco/uk/getmondo/common/accounts/d;

    invoke-virtual {v0}, Lco/uk/getmondo/common/accounts/d;->b()Lco/uk/getmondo/d/ak;

    move-result-object v0

    .line 84
    if-eqz v0, :cond_4

    invoke-virtual {v0}, Lco/uk/getmondo/d/ak;->a()Lco/uk/getmondo/d/ak$a;

    move-result-object v0

    .line 85
    :goto_2
    sget-object v2, Lco/uk/getmondo/d/ak$a;->HAS_ACCOUNT:Lco/uk/getmondo/d/ak$a;

    if-ne v0, v2, :cond_2

    .line 87
    invoke-static {p0}, Lco/uk/getmondo/background_sync/SyncFeedAndBalanceJobService;->a(Landroid/content/Context;)V

    .line 90
    :cond_2
    invoke-direct {p0, v1, v0}, Lco/uk/getmondo/fcm/PushNotificationService;->a(Lco/uk/getmondo/fcm/a/d;Lco/uk/getmondo/d/ak$a;)V

    goto :goto_0

    .line 77
    :cond_3
    const-string v0, "sub_type"

    invoke-virtual {v2, v0}, Lcom/google/gson/n;->c(Ljava/lang/String;)Lcom/google/gson/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/gson/l;->b()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 84
    :cond_4
    sget-object v0, Lco/uk/getmondo/d/ak$a;->NO_PROFILE:Lco/uk/getmondo/d/ak$a;

    goto :goto_2
.end method

.method public onCreate()V
    .locals 1

    .prologue
    .line 61
    invoke-super {p0}, Lcom/google/firebase/messaging/FirebaseMessagingService;->onCreate()V

    .line 63
    invoke-static {p0}, Lco/uk/getmondo/MonzoApplication;->a(Landroid/content/Context;)Lco/uk/getmondo/MonzoApplication;

    move-result-object v0

    invoke-virtual {v0}, Lco/uk/getmondo/MonzoApplication;->b()Lco/uk/getmondo/common/h/a/a;

    move-result-object v0

    invoke-interface {v0, p0}, Lco/uk/getmondo/common/h/a/a;->a(Lco/uk/getmondo/fcm/PushNotificationService;)V

    .line 65
    const-string v0, "notification"

    invoke-virtual {p0, v0}, Lco/uk/getmondo/fcm/PushNotificationService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    iput-object v0, p0, Lco/uk/getmondo/fcm/PushNotificationService;->f:Landroid/app/NotificationManager;

    .line 66
    return-void
.end method
