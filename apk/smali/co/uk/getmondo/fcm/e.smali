.class public Lco/uk/getmondo/fcm/e;
.super Ljava/lang/Object;
.source "PushNotificationRegistration.java"


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Lio/intercom/android/sdk/push/IntercomPushClient;


# direct methods
.method constructor <init>(Landroid/content/Context;Lio/intercom/android/sdk/push/IntercomPushClient;)V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    iput-object p1, p0, Lco/uk/getmondo/fcm/e;->a:Landroid/content/Context;

    .line 28
    iput-object p2, p0, Lco/uk/getmondo/fcm/e;->b:Lio/intercom/android/sdk/push/IntercomPushClient;

    .line 29
    return-void
.end method


# virtual methods
.method public a()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 36
    sget-object v0, Lco/uk/getmondo/a;->b:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 57
    :cond_0
    :goto_0
    return-void

    .line 41
    :cond_1
    invoke-static {}, Lcom/google/firebase/iid/FirebaseInstanceId;->a()Lcom/google/firebase/iid/FirebaseInstanceId;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/firebase/iid/FirebaseInstanceId;->c()Ljava/lang/String;

    move-result-object v0

    .line 42
    invoke-static {v0}, Lco/uk/getmondo/common/k/p;->c(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 43
    iget-object v1, p0, Lco/uk/getmondo/fcm/e;->b:Lio/intercom/android/sdk/push/IntercomPushClient;

    iget-object v2, p0, Lco/uk/getmondo/fcm/e;->a:Landroid/content/Context;

    invoke-static {v2}, Lco/uk/getmondo/MonzoApplication;->a(Landroid/content/Context;)Lco/uk/getmondo/MonzoApplication;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Lio/intercom/android/sdk/push/IntercomPushClient;->sendTokenToIntercom(Landroid/app/Application;Ljava/lang/String;)V

    .line 45
    new-instance v1, Landroid/content/ComponentName;

    iget-object v2, p0, Lco/uk/getmondo/fcm/e;->a:Landroid/content/Context;

    const-class v3, Lco/uk/getmondo/fcm/FcmRegistrationJobService;

    invoke-direct {v1, v2, v3}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 47
    new-instance v2, Landroid/os/PersistableBundle;

    invoke-direct {v2}, Landroid/os/PersistableBundle;-><init>()V

    .line 48
    const-string v3, "KEY_TOKEN"

    invoke-virtual {v2, v3, v0}, Landroid/os/PersistableBundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 50
    iget-object v0, p0, Lco/uk/getmondo/fcm/e;->a:Landroid/content/Context;

    const-string v3, "jobscheduler"

    invoke-virtual {v0, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/job/JobScheduler;

    .line 51
    new-instance v3, Landroid/app/job/JobInfo$Builder;

    const/4 v4, 0x0

    invoke-direct {v3, v4, v1}, Landroid/app/job/JobInfo$Builder;-><init>(ILandroid/content/ComponentName;)V

    .line 52
    invoke-virtual {v3, v5}, Landroid/app/job/JobInfo$Builder;->setPersisted(Z)Landroid/app/job/JobInfo$Builder;

    move-result-object v1

    .line 53
    invoke-virtual {v1, v5}, Landroid/app/job/JobInfo$Builder;->setRequiredNetworkType(I)Landroid/app/job/JobInfo$Builder;

    move-result-object v1

    .line 54
    invoke-virtual {v1, v2}, Landroid/app/job/JobInfo$Builder;->setExtras(Landroid/os/PersistableBundle;)Landroid/app/job/JobInfo$Builder;

    move-result-object v1

    .line 55
    invoke-virtual {v1}, Landroid/app/job/JobInfo$Builder;->build()Landroid/app/job/JobInfo;

    move-result-object v1

    .line 51
    invoke-virtual {v0, v1}, Landroid/app/job/JobScheduler;->schedule(Landroid/app/job/JobInfo;)I

    goto :goto_0
.end method
