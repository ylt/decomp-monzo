.class final Lco/uk/getmondo/profile/data/a$e;
.super Ljava/lang/Object;
.source "ProfileManager.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lco/uk/getmondo/profile/data/a;->a(Lco/uk/getmondo/d/s;Lco/uk/getmondo/common/pin/a/a/a;)Lio/reactivex/b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h",
        "<",
        "Ljava/lang/String;",
        "Lio/reactivex/d;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n\u00a2\u0006\u0002\u0008\u0004"
    }
    d2 = {
        "<anonymous>",
        "Lio/reactivex/Completable;",
        "userId",
        "",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field final synthetic a:Lco/uk/getmondo/profile/data/a;

.field final synthetic b:Lco/uk/getmondo/d/s;

.field final synthetic c:Lco/uk/getmondo/common/pin/a/a/a;


# direct methods
.method constructor <init>(Lco/uk/getmondo/profile/data/a;Lco/uk/getmondo/d/s;Lco/uk/getmondo/common/pin/a/a/a;)V
    .locals 0

    iput-object p1, p0, Lco/uk/getmondo/profile/data/a$e;->a:Lco/uk/getmondo/profile/data/a;

    iput-object p2, p0, Lco/uk/getmondo/profile/data/a$e;->b:Lco/uk/getmondo/d/s;

    iput-object p3, p0, Lco/uk/getmondo/profile/data/a$e;->c:Lco/uk/getmondo/common/pin/a/a/a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Lio/reactivex/b;
    .locals 10

    .prologue
    const-string v0, "userId"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 40
    iget-object v0, p0, Lco/uk/getmondo/profile/data/a$e;->a:Lco/uk/getmondo/profile/data/a;

    invoke-static {v0}, Lco/uk/getmondo/profile/data/a;->b(Lco/uk/getmondo/profile/data/a;)Lco/uk/getmondo/profile/data/MonzoProfileApi;

    move-result-object v0

    .line 42
    iget-object v1, p0, Lco/uk/getmondo/profile/data/a$e;->b:Lco/uk/getmondo/d/s;

    invoke-virtual {v1}, Lco/uk/getmondo/d/s;->h()[Ljava/lang/String;

    move-result-object v2

    .line 43
    iget-object v1, p0, Lco/uk/getmondo/profile/data/a$e;->b:Lco/uk/getmondo/d/s;

    invoke-virtual {v1}, Lco/uk/getmondo/d/s;->i()Ljava/lang/String;

    move-result-object v3

    .line 44
    iget-object v1, p0, Lco/uk/getmondo/profile/data/a$e;->b:Lco/uk/getmondo/d/s;

    invoke-virtual {v1}, Lco/uk/getmondo/d/s;->b()Ljava/lang/String;

    move-result-object v4

    .line 45
    iget-object v1, p0, Lco/uk/getmondo/profile/data/a$e;->b:Lco/uk/getmondo/d/s;

    invoke-virtual {v1}, Lco/uk/getmondo/d/s;->c()Ljava/lang/String;

    move-result-object v5

    .line 46
    iget-object v1, p0, Lco/uk/getmondo/profile/data/a$e;->b:Lco/uk/getmondo/d/s;

    invoke-virtual {v1}, Lco/uk/getmondo/d/s;->j()Ljava/lang/String;

    move-result-object v6

    .line 47
    iget-object v1, p0, Lco/uk/getmondo/profile/data/a$e;->c:Lco/uk/getmondo/common/pin/a/a/a;

    invoke-virtual {v1}, Lco/uk/getmondo/common/pin/a/a/a;->b()Ljava/lang/String;

    move-result-object v7

    .line 48
    iget-object v1, p0, Lco/uk/getmondo/profile/data/a$e;->c:Lco/uk/getmondo/common/pin/a/a/a;

    invoke-virtual {v1}, Lco/uk/getmondo/common/pin/a/a/a;->a()Lco/uk/getmondo/common/pin/a/a/a$a;

    move-result-object v1

    invoke-virtual {v1}, Lco/uk/getmondo/common/pin/a/a/a$a;->a()Ljava/lang/String;

    move-result-object v8

    .line 49
    iget-object v1, p0, Lco/uk/getmondo/profile/data/a$e;->a:Lco/uk/getmondo/profile/data/a;

    invoke-static {v1}, Lco/uk/getmondo/profile/data/a;->c(Lco/uk/getmondo/profile/data/a;)Lco/uk/getmondo/topup/a/a;

    move-result-object v1

    iget-object v9, p0, Lco/uk/getmondo/profile/data/a$e;->b:Lco/uk/getmondo/d/s;

    invoke-virtual {v1, v9}, Lco/uk/getmondo/topup/a/a;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    const-string v1, "idempotencyGenerator.getKey(address)"

    invoke-static {v9, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v1, p1

    .line 40
    invoke-interface/range {v0 .. v9}, Lco/uk/getmondo/profile/data/MonzoProfileApi;->updateAddress(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lio/reactivex/b;

    move-result-object v0

    .line 49
    return-object v0
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 17
    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1}, Lco/uk/getmondo/profile/data/a$e;->a(Ljava/lang/String;)Lio/reactivex/b;

    move-result-object v0

    return-object v0
.end method
