.class public final Lco/uk/getmondo/profile/data/c;
.super Ljava/lang/Object;
.source "ProfileManager_Factory.java"

# interfaces
.implements Lb/a/b;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lb/a/b",
        "<",
        "Lco/uk/getmondo/profile/data/a;",
        ">;"
    }
.end annotation


# static fields
.field static final synthetic a:Z


# instance fields
.field private final b:Ljavax/a/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/api/MonzoApi;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Ljavax/a/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/profile/data/MonzoProfileApi;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Ljavax/a/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/common/accounts/d;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Ljavax/a/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/topup/a/a;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 10
    const-class v0, Lco/uk/getmondo/profile/data/c;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lco/uk/getmondo/profile/data/c;->a:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/api/MonzoApi;",
            ">;",
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/profile/data/MonzoProfileApi;",
            ">;",
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/common/accounts/d;",
            ">;",
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/topup/a/a;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    sget-boolean v0, Lco/uk/getmondo/profile/data/c;->a:Z

    if-nez v0, :cond_0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 29
    :cond_0
    iput-object p1, p0, Lco/uk/getmondo/profile/data/c;->b:Ljavax/a/a;

    .line 30
    sget-boolean v0, Lco/uk/getmondo/profile/data/c;->a:Z

    if-nez v0, :cond_1

    if-nez p2, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 31
    :cond_1
    iput-object p2, p0, Lco/uk/getmondo/profile/data/c;->c:Ljavax/a/a;

    .line 32
    sget-boolean v0, Lco/uk/getmondo/profile/data/c;->a:Z

    if-nez v0, :cond_2

    if-nez p3, :cond_2

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 33
    :cond_2
    iput-object p3, p0, Lco/uk/getmondo/profile/data/c;->d:Ljavax/a/a;

    .line 34
    sget-boolean v0, Lco/uk/getmondo/profile/data/c;->a:Z

    if-nez v0, :cond_3

    if-nez p4, :cond_3

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 35
    :cond_3
    iput-object p4, p0, Lco/uk/getmondo/profile/data/c;->e:Ljavax/a/a;

    .line 36
    return-void
.end method

.method public static a(Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;)Lb/a/b;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/api/MonzoApi;",
            ">;",
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/profile/data/MonzoProfileApi;",
            ">;",
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/common/accounts/d;",
            ">;",
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/topup/a/a;",
            ">;)",
            "Lb/a/b",
            "<",
            "Lco/uk/getmondo/profile/data/a;",
            ">;"
        }
    .end annotation

    .prologue
    .line 52
    new-instance v0, Lco/uk/getmondo/profile/data/c;

    invoke-direct {v0, p0, p1, p2, p3}, Lco/uk/getmondo/profile/data/c;-><init>(Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;)V

    return-object v0
.end method


# virtual methods
.method public a()Lco/uk/getmondo/profile/data/a;
    .locals 5

    .prologue
    .line 40
    new-instance v4, Lco/uk/getmondo/profile/data/a;

    iget-object v0, p0, Lco/uk/getmondo/profile/data/c;->b:Ljavax/a/a;

    .line 41
    invoke-interface {v0}, Ljavax/a/a;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/api/MonzoApi;

    iget-object v1, p0, Lco/uk/getmondo/profile/data/c;->c:Ljavax/a/a;

    .line 42
    invoke-interface {v1}, Ljavax/a/a;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lco/uk/getmondo/profile/data/MonzoProfileApi;

    iget-object v2, p0, Lco/uk/getmondo/profile/data/c;->d:Ljavax/a/a;

    .line 43
    invoke-interface {v2}, Ljavax/a/a;->b()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lco/uk/getmondo/common/accounts/d;

    iget-object v3, p0, Lco/uk/getmondo/profile/data/c;->e:Ljavax/a/a;

    .line 44
    invoke-interface {v3}, Ljavax/a/a;->b()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lco/uk/getmondo/topup/a/a;

    invoke-direct {v4, v0, v1, v2, v3}, Lco/uk/getmondo/profile/data/a;-><init>(Lco/uk/getmondo/api/MonzoApi;Lco/uk/getmondo/profile/data/MonzoProfileApi;Lco/uk/getmondo/common/accounts/d;Lco/uk/getmondo/topup/a/a;)V

    .line 40
    return-object v4
.end method

.method public synthetic b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 10
    invoke-virtual {p0}, Lco/uk/getmondo/profile/data/c;->a()Lco/uk/getmondo/profile/data/a;

    move-result-object v0

    return-object v0
.end method
