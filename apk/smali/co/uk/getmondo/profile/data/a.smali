.class public final Lco/uk/getmondo/profile/data/a;
.super Ljava/lang/Object;
.source "ProfileManager.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000L\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\'\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u00a2\u0006\u0002\u0010\nJ\u001a\u0010\r\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00100\u000f0\u000e2\u0006\u0010\u0011\u001a\u00020\u0012J\u0006\u0010\u0013\u001a\u00020\u0014J\u0016\u0010\u0015\u001a\u00020\u00142\u0006\u0010\u0016\u001a\u00020\u00102\u0006\u0010\u0017\u001a\u00020\u0018R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u000cX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0019"
    }
    d2 = {
        "Lco/uk/getmondo/profile/data/ProfileManager;",
        "",
        "monzoApi",
        "Lco/uk/getmondo/api/MonzoApi;",
        "profileApi",
        "Lco/uk/getmondo/profile/data/MonzoProfileApi;",
        "accountService",
        "Lco/uk/getmondo/common/accounts/AccountService;",
        "idempotencyGenerator",
        "Lco/uk/getmondo/topup/util/IdempotencyGenerator;",
        "(Lco/uk/getmondo/api/MonzoApi;Lco/uk/getmondo/profile/data/MonzoProfileApi;Lco/uk/getmondo/common/accounts/AccountService;Lco/uk/getmondo/topup/util/IdempotencyGenerator;)V",
        "addressMapper",
        "Lco/uk/getmondo/model/mapper/LegacyAddressMapper;",
        "lookupPostcode",
        "Lio/reactivex/Single;",
        "",
        "Lco/uk/getmondo/model/LegacyAddress;",
        "postcode",
        "",
        "syncProfile",
        "Lio/reactivex/Completable;",
        "updateAddress",
        "address",
        "challenge",
        "Lco/uk/getmondo/common/pin/data/model/Challenge;",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field private final a:Lco/uk/getmondo/d/a/i;

.field private final b:Lco/uk/getmondo/api/MonzoApi;

.field private final c:Lco/uk/getmondo/profile/data/MonzoProfileApi;

.field private final d:Lco/uk/getmondo/common/accounts/d;

.field private final e:Lco/uk/getmondo/topup/a/a;


# direct methods
.method public constructor <init>(Lco/uk/getmondo/api/MonzoApi;Lco/uk/getmondo/profile/data/MonzoProfileApi;Lco/uk/getmondo/common/accounts/d;Lco/uk/getmondo/topup/a/a;)V
    .locals 1

    .prologue
    const-string v0, "monzoApi"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "profileApi"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "accountService"

    invoke-static {p3, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "idempotencyGenerator"

    invoke-static {p4, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lco/uk/getmondo/profile/data/a;->b:Lco/uk/getmondo/api/MonzoApi;

    iput-object p2, p0, Lco/uk/getmondo/profile/data/a;->c:Lco/uk/getmondo/profile/data/MonzoProfileApi;

    iput-object p3, p0, Lco/uk/getmondo/profile/data/a;->d:Lco/uk/getmondo/common/accounts/d;

    iput-object p4, p0, Lco/uk/getmondo/profile/data/a;->e:Lco/uk/getmondo/topup/a/a;

    .line 25
    new-instance v0, Lco/uk/getmondo/d/a/i;

    invoke-direct {v0}, Lco/uk/getmondo/d/a/i;-><init>()V

    iput-object v0, p0, Lco/uk/getmondo/profile/data/a;->a:Lco/uk/getmondo/d/a/i;

    return-void
.end method

.method public static final synthetic a(Lco/uk/getmondo/profile/data/a;)Lco/uk/getmondo/d/a/i;
    .locals 1

    .prologue
    .line 17
    iget-object v0, p0, Lco/uk/getmondo/profile/data/a;->a:Lco/uk/getmondo/d/a/i;

    return-object v0
.end method

.method public static final synthetic b(Lco/uk/getmondo/profile/data/a;)Lco/uk/getmondo/profile/data/MonzoProfileApi;
    .locals 1

    .prologue
    .line 17
    iget-object v0, p0, Lco/uk/getmondo/profile/data/a;->c:Lco/uk/getmondo/profile/data/MonzoProfileApi;

    return-object v0
.end method

.method public static final synthetic c(Lco/uk/getmondo/profile/data/a;)Lco/uk/getmondo/topup/a/a;
    .locals 1

    .prologue
    .line 17
    iget-object v0, p0, Lco/uk/getmondo/profile/data/a;->e:Lco/uk/getmondo/topup/a/a;

    return-object v0
.end method

.method public static final synthetic d(Lco/uk/getmondo/profile/data/a;)Lco/uk/getmondo/common/accounts/d;
    .locals 1

    .prologue
    .line 17
    iget-object v0, p0, Lco/uk/getmondo/profile/data/a;->d:Lco/uk/getmondo/common/accounts/d;

    return-object v0
.end method


# virtual methods
.method public final a()Lio/reactivex/b;
    .locals 3

    .prologue
    .line 55
    iget-object v0, p0, Lco/uk/getmondo/profile/data/a;->c:Lco/uk/getmondo/profile/data/MonzoProfileApi;

    invoke-interface {v0}, Lco/uk/getmondo/profile/data/MonzoProfileApi;->profile()Lio/reactivex/v;

    move-result-object v2

    .line 56
    new-instance v0, Lco/uk/getmondo/profile/data/a$b;

    new-instance v1, Lco/uk/getmondo/d/a/p;

    invoke-direct {v1}, Lco/uk/getmondo/d/a/p;-><init>()V

    invoke-direct {v0, v1}, Lco/uk/getmondo/profile/data/a$b;-><init>(Lco/uk/getmondo/d/a/p;)V

    check-cast v0, Lkotlin/d/a/b;

    new-instance v1, Lco/uk/getmondo/profile/data/b;

    invoke-direct {v1, v0}, Lco/uk/getmondo/profile/data/b;-><init>(Lkotlin/d/a/b;)V

    move-object v0, v1

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {v2, v0}, Lio/reactivex/v;->d(Lio/reactivex/c/h;)Lio/reactivex/v;

    move-result-object v1

    .line 57
    new-instance v0, Lco/uk/getmondo/profile/data/a$c;

    invoke-direct {v0, p0}, Lco/uk/getmondo/profile/data/a$c;-><init>(Lco/uk/getmondo/profile/data/a;)V

    check-cast v0, Lio/reactivex/c/g;

    invoke-virtual {v1, v0}, Lio/reactivex/v;->c(Lio/reactivex/c/g;)Lio/reactivex/v;

    move-result-object v0

    .line 58
    invoke-virtual {v0}, Lio/reactivex/v;->c()Lio/reactivex/b;

    move-result-object v0

    const-string v1, "profileApi.profile()\n   \u2026         .toCompletable()"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public final a(Lco/uk/getmondo/d/s;Lco/uk/getmondo/common/pin/a/a/a;)Lio/reactivex/b;
    .locals 2

    .prologue
    const-string v0, "address"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "challenge"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 37
    iget-object v0, p0, Lco/uk/getmondo/profile/data/a;->d:Lco/uk/getmondo/common/accounts/d;

    invoke-virtual {v0}, Lco/uk/getmondo/common/accounts/d;->c()Lio/reactivex/v;

    move-result-object v1

    .line 38
    sget-object v0, Lco/uk/getmondo/profile/data/a$d;->a:Lco/uk/getmondo/profile/data/a$d;

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {v1, v0}, Lio/reactivex/v;->d(Lio/reactivex/c/h;)Lio/reactivex/v;

    move-result-object v1

    .line 39
    new-instance v0, Lco/uk/getmondo/profile/data/a$e;

    invoke-direct {v0, p0, p1, p2}, Lco/uk/getmondo/profile/data/a$e;-><init>(Lco/uk/getmondo/profile/data/a;Lco/uk/getmondo/d/s;Lco/uk/getmondo/common/pin/a/a/a;)V

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {v1, v0}, Lio/reactivex/v;->c(Lio/reactivex/c/h;)Lio/reactivex/b;

    move-result-object v1

    .line 51
    invoke-virtual {p0}, Lco/uk/getmondo/profile/data/a;->a()Lio/reactivex/b;

    move-result-object v0

    check-cast v0, Lio/reactivex/d;

    invoke-virtual {v1, v0}, Lio/reactivex/b;->b(Lio/reactivex/d;)Lio/reactivex/b;

    move-result-object v0

    const-string v1, "accountService.user()\n  \u2026  .andThen(syncProfile())"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public final a(Ljava/lang/String;)Lio/reactivex/v;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/v",
            "<",
            "Ljava/util/List",
            "<",
            "Lco/uk/getmondo/d/s;",
            ">;>;"
        }
    .end annotation

    .prologue
    const-string v0, "postcode"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 28
    iget-object v0, p0, Lco/uk/getmondo/profile/data/a;->b:Lco/uk/getmondo/api/MonzoApi;

    sget-object v1, Lco/uk/getmondo/d/i;->Companion:Lco/uk/getmondo/d/i$a;

    invoke-virtual {v1}, Lco/uk/getmondo/d/i$a;->a()Lco/uk/getmondo/d/i;

    move-result-object v1

    invoke-virtual {v1}, Lco/uk/getmondo/d/i;->e()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Lco/uk/getmondo/api/MonzoApi;->lookupPostcode(Ljava/lang/String;Ljava/lang/String;)Lio/reactivex/v;

    move-result-object v1

    .line 29
    new-instance v0, Lco/uk/getmondo/profile/data/a$a;

    invoke-direct {v0, p0}, Lco/uk/getmondo/profile/data/a$a;-><init>(Lco/uk/getmondo/profile/data/a;)V

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {v1, v0}, Lio/reactivex/v;->a(Lio/reactivex/c/h;)Lio/reactivex/v;

    move-result-object v0

    const-string v1, "monzoApi.lookupPostcode(\u2026oList()\n                }"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method
