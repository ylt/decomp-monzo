.class final Lco/uk/getmondo/profile/address/AddressSelectionView$d;
.super Ljava/lang/Object;
.source "AddressSelectionView.kt"

# interfaces
.implements Lio/reactivex/p;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lco/uk/getmondo/profile/address/AddressSelectionView;->b()Lio/reactivex/n;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/p",
        "<TT;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u0014\u0010\u0002\u001a\u0010\u0012\u000c\u0012\n \u0004*\u0004\u0018\u00010\u00010\u00010\u0003H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "",
        "emitter",
        "Lio/reactivex/ObservableEmitter;",
        "kotlin.jvm.PlatformType",
        "subscribe"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field final synthetic a:Lco/uk/getmondo/profile/address/AddressSelectionView;


# direct methods
.method constructor <init>(Lco/uk/getmondo/profile/address/AddressSelectionView;)V
    .locals 0

    iput-object p1, p0, Lco/uk/getmondo/profile/address/AddressSelectionView$d;->a:Lco/uk/getmondo/profile/address/AddressSelectionView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lio/reactivex/o;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/o",
            "<",
            "Lkotlin/n;",
            ">;)V"
        }
    .end annotation

    .prologue
    const-string v0, "emitter"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 121
    iget-object v0, p0, Lco/uk/getmondo/profile/address/AddressSelectionView$d;->a:Lco/uk/getmondo/profile/address/AddressSelectionView;

    sget v1, Lco/uk/getmondo/c$a;->addressRecyclerView:I

    invoke-virtual {v0, v1}, Lco/uk/getmondo/profile/address/AddressSelectionView;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView;

    new-instance v1, Lco/uk/getmondo/profile/address/AddressSelectionView$d$1;

    invoke-direct {v1, p1}, Lco/uk/getmondo/profile/address/AddressSelectionView$d$1;-><init>(Lio/reactivex/o;)V

    check-cast v1, Landroid/support/v7/widget/RecyclerView$m;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setOnScrollListener(Landroid/support/v7/widget/RecyclerView$m;)V

    .line 128
    new-instance v0, Lco/uk/getmondo/profile/address/AddressSelectionView$d$2;

    invoke-direct {v0, p0}, Lco/uk/getmondo/profile/address/AddressSelectionView$d$2;-><init>(Lco/uk/getmondo/profile/address/AddressSelectionView$d;)V

    check-cast v0, Lio/reactivex/c/f;

    invoke-interface {p1, v0}, Lio/reactivex/o;->a(Lio/reactivex/c/f;)V

    .line 129
    return-void
.end method
